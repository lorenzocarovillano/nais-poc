      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         IVVS0211.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *    PROGRAMMA...... IVVS0211
      *    TIPOLOGIA...... TRASVERSALE
      *    PROCESSO....... XXX
      *    FUNZIONE....... VALORIZZATORE VARIABILI
      *    DESCRIZIONE.... MAIN GESTORE VARIABILI
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
      *--  AREA DCLGEN
       01  AREA-BUSINESS.
      *----------------------------------------------------------------*
      *   AREA INPUT VARIABILI
      *   PORTAFOGLIO VITA - PROCESSO VENDITA
      *   LUNG.
      *----------------------------------------------------------------*
      *--  AREA ADESIONE
           02 WADE-AREA-ADESIONE.
              04 WADE-ELE-ADES-MAX       PIC S9(04) COMP.
              04 WADE-TAB-ADES.
              COPY LCCVADE1              REPLACING ==(SF)== BY ==WADE==.

      *--  AREA BENEFICIARI
           02 WBEP-AREA-BENEF.
              04 WBEP-ELE-BENEF-MAX      PIC S9(04) COMP.
                COPY LCCVBEPA REPLACING   ==(SF)==  BY ==WBEP==.
              COPY LCCVBEP1              REPLACING ==(SF)== BY ==WBEP==.
      *--  AREA BENEFICIARIO DI LIQUIDAZIONE
           02 WBEL-AREA-BENEF-LIQ.
              04 WBEL-ELE-BENEF-LIQ-MAX  PIC S9(04) COMP.
                COPY LCCVBELA REPLACING   ==(SF)==  BY ==WBEL==.
              COPY LCCVBEL1              REPLACING ==(SF)== BY ==WBEL==.
      *--  AREA DATI COLLETTIVA
           02 WDCO-AREA-DT-COLLETTIVA.
              04 WDCO-ELE-COLL-MAX       PIC S9(004) COMP.
              04 WDCO-TAB-COLL.
              COPY LCCVDCO1              REPLACING ==(SF)== BY ==WDCO==.
      *--  AREA DATI FISCALE ADESIONE
           02 WDFA-AREA-DT-FISC-ADES.
              04 WDFA-ELE-FISC-ADES-MAX  PIC S9(04) COMP.
              04 WDFA-TAB-FISC-ADES.
              COPY LCCVDFA1              REPLACING ==(SF)== BY ==WDFA==.
      *--  AREA DETTAGLIO QUESTIONARIO
           02 WDEQ-AREA-DETT-QUEST.
              04 WDEQ-ELE-DETT-QUEST-MAX PIC S9(04) COMP.
            03  WDEQ-TAB.
              04 WDEQ-TAB-DETT-QUEST     OCCURS 180.
              COPY LCCVDEQ1              REPLACING ==(SF)== BY ==WDEQ==.
            03  WDEQ-TAB-R REDEFINES WDEQ-TAB.
              04 FILLER                  PIC X(558).
              04 WDEQ-RESTO-TAB          PIC X(99882).
      *--  AREA DETTAGLIO TITOLO CONTABILE
           02 WDTC-AREA-DETT-TIT-CONT.
              04 WDTC-ELE-DETT-TIT-MAX   PIC S9(04) COMP.
                COPY LCCVDTCA REPLACING   ==(SF)==  BY ==WDTC==.
              COPY LCCVDTC1              REPLACING ==(SF)== BY ==WDTC==.
      *--  AREA GARANZIA
           02 WGRZ-AREA-GARANZIA.
              04 WGRZ-ELE-GARANZIA-MAX   PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
              COPY LCCVGRZ1              REPLACING ==(SF)== BY ==WGRZ==.
      *--  AREA GARANZIA DI LIQUIDAZIONE
           02 WGRL-AREA-GARANZIA-LIQ.
              04 WGRL-ELE-GAR-LIQ-MAX    PIC S9(04) COMP.
                COPY LCCVGRLA REPLACING   ==(SF)==  BY ==WGRL==.
              COPY LCCVGRL1              REPLACING ==(SF)== BY ==WGRL==.
      *--  AREA IMPOSTA SOSTITUTIVA
           02 WISO-AREA-IMPOSTA-SOST.
              04 WISO-ELE-IMP-SOST-MAX   PIC S9(04) COMP.
              04 WISO-TAB-IMP-SOST.
              COPY LCCVISO1              REPLACING ==(SF)== BY ==WISO==.
      *--  AREA LIQUIDAZIONE
           02 WLQU-AREA-LIQUIDAZIONE.
              04 WLQU-ELE-LIQ-MAX        PIC S9(04) COMP.
                COPY LCCVLQUB REPLACING   ==(SF)==  BY ==WLQU==.
              COPY LCCVLQU1              REPLACING ==(SF)== BY ==WLQU==.
      *--  AREA MOVIMENTO
           02 WMOV-AREA-MOVIMENTO.
              04 WMOV-ELE-MOVI-MAX       PIC S9(04) COMP.
              04 WMOV-TAB-MOVI.
              COPY LCCVMOV1              REPLACING ==(SF)== BY ==WMOV==.
      *--  AREA MOVIMENTO FINANZIARIO
           02 WMFZ-AREA-MOVI-FINRIO.
              04 WMFZ-ELE-MOVI-FINRIO-MAX  PIC S9(04) COMP.
              04 WMFZ-TAB-MOVI-FINRIO    OCCURS 10.
                 COPY LCCVMFZ1           REPLACING ==(SF)== BY ==WMFZ==.
      *--  AREA PARAMETRO OGGETTO
           02 WPOG-AREA-PARAM-OGG.
              04 WPOG-ELE-PARAM-OGG-MAX  PIC S9(04) COMP.
            03  WPOG-TAB.
                COPY LCCVPOGB REPLACING   ==(SF)==  BY ==WPOG==.
              COPY LCCVPOG1              REPLACING ==(SF)== BY ==WPOG==.
            03  WPOG-TAB-R REDEFINES WPOG-TAB.
              04 FILLER                  PIC X(255).
              04 WPOG-RESTO-TAB          PIC X(50745).
      *--  AREA PARAMETRO MOVIMENTO
           02 WPMO-AREA-PARAM-MOV.
              04 WPMO-ELE-PARAM-MOV-MAX  PIC S9(04) COMP.
            03  WPMO-TAB.
                COPY LCCVPMOB REPLACING   ==(SF)==  BY ==WPMO==.
              COPY LCCVPMO1              REPLACING ==(SF)== BY ==WPMO==.
            03  WPMO-TAB-R REDEFINES WPMO-TAB.
              04 FILLER                  PIC X(323).
              04 WPMO-RESTO-TAB          PIC X(31977).
      *--  AREA PERCIPIENTE LIQUIDAZIONE
           02 WPLI-AREA-PERC-LIQ.
              04 WPLI-ELE-PERC-LIQ-MAX   PIC S9(04) COMP.
                COPY LCCVPLIA REPLACING   ==(SF)==  BY ==WPLI==.
              COPY LCCVPLI1              REPLACING ==(SF)== BY ==WPLI==.
      *--  AREA POLIZZA
           02 WPOL-AREA-POLIZZA.
              04 WPOL-ELE-POLI-MAX       PIC S9(04) COMP.
              04 WPOL-TAB-POLI.
              COPY LCCVPOL1              REPLACING ==(SF)== BY ==WPOL==.
      *--  AREA PRESTITI
           02 WPRE-AREA-PRESTITI.
              04 WPRE-ELE-PRESTITI-MAX   PIC S9(04) COMP.
              04 WPRE-TAB-PRESTITI       OCCURS 10.
              COPY LCCVPRE1              REPLACING ==(SF)== BY ==WPRE==.
      *--  AREA PROVVIGIONE DI TRANCHE
           02 WPVT-AREA-PROV.
              04 WPVT-ELE-PROV-MAX       PIC S9(04) COMP.
                COPY LCCVPVTA REPLACING   ==(SF)==  BY ==WPVT==.
              COPY LCCVPVT1              REPLACING ==(SF)== BY ==WPVT==.
      *--  AREA QUESTIONARIO
           02 WQUE-AREA-QUEST.
              04 WQUE-ELE-QUEST-MAX      PIC S9(04) COMP.
              04 WQUE-TAB-QUEST          OCCURS 12.
              COPY LCCVQUE1              REPLACING ==(SF)== BY ==WQUE==.
      *--  AREA RICHIESTA
           02 WRIC-AREA-RICH.
              04 WRIC-ELE-RICH-MAX       PIC S9(04) COMP.
              04 WRIC-TAB-RICH.
              COPY LCCVRIC1              REPLACING ==(SF)== BY ==WRIC==.
      *--  AREA RICHIESTA DISINVESTIMENTO FONDO
           02 WRDF-AREA-RICH-DISINV-FND.
              03 WRDF-ELE-RIC-INV-MAX      PIC S9(04) COMP.
              03 WRDF-TABELLA.
                 COPY LCCVRDFA REPLACING   ==(SF)==  BY ==WRDF==.
                 COPY LCCVRDF1           REPLACING ==(SF)== BY ==WRDF==.
              03 WRDF-TABELLA1 REDEFINES WRDF-TABELLA.
                 04 WRDF-EL-TABELLA  OCCURS 1250  PIC X(217).
              03 WRDF-TABELLA-R REDEFINES WRDF-TABELLA.
IDD2  *          04 FILLER                  PIC X(208).
IDD2  *          04 FILLER                  PIC X(216).
376907           04 FILLER                  PIC X(217).
IDD2  *          04 WRDF-RESTO-TABELLA      PIC X(259792).
IDD2  *          04 WRDF-RESTO-TABELLA      PIC X(269784).
376907           04 WRDF-RESTO-TABELLA      PIC X(271033).
      *--  AREA RICHIESTA INVESTIMENTO FONDO
           02 WRIF-AREA-RICH-INV-FND.
                03 WRIF-ELE-RIC-INV-MAX      PIC S9(04) COMP.
                03 WRIF-TABELLA.
                 COPY LCCVRIFA REPLACING   ==(SF)==  BY ==WRIF==.
                 COPY LCCVRIF1           REPLACING ==(SF)== BY ==WRIF==.
              03 WRIF-TABELLA1 REDEFINES WRIF-TABELLA.
                 04 WRIF-EL-TABELLA  OCCURS 1250  PIC X(193).
              03 WRIF-TABELLA-R REDEFINES WRIF-TABELLA.
376907*          04 FILLER                  PIC X(192).
376907           04 FILLER                  PIC X(193).
376907*          04 WRIF-RESTO-TABELLA      PIC X(239808).
376907           04 WRIF-RESTO-TABELLA      PIC X(241057).
      *--  AREA RAPPORTO ANAGRAFICO
           02 WRAN-AREA-RAPP-ANAG.
              04 WRAN-ELE-RAPP-ANAG-MAX  PIC S9(04) COMP.
                COPY LCCVRANA REPLACING   ==(SF)==  BY ==WRAN==.
              COPY LCCVRAN1              REPLACING ==(SF)== BY ==WRAN==.
      *--  AREA RAPPORTO ANAGRAFICO
           02 WE15-AREA-EST-RAPP-ANAG.
              04 WE15-ELE-EST-RAPP-ANAG-MAX  PIC S9(04) COMP.
                COPY LCCVE15A REPLACING   ==(SF)==  BY ==WE15==.
              COPY LCCVE151              REPLACING ==(SF)== BY ==WE15==.
      *--  AREA RAPPORTO RETE
           02 WRRE-AREA-RAPP-RETE.
              04 WRRE-ELE-RAPP-RETE-MAX  PIC S9(04) COMP.
              04 WRRE-TAB-RAPP-RETE      OCCURS 10.
              COPY LCCVRRE1              REPLACING ==(SF)== BY ==WRRE==.
      *--  AREA SOPRAPREMIO DI GARANZIA
           02 WSPG-AREA-SOPRAP-GAR.
              04 WSPG-ELE-SOPRAP-GAR-MAX PIC S9(04) COMP.
                COPY LCCVSPGA REPLACING   ==(SF)==  BY ==WSPG==.
              COPY LCCVSPG1              REPLACING ==(SF)== BY ==WSPG==.
      *--  AREA STRATEGIA DI INVESTIMENTO
           02 WSDI-AREA-STRA-INV.
              04 WSDI-ELE-STRA-INV-MAX   PIC S9(04) COMP.
              04 WSDI-TAB-STRA-INV       OCCURS 20.
              COPY LCCVSDI1              REPLACING ==(SF)== BY ==WSDI==.
      *--  AREA TITOLO CONTABILE
           02 WTIT-AREA-TIT-CONT.
              04 WTIT-ELE-TIT-CONT-MAX   PIC S9(04) COMP.
                COPY LCCVTITA REPLACING   ==(SF)==  BY ==WTIT==.
              COPY LCCVTIT1              REPLACING ==(SF)== BY ==WTIT==.
      *--  AREA TITOLO DI LIQUIDAZIONE
           02 WTCL-AREA-TIT-LIQ.
              04 WTCL-ELE-TIT-LIQ-MAX    PIC S9(04) COMP.
              04 WTCL-TAB-TIT-LIQ.
              COPY LCCVTCL1              REPLACING ==(SF)== BY ==WTCL==.
      *--  AREA TRANCHE DI GARANZIA
           02 WTGA-AREA-TRANCHE.
              04 WTGA-ELE-TRAN-MAX       PIC S9(04) COMP.
            03  WTGA-TAB.
                COPY LCCVTGAC REPLACING   ==(SF)==  BY ==WTGA==.
              COPY LCCVTGA1              REPLACING ==(SF)== BY ==WTGA==.
            03  WTGA-TAB-R REDEFINES WTGA-TAB.
IDD2  *       04 FILLER                  PIC X(911).
IDD2          04 FILLER                  PIC X(927).
IDD2  *       04 WTGA-RESTO-TAB          PIC X(1137839).
IDD2          04 WTGA-RESTO-TAB          PIC X(1157823).
      *-- AREA TRANCHE GARANZIA DI LIQUIDAZIONE
           02 WTLI-AREA-TRCH-LIQ.
              04 WTLI-ELE-TRCH-LIQ-MAX   PIC S9(04) COMP.
            03  WTLI-TAB.
                COPY LCCVTLIB REPLACING   ==(SF)==  BY ==WTLI==.
              COPY LCCVTLI1              REPLACING ==(SF)== BY ==WTLI==.
            03  WTLI-TAB-R REDEFINES WTLI-TAB.
              04 FILLER                  PIC X(254).
              04 WTLI-RESTO-TAB          PIC X(317246).
      *-- AREA DEFAULT ADESIONE
           02 WDAD-AREA-DEFAULT-ADES.
              04 WDAD-ELE-DFLT-ADES-MAX  PIC S9(04) COMP.
              04 WDAD-TAB-DFLT-ADES.
              COPY LCCVDAD1              REPLACING ==(SF)== BY ==WDAD==.
      *-- AREA OGGETTO COLLEGATO
           02 WOCO-AREA-OGG-COLL.
              04 WOCO-ELE-OGG-COLL-MAX   PIC S9(04) COMP.
            03  WOCO-TAB.
              04 WOCO-TAB-OGG-COLL       OCCURS 60.
              COPY LCCVOCO1              REPLACING ==(SF)== BY ==WOCO==.
            03  WOCO-TAB-R REDEFINES WOCO-TAB.
              04 FILLER                  PIC X(311).
              04 WOCO-RESTO-TAB          PIC X(18349).
      *-- AREA DATI FORZATI DI LIQUIDAZIONE
           02 WDFL-AREA-DFL.
              04 WDFL-ELE-DFL-MAX        PIC S9(004) COMP.
              04 WDFL-TAB-DFL.
              COPY LCCVDFL1              REPLACING ==(SF)== BY ==WDFL==.
      *-- AREA RISERVA DI TRANCHE
           02 WRST-AREA-RST.
              04 WRST-ELE-RST-MAX        PIC S9(004) COMP.
              04 WRST-TAB-RST            OCCURS 10.
              COPY LCCVRST1              REPLACING ==(SF)== BY ==WRST==.
      *-- AREA QUOTAZ FONDI UNIT
           02 WL19-AREA-QUOTE.
              COPY LCCVL197              REPLACING ==(SF)== BY ==WL19==.
              COPY LCCVL191              REPLACING ==(SF)== BY ==WL19==.
              03 WL19-TABELLA1 REDEFINES WL19-TABELLA.
                 04 WL19-EL-TABELLA  OCCURS 250  PIC X(85).
              03 WL19-TABELLA-R REDEFINES WL19-TABELLA.
                 04 FILLER                  PIC X(85).
                 04 WL19-RESTO-TABELLA      PIC X(21165).
      *-- AREA REINVEST
           02 WL30-AREA-REINVST-POLI.
              04 WL30-ELE-REINVST-POLI-MAX PIC S9(04) COMP.
              04 WL30-TAB-REINVST-POLI   OCCURS 40.
              COPY LCCVL301              REPLACING ==(SF)== BY ==WL30==.
      *--  VINCOLO PEGNO
           02 WL23-AREA-VINC-PEG.
              04 WL23-ELE-VINC-PEG-MAX   PIC S9(04) COMP.
              04 WL23-TAB-VINC-PEG       OCCURS 10.
              COPY LCCVL231              REPLACING ==(SF)== BY ==WL23==.
      *-- AREA OPZIONI
           02 WOPZ-AREA-OPZIONI.
              COPY IVVC0217              REPLACING ==(SF)== BY ==WOPZ==.
      *--  AREA GARANZIA DI OPZIONE
           02 WGOP-AREA-GARANZIA-OPZ.
              04 WGOP-ELE-GARANZIA-OPZ-MAX   PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGOP==.
              COPY LCCVGRZ1              REPLACING ==(SF)== BY ==WGOP==.
      *--  AREA TRANCHE DI GARANZIADI OPZIONE
           02 WTOP-AREA-TRANCHE-OPZ.
              04 WTOP-ELE-TRAN-OPZ-MAX       PIC S9(04) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==WTOP==.
              COPY LCCVTGA1              REPLACING ==(SF)== BY ==WTOP==.
      *--  AREA DATI CONTESTUALI
           02 WCNT-AREA-DATI-CONTEST.
              COPY IVVC0212              REPLACING ==(SF)== BY ==WCNT==.
      *--  AREA CLAUSOLE DI TESTO
           02 WCLT-AREA-CLAU-TXT.
              04 WCLT-ELE-CLAU-TXT-MAX       PIC S9(04) COMP.
              04 WCLT-TAB-CLAU-TXT           OCCURS 54.
              COPY LCCVCLT1              REPLACING ==(SF)== BY ==WCLT==.
NEW   *--  IMPOSTA DI BOLLO
NEW        02 WP58-AREA-IMPOSTA-BOLLO.
NEW           04 WP58-ELE-MAX-IMPST-BOL PIC S9(04) COMP.
52921 *       04 WP58-TAB-IMPST-BOL OCCURS 50.
52921         04 WP58-TAB-IMPST-BOL OCCURS 75.
NEW           COPY LCCVP581              REPLACING ==(SF)== BY ==WP58==.
NEW01 *--  DATI CRISTALLIZZATI
NEW01      02 WP61-AREA-D-CRIST.
NEW01         04 WP61-ELE-MAX-D-CRIST   PIC S9(04) COMP.
NEW01         04 WP61-TAB-D-CRIST.
NEW01         COPY LCCVP611              REPLACING ==(SF)== BY ==WP61==.
      *--  DATI AGGIUNTIVI PER POLIZZE CPI
           02 WP67-AREA-EST-POLI-CPI-PR.
              04 WP67-ELE-MAX-EST-POLI-CPI-PR   PIC S9(04) COMP.
              04 WP67-TAB-EST-POLI-CPI-PR.
              COPY LCCVP671              REPLACING ==(SF)== BY ==WP67==.
      *--  RICHIESTA ESTERNA
           02 WP01-AREA-RICH-EST.
              04 WP01-ELE-MAX-RICH-EST   PIC S9(04) COMP.
              04 WP01-TAB-RICH-EST.
              COPY LCCVP011              REPLACING ==(SF)== BY ==WP01==.
      *--  AREA CONTROVALORI FONDI X TRANCE
           02 IVVC0222-AREA-FND-X-TRANCHE.
              04 IVVC0222-ELE-TRCH-MAX       PIC S9(04) COMP.
              COPY LCCVTGAC          REPLACING ==(SF)== BY ==IVVC0222==.
              COPY IVVC0222          REPLACING ==(SF)== BY ==IVVC0222==.
      *-- MOTIVO LIQUIDAZIONE
           02 WP86-AREA-MOT-LIQ.
              04 WP86-ELE-MOT-LIQ-MAX    PIC S9(04) COMP.
              04 WP86-TAB-MOT-LIQ        OCCURS 01.
              COPY LCCVP861              REPLACING ==(SF)== BY ==WP86==.
12969 *--  AREA VARIABILI PER GARANZIA
12969      02 AREA-IVVC0223.
              COPY IVVC0223          REPLACING ==(SF)== BY ==IVVC0223==.
      *--  VARIABILE PER APPOGGIO MOVIMENTO DI ORIGINE (X LIQUIDAZIONE)
           02 WK-MOVI-ORIG                   PIC 9(05).
      *--  ATTIVAZIONE SERVIZI VALORE
           02 WP88-AREA-SERV-VAL.
              04 WP88-ELE-SER-VAL-MAX         PIC S9(04) COMP.
              04 WP88-TAB-SERV-VAL                 OCCURS 10.
              COPY LCCVP881                   REPLACING ==(SF)==
                                              BY ==WP88==.
      *--  DETTAGLIO ATTIVAZIONE SERVIZI VALORE
           02 WP89-AREA-DSERV-VAL.
              04 WP89-ELE-DSERV-VAL-MAX        PIC S9(04) COMP.
48945 *       04 WP89-TAB-DSERV-VAL                 OCCURS 250.
48945         04 WP89-TAB-DSERV-VAL                 OCCURS 350.
              COPY LCCVP891                   REPLACING ==(SF)==
                                              BY ==WP89==.
13385 *--  QUESTIONARIO ADEGUATA VERIFICA
13385      02 WP56-AREA-QUEST-ADEG-VER.
13385         04 WP56-QUEST-ADEG-VER-MAX    PIC S9(004) COMP.
             COPY LCCVP56A REPLACING   ==(SF)==  BY ==WP56==.
13385         COPY LCCVP561                 REPLACING ==(SF)==
13385                                              BY ==WP56==.

16324 *--  AREA AREA FONDI COMMISSIONI GESTIONE
16324      02 WCDG-AREA-COMMIS-GEST.
16324         COPY IVVC0224                 REPLACING ==(SF)==
16324                                              BY ==WCDG==.

      *--  INPUT PER VALORIZZATORE
       01  IVVC0216-FLG-AREA                      PIC  X(02).
           88 IVVC0216-AREA-VE                    VALUE 'VE'.
           88 IVVC0216-AREA-PV                    VALUE 'PV'.
      *--  AREA SCHEDA VARIABILI
       01  AREA-IVVC0216.
           COPY IVVC0216         REPLACING ==(SF)== BY ==C216==.
      *    COPY IVVC0216         REPLACING ==(SF)== BY ==IVVC0216==.

      *--  AREA TEMPORANEA DI LAVORO SCHEDA VARIABILI UTILIZZATA DAL
      *    VALORIZZATORE PER POTER CONTENERE FINO A 100 VARIABILI
      *    LETTE DALLA TABELLA VAR_FUNZ_DI_CALC IN FASE DI VENDITA
      *    O POST-VENDITA
       01  AREA-WORK-IVVC0216.
           COPY IVVC0200         REPLACING ==(SF)== BY ==IVVC0216==.

      *--  AREA SCHEDA VARIABILI
       01  AREA-WARNING-IVVC0216.
           COPY IVVC0215         REPLACING ==(SF)== BY ==IVVC0216==.

      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

           COPY LDBV1351.
           COPY LDBV1361.

           COPY LCCV0021.

       01  AREA-IVVV0212.
           COPY IVVV0212         REPLACING ==(SF)== BY ==IVVV0212==.

       01 AREA-IDSV0001.
           COPY IDSV0001.

           COPY IDSV0003.

        01 STR-DIST-DT-VLDT-PROD-TGA.
           05 DIST-TAB-INFO          OCCURS 300
                                 INDEXED BY DIST-SEARCH-IND.
              07 DIST-TAB-DT-VLDT-PROD    PIC S9(8) COMP-3.
        01 STR-DIST-DT-VLDT-PROD-TGA-R REDEFINES
           STR-DIST-DT-VLDT-PROD-TGA.
           05 FILLER                        PIC X(5).
           05 STR-RES-DIST-DT-VLDT-PROD-TGA PIC X(1495).

      ***************************************************************
      * NOMI PGM
      ***************************************************************
       01  WK-PGM                         PIC  X(08) VALUE 'IVVS0211'.

      *----------------------------------------------------------------*
      *    MODULI CHIAMATI
      *----------------------------------------------------------------*
       01 PGM-IVVS0212                    PIC X(08) VALUE 'IVVS0212'.
       01 PGM-IVVS0216                    PIC X(08) VALUE 'IVVS0216'.
       01 PGM-LDBS1400                    PIC X(08) VALUE 'LDBS1400'.
       01 PGM-LCCS0020                    PIC X(08) VALUE 'LCCS0020'.
       01 PGM-LDBS0270                    PIC X(08) VALUE 'LDBS0270'.
       01 PGM-LDBS8800                    PIC X(08) VALUE 'LDBS8800'.
       01 PGM-LDBS1350                    PIC X(08) VALUE 'LDBS1350'.
       01 PGM-LDBS1360                    PIC X(08) VALUE 'LDBS1360'.

       01  WS-TP-LIVELLO.
           05 WS-LIV-PRODOTTO        PIC X(01) VALUE 'P'.
           05 WS-LIV-GARANZIA        PIC X(01) VALUE 'G'.
      *----------------------------------------------------------------*
      *    CAMPI X ROUTINES DI GESTIONE DISPLAY IDSP8888
      *----------------------------------------------------------------*
           COPY IDSV8888.
      *----------------------------------------------------------------*
      *    DCLGEN
      *----------------------------------------------------------------*
           COPY IDBVGRZ1.
           COPY IDBVTGA1.
           COPY IDBVMVV1.
           COPY IDBVVFC1.
           COPY IDBVAC51.
           COPY IDBVSTB1.

      *----------------------------------------------------------------*
      *    COPY TIPOLOGICA TIPO TRANCHE
      *----------------------------------------------------------------*
           COPY LCCVXTH0.

      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IND-LIVELLO                PIC S9(04) COMP.
           03 IND-STR                    PIC S9(04) COMP.
           03 IND-UNZIP                  PIC S9(04) COMP.
           03 IND-START-VAR              PIC S9(04) COMP.
           03 IND-END-VAR                PIC S9(04) COMP.
           03 IND-DISTINCT               PIC S9(04) COMP.
           03 IND-TGA                    PIC S9(04) COMP.
           03 IND-GRZ                    PIC S9(04) COMP.
           03 IND-GOP                    PIC S9(04) COMP.
           03 IND-TOP                    PIC S9(04) COMP.
           03 IND-COD-VAR                PIC S9(04) COMP.
           03 IND-TIPO-OPZ               PIC S9(04) COMP.
           03 IND-OPZ                    PIC S9(04) COMP.
           03 IND-CHAR                   PIC S9(04) COMP.

           03 IND-TAB-DIST               PIC S9(04) COMP.
           03 IND-RIGA                   PIC S9(04) COMP.
           03 IND-COL                    PIC S9(04) COMP.

      *----------------------------------------------------------------*
      *    LIMITI
      *----------------------------------------------------------------*
       01 LIMITE-OPZ                     PIC 9(03) VALUE 10.
       01 LIMITE-TIPO-OPZ                PIC 9(03) VALUE 10.
       01 LIMITE-VARIABILI-UNZIPPED      PIC 9(03) VALUE 100.

      *---------------------------------------------------------------*
      * COPY ELE-MAX
      *---------------------------------------------------------------*
           COPY LCCVGRZZ.
           COPY LCCVTGAZ.

      *----------------------------------------------------------------*
      *    FLAGS
      *----------------------------------------------------------------*
       01 FLAG-AREA                     PIC X(01).
          88 AREA-TROVATA-SI            VALUE 'S'.
          88 AREA-TROVATA-NO            VALUE 'N'.

       01 FLAG-DT-VLDT-PROD             PIC X(01).
          88 DT-VLDT-PROD-VALIDA-SI     VALUE 'S'.
          88 DT-VLDT-PROD-VALIDA-NO     VALUE 'N'.

       01 FLAG-BUFFER-DATI              PIC X(01).
          88 BUFFER-DATI-VALIDO-SI      VALUE 'S'.
          88 BUFFER-DATI-VALIDO-NO      VALUE 'N'.

       01 FLAG-VERSIONE                 PIC X(01).
          88 EMISSIONE                  VALUE 'E'.
          88 ULTIMA                     VALUE 'U'.
          88 SPAZIO                     VALUE ' '.

       01 FLAG-TIPOLOGIA-SCHEDA         PIC X(02).
          88 TIPO-STANDARD              VALUE 'ST'.
          88 TIPO-OPZIONI               VALUE 'OP'.

       01 FLAG-GESTIONE                 PIC X(02).
          88 GESTIONE-PRODOTTO          VALUE 'PR'.
          88 GESTIONE-GARANZIE          VALUE 'GA'.

       01 FLAG-TIPO-TRANCHE             PIC X(02).
          88 TRANCHE-INIT               VALUE '  '.
          88 TRANCHE-CONTESTO           VALUE 'TC'.
          88 TRANCHE-DB                 VALUE 'TD'.

       01 FLAG-TIPO-GARANZIA            PIC X(02).
          88 GARANZIA-INIT              VALUE '  '.
          88 GARANZIA-CONTESTO          VALUE 'GC'.
          88 GARANZIA-DB                VALUE 'GD'.

       01 FLAG-ESITO-GARANZIE           PIC X(01).
          88 FINE-GARANZIE-SI           VALUE 'S'.
          88 FINE-GARANZIE-NO           VALUE 'N'.

       01 FLAG-ESITO-TRANCHE            PIC X(01).
          88 FINE-TRANCHE-SI            VALUE 'S'.
          88 FINE-TRANCHE-NO            VALUE 'N'.

       01 FLAG-FINE-ELAB                PIC X(01).
          88 FINE-ELAB-SI               VALUE 'S'.
          88 FINE-ELAB-NO               VALUE 'N'.

       01 FLAG-TRATTA-TRANCHE           PIC X(01).
          88 TRATTA-TRANCHE-SI          VALUE 'S'.
          88 TRATTA-TRANCHE-NO          VALUE 'N'.

       01 FLAG-SKEDE                    PIC X(02).
          88 SKEDE-PRODOTTI             VALUE 'PR'.
          88 SKEDE-GARANZIE             VALUE 'GA'.
          88 SKEDE-TOTALI               VALUE '**'.

       01 FLAG-STAT-CAUS                PIC X(01).
          88 STAT-CAUS-TROVATI-SI       VALUE 'S'.
          88 STAT-CAUS-TROVATI-NO       VALUE 'N'.

       01 FLAG-VAR-FUNZ-CALC            PIC X(01).
          88 VAR-FUNZ-CALC-SI           VALUE 'S'.
          88 VAR-FUNZ-CALC-NO           VALUE 'N'.

       01 FLAG-RIASS                    PIC X(01).
          88 RIASS-SI                   VALUE 'S'.
          88 RIASS-NO                   VALUE 'N'.

       01 FLAG-APP                      PIC X(01).
          88 TROVATO-APP-SI             VALUE 'S'.
          88 TROVATO-APP-NO             VALUE 'N'.

       01 FLAG-TRATTATO-VALIDO          PIC X(01).
          88 FLAG-TRATTATO-VALIDO-SI    VALUE 'S'.
          88 FLAG-TRATTATO-VALIDO-NO    VALUE 'N'.
      ***************************************************************
      * COSTANTI
      ***************************************************************
       01 PRESENZA-VARIABILI           PIC 9(01) VALUE 0.
       01 PRESENZA-SERVIZIO            PIC 9(01) VALUE 1.

TRACE1 01 IVVC0211-LIVELLO-DEBUG              PIC 9(001) VALUE 0.
TRACE1    88 IVVC0211-NO-DEBUG                 VALUE 0.
TRACE1    88 IVVC0211-DEBUG-BASSO              VALUE 1.
TRACE1    88 IVVC0211-DEBUG-MEDIO              VALUE 2.
TRACE1    88 IVVC0211-DEBUG-ELEVATO            VALUE 3.
TRACE1    88 IVVC0211-DEBUG-ESASPERATO         VALUE 4.
TRACE1    88 IVVC0211-ANY-APPL-DBG             VALUE 1,
TRACE1                                               2,
TRACE1                                               3,
TRACE1                                               4.
TRACE1    88 IVVC0211-ARCH-BATCH-DBG           VALUE 5.
TRACE1    88 IVVC0211-COM-COB-JAV-DBG          VALUE 6.
TRACE1    88 IVVC0211-STRESS-TEST-DBG          VALUE 7.
TRACE1    88 IVVC0211-BUSINESS-DBG             VALUE 8.
TRACE1    88 IVVC0211-TOT-TUNING-DBG           VALUE 9.
TRACE1    88 IVVC0211-ANY-TUNING-DBG           VALUE 5,
TRACE1                                               6,
TRACE1                                               7,
TRACE1                                               8,
TRACE1                                               9.

      ***************************************************************
      * COMODO
      ***************************************************************
       01 WS-SQLCODE                      PIC ++++++++9.

       01 WS-ALIAS                        PIC X(03).

      *--> Area di appoggio per RIASS
       01 VARIABILI-RIASS            PIC 9(03).
       01 ELE-MAX-APP-COD-VAR        PIC 9(03).


       01 COMODO-STAT-CAUS.

           05 COMODO-OPER-LOG-STAT-BUS    PIC X(03).
           05 COMODO-STAT-BUS-TAB.
              10 COMODO-STAT-BUS-ELE      OCCURS 07.
                15 COMODO-TP-STAT-BUS     PIC X(02).

           05 COMODO-OPER-LOG-CAUS        PIC X(03).
           05 COMODO-TP-CAUS-TAB.
              10 COMODO-TP-CAUS-ELE       OCCURS 07.
                 15 COMODO-TP-CAUS        PIC X(02).

       01 COMODO-STAT-CAUS-LIQ.
          10 COM-TP-CAUS-01-LIQ       PIC X(02) VALUE 'ST'.
          10 COM-TP-CAUS-02-LIQ       PIC X(02) VALUE 'AR'.
          10 COM-TP-CAUS-03-LIQ       PIC X(02) VALUE 'RZ'.
          10 COM-TP-CAUS-04-LIQ       PIC X(02) VALUE 'RL'.


       01 WK-ID-POLI                      PIC 9(9).
       01 WK-ID-GAR                       PIC 9(9).

       01 WK-CODICE-COMPAGNIA             PIC 9(05).
       01 WK-TIPO-MOVIMENTO               PIC 9(05).

       01 WK-CODICE-PRODOTTO-VFC          PIC X(12).
       01 WK-CODICE-TARIFFA-VFC           PIC X(12).
       01 WK-CODICE-TARIFFA-AC5           PIC X(12).
       01 WK-DATA-INIZIO-VFC              PIC 9(09).
       01 WK-NOME-FUNZIONE-VFC            PIC X(12).
       01 WK-STEP-ELAB-VFC                PIC X(20).

       01 WK-GLOVARLIST                   PIC X(02).
          88 WK-GLOVARLIST-VUOTA          VALUE 'GV'.
          88 WK-GLOVARLIST-PIENA          VALUE 'GP'.


NEWP   01 WK-VAR-FUNZ-TROVATA             PIC X(02).
NEWP      88 WK-VAR-FUNZ-TROVATA-NO       VALUE 'NO'.
NEWP      88 WK-VAR-FUNZ-TROVATA-SI       VALUE 'SI'.


       01 DISTINCT-STR.
          05 DISTINCT-TAB-INFO                OCCURS 20
                             INDEXED BY DISTINCT-SEARCH-IND.
           10 DISTINCT-DT-INI-VAL-TAR     PIC S9(8)V COMP-3.

      *----------------------------------------------------------------*
      *    FLAGS
      *----------------------------------------------------------------*
       01  WK-VARLIST                       PIC X(01).
           88 SI-FINE-VARLIST               VALUE 'S'.
           88 NO-FINE-VARLIST               VALUE 'N'.

       01 UNZIP-STRUCTURE.
          05 UNZIP-LENGTH-STR-MAX           PIC 9(04).
          05 UNZIP-LENGTH-FIELD             PIC 9(02).
          05 UNZIP-STRING-ZIPPED            PIC X(4000).
          05 UNZIP-IDENTIFICATORE           PIC X(01).
          05 UNZIP-AREA-VARIABILI.
             10 UNZIP-ELE-VARIABILI-MAX     PIC S9(04) COMP-3.
             10 UNZIP-TAB.
                15 UNZIP-TAB-VARIABILI      OCCURS 100.
                   20 UNZIP-COD-VARIABILE   PIC  X(30).
             10 UNZIP-TAB-R REDEFINES UNZIP-TAB.
                15 FILLER                   PIC  X(30).
                15 UNZIP-RESTO-TAB          PIC  X(2970).

       01 FLAG-UNZIP-TROVATO                PIC X(01).
          88 UNZIP-TROVATO-SI               VALUE 'S'.
          88 UNZIP-TROVATO-NO               VALUE 'N'.

       01  WK-ELE-VARIABILI-MAX              PIC 9(003).

      ******************************************************************
      * STRUTTURA PER GESTIONE CACHE COMP_STR_DATO / ANAG_DATO
      ******************************************************************

       01 AREA-IDSV0101.
          COPY IDSV0101.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*

       01  INPUT-IVVS0211.
           COPY IVVC0211       REPLACING ==(SF)== BY ==IVVC0211==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING INPUT-IVVS0211.
      *----------------------------------------------------------------*

           PERFORM A000-OPERAZIONI-INIZIALI THRU A000-EX.

           IF IVVC0211-SUCCESSFUL-RC
              PERFORM A100-ELABORAZIONE     THRU A100-EX
           END-IF.

           PERFORM A900-OPERAZIONI-FINALI   THRU A900-EX.

           GOBACK.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
       A000-OPERAZIONI-INIZIALI.

           PERFORM A001-INITIALIZE         THRU A001-EX.
           PERFORM A002-SET                THRU A002-EX.
           PERFORM A003-ZERO               THRU A003-EX.
           PERFORM A004-SPACE              THRU A004-EX.

           PERFORM A005-CTRL-DATI-INPUT    THRU A005-EX.

           IF IVVC0211-SUCCESSFUL-RC
              PERFORM I000-VALORIZZA-IDSV0003 THRU I000-EX
           END-IF.

       A000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    INITIALIZE
      *----------------------------------------------------------------*
       A001-INITIALIZE.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'IVVS0211'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Iniz. area di working'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           INITIALIZE                      IX-INDICI
      *                                    AREA-BUSINESS
                                           WADE-AREA-ADESIONE
                                           WBEP-AREA-BENEF
                                           WBEL-AREA-BENEF-LIQ
                                           WDCO-AREA-DT-COLLETTIVA
                                           WDFA-AREA-DT-FISC-ADES
                                           WDEQ-AREA-DETT-QUEST
                                           WDTC-AREA-DETT-TIT-CONT
                                           WGRZ-AREA-GARANZIA
                                           WGRL-AREA-GARANZIA-LIQ
                                           WISO-AREA-IMPOSTA-SOST
                                           WLQU-AREA-LIQUIDAZIONE
                                           WMOV-AREA-MOVIMENTO
                                           WMFZ-AREA-MOVI-FINRIO
                                           WPOG-AREA-PARAM-OGG
                                           WPMO-AREA-PARAM-MOV
                                           WPLI-AREA-PERC-LIQ
                                           WPOL-AREA-POLIZZA
                                           WPRE-AREA-PRESTITI
                                           WPVT-AREA-PROV
                                           WQUE-AREA-QUEST
                                           WRIC-AREA-RICH
                                           WRDF-AREA-RICH-DISINV-FND
                                           WRIF-AREA-RICH-INV-FND
                                           WRDF-ELE-RIC-INV-MAX
                                           WRIF-ELE-RIC-INV-MAX
                                           WRAN-AREA-RAPP-ANAG
                                           WE15-AREA-EST-RAPP-ANAG
                                           WRRE-AREA-RAPP-RETE
                                           WSPG-AREA-SOPRAP-GAR
                                           WSDI-AREA-STRA-INV
                                           WTIT-AREA-TIT-CONT
                                           WTCL-AREA-TIT-LIQ
                                           WTGA-AREA-TRANCHE
                                           WDAD-AREA-DEFAULT-ADES
                                           WOCO-AREA-OGG-COLL
                                           WDFL-AREA-DFL
                                           WRST-AREA-RST
                                           WL19-AREA-QUOTE
                                           WL19-ELE-FND-MAX
                                           WL30-AREA-REINVST-POLI
                                           WL23-AREA-VINC-PEG
                                           WOPZ-AREA-OPZIONI
                                           WGOP-AREA-GARANZIA-OPZ
                                           WTOP-AREA-TRANCHE-OPZ
                                           WTLI-AREA-TRCH-LIQ
                                           WCNT-AREA-DATI-CONTEST
                                           WCLT-AREA-CLAU-TXT
                                           WDEQ-ELE-DETT-QUEST-MAX
                                           WPOG-ELE-PARAM-OGG-MAX
                                           WPMO-ELE-PARAM-MOV-MAX
                                           WTGA-ELE-TRAN-MAX
                                           WTLI-ELE-TRCH-LIQ-MAX
                                           WOCO-ELE-OGG-COLL-MAX
NEW                                        WP58-AREA-IMPOSTA-BOLLO
NEW01                                      WP61-AREA-D-CRIST
                                           WP67-AREA-EST-POLI-CPI-PR
                                           WP01-AREA-RICH-EST
                                           DISTINCT-STR
                                           GAR
                                           TRCH-DI-GAR
                                           MATR-VAL-VAR
                                           VAR-FUNZ-DI-CALC
      *                                    AREA-IVVC0216
                                           AREA-WARNING-IVVC0216
                                           LCCV0021
                                           IVVC0222-AREA-FND-X-TRANCHE
12969                                      AREA-IVVC0223
16324                                      WCDG-AREA-COMMIS-GEST-VV.

      *
      *    Inizializzazione  STR-DIST-DT-VLDT-PROD-TGA
      *
           INITIALIZE  DIST-TAB-DT-VLDT-PROD(1).

           MOVE STR-DIST-DT-VLDT-PROD-TGA
             TO STR-RES-DIST-DT-VLDT-PROD-TGA.

      *--  INIZIALIZZAZIONE AREA DETTAGLIO QUESTIONARIO

           INITIALIZE             WDEQ-TAB-DETT-QUEST(1).

           MOVE  WDEQ-TAB         TO WDEQ-RESTO-TAB.

      *--  INIZIALIZZAZIONE AREA PARAMETRO OGGETTO

           INITIALIZE             WPOG-TAB-PARAM-OGG(1).

           MOVE  WPOG-TAB         TO WPOG-RESTO-TAB.

      *--  INIZIALIZZAZIONE AREA PARAMETRO MOVIMENTO

           INITIALIZE             WPMO-TAB-PARAM-MOV(1).

           MOVE  WPMO-TAB         TO WPMO-RESTO-TAB.

      *--  INIZIALIZZAZIONE AREA TRANCHE DI GARANZIA

           INITIALIZE             WTGA-TAB-TRAN(1).

           MOVE  WTGA-TAB         TO WTGA-RESTO-TAB.

      *-- INIZIALIZZAZIONE AREA TRANCHE GARANZIA DI LIQUIDAZIONE

           INITIALIZE             WTLI-TAB-TRCH-LIQ(1).

           MOVE  WTLI-TAB         TO WTLI-RESTO-TAB.

      *-- INIZIALIZZAZIONE AREA OGGETTO COLLEGATO

           INITIALIZE             WOCO-TAB-OGG-COLL(1).

           MOVE  WOCO-TAB         TO WOCO-RESTO-TAB.

      *--  AREA RICHIESTA DISINVESTIMENTO FONDO

           INITIALIZE             WRDF-TAB-RIC-DISINV(1).

           MOVE  WRDF-TABELLA     TO WRDF-RESTO-TABELLA.

      *--  AREA RICHIESTA INVESTIMENTO FONDO

           INITIALIZE             WRIF-TAB-RIC-INV(1).

           MOVE  WRIF-TABELLA     TO WRIF-RESTO-TABELLA.

      *-- INIZIALIZZAZIONE AREA QUOTAZ FONDI UNIT

           INITIALIZE             WL19-TAB-FND(1).

           MOVE  WL19-TABELLA     TO WL19-RESTO-TABELLA.

      *--  INIZIALIZZAZIONE AREA WORK SCHEDA VARIABILI

           INITIALIZE             IVVC0216-ELE-LIVELLO-MAX-P
                                  IVVC0216-DEE
                                  IVVC0216-ID-POL-P            (1)
                                  IVVC0216-COD-TIPO-OPZIONE-P  (1)
                                  IVVC0216-TP-LIVELLO-P        (1)
                                  IVVC0216-COD-LIVELLO-P       (1)
                                  IVVC0216-ID-LIVELLO-P        (1)
                                  IVVC0216-DT-INIZ-PROD-P      (1)
                                  IVVC0216-COD-RGM-FISC-P      (1)
                                  IVVC0216-NOME-SERVIZIO-P     (1)
                                  IVVC0216-ELE-VARIABILI-MAX-P (1)
                                  IVVC0216-AREA-VARIABILI-P    (1)
                                  IVVC0216-VAR-AUT-OPER.

           MOVE IVVC0216-TAB-VAL-P TO IVVC0216-RESTO-TAB-VAL-P.

           INITIALIZE             IVVC0216-ELE-LIVELLO-MAX-T
                                  IVVC0216-ID-GAR-T            (1)
                                  IVVC0216-COD-TIPO-OPZIONE-T  (1)
                                  IVVC0216-TP-LIVELLO-T        (1)
                                  IVVC0216-COD-LIVELLO-T       (1)
                                  IVVC0216-ID-LIVELLO-T        (1)
                                  IVVC0216-DT-DECOR-TRCH-T     (1)
                                  IVVC0216-DT-INIZ-TARI-T      (1)
                                  IVVC0216-COD-RGM-FISC-T      (1)
                                  IVVC0216-NOME-SERVIZIO-T     (1)
                                  IVVC0216-ELE-VARIABILI-MAX-T (1)
                                  IVVC0216-AREA-VARIABILI-T    (1)

            MOVE IVVC0216-TAB-VAL-T TO IVVC0216-RESTO-TAB-VAL-T.

      *--  INIZIALIZZAZIONE AREA VARIABILI

           INITIALIZE             C216-ELE-LIVELLO-MAX-P
                                  C216-DEE
                                  C216-ID-POL-P            (1)
                                  C216-COD-TIPO-OPZIONE-P  (1)
                                  C216-TP-LIVELLO-P        (1)
                                  C216-COD-LIVELLO-P       (1)
                                  C216-ID-LIVELLO-P        (1)
                                  C216-DT-INIZ-PROD-P      (1)
                                  C216-COD-RGM-FISC-P      (1)
                                  C216-NOME-SERVIZIO-P     (1)
                                  C216-ELE-VARIABILI-MAX-P (1)
                                  C216-AREA-VARIABILI-P    (1)
                                  C216-VAR-AUT-OPER.

           MOVE C216-TAB-VAL-P TO C216-RESTO-TAB-VAL-P.

           INITIALIZE             C216-ELE-LIVELLO-MAX-T
                                  C216-ID-GAR-T            (1)
                                  C216-COD-TIPO-OPZIONE-T  (1)
                                  C216-TP-LIVELLO-T        (1)
                                  C216-COD-LIVELLO-T       (1)
                                  C216-ID-LIVELLO-T        (1)
                                  C216-DT-DECOR-TRCH-T     (1)
                                  C216-DT-INIZ-TARI-T      (1)
                                  C216-COD-RGM-FISC-T      (1)
                                  C216-NOME-SERVIZIO-T     (1)
                                  C216-ELE-VARIABILI-MAX-T (1)
                                  C216-AREA-VARIABILI-T    (1)

            MOVE C216-TAB-VAL-T TO C216-RESTO-TAB-VAL-T.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'IVVS0211'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Iniz. area di working'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       A001-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    SETTAGGIO FLAGS
      *----------------------------------------------------------------*
       A002-SET.

           SET IVVC0211-SUCCESSFUL-RC      TO TRUE.

           SET TIPO-STANDARD               TO TRUE.
           SET SKEDE-TOTALI                TO TRUE.
           SET GARANZIA-INIT               TO TRUE.
           SET TRANCHE-INIT                TO TRUE.

           SET FINE-ELAB-NO                TO TRUE.
           SET STAT-CAUS-TROVATI-NO        TO TRUE.
NEWP       SET WK-VAR-FUNZ-TROVATA-NO      TO TRUE.

       A002-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    SETTAGGIO ZERO
      *----------------------------------------------------------------*
       A003-ZERO.

           CONTINUE.

       A003-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    SETTAGGIO SPACES
      *----------------------------------------------------------------*
       A004-SPACE.

           CONTINUE.

       A004-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLI DATI INPUT
      *----------------------------------------------------------------*
       A005-CTRL-DATI-INPUT.

      *--> CONTROLLO DELLA FUNZIONALITA'
           IF  IVVC0211-CODICE-INIZIATIVA > SPACES
           AND IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE
           AND IVVC0211-CODICE-TRATTATO   > SPACES
           AND IVVC0211-CODICE-TRATTATO   NOT = HIGH-VALUE
               SET IVVC0211-FUNZ-NON-DEFINITA TO TRUE
               MOVE WK-PGM          TO IVVC0211-COD-SERVIZIO-BE
               STRING 'FUNZIONALITA'' NON DEFINITA'
                   DELIMITED BY SIZE INTO
                   IVVC0211-DESCRIZ-ERR
               END-STRING
           END-IF.

      *--> CONTROLLO DELLO STEP ELABORAZIONE IN CASO DI ACCESSO X RIASS
           IF  IVVC0211-CODICE-TRATTATO   > SPACES
           AND IVVC0211-CODICE-TRATTATO   NOT = HIGH-VALUE
           AND NOT IVVC0211-IN-CONV
               SET IVVC0211-STEP-ELAB-NOT-VALID TO TRUE
               MOVE WK-PGM          TO IVVC0211-COD-SERVIZIO-BE
               STRING 'STEP ELABORAZIONE NON VALIDO'
                   DELIMITED BY SIZE INTO
                   IVVC0211-DESCRIZ-ERR
               END-STRING
           END-IF.

      *--> CONTROLLO DELLO STEP ELABORAZIONE IN CASO DI ACCESSO X
      *    INIZIATIVA COMMERCIALE (DA DEFINIRE)
           IF  IVVC0211-CODICE-INIZIATIVA   > SPACES
            AND IVVC0211-CODICE-INIZIATIVA   NOT = HIGH-VALUE
            AND IVVC0211-STEP-ELAB NOT = '3' AND '4' AND '5' AND '6'
                                     AND '7'
                SET IVVC0211-STEP-ELAB-NOT-VALID TO TRUE
                MOVE WK-PGM          TO IVVC0211-COD-SERVIZIO-BE
                STRING 'STEP ELABORAZIONE NON VALIDO'
                    DELIMITED BY SIZE INTO
                    IVVC0211-DESCRIZ-ERR
                END-STRING
            END-IF.

      *--> CONTROLLO DELLO STEP ELABORAZIONE IN CASO DI ACCESSO X
      *    PRODOTTO
           IF (IVVC0211-CODICE-INIZIATIVA > SPACES
           AND IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE)
           OR (IVVC0211-CODICE-TRATTATO   > SPACES
           AND IVVC0211-CODICE-TRATTATO   NOT = HIGH-VALUE)
               NEXT SENTENCE
           ELSE
              IF  NOT IVVC0211-PRE-CONV
              AND NOT IVVC0211-POST-CONV
              AND NOT IVVC0211-IN-CONV
      *--> DATI INPUT ERRATI
                  SET IVVC0211-STEP-ELAB-NOT-VALID TO TRUE
                  MOVE WK-PGM          TO IVVC0211-COD-SERVIZIO-BE
                  STRING 'STEP ELABORAZIONE NON VALIDO'
                      DELIMITED BY SIZE INTO
                      IVVC0211-DESCRIZ-ERR
                  END-STRING
              END-IF
           END-IF.

           IF IVVC0211-SUCCESSFUL-RC
      *--> CONTROLLO DEL FLAG AREA
              IF  NOT IVVC0211-AREA-VE
              AND NOT IVVC0211-AREA-PV
                  SET IVVC0211-FLAG-AREA-NOT-VALID  TO TRUE
                  MOVE WK-PGM       TO IVVC0211-COD-SERVIZIO-BE
                  STRING 'FLAG-AREA NON VALIDO'
                         DELIMITED BY SIZE INTO
                         IVVC0211-DESCRIZ-ERR
                  END-STRING
              END-IF

              IF IVVC0211-SUCCESSFUL-RC
      *--> CONTROLLO TIPO MOVIMENTO
                 IF  IVVC0211-TIPO-MOVIMENTO NOT NUMERIC
                  OR IVVC0211-TIPO-MOVIMENTO = ZEROES
                     SET IVVC0211-TIPO-MOVIM-NOT-VALID  TO TRUE
                     MOVE WK-PGM    TO IVVC0211-COD-SERVIZIO-BE

                     STRING 'TIPO MOVIMENTO NON VALIDO'
                            DELIMITED BY SIZE INTO
                            IVVC0211-DESCRIZ-ERR
                     END-STRING
                 END-IF

                 IF IVVC0211-SUCCESSFUL-RC

                    PERFORM V000-VERIFICA-OPZ-POL-GAR THRU V000-EX

                 END-IF
              END-IF
           END-IF.

       A005-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DELLA COPY IVVC0214
      *----------------------------------------------------------------*
       A010-VALORIZZA-DCLGEN.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-ADES
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WADE-AREA-ADESIONE
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-BENEF
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WBEP-AREA-BENEF
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-BENEF-LIQ
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WBEL-AREA-BENEF-LIQ
           END-IF.


           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-DT-COLL
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WDCO-AREA-DT-COLLETTIVA
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-DT-FISC-ADES
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WDFA-AREA-DT-FISC-ADES
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-DETT-QUEST
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WDEQ-AREA-DETT-QUEST
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-DETT-TIT-CONT
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WDTC-AREA-DETT-TIT-CONT
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-GARANZIA
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WGRZ-AREA-GARANZIA
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-GARANZIA-OPZ
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WGOP-AREA-GARANZIA-OPZ
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-GAR-LIQ
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WGRL-AREA-GARANZIA-LIQ
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-IMPOSTA-SOST
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WISO-AREA-IMPOSTA-SOST
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-LIQUIDAZ
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WLQU-AREA-LIQUIDAZIONE
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-MOVIMENTO
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WMOV-AREA-MOVIMENTO
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-MOVI-FINRIO
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WMFZ-AREA-MOVI-FINRIO
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-PARAM-OGG
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WPOG-AREA-PARAM-OGG
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-PARAM-MOV
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WPMO-AREA-PARAM-MOV
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-PERC-LIQ
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WPLI-AREA-PERC-LIQ
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-POLI
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WPOL-AREA-POLIZZA
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-PRESTITI
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WPRE-AREA-PRESTITI
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-PROVV-TRAN
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WPVT-AREA-PROV
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-QUEST
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WQUE-AREA-QUEST
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-RICH
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WRIC-AREA-RICH
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-RICH-DISINV-FND
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WRDF-AREA-RICH-DISINV-FND
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-RICH-INV-FND
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WRIF-AREA-RICH-INV-FND
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-RAPP-ANAG
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WRAN-AREA-RAPP-ANAG
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-EST-RAPP-ANAG
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WE15-AREA-EST-RAPP-ANAG
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-RAPP-RETE
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WRRE-AREA-RAPP-RETE
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-SOPRAP-GAR
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WSPG-AREA-SOPRAP-GAR
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-STRA-INV
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WSDI-AREA-STRA-INV
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-TIT-CONT
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WTIT-AREA-TIT-CONT
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-TIT-LIQ
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WTCL-AREA-TIT-LIQ
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-TRCH-GAR
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WTGA-AREA-TRANCHE
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-TRCH-GAR-OPZ
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WTOP-AREA-TRANCHE-OPZ
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-TRCH-LIQ
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WTLI-AREA-TRCH-LIQ
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-DFLT-ADES
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WDAD-AREA-DEFAULT-ADES
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-OGG-COLL
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WOCO-AREA-OGG-COLL
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-DT-FORZ-LIQ
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WDFL-AREA-DFL
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-OPZIONI
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WOPZ-AREA-OPZIONI
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-DATI-CONTEST
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WCNT-AREA-DATI-CONTEST
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-RIS-DI-TRANCHE
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WRST-AREA-RST
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-QOTAZ-FON
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WL19-AREA-QUOTE
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-REINVST-POLI
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WL30-AREA-REINVST-POLI
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-VINC-PEGN
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WL23-AREA-VINC-PEG
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-CLAU-TEST
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WCLT-AREA-CLAU-TXT
           END-IF.

NEW        IF IVVC0211-TAB-ALIAS(IND-STR) =
NEW           IVVC0218-ALIAS-IMPOSTA-BOLLO
NEW           MOVE IVVC0211-BUFFER-DATI
NEW               (IVVC0211-POSIZ-INI(IND-STR) :
NEW                IVVC0211-LUNGHEZZA(IND-STR))
NEW             TO WP58-AREA-IMPOSTA-BOLLO
NEW        END-IF.

NEW01      IF IVVC0211-TAB-ALIAS(IND-STR) =
NEW01         IVVC0218-ALIAS-DATI-CRIST
NEW01         MOVE IVVC0211-BUFFER-DATI
NEW01             (IVVC0211-POSIZ-INI(IND-STR) :
NEW01              IVVC0211-LUNGHEZZA(IND-STR))
NEW01          TO WP61-AREA-D-CRIST
NEW01      END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-EST-POLI-CPI-PR
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
               TO WP67-AREA-EST-POLI-CPI-PR
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-RICH-EST
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
               TO WP01-AREA-RICH-EST
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-AREA-FND-X-TRCH
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO IVVC0222-AREA-FND-X-TRANCHE
           END-IF.

12969      IF IVVC0211-TAB-ALIAS(IND-STR) =
12969         IVVC0218-ALIAS-AREA-VAR-X-GAR
12969         MOVE IVVC0211-BUFFER-DATI
12969             (IVVC0211-POSIZ-INI(IND-STR) :
12969              IVVC0211-LUNGHEZZA(IND-STR))
12969           TO AREA-IVVC0223
12969      END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-ATT-SERV-VAL
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WP88-AREA-SERV-VAL
           END-IF.

           IF IVVC0211-TAB-ALIAS(IND-STR) =
              IVVC0218-ALIAS-D-ATT-SERV-VAL
              MOVE IVVC0211-BUFFER-DATI
                  (IVVC0211-POSIZ-INI(IND-STR) :
                   IVVC0211-LUNGHEZZA(IND-STR))
                TO WP89-AREA-DSERV-VAL
           END-IF.

13385      IF IVVC0211-TAB-ALIAS(IND-STR) =
13385         IVVC0218-ALIAS-QUEST-ADEG-VER
13385         MOVE IVVC0211-BUFFER-DATI
13385             (IVVC0211-POSIZ-INI(IND-STR) :
13385              IVVC0211-LUNGHEZZA(IND-STR))
13385           TO WP56-AREA-QUEST-ADEG-VER
13385      END-IF.

16324      IF WPOL-FL-POLI-IFP NOT = 'P'
16324         IF IVVC0211-TAB-ALIAS(IND-STR) =
16324            IVVC0218-ALIAS-AREA-FND-X-CDG
16324            MOVE IVVC0211-BUFFER-DATI
16324                (IVVC0211-POSIZ-INI(IND-STR) :
16324                 IVVC0211-LUNGHEZZA(IND-STR))
16324              TO WCDG-AREA-COMMIS-GEST-VV
16324         END-IF
16324      END-IF.

       A010-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       A100-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DELLA COPY IVVC0214

           PERFORM A010-VALORIZZA-DCLGEN THRU A010-EX
                   VARYING IND-STR FROM 1 BY 1
                     UNTIL IND-STR > IVVC0211-ELE-INFO-MAX OR
                           IVVC0211-TAB-ALIAS(IND-STR) =
                      SPACES OR LOW-VALUE OR HIGH-VALUE

      *--> LA TRASCODIFICA DEL MOVIMENTO DA PTF A ACT DEVE ESSERE
      *--> EFFETTUATA SOLO SE CI TROVIAMO IN CONVERSAZIONE

           IF IVVC0211-IN-CONV
              PERFORM T000-TP-MOVI-PTF-TO-ACT  THRU T000-EX
           END-IF.

           IF IVVC0211-SUCCESSFUL-RC
              PERFORM E000-ESTRAI-STAT-CAUS    THRU E000-EX

              IF IVVC0211-SUCCESSFUL-RC
                 PERFORM L000-GESTIONE-LIVELLI THRU L000-EX
              END-IF
           END-IF

           IF IVVC0211-SUCCESSFUL-RC

              IF IVVC0211-FLAG-GAR-OPZIONE-SI
                 SET TIPO-OPZIONI                  TO TRUE
                 PERFORM G000-GESTIONE-OPZIONI     THRU G000-EX
              END-IF

              IF IVVC0211-SUCCESSFUL-RC

                 IF IVVC0211-POST-CONV
                    PERFORM P000-ESTRAI-AUT-OPER   THRU P000-EX
                 END-IF

                 IF IVVC0211-SUCCESSFUL-RC
                    PERFORM P100-ELABORA-VARIABILI THRU P100-EX
                 END-IF

              END-IF
           END-IF.

       A100-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE DATA INIZIO VALIDITA' TARIFFA DI TRANCHE
      *----------------------------------------------------------------*
       C000-GESTIONE-DT-VLDT-PROD.

           PERFORM C050-CNTL-DT-VLDT-PROD THRU C050-EX.

       C000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICA LA VALIDITA' DEL CAMPO WTGA-DT-VLDT-PROD(IND-TGA)
      *----------------------------------------------------------------*
       C050-CNTL-DT-VLDT-PROD.
           IF WTGA-DT-VLDT-PROD (IND-TGA) IS NUMERIC AND
              WTGA-DT-VLDT-PROD (IND-TGA) NOT ZERO

              IF GESTIONE-PRODOTTO
                 PERFORM C060-DISTINCT-DT-VLDT-PROD-TGA THRU C060-EX
              ELSE
                 SET DT-VLDT-PROD-VALIDA-SI           TO TRUE
              END-IF

              IF DT-VLDT-PROD-VALIDA-SI
                 PERFORM M000-GESTIONE-VAR-FUN-CALC     THRU M000-EX
              END-IF

           ELSE
              SET IVVC0211-DT-VLDT-PROD-NOT-V        TO TRUE
              MOVE WK-PGM          TO IVVC0211-COD-SERVIZIO-BE
              STRING 'DATA VALIDITA'' PRODOTTO DI TRANCHE NON VALIDA'
                  DELIMITED BY SIZE INTO
                  IVVC0211-DESCRIZ-ERR
              END-STRING
           END-IF.

       C050-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ESEGUE DISTINCT SULLA DATA VALIDITA' PRODOTTO DELLA TGA
      *----------------------------------------------------------------*
       C060-DISTINCT-DT-VLDT-PROD-TGA.

           SET DIST-SEARCH-IND                   TO 1
           SEARCH  DIST-TAB-INFO
                   AT END
                   SET DT-VLDT-PROD-VALIDA-SI    TO TRUE

                   ADD 1                         TO IND-TAB-DIST
                   MOVE WTGA-DT-VLDT-PROD (IND-TGA)
                        TO DIST-TAB-DT-VLDT-PROD (IND-TAB-DIST)

              WHEN DIST-TAB-DT-VLDT-PROD(DIST-SEARCH-IND) =
                       WTGA-DT-VLDT-PROD (IND-TGA)
                   SET DT-VLDT-PROD-VALIDA-NO    TO TRUE

           END-SEARCH.

       C060-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ESTRAZIONE STATO E CAUSALE
      *----------------------------------------------------------------*
       E000-ESTRAI-STAT-CAUS.

      *--> VALORIZZAZIONE DCLGEN
           MOVE IVVC0211-COD-COMPAGNIA-ANIA TO MVV-COD-COMPAGNIA-ANIA
           MOVE IVVC0211-TIPO-MOVIMENTO     TO MVV-TIPO-MOVIMENTO
           MOVE 'A'                         TO MVV-STEP-VALORIZZATORE
           MOVE IVVC0211-STEP-ELAB          TO MVV-STEP-CONVERSAZIONE


      *--> LIVELLO OPERAZIONE
           SET  IDSV0003-PRIMARY-KEY    TO TRUE

      *--> TIPO OPERAZIONE
           SET  IDSV0003-SELECT         TO TRUE

      *--> TRATTAMENTO-STORICITA
           SET IDSV0003-TRATT-SENZA-STOR TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE


           CALL PGM-LDBS1400           USING IDSV0003
                                             MATR-VAL-VAR

           IF IDSV0003-SUCCESSFUL-RC
               EVALUATE TRUE
                   WHEN IDSV0003-SUCCESSFUL-SQL
      *-->         OPERAZIONE ESEGUITA CORRETTAMENTE
                       SET STAT-CAUS-TROVATI-SI  TO TRUE

                       PERFORM E500-VALORIZZA-STAT-CAUS     THRU E500-EX

                       IF IVVC0211-SUCCESSFUL-RC
                          PERFORM S000-VALORIZZA-TIPO-SKEDE THRU S000-EX
                       END-IF

                   WHEN IDSV0003-NOT-FOUND
      *--->        CAMPO $ NON TROVATO
                      SET STAT-CAUS-TROVATI-NO   TO TRUE

                   WHEN IDSV0003-MORE-THAN-ONE-ROW
      *--->        ERRORE DI ACCESSO AL DB
                      SET FINE-ELAB-SI           TO TRUE
                      SET IVVC0211-SQL-ERROR     TO TRUE

                      MOVE PGM-LDBS1400
                                   TO IVVC0211-COD-SERVIZIO-BE

                      MOVE IDSV0003-SQLCODE TO WS-SQLCODE

                      STRING 'TROVATE PIU'' OCCORRENZE SU '
                             'MATR_VAL_VAR PER STATO E CAUSALE'
                             ' - '
                             'SQLCODE : '
                             WS-SQLCODE
                             DELIMITED BY SIZE INTO
                             IVVC0211-DESCRIZ-ERR
                      END-STRING

                   WHEN OTHER
      *--->        ERRORE DI ACCESSO AL DB
                      SET FINE-ELAB-SI           TO TRUE
                      SET IVVC0211-SQL-ERROR     TO TRUE

                      MOVE PGM-LDBS1400 TO IVVC0211-COD-SERVIZIO-BE

                      MOVE IDSV0003-SQLCODE TO WS-SQLCODE

                     STRING 'MATR_VAL_VAR'
                          ' - '
                          'RC : '
                         IDSV0003-RETURN-CODE
                          ' - '
                          'SQLCODE : '
                         WS-SQLCODE
                         DELIMITED BY SIZE INTO
                         IVVC0211-DESCRIZ-ERR
                     END-STRING

               END-EVALUATE
           ELSE
      *-->     GESTIRE ERRORE DISPATCHER
               SET IVVC0211-SQL-ERROR  TO TRUE

               MOVE PGM-LDBS1400       TO IVVC0211-COD-SERVIZIO-BE

               STRING PGM-LDBS1400
                      ' - '
                      'RC : '
                      IDSV0003-RETURN-CODE
                      DELIMITED BY SIZE INTO
                      IVVC0211-DESCRIZ-ERR
               END-STRING

           END-IF.

       E000-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA L'AREA VARIABILI STATO , CAUSALE
      *----------------------------------------------------------------*
       E999-CNTL-STAT-CAUS.

           IF STAT-CAUS-TROVATI-NO
              SET FINE-ELAB-SI              TO TRUE
              IF NOT IVVC0211-PRE-CONV            AND
                 NOT IVVC0211-POST-CONV           AND
IN.CO            NOT IVVC0211-PRE-CALC-INIZIATIVA AND
IN.CO            NOT IVVC0211-POST-CALC-INIZIATIVA
                 SET IVVC0211-SQL-ERROR     TO TRUE

                 MOVE PGM-LDBS1400
                           TO IVVC0211-COD-SERVIZIO-BE

                 STRING 'NON ESISTONO STATI E CAUSALI '
                        'PER ACCEDERE SUL DATABASE'
                      DELIMITED BY SIZE INTO
                      IVVC0211-DESCRIZ-ERR
                 END-STRING

                 MOVE 'MATR_VAL_VAR'
                         TO IVVC0211-NOME-TABELLA
              END-IF
           END-IF.

       E999-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA L'AREA VARIABILI STATO , CAUSALE
      *----------------------------------------------------------------*
       E500-VALORIZZA-STAT-CAUS.

           INITIALIZE                     COMODO-STAT-CAUS.

           MOVE MVV-OPER-LOG-STATO-BUS TO COMODO-OPER-LOG-STAT-BUS.
           MOVE MVV-OPER-LOG-CAUSALE   TO COMODO-OPER-LOG-CAUS.

           PERFORM E501-PREPARA-UNZIP-STAT     THRU E501-EX
           PERFORM U999-UNZIP-STRING           THRU U999-EX
           IF IVVC0211-SUCCESSFUL-RC
              PERFORM E502-CARICA-STAT-UNZIPPED   THRU E502-EX
           END-IF.

           IF IVVC0211-SUCCESSFUL-RC
              PERFORM E503-PREPARA-UNZIP-CAUS     THRU E503-EX
              PERFORM U999-UNZIP-STRING           THRU U999-EX
              IF IVVC0211-SUCCESSFUL-RC
                 PERFORM E504-CARICA-CAUS-UNZIPPED   THRU E504-EX
              END-IF
           END-IF.

       E500-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   PREPARA STRINGA DA UNZIPPARE
      *----------------------------------------------------------------*
       E501-PREPARA-UNZIP-STAT.

      *     INITIALIZE UNZIP-STRUCTURE

           INITIALIZE              UNZIP-LENGTH-STR-MAX
                                   UNZIP-LENGTH-FIELD
                                   UNZIP-STRING-ZIPPED
                                   UNZIP-IDENTIFICATORE
                                   UNZIP-ELE-VARIABILI-MAX
                                   UNZIP-COD-VARIABILE(1).

           MOVE UNZIP-TAB       TO  UNZIP-RESTO-TAB.

           MOVE LENGTH OF MVV-ARRAY-STATO-BUS TO UNZIP-LENGTH-STR-MAX.
           MOVE LENGTH OF COMODO-TP-STAT-BUS(1)
                                              TO UNZIP-LENGTH-FIELD
           MOVE MVV-ARRAY-STATO-BUS           TO UNZIP-STRING-ZIPPED.
           MOVE ','                           TO UNZIP-IDENTIFICATORE.

       E501-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CARICA CAMPI UNZIPPED
      *----------------------------------------------------------------*
       E502-CARICA-STAT-UNZIPPED.

           SET UNZIP-TROVATO-NO    TO TRUE
           PERFORM VARYING IND-UNZIP FROM 1 BY 1
                   UNTIL IND-UNZIP  >  LIMITE-VARIABILI-UNZIPPED OR
                         IND-UNZIP  >  UNZIP-ELE-VARIABILI-MAX   OR
                         UNZIP-COD-VARIABILE(IND-UNZIP) =
                         SPACES OR LOW-VALUE OR HIGH-VALUE

                   MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
                                           (1:UNZIP-LENGTH-FIELD)
                     TO COMODO-TP-STAT-BUS(IND-UNZIP)
                   SET UNZIP-TROVATO-SI    TO TRUE

           END-PERFORM.

      *--->  STATO NON TROVATO
           IF UNZIP-TROVATO-NO
              SET FINE-ELAB-SI           TO TRUE
              SET IVVC0211-GENERIC-ERROR TO TRUE

              MOVE WK-PGM                TO IVVC0211-COD-SERVIZIO-BE

              STRING 'STATO NON TROVATO SU MATR-VAL-VAR'
                     ' - '
                     'COMPAGNIA : '
                     WK-CODICE-COMPAGNIA
                     ' - '
                     'TIPO MOVIM. : '
                     WK-TIPO-MOVIMENTO
                     ' - '
                     'STEP VALOR. : '
                     MVV-STEP-VALORIZZATORE
                     ' - '
                     'STEP CONVER. : '
                     MVV-STEP-CONVERSAZIONE
                     DELIMITED BY SIZE INTO
                     IVVC0211-DESCRIZ-ERR
              END-STRING

              MOVE 'MATR_VAL_VAR'        TO IVVC0211-NOME-TABELLA
           END-IF.

       E502-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   PREPARA STRINGA DA UNZIPPARE
      *----------------------------------------------------------------*
       E503-PREPARA-UNZIP-CAUS.

      *     INITIALIZE UNZIP-STRUCTURE

           INITIALIZE              UNZIP-LENGTH-STR-MAX
                                   UNZIP-LENGTH-FIELD
                                   UNZIP-STRING-ZIPPED
                                   UNZIP-IDENTIFICATORE
                                   UNZIP-ELE-VARIABILI-MAX
                                   UNZIP-COD-VARIABILE(1).

           MOVE UNZIP-TAB       TO  UNZIP-RESTO-TAB.

           MOVE LENGTH OF MVV-ARRAY-CAUSALE   TO UNZIP-LENGTH-STR-MAX.
           MOVE LENGTH OF COMODO-TP-CAUS(1)
                                              TO UNZIP-LENGTH-FIELD
           MOVE MVV-ARRAY-CAUSALE             TO UNZIP-STRING-ZIPPED.
           MOVE ','                           TO UNZIP-IDENTIFICATORE.

       E503-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CARICA CAMPI UNZIPPED
      *----------------------------------------------------------------*
       E504-CARICA-CAUS-UNZIPPED.

           SET UNZIP-TROVATO-NO    TO TRUE
           PERFORM VARYING IND-UNZIP FROM 1 BY 1
                   UNTIL IND-UNZIP  >  LIMITE-VARIABILI-UNZIPPED OR
                         IND-UNZIP  >  UNZIP-ELE-VARIABILI-MAX   OR
                         UNZIP-COD-VARIABILE(IND-UNZIP) =
                         SPACES OR LOW-VALUE OR HIGH-VALUE

                   MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
                                           (1:UNZIP-LENGTH-FIELD)
                     TO COMODO-TP-CAUS(IND-UNZIP)
                   SET UNZIP-TROVATO-SI    TO TRUE

           END-PERFORM.

      *--->  STATO NON TROVATO
           IF UNZIP-TROVATO-NO
              SET FINE-ELAB-SI           TO TRUE
              SET IVVC0211-GENERIC-ERROR TO TRUE

              MOVE WK-PGM                TO IVVC0211-COD-SERVIZIO-BE

              STRING 'CAUSALE NON TROVATO SU MATR-VAL-VAR'
                     ' - '
                     'COMPAGNIA : '
                     WK-CODICE-COMPAGNIA
                     ' - '
                     'TIPO MOVIM. : '
                     WK-TIPO-MOVIMENTO
                     ' - '
                     'STEP VALOR. : '
                     MVV-STEP-VALORIZZATORE
                     ' - '
                     'STEP CONVER. : '
                     MVV-STEP-CONVERSAZIONE
                     DELIMITED BY SIZE INTO
                     IVVC0211-DESCRIZ-ERR
              END-STRING

              MOVE 'MATR_VAL_VAR'        TO IVVC0211-NOME-TABELLA
           END-IF.

       E504-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE OPZIONI
      *----------------------------------------------------------------*
       G000-GESTIONE-OPZIONI.

           MOVE 1                       TO IND-TIPO-OPZ
           PERFORM VARYING IND-TIPO-OPZ FROM 1 BY 1
                   UNTIL IND-TIPO-OPZ > LIMITE-TIPO-OPZ      OR
                         IND-TIPO-OPZ > WOPZ-ELE-MAX-OPZIONI OR
                         WOPZ-COD-TIPO-OPZIONE(IND-TIPO-OPZ) =
                         SPACES OR LOW-VALUE OR HIGH-VALUE   OR
                         NOT IVVC0211-SUCCESSFUL-RC

              PERFORM G100-GESTIONE-OPZIONI-PR       THRU G100-EX

              IF IVVC0211-SUCCESSFUL-RC
                 PERFORM G200-GESTIONE-OPZIONI-GA    THRU G200-EX
              END-IF

           END-PERFORM.

       G000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE OPZIONI
      *----------------------------------------------------------------*
       G100-GESTIONE-OPZIONI-PR.

           SET GESTIONE-PRODOTTO         TO TRUE

           PERFORM M000-GESTIONE-VAR-FUN-CALC  THRU M000-EX.

       G100-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE OPZIONI
      *----------------------------------------------------------------*
       G200-GESTIONE-OPZIONI-GA.

           SET GESTIONE-GARANZIE         TO TRUE

           MOVE 1                        TO IND-OPZ
           PERFORM VARYING IND-OPZ FROM 1 BY 1
             UNTIL IND-OPZ > WOPZ-NUM-GAR-OPZ-MAX-ELE(IND-TIPO-OPZ) OR
                   IND-OPZ > LIMITE-OPZ                             OR
                   WOPZ-GAR-OPZ-CODICE(IND-TIPO-OPZ, IND-OPZ) =
                     SPACES OR HIGH-VALUE OR LOW-VALUE              OR
                     FINE-ELAB-SI

                     PERFORM G500-ELABORA-OPZIONE  THRU G500-EX

           END-PERFORM.

       G200-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE OPZIONI
      *----------------------------------------------------------------*
       G500-ELABORA-OPZIONE.

           IF WOPZ-GAR-OPZ-VERSIONE(IND-TIPO-OPZ, IND-OPZ) NOT =
              LOW-VALUE OR HIGH-VALUE

              MOVE WOPZ-GAR-OPZ-VERSIONE(IND-TIPO-OPZ, IND-OPZ)
                                                  TO FLAG-VERSIONE

              PERFORM M000-GESTIONE-VAR-FUN-CALC  THRU M000-EX

           ELSE
              SET IVVC0211-VERS-OPZ-NOT-VALID     TO TRUE
              MOVE WK-PGM            TO IVVC0211-COD-SERVIZIO-BE

              STRING 'FLAG VERS. OPZ. NON VALIDO'
                    DELIMITED BY SIZE INTO
                    IVVC0211-DESCRIZ-ERR
              END-STRING
           END-IF.

       G500-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA CAMPI DI IDSV0003
      *----------------------------------------------------------------*
       I000-VALORIZZA-IDSV0003.

              MOVE IVVC0211-MODALITA-ESECUTIVA
                                    TO IDSV0003-MODALITA-ESECUTIVA
              MOVE IVVC0211-COD-COMPAGNIA-ANIA
                                    TO IDSV0003-CODICE-COMPAGNIA-ANIA
                                       WK-CODICE-COMPAGNIA
              MOVE IVVC0211-TIPO-MOVIMENTO
                                    TO IDSV0003-TIPO-MOVIMENTO
                                       WK-TIPO-MOVIMENTO

              IF IVVC0211-DATA-ULT-TIT-INC GREATER ZERO
                 MOVE IVVC0211-DATA-ULT-TIT-INC
                                    TO IDSV0003-DATA-INIZIO-EFFETTO
              ELSE
                 MOVE IVVC0211-DATA-EFFETTO
                                    TO IDSV0003-DATA-INIZIO-EFFETTO
              END-IF

              MOVE IVVC0211-DATA-COMPETENZA
                                    TO IDSV0003-DATA-COMPETENZA
              MOVE IVVC0211-DATA-COMP-AGG-STOR
                                    TO IDSV0003-DATA-COMP-AGG-STOR
              MOVE IVVC0211-TRATTAMENTO-STORICITA
                                    TO IDSV0003-TRATTAMENTO-STORICITA
              MOVE IVVC0211-FORMATO-DATA-DB
                                    TO IDSV0003-FORMATO-DATA-DB.
       I000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE CREAZIONE LIVELLI
      *----------------------------------------------------------------*
       L000-GESTIONE-LIVELLI.

           SET FLAG-TRATTATO-VALIDO-SI  TO TRUE
           SET RIASS-NO                         TO TRUE
           IF  IVVC0211-CODICE-TRATTATO > SPACES
           AND IVVC0211-CODICE-TRATTATO NOT = HIGH-VALUE
               SET RIASS-SI                     TO TRUE
           END-IF.

           SET  WK-GLOVARLIST-VUOTA  TO TRUE.

           MOVE WPOL-FL-VER-PROD   TO FLAG-VERSIONE

           PERFORM L200-CREAZIONE-PRODOTTO THRU L200-EX

           IF IVVC0211-SUCCESSFUL-RC
              PERFORM L100-CREAZIONE-GARANZIA THRU L100-EX
           END-IF.

           IF  RIASS-SI
           AND FLAG-TRATTATO-VALIDO-NO
               SET IVVC0211-TRATTATO-NOT-VALID TO TRUE
           END-IF.

       L000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE CREAZIONE LIVELLO GARANZIA
      *----------------------------------------------------------------*
       L100-CREAZIONE-GARANZIA.
           IF SKEDE-GARANZIE OR SKEDE-TOTALI
              SET GARANZIA-INIT                     TO TRUE
              PERFORM L300-GEST-GARANZIE-CONT       THRU L300-EX

              IF NOT GARANZIA-CONTESTO AND FINE-ELAB-NO
                 PERFORM N000-CERCA-ADESIONE         THRU N000-EX

                 IF IVVC0211-SUCCESSFUL-RC
                    PERFORM L440-LETTURA-GARANZIA-DB THRU L440-EX
                 END-IF

              END-IF

           END-IF.

       L100-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE CREAZIONE LIVELL PRODOTTO
      *----------------------------------------------------------------*
       L200-CREAZIONE-PRODOTTO.

           IF SKEDE-PRODOTTI OR SKEDE-TOTALI

              SET GESTIONE-PRODOTTO                 TO TRUE

              IF EMISSIONE OR IVVC0211-AREA-VE
                 PERFORM M000-GESTIONE-VAR-FUN-CALC THRU M000-EX
              ELSE
                 PERFORM L330-GESTIONE-PRODOTTO     THRU L330-EX
              END-IF

           END-IF.

       L200-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE GARANZIE DI CONTESTO
      *----------------------------------------------------------------*
       L300-GEST-GARANZIE-CONT.

           SET GESTIONE-GARANZIE          TO TRUE

           MOVE 1                         TO IND-GRZ
           PERFORM VARYING IND-GRZ FROM 1 BY 1
             UNTIL IND-GRZ > WGRZ-ELE-GARANZIA-MAX  OR
                   IND-GRZ > WK-GRZ-MAX-B             OR
                   FINE-ELAB-SI
                   IF  RIASS-SI
                       IF  WGRZ-COD-TRAT-RIASS(IND-GRZ)     > SPACES
                       AND WGRZ-COD-TRAT-RIASS(IND-GRZ) NOT = HIGH-VALUE
                           IF WGRZ-COD-TRAT-RIASS(IND-GRZ) NOT =
                              IVVC0211-CODICE-TRATTATO
                              SET FLAG-TRATTATO-VALIDO-NO  TO TRUE
                              SET FINE-ELAB-SI             TO TRUE
                           END-IF
                       ELSE
                           SET FLAG-TRATTATO-VALIDO-NO  TO TRUE
                           SET FINE-ELAB-SI             TO TRUE
                       END-IF
                   END-IF
                   IF FINE-ELAB-NO
                      SET GARANZIA-CONTESTO          TO TRUE
                      PERFORM L310-ELABORA-GARANZIA  THRU L310-EX
                   END-IF
           END-PERFORM.

       L300-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA GARANZIE
      *----------------------------------------------------------------*
       L310-ELABORA-GARANZIA.

           PERFORM L400-CERCA-TRANCHE         THRU L400-EX.

       L310-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE PRODOTTO
      *----------------------------------------------------------------*
       L330-GESTIONE-PRODOTTO.

           SET GESTIONE-PRODOTTO          TO TRUE

GIAL  *    IF WADE-ID-ADES IS NUMERIC
GIAL  *       IF WADE-ID-ADES NOT = ZEROES
                 PERFORM L400-CERCA-TRANCHE     THRU L400-EX.
GIAL  *       END-IF
GIAL  *    END-IF.

       L330-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CARICAMENTO DEI LIVELLI PRODOTTI NELLE OCCORRENZE
      *    DELL'AREA VARIABILI
      *----------------------------------------------------------------*
       L350-CARICA-LIVELLI.
           IF GESTIONE-PRODOTTO
              PERFORM L360-CARICA-LIVELLI-PROD THRU L360-EX
           ELSE
              PERFORM L370-CARICA-LIVELLI-GAR  THRU L370-EX
           END-IF.

       L350-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CARICAMENTO DEI LIVELLI PRODOTTI NELLE OCCORRENZE
      *    DELL'AREA VARIABILI
      *----------------------------------------------------------------*
       L355-CARICA-LIVELLI-RIASS.

           IF GESTIONE-PRODOTTO
              CONTINUE
           ELSE
              SUBTRACT   1          FROM IND-LIVELLO
              PERFORM L370-CARICA-LIVELLI-GAR  THRU L370-EX
           END-IF.

       L355-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CARICAMENTO DEI LIVELLI PRODOTTI NELLE OCCORRENZE
      *    DELL'AREA VARIABILI
      *----------------------------------------------------------------*
       L360-CARICA-LIVELLI-PROD.

      *    ADD 1                        TO IND-LIVELLO.

           MOVE WPOL-ID-POLI
             TO IVVC0216-ID-POL-P         (IND-LIVELLO).

           IF TIPO-OPZIONI
              MOVE WOPZ-COD-TIPO-OPZIONE     (IND-TIPO-OPZ)
                   TO IVVC0216-COD-TIPO-OPZIONE-P (IND-LIVELLO)
           END-IF

           PERFORM L390-VALORIZZA-TP-LIVELLO THRU L390-EX.


           MOVE WPOL-COD-PROD
             TO IVVC0216-COD-LIVELLO-P    (IND-LIVELLO)

           MOVE WPOL-DT-INI-VLDT-PROD
             TO IVVC0216-DT-INIZ-PROD-P   (IND-LIVELLO)

           MOVE WPOL-TP-RGM-FISC
             TO IVVC0216-COD-RGM-FISC-P   (IND-LIVELLO)

           MOVE ZEROES
             TO IVVC0216-ID-LIVELLO-P     (IND-LIVELLO).

       L360-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CARICAMENTO DEI LIVELLI GARANZIA NELLE OCCORRENZE
      *    DELL'AREA VARIABILI
      *----------------------------------------------------------------*
       L370-CARICA-LIVELLI-GAR.
           IF TIPO-OPZIONI
              PERFORM L380-CARICA-LIVELLI-OPZ-GAR THRU L380-EX

           ELSE
      *       ADD 1                               TO IND-LIVELLO
              MOVE ZEROES
                    TO IVVC0216-ID-LIVELLO-T     (IND-LIVELLO)

              EVALUATE TRUE
                 WHEN GARANZIA-CONTESTO
                     MOVE WGRZ-ID-GAR               (IND-GRZ)
                       TO IVVC0216-ID-GAR-T         (IND-LIVELLO)

                     MOVE WGRZ-COD-TARI             (IND-GRZ)
                       TO IVVC0216-COD-LIVELLO-T    (IND-LIVELLO)

                 WHEN GARANZIA-DB
                     MOVE GRZ-ID-GAR
                       TO IVVC0216-ID-GAR-T         (IND-LIVELLO)

                     MOVE GRZ-COD-TARI
                       TO IVVC0216-COD-LIVELLO-T    (IND-LIVELLO)

              END-EVALUATE

              EVALUATE TRUE
                 WHEN TRANCHE-CONTESTO
                     MOVE WTGA-ID-TRCH-DI-GAR       (IND-TGA)
                       TO IVVC0216-ID-LIVELLO-T     (IND-LIVELLO)

                     MOVE WTGA-DT-INI-VAL-TAR       (IND-TGA)
                       TO IVVC0216-DT-INIZ-TARI-T   (IND-LIVELLO)

                     MOVE WTGA-TP-RGM-FISC          (IND-TGA)
                       TO IVVC0216-COD-RGM-FISC-T   (IND-LIVELLO)

                     MOVE WTGA-DT-DECOR             (IND-TGA)
                       TO IVVC0216-DT-DECOR-TRCH-T
                                                    (IND-LIVELLO)
      * VERIFICHIAMO SE IL CAMPO SIA VALORIZZATO IN QUANTO IN FASE DI
      * CREAZIONE NUOVA TRANCHE ESEMPIO EMISSIONE NON ABBIAMO ANCORA
      * L'INFORMAZIONE
                     IF WTGA-TP-TRCH(IND-TGA) NOT =
                        HIGH-VALUE AND LOW-VALUE AND SPACES

                        COMPUTE IVVC0216-TIPO-TRCH(IND-LIVELLO) =
                                FUNCTION NUMVAL(WTGA-TP-TRCH(IND-TGA))

                        MOVE WTGA-TP-TRCH(IND-TGA)
                          TO WS-TP-TRCH

                        EVALUATE TRUE
                            WHEN TP-TRCH-NEG-PRCOS
                            WHEN TP-TRCH-NEG-RIS-PAR
                            WHEN TP-TRCH-NEG-RIS-PRO
                            WHEN TP-TRCH-NEG-IMPST-SOST
                            WHEN TP-TRCH-NEG-DA-DIS
                            WHEN TP-TRCH-NEG-INV-DA-SWIT
                                 SET IVVC0216-FLG-ITN-NEG(IND-LIVELLO)
                                  TO TRUE

                            WHEN OTHER
                                 SET IVVC0216-FLG-ITN-POS(IND-LIVELLO)
                                  TO TRUE

                        END-EVALUATE

                     END-IF

                 WHEN TRANCHE-DB

                     MOVE TGA-ID-TRCH-DI-GAR
                       TO IVVC0216-ID-LIVELLO-T     (IND-LIVELLO)

                     MOVE TGA-DT-INI-VAL-TAR TO IVVC0216-DT-INIZ-TARI-T
                                                    (IND-LIVELLO)

                     MOVE TGA-TP-RGM-FISC    TO IVVC0216-COD-RGM-FISC-T
                                                    (IND-LIVELLO)

                     MOVE TGA-DT-DECOR       TO IVVC0216-DT-DECOR-TRCH-T
                                                    (IND-LIVELLO)

      * VERIFICHIAMO SE IL CAMPO SIA VALORIZZATO IN QUANTO IN FASE DI
      * CREAZIONE NUOVA TRANCHE ESEMPIO EMISSIONE NON ABBIAMO ANCORA
      * L'INFORMAZIONE
                     IF TGA-TP-TRCH NOT =
                        HIGH-VALUE AND LOW-VALUE AND SPACES

                        COMPUTE IVVC0216-TIPO-TRCH(IND-LIVELLO) =
                                FUNCTION NUMVAL(TGA-TP-TRCH)

                        MOVE TGA-TP-TRCH
                          TO WS-TP-TRCH

                        EVALUATE TRUE
                            WHEN TP-TRCH-NEG-PRCOS
                            WHEN TP-TRCH-NEG-RIS-PAR
                            WHEN TP-TRCH-NEG-RIS-PRO
                            WHEN TP-TRCH-NEG-IMPST-SOST
                            WHEN TP-TRCH-NEG-DA-DIS
                            WHEN TP-TRCH-NEG-INV-DA-SWIT
                                 SET IVVC0216-FLG-ITN-NEG(IND-LIVELLO)
                                  TO TRUE

                            WHEN OTHER
                                 SET IVVC0216-FLG-ITN-POS(IND-LIVELLO)
                                  TO TRUE

                        END-EVALUATE

                     END-IF

              END-EVALUATE

           END-IF.

           PERFORM L390-VALORIZZA-TP-LIVELLO THRU L390-EX.


       L370-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CARICAMENTO DEI LIVELLI PRODOTTI NELLE OCCORRENZE
      *    DELL'AREA VARIABILI
      *----------------------------------------------------------------*
       L380-CARICA-LIVELLI-OPZ-GAR.

      *    ADD 1                        TO IND-LIVELLO

           MOVE WOPZ-COD-TIPO-OPZIONE     (IND-TIPO-OPZ)
             TO IVVC0216-COD-TIPO-OPZIONE-T (IND-LIVELLO)

           MOVE WOPZ-GAR-OPZ-CODICE       (IND-TIPO-OPZ, IND-OPZ)
             TO IVVC0216-COD-LIVELLO-T      (IND-LIVELLO)

           MOVE 1                         TO IND-GOP
           PERFORM VARYING IND-GOP FROM 1 BY 1
                   UNTIL IND-GOP > WK-GRZ-MAX-B             OR
                         IND-GOP > WTOP-ELE-TRAN-OPZ-MAX  OR
                         WGOP-ID-GAR(IND-GOP) NOT NUMERIC OR
                         WGOP-ID-GAR(IND-GOP) = ZEROES

                         IF WOPZ-GAR-OPZ-CODICE(IND-TIPO-OPZ, IND-OPZ)
                            =
                            WGOP-COD-TARI(IND-GOP)
                            MOVE WGOP-ID-GAR(IND-GOP)
                                 TO IVVC0216-ID-GAR-T    (IND-LIVELLO)
                            PERFORM L381-RICERCA-TOP-X-GOP THRU L381-EX
                         END-IF

           END-PERFORM.

       L380-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CARICAMENTO DEI LIVELLI PRODOTTI NELLE OCCORRENZE
      *    DELL'AREA VARIABILI
      *----------------------------------------------------------------*
       L381-RICERCA-TOP-X-GOP.

           MOVE 1                         TO IND-TOP
           PERFORM VARYING IND-TOP FROM 1 BY 1
                   UNTIL IND-TOP > WK-TGA-MAX-B                     OR
                         IND-TOP > WTOP-ELE-TRAN-OPZ-MAX          OR
                         WTOP-ID-TRCH-DI-GAR(IND-TOP) NOT NUMERIC OR
                         WTOP-ID-TRCH-DI-GAR(IND-TOP) = ZEROES

                        IF WGOP-ID-GAR(IND-GOP)
                           =
                           WTOP-ID-GAR(IND-TOP)
                           MOVE WTOP-ID-TRCH-DI-GAR      (IND-TOP)
                                TO IVVC0216-ID-LIVELLO-T  (IND-LIVELLO)

                           MOVE WTOP-DT-INI-VAL-TAR      (IND-TOP)
                                TO IVVC0216-DT-INIZ-TARI-T(IND-LIVELLO)

                           MOVE WTOP-TP-RGM-FISC         (IND-TOP)
                                TO IVVC0216-COD-RGM-FISC-T(IND-LIVELLO)

                           MOVE WTOP-DT-DECOR            (IND-TOP)
                                TO IVVC0216-DT-DECOR-TRCH-T(IND-LIVELLO)
                        END-IF

           END-PERFORM.

       L381-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CARICAMENTO DEI LIVELLI PRODOTTI NELLE OCCORRENZE
      *    DELL'AREA VARIABILI
      *----------------------------------------------------------------*
       L390-VALORIZZA-TP-LIVELLO.

           IF GESTIONE-PRODOTTO
              MOVE WS-LIV-PRODOTTO
                TO IVVC0216-TP-LIVELLO-P     (IND-LIVELLO)
           ELSE
              MOVE WS-LIV-GARANZIA
                TO IVVC0216-TP-LIVELLO-T     (IND-LIVELLO)
           END-IF.

       L390-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CERCA TRANCHE DI GARANZIA
      *----------------------------------------------------------------*
       L400-CERCA-TRANCHE.

           SET TRANCHE-INIT                   TO TRUE
           PERFORM L451-LETTURA-TRANCHE-CONT  THRU L451-EX
           IF NOT TRANCHE-CONTESTO AND FINE-ELAB-NO
              IF GESTIONE-GARANZIE AND WGRZ-ST-ADD(IND-GRZ)
                 PERFORM M000-GESTIONE-VAR-FUN-CALC THRU M000-EX
              ELSE
                  PERFORM N000-CERCA-ADESIONE THRU N000-EX
MIGCOL           IF IVVC0211-SUCCESSFUL-RC
MIGCOL              IF WPOL-TP-FRM-ASSVA = 'CO'
MIGCOL              AND (IVVC0211-TIPO-MOVIMENTO = 2031
MIGCOL                  OR IVVC0211-TIPO-MOVIMENTO = 2001
MIGCOL*                 OR IVVC0211-TIPO-MOVIMENTO = 2334
MIGCOL*                 OR IVVC0211-TIPO-MOVIMENTO = 2335
MIGCOL                  OR IVVC0211-TIPO-MOVIMENTO = 2339
MIGCOL                  OR IVVC0211-TIPO-MOVIMENTO = 2340
MIGCOL                  OR IVVC0211-TIPO-MOVIMENTO = 2006 )
MIGCOL                  CONTINUE
MIGCOL              ELSE
                    PERFORM L450-LETTURA-TRANCHE-DB THRU L450-EX
MIGCOL              END-IF
                 END-IF

              END-IF
           END-IF.

       L400-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA GARANZIA DA DB
      *----------------------------------------------------------------*
       L440-LETTURA-GARANZIA-DB.

           PERFORM E999-CNTL-STAT-CAUS THRU E999-EX

           IF FINE-ELAB-NO
      *--> valorizzazione
              PERFORM L441-VAL-WHERE-GRZ  THRU L441-EX

              MOVE LDBV1351               TO IDSV0003-BUFFER-WHERE-COND

      *--> SETTAGGI
JIRA8 *       MOVE IVVC0211-TRATTAMENTO-STORICITA
      *                          TO IDSV0003-TRATTAMENTO-STORICITA

              SET FINE-GARANZIE-NO        TO TRUE

              PERFORM UNTIL FINE-GARANZIE-SI OR
                            FINE-ELAB-SI
      *--> PER LA JIRA8 E STATA SPOSTATO IL SEL DEL TRATTAMENTO ALL
      *--> INTERNO DELLA PERFORM VARYING PER EVITARE SETTAGGGI
      *--> SPORCHI
      *--> SETTAGGI
JIRA8         MOVE IVVC0211-TRATTAMENTO-STORICITA
                                 TO IDSV0003-TRATTAMENTO-STORICITA

      *--> INIZIALIZZA CODICE DI RITORNO
              SET IDSV0003-SUCCESSFUL-RC     TO TRUE
              SET IDSV0003-SUCCESSFUL-SQL    TO TRUE
      *
              CALL PGM-LDBS1350 USING      IDSV0003
                                           GAR

               IF IDSV0003-SUCCESSFUL-RC
                EVALUATE TRUE
                    WHEN IDSV0003-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                         IF  RIASS-SI
                             IF  GRZ-COD-TRAT-RIASS     > SPACES
                             AND GRZ-COD-TRAT-RIASS NOT = HIGH-VALUE
                                 IF  GRZ-COD-TRAT-RIASS NOT =
                                     IVVC0211-CODICE-TRATTATO
                                     SET FLAG-TRATTATO-VALIDO-NO TO TRUE
                                     SET FINE-ELAB-SI            TO TRUE
                                 END-IF
                             ELSE
                                 SET FLAG-TRATTATO-VALIDO-NO TO TRUE
                                 SET FINE-ELAB-SI            TO TRUE
                             END-IF
                         END-IF
                         IF FINE-ELAB-NO
                            SET GARANZIA-DB                TO TRUE
                            PERFORM L310-ELABORA-GARANZIA  THRU L310-EX
                            SET IDSV0003-FETCH-NEXT   TO TRUE
                         END-IF
                    WHEN IDSV0003-NOT-FOUND
      *--->         CAMPO $ NON TROVATO
                         SET FINE-GARANZIE-SI       TO TRUE

                         IF IDSV0003-FETCH-FIRST
                             SET FINE-ELAB-SI       TO TRUE
                             SET IVVC0211-SQL-ERROR TO TRUE

                             MOVE PGM-LDBS1350
                                      TO IVVC0211-COD-SERVIZIO-BE

                             STRING 'GARANZIE NON TROVATE'
                                    ' - '
                                    'POLIZZA : '
                                    WK-ID-POLI
                                    ' - '
                                    'GARANZIA : '
                                    WK-ID-GAR
                                    DELIMITED BY SIZE INTO
                                    IVVC0211-DESCRIZ-ERR
                             END-STRING

                             MOVE 'GAR-STB' TO IVVC0211-NOME-TABELLA

                          END-IF
                       WHEN OTHER
      *--->            ERRORE DI ACCESSO AL DB
                           SET FINE-ELAB-SI       TO TRUE
                           SET IVVC0211-SQL-ERROR TO TRUE

                           MOVE PGM-LDBS1350
                                TO IVVC0211-COD-SERVIZIO-BE

                           MOVE IDSV0003-SQLCODE TO WS-SQLCODE

                           STRING PGM-LDBS1350
                                ' - '
                                'POLIZZA : '
                                WK-ID-POLI
                                ' - '
                                'GARANZIA : '
                                WK-ID-GAR
                                'RC : '
                               IDSV0003-RETURN-CODE
                                ' - '
                                'SQLCODE : '
                               WS-SQLCODE
                               DELIMITED BY SIZE INTO
                               IVVC0211-DESCRIZ-ERR
                           END-STRING
                  ELSE
      *-->           GESTIRE ERRORE DISPATCHER
                     SET FINE-ELAB-SI        TO TRUE
                     SET IVVC0211-SQL-ERROR  TO TRUE

                     MOVE PGM-LDBS1350
                          TO IVVC0211-COD-SERVIZIO-BE

                     STRING PGM-LDBS1350
                            ' - '
                            'RC : '
                            IDSV0003-RETURN-CODE
                            DELIMITED BY SIZE INTO
                            IVVC0211-DESCRIZ-ERR
                     END-STRING

                  END-IF
              END-PERFORM
           END-IF.

       L440-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA CAMPI WHERE CONDITION GARANZIA
      *----------------------------------------------------------------*
       L441-VAL-WHERE-GRZ.

           PERFORM L800-VALORIZZA-STAT-CAUS-GRZ THRU L800-EX

           IF IVVC0211-SUCCESSFUL-RC

              MOVE WADE-ID-ADES                TO GRZ-ID-ADES

              IF GESTIONE-PRODOTTO OR GESTIONE-GARANZIE
                 MOVE WPOL-ID-POLI             TO GRZ-ID-POLI
                                                   WK-ID-POLI
                 SET IDSV0003-FETCH-FIRST      TO TRUE
              END-IF

           END-IF.

       L441-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA TRANCHE DI GARANZIA DA DB
      *----------------------------------------------------------------*
       L450-LETTURA-TRANCHE-DB.

           PERFORM E999-CNTL-STAT-CAUS THRU E999-EX

           IF FINE-ELAB-NO
              SET IDSV0003-FETCH-FIRST                TO TRUE

              SET FINE-TRANCHE-NO            TO TRUE

      *----------------------------------------------------------------*
              PERFORM UNTIL FINE-TRANCHE-SI OR
                            FINE-ELAB-SI

      *-->    INIZIALIZZA CODICE DI RITORNO
                 SET IDSV0003-SUCCESSFUL-RC           TO TRUE
                 SET IDSV0003-SUCCESSFUL-SQL          TO TRUE

                 MOVE IVVC0211-TRATTAMENTO-STORICITA
                                  TO IDSV0003-TRATTAMENTO-STORICITA

      *-->    valorizzazione stati causali
                 PERFORM L900-VALORIZZA-STAT-CAUS-TGA THRU L900-EX

                 PERFORM L460-VALORIZZA-DCL-TGA       THRU L460-EX

                 MOVE LDBV1361       TO IDSV0003-BUFFER-WHERE-COND
      *
                  CALL PGM-LDBS1360 USING      IDSV0003
                                               TRCH-DI-GAR

                  IF IDSV0003-SUCCESSFUL-RC
                   EVALUATE TRUE
                       WHEN IDSV0003-SUCCESSFUL-SQL
      *-->             OPERAZIONE ESEGUITA CORRETTAMENTE
                         SET TRANCHE-DB               TO TRUE

                         PERFORM M000-GESTIONE-VAR-FUN-CALC
                                                      THRU M000-EX
                         SET IDSV0003-FETCH-NEXT      TO TRUE

                       WHEN IDSV0003-NOT-FOUND
      *--->            CAMPO $ NON TROVATO
                         SET FINE-TRANCHE-SI          TO TRUE

                         IF IDSV0003-FETCH-FIRST
                             SET FINE-ELAB-SI         TO TRUE
                             SET IVVC0211-SQL-ERROR   TO TRUE

                             MOVE PGM-LDBS1360
                                         TO IVVC0211-COD-SERVIZIO-BE
                             IF GESTIONE-GARANZIE
                                STRING 'TRANCHE NON TROVATA'
                                       ' PER GESTIONE GARANZIE'
                                       ' - '
                                       'POLIZZA : '
                                       WK-ID-POLI
                                       ' - '
                                       'GARANZIA : '
                                       WK-ID-GAR
                                       DELIMITED BY SIZE INTO
                                       IVVC0211-DESCRIZ-ERR
                                END-STRING
                             ELSE
                                STRING 'TRANCHE NON TROVATA'
                                       ' PER GESTIONE PRODOTTO'
                                       ' - '
                                       'POLIZZA : '
                                       WK-ID-POLI
                                       DELIMITED BY SIZE INTO
                                       IVVC0211-DESCRIZ-ERR
                                END-STRING
                             END-IF

                             MOVE 'TRCH_DI_GAR'
                                         TO IVVC0211-NOME-TABELLA
                          END-IF

                       WHEN OTHER
      *--->            ERRORE DI ACCESSO AL DB
                            SET FINE-ELAB-SI              TO TRUE
                            SET IVVC0211-SQL-ERROR        TO TRUE

                            MOVE PGM-LDBS1360
                                         TO IVVC0211-COD-SERVIZIO-BE

                            MOVE IDSV0003-SQLCODE TO WS-SQLCODE

                           STRING PGM-LDBS1360
                                ' - '
                                'POLIZZA : '
                                WK-ID-POLI
                                ' - '
                                'GARANZIA : '
                                WK-ID-GAR
                                ' - '
                                'RC : '
                               IDSV0003-RETURN-CODE
                                ' - '
                                'SQLCODE : '
                               WS-SQLCODE
                               DELIMITED BY SIZE INTO
                               IVVC0211-DESCRIZ-ERR
                           END-STRING
                  ELSE
      *-->           GESTIRE ERRORE DISPATCHER
                     SET FINE-ELAB-SI        TO TRUE
                     SET IVVC0211-SQL-ERROR  TO TRUE

                     MOVE PGM-LDBS1350       TO IVVC0211-COD-SERVIZIO-BE

                     STRING PGM-LDBS1360
                            ' - '
                            'RC : '
                            IDSV0003-RETURN-CODE
                            DELIMITED BY SIZE INTO
                            IVVC0211-DESCRIZ-ERR
                     END-STRING
                  END-IF
              END-PERFORM
           END-IF.

       L450-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA TRANCHE DI GARANZIA A CONTESTO
      *----------------------------------------------------------------*
       L451-LETTURA-TRANCHE-CONT.

      *
      *    Inizializzazione  STR-DIST-DT-VLDT-PROD-TGA
      *
           INITIALIZE  DIST-TAB-DT-VLDT-PROD(1).
           MOVE STR-DIST-DT-VLDT-PROD-TGA
             TO STR-RES-DIST-DT-VLDT-PROD-TGA.
           PERFORM VARYING IND-TGA FROM 1 BY 1
                   UNTIL IND-TGA >  WTGA-ELE-TRAN-MAX             OR
                         IND-TGA >  WK-TGA-MAX-C                    OR
                         NOT IVVC0211-SUCCESSFUL-RC

                         PERFORM L452-CNTL-TGA-X-GRZ THRU L452-EX

                         IF TRATTA-TRANCHE-SI
                            SET TRANCHE-CONTESTO       TO TRUE
                            PERFORM C000-GESTIONE-DT-VLDT-PROD
                                                       THRU C000-EX
                         END-IF
           END-PERFORM.

       L451-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLA TGA X GRZ
      *----------------------------------------------------------------*
       L452-CNTL-TGA-X-GRZ.
           SET TRATTA-TRANCHE-SI          TO TRUE
           IF GESTIONE-GARANZIE
              IF GARANZIA-CONTESTO
                 IF WTGA-ID-GAR(IND-TGA) NOT = WGRZ-ID-GAR(IND-GRZ)
                    SET TRATTA-TRANCHE-NO TO TRUE
                 END-IF
              ELSE
                 IF WTGA-ID-GAR(IND-TGA) NOT = GRZ-ID-GAR
                    SET TRATTA-TRANCHE-NO TO TRUE
                 END-IF
              END-IF

           END-IF.

       L452-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE DCLGEN TGA
      *----------------------------------------------------------------*
       L460-VALORIZZA-DCL-TGA.

           INITIALIZE TRCH-DI-GAR
           MOVE WPOL-ID-POLI               TO TGA-ID-POLI
                                               WK-ID-POLI

           MOVE WADE-ID-ADES               TO TGA-ID-ADES

           EVALUATE TRUE

              WHEN GESTIONE-PRODOTTO
                   SET LDBV1361-PRODOTTO           TO TRUE

              WHEN GESTIONE-GARANZIE
                   SET LDBV1361-GARANZIE           TO TRUE

                   IF GARANZIA-CONTESTO
                      MOVE WGRZ-ID-GAR(IND-GRZ)    TO TGA-ID-GAR
                                                       WK-ID-GAR
                   ELSE
                      MOVE  GRZ-ID-GAR             TO TGA-ID-GAR
                                                       WK-ID-GAR
                   END-IF

           END-EVALUATE.


       L460-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA STATI E CAUSALI DA COMODO X GRZ
      *----------------------------------------------------------------*
       L800-VALORIZZA-STAT-CAUS-GRZ.

           MOVE COMODO-OPER-LOG-STAT-BUS TO LDBV1351-OPER-LOG-STAT-BUS
           MOVE COMODO-STAT-BUS-TAB      TO LDBV1351-STAT-BUS-TAB.

           MOVE COMODO-OPER-LOG-CAUS     TO LDBV1351-OPER-LOG-CAUS
           MOVE COMODO-TP-CAUS-TAB       TO LDBV1351-TP-CAUS-TAB.

       L800-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA STATI E CAUSALI DA COMODO X TGA
      *----------------------------------------------------------------*
       L900-VALORIZZA-STAT-CAUS-TGA.

           INITIALIZE LDBV1361.

           MOVE COMODO-OPER-LOG-STAT-BUS TO LDBV1361-OPER-LOG-STAT-BUS
           MOVE COMODO-STAT-BUS-TAB      TO LDBV1361-STAT-BUS-TAB.

           MOVE COMODO-OPER-LOG-CAUS     TO LDBV1361-OPER-LOG-CAUS
           MOVE COMODO-TP-CAUS-TAB       TO LDBV1361-TP-CAUS-TAB.

           IF IVVC0211-DATA-ULT-TIT-INC GREATER ZERO
              MOVE COMODO-STAT-CAUS-LIQ  TO LDBV1361-TP-CAUS-TAB
           END-IF.

       L900-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE VARIABILI FUNZIONI DI CALCOLO
      *----------------------------------------------------------------*
       M000-GESTIONE-VAR-FUN-CALC.

      *--> VALORIZZAZIONE DCLGEN
           PERFORM M100-VAL-DCLGEN-VAR-FUNZ THRU M100-EX.

           IF IVVC0211-SUCCESSFUL-RC
      *     accesso x trattato RIASS
              IF RIASS-SI
                  MOVE ZEROES                     TO IND-UNZIP
                  MOVE WPOL-COD-PROD              TO VFC-CODPROD
                                                  WK-CODICE-PRODOTTO-VFC
                  PERFORM M020-ACCESSO-STANDARD     THRU M020-EX
                  IF IVVC0211-SUCCESSFUL-RC
                     MOVE IVVC0211-CODICE-TRATTATO TO AC5-CODPROD
                                                  WK-CODICE-PRODOTTO-VFC
                     PERFORM M010-ACCESSO-X-RIASS      THRU M010-EX
                  END-IF
              ELSE
                  PERFORM M020-ACCESSO-STANDARD     THRU M020-EX
              END-IF
           END-IF.

       M000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ACCESSO ALLA TABELLA VAR_FUNZ_DI_CALC_R
      *----------------------------------------------------------------*
       M010-ACCESSO-X-RIASS.

      *--> LIVELLO OPERAZIONE
              SET  IDSV0003-WHERE-CONDITION TO TRUE.

      *--> TIPO OPERAZIONE
              SET  IDSV0003-SELECT         TO TRUE

      *--> TRATTAMENTO-STORICITA
              SET IDSV0003-TRATT-SENZA-STOR TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE

              CALL PGM-LDBS8800           USING IDSV0003
                                                VAR-FUNZ-DI-CALC-R

              IF IDSV0003-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSV0003-SUCCESSFUL-SQL
      *-->         OPERAZIONE ESEGUITA CORRETTAMENTE

                          PERFORM M510-VALORIZZA-VARIABILI-RIASS
                             THRU M510-EX

                      WHEN IDSV0003-NOT-FOUND
      *--->        CAMPO $ NON TROVATO

                         IF NOT TIPO-OPZIONI
                            IF IVVC0211-IN-CONV
                               IF SKEDE-TOTALI AND GESTIONE-GARANZIE
                                  CONTINUE
                               ELSE
                                  SET FINE-ELAB-SI           TO TRUE
                                  SET IVVC0211-GENERIC-ERROR TO TRUE

                                  MOVE PGM-LDBS8800
                                         TO IVVC0211-COD-SERVIZIO-BE

                                  STRING 'OCCORRENZA NON TROVATA SU AC5'
                                         ' - '
                                         'COMP : '
                                         WK-CODICE-COMPAGNIA
                                         ' - '
                                         'PROD : '
                                         WK-CODICE-PRODOTTO-VFC
                                         ' - '
                                         'TARI : '
                                         WK-CODICE-TARIFFA-AC5
                                         ' -  '
                                         'DT INI : '
                                         WK-DATA-INIZIO-VFC
                                         ' -  '
                                         'FUNZ : '
                                         WK-NOME-FUNZIONE-VFC
                                         ' -  '
                                         'STEP ELAB. : '
                                         WK-STEP-ELAB-VFC
                                         DELIMITED BY SIZE INTO
                                         IVVC0211-DESCRIZ-ERR
                                  END-STRING

                                  MOVE 'VAR_FUNZ_DI_CALC_R'
                                               TO IVVC0211-NOME-TABELLA
                               END-IF
                            END-IF
                         END-IF

                      WHEN OTHER
      *--->        ERRORE DI ACCESSO AL DB
                         SET FINE-ELAB-SI           TO TRUE
                         SET IVVC0211-SQL-ERROR     TO TRUE

                         MOVE PGM-LDBS8800
                                      TO IVVC0211-COD-SERVIZIO-BE

                         MOVE IDSV0003-SQLCODE TO WS-SQLCODE

                        STRING PGM-LDBS8800
                                 ' - '
                                 'COMP : '
                                 WK-CODICE-COMPAGNIA
                                 ' - '
                                 'PROD : '
                                 WK-CODICE-PRODOTTO-VFC
                                 ' -  '
                                 'TARI : '
                                 WK-CODICE-TARIFFA-AC5
                                 ' -  '
                                 'DT INI : '
                                 WK-DATA-INIZIO-VFC
                                 ' -  '
                                 'FUNZ : '
                                 WK-NOME-FUNZIONE-VFC
                                 ' -  '
                                 'STEP ELAB. : '
                                 WK-STEP-ELAB-VFC
                                 ' - '
                                 'RC : '
                                 IDSV0003-RETURN-CODE
                                 ' - '
                                 'SQLCODE : '
                                 WS-SQLCODE
                                 DELIMITED BY SIZE INTO
                                 IVVC0211-DESCRIZ-ERR
                        END-STRING

                  END-EVALUATE
              ELSE
      *-->     GESTIRE ERRORE DISPATCHER
                  SET IVVC0211-SQL-ERROR  TO TRUE

                  MOVE PGM-LDBS8800       TO IVVC0211-COD-SERVIZIO-BE


                  STRING PGM-LDBS8800
                         ' - '
                         'RC : '
                         IDSV0003-RETURN-CODE
                         DELIMITED BY SIZE INTO
                         IVVC0211-DESCRIZ-ERR
                  END-STRING

              END-IF.

       M010-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ACCESSO ALLA TABELLA VAR_FUNZ_DI_CALC
      *----------------------------------------------------------------*
       M020-ACCESSO-STANDARD.
      *--> LIVELLO OPERAZIONE
              SET  IDSV0003-PRIMARY-KEY    TO TRUE

      *--> TIPO OPERAZIONE
              SET  IDSV0003-SELECT         TO TRUE

      *--> TRATTAMENTO-STORICITA
              SET IDSV0003-TRATT-SENZA-STOR TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE


              CALL PGM-LDBS0270           USING IDSV0003
                                                VAR-FUNZ-DI-CALC

              IF IDSV0003-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSV0003-SUCCESSFUL-SQL
      *-->         OPERAZIONE ESEGUITA CORRETTAMENTE

NEWP                      SET WK-VAR-FUNZ-TROVATA-SI TO TRUE
                          IF GESTIONE-PRODOTTO
                             ADD 1 TO IVVC0216-ELE-LIVELLO-MAX-P
                             MOVE IVVC0216-ELE-LIVELLO-MAX-P
                               TO IND-LIVELLO
                          ELSE
                             ADD 1 TO IVVC0216-ELE-LIVELLO-MAX-T
                             MOVE IVVC0216-ELE-LIVELLO-MAX-T
                               TO IND-LIVELLO
                          END-IF
                          PERFORM M500-VALORIZZA-VARIABILI THRU M500-EX

                      WHEN IDSV0003-NOT-FOUND
      *--->        CAMPO $ NON TROVATO

                         IF NOT TIPO-OPZIONI
                            IF IVVC0211-IN-CONV
                               IF SKEDE-TOTALI AND GESTIONE-GARANZIE
                                  CONTINUE
                               ELSE
                                  SET FINE-ELAB-SI           TO TRUE
                                  SET IVVC0211-GENERIC-ERROR TO TRUE

                                  MOVE PGM-LDBS0270
                                         TO IVVC0211-COD-SERVIZIO-BE

                                  STRING 'OCCORRENZA NON TROVATA SU VFC'
                                         ' - '
                                         'COMP : '
                                         WK-CODICE-COMPAGNIA
                                         ' - '
                                         'PROD : '
                                         WK-CODICE-PRODOTTO-VFC
                                         ' - '
                                         'TARI : '
                                         WK-CODICE-TARIFFA-VFC
                                         ' -  '
                                         'DT INI : '
                                         WK-DATA-INIZIO-VFC
                                         ' -  '
                                         'FUNZ : '
                                         WK-NOME-FUNZIONE-VFC
                                         ' -  '
                                         'STEP ELAB. : '
                                         WK-STEP-ELAB-VFC
                                         DELIMITED BY SIZE INTO
                                         IVVC0211-DESCRIZ-ERR
                                  END-STRING

                                  MOVE 'VAR_FUNZ_DI_CALC'
                                               TO IVVC0211-NOME-TABELLA
                               END-IF
                            END-IF
                         END-IF

                      WHEN OTHER
      *--->        ERRORE DI ACCESSO AL DB
                         SET FINE-ELAB-SI           TO TRUE
                         SET IVVC0211-SQL-ERROR     TO TRUE

                         MOVE PGM-LDBS0270
                                      TO IVVC0211-COD-SERVIZIO-BE

                         MOVE IDSV0003-SQLCODE TO WS-SQLCODE

                        STRING PGM-LDBS0270
                                 ' - '
                                 'COMP : '
                                 WK-CODICE-COMPAGNIA
                                 ' - '
                                 'PROD : '
                                 WK-CODICE-PRODOTTO-VFC
                                 ' -  '
                                 'TARI : '
                                 WK-CODICE-TARIFFA-VFC
                                 ' -  '
                                 'DT INI : '
                                 WK-DATA-INIZIO-VFC
                                 ' -  '
                                 'FUNZ : '
                                 WK-NOME-FUNZIONE-VFC
                                 ' -  '
                                 'STEP ELAB. : '
                                 WK-STEP-ELAB-VFC
                                 ' - '
                                 'RC : '
                                 IDSV0003-RETURN-CODE
                                 ' - '
                                 'SQLCODE : '
                                 WS-SQLCODE
                                 DELIMITED BY SIZE INTO
                                 IVVC0211-DESCRIZ-ERR
                        END-STRING

                  END-EVALUATE
              ELSE
      *-->     GESTIRE ERRORE DISPATCHER
                  SET IVVC0211-SQL-ERROR  TO TRUE

                  MOVE PGM-LDBS0270       TO IVVC0211-COD-SERVIZIO-BE


                  STRING PGM-LDBS0270
                         ' - '
                         'RC : '
                         IDSV0003-RETURN-CODE
                         DELIMITED BY SIZE INTO
                         IVVC0211-DESCRIZ-ERR
                  END-STRING

              END-IF.

       M020-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA DCLGEN TABELLA VARIABILI FUNZIONI DI CALCOLO
      *----------------------------------------------------------------*
       M100-VAL-DCLGEN-VAR-FUNZ.

           INITIALIZE                        VAR-FUNZ-DI-CALC
                                             VAR-FUNZ-DI-CALC-R.

           MOVE IVVC0211-COD-COMPAGNIA-ANIA   TO VFC-IDCOMP
                                                 AC5-IDCOMP.
           MOVE SPACES                        TO VFC-CODTARI
                                                 AC5-CODTARI
                                                 WK-CODICE-TARIFFA-AC5
                                                 WK-CODICE-TARIFFA-VFC.
           MOVE IVVC0211-STEP-ELAB            TO VFC-STEP-ELAB
                                                 AC5-STEP-ELAB
                                                 WK-STEP-ELAB-VFC
           EVALUATE TRUE

              WHEN TIPO-STANDARD
                 IF LCCV0021-TP-MOV-ACT = SPACES OR LOW-VALUE
                                          OR HIGH-VALUE
                                          OR ZEROES
                    MOVE IVVC0211-TIPO-MOVIMENTO(2:4) TO VFC-NOMEFUNZ
                                                         AC5-NOMEFUNZ
                                                  WK-NOME-FUNZIONE-VFC
                 ELSE
                    MOVE LCCV0021-TP-MOV-ACT(2:4) TO VFC-NOMEFUNZ
                                                         AC5-NOMEFUNZ
                                                  WK-NOME-FUNZIONE-VFC
                 END-IF
                 PERFORM M120-CNTL-VAL-DCLGEN-STD     THRU M120-EX

              WHEN TIPO-OPZIONI
                 MOVE WOPZ-COD-TIPO-OPZIONE(IND-TIPO-OPZ)
                                                 TO VFC-NOMEFUNZ
                                                    AC5-NOMEFUNZ
                                                    WK-NOME-FUNZIONE-VFC
                 PERFORM M130-CNTL-VAL-DCLGEN-OPZ      THRU M130-EX

           END-EVALUATE.
      *
           IF  IVVC0211-CODICE-INIZIATIVA > SPACES AND
               IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE
      *   valorizzazione dati accesso alla tab. VAR_FUNZ_DI_CALC
      *   in caso di iniziativa commerciale
               MOVE IVVC0211-CODICE-INIZIATIVA TO VFC-CODPROD
                                                  WK-CODICE-PRODOTTO-VFC
           ELSE
      *   Accesso alla tab. VAR_FUNZ_DI_CALC_R x codice trattato
      *   in caso di esplosione e ricalcolo RIASS
               IF  IVVC0211-CODICE-TRATTATO > SPACES
               AND IVVC0211-CODICE-TRATTATO NOT = HIGH-VALUE
                   CONTINUE
               ELSE
                   MOVE WPOL-COD-PROD            TO VFC-CODPROD
                                                  WK-CODICE-PRODOTTO-VFC
               END-IF
           END-IF.

       M100-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA DCLGEN TABELLA VARIABILI FUNZIONI DI CALCOLO
      *    --- GESTIONE PRODOTTO ---
      *----------------------------------------------------------------*
       M101-VAL-DCLGEN-VAR-FUNZ-PROD.

           IF  IVVC0211-CODICE-INIZIATIVA > SPACES AND
               IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE
      *   valorizzazione dati accesso alla tab. VAR_FUNZ_DI_CALC
      *   in caso di iniziativa commerciale
               MOVE 'P'                      TO VFC-CODTARI
                                                WK-CODICE-TARIFFA-VFC
           ELSE
      *   Accesso alla tab. VAR_FUNZ_DI_CALC_R x codice trattato
      *   in caso di esplosione e ricalcolo RIASS
               IF  IVVC0211-CODICE-TRATTATO > SPACES
               AND IVVC0211-CODICE-TRATTATO NOT = HIGH-VALUE
                   MOVE 'P'             TO AC5-CODTARI
                                           WK-CODICE-TARIFFA-AC5
               END-IF
           END-IF.

           PERFORM M150-VERIFICA-VERS-PROD-GP  THRU M150-EX.

       M101-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA DCLGEN TABELLA VARIABILI FUNZIONI DI CALCOLO
      *    --- GESTIONE GARANZIE ---
      *----------------------------------------------------------------*
       M102-VAL-DCLGEN-VAR-FUNZ-GAR.

           IF  IVVC0211-CODICE-INIZIATIVA > SPACES AND
               IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE
      *   valorizzazione dati accesso alla tab. VAR_FUNZ_DI_CALC
      *   in caso di iniziativa commerciale
               MOVE 'G'                      TO VFC-CODTARI
                                                WK-CODICE-TARIFFA-VFC
           ELSE
      *   Accesso alla tab. VAR_FUNZ_DI_CALC_R x codice trattato
      *   in caso di esplosione e ricalcolo RIASS
               IF  IVVC0211-CODICE-TRATTATO > SPACES
               AND IVVC0211-CODICE-TRATTATO NOT = HIGH-VALUE
                   MOVE 'G'             TO AC5-CODTARI
                                           WK-CODICE-TARIFFA-AC5
      * MOD20110729
                   MOVE WGRZ-COD-TARI(IND-GRZ) TO VFC-CODTARI
                                                WK-CODICE-TARIFFA-VFC
      * MOD20110729
               ELSE
                   EVALUATE TRUE
                      WHEN GARANZIA-CONTESTO
                          MOVE WGRZ-COD-TARI(IND-GRZ) TO VFC-CODTARI
                                                   WK-CODICE-TARIFFA-VFC

                      WHEN GARANZIA-DB
                          MOVE GRZ-COD-TARI           TO VFC-CODTARI
                                                   WK-CODICE-TARIFFA-VFC
                   END-EVALUATE
               END-IF
           END-IF.

           IF WGRZ-ST-ADD(IND-GRZ)
              PERFORM M160-VERIFICA-VERS-PROD-GG THRU M160-EX
           ELSE
              PERFORM M152-VAL-DT-VLDT-PROD   THRU M152-EX
           END-IF.

       M102-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA DCLGEN TABELLA VARIABILI FUNZIONI DI CALCOLO
      *    --- GESTIONE OPZIONI ---
      *----------------------------------------------------------------*
       M103-VAL-DCLGEN-VAR-FUNZ-OPZ.

           MOVE WOPZ-GAR-OPZ-CODICE(IND-TIPO-OPZ, IND-OPZ)
                                             TO VFC-CODTARI
                                                AC5-CODTARI
                                                WK-CODICE-TARIFFA-AC5
                                                WK-CODICE-TARIFFA-VFC.

           PERFORM M170-VERIFICA-VERS-PROD-GO THRU M170-EX.

       M103-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLO VALORIZZAZIONE DCLGEN STANDARD
      *----------------------------------------------------------------*
       M120-CNTL-VAL-DCLGEN-STD.

           EVALUATE TRUE

              WHEN GESTIONE-PRODOTTO

                 PERFORM M101-VAL-DCLGEN-VAR-FUNZ-PROD THRU M101-EX

              WHEN GESTIONE-GARANZIE

                 PERFORM M102-VAL-DCLGEN-VAR-FUNZ-GAR  THRU M102-EX

           END-EVALUATE.

       M120-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLO VALORIZZAZIONE DCLGEN TIPO OPZIONI
      *----------------------------------------------------------------*
       M130-CNTL-VAL-DCLGEN-OPZ.

           IF  IVVC0211-CODICE-INIZIATIVA > SPACES AND
               IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE
      *   segnalazione di errore nel caso di iniziativa commerciale
               SET IVVC0211-TIPO-SCHEDA-NOT-VALID TO TRUE

               MOVE WK-PGM  TO IVVC0211-COD-SERVIZIO-BE

               STRING 'TIPO SCHEDA NON GESTITA '
                      ' - '
                      'COMP : '
                      WK-CODICE-COMPAGNIA
                      ' - '
                      'TIPO SCHEDA : '
                      FLAG-TIPOLOGIA-SCHEDA
                      DELIMITED BY SIZE INTO
                      IVVC0211-DESCRIZ-ERR
               END-STRING
           ELSE
      *   Accesso alla tab. VAR_FUNZ_DI_CALC_R x codice trattato
      *   in caso di esplosione e ricalcolo RIASS
               IF  IVVC0211-CODICE-TRATTATO > SPACES
               AND IVVC0211-CODICE-TRATTATO NOT = HIGH-VALUE
                   IF GESTIONE-PRODOTTO
                      MOVE 'P'             TO AC5-CODTARI
                                              WK-CODICE-TARIFFA-AC5
                   ELSE
                      MOVE 'G'             TO AC5-CODTARI
                                              WK-CODICE-TARIFFA-AC5
                   END-IF
               END-IF
           END-IF.

           IF IVVC0211-SUCCESSFUL-RC
              EVALUATE TRUE

                 WHEN GESTIONE-PRODOTTO

                    MOVE WPOL-FL-VER-PROD             TO FLAG-VERSIONE

                    IF EMISSIONE
                       MOVE WPOL-DT-INI-VLDT-PROD     TO VFC-DINIZ
                                                         AC5-DINIZ
                                                      WK-DATA-INIZIO-VFC
                    ELSE
                       MOVE IVVC0211-DATA-ULT-VERS-PROD TO VFC-DINIZ
                                                           AC5-DINIZ
                                                      WK-DATA-INIZIO-VFC
                    END-IF

                 WHEN GESTIONE-GARANZIE

                    PERFORM M103-VAL-DCLGEN-VAR-FUNZ-OPZ  THRU M103-EX

              END-EVALUATE
           END-IF.

       M130-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICA IL FLAG VERSIONE DI PRODOTTO -
      *    --- GESTIONE PRODOTTO ---
      *----------------------------------------------------------------*
       M150-VERIFICA-VERS-PROD-GP.

           IF  (IVVC0211-CODICE-INIZIATIVA > SPACES AND
                IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE )
36895 *    OR RIASS-SI
               MOVE IVVC0211-DATA-EFFETTO TO VFC-DINIZ
                                             AC5-DINIZ
                                             WK-DATA-INIZIO-VFC
           ELSE
               EVALUATE TRUE
                  WHEN EMISSIONE
                       MOVE WPOL-DT-INI-VLDT-PROD TO VFC-DINIZ
                                                     AC5-DINIZ
                                                     WK-DATA-INIZIO-VFC
36895                  IF RIASS-SI
                          MOVE WGRZ-DT-DECOR(1)   TO AC5-DINIZ
                       END-IF

                  WHEN ULTIMA
                       PERFORM M151-CNTL-VEN-PVEN       THRU M151-EX

                  WHEN OTHER
                       SET IVVC0211-VERS-PROD-NOT-VALID  TO TRUE
                       MOVE WK-PGM           TO IVVC0211-COD-SERVIZIO-BE

                       STRING 'FLAG VERS. PROD.  NON VALIDO'
                             DELIMITED BY SIZE INTO
                             IVVC0211-DESCRIZ-ERR
                       END-STRING
               END-EVALUATE
           END-IF.

       M150-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA DCLGEN TABELLA VARIABILI FUNZIONI DI CALCOLO
      *----------------------------------------------------------------*
       M151-CNTL-VEN-PVEN.

           IF IVVC0211-AREA-VE
              MOVE IVVC0211-DATA-ULT-VERS-PROD TO VFC-DINIZ
                                                  AC5-DINIZ
                                                  WK-DATA-INIZIO-VFC
           ELSE
              PERFORM M152-VAL-DT-VLDT-PROD THRU M152-EX
           END-IF.

       M151-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA DCLGEN TABELLA VARIABILI FUNZIONI DI CALCOLO
      *----------------------------------------------------------------*
       M152-VAL-DT-VLDT-PROD.


           IF  (IVVC0211-CODICE-INIZIATIVA > SPACES AND
                IVVC0211-CODICE-INIZIATIVA NOT = HIGH-VALUE )
36895 *    OR RIASS-SI
               MOVE IVVC0211-DATA-EFFETTO TO VFC-DINIZ
                                             AC5-DINIZ
                                             WK-DATA-INIZIO-VFC
           ELSE
               IF TRANCHE-CONTESTO
18989              IF WTGA-DT-VLDT-PROD-NULL(IND-TGA) NOT = HIGH-VALUE
                      MOVE WTGA-DT-VLDT-PROD(IND-TGA) TO VFC-DINIZ
                                                      AC5-DINIZ
                                                      WK-DATA-INIZIO-VFC
36895                 IF RIASS-SI
                         MOVE WGRZ-DT-DECOR(1)       TO AC5-DINIZ
                      END-IF
18989              ELSE
  =                   SET IVVC0211-VERS-PROD-NOT-VALID  TO TRUE
  =                   MOVE WK-PGM            TO IVVC0211-COD-SERVIZIO-BE
  =                   STRING 'DATA VALID. PROD. NON VALIDA'
  =                          DELIMITED BY SIZE INTO
  =                          IVVC0211-DESCRIZ-ERR
  =                   END-STRING
  =                   MOVE 'TRCH_DI_GAR'       TO IVVC0211-NOME-TABELLA
18989              END-IF
               ELSE
18989              IF TGA-DT-VLDT-PROD-NULL NOT = HIGH-VALUE
                      MOVE TGA-DT-VLDT-PROD       TO VFC-DINIZ
                                                     AC5-DINIZ
                                                     WK-DATA-INIZIO-VFC
36895                 IF RIASS-SI
                         MOVE WGRZ-DT-DECOR(1)       TO AC5-DINIZ
                      END-IF
18989              ELSE
  =                   SET IVVC0211-VERS-PROD-NOT-VALID  TO TRUE
  =                   MOVE WK-PGM            TO IVVC0211-COD-SERVIZIO-BE
  =                   STRING 'DATA VALID. PROD. NON VALIDA'
  =                          DELIMITED BY SIZE INTO
  =                          IVVC0211-DESCRIZ-ERR
  =                   END-STRING
  =                   MOVE 'TRCH_DI_GAR'       TO IVVC0211-NOME-TABELLA
18989              END-IF
               END-IF
           END-IF.

       M152-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICA IL FLAG VERSIONE DI PRODOTTO
      *    --- GESTIONE GARANZIE ---
      *----------------------------------------------------------------*
       M160-VERIFICA-VERS-PROD-GG.

           EVALUATE TRUE
              WHEN EMISSIONE
                   MOVE WPOL-DT-INI-VLDT-PROD   TO VFC-DINIZ
                                                   AC5-DINIZ
                                                   WK-DATA-INIZIO-VFC
36895              IF RIASS-SI
                      MOVE WGRZ-DT-DECOR(IND-GRZ) TO AC5-DINIZ
                   END-IF

              WHEN ULTIMA
                   MOVE IVVC0211-DATA-ULT-VERS-PROD
                                                TO VFC-DINIZ
                                                   AC5-DINIZ
                                                   WK-DATA-INIZIO-VFC
36895              IF RIASS-SI
                      MOVE WGRZ-DT-DECOR(IND-GRZ) TO AC5-DINIZ
                   END-IF

              WHEN OTHER
                   SET IVVC0211-VERS-PROD-NOT-VALID  TO TRUE
                   MOVE WK-PGM            TO IVVC0211-COD-SERVIZIO-BE

                   STRING 'FLAG VERS. PROD.  NON VALIDO'
                         DELIMITED BY SIZE INTO
                         IVVC0211-DESCRIZ-ERR
                   END-STRING
           END-EVALUATE.

       M160-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICA IL FLAG VERSIONE DI GARANZIE
      *    --- GESTIONE OPZIONI ---
      *----------------------------------------------------------------*
       M170-VERIFICA-VERS-PROD-GO.

           EVALUATE TRUE

              WHEN EMISSIONE
                   MOVE WPOL-DT-DECOR       TO VFC-DINIZ
                                               AC5-DINIZ
                                               WK-DATA-INIZIO-VFC
              WHEN ULTIMA
                   MOVE WOPZ-GAR-OPZ-DECORRENZA(IND-TIPO-OPZ, IND-OPZ)
                                             TO VFC-DINIZ
                                                AC5-DINIZ
                                                WK-DATA-INIZIO-VFC
              WHEN SPAZIO
                   MOVE WOPZ-GAR-OPZ-DECORRENZA(IND-TIPO-OPZ, IND-OPZ)
                                             TO VFC-DINIZ
                                                AC5-DINIZ
                                                WK-DATA-INIZIO-VFC
           END-EVALUATE.

       M170-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA L'AREA VARIABILI CON L'OUTPUT RESTITUITO DALLA
      *    LETTURA SULLA TABELLA VARIABILI FUNZIONI DI CALCOLO
      *----------------------------------------------------------------*
       M500-VALORIZZA-VARIABILI.
           IF VFC-ISPRECALC = PRESENZA-VARIABILI
              PERFORM L350-CARICA-LIVELLI       THRU L350-EX

              IF VFC-GLOVARLIST  = HIGH-VALUES  OR
                                   LOW-VALUE    OR
                                   SPACES
      *--->    VARIABILI NON PRESENTI
                CONTINUE
      *           SET IVVC0211-GLOVARLIST-VUOTA  TO  TRUE

              ELSE

                 SET WK-GLOVARLIST-PIENA  TO  TRUE

                 PERFORM M600-SCOMPATTA-GLOVAR       THRU M600-EX

              END-IF
           ELSE
              IF VFC-ISPRECALC = PRESENZA-SERVIZIO
                 ADD 1                    TO IND-LIVELLO

                 IF TIPO-OPZIONI
                    IF GESTIONE-PRODOTTO
                       MOVE WOPZ-COD-TIPO-OPZIONE     (IND-TIPO-OPZ)
                            TO IVVC0216-COD-TIPO-OPZIONE-P(IND-LIVELLO)
                    ELSE
                       MOVE WOPZ-COD-TIPO-OPZIONE     (IND-TIPO-OPZ)
                            TO IVVC0216-COD-TIPO-OPZIONE-T(IND-LIVELLO)
                    END-IF
                 END-IF

                 IF GESTIONE-PRODOTTO
                    MOVE VFC-NOMEPROGR
                         TO IVVC0216-NOME-SERVIZIO-P(IND-LIVELLO)
                 ELSE
                    MOVE VFC-NOMEPROGR
                         TO IVVC0216-NOME-SERVIZIO-T(IND-LIVELLO)
                 END-IF
              END-IF
           END-IF.

       M500-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   PREPARA STRINGA DA UNZIPPARE
      *----------------------------------------------------------------*
       M501-PREPARA-UNZIP-GLOVAR.

           MOVE LENGTH OF VFC-GLOVARLIST      TO UNZIP-LENGTH-STR-MAX.

           IF GESTIONE-PRODOTTO
              MOVE LENGTH OF IVVC0216-COD-VARIABILE-P(1, 1)
                                              TO UNZIP-LENGTH-FIELD
           ELSE
              MOVE LENGTH OF IVVC0216-COD-VARIABILE-T(1, 1)
                                              TO UNZIP-LENGTH-FIELD
           END-IF

           MOVE VFC-GLOVARLIST                TO UNZIP-STRING-ZIPPED.
           MOVE ','                           TO UNZIP-IDENTIFICATORE.

       M501-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CARICA CAMPI UNZIPPED
      *----------------------------------------------------------------*
       M502-CARICA-GLOVAR-UNZIPPED.

           MOVE UNZIP-ELE-VARIABILI-MAX TO VARIABILI-RIASS

           IF RIASS-SI
              CONTINUE
           ELSE
              IF GESTIONE-PRODOTTO
                 PERFORM VARYING IND-UNZIP FROM 1 BY 1
                         UNTIL IND-UNZIP  >
                               LIMITE-VARIABILI-UNZIPPED OR
                               IND-UNZIP  >
                               UNZIP-ELE-VARIABILI-MAX   OR
                               UNZIP-COD-VARIABILE(IND-UNZIP) =
                               SPACES OR LOW-VALUE OR HIGH-VALUE
                        MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
                          TO IVVC0216-COD-VARIABILE-P
                             (IND-LIVELLO, IND-UNZIP)

                 END-PERFORM
              ELSE
                 PERFORM VARYING IND-UNZIP FROM 1 BY 1
                         UNTIL IND-UNZIP  >
                               LIMITE-VARIABILI-UNZIPPED OR
                               IND-UNZIP  >
                               UNZIP-ELE-VARIABILI-MAX   OR
                               UNZIP-COD-VARIABILE(IND-UNZIP) =
                               SPACES OR LOW-VALUE OR HIGH-VALUE

                        MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
                          TO IVVC0216-COD-VARIABILE-T
                             (IND-LIVELLO, IND-UNZIP)

                 END-PERFORM
              END-IF
           END-IF.

      *--->  STATO NON TROVATO
           IF UNZIP-TROVATO-NO
              SET FINE-ELAB-SI           TO TRUE
              SET IVVC0211-GENERIC-ERROR TO TRUE

              MOVE WK-PGM                TO IVVC0211-COD-SERVIZIO-BE

              STRING 'VARIABILI NON TROVATE SU '
                     'VAR_FUNZ_DI_CALC'
                     ' - '
                     'COMPAGNIA : '
                     ' -  '
                     WK-CODICE-COMPAGNIA
                     ' - '
                     'COD. PRODOTTO : '
                     WK-CODICE-PRODOTTO-VFC
                     ' -  '
                     'COD. TARIFFA : '
                     WK-CODICE-TARIFFA-VFC
                     ' -  '
                     'DATA INIZIO : '
                     WK-DATA-INIZIO-VFC
                     ' -  '
                     'NOMEFUNZIONE : '
                     WK-NOME-FUNZIONE-VFC
                     ' -  '
                     'STEP ELAB. : '
                     WK-STEP-ELAB-VFC
                     DELIMITED BY SIZE INTO
                     IVVC0211-DESCRIZ-ERR
              END-STRING

              MOVE 'VAR_FUNZ_DI_CALC'
                                           TO IVVC0211-NOME-TABELLA
           ELSE
              IF GESTIONE-PRODOTTO
                 MOVE UNZIP-ELE-VARIABILI-MAX
                   TO IVVC0216-ELE-VARIABILI-MAX-P(IND-LIVELLO)
              ELSE
                 MOVE UNZIP-ELE-VARIABILI-MAX
                   TO IVVC0216-ELE-VARIABILI-MAX-T(IND-LIVELLO)
              END-IF
           END-IF.

       M502-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   PREPARA STRINGA DA UNZIPPARE RIASS
      *----------------------------------------------------------------*
       M503-PREPARA-UNZIP-GLOVAR-RS.

           MOVE LENGTH OF AC5-GLOVARLIST      TO UNZIP-LENGTH-STR-MAX.
           IF GESTIONE-PRODOTTO
              MOVE LENGTH OF IVVC0216-COD-VARIABILE-P(1, 1)
                                              TO UNZIP-LENGTH-FIELD
           ELSE
              MOVE LENGTH OF IVVC0216-COD-VARIABILE-T(1, 1)
                                              TO UNZIP-LENGTH-FIELD
           END-IF

           MOVE AC5-GLOVARLIST                TO UNZIP-STRING-ZIPPED.
           MOVE ','                           TO UNZIP-IDENTIFICATORE.

       M503-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CARICA CAMPI UNZIPPED RIASS
      *----------------------------------------------------------------*
       M504-CARICA-GLOVAR-UNZIPPED-RS.

           IF RIASS-SI
              PERFORM GESTIONE-RIASS
                 THRU GESTIONE-RIASS-EX
           ELSE
              IF GESTIONE-PRODOTTO
                 PERFORM VARYING IND-UNZIP
                         FROM  1 BY 1
                         UNTIL IND-UNZIP  >
                               LIMITE-VARIABILI-UNZIPPED OR
                               IND-UNZIP  >
                               UNZIP-ELE-VARIABILI-MAX   OR
                               UNZIP-COD-VARIABILE(IND-UNZIP) =
                               SPACES OR LOW-VALUE OR HIGH-VALUE

                         MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
                           TO IVVC0216-COD-VARIABILE-P
                              (IND-LIVELLO, IND-UNZIP)
                 END-PERFORM
              ELSE
                 PERFORM VARYING IND-UNZIP
                         FROM  1 BY 1
                         UNTIL IND-UNZIP  >
                               LIMITE-VARIABILI-UNZIPPED OR
                               IND-UNZIP  >
                               UNZIP-ELE-VARIABILI-MAX   OR
                               UNZIP-COD-VARIABILE(IND-UNZIP) =
                               SPACES OR LOW-VALUE OR HIGH-VALUE

                         MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
                           TO IVVC0216-COD-VARIABILE-T
                              (IND-LIVELLO, IND-UNZIP)
                 END-PERFORM
              END-IF
           END-IF.

      *--->  STATO NON TROVATO
           IF UNZIP-TROVATO-NO
              SET FINE-ELAB-SI           TO TRUE
              SET IVVC0211-GENERIC-ERROR TO TRUE

              MOVE WK-PGM                TO IVVC0211-COD-SERVIZIO-BE

              STRING 'VARIABILI NON TROVATE SU '
                     'VAR_FUNZ_DI_CALC_RS'
                     ' - '
                     'COMPAGNIA : '
                     ' -  '
                     WK-CODICE-COMPAGNIA
                     ' - '
                     'COD. PRODOTTO : '
                     WK-CODICE-PRODOTTO-VFC
                     ' -  '
                     'COD. TARIFFA : '
                     WK-CODICE-TARIFFA-AC5
                     ' -  '
                     'DATA INIZIO : '
                     WK-DATA-INIZIO-VFC
                     ' -  '
                     'NOMEFUNZIONE : '
                     WK-NOME-FUNZIONE-VFC
                     ' -  '
                     'STEP ELAB. : '
                     WK-STEP-ELAB-VFC
                     DELIMITED BY SIZE INTO
                     IVVC0211-DESCRIZ-ERR
              END-STRING

              MOVE 'VAR_FUNZ_DI_CALC_R'
                                           TO IVVC0211-NOME-TABELLA
           ELSE
              IF GESTIONE-PRODOTTO
                 MOVE UNZIP-ELE-VARIABILI-MAX
                   TO IVVC0216-ELE-VARIABILI-MAX-P(IND-LIVELLO)
              ELSE
                 MOVE UNZIP-ELE-VARIABILI-MAX
                   TO IVVC0216-ELE-VARIABILI-MAX-T(IND-LIVELLO)
              END-IF
           END-IF.

       M504-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA L'AREA VARIABILI CON L'OUTPUT RESTITUITO DALLA
      *    LETTURA SULLA TABELLA VARIABILI FUNZIONI DI CALCOLO RIASS
      *----------------------------------------------------------------*
       M510-VALORIZZA-VARIABILI-RIASS.

           IF AC5-ISPRECALC = PRESENZA-VARIABILI

              PERFORM L355-CARICA-LIVELLI-RIASS       THRU L355-EX

              IF AC5-GLOVARLIST  = HIGH-VALUES  OR
                                   LOW-VALUE    OR
                                   SPACES
      *--->    VARIABILI NON PRESENTI
                 CONTINUE

              ELSE

                 PERFORM M601-SCOMPATTA-GLOVAR-RIASS    THRU M601-EX

              END-IF
           ELSE
              IF AC5-ISPRECALC = PRESENZA-SERVIZIO
                 ADD 1                    TO IND-LIVELLO

                 IF TIPO-OPZIONI
                    IF GESTIONE-PRODOTTO
                       MOVE WOPZ-COD-TIPO-OPZIONE     (IND-TIPO-OPZ)
                            TO IVVC0216-COD-TIPO-OPZIONE-P (IND-LIVELLO)
                    ELSE
                       MOVE WOPZ-COD-TIPO-OPZIONE     (IND-TIPO-OPZ)
                            TO IVVC0216-COD-TIPO-OPZIONE-T (IND-LIVELLO)
                    END-IF
                 END-IF

                 IF GESTIONE-PRODOTTO
                    MOVE AC5-NOMEPROGR
                         TO IVVC0216-NOME-SERVIZIO-P(IND-LIVELLO)
                 ELSE
                    MOVE AC5-NOMEPROGR
                         TO IVVC0216-NOME-SERVIZIO-T(IND-LIVELLO)
                 END-IF
              END-IF
           END-IF.

       M510-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   SCOMPATTA GLOVAR
      *----------------------------------------------------------------*
       M600-SCOMPATTA-GLOVAR.

           PERFORM M501-PREPARA-UNZIP-GLOVAR   THRU M501-EX
           PERFORM U999-UNZIP-STRING           THRU U999-EX
           IF IVVC0211-SUCCESSFUL-RC
              PERFORM M502-CARICA-GLOVAR-UNZIPPED THRU M502-EX
           END-IF.

       M600-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   SCOMPATTA GLOVAR RIASS
      *----------------------------------------------------------------*
       M601-SCOMPATTA-GLOVAR-RIASS.

           PERFORM M503-PREPARA-UNZIP-GLOVAR-RS   THRU M503-EX
           PERFORM U999-UNZIP-STRING              THRU U999-EX
           IF IVVC0211-SUCCESSFUL-RC
              PERFORM M504-CARICA-GLOVAR-UNZIPPED-RS THRU M504-EX
           END-IF.

       M601-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CERCA ADESIONE
      *----------------------------------------------------------------*
       N000-CERCA-ADESIONE.

           IF GESTIONE-GARANZIE
MIGCOL       IF  WPOL-TP-FRM-ASSVA = 'CO'
MIGCOL       AND (IVVC0211-TIPO-MOVIMENTO = 2031
MIGCOL         OR IVVC0211-TIPO-MOVIMENTO = 2001
MIGCOL         OR IVVC0211-TIPO-MOVIMENTO = 2339
MIGCOL         OR IVVC0211-TIPO-MOVIMENTO = 2340
MIGCOL         OR IVVC0211-TIPO-MOVIMENTO = 2006 )
MIGCOL          CONTINUE
MIGCOL       ELSE
              IF WADE-ID-ADES NOT NUMERIC OR
                 WADE-ID-ADES = ZEROES

                  SET IVVC0211-AREA-NOT-FOUND TO TRUE
                  MOVE WK-PGM             TO IVVC0211-COD-SERVIZIO-BE
                  STRING 'ADESIONI NON TROVATE'
                         DELIMITED BY SIZE INTO
                         IVVC0211-DESCRIZ-ERR
                  END-STRING
              END-IF
MIGCOL       END-IF
           END-IF.

       N000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   VALORIZZA TIPO FLAG-SKEDE
      *----------------------------------------------------------------*
       S000-VALORIZZA-TIPO-SKEDE.

           MOVE MVV-TIPO-OGGETTO             TO FLAG-SKEDE

           IF IVVC0211-IN-CONV
              IF NOT SKEDE-PRODOTTI AND
                 NOT SKEDE-TOTALI

                  SET FINE-ELAB-SI           TO TRUE
                  SET IVVC0211-GENERIC-ERROR TO TRUE

                  MOVE WK-PGM                TO IVVC0211-COD-SERVIZIO-BE

                  STRING 'TIPO_OGGETTO NON VALIDO'
                         ' - '
                         'COMPAGNIA : '
                         WK-CODICE-COMPAGNIA
                         ' - '
                         'TIPO MOVIM. : '
                         WK-TIPO-MOVIMENTO
                         ' - '
                         'STEP VALOR. : '
                         MVV-STEP-VALORIZZATORE
                         ' - '
                         'STEP CONVER. : '
                         MVV-STEP-CONVERSAZIONE
                         DELIMITED BY SIZE INTO
                         IVVC0211-DESCRIZ-ERR
                  END-STRING

                  MOVE 'MATR_VAL_VAR'      TO IVVC0211-NOME-TABELLA

              END-IF
           END-IF.

       S000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   ESTRAZIONE NOME VARIABILE DALLA STRINGA GLOBALVARLIST
      *----------------------------------------------------------------*
       U999-UNZIP-STRING.

      *--> IND-START-VAR  = INDICA IL PRIMO CHAR DI UNA VARIABILE(FISSO)
      *--> IND-END-VAR    = INDICA L'ULTIMO CHAR DI UNA VARIABILE(VAR)
      *--> IND-UNZIP      = E' IL CONTATORE DI VARIABILI TROVATE DA USARE
      *                     PER LA OCCURS 100 DI OUTPUT(AREA VARIABILI)
      *--> IND-CHAR       = INDICA LA LUNGHEZZA DI UNA VARIABILE
      *--> WK-GLOVAR-MAX

           MOVE 1                             TO IND-START-VAR.
           MOVE ZEROES                        TO IND-CHAR
           IF RIASS-SI
              CONTINUE
           ELSE
              MOVE ZEROES                     TO IND-UNZIP
           END-IF.

           SET NO-FINE-VARLIST                TO TRUE.


           PERFORM VARYING IND-END-VAR FROM 1 BY 1
                     UNTIL IND-END-VAR > UNZIP-LENGTH-STR-MAX
                        OR SI-FINE-VARLIST

             IF UNZIP-STRING-ZIPPED(IND-END-VAR:1) =
                                             UNZIP-IDENTIFICATORE OR
                                             SPACES               OR
                                             LOW-VALUE            OR
                                             HIGH-VALUE

      *-->   E' STATA TROVATA UNA VARIABILE E VIENE CARICATA IN
      *-->   OUTPUT
                IF IND-CHAR > 0
                   ADD  1                         TO IND-UNZIP

                   MOVE UNZIP-STRING-ZIPPED(IND-START-VAR:IND-CHAR)
                     TO UNZIP-COD-VARIABILE(IND-UNZIP)
                                           (1:UNZIP-LENGTH-FIELD)

                   COMPUTE IND-START-VAR = IND-END-VAR + 1

                   MOVE ZEROES                          TO IND-CHAR

                   IF UNZIP-STRING-ZIPPED(IND-END-VAR:1) = SPACES   OR
                                                        LOW-VALUE   OR
                                                        HIGH-VALUE
                      SET SI-FINE-VARLIST               TO TRUE
                   END-IF
                END-IF

             ELSE
                ADD  1                     TO IND-CHAR
                IF IND-END-VAR = UNZIP-LENGTH-STR-MAX
                   ADD  1                         TO IND-UNZIP

                   MOVE UNZIP-STRING-ZIPPED(IND-START-VAR:IND-CHAR)
                     TO UNZIP-COD-VARIABILE(IND-UNZIP)
                                           (1:UNZIP-LENGTH-FIELD)
                END-IF
             END-IF

           END-PERFORM.

           IF GESTIONE-PRODOTTO
              IF IND-UNZIP > IVVC0216-NUM-MAX-VARIABILI-P
                 SET FINE-ELAB-SI           TO TRUE
                 SET IVVC0211-GENERIC-ERROR TO TRUE

                 MOVE WK-PGM                TO IVVC0211-COD-SERVIZIO-BE

                STRING 'OVERFLOW TABELLA VARIABILI '
                        'IVVC0216-AREA-VARIABILI'
                        ' - '
                        'COMPAGNIA : '
                        WK-CODICE-COMPAGNIA
                        ' - '
                        'TIPO MOVIM. : '
                        WK-TIPO-MOVIMENTO
                        ' - '
                        'STEP VALOR. : '
                        MVV-STEP-VALORIZZATORE
                        ' - '
                        'STEP CONVER. : '
                        MVV-STEP-CONVERSAZIONE
                        DELIMITED BY SIZE INTO
                        IVVC0211-DESCRIZ-ERR
                 END-STRING

                 MOVE 'IVVC0216'             TO IVVC0211-NOME-TABELLA
              ELSE
                 MOVE IND-UNZIP              TO UNZIP-ELE-VARIABILI-MAX
              END-IF
           ELSE
              IF IND-UNZIP > IVVC0216-NUM-MAX-VARIABILI-T
                 SET FINE-ELAB-SI           TO TRUE
                 SET IVVC0211-GENERIC-ERROR TO TRUE

                 MOVE WK-PGM                TO IVVC0211-COD-SERVIZIO-BE

                STRING 'OVERFLOW TABELLA VARIABILI '
                        'IVVC0216-AREA-VARIABILI'
                        ' - '
                        'COMPAGNIA : '
                        WK-CODICE-COMPAGNIA
                        ' - '
                        'TIPO MOVIM. : '
                        WK-TIPO-MOVIMENTO
                        ' - '
                        'STEP VALOR. : '
                        MVV-STEP-VALORIZZATORE
                        ' - '
                        'STEP CONVER. : '
                        MVV-STEP-CONVERSAZIONE
                        DELIMITED BY SIZE INTO
                        IVVC0211-DESCRIZ-ERR
                 END-STRING

                 MOVE 'IVVC0216'             TO IVVC0211-NOME-TABELLA
              ELSE
                 MOVE IND-UNZIP              TO UNZIP-ELE-VARIABILI-MAX
              END-IF
           END-IF.

       U999-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ESTRAI AUTONOMIA OPERATIVA
      *----------------------------------------------------------------*
       P000-ESTRAI-AUT-OPER.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'IVVS0212'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Valorizzatore variabili'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           CALL PGM-IVVS0212         USING IDSV0003
                                           AREA-IVVV0212.

           MOVE IDSV0003-RETURN-CODE TO IVVC0211-RETURN-CODE
           MOVE IDSV0003-CAMPI-ESITO TO IVVC0211-CAMPI-ESITO.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'IVVS0212'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Valorizzatore variabili'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       P000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE VARIABILI
      *----------------------------------------------------------------*
       P100-ELABORA-VARIABILI.
      *
           MOVE IVVC0211-TRATTAMENTO-STORICITA
             TO IDSV0003-TRATTAMENTO-STORICITA.
      *    MOVE IVVC0216-FLG-AREA
      *      TO IVVC0211-FLG-AREA
           MOVE IVVC0211-FLG-AREA
             TO IVVC0216-FLG-AREA

           MOVE IVVC0211-AREA-ADDRESSES TO IDSV0101-AREA-ADDRESSES
      *
           MOVE IVVC0211-TIPO-MOVI-ORIG TO WK-MOVI-ORIG
      *
           IF IVVC0216-ELE-LIVELLO-MAX-P > 0
           OR IVVC0216-ELE-LIVELLO-MAX-T > 0

              SET  IDSV8888-BUSINESS-DBG      TO TRUE
              MOVE 'IVVS0216'                 TO IDSV8888-NOME-PGM
              SET  IDSV8888-INIZIO            TO TRUE
              MOVE SPACES                     TO IDSV8888-DESC-PGM
              STRING 'Valorizzatore variabili'
                      DELIMITED BY SIZE
                      INTO IDSV8888-DESC-PGM
              END-STRING
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

              CALL PGM-IVVS0216         USING IDSV0003
                                              IVVC0216-FLG-AREA
                                              AREA-WORK-IVVC0216
                                              AREA-WARNING-IVVC0216
                                              AREA-BUSINESS
                                              AREA-IDSV0101

              SET  IDSV8888-BUSINESS-DBG      TO TRUE
              MOVE 'IVVS0216'                 TO IDSV8888-NOME-PGM
              SET  IDSV8888-FINE              TO TRUE
              MOVE SPACES                     TO IDSV8888-DESC-PGM
              STRING 'Valorizzatore variabili'
                      DELIMITED BY SIZE
                      INTO IDSV8888-DESC-PGM
              END-STRING
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

              MOVE IDSV0003-RETURN-CODE TO IVVC0211-RETURN-CODE
              MOVE IDSV0003-CAMPI-ESITO TO IVVC0211-CAMPI-ESITO
              MOVE IVVC0216-AREA-VAR-KO TO IVVC0211-AREA-VAR-KO
              MOVE 'IVVS0211'           TO IVVC0211-COD-SERVIZIO-BE
           END-IF.

       P100-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       A900-OPERAZIONI-FINALI.

           IF IVVC0211-SUCCESSFUL-RC
              MOVE SPACES               TO IVVC0211-BUFFER-DATI
              PERFORM A950-TRAVASO-AREA-IVVC0216
                 THRU A950-EX
              MOVE AREA-IVVC0216        TO IVVC0211-BUFFER-DATI
              IF  WK-GLOVARLIST-VUOTA
                  SET IVVC0211-GLOVARLIST-VUOTA  TO  TRUE
              END-IF
NEWP          IF IVVC0211-PRE-CONV
NEWP          OR IVVC0211-POST-CONV
NEWP             IF (WK-VAR-FUNZ-TROVATA-NO AND
NEWP                 STAT-CAUS-TROVATI-NO   )
NEWP             OR (WK-VAR-FUNZ-TROVATA-NO AND
NEWP                 STAT-CAUS-TROVATI-SI   )
NEWP                 SET IVVC0211-SKIP-CALL-ACTUATOR TO TRUE
NEWP             END-IF
NEWP          END-IF
           ELSE
              IF IVVC0211-CALCOLO-NON-COMPLETO
                 SET IVVC0211-SUCCESSFUL-RC TO TRUE
                 INITIALIZE             IVVC0211-AREA-VAR-KO
                 MOVE SPACES            TO IVVC0211-BUFFER-DATI
                 PERFORM A950-TRAVASO-AREA-IVVC0216
                    THRU A950-EX
                 MOVE AREA-IVVC0216     TO IVVC0211-BUFFER-DATI
              END-IF
              IF IVVC0211-CACHE-PIENA
                 INITIALIZE             IVVC0211-AREA-VAR-KO
                 MOVE SPACES            TO IVVC0211-BUFFER-DATI
                 PERFORM A950-TRAVASO-AREA-IVVC0216
                    THRU A950-EX
                 MOVE AREA-IVVC0216     TO IVVC0211-BUFFER-DATI
              END-IF
           END-IF.

       A900-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    TRAVASO AREA DI WORK IVVC0216 PREDISPOSTA CON 100 VARIABILI
      *    NELL'AREA CON 60 VARIABILI RESTITUITA AL PGM CHIAMANTE
      *----------------------------------------------------------------*
       A950-TRAVASO-AREA-IVVC0216.

           MOVE IVVC0216-ELE-LIVELLO-MAX-P  TO C216-ELE-LIVELLO-MAX-P.
           MOVE IVVC0216-ELE-LIVELLO-MAX-T  TO C216-ELE-LIVELLO-MAX-T.

           MOVE IVVC0216-DEE                TO C216-DEE


           PERFORM VARYING IND-RIGA FROM 1 BY 1
                   UNTIL IND-RIGA > IVVC0216-ELE-LIVELLO-MAX-P
                      OR IND-RIGA > IVVC0216-NUM-MAX-LIVELLI-P
                      OR NOT IVVC0211-SUCCESSFUL-RC
                   MOVE IVVC0216-ID-POL-P(IND-RIGA)
                     TO C216-ID-POL-P(IND-RIGA)
                   MOVE IVVC0216-COD-TIPO-OPZIONE-P(IND-RIGA)
                     TO C216-COD-TIPO-OPZIONE-P(IND-RIGA)
                   MOVE IVVC0216-TP-LIVELLO-P(IND-RIGA)
                     TO C216-TP-LIVELLO-P(IND-RIGA)
                   MOVE IVVC0216-COD-LIVELLO-P(IND-RIGA)
                     TO C216-COD-LIVELLO-P(IND-RIGA)
      *  INFORMAZIONI DI TRANCHE
                   MOVE IVVC0216-ID-LIVELLO-P(IND-RIGA)
                     TO C216-ID-LIVELLO-P(IND-RIGA)
                   MOVE IVVC0216-DT-INIZ-PROD-P(IND-RIGA)
                     TO C216-DT-INIZ-PROD-P(IND-RIGA)
                   MOVE IVVC0216-COD-RGM-FISC-P(IND-RIGA)
                     TO C216-COD-RGM-FISC-P(IND-RIGA)
      * AREA VARIABILI PRODOTTO
                   MOVE IVVC0216-ELE-VARIABILI-MAX-P(IND-RIGA)
                     TO C216-ELE-VARIABILI-MAX-P(IND-RIGA)
                        WK-ELE-VARIABILI-MAX

      *   CONTROLLO DI OVERFLOW IN CASO DI VENDITA O
      *   POST-VENDITA SUL NUMERO DI VARIABILI RESTITUITE
                   IF  (IVVC0216-ELE-VARIABILI-MAX-P(IND-RIGA) >
                        IVVC0216-NUM-MAX-VARIABILI-P )
                      SET IVVC0211-GENERIC-ERROR TO TRUE
                      MOVE 'IVVC0216'
                        TO IDSV0003-NOME-TABELLA
                      MOVE WK-PGM            TO IVVC0211-COD-SERVIZIO-BE
                      STRING 'OVERFLOW TABELLA IVVC0216-AREA-VARIABILI'
                             ' IN FASE DI ' IVVC0216-FLG-AREA
                             ' - IVVC0216-ELE-VARIABILI-MAX :'
                              WK-ELE-VARIABILI-MAX
                      DELIMITED BY SIZE INTO IVVC0211-DESCRIZ-ERR
                      END-STRING
                   ELSE
      *               IF IVVC0211-FL-AREA-VAR-EXTRA-SI
      *
      *                  PERFORM A951-GESTIONE-VARIABILI-EXTRA
      *                     THRU A951-EX
      *
      *               ELSE
                        PERFORM VARYING IND-COL FROM 1 BY 1
                          UNTIL IND-COL >
                                IVVC0216-ELE-VARIABILI-MAX-P(IND-RIGA)
                              OR IND-COL > IVVC0216-NUM-MAX-VARIABILI-P
                          MOVE IVVC0216-TAB-VARIABILI-P
                               (IND-RIGA, IND-COL)
                            TO C216-TAB-VARIABILI-P(IND-RIGA, IND-COL)
                         END-PERFORM
                      END-IF

                      MOVE IVVC0216-NOME-SERVIZIO-P(IND-RIGA)
                        TO C216-NOME-SERVIZIO-P(IND-RIGA)
      *            END-IF

           END-PERFORM.

           PERFORM VARYING IND-RIGA FROM 1 BY 1
                   UNTIL IND-RIGA > IVVC0216-ELE-LIVELLO-MAX-T
                      OR IND-RIGA > IVVC0216-NUM-MAX-LIVELLI-T
                      OR NOT IVVC0211-SUCCESSFUL-RC
                   MOVE IVVC0216-ID-GAR-T(IND-RIGA)
                     TO C216-ID-GAR-T(IND-RIGA)
                   MOVE IVVC0216-COD-TIPO-OPZIONE-T(IND-RIGA)
                     TO C216-COD-TIPO-OPZIONE-T(IND-RIGA)
                   MOVE IVVC0216-TP-LIVELLO-T(IND-RIGA)
                     TO C216-TP-LIVELLO-T(IND-RIGA)
                   MOVE IVVC0216-COD-LIVELLO-T(IND-RIGA)
                     TO C216-COD-LIVELLO-T(IND-RIGA)
      *  INFORMAZIONI DI TRANCHE
                   MOVE IVVC0216-ID-LIVELLO-T(IND-RIGA)
                     TO C216-ID-LIVELLO-T(IND-RIGA)
                   MOVE IVVC0216-DT-INIZ-TARI-T(IND-RIGA)
                     TO C216-DT-INIZ-TARI-T(IND-RIGA)
                   MOVE IVVC0216-DT-DECOR-TRCH-T(IND-RIGA)
                     TO C216-DT-DECOR-TRCH-T(IND-RIGA)
                   MOVE IVVC0216-COD-RGM-FISC-T(IND-RIGA)
                     TO C216-COD-RGM-FISC-T(IND-RIGA)
                   MOVE IVVC0216-TIPO-TRCH(IND-RIGA)
                     TO C216-TIPO-TRCH(IND-RIGA)
                   MOVE IVVC0216-FLG-ITN(IND-RIGA)
                     TO C216-FLG-ITN(IND-RIGA)
      * AREA VARIABILI PRODOTTO
                   MOVE IVVC0216-ELE-VARIABILI-MAX-T(IND-RIGA)
                     TO C216-ELE-VARIABILI-MAX-T(IND-RIGA)
                        WK-ELE-VARIABILI-MAX

      *   CONTROLLO DI OVERFLOW IN CASO DI VENDITA O
      *   POST-VENDITA SUL NUMERO DI VARIABILI RESTITUITE
                   IF  (IVVC0216-ELE-VARIABILI-MAX-T(IND-RIGA) >
                        IVVC0216-NUM-MAX-VARIABILI-T )
                      SET IVVC0211-GENERIC-ERROR TO TRUE
                      MOVE 'IVVC0216'
                        TO IDSV0003-NOME-TABELLA
                      MOVE WK-PGM            TO IVVC0211-COD-SERVIZIO-BE
                      STRING 'OVERFLOW TABELLA IVVC0216-AREA-VARIABILI'
                             ' IN FASE DI ' IVVC0216-FLG-AREA
                             ' - IVVC0216-ELE-VARIABILI-MAX :'
                              WK-ELE-VARIABILI-MAX
                      DELIMITED BY SIZE INTO IVVC0211-DESCRIZ-ERR
                      END-STRING
                   ELSE
                     PERFORM VARYING IND-COL FROM 1 BY 1
                       UNTIL IND-COL >
                             IVVC0216-ELE-VARIABILI-MAX-T(IND-RIGA)
                           OR IND-COL > IVVC0216-NUM-MAX-VARIABILI-T
                       MOVE IVVC0216-TAB-VARIABILI-T
                            (IND-RIGA, IND-COL)
                         TO C216-TAB-VARIABILI-T(IND-RIGA, IND-COL)
                      END-PERFORM
                   END-IF

                   MOVE IVVC0216-NOME-SERVIZIO-T(IND-RIGA)
                     TO C216-NOME-SERVIZIO-T(IND-RIGA)

           END-PERFORM.

      *AREA VARIABILI AUTONOMIA OPERATIVA
           MOVE IVVC0216-VAR-AUT-OPER  TO C216-VAR-AUT-OPER.

       A950-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE STRUTTURA DATI VARIABILI EXTRA
      *----------------------------------------------------------------*
      *A951-GESTIONE-VARIABILI-EXTRA.
      *
      *    PERFORM VARYING IND-COL FROM 1 BY 1
      *      UNTIL IND-COL >
      *           IVVC0216-ELE-VARIABILI-MAX(IND-RIGA)
      *         OR IND-COL > 86
      *
      *         INITIALIZE C216-AREA-VAR-EXTRA(IND-RIGA, IND-COL)
      *
      *         MOVE IVVC0216-COD-VARIABILE(IND-RIGA, IND-COL)
      *           TO C216-COD-VAR-EXTRA(IND-RIGA, IND-COL)
      *         MOVE IVVC0216-TP-DATO(IND-RIGA, IND-COL)
      *           TO C216-TP-DATO-EXTRA(IND-RIGA, IND-COL)
      *
      *
      *         EVALUATE IVVC0216-TP-DATO(IND-RIGA, IND-COL)
      *             WHEN 'A'
      *             WHEN 'N'
      *             WHEN 'I'
      *                  MOVE IVVC0216-VAL-IMP(IND-RIGA, IND-COL)
      *                    TO C216-VAL-IMP-EXTRA(IND-RIGA, IND-COL)
      *
      *             WHEN 'P'
      *             WHEN 'M'
      *                  MOVE IVVC0216-VAL-PERC(IND-RIGA, IND-COL)
      *                    TO C216-VAL-PERC-EXTRA(IND-RIGA, IND-COL)
      *
      *             WHEN 'D'
      *             WHEN 'S'
      *             WHEN 'X'
      *                  MOVE IVVC0216-VAL-STR(IND-RIGA, IND-COL)
      *                    TO C216-VAL-STR-EXTRA(IND-RIGA, IND-COL)
      *         END-EVALUATE
      *
      *    END-PERFORM.
      *
      *A951-EX.
      *    EXIT.
      *----------------------------------------------------------------*
      *    LETTURA SULLA TABELLA MATRICE MOVIMENTO
      *----------------------------------------------------------------*
       T000-TP-MOVI-PTF-TO-ACT.

           MOVE IVVC0211-COD-COMPAGNIA-ANIA
                                        TO IDSV0001-COD-COMPAGNIA-ANIA

           MOVE IVVC0211-TIPO-MOVIMENTO TO LCCV0021-TP-MOV-PTF

           MOVE IVVC0211-TRATTAMENTO-STORICITA
                                   TO IDSV0001-TRATTAMENTO-STORICITA

           MOVE IVVC0211-FORMATO-DATA-DB
                                   TO IDSV0001-FORMATO-DATA-DB

           CALL PGM-LCCS0020            USING AREA-IDSV0001 LCCV0021.

           MOVE LCCV0021-RETURN-CODE    TO IVVC0211-RETURN-CODE
           MOVE LCCV0021-CAMPI-ESITO    TO IVVC0211-CAMPI-ESITO.

       T000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICA GARANZIE OPZIONE
      *----------------------------------------------------------------*
       V000-VERIFICA-OPZ-POL-GAR.

      *--> CONTROLLO FLAG-GARANZIE-OPZIONI
           IF IVVC0211-FLAG-GAR-OPZIONE-SI
              PERFORM V100-VERIFICA-GAR-OPZIONE THRU V100-EX
           END-IF

           IF IVVC0211-SUCCESSFUL-RC
      *--> CONTROLLO POLIZZA
              PERFORM V200-VERIFICA-POLIZZE     THRU V200-EX
           END-IF.

       V000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICA GARANZIE OPZIONE
      *----------------------------------------------------------------*
       V100-VERIFICA-GAR-OPZIONE.

           MOVE   IVVC0218-ALIAS-OPZIONI   TO WS-ALIAS
           PERFORM V999-TROVA-AREA         THRU V999-EX

           IF AREA-TROVATA-NO
               SET IVVC0211-AREA-NOT-FOUND TO TRUE
               MOVE WK-PGM             TO IVVC0211-COD-SERVIZIO-BE

               STRING 'OPZIONI NON TROVATE'
                      DELIMITED BY SIZE INTO
                      IVVC0211-DESCRIZ-ERR
               END-STRING
           ELSE
               SET IND-STR                  TO IVVC0211-SEARCH-IND
               PERFORM V888-VERIFICA-BUFFER-DATI THRU V888-EX

               IF BUFFER-DATI-VALIDO-NO
                  SET IVVC0211-BUFFER-NOT-VALID TO TRUE
                  MOVE WK-PGM               TO IVVC0211-COD-SERVIZIO-BE

                  STRING 'BUFFER DI OPZIONE NON VALIDO'
                         DELIMITED BY SIZE INTO
                         IVVC0211-DESCRIZ-ERR
                  END-STRING
               END-IF
           END-IF.

       V100-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICA POLIZZE
      *----------------------------------------------------------------*
       V200-VERIFICA-POLIZZE.
      *
           MOVE   IVVC0218-ALIAS-POLI      TO WS-ALIAS
           PERFORM V999-TROVA-AREA         THRU V999-EX

           IF AREA-TROVATA-NO
               SET IVVC0211-AREA-NOT-FOUND TO TRUE
               MOVE WK-PGM             TO IVVC0211-COD-SERVIZIO-BE

               STRING 'POLIZZE NON TROVATE'
                      DELIMITED BY SIZE INTO
                      IVVC0211-DESCRIZ-ERR
               END-STRING
           ELSE
               SET IND-STR            TO IVVC0211-SEARCH-IND
               PERFORM V888-VERIFICA-BUFFER-DATI THRU V888-EX

               IF BUFFER-DATI-VALIDO-NO
                  SET IVVC0211-BUFFER-NOT-VALID TO TRUE
                  MOVE WK-PGM               TO IVVC0211-COD-SERVIZIO-BE

                  STRING 'BUFFER DI POLIZZA NON VALIDO'
                         DELIMITED BY SIZE INTO
                         IVVC0211-DESCRIZ-ERR
                  END-STRING
               END-IF
           END-IF.

       V200-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICA GARANZIE OPZIONE
      *----------------------------------------------------------------*
       V888-VERIFICA-BUFFER-DATI.

           SET BUFFER-DATI-VALIDO-SI        TO TRUE

           IF IVVC0211-BUFFER-DATI  (IVVC0211-POSIZ-INI(IND-STR)
                                                 :
                                     IVVC0211-LUNGHEZZA(IND-STR)) =
                             SPACES OR LOW-VALUE OR HIGH-VALUE
              SET BUFFER-DATI-VALIDO-NO     TO TRUE
           END-IF.

       V888-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICA AREE
      *----------------------------------------------------------------*
       V999-TROVA-AREA.

           SET IVVC0211-SEARCH-IND        TO 1
           SEARCH  IVVC0211-TAB-INFO
                   AT END
                   SET AREA-TROVATA-NO    TO TRUE

              WHEN IVVC0211-TAB-ALIAS(IVVC0211-SEARCH-IND) = WS-ALIAS
                   SET AREA-TROVATA-SI    TO TRUE

           END-SEARCH.

       V999-EX.
           EXIT.

      *----------------------------------------------------------------*
      * GESTIONE RIASS
      *----------------------------------------------------------------*
       GESTIONE-RIASS.

           MOVE   ZERO       TO ELE-MAX-APP-COD-VAR.

           IF GESTIONE-PRODOTTO
              PERFORM VARYING IND-UNZIP FROM 1 BY 1
                     UNTIL IND-UNZIP  >  LIMITE-VARIABILI-UNZIPPED OR
                            IND-UNZIP  >  UNZIP-ELE-VARIABILI-MAX
                     SET TROVATO-APP-NO TO TRUE
                     PERFORM VARYING IND-COD-VAR FROM 1 BY 1
                       UNTIL IND-COD-VAR > ELE-MAX-APP-COD-VAR
                          OR TROVATO-APP-SI
                        IF UNZIP-COD-VARIABILE(IND-UNZIP) =
                           IVVC0216-COD-VARIABILE-P
                           (IND-LIVELLO, IND-COD-VAR)
                           SET TROVATO-APP-SI TO TRUE
                        END-IF
                     END-PERFORM

                     IF TROVATO-APP-NO
                        MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
                        TO IVVC0216-COD-VARIABILE-P
                           (IND-LIVELLO, IND-COD-VAR)
                        ADD 1 TO ELE-MAX-APP-COD-VAR
                     END-IF

              END-PERFORM
           ELSE
              PERFORM VARYING IND-UNZIP FROM 1 BY 1
                     UNTIL IND-UNZIP  >  LIMITE-VARIABILI-UNZIPPED OR
                            IND-UNZIP  >  UNZIP-ELE-VARIABILI-MAX
                     SET TROVATO-APP-NO TO TRUE
                     PERFORM VARYING IND-COD-VAR FROM 1 BY 1
                       UNTIL IND-COD-VAR > ELE-MAX-APP-COD-VAR
                          OR TROVATO-APP-SI
                        IF UNZIP-COD-VARIABILE(IND-UNZIP) =
                           IVVC0216-COD-VARIABILE-T
                           (IND-LIVELLO, IND-COD-VAR)
                           SET TROVATO-APP-SI TO TRUE
                        END-IF
                     END-PERFORM

                     IF TROVATO-APP-NO
                        MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
                        TO IVVC0216-COD-VARIABILE-T
                           (IND-LIVELLO, IND-COD-VAR)
                        ADD 1 TO ELE-MAX-APP-COD-VAR
                     END-IF

              END-PERFORM
           END-IF

           MOVE ELE-MAX-APP-COD-VAR TO UNZIP-ELE-VARIABILI-MAX.

       GESTIONE-RIASS-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI GESTIONE DISPLAY
      *----------------------------------------------------------------*
TRACE1     COPY IDSP8884.

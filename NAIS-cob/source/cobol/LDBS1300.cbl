       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LDBS1300 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  30 OTT 2007.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDA250 END-EXEC.
           EXEC SQL INCLUDE IDBVA252 END-EXEC.
           EXEC SQL INCLUDE IDBVA253 END-EXEC.
      *----COPY INPUT
           EXEC SQL INCLUDE LDBV1301 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVA251 END-EXEC.


       PROCEDURE DIVISION USING IDSV0003 PERS.

           MOVE IDSV0003-BUFFER-WHERE-COND        TO LDBV1301.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC

              EVALUATE TRUE
                 WHEN IDSV0003-PRIMARY-KEY
                    PERFORM A200-ELABORA-PK          THRU A200-EX

                 WHEN OTHER
                    SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
              END-EVALUATE

           ELSE

              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'LDBS1300'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'PERS'       TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX

              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.

      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
             SELECT
                ID_PERS
                ,TSTAM_INI_VLDT
                ,TSTAM_END_VLDT
                ,COD_PERS
                ,RIFTO_RETE
                ,COD_PRT_IVA
                ,IND_PVCY_PRSNL
                ,IND_PVCY_CMMRC
                ,IND_PVCY_INDST
                ,DT_NASC_CLI
                ,DT_ACQS_PERS
                ,IND_CLI
                ,COD_CMN
                ,COD_FRM_GIURD
                ,COD_ENTE_PUBB
                ,DEN_RGN_SOC
                ,DEN_SIG_RGN_SOC
                ,IND_ESE_FISC
                ,COD_STAT_CVL
                ,DEN_NOME
                ,DEN_COGN
                ,COD_FISC
                ,IND_SEX
                ,IND_CPCT_GIURD
                ,IND_PORT_HDCP
                ,COD_USER_INS
                ,TSTAM_INS_RIGA
                ,COD_USER_AGGM
                ,TSTAM_AGGM_RIGA
                ,DEN_CMN_NASC_STRN
                ,COD_RAMO_STGR
                ,COD_STGR_ATVT_UIC
                ,COD_RAMO_ATVT_UIC
                ,DT_END_VLDT_PERS
                ,DT_DEAD_PERS
                ,TP_STAT_CLI
                ,DT_BLOC_CLI
                ,COD_PERS_SECOND
                ,ID_SEGMENTAZ_CLI
                ,DT_1A_ATVT
             INTO
                :A25-ID-PERS
               ,:A25-TSTAM-INI-VLDT
               ,:A25-TSTAM-END-VLDT
                :IND-A25-TSTAM-END-VLDT
               ,:A25-COD-PERS
               ,:A25-RIFTO-RETE
                :IND-A25-RIFTO-RETE
               ,:A25-COD-PRT-IVA
                :IND-A25-COD-PRT-IVA
               ,:A25-IND-PVCY-PRSNL
                :IND-A25-IND-PVCY-PRSNL
               ,:A25-IND-PVCY-CMMRC
                :IND-A25-IND-PVCY-CMMRC
               ,:A25-IND-PVCY-INDST
                :IND-A25-IND-PVCY-INDST
               ,:A25-DT-NASC-CLI-DB
                :IND-A25-DT-NASC-CLI
               ,:A25-DT-ACQS-PERS-DB
                :IND-A25-DT-ACQS-PERS
               ,:A25-IND-CLI
                :IND-A25-IND-CLI
               ,:A25-COD-CMN
                :IND-A25-COD-CMN
               ,:A25-COD-FRM-GIURD
                :IND-A25-COD-FRM-GIURD
               ,:A25-COD-ENTE-PUBB
                :IND-A25-COD-ENTE-PUBB
               ,:A25-DEN-RGN-SOC-VCHAR
                :IND-A25-DEN-RGN-SOC
               ,:A25-DEN-SIG-RGN-SOC-VCHAR
                :IND-A25-DEN-SIG-RGN-SOC
               ,:A25-IND-ESE-FISC
                :IND-A25-IND-ESE-FISC
               ,:A25-COD-STAT-CVL
                :IND-A25-COD-STAT-CVL
               ,:A25-DEN-NOME-VCHAR
                :IND-A25-DEN-NOME
               ,:A25-DEN-COGN-VCHAR
                :IND-A25-DEN-COGN
               ,:A25-COD-FISC
                :IND-A25-COD-FISC
               ,:A25-IND-SEX
                :IND-A25-IND-SEX
               ,:A25-IND-CPCT-GIURD
                :IND-A25-IND-CPCT-GIURD
               ,:A25-IND-PORT-HDCP
                :IND-A25-IND-PORT-HDCP
               ,:A25-COD-USER-INS
               ,:A25-TSTAM-INS-RIGA
               ,:A25-COD-USER-AGGM
                :IND-A25-COD-USER-AGGM
               ,:A25-TSTAM-AGGM-RIGA
                :IND-A25-TSTAM-AGGM-RIGA
               ,:A25-DEN-CMN-NASC-STRN-VCHAR
                :IND-A25-DEN-CMN-NASC-STRN
               ,:A25-COD-RAMO-STGR
                :IND-A25-COD-RAMO-STGR
               ,:A25-COD-STGR-ATVT-UIC
                :IND-A25-COD-STGR-ATVT-UIC
               ,:A25-COD-RAMO-ATVT-UIC
                :IND-A25-COD-RAMO-ATVT-UIC
               ,:A25-DT-END-VLDT-PERS-DB
                :IND-A25-DT-END-VLDT-PERS
               ,:A25-DT-DEAD-PERS-DB
                :IND-A25-DT-DEAD-PERS
               ,:A25-TP-STAT-CLI
                :IND-A25-TP-STAT-CLI
               ,:A25-DT-BLOC-CLI-DB
                :IND-A25-DT-BLOC-CLI
               ,:A25-COD-PERS-SECOND
                :IND-A25-COD-PERS-SECOND
               ,:A25-ID-SEGMENTAZ-CLI
                :IND-A25-ID-SEGMENTAZ-CLI
               ,:A25-DT-1A-ATVT-DB
                :IND-A25-DT-1A-ATVT
               ,:A25-DT-SEGNAL-PARTNER-DB
                :IND-A25-DT-SEGNAL-PARTNER
             FROM PERS
             WHERE ID_PERS          = :LDBV1301-ID-TAB
               AND TSTAM_INI_VLDT  <= :LDBV1301-TIMESTAMP
               AND TSTAM_END_VLDT  >  :LDBV1301-TIMESTAMP
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-A25-TSTAM-END-VLDT = -1
              MOVE HIGH-VALUES TO A25-TSTAM-END-VLDT-NULL
           END-IF
           IF IND-A25-RIFTO-RETE = -1
              MOVE HIGH-VALUES TO A25-RIFTO-RETE-NULL
           END-IF
           IF IND-A25-COD-PRT-IVA = -1
              MOVE HIGH-VALUES TO A25-COD-PRT-IVA-NULL
           END-IF
           IF IND-A25-IND-PVCY-PRSNL = -1
              MOVE HIGH-VALUES TO A25-IND-PVCY-PRSNL-NULL
           END-IF
           IF IND-A25-IND-PVCY-CMMRC = -1
              MOVE HIGH-VALUES TO A25-IND-PVCY-CMMRC-NULL
           END-IF
           IF IND-A25-IND-PVCY-INDST = -1
              MOVE HIGH-VALUES TO A25-IND-PVCY-INDST-NULL
           END-IF
           IF IND-A25-DT-NASC-CLI = -1
              MOVE HIGH-VALUES TO A25-DT-NASC-CLI-NULL
           END-IF
           IF IND-A25-DT-ACQS-PERS = -1
              MOVE HIGH-VALUES TO A25-DT-ACQS-PERS-NULL
           END-IF
           IF IND-A25-IND-CLI = -1
              MOVE HIGH-VALUES TO A25-IND-CLI-NULL
           END-IF
           IF IND-A25-COD-CMN = -1
              MOVE HIGH-VALUES TO A25-COD-CMN-NULL
           END-IF
           IF IND-A25-COD-FRM-GIURD = -1
              MOVE HIGH-VALUES TO A25-COD-FRM-GIURD-NULL
           END-IF
           IF IND-A25-COD-ENTE-PUBB = -1
              MOVE HIGH-VALUES TO A25-COD-ENTE-PUBB-NULL
           END-IF
           IF IND-A25-DEN-RGN-SOC = -1
              MOVE HIGH-VALUES TO A25-DEN-RGN-SOC
           END-IF
           IF IND-A25-DEN-SIG-RGN-SOC = -1
              MOVE HIGH-VALUES TO A25-DEN-SIG-RGN-SOC
           END-IF
           IF IND-A25-IND-ESE-FISC = -1
              MOVE HIGH-VALUES TO A25-IND-ESE-FISC-NULL
           END-IF
           IF IND-A25-COD-STAT-CVL = -1
              MOVE HIGH-VALUES TO A25-COD-STAT-CVL-NULL
           END-IF
           IF IND-A25-DEN-NOME = -1
              MOVE HIGH-VALUES TO A25-DEN-NOME
           END-IF
           IF IND-A25-DEN-COGN = -1
              MOVE HIGH-VALUES TO A25-DEN-COGN
           END-IF
           IF IND-A25-COD-FISC = -1
              MOVE HIGH-VALUES TO A25-COD-FISC-NULL
           END-IF
           IF IND-A25-IND-SEX = -1
              MOVE HIGH-VALUES TO A25-IND-SEX-NULL
           END-IF
           IF IND-A25-IND-CPCT-GIURD = -1
              MOVE HIGH-VALUES TO A25-IND-CPCT-GIURD-NULL
           END-IF
           IF IND-A25-IND-PORT-HDCP = -1
              MOVE HIGH-VALUES TO A25-IND-PORT-HDCP-NULL
           END-IF
           IF IND-A25-COD-USER-AGGM = -1
              MOVE HIGH-VALUES TO A25-COD-USER-AGGM-NULL
           END-IF
           IF IND-A25-TSTAM-AGGM-RIGA = -1
              MOVE HIGH-VALUES TO A25-TSTAM-AGGM-RIGA-NULL
           END-IF
           IF IND-A25-DEN-CMN-NASC-STRN = -1
              MOVE HIGH-VALUES TO A25-DEN-CMN-NASC-STRN
           END-IF
           IF IND-A25-COD-RAMO-STGR = -1
              MOVE HIGH-VALUES TO A25-COD-RAMO-STGR-NULL
           END-IF
           IF IND-A25-COD-STGR-ATVT-UIC = -1
              MOVE HIGH-VALUES TO A25-COD-STGR-ATVT-UIC-NULL
           END-IF
           IF IND-A25-COD-RAMO-ATVT-UIC = -1
              MOVE HIGH-VALUES TO A25-COD-RAMO-ATVT-UIC-NULL
           END-IF
           IF IND-A25-DT-END-VLDT-PERS = -1
              MOVE HIGH-VALUES TO A25-DT-END-VLDT-PERS-NULL
           END-IF
           IF IND-A25-DT-DEAD-PERS = -1
              MOVE HIGH-VALUES TO A25-DT-DEAD-PERS-NULL
           END-IF
           IF IND-A25-TP-STAT-CLI = -1
              MOVE HIGH-VALUES TO A25-TP-STAT-CLI-NULL
           END-IF
           IF IND-A25-DT-BLOC-CLI = -1
              MOVE HIGH-VALUES TO A25-DT-BLOC-CLI-NULL
           END-IF.
           IF IND-A25-COD-PERS-SECOND = -1
              MOVE HIGH-VALUES TO A25-COD-PERS-SECOND-NULL
           END-IF
           IF IND-A25-ID-SEGMENTAZ-CLI = -1
              MOVE HIGH-VALUES TO A25-ID-SEGMENTAZ-CLI-NULL
           END-IF.
           IF IND-A25-DT-1A-ATVT = -1
              MOVE HIGH-VALUES TO A25-DT-1A-ATVT-NULL
           END-IF.
           IF IND-A25-DT-SEGNAL-PARTNER = -1
              MOVE HIGH-VALUES TO A25-DT-SEGNAL-PARTNER-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES.

           CONTINUE.
       Z150-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF A25-TSTAM-END-VLDT-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-TSTAM-END-VLDT
           ELSE
              MOVE 0 TO IND-A25-TSTAM-END-VLDT
           END-IF
           IF A25-RIFTO-RETE-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-RIFTO-RETE
           ELSE
              MOVE 0 TO IND-A25-RIFTO-RETE
           END-IF
           IF A25-COD-PRT-IVA-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-PRT-IVA
           ELSE
              MOVE 0 TO IND-A25-COD-PRT-IVA
           END-IF
           IF A25-IND-PVCY-PRSNL-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-PVCY-PRSNL
           ELSE
              MOVE 0 TO IND-A25-IND-PVCY-PRSNL
           END-IF
           IF A25-IND-PVCY-CMMRC-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-PVCY-CMMRC
           ELSE
              MOVE 0 TO IND-A25-IND-PVCY-CMMRC
           END-IF
           IF A25-IND-PVCY-INDST-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-PVCY-INDST
           ELSE
              MOVE 0 TO IND-A25-IND-PVCY-INDST
           END-IF
           IF A25-DT-NASC-CLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-DT-NASC-CLI
           ELSE
              MOVE 0 TO IND-A25-DT-NASC-CLI
           END-IF
           IF A25-DT-ACQS-PERS-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-DT-ACQS-PERS
           ELSE
              MOVE 0 TO IND-A25-DT-ACQS-PERS
           END-IF
           IF A25-IND-CLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-CLI
           ELSE
              MOVE 0 TO IND-A25-IND-CLI
           END-IF
           IF A25-COD-CMN-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-CMN
           ELSE
              MOVE 0 TO IND-A25-COD-CMN
           END-IF
           IF A25-COD-FRM-GIURD-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-FRM-GIURD
           ELSE
              MOVE 0 TO IND-A25-COD-FRM-GIURD
           END-IF
           IF A25-COD-ENTE-PUBB-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-ENTE-PUBB
           ELSE
              MOVE 0 TO IND-A25-COD-ENTE-PUBB
           END-IF
           IF A25-DEN-RGN-SOC = HIGH-VALUES
              MOVE -1 TO IND-A25-DEN-RGN-SOC
           ELSE
              MOVE 0 TO IND-A25-DEN-RGN-SOC
           END-IF
           IF A25-DEN-SIG-RGN-SOC = HIGH-VALUES
              MOVE -1 TO IND-A25-DEN-SIG-RGN-SOC
           ELSE
              MOVE 0 TO IND-A25-DEN-SIG-RGN-SOC
           END-IF
           IF A25-IND-ESE-FISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-ESE-FISC
           ELSE
              MOVE 0 TO IND-A25-IND-ESE-FISC
           END-IF
           IF A25-COD-STAT-CVL-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-STAT-CVL
           ELSE
              MOVE 0 TO IND-A25-COD-STAT-CVL
           END-IF
           IF A25-DEN-NOME = HIGH-VALUES
              MOVE -1 TO IND-A25-DEN-NOME
           ELSE
              MOVE 0 TO IND-A25-DEN-NOME
           END-IF
           IF A25-DEN-COGN = HIGH-VALUES
              MOVE -1 TO IND-A25-DEN-COGN
           ELSE
              MOVE 0 TO IND-A25-DEN-COGN
           END-IF
           IF A25-COD-FISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-FISC
           ELSE
              MOVE 0 TO IND-A25-COD-FISC
           END-IF
           IF A25-IND-SEX-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-SEX
           ELSE
              MOVE 0 TO IND-A25-IND-SEX
           END-IF
           IF A25-IND-CPCT-GIURD-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-CPCT-GIURD
           ELSE
              MOVE 0 TO IND-A25-IND-CPCT-GIURD
           END-IF
           IF A25-IND-PORT-HDCP-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-IND-PORT-HDCP
           ELSE
              MOVE 0 TO IND-A25-IND-PORT-HDCP
           END-IF
           IF A25-COD-USER-AGGM-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-USER-AGGM
           ELSE
              MOVE 0 TO IND-A25-COD-USER-AGGM
           END-IF
           IF A25-TSTAM-AGGM-RIGA-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-TSTAM-AGGM-RIGA
           ELSE
              MOVE 0 TO IND-A25-TSTAM-AGGM-RIGA
           END-IF
           IF A25-DEN-CMN-NASC-STRN = HIGH-VALUES
              MOVE -1 TO IND-A25-DEN-CMN-NASC-STRN
           ELSE
              MOVE 0 TO IND-A25-DEN-CMN-NASC-STRN
           END-IF
           IF A25-COD-RAMO-STGR-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-RAMO-STGR
           ELSE
              MOVE 0 TO IND-A25-COD-RAMO-STGR
           END-IF
           IF A25-COD-STGR-ATVT-UIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-STGR-ATVT-UIC
           ELSE
              MOVE 0 TO IND-A25-COD-STGR-ATVT-UIC
           END-IF
           IF A25-COD-RAMO-ATVT-UIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-RAMO-ATVT-UIC
           ELSE
              MOVE 0 TO IND-A25-COD-RAMO-ATVT-UIC
           END-IF
           IF A25-DT-END-VLDT-PERS-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-DT-END-VLDT-PERS
           ELSE
              MOVE 0 TO IND-A25-DT-END-VLDT-PERS
           END-IF
           IF A25-DT-DEAD-PERS-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-DT-DEAD-PERS
           ELSE
              MOVE 0 TO IND-A25-DT-DEAD-PERS
           END-IF
           IF A25-TP-STAT-CLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-TP-STAT-CLI
           ELSE
              MOVE 0 TO IND-A25-TP-STAT-CLI
           END-IF
           IF A25-DT-BLOC-CLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-DT-BLOC-CLI
           ELSE
              MOVE 0 TO IND-A25-DT-BLOC-CLI
           END-IF.
           IF A25-COD-PERS-SECOND-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-COD-PERS-SECOND
           ELSE
              MOVE 0 TO IND-A25-COD-PERS-SECOND
           END-IF
           IF A25-ID-SEGMENTAZ-CLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-ID-SEGMENTAZ-CLI
           ELSE
              MOVE 0 TO IND-A25-ID-SEGMENTAZ-CLI
           END-IF.
           IF A25-DT-1A-ATVT-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-DT-1A-ATVT
           ELSE
              MOVE 0 TO IND-A25-DT-1A-ATVT
           END-IF.
           IF A25-DT-SEGNAL-PARTNER-NULL = HIGH-VALUES
              MOVE -1 TO IND-A25-DT-SEGNAL-PARTNER
           ELSE
              MOVE 0 TO IND-A25-DT-SEGNAL-PARTNER
           END-IF.


       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.

           CONTINUE.
       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.
           CONTINUE.
       Z500-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.
           CONTINUE.
       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           IF IND-A25-DT-NASC-CLI = 0
               MOVE A25-DT-NASC-CLI TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO A25-DT-NASC-CLI-DB
           END-IF
           IF IND-A25-DT-ACQS-PERS = 0
               MOVE A25-DT-ACQS-PERS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO A25-DT-ACQS-PERS-DB
           END-IF
           IF IND-A25-DT-END-VLDT-PERS = 0
               MOVE A25-DT-END-VLDT-PERS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO A25-DT-END-VLDT-PERS-DB
           END-IF
           IF IND-A25-DT-DEAD-PERS = 0
               MOVE A25-DT-DEAD-PERS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO A25-DT-DEAD-PERS-DB
           END-IF
           IF IND-A25-DT-BLOC-CLI = 0
               MOVE A25-DT-BLOC-CLI TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO A25-DT-BLOC-CLI-DB
           END-IF.
           IF IND-A25-DT-1A-ATVT = 0
               MOVE A25-DT-1A-ATVT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO A25-DT-1A-ATVT-DB
           END-IF.
           IF IND-A25-DT-SEGNAL-PARTNER = 0
               MOVE A25-DT-SEGNAL-PARTNER TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO A25-DT-SEGNAL-PARTNER-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           IF IND-A25-DT-NASC-CLI = 0
               MOVE A25-DT-NASC-CLI-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO A25-DT-NASC-CLI
           END-IF
           IF IND-A25-DT-ACQS-PERS = 0
               MOVE A25-DT-ACQS-PERS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO A25-DT-ACQS-PERS
           END-IF
           IF IND-A25-DT-END-VLDT-PERS = 0
               MOVE A25-DT-END-VLDT-PERS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO A25-DT-END-VLDT-PERS
           END-IF
           IF IND-A25-DT-DEAD-PERS = 0
               MOVE A25-DT-DEAD-PERS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO A25-DT-DEAD-PERS
           END-IF
           IF IND-A25-DT-BLOC-CLI = 0
               MOVE A25-DT-BLOC-CLI-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO A25-DT-BLOC-CLI
           END-IF.
           IF IND-A25-DT-1A-ATVT = 0
               MOVE A25-DT-1A-ATVT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO A25-DT-1A-ATVT
           END-IF.
           IF IND-A25-DT-SEGNAL-PARTNER = 0
               MOVE A25-DT-SEGNAL-PARTNER-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO A25-DT-SEGNAL-PARTNER
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.


           MOVE LENGTH OF A25-DEN-RGN-SOC
                       TO A25-DEN-RGN-SOC-LEN
           MOVE LENGTH OF A25-DEN-SIG-RGN-SOC
                       TO A25-DEN-SIG-RGN-SOC-LEN
           MOVE LENGTH OF A25-DEN-NOME
                       TO A25-DEN-NOME-LEN
           MOVE LENGTH OF A25-DEN-COGN
                       TO A25-DEN-COGN-LEN
           MOVE LENGTH OF A25-DEN-CMN-NASC-STRN
                       TO A25-DEN-CMN-NASC-STRN-LEN.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.


      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0007.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0007
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0007'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DATA-OUTPUT                  PIC 9(04)V9(07).
       01  WK-DATA-OUTPUT-V REDEFINES WK-DATA-OUTPUT.
           03 WK-NUMERICI                  PIC 9(04).
           03 WK-DECIMALI                  PIC 9(07).

       01 WK-DATA-X-12.
          03 WK-X-AA                          PIC X(04).
          03 WK-VIROGLA                       PIC X(01) VALUE ','.
          03 WK-X-GG                          PIC X(07).

MIGCOL 01 WS-PRE-SOLO-RSH                     PIC S9(12)V9(3) COMP-3.
MIGCOL 01 WS-SOMMA-DIS                        PIC  9(15).
MIGCOL 01 WS-IX-TGA                           PIC  9(09).
MIGCOL 01 WS-IX-TGA-MAX                       PIC  9(09).

      *----------------------------------------------------------------*
      *    COPY DI INPUT PER LA CHIAMATA AL LVVS0000
      *----------------------------------------------------------------*
       01  INPUT-LVVS0000.
           COPY LVVC0000        REPLACING ==(SF)== BY ==LVVC0000==.
      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBS1590
      *---------------------------------------------------------------*
           COPY LDBV1591.
      *---------------------------------------------------------------*
MIGCOL*  COPY PER CHIAMATA LDBS3020
MIGCOL*---------------------------------------------------------------*
MIGCOL     COPY LDBV3021.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-POL.
          03 DPOL-AREA-POLIZZA.
             04 DPOL-ELE-POLI-MAX       PIC S9(04) COMP.
             04 DPOL-TAB-POLI.
             COPY LCCVPOL1              REPLACING ==(SF)== BY ==DPOL==.

MIGCOL* --  AREA TRANCHE
MIGCOL 01 AREA-TRANCHE.
MIGCOL     03 DTGA-AREA-TRANCHE.
MIGCOL        04 DTGA-ELE-TRAN-MAX       PIC S9(04) COMP.
MIGCOL          COPY LCCVTGAC REPLACING   ==(SF)==  BY ==DTGA==.
MIGCOL        COPY LCCVTGA1              REPLACING ==(SF)== BY ==DTGA==.
MIGCOL*
MIGCOL*----------------------------------------------------------------*
MIGCOL*--> ALTRE COPY
MIGCOL*----------------------------------------------------------------*
MIGCOL     COPY LCCVXCA0.
MIGCOL     COPY LCCVXOG0.
MIGCOL     COPY LCCVXSB0.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.
MIGCOL*----------------------------------------------------------------*
MIGCOL* AREA HOST VARIABLES
MIGCOL*----------------------------------------------------------------*
MIGCOL*--> TRANCHE DI GARANZIA
MIGCOL     COPY IDBVTGA1.
MIGCOL*--> DETTAGLIO TITOLI CONTABILI
MIGCOL     COPY IDBVDTC1.
MIGCOL*---------------------------------------------------------------*
MIGCOL* COPY ELE-MAX
MIGCOL*---------------------------------------------------------------*
MIGCOL     COPY LCCVTGAZ.
      *
      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
MIGCOL     03 IX-TAB-TGA                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           INITIALIZE LDBV1591.
MIGCOL     INITIALIZE LDBV3021.
MIGCOL     INITIALIZE WS-PRE-SOLO-RSH.

MIGCOL     PERFORM S00010-INIZIA-TABELLE
MIGCOL        THRU S00010-INIZIA-TABELLE-EX.
      *
           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

MIGCOL*----------------------------------------------------------------*
.     * INIZIALIZZAZIONE TABELLE
.     *----------------------------------------------------------------*
.     *
.      S00010-INIZIA-TABELLE.
.     *
.     * -->TRANCHE DI GARANZIA
.          PERFORM INIZIA-TOT-TGA
.             THRU INIZIA-TOT-TGA-EX
.          VARYING IX-TAB-TGA FROM 1 BY 1
.            UNTIL IX-TAB-TGA > WK-TGA-MAX-C.
.     *
.      S00010-INIZIA-TABELLE-EX.
MIGCOL     EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE WK-DATA-OUTPUT
                      WK-DATA-X-12.


      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
MIGCOL*  INSERITA NUOVA GESTIONE A LIVELLO DI ADESIONE PER LE POLIZZE
MIGCOL*  COLLETTIVE PARTENDO DALLE SINGOLE TRANCHE DI GARANZIA. PER LE
MIGCOL*  INDIVIDUALI LA GESTIONE RESTA INALTERATA A LIVELLO DI POLIZZA.
MIGCOL      IF DPOL-TP-FRM-ASSVA = 'IN'
             EVALUATE DPOL-TP-LIV-GENZ-TIT
               WHEN 'PO'
                 MOVE IVVC0213-ID-ADESIONE       TO LDBV1591-ID-ADES
                 MOVE IVVC0213-ID-POLIZZA        TO LDBV1591-ID-OGG
                 MOVE DPOL-DT-DECOR              TO LDBV1591-DT-DECOR
                 MOVE 'PO'                       TO LDBV1591-TP-OGG
                 MOVE 'IN'                       TO LDBV1591-TP-STAT-TIT
                 PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
               WHEN 'AD'
                 MOVE IVVC0213-ID-ADESIONE       TO LDBV1591-ID-ADES
                 MOVE IVVC0213-ID-ADESIONE       TO LDBV1591-ID-OGG
                 MOVE DPOL-DT-DECOR              TO LDBV1591-DT-DECOR
                 MOVE 'AD'                       TO LDBV1591-TP-OGG
                 MOVE 'IN'                       TO LDBV1591-TP-STAT-TIT
                 PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
             END-EVALUATE
MIGCOL      ELSE
MIGCOL       PERFORM S1300-ELABORA-COLL              THRU S1300-EX
MIGCOL      END-IF
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-POLI
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPOL-AREA-POLIZZA
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATA 1
      *----------------------------------------------------------------*
       S1250-CALCOLA-DATA1.
      *
           MOVE 'LDBS1590'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 LDBV1591
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS1590 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
ALEX  *     IF IVVC0213-ID-POLIZZA = 2071263
ALEX  *       MOVE ZERO                    TO IVVC0213-VAL-IMP-O
ALEX  *     ELSE
              MOVE LDBV1591-IMP-TOT        TO IVVC0213-VAL-IMP-O
ALEX  *     END-IF
           ELSE
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBS1590 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
           END-IF.
      *

      *
       S1250-EX.
           EXIT.
MIGCOL*----------------------------------------------------------------*
.     *  GESTIONE CALCOLO PER COLLETTIVE : LEGGI A LIVELLO DI ADESIONE
.     *  LE TRANCHE IN VIGORE E PER OGNUNO CALCOLA LA SOMMA DEL PREMIO
.     *  DI RISCHIO PER TUTTI I DETTAGLI TITOLI
.     *----------------------------------------------------------------*
.      S1300-ELABORA-COLL.
.     *
.     * --> RECUPERA TRANCHE
.          PERFORM S1310-LEGGI-TRCH      THRU S1310-EX

.     * --> CALCOLA VALORE DA DETTAGLIO TITOLI
.          PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
.             UNTIL IX-TAB-TGA > DTGA-ELE-TRAN-MAX
              OR NOT IDSV0003-SUCCESSFUL-RC
.                PERFORM S1320-LEGGI-DTC
.                   THRU S1320-EX
.          END-PERFORM

.          IF IDSV0003-SUCCESSFUL-RC
.     * --> PASSA VALORE CALCOLATO NELLA VARIABILE DI RIFERIMENTO
.            MOVE WS-PRE-SOLO-RSH    TO IVVC0213-VAL-IMP-O
.          ELSE
.            INITIALIZE                 IVVC0213-VAL-IMP-O
.          END-IF.
TOGLI      MOVE WS-PRE-SOLO-RSH       TO WS-SOMMA-DIS.
.     *
.      S1300-EX.
.          EXIT.
.     *----------------------------------------------------------------*
.     *  LETTURA TRANCHE DI GARANZIA
.     *----------------------------------------------------------------*
.      S1310-LEGGI-TRCH.
.     *
.          INITIALIZE LDBV3021.
.          INITIALIZE WS-PRE-SOLO-RSH.
.          MOVE 0  TO IX-TAB-TGA
                      DTGA-ELE-TRAN-MAX.

.          MOVE IVVC0213-ID-ADESIONE     TO LDBV3021-ID-OGG.
.          SET TRANCHE                   TO TRUE.
.          MOVE WS-TP-OGG                TO LDBV3021-TP-OGG.
.          SET IN-VIGORE                 TO TRUE.
.          MOVE WS-TP-STAT-BUS           TO LDBV3021-TP-STAT-BUS.
.          SET ATTESA-PERFEZIONAMENTO    TO TRUE.
.          MOVE WS-TP-CAUS               TO LDBV3021-TP-CAUS-BUS1.

.          MOVE LDBV3021                 TO IDSV0003-BUFFER-WHERE-COND.

.     *--> TIPO OPERAZIONE
.          SET IDSV0003-FETCH-FIRST         TO TRUE.
.          SET IDSV0003-WHERE-CONDITION     TO TRUE
.          SET IDSV0003-TRATT-X-COMPETENZA  TO TRUE

.     *--> INIZIALIZZA CODICE DI RITORNO
.          SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
.          SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.

.          MOVE 'LDBS3020'                TO WK-CALL-PGM.
.     *--> LIVELLO OPERAZIONE

.          PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
.                     OR NOT IDSV0003-SUCCESSFUL-SQL

.             CALL WK-CALL-PGM  USING  IDSV0003 TRCH-DI-GAR
.             ON EXCEPTION
.                MOVE WK-CALL-PGM
.                  TO IDSV0003-COD-SERVIZIO-BE
.                MOVE 'ERRORE CALL LDBS3020 FETCH'
.                  TO IDSV0003-DESCRIZ-ERR-DB2
.                SET IDSV0003-INVALID-OPER TO TRUE
.             END-CALL
.     *
.             IF IDSV0003-SUCCESSFUL-RC

.                EVALUATE TRUE

.                    WHEN IDSV0003-NOT-FOUND
.     *-->    NESSUN DATO IN TABELLA
.                         CONTINUE

.                    WHEN IDSV0003-SUCCESSFUL-SQL
.     *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
.                         ADD 1       TO IX-TAB-TGA
.                         PERFORM VALORIZZA-OUTPUT-TGA
.                            THRU VALORIZZA-OUTPUT-TGA-EX
.                         MOVE IX-TAB-TGA
.                           TO DTGA-ELE-TRAN-MAX
.                         SET IDSV0003-FETCH-NEXT TO TRUE
TOGLI                     MOVE IX-TAB-TGA  TO WS-IX-TGA
TOGLI                     MOVE DTGA-ELE-TRAN-MAX TO WS-IX-TGA-MAX

.                    WHEN OTHER
.     *--->   ERRORE DI ACCESSO AL DB
.                         SET IDSV0003-INVALID-OPER  TO TRUE
.                         MOVE WK-PGM
.                           TO IDSV0003-COD-SERVIZIO-BE
.                         STRING 'ERRORE LETTURA TABELLA TRCH/STB ;'
.                                IDSV0003-RETURN-CODE ';'
.                                IDSV0003-SQLCODE
.                                DELIMITED BY SIZE INTO
.                                IDSV0003-DESCRIZ-ERR-DB2
.                         END-STRING

.                END-EVALUATE

.             END-IF

.          END-PERFORM.

.      S1310-EX.
.          EXIT.
.     *----------------------------------------------------------------*
.     *   RECUPERA DETTAGLIO TITOLI
.     *----------------------------------------------------------------*
.      S1320-LEGGI-DTC.
.     *
.          INITIALIZE DETT-TIT-CONT.
.          MOVE DTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) TO DTC-ID-OGG.

.          SET TRANCHE                 TO TRUE.
.          MOVE WS-TP-OGG              TO DTC-TP-OGG.

.     *--> TIPO OPERAZIONE
.          SET IDSV0003-FETCH-FIRST         TO TRUE.
.          SET IDSV0003-ID-OGGETTO          TO TRUE.
.          SET IDSV0003-TRATT-X-COMPETENZA  TO TRUE

.     *--> INIZIALIZZA CODICE DI RITORNO
.          SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
.          SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.

.          MOVE 'IDBSDTC0'                TO WK-CALL-PGM.
.     *--> LIVELLO OPERAZIONE

.          PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
.                     OR NOT IDSV0003-SUCCESSFUL-SQL

.             CALL WK-CALL-PGM  USING  IDSV0003 DETT-TIT-CONT
.             ON EXCEPTION
.                MOVE WK-CALL-PGM
.                  TO IDSV0003-COD-SERVIZIO-BE
.                MOVE 'ERRORE CALL IDBSDTC0 FETCH'
.                  TO IDSV0003-DESCRIZ-ERR-DB2
.                SET IDSV0003-INVALID-OPER TO TRUE
.             END-CALL
.     *
.             IF IDSV0003-SUCCESSFUL-RC

.                EVALUATE TRUE

.                    WHEN IDSV0003-NOT-FOUND
.     *-->    NESSUN DATO IN TABELLA
.                         CONTINUE

.                    WHEN IDSV0003-SUCCESSFUL-SQL
.     *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
.                         IF DTC-PRE-SOLO-RSH-NULL NOT = HIGH-VALUES
.                            ADD DTC-PRE-SOLO-RSH
.                             TO WS-PRE-SOLO-RSH
TOGLI                      MOVE WS-PRE-SOLO-RSH TO WS-SOMMA-DIS
                          END-IF
.                         SET IDSV0003-FETCH-NEXT TO TRUE

.                    WHEN OTHER
.     *--->   ERRORE DI ACCESSO AL DB
.                         SET IDSV0003-INVALID-OPER  TO TRUE
.                         MOVE WK-PGM
.                           TO IDSV0003-COD-SERVIZIO-BE
.                         STRING 'ERRORE LETTURA TABE DETT_TIT_CONT ;'
.                                IDSV0003-RETURN-CODE ';'
.                                IDSV0003-SQLCODE
.                                DELIMITED BY SIZE INTO
.                                IDSV0003-DESCRIZ-ERR-DB2
.                         END-STRING

.                END-EVALUATE

.             END-IF

.          END-PERFORM.

.     *
.      S1320-EX.
MIGCOL     EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.
MIGCOL*----------------------------------------------------------------*
MIGCOL*    COPY DI VALORIZZAZIONE AREE DI OUTPUT
MIGCOL*----------------------------------------------------------------*
MIGCOL* --> TRANCHE DI GARANZIA
MIGCOL     COPY LCCVTGA3                 REPLACING ==(SF)== BY ==DTGA==.
MIGCOL     COPY LCCVTGA4                 REPLACING ==(SF)== BY ==DTGA==.


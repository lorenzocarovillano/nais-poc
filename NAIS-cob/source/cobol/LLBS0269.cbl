      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA VER. 1.0                          **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LLBS0269.
       AUTHOR.             NOMOS SISTEMA.
       DATE-WRITTEN.       2008.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *    PROGRAMMA ..... LLBS0269
      *    TIPOLOGIA...... SERVIZIO
      *    PROCESSO....... XXX
      *    FUNZIONE....... XXX
      *    DESCRIZIONE.... RECUPERA IL TS DI COMPETENZA PRECEDENTE
      *    PAGINA WEB..... LEGGE & BILANCIO - CALCOLO DELLE RISERVE
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       DATA DIVISION.
       FILE SECTION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    VARIABILI DI WORKING
      *----------------------------------------------------------------*
       01 WK-PGM                            PIC X(008) VALUE 'LLBS0269'.
      *----------------------------------------------------------------*
      *    COPY LLBV0000 - DICHIARAZIONE VARIABILI DI WORKING COMUNI
      *----------------------------------------------------------------*
           COPY LLBV0000.
      *----------------------------------------------------------------*
      *    DEFINIZIONE DI INDICI
      *----------------------------------------------------------------*
      *01  IX-INDICI.
      *----------------------------------------------------------------*
      *    VARIABILI DI WORKING
      *----------------------------------------------------------------*
      *01  WS-VARIABILI-WORKING.

       01  WK-TS-DATA-MIN                    PIC 9(18)  VALUE
                                                     190001010000000000.
      *----------------------------------------------------------------*
      *    FLAG
      *----------------------------------------------------------------*
       01  WS-FLAG-CICLO                 PIC X(001) VALUE SPACES.
           88 WS-FL-FINE-CICLO-SI        VALUE 'S'.
           88 WS-FL-FINE-CICLO-NO        VALUE 'N'.
      ******************************************************************
      *    COPY DISPATCHER
      ******************************************************************
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      ** COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      ** COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *----------------------------------------------------------------*
      *    COPY VARIBILI PER LA GESTIONE ERRORI                        *
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    COPY DELLE TABELLE DB2
      *----------------------------------------------------------------*
           COPY IDBVRIC1.
           COPY IDBVDER1.
      *----------------------------------------------------------------*
      *    MODULI CHIAMATI
      *----------------------------------------------------------------*
           COPY LDBV4511.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *---------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.
      *----------------------------------------------------------------*
      *     COMMAREA SERVIZIO
      *----------------------------------------------------------------*
      *--  COPY PER RECORD DI PRENOTAZIONE                             *
           COPY LLBO0261.

      *-- INTERFACCIA SERVIZIO
       COPY LLBV0269.

      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WLB-REC-PREN
                                LLBV0269.
      *---------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI                                         *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

      *    INIZIALIZZAZIONI
           INITIALIZE                        LLBV0269-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *    RECUPERA IL TS DI COMPETENZA PRECEDENTE, NECESSARIO IN FASE
      *    DI AGGIORNAMENTO, MA VIENE COMUNQUE VALORIZZATO, ANCHE
      *    IN FASE DI ESTRAZIONE
           PERFORM S1002-GESTIONE-TS-COMPET
              THRU EX-S1002.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    GESTIONE TIMESTAMP COMPETENZA PRECEDENTE                    *
      *----------------------------------------------------------------*
       S1002-GESTIONE-TS-COMPET.

           MOVE WLB-TP-RICH             TO WS-TP-RICH

           EVALUATE TRUE

      *       B2 => Aggiornamento Dati Estratti
      *       B6 => Aggiornamento Dati Estratti e Calcolo Riserve
      *       BA => Aggiornamento Calcolo Riserve
              WHEN WS-RICH-AGG-ESTRAZ
              WHEN WS-RICH-AGG-ESTR-E-CALC
              WHEN WS-RICH-AGG-CALC

      *            VALORIZZA IL TIMESTAMP COMPETENZA PRECEDENTE
      *            IN CASO DI AGGIORNAMENTO GLOBALE
                   IF  (WLB-COD-PROD      = SPACES OR HIGH-VALUE)
                   AND (WLB-IB-POLI-FIRST = SPACES OR HIGH-VALUE)
                   AND (WLB-IB-POLI-LAST  = SPACES OR HIGH-VALUE)
                   AND (WLB-IB-ADE-FIRST  = SPACES OR HIGH-VALUE)
                   AND (WLB-IB-ADE-LAST   = SPACES OR HIGH-VALUE)
                      PERFORM S1003-RECUPERO-TS-COMP
                         THRU EX-S1003
      *            SE STIAMO ESEGUENDO UN AGGIORNAMENTO PARZIALE
      *            (CIOE' I CAMPI "PRODOTTO", "RANGE POLIZZE" E
      *            "RANGE ADESIONI" SONO VALORIZZATI) IL TIMESTAMP
      *            COMPETENZA PRECEDENTE DOVRA' ESSERE UGUALE
      *            ALLA "DATA MINIMA" (DATA PIU' VECCHIA)
                   ELSE
                      MOVE WK-TS-DATA-MIN        TO LLBV0269-TS-PRECED
                   END-IF

              WHEN OTHER
                   MOVE WK-TS-DATA-MIN           TO LLBV0269-TS-PRECED

           END-EVALUATE.

       EX-S1002.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA TS COMPETENZA PRECEDENTE
      *----------------------------------------------------------------*
       S1003-RECUPERO-TS-COMP.

           SET WS-FL-FINE-CICLO-NO       TO TRUE

           INITIALIZE                    RICH

           MOVE WLB-ID-RICH-COLL         TO RIC-ID-RICH

      *    PRENDO IN CONSIDERAZIONE SOLO LE PRENOTAZIONI "ESEGUITE"
           SET WS-PREN-ST-ESEGUITA       TO TRUE
           MOVE WS-STATI-PRENOTAZIONE    TO LDBV4511-STATO-ELAB-00

           SET WS-RICH-ESTRAZ-MASS       TO TRUE
           MOVE WS-TP-RICH               TO LDBV4511-TP-RICH-00.
           SET WS-RICH-AGG-ESTRAZ        TO TRUE
           MOVE WS-TP-RICH               TO LDBV4511-TP-RICH-01.
           SET WS-RICH-CALC-RISERV       TO TRUE
           MOVE WS-TP-RICH               TO LDBV4511-TP-RICH-02.

           MOVE RICH                     TO IDSI0011-BUFFER-DATI.
           MOVE 'LDBS4510'               TO IDSI0011-CODICE-STR-DATO
           MOVE LDBV4511                 TO IDSI0011-BUFFER-WHERE-COND

           MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA

           SET IDSO0011-SUCCESSFUL-RC    TO TRUE
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE

           SET IDSI0011-TRATT-SENZA-STOR TO TRUE
           SET IDSI0011-WHERE-CONDITION  TO TRUE
           SET IDSI0011-FETCH-FIRST      TO TRUE

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR WS-FL-FINE-CICLO-SI

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

      *-->        OPERAZIONE ESEGUITA CON SUCCESSO
                  WHEN IDSO0011-SUCCESSFUL-SQL

                       IF NOT IDSI0011-CLOSE-CURSOR
                          MOVE IDSO0011-BUFFER-DATI
                            TO RICH
                          PERFORM S1500-LEGGI-DETT-RICH
                             THRU EX-S1500
                          SET IDSI0011-CLOSE-CURSOR     TO TRUE
                          MOVE RICH
                            TO IDSI0011-BUFFER-DATI
                          MOVE 'LDBS4510'
                            TO IDSI0011-CODICE-STR-DATO
                          MOVE LDBV4511
                            TO IDSI0011-BUFFER-WHERE-COND
                          SET IDSI0011-TRATT-SENZA-STOR TO TRUE
                          SET IDSI0011-WHERE-CONDITION  TO TRUE
                       ELSE
                          SET WS-FL-FINE-CICLO-SI       TO TRUE
                       END-IF

      *-->        NON TROVATO
                  WHEN IDSO0011-NOT-FOUND
                     IF IDSI0011-FETCH-FIRST
                        MOVE WK-PGM
                          TO IEAI9901-COD-SERVIZIO-BE
                        MOVE 'S1003-RECUPERO-TS-COMP'
                          TO IEAI9901-LABEL-ERR
                        MOVE '001114'
                          TO IEAI9901-COD-ERRORE
                        STRING 'LA RICHIESTA DI ESTRAZIONE MASSIVA '
                               'COLLEGATA DEVE ESSERE IN STATO ESEGUITA'
                        DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                        END-STRING
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                           THRU EX-S0300
                     ELSE
                        SET WS-FL-FINE-CICLO-SI       TO TRUE
                     END-IF

                  WHEN OTHER
      *-->        ERRORE DB
                     MOVE WK-PGM
                       TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1003-RECUPERO-TS-COMP'
                       TO IEAI9901-LABEL-ERR
                     MOVE '005016'
                       TO IEAI9901-COD-ERRORE
                     STRING 'LDBS4510'         ';'
                          IDSO0011-RETURN-CODE ';'
                          IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1003-RECUPERO-TS-COMP'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'LDBS4510'           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF
           END-PERFORM.

       EX-S1003.
           EXIT.

      *----------------------------------------------------------------*
      *   LETTURA DELLA DETTAGLIO RICHIESTA
      *----------------------------------------------------------------*
       S1500-LEGGI-DETT-RICH.

           INITIALIZE DETT-RICH.

           MOVE RIC-ID-RICH              TO DER-ID-RICH.
           MOVE 1                        TO DER-PROG-DETT-RICH

      *--> NOME TABELLA FISICA DB
           MOVE 'DETT-RICH'              TO IDSI0011-CODICE-STR-DATO.

      *--> DCLGEN TABELLA
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE DETT-RICH                TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT              TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR    TO TRUE.
           SET IDSI0011-PRIMARY-KEY         TO TRUE.

      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL      TO TRUE.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *--> CHIAVE NON TROVATA
                     IF IDSI0011-FETCH-FIRST
                        MOVE WK-PGM
                          TO IEAI9901-COD-SERVIZIO-BE
                        MOVE 'S1003-RECUPERO-TS-COMP'
                          TO IEAI9901-LABEL-ERR
                        MOVE '001114'
                          TO IEAI9901-COD-ERRORE
                        STRING 'DETTAGLIO RICHIESTA NON TROVATA'
                        DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                        END-STRING
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                           THRU EX-S0300
                     END-IF

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI
                         TO DETT-RICH
                       MOVE SPACES
                         TO WLB-REC-PREN
                       MOVE DER-AREA-D-RICH
                         TO WLB-REC-PREN
                       MOVE WLB-TP-RICH
                         TO WS-TP-RICH
                       IF (WS-RICH-AGG-ESTRAZ
                       OR  WS-RICH-AGG-ESTR-E-CALC
                       OR  WS-RICH-AGG-CALC)
                       AND (WLB-COD-PROD      = SPACES OR HIGH-VALUE)
                       AND (WLB-IB-POLI-FIRST = SPACES OR HIGH-VALUE)
                       AND (WLB-IB-POLI-LAST  = SPACES OR HIGH-VALUE)
                       AND (WLB-IB-ADE-FIRST  = SPACES OR HIGH-VALUE)
                       AND (WLB-IB-ADE-LAST   = SPACES OR HIGH-VALUE)
                           MOVE RIC-TS-EFF-ESEC-RICH
                             TO LLBV0269-TS-PRECED
                       ELSE
                           IF WS-RICH-ESTRAZ-MASS
                              MOVE SPACES
                                TO WLB-REC-PREN
                              MOVE DER-AREA-D-RICH
                                TO WLB-REC-PREN
                              MOVE RIC-TS-EFF-ESEC-RICH
                                TO LLBV0269-TS-PRECED
      *                    VUOL DIRE CHE E' UN AGGIORNAMENTO PARZIALE
      *                    QUINDI VA SCARTATO E LEGGO
      *                    L'OCCORRENZA SUCCESSIVA
                           ELSE
                              SET IDSI0011-TRATT-SENZA-STOR   TO TRUE
                              SET IDSI0011-WHERE-CONDITION    TO TRUE
                              SET IDSI0011-FETCH-NEXT         TO TRUE
                              MOVE 'LDBS4510'
                                TO IDSI0011-CODICE-STR-DATO
                           END-IF
                       END-IF

                  WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1500-LEGGI-DETT-RICH'
                                    TO IEAI9901-LABEL-ERR
                     MOVE '005016'  TO IEAI9901-COD-ERRORE
                     STRING 'DETT-RICH;'
                            IDSO0011-RETURN-CODE ';'
                            IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
               END-EVALUATE
            ELSE
      *--> ERRORE DISPATCHER

               MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
               MOVE 'S1500-LEGGI-DETT-RICH'
                                       TO IEAI9901-LABEL-ERR
               MOVE '005016'           TO IEAI9901-COD-ERRORE
               STRING 'DETT-RICH;'
                      IDSO0011-RETURN-CODE ';'
                      IDSO0011-SQLCODE
                   DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
               END-STRING
               PERFORM S0300-RICERCA-GRAVITA-ERRORE
                  THRU EX-S0300
            END-IF.

       EX-S1500.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      * ROUTINES GESTIONE ERRORI                                       *
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.

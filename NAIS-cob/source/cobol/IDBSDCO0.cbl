       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSDCO0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  04 SET 2008.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDDCO0 END-EXEC.
           EXEC SQL INCLUDE IDBVDCO2 END-EXEC.
           EXEC SQL INCLUDE IDBVDCO3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVDCO1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 D-COLL.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSDCO0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'D_COLL' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_D_COLL
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IMP_ARROT_PRE
                ,PC_SCON
                ,FL_ADES_SING
                ,TP_IMP
                ,FL_RICL_PRE_DA_CPT
                ,TP_ADES
                ,DT_ULT_RINN_TAC
                ,IMP_SCON
                ,FRAZ_DFLT
                ,ETA_SCAD_MASC_DFLT
                ,ETA_SCAD_FEMM_DFLT
                ,TP_DFLT_DUR
                ,DUR_AA_ADES_DFLT
                ,DUR_MM_ADES_DFLT
                ,DUR_GG_ADES_DFLT
                ,DT_SCAD_ADES_DFLT
                ,COD_FND_DFLT
                ,TP_DUR
                ,TP_CALC_DUR
                ,FL_NO_ADERENTI
                ,FL_DISTINTA_CNBTVA
                ,FL_CNBT_AUTES
                ,FL_QTZ_POST_EMIS
                ,FL_COMNZ_FND_IS
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
             INTO
                :DCO-ID-D-COLL
               ,:DCO-ID-POLI
               ,:DCO-ID-MOVI-CRZ
               ,:DCO-ID-MOVI-CHIU
                :IND-DCO-ID-MOVI-CHIU
               ,:DCO-DT-INI-EFF-DB
               ,:DCO-DT-END-EFF-DB
               ,:DCO-COD-COMP-ANIA
               ,:DCO-IMP-ARROT-PRE
                :IND-DCO-IMP-ARROT-PRE
               ,:DCO-PC-SCON
                :IND-DCO-PC-SCON
               ,:DCO-FL-ADES-SING
                :IND-DCO-FL-ADES-SING
               ,:DCO-TP-IMP
                :IND-DCO-TP-IMP
               ,:DCO-FL-RICL-PRE-DA-CPT
                :IND-DCO-FL-RICL-PRE-DA-CPT
               ,:DCO-TP-ADES
                :IND-DCO-TP-ADES
               ,:DCO-DT-ULT-RINN-TAC-DB
                :IND-DCO-DT-ULT-RINN-TAC
               ,:DCO-IMP-SCON
                :IND-DCO-IMP-SCON
               ,:DCO-FRAZ-DFLT
                :IND-DCO-FRAZ-DFLT
               ,:DCO-ETA-SCAD-MASC-DFLT
                :IND-DCO-ETA-SCAD-MASC-DFLT
               ,:DCO-ETA-SCAD-FEMM-DFLT
                :IND-DCO-ETA-SCAD-FEMM-DFLT
               ,:DCO-TP-DFLT-DUR
                :IND-DCO-TP-DFLT-DUR
               ,:DCO-DUR-AA-ADES-DFLT
                :IND-DCO-DUR-AA-ADES-DFLT
               ,:DCO-DUR-MM-ADES-DFLT
                :IND-DCO-DUR-MM-ADES-DFLT
               ,:DCO-DUR-GG-ADES-DFLT
                :IND-DCO-DUR-GG-ADES-DFLT
               ,:DCO-DT-SCAD-ADES-DFLT-DB
                :IND-DCO-DT-SCAD-ADES-DFLT
               ,:DCO-COD-FND-DFLT
                :IND-DCO-COD-FND-DFLT
               ,:DCO-TP-DUR
                :IND-DCO-TP-DUR
               ,:DCO-TP-CALC-DUR
                :IND-DCO-TP-CALC-DUR
               ,:DCO-FL-NO-ADERENTI
                :IND-DCO-FL-NO-ADERENTI
               ,:DCO-FL-DISTINTA-CNBTVA
                :IND-DCO-FL-DISTINTA-CNBTVA
               ,:DCO-FL-CNBT-AUTES
                :IND-DCO-FL-CNBT-AUTES
               ,:DCO-FL-QTZ-POST-EMIS
                :IND-DCO-FL-QTZ-POST-EMIS
               ,:DCO-FL-COMNZ-FND-IS
                :IND-DCO-FL-COMNZ-FND-IS
               ,:DCO-DS-RIGA
               ,:DCO-DS-OPER-SQL
               ,:DCO-DS-VER
               ,:DCO-DS-TS-INI-CPTZ
               ,:DCO-DS-TS-END-CPTZ
               ,:DCO-DS-UTENTE
               ,:DCO-DS-STATO-ELAB
             FROM D_COLL
             WHERE     DS_RIGA = :DCO-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO D_COLL
                     (
                        ID_D_COLL
                       ,ID_POLI
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,IMP_ARROT_PRE
                       ,PC_SCON
                       ,FL_ADES_SING
                       ,TP_IMP
                       ,FL_RICL_PRE_DA_CPT
                       ,TP_ADES
                       ,DT_ULT_RINN_TAC
                       ,IMP_SCON
                       ,FRAZ_DFLT
                       ,ETA_SCAD_MASC_DFLT
                       ,ETA_SCAD_FEMM_DFLT
                       ,TP_DFLT_DUR
                       ,DUR_AA_ADES_DFLT
                       ,DUR_MM_ADES_DFLT
                       ,DUR_GG_ADES_DFLT
                       ,DT_SCAD_ADES_DFLT
                       ,COD_FND_DFLT
                       ,TP_DUR
                       ,TP_CALC_DUR
                       ,FL_NO_ADERENTI
                       ,FL_DISTINTA_CNBTVA
                       ,FL_CNBT_AUTES
                       ,FL_QTZ_POST_EMIS
                       ,FL_COMNZ_FND_IS
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                     )
                 VALUES
                     (
                       :DCO-ID-D-COLL
                       ,:DCO-ID-POLI
                       ,:DCO-ID-MOVI-CRZ
                       ,:DCO-ID-MOVI-CHIU
                        :IND-DCO-ID-MOVI-CHIU
                       ,:DCO-DT-INI-EFF-DB
                       ,:DCO-DT-END-EFF-DB
                       ,:DCO-COD-COMP-ANIA
                       ,:DCO-IMP-ARROT-PRE
                        :IND-DCO-IMP-ARROT-PRE
                       ,:DCO-PC-SCON
                        :IND-DCO-PC-SCON
                       ,:DCO-FL-ADES-SING
                        :IND-DCO-FL-ADES-SING
                       ,:DCO-TP-IMP
                        :IND-DCO-TP-IMP
                       ,:DCO-FL-RICL-PRE-DA-CPT
                        :IND-DCO-FL-RICL-PRE-DA-CPT
                       ,:DCO-TP-ADES
                        :IND-DCO-TP-ADES
                       ,:DCO-DT-ULT-RINN-TAC-DB
                        :IND-DCO-DT-ULT-RINN-TAC
                       ,:DCO-IMP-SCON
                        :IND-DCO-IMP-SCON
                       ,:DCO-FRAZ-DFLT
                        :IND-DCO-FRAZ-DFLT
                       ,:DCO-ETA-SCAD-MASC-DFLT
                        :IND-DCO-ETA-SCAD-MASC-DFLT
                       ,:DCO-ETA-SCAD-FEMM-DFLT
                        :IND-DCO-ETA-SCAD-FEMM-DFLT
                       ,:DCO-TP-DFLT-DUR
                        :IND-DCO-TP-DFLT-DUR
                       ,:DCO-DUR-AA-ADES-DFLT
                        :IND-DCO-DUR-AA-ADES-DFLT
                       ,:DCO-DUR-MM-ADES-DFLT
                        :IND-DCO-DUR-MM-ADES-DFLT
                       ,:DCO-DUR-GG-ADES-DFLT
                        :IND-DCO-DUR-GG-ADES-DFLT
                       ,:DCO-DT-SCAD-ADES-DFLT-DB
                        :IND-DCO-DT-SCAD-ADES-DFLT
                       ,:DCO-COD-FND-DFLT
                        :IND-DCO-COD-FND-DFLT
                       ,:DCO-TP-DUR
                        :IND-DCO-TP-DUR
                       ,:DCO-TP-CALC-DUR
                        :IND-DCO-TP-CALC-DUR
                       ,:DCO-FL-NO-ADERENTI
                        :IND-DCO-FL-NO-ADERENTI
                       ,:DCO-FL-DISTINTA-CNBTVA
                        :IND-DCO-FL-DISTINTA-CNBTVA
                       ,:DCO-FL-CNBT-AUTES
                        :IND-DCO-FL-CNBT-AUTES
                       ,:DCO-FL-QTZ-POST-EMIS
                        :IND-DCO-FL-QTZ-POST-EMIS
                       ,:DCO-FL-COMNZ-FND-IS
                        :IND-DCO-FL-COMNZ-FND-IS
                       ,:DCO-DS-RIGA
                       ,:DCO-DS-OPER-SQL
                       ,:DCO-DS-VER
                       ,:DCO-DS-TS-INI-CPTZ
                       ,:DCO-DS-TS-END-CPTZ
                       ,:DCO-DS-UTENTE
                       ,:DCO-DS-STATO-ELAB
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE D_COLL SET

                   ID_D_COLL              =
                :DCO-ID-D-COLL
                  ,ID_POLI                =
                :DCO-ID-POLI
                  ,ID_MOVI_CRZ            =
                :DCO-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :DCO-ID-MOVI-CHIU
                                       :IND-DCO-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :DCO-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :DCO-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :DCO-COD-COMP-ANIA
                  ,IMP_ARROT_PRE          =
                :DCO-IMP-ARROT-PRE
                                       :IND-DCO-IMP-ARROT-PRE
                  ,PC_SCON                =
                :DCO-PC-SCON
                                       :IND-DCO-PC-SCON
                  ,FL_ADES_SING           =
                :DCO-FL-ADES-SING
                                       :IND-DCO-FL-ADES-SING
                  ,TP_IMP                 =
                :DCO-TP-IMP
                                       :IND-DCO-TP-IMP
                  ,FL_RICL_PRE_DA_CPT     =
                :DCO-FL-RICL-PRE-DA-CPT
                                       :IND-DCO-FL-RICL-PRE-DA-CPT
                  ,TP_ADES                =
                :DCO-TP-ADES
                                       :IND-DCO-TP-ADES
                  ,DT_ULT_RINN_TAC        =
           :DCO-DT-ULT-RINN-TAC-DB
                                       :IND-DCO-DT-ULT-RINN-TAC
                  ,IMP_SCON               =
                :DCO-IMP-SCON
                                       :IND-DCO-IMP-SCON
                  ,FRAZ_DFLT              =
                :DCO-FRAZ-DFLT
                                       :IND-DCO-FRAZ-DFLT
                  ,ETA_SCAD_MASC_DFLT     =
                :DCO-ETA-SCAD-MASC-DFLT
                                       :IND-DCO-ETA-SCAD-MASC-DFLT
                  ,ETA_SCAD_FEMM_DFLT     =
                :DCO-ETA-SCAD-FEMM-DFLT
                                       :IND-DCO-ETA-SCAD-FEMM-DFLT
                  ,TP_DFLT_DUR            =
                :DCO-TP-DFLT-DUR
                                       :IND-DCO-TP-DFLT-DUR
                  ,DUR_AA_ADES_DFLT       =
                :DCO-DUR-AA-ADES-DFLT
                                       :IND-DCO-DUR-AA-ADES-DFLT
                  ,DUR_MM_ADES_DFLT       =
                :DCO-DUR-MM-ADES-DFLT
                                       :IND-DCO-DUR-MM-ADES-DFLT
                  ,DUR_GG_ADES_DFLT       =
                :DCO-DUR-GG-ADES-DFLT
                                       :IND-DCO-DUR-GG-ADES-DFLT
                  ,DT_SCAD_ADES_DFLT      =
           :DCO-DT-SCAD-ADES-DFLT-DB
                                       :IND-DCO-DT-SCAD-ADES-DFLT
                  ,COD_FND_DFLT           =
                :DCO-COD-FND-DFLT
                                       :IND-DCO-COD-FND-DFLT
                  ,TP_DUR                 =
                :DCO-TP-DUR
                                       :IND-DCO-TP-DUR
                  ,TP_CALC_DUR            =
                :DCO-TP-CALC-DUR
                                       :IND-DCO-TP-CALC-DUR
                  ,FL_NO_ADERENTI         =
                :DCO-FL-NO-ADERENTI
                                       :IND-DCO-FL-NO-ADERENTI
                  ,FL_DISTINTA_CNBTVA     =
                :DCO-FL-DISTINTA-CNBTVA
                                       :IND-DCO-FL-DISTINTA-CNBTVA
                  ,FL_CNBT_AUTES          =
                :DCO-FL-CNBT-AUTES
                                       :IND-DCO-FL-CNBT-AUTES
                  ,FL_QTZ_POST_EMIS       =
                :DCO-FL-QTZ-POST-EMIS
                                       :IND-DCO-FL-QTZ-POST-EMIS
                  ,FL_COMNZ_FND_IS        =
                :DCO-FL-COMNZ-FND-IS
                                       :IND-DCO-FL-COMNZ-FND-IS
                  ,DS_RIGA                =
                :DCO-DS-RIGA
                  ,DS_OPER_SQL            =
                :DCO-DS-OPER-SQL
                  ,DS_VER                 =
                :DCO-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :DCO-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :DCO-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :DCO-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :DCO-DS-STATO-ELAB
                WHERE     DS_RIGA = :DCO-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM D_COLL
                WHERE     DS_RIGA = :DCO-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-DCO CURSOR FOR
              SELECT
                     ID_D_COLL
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IMP_ARROT_PRE
                    ,PC_SCON
                    ,FL_ADES_SING
                    ,TP_IMP
                    ,FL_RICL_PRE_DA_CPT
                    ,TP_ADES
                    ,DT_ULT_RINN_TAC
                    ,IMP_SCON
                    ,FRAZ_DFLT
                    ,ETA_SCAD_MASC_DFLT
                    ,ETA_SCAD_FEMM_DFLT
                    ,TP_DFLT_DUR
                    ,DUR_AA_ADES_DFLT
                    ,DUR_MM_ADES_DFLT
                    ,DUR_GG_ADES_DFLT
                    ,DT_SCAD_ADES_DFLT
                    ,COD_FND_DFLT
                    ,TP_DUR
                    ,TP_CALC_DUR
                    ,FL_NO_ADERENTI
                    ,FL_DISTINTA_CNBTVA
                    ,FL_CNBT_AUTES
                    ,FL_QTZ_POST_EMIS
                    ,FL_COMNZ_FND_IS
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
              FROM D_COLL
              WHERE     ID_D_COLL = :DCO-ID-D-COLL
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC
           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_D_COLL
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IMP_ARROT_PRE
                ,PC_SCON
                ,FL_ADES_SING
                ,TP_IMP
                ,FL_RICL_PRE_DA_CPT
                ,TP_ADES
                ,DT_ULT_RINN_TAC
                ,IMP_SCON
                ,FRAZ_DFLT
                ,ETA_SCAD_MASC_DFLT
                ,ETA_SCAD_FEMM_DFLT
                ,TP_DFLT_DUR
                ,DUR_AA_ADES_DFLT
                ,DUR_MM_ADES_DFLT
                ,DUR_GG_ADES_DFLT
                ,DT_SCAD_ADES_DFLT
                ,COD_FND_DFLT
                ,TP_DUR
                ,TP_CALC_DUR
                ,FL_NO_ADERENTI
                ,FL_DISTINTA_CNBTVA
                ,FL_CNBT_AUTES
                ,FL_QTZ_POST_EMIS
                ,FL_COMNZ_FND_IS
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
             INTO
                :DCO-ID-D-COLL
               ,:DCO-ID-POLI
               ,:DCO-ID-MOVI-CRZ
               ,:DCO-ID-MOVI-CHIU
                :IND-DCO-ID-MOVI-CHIU
               ,:DCO-DT-INI-EFF-DB
               ,:DCO-DT-END-EFF-DB
               ,:DCO-COD-COMP-ANIA
               ,:DCO-IMP-ARROT-PRE
                :IND-DCO-IMP-ARROT-PRE
               ,:DCO-PC-SCON
                :IND-DCO-PC-SCON
               ,:DCO-FL-ADES-SING
                :IND-DCO-FL-ADES-SING
               ,:DCO-TP-IMP
                :IND-DCO-TP-IMP
               ,:DCO-FL-RICL-PRE-DA-CPT
                :IND-DCO-FL-RICL-PRE-DA-CPT
               ,:DCO-TP-ADES
                :IND-DCO-TP-ADES
               ,:DCO-DT-ULT-RINN-TAC-DB
                :IND-DCO-DT-ULT-RINN-TAC
               ,:DCO-IMP-SCON
                :IND-DCO-IMP-SCON
               ,:DCO-FRAZ-DFLT
                :IND-DCO-FRAZ-DFLT
               ,:DCO-ETA-SCAD-MASC-DFLT
                :IND-DCO-ETA-SCAD-MASC-DFLT
               ,:DCO-ETA-SCAD-FEMM-DFLT
                :IND-DCO-ETA-SCAD-FEMM-DFLT
               ,:DCO-TP-DFLT-DUR
                :IND-DCO-TP-DFLT-DUR
               ,:DCO-DUR-AA-ADES-DFLT
                :IND-DCO-DUR-AA-ADES-DFLT
               ,:DCO-DUR-MM-ADES-DFLT
                :IND-DCO-DUR-MM-ADES-DFLT
               ,:DCO-DUR-GG-ADES-DFLT
                :IND-DCO-DUR-GG-ADES-DFLT
               ,:DCO-DT-SCAD-ADES-DFLT-DB
                :IND-DCO-DT-SCAD-ADES-DFLT
               ,:DCO-COD-FND-DFLT
                :IND-DCO-COD-FND-DFLT
               ,:DCO-TP-DUR
                :IND-DCO-TP-DUR
               ,:DCO-TP-CALC-DUR
                :IND-DCO-TP-CALC-DUR
               ,:DCO-FL-NO-ADERENTI
                :IND-DCO-FL-NO-ADERENTI
               ,:DCO-FL-DISTINTA-CNBTVA
                :IND-DCO-FL-DISTINTA-CNBTVA
               ,:DCO-FL-CNBT-AUTES
                :IND-DCO-FL-CNBT-AUTES
               ,:DCO-FL-QTZ-POST-EMIS
                :IND-DCO-FL-QTZ-POST-EMIS
               ,:DCO-FL-COMNZ-FND-IS
                :IND-DCO-FL-COMNZ-FND-IS
               ,:DCO-DS-RIGA
               ,:DCO-DS-OPER-SQL
               ,:DCO-DS-VER
               ,:DCO-DS-TS-INI-CPTZ
               ,:DCO-DS-TS-END-CPTZ
               ,:DCO-DS-UTENTE
               ,:DCO-DS-STATO-ELAB
             FROM D_COLL
             WHERE     ID_D_COLL = :DCO-ID-D-COLL
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE D_COLL SET

                   ID_D_COLL              =
                :DCO-ID-D-COLL
                  ,ID_POLI                =
                :DCO-ID-POLI
                  ,ID_MOVI_CRZ            =
                :DCO-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :DCO-ID-MOVI-CHIU
                                       :IND-DCO-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :DCO-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :DCO-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :DCO-COD-COMP-ANIA
                  ,IMP_ARROT_PRE          =
                :DCO-IMP-ARROT-PRE
                                       :IND-DCO-IMP-ARROT-PRE
                  ,PC_SCON                =
                :DCO-PC-SCON
                                       :IND-DCO-PC-SCON
                  ,FL_ADES_SING           =
                :DCO-FL-ADES-SING
                                       :IND-DCO-FL-ADES-SING
                  ,TP_IMP                 =
                :DCO-TP-IMP
                                       :IND-DCO-TP-IMP
                  ,FL_RICL_PRE_DA_CPT     =
                :DCO-FL-RICL-PRE-DA-CPT
                                       :IND-DCO-FL-RICL-PRE-DA-CPT
                  ,TP_ADES                =
                :DCO-TP-ADES
                                       :IND-DCO-TP-ADES
                  ,DT_ULT_RINN_TAC        =
           :DCO-DT-ULT-RINN-TAC-DB
                                       :IND-DCO-DT-ULT-RINN-TAC
                  ,IMP_SCON               =
                :DCO-IMP-SCON
                                       :IND-DCO-IMP-SCON
                  ,FRAZ_DFLT              =
                :DCO-FRAZ-DFLT
                                       :IND-DCO-FRAZ-DFLT
                  ,ETA_SCAD_MASC_DFLT     =
                :DCO-ETA-SCAD-MASC-DFLT
                                       :IND-DCO-ETA-SCAD-MASC-DFLT
                  ,ETA_SCAD_FEMM_DFLT     =
                :DCO-ETA-SCAD-FEMM-DFLT
                                       :IND-DCO-ETA-SCAD-FEMM-DFLT
                  ,TP_DFLT_DUR            =
                :DCO-TP-DFLT-DUR
                                       :IND-DCO-TP-DFLT-DUR
                  ,DUR_AA_ADES_DFLT       =
                :DCO-DUR-AA-ADES-DFLT
                                       :IND-DCO-DUR-AA-ADES-DFLT
                  ,DUR_MM_ADES_DFLT       =
                :DCO-DUR-MM-ADES-DFLT
                                       :IND-DCO-DUR-MM-ADES-DFLT
                  ,DUR_GG_ADES_DFLT       =
                :DCO-DUR-GG-ADES-DFLT
                                       :IND-DCO-DUR-GG-ADES-DFLT
                  ,DT_SCAD_ADES_DFLT      =
           :DCO-DT-SCAD-ADES-DFLT-DB
                                       :IND-DCO-DT-SCAD-ADES-DFLT
                  ,COD_FND_DFLT           =
                :DCO-COD-FND-DFLT
                                       :IND-DCO-COD-FND-DFLT
                  ,TP_DUR                 =
                :DCO-TP-DUR
                                       :IND-DCO-TP-DUR
                  ,TP_CALC_DUR            =
                :DCO-TP-CALC-DUR
                                       :IND-DCO-TP-CALC-DUR
                  ,FL_NO_ADERENTI         =
                :DCO-FL-NO-ADERENTI
                                       :IND-DCO-FL-NO-ADERENTI
                  ,FL_DISTINTA_CNBTVA     =
                :DCO-FL-DISTINTA-CNBTVA
                                       :IND-DCO-FL-DISTINTA-CNBTVA
                  ,FL_CNBT_AUTES          =
                :DCO-FL-CNBT-AUTES
                                       :IND-DCO-FL-CNBT-AUTES
                  ,FL_QTZ_POST_EMIS       =
                :DCO-FL-QTZ-POST-EMIS
                                       :IND-DCO-FL-QTZ-POST-EMIS
                  ,FL_COMNZ_FND_IS        =
                :DCO-FL-COMNZ-FND-IS
                                       :IND-DCO-FL-COMNZ-FND-IS
                  ,DS_RIGA                =
                :DCO-DS-RIGA
                  ,DS_OPER_SQL            =
                :DCO-DS-OPER-SQL
                  ,DS_VER                 =
                :DCO-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :DCO-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :DCO-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :DCO-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :DCO-DS-STATO-ELAB
                WHERE     DS_RIGA = :DCO-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-DCO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-DCO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-DCO
           INTO
                :DCO-ID-D-COLL
               ,:DCO-ID-POLI
               ,:DCO-ID-MOVI-CRZ
               ,:DCO-ID-MOVI-CHIU
                :IND-DCO-ID-MOVI-CHIU
               ,:DCO-DT-INI-EFF-DB
               ,:DCO-DT-END-EFF-DB
               ,:DCO-COD-COMP-ANIA
               ,:DCO-IMP-ARROT-PRE
                :IND-DCO-IMP-ARROT-PRE
               ,:DCO-PC-SCON
                :IND-DCO-PC-SCON
               ,:DCO-FL-ADES-SING
                :IND-DCO-FL-ADES-SING
               ,:DCO-TP-IMP
                :IND-DCO-TP-IMP
               ,:DCO-FL-RICL-PRE-DA-CPT
                :IND-DCO-FL-RICL-PRE-DA-CPT
               ,:DCO-TP-ADES
                :IND-DCO-TP-ADES
               ,:DCO-DT-ULT-RINN-TAC-DB
                :IND-DCO-DT-ULT-RINN-TAC
               ,:DCO-IMP-SCON
                :IND-DCO-IMP-SCON
               ,:DCO-FRAZ-DFLT
                :IND-DCO-FRAZ-DFLT
               ,:DCO-ETA-SCAD-MASC-DFLT
                :IND-DCO-ETA-SCAD-MASC-DFLT
               ,:DCO-ETA-SCAD-FEMM-DFLT
                :IND-DCO-ETA-SCAD-FEMM-DFLT
               ,:DCO-TP-DFLT-DUR
                :IND-DCO-TP-DFLT-DUR
               ,:DCO-DUR-AA-ADES-DFLT
                :IND-DCO-DUR-AA-ADES-DFLT
               ,:DCO-DUR-MM-ADES-DFLT
                :IND-DCO-DUR-MM-ADES-DFLT
               ,:DCO-DUR-GG-ADES-DFLT
                :IND-DCO-DUR-GG-ADES-DFLT
               ,:DCO-DT-SCAD-ADES-DFLT-DB
                :IND-DCO-DT-SCAD-ADES-DFLT
               ,:DCO-COD-FND-DFLT
                :IND-DCO-COD-FND-DFLT
               ,:DCO-TP-DUR
                :IND-DCO-TP-DUR
               ,:DCO-TP-CALC-DUR
                :IND-DCO-TP-CALC-DUR
               ,:DCO-FL-NO-ADERENTI
                :IND-DCO-FL-NO-ADERENTI
               ,:DCO-FL-DISTINTA-CNBTVA
                :IND-DCO-FL-DISTINTA-CNBTVA
               ,:DCO-FL-CNBT-AUTES
                :IND-DCO-FL-CNBT-AUTES
               ,:DCO-FL-QTZ-POST-EMIS
                :IND-DCO-FL-QTZ-POST-EMIS
               ,:DCO-FL-COMNZ-FND-IS
                :IND-DCO-FL-COMNZ-FND-IS
               ,:DCO-DS-RIGA
               ,:DCO-DS-OPER-SQL
               ,:DCO-DS-VER
               ,:DCO-DS-TS-INI-CPTZ
               ,:DCO-DS-TS-END-CPTZ
               ,:DCO-DS-UTENTE
               ,:DCO-DS-STATO-ELAB
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-EFF-DCO CURSOR FOR
              SELECT
                     ID_D_COLL
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IMP_ARROT_PRE
                    ,PC_SCON
                    ,FL_ADES_SING
                    ,TP_IMP
                    ,FL_RICL_PRE_DA_CPT
                    ,TP_ADES
                    ,DT_ULT_RINN_TAC
                    ,IMP_SCON
                    ,FRAZ_DFLT
                    ,ETA_SCAD_MASC_DFLT
                    ,ETA_SCAD_FEMM_DFLT
                    ,TP_DFLT_DUR
                    ,DUR_AA_ADES_DFLT
                    ,DUR_MM_ADES_DFLT
                    ,DUR_GG_ADES_DFLT
                    ,DT_SCAD_ADES_DFLT
                    ,COD_FND_DFLT
                    ,TP_DUR
                    ,TP_CALC_DUR
                    ,FL_NO_ADERENTI
                    ,FL_DISTINTA_CNBTVA
                    ,FL_CNBT_AUTES
                    ,FL_QTZ_POST_EMIS
                    ,FL_COMNZ_FND_IS
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
              FROM D_COLL
              WHERE     ID_POLI = :DCO-ID-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_D_COLL ASC
           END-EXEC.

       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_D_COLL
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IMP_ARROT_PRE
                ,PC_SCON
                ,FL_ADES_SING
                ,TP_IMP
                ,FL_RICL_PRE_DA_CPT
                ,TP_ADES
                ,DT_ULT_RINN_TAC
                ,IMP_SCON
                ,FRAZ_DFLT
                ,ETA_SCAD_MASC_DFLT
                ,ETA_SCAD_FEMM_DFLT
                ,TP_DFLT_DUR
                ,DUR_AA_ADES_DFLT
                ,DUR_MM_ADES_DFLT
                ,DUR_GG_ADES_DFLT
                ,DT_SCAD_ADES_DFLT
                ,COD_FND_DFLT
                ,TP_DUR
                ,TP_CALC_DUR
                ,FL_NO_ADERENTI
                ,FL_DISTINTA_CNBTVA
                ,FL_CNBT_AUTES
                ,FL_QTZ_POST_EMIS
                ,FL_COMNZ_FND_IS
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
             INTO
                :DCO-ID-D-COLL
               ,:DCO-ID-POLI
               ,:DCO-ID-MOVI-CRZ
               ,:DCO-ID-MOVI-CHIU
                :IND-DCO-ID-MOVI-CHIU
               ,:DCO-DT-INI-EFF-DB
               ,:DCO-DT-END-EFF-DB
               ,:DCO-COD-COMP-ANIA
               ,:DCO-IMP-ARROT-PRE
                :IND-DCO-IMP-ARROT-PRE
               ,:DCO-PC-SCON
                :IND-DCO-PC-SCON
               ,:DCO-FL-ADES-SING
                :IND-DCO-FL-ADES-SING
               ,:DCO-TP-IMP
                :IND-DCO-TP-IMP
               ,:DCO-FL-RICL-PRE-DA-CPT
                :IND-DCO-FL-RICL-PRE-DA-CPT
               ,:DCO-TP-ADES
                :IND-DCO-TP-ADES
               ,:DCO-DT-ULT-RINN-TAC-DB
                :IND-DCO-DT-ULT-RINN-TAC
               ,:DCO-IMP-SCON
                :IND-DCO-IMP-SCON
               ,:DCO-FRAZ-DFLT
                :IND-DCO-FRAZ-DFLT
               ,:DCO-ETA-SCAD-MASC-DFLT
                :IND-DCO-ETA-SCAD-MASC-DFLT
               ,:DCO-ETA-SCAD-FEMM-DFLT
                :IND-DCO-ETA-SCAD-FEMM-DFLT
               ,:DCO-TP-DFLT-DUR
                :IND-DCO-TP-DFLT-DUR
               ,:DCO-DUR-AA-ADES-DFLT
                :IND-DCO-DUR-AA-ADES-DFLT
               ,:DCO-DUR-MM-ADES-DFLT
                :IND-DCO-DUR-MM-ADES-DFLT
               ,:DCO-DUR-GG-ADES-DFLT
                :IND-DCO-DUR-GG-ADES-DFLT
               ,:DCO-DT-SCAD-ADES-DFLT-DB
                :IND-DCO-DT-SCAD-ADES-DFLT
               ,:DCO-COD-FND-DFLT
                :IND-DCO-COD-FND-DFLT
               ,:DCO-TP-DUR
                :IND-DCO-TP-DUR
               ,:DCO-TP-CALC-DUR
                :IND-DCO-TP-CALC-DUR
               ,:DCO-FL-NO-ADERENTI
                :IND-DCO-FL-NO-ADERENTI
               ,:DCO-FL-DISTINTA-CNBTVA
                :IND-DCO-FL-DISTINTA-CNBTVA
               ,:DCO-FL-CNBT-AUTES
                :IND-DCO-FL-CNBT-AUTES
               ,:DCO-FL-QTZ-POST-EMIS
                :IND-DCO-FL-QTZ-POST-EMIS
               ,:DCO-FL-COMNZ-FND-IS
                :IND-DCO-FL-COMNZ-FND-IS
               ,:DCO-DS-RIGA
               ,:DCO-DS-OPER-SQL
               ,:DCO-DS-VER
               ,:DCO-DS-TS-INI-CPTZ
               ,:DCO-DS-TS-END-CPTZ
               ,:DCO-DS-UTENTE
               ,:DCO-DS-STATO-ELAB
             FROM D_COLL
             WHERE     ID_POLI = :DCO-ID-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           EXEC SQL
                OPEN C-IDP-EFF-DCO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           EXEC SQL
                CLOSE C-IDP-EFF-DCO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           EXEC SQL
                FETCH C-IDP-EFF-DCO
           INTO
                :DCO-ID-D-COLL
               ,:DCO-ID-POLI
               ,:DCO-ID-MOVI-CRZ
               ,:DCO-ID-MOVI-CHIU
                :IND-DCO-ID-MOVI-CHIU
               ,:DCO-DT-INI-EFF-DB
               ,:DCO-DT-END-EFF-DB
               ,:DCO-COD-COMP-ANIA
               ,:DCO-IMP-ARROT-PRE
                :IND-DCO-IMP-ARROT-PRE
               ,:DCO-PC-SCON
                :IND-DCO-PC-SCON
               ,:DCO-FL-ADES-SING
                :IND-DCO-FL-ADES-SING
               ,:DCO-TP-IMP
                :IND-DCO-TP-IMP
               ,:DCO-FL-RICL-PRE-DA-CPT
                :IND-DCO-FL-RICL-PRE-DA-CPT
               ,:DCO-TP-ADES
                :IND-DCO-TP-ADES
               ,:DCO-DT-ULT-RINN-TAC-DB
                :IND-DCO-DT-ULT-RINN-TAC
               ,:DCO-IMP-SCON
                :IND-DCO-IMP-SCON
               ,:DCO-FRAZ-DFLT
                :IND-DCO-FRAZ-DFLT
               ,:DCO-ETA-SCAD-MASC-DFLT
                :IND-DCO-ETA-SCAD-MASC-DFLT
               ,:DCO-ETA-SCAD-FEMM-DFLT
                :IND-DCO-ETA-SCAD-FEMM-DFLT
               ,:DCO-TP-DFLT-DUR
                :IND-DCO-TP-DFLT-DUR
               ,:DCO-DUR-AA-ADES-DFLT
                :IND-DCO-DUR-AA-ADES-DFLT
               ,:DCO-DUR-MM-ADES-DFLT
                :IND-DCO-DUR-MM-ADES-DFLT
               ,:DCO-DUR-GG-ADES-DFLT
                :IND-DCO-DUR-GG-ADES-DFLT
               ,:DCO-DT-SCAD-ADES-DFLT-DB
                :IND-DCO-DT-SCAD-ADES-DFLT
               ,:DCO-COD-FND-DFLT
                :IND-DCO-COD-FND-DFLT
               ,:DCO-TP-DUR
                :IND-DCO-TP-DUR
               ,:DCO-TP-CALC-DUR
                :IND-DCO-TP-CALC-DUR
               ,:DCO-FL-NO-ADERENTI
                :IND-DCO-FL-NO-ADERENTI
               ,:DCO-FL-DISTINTA-CNBTVA
                :IND-DCO-FL-DISTINTA-CNBTVA
               ,:DCO-FL-CNBT-AUTES
                :IND-DCO-FL-CNBT-AUTES
               ,:DCO-FL-QTZ-POST-EMIS
                :IND-DCO-FL-QTZ-POST-EMIS
               ,:DCO-FL-COMNZ-FND-IS
                :IND-DCO-FL-COMNZ-FND-IS
               ,:DCO-DS-RIGA
               ,:DCO-DS-OPER-SQL
               ,:DCO-DS-VER
               ,:DCO-DS-TS-INI-CPTZ
               ,:DCO-DS-TS-END-CPTZ
               ,:DCO-DS-UTENTE
               ,:DCO-DS-STATO-ELAB
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_D_COLL
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IMP_ARROT_PRE
                ,PC_SCON
                ,FL_ADES_SING
                ,TP_IMP
                ,FL_RICL_PRE_DA_CPT
                ,TP_ADES
                ,DT_ULT_RINN_TAC
                ,IMP_SCON
                ,FRAZ_DFLT
                ,ETA_SCAD_MASC_DFLT
                ,ETA_SCAD_FEMM_DFLT
                ,TP_DFLT_DUR
                ,DUR_AA_ADES_DFLT
                ,DUR_MM_ADES_DFLT
                ,DUR_GG_ADES_DFLT
                ,DT_SCAD_ADES_DFLT
                ,COD_FND_DFLT
                ,TP_DUR
                ,TP_CALC_DUR
                ,FL_NO_ADERENTI
                ,FL_DISTINTA_CNBTVA
                ,FL_CNBT_AUTES
                ,FL_QTZ_POST_EMIS
                ,FL_COMNZ_FND_IS
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
             INTO
                :DCO-ID-D-COLL
               ,:DCO-ID-POLI
               ,:DCO-ID-MOVI-CRZ
               ,:DCO-ID-MOVI-CHIU
                :IND-DCO-ID-MOVI-CHIU
               ,:DCO-DT-INI-EFF-DB
               ,:DCO-DT-END-EFF-DB
               ,:DCO-COD-COMP-ANIA
               ,:DCO-IMP-ARROT-PRE
                :IND-DCO-IMP-ARROT-PRE
               ,:DCO-PC-SCON
                :IND-DCO-PC-SCON
               ,:DCO-FL-ADES-SING
                :IND-DCO-FL-ADES-SING
               ,:DCO-TP-IMP
                :IND-DCO-TP-IMP
               ,:DCO-FL-RICL-PRE-DA-CPT
                :IND-DCO-FL-RICL-PRE-DA-CPT
               ,:DCO-TP-ADES
                :IND-DCO-TP-ADES
               ,:DCO-DT-ULT-RINN-TAC-DB
                :IND-DCO-DT-ULT-RINN-TAC
               ,:DCO-IMP-SCON
                :IND-DCO-IMP-SCON
               ,:DCO-FRAZ-DFLT
                :IND-DCO-FRAZ-DFLT
               ,:DCO-ETA-SCAD-MASC-DFLT
                :IND-DCO-ETA-SCAD-MASC-DFLT
               ,:DCO-ETA-SCAD-FEMM-DFLT
                :IND-DCO-ETA-SCAD-FEMM-DFLT
               ,:DCO-TP-DFLT-DUR
                :IND-DCO-TP-DFLT-DUR
               ,:DCO-DUR-AA-ADES-DFLT
                :IND-DCO-DUR-AA-ADES-DFLT
               ,:DCO-DUR-MM-ADES-DFLT
                :IND-DCO-DUR-MM-ADES-DFLT
               ,:DCO-DUR-GG-ADES-DFLT
                :IND-DCO-DUR-GG-ADES-DFLT
               ,:DCO-DT-SCAD-ADES-DFLT-DB
                :IND-DCO-DT-SCAD-ADES-DFLT
               ,:DCO-COD-FND-DFLT
                :IND-DCO-COD-FND-DFLT
               ,:DCO-TP-DUR
                :IND-DCO-TP-DUR
               ,:DCO-TP-CALC-DUR
                :IND-DCO-TP-CALC-DUR
               ,:DCO-FL-NO-ADERENTI
                :IND-DCO-FL-NO-ADERENTI
               ,:DCO-FL-DISTINTA-CNBTVA
                :IND-DCO-FL-DISTINTA-CNBTVA
               ,:DCO-FL-CNBT-AUTES
                :IND-DCO-FL-CNBT-AUTES
               ,:DCO-FL-QTZ-POST-EMIS
                :IND-DCO-FL-QTZ-POST-EMIS
               ,:DCO-FL-COMNZ-FND-IS
                :IND-DCO-FL-COMNZ-FND-IS
               ,:DCO-DS-RIGA
               ,:DCO-DS-OPER-SQL
               ,:DCO-DS-VER
               ,:DCO-DS-TS-INI-CPTZ
               ,:DCO-DS-TS-END-CPTZ
               ,:DCO-DS-UTENTE
               ,:DCO-DS-STATO-ELAB
             FROM D_COLL
             WHERE     ID_D_COLL = :DCO-ID-D-COLL
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-CPZ-DCO CURSOR FOR
              SELECT
                     ID_D_COLL
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IMP_ARROT_PRE
                    ,PC_SCON
                    ,FL_ADES_SING
                    ,TP_IMP
                    ,FL_RICL_PRE_DA_CPT
                    ,TP_ADES
                    ,DT_ULT_RINN_TAC
                    ,IMP_SCON
                    ,FRAZ_DFLT
                    ,ETA_SCAD_MASC_DFLT
                    ,ETA_SCAD_FEMM_DFLT
                    ,TP_DFLT_DUR
                    ,DUR_AA_ADES_DFLT
                    ,DUR_MM_ADES_DFLT
                    ,DUR_GG_ADES_DFLT
                    ,DT_SCAD_ADES_DFLT
                    ,COD_FND_DFLT
                    ,TP_DUR
                    ,TP_CALC_DUR
                    ,FL_NO_ADERENTI
                    ,FL_DISTINTA_CNBTVA
                    ,FL_CNBT_AUTES
                    ,FL_QTZ_POST_EMIS
                    ,FL_COMNZ_FND_IS
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
              FROM D_COLL
              WHERE     ID_POLI = :DCO-ID-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_D_COLL ASC
           END-EXEC.

       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_D_COLL
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IMP_ARROT_PRE
                ,PC_SCON
                ,FL_ADES_SING
                ,TP_IMP
                ,FL_RICL_PRE_DA_CPT
                ,TP_ADES
                ,DT_ULT_RINN_TAC
                ,IMP_SCON
                ,FRAZ_DFLT
                ,ETA_SCAD_MASC_DFLT
                ,ETA_SCAD_FEMM_DFLT
                ,TP_DFLT_DUR
                ,DUR_AA_ADES_DFLT
                ,DUR_MM_ADES_DFLT
                ,DUR_GG_ADES_DFLT
                ,DT_SCAD_ADES_DFLT
                ,COD_FND_DFLT
                ,TP_DUR
                ,TP_CALC_DUR
                ,FL_NO_ADERENTI
                ,FL_DISTINTA_CNBTVA
                ,FL_CNBT_AUTES
                ,FL_QTZ_POST_EMIS
                ,FL_COMNZ_FND_IS
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
             INTO
                :DCO-ID-D-COLL
               ,:DCO-ID-POLI
               ,:DCO-ID-MOVI-CRZ
               ,:DCO-ID-MOVI-CHIU
                :IND-DCO-ID-MOVI-CHIU
               ,:DCO-DT-INI-EFF-DB
               ,:DCO-DT-END-EFF-DB
               ,:DCO-COD-COMP-ANIA
               ,:DCO-IMP-ARROT-PRE
                :IND-DCO-IMP-ARROT-PRE
               ,:DCO-PC-SCON
                :IND-DCO-PC-SCON
               ,:DCO-FL-ADES-SING
                :IND-DCO-FL-ADES-SING
               ,:DCO-TP-IMP
                :IND-DCO-TP-IMP
               ,:DCO-FL-RICL-PRE-DA-CPT
                :IND-DCO-FL-RICL-PRE-DA-CPT
               ,:DCO-TP-ADES
                :IND-DCO-TP-ADES
               ,:DCO-DT-ULT-RINN-TAC-DB
                :IND-DCO-DT-ULT-RINN-TAC
               ,:DCO-IMP-SCON
                :IND-DCO-IMP-SCON
               ,:DCO-FRAZ-DFLT
                :IND-DCO-FRAZ-DFLT
               ,:DCO-ETA-SCAD-MASC-DFLT
                :IND-DCO-ETA-SCAD-MASC-DFLT
               ,:DCO-ETA-SCAD-FEMM-DFLT
                :IND-DCO-ETA-SCAD-FEMM-DFLT
               ,:DCO-TP-DFLT-DUR
                :IND-DCO-TP-DFLT-DUR
               ,:DCO-DUR-AA-ADES-DFLT
                :IND-DCO-DUR-AA-ADES-DFLT
               ,:DCO-DUR-MM-ADES-DFLT
                :IND-DCO-DUR-MM-ADES-DFLT
               ,:DCO-DUR-GG-ADES-DFLT
                :IND-DCO-DUR-GG-ADES-DFLT
               ,:DCO-DT-SCAD-ADES-DFLT-DB
                :IND-DCO-DT-SCAD-ADES-DFLT
               ,:DCO-COD-FND-DFLT
                :IND-DCO-COD-FND-DFLT
               ,:DCO-TP-DUR
                :IND-DCO-TP-DUR
               ,:DCO-TP-CALC-DUR
                :IND-DCO-TP-CALC-DUR
               ,:DCO-FL-NO-ADERENTI
                :IND-DCO-FL-NO-ADERENTI
               ,:DCO-FL-DISTINTA-CNBTVA
                :IND-DCO-FL-DISTINTA-CNBTVA
               ,:DCO-FL-CNBT-AUTES
                :IND-DCO-FL-CNBT-AUTES
               ,:DCO-FL-QTZ-POST-EMIS
                :IND-DCO-FL-QTZ-POST-EMIS
               ,:DCO-FL-COMNZ-FND-IS
                :IND-DCO-FL-COMNZ-FND-IS
               ,:DCO-DS-RIGA
               ,:DCO-DS-OPER-SQL
               ,:DCO-DS-VER
               ,:DCO-DS-TS-INI-CPTZ
               ,:DCO-DS-TS-END-CPTZ
               ,:DCO-DS-UTENTE
               ,:DCO-DS-STATO-ELAB
             FROM D_COLL
             WHERE     ID_POLI = :DCO-ID-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           EXEC SQL
                OPEN C-IDP-CPZ-DCO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           EXEC SQL
                CLOSE C-IDP-CPZ-DCO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           EXEC SQL
                FETCH C-IDP-CPZ-DCO
           INTO
                :DCO-ID-D-COLL
               ,:DCO-ID-POLI
               ,:DCO-ID-MOVI-CRZ
               ,:DCO-ID-MOVI-CHIU
                :IND-DCO-ID-MOVI-CHIU
               ,:DCO-DT-INI-EFF-DB
               ,:DCO-DT-END-EFF-DB
               ,:DCO-COD-COMP-ANIA
               ,:DCO-IMP-ARROT-PRE
                :IND-DCO-IMP-ARROT-PRE
               ,:DCO-PC-SCON
                :IND-DCO-PC-SCON
               ,:DCO-FL-ADES-SING
                :IND-DCO-FL-ADES-SING
               ,:DCO-TP-IMP
                :IND-DCO-TP-IMP
               ,:DCO-FL-RICL-PRE-DA-CPT
                :IND-DCO-FL-RICL-PRE-DA-CPT
               ,:DCO-TP-ADES
                :IND-DCO-TP-ADES
               ,:DCO-DT-ULT-RINN-TAC-DB
                :IND-DCO-DT-ULT-RINN-TAC
               ,:DCO-IMP-SCON
                :IND-DCO-IMP-SCON
               ,:DCO-FRAZ-DFLT
                :IND-DCO-FRAZ-DFLT
               ,:DCO-ETA-SCAD-MASC-DFLT
                :IND-DCO-ETA-SCAD-MASC-DFLT
               ,:DCO-ETA-SCAD-FEMM-DFLT
                :IND-DCO-ETA-SCAD-FEMM-DFLT
               ,:DCO-TP-DFLT-DUR
                :IND-DCO-TP-DFLT-DUR
               ,:DCO-DUR-AA-ADES-DFLT
                :IND-DCO-DUR-AA-ADES-DFLT
               ,:DCO-DUR-MM-ADES-DFLT
                :IND-DCO-DUR-MM-ADES-DFLT
               ,:DCO-DUR-GG-ADES-DFLT
                :IND-DCO-DUR-GG-ADES-DFLT
               ,:DCO-DT-SCAD-ADES-DFLT-DB
                :IND-DCO-DT-SCAD-ADES-DFLT
               ,:DCO-COD-FND-DFLT
                :IND-DCO-COD-FND-DFLT
               ,:DCO-TP-DUR
                :IND-DCO-TP-DUR
               ,:DCO-TP-CALC-DUR
                :IND-DCO-TP-CALC-DUR
               ,:DCO-FL-NO-ADERENTI
                :IND-DCO-FL-NO-ADERENTI
               ,:DCO-FL-DISTINTA-CNBTVA
                :IND-DCO-FL-DISTINTA-CNBTVA
               ,:DCO-FL-CNBT-AUTES
                :IND-DCO-FL-CNBT-AUTES
               ,:DCO-FL-QTZ-POST-EMIS
                :IND-DCO-FL-QTZ-POST-EMIS
               ,:DCO-FL-COMNZ-FND-IS
                :IND-DCO-FL-COMNZ-FND-IS
               ,:DCO-DS-RIGA
               ,:DCO-DS-OPER-SQL
               ,:DCO-DS-VER
               ,:DCO-DS-TS-INI-CPTZ
               ,:DCO-DS-TS-END-CPTZ
               ,:DCO-DS-UTENTE
               ,:DCO-DS-STATO-ELAB
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-DCO-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO DCO-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-DCO-IMP-ARROT-PRE = -1
              MOVE HIGH-VALUES TO DCO-IMP-ARROT-PRE-NULL
           END-IF
           IF IND-DCO-PC-SCON = -1
              MOVE HIGH-VALUES TO DCO-PC-SCON-NULL
           END-IF
           IF IND-DCO-FL-ADES-SING = -1
              MOVE HIGH-VALUES TO DCO-FL-ADES-SING-NULL
           END-IF
           IF IND-DCO-TP-IMP = -1
              MOVE HIGH-VALUES TO DCO-TP-IMP-NULL
           END-IF
           IF IND-DCO-FL-RICL-PRE-DA-CPT = -1
              MOVE HIGH-VALUES TO DCO-FL-RICL-PRE-DA-CPT-NULL
           END-IF
           IF IND-DCO-TP-ADES = -1
              MOVE HIGH-VALUES TO DCO-TP-ADES-NULL
           END-IF
           IF IND-DCO-DT-ULT-RINN-TAC = -1
              MOVE HIGH-VALUES TO DCO-DT-ULT-RINN-TAC-NULL
           END-IF
           IF IND-DCO-IMP-SCON = -1
              MOVE HIGH-VALUES TO DCO-IMP-SCON-NULL
           END-IF
           IF IND-DCO-FRAZ-DFLT = -1
              MOVE HIGH-VALUES TO DCO-FRAZ-DFLT-NULL
           END-IF
           IF IND-DCO-ETA-SCAD-MASC-DFLT = -1
              MOVE HIGH-VALUES TO DCO-ETA-SCAD-MASC-DFLT-NULL
           END-IF
           IF IND-DCO-ETA-SCAD-FEMM-DFLT = -1
              MOVE HIGH-VALUES TO DCO-ETA-SCAD-FEMM-DFLT-NULL
           END-IF
           IF IND-DCO-TP-DFLT-DUR = -1
              MOVE HIGH-VALUES TO DCO-TP-DFLT-DUR-NULL
           END-IF
           IF IND-DCO-DUR-AA-ADES-DFLT = -1
              MOVE HIGH-VALUES TO DCO-DUR-AA-ADES-DFLT-NULL
           END-IF
           IF IND-DCO-DUR-MM-ADES-DFLT = -1
              MOVE HIGH-VALUES TO DCO-DUR-MM-ADES-DFLT-NULL
           END-IF
           IF IND-DCO-DUR-GG-ADES-DFLT = -1
              MOVE HIGH-VALUES TO DCO-DUR-GG-ADES-DFLT-NULL
           END-IF
           IF IND-DCO-DT-SCAD-ADES-DFLT = -1
              MOVE HIGH-VALUES TO DCO-DT-SCAD-ADES-DFLT-NULL
           END-IF
           IF IND-DCO-COD-FND-DFLT = -1
              MOVE HIGH-VALUES TO DCO-COD-FND-DFLT-NULL
           END-IF
           IF IND-DCO-TP-DUR = -1
              MOVE HIGH-VALUES TO DCO-TP-DUR-NULL
           END-IF
           IF IND-DCO-TP-CALC-DUR = -1
              MOVE HIGH-VALUES TO DCO-TP-CALC-DUR-NULL
           END-IF
           IF IND-DCO-FL-NO-ADERENTI = -1
              MOVE HIGH-VALUES TO DCO-FL-NO-ADERENTI-NULL
           END-IF
           IF IND-DCO-FL-DISTINTA-CNBTVA = -1
              MOVE HIGH-VALUES TO DCO-FL-DISTINTA-CNBTVA-NULL
           END-IF
           IF IND-DCO-FL-CNBT-AUTES = -1
              MOVE HIGH-VALUES TO DCO-FL-CNBT-AUTES-NULL
           END-IF
           IF IND-DCO-FL-QTZ-POST-EMIS = -1
              MOVE HIGH-VALUES TO DCO-FL-QTZ-POST-EMIS-NULL
           END-IF
           IF IND-DCO-FL-COMNZ-FND-IS = -1
              MOVE HIGH-VALUES TO DCO-FL-COMNZ-FND-IS-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO DCO-DS-OPER-SQL
           MOVE 0                   TO DCO-DS-VER
           MOVE IDSV0003-USER-NAME TO DCO-DS-UTENTE
           MOVE '1'                   TO DCO-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO DCO-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO DCO-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF DCO-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-DCO-ID-MOVI-CHIU
           END-IF
           IF DCO-IMP-ARROT-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-IMP-ARROT-PRE
           ELSE
              MOVE 0 TO IND-DCO-IMP-ARROT-PRE
           END-IF
           IF DCO-PC-SCON-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-PC-SCON
           ELSE
              MOVE 0 TO IND-DCO-PC-SCON
           END-IF
           IF DCO-FL-ADES-SING-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-FL-ADES-SING
           ELSE
              MOVE 0 TO IND-DCO-FL-ADES-SING
           END-IF
           IF DCO-TP-IMP-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-TP-IMP
           ELSE
              MOVE 0 TO IND-DCO-TP-IMP
           END-IF
           IF DCO-FL-RICL-PRE-DA-CPT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-FL-RICL-PRE-DA-CPT
           ELSE
              MOVE 0 TO IND-DCO-FL-RICL-PRE-DA-CPT
           END-IF
           IF DCO-TP-ADES-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-TP-ADES
           ELSE
              MOVE 0 TO IND-DCO-TP-ADES
           END-IF
           IF DCO-DT-ULT-RINN-TAC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-DT-ULT-RINN-TAC
           ELSE
              MOVE 0 TO IND-DCO-DT-ULT-RINN-TAC
           END-IF
           IF DCO-IMP-SCON-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-IMP-SCON
           ELSE
              MOVE 0 TO IND-DCO-IMP-SCON
           END-IF
           IF DCO-FRAZ-DFLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-FRAZ-DFLT
           ELSE
              MOVE 0 TO IND-DCO-FRAZ-DFLT
           END-IF
           IF DCO-ETA-SCAD-MASC-DFLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-ETA-SCAD-MASC-DFLT
           ELSE
              MOVE 0 TO IND-DCO-ETA-SCAD-MASC-DFLT
           END-IF
           IF DCO-ETA-SCAD-FEMM-DFLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-ETA-SCAD-FEMM-DFLT
           ELSE
              MOVE 0 TO IND-DCO-ETA-SCAD-FEMM-DFLT
           END-IF
           IF DCO-TP-DFLT-DUR-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-TP-DFLT-DUR
           ELSE
              MOVE 0 TO IND-DCO-TP-DFLT-DUR
           END-IF
           IF DCO-DUR-AA-ADES-DFLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-DUR-AA-ADES-DFLT
           ELSE
              MOVE 0 TO IND-DCO-DUR-AA-ADES-DFLT
           END-IF
           IF DCO-DUR-MM-ADES-DFLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-DUR-MM-ADES-DFLT
           ELSE
              MOVE 0 TO IND-DCO-DUR-MM-ADES-DFLT
           END-IF
           IF DCO-DUR-GG-ADES-DFLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-DUR-GG-ADES-DFLT
           ELSE
              MOVE 0 TO IND-DCO-DUR-GG-ADES-DFLT
           END-IF
           IF DCO-DT-SCAD-ADES-DFLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-DT-SCAD-ADES-DFLT
           ELSE
              MOVE 0 TO IND-DCO-DT-SCAD-ADES-DFLT
           END-IF
           IF DCO-COD-FND-DFLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-COD-FND-DFLT
           ELSE
              MOVE 0 TO IND-DCO-COD-FND-DFLT
           END-IF
           IF DCO-TP-DUR-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-TP-DUR
           ELSE
              MOVE 0 TO IND-DCO-TP-DUR
           END-IF
           IF DCO-TP-CALC-DUR-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-TP-CALC-DUR
           ELSE
              MOVE 0 TO IND-DCO-TP-CALC-DUR
           END-IF
           IF DCO-FL-NO-ADERENTI-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-FL-NO-ADERENTI
           ELSE
              MOVE 0 TO IND-DCO-FL-NO-ADERENTI
           END-IF
           IF DCO-FL-DISTINTA-CNBTVA-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-FL-DISTINTA-CNBTVA
           ELSE
              MOVE 0 TO IND-DCO-FL-DISTINTA-CNBTVA
           END-IF
           IF DCO-FL-CNBT-AUTES-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-FL-CNBT-AUTES
           ELSE
              MOVE 0 TO IND-DCO-FL-CNBT-AUTES
           END-IF
           IF DCO-FL-QTZ-POST-EMIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-FL-QTZ-POST-EMIS
           ELSE
              MOVE 0 TO IND-DCO-FL-QTZ-POST-EMIS
           END-IF
           IF DCO-FL-COMNZ-FND-IS-NULL = HIGH-VALUES
              MOVE -1 TO IND-DCO-FL-COMNZ-FND-IS
           ELSE
              MOVE 0 TO IND-DCO-FL-COMNZ-FND-IS
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : DCO-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE D-COLL TO WS-BUFFER-TABLE.

           MOVE DCO-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO DCO-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO DCO-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO DCO-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO DCO-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO DCO-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO DCO-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO DCO-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO D-COLL.

           MOVE WS-ID-MOVI-CRZ  TO DCO-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO DCO-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO DCO-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO DCO-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO DCO-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO DCO-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO DCO-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE DCO-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO DCO-DT-INI-EFF-DB
           MOVE DCO-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO DCO-DT-END-EFF-DB
           IF IND-DCO-DT-ULT-RINN-TAC = 0
               MOVE DCO-DT-ULT-RINN-TAC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO DCO-DT-ULT-RINN-TAC-DB
           END-IF
           IF IND-DCO-DT-SCAD-ADES-DFLT = 0
               MOVE DCO-DT-SCAD-ADES-DFLT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO DCO-DT-SCAD-ADES-DFLT-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE DCO-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO DCO-DT-INI-EFF
           MOVE DCO-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO DCO-DT-END-EFF
           IF IND-DCO-DT-ULT-RINN-TAC = 0
               MOVE DCO-DT-ULT-RINN-TAC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO DCO-DT-ULT-RINN-TAC
           END-IF
           IF IND-DCO-DT-SCAD-ADES-DFLT = 0
               MOVE DCO-DT-SCAD-ADES-DFLT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO DCO-DT-SCAD-ADES-DFLT
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSDFA0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  22 APR 2010.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDDFA0 END-EXEC.
           EXEC SQL INCLUDE IDBVDFA2 END-EXEC.
           EXEC SQL INCLUDE IDBVDFA3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVDFA1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 D-FISC-ADES.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSDFA0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'D_FISC_ADES' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_D_FISC_ADES
                ,ID_ADES
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_ISC_FND
                ,DT_ACCNS_RAPP_FND
                ,IMP_CNBT_AZ_K1
                ,IMP_CNBT_ISC_K1
                ,IMP_CNBT_TFR_K1
                ,IMP_CNBT_VOL_K1
                ,IMP_CNBT_AZ_K2
                ,IMP_CNBT_ISC_K2
                ,IMP_CNBT_TFR_K2
                ,IMP_CNBT_VOL_K2
                ,MATU_K1
                ,MATU_RES_K1
                ,MATU_K2
                ,IMPB_VIS
                ,IMPST_VIS
                ,IMPB_IS_K2
                ,IMPST_SOST_K2
                ,RIDZ_TFR
                ,PC_TFR
                ,ALQ_TFR
                ,TOT_ANTIC
                ,IMPB_TFR_ANTIC
                ,IMPST_TFR_ANTIC
                ,RIDZ_TFR_SU_ANTIC
                ,IMPB_VIS_ANTIC
                ,IMPST_VIS_ANTIC
                ,IMPB_IS_K2_ANTIC
                ,IMPST_SOST_K2_ANTI
                ,ULT_COMMIS_TRASFE
                ,COD_DVS
                ,ALQ_PRVR
                ,PC_ESE_IMPST_TFR
                ,IMP_ESE_IMPST_TFR
                ,ANZ_CNBTVA_CARASS
                ,ANZ_CNBTVA_CARAZI
                ,ANZ_SRVZ
                ,IMP_CNBT_NDED_K1
                ,IMP_CNBT_NDED_K2
                ,IMP_CNBT_NDED_K3
                ,IMP_CNBT_AZ_K3
                ,IMP_CNBT_ISC_K3
                ,IMP_CNBT_TFR_K3
                ,IMP_CNBT_VOL_K3
                ,MATU_K3
                ,IMPB_252_ANTIC
                ,IMPST_252_ANTIC
                ,DT_1A_CNBZ
                ,COMMIS_DI_TRASFE
                ,AA_CNBZ_K1
                ,AA_CNBZ_K2
                ,AA_CNBZ_K3
                ,MM_CNBZ_K1
                ,MM_CNBZ_K2
                ,MM_CNBZ_K3
                ,FL_APPLZ_NEWFIS
                ,REDT_TASS_ABBAT_K3
                ,CNBT_ECC_4X100_K1
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CREDITO_IS
                ,REDT_TASS_ABBAT_K2
                ,IMPB_IS_K3
                ,IMPST_SOST_K3
                ,IMPB_252_K3
                ,IMPST_252_K3
                ,IMPB_IS_K3_ANTIC
                ,IMPST_SOST_K3_ANTI
                ,IMPB_IRPEF_K1_ANTI
                ,IMPST_IRPEF_K1_ANT
                ,IMPB_IRPEF_K2_ANTI
                ,IMPST_IRPEF_K2_ANT
                ,DT_CESSAZIONE
                ,TOT_IMPST
                ,ONER_TRASFE
                ,IMP_NET_TRASFERITO
             INTO
                :DFA-ID-D-FISC-ADES
               ,:DFA-ID-ADES
               ,:DFA-ID-MOVI-CRZ
               ,:DFA-ID-MOVI-CHIU
                :IND-DFA-ID-MOVI-CHIU
               ,:DFA-DT-INI-EFF-DB
               ,:DFA-DT-END-EFF-DB
               ,:DFA-COD-COMP-ANIA
               ,:DFA-TP-ISC-FND
                :IND-DFA-TP-ISC-FND
               ,:DFA-DT-ACCNS-RAPP-FND-DB
                :IND-DFA-DT-ACCNS-RAPP-FND
               ,:DFA-IMP-CNBT-AZ-K1
                :IND-DFA-IMP-CNBT-AZ-K1
               ,:DFA-IMP-CNBT-ISC-K1
                :IND-DFA-IMP-CNBT-ISC-K1
               ,:DFA-IMP-CNBT-TFR-K1
                :IND-DFA-IMP-CNBT-TFR-K1
               ,:DFA-IMP-CNBT-VOL-K1
                :IND-DFA-IMP-CNBT-VOL-K1
               ,:DFA-IMP-CNBT-AZ-K2
                :IND-DFA-IMP-CNBT-AZ-K2
               ,:DFA-IMP-CNBT-ISC-K2
                :IND-DFA-IMP-CNBT-ISC-K2
               ,:DFA-IMP-CNBT-TFR-K2
                :IND-DFA-IMP-CNBT-TFR-K2
               ,:DFA-IMP-CNBT-VOL-K2
                :IND-DFA-IMP-CNBT-VOL-K2
               ,:DFA-MATU-K1
                :IND-DFA-MATU-K1
               ,:DFA-MATU-RES-K1
                :IND-DFA-MATU-RES-K1
               ,:DFA-MATU-K2
                :IND-DFA-MATU-K2
               ,:DFA-IMPB-VIS
                :IND-DFA-IMPB-VIS
               ,:DFA-IMPST-VIS
                :IND-DFA-IMPST-VIS
               ,:DFA-IMPB-IS-K2
                :IND-DFA-IMPB-IS-K2
               ,:DFA-IMPST-SOST-K2
                :IND-DFA-IMPST-SOST-K2
               ,:DFA-RIDZ-TFR
                :IND-DFA-RIDZ-TFR
               ,:DFA-PC-TFR
                :IND-DFA-PC-TFR
               ,:DFA-ALQ-TFR
                :IND-DFA-ALQ-TFR
               ,:DFA-TOT-ANTIC
                :IND-DFA-TOT-ANTIC
               ,:DFA-IMPB-TFR-ANTIC
                :IND-DFA-IMPB-TFR-ANTIC
               ,:DFA-IMPST-TFR-ANTIC
                :IND-DFA-IMPST-TFR-ANTIC
               ,:DFA-RIDZ-TFR-SU-ANTIC
                :IND-DFA-RIDZ-TFR-SU-ANTIC
               ,:DFA-IMPB-VIS-ANTIC
                :IND-DFA-IMPB-VIS-ANTIC
               ,:DFA-IMPST-VIS-ANTIC
                :IND-DFA-IMPST-VIS-ANTIC
               ,:DFA-IMPB-IS-K2-ANTIC
                :IND-DFA-IMPB-IS-K2-ANTIC
               ,:DFA-IMPST-SOST-K2-ANTI
                :IND-DFA-IMPST-SOST-K2-ANTI
               ,:DFA-ULT-COMMIS-TRASFE
                :IND-DFA-ULT-COMMIS-TRASFE
               ,:DFA-COD-DVS
                :IND-DFA-COD-DVS
               ,:DFA-ALQ-PRVR
                :IND-DFA-ALQ-PRVR
               ,:DFA-PC-ESE-IMPST-TFR
                :IND-DFA-PC-ESE-IMPST-TFR
               ,:DFA-IMP-ESE-IMPST-TFR
                :IND-DFA-IMP-ESE-IMPST-TFR
               ,:DFA-ANZ-CNBTVA-CARASS
                :IND-DFA-ANZ-CNBTVA-CARASS
               ,:DFA-ANZ-CNBTVA-CARAZI
                :IND-DFA-ANZ-CNBTVA-CARAZI
               ,:DFA-ANZ-SRVZ
                :IND-DFA-ANZ-SRVZ
               ,:DFA-IMP-CNBT-NDED-K1
                :IND-DFA-IMP-CNBT-NDED-K1
               ,:DFA-IMP-CNBT-NDED-K2
                :IND-DFA-IMP-CNBT-NDED-K2
               ,:DFA-IMP-CNBT-NDED-K3
                :IND-DFA-IMP-CNBT-NDED-K3
               ,:DFA-IMP-CNBT-AZ-K3
                :IND-DFA-IMP-CNBT-AZ-K3
               ,:DFA-IMP-CNBT-ISC-K3
                :IND-DFA-IMP-CNBT-ISC-K3
               ,:DFA-IMP-CNBT-TFR-K3
                :IND-DFA-IMP-CNBT-TFR-K3
               ,:DFA-IMP-CNBT-VOL-K3
                :IND-DFA-IMP-CNBT-VOL-K3
               ,:DFA-MATU-K3
                :IND-DFA-MATU-K3
               ,:DFA-IMPB-252-ANTIC
                :IND-DFA-IMPB-252-ANTIC
               ,:DFA-IMPST-252-ANTIC
                :IND-DFA-IMPST-252-ANTIC
               ,:DFA-DT-1A-CNBZ-DB
                :IND-DFA-DT-1A-CNBZ
               ,:DFA-COMMIS-DI-TRASFE
                :IND-DFA-COMMIS-DI-TRASFE
               ,:DFA-AA-CNBZ-K1
                :IND-DFA-AA-CNBZ-K1
               ,:DFA-AA-CNBZ-K2
                :IND-DFA-AA-CNBZ-K2
               ,:DFA-AA-CNBZ-K3
                :IND-DFA-AA-CNBZ-K3
               ,:DFA-MM-CNBZ-K1
                :IND-DFA-MM-CNBZ-K1
               ,:DFA-MM-CNBZ-K2
                :IND-DFA-MM-CNBZ-K2
               ,:DFA-MM-CNBZ-K3
                :IND-DFA-MM-CNBZ-K3
               ,:DFA-FL-APPLZ-NEWFIS
                :IND-DFA-FL-APPLZ-NEWFIS
               ,:DFA-REDT-TASS-ABBAT-K3
                :IND-DFA-REDT-TASS-ABBAT-K3
               ,:DFA-CNBT-ECC-4X100-K1
                :IND-DFA-CNBT-ECC-4X100-K1
               ,:DFA-DS-RIGA
               ,:DFA-DS-OPER-SQL
               ,:DFA-DS-VER
               ,:DFA-DS-TS-INI-CPTZ
               ,:DFA-DS-TS-END-CPTZ
               ,:DFA-DS-UTENTE
               ,:DFA-DS-STATO-ELAB
               ,:DFA-CREDITO-IS
                :IND-DFA-CREDITO-IS
               ,:DFA-REDT-TASS-ABBAT-K2
                :IND-DFA-REDT-TASS-ABBAT-K2
               ,:DFA-IMPB-IS-K3
                :IND-DFA-IMPB-IS-K3
               ,:DFA-IMPST-SOST-K3
                :IND-DFA-IMPST-SOST-K3
               ,:DFA-IMPB-252-K3
                :IND-DFA-IMPB-252-K3
               ,:DFA-IMPST-252-K3
                :IND-DFA-IMPST-252-K3
               ,:DFA-IMPB-IS-K3-ANTIC
                :IND-DFA-IMPB-IS-K3-ANTIC
               ,:DFA-IMPST-SOST-K3-ANTI
                :IND-DFA-IMPST-SOST-K3-ANTI
               ,:DFA-IMPB-IRPEF-K1-ANTI
                :IND-DFA-IMPB-IRPEF-K1-ANTI
               ,:DFA-IMPST-IRPEF-K1-ANT
                :IND-DFA-IMPST-IRPEF-K1-ANT
               ,:DFA-IMPB-IRPEF-K2-ANTI
                :IND-DFA-IMPB-IRPEF-K2-ANTI
               ,:DFA-IMPST-IRPEF-K2-ANT
                :IND-DFA-IMPST-IRPEF-K2-ANT
               ,:DFA-DT-CESSAZIONE-DB
                :IND-DFA-DT-CESSAZIONE
               ,:DFA-TOT-IMPST
                :IND-DFA-TOT-IMPST
               ,:DFA-ONER-TRASFE
                :IND-DFA-ONER-TRASFE
               ,:DFA-IMP-NET-TRASFERITO
                :IND-DFA-IMP-NET-TRASFERITO
             FROM D_FISC_ADES
             WHERE     DS_RIGA = :DFA-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO D_FISC_ADES
                     (
                        ID_D_FISC_ADES
                       ,ID_ADES
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_ISC_FND
                       ,DT_ACCNS_RAPP_FND
                       ,IMP_CNBT_AZ_K1
                       ,IMP_CNBT_ISC_K1
                       ,IMP_CNBT_TFR_K1
                       ,IMP_CNBT_VOL_K1
                       ,IMP_CNBT_AZ_K2
                       ,IMP_CNBT_ISC_K2
                       ,IMP_CNBT_TFR_K2
                       ,IMP_CNBT_VOL_K2
                       ,MATU_K1
                       ,MATU_RES_K1
                       ,MATU_K2
                       ,IMPB_VIS
                       ,IMPST_VIS
                       ,IMPB_IS_K2
                       ,IMPST_SOST_K2
                       ,RIDZ_TFR
                       ,PC_TFR
                       ,ALQ_TFR
                       ,TOT_ANTIC
                       ,IMPB_TFR_ANTIC
                       ,IMPST_TFR_ANTIC
                       ,RIDZ_TFR_SU_ANTIC
                       ,IMPB_VIS_ANTIC
                       ,IMPST_VIS_ANTIC
                       ,IMPB_IS_K2_ANTIC
                       ,IMPST_SOST_K2_ANTI
                       ,ULT_COMMIS_TRASFE
                       ,COD_DVS
                       ,ALQ_PRVR
                       ,PC_ESE_IMPST_TFR
                       ,IMP_ESE_IMPST_TFR
                       ,ANZ_CNBTVA_CARASS
                       ,ANZ_CNBTVA_CARAZI
                       ,ANZ_SRVZ
                       ,IMP_CNBT_NDED_K1
                       ,IMP_CNBT_NDED_K2
                       ,IMP_CNBT_NDED_K3
                       ,IMP_CNBT_AZ_K3
                       ,IMP_CNBT_ISC_K3
                       ,IMP_CNBT_TFR_K3
                       ,IMP_CNBT_VOL_K3
                       ,MATU_K3
                       ,IMPB_252_ANTIC
                       ,IMPST_252_ANTIC
                       ,DT_1A_CNBZ
                       ,COMMIS_DI_TRASFE
                       ,AA_CNBZ_K1
                       ,AA_CNBZ_K2
                       ,AA_CNBZ_K3
                       ,MM_CNBZ_K1
                       ,MM_CNBZ_K2
                       ,MM_CNBZ_K3
                       ,FL_APPLZ_NEWFIS
                       ,REDT_TASS_ABBAT_K3
                       ,CNBT_ECC_4X100_K1
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,CREDITO_IS
                       ,REDT_TASS_ABBAT_K2
                       ,IMPB_IS_K3
                       ,IMPST_SOST_K3
                       ,IMPB_252_K3
                       ,IMPST_252_K3
                       ,IMPB_IS_K3_ANTIC
                       ,IMPST_SOST_K3_ANTI
                       ,IMPB_IRPEF_K1_ANTI
                       ,IMPST_IRPEF_K1_ANT
                       ,IMPB_IRPEF_K2_ANTI
                       ,IMPST_IRPEF_K2_ANT
                       ,DT_CESSAZIONE
                       ,TOT_IMPST
                       ,ONER_TRASFE
                       ,IMP_NET_TRASFERITO
                     )
                 VALUES
                     (
                       :DFA-ID-D-FISC-ADES
                       ,:DFA-ID-ADES
                       ,:DFA-ID-MOVI-CRZ
                       ,:DFA-ID-MOVI-CHIU
                        :IND-DFA-ID-MOVI-CHIU
                       ,:DFA-DT-INI-EFF-DB
                       ,:DFA-DT-END-EFF-DB
                       ,:DFA-COD-COMP-ANIA
                       ,:DFA-TP-ISC-FND
                        :IND-DFA-TP-ISC-FND
                       ,:DFA-DT-ACCNS-RAPP-FND-DB
                        :IND-DFA-DT-ACCNS-RAPP-FND
                       ,:DFA-IMP-CNBT-AZ-K1
                        :IND-DFA-IMP-CNBT-AZ-K1
                       ,:DFA-IMP-CNBT-ISC-K1
                        :IND-DFA-IMP-CNBT-ISC-K1
                       ,:DFA-IMP-CNBT-TFR-K1
                        :IND-DFA-IMP-CNBT-TFR-K1
                       ,:DFA-IMP-CNBT-VOL-K1
                        :IND-DFA-IMP-CNBT-VOL-K1
                       ,:DFA-IMP-CNBT-AZ-K2
                        :IND-DFA-IMP-CNBT-AZ-K2
                       ,:DFA-IMP-CNBT-ISC-K2
                        :IND-DFA-IMP-CNBT-ISC-K2
                       ,:DFA-IMP-CNBT-TFR-K2
                        :IND-DFA-IMP-CNBT-TFR-K2
                       ,:DFA-IMP-CNBT-VOL-K2
                        :IND-DFA-IMP-CNBT-VOL-K2
                       ,:DFA-MATU-K1
                        :IND-DFA-MATU-K1
                       ,:DFA-MATU-RES-K1
                        :IND-DFA-MATU-RES-K1
                       ,:DFA-MATU-K2
                        :IND-DFA-MATU-K2
                       ,:DFA-IMPB-VIS
                        :IND-DFA-IMPB-VIS
                       ,:DFA-IMPST-VIS
                        :IND-DFA-IMPST-VIS
                       ,:DFA-IMPB-IS-K2
                        :IND-DFA-IMPB-IS-K2
                       ,:DFA-IMPST-SOST-K2
                        :IND-DFA-IMPST-SOST-K2
                       ,:DFA-RIDZ-TFR
                        :IND-DFA-RIDZ-TFR
                       ,:DFA-PC-TFR
                        :IND-DFA-PC-TFR
                       ,:DFA-ALQ-TFR
                        :IND-DFA-ALQ-TFR
                       ,:DFA-TOT-ANTIC
                        :IND-DFA-TOT-ANTIC
                       ,:DFA-IMPB-TFR-ANTIC
                        :IND-DFA-IMPB-TFR-ANTIC
                       ,:DFA-IMPST-TFR-ANTIC
                        :IND-DFA-IMPST-TFR-ANTIC
                       ,:DFA-RIDZ-TFR-SU-ANTIC
                        :IND-DFA-RIDZ-TFR-SU-ANTIC
                       ,:DFA-IMPB-VIS-ANTIC
                        :IND-DFA-IMPB-VIS-ANTIC
                       ,:DFA-IMPST-VIS-ANTIC
                        :IND-DFA-IMPST-VIS-ANTIC
                       ,:DFA-IMPB-IS-K2-ANTIC
                        :IND-DFA-IMPB-IS-K2-ANTIC
                       ,:DFA-IMPST-SOST-K2-ANTI
                        :IND-DFA-IMPST-SOST-K2-ANTI
                       ,:DFA-ULT-COMMIS-TRASFE
                        :IND-DFA-ULT-COMMIS-TRASFE
                       ,:DFA-COD-DVS
                        :IND-DFA-COD-DVS
                       ,:DFA-ALQ-PRVR
                        :IND-DFA-ALQ-PRVR
                       ,:DFA-PC-ESE-IMPST-TFR
                        :IND-DFA-PC-ESE-IMPST-TFR
                       ,:DFA-IMP-ESE-IMPST-TFR
                        :IND-DFA-IMP-ESE-IMPST-TFR
                       ,:DFA-ANZ-CNBTVA-CARASS
                        :IND-DFA-ANZ-CNBTVA-CARASS
                       ,:DFA-ANZ-CNBTVA-CARAZI
                        :IND-DFA-ANZ-CNBTVA-CARAZI
                       ,:DFA-ANZ-SRVZ
                        :IND-DFA-ANZ-SRVZ
                       ,:DFA-IMP-CNBT-NDED-K1
                        :IND-DFA-IMP-CNBT-NDED-K1
                       ,:DFA-IMP-CNBT-NDED-K2
                        :IND-DFA-IMP-CNBT-NDED-K2
                       ,:DFA-IMP-CNBT-NDED-K3
                        :IND-DFA-IMP-CNBT-NDED-K3
                       ,:DFA-IMP-CNBT-AZ-K3
                        :IND-DFA-IMP-CNBT-AZ-K3
                       ,:DFA-IMP-CNBT-ISC-K3
                        :IND-DFA-IMP-CNBT-ISC-K3
                       ,:DFA-IMP-CNBT-TFR-K3
                        :IND-DFA-IMP-CNBT-TFR-K3
                       ,:DFA-IMP-CNBT-VOL-K3
                        :IND-DFA-IMP-CNBT-VOL-K3
                       ,:DFA-MATU-K3
                        :IND-DFA-MATU-K3
                       ,:DFA-IMPB-252-ANTIC
                        :IND-DFA-IMPB-252-ANTIC
                       ,:DFA-IMPST-252-ANTIC
                        :IND-DFA-IMPST-252-ANTIC
                       ,:DFA-DT-1A-CNBZ-DB
                        :IND-DFA-DT-1A-CNBZ
                       ,:DFA-COMMIS-DI-TRASFE
                        :IND-DFA-COMMIS-DI-TRASFE
                       ,:DFA-AA-CNBZ-K1
                        :IND-DFA-AA-CNBZ-K1
                       ,:DFA-AA-CNBZ-K2
                        :IND-DFA-AA-CNBZ-K2
                       ,:DFA-AA-CNBZ-K3
                        :IND-DFA-AA-CNBZ-K3
                       ,:DFA-MM-CNBZ-K1
                        :IND-DFA-MM-CNBZ-K1
                       ,:DFA-MM-CNBZ-K2
                        :IND-DFA-MM-CNBZ-K2
                       ,:DFA-MM-CNBZ-K3
                        :IND-DFA-MM-CNBZ-K3
                       ,:DFA-FL-APPLZ-NEWFIS
                        :IND-DFA-FL-APPLZ-NEWFIS
                       ,:DFA-REDT-TASS-ABBAT-K3
                        :IND-DFA-REDT-TASS-ABBAT-K3
                       ,:DFA-CNBT-ECC-4X100-K1
                        :IND-DFA-CNBT-ECC-4X100-K1
                       ,:DFA-DS-RIGA
                       ,:DFA-DS-OPER-SQL
                       ,:DFA-DS-VER
                       ,:DFA-DS-TS-INI-CPTZ
                       ,:DFA-DS-TS-END-CPTZ
                       ,:DFA-DS-UTENTE
                       ,:DFA-DS-STATO-ELAB
                       ,:DFA-CREDITO-IS
                        :IND-DFA-CREDITO-IS
                       ,:DFA-REDT-TASS-ABBAT-K2
                        :IND-DFA-REDT-TASS-ABBAT-K2
                       ,:DFA-IMPB-IS-K3
                        :IND-DFA-IMPB-IS-K3
                       ,:DFA-IMPST-SOST-K3
                        :IND-DFA-IMPST-SOST-K3
                       ,:DFA-IMPB-252-K3
                        :IND-DFA-IMPB-252-K3
                       ,:DFA-IMPST-252-K3
                        :IND-DFA-IMPST-252-K3
                       ,:DFA-IMPB-IS-K3-ANTIC
                        :IND-DFA-IMPB-IS-K3-ANTIC
                       ,:DFA-IMPST-SOST-K3-ANTI
                        :IND-DFA-IMPST-SOST-K3-ANTI
                       ,:DFA-IMPB-IRPEF-K1-ANTI
                        :IND-DFA-IMPB-IRPEF-K1-ANTI
                       ,:DFA-IMPST-IRPEF-K1-ANT
                        :IND-DFA-IMPST-IRPEF-K1-ANT
                       ,:DFA-IMPB-IRPEF-K2-ANTI
                        :IND-DFA-IMPB-IRPEF-K2-ANTI
                       ,:DFA-IMPST-IRPEF-K2-ANT
                        :IND-DFA-IMPST-IRPEF-K2-ANT
                       ,:DFA-DT-CESSAZIONE-DB
                        :IND-DFA-DT-CESSAZIONE
                       ,:DFA-TOT-IMPST
                        :IND-DFA-TOT-IMPST
                       ,:DFA-ONER-TRASFE
                        :IND-DFA-ONER-TRASFE
                       ,:DFA-IMP-NET-TRASFERITO
                        :IND-DFA-IMP-NET-TRASFERITO
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE D_FISC_ADES SET

                   ID_D_FISC_ADES         =
                :DFA-ID-D-FISC-ADES
                  ,ID_ADES                =
                :DFA-ID-ADES
                  ,ID_MOVI_CRZ            =
                :DFA-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :DFA-ID-MOVI-CHIU
                                       :IND-DFA-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :DFA-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :DFA-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :DFA-COD-COMP-ANIA
                  ,TP_ISC_FND             =
                :DFA-TP-ISC-FND
                                       :IND-DFA-TP-ISC-FND
                  ,DT_ACCNS_RAPP_FND      =
           :DFA-DT-ACCNS-RAPP-FND-DB
                                       :IND-DFA-DT-ACCNS-RAPP-FND
                  ,IMP_CNBT_AZ_K1         =
                :DFA-IMP-CNBT-AZ-K1
                                       :IND-DFA-IMP-CNBT-AZ-K1
                  ,IMP_CNBT_ISC_K1        =
                :DFA-IMP-CNBT-ISC-K1
                                       :IND-DFA-IMP-CNBT-ISC-K1
                  ,IMP_CNBT_TFR_K1        =
                :DFA-IMP-CNBT-TFR-K1
                                       :IND-DFA-IMP-CNBT-TFR-K1
                  ,IMP_CNBT_VOL_K1        =
                :DFA-IMP-CNBT-VOL-K1
                                       :IND-DFA-IMP-CNBT-VOL-K1
                  ,IMP_CNBT_AZ_K2         =
                :DFA-IMP-CNBT-AZ-K2
                                       :IND-DFA-IMP-CNBT-AZ-K2
                  ,IMP_CNBT_ISC_K2        =
                :DFA-IMP-CNBT-ISC-K2
                                       :IND-DFA-IMP-CNBT-ISC-K2
                  ,IMP_CNBT_TFR_K2        =
                :DFA-IMP-CNBT-TFR-K2
                                       :IND-DFA-IMP-CNBT-TFR-K2
                  ,IMP_CNBT_VOL_K2        =
                :DFA-IMP-CNBT-VOL-K2
                                       :IND-DFA-IMP-CNBT-VOL-K2
                  ,MATU_K1                =
                :DFA-MATU-K1
                                       :IND-DFA-MATU-K1
                  ,MATU_RES_K1            =
                :DFA-MATU-RES-K1
                                       :IND-DFA-MATU-RES-K1
                  ,MATU_K2                =
                :DFA-MATU-K2
                                       :IND-DFA-MATU-K2
                  ,IMPB_VIS               =
                :DFA-IMPB-VIS
                                       :IND-DFA-IMPB-VIS
                  ,IMPST_VIS              =
                :DFA-IMPST-VIS
                                       :IND-DFA-IMPST-VIS
                  ,IMPB_IS_K2             =
                :DFA-IMPB-IS-K2
                                       :IND-DFA-IMPB-IS-K2
                  ,IMPST_SOST_K2          =
                :DFA-IMPST-SOST-K2
                                       :IND-DFA-IMPST-SOST-K2
                  ,RIDZ_TFR               =
                :DFA-RIDZ-TFR
                                       :IND-DFA-RIDZ-TFR
                  ,PC_TFR                 =
                :DFA-PC-TFR
                                       :IND-DFA-PC-TFR
                  ,ALQ_TFR                =
                :DFA-ALQ-TFR
                                       :IND-DFA-ALQ-TFR
                  ,TOT_ANTIC              =
                :DFA-TOT-ANTIC
                                       :IND-DFA-TOT-ANTIC
                  ,IMPB_TFR_ANTIC         =
                :DFA-IMPB-TFR-ANTIC
                                       :IND-DFA-IMPB-TFR-ANTIC
                  ,IMPST_TFR_ANTIC        =
                :DFA-IMPST-TFR-ANTIC
                                       :IND-DFA-IMPST-TFR-ANTIC
                  ,RIDZ_TFR_SU_ANTIC      =
                :DFA-RIDZ-TFR-SU-ANTIC
                                       :IND-DFA-RIDZ-TFR-SU-ANTIC
                  ,IMPB_VIS_ANTIC         =
                :DFA-IMPB-VIS-ANTIC
                                       :IND-DFA-IMPB-VIS-ANTIC
                  ,IMPST_VIS_ANTIC        =
                :DFA-IMPST-VIS-ANTIC
                                       :IND-DFA-IMPST-VIS-ANTIC
                  ,IMPB_IS_K2_ANTIC       =
                :DFA-IMPB-IS-K2-ANTIC
                                       :IND-DFA-IMPB-IS-K2-ANTIC
                  ,IMPST_SOST_K2_ANTI     =
                :DFA-IMPST-SOST-K2-ANTI
                                       :IND-DFA-IMPST-SOST-K2-ANTI
                  ,ULT_COMMIS_TRASFE      =
                :DFA-ULT-COMMIS-TRASFE
                                       :IND-DFA-ULT-COMMIS-TRASFE
                  ,COD_DVS                =
                :DFA-COD-DVS
                                       :IND-DFA-COD-DVS
                  ,ALQ_PRVR               =
                :DFA-ALQ-PRVR
                                       :IND-DFA-ALQ-PRVR
                  ,PC_ESE_IMPST_TFR       =
                :DFA-PC-ESE-IMPST-TFR
                                       :IND-DFA-PC-ESE-IMPST-TFR
                  ,IMP_ESE_IMPST_TFR      =
                :DFA-IMP-ESE-IMPST-TFR
                                       :IND-DFA-IMP-ESE-IMPST-TFR
                  ,ANZ_CNBTVA_CARASS      =
                :DFA-ANZ-CNBTVA-CARASS
                                       :IND-DFA-ANZ-CNBTVA-CARASS
                  ,ANZ_CNBTVA_CARAZI      =
                :DFA-ANZ-CNBTVA-CARAZI
                                       :IND-DFA-ANZ-CNBTVA-CARAZI
                  ,ANZ_SRVZ               =
                :DFA-ANZ-SRVZ
                                       :IND-DFA-ANZ-SRVZ
                  ,IMP_CNBT_NDED_K1       =
                :DFA-IMP-CNBT-NDED-K1
                                       :IND-DFA-IMP-CNBT-NDED-K1
                  ,IMP_CNBT_NDED_K2       =
                :DFA-IMP-CNBT-NDED-K2
                                       :IND-DFA-IMP-CNBT-NDED-K2
                  ,IMP_CNBT_NDED_K3       =
                :DFA-IMP-CNBT-NDED-K3
                                       :IND-DFA-IMP-CNBT-NDED-K3
                  ,IMP_CNBT_AZ_K3         =
                :DFA-IMP-CNBT-AZ-K3
                                       :IND-DFA-IMP-CNBT-AZ-K3
                  ,IMP_CNBT_ISC_K3        =
                :DFA-IMP-CNBT-ISC-K3
                                       :IND-DFA-IMP-CNBT-ISC-K3
                  ,IMP_CNBT_TFR_K3        =
                :DFA-IMP-CNBT-TFR-K3
                                       :IND-DFA-IMP-CNBT-TFR-K3
                  ,IMP_CNBT_VOL_K3        =
                :DFA-IMP-CNBT-VOL-K3
                                       :IND-DFA-IMP-CNBT-VOL-K3
                  ,MATU_K3                =
                :DFA-MATU-K3
                                       :IND-DFA-MATU-K3
                  ,IMPB_252_ANTIC         =
                :DFA-IMPB-252-ANTIC
                                       :IND-DFA-IMPB-252-ANTIC
                  ,IMPST_252_ANTIC        =
                :DFA-IMPST-252-ANTIC
                                       :IND-DFA-IMPST-252-ANTIC
                  ,DT_1A_CNBZ             =
           :DFA-DT-1A-CNBZ-DB
                                       :IND-DFA-DT-1A-CNBZ
                  ,COMMIS_DI_TRASFE       =
                :DFA-COMMIS-DI-TRASFE
                                       :IND-DFA-COMMIS-DI-TRASFE
                  ,AA_CNBZ_K1             =
                :DFA-AA-CNBZ-K1
                                       :IND-DFA-AA-CNBZ-K1
                  ,AA_CNBZ_K2             =
                :DFA-AA-CNBZ-K2
                                       :IND-DFA-AA-CNBZ-K2
                  ,AA_CNBZ_K3             =
                :DFA-AA-CNBZ-K3
                                       :IND-DFA-AA-CNBZ-K3
                  ,MM_CNBZ_K1             =
                :DFA-MM-CNBZ-K1
                                       :IND-DFA-MM-CNBZ-K1
                  ,MM_CNBZ_K2             =
                :DFA-MM-CNBZ-K2
                                       :IND-DFA-MM-CNBZ-K2
                  ,MM_CNBZ_K3             =
                :DFA-MM-CNBZ-K3
                                       :IND-DFA-MM-CNBZ-K3
                  ,FL_APPLZ_NEWFIS        =
                :DFA-FL-APPLZ-NEWFIS
                                       :IND-DFA-FL-APPLZ-NEWFIS
                  ,REDT_TASS_ABBAT_K3     =
                :DFA-REDT-TASS-ABBAT-K3
                                       :IND-DFA-REDT-TASS-ABBAT-K3
                  ,CNBT_ECC_4X100_K1      =
                :DFA-CNBT-ECC-4X100-K1
                                       :IND-DFA-CNBT-ECC-4X100-K1
                  ,DS_RIGA                =
                :DFA-DS-RIGA
                  ,DS_OPER_SQL            =
                :DFA-DS-OPER-SQL
                  ,DS_VER                 =
                :DFA-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :DFA-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :DFA-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :DFA-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :DFA-DS-STATO-ELAB
                  ,CREDITO_IS             =
                :DFA-CREDITO-IS
                                       :IND-DFA-CREDITO-IS
                  ,REDT_TASS_ABBAT_K2     =
                :DFA-REDT-TASS-ABBAT-K2
                                       :IND-DFA-REDT-TASS-ABBAT-K2
                  ,IMPB_IS_K3             =
                :DFA-IMPB-IS-K3
                                       :IND-DFA-IMPB-IS-K3
                  ,IMPST_SOST_K3          =
                :DFA-IMPST-SOST-K3
                                       :IND-DFA-IMPST-SOST-K3
                  ,IMPB_252_K3            =
                :DFA-IMPB-252-K3
                                       :IND-DFA-IMPB-252-K3
                  ,IMPST_252_K3           =
                :DFA-IMPST-252-K3
                                       :IND-DFA-IMPST-252-K3
                  ,IMPB_IS_K3_ANTIC       =
                :DFA-IMPB-IS-K3-ANTIC
                                       :IND-DFA-IMPB-IS-K3-ANTIC
                  ,IMPST_SOST_K3_ANTI     =
                :DFA-IMPST-SOST-K3-ANTI
                                       :IND-DFA-IMPST-SOST-K3-ANTI
                  ,IMPB_IRPEF_K1_ANTI     =
                :DFA-IMPB-IRPEF-K1-ANTI
                                       :IND-DFA-IMPB-IRPEF-K1-ANTI
                  ,IMPST_IRPEF_K1_ANT     =
                :DFA-IMPST-IRPEF-K1-ANT
                                       :IND-DFA-IMPST-IRPEF-K1-ANT
                  ,IMPB_IRPEF_K2_ANTI     =
                :DFA-IMPB-IRPEF-K2-ANTI
                                       :IND-DFA-IMPB-IRPEF-K2-ANTI
                  ,IMPST_IRPEF_K2_ANT     =
                :DFA-IMPST-IRPEF-K2-ANT
                                       :IND-DFA-IMPST-IRPEF-K2-ANT
                  ,DT_CESSAZIONE          =
           :DFA-DT-CESSAZIONE-DB
                                       :IND-DFA-DT-CESSAZIONE
                  ,TOT_IMPST              =
                :DFA-TOT-IMPST
                                       :IND-DFA-TOT-IMPST
                  ,ONER_TRASFE            =
                :DFA-ONER-TRASFE
                                       :IND-DFA-ONER-TRASFE
                  ,IMP_NET_TRASFERITO     =
                :DFA-IMP-NET-TRASFERITO
                                       :IND-DFA-IMP-NET-TRASFERITO
                WHERE     DS_RIGA = :DFA-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM D_FISC_ADES
                WHERE     DS_RIGA = :DFA-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-DFA CURSOR FOR
              SELECT
                     ID_D_FISC_ADES
                    ,ID_ADES
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_ISC_FND
                    ,DT_ACCNS_RAPP_FND
                    ,IMP_CNBT_AZ_K1
                    ,IMP_CNBT_ISC_K1
                    ,IMP_CNBT_TFR_K1
                    ,IMP_CNBT_VOL_K1
                    ,IMP_CNBT_AZ_K2
                    ,IMP_CNBT_ISC_K2
                    ,IMP_CNBT_TFR_K2
                    ,IMP_CNBT_VOL_K2
                    ,MATU_K1
                    ,MATU_RES_K1
                    ,MATU_K2
                    ,IMPB_VIS
                    ,IMPST_VIS
                    ,IMPB_IS_K2
                    ,IMPST_SOST_K2
                    ,RIDZ_TFR
                    ,PC_TFR
                    ,ALQ_TFR
                    ,TOT_ANTIC
                    ,IMPB_TFR_ANTIC
                    ,IMPST_TFR_ANTIC
                    ,RIDZ_TFR_SU_ANTIC
                    ,IMPB_VIS_ANTIC
                    ,IMPST_VIS_ANTIC
                    ,IMPB_IS_K2_ANTIC
                    ,IMPST_SOST_K2_ANTI
                    ,ULT_COMMIS_TRASFE
                    ,COD_DVS
                    ,ALQ_PRVR
                    ,PC_ESE_IMPST_TFR
                    ,IMP_ESE_IMPST_TFR
                    ,ANZ_CNBTVA_CARASS
                    ,ANZ_CNBTVA_CARAZI
                    ,ANZ_SRVZ
                    ,IMP_CNBT_NDED_K1
                    ,IMP_CNBT_NDED_K2
                    ,IMP_CNBT_NDED_K3
                    ,IMP_CNBT_AZ_K3
                    ,IMP_CNBT_ISC_K3
                    ,IMP_CNBT_TFR_K3
                    ,IMP_CNBT_VOL_K3
                    ,MATU_K3
                    ,IMPB_252_ANTIC
                    ,IMPST_252_ANTIC
                    ,DT_1A_CNBZ
                    ,COMMIS_DI_TRASFE
                    ,AA_CNBZ_K1
                    ,AA_CNBZ_K2
                    ,AA_CNBZ_K3
                    ,MM_CNBZ_K1
                    ,MM_CNBZ_K2
                    ,MM_CNBZ_K3
                    ,FL_APPLZ_NEWFIS
                    ,REDT_TASS_ABBAT_K3
                    ,CNBT_ECC_4X100_K1
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,CREDITO_IS
                    ,REDT_TASS_ABBAT_K2
                    ,IMPB_IS_K3
                    ,IMPST_SOST_K3
                    ,IMPB_252_K3
                    ,IMPST_252_K3
                    ,IMPB_IS_K3_ANTIC
                    ,IMPST_SOST_K3_ANTI
                    ,IMPB_IRPEF_K1_ANTI
                    ,IMPST_IRPEF_K1_ANT
                    ,IMPB_IRPEF_K2_ANTI
                    ,IMPST_IRPEF_K2_ANT
                    ,DT_CESSAZIONE
                    ,TOT_IMPST
                    ,ONER_TRASFE
                    ,IMP_NET_TRASFERITO
              FROM D_FISC_ADES
              WHERE     ID_D_FISC_ADES = :DFA-ID-D-FISC-ADES
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_D_FISC_ADES
                ,ID_ADES
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_ISC_FND
                ,DT_ACCNS_RAPP_FND
                ,IMP_CNBT_AZ_K1
                ,IMP_CNBT_ISC_K1
                ,IMP_CNBT_TFR_K1
                ,IMP_CNBT_VOL_K1
                ,IMP_CNBT_AZ_K2
                ,IMP_CNBT_ISC_K2
                ,IMP_CNBT_TFR_K2
                ,IMP_CNBT_VOL_K2
                ,MATU_K1
                ,MATU_RES_K1
                ,MATU_K2
                ,IMPB_VIS
                ,IMPST_VIS
                ,IMPB_IS_K2
                ,IMPST_SOST_K2
                ,RIDZ_TFR
                ,PC_TFR
                ,ALQ_TFR
                ,TOT_ANTIC
                ,IMPB_TFR_ANTIC
                ,IMPST_TFR_ANTIC
                ,RIDZ_TFR_SU_ANTIC
                ,IMPB_VIS_ANTIC
                ,IMPST_VIS_ANTIC
                ,IMPB_IS_K2_ANTIC
                ,IMPST_SOST_K2_ANTI
                ,ULT_COMMIS_TRASFE
                ,COD_DVS
                ,ALQ_PRVR
                ,PC_ESE_IMPST_TFR
                ,IMP_ESE_IMPST_TFR
                ,ANZ_CNBTVA_CARASS
                ,ANZ_CNBTVA_CARAZI
                ,ANZ_SRVZ
                ,IMP_CNBT_NDED_K1
                ,IMP_CNBT_NDED_K2
                ,IMP_CNBT_NDED_K3
                ,IMP_CNBT_AZ_K3
                ,IMP_CNBT_ISC_K3
                ,IMP_CNBT_TFR_K3
                ,IMP_CNBT_VOL_K3
                ,MATU_K3
                ,IMPB_252_ANTIC
                ,IMPST_252_ANTIC
                ,DT_1A_CNBZ
                ,COMMIS_DI_TRASFE
                ,AA_CNBZ_K1
                ,AA_CNBZ_K2
                ,AA_CNBZ_K3
                ,MM_CNBZ_K1
                ,MM_CNBZ_K2
                ,MM_CNBZ_K3
                ,FL_APPLZ_NEWFIS
                ,REDT_TASS_ABBAT_K3
                ,CNBT_ECC_4X100_K1
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CREDITO_IS
                ,REDT_TASS_ABBAT_K2
                ,IMPB_IS_K3
                ,IMPST_SOST_K3
                ,IMPB_252_K3
                ,IMPST_252_K3
                ,IMPB_IS_K3_ANTIC
                ,IMPST_SOST_K3_ANTI
                ,IMPB_IRPEF_K1_ANTI
                ,IMPST_IRPEF_K1_ANT
                ,IMPB_IRPEF_K2_ANTI
                ,IMPST_IRPEF_K2_ANT
                ,DT_CESSAZIONE
                ,TOT_IMPST
                ,ONER_TRASFE
                ,IMP_NET_TRASFERITO
             INTO
                :DFA-ID-D-FISC-ADES
               ,:DFA-ID-ADES
               ,:DFA-ID-MOVI-CRZ
               ,:DFA-ID-MOVI-CHIU
                :IND-DFA-ID-MOVI-CHIU
               ,:DFA-DT-INI-EFF-DB
               ,:DFA-DT-END-EFF-DB
               ,:DFA-COD-COMP-ANIA
               ,:DFA-TP-ISC-FND
                :IND-DFA-TP-ISC-FND
               ,:DFA-DT-ACCNS-RAPP-FND-DB
                :IND-DFA-DT-ACCNS-RAPP-FND
               ,:DFA-IMP-CNBT-AZ-K1
                :IND-DFA-IMP-CNBT-AZ-K1
               ,:DFA-IMP-CNBT-ISC-K1
                :IND-DFA-IMP-CNBT-ISC-K1
               ,:DFA-IMP-CNBT-TFR-K1
                :IND-DFA-IMP-CNBT-TFR-K1
               ,:DFA-IMP-CNBT-VOL-K1
                :IND-DFA-IMP-CNBT-VOL-K1
               ,:DFA-IMP-CNBT-AZ-K2
                :IND-DFA-IMP-CNBT-AZ-K2
               ,:DFA-IMP-CNBT-ISC-K2
                :IND-DFA-IMP-CNBT-ISC-K2
               ,:DFA-IMP-CNBT-TFR-K2
                :IND-DFA-IMP-CNBT-TFR-K2
               ,:DFA-IMP-CNBT-VOL-K2
                :IND-DFA-IMP-CNBT-VOL-K2
               ,:DFA-MATU-K1
                :IND-DFA-MATU-K1
               ,:DFA-MATU-RES-K1
                :IND-DFA-MATU-RES-K1
               ,:DFA-MATU-K2
                :IND-DFA-MATU-K2
               ,:DFA-IMPB-VIS
                :IND-DFA-IMPB-VIS
               ,:DFA-IMPST-VIS
                :IND-DFA-IMPST-VIS
               ,:DFA-IMPB-IS-K2
                :IND-DFA-IMPB-IS-K2
               ,:DFA-IMPST-SOST-K2
                :IND-DFA-IMPST-SOST-K2
               ,:DFA-RIDZ-TFR
                :IND-DFA-RIDZ-TFR
               ,:DFA-PC-TFR
                :IND-DFA-PC-TFR
               ,:DFA-ALQ-TFR
                :IND-DFA-ALQ-TFR
               ,:DFA-TOT-ANTIC
                :IND-DFA-TOT-ANTIC
               ,:DFA-IMPB-TFR-ANTIC
                :IND-DFA-IMPB-TFR-ANTIC
               ,:DFA-IMPST-TFR-ANTIC
                :IND-DFA-IMPST-TFR-ANTIC
               ,:DFA-RIDZ-TFR-SU-ANTIC
                :IND-DFA-RIDZ-TFR-SU-ANTIC
               ,:DFA-IMPB-VIS-ANTIC
                :IND-DFA-IMPB-VIS-ANTIC
               ,:DFA-IMPST-VIS-ANTIC
                :IND-DFA-IMPST-VIS-ANTIC
               ,:DFA-IMPB-IS-K2-ANTIC
                :IND-DFA-IMPB-IS-K2-ANTIC
               ,:DFA-IMPST-SOST-K2-ANTI
                :IND-DFA-IMPST-SOST-K2-ANTI
               ,:DFA-ULT-COMMIS-TRASFE
                :IND-DFA-ULT-COMMIS-TRASFE
               ,:DFA-COD-DVS
                :IND-DFA-COD-DVS
               ,:DFA-ALQ-PRVR
                :IND-DFA-ALQ-PRVR
               ,:DFA-PC-ESE-IMPST-TFR
                :IND-DFA-PC-ESE-IMPST-TFR
               ,:DFA-IMP-ESE-IMPST-TFR
                :IND-DFA-IMP-ESE-IMPST-TFR
               ,:DFA-ANZ-CNBTVA-CARASS
                :IND-DFA-ANZ-CNBTVA-CARASS
               ,:DFA-ANZ-CNBTVA-CARAZI
                :IND-DFA-ANZ-CNBTVA-CARAZI
               ,:DFA-ANZ-SRVZ
                :IND-DFA-ANZ-SRVZ
               ,:DFA-IMP-CNBT-NDED-K1
                :IND-DFA-IMP-CNBT-NDED-K1
               ,:DFA-IMP-CNBT-NDED-K2
                :IND-DFA-IMP-CNBT-NDED-K2
               ,:DFA-IMP-CNBT-NDED-K3
                :IND-DFA-IMP-CNBT-NDED-K3
               ,:DFA-IMP-CNBT-AZ-K3
                :IND-DFA-IMP-CNBT-AZ-K3
               ,:DFA-IMP-CNBT-ISC-K3
                :IND-DFA-IMP-CNBT-ISC-K3
               ,:DFA-IMP-CNBT-TFR-K3
                :IND-DFA-IMP-CNBT-TFR-K3
               ,:DFA-IMP-CNBT-VOL-K3
                :IND-DFA-IMP-CNBT-VOL-K3
               ,:DFA-MATU-K3
                :IND-DFA-MATU-K3
               ,:DFA-IMPB-252-ANTIC
                :IND-DFA-IMPB-252-ANTIC
               ,:DFA-IMPST-252-ANTIC
                :IND-DFA-IMPST-252-ANTIC
               ,:DFA-DT-1A-CNBZ-DB
                :IND-DFA-DT-1A-CNBZ
               ,:DFA-COMMIS-DI-TRASFE
                :IND-DFA-COMMIS-DI-TRASFE
               ,:DFA-AA-CNBZ-K1
                :IND-DFA-AA-CNBZ-K1
               ,:DFA-AA-CNBZ-K2
                :IND-DFA-AA-CNBZ-K2
               ,:DFA-AA-CNBZ-K3
                :IND-DFA-AA-CNBZ-K3
               ,:DFA-MM-CNBZ-K1
                :IND-DFA-MM-CNBZ-K1
               ,:DFA-MM-CNBZ-K2
                :IND-DFA-MM-CNBZ-K2
               ,:DFA-MM-CNBZ-K3
                :IND-DFA-MM-CNBZ-K3
               ,:DFA-FL-APPLZ-NEWFIS
                :IND-DFA-FL-APPLZ-NEWFIS
               ,:DFA-REDT-TASS-ABBAT-K3
                :IND-DFA-REDT-TASS-ABBAT-K3
               ,:DFA-CNBT-ECC-4X100-K1
                :IND-DFA-CNBT-ECC-4X100-K1
               ,:DFA-DS-RIGA
               ,:DFA-DS-OPER-SQL
               ,:DFA-DS-VER
               ,:DFA-DS-TS-INI-CPTZ
               ,:DFA-DS-TS-END-CPTZ
               ,:DFA-DS-UTENTE
               ,:DFA-DS-STATO-ELAB
               ,:DFA-CREDITO-IS
                :IND-DFA-CREDITO-IS
               ,:DFA-REDT-TASS-ABBAT-K2
                :IND-DFA-REDT-TASS-ABBAT-K2
               ,:DFA-IMPB-IS-K3
                :IND-DFA-IMPB-IS-K3
               ,:DFA-IMPST-SOST-K3
                :IND-DFA-IMPST-SOST-K3
               ,:DFA-IMPB-252-K3
                :IND-DFA-IMPB-252-K3
               ,:DFA-IMPST-252-K3
                :IND-DFA-IMPST-252-K3
               ,:DFA-IMPB-IS-K3-ANTIC
                :IND-DFA-IMPB-IS-K3-ANTIC
               ,:DFA-IMPST-SOST-K3-ANTI
                :IND-DFA-IMPST-SOST-K3-ANTI
               ,:DFA-IMPB-IRPEF-K1-ANTI
                :IND-DFA-IMPB-IRPEF-K1-ANTI
               ,:DFA-IMPST-IRPEF-K1-ANT
                :IND-DFA-IMPST-IRPEF-K1-ANT
               ,:DFA-IMPB-IRPEF-K2-ANTI
                :IND-DFA-IMPB-IRPEF-K2-ANTI
               ,:DFA-IMPST-IRPEF-K2-ANT
                :IND-DFA-IMPST-IRPEF-K2-ANT
               ,:DFA-DT-CESSAZIONE-DB
                :IND-DFA-DT-CESSAZIONE
               ,:DFA-TOT-IMPST
                :IND-DFA-TOT-IMPST
               ,:DFA-ONER-TRASFE
                :IND-DFA-ONER-TRASFE
               ,:DFA-IMP-NET-TRASFERITO
                :IND-DFA-IMP-NET-TRASFERITO
             FROM D_FISC_ADES
             WHERE     ID_D_FISC_ADES = :DFA-ID-D-FISC-ADES
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE D_FISC_ADES SET

                   ID_D_FISC_ADES         =
                :DFA-ID-D-FISC-ADES
                  ,ID_ADES                =
                :DFA-ID-ADES
                  ,ID_MOVI_CRZ            =
                :DFA-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :DFA-ID-MOVI-CHIU
                                       :IND-DFA-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :DFA-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :DFA-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :DFA-COD-COMP-ANIA
                  ,TP_ISC_FND             =
                :DFA-TP-ISC-FND
                                       :IND-DFA-TP-ISC-FND
                  ,DT_ACCNS_RAPP_FND      =
           :DFA-DT-ACCNS-RAPP-FND-DB
                                       :IND-DFA-DT-ACCNS-RAPP-FND
                  ,IMP_CNBT_AZ_K1         =
                :DFA-IMP-CNBT-AZ-K1
                                       :IND-DFA-IMP-CNBT-AZ-K1
                  ,IMP_CNBT_ISC_K1        =
                :DFA-IMP-CNBT-ISC-K1
                                       :IND-DFA-IMP-CNBT-ISC-K1
                  ,IMP_CNBT_TFR_K1        =
                :DFA-IMP-CNBT-TFR-K1
                                       :IND-DFA-IMP-CNBT-TFR-K1
                  ,IMP_CNBT_VOL_K1        =
                :DFA-IMP-CNBT-VOL-K1
                                       :IND-DFA-IMP-CNBT-VOL-K1
                  ,IMP_CNBT_AZ_K2         =
                :DFA-IMP-CNBT-AZ-K2
                                       :IND-DFA-IMP-CNBT-AZ-K2
                  ,IMP_CNBT_ISC_K2        =
                :DFA-IMP-CNBT-ISC-K2
                                       :IND-DFA-IMP-CNBT-ISC-K2
                  ,IMP_CNBT_TFR_K2        =
                :DFA-IMP-CNBT-TFR-K2
                                       :IND-DFA-IMP-CNBT-TFR-K2
                  ,IMP_CNBT_VOL_K2        =
                :DFA-IMP-CNBT-VOL-K2
                                       :IND-DFA-IMP-CNBT-VOL-K2
                  ,MATU_K1                =
                :DFA-MATU-K1
                                       :IND-DFA-MATU-K1
                  ,MATU_RES_K1            =
                :DFA-MATU-RES-K1
                                       :IND-DFA-MATU-RES-K1
                  ,MATU_K2                =
                :DFA-MATU-K2
                                       :IND-DFA-MATU-K2
                  ,IMPB_VIS               =
                :DFA-IMPB-VIS
                                       :IND-DFA-IMPB-VIS
                  ,IMPST_VIS              =
                :DFA-IMPST-VIS
                                       :IND-DFA-IMPST-VIS
                  ,IMPB_IS_K2             =
                :DFA-IMPB-IS-K2
                                       :IND-DFA-IMPB-IS-K2
                  ,IMPST_SOST_K2          =
                :DFA-IMPST-SOST-K2
                                       :IND-DFA-IMPST-SOST-K2
                  ,RIDZ_TFR               =
                :DFA-RIDZ-TFR
                                       :IND-DFA-RIDZ-TFR
                  ,PC_TFR                 =
                :DFA-PC-TFR
                                       :IND-DFA-PC-TFR
                  ,ALQ_TFR                =
                :DFA-ALQ-TFR
                                       :IND-DFA-ALQ-TFR
                  ,TOT_ANTIC              =
                :DFA-TOT-ANTIC
                                       :IND-DFA-TOT-ANTIC
                  ,IMPB_TFR_ANTIC         =
                :DFA-IMPB-TFR-ANTIC
                                       :IND-DFA-IMPB-TFR-ANTIC
                  ,IMPST_TFR_ANTIC        =
                :DFA-IMPST-TFR-ANTIC
                                       :IND-DFA-IMPST-TFR-ANTIC
                  ,RIDZ_TFR_SU_ANTIC      =
                :DFA-RIDZ-TFR-SU-ANTIC
                                       :IND-DFA-RIDZ-TFR-SU-ANTIC
                  ,IMPB_VIS_ANTIC         =
                :DFA-IMPB-VIS-ANTIC
                                       :IND-DFA-IMPB-VIS-ANTIC
                  ,IMPST_VIS_ANTIC        =
                :DFA-IMPST-VIS-ANTIC
                                       :IND-DFA-IMPST-VIS-ANTIC
                  ,IMPB_IS_K2_ANTIC       =
                :DFA-IMPB-IS-K2-ANTIC
                                       :IND-DFA-IMPB-IS-K2-ANTIC
                  ,IMPST_SOST_K2_ANTI     =
                :DFA-IMPST-SOST-K2-ANTI
                                       :IND-DFA-IMPST-SOST-K2-ANTI
                  ,ULT_COMMIS_TRASFE      =
                :DFA-ULT-COMMIS-TRASFE
                                       :IND-DFA-ULT-COMMIS-TRASFE
                  ,COD_DVS                =
                :DFA-COD-DVS
                                       :IND-DFA-COD-DVS
                  ,ALQ_PRVR               =
                :DFA-ALQ-PRVR
                                       :IND-DFA-ALQ-PRVR
                  ,PC_ESE_IMPST_TFR       =
                :DFA-PC-ESE-IMPST-TFR
                                       :IND-DFA-PC-ESE-IMPST-TFR
                  ,IMP_ESE_IMPST_TFR      =
                :DFA-IMP-ESE-IMPST-TFR
                                       :IND-DFA-IMP-ESE-IMPST-TFR
                  ,ANZ_CNBTVA_CARASS      =
                :DFA-ANZ-CNBTVA-CARASS
                                       :IND-DFA-ANZ-CNBTVA-CARASS
                  ,ANZ_CNBTVA_CARAZI      =
                :DFA-ANZ-CNBTVA-CARAZI
                                       :IND-DFA-ANZ-CNBTVA-CARAZI
                  ,ANZ_SRVZ               =
                :DFA-ANZ-SRVZ
                                       :IND-DFA-ANZ-SRVZ
                  ,IMP_CNBT_NDED_K1       =
                :DFA-IMP-CNBT-NDED-K1
                                       :IND-DFA-IMP-CNBT-NDED-K1
                  ,IMP_CNBT_NDED_K2       =
                :DFA-IMP-CNBT-NDED-K2
                                       :IND-DFA-IMP-CNBT-NDED-K2
                  ,IMP_CNBT_NDED_K3       =
                :DFA-IMP-CNBT-NDED-K3
                                       :IND-DFA-IMP-CNBT-NDED-K3
                  ,IMP_CNBT_AZ_K3         =
                :DFA-IMP-CNBT-AZ-K3
                                       :IND-DFA-IMP-CNBT-AZ-K3
                  ,IMP_CNBT_ISC_K3        =
                :DFA-IMP-CNBT-ISC-K3
                                       :IND-DFA-IMP-CNBT-ISC-K3
                  ,IMP_CNBT_TFR_K3        =
                :DFA-IMP-CNBT-TFR-K3
                                       :IND-DFA-IMP-CNBT-TFR-K3
                  ,IMP_CNBT_VOL_K3        =
                :DFA-IMP-CNBT-VOL-K3
                                       :IND-DFA-IMP-CNBT-VOL-K3
                  ,MATU_K3                =
                :DFA-MATU-K3
                                       :IND-DFA-MATU-K3
                  ,IMPB_252_ANTIC         =
                :DFA-IMPB-252-ANTIC
                                       :IND-DFA-IMPB-252-ANTIC
                  ,IMPST_252_ANTIC        =
                :DFA-IMPST-252-ANTIC
                                       :IND-DFA-IMPST-252-ANTIC
                  ,DT_1A_CNBZ             =
           :DFA-DT-1A-CNBZ-DB
                                       :IND-DFA-DT-1A-CNBZ
                  ,COMMIS_DI_TRASFE       =
                :DFA-COMMIS-DI-TRASFE
                                       :IND-DFA-COMMIS-DI-TRASFE
                  ,AA_CNBZ_K1             =
                :DFA-AA-CNBZ-K1
                                       :IND-DFA-AA-CNBZ-K1
                  ,AA_CNBZ_K2             =
                :DFA-AA-CNBZ-K2
                                       :IND-DFA-AA-CNBZ-K2
                  ,AA_CNBZ_K3             =
                :DFA-AA-CNBZ-K3
                                       :IND-DFA-AA-CNBZ-K3
                  ,MM_CNBZ_K1             =
                :DFA-MM-CNBZ-K1
                                       :IND-DFA-MM-CNBZ-K1
                  ,MM_CNBZ_K2             =
                :DFA-MM-CNBZ-K2
                                       :IND-DFA-MM-CNBZ-K2
                  ,MM_CNBZ_K3             =
                :DFA-MM-CNBZ-K3
                                       :IND-DFA-MM-CNBZ-K3
                  ,FL_APPLZ_NEWFIS        =
                :DFA-FL-APPLZ-NEWFIS
                                       :IND-DFA-FL-APPLZ-NEWFIS
                  ,REDT_TASS_ABBAT_K3     =
                :DFA-REDT-TASS-ABBAT-K3
                                       :IND-DFA-REDT-TASS-ABBAT-K3
                  ,CNBT_ECC_4X100_K1      =
                :DFA-CNBT-ECC-4X100-K1
                                       :IND-DFA-CNBT-ECC-4X100-K1
                  ,DS_RIGA                =
                :DFA-DS-RIGA
                  ,DS_OPER_SQL            =
                :DFA-DS-OPER-SQL
                  ,DS_VER                 =
                :DFA-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :DFA-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :DFA-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :DFA-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :DFA-DS-STATO-ELAB
                  ,CREDITO_IS             =
                :DFA-CREDITO-IS
                                       :IND-DFA-CREDITO-IS
                  ,REDT_TASS_ABBAT_K2     =
                :DFA-REDT-TASS-ABBAT-K2
                                       :IND-DFA-REDT-TASS-ABBAT-K2
                  ,IMPB_IS_K3             =
                :DFA-IMPB-IS-K3
                                       :IND-DFA-IMPB-IS-K3
                  ,IMPST_SOST_K3          =
                :DFA-IMPST-SOST-K3
                                       :IND-DFA-IMPST-SOST-K3
                  ,IMPB_252_K3            =
                :DFA-IMPB-252-K3
                                       :IND-DFA-IMPB-252-K3
                  ,IMPST_252_K3           =
                :DFA-IMPST-252-K3
                                       :IND-DFA-IMPST-252-K3
                  ,IMPB_IS_K3_ANTIC       =
                :DFA-IMPB-IS-K3-ANTIC
                                       :IND-DFA-IMPB-IS-K3-ANTIC
                  ,IMPST_SOST_K3_ANTI     =
                :DFA-IMPST-SOST-K3-ANTI
                                       :IND-DFA-IMPST-SOST-K3-ANTI
                  ,IMPB_IRPEF_K1_ANTI     =
                :DFA-IMPB-IRPEF-K1-ANTI
                                       :IND-DFA-IMPB-IRPEF-K1-ANTI
                  ,IMPST_IRPEF_K1_ANT     =
                :DFA-IMPST-IRPEF-K1-ANT
                                       :IND-DFA-IMPST-IRPEF-K1-ANT
                  ,IMPB_IRPEF_K2_ANTI     =
                :DFA-IMPB-IRPEF-K2-ANTI
                                       :IND-DFA-IMPB-IRPEF-K2-ANTI
                  ,IMPST_IRPEF_K2_ANT     =
                :DFA-IMPST-IRPEF-K2-ANT
                                       :IND-DFA-IMPST-IRPEF-K2-ANT
                  ,DT_CESSAZIONE          =
           :DFA-DT-CESSAZIONE-DB
                                       :IND-DFA-DT-CESSAZIONE
                  ,TOT_IMPST              =
                :DFA-TOT-IMPST
                                       :IND-DFA-TOT-IMPST
                  ,ONER_TRASFE            =
                :DFA-ONER-TRASFE
                                       :IND-DFA-ONER-TRASFE
                  ,IMP_NET_TRASFERITO     =
                :DFA-IMP-NET-TRASFERITO
                                       :IND-DFA-IMP-NET-TRASFERITO
                WHERE     DS_RIGA = :DFA-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-DFA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-DFA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-DFA
           INTO
                :DFA-ID-D-FISC-ADES
               ,:DFA-ID-ADES
               ,:DFA-ID-MOVI-CRZ
               ,:DFA-ID-MOVI-CHIU
                :IND-DFA-ID-MOVI-CHIU
               ,:DFA-DT-INI-EFF-DB
               ,:DFA-DT-END-EFF-DB
               ,:DFA-COD-COMP-ANIA
               ,:DFA-TP-ISC-FND
                :IND-DFA-TP-ISC-FND
               ,:DFA-DT-ACCNS-RAPP-FND-DB
                :IND-DFA-DT-ACCNS-RAPP-FND
               ,:DFA-IMP-CNBT-AZ-K1
                :IND-DFA-IMP-CNBT-AZ-K1
               ,:DFA-IMP-CNBT-ISC-K1
                :IND-DFA-IMP-CNBT-ISC-K1
               ,:DFA-IMP-CNBT-TFR-K1
                :IND-DFA-IMP-CNBT-TFR-K1
               ,:DFA-IMP-CNBT-VOL-K1
                :IND-DFA-IMP-CNBT-VOL-K1
               ,:DFA-IMP-CNBT-AZ-K2
                :IND-DFA-IMP-CNBT-AZ-K2
               ,:DFA-IMP-CNBT-ISC-K2
                :IND-DFA-IMP-CNBT-ISC-K2
               ,:DFA-IMP-CNBT-TFR-K2
                :IND-DFA-IMP-CNBT-TFR-K2
               ,:DFA-IMP-CNBT-VOL-K2
                :IND-DFA-IMP-CNBT-VOL-K2
               ,:DFA-MATU-K1
                :IND-DFA-MATU-K1
               ,:DFA-MATU-RES-K1
                :IND-DFA-MATU-RES-K1
               ,:DFA-MATU-K2
                :IND-DFA-MATU-K2
               ,:DFA-IMPB-VIS
                :IND-DFA-IMPB-VIS
               ,:DFA-IMPST-VIS
                :IND-DFA-IMPST-VIS
               ,:DFA-IMPB-IS-K2
                :IND-DFA-IMPB-IS-K2
               ,:DFA-IMPST-SOST-K2
                :IND-DFA-IMPST-SOST-K2
               ,:DFA-RIDZ-TFR
                :IND-DFA-RIDZ-TFR
               ,:DFA-PC-TFR
                :IND-DFA-PC-TFR
               ,:DFA-ALQ-TFR
                :IND-DFA-ALQ-TFR
               ,:DFA-TOT-ANTIC
                :IND-DFA-TOT-ANTIC
               ,:DFA-IMPB-TFR-ANTIC
                :IND-DFA-IMPB-TFR-ANTIC
               ,:DFA-IMPST-TFR-ANTIC
                :IND-DFA-IMPST-TFR-ANTIC
               ,:DFA-RIDZ-TFR-SU-ANTIC
                :IND-DFA-RIDZ-TFR-SU-ANTIC
               ,:DFA-IMPB-VIS-ANTIC
                :IND-DFA-IMPB-VIS-ANTIC
               ,:DFA-IMPST-VIS-ANTIC
                :IND-DFA-IMPST-VIS-ANTIC
               ,:DFA-IMPB-IS-K2-ANTIC
                :IND-DFA-IMPB-IS-K2-ANTIC
               ,:DFA-IMPST-SOST-K2-ANTI
                :IND-DFA-IMPST-SOST-K2-ANTI
               ,:DFA-ULT-COMMIS-TRASFE
                :IND-DFA-ULT-COMMIS-TRASFE
               ,:DFA-COD-DVS
                :IND-DFA-COD-DVS
               ,:DFA-ALQ-PRVR
                :IND-DFA-ALQ-PRVR
               ,:DFA-PC-ESE-IMPST-TFR
                :IND-DFA-PC-ESE-IMPST-TFR
               ,:DFA-IMP-ESE-IMPST-TFR
                :IND-DFA-IMP-ESE-IMPST-TFR
               ,:DFA-ANZ-CNBTVA-CARASS
                :IND-DFA-ANZ-CNBTVA-CARASS
               ,:DFA-ANZ-CNBTVA-CARAZI
                :IND-DFA-ANZ-CNBTVA-CARAZI
               ,:DFA-ANZ-SRVZ
                :IND-DFA-ANZ-SRVZ
               ,:DFA-IMP-CNBT-NDED-K1
                :IND-DFA-IMP-CNBT-NDED-K1
               ,:DFA-IMP-CNBT-NDED-K2
                :IND-DFA-IMP-CNBT-NDED-K2
               ,:DFA-IMP-CNBT-NDED-K3
                :IND-DFA-IMP-CNBT-NDED-K3
               ,:DFA-IMP-CNBT-AZ-K3
                :IND-DFA-IMP-CNBT-AZ-K3
               ,:DFA-IMP-CNBT-ISC-K3
                :IND-DFA-IMP-CNBT-ISC-K3
               ,:DFA-IMP-CNBT-TFR-K3
                :IND-DFA-IMP-CNBT-TFR-K3
               ,:DFA-IMP-CNBT-VOL-K3
                :IND-DFA-IMP-CNBT-VOL-K3
               ,:DFA-MATU-K3
                :IND-DFA-MATU-K3
               ,:DFA-IMPB-252-ANTIC
                :IND-DFA-IMPB-252-ANTIC
               ,:DFA-IMPST-252-ANTIC
                :IND-DFA-IMPST-252-ANTIC
               ,:DFA-DT-1A-CNBZ-DB
                :IND-DFA-DT-1A-CNBZ
               ,:DFA-COMMIS-DI-TRASFE
                :IND-DFA-COMMIS-DI-TRASFE
               ,:DFA-AA-CNBZ-K1
                :IND-DFA-AA-CNBZ-K1
               ,:DFA-AA-CNBZ-K2
                :IND-DFA-AA-CNBZ-K2
               ,:DFA-AA-CNBZ-K3
                :IND-DFA-AA-CNBZ-K3
               ,:DFA-MM-CNBZ-K1
                :IND-DFA-MM-CNBZ-K1
               ,:DFA-MM-CNBZ-K2
                :IND-DFA-MM-CNBZ-K2
               ,:DFA-MM-CNBZ-K3
                :IND-DFA-MM-CNBZ-K3
               ,:DFA-FL-APPLZ-NEWFIS
                :IND-DFA-FL-APPLZ-NEWFIS
               ,:DFA-REDT-TASS-ABBAT-K3
                :IND-DFA-REDT-TASS-ABBAT-K3
               ,:DFA-CNBT-ECC-4X100-K1
                :IND-DFA-CNBT-ECC-4X100-K1
               ,:DFA-DS-RIGA
               ,:DFA-DS-OPER-SQL
               ,:DFA-DS-VER
               ,:DFA-DS-TS-INI-CPTZ
               ,:DFA-DS-TS-END-CPTZ
               ,:DFA-DS-UTENTE
               ,:DFA-DS-STATO-ELAB
               ,:DFA-CREDITO-IS
                :IND-DFA-CREDITO-IS
               ,:DFA-REDT-TASS-ABBAT-K2
                :IND-DFA-REDT-TASS-ABBAT-K2
               ,:DFA-IMPB-IS-K3
                :IND-DFA-IMPB-IS-K3
               ,:DFA-IMPST-SOST-K3
                :IND-DFA-IMPST-SOST-K3
               ,:DFA-IMPB-252-K3
                :IND-DFA-IMPB-252-K3
               ,:DFA-IMPST-252-K3
                :IND-DFA-IMPST-252-K3
               ,:DFA-IMPB-IS-K3-ANTIC
                :IND-DFA-IMPB-IS-K3-ANTIC
               ,:DFA-IMPST-SOST-K3-ANTI
                :IND-DFA-IMPST-SOST-K3-ANTI
               ,:DFA-IMPB-IRPEF-K1-ANTI
                :IND-DFA-IMPB-IRPEF-K1-ANTI
               ,:DFA-IMPST-IRPEF-K1-ANT
                :IND-DFA-IMPST-IRPEF-K1-ANT
               ,:DFA-IMPB-IRPEF-K2-ANTI
                :IND-DFA-IMPB-IRPEF-K2-ANTI
               ,:DFA-IMPST-IRPEF-K2-ANT
                :IND-DFA-IMPST-IRPEF-K2-ANT
               ,:DFA-DT-CESSAZIONE-DB
                :IND-DFA-DT-CESSAZIONE
               ,:DFA-TOT-IMPST
                :IND-DFA-TOT-IMPST
               ,:DFA-ONER-TRASFE
                :IND-DFA-ONER-TRASFE
               ,:DFA-IMP-NET-TRASFERITO
                :IND-DFA-IMP-NET-TRASFERITO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-EFF-DFA CURSOR FOR
              SELECT
                     ID_D_FISC_ADES
                    ,ID_ADES
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_ISC_FND
                    ,DT_ACCNS_RAPP_FND
                    ,IMP_CNBT_AZ_K1
                    ,IMP_CNBT_ISC_K1
                    ,IMP_CNBT_TFR_K1
                    ,IMP_CNBT_VOL_K1
                    ,IMP_CNBT_AZ_K2
                    ,IMP_CNBT_ISC_K2
                    ,IMP_CNBT_TFR_K2
                    ,IMP_CNBT_VOL_K2
                    ,MATU_K1
                    ,MATU_RES_K1
                    ,MATU_K2
                    ,IMPB_VIS
                    ,IMPST_VIS
                    ,IMPB_IS_K2
                    ,IMPST_SOST_K2
                    ,RIDZ_TFR
                    ,PC_TFR
                    ,ALQ_TFR
                    ,TOT_ANTIC
                    ,IMPB_TFR_ANTIC
                    ,IMPST_TFR_ANTIC
                    ,RIDZ_TFR_SU_ANTIC
                    ,IMPB_VIS_ANTIC
                    ,IMPST_VIS_ANTIC
                    ,IMPB_IS_K2_ANTIC
                    ,IMPST_SOST_K2_ANTI
                    ,ULT_COMMIS_TRASFE
                    ,COD_DVS
                    ,ALQ_PRVR
                    ,PC_ESE_IMPST_TFR
                    ,IMP_ESE_IMPST_TFR
                    ,ANZ_CNBTVA_CARASS
                    ,ANZ_CNBTVA_CARAZI
                    ,ANZ_SRVZ
                    ,IMP_CNBT_NDED_K1
                    ,IMP_CNBT_NDED_K2
                    ,IMP_CNBT_NDED_K3
                    ,IMP_CNBT_AZ_K3
                    ,IMP_CNBT_ISC_K3
                    ,IMP_CNBT_TFR_K3
                    ,IMP_CNBT_VOL_K3
                    ,MATU_K3
                    ,IMPB_252_ANTIC
                    ,IMPST_252_ANTIC
                    ,DT_1A_CNBZ
                    ,COMMIS_DI_TRASFE
                    ,AA_CNBZ_K1
                    ,AA_CNBZ_K2
                    ,AA_CNBZ_K3
                    ,MM_CNBZ_K1
                    ,MM_CNBZ_K2
                    ,MM_CNBZ_K3
                    ,FL_APPLZ_NEWFIS
                    ,REDT_TASS_ABBAT_K3
                    ,CNBT_ECC_4X100_K1
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,CREDITO_IS
                    ,REDT_TASS_ABBAT_K2
                    ,IMPB_IS_K3
                    ,IMPST_SOST_K3
                    ,IMPB_252_K3
                    ,IMPST_252_K3
                    ,IMPB_IS_K3_ANTIC
                    ,IMPST_SOST_K3_ANTI
                    ,IMPB_IRPEF_K1_ANTI
                    ,IMPST_IRPEF_K1_ANT
                    ,IMPB_IRPEF_K2_ANTI
                    ,IMPST_IRPEF_K2_ANT
                    ,DT_CESSAZIONE
                    ,TOT_IMPST
                    ,ONER_TRASFE
                    ,IMP_NET_TRASFERITO
              FROM D_FISC_ADES
              WHERE     ID_ADES = :DFA-ID-ADES
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_D_FISC_ADES ASC

           END-EXEC.

       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_D_FISC_ADES
                ,ID_ADES
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_ISC_FND
                ,DT_ACCNS_RAPP_FND
                ,IMP_CNBT_AZ_K1
                ,IMP_CNBT_ISC_K1
                ,IMP_CNBT_TFR_K1
                ,IMP_CNBT_VOL_K1
                ,IMP_CNBT_AZ_K2
                ,IMP_CNBT_ISC_K2
                ,IMP_CNBT_TFR_K2
                ,IMP_CNBT_VOL_K2
                ,MATU_K1
                ,MATU_RES_K1
                ,MATU_K2
                ,IMPB_VIS
                ,IMPST_VIS
                ,IMPB_IS_K2
                ,IMPST_SOST_K2
                ,RIDZ_TFR
                ,PC_TFR
                ,ALQ_TFR
                ,TOT_ANTIC
                ,IMPB_TFR_ANTIC
                ,IMPST_TFR_ANTIC
                ,RIDZ_TFR_SU_ANTIC
                ,IMPB_VIS_ANTIC
                ,IMPST_VIS_ANTIC
                ,IMPB_IS_K2_ANTIC
                ,IMPST_SOST_K2_ANTI
                ,ULT_COMMIS_TRASFE
                ,COD_DVS
                ,ALQ_PRVR
                ,PC_ESE_IMPST_TFR
                ,IMP_ESE_IMPST_TFR
                ,ANZ_CNBTVA_CARASS
                ,ANZ_CNBTVA_CARAZI
                ,ANZ_SRVZ
                ,IMP_CNBT_NDED_K1
                ,IMP_CNBT_NDED_K2
                ,IMP_CNBT_NDED_K3
                ,IMP_CNBT_AZ_K3
                ,IMP_CNBT_ISC_K3
                ,IMP_CNBT_TFR_K3
                ,IMP_CNBT_VOL_K3
                ,MATU_K3
                ,IMPB_252_ANTIC
                ,IMPST_252_ANTIC
                ,DT_1A_CNBZ
                ,COMMIS_DI_TRASFE
                ,AA_CNBZ_K1
                ,AA_CNBZ_K2
                ,AA_CNBZ_K3
                ,MM_CNBZ_K1
                ,MM_CNBZ_K2
                ,MM_CNBZ_K3
                ,FL_APPLZ_NEWFIS
                ,REDT_TASS_ABBAT_K3
                ,CNBT_ECC_4X100_K1
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CREDITO_IS
                ,REDT_TASS_ABBAT_K2
                ,IMPB_IS_K3
                ,IMPST_SOST_K3
                ,IMPB_252_K3
                ,IMPST_252_K3
                ,IMPB_IS_K3_ANTIC
                ,IMPST_SOST_K3_ANTI
                ,IMPB_IRPEF_K1_ANTI
                ,IMPST_IRPEF_K1_ANT
                ,IMPB_IRPEF_K2_ANTI
                ,IMPST_IRPEF_K2_ANT
                ,DT_CESSAZIONE
                ,TOT_IMPST
                ,ONER_TRASFE
                ,IMP_NET_TRASFERITO
             INTO
                :DFA-ID-D-FISC-ADES
               ,:DFA-ID-ADES
               ,:DFA-ID-MOVI-CRZ
               ,:DFA-ID-MOVI-CHIU
                :IND-DFA-ID-MOVI-CHIU
               ,:DFA-DT-INI-EFF-DB
               ,:DFA-DT-END-EFF-DB
               ,:DFA-COD-COMP-ANIA
               ,:DFA-TP-ISC-FND
                :IND-DFA-TP-ISC-FND
               ,:DFA-DT-ACCNS-RAPP-FND-DB
                :IND-DFA-DT-ACCNS-RAPP-FND
               ,:DFA-IMP-CNBT-AZ-K1
                :IND-DFA-IMP-CNBT-AZ-K1
               ,:DFA-IMP-CNBT-ISC-K1
                :IND-DFA-IMP-CNBT-ISC-K1
               ,:DFA-IMP-CNBT-TFR-K1
                :IND-DFA-IMP-CNBT-TFR-K1
               ,:DFA-IMP-CNBT-VOL-K1
                :IND-DFA-IMP-CNBT-VOL-K1
               ,:DFA-IMP-CNBT-AZ-K2
                :IND-DFA-IMP-CNBT-AZ-K2
               ,:DFA-IMP-CNBT-ISC-K2
                :IND-DFA-IMP-CNBT-ISC-K2
               ,:DFA-IMP-CNBT-TFR-K2
                :IND-DFA-IMP-CNBT-TFR-K2
               ,:DFA-IMP-CNBT-VOL-K2
                :IND-DFA-IMP-CNBT-VOL-K2
               ,:DFA-MATU-K1
                :IND-DFA-MATU-K1
               ,:DFA-MATU-RES-K1
                :IND-DFA-MATU-RES-K1
               ,:DFA-MATU-K2
                :IND-DFA-MATU-K2
               ,:DFA-IMPB-VIS
                :IND-DFA-IMPB-VIS
               ,:DFA-IMPST-VIS
                :IND-DFA-IMPST-VIS
               ,:DFA-IMPB-IS-K2
                :IND-DFA-IMPB-IS-K2
               ,:DFA-IMPST-SOST-K2
                :IND-DFA-IMPST-SOST-K2
               ,:DFA-RIDZ-TFR
                :IND-DFA-RIDZ-TFR
               ,:DFA-PC-TFR
                :IND-DFA-PC-TFR
               ,:DFA-ALQ-TFR
                :IND-DFA-ALQ-TFR
               ,:DFA-TOT-ANTIC
                :IND-DFA-TOT-ANTIC
               ,:DFA-IMPB-TFR-ANTIC
                :IND-DFA-IMPB-TFR-ANTIC
               ,:DFA-IMPST-TFR-ANTIC
                :IND-DFA-IMPST-TFR-ANTIC
               ,:DFA-RIDZ-TFR-SU-ANTIC
                :IND-DFA-RIDZ-TFR-SU-ANTIC
               ,:DFA-IMPB-VIS-ANTIC
                :IND-DFA-IMPB-VIS-ANTIC
               ,:DFA-IMPST-VIS-ANTIC
                :IND-DFA-IMPST-VIS-ANTIC
               ,:DFA-IMPB-IS-K2-ANTIC
                :IND-DFA-IMPB-IS-K2-ANTIC
               ,:DFA-IMPST-SOST-K2-ANTI
                :IND-DFA-IMPST-SOST-K2-ANTI
               ,:DFA-ULT-COMMIS-TRASFE
                :IND-DFA-ULT-COMMIS-TRASFE
               ,:DFA-COD-DVS
                :IND-DFA-COD-DVS
               ,:DFA-ALQ-PRVR
                :IND-DFA-ALQ-PRVR
               ,:DFA-PC-ESE-IMPST-TFR
                :IND-DFA-PC-ESE-IMPST-TFR
               ,:DFA-IMP-ESE-IMPST-TFR
                :IND-DFA-IMP-ESE-IMPST-TFR
               ,:DFA-ANZ-CNBTVA-CARASS
                :IND-DFA-ANZ-CNBTVA-CARASS
               ,:DFA-ANZ-CNBTVA-CARAZI
                :IND-DFA-ANZ-CNBTVA-CARAZI
               ,:DFA-ANZ-SRVZ
                :IND-DFA-ANZ-SRVZ
               ,:DFA-IMP-CNBT-NDED-K1
                :IND-DFA-IMP-CNBT-NDED-K1
               ,:DFA-IMP-CNBT-NDED-K2
                :IND-DFA-IMP-CNBT-NDED-K2
               ,:DFA-IMP-CNBT-NDED-K3
                :IND-DFA-IMP-CNBT-NDED-K3
               ,:DFA-IMP-CNBT-AZ-K3
                :IND-DFA-IMP-CNBT-AZ-K3
               ,:DFA-IMP-CNBT-ISC-K3
                :IND-DFA-IMP-CNBT-ISC-K3
               ,:DFA-IMP-CNBT-TFR-K3
                :IND-DFA-IMP-CNBT-TFR-K3
               ,:DFA-IMP-CNBT-VOL-K3
                :IND-DFA-IMP-CNBT-VOL-K3
               ,:DFA-MATU-K3
                :IND-DFA-MATU-K3
               ,:DFA-IMPB-252-ANTIC
                :IND-DFA-IMPB-252-ANTIC
               ,:DFA-IMPST-252-ANTIC
                :IND-DFA-IMPST-252-ANTIC
               ,:DFA-DT-1A-CNBZ-DB
                :IND-DFA-DT-1A-CNBZ
               ,:DFA-COMMIS-DI-TRASFE
                :IND-DFA-COMMIS-DI-TRASFE
               ,:DFA-AA-CNBZ-K1
                :IND-DFA-AA-CNBZ-K1
               ,:DFA-AA-CNBZ-K2
                :IND-DFA-AA-CNBZ-K2
               ,:DFA-AA-CNBZ-K3
                :IND-DFA-AA-CNBZ-K3
               ,:DFA-MM-CNBZ-K1
                :IND-DFA-MM-CNBZ-K1
               ,:DFA-MM-CNBZ-K2
                :IND-DFA-MM-CNBZ-K2
               ,:DFA-MM-CNBZ-K3
                :IND-DFA-MM-CNBZ-K3
               ,:DFA-FL-APPLZ-NEWFIS
                :IND-DFA-FL-APPLZ-NEWFIS
               ,:DFA-REDT-TASS-ABBAT-K3
                :IND-DFA-REDT-TASS-ABBAT-K3
               ,:DFA-CNBT-ECC-4X100-K1
                :IND-DFA-CNBT-ECC-4X100-K1
               ,:DFA-DS-RIGA
               ,:DFA-DS-OPER-SQL
               ,:DFA-DS-VER
               ,:DFA-DS-TS-INI-CPTZ
               ,:DFA-DS-TS-END-CPTZ
               ,:DFA-DS-UTENTE
               ,:DFA-DS-STATO-ELAB
               ,:DFA-CREDITO-IS
                :IND-DFA-CREDITO-IS
               ,:DFA-REDT-TASS-ABBAT-K2
                :IND-DFA-REDT-TASS-ABBAT-K2
               ,:DFA-IMPB-IS-K3
                :IND-DFA-IMPB-IS-K3
               ,:DFA-IMPST-SOST-K3
                :IND-DFA-IMPST-SOST-K3
               ,:DFA-IMPB-252-K3
                :IND-DFA-IMPB-252-K3
               ,:DFA-IMPST-252-K3
                :IND-DFA-IMPST-252-K3
               ,:DFA-IMPB-IS-K3-ANTIC
                :IND-DFA-IMPB-IS-K3-ANTIC
               ,:DFA-IMPST-SOST-K3-ANTI
                :IND-DFA-IMPST-SOST-K3-ANTI
               ,:DFA-IMPB-IRPEF-K1-ANTI
                :IND-DFA-IMPB-IRPEF-K1-ANTI
               ,:DFA-IMPST-IRPEF-K1-ANT
                :IND-DFA-IMPST-IRPEF-K1-ANT
               ,:DFA-IMPB-IRPEF-K2-ANTI
                :IND-DFA-IMPB-IRPEF-K2-ANTI
               ,:DFA-IMPST-IRPEF-K2-ANT
                :IND-DFA-IMPST-IRPEF-K2-ANT
               ,:DFA-DT-CESSAZIONE-DB
                :IND-DFA-DT-CESSAZIONE
               ,:DFA-TOT-IMPST
                :IND-DFA-TOT-IMPST
               ,:DFA-ONER-TRASFE
                :IND-DFA-ONER-TRASFE
               ,:DFA-IMP-NET-TRASFERITO
                :IND-DFA-IMP-NET-TRASFERITO
             FROM D_FISC_ADES
             WHERE     ID_ADES = :DFA-ID-ADES
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           EXEC SQL
                OPEN C-IDP-EFF-DFA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           EXEC SQL
                CLOSE C-IDP-EFF-DFA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           EXEC SQL
                FETCH C-IDP-EFF-DFA
           INTO
                :DFA-ID-D-FISC-ADES
               ,:DFA-ID-ADES
               ,:DFA-ID-MOVI-CRZ
               ,:DFA-ID-MOVI-CHIU
                :IND-DFA-ID-MOVI-CHIU
               ,:DFA-DT-INI-EFF-DB
               ,:DFA-DT-END-EFF-DB
               ,:DFA-COD-COMP-ANIA
               ,:DFA-TP-ISC-FND
                :IND-DFA-TP-ISC-FND
               ,:DFA-DT-ACCNS-RAPP-FND-DB
                :IND-DFA-DT-ACCNS-RAPP-FND
               ,:DFA-IMP-CNBT-AZ-K1
                :IND-DFA-IMP-CNBT-AZ-K1
               ,:DFA-IMP-CNBT-ISC-K1
                :IND-DFA-IMP-CNBT-ISC-K1
               ,:DFA-IMP-CNBT-TFR-K1
                :IND-DFA-IMP-CNBT-TFR-K1
               ,:DFA-IMP-CNBT-VOL-K1
                :IND-DFA-IMP-CNBT-VOL-K1
               ,:DFA-IMP-CNBT-AZ-K2
                :IND-DFA-IMP-CNBT-AZ-K2
               ,:DFA-IMP-CNBT-ISC-K2
                :IND-DFA-IMP-CNBT-ISC-K2
               ,:DFA-IMP-CNBT-TFR-K2
                :IND-DFA-IMP-CNBT-TFR-K2
               ,:DFA-IMP-CNBT-VOL-K2
                :IND-DFA-IMP-CNBT-VOL-K2
               ,:DFA-MATU-K1
                :IND-DFA-MATU-K1
               ,:DFA-MATU-RES-K1
                :IND-DFA-MATU-RES-K1
               ,:DFA-MATU-K2
                :IND-DFA-MATU-K2
               ,:DFA-IMPB-VIS
                :IND-DFA-IMPB-VIS
               ,:DFA-IMPST-VIS
                :IND-DFA-IMPST-VIS
               ,:DFA-IMPB-IS-K2
                :IND-DFA-IMPB-IS-K2
               ,:DFA-IMPST-SOST-K2
                :IND-DFA-IMPST-SOST-K2
               ,:DFA-RIDZ-TFR
                :IND-DFA-RIDZ-TFR
               ,:DFA-PC-TFR
                :IND-DFA-PC-TFR
               ,:DFA-ALQ-TFR
                :IND-DFA-ALQ-TFR
               ,:DFA-TOT-ANTIC
                :IND-DFA-TOT-ANTIC
               ,:DFA-IMPB-TFR-ANTIC
                :IND-DFA-IMPB-TFR-ANTIC
               ,:DFA-IMPST-TFR-ANTIC
                :IND-DFA-IMPST-TFR-ANTIC
               ,:DFA-RIDZ-TFR-SU-ANTIC
                :IND-DFA-RIDZ-TFR-SU-ANTIC
               ,:DFA-IMPB-VIS-ANTIC
                :IND-DFA-IMPB-VIS-ANTIC
               ,:DFA-IMPST-VIS-ANTIC
                :IND-DFA-IMPST-VIS-ANTIC
               ,:DFA-IMPB-IS-K2-ANTIC
                :IND-DFA-IMPB-IS-K2-ANTIC
               ,:DFA-IMPST-SOST-K2-ANTI
                :IND-DFA-IMPST-SOST-K2-ANTI
               ,:DFA-ULT-COMMIS-TRASFE
                :IND-DFA-ULT-COMMIS-TRASFE
               ,:DFA-COD-DVS
                :IND-DFA-COD-DVS
               ,:DFA-ALQ-PRVR
                :IND-DFA-ALQ-PRVR
               ,:DFA-PC-ESE-IMPST-TFR
                :IND-DFA-PC-ESE-IMPST-TFR
               ,:DFA-IMP-ESE-IMPST-TFR
                :IND-DFA-IMP-ESE-IMPST-TFR
               ,:DFA-ANZ-CNBTVA-CARASS
                :IND-DFA-ANZ-CNBTVA-CARASS
               ,:DFA-ANZ-CNBTVA-CARAZI
                :IND-DFA-ANZ-CNBTVA-CARAZI
               ,:DFA-ANZ-SRVZ
                :IND-DFA-ANZ-SRVZ
               ,:DFA-IMP-CNBT-NDED-K1
                :IND-DFA-IMP-CNBT-NDED-K1
               ,:DFA-IMP-CNBT-NDED-K2
                :IND-DFA-IMP-CNBT-NDED-K2
               ,:DFA-IMP-CNBT-NDED-K3
                :IND-DFA-IMP-CNBT-NDED-K3
               ,:DFA-IMP-CNBT-AZ-K3
                :IND-DFA-IMP-CNBT-AZ-K3
               ,:DFA-IMP-CNBT-ISC-K3
                :IND-DFA-IMP-CNBT-ISC-K3
               ,:DFA-IMP-CNBT-TFR-K3
                :IND-DFA-IMP-CNBT-TFR-K3
               ,:DFA-IMP-CNBT-VOL-K3
                :IND-DFA-IMP-CNBT-VOL-K3
               ,:DFA-MATU-K3
                :IND-DFA-MATU-K3
               ,:DFA-IMPB-252-ANTIC
                :IND-DFA-IMPB-252-ANTIC
               ,:DFA-IMPST-252-ANTIC
                :IND-DFA-IMPST-252-ANTIC
               ,:DFA-DT-1A-CNBZ-DB
                :IND-DFA-DT-1A-CNBZ
               ,:DFA-COMMIS-DI-TRASFE
                :IND-DFA-COMMIS-DI-TRASFE
               ,:DFA-AA-CNBZ-K1
                :IND-DFA-AA-CNBZ-K1
               ,:DFA-AA-CNBZ-K2
                :IND-DFA-AA-CNBZ-K2
               ,:DFA-AA-CNBZ-K3
                :IND-DFA-AA-CNBZ-K3
               ,:DFA-MM-CNBZ-K1
                :IND-DFA-MM-CNBZ-K1
               ,:DFA-MM-CNBZ-K2
                :IND-DFA-MM-CNBZ-K2
               ,:DFA-MM-CNBZ-K3
                :IND-DFA-MM-CNBZ-K3
               ,:DFA-FL-APPLZ-NEWFIS
                :IND-DFA-FL-APPLZ-NEWFIS
               ,:DFA-REDT-TASS-ABBAT-K3
                :IND-DFA-REDT-TASS-ABBAT-K3
               ,:DFA-CNBT-ECC-4X100-K1
                :IND-DFA-CNBT-ECC-4X100-K1
               ,:DFA-DS-RIGA
               ,:DFA-DS-OPER-SQL
               ,:DFA-DS-VER
               ,:DFA-DS-TS-INI-CPTZ
               ,:DFA-DS-TS-END-CPTZ
               ,:DFA-DS-UTENTE
               ,:DFA-DS-STATO-ELAB
               ,:DFA-CREDITO-IS
                :IND-DFA-CREDITO-IS
               ,:DFA-REDT-TASS-ABBAT-K2
                :IND-DFA-REDT-TASS-ABBAT-K2
               ,:DFA-IMPB-IS-K3
                :IND-DFA-IMPB-IS-K3
               ,:DFA-IMPST-SOST-K3
                :IND-DFA-IMPST-SOST-K3
               ,:DFA-IMPB-252-K3
                :IND-DFA-IMPB-252-K3
               ,:DFA-IMPST-252-K3
                :IND-DFA-IMPST-252-K3
               ,:DFA-IMPB-IS-K3-ANTIC
                :IND-DFA-IMPB-IS-K3-ANTIC
               ,:DFA-IMPST-SOST-K3-ANTI
                :IND-DFA-IMPST-SOST-K3-ANTI
               ,:DFA-IMPB-IRPEF-K1-ANTI
                :IND-DFA-IMPB-IRPEF-K1-ANTI
               ,:DFA-IMPST-IRPEF-K1-ANT
                :IND-DFA-IMPST-IRPEF-K1-ANT
               ,:DFA-IMPB-IRPEF-K2-ANTI
                :IND-DFA-IMPB-IRPEF-K2-ANTI
               ,:DFA-IMPST-IRPEF-K2-ANT
                :IND-DFA-IMPST-IRPEF-K2-ANT
               ,:DFA-DT-CESSAZIONE-DB
                :IND-DFA-DT-CESSAZIONE
               ,:DFA-TOT-IMPST
                :IND-DFA-TOT-IMPST
               ,:DFA-ONER-TRASFE
                :IND-DFA-ONER-TRASFE
               ,:DFA-IMP-NET-TRASFERITO
                :IND-DFA-IMP-NET-TRASFERITO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_D_FISC_ADES
                ,ID_ADES
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_ISC_FND
                ,DT_ACCNS_RAPP_FND
                ,IMP_CNBT_AZ_K1
                ,IMP_CNBT_ISC_K1
                ,IMP_CNBT_TFR_K1
                ,IMP_CNBT_VOL_K1
                ,IMP_CNBT_AZ_K2
                ,IMP_CNBT_ISC_K2
                ,IMP_CNBT_TFR_K2
                ,IMP_CNBT_VOL_K2
                ,MATU_K1
                ,MATU_RES_K1
                ,MATU_K2
                ,IMPB_VIS
                ,IMPST_VIS
                ,IMPB_IS_K2
                ,IMPST_SOST_K2
                ,RIDZ_TFR
                ,PC_TFR
                ,ALQ_TFR
                ,TOT_ANTIC
                ,IMPB_TFR_ANTIC
                ,IMPST_TFR_ANTIC
                ,RIDZ_TFR_SU_ANTIC
                ,IMPB_VIS_ANTIC
                ,IMPST_VIS_ANTIC
                ,IMPB_IS_K2_ANTIC
                ,IMPST_SOST_K2_ANTI
                ,ULT_COMMIS_TRASFE
                ,COD_DVS
                ,ALQ_PRVR
                ,PC_ESE_IMPST_TFR
                ,IMP_ESE_IMPST_TFR
                ,ANZ_CNBTVA_CARASS
                ,ANZ_CNBTVA_CARAZI
                ,ANZ_SRVZ
                ,IMP_CNBT_NDED_K1
                ,IMP_CNBT_NDED_K2
                ,IMP_CNBT_NDED_K3
                ,IMP_CNBT_AZ_K3
                ,IMP_CNBT_ISC_K3
                ,IMP_CNBT_TFR_K3
                ,IMP_CNBT_VOL_K3
                ,MATU_K3
                ,IMPB_252_ANTIC
                ,IMPST_252_ANTIC
                ,DT_1A_CNBZ
                ,COMMIS_DI_TRASFE
                ,AA_CNBZ_K1
                ,AA_CNBZ_K2
                ,AA_CNBZ_K3
                ,MM_CNBZ_K1
                ,MM_CNBZ_K2
                ,MM_CNBZ_K3
                ,FL_APPLZ_NEWFIS
                ,REDT_TASS_ABBAT_K3
                ,CNBT_ECC_4X100_K1
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CREDITO_IS
                ,REDT_TASS_ABBAT_K2
                ,IMPB_IS_K3
                ,IMPST_SOST_K3
                ,IMPB_252_K3
                ,IMPST_252_K3
                ,IMPB_IS_K3_ANTIC
                ,IMPST_SOST_K3_ANTI
                ,IMPB_IRPEF_K1_ANTI
                ,IMPST_IRPEF_K1_ANT
                ,IMPB_IRPEF_K2_ANTI
                ,IMPST_IRPEF_K2_ANT
                ,DT_CESSAZIONE
                ,TOT_IMPST
                ,ONER_TRASFE
                ,IMP_NET_TRASFERITO
             INTO
                :DFA-ID-D-FISC-ADES
               ,:DFA-ID-ADES
               ,:DFA-ID-MOVI-CRZ
               ,:DFA-ID-MOVI-CHIU
                :IND-DFA-ID-MOVI-CHIU
               ,:DFA-DT-INI-EFF-DB
               ,:DFA-DT-END-EFF-DB
               ,:DFA-COD-COMP-ANIA
               ,:DFA-TP-ISC-FND
                :IND-DFA-TP-ISC-FND
               ,:DFA-DT-ACCNS-RAPP-FND-DB
                :IND-DFA-DT-ACCNS-RAPP-FND
               ,:DFA-IMP-CNBT-AZ-K1
                :IND-DFA-IMP-CNBT-AZ-K1
               ,:DFA-IMP-CNBT-ISC-K1
                :IND-DFA-IMP-CNBT-ISC-K1
               ,:DFA-IMP-CNBT-TFR-K1
                :IND-DFA-IMP-CNBT-TFR-K1
               ,:DFA-IMP-CNBT-VOL-K1
                :IND-DFA-IMP-CNBT-VOL-K1
               ,:DFA-IMP-CNBT-AZ-K2
                :IND-DFA-IMP-CNBT-AZ-K2
               ,:DFA-IMP-CNBT-ISC-K2
                :IND-DFA-IMP-CNBT-ISC-K2
               ,:DFA-IMP-CNBT-TFR-K2
                :IND-DFA-IMP-CNBT-TFR-K2
               ,:DFA-IMP-CNBT-VOL-K2
                :IND-DFA-IMP-CNBT-VOL-K2
               ,:DFA-MATU-K1
                :IND-DFA-MATU-K1
               ,:DFA-MATU-RES-K1
                :IND-DFA-MATU-RES-K1
               ,:DFA-MATU-K2
                :IND-DFA-MATU-K2
               ,:DFA-IMPB-VIS
                :IND-DFA-IMPB-VIS
               ,:DFA-IMPST-VIS
                :IND-DFA-IMPST-VIS
               ,:DFA-IMPB-IS-K2
                :IND-DFA-IMPB-IS-K2
               ,:DFA-IMPST-SOST-K2
                :IND-DFA-IMPST-SOST-K2
               ,:DFA-RIDZ-TFR
                :IND-DFA-RIDZ-TFR
               ,:DFA-PC-TFR
                :IND-DFA-PC-TFR
               ,:DFA-ALQ-TFR
                :IND-DFA-ALQ-TFR
               ,:DFA-TOT-ANTIC
                :IND-DFA-TOT-ANTIC
               ,:DFA-IMPB-TFR-ANTIC
                :IND-DFA-IMPB-TFR-ANTIC
               ,:DFA-IMPST-TFR-ANTIC
                :IND-DFA-IMPST-TFR-ANTIC
               ,:DFA-RIDZ-TFR-SU-ANTIC
                :IND-DFA-RIDZ-TFR-SU-ANTIC
               ,:DFA-IMPB-VIS-ANTIC
                :IND-DFA-IMPB-VIS-ANTIC
               ,:DFA-IMPST-VIS-ANTIC
                :IND-DFA-IMPST-VIS-ANTIC
               ,:DFA-IMPB-IS-K2-ANTIC
                :IND-DFA-IMPB-IS-K2-ANTIC
               ,:DFA-IMPST-SOST-K2-ANTI
                :IND-DFA-IMPST-SOST-K2-ANTI
               ,:DFA-ULT-COMMIS-TRASFE
                :IND-DFA-ULT-COMMIS-TRASFE
               ,:DFA-COD-DVS
                :IND-DFA-COD-DVS
               ,:DFA-ALQ-PRVR
                :IND-DFA-ALQ-PRVR
               ,:DFA-PC-ESE-IMPST-TFR
                :IND-DFA-PC-ESE-IMPST-TFR
               ,:DFA-IMP-ESE-IMPST-TFR
                :IND-DFA-IMP-ESE-IMPST-TFR
               ,:DFA-ANZ-CNBTVA-CARASS
                :IND-DFA-ANZ-CNBTVA-CARASS
               ,:DFA-ANZ-CNBTVA-CARAZI
                :IND-DFA-ANZ-CNBTVA-CARAZI
               ,:DFA-ANZ-SRVZ
                :IND-DFA-ANZ-SRVZ
               ,:DFA-IMP-CNBT-NDED-K1
                :IND-DFA-IMP-CNBT-NDED-K1
               ,:DFA-IMP-CNBT-NDED-K2
                :IND-DFA-IMP-CNBT-NDED-K2
               ,:DFA-IMP-CNBT-NDED-K3
                :IND-DFA-IMP-CNBT-NDED-K3
               ,:DFA-IMP-CNBT-AZ-K3
                :IND-DFA-IMP-CNBT-AZ-K3
               ,:DFA-IMP-CNBT-ISC-K3
                :IND-DFA-IMP-CNBT-ISC-K3
               ,:DFA-IMP-CNBT-TFR-K3
                :IND-DFA-IMP-CNBT-TFR-K3
               ,:DFA-IMP-CNBT-VOL-K3
                :IND-DFA-IMP-CNBT-VOL-K3
               ,:DFA-MATU-K3
                :IND-DFA-MATU-K3
               ,:DFA-IMPB-252-ANTIC
                :IND-DFA-IMPB-252-ANTIC
               ,:DFA-IMPST-252-ANTIC
                :IND-DFA-IMPST-252-ANTIC
               ,:DFA-DT-1A-CNBZ-DB
                :IND-DFA-DT-1A-CNBZ
               ,:DFA-COMMIS-DI-TRASFE
                :IND-DFA-COMMIS-DI-TRASFE
               ,:DFA-AA-CNBZ-K1
                :IND-DFA-AA-CNBZ-K1
               ,:DFA-AA-CNBZ-K2
                :IND-DFA-AA-CNBZ-K2
               ,:DFA-AA-CNBZ-K3
                :IND-DFA-AA-CNBZ-K3
               ,:DFA-MM-CNBZ-K1
                :IND-DFA-MM-CNBZ-K1
               ,:DFA-MM-CNBZ-K2
                :IND-DFA-MM-CNBZ-K2
               ,:DFA-MM-CNBZ-K3
                :IND-DFA-MM-CNBZ-K3
               ,:DFA-FL-APPLZ-NEWFIS
                :IND-DFA-FL-APPLZ-NEWFIS
               ,:DFA-REDT-TASS-ABBAT-K3
                :IND-DFA-REDT-TASS-ABBAT-K3
               ,:DFA-CNBT-ECC-4X100-K1
                :IND-DFA-CNBT-ECC-4X100-K1
               ,:DFA-DS-RIGA
               ,:DFA-DS-OPER-SQL
               ,:DFA-DS-VER
               ,:DFA-DS-TS-INI-CPTZ
               ,:DFA-DS-TS-END-CPTZ
               ,:DFA-DS-UTENTE
               ,:DFA-DS-STATO-ELAB
               ,:DFA-CREDITO-IS
                :IND-DFA-CREDITO-IS
               ,:DFA-REDT-TASS-ABBAT-K2
                :IND-DFA-REDT-TASS-ABBAT-K2
               ,:DFA-IMPB-IS-K3
                :IND-DFA-IMPB-IS-K3
               ,:DFA-IMPST-SOST-K3
                :IND-DFA-IMPST-SOST-K3
               ,:DFA-IMPB-252-K3
                :IND-DFA-IMPB-252-K3
               ,:DFA-IMPST-252-K3
                :IND-DFA-IMPST-252-K3
               ,:DFA-IMPB-IS-K3-ANTIC
                :IND-DFA-IMPB-IS-K3-ANTIC
               ,:DFA-IMPST-SOST-K3-ANTI
                :IND-DFA-IMPST-SOST-K3-ANTI
               ,:DFA-IMPB-IRPEF-K1-ANTI
                :IND-DFA-IMPB-IRPEF-K1-ANTI
               ,:DFA-IMPST-IRPEF-K1-ANT
                :IND-DFA-IMPST-IRPEF-K1-ANT
               ,:DFA-IMPB-IRPEF-K2-ANTI
                :IND-DFA-IMPB-IRPEF-K2-ANTI
               ,:DFA-IMPST-IRPEF-K2-ANT
                :IND-DFA-IMPST-IRPEF-K2-ANT
               ,:DFA-DT-CESSAZIONE-DB
                :IND-DFA-DT-CESSAZIONE
               ,:DFA-TOT-IMPST
                :IND-DFA-TOT-IMPST
               ,:DFA-ONER-TRASFE
                :IND-DFA-ONER-TRASFE
               ,:DFA-IMP-NET-TRASFERITO
                :IND-DFA-IMP-NET-TRASFERITO
             FROM D_FISC_ADES
             WHERE     ID_D_FISC_ADES = :DFA-ID-D-FISC-ADES
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-CPZ-DFA CURSOR FOR
              SELECT
                     ID_D_FISC_ADES
                    ,ID_ADES
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_ISC_FND
                    ,DT_ACCNS_RAPP_FND
                    ,IMP_CNBT_AZ_K1
                    ,IMP_CNBT_ISC_K1
                    ,IMP_CNBT_TFR_K1
                    ,IMP_CNBT_VOL_K1
                    ,IMP_CNBT_AZ_K2
                    ,IMP_CNBT_ISC_K2
                    ,IMP_CNBT_TFR_K2
                    ,IMP_CNBT_VOL_K2
                    ,MATU_K1
                    ,MATU_RES_K1
                    ,MATU_K2
                    ,IMPB_VIS
                    ,IMPST_VIS
                    ,IMPB_IS_K2
                    ,IMPST_SOST_K2
                    ,RIDZ_TFR
                    ,PC_TFR
                    ,ALQ_TFR
                    ,TOT_ANTIC
                    ,IMPB_TFR_ANTIC
                    ,IMPST_TFR_ANTIC
                    ,RIDZ_TFR_SU_ANTIC
                    ,IMPB_VIS_ANTIC
                    ,IMPST_VIS_ANTIC
                    ,IMPB_IS_K2_ANTIC
                    ,IMPST_SOST_K2_ANTI
                    ,ULT_COMMIS_TRASFE
                    ,COD_DVS
                    ,ALQ_PRVR
                    ,PC_ESE_IMPST_TFR
                    ,IMP_ESE_IMPST_TFR
                    ,ANZ_CNBTVA_CARASS
                    ,ANZ_CNBTVA_CARAZI
                    ,ANZ_SRVZ
                    ,IMP_CNBT_NDED_K1
                    ,IMP_CNBT_NDED_K2
                    ,IMP_CNBT_NDED_K3
                    ,IMP_CNBT_AZ_K3
                    ,IMP_CNBT_ISC_K3
                    ,IMP_CNBT_TFR_K3
                    ,IMP_CNBT_VOL_K3
                    ,MATU_K3
                    ,IMPB_252_ANTIC
                    ,IMPST_252_ANTIC
                    ,DT_1A_CNBZ
                    ,COMMIS_DI_TRASFE
                    ,AA_CNBZ_K1
                    ,AA_CNBZ_K2
                    ,AA_CNBZ_K3
                    ,MM_CNBZ_K1
                    ,MM_CNBZ_K2
                    ,MM_CNBZ_K3
                    ,FL_APPLZ_NEWFIS
                    ,REDT_TASS_ABBAT_K3
                    ,CNBT_ECC_4X100_K1
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,CREDITO_IS
                    ,REDT_TASS_ABBAT_K2
                    ,IMPB_IS_K3
                    ,IMPST_SOST_K3
                    ,IMPB_252_K3
                    ,IMPST_252_K3
                    ,IMPB_IS_K3_ANTIC
                    ,IMPST_SOST_K3_ANTI
                    ,IMPB_IRPEF_K1_ANTI
                    ,IMPST_IRPEF_K1_ANT
                    ,IMPB_IRPEF_K2_ANTI
                    ,IMPST_IRPEF_K2_ANT
                    ,DT_CESSAZIONE
                    ,TOT_IMPST
                    ,ONER_TRASFE
                    ,IMP_NET_TRASFERITO
              FROM D_FISC_ADES
              WHERE     ID_ADES = :DFA-ID-ADES
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_D_FISC_ADES ASC

           END-EXEC.

       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_D_FISC_ADES
                ,ID_ADES
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_ISC_FND
                ,DT_ACCNS_RAPP_FND
                ,IMP_CNBT_AZ_K1
                ,IMP_CNBT_ISC_K1
                ,IMP_CNBT_TFR_K1
                ,IMP_CNBT_VOL_K1
                ,IMP_CNBT_AZ_K2
                ,IMP_CNBT_ISC_K2
                ,IMP_CNBT_TFR_K2
                ,IMP_CNBT_VOL_K2
                ,MATU_K1
                ,MATU_RES_K1
                ,MATU_K2
                ,IMPB_VIS
                ,IMPST_VIS
                ,IMPB_IS_K2
                ,IMPST_SOST_K2
                ,RIDZ_TFR
                ,PC_TFR
                ,ALQ_TFR
                ,TOT_ANTIC
                ,IMPB_TFR_ANTIC
                ,IMPST_TFR_ANTIC
                ,RIDZ_TFR_SU_ANTIC
                ,IMPB_VIS_ANTIC
                ,IMPST_VIS_ANTIC
                ,IMPB_IS_K2_ANTIC
                ,IMPST_SOST_K2_ANTI
                ,ULT_COMMIS_TRASFE
                ,COD_DVS
                ,ALQ_PRVR
                ,PC_ESE_IMPST_TFR
                ,IMP_ESE_IMPST_TFR
                ,ANZ_CNBTVA_CARASS
                ,ANZ_CNBTVA_CARAZI
                ,ANZ_SRVZ
                ,IMP_CNBT_NDED_K1
                ,IMP_CNBT_NDED_K2
                ,IMP_CNBT_NDED_K3
                ,IMP_CNBT_AZ_K3
                ,IMP_CNBT_ISC_K3
                ,IMP_CNBT_TFR_K3
                ,IMP_CNBT_VOL_K3
                ,MATU_K3
                ,IMPB_252_ANTIC
                ,IMPST_252_ANTIC
                ,DT_1A_CNBZ
                ,COMMIS_DI_TRASFE
                ,AA_CNBZ_K1
                ,AA_CNBZ_K2
                ,AA_CNBZ_K3
                ,MM_CNBZ_K1
                ,MM_CNBZ_K2
                ,MM_CNBZ_K3
                ,FL_APPLZ_NEWFIS
                ,REDT_TASS_ABBAT_K3
                ,CNBT_ECC_4X100_K1
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CREDITO_IS
                ,REDT_TASS_ABBAT_K2
                ,IMPB_IS_K3
                ,IMPST_SOST_K3
                ,IMPB_252_K3
                ,IMPST_252_K3
                ,IMPB_IS_K3_ANTIC
                ,IMPST_SOST_K3_ANTI
                ,IMPB_IRPEF_K1_ANTI
                ,IMPST_IRPEF_K1_ANT
                ,IMPB_IRPEF_K2_ANTI
                ,IMPST_IRPEF_K2_ANT
                ,DT_CESSAZIONE
                ,TOT_IMPST
                ,ONER_TRASFE
                ,IMP_NET_TRASFERITO
             INTO
                :DFA-ID-D-FISC-ADES
               ,:DFA-ID-ADES
               ,:DFA-ID-MOVI-CRZ
               ,:DFA-ID-MOVI-CHIU
                :IND-DFA-ID-MOVI-CHIU
               ,:DFA-DT-INI-EFF-DB
               ,:DFA-DT-END-EFF-DB
               ,:DFA-COD-COMP-ANIA
               ,:DFA-TP-ISC-FND
                :IND-DFA-TP-ISC-FND
               ,:DFA-DT-ACCNS-RAPP-FND-DB
                :IND-DFA-DT-ACCNS-RAPP-FND
               ,:DFA-IMP-CNBT-AZ-K1
                :IND-DFA-IMP-CNBT-AZ-K1
               ,:DFA-IMP-CNBT-ISC-K1
                :IND-DFA-IMP-CNBT-ISC-K1
               ,:DFA-IMP-CNBT-TFR-K1
                :IND-DFA-IMP-CNBT-TFR-K1
               ,:DFA-IMP-CNBT-VOL-K1
                :IND-DFA-IMP-CNBT-VOL-K1
               ,:DFA-IMP-CNBT-AZ-K2
                :IND-DFA-IMP-CNBT-AZ-K2
               ,:DFA-IMP-CNBT-ISC-K2
                :IND-DFA-IMP-CNBT-ISC-K2
               ,:DFA-IMP-CNBT-TFR-K2
                :IND-DFA-IMP-CNBT-TFR-K2
               ,:DFA-IMP-CNBT-VOL-K2
                :IND-DFA-IMP-CNBT-VOL-K2
               ,:DFA-MATU-K1
                :IND-DFA-MATU-K1
               ,:DFA-MATU-RES-K1
                :IND-DFA-MATU-RES-K1
               ,:DFA-MATU-K2
                :IND-DFA-MATU-K2
               ,:DFA-IMPB-VIS
                :IND-DFA-IMPB-VIS
               ,:DFA-IMPST-VIS
                :IND-DFA-IMPST-VIS
               ,:DFA-IMPB-IS-K2
                :IND-DFA-IMPB-IS-K2
               ,:DFA-IMPST-SOST-K2
                :IND-DFA-IMPST-SOST-K2
               ,:DFA-RIDZ-TFR
                :IND-DFA-RIDZ-TFR
               ,:DFA-PC-TFR
                :IND-DFA-PC-TFR
               ,:DFA-ALQ-TFR
                :IND-DFA-ALQ-TFR
               ,:DFA-TOT-ANTIC
                :IND-DFA-TOT-ANTIC
               ,:DFA-IMPB-TFR-ANTIC
                :IND-DFA-IMPB-TFR-ANTIC
               ,:DFA-IMPST-TFR-ANTIC
                :IND-DFA-IMPST-TFR-ANTIC
               ,:DFA-RIDZ-TFR-SU-ANTIC
                :IND-DFA-RIDZ-TFR-SU-ANTIC
               ,:DFA-IMPB-VIS-ANTIC
                :IND-DFA-IMPB-VIS-ANTIC
               ,:DFA-IMPST-VIS-ANTIC
                :IND-DFA-IMPST-VIS-ANTIC
               ,:DFA-IMPB-IS-K2-ANTIC
                :IND-DFA-IMPB-IS-K2-ANTIC
               ,:DFA-IMPST-SOST-K2-ANTI
                :IND-DFA-IMPST-SOST-K2-ANTI
               ,:DFA-ULT-COMMIS-TRASFE
                :IND-DFA-ULT-COMMIS-TRASFE
               ,:DFA-COD-DVS
                :IND-DFA-COD-DVS
               ,:DFA-ALQ-PRVR
                :IND-DFA-ALQ-PRVR
               ,:DFA-PC-ESE-IMPST-TFR
                :IND-DFA-PC-ESE-IMPST-TFR
               ,:DFA-IMP-ESE-IMPST-TFR
                :IND-DFA-IMP-ESE-IMPST-TFR
               ,:DFA-ANZ-CNBTVA-CARASS
                :IND-DFA-ANZ-CNBTVA-CARASS
               ,:DFA-ANZ-CNBTVA-CARAZI
                :IND-DFA-ANZ-CNBTVA-CARAZI
               ,:DFA-ANZ-SRVZ
                :IND-DFA-ANZ-SRVZ
               ,:DFA-IMP-CNBT-NDED-K1
                :IND-DFA-IMP-CNBT-NDED-K1
               ,:DFA-IMP-CNBT-NDED-K2
                :IND-DFA-IMP-CNBT-NDED-K2
               ,:DFA-IMP-CNBT-NDED-K3
                :IND-DFA-IMP-CNBT-NDED-K3
               ,:DFA-IMP-CNBT-AZ-K3
                :IND-DFA-IMP-CNBT-AZ-K3
               ,:DFA-IMP-CNBT-ISC-K3
                :IND-DFA-IMP-CNBT-ISC-K3
               ,:DFA-IMP-CNBT-TFR-K3
                :IND-DFA-IMP-CNBT-TFR-K3
               ,:DFA-IMP-CNBT-VOL-K3
                :IND-DFA-IMP-CNBT-VOL-K3
               ,:DFA-MATU-K3
                :IND-DFA-MATU-K3
               ,:DFA-IMPB-252-ANTIC
                :IND-DFA-IMPB-252-ANTIC
               ,:DFA-IMPST-252-ANTIC
                :IND-DFA-IMPST-252-ANTIC
               ,:DFA-DT-1A-CNBZ-DB
                :IND-DFA-DT-1A-CNBZ
               ,:DFA-COMMIS-DI-TRASFE
                :IND-DFA-COMMIS-DI-TRASFE
               ,:DFA-AA-CNBZ-K1
                :IND-DFA-AA-CNBZ-K1
               ,:DFA-AA-CNBZ-K2
                :IND-DFA-AA-CNBZ-K2
               ,:DFA-AA-CNBZ-K3
                :IND-DFA-AA-CNBZ-K3
               ,:DFA-MM-CNBZ-K1
                :IND-DFA-MM-CNBZ-K1
               ,:DFA-MM-CNBZ-K2
                :IND-DFA-MM-CNBZ-K2
               ,:DFA-MM-CNBZ-K3
                :IND-DFA-MM-CNBZ-K3
               ,:DFA-FL-APPLZ-NEWFIS
                :IND-DFA-FL-APPLZ-NEWFIS
               ,:DFA-REDT-TASS-ABBAT-K3
                :IND-DFA-REDT-TASS-ABBAT-K3
               ,:DFA-CNBT-ECC-4X100-K1
                :IND-DFA-CNBT-ECC-4X100-K1
               ,:DFA-DS-RIGA
               ,:DFA-DS-OPER-SQL
               ,:DFA-DS-VER
               ,:DFA-DS-TS-INI-CPTZ
               ,:DFA-DS-TS-END-CPTZ
               ,:DFA-DS-UTENTE
               ,:DFA-DS-STATO-ELAB
               ,:DFA-CREDITO-IS
                :IND-DFA-CREDITO-IS
               ,:DFA-REDT-TASS-ABBAT-K2
                :IND-DFA-REDT-TASS-ABBAT-K2
               ,:DFA-IMPB-IS-K3
                :IND-DFA-IMPB-IS-K3
               ,:DFA-IMPST-SOST-K3
                :IND-DFA-IMPST-SOST-K3
               ,:DFA-IMPB-252-K3
                :IND-DFA-IMPB-252-K3
               ,:DFA-IMPST-252-K3
                :IND-DFA-IMPST-252-K3
               ,:DFA-IMPB-IS-K3-ANTIC
                :IND-DFA-IMPB-IS-K3-ANTIC
               ,:DFA-IMPST-SOST-K3-ANTI
                :IND-DFA-IMPST-SOST-K3-ANTI
               ,:DFA-IMPB-IRPEF-K1-ANTI
                :IND-DFA-IMPB-IRPEF-K1-ANTI
               ,:DFA-IMPST-IRPEF-K1-ANT
                :IND-DFA-IMPST-IRPEF-K1-ANT
               ,:DFA-IMPB-IRPEF-K2-ANTI
                :IND-DFA-IMPB-IRPEF-K2-ANTI
               ,:DFA-IMPST-IRPEF-K2-ANT
                :IND-DFA-IMPST-IRPEF-K2-ANT
               ,:DFA-DT-CESSAZIONE-DB
                :IND-DFA-DT-CESSAZIONE
               ,:DFA-TOT-IMPST
                :IND-DFA-TOT-IMPST
               ,:DFA-ONER-TRASFE
                :IND-DFA-ONER-TRASFE
               ,:DFA-IMP-NET-TRASFERITO
                :IND-DFA-IMP-NET-TRASFERITO
             FROM D_FISC_ADES
             WHERE     ID_ADES = :DFA-ID-ADES
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           EXEC SQL
                OPEN C-IDP-CPZ-DFA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           EXEC SQL
                CLOSE C-IDP-CPZ-DFA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           EXEC SQL
                FETCH C-IDP-CPZ-DFA
           INTO
                :DFA-ID-D-FISC-ADES
               ,:DFA-ID-ADES
               ,:DFA-ID-MOVI-CRZ
               ,:DFA-ID-MOVI-CHIU
                :IND-DFA-ID-MOVI-CHIU
               ,:DFA-DT-INI-EFF-DB
               ,:DFA-DT-END-EFF-DB
               ,:DFA-COD-COMP-ANIA
               ,:DFA-TP-ISC-FND
                :IND-DFA-TP-ISC-FND
               ,:DFA-DT-ACCNS-RAPP-FND-DB
                :IND-DFA-DT-ACCNS-RAPP-FND
               ,:DFA-IMP-CNBT-AZ-K1
                :IND-DFA-IMP-CNBT-AZ-K1
               ,:DFA-IMP-CNBT-ISC-K1
                :IND-DFA-IMP-CNBT-ISC-K1
               ,:DFA-IMP-CNBT-TFR-K1
                :IND-DFA-IMP-CNBT-TFR-K1
               ,:DFA-IMP-CNBT-VOL-K1
                :IND-DFA-IMP-CNBT-VOL-K1
               ,:DFA-IMP-CNBT-AZ-K2
                :IND-DFA-IMP-CNBT-AZ-K2
               ,:DFA-IMP-CNBT-ISC-K2
                :IND-DFA-IMP-CNBT-ISC-K2
               ,:DFA-IMP-CNBT-TFR-K2
                :IND-DFA-IMP-CNBT-TFR-K2
               ,:DFA-IMP-CNBT-VOL-K2
                :IND-DFA-IMP-CNBT-VOL-K2
               ,:DFA-MATU-K1
                :IND-DFA-MATU-K1
               ,:DFA-MATU-RES-K1
                :IND-DFA-MATU-RES-K1
               ,:DFA-MATU-K2
                :IND-DFA-MATU-K2
               ,:DFA-IMPB-VIS
                :IND-DFA-IMPB-VIS
               ,:DFA-IMPST-VIS
                :IND-DFA-IMPST-VIS
               ,:DFA-IMPB-IS-K2
                :IND-DFA-IMPB-IS-K2
               ,:DFA-IMPST-SOST-K2
                :IND-DFA-IMPST-SOST-K2
               ,:DFA-RIDZ-TFR
                :IND-DFA-RIDZ-TFR
               ,:DFA-PC-TFR
                :IND-DFA-PC-TFR
               ,:DFA-ALQ-TFR
                :IND-DFA-ALQ-TFR
               ,:DFA-TOT-ANTIC
                :IND-DFA-TOT-ANTIC
               ,:DFA-IMPB-TFR-ANTIC
                :IND-DFA-IMPB-TFR-ANTIC
               ,:DFA-IMPST-TFR-ANTIC
                :IND-DFA-IMPST-TFR-ANTIC
               ,:DFA-RIDZ-TFR-SU-ANTIC
                :IND-DFA-RIDZ-TFR-SU-ANTIC
               ,:DFA-IMPB-VIS-ANTIC
                :IND-DFA-IMPB-VIS-ANTIC
               ,:DFA-IMPST-VIS-ANTIC
                :IND-DFA-IMPST-VIS-ANTIC
               ,:DFA-IMPB-IS-K2-ANTIC
                :IND-DFA-IMPB-IS-K2-ANTIC
               ,:DFA-IMPST-SOST-K2-ANTI
                :IND-DFA-IMPST-SOST-K2-ANTI
               ,:DFA-ULT-COMMIS-TRASFE
                :IND-DFA-ULT-COMMIS-TRASFE
               ,:DFA-COD-DVS
                :IND-DFA-COD-DVS
               ,:DFA-ALQ-PRVR
                :IND-DFA-ALQ-PRVR
               ,:DFA-PC-ESE-IMPST-TFR
                :IND-DFA-PC-ESE-IMPST-TFR
               ,:DFA-IMP-ESE-IMPST-TFR
                :IND-DFA-IMP-ESE-IMPST-TFR
               ,:DFA-ANZ-CNBTVA-CARASS
                :IND-DFA-ANZ-CNBTVA-CARASS
               ,:DFA-ANZ-CNBTVA-CARAZI
                :IND-DFA-ANZ-CNBTVA-CARAZI
               ,:DFA-ANZ-SRVZ
                :IND-DFA-ANZ-SRVZ
               ,:DFA-IMP-CNBT-NDED-K1
                :IND-DFA-IMP-CNBT-NDED-K1
               ,:DFA-IMP-CNBT-NDED-K2
                :IND-DFA-IMP-CNBT-NDED-K2
               ,:DFA-IMP-CNBT-NDED-K3
                :IND-DFA-IMP-CNBT-NDED-K3
               ,:DFA-IMP-CNBT-AZ-K3
                :IND-DFA-IMP-CNBT-AZ-K3
               ,:DFA-IMP-CNBT-ISC-K3
                :IND-DFA-IMP-CNBT-ISC-K3
               ,:DFA-IMP-CNBT-TFR-K3
                :IND-DFA-IMP-CNBT-TFR-K3
               ,:DFA-IMP-CNBT-VOL-K3
                :IND-DFA-IMP-CNBT-VOL-K3
               ,:DFA-MATU-K3
                :IND-DFA-MATU-K3
               ,:DFA-IMPB-252-ANTIC
                :IND-DFA-IMPB-252-ANTIC
               ,:DFA-IMPST-252-ANTIC
                :IND-DFA-IMPST-252-ANTIC
               ,:DFA-DT-1A-CNBZ-DB
                :IND-DFA-DT-1A-CNBZ
               ,:DFA-COMMIS-DI-TRASFE
                :IND-DFA-COMMIS-DI-TRASFE
               ,:DFA-AA-CNBZ-K1
                :IND-DFA-AA-CNBZ-K1
               ,:DFA-AA-CNBZ-K2
                :IND-DFA-AA-CNBZ-K2
               ,:DFA-AA-CNBZ-K3
                :IND-DFA-AA-CNBZ-K3
               ,:DFA-MM-CNBZ-K1
                :IND-DFA-MM-CNBZ-K1
               ,:DFA-MM-CNBZ-K2
                :IND-DFA-MM-CNBZ-K2
               ,:DFA-MM-CNBZ-K3
                :IND-DFA-MM-CNBZ-K3
               ,:DFA-FL-APPLZ-NEWFIS
                :IND-DFA-FL-APPLZ-NEWFIS
               ,:DFA-REDT-TASS-ABBAT-K3
                :IND-DFA-REDT-TASS-ABBAT-K3
               ,:DFA-CNBT-ECC-4X100-K1
                :IND-DFA-CNBT-ECC-4X100-K1
               ,:DFA-DS-RIGA
               ,:DFA-DS-OPER-SQL
               ,:DFA-DS-VER
               ,:DFA-DS-TS-INI-CPTZ
               ,:DFA-DS-TS-END-CPTZ
               ,:DFA-DS-UTENTE
               ,:DFA-DS-STATO-ELAB
               ,:DFA-CREDITO-IS
                :IND-DFA-CREDITO-IS
               ,:DFA-REDT-TASS-ABBAT-K2
                :IND-DFA-REDT-TASS-ABBAT-K2
               ,:DFA-IMPB-IS-K3
                :IND-DFA-IMPB-IS-K3
               ,:DFA-IMPST-SOST-K3
                :IND-DFA-IMPST-SOST-K3
               ,:DFA-IMPB-252-K3
                :IND-DFA-IMPB-252-K3
               ,:DFA-IMPST-252-K3
                :IND-DFA-IMPST-252-K3
               ,:DFA-IMPB-IS-K3-ANTIC
                :IND-DFA-IMPB-IS-K3-ANTIC
               ,:DFA-IMPST-SOST-K3-ANTI
                :IND-DFA-IMPST-SOST-K3-ANTI
               ,:DFA-IMPB-IRPEF-K1-ANTI
                :IND-DFA-IMPB-IRPEF-K1-ANTI
               ,:DFA-IMPST-IRPEF-K1-ANT
                :IND-DFA-IMPST-IRPEF-K1-ANT
               ,:DFA-IMPB-IRPEF-K2-ANTI
                :IND-DFA-IMPB-IRPEF-K2-ANTI
               ,:DFA-IMPST-IRPEF-K2-ANT
                :IND-DFA-IMPST-IRPEF-K2-ANT
               ,:DFA-DT-CESSAZIONE-DB
                :IND-DFA-DT-CESSAZIONE
               ,:DFA-TOT-IMPST
                :IND-DFA-TOT-IMPST
               ,:DFA-ONER-TRASFE
                :IND-DFA-ONER-TRASFE
               ,:DFA-IMP-NET-TRASFERITO
                :IND-DFA-IMP-NET-TRASFERITO
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-DFA-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO DFA-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-DFA-TP-ISC-FND = -1
              MOVE HIGH-VALUES TO DFA-TP-ISC-FND-NULL
           END-IF
           IF IND-DFA-DT-ACCNS-RAPP-FND = -1
              MOVE HIGH-VALUES TO DFA-DT-ACCNS-RAPP-FND-NULL
           END-IF
           IF IND-DFA-IMP-CNBT-AZ-K1 = -1
              MOVE HIGH-VALUES TO DFA-IMP-CNBT-AZ-K1-NULL
           END-IF
           IF IND-DFA-IMP-CNBT-ISC-K1 = -1
              MOVE HIGH-VALUES TO DFA-IMP-CNBT-ISC-K1-NULL
           END-IF
           IF IND-DFA-IMP-CNBT-TFR-K1 = -1
              MOVE HIGH-VALUES TO DFA-IMP-CNBT-TFR-K1-NULL
           END-IF
           IF IND-DFA-IMP-CNBT-VOL-K1 = -1
              MOVE HIGH-VALUES TO DFA-IMP-CNBT-VOL-K1-NULL
           END-IF
           IF IND-DFA-IMP-CNBT-AZ-K2 = -1
              MOVE HIGH-VALUES TO DFA-IMP-CNBT-AZ-K2-NULL
           END-IF
           IF IND-DFA-IMP-CNBT-ISC-K2 = -1
              MOVE HIGH-VALUES TO DFA-IMP-CNBT-ISC-K2-NULL
           END-IF
           IF IND-DFA-IMP-CNBT-TFR-K2 = -1
              MOVE HIGH-VALUES TO DFA-IMP-CNBT-TFR-K2-NULL
           END-IF
           IF IND-DFA-IMP-CNBT-VOL-K2 = -1
              MOVE HIGH-VALUES TO DFA-IMP-CNBT-VOL-K2-NULL
           END-IF
           IF IND-DFA-MATU-K1 = -1
              MOVE HIGH-VALUES TO DFA-MATU-K1-NULL
           END-IF
           IF IND-DFA-MATU-RES-K1 = -1
              MOVE HIGH-VALUES TO DFA-MATU-RES-K1-NULL
           END-IF
           IF IND-DFA-MATU-K2 = -1
              MOVE HIGH-VALUES TO DFA-MATU-K2-NULL
           END-IF
           IF IND-DFA-IMPB-VIS = -1
              MOVE HIGH-VALUES TO DFA-IMPB-VIS-NULL
           END-IF
           IF IND-DFA-IMPST-VIS = -1
              MOVE HIGH-VALUES TO DFA-IMPST-VIS-NULL
           END-IF
           IF IND-DFA-IMPB-IS-K2 = -1
              MOVE HIGH-VALUES TO DFA-IMPB-IS-K2-NULL
           END-IF
           IF IND-DFA-IMPST-SOST-K2 = -1
              MOVE HIGH-VALUES TO DFA-IMPST-SOST-K2-NULL
           END-IF
           IF IND-DFA-RIDZ-TFR = -1
              MOVE HIGH-VALUES TO DFA-RIDZ-TFR-NULL
           END-IF
           IF IND-DFA-PC-TFR = -1
              MOVE HIGH-VALUES TO DFA-PC-TFR-NULL
           END-IF
           IF IND-DFA-ALQ-TFR = -1
              MOVE HIGH-VALUES TO DFA-ALQ-TFR-NULL
           END-IF
           IF IND-DFA-TOT-ANTIC = -1
              MOVE HIGH-VALUES TO DFA-TOT-ANTIC-NULL
           END-IF
           IF IND-DFA-IMPB-TFR-ANTIC = -1
              MOVE HIGH-VALUES TO DFA-IMPB-TFR-ANTIC-NULL
           END-IF
           IF IND-DFA-IMPST-TFR-ANTIC = -1
              MOVE HIGH-VALUES TO DFA-IMPST-TFR-ANTIC-NULL
           END-IF
           IF IND-DFA-RIDZ-TFR-SU-ANTIC = -1
              MOVE HIGH-VALUES TO DFA-RIDZ-TFR-SU-ANTIC-NULL
           END-IF
           IF IND-DFA-IMPB-VIS-ANTIC = -1
              MOVE HIGH-VALUES TO DFA-IMPB-VIS-ANTIC-NULL
           END-IF
           IF IND-DFA-IMPST-VIS-ANTIC = -1
              MOVE HIGH-VALUES TO DFA-IMPST-VIS-ANTIC-NULL
           END-IF
           IF IND-DFA-IMPB-IS-K2-ANTIC = -1
              MOVE HIGH-VALUES TO DFA-IMPB-IS-K2-ANTIC-NULL
           END-IF
           IF IND-DFA-IMPST-SOST-K2-ANTI = -1
              MOVE HIGH-VALUES TO DFA-IMPST-SOST-K2-ANTI-NULL
           END-IF
           IF IND-DFA-ULT-COMMIS-TRASFE = -1
              MOVE HIGH-VALUES TO DFA-ULT-COMMIS-TRASFE-NULL
           END-IF
           IF IND-DFA-COD-DVS = -1
              MOVE HIGH-VALUES TO DFA-COD-DVS-NULL
           END-IF
           IF IND-DFA-ALQ-PRVR = -1
              MOVE HIGH-VALUES TO DFA-ALQ-PRVR-NULL
           END-IF
           IF IND-DFA-PC-ESE-IMPST-TFR = -1
              MOVE HIGH-VALUES TO DFA-PC-ESE-IMPST-TFR-NULL
           END-IF
           IF IND-DFA-IMP-ESE-IMPST-TFR = -1
              MOVE HIGH-VALUES TO DFA-IMP-ESE-IMPST-TFR-NULL
           END-IF
           IF IND-DFA-ANZ-CNBTVA-CARASS = -1
              MOVE HIGH-VALUES TO DFA-ANZ-CNBTVA-CARASS-NULL
           END-IF
           IF IND-DFA-ANZ-CNBTVA-CARAZI = -1
              MOVE HIGH-VALUES TO DFA-ANZ-CNBTVA-CARAZI-NULL
           END-IF
           IF IND-DFA-ANZ-SRVZ = -1
              MOVE HIGH-VALUES TO DFA-ANZ-SRVZ-NULL
           END-IF
           IF IND-DFA-IMP-CNBT-NDED-K1 = -1
              MOVE HIGH-VALUES TO DFA-IMP-CNBT-NDED-K1-NULL
           END-IF
           IF IND-DFA-IMP-CNBT-NDED-K2 = -1
              MOVE HIGH-VALUES TO DFA-IMP-CNBT-NDED-K2-NULL
           END-IF
           IF IND-DFA-IMP-CNBT-NDED-K3 = -1
              MOVE HIGH-VALUES TO DFA-IMP-CNBT-NDED-K3-NULL
           END-IF
           IF IND-DFA-IMP-CNBT-AZ-K3 = -1
              MOVE HIGH-VALUES TO DFA-IMP-CNBT-AZ-K3-NULL
           END-IF
           IF IND-DFA-IMP-CNBT-ISC-K3 = -1
              MOVE HIGH-VALUES TO DFA-IMP-CNBT-ISC-K3-NULL
           END-IF
           IF IND-DFA-IMP-CNBT-TFR-K3 = -1
              MOVE HIGH-VALUES TO DFA-IMP-CNBT-TFR-K3-NULL
           END-IF
           IF IND-DFA-IMP-CNBT-VOL-K3 = -1
              MOVE HIGH-VALUES TO DFA-IMP-CNBT-VOL-K3-NULL
           END-IF
           IF IND-DFA-MATU-K3 = -1
              MOVE HIGH-VALUES TO DFA-MATU-K3-NULL
           END-IF
           IF IND-DFA-IMPB-252-ANTIC = -1
              MOVE HIGH-VALUES TO DFA-IMPB-252-ANTIC-NULL
           END-IF
           IF IND-DFA-IMPST-252-ANTIC = -1
              MOVE HIGH-VALUES TO DFA-IMPST-252-ANTIC-NULL
           END-IF
           IF IND-DFA-DT-1A-CNBZ = -1
              MOVE HIGH-VALUES TO DFA-DT-1A-CNBZ-NULL
           END-IF
           IF IND-DFA-COMMIS-DI-TRASFE = -1
              MOVE HIGH-VALUES TO DFA-COMMIS-DI-TRASFE-NULL
           END-IF
           IF IND-DFA-AA-CNBZ-K1 = -1
              MOVE HIGH-VALUES TO DFA-AA-CNBZ-K1-NULL
           END-IF
           IF IND-DFA-AA-CNBZ-K2 = -1
              MOVE HIGH-VALUES TO DFA-AA-CNBZ-K2-NULL
           END-IF
           IF IND-DFA-AA-CNBZ-K3 = -1
              MOVE HIGH-VALUES TO DFA-AA-CNBZ-K3-NULL
           END-IF
           IF IND-DFA-MM-CNBZ-K1 = -1
              MOVE HIGH-VALUES TO DFA-MM-CNBZ-K1-NULL
           END-IF
           IF IND-DFA-MM-CNBZ-K2 = -1
              MOVE HIGH-VALUES TO DFA-MM-CNBZ-K2-NULL
           END-IF
           IF IND-DFA-MM-CNBZ-K3 = -1
              MOVE HIGH-VALUES TO DFA-MM-CNBZ-K3-NULL
           END-IF
           IF IND-DFA-FL-APPLZ-NEWFIS = -1
              MOVE HIGH-VALUES TO DFA-FL-APPLZ-NEWFIS-NULL
           END-IF
           IF IND-DFA-REDT-TASS-ABBAT-K3 = -1
              MOVE HIGH-VALUES TO DFA-REDT-TASS-ABBAT-K3-NULL
           END-IF
           IF IND-DFA-CNBT-ECC-4X100-K1 = -1
              MOVE HIGH-VALUES TO DFA-CNBT-ECC-4X100-K1-NULL
           END-IF
           IF IND-DFA-CREDITO-IS = -1
              MOVE HIGH-VALUES TO DFA-CREDITO-IS-NULL
           END-IF
           IF IND-DFA-REDT-TASS-ABBAT-K2 = -1
              MOVE HIGH-VALUES TO DFA-REDT-TASS-ABBAT-K2-NULL
           END-IF
           IF IND-DFA-IMPB-IS-K3 = -1
              MOVE HIGH-VALUES TO DFA-IMPB-IS-K3-NULL
           END-IF
           IF IND-DFA-IMPST-SOST-K3 = -1
              MOVE HIGH-VALUES TO DFA-IMPST-SOST-K3-NULL
           END-IF
           IF IND-DFA-IMPB-252-K3 = -1
              MOVE HIGH-VALUES TO DFA-IMPB-252-K3-NULL
           END-IF
           IF IND-DFA-IMPST-252-K3 = -1
              MOVE HIGH-VALUES TO DFA-IMPST-252-K3-NULL
           END-IF
           IF IND-DFA-IMPB-IS-K3-ANTIC = -1
              MOVE HIGH-VALUES TO DFA-IMPB-IS-K3-ANTIC-NULL
           END-IF
           IF IND-DFA-IMPST-SOST-K3-ANTI = -1
              MOVE HIGH-VALUES TO DFA-IMPST-SOST-K3-ANTI-NULL
           END-IF
           IF IND-DFA-IMPB-IRPEF-K1-ANTI = -1
              MOVE HIGH-VALUES TO DFA-IMPB-IRPEF-K1-ANTI-NULL
           END-IF
           IF IND-DFA-IMPST-IRPEF-K1-ANT = -1
              MOVE HIGH-VALUES TO DFA-IMPST-IRPEF-K1-ANT-NULL
           END-IF
           IF IND-DFA-IMPB-IRPEF-K2-ANTI = -1
              MOVE HIGH-VALUES TO DFA-IMPB-IRPEF-K2-ANTI-NULL
           END-IF
           IF IND-DFA-IMPST-IRPEF-K2-ANT = -1
              MOVE HIGH-VALUES TO DFA-IMPST-IRPEF-K2-ANT-NULL
           END-IF
           IF IND-DFA-DT-CESSAZIONE = -1
              MOVE HIGH-VALUES TO DFA-DT-CESSAZIONE-NULL
           END-IF
           IF IND-DFA-TOT-IMPST = -1
              MOVE HIGH-VALUES TO DFA-TOT-IMPST-NULL
           END-IF
           IF IND-DFA-ONER-TRASFE = -1
              MOVE HIGH-VALUES TO DFA-ONER-TRASFE-NULL
           END-IF
           IF IND-DFA-IMP-NET-TRASFERITO = -1
              MOVE HIGH-VALUES TO DFA-IMP-NET-TRASFERITO-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO DFA-DS-OPER-SQL
           MOVE 0                   TO DFA-DS-VER
           MOVE IDSV0003-USER-NAME TO DFA-DS-UTENTE
           MOVE '1'                   TO DFA-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO DFA-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO DFA-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF DFA-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-DFA-ID-MOVI-CHIU
           END-IF
           IF DFA-TP-ISC-FND-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-TP-ISC-FND
           ELSE
              MOVE 0 TO IND-DFA-TP-ISC-FND
           END-IF
           IF DFA-DT-ACCNS-RAPP-FND-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-DT-ACCNS-RAPP-FND
           ELSE
              MOVE 0 TO IND-DFA-DT-ACCNS-RAPP-FND
           END-IF
           IF DFA-IMP-CNBT-AZ-K1-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-CNBT-AZ-K1
           ELSE
              MOVE 0 TO IND-DFA-IMP-CNBT-AZ-K1
           END-IF
           IF DFA-IMP-CNBT-ISC-K1-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-CNBT-ISC-K1
           ELSE
              MOVE 0 TO IND-DFA-IMP-CNBT-ISC-K1
           END-IF
           IF DFA-IMP-CNBT-TFR-K1-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-CNBT-TFR-K1
           ELSE
              MOVE 0 TO IND-DFA-IMP-CNBT-TFR-K1
           END-IF
           IF DFA-IMP-CNBT-VOL-K1-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-CNBT-VOL-K1
           ELSE
              MOVE 0 TO IND-DFA-IMP-CNBT-VOL-K1
           END-IF
           IF DFA-IMP-CNBT-AZ-K2-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-CNBT-AZ-K2
           ELSE
              MOVE 0 TO IND-DFA-IMP-CNBT-AZ-K2
           END-IF
           IF DFA-IMP-CNBT-ISC-K2-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-CNBT-ISC-K2
           ELSE
              MOVE 0 TO IND-DFA-IMP-CNBT-ISC-K2
           END-IF
           IF DFA-IMP-CNBT-TFR-K2-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-CNBT-TFR-K2
           ELSE
              MOVE 0 TO IND-DFA-IMP-CNBT-TFR-K2
           END-IF
           IF DFA-IMP-CNBT-VOL-K2-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-CNBT-VOL-K2
           ELSE
              MOVE 0 TO IND-DFA-IMP-CNBT-VOL-K2
           END-IF
           IF DFA-MATU-K1-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-MATU-K1
           ELSE
              MOVE 0 TO IND-DFA-MATU-K1
           END-IF
           IF DFA-MATU-RES-K1-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-MATU-RES-K1
           ELSE
              MOVE 0 TO IND-DFA-MATU-RES-K1
           END-IF
           IF DFA-MATU-K2-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-MATU-K2
           ELSE
              MOVE 0 TO IND-DFA-MATU-K2
           END-IF
           IF DFA-IMPB-VIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPB-VIS
           ELSE
              MOVE 0 TO IND-DFA-IMPB-VIS
           END-IF
           IF DFA-IMPST-VIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPST-VIS
           ELSE
              MOVE 0 TO IND-DFA-IMPST-VIS
           END-IF
           IF DFA-IMPB-IS-K2-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPB-IS-K2
           ELSE
              MOVE 0 TO IND-DFA-IMPB-IS-K2
           END-IF
           IF DFA-IMPST-SOST-K2-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPST-SOST-K2
           ELSE
              MOVE 0 TO IND-DFA-IMPST-SOST-K2
           END-IF
           IF DFA-RIDZ-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-RIDZ-TFR
           ELSE
              MOVE 0 TO IND-DFA-RIDZ-TFR
           END-IF
           IF DFA-PC-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-PC-TFR
           ELSE
              MOVE 0 TO IND-DFA-PC-TFR
           END-IF
           IF DFA-ALQ-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-ALQ-TFR
           ELSE
              MOVE 0 TO IND-DFA-ALQ-TFR
           END-IF
           IF DFA-TOT-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-TOT-ANTIC
           ELSE
              MOVE 0 TO IND-DFA-TOT-ANTIC
           END-IF
           IF DFA-IMPB-TFR-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPB-TFR-ANTIC
           ELSE
              MOVE 0 TO IND-DFA-IMPB-TFR-ANTIC
           END-IF
           IF DFA-IMPST-TFR-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPST-TFR-ANTIC
           ELSE
              MOVE 0 TO IND-DFA-IMPST-TFR-ANTIC
           END-IF
           IF DFA-RIDZ-TFR-SU-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-RIDZ-TFR-SU-ANTIC
           ELSE
              MOVE 0 TO IND-DFA-RIDZ-TFR-SU-ANTIC
           END-IF
           IF DFA-IMPB-VIS-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPB-VIS-ANTIC
           ELSE
              MOVE 0 TO IND-DFA-IMPB-VIS-ANTIC
           END-IF
           IF DFA-IMPST-VIS-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPST-VIS-ANTIC
           ELSE
              MOVE 0 TO IND-DFA-IMPST-VIS-ANTIC
           END-IF
           IF DFA-IMPB-IS-K2-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPB-IS-K2-ANTIC
           ELSE
              MOVE 0 TO IND-DFA-IMPB-IS-K2-ANTIC
           END-IF
           IF DFA-IMPST-SOST-K2-ANTI-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPST-SOST-K2-ANTI
           ELSE
              MOVE 0 TO IND-DFA-IMPST-SOST-K2-ANTI
           END-IF
           IF DFA-ULT-COMMIS-TRASFE-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-ULT-COMMIS-TRASFE
           ELSE
              MOVE 0 TO IND-DFA-ULT-COMMIS-TRASFE
           END-IF
           IF DFA-COD-DVS-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-COD-DVS
           ELSE
              MOVE 0 TO IND-DFA-COD-DVS
           END-IF
           IF DFA-ALQ-PRVR-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-ALQ-PRVR
           ELSE
              MOVE 0 TO IND-DFA-ALQ-PRVR
           END-IF
           IF DFA-PC-ESE-IMPST-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-PC-ESE-IMPST-TFR
           ELSE
              MOVE 0 TO IND-DFA-PC-ESE-IMPST-TFR
           END-IF
           IF DFA-IMP-ESE-IMPST-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-ESE-IMPST-TFR
           ELSE
              MOVE 0 TO IND-DFA-IMP-ESE-IMPST-TFR
           END-IF
           IF DFA-ANZ-CNBTVA-CARASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-ANZ-CNBTVA-CARASS
           ELSE
              MOVE 0 TO IND-DFA-ANZ-CNBTVA-CARASS
           END-IF
           IF DFA-ANZ-CNBTVA-CARAZI-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-ANZ-CNBTVA-CARAZI
           ELSE
              MOVE 0 TO IND-DFA-ANZ-CNBTVA-CARAZI
           END-IF
           IF DFA-ANZ-SRVZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-ANZ-SRVZ
           ELSE
              MOVE 0 TO IND-DFA-ANZ-SRVZ
           END-IF
           IF DFA-IMP-CNBT-NDED-K1-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-CNBT-NDED-K1
           ELSE
              MOVE 0 TO IND-DFA-IMP-CNBT-NDED-K1
           END-IF
           IF DFA-IMP-CNBT-NDED-K2-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-CNBT-NDED-K2
           ELSE
              MOVE 0 TO IND-DFA-IMP-CNBT-NDED-K2
           END-IF
           IF DFA-IMP-CNBT-NDED-K3-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-CNBT-NDED-K3
           ELSE
              MOVE 0 TO IND-DFA-IMP-CNBT-NDED-K3
           END-IF
           IF DFA-IMP-CNBT-AZ-K3-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-CNBT-AZ-K3
           ELSE
              MOVE 0 TO IND-DFA-IMP-CNBT-AZ-K3
           END-IF
           IF DFA-IMP-CNBT-ISC-K3-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-CNBT-ISC-K3
           ELSE
              MOVE 0 TO IND-DFA-IMP-CNBT-ISC-K3
           END-IF
           IF DFA-IMP-CNBT-TFR-K3-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-CNBT-TFR-K3
           ELSE
              MOVE 0 TO IND-DFA-IMP-CNBT-TFR-K3
           END-IF
           IF DFA-IMP-CNBT-VOL-K3-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-CNBT-VOL-K3
           ELSE
              MOVE 0 TO IND-DFA-IMP-CNBT-VOL-K3
           END-IF
           IF DFA-MATU-K3-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-MATU-K3
           ELSE
              MOVE 0 TO IND-DFA-MATU-K3
           END-IF
           IF DFA-IMPB-252-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPB-252-ANTIC
           ELSE
              MOVE 0 TO IND-DFA-IMPB-252-ANTIC
           END-IF
           IF DFA-IMPST-252-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPST-252-ANTIC
           ELSE
              MOVE 0 TO IND-DFA-IMPST-252-ANTIC
           END-IF
           IF DFA-DT-1A-CNBZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-DT-1A-CNBZ
           ELSE
              MOVE 0 TO IND-DFA-DT-1A-CNBZ
           END-IF
           IF DFA-COMMIS-DI-TRASFE-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-COMMIS-DI-TRASFE
           ELSE
              MOVE 0 TO IND-DFA-COMMIS-DI-TRASFE
           END-IF
           IF DFA-AA-CNBZ-K1-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-AA-CNBZ-K1
           ELSE
              MOVE 0 TO IND-DFA-AA-CNBZ-K1
           END-IF
           IF DFA-AA-CNBZ-K2-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-AA-CNBZ-K2
           ELSE
              MOVE 0 TO IND-DFA-AA-CNBZ-K2
           END-IF
           IF DFA-AA-CNBZ-K3-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-AA-CNBZ-K3
           ELSE
              MOVE 0 TO IND-DFA-AA-CNBZ-K3
           END-IF
           IF DFA-MM-CNBZ-K1-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-MM-CNBZ-K1
           ELSE
              MOVE 0 TO IND-DFA-MM-CNBZ-K1
           END-IF
           IF DFA-MM-CNBZ-K2-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-MM-CNBZ-K2
           ELSE
              MOVE 0 TO IND-DFA-MM-CNBZ-K2
           END-IF
           IF DFA-MM-CNBZ-K3-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-MM-CNBZ-K3
           ELSE
              MOVE 0 TO IND-DFA-MM-CNBZ-K3
           END-IF
           IF DFA-FL-APPLZ-NEWFIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-FL-APPLZ-NEWFIS
           ELSE
              MOVE 0 TO IND-DFA-FL-APPLZ-NEWFIS
           END-IF
           IF DFA-REDT-TASS-ABBAT-K3-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-REDT-TASS-ABBAT-K3
           ELSE
              MOVE 0 TO IND-DFA-REDT-TASS-ABBAT-K3
           END-IF
           IF DFA-CNBT-ECC-4X100-K1-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-CNBT-ECC-4X100-K1
           ELSE
              MOVE 0 TO IND-DFA-CNBT-ECC-4X100-K1
           END-IF
           IF DFA-CREDITO-IS-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-CREDITO-IS
           ELSE
              MOVE 0 TO IND-DFA-CREDITO-IS
           END-IF
           IF DFA-REDT-TASS-ABBAT-K2-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-REDT-TASS-ABBAT-K2
           ELSE
              MOVE 0 TO IND-DFA-REDT-TASS-ABBAT-K2
           END-IF
           IF DFA-IMPB-IS-K3-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPB-IS-K3
           ELSE
              MOVE 0 TO IND-DFA-IMPB-IS-K3
           END-IF
           IF DFA-IMPST-SOST-K3-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPST-SOST-K3
           ELSE
              MOVE 0 TO IND-DFA-IMPST-SOST-K3
           END-IF
           IF DFA-IMPB-252-K3-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPB-252-K3
           ELSE
              MOVE 0 TO IND-DFA-IMPB-252-K3
           END-IF
           IF DFA-IMPST-252-K3-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPST-252-K3
           ELSE
              MOVE 0 TO IND-DFA-IMPST-252-K3
           END-IF
           IF DFA-IMPB-IS-K3-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPB-IS-K3-ANTIC
           ELSE
              MOVE 0 TO IND-DFA-IMPB-IS-K3-ANTIC
           END-IF
           IF DFA-IMPST-SOST-K3-ANTI-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPST-SOST-K3-ANTI
           ELSE
              MOVE 0 TO IND-DFA-IMPST-SOST-K3-ANTI
           END-IF
           IF DFA-IMPB-IRPEF-K1-ANTI-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPB-IRPEF-K1-ANTI
           ELSE
              MOVE 0 TO IND-DFA-IMPB-IRPEF-K1-ANTI
           END-IF
           IF DFA-IMPST-IRPEF-K1-ANT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPST-IRPEF-K1-ANT
           ELSE
              MOVE 0 TO IND-DFA-IMPST-IRPEF-K1-ANT
           END-IF
           IF DFA-IMPB-IRPEF-K2-ANTI-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPB-IRPEF-K2-ANTI
           ELSE
              MOVE 0 TO IND-DFA-IMPB-IRPEF-K2-ANTI
           END-IF
           IF DFA-IMPST-IRPEF-K2-ANT-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMPST-IRPEF-K2-ANT
           ELSE
              MOVE 0 TO IND-DFA-IMPST-IRPEF-K2-ANT
           END-IF
           IF DFA-DT-CESSAZIONE-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-DT-CESSAZIONE
           ELSE
              MOVE 0 TO IND-DFA-DT-CESSAZIONE
           END-IF
           IF DFA-TOT-IMPST-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-TOT-IMPST
           ELSE
              MOVE 0 TO IND-DFA-TOT-IMPST
           END-IF
           IF DFA-ONER-TRASFE-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-ONER-TRASFE
           ELSE
              MOVE 0 TO IND-DFA-ONER-TRASFE
           END-IF
           IF DFA-IMP-NET-TRASFERITO-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFA-IMP-NET-TRASFERITO
           ELSE
              MOVE 0 TO IND-DFA-IMP-NET-TRASFERITO
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : DFA-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE D-FISC-ADES TO WS-BUFFER-TABLE.

           MOVE DFA-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO DFA-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO DFA-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO DFA-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO DFA-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO DFA-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO DFA-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO DFA-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE D-FISC-ADES TO WS-BUFFER-TABLE.

           MOVE DFA-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO D-FISC-ADES.

           MOVE WS-ID-MOVI-CRZ  TO DFA-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO DFA-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO DFA-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO DFA-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO DFA-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO DFA-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO DFA-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE DFA-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO DFA-DT-INI-EFF-DB
           MOVE DFA-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO DFA-DT-END-EFF-DB
           IF IND-DFA-DT-ACCNS-RAPP-FND = 0
               MOVE DFA-DT-ACCNS-RAPP-FND TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO DFA-DT-ACCNS-RAPP-FND-DB
           END-IF
           IF IND-DFA-DT-1A-CNBZ = 0
               MOVE DFA-DT-1A-CNBZ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO DFA-DT-1A-CNBZ-DB
           END-IF
           IF IND-DFA-DT-CESSAZIONE = 0
               MOVE DFA-DT-CESSAZIONE TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO DFA-DT-CESSAZIONE-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE DFA-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO DFA-DT-INI-EFF
           MOVE DFA-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO DFA-DT-END-EFF
           IF IND-DFA-DT-ACCNS-RAPP-FND = 0
               MOVE DFA-DT-ACCNS-RAPP-FND-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO DFA-DT-ACCNS-RAPP-FND
           END-IF
           IF IND-DFA-DT-1A-CNBZ = 0
               MOVE DFA-DT-1A-CNBZ-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO DFA-DT-1A-CNBZ
           END-IF
           IF IND-DFA-DT-CESSAZIONE = 0
               MOVE DFA-DT-CESSAZIONE-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO DFA-DT-CESSAZIONE
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

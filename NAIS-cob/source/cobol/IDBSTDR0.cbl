       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSTDR0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  05 DIC 2014.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDTDR0 END-EXEC.
           EXEC SQL INCLUDE IDBVTDR2 END-EXEC.
           EXEC SQL INCLUDE IDBVTDR3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVTDR1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 TIT-RAT.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSTDR0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'TIT_RAT' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TIT_RAT
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_TIT
                ,PROG_TIT
                ,TP_PRE_TIT
                ,TP_STAT_TIT
                ,DT_INI_COP
                ,DT_END_COP
                ,IMP_PAG
                ,FL_SOLL
                ,FRAZ
                ,DT_APPLZ_MORA
                ,FL_MORA
                ,ID_RAPP_RETE
                ,ID_RAPP_ANA
                ,COD_DVS
                ,DT_EMIS_TIT
                ,DT_ESI_TIT
                ,TOT_PRE_NET
                ,TOT_INTR_FRAZ
                ,TOT_INTR_MORA
                ,TOT_INTR_PREST
                ,TOT_INTR_RETDT
                ,TOT_INTR_RIAT
                ,TOT_DIR
                ,TOT_SPE_MED
                ,TOT_SPE_AGE
                ,TOT_TAX
                ,TOT_SOPR_SAN
                ,TOT_SOPR_TEC
                ,TOT_SOPR_SPO
                ,TOT_SOPR_PROF
                ,TOT_SOPR_ALT
                ,TOT_PRE_TOT
                ,TOT_PRE_PP_IAS
                ,TOT_CAR_IAS
                ,TOT_PRE_SOLO_RSH
                ,TOT_PROV_ACQ_1AA
                ,TOT_PROV_ACQ_2AA
                ,TOT_PROV_RICOR
                ,TOT_PROV_INC
                ,TOT_PROV_DA_REC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,FL_VLDT_TIT
                ,TOT_CAR_ACQ
                ,TOT_CAR_GEST
                ,TOT_CAR_INC
                ,TOT_MANFEE_ANTIC
                ,TOT_MANFEE_RICOR
                ,TOT_MANFEE_REC
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,TOT_ACQ_EXP
                ,TOT_REMUN_ASS
                ,TOT_COMMIS_INTER
                ,TOT_CNBT_ANTIRAC
                ,FL_INC_AUTOGEN
             INTO
                :TDR-ID-TIT-RAT
               ,:TDR-ID-OGG
               ,:TDR-TP-OGG
               ,:TDR-ID-MOVI-CRZ
               ,:TDR-ID-MOVI-CHIU
                :IND-TDR-ID-MOVI-CHIU
               ,:TDR-DT-INI-EFF-DB
               ,:TDR-DT-END-EFF-DB
               ,:TDR-COD-COMP-ANIA
               ,:TDR-TP-TIT
               ,:TDR-PROG-TIT
                :IND-TDR-PROG-TIT
               ,:TDR-TP-PRE-TIT
               ,:TDR-TP-STAT-TIT
               ,:TDR-DT-INI-COP-DB
                :IND-TDR-DT-INI-COP
               ,:TDR-DT-END-COP-DB
                :IND-TDR-DT-END-COP
               ,:TDR-IMP-PAG
                :IND-TDR-IMP-PAG
               ,:TDR-FL-SOLL
                :IND-TDR-FL-SOLL
               ,:TDR-FRAZ
                :IND-TDR-FRAZ
               ,:TDR-DT-APPLZ-MORA-DB
                :IND-TDR-DT-APPLZ-MORA
               ,:TDR-FL-MORA
                :IND-TDR-FL-MORA
               ,:TDR-ID-RAPP-RETE
                :IND-TDR-ID-RAPP-RETE
               ,:TDR-ID-RAPP-ANA
                :IND-TDR-ID-RAPP-ANA
               ,:TDR-COD-DVS
                :IND-TDR-COD-DVS
               ,:TDR-DT-EMIS-TIT-DB
               ,:TDR-DT-ESI-TIT-DB
                :IND-TDR-DT-ESI-TIT
               ,:TDR-TOT-PRE-NET
                :IND-TDR-TOT-PRE-NET
               ,:TDR-TOT-INTR-FRAZ
                :IND-TDR-TOT-INTR-FRAZ
               ,:TDR-TOT-INTR-MORA
                :IND-TDR-TOT-INTR-MORA
               ,:TDR-TOT-INTR-PREST
                :IND-TDR-TOT-INTR-PREST
               ,:TDR-TOT-INTR-RETDT
                :IND-TDR-TOT-INTR-RETDT
               ,:TDR-TOT-INTR-RIAT
                :IND-TDR-TOT-INTR-RIAT
               ,:TDR-TOT-DIR
                :IND-TDR-TOT-DIR
               ,:TDR-TOT-SPE-MED
                :IND-TDR-TOT-SPE-MED
               ,:TDR-TOT-SPE-AGE
                :IND-TDR-TOT-SPE-AGE
               ,:TDR-TOT-TAX
                :IND-TDR-TOT-TAX
               ,:TDR-TOT-SOPR-SAN
                :IND-TDR-TOT-SOPR-SAN
               ,:TDR-TOT-SOPR-TEC
                :IND-TDR-TOT-SOPR-TEC
               ,:TDR-TOT-SOPR-SPO
                :IND-TDR-TOT-SOPR-SPO
               ,:TDR-TOT-SOPR-PROF
                :IND-TDR-TOT-SOPR-PROF
               ,:TDR-TOT-SOPR-ALT
                :IND-TDR-TOT-SOPR-ALT
               ,:TDR-TOT-PRE-TOT
                :IND-TDR-TOT-PRE-TOT
               ,:TDR-TOT-PRE-PP-IAS
                :IND-TDR-TOT-PRE-PP-IAS
               ,:TDR-TOT-CAR-IAS
                :IND-TDR-TOT-CAR-IAS
               ,:TDR-TOT-PRE-SOLO-RSH
                :IND-TDR-TOT-PRE-SOLO-RSH
               ,:TDR-TOT-PROV-ACQ-1AA
                :IND-TDR-TOT-PROV-ACQ-1AA
               ,:TDR-TOT-PROV-ACQ-2AA
                :IND-TDR-TOT-PROV-ACQ-2AA
               ,:TDR-TOT-PROV-RICOR
                :IND-TDR-TOT-PROV-RICOR
               ,:TDR-TOT-PROV-INC
                :IND-TDR-TOT-PROV-INC
               ,:TDR-TOT-PROV-DA-REC
                :IND-TDR-TOT-PROV-DA-REC
               ,:TDR-IMP-AZ
                :IND-TDR-IMP-AZ
               ,:TDR-IMP-ADER
                :IND-TDR-IMP-ADER
               ,:TDR-IMP-TFR
                :IND-TDR-IMP-TFR
               ,:TDR-IMP-VOLO
                :IND-TDR-IMP-VOLO
               ,:TDR-FL-VLDT-TIT
                :IND-TDR-FL-VLDT-TIT
               ,:TDR-TOT-CAR-ACQ
                :IND-TDR-TOT-CAR-ACQ
               ,:TDR-TOT-CAR-GEST
                :IND-TDR-TOT-CAR-GEST
               ,:TDR-TOT-CAR-INC
                :IND-TDR-TOT-CAR-INC
               ,:TDR-TOT-MANFEE-ANTIC
                :IND-TDR-TOT-MANFEE-ANTIC
               ,:TDR-TOT-MANFEE-RICOR
                :IND-TDR-TOT-MANFEE-RICOR
               ,:TDR-TOT-MANFEE-REC
                :IND-TDR-TOT-MANFEE-REC
               ,:TDR-DS-RIGA
               ,:TDR-DS-OPER-SQL
               ,:TDR-DS-VER
               ,:TDR-DS-TS-INI-CPTZ
               ,:TDR-DS-TS-END-CPTZ
               ,:TDR-DS-UTENTE
               ,:TDR-DS-STATO-ELAB
               ,:TDR-IMP-TRASFE
                :IND-TDR-IMP-TRASFE
               ,:TDR-IMP-TFR-STRC
                :IND-TDR-IMP-TFR-STRC
               ,:TDR-TOT-ACQ-EXP
                :IND-TDR-TOT-ACQ-EXP
               ,:TDR-TOT-REMUN-ASS
                :IND-TDR-TOT-REMUN-ASS
               ,:TDR-TOT-COMMIS-INTER
                :IND-TDR-TOT-COMMIS-INTER
               ,:TDR-TOT-CNBT-ANTIRAC
                :IND-TDR-TOT-CNBT-ANTIRAC
               ,:TDR-FL-INC-AUTOGEN
                :IND-TDR-FL-INC-AUTOGEN
             FROM TIT_RAT
             WHERE     DS_RIGA = :TDR-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO TIT_RAT
                     (
                        ID_TIT_RAT
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_TIT
                       ,PROG_TIT
                       ,TP_PRE_TIT
                       ,TP_STAT_TIT
                       ,DT_INI_COP
                       ,DT_END_COP
                       ,IMP_PAG
                       ,FL_SOLL
                       ,FRAZ
                       ,DT_APPLZ_MORA
                       ,FL_MORA
                       ,ID_RAPP_RETE
                       ,ID_RAPP_ANA
                       ,COD_DVS
                       ,DT_EMIS_TIT
                       ,DT_ESI_TIT
                       ,TOT_PRE_NET
                       ,TOT_INTR_FRAZ
                       ,TOT_INTR_MORA
                       ,TOT_INTR_PREST
                       ,TOT_INTR_RETDT
                       ,TOT_INTR_RIAT
                       ,TOT_DIR
                       ,TOT_SPE_MED
                       ,TOT_SPE_AGE
                       ,TOT_TAX
                       ,TOT_SOPR_SAN
                       ,TOT_SOPR_TEC
                       ,TOT_SOPR_SPO
                       ,TOT_SOPR_PROF
                       ,TOT_SOPR_ALT
                       ,TOT_PRE_TOT
                       ,TOT_PRE_PP_IAS
                       ,TOT_CAR_IAS
                       ,TOT_PRE_SOLO_RSH
                       ,TOT_PROV_ACQ_1AA
                       ,TOT_PROV_ACQ_2AA
                       ,TOT_PROV_RICOR
                       ,TOT_PROV_INC
                       ,TOT_PROV_DA_REC
                       ,IMP_AZ
                       ,IMP_ADER
                       ,IMP_TFR
                       ,IMP_VOLO
                       ,FL_VLDT_TIT
                       ,TOT_CAR_ACQ
                       ,TOT_CAR_GEST
                       ,TOT_CAR_INC
                       ,TOT_MANFEE_ANTIC
                       ,TOT_MANFEE_RICOR
                       ,TOT_MANFEE_REC
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,IMP_TRASFE
                       ,IMP_TFR_STRC
                       ,TOT_ACQ_EXP
                       ,TOT_REMUN_ASS
                       ,TOT_COMMIS_INTER
                       ,TOT_CNBT_ANTIRAC
                       ,FL_INC_AUTOGEN
                     )
                 VALUES
                     (
                       :TDR-ID-TIT-RAT
                       ,:TDR-ID-OGG
                       ,:TDR-TP-OGG
                       ,:TDR-ID-MOVI-CRZ
                       ,:TDR-ID-MOVI-CHIU
                        :IND-TDR-ID-MOVI-CHIU
                       ,:TDR-DT-INI-EFF-DB
                       ,:TDR-DT-END-EFF-DB
                       ,:TDR-COD-COMP-ANIA
                       ,:TDR-TP-TIT
                       ,:TDR-PROG-TIT
                        :IND-TDR-PROG-TIT
                       ,:TDR-TP-PRE-TIT
                       ,:TDR-TP-STAT-TIT
                       ,:TDR-DT-INI-COP-DB
                        :IND-TDR-DT-INI-COP
                       ,:TDR-DT-END-COP-DB
                        :IND-TDR-DT-END-COP
                       ,:TDR-IMP-PAG
                        :IND-TDR-IMP-PAG
                       ,:TDR-FL-SOLL
                        :IND-TDR-FL-SOLL
                       ,:TDR-FRAZ
                        :IND-TDR-FRAZ
                       ,:TDR-DT-APPLZ-MORA-DB
                        :IND-TDR-DT-APPLZ-MORA
                       ,:TDR-FL-MORA
                        :IND-TDR-FL-MORA
                       ,:TDR-ID-RAPP-RETE
                        :IND-TDR-ID-RAPP-RETE
                       ,:TDR-ID-RAPP-ANA
                        :IND-TDR-ID-RAPP-ANA
                       ,:TDR-COD-DVS
                        :IND-TDR-COD-DVS
                       ,:TDR-DT-EMIS-TIT-DB
                       ,:TDR-DT-ESI-TIT-DB
                        :IND-TDR-DT-ESI-TIT
                       ,:TDR-TOT-PRE-NET
                        :IND-TDR-TOT-PRE-NET
                       ,:TDR-TOT-INTR-FRAZ
                        :IND-TDR-TOT-INTR-FRAZ
                       ,:TDR-TOT-INTR-MORA
                        :IND-TDR-TOT-INTR-MORA
                       ,:TDR-TOT-INTR-PREST
                        :IND-TDR-TOT-INTR-PREST
                       ,:TDR-TOT-INTR-RETDT
                        :IND-TDR-TOT-INTR-RETDT
                       ,:TDR-TOT-INTR-RIAT
                        :IND-TDR-TOT-INTR-RIAT
                       ,:TDR-TOT-DIR
                        :IND-TDR-TOT-DIR
                       ,:TDR-TOT-SPE-MED
                        :IND-TDR-TOT-SPE-MED
                       ,:TDR-TOT-SPE-AGE
                        :IND-TDR-TOT-SPE-AGE
                       ,:TDR-TOT-TAX
                        :IND-TDR-TOT-TAX
                       ,:TDR-TOT-SOPR-SAN
                        :IND-TDR-TOT-SOPR-SAN
                       ,:TDR-TOT-SOPR-TEC
                        :IND-TDR-TOT-SOPR-TEC
                       ,:TDR-TOT-SOPR-SPO
                        :IND-TDR-TOT-SOPR-SPO
                       ,:TDR-TOT-SOPR-PROF
                        :IND-TDR-TOT-SOPR-PROF
                       ,:TDR-TOT-SOPR-ALT
                        :IND-TDR-TOT-SOPR-ALT
                       ,:TDR-TOT-PRE-TOT
                        :IND-TDR-TOT-PRE-TOT
                       ,:TDR-TOT-PRE-PP-IAS
                        :IND-TDR-TOT-PRE-PP-IAS
                       ,:TDR-TOT-CAR-IAS
                        :IND-TDR-TOT-CAR-IAS
                       ,:TDR-TOT-PRE-SOLO-RSH
                        :IND-TDR-TOT-PRE-SOLO-RSH
                       ,:TDR-TOT-PROV-ACQ-1AA
                        :IND-TDR-TOT-PROV-ACQ-1AA
                       ,:TDR-TOT-PROV-ACQ-2AA
                        :IND-TDR-TOT-PROV-ACQ-2AA
                       ,:TDR-TOT-PROV-RICOR
                        :IND-TDR-TOT-PROV-RICOR
                       ,:TDR-TOT-PROV-INC
                        :IND-TDR-TOT-PROV-INC
                       ,:TDR-TOT-PROV-DA-REC
                        :IND-TDR-TOT-PROV-DA-REC
                       ,:TDR-IMP-AZ
                        :IND-TDR-IMP-AZ
                       ,:TDR-IMP-ADER
                        :IND-TDR-IMP-ADER
                       ,:TDR-IMP-TFR
                        :IND-TDR-IMP-TFR
                       ,:TDR-IMP-VOLO
                        :IND-TDR-IMP-VOLO
                       ,:TDR-FL-VLDT-TIT
                        :IND-TDR-FL-VLDT-TIT
                       ,:TDR-TOT-CAR-ACQ
                        :IND-TDR-TOT-CAR-ACQ
                       ,:TDR-TOT-CAR-GEST
                        :IND-TDR-TOT-CAR-GEST
                       ,:TDR-TOT-CAR-INC
                        :IND-TDR-TOT-CAR-INC
                       ,:TDR-TOT-MANFEE-ANTIC
                        :IND-TDR-TOT-MANFEE-ANTIC
                       ,:TDR-TOT-MANFEE-RICOR
                        :IND-TDR-TOT-MANFEE-RICOR
                       ,:TDR-TOT-MANFEE-REC
                        :IND-TDR-TOT-MANFEE-REC
                       ,:TDR-DS-RIGA
                       ,:TDR-DS-OPER-SQL
                       ,:TDR-DS-VER
                       ,:TDR-DS-TS-INI-CPTZ
                       ,:TDR-DS-TS-END-CPTZ
                       ,:TDR-DS-UTENTE
                       ,:TDR-DS-STATO-ELAB
                       ,:TDR-IMP-TRASFE
                        :IND-TDR-IMP-TRASFE
                       ,:TDR-IMP-TFR-STRC
                        :IND-TDR-IMP-TFR-STRC
                       ,:TDR-TOT-ACQ-EXP
                        :IND-TDR-TOT-ACQ-EXP
                       ,:TDR-TOT-REMUN-ASS
                        :IND-TDR-TOT-REMUN-ASS
                       ,:TDR-TOT-COMMIS-INTER
                        :IND-TDR-TOT-COMMIS-INTER
                       ,:TDR-TOT-CNBT-ANTIRAC
                        :IND-TDR-TOT-CNBT-ANTIRAC
                       ,:TDR-FL-INC-AUTOGEN
                        :IND-TDR-FL-INC-AUTOGEN
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE TIT_RAT SET

                   ID_TIT_RAT             =
                :TDR-ID-TIT-RAT
                  ,ID_OGG                 =
                :TDR-ID-OGG
                  ,TP_OGG                 =
                :TDR-TP-OGG
                  ,ID_MOVI_CRZ            =
                :TDR-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :TDR-ID-MOVI-CHIU
                                       :IND-TDR-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :TDR-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :TDR-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :TDR-COD-COMP-ANIA
                  ,TP_TIT                 =
                :TDR-TP-TIT
                  ,PROG_TIT               =
                :TDR-PROG-TIT
                                       :IND-TDR-PROG-TIT
                  ,TP_PRE_TIT             =
                :TDR-TP-PRE-TIT
                  ,TP_STAT_TIT            =
                :TDR-TP-STAT-TIT
                  ,DT_INI_COP             =
           :TDR-DT-INI-COP-DB
                                       :IND-TDR-DT-INI-COP
                  ,DT_END_COP             =
           :TDR-DT-END-COP-DB
                                       :IND-TDR-DT-END-COP
                  ,IMP_PAG                =
                :TDR-IMP-PAG
                                       :IND-TDR-IMP-PAG
                  ,FL_SOLL                =
                :TDR-FL-SOLL
                                       :IND-TDR-FL-SOLL
                  ,FRAZ                   =
                :TDR-FRAZ
                                       :IND-TDR-FRAZ
                  ,DT_APPLZ_MORA          =
           :TDR-DT-APPLZ-MORA-DB
                                       :IND-TDR-DT-APPLZ-MORA
                  ,FL_MORA                =
                :TDR-FL-MORA
                                       :IND-TDR-FL-MORA
                  ,ID_RAPP_RETE           =
                :TDR-ID-RAPP-RETE
                                       :IND-TDR-ID-RAPP-RETE
                  ,ID_RAPP_ANA            =
                :TDR-ID-RAPP-ANA
                                       :IND-TDR-ID-RAPP-ANA
                  ,COD_DVS                =
                :TDR-COD-DVS
                                       :IND-TDR-COD-DVS
                  ,DT_EMIS_TIT            =
           :TDR-DT-EMIS-TIT-DB
                  ,DT_ESI_TIT             =
           :TDR-DT-ESI-TIT-DB
                                       :IND-TDR-DT-ESI-TIT
                  ,TOT_PRE_NET            =
                :TDR-TOT-PRE-NET
                                       :IND-TDR-TOT-PRE-NET
                  ,TOT_INTR_FRAZ          =
                :TDR-TOT-INTR-FRAZ
                                       :IND-TDR-TOT-INTR-FRAZ
                  ,TOT_INTR_MORA          =
                :TDR-TOT-INTR-MORA
                                       :IND-TDR-TOT-INTR-MORA
                  ,TOT_INTR_PREST         =
                :TDR-TOT-INTR-PREST
                                       :IND-TDR-TOT-INTR-PREST
                  ,TOT_INTR_RETDT         =
                :TDR-TOT-INTR-RETDT
                                       :IND-TDR-TOT-INTR-RETDT
                  ,TOT_INTR_RIAT          =
                :TDR-TOT-INTR-RIAT
                                       :IND-TDR-TOT-INTR-RIAT
                  ,TOT_DIR                =
                :TDR-TOT-DIR
                                       :IND-TDR-TOT-DIR
                  ,TOT_SPE_MED            =
                :TDR-TOT-SPE-MED
                                       :IND-TDR-TOT-SPE-MED
                  ,TOT_SPE_AGE            =
                :TDR-TOT-SPE-AGE
                                       :IND-TDR-TOT-SPE-AGE
                  ,TOT_TAX                =
                :TDR-TOT-TAX
                                       :IND-TDR-TOT-TAX
                  ,TOT_SOPR_SAN           =
                :TDR-TOT-SOPR-SAN
                                       :IND-TDR-TOT-SOPR-SAN
                  ,TOT_SOPR_TEC           =
                :TDR-TOT-SOPR-TEC
                                       :IND-TDR-TOT-SOPR-TEC
                  ,TOT_SOPR_SPO           =
                :TDR-TOT-SOPR-SPO
                                       :IND-TDR-TOT-SOPR-SPO
                  ,TOT_SOPR_PROF          =
                :TDR-TOT-SOPR-PROF
                                       :IND-TDR-TOT-SOPR-PROF
                  ,TOT_SOPR_ALT           =
                :TDR-TOT-SOPR-ALT
                                       :IND-TDR-TOT-SOPR-ALT
                  ,TOT_PRE_TOT            =
                :TDR-TOT-PRE-TOT
                                       :IND-TDR-TOT-PRE-TOT
                  ,TOT_PRE_PP_IAS         =
                :TDR-TOT-PRE-PP-IAS
                                       :IND-TDR-TOT-PRE-PP-IAS
                  ,TOT_CAR_IAS            =
                :TDR-TOT-CAR-IAS
                                       :IND-TDR-TOT-CAR-IAS
                  ,TOT_PRE_SOLO_RSH       =
                :TDR-TOT-PRE-SOLO-RSH
                                       :IND-TDR-TOT-PRE-SOLO-RSH
                  ,TOT_PROV_ACQ_1AA       =
                :TDR-TOT-PROV-ACQ-1AA
                                       :IND-TDR-TOT-PROV-ACQ-1AA
                  ,TOT_PROV_ACQ_2AA       =
                :TDR-TOT-PROV-ACQ-2AA
                                       :IND-TDR-TOT-PROV-ACQ-2AA
                  ,TOT_PROV_RICOR         =
                :TDR-TOT-PROV-RICOR
                                       :IND-TDR-TOT-PROV-RICOR
                  ,TOT_PROV_INC           =
                :TDR-TOT-PROV-INC
                                       :IND-TDR-TOT-PROV-INC
                  ,TOT_PROV_DA_REC        =
                :TDR-TOT-PROV-DA-REC
                                       :IND-TDR-TOT-PROV-DA-REC
                  ,IMP_AZ                 =
                :TDR-IMP-AZ
                                       :IND-TDR-IMP-AZ
                  ,IMP_ADER               =
                :TDR-IMP-ADER
                                       :IND-TDR-IMP-ADER
                  ,IMP_TFR                =
                :TDR-IMP-TFR
                                       :IND-TDR-IMP-TFR
                  ,IMP_VOLO               =
                :TDR-IMP-VOLO
                                       :IND-TDR-IMP-VOLO
                  ,FL_VLDT_TIT            =
                :TDR-FL-VLDT-TIT
                                       :IND-TDR-FL-VLDT-TIT
                  ,TOT_CAR_ACQ            =
                :TDR-TOT-CAR-ACQ
                                       :IND-TDR-TOT-CAR-ACQ
                  ,TOT_CAR_GEST           =
                :TDR-TOT-CAR-GEST
                                       :IND-TDR-TOT-CAR-GEST
                  ,TOT_CAR_INC            =
                :TDR-TOT-CAR-INC
                                       :IND-TDR-TOT-CAR-INC
                  ,TOT_MANFEE_ANTIC       =
                :TDR-TOT-MANFEE-ANTIC
                                       :IND-TDR-TOT-MANFEE-ANTIC
                  ,TOT_MANFEE_RICOR       =
                :TDR-TOT-MANFEE-RICOR
                                       :IND-TDR-TOT-MANFEE-RICOR
                  ,TOT_MANFEE_REC         =
                :TDR-TOT-MANFEE-REC
                                       :IND-TDR-TOT-MANFEE-REC
                  ,DS_RIGA                =
                :TDR-DS-RIGA
                  ,DS_OPER_SQL            =
                :TDR-DS-OPER-SQL
                  ,DS_VER                 =
                :TDR-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :TDR-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :TDR-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :TDR-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :TDR-DS-STATO-ELAB
                  ,IMP_TRASFE             =
                :TDR-IMP-TRASFE
                                       :IND-TDR-IMP-TRASFE
                  ,IMP_TFR_STRC           =
                :TDR-IMP-TFR-STRC
                                       :IND-TDR-IMP-TFR-STRC
                  ,TOT_ACQ_EXP            =
                :TDR-TOT-ACQ-EXP
                                       :IND-TDR-TOT-ACQ-EXP
                  ,TOT_REMUN_ASS          =
                :TDR-TOT-REMUN-ASS
                                       :IND-TDR-TOT-REMUN-ASS
                  ,TOT_COMMIS_INTER       =
                :TDR-TOT-COMMIS-INTER
                                       :IND-TDR-TOT-COMMIS-INTER
                  ,TOT_CNBT_ANTIRAC       =
                :TDR-TOT-CNBT-ANTIRAC
                                       :IND-TDR-TOT-CNBT-ANTIRAC
                  ,FL_INC_AUTOGEN         =
                :TDR-FL-INC-AUTOGEN
                                       :IND-TDR-FL-INC-AUTOGEN
                WHERE     DS_RIGA = :TDR-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM TIT_RAT
                WHERE     DS_RIGA = :TDR-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-TDR CURSOR FOR
              SELECT
                     ID_TIT_RAT
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_TIT
                    ,PROG_TIT
                    ,TP_PRE_TIT
                    ,TP_STAT_TIT
                    ,DT_INI_COP
                    ,DT_END_COP
                    ,IMP_PAG
                    ,FL_SOLL
                    ,FRAZ
                    ,DT_APPLZ_MORA
                    ,FL_MORA
                    ,ID_RAPP_RETE
                    ,ID_RAPP_ANA
                    ,COD_DVS
                    ,DT_EMIS_TIT
                    ,DT_ESI_TIT
                    ,TOT_PRE_NET
                    ,TOT_INTR_FRAZ
                    ,TOT_INTR_MORA
                    ,TOT_INTR_PREST
                    ,TOT_INTR_RETDT
                    ,TOT_INTR_RIAT
                    ,TOT_DIR
                    ,TOT_SPE_MED
                    ,TOT_SPE_AGE
                    ,TOT_TAX
                    ,TOT_SOPR_SAN
                    ,TOT_SOPR_TEC
                    ,TOT_SOPR_SPO
                    ,TOT_SOPR_PROF
                    ,TOT_SOPR_ALT
                    ,TOT_PRE_TOT
                    ,TOT_PRE_PP_IAS
                    ,TOT_CAR_IAS
                    ,TOT_PRE_SOLO_RSH
                    ,TOT_PROV_ACQ_1AA
                    ,TOT_PROV_ACQ_2AA
                    ,TOT_PROV_RICOR
                    ,TOT_PROV_INC
                    ,TOT_PROV_DA_REC
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,FL_VLDT_TIT
                    ,TOT_CAR_ACQ
                    ,TOT_CAR_GEST
                    ,TOT_CAR_INC
                    ,TOT_MANFEE_ANTIC
                    ,TOT_MANFEE_RICOR
                    ,TOT_MANFEE_REC
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,TOT_ACQ_EXP
                    ,TOT_REMUN_ASS
                    ,TOT_COMMIS_INTER
                    ,TOT_CNBT_ANTIRAC
                    ,FL_INC_AUTOGEN
              FROM TIT_RAT
              WHERE     ID_TIT_RAT = :TDR-ID-TIT-RAT
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TIT_RAT
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_TIT
                ,PROG_TIT
                ,TP_PRE_TIT
                ,TP_STAT_TIT
                ,DT_INI_COP
                ,DT_END_COP
                ,IMP_PAG
                ,FL_SOLL
                ,FRAZ
                ,DT_APPLZ_MORA
                ,FL_MORA
                ,ID_RAPP_RETE
                ,ID_RAPP_ANA
                ,COD_DVS
                ,DT_EMIS_TIT
                ,DT_ESI_TIT
                ,TOT_PRE_NET
                ,TOT_INTR_FRAZ
                ,TOT_INTR_MORA
                ,TOT_INTR_PREST
                ,TOT_INTR_RETDT
                ,TOT_INTR_RIAT
                ,TOT_DIR
                ,TOT_SPE_MED
                ,TOT_SPE_AGE
                ,TOT_TAX
                ,TOT_SOPR_SAN
                ,TOT_SOPR_TEC
                ,TOT_SOPR_SPO
                ,TOT_SOPR_PROF
                ,TOT_SOPR_ALT
                ,TOT_PRE_TOT
                ,TOT_PRE_PP_IAS
                ,TOT_CAR_IAS
                ,TOT_PRE_SOLO_RSH
                ,TOT_PROV_ACQ_1AA
                ,TOT_PROV_ACQ_2AA
                ,TOT_PROV_RICOR
                ,TOT_PROV_INC
                ,TOT_PROV_DA_REC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,FL_VLDT_TIT
                ,TOT_CAR_ACQ
                ,TOT_CAR_GEST
                ,TOT_CAR_INC
                ,TOT_MANFEE_ANTIC
                ,TOT_MANFEE_RICOR
                ,TOT_MANFEE_REC
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,TOT_ACQ_EXP
                ,TOT_REMUN_ASS
                ,TOT_COMMIS_INTER
                ,TOT_CNBT_ANTIRAC
                ,FL_INC_AUTOGEN
             INTO
                :TDR-ID-TIT-RAT
               ,:TDR-ID-OGG
               ,:TDR-TP-OGG
               ,:TDR-ID-MOVI-CRZ
               ,:TDR-ID-MOVI-CHIU
                :IND-TDR-ID-MOVI-CHIU
               ,:TDR-DT-INI-EFF-DB
               ,:TDR-DT-END-EFF-DB
               ,:TDR-COD-COMP-ANIA
               ,:TDR-TP-TIT
               ,:TDR-PROG-TIT
                :IND-TDR-PROG-TIT
               ,:TDR-TP-PRE-TIT
               ,:TDR-TP-STAT-TIT
               ,:TDR-DT-INI-COP-DB
                :IND-TDR-DT-INI-COP
               ,:TDR-DT-END-COP-DB
                :IND-TDR-DT-END-COP
               ,:TDR-IMP-PAG
                :IND-TDR-IMP-PAG
               ,:TDR-FL-SOLL
                :IND-TDR-FL-SOLL
               ,:TDR-FRAZ
                :IND-TDR-FRAZ
               ,:TDR-DT-APPLZ-MORA-DB
                :IND-TDR-DT-APPLZ-MORA
               ,:TDR-FL-MORA
                :IND-TDR-FL-MORA
               ,:TDR-ID-RAPP-RETE
                :IND-TDR-ID-RAPP-RETE
               ,:TDR-ID-RAPP-ANA
                :IND-TDR-ID-RAPP-ANA
               ,:TDR-COD-DVS
                :IND-TDR-COD-DVS
               ,:TDR-DT-EMIS-TIT-DB
               ,:TDR-DT-ESI-TIT-DB
                :IND-TDR-DT-ESI-TIT
               ,:TDR-TOT-PRE-NET
                :IND-TDR-TOT-PRE-NET
               ,:TDR-TOT-INTR-FRAZ
                :IND-TDR-TOT-INTR-FRAZ
               ,:TDR-TOT-INTR-MORA
                :IND-TDR-TOT-INTR-MORA
               ,:TDR-TOT-INTR-PREST
                :IND-TDR-TOT-INTR-PREST
               ,:TDR-TOT-INTR-RETDT
                :IND-TDR-TOT-INTR-RETDT
               ,:TDR-TOT-INTR-RIAT
                :IND-TDR-TOT-INTR-RIAT
               ,:TDR-TOT-DIR
                :IND-TDR-TOT-DIR
               ,:TDR-TOT-SPE-MED
                :IND-TDR-TOT-SPE-MED
               ,:TDR-TOT-SPE-AGE
                :IND-TDR-TOT-SPE-AGE
               ,:TDR-TOT-TAX
                :IND-TDR-TOT-TAX
               ,:TDR-TOT-SOPR-SAN
                :IND-TDR-TOT-SOPR-SAN
               ,:TDR-TOT-SOPR-TEC
                :IND-TDR-TOT-SOPR-TEC
               ,:TDR-TOT-SOPR-SPO
                :IND-TDR-TOT-SOPR-SPO
               ,:TDR-TOT-SOPR-PROF
                :IND-TDR-TOT-SOPR-PROF
               ,:TDR-TOT-SOPR-ALT
                :IND-TDR-TOT-SOPR-ALT
               ,:TDR-TOT-PRE-TOT
                :IND-TDR-TOT-PRE-TOT
               ,:TDR-TOT-PRE-PP-IAS
                :IND-TDR-TOT-PRE-PP-IAS
               ,:TDR-TOT-CAR-IAS
                :IND-TDR-TOT-CAR-IAS
               ,:TDR-TOT-PRE-SOLO-RSH
                :IND-TDR-TOT-PRE-SOLO-RSH
               ,:TDR-TOT-PROV-ACQ-1AA
                :IND-TDR-TOT-PROV-ACQ-1AA
               ,:TDR-TOT-PROV-ACQ-2AA
                :IND-TDR-TOT-PROV-ACQ-2AA
               ,:TDR-TOT-PROV-RICOR
                :IND-TDR-TOT-PROV-RICOR
               ,:TDR-TOT-PROV-INC
                :IND-TDR-TOT-PROV-INC
               ,:TDR-TOT-PROV-DA-REC
                :IND-TDR-TOT-PROV-DA-REC
               ,:TDR-IMP-AZ
                :IND-TDR-IMP-AZ
               ,:TDR-IMP-ADER
                :IND-TDR-IMP-ADER
               ,:TDR-IMP-TFR
                :IND-TDR-IMP-TFR
               ,:TDR-IMP-VOLO
                :IND-TDR-IMP-VOLO
               ,:TDR-FL-VLDT-TIT
                :IND-TDR-FL-VLDT-TIT
               ,:TDR-TOT-CAR-ACQ
                :IND-TDR-TOT-CAR-ACQ
               ,:TDR-TOT-CAR-GEST
                :IND-TDR-TOT-CAR-GEST
               ,:TDR-TOT-CAR-INC
                :IND-TDR-TOT-CAR-INC
               ,:TDR-TOT-MANFEE-ANTIC
                :IND-TDR-TOT-MANFEE-ANTIC
               ,:TDR-TOT-MANFEE-RICOR
                :IND-TDR-TOT-MANFEE-RICOR
               ,:TDR-TOT-MANFEE-REC
                :IND-TDR-TOT-MANFEE-REC
               ,:TDR-DS-RIGA
               ,:TDR-DS-OPER-SQL
               ,:TDR-DS-VER
               ,:TDR-DS-TS-INI-CPTZ
               ,:TDR-DS-TS-END-CPTZ
               ,:TDR-DS-UTENTE
               ,:TDR-DS-STATO-ELAB
               ,:TDR-IMP-TRASFE
                :IND-TDR-IMP-TRASFE
               ,:TDR-IMP-TFR-STRC
                :IND-TDR-IMP-TFR-STRC
               ,:TDR-TOT-ACQ-EXP
                :IND-TDR-TOT-ACQ-EXP
               ,:TDR-TOT-REMUN-ASS
                :IND-TDR-TOT-REMUN-ASS
               ,:TDR-TOT-COMMIS-INTER
                :IND-TDR-TOT-COMMIS-INTER
               ,:TDR-TOT-CNBT-ANTIRAC
                :IND-TDR-TOT-CNBT-ANTIRAC
               ,:TDR-FL-INC-AUTOGEN
                :IND-TDR-FL-INC-AUTOGEN
             FROM TIT_RAT
             WHERE     ID_TIT_RAT = :TDR-ID-TIT-RAT
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE TIT_RAT SET

                   ID_TIT_RAT             =
                :TDR-ID-TIT-RAT
                  ,ID_OGG                 =
                :TDR-ID-OGG
                  ,TP_OGG                 =
                :TDR-TP-OGG
                  ,ID_MOVI_CRZ            =
                :TDR-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :TDR-ID-MOVI-CHIU
                                       :IND-TDR-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :TDR-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :TDR-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :TDR-COD-COMP-ANIA
                  ,TP_TIT                 =
                :TDR-TP-TIT
                  ,PROG_TIT               =
                :TDR-PROG-TIT
                                       :IND-TDR-PROG-TIT
                  ,TP_PRE_TIT             =
                :TDR-TP-PRE-TIT
                  ,TP_STAT_TIT            =
                :TDR-TP-STAT-TIT
                  ,DT_INI_COP             =
           :TDR-DT-INI-COP-DB
                                       :IND-TDR-DT-INI-COP
                  ,DT_END_COP             =
           :TDR-DT-END-COP-DB
                                       :IND-TDR-DT-END-COP
                  ,IMP_PAG                =
                :TDR-IMP-PAG
                                       :IND-TDR-IMP-PAG
                  ,FL_SOLL                =
                :TDR-FL-SOLL
                                       :IND-TDR-FL-SOLL
                  ,FRAZ                   =
                :TDR-FRAZ
                                       :IND-TDR-FRAZ
                  ,DT_APPLZ_MORA          =
           :TDR-DT-APPLZ-MORA-DB
                                       :IND-TDR-DT-APPLZ-MORA
                  ,FL_MORA                =
                :TDR-FL-MORA
                                       :IND-TDR-FL-MORA
                  ,ID_RAPP_RETE           =
                :TDR-ID-RAPP-RETE
                                       :IND-TDR-ID-RAPP-RETE
                  ,ID_RAPP_ANA            =
                :TDR-ID-RAPP-ANA
                                       :IND-TDR-ID-RAPP-ANA
                  ,COD_DVS                =
                :TDR-COD-DVS
                                       :IND-TDR-COD-DVS
                  ,DT_EMIS_TIT            =
           :TDR-DT-EMIS-TIT-DB
                  ,DT_ESI_TIT             =
           :TDR-DT-ESI-TIT-DB
                                       :IND-TDR-DT-ESI-TIT
                  ,TOT_PRE_NET            =
                :TDR-TOT-PRE-NET
                                       :IND-TDR-TOT-PRE-NET
                  ,TOT_INTR_FRAZ          =
                :TDR-TOT-INTR-FRAZ
                                       :IND-TDR-TOT-INTR-FRAZ
                  ,TOT_INTR_MORA          =
                :TDR-TOT-INTR-MORA
                                       :IND-TDR-TOT-INTR-MORA
                  ,TOT_INTR_PREST         =
                :TDR-TOT-INTR-PREST
                                       :IND-TDR-TOT-INTR-PREST
                  ,TOT_INTR_RETDT         =
                :TDR-TOT-INTR-RETDT
                                       :IND-TDR-TOT-INTR-RETDT
                  ,TOT_INTR_RIAT          =
                :TDR-TOT-INTR-RIAT
                                       :IND-TDR-TOT-INTR-RIAT
                  ,TOT_DIR                =
                :TDR-TOT-DIR
                                       :IND-TDR-TOT-DIR
                  ,TOT_SPE_MED            =
                :TDR-TOT-SPE-MED
                                       :IND-TDR-TOT-SPE-MED
                  ,TOT_SPE_AGE            =
                :TDR-TOT-SPE-AGE
                                       :IND-TDR-TOT-SPE-AGE
                  ,TOT_TAX                =
                :TDR-TOT-TAX
                                       :IND-TDR-TOT-TAX
                  ,TOT_SOPR_SAN           =
                :TDR-TOT-SOPR-SAN
                                       :IND-TDR-TOT-SOPR-SAN
                  ,TOT_SOPR_TEC           =
                :TDR-TOT-SOPR-TEC
                                       :IND-TDR-TOT-SOPR-TEC
                  ,TOT_SOPR_SPO           =
                :TDR-TOT-SOPR-SPO
                                       :IND-TDR-TOT-SOPR-SPO
                  ,TOT_SOPR_PROF          =
                :TDR-TOT-SOPR-PROF
                                       :IND-TDR-TOT-SOPR-PROF
                  ,TOT_SOPR_ALT           =
                :TDR-TOT-SOPR-ALT
                                       :IND-TDR-TOT-SOPR-ALT
                  ,TOT_PRE_TOT            =
                :TDR-TOT-PRE-TOT
                                       :IND-TDR-TOT-PRE-TOT
                  ,TOT_PRE_PP_IAS         =
                :TDR-TOT-PRE-PP-IAS
                                       :IND-TDR-TOT-PRE-PP-IAS
                  ,TOT_CAR_IAS            =
                :TDR-TOT-CAR-IAS
                                       :IND-TDR-TOT-CAR-IAS
                  ,TOT_PRE_SOLO_RSH       =
                :TDR-TOT-PRE-SOLO-RSH
                                       :IND-TDR-TOT-PRE-SOLO-RSH
                  ,TOT_PROV_ACQ_1AA       =
                :TDR-TOT-PROV-ACQ-1AA
                                       :IND-TDR-TOT-PROV-ACQ-1AA
                  ,TOT_PROV_ACQ_2AA       =
                :TDR-TOT-PROV-ACQ-2AA
                                       :IND-TDR-TOT-PROV-ACQ-2AA
                  ,TOT_PROV_RICOR         =
                :TDR-TOT-PROV-RICOR
                                       :IND-TDR-TOT-PROV-RICOR
                  ,TOT_PROV_INC           =
                :TDR-TOT-PROV-INC
                                       :IND-TDR-TOT-PROV-INC
                  ,TOT_PROV_DA_REC        =
                :TDR-TOT-PROV-DA-REC
                                       :IND-TDR-TOT-PROV-DA-REC
                  ,IMP_AZ                 =
                :TDR-IMP-AZ
                                       :IND-TDR-IMP-AZ
                  ,IMP_ADER               =
                :TDR-IMP-ADER
                                       :IND-TDR-IMP-ADER
                  ,IMP_TFR                =
                :TDR-IMP-TFR
                                       :IND-TDR-IMP-TFR
                  ,IMP_VOLO               =
                :TDR-IMP-VOLO
                                       :IND-TDR-IMP-VOLO
                  ,FL_VLDT_TIT            =
                :TDR-FL-VLDT-TIT
                                       :IND-TDR-FL-VLDT-TIT
                  ,TOT_CAR_ACQ            =
                :TDR-TOT-CAR-ACQ
                                       :IND-TDR-TOT-CAR-ACQ
                  ,TOT_CAR_GEST           =
                :TDR-TOT-CAR-GEST
                                       :IND-TDR-TOT-CAR-GEST
                  ,TOT_CAR_INC            =
                :TDR-TOT-CAR-INC
                                       :IND-TDR-TOT-CAR-INC
                  ,TOT_MANFEE_ANTIC       =
                :TDR-TOT-MANFEE-ANTIC
                                       :IND-TDR-TOT-MANFEE-ANTIC
                  ,TOT_MANFEE_RICOR       =
                :TDR-TOT-MANFEE-RICOR
                                       :IND-TDR-TOT-MANFEE-RICOR
                  ,TOT_MANFEE_REC         =
                :TDR-TOT-MANFEE-REC
                                       :IND-TDR-TOT-MANFEE-REC
                  ,DS_RIGA                =
                :TDR-DS-RIGA
                  ,DS_OPER_SQL            =
                :TDR-DS-OPER-SQL
                  ,DS_VER                 =
                :TDR-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :TDR-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :TDR-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :TDR-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :TDR-DS-STATO-ELAB
                  ,IMP_TRASFE             =
                :TDR-IMP-TRASFE
                                       :IND-TDR-IMP-TRASFE
                  ,IMP_TFR_STRC           =
                :TDR-IMP-TFR-STRC
                                       :IND-TDR-IMP-TFR-STRC
                  ,TOT_ACQ_EXP            =
                :TDR-TOT-ACQ-EXP
                                       :IND-TDR-TOT-ACQ-EXP
                  ,TOT_REMUN_ASS          =
                :TDR-TOT-REMUN-ASS
                                       :IND-TDR-TOT-REMUN-ASS
                  ,TOT_COMMIS_INTER       =
                :TDR-TOT-COMMIS-INTER
                                       :IND-TDR-TOT-COMMIS-INTER
                  ,TOT_CNBT_ANTIRAC       =
                :TDR-TOT-CNBT-ANTIRAC
                                       :IND-TDR-TOT-CNBT-ANTIRAC
                  ,FL_INC_AUTOGEN         =
                :TDR-FL-INC-AUTOGEN
                                       :IND-TDR-FL-INC-AUTOGEN
                WHERE     DS_RIGA = :TDR-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-TDR
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-TDR
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-TDR
           INTO
                :TDR-ID-TIT-RAT
               ,:TDR-ID-OGG
               ,:TDR-TP-OGG
               ,:TDR-ID-MOVI-CRZ
               ,:TDR-ID-MOVI-CHIU
                :IND-TDR-ID-MOVI-CHIU
               ,:TDR-DT-INI-EFF-DB
               ,:TDR-DT-END-EFF-DB
               ,:TDR-COD-COMP-ANIA
               ,:TDR-TP-TIT
               ,:TDR-PROG-TIT
                :IND-TDR-PROG-TIT
               ,:TDR-TP-PRE-TIT
               ,:TDR-TP-STAT-TIT
               ,:TDR-DT-INI-COP-DB
                :IND-TDR-DT-INI-COP
               ,:TDR-DT-END-COP-DB
                :IND-TDR-DT-END-COP
               ,:TDR-IMP-PAG
                :IND-TDR-IMP-PAG
               ,:TDR-FL-SOLL
                :IND-TDR-FL-SOLL
               ,:TDR-FRAZ
                :IND-TDR-FRAZ
               ,:TDR-DT-APPLZ-MORA-DB
                :IND-TDR-DT-APPLZ-MORA
               ,:TDR-FL-MORA
                :IND-TDR-FL-MORA
               ,:TDR-ID-RAPP-RETE
                :IND-TDR-ID-RAPP-RETE
               ,:TDR-ID-RAPP-ANA
                :IND-TDR-ID-RAPP-ANA
               ,:TDR-COD-DVS
                :IND-TDR-COD-DVS
               ,:TDR-DT-EMIS-TIT-DB
               ,:TDR-DT-ESI-TIT-DB
                :IND-TDR-DT-ESI-TIT
               ,:TDR-TOT-PRE-NET
                :IND-TDR-TOT-PRE-NET
               ,:TDR-TOT-INTR-FRAZ
                :IND-TDR-TOT-INTR-FRAZ
               ,:TDR-TOT-INTR-MORA
                :IND-TDR-TOT-INTR-MORA
               ,:TDR-TOT-INTR-PREST
                :IND-TDR-TOT-INTR-PREST
               ,:TDR-TOT-INTR-RETDT
                :IND-TDR-TOT-INTR-RETDT
               ,:TDR-TOT-INTR-RIAT
                :IND-TDR-TOT-INTR-RIAT
               ,:TDR-TOT-DIR
                :IND-TDR-TOT-DIR
               ,:TDR-TOT-SPE-MED
                :IND-TDR-TOT-SPE-MED
               ,:TDR-TOT-SPE-AGE
                :IND-TDR-TOT-SPE-AGE
               ,:TDR-TOT-TAX
                :IND-TDR-TOT-TAX
               ,:TDR-TOT-SOPR-SAN
                :IND-TDR-TOT-SOPR-SAN
               ,:TDR-TOT-SOPR-TEC
                :IND-TDR-TOT-SOPR-TEC
               ,:TDR-TOT-SOPR-SPO
                :IND-TDR-TOT-SOPR-SPO
               ,:TDR-TOT-SOPR-PROF
                :IND-TDR-TOT-SOPR-PROF
               ,:TDR-TOT-SOPR-ALT
                :IND-TDR-TOT-SOPR-ALT
               ,:TDR-TOT-PRE-TOT
                :IND-TDR-TOT-PRE-TOT
               ,:TDR-TOT-PRE-PP-IAS
                :IND-TDR-TOT-PRE-PP-IAS
               ,:TDR-TOT-CAR-IAS
                :IND-TDR-TOT-CAR-IAS
               ,:TDR-TOT-PRE-SOLO-RSH
                :IND-TDR-TOT-PRE-SOLO-RSH
               ,:TDR-TOT-PROV-ACQ-1AA
                :IND-TDR-TOT-PROV-ACQ-1AA
               ,:TDR-TOT-PROV-ACQ-2AA
                :IND-TDR-TOT-PROV-ACQ-2AA
               ,:TDR-TOT-PROV-RICOR
                :IND-TDR-TOT-PROV-RICOR
               ,:TDR-TOT-PROV-INC
                :IND-TDR-TOT-PROV-INC
               ,:TDR-TOT-PROV-DA-REC
                :IND-TDR-TOT-PROV-DA-REC
               ,:TDR-IMP-AZ
                :IND-TDR-IMP-AZ
               ,:TDR-IMP-ADER
                :IND-TDR-IMP-ADER
               ,:TDR-IMP-TFR
                :IND-TDR-IMP-TFR
               ,:TDR-IMP-VOLO
                :IND-TDR-IMP-VOLO
               ,:TDR-FL-VLDT-TIT
                :IND-TDR-FL-VLDT-TIT
               ,:TDR-TOT-CAR-ACQ
                :IND-TDR-TOT-CAR-ACQ
               ,:TDR-TOT-CAR-GEST
                :IND-TDR-TOT-CAR-GEST
               ,:TDR-TOT-CAR-INC
                :IND-TDR-TOT-CAR-INC
               ,:TDR-TOT-MANFEE-ANTIC
                :IND-TDR-TOT-MANFEE-ANTIC
               ,:TDR-TOT-MANFEE-RICOR
                :IND-TDR-TOT-MANFEE-RICOR
               ,:TDR-TOT-MANFEE-REC
                :IND-TDR-TOT-MANFEE-REC
               ,:TDR-DS-RIGA
               ,:TDR-DS-OPER-SQL
               ,:TDR-DS-VER
               ,:TDR-DS-TS-INI-CPTZ
               ,:TDR-DS-TS-END-CPTZ
               ,:TDR-DS-UTENTE
               ,:TDR-DS-STATO-ELAB
               ,:TDR-IMP-TRASFE
                :IND-TDR-IMP-TRASFE
               ,:TDR-IMP-TFR-STRC
                :IND-TDR-IMP-TFR-STRC
               ,:TDR-TOT-ACQ-EXP
                :IND-TDR-TOT-ACQ-EXP
               ,:TDR-TOT-REMUN-ASS
                :IND-TDR-TOT-REMUN-ASS
               ,:TDR-TOT-COMMIS-INTER
                :IND-TDR-TOT-COMMIS-INTER
               ,:TDR-TOT-CNBT-ANTIRAC
                :IND-TDR-TOT-CNBT-ANTIRAC
               ,:TDR-FL-INC-AUTOGEN
                :IND-TDR-FL-INC-AUTOGEN
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDO-EFF-TDR CURSOR FOR
              SELECT
                     ID_TIT_RAT
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_TIT
                    ,PROG_TIT
                    ,TP_PRE_TIT
                    ,TP_STAT_TIT
                    ,DT_INI_COP
                    ,DT_END_COP
                    ,IMP_PAG
                    ,FL_SOLL
                    ,FRAZ
                    ,DT_APPLZ_MORA
                    ,FL_MORA
                    ,ID_RAPP_RETE
                    ,ID_RAPP_ANA
                    ,COD_DVS
                    ,DT_EMIS_TIT
                    ,DT_ESI_TIT
                    ,TOT_PRE_NET
                    ,TOT_INTR_FRAZ
                    ,TOT_INTR_MORA
                    ,TOT_INTR_PREST
                    ,TOT_INTR_RETDT
                    ,TOT_INTR_RIAT
                    ,TOT_DIR
                    ,TOT_SPE_MED
                    ,TOT_SPE_AGE
                    ,TOT_TAX
                    ,TOT_SOPR_SAN
                    ,TOT_SOPR_TEC
                    ,TOT_SOPR_SPO
                    ,TOT_SOPR_PROF
                    ,TOT_SOPR_ALT
                    ,TOT_PRE_TOT
                    ,TOT_PRE_PP_IAS
                    ,TOT_CAR_IAS
                    ,TOT_PRE_SOLO_RSH
                    ,TOT_PROV_ACQ_1AA
                    ,TOT_PROV_ACQ_2AA
                    ,TOT_PROV_RICOR
                    ,TOT_PROV_INC
                    ,TOT_PROV_DA_REC
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,FL_VLDT_TIT
                    ,TOT_CAR_ACQ
                    ,TOT_CAR_GEST
                    ,TOT_CAR_INC
                    ,TOT_MANFEE_ANTIC
                    ,TOT_MANFEE_RICOR
                    ,TOT_MANFEE_REC
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,TOT_ACQ_EXP
                    ,TOT_REMUN_ASS
                    ,TOT_COMMIS_INTER
                    ,TOT_CNBT_ANTIRAC
                    ,FL_INC_AUTOGEN
              FROM TIT_RAT
              WHERE     ID_OGG = :TDR-ID-OGG
                    AND TP_OGG = :TDR-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_TIT_RAT ASC

           END-EXEC.

       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TIT_RAT
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_TIT
                ,PROG_TIT
                ,TP_PRE_TIT
                ,TP_STAT_TIT
                ,DT_INI_COP
                ,DT_END_COP
                ,IMP_PAG
                ,FL_SOLL
                ,FRAZ
                ,DT_APPLZ_MORA
                ,FL_MORA
                ,ID_RAPP_RETE
                ,ID_RAPP_ANA
                ,COD_DVS
                ,DT_EMIS_TIT
                ,DT_ESI_TIT
                ,TOT_PRE_NET
                ,TOT_INTR_FRAZ
                ,TOT_INTR_MORA
                ,TOT_INTR_PREST
                ,TOT_INTR_RETDT
                ,TOT_INTR_RIAT
                ,TOT_DIR
                ,TOT_SPE_MED
                ,TOT_SPE_AGE
                ,TOT_TAX
                ,TOT_SOPR_SAN
                ,TOT_SOPR_TEC
                ,TOT_SOPR_SPO
                ,TOT_SOPR_PROF
                ,TOT_SOPR_ALT
                ,TOT_PRE_TOT
                ,TOT_PRE_PP_IAS
                ,TOT_CAR_IAS
                ,TOT_PRE_SOLO_RSH
                ,TOT_PROV_ACQ_1AA
                ,TOT_PROV_ACQ_2AA
                ,TOT_PROV_RICOR
                ,TOT_PROV_INC
                ,TOT_PROV_DA_REC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,FL_VLDT_TIT
                ,TOT_CAR_ACQ
                ,TOT_CAR_GEST
                ,TOT_CAR_INC
                ,TOT_MANFEE_ANTIC
                ,TOT_MANFEE_RICOR
                ,TOT_MANFEE_REC
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,TOT_ACQ_EXP
                ,TOT_REMUN_ASS
                ,TOT_COMMIS_INTER
                ,TOT_CNBT_ANTIRAC
                ,FL_INC_AUTOGEN
             INTO
                :TDR-ID-TIT-RAT
               ,:TDR-ID-OGG
               ,:TDR-TP-OGG
               ,:TDR-ID-MOVI-CRZ
               ,:TDR-ID-MOVI-CHIU
                :IND-TDR-ID-MOVI-CHIU
               ,:TDR-DT-INI-EFF-DB
               ,:TDR-DT-END-EFF-DB
               ,:TDR-COD-COMP-ANIA
               ,:TDR-TP-TIT
               ,:TDR-PROG-TIT
                :IND-TDR-PROG-TIT
               ,:TDR-TP-PRE-TIT
               ,:TDR-TP-STAT-TIT
               ,:TDR-DT-INI-COP-DB
                :IND-TDR-DT-INI-COP
               ,:TDR-DT-END-COP-DB
                :IND-TDR-DT-END-COP
               ,:TDR-IMP-PAG
                :IND-TDR-IMP-PAG
               ,:TDR-FL-SOLL
                :IND-TDR-FL-SOLL
               ,:TDR-FRAZ
                :IND-TDR-FRAZ
               ,:TDR-DT-APPLZ-MORA-DB
                :IND-TDR-DT-APPLZ-MORA
               ,:TDR-FL-MORA
                :IND-TDR-FL-MORA
               ,:TDR-ID-RAPP-RETE
                :IND-TDR-ID-RAPP-RETE
               ,:TDR-ID-RAPP-ANA
                :IND-TDR-ID-RAPP-ANA
               ,:TDR-COD-DVS
                :IND-TDR-COD-DVS
               ,:TDR-DT-EMIS-TIT-DB
               ,:TDR-DT-ESI-TIT-DB
                :IND-TDR-DT-ESI-TIT
               ,:TDR-TOT-PRE-NET
                :IND-TDR-TOT-PRE-NET
               ,:TDR-TOT-INTR-FRAZ
                :IND-TDR-TOT-INTR-FRAZ
               ,:TDR-TOT-INTR-MORA
                :IND-TDR-TOT-INTR-MORA
               ,:TDR-TOT-INTR-PREST
                :IND-TDR-TOT-INTR-PREST
               ,:TDR-TOT-INTR-RETDT
                :IND-TDR-TOT-INTR-RETDT
               ,:TDR-TOT-INTR-RIAT
                :IND-TDR-TOT-INTR-RIAT
               ,:TDR-TOT-DIR
                :IND-TDR-TOT-DIR
               ,:TDR-TOT-SPE-MED
                :IND-TDR-TOT-SPE-MED
               ,:TDR-TOT-SPE-AGE
                :IND-TDR-TOT-SPE-AGE
               ,:TDR-TOT-TAX
                :IND-TDR-TOT-TAX
               ,:TDR-TOT-SOPR-SAN
                :IND-TDR-TOT-SOPR-SAN
               ,:TDR-TOT-SOPR-TEC
                :IND-TDR-TOT-SOPR-TEC
               ,:TDR-TOT-SOPR-SPO
                :IND-TDR-TOT-SOPR-SPO
               ,:TDR-TOT-SOPR-PROF
                :IND-TDR-TOT-SOPR-PROF
               ,:TDR-TOT-SOPR-ALT
                :IND-TDR-TOT-SOPR-ALT
               ,:TDR-TOT-PRE-TOT
                :IND-TDR-TOT-PRE-TOT
               ,:TDR-TOT-PRE-PP-IAS
                :IND-TDR-TOT-PRE-PP-IAS
               ,:TDR-TOT-CAR-IAS
                :IND-TDR-TOT-CAR-IAS
               ,:TDR-TOT-PRE-SOLO-RSH
                :IND-TDR-TOT-PRE-SOLO-RSH
               ,:TDR-TOT-PROV-ACQ-1AA
                :IND-TDR-TOT-PROV-ACQ-1AA
               ,:TDR-TOT-PROV-ACQ-2AA
                :IND-TDR-TOT-PROV-ACQ-2AA
               ,:TDR-TOT-PROV-RICOR
                :IND-TDR-TOT-PROV-RICOR
               ,:TDR-TOT-PROV-INC
                :IND-TDR-TOT-PROV-INC
               ,:TDR-TOT-PROV-DA-REC
                :IND-TDR-TOT-PROV-DA-REC
               ,:TDR-IMP-AZ
                :IND-TDR-IMP-AZ
               ,:TDR-IMP-ADER
                :IND-TDR-IMP-ADER
               ,:TDR-IMP-TFR
                :IND-TDR-IMP-TFR
               ,:TDR-IMP-VOLO
                :IND-TDR-IMP-VOLO
               ,:TDR-FL-VLDT-TIT
                :IND-TDR-FL-VLDT-TIT
               ,:TDR-TOT-CAR-ACQ
                :IND-TDR-TOT-CAR-ACQ
               ,:TDR-TOT-CAR-GEST
                :IND-TDR-TOT-CAR-GEST
               ,:TDR-TOT-CAR-INC
                :IND-TDR-TOT-CAR-INC
               ,:TDR-TOT-MANFEE-ANTIC
                :IND-TDR-TOT-MANFEE-ANTIC
               ,:TDR-TOT-MANFEE-RICOR
                :IND-TDR-TOT-MANFEE-RICOR
               ,:TDR-TOT-MANFEE-REC
                :IND-TDR-TOT-MANFEE-REC
               ,:TDR-DS-RIGA
               ,:TDR-DS-OPER-SQL
               ,:TDR-DS-VER
               ,:TDR-DS-TS-INI-CPTZ
               ,:TDR-DS-TS-END-CPTZ
               ,:TDR-DS-UTENTE
               ,:TDR-DS-STATO-ELAB
               ,:TDR-IMP-TRASFE
                :IND-TDR-IMP-TRASFE
               ,:TDR-IMP-TFR-STRC
                :IND-TDR-IMP-TFR-STRC
               ,:TDR-TOT-ACQ-EXP
                :IND-TDR-TOT-ACQ-EXP
               ,:TDR-TOT-REMUN-ASS
                :IND-TDR-TOT-REMUN-ASS
               ,:TDR-TOT-COMMIS-INTER
                :IND-TDR-TOT-COMMIS-INTER
               ,:TDR-TOT-CNBT-ANTIRAC
                :IND-TDR-TOT-CNBT-ANTIRAC
               ,:TDR-FL-INC-AUTOGEN
                :IND-TDR-FL-INC-AUTOGEN
             FROM TIT_RAT
             WHERE     ID_OGG = :TDR-ID-OGG
                    AND TP_OGG = :TDR-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           EXEC SQL
                OPEN C-IDO-EFF-TDR
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           EXEC SQL
                CLOSE C-IDO-EFF-TDR
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           EXEC SQL
                FETCH C-IDO-EFF-TDR
           INTO
                :TDR-ID-TIT-RAT
               ,:TDR-ID-OGG
               ,:TDR-TP-OGG
               ,:TDR-ID-MOVI-CRZ
               ,:TDR-ID-MOVI-CHIU
                :IND-TDR-ID-MOVI-CHIU
               ,:TDR-DT-INI-EFF-DB
               ,:TDR-DT-END-EFF-DB
               ,:TDR-COD-COMP-ANIA
               ,:TDR-TP-TIT
               ,:TDR-PROG-TIT
                :IND-TDR-PROG-TIT
               ,:TDR-TP-PRE-TIT
               ,:TDR-TP-STAT-TIT
               ,:TDR-DT-INI-COP-DB
                :IND-TDR-DT-INI-COP
               ,:TDR-DT-END-COP-DB
                :IND-TDR-DT-END-COP
               ,:TDR-IMP-PAG
                :IND-TDR-IMP-PAG
               ,:TDR-FL-SOLL
                :IND-TDR-FL-SOLL
               ,:TDR-FRAZ
                :IND-TDR-FRAZ
               ,:TDR-DT-APPLZ-MORA-DB
                :IND-TDR-DT-APPLZ-MORA
               ,:TDR-FL-MORA
                :IND-TDR-FL-MORA
               ,:TDR-ID-RAPP-RETE
                :IND-TDR-ID-RAPP-RETE
               ,:TDR-ID-RAPP-ANA
                :IND-TDR-ID-RAPP-ANA
               ,:TDR-COD-DVS
                :IND-TDR-COD-DVS
               ,:TDR-DT-EMIS-TIT-DB
               ,:TDR-DT-ESI-TIT-DB
                :IND-TDR-DT-ESI-TIT
               ,:TDR-TOT-PRE-NET
                :IND-TDR-TOT-PRE-NET
               ,:TDR-TOT-INTR-FRAZ
                :IND-TDR-TOT-INTR-FRAZ
               ,:TDR-TOT-INTR-MORA
                :IND-TDR-TOT-INTR-MORA
               ,:TDR-TOT-INTR-PREST
                :IND-TDR-TOT-INTR-PREST
               ,:TDR-TOT-INTR-RETDT
                :IND-TDR-TOT-INTR-RETDT
               ,:TDR-TOT-INTR-RIAT
                :IND-TDR-TOT-INTR-RIAT
               ,:TDR-TOT-DIR
                :IND-TDR-TOT-DIR
               ,:TDR-TOT-SPE-MED
                :IND-TDR-TOT-SPE-MED
               ,:TDR-TOT-SPE-AGE
                :IND-TDR-TOT-SPE-AGE
               ,:TDR-TOT-TAX
                :IND-TDR-TOT-TAX
               ,:TDR-TOT-SOPR-SAN
                :IND-TDR-TOT-SOPR-SAN
               ,:TDR-TOT-SOPR-TEC
                :IND-TDR-TOT-SOPR-TEC
               ,:TDR-TOT-SOPR-SPO
                :IND-TDR-TOT-SOPR-SPO
               ,:TDR-TOT-SOPR-PROF
                :IND-TDR-TOT-SOPR-PROF
               ,:TDR-TOT-SOPR-ALT
                :IND-TDR-TOT-SOPR-ALT
               ,:TDR-TOT-PRE-TOT
                :IND-TDR-TOT-PRE-TOT
               ,:TDR-TOT-PRE-PP-IAS
                :IND-TDR-TOT-PRE-PP-IAS
               ,:TDR-TOT-CAR-IAS
                :IND-TDR-TOT-CAR-IAS
               ,:TDR-TOT-PRE-SOLO-RSH
                :IND-TDR-TOT-PRE-SOLO-RSH
               ,:TDR-TOT-PROV-ACQ-1AA
                :IND-TDR-TOT-PROV-ACQ-1AA
               ,:TDR-TOT-PROV-ACQ-2AA
                :IND-TDR-TOT-PROV-ACQ-2AA
               ,:TDR-TOT-PROV-RICOR
                :IND-TDR-TOT-PROV-RICOR
               ,:TDR-TOT-PROV-INC
                :IND-TDR-TOT-PROV-INC
               ,:TDR-TOT-PROV-DA-REC
                :IND-TDR-TOT-PROV-DA-REC
               ,:TDR-IMP-AZ
                :IND-TDR-IMP-AZ
               ,:TDR-IMP-ADER
                :IND-TDR-IMP-ADER
               ,:TDR-IMP-TFR
                :IND-TDR-IMP-TFR
               ,:TDR-IMP-VOLO
                :IND-TDR-IMP-VOLO
               ,:TDR-FL-VLDT-TIT
                :IND-TDR-FL-VLDT-TIT
               ,:TDR-TOT-CAR-ACQ
                :IND-TDR-TOT-CAR-ACQ
               ,:TDR-TOT-CAR-GEST
                :IND-TDR-TOT-CAR-GEST
               ,:TDR-TOT-CAR-INC
                :IND-TDR-TOT-CAR-INC
               ,:TDR-TOT-MANFEE-ANTIC
                :IND-TDR-TOT-MANFEE-ANTIC
               ,:TDR-TOT-MANFEE-RICOR
                :IND-TDR-TOT-MANFEE-RICOR
               ,:TDR-TOT-MANFEE-REC
                :IND-TDR-TOT-MANFEE-REC
               ,:TDR-DS-RIGA
               ,:TDR-DS-OPER-SQL
               ,:TDR-DS-VER
               ,:TDR-DS-TS-INI-CPTZ
               ,:TDR-DS-TS-END-CPTZ
               ,:TDR-DS-UTENTE
               ,:TDR-DS-STATO-ELAB
               ,:TDR-IMP-TRASFE
                :IND-TDR-IMP-TRASFE
               ,:TDR-IMP-TFR-STRC
                :IND-TDR-IMP-TFR-STRC
               ,:TDR-TOT-ACQ-EXP
                :IND-TDR-TOT-ACQ-EXP
               ,:TDR-TOT-REMUN-ASS
                :IND-TDR-TOT-REMUN-ASS
               ,:TDR-TOT-COMMIS-INTER
                :IND-TDR-TOT-COMMIS-INTER
               ,:TDR-TOT-CNBT-ANTIRAC
                :IND-TDR-TOT-CNBT-ANTIRAC
               ,:TDR-FL-INC-AUTOGEN
                :IND-TDR-FL-INC-AUTOGEN
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TIT_RAT
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_TIT
                ,PROG_TIT
                ,TP_PRE_TIT
                ,TP_STAT_TIT
                ,DT_INI_COP
                ,DT_END_COP
                ,IMP_PAG
                ,FL_SOLL
                ,FRAZ
                ,DT_APPLZ_MORA
                ,FL_MORA
                ,ID_RAPP_RETE
                ,ID_RAPP_ANA
                ,COD_DVS
                ,DT_EMIS_TIT
                ,DT_ESI_TIT
                ,TOT_PRE_NET
                ,TOT_INTR_FRAZ
                ,TOT_INTR_MORA
                ,TOT_INTR_PREST
                ,TOT_INTR_RETDT
                ,TOT_INTR_RIAT
                ,TOT_DIR
                ,TOT_SPE_MED
                ,TOT_SPE_AGE
                ,TOT_TAX
                ,TOT_SOPR_SAN
                ,TOT_SOPR_TEC
                ,TOT_SOPR_SPO
                ,TOT_SOPR_PROF
                ,TOT_SOPR_ALT
                ,TOT_PRE_TOT
                ,TOT_PRE_PP_IAS
                ,TOT_CAR_IAS
                ,TOT_PRE_SOLO_RSH
                ,TOT_PROV_ACQ_1AA
                ,TOT_PROV_ACQ_2AA
                ,TOT_PROV_RICOR
                ,TOT_PROV_INC
                ,TOT_PROV_DA_REC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,FL_VLDT_TIT
                ,TOT_CAR_ACQ
                ,TOT_CAR_GEST
                ,TOT_CAR_INC
                ,TOT_MANFEE_ANTIC
                ,TOT_MANFEE_RICOR
                ,TOT_MANFEE_REC
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,TOT_ACQ_EXP
                ,TOT_REMUN_ASS
                ,TOT_COMMIS_INTER
                ,TOT_CNBT_ANTIRAC
                ,FL_INC_AUTOGEN
             INTO
                :TDR-ID-TIT-RAT
               ,:TDR-ID-OGG
               ,:TDR-TP-OGG
               ,:TDR-ID-MOVI-CRZ
               ,:TDR-ID-MOVI-CHIU
                :IND-TDR-ID-MOVI-CHIU
               ,:TDR-DT-INI-EFF-DB
               ,:TDR-DT-END-EFF-DB
               ,:TDR-COD-COMP-ANIA
               ,:TDR-TP-TIT
               ,:TDR-PROG-TIT
                :IND-TDR-PROG-TIT
               ,:TDR-TP-PRE-TIT
               ,:TDR-TP-STAT-TIT
               ,:TDR-DT-INI-COP-DB
                :IND-TDR-DT-INI-COP
               ,:TDR-DT-END-COP-DB
                :IND-TDR-DT-END-COP
               ,:TDR-IMP-PAG
                :IND-TDR-IMP-PAG
               ,:TDR-FL-SOLL
                :IND-TDR-FL-SOLL
               ,:TDR-FRAZ
                :IND-TDR-FRAZ
               ,:TDR-DT-APPLZ-MORA-DB
                :IND-TDR-DT-APPLZ-MORA
               ,:TDR-FL-MORA
                :IND-TDR-FL-MORA
               ,:TDR-ID-RAPP-RETE
                :IND-TDR-ID-RAPP-RETE
               ,:TDR-ID-RAPP-ANA
                :IND-TDR-ID-RAPP-ANA
               ,:TDR-COD-DVS
                :IND-TDR-COD-DVS
               ,:TDR-DT-EMIS-TIT-DB
               ,:TDR-DT-ESI-TIT-DB
                :IND-TDR-DT-ESI-TIT
               ,:TDR-TOT-PRE-NET
                :IND-TDR-TOT-PRE-NET
               ,:TDR-TOT-INTR-FRAZ
                :IND-TDR-TOT-INTR-FRAZ
               ,:TDR-TOT-INTR-MORA
                :IND-TDR-TOT-INTR-MORA
               ,:TDR-TOT-INTR-PREST
                :IND-TDR-TOT-INTR-PREST
               ,:TDR-TOT-INTR-RETDT
                :IND-TDR-TOT-INTR-RETDT
               ,:TDR-TOT-INTR-RIAT
                :IND-TDR-TOT-INTR-RIAT
               ,:TDR-TOT-DIR
                :IND-TDR-TOT-DIR
               ,:TDR-TOT-SPE-MED
                :IND-TDR-TOT-SPE-MED
               ,:TDR-TOT-SPE-AGE
                :IND-TDR-TOT-SPE-AGE
               ,:TDR-TOT-TAX
                :IND-TDR-TOT-TAX
               ,:TDR-TOT-SOPR-SAN
                :IND-TDR-TOT-SOPR-SAN
               ,:TDR-TOT-SOPR-TEC
                :IND-TDR-TOT-SOPR-TEC
               ,:TDR-TOT-SOPR-SPO
                :IND-TDR-TOT-SOPR-SPO
               ,:TDR-TOT-SOPR-PROF
                :IND-TDR-TOT-SOPR-PROF
               ,:TDR-TOT-SOPR-ALT
                :IND-TDR-TOT-SOPR-ALT
               ,:TDR-TOT-PRE-TOT
                :IND-TDR-TOT-PRE-TOT
               ,:TDR-TOT-PRE-PP-IAS
                :IND-TDR-TOT-PRE-PP-IAS
               ,:TDR-TOT-CAR-IAS
                :IND-TDR-TOT-CAR-IAS
               ,:TDR-TOT-PRE-SOLO-RSH
                :IND-TDR-TOT-PRE-SOLO-RSH
               ,:TDR-TOT-PROV-ACQ-1AA
                :IND-TDR-TOT-PROV-ACQ-1AA
               ,:TDR-TOT-PROV-ACQ-2AA
                :IND-TDR-TOT-PROV-ACQ-2AA
               ,:TDR-TOT-PROV-RICOR
                :IND-TDR-TOT-PROV-RICOR
               ,:TDR-TOT-PROV-INC
                :IND-TDR-TOT-PROV-INC
               ,:TDR-TOT-PROV-DA-REC
                :IND-TDR-TOT-PROV-DA-REC
               ,:TDR-IMP-AZ
                :IND-TDR-IMP-AZ
               ,:TDR-IMP-ADER
                :IND-TDR-IMP-ADER
               ,:TDR-IMP-TFR
                :IND-TDR-IMP-TFR
               ,:TDR-IMP-VOLO
                :IND-TDR-IMP-VOLO
               ,:TDR-FL-VLDT-TIT
                :IND-TDR-FL-VLDT-TIT
               ,:TDR-TOT-CAR-ACQ
                :IND-TDR-TOT-CAR-ACQ
               ,:TDR-TOT-CAR-GEST
                :IND-TDR-TOT-CAR-GEST
               ,:TDR-TOT-CAR-INC
                :IND-TDR-TOT-CAR-INC
               ,:TDR-TOT-MANFEE-ANTIC
                :IND-TDR-TOT-MANFEE-ANTIC
               ,:TDR-TOT-MANFEE-RICOR
                :IND-TDR-TOT-MANFEE-RICOR
               ,:TDR-TOT-MANFEE-REC
                :IND-TDR-TOT-MANFEE-REC
               ,:TDR-DS-RIGA
               ,:TDR-DS-OPER-SQL
               ,:TDR-DS-VER
               ,:TDR-DS-TS-INI-CPTZ
               ,:TDR-DS-TS-END-CPTZ
               ,:TDR-DS-UTENTE
               ,:TDR-DS-STATO-ELAB
               ,:TDR-IMP-TRASFE
                :IND-TDR-IMP-TRASFE
               ,:TDR-IMP-TFR-STRC
                :IND-TDR-IMP-TFR-STRC
               ,:TDR-TOT-ACQ-EXP
                :IND-TDR-TOT-ACQ-EXP
               ,:TDR-TOT-REMUN-ASS
                :IND-TDR-TOT-REMUN-ASS
               ,:TDR-TOT-COMMIS-INTER
                :IND-TDR-TOT-COMMIS-INTER
               ,:TDR-TOT-CNBT-ANTIRAC
                :IND-TDR-TOT-CNBT-ANTIRAC
               ,:TDR-FL-INC-AUTOGEN
                :IND-TDR-FL-INC-AUTOGEN
             FROM TIT_RAT
             WHERE     ID_TIT_RAT = :TDR-ID-TIT-RAT
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDO-CPZ-TDR CURSOR FOR
              SELECT
                     ID_TIT_RAT
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_TIT
                    ,PROG_TIT
                    ,TP_PRE_TIT
                    ,TP_STAT_TIT
                    ,DT_INI_COP
                    ,DT_END_COP
                    ,IMP_PAG
                    ,FL_SOLL
                    ,FRAZ
                    ,DT_APPLZ_MORA
                    ,FL_MORA
                    ,ID_RAPP_RETE
                    ,ID_RAPP_ANA
                    ,COD_DVS
                    ,DT_EMIS_TIT
                    ,DT_ESI_TIT
                    ,TOT_PRE_NET
                    ,TOT_INTR_FRAZ
                    ,TOT_INTR_MORA
                    ,TOT_INTR_PREST
                    ,TOT_INTR_RETDT
                    ,TOT_INTR_RIAT
                    ,TOT_DIR
                    ,TOT_SPE_MED
                    ,TOT_SPE_AGE
                    ,TOT_TAX
                    ,TOT_SOPR_SAN
                    ,TOT_SOPR_TEC
                    ,TOT_SOPR_SPO
                    ,TOT_SOPR_PROF
                    ,TOT_SOPR_ALT
                    ,TOT_PRE_TOT
                    ,TOT_PRE_PP_IAS
                    ,TOT_CAR_IAS
                    ,TOT_PRE_SOLO_RSH
                    ,TOT_PROV_ACQ_1AA
                    ,TOT_PROV_ACQ_2AA
                    ,TOT_PROV_RICOR
                    ,TOT_PROV_INC
                    ,TOT_PROV_DA_REC
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,FL_VLDT_TIT
                    ,TOT_CAR_ACQ
                    ,TOT_CAR_GEST
                    ,TOT_CAR_INC
                    ,TOT_MANFEE_ANTIC
                    ,TOT_MANFEE_RICOR
                    ,TOT_MANFEE_REC
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,TOT_ACQ_EXP
                    ,TOT_REMUN_ASS
                    ,TOT_COMMIS_INTER
                    ,TOT_CNBT_ANTIRAC
                    ,FL_INC_AUTOGEN
              FROM TIT_RAT
              WHERE     ID_OGG = :TDR-ID-OGG
           AND TP_OGG = :TDR-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_TIT_RAT ASC

           END-EXEC.

       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TIT_RAT
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_TIT
                ,PROG_TIT
                ,TP_PRE_TIT
                ,TP_STAT_TIT
                ,DT_INI_COP
                ,DT_END_COP
                ,IMP_PAG
                ,FL_SOLL
                ,FRAZ
                ,DT_APPLZ_MORA
                ,FL_MORA
                ,ID_RAPP_RETE
                ,ID_RAPP_ANA
                ,COD_DVS
                ,DT_EMIS_TIT
                ,DT_ESI_TIT
                ,TOT_PRE_NET
                ,TOT_INTR_FRAZ
                ,TOT_INTR_MORA
                ,TOT_INTR_PREST
                ,TOT_INTR_RETDT
                ,TOT_INTR_RIAT
                ,TOT_DIR
                ,TOT_SPE_MED
                ,TOT_SPE_AGE
                ,TOT_TAX
                ,TOT_SOPR_SAN
                ,TOT_SOPR_TEC
                ,TOT_SOPR_SPO
                ,TOT_SOPR_PROF
                ,TOT_SOPR_ALT
                ,TOT_PRE_TOT
                ,TOT_PRE_PP_IAS
                ,TOT_CAR_IAS
                ,TOT_PRE_SOLO_RSH
                ,TOT_PROV_ACQ_1AA
                ,TOT_PROV_ACQ_2AA
                ,TOT_PROV_RICOR
                ,TOT_PROV_INC
                ,TOT_PROV_DA_REC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,FL_VLDT_TIT
                ,TOT_CAR_ACQ
                ,TOT_CAR_GEST
                ,TOT_CAR_INC
                ,TOT_MANFEE_ANTIC
                ,TOT_MANFEE_RICOR
                ,TOT_MANFEE_REC
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,TOT_ACQ_EXP
                ,TOT_REMUN_ASS
                ,TOT_COMMIS_INTER
                ,TOT_CNBT_ANTIRAC
                ,FL_INC_AUTOGEN
             INTO
                :TDR-ID-TIT-RAT
               ,:TDR-ID-OGG
               ,:TDR-TP-OGG
               ,:TDR-ID-MOVI-CRZ
               ,:TDR-ID-MOVI-CHIU
                :IND-TDR-ID-MOVI-CHIU
               ,:TDR-DT-INI-EFF-DB
               ,:TDR-DT-END-EFF-DB
               ,:TDR-COD-COMP-ANIA
               ,:TDR-TP-TIT
               ,:TDR-PROG-TIT
                :IND-TDR-PROG-TIT
               ,:TDR-TP-PRE-TIT
               ,:TDR-TP-STAT-TIT
               ,:TDR-DT-INI-COP-DB
                :IND-TDR-DT-INI-COP
               ,:TDR-DT-END-COP-DB
                :IND-TDR-DT-END-COP
               ,:TDR-IMP-PAG
                :IND-TDR-IMP-PAG
               ,:TDR-FL-SOLL
                :IND-TDR-FL-SOLL
               ,:TDR-FRAZ
                :IND-TDR-FRAZ
               ,:TDR-DT-APPLZ-MORA-DB
                :IND-TDR-DT-APPLZ-MORA
               ,:TDR-FL-MORA
                :IND-TDR-FL-MORA
               ,:TDR-ID-RAPP-RETE
                :IND-TDR-ID-RAPP-RETE
               ,:TDR-ID-RAPP-ANA
                :IND-TDR-ID-RAPP-ANA
               ,:TDR-COD-DVS
                :IND-TDR-COD-DVS
               ,:TDR-DT-EMIS-TIT-DB
               ,:TDR-DT-ESI-TIT-DB
                :IND-TDR-DT-ESI-TIT
               ,:TDR-TOT-PRE-NET
                :IND-TDR-TOT-PRE-NET
               ,:TDR-TOT-INTR-FRAZ
                :IND-TDR-TOT-INTR-FRAZ
               ,:TDR-TOT-INTR-MORA
                :IND-TDR-TOT-INTR-MORA
               ,:TDR-TOT-INTR-PREST
                :IND-TDR-TOT-INTR-PREST
               ,:TDR-TOT-INTR-RETDT
                :IND-TDR-TOT-INTR-RETDT
               ,:TDR-TOT-INTR-RIAT
                :IND-TDR-TOT-INTR-RIAT
               ,:TDR-TOT-DIR
                :IND-TDR-TOT-DIR
               ,:TDR-TOT-SPE-MED
                :IND-TDR-TOT-SPE-MED
               ,:TDR-TOT-SPE-AGE
                :IND-TDR-TOT-SPE-AGE
               ,:TDR-TOT-TAX
                :IND-TDR-TOT-TAX
               ,:TDR-TOT-SOPR-SAN
                :IND-TDR-TOT-SOPR-SAN
               ,:TDR-TOT-SOPR-TEC
                :IND-TDR-TOT-SOPR-TEC
               ,:TDR-TOT-SOPR-SPO
                :IND-TDR-TOT-SOPR-SPO
               ,:TDR-TOT-SOPR-PROF
                :IND-TDR-TOT-SOPR-PROF
               ,:TDR-TOT-SOPR-ALT
                :IND-TDR-TOT-SOPR-ALT
               ,:TDR-TOT-PRE-TOT
                :IND-TDR-TOT-PRE-TOT
               ,:TDR-TOT-PRE-PP-IAS
                :IND-TDR-TOT-PRE-PP-IAS
               ,:TDR-TOT-CAR-IAS
                :IND-TDR-TOT-CAR-IAS
               ,:TDR-TOT-PRE-SOLO-RSH
                :IND-TDR-TOT-PRE-SOLO-RSH
               ,:TDR-TOT-PROV-ACQ-1AA
                :IND-TDR-TOT-PROV-ACQ-1AA
               ,:TDR-TOT-PROV-ACQ-2AA
                :IND-TDR-TOT-PROV-ACQ-2AA
               ,:TDR-TOT-PROV-RICOR
                :IND-TDR-TOT-PROV-RICOR
               ,:TDR-TOT-PROV-INC
                :IND-TDR-TOT-PROV-INC
               ,:TDR-TOT-PROV-DA-REC
                :IND-TDR-TOT-PROV-DA-REC
               ,:TDR-IMP-AZ
                :IND-TDR-IMP-AZ
               ,:TDR-IMP-ADER
                :IND-TDR-IMP-ADER
               ,:TDR-IMP-TFR
                :IND-TDR-IMP-TFR
               ,:TDR-IMP-VOLO
                :IND-TDR-IMP-VOLO
               ,:TDR-FL-VLDT-TIT
                :IND-TDR-FL-VLDT-TIT
               ,:TDR-TOT-CAR-ACQ
                :IND-TDR-TOT-CAR-ACQ
               ,:TDR-TOT-CAR-GEST
                :IND-TDR-TOT-CAR-GEST
               ,:TDR-TOT-CAR-INC
                :IND-TDR-TOT-CAR-INC
               ,:TDR-TOT-MANFEE-ANTIC
                :IND-TDR-TOT-MANFEE-ANTIC
               ,:TDR-TOT-MANFEE-RICOR
                :IND-TDR-TOT-MANFEE-RICOR
               ,:TDR-TOT-MANFEE-REC
                :IND-TDR-TOT-MANFEE-REC
               ,:TDR-DS-RIGA
               ,:TDR-DS-OPER-SQL
               ,:TDR-DS-VER
               ,:TDR-DS-TS-INI-CPTZ
               ,:TDR-DS-TS-END-CPTZ
               ,:TDR-DS-UTENTE
               ,:TDR-DS-STATO-ELAB
               ,:TDR-IMP-TRASFE
                :IND-TDR-IMP-TRASFE
               ,:TDR-IMP-TFR-STRC
                :IND-TDR-IMP-TFR-STRC
               ,:TDR-TOT-ACQ-EXP
                :IND-TDR-TOT-ACQ-EXP
               ,:TDR-TOT-REMUN-ASS
                :IND-TDR-TOT-REMUN-ASS
               ,:TDR-TOT-COMMIS-INTER
                :IND-TDR-TOT-COMMIS-INTER
               ,:TDR-TOT-CNBT-ANTIRAC
                :IND-TDR-TOT-CNBT-ANTIRAC
               ,:TDR-FL-INC-AUTOGEN
                :IND-TDR-FL-INC-AUTOGEN
             FROM TIT_RAT
             WHERE     ID_OGG = :TDR-ID-OGG
                    AND TP_OGG = :TDR-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           EXEC SQL
                OPEN C-IDO-CPZ-TDR
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           EXEC SQL
                CLOSE C-IDO-CPZ-TDR
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           EXEC SQL
                FETCH C-IDO-CPZ-TDR
           INTO
                :TDR-ID-TIT-RAT
               ,:TDR-ID-OGG
               ,:TDR-TP-OGG
               ,:TDR-ID-MOVI-CRZ
               ,:TDR-ID-MOVI-CHIU
                :IND-TDR-ID-MOVI-CHIU
               ,:TDR-DT-INI-EFF-DB
               ,:TDR-DT-END-EFF-DB
               ,:TDR-COD-COMP-ANIA
               ,:TDR-TP-TIT
               ,:TDR-PROG-TIT
                :IND-TDR-PROG-TIT
               ,:TDR-TP-PRE-TIT
               ,:TDR-TP-STAT-TIT
               ,:TDR-DT-INI-COP-DB
                :IND-TDR-DT-INI-COP
               ,:TDR-DT-END-COP-DB
                :IND-TDR-DT-END-COP
               ,:TDR-IMP-PAG
                :IND-TDR-IMP-PAG
               ,:TDR-FL-SOLL
                :IND-TDR-FL-SOLL
               ,:TDR-FRAZ
                :IND-TDR-FRAZ
               ,:TDR-DT-APPLZ-MORA-DB
                :IND-TDR-DT-APPLZ-MORA
               ,:TDR-FL-MORA
                :IND-TDR-FL-MORA
               ,:TDR-ID-RAPP-RETE
                :IND-TDR-ID-RAPP-RETE
               ,:TDR-ID-RAPP-ANA
                :IND-TDR-ID-RAPP-ANA
               ,:TDR-COD-DVS
                :IND-TDR-COD-DVS
               ,:TDR-DT-EMIS-TIT-DB
               ,:TDR-DT-ESI-TIT-DB
                :IND-TDR-DT-ESI-TIT
               ,:TDR-TOT-PRE-NET
                :IND-TDR-TOT-PRE-NET
               ,:TDR-TOT-INTR-FRAZ
                :IND-TDR-TOT-INTR-FRAZ
               ,:TDR-TOT-INTR-MORA
                :IND-TDR-TOT-INTR-MORA
               ,:TDR-TOT-INTR-PREST
                :IND-TDR-TOT-INTR-PREST
               ,:TDR-TOT-INTR-RETDT
                :IND-TDR-TOT-INTR-RETDT
               ,:TDR-TOT-INTR-RIAT
                :IND-TDR-TOT-INTR-RIAT
               ,:TDR-TOT-DIR
                :IND-TDR-TOT-DIR
               ,:TDR-TOT-SPE-MED
                :IND-TDR-TOT-SPE-MED
               ,:TDR-TOT-SPE-AGE
                :IND-TDR-TOT-SPE-AGE
               ,:TDR-TOT-TAX
                :IND-TDR-TOT-TAX
               ,:TDR-TOT-SOPR-SAN
                :IND-TDR-TOT-SOPR-SAN
               ,:TDR-TOT-SOPR-TEC
                :IND-TDR-TOT-SOPR-TEC
               ,:TDR-TOT-SOPR-SPO
                :IND-TDR-TOT-SOPR-SPO
               ,:TDR-TOT-SOPR-PROF
                :IND-TDR-TOT-SOPR-PROF
               ,:TDR-TOT-SOPR-ALT
                :IND-TDR-TOT-SOPR-ALT
               ,:TDR-TOT-PRE-TOT
                :IND-TDR-TOT-PRE-TOT
               ,:TDR-TOT-PRE-PP-IAS
                :IND-TDR-TOT-PRE-PP-IAS
               ,:TDR-TOT-CAR-IAS
                :IND-TDR-TOT-CAR-IAS
               ,:TDR-TOT-PRE-SOLO-RSH
                :IND-TDR-TOT-PRE-SOLO-RSH
               ,:TDR-TOT-PROV-ACQ-1AA
                :IND-TDR-TOT-PROV-ACQ-1AA
               ,:TDR-TOT-PROV-ACQ-2AA
                :IND-TDR-TOT-PROV-ACQ-2AA
               ,:TDR-TOT-PROV-RICOR
                :IND-TDR-TOT-PROV-RICOR
               ,:TDR-TOT-PROV-INC
                :IND-TDR-TOT-PROV-INC
               ,:TDR-TOT-PROV-DA-REC
                :IND-TDR-TOT-PROV-DA-REC
               ,:TDR-IMP-AZ
                :IND-TDR-IMP-AZ
               ,:TDR-IMP-ADER
                :IND-TDR-IMP-ADER
               ,:TDR-IMP-TFR
                :IND-TDR-IMP-TFR
               ,:TDR-IMP-VOLO
                :IND-TDR-IMP-VOLO
               ,:TDR-FL-VLDT-TIT
                :IND-TDR-FL-VLDT-TIT
               ,:TDR-TOT-CAR-ACQ
                :IND-TDR-TOT-CAR-ACQ
               ,:TDR-TOT-CAR-GEST
                :IND-TDR-TOT-CAR-GEST
               ,:TDR-TOT-CAR-INC
                :IND-TDR-TOT-CAR-INC
               ,:TDR-TOT-MANFEE-ANTIC
                :IND-TDR-TOT-MANFEE-ANTIC
               ,:TDR-TOT-MANFEE-RICOR
                :IND-TDR-TOT-MANFEE-RICOR
               ,:TDR-TOT-MANFEE-REC
                :IND-TDR-TOT-MANFEE-REC
               ,:TDR-DS-RIGA
               ,:TDR-DS-OPER-SQL
               ,:TDR-DS-VER
               ,:TDR-DS-TS-INI-CPTZ
               ,:TDR-DS-TS-END-CPTZ
               ,:TDR-DS-UTENTE
               ,:TDR-DS-STATO-ELAB
               ,:TDR-IMP-TRASFE
                :IND-TDR-IMP-TRASFE
               ,:TDR-IMP-TFR-STRC
                :IND-TDR-IMP-TFR-STRC
               ,:TDR-TOT-ACQ-EXP
                :IND-TDR-TOT-ACQ-EXP
               ,:TDR-TOT-REMUN-ASS
                :IND-TDR-TOT-REMUN-ASS
               ,:TDR-TOT-COMMIS-INTER
                :IND-TDR-TOT-COMMIS-INTER
               ,:TDR-TOT-CNBT-ANTIRAC
                :IND-TDR-TOT-CNBT-ANTIRAC
               ,:TDR-FL-INC-AUTOGEN
                :IND-TDR-FL-INC-AUTOGEN
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B770-CLOSE-CURSOR-IDO-CPZ     THRU B770-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-TDR-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO TDR-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-TDR-PROG-TIT = -1
              MOVE HIGH-VALUES TO TDR-PROG-TIT-NULL
           END-IF
           IF IND-TDR-DT-INI-COP = -1
              MOVE HIGH-VALUES TO TDR-DT-INI-COP-NULL
           END-IF
           IF IND-TDR-DT-END-COP = -1
              MOVE HIGH-VALUES TO TDR-DT-END-COP-NULL
           END-IF
           IF IND-TDR-IMP-PAG = -1
              MOVE HIGH-VALUES TO TDR-IMP-PAG-NULL
           END-IF
           IF IND-TDR-FL-SOLL = -1
              MOVE HIGH-VALUES TO TDR-FL-SOLL-NULL
           END-IF
           IF IND-TDR-FRAZ = -1
              MOVE HIGH-VALUES TO TDR-FRAZ-NULL
           END-IF
           IF IND-TDR-DT-APPLZ-MORA = -1
              MOVE HIGH-VALUES TO TDR-DT-APPLZ-MORA-NULL
           END-IF
           IF IND-TDR-FL-MORA = -1
              MOVE HIGH-VALUES TO TDR-FL-MORA-NULL
           END-IF
           IF IND-TDR-ID-RAPP-RETE = -1
              MOVE HIGH-VALUES TO TDR-ID-RAPP-RETE-NULL
           END-IF
           IF IND-TDR-ID-RAPP-ANA = -1
              MOVE HIGH-VALUES TO TDR-ID-RAPP-ANA-NULL
           END-IF
           IF IND-TDR-COD-DVS = -1
              MOVE HIGH-VALUES TO TDR-COD-DVS-NULL
           END-IF
           IF IND-TDR-DT-ESI-TIT = -1
              MOVE HIGH-VALUES TO TDR-DT-ESI-TIT-NULL
           END-IF
           IF IND-TDR-TOT-PRE-NET = -1
              MOVE HIGH-VALUES TO TDR-TOT-PRE-NET-NULL
           END-IF
           IF IND-TDR-TOT-INTR-FRAZ = -1
              MOVE HIGH-VALUES TO TDR-TOT-INTR-FRAZ-NULL
           END-IF
           IF IND-TDR-TOT-INTR-MORA = -1
              MOVE HIGH-VALUES TO TDR-TOT-INTR-MORA-NULL
           END-IF
           IF IND-TDR-TOT-INTR-PREST = -1
              MOVE HIGH-VALUES TO TDR-TOT-INTR-PREST-NULL
           END-IF
           IF IND-TDR-TOT-INTR-RETDT = -1
              MOVE HIGH-VALUES TO TDR-TOT-INTR-RETDT-NULL
           END-IF
           IF IND-TDR-TOT-INTR-RIAT = -1
              MOVE HIGH-VALUES TO TDR-TOT-INTR-RIAT-NULL
           END-IF
           IF IND-TDR-TOT-DIR = -1
              MOVE HIGH-VALUES TO TDR-TOT-DIR-NULL
           END-IF
           IF IND-TDR-TOT-SPE-MED = -1
              MOVE HIGH-VALUES TO TDR-TOT-SPE-MED-NULL
           END-IF
           IF IND-TDR-TOT-SPE-AGE = -1
              MOVE HIGH-VALUES TO TDR-TOT-SPE-AGE-NULL
           END-IF
           IF IND-TDR-TOT-TAX = -1
              MOVE HIGH-VALUES TO TDR-TOT-TAX-NULL
           END-IF
           IF IND-TDR-TOT-SOPR-SAN = -1
              MOVE HIGH-VALUES TO TDR-TOT-SOPR-SAN-NULL
           END-IF
           IF IND-TDR-TOT-SOPR-TEC = -1
              MOVE HIGH-VALUES TO TDR-TOT-SOPR-TEC-NULL
           END-IF
           IF IND-TDR-TOT-SOPR-SPO = -1
              MOVE HIGH-VALUES TO TDR-TOT-SOPR-SPO-NULL
           END-IF
           IF IND-TDR-TOT-SOPR-PROF = -1
              MOVE HIGH-VALUES TO TDR-TOT-SOPR-PROF-NULL
           END-IF
           IF IND-TDR-TOT-SOPR-ALT = -1
              MOVE HIGH-VALUES TO TDR-TOT-SOPR-ALT-NULL
           END-IF
           IF IND-TDR-TOT-PRE-TOT = -1
              MOVE HIGH-VALUES TO TDR-TOT-PRE-TOT-NULL
           END-IF
           IF IND-TDR-TOT-PRE-PP-IAS = -1
              MOVE HIGH-VALUES TO TDR-TOT-PRE-PP-IAS-NULL
           END-IF
           IF IND-TDR-TOT-CAR-IAS = -1
              MOVE HIGH-VALUES TO TDR-TOT-CAR-IAS-NULL
           END-IF
           IF IND-TDR-TOT-PRE-SOLO-RSH = -1
              MOVE HIGH-VALUES TO TDR-TOT-PRE-SOLO-RSH-NULL
           END-IF
           IF IND-TDR-TOT-PROV-ACQ-1AA = -1
              MOVE HIGH-VALUES TO TDR-TOT-PROV-ACQ-1AA-NULL
           END-IF
           IF IND-TDR-TOT-PROV-ACQ-2AA = -1
              MOVE HIGH-VALUES TO TDR-TOT-PROV-ACQ-2AA-NULL
           END-IF
           IF IND-TDR-TOT-PROV-RICOR = -1
              MOVE HIGH-VALUES TO TDR-TOT-PROV-RICOR-NULL
           END-IF
           IF IND-TDR-TOT-PROV-INC = -1
              MOVE HIGH-VALUES TO TDR-TOT-PROV-INC-NULL
           END-IF
           IF IND-TDR-TOT-PROV-DA-REC = -1
              MOVE HIGH-VALUES TO TDR-TOT-PROV-DA-REC-NULL
           END-IF
           IF IND-TDR-IMP-AZ = -1
              MOVE HIGH-VALUES TO TDR-IMP-AZ-NULL
           END-IF
           IF IND-TDR-IMP-ADER = -1
              MOVE HIGH-VALUES TO TDR-IMP-ADER-NULL
           END-IF
           IF IND-TDR-IMP-TFR = -1
              MOVE HIGH-VALUES TO TDR-IMP-TFR-NULL
           END-IF
           IF IND-TDR-IMP-VOLO = -1
              MOVE HIGH-VALUES TO TDR-IMP-VOLO-NULL
           END-IF
           IF IND-TDR-FL-VLDT-TIT = -1
              MOVE HIGH-VALUES TO TDR-FL-VLDT-TIT-NULL
           END-IF
           IF IND-TDR-TOT-CAR-ACQ = -1
              MOVE HIGH-VALUES TO TDR-TOT-CAR-ACQ-NULL
           END-IF
           IF IND-TDR-TOT-CAR-GEST = -1
              MOVE HIGH-VALUES TO TDR-TOT-CAR-GEST-NULL
           END-IF
           IF IND-TDR-TOT-CAR-INC = -1
              MOVE HIGH-VALUES TO TDR-TOT-CAR-INC-NULL
           END-IF
           IF IND-TDR-TOT-MANFEE-ANTIC = -1
              MOVE HIGH-VALUES TO TDR-TOT-MANFEE-ANTIC-NULL
           END-IF
           IF IND-TDR-TOT-MANFEE-RICOR = -1
              MOVE HIGH-VALUES TO TDR-TOT-MANFEE-RICOR-NULL
           END-IF
           IF IND-TDR-TOT-MANFEE-REC = -1
              MOVE HIGH-VALUES TO TDR-TOT-MANFEE-REC-NULL
           END-IF
           IF IND-TDR-IMP-TRASFE = -1
              MOVE HIGH-VALUES TO TDR-IMP-TRASFE-NULL
           END-IF
           IF IND-TDR-IMP-TFR-STRC = -1
              MOVE HIGH-VALUES TO TDR-IMP-TFR-STRC-NULL
           END-IF
           IF IND-TDR-TOT-ACQ-EXP = -1
              MOVE HIGH-VALUES TO TDR-TOT-ACQ-EXP-NULL
           END-IF
           IF IND-TDR-TOT-REMUN-ASS = -1
              MOVE HIGH-VALUES TO TDR-TOT-REMUN-ASS-NULL
           END-IF
           IF IND-TDR-TOT-COMMIS-INTER = -1
              MOVE HIGH-VALUES TO TDR-TOT-COMMIS-INTER-NULL
           END-IF
           IF IND-TDR-TOT-CNBT-ANTIRAC = -1
              MOVE HIGH-VALUES TO TDR-TOT-CNBT-ANTIRAC-NULL
           END-IF
           IF IND-TDR-FL-INC-AUTOGEN = -1
              MOVE HIGH-VALUES TO TDR-FL-INC-AUTOGEN-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO TDR-DS-OPER-SQL
           MOVE 0                   TO TDR-DS-VER
           MOVE IDSV0003-USER-NAME TO TDR-DS-UTENTE
           MOVE '1'                   TO TDR-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO TDR-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO TDR-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF TDR-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-TDR-ID-MOVI-CHIU
           END-IF
           IF TDR-PROG-TIT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-PROG-TIT
           ELSE
              MOVE 0 TO IND-TDR-PROG-TIT
           END-IF
           IF TDR-DT-INI-COP-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-DT-INI-COP
           ELSE
              MOVE 0 TO IND-TDR-DT-INI-COP
           END-IF
           IF TDR-DT-END-COP-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-DT-END-COP
           ELSE
              MOVE 0 TO IND-TDR-DT-END-COP
           END-IF
           IF TDR-IMP-PAG-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-IMP-PAG
           ELSE
              MOVE 0 TO IND-TDR-IMP-PAG
           END-IF
           IF TDR-FL-SOLL-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-FL-SOLL
           ELSE
              MOVE 0 TO IND-TDR-FL-SOLL
           END-IF
           IF TDR-FRAZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-FRAZ
           ELSE
              MOVE 0 TO IND-TDR-FRAZ
           END-IF
           IF TDR-DT-APPLZ-MORA-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-DT-APPLZ-MORA
           ELSE
              MOVE 0 TO IND-TDR-DT-APPLZ-MORA
           END-IF
           IF TDR-FL-MORA-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-FL-MORA
           ELSE
              MOVE 0 TO IND-TDR-FL-MORA
           END-IF
           IF TDR-ID-RAPP-RETE-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-ID-RAPP-RETE
           ELSE
              MOVE 0 TO IND-TDR-ID-RAPP-RETE
           END-IF
           IF TDR-ID-RAPP-ANA-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-ID-RAPP-ANA
           ELSE
              MOVE 0 TO IND-TDR-ID-RAPP-ANA
           END-IF
           IF TDR-COD-DVS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-COD-DVS
           ELSE
              MOVE 0 TO IND-TDR-COD-DVS
           END-IF
           IF TDR-DT-ESI-TIT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-DT-ESI-TIT
           ELSE
              MOVE 0 TO IND-TDR-DT-ESI-TIT
           END-IF
           IF TDR-TOT-PRE-NET-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-PRE-NET
           ELSE
              MOVE 0 TO IND-TDR-TOT-PRE-NET
           END-IF
           IF TDR-TOT-INTR-FRAZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-INTR-FRAZ
           ELSE
              MOVE 0 TO IND-TDR-TOT-INTR-FRAZ
           END-IF
           IF TDR-TOT-INTR-MORA-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-INTR-MORA
           ELSE
              MOVE 0 TO IND-TDR-TOT-INTR-MORA
           END-IF
           IF TDR-TOT-INTR-PREST-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-INTR-PREST
           ELSE
              MOVE 0 TO IND-TDR-TOT-INTR-PREST
           END-IF
           IF TDR-TOT-INTR-RETDT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-INTR-RETDT
           ELSE
              MOVE 0 TO IND-TDR-TOT-INTR-RETDT
           END-IF
           IF TDR-TOT-INTR-RIAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-INTR-RIAT
           ELSE
              MOVE 0 TO IND-TDR-TOT-INTR-RIAT
           END-IF
           IF TDR-TOT-DIR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-DIR
           ELSE
              MOVE 0 TO IND-TDR-TOT-DIR
           END-IF
           IF TDR-TOT-SPE-MED-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-SPE-MED
           ELSE
              MOVE 0 TO IND-TDR-TOT-SPE-MED
           END-IF
           IF TDR-TOT-SPE-AGE-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-SPE-AGE
           ELSE
              MOVE 0 TO IND-TDR-TOT-SPE-AGE
           END-IF
           IF TDR-TOT-TAX-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-TAX
           ELSE
              MOVE 0 TO IND-TDR-TOT-TAX
           END-IF
           IF TDR-TOT-SOPR-SAN-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-SOPR-SAN
           ELSE
              MOVE 0 TO IND-TDR-TOT-SOPR-SAN
           END-IF
           IF TDR-TOT-SOPR-TEC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-SOPR-TEC
           ELSE
              MOVE 0 TO IND-TDR-TOT-SOPR-TEC
           END-IF
           IF TDR-TOT-SOPR-SPO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-SOPR-SPO
           ELSE
              MOVE 0 TO IND-TDR-TOT-SOPR-SPO
           END-IF
           IF TDR-TOT-SOPR-PROF-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-SOPR-PROF
           ELSE
              MOVE 0 TO IND-TDR-TOT-SOPR-PROF
           END-IF
           IF TDR-TOT-SOPR-ALT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-SOPR-ALT
           ELSE
              MOVE 0 TO IND-TDR-TOT-SOPR-ALT
           END-IF
           IF TDR-TOT-PRE-TOT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-PRE-TOT
           ELSE
              MOVE 0 TO IND-TDR-TOT-PRE-TOT
           END-IF
           IF TDR-TOT-PRE-PP-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-PRE-PP-IAS
           ELSE
              MOVE 0 TO IND-TDR-TOT-PRE-PP-IAS
           END-IF
           IF TDR-TOT-CAR-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-CAR-IAS
           ELSE
              MOVE 0 TO IND-TDR-TOT-CAR-IAS
           END-IF
           IF TDR-TOT-PRE-SOLO-RSH-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-PRE-SOLO-RSH
           ELSE
              MOVE 0 TO IND-TDR-TOT-PRE-SOLO-RSH
           END-IF
           IF TDR-TOT-PROV-ACQ-1AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-PROV-ACQ-1AA
           ELSE
              MOVE 0 TO IND-TDR-TOT-PROV-ACQ-1AA
           END-IF
           IF TDR-TOT-PROV-ACQ-2AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-PROV-ACQ-2AA
           ELSE
              MOVE 0 TO IND-TDR-TOT-PROV-ACQ-2AA
           END-IF
           IF TDR-TOT-PROV-RICOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-PROV-RICOR
           ELSE
              MOVE 0 TO IND-TDR-TOT-PROV-RICOR
           END-IF
           IF TDR-TOT-PROV-INC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-PROV-INC
           ELSE
              MOVE 0 TO IND-TDR-TOT-PROV-INC
           END-IF
           IF TDR-TOT-PROV-DA-REC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-PROV-DA-REC
           ELSE
              MOVE 0 TO IND-TDR-TOT-PROV-DA-REC
           END-IF
           IF TDR-IMP-AZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-IMP-AZ
           ELSE
              MOVE 0 TO IND-TDR-IMP-AZ
           END-IF
           IF TDR-IMP-ADER-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-IMP-ADER
           ELSE
              MOVE 0 TO IND-TDR-IMP-ADER
           END-IF
           IF TDR-IMP-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-IMP-TFR
           ELSE
              MOVE 0 TO IND-TDR-IMP-TFR
           END-IF
           IF TDR-IMP-VOLO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-IMP-VOLO
           ELSE
              MOVE 0 TO IND-TDR-IMP-VOLO
           END-IF
           IF TDR-FL-VLDT-TIT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-FL-VLDT-TIT
           ELSE
              MOVE 0 TO IND-TDR-FL-VLDT-TIT
           END-IF
           IF TDR-TOT-CAR-ACQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-CAR-ACQ
           ELSE
              MOVE 0 TO IND-TDR-TOT-CAR-ACQ
           END-IF
           IF TDR-TOT-CAR-GEST-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-CAR-GEST
           ELSE
              MOVE 0 TO IND-TDR-TOT-CAR-GEST
           END-IF
           IF TDR-TOT-CAR-INC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-CAR-INC
           ELSE
              MOVE 0 TO IND-TDR-TOT-CAR-INC
           END-IF
           IF TDR-TOT-MANFEE-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-MANFEE-ANTIC
           ELSE
              MOVE 0 TO IND-TDR-TOT-MANFEE-ANTIC
           END-IF
           IF TDR-TOT-MANFEE-RICOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-MANFEE-RICOR
           ELSE
              MOVE 0 TO IND-TDR-TOT-MANFEE-RICOR
           END-IF
           IF TDR-TOT-MANFEE-REC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-MANFEE-REC
           ELSE
              MOVE 0 TO IND-TDR-TOT-MANFEE-REC
           END-IF
           IF TDR-IMP-TRASFE-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-IMP-TRASFE
           ELSE
              MOVE 0 TO IND-TDR-IMP-TRASFE
           END-IF
           IF TDR-IMP-TFR-STRC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-IMP-TFR-STRC
           ELSE
              MOVE 0 TO IND-TDR-IMP-TFR-STRC
           END-IF
           IF TDR-TOT-ACQ-EXP-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-ACQ-EXP
           ELSE
              MOVE 0 TO IND-TDR-TOT-ACQ-EXP
           END-IF
           IF TDR-TOT-REMUN-ASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-REMUN-ASS
           ELSE
              MOVE 0 TO IND-TDR-TOT-REMUN-ASS
           END-IF
           IF TDR-TOT-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-COMMIS-INTER
           ELSE
              MOVE 0 TO IND-TDR-TOT-COMMIS-INTER
           END-IF
           IF TDR-TOT-CNBT-ANTIRAC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-TOT-CNBT-ANTIRAC
           ELSE
              MOVE 0 TO IND-TDR-TOT-CNBT-ANTIRAC
           END-IF
           IF TDR-FL-INC-AUTOGEN-NULL = HIGH-VALUES
              MOVE -1 TO IND-TDR-FL-INC-AUTOGEN
           ELSE
              MOVE 0 TO IND-TDR-FL-INC-AUTOGEN
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : TDR-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE TIT-RAT TO WS-BUFFER-TABLE.

           MOVE TDR-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO TDR-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO TDR-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO TDR-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO TDR-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO TDR-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO TDR-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO TDR-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE TIT-RAT TO WS-BUFFER-TABLE.

           MOVE TDR-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO TIT-RAT.

           MOVE WS-ID-MOVI-CRZ  TO TDR-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO TDR-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO TDR-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO TDR-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO TDR-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO TDR-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO TDR-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE TDR-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO TDR-DT-INI-EFF-DB
           MOVE TDR-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO TDR-DT-END-EFF-DB
           IF IND-TDR-DT-INI-COP = 0
               MOVE TDR-DT-INI-COP TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TDR-DT-INI-COP-DB
           END-IF
           IF IND-TDR-DT-END-COP = 0
               MOVE TDR-DT-END-COP TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TDR-DT-END-COP-DB
           END-IF
           IF IND-TDR-DT-APPLZ-MORA = 0
               MOVE TDR-DT-APPLZ-MORA TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TDR-DT-APPLZ-MORA-DB
           END-IF
           MOVE TDR-DT-EMIS-TIT TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO TDR-DT-EMIS-TIT-DB
           IF IND-TDR-DT-ESI-TIT = 0
               MOVE TDR-DT-ESI-TIT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TDR-DT-ESI-TIT-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE TDR-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO TDR-DT-INI-EFF
           MOVE TDR-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO TDR-DT-END-EFF
           IF IND-TDR-DT-INI-COP = 0
               MOVE TDR-DT-INI-COP-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TDR-DT-INI-COP
           END-IF
           IF IND-TDR-DT-END-COP = 0
               MOVE TDR-DT-END-COP-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TDR-DT-END-COP
           END-IF
           IF IND-TDR-DT-APPLZ-MORA = 0
               MOVE TDR-DT-APPLZ-MORA-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TDR-DT-APPLZ-MORA
           END-IF
           MOVE TDR-DT-EMIS-TIT-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO TDR-DT-EMIS-TIT
           IF IND-TDR-DT-ESI-TIT = 0
               MOVE TDR-DT-ESI-TIT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TDR-DT-ESI-TIT
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

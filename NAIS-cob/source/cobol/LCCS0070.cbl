      ******************************************************************
      *                                                                *
      *    PORTAFOGLIO VITA ITALIA  VER 1.0                            *
      *                                                                *
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LCCS0070.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2009.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *    PROGRAMMA ..... LCCS0070
      *    TIPOLOGIA...... SERVIZIO TRASVERSALE
      *    PROCESSO....... XXX
      *    FUNZIONE....... XXX
      *    DESCRIZIONE.... CALCOLA IB OGGETTO PER ADESIONE,GARANZIA
      *                    E TRANCHE
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01 WK-PGM                            PIC X(008) VALUE 'LCCS0070'.
       01 WK-TABELLA                        PIC X(030) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01 WS-VARIABILI.
      *-- NUMERAZIONE ADESIONE GARANZIA E TRANCHE
          03 WS-IB-OGGETTO.
             05 WS-NUM-APPO                  PIC 9(009) VALUE ZEROES.
             05 WS-NUMER-FILLER              PIC X(031) VALUE SPACES.

      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
      *01  IX-INDICI.
      *----------------------------------------------------------------*
      *    FLAG
      *----------------------------------------------------------------*
       01 WS-OGGETTO-IN-PTF                PIC X(001) VALUE SPACES.
          88 OGGETTO-IN-PTF-SI             VALUE 'S'.
          88 OGGETTO-IN-PTF-NO             VALUE 'N'.

      *----------------------------------------------------------------*
      *    COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    COPY DISPATCHER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
           COPY IDSI0011.
           COPY IDSO0011.

      *----------------------------------------------------------------*
      *    COPY DELLE TABELLE DB2
      *----------------------------------------------------------------*
           COPY IDBVNUM1.

      *----------------------------------------------------------------*
      *    MODULI CHIAMATI
      *----------------------------------------------------------------*

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.

       01  AREA-IO-LCCS0070.
           COPY LCCC0070.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                AREA-IO-LCCS0070.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                      WS-VARIABILI

           PERFORM S0005-CTRL-DATI-INPUT
              THRU EX-S0005.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLO DATI INPUT
      *----------------------------------------------------------------*
       S0005-CTRL-DATI-INPUT.

           EVALUATE TRUE
             WHEN LCCC0070-CALC-IB-ADE
                IF LCCC0070-TP-OGGETTO NOT = 'PO'
                   MOVE WK-PGM
                     TO IEAI9901-COD-SERVIZIO-BE
                   MOVE 'S0005-CTRL-DATI-INPUT'
                     TO IEAI9901-LABEL-ERR
                   MOVE '005020'               TO IEAI9901-COD-ERRORE
                   MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
                END-IF
             WHEN LCCC0070-CALC-IB-GAR
                IF LCCC0070-TP-OGGETTO NOT = 'AD'
                   MOVE WK-PGM
                     TO IEAI9901-COD-SERVIZIO-BE
                   MOVE 'S0005-CTRL-DATI-INPUT'
                     TO IEAI9901-LABEL-ERR
                   MOVE '005020'               TO IEAI9901-COD-ERRORE
                   MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
                END-IF
             WHEN LCCC0070-CALC-IB-TGA
                IF LCCC0070-TP-OGGETTO NOT = 'GA'
                   MOVE WK-PGM
                     TO IEAI9901-COD-SERVIZIO-BE
                   MOVE 'S0005-CTRL-DATI-INPUT'
                     TO IEAI9901-LABEL-ERR
                   MOVE '005020'               TO IEAI9901-COD-ERRORE
                   MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
                END-IF
             WHEN OTHER
                MOVE WK-PGM
                  TO IEAI9901-COD-SERVIZIO-BE
                MOVE 'S0005-CTRL-DATI-INPUT'
                  TO IEAI9901-LABEL-ERR
                MOVE '005020'               TO IEAI9901-COD-ERRORE
                MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
                PERFORM S0300-RICERCA-GRAVITA-ERRORE
                   THRU EX-S0300
           END-EVALUATE.


       EX-S0005.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           PERFORM S1010-LETT-ADE-GAR-TRA
              THRU S1010-EX

           IF IDSV0001-ESITO-OK
              IF OGGETTO-IN-PTF-SI
      *--        Aggiornamento occorrenza in tabella
                 PERFORM S1020-UPDATE-ADE-GAR-TRA
                    THRU S1020-EX
                 IF IDSV0001-ESITO-OK
                    MOVE WS-IB-OGGETTO TO LCCC0070-IB-OGGETTO
                 END-IF
              ELSE
      *--        Inserimento occorrenza in tabella
                 PERFORM S1030-INSERT-ADE-GAR-TRA
                    THRU S1030-EX
                 IF IDSV0001-ESITO-OK
                    MOVE WS-IB-OGGETTO TO LCCC0070-IB-OGGETTO
                 END-IF
              END-IF
           END-IF.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    Lettura della tabella numerazione ade/gar/tga
      *----------------------------------------------------------------*
       S1010-LETT-ADE-GAR-TRA.

           SET OGGETTO-IN-PTF-NO TO TRUE

           INITIALIZE NUM-ADE-GAR-TRA

           MOVE IDSV0001-DATA-EFFETTO    TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
           MOVE IDSV0001-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-SELECT              TO TRUE

           MOVE 'NUM-ADE-GAR-TRA'           TO WK-TABELLA

           SET IDSI0011-PRIMARY-KEY         TO TRUE
           SET IDSI0011-TRATT-SENZA-STOR    TO TRUE

           MOVE IDSV0001-COD-COMPAGNIA-ANIA TO NUM-COD-COMP-ANIA
           MOVE LCCC0070-ID-OGGETTO         TO NUM-ID-OGG
           MOVE LCCC0070-TP-OGGETTO         TO NUM-TP-OGG

           MOVE WK-TABELLA                  TO IDSI0011-CODICE-STR-DATO

           MOVE NUM-ADE-GAR-TRA             TO IDSI0011-BUFFER-DATI.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                     CONTINUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                     SET OGGETTO-IN-PTF-SI       TO TRUE
                     MOVE IDSO0011-BUFFER-DATI TO NUM-ADE-GAR-TRA
              END-EVALUATE
           ELSE
              MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SELECT-DISPATCHER'      TO IEAI9901-LABEL-ERR
              MOVE '005016'                 TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S1010-EX .
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1020-UPDATE-ADE-GAR-TRA.

           MOVE IDSV0001-DATA-EFFETTO    TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
           MOVE IDSV0001-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA

           MOVE 'NUM-ADE-GAR-TRA'           TO WK-TABELLA
           ADD  1                           TO NUM-ULT-NUM-LIN
           MOVE IDSV0001-COD-COMPAGNIA-ANIA TO NUM-COD-COMP-ANIA
           MOVE LCCC0070-ID-OGGETTO         TO NUM-ID-OGG
           MOVE LCCC0070-TP-OGGETTO         TO NUM-TP-OGG

           SET IDSI0011-UPDATE              TO TRUE
           SET IDSI0011-PRIMARY-KEY         TO TRUE
           SET IDSI0011-TRATT-SENZA-STOR    TO TRUE

           MOVE WK-TABELLA                  TO IDSI0011-CODICE-STR-DATO
           MOVE NUM-ADE-GAR-TRA             TO IDSI0011-BUFFER-DATI

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                   INITIALIZE WS-IB-OGGETTO
                   MOVE NUM-ULT-NUM-LIN      TO WS-NUM-APPO
              END-EVALUATE
           ELSE
              MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1020-UPDATE-ADE-GAR-TRA'
                                            TO IEAI9901-LABEL-ERR
              MOVE '005016'                 TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S1020-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1030-INSERT-ADE-GAR-TRA.

           INITIALIZE NUM-ADE-GAR-TRA

           MOVE 'NUM-ADE-GAR-TRA'           TO WK-TABELLA
           ADD  1                           TO NUM-ULT-NUM-LIN
           MOVE IDSV0001-COD-COMPAGNIA-ANIA TO NUM-COD-COMP-ANIA
           MOVE LCCC0070-ID-OGGETTO         TO NUM-ID-OGG
           MOVE LCCC0070-TP-OGGETTO         TO NUM-TP-OGG

           SET IDSI0011-INSERT              TO TRUE
           SET IDSI0011-PRIMARY-KEY         TO TRUE
           SET IDSI0011-TRATT-SENZA-STOR    TO TRUE

           MOVE WK-TABELLA                  TO IDSI0011-CODICE-STR-DATO
           MOVE NUM-ADE-GAR-TRA             TO IDSI0011-BUFFER-DATI

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                   INITIALIZE WS-IB-OGGETTO
                   MOVE NUM-ULT-NUM-LIN      TO WS-NUM-APPO
              END-EVALUATE
           ELSE
              MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1030-INSERT-ADE-GAR-TRA'
                                            TO IEAI9901-LABEL-ERR
              MOVE '005016'                 TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S1030-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.

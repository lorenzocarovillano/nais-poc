       IDENTIFICATION DIVISION.
       PROGRAM-ID. IDSS0020 IS INITIAL.
       AUTHOR. ATS NAPOLI.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER. IBM-AT.
       OBJECT-COMPUTER. IBM-AT.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01 INDICE-ANAGR-DATO       PIC 9(03) VALUE 0.
       01 WK-NUMERO-LIVELLO       PIC 9(03) VALUE 0.
       01 WK-NUMERO-LIVELLO-RED   PIC 9(03) VALUE 0.
       01 WK-LIVELLO-PADRE        PIC 9(03) VALUE 1.
       01 WK-INIZIO-POSIZIONE     PIC S9(05) COMP-3 VALUE 1.
       01 WK-INIZIO-POSIZIONE-RED PIC S9(05) COMP-3.
       01 WK-INIZIO-POSIZIONE-RIC PIC S9(05) COMP-3.
       01 WK-INIZIO-POSIZIONE-MOL PIC S9(05) COMP-3.
       01 WK-NUMERO-RICORRENZA    PIC S9(05) COMP-3.

       77 WK-SQLCODE                      PIC S9(009).

       01 FLAG-REDEFINES      PIC X(01) VALUE 'N'.
          88 CAMPI-REDEFINES            VALUE 'S'.
          88 CAMPI-NON-REDEFINES        VALUE 'N'.


       01 WK-CALCOLO-PADRE.
          05 WK-ELEMENTI-PADRE OCCURS 10.
             10 WK-INDICE-PADRE      PIC 9(03).
             10 WK-LUNG-PADRE        PIC S9(05) COMP-3.
             10 WK-NUMERO-RICORRENZE PIC S9(05) COMP-3.

       01 WK-CALCOLO-PADRE-RED.
          05 WK-ELEMENTI-PADRE-RED OCCURS 10.
             10 WK-INDICE-PADRE-RED PIC 9(03).
             10 WK-LUNG-PADRE-RED   PIC S9(05) COMP-3.
             10 WK-NUMERO-RICORRENZE-RED PIC S9(05) COMP-3.

       01 WK-LIMITE-LIVELLI     PIC 9(02) VALUE 10.


       01 AREA-SCREMATURA.
          COPY IDSV0042 REPLACING ==(DS)== BY ==SCR==.

       01 PGM-NAME-IDSS0020           PIC X(08) VALUE 'IDSS0020'.
       01 PGM-NAME-IDSS0080           PIC X(08) VALUE 'IDSS0080'.


      *************************************************************************
      *   COPY DI WORKING CENTRALIZZATA X GESTIONE DATABASE SERVICES
      *************************************************************************
CTU****   EXEC SQL INCLUDE IDSV0009 END-EXEC.
      ***************************************************************
      * CAMPI LIMITE
      ***************************************************************
       01 WK-LIMITE-COPY-I-O             PIC 9(02) VALUE  2.
       01 WK-LIMITE-STR-DATO             PIC 9(04) VALUE 1000.
       01 WK-LIMITE-USING-AREA           PIC 9(02) VALUE 30.
       01 WK-LIMITE-SERVIZI              PIC 9(02) VALUE 05.
       01 WK-LIMITE-CODA                 PIC 9(02) VALUE 10.
      ***************************************************************
      *           CAMPI FLAG
      ***************************************************************
       01 PGM-CHIAMATO                   PIC X(08) VALUE SPACES.
          88 PGM-IDSS0020                VALUE 'IDSS0020'.
          88 PGM-IDSS0030                VALUE 'IDSS0030'.
          88 PGM-IDSS0040                VALUE 'IDSS0040'.
          88 PGM-IDSS0050                VALUE 'IDSS0050'.
          88 PGM-IDSS0060                VALUE 'IDSS0060'.
          88 PGM-IDSS0070                VALUE 'IDSS0070'.
          88 PGM-IDSS0090                VALUE 'IDSS0090'.
          88 PGM-IDSS0110                VALUE 'IDSS0110'.
          88 PGM-IDSS0120                VALUE 'IDSS0120'.
          88 PGM-IDSS0130                VALUE 'IDSS0130'.
          88 PGM-IDBSSMR0                VALUE 'IDBSSMR0'.
       01 CONVERSION-VARIABLES.
          05 WK-CTRL-LUNG-CAMPO-TIPO PIC X(02) VALUE SPACES.
          05 WK-CAMPO-TIPO-COMP      PIC X(02) VALUE 'C0'.
          05 WK-CAMPO-TIPO-COMP1     PIC X(02) VALUE 'C1'.
          05 WK-CAMPO-TIPO-COMP2     PIC X(02) VALUE 'C2'.
          05 WK-CAMPO-TIPO-COMP3     PIC X(02) VALUE 'C3'.
          05 WK-CAMPO-TIPO-COMP4     PIC X(02) VALUE 'C4'.
          05 WK-CAMPO-TIPO-COMP5     PIC X(02) VALUE 'C5'.
          05 WK-CAMPO-TIPO-NN        PIC X(02) VALUE 'NN'.
          05 WK-CAMPO-TIPO-NS        PIC X(02) VALUE 'NS'.
          05 WK-CAMPO-TIPO-AL        PIC X(02) VALUE 'AL'.
          05 WK-CTRL-LUNG-CAMPO-LUNG     PIC S9(05) COMP-3.
          05 WK-CTRL-LUNG-CAMPO-LUNG-EFF PIC S9(05) COMP-3.
       01 WK-TIPO-AREA                   PIC X(01).
          88 WK-AREA-UNICA               VALUE 'U'.
          88 WK-MULTI-AREA               VALUE 'M'.
       01 WK-FLAG-CONVERSIONE            PIC X(01) VALUE 'N'.
          88 DATO-DA-CONVERTIRE          VALUE 'S'.
          88 DATO-DA-NON-CONVERTIRE      VALUE 'N'.
       01 WK-CAMPO-ATTIVO                PIC X(01) VALUE 'S'.
       01 WK-CAMPO-MEMO                  PIC X(01) VALUE 'M'.
       01 WK-CAMPO-READ                  PIC X(01) VALUE 'R'.
       01 WK-CAMPO-PRESA-DIRETTA         PIC X(01) VALUE 'P'.
       01 WK-CAMPO-JOLLY                 PIC X(01) VALUE '*'.
       01 WK-CAMPO-SRV-MAIN  PIC X(10) VALUE 'SRV_MAIN'.
       01 SPECIAL-WORD       PIC X(30) VALUE 'FILLER'.
       01 SPECIAL-WORD2      PIC X(30) VALUE 'RETURN-CODE'.
       77 WK-TIPO-STR-DATO               PIC X(01).
       77 STR-DATO-INPUT                 PIC X(01) VALUE 'I'.
       77 STR-DATO-OUTPUT                PIC X(01) VALUE 'O'.
       77 DESCRIZ-ERR-DB2 PIC  X(300) VALUE SPACES.
       01 WK-CAMPO-TROVATO               PIC X(01) VALUE 'N'.
          88 WK-TROVATO                            VALUE 'S'.
          88 WK-NON-TROVATO                        VALUE 'N'.
       01 WK-TIPO-SERVIZIO.
          05 WK-SERVIZIO-VSAM            PIC X(03) VALUE 'VSM'.
          05 WK-SERVIZIO-DB2             PIC X(03) VALUE 'DB2'.
          05 WK-SERVIZIO-ORACLE          PIC X(03) VALUE 'ORA'.
          05 WK-SERVIZIO-CONVERSIONE     PIC X(03) VALUE 'CON'.
       01 WS-TIMESTAMP          PIC X(18).
       01 WS-TIMESTAMP-NUM REDEFINES WS-TIMESTAMP PIC 9(18).
       01 WHERE-VARIABLES.
         05 STR-WH-COD-COMP-ANIA  PIC S9(05)V COMP-3.
         05 STR-WH-COD-STR-DATO-VCHAR.
            49 STR-WH-COD-STR-DATO-LEN PIC S9(4) COMP-5.
            49 STR-WH-COD-STR-DATO PIC X(30).
      ***************************************************************
      * INDICI
      ***************************************************************
       01 IND-INP-OUT                    PIC 9(02) VALUE 0.
       01 IND-USING-AREA                 PIC 9(04).
       01 IND-CALL-USING                 PIC 9(04).
       01 IND-SERV                       PIC 9(02) VALUE 0.
       01 IND-SINONIM                    PIC 9(01) VALUE 0.
       01 IND-CODA                       PIC 9(02) VALUE 0.
       01 IND-DESTINATARIO               PIC 9(04) VALUE 0.

      ***************************************************************
      * AREE DI COMUNICAZIONE
      ***************************************************************
CTU****       01 AREA-LINK                 PIC X(32000).
CTU****       01 AREA-CALL-I-O.
CTU****        02 AREA-CALL-I-O-TOT OCCURS 30 TIMES.
       01 AREA-CALL-USING-ELEMENTS.
CTU****                    REDEFINES AREA-CALL-I-O.
          05 AREA-CALL-I-O-01       PIC X(2000).
          05 AREA-CALL-I-O-02       PIC X(2000).
          05 AREA-CALL-I-O-03       PIC X(2000).
          05 AREA-CALL-I-O-04       PIC X(2000).
          05 AREA-CALL-I-O-05       PIC X(2000).
          05 AREA-CALL-I-O-06       PIC X(2000).
          05 AREA-CALL-I-O-07       PIC X(2000).
          05 AREA-CALL-I-O-08       PIC X(2000).
          05 AREA-CALL-I-O-09       PIC X(2000).
          05 AREA-CALL-I-O-10       PIC X(2000).
          05 AREA-CALL-I-O-11       PIC X(2000).
          05 AREA-CALL-I-O-12       PIC X(2000).
          05 AREA-CALL-I-O-13       PIC X(2000).
          05 AREA-CALL-I-O-14       PIC X(2000).
          05 AREA-CALL-I-O-15       PIC X(2000).
          05 AREA-CALL-I-O-16       PIC X(2000).
          05 AREA-CALL-I-O-17       PIC X(2000).
          05 AREA-CALL-I-O-18       PIC X(2000).
          05 AREA-CALL-I-O-19       PIC X(2000).
          05 AREA-CALL-I-O-20       PIC X(2000).
          05 AREA-CALL-I-O-21       PIC X(2000).
          05 AREA-CALL-I-O-22       PIC X(2000).
          05 AREA-CALL-I-O-23       PIC X(2000).
          05 AREA-CALL-I-O-24       PIC X(2000).
          05 AREA-CALL-I-O-25       PIC X(2000).
          05 AREA-CALL-I-O-26       PIC X(2000).
          05 AREA-CALL-I-O-27       PIC X(2000).
          05 AREA-CALL-I-O-28       PIC X(2000).
          05 AREA-CALL-I-O-29       PIC X(2000).
          05 AREA-CALL-I-O-30       PIC X(2000).
      ***************************************************************
      *   CAMPI COMODO
      ***************************************************************
       01 COMODO-BUFFER-DATI-SERV PIC  X(30000) VALUE SPACES.
       01 COMODO-BUFFER-DATI-MITT PIC  X(30000) VALUE SPACES.
       01 COMODO-BUFFER-DATI-DEST PIC  X(30000) VALUE SPACES.
       01 COMODO-INIZIO-POS-U-A   PIC S9(05) COMP-3.
       01 COMODO-CODICE-STR-DATO  PIC  X(30).
       01 COMODO-SQLCODE          PIC S9(009) VALUE +0.
       01 COMODO-DESCRIZ-ERR-DB2  PIC  X(300) VALUE SPACES.
       01 COMODO-INIZIO-POSIZIONE PIC 9(03) VALUE 1.
       01 COMODO-KEY-ARCHIVIO     PIC X(100) VALUE SPACES.
       01 COMODO-SERV-CONVERSIONE PIC X(08) VALUE HIGH-VALUES.
       01 COMODO-X-SOTTRAZIONE    PIC S9(05) COMP-3.
       01 COMODO-TIPO-MOVIMENTO   PIC 9(05).
       01 COMODO-MITTENTE.
          05 FLAG-TRATTAMENTO OCCURS 1 TO 1000 TIMES
                               DEPENDING ON WK-LIMITE-STR-DATO
                               INDEXED BY IND-TRATTAMENTO.
             10 DATO-TRATTATO            PIC X(01).
       01 R999-CODICE-STR-DATO           PIC X(30).
       01 R999-STR-DATO.
      *     COPY IDSV0042 REPLACING ==(DS)== BY ==COM==.
      *   inizio COPY IDSV0042
      * -- STRUTTURA DATI
                10 COM-ELEMENTS-STR-DATO OCCURS 1000 TIMES
                                          INDEXED BY COM-IND.
                   15 COM-CODICE-DATO            PIC X(030).
                   15 COM-FLAG-KEY               PIC X(001).
                   15 COM-FLAG-RETURN-CODE       PIC X(001).
                   15 COM-FLAG-CALL-USING        PIC X(001).
                   15 COM-FLAG-WHERE-COND        PIC X(001).
                   15 COM-FLAG-REDEFINES         PIC X(001).
                   15 COM-TIPO-DATO              PIC X(002).
                   15 COM-INIZIO-POSIZIONE   PIC S9(05) COMP-3.
                   15 COM-LUNGHEZZA-DATO     PIC S9(05) COMP-3.
                   15 COM-LUNGHEZZA-DATO-NOM PIC S9(05) COMP-3.
                   15 COM-PRECISIONE-DATO    PIC S9(02) COMP-3.
                   15 COM-FORMATTAZIONE-DATO     PIC X(020).
                   15 COM-SERV-CONVERSIONE       PIC X(008).
                   15 COM-AREA-CONV-STANDARD     PIC X(001).
                   15 COM-CODICE-DOMINIO         PIC X(030).
                   15 COM-SERVIZIO-VALIDAZIONE   PIC X(008).
                   15 COM-RICORRENZA             PIC S9(05) COMP-3.
                   15 COM-CLONE                  PIC X(01).
                   15 COM-OBBLIGATORIETA         PIC X(001).
                   15 COM-VALORE-DEFAULT         PIC X(010).
      *   fine COPY IDSV0042
       01 TABELLA-USING-AREA.
         05 TABELLA-USING-AREA-ELEMENTS OCCURS 30 TIMES
                                 INDEXED BY   INDEX-CALL-USING.
            10 INIZIO-POS-U-A       PIC S9(05) COMP-3.
            10 LUNGHEZZA-U-A        PIC S9(05) COMP-3.
            10 FLAG-CALL-USING-U-A  PIC X(01).
            10 CODICE-STR-DATO-U-A  PIC X(30).
      ***************************************************************
      *            DEFINIZIONE CAMPI DI COMODO PER CODA TS
      ***************************************************************
       01 WK-NUM-REK                PIC S9(04) COMP  VALUE +1.
       01 LEN-CODA                  PIC S9(04) COMP VALUE 6500.
       01 WK-NOME-CODA.
          05 FILLER                 PIC X(04) VALUE 'DISP'.
          05 WK-EIBTRMID            PIC X(04) VALUE SPACES.

      ***************************************************************
      *  SEARCH AREA
      ***************************************************************
       01 SEARCH-AREA.
      *     COPY IDSV0042 REPLACING ==(DS)== BY ==SEARCH==.

      *   inizio COPY IDSV0042
      * -- STRUTTURA DATI
                10 SEARCH-ELEMENTS-STR-DATO OCCURS 1000 TIMES
                                          INDEXED BY SEARCH-IND.
                   15 SEARCH-CODICE-DATO            PIC X(030).
                   15 SEARCH-FLAG-KEY               PIC X(001).
                   15 SEARCH-FLAG-RETURN-CODE       PIC X(001).
                   15 SEARCH-FLAG-CALL-USING        PIC X(001).
                   15 SEARCH-FLAG-WHERE-COND        PIC X(001).
                   15 SEARCH-FLAG-REDEFINES         PIC X(001).
                   15 SEARCH-TIPO-DATO              PIC X(002).
                   15 SEARCH-INIZIO-POSIZIONE   PIC S9(05) COMP-3.
                   15 SEARCH-LUNGHEZZA-DATO     PIC S9(05) COMP-3.
                   15 SEARCH-LUNGHEZZA-DATO-NOM PIC S9(05) COMP-3.
                   15 SEARCH-PRECISIONE-DATO    PIC S9(02) COMP-3.
                   15 SEARCH-FORMATTAZIONE-DATO     PIC X(020).
                   15 SEARCH-SERV-CONVERSIONE       PIC X(008).
                   15 SEARCH-AREA-CONV-STANDARD     PIC X(001).
                   15 SEARCH-CODICE-DOMINIO         PIC X(030).
                   15 SEARCH-SERVIZIO-VALIDAZIONE   PIC X(008).
                   15 SEARCH-RICORRENZA             PIC S9(05) COMP-3.
                   15 SEARCH-CLONE                  PIC X(01).
                   15 SEARCH-OBBLIGATORIETA         PIC X(001).
                   15 SEARCH-VALORE-DEFAULT         PIC X(010).
      *   fine COPY IDSV0042

      ***************************************************************
      * COPY X STRUTTURE DATI
      ***************************************************************
          COPY IDSV0003.

       01 AREA-IDSV0045.
      *    COPY IDSV0045 REPLACING ==(DS)== BY ==IDSV0045==.
      * --
      *   inizio COPY IDSV0045
             05 IDSV0045-CODICE-SERVIZIO           PIC X(08).
             05 IDSV0045-OBBLIGATORIETA-SERV       PIC X(01).
             05 IDSV0045-FLAG-SHARED-MEMORY        PIC X(01).
             05 IDSV0045-TIPO-SERVIZIO             PIC X(03).
             05 IDSV0045-STRUTT-SERV-ELEMENTS.
              07 IDSV0045-STR-INP-OUT  OCCURS 2 TIMES.
                08 IDSV0045-FLAG-INPUT-OUTPUT      PIC X(01).
                08 IDSV0045-FLAG-CALL-USING-I-O    PIC X(01).
                08 IDSV0045-CODICE-STR-DATO        PIC X(30).

                08 IDSV0045-STRUTTURA-DATO.

      * -- STRUTTURA DATI
      *      COPY IDSV0042.
      *   inizio COPY IDSV0042
      * -- STRUTTURA DATI
                10 IDSV0045-ELEMENTS-STR-DATO OCCURS 1000 TIMES
                                          INDEXED BY IDSV0045-IND.
                   15 IDSV0045-CODICE-DATO            PIC X(030).
                   15 IDSV0045-FLAG-KEY               PIC X(001).
                   15 IDSV0045-FLAG-RETURN-CODE       PIC X(001).
                   15 IDSV0045-FLAG-CALL-USING        PIC X(001).
                   15 IDSV0045-FLAG-WHERE-COND        PIC X(001).
                   15 IDSV0045-FLAG-REDEFINES         PIC X(001).
                   15 IDSV0045-TIPO-DATO              PIC X(002).
                   15 IDSV0045-INIZIO-POSIZIONE   PIC S9(05) COMP-3.
                   15 IDSV0045-LUNGHEZZA-DATO     PIC S9(05) COMP-3.
                   15 IDSV0045-LUNGHEZZA-DATO-NOM PIC S9(05) COMP-3.
                   15 IDSV0045-PRECISIONE-DATO    PIC S9(02) COMP-3.
                   15 IDSV0045-FORMATTAZIONE-DATO     PIC X(020).
                   15 IDSV0045-SERV-CONVERSIONE       PIC X(008).
                   15 IDSV0045-AREA-CONV-STANDARD     PIC X(001).
                   15 IDSV0045-CODICE-DOMINIO         PIC X(030).
                   15 IDSV0045-SERVIZIO-VALIDAZIONE   PIC X(008).
                   15 IDSV0045-RICORRENZA             PIC S9(05) COMP-3.
                   15 IDSV0045-CLONE                  PIC X(01).
                   15 IDSV0045-OBBLIGATORIETA         PIC X(001).
                   15 IDSV0045-VALORE-DEFAULT         PIC X(010).
      *   fine COPY IDSV0042
      *   fine COPY IDSV0045
      *************************************************************************
      *    COPY INPUT
      *************************************************************************
       01 INPUT-IDSS0020.
           COPY IDSI0021.

      *************************************************************************
      *    COPY OUTPUT
      *************************************************************************
       01 OUTPUT-IDSS0020.
           COPY IDSO0021.

           EXEC SQL INCLUDE SQLCA    END-EXEC.
           EXEC SQL INCLUDE IDBDADA0 END-EXEC.
           EXEC SQL INCLUDE IDBVADA1 END-EXEC.
           EXEC SQL INCLUDE IDBVADA2 END-EXEC.
           EXEC SQL INCLUDE IDBDCSD0 END-EXEC.
           EXEC SQL INCLUDE IDBVCSD1 END-EXEC.
           EXEC SQL INCLUDE IDBVCSD2 END-EXEC.
           EXEC SQL INCLUDE IDBDSST0 END-EXEC.
           EXEC SQL INCLUDE IDBVSST1 END-EXEC.
           EXEC SQL INCLUDE IDBDRDS0 END-EXEC.
           EXEC SQL INCLUDE IDBVRDS1 END-EXEC.

       LOCAL-STORAGE SECTION.

       01 WK-COUNTER              PIC X(01) VALUE 'N'.

       01 INDEX-PADRE                PIC 9(03).
       01 COMODO-INDICE-PADRE        PIC 9(03).

       LINKAGE SECTION.
       01 AREA-CALL.
          COPY IDSV0044.

      *************************************************************************
       PROCEDURE DIVISION USING AREA-CALL.
      *************************************************************************
       MAIN.
      *
ALEX  *     MOVE SPACES               TO INPUT-IDSS0020
           MOVE AREA-CALL            TO INPUT-IDSS0020.
           MOVE 'COMP_STR_DATO'      TO IDSO0021-NOME-TABELLA.

           PERFORM A000-INIZIALIZZA-OUTPUT.


           IF IDSO0021-SUCCESSFUL-RC
              MOVE IDSI0021-CODICE-COMPAGNIA-ANIA
                                            TO CSD-COD-COMPAGNIA-ANIA
              MOVE IDSI0021-CODICE-STR-DATO TO CSD-COD-STR-DATO

              ADD  1                        TO WK-NUMERO-LIVELLO

              IF WK-NUMERO-LIVELLO > WK-LIMITE-LIVELLI
                 MOVE 'SUPERATO LIMITE MASSIMO DI RICORSIONI'
                                TO IDSO0021-DESCRIZ-ERR-DB2
                 PERFORM 2000-DBERROR
                 SET IDSO0021-EXCESS-OF-RECURSION TO TRUE
              END-IF

              IF IDSO0021-SUCCESSFUL-RC
                 PERFORM A100-DECLARE
                 PERFORM A200-OPEN

                 IF IDSO0021-SUCCESSFUL-RC
                    PERFORM A300-FETCH
                 END-IF

                 IF IDSO0021-SUCCESSFUL-RC
                    PERFORM A400-CLOSE
                    PERFORM A500-OPERAZIONI-FINALI
                    PERFORM 4000-FINE
                 END-IF
              END-IF

           END-IF

           MOVE SPACES                    TO AREA-CALL
           MOVE OUTPUT-IDSS0020           TO AREA-CALL.


           GOBACK.

      *************************************************************************
       A000-INIZIALIZZA-OUTPUT SECTION.
      *
           IF IDSI0021-INITIALIZE-SI
              MOVE HIGH-VALUE           TO OUTPUT-IDSS0020
              INITIALIZE
                                        WK-NUMERO-LIVELLO
                                        INDICE-ANAGR-DATO
                                        WK-CALCOLO-PADRE
                                        WK-CALCOLO-PADRE-RED
                                        WK-INIZIO-POSIZIONE-RED
                                        WK-INIZIO-POSIZIONE-RIC
                                        WK-INIZIO-POSIZIONE-MOL
                                        WK-NUMERO-RICORRENZA
                                        IND-SINONIM
              MOVE 'N'               TO FLAG-REDEFINES
              MOVE 1                 TO WK-INIZIO-POSIZIONE
              SET IDSI0021-INITIALIZE-NO TO TRUE
              SET IDSO0021-SUCCESSFUL-RC TO TRUE

              PERFORM S000-ELABORA-SST
              MOVE PGM-NAME-IDSS0020     TO IDSO0021-COD-SERVIZIO-BE
           END-IF.

      *
       A000-EX.
           EXIT.

      *************************************************************************
       A100-DECLARE SECTION.
      *
           EVALUATE WK-NUMERO-LIVELLO
           WHEN 1
              EXEC SQL DECLARE CUR1 CURSOR FOR
                SELECT
                COD_COMPAGNIA_ANIA
                ,COD_STR_DATO
                ,POSIZIONE
                ,COD_STR_DATO2
                ,COD_DATO
                ,FLAG_KEY
                ,FLAG_RETURN_CODE
                ,FLAG_CALL_USING
                ,FLAG_WHERE_COND
                ,FLAG_REDEFINES
                ,TIPO_DATO
                ,LUNGHEZZA_DATO
                ,PRECISIONE_DATO
                ,COD_DOMINIO
                ,FORMATTAZIONE_DATO
                ,SERVIZIO_CONVERS
                ,AREA_CONV_STANDARD
                ,RICORRENZA
                ,OBBLIGATORIETA
                ,VALORE_DEFAULT
                ,DS_UTENTE
                 FROM COMP_STR_DATO
               WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
                 AND COD_STR_DATO       = :CSD-COD-STR-DATO
                 ORDER BY POSIZIONE
              END-EXEC
              CONTINUE

           WHEN 2
            EXEC SQL DECLARE CUR2 CURSOR FOR
                SELECT
                COD_COMPAGNIA_ANIA
                ,COD_STR_DATO
                ,POSIZIONE
                ,COD_STR_DATO2
                ,COD_DATO
                ,FLAG_KEY
                ,FLAG_RETURN_CODE
                ,FLAG_CALL_USING
                ,FLAG_WHERE_COND
                ,FLAG_REDEFINES
                ,TIPO_DATO
                ,LUNGHEZZA_DATO
                ,PRECISIONE_DATO
                ,COD_DOMINIO
                ,FORMATTAZIONE_DATO
                ,SERVIZIO_CONVERS
                ,AREA_CONV_STANDARD
                ,RICORRENZA
                ,OBBLIGATORIETA
                ,VALORE_DEFAULT
                ,DS_UTENTE
                 FROM COMP_STR_DATO
               WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
                 AND COD_STR_DATO       = :CSD-COD-STR-DATO
                 ORDER BY POSIZIONE
              END-EXEC
              CONTINUE

           WHEN 3
               EXEC SQL DECLARE CUR3 CURSOR FOR
                SELECT
                COD_COMPAGNIA_ANIA
                ,COD_STR_DATO
                ,POSIZIONE
                ,COD_STR_DATO2
                ,COD_DATO
                ,FLAG_KEY
                ,FLAG_RETURN_CODE
                ,FLAG_CALL_USING
                ,FLAG_WHERE_COND
                ,FLAG_REDEFINES
                ,TIPO_DATO
                ,LUNGHEZZA_DATO
                ,PRECISIONE_DATO
                ,COD_DOMINIO
                ,FORMATTAZIONE_DATO
                ,SERVIZIO_CONVERS
                ,AREA_CONV_STANDARD
                ,RICORRENZA
                ,OBBLIGATORIETA
                ,VALORE_DEFAULT
                ,DS_UTENTE
                 FROM COMP_STR_DATO
               WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
                 AND COD_STR_DATO       = :CSD-COD-STR-DATO
                 ORDER BY POSIZIONE
              END-EXEC
              CONTINUE

           WHEN 4
               EXEC SQL DECLARE CUR4 CURSOR FOR
                SELECT
                COD_COMPAGNIA_ANIA
                ,COD_STR_DATO
                ,POSIZIONE
                ,COD_STR_DATO2
                ,COD_DATO
                ,FLAG_KEY
                ,FLAG_RETURN_CODE
                ,FLAG_CALL_USING
                ,FLAG_WHERE_COND
                ,FLAG_REDEFINES
                ,TIPO_DATO
                ,LUNGHEZZA_DATO
                ,PRECISIONE_DATO
                ,COD_DOMINIO
                ,FORMATTAZIONE_DATO
                ,SERVIZIO_CONVERS
                ,AREA_CONV_STANDARD
                ,RICORRENZA
                ,OBBLIGATORIETA
                ,VALORE_DEFAULT
                ,DS_UTENTE
                 FROM COMP_STR_DATO
               WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
                 AND COD_STR_DATO       = :CSD-COD-STR-DATO
                 ORDER BY POSIZIONE
              END-EXEC
              CONTINUE

           WHEN 5
               EXEC SQL DECLARE CUR5 CURSOR FOR
                SELECT
                COD_COMPAGNIA_ANIA
                ,COD_STR_DATO
                ,POSIZIONE
                ,COD_STR_DATO2
                ,COD_DATO
                ,FLAG_KEY
                ,FLAG_RETURN_CODE
                ,FLAG_CALL_USING
                ,FLAG_WHERE_COND
                ,FLAG_REDEFINES
                ,TIPO_DATO
                ,LUNGHEZZA_DATO
                ,PRECISIONE_DATO
                ,COD_DOMINIO
                ,FORMATTAZIONE_DATO
                ,SERVIZIO_CONVERS
                ,AREA_CONV_STANDARD
                ,RICORRENZA
                ,OBBLIGATORIETA
                ,VALORE_DEFAULT
                ,DS_UTENTE
                 FROM COMP_STR_DATO
               WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
                 AND COD_STR_DATO       = :CSD-COD-STR-DATO
                 ORDER BY POSIZIONE
              END-EXEC
              CONTINUE

           WHEN 6
              EXEC SQL DECLARE CUR6 CURSOR FOR
                SELECT
                COD_COMPAGNIA_ANIA
                ,COD_STR_DATO
                ,POSIZIONE
                ,COD_STR_DATO2
                ,COD_DATO
                ,FLAG_KEY
                ,FLAG_RETURN_CODE
                ,FLAG_CALL_USING
                ,FLAG_WHERE_COND
                ,FLAG_REDEFINES
                ,TIPO_DATO
                ,LUNGHEZZA_DATO
                ,PRECISIONE_DATO
                ,COD_DOMINIO
                ,FORMATTAZIONE_DATO
                ,SERVIZIO_CONVERS
                ,AREA_CONV_STANDARD
                ,RICORRENZA
                ,OBBLIGATORIETA
                ,VALORE_DEFAULT
                ,DS_UTENTE
                 FROM COMP_STR_DATO
               WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
                 AND COD_STR_DATO       = :CSD-COD-STR-DATO
                 ORDER BY POSIZIONE
              END-EXEC
              CONTINUE

           WHEN 7
              EXEC SQL DECLARE CUR7 CURSOR FOR
                SELECT
                COD_COMPAGNIA_ANIA
                ,COD_STR_DATO
                ,POSIZIONE
                ,COD_STR_DATO2
                ,COD_DATO
                ,FLAG_KEY
                ,FLAG_RETURN_CODE
                ,FLAG_CALL_USING
                ,FLAG_WHERE_COND
                ,FLAG_REDEFINES
                ,TIPO_DATO
                ,LUNGHEZZA_DATO
                ,PRECISIONE_DATO
                ,COD_DOMINIO
                ,FORMATTAZIONE_DATO
                ,SERVIZIO_CONVERS
                ,AREA_CONV_STANDARD
                ,RICORRENZA
                ,OBBLIGATORIETA
                ,VALORE_DEFAULT
                ,DS_UTENTE
                 FROM COMP_STR_DATO
               WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
                 AND COD_STR_DATO       = :CSD-COD-STR-DATO
                 ORDER BY POSIZIONE
              END-EXEC
              CONTINUE

           WHEN 8
              EXEC SQL DECLARE CUR8 CURSOR FOR
                SELECT
                COD_COMPAGNIA_ANIA
                ,COD_STR_DATO
                ,POSIZIONE
                ,COD_STR_DATO2
                ,COD_DATO
                ,FLAG_KEY
                ,FLAG_RETURN_CODE
                ,FLAG_CALL_USING
                ,FLAG_WHERE_COND
                ,FLAG_REDEFINES
                ,TIPO_DATO
                ,LUNGHEZZA_DATO
                ,PRECISIONE_DATO
                ,COD_DOMINIO
                ,FORMATTAZIONE_DATO
                ,SERVIZIO_CONVERS
                ,AREA_CONV_STANDARD
                ,RICORRENZA
                ,OBBLIGATORIETA
                ,VALORE_DEFAULT
                ,DS_UTENTE
                 FROM COMP_STR_DATO
               WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
                 AND COD_STR_DATO       = :CSD-COD-STR-DATO
                 ORDER BY POSIZIONE
              END-EXEC
              CONTINUE

           WHEN 9
              EXEC SQL DECLARE CUR9 CURSOR FOR
                SELECT
                COD_COMPAGNIA_ANIA
                ,COD_STR_DATO
                ,POSIZIONE
                ,COD_STR_DATO2
                ,COD_DATO
                ,FLAG_KEY
                ,FLAG_RETURN_CODE
                ,FLAG_CALL_USING
                ,FLAG_WHERE_COND
                ,FLAG_REDEFINES
                ,TIPO_DATO
                ,LUNGHEZZA_DATO
                ,PRECISIONE_DATO
                ,COD_DOMINIO
                ,FORMATTAZIONE_DATO
                ,SERVIZIO_CONVERS
                ,AREA_CONV_STANDARD
                ,RICORRENZA
                ,OBBLIGATORIETA
                ,VALORE_DEFAULT
                ,DS_UTENTE
                 FROM COMP_STR_DATO
               WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
                 AND     COD_STR_DATO   = :CSD-COD-STR-DATO
                 ORDER BY POSIZIONE
              END-EXEC
              CONTINUE

           WHEN 10
              EXEC SQL DECLARE CUR10 CURSOR FOR
                SELECT
                COD_COMPAGNIA_ANIA
                ,COD_STR_DATO
                ,POSIZIONE
                ,COD_STR_DATO2
                ,COD_DATO
                ,FLAG_KEY
                ,FLAG_RETURN_CODE
                ,FLAG_CALL_USING
                ,FLAG_WHERE_COND
                ,FLAG_REDEFINES
                ,TIPO_DATO
                ,LUNGHEZZA_DATO
                ,PRECISIONE_DATO
                ,COD_DOMINIO
                ,FORMATTAZIONE_DATO
                ,SERVIZIO_CONVERS
                ,AREA_CONV_STANDARD
                ,RICORRENZA
                ,OBBLIGATORIETA
                ,VALORE_DEFAULT
                ,DS_UTENTE
                 FROM COMP_STR_DATO
               WHERE COD_COMPAGNIA_ANIA = :CSD-COD-COMPAGNIA-ANIA
                 AND COD_STR_DATO       = :CSD-COD-STR-DATO
                 ORDER BY POSIZIONE
              END-EXEC
              CONTINUE

           END-EVALUATE.

      *
       A100-EX.
           EXIT.
      *************************************************************************
       A200-OPEN SECTION.
      *
           EVALUATE WK-NUMERO-LIVELLO
           WHEN 1
              EXEC SQL OPEN CUR1  END-EXEC
              CONTINUE
           WHEN 2
              EXEC SQL OPEN CUR2  END-EXEC
              CONTINUE
           WHEN 3
              EXEC SQL OPEN CUR3  END-EXEC
              CONTINUE
           WHEN 4
              EXEC SQL OPEN CUR4  END-EXEC
              CONTINUE
           WHEN 5
              EXEC SQL OPEN CUR5  END-EXEC
              CONTINUE
           WHEN 6
              EXEC SQL OPEN CUR6  END-EXEC
              CONTINUE
           WHEN 7
              EXEC SQL OPEN CUR7  END-EXEC
              CONTINUE
           WHEN 8
              EXEC SQL OPEN CUR8  END-EXEC
              CONTINUE
           WHEN 9
              EXEC SQL OPEN CUR9  END-EXEC
              CONTINUE
           WHEN 10
              EXEC SQL OPEN CUR10 END-EXEC
              CONTINUE
           END-EVALUATE.

           MOVE SQLCODE         TO IDSO0021-SQLCODE
           MOVE DESCRIZ-ERR-DB2 TO IDSO0021-DESCRIZ-ERR-DB2

           IF NOT IDSO0021-SUCCESSFUL-SQL
              PERFORM 2000-DBERROR
           END-IF.

       A200-EX.
           EXIT.
      *************************************************************************
       A300-FETCH SECTION.
      *
           MOVE SPACES               TO COMP-STR-DATO

           EVALUATE WK-NUMERO-LIVELLO
           WHEN 1
              PERFORM UNTIL IDSO0021-NOT-FOUND
              OR NOT IDSO0021-SUCCESSFUL-RC
                 EXEC SQL FETCH CUR1
                 INTO
                 :CSD-COD-COMPAGNIA-ANIA
                 ,:CSD-COD-STR-DATO
                 ,:CSD-POSIZIONE
                 ,:CSD-COD-STR-DATO2
                  :IND-CSD-COD-STR-DATO2
                 ,:CSD-COD-DATO
                  :IND-CSD-COD-DATO
                 ,:CSD-FLAG-KEY
                  :IND-CSD-FLAG-KEY
                 ,:CSD-FLAG-RETURN-CODE
                  :IND-CSD-FLAG-RETURN-CODE
                 ,:CSD-FLAG-CALL-USING
                  :IND-CSD-FLAG-CALL-USING
                 ,:CSD-FLAG-WHERE-COND
                  :IND-CSD-FLAG-WHERE-COND
                 ,:CSD-FLAG-REDEFINES
                  :IND-CSD-FLAG-REDEFINES
                 ,:CSD-TIPO-DATO
                  :IND-CSD-TIPO-DATO
                 ,:CSD-LUNGHEZZA-DATO
                  :IND-CSD-LUNGHEZZA-DATO
                 ,:CSD-PRECISIONE-DATO
                  :IND-CSD-PRECISIONE-DATO
                 ,:CSD-COD-DOMINIO
                  :IND-CSD-COD-DOMINIO
                 ,:CSD-FORMATTAZIONE-DATO
                  :IND-CSD-FORMATTAZIONE-DATO
                 ,:CSD-SERVIZIO-CONVERS
                  :IND-CSD-SERVIZIO-CONVERS
                 ,:CSD-AREA-CONV-STANDARD
                  :IND-CSD-AREA-CONV-STANDARD
                 ,:CSD-RICORRENZA
                  :IND-CSD-RICORRENZA
                 ,:CSD-OBBLIGATORIETA
                  :IND-CSD-OBBLIGATORIETA
                 ,:CSD-VALORE-DEFAULT
                  :IND-CSD-VALORE-DEFAULT
                 ,:CSD-DS-UTENTE
                 END-EXEC

                 PERFORM C000-CONTROLLO-FIGLIO

              END-PERFORM

           WHEN 2
              PERFORM UNTIL IDSO0021-NOT-FOUND
              OR NOT IDSO0021-SUCCESSFUL-RC
                 EXEC SQL FETCH CUR2
                  INTO
                 :CSD-COD-COMPAGNIA-ANIA
                 ,:CSD-COD-STR-DATO
                 ,:CSD-POSIZIONE
                 ,:CSD-COD-STR-DATO2
                  :IND-CSD-COD-STR-DATO2
                 ,:CSD-COD-DATO
                  :IND-CSD-COD-DATO
                 ,:CSD-FLAG-KEY
                  :IND-CSD-FLAG-KEY
                 ,:CSD-FLAG-RETURN-CODE
                  :IND-CSD-FLAG-RETURN-CODE
                 ,:CSD-FLAG-CALL-USING
                  :IND-CSD-FLAG-CALL-USING
                 ,:CSD-FLAG-WHERE-COND
                  :IND-CSD-FLAG-WHERE-COND
                 ,:CSD-FLAG-REDEFINES
                  :IND-CSD-FLAG-REDEFINES
                 ,:CSD-TIPO-DATO
                  :IND-CSD-TIPO-DATO
                 ,:CSD-LUNGHEZZA-DATO
                  :IND-CSD-LUNGHEZZA-DATO
                 ,:CSD-PRECISIONE-DATO
                  :IND-CSD-PRECISIONE-DATO
                 ,:CSD-COD-DOMINIO
                  :IND-CSD-COD-DOMINIO
                 ,:CSD-FORMATTAZIONE-DATO
                  :IND-CSD-FORMATTAZIONE-DATO
                 ,:CSD-SERVIZIO-CONVERS
                  :IND-CSD-SERVIZIO-CONVERS
                 ,:CSD-AREA-CONV-STANDARD
                  :IND-CSD-AREA-CONV-STANDARD
                 ,:CSD-RICORRENZA
                  :IND-CSD-RICORRENZA
                 ,:CSD-OBBLIGATORIETA
                  :IND-CSD-OBBLIGATORIETA
                 ,:CSD-VALORE-DEFAULT
                  :IND-CSD-VALORE-DEFAULT
                 ,:CSD-DS-UTENTE
                 END-EXEC

                 PERFORM C000-CONTROLLO-FIGLIO

              END-PERFORM
           WHEN 3
              PERFORM UNTIL IDSO0021-NOT-FOUND
              OR NOT IDSO0021-SUCCESSFUL-RC
                 EXEC SQL FETCH CUR3
                  INTO
                 :CSD-COD-COMPAGNIA-ANIA
                 ,:CSD-COD-STR-DATO
                 ,:CSD-POSIZIONE
                 ,:CSD-COD-STR-DATO2
                  :IND-CSD-COD-STR-DATO2
                 ,:CSD-COD-DATO
                  :IND-CSD-COD-DATO
                 ,:CSD-FLAG-KEY
                  :IND-CSD-FLAG-KEY
                 ,:CSD-FLAG-RETURN-CODE
                  :IND-CSD-FLAG-RETURN-CODE
                 ,:CSD-FLAG-CALL-USING
                  :IND-CSD-FLAG-CALL-USING
                 ,:CSD-FLAG-WHERE-COND
                  :IND-CSD-FLAG-WHERE-COND
                 ,:CSD-FLAG-REDEFINES
                  :IND-CSD-FLAG-REDEFINES
                 ,:CSD-TIPO-DATO
                  :IND-CSD-TIPO-DATO
                 ,:CSD-LUNGHEZZA-DATO
                  :IND-CSD-LUNGHEZZA-DATO
                 ,:CSD-PRECISIONE-DATO
                  :IND-CSD-PRECISIONE-DATO
                 ,:CSD-COD-DOMINIO
                  :IND-CSD-COD-DOMINIO
                 ,:CSD-FORMATTAZIONE-DATO
                  :IND-CSD-FORMATTAZIONE-DATO
                 ,:CSD-SERVIZIO-CONVERS
                  :IND-CSD-SERVIZIO-CONVERS
                 ,:CSD-AREA-CONV-STANDARD
                  :IND-CSD-AREA-CONV-STANDARD
                 ,:CSD-RICORRENZA
                  :IND-CSD-RICORRENZA
                 ,:CSD-OBBLIGATORIETA
                  :IND-CSD-OBBLIGATORIETA
                 ,:CSD-VALORE-DEFAULT
                  :IND-CSD-VALORE-DEFAULT
                 ,:CSD-DS-UTENTE
                 END-EXEC

                 PERFORM C000-CONTROLLO-FIGLIO

              END-PERFORM
           WHEN 4
              PERFORM UNTIL IDSO0021-NOT-FOUND
              OR NOT IDSO0021-SUCCESSFUL-RC
                 EXEC SQL FETCH CUR4
                  INTO
                 :CSD-COD-COMPAGNIA-ANIA
                 ,:CSD-COD-STR-DATO
                 ,:CSD-POSIZIONE
                 ,:CSD-COD-STR-DATO2
                  :IND-CSD-COD-STR-DATO2
                 ,:CSD-COD-DATO
                  :IND-CSD-COD-DATO
                 ,:CSD-FLAG-KEY
                  :IND-CSD-FLAG-KEY
                 ,:CSD-FLAG-RETURN-CODE
                  :IND-CSD-FLAG-RETURN-CODE
                 ,:CSD-FLAG-CALL-USING
                  :IND-CSD-FLAG-CALL-USING
                 ,:CSD-FLAG-WHERE-COND
                  :IND-CSD-FLAG-WHERE-COND
                 ,:CSD-FLAG-REDEFINES
                  :IND-CSD-FLAG-REDEFINES
                 ,:CSD-TIPO-DATO
                  :IND-CSD-TIPO-DATO
                 ,:CSD-LUNGHEZZA-DATO
                  :IND-CSD-LUNGHEZZA-DATO
                 ,:CSD-PRECISIONE-DATO
                  :IND-CSD-PRECISIONE-DATO
                 ,:CSD-COD-DOMINIO
                  :IND-CSD-COD-DOMINIO
                 ,:CSD-FORMATTAZIONE-DATO
                  :IND-CSD-FORMATTAZIONE-DATO
                 ,:CSD-SERVIZIO-CONVERS
                  :IND-CSD-SERVIZIO-CONVERS
                 ,:CSD-AREA-CONV-STANDARD
                  :IND-CSD-AREA-CONV-STANDARD
                 ,:CSD-RICORRENZA
                  :IND-CSD-RICORRENZA
                 ,:CSD-OBBLIGATORIETA
                  :IND-CSD-OBBLIGATORIETA
                 ,:CSD-VALORE-DEFAULT
                  :IND-CSD-VALORE-DEFAULT
                 ,:CSD-DS-UTENTE
                 END-EXEC

                 PERFORM C000-CONTROLLO-FIGLIO

              END-PERFORM
           WHEN 5
              PERFORM UNTIL IDSO0021-NOT-FOUND
              OR NOT IDSO0021-SUCCESSFUL-RC
                 EXEC SQL FETCH CUR5
                  INTO
                 :CSD-COD-COMPAGNIA-ANIA
                 ,:CSD-COD-STR-DATO
                 ,:CSD-POSIZIONE
                 ,:CSD-COD-STR-DATO2
                  :IND-CSD-COD-STR-DATO2
                 ,:CSD-COD-DATO
                  :IND-CSD-COD-DATO
                 ,:CSD-FLAG-KEY
                  :IND-CSD-FLAG-KEY
                 ,:CSD-FLAG-RETURN-CODE
                  :IND-CSD-FLAG-RETURN-CODE
                 ,:CSD-FLAG-CALL-USING
                  :IND-CSD-FLAG-CALL-USING
                 ,:CSD-FLAG-WHERE-COND
                  :IND-CSD-FLAG-WHERE-COND
                 ,:CSD-FLAG-REDEFINES
                  :IND-CSD-FLAG-REDEFINES
                 ,:CSD-TIPO-DATO
                  :IND-CSD-TIPO-DATO
                 ,:CSD-LUNGHEZZA-DATO
                  :IND-CSD-LUNGHEZZA-DATO
                 ,:CSD-PRECISIONE-DATO
                  :IND-CSD-PRECISIONE-DATO
                 ,:CSD-COD-DOMINIO
                  :IND-CSD-COD-DOMINIO
                 ,:CSD-FORMATTAZIONE-DATO
                  :IND-CSD-FORMATTAZIONE-DATO
                 ,:CSD-SERVIZIO-CONVERS
                  :IND-CSD-SERVIZIO-CONVERS
                 ,:CSD-AREA-CONV-STANDARD
                  :IND-CSD-AREA-CONV-STANDARD
                 ,:CSD-RICORRENZA
                  :IND-CSD-RICORRENZA
                 ,:CSD-OBBLIGATORIETA
                  :IND-CSD-OBBLIGATORIETA
                 ,:CSD-VALORE-DEFAULT
                  :IND-CSD-VALORE-DEFAULT
                 ,:CSD-DS-UTENTE
                 END-EXEC

                 PERFORM C000-CONTROLLO-FIGLIO

              END-PERFORM
              WHEN 6
              PERFORM UNTIL IDSO0021-NOT-FOUND
              OR NOT IDSO0021-SUCCESSFUL-RC
                 EXEC SQL FETCH CUR6
                 INTO
                 :CSD-COD-COMPAGNIA-ANIA
                 ,:CSD-COD-STR-DATO
                 ,:CSD-POSIZIONE
                 ,:CSD-COD-STR-DATO2
                  :IND-CSD-COD-STR-DATO2
                 ,:CSD-COD-DATO
                  :IND-CSD-COD-DATO
                 ,:CSD-FLAG-KEY
                  :IND-CSD-FLAG-KEY
                 ,:CSD-FLAG-RETURN-CODE
                  :IND-CSD-FLAG-RETURN-CODE
                 ,:CSD-FLAG-CALL-USING
                  :IND-CSD-FLAG-CALL-USING
                 ,:CSD-FLAG-WHERE-COND
                  :IND-CSD-FLAG-WHERE-COND
                 ,:CSD-FLAG-REDEFINES
                  :IND-CSD-FLAG-REDEFINES
                 ,:CSD-TIPO-DATO
                  :IND-CSD-TIPO-DATO
                 ,:CSD-LUNGHEZZA-DATO
                  :IND-CSD-LUNGHEZZA-DATO
                 ,:CSD-PRECISIONE-DATO
                  :IND-CSD-PRECISIONE-DATO
                 ,:CSD-COD-DOMINIO
                  :IND-CSD-COD-DOMINIO
                 ,:CSD-FORMATTAZIONE-DATO
                  :IND-CSD-FORMATTAZIONE-DATO
                 ,:CSD-SERVIZIO-CONVERS
                  :IND-CSD-SERVIZIO-CONVERS
                 ,:CSD-AREA-CONV-STANDARD
                  :IND-CSD-AREA-CONV-STANDARD
                 ,:CSD-RICORRENZA
                  :IND-CSD-RICORRENZA
                 ,:CSD-OBBLIGATORIETA
                  :IND-CSD-OBBLIGATORIETA
                 ,:CSD-VALORE-DEFAULT
                  :IND-CSD-VALORE-DEFAULT
                 ,:CSD-DS-UTENTE
                 END-EXEC

                 PERFORM C000-CONTROLLO-FIGLIO

              END-PERFORM
              WHEN 7
              PERFORM UNTIL IDSO0021-NOT-FOUND
              OR NOT IDSO0021-SUCCESSFUL-RC
                 EXEC SQL FETCH CUR7
                 INTO
                 :CSD-COD-COMPAGNIA-ANIA
                 ,:CSD-COD-STR-DATO
                 ,:CSD-POSIZIONE
                 ,:CSD-COD-STR-DATO2
                  :IND-CSD-COD-STR-DATO2
                 ,:CSD-COD-DATO
                  :IND-CSD-COD-DATO
                 ,:CSD-FLAG-KEY
                  :IND-CSD-FLAG-KEY
                 ,:CSD-FLAG-RETURN-CODE
                  :IND-CSD-FLAG-RETURN-CODE
                 ,:CSD-FLAG-CALL-USING
                  :IND-CSD-FLAG-CALL-USING
                 ,:CSD-FLAG-WHERE-COND
                  :IND-CSD-FLAG-WHERE-COND
                 ,:CSD-FLAG-REDEFINES
                  :IND-CSD-FLAG-REDEFINES
                 ,:CSD-TIPO-DATO
                  :IND-CSD-TIPO-DATO
                 ,:CSD-LUNGHEZZA-DATO
                  :IND-CSD-LUNGHEZZA-DATO
                 ,:CSD-PRECISIONE-DATO
                  :IND-CSD-PRECISIONE-DATO
                 ,:CSD-COD-DOMINIO
                  :IND-CSD-COD-DOMINIO
                 ,:CSD-FORMATTAZIONE-DATO
                  :IND-CSD-FORMATTAZIONE-DATO
                 ,:CSD-SERVIZIO-CONVERS
                  :IND-CSD-SERVIZIO-CONVERS
                 ,:CSD-AREA-CONV-STANDARD
                  :IND-CSD-AREA-CONV-STANDARD
                 ,:CSD-RICORRENZA
                  :IND-CSD-RICORRENZA
                 ,:CSD-OBBLIGATORIETA
                  :IND-CSD-OBBLIGATORIETA
                 ,:CSD-VALORE-DEFAULT
                  :IND-CSD-VALORE-DEFAULT
                 ,:CSD-DS-UTENTE
                 END-EXEC

                 PERFORM C000-CONTROLLO-FIGLIO

              END-PERFORM
              WHEN 8
              PERFORM UNTIL IDSO0021-NOT-FOUND
              OR NOT IDSO0021-SUCCESSFUL-RC
                 EXEC SQL FETCH CUR8
                 INTO
                 :CSD-COD-COMPAGNIA-ANIA
                 ,:CSD-COD-STR-DATO
                 ,:CSD-POSIZIONE
                 ,:CSD-COD-STR-DATO2
                  :IND-CSD-COD-STR-DATO2
                 ,:CSD-COD-DATO
                  :IND-CSD-COD-DATO
                 ,:CSD-FLAG-KEY
                  :IND-CSD-FLAG-KEY
                 ,:CSD-FLAG-RETURN-CODE
                  :IND-CSD-FLAG-RETURN-CODE
                 ,:CSD-FLAG-CALL-USING
                  :IND-CSD-FLAG-CALL-USING
                 ,:CSD-FLAG-WHERE-COND
                  :IND-CSD-FLAG-WHERE-COND
                 ,:CSD-FLAG-REDEFINES
                  :IND-CSD-FLAG-REDEFINES
                 ,:CSD-TIPO-DATO
                  :IND-CSD-TIPO-DATO
                 ,:CSD-LUNGHEZZA-DATO
                  :IND-CSD-LUNGHEZZA-DATO
                 ,:CSD-PRECISIONE-DATO
                  :IND-CSD-PRECISIONE-DATO
                 ,:CSD-COD-DOMINIO
                  :IND-CSD-COD-DOMINIO
                 ,:CSD-FORMATTAZIONE-DATO
                  :IND-CSD-FORMATTAZIONE-DATO
                 ,:CSD-SERVIZIO-CONVERS
                  :IND-CSD-SERVIZIO-CONVERS
                 ,:CSD-AREA-CONV-STANDARD
                  :IND-CSD-AREA-CONV-STANDARD
                 ,:CSD-RICORRENZA
                  :IND-CSD-RICORRENZA
                 ,:CSD-OBBLIGATORIETA
                  :IND-CSD-OBBLIGATORIETA
                 ,:CSD-VALORE-DEFAULT
                  :IND-CSD-VALORE-DEFAULT
                 ,:CSD-DS-UTENTE
                 END-EXEC

                 PERFORM C000-CONTROLLO-FIGLIO

              END-PERFORM
              WHEN 9
              PERFORM UNTIL IDSO0021-NOT-FOUND
              OR NOT IDSO0021-SUCCESSFUL-RC
                 EXEC SQL FETCH CUR9
                 INTO
                 :CSD-COD-COMPAGNIA-ANIA
                 ,:CSD-COD-STR-DATO
                 ,:CSD-POSIZIONE
                 ,:CSD-COD-STR-DATO2
                  :IND-CSD-COD-STR-DATO2
                 ,:CSD-COD-DATO
                  :IND-CSD-COD-DATO
                 ,:CSD-FLAG-KEY
                  :IND-CSD-FLAG-KEY
                 ,:CSD-FLAG-RETURN-CODE
                  :IND-CSD-FLAG-RETURN-CODE
                 ,:CSD-FLAG-CALL-USING
                  :IND-CSD-FLAG-CALL-USING
                 ,:CSD-FLAG-WHERE-COND
                  :IND-CSD-FLAG-WHERE-COND
                 ,:CSD-FLAG-REDEFINES
                  :IND-CSD-FLAG-REDEFINES
                 ,:CSD-TIPO-DATO
                  :IND-CSD-TIPO-DATO
                 ,:CSD-LUNGHEZZA-DATO
                  :IND-CSD-LUNGHEZZA-DATO
                 ,:CSD-PRECISIONE-DATO
                  :IND-CSD-PRECISIONE-DATO
                 ,:CSD-COD-DOMINIO
                  :IND-CSD-COD-DOMINIO
                 ,:CSD-FORMATTAZIONE-DATO
                  :IND-CSD-FORMATTAZIONE-DATO
                 ,:CSD-SERVIZIO-CONVERS
                  :IND-CSD-SERVIZIO-CONVERS
                 ,:CSD-AREA-CONV-STANDARD
                  :IND-CSD-AREA-CONV-STANDARD
                 ,:CSD-RICORRENZA
                  :IND-CSD-RICORRENZA
                 ,:CSD-OBBLIGATORIETA
                  :IND-CSD-OBBLIGATORIETA
                 ,:CSD-VALORE-DEFAULT
                  :IND-CSD-VALORE-DEFAULT
                 ,:CSD-DS-UTENTE
                 END-EXEC

                 PERFORM C000-CONTROLLO-FIGLIO

              END-PERFORM
              WHEN 10
              PERFORM UNTIL IDSO0021-NOT-FOUND
              OR NOT IDSO0021-SUCCESSFUL-RC
                 EXEC SQL FETCH CUR10
                 INTO
                 :CSD-COD-COMPAGNIA-ANIA
                 ,:CSD-COD-STR-DATO
                 ,:CSD-POSIZIONE
                 ,:CSD-COD-STR-DATO2
                  :IND-CSD-COD-STR-DATO2
                 ,:CSD-COD-DATO
                  :IND-CSD-COD-DATO
                 ,:CSD-FLAG-KEY
                  :IND-CSD-FLAG-KEY
                 ,:CSD-FLAG-RETURN-CODE
                  :IND-CSD-FLAG-RETURN-CODE
                 ,:CSD-FLAG-CALL-USING
                  :IND-CSD-FLAG-CALL-USING
                 ,:CSD-FLAG-WHERE-COND
                  :IND-CSD-FLAG-WHERE-COND
                 ,:CSD-FLAG-REDEFINES
                  :IND-CSD-FLAG-REDEFINES
                 ,:CSD-TIPO-DATO
                  :IND-CSD-TIPO-DATO
                 ,:CSD-LUNGHEZZA-DATO
                  :IND-CSD-LUNGHEZZA-DATO
                 ,:CSD-PRECISIONE-DATO
                  :IND-CSD-PRECISIONE-DATO
                 ,:CSD-COD-DOMINIO
                  :IND-CSD-COD-DOMINIO
                 ,:CSD-FORMATTAZIONE-DATO
                  :IND-CSD-FORMATTAZIONE-DATO
                 ,:CSD-SERVIZIO-CONVERS
                  :IND-CSD-SERVIZIO-CONVERS
                 ,:CSD-AREA-CONV-STANDARD
                  :IND-CSD-AREA-CONV-STANDARD
                 ,:CSD-RICORRENZA
                  :IND-CSD-RICORRENZA
                 ,:CSD-OBBLIGATORIETA
                  :IND-CSD-OBBLIGATORIETA
                 ,:CSD-VALORE-DEFAULT
                  :IND-CSD-VALORE-DEFAULT
                 ,:CSD-DS-UTENTE
                 END-EXEC

                 PERFORM C000-CONTROLLO-FIGLIO

              END-PERFORM
           END-EVALUATE.

       A300-EX.
           EXIT.
      *************************************************************************
       A400-CLOSE SECTION.
      *
           EVALUATE WK-NUMERO-LIVELLO
           WHEN 1
              EXEC SQL CLOSE  CUR1  END-EXEC
              CONTINUE
           WHEN 2
              EXEC SQL CLOSE  CUR2  END-EXEC
              CONTINUE
           WHEN 3
              EXEC SQL CLOSE  CUR3  END-EXEC
              CONTINUE
           WHEN 4
              EXEC SQL CLOSE  CUR4  END-EXEC
              CONTINUE
           WHEN 5
              EXEC SQL CLOSE  CUR5  END-EXEC
              CONTINUE
           WHEN 6
              EXEC SQL CLOSE  CUR6  END-EXEC
              CONTINUE
           WHEN 7
              EXEC SQL CLOSE  CUR7  END-EXEC
              CONTINUE
           WHEN 8
              EXEC SQL CLOSE  CUR8  END-EXEC
              CONTINUE
           WHEN 9
              EXEC SQL CLOSE  CUR9  END-EXEC
              CONTINUE
           WHEN 10
              EXEC SQL CLOSE  CUR10 END-EXEC
              CONTINUE
           END-EVALUATE.

           IF SQLCODE NOT = ZEROES
              MOVE SQLCODE         TO IDSO0021-SQLCODE
              MOVE DESCRIZ-ERR-DB2 TO IDSO0021-DESCRIZ-ERR-DB2
              PERFORM 2000-DBERROR
           END-IF.

       A400-EX.
           EXIT.
      *************************************************************************
       A500-OPERAZIONI-FINALI SECTION.
      *
           SUBTRACT 1 FROM WK-NUMERO-LIVELLO.

           PERFORM C300-CTRL-STRUTTURA-PADRE.
      *
       A500-EX.
           EXIT.

      *************************************************************************
       C000-CONTROLLO-FIGLIO SECTION.
      *
           MOVE SQLCODE         TO IDSO0021-SQLCODE
           MOVE DESCRIZ-ERR-DB2 TO IDSO0021-DESCRIZ-ERR-DB2

           IF  NOT IDSO0021-SUCCESSFUL-SQL
           AND NOT IDSO0021-NOT-FOUND
              PERFORM 2000-DBERROR
              PERFORM A400-CLOSE
           END-IF.

           IF IDSO0021-SUCCESSFUL-RC AND
              IDSO0021-SUCCESSFUL-SQL

              MOVE 'S'                         TO WK-COUNTER
              PERFORM N000-CNTL-CAMPI-NULL-CSD
              PERFORM C050-VALORIZZA-BASE
              IF CSD-COD-STR-DATO2 =
                 SPACES OR LOW-VALUES OR HIGH-VALUES
                 IF CSD-COD-DATO  = SPACES OR LOW-VALUES OR HIGH-VALUES
                    SET IDSO0021-FIELD-NOT-VALUED TO TRUE
                    MOVE 'CODICE DATO NON VALORIZZATO'
                                             TO IDSO0021-DESCRIZ-ERR-DB2
                 ELSE
                    PERFORM C100-CONTROLLO-COMPOSIZIONE
                 END-IF
              ELSE

                 PERFORM C150-COMPOSIZIONE-PADRE

                 IF CSD-LUNGHEZZA-DATO IS NUMERIC AND
                    CSD-LUNGHEZZA-DATO NOT = 0
                    PERFORM C130-VALORIZZA-DATO-COMPLETO
                 ELSE

                    PERFORM R300-CNTL-RICORRENZA
                    PERFORM R000-RICORSIONE

                    IF CAMPI-REDEFINES
                       IF WK-NUMERO-RICORRENZE-RED
                                     (WK-NUMERO-LIVELLO) > 0
                          MOVE 0  TO WK-NUMERO-RICORRENZE-RED
                                       (WK-NUMERO-LIVELLO)
                                     WK-NUMERO-RICORRENZA
                                     WK-INIZIO-POSIZIONE-RIC
                       END-IF

                       MOVE WK-INDICE-PADRE-RED(WK-NUMERO-LIVELLO)
                            TO COMODO-INDICE-PADRE

                       MOVE WK-LUNG-PADRE-RED  (WK-NUMERO-LIVELLO)
                                            TO IDSO0021-LUNGHEZZA-DATO
                                               (COMODO-INDICE-PADRE)

                       MOVE 0 TO WK-LUNG-PADRE-RED(WK-NUMERO-LIVELLO)
                       IF WK-NUMERO-LIVELLO =  WK-NUMERO-LIVELLO-RED
                           SET CAMPI-NON-REDEFINES TO TRUE
                       END-IF
                    ELSE
                       IF WK-NUMERO-RICORRENZE(WK-NUMERO-LIVELLO) > 0

                          ADD  WK-INIZIO-POSIZIONE-RIC
                                                TO WK-INIZIO-POSIZIONE
                          MOVE 0
                            TO WK-NUMERO-RICORRENZE (WK-NUMERO-LIVELLO)
                               WK-NUMERO-RICORRENZA
                               WK-INIZIO-POSIZIONE-RIC
                       END-IF

                       MOVE WK-INDICE-PADRE(WK-NUMERO-LIVELLO)
                            TO COMODO-INDICE-PADRE

                       MOVE WK-LUNG-PADRE(WK-NUMERO-LIVELLO)
                            TO IDSO0021-LUNGHEZZA-DATO
                         (COMODO-INDICE-PADRE)
                       MOVE 0  TO WK-LUNG-PADRE(WK-NUMERO-LIVELLO)
                    END-IF
                 END-IF
              END-IF

              IF WK-COUNTER = 'N'
                 MOVE 'NESSUN CAMPO DI STRUTTURA TROVATO'
                             TO IDSO0021-DESCRIZ-ERR-DB2
                 PERFORM 2000-DBERROR
              END-IF

              IF IDSO0021-SUCCESSFUL-RC AND
                 IDSO0021-SUCCESSFUL-SQL
                 PERFORM R600-CNTL-REDEFINES
                 MOVE SPACES              TO COMP-STR-DATO
              END-IF
           END-IF.

       C000-EX.
           EXIT.
      *************************************************************************
       C050-VALORIZZA-BASE SECTION.
      *
           MOVE SPACES                 TO WK-CTRL-LUNG-CAMPO-TIPO
           MOVE 0                      TO WK-CTRL-LUNG-CAMPO-LUNG

           ADD 1                       TO INDICE-ANAGR-DATO
           MOVE CSD-FLAG-KEY           TO IDSO0021-FLAG-KEY
                                          (INDICE-ANAGR-DATO)
           MOVE CSD-FLAG-RETURN-CODE
                                       TO IDSO0021-FLAG-RETURN-CODE
                                          (INDICE-ANAGR-DATO)
           MOVE CSD-FLAG-CALL-USING    TO IDSO0021-FLAG-CALL-USING
                                          (INDICE-ANAGR-DATO)
           MOVE CSD-FLAG-WHERE-COND    TO IDSO0021-FLAG-WHERE-COND
                                          (INDICE-ANAGR-DATO)
           MOVE CSD-FLAG-REDEFINES     TO IDSO0021-FLAG-REDEFINES
                                          (INDICE-ANAGR-DATO)

           MOVE CSD-SERVIZIO-CONVERS   TO IDSO0021-SERV-CONVERSIONE
                                          (INDICE-ANAGR-DATO)
           MOVE CSD-AREA-CONV-STANDARD TO IDSO0021-AREA-CONV-STANDARD
                                          (INDICE-ANAGR-DATO)

           IF CSD-RICORRENZA-NULL NOT = HIGH-VALUE
              MOVE CSD-RICORRENZA      TO IDSO0021-RICORRENZA
                                          (INDICE-ANAGR-DATO)
           END-IF

           IF WK-NUMERO-RICORRENZA > 0
              MOVE WK-CAMPO-ATTIVO     TO IDSO0021-CLONE
                                          (INDICE-ANAGR-DATO)
           END-IF

           MOVE CSD-OBBLIGATORIETA     TO IDSO0021-OBBLIGATORIETA
                                          (INDICE-ANAGR-DATO)
           MOVE CSD-VALORE-DEFAULT     TO IDSO0021-VALORE-DEFAULT
                                          (INDICE-ANAGR-DATO).
      *
       C050-EX.
           EXIT.
      *************************************************************************
       C100-CONTROLLO-COMPOSIZIONE SECTION.
      *
           IF CSD-TIPO-DATO  = SPACES OR LOW-VALUES OR HIGH-VALUES
              MOVE CSD-COD-DATO              TO ADA-COD-DATO
              MOVE IDSI0021-CODICE-COMPAGNIA-ANIA
                                             TO ADA-COD-COMPAGNIA-ANIA
              PERFORM R100-ACCEDI-ANAGR-DATO

              IF IDSO0021-SUCCESSFUL-RC
                 MOVE ADA-COD-DATO           TO IDSO0021-CODICE-DATO
                                            (INDICE-ANAGR-DATO)

                 IF CAMPI-REDEFINES
                    MOVE WK-INIZIO-POSIZIONE-RED
                         TO IDSO0021-INIZIO-POSIZIONE
                                            (INDICE-ANAGR-DATO)
                 ELSE
                    MOVE WK-INIZIO-POSIZIONE
                         TO IDSO0021-INIZIO-POSIZIONE
                                            (INDICE-ANAGR-DATO)
                 END-IF

                 MOVE ADA-TIPO-DATO       TO IDSO0021-TIPO-DATO
                                             (INDICE-ANAGR-DATO)
                                             WK-CTRL-LUNG-CAMPO-TIPO

                 MOVE ADA-LUNGHEZZA-DATO  TO WK-CTRL-LUNG-CAMPO-LUNG
                                             IDSO0021-LUNGHEZZA-DATO-NOM
                                             (INDICE-ANAGR-DATO)


                 IF WK-CTRL-LUNG-CAMPO-TIPO =
                    WK-CAMPO-TIPO-COMP  OR WK-CAMPO-TIPO-COMP1 OR
                    WK-CAMPO-TIPO-COMP2 OR WK-CAMPO-TIPO-COMP3 OR
                    WK-CAMPO-TIPO-COMP4 OR WK-CAMPO-TIPO-COMP5

                    PERFORM R200-CTRL-LUNGHEZZA-CAMPO

                 END-IF

                 MOVE WK-CTRL-LUNG-CAMPO-LUNG
                                          TO IDSO0021-LUNGHEZZA-DATO
                                             (INDICE-ANAGR-DATO)

                 PERFORM C200-INCREMENTA-LUNGHEZZA

                 IF CAMPI-REDEFINES
                    ADD  WK-CTRL-LUNG-CAMPO-LUNG
                                          TO WK-INIZIO-POSIZIONE-RED
                 ELSE
                    MOVE WK-INIZIO-POSIZIONE
                                          TO WK-INIZIO-POSIZIONE-RED
                    ADD  WK-CTRL-LUNG-CAMPO-LUNG
                                          TO WK-INIZIO-POSIZIONE
                 END-IF

                 MOVE ADA-PRECISIONE-DATO TO IDSO0021-PRECISIONE-DATO
                                             (INDICE-ANAGR-DATO)
                 MOVE ADA-FORMATTAZIONE-DATO
                                          TO IDSO0021-FORMATTAZIONE-DATO
                                             (INDICE-ANAGR-DATO)
                 MOVE ADA-COD-DOMINIO     TO IDSO0021-CODICE-DOMINIO
                                             (INDICE-ANAGR-DATO)
              END-IF

           ELSE

              PERFORM C130-VALORIZZA-DATO-COMPLETO

           END-IF.

       C100-EX.
           EXIT.
      *************************************************************************
       C130-VALORIZZA-DATO-COMPLETO SECTION.
      *
              IF CSD-COD-STR-DATO2 =
                 SPACES OR LOW-VALUES OR HIGH-VALUES
                 MOVE CSD-COD-DATO        TO IDSO0021-CODICE-DATO
                                            (INDICE-ANAGR-DATO)
              ELSE
                 MOVE CSD-COD-STR-DATO2   TO IDSO0021-CODICE-DATO
                                             (INDICE-ANAGR-DATO)
              END-IF

              MOVE CSD-TIPO-DATO          TO IDSO0021-TIPO-DATO
                                             (INDICE-ANAGR-DATO)
                                             WK-CTRL-LUNG-CAMPO-TIPO

              IF CAMPI-REDEFINES
                 MOVE WK-INIZIO-POSIZIONE-RED
                   TO IDSO0021-INIZIO-POSIZIONE(INDICE-ANAGR-DATO)
              ELSE
                 MOVE WK-INIZIO-POSIZIONE
                   TO IDSO0021-INIZIO-POSIZIONE(INDICE-ANAGR-DATO)
              END-IF

              MOVE CSD-LUNGHEZZA-DATO
                   TO WK-CTRL-LUNG-CAMPO-LUNG
                      IDSO0021-LUNGHEZZA-DATO-NOM
                       (INDICE-ANAGR-DATO)

              IF WK-CTRL-LUNG-CAMPO-TIPO =
                 WK-CAMPO-TIPO-COMP  OR WK-CAMPO-TIPO-COMP1 OR
                 WK-CAMPO-TIPO-COMP2 OR WK-CAMPO-TIPO-COMP3 OR
                 WK-CAMPO-TIPO-COMP4 OR WK-CAMPO-TIPO-COMP5

                 PERFORM R200-CTRL-LUNGHEZZA-CAMPO

              END-IF

              MOVE WK-CTRL-LUNG-CAMPO-LUNG
                   TO IDSO0021-LUNGHEZZA-DATO
                       (INDICE-ANAGR-DATO)

              PERFORM C200-INCREMENTA-LUNGHEZZA

              IF CAMPI-REDEFINES
                 ADD WK-CTRL-LUNG-CAMPO-LUNG TO WK-INIZIO-POSIZIONE-RED
              ELSE
                  MOVE WK-INIZIO-POSIZIONE   TO WK-INIZIO-POSIZIONE-RED
                 ADD WK-CTRL-LUNG-CAMPO-LUNG TO WK-INIZIO-POSIZIONE
              END-IF

              IF CSD-PRECISIONE-DATO-NULL NOT = HIGH-VALUES
                 MOVE CSD-PRECISIONE-DATO
                      TO IDSO0021-PRECISIONE-DATO(INDICE-ANAGR-DATO)
              END-IF.


      *
       C130-EX.
           EXIT.

      *************************************************************************
       C150-COMPOSIZIONE-PADRE SECTION.
      *
           MOVE CSD-COD-STR-DATO2
                   TO IDSO0021-CODICE-DATO(INDICE-ANAGR-DATO)
           IF CAMPI-REDEFINES
              MOVE WK-INIZIO-POSIZIONE-RED
                   TO IDSO0021-INIZIO-POSIZIONE(INDICE-ANAGR-DATO)
              MOVE HIGH-VALUES
                   TO IDSO0021-TIPO-DATO(INDICE-ANAGR-DATO)
                      WK-CTRL-LUNG-CAMPO-TIPO
           ELSE
              MOVE WK-INIZIO-POSIZIONE
                   TO IDSO0021-INIZIO-POSIZIONE(INDICE-ANAGR-DATO)
              MOVE CSD-TIPO-DATO
                   TO IDSO0021-TIPO-DATO(INDICE-ANAGR-DATO)
                      WK-CTRL-LUNG-CAMPO-TIPO
           END-IF.
      *
       C150-EX.
           EXIT.
      *************************************************************************
       C180-COMPOSIZIONE-RDS SECTION.
      *
           MOVE CSD-COD-COMPAGNIA-ANIA  TO RDS-COD-COMPAGNIA-ANIA
           MOVE IDSI0021-TIPO-MOVIMENTO TO RDS-TIPO-MOVIMENTO
           MOVE CSD-COD-DATO            TO RDS-COD-DATO.
      *
       C180-EX.
           EXIT.
      *************************************************************************
       C200-INCREMENTA-LUNGHEZZA SECTION.
      *

           IF WK-NUMERO-RICORRENZA > 0
              MOVE 0   TO WK-CTRL-LUNG-CAMPO-LUNG-EFF
                          WK-INIZIO-POSIZIONE-MOL
              COMPUTE WK-CTRL-LUNG-CAMPO-LUNG-EFF  =
                      WK-CTRL-LUNG-CAMPO-LUNG *
                      WK-NUMERO-RICORRENZA
              COMPUTE WK-INIZIO-POSIZIONE-MOL =
                      WK-CTRL-LUNG-CAMPO-LUNG *
                     (WK-NUMERO-RICORRENZA - 1)
              ADD     WK-INIZIO-POSIZIONE-MOL
                      TO WK-INIZIO-POSIZIONE-RIC
           ELSE
              MOVE WK-CTRL-LUNG-CAMPO-LUNG
                      TO WK-CTRL-LUNG-CAMPO-LUNG-EFF
           END-IF

           PERFORM VARYING INDEX-PADRE FROM 1 BY 1
                   UNTIL   INDEX-PADRE > WK-LIMITE-LIVELLI
                   OR      INDEX-PADRE >= WK-NUMERO-LIVELLO

                   IF CAMPI-REDEFINES
                      ADD WK-CTRL-LUNG-CAMPO-LUNG-EFF
                            TO WK-LUNG-PADRE-RED(INDEX-PADRE)
                   ELSE

                      ADD WK-CTRL-LUNG-CAMPO-LUNG-EFF
                          TO WK-LUNG-PADRE(INDEX-PADRE)
                   END-IF

           END-PERFORM.

       C200-EX.
           EXIT.

      *************************************************************************
       C300-CTRL-STRUTTURA-PADRE SECTION.

           IF IDSI0021-PTF-NEWLIFE AND
              WK-NUMERO-LIVELLO < WK-LIVELLO-PADRE
              PERFORM C330-RICERCA-FLAG-CALL-USING
           END-IF.

           MOVE INDICE-ANAGR-DATO    TO IDSO0021-NUM-ELEM.
      *
       C300-EXIT.
           EXIT.
      *************************************************************************
      * RICERCA DI FLAG-CALL-USING
      *************************************************************************
       C330-RICERCA-FLAG-CALL-USING SECTION.
      *
           SET IDSO0021-IND               TO 1
           SEARCH  IDSO0021-ELEMENTS-STR-DATO

             WHEN IDSO0021-FLAG-CALL-USING(IDSO0021-IND)
                           = WK-CAMPO-ATTIVO
                  PERFORM C350-SCHIACCIA-STRUTTURA
           END-SEARCH.
      *
       C330-EX.
           EXIT.
      *************************************************************************
      *
      * SCHIACCIAMTO DELLE STRUTTURE MICRO (CAMPI DELLE STRUTTURE)
      * LASCIANDO SOLO QUELLE MACRO (ES. POLI, MOVI ....)
      *
      *************************************************************************
       C350-SCHIACCIA-STRUTTURA SECTION.
      *
           MOVE 0                    TO INDICE-ANAGR-DATO
           SET SCR-IND               TO 1
           MOVE IDSO0021-STRUTTURA-DATO
                                     TO AREA-SCREMATURA
           MOVE SPACES               TO IDSO0021-STRUTTURA-DATO
           PERFORM VARYING SCR-IND FROM 1 BY 1
                   UNTIL   SCR-IND > WK-LIMITE-STR-DATO
                   OR      SCR-CODICE-DATO(SCR-IND) =
                           SPACES OR LOW-VALUES OR HIGH-VALUES

                   IF SCR-FLAG-CALL-USING(SCR-IND) =
                                         WK-CAMPO-ATTIVO
                      ADD 1           TO INDICE-ANAGR-DATO
                      MOVE SCR-ELEMENTS-STR-DATO(SCR-IND)
                                      TO IDSO0021-ELEMENTS-STR-DATO
                                                (INDICE-ANAGR-DATO)
                   END-IF
           END-PERFORM.
      *
       C350-EX.
           EXIT.
      *************************************************************************
       N000-CNTL-CAMPI-NULL-CSD SECTION.
      *
           IF IND-CSD-COD-STR-DATO2 = -1
              MOVE HIGH-VALUES TO CSD-COD-STR-DATO2
           END-IF
           IF IND-CSD-COD-DATO = -1
              MOVE HIGH-VALUES TO CSD-COD-DATO
           END-IF
           IF IND-CSD-FLAG-KEY = -1
              MOVE HIGH-VALUES TO CSD-FLAG-KEY-NULL
           END-IF
           IF IND-CSD-FLAG-RETURN-CODE = -1
              MOVE HIGH-VALUES TO CSD-FLAG-RETURN-CODE-NULL
           END-IF
           IF IND-CSD-FLAG-CALL-USING = -1
              MOVE HIGH-VALUES TO CSD-FLAG-CALL-USING-NULL
           END-IF
           IF IND-CSD-FLAG-WHERE-COND = -1
              MOVE HIGH-VALUES TO CSD-FLAG-WHERE-COND-NULL
           END-IF
           IF IND-CSD-FLAG-REDEFINES = -1
              MOVE HIGH-VALUES TO CSD-FLAG-REDEFINES-NULL
           END-IF
           IF IND-CSD-TIPO-DATO = -1
              MOVE HIGH-VALUES TO CSD-TIPO-DATO-NULL
           END-IF
           IF IND-CSD-LUNGHEZZA-DATO = -1
              MOVE HIGH-VALUES TO CSD-LUNGHEZZA-DATO-NULL
           END-IF
           IF IND-CSD-PRECISIONE-DATO = -1
              MOVE HIGH-VALUES TO CSD-PRECISIONE-DATO-NULL
           END-IF
           IF IND-CSD-COD-DOMINIO = -1
              MOVE HIGH-VALUES TO CSD-COD-DOMINIO
           END-IF
           IF IND-CSD-FORMATTAZIONE-DATO = -1
              MOVE HIGH-VALUES TO CSD-FORMATTAZIONE-DATO
           END-IF
           IF IND-CSD-SERVIZIO-CONVERS = -1
              MOVE HIGH-VALUES TO CSD-SERVIZIO-CONVERS-NULL
           END-IF
           IF IND-CSD-AREA-CONV-STANDARD = -1
              MOVE HIGH-VALUES TO CSD-AREA-CONV-STANDARD-NULL
           END-IF
           IF IND-CSD-RICORRENZA = -1
              MOVE HIGH-VALUES TO CSD-RICORRENZA-NULL
           END-IF
           IF IND-CSD-OBBLIGATORIETA = -1
              MOVE HIGH-VALUES TO CSD-OBBLIGATORIETA-NULL
           END-IF
           IF IND-CSD-VALORE-DEFAULT = -1
              MOVE HIGH-VALUES TO CSD-VALORE-DEFAULT
           END-IF.
      *
       N000-EX.
           EXIT.
      *************************************************************************
       N100-CNTL-CAMPI-NULL-ADA SECTION.
      *
           IF IND-ADA-DESC-DATO = -1
              MOVE HIGH-VALUES TO ADA-DESC-DATO
           END-IF
           IF IND-ADA-TIPO-DATO = -1
              MOVE HIGH-VALUES TO ADA-TIPO-DATO-NULL
           END-IF
           IF IND-ADA-LUNGHEZZA-DATO = -1
              MOVE HIGH-VALUES TO ADA-LUNGHEZZA-DATO-NULL
           END-IF
           IF IND-ADA-PRECISIONE-DATO = -1
              MOVE HIGH-VALUES TO ADA-PRECISIONE-DATO-NULL
           END-IF
           IF IND-ADA-COD-DOMINIO = -1
              MOVE HIGH-VALUES TO ADA-COD-DOMINIO
           END-IF
           IF IND-ADA-FORMATTAZIONE-DATO = -1
              MOVE HIGH-VALUES TO ADA-FORMATTAZIONE-DATO
           END-IF.
      *
       N100-EX.
           EXIT.
      *************************************************************************
        R000-RICORSIONE SECTION.
      *
           MOVE CSD-COD-STR-DATO2
                                     TO IDSI0021-CODICE-STR-DATO
           IF CAMPI-REDEFINES
              MOVE INDICE-ANAGR-DATO
                               TO WK-INDICE-PADRE-RED
                                 (WK-NUMERO-LIVELLO)
           ELSE
              MOVE INDICE-ANAGR-DATO
                               TO WK-INDICE-PADRE
                                (WK-NUMERO-LIVELLO)
           END-IF

           MOVE SPACES               TO AREA-CALL
           MOVE INPUT-IDSS0020       TO AREA-CALL.
           CALL PGM-NAME-IDSS0020 USING AREA-CALL.
ALEX  *     MOVE SPACES               TO OUTPUT-IDSS0020
           MOVE AREA-CALL            TO OUTPUT-IDSS0020

           IF IDSO0021-RETURN-CODE NOT = '00'
              GOBACK
           END-IF.

       R000-EX.
           EXIT.
      *************************************************************************
       R100-ACCEDI-ANAGR-DATO SECTION.
      *

      *--> TIPO OPERAZIONE
           SET IDSV0003-SELECT            TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

           CALL PGM-NAME-IDSS0080               USING IDSV0003
                                                 ANAG-DATO

           IF IDSV0003-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSV0003-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                     PERFORM N100-CNTL-CAMPI-NULL-ADA

                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB

                       MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                       STRING 'ANAG_DATO'
                              ' - '
                              'RC : '
                              IDSV0003-RETURN-CODE
                              'SQLCODE : '
                              WK-SQLCODE
                              DELIMITED BY SIZE INTO
                              IDSO0021-DESCRIZ-ERR-DB2
                       END-STRING

                   PERFORM 2000-DBERROR
                   MOVE 'ANAG_DATO' TO IDSO0021-NOME-TABELLA

              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER

              STRING 'ERRORE CHIAMATA MODULO '
                      PGM-NAME-IDSS0080
                      ' - '
                      'RC : '
                      IDSV0003-RETURN-CODE
                      DELIMITED BY SIZE INTO
                      IDSO0021-DESCRIZ-ERR-DB2
              END-STRING

              PERFORM 2000-DBERROR
              MOVE 'ANAG_DATO' TO IDSO0021-NOME-TABELLA

           END-IF.

       R100-EX.
           EXIT.
      *************************************************************************
       R200-CTRL-LUNGHEZZA-CAMPO SECTION.
      *
           IF WK-CTRL-LUNG-CAMPO-TIPO =
                WK-CAMPO-TIPO-COMP

                EVALUATE WK-CTRL-LUNG-CAMPO-LUNG
                  WHEN 1 THRU 4
                     MOVE 2      TO WK-CTRL-LUNG-CAMPO-LUNG
                  WHEN 5 THRU 9
                     MOVE 4      TO WK-CTRL-LUNG-CAMPO-LUNG
                  WHEN 10 THRU 18
                     MOVE 8      TO WK-CTRL-LUNG-CAMPO-LUNG
                END-EVALUATE

           END-IF

           IF WK-CTRL-LUNG-CAMPO-TIPO =
              WK-CAMPO-TIPO-COMP3
               COMPUTE WK-CTRL-LUNG-CAMPO-LUNG =
                       WK-CTRL-LUNG-CAMPO-LUNG / 2
               COMPUTE WK-CTRL-LUNG-CAMPO-LUNG =
                       WK-CTRL-LUNG-CAMPO-LUNG + 1
           END-IF.

       R200-EX.
           EXIT.
      *************************************************************************
       R300-CNTL-RICORRENZA SECTION.

           IF CSD-RICORRENZA IS NUMERIC AND
              CSD-RICORRENZA > 0
              IF CAMPI-REDEFINES
                 MOVE CSD-RICORRENZA
                      TO WK-NUMERO-RICORRENZE-RED(WK-NUMERO-LIVELLO)
                         WK-NUMERO-RICORRENZA
              ELSE
                 MOVE CSD-RICORRENZA
                      TO WK-NUMERO-RICORRENZE    (WK-NUMERO-LIVELLO)
                         WK-NUMERO-RICORRENZA
              END-IF
           END-IF.

       R300-EX.
           EXIT.
      *************************************************************************
       R500-ACCEDI-SU-RDS SECTION.
      *
           EXEC SQL
              SELECT
                  COD_STR_DATO
              INTO
                  :RDS-COD-DATO
              FROM RIDEF_DATO_STR
              WHERE COD_COMPAGNIA_ANIA = :RDS-COD-COMPAGNIA-ANIA
              AND   TIPO_MOVIMENTO     = :RDS-TIPO-MOVIMENTO
              AND   COD_DATO           = :RDS-COD-DATO
           END-EXEC.

           MOVE SQLCODE         TO IDSO0021-SQLCODE
           MOVE DESCRIZ-ERR-DB2 TO IDSO0021-DESCRIZ-ERR-DB2.

      *
       R500-EX.
           EXIT.
      *************************************************************************
       R600-CNTL-REDEFINES SECTION.
      *
           IF CSD-FLAG-REDEFINES = WK-CAMPO-ATTIVO
              PERFORM C180-COMPOSIZIONE-RDS
              PERFORM R500-ACCEDI-SU-RDS

              IF NOT IDSO0021-SUCCESSFUL-SQL
                 IF IDSO0021-NOT-FOUND
                    MOVE 99999  TO RDS-TIPO-MOVIMENTO
                    SET IDSO0021-SUCCESSFUL-RC  TO TRUE
                    SET IDSO0021-SUCCESSFUL-SQL TO TRUE
                    PERFORM R500-ACCEDI-SU-RDS
                 ELSE
                    PERFORM 2000-DBERROR
                    MOVE 'RIDEF_DATO_STR'
                                   TO IDSO0021-NOME-TABELLA
                 END-IF
              END-IF

              IF IDSO0021-SUCCESSFUL-SQL
                 MOVE WK-NUMERO-LIVELLO TO WK-NUMERO-LIVELLO-RED
                 SET CAMPI-REDEFINES    TO TRUE

                 MOVE RDS-COD-STR-DATO  TO CSD-COD-STR-DATO2
                 MOVE 0                 TO IND-CSD-COD-STR-DATO2
                 PERFORM C000-CONTROLLO-FIGLIO
              ELSE
                 IF IDSO0021-NOT-FOUND
                    SET IDSO0021-SUCCESSFUL-RC  TO TRUE
                    SET IDSO0021-SUCCESSFUL-SQL TO TRUE
                 ELSE
                    PERFORM 2000-DBERROR
                    MOVE 'RIDEF_DATO_STR' TO IDSO0021-NOME-TABELLA
                 END-IF
              END-IF
           END-IF.

      *
       R600-EX.
           EXIT.
      *************************************************************************
       S000-ELABORA-SST SECTION.
      *
           PERFORM S210-VALORIZZA-CAMPI
      *     PERFORM S200-ACCEDI-SU-SST

           IF IDSO0021-SUCCESSFUL-RC AND
              IND-SINONIM = 0
              PERFORM S210-VALORIZZA-CAMPI
              MOVE '*'              TO  SST-OPERAZIONE
              PERFORM S200-ACCEDI-SU-SST

              IF IDSO0021-SUCCESSFUL-RC AND
                 IND-SINONIM = 0
                 PERFORM S210-VALORIZZA-CAMPI
                 MOVE '*'            TO  SST-FLAG-MOD-ESECUTIVA
                 PERFORM S200-ACCEDI-SU-SST

                 IF IDSO0021-SUCCESSFUL-RC AND
                    IND-SINONIM = 0
                    PERFORM S210-VALORIZZA-CAMPI
                    MOVE '*'         TO  SST-OPERAZIONE
                    MOVE '*'         TO  SST-FLAG-MOD-ESECUTIVA
                    PERFORM S200-ACCEDI-SU-SST

                 END-IF
              END-IF
           END-IF.

       S000-EX.
           EXIT.
      *************************************************************************
       S210-VALORIZZA-CAMPI SECTION.
      *
           MOVE IDSI0021-CODICE-COMPAGNIA-ANIA TO SST-COD-COMPAGNIA-ANIA
           MOVE IDSI0021-CODICE-STR-DATO       TO SST-COD-STR-DATO
           MOVE IDSI0021-MODALITA-ESECUTIVA    TO SST-FLAG-MOD-ESECUTIVA
           MOVE IDSI0021-OPERAZIONE            TO SST-OPERAZIONE.
      *
       S210-EX.
           EXIT.
      *************************************************************************
       S200-ACCEDI-SU-SST SECTION.
      *
           EXEC SQL
                SELECT COD_SIN_STR_DATO
                   INTO :SST-COD-STR-DATO
                FROM SINONIMO_STR
                WHERE COD_COMPAGNIA_ANIA = :SST-COD-COMPAGNIA-ANIA
                AND   COD_STR_DATO       = :SST-COD-STR-DATO
                AND   OPERAZIONE         = :SST-OPERAZIONE
                AND   FLAG_MOD_ESECUTIVA = :SST-FLAG-MOD-ESECUTIVA
           END-EXEC.

            MOVE SQLCODE         TO IDSO0021-SQLCODE
            MOVE DESCRIZ-ERR-DB2 TO IDSO0021-DESCRIZ-ERR-DB2

            IF IDSO0021-SUCCESSFUL-SQL
               MOVE SST-COD-SIN-STR-DATO  TO IDSI0021-CODICE-STR-DATO
               ADD 1                      TO IND-SINONIM
            ELSE
               IF NOT IDSO0021-NOT-FOUND
                  PERFORM 2000-DBERROR
                  MOVE 'SINONIMO_STR'     TO IDSO0021-NOME-TABELLA
               END-IF
            END-IF.
      *
       S200-EX.
           EXIT.
      *************************************************************************
      *                      GESTIONE ERRORE DB2
      *************************************************************************
       2000-DBERROR SECTION.
      *
           MOVE 'D3'                TO IDSO0021-RETURN-CODE.
           MOVE PGM-NAME-IDSS0020   TO IDSO0021-COD-SERVIZIO-BE.
      *
       2000-EX.
           EXIT.
      *************************************************************************
      *               GESTIONE CONDIZIONE ERRORE
      *************************************************************************
       4000-FINE SECTION.
      *
           IF  INDICE-ANAGR-DATO = 0 AND WK-NUMERO-LIVELLO = 0
              PERFORM 2000-DBERROR
           END-IF.


       4000-EX-FINE.
           EXIT.



























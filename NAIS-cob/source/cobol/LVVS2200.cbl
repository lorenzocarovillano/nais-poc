      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS2200.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS2200
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CALCOLO DELLA VARIABILE PRATA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS2200'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
      *
      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBS
      *---------------------------------------------------------------*
      *---------------------------------------------------------------*
      *
           COPY IDBVSTB1.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-PMO.
          03 DPMO-AREA-PMO.
             04 DPMO-ELE-PMO-MAX        PIC S9(04) COMP.
                COPY LCCVPMOB REPLACING   ==(SF)==  BY ==DPMO==.
             COPY LCCVPMO1              REPLACING ==(SF)== BY ==DPMO==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
      *
       01  WK-TROVATO                        PIC X(02).
           88 WK-TROVATO-SI                  VALUE 'SI'.
           88 WK-TROVATO-NO                  VALUE 'NO'.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET WK-TROVATO-NO                 TO TRUE.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-PMO
      *
      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1200-CALCOLA-PRATA          THRU S1200-EX
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-PARAM-MOV
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPMO-AREA-PMO
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA PRATA
      *----------------------------------------------------------------*
       S1200-CALCOLA-PRATA.
      *
           MOVE ZEROES               TO IVVC0213-VAL-IMP-O

           PERFORM VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > DPMO-ELE-PMO-MAX
                OR WK-TROVATO-SI
                EVALUATE DPMO-TP-MOVI(IX-DCLGEN)
                    WHEN 6101
                    WHEN 6002
                    WHEN 6003
FAZIO               WHEN 1075
                      IF DPMO-IMP-LRD-DI-RAT-NULL(IX-DCLGEN)
                         = HIGH-VALUE OR LOW-VALUE OR SPACES
                         CONTINUE
                      ELSE
                         MOVE DPMO-IMP-LRD-DI-RAT(IX-DCLGEN)
                           TO IVVC0213-VAL-IMP-O
                         SET WK-TROVATO-SI TO TRUE
                      END-IF
                END-EVALUATE
           END-PERFORM.

       S1200-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

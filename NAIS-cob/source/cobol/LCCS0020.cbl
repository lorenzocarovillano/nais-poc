      ******************************************************************
      **                                                        ********
      **    PORTAFOGLIO VITA ITALIA  VER 1.0                    ********
      **                                                        ********
      ******************************************************************

      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LCCS0020.
       AUTHOR.             ATS.
       DATE-WRITTEN.       11/2007.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *    PROGRAMMA ..... LCCS0020
      *    TIPOLOGIA...... XXX
      *    PROCESSO....... XXX
      *    FUNZIONE....... LETTURA TABELLA MATRICE MOVIMENTO
      *    DESCRIZIONE.... TRASCODIFICA TIPO MOV-NAIS <-> MOV-ACTUATOR
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(08) VALUE 'LCCS0020'.

      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01 WK-VARIABILI.
          03 WK-MOVI-INP                    PIC X(05).
          03 WK-MOVI-OUT                    PIC 9(05).

      *----------------------------------------------------------------*
      *    DEFINIZIONE DI FLAG E SWITCH                                *
      *----------------------------------------------------------------*
       01  WS-ESITO                         PIC X(01) VALUE 'S'.
           88 WS-ESITO-OK                             VALUE 'S'.
           88 WS-ESITO-KO                             VALUE 'N'.

      *----------------------------------------------------------------*
      *    DCLGEN MATRICE MOVIMENTO                                    *
      *----------------------------------------------------------------*
           COPY IDBVMMO1.

      *----------------------------------------------------------------*
      *     MODULI CHIAMATI                                            *
      *----------------------------------------------------------------*
       01  LDBS0260                         PIC X(08) VALUE 'LDBS0260'.
      *--> pgm conversione stringa a numerico
       01  IWFS0050                         PIC X(08) VALUE 'IWFS0050'.

      *----------------------------------------------------------------*
      *--> Area per modulo conversione stringa
       01  AREA-CALL-IWFS0050.
           COPY IWFI0051.
           COPY IWFO0051.

      *----------------------------------------------------------------*
      *    COPY DI INPUT PER LA CHIAMATA AL LDBS0260
      *----------------------------------------------------------------*
           COPY LDBI0260.
           COPY IDSV0003.

      *----------------------------------------------------------------*
      *     COMMAREA SERVIZIO
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01 IDSV0001.
          COPY IDSV0001.

          COPY LCCV0021.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0001 LCCV0021.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF WS-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI                                         *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           PERFORM S0005-INITIALIZE
              THRU EX-S0005.

           PERFORM S0010-CTRL-DATI-INPUT
              THRU EX-S0010.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    INIZIALIZZAZIONI
      *----------------------------------------------------------------*
       S0005-INITIALIZE.

           SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
           SET WS-ESITO-OK                TO TRUE.


           MOVE ZEROES                    TO IDSV0003-SQLCODE.

           MOVE SPACES                    TO IDSV0003-DESCRIZ-ERR-DB2
                                             IDSV0003-NOME-TABELLA
                                             IDSV0003-KEY-TABELLA.
       EX-S0005.
           EXIT.
      *----------------------------------------------------------------*
      *  CONTROLLI DI INPUT
      *----------------------------------------------------------------*
       S0010-CTRL-DATI-INPUT.

           IF IDSV0001-COD-COMPAGNIA-ANIA IS NOT NUMERIC
           OR IDSV0001-COD-COMPAGNIA-ANIA = ZERO
      *-->    ERRORE:IL CAMPO E' OBBLIGATORIO
              SET  WS-ESITO-KO      TO TRUE
              MOVE WK-PGM           TO LCCV0021-COD-SERVIZIO-BE
              STRING 'CODICE COMPAGNIA NON VALIDO '
              DELIMITED BY SIZE   INTO LCCV0021-DESCRIZ-ERR
              END-STRING
           ELSE
              IF LCCV0021-TP-MOV-PTF IS NOT NUMERIC
              OR LCCV0021-TP-MOV-PTF = ZERO
      *-->       ERRORE: IL CAMPO E' OBBLIGATORIO
                 SET  WS-ESITO-KO      TO TRUE
                 MOVE WK-PGM           TO LCCV0021-COD-SERVIZIO-BE
                 STRING 'TIPO MOVIMENTO NON VALIDO '
                 DELIMITED BY SIZE   INTO LCCV0021-DESCRIZ-ERR
                 END-STRING
              END-IF
           END-IF.

       EX-S0010.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE                                                *
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           PERFORM S1100-PREPARA-LDBS0260
              THRU EX-S1100.

           PERFORM S1200-CALL-LDBS0260
              THRU EX-S1200.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE CAMPI DI INPUT DA PASSARE AL SERVIZIO        *
      *----------------------------------------------------------------*
       S1100-PREPARA-LDBS0260.

           SET IDSV0003-SELECT            TO TRUE.
           SET IDSV0003-WHERE-CONDITION   TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.

           MOVE IDSV0001-TRATTAMENTO-STORICITA
             TO IDSV0003-TRATTAMENTO-STORICITA.

           MOVE IDSV0001-FORMATO-DATA-DB
             TO IDSV0003-FORMATO-DATA-DB.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO IDSV0003-CODICE-COMPAGNIA-ANIA.
           MOVE LCCV0021-TP-MOV-PTF
             TO LDBI0260-COD-MOVI-NAIS.

           MOVE AREA-LDBI0260
             TO IDSV0003-BUFFER-WHERE-COND.

       EX-S1100.
           EXIT.
      *----------------------------------------------------------------*
      *    CHIAMATA AL SERVIZIO LDBS0260
      *----------------------------------------------------------------*
       S1200-CALL-LDBS0260.

           CALL LDBS0260        USING IDSV0003
                                      MATR-MOVIMENTO
           ON EXCEPTION
              MOVE WK-PGM              TO LCCV0021-COD-SERVIZIO-BE
              SET  IDSV0003-SQL-ERROR  TO TRUE
              STRING 'ERRORE CALL LDBS0260 '
              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-CALL.

           INITIALIZE                     LCCV0021-AREA-OUTPUT.

           MOVE IDSV0003-RETURN-CODE   TO LCCV0021-RETURN-CODE.
           MOVE IDSV0003-SQLCODE       TO LCCV0021-SQLCODE.
           MOVE IDSV0003-CAMPI-ESITO   TO LCCV0021-CAMPI-ESITO.

           IF  LCCV0021-SUCCESSFUL-RC
           AND LCCV0021-SUCCESSFUL-SQL
               MOVE MMO-TP-MOVI-ACT    TO WK-MOVI-INP
      *--> CODIFICA DEL CODICE MOVIMENTO DA ALFANUMERICO A NUMERICO DI 5
               PERFORM S3000-CODIFICA-MOVI
                  THRU S3000-EX
               MOVE WK-MOVI-OUT         TO LCCV0021-TP-MOV-ACT
               MOVE MMO-COD-PROCESSO-WF TO LCCV0021-COD-PROCESSO-WF
           ELSE
              IF LCCV0021-NOT-FOUND
                 SET  LCCV0021-SQL-ERROR TO TRUE
                 MOVE SPACES             TO LCCV0021-DESCRIZ-ERR
                 MOVE WK-PGM             TO LCCV0021-COD-SERVIZIO-BE
                 MOVE  'OCCORRENZA NON TROVATA SU TAB. MATR_MOVIMENTO'
                                         TO LCCV0021-DESCRIZ-ERR
              END-IF
           END-IF.

       EX-S1200.
           EXIT.
      *----------------------------------------------------------------*
      *    CONVERTI VALORE DA STRINGA IN NUMERICO
      *----------------------------------------------------------------*
       S3000-CODIFICA-MOVI.

           MOVE HIGH-VALUE              TO AREA-CALL-IWFS0050.
           MOVE WK-MOVI-INP             TO IWFI0051-ARRAY-STRINGA-INPUT.

           CALL IWFS0050 USING AREA-CALL-IWFS0050

           MOVE IWFO0051-ESITO          TO IDSV0001-ESITO.
           MOVE IWFO0051-LOG-ERRORE     TO IDSV0001-LOG-ERRORE.
           MOVE IWFO0051-CAMPO-OUTPUT-DEFI
                                        TO WK-MOVI-OUT.

       S3000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.

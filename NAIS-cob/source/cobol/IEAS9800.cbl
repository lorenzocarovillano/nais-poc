      *****************************************************************
      *                                                               *
      *                   IDENTIFICATION DIVISION                     *
      *                                                               *
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IEAS9800.
       AUTHOR.        ACCENTURE.
       DATE-WRITTEN.  18/07/06.

      *****************************************************************
      *                                                               *
      *    NOME :           IEAS9800                                  *
      *    TIPO :           ROUTINE COBOL-DB2                         *
      *    DESCRIZIONE :    ROUTINE SOSTITUZIONE PARAMETRI IN         *
      *                     DESCREIZIONE ESTESA TRAMITE PLACEHOLDER $ *
      *                     AD ESCLUSIVO USO DELLA ROUTINE IEAS9900   *
      *                                                               *
      *    AREE DI PASSAGGIO DATI                                     *
      *                                                               *
      *    DATI DI INPUT : IDSV0001, IEAI9801                         *
      *    DATI DI OUTPUT: IEAO9801                                   *
      *                                                               *
      *****************************************************************
      * LOG MODIFICHE :                                               *
      *                                                               *
      *                                                               *
      *****************************************************************
      *****************************************************************
      *                                                               *
      *                   ENVIRONMENT  DIVISION                       *
      *                                                               *
      *****************************************************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
      *SOURCE-COMPUTER. IBM-370 WITH DEBUGGING MODE.
      *OBJECT-COMPUTER. IBM-370.
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.


      *****************************************************************
      *                                                               *
      *                   DATA  DIVISION                              *
      *                                                               *
      *****************************************************************
       DATA DIVISION.

      *****************************************************************
      *                                                               *
      *                   WORKING STORAGE SECTION                     *
      *                                                               *
      *****************************************************************
       WORKING-STORAGE SECTION.

      *****************************************************************
      *                                                               *
      *                   FLAG PER ESITO ELABORAZIONE                 *
      *                                                               *
      *****************************************************************

       01 WS-ESITO                                 PIC X(02).
          88 SI-CORRETTO                           VALUE 'OK'.
          88 NO-CORRETTO                           VALUE 'KO'.

      *****************************************************************
      *                                                               *
      *                   INDICI                                      *
      *                                                               *
      *****************************************************************
       01 WS-INDICI.
          05 WS-IND-PARAM                          PIC 9(9).
          05 WS-INDICE                             PIC 9(9).
          05 WS-IND                                PIC 9(9).
          05 WS-IND-STRINGA                        PIC 9(9).
          05 WS-IND-POS                            PIC 9(9).
          05 WS-IND-PARM                           PIC 9(9).
          05 WS-POS-SUCC                           PIC 9(9).
          05 WS-POS-NEXT                           PIC 9(9).

      *****************************************************************
      *
      * COSTANTI
      *
      *****************************************************************

       01 WS-LIMITE-ELE-PARAMETRI          PIC 9(02) VALUE 010.
       01 WS-LIMITE-STR-PARAMETRO          PIC 9(03) VALUE 100.
       01 WS-LIMITE-STR-DESC-ERR           PIC 9(03) VALUE 200.


      *****************************************************************
      *                                                               *
      *                   TABELLA DI APPOGGIO PER ESTRAZIONE          *
      *                   PARAMETRI                                   *
      *                                                               *
      *****************************************************************

       01 WS-LISTA-PARAMETRI.
          05 WS-TABELLA-PARAMETRI          OCCURS 10 TIMES.
             10 WS-PARAMETRO               PIC X(100) VALUE SPACES.

       01 WS-DESC-ERRORE                   PIC X(200).

      *-- COPY DI INPUT DELLA ROUTINE
      *     COPY IEAI9801.

      *-- COPY DI OUTPUT DELLA ROUTINE
      *     COPY IEAO9801.

      *****************************************************************
      *                                                               *
      *                   LINKAGE SECTION                             *
      *                                                               *
      *****************************************************************
       LINKAGE SECTION.

      *-- COMMAREA PER CHIAMATA DA CICS
       01 AREA-IDSV0001.
           COPY IDSV0001.

      *-- COPY DI INPUT DELLA ROUTINE
           COPY IEAI9801.

      *-- COPY DI OUTPUT DELLA ROUTINE
           COPY IEAO9801.

      *****************************************************************
      *                                                               *
      *                   PROCEDURE DIVISION                          *
      *                                                               *
      *****************************************************************
      *
       PROCEDURE DIVISION USING AREA-IDSV0001
                                IEAI9801-AREA
                                IEAO9801-AREA.

      *****************************************************************
      *                                                               *
      *                   1000-PRINCIPALE                             *
      *                                                               *
      *****************************************************************
       1000-PRINCIPALE.

      *-- INIZIALIZZO IL FLAG PER L'ESITO DELL'ELABORAZIONE
           SET SI-CORRETTO                    TO TRUE.

      *-- INIZIALIZZO AREA DI INPUT/OUTPUT
SET06 *      MOVE SPACES                       TO IEAI9801-AREA
SET06 *                                           IEAO9801-AREA
OTT06       MOVE SPACES                       TO WS-DESC-ERRORE
OTT06                                            WS-LISTA-PARAMETRI.

      *-- ESTRAZIONE DEI PARAMETRI DI SOSTITUZIONE
           IF SI-CORRETTO
              PERFORM 1100-ESTRAI-PARAMETRI
                 THRU 1100-ESTRAI-PARAMETRI-EXIT
           END-IF.

      *-- INIZIA RICERCA NELLA STRINGA,
      *-- CREANDO UNA STRINGA DI APPOGGIO FORMATTATA
           IF SI-CORRETTO
              PERFORM 1200-RICERCA-DOLLARO
                 THRU 1200-RICERCA-DOLLARO-EXIT
           END-IF.

      *-- VALORIZZAZIONE DELL'OUTPUT
           IF SI-CORRETTO
              PERFORM 1300-VALORIZZA-OUTPUT
                 THRU 1300-VALORIZZA-OUTPUT-EXIT
           END-IF.

       1000-PRINCIPALE-EXIT.
           EXIT.
           GOBACK.

      *-- ESTRAZIONE DEI PARAMETRI DI SOSTITUZIONE
       1100-ESTRAI-PARAMETRI.

           MOVE 1                        TO WS-IND-POS
                                            WS-IND-PARM.
           PERFORM VARYING WS-INDICE FROM 1 BY 1
             UNTIL WS-INDICE > WS-LIMITE-STR-PARAMETRO
OTT06           OR IEAI9801-PARAMETRI-ERR(WS-INDICE:)
                            = SPACES OR LOW-VALUE OR HIGH-VALUE
             IF IEAI9801-PARAMETRI-ERR(WS-INDICE:1) = ';'
                ADD  1                   TO WS-IND-PARM
                MOVE 1                   TO WS-IND-POS
             ELSE
                MOVE IEAI9801-PARAMETRI-ERR(WS-INDICE:1)
                  TO WS-PARAMETRO(WS-IND-PARM)(WS-IND-POS:1)
                ADD 1 TO WS-IND-POS
             END-IF
           END-PERFORM.

       1100-ESTRAI-PARAMETRI-EXIT.
           EXIT.

      *-- EFFETTUA LA RICERCA DEI DOLLARI, FORMATTANDO UNA STRINGA DI
      *-- APPOGGIO E GESTITA ANCHE LA SOSTITUZIONE INDICIZZATA,
      *-- DOVE SUBITO DOPO IL DOLLARO VIENE SPECIFICATO
      *-- IL PARAMETRO CHE VA INSERITO NELLA GIUSTA POSIZIONE.
       1200-RICERCA-DOLLARO.

            MOVE 1 TO WS-IND-STRINGA
                      WS-IND-PARAM.

            PERFORM VARYING WS-INDICE FROM 1 BY 1
              UNTIL WS-INDICE > WS-LIMITE-STR-DESC-ERR
OTT06            OR IEAI9801-DESC-ERRORE-ESTESA(WS-INDICE:) =
                    SPACES OR LOW-VALUE OR HIGH-VALUE
                 OR NO-CORRETTO

                 IF WS-IND-PARAM >= WS-LIMITE-ELE-PARAMETRI

                    SET NO-CORRETTO      TO TRUE
                    MOVE 99              TO IEAO9801-COD-ERRORE-990

                 ELSE

                    IF IEAI9801-DESC-ERRORE-ESTESA
                                          (WS-INDICE:1) = '$'
                       COMPUTE WS-POS-SUCC = WS-INDICE + 1
                       COMPUTE WS-POS-NEXT = WS-INDICE + 2

                       IF IEAI9801-DESC-ERRORE-ESTESA(WS-POS-SUCC:1)
                          IS NUMERIC
                          IF IEAI9801-DESC-ERRORE-ESTESA
                                          (WS-POS-NEXT:1)
                             IS NUMERIC
                            MOVE IEAI9801-DESC-ERRORE-ESTESA
                                          (WS-POS-SUCC:2)
                                            TO WS-IND-PARM
                             ADD 2          TO WS-INDICE
                          ELSE
                            MOVE IEAI9801-DESC-ERRORE-ESTESA
                                          (WS-POS-SUCC:1)
                                            TO WS-IND-PARM
                             ADD 1          TO WS-INDICE
                          END-IF
                          PERFORM VARYING WS-IND FROM 1 BY 1
OTT06                        UNTIL WS-IND > WS-LIMITE-STR-PARAMETRO OR
                                  (WS-PARAMETRO(WS-IND-PARAM)(WS-IND:)
OTT06                          = SPACES OR LOW-VALUE OR HIGH-VALUE)
                              MOVE WS-PARAMETRO(WS-IND-PARM)(WS-IND:1)
                                           TO WS-DESC-ERRORE
                                           (WS-IND-STRINGA:1)
                              ADD 1        TO WS-IND-STRINGA
                          END-PERFORM
                       ELSE
                           PERFORM VARYING WS-IND FROM 1 BY 1
OTT06                         UNTIL WS-IND > WS-LIMITE-STR-PARAMETRO OR
                                   (WS-PARAMETRO(WS-IND-PARAM)
                                   (WS-IND:)
OTT06                           = SPACES OR LOW-VALUE OR HIGH-VALUE)
                               MOVE WS-PARAMETRO(WS-IND-PARAM)
                               (WS-IND:1)
                                           TO WS-DESC-ERRORE
                                           (WS-IND-STRINGA:1)
                               ADD 1       TO WS-IND-STRINGA
                           END-PERFORM
                           ADD 1           TO WS-IND-PARAM
                       END-IF
                    ELSE
                       MOVE IEAI9801-DESC-ERRORE-ESTESA(WS-INDICE:1)
                         TO  WS-DESC-ERRORE(WS-IND-STRINGA:1)
                       ADD 1                TO WS-IND-STRINGA
                    END-IF
                 END-IF

            END-PERFORM.

            MOVE SPACE       TO WS-DESC-ERRORE(WS-IND-STRINGA:).

       1200-RICERCA-DOLLARO-EXIT.
           EXIT.

      *-- VALORIZZAZIONE DEL'OUTPUT
       1300-VALORIZZA-OUTPUT.

           MOVE SPACES                   TO IEAO9801-DESC-ERRORE-ESTESA.
           MOVE WS-DESC-ERRORE           TO IEAO9801-DESC-ERRORE-ESTESA.
SET06      MOVE ZEROES                   TO IEAO9801-COD-ERRORE-990.

       1300-VALORIZZA-OUTPUT-EXIT.
           EXIT.

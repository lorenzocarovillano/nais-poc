       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LRGS0660.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.  2010.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                      *
      * F A S E         : SERVIZIO BUSINESS RICHIAMATO DAL BATCH EXEC. *
      *                                                                *
      *----------------------------------------------------------------*
      * Fase Diagnostica - Estratto Conto
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

      *--> FILEOUT 1
      *--> OK E/C 'RIVALUTABILI, INDEX E UNIT'
           SELECT FILESQS1 ASSIGN TO FILESQS1
           ACCESS IS SEQUENTIAL
           FILE STATUS IS FS-FILESQS1.

      *--> FILEOUT 2
      *--> KO E/C 'RIVALUTABILI, INDEX E UNIT'
           SELECT FILESQS2 ASSIGN TO FILESQS2
           ACCESS IS SEQUENTIAL
           FILE STATUS IS FS-FILESQS2.

      *--> FILEOUT 3
      *--> FILE DEGLI SCARTI
           SELECT FILESQS3 ASSIGN TO FILESQS3
           ACCESS IS SEQUENTIAL
           FILE STATUS IS FS-FILESQS3.

      *--> INPUT 4
      *--> IMPOSTA SOSTITUTIVA
           SELECT FILESQS4 ASSIGN TO FILESQS4
           ACCESS IS SEQUENTIAL
           FILE STATUS IS FS-FILESQS4.

      *--> INPUT 5
      *--> GRAVITA ERRORI
           SELECT FILESQS5 ASSIGN TO FILESQS5
           ACCESS IS SEQUENTIAL
           FILE STATUS IS FS-FILESQS5.

      *--> FILEOUT 6
      *--> KO E/C 'RIVALUTABILI, INDEX E UNIT'
           SELECT FILESQS6 ASSIGN TO FILESQS6
           ACCESS IS SEQUENTIAL
           FILE STATUS IS FS-FILESQS6.

       DATA DIVISION.
       FILE SECTION.

       FD  FILESQS1.
       01  FILESQS1-REC                  PIC X(2500).

       FD  FILESQS2.
       01  FILESQS2-REC                  PIC X(049).

       FD  FILESQS3.
       01  FILESQS3-REC                  PIC X(282).

       FD  FILESQS4.
       01  FILESQS4-REC                  PIC X(500).

       FD  FILESQS5.
       01  FILESQS5-REC                  PIC X(21).

       FD  FILESQS6.
       01  FILESQS6-REC                  PIC X(2500).

       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *   COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                        PIC X(08) VALUE 'LRGS0660'.
       77 ISPS0580                       PIC X(08) VALUE 'ISPS0580'.
       77 ISPS0590                       PIC X(08) VALUE 'ISPS0590'.
       77 PGM-LCCS0003                   PIC X(08) VALUE 'LCCS0003'.
       77 LDBS6190                       PIC X(08) VALUE 'LDBS6190'.
      *----------------------------------------------------------------*
      *   FLAG
      *----------------------------------------------------------------*
       01 AREA-FLAG.
          03 ERRORE-35                   PIC X(1) VALUE SPACES.
             88 35-ERRORE-SI             VALUE 'S'.
             88 35-ERRORE-NO             VALUE 'N'.

          03 ERRORE-37-TR               PIC X(1) VALUE SPACES.
             88 TROVATO-37-SI           VALUE 'S'.
             88 TROVATO-37-NO           VALUE 'N'.

          03 FLAG-CONTROLLO              PIC X(1) VALUE SPACES.
             88 CONTROLLO-OK             VALUE 'S'.
             88 CONTROLLO-KO             VALUE 'N'.

          03 FLAG-ABBINATA               PIC X(1) VALUE SPACES.
             88 ABBINATA-SI              VALUE 'S'.
             88 ABBINATA-NO              VALUE 'N'.

          03 FLAG-SCRITTURA              PIC X(1) VALUE SPACES.
             88 WRITE-SI                 VALUE 'S'.
             88 WRITE-NO                 VALUE 'N'.

          03 FLAG-LOOP-CNTRL-25          PIC X(1) VALUE SPACES.
             88 LOOP-CNTRL-25-SI         VALUE 'S'.
             88 LOOP-CNTRL-25-NO         VALUE 'N'.

          03 FLAG-RECORD-0               PIC X(1) VALUE SPACES.
             88 PRIMO-RECORD-SI          VALUE 'S'.
             88 PRIMO-RECORD-NO          VALUE 'N'.

          03 FLAG-CNTRL-37               PIC X(1) VALUE SPACES.
             88 CNTRL-37-ERR-SI          VALUE 'S'.
             88 CNTRL-37-ERR-NO          VALUE 'N'.

          03 FLAG-CNTRL-34               PIC X(1) VALUE SPACES.
             88 CNTRL-34-ERR-SI          VALUE 'S'.
             88 CNTRL-34-ERR-NO          VALUE 'N'.

MOD       03 FLAG-CNTRL-ESEC-34          PIC X(1) VALUE SPACES.
MOD          88 CNTRL-34-SI              VALUE 'S'.
MOD          88 CNTRL-34-NO              VALUE 'N'.

          03 FLAG-CNTRL-3-RV             PIC X(1) VALUE SPACES.
             88 CONTR-3-RV-SI            VALUE 'S'.
             88 CONTR-3-RV-NO            VALUE 'N'.

          03 FLAG-CNTRL-12-RV            PIC X(1) VALUE SPACES.
             88 CONTR-12-RV-SI           VALUE 'S'.
             88 CONTR-12-RV-NO           VALUE 'N'.

          03 FLAG-580                    PIC X(1) VALUE SPACES.
             88 POLI-580-SI              VALUE 'S'.
             88 POLI-580-NO              VALUE 'N'.

13087     03 FLAG-CNTRL-30               PIC X(1) VALUE SPACES.
13087        88 CONTROLLO-30-SI          VALUE 'S'.
13087        88 CONTROLLO-30-NO          VALUE 'N'.

          03 FLAG-INT-ERR                PIC X(1) VALUE SPACES.
             88 INTESTAZIONE-SI          VALUE 'S'.
             88 INTESTAZIONE-NO          VALUE 'N'.

13091     03 FLAG-ERRORE-SI              PIC X(1) VALUE SPACES.
13091        88 ERRORE-SI                VALUE 'S'.
13091        88 ERRORE-NO                VALUE 'N'.

12193     03 FLAG-TROVATO-MOVI           PIC X(1) VALUE SPACES.
12193        88 TROVATO-MOVI-SI          VALUE 'S'.
12193        88 TROVATO-MOVI-NO          VALUE 'N'.

12193     03 REC-9-TP-MOVI-TROVATO       PIC X(1) VALUE SPACES.
12193        88 REC-9-TP-MOVI-TROVATO-SI VALUE 'S'.
12193        88 REC-9-TP-MOVI-TROVATO-NO VALUE 'N'.

       01 TIPOLOGIA-EC                   PIC X(0003).
             88 TP-ESTR-MU               VALUE 'MU '.
             88 TP-ESTR-RV               VALUE 'PRI'.
             88 TP-ESTR-UL               VALUE 'PUL'.
ALFR         88 TP-ESTR-TCM              VALUE 'TC '.

      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-IND                       PIC  9(04).
           03 IX-REC-5                     PIC  9(04).
           03 IX-REC-8                     PIC  9(04).
           03 IX-REC-3                     PIC  9(04).
           03 IX-REC-10                    PIC  9(04).
12193      03 IX-REC-9                     PIC  9(04).
           03 IX-IND-P84                   PIC  9(04).
           03 IX-TAB-P85                   PIC  9(04).
           03 IX-CNTR-34                   PIC  9(04).
           03 IX-580                       PIC  9(04).
           03 IX-GRAV                      PIC  9(04).
           03 IX-C37-TAB                   PIC  9(04).
           03 IX-C35-TAB                   PIC  9(04).
12193      03 IX-REC9-TP-MOVI              PIC  9(04).
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WS-VARIABILI.
           03 REC2-COGNOME-ASS            PIC X(0050).
           03 REC2-NOME-ASS               PIC X(0050).
           03 WK-TABELLA                  PIC X(20).
           03 WK-LABEL                    PIC X(40).
           03 WS-ERRORE                   PIC X(100).
           03 WK-LABEL-ERR                PIC X(30).
           03 TMP-IMP-OPER                PIC -9(012)V9(003).
           03 WK-CNTRL-37                 PIC S9(012)V9(003).
           03 WK-RCODE                    PIC  X(02).
           03 WK-DATA-IN                  PIC  9(08).
           03 WK-DATA-OUT                 PIC  9(08).
           03 WK-CONTROLLO                PIC  9(08).
           03 WK-FRAZ                     PIC  9(05).
           03 WK-DT-DECOR                 PIC  X(0008).
           03 WK-DT-DECOR-AA              PIC  9(4) VALUE ZERO.
           03 WK-DT-INF                   PIC  9(008).
           03 WK-DT-SUP                   PIC  9(008).
GRAV       03 REC9-IMP-LORDO-RISC-PARZ    PIC 9(0012)V9(03).
       01 DATE-WORK.
           03 WK-DT-RICOR-DA-X            PIC X(08).
           03 WK-DT-RICOR-DA   REDEFINES
              WK-DT-RICOR-DA-X            PIC 9(08).
           03 WK-DT-RICOR-A-X            PIC X(08).
           03 WK-DT-RICOR-A   REDEFINES
              WK-DT-RICOR-A-X            PIC 9(08).
           03 WK-ANNO-RIF-X               PIC X(04).
           03 WK-ANNO-RIF     REDEFINES
              WK-ANNO-RIF-X               PIC 9(04).
           03 WK-INI-RIC.
              05 WK-DT-INI-AA             PIC 9(04).
              05 WK-DT-INI-MM             PIC 9(02) VALUE 12.
              05 WK-DT-INI-GG             PIC 9(02) VALUE 31.
           03 WK-FIN-RIC.
              05 WK-DT-FIN-AA             PIC 9(04).
              05 WK-DT-FIN-MM             PIC 9(02) VALUE 12.
              05 WK-DT-FIN-GG             PIC 9(02) VALUE 31.

       01 WORK-CALCOLI.
          03 WS-DIFF                      PIC S9(0012)V9(03).
          03 WS-DIFF-3                    PIC S9(0007)V9(05).
NEW       03 WS-DIFF-2                    PIC S9(0012)V9(02).
          03 WS-DIV                       PIC 9(0012)V9(03).
          03 WS-PERC                      PIC 9(0012)V9(03).
          03 WS-SOMMA                     PIC 9(0012)V9(03).
          03 WS-SOMMA-2                   PIC 9(0012)V9(03).
          03 WS-SOMMA-3                   PIC 9(0007)V9(05).
ALEX1     03 WS-SOMMA-4                   PIC 9(0007)V9(05).
          03 WS-SOMMA-123                 PIC 9(0012)V9(03).
          03 WS-DIFF-123                  PIC S9(0012)V9(03).
          03 WS-CNTRVAL-PREC              PIC 9(0012)V9(03).
          03 WS-APPO-43-1                 PIC S9(0012)V9(03).
          03 WS-APPO-43-2                 PIC S9(0012)V9(03).
          03 WS-APPO-1                    PIC 9(0012)V9(03).
          03 WS-APPO-2                    PIC 9(0012)V9(03).
          03 WS-APPO-3                    PIC 9(0012)V9(03).
          03 WS-CAMPO-APPOGGIO            PIC 9(0003)V9(02).
          03 WK-TASSO-TEC-RV              PIC 9(0005)V9(09).

       01 TRACCIATO-FILE.
          03 OUT-IB-OGG                  PIC X(20).
          03 FILLER-1                    PIC X(01).
          03 OUT-ERR                     PIC X(185).
          03 FILLER-2                    PIC X(01).
          03 WS-CAMPO-1                  PIC 9(12),9(03).
          03 FILLER-3                    PIC X(01).
          03 WS-CAMPO-2                  PIC 9(12),9(03).
          03 FILLER-4                    PIC X(01).
          03 WS-CAMPO-3                  PIC 9(12),9(03).
          03 FILLER-7                    PIC X(01).
          03 WS-DATA-1                   PIC X(08).
          03 FILLER-5                    PIC X(01).
          03 WS-DATA-2                   PIC X(08).
          03 FILLER-6                    PIC X(01).
          03 WS-DATA-3                   PIC X(08).
          03 FILLER                      PIC X(02).


       01 TRACCIATO-FILE-GRAV.
          03 IN-NUM-CNTRL                PIC 9(9).
          03 FILLER                      PIC X(1).
          03 IN-GRAV-CNTRL               PIC 9(9).
          03 FILLER                      PIC X(1).
          03 IN-TP-CNTRL                 PIC X(1).

       01 AREA-ERRORI.
          03 ELE-MAX-ERRORI            PIC 9(9).
          03 ELE-AREA-ERR            OCCURS 500.
             05 CONTROLLO              PIC 9(9).
             05 GRAVITA                PIC 9(9).

       01 WK-AREA-FILE.
          03 WK-IB-OGG                   PIC X(40).
          03 WK-ID-POLI                  PIC 9(09).
          03 WK-ID-ADES                  PIC 9(09).

      *----------------------------------------------------------------*
      *    FLAG
      *----------------------------------------------------------------*
       01 FLAG-TROVATO                     PIC X(01).
          88 TROVATO-SI                    VALUE 'S'.
          88 TROVATO-NO                    VALUE 'N'.

       01 FLAG-ELABOR-CED                  PIC X(01).
          88 ELABORATO-SI                  VALUE 'S'.
          88 ELABORATO-NO                  VALUE 'N'.

       01 FLAG-FINE-FILE-RIV               PIC X(01).
          88 FINE-FILE-RIV-SI              VALUE 'S'.
          88 FINE-FILE-RIV-NO              VALUE 'N'.

       01 FLAG-FINE-FILE-CED               PIC X(01).
          88 FINE-FILE-CED-SI              VALUE 'S'.
          88 FINE-FILE-CED-NO              VALUE 'N'.

       01 FLAG-FINE-FILE-IMP               PIC X(01).
          88 FINE-FILE-IMP-SI              VALUE 'S'.
          88 FINE-FILE-IMP-NO              VALUE 'N'.

       01 FLAG-FINE-FILE-POL               PIC X(01).
          88 FINE-LETTURA-POLI-SI          VALUE 'S'.
          88 FINE-LETTURA-POLI-NO          VALUE 'N'.

ALFR   01 FLAG-GAR-RAMO3                   PIC X(01).
          88 GAR-RAMO3-SI                  VALUE 'S'.
          88 GAR-RAMO3-NO                  VALUE 'N'.

ALFR   01 FLAG-GAR-RAMO3-STORNATA          PIC X(01).
          88 GAR-RAMO3-STOR-SI             VALUE 'S'.
          88 GAR-RAMO3-STOR-NO             VALUE 'N'.

      *---------------------------------------------------------------*
      * AREA PER CONTATORI PERSONALIZZATI
      *---------------------------------------------------------------*
       01 WK-AREA-CONTATORI.
          03 WK-CTR-1.
             05 DESC-CTR-1           PIC X(050)
                 VALUE '   POLIZZE LETTE          '.
             05 REC-ELABORATI        PIC 9(002) VALUE 1.
          03 WK-CTR-2.
             05 DESC-CTR-2           PIC X(050)
                 VALUE '   POLIZZE DA SCARTARE '.
             05 REC-SCARTATI         PIC 9(002) VALUE 2.
          03 WK-CTR-3.
             05 DESC-CTR-3           PIC X(050)
                 VALUE 'TOTALE PERCENTUALE INVIABILI  %               '.
             05 REC-PERC             PIC 9(002) VALUE 3.

      *-----------------------------------------------------------------*
      *    CAMPI X ROUTINES DI GESTIONE DISPLAY IDSP8888
      *-----------------------------------------------------------------*
           COPY IDSV8888.
      *----------------------------------------------------------------*
      *    COPY PER GESTIONE FILE SEQUENZIALI
      *----------------------------------------------------------------*
           COPY IABVSQS1.
           COPY IABCSQ99.
      *----------------------------------------------------------------*
      *    COPY DB2
      *----------------------------------------------------------------*
           COPY IDBVPOL1.
           COPY IDBVISO1.
           COPY IDBVISO2.
           COPY IDBVISO3.
           COPY LDBVF301.
           COPY LDBVF321.
           COPY LDBV6191.
      *----------------------------------------------------------------*
      *    Area RIVALUTAZIONE
      *----------------------------------------------------------------
       01  AREA-RIVAL.
           COPY LOAR0171.
      *----------------------------------------------------------------*
           COPY IDBVP841.
      *----------------------------------------------------------------*
      *    Area CEDOLE
      *----------------------------------------------------------------
      *01  AREA-CEDOLE.
      *    COPY LOAR0271.
           COPY IDBVP851.

41336 *-- AREA-EC-DIAGNOSTICO-CEDOLE
41336  01 AREA-P84.
41336     04 WP84-ELE-P84-MAX                   PIC S9(04) COMP VALUE 0.
41336     04 WP84-TAB-P84                       OCCURS 30.
41336        COPY LCCVP841                      REPLACING ==(SF)==
41336                                           BY ==WP84==.

41336 *-- AREA-EC-DIAGNOSTICO-CEDOLE
41336  01 AREA-P85.
41336     04 WP84-ELE-P85-MAX                   PIC S9(04) COMP VALUE 0.
41336     04 WP84-TAB-P85                       OCCURS 30.
41336        COPY LCCVP851                      REPLACING ==(SF)==
41336                                           BY ==WP85==.
      *----------------------------------------------------------------*
      * -- AREE TABELLE TEMPORANEE
      *----------------------------------------------------------------*
       01 AREA-CACHE-CONTR-37.
          03 IX-MAX-C37                   PIC 9(9).
50636 *   03 ELE-CACHE-37 OCCURS 50.
50636     03 ELE-CACHE-37 OCCURS 100.
             05 C37-COD-FND                   PIC X(0020).
             05 C37-NUM-QUO-PREC              PIC 9(0007)V9(05).
             05 C37-NUM-QUO-DISINV-RISC-PARZ  PIC 9(0007)V9(05).
             05 C37-NUM-QUO-DIS-RISC-PARZ-PRG PIC 9(0007)V9(05).
             05 C37-NUM-QUO-DISINV-IMPOS-SOST PIC 9(0007)V9(05).
             05 C37-NUM-QUO-DISINV-COMMGES    PIC 9(0007)V9(05).
             05 C37-NUM-QUO-DISINV-PREL-COSTI PIC 9(0007)V9(05).
             05 C37-NUM-QUO-DISINV-SWITCH     PIC 9(0007)V9(05).
             05 C37-NUM-QUO-INVST-SWITCH      PIC 9(0007)V9(05).
             05 C37-NUM-QUO-INVST-VERS        PIC 9(0007)V9(05).
             05 C37-NUM-QUO-INVST-REBATE      PIC 9(0007)V9(05).
             05 C37-NUM-QUO-INVST-PURO-RISC   PIC 9(0007)V9(05).
             05 C37-NUM-QUO-ATTUALE           PIC 9(0007)V9(05).
13815        05 C37-NUM-QUO-DISINV-COMP-NAV   PIC 9(0007)V9(05).
13815        05 C37-NUM-QUO-INVST-COMP-NAV    PIC 9(0007)V9(05).

       01 AREA-CACHE-CONTR-35.
          03 IX-MAX-C35                   PIC 9(9).
13067 *   03 ELE-CACHE-35 OCCURS 50.
13067 *   03 ELE-CACHE-35 OCCURS 70.
15424     03 ELE-CACHE-35 OCCURS 100.
             05 C35-ID-GAR                    PIC 9(0009).
             05 C35-CNTRVAL-PREC              PIC 9(0012)V9(03).

       01 AREA-CACHE-3.
          03 ELE-MAX-CACHE-3              PIC 9(9).
          03 ELE-CACHE-3 OCCURS 50.
             05 REC3-ID-GAR                     PIC 9(0009).
             05 REC3-COD-GAR                    PIC X(0012).
             05 REC3-TP-GAR                     PIC 9(0002).
             05 REC3-TP-INVST                   PIC 9(0002).
             05 REC3-CUM-PREM-INVST-AA-RIF      PIC 9(0012)V9(03).
             05 REC3-CUM-PREM-VERS-AA-RIF       PIC 9(0012)V9(03).
             05 REC3-PREST-MATUR-AA-RIF         PIC 9(0012)V9(03).
             05 REC3-CAP-LIQ-GA                 PIC 9(0012)V9(03).
             05 REC3-RISC-TOT-GA                PIC 9(0012)V9(03).
             05 REC3-CUM-PREM-VERS-EC-PREC      PIC 9(0012)V9(03).
             05 REC3-PREST-MATUR-EC-PREC        PIC 9(0012)V9(03).
             05 REC3-IMP-LRD-RISC-PARZ-AP-GAR   PIC 9(0012)V9(03).
             05 REC3-IMP-LRD-RISC-PARZ-AC-GAR   PIC 9(0012)V9(03).
             05 REC3-TASSO-ANN-RIVAL-PREST-GAR  PIC 9(0005)V9(09).
             05 REC3-TASSO-TECNICO              PIC 9(0005)V9(09).
             05 REC3-REND-LOR-GEST-SEP-GAR      PIC 9(0005)V9(09).
             05 REC3-ALIQ-RETROC-RICON-GAR      PIC 9(0003)V9(03).
             05 REC3-TASSO-ANN-REND-RETROC-GAR  PIC 9(0005)V9(09).
             05 REC3-PC-COMM-GEST-GAR           PIC 9(0003)V9(03).
             05 REC3-REN-MIN-TRNUT              PIC 9(0005)V9(09).
             05 REC3-REN-MIN-GARTO              PIC 9(0005)V9(09).
ALFR         05 REC3-FL-STORNATE-ANN            PIC X.

       01 AREA-CACHE-10.
          03 ELE-MAX-CACHE-10             PIC 9(9).
ALFR  *   03 ELE-CACHE-10 OCCURS 300.
ALFR      03 ELE-CACHE-10 OCCURS 350.
             05 REC10-ID-GAR                    PIC 9(0009).
             05 REC10-DESC-TP-MOVI              PIC X(0050).
             05 REC10-TP-MOVI                   PIC 9(0005).
             05 REC10-IMP-OPER                  PIC 9(0012)V9(03).
             05 REC10-IMP-VERS                  PIC 9(0012)V9(03).
             05 REC10-NUMERO-QUO                PIC 9(0007)V9(05).
             05 REC10-DT-EFF-MOVI               PIC X(008).

       01 AREA-CACHE-8.
             05 REC8-NUM-QUO-DISINV-COMMGES     PIC 9(0007)V9(05).
             05 REC8-CNTRVAL-PREC               PIC 9(0012)V9(03).
             05 REC8-CNTRVAL-ATTUALE            PIC 9(0012)V9(03).
             05 REC8-NUM-QUO-INV-SWITCH         PIC 9(0007)V9(05).
             05 REC8-NUM-QUO-INVST-VERS         PIC 9(0007)V9(05).
             05 REC8-NUM-QUO-INVST-REBATE       PIC 9(0007)V9(05).
             05 REC8-NUM-QUO-DISINV-SWITCH      PIC 9(0007)V9(05).
             05 REC8-NUM-QUO-DISINV-PREL-COSTI  PIC 9(0007)V9(05).
13815        05 REC8-NUM-QUO-DISINV-COMP-NAV    PIC 9(0007)V9(05).
13815        05 REC8-NUM-QUO-INVST-COMP-NAV     PIC 9(0007)V9(05).

       01 AREA-CACHE-9.
12193     03 ELE-MAX-CACHE-9             PIC 9(9).
12193     03 ELE-CACHE-9                 OCCURS 5.
12193        05 REC9-IMP-LRD-RISC-PARZ        PIC 9(0012)V9(03).
12193        05 REC9-RP-TP-MOVI                 PIC 9(005).
12193        05 REC9-DT-EFFETTO-LIQ             PIC X(008).
      *----------------------------------------------------------------*
      *    AREA TRACCIATI RECORD                                       *
      *----------------------------------------------------------------*
      *    Flusso dati ESTRATTO CONTO
      *    Area File di Input (OUTPUT ESTRATTO CONTO)
      *----------------------------------------------------------------*
       01  WRIN-REC-FILEIN.
           COPY LRGC0031                 REPLACING ==(SF)== BY ==WRIN==.
      *----------------------------------------------------------------*
      *    Area File di Output (FILESQS1)
      *----------------------------------------------------------------
       01  AREA-LRGC0031.
           COPY LRGC0031                 REPLACING ==(SF)== BY ==WOUT==.

      *----------------------------------------------------------------*
      *    Area File IMPOSTA SOSTITUTIVA
      *----------------------------------------------------------------
       01  AREA-IMPSOST.
           COPY LOAR0331.
      ******************************************************************
      * --> Area Servizio
      ******************************************************************
       01  AREA-IO-ISPS0580.
           COPY ISPC0580.
      *
       01  AREA-IO-ISPS0590.
           COPY ISPC0590.
      *----------------------------------------------------------------*
      *    COPY GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
           COPY IEAV9903.
      *----------------------------------------------------------------*
      *    COPY GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY LRGC0661.
      *--  COPY PER SERVIZIO VERIFICA CALCOLI SU DATE
           COPY LCCC0003.
      *----------------------------------------------------------------*
      *    COPY DATA DISPATCHER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      *--  COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *--  COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *----------------------------------------------------------------*
      *     AREA WORKING TABELLE DB2
      *----------------------------------------------------------------*
      *01  WORK-COMMAREA.

      *-----------------------------------------------------------------
       LINKAGE SECTION.
      *-----------------------------------------------------------------
      *  CAMPI DI ESITO, AREE DI SERVIZIO
      *-----------------------------------------------------------------
       01  AREA-IDSV0001.
           COPY IDSV0001.

      *  -- AREA INFRASTRUTTURALE
       01 WCOM-IO-STATI.
           03 WCOM-AREA-STATI.
              COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.
      *----------------------------------------------------------------*
      * AREA DATI EXTRA IN AMBITO DI BATCH
      *----------------------------------------------------------------*
       01  IABV0006.
           COPY IABV0006.
      *----------------------------------------------------------------*
      * AREA DATI BLOB
      *----------------------------------------------------------------*
       01 WK-APPO-DATA-JOB.
          05 WK-APPO-ELE-MAX                     PIC 9(0003).
          05 WK-APPO-BLOB-DATA                OCCURS 1000 TIMES.
             10 WK-APPO-BLOB-DATA-REC            PIC X(2500).

      *01 AREA-CONTATORI.
      *   03 WS-NUM-LETTI                PIC 9(09).
      *   03 WS-NUM-SCART                PIC 9(09).
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-IO-STATI
                                IABV0006
                                WK-APPO-DATA-JOB.
      *                         AREA-CONTATORI.
      *----------------------------------------------------------------*
           IF IABV0006-ULTIMO-LANCIO

              PERFORM CHIUSURA-FILE
                 THRU CHIUSURA-FILE-EX

              COMPUTE  IABV0006-CUSTOM-COUNT(REC-PERC) =
                     (100 * (IABV0006-CUSTOM-COUNT(REC-ELABORATI)
                       - IABV0006-CUSTOM-COUNT(REC-SCARTATI)))
                     / IABV0006-CUSTOM-COUNT(REC-ELABORATI)

           ELSE
              PERFORM S0000-OPERAZIONI-INIZIALI
                 THRU EX-S0000

              IF IDSV0001-ESITO-OK
                 PERFORM S8000-ELABORA
                    THRU EX-S8000
              END-IF

              PERFORM S9000-OPERAZIONI-FINALI
                 THRU EX-S9000
           END-IF.

           GOBACK.
      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.
      *
           MOVE 'S0000-OPERAZIONI-INIZIALI'
             TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY
              THRU ESEGUI-DISPLAY-EX.
      *
           INITIALIZE WS-ERRORE
                      TRACCIATO-FILE.
      *
           PERFORM S0001-INIZIALIZZA
              THRU EX-S0001
      *
      *--> COPY PER L'INIZIALIZZAZIONE AREA ERRORI
      *--> DELL'AREA CONTESTO.
           COPY IERP0001.
      *
           SET PRIMO-RECORD-NO    TO TRUE
      *
      *--> APERTURA FILE DI OUTPUT
           IF IDSV0001-ESITO-OK
              IF IABV0006-PRIMO-LANCIO
                 PERFORM S8100-APERTURA-FLUSSI
                    THRU EX-S8100
      *
                 IF IDSV0001-ESITO-OK
                    PERFORM S8120-SCRIVI-TST
                       THRU EX-S8120
                 END-IF
      *
                 IF IDSV0001-ESITO-OK
                    PERFORM S8140-APERTURA-GRAV-ERR
                       THRU EX-S8140
                 END-IF
              END-IF
           END-IF.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *   INIZIALIZZAZIONE VARIABILI
      *----------------------------------------------------------------*
       S0001-INIZIALIZZA.
           MOVE 'S0001-INIZIALIZZA'
             TO IDSV8888-AREA-DISPLAY.

           INITIALIZE                       IX-INDICI
                                            WS-VARIABILI
                                            ELE-MAX-CACHE-3
                                            ELE-MAX-CACHE-10
                                            AREA-CACHE-8
                                            AREA-CACHE-9
                                            AREA-CACHE-10.

           SET CNTRL-37-ERR-NO TO TRUE.
           SET CNTRL-34-ERR-NO TO TRUE.
           SET INTESTAZIONE-SI TO TRUE.

       EX-S0001.
           EXIT.
      *----------------------------------------------------------------*
      *   Elaboriamo l'INPUT (OUTPUT del processo di Estratto Conto)   *
      *   e lo riscriviamo dopo opportuni controlli                    *
      *----------------------------------------------------------------*
       S8000-ELABORA.

           MOVE 'S8000-ELABORA'
             TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY
              THRU ESEGUI-DISPLAY-EX.

           SET WRITE-SI TO TRUE
           SET PRIMO-RECORD-NO TO TRUE
      *--> FASE DI CREAZIONE DELLE CACHE
           IF IDSV0001-ESITO-OK
              PERFORM S9600-SCORRI-INPUT
                 THRU EX-S9600
              VARYING IX-IND FROM 1 BY 1
                UNTIL IX-IND > WK-APPO-ELE-MAX
                   OR IDSV0001-ESITO-KO
           END-IF

      *--> FASE DEI CONTROLLI
           IF IDSV0001-ESITO-OK
           AND PRIMO-RECORD-NO
              PERFORM S9700-CONTROLLI
                 THRU EX-S9700
           END-IF

           IF WRITE-SI
           OR PRIMO-RECORD-SI
      *--> FASE DI SCRITTURA DELL'E/C CORRETTI
              PERFORM W100-SCRIVI-FILE
                 THRU EX-W100
              VARYING IX-IND FROM 1 BY 1
                UNTIL IX-IND > WK-APPO-ELE-MAX
                   OR IDSV0001-ESITO-KO
           ELSE
      *--> FASE DI SCRITTURA SCARTI
              PERFORM W200-SCRIVI-SCARTI
                 THRU EX-W200
      *--> FASE DI SCRITTURA DELL'E/C KO
              PERFORM W999-SCRIVI-FILE-KO
                 THRU EX-W999
              VARYING IX-IND FROM 1 BY 1
                UNTIL IX-IND > WK-APPO-ELE-MAX
                   OR IDSV0001-ESITO-KO
           END-IF.

       EX-S8000.
           EXIT.

      *----------------------------------------------------------------*
      *   APERTURA DEI FILE DI INPUT/OUTPUT                            *
      *----------------------------------------------------------------*
       S8100-APERTURA-FLUSSI.

           MOVE 'S8100-APERTURA-FLUSSI'                 TO WK-LABEL.

           SET OPEN-FILESQS-OPER             TO TRUE

           SET OPEN-FILESQS1-SI              TO TRUE
           SET OPEN-FILESQS2-SI              TO TRUE
           SET OPEN-FILESQS3-SI              TO TRUE
           SET OPEN-FILESQS4-NO              TO TRUE
           SET OPEN-FILESQS6-SI              TO TRUE

           SET OUTPUT-FILESQS1               TO TRUE
           SET OUTPUT-FILESQS2               TO TRUE
           SET OUTPUT-FILESQS3               TO TRUE
           SET OUTPUT-FILESQS6               TO TRUE


           PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX

           IF NOT FILE-STATUS-OK
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
              MOVE '005166'                  TO IEAI9901-COD-ERRORE
              MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
              MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                                             TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S8100.
           EXIT.
      *----------------------------------------------------------------*
      *   Scorriamo l'INPUT (OUTPUT del processo di Estratto Conto)    *
      *   e totalizziamo alcuni valori                                 *
      *----------------------------------------------------------------*
       S9600-SCORRI-INPUT.

           MOVE 'S9600-SCORRI-INPUT'                 TO WK-LABEL.


           INITIALIZE WRIN-REC-FILEIN.
           MOVE WK-APPO-BLOB-DATA-REC(IX-IND)        TO WRIN-REC-FILEIN.

           EVALUATE WRIN-TP-RECORD

               WHEN '0000'
                     SET PRIMO-RECORD-SI TO TRUE
                     INITIALIZE DATE-WORK
                     MOVE WRIN-DATA-RICH-DA TO WK-DT-RICOR-DA-X
                     MOVE WRIN-DATA-RICH-A  TO WK-DT-RICOR-A-X
                     MOVE WRIN-ANNO-RIF     TO WK-ANNO-RIF-X
MOD                  IF (WK-ANNO-RIF > 2014)
MOD                     SET CNTRL-34-NO TO TRUE
MOD                  ELSE
MOD                     SET CNTRL-34-SI TO TRUE
MOD                  END-IF
                     MOVE WRIN-TP-INVST      TO TIPOLOGIA-EC
                     IF TP-ESTR-RV
                        PERFORM S100-CALL-ACTUATOR
                           THRU S100-CALL-ACTUATOR-EX
                     END-IF
                     IF IDSV0001-ESITO-OK
                        PERFORM W999-SCRIVI-FILE-KO
                           THRU EX-W999
                     END-IF

               WHEN '0001'
                     INITIALIZE              WK-AREA-FILE
                     MOVE 0                  TO WK-TASSO-TEC-RV
ALEX                 DISPLAY 'LRGS0660 - POLIZZA:' WRIN-IB-POLI-IND
ALEX                 DISPLAY 'LRGS0660 - ID-POLI:' WRIN-ID-POLI
                     MOVE WRIN-ID-POLI       TO WK-ID-POLI
                     MOVE WRIN-ID-ADES       TO WK-ID-ADES
                     MOVE WRIN-IB-POLI-IND   TO IABV0006-IB-OGG-POLI
                                                OUT-IB-OGG
                                                WK-IB-OGG
                     MOVE WRIN-DT-RIF-DA     TO WK-DT-INF
                     MOVE WRIN-DT-RIF-A      TO WK-DT-SUP
                     MOVE WRIN-IB-ADES       TO IABV0006-IB-OGG-ADES
                     MOVE WRIN-DT-DECOR-POLI
                       TO WK-DT-DECOR
                     ADD 1     TO IABV0006-CUSTOM-COUNT(REC-ELABORATI)
                     MOVE 0    TO IX-C37-TAB
                     MOVE 0    TO IX-MAX-C37
                     MOVE 0    TO IX-MAX-C35
                     SET CNTRL-34-ERR-NO TO TRUE
                     SET CNTRL-37-ERR-NO TO TRUE
               WHEN '0002'
                     MOVE WRIN-COGNOME-ASSICU
                       TO REC2-COGNOME-ASS

                     MOVE WRIN-NOME-ASSICU
                       TO REC2-NOME-ASS

               WHEN '0003'
      *--->    MEMORIZZAZIONE DEL RECORD 3
                     IF TP-ESTR-RV
ALEX1 *                 IF WRIN-TP-GAR = 1
ALEX1                   IF WRIN-TP-GAR = ( 1 OR 5 )
                        AND WRIN-TP-INVST-GAR = 4
                           MOVE WRIN-FRAZIONAMENTO
                             TO WK-FRAZ
                        END-IF
                     END-IF
                     PERFORM C100-CACHE-REC-3
                        THRU EX-C100
      *
      *--> FLAG PER CONTROLLO 34
                   IF  (WRIN-COD-GAR = 'TRVU1'
                        OR WRIN-COD-GAR = 'TRVUD')
                       MOVE WK-ANNO-RIF
                         TO WK-DT-FIN-AA
                       MOVE 12 TO  WK-DT-INI-MM  WK-DT-FIN-MM
                       MOVE 31 TO  WK-DT-INI-GG  WK-DT-FIN-GG
                       COMPUTE WK-DT-INI-AA = WK-ANNO-RIF - 1
                       PERFORM VARYING IX-CNTR-34 FROM 1 BY 1
                                 UNTIL IX-CNTR-34 > 12
                                   OR CNTRL-34-ERR-SI
      *--> DT-fin-PER-INV >= dt_ini_elab
      *--> && DT_INI_PER_INV <= DT_END_ELAB
                       IF  WRIN-DT-INI-PER-INV(IX-CNTR-34) NOT EQUAL
                           SPACES AND LOW-VALUES AND HIGH-VALUES
                         IF  WRIN-DT-FIN-PER-INV(IX-CNTR-34) >=
                             WK-INI-RIC
                         AND WRIN-DT-INI-PER-INV(IX-CNTR-34) <=
                             WK-FIN-RIC
                             CONTINUE
                         ELSE
                             SET CNTRL-34-ERR-SI TO TRUE
                         END-IF
                       ELSE
                         IF IX-CNTR-34 < 2
                            SET CNTRL-34-ERR-SI TO TRUE
                         END-IF
                       END-IF
                       END-PERFORM
                   ELSE
                      CONTINUE
                   END-IF

               WHEN '0005'
                    MOVE IX-IND              TO IX-REC-5

               WHEN '0008'
      *--->    MEMORIZZAZIONE DELLE SOMME DEL RECORD 8
                     PERFORM C300-CACHE-REC-8
                        THRU EX-C300

                     ADD 1 TO IX-C35-TAB

                     MOVE WRIN-ID-GAR-FND
                       TO C35-ID-GAR(IX-C35-TAB)

                     MOVE WRIN-CNTRVAL-PREC
                       TO C35-CNTRVAL-PREC(IX-C35-TAB)

                     MOVE IX-C35-TAB TO IX-MAX-C35

                     SET TROVATO-37-NO TO TRUE

13067 *              MOVE IX-C37-TAB TO IX-MAX-C37
                     PERFORM VARYING IX-C37-TAB FROM 1 BY 1
                       UNTIL IX-C37-TAB > IX-MAX-C37

                         IF WRIN-COD-FND =
                             C37-COD-FND(IX-C37-TAB)

                             SET TROVATO-37-SI TO TRUE

                          COMPUTE C37-NUM-QUO-PREC(IX-C37-TAB) =
                                  WRIN-NUM-QUO-PREC +
                              C37-NUM-QUO-PREC(IX-C37-TAB)

ALEX1               COMPUTE C37-NUM-QUO-DISINV-RISC-PARZ(IX-C37-TAB)=
ALEX1                       WRIN-NUM-QUO-DISINV-RISC-PARZ +
ALEX1                     C37-NUM-QUO-DISINV-RISC-PARZ(IX-C37-TAB)

                    COMPUTE C37-NUM-QUO-DIS-RISC-PARZ-PRG(IX-C37-TAB)=
                            WRIN-NUM-QUO-DIS-RISC-PARZ-PRG +
                          C37-NUM-QUO-DIS-RISC-PARZ-PRG(IX-C37-TAB)


                    COMPUTE C37-NUM-QUO-DISINV-IMPOS-SOST(IX-C37-TAB)=
                              WRIN-NUM-QUO-DISINV-IMPOS-SOST +
                            C37-NUM-QUO-DISINV-IMPOS-SOST(IX-C37-TAB)


                    COMPUTE C37-NUM-QUO-DISINV-COMMGES(IX-C37-TAB)=
                             WRIN-NUM-QUO-DISINV-COMMGES +
                        C37-NUM-QUO-DISINV-COMMGES(IX-C37-TAB)


                    COMPUTE C37-NUM-QUO-DISINV-PREL-COSTI(IX-C37-TAB)=
                             WRIN-NUM-QUO-DISINV-PREL-COSTI +
                       C37-NUM-QUO-DISINV-PREL-COSTI(IX-C37-TAB)


                    COMPUTE C37-NUM-QUO-DISINV-SWITCH(IX-C37-TAB)=
                           WRIN-NUM-QUO-DISINV-SWITCH +
                        C37-NUM-QUO-DISINV-SWITCH(IX-C37-TAB)


                    COMPUTE C37-NUM-QUO-INVST-SWITCH(IX-C37-TAB)=
                         WRIN-NUM-QUO-INVST-SWITCH +
                      C37-NUM-QUO-INVST-SWITCH(IX-C37-TAB)


                    COMPUTE C37-NUM-QUO-INVST-VERS(IX-C37-TAB)=
                       WRIN-NUM-QUO-INVST-VERS +
                      C37-NUM-QUO-INVST-VERS(IX-C37-TAB)


                    COMPUTE C37-NUM-QUO-INVST-REBATE(IX-C37-TAB)=
                             WRIN-NUM-QUO-INVST-REBATE +
                           C37-NUM-QUO-INVST-REBATE(IX-C37-TAB)


                    COMPUTE C37-NUM-QUO-INVST-PURO-RISC(IX-C37-TAB)=
                             WRIN-NUM-QUO-INVST-PURO-RISC +
                         C37-NUM-QUO-INVST-PURO-RISC(IX-C37-TAB)

                    COMPUTE C37-NUM-QUO-ATTUALE(IX-C37-TAB) =
                              WRIN-NUM-QUO-ATTUALE +
                            C37-NUM-QUO-ATTUALE(IX-C37-TAB)

13815               COMPUTE C37-NUM-QUO-DISINV-COMP-NAV(IX-C37-TAB)=
13815                        WRIN-NUM-QUO-DISINV-COMP-NAV +
13815                   C37-NUM-QUO-DISINV-COMP-NAV(IX-C37-TAB)

13815               COMPUTE C37-NUM-QUO-INVST-COMP-NAV(IX-C37-TAB) =
13815                        WRIN-NUM-QUO-INVST-COMP-NAV +
13815                      C37-NUM-QUO-INVST-COMP-NAV(IX-C37-TAB)

                          END-IF
      *
                        END-PERFORM

                     IF TROVATO-37-NO

                        MOVE WRIN-COD-FND
                          TO C37-COD-FND(IX-C37-TAB)

                        MOVE WRIN-NUM-QUO-PREC
                          TO C37-NUM-QUO-PREC(IX-C37-TAB)

                   MOVE WRIN-NUM-QUO-DISINV-RISC-PARZ
                     TO C37-NUM-QUO-DISINV-RISC-PARZ(IX-C37-TAB)
                   MOVE WRIN-NUM-QUO-DIS-RISC-PARZ-PRG
                     TO C37-NUM-QUO-DIS-RISC-PARZ-PRG(IX-C37-TAB)
                   MOVE WRIN-NUM-QUO-DISINV-IMPOS-SOST
                     TO C37-NUM-QUO-DISINV-IMPOS-SOST(IX-C37-TAB)
                   MOVE WRIN-NUM-QUO-DISINV-COMMGES
                     TO C37-NUM-QUO-DISINV-COMMGES(IX-C37-TAB)
                   MOVE WRIN-NUM-QUO-DISINV-PREL-COSTI
                     TO C37-NUM-QUO-DISINV-PREL-COSTI(IX-C37-TAB)
                   MOVE WRIN-NUM-QUO-DISINV-SWITCH
                     TO C37-NUM-QUO-DISINV-SWITCH(IX-C37-TAB)
                   MOVE WRIN-NUM-QUO-INVST-SWITCH
                     TO C37-NUM-QUO-INVST-SWITCH(IX-C37-TAB)
                   MOVE WRIN-NUM-QUO-INVST-VERS
                     TO C37-NUM-QUO-INVST-VERS(IX-C37-TAB)
                   MOVE WRIN-NUM-QUO-INVST-REBATE
                     TO C37-NUM-QUO-INVST-REBATE(IX-C37-TAB)
                   MOVE WRIN-NUM-QUO-INVST-PURO-RISC
                     TO C37-NUM-QUO-INVST-PURO-RISC(IX-C37-TAB)
                   MOVE WRIN-NUM-QUO-ATTUALE
                     TO C37-NUM-QUO-ATTUALE(IX-C37-TAB)
13815              MOVE WRIN-NUM-QUO-DISINV-COMP-NAV
13815                TO C37-NUM-QUO-DISINV-COMP-NAV(IX-C37-TAB)
13815              MOVE WRIN-NUM-QUO-INVST-COMP-NAV
13815                TO C37-NUM-QUO-INVST-COMP-NAV(IX-C37-TAB)

                        MOVE IX-C37-TAB TO IX-MAX-C37

                    END-IF


      *--> FLAG PER CONTROLLO 37
      *             COMPUTE WK-CNTRL-37 =
      *                     (WRIN-NUM-QUO-PREC -
      *                      WRIN-NUM-QUO-DISINV-RISC-PARZ -
      *                      WRIN-NUM-QUO-DIS-RISC-PARZ-PRG -
      *                      WRIN-NUM-QUO-DISINV-IMPOS-SOST -
      *                      WRIN-NUM-QUO-DISINV-COMMGES -
      *                      WRIN-NUM-QUO-DISINV-PREL-COSTI -
      *                      WRIN-NUM-QUO-DISINV-SWITCH +
      *                      WRIN-NUM-QUO-INVST-SWITCH +
      *                      WRIN-NUM-QUO-INVST-VERS +
      *                      WRIN-NUM-QUO-INVST-REBATE  +
      *                      WRIN-NUM-QUO-INVST-PURO-RISC) -
      *                      WRIN-NUM-QUO-ATTUALE
      *
      *--> FLAG PER CONTROLLO 37
      *            IF WK-CNTRL-37 > 0,01
      *            OR WK-CNTRL-37 < -0,01
      *               SET CNTRL-37-ERR-SI TO TRUE
      *            ELSE
      *               CONTINUE
      *            END-IF
      *            END-IF

               WHEN '0009'
      *--> PER IL CONTROLLO 38 VIENE LASCITO IL CUMULO DEL RISCATTO
      *--> DI PRIMA
12193                 COMPUTE REC9-IMP-LORDO-RISC-PARZ =
12193                         REC9-IMP-LORDO-RISC-PARZ +
12193                          WRIN-IMP-LORDO-RISC-PARZ
12193              PERFORM C195-CACHE-REC-9
12193                 THRU EX-C915

               WHEN '0010'
      *--->    MEMORIZZAZIONE DEL RECORD 10
                     PERFORM C200-CACHE-REC-10
                        THRU EX-C200

               WHEN OTHER
                    CONTINUE

           END-EVALUATE.

       EX-S9600.
           EXIT.
12193 *---------------------------------------------------------------*
12193 *--> CACHE DEL RECORD 9 IN BASE AL TIPO MOVIMENTO
12193 *----------------------------------------------------------------*
12193  C195-CACHE-REC-9.
12193      MOVE 'C195-CACHE-REC-9'
12193         TO WK-LABEL

12193      SET TROVATO-MOVI-NO           TO TRUE
12193      PERFORM VARYING IX-REC-9 FROM 1 BY 1
12193        UNTIL IX-REC-9 > ELE-MAX-CACHE-9
12193           OR TROVATO-MOVI-SI
12193          IF WRIN-RP-TP-MOVI = REC9-RP-TP-MOVI(IX-REC-9)
12193             ADD WRIN-IMP-LORDO-RISC-PARZ
12193               TO REC9-IMP-LRD-RISC-PARZ(IX-REC-9)
12193             SET TROVATO-MOVI-SI          TO TRUE
12193          END-IF
12193      END-PERFORM

12193      IF TROVATO-MOVI-NO
12193         ADD 1 TO ELE-MAX-CACHE-9
12193         MOVE ELE-MAX-CACHE-9
12193            TO IX-REC-9
12193         MOVE WRIN-DT-EFFETTO-LIQ
12193            TO REC9-DT-EFFETTO-LIQ(IX-REC-9)
12193         MOVE WRIN-RP-TP-MOVI
12193            TO REC9-RP-TP-MOVI(IX-REC-9)
12193         MOVE WRIN-IMP-LORDO-RISC-PARZ
12193            TO REC9-IMP-LRD-RISC-PARZ(IX-REC-9)
12193      END-IF.
12193  EX-C915.
12193      EXIT.
      *----------------------------------------------------------------*
      *   CREAZIONE CACHE X IL RECORD 3                                *
      *----------------------------------------------------------------*
       C100-CACHE-REC-3.

           MOVE 'C100-CACHE-REC-3'                   TO WK-LABEL.

           ADD 1 TO IX-REC-3

           MOVE WRIN-ID-GAR
             TO REC3-ID-GAR(IX-REC-3)
           MOVE WRIN-COD-GAR
             TO REC3-COD-GAR(IX-REC-3)
           MOVE WRIN-TP-GAR
             TO REC3-TP-GAR(IX-REC-3)
           MOVE WRIN-TP-INVST-GAR
             TO REC3-TP-INVST(IX-REC-3)
           MOVE WRIN-CUM-PREM-INVST-AA-RIF
             TO REC3-CUM-PREM-INVST-AA-RIF(IX-REC-3)
           MOVE WRIN-CUM-PREM-VERS-AA-RIF
             TO REC3-CUM-PREM-VERS-AA-RIF(IX-REC-3)
           MOVE WRIN-PREST-MATUR-AA-RIF
             TO REC3-PREST-MATUR-AA-RIF(IX-REC-3)
           MOVE WRIN-CAP-LIQ-GA
             TO REC3-CAP-LIQ-GA(IX-REC-3)
           MOVE WRIN-RISC-TOT-GA
             TO REC3-RISC-TOT-GA(IX-REC-3)
           MOVE WRIN-CUM-PREM-VERS-EC-PREC
             TO REC3-CUM-PREM-VERS-EC-PREC(IX-REC-3)
           MOVE WRIN-PREST-MATUR-EC-PREC
             TO REC3-PREST-MATUR-EC-PREC(IX-REC-3)
           MOVE WRIN-IMP-LRD-RISC-PARZ-AP-GA
             TO REC3-IMP-LRD-RISC-PARZ-AP-GAR(IX-REC-3)
           MOVE WRIN-IMP-LRD-RISC-PARZ-AC-GA
             TO REC3-IMP-LRD-RISC-PARZ-AC-GAR(IX-REC-3)
           MOVE WRIN-TASSO-ANN-RIVAL-PREST-GAR
             TO REC3-TASSO-ANN-RIVAL-PREST-GAR(IX-REC-3)
           MOVE WRIN-TASSO-TECNICO
             TO REC3-TASSO-TECNICO(IX-REC-3)
NEW        IF TP-ESTR-RV
NEW   *    AND WRIN-TP-GAR =  1 AND WRIN-TP-INVST-GAR = 4
ALFR       AND WRIN-TP-GAR =  ( 1 OR 5 ) AND WRIN-TP-INVST-GAR = 4
NEW           MOVE WRIN-TASSO-TECNICO
NEW             TO WK-TASSO-TEC-RV
NEW        END-IF
           MOVE WRIN-REND-LOR-GEST-SEP-GAR
             TO REC3-REND-LOR-GEST-SEP-GAR(IX-REC-3)
           MOVE WRIN-ALIQ-RETROC-RICON-GAR
             TO REC3-ALIQ-RETROC-RICON-GAR(IX-REC-3)
           MOVE WRIN-TASSO-ANN-REND-RETROC-GAR
             TO REC3-TASSO-ANN-REND-RETROC-GAR(IX-REC-3)
           MOVE WRIN-PC-COMM-GEST-GAR
             TO REC3-PC-COMM-GEST-GAR(IX-REC-3)
           MOVE WRIN-REN-MIN-TRNUT
             TO REC3-REN-MIN-TRNUT(IX-REC-3)
           MOVE WRIN-REN-MIN-GARTO
             TO REC3-REN-MIN-GARTO(IX-REC-3).
ALFR       MOVE WRIN-FL-STORNATE-ANN
ALFR         TO REC3-FL-STORNATE-ANN(IX-REC-3).

           MOVE IX-REC-3   TO ELE-MAX-CACHE-3.

       EX-C100.
           EXIT.
      *----------------------------------------------------------------*
      *   CREAZIONE CACHE X LE SOMME DEL RECORD 8                      *
      *----------------------------------------------------------------*
       C300-CACHE-REC-8.

           MOVE 'C300-CACHE-REC-8'                    TO WK-LABEL.

           COMPUTE REC8-CNTRVAL-PREC =
                   REC8-CNTRVAL-PREC
                  + WRIN-CNTRVAL-PREC

           COMPUTE REC8-CNTRVAL-ATTUALE =
                   REC8-CNTRVAL-ATTUALE +
                   WRIN-CNTRVAL-ATTUALE

           COMPUTE REC8-NUM-QUO-INVST-VERS =
                   REC8-NUM-QUO-INVST-VERS +
                   WRIN-NUM-QUO-INVST-VERS

           COMPUTE REC8-NUM-QUO-DISINV-COMMGES =
                   REC8-NUM-QUO-DISINV-COMMGES +
                   WRIN-NUM-QUO-DISINV-COMMGES

           COMPUTE REC8-NUM-QUO-DISINV-PREL-COSTI =
                   REC8-NUM-QUO-DISINV-PREL-COSTI +
                   WRIN-NUM-QUO-DISINV-PREL-COSTI

           COMPUTE REC8-NUM-QUO-INVST-REBATE =
                   REC8-NUM-QUO-INVST-REBATE +
                   WRIN-NUM-QUO-INVST-REBATE

           COMPUTE REC8-NUM-QUO-DISINV-SWITCH =
                   REC8-NUM-QUO-DISINV-SWITCH +
                   WRIN-NUM-QUO-DISINV-SWITCH

           COMPUTE REC8-NUM-QUO-INV-SWITCH =
                   REC8-NUM-QUO-INV-SWITCH +
                   WRIN-NUM-QUO-INVST-SWITCH

13815      COMPUTE REC8-NUM-QUO-DISINV-COMP-NAV =
13815              REC8-NUM-QUO-DISINV-COMP-NAV +
13815              WRIN-NUM-QUO-DISINV-COMP-NAV

13815      COMPUTE REC8-NUM-QUO-INVST-COMP-NAV =
13815              REC8-NUM-QUO-INVST-COMP-NAV +
13815              WRIN-NUM-QUO-INVST-COMP-NAV
      *--> SALVATAGGIO DELL'INDICE DEL RECORD 8 X I CONTRLLI

           MOVE IX-IND              TO IX-REC-8.

       EX-C300.
           EXIT.
      *----------------------------------------------------------------*
      *   CREAZIONE CACHE X IL RECORD 10                               *
      *----------------------------------------------------------------*
       C200-CACHE-REC-10.

           MOVE 'C100-CACHE-REC-10'                   TO WK-LABEL.

           ADD 1 TO IX-REC-10

           MOVE WRIN-ID-GARANZIA
             TO REC10-ID-GAR(IX-REC-10)
           MOVE WRIN-DESC-TP-MOVI
             TO REC10-DESC-TP-MOVI(IX-REC-10)
           MOVE WRIN-IMP-OPER
             TO REC10-IMP-OPER(IX-REC-10)
           MOVE WRIN-IMP-VERS
             TO REC10-IMP-VERS(IX-REC-10)
           MOVE WRIN-NUMERO-QUO
             TO REC10-NUMERO-QUO(IX-REC-10)
12193      MOVE WRIN-DT-EFF-MOVI
12193        TO REC10-DT-EFF-MOVI(IX-REC-10)
12193      MOVE WRIN-TP-MOVI
12193        TO REC10-TP-MOVI(IX-REC-10)

           MOVE IX-REC-10   TO ELE-MAX-CACHE-10.

       EX-C200.
           EXIT.

      *----------------------------------------------------------------*
      *   CONTROLLI                                                    *
      *----------------------------------------------------------------*
       S9700-CONTROLLI.

           MOVE 'S9700-CONTROLLI'                   TO WK-LABEL.

           PERFORM S9710-CONTROLLI-COMUNI
              THRU EX-S9710.

           SET CONTROLLO-OK TO TRUE

           IF  CONTROLLO-OK
           AND IDSV0001-ESITO-OK
              PERFORM S9720-CONTROLLI-AD-HOC
                 THRU EX-S9720
           END-IF.

       EX-S9700.
           EXIT.

      *----------------------------------------------------------------*
      *   CONTROLLI COMUNI                                             *
      *----------------------------------------------------------------*
       S9710-CONTROLLI-COMUNI.

           MOVE 'S9710-CONTROLLI-COMUNI'          TO WK-LABEL.

      *--> CONTROLLI SUL RECORD 1

           MOVE WK-APPO-BLOB-DATA-REC(1)        TO WRIN-REC-FILEIN
           SET CONTROLLO-OK TO TRUE

           PERFORM C0001-CNTRL-1
              THRU EX-C0001

           IF CONTROLLO-OK
              PERFORM C0002-CNTRL-2
                 THRU EX-C0002
           END-IF


           IF CONTROLLO-OK
              PERFORM C0030-CNTRL-3-12
                 THRU EX-C0030
           END-IF.


           IF CONTROLLO-OK
              PERFORM C0003-CNTRL-6-7-14
                 THRU EX-C0003
           END-IF

           IF CONTROLLO-OK
              PERFORM C0004-CNTRL-8-9-18
                 THRU EX-C0004
           END-IF

           IF CONTROLLO-OK
              PERFORM C0005-CNTRL-10-19
                 THRU EX-C0005
           END-IF

           IF CONTROLLO-OK
              PERFORM C0006-CNTRL-11-15
                 THRU EX-C0006
           END-IF

           IF CONTROLLO-OK
              PERFORM C0007-CNTRL-13-42
                 THRU EX-C0007
           END-IF

           IF CONTROLLO-OK
              PERFORM C0008-CNTRL-16
                 THRU EX-C0008
           END-IF

           IF CONTROLLO-OK
              PERFORM C0009-CNTRL-17
                 THRU EX-C0009
           END-IF

           IF CONTROLLO-OK
              PERFORM C0010-CNTRL-20
                 THRU EX-C0010
           END-IF

           IF CONTROLLO-OK
              PERFORM C0011-CNTRL-21
                 THRU EX-C0011
           END-IF

      *-->

           IF CONTROLLO-OK
              PERFORM C0012-CNTRL-23
                 THRU EX-C0012
           END-IF

           IF CONTROLLO-OK
              PERFORM C0013-CNTRL-33
                 THRU EX-C0013
           END-IF

           IF CONTROLLO-OK
              PERFORM C0014-CNTRL-38
                 THRU EX-C0014
           END-IF

           IF CONTROLLO-OK
              PERFORM C0015-CNTRL-40
                 THRU EX-C0015
           END-IF.

       EX-S9710.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLI AD HOC DIFFERENZIATI PER TP ESTR CONTO             *
      *----------------------------------------------------------------*
       S9720-CONTROLLI-AD-HOC.

           MOVE 'S9720-CONTROLLI-AD-HOC'          TO WK-LABEL.

           IF TP-ESTR-MU
           OR TP-ESTR-RV
              PERFORM S9722-CONTROLLI-MU-RV
                 THRU EX-S9722
           END-IF

           IF TP-ESTR-MU
           OR TP-ESTR-UL
              PERFORM S9724-CONTROLLI-MU-UL
                 THRU EX-S9724
           END-IF

           IF TP-ESTR-MU
              PERFORM S9726-CONTROLLI-MU
                 THRU EX-S9726
           END-IF.

       EX-S9720.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLI PER MULTIRAMO E RIVALUTABILI                       *
      *----------------------------------------------------------------*
       S9722-CONTROLLI-MU-RV.

           MOVE 'S9722-CONTROLLI-MU-RV'          TO WK-LABEL.

           IF CONTROLLO-OK
              PERFORM C0020-CNTRL-4-5-39
                 THRU EX-C0020
           END-IF.

           IF CONTROLLO-OK
              PERFORM C0022-CNTRL-RIVALUTAZ
                 THRU EX-C0022
           END-IF.

           IF CONTROLLO-OK
              PERFORM C0255-CNTRL-25
                 THRU EX-C0255
           END-IF.

           IF CONTROLLO-OK
              PERFORM C0024-CNTRL-43
                 THRU EX-C0024
           END-IF.

       EX-S9722.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLI PER MULTIRAMO E UNIT LINKED                        *
      *----------------------------------------------------------------*
       S9724-CONTROLLI-MU-UL.

           MOVE 'S9724-CONTROLLI-MU-UL'          TO WK-LABEL.

           IF CONTROLLO-OK
              PERFORM C0032-CNTRL-24
                 THRU EX-C0032
           END-IF.

           IF CONTROLLO-OK
              PERFORM C0034-CNTRL-35
                 THRU EX-C0034
           END-IF.

           IF CONTROLLO-OK
              PERFORM C0036-CNTRL-36
                 THRU EX-C0036
           END-IF.

           IF CONTROLLO-OK
              PERFORM C0038-CNTRL-37
                 THRU EX-C0038
           END-IF.

           IF CONTROLLO-OK
              PERFORM C0040-CNTRL-44
                 THRU EX-C0040
           END-IF.

           IF CONTROLLO-OK
              PERFORM C0042-CNTRL-45
                 THRU EX-C0042
           END-IF.

           IF CONTROLLO-OK
              PERFORM C0044-CNTRL-46
                 THRU EX-C0044
           END-IF.

           IF CONTROLLO-OK
              PERFORM C0046-CNTRL-47
                 THRU EX-C0046
           END-IF.

           IF CONTROLLO-OK
              PERFORM C0048-CNTRL-48
                 THRU EX-C0048
           END-IF.

           IF CONTROLLO-OK
              PERFORM C0050-CNTRL-49
                 THRU EX-C0050
           END-IF.

13815      IF CONTROLLO-OK
13815         PERFORM C0052-CNTRL-50
14235            THRU EX-C0052
13815      END-IF.

13815      IF CONTROLLO-OK
13815         PERFORM C0054-CNTRL-51
14235            THRU EX-C0054
13815      END-IF.

       EX-S9724.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLI PER MULTIRAMO                                      *
      *----------------------------------------------------------------*
       S9726-CONTROLLI-MU.

           MOVE 'S9726-CONTROLLI-MU'          TO WK-LABEL.

           IF CONTROLLO-OK
              PERFORM C0060-CNTRL-34
                 THRU EX-C0060
           END-IF.

       EX-S9726.
           EXIT.
      *----------------------------------------------------------------*
      *    SCRITTURA FILE DI OUTPUT OK
      *----------------------------------------------------------------*
       W100-SCRIVI-FILE.
      *
           MOVE 'W100-SCRIVI-FILE'
             TO IDSV8888-AREA-DISPLAY.
      *
           INITIALIZE WRIN-REC-FILEIN.
      *
           MOVE WK-APPO-BLOB-DATA-REC(IX-IND)
             TO WRIN-REC-FILEIN.
      *
           INITIALIZE FILESQS1-REC
                      WOUT-REC-OUTPUT.
      *
           MOVE WRIN-REC-OUTPUT              TO WOUT-REC-OUTPUT
           MOVE AREA-LRGC0031                TO FILESQS1-REC
           SET WRITE-FILESQS-OPER            TO TRUE
           SET WRITE-FILESQS1-SI             TO TRUE

           PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX

           IF SEQUENTIAL-ESITO-KO
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
              MOVE '005166'                  TO IEAI9901-COD-ERRORE
              MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
              MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                                             TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.
      *
       EX-W100.
           EXIT.
      *----------------------------------------------------------------*
      *    SCRITTURA FILE DI OUTPUT KO FORMATO E/C
      *----------------------------------------------------------------*
       W999-SCRIVI-FILE-KO.
      *
           MOVE 'W999-SCRIVI-FILE-KO'
             TO IDSV8888-AREA-DISPLAY.
      *
           INITIALIZE WRIN-REC-FILEIN.
      *
           MOVE WK-APPO-BLOB-DATA-REC(IX-IND)
             TO WRIN-REC-FILEIN.
      *
           INITIALIZE FILESQS6-REC.
           INITIALIZE  WOUT-REC-OUTPUT.
      *
           MOVE WRIN-REC-OUTPUT              TO WOUT-REC-OUTPUT
           MOVE AREA-LRGC0031                TO FILESQS6-REC
           SET WRITE-FILESQS-OPER            TO TRUE
           SET WRITE-FILESQS6-SI             TO TRUE

           PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX

           IF SEQUENTIAL-ESITO-KO
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
              MOVE '005166'                  TO IEAI9901-COD-ERRORE
              MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
              MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                                             TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.
      *
       EX-W999.
           EXIT.
      *----------------------------------------------------------------*
      *    SCRITTURA FILE DI OUTPUT KO
      *----------------------------------------------------------------*
       W200-SCRIVI-SCARTI.
      *
           MOVE ' W200-SCRIVI-SCARTI'
             TO IDSV8888-AREA-DISPLAY.
      *
      *    COMPUTE WS-NUM-SCART = WS-NUM-SCART + 1
      *
           ADD 1     TO IABV0006-CUSTOM-COUNT(REC-SCARTATI)
           INITIALIZE FILESQS2-REC
           MOVE WK-AREA-FILE                 TO FILESQS2-REC
           SET WRITE-FILESQS-OPER            TO TRUE
           SET WRITE-FILESQS2-SI             TO TRUE

           PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX

           IF SEQUENTIAL-ESITO-KO
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
              MOVE '005166'                  TO IEAI9901-COD-ERRORE
              MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
              MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                                             TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.


       EX-W200.
           EXIT.
      *----------------------------------------------------------------*
      *    SCRITTURA SCARTO
      *----------------------------------------------------------------*
       E001-SCARTO.
      *
           MOVE 'E001-SCARTO'
             TO IDSV8888-AREA-DISPLAY.
           MOVE SPACES TO FILESQS3-REC.

           PERFORM VARYING IX-GRAV FROM 1 BY 1
             UNTIL IX-GRAV > ELE-MAX-ERRORI
                OR CONTROLLO(IX-GRAV) = WK-CONTROLLO
                OR CONTROLLO(IX-GRAV) > WK-CONTROLLO
           END-PERFORM

           IF CONTROLLO(IX-GRAV) = WK-CONTROLLO
              IF GRAVITA(IX-GRAV) = 3
                 SET WRITE-NO  TO TRUE
              ELSE
                 CONTINUE
              END-IF
           END-IF

           MOVE WS-ERRORE                    TO OUT-ERR
           MOVE IABV0006-IB-OGG-POLI         TO OUT-IB-OGG
           MOVE ';'                          TO FILLER-1
                                                FILLER-2
                                                FILLER-3
                                                FILLER-4
                                                FILLER-5
                                                FILLER-6
                                                FILLER-7

           MOVE TRACCIATO-FILE               TO FILESQS3-REC

           SET WRITE-FILESQS-OPER            TO TRUE
           SET WRITE-FILESQS3-SI             TO TRUE

           PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX

           IF SEQUENTIAL-ESITO-KO
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
              MOVE '005166'                  TO IEAI9901-COD-ERRORE
              MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
              MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                                             TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

           INITIALIZE WS-ERRORE
                      TRACCIATO-FILE.

       EX-E001.
           EXIT.
      *----------------------------------------------------------------*
      *    SCRITTURA INTESTAZIONE FILE DI SCARTO
      *----------------------------------------------------------------*
       S8120-SCRIVI-TST.
      *
           MOVE 'S8120-SCRIVI-TST'
             TO IDSV8888-AREA-DISPLAY.

           MOVE SPACES TO FILESQS3-REC.

           STRING 'IB-OGGETTO;NUMERO CNTRL;DESC;CAMPO-1;CAMPO-2;CAMPO-3'
                  ';DATA-1;DATA-2'
           DELIMITED BY SIZE
           INTO  FILESQS3-REC

           SET WRITE-FILESQS-OPER            TO TRUE
           SET WRITE-FILESQS3-SI             TO TRUE

           PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX

           IF SEQUENTIAL-ESITO-KO
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
              MOVE '005166'                  TO IEAI9901-COD-ERRORE
              MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
              MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                                             TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.


       EX-S8120.
           EXIT.
      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           MOVE 'S9000-OPERAZIONI-FINALI'
             TO IDSV8888-AREA-DISPLAY.
      *
           PERFORM ESEGUI-DISPLAY
              THRU ESEGUI-DISPLAY-EX.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      * CLOSE FILE SEQUENZIALE
      *----------------------------------------------------------------*
       CHIUSURA-FILE.
           MOVE 'CHIUSURA-FILE'              TO WK-LABEL.
           MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY            THRU ESEGUI-DISPLAY-EX.

           SET CLOSE-FILESQS-OPER            TO TRUE
           SET CLOSE-FILESQS1-SI             TO TRUE
           SET CLOSE-FILESQS2-SI             TO TRUE
           SET CLOSE-FILESQS3-SI             TO TRUE
           SET CLOSE-FILESQS4-NO             TO TRUE
           SET CLOSE-FILESQS6-SI             TO TRUE

           PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX

           IF NOT FILE-STATUS-OK
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL                  TO IEAI9901-LABEL-ERR
              MOVE '005166'                  TO IEAI9901-COD-ERRORE
              MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
              MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                                             TO IEAI9901-PARAMETRI-ERR
             PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       CHIUSURA-FILE-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    SERVIZIO ACTUATOR PER RECUPERO TASSI MINIMI
      *----------------------------------------------------------------*
       S100-CALL-ACTUATOR.
      *
           PERFORM S110-PREP-AREA-ISPS0580
              THRU S110-PREP-AREA-ISPS0580-EX.
      *
           PERFORM S120-CALL-ISPS0580
              THRU S120-CALL-ISPS0580-EX.
      *
           IF IDSV0001-ESITO-OK
              PERFORM S130-PREP-AREA-ISPS0590
                 THRU S130-PREP-AREA-ISPS0590-EX
      *
              PERFORM S140-CALL-ISPS0590
                 THRU S140-CALL-ISPS0590-EX
           END-IF.
      *
       S100-CALL-ACTUATOR-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  VALORIZZAZIONE CAMPI DI INPUT DA PASSARE AL SERVIZIO          *
      *----------------------------------------------------------------*
       S110-PREP-AREA-ISPS0580.
      *
           INITIALIZE AREA-IO-ISPS0580.
      *
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO ISPC0580-COD-COMPAGNIA-ANIA.
      *
           MOVE WK-DT-RICOR-DA
             TO ISPC0580-DATA-INIZ-ELAB

           MOVE WK-DT-RICOR-A
             TO ISPC0580-DATA-FINE-ELAB

           MOVE WK-DT-RICOR-A
             TO ISPC0580-DATA-RIFERIMENTO
      *
           MOVE 1
             TO ISPC0580-LIVELLO-UTENTE.
      *
           MOVE IDSV0001-TIPO-MOVIMENTO
             TO ISPC0580-FUNZIONALITA.
      *
           MOVE IDSV0001-SESSIONE
             TO ISPC0580-SESSION-ID.

       S110-PREP-AREA-ISPS0580-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CHIAMATA AL SERVIZIO CONSULTAZIONE FONDI
      *----------------------------------------------------------------*
       S120-CALL-ISPS0580.
      *
           CALL ISPS0580 USING AREA-IDSV0001
                               WCOM-AREA-STATI
                               AREA-IO-ISPS0580
             ON EXCEPTION
                 MOVE 'ISPS0580'
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'SERVIZIO ACTUATOR DIAGNOSTICO'
                   TO CALL-DESC
                 MOVE 'CALL-ISPS0580'
                   TO WK-LABEL-ERR
                 PERFORM GESTIONE-ERR-SIST
                    THRU GESTIONE-ERR-SIST-EX
           END-CALL.

       S120-CALL-ISPS0580-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  VALORIZZAZIONE CAMPI DI INPUT DA PASSARE AL SERVIZIO          *
      *----------------------------------------------------------------*
       S130-PREP-AREA-ISPS0590.
      *
           INITIALIZE AREA-IO-ISPS0590.
      *
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO ISPC0590-COD-COMPAGNIA-ANIA.
      *
           MOVE WK-DT-RICOR-DA
             TO ISPC0590-DATA-INIZ-ELAB

           MOVE WK-DT-RICOR-A
             TO ISPC0590-DATA-FINE-ELAB

           MOVE WK-DT-RICOR-A
             TO ISPC0590-DATA-RIFERIMENTO
      *
           MOVE 1
             TO ISPC0590-LIVELLO-UTENTE.
      *
           MOVE IDSV0001-TIPO-MOVIMENTO
             TO ISPC0590-FUNZIONALITA.
      *
           MOVE IDSV0001-SESSIONE
             TO ISPC0590-SESSION-ID.

       S130-PREP-AREA-ISPS0590-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CHIAMATA AL SERVIZIO TASSI MINIMI
      *----------------------------------------------------------------*
       S140-CALL-ISPS0590.
      *
           CALL ISPS0590 USING AREA-IDSV0001
                               WCOM-AREA-STATI
                               AREA-IO-ISPS0590
             ON EXCEPTION
                 MOVE 'ISPS0590'
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'SERVIZIO ACTUATOR DIAGNOSTICO'
                   TO CALL-DESC
                 MOVE 'CALL-ISPS0590'
                   TO WK-LABEL-ERR
                 PERFORM GESTIONE-ERR-SIST
                    THRU GESTIONE-ERR-SIST-EX
           END-CALL.
      *
       S140-CALL-ISPS0590-EX.
           EXIT.
      *----------------------------------------------------------------*
      *          GESTIONE FILE ESTERNO DELLE GRAVITA ERRORI            *
      *----------------------------------------------------------------*
      *
       S8140-APERTURA-GRAV-ERR.
      *
           MOVE 0                            TO ELE-MAX-ERRORI
      *
           SET OPEN-FILESQS-OPER             TO TRUE
           SET OPEN-FILESQS1-NO              TO TRUE
           SET OPEN-FILESQS2-NO              TO TRUE
           SET OPEN-FILESQS3-NO              TO TRUE
           SET OPEN-FILESQS4-NO              TO TRUE
           SET OPEN-FILESQS5-SI              TO TRUE
           SET OPEN-FILESQS6-NO              TO TRUE
           SET INPUT-FILESQS5                TO TRUE

           PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX

           IF NOT FILE-STATUS-OK
              MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL          TO IEAI9901-LABEL-ERR
              MOVE '005166'          TO IEAI9901-COD-ERRORE
              MOVE SPACES            TO IEAI9901-PARAMETRI-ERR
              MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                                     TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF

           MOVE 0 TO IX-GRAV

           PERFORM UNTIL IDSV0001-ESITO-KO
                      OR FILESQS5-EOF-SI
                      OR SEQUENTIAL-ESITO-KO

                   SET READ-FILESQS-OPER      TO TRUE
                   SET READ-FILESQS5-SI       TO TRUE
                   PERFORM CALL-FILESEQ     THRU CALL-FILESEQ-EX

---->              IF SEQUENTIAL-ESITO-KO

---->                 MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                      MOVE '005016'
                          TO IEAI9901-COD-ERRORE
                      MOVE WK-PGM
                          TO IEAI9901-COD-SERVIZIO-BE
                      STRING 'ERRORE LETTURA SEQ'
                             DELIMITED BY SIZE INTO
                             IEAI9901-PARAMETRI-ERR
                      END-STRING
                      PERFORM S0300-RICERCA-GRAVITA-ERRORE
                         THRU EX-S0300
                   END-IF

                   IF SEQUENTIAL-ESITO-OK
                      IF INTESTAZIONE-SI
                         SET INTESTAZIONE-NO TO TRUE
                      ELSE
                         IF FILESQS5-EOF-NO
                            MOVE FILESQS5-REC
                              TO TRACCIATO-FILE-GRAV
                            ADD 1 TO IX-GRAV
                            MOVE IN-NUM-CNTRL
                              TO CONTROLLO(IX-GRAV)
                            MOVE IN-GRAV-CNTRL
                              TO GRAVITA(IX-GRAV)
                            MOVE IX-GRAV
                              TO ELE-MAX-ERRORI
                         END-IF
                      END-IF
                   END-IF

           END-PERFORM

      *
           IF IDSV0001-ESITO-OK
           AND FILESQS5-APERTO
              SET CLOSE-FILESQS-OPER    TO TRUE
              SET CLOSE-FILESQS1-NO     TO TRUE
              SET CLOSE-FILESQS2-NO     TO TRUE
              SET CLOSE-FILESQS3-NO     TO TRUE
              SET CLOSE-FILESQS4-NO     TO TRUE
              SET CLOSE-FILESQS5-SI     TO TRUE
              SET CLOSE-FILESQS6-NO     TO TRUE

              PERFORM CALL-FILESEQ      THRU CALL-FILESEQ-EX

              IF NOT FILE-STATUS-OK
                 MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL          TO IEAI9901-LABEL-ERR
                 MOVE '005166'          TO IEAI9901-COD-ERRORE
                 MOVE SPACES            TO IEAI9901-PARAMETRI-ERR
                 MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                                        TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.


       EX-S8140.
           EXIT.
      *----------------------------------------------------------------*
      *           GESTIONE STANDARD DELL'ERRORE DI SISTEMA             *
      *----------------------------------------------------------------*
      *
       GESTIONE-ERR-SIST.
      *
           MOVE WK-LABEL-ERR
             TO IEAI9901-LABEL-ERR
      *
           PERFORM S0290-ERRORE-DI-SISTEMA
              THRU EX-S0290.
      *
       GESTIONE-ERR-SIST-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE FILE SEQUENZIALI
      *----------------------------------------------------------------*
           COPY IABPSQS6.
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
      *----------------------------------------------------------------*
      *    ROUTINES DI GESTIONE DISPLAY
      *----------------------------------------------------------------*
           COPY IDSP8888.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.
      *----------------------------------------------------------------*
      *    COPY PROCEDURE
      *----------------------------------------------------------------*
      *--> CONTROLLI COMUNI A TUTTI GLI E/C
           COPY LRGP0661.
      *--> CONTROLLI
           COPY LRGP0662.
           COPY LRGP0663.
           COPY LRGP0664.
      *----------------------------------------------------------------*
      *    COPY TAB ESTR CNT DIAGN
      *----------------------------------------------------------------*
           COPY LCCVP853 REPLACING ==(SF)==  BY ==WP85==.


       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IABS0080 IS INITIAL.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.  MAGGIO 2007.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULI PER ACCESSO RISORSE DB               *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE     PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ      PIC 9(009)  VALUE ZEROES.
       77 FLAG-INSERT-NEW-ROW PIC X       VALUE SPACE.

       01 WS-BUFFER-WHERE-COND.
          05 WS-LUNG-EFF-BLOB    PIC 9(009).


       01 PGM-LCCS0090          PIC X(08) VALUE 'LCCS0090'.

      ***************************************************************
      *  COPY AREA CALL
      ***************************************************************

       01 LCCC0090.
           COPY LCCC0090            REPLACING ==(SF)== BY ==LINK==.

           EXEC SQL INCLUDE IDSV0010 END-EXEC.


      *----------------------------------------------------------------*
      *    VARIABILI HOST
      *----------------------------------------------------------------*
       01 VARIABILI-HOST.
          02 WS-ID-EXECUTION                    PIC S9(09) COMP-3.

      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*
           EXEC SQL INCLUDE IDBDBJE0 END-EXEC.
           EXEC SQL INCLUDE IDBVBJE2 END-EXEC.
           EXEC SQL INCLUDE IDBVBJE3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVBJE1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 BTC-JOB-EXECUTION.

           PERFORM A000-INIZIO                    THRU A000-EX.


           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
                 PERFORM A200-ELABORA-PK       THRU A200-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
           END-EVALUATE

           GOBACK.
      ******************************************************************
       A000-INIZIO.

           MOVE 'IABS0080'               TO   IDSV0003-COD-SERVIZIO-BE.
           MOVE 'BTC_JOB_EXECUTION'      TO   IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.


           MOVE IDSV0003-BUFFER-WHERE-COND
                                        TO WS-BUFFER-WHERE-COND.

       A000-EX.
           EXIT.
      ******************************************************************
       A100-CHECK-RETURN-CODE.

           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.

       A100-EX.
           EXIT.
      ******************************************************************
       A200-ELABORA-PK.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.

       A200-EX.
           EXIT.
      ******************************************************************
       A210-SELECT-PK.

           EXEC SQL
             SELECT
                ID_BATCH
                ,ID_JOB
                ,ID_EXECUTION
                ,COD_ELAB_STATE
                ,DATA
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
             INTO
                :BJE-ID-BATCH
               ,:BJE-ID-JOB
               ,:BJE-ID-EXECUTION
               ,:BJE-COD-ELAB-STATE
               ,:BJE-DATA-VCHAR
                :IND-BJE-DATA
               ,:BJE-FLAG-WARNINGS
                :IND-BJE-FLAG-WARNINGS
               ,:BJE-DT-START-DB
               ,:BJE-DT-END-DB
                :IND-BJE-DT-END
               ,:BJE-USER-START
             FROM BTC_JOB_EXECUTION
             WHERE     ID_BATCH = :BJE-ID-BATCH
                   AND ID_JOB = :BJE-ID-JOB
                   AND ID_EXECUTION = :BJE-ID-EXECUTION

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX


           END-IF.

       A210-EX.
           EXIT.
      ******************************************************************
       A220-INSERT-PK.

           PERFORM Z080-ESTRAI-ID-EXECUTION     THRU Z080-EX.

           IF IDSV0003-SUCCESSFUL-RC

              PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX

              MOVE WS-LUNG-EFF-BLOB                TO BJE-DATA-LEN

              EXEC SQL
                 INSERT
                 INTO BTC_JOB_EXECUTION
                     (
                        ID_BATCH
                       ,ID_JOB
                       ,ID_EXECUTION
                       ,COD_ELAB_STATE
                       ,DATA
                       ,FLAG_WARNINGS
                       ,DT_START
                       ,DT_END
                       ,USER_START
                     )
                 VALUES
                     (
                       :BJE-ID-BATCH
                       ,:BJE-ID-JOB
                       ,:BJE-ID-EXECUTION
                       ,:BJE-COD-ELAB-STATE
                       ,:BJE-DATA-VCHAR
                        :IND-BJE-DATA
                       ,:BJE-FLAG-WARNINGS
                        :IND-BJE-FLAG-WARNINGS
                       ,:BJE-DT-START-DB
                       ,:BJE-DT-END-DB
                        :IND-BJE-DT-END
                       ,:BJE-USER-START
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.

       A220-EX.
           EXIT.
      ******************************************************************
       A230-UPDATE-PK.

           MOVE WS-LUNG-EFF-BLOB     TO BJE-DATA-LEN

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX

           EXEC SQL
                UPDATE BTC_JOB_EXECUTION SET

                   ID_BATCH       = :BJE-ID-BATCH
                  ,ID_JOB         = :BJE-ID-JOB
                  ,ID_EXECUTION   = :BJE-ID-EXECUTION
                  ,COD_ELAB_STATE = :BJE-COD-ELAB-STATE
                  ,DATA           = :BJE-DATA-VCHAR
                                    :IND-BJE-DATA
                  ,FLAG_WARNINGS  = :BJE-FLAG-WARNINGS
                                    :IND-BJE-FLAG-WARNINGS
                  ,DT_START       = :BJE-DT-START-DB
                  ,DT_END         = :BJE-DT-END-DB
                                    :IND-BJE-DT-END
                  ,USER_START     = :BJE-USER-START

                WHERE  ID_BATCH     = :BJE-ID-BATCH
                   AND ID_JOB       = :BJE-ID-JOB
                   AND ID_EXECUTION = :BJE-ID-EXECUTION

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.
      ******************************************************************
       A240-DELETE-PK.

           EXEC SQL
                DELETE
                FROM BTC_JOB_EXECUTION
                WHERE  ID_BATCH     = :BJE-ID-BATCH
                   AND ID_JOB       = :BJE-ID-JOB
                   AND ID_EXECUTION = :BJE-ID-EXECUTION

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A240-EX.
           EXIT.
      ******************************************************************
       Z080-ESTRAI-ID-EXECUTION.

      ***************************************************************
      * STACCO DI SEQUENCE
      ***************************************************************

           MOVE SPACES             TO LCCC0090
           MOVE 'SEQ_ID_JOB'       TO LINK-NOME-TABELLA

           CALL PGM-LCCS0090       USING LCCC0090


           MOVE LINK-RETURN-CODE
                            TO IDSV0003-RETURN-CODE
           MOVE LINK-SQLCODE
                            TO IDSV0003-SQLCODE
           MOVE LINK-DESCRIZ-ERR-DB2
                            TO IDSV0003-DESCRIZ-ERR-DB2


           IF IDSV0003-SUCCESSFUL-RC
              MOVE LINK-SEQ-TABELLA TO BJE-ID-EXECUTION
           END-IF.

       Z080-EX.
           EXIT.
      ******************************************************************
       Z100-SET-COLONNE-NULL.

           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-BJE-DATA = -1
              MOVE HIGH-VALUES TO BJE-DATA
           END-IF
           IF IND-BJE-FLAG-WARNINGS = -1
              MOVE HIGH-VALUES TO BJE-FLAG-WARNINGS-NULL
           END-IF
           IF IND-BJE-DT-END = -1
              MOVE HIGH-VALUES TO BJE-DT-END-NULL
           END-IF.

       Z100-EX.
           EXIT.
      ******************************************************************
       Z200-SET-INDICATORI-NULL.

           IF BJE-DATA = HIGH-VALUES
              MOVE -1 TO IND-BJE-DATA
           ELSE
              MOVE 0 TO IND-BJE-DATA
           END-IF
           IF BJE-FLAG-WARNINGS-NULL = HIGH-VALUES
              MOVE -1 TO IND-BJE-FLAG-WARNINGS
           ELSE
              MOVE 0 TO IND-BJE-FLAG-WARNINGS
           END-IF
           IF BJE-DT-END-NULL = HIGH-VALUES
              MOVE -1 TO IND-BJE-DT-END
           ELSE
              MOVE 0 TO IND-BJE-DT-END
           END-IF.

       Z200-EX.
           EXIT.
      ******************************************************************
       Z400-SEQ-RIGA.

           CONTINUE.

       Z400-EX.
           EXIT.
      ******************************************************************
       Z500-AGGIORNAMENTO-STORICO.

           CONTINUE.

       Z500-EX.
           EXIT.
      ******************************************************************
       Z600-INSERT-NUOVA-RIGA-STORICA.

           CONTINUE.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE BJE-DT-START        TO WS-TIMESTAMP-N
           PERFORM Z701-TS-N-TO-X   THRU Z701-EX
           MOVE WS-TIMESTAMP-X      TO BJE-DT-START-DB

           IF IND-BJE-DT-END = 0
               MOVE BJE-DT-END         TO WS-TIMESTAMP-N
               PERFORM Z701-TS-N-TO-X  THRU Z701-EX
               MOVE WS-TIMESTAMP-X     TO BJE-DT-END-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE BJE-DT-START-DB TO WS-TIMESTAMP-X
           PERFORM Z801-TS-X-TO-N     THRU Z801-EX
           MOVE WS-TIMESTAMP-N      TO BJE-DT-START

           IF IND-BJE-DT-END = 0
               MOVE BJE-DT-END-DB TO WS-TIMESTAMP-X
               PERFORM Z801-TS-X-TO-N     THRU Z801-EX
               MOVE WS-TIMESTAMP-N      TO BJE-DT-END
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           MOVE LENGTH OF BJE-DATA
                       TO BJE-DATA-LEN.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.


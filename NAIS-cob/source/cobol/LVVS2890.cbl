      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS2890.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2013.
       DATE-COMPILED.
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS2890'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DATA-OUTPUT                  PIC 9(04)V9(07).
       01  WK-DATA-OUTPUT-V REDEFINES WK-DATA-OUTPUT.
           03 WK-NUMERICI                  PIC 9(04).
           03 WK-DECIMALI                  PIC 9(07).

       01 WK-DATA-X-12.
          03 WK-X-AA                          PIC X(04).
          03 WK-VIROGLA                       PIC X(01) VALUE ','.
          03 WK-X-GG                          PIC X(07).
      *---------------------------------------------------------------*
      *  WORKING ANAGRAFICA
      *---------------------------------------------------------------*
       01 AREA-RECUPERO-ANAGRAFICA.
         03 WK-TIMESTAMP.
            05 WK-DATA-EFFETTO            PIC  X(008)     VALUE SPACES.
            05 WK-ORA                     PIC  X(010)
                                          VALUE '2359999999'.
         03 WK-TIMESTAMP-N                REDEFINES
            WK-TIMESTAMP                  PIC 9(18).

         03 WK-ID-STRINGA                 PIC  X(020).
         03 WK-ID-NUM                     PIC  9(011).
         03 WK-ID-PERS                    PIC S9(009) COMP-3.

         03 WK-COD-PRT-IVA                PIC X(11).
         03 WK-COD-FISC                   PIC X(16).
      *---------------------------------------------------------------*
      *    Area per modulo conversione stringa
      *---------------------------------------------------------------*
      *--> Conversione Stringa a Numerico
       01  IWFS0050                      PIC X(008) VALUE 'IWFS0050'.

       01  AREA-CALL-IWFS0050.
           COPY IWFI0051.
           COPY IWFO0051.
      *
           COPY IABC0010.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 ARAN-IO-RAN.
          03 DRAN-AREA-RAN.
             04 DRAN-ELE-RAN-MAX       PIC S9(04) COMP.
                COPY LCCVRANA REPLACING   ==(SF)==  BY ==DRAN==.
             COPY LCCVRAN1             REPLACING ==(SF)== BY ==DRAN==.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
      *---------------------------------------------------------------*
      *  COPY TABELLE
      *---------------------------------------------------------------*
           COPY LDBV1301.
      *--> TABELLA PERSONA
           COPY IDBVA251.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000
      *
           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.
      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
      *
           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE WK-DATA-OUTPUT
                      WK-DATA-X-12.

           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           PERFORM S1200-CONTROLLO-DATI
              THRU S1200-CONTROLLO-DATI-EX.

           PERFORM S1300-RECUPERO-INFO
              THRU S1300-RECUPERO-INFO-EX.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-RAPP-ANAG
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DRAN-AREA-RAN
           END-IF.
      *
       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO DATI DCLGEN
      *----------------------------------------------------------------*
       S1200-CONTROLLO-DATI.
      *
           IF DRAN-TP-PERS(IVVC0213-IX-TABB) EQUAL HIGH-VALUE
           OR LOW-VALUE OR SPACES
              SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'TIPO PERSONA NON VALORIZZATO'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.
      *
       S1200-CONTROLLO-DATI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   RECUPERO INFORMAZIONI
      *----------------------------------------------------------------*
       S1300-RECUPERO-INFO.

           PERFORM ACCESSO-PERS
              THRU ACCESSO-PERS-EX.

       S1300-RECUPERO-INFO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   RECUPERO DELLA PERS
      *----------------------------------------------------------------*
       ACCESSO-PERS.

           INITIALIZE LDBV1301
                      PERS.

           IF DRAN-COD-SOGG-NULL(IVVC0213-IX-TABB) = HIGH-VALUES OR
                                                     LOW-VALUES  OR
                                                     SPACES
              SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CODICE SOGGETTO NON VALORIZZATO'
                TO IDSV0003-DESCRIZ-ERR-DB2
           ELSE

              MOVE DRAN-COD-SOGG(IVVC0213-IX-TABB) TO WK-ID-STRINGA
              PERFORM E000-CONVERTI-CHAR
                 THRU EX-E000
              MOVE WK-ID-NUM                    TO WK-ID-PERS

              MOVE 'LDBS1300'                   TO WK-CALL-PGM
              SET IDSV0003-PRIMARY-KEY          TO TRUE
              SET IDSV0003-SELECT               TO TRUE

              MOVE WK-ID-PERS                   TO LDBV1301-ID-TAB
              MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WK-DATA-EFFETTO
              MOVE WK-TIMESTAMP-N               TO LDBV1301-TIMESTAMP

              MOVE LDBV1301 TO IDSV0003-BUFFER-WHERE-COND

           CALL WK-CALL-PGM  USING  IDSV0003 PERS
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2890'
              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
               END-STRING
              SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL

           IF IDSV0003-SUCCESSFUL-SQL

              IF A25-COD-FRM-GIURD-NULL = HIGH-VALUES OR
                                          LOW-VALUES  OR
                                          SPACES
                 MOVE SPACES               TO A25-COD-FRM-GIURD
              END-IF

              IF A25-IND-CLI = 'G' AND
                 (A25-COD-FRM-GIURD NOT = 'F' AND 'I')

                 MOVE 'G'                  TO IVVC0213-VAL-STR-O

              ELSE

                 MOVE 'F'                  TO IVVC0213-VAL-STR-O

              END-IF
           ELSE
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING  WK-PGM ' ERRORE CHIAMATA - MODULO LVVS2890'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
           END-IF.

       ACCESSO-PERS-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CONVERTI VALORE DA STRINGA IN NUMERICO
      *----------------------------------------------------------------*
       E000-CONVERTI-CHAR.

           MOVE ZERO                  TO WK-ID-NUM.
           MOVE HIGH-VALUE            TO AREA-CALL-IWFS0050.

           MOVE WK-ID-STRINGA         TO IWFI0051-ARRAY-STRINGA-INPUT.

           CALL IWFS0050              USING AREA-CALL-IWFS0050

      *    MOVE IWFO0051-ESITO        TO IDSV0003-ESITO.
      *    MOVE IWFO0051-LOG-ERRORE   TO IDSV0003-LOG-ERRORE.
           MOVE IWFO0051-CAMPO-OUTPUT-DEFI
                                      TO WK-ID-NUM.

       EX-E000.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.


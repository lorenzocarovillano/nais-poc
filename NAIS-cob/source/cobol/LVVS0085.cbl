      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0085.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0085
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0085'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-TP-ASS                          PIC X(03).

       01 WK-DATA-X-12.
          03 WK-X-AA                          PIC X(04).
          03 WK-VIROGLA                       PIC X(01) VALUE ','.
          03 WK-X-GG                          PIC X(07).

      *-- AREA ROUTINE PER IL CALCOLO
       01  FORMATO                 PIC X(01).
       01  DATA-INFERIORE.
           05 AAAA-INF             PIC 9(04).
           05 MM-INF               PIC 9(02).
           05 GG-INF               PIC 9(02).
       01  DATA-SUPERIORE.
           05 AAAA-SUP             PIC 9(04).
           05 MM-SUP               PIC 9(02).
           05 GG-SUP               PIC 9(02).
       01  GG-DIFF                 PIC 9(05).
       01  CODICE-RITORNO          PIC X(01).

       01  WK-FIND-LETTO                     PIC X(001).
           88 WK-LETTO-SI                    VALUE 'S'.
           88 WK-LETTO-NO                    VALUE 'N'.

       01  WK-DATA-INFERIORE               PIC 9(08) VALUE ZERO.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-TGA.
          03 DTGA-AREA-TGA.
             04 DTGA-ELE-TGA-MAX        PIC S9(04) COMP.
                COPY LCCVTGAC REPLACING   ==(SF)==  BY ==DTGA==.
             COPY LCCVTGA1              REPLACING ==(SF)== BY ==DTGA==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-TGA                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0085.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0085.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

           MOVE IVVC0213-WHERE-COND          TO WK-TP-ASS.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1200-CONTROLLO-DATI
                  THRU S1200-CONTROLLO-DATI-EX
           END-IF.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1250-CALCOLA-DATA1
                  THRU S1250-EX
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-TRCH-GAR
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DTGA-AREA-TGA
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATA 1
      *----------------------------------------------------------------*
       S1250-CALCOLA-DATA1.
      *
           EVALUATE WK-TP-ASS
              WHEN 'I'
                IF DTGA-ETA-AA-1O-ASSTO(IVVC0213-IX-TABB) IS NUMERIC
                   MOVE DTGA-ETA-AA-1O-ASSTO(IVVC0213-IX-TABB)
                     TO IVVC0213-VAL-IMP-O

                   IF DTGA-ETA-MM-1O-ASSTO(IVVC0213-IX-TABB) IS NUMERIC
36829 *               IF DTGA-ETA-MM-1O-ASSTO(IVVC0213-IX-TABB) > 5
36829                 IF DTGA-ETA-MM-1O-ASSTO(IVVC0213-IX-TABB) > 6
                         ADD 1               TO IVVC0213-VAL-IMP-O
                      END-IF
                   END-IF
                ELSE
                   SET IDSV0003-FIELD-NOT-VALUED           TO TRUE
                END-IF

              WHEN 'II'
                IF DTGA-ETA-AA-2O-ASSTO(IVVC0213-IX-TABB) IS NUMERIC
                   MOVE DTGA-ETA-AA-2O-ASSTO(IVVC0213-IX-TABB)
                     TO IVVC0213-VAL-IMP-O
                ELSE
                   SET IDSV0003-FIELD-NOT-VALUED           TO TRUE
                END-IF

                IF DTGA-ETA-MM-2O-ASSTO(IVVC0213-IX-TABB) IS NUMERIC
36829 *            IF DTGA-ETA-MM-2O-ASSTO(IVVC0213-IX-TABB) > 5
36829              IF DTGA-ETA-MM-2O-ASSTO(IVVC0213-IX-TABB) > 6
                      ADD 1               TO IVVC0213-VAL-IMP-O
                   END-IF
                END-IF

              WHEN 'III'
                IF DTGA-ETA-AA-3O-ASSTO(IVVC0213-IX-TABB) IS NUMERIC
                   MOVE DTGA-ETA-AA-3O-ASSTO(IVVC0213-IX-TABB)
                     TO IVVC0213-VAL-IMP-O
                ELSE
                   SET IDSV0003-FIELD-NOT-VALUED           TO TRUE
                END-IF

                IF DTGA-ETA-MM-3O-ASSTO(IVVC0213-IX-TABB) IS NUMERIC
36829 *            IF DTGA-ETA-MM-3O-ASSTO(IVVC0213-IX-TABB) > 5
36829              IF DTGA-ETA-MM-3O-ASSTO(IVVC0213-IX-TABB) > 6
                      ADD 1               TO IVVC0213-VAL-IMP-O
                   END-IF
                END-IF
           END-EVALUATE.
      *
       S1250-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO DATI DCLGEN
      *----------------------------------------------------------------*
       S1200-CONTROLLO-DATI.

      *    MOVE DPOL-TP-FRM-ASSVA            TO WS-TP-FRM-ASSVA
           IF IVVC0213-ID-TRANCHE IS NUMERIC
              IF IVVC0213-ID-TRANCHE = 0
                SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                MOVE WK-PGM
                  TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'ID-POLIZZA NON VALORIZZATO'
                  TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
           ELSE
              SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ID-POLIZZA NON VALORIZZATO'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.

       S1200-CONTROLLO-DATI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.


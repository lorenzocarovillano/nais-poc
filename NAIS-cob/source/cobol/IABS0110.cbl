       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IABS0110 IS INITIAL.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : SERVIZIO ESTRAZIONE OCCORRENZE              *
      *                   DA TABELLE GUIDA RICHIAMATO DAL BATCH EXEC. *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2       PIC X(300)      VALUE SPACES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.

      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*
           EXEC SQL INCLUDE IDBDBRS0 END-EXEC.
           EXEC SQL INCLUDE IDBVBRS2 END-EXEC.
      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*
           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IABV0002 END-EXEC.

           EXEC SQL INCLUDE IDBVBRS1 END-EXEC.

      *****************************************************************
       PROCEDURE DIVISION USING IDSV0003 IABV0002 BTC-REC-SCHEDULE.

           PERFORM A000-INIZIO                 THRU A000-EX.

           PERFORM A300-ELABORA                THRU A300-EX.

           PERFORM A400-FINE                   THRU A400-EX.

           GOBACK.

      ******************************************************************
       A000-INIZIO.

           MOVE 'IABS0110'              TO   IDSV0003-COD-SERVIZIO-BE.
           MOVE 'BTC_REC_SCHEDULE'      TO   IDSV0003-NOME-TABELLA.

           MOVE '00'                    TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                  TO   IDSV0003-SQLCODE
                                             IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2
                                             IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.
      ******************************************************************
       A100-CHECK-RETURN-CODE.

           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE
           END-IF.

       A100-EX.
           EXIT.
      ******************************************************************
       A300-ELABORA.

           EVALUATE TRUE
              WHEN IDSV0003-DELETE
                 PERFORM A310-DELETE                 THRU A310-EX
              WHEN IDSV0003-INSERT
                 PERFORM A320-INSERT                 THRU A320-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR           THRU A370-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST            THRU A380-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT             THRU A390-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.

       A300-EX.
           EXIT.
      ******************************************************************
       A400-FINE.

           CONTINUE.

       A400-EX.
           EXIT.
      ******************************************************************
       A305-DECLARE-CURSOR.

           EXEC SQL
              DECLARE CUR-BRS CURSOR WITH HOLD FOR
              SELECT
                ID_BATCH
                ,ID_JOB
                ,ID_RECORD
                ,TYPE_RECORD
                ,DATA_RECORD

              FROM BTC_REC_SCHEDULE
             WHERE     ID_BATCH = :BRS-ID-BATCH
                   AND ID_JOB   = :BRS-ID-JOB

              ORDER BY ID_RECORD

           END-EXEC.

       A305-EX.
           EXIT.

       A320-INSERT.

           PERFORM Z400-SEQ                       THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO BTC_REC_SCHEDULE
                     (
                        ID_BATCH
                       ,ID_JOB
                       ,ID_RECORD
                       ,TYPE_RECORD
                       ,DATA_RECORD
                     )
                 VALUES
                     (
                       :BRS-ID-BATCH
                       ,:BRS-ID-JOB
                       ,:BRS-ID-RECORD
                       ,:BRS-TYPE-RECORD
                        :IND-BRS-TYPE-RECORD
                       ,:BRS-DATA-RECORD-VCHAR
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A320-EX.
           EXIT.
      ******************************************************************
       A360-OPEN-CURSOR.

           PERFORM A305-DECLARE-CURSOR THRU A305-EX.

           EXEC SQL
                OPEN CUR-BRS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A360-EX.
           EXIT.
      ******************************************************************
       A370-CLOSE-CURSOR.

           EXEC SQL
                CLOSE CUR-BRS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A370-EX.
           EXIT.
      ******************************************************************
       A380-FETCH-FIRST.

           PERFORM A360-OPEN-CURSOR    THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT THRU A390-EX
           END-IF.

       A380-EX.
           EXIT.
      ******************************************************************
       A390-FETCH-NEXT.

           INITIALIZE BTC-REC-SCHEDULE.

           EXEC SQL
                FETCH CUR-BRS
           INTO
                 :BRS-ID-BATCH
                 ,:BRS-ID-JOB
                 ,:BRS-ID-RECORD
                 ,:BRS-TYPE-RECORD
                  :IND-BRS-TYPE-RECORD
                 ,:BRS-DATA-RECORD-VCHAR
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       A390-EX.
           EXIT.

      ******************************************************************
       A310-DELETE.

           EXEC SQL
                DELETE
                FROM BTC_REC_SCHEDULE
                WHERE  ID_BATCH = :BRS-ID-BATCH
                   AND ID_JOB   = :BRS-ID-JOB
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A310-EX.
           EXIT.

      ******************************************************************
       Z100-SET-COLONNE-NULL.

           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.

           IF IND-BRS-TYPE-RECORD = -1
              MOVE HIGH-VALUES TO BRS-TYPE-RECORD-NULL
           END-IF.

       Z100-EX.
           EXIT.

      ******************************************************************
       Z150-VALORIZZA-DATA-SERVICES.

           CONTINUE.
       Z150-EX.
           EXIT.

      ******************************************************************
       Z200-SET-INDICATORI-NULL.

           IF BRS-TYPE-RECORD-NULL = HIGH-VALUES
              MOVE -1 TO IND-BRS-TYPE-RECORD
           ELSE
              MOVE 0 TO IND-BRS-TYPE-RECORD
           END-IF.

       Z200-EX.
           EXIT.


       Z400-SEQ.

           EXEC SQL
              VALUES NEXTVAL FOR SEQ_ID_RECORD
              INTO :BRS-ID-RECORD
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.

           CONTINUE.
       Z900-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           IF BRS-DATA-RECORD-LEN NOT > ZEROES
              MOVE LENGTH OF BRS-DATA-RECORD
                          TO BRS-DATA-RECORD-LEN
           END-IF.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
----->     EXEC SQL INCLUDE IDSP0003 END-EXEC.

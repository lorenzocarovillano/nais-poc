      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS3480.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2018.
       DATE-COMPILED.
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS3480'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
      *01 WK-VARIABILI.
      *---------------------------------------------------------------*
      *  COPY TIPOLOGICHE
      *---------------------------------------------------------------*


      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *-- AREA AREA FONDI COMMISSIONI GESTIONE
       01 WCDG-AREA-COMMIS-GEST.
          COPY IVVC0224          REPLACING ==(SF)== BY ==WCDG==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-TAB-CDG                     PIC S9(04) COMP VALUE 0.
           03 IX-DCLGEN                      PIC S9(04) COMP VALUE 0.
           03 IX-TAB-0501                    PIC S9(04) COMP VALUE 0.
      *----------------------------------------------------------------*
      *      per modulo di stringatura variabili                       *
      *----------------------------------------------------------------*
           COPY IDSV0502.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS3480.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

       COPY IVVC0501.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003
                                INPUT-LVVS3480
                                IVVC0501-OUTPUT.
      *----------------------------------------------------------------*
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000.
      *
           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IVVC0501-OUTPUT
           INITIALIZE                        IDSV0501-OUTPUT
           INITIALIZE                        IDSV0501-INPUT

           MOVE ZEROES                       TO IX-TAB-0501
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

      *    MOVE IVVC0213-TIPO-MOVI-ORIG
      *      TO WS-MOVIMENTO.
           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.
       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

      *--> VALORIZZA LA VARIABILE A LISTA
           PERFORM VARYING IX-TAB-CDG FROM 1 BY 1
                     UNTIL IX-TAB-CDG > WCDG-ELE-MAX-CDG-VV

               IF IX-TAB-0501 < LIMITE-ARRAY-INPUT
                  ADD 1               TO IX-TAB-0501
                  MOVE 'L'            TO IDSV0501-TP-DATO(IX-TAB-0501)
                  MOVE WCDG-COD-FND-VV(IX-TAB-CDG)
                    TO IDSV0501-VAL-STR(IX-TAB-0501)
               ELSE
                  MOVE 'VARLIST'
                    TO IDSV0003-COD-SERVIZIO-BE
                  MOVE 'LVVS3480 - LIMITE-ARRAY-INPUT SUPERATO'
                    TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER       TO TRUE
               END-IF

               IF IX-TAB-0501 < LIMITE-ARRAY-INPUT
                  ADD 1               TO IX-TAB-0501
      *           MOVE 'R'            TO IDSV0501-TP-DATO(IX-TAB-0501)
                  MOVE 'T'            TO IDSV0501-TP-DATO(IX-TAB-0501)
                  MOVE WCDG-CDG-REST-VV(IX-TAB-CDG)
      *             TO IDSV0501-VAL-IMP(IX-TAB-0501)
                    TO IDSV0501-VAL-PERC(IX-TAB-0501)
               ELSE
                  MOVE 'VARLIST'
                    TO IDSV0003-COD-SERVIZIO-BE
                  MOVE 'LVVS3480 - LIMITE-ARRAY-INPUT SUPERATO'
                    TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER       TO TRUE
               END-IF

           END-PERFORM

      *    SE NON HO NESSUN ELEMENTO, INIZIALIZZO LA VARIABILE
           IF IDSV0003-SUCCESSFUL-RC
              IF IX-TAB-0501 = ZEROES
                 PERFORM S1360-INIT-VARIABILE
                    THRU S1360-EX
      *       END-IF
           END-IF

      *--> VALORIZZA LA VARIABILE A LISTA
           IF IDSV0003-SUCCESSFUL-RC
      *       IF FL-LIQUI-SI
                 PERFORM S1400-VALORIZZA-VARIABILE
                    THRU S1400-EX
      *       END-IF
           END-IF

           MOVE IVVC0213-COD-VARIABILE
              TO IVVC0213-COD-VARIABILE-O.
      *
       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-AREA-FND-X-CDG
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO WCDG-AREA-COMMIS-GEST-VV
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   INIZIALIZZO LA VARIABILE SE NON VALORIZZATA
      *----------------------------------------------------------------*
       S1360-INIT-VARIABILE.
      *
           MOVE 1                      TO IX-TAB-0501.
           MOVE 'L'                    TO IDSV0501-TP-DATO(IX-TAB-0501).
           MOVE SPACES                 TO IDSV0501-VAL-STR(IX-TAB-0501).
           MOVE 2                      TO IX-TAB-0501.
      *    MOVE 'R'                    TO IDSV0501-TP-DATO(IX-TAB-0501).
           MOVE 'T'                    TO IDSV0501-TP-DATO(IX-TAB-0501).
      *    MOVE ZEROES                 TO IDSV0501-VAL-IMP(IX-TAB-0501).
           MOVE ZEROES                TO IDSV0501-VAL-PERC(IX-TAB-0501).
      *
       S1360-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   VALORIZZO LA VARIABILE A LISTA
      *----------------------------------------------------------------*
       S1400-VALORIZZA-VARIABILE.

           SET IDSV0003-SUCCESSFUL-RC  TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL TO TRUE.

           IF IDSV0003-SUCCESSFUL-RC
              PERFORM STRINGA-X-VALORIZZATORE
                 THRU STRINGA-X-VALORIZZATORE-EX

              IF IDSV0501-SUCCESSFUL-RC
                 MOVE IDSV0501-OUTPUT
                   TO IVVC0501-OUTPUT
              ELSE
                 SET IDSV0003-FIELD-NOT-VALUED TO TRUE
              END-IF
           END-IF.

       S1400-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *   CODICE PER STRINGATURA VARIABILI
      *----------------------------------------------------------------*
           COPY IVVP0501.


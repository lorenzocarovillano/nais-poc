      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0001.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0001
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... ESTRAZIONE VALORE FATTORE PARAMETRICO
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0001'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
       01  WKS-NOME-TABELLA                 PIC X(030).

       01  WS-TP-DATO.
           05 WS-TASSO                      PIC X(01) VALUE 'A'.
           05 WS-IMPORTO                    PIC X(01) VALUE 'N'.
           05 WS-PERCENTUALE                PIC X(01) VALUE 'P'.
           05 WS-MILLESIMI                  PIC X(01) VALUE 'M'.
           05 WS-DATA                       PIC X(01) VALUE 'D'.
           05 WS-STRINGA                    PIC X(01) VALUE 'S'.
           05 WS-NUMERO                     PIC X(01) VALUE 'I'.
           05 WS-FLAG                       PIC X(01) VALUE 'F'.

       01  WS-TP-OGGETTO.
           05 WS-POLIZZA                    PIC X(02) VALUE 'PO'.
           05 WS-ADESIONE                   PIC X(02) VALUE 'AD'.
           05 WS-GARANZIA                   PIC X(02) VALUE 'GA'.
           05 WS-TRANCHE                    PIC X(02) VALUE 'TG'.
      *----------------------------------------------------------------*
      *    COPY DELLE TABELLE DB2
      *----------------------------------------------------------------*
      *--> PARAMETRO OGGETTO
           COPY IDBVPOG1.
      *--> DATI COLLETTIVA
           COPY IDBVDCO1.
      *--> ADESIONE
           COPY IDBVADE1.
      *--> GARANZIA
           COPY IDBVGRZ1.
      *--> PARAMETRO MOVIMENTO
           COPY IDBVPMO1.
      *--> TRANCHE DI GARANZIA
           COPY IDBVTGA1.
           COPY LCCVPOGZ.
      ******************************************************************
      *    COPY DISPATCHER
      ******************************************************************
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      ** COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      ** COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *----------------------------------------------------------------*
      *--> AREA DATI COLLETTIVA
      *----------------------------------------------------------------*
       01 AREE-APPOGGIO.
         02  AREA-IO-DCO.
             03 DDCO-AREA-DCO.
                04 DDCO-ELE-DCO-MAX           PIC S9(004) COMP.
                04 DDCO-TAB-COLL.
                   COPY LCCVDCO1         REPLACING ==(SF)== BY ==DDCO==.
      *----------------------------------------------------------------*
      *--> AREA GARANZIE
      *----------------------------------------------------------------*
         02  AREA-IO-GRZ.
             03 DGRZ-AREA-GARANZIA.
                04 DGRZ-ELE-GAR-MAX           PIC S9(004) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==DGRZ==.
                   COPY LCCVGRZ1         REPLACING ==(SF)== BY ==DGRZ==.
      *----------------------------------------------------------------*
      *--> AREA ADESIONE
      *----------------------------------------------------------------*
         02  AREA-IO-ADE.
             03 DADE-AREA-ADESIONE.
                04 DADE-ELE-ADES-MAX       PIC S9(004) COMP.
                04 DADE-TAB-ADES           OCCURS 1.
                COPY LCCVADE1            REPLACING ==(SF)== BY ==DADE==.
      *----------------------------------------------------------------*
      *--> AREA PARAMETRO MOVIMENTO
      *----------------------------------------------------------------*
         02  AREA-IO-MOV.
             03 DPMO-AREA-PARAM-MOVI.
                04 DPMO-ELE-PARAM-MOVI-MAX    PIC S9(004) COMP.
                COPY LCCVPMOB REPLACING   ==(SF)==  BY ==DPMO==.
                   COPY LCCVPMO1         REPLACING ==(SF)== BY ==DPMO==.
      *----------------------------------------------------------------*
      *--> AREA TRANCHE DI GARANZIA
      *----------------------------------------------------------------*
         02  AREA-IO-TGA.
             03 DTGA-AREA-TRCH-DI-GAR.
                04 DTGA-ELE-TGA-MAX           PIC S9(004) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==DTGA==.
                   COPY LCCVTGA1         REPLACING ==(SF)== BY ==DTGA==.
      *----------------------------------------------------------------*
      *--> AREA PARAMETRO OGGETTO
      *----------------------------------------------------------------*
         02  AREA-IO-POG.
             03 DPOG-AREA-PARAM-OGG.
                04 DPOG-ELE-PARAM-OGG-MAX     PIC S9(04) COMP.
                COPY LCCVPOGB REPLACING   ==(SF)==  BY ==DPOG==.
                   COPY LCCVPOG1         REPLACING ==(SF)== BY ==DPOG==.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.
      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-TAB-PARAM-OGG               PIC S9(04) COMP.
           03 IX-TAB-DCO                     PIC S9(04) COMP.
           03 IX-TAB-ADE                     PIC S9(04) COMP.
           03 IX-TAB-GRZ                     PIC S9(04) COMP.
           03 IX-TAB-TGA                     PIC S9(04) COMP.
           03 IX-TAB-POG                     PIC S9(04) COMP.
           03 IX-TAB-PMO                     PIC S9(04) COMP.
           03 CONT-FETCH                     PIC S9(04) COMP.
           03 IX-DCLGEN                      PIC S9(04) COMP.
      *----------------------------------------------------------------*
      *    FLAG
      *----------------------------------------------------------------*
      *--> FLAG PER GESTIONE OVERFLOW LETTURA IN FETCH
       01  WCOM-FLAG-OVERFLOW                PIC X(002).
              88 WCOM-OVERFLOW-YES            VALUE 'SI'.
              88 WCOM-OVERFLOW-NO             VALUE 'NO'.

       01  WCOM-FLAG-TROVATO                 PIC X(001).
              88 WCOM-SI-TROVATO              VALUE 'S'.
              88 WCOM-NO-TROVATO              VALUE 'N'.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0001.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0001.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1000-ELABORAZIONE
                  THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                           IX-INDICI.
           INITIALIZE                           AREE-APPOGGIO.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
           SET WCOM-NO-TROVATO               TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

           PERFORM S0005-CTRL-DATI-INPUT
                THRU EX-S0005.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLO DATI INPUT
      *----------------------------------------------------------------*
       S0005-CTRL-DATI-INPUT.

           IF IVVC0213-COD-PARAMETRO = SPACES OR LOW-VALUE
      *---    COD-PARAMETRO NON VALORIZZATO
              SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'COD-PARAMETRO NON VALORIZZATO'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
              IF IVVC0213-ID-TRANCHE  > ZEROES AND
                 IVVC0213-ID-POLIZZA  = ZEROES AND
                 IVVC0213-ID-ADESIONE = ZEROES AND
                 IVVC0213-ID-GARANZIA = ZEROES
      *---       ID-POLIZZA ID-ADESIONE E ID-GARANZIA NON VALORIZZATI
                 SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'ID-POLIZZA ID-ADESIONE E ID-GARANZIA NON VALORIZZATI'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
           END-IF.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
              IF IVVC0213-ID-GARANZIA > ZEROES AND
                 IVVC0213-ID-POLIZZA  = ZEROES AND
                 IVVC0213-ID-ADESIONE = ZEROES
      *---       ID-POLIZZA E ID-ADESIONE NON VALORIZZATI
                 SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'ID-POLIZZA E ID-ADESIONE NON VALORIZZATI'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
           END-IF.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
              IF IVVC0213-ID-ADESIONE > ZEROES AND
                 IVVC0213-ID-POLIZZA  = ZEROES
      *---       ID-POLIZZA NON VALORIZZATO
                 SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'ID-POLIZZA NON VALORIZZATO'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
           END-IF.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
              IF IVVC0213-ID-TRANCHE  = ZEROES AND
                 IVVC0213-ID-POLIZZA  = ZEROES AND
                 IVVC0213-ID-ADESIONE = ZEROES AND
                 IVVC0213-ID-GARANZIA = ZEROES
      *---       NESSUN ID VALORIZZATO
                 SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'NESSUN ID VALORIZZATO'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
           END-IF.

       EX-S0005.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE

      *--> ROUTINE PER CERCARE IL CODICE PARAMETRO SULLA DCLGEN
      *--> PARAMETRO OGGETTO PRESENTE A CONTESTO
           PERFORM S1200-CONTROLLO-DATI
              THRU S1200-CONTROLLO-DATI-EX
              VARYING IVVC0213-IX-TABB  FROM 1 BY 1
              UNTIL IVVC0213-IX-TABB > DPOG-ELE-PARAM-OGG-MAX
                 OR WCOM-SI-TROVATO.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
           AND WCOM-NO-TROVATO
      *---     COD-PARAMETRO NON TROVATO
               SET  IDSV0003-FIELD-NOT-VALUED   TO TRUE
               MOVE WK-PGM
                 TO IDSV0003-COD-SERVIZIO-BE
               MOVE 'COD-PARAMETRO NON TROVATO'
                 TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-PARAM-OGG
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPOG-AREA-PARAM-OGG
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLO DATI
      *----------------------------------------------------------------*
       S1200-CONTROLLO-DATI.

           IF IVVC0213-COD-PARAMETRO = DPOG-COD-PARAM(IVVC0213-IX-TABB)
              IF  DPOG-VAL-IMP(IVVC0213-IX-TABB) IS NUMERIC
                  CONTINUE
              ELSE
                  MOVE ZEROES TO DPOG-VAL-IMP(IVVC0213-IX-TABB)
              END-IF

              IF  DPOG-VAL-PC (IVVC0213-IX-TABB) IS NUMERIC
                  CONTINUE
              ELSE
                  MOVE ZEROES TO DPOG-VAL-PC (IVVC0213-IX-TABB)
              END-IF

              IF  DPOG-VAL-IMP(IVVC0213-IX-TABB)  = ZEROES
              AND DPOG-VAL-PC (IVVC0213-IX-TABB)  = ZEROES
              AND DPOG-VAL-TXT(IVVC0213-IX-TABB)  = SPACES

                  PERFORM S1210-LEGGI-DATI-TAB
                     THRU S1210-LEGGI-DATI-TAB-EX
              ELSE
                  PERFORM S1220-VALORIZZA-OUT
                     THRU S1220-VALORIZZA-OUT-EX

                  SET WCOM-SI-TROVATO               TO TRUE
              END-IF
           ELSE
              SET WCOM-NO-TROVATO                   TO TRUE
           END-IF.

           IF IVVC0213-COD-PARAMETRO = 'CEDOLAREG'
              COMPUTE IVVC0213-VAL-IMP-O = 360 / IVVC0213-VAL-IMP-O
           END-IF.

       S1200-CONTROLLO-DATI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA DATI DALLE TABELLE
      *----------------------------------------------------------------*
       S1210-LEGGI-DATI-TAB.

           EVALUATE IVVC0213-COD-PARAMETRO
             WHEN 'IMPOARROT'
             WHEN 'GRUPREDACAP'
      *-->       LETTURA DATI COLLETTIVA
                 PERFORM S1211-VALORIZZA-INPUT-DCO
                    THRU S1211-VALORIZZA-INPUT-DCO-EX
                 PERFORM LETTURA-DCO
                    THRU LETTURA-DCO-EX
                 IF  IDSV0003-SUCCESSFUL-RC
                 AND IDSV0003-SUCCESSFUL-SQL
                     MOVE DDCO-IMP-ARROT-PRE
                       TO DPOG-VAL-IMP(IVVC0213-IX-TABB)
                     MOVE DDCO-FL-RICL-PRE-DA-CPT
                       TO DPOG-VAL-FL(IVVC0213-IX-TABB)
                     SET  WCOM-SI-TROVATO   TO TRUE
                 END-IF
             WHEN 'MODDURPRD'
      *-->       LETTURA ADESIONE
                 PERFORM S1212-VALORIZZA-INPUT-ADE
                    THRU S1212-VALORIZZA-INPUT-ADE-EX
                 PERFORM LETTURA-ADE
                    THRU LETTURA-ADE-EX
                 IF  IDSV0003-SUCCESSFUL-RC
                 AND IDSV0003-SUCCESSFUL-SQL
                     SET  WCOM-SI-TROVATO   TO TRUE
                 END-IF
             WHEN 'NUMERORATE'
      *-->       LETTURA GARANZIA
                 PERFORM S1213-VALORIZZA-INPUT-GRZ
                    THRU S1213-VALORIZZA-INPUT-GRZ-EX
                 PERFORM LETTURA-GRZ
                    THRU LETTURA-GRZ-EX
                 IF  IDSV0003-SUCCESSFUL-RC
                 AND IDSV0003-SUCCESSFUL-SQL
                     MOVE DGRZ-MM-1O-RAT(IVVC0213-IX-TABB)
                       TO DPOG-VAL-NUM(IVVC0213-IX-TABB)
                     SET  WCOM-SI-TROVATO   TO TRUE
                 END-IF
             WHEN 'PERCREVEREND'
             WHEN 'ANNIRENCERT'
      *-->       LETTURA PARAMETRO MOVIMENTO
                 PERFORM S1214-VALORIZZA-INPUT-PMO
                    THRU S1214-VALORIZZA-INPUT-PMO-EX
                 PERFORM LETTURA-PMO
                    THRU LETTURA-PMO-EX
                 IF  IDSV0003-SUCCESSFUL-RC
                 AND IDSV0003-SUCCESSFUL-SQL
                     MOVE DPMO-PC-REVRSB(IVVC0213-IX-TABB)
                       TO DPOG-VAL-PC(IVVC0213-IX-TABB)
                     MOVE DPMO-AA-REN-CER(IVVC0213-IX-TABB)
                       TO DPOG-VAL-NUM(IVVC0213-IX-TABB)
                     SET  WCOM-SI-TROVATO   TO TRUE
                 END-IF
             WHEN 'CARICOCONT'
      *-->       LETTURA TRANCHE DI GARANZIA
                 PERFORM S1215-VALORIZZA-INPUT-TGA
                    THRU S1215-VALORIZZA-INPUT-TGA-EX
                 PERFORM LETTURA-TGA
                    THRU LETTURA-TGA-EX
                 IF  IDSV0003-SUCCESSFUL-RC
                 AND IDSV0003-SUCCESSFUL-SQL
                     MOVE DTGA-FL-CAR-CONT(IVVC0213-IX-TABB)
                       TO DPOG-VAL-FL(IVVC0213-IX-TABB)
                     SET  WCOM-SI-TROVATO   TO TRUE
                 END-IF
16653        WHEN 'LIQNOADEG'
16653            CONTINUE
             WHEN OTHER
                 SET  WCOM-NO-TROVATO   TO TRUE
           END-EVALUATE

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1220-VALORIZZA-OUT
                  THRU S1220-VALORIZZA-OUT-EX
           END-IF.

       S1210-LEGGI-DATI-TAB-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA AREA PER LETTURA DATI COLLETTIVA
      *----------------------------------------------------------------*
       S1211-VALORIZZA-INPUT-DCO.

           MOVE IVVC0213-ID-POLIZZA  TO DCO-ID-POLI.

           MOVE ZERO                 TO IDSI0011-DATA-INIZIO-EFFETTO
                                        IDSI0011-DATA-FINE-EFFETTO
                                        IDSI0011-DATA-COMPETENZA.

           SET  IDSI0011-TRATT-X-COMPETENZA  TO TRUE.

      *--> TIPO OPERAZIONE
           SET  IDSI0011-SELECT      TO TRUE.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID-PADRE    TO TRUE.

      *--> NOME TABELLA FISICA DB
           MOVE 'D-COLL'             TO IDSI0011-CODICE-STR-DATO
                                        WKS-NOME-TABELLA.

      *--> DCLGEN TABELLA
           MOVE D-COLL               TO IDSI0011-BUFFER-DATI.

       S1211-VALORIZZA-INPUT-DCO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA AREA PER LETTURA ADESIONE
      *----------------------------------------------------------------*
       S1212-VALORIZZA-INPUT-ADE.

           MOVE IVVC0213-ID-POLIZZA      TO ADE-ID-POLI.
           MOVE IVVC0213-ID-ADESIONE     TO ADE-ID-ADES.

      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.

      *--> NOME TABELLA FISICA DB
           MOVE 'ADES'                   TO IDSI0011-CODICE-STR-DATO
                                            WKS-NOME-TABELLA.

      *--> DCLGEN TABELLA
           MOVE ADES                     TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT           TO TRUE.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-ID               TO TRUE.

       S1212-VALORIZZA-INPUT-ADE-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA AREA PER LETTURA GARANZIE
      *----------------------------------------------------------------*
       S1213-VALORIZZA-INPUT-GRZ.

           MOVE IVVC0213-ID-GARANZIA    TO GRZ-ID-GAR.

           MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO
                                           IDSI0011-DATA-FINE-EFFETTO
                                           IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-X-COMPETENZA TO TRUE.

      *--> NOME TABELLA FISICA DB
           MOVE 'GAR'                   TO IDSI0011-CODICE-STR-DATO
                                           WKS-NOME-TABELLA.

      *--> DCLGEN TABELLA
           MOVE GAR                     TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT          TO TRUE.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

       S1213-VALORIZZA-INPUT-GRZ-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA AREA PER LETTURA PARAMETRO MOVIMENTO
      *----------------------------------------------------------------*
       S1214-VALORIZZA-INPUT-PMO.

           MOVE IVVC0213-ID-GARANZIA     TO PMO-ID-OGG.
           MOVE WS-GARANZIA              TO PMO-TP-OGG.
           MOVE IDSV0003-TIPO-MOVIMENTO  TO PMO-TP-MOVI.

      *--> VALORIZZAZIONE DATE
           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.
      *--> VALORIZZAZIONE STORICITA'
           SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.

      *--> NOME TABELLA FISICA DB
           MOVE 'PARAM-MOVI'             TO IDSI0011-CODICE-STR-DATO
                                            WKS-NOME-TABELLA.

      *--> DCLGEN TABELLA
           MOVE PARAM-MOVI               TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT           TO TRUE.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-ID-OGGETTO       TO TRUE.

       S1214-VALORIZZA-INPUT-PMO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA AREA PER LETTURA TRANCHE DI GARANZIA
      *----------------------------------------------------------------*
       S1215-VALORIZZA-INPUT-TGA.

           MOVE IVVC0213-ID-TRANCHE      TO TGA-ID-TRCH-DI-GAR.

      *--> VALORIZZAZIONE DATE
           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.

      *--> NOME TABELLA FISICA DB
           MOVE 'TRCH-DI-GAR'            TO IDSI0011-CODICE-STR-DATO
                                            WKS-NOME-TABELLA.

      *--> DCLGEN TABELLA
           MOVE TRCH-DI-GAR              TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT           TO TRUE.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID              TO TRUE.

       S1215-VALORIZZA-INPUT-TGA-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   VALORIZZA OUTPUT
      *----------------------------------------------------------------*
       S1220-VALORIZZA-OUT.

           MOVE DPOG-TP-D(IVVC0213-IX-TABB)
             TO IVVC0213-TP-DATO-O.

           EVALUATE IVVC0213-TP-DATO-O
             WHEN WS-IMPORTO
               IF DPOG-VAL-IMP-NULL(IVVC0213-IX-TABB) = HIGH-VALUE
                  MOVE ZEROES
                    TO IVVC0213-VAL-IMP-O
               ELSE
                  MOVE DPOG-VAL-IMP(IVVC0213-IX-TABB)
                    TO IVVC0213-VAL-IMP-O
               END-IF

             WHEN WS-NUMERO
               IF DPOG-VAL-NUM-NULL(IVVC0213-IX-TABB) = HIGH-VALUE
                  MOVE ZEROES
                    TO IVVC0213-VAL-IMP-O
               ELSE
                  MOVE DPOG-VAL-NUM(IVVC0213-IX-TABB)
                    TO IVVC0213-VAL-IMP-O
               END-IF

             WHEN WS-TASSO
             WHEN WS-MILLESIMI
               IF DPOG-VAL-TS-NULL(IVVC0213-IX-TABB) = HIGH-VALUE
                  MOVE ZEROES
                    TO IVVC0213-VAL-PERC-O
               ELSE
                  MOVE DPOG-VAL-TS(IVVC0213-IX-TABB)
                    TO IVVC0213-VAL-PERC-O
               END-IF

             WHEN WS-PERCENTUALE
               IF DPOG-VAL-PC-NULL(IVVC0213-IX-TABB) = HIGH-VALUE
                  MOVE ZEROES
                    TO IVVC0213-VAL-PERC-O
               ELSE
                  MOVE DPOG-VAL-PC(IVVC0213-IX-TABB)
                    TO IVVC0213-VAL-PERC-O
               END-IF

             WHEN WS-DATA
               IF DPOG-VAL-DT-NULL(IVVC0213-IX-TABB) = HIGH-VALUE
                  MOVE SPACES
                    TO IVVC0213-VAL-STR-O
               ELSE
                  MOVE DPOG-VAL-DT(IVVC0213-IX-TABB)
                    TO IVVC0213-VAL-STR-O
               END-IF

             WHEN WS-STRINGA
               IF DPOG-VAL-TXT(IVVC0213-IX-TABB) = HIGH-VALUE
                  MOVE SPACES
                    TO IVVC0213-VAL-STR-O
               ELSE
                  MOVE DPOG-VAL-TXT(IVVC0213-IX-TABB)
                    TO IVVC0213-VAL-STR-O
               END-IF

             WHEN WS-FLAG
               IF DPOG-VAL-FL(IVVC0213-IX-TABB) = HIGH-VALUE
                  MOVE SPACES
                    TO IVVC0213-VAL-STR-O
               ELSE
                  MOVE DPOG-VAL-FL(IVVC0213-IX-TABB)
                    TO IVVC0213-VAL-STR-O
               END-IF
           END-EVALUATE.

       S1220-VALORIZZA-OUT-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA AREA PER LETTURA PARAMETRI OGGETTO
      *----------------------------------------------------------------*
       S1300-VALORIZZA-INPUT-POG.

           INITIALIZE    PARAM-OGG.

           IF IVVC0213-ID-TRANCHE > ZEROES
              MOVE IVVC0213-ID-TRANCHE          TO POG-ID-OGG
              MOVE WS-TRANCHE                   TO POG-TP-OGG
           ELSE
              IF IVVC0213-ID-GARANZIA > ZEROES
                 MOVE IVVC0213-ID-GARANZIA      TO POG-ID-OGG
                 MOVE WS-GARANZIA               TO POG-TP-OGG
              ELSE
                 IF IVVC0213-ID-ADESIONE > ZEROES
                    MOVE IVVC0213-ID-ADESIONE   TO POG-ID-OGG
                    MOVE WS-ADESIONE            TO POG-TP-OGG
                 ELSE
                    IF IVVC0213-ID-POLIZZA > ZEROES
                       MOVE IVVC0213-ID-POLIZZA TO POG-ID-OGG
                       MOVE WS-POLIZZA          TO POG-TP-OGG
                    END-IF
                 END-IF
              END-IF
           END-IF.

           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           MOVE 'PARAM-OGG'              TO IDSI0011-CODICE-STR-DATO
                                            WKS-NOME-TABELLA.

           MOVE  PARAM-OGG               TO IDSI0011-BUFFER-DATI.

      *--> STORICITA
           SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-ID-OGGETTO       TO TRUE.

      *--> TIPO OPERAZIONE
           SET IDSI0011-FETCH-FIRST      TO TRUE.

      *--> SET VALORE MASSIMO PER FETCH
           MOVE WK-POG-MAX-B             TO DPOG-ELE-PARAM-OGG-MAX.

      *--> INIZIALIZZAZIONE INDICE
           MOVE ZEROES                   TO IX-TAB-POG.

       S1300-VALORIZZA-INPUT-POG-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA SULLA TABELLA PARAMETRO OGGETTO
      *----------------------------------------------------------------*
       LETTURA-POG.

           SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
           SET  WCOM-OVERFLOW-NO                  TO TRUE.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR WCOM-OVERFLOW-YES

              PERFORM CALL-DISPATCHER
                 THRU CALL-DISPATCHER-EX

              IF IDSO0011-SUCCESSFUL-RC
                EVALUATE TRUE

                    WHEN IDSO0011-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                         MOVE IDSO0011-BUFFER-DATI   TO PARAM-OGG
                         ADD 1                       TO IX-TAB-POG
                         IF IX-TAB-POG > DPOG-ELE-PARAM-OGG-MAX
                           SET WCOM-OVERFLOW-YES     TO TRUE
                         ELSE
                           PERFORM VALORIZZA-OUTPUT-POG
                              THRU VALORIZZA-OUTPUT-POG-EX
                           SET IDSI0011-FETCH-NEXT   TO TRUE
                         END-IF

                    WHEN IDSO0011-NOT-FOUND
      *-->          OCCORRENZA NON TROVATA
                         IF IDSI0011-FETCH-FIRST
                            SET IDSV0003-NOT-FOUND   TO TRUE
                            MOVE WK-PGM
                              TO IDSV0003-COD-SERVIZIO-BE
                            STRING WKS-NOME-TABELLA      ';'
                                   IDSO0011-RETURN-CODE  ';'
                                   IDSO0011-SQLCODE
                            DELIMITED BY SIZE
                            INTO IDSV0003-DESCRIZ-ERR-DB2
                            END-STRING
                         END-IF

                    WHEN OTHER
      *--->         ERRORE DI ACCESSO AL DB
                         SET IDSV0003-INVALID-OPER TO TRUE
                         MOVE WK-PGM
                           TO IDSV0003-COD-SERVIZIO-BE
                         STRING WKS-NOME-TABELLA      ';'
                                IDSO0011-RETURN-CODE  ';'
                                IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IDSV0003-DESCRIZ-ERR-DB2
                         END-STRING

                 END-EVALUATE
              ELSE
      *-->       GESTIRE ERRORE DISPATCHER
                 SET IDSV0003-INVALID-OPER TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 STRING WKS-NOME-TABELLA      ';'
                        IDSO0011-RETURN-CODE  ';'
                        IDSO0011-SQLCODE
                 DELIMITED BY SIZE
                 INTO IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING
              END-IF
           END-PERFORM.

       LETTURA-POG-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA SULLA TABELLA DATI COLLETTIVA
      *----------------------------------------------------------------*
       LETTURA-DCO.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

                 WHEN IDSO0011-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                      MOVE IDSO0011-BUFFER-DATI
                        TO D-COLL
                      PERFORM VALORIZZA-OUTPUT-DCO
                         THRU VALORIZZA-OUTPUT-DCO-EX

                 WHEN IDSO0011-NOT-FOUND
      *--->      CHIAVE NON TROVATA
                      SET IDSV0003-NOT-FOUND   TO TRUE
                      MOVE WK-PGM
                        TO IDSV0003-COD-SERVIZIO-BE
                      STRING WKS-NOME-TABELLA      ';'
                             IDSO0011-RETURN-CODE  ';'
                             IDSO0011-SQLCODE
                      DELIMITED BY SIZE
                      INTO IDSV0003-DESCRIZ-ERR-DB2
                      END-STRING

                 WHEN OTHER
      *--->      ERRORE DI ACCESSO AL DB
                      SET IDSV0003-INVALID-OPER TO TRUE
                      MOVE WK-PGM
                        TO IDSV0003-COD-SERVIZIO-BE
                      STRING WKS-NOME-TABELLA      ';'
                             IDSO0011-RETURN-CODE  ';'
                             IDSO0011-SQLCODE
                      DELIMITED BY SIZE
                      INTO IDSV0003-DESCRIZ-ERR-DB2
                      END-STRING

              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              SET IDSV0003-INVALID-OPER TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING WKS-NOME-TABELLA      ';'
                     IDSO0011-RETURN-CODE  ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE
              INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.

       LETTURA-DCO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA SULLA TABELLA ADESIONE
      *----------------------------------------------------------------*
       LETTURA-ADE.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--->       OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI   TO ADES
                       MOVE 1                      TO IX-TAB-ADE
                       PERFORM VALORIZZA-OUTPUT-ADE
                          THRU VALORIZZA-OUTPUT-ADE-EX

                 WHEN IDSO0011-NOT-FOUND
      *--->      CHIAVE NON TROVATA
                      SET IDSV0003-NOT-FOUND   TO TRUE
                      MOVE WK-PGM
                        TO IDSV0003-COD-SERVIZIO-BE
                      STRING WKS-NOME-TABELLA      ';'
                             IDSO0011-RETURN-CODE  ';'
                             IDSO0011-SQLCODE
                      DELIMITED BY SIZE
                      INTO IDSV0003-DESCRIZ-ERR-DB2
                      END-STRING

                 WHEN OTHER
      *--->      ERRORE DI ACCESSO AL DB
                      SET IDSV0003-INVALID-OPER TO TRUE
                      MOVE WK-PGM
                        TO IDSV0003-COD-SERVIZIO-BE
                      STRING WKS-NOME-TABELLA      ';'
                             IDSO0011-RETURN-CODE  ';'
                             IDSO0011-SQLCODE
                      DELIMITED BY SIZE
                      INTO IDSV0003-DESCRIZ-ERR-DB2
                      END-STRING

              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              SET IDSV0003-INVALID-OPER TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING WKS-NOME-TABELLA      ';'
                     IDSO0011-RETURN-CODE  ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE
              INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.

       LETTURA-ADE-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA SULLA TABELLA PARAMETRO MOVIMENTO
      *----------------------------------------------------------------*
       LETTURA-PMO.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI TO PARAM-MOVI
                     MOVE 1                    TO IX-TAB-PMO
                     PERFORM VALORIZZA-OUTPUT-PMO
                        THRU VALORIZZA-OUTPUT-PMO-EX

                 WHEN IDSO0011-NOT-FOUND
      *--->      CHIAVE NON TROVATA
                      SET IDSV0003-NOT-FOUND   TO TRUE
                      MOVE WK-PGM
                        TO IDSV0003-COD-SERVIZIO-BE
                      STRING WKS-NOME-TABELLA      ';'
                             IDSO0011-RETURN-CODE  ';'
                             IDSO0011-SQLCODE
                      DELIMITED BY SIZE
                      INTO IDSV0003-DESCRIZ-ERR-DB2
                      END-STRING

                 WHEN OTHER
      *--->      ERRORE DI ACCESSO AL DB
                      SET IDSV0003-INVALID-OPER TO TRUE
                      MOVE WK-PGM
                        TO IDSV0003-COD-SERVIZIO-BE
                      STRING WKS-NOME-TABELLA      ';'
                             IDSO0011-RETURN-CODE  ';'
                             IDSO0011-SQLCODE
                      DELIMITED BY SIZE
                      INTO IDSV0003-DESCRIZ-ERR-DB2
                      END-STRING

              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              SET IDSV0003-INVALID-OPER TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING WKS-NOME-TABELLA      ';'
                     IDSO0011-RETURN-CODE  ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE
              INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.

       LETTURA-PMO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA SULLA TABELLA GARANZIA
      *----------------------------------------------------------------*
       LETTURA-GRZ.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

                 WHEN IDSO0011-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                      MOVE IDSO0011-BUFFER-DATI TO GAR
                      MOVE 1                    TO IX-TAB-GRZ
                      PERFORM VALORIZZA-OUTPUT-GRZ
                         THRU VALORIZZA-OUTPUT-GRZ-EX

                 WHEN IDSO0011-NOT-FOUND
      *--->      CHIAVE NON TROVATA
                      SET IDSV0003-NOT-FOUND   TO TRUE
                      MOVE WK-PGM
                        TO IDSV0003-COD-SERVIZIO-BE
                      STRING WKS-NOME-TABELLA      ';'
                             IDSO0011-RETURN-CODE  ';'
                             IDSO0011-SQLCODE
                      DELIMITED BY SIZE
                      INTO IDSV0003-DESCRIZ-ERR-DB2
                      END-STRING

                 WHEN OTHER
      *--->      ERRORE DI ACCESSO AL DB
                      SET IDSV0003-INVALID-OPER TO TRUE
                      MOVE WK-PGM
                        TO IDSV0003-COD-SERVIZIO-BE
                      STRING WKS-NOME-TABELLA      ';'
                             IDSO0011-RETURN-CODE  ';'
                             IDSO0011-SQLCODE
                      DELIMITED BY SIZE
                      INTO IDSV0003-DESCRIZ-ERR-DB2
                      END-STRING

              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              SET IDSV0003-INVALID-OPER TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING WKS-NOME-TABELLA      ';'
                     IDSO0011-RETURN-CODE  ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE
              INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.

       LETTURA-GRZ-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA SULLA TABELLA TRANCHE DI GARANZIA
      *----------------------------------------------------------------*
       LETTURA-TGA.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

                 WHEN IDSO0011-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                      MOVE IDSO0011-BUFFER-DATI   TO TRCH-DI-GAR
                      MOVE 1                      TO IX-TAB-TGA
                      PERFORM VALORIZZA-OUTPUT-TGA
                         THRU VALORIZZA-OUTPUT-TGA-EX

                 WHEN IDSO0011-NOT-FOUND
      *--->      CHIAVE NON TROVATA
                      SET IDSV0003-NOT-FOUND   TO TRUE
                      MOVE WK-PGM
                        TO IDSV0003-COD-SERVIZIO-BE
                      STRING WKS-NOME-TABELLA      ';'
                             IDSO0011-RETURN-CODE  ';'
                             IDSO0011-SQLCODE
                      DELIMITED BY SIZE
                      INTO IDSV0003-DESCRIZ-ERR-DB2
                      END-STRING

                 WHEN OTHER
      *--->      ERRORE DI ACCESSO AL DB
                      SET IDSV0003-INVALID-OPER TO TRUE
                      MOVE WK-PGM
                        TO IDSV0003-COD-SERVIZIO-BE
                      STRING WKS-NOME-TABELLA      ';'
                             IDSO0011-RETURN-CODE  ';'
                             IDSO0011-SQLCODE
                      DELIMITED BY SIZE
                      INTO IDSV0003-DESCRIZ-ERR-DB2
                      END-STRING

              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              SET IDSV0003-INVALID-OPER TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING WKS-NOME-TABELLA      ';'
                     IDSO0011-RETURN-CODE  ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE
              INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.

       LETTURA-TGA-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           IF NOT IDSV0003-SUCCESSFUL-SQL
              SET IDSV0003-FIELD-NOT-VALUED   TO TRUE
           END-IF.

           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      * ROUTINES GESTIONE VALORIZZAZIONE OUTPUT DISPATCHER             *
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *    COPY VALORIZZA OUTPUT TABELLE
      *----------------------------------------------------------------*
      *--> PARAMETRO OGGETTO
           COPY LCCVPOG3     REPLACING ==(SF)== BY ==DPOG==.
      *--> DATI COLLETTIVA
           COPY LCCVDCO3     REPLACING ==(SF)== BY ==DDCO==.
      *--> ADESIONE
           COPY LCCVADE3     REPLACING ==(SF)== BY ==DADE==.
      *--> PARAMETRO MOVIMENTO
           COPY LCCVPMO3     REPLACING ==(SF)== BY ==DPMO==.
      *--> GARANZIA
           COPY LCCVGRZ3     REPLACING ==(SF)== BY ==DGRZ==.
      *--> TRANCHE DI GARANZIA
           COPY LCCVTGA3     REPLACING ==(SF)== BY ==DTGA==.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
      *****************************************************************
      *         CALL DISPATCHER
      *****************************************************************
       CALL-DISPATCHER.

           MOVE IDSV0003-MODALITA-ESECUTIVA
                              TO IDSI0011-MODALITA-ESECUTIVA
           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
           MOVE IDSV0003-COD-MAIN-BATCH
                              TO IDSI0011-COD-MAIN-BATCH
           MOVE IDSV0003-TIPO-MOVIMENTO
                              TO IDSI0011-TIPO-MOVIMENTO
           MOVE IDSV0003-SESSIONE
                              TO IDSI0011-SESSIONE
           MOVE IDSV0003-USER-NAME
                              TO IDSI0011-USER-NAME

           IF IDSI0011-DATA-INIZIO-EFFETTO = 0
              MOVE IDSV0003-DATA-INIZIO-EFFETTO
                TO IDSI0011-DATA-INIZIO-EFFETTO
           END-IF

           IF IDSI0011-DATA-FINE-EFFETTO = 0
              MOVE 99991231 TO IDSI0011-DATA-FINE-EFFETTO
           END-IF

           MOVE IDSV0003-DATA-COMP-AGG-STOR
                              TO IDSI0011-DATA-COMP-AGG-STOR

           IF IDSI0011-DATA-COMPETENZA = 0
              MOVE IDSV0003-DATA-COMPETENZA
                                       TO IDSI0011-DATA-COMPETENZA
           END-IF

           MOVE IDSV0003-FORMATO-DATA-DB
                                       TO IDSI0011-FORMATO-DATA-DB

           MOVE 0                      TO IDSI0011-ID-MOVI-ANNULLATO

           SET IDSI0011-PTF-NEWLIFE    TO TRUE

      *     SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE

      *     MOVE IDSI0011-AREA          TO IDSV0012-AREA-I-O
      *     SET WS-ADDRESS-DIS          TO ADDRESS OF IDSV0012

           MOVE 'IDSS0010'             TO WK-CALL-PGM
      *     CALL WK-CALL-PGM           USING DISPATCHER-ADDRESS
      *                                      IDSV0012.
      *     MOVE IDSV0012-AREA-I-O      TO IDSO0011-AREA.

           CALL WK-CALL-PGM            USING IDSI0011-AREA
                                             IDSO0011-AREA.
            MOVE IDSO0011-SQLCODE-SIGNED
                                  TO IDSO0011-SQLCODE.

       CALL-DISPATCHER-EX.
           EXIT.


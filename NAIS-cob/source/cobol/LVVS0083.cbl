      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0083.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0007
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... SOMMATORIA IMPORTO DI PRESTITO
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(08) VALUE 'LVVS0083'.
       01  WK-CALL-PGM                      PIC X(08) VALUE SPACES.

      *---------------------------------------------------------------
      *  COPY PER CHIAMATA LDBS2820
      *---------------------------------------------------------------
           COPY LDBV2821.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IVVC0213-TAB-OUTPUT
                                             LDBV2821.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           MOVE IVVC0213-ID-ADESIONE          TO LDBV2821-ID-OGG.
           MOVE 'AD'                          TO LDBV2821-TP-OGG.
           PERFORM S1100-SOMMA-IMP-PREST      THRU S1250-EX.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    SOMMATORIA IMPORTO DI PRESTITO
      *----------------------------------------------------------------*
       S1100-SOMMA-IMP-PREST.
      *
           MOVE 'LDBS2820'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 LDBV2821
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS2820 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE LDBV2821-IMP-PREST      TO IVVC0213-VAL-IMP-O
           ELSE
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBS2820 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                     DELIMITED BY SIZE INTO
                     IDSV0003-DESCRIZ-ERR-DB2
              END-STRING

              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
           END-IF.
      *
       S1250-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

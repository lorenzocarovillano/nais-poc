      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0052.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0052'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-TP-OGG                        PIC X(02) VALUE SPACES.
      *----------------------------------------------------------------*
      *    COPY DI INPUT PER LA CHIAMATA AL LVVS0000
      *----------------------------------------------------------------*
       01  INPUT-LVVS0000.
           COPY LVVC0000        REPLACING ==(SF)== BY ==LVVC0000==.
      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBS1700
      *---------------------------------------------------------------*
           COPY LDBV1701.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-POL.
          03 DPOL-AREA-POL.
             04 DPOL-ELE-POL-MAX       PIC S9(04) COMP.
             04 DPOL-TAB-POL.
             COPY LCCVPOL1              REPLACING ==(SF)== BY ==DPOL==.

       01 AREA-IO-TGA.
          03 DTGA-AREA-TGA.
             04 DTGA-ELE-TGA-MAX       PIC S9(04) COMP.
                COPY LCCVTGAC REPLACING   ==(SF)==  BY ==DTGA==.
             COPY LCCVTGA1              REPLACING ==(SF)== BY ==DTGA==.

       01 AREA-IO-GAR.
          03 DGRZ-AREA-GRA.
             04 DGRZ-ELE-GAR-MAX        PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==DGRZ==.
             COPY LCCVGRZ1              REPLACING ==(SF)== BY ==DGRZ==.

       01 AREA-IO-ADE.
          03 DADE-AREA-ADES.
             04 DADE-ELE-ADE-MAX       PIC S9(04) COMP.
             04 DADE-TAB-ADE.
             COPY LCCVADE1              REPLACING ==(SF)== BY ==DADE==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000
      *
           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.
      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           INITIALIZE LDBV1701.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

           MOVE IVVC0213-WHERE-COND(1:2)     TO WK-TP-OGG.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1200-CONTROLLO-DATI
                  THRU S1200-CONTROLLO-DATI-EX
           END-IF.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL

               IF (WK-TP-OGG = 'PO' AND IVVC0213-ID-POL-AUTENTICO) OR
                  (WK-TP-OGG = 'AD' AND IVVC0213-ID-ADE-AUTENTICO) OR
                  (WK-TP-OGG = 'GA' AND IVVC0213-ID-GRZ-AUTENTICO) OR
                  (WK-TP-OGG = 'TG' AND IVVC0213-ID-TGA-AUTENTICO)

                  EVALUATE WK-TP-OGG
                    WHEN 'PO'
                      MOVE IVVC0213-ID-POLIZZA  TO LDBV1701-ID-OGG
                      MOVE WK-TP-OGG            TO LDBV1701-TP-OGG
                    WHEN 'AD'
                      MOVE IVVC0213-ID-ADESIONE TO LDBV1701-ID-OGG
                      MOVE WK-TP-OGG            TO LDBV1701-TP-OGG
                    WHEN 'GA'
                      MOVE IVVC0213-ID-GARANZIA TO LDBV1701-ID-OGG
                      MOVE WK-TP-OGG            TO LDBV1701-TP-OGG
                    WHEN 'TG'
                      MOVE IVVC0213-ID-TRANCHE  TO LDBV1701-ID-OGG
                      MOVE WK-TP-OGG            TO LDBV1701-TP-OGG
                  END-EVALUATE

                  PERFORM S1250-CALCOLA-DATA1   THRU S1250-EX
               ELSE
                  MOVE 'TR'                     TO IVVC0213-VAL-STR-O
               END-IF
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-POLI
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPOL-AREA-POL
           END-IF.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-TRCH-GAR
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DTGA-AREA-TGA
           END-IF.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-GARANZIA
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DGRZ-AREA-GRA
           END-IF.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-ADES
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DADE-AREA-ADES
           END-IF.
      *
       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATA 1
      *----------------------------------------------------------------*
       S1250-CALCOLA-DATA1.
      *
           SET IDSV0003-WHERE-CONDITION        TO TRUE.
      *
           MOVE 'LDBS1700'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 LDBV1701
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS1700 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE LDBV1701-TP-STAT        TO IVVC0213-VAL-STR-O
           ELSE
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBS1700 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
           END-IF.
      *
       S1250-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO DATI DCLGEN
      *----------------------------------------------------------------*
       S1200-CONTROLLO-DATI.
      *    MOVE DPOL-TP-FRM-ASSVA            TO WS-TP-FRM-ASSVA
           IF WK-TP-OGG = 'GA' OR 'TG'
              IF DTGA-ELE-TGA-MAX GREATER ZERO
                 IF IVVC0213-ID-TRANCHE IS NUMERIC
                    IF IVVC0213-ID-TRANCHE = 0
                      SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                      MOVE WK-PGM
                        TO IDSV0003-COD-SERVIZIO-BE
                      MOVE 'ID-POLIZZA NON VALORIZZATO'
                        TO IDSV0003-DESCRIZ-ERR-DB2
                    END-IF
                 ELSE
                    SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                    MOVE WK-PGM
                      TO IDSV0003-COD-SERVIZIO-BE
                    MOVE 'ID-TRCH NON VALORIZZATO'
                      TO IDSV0003-DESCRIZ-ERR-DB2
                 END-IF
              END-IF
           END-IF.

           IF WK-TP-OGG = 'PO'
              IF IVVC0213-ID-POLIZZA IS NUMERIC
                 IF IVVC0213-ID-POLIZZA = 0
                   SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                   MOVE WK-PGM
                     TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'ID-POLIZZA NON VALORIZZATO'
                     TO IDSV0003-DESCRIZ-ERR-DB2
                 END-IF
              ELSE
                 SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'ID-POLIZZA NON VALORIZZATO'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
           END-IF.

       S1200-CONTROLLO-DATI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

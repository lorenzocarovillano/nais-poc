      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0037.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0037'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
42683  01  LDBS6040                         PIC X(008) VALUE 'LDBS6040'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DATA-OUTPUT                  PIC 9(04)V9(07).
       01  WK-DATA-OUTPUT-V REDEFINES WK-DATA-OUTPUT.
           03 WK-NUMERICI                  PIC 9(04).
           03 WK-DECIMALI                  PIC 9(07).

       01 WK-DATA-X-12.
          03 WK-X-AA                          PIC X(04).
          03 WK-VIROGLA                       PIC X(01) VALUE ','.
          03 WK-X-GG                          PIC X(07).

42683  01 WS-MOVIMENTO-ORIG                   PIC 9(05) VALUE ZEROES.

      *---------------------------------------------------------------*
      *  LISTA TABELLE E TIPOLOGICHE
      *---------------------------------------------------------------*
42683     COPY LCCVXMVZ.
42683     COPY LCCVXMV2.
42683     COPY LCCVXMV3.
42683     COPY LCCVXMV5.
42683     COPY LCCVXMV6.

      *----------------------------------------------------------------*
      *    FLAG
      *----------------------------------------------------------------*
42683  01 FLAG-CUR-MOV                       PIC X(01).
42683      88 INIT-CUR-MOV                   VALUE 'S'.
42683      88 FINE-CUR-MOV                   VALUE 'N'.

42683  01 FLAG-COMUN-TROV                    PIC X(01).
42683      88 COMUN-TROV-SI                  VALUE 'S'.
42683      88 COMUN-TROV-NO                  VALUE 'N'.

42683  01 FLAG-ULTIMA-LETTURA               PIC X(01).
42683     88 SI-ULTIMA-LETTURA              VALUE 'S'.
42683     88 NO-ULTIMA-LETTURA              VALUE 'N'.

42683  01  WK-VAR-MOVI-COMUN.
42683      05 WK-ID-MOVI-COMUN             PIC S9(09) COMP-3.
42683      05 WK-DATA-EFF-PREC             PIC S9(08) COMP-3.
42683      05 WK-DATA-CPTZ-PREC            PIC S9(18) COMP-3.
42683      05 WK-DATA-EFF-RIP              PIC S9(08) COMP-3.
42683      05 WK-DATA-CPTZ-RIP             PIC S9(18) COMP-3.

      *---------------------------------------------------------------*
      *  COPY DB2
      *---------------------------------------------------------------*
42683      COPY IDBVMOV1.

      *----------------------------------------------------------------*
      *    COPY DI INPUT PER LA CHIAMATA AL LVVS0000
      *----------------------------------------------------------------*
       01  INPUT-LVVS0000.
           COPY LVVC0000        REPLACING ==(SF)== BY ==LVVC0000==.
      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBS1700
      *---------------------------------------------------------------*
           COPY LDBV1701.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-POL.
          03 DPOL-AREA-POL.
             04 DPOL-ELE-POL-MAX       PIC S9(04) COMP.
             04 DPOL-TAB-POL.
             COPY LCCVPOL1              REPLACING ==(SF)== BY ==DPOL==.

       01 AREA-IO-ADE.
          03 DADE-AREA-ADES.
             04 DADE-ELE-ADES-MAX       PIC S9(04) COMP.
             04 DADE-TAB-ADES.
             COPY LCCVADE1              REPLACING ==(SF)== BY ==DADE==.

       01 AREA-IO-GAR.
          03 DGRZ-AREA-GAR.
             04 DGRZ-ELE-GAR-MAX       PIC S9(04) COMP.
             04 DGRZ-TAB-GAR.
             COPY LCCVGRZ1              REPLACING ==(SF)== BY ==DGRZ==.

       01 AREA-IO-TGA.
          03 DTGA-AREA-TGA.
             04 DTGA-ELE-TGA-MAX       PIC S9(04) COMP.
             04 DTGA-TAB-TGA.
             COPY LCCVTGA1              REPLACING ==(SF)== BY ==DTGA==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           INITIALIZE LDBV1701
                      AREA-IO-POL
                      AREA-IO-ADE
                      AREA-IO-GAR
                      AREA-IO-TGA.

44861      SET COMUN-TROV-NO              TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE WK-DATA-OUTPUT
                      WK-DATA-X-12.


      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1200-CONTROLLO-DATI
                  THRU S1200-CONTROLLO-DATI-EX
           END-IF.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL

               IF DPOL-ELE-POL-MAX GREATER ZERO
                  MOVE IVVC0213-ID-POLIZZA     TO LDBV1701-ID-OGG
                  MOVE 'PO'                    TO LDBV1701-TP-OGG
               END-IF
               IF DADE-ELE-ADES-MAX GREATER ZERO
                  MOVE IVVC0213-ID-ADESIONE    TO LDBV1701-ID-OGG
                  MOVE 'AD'                    TO LDBV1701-TP-OGG
               END-IF
               IF DGRZ-ELE-GAR-MAX GREATER ZERO
                  MOVE IVVC0213-ID-GARANZIA    TO LDBV1701-ID-OGG
                  MOVE 'GA'                    TO LDBV1701-TP-OGG
               END-IF
               IF DTGA-ELE-TGA-MAX GREATER ZERO
                  MOVE IVVC0213-ID-TRANCHE     TO LDBV1701-ID-OGG
                  MOVE 'TG'                    TO LDBV1701-TP-OGG
               END-IF

               IF (LDBV1701-TP-OGG = 'PO' AND
                                     IVVC0213-ID-POL-AUTENTICO) OR
                  (LDBV1701-TP-OGG = 'AD' AND
                                     IVVC0213-ID-ADE-AUTENTICO) OR
                  (LDBV1701-TP-OGG = 'GA' AND
                                     IVVC0213-ID-GRZ-AUTENTICO) OR
                  (LDBV1701-TP-OGG = 'TG' AND
                                     IVVC0213-ID-TGA-AUTENTICO)

42683             PERFORM VERIFICA-MOVIMENTO
42683                THRU VERIFICA-MOVIMENTO-EX

42683             IF IDSV0003-SUCCESSFUL-RC
42683                PERFORM S1250-CALCOLA-DATA1    THRU S1250-EX
42683             END-IF
               ELSE
                  SET IVVC0213-SALTA-SI          TO TRUE
               END-IF
           END-IF.
      *
       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    VERIFICA MOVIMENTO
      *----------------------------------------------------------------*
42683  VERIFICA-MOVIMENTO.

      *--> SE CI TROVIAMO IN FASE DI LIQUIDAZIONE DEVO RECUPERARE LE
      *--> IMMAGINI IN FASE DI COMUNICAZIONE
           MOVE IVVC0213-TIPO-MOVI-ORIG
             TO WS-MOVIMENTO
42683           WS-MOVIMENTO-ORIG

           IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
              LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND OR
10819         LIQUI-RPP-TAKE-PROFIT
10819X     OR LIQUI-RISTOT-INCAPIENZA
10819X     OR LIQUI-RPP-REDDITO-PROGR
10819      OR LIQUI-RPP-BENEFICIO-CONTR
FNZS2      OR LIQUI-RISTOT-INCAP
      *--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
              PERFORM RECUP-MOVI-COMUN
                 THRU RECUP-MOVI-COMUN-EX

              IF COMUN-TROV-SI
                 MOVE IDSV0003-DATA-COMPETENZA
                   TO WK-DATA-CPTZ-RIP

                 MOVE IDSV0003-DATA-INIZIO-EFFETTO
                   TO WK-DATA-EFF-RIP

                 MOVE WK-DATA-CPTZ-PREC
                   TO IDSV0003-DATA-COMPETENZA

                 MOVE WK-DATA-EFF-PREC
                   TO IDSV0003-DATA-INIZIO-EFFETTO
                      IDSV0003-DATA-FINE-EFFETTO
              END-IF
           END-IF.

42683  VERIFICA-MOVIMENTO-EX.
42683      EXIT.

      *----------------------------------------------------------------*
      *    Recupero il movimento di comunicazione
      *----------------------------------------------------------------*
42683  RECUP-MOVI-COMUN.

           SET INIT-CUR-MOV               TO TRUE
44861 *    SET COMUN-TROV-NO              TO TRUE

           INITIALIZE MOVI

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
           SET  NO-ULTIMA-LETTURA         TO TRUE

      *
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR FINE-CUR-MOV
                      OR COMUN-TROV-SI

              INITIALIZE MOVI
      *
              MOVE IVVC0213-ID-POLIZZA    TO MOV-ID-OGG
              MOVE 'PO'                   TO MOV-TP-OGG

              SET IDSV0003-TRATT-SENZA-STOR   TO TRUE

      *-->    LIVELLO OPERAZIONE
              SET IDSV0003-WHERE-CONDITION TO TRUE

      *-->    INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL TO TRUE

              CALL  LDBS6040   USING IDSV0003 MOVI
                ON EXCEPTION
                   MOVE LDBS6040          TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                     TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER   TO TRUE
              END-CALL


                IF IDSV0003-SUCCESSFUL-RC

                    EVALUATE TRUE
                        WHEN IDSV0003-NOT-FOUND
      *-->                NESSUN DATO IN TABELLA
      *-->                LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
      *-->                BOLLO IN INPUT
                          SET FINE-CUR-MOV TO TRUE

                        WHEN IDSV0003-SUCCESSFUL-SQL
      *-->                OPERAZIONE ESEGUITA CORRETTAMENTE
      *                   MOVE IDSV0003-BUFFER-DATI TO MOVI
      *                   TROVO IL MOVIMENTO DI
      *                   COMUNICAZIONE RISCATTO PARZIALE
                          MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                          EVALUATE TRUE
                             WHEN COMUN-RISPAR-IND
                             WHEN RISTO-INDIVI
                             WHEN VARIA-OPZION
                             WHEN SINIS-INDIVI
                             WHEN RECES-INDIVI
10819                        WHEN RPP-TAKE-PROFIT
10819X                       WHEN COMUN-RISTOT-INCAPIENZA
10819X                       WHEN RPP-REDDITO-PROGRAMMATO
10819                        WHEN RPP-BENEFICIO-CONTR
FNZS2                        WHEN COMUN-RISTOT-INCAP
                                 MOVE MOV-ID-MOVI    TO WK-ID-MOVI-COMUN
                                 SET SI-ULTIMA-LETTURA  TO TRUE
                                 MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                                 COMPUTE WK-DATA-CPTZ-PREC =
                                         MOV-DS-TS-CPTZ - 1
                                 SET COMUN-TROV-SI   TO TRUE
                                 PERFORM CLOSE-MOVI
                                     THRU CLOSE-MOVI-EX
                                 SET FINE-CUR-MOV   TO TRUE
                             WHEN OTHER
                                 SET IDSV0003-FETCH-NEXT   TO TRUE
                          END-EVALUATE
                        WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                             MOVE LDBS6040
                               TO IDSV0003-COD-SERVIZIO-BE
                             MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                               TO IDSV0003-DESCRIZ-ERR-DB2
                             SET IDSV0003-INVALID-OPER   TO TRUE
                    END-EVALUATE
                 ELSE
      *--> GESTIRE ERRORE
                    MOVE LDBS6040
                      TO IDSV0003-COD-SERVIZIO-BE
                    MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                      TO IDSV0003-DESCRIZ-ERR-DB2
                    SET IDSV0003-INVALID-OPER   TO TRUE
                 END-IF
           END-PERFORM.

           MOVE IDSV0003-TIPO-MOVIMENTO         TO WS-MOVIMENTO.

42683  RECUP-MOVI-COMUN-EX.
42683      EXIT.

      *----------------------------------------------------------------*
      *    CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
      *----------------------------------------------------------------*
42683  CLOSE-MOVI.

           SET IDSV0003-CLOSE-CURSOR            TO TRUE.

           CALL LDBS6040 USING IDSV0003 MOVI
           ON EXCEPTION
              MOVE LDBS6040
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CALL LDBS6040 CLOSE'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER TO TRUE
           END-CALL.

           IF NOT IDSV0003-SUCCESSFUL-RC
           OR NOT IDSV0003-SUCCESSFUL-SQL
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CLOSE CURSORE LDBS6040'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER
                TO TRUE
           END-IF.

42683  CLOSE-MOVI-EX.
42683      EXIT.

      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-POLI
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPOL-AREA-POL
           END-IF.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-ADES
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DADE-AREA-ADES
           END-IF.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-GARANZIA
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DGRZ-AREA-GAR
           END-IF.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-TRCH-GAR
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DTGA-AREA-TGA
           END-IF.
      *
      *
       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATA 1
      *----------------------------------------------------------------*
       S1250-CALCOLA-DATA1.
      *
           SET IDSV0003-WHERE-CONDITION        TO TRUE.
42683      SET IDSV0003-SELECT                 TO TRUE.
42683      SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE.

42683      SET IDSV0003-SUCCESSFUL-SQL         TO TRUE.
42683      SET IDSV0003-SUCCESSFUL-RC          TO TRUE.
      *
           MOVE 'LDBS1700'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 LDBV1701
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS1700 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE LDBV1701-TP-CAUS        TO IVVC0213-VAL-STR-O
           ELSE
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBS1700 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
           END-IF.
      *
       S1250-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO DATI DCLGEN
      *----------------------------------------------------------------*
       S1200-CONTROLLO-DATI.
      *    MOVE DPOL-TP-FRM-ASSVA            TO WS-TP-FRM-ASSVA
           IF DTGA-ELE-TGA-MAX GREATER ZERO
              IF IVVC0213-ID-TRANCHE IS NUMERIC
                 IF IVVC0213-ID-TRANCHE = 0
                   SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                   MOVE WK-PGM
                     TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'ID-POLIZZA NON VALORIZZATO'
                     TO IDSV0003-DESCRIZ-ERR-DB2
                 END-IF
              ELSE
                 SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'ID-TRCH NON VALORIZZATO'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
           END-IF.

           IF DPOL-ELE-POL-MAX GREATER ZEROES
              IF IVVC0213-ID-POLIZZA IS NUMERIC
                 IF IVVC0213-ID-POLIZZA = 0
                   SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                   MOVE WK-PGM
                     TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'ID-POLIZZA NON VALORIZZATO'
                     TO IDSV0003-DESCRIZ-ERR-DB2
                 END-IF
              ELSE
                 SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'ID-POLIZZA NON VALORIZZATO'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
           END-IF.

           IF DADE-ELE-ADES-MAX GREATER ZEROES
              IF IVVC0213-ID-ADESIONE IS NUMERIC
                 IF IVVC0213-ID-ADESIONE = 0
                   SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                   MOVE WK-PGM
                     TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'ID-POLIZZA NON VALORIZZATO'
                     TO IDSV0003-DESCRIZ-ERR-DB2
                 END-IF
              ELSE
                 SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'ID-POLIZZA NON VALORIZZATO'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
           END-IF.

           IF DGRZ-ELE-GAR-MAX GREATER ZEROES
              IF IVVC0213-ID-GARANZIA IS NUMERIC
                 IF IVVC0213-ID-GARANZIA = 0
                   SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                   MOVE WK-PGM
                     TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'ID-POLIZZA NON VALORIZZATO'
                     TO IDSV0003-DESCRIZ-ERR-DB2
                 END-IF
              ELSE
                 SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'ID-POLIZZA NON VALORIZZATO'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
           END-IF.

       S1200-CONTROLLO-DATI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
42683      IF COMUN-TROV-SI
42683         MOVE WS-MOVIMENTO-ORIG
42683           TO IDSV0003-TIPO-MOVIMENTO

42683         MOVE WK-DATA-CPTZ-RIP
42683           TO IDSV0003-DATA-COMPETENZA

42683         MOVE WK-DATA-EFF-RIP
42683           TO IDSV0003-DATA-INIZIO-EFFETTO
42683              IDSV0003-DATA-FINE-EFFETTO
42683      END-IF

           GOBACK.
      *
       EX-S9000.
           EXIT.

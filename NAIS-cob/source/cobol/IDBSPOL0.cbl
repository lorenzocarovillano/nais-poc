       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSPOL0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  07 DIC 2017.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.

NEWP   01 WK-ID-OGG               PIC 9(009)  VALUE ZEROES.

           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDPOL0 END-EXEC.
           EXEC SQL INCLUDE IDBVPOL2 END-EXEC.
           EXEC SQL INCLUDE IDBVPOL3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVPOL1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 POLI.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSPOL0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'POLI' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,IB_OGG
                ,IB_PROP
                ,DT_PROP
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_EMIS
                ,TP_POLI
                ,DUR_AA
                ,DUR_MM
                ,DT_SCAD
                ,COD_PROD
                ,DT_INI_VLDT_PROD
                ,COD_CONV
                ,COD_RAMO
                ,DT_INI_VLDT_CONV
                ,DT_APPLZ_CONV
                ,TP_FRM_ASSVA
                ,TP_RGM_FISC
                ,FL_ESTAS
                ,FL_RSH_COMUN
                ,FL_RSH_COMUN_COND
                ,TP_LIV_GENZ_TIT
                ,FL_COP_FINANZ
                ,TP_APPLZ_DIR
                ,SPE_MED
                ,DIR_EMIS
                ,DIR_1O_VERS
                ,DIR_VERS_AGG
                ,COD_DVS
                ,FL_FNT_AZ
                ,FL_FNT_ADER
                ,FL_FNT_TFR
                ,FL_FNT_VOLO
                ,TP_OPZ_A_SCAD
                ,AA_DIFF_PROR_DFLT
                ,FL_VER_PROD
                ,DUR_GG
                ,DIR_QUIET
                ,TP_PTF_ESTNO
                ,FL_CUM_PRE_CNTR
                ,FL_AMMB_MOVI
                ,CONV_GECO
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_SCUDO_FISC
                ,FL_TRASFE
                ,FL_TFR_STRC
                ,DT_PRESC
                ,COD_CONV_AGG
                ,SUBCAT_PROD
                ,FL_QUEST_ADEGZ_ASS
                ,COD_TPA
                ,ID_ACC_COMM
                ,FL_POLI_CPI_PR
                ,FL_POLI_BUNDLING
                ,IND_POLI_PRIN_COLL
                ,FL_VND_BUNDLE
                ,IB_BS
                ,FL_POLI_IFP
             INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
             FROM POLI
             WHERE     DS_RIGA = :POL-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO POLI
                     (
                        ID_POLI
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,IB_OGG
                       ,IB_PROP
                       ,DT_PROP
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,DT_DECOR
                       ,DT_EMIS
                       ,TP_POLI
                       ,DUR_AA
                       ,DUR_MM
                       ,DT_SCAD
                       ,COD_PROD
                       ,DT_INI_VLDT_PROD
                       ,COD_CONV
                       ,COD_RAMO
                       ,DT_INI_VLDT_CONV
                       ,DT_APPLZ_CONV
                       ,TP_FRM_ASSVA
                       ,TP_RGM_FISC
                       ,FL_ESTAS
                       ,FL_RSH_COMUN
                       ,FL_RSH_COMUN_COND
                       ,TP_LIV_GENZ_TIT
                       ,FL_COP_FINANZ
                       ,TP_APPLZ_DIR
                       ,SPE_MED
                       ,DIR_EMIS
                       ,DIR_1O_VERS
                       ,DIR_VERS_AGG
                       ,COD_DVS
                       ,FL_FNT_AZ
                       ,FL_FNT_ADER
                       ,FL_FNT_TFR
                       ,FL_FNT_VOLO
                       ,TP_OPZ_A_SCAD
                       ,AA_DIFF_PROR_DFLT
                       ,FL_VER_PROD
                       ,DUR_GG
                       ,DIR_QUIET
                       ,TP_PTF_ESTNO
                       ,FL_CUM_PRE_CNTR
                       ,FL_AMMB_MOVI
                       ,CONV_GECO
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,FL_SCUDO_FISC
                       ,FL_TRASFE
                       ,FL_TFR_STRC
                       ,DT_PRESC
                       ,COD_CONV_AGG
                       ,SUBCAT_PROD
                       ,FL_QUEST_ADEGZ_ASS
                       ,COD_TPA
                       ,ID_ACC_COMM
                       ,FL_POLI_CPI_PR
                       ,FL_POLI_BUNDLING
                       ,IND_POLI_PRIN_COLL
                       ,FL_VND_BUNDLE
                       ,IB_BS
                       ,FL_POLI_IFP
                     )
                 VALUES
                     (
                       :POL-ID-POLI
                       ,:POL-ID-MOVI-CRZ
                       ,:POL-ID-MOVI-CHIU
                        :IND-POL-ID-MOVI-CHIU
                       ,:POL-IB-OGG
                        :IND-POL-IB-OGG
                       ,:POL-IB-PROP
                       ,:POL-DT-PROP-DB
                        :IND-POL-DT-PROP
                       ,:POL-DT-INI-EFF-DB
                       ,:POL-DT-END-EFF-DB
                       ,:POL-COD-COMP-ANIA
                       ,:POL-DT-DECOR-DB
                       ,:POL-DT-EMIS-DB
                       ,:POL-TP-POLI
                       ,:POL-DUR-AA
                        :IND-POL-DUR-AA
                       ,:POL-DUR-MM
                        :IND-POL-DUR-MM
                       ,:POL-DT-SCAD-DB
                        :IND-POL-DT-SCAD
                       ,:POL-COD-PROD
                       ,:POL-DT-INI-VLDT-PROD-DB
                       ,:POL-COD-CONV
                        :IND-POL-COD-CONV
                       ,:POL-COD-RAMO
                        :IND-POL-COD-RAMO
                       ,:POL-DT-INI-VLDT-CONV-DB
                        :IND-POL-DT-INI-VLDT-CONV
                       ,:POL-DT-APPLZ-CONV-DB
                        :IND-POL-DT-APPLZ-CONV
                       ,:POL-TP-FRM-ASSVA
                       ,:POL-TP-RGM-FISC
                        :IND-POL-TP-RGM-FISC
                       ,:POL-FL-ESTAS
                        :IND-POL-FL-ESTAS
                       ,:POL-FL-RSH-COMUN
                        :IND-POL-FL-RSH-COMUN
                       ,:POL-FL-RSH-COMUN-COND
                        :IND-POL-FL-RSH-COMUN-COND
                       ,:POL-TP-LIV-GENZ-TIT
                       ,:POL-FL-COP-FINANZ
                        :IND-POL-FL-COP-FINANZ
                       ,:POL-TP-APPLZ-DIR
                        :IND-POL-TP-APPLZ-DIR
                       ,:POL-SPE-MED
                        :IND-POL-SPE-MED
                       ,:POL-DIR-EMIS
                        :IND-POL-DIR-EMIS
                       ,:POL-DIR-1O-VERS
                        :IND-POL-DIR-1O-VERS
                       ,:POL-DIR-VERS-AGG
                        :IND-POL-DIR-VERS-AGG
                       ,:POL-COD-DVS
                        :IND-POL-COD-DVS
                       ,:POL-FL-FNT-AZ
                        :IND-POL-FL-FNT-AZ
                       ,:POL-FL-FNT-ADER
                        :IND-POL-FL-FNT-ADER
                       ,:POL-FL-FNT-TFR
                        :IND-POL-FL-FNT-TFR
                       ,:POL-FL-FNT-VOLO
                        :IND-POL-FL-FNT-VOLO
                       ,:POL-TP-OPZ-A-SCAD
                        :IND-POL-TP-OPZ-A-SCAD
                       ,:POL-AA-DIFF-PROR-DFLT
                        :IND-POL-AA-DIFF-PROR-DFLT
                       ,:POL-FL-VER-PROD
                        :IND-POL-FL-VER-PROD
                       ,:POL-DUR-GG
                        :IND-POL-DUR-GG
                       ,:POL-DIR-QUIET
                        :IND-POL-DIR-QUIET
                       ,:POL-TP-PTF-ESTNO
                        :IND-POL-TP-PTF-ESTNO
                       ,:POL-FL-CUM-PRE-CNTR
                        :IND-POL-FL-CUM-PRE-CNTR
                       ,:POL-FL-AMMB-MOVI
                        :IND-POL-FL-AMMB-MOVI
                       ,:POL-CONV-GECO
                        :IND-POL-CONV-GECO
                       ,:POL-DS-RIGA
                       ,:POL-DS-OPER-SQL
                       ,:POL-DS-VER
                       ,:POL-DS-TS-INI-CPTZ
                       ,:POL-DS-TS-END-CPTZ
                       ,:POL-DS-UTENTE
                       ,:POL-DS-STATO-ELAB
                       ,:POL-FL-SCUDO-FISC
                        :IND-POL-FL-SCUDO-FISC
                       ,:POL-FL-TRASFE
                        :IND-POL-FL-TRASFE
                       ,:POL-FL-TFR-STRC
                        :IND-POL-FL-TFR-STRC
                       ,:POL-DT-PRESC-DB
                        :IND-POL-DT-PRESC
                       ,:POL-COD-CONV-AGG
                        :IND-POL-COD-CONV-AGG
                       ,:POL-SUBCAT-PROD
                        :IND-POL-SUBCAT-PROD
                       ,:POL-FL-QUEST-ADEGZ-ASS
                        :IND-POL-FL-QUEST-ADEGZ-ASS
                       ,:POL-COD-TPA
                        :IND-POL-COD-TPA
                       ,:POL-ID-ACC-COMM
                        :IND-POL-ID-ACC-COMM
                       ,:POL-FL-POLI-CPI-PR
                        :IND-POL-FL-POLI-CPI-PR
                       ,:POL-FL-POLI-BUNDLING
                        :IND-POL-FL-POLI-BUNDLING
                       ,:POL-IND-POLI-PRIN-COLL
                        :IND-POL-IND-POLI-PRIN-COLL
                       ,:POL-FL-VND-BUNDLE
                        :IND-POL-FL-VND-BUNDLE
                       ,:POL-IB-BS
                        :IND-POL-IB-BS
                       ,:POL-FL-POLI-IFP
                        :IND-POL-FL-POLI-IFP
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE POLI SET

                   ID_POLI                =
                :POL-ID-POLI
                  ,ID_MOVI_CRZ            =
                :POL-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :POL-ID-MOVI-CHIU
                                       :IND-POL-ID-MOVI-CHIU
                  ,IB_OGG                 =
                :POL-IB-OGG
                                       :IND-POL-IB-OGG
                  ,IB_PROP                =
                :POL-IB-PROP
                  ,DT_PROP                =
           :POL-DT-PROP-DB
                                       :IND-POL-DT-PROP
                  ,DT_INI_EFF             =
           :POL-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :POL-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :POL-COD-COMP-ANIA
                  ,DT_DECOR               =
           :POL-DT-DECOR-DB
                  ,DT_EMIS                =
           :POL-DT-EMIS-DB
                  ,TP_POLI                =
                :POL-TP-POLI
                  ,DUR_AA                 =
                :POL-DUR-AA
                                       :IND-POL-DUR-AA
                  ,DUR_MM                 =
                :POL-DUR-MM
                                       :IND-POL-DUR-MM
                  ,DT_SCAD                =
           :POL-DT-SCAD-DB
                                       :IND-POL-DT-SCAD
                  ,COD_PROD               =
                :POL-COD-PROD
                  ,DT_INI_VLDT_PROD       =
           :POL-DT-INI-VLDT-PROD-DB
                  ,COD_CONV               =
                :POL-COD-CONV
                                       :IND-POL-COD-CONV
                  ,COD_RAMO               =
                :POL-COD-RAMO
                                       :IND-POL-COD-RAMO
                  ,DT_INI_VLDT_CONV       =
           :POL-DT-INI-VLDT-CONV-DB
                                       :IND-POL-DT-INI-VLDT-CONV
                  ,DT_APPLZ_CONV          =
           :POL-DT-APPLZ-CONV-DB
                                       :IND-POL-DT-APPLZ-CONV
                  ,TP_FRM_ASSVA           =
                :POL-TP-FRM-ASSVA
                  ,TP_RGM_FISC            =
                :POL-TP-RGM-FISC
                                       :IND-POL-TP-RGM-FISC
                  ,FL_ESTAS               =
                :POL-FL-ESTAS
                                       :IND-POL-FL-ESTAS
                  ,FL_RSH_COMUN           =
                :POL-FL-RSH-COMUN
                                       :IND-POL-FL-RSH-COMUN
                  ,FL_RSH_COMUN_COND      =
                :POL-FL-RSH-COMUN-COND
                                       :IND-POL-FL-RSH-COMUN-COND
                  ,TP_LIV_GENZ_TIT        =
                :POL-TP-LIV-GENZ-TIT
                  ,FL_COP_FINANZ          =
                :POL-FL-COP-FINANZ
                                       :IND-POL-FL-COP-FINANZ
                  ,TP_APPLZ_DIR           =
                :POL-TP-APPLZ-DIR
                                       :IND-POL-TP-APPLZ-DIR
                  ,SPE_MED                =
                :POL-SPE-MED
                                       :IND-POL-SPE-MED
                  ,DIR_EMIS               =
                :POL-DIR-EMIS
                                       :IND-POL-DIR-EMIS
                  ,DIR_1O_VERS            =
                :POL-DIR-1O-VERS
                                       :IND-POL-DIR-1O-VERS
                  ,DIR_VERS_AGG           =
                :POL-DIR-VERS-AGG
                                       :IND-POL-DIR-VERS-AGG
                  ,COD_DVS                =
                :POL-COD-DVS
                                       :IND-POL-COD-DVS
                  ,FL_FNT_AZ              =
                :POL-FL-FNT-AZ
                                       :IND-POL-FL-FNT-AZ
                  ,FL_FNT_ADER            =
                :POL-FL-FNT-ADER
                                       :IND-POL-FL-FNT-ADER
                  ,FL_FNT_TFR             =
                :POL-FL-FNT-TFR
                                       :IND-POL-FL-FNT-TFR
                  ,FL_FNT_VOLO            =
                :POL-FL-FNT-VOLO
                                       :IND-POL-FL-FNT-VOLO
                  ,TP_OPZ_A_SCAD          =
                :POL-TP-OPZ-A-SCAD
                                       :IND-POL-TP-OPZ-A-SCAD
                  ,AA_DIFF_PROR_DFLT      =
                :POL-AA-DIFF-PROR-DFLT
                                       :IND-POL-AA-DIFF-PROR-DFLT
                  ,FL_VER_PROD            =
                :POL-FL-VER-PROD
                                       :IND-POL-FL-VER-PROD
                  ,DUR_GG                 =
                :POL-DUR-GG
                                       :IND-POL-DUR-GG
                  ,DIR_QUIET              =
                :POL-DIR-QUIET
                                       :IND-POL-DIR-QUIET
                  ,TP_PTF_ESTNO           =
                :POL-TP-PTF-ESTNO
                                       :IND-POL-TP-PTF-ESTNO
                  ,FL_CUM_PRE_CNTR        =
                :POL-FL-CUM-PRE-CNTR
                                       :IND-POL-FL-CUM-PRE-CNTR
                  ,FL_AMMB_MOVI           =
                :POL-FL-AMMB-MOVI
                                       :IND-POL-FL-AMMB-MOVI
                  ,CONV_GECO              =
                :POL-CONV-GECO
                                       :IND-POL-CONV-GECO
                  ,DS_RIGA                =
                :POL-DS-RIGA
                  ,DS_OPER_SQL            =
                :POL-DS-OPER-SQL
                  ,DS_VER                 =
                :POL-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :POL-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :POL-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :POL-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :POL-DS-STATO-ELAB
                  ,FL_SCUDO_FISC          =
                :POL-FL-SCUDO-FISC
                                       :IND-POL-FL-SCUDO-FISC
                  ,FL_TRASFE              =
                :POL-FL-TRASFE
                                       :IND-POL-FL-TRASFE
                  ,FL_TFR_STRC            =
                :POL-FL-TFR-STRC
                                       :IND-POL-FL-TFR-STRC
                  ,DT_PRESC               =
           :POL-DT-PRESC-DB
                                       :IND-POL-DT-PRESC
                  ,COD_CONV_AGG           =
                :POL-COD-CONV-AGG
                                       :IND-POL-COD-CONV-AGG
                  ,SUBCAT_PROD            =
                :POL-SUBCAT-PROD
                                       :IND-POL-SUBCAT-PROD
                  ,FL_QUEST_ADEGZ_ASS     =
                :POL-FL-QUEST-ADEGZ-ASS
                                       :IND-POL-FL-QUEST-ADEGZ-ASS
                  ,COD_TPA                =
                :POL-COD-TPA
                                       :IND-POL-COD-TPA
                  ,ID_ACC_COMM            =
                :POL-ID-ACC-COMM
                                       :IND-POL-ID-ACC-COMM
                  ,FL_POLI_CPI_PR         =
                :POL-FL-POLI-CPI-PR
                                       :IND-POL-FL-POLI-CPI-PR
                  ,FL_POLI_BUNDLING       =
                :POL-FL-POLI-BUNDLING
                                       :IND-POL-FL-POLI-BUNDLING
                  ,IND_POLI_PRIN_COLL     =
                :POL-IND-POLI-PRIN-COLL
                                       :IND-POL-IND-POLI-PRIN-COLL
                  ,FL_VND_BUNDLE          =
                :POL-FL-VND-BUNDLE
                                       :IND-POL-FL-VND-BUNDLE
                  ,IB_BS                  =
                :POL-IB-BS
                                       :IND-POL-IB-BS
                  ,FL_POLI_IFP            =
                :POL-FL-POLI-IFP
                                       :IND-POL-FL-POLI-IFP
                WHERE     DS_RIGA = :POL-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM POLI
                WHERE     DS_RIGA = :POL-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-POL CURSOR FOR
              SELECT
                     ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,IB_OGG
                    ,IB_PROP
                    ,DT_PROP
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_EMIS
                    ,TP_POLI
                    ,DUR_AA
                    ,DUR_MM
                    ,DT_SCAD
                    ,COD_PROD
                    ,DT_INI_VLDT_PROD
                    ,COD_CONV
                    ,COD_RAMO
                    ,DT_INI_VLDT_CONV
                    ,DT_APPLZ_CONV
                    ,TP_FRM_ASSVA
                    ,TP_RGM_FISC
                    ,FL_ESTAS
                    ,FL_RSH_COMUN
                    ,FL_RSH_COMUN_COND
                    ,TP_LIV_GENZ_TIT
                    ,FL_COP_FINANZ
                    ,TP_APPLZ_DIR
                    ,SPE_MED
                    ,DIR_EMIS
                    ,DIR_1O_VERS
                    ,DIR_VERS_AGG
                    ,COD_DVS
                    ,FL_FNT_AZ
                    ,FL_FNT_ADER
                    ,FL_FNT_TFR
                    ,FL_FNT_VOLO
                    ,TP_OPZ_A_SCAD
                    ,AA_DIFF_PROR_DFLT
                    ,FL_VER_PROD
                    ,DUR_GG
                    ,DIR_QUIET
                    ,TP_PTF_ESTNO
                    ,FL_CUM_PRE_CNTR
                    ,FL_AMMB_MOVI
                    ,CONV_GECO
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,FL_SCUDO_FISC
                    ,FL_TRASFE
                    ,FL_TFR_STRC
                    ,DT_PRESC
                    ,COD_CONV_AGG
                    ,SUBCAT_PROD
                    ,FL_QUEST_ADEGZ_ASS
                    ,COD_TPA
                    ,ID_ACC_COMM
                    ,FL_POLI_CPI_PR
                    ,FL_POLI_BUNDLING
                    ,IND_POLI_PRIN_COLL
                    ,FL_VND_BUNDLE
                    ,IB_BS
                    ,FL_POLI_IFP
              FROM POLI
              WHERE     ID_POLI = :POL-ID-POLI
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,IB_OGG
                ,IB_PROP
                ,DT_PROP
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_EMIS
                ,TP_POLI
                ,DUR_AA
                ,DUR_MM
                ,DT_SCAD
                ,COD_PROD
                ,DT_INI_VLDT_PROD
                ,COD_CONV
                ,COD_RAMO
                ,DT_INI_VLDT_CONV
                ,DT_APPLZ_CONV
                ,TP_FRM_ASSVA
                ,TP_RGM_FISC
                ,FL_ESTAS
                ,FL_RSH_COMUN
                ,FL_RSH_COMUN_COND
                ,TP_LIV_GENZ_TIT
                ,FL_COP_FINANZ
                ,TP_APPLZ_DIR
                ,SPE_MED
                ,DIR_EMIS
                ,DIR_1O_VERS
                ,DIR_VERS_AGG
                ,COD_DVS
                ,FL_FNT_AZ
                ,FL_FNT_ADER
                ,FL_FNT_TFR
                ,FL_FNT_VOLO
                ,TP_OPZ_A_SCAD
                ,AA_DIFF_PROR_DFLT
                ,FL_VER_PROD
                ,DUR_GG
                ,DIR_QUIET
                ,TP_PTF_ESTNO
                ,FL_CUM_PRE_CNTR
                ,FL_AMMB_MOVI
                ,CONV_GECO
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_SCUDO_FISC
                ,FL_TRASFE
                ,FL_TFR_STRC
                ,DT_PRESC
                ,COD_CONV_AGG
                ,SUBCAT_PROD
                ,FL_QUEST_ADEGZ_ASS
                ,COD_TPA
                ,ID_ACC_COMM
                ,FL_POLI_CPI_PR
                ,FL_POLI_BUNDLING
                ,IND_POLI_PRIN_COLL
                ,FL_VND_BUNDLE
                ,IB_BS
                ,FL_POLI_IFP
             INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
             FROM POLI
             WHERE     ID_POLI = :POL-ID-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE POLI SET

                   ID_POLI                =
                :POL-ID-POLI
                  ,ID_MOVI_CRZ            =
                :POL-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :POL-ID-MOVI-CHIU
                                       :IND-POL-ID-MOVI-CHIU
                  ,IB_OGG                 =
                :POL-IB-OGG
                                       :IND-POL-IB-OGG
                  ,IB_PROP                =
                :POL-IB-PROP
                  ,DT_PROP                =
           :POL-DT-PROP-DB
                                       :IND-POL-DT-PROP
                  ,DT_INI_EFF             =
           :POL-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :POL-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :POL-COD-COMP-ANIA
                  ,DT_DECOR               =
           :POL-DT-DECOR-DB
                  ,DT_EMIS                =
           :POL-DT-EMIS-DB
                  ,TP_POLI                =
                :POL-TP-POLI
                  ,DUR_AA                 =
                :POL-DUR-AA
                                       :IND-POL-DUR-AA
                  ,DUR_MM                 =
                :POL-DUR-MM
                                       :IND-POL-DUR-MM
                  ,DT_SCAD                =
           :POL-DT-SCAD-DB
                                       :IND-POL-DT-SCAD
                  ,COD_PROD               =
                :POL-COD-PROD
                  ,DT_INI_VLDT_PROD       =
           :POL-DT-INI-VLDT-PROD-DB
                  ,COD_CONV               =
                :POL-COD-CONV
                                       :IND-POL-COD-CONV
                  ,COD_RAMO               =
                :POL-COD-RAMO
                                       :IND-POL-COD-RAMO
                  ,DT_INI_VLDT_CONV       =
           :POL-DT-INI-VLDT-CONV-DB
                                       :IND-POL-DT-INI-VLDT-CONV
                  ,DT_APPLZ_CONV          =
           :POL-DT-APPLZ-CONV-DB
                                       :IND-POL-DT-APPLZ-CONV
                  ,TP_FRM_ASSVA           =
                :POL-TP-FRM-ASSVA
                  ,TP_RGM_FISC            =
                :POL-TP-RGM-FISC
                                       :IND-POL-TP-RGM-FISC
                  ,FL_ESTAS               =
                :POL-FL-ESTAS
                                       :IND-POL-FL-ESTAS
                  ,FL_RSH_COMUN           =
                :POL-FL-RSH-COMUN
                                       :IND-POL-FL-RSH-COMUN
                  ,FL_RSH_COMUN_COND      =
                :POL-FL-RSH-COMUN-COND
                                       :IND-POL-FL-RSH-COMUN-COND
                  ,TP_LIV_GENZ_TIT        =
                :POL-TP-LIV-GENZ-TIT
                  ,FL_COP_FINANZ          =
                :POL-FL-COP-FINANZ
                                       :IND-POL-FL-COP-FINANZ
                  ,TP_APPLZ_DIR           =
                :POL-TP-APPLZ-DIR
                                       :IND-POL-TP-APPLZ-DIR
                  ,SPE_MED                =
                :POL-SPE-MED
                                       :IND-POL-SPE-MED
                  ,DIR_EMIS               =
                :POL-DIR-EMIS
                                       :IND-POL-DIR-EMIS
                  ,DIR_1O_VERS            =
                :POL-DIR-1O-VERS
                                       :IND-POL-DIR-1O-VERS
                  ,DIR_VERS_AGG           =
                :POL-DIR-VERS-AGG
                                       :IND-POL-DIR-VERS-AGG
                  ,COD_DVS                =
                :POL-COD-DVS
                                       :IND-POL-COD-DVS
                  ,FL_FNT_AZ              =
                :POL-FL-FNT-AZ
                                       :IND-POL-FL-FNT-AZ
                  ,FL_FNT_ADER            =
                :POL-FL-FNT-ADER
                                       :IND-POL-FL-FNT-ADER
                  ,FL_FNT_TFR             =
                :POL-FL-FNT-TFR
                                       :IND-POL-FL-FNT-TFR
                  ,FL_FNT_VOLO            =
                :POL-FL-FNT-VOLO
                                       :IND-POL-FL-FNT-VOLO
                  ,TP_OPZ_A_SCAD          =
                :POL-TP-OPZ-A-SCAD
                                       :IND-POL-TP-OPZ-A-SCAD
                  ,AA_DIFF_PROR_DFLT      =
                :POL-AA-DIFF-PROR-DFLT
                                       :IND-POL-AA-DIFF-PROR-DFLT
                  ,FL_VER_PROD            =
                :POL-FL-VER-PROD
                                       :IND-POL-FL-VER-PROD
                  ,DUR_GG                 =
                :POL-DUR-GG
                                       :IND-POL-DUR-GG
                  ,DIR_QUIET              =
                :POL-DIR-QUIET
                                       :IND-POL-DIR-QUIET
                  ,TP_PTF_ESTNO           =
                :POL-TP-PTF-ESTNO
                                       :IND-POL-TP-PTF-ESTNO
                  ,FL_CUM_PRE_CNTR        =
                :POL-FL-CUM-PRE-CNTR
                                       :IND-POL-FL-CUM-PRE-CNTR
                  ,FL_AMMB_MOVI           =
                :POL-FL-AMMB-MOVI
                                       :IND-POL-FL-AMMB-MOVI
                  ,CONV_GECO              =
                :POL-CONV-GECO
                                       :IND-POL-CONV-GECO
                  ,DS_RIGA                =
                :POL-DS-RIGA
                  ,DS_OPER_SQL            =
                :POL-DS-OPER-SQL
                  ,DS_VER                 =
                :POL-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :POL-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :POL-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :POL-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :POL-DS-STATO-ELAB
                  ,FL_SCUDO_FISC          =
                :POL-FL-SCUDO-FISC
                                       :IND-POL-FL-SCUDO-FISC
                  ,FL_TRASFE              =
                :POL-FL-TRASFE
                                       :IND-POL-FL-TRASFE
                  ,FL_TFR_STRC            =
                :POL-FL-TFR-STRC
                                       :IND-POL-FL-TFR-STRC
                  ,DT_PRESC               =
           :POL-DT-PRESC-DB
                                       :IND-POL-DT-PRESC
                  ,COD_CONV_AGG           =
                :POL-COD-CONV-AGG
                                       :IND-POL-COD-CONV-AGG
                  ,SUBCAT_PROD            =
                :POL-SUBCAT-PROD
                                       :IND-POL-SUBCAT-PROD
                  ,FL_QUEST_ADEGZ_ASS     =
                :POL-FL-QUEST-ADEGZ-ASS
                                       :IND-POL-FL-QUEST-ADEGZ-ASS
                  ,COD_TPA                =
                :POL-COD-TPA
                                       :IND-POL-COD-TPA
                  ,ID_ACC_COMM            =
                :POL-ID-ACC-COMM
                                       :IND-POL-ID-ACC-COMM
                  ,FL_POLI_CPI_PR         =
                :POL-FL-POLI-CPI-PR
                                       :IND-POL-FL-POLI-CPI-PR
                  ,FL_POLI_BUNDLING       =
                :POL-FL-POLI-BUNDLING
                                       :IND-POL-FL-POLI-BUNDLING
                  ,IND_POLI_PRIN_COLL     =
                :POL-IND-POLI-PRIN-COLL
                                       :IND-POL-IND-POLI-PRIN-COLL
                  ,FL_VND_BUNDLE          =
                :POL-FL-VND-BUNDLE
                                       :IND-POL-FL-VND-BUNDLE
                  ,IB_BS                  =
                :POL-IB-BS
                                       :IND-POL-IB-BS
                  ,FL_POLI_IFP            =
                :POL-FL-POLI-IFP
                                       :IND-POL-FL-POLI-IFP
                WHERE     DS_RIGA = :POL-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-POL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-POL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-POL
           INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IBO-EFF-POL CURSOR FOR
              SELECT
                     ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,IB_OGG
                    ,IB_PROP
                    ,DT_PROP
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_EMIS
                    ,TP_POLI
                    ,DUR_AA
                    ,DUR_MM
                    ,DT_SCAD
                    ,COD_PROD
                    ,DT_INI_VLDT_PROD
                    ,COD_CONV
                    ,COD_RAMO
                    ,DT_INI_VLDT_CONV
                    ,DT_APPLZ_CONV
                    ,TP_FRM_ASSVA
                    ,TP_RGM_FISC
                    ,FL_ESTAS
                    ,FL_RSH_COMUN
                    ,FL_RSH_COMUN_COND
                    ,TP_LIV_GENZ_TIT
                    ,FL_COP_FINANZ
                    ,TP_APPLZ_DIR
                    ,SPE_MED
                    ,DIR_EMIS
                    ,DIR_1O_VERS
                    ,DIR_VERS_AGG
                    ,COD_DVS
                    ,FL_FNT_AZ
                    ,FL_FNT_ADER
                    ,FL_FNT_TFR
                    ,FL_FNT_VOLO
                    ,TP_OPZ_A_SCAD
                    ,AA_DIFF_PROR_DFLT
                    ,FL_VER_PROD
                    ,DUR_GG
                    ,DIR_QUIET
                    ,TP_PTF_ESTNO
                    ,FL_CUM_PRE_CNTR
                    ,FL_AMMB_MOVI
                    ,CONV_GECO
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,FL_SCUDO_FISC
                    ,FL_TRASFE
                    ,FL_TFR_STRC
                    ,DT_PRESC
                    ,COD_CONV_AGG
                    ,SUBCAT_PROD
                    ,FL_QUEST_ADEGZ_ASS
                    ,COD_TPA
                    ,ID_ACC_COMM
                    ,FL_POLI_CPI_PR
                    ,FL_POLI_BUNDLING
                    ,IND_POLI_PRIN_COLL
                    ,FL_VND_BUNDLE
                    ,IB_BS
                    ,FL_POLI_IFP
              FROM POLI
              WHERE     IB_OGG = :POL-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_POLI ASC

           END-EXEC.

       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,IB_OGG
                ,IB_PROP
                ,DT_PROP
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_EMIS
                ,TP_POLI
                ,DUR_AA
                ,DUR_MM
                ,DT_SCAD
                ,COD_PROD
                ,DT_INI_VLDT_PROD
                ,COD_CONV
                ,COD_RAMO
                ,DT_INI_VLDT_CONV
                ,DT_APPLZ_CONV
                ,TP_FRM_ASSVA
                ,TP_RGM_FISC
                ,FL_ESTAS
                ,FL_RSH_COMUN
                ,FL_RSH_COMUN_COND
                ,TP_LIV_GENZ_TIT
                ,FL_COP_FINANZ
                ,TP_APPLZ_DIR
                ,SPE_MED
                ,DIR_EMIS
                ,DIR_1O_VERS
                ,DIR_VERS_AGG
                ,COD_DVS
                ,FL_FNT_AZ
                ,FL_FNT_ADER
                ,FL_FNT_TFR
                ,FL_FNT_VOLO
                ,TP_OPZ_A_SCAD
                ,AA_DIFF_PROR_DFLT
                ,FL_VER_PROD
                ,DUR_GG
                ,DIR_QUIET
                ,TP_PTF_ESTNO
                ,FL_CUM_PRE_CNTR
                ,FL_AMMB_MOVI
                ,CONV_GECO
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_SCUDO_FISC
                ,FL_TRASFE
                ,FL_TFR_STRC
                ,DT_PRESC
                ,COD_CONV_AGG
                ,SUBCAT_PROD
                ,FL_QUEST_ADEGZ_ASS
                ,COD_TPA
                ,ID_ACC_COMM
                ,FL_POLI_CPI_PR
                ,FL_POLI_BUNDLING
                ,IND_POLI_PRIN_COLL
                ,FL_VND_BUNDLE
                ,IB_BS
                ,FL_POLI_IFP
             INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
             FROM POLI
             WHERE     IB_OGG = :POL-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           EXEC SQL
                OPEN C-IBO-EFF-POL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           EXEC SQL
                CLOSE C-IBO-EFF-POL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           EXEC SQL
                FETCH C-IBO-EFF-POL
           INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A570-CLOSE-CURSOR-IBO     THRU A570-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DCL-CUR-IBS-PROP.
           EXEC SQL
                DECLARE C-IBS-EFF-POL-0 CURSOR FOR
              SELECT
                     ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,IB_OGG
                    ,IB_PROP
                    ,DT_PROP
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_EMIS
                    ,TP_POLI
                    ,DUR_AA
                    ,DUR_MM
                    ,DT_SCAD
                    ,COD_PROD
                    ,DT_INI_VLDT_PROD
                    ,COD_CONV
                    ,COD_RAMO
                    ,DT_INI_VLDT_CONV
                    ,DT_APPLZ_CONV
                    ,TP_FRM_ASSVA
                    ,TP_RGM_FISC
                    ,FL_ESTAS
                    ,FL_RSH_COMUN
                    ,FL_RSH_COMUN_COND
                    ,TP_LIV_GENZ_TIT
                    ,FL_COP_FINANZ
                    ,TP_APPLZ_DIR
                    ,SPE_MED
                    ,DIR_EMIS
                    ,DIR_1O_VERS
                    ,DIR_VERS_AGG
                    ,COD_DVS
                    ,FL_FNT_AZ
                    ,FL_FNT_ADER
                    ,FL_FNT_TFR
                    ,FL_FNT_VOLO
                    ,TP_OPZ_A_SCAD
                    ,AA_DIFF_PROR_DFLT
                    ,FL_VER_PROD
                    ,DUR_GG
                    ,DIR_QUIET
                    ,TP_PTF_ESTNO
                    ,FL_CUM_PRE_CNTR
                    ,FL_AMMB_MOVI
                    ,CONV_GECO
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,FL_SCUDO_FISC
                    ,FL_TRASFE
                    ,FL_TFR_STRC
                    ,DT_PRESC
                    ,COD_CONV_AGG
                    ,SUBCAT_PROD
                    ,FL_QUEST_ADEGZ_ASS
                    ,COD_TPA
                    ,ID_ACC_COMM
                    ,FL_POLI_CPI_PR
                    ,FL_POLI_BUNDLING
                    ,IND_POLI_PRIN_COLL
                    ,FL_VND_BUNDLE
                    ,IB_BS
                    ,FL_POLI_IFP
              FROM POLI
              WHERE     IB_PROP = :POL-IB-PROP
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_POLI ASC

           END-EXEC.
       A605-PROP-EX.
           EXIT.

       A605-DCL-CUR-IBS-BS.
           EXEC SQL
                DECLARE C-IBS-EFF-POL-1 CURSOR FOR
              SELECT
                     ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,IB_OGG
                    ,IB_PROP
                    ,DT_PROP
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_EMIS
                    ,TP_POLI
                    ,DUR_AA
                    ,DUR_MM
                    ,DT_SCAD
                    ,COD_PROD
                    ,DT_INI_VLDT_PROD
                    ,COD_CONV
                    ,COD_RAMO
                    ,DT_INI_VLDT_CONV
                    ,DT_APPLZ_CONV
                    ,TP_FRM_ASSVA
                    ,TP_RGM_FISC
                    ,FL_ESTAS
                    ,FL_RSH_COMUN
                    ,FL_RSH_COMUN_COND
                    ,TP_LIV_GENZ_TIT
                    ,FL_COP_FINANZ
                    ,TP_APPLZ_DIR
                    ,SPE_MED
                    ,DIR_EMIS
                    ,DIR_1O_VERS
                    ,DIR_VERS_AGG
                    ,COD_DVS
                    ,FL_FNT_AZ
                    ,FL_FNT_ADER
                    ,FL_FNT_TFR
                    ,FL_FNT_VOLO
                    ,TP_OPZ_A_SCAD
                    ,AA_DIFF_PROR_DFLT
                    ,FL_VER_PROD
                    ,DUR_GG
                    ,DIR_QUIET
                    ,TP_PTF_ESTNO
                    ,FL_CUM_PRE_CNTR
                    ,FL_AMMB_MOVI
                    ,CONV_GECO
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,FL_SCUDO_FISC
                    ,FL_TRASFE
                    ,FL_TFR_STRC
                    ,DT_PRESC
                    ,COD_CONV_AGG
                    ,SUBCAT_PROD
                    ,FL_QUEST_ADEGZ_ASS
                    ,COD_TPA
                    ,ID_ACC_COMM
                    ,FL_POLI_CPI_PR
                    ,FL_POLI_BUNDLING
                    ,IND_POLI_PRIN_COLL
                    ,FL_VND_BUNDLE
                    ,IB_BS
                    ,FL_POLI_IFP
              FROM POLI
              WHERE     IB_BS = :POL-IB-BS
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_POLI ASC

           END-EXEC.
       A605-BS-EX.
           EXIT.


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF POL-IB-PROP NOT = HIGH-VALUES
               PERFORM A605-DCL-CUR-IBS-PROP
                  THRU A605-PROP-EX
           ELSE
           IF POL-IB-BS NOT = HIGH-VALUES
               PERFORM A605-DCL-CUR-IBS-BS
                  THRU A605-BS-EX
           END-IF
           END-IF.

       A605-EX.
           EXIT.


       A610-SELECT-IBS-PROP.
           EXEC SQL
             SELECT
                ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,IB_OGG
                ,IB_PROP
                ,DT_PROP
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_EMIS
                ,TP_POLI
                ,DUR_AA
                ,DUR_MM
                ,DT_SCAD
                ,COD_PROD
                ,DT_INI_VLDT_PROD
                ,COD_CONV
                ,COD_RAMO
                ,DT_INI_VLDT_CONV
                ,DT_APPLZ_CONV
                ,TP_FRM_ASSVA
                ,TP_RGM_FISC
                ,FL_ESTAS
                ,FL_RSH_COMUN
                ,FL_RSH_COMUN_COND
                ,TP_LIV_GENZ_TIT
                ,FL_COP_FINANZ
                ,TP_APPLZ_DIR
                ,SPE_MED
                ,DIR_EMIS
                ,DIR_1O_VERS
                ,DIR_VERS_AGG
                ,COD_DVS
                ,FL_FNT_AZ
                ,FL_FNT_ADER
                ,FL_FNT_TFR
                ,FL_FNT_VOLO
                ,TP_OPZ_A_SCAD
                ,AA_DIFF_PROR_DFLT
                ,FL_VER_PROD
                ,DUR_GG
                ,DIR_QUIET
                ,TP_PTF_ESTNO
                ,FL_CUM_PRE_CNTR
                ,FL_AMMB_MOVI
                ,CONV_GECO
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_SCUDO_FISC
                ,FL_TRASFE
                ,FL_TFR_STRC
                ,DT_PRESC
                ,COD_CONV_AGG
                ,SUBCAT_PROD
                ,FL_QUEST_ADEGZ_ASS
                ,COD_TPA
                ,ID_ACC_COMM
                ,FL_POLI_CPI_PR
                ,FL_POLI_BUNDLING
                ,IND_POLI_PRIN_COLL
                ,FL_VND_BUNDLE
                ,IB_BS
                ,FL_POLI_IFP
             INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
             FROM POLI
             WHERE     IB_PROP = :POL-IB-PROP
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.
       A610-PROP-EX.
           EXIT.

       A610-SELECT-IBS-BS.
           EXEC SQL
             SELECT
                ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,IB_OGG
                ,IB_PROP
                ,DT_PROP
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_EMIS
                ,TP_POLI
                ,DUR_AA
                ,DUR_MM
                ,DT_SCAD
                ,COD_PROD
                ,DT_INI_VLDT_PROD
                ,COD_CONV
                ,COD_RAMO
                ,DT_INI_VLDT_CONV
                ,DT_APPLZ_CONV
                ,TP_FRM_ASSVA
                ,TP_RGM_FISC
                ,FL_ESTAS
                ,FL_RSH_COMUN
                ,FL_RSH_COMUN_COND
                ,TP_LIV_GENZ_TIT
                ,FL_COP_FINANZ
                ,TP_APPLZ_DIR
                ,SPE_MED
                ,DIR_EMIS
                ,DIR_1O_VERS
                ,DIR_VERS_AGG
                ,COD_DVS
                ,FL_FNT_AZ
                ,FL_FNT_ADER
                ,FL_FNT_TFR
                ,FL_FNT_VOLO
                ,TP_OPZ_A_SCAD
                ,AA_DIFF_PROR_DFLT
                ,FL_VER_PROD
                ,DUR_GG
                ,DIR_QUIET
                ,TP_PTF_ESTNO
                ,FL_CUM_PRE_CNTR
                ,FL_AMMB_MOVI
                ,CONV_GECO
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_SCUDO_FISC
                ,FL_TRASFE
                ,FL_TFR_STRC
                ,DT_PRESC
                ,COD_CONV_AGG
                ,SUBCAT_PROD
                ,FL_QUEST_ADEGZ_ASS
                ,COD_TPA
                ,ID_ACC_COMM
                ,FL_POLI_CPI_PR
                ,FL_POLI_BUNDLING
                ,IND_POLI_PRIN_COLL
                ,FL_VND_BUNDLE
                ,IB_BS
                ,FL_POLI_IFP
             INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
             FROM POLI
             WHERE     IB_BS = :POL-IB-BS
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.
       A610-BS-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF POL-IB-PROP NOT = HIGH-VALUES
               PERFORM A610-SELECT-IBS-PROP
                  THRU A610-PROP-EX
           ELSE
           IF POL-IB-BS NOT = HIGH-VALUES
               PERFORM A610-SELECT-IBS-BS
                  THRU A610-BS-EX
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           IF POL-IB-PROP NOT = HIGH-VALUES
              EXEC SQL
                   OPEN C-IBS-EFF-POL-0
              END-EXEC
           ELSE
           IF POL-IB-BS NOT = HIGH-VALUES
              EXEC SQL
                   OPEN C-IBS-EFF-POL-1
              END-EXEC
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           IF POL-IB-PROP NOT = HIGH-VALUES
              EXEC SQL
                   CLOSE C-IBS-EFF-POL-0
              END-EXEC
           ELSE
           IF POL-IB-BS NOT = HIGH-VALUES
              EXEC SQL
                   CLOSE C-IBS-EFF-POL-1
              END-EXEC
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FN-IBS-PROP.
           EXEC SQL
                FETCH C-IBS-EFF-POL-0
           INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
           END-EXEC.
       A690-PROP-EX.
           EXIT.

       A690-FN-IBS-BS.
           EXEC SQL
                FETCH C-IBS-EFF-POL-1
           INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
           END-EXEC.
       A690-BS-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           IF POL-IB-PROP NOT = HIGH-VALUES
               PERFORM A690-FN-IBS-PROP
                  THRU A690-PROP-EX
           ELSE
           IF POL-IB-BS NOT = HIGH-VALUES
               PERFORM A690-FN-IBS-BS
                  THRU A690-BS-EX
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A670-CLOSE-CURSOR-IBS     THRU A670-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,IB_OGG
                ,IB_PROP
                ,DT_PROP
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_EMIS
                ,TP_POLI
                ,DUR_AA
                ,DUR_MM
                ,DT_SCAD
                ,COD_PROD
                ,DT_INI_VLDT_PROD
                ,COD_CONV
                ,COD_RAMO
                ,DT_INI_VLDT_CONV
                ,DT_APPLZ_CONV
                ,TP_FRM_ASSVA
                ,TP_RGM_FISC
                ,FL_ESTAS
                ,FL_RSH_COMUN
                ,FL_RSH_COMUN_COND
                ,TP_LIV_GENZ_TIT
                ,FL_COP_FINANZ
                ,TP_APPLZ_DIR
                ,SPE_MED
                ,DIR_EMIS
                ,DIR_1O_VERS
                ,DIR_VERS_AGG
                ,COD_DVS
                ,FL_FNT_AZ
                ,FL_FNT_ADER
                ,FL_FNT_TFR
                ,FL_FNT_VOLO
                ,TP_OPZ_A_SCAD
                ,AA_DIFF_PROR_DFLT
                ,FL_VER_PROD
                ,DUR_GG
                ,DIR_QUIET
                ,TP_PTF_ESTNO
                ,FL_CUM_PRE_CNTR
                ,FL_AMMB_MOVI
                ,CONV_GECO
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_SCUDO_FISC
                ,FL_TRASFE
                ,FL_TFR_STRC
                ,DT_PRESC
                ,COD_CONV_AGG
                ,SUBCAT_PROD
                ,FL_QUEST_ADEGZ_ASS
                ,COD_TPA
                ,ID_ACC_COMM
                ,FL_POLI_CPI_PR
                ,FL_POLI_BUNDLING
                ,IND_POLI_PRIN_COLL
                ,FL_VND_BUNDLE
                ,IB_BS
                ,FL_POLI_IFP
             INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
             FROM POLI
             WHERE     ID_POLI = :POL-ID-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IBO-CPZ-POL CURSOR FOR
              SELECT
                     ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,IB_OGG
                    ,IB_PROP
                    ,DT_PROP
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_EMIS
                    ,TP_POLI
                    ,DUR_AA
                    ,DUR_MM
                    ,DT_SCAD
                    ,COD_PROD
                    ,DT_INI_VLDT_PROD
                    ,COD_CONV
                    ,COD_RAMO
                    ,DT_INI_VLDT_CONV
                    ,DT_APPLZ_CONV
                    ,TP_FRM_ASSVA
                    ,TP_RGM_FISC
                    ,FL_ESTAS
                    ,FL_RSH_COMUN
                    ,FL_RSH_COMUN_COND
                    ,TP_LIV_GENZ_TIT
                    ,FL_COP_FINANZ
                    ,TP_APPLZ_DIR
                    ,SPE_MED
                    ,DIR_EMIS
                    ,DIR_1O_VERS
                    ,DIR_VERS_AGG
                    ,COD_DVS
                    ,FL_FNT_AZ
                    ,FL_FNT_ADER
                    ,FL_FNT_TFR
                    ,FL_FNT_VOLO
                    ,TP_OPZ_A_SCAD
                    ,AA_DIFF_PROR_DFLT
                    ,FL_VER_PROD
                    ,DUR_GG
                    ,DIR_QUIET
                    ,TP_PTF_ESTNO
                    ,FL_CUM_PRE_CNTR
                    ,FL_AMMB_MOVI
                    ,CONV_GECO
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,FL_SCUDO_FISC
                    ,FL_TRASFE
                    ,FL_TFR_STRC
                    ,DT_PRESC
                    ,COD_CONV_AGG
                    ,SUBCAT_PROD
                    ,FL_QUEST_ADEGZ_ASS
                    ,COD_TPA
                    ,ID_ACC_COMM
                    ,FL_POLI_CPI_PR
                    ,FL_POLI_BUNDLING
                    ,IND_POLI_PRIN_COLL
                    ,FL_VND_BUNDLE
                    ,IB_BS
                    ,FL_POLI_IFP
              FROM POLI
              WHERE     IB_OGG = :POL-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_POLI ASC

           END-EXEC.

       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,IB_OGG
                ,IB_PROP
                ,DT_PROP
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_EMIS
                ,TP_POLI
                ,DUR_AA
                ,DUR_MM
                ,DT_SCAD
                ,COD_PROD
                ,DT_INI_VLDT_PROD
                ,COD_CONV
                ,COD_RAMO
                ,DT_INI_VLDT_CONV
                ,DT_APPLZ_CONV
                ,TP_FRM_ASSVA
                ,TP_RGM_FISC
                ,FL_ESTAS
                ,FL_RSH_COMUN
                ,FL_RSH_COMUN_COND
                ,TP_LIV_GENZ_TIT
                ,FL_COP_FINANZ
                ,TP_APPLZ_DIR
                ,SPE_MED
                ,DIR_EMIS
                ,DIR_1O_VERS
                ,DIR_VERS_AGG
                ,COD_DVS
                ,FL_FNT_AZ
                ,FL_FNT_ADER
                ,FL_FNT_TFR
                ,FL_FNT_VOLO
                ,TP_OPZ_A_SCAD
                ,AA_DIFF_PROR_DFLT
                ,FL_VER_PROD
                ,DUR_GG
                ,DIR_QUIET
                ,TP_PTF_ESTNO
                ,FL_CUM_PRE_CNTR
                ,FL_AMMB_MOVI
                ,CONV_GECO
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_SCUDO_FISC
                ,FL_TRASFE
                ,FL_TFR_STRC
                ,DT_PRESC
                ,COD_CONV_AGG
                ,SUBCAT_PROD
                ,FL_QUEST_ADEGZ_ASS
                ,COD_TPA
                ,ID_ACC_COMM
                ,FL_POLI_CPI_PR
                ,FL_POLI_BUNDLING
                ,IND_POLI_PRIN_COLL
                ,FL_VND_BUNDLE
                ,IB_BS
                ,FL_POLI_IFP
             INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
             FROM POLI
             WHERE     IB_OGG = :POL-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           EXEC SQL
                OPEN C-IBO-CPZ-POL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           EXEC SQL
                CLOSE C-IBO-CPZ-POL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           EXEC SQL
                FETCH C-IBO-CPZ-POL
           INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B570-CLOSE-CURSOR-IBO-CPZ     THRU B570-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DCL-CUR-IBS-PROP.
           EXEC SQL
                DECLARE C-IBS-CPZ-POL-0 CURSOR FOR
              SELECT
                     ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,IB_OGG
                    ,IB_PROP
                    ,DT_PROP
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_EMIS
                    ,TP_POLI
                    ,DUR_AA
                    ,DUR_MM
                    ,DT_SCAD
                    ,COD_PROD
                    ,DT_INI_VLDT_PROD
                    ,COD_CONV
                    ,COD_RAMO
                    ,DT_INI_VLDT_CONV
                    ,DT_APPLZ_CONV
                    ,TP_FRM_ASSVA
                    ,TP_RGM_FISC
                    ,FL_ESTAS
                    ,FL_RSH_COMUN
                    ,FL_RSH_COMUN_COND
                    ,TP_LIV_GENZ_TIT
                    ,FL_COP_FINANZ
                    ,TP_APPLZ_DIR
                    ,SPE_MED
                    ,DIR_EMIS
                    ,DIR_1O_VERS
                    ,DIR_VERS_AGG
                    ,COD_DVS
                    ,FL_FNT_AZ
                    ,FL_FNT_ADER
                    ,FL_FNT_TFR
                    ,FL_FNT_VOLO
                    ,TP_OPZ_A_SCAD
                    ,AA_DIFF_PROR_DFLT
                    ,FL_VER_PROD
                    ,DUR_GG
                    ,DIR_QUIET
                    ,TP_PTF_ESTNO
                    ,FL_CUM_PRE_CNTR
                    ,FL_AMMB_MOVI
                    ,CONV_GECO
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,FL_SCUDO_FISC
                    ,FL_TRASFE
                    ,FL_TFR_STRC
                    ,DT_PRESC
                    ,COD_CONV_AGG
                    ,SUBCAT_PROD
                    ,FL_QUEST_ADEGZ_ASS
                    ,COD_TPA
                    ,ID_ACC_COMM
                    ,FL_POLI_CPI_PR
                    ,FL_POLI_BUNDLING
                    ,IND_POLI_PRIN_COLL
                    ,FL_VND_BUNDLE
                    ,IB_BS
                    ,FL_POLI_IFP
              FROM POLI
              WHERE     IB_PROP = :POL-IB-PROP
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_POLI ASC

           END-EXEC.
       B605-PROP-EX.
           EXIT.

       B605-DCL-CUR-IBS-BS.
           EXEC SQL
                DECLARE C-IBS-CPZ-POL-1 CURSOR FOR
              SELECT
                     ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,IB_OGG
                    ,IB_PROP
                    ,DT_PROP
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_EMIS
                    ,TP_POLI
                    ,DUR_AA
                    ,DUR_MM
                    ,DT_SCAD
                    ,COD_PROD
                    ,DT_INI_VLDT_PROD
                    ,COD_CONV
                    ,COD_RAMO
                    ,DT_INI_VLDT_CONV
                    ,DT_APPLZ_CONV
                    ,TP_FRM_ASSVA
                    ,TP_RGM_FISC
                    ,FL_ESTAS
                    ,FL_RSH_COMUN
                    ,FL_RSH_COMUN_COND
                    ,TP_LIV_GENZ_TIT
                    ,FL_COP_FINANZ
                    ,TP_APPLZ_DIR
                    ,SPE_MED
                    ,DIR_EMIS
                    ,DIR_1O_VERS
                    ,DIR_VERS_AGG
                    ,COD_DVS
                    ,FL_FNT_AZ
                    ,FL_FNT_ADER
                    ,FL_FNT_TFR
                    ,FL_FNT_VOLO
                    ,TP_OPZ_A_SCAD
                    ,AA_DIFF_PROR_DFLT
                    ,FL_VER_PROD
                    ,DUR_GG
                    ,DIR_QUIET
                    ,TP_PTF_ESTNO
                    ,FL_CUM_PRE_CNTR
                    ,FL_AMMB_MOVI
                    ,CONV_GECO
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,FL_SCUDO_FISC
                    ,FL_TRASFE
                    ,FL_TFR_STRC
                    ,DT_PRESC
                    ,COD_CONV_AGG
                    ,SUBCAT_PROD
                    ,FL_QUEST_ADEGZ_ASS
                    ,COD_TPA
                    ,ID_ACC_COMM
                    ,FL_POLI_CPI_PR
                    ,FL_POLI_BUNDLING
                    ,IND_POLI_PRIN_COLL
                    ,FL_VND_BUNDLE
                    ,IB_BS
                    ,FL_POLI_IFP
              FROM POLI
              WHERE     IB_BS = :POL-IB-BS
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_POLI ASC

           END-EXEC.
       B605-BS-EX.
           EXIT.


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF POL-IB-PROP NOT = HIGH-VALUES
               PERFORM B605-DCL-CUR-IBS-PROP
                  THRU B605-PROP-EX
           ELSE
           IF POL-IB-BS NOT = HIGH-VALUES
               PERFORM B605-DCL-CUR-IBS-BS
                  THRU B605-BS-EX
           END-IF
           END-IF.

       B605-EX.
           EXIT.


       B610-SELECT-IBS-PROP.
           EXEC SQL
             SELECT
                ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,IB_OGG
                ,IB_PROP
                ,DT_PROP
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_EMIS
                ,TP_POLI
                ,DUR_AA
                ,DUR_MM
                ,DT_SCAD
                ,COD_PROD
                ,DT_INI_VLDT_PROD
                ,COD_CONV
                ,COD_RAMO
                ,DT_INI_VLDT_CONV
                ,DT_APPLZ_CONV
                ,TP_FRM_ASSVA
                ,TP_RGM_FISC
                ,FL_ESTAS
                ,FL_RSH_COMUN
                ,FL_RSH_COMUN_COND
                ,TP_LIV_GENZ_TIT
                ,FL_COP_FINANZ
                ,TP_APPLZ_DIR
                ,SPE_MED
                ,DIR_EMIS
                ,DIR_1O_VERS
                ,DIR_VERS_AGG
                ,COD_DVS
                ,FL_FNT_AZ
                ,FL_FNT_ADER
                ,FL_FNT_TFR
                ,FL_FNT_VOLO
                ,TP_OPZ_A_SCAD
                ,AA_DIFF_PROR_DFLT
                ,FL_VER_PROD
                ,DUR_GG
                ,DIR_QUIET
                ,TP_PTF_ESTNO
                ,FL_CUM_PRE_CNTR
                ,FL_AMMB_MOVI
                ,CONV_GECO
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_SCUDO_FISC
                ,FL_TRASFE
                ,FL_TFR_STRC
                ,DT_PRESC
                ,COD_CONV_AGG
                ,SUBCAT_PROD
                ,FL_QUEST_ADEGZ_ASS
                ,COD_TPA
                ,ID_ACC_COMM
                ,FL_POLI_CPI_PR
                ,FL_POLI_BUNDLING
                ,IND_POLI_PRIN_COLL
                ,FL_VND_BUNDLE
                ,IB_BS
                ,FL_POLI_IFP
             INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
             FROM POLI
             WHERE     IB_PROP = :POL-IB-PROP
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.
       B610-PROP-EX.
           EXIT.

       B610-SELECT-IBS-BS.
           EXEC SQL
             SELECT
                ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,IB_OGG
                ,IB_PROP
                ,DT_PROP
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_EMIS
                ,TP_POLI
                ,DUR_AA
                ,DUR_MM
                ,DT_SCAD
                ,COD_PROD
                ,DT_INI_VLDT_PROD
                ,COD_CONV
                ,COD_RAMO
                ,DT_INI_VLDT_CONV
                ,DT_APPLZ_CONV
                ,TP_FRM_ASSVA
                ,TP_RGM_FISC
                ,FL_ESTAS
                ,FL_RSH_COMUN
                ,FL_RSH_COMUN_COND
                ,TP_LIV_GENZ_TIT
                ,FL_COP_FINANZ
                ,TP_APPLZ_DIR
                ,SPE_MED
                ,DIR_EMIS
                ,DIR_1O_VERS
                ,DIR_VERS_AGG
                ,COD_DVS
                ,FL_FNT_AZ
                ,FL_FNT_ADER
                ,FL_FNT_TFR
                ,FL_FNT_VOLO
                ,TP_OPZ_A_SCAD
                ,AA_DIFF_PROR_DFLT
                ,FL_VER_PROD
                ,DUR_GG
                ,DIR_QUIET
                ,TP_PTF_ESTNO
                ,FL_CUM_PRE_CNTR
                ,FL_AMMB_MOVI
                ,CONV_GECO
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_SCUDO_FISC
                ,FL_TRASFE
                ,FL_TFR_STRC
                ,DT_PRESC
                ,COD_CONV_AGG
                ,SUBCAT_PROD
                ,FL_QUEST_ADEGZ_ASS
                ,COD_TPA
                ,ID_ACC_COMM
                ,FL_POLI_CPI_PR
                ,FL_POLI_BUNDLING
                ,IND_POLI_PRIN_COLL
                ,FL_VND_BUNDLE
                ,IB_BS
                ,FL_POLI_IFP
             INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
             FROM POLI
             WHERE     IB_BS = :POL-IB-BS
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.
       B610-BS-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF POL-IB-PROP NOT = HIGH-VALUES
               PERFORM B610-SELECT-IBS-PROP
                  THRU B610-PROP-EX
           ELSE
           IF POL-IB-BS NOT = HIGH-VALUES
               PERFORM B610-SELECT-IBS-BS
                  THRU B610-BS-EX
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           IF POL-IB-PROP NOT = HIGH-VALUES
              EXEC SQL
                   OPEN C-IBS-CPZ-POL-0
              END-EXEC
           ELSE
           IF POL-IB-BS NOT = HIGH-VALUES
              EXEC SQL
                   OPEN C-IBS-CPZ-POL-1
              END-EXEC
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           IF POL-IB-PROP NOT = HIGH-VALUES
              EXEC SQL
                   CLOSE C-IBS-CPZ-POL-0
              END-EXEC
           ELSE
           IF POL-IB-BS NOT = HIGH-VALUES
              EXEC SQL
                   CLOSE C-IBS-CPZ-POL-1
              END-EXEC
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FN-IBS-PROP.
           EXEC SQL
                FETCH C-IBS-CPZ-POL-0
           INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
           END-EXEC.
       B690-PROP-EX.
           EXIT.

       B690-FN-IBS-BS.
           EXEC SQL
                FETCH C-IBS-CPZ-POL-1
           INTO
                :POL-ID-POLI
               ,:POL-ID-MOVI-CRZ
               ,:POL-ID-MOVI-CHIU
                :IND-POL-ID-MOVI-CHIU
               ,:POL-IB-OGG
                :IND-POL-IB-OGG
               ,:POL-IB-PROP
               ,:POL-DT-PROP-DB
                :IND-POL-DT-PROP
               ,:POL-DT-INI-EFF-DB
               ,:POL-DT-END-EFF-DB
               ,:POL-COD-COMP-ANIA
               ,:POL-DT-DECOR-DB
               ,:POL-DT-EMIS-DB
               ,:POL-TP-POLI
               ,:POL-DUR-AA
                :IND-POL-DUR-AA
               ,:POL-DUR-MM
                :IND-POL-DUR-MM
               ,:POL-DT-SCAD-DB
                :IND-POL-DT-SCAD
               ,:POL-COD-PROD
               ,:POL-DT-INI-VLDT-PROD-DB
               ,:POL-COD-CONV
                :IND-POL-COD-CONV
               ,:POL-COD-RAMO
                :IND-POL-COD-RAMO
               ,:POL-DT-INI-VLDT-CONV-DB
                :IND-POL-DT-INI-VLDT-CONV
               ,:POL-DT-APPLZ-CONV-DB
                :IND-POL-DT-APPLZ-CONV
               ,:POL-TP-FRM-ASSVA
               ,:POL-TP-RGM-FISC
                :IND-POL-TP-RGM-FISC
               ,:POL-FL-ESTAS
                :IND-POL-FL-ESTAS
               ,:POL-FL-RSH-COMUN
                :IND-POL-FL-RSH-COMUN
               ,:POL-FL-RSH-COMUN-COND
                :IND-POL-FL-RSH-COMUN-COND
               ,:POL-TP-LIV-GENZ-TIT
               ,:POL-FL-COP-FINANZ
                :IND-POL-FL-COP-FINANZ
               ,:POL-TP-APPLZ-DIR
                :IND-POL-TP-APPLZ-DIR
               ,:POL-SPE-MED
                :IND-POL-SPE-MED
               ,:POL-DIR-EMIS
                :IND-POL-DIR-EMIS
               ,:POL-DIR-1O-VERS
                :IND-POL-DIR-1O-VERS
               ,:POL-DIR-VERS-AGG
                :IND-POL-DIR-VERS-AGG
               ,:POL-COD-DVS
                :IND-POL-COD-DVS
               ,:POL-FL-FNT-AZ
                :IND-POL-FL-FNT-AZ
               ,:POL-FL-FNT-ADER
                :IND-POL-FL-FNT-ADER
               ,:POL-FL-FNT-TFR
                :IND-POL-FL-FNT-TFR
               ,:POL-FL-FNT-VOLO
                :IND-POL-FL-FNT-VOLO
               ,:POL-TP-OPZ-A-SCAD
                :IND-POL-TP-OPZ-A-SCAD
               ,:POL-AA-DIFF-PROR-DFLT
                :IND-POL-AA-DIFF-PROR-DFLT
               ,:POL-FL-VER-PROD
                :IND-POL-FL-VER-PROD
               ,:POL-DUR-GG
                :IND-POL-DUR-GG
               ,:POL-DIR-QUIET
                :IND-POL-DIR-QUIET
               ,:POL-TP-PTF-ESTNO
                :IND-POL-TP-PTF-ESTNO
               ,:POL-FL-CUM-PRE-CNTR
                :IND-POL-FL-CUM-PRE-CNTR
               ,:POL-FL-AMMB-MOVI
                :IND-POL-FL-AMMB-MOVI
               ,:POL-CONV-GECO
                :IND-POL-CONV-GECO
               ,:POL-DS-RIGA
               ,:POL-DS-OPER-SQL
               ,:POL-DS-VER
               ,:POL-DS-TS-INI-CPTZ
               ,:POL-DS-TS-END-CPTZ
               ,:POL-DS-UTENTE
               ,:POL-DS-STATO-ELAB
               ,:POL-FL-SCUDO-FISC
                :IND-POL-FL-SCUDO-FISC
               ,:POL-FL-TRASFE
                :IND-POL-FL-TRASFE
               ,:POL-FL-TFR-STRC
                :IND-POL-FL-TFR-STRC
               ,:POL-DT-PRESC-DB
                :IND-POL-DT-PRESC
               ,:POL-COD-CONV-AGG
                :IND-POL-COD-CONV-AGG
               ,:POL-SUBCAT-PROD
                :IND-POL-SUBCAT-PROD
               ,:POL-FL-QUEST-ADEGZ-ASS
                :IND-POL-FL-QUEST-ADEGZ-ASS
               ,:POL-COD-TPA
                :IND-POL-COD-TPA
               ,:POL-ID-ACC-COMM
                :IND-POL-ID-ACC-COMM
               ,:POL-FL-POLI-CPI-PR
                :IND-POL-FL-POLI-CPI-PR
               ,:POL-FL-POLI-BUNDLING
                :IND-POL-FL-POLI-BUNDLING
               ,:POL-IND-POLI-PRIN-COLL
                :IND-POL-IND-POLI-PRIN-COLL
               ,:POL-FL-VND-BUNDLE
                :IND-POL-FL-VND-BUNDLE
               ,:POL-IB-BS
                :IND-POL-IB-BS
               ,:POL-FL-POLI-IFP
                :IND-POL-FL-POLI-IFP
           END-EXEC.
       B690-BS-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           IF POL-IB-PROP NOT = HIGH-VALUES
               PERFORM B690-FN-IBS-PROP
                  THRU B690-PROP-EX
           ELSE
           IF POL-IB-BS NOT = HIGH-VALUES
               PERFORM B690-FN-IBS-BS
                  THRU B690-BS-EX
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B670-CLOSE-CURSOR-IBS-CPZ     THRU B670-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-POL-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO POL-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-POL-IB-OGG = -1
              MOVE HIGH-VALUES TO POL-IB-OGG-NULL
           END-IF
           IF IND-POL-DT-PROP = -1
              MOVE HIGH-VALUES TO POL-DT-PROP-NULL
           END-IF
           IF IND-POL-DUR-AA = -1
              MOVE HIGH-VALUES TO POL-DUR-AA-NULL
           END-IF
           IF IND-POL-DUR-MM = -1
              MOVE HIGH-VALUES TO POL-DUR-MM-NULL
           END-IF
           IF IND-POL-DT-SCAD = -1
              MOVE HIGH-VALUES TO POL-DT-SCAD-NULL
           END-IF
           IF IND-POL-COD-CONV = -1
              MOVE HIGH-VALUES TO POL-COD-CONV-NULL
           END-IF
           IF IND-POL-COD-RAMO = -1
              MOVE HIGH-VALUES TO POL-COD-RAMO-NULL
           END-IF
           IF IND-POL-DT-INI-VLDT-CONV = -1
              MOVE HIGH-VALUES TO POL-DT-INI-VLDT-CONV-NULL
           END-IF
           IF IND-POL-DT-APPLZ-CONV = -1
              MOVE HIGH-VALUES TO POL-DT-APPLZ-CONV-NULL
           END-IF
           IF IND-POL-TP-RGM-FISC = -1
              MOVE HIGH-VALUES TO POL-TP-RGM-FISC-NULL
           END-IF
           IF IND-POL-FL-ESTAS = -1
              MOVE HIGH-VALUES TO POL-FL-ESTAS-NULL
           END-IF
           IF IND-POL-FL-RSH-COMUN = -1
              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-NULL
           END-IF
           IF IND-POL-FL-RSH-COMUN-COND = -1
              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-COND-NULL
           END-IF
           IF IND-POL-FL-COP-FINANZ = -1
              MOVE HIGH-VALUES TO POL-FL-COP-FINANZ-NULL
           END-IF
           IF IND-POL-TP-APPLZ-DIR = -1
              MOVE HIGH-VALUES TO POL-TP-APPLZ-DIR-NULL
           END-IF
           IF IND-POL-SPE-MED = -1
              MOVE HIGH-VALUES TO POL-SPE-MED-NULL
           END-IF
           IF IND-POL-DIR-EMIS = -1
              MOVE HIGH-VALUES TO POL-DIR-EMIS-NULL
           END-IF
           IF IND-POL-DIR-1O-VERS = -1
              MOVE HIGH-VALUES TO POL-DIR-1O-VERS-NULL
           END-IF
           IF IND-POL-DIR-VERS-AGG = -1
              MOVE HIGH-VALUES TO POL-DIR-VERS-AGG-NULL
           END-IF
           IF IND-POL-COD-DVS = -1
              MOVE HIGH-VALUES TO POL-COD-DVS-NULL
           END-IF
           IF IND-POL-FL-FNT-AZ = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-AZ-NULL
           END-IF
           IF IND-POL-FL-FNT-ADER = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-ADER-NULL
           END-IF
           IF IND-POL-FL-FNT-TFR = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-TFR-NULL
           END-IF
           IF IND-POL-FL-FNT-VOLO = -1
              MOVE HIGH-VALUES TO POL-FL-FNT-VOLO-NULL
           END-IF
           IF IND-POL-TP-OPZ-A-SCAD = -1
              MOVE HIGH-VALUES TO POL-TP-OPZ-A-SCAD-NULL
           END-IF
           IF IND-POL-AA-DIFF-PROR-DFLT = -1
              MOVE HIGH-VALUES TO POL-AA-DIFF-PROR-DFLT-NULL
           END-IF
           IF IND-POL-FL-VER-PROD = -1
              MOVE HIGH-VALUES TO POL-FL-VER-PROD-NULL
           END-IF
           IF IND-POL-DUR-GG = -1
              MOVE HIGH-VALUES TO POL-DUR-GG-NULL
           END-IF
           IF IND-POL-DIR-QUIET = -1
              MOVE HIGH-VALUES TO POL-DIR-QUIET-NULL
           END-IF
           IF IND-POL-TP-PTF-ESTNO = -1
              MOVE HIGH-VALUES TO POL-TP-PTF-ESTNO-NULL
           END-IF
           IF IND-POL-FL-CUM-PRE-CNTR = -1
              MOVE HIGH-VALUES TO POL-FL-CUM-PRE-CNTR-NULL
           END-IF
           IF IND-POL-FL-AMMB-MOVI = -1
              MOVE HIGH-VALUES TO POL-FL-AMMB-MOVI-NULL
           END-IF
           IF IND-POL-CONV-GECO = -1
              MOVE HIGH-VALUES TO POL-CONV-GECO-NULL
           END-IF
           IF IND-POL-FL-SCUDO-FISC = -1
              MOVE HIGH-VALUES TO POL-FL-SCUDO-FISC-NULL
           END-IF
           IF IND-POL-FL-TRASFE = -1
              MOVE HIGH-VALUES TO POL-FL-TRASFE-NULL
           END-IF
           IF IND-POL-FL-TFR-STRC = -1
              MOVE HIGH-VALUES TO POL-FL-TFR-STRC-NULL
           END-IF
           IF IND-POL-DT-PRESC = -1
              MOVE HIGH-VALUES TO POL-DT-PRESC-NULL
           END-IF
           IF IND-POL-COD-CONV-AGG = -1
              MOVE HIGH-VALUES TO POL-COD-CONV-AGG-NULL
           END-IF
           IF IND-POL-SUBCAT-PROD = -1
              MOVE HIGH-VALUES TO POL-SUBCAT-PROD-NULL
           END-IF
           IF IND-POL-FL-QUEST-ADEGZ-ASS = -1
              MOVE HIGH-VALUES TO POL-FL-QUEST-ADEGZ-ASS-NULL
           END-IF
           IF IND-POL-COD-TPA = -1
              MOVE HIGH-VALUES TO POL-COD-TPA-NULL
           END-IF
           IF IND-POL-ID-ACC-COMM = -1
              MOVE HIGH-VALUES TO POL-ID-ACC-COMM-NULL
           END-IF
           IF IND-POL-FL-POLI-CPI-PR = -1
              MOVE HIGH-VALUES TO POL-FL-POLI-CPI-PR-NULL
           END-IF
           IF IND-POL-FL-POLI-BUNDLING = -1
              MOVE HIGH-VALUES TO POL-FL-POLI-BUNDLING-NULL
           END-IF
           IF IND-POL-IND-POLI-PRIN-COLL = -1
              MOVE HIGH-VALUES TO POL-IND-POLI-PRIN-COLL-NULL
           END-IF
           IF IND-POL-FL-VND-BUNDLE = -1
              MOVE HIGH-VALUES TO POL-FL-VND-BUNDLE-NULL
           END-IF
           IF IND-POL-IB-BS = -1
              MOVE HIGH-VALUES TO POL-IB-BS-NULL
           END-IF
           IF IND-POL-FL-POLI-IFP = -1
              MOVE HIGH-VALUES TO POL-FL-POLI-IFP-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO POL-DS-OPER-SQL
           MOVE 0                   TO POL-DS-VER
NEWP       INITIALIZE WK-ID-OGG
NEWP       MOVE POL-ID-POLI
NEWP         TO WK-ID-OGG
NEWP       MOVE WK-ID-OGG(8:2)
NEWP         TO POL-DS-VER
NEWP       IF POL-DS-VER = 00
NEWP          MOVE 100
NEWP            TO POL-DS-VER
NEWP       END-IF
           MOVE IDSV0003-USER-NAME TO POL-DS-UTENTE
           MOVE '1'                   TO POL-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO POL-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO POL-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF POL-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-POL-ID-MOVI-CHIU
           END-IF
           IF POL-IB-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-IB-OGG
           ELSE
              MOVE 0 TO IND-POL-IB-OGG
           END-IF
           IF POL-DT-PROP-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DT-PROP
           ELSE
              MOVE 0 TO IND-POL-DT-PROP
           END-IF
           IF POL-DUR-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DUR-AA
           ELSE
              MOVE 0 TO IND-POL-DUR-AA
           END-IF
           IF POL-DUR-MM-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DUR-MM
           ELSE
              MOVE 0 TO IND-POL-DUR-MM
           END-IF
           IF POL-DT-SCAD-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DT-SCAD
           ELSE
              MOVE 0 TO IND-POL-DT-SCAD
           END-IF
           IF POL-COD-CONV-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-COD-CONV
           ELSE
              MOVE 0 TO IND-POL-COD-CONV
           END-IF
           IF POL-COD-RAMO-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-COD-RAMO
           ELSE
              MOVE 0 TO IND-POL-COD-RAMO
           END-IF
           IF POL-DT-INI-VLDT-CONV-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DT-INI-VLDT-CONV
           ELSE
              MOVE 0 TO IND-POL-DT-INI-VLDT-CONV
           END-IF
           IF POL-DT-APPLZ-CONV-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DT-APPLZ-CONV
           ELSE
              MOVE 0 TO IND-POL-DT-APPLZ-CONV
           END-IF
           IF POL-TP-RGM-FISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-TP-RGM-FISC
           ELSE
              MOVE 0 TO IND-POL-TP-RGM-FISC
           END-IF
           IF POL-FL-ESTAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-ESTAS
           ELSE
              MOVE 0 TO IND-POL-FL-ESTAS
           END-IF
           IF POL-FL-RSH-COMUN-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-RSH-COMUN
           ELSE
              MOVE 0 TO IND-POL-FL-RSH-COMUN
           END-IF
           IF POL-FL-RSH-COMUN-COND-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-RSH-COMUN-COND
           ELSE
              MOVE 0 TO IND-POL-FL-RSH-COMUN-COND
           END-IF
           IF POL-FL-COP-FINANZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-COP-FINANZ
           ELSE
              MOVE 0 TO IND-POL-FL-COP-FINANZ
           END-IF
           IF POL-TP-APPLZ-DIR-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-TP-APPLZ-DIR
           ELSE
              MOVE 0 TO IND-POL-TP-APPLZ-DIR
           END-IF
           IF POL-SPE-MED-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-SPE-MED
           ELSE
              MOVE 0 TO IND-POL-SPE-MED
           END-IF
           IF POL-DIR-EMIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DIR-EMIS
           ELSE
              MOVE 0 TO IND-POL-DIR-EMIS
           END-IF
           IF POL-DIR-1O-VERS-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DIR-1O-VERS
           ELSE
              MOVE 0 TO IND-POL-DIR-1O-VERS
           END-IF
           IF POL-DIR-VERS-AGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DIR-VERS-AGG
           ELSE
              MOVE 0 TO IND-POL-DIR-VERS-AGG
           END-IF
           IF POL-COD-DVS-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-COD-DVS
           ELSE
              MOVE 0 TO IND-POL-COD-DVS
           END-IF
           IF POL-FL-FNT-AZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-FNT-AZ
           ELSE
              MOVE 0 TO IND-POL-FL-FNT-AZ
           END-IF
           IF POL-FL-FNT-ADER-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-FNT-ADER
           ELSE
              MOVE 0 TO IND-POL-FL-FNT-ADER
           END-IF
           IF POL-FL-FNT-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-FNT-TFR
           ELSE
              MOVE 0 TO IND-POL-FL-FNT-TFR
           END-IF
           IF POL-FL-FNT-VOLO-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-FNT-VOLO
           ELSE
              MOVE 0 TO IND-POL-FL-FNT-VOLO
           END-IF
           IF POL-TP-OPZ-A-SCAD-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-TP-OPZ-A-SCAD
           ELSE
              MOVE 0 TO IND-POL-TP-OPZ-A-SCAD
           END-IF
           IF POL-AA-DIFF-PROR-DFLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-AA-DIFF-PROR-DFLT
           ELSE
              MOVE 0 TO IND-POL-AA-DIFF-PROR-DFLT
           END-IF
           IF POL-FL-VER-PROD-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-VER-PROD
           ELSE
              MOVE 0 TO IND-POL-FL-VER-PROD
           END-IF
           IF POL-DUR-GG-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DUR-GG
           ELSE
              MOVE 0 TO IND-POL-DUR-GG
           END-IF
           IF POL-DIR-QUIET-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DIR-QUIET
           ELSE
              MOVE 0 TO IND-POL-DIR-QUIET
           END-IF
           IF POL-TP-PTF-ESTNO-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-TP-PTF-ESTNO
           ELSE
              MOVE 0 TO IND-POL-TP-PTF-ESTNO
           END-IF
           IF POL-FL-CUM-PRE-CNTR-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-CUM-PRE-CNTR
           ELSE
              MOVE 0 TO IND-POL-FL-CUM-PRE-CNTR
           END-IF
           IF POL-FL-AMMB-MOVI-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-AMMB-MOVI
           ELSE
              MOVE 0 TO IND-POL-FL-AMMB-MOVI
           END-IF
           IF POL-CONV-GECO-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-CONV-GECO
           ELSE
              MOVE 0 TO IND-POL-CONV-GECO
           END-IF
           IF POL-FL-SCUDO-FISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-SCUDO-FISC
           ELSE
              MOVE 0 TO IND-POL-FL-SCUDO-FISC
           END-IF
           IF POL-FL-TRASFE-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-TRASFE
           ELSE
              MOVE 0 TO IND-POL-FL-TRASFE
           END-IF
           IF POL-FL-TFR-STRC-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-TFR-STRC
           ELSE
              MOVE 0 TO IND-POL-FL-TFR-STRC
           END-IF
           IF POL-DT-PRESC-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-DT-PRESC
           ELSE
              MOVE 0 TO IND-POL-DT-PRESC
           END-IF
           IF POL-COD-CONV-AGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-COD-CONV-AGG
           ELSE
              MOVE 0 TO IND-POL-COD-CONV-AGG
           END-IF
           IF POL-SUBCAT-PROD-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-SUBCAT-PROD
           ELSE
              MOVE 0 TO IND-POL-SUBCAT-PROD
           END-IF
           IF POL-FL-QUEST-ADEGZ-ASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-QUEST-ADEGZ-ASS
           ELSE
              MOVE 0 TO IND-POL-FL-QUEST-ADEGZ-ASS
           END-IF
           IF POL-COD-TPA-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-COD-TPA
           ELSE
              MOVE 0 TO IND-POL-COD-TPA
           END-IF
           IF POL-ID-ACC-COMM-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-ID-ACC-COMM
           ELSE
              MOVE 0 TO IND-POL-ID-ACC-COMM
           END-IF
           IF POL-FL-POLI-CPI-PR-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-POLI-CPI-PR
           ELSE
              MOVE 0 TO IND-POL-FL-POLI-CPI-PR
           END-IF
           IF POL-FL-POLI-BUNDLING-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-POLI-BUNDLING
           ELSE
              MOVE 0 TO IND-POL-FL-POLI-BUNDLING
           END-IF
           IF POL-IND-POLI-PRIN-COLL-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-IND-POLI-PRIN-COLL
           ELSE
              MOVE 0 TO IND-POL-IND-POLI-PRIN-COLL
           END-IF
           IF POL-FL-VND-BUNDLE-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-VND-BUNDLE
           ELSE
              MOVE 0 TO IND-POL-FL-VND-BUNDLE
           END-IF
           IF POL-IB-BS-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-IB-BS
           ELSE
              MOVE 0 TO IND-POL-IB-BS
           END-IF
           IF POL-FL-POLI-IFP-NULL = HIGH-VALUES
              MOVE -1 TO IND-POL-FL-POLI-IFP
           ELSE
              MOVE 0 TO IND-POL-FL-POLI-IFP
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : POL-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE POLI TO WS-BUFFER-TABLE.

           MOVE POL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO POL-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO POL-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO POL-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO POL-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO POL-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO POL-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO POL-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE POLI TO WS-BUFFER-TABLE.

           MOVE POL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO POLI.

           MOVE WS-ID-MOVI-CRZ  TO POL-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO POL-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO POL-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO POL-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO POL-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO POL-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO POL-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           IF IND-POL-DT-PROP = 0
               MOVE POL-DT-PROP TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-PROP-DB
           END-IF
           MOVE POL-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-INI-EFF-DB
           MOVE POL-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-END-EFF-DB
           MOVE POL-DT-DECOR TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-DECOR-DB
           MOVE POL-DT-EMIS TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-EMIS-DB
           IF IND-POL-DT-SCAD = 0
               MOVE POL-DT-SCAD TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-SCAD-DB
           END-IF
           MOVE POL-DT-INI-VLDT-PROD TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO POL-DT-INI-VLDT-PROD-DB
           IF IND-POL-DT-INI-VLDT-CONV = 0
               MOVE POL-DT-INI-VLDT-CONV TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-INI-VLDT-CONV-DB
           END-IF
           IF IND-POL-DT-APPLZ-CONV = 0
               MOVE POL-DT-APPLZ-CONV TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-APPLZ-CONV-DB
           END-IF
           IF IND-POL-DT-PRESC = 0
               MOVE POL-DT-PRESC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO POL-DT-PRESC-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           IF IND-POL-DT-PROP = 0
               MOVE POL-DT-PROP-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-PROP
           END-IF
           MOVE POL-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-INI-EFF
           MOVE POL-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-END-EFF
           MOVE POL-DT-DECOR-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-DECOR
           MOVE POL-DT-EMIS-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-EMIS
           IF IND-POL-DT-SCAD = 0
               MOVE POL-DT-SCAD-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-SCAD
           END-IF
           MOVE POL-DT-INI-VLDT-PROD-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO POL-DT-INI-VLDT-PROD
           IF IND-POL-DT-INI-VLDT-CONV = 0
               MOVE POL-DT-INI-VLDT-CONV-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-INI-VLDT-CONV
           END-IF
           IF IND-POL-DT-APPLZ-CONV = 0
               MOVE POL-DT-APPLZ-CONV-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-APPLZ-CONV
           END-IF
           IF IND-POL-DT-PRESC = 0
               MOVE POL-DT-PRESC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO POL-DT-PRESC
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

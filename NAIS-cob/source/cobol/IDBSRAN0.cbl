       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSRAN0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  24 GEN 2014.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDRAN0 END-EXEC.
           EXEC SQL INCLUDE IDBVRAN2 END-EXEC.
           EXEC SQL INCLUDE IDBVRAN3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVRAN1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 RAPP-ANA.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSRAN0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'RAPP_ANA' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RAPP_ANA
                ,ID_RAPP_ANA_COLLG
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,COD_SOGG
                ,TP_RAPP_ANA
                ,TP_PERS
                ,SEX
                ,DT_NASC
                ,FL_ESTAS
                ,INDIR_1
                ,INDIR_2
                ,INDIR_3
                ,TP_UTLZ_INDIR_1
                ,TP_UTLZ_INDIR_2
                ,TP_UTLZ_INDIR_3
                ,ESTR_CNT_CORR_ACCR
                ,ESTR_CNT_CORR_ADD
                ,ESTR_DOCTO
                ,PC_NEL_RAPP
                ,TP_MEZ_PAG_ADD
                ,TP_MEZ_PAG_ACCR
                ,COD_MATR
                ,TP_ADEGZ
                ,FL_TST_RSH
                ,COD_AZ
                ,IND_PRINC
                ,DT_DELIBERA_CDA
                ,DLG_AL_RISC
                ,LEGALE_RAPPR_PRINC
                ,TP_LEGALE_RAPPR
                ,TP_IND_PRINC
                ,TP_STAT_RID
                ,NOME_INT_RID
                ,COGN_INT_RID
                ,COGN_INT_TRATT
                ,NOME_INT_TRATT
                ,CF_INT_RID
                ,FL_COINC_DIP_CNTR
                ,FL_COINC_INT_CNTR
                ,DT_DECES
                ,FL_FUMATORE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_LAV_DIP
                ,TP_VARZ_PAGAT
                ,COD_RID
                ,TP_CAUS_RID
                ,IND_MASSA_CORP
                ,CAT_RSH_PROF
             INTO
                :RAN-ID-RAPP-ANA
               ,:RAN-ID-RAPP-ANA-COLLG
                :IND-RAN-ID-RAPP-ANA-COLLG
               ,:RAN-ID-OGG
               ,:RAN-TP-OGG
               ,:RAN-ID-MOVI-CRZ
               ,:RAN-ID-MOVI-CHIU
                :IND-RAN-ID-MOVI-CHIU
               ,:RAN-DT-INI-EFF-DB
               ,:RAN-DT-END-EFF-DB
               ,:RAN-COD-COMP-ANIA
               ,:RAN-COD-SOGG
                :IND-RAN-COD-SOGG
               ,:RAN-TP-RAPP-ANA
               ,:RAN-TP-PERS
                :IND-RAN-TP-PERS
               ,:RAN-SEX
                :IND-RAN-SEX
               ,:RAN-DT-NASC-DB
                :IND-RAN-DT-NASC
               ,:RAN-FL-ESTAS
                :IND-RAN-FL-ESTAS
               ,:RAN-INDIR-1
                :IND-RAN-INDIR-1
               ,:RAN-INDIR-2
                :IND-RAN-INDIR-2
               ,:RAN-INDIR-3
                :IND-RAN-INDIR-3
               ,:RAN-TP-UTLZ-INDIR-1
                :IND-RAN-TP-UTLZ-INDIR-1
               ,:RAN-TP-UTLZ-INDIR-2
                :IND-RAN-TP-UTLZ-INDIR-2
               ,:RAN-TP-UTLZ-INDIR-3
                :IND-RAN-TP-UTLZ-INDIR-3
               ,:RAN-ESTR-CNT-CORR-ACCR
                :IND-RAN-ESTR-CNT-CORR-ACCR
               ,:RAN-ESTR-CNT-CORR-ADD
                :IND-RAN-ESTR-CNT-CORR-ADD
               ,:RAN-ESTR-DOCTO
                :IND-RAN-ESTR-DOCTO
               ,:RAN-PC-NEL-RAPP
                :IND-RAN-PC-NEL-RAPP
               ,:RAN-TP-MEZ-PAG-ADD
                :IND-RAN-TP-MEZ-PAG-ADD
               ,:RAN-TP-MEZ-PAG-ACCR
                :IND-RAN-TP-MEZ-PAG-ACCR
               ,:RAN-COD-MATR
                :IND-RAN-COD-MATR
               ,:RAN-TP-ADEGZ
                :IND-RAN-TP-ADEGZ
               ,:RAN-FL-TST-RSH
                :IND-RAN-FL-TST-RSH
               ,:RAN-COD-AZ
                :IND-RAN-COD-AZ
               ,:RAN-IND-PRINC
                :IND-RAN-IND-PRINC
               ,:RAN-DT-DELIBERA-CDA-DB
                :IND-RAN-DT-DELIBERA-CDA
               ,:RAN-DLG-AL-RISC
                :IND-RAN-DLG-AL-RISC
               ,:RAN-LEGALE-RAPPR-PRINC
                :IND-RAN-LEGALE-RAPPR-PRINC
               ,:RAN-TP-LEGALE-RAPPR
                :IND-RAN-TP-LEGALE-RAPPR
               ,:RAN-TP-IND-PRINC
                :IND-RAN-TP-IND-PRINC
               ,:RAN-TP-STAT-RID
                :IND-RAN-TP-STAT-RID
               ,:RAN-NOME-INT-RID-VCHAR
                :IND-RAN-NOME-INT-RID
               ,:RAN-COGN-INT-RID-VCHAR
                :IND-RAN-COGN-INT-RID
               ,:RAN-COGN-INT-TRATT-VCHAR
                :IND-RAN-COGN-INT-TRATT
               ,:RAN-NOME-INT-TRATT-VCHAR
                :IND-RAN-NOME-INT-TRATT
               ,:RAN-CF-INT-RID
                :IND-RAN-CF-INT-RID
               ,:RAN-FL-COINC-DIP-CNTR
                :IND-RAN-FL-COINC-DIP-CNTR
               ,:RAN-FL-COINC-INT-CNTR
                :IND-RAN-FL-COINC-INT-CNTR
               ,:RAN-DT-DECES-DB
                :IND-RAN-DT-DECES
               ,:RAN-FL-FUMATORE
                :IND-RAN-FL-FUMATORE
               ,:RAN-DS-RIGA
               ,:RAN-DS-OPER-SQL
               ,:RAN-DS-VER
               ,:RAN-DS-TS-INI-CPTZ
               ,:RAN-DS-TS-END-CPTZ
               ,:RAN-DS-UTENTE
               ,:RAN-DS-STATO-ELAB
               ,:RAN-FL-LAV-DIP
                :IND-RAN-FL-LAV-DIP
               ,:RAN-TP-VARZ-PAGAT
                :IND-RAN-TP-VARZ-PAGAT
               ,:RAN-COD-RID
                :IND-RAN-COD-RID
               ,:RAN-TP-CAUS-RID
                :IND-RAN-TP-CAUS-RID
               ,:RAN-IND-MASSA-CORP
                :IND-RAN-IND-MASSA-CORP
               ,:RAN-CAT-RSH-PROF
                :IND-RAN-CAT-RSH-PROF
             FROM RAPP_ANA
             WHERE     DS_RIGA = :RAN-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO RAPP_ANA
                     (
                        ID_RAPP_ANA
                       ,ID_RAPP_ANA_COLLG
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,COD_SOGG
                       ,TP_RAPP_ANA
                       ,TP_PERS
                       ,SEX
                       ,DT_NASC
                       ,FL_ESTAS
                       ,INDIR_1
                       ,INDIR_2
                       ,INDIR_3
                       ,TP_UTLZ_INDIR_1
                       ,TP_UTLZ_INDIR_2
                       ,TP_UTLZ_INDIR_3
                       ,ESTR_CNT_CORR_ACCR
                       ,ESTR_CNT_CORR_ADD
                       ,ESTR_DOCTO
                       ,PC_NEL_RAPP
                       ,TP_MEZ_PAG_ADD
                       ,TP_MEZ_PAG_ACCR
                       ,COD_MATR
                       ,TP_ADEGZ
                       ,FL_TST_RSH
                       ,COD_AZ
                       ,IND_PRINC
                       ,DT_DELIBERA_CDA
                       ,DLG_AL_RISC
                       ,LEGALE_RAPPR_PRINC
                       ,TP_LEGALE_RAPPR
                       ,TP_IND_PRINC
                       ,TP_STAT_RID
                       ,NOME_INT_RID
                       ,COGN_INT_RID
                       ,COGN_INT_TRATT
                       ,NOME_INT_TRATT
                       ,CF_INT_RID
                       ,FL_COINC_DIP_CNTR
                       ,FL_COINC_INT_CNTR
                       ,DT_DECES
                       ,FL_FUMATORE
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,FL_LAV_DIP
                       ,TP_VARZ_PAGAT
                       ,COD_RID
                       ,TP_CAUS_RID
                       ,IND_MASSA_CORP
                       ,CAT_RSH_PROF
                     )
                 VALUES
                     (
                       :RAN-ID-RAPP-ANA
                       ,:RAN-ID-RAPP-ANA-COLLG
                        :IND-RAN-ID-RAPP-ANA-COLLG
                       ,:RAN-ID-OGG
                       ,:RAN-TP-OGG
                       ,:RAN-ID-MOVI-CRZ
                       ,:RAN-ID-MOVI-CHIU
                        :IND-RAN-ID-MOVI-CHIU
                       ,:RAN-DT-INI-EFF-DB
                       ,:RAN-DT-END-EFF-DB
                       ,:RAN-COD-COMP-ANIA
                       ,:RAN-COD-SOGG
                        :IND-RAN-COD-SOGG
                       ,:RAN-TP-RAPP-ANA
                       ,:RAN-TP-PERS
                        :IND-RAN-TP-PERS
                       ,:RAN-SEX
                        :IND-RAN-SEX
                       ,:RAN-DT-NASC-DB
                        :IND-RAN-DT-NASC
                       ,:RAN-FL-ESTAS
                        :IND-RAN-FL-ESTAS
                       ,:RAN-INDIR-1
                        :IND-RAN-INDIR-1
                       ,:RAN-INDIR-2
                        :IND-RAN-INDIR-2
                       ,:RAN-INDIR-3
                        :IND-RAN-INDIR-3
                       ,:RAN-TP-UTLZ-INDIR-1
                        :IND-RAN-TP-UTLZ-INDIR-1
                       ,:RAN-TP-UTLZ-INDIR-2
                        :IND-RAN-TP-UTLZ-INDIR-2
                       ,:RAN-TP-UTLZ-INDIR-3
                        :IND-RAN-TP-UTLZ-INDIR-3
                       ,:RAN-ESTR-CNT-CORR-ACCR
                        :IND-RAN-ESTR-CNT-CORR-ACCR
                       ,:RAN-ESTR-CNT-CORR-ADD
                        :IND-RAN-ESTR-CNT-CORR-ADD
                       ,:RAN-ESTR-DOCTO
                        :IND-RAN-ESTR-DOCTO
                       ,:RAN-PC-NEL-RAPP
                        :IND-RAN-PC-NEL-RAPP
                       ,:RAN-TP-MEZ-PAG-ADD
                        :IND-RAN-TP-MEZ-PAG-ADD
                       ,:RAN-TP-MEZ-PAG-ACCR
                        :IND-RAN-TP-MEZ-PAG-ACCR
                       ,:RAN-COD-MATR
                        :IND-RAN-COD-MATR
                       ,:RAN-TP-ADEGZ
                        :IND-RAN-TP-ADEGZ
                       ,:RAN-FL-TST-RSH
                        :IND-RAN-FL-TST-RSH
                       ,:RAN-COD-AZ
                        :IND-RAN-COD-AZ
                       ,:RAN-IND-PRINC
                        :IND-RAN-IND-PRINC
                       ,:RAN-DT-DELIBERA-CDA-DB
                        :IND-RAN-DT-DELIBERA-CDA
                       ,:RAN-DLG-AL-RISC
                        :IND-RAN-DLG-AL-RISC
                       ,:RAN-LEGALE-RAPPR-PRINC
                        :IND-RAN-LEGALE-RAPPR-PRINC
                       ,:RAN-TP-LEGALE-RAPPR
                        :IND-RAN-TP-LEGALE-RAPPR
                       ,:RAN-TP-IND-PRINC
                        :IND-RAN-TP-IND-PRINC
                       ,:RAN-TP-STAT-RID
                        :IND-RAN-TP-STAT-RID
                       ,:RAN-NOME-INT-RID-VCHAR
                        :IND-RAN-NOME-INT-RID
                       ,:RAN-COGN-INT-RID-VCHAR
                        :IND-RAN-COGN-INT-RID
                       ,:RAN-COGN-INT-TRATT-VCHAR
                        :IND-RAN-COGN-INT-TRATT
                       ,:RAN-NOME-INT-TRATT-VCHAR
                        :IND-RAN-NOME-INT-TRATT
                       ,:RAN-CF-INT-RID
                        :IND-RAN-CF-INT-RID
                       ,:RAN-FL-COINC-DIP-CNTR
                        :IND-RAN-FL-COINC-DIP-CNTR
                       ,:RAN-FL-COINC-INT-CNTR
                        :IND-RAN-FL-COINC-INT-CNTR
                       ,:RAN-DT-DECES-DB
                        :IND-RAN-DT-DECES
                       ,:RAN-FL-FUMATORE
                        :IND-RAN-FL-FUMATORE
                       ,:RAN-DS-RIGA
                       ,:RAN-DS-OPER-SQL
                       ,:RAN-DS-VER
                       ,:RAN-DS-TS-INI-CPTZ
                       ,:RAN-DS-TS-END-CPTZ
                       ,:RAN-DS-UTENTE
                       ,:RAN-DS-STATO-ELAB
                       ,:RAN-FL-LAV-DIP
                        :IND-RAN-FL-LAV-DIP
                       ,:RAN-TP-VARZ-PAGAT
                        :IND-RAN-TP-VARZ-PAGAT
                       ,:RAN-COD-RID
                        :IND-RAN-COD-RID
                       ,:RAN-TP-CAUS-RID
                        :IND-RAN-TP-CAUS-RID
                       ,:RAN-IND-MASSA-CORP
                        :IND-RAN-IND-MASSA-CORP
                       ,:RAN-CAT-RSH-PROF
                        :IND-RAN-CAT-RSH-PROF
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE RAPP_ANA SET

                   ID_RAPP_ANA            =
                :RAN-ID-RAPP-ANA
                  ,ID_RAPP_ANA_COLLG      =
                :RAN-ID-RAPP-ANA-COLLG
                                       :IND-RAN-ID-RAPP-ANA-COLLG
                  ,ID_OGG                 =
                :RAN-ID-OGG
                  ,TP_OGG                 =
                :RAN-TP-OGG
                  ,ID_MOVI_CRZ            =
                :RAN-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :RAN-ID-MOVI-CHIU
                                       :IND-RAN-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :RAN-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :RAN-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :RAN-COD-COMP-ANIA
                  ,COD_SOGG               =
                :RAN-COD-SOGG
                                       :IND-RAN-COD-SOGG
                  ,TP_RAPP_ANA            =
                :RAN-TP-RAPP-ANA
                  ,TP_PERS                =
                :RAN-TP-PERS
                                       :IND-RAN-TP-PERS
                  ,SEX                    =
                :RAN-SEX
                                       :IND-RAN-SEX
                  ,DT_NASC                =
           :RAN-DT-NASC-DB
                                       :IND-RAN-DT-NASC
                  ,FL_ESTAS               =
                :RAN-FL-ESTAS
                                       :IND-RAN-FL-ESTAS
                  ,INDIR_1                =
                :RAN-INDIR-1
                                       :IND-RAN-INDIR-1
                  ,INDIR_2                =
                :RAN-INDIR-2
                                       :IND-RAN-INDIR-2
                  ,INDIR_3                =
                :RAN-INDIR-3
                                       :IND-RAN-INDIR-3
                  ,TP_UTLZ_INDIR_1        =
                :RAN-TP-UTLZ-INDIR-1
                                       :IND-RAN-TP-UTLZ-INDIR-1
                  ,TP_UTLZ_INDIR_2        =
                :RAN-TP-UTLZ-INDIR-2
                                       :IND-RAN-TP-UTLZ-INDIR-2
                  ,TP_UTLZ_INDIR_3        =
                :RAN-TP-UTLZ-INDIR-3
                                       :IND-RAN-TP-UTLZ-INDIR-3
                  ,ESTR_CNT_CORR_ACCR     =
                :RAN-ESTR-CNT-CORR-ACCR
                                       :IND-RAN-ESTR-CNT-CORR-ACCR
                  ,ESTR_CNT_CORR_ADD      =
                :RAN-ESTR-CNT-CORR-ADD
                                       :IND-RAN-ESTR-CNT-CORR-ADD
                  ,ESTR_DOCTO             =
                :RAN-ESTR-DOCTO
                                       :IND-RAN-ESTR-DOCTO
                  ,PC_NEL_RAPP            =
                :RAN-PC-NEL-RAPP
                                       :IND-RAN-PC-NEL-RAPP
                  ,TP_MEZ_PAG_ADD         =
                :RAN-TP-MEZ-PAG-ADD
                                       :IND-RAN-TP-MEZ-PAG-ADD
                  ,TP_MEZ_PAG_ACCR        =
                :RAN-TP-MEZ-PAG-ACCR
                                       :IND-RAN-TP-MEZ-PAG-ACCR
                  ,COD_MATR               =
                :RAN-COD-MATR
                                       :IND-RAN-COD-MATR
                  ,TP_ADEGZ               =
                :RAN-TP-ADEGZ
                                       :IND-RAN-TP-ADEGZ
                  ,FL_TST_RSH             =
                :RAN-FL-TST-RSH
                                       :IND-RAN-FL-TST-RSH
                  ,COD_AZ                 =
                :RAN-COD-AZ
                                       :IND-RAN-COD-AZ
                  ,IND_PRINC              =
                :RAN-IND-PRINC
                                       :IND-RAN-IND-PRINC
                  ,DT_DELIBERA_CDA        =
           :RAN-DT-DELIBERA-CDA-DB
                                       :IND-RAN-DT-DELIBERA-CDA
                  ,DLG_AL_RISC            =
                :RAN-DLG-AL-RISC
                                       :IND-RAN-DLG-AL-RISC
                  ,LEGALE_RAPPR_PRINC     =
                :RAN-LEGALE-RAPPR-PRINC
                                       :IND-RAN-LEGALE-RAPPR-PRINC
                  ,TP_LEGALE_RAPPR        =
                :RAN-TP-LEGALE-RAPPR
                                       :IND-RAN-TP-LEGALE-RAPPR
                  ,TP_IND_PRINC           =
                :RAN-TP-IND-PRINC
                                       :IND-RAN-TP-IND-PRINC
                  ,TP_STAT_RID            =
                :RAN-TP-STAT-RID
                                       :IND-RAN-TP-STAT-RID
                  ,NOME_INT_RID           =
                :RAN-NOME-INT-RID-VCHAR
                                       :IND-RAN-NOME-INT-RID
                  ,COGN_INT_RID           =
                :RAN-COGN-INT-RID-VCHAR
                                       :IND-RAN-COGN-INT-RID
                  ,COGN_INT_TRATT         =
                :RAN-COGN-INT-TRATT-VCHAR
                                       :IND-RAN-COGN-INT-TRATT
                  ,NOME_INT_TRATT         =
                :RAN-NOME-INT-TRATT-VCHAR
                                       :IND-RAN-NOME-INT-TRATT
                  ,CF_INT_RID             =
                :RAN-CF-INT-RID
                                       :IND-RAN-CF-INT-RID
                  ,FL_COINC_DIP_CNTR      =
                :RAN-FL-COINC-DIP-CNTR
                                       :IND-RAN-FL-COINC-DIP-CNTR
                  ,FL_COINC_INT_CNTR      =
                :RAN-FL-COINC-INT-CNTR
                                       :IND-RAN-FL-COINC-INT-CNTR
                  ,DT_DECES               =
           :RAN-DT-DECES-DB
                                       :IND-RAN-DT-DECES
                  ,FL_FUMATORE            =
                :RAN-FL-FUMATORE
                                       :IND-RAN-FL-FUMATORE
                  ,DS_RIGA                =
                :RAN-DS-RIGA
                  ,DS_OPER_SQL            =
                :RAN-DS-OPER-SQL
                  ,DS_VER                 =
                :RAN-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :RAN-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :RAN-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :RAN-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :RAN-DS-STATO-ELAB
                  ,FL_LAV_DIP             =
                :RAN-FL-LAV-DIP
                                       :IND-RAN-FL-LAV-DIP
                  ,TP_VARZ_PAGAT          =
                :RAN-TP-VARZ-PAGAT
                                       :IND-RAN-TP-VARZ-PAGAT
                  ,COD_RID                =
                :RAN-COD-RID
                                       :IND-RAN-COD-RID
                  ,TP_CAUS_RID            =
                :RAN-TP-CAUS-RID
                                       :IND-RAN-TP-CAUS-RID
                  ,IND_MASSA_CORP         =
                :RAN-IND-MASSA-CORP
                                       :IND-RAN-IND-MASSA-CORP
                  ,CAT_RSH_PROF           =
                :RAN-CAT-RSH-PROF
                                       :IND-RAN-CAT-RSH-PROF
                WHERE     DS_RIGA = :RAN-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM RAPP_ANA
                WHERE     DS_RIGA = :RAN-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-RAN CURSOR FOR
              SELECT
                     ID_RAPP_ANA
                    ,ID_RAPP_ANA_COLLG
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,COD_SOGG
                    ,TP_RAPP_ANA
                    ,TP_PERS
                    ,SEX
                    ,DT_NASC
                    ,FL_ESTAS
                    ,INDIR_1
                    ,INDIR_2
                    ,INDIR_3
                    ,TP_UTLZ_INDIR_1
                    ,TP_UTLZ_INDIR_2
                    ,TP_UTLZ_INDIR_3
                    ,ESTR_CNT_CORR_ACCR
                    ,ESTR_CNT_CORR_ADD
                    ,ESTR_DOCTO
                    ,PC_NEL_RAPP
                    ,TP_MEZ_PAG_ADD
                    ,TP_MEZ_PAG_ACCR
                    ,COD_MATR
                    ,TP_ADEGZ
                    ,FL_TST_RSH
                    ,COD_AZ
                    ,IND_PRINC
                    ,DT_DELIBERA_CDA
                    ,DLG_AL_RISC
                    ,LEGALE_RAPPR_PRINC
                    ,TP_LEGALE_RAPPR
                    ,TP_IND_PRINC
                    ,TP_STAT_RID
                    ,NOME_INT_RID
                    ,COGN_INT_RID
                    ,COGN_INT_TRATT
                    ,NOME_INT_TRATT
                    ,CF_INT_RID
                    ,FL_COINC_DIP_CNTR
                    ,FL_COINC_INT_CNTR
                    ,DT_DECES
                    ,FL_FUMATORE
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,FL_LAV_DIP
                    ,TP_VARZ_PAGAT
                    ,COD_RID
                    ,TP_CAUS_RID
                    ,IND_MASSA_CORP
                    ,CAT_RSH_PROF
              FROM RAPP_ANA
              WHERE     ID_RAPP_ANA = :RAN-ID-RAPP-ANA
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RAPP_ANA
                ,ID_RAPP_ANA_COLLG
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,COD_SOGG
                ,TP_RAPP_ANA
                ,TP_PERS
                ,SEX
                ,DT_NASC
                ,FL_ESTAS
                ,INDIR_1
                ,INDIR_2
                ,INDIR_3
                ,TP_UTLZ_INDIR_1
                ,TP_UTLZ_INDIR_2
                ,TP_UTLZ_INDIR_3
                ,ESTR_CNT_CORR_ACCR
                ,ESTR_CNT_CORR_ADD
                ,ESTR_DOCTO
                ,PC_NEL_RAPP
                ,TP_MEZ_PAG_ADD
                ,TP_MEZ_PAG_ACCR
                ,COD_MATR
                ,TP_ADEGZ
                ,FL_TST_RSH
                ,COD_AZ
                ,IND_PRINC
                ,DT_DELIBERA_CDA
                ,DLG_AL_RISC
                ,LEGALE_RAPPR_PRINC
                ,TP_LEGALE_RAPPR
                ,TP_IND_PRINC
                ,TP_STAT_RID
                ,NOME_INT_RID
                ,COGN_INT_RID
                ,COGN_INT_TRATT
                ,NOME_INT_TRATT
                ,CF_INT_RID
                ,FL_COINC_DIP_CNTR
                ,FL_COINC_INT_CNTR
                ,DT_DECES
                ,FL_FUMATORE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_LAV_DIP
                ,TP_VARZ_PAGAT
                ,COD_RID
                ,TP_CAUS_RID
                ,IND_MASSA_CORP
                ,CAT_RSH_PROF
             INTO
                :RAN-ID-RAPP-ANA
               ,:RAN-ID-RAPP-ANA-COLLG
                :IND-RAN-ID-RAPP-ANA-COLLG
               ,:RAN-ID-OGG
               ,:RAN-TP-OGG
               ,:RAN-ID-MOVI-CRZ
               ,:RAN-ID-MOVI-CHIU
                :IND-RAN-ID-MOVI-CHIU
               ,:RAN-DT-INI-EFF-DB
               ,:RAN-DT-END-EFF-DB
               ,:RAN-COD-COMP-ANIA
               ,:RAN-COD-SOGG
                :IND-RAN-COD-SOGG
               ,:RAN-TP-RAPP-ANA
               ,:RAN-TP-PERS
                :IND-RAN-TP-PERS
               ,:RAN-SEX
                :IND-RAN-SEX
               ,:RAN-DT-NASC-DB
                :IND-RAN-DT-NASC
               ,:RAN-FL-ESTAS
                :IND-RAN-FL-ESTAS
               ,:RAN-INDIR-1
                :IND-RAN-INDIR-1
               ,:RAN-INDIR-2
                :IND-RAN-INDIR-2
               ,:RAN-INDIR-3
                :IND-RAN-INDIR-3
               ,:RAN-TP-UTLZ-INDIR-1
                :IND-RAN-TP-UTLZ-INDIR-1
               ,:RAN-TP-UTLZ-INDIR-2
                :IND-RAN-TP-UTLZ-INDIR-2
               ,:RAN-TP-UTLZ-INDIR-3
                :IND-RAN-TP-UTLZ-INDIR-3
               ,:RAN-ESTR-CNT-CORR-ACCR
                :IND-RAN-ESTR-CNT-CORR-ACCR
               ,:RAN-ESTR-CNT-CORR-ADD
                :IND-RAN-ESTR-CNT-CORR-ADD
               ,:RAN-ESTR-DOCTO
                :IND-RAN-ESTR-DOCTO
               ,:RAN-PC-NEL-RAPP
                :IND-RAN-PC-NEL-RAPP
               ,:RAN-TP-MEZ-PAG-ADD
                :IND-RAN-TP-MEZ-PAG-ADD
               ,:RAN-TP-MEZ-PAG-ACCR
                :IND-RAN-TP-MEZ-PAG-ACCR
               ,:RAN-COD-MATR
                :IND-RAN-COD-MATR
               ,:RAN-TP-ADEGZ
                :IND-RAN-TP-ADEGZ
               ,:RAN-FL-TST-RSH
                :IND-RAN-FL-TST-RSH
               ,:RAN-COD-AZ
                :IND-RAN-COD-AZ
               ,:RAN-IND-PRINC
                :IND-RAN-IND-PRINC
               ,:RAN-DT-DELIBERA-CDA-DB
                :IND-RAN-DT-DELIBERA-CDA
               ,:RAN-DLG-AL-RISC
                :IND-RAN-DLG-AL-RISC
               ,:RAN-LEGALE-RAPPR-PRINC
                :IND-RAN-LEGALE-RAPPR-PRINC
               ,:RAN-TP-LEGALE-RAPPR
                :IND-RAN-TP-LEGALE-RAPPR
               ,:RAN-TP-IND-PRINC
                :IND-RAN-TP-IND-PRINC
               ,:RAN-TP-STAT-RID
                :IND-RAN-TP-STAT-RID
               ,:RAN-NOME-INT-RID-VCHAR
                :IND-RAN-NOME-INT-RID
               ,:RAN-COGN-INT-RID-VCHAR
                :IND-RAN-COGN-INT-RID
               ,:RAN-COGN-INT-TRATT-VCHAR
                :IND-RAN-COGN-INT-TRATT
               ,:RAN-NOME-INT-TRATT-VCHAR
                :IND-RAN-NOME-INT-TRATT
               ,:RAN-CF-INT-RID
                :IND-RAN-CF-INT-RID
               ,:RAN-FL-COINC-DIP-CNTR
                :IND-RAN-FL-COINC-DIP-CNTR
               ,:RAN-FL-COINC-INT-CNTR
                :IND-RAN-FL-COINC-INT-CNTR
               ,:RAN-DT-DECES-DB
                :IND-RAN-DT-DECES
               ,:RAN-FL-FUMATORE
                :IND-RAN-FL-FUMATORE
               ,:RAN-DS-RIGA
               ,:RAN-DS-OPER-SQL
               ,:RAN-DS-VER
               ,:RAN-DS-TS-INI-CPTZ
               ,:RAN-DS-TS-END-CPTZ
               ,:RAN-DS-UTENTE
               ,:RAN-DS-STATO-ELAB
               ,:RAN-FL-LAV-DIP
                :IND-RAN-FL-LAV-DIP
               ,:RAN-TP-VARZ-PAGAT
                :IND-RAN-TP-VARZ-PAGAT
               ,:RAN-COD-RID
                :IND-RAN-COD-RID
               ,:RAN-TP-CAUS-RID
                :IND-RAN-TP-CAUS-RID
               ,:RAN-IND-MASSA-CORP
                :IND-RAN-IND-MASSA-CORP
               ,:RAN-CAT-RSH-PROF
                :IND-RAN-CAT-RSH-PROF
             FROM RAPP_ANA
             WHERE     ID_RAPP_ANA = :RAN-ID-RAPP-ANA
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE RAPP_ANA SET

                   ID_RAPP_ANA            =
                :RAN-ID-RAPP-ANA
                  ,ID_RAPP_ANA_COLLG      =
                :RAN-ID-RAPP-ANA-COLLG
                                       :IND-RAN-ID-RAPP-ANA-COLLG
                  ,ID_OGG                 =
                :RAN-ID-OGG
                  ,TP_OGG                 =
                :RAN-TP-OGG
                  ,ID_MOVI_CRZ            =
                :RAN-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :RAN-ID-MOVI-CHIU
                                       :IND-RAN-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :RAN-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :RAN-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :RAN-COD-COMP-ANIA
                  ,COD_SOGG               =
                :RAN-COD-SOGG
                                       :IND-RAN-COD-SOGG
                  ,TP_RAPP_ANA            =
                :RAN-TP-RAPP-ANA
                  ,TP_PERS                =
                :RAN-TP-PERS
                                       :IND-RAN-TP-PERS
                  ,SEX                    =
                :RAN-SEX
                                       :IND-RAN-SEX
                  ,DT_NASC                =
           :RAN-DT-NASC-DB
                                       :IND-RAN-DT-NASC
                  ,FL_ESTAS               =
                :RAN-FL-ESTAS
                                       :IND-RAN-FL-ESTAS
                  ,INDIR_1                =
                :RAN-INDIR-1
                                       :IND-RAN-INDIR-1
                  ,INDIR_2                =
                :RAN-INDIR-2
                                       :IND-RAN-INDIR-2
                  ,INDIR_3                =
                :RAN-INDIR-3
                                       :IND-RAN-INDIR-3
                  ,TP_UTLZ_INDIR_1        =
                :RAN-TP-UTLZ-INDIR-1
                                       :IND-RAN-TP-UTLZ-INDIR-1
                  ,TP_UTLZ_INDIR_2        =
                :RAN-TP-UTLZ-INDIR-2
                                       :IND-RAN-TP-UTLZ-INDIR-2
                  ,TP_UTLZ_INDIR_3        =
                :RAN-TP-UTLZ-INDIR-3
                                       :IND-RAN-TP-UTLZ-INDIR-3
                  ,ESTR_CNT_CORR_ACCR     =
                :RAN-ESTR-CNT-CORR-ACCR
                                       :IND-RAN-ESTR-CNT-CORR-ACCR
                  ,ESTR_CNT_CORR_ADD      =
                :RAN-ESTR-CNT-CORR-ADD
                                       :IND-RAN-ESTR-CNT-CORR-ADD
                  ,ESTR_DOCTO             =
                :RAN-ESTR-DOCTO
                                       :IND-RAN-ESTR-DOCTO
                  ,PC_NEL_RAPP            =
                :RAN-PC-NEL-RAPP
                                       :IND-RAN-PC-NEL-RAPP
                  ,TP_MEZ_PAG_ADD         =
                :RAN-TP-MEZ-PAG-ADD
                                       :IND-RAN-TP-MEZ-PAG-ADD
                  ,TP_MEZ_PAG_ACCR        =
                :RAN-TP-MEZ-PAG-ACCR
                                       :IND-RAN-TP-MEZ-PAG-ACCR
                  ,COD_MATR               =
                :RAN-COD-MATR
                                       :IND-RAN-COD-MATR
                  ,TP_ADEGZ               =
                :RAN-TP-ADEGZ
                                       :IND-RAN-TP-ADEGZ
                  ,FL_TST_RSH             =
                :RAN-FL-TST-RSH
                                       :IND-RAN-FL-TST-RSH
                  ,COD_AZ                 =
                :RAN-COD-AZ
                                       :IND-RAN-COD-AZ
                  ,IND_PRINC              =
                :RAN-IND-PRINC
                                       :IND-RAN-IND-PRINC
                  ,DT_DELIBERA_CDA        =
           :RAN-DT-DELIBERA-CDA-DB
                                       :IND-RAN-DT-DELIBERA-CDA
                  ,DLG_AL_RISC            =
                :RAN-DLG-AL-RISC
                                       :IND-RAN-DLG-AL-RISC
                  ,LEGALE_RAPPR_PRINC     =
                :RAN-LEGALE-RAPPR-PRINC
                                       :IND-RAN-LEGALE-RAPPR-PRINC
                  ,TP_LEGALE_RAPPR        =
                :RAN-TP-LEGALE-RAPPR
                                       :IND-RAN-TP-LEGALE-RAPPR
                  ,TP_IND_PRINC           =
                :RAN-TP-IND-PRINC
                                       :IND-RAN-TP-IND-PRINC
                  ,TP_STAT_RID            =
                :RAN-TP-STAT-RID
                                       :IND-RAN-TP-STAT-RID
                  ,NOME_INT_RID           =
                :RAN-NOME-INT-RID-VCHAR
                                       :IND-RAN-NOME-INT-RID
                  ,COGN_INT_RID           =
                :RAN-COGN-INT-RID-VCHAR
                                       :IND-RAN-COGN-INT-RID
                  ,COGN_INT_TRATT         =
                :RAN-COGN-INT-TRATT-VCHAR
                                       :IND-RAN-COGN-INT-TRATT
                  ,NOME_INT_TRATT         =
                :RAN-NOME-INT-TRATT-VCHAR
                                       :IND-RAN-NOME-INT-TRATT
                  ,CF_INT_RID             =
                :RAN-CF-INT-RID
                                       :IND-RAN-CF-INT-RID
                  ,FL_COINC_DIP_CNTR      =
                :RAN-FL-COINC-DIP-CNTR
                                       :IND-RAN-FL-COINC-DIP-CNTR
                  ,FL_COINC_INT_CNTR      =
                :RAN-FL-COINC-INT-CNTR
                                       :IND-RAN-FL-COINC-INT-CNTR
                  ,DT_DECES               =
           :RAN-DT-DECES-DB
                                       :IND-RAN-DT-DECES
                  ,FL_FUMATORE            =
                :RAN-FL-FUMATORE
                                       :IND-RAN-FL-FUMATORE
                  ,DS_RIGA                =
                :RAN-DS-RIGA
                  ,DS_OPER_SQL            =
                :RAN-DS-OPER-SQL
                  ,DS_VER                 =
                :RAN-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :RAN-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :RAN-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :RAN-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :RAN-DS-STATO-ELAB
                  ,FL_LAV_DIP             =
                :RAN-FL-LAV-DIP
                                       :IND-RAN-FL-LAV-DIP
                  ,TP_VARZ_PAGAT          =
                :RAN-TP-VARZ-PAGAT
                                       :IND-RAN-TP-VARZ-PAGAT
                  ,COD_RID                =
                :RAN-COD-RID
                                       :IND-RAN-COD-RID
                  ,TP_CAUS_RID            =
                :RAN-TP-CAUS-RID
                                       :IND-RAN-TP-CAUS-RID
                  ,IND_MASSA_CORP         =
                :RAN-IND-MASSA-CORP
                                       :IND-RAN-IND-MASSA-CORP
                  ,CAT_RSH_PROF           =
                :RAN-CAT-RSH-PROF
                                       :IND-RAN-CAT-RSH-PROF
                WHERE     DS_RIGA = :RAN-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-RAN
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-RAN
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-RAN
           INTO
                :RAN-ID-RAPP-ANA
               ,:RAN-ID-RAPP-ANA-COLLG
                :IND-RAN-ID-RAPP-ANA-COLLG
               ,:RAN-ID-OGG
               ,:RAN-TP-OGG
               ,:RAN-ID-MOVI-CRZ
               ,:RAN-ID-MOVI-CHIU
                :IND-RAN-ID-MOVI-CHIU
               ,:RAN-DT-INI-EFF-DB
               ,:RAN-DT-END-EFF-DB
               ,:RAN-COD-COMP-ANIA
               ,:RAN-COD-SOGG
                :IND-RAN-COD-SOGG
               ,:RAN-TP-RAPP-ANA
               ,:RAN-TP-PERS
                :IND-RAN-TP-PERS
               ,:RAN-SEX
                :IND-RAN-SEX
               ,:RAN-DT-NASC-DB
                :IND-RAN-DT-NASC
               ,:RAN-FL-ESTAS
                :IND-RAN-FL-ESTAS
               ,:RAN-INDIR-1
                :IND-RAN-INDIR-1
               ,:RAN-INDIR-2
                :IND-RAN-INDIR-2
               ,:RAN-INDIR-3
                :IND-RAN-INDIR-3
               ,:RAN-TP-UTLZ-INDIR-1
                :IND-RAN-TP-UTLZ-INDIR-1
               ,:RAN-TP-UTLZ-INDIR-2
                :IND-RAN-TP-UTLZ-INDIR-2
               ,:RAN-TP-UTLZ-INDIR-3
                :IND-RAN-TP-UTLZ-INDIR-3
               ,:RAN-ESTR-CNT-CORR-ACCR
                :IND-RAN-ESTR-CNT-CORR-ACCR
               ,:RAN-ESTR-CNT-CORR-ADD
                :IND-RAN-ESTR-CNT-CORR-ADD
               ,:RAN-ESTR-DOCTO
                :IND-RAN-ESTR-DOCTO
               ,:RAN-PC-NEL-RAPP
                :IND-RAN-PC-NEL-RAPP
               ,:RAN-TP-MEZ-PAG-ADD
                :IND-RAN-TP-MEZ-PAG-ADD
               ,:RAN-TP-MEZ-PAG-ACCR
                :IND-RAN-TP-MEZ-PAG-ACCR
               ,:RAN-COD-MATR
                :IND-RAN-COD-MATR
               ,:RAN-TP-ADEGZ
                :IND-RAN-TP-ADEGZ
               ,:RAN-FL-TST-RSH
                :IND-RAN-FL-TST-RSH
               ,:RAN-COD-AZ
                :IND-RAN-COD-AZ
               ,:RAN-IND-PRINC
                :IND-RAN-IND-PRINC
               ,:RAN-DT-DELIBERA-CDA-DB
                :IND-RAN-DT-DELIBERA-CDA
               ,:RAN-DLG-AL-RISC
                :IND-RAN-DLG-AL-RISC
               ,:RAN-LEGALE-RAPPR-PRINC
                :IND-RAN-LEGALE-RAPPR-PRINC
               ,:RAN-TP-LEGALE-RAPPR
                :IND-RAN-TP-LEGALE-RAPPR
               ,:RAN-TP-IND-PRINC
                :IND-RAN-TP-IND-PRINC
               ,:RAN-TP-STAT-RID
                :IND-RAN-TP-STAT-RID
               ,:RAN-NOME-INT-RID-VCHAR
                :IND-RAN-NOME-INT-RID
               ,:RAN-COGN-INT-RID-VCHAR
                :IND-RAN-COGN-INT-RID
               ,:RAN-COGN-INT-TRATT-VCHAR
                :IND-RAN-COGN-INT-TRATT
               ,:RAN-NOME-INT-TRATT-VCHAR
                :IND-RAN-NOME-INT-TRATT
               ,:RAN-CF-INT-RID
                :IND-RAN-CF-INT-RID
               ,:RAN-FL-COINC-DIP-CNTR
                :IND-RAN-FL-COINC-DIP-CNTR
               ,:RAN-FL-COINC-INT-CNTR
                :IND-RAN-FL-COINC-INT-CNTR
               ,:RAN-DT-DECES-DB
                :IND-RAN-DT-DECES
               ,:RAN-FL-FUMATORE
                :IND-RAN-FL-FUMATORE
               ,:RAN-DS-RIGA
               ,:RAN-DS-OPER-SQL
               ,:RAN-DS-VER
               ,:RAN-DS-TS-INI-CPTZ
               ,:RAN-DS-TS-END-CPTZ
               ,:RAN-DS-UTENTE
               ,:RAN-DS-STATO-ELAB
               ,:RAN-FL-LAV-DIP
                :IND-RAN-FL-LAV-DIP
               ,:RAN-TP-VARZ-PAGAT
                :IND-RAN-TP-VARZ-PAGAT
               ,:RAN-COD-RID
                :IND-RAN-COD-RID
               ,:RAN-TP-CAUS-RID
                :IND-RAN-TP-CAUS-RID
               ,:RAN-IND-MASSA-CORP
                :IND-RAN-IND-MASSA-CORP
               ,:RAN-CAT-RSH-PROF
                :IND-RAN-CAT-RSH-PROF
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDO-EFF-RAN CURSOR FOR
              SELECT
                     ID_RAPP_ANA
                    ,ID_RAPP_ANA_COLLG
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,COD_SOGG
                    ,TP_RAPP_ANA
                    ,TP_PERS
                    ,SEX
                    ,DT_NASC
                    ,FL_ESTAS
                    ,INDIR_1
                    ,INDIR_2
                    ,INDIR_3
                    ,TP_UTLZ_INDIR_1
                    ,TP_UTLZ_INDIR_2
                    ,TP_UTLZ_INDIR_3
                    ,ESTR_CNT_CORR_ACCR
                    ,ESTR_CNT_CORR_ADD
                    ,ESTR_DOCTO
                    ,PC_NEL_RAPP
                    ,TP_MEZ_PAG_ADD
                    ,TP_MEZ_PAG_ACCR
                    ,COD_MATR
                    ,TP_ADEGZ
                    ,FL_TST_RSH
                    ,COD_AZ
                    ,IND_PRINC
                    ,DT_DELIBERA_CDA
                    ,DLG_AL_RISC
                    ,LEGALE_RAPPR_PRINC
                    ,TP_LEGALE_RAPPR
                    ,TP_IND_PRINC
                    ,TP_STAT_RID
                    ,NOME_INT_RID
                    ,COGN_INT_RID
                    ,COGN_INT_TRATT
                    ,NOME_INT_TRATT
                    ,CF_INT_RID
                    ,FL_COINC_DIP_CNTR
                    ,FL_COINC_INT_CNTR
                    ,DT_DECES
                    ,FL_FUMATORE
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,FL_LAV_DIP
                    ,TP_VARZ_PAGAT
                    ,COD_RID
                    ,TP_CAUS_RID
                    ,IND_MASSA_CORP
                    ,CAT_RSH_PROF
              FROM RAPP_ANA
              WHERE     ID_OGG = :RAN-ID-OGG
                    AND TP_OGG = :RAN-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_RAPP_ANA ASC

           END-EXEC.

       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RAPP_ANA
                ,ID_RAPP_ANA_COLLG
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,COD_SOGG
                ,TP_RAPP_ANA
                ,TP_PERS
                ,SEX
                ,DT_NASC
                ,FL_ESTAS
                ,INDIR_1
                ,INDIR_2
                ,INDIR_3
                ,TP_UTLZ_INDIR_1
                ,TP_UTLZ_INDIR_2
                ,TP_UTLZ_INDIR_3
                ,ESTR_CNT_CORR_ACCR
                ,ESTR_CNT_CORR_ADD
                ,ESTR_DOCTO
                ,PC_NEL_RAPP
                ,TP_MEZ_PAG_ADD
                ,TP_MEZ_PAG_ACCR
                ,COD_MATR
                ,TP_ADEGZ
                ,FL_TST_RSH
                ,COD_AZ
                ,IND_PRINC
                ,DT_DELIBERA_CDA
                ,DLG_AL_RISC
                ,LEGALE_RAPPR_PRINC
                ,TP_LEGALE_RAPPR
                ,TP_IND_PRINC
                ,TP_STAT_RID
                ,NOME_INT_RID
                ,COGN_INT_RID
                ,COGN_INT_TRATT
                ,NOME_INT_TRATT
                ,CF_INT_RID
                ,FL_COINC_DIP_CNTR
                ,FL_COINC_INT_CNTR
                ,DT_DECES
                ,FL_FUMATORE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_LAV_DIP
                ,TP_VARZ_PAGAT
                ,COD_RID
                ,TP_CAUS_RID
                ,IND_MASSA_CORP
                ,CAT_RSH_PROF
             INTO
                :RAN-ID-RAPP-ANA
               ,:RAN-ID-RAPP-ANA-COLLG
                :IND-RAN-ID-RAPP-ANA-COLLG
               ,:RAN-ID-OGG
               ,:RAN-TP-OGG
               ,:RAN-ID-MOVI-CRZ
               ,:RAN-ID-MOVI-CHIU
                :IND-RAN-ID-MOVI-CHIU
               ,:RAN-DT-INI-EFF-DB
               ,:RAN-DT-END-EFF-DB
               ,:RAN-COD-COMP-ANIA
               ,:RAN-COD-SOGG
                :IND-RAN-COD-SOGG
               ,:RAN-TP-RAPP-ANA
               ,:RAN-TP-PERS
                :IND-RAN-TP-PERS
               ,:RAN-SEX
                :IND-RAN-SEX
               ,:RAN-DT-NASC-DB
                :IND-RAN-DT-NASC
               ,:RAN-FL-ESTAS
                :IND-RAN-FL-ESTAS
               ,:RAN-INDIR-1
                :IND-RAN-INDIR-1
               ,:RAN-INDIR-2
                :IND-RAN-INDIR-2
               ,:RAN-INDIR-3
                :IND-RAN-INDIR-3
               ,:RAN-TP-UTLZ-INDIR-1
                :IND-RAN-TP-UTLZ-INDIR-1
               ,:RAN-TP-UTLZ-INDIR-2
                :IND-RAN-TP-UTLZ-INDIR-2
               ,:RAN-TP-UTLZ-INDIR-3
                :IND-RAN-TP-UTLZ-INDIR-3
               ,:RAN-ESTR-CNT-CORR-ACCR
                :IND-RAN-ESTR-CNT-CORR-ACCR
               ,:RAN-ESTR-CNT-CORR-ADD
                :IND-RAN-ESTR-CNT-CORR-ADD
               ,:RAN-ESTR-DOCTO
                :IND-RAN-ESTR-DOCTO
               ,:RAN-PC-NEL-RAPP
                :IND-RAN-PC-NEL-RAPP
               ,:RAN-TP-MEZ-PAG-ADD
                :IND-RAN-TP-MEZ-PAG-ADD
               ,:RAN-TP-MEZ-PAG-ACCR
                :IND-RAN-TP-MEZ-PAG-ACCR
               ,:RAN-COD-MATR
                :IND-RAN-COD-MATR
               ,:RAN-TP-ADEGZ
                :IND-RAN-TP-ADEGZ
               ,:RAN-FL-TST-RSH
                :IND-RAN-FL-TST-RSH
               ,:RAN-COD-AZ
                :IND-RAN-COD-AZ
               ,:RAN-IND-PRINC
                :IND-RAN-IND-PRINC
               ,:RAN-DT-DELIBERA-CDA-DB
                :IND-RAN-DT-DELIBERA-CDA
               ,:RAN-DLG-AL-RISC
                :IND-RAN-DLG-AL-RISC
               ,:RAN-LEGALE-RAPPR-PRINC
                :IND-RAN-LEGALE-RAPPR-PRINC
               ,:RAN-TP-LEGALE-RAPPR
                :IND-RAN-TP-LEGALE-RAPPR
               ,:RAN-TP-IND-PRINC
                :IND-RAN-TP-IND-PRINC
               ,:RAN-TP-STAT-RID
                :IND-RAN-TP-STAT-RID
               ,:RAN-NOME-INT-RID-VCHAR
                :IND-RAN-NOME-INT-RID
               ,:RAN-COGN-INT-RID-VCHAR
                :IND-RAN-COGN-INT-RID
               ,:RAN-COGN-INT-TRATT-VCHAR
                :IND-RAN-COGN-INT-TRATT
               ,:RAN-NOME-INT-TRATT-VCHAR
                :IND-RAN-NOME-INT-TRATT
               ,:RAN-CF-INT-RID
                :IND-RAN-CF-INT-RID
               ,:RAN-FL-COINC-DIP-CNTR
                :IND-RAN-FL-COINC-DIP-CNTR
               ,:RAN-FL-COINC-INT-CNTR
                :IND-RAN-FL-COINC-INT-CNTR
               ,:RAN-DT-DECES-DB
                :IND-RAN-DT-DECES
               ,:RAN-FL-FUMATORE
                :IND-RAN-FL-FUMATORE
               ,:RAN-DS-RIGA
               ,:RAN-DS-OPER-SQL
               ,:RAN-DS-VER
               ,:RAN-DS-TS-INI-CPTZ
               ,:RAN-DS-TS-END-CPTZ
               ,:RAN-DS-UTENTE
               ,:RAN-DS-STATO-ELAB
               ,:RAN-FL-LAV-DIP
                :IND-RAN-FL-LAV-DIP
               ,:RAN-TP-VARZ-PAGAT
                :IND-RAN-TP-VARZ-PAGAT
               ,:RAN-COD-RID
                :IND-RAN-COD-RID
               ,:RAN-TP-CAUS-RID
                :IND-RAN-TP-CAUS-RID
               ,:RAN-IND-MASSA-CORP
                :IND-RAN-IND-MASSA-CORP
               ,:RAN-CAT-RSH-PROF
                :IND-RAN-CAT-RSH-PROF
             FROM RAPP_ANA
             WHERE     ID_OGG = :RAN-ID-OGG
                    AND TP_OGG = :RAN-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           EXEC SQL
                OPEN C-IDO-EFF-RAN
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           EXEC SQL
                CLOSE C-IDO-EFF-RAN
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           EXEC SQL
                FETCH C-IDO-EFF-RAN
           INTO
                :RAN-ID-RAPP-ANA
               ,:RAN-ID-RAPP-ANA-COLLG
                :IND-RAN-ID-RAPP-ANA-COLLG
               ,:RAN-ID-OGG
               ,:RAN-TP-OGG
               ,:RAN-ID-MOVI-CRZ
               ,:RAN-ID-MOVI-CHIU
                :IND-RAN-ID-MOVI-CHIU
               ,:RAN-DT-INI-EFF-DB
               ,:RAN-DT-END-EFF-DB
               ,:RAN-COD-COMP-ANIA
               ,:RAN-COD-SOGG
                :IND-RAN-COD-SOGG
               ,:RAN-TP-RAPP-ANA
               ,:RAN-TP-PERS
                :IND-RAN-TP-PERS
               ,:RAN-SEX
                :IND-RAN-SEX
               ,:RAN-DT-NASC-DB
                :IND-RAN-DT-NASC
               ,:RAN-FL-ESTAS
                :IND-RAN-FL-ESTAS
               ,:RAN-INDIR-1
                :IND-RAN-INDIR-1
               ,:RAN-INDIR-2
                :IND-RAN-INDIR-2
               ,:RAN-INDIR-3
                :IND-RAN-INDIR-3
               ,:RAN-TP-UTLZ-INDIR-1
                :IND-RAN-TP-UTLZ-INDIR-1
               ,:RAN-TP-UTLZ-INDIR-2
                :IND-RAN-TP-UTLZ-INDIR-2
               ,:RAN-TP-UTLZ-INDIR-3
                :IND-RAN-TP-UTLZ-INDIR-3
               ,:RAN-ESTR-CNT-CORR-ACCR
                :IND-RAN-ESTR-CNT-CORR-ACCR
               ,:RAN-ESTR-CNT-CORR-ADD
                :IND-RAN-ESTR-CNT-CORR-ADD
               ,:RAN-ESTR-DOCTO
                :IND-RAN-ESTR-DOCTO
               ,:RAN-PC-NEL-RAPP
                :IND-RAN-PC-NEL-RAPP
               ,:RAN-TP-MEZ-PAG-ADD
                :IND-RAN-TP-MEZ-PAG-ADD
               ,:RAN-TP-MEZ-PAG-ACCR
                :IND-RAN-TP-MEZ-PAG-ACCR
               ,:RAN-COD-MATR
                :IND-RAN-COD-MATR
               ,:RAN-TP-ADEGZ
                :IND-RAN-TP-ADEGZ
               ,:RAN-FL-TST-RSH
                :IND-RAN-FL-TST-RSH
               ,:RAN-COD-AZ
                :IND-RAN-COD-AZ
               ,:RAN-IND-PRINC
                :IND-RAN-IND-PRINC
               ,:RAN-DT-DELIBERA-CDA-DB
                :IND-RAN-DT-DELIBERA-CDA
               ,:RAN-DLG-AL-RISC
                :IND-RAN-DLG-AL-RISC
               ,:RAN-LEGALE-RAPPR-PRINC
                :IND-RAN-LEGALE-RAPPR-PRINC
               ,:RAN-TP-LEGALE-RAPPR
                :IND-RAN-TP-LEGALE-RAPPR
               ,:RAN-TP-IND-PRINC
                :IND-RAN-TP-IND-PRINC
               ,:RAN-TP-STAT-RID
                :IND-RAN-TP-STAT-RID
               ,:RAN-NOME-INT-RID-VCHAR
                :IND-RAN-NOME-INT-RID
               ,:RAN-COGN-INT-RID-VCHAR
                :IND-RAN-COGN-INT-RID
               ,:RAN-COGN-INT-TRATT-VCHAR
                :IND-RAN-COGN-INT-TRATT
               ,:RAN-NOME-INT-TRATT-VCHAR
                :IND-RAN-NOME-INT-TRATT
               ,:RAN-CF-INT-RID
                :IND-RAN-CF-INT-RID
               ,:RAN-FL-COINC-DIP-CNTR
                :IND-RAN-FL-COINC-DIP-CNTR
               ,:RAN-FL-COINC-INT-CNTR
                :IND-RAN-FL-COINC-INT-CNTR
               ,:RAN-DT-DECES-DB
                :IND-RAN-DT-DECES
               ,:RAN-FL-FUMATORE
                :IND-RAN-FL-FUMATORE
               ,:RAN-DS-RIGA
               ,:RAN-DS-OPER-SQL
               ,:RAN-DS-VER
               ,:RAN-DS-TS-INI-CPTZ
               ,:RAN-DS-TS-END-CPTZ
               ,:RAN-DS-UTENTE
               ,:RAN-DS-STATO-ELAB
               ,:RAN-FL-LAV-DIP
                :IND-RAN-FL-LAV-DIP
               ,:RAN-TP-VARZ-PAGAT
                :IND-RAN-TP-VARZ-PAGAT
               ,:RAN-COD-RID
                :IND-RAN-COD-RID
               ,:RAN-TP-CAUS-RID
                :IND-RAN-TP-CAUS-RID
               ,:RAN-IND-MASSA-CORP
                :IND-RAN-IND-MASSA-CORP
               ,:RAN-CAT-RSH-PROF
                :IND-RAN-CAT-RSH-PROF
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RAPP_ANA
                ,ID_RAPP_ANA_COLLG
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,COD_SOGG
                ,TP_RAPP_ANA
                ,TP_PERS
                ,SEX
                ,DT_NASC
                ,FL_ESTAS
                ,INDIR_1
                ,INDIR_2
                ,INDIR_3
                ,TP_UTLZ_INDIR_1
                ,TP_UTLZ_INDIR_2
                ,TP_UTLZ_INDIR_3
                ,ESTR_CNT_CORR_ACCR
                ,ESTR_CNT_CORR_ADD
                ,ESTR_DOCTO
                ,PC_NEL_RAPP
                ,TP_MEZ_PAG_ADD
                ,TP_MEZ_PAG_ACCR
                ,COD_MATR
                ,TP_ADEGZ
                ,FL_TST_RSH
                ,COD_AZ
                ,IND_PRINC
                ,DT_DELIBERA_CDA
                ,DLG_AL_RISC
                ,LEGALE_RAPPR_PRINC
                ,TP_LEGALE_RAPPR
                ,TP_IND_PRINC
                ,TP_STAT_RID
                ,NOME_INT_RID
                ,COGN_INT_RID
                ,COGN_INT_TRATT
                ,NOME_INT_TRATT
                ,CF_INT_RID
                ,FL_COINC_DIP_CNTR
                ,FL_COINC_INT_CNTR
                ,DT_DECES
                ,FL_FUMATORE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_LAV_DIP
                ,TP_VARZ_PAGAT
                ,COD_RID
                ,TP_CAUS_RID
                ,IND_MASSA_CORP
                ,CAT_RSH_PROF
             INTO
                :RAN-ID-RAPP-ANA
               ,:RAN-ID-RAPP-ANA-COLLG
                :IND-RAN-ID-RAPP-ANA-COLLG
               ,:RAN-ID-OGG
               ,:RAN-TP-OGG
               ,:RAN-ID-MOVI-CRZ
               ,:RAN-ID-MOVI-CHIU
                :IND-RAN-ID-MOVI-CHIU
               ,:RAN-DT-INI-EFF-DB
               ,:RAN-DT-END-EFF-DB
               ,:RAN-COD-COMP-ANIA
               ,:RAN-COD-SOGG
                :IND-RAN-COD-SOGG
               ,:RAN-TP-RAPP-ANA
               ,:RAN-TP-PERS
                :IND-RAN-TP-PERS
               ,:RAN-SEX
                :IND-RAN-SEX
               ,:RAN-DT-NASC-DB
                :IND-RAN-DT-NASC
               ,:RAN-FL-ESTAS
                :IND-RAN-FL-ESTAS
               ,:RAN-INDIR-1
                :IND-RAN-INDIR-1
               ,:RAN-INDIR-2
                :IND-RAN-INDIR-2
               ,:RAN-INDIR-3
                :IND-RAN-INDIR-3
               ,:RAN-TP-UTLZ-INDIR-1
                :IND-RAN-TP-UTLZ-INDIR-1
               ,:RAN-TP-UTLZ-INDIR-2
                :IND-RAN-TP-UTLZ-INDIR-2
               ,:RAN-TP-UTLZ-INDIR-3
                :IND-RAN-TP-UTLZ-INDIR-3
               ,:RAN-ESTR-CNT-CORR-ACCR
                :IND-RAN-ESTR-CNT-CORR-ACCR
               ,:RAN-ESTR-CNT-CORR-ADD
                :IND-RAN-ESTR-CNT-CORR-ADD
               ,:RAN-ESTR-DOCTO
                :IND-RAN-ESTR-DOCTO
               ,:RAN-PC-NEL-RAPP
                :IND-RAN-PC-NEL-RAPP
               ,:RAN-TP-MEZ-PAG-ADD
                :IND-RAN-TP-MEZ-PAG-ADD
               ,:RAN-TP-MEZ-PAG-ACCR
                :IND-RAN-TP-MEZ-PAG-ACCR
               ,:RAN-COD-MATR
                :IND-RAN-COD-MATR
               ,:RAN-TP-ADEGZ
                :IND-RAN-TP-ADEGZ
               ,:RAN-FL-TST-RSH
                :IND-RAN-FL-TST-RSH
               ,:RAN-COD-AZ
                :IND-RAN-COD-AZ
               ,:RAN-IND-PRINC
                :IND-RAN-IND-PRINC
               ,:RAN-DT-DELIBERA-CDA-DB
                :IND-RAN-DT-DELIBERA-CDA
               ,:RAN-DLG-AL-RISC
                :IND-RAN-DLG-AL-RISC
               ,:RAN-LEGALE-RAPPR-PRINC
                :IND-RAN-LEGALE-RAPPR-PRINC
               ,:RAN-TP-LEGALE-RAPPR
                :IND-RAN-TP-LEGALE-RAPPR
               ,:RAN-TP-IND-PRINC
                :IND-RAN-TP-IND-PRINC
               ,:RAN-TP-STAT-RID
                :IND-RAN-TP-STAT-RID
               ,:RAN-NOME-INT-RID-VCHAR
                :IND-RAN-NOME-INT-RID
               ,:RAN-COGN-INT-RID-VCHAR
                :IND-RAN-COGN-INT-RID
               ,:RAN-COGN-INT-TRATT-VCHAR
                :IND-RAN-COGN-INT-TRATT
               ,:RAN-NOME-INT-TRATT-VCHAR
                :IND-RAN-NOME-INT-TRATT
               ,:RAN-CF-INT-RID
                :IND-RAN-CF-INT-RID
               ,:RAN-FL-COINC-DIP-CNTR
                :IND-RAN-FL-COINC-DIP-CNTR
               ,:RAN-FL-COINC-INT-CNTR
                :IND-RAN-FL-COINC-INT-CNTR
               ,:RAN-DT-DECES-DB
                :IND-RAN-DT-DECES
               ,:RAN-FL-FUMATORE
                :IND-RAN-FL-FUMATORE
               ,:RAN-DS-RIGA
               ,:RAN-DS-OPER-SQL
               ,:RAN-DS-VER
               ,:RAN-DS-TS-INI-CPTZ
               ,:RAN-DS-TS-END-CPTZ
               ,:RAN-DS-UTENTE
               ,:RAN-DS-STATO-ELAB
               ,:RAN-FL-LAV-DIP
                :IND-RAN-FL-LAV-DIP
               ,:RAN-TP-VARZ-PAGAT
                :IND-RAN-TP-VARZ-PAGAT
               ,:RAN-COD-RID
                :IND-RAN-COD-RID
               ,:RAN-TP-CAUS-RID
                :IND-RAN-TP-CAUS-RID
               ,:RAN-IND-MASSA-CORP
                :IND-RAN-IND-MASSA-CORP
               ,:RAN-CAT-RSH-PROF
                :IND-RAN-CAT-RSH-PROF
             FROM RAPP_ANA
             WHERE     ID_RAPP_ANA = :RAN-ID-RAPP-ANA
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDO-CPZ-RAN CURSOR FOR
              SELECT
                     ID_RAPP_ANA
                    ,ID_RAPP_ANA_COLLG
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,COD_SOGG
                    ,TP_RAPP_ANA
                    ,TP_PERS
                    ,SEX
                    ,DT_NASC
                    ,FL_ESTAS
                    ,INDIR_1
                    ,INDIR_2
                    ,INDIR_3
                    ,TP_UTLZ_INDIR_1
                    ,TP_UTLZ_INDIR_2
                    ,TP_UTLZ_INDIR_3
                    ,ESTR_CNT_CORR_ACCR
                    ,ESTR_CNT_CORR_ADD
                    ,ESTR_DOCTO
                    ,PC_NEL_RAPP
                    ,TP_MEZ_PAG_ADD
                    ,TP_MEZ_PAG_ACCR
                    ,COD_MATR
                    ,TP_ADEGZ
                    ,FL_TST_RSH
                    ,COD_AZ
                    ,IND_PRINC
                    ,DT_DELIBERA_CDA
                    ,DLG_AL_RISC
                    ,LEGALE_RAPPR_PRINC
                    ,TP_LEGALE_RAPPR
                    ,TP_IND_PRINC
                    ,TP_STAT_RID
                    ,NOME_INT_RID
                    ,COGN_INT_RID
                    ,COGN_INT_TRATT
                    ,NOME_INT_TRATT
                    ,CF_INT_RID
                    ,FL_COINC_DIP_CNTR
                    ,FL_COINC_INT_CNTR
                    ,DT_DECES
                    ,FL_FUMATORE
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,FL_LAV_DIP
                    ,TP_VARZ_PAGAT
                    ,COD_RID
                    ,TP_CAUS_RID
                    ,IND_MASSA_CORP
                    ,CAT_RSH_PROF
              FROM RAPP_ANA
              WHERE     ID_OGG = :RAN-ID-OGG
           AND TP_OGG = :RAN-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_RAPP_ANA ASC

           END-EXEC.

       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RAPP_ANA
                ,ID_RAPP_ANA_COLLG
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,COD_SOGG
                ,TP_RAPP_ANA
                ,TP_PERS
                ,SEX
                ,DT_NASC
                ,FL_ESTAS
                ,INDIR_1
                ,INDIR_2
                ,INDIR_3
                ,TP_UTLZ_INDIR_1
                ,TP_UTLZ_INDIR_2
                ,TP_UTLZ_INDIR_3
                ,ESTR_CNT_CORR_ACCR
                ,ESTR_CNT_CORR_ADD
                ,ESTR_DOCTO
                ,PC_NEL_RAPP
                ,TP_MEZ_PAG_ADD
                ,TP_MEZ_PAG_ACCR
                ,COD_MATR
                ,TP_ADEGZ
                ,FL_TST_RSH
                ,COD_AZ
                ,IND_PRINC
                ,DT_DELIBERA_CDA
                ,DLG_AL_RISC
                ,LEGALE_RAPPR_PRINC
                ,TP_LEGALE_RAPPR
                ,TP_IND_PRINC
                ,TP_STAT_RID
                ,NOME_INT_RID
                ,COGN_INT_RID
                ,COGN_INT_TRATT
                ,NOME_INT_TRATT
                ,CF_INT_RID
                ,FL_COINC_DIP_CNTR
                ,FL_COINC_INT_CNTR
                ,DT_DECES
                ,FL_FUMATORE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_LAV_DIP
                ,TP_VARZ_PAGAT
                ,COD_RID
                ,TP_CAUS_RID
                ,IND_MASSA_CORP
                ,CAT_RSH_PROF
             INTO
                :RAN-ID-RAPP-ANA
               ,:RAN-ID-RAPP-ANA-COLLG
                :IND-RAN-ID-RAPP-ANA-COLLG
               ,:RAN-ID-OGG
               ,:RAN-TP-OGG
               ,:RAN-ID-MOVI-CRZ
               ,:RAN-ID-MOVI-CHIU
                :IND-RAN-ID-MOVI-CHIU
               ,:RAN-DT-INI-EFF-DB
               ,:RAN-DT-END-EFF-DB
               ,:RAN-COD-COMP-ANIA
               ,:RAN-COD-SOGG
                :IND-RAN-COD-SOGG
               ,:RAN-TP-RAPP-ANA
               ,:RAN-TP-PERS
                :IND-RAN-TP-PERS
               ,:RAN-SEX
                :IND-RAN-SEX
               ,:RAN-DT-NASC-DB
                :IND-RAN-DT-NASC
               ,:RAN-FL-ESTAS
                :IND-RAN-FL-ESTAS
               ,:RAN-INDIR-1
                :IND-RAN-INDIR-1
               ,:RAN-INDIR-2
                :IND-RAN-INDIR-2
               ,:RAN-INDIR-3
                :IND-RAN-INDIR-3
               ,:RAN-TP-UTLZ-INDIR-1
                :IND-RAN-TP-UTLZ-INDIR-1
               ,:RAN-TP-UTLZ-INDIR-2
                :IND-RAN-TP-UTLZ-INDIR-2
               ,:RAN-TP-UTLZ-INDIR-3
                :IND-RAN-TP-UTLZ-INDIR-3
               ,:RAN-ESTR-CNT-CORR-ACCR
                :IND-RAN-ESTR-CNT-CORR-ACCR
               ,:RAN-ESTR-CNT-CORR-ADD
                :IND-RAN-ESTR-CNT-CORR-ADD
               ,:RAN-ESTR-DOCTO
                :IND-RAN-ESTR-DOCTO
               ,:RAN-PC-NEL-RAPP
                :IND-RAN-PC-NEL-RAPP
               ,:RAN-TP-MEZ-PAG-ADD
                :IND-RAN-TP-MEZ-PAG-ADD
               ,:RAN-TP-MEZ-PAG-ACCR
                :IND-RAN-TP-MEZ-PAG-ACCR
               ,:RAN-COD-MATR
                :IND-RAN-COD-MATR
               ,:RAN-TP-ADEGZ
                :IND-RAN-TP-ADEGZ
               ,:RAN-FL-TST-RSH
                :IND-RAN-FL-TST-RSH
               ,:RAN-COD-AZ
                :IND-RAN-COD-AZ
               ,:RAN-IND-PRINC
                :IND-RAN-IND-PRINC
               ,:RAN-DT-DELIBERA-CDA-DB
                :IND-RAN-DT-DELIBERA-CDA
               ,:RAN-DLG-AL-RISC
                :IND-RAN-DLG-AL-RISC
               ,:RAN-LEGALE-RAPPR-PRINC
                :IND-RAN-LEGALE-RAPPR-PRINC
               ,:RAN-TP-LEGALE-RAPPR
                :IND-RAN-TP-LEGALE-RAPPR
               ,:RAN-TP-IND-PRINC
                :IND-RAN-TP-IND-PRINC
               ,:RAN-TP-STAT-RID
                :IND-RAN-TP-STAT-RID
               ,:RAN-NOME-INT-RID-VCHAR
                :IND-RAN-NOME-INT-RID
               ,:RAN-COGN-INT-RID-VCHAR
                :IND-RAN-COGN-INT-RID
               ,:RAN-COGN-INT-TRATT-VCHAR
                :IND-RAN-COGN-INT-TRATT
               ,:RAN-NOME-INT-TRATT-VCHAR
                :IND-RAN-NOME-INT-TRATT
               ,:RAN-CF-INT-RID
                :IND-RAN-CF-INT-RID
               ,:RAN-FL-COINC-DIP-CNTR
                :IND-RAN-FL-COINC-DIP-CNTR
               ,:RAN-FL-COINC-INT-CNTR
                :IND-RAN-FL-COINC-INT-CNTR
               ,:RAN-DT-DECES-DB
                :IND-RAN-DT-DECES
               ,:RAN-FL-FUMATORE
                :IND-RAN-FL-FUMATORE
               ,:RAN-DS-RIGA
               ,:RAN-DS-OPER-SQL
               ,:RAN-DS-VER
               ,:RAN-DS-TS-INI-CPTZ
               ,:RAN-DS-TS-END-CPTZ
               ,:RAN-DS-UTENTE
               ,:RAN-DS-STATO-ELAB
               ,:RAN-FL-LAV-DIP
                :IND-RAN-FL-LAV-DIP
               ,:RAN-TP-VARZ-PAGAT
                :IND-RAN-TP-VARZ-PAGAT
               ,:RAN-COD-RID
                :IND-RAN-COD-RID
               ,:RAN-TP-CAUS-RID
                :IND-RAN-TP-CAUS-RID
               ,:RAN-IND-MASSA-CORP
                :IND-RAN-IND-MASSA-CORP
               ,:RAN-CAT-RSH-PROF
                :IND-RAN-CAT-RSH-PROF
             FROM RAPP_ANA
             WHERE     ID_OGG = :RAN-ID-OGG
                    AND TP_OGG = :RAN-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           EXEC SQL
                OPEN C-IDO-CPZ-RAN
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           EXEC SQL
                CLOSE C-IDO-CPZ-RAN
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           EXEC SQL
                FETCH C-IDO-CPZ-RAN
           INTO
                :RAN-ID-RAPP-ANA
               ,:RAN-ID-RAPP-ANA-COLLG
                :IND-RAN-ID-RAPP-ANA-COLLG
               ,:RAN-ID-OGG
               ,:RAN-TP-OGG
               ,:RAN-ID-MOVI-CRZ
               ,:RAN-ID-MOVI-CHIU
                :IND-RAN-ID-MOVI-CHIU
               ,:RAN-DT-INI-EFF-DB
               ,:RAN-DT-END-EFF-DB
               ,:RAN-COD-COMP-ANIA
               ,:RAN-COD-SOGG
                :IND-RAN-COD-SOGG
               ,:RAN-TP-RAPP-ANA
               ,:RAN-TP-PERS
                :IND-RAN-TP-PERS
               ,:RAN-SEX
                :IND-RAN-SEX
               ,:RAN-DT-NASC-DB
                :IND-RAN-DT-NASC
               ,:RAN-FL-ESTAS
                :IND-RAN-FL-ESTAS
               ,:RAN-INDIR-1
                :IND-RAN-INDIR-1
               ,:RAN-INDIR-2
                :IND-RAN-INDIR-2
               ,:RAN-INDIR-3
                :IND-RAN-INDIR-3
               ,:RAN-TP-UTLZ-INDIR-1
                :IND-RAN-TP-UTLZ-INDIR-1
               ,:RAN-TP-UTLZ-INDIR-2
                :IND-RAN-TP-UTLZ-INDIR-2
               ,:RAN-TP-UTLZ-INDIR-3
                :IND-RAN-TP-UTLZ-INDIR-3
               ,:RAN-ESTR-CNT-CORR-ACCR
                :IND-RAN-ESTR-CNT-CORR-ACCR
               ,:RAN-ESTR-CNT-CORR-ADD
                :IND-RAN-ESTR-CNT-CORR-ADD
               ,:RAN-ESTR-DOCTO
                :IND-RAN-ESTR-DOCTO
               ,:RAN-PC-NEL-RAPP
                :IND-RAN-PC-NEL-RAPP
               ,:RAN-TP-MEZ-PAG-ADD
                :IND-RAN-TP-MEZ-PAG-ADD
               ,:RAN-TP-MEZ-PAG-ACCR
                :IND-RAN-TP-MEZ-PAG-ACCR
               ,:RAN-COD-MATR
                :IND-RAN-COD-MATR
               ,:RAN-TP-ADEGZ
                :IND-RAN-TP-ADEGZ
               ,:RAN-FL-TST-RSH
                :IND-RAN-FL-TST-RSH
               ,:RAN-COD-AZ
                :IND-RAN-COD-AZ
               ,:RAN-IND-PRINC
                :IND-RAN-IND-PRINC
               ,:RAN-DT-DELIBERA-CDA-DB
                :IND-RAN-DT-DELIBERA-CDA
               ,:RAN-DLG-AL-RISC
                :IND-RAN-DLG-AL-RISC
               ,:RAN-LEGALE-RAPPR-PRINC
                :IND-RAN-LEGALE-RAPPR-PRINC
               ,:RAN-TP-LEGALE-RAPPR
                :IND-RAN-TP-LEGALE-RAPPR
               ,:RAN-TP-IND-PRINC
                :IND-RAN-TP-IND-PRINC
               ,:RAN-TP-STAT-RID
                :IND-RAN-TP-STAT-RID
               ,:RAN-NOME-INT-RID-VCHAR
                :IND-RAN-NOME-INT-RID
               ,:RAN-COGN-INT-RID-VCHAR
                :IND-RAN-COGN-INT-RID
               ,:RAN-COGN-INT-TRATT-VCHAR
                :IND-RAN-COGN-INT-TRATT
               ,:RAN-NOME-INT-TRATT-VCHAR
                :IND-RAN-NOME-INT-TRATT
               ,:RAN-CF-INT-RID
                :IND-RAN-CF-INT-RID
               ,:RAN-FL-COINC-DIP-CNTR
                :IND-RAN-FL-COINC-DIP-CNTR
               ,:RAN-FL-COINC-INT-CNTR
                :IND-RAN-FL-COINC-INT-CNTR
               ,:RAN-DT-DECES-DB
                :IND-RAN-DT-DECES
               ,:RAN-FL-FUMATORE
                :IND-RAN-FL-FUMATORE
               ,:RAN-DS-RIGA
               ,:RAN-DS-OPER-SQL
               ,:RAN-DS-VER
               ,:RAN-DS-TS-INI-CPTZ
               ,:RAN-DS-TS-END-CPTZ
               ,:RAN-DS-UTENTE
               ,:RAN-DS-STATO-ELAB
               ,:RAN-FL-LAV-DIP
                :IND-RAN-FL-LAV-DIP
               ,:RAN-TP-VARZ-PAGAT
                :IND-RAN-TP-VARZ-PAGAT
               ,:RAN-COD-RID
                :IND-RAN-COD-RID
               ,:RAN-TP-CAUS-RID
                :IND-RAN-TP-CAUS-RID
               ,:RAN-IND-MASSA-CORP
                :IND-RAN-IND-MASSA-CORP
               ,:RAN-CAT-RSH-PROF
                :IND-RAN-CAT-RSH-PROF
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B770-CLOSE-CURSOR-IDO-CPZ     THRU B770-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-RAN-ID-RAPP-ANA-COLLG = -1
              MOVE HIGH-VALUES TO RAN-ID-RAPP-ANA-COLLG-NULL
           END-IF
           IF IND-RAN-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO RAN-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-RAN-COD-SOGG = -1
              MOVE HIGH-VALUES TO RAN-COD-SOGG-NULL
           END-IF
           IF IND-RAN-TP-PERS = -1
              MOVE HIGH-VALUES TO RAN-TP-PERS-NULL
           END-IF
           IF IND-RAN-SEX = -1
              MOVE HIGH-VALUES TO RAN-SEX-NULL
           END-IF
           IF IND-RAN-DT-NASC = -1
              MOVE HIGH-VALUES TO RAN-DT-NASC-NULL
           END-IF
           IF IND-RAN-FL-ESTAS = -1
              MOVE HIGH-VALUES TO RAN-FL-ESTAS-NULL
           END-IF
           IF IND-RAN-INDIR-1 = -1
              MOVE HIGH-VALUES TO RAN-INDIR-1-NULL
           END-IF
           IF IND-RAN-INDIR-2 = -1
              MOVE HIGH-VALUES TO RAN-INDIR-2-NULL
           END-IF
           IF IND-RAN-INDIR-3 = -1
              MOVE HIGH-VALUES TO RAN-INDIR-3-NULL
           END-IF
           IF IND-RAN-TP-UTLZ-INDIR-1 = -1
              MOVE HIGH-VALUES TO RAN-TP-UTLZ-INDIR-1-NULL
           END-IF
           IF IND-RAN-TP-UTLZ-INDIR-2 = -1
              MOVE HIGH-VALUES TO RAN-TP-UTLZ-INDIR-2-NULL
           END-IF
           IF IND-RAN-TP-UTLZ-INDIR-3 = -1
              MOVE HIGH-VALUES TO RAN-TP-UTLZ-INDIR-3-NULL
           END-IF
           IF IND-RAN-ESTR-CNT-CORR-ACCR = -1
              MOVE HIGH-VALUES TO RAN-ESTR-CNT-CORR-ACCR-NULL
           END-IF
           IF IND-RAN-ESTR-CNT-CORR-ADD = -1
              MOVE HIGH-VALUES TO RAN-ESTR-CNT-CORR-ADD-NULL
           END-IF
           IF IND-RAN-ESTR-DOCTO = -1
              MOVE HIGH-VALUES TO RAN-ESTR-DOCTO-NULL
           END-IF
           IF IND-RAN-PC-NEL-RAPP = -1
              MOVE HIGH-VALUES TO RAN-PC-NEL-RAPP-NULL
           END-IF
           IF IND-RAN-TP-MEZ-PAG-ADD = -1
              MOVE HIGH-VALUES TO RAN-TP-MEZ-PAG-ADD-NULL
           END-IF
           IF IND-RAN-TP-MEZ-PAG-ACCR = -1
              MOVE HIGH-VALUES TO RAN-TP-MEZ-PAG-ACCR-NULL
           END-IF
           IF IND-RAN-COD-MATR = -1
              MOVE HIGH-VALUES TO RAN-COD-MATR-NULL
           END-IF
           IF IND-RAN-TP-ADEGZ = -1
              MOVE HIGH-VALUES TO RAN-TP-ADEGZ-NULL
           END-IF
           IF IND-RAN-FL-TST-RSH = -1
              MOVE HIGH-VALUES TO RAN-FL-TST-RSH-NULL
           END-IF
           IF IND-RAN-COD-AZ = -1
              MOVE HIGH-VALUES TO RAN-COD-AZ-NULL
           END-IF
           IF IND-RAN-IND-PRINC = -1
              MOVE HIGH-VALUES TO RAN-IND-PRINC-NULL
           END-IF
           IF IND-RAN-DT-DELIBERA-CDA = -1
              MOVE HIGH-VALUES TO RAN-DT-DELIBERA-CDA-NULL
           END-IF
           IF IND-RAN-DLG-AL-RISC = -1
              MOVE HIGH-VALUES TO RAN-DLG-AL-RISC-NULL
           END-IF
           IF IND-RAN-LEGALE-RAPPR-PRINC = -1
              MOVE HIGH-VALUES TO RAN-LEGALE-RAPPR-PRINC-NULL
           END-IF
           IF IND-RAN-TP-LEGALE-RAPPR = -1
              MOVE HIGH-VALUES TO RAN-TP-LEGALE-RAPPR-NULL
           END-IF
           IF IND-RAN-TP-IND-PRINC = -1
              MOVE HIGH-VALUES TO RAN-TP-IND-PRINC-NULL
           END-IF
           IF IND-RAN-TP-STAT-RID = -1
              MOVE HIGH-VALUES TO RAN-TP-STAT-RID-NULL
           END-IF
           IF IND-RAN-NOME-INT-RID = -1
              MOVE HIGH-VALUES TO RAN-NOME-INT-RID
           END-IF
           IF IND-RAN-COGN-INT-RID = -1
              MOVE HIGH-VALUES TO RAN-COGN-INT-RID
           END-IF
           IF IND-RAN-COGN-INT-TRATT = -1
              MOVE HIGH-VALUES TO RAN-COGN-INT-TRATT
           END-IF
           IF IND-RAN-NOME-INT-TRATT = -1
              MOVE HIGH-VALUES TO RAN-NOME-INT-TRATT
           END-IF
           IF IND-RAN-CF-INT-RID = -1
              MOVE HIGH-VALUES TO RAN-CF-INT-RID-NULL
           END-IF
           IF IND-RAN-FL-COINC-DIP-CNTR = -1
              MOVE HIGH-VALUES TO RAN-FL-COINC-DIP-CNTR-NULL
           END-IF
           IF IND-RAN-FL-COINC-INT-CNTR = -1
              MOVE HIGH-VALUES TO RAN-FL-COINC-INT-CNTR-NULL
           END-IF
           IF IND-RAN-DT-DECES = -1
              MOVE HIGH-VALUES TO RAN-DT-DECES-NULL
           END-IF
           IF IND-RAN-FL-FUMATORE = -1
              MOVE HIGH-VALUES TO RAN-FL-FUMATORE-NULL
           END-IF
           IF IND-RAN-FL-LAV-DIP = -1
              MOVE HIGH-VALUES TO RAN-FL-LAV-DIP-NULL
           END-IF
           IF IND-RAN-TP-VARZ-PAGAT = -1
              MOVE HIGH-VALUES TO RAN-TP-VARZ-PAGAT-NULL
           END-IF
           IF IND-RAN-COD-RID = -1
              MOVE HIGH-VALUES TO RAN-COD-RID-NULL
           END-IF
           IF IND-RAN-TP-CAUS-RID = -1
              MOVE HIGH-VALUES TO RAN-TP-CAUS-RID-NULL
           END-IF
           IF IND-RAN-IND-MASSA-CORP = -1
              MOVE HIGH-VALUES TO RAN-IND-MASSA-CORP-NULL
           END-IF
           IF IND-RAN-CAT-RSH-PROF = -1
              MOVE HIGH-VALUES TO RAN-CAT-RSH-PROF-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO RAN-DS-OPER-SQL
           MOVE 0                   TO RAN-DS-VER
           MOVE IDSV0003-USER-NAME TO RAN-DS-UTENTE
           MOVE '1'                   TO RAN-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO RAN-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO RAN-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF RAN-ID-RAPP-ANA-COLLG-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-ID-RAPP-ANA-COLLG
           ELSE
              MOVE 0 TO IND-RAN-ID-RAPP-ANA-COLLG
           END-IF
           IF RAN-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-RAN-ID-MOVI-CHIU
           END-IF
           IF RAN-COD-SOGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-COD-SOGG
           ELSE
              MOVE 0 TO IND-RAN-COD-SOGG
           END-IF
           IF RAN-TP-PERS-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-TP-PERS
           ELSE
              MOVE 0 TO IND-RAN-TP-PERS
           END-IF
           IF RAN-SEX-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-SEX
           ELSE
              MOVE 0 TO IND-RAN-SEX
           END-IF
           IF RAN-DT-NASC-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-DT-NASC
           ELSE
              MOVE 0 TO IND-RAN-DT-NASC
           END-IF
           IF RAN-FL-ESTAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-FL-ESTAS
           ELSE
              MOVE 0 TO IND-RAN-FL-ESTAS
           END-IF
           IF RAN-INDIR-1-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-INDIR-1
           ELSE
              MOVE 0 TO IND-RAN-INDIR-1
           END-IF
           IF RAN-INDIR-2-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-INDIR-2
           ELSE
              MOVE 0 TO IND-RAN-INDIR-2
           END-IF
           IF RAN-INDIR-3-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-INDIR-3
           ELSE
              MOVE 0 TO IND-RAN-INDIR-3
           END-IF
           IF RAN-TP-UTLZ-INDIR-1-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-TP-UTLZ-INDIR-1
           ELSE
              MOVE 0 TO IND-RAN-TP-UTLZ-INDIR-1
           END-IF
           IF RAN-TP-UTLZ-INDIR-2-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-TP-UTLZ-INDIR-2
           ELSE
              MOVE 0 TO IND-RAN-TP-UTLZ-INDIR-2
           END-IF
           IF RAN-TP-UTLZ-INDIR-3-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-TP-UTLZ-INDIR-3
           ELSE
              MOVE 0 TO IND-RAN-TP-UTLZ-INDIR-3
           END-IF
           IF RAN-ESTR-CNT-CORR-ACCR-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-ESTR-CNT-CORR-ACCR
           ELSE
              MOVE 0 TO IND-RAN-ESTR-CNT-CORR-ACCR
           END-IF
           IF RAN-ESTR-CNT-CORR-ADD-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-ESTR-CNT-CORR-ADD
           ELSE
              MOVE 0 TO IND-RAN-ESTR-CNT-CORR-ADD
           END-IF
           IF RAN-ESTR-DOCTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-ESTR-DOCTO
           ELSE
              MOVE 0 TO IND-RAN-ESTR-DOCTO
           END-IF
           IF RAN-PC-NEL-RAPP-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-PC-NEL-RAPP
           ELSE
              MOVE 0 TO IND-RAN-PC-NEL-RAPP
           END-IF
           IF RAN-TP-MEZ-PAG-ADD-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-TP-MEZ-PAG-ADD
           ELSE
              MOVE 0 TO IND-RAN-TP-MEZ-PAG-ADD
           END-IF
           IF RAN-TP-MEZ-PAG-ACCR-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-TP-MEZ-PAG-ACCR
           ELSE
              MOVE 0 TO IND-RAN-TP-MEZ-PAG-ACCR
           END-IF
           IF RAN-COD-MATR-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-COD-MATR
           ELSE
              MOVE 0 TO IND-RAN-COD-MATR
           END-IF
           IF RAN-TP-ADEGZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-TP-ADEGZ
           ELSE
              MOVE 0 TO IND-RAN-TP-ADEGZ
           END-IF
           IF RAN-FL-TST-RSH-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-FL-TST-RSH
           ELSE
              MOVE 0 TO IND-RAN-FL-TST-RSH
           END-IF
           IF RAN-COD-AZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-COD-AZ
           ELSE
              MOVE 0 TO IND-RAN-COD-AZ
           END-IF
           IF RAN-IND-PRINC-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-IND-PRINC
           ELSE
              MOVE 0 TO IND-RAN-IND-PRINC
           END-IF
           IF RAN-DT-DELIBERA-CDA-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-DT-DELIBERA-CDA
           ELSE
              MOVE 0 TO IND-RAN-DT-DELIBERA-CDA
           END-IF
           IF RAN-DLG-AL-RISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-DLG-AL-RISC
           ELSE
              MOVE 0 TO IND-RAN-DLG-AL-RISC
           END-IF
           IF RAN-LEGALE-RAPPR-PRINC-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-LEGALE-RAPPR-PRINC
           ELSE
              MOVE 0 TO IND-RAN-LEGALE-RAPPR-PRINC
           END-IF
           IF RAN-TP-LEGALE-RAPPR-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-TP-LEGALE-RAPPR
           ELSE
              MOVE 0 TO IND-RAN-TP-LEGALE-RAPPR
           END-IF
           IF RAN-TP-IND-PRINC-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-TP-IND-PRINC
           ELSE
              MOVE 0 TO IND-RAN-TP-IND-PRINC
           END-IF
           IF RAN-TP-STAT-RID-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-TP-STAT-RID
           ELSE
              MOVE 0 TO IND-RAN-TP-STAT-RID
           END-IF
           IF RAN-NOME-INT-RID = HIGH-VALUES
              MOVE -1 TO IND-RAN-NOME-INT-RID
           ELSE
              MOVE 0 TO IND-RAN-NOME-INT-RID
           END-IF
           IF RAN-COGN-INT-RID = HIGH-VALUES
              MOVE -1 TO IND-RAN-COGN-INT-RID
           ELSE
              MOVE 0 TO IND-RAN-COGN-INT-RID
           END-IF
           IF RAN-COGN-INT-TRATT = HIGH-VALUES
              MOVE -1 TO IND-RAN-COGN-INT-TRATT
           ELSE
              MOVE 0 TO IND-RAN-COGN-INT-TRATT
           END-IF
           IF RAN-NOME-INT-TRATT = HIGH-VALUES
              MOVE -1 TO IND-RAN-NOME-INT-TRATT
           ELSE
              MOVE 0 TO IND-RAN-NOME-INT-TRATT
           END-IF
           IF RAN-CF-INT-RID-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-CF-INT-RID
           ELSE
              MOVE 0 TO IND-RAN-CF-INT-RID
           END-IF
           IF RAN-FL-COINC-DIP-CNTR-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-FL-COINC-DIP-CNTR
           ELSE
              MOVE 0 TO IND-RAN-FL-COINC-DIP-CNTR
           END-IF
           IF RAN-FL-COINC-INT-CNTR-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-FL-COINC-INT-CNTR
           ELSE
              MOVE 0 TO IND-RAN-FL-COINC-INT-CNTR
           END-IF
           IF RAN-DT-DECES-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-DT-DECES
           ELSE
              MOVE 0 TO IND-RAN-DT-DECES
           END-IF
           IF RAN-FL-FUMATORE-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-FL-FUMATORE
           ELSE
              MOVE 0 TO IND-RAN-FL-FUMATORE
           END-IF
           IF RAN-FL-LAV-DIP-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-FL-LAV-DIP
           ELSE
              MOVE 0 TO IND-RAN-FL-LAV-DIP
           END-IF
           IF RAN-TP-VARZ-PAGAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-TP-VARZ-PAGAT
           ELSE
              MOVE 0 TO IND-RAN-TP-VARZ-PAGAT
           END-IF
           IF RAN-COD-RID-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-COD-RID
           ELSE
              MOVE 0 TO IND-RAN-COD-RID
           END-IF
           IF RAN-TP-CAUS-RID-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-TP-CAUS-RID
           ELSE
              MOVE 0 TO IND-RAN-TP-CAUS-RID
           END-IF
           IF RAN-IND-MASSA-CORP-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-IND-MASSA-CORP
           ELSE
              MOVE 0 TO IND-RAN-IND-MASSA-CORP
           END-IF
           IF RAN-CAT-RSH-PROF-NULL = HIGH-VALUES
              MOVE -1 TO IND-RAN-CAT-RSH-PROF
           ELSE
              MOVE 0 TO IND-RAN-CAT-RSH-PROF
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : RAN-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE RAPP-ANA TO WS-BUFFER-TABLE.

           MOVE RAN-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO RAN-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO RAN-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO RAN-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO RAN-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO RAN-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO RAN-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO RAN-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE RAPP-ANA TO WS-BUFFER-TABLE.

           MOVE RAN-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO RAPP-ANA.

           MOVE WS-ID-MOVI-CRZ  TO RAN-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO RAN-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO RAN-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO RAN-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO RAN-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO RAN-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO RAN-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE RAN-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO RAN-DT-INI-EFF-DB
           MOVE RAN-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO RAN-DT-END-EFF-DB
           IF IND-RAN-DT-NASC = 0
               MOVE RAN-DT-NASC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO RAN-DT-NASC-DB
           END-IF
           IF IND-RAN-DT-DELIBERA-CDA = 0
               MOVE RAN-DT-DELIBERA-CDA TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO RAN-DT-DELIBERA-CDA-DB
           END-IF
           IF IND-RAN-DT-DECES = 0
               MOVE RAN-DT-DECES TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO RAN-DT-DECES-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE RAN-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO RAN-DT-INI-EFF
           MOVE RAN-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO RAN-DT-END-EFF
           IF IND-RAN-DT-NASC = 0
               MOVE RAN-DT-NASC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO RAN-DT-NASC
           END-IF
           IF IND-RAN-DT-DELIBERA-CDA = 0
               MOVE RAN-DT-DELIBERA-CDA-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO RAN-DT-DELIBERA-CDA
           END-IF
           IF IND-RAN-DT-DECES = 0
               MOVE RAN-DT-DECES-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO RAN-DT-DECES
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.


           MOVE LENGTH OF RAN-NOME-INT-RID
                       TO RAN-NOME-INT-RID-LEN
           MOVE LENGTH OF RAN-COGN-INT-RID
                       TO RAN-COGN-INT-RID-LEN
           MOVE LENGTH OF RAN-COGN-INT-TRATT
                       TO RAN-COGN-INT-TRATT-LEN
           MOVE LENGTH OF RAN-NOME-INT-TRATT
                       TO RAN-NOME-INT-TRATT-LEN.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

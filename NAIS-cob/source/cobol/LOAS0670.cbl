      *  ============================================================= *
      *                                                                *
      *        PORTAFOGLIO VITA ITALIA  VER 1.0                        *
      *                                                                *
      *  ============================================================= *
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LOAS0670.
       AUTHOR.             ATS.
       DATE-WRITTEN.       SETTEMBRE 2007.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *     PROGRAMMA ..... LOAS0670                                   *
      *     TIPOLOGIA...... OPERAZIONI AUTOMATICHE                     *
      *     PROCESSO....... ADEGUAMENTO PREMIO PRESTAZIONE             *
      *     FUNZIONE....... BATCH                                      *
      *     DESCRIZIONE.... GAP RICH090 COLLETTIVE                     *
      *     PAGINA WEB..... N.A.                                       *
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
      *
       DATA DIVISION.
      *
       WORKING-STORAGE SECTION.
      *
      *----------------------------------------------------------------*
      *       COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
      *
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IEAV9903.
           COPY IDSV0002.
      *
      *----------------------------------------------------------------*
      *     COPY DELLE TABELLE DB2 PER IL DISPATCHER DATI
      *----------------------------------------------------------------*
      *
      *  --> TITOLO CONTABILE
           COPY IDBVTIT1.

      *  --> Where condition ad hoc per lettura Titolo Contabile
           COPY LDBV3101.
      *  --> DETTAGLIO TITOLO CONTABILE
           COPY IDBVDTC1.

      *----------------------------------------------------------------*
      *  AREE TIPOLOGICHE OGGETTO
      *----------------------------------------------------------------*
           COPY LCCVXOG0.
      *---------------------------------------------------------------*
      * COPY ELE-MAX
      *---------------------------------------------------------------*
           COPY LCCVTGAZ.

      *----------------------------------------------------------------*
      *   INDICI
      *----------------------------------------------------------------*
      *
       01  IX-INDICI.
      *
           03 IX-TAB-TGA                   PIC S9(04) COMP.
      *
           03 IX-W670                      PIC S9(04) COMP.
      *
           03 IX-GRZ                       PIC S9(04) COMP.
      *
           03 IX-TAB-ERR                   PIC S9(04) COMP.
      *
      *----------------------------------------------------------------*
      *    AREE DI COMODO
      *----------------------------------------------------------------*
      *
      * --> Ricerca occorrenza Titolo Contabile
      *
       01  WK-TIT-TROVATO               PIC X.
           88 WK-TIT-SI                   VALUE 'S'.
           88 WK-TIT-NO                   VALUE 'N'.
      *
      * --> Ricerca occorrenza Garanzia
      *
       01  WK-GRZ-TROVATA               PIC X.
           88 WK-GRZ-SI                   VALUE 'S'.
           88 WK-GRZ-NO                   VALUE 'N'.
      *
      * -->  Flag fine fetch
      *
       01 WK-FINE-FETCH                 PIC X(01).
          88 WK-FINE-FETCH-SI             VALUE 'Y'.
          88 WK-FINE-FETCH-NO             VALUE 'N'.
      *
      * --> Area di appoggio per i messaggi di errore
      *
       01 WK-GESTIONE-MSG-ERR.
          03 WK-COD-ERR                 PIC X(06) VALUE ZEROES.
          03 WK-LABEL-ERR               PIC X(30) VALUE SPACES.
          03 WK-STRING                  PIC X(85) VALUE SPACES.
      *
       01 WK-APPO-DT-RICOR-SUCC         PIC 9(08) VALUE ZEROES.
      *
       01 WK-APPO-DT-NUM                PIC 9(08) VALUE ZEROES.
      *
       01 WK-APPO-DT.
          03 WK-APPO-DT-AA              PIC 9(04) VALUE ZEROES.
          03 WK-APPO-DT-MM              PIC 9(02) VALUE ZEROES.
          03 WK-APPO-DT-GG              PIC 9(02) VALUE ZEROES.
      *
       01 WMAX-TAB-TIT                  PIC 9(04) VALUE 300.
      *
      * --> Nome del programma Businesss Service
      *
       01 WK-PGM                        PIC X(08) VALUE 'LOAS0670'.
      *
      * --> Area di accept della data dalla variabile di sistema DATE
      *
       01 WK-CURRENT-DATE               PIC 9(08) VALUE ZEROES.
      *
      * --> Area di appoggio per gestione Management Fee
      *
      *
       01 WK-CTR-TIT                    PIC 9(01).
      *
VSTEF *--> Tipo periodo premio (Garanzia)
       01  WS-TP-PER-PREMIO                       PIC X(001).
           88 WS-PREMIO-UNICO                     VALUE 'U'.
           88 WS-PREMIO-RICORRENTE                VALUE 'R'.
           88 WS-PREMIO-ANNUALE                   VALUE 'A'.
      *
      *----------------------------------------------------------------*
      * DEFINIZIONE AREA DI APPOGGIO PER FORMATTAZIONE DATE
      *----------------------------------------------------------------*
      *
       01 WK-DATA-AMG.
          05 AAAA-SYS                   PIC X(004) VALUE ZEROES.
          05 MM-SYS                     PIC X(002) VALUE ZEROES.
          05 GG-SYS                     PIC X(002) VALUE ZEROES.
      *
       01 WK-ORA-HMS.
          05 HH-SYS                     PIC X(002) VALUE ZEROES.
          05 MI-SYS                     PIC X(002) VALUE ZEROES.
          05 SS-SYS                     PIC X(002) VALUE ZEROES.
          05 CS-SYS                     PIC X(002) VALUE ZEROES.
      *
       01 WK-DATA-SISTEMA.
          05 GG-SYS                     PIC X(002) VALUE SPACES.
          05 FILLER                     PIC X(001) VALUE '/'.
          05 MM-SYS                     PIC X(002) VALUE SPACES.
          05 FILLER                     PIC X(001) VALUE '/'.
          05 AAAA-SYS                   PIC X(004).
      *
       01 WK-ORA-SISTEMA.
          05 HH-SYS                     PIC X(002) VALUE SPACES.
          05 FILLER                     PIC X(001) VALUE '.'.
          05 MI-SYS                     PIC X(002) VALUE SPACES.
          05 FILLER                     PIC X(001) VALUE '.'.
          05 SS-SYS                     PIC X(002).
      *
      *----------------------------------------------------------------*
      *    AREA CALCOLO DIFFERENZA TRA DATE
      *----------------------------------------------------------------*
      *
      *  --> Area per gestione della differenza tra date
      *
       01 FORMATO                       PIC X(01) VALUE ZEROES.
       01 DATA-INFERIORE.
          05 AAAA-INF                   PIC 9(04) VALUE ZEROES.
          05 MM-INF                     PIC 9(02) VALUE ZEROES.
          05 GG-INF                     PIC 9(02) VALUE ZEROES.
       01 DATA-SUPERIORE.
          05 AAAA-SUP                   PIC 9(04) VALUE ZEROES.
          05 MM-SUP                     PIC 9(02) VALUE ZEROES.
          05 GG-SUP                     PIC 9(02) VALUE ZEROES.
       01 CODICE-RITORNO                PIC X(01) VALUE ZEROES.
       01 WK-COM-AAMM.
          05 WK-COM-AA                  PIC 9(04) VALUE ZEROES.
          05 WK-COM-MM                  PIC 9(02) VALUE ZEROES.
       01 DURATA.
          05 DUR-AAAA                   PIC 9(04) VALUE ZEROES.
          05 DUR-MM                     PIC 9(02) VALUE ZEROES.
          05 DUR-GG                     PIC 9(02) VALUE ZEROES.
      *
      *  --> Area per gestione resti operazioni
      *
       01 WK-OPERAZIONI.
          03 WK-APPO-RESTO              PIC S9(5)V9(5) VALUE ZEROES.
          03 WK-APPO-RESTO1             PIC S9(5)V9(5) VALUE ZEROES.
      *
      *  --> GESTIONE DATE
       01  LCCS0003                         PIC X(8) VALUE 'LCCS0003'.
      *
      *----------------------------------------------------------------*
      *     COPY DISPATCHER
      *----------------------------------------------------------------*
      *
       01  IDSV0012.
           COPY IDSV0012.
      *
       01  DISPATCHER-VARIABLES.
      *  COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *  COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *
       COPY IDSV0003.
      *
      *----------------------------------------------------------------*
      *    AREA IO SERVIZIO CALCOLI SULLE DATE
      *----------------------------------------------------------------*
      *
          COPY LCCC0003.
      *
       01 IN-RCODE                       PIC 9(2).
       01 WK-DATA-CALCOLATA              PIC 9(8) VALUE ZEROES.
      *
      *----------------------------------------------------------------*
      *  AREE TIPOLOGICHE DI PORTAFOGLIO
      *----------------------------------------------------------------*
          COPY LCCC0006.
VSTEF *----------------------------------------------------------------*
      *    COPY IDSV0015
      *----------------------------------------------------------------*
           COPY IDSV0015.
      *----------------------------------------------------------------*
      *        L I N K A G E   S E C T I O N
      *----------------------------------------------------------------*
      *
       LINKAGE SECTION.
      *
      *----------------------------------------------------------------*
      *   AREA-IDSV0001
      *----------------------------------------------------------------*
      *
       01  AREA-IDSV0001.
           COPY IDSV0001.
      *
       01 WCOM-AREA-STATI.
          COPY LCCC0001               REPLACING ==(SF)== BY ==WCOM==.

      *----------------------------------------------------------------*
      *  AREA DI PASSAGGIO DA-A MAIN
      *----------------------------------------------------------------*
      *
      * -- Area Parametro Movimento
       01 WPMO-AREA-PARAM-MOVI.
          04 WPMO-ELE-PARAM-MOV-MAX      PIC S9(04) COMP.
             COPY LCCVPMOA REPLACING   ==(SF)==  BY ==WPMO==.
             COPY LCCVPMO1               REPLACING ==(SF)== BY ==WPMO==.

      * -- Area polizza
       01 WPOL-AREA-POLIZZA.
          04 WPOL-ELE-POLI-MAX           PIC S9(04) COMP.
          04 WPOL-TAB-POL.
             COPY LCCVPOL1               REPLACING ==(SF)== BY ==WPOL==.

      * -- Area Adesione
       01 WADE-AREA-ADESIONE.
          04 WADE-ELE-ADES-MAX           PIC S9(04) COMP.
          04 WADE-TAB-ADE                OCCURS 1.
             COPY LCCVADE1               REPLACING ==(SF)== BY ==WADE==.

      * -- Area Garanzia
       01 WGRZ-AREA-GARANZIA.
          04 WGRZ-ELE-GAR-MAX            PIC S9(04) COMP.
             COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
             COPY LCCVGRZ1               REPLACING ==(SF)== BY ==WGRZ==.

      * -- Area Tranche Di Garanzia
       01 WTGA-AREA-TRANCHE.
          04 WTGA-ELE-TRAN-MAX           PIC S9(04) COMP.
             COPY LCCVTGAC REPLACING   ==(SF)==  BY ==WTGA==.
             COPY LCCVTGA1               REPLACING ==(SF)== BY ==WTGA==.
      *
       01 W670-AREA-LOAS0670.
          COPY LOAC0670                  REPLACING ==(SF)== BY ==W670==.
      *
      *  ====================  M A I N    L I N E  =================== *
      *
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                WPMO-AREA-PARAM-MOVI
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TRANCHE
                                W670-AREA-LOAS0670.

      *
           PERFORM S00000-OPERAZ-INIZIALI
              THRU S00000-OPERAZ-INIZIALI-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10000-ELABORAZIONE
                 THRU S10000-ELABORAZIONE-EX
      *
           END-IF.
      *
           PERFORM S90000-OPERAZ-FINALI
              THRU S90000-OPERAZ-FINALI-EX.
      *
           GOBACK.
      *
      * ============================================================== *
      * -->           O P E R Z I O N I   I N I Z I A L I          <-- *
      * ============================================================== *
      *
       S00000-OPERAZ-INIZIALI.
      *
           MOVE 'S00000-OPERAZ-INIZIALI'
             TO WK-LABEL-ERR.
      *
      * --> Inizializazione delle aree di working storage
      *
           PERFORM S00200-INIZIA-AREE-WS
              THRU S00200-INIZIA-AREE-WS-EX.
      *
       S00000-OPERAZ-INIZIALI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     INIZIALIZZAZIONE DELLE AREE DI WORKING STORAGE             *
      *----------------------------------------------------------------*
      *
       S00200-INIZIA-AREE-WS.
      *
           MOVE 'S00200-INIZIA-AREE-WS'
             TO WK-LABEL-ERR.
      *
      *  Inizializazione di tutti gli indici e le aree di WS
      *
           INITIALIZE IX-INDICI.
      *
           SET  IDSV0001-ESITO-OK
             TO TRUE.
      *
       S00200-INIZIA-AREE-WS-EX.
           EXIT.
      *
      * ============================================================== *
      * -->               E L A B O R A Z I O N E                  <-- *
      * ============================================================== *
      *
       S10000-ELABORAZIONE.
      *
           MOVE 'S10000-ELABORAZIONE'
             TO WK-LABEL-ERR.
      *

           IF IDSV0001-ESITO-OK
      *
MM            IF W670-NUM-MAX-ELE = ZEROES
      *
                 PERFORM S10400-CARICA-TIT-DB
                    THRU S10400-CARICA-TIT-DB-EX
      *
              END-IF
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10500-CTRL-TIT
                 THRU S10500-CTRL-TIT-EX
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10600-DETERMINA-TP-RIVAL
                 THRU S10600-DETERMINA-TP-RIVAL-EX
      *
           END-IF.
      *
       S10000-ELABORAZIONE-EX.
           EXIT.
      *
      *
      *----------------------------------------------------------------*
      *              GESTIONE DEL TIPO DI RIVALUTAZIONE                *
      *----------------------------------------------------------------*
      *
       S10600-DETERMINA-TP-RIVAL.
      *
           MOVE 'S10600-DETERMINA-TP-RIVAL'
             TO WK-LABEL-ERR.

      * --> Tipi Rivalutazione :
      *
      *     -> Piena
      *     -> Parziale
      *     -> Nulla
      *     -> Da Rieseguire
      *
           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
                OR IDSV0001-ESITO-KO
      *
               PERFORM VARYING IX-W670 FROM 1 BY 1
                  UNTIL IX-W670 > W670-NUM-MAX-ELE
      *
                    IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
                       W670-ID-TRCH-DI-GAR(IX-W670)
      *
                       PERFORM S10290-GESTIONE-TIT-CONT
                          THRU S10290-GESTIONE-TIT-CONT-EX
      *
                    END-IF
      *
               END-PERFORM
      *
           END-PERFORM.
      *
       S10600-DETERMINA-TP-RIVAL-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                     RICERCA TITOLI CONTABILI                   *
      *----------------------------------------------------------------*
      *
       S10220-RICERCA-TITOLO.
      *
           MOVE 'S10220-RICERCA-TITOLO'
             TO WK-LABEL-ERR.
      *
      * -->> Indicatore di fine fetch
      *
           SET WK-FINE-FETCH-NO
             TO TRUE.
      *
      * -->> Flag Ricerca Titolo Contabile Incassato
      *
           SET WK-TIT-NO
             TO TRUE.
      *
           MOVE 'IN'
             TO LDBV3101-TP-STA-TIT.
      *
           SET IDSI0011-FETCH-FIRST
             TO TRUE.
      *
           PERFORM S10230-IMPOSTA-TIT-CONT
              THRU S10230-IMPOSTA-TIT-CONT-EX
      *
           PERFORM S10240-LEGGI-TIT-CONT
              THRU S10240-LEGGI-TIT-CONT-EX
             UNTIL WK-FINE-FETCH-SI
                OR IDSV0001-ESITO-KO
      *
      * --> Non esistono Titoli Contabili con stato INCASSATO
      *
           IF IDSV0001-ESITO-OK
      *
              IF WK-TIT-NO
      *
                 SET WK-FINE-FETCH-NO
                   TO TRUE
      *
                 MOVE 'EM'
                   TO LDBV3101-TP-STA-TIT
      *
                 SET IDSI0011-FETCH-FIRST
                   TO TRUE
      *
                 PERFORM S10230-IMPOSTA-TIT-CONT
                    THRU S10230-IMPOSTA-TIT-CONT-EX
      *
                 PERFORM S10240-LEGGI-TIT-CONT
                    THRU S10240-LEGGI-TIT-CONT-EX
                   UNTIL WK-FINE-FETCH-SI
                      OR IDSV0001-ESITO-KO
      *
      * --> Non esistono Titoli Contabili con stato EMESSO
      *
                 IF IDSV0001-ESITO-OK
      *
                    IF WK-TIT-NO
      *
VSTEF *     SIR UNIPOL: Per le garanzie a premio unico,
      *     se non esistono titoli contabili allora comunque
  ..  *     la rivalutazione h piena (perchi significa che non vi
  ..  *     sono stati versamenti e valgono i titoli di emissione)

  ..                   PERFORM RICERCA-GAR
  ..                      THRU RICERCA-GAR-EX

  ..                   IF  WK-GRZ-SI
  ..                   AND (WS-PREMIO-UNICO OR
                            WS-PREMIO-RICORRENTE)
  ..                       CONTINUE
  ..                   ELSE

  ..                      MOVE 'NON ESISTONO TITOLI CONTABILI'
  ..                        TO WK-STRING
  ..                      MOVE '001114'
  ..                        TO WK-COD-ERR
  ..                      PERFORM GESTIONE-ERR-STD
  ..                         THRU GESTIONE-ERR-STD-EX
VSTEF                  END-IF
      *
                    END-IF
      *
              END-IF
      *
           END-IF.
      *
       S10220-RICERCA-TITOLO-EX.
             EXIT.
      *
VSTEF *----------------------------------------------------------------*
  ..  *    RICERCA GARANZIA
  ..  *----------------------------------------------------------------*
  ..   RICERCA-GAR.

  ..       SET WK-GRZ-NO       TO TRUE

  ..       PERFORM VARYING IX-GRZ FROM 1 BY 1
  ..                 UNTIL IX-GRZ > WGRZ-ELE-GAR-MAX
  ..                    OR WK-GRZ-SI

  ..          IF WGRZ-ID-GAR(IX-GRZ) = WTGA-ID-GAR(IX-TAB-TGA)

  ..             SET WK-GRZ-SI               TO TRUE

  ..             MOVE WGRZ-TP-PER-PRE(IX-GRZ)
  ..               TO WS-TP-PER-PREMIO

  ..          END-IF

  ..       END-PERFORM.

  ..   RICERCA-GAR-EX.
VSTEF        EXIT.
      *
      *----------------------------------------------------------------*
      *     VALORIZZAZIONI PER LA LETTURA CON WHERE CONDITION AD HOC   *
      *----------------------------------------------------------------*
      *
       S10230-IMPOSTA-TIT-CONT.
      *
           MOVE 'S10230-IMPOSTA-TIT-CONT'
             TO WK-LABEL-ERR.
      *
      * --> Vengono Cercati I Titoli Con Effetto Compreso Tra L'Ultima
      * --> Ricorrenza Elaborata e L'Effetto di Elaborazione
      *
           IF WPMO-DT-RICOR-PREC-NULL(1) = HIGH-VALUES
      *
              MOVE WTGA-DT-DECOR(IX-TAB-TGA)
                TO LDBV3101-DT-DA
      *
           ELSE
      *
              MOVE WPMO-DT-RICOR-PREC(1)
                TO LDBV3101-DT-DA
      *
           END-IF.
      *
           MOVE WPMO-DT-RICOR-SUCC(1)
             TO LDBV3101-DT-A.
      *
           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
             TO LDBV3101-ID-OGG.
           MOVE 'TG'
             TO LDBV3101-TP-OGG.
           MOVE 'PR'
             TO LDBV3101-TP-TIT.
      *
      *  --> La data effetto viene sempre valorizzata
      *
VSTEF *    MOVE ZERO
      *      TO IDSI0011-DATA-INIZIO-EFFETTO
      *         IDSI0011-DATA-FINE-EFFETTO
      *         IDSI0011-DATA-COMPETENZA.
      *
VSTEF *    SIR CQPrd00020801: LA DATA INCASSO NON VIENE VALORIZZATA
      *    NEL FILE DI OUTPUT

VSTEF      MOVE WS-DT-INFINITO-1-N
             TO IDSI0011-DATA-INIZIO-EFFETTO
VSTEF      MOVE WS-TS-INFINITO-1-N
             TO IDSI0011-DATA-COMPETENZA
VSTEF      MOVE ZEROES
             TO IDSI0011-DATA-FINE-EFFETTO
      *
      *  --> Nome tabella fisica db
      *
           MOVE 'LDBS3100'
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE SPACES
             TO IDSI0011-BUFFER-DATI.
           MOVE LDBV3101
             TO IDSI0011-BUFFER-WHERE-COND.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-WHERE-CONDITION
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSI0011-TRATT-DEFAULT
             TO TRUE.
      *
      *  --> Inizializzazione Return Code
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S10230-IMPOSTA-TIT-CONT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *            LETTURA DELLE TABELLA TRANCHE DI GARANZIA           *
      *----------------------------------------------------------------*
      *
       S10240-LEGGI-TIT-CONT.
      *
           MOVE 'S10240-LEGGI-TIT-CONT'
             TO WK-LABEL-ERR.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata, fine occorrenze fetch
      *
                       SET WK-FINE-FETCH-SI
                         TO TRUE
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
      *  --> Operazione eseguita correttamente
      *
                       SET WK-TIT-SI
                         TO TRUE
      *
                       SET IDSI0011-FETCH-NEXT
                         TO TRUE
      *
                       MOVE W670-NUM-MAX-ELE
                         TO IX-W670
      *
                       ADD 1
                         TO IX-W670
      *
                       MOVE IX-W670
                         TO W670-NUM-MAX-ELE
      *
                       IF IX-W670 > WK-TGA-MAX-C
      *
                          MOVE 'OVERFLOW CARICAMENTO TITOLI CONTABILI'
                            TO WK-STRING
                          MOVE '005059'
                            TO WK-COD-ERR
                          PERFORM GESTIONE-ERR-STD
                             THRU GESTIONE-ERR-STD-EX
      *
                       ELSE
      *
                          MOVE IDSO0011-BUFFER-DATI
                            TO TIT-CONT
      *
                          PERFORM S10250-CARICA-TAB-W670
                             THRU S10250-CARICA-TAB-W670-EX
      *
                       END-IF
      *
                  WHEN OTHER
      *
      *  --> Errore di accesso al db
                       MOVE 'ERRORE LETTURA TITOLO CONTABILE'
                         TO WK-STRING
                       MOVE '005016'
                         TO WK-COD-ERR
                       PERFORM GESTIONE-ERR-STD
                          THRU GESTIONE-ERR-STD-EX
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore dispatcher
      *
              MOVE 'ERRORE DISPATCHER LETTURA TITOLO CONTABILE'
                TO WK-STRING
              MOVE '005016'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S10240-LEGGI-TIT-CONT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  CARICAMENTO TABELLA WS
      *----------------------------------------------------------------*
      *
       S10250-CARICA-TAB-W670.
      *
           MOVE 'S10250-CARICA-TAB-W670'
             TO WK-LABEL-ERR.
      *
           MOVE TIT-ID-TIT-CONT
             TO W670-ID-TIT-CONT(IX-W670).
      *
           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
             TO W670-ID-TRCH-DI-GAR(IX-W670).
      *
           IF TIT-DT-VLT-NULL = HIGH-VALUE OR LOW-VALUE
      *
              MOVE TIT-DT-VLT-NULL
                TO W670-DT-VLT-NULL(IX-W670)
      *
           ELSE
      *
              MOVE TIT-DT-VLT
                TO W670-DT-VLT(IX-W670)
      *
           END-IF.
      *
           MOVE TIT-DT-INI-COP
             TO W670-DT-INI-EFF(IX-W670).
      *
           MOVE TIT-TP-STAT-TIT
             TO W670-TP-STAT-TIT(IX-W670).
      *
       S10250-CARICA-TAB-W670-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *          DETERMINA IL TIPO DI RIVALUTAZIONE DA ESEGUIRE
      *  PER I TITOLI INCASSATI : PIENA , PARZIALE , NULLL
      *----------------------------------------------------------------*
      *
       S10290-GESTIONE-TIT-CONT.
      *
           MOVE 'S10290-GESTIONE-TIT-CONT'
             TO WK-LABEL-ERR.
      *
           IF IDSV0001-ESITO-OK
      *
              IF W670-DT-VLT-NULL(IX-W670) = HIGH-VALUES  OR
                 W670-DT-VLT(IX-W670)      = ZEROES
      *
                  MOVE 'TIT.CONT. NON INCASSATO, RIESEGUIRE RIVAL'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
      *
              ELSE
      *
                 PERFORM S10300-RIV-PIENA-PARZ-NULLA
                    THRU S10300-RIV-PIENA-PARZ-NULLA-EX
      *
              END-IF
      *
           END-IF.
      *
       S10290-GESTIONE-TIT-CONT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *          LA RIVALUTAZIONE E' PIENA PARZIALE O NULLA            *
      *----------------------------------------------------------------*
      *
       S10300-RIV-PIENA-PARZ-NULLA.
      *
           MOVE 'S10300-RIV-PIENA-PARZ-NULLA'
             TO WK-LABEL-ERR.
      *
           PERFORM S10310-LEGGE-GG-RIT-PAG
              THRU S10310-LEGGE-GG-RIT-PAG-EX.

           IF IDSV0001-ESITO-OK
      *

              SET WTGA-ST-MOD(IX-TAB-TGA) TO TRUE
              MOVE 'CO'
                TO WTGA-TP-RIVAL(IX-TAB-TGA)

              IF DTC-NUM-GG-RITARDO-PAG-NULL = HIGH-VALUES
                 MOVE ZERO TO DTC-NUM-GG-RITARDO-PAG
              END-IF

              IF DTC-NUM-GG-RITARDO-PAG > ZERO
      *
                 IF W670-DT-VLT(IX-W670) <
                    WPMO-DT-RICOR-SUCC(1)
      *
                    MOVE 'PA'
                      TO WTGA-TP-RIVAL(IX-TAB-TGA)
      *
                 ELSE
      *
                    MOVE 'NU'
                      TO WTGA-TP-RIVAL(IX-TAB-TGA)
      *
                 END-IF
      *
              END-IF
      *
           END-IF.
      *
       S10300-RIV-PIENA-PARZ-NULLA-EX.
           EXIT.
      *

       S10310-LEGGE-GG-RIT-PAG.

           MOVE 'S10310-LEGGE-GG-RIT-PAG'
                TO WK-LABEL-ERR.

           SET IDSI0011-SELECT       TO TRUE.

           MOVE ZERO                 TO IDSI0011-DATA-INIZIO-EFFETTO
                                        IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                 TO IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-DEFAULT  TO TRUE.

           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) TO DTC-ID-OGG.

           SET TRANCHE                 TO TRUE.
           MOVE WS-TP-OGG              TO DTC-TP-OGG.

           SET IDSI0011-ID-OGGETTO     TO TRUE.

           MOVE 'DETT-TIT-CONT'        TO IDSI0011-CODICE-STR-DATO.
           MOVE DETT-TIT-CONT          TO IDSI0011-BUFFER-DATI.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSV0001-ESITO-OK
              IF IDSO0011-SUCCESSFUL-RC
                 EVALUATE TRUE
                        WHEN IDSO0011-NOT-FOUND
      *-->    NESSUN DATO ESTRATTO DALLA TABELLA

                             MOVE 'IDBSDTC0'
                               TO IEAI9901-COD-SERVIZIO-BE
                             MOVE 'S11000-LEGGI-DETT-TIT-CONT'
                               TO IEAI9901-LABEL-ERR
                             MOVE '005166'
                               TO IEAI9901-COD-ERRORE
                             MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                             STRING IDSI0011-CODICE-STR-DATO ';'
                                    IDSO0011-RETURN-CODE     ';'
                                    IDSO0011-SQLCODE
                                    DELIMITED BY SIZE
                                    INTO IEAI9901-PARAMETRI-ERR
                             END-STRING
                             PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                THRU EX-S0300

                        WHEN IDSO0011-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                             MOVE IDSO0011-BUFFER-DATI TO DETT-TIT-CONT

                        WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                             MOVE 'IDBSDTC0'
                               TO IEAI9901-COD-SERVIZIO-BE
                             MOVE 'S11000-LEGGI-DETT-TIT-CONT'
                               TO IEAI9901-LABEL-ERR
                             MOVE '005166'
                               TO IEAI9901-COD-ERRORE
                             MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                             STRING IDSI0011-CODICE-STR-DATO ';'
                                    IDSO0011-RETURN-CODE     ';'
                                    IDSO0011-SQLCODE
                                    DELIMITED BY SIZE
                                    INTO IEAI9901-PARAMETRI-ERR
                             END-STRING
                             PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                THRU EX-S0300
                 END-EVALUATE
               ELSE
      *--> GESTIRE ERRORE
                    MOVE 'IDBSDTC0'
                      TO IEAI9901-COD-SERVIZIO-BE
                    MOVE 'S11000-LEGGI-DETT-TIT-CONT'
                      TO IEAI9901-LABEL-ERR
                    MOVE '005166'
                      TO IEAI9901-COD-ERRORE
                    MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                    STRING IDSI0011-CODICE-STR-DATO ';'
                           IDSO0011-RETURN-CODE     ';'
                           IDSO0011-SQLCODE
                           DELIMITED BY SIZE
                           INTO IEAI9901-PARAMETRI-ERR
                    END-STRING
                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
                       THRU EX-S0300
               END-IF
            ELSE
      *--> GESTIRE ERRORE
                 MOVE 'IDBSDTC0'
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S11000-LEGGI-DETT-TIT-CONT'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005166'
                   TO IEAI9901-COD-ERRORE
                 MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                 STRING IDSI0011-CODICE-STR-DATO ';'
                        IDSO0011-RETURN-CODE     ';'
                        IDSO0011-SQLCODE
                        DELIMITED BY SIZE
                        INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
            END-IF.

       S10310-LEGGE-GG-RIT-PAG-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CARICAMENTO TITOLO CONTABILE DA DB
      *----------------------------------------------------------------*
      *
       S10400-CARICA-TIT-DB.
      *
           MOVE 'S10400-CARICA-TIT-DB'
             TO WK-LABEL-ERR.
      *
           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
                OR IDSV0001-ESITO-KO
      *
                  PERFORM S10220-RICERCA-TITOLO
                     THRU S10220-RICERCA-TITOLO-EX
      *
           END-PERFORM.
      *
       S10400-CARICA-TIT-DB-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CONTROLLO CHE PER OGNI TRANCHE VI SIA UN SOLO TITOLO CONTABILE
      *----------------------------------------------------------------*
      *
       S10500-CTRL-TIT.
      *
           MOVE 'S10500-CTRL-TIT'
             TO WK-LABEL-ERR.
      *
      * --> Viene gestito un errore bloccante se esistono
      * --> piy Titoli Cotabili per una Tranche
      *
           PERFORM S10510-CTRL-STATO-TIT
              THRU S10510-CTRL-STATO-TIT-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10520-CTRL-UNI-TIT
                 THRU S10520-CTRL-UNI-TIT-EX
      *
           END-IF.
      *
       S10500-CTRL-TIT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CONTROLLI SULLO STATO DEL TITOLO
      *----------------------------------------------------------------*
      *
       S10510-CTRL-STATO-TIT.
      *
           MOVE 'S10510-CTRL-STATO-TIT'
             TO WK-LABEL-ERR.
      *
           PERFORM VARYING IX-W670 FROM 1 BY 1
             UNTIL IX-W670 > W670-NUM-MAX-ELE
                OR IDSV0001-ESITO-KO
      *
                 IF W670-TP-STAT-TIT(IX-W670) = 'EM'
      *
                    MOVE 'TIT.CONT. NON INCASSATO, RIESEGUIRE RIVAL'
                      TO WK-STRING
                    MOVE '001114'
                      TO WK-COD-ERR
                    PERFORM GESTIONE-ERR-STD
                       THRU GESTIONE-ERR-STD-EX
      *
                 END-IF
      *
           END-PERFORM.
      *
       S10510-CTRL-STATO-TIT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * PER UNA TRANCHE NON CI POSSONO ESSERE PIU' TITOLI
      *----------------------------------------------------------------*
      *
       S10520-CTRL-UNI-TIT.
      *
           MOVE 'S10520-CTRL-UNI-TIT'
             TO WK-LABEL-ERR.
      *
           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
                OR IDSV0001-ESITO-KO
      *
               MOVE ZEROES
                 TO WK-CTR-TIT
      *
               PERFORM VARYING IX-W670 FROM 1 BY 1
                 UNTIL IX-W670 > W670-NUM-MAX-ELE
                    OR WK-CTR-TIT > 1
                    OR IDSV0001-ESITO-KO
      *
                     IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
                        W670-ID-TRCH-DI-GAR(IX-W670)
      *
                        ADD 1
                          TO WK-CTR-TIT
      *
                     END-IF
      *
               END-PERFORM
      *
               IF WK-CTR-TIT > 1
      *
                  MOVE 'ESISTONO PIU TIT.CONT. PER UNA TRANCHE'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
      *
               END-IF
      *
           END-PERFORM.
      *
       S10520-CTRL-UNI-TIT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                    CALL AL MODULO LCCS0003                     *
      *----------------------------------------------------------------*
      *
       S10850-CHIAMA-LCCS0003.
      *
           MOVE 'S10850-CHIAMA-LCCS0003'
             TO WK-LABEL-ERR.
      *
           CALL LCCS0003         USING IO-A2K-LCCC0003
                                       IN-RCODE
           ON EXCEPTION
      *
                 MOVE 'LCCS0003'
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'ERRORE PROGRAMMA LCCS0003'
                   TO CALL-DESC
                 MOVE 'S10850-CHIAMA-LCCS0003'
                   TO WK-LABEL-ERR
      *
                 PERFORM GESTIONE-ERR-SIST
                    THRU GESTIONE-ERR-SIST-EX
      *
           END-CALL.

           IF IN-RCODE  = '00'
              MOVE A2K-OUAMG
                TO A2K-INAMG
           ELSE
              MOVE 'ERRORE CALCOLO DATA PROX RIC'
                TO WK-STRING
              MOVE '001114'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
           END-IF.
      *
       S10850-CHIAMA-LCCS0003-EX.
           EXIT.
      *
      * ============================================================== *
      * -->           O P E R Z I O N I   F I N A L I              <-- *
      * ============================================================== *
      *
       S90000-OPERAZ-FINALI.
           MOVE 'S90000-OPERAZ-FINALI'  TO WK-LABEL-ERR.
      *
           CONTINUE.
      *
       S90000-OPERAZ-FINALI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                GESTIONE STANDARD DELL'ERRORE                   *
      *----------------------------------------------------------------*
      *
       GESTIONE-ERR-STD.
      *
           MOVE WK-PGM
             TO IEAI9901-COD-SERVIZIO-BE.
           MOVE WK-LABEL-ERR
             TO IEAI9901-LABEL-ERR.
           MOVE WK-COD-ERR
             TO IEAI9901-COD-ERRORE.
           STRING WK-STRING ';'
                  IDSO0011-RETURN-CODE ';'
                  IDSO0011-SQLCODE
                  DELIMITED BY SIZE
                  INTO IEAI9901-PARAMETRI-ERR
           END-STRING.
      *
           PERFORM S0300-RICERCA-GRAVITA-ERRORE
              THRU EX-S0300.
      *
       GESTIONE-ERR-STD-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *           GESTIONE STANDARD DELL'ERRORE DI SISTEMA             *
      *----------------------------------------------------------------*
      *
       GESTIONE-ERR-SIST.
      *
           MOVE WK-LABEL-ERR
             TO IEAI9901-LABEL-ERR
      *
           PERFORM S0290-ERRORE-DI-SISTEMA
              THRU EX-S0290.
      *
       GESTIONE-ERR-SIST-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     ROUTINES CALL DISPATCHER                                   *
      *----------------------------------------------------------------*
      *
           COPY IDSP0012.
      *
      *----------------------------------------------------------------*
      *  ROUTINES GESTIONE ERRORI                                      *
      *----------------------------------------------------------------*
      *
           COPY IERP9901.
           COPY IERP9902.
           COPY IERP9903.
      *

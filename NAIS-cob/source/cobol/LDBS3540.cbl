       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LDBS3540.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN   AGOSTO 2008.
       DATE-COMPILED.
      * --------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : SERVIZIO ESTRAZIONE OCCORRENZE ALPO         *
      *                   DA TABELLA PARAMETRO MOVIMENTO.             *
      * FUNZIONE        : ADEGUAMENTO PREMIO PRESTAZIONE              *
      * --------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *
       77 DESCRIZ-ERR-DB2         PIC X(300)  VALUE SPACES.
       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.
      *
       01 WS-TIMESTAMP            PIC X(18).
       01 WS-TIMESTAMP-NUM REDEFINES WS-TIMESTAMP PIC 9(18).
      *
           EXEC SQL INCLUDE IDSV0010 END-EXEC.
      *
      * ---------------------------------------------------------------*
      *  AREA TABELLE DB2                                              *
      * ---------------------------------------------------------------*
      *
      * TABELLA PARAMETRO MOVIMENTO.
           EXEC SQL INCLUDE IDBDPMO0 END-EXEC.
           EXEC SQL INCLUDE IDBVPMO2 END-EXEC.
           EXEC SQL INCLUDE IDBVPMO3 END-EXEC.
      *
           EXEC SQL INCLUDE SQLCA    END-EXEC.
      *
       LINKAGE SECTION.
      *
      * ---------------------------------------------------------------*
      *                  CAMPI DI ESITO, AREE DI SERVIZIO              *
      * ---------------------------------------------------------------*
      *
            EXEC SQL INCLUDE IDSV0003 END-EXEC.
      *
      * ---------------------------------------------------------------*
      *                         AREA STATI                             *
      * ---------------------------------------------------------------*
      *
            EXEC SQL INCLUDE IABV0002 END-EXEC.
      *
      * ---------------------------------------------------------------*
      * AREA DATI    ( BLOB da 1M)
      * ---------------------------------------------------------------*
      *
      *
            EXEC SQL INCLUDE IDBVPMO1 END-EXEC.
      *
       PROCEDURE DIVISION USING IDSV0003
                                IABV0002
                                PARAM-MOVI.
      *
           PERFORM A000-INIZIO      THRU A000-EX.

           PERFORM A300-ELABORA     THRU A300-EX

           PERFORM A350-CTRL-COMMIT THRU A350-EX.

           PERFORM A400-FINE        THRU A400-EX.
      *
           GOBACK.
      *
      * ****************************************************************
      *                         OPERAZIONI INIZIALI                    *
      * ****************************************************************
       A000-INIZIO.
      *
           MOVE 'LDBS3540'              TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'PARAM-MOVI'            TO IDSV0003-NOME-TABELLA.
           MOVE '00'                    TO IDSV0003-RETURN-CODE.
           MOVE ZEROES                  TO IDSV0003-SQLCODE
                                           IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                  TO IDSV0003-DESCRIZ-ERR-DB2
                                           IDSV0003-KEY-TABELLA.
           MOVE ZEROES                  TO WS-TIMESTAMP-NUM.

           PERFORM A001-TRATTA-DATE-TIMESTAMP
                                        THRU A001-EX.
      *
       A000-EX.
           EXIT.
      * ****************************************************************
      * CONTROLLO RETURN CODE
      * ****************************************************************
      *
       A100-CHECK-RETURN-CODE.
      *
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE
                TO IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2
                TO IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                       CONTINUE
                  WHEN +100
                     IF IDSV0003-SELECT        OR
                        IDSV0003-FETCH-FIRST   OR
                        IDSV0003-FETCH-NEXT
                         CONTINUE
                     ELSE
                        SET IDSV0003-SQL-ERROR
                         TO TRUE
                     END-IF
                  WHEN OTHER
                     SET IDSV0003-SQL-ERROR
                       TO TRUE
              END-EVALUATE

           END-IF.
      *
       A100-EX.
           EXIT.
      *
      * ****************************************************************
      * ELABORAZIONE PER ESTRARRE LE OCCORRENZE DA ELABORARE
      * ****************************************************************
      *
       A300-ELABORA.
      *
           EVALUATE TRUE

              WHEN IDSV0003-WHERE-CONDITION-01
                   PERFORM SC01-SELECTION-CURSOR-01 THRU SC01-EX
              WHEN IDSV0003-WHERE-CONDITION-02
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
              WHEN IDSV0003-WHERE-CONDITION-03
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
              WHEN IDSV0003-WHERE-CONDITION-04
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
              WHEN IDSV0003-WHERE-CONDITION-05
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
              WHEN IDSV0003-WHERE-CONDITION-06
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
              WHEN IDSV0003-WHERE-CONDITION-07
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
              WHEN IDSV0003-WHERE-CONDITION-08
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
              WHEN IDSV0003-WHERE-CONDITION-09
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
              WHEN IDSV0003-WHERE-CONDITION-10
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE

           END-EVALUATE.
      *
       A300-EX.
           EXIT.
      *
      * ****************************************************************
      * DICHIARAZIONE CURSORE PER ESTRAZIONE DATI X ALPO
      * ****************************************************************
      *
       A305-DECLARE-CURSOR-SC01.
      *
           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                DECLARE CUR-PMO CURSOR WITH HOLD FOR
              SELECT
                ID_PARAM_MOVI
                ,ID_OGG
                ,TP_OGG
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_MOVI
                ,FRQ_MOVI
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,DT_RICOR_PREC
                ,DT_RICOR_SUCC
                ,PC_INTR_FRAZ
                ,IMP_BNS_DA_SCO_TOT
                ,IMP_BNS_DA_SCO
                ,PC_ANTIC_BNS
                ,TP_RINN_COLL
                ,TP_RIVAL_PRE
                ,TP_RIVAL_PRSTZ
                ,FL_EVID_RIVAL
                ,ULT_PC_PERD
                ,TOT_AA_GIA_PROR
                ,TP_OPZ
                ,AA_REN_CER
                ,PC_REVRSB
                ,IMP_RISC_PARZ_PRGT
                ,IMP_LRD_DI_RAT
                ,IB_OGG
                ,COS_ONER
                ,SPE_PC
                ,FL_ATTIV_GAR
                ,CAMBIO_VER_PROD
                ,MM_DIFF
                ,IMP_RAT_MANFEE
                ,DT_ULT_EROG_MANFEE
                ,TP_OGG_RIVAL
                ,SOM_ASSTA_GARAC
                ,PC_APPLZ_OPZ
                ,ID_ADES
                ,ID_POLI
                ,TP_FRM_ASSVA
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TP_ESTR_CNT
                ,COD_RAMO
                ,GEN_DA_SIN
                ,COD_TARI
                ,NUM_RAT_PAG_PRE
                ,PC_SERV_VAL
                ,ETA_AA_SOGL_BNFICR
              FROM PARAM_MOVI
              WHERE COD_COMP_ANIA   = :PMO-COD-COMP-ANIA
                AND TP_MOVI         = :PMO-TP-MOVI
                AND TP_OGG          = :PMO-TP-OGG
                AND TP_FRM_ASSVA    = :PMO-TP-FRM-ASSVA
                AND ID_POLI         = :PMO-ID-POLI
                AND ((ID_ADES       = :PMO-ID-ADES
                       AND
                      TP_FRM_ASSVA  = 'CO')
                      OR
                      TP_FRM_ASSVA  = 'IN')
                AND (
                      DT_RICOR_SUCC = :PMO-DT-RICOR-SUCC-DB
                     OR
                      DT_RICOR_SUCC + MM_DIFF MONTH
                                    = :PMO-DT-RICOR-SUCC-DB
                    )

                AND DT_INI_EFF     <= :PMO-DT-INI-EFF-DB
                AND DT_END_EFF     >  :PMO-DT-END-EFF-DB
                AND DS_TS_INI_CPTZ <= :PMO-DS-TS-INI-CPTZ
                AND DS_TS_END_CPTZ =  :PMO-DS-TS-END-CPTZ

                AND DS_STATO_ELAB IN (
                                     :IABV0002-STATE-01,
                                     :IABV0002-STATE-02,
                                     :IABV0002-STATE-03,
                                     :IABV0002-STATE-04,
                                     :IABV0002-STATE-05,
                                     :IABV0002-STATE-06,
                                     :IABV0002-STATE-07,
                                     :IABV0002-STATE-08,
                                     :IABV0002-STATE-09,
                                     :IABV0002-STATE-10
                                     )
           END-EXEC.
      *
       A305-SC01-EX.
           EXIT.

      * ****************************************************************
      * UPDATE TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
       A320-UPDATE-SC01.
      *
           PERFORM A330-UPDATE-PK-SC01          THRU A330-SC01-EX.
      *
       A320-SC01-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE PARAMETRO MOVIMENTO PER CHIAVE PRIMARIA
      * ****************************************************************
      *
       A330-UPDATE-PK-SC01.
      *
           PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
      *
           EXEC SQL
                UPDATE PARAM_MOVI SET
                   DS_UTENTE        = :IDSV0003-USER-NAME
                  ,DS_STATO_ELAB    = :IABV0002-STATE-CURRENT

              WHERE DS_RIGA         = :PMO-DS-RIGA

           END-EXEC.
      *
           PERFORM A100-CHECK-RETURN-CODE          THRU A100-EX.
      *
       A330-SC01-EX.
           EXIT.
      *
      * ****************************************************************
      * CONTROLLA COMMIT
      * ****************************************************************
       A350-CTRL-COMMIT.

           CONTINUE.

       A350-EX.
           EXIT.
      *
      * ****************************************************************
      * APERTURA CURSORE
      * ****************************************************************
      *
       A360-OPEN-CURSOR-SC01.
      *
           PERFORM A305-DECLARE-CURSOR-SC01      THRU A305-SC01-EX.
      *
           EXEC SQL
                OPEN CUR-PMO
           END-EXEC.
      *
           PERFORM A100-CHECK-RETURN-CODE        THRU A100-EX.
      *
       A360-SC01-EX.
           EXIT.
      *
      * ****************************************************************
      * CHIUSURA CURSORE
      * ****************************************************************
      *
       A370-CLOSE-CURSOR-SC01.
      *
           EXEC SQL
                CLOSE CUR-PMO
           END-EXEC.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A370-SC01-EX.
           EXIT.
      *
      * ****************************************************************
      * PRIMA FETCH SU TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
       A380-FETCH-FIRST-SC01.
      *
           PERFORM A360-OPEN-CURSOR-SC01         THRU A360-SC01-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-SC01       THRU A390-SC01-EX
           END-IF.
      *
       A380-SC01-EX.
           EXIT.
      *
      * ****************************************************************
      * FETCH SUCCESSIVE SULLA TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
       A390-FETCH-NEXT-SC01.

           EXEC SQL
                FETCH CUR-PMO
           INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
               ,:PMO-COD-RAMO
                :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR
           END-EXEC.
      *
           PERFORM A100-CHECK-RETURN-CODE        THRU A100-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL      THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N       THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC01   THRU A370-SC01-EX

                IF IDSV0003-SUCCESSFUL-SQL

                    SET IDSV0003-NOT-FOUND       TO TRUE

                END-IF
             END-IF
           END-IF.
      *
       A390-SC01-EX.
           EXIT.
      *
      * ****************************************************************
      * OPERAZIONI FINALI
      * ****************************************************************
      *
       A400-FINE.
      *
           CONTINUE.
      *
       A400-EX.
           EXIT.
      ******************************************************************
       SC01-SELECTION-CURSOR-01.

           EVALUATE TRUE

              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC01  THRU A360-SC01-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC01 THRU A370-SC01-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC01  THRU A380-SC01-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC01   THRU A390-SC01-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC01       THRU A320-SC01-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER
                  TO TRUE

           END-EVALUATE.

       SC01-EX.
           EXIT.
      *
      * ****************************************************************
      * SETTAGGIO CAMPI NULL DELLA TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
       Z100-SET-COLONNE-NULL.
      *
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-PMO-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO PMO-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-PMO-TP-MOVI = -1
              MOVE HIGH-VALUES TO PMO-TP-MOVI-NULL
           END-IF
           IF IND-PMO-FRQ-MOVI = -1
              MOVE HIGH-VALUES TO PMO-FRQ-MOVI-NULL
           END-IF
           IF IND-PMO-DUR-AA = -1
              MOVE HIGH-VALUES TO PMO-DUR-AA-NULL
           END-IF
           IF IND-PMO-DUR-MM = -1
              MOVE HIGH-VALUES TO PMO-DUR-MM-NULL
           END-IF
           IF IND-PMO-DUR-GG = -1
              MOVE HIGH-VALUES TO PMO-DUR-GG-NULL
           END-IF
           IF IND-PMO-DT-RICOR-PREC = -1
              MOVE HIGH-VALUES TO PMO-DT-RICOR-PREC-NULL
           END-IF
           IF IND-PMO-DT-RICOR-SUCC = -1
              MOVE HIGH-VALUES TO PMO-DT-RICOR-SUCC-NULL
           END-IF
           IF IND-PMO-PC-INTR-FRAZ = -1
              MOVE HIGH-VALUES TO PMO-PC-INTR-FRAZ-NULL
           END-IF
           IF IND-PMO-IMP-BNS-DA-SCO-TOT = -1
              MOVE HIGH-VALUES TO PMO-IMP-BNS-DA-SCO-TOT-NULL
           END-IF
           IF IND-PMO-IMP-BNS-DA-SCO = -1
              MOVE HIGH-VALUES TO PMO-IMP-BNS-DA-SCO-NULL
           END-IF
           IF IND-PMO-PC-ANTIC-BNS = -1
              MOVE HIGH-VALUES TO PMO-PC-ANTIC-BNS-NULL
           END-IF
           IF IND-PMO-TP-RINN-COLL = -1
              MOVE HIGH-VALUES TO PMO-TP-RINN-COLL-NULL
           END-IF
           IF IND-PMO-TP-RIVAL-PRE = -1
              MOVE HIGH-VALUES TO PMO-TP-RIVAL-PRE-NULL
           END-IF
           IF IND-PMO-TP-RIVAL-PRSTZ = -1
              MOVE HIGH-VALUES TO PMO-TP-RIVAL-PRSTZ-NULL
           END-IF
           IF IND-PMO-FL-EVID-RIVAL = -1
              MOVE HIGH-VALUES TO PMO-FL-EVID-RIVAL-NULL
           END-IF
           IF IND-PMO-ULT-PC-PERD = -1
              MOVE HIGH-VALUES TO PMO-ULT-PC-PERD-NULL
           END-IF
           IF IND-PMO-TOT-AA-GIA-PROR = -1
              MOVE HIGH-VALUES TO PMO-TOT-AA-GIA-PROR-NULL
           END-IF
           IF IND-PMO-TP-OPZ = -1
              MOVE HIGH-VALUES TO PMO-TP-OPZ-NULL
           END-IF
           IF IND-PMO-AA-REN-CER = -1
              MOVE HIGH-VALUES TO PMO-AA-REN-CER-NULL
           END-IF
           IF IND-PMO-PC-REVRSB = -1
              MOVE HIGH-VALUES TO PMO-PC-REVRSB-NULL
           END-IF
           IF IND-PMO-IMP-RISC-PARZ-PRGT = -1
              MOVE HIGH-VALUES TO PMO-IMP-RISC-PARZ-PRGT-NULL
           END-IF
           IF IND-PMO-IMP-LRD-DI-RAT = -1
              MOVE HIGH-VALUES TO PMO-IMP-LRD-DI-RAT-NULL
           END-IF
           IF IND-PMO-IB-OGG = -1
              MOVE HIGH-VALUES TO PMO-IB-OGG-NULL
           END-IF
           IF IND-PMO-COS-ONER = -1
              MOVE HIGH-VALUES TO PMO-COS-ONER-NULL
           END-IF
           IF IND-PMO-SPE-PC = -1
              MOVE HIGH-VALUES TO PMO-SPE-PC-NULL
           END-IF
           IF IND-PMO-FL-ATTIV-GAR = -1
              MOVE HIGH-VALUES TO PMO-FL-ATTIV-GAR-NULL
           END-IF
           IF IND-PMO-CAMBIO-VER-PROD = -1
              MOVE HIGH-VALUES TO PMO-CAMBIO-VER-PROD-NULL
           END-IF
           IF IND-PMO-MM-DIFF = -1
              MOVE HIGH-VALUES TO PMO-MM-DIFF-NULL
           END-IF
           IF IND-PMO-IMP-RAT-MANFEE = -1
              MOVE HIGH-VALUES TO PMO-IMP-RAT-MANFEE-NULL
           END-IF
           IF IND-PMO-DT-ULT-EROG-MANFEE = -1
              MOVE HIGH-VALUES TO PMO-DT-ULT-EROG-MANFEE-NULL
           END-IF
           IF IND-PMO-TP-OGG-RIVAL = -1
              MOVE HIGH-VALUES TO PMO-TP-OGG-RIVAL-NULL
           END-IF
           IF IND-PMO-SOM-ASSTA-GARAC = -1
              MOVE HIGH-VALUES TO PMO-SOM-ASSTA-GARAC-NULL
           END-IF
           IF IND-PMO-PC-APPLZ-OPZ = -1
              MOVE HIGH-VALUES TO PMO-PC-APPLZ-OPZ-NULL
           END-IF
           IF IND-PMO-ID-ADES = -1
              MOVE HIGH-VALUES TO PMO-ID-ADES-NULL
           END-IF
           IF IND-PMO-TP-ESTR-CNT = -1
              MOVE HIGH-VALUES TO PMO-TP-ESTR-CNT-NULL
           END-IF
           IF IND-PMO-COD-RAMO = -1
              MOVE HIGH-VALUES TO PMO-COD-RAMO-NULL
           END-IF
           IF IND-PMO-GEN-DA-SIN = -1
              MOVE HIGH-VALUES TO PMO-GEN-DA-SIN-NULL
           END-IF
           IF IND-PMO-COD-TARI = -1
              MOVE HIGH-VALUES TO PMO-COD-TARI-NULL
           END-IF
           IF IND-PMO-NUM-RAT-PAG-PRE = -1
              MOVE HIGH-VALUES TO PMO-NUM-RAT-PAG-PRE-NULL
           END-IF.
           IF IND-PMO-PC-SERV-VAL = -1
             MOVE HIGH-VALUES  TO PMO-PC-SERV-VAL-NULL
           END-IF.
           IF IND-PMO-ETA-AA-SOGL-BNFICR = -1
             MOVE HIGH-VALUES  TO PMO-ETA-AA-SOGL-BNFICR-NULL
           END-IF.
      *
       Z100-EX.
           EXIT.
      *
      * ****************************************************************
      * VALORIZZAZZIONE CAMPI DATA SERVICES.
      * ****************************************************************
      *
       Z150-VALORIZZA-DATA-SERVICES.
      *
----->     MOVE IDSV0003-OPERAZIONE     TO PMO-DS-OPER-SQL.

----->*    MOVE 1                       TO PMO-DS-VER.

----->     MOVE IDSV0003-USER-NAME      TO PMO-DS-UTENTE.
      *
       Z150-EX.
           EXIT.
      *
       Z200-SET-INDICATORI-NULL.
      *
      *  --> PARAMETRO MOVIMENTO
      *
           IF PMO-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-PMO-ID-MOVI-CHIU
           END-IF
           IF PMO-TP-MOVI-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-MOVI
           ELSE
              MOVE 0 TO IND-PMO-TP-MOVI
           END-IF
           IF PMO-FRQ-MOVI-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-FRQ-MOVI
           ELSE
              MOVE 0 TO IND-PMO-FRQ-MOVI
           END-IF
           IF PMO-DUR-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DUR-AA
           ELSE
              MOVE 0 TO IND-PMO-DUR-AA
           END-IF
           IF PMO-DUR-MM-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DUR-MM
           ELSE
              MOVE 0 TO IND-PMO-DUR-MM
           END-IF
           IF PMO-DUR-GG-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DUR-GG
           ELSE
              MOVE 0 TO IND-PMO-DUR-GG
           END-IF
           IF PMO-DT-RICOR-PREC-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DT-RICOR-PREC
           ELSE
              MOVE 0 TO IND-PMO-DT-RICOR-PREC
           END-IF
           IF PMO-DT-RICOR-SUCC-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DT-RICOR-SUCC
           ELSE
              MOVE 0 TO IND-PMO-DT-RICOR-SUCC
           END-IF
           IF PMO-PC-INTR-FRAZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-PC-INTR-FRAZ
           ELSE
              MOVE 0 TO IND-PMO-PC-INTR-FRAZ
           END-IF
           IF PMO-IMP-BNS-DA-SCO-TOT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IMP-BNS-DA-SCO-TOT
           ELSE
              MOVE 0 TO IND-PMO-IMP-BNS-DA-SCO-TOT
           END-IF
           IF PMO-IMP-BNS-DA-SCO-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IMP-BNS-DA-SCO
           ELSE
              MOVE 0 TO IND-PMO-IMP-BNS-DA-SCO
           END-IF
           IF PMO-PC-ANTIC-BNS-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-PC-ANTIC-BNS
           ELSE
              MOVE 0 TO IND-PMO-PC-ANTIC-BNS
           END-IF
           IF PMO-TP-RINN-COLL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-RINN-COLL
           ELSE
              MOVE 0 TO IND-PMO-TP-RINN-COLL
           END-IF
           IF PMO-TP-RIVAL-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-RIVAL-PRE
           ELSE
              MOVE 0 TO IND-PMO-TP-RIVAL-PRE
           END-IF
           IF PMO-TP-RIVAL-PRSTZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-RIVAL-PRSTZ
           ELSE
              MOVE 0 TO IND-PMO-TP-RIVAL-PRSTZ
           END-IF
           IF PMO-FL-EVID-RIVAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-FL-EVID-RIVAL
           ELSE
              MOVE 0 TO IND-PMO-FL-EVID-RIVAL
           END-IF
           IF PMO-ULT-PC-PERD-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-ULT-PC-PERD
           ELSE
              MOVE 0 TO IND-PMO-ULT-PC-PERD
           END-IF
           IF PMO-TOT-AA-GIA-PROR-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TOT-AA-GIA-PROR
           ELSE
              MOVE 0 TO IND-PMO-TOT-AA-GIA-PROR
           END-IF
           IF PMO-TP-OPZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-OPZ
           ELSE
              MOVE 0 TO IND-PMO-TP-OPZ
           END-IF
           IF PMO-AA-REN-CER-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-AA-REN-CER
           ELSE
              MOVE 0 TO IND-PMO-AA-REN-CER
           END-IF
           IF PMO-PC-REVRSB-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-PC-REVRSB
           ELSE
              MOVE 0 TO IND-PMO-PC-REVRSB
           END-IF
           IF PMO-IMP-RISC-PARZ-PRGT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IMP-RISC-PARZ-PRGT
           ELSE
              MOVE 0 TO IND-PMO-IMP-RISC-PARZ-PRGT
           END-IF
           IF PMO-IMP-LRD-DI-RAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IMP-LRD-DI-RAT
           ELSE
              MOVE 0 TO IND-PMO-IMP-LRD-DI-RAT
           END-IF
           IF PMO-IB-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IB-OGG
           ELSE
              MOVE 0 TO IND-PMO-IB-OGG
           END-IF
           IF PMO-COS-ONER-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-COS-ONER
           ELSE
              MOVE 0 TO IND-PMO-COS-ONER
           END-IF
           IF PMO-SPE-PC-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-SPE-PC
           ELSE
              MOVE 0 TO IND-PMO-SPE-PC
           END-IF
           IF PMO-FL-ATTIV-GAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-FL-ATTIV-GAR
           ELSE
              MOVE 0 TO IND-PMO-FL-ATTIV-GAR
           END-IF
           IF PMO-CAMBIO-VER-PROD-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-CAMBIO-VER-PROD
           ELSE
              MOVE 0 TO IND-PMO-CAMBIO-VER-PROD
           END-IF
           IF PMO-MM-DIFF-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-MM-DIFF
           ELSE
              MOVE 0 TO IND-PMO-MM-DIFF
           END-IF
           IF PMO-IMP-RAT-MANFEE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IMP-RAT-MANFEE
           ELSE
              MOVE 0 TO IND-PMO-IMP-RAT-MANFEE
           END-IF
           IF PMO-DT-ULT-EROG-MANFEE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DT-ULT-EROG-MANFEE
           ELSE
              MOVE 0 TO IND-PMO-DT-ULT-EROG-MANFEE
           END-IF
           IF PMO-TP-OGG-RIVAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-OGG-RIVAL
           ELSE
              MOVE 0 TO IND-PMO-TP-OGG-RIVAL
           END-IF
           IF PMO-SOM-ASSTA-GARAC-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-SOM-ASSTA-GARAC
           ELSE
              MOVE 0 TO IND-PMO-SOM-ASSTA-GARAC
           END-IF
           IF PMO-PC-APPLZ-OPZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-PC-APPLZ-OPZ
           ELSE
              MOVE 0 TO IND-PMO-PC-APPLZ-OPZ
           END-IF
           IF PMO-ID-ADES-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-ID-ADES
           ELSE
              MOVE 0 TO IND-PMO-ID-ADES
           END-IF
           IF PMO-TP-ESTR-CNT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-ESTR-CNT
           ELSE
              MOVE 0 TO IND-PMO-TP-ESTR-CNT
           END-IF
           IF PMO-COD-RAMO-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-COD-RAMO
           ELSE
              MOVE 0 TO IND-PMO-COD-RAMO
           END-IF
            IF PMO-GEN-DA-SIN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-GEN-DA-SIN
           ELSE
              MOVE 0 TO IND-PMO-GEN-DA-SIN
           END-IF
           IF PMO-COD-TARI-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-COD-TARI
           ELSE
              MOVE 0 TO IND-PMO-COD-TARI
           END-IF
           IF PMO-NUM-RAT-PAG-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-NUM-RAT-PAG-PRE
           ELSE
              MOVE 0 TO IND-PMO-NUM-RAT-PAG-PRE
           END-IF
           IF PMO-PC-SERV-VAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-PC-SERV-VAL
           ELSE
              MOVE 0  TO IND-PMO-PC-SERV-VAL
           END-IF
           IF PMO-ETA-AA-SOGL-BNFICR-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-ETA-AA-SOGL-BNFICR
           ELSE
              MOVE 0  TO IND-PMO-ETA-AA-SOGL-BNFICR
           END-IF.

      *
       Z200-EX.
           EXIT.
      *
       Z900-CONVERTI-N-TO-X.
      *
      * --> PARAMETRO MOVIMENTO
      *
           MOVE PMO-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO PMO-DT-INI-EFF-DB
           MOVE PMO-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO PMO-DT-END-EFF-DB
           IF IND-PMO-DT-RICOR-PREC = 0
               MOVE PMO-DT-RICOR-PREC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PMO-DT-RICOR-PREC-DB
           END-IF
           IF IND-PMO-DT-RICOR-SUCC = 0
               MOVE PMO-DT-RICOR-SUCC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PMO-DT-RICOR-SUCC-DB
           END-IF
           IF IND-PMO-DT-ULT-EROG-MANFEE = 0
               MOVE PMO-DT-ULT-EROG-MANFEE TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PMO-DT-ULT-EROG-MANFEE-DB
           END-IF.
      *
       Z900-EX.
           EXIT.

       Z950-CONVERTI-X-TO-N.
      *
      * --> PARAMETRO MOVIMENTO
      *
           MOVE PMO-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO PMO-DT-INI-EFF
           MOVE PMO-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO PMO-DT-END-EFF
           IF IND-PMO-DT-RICOR-PREC = 0
               MOVE PMO-DT-RICOR-PREC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PMO-DT-RICOR-PREC
           END-IF
           IF IND-PMO-DT-RICOR-SUCC = 0
               MOVE PMO-DT-RICOR-SUCC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PMO-DT-RICOR-SUCC
           END-IF
           IF IND-PMO-DT-ULT-EROG-MANFEE = 0
               MOVE PMO-DT-ULT-EROG-MANFEE-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PMO-DT-ULT-EROG-MANFEE
           END-IF.
      *
       Z950-EX.
           EXIT.
      *
       Z960-LENGTH-VCHAR.
      *
           CONTINUE.
      *
       Z960-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
      *
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

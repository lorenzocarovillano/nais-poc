      *  ============================================================= *
      *                                                                *
      *        PORTAFOGLIO VITA ITALIA  VER 1.0                        *
      *                                                                *
      *  ============================================================= *
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LOAS0320.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *     PROGRAMMA ..... LOAS0320                                   *
      *     TIPOLOGIA...... DRIVER EOC                                 *
      *     PROCESSO....... XXX                                        *
      *     FUNZIONE....... XXX                                        *
      *     DESCRIZIONE.... RIVALUTAZIONI - EOC                        *
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
         SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *
      *----------------------------------------------------------------*
      *     VARIABILI DI WORKING
      *----------------------------------------------------------------*
      *
       01 WK-PGM                           PIC X(008) VALUE 'LOAS0320'.
       01 WK-TABELLA                       PIC X(030) VALUE SPACES.
      *
      *----------------------------------------------------------------*
      *     INDICI
      *----------------------------------------------------------------*
      *
       01  IX-INDICI.
           03 IX-TAB-GRZ                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-TGA                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-PMO                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-TDR                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-DTR                     PIC S9(04) COMP VALUE 0.
      *
      *----------------------------------------------------------------*
      *       COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
      *
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
      *
      *----------------------------------------------------------------*
      *   AREA ESTRAZIONE SEQUANCE
      *----------------------------------------------------------------*
      *
       01  AREA-IO-LCCS0090.
           COPY LCCC0090                 REPLACING ==(SF)== BY ==S090==.
      *
      *----------------------------------------------------------------*
      *    COPY DISPATCHER
      *----------------------------------------------------------------*
      *
       01  IDSV0012.
           COPY IDSV0012.
      *
       01  DISPATCHER-VARIABLES.
      *
      * Copy di input  alla  chiamata del modulo dispatcher
      *
           COPY IDSI0011.
      *
      * Copy di output dalla chiamata del modulo dispatcher
      *
           COPY IDSO0011.
      *
      *----------------------------------------------------------------*
      *    COPY DELLE TABELLE DB2
      *----------------------------------------------------------------*
      *
      * --> TRANCHE DI GARANZIA
           COPY IDBVTGA1.
      * --> PARAMETRO MOVIMENTO
           COPY IDBVPMO1.
      * --> TITOLO DI RATA
           COPY IDBVTDR1.
      * --> DETTAGLIO TITOLO DI RATA
           COPY IDBVDTR1.
      * --> WHERE CONDITION SU DETTAGLIO TITOLO DI RATA
           COPY LDBV3461.

      * COPY ELE MAX TAB PTF
           COPY LCCVDTRZ.
           COPY LCCVTDRZ.
      *
      *----------------------------------------------------------------*
      *         CAMPI DI APPOGGIO VARI
      *----------------------------------------------------------------*
      *
       01  WS-ID-GARANZIA                    PIC S9(09) COMP-3.
       01  WS-ID-PTF-GAR                     PIC S9(09) COMP-3.
       01  WS-ID-TIT-RAT                     PIC S9(09) COMP-3.
       01  WS-ID-PTF-TIT-RAT                 PIC S9(09) COMP-3.
      *
       01 FLAG                               PIC  X(02).
           88 TROVATO                          VALUE 'SI'.
           88 NON-TROVATO                      VALUE 'NO'.
      *
       01 WK-FINE-DTR                        PIC  X(02).
           88 WK-FINE-DTR-SI                   VALUE 'SI'.
           88 WK-FINE-DTR-NO                   VALUE 'NO'.
      *
      * --> Area di appoggio per i messaggi di errore
      *
       01 WK-GESTIONE-MSG-ERR.
          03 WK-LABEL-ERR                  PIC X(30).
          03 WK-STRING                     PIC X(85).
      *
      * -- ID Titolo Di Rata
      *
       01 WK-ID-TIT-RAT                    PIC S9(09) COMP-3.
      *
      *----------------------------------------------------------------*
      * AREE MODULO LCCS0234
      *----------------------------------------------------------------*
      *--> AREA I/O LCCS0234
       01 S234-DATI-INPUT.
            04 S234-ID-OGG-EOC         PIC S9(009).
            04 S234-TIPO-OGG-EOC       PIC X(002).
       01 S234-DATI-OUTPUT.
            04 S234-ID-OGG-PTF-EOC     PIC S9(009).
            04 S234-IB-OGG-PTF-EOC     PIC X(040).
            04 S234-ID-POLI-PTF        PIC S9(9).
            04 S234-ID-ADES-PTF        PIC S9(9).
      *
      *----------------------------------------------------------------*
      *                           MODULI CHIAMATI
      *----------------------------------------------------------------*
      *
      *  --> EOC COMUNE
       01  LOAS0110                         PIC X(8) VALUE 'LOAS0110'.
      *  --> ESTRAZIONE SEQUENCE
       01  LCCS0090                         PIC X(008) VALUE 'LCCS0090'.
      *  --> ESTRAZIONE OGGETTO PTF
       01  LCCS0234                         PIC X(008) VALUE 'LCCS0234'.
      *  --> TIPOLOGICHE PORTAFOGLIO
       COPY LCCC0006.
      *
      *----------------------------------------------------------------*
      *    DEFINIZIONE DI COSTANTI
      *----------------------------------------------------------------*
      *
      *
      *----------------------------------------------------------------*
      *    AREA PER MODULO LOAS0110 - EOC COMUNE
      *----------------------------------------------------------------*
      *
       01 WRIC-AREA-RICHIESTA.
          03 WRIC-ELE-RICH-MAX       PIC S9(04) COMP.
          03 WRIC-TAB-RICH.
          COPY LCCVRIC1              REPLACING ==(SF)== BY ==WRIC==.
      *
      *----------------------------------------------------------------*
      *    AREA GESTIONE TITOLO DI RATA E DETTAGLIO TITOLO DI RATA
      *----------------------------------------------------------------*
      *
       01 AREA-RATA.
      * --> Area Titolo Di Rata
           03 WTDR-AREA-TDR.
              04 WTDR-ELE-TDR-MAX        PIC S9(04) COMP VALUE 0.
                COPY LCCVTDRA REPLACING   ==(SF)==  BY ==WTDR==.
              COPY LCCVTDR1              REPLACING ==(SF)== BY ==WTDR==.
      * --> Area Dettaglio Titolo Di Rata
           03 WDTR-AREA-DTR.
              04 WDTR-ELE-DTR-MAX        PIC S9(04) COMP VALUE 0.
                COPY LCCVDTRA REPLACING   ==(SF)==  BY ==WDTR==.
              COPY LCCVDTR1              REPLACING ==(SF)== BY ==WDTR==.
      *
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
      *
       01  AREA-IDSV0001.
           COPY IDSV0001.
      *
      *----------------------------------------------------------------*
      *    WORK-COMMAREA
      *----------------------------------------------------------------*
      *
      *--  AREA GARANZIA
       01 WGRZ-AREA-GARANZIA.
          04 WGRZ-ELE-GAR-MAX        PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
          COPY LCCVGRZ1              REPLACING ==(SF)== BY ==WGRZ==.
      *--  AREA TRANCHE DI GARANZIA
       01 WTGA-AREA-TRANCHE.
          04 WTGA-ELE-TRAN-MAX       PIC S9(04) COMP.
                COPY LCCVTGAC REPLACING   ==(SF)==  BY ==WTGA==.
          COPY LCCVTGA1              REPLACING ==(SF)== BY ==WTGA==.
      *
      *----------------------------------------------------------------*
      *    AREA PASSAGGIO DA-A MAIN
      *----------------------------------------------------------------*
      *
       01 AREA-MAIN.
      * -- Area parametro movimento
          03 WPMO-AREA-PARAM-MOV.
             04 WPMO-ELE-PARAM-MOV-MAX   PIC S9(04) COMP.
                COPY LCCVPMOA REPLACING   ==(SF)==  BY ==WPMO==.
             COPY LCCVPMO1               REPLACING ==(SF)== BY ==WPMO==.
      * -- Area polizza
          03 WPOL-AREA-POLIZZA.
             04 WPOL-ELE-POLI-MAX        PIC S9(04) COMP.
             04 WPOL-TAB-POL.
             COPY LCCVPOL1               REPLACING ==(SF)== BY ==WPOL==.
      * -- Area Adesione
          03 WADE-AREA-ADESIONE.
             04 WADE-ELE-ADES-MAX        PIC S9(04) COMP.
             04 WADE-TAB-ADE.
             COPY LCCVADE1               REPLACING ==(SF)== BY ==WADE==.
      * --> Area Movimento
          03 WMOV-AREA-MOVIMENTO.
              04 WMOV-ELE-MOVI-MAX        PIC S9(04) COMP.
              04 WMOV-TAB-MOV.
              COPY LCCVMOV1              REPLACING ==(SF)== BY ==WMOV==.
      *
      *----------------------------------------------------------------*
      *  AREA DATI DI PASSAGGIO
      *----------------------------------------------------------------*
      *
       01 WCOM-IO-STATI.
           03 WCOM-AREA-STATI.
              COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.
           03 WCOM-AREA-LCCC0261.
              COPY LCCC0261              REPLACING ==(SF)== BY ==WCOM==.

A3227  01  IABV0006.
A3227      COPY IABV0006.
      *
      *  ====================  M A I N    L I N E  =================== *
      *
       PROCEDURE DIVISION           USING AREA-IDSV0001
                                          WGRZ-AREA-GARANZIA
                                          WTGA-AREA-TRANCHE
                                          AREA-MAIN
                                          WCOM-IO-STATI
A3227                                     IABV0006.
      *
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU S0000-OPERAZIONI-INIZIALI-EX.
      *
           PERFORM S1000-ELABORAZIONE
              THRU S1000-ELABORAZIONE-EX.
      *
           PERFORM S9000-OPERAZIONI-FINALI
              THRU S9000-OPERAZIONI-FINALI-EX.
      *
           GOBACK.
      *
      *----------------------------------------------------------------*
      *                      OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
      *
       S0000-OPERAZIONI-INIZIALI.
      *
           MOVE 'S0000-OPERAZIONI-INIZIALI'
             TO WK-LABEL-ERR.
      *
      * --> Inizializzazione Area E Indice Titolo Di Rata
      *
           PERFORM S0100-INIZIA-AREA-TDR
              THRU S0100-INIZIA-AREA-TDR-EX.
      *
      * --> Inizializzazione Area E Indice Dettaglio Titolo Di Rata
      *
           PERFORM S0200-INIZIA-AREA-DTR
              THRU S0200-INIZIA-AREA-DTR-EX.
      *
           MOVE ZEROES
             TO WK-ID-TIT-RAT.
      *
       S0000-OPERAZIONI-INIZIALI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *      INIZIALIZZAZIONE AREA E INDICE TITOLO DI RATA
      *----------------------------------------------------------------*
      *
       S0100-INIZIA-AREA-TDR.
      *
      * --> Inizializzazione Titolo Di Rata
      *
           PERFORM INIZIA-TOT-TDR
              THRU INIZIA-TOT-TDR-EX
           VARYING IX-TAB-TDR FROM 1 BY 1
             UNTIL IX-TAB-TDR > WK-TDR-MAX-A
      *
           MOVE ZEROES
             TO WTDR-ELE-TDR-MAX
                IX-TAB-TDR.
      *
       S0100-INIZIA-AREA-TDR-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *      INIZIALIZZAZIONE AREA E INDICE DETTAGLIO TITOLO DI RATA
      *----------------------------------------------------------------*
      *
       S0200-INIZIA-AREA-DTR.
      *
      * --> Inizializzazione Titolo Di Rata
      *
           PERFORM INIZIA-TOT-DTR
              THRU INIZIA-TOT-DTR-EX
           VARYING IX-TAB-DTR FROM 1 BY 1
             UNTIL IX-TAB-DTR > WK-DTR-MAX-A
      *
           MOVE ZEROES
             TO WDTR-ELE-DTR-MAX
                IX-TAB-DTR.
      *
       S0200-INIZIA-AREA-DTR-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                         ELABORAZIONE
      *----------------------------------------------------------------*
      *
       S1000-ELABORAZIONE.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM GESTIONE-EOC-COMUNE
                 THRU GESTIONE-EOC-COMUNE-EX
      *
           END-IF.
      *
           PERFORM AGGIORNA-TRCH-DI-GAR
              THRU AGGIORNA-TRCH-DI-GAR-EX
           VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
                OR IDSV0001-ESITO-KO.
      *
           PERFORM S1100-INVALIDA-DTR
              THRU S1100-INVALIDA-DTR-EX
           VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
                OR IDSV0001-ESITO-KO.
      *
           IF IDSV0001-ESITO-OK
      *
              IF WK-ID-TIT-RAT NOT = ZEROES
      *
                 IF  WPOL-TP-FRM-ASSVA = 'IN'       OR
                    (WPOL-TP-FRM-ASSVA = 'CO'       AND
                     WPOL-TP-LIV-GENZ-TIT = 'AD')
      *
                     PERFORM S1200-INVALIDA-TDR
                        THRU S1200-INVALIDA-TDR-EX
      *
                 END-IF
      *
              END-IF
      *
           END-IF.
      *
       S1000-ELABORAZIONE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *   INVALIDAZIONE DETTAGLI TITOLI DI RATA
      *----------------------------------------------------------------*
      *
       S1100-INVALIDA-DTR.
      *
           PERFORM S1110-IMPOSTA-DTR
              THRU S1110-IMPOSTA-DTR-EX.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata
      *
                       CONTINUE
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
      *  --> Operazione eseguita correttamente
      *
                       MOVE 1
                         TO IX-TAB-DTR
                            WDTR-ELE-DTR-MAX
                       MOVE IDSO0011-BUFFER-DATI
                         TO DETT-TIT-DI-RAT
                       PERFORM VALORIZZA-OUTPUT-DTR
                          THRU VALORIZZA-OUTPUT-DTR-EX
                       SET WDTR-ST-MOD(1)
                         TO TRUE
      *
                       MOVE 'N'
                         TO WDTR-FL-VLDT-TIT(1)
      *
                       MOVE WDTR-ID-TIT-RAT(1)
                         TO WK-ID-TIT-RAT
      *
                       PERFORM AGGIORNA-DETT-TIT-DI-RAT
                          THRU AGGIORNA-DETT-TIT-DI-RAT-EX
      *
                  WHEN OTHER
      *
      *  --> Errore di accesso al db
      *
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1100-INVALIDA-DTR'
                         TO IEAI9901-LABEL-ERR
                       MOVE '001114'
                         TO IEAI9901-COD-ERRORE
                       MOVE 'ERRORE LETTURA DTR'
                         TO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore dispatcher
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1100-INVALIDA-DTR'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              MOVE 'ERRORE DISPATCHER LETTURA DTR'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           PERFORM S0200-INIZIA-AREA-DTR
              THRU S0200-INIZIA-AREA-DTR-EX.
      *
           PERFORM S1110-IMPOSTA-DTR
              THRU S1110-IMPOSTA-DTR-EX.
      *
       S1100-INVALIDA-DTR-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VALORIZZAZIONE PER LETTURA DETTAGLIO TITOLO DI RATA
      *----------------------------------------------------------------*
      *
       S1110-IMPOSTA-DTR.
      *
           INITIALIZE DETT-TIT-DI-RAT.
      *
           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
             TO DTR-ID-OGG.
      *
           MOVE 'TG'
             TO DTR-TP-OGG.
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE ZERO
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
                IDSI0011-DATA-COMPETENZA.
      *
      *  --> Nome tabella fisica db
      *
           MOVE 'DETT-TIT-DI-RAT'
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE DETT-TIT-DI-RAT
             TO IDSI0011-BUFFER-DATI.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-SELECT
             TO TRUE.
      *
      *  --> Tipo livello
      *
           SET IDSI0011-ID-OGGETTO
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSI0011-TRATT-DEFAULT
             TO TRUE.
      *
      *  --> Inizializzazione Return Code
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S1110-IMPOSTA-DTR-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *   INVALIDAZIONE TITOLO DI RATA
      *----------------------------------------------------------------*
      *
       S1200-INVALIDA-TDR.
      *
           PERFORM S1210-IMPOSTA-TDR
              THRU S1210-IMPOSTA-TDR-EX.
      *
           PERFORM S1220-DISPATCHER-TDR
              THRU S1220-DISPATCHER-TDR-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S1230-IMPOSTA-LDBV3461
                 THRU S1230-IMPOSTA-LDBV3461-EX
      *
              SET WK-FINE-DTR-NO
                TO TRUE
              SET IDSI0011-FETCH-FIRST
                TO TRUE
      *
              PERFORM S1240-AGGIORNA-DTR-COLLEG
                 THRU S1240-AGGIORNA-DTR-COLLEG-EX
                UNTIL WK-FINE-DTR-SI
                   OR IDSV0001-ESITO-KO
      *
           END-IF.
      *
       S1200-INVALIDA-TDR-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * LETTURA/AGGIORNAMENTO TITOLO DI RATA
      *----------------------------------------------------------------*
      *
       S1220-DISPATCHER-TDR.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata
      *
                       CONTINUE
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
      *  --> Operazione eseguita correttamente
      *
                       MOVE 1
                         TO IX-TAB-TDR
                            WTDR-ELE-TDR-MAX
                       MOVE IDSO0011-BUFFER-DATI
                         TO TIT-RAT
                       PERFORM VALORIZZA-OUTPUT-TDR
                          THRU VALORIZZA-OUTPUT-TDR-EX
                       SET WTDR-ST-MOD(1)
                         TO TRUE
      *
                       MOVE 'N'
                         TO WTDR-FL-VLDT-TIT(1)
      *
                       PERFORM AGGIORNA-TIT-RAT
                          THRU AGGIORNA-TIT-RAT-EX
      *
                  WHEN OTHER
      *
      *  --> Errore di accesso al db
      *
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1200-INVALIDA-TDR'
                         TO IEAI9901-LABEL-ERR
                       MOVE '001114'
                         TO IEAI9901-COD-ERRORE
                       MOVE 'ERRORE LETTURA TDR'
                         TO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore dispatcher
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1200-INVALIDA-TDR'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              MOVE 'ERRORE DISPATCHER LETTURA TDR'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           PERFORM S0100-INIZIA-AREA-TDR
              THRU S0100-INIZIA-AREA-TDR-EX.
      *
      *
       S1220-DISPATCHER-TDR-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * AGGIORNAMENTO DI EVENTUALI ALTRI DETTAGLI LEGATI AL TITOLO
      *----------------------------------------------------------------*
      *
       S1240-AGGIORNA-DTR-COLLEG.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata
      *
                       SET WK-FINE-DTR-SI
                         TO TRUE
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
      *  --> Operazione eseguita correttamente
      *
                       MOVE 1
                         TO IX-TAB-DTR
                            WDTR-ELE-DTR-MAX
                       MOVE IDSO0011-BUFFER-DATI
                         TO DETT-TIT-DI-RAT
                       PERFORM VALORIZZA-OUTPUT-DTR
                          THRU VALORIZZA-OUTPUT-DTR-EX
                       SET WDTR-ST-MOD(1)
                         TO TRUE
      *
                       MOVE 'N'
                         TO WDTR-FL-VLDT-TIT(1)
      *
                       MOVE WDTR-ID-TIT-RAT(1)
                         TO WK-ID-TIT-RAT
      *
                       PERFORM AGGIORNA-DETT-TIT-DI-RAT
                          THRU AGGIORNA-DETT-TIT-DI-RAT-EX
      *
                       SET IDSI0011-FETCH-NEXT
                         TO TRUE
      *
                  WHEN OTHER
      *
      *  --> Errore di accesso al db
      *
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1240-AGGIORNA-DTR-COLLEG'
                         TO IEAI9901-LABEL-ERR
                       MOVE '001114'
                         TO IEAI9901-COD-ERRORE
                       MOVE 'ERRORE LETTURA DTR'
                         TO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore dispatcher
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1240-AGGIORNA-DTR-COLLEG'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              MOVE 'ERRORE DISPATCHER LETTURA DTR'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           PERFORM S0200-INIZIA-AREA-DTR
              THRU S0200-INIZIA-AREA-DTR-EX.
      *
           PERFORM S1230-IMPOSTA-LDBV3461
              THRU S1230-IMPOSTA-LDBV3461-EX.
      *
       S1240-AGGIORNA-DTR-COLLEG-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VALORIZZAZIONE PER LETTURA DETTAGLIO TITOLO DI RATA (AD HOC)
      *----------------------------------------------------------------*
      *
       S1230-IMPOSTA-LDBV3461.
      *
           MOVE WK-ID-TIT-RAT
             TO DTR-ID-TIT-RAT.
      *      TO LDBV3461-ID-TIT-RAT.
      *
           MOVE 'S'
             TO DTR-FL-VLDT-TIT.
      *      TO LDBV3461-FL-VLDT-TIT
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE ZERO
            TO IDSI0011-DATA-INIZIO-EFFETTO
               IDSI0011-DATA-FINE-EFFETTO
               IDSI0011-DATA-COMPETENZA.
      *
           SET IDSI0011-TRATT-DEFAULT  TO TRUE.
      *
      *  --> Nome tabella fisica db
      *
           MOVE 'LDBS3460'
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE SPACES
             TO IDSI0011-BUFFER-WHERE-COND.
           MOVE DETT-TIT-DI-RAT
             TO IDSI0011-BUFFER-DATI.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-TRATT-DEFAULT
             TO TRUE.
           SET IDSI0011-WHERE-CONDITION
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S1230-IMPOSTA-LDBV3461-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VALORIZZAZIONE PER LETTURA TITOLO DI RATA
      *----------------------------------------------------------------*
      *
       S1210-IMPOSTA-TDR.
      *
           INITIALIZE DETT-TIT-DI-RAT.
      *
           MOVE WK-ID-TIT-RAT
             TO TDR-ID-TIT-RAT.
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE ZERO
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
                IDSI0011-DATA-COMPETENZA.
      *
      *  --> Nome tabella fisica db
      *
           MOVE 'TIT-RAT'
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE TIT-RAT
             TO IDSI0011-BUFFER-DATI.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-SELECT
             TO TRUE.
      *
      *  --> Tipo livello
      *
           SET IDSI0011-ID
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSI0011-TRATT-DEFAULT
             TO TRUE.
      *
      *  --> Inizializzazione Return Code
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S1210-IMPOSTA-TDR-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *        GESTIONE DELLA CHIAMATA AL SERVIZIO DI EOC COMUNE
      *----------------------------------------------------------------*
      *
       GESTIONE-EOC-COMUNE.
      *
           PERFORM PREPARA-AREA-LOAS0110
              THRU PREPARA-AREA-LOAS0110-EX.
      *
           PERFORM CALL-EOC-COMUNE
              THRU CALL-EOC-COMUNE-EX.
      *
       GESTIONE-EOC-COMUNE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *         VALORIZZAZIONE INPUT DEL SERVIZIO DI EOC COMUNE
      *----------------------------------------------------------------*
      *
       PREPARA-AREA-LOAS0110.
      *
           MOVE 1                          TO WCOM-STEP-ELAB.
      *
       PREPARA-AREA-LOAS0110-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                  CALL AL SERVIZIO DI EOC COMUNE
      *----------------------------------------------------------------*
      *
       CALL-EOC-COMUNE.
      *
           CALL LOAS0110    USING AREA-IDSV0001
                                  WADE-AREA-ADESIONE
                                  WGRZ-AREA-GARANZIA
                                  WMOV-AREA-MOVIMENTO
                                  WPMO-AREA-PARAM-MOV
                                  WPOL-AREA-POLIZZA
                                  WRIC-AREA-RICHIESTA
                                  WTGA-AREA-TRANCHE
                                  WCOM-IO-STATI
A3227                             IABV0006
      *
           ON EXCEPTION
      *
                 MOVE 'LOAS0110'             TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'SERVIZIO EOC COMUNE'  TO CALL-DESC
                 MOVE 'CALL-EOC-COMUNE'      TO WK-LABEL-ERR
      *
                 PERFORM GESTIONE-ERR-SIST
                    THRU GESTIONE-ERR-SIST-EX
      *
           END-CALL.
      *
       CALL-EOC-COMUNE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *          CALL SERVIZIO ESTRAZIONE OGGETTO PTF LCCS0234
      *----------------------------------------------------------------*
      *
       CALL-LCCS0234.
      *
           CALL LCCS0234  USING AREA-IDSV0001
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TRANCHE
                                S234-DATI-INPUT
                                S234-DATI-OUTPUT
           ON EXCEPTION
      *
                  MOVE 'LCCS0234'
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'IL REPERIMENTO ID PORTAFOGLIO'
                    TO CALL-DESC
                  MOVE 'CALL-LCCS0234'
                    TO WK-LABEL-ERR
      *
                  PERFORM GESTIONE-ERR-SIST
                     THRU GESTIONE-ERR-SIST-EX
      *
           END-CALL.
      *
       CALL-LCCS0234-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *           GESTIONE STANDARD DELL'ERRORE DI SISTEMA
      *----------------------------------------------------------------*
      *
       GESTIONE-ERR-SIST.
      *
           SET WCOM-WRITE-ERR          TO TRUE.
      *
           MOVE WK-LABEL-ERR           TO IEAI9901-LABEL-ERR
      *
           PERFORM S0290-ERRORE-DI-SISTEMA
              THRU EX-S0290.
      *
       GESTIONE-ERR-SIST-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                         OPERAZIONI FINALI
      *----------------------------------------------------------------*
      *
       S9000-OPERAZIONI-FINALI.
      *
           MOVE 'S9000-OPERAZIONI-FINALI'  TO WK-LABEL-ERR.
      *
       S9000-OPERAZIONI-FINALI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     VALORIZZAZION TABELLE
      *----------------------------------------------------------------*
      *
            COPY LCCVTDR3                REPLACING ==(SF)== BY ==WTDR==.
            COPY LCCVTDR4                REPLACING ==(SF)== BY ==WTDR==.
            COPY LCCVDTR3                REPLACING ==(SF)== BY ==WDTR==.
            COPY LCCVDTR4                REPLACING ==(SF)== BY ==WDTR==.
      *
      *----------------------------------------------------------------*
      *     CONTIENE STATEMENTS PER LA FASE DI EOC
      *----------------------------------------------------------------*
      *
            COPY LCCVTGA5        REPLACING ==(SF)== BY ==WTGA==.
            COPY LCCVTGA6        REPLACING ==(SF)== BY ==WTGA==.
            COPY LCCVPMO5        REPLACING ==(SF)== BY ==WPMO==.
            COPY LCCVPMO6        REPLACING ==(SF)== BY ==WPMO==.
            COPY LCCVTDR5        REPLACING ==(SF)== BY ==WTDR==.
            COPY LCCVTDR6        REPLACING ==(SF)== BY ==WTDR==.
            COPY LCCVDTR5        REPLACING ==(SF)== BY ==WDTR==.
            COPY LCCVDTR6        REPLACING ==(SF)== BY ==WDTR==.
      *
            COPY LCCP0001.
            COPY LCCP0002.
      *
      *----------------------------------------------------------------*
      *  ROUTINES GESTIONE ERRORI
      *----------------------------------------------------------------*
      *
           COPY IERP9902.
           COPY IERP9901.
      *
      *----------------------------------------------------------------*
      *    ROUTINES DISPATCHER
      *----------------------------------------------------------------*
      *
           COPY IDSP0012.

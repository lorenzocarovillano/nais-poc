      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS2760.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2010.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS2760
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... MODULO DI CALCOLO FLPOLUNIC
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS2760'.
      *--> Conversione Stringa a Numerico
       01  IWFS0050                         PIC X(008) VALUE 'IWFS0050'.
       01  LDBS5910                         PIC X(008) VALUE 'LDBS5910'.
       01  LDBS7480                         PIC X(008) VALUE 'LDBS7480'.
       01  LDBS1420                         PIC X(008) VALUE 'LDBS1420'.
       01  WK-CALL-PGM                      PIC X(008).
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*

      *--> Area per modulo conversione stringa
       01  AREA-CALL-IWFS0050.
           COPY IWFI0051.
           COPY IWFO0051.

      *----------------------------------------------------------------*
      *  COPY PER CHIAMATE LDBS
      *----------------------------------------------------------------*
           COPY LDBV1200.

           COPY IDBVRAN1.
           COPY IDBVGRZ1.
           COPY IDBVPOL1.
           COPY LDBV1421.
NEWFI      COPY IDBVSTB1.
      *---------------------------------------------------------------*
      *  COPY TIPOLOGICHE
      *---------------------------------------------------------------*
           COPY ISPV0000.
      *----------------------------------------------------------------*
      *--> AREA RAPPORTO ANAGRAFICO
      *----------------------------------------------------------------*
       01 AREA-IO-RAN.
          03 DRAN-AREA-RAPP-ANA.
             04 DRAN-ELE-RAN-MAX        PIC S9(04) COMP.
                COPY LCCVRANA REPLACING   ==(SF)==  BY ==DRAN==.
             COPY LCCVRAN1              REPLACING ==(SF)== BY ==DRAN==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.
      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *--
           COPY LCCVXOG0.
           COPY LCCVXSB0.

      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                     PIC S9(04) COMP.


      *----------------------------------------------------------------*
      * VARIABILI DI WORKING
      *----------------------------------------------------------------*
       01 WK-VARIABILI.
           03 NUM-POLI                        PIC 9(09).

           03 FLAG-POLI                       PIC X(1).
              88 POLI-SI                      VALUE 'S'.
              88 POLI-NO                      VALUES 'N'.

           03 FLAG-FINE-RAN                       PIC X(1).
              88 FINE-RAN-SI                      VALUE 'S'.
              88 FINE-RAN-NO                      VALUES 'N'.

           03 FLAG-FINE-GAR                       PIC X(1).
              88 FINE-GAR-SI                      VALUE 'S'.
              88 FINE-GAR-NO                      VALUES 'N'.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS2760.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS2760.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT
                                             WK-VARIABILI.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

           MOVE 0 TO NUM-POLI.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           PERFORM S1100-LETTURA-CONTRAENTE
              THRU S1100-LETTURA-CONTRAENTE-EX.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-RAPP-ANAG
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DRAN-AREA-RAPP-ANA
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    INIZIALIZZA LETTURA TABELLA CONTRAENTE
      *----------------------------------------------------------------*
       S1101-INIZIA-CONTRAENTE.

              INITIALIZE RAPP-ANA

              MOVE DRAN-COD-SOGG(1)
                TO RAN-COD-SOGG

      *--> TIPO CONTRAENTE
              MOVE 'CO'              TO RAN-TP-RAPP-ANA

              MOVE 'PO'              TO RAN-TP-OGG

              MOVE RAPP-ANA          TO IDSV0003-BUFFER-WHERE-COND

              SET IDSV0003-TRATT-X-COMPETENZA TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSV0003-WHERE-CONDITION    TO TRUE.

       S1101-INIZIA-CONTRAENTE-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA DATI TABELLA CONTRAENTE
      *----------------------------------------------------------------*
       S1100-LETTURA-CONTRAENTE.

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE.

           SET FINE-RAN-NO                TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.

           PERFORM UNTIL FINE-RAN-SI

              PERFORM S1101-INIZIA-CONTRAENTE
                 THRU S1101-INIZIA-CONTRAENTE-EX

              CALL LDBS5910 USING IDSV0003 RAPP-ANA
              ON EXCEPTION
                 MOVE LDBS5910
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'ERRORE CALL LDBS5910 FETCH'
                   TO IDSV0003-DESCRIZ-ERR-DB2
                 SET IDSV0003-INVALID-OPER TO TRUE
              END-CALL

              IF IDSV0003-SUCCESSFUL-RC

                 EVALUATE TRUE

                     WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA
                          SET FINE-RAN-SI                TO TRUE

                     WHEN IDSV0003-SUCCESSFUL-SQL
      *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
      *-->   SE AL SOGGETTO SONO ASSOCIATE PIU' POLIZZE IL FLAG E' PARI AD 1
      *-->   SU CUI E' ABILITATO IL CALCOLO DI IMPOSTA DI BOLLO

                          PERFORM LETTURA-POLIZZA
                             THRU LETTURA-POLIZZA-EX

                          SET IDSV0003-FETCH-NEXT TO TRUE

                          PERFORM S1101-INIZIA-CONTRAENTE
                             THRU S1101-INIZIA-CONTRAENTE-EX

                          MOVE '1'
                            TO IVVC0213-VAL-STR-O
                          MOVE 1
                            TO IVVC0213-VAL-IMP-O
                          IF NUM-POLI > 1
                             MOVE '0'
                               TO IVVC0213-VAL-STR-O
                             MOVE 0
                               TO IVVC0213-VAL-IMP-O
                             PERFORM S1130-CHIUSURA-CURS
                                THRU S1130-CHIUSURA-CURS-EX
                             SET FINE-RAN-SI     TO TRUE
                          END-IF

                     WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                          SET IDSV0003-INVALID-OPER  TO TRUE
                          MOVE WK-PGM
                            TO IDSV0003-COD-SERVIZIO-BE
                          STRING 'ERRORE LETTURA TABELLA RAPP-ANA ;'
                                 IDSV0003-RETURN-CODE ';'
                                 IDSV0003-SQLCODE
                                 DELIMITED BY SIZE INTO
                                 IDSV0003-DESCRIZ-ERR-DB2
                          END-STRING
                          SET FINE-RAN-SI                TO TRUE

                 END-EVALUATE

              END-IF

           END-PERFORM.

       S1100-LETTURA-CONTRAENTE-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   LETTURA DELLA POLIZZA ASSOCIATA AL CONTRAENTE
      *----------------------------------------------------------------*
       LETTURA-POLIZZA.

           INITIALIZE POLI.

           MOVE RAN-ID-OGG       TO POL-ID-POLI.
           SET POLI-NO TO TRUE

           MOVE 'IDBSPOL0'                 TO WK-CALL-PGM.

      *  --> Tipo operazione
           SET IDSV0003-SELECT             TO TRUE.
      *  --> Tipo livello
           SET IDSV0003-ID                 TO TRUE.
      *
           SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.

           CALL WK-CALL-PGM USING IDSV0003 POLI

           ON EXCEPTION
              MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-IDBSPOL0 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER   TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
NEWFI         IF POL-ID-POLI = IVVC0213-ID-POLIZZA
NEWFI            SET POLI-SI TO TRUE
NEWFI         ELSE
                 IF POL-TP-RGM-FISC = '01'
                    MOVE POL-TP-POLI
                      TO ISPV0000-TP-POLI
                    IF ISPV0000-IND-FIP-PIP
                       CONTINUE
PROVA               ELSE
NEWFI                  PERFORM LETTURA-STB
NEWFI                     THRU LETTURA-STB-EX
NEWFI                  IF IN-VIGORE
PROVA                     PERFORM S1110-LETTURA-GARANZIA
PROVA                        THRU S1110-LETTURA-GARANZIA-EX
NEWFI                  END-IF
NEWFI               END-IF
                 END-IF
              END-IF

              IF POLI-SI
                 COMPUTE NUM-POLI = NUM-POLI + 1
              END-IF
           ELSE
              MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA IDBSPOL0 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER     TO TRUE
              END-IF
           END-IF.

       LETTURA-POLIZZA-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    LETTURA STATO OGGETTO BUSINESS
      *----------------------------------------------------------------*
NEWFI  LETTURA-STB.

NEWFI      INITIALIZE STAT-OGG-BUS.

NEWFI      SET POLIZZA                   TO TRUE
NEWFI      MOVE POL-ID-POLI              TO STB-ID-OGG.
NEWFI      MOVE WS-TP-OGG                TO STB-TP-OGG.

NEWFI      MOVE 'IDBSSTB0'               TO WK-CALL-PGM.

NEWFI *  --> Tipo operazione
NEWFI      SET IDSV0003-SELECT              TO TRUE.
NEWFI *  --> Tipo livello
NEWFI      SET IDSV0003-ID-OGGETTO          TO TRUE.
NEWFI *
NEWFI      SET IDSV0003-TRATT-X-COMPETENZA  TO TRUE.

NEWFI      CALL WK-CALL-PGM USING IDSV0003 STAT-OGG-BUS

NEWFI      ON EXCEPTION
NEWFI         MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
NEWFI         MOVE 'CALL-IDBSSTB0 ERRORE CHIAMATA'
NEWFI           TO IDSV0003-DESCRIZ-ERR-DB2
NEWFI         SET IDSV0003-INVALID-OPER   TO TRUE
NEWFI      END-CALL.

NEWFI      IF IDSV0003-SUCCESSFUL-SQL
NEWFI *       MOVE IDSO0011-BUFFER-DATI TO STAT-OGG-BUS
NEWFI         MOVE STB-TP-STAT-BUS      TO WS-TP-STAT-BUS

NEWFI      ELSE
NEWFI         MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
NEWFI         STRING 'CHIAMATA IDBSSTB0 ;'
NEWFI                IDSV0003-RETURN-CODE ';'
NEWFI                IDSV0003-SQLCODE
NEWFI            DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
NEWFI         END-STRING
NEWFI         IF IDSV0003-NOT-FOUND
NEWFI            SET IDSV0003-FIELD-NOT-VALUED TO TRUE
NEWFI         ELSE
NEWFI            SET IDSV0003-INVALID-OPER     TO TRUE
NEWFI         END-IF
NEWFI      END-IF.


NEWFI  LETTURA-STB-EX.
NEWFI      EXIT.
      *----------------------------------------------------------------*
      *    LETTURA DATI TABELLA GARANZIA
      *----------------------------------------------------------------*
       S1110-LETTURA-GARANZIA.

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR NOT IDSV0003-SUCCESSFUL-SQL
                      OR IDSV0003-CLOSE-CURSOR

              INITIALIZE GAR

              MOVE POL-ID-POLI
                TO LDBV1421-ID-POLI

              MOVE 5
                TO LDBV1421-RAMO-BILA-1
              MOVE 3
                TO LDBV1421-RAMO-BILA-2

              MOVE LDBV1421     TO IDSV0003-BUFFER-WHERE-COND

              SET IDSV0003-TRATT-X-COMPETENZA TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSV0003-WHERE-CONDITION TO TRUE

              CALL LDBS1420 USING IDSV0003 GAR
              ON EXCEPTION
                 MOVE LDBS1420
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'ERRORE CALL LDBS1420 FETCH'
                   TO IDSV0003-DESCRIZ-ERR-DB2
                 SET IDSV0003-INVALID-OPER TO TRUE
              END-CALL

              IF IDSV0003-SUCCESSFUL-RC

                 EVALUATE TRUE

                     WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA
                          CONTINUE

                     WHEN IDSV0003-SUCCESSFUL-SQL
      *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
      *-->   SE LA POLIZZA HA ALMENO UNA GARANZIA DI RAMO 5 O 3         AD 1
      *-->   E' AMMESSO IL CALCOLO DEL BOLLO                            AD 1
                          SET POLI-SI TO TRUE
                          PERFORM S1135-CHIUSURA-CURS-GAR
                             THRU S1135-CHIUSURA-CURS-GAR-EX

                     WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                          SET IDSV0003-INVALID-OPER  TO TRUE
                          MOVE WK-PGM
                            TO IDSV0003-COD-SERVIZIO-BE
                          STRING 'ERRORE LETTURA TABELLA GARANZIA ;'
                                 IDSV0003-RETURN-CODE ';'
                                 IDSV0003-SQLCODE
                                 DELIMITED BY SIZE INTO
                                 IDSV0003-DESCRIZ-ERR-DB2
                          END-STRING

                 END-EVALUATE

              END-IF

           END-PERFORM.

       S1110-LETTURA-GARANZIA-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CHIUSURA CURSORE GARANZIA
      *----------------------------------------------------------------*
       S1135-CHIUSURA-CURS-GAR.

           SET IDSV0003-CLOSE-CURSOR            TO TRUE.

           CALL LDBS1420 USING IDSV0003 GAR
           ON EXCEPTION
              MOVE LDBS1420
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CALL LDBS1420 CLOSE'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER TO TRUE
           END-CALL.

           IF NOT IDSV0003-SUCCESSFUL-RC
           OR NOT IDSV0003-SUCCESSFUL-SQL
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CLOSE CURSORE LDBS1420'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER
                TO TRUE
           END-IF.

       S1135-CHIUSURA-CURS-GAR-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CHIUSURA CURSORE BENEFICIARIO
      *----------------------------------------------------------------*
       S1130-CHIUSURA-CURS.

           SET IDSV0003-CLOSE-CURSOR            TO TRUE.

           CALL LDBS5910 USING IDSV0003 RAPP-ANA
           ON EXCEPTION
              MOVE LDBS5910
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CALL LDBS5910 CLOSE'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER TO TRUE
           END-CALL.

           IF NOT IDSV0003-SUCCESSFUL-RC
           OR NOT IDSV0003-SUCCESSFUL-SQL
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CLOSE CURSORE LDBS5910'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER
                TO TRUE
           END-IF.

       S1130-CHIUSURA-CURS-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           MOVE 0                          TO IVVC0213-VAL-PERC-O.

           GOBACK.

       EX-S9000.
           EXIT.

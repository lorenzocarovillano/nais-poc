       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IABS0040 IS INITIAL.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE
      * F A S E         : GESTIONE TABELLA BTC_BATCH
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*

       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)     VALUE SPACES.
       01 AREA-WHERE-CONDITION.
          02 FLAG-OPERAZIONE             PIC X(06).
             88 SELECT-X-TP-BATCH        VALUE 'TB    '.
             88 SELECT-X-DT-EFFETTO      VALUE 'DT-EFF'.
             88 SELECT-X-DT-EFFETTO-MAX  VALUE 'MAXEFF'.
             88 SELECT-X-DT-EFFETTO-MIN  VALUE 'MINEFF'.
             88 SELECT-X-DT-INSERIM-MAX  VALUE 'MAXINS'.
             88 SELECT-X-DT-INSERIM-MIN  VALUE 'MININS'.
             88 SELECT-ALL-BATCH         VALUE 'ALLBTC'.

           EXEC SQL INCLUDE IDSV0010 END-EXEC.

      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDBTC0 END-EXEC.
           EXEC SQL INCLUDE IDBVBTC2 END-EXEC.
           EXEC SQL INCLUDE IDBVBTC3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IABV0002 END-EXEC.

           EXEC SQL INCLUDE IDBVBTC1 END-EXEC.


      *****************************************************************

       PROCEDURE DIVISION USING IDSV0003 IABV0002 BTC-BATCH.

           PERFORM A000-INIZIO              THRU A000-EX.

           PERFORM A300-ELABORA             THRU A300-EX

           GOBACK.

      ******************************************************************
       A000-INIZIO.
           MOVE 'IABS0040'              TO   IDSV0003-COD-SERVIZIO-BE.
           MOVE 'BTC_BATCH'             TO   IDSV0003-NOME-TABELLA.

           MOVE '00'                    TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                  TO   IDSV0003-SQLCODE
                                             IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2
                                             IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.
      ******************************************************************
       A100-CHECK-RETURN-CODE.

           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE
           END-IF.

       A100-EX.
           EXIT.
      ******************************************************************
       A300-ELABORA.

           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
                 PERFORM A301-ELABORA-PK             THRU A301-EX
              WHEN IDSV0003-WHERE-CONDITION
                 PERFORM A302-ELABORA-WC             THRU A302-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-LEVEL-OPER     TO TRUE
           END-EVALUATE.

       A300-EX.
           EXIT.
      ******************************************************************
       A301-ELABORA-PK.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A306-SELECT                 THRU A306-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST            THRU A380-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT             THRU A390-EX
              WHEN IDSV0003-INSERT
                 PERFORM A320-INSERT                 THRU A320-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A330-UPDATE                 THRU A330-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.

       A301-EX.
           EXIT.
      ******************************************************************
       A302-ELABORA-WC.

           EVALUATE TRUE
              WHEN IDSV0003-WHERE-CONDITION-01
                 PERFORM W001-WHERE-CONDITION-01     THRU W001-01-EX

              WHEN IDSV0003-WHERE-CONDITION-02
                 PERFORM W002-WHERE-CONDITION-02     THRU W002-02-EX

              WHEN OTHER
                 SET IDSV0003-INVALID-OPER           TO TRUE

           END-EVALUATE.

       A302-EX.
           EXIT.
      ******************************************************************
       A305-DECLARE-CURSOR.

           EXEC SQL
                DECLARE CUR-BTC CURSOR WITH HOLD FOR

              SELECT
                ID_BATCH
                ,PROTOCOL
                ,DT_INS
                ,USER_INS
                ,COD_BATCH_TYPE
                ,COD_COMP_ANIA
                ,COD_BATCH_STATE
                ,DT_START
                ,DT_END
                ,USER_START
                ,ID_RICH
                ,DT_EFF
                ,DATA_BATCH
              FROM BTC_BATCH
              WHERE COD_BATCH_TYPE = :BTC-COD-BATCH-TYPE
                AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
                AND
                    (
                    COD_BATCH_STATE IN (
                                     :IABV0002-STATE-01,
                                     :IABV0002-STATE-02,
                                     :IABV0002-STATE-03,
                                     :IABV0002-STATE-04,
                                     :IABV0002-STATE-05,
                                     :IABV0002-STATE-06,
                                     :IABV0002-STATE-07,
                                     :IABV0002-STATE-08,
                                     :IABV0002-STATE-09,
                                     :IABV0002-STATE-10
                                        )
                     )
             ORDER BY ID_BATCH
           END-EXEC.

       A305-EX.
           EXIT.
      ******************************************************************
       A306-SELECT.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
              SELECT
                ID_BATCH
                ,PROTOCOL
                ,DT_INS
                ,USER_INS
                ,COD_BATCH_TYPE
                ,COD_COMP_ANIA
                ,COD_BATCH_STATE
                ,DT_START
                ,DT_END
                ,USER_START
                ,ID_RICH
                ,DT_EFF
                ,DATA_BATCH
             INTO
                :BTC-ID-BATCH
               ,:BTC-PROTOCOL
               ,:BTC-DT-INS-DB
               ,:BTC-USER-INS
               ,:BTC-COD-BATCH-TYPE
               ,:BTC-COD-COMP-ANIA
               ,:BTC-COD-BATCH-STATE
               ,:BTC-DT-START-DB
                :IND-BTC-DT-START
               ,:BTC-DT-END-DB
                :IND-BTC-DT-END
               ,:BTC-USER-START
                :IND-BTC-USER-START
               ,:BTC-ID-RICH
                :IND-BTC-ID-RICH
               ,:BTC-DT-EFF-DB
                :IND-BTC-DT-EFF
               ,:BTC-DATA-BATCH-VCHAR
                :IND-BTC-DATA-BATCH
              FROM BTC_BATCH
              WHERE PROTOCOL       = :BTC-PROTOCOL
                AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
                AND
                    (
                    COD_BATCH_STATE IN (
                                     :IABV0002-STATE-01,
                                     :IABV0002-STATE-02,
                                     :IABV0002-STATE-03,
                                     :IABV0002-STATE-04,
                                     :IABV0002-STATE-05,
                                     :IABV0002-STATE-06,
                                     :IABV0002-STATE-07,
                                     :IABV0002-STATE-08,
                                     :IABV0002-STATE-09,
                                     :IABV0002-STATE-10
                                        )
                     )
ALEX  *       WITH UR
           END-EXEC.

            PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       A306-EX.
           EXIT.

       A320-INSERT.

           PERFORM Z400-SEQ                        THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO BTC_BATCH
                     (
                       ID_BATCH
                      ,PROTOCOL
                      ,DT_INS
                      ,USER_INS
                      ,COD_BATCH_TYPE
                      ,COD_COMP_ANIA
                      ,COD_BATCH_STATE
                      ,DT_START
                      ,DT_END
                      ,USER_START
                      ,ID_RICH
                      ,DT_EFF
                      ,DATA_BATCH
                     )
                 VALUES
                     (
                       :BTC-ID-BATCH
                      ,:BTC-PROTOCOL
                      ,:BTC-DT-INS-DB
                      ,:BTC-USER-INS
                      ,:BTC-COD-BATCH-TYPE
                      ,:BTC-COD-COMP-ANIA
                      ,:BTC-COD-BATCH-STATE
                      ,:BTC-DT-START-DB
                       :IND-BTC-DT-START
                      ,:BTC-DT-END-DB
                       :IND-BTC-DT-END
                      ,:BTC-USER-START
                       :IND-BTC-USER-START
                      ,:BTC-ID-RICH
                       :IND-BTC-ID-RICH
                      ,:BTC-DT-EFF-DB
                       :IND-BTC-DT-EFF
                      ,:BTC-DATA-BATCH-VCHAR
                       :IND-BTC-DATA-BATCH
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A320-EX.
           EXIT.
      ******************************************************************
       A330-UPDATE.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE BTC_BATCH SET
                   COD_BATCH_STATE        = :IABV0002-STATE-CURRENT
                  ,DT_START               = :BTC-DT-START-DB
                                            :IND-BTC-DT-START
                  ,DT_END                 = :BTC-DT-END-DB
                                            :IND-BTC-DT-END
                  ,USER_START             = :BTC-USER-START
                                            :IND-BTC-USER-START

                WHERE   ID_BATCH = :BTC-ID-BATCH

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO BTC-COD-BATCH-STATE
           END-IF.

       A330-EX.
           EXIT.
      ******************************************************************
       A360-OPEN-CURSOR.

            PERFORM A305-DECLARE-CURSOR    THRU A305-EX
            EXEC SQL OPEN CUR-BTC      END-EXEC

            PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A360-EX.
           EXIT.
      ******************************************************************
       A370-CLOSE-CURSOR.

           EXEC SQL
                CLOSE CUR-BTC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A370-EX.
           EXIT.
      ******************************************************************
       A380-FETCH-FIRST.

           PERFORM A360-OPEN-CURSOR    THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT      THRU A390-EX
           END-IF.

       A380-EX.
           EXIT.
      ******************************************************************
       A390-FETCH-NEXT.

           EXEC SQL
                FETCH CUR-BTC
           INTO
                :BTC-ID-BATCH
               ,:BTC-PROTOCOL
               ,:BTC-DT-INS-DB
               ,:BTC-USER-INS
               ,:BTC-COD-BATCH-TYPE
               ,:BTC-COD-COMP-ANIA
               ,:BTC-COD-BATCH-STATE
               ,:BTC-DT-START-DB
                :IND-BTC-DT-START
               ,:BTC-DT-END-DB
                :IND-BTC-DT-END
               ,:BTC-USER-START
                :IND-BTC-USER-START
               ,:BTC-ID-RICH
                :IND-BTC-ID-RICH
               ,:BTC-DT-EFF-DB
                :IND-BTC-DT-EFF
               ,:BTC-DATA-BATCH-VCHAR
                :IND-BTC-DATA-BATCH
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       A390-EX.
           EXIT.

      ******************************************************************
      * WHERE CONDITION 01
      ******************************************************************
       W001-WHERE-CONDITION-01.

           MOVE IDSV0003-BUFFER-WHERE-COND
                TO AREA-WHERE-CONDITION

           EVALUATE TRUE

              WHEN SELECT-X-TP-BATCH
                   PERFORM W100-WH-SELECT-TP-BATCH THRU W100-EX
              WHEN SELECT-X-DT-EFFETTO
                   PERFORM W200-WH-SELECT-DT-EFFETTO THRU W200-EX
              WHEN SELECT-X-DT-EFFETTO-MAX
                   PERFORM W300-WH-SELECT-DT-EFFETTO-MAX THRU W300-EX
              WHEN SELECT-X-DT-EFFETTO-MIN
                   PERFORM W400-WH-SELECT-DT-EFFETTO-MIN THRU W400-EX
              WHEN SELECT-X-DT-INSERIM-MAX
                   PERFORM W500-WH-SELECT-DT-INSERIM-MAX THRU W500-EX
              WHEN SELECT-X-DT-INSERIM-MIN
                   PERFORM W600-WH-SELECT-DT-INSERIM-MIN THRU W600-EX
              WHEN SELECT-ALL-BATCH
                   PERFORM W700-WH-ELABORA-ALL-BATCH     THRU W700-EX
              WHEN OTHER
                 CONTINUE

           END-EVALUATE.

       W001-01-EX.
           EXIT.

      ******************************************************************
      * WHERE CONDITION 02
      ******************************************************************
       W002-WHERE-CONDITION-02.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE BTC_BATCH SET
                   COD_BATCH_STATE = :IABV0002-STATE-CURRENT

                WHERE   ID_RICH    = :BTC-ID-RICH

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO BTC-COD-BATCH-STATE
           END-IF.

       W002-02-EX.
           EXIT.
      ******************************************************************
      * WHERE CONDITION - SELECT X TIPOLOGIA BATCH
      ******************************************************************
       W100-WH-SELECT-TP-BATCH.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
              SELECT
                ID_BATCH
                ,PROTOCOL
                ,DT_INS
                ,USER_INS
                ,COD_BATCH_TYPE
                ,COD_COMP_ANIA
                ,COD_BATCH_STATE
                ,DT_START
                ,DT_END
                ,USER_START
                ,ID_RICH
                ,DT_EFF
                ,DATA_BATCH
             INTO
                :BTC-ID-BATCH
               ,:BTC-PROTOCOL
               ,:BTC-DT-INS-DB
               ,:BTC-USER-INS
               ,:BTC-COD-BATCH-TYPE
               ,:BTC-COD-COMP-ANIA
               ,:BTC-COD-BATCH-STATE
               ,:BTC-DT-START-DB
                :IND-BTC-DT-START
               ,:BTC-DT-END-DB
                :IND-BTC-DT-END
               ,:BTC-USER-START
                :IND-BTC-USER-START
               ,:BTC-ID-RICH
                :IND-BTC-ID-RICH
               ,:BTC-DT-EFF-DB
                :IND-BTC-DT-EFF
               ,:BTC-DATA-BATCH-VCHAR
                :IND-BTC-DATA-BATCH
              FROM BTC_BATCH
              WHERE COD_BATCH_TYPE = :BTC-COD-BATCH-TYPE
                AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
                AND COD_BATCH_STATE IN  (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                        )
              ORDER BY DT_INS DESC
              FETCH FIRST ROW ONLY

           END-EXEC.

            PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       W100-EX.
           EXIT.

      ******************************************************************
      * WHERE CONDITION - SELECT X TIPOLOGIA BATCH E DATA EFFETTO
      ******************************************************************
       W200-WH-SELECT-DT-EFFETTO.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           EXEC SQL
              SELECT
                ID_BATCH
                ,PROTOCOL
                ,DT_INS
                ,USER_INS
                ,COD_BATCH_TYPE
                ,COD_COMP_ANIA
                ,COD_BATCH_STATE
                ,DT_START
                ,DT_END
                ,USER_START
                ,ID_RICH
                ,DT_EFF
                ,DATA_BATCH
             INTO
                :BTC-ID-BATCH
               ,:BTC-PROTOCOL
               ,:BTC-DT-INS-DB
               ,:BTC-USER-INS
               ,:BTC-COD-BATCH-TYPE
               ,:BTC-COD-COMP-ANIA
               ,:BTC-COD-BATCH-STATE
               ,:BTC-DT-START-DB
                :IND-BTC-DT-START
               ,:BTC-DT-END-DB
                :IND-BTC-DT-END
               ,:BTC-USER-START
                :IND-BTC-USER-START
               ,:BTC-ID-RICH
                :IND-BTC-ID-RICH
               ,:BTC-DT-EFF-DB
                :IND-BTC-DT-EFF
               ,:BTC-DATA-BATCH-VCHAR
                :IND-BTC-DATA-BATCH
              FROM BTC_BATCH
              WHERE COD_BATCH_TYPE = :BTC-COD-BATCH-TYPE
                AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
                AND DT_EFF         = :BTC-DT-EFF-DB
                AND COD_BATCH_STATE IN  (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                        )
              ORDER BY DT_INS DESC
              FETCH FIRST ROW ONLY

           END-EXEC.

            PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       W200-EX.
           EXIT.
      ******************************************************************
      * WHERE CONDITION - SELECT X TIPOLOGIA BATCH E DATA EFFETTO MAX
      ******************************************************************
       W300-WH-SELECT-DT-EFFETTO-MAX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           EXEC SQL
              SELECT
                ID_BATCH
                ,PROTOCOL
                ,DT_INS
                ,USER_INS
                ,COD_BATCH_TYPE
                ,COD_COMP_ANIA
                ,COD_BATCH_STATE
                ,DT_START
                ,DT_END
                ,USER_START
                ,ID_RICH
                ,DT_EFF
                ,DATA_BATCH
             INTO
                :BTC-ID-BATCH
               ,:BTC-PROTOCOL
               ,:BTC-DT-INS-DB
               ,:BTC-USER-INS
               ,:BTC-COD-BATCH-TYPE
               ,:BTC-COD-COMP-ANIA
               ,:BTC-COD-BATCH-STATE
               ,:BTC-DT-START-DB
                :IND-BTC-DT-START
               ,:BTC-DT-END-DB
                :IND-BTC-DT-END
               ,:BTC-USER-START
                :IND-BTC-USER-START
               ,:BTC-ID-RICH
                :IND-BTC-ID-RICH
               ,:BTC-DT-EFF-DB
                :IND-BTC-DT-EFF
               ,:BTC-DATA-BATCH-VCHAR
                :IND-BTC-DATA-BATCH
              FROM BTC_BATCH
              WHERE COD_BATCH_TYPE = :BTC-COD-BATCH-TYPE
                AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
                AND COD_BATCH_STATE IN  (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                        )
              ORDER BY DT_EFF DESC
              FETCH FIRST ROW ONLY

           END-EXEC.

            PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       W300-EX.
           EXIT.
      ******************************************************************
      * WHERE CONDITION - SELECT X TIPOLOGIA BATCH E DATA EFFETTO MIN
      ******************************************************************
       W400-WH-SELECT-DT-EFFETTO-MIN.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           EXEC SQL
              SELECT
                ID_BATCH
                ,PROTOCOL
                ,DT_INS
                ,USER_INS
                ,COD_BATCH_TYPE
                ,COD_COMP_ANIA
                ,COD_BATCH_STATE
                ,DT_START
                ,DT_END
                ,USER_START
                ,ID_RICH
                ,DT_EFF
                ,DATA_BATCH
             INTO
                :BTC-ID-BATCH
               ,:BTC-PROTOCOL
               ,:BTC-DT-INS-DB
               ,:BTC-USER-INS
               ,:BTC-COD-BATCH-TYPE
               ,:BTC-COD-COMP-ANIA
               ,:BTC-COD-BATCH-STATE
               ,:BTC-DT-START-DB
                :IND-BTC-DT-START
               ,:BTC-DT-END-DB
                :IND-BTC-DT-END
               ,:BTC-USER-START
                :IND-BTC-USER-START
               ,:BTC-ID-RICH
                :IND-BTC-ID-RICH
               ,:BTC-DT-EFF-DB
                :IND-BTC-DT-EFF
               ,:BTC-DATA-BATCH-VCHAR
                :IND-BTC-DATA-BATCH
              FROM BTC_BATCH
              WHERE COD_BATCH_TYPE = :BTC-COD-BATCH-TYPE
                AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
                AND COD_BATCH_STATE IN  (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                        )
              ORDER BY DT_EFF
              FETCH FIRST ROW ONLY

           END-EXEC.

            PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       W400-EX.
           EXIT.
      ******************************************************************
      * WHERE CONDITION - SELECT X TIPOLOGIA BATCH E DATA INSERIM. MAX
      ******************************************************************
       W500-WH-SELECT-DT-INSERIM-MAX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           EXEC SQL
              SELECT
                ID_BATCH
                ,PROTOCOL
                ,DT_INS
                ,USER_INS
                ,COD_BATCH_TYPE
                ,COD_COMP_ANIA
                ,COD_BATCH_STATE
                ,DT_START
                ,DT_END
                ,USER_START
                ,ID_RICH
                ,DT_EFF
                ,DATA_BATCH
             INTO
                :BTC-ID-BATCH
               ,:BTC-PROTOCOL
               ,:BTC-DT-INS-DB
               ,:BTC-USER-INS
               ,:BTC-COD-BATCH-TYPE
               ,:BTC-COD-COMP-ANIA
               ,:BTC-COD-BATCH-STATE
               ,:BTC-DT-START-DB
                :IND-BTC-DT-START
               ,:BTC-DT-END-DB
                :IND-BTC-DT-END
               ,:BTC-USER-START
                :IND-BTC-USER-START
               ,:BTC-ID-RICH
                :IND-BTC-ID-RICH
               ,:BTC-DT-EFF-DB
                :IND-BTC-DT-EFF
               ,:BTC-DATA-BATCH-VCHAR
                :IND-BTC-DATA-BATCH
              FROM BTC_BATCH
              WHERE COD_BATCH_TYPE = :BTC-COD-BATCH-TYPE
                AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
                AND COD_BATCH_STATE IN  (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                        )
              ORDER BY DT_INS DESC
              FETCH FIRST ROW ONLY

           END-EXEC.

            PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       W500-EX.
           EXIT.
      ******************************************************************
      * WHERE CONDITION - SELECT X TIPOLOGIA BATCH E DATA INSERIM. MIN
      ******************************************************************
       W600-WH-SELECT-DT-INSERIM-MIN.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           EXEC SQL
              SELECT
                ID_BATCH
                ,PROTOCOL
                ,DT_INS
                ,USER_INS
                ,COD_BATCH_TYPE
                ,COD_COMP_ANIA
                ,COD_BATCH_STATE
                ,DT_START
                ,DT_END
                ,USER_START
                ,ID_RICH
                ,DT_EFF
                ,DATA_BATCH
             INTO
                :BTC-ID-BATCH
               ,:BTC-PROTOCOL
               ,:BTC-DT-INS-DB
               ,:BTC-USER-INS
               ,:BTC-COD-BATCH-TYPE
               ,:BTC-COD-COMP-ANIA
               ,:BTC-COD-BATCH-STATE
               ,:BTC-DT-START-DB
                :IND-BTC-DT-START
               ,:BTC-DT-END-DB
                :IND-BTC-DT-END
               ,:BTC-USER-START
                :IND-BTC-USER-START
               ,:BTC-ID-RICH
                :IND-BTC-ID-RICH
               ,:BTC-DT-EFF-DB
                :IND-BTC-DT-EFF
               ,:BTC-DATA-BATCH-VCHAR
                :IND-BTC-DATA-BATCH
              FROM BTC_BATCH
              WHERE COD_BATCH_TYPE = :BTC-COD-BATCH-TYPE
                AND COD_COMP_ANIA  = :BTC-COD-COMP-ANIA
                AND COD_BATCH_STATE IN  (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                        )
              ORDER BY DT_INS
              FETCH FIRST ROW ONLY

           END-EXEC.

            PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       W600-EX.
           EXIT.
      ******************************************************************
      * WHERE CONDITION - ELABORA X TIPOLOGIA BATCH
      ******************************************************************
       W700-WH-ELABORA-ALL-BATCH.

           EVALUATE TRUE
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST            THRU A380-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT             THRU A390-EX
              WHEN IDSV0003-INSERT
                 PERFORM A320-INSERT                 THRU A320-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A330-UPDATE                 THRU A330-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.

       W700-EX.
           EXIT.
      ******************************************************************

       Z100-SET-COLONNE-NULL.

           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-BTC-DT-START = -1
              MOVE HIGH-VALUES TO BTC-DT-START-NULL
           END-IF
           IF IND-BTC-DT-END = -1
              MOVE HIGH-VALUES TO BTC-DT-END-NULL
           END-IF
           IF IND-BTC-USER-START = -1
              MOVE HIGH-VALUES TO BTC-USER-START-NULL
           END-IF
           IF IND-BTC-ID-RICH = -1
              MOVE HIGH-VALUES TO BTC-ID-RICH-NULL
           END-IF
           IF IND-BTC-DATA-BATCH = -1
              MOVE HIGH-VALUES TO BTC-DATA-BATCH
           END-IF.
           IF IND-BTC-DT-EFF = -1
              MOVE HIGH-VALUES TO BTC-DT-EFF-NULL
           END-IF.

       Z100-EX.
           EXIT.

      ******************************************************************

       Z150-VALORIZZA-DATA-SERVICES.

           CONTINUE.
       Z150-EX.
           EXIT.


      ******************************************************************
       Z200-SET-INDICATORI-NULL.

           IF BTC-DT-START-NULL = HIGH-VALUES
              MOVE -1 TO IND-BTC-DT-START
           ELSE
              MOVE 0 TO IND-BTC-DT-START
           END-IF
           IF BTC-DT-END-NULL = HIGH-VALUES
              MOVE -1 TO IND-BTC-DT-END
           ELSE
              MOVE 0 TO IND-BTC-DT-END
           END-IF
           IF BTC-USER-START-NULL = HIGH-VALUES
              MOVE -1 TO IND-BTC-USER-START
           ELSE
              MOVE 0 TO IND-BTC-USER-START
           END-IF
           IF BTC-ID-RICH-NULL = HIGH-VALUES
              MOVE -1 TO IND-BTC-ID-RICH
           ELSE
              MOVE 0 TO IND-BTC-ID-RICH
           END-IF
           IF BTC-DATA-BATCH = HIGH-VALUES
              MOVE -1 TO IND-BTC-DATA-BATCH
           ELSE
              MOVE 0 TO IND-BTC-DATA-BATCH
           END-IF.
           IF BTC-DT-EFF-NULL = HIGH-VALUES
              MOVE -1 TO IND-BTC-DT-EFF
           ELSE
              MOVE 0 TO IND-BTC-DT-EFF
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ.

           EXEC SQL
              VALUES NEXTVAL FOR SEQ_ID_BATCH
              INTO :BTC-ID-BATCH
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.


       Z400-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE BTC-DT-INS TO WS-TIMESTAMP-N
           PERFORM Z701-TS-N-TO-X   THRU Z701-EX
           MOVE WS-TIMESTAMP-X   TO BTC-DT-INS-DB

           IF IND-BTC-DT-START = 0
               MOVE BTC-DT-START TO WS-TIMESTAMP-N
               PERFORM Z701-TS-N-TO-X   THRU Z701-EX
               MOVE WS-TIMESTAMP-X      TO BTC-DT-START-DB
           END-IF

           IF IND-BTC-DT-END = 0
               MOVE BTC-DT-END TO WS-TIMESTAMP-N
               PERFORM Z701-TS-N-TO-X   THRU Z701-EX
               MOVE WS-TIMESTAMP-X      TO BTC-DT-END-DB
           END-IF.

           IF IND-BTC-DT-EFF = 0
               MOVE BTC-DT-EFF     TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO BTC-DT-EFF-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE BTC-DT-INS-DB TO WS-TIMESTAMP-X
           PERFORM Z801-TS-X-TO-N     THRU Z801-EX
           MOVE WS-TIMESTAMP-N      TO BTC-DT-INS

           IF IND-BTC-DT-START = 0
               MOVE BTC-DT-START-DB TO WS-TIMESTAMP-X
               PERFORM Z801-TS-X-TO-N     THRU Z801-EX
               MOVE WS-TIMESTAMP-N      TO BTC-DT-START
           END-IF

           IF IND-BTC-DT-END = 0
               MOVE BTC-DT-END-DB TO WS-TIMESTAMP-X
               PERFORM Z801-TS-X-TO-N     THRU Z801-EX
               MOVE WS-TIMESTAMP-N      TO BTC-DT-END
           END-IF.

           IF IND-BTC-DT-EFF = 0
               MOVE BTC-DT-EFF-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO BTC-DT-EFF
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           MOVE LENGTH OF BTC-DATA-BATCH
                       TO BTC-DATA-BATCH-LEN.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

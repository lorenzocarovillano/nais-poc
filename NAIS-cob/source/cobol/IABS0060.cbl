       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IABS0060 IS INITIAL.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : SERVIZIO ESTRAZIONE OCCORRENZE              *
      *                   DA TABELLE GUIDA RICHIAMATO DAL BATCH EXEC. *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2       PIC X(300)      VALUE SPACES.


       01 FLAG-ACCESSO                PIC X(01).
          88 ACCESSO-STD              VALUE '1'.
          88 ACCESSO-MCRFNCT          VALUE '2'.
          88 ACCESSO-TPMV             VALUE '3'.
          88 ACCESSO-MCRFNCT-TPMV     VALUE '4'.
          88 ACCESSO-IB-OGG           VALUE '5'.

       01 FLAG-ACCESSO-X-RANGE        PIC X(01).
          88 ACCESSO-X-RANGE-SI       VALUE 'S'.
          88 ACCESSO-X-RANGE-NO       VALUE 'N'.

           EXEC SQL INCLUDE IDSV0010 END-EXEC.

       01 WK-ID-JOB-X                 PIC X(09).
       01 WK-ID-JOB-N REDEFINES WK-ID-JOB-X PIC 9(09).

      *---------------------------------------------------------------*
      * STRUTTURA WHERE CONDITION
      *---------------------------------------------------------------*
       01 WS-WHERE-CONDITION.
          05 FILLER                  PIC X(300).


      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDBJS0 END-EXEC.

           EXEC SQL INCLUDE IDBVBJS2 END-EXEC.

           EXEC SQL INCLUDE IDBVBJS3 END-EXEC.
      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IABV0002 END-EXEC.

           EXEC SQL INCLUDE IDBVBJS1 END-EXEC.

      *****************************************************************
       PROCEDURE DIVISION USING IDSV0003 IABV0002 BTC-JOB-SCHEDULE.

           PERFORM A000-INIZIO                 THRU A000-EX.

           PERFORM A060-CNTL-INPUT             THRU A060-EX.

           PERFORM A300-ELABORA                THRU A300-EX.

           PERFORM A400-FINE                   THRU A400-EX.


           GOBACK.

      ******************************************************************
       A000-INIZIO.

           MOVE 'IABS0060'               TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'BTC_JOB_SCHEDULE'       TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO IDSV0003-SQLCODE
                                            IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO IDSV0003-DESCRIZ-ERR-DB2
                                            IDSV0003-KEY-TABELLA
                                            FLAG-ACCESSO.

           SET ACCESSO-X-RANGE-NO        TO TRUE.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

           MOVE IDSV0003-BUFFER-WHERE-COND TO FLAG-ACCESSO.

       A000-EX.
           EXIT.
      ******************************************************************
       A060-CNTL-INPUT.



           IF NOT ACCESSO-IB-OGG

              SET ACCESSO-STD              TO TRUE

              IF BJS-COD-MACROFUNCT NOT = SPACES     AND
                 BJS-COD-MACROFUNCT NOT = LOW-VALUE  AND
                 BJS-COD-MACROFUNCT NOT = HIGH-VALUE
                 SET ACCESSO-MCRFNCT          TO TRUE
              END-IF

              IF BJS-TP-MOVI-NULL IS NUMERIC   AND
                 BJS-TP-MOVI      NOT = ZERO
                 IF ACCESSO-MCRFNCT
                    SET ACCESSO-MCRFNCT-TPMV  TO TRUE
                 ELSE
                    SET ACCESSO-TPMV          TO TRUE
                 END-IF
              END-IF

              SET ACCESSO-X-RANGE-NO          TO TRUE
              IF IABV0009-ID-OGG-DA IS NUMERIC AND
                 IABV0009-ID-OGG-DA NOT = ZEROES

                 IF IABV0009-ID-OGG-A  IS NUMERIC AND
                    IABV0009-ID-OGG-A  NOT = ZEROES

                    SET ACCESSO-X-RANGE-SI    TO TRUE
                    SET ACCESSO-STD           TO TRUE

                 END-IF
              END-IF

           END-IF.

       A060-EX.
           EXIT.
      ******************************************************************
       A100-CHECK-RETURN-CODE.

           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-SELECT         OR
                        IDSV0003-FETCH-FIRST    OR
                        IDSV0003-FETCH-NEXT     OR
                        IDSV0003-UPDATE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE
           END-IF.

       A100-EX.
           EXIT.
      ******************************************************************
       A300-ELABORA.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT                 THRU A310-EX
              WHEN IDSV0003-INSERT
                 PERFORM A318-INSERT                 THRU A318-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR            THRU A360-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR           THRU A370-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST            THRU A380-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT             THRU A390-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE                 THRU A320-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.

       A300-EX.
           EXIT.
      ******************************************************************
       A400-FINE.

           CONTINUE.

       A400-EX.
           EXIT.

      ******************************************************************
       A310-SELECT.

           IF ACCESSO-X-RANGE-NO

              EVALUATE TRUE
                 WHEN ACCESSO-STD
                    PERFORM A311-SELECT-STD          THRU A311-EX

                 WHEN ACCESSO-MCRFNCT
                    PERFORM A312-SELECT-MCRFNCT      THRU A312-EX

                 WHEN ACCESSO-TPMV
                    PERFORM A313-SELECT-TPMV         THRU A313-EX

                 WHEN ACCESSO-MCRFNCT-TPMV
                    PERFORM A314-SELECT-MCRFNCT-TPMV THRU A314-EX

                 WHEN ACCESSO-IB-OGG
                    PERFORM A315-SELECT-IB-OGG       THRU A315-EX

              END-EVALUATE
           ELSE
              EVALUATE TRUE
                 WHEN ACCESSO-STD
                    PERFORM R311-SELECT-STD-X-RNG     THRU R311-EX

                 WHEN OTHER
                    SET IDSV0003-GENERIC-ERROR        TO TRUE
                    MOVE 'TIPO DI ACCESSO NON VALIDO X MODALITA'' RANGE'
                                        TO IDSV0003-DESCRIZ-ERR-DB2

              END-EVALUATE

           END-IF.

       A310-EX.
           EXIT.

      ******************************************************************
       A311-SELECT-STD.

           EXEC SQL
             SELECT
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA
           INTO
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA

              FROM BTC_JOB_SCHEDULE
              WHERE     ID_BATCH                 = :BJS-ID-BATCH
                    AND
                    (
                          COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )

              FETCH FIRST ROW ONLY

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       A311-EX.
           EXIT.
      ******************************************************************
       A312-SELECT-MCRFNCT.

           EXEC SQL
             SELECT
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA
           INTO
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA

              FROM BTC_JOB_SCHEDULE
              WHERE     ID_BATCH                 = :BJS-ID-BATCH
                    AND COD_MACROFUNCT           = :BJS-COD-MACROFUNCT
                    AND
                    (
                          COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )


              FETCH FIRST ROW ONLY

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX


           END-IF.

       A312-EX.
           EXIT.
      ******************************************************************
       A313-SELECT-TPMV.

           EXEC SQL
             SELECT
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA
           INTO
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA

              FROM BTC_JOB_SCHEDULE
              WHERE     ID_BATCH                 = :BJS-ID-BATCH
                    AND TP_MOVI                  = :BJS-TP-MOVI
                    AND
                    (
                          COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )


              FETCH FIRST ROW ONLY

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       A313-EX.
           EXIT.
      ******************************************************************
       A314-SELECT-MCRFNCT-TPMV.

           EXEC SQL
             SELECT
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA
           INTO
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA

              FROM BTC_JOB_SCHEDULE
              WHERE     ID_BATCH                 = :BJS-ID-BATCH
                    AND COD_MACROFUNCT           = :BJS-COD-MACROFUNCT
                    AND TP_MOVI                  = :BJS-TP-MOVI
                    AND
                    (
                          COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )


              FETCH FIRST ROW ONLY

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       A314-EX.
           EXIT.

      ******************************************************************
       A315-SELECT-IB-OGG.

           EXEC SQL
             SELECT
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA
           INTO
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA

              FROM BTC_JOB_SCHEDULE
              WHERE ID_BATCH                 = :BJS-ID-BATCH AND
                    IB_OGG                   = :BJS-IB-OGG   AND
                    (
                          COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )


           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       A315-EX.
           EXIT.

      ******************************************************************
       A318-INSERT.

           MOVE BJS-IB-OGG(41:9) TO WK-ID-JOB-X
           MOVE SPACES           TO BJS-IB-OGG(41:9)
           IF  WK-ID-JOB-N IS NUMERIC
           AND WK-ID-JOB-N > ZERO
              MOVE WK-ID-JOB-N        TO BJS-ID-JOB
           ELSE
              PERFORM Z400-SEQ                       THRU Z400-EX
           END-IF.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO BTC_JOB_SCHEDULE
                     (
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA
                     )
                 VALUES
                     (
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.

       A318-EX.
           EXIT.
      ******************************************************************
       A320-UPDATE.

           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
                   PERFORM A330-UPDATE-PK           THRU A330-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A340-UPDATE-FIRST-ACTION THRU A340-EX
              WHEN IDSV0003-WHERE-CONDITION
                   PERFORM A350-UPDATE-WHERE-COND   THRU A350-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
           END-EVALUATE.

       A320-EX.
           EXIT.
      ******************************************************************
       A330-UPDATE-PK.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE BTC_JOB_SCHEDULE SET
                   COD_ELAB_STATE         = :IABV0002-STATE-CURRENT
                  ,EXECUTIONS_COUNT       = :BJS-EXECUTIONS-COUNT
                                            :IND-BJS-EXECUTIONS-COUNT
                  ,FLAG_WARNINGS          = :BJS-FLAG-WARNINGS
                                            :IND-BJS-FLAG-WARNINGS
                  ,DT_START               = :BJS-DT-START-DB
                                            :IND-BJS-DT-START
                  ,DT_END                 = :BJS-DT-END-DB
                                            :IND-BJS-DT-END
                  ,USER_START             = :BJS-USER-START
                                            :IND-BJS-USER-START

                WHERE   ID_BATCH = :BJS-ID-BATCH
                  AND   ID_JOB   = :BJS-ID-JOB

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
           END-IF.

       A330-EX.
           EXIT.

      ******************************************************************
       A340-UPDATE-FIRST-ACTION.

           EVALUATE TRUE
              WHEN ACCESSO-STD
                 PERFORM A341-UPDATE-F-ACT-STD          THRU A341-EX

              WHEN ACCESSO-MCRFNCT
                 PERFORM A342-UPDATE-F-ACT-MCRFNCT      THRU A342-EX

              WHEN ACCESSO-TPMV
                 PERFORM A343-UPDATE-F-ACT-TPMV         THRU A343-EX

              WHEN ACCESSO-MCRFNCT-TPMV
                 PERFORM A344-UPDATE-F-ACT-MCRFNCT-TPMV THRU A344-EX

           END-EVALUATE.

       A340-EX.
           EXIT.
      ******************************************************************
       A341-UPDATE-F-ACT-STD.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE BTC_JOB_SCHEDULE SET
                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
              WHERE     ID_BATCH       = :BJS-ID-BATCH
                    AND (
                         COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
           END-IF.

       A341-EX.
           EXIT.
      ******************************************************************
       A342-UPDATE-F-ACT-MCRFNCT.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE BTC_JOB_SCHEDULE SET
                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
              WHERE     ID_BATCH       = :BJS-ID-BATCH
                    AND COD_MACROFUNCT = :BJS-COD-MACROFUNCT
                    AND (
                         COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
           END-IF.

       A342-EX.

           EXIT.
      ******************************************************************
       A343-UPDATE-F-ACT-TPMV.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE BTC_JOB_SCHEDULE SET
                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
              WHERE     ID_BATCH       = :BJS-ID-BATCH
                    AND TP_MOVI        = :BJS-TP-MOVI
                    AND (
                         COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
           END-IF.

       A343-EX.
           EXIT.
      ******************************************************************
       A344-UPDATE-F-ACT-MCRFNCT-TPMV.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE BTC_JOB_SCHEDULE SET
                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
              WHERE     ID_BATCH       = :BJS-ID-BATCH
                    AND COD_MACROFUNCT = :BJS-COD-MACROFUNCT
                    AND TP_MOVI        = :BJS-TP-MOVI
                    AND (
                         COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
           END-IF.

       A344-EX.
           EXIT.
      ******************************************************************
       A350-UPDATE-WHERE-COND.

           IF ACCESSO-X-RANGE-NO

              EVALUATE TRUE
                 WHEN ACCESSO-STD
                    PERFORM A351-UPD-WH-COND-STD          THRU A351-EX

                 WHEN ACCESSO-MCRFNCT
                    PERFORM A352-UPD-WH-COND-MCRFNCT      THRU A352-EX

                 WHEN ACCESSO-TPMV
                    PERFORM A353-UPD-WH-COND-TPMV         THRU A353-EX

                 WHEN ACCESSO-MCRFNCT-TPMV
                    PERFORM A354-UPD-WH-COND-MCRFNCT-TPMV THRU A354-EX

              END-EVALUATE
           ELSE
              EVALUATE TRUE
                 WHEN ACCESSO-STD
                    PERFORM U351-UPD-WH-COND-STD-X-RNG    THRU U351-EX

                 WHEN OTHER
                    SET IDSV0003-GENERIC-ERROR        TO TRUE
                    MOVE 'TIPO DI ACCESSO NON VALIDO X MODALITA'' RANGE'
                                        TO IDSV0003-DESCRIZ-ERR-DB2

              END-EVALUATE

           END-IF.

       A350-EX.
           EXIT.
      ******************************************************************
       A351-UPD-WH-COND-STD.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE BTC_JOB_SCHEDULE SET
                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
              WHERE     ID_BATCH       = :BJS-ID-BATCH
                    AND COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                          )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
           END-IF.

       A351-EX.
           EXIT.
      ******************************************************************
       A352-UPD-WH-COND-MCRFNCT.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE BTC_JOB_SCHEDULE SET
                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
              WHERE     ID_BATCH       = :BJS-ID-BATCH
                    AND COD_MACROFUNCT = :BJS-COD-MACROFUNCT
                    AND COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                          )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
           END-IF.

       A352-EX.
           EXIT.
      ******************************************************************
       A353-UPD-WH-COND-TPMV.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE BTC_JOB_SCHEDULE SET
                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
              WHERE     ID_BATCH       = :BJS-ID-BATCH
                    AND TP_MOVI        = :BJS-TP-MOVI
                    AND COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                          )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
           END-IF.

       A353-EX.
           EXIT.
      ******************************************************************
       A354-UPD-WH-COND-MCRFNCT-TPMV.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE BTC_JOB_SCHEDULE SET
                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
              WHERE     ID_BATCH       = :BJS-ID-BATCH
                    AND COD_MACROFUNCT = :BJS-COD-MACROFUNCT
                    AND TP_MOVI        = :BJS-TP-MOVI
                    AND COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                          )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
           END-IF.

       A354-EX.
           EXIT.
      ******************************************************************
       A360-OPEN-CURSOR.

           EVALUATE TRUE
              WHEN IABV0002-ORDER-BY-ID-JOB
                   PERFORM D100-DECLARE-CURSOR-IDJ THRU D100-EX

              WHEN IABV0002-ORDER-BY-IB-OGG
                   PERFORM D600-DECLARE-CURSOR-IBO THRU D600-EX

              WHEN OTHER
                   SET IDSV0003-INVALID-OPER       TO TRUE

           END-EVALUATE.

           IF IDSV0003-SUCCESSFUL-RC
              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
           END-IF.

       A360-EX.
           EXIT.

      ******************************************************************
       A370-CLOSE-CURSOR.

           EVALUATE TRUE
              WHEN IABV0002-ORDER-BY-ID-JOB
                   PERFORM C100-CLOSE-IDJ THRU C100-EX

              WHEN IABV0002-ORDER-BY-IB-OGG
                   PERFORM C600-CLOSE-IBO THRU C600-EX

              WHEN OTHER
                   SET IDSV0003-INVALID-OPER       TO TRUE

           END-EVALUATE.

           IF IDSV0003-SUCCESSFUL-RC
              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
           END-IF.

       A370-EX.
           EXIT.
      ******************************************************************
       A380-FETCH-FIRST.

           PERFORM A360-OPEN-CURSOR    THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-RC
              PERFORM A390-FETCH-NEXT THRU A390-EX
           END-IF.

       A380-EX.
           EXIT.
      ******************************************************************
       A390-FETCH-NEXT.

           EVALUATE TRUE
              WHEN IABV0002-ORDER-BY-ID-JOB
                   PERFORM F100-FET-NEXT-IDJ     THRU F100-EX

              WHEN IABV0002-ORDER-BY-IB-OGG
                   PERFORM F600-FET-NEXT-IBO     THRU F600-EX

              WHEN OTHER
                   SET IDSV0003-INVALID-OPER     TO TRUE
           END-EVALUATE.

       A390-EX.
           EXIT.


      ******************************************************************
       D101-DCL-CUR-IDJ-STD.

           EXEC SQL
              DECLARE CUR-BJS-IDJ-STD CURSOR WITH HOLD FOR
              SELECT
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA

              FROM BTC_JOB_SCHEDULE
              WHERE     ID_BATCH              =  :BJS-ID-BATCH
                    AND
                    (
                          COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )              ORDER BY ID_JOB

           END-EXEC.

           CONTINUE.

       D101-EX.
           EXIT.
      ******************************************************************
       D102-DCL-CUR-IDJ-MCRFNCT.

           EXEC SQL
              DECLARE CUR-BJS-IDJ-MF CURSOR WITH HOLD FOR
              SELECT
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA

              FROM BTC_JOB_SCHEDULE
              WHERE     ID_BATCH              =  :BJS-ID-BATCH
                    AND COD_MACROFUNCT        =  :BJS-COD-MACROFUNCT
                    AND
                    (
                          COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )              ORDER BY ID_JOB

           END-EXEC.

           CONTINUE.

       D102-EX.
           EXIT.
      ******************************************************************
       D103-DCL-CUR-IDJ-TPMV.

           EXEC SQL
              DECLARE CUR-BJS-IDJ-TM CURSOR WITH HOLD FOR
              SELECT
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA

              FROM BTC_JOB_SCHEDULE
              WHERE     ID_BATCH              =  :BJS-ID-BATCH
                    AND TP_MOVI               =  :BJS-TP-MOVI
                    AND
                    (
                          COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )              ORDER BY ID_JOB

           END-EXEC.

           CONTINUE.

       D103-EX.
           EXIT.
      ******************************************************************
       D104-DCL-CUR-IDJ-MCRFNCT-TPMV.

           EXEC SQL
              DECLARE CUR-BJS-IDJ-MF-TM CURSOR WITH HOLD FOR
              SELECT
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA

              FROM BTC_JOB_SCHEDULE
              WHERE     ID_BATCH              =  :BJS-ID-BATCH
                    AND COD_MACROFUNCT        =  :BJS-COD-MACROFUNCT
                    AND TP_MOVI               =  :BJS-TP-MOVI
                    AND
                    (
                          COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )              ORDER BY ID_JOB

           END-EXEC.

           CONTINUE.

       D104-EX.
           EXIT.


      ******************************************************************
       D601-DCL-CUR-IBO-STD.

           EXEC SQL
              DECLARE CUR-BJS-IBO-STD CURSOR WITH HOLD FOR
              SELECT
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA

              FROM BTC_JOB_SCHEDULE
              WHERE     ID_BATCH               = :BJS-ID-BATCH
                   AND
                    (
                          COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )

              ORDER BY IB_OGG

           END-EXEC.

           CONTINUE.

       D601-EX.
           EXIT.
      ******************************************************************
       D602-DCL-CUR-IBO-MCRFNCT.

           EXEC SQL
              DECLARE CUR-BJS-IBO-MF CURSOR WITH HOLD FOR
              SELECT
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA

              FROM BTC_JOB_SCHEDULE
              WHERE     ID_BATCH               = :BJS-ID-BATCH
                   AND COD_MACROFUNCT          = :BJS-COD-MACROFUNCT
                   AND
                    (
                          COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )

              ORDER BY IB_OGG

           END-EXEC.

           CONTINUE.

       D602-EX.
           EXIT.
      ******************************************************************

       D603-DCL-CUR-IBO-TPMV.

           EXEC SQL
              DECLARE CUR-BJS-IBO-TM CURSOR WITH HOLD FOR
              SELECT
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA

              FROM BTC_JOB_SCHEDULE
              WHERE     ID_BATCH               = :BJS-ID-BATCH
                   AND TP_MOVI                 = :BJS-TP-MOVI
                   AND
                    (
                          COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )

              ORDER BY IB_OGG

           END-EXEC.

           CONTINUE.

       D603-EX.
           EXIT.
      ******************************************************************
       D604-DCL-CUR-IBO-MCRFNCT-TPMV.

           EXEC SQL
              DECLARE CUR-BJS-IBO-MF-TM CURSOR WITH HOLD FOR
              SELECT
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA

              FROM BTC_JOB_SCHEDULE
              WHERE     ID_BATCH               = :BJS-ID-BATCH
                   AND COD_MACROFUNCT          = :BJS-COD-MACROFUNCT
                   AND TP_MOVI                 = :BJS-TP-MOVI
                   AND
                    (
                          COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )

              ORDER BY IB_OGG

           END-EXEC.

           CONTINUE.

       D604-EX.
           EXIT.
      ******************************************************************
       F100-FET-NEXT-IDJ.

           IF ACCESSO-X-RANGE-NO

              EVALUATE TRUE
                 WHEN ACCESSO-STD
                    PERFORM F101-FET-NEXT-IDJ-STD          THRU F101-EX

                 WHEN ACCESSO-MCRFNCT
                    PERFORM F102-FET-NEXT-IDJ-MCRFNCT      THRU F102-EX

                 WHEN ACCESSO-TPMV
                    PERFORM F103-FET-NEXT-IDJ-TPMV         THRU F103-EX

                 WHEN ACCESSO-MCRFNCT-TPMV
                    PERFORM F104-FET-NEXT-IDJ-MCRFNCT-TPMV THRU F104-EX

              END-EVALUATE
           ELSE
              EVALUATE TRUE
                 WHEN ACCESSO-STD
                    PERFORM T101-FET-NEXT-IDJ-STD-X-RNG    THRU T101-EX

                 WHEN OTHER
                    SET IDSV0003-GENERIC-ERROR        TO TRUE
                    MOVE 'TIPO DI ACCESSO NON VALIDO X MODALITA'' RANGE'
                                        TO IDSV0003-DESCRIZ-ERR-DB2

              END-EVALUATE

           END-IF.

       F100-EX.
           EXIT.
      ******************************************************************
       F101-FET-NEXT-IDJ-STD.

           EXEC SQL
                FETCH CUR-BJS-IDJ-STD
           INTO
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX


           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       F101-EX.
           EXIT.
      **************************************************************
       F102-FET-NEXT-IDJ-MCRFNCT.

           EXEC SQL
                FETCH CUR-BJS-IDJ-MF
           INTO
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX


           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       F102-EX.
           EXIT.
      ******************************************************************
       F103-FET-NEXT-IDJ-TPMV.

           EXEC SQL
                FETCH CUR-BJS-IDJ-TM
           INTO
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX


           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       F103-EX.
           EXIT.
      ******************************************************************
       F104-FET-NEXT-IDJ-MCRFNCT-TPMV.

           EXEC SQL
                FETCH CUR-BJS-IDJ-MF-TM
           INTO
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX


           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       F104-EX.
           EXIT.
      ******************************************************************
       F600-FET-NEXT-IBO.

           EVALUATE TRUE
              WHEN ACCESSO-STD
                 PERFORM F601-FET-NEXT-IBO-STD          THRU F601-EX

              WHEN ACCESSO-MCRFNCT
                 PERFORM F602-FET-NEXT-IBO-MCRFNCT      THRU F602-EX

              WHEN ACCESSO-TPMV
                 PERFORM F603-FET-NEXT-IBO-TPMV         THRU F603-EX

              WHEN ACCESSO-MCRFNCT-TPMV
                 PERFORM F604-FET-NEXT-IBO-MCRFNCT-TPMV THRU F604-EX

           END-EVALUATE.

       F600-EX.
           EXIT.
      ******************************************************************
       F601-FET-NEXT-IBO-STD.

           EXEC SQL
                FETCH CUR-BJS-IBO-STD
           INTO
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX


           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       F601-EX.
           EXIT.
      ******************************************************************
       F602-FET-NEXT-IBO-MCRFNCT.

           EXEC SQL
                FETCH CUR-BJS-IBO-MF
           INTO
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX


           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       F602-EX.
           EXIT.
      ******************************************************************
       F603-FET-NEXT-IBO-TPMV.

           EXEC SQL
                FETCH CUR-BJS-IBO-TM
           INTO
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX


           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       F603-EX.
           EXIT.
      ******************************************************************
       F604-FET-NEXT-IBO-MCRFNCT-TPMV.

           EXEC SQL
                FETCH CUR-BJS-IBO-MF-TM
           INTO
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX


           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       F604-EX.
           EXIT.

      ******************************************************************
       R311-SELECT-STD-X-RNG.

           EXEC SQL
             SELECT
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA
           INTO
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA

              FROM BTC_JOB_SCHEDULE
              WHERE     ID_BATCH        = :BJS-ID-BATCH
                    AND
                        ID_JOB BETWEEN :IABV0009-ID-OGG-DA AND
                                       :IABV0009-ID-OGG-A
                    AND
                    (
                          COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )

              FETCH FIRST ROW ONLY

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       R311-EX.
           EXIT.
      ******************************************************************
       S101-DCL-IDJ-STD-X-RNG.

           EXEC SQL
              DECLARE CUR-IDJ-STD-X-RNG CURSOR WITH HOLD FOR
              SELECT
                ID_BATCH
                ,ID_JOB
                ,COD_ELAB_STATE
                ,FLAG_WARNINGS
                ,DT_START
                ,DT_END
                ,USER_START
                ,FLAG_ALWAYS_EXE
                ,EXECUTIONS_COUNT
                ,DATA_CONTENT_TYPE
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_OGG
                ,DATA

              FROM BTC_JOB_SCHEDULE
              WHERE     ID_BATCH              =  :BJS-ID-BATCH
                    AND
                        ID_JOB BETWEEN :IABV0009-ID-OGG-DA AND
                                       :IABV0009-ID-OGG-A
                    AND
                    (
                          COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                            )
                        )              ORDER BY ID_JOB

           END-EXEC.

           CONTINUE.

       S101-EX.
           EXIT.

      ******************************************************************
       T101-FET-NEXT-IDJ-STD-X-RNG.

           EXEC SQL
                FETCH CUR-IDJ-STD-X-RNG
           INTO
                :BJS-ID-BATCH
               ,:BJS-ID-JOB
               ,:BJS-COD-ELAB-STATE
               ,:BJS-FLAG-WARNINGS
                :IND-BJS-FLAG-WARNINGS
               ,:BJS-DT-START-DB
                :IND-BJS-DT-START
               ,:BJS-DT-END-DB
                :IND-BJS-DT-END
               ,:BJS-USER-START
                :IND-BJS-USER-START
               ,:BJS-FLAG-ALWAYS-EXE
                :IND-BJS-FLAG-ALWAYS-EXE
               ,:BJS-EXECUTIONS-COUNT
                :IND-BJS-EXECUTIONS-COUNT
               ,:BJS-DATA-CONTENT-TYPE
                :IND-BJS-DATA-CONTENT-TYPE
               ,:BJS-COD-MACROFUNCT
                :IND-BJS-COD-MACROFUNCT
               ,:BJS-TP-MOVI
                :IND-BJS-TP-MOVI
               ,:BJS-IB-OGG
                :IND-BJS-IB-OGG
               ,:BJS-DATA-VCHAR
                :IND-BJS-DATA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX


           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       T101-EX.
           EXIT.

      ******************************************************************
       U351-UPD-WH-COND-STD-X-RNG.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE BTC_JOB_SCHEDULE SET
                  COD_ELAB_STATE       = :IABV0002-STATE-CURRENT
              WHERE     ID_BATCH       = :BJS-ID-BATCH
                    AND
                      ID_JOB   BETWEEN :IABV0009-ID-OGG-DA AND
                                       :IABV0009-ID-OGG-A
                    AND
                     COD_ELAB_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                          )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO BJS-COD-ELAB-STATE
           END-IF.

       U351-EX.
           EXIT.
      ******************************************************************
       Z100-SET-COLONNE-NULL.

           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-BJS-FLAG-WARNINGS = -1
              MOVE HIGH-VALUES TO BJS-FLAG-WARNINGS-NULL
           END-IF
           IF IND-BJS-DT-START = -1
              MOVE HIGH-VALUES TO BJS-DT-START-NULL
           END-IF
           IF IND-BJS-DT-END = -1
              MOVE HIGH-VALUES TO BJS-DT-END-NULL
           END-IF
           IF IND-BJS-USER-START = -1
              MOVE HIGH-VALUES TO BJS-USER-START-NULL
           END-IF
           IF IND-BJS-FLAG-ALWAYS-EXE = -1
              MOVE HIGH-VALUES TO BJS-FLAG-ALWAYS-EXE-NULL
           END-IF
           IF IND-BJS-EXECUTIONS-COUNT = -1
              MOVE HIGH-VALUES TO BJS-EXECUTIONS-COUNT-NULL
           END-IF
           IF IND-BJS-DATA-CONTENT-TYPE = -1
              MOVE HIGH-VALUES TO BJS-DATA-CONTENT-TYPE-NULL
           END-IF
           IF IND-BJS-COD-MACROFUNCT = -1
              MOVE HIGH-VALUES TO BJS-COD-MACROFUNCT-NULL
           END-IF
           IF IND-BJS-TP-MOVI = -1
              MOVE HIGH-VALUES TO BJS-TP-MOVI-NULL
           END-IF
           IF IND-BJS-IB-OGG = -1
              MOVE HIGH-VALUES TO BJS-IB-OGG-NULL
           END-IF
           IF IND-BJS-DATA = -1
              MOVE HIGH-VALUES TO BJS-DATA
           END-IF.

       Z100-EX.
           EXIT.

      ******************************************************************

       Z150-VALORIZZA-DATA-SERVICES.

           CONTINUE.
       Z150-EX.
           EXIT.

      ******************************************************************
       Z200-SET-INDICATORI-NULL.


           IF BJS-FLAG-WARNINGS-NULL = HIGH-VALUES
              MOVE -1 TO IND-BJS-FLAG-WARNINGS
           ELSE
              MOVE 0 TO IND-BJS-FLAG-WARNINGS
           END-IF
           IF BJS-DT-START-NULL = HIGH-VALUES
              MOVE -1 TO IND-BJS-DT-START
           ELSE
              MOVE 0 TO IND-BJS-DT-START
           END-IF
           IF BJS-DT-END-NULL = HIGH-VALUES
              MOVE -1 TO IND-BJS-DT-END
           ELSE
              MOVE 0 TO IND-BJS-DT-END
           END-IF
           IF BJS-USER-START-NULL = HIGH-VALUES
              MOVE -1 TO IND-BJS-USER-START
           ELSE
              MOVE 0 TO IND-BJS-USER-START
           END-IF
           IF BJS-FLAG-ALWAYS-EXE-NULL = HIGH-VALUES
              MOVE -1 TO IND-BJS-FLAG-ALWAYS-EXE
           ELSE
              MOVE 0 TO IND-BJS-FLAG-ALWAYS-EXE
           END-IF
           IF BJS-EXECUTIONS-COUNT-NULL = HIGH-VALUES
              MOVE -1 TO IND-BJS-EXECUTIONS-COUNT
           ELSE
              MOVE 0 TO IND-BJS-EXECUTIONS-COUNT
           END-IF
           IF BJS-DATA-CONTENT-TYPE-NULL = HIGH-VALUES
              MOVE -1 TO IND-BJS-DATA-CONTENT-TYPE
           ELSE
              MOVE 0 TO IND-BJS-DATA-CONTENT-TYPE
           END-IF
           IF BJS-COD-MACROFUNCT-NULL = HIGH-VALUES
              MOVE -1 TO IND-BJS-COD-MACROFUNCT
           ELSE
              MOVE 0 TO IND-BJS-COD-MACROFUNCT
           END-IF
           IF BJS-TP-MOVI-NULL = HIGH-VALUES
              MOVE -1 TO IND-BJS-TP-MOVI
           ELSE
              MOVE 0 TO IND-BJS-TP-MOVI
           END-IF
           IF BJS-IB-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-BJS-IB-OGG
           ELSE
              MOVE 0 TO IND-BJS-IB-OGG
           END-IF
           IF BJS-DATA = HIGH-VALUES
              MOVE -1 TO IND-BJS-DATA
           ELSE
              MOVE 0 TO IND-BJS-DATA
           END-IF.

       Z200-EX.
           EXIT.


       Z400-SEQ.

           EXEC SQL
              VALUES NEXTVAL FOR SEQ_ID_JOB
              INTO :BJS-ID-JOB
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           IF IND-BJS-DT-START = 0
               MOVE BJS-DT-START TO WS-TIMESTAMP-N
               PERFORM Z701-TS-N-TO-X   THRU Z701-EX
               MOVE WS-TIMESTAMP-X      TO BJS-DT-START-DB
           END-IF
           IF IND-BJS-DT-END = 0
               MOVE BJS-DT-END TO WS-TIMESTAMP-N
               PERFORM Z701-TS-N-TO-X   THRU Z701-EX
               MOVE WS-TIMESTAMP-X      TO BJS-DT-END-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           IF IND-BJS-DT-START = 0
               MOVE BJS-DT-START-DB TO WS-TIMESTAMP-X
               PERFORM Z801-TS-X-TO-N     THRU Z801-EX
               MOVE WS-TIMESTAMP-N      TO BJS-DT-START
           END-IF
           IF IND-BJS-DT-END = 0
               MOVE BJS-DT-END-DB TO WS-TIMESTAMP-X
               PERFORM Z801-TS-X-TO-N     THRU Z801-EX
               MOVE WS-TIMESTAMP-N      TO BJS-DT-END
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           IF BJS-DATA-LEN NOT > ZEROES
              MOVE LENGTH OF BJS-DATA
                          TO BJS-DATA-LEN
           END-IF.

       Z960-EX.
           EXIT.
      ******************************************************************
       C100-CLOSE-IDJ.

           IF ACCESSO-X-RANGE-NO

              EVALUATE TRUE
                 WHEN ACCESSO-STD

                    EXEC SQL
                         CLOSE CUR-BJS-IDJ-STD
                    END-EXEC


                 WHEN ACCESSO-MCRFNCT

                    EXEC SQL
                         CLOSE CUR-BJS-IDJ-MF
                    END-EXEC

                 WHEN ACCESSO-TPMV

                    EXEC SQL
                         CLOSE CUR-BJS-IDJ-TM
                    END-EXEC

                 WHEN ACCESSO-MCRFNCT-TPMV

                    EXEC SQL
                         CLOSE CUR-BJS-IDJ-MF-TM
                    END-EXEC

              END-EVALUATE
           ELSE
              EVALUATE TRUE
                 WHEN ACCESSO-STD
                    EXEC SQL
                         CLOSE CUR-IDJ-STD-X-RNG
                    END-EXEC

                 WHEN OTHER
                    SET IDSV0003-GENERIC-ERROR        TO TRUE
                    MOVE 'TIPO DI ACCESSO NON VALIDO X MODALITA'' RANGE'
                                        TO IDSV0003-DESCRIZ-ERR-DB2

              END-EVALUATE

           END-IF.

       C100-EX.
           EXIT.
      ******************************************************************
       C600-CLOSE-IBO.

           EVALUATE TRUE
              WHEN ACCESSO-STD

                 EXEC SQL
                      CLOSE CUR-BJS-IBO-STD
                 END-EXEC

              WHEN ACCESSO-MCRFNCT

                 EXEC SQL
                      CLOSE CUR-BJS-IBO-MF
                 END-EXEC

              WHEN ACCESSO-TPMV

                 EXEC SQL
                      CLOSE CUR-BJS-IBO-TM
                 END-EXEC

              WHEN ACCESSO-MCRFNCT-TPMV

                 EXEC SQL
                      CLOSE CUR-BJS-IBO-MF-TM
                 END-EXEC

           END-EVALUATE.

       C600-EX.
           EXIT.
      ******************************************************************
       D100-DECLARE-CURSOR-IDJ.

           IF ACCESSO-X-RANGE-NO

              EVALUATE TRUE
                 WHEN ACCESSO-STD
                    PERFORM D101-DCL-CUR-IDJ-STD     THRU D101-EX

                    EXEC SQL
                         OPEN CUR-BJS-IDJ-STD
                    END-EXEC

                 WHEN ACCESSO-MCRFNCT
                    PERFORM D102-DCL-CUR-IDJ-MCRFNCT THRU D102-EX

                    EXEC SQL
                         OPEN CUR-BJS-IDJ-MF
                    END-EXEC

                 WHEN ACCESSO-TPMV
                    PERFORM D103-DCL-CUR-IDJ-TPMV    THRU D103-EX

                    EXEC SQL
                         OPEN CUR-BJS-IDJ-TM
                    END-EXEC

                 WHEN ACCESSO-MCRFNCT-TPMV
                    PERFORM D104-DCL-CUR-IDJ-MCRFNCT-TPMV
                                                     THRU D104-EX

                    EXEC SQL
                         OPEN CUR-BJS-IDJ-MF-TM
                    END-EXEC

              END-EVALUATE

           ELSE
              EVALUATE TRUE
                 WHEN ACCESSO-STD
                    PERFORM S101-DCL-IDJ-STD-X-RNG   THRU S101-EX

                    EXEC SQL
                         OPEN CUR-IDJ-STD-X-RNG
                    END-EXEC

                 WHEN OTHER
                    SET IDSV0003-GENERIC-ERROR        TO TRUE
                    MOVE 'TIPO DI ACCESSO NON VALIDO X MODALITA'' RANGE'
                                        TO IDSV0003-DESCRIZ-ERR-DB2

              END-EVALUATE

           END-IF.

       D100-EX.
           EXIT.
      ******************************************************************
       D600-DECLARE-CURSOR-IBO.

           EVALUATE TRUE
              WHEN ACCESSO-STD
                 PERFORM D601-DCL-CUR-IBO-STD          THRU D601-EX

                 EXEC SQL
                      OPEN CUR-BJS-IBO-STD
                 END-EXEC

              WHEN ACCESSO-MCRFNCT
                 PERFORM D602-DCL-CUR-IBO-MCRFNCT      THRU D602-EX

                 EXEC SQL
                      OPEN CUR-BJS-IBO-MF
                 END-EXEC

              WHEN ACCESSO-TPMV
                 PERFORM D603-DCL-CUR-IBO-TPMV         THRU D603-EX

                 EXEC SQL
                      OPEN CUR-BJS-IBO-TM
                 END-EXEC

              WHEN ACCESSO-MCRFNCT-TPMV
                 PERFORM D604-DCL-CUR-IBO-MCRFNCT-TPMV THRU D604-EX

                 EXEC SQL
                      OPEN CUR-BJS-IBO-MF-TM
                 END-EXEC

           END-EVALUATE.

       D600-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
----->     EXEC SQL INCLUDE IDSP0003 END-EXEC.

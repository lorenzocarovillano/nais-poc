      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS2310.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2012.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS2310
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... MODULO CALCOLO VARIABILE ISVISMEDICA1
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS2310'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
       01  WKS-NOME-TABELLA                 PIC X(030).

       01  WS-TP-DATO.
           05 WS-TASSO                      PIC X(01) VALUE 'A'.
           05 WS-IMPORTO                    PIC X(01) VALUE 'N'.
           05 WS-PERCENTUALE                PIC X(01) VALUE 'P'.
           05 WS-MILLESIMI                  PIC X(01) VALUE 'M'.
           05 WS-DATA                       PIC X(01) VALUE 'D'.
           05 WS-STRINGA                    PIC X(01) VALUE 'S'.
           05 WS-NUMERO                     PIC X(01) VALUE 'I'.
           05 WS-FLAG                       PIC X(01) VALUE 'F'.

       01  WS-TP-OGGETTO.
           05 WS-POLIZZA                    PIC X(02) VALUE 'PO'.
           05 WS-ADESIONE                   PIC X(02) VALUE 'AD'.
           05 WS-GARANZIA                   PIC X(02) VALUE 'GA'.
           05 WS-TRANCHE                    PIC X(02) VALUE 'TG'.
      *----------------------------------------------------------------*
      *    COPY DELLE TABELLE DB2
      *----------------------------------------------------------------*
      *--> PARAMETRO OGGETTO
           COPY IDBVPOG1.
      *--> DATI COLLETTIVA
           COPY IDBVDCO1.
      *--> ADESIONE
           COPY IDBVADE1.
      *--> GARANZIA
           COPY IDBVGRZ1.
      *--> PARAMETRO MOVIMENTO
           COPY IDBVPMO1.
      *--> TRANCHE DI GARANZIA
           COPY IDBVTGA1.
      ******************************************************************
      *    COPY DISPATCHER
      ******************************************************************
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      ** COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      ** COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *----------------------------------------------------------------*
      *--> AREA DATI COLLETTIVA
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *--> AREA PARAMETRO OGGETTO
      *----------------------------------------------------------------*
         02  AREA-IO-POG.
             03 DPOG-AREA-PARAM-OGG.
                04 DPOG-ELE-PARAM-OGG-MAX     PIC S9(04) COMP.
                COPY LCCVPOGB REPLACING   ==(SF)==  BY ==DPOG==.
                   COPY LCCVPOG1         REPLACING ==(SF)== BY ==DPOG==.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.
      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-TAB-PARAM-OGG               PIC S9(04) COMP.
           03 IX-TAB-DCO                     PIC S9(04) COMP.
           03 IX-TAB-ADE                     PIC S9(04) COMP.
           03 IX-TAB-GRZ                     PIC S9(04) COMP.
           03 IX-TAB-TGA                     PIC S9(04) COMP.
           03 IX-TAB-POG                     PIC S9(04) COMP.
           03 IX-TAB-PMO                     PIC S9(04) COMP.
           03 CONT-FETCH                     PIC S9(04) COMP.
           03 IX-DCLGEN                      PIC S9(04) COMP.
      *----------------------------------------------------------------*
      *    FLAG
      *----------------------------------------------------------------*
      *--> FLAG PER GESTIONE OVERFLOW LETTURA IN FETCH
       01  WCOM-FLAG-OVERFLOW                PIC X(002).
              88 WCOM-OVERFLOW-YES            VALUE 'SI'.
              88 WCOM-OVERFLOW-NO             VALUE 'NO'.

       01  WCOM-FLAG-TROVATO                 PIC X(001).
              88 WCOM-SI-TROVATO              VALUE 'S'.
              88 WCOM-NO-TROVATO              VALUE 'N'.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0001.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0001.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1000-ELABORAZIONE
                  THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                           IX-INDICI.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
           SET WCOM-NO-TROVATO               TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

           PERFORM S0005-CTRL-DATI-INPUT
                THRU EX-S0005.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLO DATI INPUT
      *----------------------------------------------------------------*
       S0005-CTRL-DATI-INPUT.

           IF IVVC0213-COD-PARAMETRO = SPACES OR LOW-VALUE
      *---    COD-PARAMETRO NON VALORIZZATO
              SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'COD-PARAMETRO NON VALORIZZATO'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
              IF IVVC0213-ID-ADESIONE > ZEROES AND
                 IVVC0213-ID-POLIZZA  = ZEROES
      *---       ID-POLIZZA NON VALORIZZATO
                 SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'ID-POLIZZA NON VALORIZZATO'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
           END-IF.

       EX-S0005.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE

      *--> ROUTINE PER CERCARE IL CODICE PARAMETRO SULLA DCLGEN
      *--> PARAMETRO OGGETTO PRESENTE A CONTESTO
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
              PERFORM S1200-CONTROLLO-DATI
                 THRU S1200-CONTROLLO-DATI-EX
           END-IF.


       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-PARAM-OGG
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPOG-AREA-PARAM-OGG
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLO DATI
      *----------------------------------------------------------------*
       S1200-CONTROLLO-DATI.

           IF IVVC0213-COD-PARAMETRO = DPOG-COD-PARAM(IVVC0213-IX-TABB)
              IF  DPOG-VAL-NUM(IVVC0213-IX-TABB) IS NUMERIC

                  EVALUATE DPOG-VAL-NUM(IVVC0213-IX-TABB)
                     WHEN 1
                     WHEN 2
                        MOVE 1
                          TO IVVC0213-VAL-IMP-O
                     WHEN 0
                        MOVE 0
                          TO IVVC0213-VAL-IMP-O
                     WHEN OTHER
                        SET IDSV0003-FIELD-NOT-VALUED   TO TRUE
                  END-EVALUATE

              ELSE
                  SET IDSV0003-FIELD-NOT-VALUED   TO TRUE
              END-IF

           END-IF.

       S1200-CONTROLLO-DATI-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.

      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS2780.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2013.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS2780
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... IMPOSTA DI BOLLO
      *               CALCOLO DELLA VARIABILE AMMISSIONE BOLLO (Isbollo)
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS2780'.
       01  LDBS1420                         PIC X(008) VALUE 'LDBS1420'.
       01  IDBSP580                         PIC X(008) VALUE 'IDBSP580'.
       01  LDBS6040                         PIC X(008) VALUE 'LDBS6040'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DATA                         PIC 9(08).
       01  WK-DATA-SEPARATA REDEFINES WK-DATA.
           03 WK-ANNO                  PIC 9(04).
           03 WK-MESE                  PIC 9(02).
           03 WK-GG                    PIC 9(02).

       01 WK-DATA-INIZIO.
           03 WK-ANNO-INI              PIC 9(04).
           03 WK-MESE-INI              PIC 9(02).
           03 WK-GG-INI                PIC 9(02).

       01 WK-DATA-INIZIO-D             REDEFINES
             WK-DATA-INIZIO               PIC S9(8)V COMP-3.

       01 WK-DATA-FINE.
           03 WK-ANNO-FINE              PIC 9(04).
           03 WK-MESE-FINE              PIC 9(02).
           03 WK-GG-FINE                PIC 9(02).

       01 WK-DATA-FINE-D             REDEFINES
             WK-DATA-FINE               PIC S9(8)V COMP-3.

       01 FINE-CICLO                    PIC X(1).
          88 FINE-CICLO-SI              VALUE 'S'.
          88 FINE-CICLO-NO              VALUE 'N'.

       01 FINE-GAR                      PIC X(1).
          88 FINE-GAR-SI              VALUE 'S'.
          88 FINE-GAR-NO              VALUE 'N'.

       01 FLAG-RAMO-III-V               PIC X(1).
          88 RAMO-III-V-SI            VALUE 'S'.
          88 RAMO-III-V-NO            VALUE 'N'.

       01 FLAG-RAMO-I                   PIC X(1).
          88 RAMO-I-SI            VALUE 'S'.
          88 RAMO-I-NO            VALUE 'N'.

       01 FLAG-COMUN-TROV                    PIC X(01).
           88 COMUN-TROV-SI                  VALUE 'S'.
           88 COMUN-TROV-NO                  VALUE 'N'.

       01 FLAG-CUR-MOV                       PIC X(01).
           88 INIT-CUR-MOV                   VALUE 'S'.
           88 FINE-CUR-MOV                   VALUE 'N'.

       01 FLAG-ULTIMA-LETTURA               PIC X(01).
          88 SI-ULTIMA-LETTURA              VALUE 'S'.
          88 NO-ULTIMA-LETTURA              VALUE 'N'.

       01  WK-VAR-MOVI-COMUN.
           05 WK-ID-MOVI-COMUN             PIC S9(09) COMP-3.
           05 WK-DATA-EFF-PREC             PIC S9(08) COMP-3.
           05 WK-DATA-CPTZ-PREC            PIC S9(18) COMP-3.
           05 WK-DATA-EFF-RIP              PIC S9(08) COMP-3.
           05 WK-DATA-CPTZ-RIP             PIC S9(18) COMP-3.
      *---------------------------------------------------------------*
      *  LISTA MOVIMENTI IMPLEMENTATI LCCC0006
      *---------------------------------------------------------------*
           COPY LCCC0006.
      *---------------------------------------------------------------*
      *  COPY DB2
      *---------------------------------------------------------------*
           COPY IDBVGRZ1.
           COPY IDBVP581.
           COPY IDBVMOV1.
           COPY LDBV1421.
      *---------------------------------------------------------------*
      *  COPY TIPOLOGICHE
      *---------------------------------------------------------------*
           COPY ISPV0000.
10819      COPY LCCVXMVZ.
10819      COPY LCCVXMV2.
10819      COPY LCCVXMV3.
10819      COPY LCCVXMV5.
10819      COPY LCCVXMV6.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-POLI.
          03 DPOL-AREA-POLI.
             04 DPOL-ELE-POLI-MAX        PIC S9(04) COMP.
             04 DPOL-TAB-POLI.
             COPY LCCVPOL1              REPLACING ==(SF)== BY ==DPOL==.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIABILI PER LA GESTIONE ERRORI                     *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-ISO                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS2780.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS2780.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT
                                             WK-VAR-MOVI-COMUN.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           SET COMUN-TROV-NO                 TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-POLI.


      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE

           PERFORM CALCOLA-FLAG
              THRU CALCOLA-FLAG-EX.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    VERIFICA POLIZZA
      *----------------------------------------------------------------*
       CALCOLA-FLAG.

           MOVE 0 TO IVVC0213-VAL-IMP-O

           IF DPOL-TP-RGM-FISC = '01'
              MOVE DPOL-TP-POLI
                TO ISPV0000-TP-POLI
              IF ISPV0000-IND-FIP-PIP
                 CONTINUE
              ELSE
                 PERFORM VERIFICA-MOVIMENTO
                    THRU VERIFICA-MOVIMENTO-EX

                 PERFORM S1110-LETTURA-GARANZIA
                    THRU S1110-LETTURA-GARANZIA-EX

                 IF IDSV0003-SUCCESSFUL-RC
      *--> SE SONO PRESENTI GARANZIE DI RAMO I E III O V
      *--> VERIFICHIAMO LA PRESENZA DI BOLLI PREGRESSI
                    IF RAMO-I-SI AND RAMO-III-V-SI
      *                PERFORM S1120-VERIFICA-BOL-PRE
      *                   THRU S1120-VERIFICA-BOL-PRE-EX
                       MOVE 1 TO IVVC0213-VAL-IMP-O
                    ELSE
                       IF RAMO-I-SI AND RAMO-III-V-NO
                          PERFORM S1120-VERIFICA-BOL-PRE
                             THRU S1120-VERIFICA-BOL-PRE-EX
                       ELSE
                          IF RAMO-I-NO AND RAMO-III-V-SI
                             MOVE 1 TO IVVC0213-VAL-IMP-O
                          ELSE
                             MOVE 0 TO IVVC0213-VAL-IMP-O
                          END-IF
                       END-IF
                    END-IF
                 END-IF
              END-IF
           END-IF.

       CALCOLA-FLAG-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICA MOVIMENTO
      *----------------------------------------------------------------*
       VERIFICA-MOVIMENTO.

      *--> SE CI TROVIAMO IN FASE DI LIQUIDAZIONE DEVO RECUPERARE LE
      *--> IMMAGINI IN FASE DI COMUNICAZIONE
NEW   *    MOVE IVVC0213-TIPO-MOVIMENTO
NEW   *      TO WS-MOVIMENTO
NEW        MOVE IVVC0213-TIPO-MOVI-ORIG
NEW          TO WS-MOVIMENTO

NEW        IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
NEW           LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND OR
10819         LIQUI-RPP-TAKE-PROFIT
10819X     OR LIQUI-RISTOT-INCAPIENZA
10819X     OR LIQUI-RPP-REDDITO-PROGR
10819      OR LIQUI-RPP-BENEFICIO-CONTR
FNZS2      OR LIQUI-RISTOT-INCAP
      *--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
NEW           PERFORM RECUP-MOVI-COMUN
NEW              THRU RECUP-MOVI-COMUN-EX

              IF COMUN-TROV-SI
                 MOVE IDSV0003-DATA-COMPETENZA
                   TO WK-DATA-CPTZ-RIP

                 MOVE IDSV0003-DATA-INIZIO-EFFETTO
                   TO WK-DATA-EFF-RIP

                 MOVE WK-DATA-CPTZ-PREC
                   TO IDSV0003-DATA-COMPETENZA

                 MOVE WK-DATA-EFF-PREC
                   TO IDSV0003-DATA-INIZIO-EFFETTO
                      IDSV0003-DATA-FINE-EFFETTO
NEW           ELSE
                 CONTINUE
NEW           END-IF
NEW        ELSE
              CONTINUE
           END-IF.

       VERIFICA-MOVIMENTO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Recupero il movimento di comunicazione
      *----------------------------------------------------------------*
       RECUP-MOVI-COMUN.

           SET INIT-CUR-MOV               TO TRUE
           SET COMUN-TROV-NO              TO TRUE

           INITIALIZE MOVI

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
           SET  NO-ULTIMA-LETTURA         TO TRUE

      *
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR FINE-CUR-MOV
                      OR COMUN-TROV-SI

              INITIALIZE MOVI
      *
              MOVE IVVC0213-ID-POLIZZA    TO MOV-ID-OGG
              MOVE 'PO'                   TO MOV-TP-OGG

              SET IDSV0003-TRATT-SENZA-STOR   TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSV0003-WHERE-CONDITION TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL TO TRUE

              CALL  LDBS6040   USING IDSV0003 MOVI
                ON EXCEPTION
                   MOVE LDBS6040          TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                     TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER   TO TRUE
              END-CALL


                IF IDSV0003-SUCCESSFUL-RC

                    EVALUATE TRUE
                        WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA
      *-->     LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
      *-->     BOLLO IN INPUT
                          SET FINE-CUR-MOV TO TRUE

                        WHEN IDSV0003-SUCCESSFUL-SQL
      *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
      *                   MOVE IDSV0003-BUFFER-DATI TO MOVI
      *      TROVO IL MOVIMENTO DI COMUNICAZIONE RISCATTO PARZIALE
                          MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                          IF COMUN-RISPAR-IND
                          OR RISTO-INDIVI
                          OR VARIA-OPZION
                          OR SINIS-INDIVI
                          OR RECES-INDIVI
10819                     OR RPP-TAKE-PROFIT
10819X                    OR COMUN-RISTOT-INCAPIENZA
10819X                    OR RPP-REDDITO-PROGRAMMATO
10819                     OR RPP-BENEFICIO-CONTR
FNZS2                     OR COMUN-RISTOT-INCAP
                             MOVE MOV-ID-MOVI    TO WK-ID-MOVI-COMUN
                             SET SI-ULTIMA-LETTURA  TO TRUE
                             MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                             COMPUTE WK-DATA-CPTZ-PREC =  MOV-DS-TS-CPTZ
                                                       - 1
                             SET COMUN-TROV-SI   TO TRUE
                             PERFORM CLOSE-MOVI
                                THRU CLOSE-MOVI-EX
                             SET FINE-CUR-MOV   TO TRUE
                          ELSE
                             SET IDSV0003-FETCH-NEXT   TO TRUE
                          END-IF
                        WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                             MOVE LDBS6040
                               TO IDSV0003-COD-SERVIZIO-BE
                             MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                               TO IDSV0003-DESCRIZ-ERR-DB2
                             SET IDSV0003-INVALID-OPER   TO TRUE
                    END-EVALUATE
                 ELSE
      *--> GESTIRE ERRORE
                    MOVE LDBS6040
                      TO IDSV0003-COD-SERVIZIO-BE
                    MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                      TO IDSV0003-DESCRIZ-ERR-DB2
                    SET IDSV0003-INVALID-OPER   TO TRUE
                 END-IF
           END-PERFORM.
           MOVE IDSV0003-TIPO-MOVIMENTO TO WS-MOVIMENTO.

       RECUP-MOVI-COMUN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA DATI TABELLA GARANZIA
      *----------------------------------------------------------------*
       S1110-LETTURA-GARANZIA.

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE.

      *--> FLAG
           SET RAMO-III-V-NO              TO TRUE.
           SET RAMO-I-NO                  TO TRUE.
           SET FINE-GAR-NO                TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR NOT IDSV0003-SUCCESSFUL-SQL
                      OR FINE-GAR-SI

              INITIALIZE GAR

              MOVE DPOL-ID-POLI
                TO LDBV1421-ID-POLI

              MOVE 5
                TO LDBV1421-RAMO-BILA-1
              MOVE 3
                TO LDBV1421-RAMO-BILA-2
              MOVE 1
                TO LDBV1421-RAMO-BILA-3

              MOVE LDBV1421     TO IDSV0003-BUFFER-WHERE-COND

              SET IDSV0003-TRATT-X-COMPETENZA TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSV0003-WHERE-CONDITION TO TRUE

              CALL LDBS1420 USING IDSV0003 GAR
              ON EXCEPTION
                 MOVE LDBS1420
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'ERRORE CALL LDBS1420 FETCH'
                   TO IDSV0003-DESCRIZ-ERR-DB2
                 SET IDSV0003-INVALID-OPER TO TRUE
              END-CALL

              IF IDSV0003-SUCCESSFUL-RC

                 EVALUATE TRUE

                     WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA
                          SET FINE-GAR-SI TO TRUE

                     WHEN IDSV0003-SUCCESSFUL-SQL
      *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
      *-->   SE LA POLIZZA HA ALMENO UNA GARANZIA DI RAMO 5 O 3         AD 1
      *-->   E' AMMESSO IL CALCOLO DEL BOLLO                            AD 1
                          IF GRZ-RAMO-BILA = 3 OR 5
                             SET RAMO-III-V-SI TO TRUE
                          ELSE
                             IF GRZ-RAMO-BILA = 1
                                SET RAMO-I-SI TO TRUE
                             END-IF
                          END-IF
                          SET IDSV0003-FETCH-NEXT
                           TO TRUE
      *                   MOVE 1 TO IVVC0213-VAL-IMP-O
      *                   PERFORM S1130-CHIUSURA-CURS
      *                      THRU S1130-CHIUSURA-CURS-EX

                     WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                          SET IDSV0003-INVALID-OPER  TO TRUE
                          MOVE WK-PGM
                            TO IDSV0003-COD-SERVIZIO-BE
                          STRING 'ERRORE LETTURA TABELLA GARANZIA ;'
                                 IDSV0003-RETURN-CODE ';'
                                 IDSV0003-SQLCODE
                                 DELIMITED BY SIZE INTO
                                 IDSV0003-DESCRIZ-ERR-DB2
                          END-STRING
                          SET FINE-GAR-SI TO TRUE
                 END-EVALUATE

              END-IF

           END-PERFORM.

       S1110-LETTURA-GARANZIA-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA IMPOSTA DI BOLLO PER BOLLI PREGRESSI
      *----------------------------------------------------------------*
       S1120-VERIFICA-BOL-PRE.

           INITIALIZE IMPST-BOLLO

           MOVE DPOL-IB-OGG      TO P58-IB-POLI.

           SET FINE-CICLO-NO     TO TRUE.

      *  --> Tipo operazione
           SET IDSV0003-FETCH-FIRST        TO TRUE.
      *  --> Tipo livello
           SET IDSV0003-IB-SECONDARIO      TO TRUE.
           SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR FINE-CICLO-SI
                      OR IDSV0003-CLOSE-CURSOR

           CALL IDBSP580    USING IDSV0003 IMPST-BOLLO

           ON EXCEPTION
              MOVE IDBSP580               TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-IDBSPOL0 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER   TO TRUE
           END-CALL
              IF IDSV0003-SUCCESSFUL-RC
                 EVALUATE TRUE
                     WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA
                          SET FINE-CICLO-SI TO TRUE
                          CONTINUE
                     WHEN IDSV0003-SUCCESSFUL-SQL
      *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                          IF P58-TP-CAUS-BOLLO = 'AN' OR 'SW'
                             MOVE 1 TO IVVC0213-VAL-IMP-O
                             PERFORM S1140-CHIUSURA-CURS
                                THRU S1140-CHIUSURA-CURS-EX
                          ELSE
                             SET IDSV0003-FETCH-NEXT TO TRUE
                          END-IF
                     WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                          SET FINE-CICLO-SI TO TRUE
                          SET IDSV0003-INVALID-OPER  TO TRUE
                          MOVE IDBSP580
                            TO IDSV0003-COD-SERVIZIO-BE
                          STRING 'ERRORE LETTURA TABELLA IMPST-BOL;'
                                 IDSV0003-RETURN-CODE ';'
                                 IDSV0003-SQLCODE
                                 DELIMITED BY SIZE INTO
                                 IDSV0003-DESCRIZ-ERR-DB2
                          END-STRING

                 END-EVALUATE
              END-IF
           END-PERFORM.
       S1120-VERIFICA-BOL-PRE-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CHIUSURA CURSORE GARANZIA
      *----------------------------------------------------------------*
       S1130-CHIUSURA-CURS.

           SET IDSV0003-CLOSE-CURSOR            TO TRUE.

           CALL LDBS1420 USING IDSV0003 GAR
           ON EXCEPTION
              MOVE LDBS1420
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CALL LDBS1420 CLOSE'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER TO TRUE
           END-CALL.

           IF NOT IDSV0003-SUCCESSFUL-RC
           OR NOT IDSV0003-SUCCESSFUL-SQL
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CLOSE CURSORE LDBS1420'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER
                TO TRUE
           END-IF.

       S1130-CHIUSURA-CURS-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   CHIUSURA CURSORE IMPOSTA DI BOLLO
      *----------------------------------------------------------------*
       S1140-CHIUSURA-CURS.

           SET IDSV0003-CLOSE-CURSOR            TO TRUE.

           CALL IDBSP580 USING IDSV0003 IMPST-BOLLO
           ON EXCEPTION
              MOVE LDBS1420
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CALL IDBSP580 CLOSE'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER TO TRUE
           END-CALL.

           IF NOT IDSV0003-SUCCESSFUL-RC
           OR NOT IDSV0003-SUCCESSFUL-SQL
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CLOSE CURSORE IMPST-BOLLO'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER
                TO TRUE
           END-IF.

           SET FINE-CICLO-SI TO TRUE.

       S1140-CHIUSURA-CURS-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-POLI
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPOL-AREA-POLI
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
      *----------------------------------------------------------------*
       CLOSE-MOVI.

           SET IDSV0003-CLOSE-CURSOR            TO TRUE.

           CALL LDBS6040 USING IDSV0003 MOVI
           ON EXCEPTION
              MOVE LDBS6040
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CALL LDBS6040 CLOSE'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER TO TRUE
           END-CALL.

           IF NOT IDSV0003-SUCCESSFUL-RC
           OR NOT IDSV0003-SUCCESSFUL-SQL
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CLOSE CURSORE LDBS6040'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER
                TO TRUE
           END-IF.
       CLOSE-MOVI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           MOVE SPACES                     TO IVVC0213-VAL-STR-O.
           MOVE 0                          TO IVVC0213-VAL-PERC-O.

NEW   *    IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
NEW   *       LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND
NEW        IF COMUN-TROV-SI
              MOVE WK-DATA-CPTZ-RIP
                TO IDSV0003-DATA-COMPETENZA

              MOVE WK-DATA-EFF-RIP
                TO IDSV0003-DATA-INIZIO-EFFETTO
                   IDSV0003-DATA-FINE-EFFETTO
           END-IF
      *
           GOBACK.

       EX-S9000.
           EXIT.

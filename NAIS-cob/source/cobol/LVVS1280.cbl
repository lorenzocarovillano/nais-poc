      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS1280.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2009.
       DATE-COMPILED.
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS1280'.
       01  LDBS4910                         PIC X(008) VALUE 'LDBS4910'.
       01  LDBS6000                         PIC X(008) VALUE 'LDBS6000'.
       01  LDBS5990                         PIC X(008) VALUE 'LDBS5990'.
       01  LDBS1530                         PIC X(008) VALUE 'LDBS1530'.
       01  LDBS5950                         PIC X(008) VALUE 'LDBS5950'.
       01  IDBSSTW0                         PIC X(008) VALUE 'IDBSSTW0'.
       01  IDBSL190                         PIC X(008) VALUE 'IDBSL190'.
       01  IDBSL410                         PIC X(008) VALUE 'IDBSL410'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-VAL-FONDO                  PIC S9(12)V9(3) COMP-3 VALUE 0.
       01  WK-IMP-DISPL                  PIC -ZZZZZZZZZZZ9,999.
      *
       01  WK-ID-COD-LIV-FLAG            PIC X(001).
           88 WK-ID-COD-TROVATO          VALUE 'S'.
           88 WK-ID-COD-NON-TROVATO      VALUE 'N'.
      *
       01  WK-NUM-QUO                    PIC X(002).
           88 WK-NUM-QUO-OK              VALUE 'OK'.
           88 WK-NUM-QUO-KO              VALUE 'KO'.

       77 WK-APP-ARRO                    PIC 9(001) VALUE 0.
       77 WK-APP-ARRO-1                  PIC 9(001) VALUE 0.
VVV    77 WK-ADD-CENT                    PIC 9(1)V9(02) VALUE 0,01.
VVV    77 WK-APPO-2DEC                   PIC S9(11)V9(02) VALUE 0.
       01 WK-AREA-ARRO.
          05 WK-TAB-ARRO                 OCCURS 20.
             10 WK-IMP-ARRO              PIC X(1).

       01 WK-ANN-COM-RIS-TOT             PIC 9(005) VALUE 93012.
       01 WK-ANN-LIQ-RIS-TOT             PIC 9(005) VALUE 95010.
       01 WK-ANN-COM-SIN                 PIC 9(005) VALUE 93019.
       01 WK-ANN-LIQ-SIN                 PIC 9(005) VALUE 95018.

       01 WK-ID-MOVI-FINRIO              PIC S9(009)V COMP-3.
       01 WK-DT-EFF-COMUN                PIC S9(08) COMP-3.
       01 WK-DT-CPTZ-COMUN               PIC S9(18) COMP-3.

       01 WK-TP-VAL-AST                  PIC X(002).
          88  VALORE-POSITIVO            VALUE 'VP'.
          88  ANNULLO-POSITIVO           VALUE 'AP'.
          88  VALORE-NEGATIVO            VALUE 'VN'.
          88  ANNULLO-NEGATIVO           VALUE 'AN'.
          88  VAS-LIQUIDAZIONE           VALUE 'LQ'.
          88  ANNULLO-LIQUIDAZIONE       VALUE 'AL'.

       01 FLAG-APPO                      PIC X(002).
          88  APPO-TROVATO               VALUE 'SI'.
          88  APPO-NO-TROVATO            VALUE 'NO'.

       01 WK-APPO-DATE.
          05 WK-APPO-DATA-INIZIO-EFFETTO PIC S9(008) COMP-3.
          05 WK-APPO-DATA-FINE-EFFETTO   PIC S9(008) COMP-3.
          05 WK-APPO-DATA-COMPETENZA     PIC S9(018) COMP-3.

       01 APPO-EFFETTO                   PIC  9(8).
       01 EFFETTO-CONT                   PIC  9(8).

       01 APPO-DATE.
          03 AA-APPO                     PIC 9(4).
          03 MM-APPO                     PIC 9(2).
          03 GG-APPO                     PIC 9(2).

       01 APPO-DATE2.
          03 AA-APPO2                    PIC 9(4).
          03 MM-APPO2                    PIC 9(2).
          03 GG-APPO2                    PIC 9(2).

       01 WK-VAL-QUO PIC S9(7)V9(5) COMP-3.
      *---------------------------------------------------------------*
      *  COPY TIPOLOGICHE
      *---------------------------------------------------------------*
10819      COPY LCCVXMVZ.
10819      COPY LCCVXMV2.
10819      COPY LCCVXMV5.

      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA VALORE ASSET
      *---------------------------------------------------------------*
           COPY IDBVVAS1.
           COPY IDBVMOV1.
           COPY IDBVMFZ1.
           COPY IDBVSTW1.

      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBS
      *---------------------------------------------------------------*
           COPY LDBV6001.
           COPY LDBV5991.
           COPY LDBV4911.
           COPY IDBVL191.
           COPY IDBVL411.

      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-TGA.
          03 DTGA-AREA-TGA.
             04 DTGA-ELE-TGA-MAX       PIC S9(04) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==DTGA==.
             COPY LCCVTGA1              REPLACING ==(SF)== BY ==DTGA==.

       01 AREA-IO-GAR.
          03 DGRZ-AREA-GRA.
             04 DGRZ-ELE-GAR-MAX        PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==DGRZ==.
             COPY LCCVGRZ1              REPLACING ==(SF)== BY ==DGRZ==.

       01 AREA-IO-L19.
          02 DL19-AREA-QUOTA.
             COPY LCCVL197              REPLACING ==(SF)== BY ==DL19==.
             COPY LCCVL191              REPLACING ==(SF)== BY ==DL19==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP VALUE 0.
           03 IX-TAB-GRZ                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-L19                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-TGA                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-VAS                     PIC S9(04) COMP VALUE 0.
           03 IX-GUIDA-GRZ                   PIC S9(04) COMP VALUE 0.
           03 IX-TGA-APPO                    PIC S9(04) COMP VALUE 0.
      *
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*


           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000
      *

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.
      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.
           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           INITIALIZE VAL-AST
                      AREA-IO-TGA
                      AREA-IO-GAR
                      AREA-IO-L19.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

           MOVE IVVC0213-TIPO-MOVI-ORIG
             TO WS-MOVIMENTO.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.


      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1200-CONTROLLO-DATI
                  THRU S1200-CONTROLLO-DATI-EX
      *
               IF  IDSV0003-SUCCESSFUL-RC
               AND IDSV0003-SUCCESSFUL-SQL
                   IF DGRZ-TP-INVST(IX-GUIDA-GRZ) = 7 OR 8
                      IF DL19-ELE-FND-MAX EQUAL ZERO
                         IF DTGA-PRSTZ-ULT(IVVC0213-IX-TABB) IS NUMERIC
                            MOVE DTGA-PRSTZ-ULT(IVVC0213-IX-TABB)
                              TO IVVC0213-VAL-IMP-O
                         ELSE
                            MOVE ZEROES
                              TO IVVC0213-VAL-IMP-O
                         END-IF
                      ELSE
                         IF LIQUI-RISPAR-POLIND
10819X                   OR LIQUI-RPP-REDDITO-PROGR
                         OR LIQUI-RISPAR-ADE
10819                    OR LIQUI-RPP-TAKE-PROFIT
                            PERFORM S1230-RECUP-ID-COMUN
                               THRU S1230-EX
                         ELSE
                            MOVE ZEROES
                              TO WK-ID-MOVI-FINRIO
                         END-IF
                         PERFORM S1250-CALCOLA-QUOTE     THRU S1250-EX
                         IF IVVC0213-VAL-IMP-O < ZEROES
                            COMPUTE IVVC0213-VAL-IMP-O =
                                    IVVC0213-VAL-IMP-O * (-1)
                         END-IF
                      END-IF
                   ELSE
                      IF DTGA-PRSTZ-ULT(IVVC0213-IX-TABB) IS NUMERIC
                         MOVE DTGA-PRSTZ-ULT(IVVC0213-IX-TABB)
                           TO IVVC0213-VAL-IMP-O
                      ELSE
                         MOVE ZEROES
                           TO IVVC0213-VAL-IMP-O
                      END-IF
                   END-IF
               END-IF
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-TRCH-GAR
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DTGA-AREA-TGA
           END-IF.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-GARANZIA
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DGRZ-AREA-GRA
           END-IF.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-QOTAZ-FON
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DL19-AREA-QUOTA
           END-IF.
      *
       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO DATI DCLGEN
      *----------------------------------------------------------------*
       S1200-CONTROLLO-DATI.
      *
           SET WK-ID-COD-NON-TROVATO              TO TRUE.
      *
           IF DGRZ-ELE-GAR-MAX GREATER ZERO
      *
              PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
                UNTIL IX-TAB-GRZ > DGRZ-ELE-GAR-MAX
                   OR WK-ID-COD-TROVATO
                   IF DTGA-ID-GAR(IVVC0213-IX-TABB) =
                      DGRZ-ID-GAR(IX-TAB-GRZ)
                      SET WK-ID-COD-TROVATO     TO TRUE
                      MOVE IX-TAB-GRZ           TO IX-GUIDA-GRZ
                   END-IF
              END-PERFORM
      *
              IF WK-ID-COD-NON-TROVATO
                 SET  IDSV0003-INVALID-OPER               TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'GARANZIA NON VALORIZZATA PER CALCOLO QUOTE'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
      *
           ELSE
              SET  IDSV0003-INVALID-OPER                  TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'GARANZIA NON VALORIZZATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.
      *
       S1200-CONTROLLO-DATI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  RECUPERO L'ID DEL MOVIMENTO DI COMUNICAZIONE
      *----------------------------------------------------------------*
       S1230-RECUP-ID-COMUN.
      *
           INITIALIZE                      MOVI.
           SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.

           SET IDSV0003-SELECT             TO TRUE.
           SET IDSV0003-WHERE-CONDITION    TO TRUE.
           SET IDSV0003-TRATT-SENZA-STOR   TO TRUE.

           IF LIQUI-RISPAR-POLIND
              MOVE IVVC0213-ID-POLIZZA     TO MOV-ID-OGG
              MOVE 'PO'                    TO MOV-TP-OGG
              SET COMUN-RISPAR-IND         TO TRUE
              MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
           END-IF.

10819X     IF LIQUI-RPP-REDDITO-PROGR
10819X        MOVE IVVC0213-ID-POLIZZA     TO MOV-ID-OGG
10819X        MOVE 'PO'                    TO MOV-TP-OGG
10819X        SET RPP-REDDITO-PROGRAMMATO  TO TRUE
10819X        MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
10819X     END-IF.

           IF LIQUI-RISPAR-ADE
              MOVE IVVC0213-ID-ADESIONE    TO MOV-ID-OGG
              MOVE 'AD'                    TO MOV-TP-OGG
              SET COMUN-RISPAR-ADE         TO TRUE
              MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
           END-IF.

10819      IF LIQUI-RPP-TAKE-PROFIT
10819         MOVE IVVC0213-ID-POLIZZA     TO MOV-ID-OGG
10819         MOVE 'PO'                    TO MOV-TP-OGG
10819         SET RPP-TAKE-PROFIT          TO TRUE
10819         MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
10819      END-IF.

           MOVE MOVI                       TO IDSV0003-BUFFER-WHERE-COND

           CALL LDBS1530  USING  IDSV0003 MOVI
           ON EXCEPTION
              MOVE LDBS1530
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'ERRORE CALL LDBS1530'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL

           EVALUATE TRUE
               WHEN IDSV0003-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CON SUCCESSO
                    PERFORM RECUP-STAT-MOVI-COMUN
                       THRU RECUP-STAT-MOVI-COMUN-EX
                    IF  IDSV0003-SUCCESSFUL-RC
                    AND IDSV0003-SUCCESSFUL-SQL
                        PERFORM S1235-RECUP-MOVI-FINRIO
                           THRU S1235-EX
                    END-IF
               WHEN IDSV0003-NOT-FOUND
      *-->     CHIAVE NON TROVATA
                    MOVE ZEROES
                      TO WK-ID-MOVI-FINRIO

               WHEN OTHER
      *-->     ERRORE DB
                  SET IDSV0003-INVALID-OPER  TO TRUE
                  MOVE WK-PGM
                    TO IDSV0003-COD-SERVIZIO-BE
                  STRING 'ERRORE LETTURA TABELLA LDBS1530 ;'
                        IDSV0003-RETURN-CODE ';'
                        IDSV0003-SQLCODE
                    DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                  END-STRING
           END-EVALUATE.
      *
           SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
      *
       S1230-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Valorizzazione dell'area di pagina
      *----------------------------------------------------------------*
       RECUP-STAT-MOVI-COMUN.
           INITIALIZE STAT-OGG-WF.

      *--> DATA EFFETTO

      *--> SALVO LE DATE DI CONTESTO PER RIPRISTINARLE DOPO L'ACCESSO
           INITIALIZE WK-APPO-DATE
           IF  IDSV0003-DATA-INIZIO-EFFETTO IS NUMERIC
           AND IDSV0003-DATA-INIZIO-EFFETTO > ZEROES
               MOVE IDSV0003-DATA-INIZIO-EFFETTO
                 TO WK-APPO-DATA-INIZIO-EFFETTO
           END-IF
           IF  IDSV0003-DATA-FINE-EFFETTO IS NUMERIC
           AND IDSV0003-DATA-FINE-EFFETTO > ZEROES
               MOVE IDSV0003-DATA-FINE-EFFETTO
                 TO WK-APPO-DATA-FINE-EFFETTO
           END-IF
           IF  IDSV0003-DATA-COMPETENZA IS NUMERIC
           AND IDSV0003-DATA-COMPETENZA > ZEROES
               MOVE IDSV0003-DATA-COMPETENZA
                 TO WK-APPO-DATA-COMPETENZA
           END-IF
           SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
           SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.
           SET IDSV0003-ID-OGGETTO         TO TRUE.
           SET IDSV0003-SELECT             TO TRUE.

      *--> VALORRIZZA DCLGEN TABELLA
           MOVE MOV-ID-MOVI              TO STW-ID-OGG.
           MOVE 'MO'                     TO STW-TP-OGG.
           CALL IDBSSTW0  USING  IDSV0003 STAT-OGG-WF
           ON EXCEPTION
              MOVE IDBSSTW0
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'ERRORE CALL LDBS1530'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL

           IF IDSV0003-SUCCESSFUL-RC
      *-->    GESTIRE ERRORE DB
              EVALUATE TRUE
                  WHEN IDSV0003-SUCCESSFUL-SQL
      *-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                     IF STW-STAT-OGG-WF = 'CO'
                        MOVE STW-DT-INI-EFF
                          TO WK-DT-EFF-COMUN
                        MOVE STW-DS-TS-INI-CPTZ
                          TO WK-DT-CPTZ-COMUN
                     ELSE
                        SET IDSV0003-INVALID-OPER  TO TRUE
                        MOVE IDBSSTW0
                          TO IDSV0003-COD-SERVIZIO-BE
                        STRING 'ERRORE LETTURA TABELLA IDBSSTW0 ;'
                                IDSV0003-RETURN-CODE ';'
                                IDSV0003-SQLCODE
                         DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                        END-STRING
                     END-IF
                  WHEN IDSV0003-NOT-FOUND
      *-->        CHIAVE NON TROVATA
                        SET IDSV0003-INVALID-OPER  TO TRUE
                        MOVE IDBSSTW0
                          TO IDSV0003-COD-SERVIZIO-BE
                        STRING 'ERRORE LETTURA TABELLA IDBSSTW0 ;'
                                IDSV0003-RETURN-CODE ';'
                                IDSV0003-SQLCODE
                         DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                        END-STRING

                  WHEN OTHER
      *-->        ERRORE DI ACCESSO AL DB
                     SET IDSV0003-INVALID-OPER  TO TRUE
                     MOVE IDBSSTW0
                       TO IDSV0003-COD-SERVIZIO-BE
                     STRING 'ERRORE LETTURA TABELLA IDBSSTW0 ;'
                             IDSV0003-RETURN-CODE ';'
                             IDSV0003-SQLCODE
                         DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                     END-STRING
              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              SET IDSV0003-INVALID-OPER  TO TRUE
              MOVE IDBSSTW0
                TO IDSV0003-COD-SERVIZIO-BE
              STRING 'ERRORE LETTURA TABELLA IDBSSTW0 ;'
                      IDSV0003-RETURN-CODE ';'
                      IDSV0003-SQLCODE
                      DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.
      *--> RIPRISTINO LE DATE DI CONTESTO
           IF WK-APPO-DATA-INIZIO-EFFETTO > ZEROES
              MOVE WK-APPO-DATA-INIZIO-EFFETTO
                TO IDSV0003-DATA-INIZIO-EFFETTO
           END-IF.
           IF WK-APPO-DATA-FINE-EFFETTO > ZEROES
              MOVE WK-APPO-DATA-FINE-EFFETTO
                TO IDSV0003-DATA-FINE-EFFETTO
           END-IF.
           IF WK-APPO-DATA-COMPETENZA > ZEROES
              MOVE WK-APPO-DATA-COMPETENZA
                TO IDSV0003-DATA-COMPETENZA
           END-IF.

       RECUP-STAT-MOVI-COMUN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  RECUPERO L'ID DELLA MOVIMENTO FINANZIARIO DEL RISC. PARZIALE
      *----------------------------------------------------------------*
       S1235-RECUP-MOVI-FINRIO.
           INITIALIZE                      MOVI-FINRIO.
           SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.

           SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.
           SET IDSV0003-WHERE-CONDITION    TO TRUE.
           SET IDSV0003-SELECT             TO TRUE.

           MOVE MOV-ID-MOVI                TO MFZ-ID-MOVI-CRZ

      *--> SALVO LE DATE DI CONTESTO PER RIPRISTINARLE DOPO L'ACCESSO
           INITIALIZE WK-APPO-DATE
           IF  IDSV0003-DATA-INIZIO-EFFETTO IS NUMERIC
           AND IDSV0003-DATA-INIZIO-EFFETTO > ZEROES
               MOVE IDSV0003-DATA-INIZIO-EFFETTO
                 TO WK-APPO-DATA-INIZIO-EFFETTO
           END-IF
           IF  IDSV0003-DATA-FINE-EFFETTO IS NUMERIC
           AND IDSV0003-DATA-FINE-EFFETTO > ZEROES
               MOVE IDSV0003-DATA-FINE-EFFETTO
                 TO WK-APPO-DATA-FINE-EFFETTO
           END-IF
           IF  IDSV0003-DATA-COMPETENZA IS NUMERIC
           AND IDSV0003-DATA-COMPETENZA > ZEROES
               MOVE IDSV0003-DATA-COMPETENZA
                 TO WK-APPO-DATA-COMPETENZA
           END-IF

           MOVE WK-DT-EFF-COMUN          TO IDSV0003-DATA-INIZIO-EFFETTO
                                            IDSV0003-DATA-FINE-EFFETTO
           MOVE WK-DT-CPTZ-COMUN         TO IDSV0003-DATA-COMPETENZA

           MOVE MOVI-FINRIO                TO IDSV0003-BUFFER-WHERE-COND

           CALL LDBS5950  USING  IDSV0003 MOVI-FINRIO
           ON EXCEPTION
              MOVE LDBS5950
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'ERRORE CALL LDBS5950'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL

           EVALUATE TRUE
               WHEN IDSV0003-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CON SUCCESSO
                    MOVE MFZ-ID-MOVI-FINRIO
                      TO WK-ID-MOVI-FINRIO

               WHEN IDSV0003-NOT-FOUND
      *-->     CHIAVE NON TROVATA
                    MOVE ZEROES
                      TO WK-ID-MOVI-FINRIO

               WHEN OTHER
      *-->     ERRORE DB
                  SET IDSV0003-INVALID-OPER  TO TRUE
                  MOVE WK-PGM
                    TO IDSV0003-COD-SERVIZIO-BE
                  STRING 'ERRORE LETTURA TABELLA LDBS1530 ;'
                        IDSV0003-RETURN-CODE ';'
                        IDSV0003-SQLCODE
                    DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                  END-STRING
           END-EVALUATE.

      *--> RIPRISTINO LE DATE DI CONTESTO
           IF WK-APPO-DATA-INIZIO-EFFETTO > ZEROES
              MOVE WK-APPO-DATA-INIZIO-EFFETTO
                TO IDSV0003-DATA-INIZIO-EFFETTO
           END-IF.
           IF WK-APPO-DATA-FINE-EFFETTO > ZEROES
              MOVE WK-APPO-DATA-FINE-EFFETTO
                TO IDSV0003-DATA-FINE-EFFETTO
           END-IF.
           IF WK-APPO-DATA-COMPETENZA > ZEROES
              MOVE WK-APPO-DATA-COMPETENZA
                TO IDSV0003-DATA-COMPETENZA
           END-IF.
      *
       S1235-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATA 1
      *----------------------------------------------------------------*
       S1250-CALCOLA-QUOTE.
      *
           SET IDSV0003-WHERE-CONDITION        TO TRUE.
           SET IDSV0003-FETCH-FIRST            TO TRUE.
           SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE.
           SET WK-NUM-QUO-OK                   TO TRUE.
      *
           INITIALIZE LDBV4911
           MOVE IVVC0213-ID-TRANCHE   TO LDBV4911-ID-TRCH-DI-GAR
           MOVE 'LQ'                  TO LDBV4911-TP-VAL-AST-1
           MOVE 'AL'                  TO LDBV4911-TP-VAL-AST-2
           MOVE LDBV4911              TO IDSV0003-BUFFER-WHERE-COND.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR NOT IDSV0003-SUCCESSFUL-SQL
                      OR WK-NUM-QUO-KO
      *
              CALL LDBS4910  USING  IDSV0003 VAL-AST
              ON EXCEPTION
                 MOVE LDBS4910
                   TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'ERRORE CALL LDBS4910'
                   TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER  TO TRUE
              END-CALL
      *
              EVALUATE TRUE
                 WHEN IDSV0003-NOT-FOUND
      *-->          CHIAVE NON TROVATA
                    IF IDSV0003-FETCH-FIRST
                       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                       MOVE WK-PGM
                         TO IDSV0003-COD-SERVIZIO-BE
                       MOVE 'LETTURA VALORE ASSETT - SQLCODE = 100 '
                         TO IDSV0003-DESCRIZ-ERR-DB2
                    END-IF
                 WHEN IDSV0003-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                    PERFORM S1251-CALCOLA-QUOTE
                       THRU S1251-EX

                    SET IDSV0003-WHERE-CONDITION        TO TRUE
                    SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE
                    SET IDSV0003-FETCH-NEXT TO TRUE
      *
                    INITIALIZE LDBV4911
                    MOVE IVVC0213-ID-TRANCHE
                                            TO LDBV4911-ID-TRCH-DI-GAR
                    MOVE 'LQ'  TO LDBV4911-TP-VAL-AST-1
                    MOVE 'AL'  TO LDBV4911-TP-VAL-AST-2
                    MOVE LDBV4911  TO IDSV0003-BUFFER-WHERE-COND
                    SET IDSV0003-SUCCESSFUL-SQL TO TRUE
      *
                 WHEN OTHER
                    SET IDSV0003-INVALID-OPER  TO TRUE
                    MOVE WK-PGM
                      TO IDSV0003-COD-SERVIZIO-BE
                    STRING 'ERRORE LETTURA TABELLA VALORE ASSET ;'
                          IDSV0003-RETURN-CODE ';'
                          IDSV0003-SQLCODE
                      DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    END-STRING
              END-EVALUATE
      *
           END-PERFORM.
      *
           IF WK-NUM-QUO-KO
      *
              PERFORM S1252-CHIUDE-CURVAS         THRU S1252-EX
      *
              IF DTGA-PRSTZ-INI(IVVC0213-IX-TABB) IS NUMERIC
                 MOVE DTGA-PRSTZ-INI(IVVC0213-IX-TABB)
                   TO IVVC0213-VAL-IMP-O
              ELSE
                 SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'QUOTE NON VALORIZZATE'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
      *
           END-IF.

           PERFORM S1253-ARROTONDA-IMP     THRU S1253-EX.
      *
       S1250-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CHIUSURA CURSORE VALORE ASSET VALORE ASSETT
      *----------------------------------------------------------------*
       S1252-CHIUDE-CURVAS.
      *
           SET IDSV0003-CLOSE-CURSOR            TO TRUE.
      *
           CALL LDBS4910 USING  IDSV0003 VAL-AST
             ON EXCEPTION
                MOVE LDBS4910
                  TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'ERRORE CALL LDBS4910'
                  TO IDSV0003-DESCRIZ-ERR-DB2
                 SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           IF NOT IDSV0003-SUCCESSFUL-RC
           OR NOT IDSV0003-SUCCESSFUL-SQL
              MOVE LDBS4910
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CALL LDBS4910'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER
                TO TRUE
           END-IF.
      *
       S1252-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   LETTURA VALORIZZAZIONE VALORE ASSETT
      *----------------------------------------------------------------*
       S1251-CALCOLA-QUOTE.
      *
           SET WK-ID-COD-NON-TROVATO           TO TRUE.
      *
           PERFORM VARYING IX-TAB-L19 FROM 1 BY 1
             UNTIL IX-TAB-L19 > DL19-ELE-FND-MAX
                OR WK-ID-COD-TROVATO
                OR WK-NUM-QUO-KO

                IF VAS-COD-FND = DL19-COD-FND(IX-TAB-L19)
                   SET WK-ID-COD-TROVATO       TO TRUE
      *
                   MOVE ZERO  TO WK-VAL-QUO
                   IF VAS-ID-TRCH-DI-GAR-NULL NOT = HIGH-VALUE
                                        OR LOW-VALUE OR SPACES
                      SET APPO-NO-TROVATO  TO TRUE

                         PERFORM VARYING IX-TGA-APPO FROM 1 BY 1
                         UNTIL IX-TGA-APPO > DTGA-ELE-TGA-MAX
                            OR APPO-TROVATO

                           IF VAS-ID-TRCH-DI-GAR =
                              DTGA-ID-TRCH-DI-GAR(IX-TGA-APPO)

                              SET APPO-TROVATO          TO TRUE
                              MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                                        TO APPO-EFFETTO
                                                           EFFETTO-CONT
                              MOVE APPO-EFFETTO         TO APPO-DATE
                              MOVE DTGA-DT-DECOR(IX-TGA-APPO)
                                                        TO APPO-EFFETTO
                              MOVE APPO-EFFETTO         TO APPO-DATE2

                              IF AA-APPO = AA-APPO2
                                 MOVE DTGA-DT-DECOR(IX-TGA-APPO)
                                                        TO APPO-EFFETTO
                                 MOVE APPO-EFFETTO      TO APPO-DATE
                                 PERFORM S1300-LEGGI-QUOTZ
                                    THRU S1300-EX
                              ELSE
                                 MOVE 31   TO GG-APPO
                                 MOVE 12   TO MM-APPO
                                 COMPUTE AA-APPO = AA-APPO - 1

                                 PERFORM S1310-LEGGI-QUOTZ-AGG
                                    THRU S1310-EX
                              END-IF
                           END-IF
                         END-PERFORM
                      END-IF


      *            IF DL19-VAL-QUO(IX-TAB-L19) IS NUMERIC
      *--> PER IL PRIMO FONDO CHE HA NUM QUOTE A NULL ESCE DAL CICLO
                      IF VAS-NUM-QUO IS NUMERIC
                         IF VAS-ID-MOVI-FINRIO = WK-ID-MOVI-FINRIO
                            CONTINUE
                         ELSE
                            MOVE ZERO          TO WK-VAL-FONDO
                            COMPUTE WK-VAL-FONDO ROUNDED = (VAS-NUM-QUO
                                            * WK-VAL-QUO)
      *                                       DL19-VAL-QUO(IX-TAB-L19))

                            MOVE VAS-TP-VAL-AST TO WK-TP-VAL-AST

                            IF VALORE-POSITIVO
                            OR ANNULLO-NEGATIVO
                               COMPUTE IVVC0213-VAL-IMP-O
                                  = (IVVC0213-VAL-IMP-O + WK-VAL-FONDO)
                            ELSE
                               IF VALORE-NEGATIVO
                               OR ANNULLO-POSITIVO
                                  COMPUTE IVVC0213-VAL-IMP-O
                                   = (IVVC0213-VAL-IMP-O - WK-VAL-FONDO)
                               END-IF
                            END-IF
                         END-IF
                      ELSE
                         SET WK-NUM-QUO-KO      TO TRUE
                      END-IF
      *
      *            ELSE
      *
      *               MOVE LDBS4910
      *                 TO IDSV0003-COD-SERVIZIO-BE
      *               MOVE 'NUM-QUO / VAL-QUO NON NUMERICI'
      *                 TO IDSV0003-DESCRIZ-ERR-DB2
      *                SET IDSV0003-INVALID-OPER  TO TRUE
      *            END-IF
                END-IF
           END-PERFORM.
      *
       S1251-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   ARROTONDAMENTO IMPORTO
      *----------------------------------------------------------------*
       S1253-ARROTONDA-IMP.
      *
           MOVE IVVC0213-VAL-IMP-O              TO WK-AREA-ARRO.
           MOVE IVVC0213-VAL-IMP-O              TO WK-APPO-2DEC.

           MOVE WK-IMP-ARRO(14)                 TO WK-APP-ARRO-1.
           IF WK-APP-ARRO-1 NOT LESS 5
              IF WK-APPO-2DEC < ZERO
                 SUBTRACT WK-ADD-CENT           FROM WK-APPO-2DEC
              ELSE
                 ADD WK-ADD-CENT                TO WK-APPO-2DEC
              END-IF
           END-IF.
      *
           MOVE WK-APPO-2DEC                    TO IVVC0213-VAL-IMP-O.
      *
       S1253-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ESTRAI MOVIMENTI
      *----------------------------------------------------------------*

      *----------------------------------------------------------------*
      * LEGGI QUOTAZIONE FONDI  ALLA DATA EFFETTO DELLA TGA
      *----------------------------------------------------------------*
       S1300-LEGGI-QUOTZ.

           INITIALIZE  QUOTZ-FND-UNIT

           SET IDSV0003-SUCCESSFUL-RC      TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL     TO TRUE

           SET IDSV0003-TRATT-SENZA-STOR   TO TRUE
           SET IDSV0003-PRIMARY-KEY        TO TRUE
           SET IDSV0003-SELECT             TO TRUE

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                           TO L19-COD-COMP-ANIA
           MOVE DL19-COD-FND(IX-TAB-L19)   TO L19-COD-FND
           MOVE APPO-DATE                  TO APPO-EFFETTO
           MOVE APPO-EFFETTO               TO L19-DT-QTZ


           CALL IDBSL190  USING  IDSV0003 QUOTZ-FND-UNIT
           ON EXCEPTION
              MOVE LDBS5950
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'ERRORE CALL IDBSL190'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL

           EVALUATE TRUE
               WHEN IDSV0003-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CON SUCCESSO
                    IF L19-VAL-QUO-NULL NOT = HIGH-VALUE OR
                                        LOW-VALUE OR SPACES
      *                MOVE L19-VAL-QUO  TO DL19-VAL-QUO(IX-TAB-L19)
                       MOVE L19-VAL-QUO  TO WK-VAL-QUO
                    END-IF

               WHEN IDSV0003-NOT-FOUND
      *-->     CHIAVE NON TROVATA
      *             CONTINUE
                    MOVE ZERO    TO WK-VAL-QUO

               WHEN OTHER
      *-->     ERRORE DB
                  SET IDSV0003-INVALID-OPER  TO TRUE
                  MOVE WK-PGM
                    TO IDSV0003-COD-SERVIZIO-BE
                  STRING 'ERRORE LETTURA TABELLA IDBSL190 ;'
                        IDSV0003-RETURN-CODE ';'
                        IDSV0003-SQLCODE
                    DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                  END-STRING
           END-EVALUATE.


       S1300-EX.
           EXIT.


      *----------------------------------------------------------------*
      * LEGGI QUOTAZIONE FONDI AGGIUNTI ALLA DATA 31- 12 - ANNO-1
      *----------------------------------------------------------------*
       S1310-LEGGI-QUOTZ-AGG.

           INITIALIZE  QUOTZ-AGG-FND

           SET IDSV0003-SUCCESSFUL-RC      TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL     TO TRUE

           SET IDSV0003-TRATT-SENZA-STOR   TO TRUE
           SET IDSV0003-PRIMARY-KEY        TO TRUE
           SET IDSV0003-SELECT             TO TRUE

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                           TO L41-COD-COMP-ANIA
           MOVE DL19-COD-FND(IX-TAB-L19)   TO L41-COD-FND
           MOVE APPO-DATE                  TO APPO-EFFETTO
           MOVE APPO-EFFETTO               TO L41-DT-QTZ
           MOVE 'CR'                       TO L41-TP-QTZ


           CALL IDBSL410  USING  IDSV0003 QUOTZ-AGG-FND
           ON EXCEPTION
              MOVE LDBS5950
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'ERRORE CALL IDBSL410'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL

           EVALUATE TRUE
               WHEN IDSV0003-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CON SUCCESSO
      *             MOVE L41-VAL-QUO  TO DL19-VAL-QUO(IX-TAB-L19)
                    MOVE L41-VAL-QUO  TO WK-VAL-QUO

               WHEN IDSV0003-NOT-FOUND
      *-->     CHIAVE NON TROVATA
                    PERFORM S1300-LEGGI-QUOTZ
                       THRU S1300-EX
      *             CONTINUE

               WHEN OTHER
      *-->     ERRORE DB
                  SET IDSV0003-INVALID-OPER  TO TRUE
                  MOVE WK-PGM
                    TO IDSV0003-COD-SERVIZIO-BE
                  STRING 'ERRORE LETTURA TABELLA IDBSL410 ;'
                        IDSV0003-RETURN-CODE ';'
                        IDSV0003-SQLCODE
                    DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                  END-STRING
           END-EVALUATE.


       S1310-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   CHIUSURA CURSORE VALORE ASSET VALORE ASSETT
      *----------------------------------------------------------------*
       S1263-CHIUDE-CURVAS.
      *
           SET IDSV0003-CLOSE-CURSOR            TO TRUE.
      *
           CALL LDBS6000 USING  IDSV0003 VAL-AST
             ON EXCEPTION
                MOVE LDBS6000
                  TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'ERRORE CALL LDBS6000'
                  TO IDSV0003-DESCRIZ-ERR-DB2
                 SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           IF NOT IDSV0003-SUCCESSFUL-RC
           OR NOT IDSV0003-SUCCESSFUL-SQL
              MOVE LDBS6000
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CALL LDBS6000'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER
                TO TRUE
           END-IF.
      *
       S1263-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.




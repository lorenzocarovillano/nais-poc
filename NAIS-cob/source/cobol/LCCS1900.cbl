       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LCCS1900.
       AUTHOR.             ATS NAPOLI.
       DATE-WRITTEN.       MARZO 2015.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LCCS1900
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... GETRA
      *  FUNZIONE....... ROUTINE CONCOMITANZA
      *  DESCRIZIONE.... VERIFICA FLAG ESEGUIBILITA S/N
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.

       DATA DIVISION.
      *
      *
       WORKING-STORAGE SECTION.

       01 WK-PGM                           PIC X(008) VALUE 'LCCS1900'.


      *---------------------------------------------------------------*
      * PGM CHIAMATI
      *---------------------------------------------------------------*
       77  LDBS0640                        PIC X(008) VALUE 'LDBS0640'.
       77  LDBSF610                        PIC X(008) VALUE 'LDBSF610'.
       77  PGM-IDBSALL0                    PIC X(008) VALUE 'IDBSALL0'.
       77  IDBSSDI0                        PIC X(008) VALUE 'IDBSSDI0'.
       77  PGM-IDBSRRE0                    PIC X(008) VALUE 'IDBSRRE0'.

      *----------------------------------------------------------------*
      *    COPY DISPATCHER
      *----------------------------------------------------------------*
       01 IDSV0012.
          COPY IDSV0012.
      *
       01 DISPATCHER-VARIABLES.
      ** COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
          COPY IDSI0011.
      ** COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
          COPY IDSO0011.
      *----------------------------------------------------------------*
      *      COPY VARIABILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IEAV9903.
           COPY IDSV0002.


      *
      *----------------------------------------------------------------*
      *    CAMPI X ROUTINES DI GESTIONE DISPLAY IDSP8888
      *----------------------------------------------------------------*
           COPY IDSV8888.

      *--> AREE WORKING
      *-- AREA ASSET ALLOCATION
       01 WALL-AREA-ASSET.
          COPY LCCVALL7.
          COPY LCCVALL1              REPLACING ==(SF)== BY ==WALL==.

      *----------------------------------------------------------------*
      *    VARIABILI DI COMODO
      *----------------------------------------------------------------*
       01 WK-VARIABILI.
          05 WK-DT-RICOR-SUCC                PIC 9(008).
          05 WK-COD-CAN                      PIC S9(5)V     COMP-3.
          05 WK-LABEL                        PIC X(030).


      *----------------------------------------------------------------*
      *    FLAG
      *----------------------------------------------------------------*
       01 TROVATA-GAR          PIC X(001).
          88 TROVATA-GAR-SI    VALUE 'S'.
          88 TROVATA-GAR-NO    VALUE 'N'.


       01 AREA-INDICI.
          03 IX-LCCC1901        PIC 9(004).
          03 IX-TAB-ALL         PIC 9(004).


      *----------------------------------------------------------------*
      *    COPY TABELLE DB2                                            *
      *----------------------------------------------------------------*
          COPY IDBVPMO1.
          COPY IDBVPMO2.
          COPY IDBVSDI1.
          COPY IDBVSDI2.
          COPY IDBVALL1.
          COPY IDBVALL2.
          COPY IDBVRRE1.
          COPY IDBVRRE2.


      *----------------------------------------------------------------*
      *   COPY PER LDBS*                                               *
      *----------------------------------------------------------------*
          COPY LDBV0641.


      *----------------------------------------------------------------*
      * COPY TIPOLOGICHE
      *----------------------------------------------------------------*
           COPY LCCVXOG0.
           COPY LCCVXSB0.
           COPY LCCVXMVZ.
           COPY LCCVXMV1.
           COPY LCCVXMV2.
           COPY LCCVXMV3.
           COPY LCCVXMV5.
           COPY LCCVXMV6.

      *----------------------------------------------------------------*
        LINKAGE SECTION.
      *----------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.

       01 WCOM-AREA-STATI.
           COPY LCCC0001                REPLACING ==(SF)== BY ==WCOM==.

      *-- AREA POLIZZA
       01 WPOL-AREA-POLIZZA.
          04 WPOL-ELE-POLI-MAX             PIC S9(004) COMP.
          04 WPOL-TAB-POLI.
           COPY LCCVPOL1                REPLACING ==(SF)== BY ==WPOL==.

      *--- ADESIONE
       01 WADE-AREA-ADESIONE.
          04 WADE-ELE-ADES-MAX             PIC S9(004) COMP.
          04 WADE-TAB-ADES.
           COPY LCCVADE1                REPLACING ==(SF)== BY ==WADE==.


      * -- INPUT: AREA TARIFFE GAR
       01 LCCC1901-AREA.
           COPY LCCC1901.
      *----------------------------------------------------------------*
      *     P R O C E D U R E    D I V I S I O N
      *----------------------------------------------------------------*
       PROCEDURE DIVISION                  USING AREA-IDSV0001
                                                 WCOM-AREA-STATI
                                                 WPOL-AREA-POLIZZA
                                                 WADE-AREA-ADESIONE
                                                 LCCC1901-AREA.
      *----------------------------------------------------------------*
      *
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.
      *
           PERFORM S9000-OPERAZ-FINALI
              THRU EX-S9000.

           GOBACK.
      *----------------------------------------------------------------*
      * OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           MOVE 'S0000-OPERAZIONI-INIZIALI'    TO WK-LABEL.

           INITIALIZE                             WK-VARIABILI.




       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      * ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           MOVE 'S1000-ELABORAZIONE'         TO WK-LABEL.

           MOVE IDSV0001-TIPO-MOVIMENTO      TO WS-MOVIMENTO.

           SET LCCC1901-FL-ESEGUIBILE-SI     TO TRUE
           IF  IDSV0001-ON-LINE
           AND COMUN-SWITCH-FND
               CONTINUE
           ELSE
              IF IDSV0001-ESITO-OK
                 PERFORM RECUPERA-RAPP-RETE
                    THRU RECUPERA-RAPP-RETE-EX
              END-IF
UBI   *       IF WK-COD-CAN = 3 OR 4
UBI           IF WK-COD-CAN = 4

                 IF IDSV0001-ESITO-OK
                    PERFORM LEGGI-PARAM-MOVI
                       THRU LEGGI-PARAM-MOVI-EX
                 END-IF
                 IF WK-DT-RICOR-SUCC > ZERO
                    IF IDSV0001-ESITO-OK
                       IF COMUN-SWITCH-FND
13373                     IF WK-DT-RICOR-SUCC <= IDSV0001-DATA-EFFETTO
                             SET LCCC1901-FL-ESEGUIBILE-NO TO TRUE
                          END-IF
                       END-IF
                    END-IF

                    IF IDSV0001-ESITO-OK
                    AND (COMUN-RISPAR-IND
                         OR VERSAM-AGGIUNTIVO)
13373                   IF WK-DT-RICOR-SUCC <= IDSV0001-DATA-EFFETTO
                           IF LCCC1901-GAR-MAX > 0
                              PERFORM LEGGI-STRA-INVST
                                 THRU LEGGI-STRA-INVST-EX

                              IF IDSV0001-ESITO-OK
                                 PERFORM VERIFICA-GRZ
                                    THRU VERIFICA-GRZ-EX
                              END-IF

                              IF TROVATA-GAR-SI
                                 SET LCCC1901-FL-ESEGUIBILE-NO TO TRUE
                              END-IF

                           END-IF
                        END-IF
                    END-IF
                 END-IF
              END-IF
           END-IF.
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *--> VERIFICA I CODICI TARIFFA DA ELIMINARE/AGGIUNGERE CON LA
      *--> STRATEGIA D INVESTIMENTO
      *----------------------------------------------------------------*
       VERIFICA-GRZ.

           SET TROVATA-GAR-NO           TO TRUE

           PERFORM VARYING IX-LCCC1901 FROM 1 BY 1
                    UNTIL  IX-LCCC1901 > LCCC1901-GAR-MAX

             PERFORM VARYING IX-TAB-ALL FROM 1 BY 1
                       UNTIL IX-TAB-ALL > WALL-ELE-ASSET-ALL-MAX

                 IF LCCC1901-COD-TARI(IX-LCCC1901) =
                    WALL-COD-TARI(IX-TAB-ALL)
                    SET TROVATA-GAR-SI  TO TRUE
                 END-IF
             END-PERFORM
           END-PERFORM.

       VERIFICA-GRZ-EX.
           EXIT.
      *----------------------------------------------------------------*
      *--> RECUPERA ASSET ALLOCATION PER LA STRATEGIA DEL CONTRATTO
      *----------------------------------------------------------------*
       LEGGI-AST-ALLOC.

           MOVE 'LEGGI-AST-ALLOC'
              TO WK-LABEL

           INITIALIZE IDSI0011-BUFFER-DATI
                      AST-ALLOC.
           MOVE ZERO
              TO WALL-ELE-ASSET-ALL-MAX

      *--> TRATTAMENTO STORICITA
           SET IDSI0011-TRATT-X-COMPETENZA TO TRUE

      *--> TIPO OPERAZIONE
           SET IDSI0011-FETCH-FIRST      TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.
      *
      *--> LIVELLO OPERAZIONE
           SET IDSI0011-ID-PADRE      TO TRUE

           MOVE SDI-ID-STRA-DI-INVST  TO ALL-ID-STRA-DI-INVST
           MOVE 'AST-ALLOC'           TO IDSI0011-CODICE-STR-DATO
           MOVE AST-ALLOC             TO IDSI0011-BUFFER-DATI

           MOVE ZEROES                TO IDSI0011-DATA-INIZIO-EFFETTO
                                         IDSI0011-DATA-FINE-EFFETTO
           MOVE ZEROES                TO IDSI0011-DATA-COMPETENZA

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR IDSV0001-ESITO-KO
      *---
              PERFORM CALL-DISPATCHER
                 THRU CALL-DISPATCHER-EX
      *---
              IF IDSO0011-SUCCESSFUL-RC

                 EVALUATE TRUE
      *-->         OPERAZIONE ESEGUITA CORRETTAMENTE
                   WHEN IDSO0011-SUCCESSFUL-SQL
                      MOVE IDSO0011-BUFFER-DATI TO AST-ALLOC
                      ADD  1
                        TO WALL-ELE-ASSET-ALL-MAX

                      MOVE WALL-ELE-ASSET-ALL-MAX
                         TO IX-TAB-ALL

                      PERFORM VALORIZZA-OUTPUT-ALL
                         THRU VALORIZZA-OUTPUT-ALL-EX
                      SET WALL-ST-INV(IX-TAB-ALL)
                       TO TRUE
                      MOVE IX-TAB-ALL
                        TO WALL-ELE-ASSET-ALL-MAX
                      SET IDSI0011-FETCH-NEXT
                       TO TRUE


      *--->        CHIAVE NON TROVATA
                   WHEN IDSO0011-NOT-FOUND
                      IF IDSI0011-FETCH-FIRST
                         MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                         MOVE WK-LABEL      TO IEAI9901-LABEL-ERR
                         MOVE '005016'      TO IEAI9901-COD-ERRORE
                         MOVE SPACES        TO IEAI9901-PARAMETRI-ERR
                         STRING PGM-IDBSALL0 '; '

                           IDSO0011-RETURN-CODE ';'
                           IDSO0011-SQLCODE
                           DELIMITED BY SIZE
                           INTO IEAI9901-PARAMETRI-ERR
                         END-STRING
                         PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            THRU EX-S0300
                      END-IF

      *--->        ERRORE DI ACCESSO AL DB
                   WHEN OTHER
                      MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
                      MOVE WK-LABEL         TO IEAI9901-LABEL-ERR
                      MOVE '005016'         TO IEAI9901-COD-ERRORE
                      MOVE SPACES           TO IEAI9901-PARAMETRI-ERR

                      STRING PGM-IDBSALL0 '; '

                        IDSO0011-RETURN-CODE ';'
                        IDSO0011-SQLCODE
                        DELIMITED BY SIZE
                        INTO IEAI9901-PARAMETRI-ERR
                      END-STRING
                      PERFORM S0300-RICERCA-GRAVITA-ERRORE
                         THRU EX-S0300
                 END-EVALUATE
              ELSE
      *--->      GESTIRE ERRORE
                 MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL              TO IEAI9901-LABEL-ERR
                 MOVE '005016'              TO IEAI9901-COD-ERRORE
                 MOVE SPACES                TO IEAI9901-PARAMETRI-ERR

                 STRING PGM-IDBSALL0 '; '

                   IDSO0011-RETURN-CODE ';'
                   IDSO0011-SQLCODE
                   DELIMITED BY SIZE
                   INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
                 END-IF

           END-PERFORM.


       LEGGI-AST-ALLOC-EX.
           EXIT.
      *----------------------------------------------------------------*
      *--> RECUPERA LA STRATEGIA D INVESTIMENTO PRESENTE SUL CONTRATTO
      *----------------------------------------------------------------*
       LEGGI-STRA-INVST.
           MOVE 'LEGGI-STRA-INVST'
              TO WK-LABEL

           INITIALIZE STRA-DI-INVST.
      *
           MOVE WADE-ID-ADES
             TO SDI-ID-OGG
           SET ADESIONE
             TO TRUE
           MOVE WS-TP-OGG
             TO SDI-TP-OGG
      *
           MOVE ZEROES
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
                IDSI0011-DATA-COMPETENZA.
      *
            SET IDSI0011-TRATT-DEFAULT
             TO TRUE.
      *
           MOVE 'STRA-DI-INVST'
             TO IDSI0011-CODICE-STR-DATO.
      *
           MOVE STRA-DI-INVST
             TO IDSI0011-BUFFER-DATI.
      *
            SET IDSI0011-SELECT
             TO TRUE.

            SET IDSI0011-ID-OGGETTO
             TO TRUE.
      *
            SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
            SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
            PERFORM CALL-DISPATCHER
               THRU CALL-DISPATCHER-EX.

             IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL
                         TO IEAI9901-LABEL-ERR
                       MOVE '005015'
                         TO IEAI9901-COD-ERRORE
                       STRING 'STRAT.DI INVEST. NON TROVATA ' ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE
                       INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
                       MOVE IDSO0011-BUFFER-DATI
                         TO STRA-DI-INVST
      *
                       PERFORM LEGGI-AST-ALLOC
                          THRU LEGGI-AST-ALLOC-EX

                  WHEN OTHER
      *
      *  --> Errore Lettura
      *
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL
                         TO IEAI9901-LABEL-ERR
                       MOVE '005015'
                         TO IEAI9901-COD-ERRORE
                       STRING 'ERRORE LETTURA STRAT.DI INVEST. ' ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE
                       INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore Dispatcher
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              MOVE 'ERRORE DISPATCHER LETTURA STRAT.DI INVEST.'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
       LEGGI-STRA-INVST-EX.
           EXIT.
      *----------------------------------------------------------------*
      *--> RECUPERA IL CANALE DI POLIZZA
      *----------------------------------------------------------------*
       RECUPERA-RAPP-RETE.
           MOVE 'RECUPERA-RAPP-RETE'
              TO WK-LABEL

           INITIALIZE IDSI0011-BUFFER-DATI
                      RAPP-RETE

           MOVE WPOL-ID-POLI            TO RRE-ID-OGG
           SET POLIZZA                  TO TRUE
           MOVE WS-TP-OGG               TO RRE-TP-OGG

           MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO
                                           IDSI0011-DATA-FINE-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA

           MOVE 'RAPP-RETE'             TO IDSI0011-CODICE-STR-DATO
           MOVE  RAPP-RETE              TO IDSI0011-BUFFER-DATI
           MOVE  SPACES                 TO IDSI0011-BUFFER-WHERE-COND


           SET IDSI0011-FETCH-FIRST               TO TRUE
           SET IDSI0011-ID-OGGETTO                TO TRUE
           SET IDSI0011-TRATT-X-COMPETENZA        TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSO0011-SUCCESSFUL-RC            TO TRUE
           SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR IDSI0011-CLOSE-CURSOR

             PERFORM CALL-DISPATCHER   THRU CALL-DISPATCHER-EX

              IF IDSO0011-SUCCESSFUL-RC
                 EVALUATE TRUE

                    WHEN IDSO0011-SUCCESSFUL-SQL
      *-->            OPERAZIONE ESEGUITA CORRETTAMENTE
                      MOVE IDSO0011-BUFFER-DATI   TO RAPP-RETE
                      IF RRE-COD-CAN-NULL NOT EQUAL HIGH-VALUE
                         MOVE RRE-COD-CAN
                           TO WK-COD-CAN
                         PERFORM B200-CLOSE-CURSOR
                            THRU B200-CLOSE-CURSOR-EX
                      ELSE
                         SET IDSI0011-FETCH-NEXT  TO TRUE
                      END-IF

                    WHEN IDSO0011-NOT-FOUND
      *-->               GESTIONE ERRORE
                         MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                         MOVE WK-LABEL     TO IEAI9901-LABEL-ERR
                         MOVE '005016'     TO IEAI9901-COD-ERRORE
                         MOVE SPACES       TO IEAI9901-PARAMETRI-ERR
                         STRING PGM-IDBSRRE0         ';'
                                IDSO0011-RETURN-CODE ';'
                                IDSO0011-SQLCODE
                                DELIMITED BY SIZE
                                INTO IEAI9901-PARAMETRI-ERR
                         END-STRING

                         PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            THRU EX-S0300


                    WHEN OTHER
      *--->              EROREDI ACCESSO AL DB
                         MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                         MOVE WK-LABEL     TO IEAI9901-LABEL-ERR
                         MOVE '005016'     TO IEAI9901-COD-ERRORE
                         MOVE SPACES       TO IEAI9901-PARAMETRI-ERR
                         STRING PGM-IDBSRRE0         ';'
                                IDSO0011-RETURN-CODE ';'
                                IDSO0011-SQLCODE
                                DELIMITED BY SIZE
                                INTO IEAI9901-PARAMETRI-ERR
                         END-STRING

                         PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            THRU EX-S0300


                 END-EVALUATE
              ELSE
      *-->       GESTIRE ERRORE
                 MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL           TO IEAI9901-LABEL-ERR
                 MOVE '005016'           TO IEAI9901-COD-ERRORE
                 MOVE SPACES             TO IEAI9901-PARAMETRI-ERR
                 STRING PGM-IDBSRRE0         ';'
                        IDSO0011-RETURN-CODE ';'
                        IDSO0011-SQLCODE
                        DELIMITED BY SIZE
                        INTO IEAI9901-PARAMETRI-ERR
                 END-STRING

                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300

              END-IF

           END-PERFORM.


       RECUPERA-RAPP-RETE-EX.
           EXIT.

      *----------------------------------------------------------------*
      * FORZO LA CHIUSURA DEL CURSORE SULLA RAPP-RETE
      *----------------------------------------------------------------*
       B200-CLOSE-CURSOR.

           MOVE 'B200-CLOSE-CURSOR'      TO WK-LABEL.
           MOVE WK-LABEL                 TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY        THRU ESEGUI-DISPLAY-EX.

           INITIALIZE IDSI0011-BUFFER-DATI
                      IDSI0011-OPERAZIONE.

           MOVE 'RAPP-RETE'            TO IDSI0011-CODICE-STR-DATO
           MOVE  SPACES                TO IDSI0011-BUFFER-DATI
           MOVE  SPACES                TO IDSI0011-BUFFER-WHERE-COND

           SET IDSI0011-CLOSE-CURSOR                     TO TRUE.
           SET IDSI0011-ID-OGGETTO                       TO TRUE.
           SET IDSI0011-TRATT-X-COMPETENZA               TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET IDSO0011-SUCCESSFUL-RC                    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL                   TO TRUE.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->            OPERAZIONE ESEGUITA CORRETTAMENTE
                      CONTINUE

                  WHEN OTHER
      *-->              ERRORE DI ACCESSO AL DB
                        MOVE WK-PGM      TO IEAI9901-COD-SERVIZIO-BE
                        MOVE WK-LABEL    TO IEAI9901-LABEL-ERR
                        MOVE '005016'    TO IEAI9901-COD-ERRORE
                        MOVE SPACES      TO IEAI9901-PARAMETRI-ERR
                        STRING 'B200-CLOSE-CURSOR'       ';'
                             IDSO0011-RETURN-CODE        ';'
                             IDSO0011-SQLCODE
                             DELIMITED BY SIZE
                             INTO IEAI9901-PARAMETRI-ERR
                        END-STRING
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                           THRU EX-S0300

              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL              TO IEAI9901-LABEL-ERR
              MOVE '005016'              TO IEAI9901-COD-ERRORE
              MOVE SPACES                TO IEAI9901-PARAMETRI-ERR
              STRING 'B200-CLOSE-CURSOR'       ';'
                     IDSO0011-RETURN-CODE      ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300

           END-IF.

       B200-CLOSE-CURSOR-EX.
           EXIT.

      *----------------------------------------------------------------*
      *--> RECUPERA LA PARAM_MOVI DI GENERAZIONE TRANCHE
      *----------------------------------------------------------------*
       LEGGI-PARAM-MOVI.
           MOVE 'LEGGI-PARAM-MOVI'      TO WK-LABEL.
           MOVE WK-LABEL                  TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY         THRU ESEGUI-DISPLAY-EX.

           INITIALIZE PARAM-MOVI
                      LDBV0641
                      IDSI0011-BUFFER-DATI.

           SET IDSI0011-TRATT-DEFAULT    TO TRUE.
           SET IDSI0011-FETCH-FIRST      TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.

           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
           SET IDSV0001-ESITO-OK         TO TRUE.

           MOVE WPOL-ID-POLI             TO LDBV0641-ID-POLI.


           MOVE 6003             TO LDBV0641-TP-MOVI-1



      *    MOVE IDSV0001-TIPO-MOVIMENTO  TO WS-MOVIMENTO

           MOVE LDBS0640                 TO IDSI0011-CODICE-STR-DATO.
           MOVE PARAM-MOVI               TO IDSI0011-BUFFER-DATI.
           MOVE LDBV0641                 TO IDSI0011-BUFFER-WHERE-COND.

           MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.
           MOVE ZERO
              TO WK-DT-RICOR-SUCC
           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC  OR
                         NOT IDSO0011-SUCCESSFUL-SQL OR
                         NOT IDSV0001-ESITO-OK

               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *-->            SE CHIAVE NON TROVATA

                         CONTINUE

                      WHEN IDSO0011-SUCCESSFUL-SQL

                         MOVE IDSO0011-BUFFER-DATI    TO PARAM-MOVI

                         IF PMO-DT-RICOR-SUCC-NULL NOT = HIGH-VALUE
                                                     AND LOW-VALUE
                                                     AND SPACE
                            IF WK-DT-RICOR-SUCC > PMO-DT-RICOR-SUCC
                              CONTINUE
                            ELSE
                               MOVE PMO-DT-RICOR-SUCC
                                  TO WK-DT-RICOR-SUCC
                            END-IF
                         END-IF

                         SET IDSI0011-FETCH-NEXT      TO TRUE

                      WHEN OTHER
      *-->            ERRORE DI ACCESSO AL DB
                         MOVE WK-PGM
                           TO IEAI9901-COD-SERVIZIO-BE
                         MOVE 'S1200-CERCA-PARAM-MOV'
                           TO IEAI9901-LABEL-ERR
                         MOVE '005016'
                           TO IEAI9901-COD-ERRORE
                         STRING IDSI0011-CODICE-STR-DATO ';'
                                IDSO0011-RETURN-CODE     ';'
                                IDSO0011-SQLCODE
                                DELIMITED BY SIZE
                                INTO IEAI9901-PARAMETRI-ERR
                         END-STRING
                         PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            THRU EX-S0300
                  END-EVALUATE
               ELSE
      *->         ERRORE DISPATCHER
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S1200-CERCA-PARAM-MOV'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005016'
                    TO IEAI9901-COD-ERRORE
                  STRING IDSI0011-CODICE-STR-DATO ';'
                         IDSO0011-RETURN-CODE     ';'
                         IDSO0011-SQLCODE
                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

       LEGGI-PARAM-MOVI-EX.
           EXIT.



      *----------------------------------------------------------------*
      * OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZ-FINALI.

           CONTINUE.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IERP9901.
           COPY IERP9902.
      *
      *----------------------------------------------------------------*
      *    ROUTINES DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.
      *----------------------------------------------------------------*
      *    ROUTINES DI GESTIONE DISPLAY
      *----------------------------------------------------------------*
           COPY IDSP8888.
      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE AREE TABELLE
      *----------------------------------------------------------------*
      *
            COPY LCCVALL3                REPLACING ==(SF)== BY ==WALL==.

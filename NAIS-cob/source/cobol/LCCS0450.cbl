       IDENTIFICATION DIVISION.
-----> PROGRAM-ID.    LCCS0450.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                      *
      * F A S E         : SERVIZIO PER POPOLAMENTO MATRICE             *
      *                   RIEPILOGATIVA DELLA SITUAZIONE DEI FONDI     *
      *                   PER LA RISERVA MATEMATICA                    *
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
      *----------------------------------------------------------------*
       WORKING-STORAGE SECTION.
       77  WK-PGM                          PIC X(08) VALUE 'LCCS0450'.

      *----------------------------------------------------------------*
      *    PGM CHIAMATI
      *----------------------------------------------------------------*
       77  PGM-LDBS0130                    PIC X(08) VALUE 'LDBS0130'.
       77  PGM-IDBSRIF0                    PIC X(08) VALUE 'IDBSRIF0'.
       77  PGM-IDBSRDF0                    PIC X(08) VALUE 'IDBSRDF0'.
       77  PGM-LDBS4910                    PIC X(08) VALUE 'LDBS4910'.
       77  PGM-LDBS2080                    PIC X(08) VALUE 'LDBS2080'.
       77  PGM-IDBSL410                    PIC X(08) VALUE 'IDBSL410'.
      *--> Routine date
       01  LCCS0003                        PIC X(08) VALUE 'LCCS0003'.

      *----------------------------------------------------------------*
      *    COPY PER SERVIZIO VERIFICA CALCOLI SU DATE
      *----------------------------------------------------------------*
           COPY LCCC0003.

       01  IN-RCODE                        PIC 9(02) VALUE ZEROES.

      *----------------------------------------------------------------*
      *    AREA HOST VARIABLES
      *----------------------------------------------------------------*
           COPY IDBVGRZ1.
           COPY IDBVTGA1.
           COPY IDBVRDF1.
           COPY IDBVRIF1.
           COPY IDBVVAS1.
           COPY IDBVL191.
           COPY IDBVL411.

      *----------------------------------------------------------------*
      *    COPY X LDBS*
      *----------------------------------------------------------------*
           COPY LDBV0011.
           COPY LDBV4911.

      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       77  VALORE-POSITIVO             PIC X(02) VALUE 'VP'.
       77  ANNULLO-POSITIVO            PIC X(02) VALUE 'AP'.
       77  VALORE-NEGATIVO             PIC X(02) VALUE 'VN'.
       77  ANNULLO-NEGATIVO            PIC X(02) VALUE 'AN'.
       77  VAS-LIQUIDAZIONE            PIC X(02) VALUE 'LQ'.
       77  ANNULLO-LIQUIDAZIONE        PIC X(02) VALUE 'AL'.
       77  MAX-VAL-L19                 PIC 9(04) COMP VALUE 250.

      *----------------------------------------------------------------*
      *   FLAGS
      *----------------------------------------------------------------*
       01 FLAG-CUR-GAR                 PIC X(01).
          88 INIT-CUR-GAR              VALUE 'S'.
          88 FINE-CUR-GAR              VALUE 'N'.

       01 FLAG-CUR-TGA                 PIC X(01).
          88 INIT-CUR-TGA              VALUE 'S'.
          88 FINE-CUR-TGA              VALUE 'N'.

       01 FLAG-CUR-VAS                 PIC X(01).
          88 INIT-CUR-VAS              VALUE 'S'.
          88 FINE-CUR-VAS              VALUE 'N'.

       01 FLAG-CUR-DTC                 PIC X(01).
          88 INIT-CUR-DTC              VALUE 'S'.
          88 FINE-CUR-DTC              VALUE 'N'.

       01 FLAG-CUR-ADE                 PIC X(01).
          88 INIT-CUR-ADE              VALUE 'S'.
          88 FINE-CUR-ADE              VALUE 'N'.

       01 FLAG-QTZ-AGG                 PIC X(01).
          88 QTZ-AGG-OK                VALUE 'S'.
          88 QTZ-AGG-KO                VALUE 'N'.

       01 FLAG-TP-TRCH                 PIC X(01).
          88 TRANCHE-POSITIVA          VALUE 'S'.
          88 TRANCHE-NEGATIVA          VALUE 'N'.

       01 FLAG-FONDO                   PIC X(01).
          88 FONDO-TROVATO             VALUE 'S'.
          88 FONDO-NON-TROVATO         VALUE 'N'.

      *----------------------------------------------------------------*
      *   VARIABILI
      *----------------------------------------------------------------*
       01 VARIABILI.
          03 WK-NUMERO-QUOTE             PIC S9(07)V9(5) COMP-3.
          03 WK-CONTROVALORE             PIC S9(12)V9(3) COMP-3.
          03 WK-DT-RICOR-TRANCHE-NUM     PIC 9(8).
          03 WK-DT-RICOR-TRANCHE REDEFINES
             WK-DT-RICOR-TRANCHE-NUM.
             04 WK-DT-RICOR-TRANCHE-AA   PIC 9(4).
             04 WK-DT-RICOR-TRANCHE-MM   PIC 9(2).
             04 WK-DT-RICOR-TRANCHE-GG   PIC 9(2).
          03 WK-ANNO                     PIC 9(4).
          03 WK-LABEL                    PIC X(30).

      *----------------------------------------------------------------*
      *   TABELLE DI APPOGGIO
      *----------------------------------------------------------------*
      *01 TAB-APPOGGIO.
      *   03 WK-RAMO-GAR.
      *      04 WK-MAX-ELE-RAMO        PIC S9(04) COMP VALUE ZEROES.
      *      04 WK-TAB-RAMO1.
      *         05 WK-TAB-RAMO-GAR        OCCURS 100.
      *            06 WK-ID-GAR-TAB       PIC S9(09)V COMP-3.
      *            06 WK-RAMO-TAB         PIC  X(12).
      *            06 WK-COD-TARI-TAB     PIC  X(12).
      *            06 WK-COD-FONDO-TAB    PIC  X(12).
      *      04 WK-TAB-RAMO1-R REDEFINES WK-TAB-RAMO1.
      *         05 FILLER                 PIC  X(41).
      *         05 WK-RESTO-TAB-RAMO1     PIC  X(4059).

       01  WCOM-TABELLE.
      *--  AREA GARANZIA
           03 WGAR-AREA-GARANZIA.
              04 WGAR-ELE-GAR-MAX        PIC S9(04) COMP VALUE ZEROES.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGAR==.
              COPY LCCVGRZ1              REPLACING ==(SF)== BY ==WGAR==.
      *----------------------------------------------------------------*
      *   INDICI
      *----------------------------------------------------------------*
       01 INDICI.
          03 IX-TAB-GRZ                PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-TGA                PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-MAT                PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-TOT                PIC 9(4) COMP VALUE ZEROES.
          03 IX-IND-TAR                PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-DTC                PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-ADE                PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-GAR                PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-L19                PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-WL19               PIC 9(4) COMP VALUE ZEROES.
      *----------------------------------------------------------------*
      *    COPY GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
           COPY IDSV0003.
           COPY IDSV0010.

      *----------------------------------------------------------------*
      *    CAMPI X ROUTINES DI GESTIONE DISPLAY IDSP8888
      *----------------------------------------------------------------*
           COPY IDSV8888.

      *----------------------------------------------------------------*
      *    TIPOLOGICHE DI PORTAFOGLIO
      *----------------------------------------------------------------*
           COPY LCCVXTH0.

      *----------------------------------------------------------------*
      *    COPY DISPATHCER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      *--> COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *--> COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.

      *-----------------------------------------------------------------
      *    LINKAGE
      *-----------------------------------------------------------------
       LINKAGE SECTION.

      *-----------------------------------------------------------------
      *    CAMPI DI ESITO, AREE DI SERVIZIO
      *-----------------------------------------------------------------
       01  AREA-IDSV0001.
           COPY IDSV0001.

      *--  AREA GARANZIA
       01  WGRZ-AREA-GARANZIA.
           04 WGRZ-ELE-GAR-MAX        PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
           COPY LCCVGRZ1              REPLACING ==(SF)== BY ==WGRZ==.

      *--  AREA TRANCHE DI GARANZIA
       01  WTGA-AREA-TRCH-DI-GAR.
           04 WTGA-ELE-TGA-MAX        PIC S9(04) COMP.
                COPY LCCVTGAC REPLACING   ==(SF)==  BY ==WTGA==.
           COPY LCCVTGA1              REPLACING ==(SF)== BY ==WTGA==.

      *--  AREA FONDI
       01  WL19-AREA-FONDI.
           COPY LCCVL197              REPLACING ==(SF)== BY ==WL19==.
           COPY LCCVL191              REPLACING ==(SF)== BY ==WL19==.

      ******************************************************************
      *   INPUT BUSINESS SERVICES
      ******************************************************************
           COPY LCCC0450.

      ******************************************************************
       PROCEDURE DIVISION             USING AREA-IDSV0001
                                            WGRZ-AREA-GARANZIA
                                            WTGA-AREA-TRCH-DI-GAR
                                            WL19-AREA-FONDI
                                            LCCC0450.

      *--> COPY PER L'INIZIALIZZAZIONE AREA ERRORI
      *--> DELL'AREA CONTESTO.
           COPY IERP0001.

           PERFORM A000-INIZIO              THRU A000-EX.

           IF IDSV0001-ESITO-OK
              PERFORM A100-ELABORA          THRU A100-EX
           END-IF.

           GOBACK.

      ******************************************************************
       A000-INIZIO.

           INITIALIZE  VAL-AST
      *                WK-MAX-ELE-RAMO
                       WK-NUMERO-QUOTE
                       WK-DT-RICOR-TRANCHE-NUM
                       WK-ANNO
                       VARIABILI
                       INDICI
                       WCOM-TABELLE
      *                WK-RAMO-GAR.

           MOVE 'A000-INIZIO'               TO WK-LABEL.

      *    inizializzazione tabella ramo
      *    INITIALIZE WK-TAB-RAMO-GAR(1).

      *    MOVE WK-TAB-RAMO1     TO WK-RESTO-TAB-RAMO1.

      *    INITIALIZE LCCC0450-AREA-OUTPUT.
           INITIALIZE LCCC0450-ELE-TAB-MAX
                      LCCC0450-MATRICE(1).

           MOVE LCCC0450-TAB-MATRICE TO LCCC0450-RESTO-TAB-MATRICE.

           MOVE ZEROES
             TO WL19-ELE-FND-MAX

           IF LCCC0450-FLAG-OPER  = 'TG' OR 'GA'
              CONTINUE
           ELSE
              MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
              MOVE '005166'   TO IEAI9901-COD-ERRORE
              STRING 'FLAG-OPERAZIONE = '
                     LCCC0450-FLAG-OPER
                     ' NON CONSENTITO'
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

           IF LCCC0450-FLAG-OPER = 'TG'
              IF WTGA-ELE-TGA-MAX > ZEROES
                 CONTINUE
              ELSE
                 MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                 MOVE '005166'   TO IEAI9901-COD-ERRORE
                 STRING 'OPERAZIONE NON CONSENTITA - '
                        'TRANCHE NON ESISTENTI'
                        DELIMITED BY SIZE
                        INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

           IF LCCC0450-FLAG-OPER = 'GA'
              IF WGRZ-ELE-GAR-MAX > ZEROES
                 CONTINUE
              ELSE
                 MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                 MOVE '005166'   TO IEAI9901-COD-ERRORE
                 STRING 'OPERAZIONE NON CONSENTITA - '
                        'GARANZIE NON ESISTENTI'
                        DELIMITED BY SIZE
                        INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

      *--> CONTROLLO SULLA DATA RISERVA
           IF LCCC0450-DATA-RISERVA IS NOT NUMERIC
           OR LCCC0450-DATA-RISERVA = ZEROES
              MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
              MOVE '005007'   TO IEAI9901-COD-ERRORE
              STRING 'DATA RISERVA'
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       A000-EX.
           EXIT.
      ******************************************************************
       A100-ELABORA.

           MOVE 'A100-ELABORA'            TO WK-LABEL.

      *--> SE FLAG DI MODALITA = 'GA' RECUPERA SOLO LE GARANZIA DI
      *--> RAMO III E LE RELATIVE TRANCHE
           IF LCCC0450-FLAG-OPER = 'GA'
              PERFORM A120-FILTRA-GAR-RAMO-III
                 THRU A120-EX

              PERFORM INIZIA-TOT-TGA
                 THRU INIZIA-TOT-TGA-EX
              VARYING IX-TAB-TGA FROM 1 BY 1
                UNTIL IX-TAB-TGA > 100

              MOVE ZEROES                 TO IX-TAB-TGA

              PERFORM A130-RECUP-TRCH-DI-GAR
                 THRU A130-EX
              VARYING IX-TAB-GAR FROM 1 BY 1
                UNTIL IX-TAB-GAR > WGAR-ELE-GAR-MAX
                   OR IDSV0001-ESITO-KO
           END-IF.

      *--> SE FLAG DI MODALITA = 'TG' ACCEDE ALLA VALOR ASSET
           PERFORM A140-RECUP-VALORE-ASSET
              THRU A140-EX
           VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TGA-MAX
                OR IDSV0001-ESITO-KO.

      *--> CALCOLO IL TOTALE DEL CONTRATTO
           IF IDSV0001-ESITO-OK
              PERFORM A180-CALC-TOTALE
                 THRU A180-EX
           END-IF.

       A100-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    FILTRA LE GARANZIA DI RAMO III
      *----------------------------------------------------------------*
       A120-FILTRA-GAR-RAMO-III.

           MOVE 'A120-FILTRA-GAR-RAMO-III'     TO WK-LABEL.

           PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
             UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX

             IF WGRZ-RAMO-BILA(IX-TAB-GRZ) = '3'
                ADD 1 TO WGAR-ELE-GAR-MAX
                         IX-TAB-GAR
                MOVE WGRZ-TAB-GAR(IX-TAB-GRZ)
                  TO WGAR-TAB-GAR(IX-TAB-GAR)
             END-IF

           END-PERFORM.

       A120-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA DATI TRANCHE DI GARANZIA
      *----------------------------------------------------------------*
       A130-RECUP-TRCH-DI-GAR.

           MOVE 'A130-RECUP-TRCH-DI-GAR'  TO WK-LABEL.
           MOVE WK-LABEL                  TO IDSV8888-AREA-DISPLAY
      *    PERFORM ESEGUI-DISPLAY         THRU ESEGUI-DISPLAY-EX.

           INITIALIZE IDSI0011-BUFFER-DATI.

           SET INIT-CUR-TGA               TO TRUE.
           SET IDSI0011-FETCH-FIRST       TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC     TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL    TO TRUE.
      *
           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR IDSV0001-ESITO-KO
                      OR FINE-CUR-TGA

             INITIALIZE LDBV0011

             SET IDSI0011-WHERE-CONDITION TO TRUE
             SET IDSI0011-TRATT-DEFAULT   TO TRUE

             MOVE LCCC0450-DATA-EFFETTO  TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
             MOVE LCCC0450-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA

             MOVE WGRZ-ID-GAR(IX-TAB-GRZ)  TO LDBV0011-ID-GAR
             MOVE LDBV0011                 TO IDSI0011-BUFFER-WHERE-COND
             MOVE 'LDBS0130'               TO IDSI0011-CODICE-STR-DATO
             MOVE SPACES                   TO IDSI0011-BUFFER-DATI

             PERFORM CALL-DISPATCHER      THRU CALL-DISPATCHER-EX

             IF IDSO0011-SUCCESSFUL-RC
                EVALUATE TRUE
                    WHEN IDSO0011-NOT-FOUND
      *-->          NESSUN DATO IN TABELLA
                       SET FINE-CUR-TGA TO TRUE

                       IF IDSI0011-FETCH-FIRST
                          MOVE WK-PGM   TO IEAI9901-COD-SERVIZIO-BE
                          MOVE WK-LABEL TO IEAI9901-LABEL-ERR
                          MOVE '005016' TO IEAI9901-COD-ERRORE
                          STRING PGM-LDBS0130         ';'
                                 IDSO0011-RETURN-CODE ';'
                                 IDSO0011-SQLCODE
                                 DELIMITED BY SIZE
                            INTO IEAI9901-PARAMETRI-ERR
                          END-STRING
                          PERFORM S0300-RICERCA-GRAVITA-ERRORE
                             THRU EX-S0300
                       END-IF

                    WHEN IDSO0011-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI   TO TRCH-DI-GAR

                       ADD 1                       TO IX-TAB-TGA
                       SET WTGA-ST-INV(IX-TAB-TGA) TO TRUE
                       MOVE IX-TAB-TGA             TO WTGA-ELE-TGA-MAX

                       PERFORM VALORIZZA-OUTPUT-TGA
                          THRU VALORIZZA-OUTPUT-TGA-EX

                       SET IDSI0011-FETCH-NEXT     TO TRUE

                    WHEN OTHER
      *--->         ERRORE DI ACCESSO AL DB
                       MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                       MOVE '005016'   TO IEAI9901-COD-ERRORE
                       STRING PGM-LDBS0130         ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
                END-EVALUATE
             ELSE
      *-->   GESTIONE ERRORE
                MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
                MOVE WK-LABEL         TO IEAI9901-LABEL-ERR
                MOVE '005016'         TO IEAI9901-COD-ERRORE
                STRING PGM-LDBS0130         ';'
                       IDSO0011-RETURN-CODE ';'
                       IDSO0011-SQLCODE
                       DELIMITED BY SIZE
                       INTO IEAI9901-PARAMETRI-ERR
                END-STRING
                PERFORM S0300-RICERCA-GRAVITA-ERRORE
                   THRU EX-S0300
             END-IF
           END-PERFORM.

       A130-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA DATI TABELLA VALORE ASSET
      *----------------------------------------------------------------*
       A140-RECUP-VALORE-ASSET.

           MOVE 'A140-RECUP-VALORE-ASSET' TO WK-LABEL.

           MOVE ZEROES                    TO IX-TAB-GRZ

           MOVE WTGA-TP-TRCH(IX-TAB-TGA)  TO WS-TP-TRCH

           EVALUATE TRUE
             WHEN TP-TRCH-NEG-PRCOS
             WHEN TP-TRCH-NEG-RIS-PAR
             WHEN TP-TRCH-NEG-RIS-PRO
             WHEN TP-TRCH-NEG-IMPST-SOST
             WHEN TP-TRCH-NEG-DA-DIS
             WHEN TP-TRCH-NEG-INV-DA-SWIT
                  SET TRANCHE-NEGATIVA    TO TRUE
             WHEN OTHER
                  SET TRANCHE-POSITIVA    TO TRUE
           END-EVALUATE.

           INITIALIZE IDSI0011-BUFFER-DATI

           SET  INIT-CUR-VAS              TO TRUE.
           SET  IDSI0011-FETCH-FIRST      TO TRUE.
           SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.
      *
           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR IDSV0001-ESITO-KO
                      OR FINE-CUR-VAS

              INITIALIZE VAL-AST
                         LDBV4911

              SET IDSI0011-WHERE-CONDITION TO TRUE
              SET IDSI0011-TRATT-DEFAULT   TO TRUE

              MOVE LCCC0450-DATA-EFFETTO TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
              MOVE LCCC0450-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA
              MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                                           TO LDBV4911-ID-TRCH-DI-GAR
              MOVE SPACES                  TO LDBV4911-TP-VAL-AST-1
                                              LDBV4911-TP-VAL-AST-2
                                              LDBV4911-TP-VAL-AST-3
                                              LDBV4911-TP-VAL-AST-4
                                              LDBV4911-TP-VAL-AST-5
                                              LDBV4911-TP-VAL-AST-6
                                              LDBV4911-TP-VAL-AST-7
                                              LDBV4911-TP-VAL-AST-8
                                              LDBV4911-TP-VAL-AST-9
                                              LDBV4911-TP-VAL-AST-10
              MOVE LDBV4911                TO IDSI0011-BUFFER-WHERE-COND
              MOVE 'LDBS4910'              TO IDSI0011-CODICE-STR-DATO
              MOVE VAL-AST                 TO IDSI0011-BUFFER-DATI

              PERFORM CALL-DISPATCHER      THRU CALL-DISPATCHER-EX

              IF IDSO0011-SUCCESSFUL-RC
                 EVALUATE TRUE
                     WHEN IDSO0011-NOT-FOUND
      *-->           NESSUN DATO IN TABELLA
                        SET FINE-CUR-VAS          TO TRUE

                     WHEN IDSO0011-SUCCESSFUL-SQL
      *-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                        MOVE IDSO0011-BUFFER-DATI TO VAL-AST

                        PERFORM A150-CARICA-COD-FONDO
                           THRU A150-EX

                        SET IDSI0011-FETCH-NEXT   TO TRUE

                     WHEN OTHER
      *--->          ERRORE DI ACCESSO AL DB
                        MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                        MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                        MOVE '005016'   TO IEAI9901-COD-ERRORE
                        STRING PGM-LDBS4910         ';'
                               IDSO0011-RETURN-CODE ';'
                               IDSO0011-SQLCODE
                               DELIMITED BY SIZE
                               INTO IEAI9901-PARAMETRI-ERR
                        END-STRING
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                           THRU EX-S0300
                 END-EVALUATE
              ELSE
      *-->    GESTIONE ERRORE
                 MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL         TO IEAI9901-LABEL-ERR
                 MOVE '005016'         TO IEAI9901-COD-ERRORE
                 STRING PGM-LDBS4910         ';'
                        IDSO0011-RETURN-CODE ';'
                        IDSO0011-SQLCODE
                        DELIMITED BY SIZE
                        INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF

           END-PERFORM.

       A140-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CARICO CODICE FONDO, NUMERO QUOTE , VALORE QUOTA
      *----------------------------------------------------------------*
       A150-CARICA-COD-FONDO.

           MOVE 'A150-CARICA-COD-FONDO'   TO WK-LABEL.

           PERFORM VARYING IX-TAB-MAT FROM 1 BY 1
             UNTIL IX-TAB-MAT > LCCC0450-ELE-TAB-MAX
                OR (VAS-COD-FND        = LCCC0450-COD-FONDO(IX-TAB-MAT)
                AND VAS-ID-TRCH-DI-GAR = LCCC0450-ID-TRCH-DI-GAR
                                                           (IX-TAB-MAT))
           END-PERFORM.

           IF IX-TAB-MAT > LCCC0450-ELE-TAB-MAX
              MOVE LCCC0450-ELE-TAB-MAX   TO IX-TAB-TOT
              ADD 1                       TO IX-TAB-TOT

      *-->    VALORIZZO IL CAMPO CODICE FONDO PER LA VARIABILE LCODFONDO
              MOVE VAS-COD-FND
                TO LCCC0450-COD-FONDO(IX-TAB-TOT)

      *-->    CALCOLO PER LA VARIABILE LVALQUOTE
              PERFORM A250-RECUP-QTZ-AGG
                 THRU A250-EX

              IF IDSV0001-ESITO-OK
                  MOVE L41-VAL-QUO
                    TO LCCC0450-VAL-QUOTA(IX-TAB-TOT)

      *-->      SALVO I DATI DELLA VALORE ASSET ORIGINARIA DELLA TRANCHE
                  IF  TRANCHE-POSITIVA
                  AND VAS-TP-VAL-AST = VALORE-POSITIVO
                     IF VAS-NUM-QUO-NULL NOT = HIGH-VALUES
                        MOVE VAS-NUM-QUO
                          TO LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT)
                     END-IF
                     IF VAS-VAL-QUO-NULL NOT = HIGH-VALUES
                        MOVE VAS-VAL-QUO
                          TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                     ELSE
                        MOVE L41-VAL-QUO
                          TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                     END-IF
                     IF VAS-DT-VALZZ-NULL NOT = HIGH-VALUES
                        MOVE VAS-DT-VALZZ
                          TO LCCC0450-DT-VAL-QUOTE(IX-TAB-TOT)
                     END-IF
                  END-IF

      *-->      SALVO I DATI DELLA VALORE ASSET ORIGINARIA DELLA TRANCHE
                  IF  TRANCHE-NEGATIVA
                  AND VAS-TP-VAL-AST = VALORE-NEGATIVO
                     IF VAS-NUM-QUO-NULL NOT = HIGH-VALUES
                        MOVE VAS-NUM-QUO
                          TO LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT)
                     END-IF
                     IF VAS-VAL-QUO-NULL NOT = HIGH-VALUES
                        MOVE VAS-VAL-QUO
                          TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                     ELSE
                        MOVE L41-VAL-QUO
                          TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                     END-IF
                     IF VAS-DT-VALZZ-NULL NOT = HIGH-VALUES
                        MOVE VAS-DT-VALZZ
                          TO LCCC0450-DT-VAL-QUOTE(IX-TAB-TOT)
                     END-IF
                  END-IF

      *-->        VALORIZZO IL CAMPO TRANCHE_DI GARANZIA
                  MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                    TO LCCC0450-ID-TRCH-DI-GAR(IX-TAB-TOT)

                  IF VAS-ID-RICH-INVST-FND-NULL NOT = HIGH-VALUES
      *-->           RECUPERO LA RICHIESTA DI INVESTIMENTO
TEST               IF VAS-ID-RICH-INVST-FND > 0
                     PERFORM A220-RECUP-IMP-INVES
                        THRU A220-EX

                     IF IDSV0001-ESITO-OK
      *-->              SALVO I DATI DELLA RICHIESTA DI INVESTIMENTO
      *-->              ORIGINARIA DELLA TRANCHE
                        IF  TRANCHE-POSITIVA
                        AND VAS-TP-VAL-AST = VALORE-POSITIVO
      *-->                  VALORIZZAZIONE PERCENTUALE DI INVESTIMENTO
                            IF RIF-PC-NULL NOT = HIGH-VALUES
                               MOVE RIF-PC
                                 TO LCCC0450-PERCENT-INV(IX-TAB-TOT)
                            END-IF

                            IF RIF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                               MOVE RIF-IMP-MOVTO
                                 TO LCCC0450-IMP-INVES(IX-TAB-TOT)
                            END-IF
                        END-IF

                        IF  (VAS-NUM-QUO-NULL = HIGH-VALUES OR ZERO)
                        AND L41-VAL-QUO > 0
                           IF RIF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                              COMPUTE WK-NUMERO-QUOTE ROUNDED =
                                  RIF-IMP-MOVTO / L41-VAL-QUO
                           ELSE
                              MOVE ZEROES
                                TO WK-NUMERO-QUOTE
                           END-IF
                        ELSE
                           IF VAS-NUM-QUO IS NUMERIC
                              MOVE VAS-NUM-QUO
                                TO WK-NUMERO-QUOTE
                           ELSE
                              MOVE ZEROES
                                TO WK-NUMERO-QUOTE
                           END-IF
                        END-IF
                        PERFORM CALCOLA-NUM-QUOTE
                           THRU CALCOLA-NUM-QUOTE-EX
                     END-IF
TEST               END-IF
                  END-IF

                  IF VAS-ID-RICH-DIS-FND-NULL NOT = HIGH-VALUES
      *-->           RECUPERO LA RICHIESTA DI DISINVESTIMENTO
                     PERFORM A230-RECUP-IMP-DISIN
                        THRU A230-EX

                     IF IDSV0001-ESITO-OK
      *-->              SALVO I DATI DELLA RICHIESTA DI DISINVESTIMENTO
      *-->              ORIGINARIA DELLA TRANCHE
                        IF  TRANCHE-NEGATIVA
                        AND VAS-TP-VAL-AST = VALORE-NEGATIVO
      *-->                 VALORIZZAZIONE PERCENTUALE DI DISINVESTIMENTO
                            IF RDF-PC-NULL NOT = HIGH-VALUES
                               MOVE RDF-PC
                                 TO LCCC0450-PERCENT-INV(IX-TAB-TOT)
                            END-IF

                            IF RDF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                               MOVE RDF-IMP-MOVTO
                                 TO LCCC0450-IMP-DISINV(IX-TAB-TOT)
                            END-IF
                        END-IF

                        IF (VAS-NUM-QUO-NULL = HIGH-VALUES OR ZERO)
                        AND (L41-VAL-QUO > 0)
      *-->              EFFETTUO UNA MODIFICA AL NUMERO QUOTE SOLO SE
      *-->              IL TIPO VALORE ASSET E' VALORE NEGATIVO
      *-->              OPPURE SE E' DI LIQUIDAZIONE DOBBIAMO
      *-->              CONSIDERARLA SOLO SE LO STATO DELLA RICHIESTA
      *-->              DI DISINVESTIMENTO ASSOCIATA E' CONCLUSO
                        AND ((VAS-TP-VAL-AST = VALORE-NEGATIVO
                         OR   VAS-TP-VAL-AST = ANNULLO-POSITIVO
                         OR   VAS-TP-VAL-AST = ANNULLO-LIQUIDAZIONE)
                         OR  (VAS-TP-VAL-AST = VAS-LIQUIDAZIONE
                        AND   RDF-TP-STAT = 'CL'))
                            IF RDF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                               COMPUTE WK-NUMERO-QUOTE ROUNDED =
                                   RDF-IMP-MOVTO / L41-VAL-QUO
                            ELSE
                               MOVE ZEROES
                                 TO WK-NUMERO-QUOTE
                            END-IF
                        ELSE
                           IF  (VAS-TP-VAL-AST = VAS-LIQUIDAZIONE)
                           AND (RDF-TP-STAT NOT = 'CL')
                               MOVE ZEROES
                                 TO WK-NUMERO-QUOTE
                           ELSE
                               IF VAS-NUM-QUO IS NUMERIC
                                  MOVE VAS-NUM-QUO
                                    TO WK-NUMERO-QUOTE
                               ELSE
                                  MOVE ZEROES
                                    TO WK-NUMERO-QUOTE
                               END-IF
                           END-IF
                        END-IF
                        PERFORM CALCOLA-NUM-QUOTE
                           THRU CALCOLA-NUM-QUOTE-EX
                     END-IF
                  END-IF

                  IF IDSV0001-ESITO-OK
      *-->           VALORIZZO IL CAMPO CONTROVALORE
                     MOVE ZERO             TO WK-CONTROVALORE

                     COMPUTE WK-CONTROVALORE                    =
                            (LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT) *
                             LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)  )

                     COMPUTE LCCC0450-CONTROVALORE (IX-TAB-TOT) =
                             LCCC0450-CONTROVALORE (IX-TAB-TOT) +
                             WK-CONTROVALORE

                     MOVE IX-TAB-TOT      TO LCCC0450-ELE-TAB-MAX
                  END-IF
              END-IF
           ELSE
              MOVE IX-TAB-MAT             TO IX-TAB-TOT

      *-->    SALVO I DATI DELLA VALORE ASSET ORIGINARIA DELLA TRANCHE
              IF  TRANCHE-POSITIVA
              AND VAS-TP-VAL-AST = VALORE-POSITIVO
                 IF VAS-NUM-QUO-NULL NOT = HIGH-VALUES
                    MOVE VAS-NUM-QUO
                      TO LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT)
                 END-IF
                 IF VAS-VAL-QUO-NULL NOT = HIGH-VALUES
                    MOVE VAS-VAL-QUO
                      TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                 ELSE
                    MOVE L41-VAL-QUO
                      TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                 END-IF
                 IF VAS-DT-VALZZ-NULL NOT = HIGH-VALUES
                    MOVE VAS-DT-VALZZ
                      TO LCCC0450-DT-VAL-QUOTE(IX-TAB-TOT)
                 END-IF
              END-IF

      *-->    SALVO I DATI DELLA VALORE ASSET ORIGINARIA DELLA TRANCHE
              IF  TRANCHE-NEGATIVA
              AND VAS-TP-VAL-AST = VALORE-NEGATIVO
                 IF VAS-NUM-QUO-NULL NOT = HIGH-VALUES
                    MOVE VAS-NUM-QUO
                      TO LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT)
                 END-IF
                 IF VAS-VAL-QUO-NULL NOT = HIGH-VALUES
                    MOVE VAS-VAL-QUO
                      TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                 ELSE
                    MOVE L41-VAL-QUO
                      TO LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)
                 END-IF
                 IF VAS-DT-VALZZ-NULL NOT = HIGH-VALUES
                    MOVE VAS-DT-VALZZ
                      TO LCCC0450-DT-VAL-QUOTE(IX-TAB-TOT)
                 END-IF
              END-IF

              IF VAS-ID-RICH-INVST-FND-NULL NOT = HIGH-VALUES
TEST           IF VAS-ID-RICH-INVST-FND > 0
                 IF VAS-NUM-QUO-NULL = HIGH-VALUES OR ZERO
      *-->          RECUPERO LA RICHIESTA DI INVESTIMENTO
                    PERFORM A220-RECUP-IMP-INVES
                       THRU A220-EX
                    MOVE ZEROES
                      TO WK-NUMERO-QUOTE

                    IF IDSV0001-ESITO-OK
      *-->             SALVO I DATI DELLA RICHIESTA DI INVESTIMENTO
      *-->             ORIGINARIA DELLA TRANCHE
                       IF  TRANCHE-POSITIVA
                       AND VAS-TP-VAL-AST = VALORE-POSITIVO
      *-->                 VALORIZZAZIONE PERCENTUALE DI INVESTIMENTO
                           IF RIF-PC-NULL NOT = HIGH-VALUES
                              MOVE RIF-PC
                                TO LCCC0450-PERCENT-INV(IX-TAB-TOT)
                           END-IF

                           IF RIF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                              MOVE RIF-IMP-MOVTO
                                TO LCCC0450-IMP-INVES(IX-TAB-TOT)
                           END-IF
                       END-IF

                       PERFORM A250-RECUP-QTZ-AGG
                          THRU A250-EX

                       IF IDSV0001-ESITO-OK
                       AND QTZ-AGG-OK
                       AND L41-VAL-QUO > 0
                           IF RIF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                              COMPUTE WK-NUMERO-QUOTE ROUNDED =
                                  RIF-IMP-MOVTO / L41-VAL-QUO
                           END-IF
                       END-IF
                    END-IF
                 ELSE
                    MOVE VAS-NUM-QUO
                      TO WK-NUMERO-QUOTE
                 END-IF
                 IF IDSV0001-ESITO-OK
                    PERFORM CALCOLA-NUM-QUOTE
                       THRU CALCOLA-NUM-QUOTE-EX
                 END-IF
TEST           END-IF
              END-IF

              IF VAS-ID-RICH-DIS-FND-NULL NOT = HIGH-VALUES
                 IF VAS-NUM-QUO-NULL = HIGH-VALUES OR ZERO
      *-->          RECUPERO LA RICHIESTA DI DISINVESTIMENTO
                    PERFORM A230-RECUP-IMP-DISIN
                       THRU A230-EX

                    MOVE ZEROES
                      TO WK-NUMERO-QUOTE
                    IF IDSV0001-ESITO-OK
      *-->             SALVO I DATI DELLA RICHIESTA DI DISINVESTIMENTO
      *-->             ORIGINARIA DELLA TRANCHE
                       IF  TRANCHE-NEGATIVA
                       AND VAS-TP-VAL-AST = VALORE-NEGATIVO
      *-->                 VALORIZZAZIONE PERCENTUALE DI DISINVESTIMENTO
                           IF RDF-PC-NULL NOT = HIGH-VALUES
                              MOVE RDF-PC
                                TO LCCC0450-PERCENT-INV(IX-TAB-TOT)
                           END-IF

                           IF RDF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                              MOVE RDF-IMP-MOVTO
                                TO LCCC0450-IMP-DISINV(IX-TAB-TOT)
                           END-IF
                       END-IF

                       PERFORM A250-RECUP-QTZ-AGG
                          THRU A250-EX

                       IF IDSV0001-ESITO-OK
                       AND QTZ-AGG-OK
                       AND L41-VAL-QUO > 0
      *-->             EFFETTUO UNA MODIFICA AL NUMERO QUOTE SOLO SE
      *-->             IL TIPO VALORE ASSET E' VALORE NEGATIVO
      *-->             OPPURE SE E' DI LIQUIDAZIONE DOBBIAMO
      *-->             CONSIDERARLA SOLO SE LO STATO DELLA RICHIESTA
      *-->             DI DISINVESTIMENTO ASSOCIATA E' CONCLUSO
                       AND ((VAS-TP-VAL-AST = VALORE-NEGATIVO
                        OR   VAS-TP-VAL-AST = ANNULLO-POSITIVO
                        OR   VAS-TP-VAL-AST = ANNULLO-LIQUIDAZIONE)
                        OR  (VAS-TP-VAL-AST = VAS-LIQUIDAZIONE
                       AND   RDF-TP-STAT = 'CL'))
                           IF RDF-IMP-MOVTO-NULL NOT = HIGH-VALUES
                               COMPUTE WK-NUMERO-QUOTE ROUNDED =
                                   RDF-IMP-MOVTO / L41-VAL-QUO
                           END-IF
                       END-IF
                    END-IF
                 ELSE
                    MOVE VAS-NUM-QUO
                      TO WK-NUMERO-QUOTE
                 END-IF
                 IF IDSV0001-ESITO-OK
                    PERFORM CALCOLA-NUM-QUOTE
                       THRU CALCOLA-NUM-QUOTE-EX
                 END-IF
              END-IF

              IF IDSV0001-ESITO-OK
      *-->       VALORIZZO IL CAMPO CONTROVALORE
                 MOVE ZERO             TO WK-CONTROVALORE

                 COMPUTE WK-CONTROVALORE                    =
                        (LCCC0450-NUM-QUOTE-INI(IX-TAB-TOT) *
                         LCCC0450-VAL-QUOTA-INI(IX-TAB-TOT)  )

                 COMPUTE LCCC0450-CONTROVALORE (IX-TAB-TOT) =
                         LCCC0450-CONTROVALORE (IX-TAB-TOT) +
                         WK-CONTROVALORE
              END-IF

      *-->    VALORIZZA LA VARIABILE VALQUOTET
              IF IDSV0001-ESITO-OK
                 PERFORM A190-VALORIZZA-VALQUOTET
                    THRU A190-EX
              END-IF

           END-IF.

       A150-EX.
           EXIT.
      *----------------------------------------------------------------*
       CALCOLA-NUM-QUOTE.

           IF VAS-TP-VAL-AST = VALORE-POSITIVO
           OR VAS-TP-VAL-AST = ANNULLO-NEGATIVO
           OR VAS-TP-VAL-AST = ANNULLO-LIQUIDAZIONE
              ADD WK-NUMERO-QUOTE
               TO LCCC0450-NUM-QUOTE(IX-TAB-TOT)
           ELSE
              IF VAS-TP-VAL-AST = VALORE-NEGATIVO
              OR VAS-TP-VAL-AST = ANNULLO-POSITIVO
              OR VAS-TP-VAL-AST = VAS-LIQUIDAZIONE
                 SUBTRACT WK-NUMERO-QUOTE
                     FROM LCCC0450-NUM-QUOTE(IX-TAB-TOT)
              END-IF
           END-IF.

       CALCOLA-NUM-QUOTE-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA LA VARIABILE VALQUOTET
      *----------------------------------------------------------------*
       A190-VALORIZZA-VALQUOTET.

      *--> Calcolo della variabile VALQUOTET:
      *--> Calcolare per ogni fondo legato alla Tranche e
      *--> presente sulla Valore Asset (fondi con numero quote > 0)
      *    IF LCCC0450-NUM-QUOTE(IX-TAB-TOT) > 0
           IF LCCC0450-NUM-QUOTE(IX-TAB-TOT) NOT = 0

      *-->    Calcolo prima la Data Ricorrenza Anniversaria di Tranche
      *-->    (utilizzata per reperire valore quote al tempo T)
              PERFORM A191-CALCOLA-DT-RICOR-TRANCHE
                 THRU A191-EX

              IF IDSV0001-ESITO-OK
      *-->       Valore delle quote dalla tabella QUOTAZIONE_FONDI_UNIT
      *-->       in T, ossia alla data di ricorrenza anniversaria
      *-->       (ricorrenza calcolata rispetto alla data
      *-->       calcolo Riserva).
                 PERFORM A160-RECUP-QUOTAZ-FONDO
                    THRU A160-EX

                 IF IDSV0001-ESITO-OK
      *-->          VALORIZZO IL CAMPO VALORE QUOTA T
                    IF L19-VAL-QUO-NULL = HIGH-VALUES
                       MOVE ZEROES
                         TO LCCC0450-VAL-QUOTA-T(IX-TAB-TOT)
                    ELSE
                       MOVE L19-VAL-QUO
                         TO LCCC0450-VAL-QUOTA-T(IX-TAB-TOT)
                    END-IF
                 END-IF
              END-IF

           END-IF.

       A190-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CONVERTI VALORE DA STRINGA IN NUMERICO
      *----------------------------------------------------------------*
       A191-CALCOLA-DT-RICOR-TRANCHE.

           INITIALIZE           IO-A2K-LCCC0003
                                IN-RCODE

      *--> somma DELTA e conversione data
           MOVE '02'                        TO A2K-FUNZ

      *--> FORMATO AAAAMMGG
           MOVE '03'                        TO A2K-INFO

      *--> 1 ANNO
           MOVE 1                           TO A2K-DELTA

      *--> ANNI
           MOVE 'A'                         TO A2K-TDELTA

      *--> GIORNI FISSI
           MOVE '0'                         TO A2K-FISLAV

      *--> INIZIO CONTEGGIO DA STESSO GIORNO
           MOVE 0                           TO A2K-INICON

      *--> DATA INPUT
      *--> Data decorrenza di tranche
           MOVE WTGA-DT-DECOR(IX-TAB-TGA)
             TO A2K-INDATA

           PERFORM CALL-ROUTINE-DATE
              THRU CALL-ROUTINE-DATE-EX

      *--> Se Data decorrenza di tranche + 1 anno e' > della Data
      *--> calcolo Riserva allora la data di ricorrenza
      *--> e' la Data decorrenza di tranche
           IF IDSV0001-ESITO-OK
      *-->    DATA OUTPUT (CALCOLATA)
              IF A2K-OUAMG > LCCC0450-DATA-RISERVA
                 MOVE WTGA-DT-DECOR(IX-TAB-TGA)
                   TO WK-DT-RICOR-TRANCHE-NUM
              ELSE
      *-->       Altrimenti calcolare data ricorrenza con gg e mm
      *-->       di decorrenza tranche e anno = anno di calcolo
      *-->       riserva
                 MOVE ZEROES
                   TO WK-DT-RICOR-TRANCHE-NUM
                 MOVE WTGA-DT-DECOR(IX-TAB-TGA)
                   TO WK-DT-RICOR-TRANCHE-NUM

      *-->       Se la data cosl calcolata e' < = alla data di calcolo
      *-->       riserva allora impostare la data ricorrenza
      *-->       con tale data
                 IF WK-DT-RICOR-TRANCHE-NUM <= LCCC0450-DATA-RISERVA
                    CONTINUE
                 ELSE
      *-->          Altrimenti (se > di data calcolo riserva)
      *-->          sottrarre 1 anno e valorizzare la data
      *-->          ricorrenza con tale data
                    MOVE WK-DT-RICOR-TRANCHE-AA
                      TO WK-ANNO
                    SUBTRACT 1
                        FROM WK-ANNO
                    MOVE WK-ANNO
                      TO WK-DT-RICOR-TRANCHE-AA
                 END-IF

              END-IF

           END-IF.

       A191-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CHIAMATA AL SERVIZIO CALCOLA DATA
      *----------------------------------------------------------------*
       CALL-ROUTINE-DATE.

           CALL LCCS0003      USING IO-A2K-LCCC0003
                                    IN-RCODE

           ON EXCEPTION
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'ROUTINE CALCOLO DATA (LCCS0003)'
                TO CALL-DESC
              MOVE 'CALL-ROUTINE-DATE'
                TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

           IF IN-RCODE  = '00'
              CONTINUE
           ELSE
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'CALL-ROUTINE-DATE'    TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       CALL-ROUTINE-DATE-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CONVERTI VALORE DA STRINGA IN NUMERICO
      *----------------------------------------------------------------*
       A160-RECUP-QUOTAZ-FONDO.

           MOVE 'A160-RECUP-QUOTAZ-FONDO' TO WK-LABEL.
           MOVE WK-LABEL                  TO IDSV8888-AREA-DISPLAY

           INITIALIZE QUOTZ-FND-UNIT
                      IDSI0011-BUFFER-DATI.

           MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.
           SET  IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET  IDSI0011-WHERE-CONDITION  TO TRUE.
           SET  IDSI0011-SELECT           TO TRUE.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                          TO L19-COD-COMP-ANIA.
           MOVE LCCC0450-COD-FONDO(IX-TAB-TOT)
                                          TO L19-COD-FND.
           MOVE WK-DT-RICOR-TRANCHE-NUM   TO L19-DT-QTZ.
           MOVE SPACES                    TO IDSI0011-BUFFER-WHERE-COND.
           MOVE 'LDBS2080'                TO IDSI0011-CODICE-STR-DATO
           MOVE QUOTZ-FND-UNIT            TO IDSI0011-BUFFER-DATI.

           PERFORM CALL-DISPATCHER        THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI TO QUOTZ-FND-UNIT

                  WHEN IDSO0011-NOT-FOUND
      *--->       QUOTAZIONE NON TROVATA PER IL FONDO $ ALLA DATA $
                     CONTINUE

                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                     MOVE WK-LABEL      TO IEAI9901-LABEL-ERR
                     MOVE '005016'      TO IEAI9901-COD-ERRORE
                     STRING PGM-LDBS2080         ';'
                            IDSO0011-RETURN-CODE ';'
                            IDSO0011-SQLCODE
                            DELIMITED BY SIZE
                            INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> GESTIONE ERRORE
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL           TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              STRING PGM-LDBS2080         ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       A160-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CALCOLO IL TOTALE DEL CONTRATTO
      *----------------------------------------------------------------*
       A180-CALC-TOTALE.

           MOVE 'A180-CALC-TOTALE'        TO WK-LABEL.
           MOVE WK-LABEL                  TO IDSV8888-AREA-DISPLAY
      *    PERFORM ESEGUI-DISPLAY         THRU ESEGUI-DISPLAY-EX

           MOVE ZERO                      TO LCCC0450-TOT-CONTRATTO.
           MOVE ZERO                      TO LCCC0450-TOT-IMP-INVES.
           MOVE ZERO                      TO LCCC0450-TOT-IMP-DISINV.

           PERFORM VARYING IX-TAB-TOT FROM 1 BY 1
             UNTIL IX-TAB-TOT > LCCC0450-ELE-TAB-MAX

               COMPUTE LCCC0450-TOT-CONTRATTO =
                       LCCC0450-TOT-CONTRATTO +
                       LCCC0450-CONTROVALORE(IX-TAB-TOT)

               COMPUTE LCCC0450-TOT-IMP-INVES =
                       LCCC0450-TOT-IMP-INVES +
                       LCCC0450-IMP-INVES(IX-TAB-TOT)

               COMPUTE LCCC0450-TOT-IMP-DISINV =
                       LCCC0450-TOT-IMP-DISINV +
                       LCCC0450-IMP-DISINV(IX-TAB-TOT)

           END-PERFORM.

       A180-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RECUPERA RICHIESTA DI INVESTIMENTO
      *----------------------------------------------------------------*
       A220-RECUP-IMP-INVES.

           MOVE 'A220-RECUP-IMP-INVES'    TO WK-LABEL.
           MOVE WK-LABEL                  TO IDSV8888-AREA-DISPLAY
      *    PERFORM ESEGUI-DISPLAY         THRU ESEGUI-DISPLAY-EX

           INITIALIZE RICH-INVST-FND
                      IDSI0011-BUFFER-DATI.

           SET  IDSI0011-ID              TO TRUE.
           SET  IDSI0011-SELECT          TO TRUE.
           SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.
           SET  IDSI0011-TRATT-DEFAULT   TO TRUE.

           MOVE LCCC0450-DATA-EFFETTO    TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
           MOVE LCCC0450-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA.

           MOVE VAS-ID-RICH-INVST-FND    TO RIF-ID-RICH-INVST-FND.
           MOVE 'RICH-INVST-FND'         TO IDSI0011-CODICE-STR-DATO
           MOVE RICH-INVST-FND           TO IDSI0011-BUFFER-DATI.

           PERFORM CALL-DISPATCHER       THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    MOVE IDSO0011-BUFFER-DATI TO RICH-INVST-FND

                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                    MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    MOVE WK-LABEL      TO IEAI9901-LABEL-ERR
                    MOVE '005016'      TO IEAI9901-COD-ERRORE
                    STRING PGM-IDBSRIF0         ';'
                           IDSO0011-RETURN-CODE ';'
                           IDSO0011-SQLCODE
                           DELIMITED BY SIZE
                           INTO IEAI9901-PARAMETRI-ERR
                    END-STRING
                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
                       THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> GESTIRE ERRORE
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL           TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              STRING PGM-IDBSRIF0         ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       A220-EX.
           EXIT.
      **---------------------------------------------------------------*
      *    RECUPERA RICHIESTA DI DISINVESTIMENTO
      *----------------------------------------------------------------*
       A230-RECUP-IMP-DISIN.

           MOVE 'A230-RECUP-IMP-DISIN'    TO WK-LABEL.
           MOVE WK-LABEL                  TO IDSV8888-AREA-DISPLAY
      *    PERFORM ESEGUI-DISPLAY         THRU ESEGUI-DISPLAY-EX

           INITIALIZE RICH-DIS-FND
                      IDSI0011-BUFFER-DATI.

           SET  IDSI0011-ID              TO TRUE.
           SET  IDSI0011-SELECT          TO TRUE.
           SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.
           SET  IDSI0011-TRATT-DEFAULT   TO TRUE.

           MOVE LCCC0450-DATA-EFFETTO    TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
           MOVE LCCC0450-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA.

           MOVE VAS-ID-RICH-DIS-FND      TO RDF-ID-RICH-DIS-FND.
           MOVE 'RICH-DIS-FND'           TO IDSI0011-CODICE-STR-DATO
           MOVE RICH-DIS-FND             TO IDSI0011-BUFFER-DATI.

           PERFORM CALL-DISPATCHER       THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    MOVE IDSO0011-BUFFER-DATI TO RICH-DIS-FND

                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                    MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    MOVE WK-LABEL      TO IEAI9901-LABEL-ERR
                    MOVE '005016'      TO IEAI9901-COD-ERRORE
                    STRING PGM-IDBSRDF0         ';'
                           IDSO0011-RETURN-CODE ';'
                           IDSO0011-SQLCODE
                           DELIMITED BY SIZE
                           INTO IEAI9901-PARAMETRI-ERR
                    END-STRING
                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
                       THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> GESTIONE ERRORE
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL           TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              STRING PGM-IDBSRDF0         ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       A230-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RECUPERA QUOTAZIONE AGGIUNTIVA FONDO
      *----------------------------------------------------------------*
       A250-RECUP-QTZ-AGG.
           MOVE 'A250-RECUP-QTZ-AGG'           TO WK-LABEL.

           INITIALIZE QUOTZ-AGG-FND
                      IDSI0011-BUFFER-DATI.

           SET  IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET  IDSI0011-PRIMARY-KEY      TO TRUE.
           SET  IDSI0011-SELECT           TO TRUE.
           SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.
           SET  QTZ-AGG-OK                TO TRUE.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA    TO L41-COD-COMP-ANIA
           MOVE 'CR'                           TO L41-TP-QTZ
           MOVE LCCC0450-COD-FONDO(IX-TAB-TOT) TO L41-COD-FND
           MOVE LCCC0450-DATA-EFFETTO          TO L41-DT-QTZ

           MOVE LCCC0450-DATA-EFFETTO    TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
           MOVE LCCC0450-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA.
           MOVE 'QUOTZ-AGG-FND'          TO IDSI0011-CODICE-STR-DATO
           MOVE QUOTZ-AGG-FND            TO IDSI0011-BUFFER-DATI.

           PERFORM CALL-DISPATCHER       THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI TO QUOTZ-AGG-FND
      *-->           SALVA I DATI LETTI DALLA TABELLA QUOTZ-AGG-FND
      *-->           IN WORKCOMMAREA (WL19)
                     PERFORM A260-SALVA-QUOTZ-AGG-WK
                        THRU A260-EX

                  WHEN IDSO0011-NOT-FOUND
      *--->       QUOTAZIONE NON TROVATA PER IL FONDO $ ALLA DATA $
                     CONTINUE

                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM         TO IEAI9901-COD-SERVIZIO-BE
                     MOVE WK-LABEL       TO IEAI9901-LABEL-ERR
                     MOVE '005016'       TO IEAI9901-COD-ERRORE
                     STRING PGM-IDBSL410         ';'
                            IDSO0011-RETURN-CODE ';'
                            IDSO0011-SQLCODE
                            DELIMITED BY SIZE
                            INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> GESTIONE ERRORE
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL           TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              STRING PGM-IDBSL410         ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       A250-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    SALVA I DATI LETTI DALLA TABELLA QUOTZ-AGG-FND
      *    IN WORKCOMMAREA (WL19)
      *----------------------------------------------------------------*
       A260-SALVA-QUOTZ-AGG-WK.

           MOVE WL19-ELE-FND-MAX
             TO IX-TAB-L19.

           SET FONDO-NON-TROVATO  TO TRUE
           PERFORM VARYING IX-TAB-WL19 FROM 1 BY 1
                   UNTIL IX-TAB-WL19 > MAX-VAL-L19
                   OR WL19-COD-FND(IX-TAB-WL19) NOT > SPACES
                   OR FONDO-TROVATO
                   IF WL19-COD-FND(IX-TAB-WL19) = L41-COD-FND
                      SET FONDO-TROVATO  TO TRUE
                   END-IF
           END-PERFORM
           IF WL19-ELE-FND-MAX < MAX-VAL-L19
              IF FONDO-NON-TROVATO

                 ADD 1
                  TO IX-TAB-L19

                 PERFORM INIZIA-TOT-L19
                    THRU INIZIA-TOT-L19-EX

                 PERFORM VALORIZZA-OUTPUT-L41
                    THRU VALORIZZA-OUTPUT-L41-EX
              END-IF
           ELSE
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL           TO IEAI9901-LABEL-ERR
              MOVE '005059'           TO IEAI9901-COD-ERRORE
              MOVE 'WL19-AREA-FONDI'  TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       A260-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA OUTPUT QUOTZ-AGG-FND
      *----------------------------------------------------------------*
       VALORIZZA-OUTPUT-L41.

           SET WL19-ST-INV(IX-TAB-L19)
            TO TRUE
           MOVE L41-COD-COMP-ANIA
             TO WL19-COD-COMP-ANIA(IX-TAB-L19)
           MOVE L41-COD-FND
             TO WL19-COD-FND(IX-TAB-L19)
           MOVE L41-DT-QTZ
             TO WL19-DT-QTZ(IX-TAB-L19)
           MOVE L41-VAL-QUO
             TO WL19-VAL-QUO(IX-TAB-L19)
           MOVE L41-DS-OPER-SQL
             TO WL19-DS-OPER-SQL(IX-TAB-L19)
           MOVE L41-DS-VER
             TO WL19-DS-VER(IX-TAB-L19)
           MOVE L41-DS-TS-CPTZ
             TO WL19-DS-TS-CPTZ(IX-TAB-L19)
           MOVE L41-DS-UTENTE
             TO WL19-DS-UTENTE(IX-TAB-L19)
           MOVE L41-DS-STATO-ELAB
             TO WL19-DS-STATO-ELAB(IX-TAB-L19)
           MOVE HIGH-VALUES
             TO WL19-VAL-QUO-MANFEE-NULL(IX-TAB-L19)
           MOVE HIGH-VALUES
             TO WL19-TP-FND(IX-TAB-L19)
           MOVE HIGH-VALUES
             TO WL19-DT-RILEVAZIONE-NAV-NULL(IX-TAB-L19)
           MOVE IX-TAB-L19
             TO WL19-ELE-FND-MAX.

       VALORIZZA-OUTPUT-L41-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.
      *----------------------------------------------------------------*
      *    ROUTINES DI GESTIONE DISPLAY
      *----------------------------------------------------------------*
      *    COPY IDSP8888.
      *----------------------------------------------------------------*
      *    COPY DI VALORIZZAZIONE AREE DI OUTPUT
      *----------------------------------------------------------------*
           COPY LCCVGRZ3                REPLACING ==(SF)== BY ==WGRZ==.
           COPY LCCVTGA3                REPLACING ==(SF)== BY ==WTGA==.
      *----------------------------------------------------------------*
      *    COPY DI INIZIALIZZAZIONE AREE DI OUTPUT
      *----------------------------------------------------------------*
           COPY LCCVGRZ4                REPLACING ==(SF)== BY ==WGRZ==.
           COPY LCCVTGA4                REPLACING ==(SF)== BY ==WTGA==.
           COPY LCCVL194                REPLACING ==(SF)== BY ==WL19==.


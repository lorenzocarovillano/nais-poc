      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0000.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0000
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CALCOLO DATA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0000'.

      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DATA-OUTPUT                  PIC 9(04)V9(07).
       01  WK-DATA-OUTPUT-V REDEFINES WK-DATA-OUTPUT.
           03 WK-NUMERICI                  PIC 9(04).
           03 WK-DECIMALI                  PIC 9(07).

       01  WK-APPO-DEC                     PIC 9(01)V9(07).
       01  WK-APPO-DEC-V REDEFINES WK-APPO-DEC.
           03 WK-NUMER-AP                  PIC 9(01).
           03 WK-DECIM-AP                  PIC 9(07).

       01  WK-DATA-INPUT.
           03 WK-AAAA-INPUT                PIC 9(04).
           03 WK-MM-INPUT                  PIC 9(02).
           03 WK-GG-INPUT                  PIC 9(02).

       77 WK-APPO-MESI                     PIC 9(9) VALUE ZERO.
       77 WK-APPO-ANNI                     PIC 9(9) VALUE ZERO.

       01  TAB-GIORNI  PIC X(024) VALUE
           '312831303130313130313031'.
       01  FILLER REDEFINES TAB-GIORNI.
           05 TAB-GG   PIC 9(002) OCCURS 12.
       01  IND-MESE    PIC 9(002) VALUE ZERO.

       01  WK-ANNO-DIVISIONE               PIC 9(04).
       01  RESTO                           PIC 9(001) VALUE ZERO.
       01  RISULT                          PIC 9(003) VALUE ZERO.
       01  WK-GG-APPOGGIO                  PIC 9(004) VALUE ZERO.
       01  WK-GG-DA-SOMMARE                PIC 9(002) VALUE ZERO.
       01  WK-MESI                         PIC 9(002) VALUE ZERO.
      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.


      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0000.
           COPY LVVC0000        REPLACING ==(SF)== BY ==LVVC0000==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0000.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1000-ELABORAZIONE
                  THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.
      *
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
      *
           IF LVVC0000-FORMAT-DATE EQUAL SPACES
           OR LVVC0000-FORMAT-DATE EQUAL LOW-VALUE
           OR LVVC0000-FORMAT-DATE EQUAL HIGH-VALUE
              SET LVVC0000-AAA-V-GGG         TO TRUE
           END-IF.
      *
       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLO DATI INPUT FORRMATO 1
      *----------------------------------------------------------------*
       S0005-CTRL-DATI-INPUT-1.

           IF LVVC0000-DATA-INPUT-1 NOT NUMERIC
              SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'DATA-INPUT NON NUMERICA'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               IF LVVC0000-DATA-INPUT-1 = ZERO
                  SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                  MOVE WK-PGM
                    TO IDSV0003-COD-SERVIZIO-BE
                  MOVE 'DATA-INPUT NON VALORIZZATA'
                    TO IDSV0003-DESCRIZ-ERR-DB2
               END-IF
           END-IF.

       EX-S0005.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLO DATI INPUT FORMATO 2
      *----------------------------------------------------------------*
       S0010-CTRL-DATI-INPUT-2.

           IF LVVC0000-DATA-INPUT-1 NOT NUMERIC
              SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'DATA-INPUT-2 NON NUMERICA'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               IF LVVC0000-DATA-INPUT-1 = ZERO
                  SET  IDSV0003-FIELD-NOT-VALUED TO TRUE
                  MOVE WK-PGM
                    TO IDSV0003-COD-SERVIZIO-BE
                  MOVE 'DATA-INPUT-2 NON VALORIZZATA'
                    TO IDSV0003-DESCRIZ-ERR-DB2
               END-IF
           END-IF.
      *
       EX-S0010.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLO DATI INPUT FORMATO 3
      *----------------------------------------------------------------*
       S0015-CTRL-DATI-INPUT-3.

           IF LVVC0000-DATA-INPUT-1 NOT NUMERIC
              SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ANNI-INPUT-2 NON NUMERICA'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               IF LVVC0000-DATA-INPUT-1 = ZERO
                  SET  IDSV0003-FIELD-NOT-VALUED TO TRUE
                  MOVE WK-PGM
                    TO IDSV0003-COD-SERVIZIO-BE
                  MOVE 'ANNI-INPUT-2 NON VALORIZZATA'
                    TO IDSV0003-DESCRIZ-ERR-DB2
               END-IF
           END-IF.

           IF LVVC0000-DATA-INPUT-1 NOT NUMERIC
              SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'MESI-INPUT-2 NON NUMERICA'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.

      *    IF  IDSV0003-SUCCESSFUL-RC
      *    AND IDSV0003-SUCCESSFUL-SQL
      *        IF LVVC0000-MESI-INPUT-2 = ZERO
      *           SET  IDSV0003-FIELD-NOT-VALUED TO TRUE
      *           MOVE WK-PGM
      *             TO IDSV0003-COD-SERVIZIO-BE
      *           MOVE 'MESI-INPUT-2 NON VALORIZZATA'
      *             TO IDSV0003-DESCRIZ-ERR-DB2
      *        END-IF
      *    END-IF.

       EX-S0015.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.


           EVALUATE TRUE
              WHEN LVVC0000-AAAA-V-9999999
                   PERFORM S0005-CTRL-DATI-INPUT-1 THRU EX-S0005
                   IF IDSV0003-SUCCESSFUL-RC
                      PERFORM C100-CONVERTI-FORMAT-1  THRU C100-EX
                   END-IF

              WHEN LVVC0000-AAA-V-GGG
                   PERFORM S0010-CTRL-DATI-INPUT-2 THRU EX-S0010
                   IF IDSV0003-SUCCESSFUL-RC
                      PERFORM C200-CONVERTI-FORMAT-2  THRU C200-EX
                   END-IF

              WHEN LVVC0000-AAA-V-MM
                   PERFORM S0015-CTRL-DATI-INPUT-3 THRU EX-S0015
                   IF IDSV0003-SUCCESSFUL-RC
                      PERFORM C300-CONVERTI-FORMAT-3  THRU C300-EX
                   END-IF

              WHEN OTHER
                   SET  IDSV0003-OPER-NOT-V       TO TRUE
                   MOVE WK-PGM TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'FORMATO RICHIESTO NON VALIDO'
                               TO IDSV0003-DESCRIZ-ERR-DB2
           END-EVALUATE.

      *
       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    CONVERSIONE IN AAAA,9999999
      *----------------------------------------------------------------*
       C100-CONVERTI-FORMAT-1.
      *
      *-- CALCOLO ANNO BISESTILE
           MOVE ZERO                TO WK-ANNO-DIVISIONE
                                       RESTO.
           MOVE LVVC0000-DATA-INPUT-1 TO WK-DATA-INPUT.
           DIVIDE WK-AAAA-INPUT BY 4 GIVING RISULT REMAINDER RESTO.
           IF RESTO = 0
              MOVE 29  TO TAB-GG (2)
              MOVE 366 TO WK-ANNO-DIVISIONE
           ELSE
              MOVE 28  TO TAB-GG (2)
              MOVE 365 TO WK-ANNO-DIVISIONE
           END-IF.

      *-- CALCOLO GIORNO TOTALE DELL'ANNO
           MOVE ZERO                TO WK-GG-APPOGGIO.
           PERFORM VARYING IND-MESE FROM 1 BY 1
           UNTIL IND-MESE > WK-MM-INPUT OR
                 IND-MESE > 12

             IF IND-MESE < WK-MM-INPUT
                ADD TAB-GG(IND-MESE)  TO  WK-GG-APPOGGIO
             END-IF

             IF IND-MESE = WK-MM-INPUT
                ADD WK-GG-INPUT       TO  WK-GG-APPOGGIO
             END-IF

           END-PERFORM.


      *-- VALORIZZAZIONE AREEA DI OUTPUT
           MOVE WK-AAAA-INPUT           TO WK-NUMERICI.
           COMPUTE WK-APPO-DEC = WK-GG-APPOGGIO / WK-ANNO-DIVISIONE.
      *
           IF WK-DECIM-AP GREATER ZERO
              MOVE WK-DECIM-AP          TO WK-DECIMALI
           ELSE
              MOVE 9999999              TO WK-DECIMALI
           END-IF.
      *
           MOVE WK-DATA-OUTPUT          TO LVVC0000-DATA-OUTPUT.
      *
       C100-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    CONVERSIONE IN AAAA,GGG
      *----------------------------------------------------------------*
       C200-CONVERTI-FORMAT-2.
      *
           MOVE LVVC0000-DATA-INPUT-1            TO WK-DATA-INPUT.
      *
      *    SE IL MESE E' FEBBRAIO E IL GIORNO E' 28 O 29
      *    OPPURE SIAMO AL TRENTUNESIMO GIORNO DEL MESE
           IF  (WK-MM-INPUT = 2
           AND (WK-GG-INPUT  = 28 OR 29))
           OR  WK-GG-INPUT  = 31
               MOVE 30
                 TO WK-GG-DA-SOMMARE
           ELSE
               MOVE WK-GG-INPUT
                 TO WK-GG-DA-SOMMARE
           END-IF

      *    DECREMENTO IL NUMERO DEI MESI DI UNO
           IF WK-GG-DA-SOMMARE GREATER ZERO
              COMPUTE WK-MESI = WK-MM-INPUT - 1
           END-IF.

           COMPUTE WK-DATA-OUTPUT = ((( WK-MESI * 30 ) +
                                     WK-GG-DA-SOMMARE) / 360).
      *
           IF WK-DECIMALI EQUAL ZERO
              MOVE 9999999                      TO WK-DECIMALI
           END-IF.
      *
           MOVE WK-AAAA-INPUT                   TO WK-NUMERICI.
           MOVE WK-DATA-OUTPUT                  TO LVVC0000-DATA-OUTPUT.
      *
       C200-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CONVERSIONE IN AAAA,MM
      *----------------------------------------------------------------*
       C300-CONVERTI-FORMAT-3.
      *
           MOVE LVVC0000-DATA-INPUT-1            TO WK-DATA-INPUT.

           COMPUTE WK-APPO-MESI = (WK-MM-INPUT * 30).
           COMPUTE WK-APPO-ANNI = (WK-AAAA-INPUT * 360).
      *
           COMPUTE LVVC0000-DATA-OUTPUT =
                   ((WK-APPO-ANNI + WK-APPO-MESI) / 360).
      *
       C300-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    CONTROLLO DATI
      *----------------------------------------------------------------*
       S1200-CONTROLLO-DATI.

           CONTINUE.

       S1200-CONTROLLO-DATI-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.

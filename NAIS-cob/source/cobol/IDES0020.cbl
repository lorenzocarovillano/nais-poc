       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDES0020 IS INITIAL.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE
      * F A S E         : GESTIONE TABELLA GRU_ARZ
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.

      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDGRU0 END-EXEC.
           EXEC SQL INCLUDE IDBVGRU3 END-EXEC.


      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVGRU1 END-EXEC.

      *****************************************************************

       PROCEDURE DIVISION USING IDSV0003 GRU-ARZ.

           PERFORM A000-INIZIO              THRU A000-EX.

           PERFORM A300-ELABORA             THRU A300-EX

           GOBACK.
      ******************************************************************
       A000-INIZIO.

           MOVE 'IDES0020'              TO   IDSV0003-COD-SERVIZIO-BE.
           MOVE 'GRU_ARZ'               TO   IDSV0003-NOME-TABELLA.

           MOVE '00'                    TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                  TO   IDSV0003-SQLCODE
                                             IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2
                                             IDSV0003-KEY-TABELLA.

       A000-EX.
           EXIT.

      ******************************************************************
       A100-CHECK-RETURN-CODE.

           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      ******************************************************************
       A300-ELABORA.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT        THRU A310-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER  TO TRUE
           END-EVALUATE.

       A300-EX.
           EXIT.

      ******************************************************************
       A310-SELECT.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                SELECT
                COD_GRU_ARZ
                ,COD_COMP_ANIA
                ,COD_LIV_OGZ
                ,DSC_GRU_ARZ
                ,DAT_INI_GRU_ARZ
                ,DAT_FINE_GRU_ARZ
                ,DEN_RESP_GRU_ARZ
                ,IND_TLM_RESP
                ,COD_UTE_INS
                ,TMST_INS_RIG
                ,COD_UTE_AGR
                ,TMST_AGR_RIG
             INTO
                :GRU-COD-GRU-ARZ
               ,:GRU-COD-COMP-ANIA
               ,:GRU-COD-LIV-OGZ
               ,:GRU-DSC-GRU-ARZ-VCHAR
               ,:GRU-DAT-INI-GRU-ARZ-DB
               ,:GRU-DAT-FINE-GRU-ARZ-DB
               ,:GRU-DEN-RESP-GRU-ARZ-VCHAR
               ,:GRU-IND-TLM-RESP-VCHAR
               ,:GRU-COD-UTE-INS
               ,:GRU-TMST-INS-RIG-DB
               ,:GRU-COD-UTE-AGR
               ,:GRU-TMST-AGR-RIG-DB

               FROM GRU_ARZ
             WHERE COD_GRU_ARZ   = :GRU-COD-GRU-ARZ

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX


           END-IF.

       A310-EX.
           EXIT.
      ******************************************************************
       Z100-SET-COLONNE-NULL.

           CONTINUE.

       Z100-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE GRU-DAT-INI-GRU-ARZ-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO GRU-DAT-INI-GRU-ARZ
           MOVE GRU-DAT-FINE-GRU-ARZ-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO GRU-DAT-FINE-GRU-ARZ
           MOVE GRU-TMST-INS-RIG-DB TO WS-TIMESTAMP-X
           PERFORM Z801-TS-X-TO-N     THRU Z801-EX
           MOVE WS-TIMESTAMP-N      TO GRU-TMST-INS-RIG
           MOVE GRU-TMST-AGR-RIG-DB TO WS-TIMESTAMP-X
           PERFORM Z801-TS-X-TO-N     THRU Z801-EX
           MOVE WS-TIMESTAMP-N      TO GRU-TMST-AGR-RIG.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.


           MOVE LENGTH OF GRU-DSC-GRU-ARZ
                       TO GRU-DSC-GRU-ARZ-LEN
           MOVE LENGTH OF GRU-DEN-RESP-GRU-ARZ
                       TO GRU-DEN-RESP-GRU-ARZ-LEN
           MOVE LENGTH OF GRU-IND-TLM-RESP
                       TO GRU-IND-TLM-RESP-LEN.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

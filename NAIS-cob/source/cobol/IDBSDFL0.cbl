       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSDFL0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  02 LUG 2014.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDDFL0 END-EXEC.
           EXEC SQL INCLUDE IDBVDFL2 END-EXEC.
           EXEC SQL INCLUDE IDBVDFL3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVDFL1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 D-FORZ-LIQ.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSDFL0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'D_FORZ_LIQ' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_D_FORZ_LIQ
                ,ID_LIQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,COD_COMP_ANIA
                ,DT_INI_EFF
                ,DT_END_EFF
                ,IMP_LRD_CALC
                ,IMP_LRD_DFZ
                ,IMP_LRD_EFFLQ
                ,IMP_NET_CALC
                ,IMP_NET_DFZ
                ,IMP_NET_EFFLQ
                ,IMPST_PRVR_CALC
                ,IMPST_PRVR_DFZ
                ,IMPST_PRVR_EFFLQ
                ,IMPST_VIS_CALC
                ,IMPST_VIS_DFZ
                ,IMPST_VIS_EFFLQ
                ,RIT_ACC_CALC
                ,RIT_ACC_DFZ
                ,RIT_ACC_EFFLQ
                ,RIT_IRPEF_CALC
                ,RIT_IRPEF_DFZ
                ,RIT_IRPEF_EFFLQ
                ,IMPST_SOST_CALC
                ,IMPST_SOST_DFZ
                ,IMPST_SOST_EFFLQ
                ,TAX_SEP_CALC
                ,TAX_SEP_DFZ
                ,TAX_SEP_EFFLQ
                ,INTR_PREST_CALC
                ,INTR_PREST_DFZ
                ,INTR_PREST_EFFLQ
                ,ACCPRE_SOST_CALC
                ,ACCPRE_SOST_DFZ
                ,ACCPRE_SOST_EFFLQ
                ,ACCPRE_VIS_CALC
                ,ACCPRE_VIS_DFZ
                ,ACCPRE_VIS_EFFLQ
                ,ACCPRE_ACC_CALC
                ,ACCPRE_ACC_DFZ
                ,ACCPRE_ACC_EFFLQ
                ,RES_PRSTZ_CALC
                ,RES_PRSTZ_DFZ
                ,RES_PRSTZ_EFFLQ
                ,RES_PRE_ATT_CALC
                ,RES_PRE_ATT_DFZ
                ,RES_PRE_ATT_EFFLQ
                ,IMP_EXCONTR_EFF
                ,IMPB_VIS_CALC
                ,IMPB_VIS_EFFLQ
                ,IMPB_VIS_DFZ
                ,IMPB_RIT_ACC_CALC
                ,IMPB_RIT_ACC_EFFLQ
                ,IMPB_RIT_ACC_DFZ
                ,IMPB_TFR_CALC
                ,IMPB_TFR_EFFLQ
                ,IMPB_TFR_DFZ
                ,IMPB_IS_CALC
                ,IMPB_IS_EFFLQ
                ,IMPB_IS_DFZ
                ,IMPB_TAX_SEP_CALC
                ,IMPB_TAX_SEP_EFFLQ
                ,IMPB_TAX_SEP_DFZ
                ,IINT_PREST_CALC
                ,IINT_PREST_EFFLQ
                ,IINT_PREST_DFZ
                ,MONT_END2000_CALC
                ,MONT_END2000_EFFLQ
                ,MONT_END2000_DFZ
                ,MONT_END2006_CALC
                ,MONT_END2006_EFFLQ
                ,MONT_END2006_DFZ
                ,MONT_DAL2007_CALC
                ,MONT_DAL2007_EFFLQ
                ,MONT_DAL2007_DFZ
                ,IIMPST_PRVR_CALC
                ,IIMPST_PRVR_EFFLQ
                ,IIMPST_PRVR_DFZ
                ,IIMPST_252_CALC
                ,IIMPST_252_EFFLQ
                ,IIMPST_252_DFZ
                ,IMPST_252_CALC
                ,IMPST_252_EFFLQ
                ,RIT_TFR_CALC
                ,RIT_TFR_EFFLQ
                ,RIT_TFR_DFZ
                ,CNBT_INPSTFM_CALC
                ,CNBT_INPSTFM_EFFLQ
                ,CNBT_INPSTFM_DFZ
                ,ICNB_INPSTFM_CALC
                ,ICNB_INPSTFM_EFFLQ
                ,ICNB_INPSTFM_DFZ
                ,CNDE_END2000_CALC
                ,CNDE_END2000_EFFLQ
                ,CNDE_END2000_DFZ
                ,CNDE_END2006_CALC
                ,CNDE_END2006_EFFLQ
                ,CNDE_END2006_DFZ
                ,CNDE_DAL2007_CALC
                ,CNDE_DAL2007_EFFLQ
                ,CNDE_DAL2007_DFZ
                ,AA_CNBZ_END2000_EF
                ,AA_CNBZ_END2006_EF
                ,AA_CNBZ_DAL2007_EF
                ,MM_CNBZ_END2000_EF
                ,MM_CNBZ_END2006_EF
                ,MM_CNBZ_DAL2007_EF
                ,IMPST_DA_RIMB_EFF
                ,ALQ_TAX_SEP_CALC
                ,ALQ_TAX_SEP_EFFLQ
                ,ALQ_TAX_SEP_DFZ
                ,ALQ_CNBT_INPSTFM_C
                ,ALQ_CNBT_INPSTFM_E
                ,ALQ_CNBT_INPSTFM_D
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,IMPB_VIS_1382011C
                ,IMPB_VIS_1382011D
                ,IMPB_VIS_1382011L
                ,IMPST_VIS_1382011C
                ,IMPST_VIS_1382011D
                ,IMPST_VIS_1382011L
                ,IMPB_IS_1382011C
                ,IMPB_IS_1382011D
                ,IMPB_IS_1382011L
                ,IS_1382011C
                ,IS_1382011D
                ,IS_1382011L
                ,IMP_INTR_RIT_PAG_C
                ,IMP_INTR_RIT_PAG_D
                ,IMP_INTR_RIT_PAG_L
                ,IMPB_BOLLO_DETT_C
                ,IMPB_BOLLO_DETT_D
                ,IMPB_BOLLO_DETT_L
                ,IMPST_BOLLO_DETT_C
                ,IMPST_BOLLO_DETT_D
                ,IMPST_BOLLO_DETT_L
                ,IMPST_BOLLO_TOT_VC
                ,IMPST_BOLLO_TOT_VD
                ,IMPST_BOLLO_TOT_VL
                ,IMPB_VIS_662014C
                ,IMPB_VIS_662014D
                ,IMPB_VIS_662014L
                ,IMPST_VIS_662014C
                ,IMPST_VIS_662014D
                ,IMPST_VIS_662014L
                ,IMPB_IS_662014C
                ,IMPB_IS_662014D
                ,IMPB_IS_662014L
                ,IS_662014C
                ,IS_662014D
                ,IS_662014L
             INTO
                :DFL-ID-D-FORZ-LIQ
               ,:DFL-ID-LIQ
               ,:DFL-ID-MOVI-CRZ
               ,:DFL-ID-MOVI-CHIU
                :IND-DFL-ID-MOVI-CHIU
               ,:DFL-COD-COMP-ANIA
               ,:DFL-DT-INI-EFF-DB
               ,:DFL-DT-END-EFF-DB
               ,:DFL-IMP-LRD-CALC
                :IND-DFL-IMP-LRD-CALC
               ,:DFL-IMP-LRD-DFZ
                :IND-DFL-IMP-LRD-DFZ
               ,:DFL-IMP-LRD-EFFLQ
                :IND-DFL-IMP-LRD-EFFLQ
               ,:DFL-IMP-NET-CALC
                :IND-DFL-IMP-NET-CALC
               ,:DFL-IMP-NET-DFZ
                :IND-DFL-IMP-NET-DFZ
               ,:DFL-IMP-NET-EFFLQ
                :IND-DFL-IMP-NET-EFFLQ
               ,:DFL-IMPST-PRVR-CALC
                :IND-DFL-IMPST-PRVR-CALC
               ,:DFL-IMPST-PRVR-DFZ
                :IND-DFL-IMPST-PRVR-DFZ
               ,:DFL-IMPST-PRVR-EFFLQ
                :IND-DFL-IMPST-PRVR-EFFLQ
               ,:DFL-IMPST-VIS-CALC
                :IND-DFL-IMPST-VIS-CALC
               ,:DFL-IMPST-VIS-DFZ
                :IND-DFL-IMPST-VIS-DFZ
               ,:DFL-IMPST-VIS-EFFLQ
                :IND-DFL-IMPST-VIS-EFFLQ
               ,:DFL-RIT-ACC-CALC
                :IND-DFL-RIT-ACC-CALC
               ,:DFL-RIT-ACC-DFZ
                :IND-DFL-RIT-ACC-DFZ
               ,:DFL-RIT-ACC-EFFLQ
                :IND-DFL-RIT-ACC-EFFLQ
               ,:DFL-RIT-IRPEF-CALC
                :IND-DFL-RIT-IRPEF-CALC
               ,:DFL-RIT-IRPEF-DFZ
                :IND-DFL-RIT-IRPEF-DFZ
               ,:DFL-RIT-IRPEF-EFFLQ
                :IND-DFL-RIT-IRPEF-EFFLQ
               ,:DFL-IMPST-SOST-CALC
                :IND-DFL-IMPST-SOST-CALC
               ,:DFL-IMPST-SOST-DFZ
                :IND-DFL-IMPST-SOST-DFZ
               ,:DFL-IMPST-SOST-EFFLQ
                :IND-DFL-IMPST-SOST-EFFLQ
               ,:DFL-TAX-SEP-CALC
                :IND-DFL-TAX-SEP-CALC
               ,:DFL-TAX-SEP-DFZ
                :IND-DFL-TAX-SEP-DFZ
               ,:DFL-TAX-SEP-EFFLQ
                :IND-DFL-TAX-SEP-EFFLQ
               ,:DFL-INTR-PREST-CALC
                :IND-DFL-INTR-PREST-CALC
               ,:DFL-INTR-PREST-DFZ
                :IND-DFL-INTR-PREST-DFZ
               ,:DFL-INTR-PREST-EFFLQ
                :IND-DFL-INTR-PREST-EFFLQ
               ,:DFL-ACCPRE-SOST-CALC
                :IND-DFL-ACCPRE-SOST-CALC
               ,:DFL-ACCPRE-SOST-DFZ
                :IND-DFL-ACCPRE-SOST-DFZ
               ,:DFL-ACCPRE-SOST-EFFLQ
                :IND-DFL-ACCPRE-SOST-EFFLQ
               ,:DFL-ACCPRE-VIS-CALC
                :IND-DFL-ACCPRE-VIS-CALC
               ,:DFL-ACCPRE-VIS-DFZ
                :IND-DFL-ACCPRE-VIS-DFZ
               ,:DFL-ACCPRE-VIS-EFFLQ
                :IND-DFL-ACCPRE-VIS-EFFLQ
               ,:DFL-ACCPRE-ACC-CALC
                :IND-DFL-ACCPRE-ACC-CALC
               ,:DFL-ACCPRE-ACC-DFZ
                :IND-DFL-ACCPRE-ACC-DFZ
               ,:DFL-ACCPRE-ACC-EFFLQ
                :IND-DFL-ACCPRE-ACC-EFFLQ
               ,:DFL-RES-PRSTZ-CALC
                :IND-DFL-RES-PRSTZ-CALC
               ,:DFL-RES-PRSTZ-DFZ
                :IND-DFL-RES-PRSTZ-DFZ
               ,:DFL-RES-PRSTZ-EFFLQ
                :IND-DFL-RES-PRSTZ-EFFLQ
               ,:DFL-RES-PRE-ATT-CALC
                :IND-DFL-RES-PRE-ATT-CALC
               ,:DFL-RES-PRE-ATT-DFZ
                :IND-DFL-RES-PRE-ATT-DFZ
               ,:DFL-RES-PRE-ATT-EFFLQ
                :IND-DFL-RES-PRE-ATT-EFFLQ
               ,:DFL-IMP-EXCONTR-EFF
                :IND-DFL-IMP-EXCONTR-EFF
               ,:DFL-IMPB-VIS-CALC
                :IND-DFL-IMPB-VIS-CALC
               ,:DFL-IMPB-VIS-EFFLQ
                :IND-DFL-IMPB-VIS-EFFLQ
               ,:DFL-IMPB-VIS-DFZ
                :IND-DFL-IMPB-VIS-DFZ
               ,:DFL-IMPB-RIT-ACC-CALC
                :IND-DFL-IMPB-RIT-ACC-CALC
               ,:DFL-IMPB-RIT-ACC-EFFLQ
                :IND-DFL-IMPB-RIT-ACC-EFFLQ
               ,:DFL-IMPB-RIT-ACC-DFZ
                :IND-DFL-IMPB-RIT-ACC-DFZ
               ,:DFL-IMPB-TFR-CALC
                :IND-DFL-IMPB-TFR-CALC
               ,:DFL-IMPB-TFR-EFFLQ
                :IND-DFL-IMPB-TFR-EFFLQ
               ,:DFL-IMPB-TFR-DFZ
                :IND-DFL-IMPB-TFR-DFZ
               ,:DFL-IMPB-IS-CALC
                :IND-DFL-IMPB-IS-CALC
               ,:DFL-IMPB-IS-EFFLQ
                :IND-DFL-IMPB-IS-EFFLQ
               ,:DFL-IMPB-IS-DFZ
                :IND-DFL-IMPB-IS-DFZ
               ,:DFL-IMPB-TAX-SEP-CALC
                :IND-DFL-IMPB-TAX-SEP-CALC
               ,:DFL-IMPB-TAX-SEP-EFFLQ
                :IND-DFL-IMPB-TAX-SEP-EFFLQ
               ,:DFL-IMPB-TAX-SEP-DFZ
                :IND-DFL-IMPB-TAX-SEP-DFZ
               ,:DFL-IINT-PREST-CALC
                :IND-DFL-IINT-PREST-CALC
               ,:DFL-IINT-PREST-EFFLQ
                :IND-DFL-IINT-PREST-EFFLQ
               ,:DFL-IINT-PREST-DFZ
                :IND-DFL-IINT-PREST-DFZ
               ,:DFL-MONT-END2000-CALC
                :IND-DFL-MONT-END2000-CALC
               ,:DFL-MONT-END2000-EFFLQ
                :IND-DFL-MONT-END2000-EFFLQ
               ,:DFL-MONT-END2000-DFZ
                :IND-DFL-MONT-END2000-DFZ
               ,:DFL-MONT-END2006-CALC
                :IND-DFL-MONT-END2006-CALC
               ,:DFL-MONT-END2006-EFFLQ
                :IND-DFL-MONT-END2006-EFFLQ
               ,:DFL-MONT-END2006-DFZ
                :IND-DFL-MONT-END2006-DFZ
               ,:DFL-MONT-DAL2007-CALC
                :IND-DFL-MONT-DAL2007-CALC
               ,:DFL-MONT-DAL2007-EFFLQ
                :IND-DFL-MONT-DAL2007-EFFLQ
               ,:DFL-MONT-DAL2007-DFZ
                :IND-DFL-MONT-DAL2007-DFZ
               ,:DFL-IIMPST-PRVR-CALC
                :IND-DFL-IIMPST-PRVR-CALC
               ,:DFL-IIMPST-PRVR-EFFLQ
                :IND-DFL-IIMPST-PRVR-EFFLQ
               ,:DFL-IIMPST-PRVR-DFZ
                :IND-DFL-IIMPST-PRVR-DFZ
               ,:DFL-IIMPST-252-CALC
                :IND-DFL-IIMPST-252-CALC
               ,:DFL-IIMPST-252-EFFLQ
                :IND-DFL-IIMPST-252-EFFLQ
               ,:DFL-IIMPST-252-DFZ
                :IND-DFL-IIMPST-252-DFZ
               ,:DFL-IMPST-252-CALC
                :IND-DFL-IMPST-252-CALC
               ,:DFL-IMPST-252-EFFLQ
                :IND-DFL-IMPST-252-EFFLQ
               ,:DFL-RIT-TFR-CALC
                :IND-DFL-RIT-TFR-CALC
               ,:DFL-RIT-TFR-EFFLQ
                :IND-DFL-RIT-TFR-EFFLQ
               ,:DFL-RIT-TFR-DFZ
                :IND-DFL-RIT-TFR-DFZ
               ,:DFL-CNBT-INPSTFM-CALC
                :IND-DFL-CNBT-INPSTFM-CALC
               ,:DFL-CNBT-INPSTFM-EFFLQ
                :IND-DFL-CNBT-INPSTFM-EFFLQ
               ,:DFL-CNBT-INPSTFM-DFZ
                :IND-DFL-CNBT-INPSTFM-DFZ
               ,:DFL-ICNB-INPSTFM-CALC
                :IND-DFL-ICNB-INPSTFM-CALC
               ,:DFL-ICNB-INPSTFM-EFFLQ
                :IND-DFL-ICNB-INPSTFM-EFFLQ
               ,:DFL-ICNB-INPSTFM-DFZ
                :IND-DFL-ICNB-INPSTFM-DFZ
               ,:DFL-CNDE-END2000-CALC
                :IND-DFL-CNDE-END2000-CALC
               ,:DFL-CNDE-END2000-EFFLQ
                :IND-DFL-CNDE-END2000-EFFLQ
               ,:DFL-CNDE-END2000-DFZ
                :IND-DFL-CNDE-END2000-DFZ
               ,:DFL-CNDE-END2006-CALC
                :IND-DFL-CNDE-END2006-CALC
               ,:DFL-CNDE-END2006-EFFLQ
                :IND-DFL-CNDE-END2006-EFFLQ
               ,:DFL-CNDE-END2006-DFZ
                :IND-DFL-CNDE-END2006-DFZ
               ,:DFL-CNDE-DAL2007-CALC
                :IND-DFL-CNDE-DAL2007-CALC
               ,:DFL-CNDE-DAL2007-EFFLQ
                :IND-DFL-CNDE-DAL2007-EFFLQ
               ,:DFL-CNDE-DAL2007-DFZ
                :IND-DFL-CNDE-DAL2007-DFZ
               ,:DFL-AA-CNBZ-END2000-EF
                :IND-DFL-AA-CNBZ-END2000-EF
               ,:DFL-AA-CNBZ-END2006-EF
                :IND-DFL-AA-CNBZ-END2006-EF
               ,:DFL-AA-CNBZ-DAL2007-EF
                :IND-DFL-AA-CNBZ-DAL2007-EF
               ,:DFL-MM-CNBZ-END2000-EF
                :IND-DFL-MM-CNBZ-END2000-EF
               ,:DFL-MM-CNBZ-END2006-EF
                :IND-DFL-MM-CNBZ-END2006-EF
               ,:DFL-MM-CNBZ-DAL2007-EF
                :IND-DFL-MM-CNBZ-DAL2007-EF
               ,:DFL-IMPST-DA-RIMB-EFF
                :IND-DFL-IMPST-DA-RIMB-EFF
               ,:DFL-ALQ-TAX-SEP-CALC
                :IND-DFL-ALQ-TAX-SEP-CALC
               ,:DFL-ALQ-TAX-SEP-EFFLQ
                :IND-DFL-ALQ-TAX-SEP-EFFLQ
               ,:DFL-ALQ-TAX-SEP-DFZ
                :IND-DFL-ALQ-TAX-SEP-DFZ
               ,:DFL-ALQ-CNBT-INPSTFM-C
                :IND-DFL-ALQ-CNBT-INPSTFM-C
               ,:DFL-ALQ-CNBT-INPSTFM-E
                :IND-DFL-ALQ-CNBT-INPSTFM-E
               ,:DFL-ALQ-CNBT-INPSTFM-D
                :IND-DFL-ALQ-CNBT-INPSTFM-D
               ,:DFL-DS-RIGA
               ,:DFL-DS-OPER-SQL
               ,:DFL-DS-VER
               ,:DFL-DS-TS-INI-CPTZ
               ,:DFL-DS-TS-END-CPTZ
               ,:DFL-DS-UTENTE
               ,:DFL-DS-STATO-ELAB
               ,:DFL-IMPB-VIS-1382011C
                :IND-DFL-IMPB-VIS-1382011C
               ,:DFL-IMPB-VIS-1382011D
                :IND-DFL-IMPB-VIS-1382011D
               ,:DFL-IMPB-VIS-1382011L
                :IND-DFL-IMPB-VIS-1382011L
               ,:DFL-IMPST-VIS-1382011C
                :IND-DFL-IMPST-VIS-1382011C
               ,:DFL-IMPST-VIS-1382011D
                :IND-DFL-IMPST-VIS-1382011D
               ,:DFL-IMPST-VIS-1382011L
                :IND-DFL-IMPST-VIS-1382011L
               ,:DFL-IMPB-IS-1382011C
                :IND-DFL-IMPB-IS-1382011C
               ,:DFL-IMPB-IS-1382011D
                :IND-DFL-IMPB-IS-1382011D
               ,:DFL-IMPB-IS-1382011L
                :IND-DFL-IMPB-IS-1382011L
               ,:DFL-IS-1382011C
                :IND-DFL-IS-1382011C
               ,:DFL-IS-1382011D
                :IND-DFL-IS-1382011D
               ,:DFL-IS-1382011L
                :IND-DFL-IS-1382011L
               ,:DFL-IMP-INTR-RIT-PAG-C
                :IND-DFL-IMP-INTR-RIT-PAG-C
               ,:DFL-IMP-INTR-RIT-PAG-D
                :IND-DFL-IMP-INTR-RIT-PAG-D
               ,:DFL-IMP-INTR-RIT-PAG-L
                :IND-DFL-IMP-INTR-RIT-PAG-L
               ,:DFL-IMPB-BOLLO-DETT-C
                :IND-DFL-IMPB-BOLLO-DETT-C
               ,:DFL-IMPB-BOLLO-DETT-D
                :IND-DFL-IMPB-BOLLO-DETT-D
               ,:DFL-IMPB-BOLLO-DETT-L
                :IND-DFL-IMPB-BOLLO-DETT-L
               ,:DFL-IMPST-BOLLO-DETT-C
                :IND-DFL-IMPST-BOLLO-DETT-C
               ,:DFL-IMPST-BOLLO-DETT-D
                :IND-DFL-IMPST-BOLLO-DETT-D
               ,:DFL-IMPST-BOLLO-DETT-L
                :IND-DFL-IMPST-BOLLO-DETT-L
               ,:DFL-IMPST-BOLLO-TOT-VC
                :IND-DFL-IMPST-BOLLO-TOT-VC
               ,:DFL-IMPST-BOLLO-TOT-VD
                :IND-DFL-IMPST-BOLLO-TOT-VD
               ,:DFL-IMPST-BOLLO-TOT-VL
                :IND-DFL-IMPST-BOLLO-TOT-VL
               ,:DFL-IMPB-VIS-662014C
                :IND-DFL-IMPB-VIS-662014C
               ,:DFL-IMPB-VIS-662014D
                :IND-DFL-IMPB-VIS-662014D
               ,:DFL-IMPB-VIS-662014L
                :IND-DFL-IMPB-VIS-662014L
               ,:DFL-IMPST-VIS-662014C
                :IND-DFL-IMPST-VIS-662014C
               ,:DFL-IMPST-VIS-662014D
                :IND-DFL-IMPST-VIS-662014D
               ,:DFL-IMPST-VIS-662014L
                :IND-DFL-IMPST-VIS-662014L
               ,:DFL-IMPB-IS-662014C
                :IND-DFL-IMPB-IS-662014C
               ,:DFL-IMPB-IS-662014D
                :IND-DFL-IMPB-IS-662014D
               ,:DFL-IMPB-IS-662014L
                :IND-DFL-IMPB-IS-662014L
               ,:DFL-IS-662014C
                :IND-DFL-IS-662014C
               ,:DFL-IS-662014D
                :IND-DFL-IS-662014D
               ,:DFL-IS-662014L
                :IND-DFL-IS-662014L
             FROM D_FORZ_LIQ
             WHERE     DS_RIGA = :DFL-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO D_FORZ_LIQ
                     (
                        ID_D_FORZ_LIQ
                       ,ID_LIQ
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,COD_COMP_ANIA
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,IMP_LRD_CALC
                       ,IMP_LRD_DFZ
                       ,IMP_LRD_EFFLQ
                       ,IMP_NET_CALC
                       ,IMP_NET_DFZ
                       ,IMP_NET_EFFLQ
                       ,IMPST_PRVR_CALC
                       ,IMPST_PRVR_DFZ
                       ,IMPST_PRVR_EFFLQ
                       ,IMPST_VIS_CALC
                       ,IMPST_VIS_DFZ
                       ,IMPST_VIS_EFFLQ
                       ,RIT_ACC_CALC
                       ,RIT_ACC_DFZ
                       ,RIT_ACC_EFFLQ
                       ,RIT_IRPEF_CALC
                       ,RIT_IRPEF_DFZ
                       ,RIT_IRPEF_EFFLQ
                       ,IMPST_SOST_CALC
                       ,IMPST_SOST_DFZ
                       ,IMPST_SOST_EFFLQ
                       ,TAX_SEP_CALC
                       ,TAX_SEP_DFZ
                       ,TAX_SEP_EFFLQ
                       ,INTR_PREST_CALC
                       ,INTR_PREST_DFZ
                       ,INTR_PREST_EFFLQ
                       ,ACCPRE_SOST_CALC
                       ,ACCPRE_SOST_DFZ
                       ,ACCPRE_SOST_EFFLQ
                       ,ACCPRE_VIS_CALC
                       ,ACCPRE_VIS_DFZ
                       ,ACCPRE_VIS_EFFLQ
                       ,ACCPRE_ACC_CALC
                       ,ACCPRE_ACC_DFZ
                       ,ACCPRE_ACC_EFFLQ
                       ,RES_PRSTZ_CALC
                       ,RES_PRSTZ_DFZ
                       ,RES_PRSTZ_EFFLQ
                       ,RES_PRE_ATT_CALC
                       ,RES_PRE_ATT_DFZ
                       ,RES_PRE_ATT_EFFLQ
                       ,IMP_EXCONTR_EFF
                       ,IMPB_VIS_CALC
                       ,IMPB_VIS_EFFLQ
                       ,IMPB_VIS_DFZ
                       ,IMPB_RIT_ACC_CALC
                       ,IMPB_RIT_ACC_EFFLQ
                       ,IMPB_RIT_ACC_DFZ
                       ,IMPB_TFR_CALC
                       ,IMPB_TFR_EFFLQ
                       ,IMPB_TFR_DFZ
                       ,IMPB_IS_CALC
                       ,IMPB_IS_EFFLQ
                       ,IMPB_IS_DFZ
                       ,IMPB_TAX_SEP_CALC
                       ,IMPB_TAX_SEP_EFFLQ
                       ,IMPB_TAX_SEP_DFZ
                       ,IINT_PREST_CALC
                       ,IINT_PREST_EFFLQ
                       ,IINT_PREST_DFZ
                       ,MONT_END2000_CALC
                       ,MONT_END2000_EFFLQ
                       ,MONT_END2000_DFZ
                       ,MONT_END2006_CALC
                       ,MONT_END2006_EFFLQ
                       ,MONT_END2006_DFZ
                       ,MONT_DAL2007_CALC
                       ,MONT_DAL2007_EFFLQ
                       ,MONT_DAL2007_DFZ
                       ,IIMPST_PRVR_CALC
                       ,IIMPST_PRVR_EFFLQ
                       ,IIMPST_PRVR_DFZ
                       ,IIMPST_252_CALC
                       ,IIMPST_252_EFFLQ
                       ,IIMPST_252_DFZ
                       ,IMPST_252_CALC
                       ,IMPST_252_EFFLQ
                       ,RIT_TFR_CALC
                       ,RIT_TFR_EFFLQ
                       ,RIT_TFR_DFZ
                       ,CNBT_INPSTFM_CALC
                       ,CNBT_INPSTFM_EFFLQ
                       ,CNBT_INPSTFM_DFZ
                       ,ICNB_INPSTFM_CALC
                       ,ICNB_INPSTFM_EFFLQ
                       ,ICNB_INPSTFM_DFZ
                       ,CNDE_END2000_CALC
                       ,CNDE_END2000_EFFLQ
                       ,CNDE_END2000_DFZ
                       ,CNDE_END2006_CALC
                       ,CNDE_END2006_EFFLQ
                       ,CNDE_END2006_DFZ
                       ,CNDE_DAL2007_CALC
                       ,CNDE_DAL2007_EFFLQ
                       ,CNDE_DAL2007_DFZ
                       ,AA_CNBZ_END2000_EF
                       ,AA_CNBZ_END2006_EF
                       ,AA_CNBZ_DAL2007_EF
                       ,MM_CNBZ_END2000_EF
                       ,MM_CNBZ_END2006_EF
                       ,MM_CNBZ_DAL2007_EF
                       ,IMPST_DA_RIMB_EFF
                       ,ALQ_TAX_SEP_CALC
                       ,ALQ_TAX_SEP_EFFLQ
                       ,ALQ_TAX_SEP_DFZ
                       ,ALQ_CNBT_INPSTFM_C
                       ,ALQ_CNBT_INPSTFM_E
                       ,ALQ_CNBT_INPSTFM_D
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,IMPB_VIS_1382011C
                       ,IMPB_VIS_1382011D
                       ,IMPB_VIS_1382011L
                       ,IMPST_VIS_1382011C
                       ,IMPST_VIS_1382011D
                       ,IMPST_VIS_1382011L
                       ,IMPB_IS_1382011C
                       ,IMPB_IS_1382011D
                       ,IMPB_IS_1382011L
                       ,IS_1382011C
                       ,IS_1382011D
                       ,IS_1382011L
                       ,IMP_INTR_RIT_PAG_C
                       ,IMP_INTR_RIT_PAG_D
                       ,IMP_INTR_RIT_PAG_L
                       ,IMPB_BOLLO_DETT_C
                       ,IMPB_BOLLO_DETT_D
                       ,IMPB_BOLLO_DETT_L
                       ,IMPST_BOLLO_DETT_C
                       ,IMPST_BOLLO_DETT_D
                       ,IMPST_BOLLO_DETT_L
                       ,IMPST_BOLLO_TOT_VC
                       ,IMPST_BOLLO_TOT_VD
                       ,IMPST_BOLLO_TOT_VL
                       ,IMPB_VIS_662014C
                       ,IMPB_VIS_662014D
                       ,IMPB_VIS_662014L
                       ,IMPST_VIS_662014C
                       ,IMPST_VIS_662014D
                       ,IMPST_VIS_662014L
                       ,IMPB_IS_662014C
                       ,IMPB_IS_662014D
                       ,IMPB_IS_662014L
                       ,IS_662014C
                       ,IS_662014D
                       ,IS_662014L
                     )
                 VALUES
                     (
                       :DFL-ID-D-FORZ-LIQ
                       ,:DFL-ID-LIQ
                       ,:DFL-ID-MOVI-CRZ
                       ,:DFL-ID-MOVI-CHIU
                        :IND-DFL-ID-MOVI-CHIU
                       ,:DFL-COD-COMP-ANIA
                       ,:DFL-DT-INI-EFF-DB
                       ,:DFL-DT-END-EFF-DB
                       ,:DFL-IMP-LRD-CALC
                        :IND-DFL-IMP-LRD-CALC
                       ,:DFL-IMP-LRD-DFZ
                        :IND-DFL-IMP-LRD-DFZ
                       ,:DFL-IMP-LRD-EFFLQ
                        :IND-DFL-IMP-LRD-EFFLQ
                       ,:DFL-IMP-NET-CALC
                        :IND-DFL-IMP-NET-CALC
                       ,:DFL-IMP-NET-DFZ
                        :IND-DFL-IMP-NET-DFZ
                       ,:DFL-IMP-NET-EFFLQ
                        :IND-DFL-IMP-NET-EFFLQ
                       ,:DFL-IMPST-PRVR-CALC
                        :IND-DFL-IMPST-PRVR-CALC
                       ,:DFL-IMPST-PRVR-DFZ
                        :IND-DFL-IMPST-PRVR-DFZ
                       ,:DFL-IMPST-PRVR-EFFLQ
                        :IND-DFL-IMPST-PRVR-EFFLQ
                       ,:DFL-IMPST-VIS-CALC
                        :IND-DFL-IMPST-VIS-CALC
                       ,:DFL-IMPST-VIS-DFZ
                        :IND-DFL-IMPST-VIS-DFZ
                       ,:DFL-IMPST-VIS-EFFLQ
                        :IND-DFL-IMPST-VIS-EFFLQ
                       ,:DFL-RIT-ACC-CALC
                        :IND-DFL-RIT-ACC-CALC
                       ,:DFL-RIT-ACC-DFZ
                        :IND-DFL-RIT-ACC-DFZ
                       ,:DFL-RIT-ACC-EFFLQ
                        :IND-DFL-RIT-ACC-EFFLQ
                       ,:DFL-RIT-IRPEF-CALC
                        :IND-DFL-RIT-IRPEF-CALC
                       ,:DFL-RIT-IRPEF-DFZ
                        :IND-DFL-RIT-IRPEF-DFZ
                       ,:DFL-RIT-IRPEF-EFFLQ
                        :IND-DFL-RIT-IRPEF-EFFLQ
                       ,:DFL-IMPST-SOST-CALC
                        :IND-DFL-IMPST-SOST-CALC
                       ,:DFL-IMPST-SOST-DFZ
                        :IND-DFL-IMPST-SOST-DFZ
                       ,:DFL-IMPST-SOST-EFFLQ
                        :IND-DFL-IMPST-SOST-EFFLQ
                       ,:DFL-TAX-SEP-CALC
                        :IND-DFL-TAX-SEP-CALC
                       ,:DFL-TAX-SEP-DFZ
                        :IND-DFL-TAX-SEP-DFZ
                       ,:DFL-TAX-SEP-EFFLQ
                        :IND-DFL-TAX-SEP-EFFLQ
                       ,:DFL-INTR-PREST-CALC
                        :IND-DFL-INTR-PREST-CALC
                       ,:DFL-INTR-PREST-DFZ
                        :IND-DFL-INTR-PREST-DFZ
                       ,:DFL-INTR-PREST-EFFLQ
                        :IND-DFL-INTR-PREST-EFFLQ
                       ,:DFL-ACCPRE-SOST-CALC
                        :IND-DFL-ACCPRE-SOST-CALC
                       ,:DFL-ACCPRE-SOST-DFZ
                        :IND-DFL-ACCPRE-SOST-DFZ
                       ,:DFL-ACCPRE-SOST-EFFLQ
                        :IND-DFL-ACCPRE-SOST-EFFLQ
                       ,:DFL-ACCPRE-VIS-CALC
                        :IND-DFL-ACCPRE-VIS-CALC
                       ,:DFL-ACCPRE-VIS-DFZ
                        :IND-DFL-ACCPRE-VIS-DFZ
                       ,:DFL-ACCPRE-VIS-EFFLQ
                        :IND-DFL-ACCPRE-VIS-EFFLQ
                       ,:DFL-ACCPRE-ACC-CALC
                        :IND-DFL-ACCPRE-ACC-CALC
                       ,:DFL-ACCPRE-ACC-DFZ
                        :IND-DFL-ACCPRE-ACC-DFZ
                       ,:DFL-ACCPRE-ACC-EFFLQ
                        :IND-DFL-ACCPRE-ACC-EFFLQ
                       ,:DFL-RES-PRSTZ-CALC
                        :IND-DFL-RES-PRSTZ-CALC
                       ,:DFL-RES-PRSTZ-DFZ
                        :IND-DFL-RES-PRSTZ-DFZ
                       ,:DFL-RES-PRSTZ-EFFLQ
                        :IND-DFL-RES-PRSTZ-EFFLQ
                       ,:DFL-RES-PRE-ATT-CALC
                        :IND-DFL-RES-PRE-ATT-CALC
                       ,:DFL-RES-PRE-ATT-DFZ
                        :IND-DFL-RES-PRE-ATT-DFZ
                       ,:DFL-RES-PRE-ATT-EFFLQ
                        :IND-DFL-RES-PRE-ATT-EFFLQ
                       ,:DFL-IMP-EXCONTR-EFF
                        :IND-DFL-IMP-EXCONTR-EFF
                       ,:DFL-IMPB-VIS-CALC
                        :IND-DFL-IMPB-VIS-CALC
                       ,:DFL-IMPB-VIS-EFFLQ
                        :IND-DFL-IMPB-VIS-EFFLQ
                       ,:DFL-IMPB-VIS-DFZ
                        :IND-DFL-IMPB-VIS-DFZ
                       ,:DFL-IMPB-RIT-ACC-CALC
                        :IND-DFL-IMPB-RIT-ACC-CALC
                       ,:DFL-IMPB-RIT-ACC-EFFLQ
                        :IND-DFL-IMPB-RIT-ACC-EFFLQ
                       ,:DFL-IMPB-RIT-ACC-DFZ
                        :IND-DFL-IMPB-RIT-ACC-DFZ
                       ,:DFL-IMPB-TFR-CALC
                        :IND-DFL-IMPB-TFR-CALC
                       ,:DFL-IMPB-TFR-EFFLQ
                        :IND-DFL-IMPB-TFR-EFFLQ
                       ,:DFL-IMPB-TFR-DFZ
                        :IND-DFL-IMPB-TFR-DFZ
                       ,:DFL-IMPB-IS-CALC
                        :IND-DFL-IMPB-IS-CALC
                       ,:DFL-IMPB-IS-EFFLQ
                        :IND-DFL-IMPB-IS-EFFLQ
                       ,:DFL-IMPB-IS-DFZ
                        :IND-DFL-IMPB-IS-DFZ
                       ,:DFL-IMPB-TAX-SEP-CALC
                        :IND-DFL-IMPB-TAX-SEP-CALC
                       ,:DFL-IMPB-TAX-SEP-EFFLQ
                        :IND-DFL-IMPB-TAX-SEP-EFFLQ
                       ,:DFL-IMPB-TAX-SEP-DFZ
                        :IND-DFL-IMPB-TAX-SEP-DFZ
                       ,:DFL-IINT-PREST-CALC
                        :IND-DFL-IINT-PREST-CALC
                       ,:DFL-IINT-PREST-EFFLQ
                        :IND-DFL-IINT-PREST-EFFLQ
                       ,:DFL-IINT-PREST-DFZ
                        :IND-DFL-IINT-PREST-DFZ
                       ,:DFL-MONT-END2000-CALC
                        :IND-DFL-MONT-END2000-CALC
                       ,:DFL-MONT-END2000-EFFLQ
                        :IND-DFL-MONT-END2000-EFFLQ
                       ,:DFL-MONT-END2000-DFZ
                        :IND-DFL-MONT-END2000-DFZ
                       ,:DFL-MONT-END2006-CALC
                        :IND-DFL-MONT-END2006-CALC
                       ,:DFL-MONT-END2006-EFFLQ
                        :IND-DFL-MONT-END2006-EFFLQ
                       ,:DFL-MONT-END2006-DFZ
                        :IND-DFL-MONT-END2006-DFZ
                       ,:DFL-MONT-DAL2007-CALC
                        :IND-DFL-MONT-DAL2007-CALC
                       ,:DFL-MONT-DAL2007-EFFLQ
                        :IND-DFL-MONT-DAL2007-EFFLQ
                       ,:DFL-MONT-DAL2007-DFZ
                        :IND-DFL-MONT-DAL2007-DFZ
                       ,:DFL-IIMPST-PRVR-CALC
                        :IND-DFL-IIMPST-PRVR-CALC
                       ,:DFL-IIMPST-PRVR-EFFLQ
                        :IND-DFL-IIMPST-PRVR-EFFLQ
                       ,:DFL-IIMPST-PRVR-DFZ
                        :IND-DFL-IIMPST-PRVR-DFZ
                       ,:DFL-IIMPST-252-CALC
                        :IND-DFL-IIMPST-252-CALC
                       ,:DFL-IIMPST-252-EFFLQ
                        :IND-DFL-IIMPST-252-EFFLQ
                       ,:DFL-IIMPST-252-DFZ
                        :IND-DFL-IIMPST-252-DFZ
                       ,:DFL-IMPST-252-CALC
                        :IND-DFL-IMPST-252-CALC
                       ,:DFL-IMPST-252-EFFLQ
                        :IND-DFL-IMPST-252-EFFLQ
                       ,:DFL-RIT-TFR-CALC
                        :IND-DFL-RIT-TFR-CALC
                       ,:DFL-RIT-TFR-EFFLQ
                        :IND-DFL-RIT-TFR-EFFLQ
                       ,:DFL-RIT-TFR-DFZ
                        :IND-DFL-RIT-TFR-DFZ
                       ,:DFL-CNBT-INPSTFM-CALC
                        :IND-DFL-CNBT-INPSTFM-CALC
                       ,:DFL-CNBT-INPSTFM-EFFLQ
                        :IND-DFL-CNBT-INPSTFM-EFFLQ
                       ,:DFL-CNBT-INPSTFM-DFZ
                        :IND-DFL-CNBT-INPSTFM-DFZ
                       ,:DFL-ICNB-INPSTFM-CALC
                        :IND-DFL-ICNB-INPSTFM-CALC
                       ,:DFL-ICNB-INPSTFM-EFFLQ
                        :IND-DFL-ICNB-INPSTFM-EFFLQ
                       ,:DFL-ICNB-INPSTFM-DFZ
                        :IND-DFL-ICNB-INPSTFM-DFZ
                       ,:DFL-CNDE-END2000-CALC
                        :IND-DFL-CNDE-END2000-CALC
                       ,:DFL-CNDE-END2000-EFFLQ
                        :IND-DFL-CNDE-END2000-EFFLQ
                       ,:DFL-CNDE-END2000-DFZ
                        :IND-DFL-CNDE-END2000-DFZ
                       ,:DFL-CNDE-END2006-CALC
                        :IND-DFL-CNDE-END2006-CALC
                       ,:DFL-CNDE-END2006-EFFLQ
                        :IND-DFL-CNDE-END2006-EFFLQ
                       ,:DFL-CNDE-END2006-DFZ
                        :IND-DFL-CNDE-END2006-DFZ
                       ,:DFL-CNDE-DAL2007-CALC
                        :IND-DFL-CNDE-DAL2007-CALC
                       ,:DFL-CNDE-DAL2007-EFFLQ
                        :IND-DFL-CNDE-DAL2007-EFFLQ
                       ,:DFL-CNDE-DAL2007-DFZ
                        :IND-DFL-CNDE-DAL2007-DFZ
                       ,:DFL-AA-CNBZ-END2000-EF
                        :IND-DFL-AA-CNBZ-END2000-EF
                       ,:DFL-AA-CNBZ-END2006-EF
                        :IND-DFL-AA-CNBZ-END2006-EF
                       ,:DFL-AA-CNBZ-DAL2007-EF
                        :IND-DFL-AA-CNBZ-DAL2007-EF
                       ,:DFL-MM-CNBZ-END2000-EF
                        :IND-DFL-MM-CNBZ-END2000-EF
                       ,:DFL-MM-CNBZ-END2006-EF
                        :IND-DFL-MM-CNBZ-END2006-EF
                       ,:DFL-MM-CNBZ-DAL2007-EF
                        :IND-DFL-MM-CNBZ-DAL2007-EF
                       ,:DFL-IMPST-DA-RIMB-EFF
                        :IND-DFL-IMPST-DA-RIMB-EFF
                       ,:DFL-ALQ-TAX-SEP-CALC
                        :IND-DFL-ALQ-TAX-SEP-CALC
                       ,:DFL-ALQ-TAX-SEP-EFFLQ
                        :IND-DFL-ALQ-TAX-SEP-EFFLQ
                       ,:DFL-ALQ-TAX-SEP-DFZ
                        :IND-DFL-ALQ-TAX-SEP-DFZ
                       ,:DFL-ALQ-CNBT-INPSTFM-C
                        :IND-DFL-ALQ-CNBT-INPSTFM-C
                       ,:DFL-ALQ-CNBT-INPSTFM-E
                        :IND-DFL-ALQ-CNBT-INPSTFM-E
                       ,:DFL-ALQ-CNBT-INPSTFM-D
                        :IND-DFL-ALQ-CNBT-INPSTFM-D
                       ,:DFL-DS-RIGA
                       ,:DFL-DS-OPER-SQL
                       ,:DFL-DS-VER
                       ,:DFL-DS-TS-INI-CPTZ
                       ,:DFL-DS-TS-END-CPTZ
                       ,:DFL-DS-UTENTE
                       ,:DFL-DS-STATO-ELAB
                       ,:DFL-IMPB-VIS-1382011C
                        :IND-DFL-IMPB-VIS-1382011C
                       ,:DFL-IMPB-VIS-1382011D
                        :IND-DFL-IMPB-VIS-1382011D
                       ,:DFL-IMPB-VIS-1382011L
                        :IND-DFL-IMPB-VIS-1382011L
                       ,:DFL-IMPST-VIS-1382011C
                        :IND-DFL-IMPST-VIS-1382011C
                       ,:DFL-IMPST-VIS-1382011D
                        :IND-DFL-IMPST-VIS-1382011D
                       ,:DFL-IMPST-VIS-1382011L
                        :IND-DFL-IMPST-VIS-1382011L
                       ,:DFL-IMPB-IS-1382011C
                        :IND-DFL-IMPB-IS-1382011C
                       ,:DFL-IMPB-IS-1382011D
                        :IND-DFL-IMPB-IS-1382011D
                       ,:DFL-IMPB-IS-1382011L
                        :IND-DFL-IMPB-IS-1382011L
                       ,:DFL-IS-1382011C
                        :IND-DFL-IS-1382011C
                       ,:DFL-IS-1382011D
                        :IND-DFL-IS-1382011D
                       ,:DFL-IS-1382011L
                        :IND-DFL-IS-1382011L
                       ,:DFL-IMP-INTR-RIT-PAG-C
                        :IND-DFL-IMP-INTR-RIT-PAG-C
                       ,:DFL-IMP-INTR-RIT-PAG-D
                        :IND-DFL-IMP-INTR-RIT-PAG-D
                       ,:DFL-IMP-INTR-RIT-PAG-L
                        :IND-DFL-IMP-INTR-RIT-PAG-L
                       ,:DFL-IMPB-BOLLO-DETT-C
                        :IND-DFL-IMPB-BOLLO-DETT-C
                       ,:DFL-IMPB-BOLLO-DETT-D
                        :IND-DFL-IMPB-BOLLO-DETT-D
                       ,:DFL-IMPB-BOLLO-DETT-L
                        :IND-DFL-IMPB-BOLLO-DETT-L
                       ,:DFL-IMPST-BOLLO-DETT-C
                        :IND-DFL-IMPST-BOLLO-DETT-C
                       ,:DFL-IMPST-BOLLO-DETT-D
                        :IND-DFL-IMPST-BOLLO-DETT-D
                       ,:DFL-IMPST-BOLLO-DETT-L
                        :IND-DFL-IMPST-BOLLO-DETT-L
                       ,:DFL-IMPST-BOLLO-TOT-VC
                        :IND-DFL-IMPST-BOLLO-TOT-VC
                       ,:DFL-IMPST-BOLLO-TOT-VD
                        :IND-DFL-IMPST-BOLLO-TOT-VD
                       ,:DFL-IMPST-BOLLO-TOT-VL
                        :IND-DFL-IMPST-BOLLO-TOT-VL
                       ,:DFL-IMPB-VIS-662014C
                        :IND-DFL-IMPB-VIS-662014C
                       ,:DFL-IMPB-VIS-662014D
                        :IND-DFL-IMPB-VIS-662014D
                       ,:DFL-IMPB-VIS-662014L
                        :IND-DFL-IMPB-VIS-662014L
                       ,:DFL-IMPST-VIS-662014C
                        :IND-DFL-IMPST-VIS-662014C
                       ,:DFL-IMPST-VIS-662014D
                        :IND-DFL-IMPST-VIS-662014D
                       ,:DFL-IMPST-VIS-662014L
                        :IND-DFL-IMPST-VIS-662014L
                       ,:DFL-IMPB-IS-662014C
                        :IND-DFL-IMPB-IS-662014C
                       ,:DFL-IMPB-IS-662014D
                        :IND-DFL-IMPB-IS-662014D
                       ,:DFL-IMPB-IS-662014L
                        :IND-DFL-IMPB-IS-662014L
                       ,:DFL-IS-662014C
                        :IND-DFL-IS-662014C
                       ,:DFL-IS-662014D
                        :IND-DFL-IS-662014D
                       ,:DFL-IS-662014L
                        :IND-DFL-IS-662014L
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE D_FORZ_LIQ SET

                   ID_D_FORZ_LIQ          =
                :DFL-ID-D-FORZ-LIQ
                  ,ID_LIQ                 =
                :DFL-ID-LIQ
                  ,ID_MOVI_CRZ            =
                :DFL-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :DFL-ID-MOVI-CHIU
                                       :IND-DFL-ID-MOVI-CHIU
                  ,COD_COMP_ANIA          =
                :DFL-COD-COMP-ANIA
                  ,DT_INI_EFF             =
           :DFL-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :DFL-DT-END-EFF-DB
                  ,IMP_LRD_CALC           =
                :DFL-IMP-LRD-CALC
                                       :IND-DFL-IMP-LRD-CALC
                  ,IMP_LRD_DFZ            =
                :DFL-IMP-LRD-DFZ
                                       :IND-DFL-IMP-LRD-DFZ
                  ,IMP_LRD_EFFLQ          =
                :DFL-IMP-LRD-EFFLQ
                                       :IND-DFL-IMP-LRD-EFFLQ
                  ,IMP_NET_CALC           =
                :DFL-IMP-NET-CALC
                                       :IND-DFL-IMP-NET-CALC
                  ,IMP_NET_DFZ            =
                :DFL-IMP-NET-DFZ
                                       :IND-DFL-IMP-NET-DFZ
                  ,IMP_NET_EFFLQ          =
                :DFL-IMP-NET-EFFLQ
                                       :IND-DFL-IMP-NET-EFFLQ
                  ,IMPST_PRVR_CALC        =
                :DFL-IMPST-PRVR-CALC
                                       :IND-DFL-IMPST-PRVR-CALC
                  ,IMPST_PRVR_DFZ         =
                :DFL-IMPST-PRVR-DFZ
                                       :IND-DFL-IMPST-PRVR-DFZ
                  ,IMPST_PRVR_EFFLQ       =
                :DFL-IMPST-PRVR-EFFLQ
                                       :IND-DFL-IMPST-PRVR-EFFLQ
                  ,IMPST_VIS_CALC         =
                :DFL-IMPST-VIS-CALC
                                       :IND-DFL-IMPST-VIS-CALC
                  ,IMPST_VIS_DFZ          =
                :DFL-IMPST-VIS-DFZ
                                       :IND-DFL-IMPST-VIS-DFZ
                  ,IMPST_VIS_EFFLQ        =
                :DFL-IMPST-VIS-EFFLQ
                                       :IND-DFL-IMPST-VIS-EFFLQ
                  ,RIT_ACC_CALC           =
                :DFL-RIT-ACC-CALC
                                       :IND-DFL-RIT-ACC-CALC
                  ,RIT_ACC_DFZ            =
                :DFL-RIT-ACC-DFZ
                                       :IND-DFL-RIT-ACC-DFZ
                  ,RIT_ACC_EFFLQ          =
                :DFL-RIT-ACC-EFFLQ
                                       :IND-DFL-RIT-ACC-EFFLQ
                  ,RIT_IRPEF_CALC         =
                :DFL-RIT-IRPEF-CALC
                                       :IND-DFL-RIT-IRPEF-CALC
                  ,RIT_IRPEF_DFZ          =
                :DFL-RIT-IRPEF-DFZ
                                       :IND-DFL-RIT-IRPEF-DFZ
                  ,RIT_IRPEF_EFFLQ        =
                :DFL-RIT-IRPEF-EFFLQ
                                       :IND-DFL-RIT-IRPEF-EFFLQ
                  ,IMPST_SOST_CALC        =
                :DFL-IMPST-SOST-CALC
                                       :IND-DFL-IMPST-SOST-CALC
                  ,IMPST_SOST_DFZ         =
                :DFL-IMPST-SOST-DFZ
                                       :IND-DFL-IMPST-SOST-DFZ
                  ,IMPST_SOST_EFFLQ       =
                :DFL-IMPST-SOST-EFFLQ
                                       :IND-DFL-IMPST-SOST-EFFLQ
                  ,TAX_SEP_CALC           =
                :DFL-TAX-SEP-CALC
                                       :IND-DFL-TAX-SEP-CALC
                  ,TAX_SEP_DFZ            =
                :DFL-TAX-SEP-DFZ
                                       :IND-DFL-TAX-SEP-DFZ
                  ,TAX_SEP_EFFLQ          =
                :DFL-TAX-SEP-EFFLQ
                                       :IND-DFL-TAX-SEP-EFFLQ
                  ,INTR_PREST_CALC        =
                :DFL-INTR-PREST-CALC
                                       :IND-DFL-INTR-PREST-CALC
                  ,INTR_PREST_DFZ         =
                :DFL-INTR-PREST-DFZ
                                       :IND-DFL-INTR-PREST-DFZ
                  ,INTR_PREST_EFFLQ       =
                :DFL-INTR-PREST-EFFLQ
                                       :IND-DFL-INTR-PREST-EFFLQ
                  ,ACCPRE_SOST_CALC       =
                :DFL-ACCPRE-SOST-CALC
                                       :IND-DFL-ACCPRE-SOST-CALC
                  ,ACCPRE_SOST_DFZ        =
                :DFL-ACCPRE-SOST-DFZ
                                       :IND-DFL-ACCPRE-SOST-DFZ
                  ,ACCPRE_SOST_EFFLQ      =
                :DFL-ACCPRE-SOST-EFFLQ
                                       :IND-DFL-ACCPRE-SOST-EFFLQ
                  ,ACCPRE_VIS_CALC        =
                :DFL-ACCPRE-VIS-CALC
                                       :IND-DFL-ACCPRE-VIS-CALC
                  ,ACCPRE_VIS_DFZ         =
                :DFL-ACCPRE-VIS-DFZ
                                       :IND-DFL-ACCPRE-VIS-DFZ
                  ,ACCPRE_VIS_EFFLQ       =
                :DFL-ACCPRE-VIS-EFFLQ
                                       :IND-DFL-ACCPRE-VIS-EFFLQ
                  ,ACCPRE_ACC_CALC        =
                :DFL-ACCPRE-ACC-CALC
                                       :IND-DFL-ACCPRE-ACC-CALC
                  ,ACCPRE_ACC_DFZ         =
                :DFL-ACCPRE-ACC-DFZ
                                       :IND-DFL-ACCPRE-ACC-DFZ
                  ,ACCPRE_ACC_EFFLQ       =
                :DFL-ACCPRE-ACC-EFFLQ
                                       :IND-DFL-ACCPRE-ACC-EFFLQ
                  ,RES_PRSTZ_CALC         =
                :DFL-RES-PRSTZ-CALC
                                       :IND-DFL-RES-PRSTZ-CALC
                  ,RES_PRSTZ_DFZ          =
                :DFL-RES-PRSTZ-DFZ
                                       :IND-DFL-RES-PRSTZ-DFZ
                  ,RES_PRSTZ_EFFLQ        =
                :DFL-RES-PRSTZ-EFFLQ
                                       :IND-DFL-RES-PRSTZ-EFFLQ
                  ,RES_PRE_ATT_CALC       =
                :DFL-RES-PRE-ATT-CALC
                                       :IND-DFL-RES-PRE-ATT-CALC
                  ,RES_PRE_ATT_DFZ        =
                :DFL-RES-PRE-ATT-DFZ
                                       :IND-DFL-RES-PRE-ATT-DFZ
                  ,RES_PRE_ATT_EFFLQ      =
                :DFL-RES-PRE-ATT-EFFLQ
                                       :IND-DFL-RES-PRE-ATT-EFFLQ
                  ,IMP_EXCONTR_EFF        =
                :DFL-IMP-EXCONTR-EFF
                                       :IND-DFL-IMP-EXCONTR-EFF
                  ,IMPB_VIS_CALC          =
                :DFL-IMPB-VIS-CALC
                                       :IND-DFL-IMPB-VIS-CALC
                  ,IMPB_VIS_EFFLQ         =
                :DFL-IMPB-VIS-EFFLQ
                                       :IND-DFL-IMPB-VIS-EFFLQ
                  ,IMPB_VIS_DFZ           =
                :DFL-IMPB-VIS-DFZ
                                       :IND-DFL-IMPB-VIS-DFZ
                  ,IMPB_RIT_ACC_CALC      =
                :DFL-IMPB-RIT-ACC-CALC
                                       :IND-DFL-IMPB-RIT-ACC-CALC
                  ,IMPB_RIT_ACC_EFFLQ     =
                :DFL-IMPB-RIT-ACC-EFFLQ
                                       :IND-DFL-IMPB-RIT-ACC-EFFLQ
                  ,IMPB_RIT_ACC_DFZ       =
                :DFL-IMPB-RIT-ACC-DFZ
                                       :IND-DFL-IMPB-RIT-ACC-DFZ
                  ,IMPB_TFR_CALC          =
                :DFL-IMPB-TFR-CALC
                                       :IND-DFL-IMPB-TFR-CALC
                  ,IMPB_TFR_EFFLQ         =
                :DFL-IMPB-TFR-EFFLQ
                                       :IND-DFL-IMPB-TFR-EFFLQ
                  ,IMPB_TFR_DFZ           =
                :DFL-IMPB-TFR-DFZ
                                       :IND-DFL-IMPB-TFR-DFZ
                  ,IMPB_IS_CALC           =
                :DFL-IMPB-IS-CALC
                                       :IND-DFL-IMPB-IS-CALC
                  ,IMPB_IS_EFFLQ          =
                :DFL-IMPB-IS-EFFLQ
                                       :IND-DFL-IMPB-IS-EFFLQ
                  ,IMPB_IS_DFZ            =
                :DFL-IMPB-IS-DFZ
                                       :IND-DFL-IMPB-IS-DFZ
                  ,IMPB_TAX_SEP_CALC      =
                :DFL-IMPB-TAX-SEP-CALC
                                       :IND-DFL-IMPB-TAX-SEP-CALC
                  ,IMPB_TAX_SEP_EFFLQ     =
                :DFL-IMPB-TAX-SEP-EFFLQ
                                       :IND-DFL-IMPB-TAX-SEP-EFFLQ
                  ,IMPB_TAX_SEP_DFZ       =
                :DFL-IMPB-TAX-SEP-DFZ
                                       :IND-DFL-IMPB-TAX-SEP-DFZ
                  ,IINT_PREST_CALC        =
                :DFL-IINT-PREST-CALC
                                       :IND-DFL-IINT-PREST-CALC
                  ,IINT_PREST_EFFLQ       =
                :DFL-IINT-PREST-EFFLQ
                                       :IND-DFL-IINT-PREST-EFFLQ
                  ,IINT_PREST_DFZ         =
                :DFL-IINT-PREST-DFZ
                                       :IND-DFL-IINT-PREST-DFZ
                  ,MONT_END2000_CALC      =
                :DFL-MONT-END2000-CALC
                                       :IND-DFL-MONT-END2000-CALC
                  ,MONT_END2000_EFFLQ     =
                :DFL-MONT-END2000-EFFLQ
                                       :IND-DFL-MONT-END2000-EFFLQ
                  ,MONT_END2000_DFZ       =
                :DFL-MONT-END2000-DFZ
                                       :IND-DFL-MONT-END2000-DFZ
                  ,MONT_END2006_CALC      =
                :DFL-MONT-END2006-CALC
                                       :IND-DFL-MONT-END2006-CALC
                  ,MONT_END2006_EFFLQ     =
                :DFL-MONT-END2006-EFFLQ
                                       :IND-DFL-MONT-END2006-EFFLQ
                  ,MONT_END2006_DFZ       =
                :DFL-MONT-END2006-DFZ
                                       :IND-DFL-MONT-END2006-DFZ
                  ,MONT_DAL2007_CALC      =
                :DFL-MONT-DAL2007-CALC
                                       :IND-DFL-MONT-DAL2007-CALC
                  ,MONT_DAL2007_EFFLQ     =
                :DFL-MONT-DAL2007-EFFLQ
                                       :IND-DFL-MONT-DAL2007-EFFLQ
                  ,MONT_DAL2007_DFZ       =
                :DFL-MONT-DAL2007-DFZ
                                       :IND-DFL-MONT-DAL2007-DFZ
                  ,IIMPST_PRVR_CALC       =
                :DFL-IIMPST-PRVR-CALC
                                       :IND-DFL-IIMPST-PRVR-CALC
                  ,IIMPST_PRVR_EFFLQ      =
                :DFL-IIMPST-PRVR-EFFLQ
                                       :IND-DFL-IIMPST-PRVR-EFFLQ
                  ,IIMPST_PRVR_DFZ        =
                :DFL-IIMPST-PRVR-DFZ
                                       :IND-DFL-IIMPST-PRVR-DFZ
                  ,IIMPST_252_CALC        =
                :DFL-IIMPST-252-CALC
                                       :IND-DFL-IIMPST-252-CALC
                  ,IIMPST_252_EFFLQ       =
                :DFL-IIMPST-252-EFFLQ
                                       :IND-DFL-IIMPST-252-EFFLQ
                  ,IIMPST_252_DFZ         =
                :DFL-IIMPST-252-DFZ
                                       :IND-DFL-IIMPST-252-DFZ
                  ,IMPST_252_CALC         =
                :DFL-IMPST-252-CALC
                                       :IND-DFL-IMPST-252-CALC
                  ,IMPST_252_EFFLQ        =
                :DFL-IMPST-252-EFFLQ
                                       :IND-DFL-IMPST-252-EFFLQ
                  ,RIT_TFR_CALC           =
                :DFL-RIT-TFR-CALC
                                       :IND-DFL-RIT-TFR-CALC
                  ,RIT_TFR_EFFLQ          =
                :DFL-RIT-TFR-EFFLQ
                                       :IND-DFL-RIT-TFR-EFFLQ
                  ,RIT_TFR_DFZ            =
                :DFL-RIT-TFR-DFZ
                                       :IND-DFL-RIT-TFR-DFZ
                  ,CNBT_INPSTFM_CALC      =
                :DFL-CNBT-INPSTFM-CALC
                                       :IND-DFL-CNBT-INPSTFM-CALC
                  ,CNBT_INPSTFM_EFFLQ     =
                :DFL-CNBT-INPSTFM-EFFLQ
                                       :IND-DFL-CNBT-INPSTFM-EFFLQ
                  ,CNBT_INPSTFM_DFZ       =
                :DFL-CNBT-INPSTFM-DFZ
                                       :IND-DFL-CNBT-INPSTFM-DFZ
                  ,ICNB_INPSTFM_CALC      =
                :DFL-ICNB-INPSTFM-CALC
                                       :IND-DFL-ICNB-INPSTFM-CALC
                  ,ICNB_INPSTFM_EFFLQ     =
                :DFL-ICNB-INPSTFM-EFFLQ
                                       :IND-DFL-ICNB-INPSTFM-EFFLQ
                  ,ICNB_INPSTFM_DFZ       =
                :DFL-ICNB-INPSTFM-DFZ
                                       :IND-DFL-ICNB-INPSTFM-DFZ
                  ,CNDE_END2000_CALC      =
                :DFL-CNDE-END2000-CALC
                                       :IND-DFL-CNDE-END2000-CALC
                  ,CNDE_END2000_EFFLQ     =
                :DFL-CNDE-END2000-EFFLQ
                                       :IND-DFL-CNDE-END2000-EFFLQ
                  ,CNDE_END2000_DFZ       =
                :DFL-CNDE-END2000-DFZ
                                       :IND-DFL-CNDE-END2000-DFZ
                  ,CNDE_END2006_CALC      =
                :DFL-CNDE-END2006-CALC
                                       :IND-DFL-CNDE-END2006-CALC
                  ,CNDE_END2006_EFFLQ     =
                :DFL-CNDE-END2006-EFFLQ
                                       :IND-DFL-CNDE-END2006-EFFLQ
                  ,CNDE_END2006_DFZ       =
                :DFL-CNDE-END2006-DFZ
                                       :IND-DFL-CNDE-END2006-DFZ
                  ,CNDE_DAL2007_CALC      =
                :DFL-CNDE-DAL2007-CALC
                                       :IND-DFL-CNDE-DAL2007-CALC
                  ,CNDE_DAL2007_EFFLQ     =
                :DFL-CNDE-DAL2007-EFFLQ
                                       :IND-DFL-CNDE-DAL2007-EFFLQ
                  ,CNDE_DAL2007_DFZ       =
                :DFL-CNDE-DAL2007-DFZ
                                       :IND-DFL-CNDE-DAL2007-DFZ
                  ,AA_CNBZ_END2000_EF     =
                :DFL-AA-CNBZ-END2000-EF
                                       :IND-DFL-AA-CNBZ-END2000-EF
                  ,AA_CNBZ_END2006_EF     =
                :DFL-AA-CNBZ-END2006-EF
                                       :IND-DFL-AA-CNBZ-END2006-EF
                  ,AA_CNBZ_DAL2007_EF     =
                :DFL-AA-CNBZ-DAL2007-EF
                                       :IND-DFL-AA-CNBZ-DAL2007-EF
                  ,MM_CNBZ_END2000_EF     =
                :DFL-MM-CNBZ-END2000-EF
                                       :IND-DFL-MM-CNBZ-END2000-EF
                  ,MM_CNBZ_END2006_EF     =
                :DFL-MM-CNBZ-END2006-EF
                                       :IND-DFL-MM-CNBZ-END2006-EF
                  ,MM_CNBZ_DAL2007_EF     =
                :DFL-MM-CNBZ-DAL2007-EF
                                       :IND-DFL-MM-CNBZ-DAL2007-EF
                  ,IMPST_DA_RIMB_EFF      =
                :DFL-IMPST-DA-RIMB-EFF
                                       :IND-DFL-IMPST-DA-RIMB-EFF
                  ,ALQ_TAX_SEP_CALC       =
                :DFL-ALQ-TAX-SEP-CALC
                                       :IND-DFL-ALQ-TAX-SEP-CALC
                  ,ALQ_TAX_SEP_EFFLQ      =
                :DFL-ALQ-TAX-SEP-EFFLQ
                                       :IND-DFL-ALQ-TAX-SEP-EFFLQ
                  ,ALQ_TAX_SEP_DFZ        =
                :DFL-ALQ-TAX-SEP-DFZ
                                       :IND-DFL-ALQ-TAX-SEP-DFZ
                  ,ALQ_CNBT_INPSTFM_C     =
                :DFL-ALQ-CNBT-INPSTFM-C
                                       :IND-DFL-ALQ-CNBT-INPSTFM-C
                  ,ALQ_CNBT_INPSTFM_E     =
                :DFL-ALQ-CNBT-INPSTFM-E
                                       :IND-DFL-ALQ-CNBT-INPSTFM-E
                  ,ALQ_CNBT_INPSTFM_D     =
                :DFL-ALQ-CNBT-INPSTFM-D
                                       :IND-DFL-ALQ-CNBT-INPSTFM-D
                  ,DS_RIGA                =
                :DFL-DS-RIGA
                  ,DS_OPER_SQL            =
                :DFL-DS-OPER-SQL
                  ,DS_VER                 =
                :DFL-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :DFL-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :DFL-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :DFL-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :DFL-DS-STATO-ELAB
                  ,IMPB_VIS_1382011C      =
                :DFL-IMPB-VIS-1382011C
                                       :IND-DFL-IMPB-VIS-1382011C
                  ,IMPB_VIS_1382011D      =
                :DFL-IMPB-VIS-1382011D
                                       :IND-DFL-IMPB-VIS-1382011D
                  ,IMPB_VIS_1382011L      =
                :DFL-IMPB-VIS-1382011L
                                       :IND-DFL-IMPB-VIS-1382011L
                  ,IMPST_VIS_1382011C     =
                :DFL-IMPST-VIS-1382011C
                                       :IND-DFL-IMPST-VIS-1382011C
                  ,IMPST_VIS_1382011D     =
                :DFL-IMPST-VIS-1382011D
                                       :IND-DFL-IMPST-VIS-1382011D
                  ,IMPST_VIS_1382011L     =
                :DFL-IMPST-VIS-1382011L
                                       :IND-DFL-IMPST-VIS-1382011L
                  ,IMPB_IS_1382011C       =
                :DFL-IMPB-IS-1382011C
                                       :IND-DFL-IMPB-IS-1382011C
                  ,IMPB_IS_1382011D       =
                :DFL-IMPB-IS-1382011D
                                       :IND-DFL-IMPB-IS-1382011D
                  ,IMPB_IS_1382011L       =
                :DFL-IMPB-IS-1382011L
                                       :IND-DFL-IMPB-IS-1382011L
                  ,IS_1382011C            =
                :DFL-IS-1382011C
                                       :IND-DFL-IS-1382011C
                  ,IS_1382011D            =
                :DFL-IS-1382011D
                                       :IND-DFL-IS-1382011D
                  ,IS_1382011L            =
                :DFL-IS-1382011L
                                       :IND-DFL-IS-1382011L
                  ,IMP_INTR_RIT_PAG_C     =
                :DFL-IMP-INTR-RIT-PAG-C
                                       :IND-DFL-IMP-INTR-RIT-PAG-C
                  ,IMP_INTR_RIT_PAG_D     =
                :DFL-IMP-INTR-RIT-PAG-D
                                       :IND-DFL-IMP-INTR-RIT-PAG-D
                  ,IMP_INTR_RIT_PAG_L     =
                :DFL-IMP-INTR-RIT-PAG-L
                                       :IND-DFL-IMP-INTR-RIT-PAG-L
                  ,IMPB_BOLLO_DETT_C      =
                :DFL-IMPB-BOLLO-DETT-C
                                       :IND-DFL-IMPB-BOLLO-DETT-C
                  ,IMPB_BOLLO_DETT_D      =
                :DFL-IMPB-BOLLO-DETT-D
                                       :IND-DFL-IMPB-BOLLO-DETT-D
                  ,IMPB_BOLLO_DETT_L      =
                :DFL-IMPB-BOLLO-DETT-L
                                       :IND-DFL-IMPB-BOLLO-DETT-L
                  ,IMPST_BOLLO_DETT_C     =
                :DFL-IMPST-BOLLO-DETT-C
                                       :IND-DFL-IMPST-BOLLO-DETT-C
                  ,IMPST_BOLLO_DETT_D     =
                :DFL-IMPST-BOLLO-DETT-D
                                       :IND-DFL-IMPST-BOLLO-DETT-D
                  ,IMPST_BOLLO_DETT_L     =
                :DFL-IMPST-BOLLO-DETT-L
                                       :IND-DFL-IMPST-BOLLO-DETT-L
                  ,IMPST_BOLLO_TOT_VC     =
                :DFL-IMPST-BOLLO-TOT-VC
                                       :IND-DFL-IMPST-BOLLO-TOT-VC
                  ,IMPST_BOLLO_TOT_VD     =
                :DFL-IMPST-BOLLO-TOT-VD
                                       :IND-DFL-IMPST-BOLLO-TOT-VD
                  ,IMPST_BOLLO_TOT_VL     =
                :DFL-IMPST-BOLLO-TOT-VL
                                       :IND-DFL-IMPST-BOLLO-TOT-VL
                  ,IMPB_VIS_662014C       =
                :DFL-IMPB-VIS-662014C
                                       :IND-DFL-IMPB-VIS-662014C
                  ,IMPB_VIS_662014D       =
                :DFL-IMPB-VIS-662014D
                                       :IND-DFL-IMPB-VIS-662014D
                  ,IMPB_VIS_662014L       =
                :DFL-IMPB-VIS-662014L
                                       :IND-DFL-IMPB-VIS-662014L
                  ,IMPST_VIS_662014C      =
                :DFL-IMPST-VIS-662014C
                                       :IND-DFL-IMPST-VIS-662014C
                  ,IMPST_VIS_662014D      =
                :DFL-IMPST-VIS-662014D
                                       :IND-DFL-IMPST-VIS-662014D
                  ,IMPST_VIS_662014L      =
                :DFL-IMPST-VIS-662014L
                                       :IND-DFL-IMPST-VIS-662014L
                  ,IMPB_IS_662014C        =
                :DFL-IMPB-IS-662014C
                                       :IND-DFL-IMPB-IS-662014C
                  ,IMPB_IS_662014D        =
                :DFL-IMPB-IS-662014D
                                       :IND-DFL-IMPB-IS-662014D
                  ,IMPB_IS_662014L        =
                :DFL-IMPB-IS-662014L
                                       :IND-DFL-IMPB-IS-662014L
                  ,IS_662014C             =
                :DFL-IS-662014C
                                       :IND-DFL-IS-662014C
                  ,IS_662014D             =
                :DFL-IS-662014D
                                       :IND-DFL-IS-662014D
                  ,IS_662014L             =
                :DFL-IS-662014L
                                       :IND-DFL-IS-662014L
                WHERE     DS_RIGA = :DFL-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM D_FORZ_LIQ
                WHERE     DS_RIGA = :DFL-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-DFL CURSOR FOR
              SELECT
                     ID_D_FORZ_LIQ
                    ,ID_LIQ
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,COD_COMP_ANIA
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,IMP_LRD_CALC
                    ,IMP_LRD_DFZ
                    ,IMP_LRD_EFFLQ
                    ,IMP_NET_CALC
                    ,IMP_NET_DFZ
                    ,IMP_NET_EFFLQ
                    ,IMPST_PRVR_CALC
                    ,IMPST_PRVR_DFZ
                    ,IMPST_PRVR_EFFLQ
                    ,IMPST_VIS_CALC
                    ,IMPST_VIS_DFZ
                    ,IMPST_VIS_EFFLQ
                    ,RIT_ACC_CALC
                    ,RIT_ACC_DFZ
                    ,RIT_ACC_EFFLQ
                    ,RIT_IRPEF_CALC
                    ,RIT_IRPEF_DFZ
                    ,RIT_IRPEF_EFFLQ
                    ,IMPST_SOST_CALC
                    ,IMPST_SOST_DFZ
                    ,IMPST_SOST_EFFLQ
                    ,TAX_SEP_CALC
                    ,TAX_SEP_DFZ
                    ,TAX_SEP_EFFLQ
                    ,INTR_PREST_CALC
                    ,INTR_PREST_DFZ
                    ,INTR_PREST_EFFLQ
                    ,ACCPRE_SOST_CALC
                    ,ACCPRE_SOST_DFZ
                    ,ACCPRE_SOST_EFFLQ
                    ,ACCPRE_VIS_CALC
                    ,ACCPRE_VIS_DFZ
                    ,ACCPRE_VIS_EFFLQ
                    ,ACCPRE_ACC_CALC
                    ,ACCPRE_ACC_DFZ
                    ,ACCPRE_ACC_EFFLQ
                    ,RES_PRSTZ_CALC
                    ,RES_PRSTZ_DFZ
                    ,RES_PRSTZ_EFFLQ
                    ,RES_PRE_ATT_CALC
                    ,RES_PRE_ATT_DFZ
                    ,RES_PRE_ATT_EFFLQ
                    ,IMP_EXCONTR_EFF
                    ,IMPB_VIS_CALC
                    ,IMPB_VIS_EFFLQ
                    ,IMPB_VIS_DFZ
                    ,IMPB_RIT_ACC_CALC
                    ,IMPB_RIT_ACC_EFFLQ
                    ,IMPB_RIT_ACC_DFZ
                    ,IMPB_TFR_CALC
                    ,IMPB_TFR_EFFLQ
                    ,IMPB_TFR_DFZ
                    ,IMPB_IS_CALC
                    ,IMPB_IS_EFFLQ
                    ,IMPB_IS_DFZ
                    ,IMPB_TAX_SEP_CALC
                    ,IMPB_TAX_SEP_EFFLQ
                    ,IMPB_TAX_SEP_DFZ
                    ,IINT_PREST_CALC
                    ,IINT_PREST_EFFLQ
                    ,IINT_PREST_DFZ
                    ,MONT_END2000_CALC
                    ,MONT_END2000_EFFLQ
                    ,MONT_END2000_DFZ
                    ,MONT_END2006_CALC
                    ,MONT_END2006_EFFLQ
                    ,MONT_END2006_DFZ
                    ,MONT_DAL2007_CALC
                    ,MONT_DAL2007_EFFLQ
                    ,MONT_DAL2007_DFZ
                    ,IIMPST_PRVR_CALC
                    ,IIMPST_PRVR_EFFLQ
                    ,IIMPST_PRVR_DFZ
                    ,IIMPST_252_CALC
                    ,IIMPST_252_EFFLQ
                    ,IIMPST_252_DFZ
                    ,IMPST_252_CALC
                    ,IMPST_252_EFFLQ
                    ,RIT_TFR_CALC
                    ,RIT_TFR_EFFLQ
                    ,RIT_TFR_DFZ
                    ,CNBT_INPSTFM_CALC
                    ,CNBT_INPSTFM_EFFLQ
                    ,CNBT_INPSTFM_DFZ
                    ,ICNB_INPSTFM_CALC
                    ,ICNB_INPSTFM_EFFLQ
                    ,ICNB_INPSTFM_DFZ
                    ,CNDE_END2000_CALC
                    ,CNDE_END2000_EFFLQ
                    ,CNDE_END2000_DFZ
                    ,CNDE_END2006_CALC
                    ,CNDE_END2006_EFFLQ
                    ,CNDE_END2006_DFZ
                    ,CNDE_DAL2007_CALC
                    ,CNDE_DAL2007_EFFLQ
                    ,CNDE_DAL2007_DFZ
                    ,AA_CNBZ_END2000_EF
                    ,AA_CNBZ_END2006_EF
                    ,AA_CNBZ_DAL2007_EF
                    ,MM_CNBZ_END2000_EF
                    ,MM_CNBZ_END2006_EF
                    ,MM_CNBZ_DAL2007_EF
                    ,IMPST_DA_RIMB_EFF
                    ,ALQ_TAX_SEP_CALC
                    ,ALQ_TAX_SEP_EFFLQ
                    ,ALQ_TAX_SEP_DFZ
                    ,ALQ_CNBT_INPSTFM_C
                    ,ALQ_CNBT_INPSTFM_E
                    ,ALQ_CNBT_INPSTFM_D
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,IMPB_VIS_1382011C
                    ,IMPB_VIS_1382011D
                    ,IMPB_VIS_1382011L
                    ,IMPST_VIS_1382011C
                    ,IMPST_VIS_1382011D
                    ,IMPST_VIS_1382011L
                    ,IMPB_IS_1382011C
                    ,IMPB_IS_1382011D
                    ,IMPB_IS_1382011L
                    ,IS_1382011C
                    ,IS_1382011D
                    ,IS_1382011L
                    ,IMP_INTR_RIT_PAG_C
                    ,IMP_INTR_RIT_PAG_D
                    ,IMP_INTR_RIT_PAG_L
                    ,IMPB_BOLLO_DETT_C
                    ,IMPB_BOLLO_DETT_D
                    ,IMPB_BOLLO_DETT_L
                    ,IMPST_BOLLO_DETT_C
                    ,IMPST_BOLLO_DETT_D
                    ,IMPST_BOLLO_DETT_L
                    ,IMPST_BOLLO_TOT_VC
                    ,IMPST_BOLLO_TOT_VD
                    ,IMPST_BOLLO_TOT_VL
                    ,IMPB_VIS_662014C
                    ,IMPB_VIS_662014D
                    ,IMPB_VIS_662014L
                    ,IMPST_VIS_662014C
                    ,IMPST_VIS_662014D
                    ,IMPST_VIS_662014L
                    ,IMPB_IS_662014C
                    ,IMPB_IS_662014D
                    ,IMPB_IS_662014L
                    ,IS_662014C
                    ,IS_662014D
                    ,IS_662014L
              FROM D_FORZ_LIQ
              WHERE     ID_D_FORZ_LIQ = :DFL-ID-D-FORZ-LIQ
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_D_FORZ_LIQ
                ,ID_LIQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,COD_COMP_ANIA
                ,DT_INI_EFF
                ,DT_END_EFF
                ,IMP_LRD_CALC
                ,IMP_LRD_DFZ
                ,IMP_LRD_EFFLQ
                ,IMP_NET_CALC
                ,IMP_NET_DFZ
                ,IMP_NET_EFFLQ
                ,IMPST_PRVR_CALC
                ,IMPST_PRVR_DFZ
                ,IMPST_PRVR_EFFLQ
                ,IMPST_VIS_CALC
                ,IMPST_VIS_DFZ
                ,IMPST_VIS_EFFLQ
                ,RIT_ACC_CALC
                ,RIT_ACC_DFZ
                ,RIT_ACC_EFFLQ
                ,RIT_IRPEF_CALC
                ,RIT_IRPEF_DFZ
                ,RIT_IRPEF_EFFLQ
                ,IMPST_SOST_CALC
                ,IMPST_SOST_DFZ
                ,IMPST_SOST_EFFLQ
                ,TAX_SEP_CALC
                ,TAX_SEP_DFZ
                ,TAX_SEP_EFFLQ
                ,INTR_PREST_CALC
                ,INTR_PREST_DFZ
                ,INTR_PREST_EFFLQ
                ,ACCPRE_SOST_CALC
                ,ACCPRE_SOST_DFZ
                ,ACCPRE_SOST_EFFLQ
                ,ACCPRE_VIS_CALC
                ,ACCPRE_VIS_DFZ
                ,ACCPRE_VIS_EFFLQ
                ,ACCPRE_ACC_CALC
                ,ACCPRE_ACC_DFZ
                ,ACCPRE_ACC_EFFLQ
                ,RES_PRSTZ_CALC
                ,RES_PRSTZ_DFZ
                ,RES_PRSTZ_EFFLQ
                ,RES_PRE_ATT_CALC
                ,RES_PRE_ATT_DFZ
                ,RES_PRE_ATT_EFFLQ
                ,IMP_EXCONTR_EFF
                ,IMPB_VIS_CALC
                ,IMPB_VIS_EFFLQ
                ,IMPB_VIS_DFZ
                ,IMPB_RIT_ACC_CALC
                ,IMPB_RIT_ACC_EFFLQ
                ,IMPB_RIT_ACC_DFZ
                ,IMPB_TFR_CALC
                ,IMPB_TFR_EFFLQ
                ,IMPB_TFR_DFZ
                ,IMPB_IS_CALC
                ,IMPB_IS_EFFLQ
                ,IMPB_IS_DFZ
                ,IMPB_TAX_SEP_CALC
                ,IMPB_TAX_SEP_EFFLQ
                ,IMPB_TAX_SEP_DFZ
                ,IINT_PREST_CALC
                ,IINT_PREST_EFFLQ
                ,IINT_PREST_DFZ
                ,MONT_END2000_CALC
                ,MONT_END2000_EFFLQ
                ,MONT_END2000_DFZ
                ,MONT_END2006_CALC
                ,MONT_END2006_EFFLQ
                ,MONT_END2006_DFZ
                ,MONT_DAL2007_CALC
                ,MONT_DAL2007_EFFLQ
                ,MONT_DAL2007_DFZ
                ,IIMPST_PRVR_CALC
                ,IIMPST_PRVR_EFFLQ
                ,IIMPST_PRVR_DFZ
                ,IIMPST_252_CALC
                ,IIMPST_252_EFFLQ
                ,IIMPST_252_DFZ
                ,IMPST_252_CALC
                ,IMPST_252_EFFLQ
                ,RIT_TFR_CALC
                ,RIT_TFR_EFFLQ
                ,RIT_TFR_DFZ
                ,CNBT_INPSTFM_CALC
                ,CNBT_INPSTFM_EFFLQ
                ,CNBT_INPSTFM_DFZ
                ,ICNB_INPSTFM_CALC
                ,ICNB_INPSTFM_EFFLQ
                ,ICNB_INPSTFM_DFZ
                ,CNDE_END2000_CALC
                ,CNDE_END2000_EFFLQ
                ,CNDE_END2000_DFZ
                ,CNDE_END2006_CALC
                ,CNDE_END2006_EFFLQ
                ,CNDE_END2006_DFZ
                ,CNDE_DAL2007_CALC
                ,CNDE_DAL2007_EFFLQ
                ,CNDE_DAL2007_DFZ
                ,AA_CNBZ_END2000_EF
                ,AA_CNBZ_END2006_EF
                ,AA_CNBZ_DAL2007_EF
                ,MM_CNBZ_END2000_EF
                ,MM_CNBZ_END2006_EF
                ,MM_CNBZ_DAL2007_EF
                ,IMPST_DA_RIMB_EFF
                ,ALQ_TAX_SEP_CALC
                ,ALQ_TAX_SEP_EFFLQ
                ,ALQ_TAX_SEP_DFZ
                ,ALQ_CNBT_INPSTFM_C
                ,ALQ_CNBT_INPSTFM_E
                ,ALQ_CNBT_INPSTFM_D
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,IMPB_VIS_1382011C
                ,IMPB_VIS_1382011D
                ,IMPB_VIS_1382011L
                ,IMPST_VIS_1382011C
                ,IMPST_VIS_1382011D
                ,IMPST_VIS_1382011L
                ,IMPB_IS_1382011C
                ,IMPB_IS_1382011D
                ,IMPB_IS_1382011L
                ,IS_1382011C
                ,IS_1382011D
                ,IS_1382011L
                ,IMP_INTR_RIT_PAG_C
                ,IMP_INTR_RIT_PAG_D
                ,IMP_INTR_RIT_PAG_L
                ,IMPB_BOLLO_DETT_C
                ,IMPB_BOLLO_DETT_D
                ,IMPB_BOLLO_DETT_L
                ,IMPST_BOLLO_DETT_C
                ,IMPST_BOLLO_DETT_D
                ,IMPST_BOLLO_DETT_L
                ,IMPST_BOLLO_TOT_VC
                ,IMPST_BOLLO_TOT_VD
                ,IMPST_BOLLO_TOT_VL
                ,IMPB_VIS_662014C
                ,IMPB_VIS_662014D
                ,IMPB_VIS_662014L
                ,IMPST_VIS_662014C
                ,IMPST_VIS_662014D
                ,IMPST_VIS_662014L
                ,IMPB_IS_662014C
                ,IMPB_IS_662014D
                ,IMPB_IS_662014L
                ,IS_662014C
                ,IS_662014D
                ,IS_662014L
             INTO
                :DFL-ID-D-FORZ-LIQ
               ,:DFL-ID-LIQ
               ,:DFL-ID-MOVI-CRZ
               ,:DFL-ID-MOVI-CHIU
                :IND-DFL-ID-MOVI-CHIU
               ,:DFL-COD-COMP-ANIA
               ,:DFL-DT-INI-EFF-DB
               ,:DFL-DT-END-EFF-DB
               ,:DFL-IMP-LRD-CALC
                :IND-DFL-IMP-LRD-CALC
               ,:DFL-IMP-LRD-DFZ
                :IND-DFL-IMP-LRD-DFZ
               ,:DFL-IMP-LRD-EFFLQ
                :IND-DFL-IMP-LRD-EFFLQ
               ,:DFL-IMP-NET-CALC
                :IND-DFL-IMP-NET-CALC
               ,:DFL-IMP-NET-DFZ
                :IND-DFL-IMP-NET-DFZ
               ,:DFL-IMP-NET-EFFLQ
                :IND-DFL-IMP-NET-EFFLQ
               ,:DFL-IMPST-PRVR-CALC
                :IND-DFL-IMPST-PRVR-CALC
               ,:DFL-IMPST-PRVR-DFZ
                :IND-DFL-IMPST-PRVR-DFZ
               ,:DFL-IMPST-PRVR-EFFLQ
                :IND-DFL-IMPST-PRVR-EFFLQ
               ,:DFL-IMPST-VIS-CALC
                :IND-DFL-IMPST-VIS-CALC
               ,:DFL-IMPST-VIS-DFZ
                :IND-DFL-IMPST-VIS-DFZ
               ,:DFL-IMPST-VIS-EFFLQ
                :IND-DFL-IMPST-VIS-EFFLQ
               ,:DFL-RIT-ACC-CALC
                :IND-DFL-RIT-ACC-CALC
               ,:DFL-RIT-ACC-DFZ
                :IND-DFL-RIT-ACC-DFZ
               ,:DFL-RIT-ACC-EFFLQ
                :IND-DFL-RIT-ACC-EFFLQ
               ,:DFL-RIT-IRPEF-CALC
                :IND-DFL-RIT-IRPEF-CALC
               ,:DFL-RIT-IRPEF-DFZ
                :IND-DFL-RIT-IRPEF-DFZ
               ,:DFL-RIT-IRPEF-EFFLQ
                :IND-DFL-RIT-IRPEF-EFFLQ
               ,:DFL-IMPST-SOST-CALC
                :IND-DFL-IMPST-SOST-CALC
               ,:DFL-IMPST-SOST-DFZ
                :IND-DFL-IMPST-SOST-DFZ
               ,:DFL-IMPST-SOST-EFFLQ
                :IND-DFL-IMPST-SOST-EFFLQ
               ,:DFL-TAX-SEP-CALC
                :IND-DFL-TAX-SEP-CALC
               ,:DFL-TAX-SEP-DFZ
                :IND-DFL-TAX-SEP-DFZ
               ,:DFL-TAX-SEP-EFFLQ
                :IND-DFL-TAX-SEP-EFFLQ
               ,:DFL-INTR-PREST-CALC
                :IND-DFL-INTR-PREST-CALC
               ,:DFL-INTR-PREST-DFZ
                :IND-DFL-INTR-PREST-DFZ
               ,:DFL-INTR-PREST-EFFLQ
                :IND-DFL-INTR-PREST-EFFLQ
               ,:DFL-ACCPRE-SOST-CALC
                :IND-DFL-ACCPRE-SOST-CALC
               ,:DFL-ACCPRE-SOST-DFZ
                :IND-DFL-ACCPRE-SOST-DFZ
               ,:DFL-ACCPRE-SOST-EFFLQ
                :IND-DFL-ACCPRE-SOST-EFFLQ
               ,:DFL-ACCPRE-VIS-CALC
                :IND-DFL-ACCPRE-VIS-CALC
               ,:DFL-ACCPRE-VIS-DFZ
                :IND-DFL-ACCPRE-VIS-DFZ
               ,:DFL-ACCPRE-VIS-EFFLQ
                :IND-DFL-ACCPRE-VIS-EFFLQ
               ,:DFL-ACCPRE-ACC-CALC
                :IND-DFL-ACCPRE-ACC-CALC
               ,:DFL-ACCPRE-ACC-DFZ
                :IND-DFL-ACCPRE-ACC-DFZ
               ,:DFL-ACCPRE-ACC-EFFLQ
                :IND-DFL-ACCPRE-ACC-EFFLQ
               ,:DFL-RES-PRSTZ-CALC
                :IND-DFL-RES-PRSTZ-CALC
               ,:DFL-RES-PRSTZ-DFZ
                :IND-DFL-RES-PRSTZ-DFZ
               ,:DFL-RES-PRSTZ-EFFLQ
                :IND-DFL-RES-PRSTZ-EFFLQ
               ,:DFL-RES-PRE-ATT-CALC
                :IND-DFL-RES-PRE-ATT-CALC
               ,:DFL-RES-PRE-ATT-DFZ
                :IND-DFL-RES-PRE-ATT-DFZ
               ,:DFL-RES-PRE-ATT-EFFLQ
                :IND-DFL-RES-PRE-ATT-EFFLQ
               ,:DFL-IMP-EXCONTR-EFF
                :IND-DFL-IMP-EXCONTR-EFF
               ,:DFL-IMPB-VIS-CALC
                :IND-DFL-IMPB-VIS-CALC
               ,:DFL-IMPB-VIS-EFFLQ
                :IND-DFL-IMPB-VIS-EFFLQ
               ,:DFL-IMPB-VIS-DFZ
                :IND-DFL-IMPB-VIS-DFZ
               ,:DFL-IMPB-RIT-ACC-CALC
                :IND-DFL-IMPB-RIT-ACC-CALC
               ,:DFL-IMPB-RIT-ACC-EFFLQ
                :IND-DFL-IMPB-RIT-ACC-EFFLQ
               ,:DFL-IMPB-RIT-ACC-DFZ
                :IND-DFL-IMPB-RIT-ACC-DFZ
               ,:DFL-IMPB-TFR-CALC
                :IND-DFL-IMPB-TFR-CALC
               ,:DFL-IMPB-TFR-EFFLQ
                :IND-DFL-IMPB-TFR-EFFLQ
               ,:DFL-IMPB-TFR-DFZ
                :IND-DFL-IMPB-TFR-DFZ
               ,:DFL-IMPB-IS-CALC
                :IND-DFL-IMPB-IS-CALC
               ,:DFL-IMPB-IS-EFFLQ
                :IND-DFL-IMPB-IS-EFFLQ
               ,:DFL-IMPB-IS-DFZ
                :IND-DFL-IMPB-IS-DFZ
               ,:DFL-IMPB-TAX-SEP-CALC
                :IND-DFL-IMPB-TAX-SEP-CALC
               ,:DFL-IMPB-TAX-SEP-EFFLQ
                :IND-DFL-IMPB-TAX-SEP-EFFLQ
               ,:DFL-IMPB-TAX-SEP-DFZ
                :IND-DFL-IMPB-TAX-SEP-DFZ
               ,:DFL-IINT-PREST-CALC
                :IND-DFL-IINT-PREST-CALC
               ,:DFL-IINT-PREST-EFFLQ
                :IND-DFL-IINT-PREST-EFFLQ
               ,:DFL-IINT-PREST-DFZ
                :IND-DFL-IINT-PREST-DFZ
               ,:DFL-MONT-END2000-CALC
                :IND-DFL-MONT-END2000-CALC
               ,:DFL-MONT-END2000-EFFLQ
                :IND-DFL-MONT-END2000-EFFLQ
               ,:DFL-MONT-END2000-DFZ
                :IND-DFL-MONT-END2000-DFZ
               ,:DFL-MONT-END2006-CALC
                :IND-DFL-MONT-END2006-CALC
               ,:DFL-MONT-END2006-EFFLQ
                :IND-DFL-MONT-END2006-EFFLQ
               ,:DFL-MONT-END2006-DFZ
                :IND-DFL-MONT-END2006-DFZ
               ,:DFL-MONT-DAL2007-CALC
                :IND-DFL-MONT-DAL2007-CALC
               ,:DFL-MONT-DAL2007-EFFLQ
                :IND-DFL-MONT-DAL2007-EFFLQ
               ,:DFL-MONT-DAL2007-DFZ
                :IND-DFL-MONT-DAL2007-DFZ
               ,:DFL-IIMPST-PRVR-CALC
                :IND-DFL-IIMPST-PRVR-CALC
               ,:DFL-IIMPST-PRVR-EFFLQ
                :IND-DFL-IIMPST-PRVR-EFFLQ
               ,:DFL-IIMPST-PRVR-DFZ
                :IND-DFL-IIMPST-PRVR-DFZ
               ,:DFL-IIMPST-252-CALC
                :IND-DFL-IIMPST-252-CALC
               ,:DFL-IIMPST-252-EFFLQ
                :IND-DFL-IIMPST-252-EFFLQ
               ,:DFL-IIMPST-252-DFZ
                :IND-DFL-IIMPST-252-DFZ
               ,:DFL-IMPST-252-CALC
                :IND-DFL-IMPST-252-CALC
               ,:DFL-IMPST-252-EFFLQ
                :IND-DFL-IMPST-252-EFFLQ
               ,:DFL-RIT-TFR-CALC
                :IND-DFL-RIT-TFR-CALC
               ,:DFL-RIT-TFR-EFFLQ
                :IND-DFL-RIT-TFR-EFFLQ
               ,:DFL-RIT-TFR-DFZ
                :IND-DFL-RIT-TFR-DFZ
               ,:DFL-CNBT-INPSTFM-CALC
                :IND-DFL-CNBT-INPSTFM-CALC
               ,:DFL-CNBT-INPSTFM-EFFLQ
                :IND-DFL-CNBT-INPSTFM-EFFLQ
               ,:DFL-CNBT-INPSTFM-DFZ
                :IND-DFL-CNBT-INPSTFM-DFZ
               ,:DFL-ICNB-INPSTFM-CALC
                :IND-DFL-ICNB-INPSTFM-CALC
               ,:DFL-ICNB-INPSTFM-EFFLQ
                :IND-DFL-ICNB-INPSTFM-EFFLQ
               ,:DFL-ICNB-INPSTFM-DFZ
                :IND-DFL-ICNB-INPSTFM-DFZ
               ,:DFL-CNDE-END2000-CALC
                :IND-DFL-CNDE-END2000-CALC
               ,:DFL-CNDE-END2000-EFFLQ
                :IND-DFL-CNDE-END2000-EFFLQ
               ,:DFL-CNDE-END2000-DFZ
                :IND-DFL-CNDE-END2000-DFZ
               ,:DFL-CNDE-END2006-CALC
                :IND-DFL-CNDE-END2006-CALC
               ,:DFL-CNDE-END2006-EFFLQ
                :IND-DFL-CNDE-END2006-EFFLQ
               ,:DFL-CNDE-END2006-DFZ
                :IND-DFL-CNDE-END2006-DFZ
               ,:DFL-CNDE-DAL2007-CALC
                :IND-DFL-CNDE-DAL2007-CALC
               ,:DFL-CNDE-DAL2007-EFFLQ
                :IND-DFL-CNDE-DAL2007-EFFLQ
               ,:DFL-CNDE-DAL2007-DFZ
                :IND-DFL-CNDE-DAL2007-DFZ
               ,:DFL-AA-CNBZ-END2000-EF
                :IND-DFL-AA-CNBZ-END2000-EF
               ,:DFL-AA-CNBZ-END2006-EF
                :IND-DFL-AA-CNBZ-END2006-EF
               ,:DFL-AA-CNBZ-DAL2007-EF
                :IND-DFL-AA-CNBZ-DAL2007-EF
               ,:DFL-MM-CNBZ-END2000-EF
                :IND-DFL-MM-CNBZ-END2000-EF
               ,:DFL-MM-CNBZ-END2006-EF
                :IND-DFL-MM-CNBZ-END2006-EF
               ,:DFL-MM-CNBZ-DAL2007-EF
                :IND-DFL-MM-CNBZ-DAL2007-EF
               ,:DFL-IMPST-DA-RIMB-EFF
                :IND-DFL-IMPST-DA-RIMB-EFF
               ,:DFL-ALQ-TAX-SEP-CALC
                :IND-DFL-ALQ-TAX-SEP-CALC
               ,:DFL-ALQ-TAX-SEP-EFFLQ
                :IND-DFL-ALQ-TAX-SEP-EFFLQ
               ,:DFL-ALQ-TAX-SEP-DFZ
                :IND-DFL-ALQ-TAX-SEP-DFZ
               ,:DFL-ALQ-CNBT-INPSTFM-C
                :IND-DFL-ALQ-CNBT-INPSTFM-C
               ,:DFL-ALQ-CNBT-INPSTFM-E
                :IND-DFL-ALQ-CNBT-INPSTFM-E
               ,:DFL-ALQ-CNBT-INPSTFM-D
                :IND-DFL-ALQ-CNBT-INPSTFM-D
               ,:DFL-DS-RIGA
               ,:DFL-DS-OPER-SQL
               ,:DFL-DS-VER
               ,:DFL-DS-TS-INI-CPTZ
               ,:DFL-DS-TS-END-CPTZ
               ,:DFL-DS-UTENTE
               ,:DFL-DS-STATO-ELAB
               ,:DFL-IMPB-VIS-1382011C
                :IND-DFL-IMPB-VIS-1382011C
               ,:DFL-IMPB-VIS-1382011D
                :IND-DFL-IMPB-VIS-1382011D
               ,:DFL-IMPB-VIS-1382011L
                :IND-DFL-IMPB-VIS-1382011L
               ,:DFL-IMPST-VIS-1382011C
                :IND-DFL-IMPST-VIS-1382011C
               ,:DFL-IMPST-VIS-1382011D
                :IND-DFL-IMPST-VIS-1382011D
               ,:DFL-IMPST-VIS-1382011L
                :IND-DFL-IMPST-VIS-1382011L
               ,:DFL-IMPB-IS-1382011C
                :IND-DFL-IMPB-IS-1382011C
               ,:DFL-IMPB-IS-1382011D
                :IND-DFL-IMPB-IS-1382011D
               ,:DFL-IMPB-IS-1382011L
                :IND-DFL-IMPB-IS-1382011L
               ,:DFL-IS-1382011C
                :IND-DFL-IS-1382011C
               ,:DFL-IS-1382011D
                :IND-DFL-IS-1382011D
               ,:DFL-IS-1382011L
                :IND-DFL-IS-1382011L
               ,:DFL-IMP-INTR-RIT-PAG-C
                :IND-DFL-IMP-INTR-RIT-PAG-C
               ,:DFL-IMP-INTR-RIT-PAG-D
                :IND-DFL-IMP-INTR-RIT-PAG-D
               ,:DFL-IMP-INTR-RIT-PAG-L
                :IND-DFL-IMP-INTR-RIT-PAG-L
               ,:DFL-IMPB-BOLLO-DETT-C
                :IND-DFL-IMPB-BOLLO-DETT-C
               ,:DFL-IMPB-BOLLO-DETT-D
                :IND-DFL-IMPB-BOLLO-DETT-D
               ,:DFL-IMPB-BOLLO-DETT-L
                :IND-DFL-IMPB-BOLLO-DETT-L
               ,:DFL-IMPST-BOLLO-DETT-C
                :IND-DFL-IMPST-BOLLO-DETT-C
               ,:DFL-IMPST-BOLLO-DETT-D
                :IND-DFL-IMPST-BOLLO-DETT-D
               ,:DFL-IMPST-BOLLO-DETT-L
                :IND-DFL-IMPST-BOLLO-DETT-L
               ,:DFL-IMPST-BOLLO-TOT-VC
                :IND-DFL-IMPST-BOLLO-TOT-VC
               ,:DFL-IMPST-BOLLO-TOT-VD
                :IND-DFL-IMPST-BOLLO-TOT-VD
               ,:DFL-IMPST-BOLLO-TOT-VL
                :IND-DFL-IMPST-BOLLO-TOT-VL
               ,:DFL-IMPB-VIS-662014C
                :IND-DFL-IMPB-VIS-662014C
               ,:DFL-IMPB-VIS-662014D
                :IND-DFL-IMPB-VIS-662014D
               ,:DFL-IMPB-VIS-662014L
                :IND-DFL-IMPB-VIS-662014L
               ,:DFL-IMPST-VIS-662014C
                :IND-DFL-IMPST-VIS-662014C
               ,:DFL-IMPST-VIS-662014D
                :IND-DFL-IMPST-VIS-662014D
               ,:DFL-IMPST-VIS-662014L
                :IND-DFL-IMPST-VIS-662014L
               ,:DFL-IMPB-IS-662014C
                :IND-DFL-IMPB-IS-662014C
               ,:DFL-IMPB-IS-662014D
                :IND-DFL-IMPB-IS-662014D
               ,:DFL-IMPB-IS-662014L
                :IND-DFL-IMPB-IS-662014L
               ,:DFL-IS-662014C
                :IND-DFL-IS-662014C
               ,:DFL-IS-662014D
                :IND-DFL-IS-662014D
               ,:DFL-IS-662014L
                :IND-DFL-IS-662014L
             FROM D_FORZ_LIQ
             WHERE     ID_D_FORZ_LIQ = :DFL-ID-D-FORZ-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE D_FORZ_LIQ SET

                   ID_D_FORZ_LIQ          =
                :DFL-ID-D-FORZ-LIQ
                  ,ID_LIQ                 =
                :DFL-ID-LIQ
                  ,ID_MOVI_CRZ            =
                :DFL-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :DFL-ID-MOVI-CHIU
                                       :IND-DFL-ID-MOVI-CHIU
                  ,COD_COMP_ANIA          =
                :DFL-COD-COMP-ANIA
                  ,DT_INI_EFF             =
           :DFL-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :DFL-DT-END-EFF-DB
                  ,IMP_LRD_CALC           =
                :DFL-IMP-LRD-CALC
                                       :IND-DFL-IMP-LRD-CALC
                  ,IMP_LRD_DFZ            =
                :DFL-IMP-LRD-DFZ
                                       :IND-DFL-IMP-LRD-DFZ
                  ,IMP_LRD_EFFLQ          =
                :DFL-IMP-LRD-EFFLQ
                                       :IND-DFL-IMP-LRD-EFFLQ
                  ,IMP_NET_CALC           =
                :DFL-IMP-NET-CALC
                                       :IND-DFL-IMP-NET-CALC
                  ,IMP_NET_DFZ            =
                :DFL-IMP-NET-DFZ
                                       :IND-DFL-IMP-NET-DFZ
                  ,IMP_NET_EFFLQ          =
                :DFL-IMP-NET-EFFLQ
                                       :IND-DFL-IMP-NET-EFFLQ
                  ,IMPST_PRVR_CALC        =
                :DFL-IMPST-PRVR-CALC
                                       :IND-DFL-IMPST-PRVR-CALC
                  ,IMPST_PRVR_DFZ         =
                :DFL-IMPST-PRVR-DFZ
                                       :IND-DFL-IMPST-PRVR-DFZ
                  ,IMPST_PRVR_EFFLQ       =
                :DFL-IMPST-PRVR-EFFLQ
                                       :IND-DFL-IMPST-PRVR-EFFLQ
                  ,IMPST_VIS_CALC         =
                :DFL-IMPST-VIS-CALC
                                       :IND-DFL-IMPST-VIS-CALC
                  ,IMPST_VIS_DFZ          =
                :DFL-IMPST-VIS-DFZ
                                       :IND-DFL-IMPST-VIS-DFZ
                  ,IMPST_VIS_EFFLQ        =
                :DFL-IMPST-VIS-EFFLQ
                                       :IND-DFL-IMPST-VIS-EFFLQ
                  ,RIT_ACC_CALC           =
                :DFL-RIT-ACC-CALC
                                       :IND-DFL-RIT-ACC-CALC
                  ,RIT_ACC_DFZ            =
                :DFL-RIT-ACC-DFZ
                                       :IND-DFL-RIT-ACC-DFZ
                  ,RIT_ACC_EFFLQ          =
                :DFL-RIT-ACC-EFFLQ
                                       :IND-DFL-RIT-ACC-EFFLQ
                  ,RIT_IRPEF_CALC         =
                :DFL-RIT-IRPEF-CALC
                                       :IND-DFL-RIT-IRPEF-CALC
                  ,RIT_IRPEF_DFZ          =
                :DFL-RIT-IRPEF-DFZ
                                       :IND-DFL-RIT-IRPEF-DFZ
                  ,RIT_IRPEF_EFFLQ        =
                :DFL-RIT-IRPEF-EFFLQ
                                       :IND-DFL-RIT-IRPEF-EFFLQ
                  ,IMPST_SOST_CALC        =
                :DFL-IMPST-SOST-CALC
                                       :IND-DFL-IMPST-SOST-CALC
                  ,IMPST_SOST_DFZ         =
                :DFL-IMPST-SOST-DFZ
                                       :IND-DFL-IMPST-SOST-DFZ
                  ,IMPST_SOST_EFFLQ       =
                :DFL-IMPST-SOST-EFFLQ
                                       :IND-DFL-IMPST-SOST-EFFLQ
                  ,TAX_SEP_CALC           =
                :DFL-TAX-SEP-CALC
                                       :IND-DFL-TAX-SEP-CALC
                  ,TAX_SEP_DFZ            =
                :DFL-TAX-SEP-DFZ
                                       :IND-DFL-TAX-SEP-DFZ
                  ,TAX_SEP_EFFLQ          =
                :DFL-TAX-SEP-EFFLQ
                                       :IND-DFL-TAX-SEP-EFFLQ
                  ,INTR_PREST_CALC        =
                :DFL-INTR-PREST-CALC
                                       :IND-DFL-INTR-PREST-CALC
                  ,INTR_PREST_DFZ         =
                :DFL-INTR-PREST-DFZ
                                       :IND-DFL-INTR-PREST-DFZ
                  ,INTR_PREST_EFFLQ       =
                :DFL-INTR-PREST-EFFLQ
                                       :IND-DFL-INTR-PREST-EFFLQ
                  ,ACCPRE_SOST_CALC       =
                :DFL-ACCPRE-SOST-CALC
                                       :IND-DFL-ACCPRE-SOST-CALC
                  ,ACCPRE_SOST_DFZ        =
                :DFL-ACCPRE-SOST-DFZ
                                       :IND-DFL-ACCPRE-SOST-DFZ
                  ,ACCPRE_SOST_EFFLQ      =
                :DFL-ACCPRE-SOST-EFFLQ
                                       :IND-DFL-ACCPRE-SOST-EFFLQ
                  ,ACCPRE_VIS_CALC        =
                :DFL-ACCPRE-VIS-CALC
                                       :IND-DFL-ACCPRE-VIS-CALC
                  ,ACCPRE_VIS_DFZ         =
                :DFL-ACCPRE-VIS-DFZ
                                       :IND-DFL-ACCPRE-VIS-DFZ
                  ,ACCPRE_VIS_EFFLQ       =
                :DFL-ACCPRE-VIS-EFFLQ
                                       :IND-DFL-ACCPRE-VIS-EFFLQ
                  ,ACCPRE_ACC_CALC        =
                :DFL-ACCPRE-ACC-CALC
                                       :IND-DFL-ACCPRE-ACC-CALC
                  ,ACCPRE_ACC_DFZ         =
                :DFL-ACCPRE-ACC-DFZ
                                       :IND-DFL-ACCPRE-ACC-DFZ
                  ,ACCPRE_ACC_EFFLQ       =
                :DFL-ACCPRE-ACC-EFFLQ
                                       :IND-DFL-ACCPRE-ACC-EFFLQ
                  ,RES_PRSTZ_CALC         =
                :DFL-RES-PRSTZ-CALC
                                       :IND-DFL-RES-PRSTZ-CALC
                  ,RES_PRSTZ_DFZ          =
                :DFL-RES-PRSTZ-DFZ
                                       :IND-DFL-RES-PRSTZ-DFZ
                  ,RES_PRSTZ_EFFLQ        =
                :DFL-RES-PRSTZ-EFFLQ
                                       :IND-DFL-RES-PRSTZ-EFFLQ
                  ,RES_PRE_ATT_CALC       =
                :DFL-RES-PRE-ATT-CALC
                                       :IND-DFL-RES-PRE-ATT-CALC
                  ,RES_PRE_ATT_DFZ        =
                :DFL-RES-PRE-ATT-DFZ
                                       :IND-DFL-RES-PRE-ATT-DFZ
                  ,RES_PRE_ATT_EFFLQ      =
                :DFL-RES-PRE-ATT-EFFLQ
                                       :IND-DFL-RES-PRE-ATT-EFFLQ
                  ,IMP_EXCONTR_EFF        =
                :DFL-IMP-EXCONTR-EFF
                                       :IND-DFL-IMP-EXCONTR-EFF
                  ,IMPB_VIS_CALC          =
                :DFL-IMPB-VIS-CALC
                                       :IND-DFL-IMPB-VIS-CALC
                  ,IMPB_VIS_EFFLQ         =
                :DFL-IMPB-VIS-EFFLQ
                                       :IND-DFL-IMPB-VIS-EFFLQ
                  ,IMPB_VIS_DFZ           =
                :DFL-IMPB-VIS-DFZ
                                       :IND-DFL-IMPB-VIS-DFZ
                  ,IMPB_RIT_ACC_CALC      =
                :DFL-IMPB-RIT-ACC-CALC
                                       :IND-DFL-IMPB-RIT-ACC-CALC
                  ,IMPB_RIT_ACC_EFFLQ     =
                :DFL-IMPB-RIT-ACC-EFFLQ
                                       :IND-DFL-IMPB-RIT-ACC-EFFLQ
                  ,IMPB_RIT_ACC_DFZ       =
                :DFL-IMPB-RIT-ACC-DFZ
                                       :IND-DFL-IMPB-RIT-ACC-DFZ
                  ,IMPB_TFR_CALC          =
                :DFL-IMPB-TFR-CALC
                                       :IND-DFL-IMPB-TFR-CALC
                  ,IMPB_TFR_EFFLQ         =
                :DFL-IMPB-TFR-EFFLQ
                                       :IND-DFL-IMPB-TFR-EFFLQ
                  ,IMPB_TFR_DFZ           =
                :DFL-IMPB-TFR-DFZ
                                       :IND-DFL-IMPB-TFR-DFZ
                  ,IMPB_IS_CALC           =
                :DFL-IMPB-IS-CALC
                                       :IND-DFL-IMPB-IS-CALC
                  ,IMPB_IS_EFFLQ          =
                :DFL-IMPB-IS-EFFLQ
                                       :IND-DFL-IMPB-IS-EFFLQ
                  ,IMPB_IS_DFZ            =
                :DFL-IMPB-IS-DFZ
                                       :IND-DFL-IMPB-IS-DFZ
                  ,IMPB_TAX_SEP_CALC      =
                :DFL-IMPB-TAX-SEP-CALC
                                       :IND-DFL-IMPB-TAX-SEP-CALC
                  ,IMPB_TAX_SEP_EFFLQ     =
                :DFL-IMPB-TAX-SEP-EFFLQ
                                       :IND-DFL-IMPB-TAX-SEP-EFFLQ
                  ,IMPB_TAX_SEP_DFZ       =
                :DFL-IMPB-TAX-SEP-DFZ
                                       :IND-DFL-IMPB-TAX-SEP-DFZ
                  ,IINT_PREST_CALC        =
                :DFL-IINT-PREST-CALC
                                       :IND-DFL-IINT-PREST-CALC
                  ,IINT_PREST_EFFLQ       =
                :DFL-IINT-PREST-EFFLQ
                                       :IND-DFL-IINT-PREST-EFFLQ
                  ,IINT_PREST_DFZ         =
                :DFL-IINT-PREST-DFZ
                                       :IND-DFL-IINT-PREST-DFZ
                  ,MONT_END2000_CALC      =
                :DFL-MONT-END2000-CALC
                                       :IND-DFL-MONT-END2000-CALC
                  ,MONT_END2000_EFFLQ     =
                :DFL-MONT-END2000-EFFLQ
                                       :IND-DFL-MONT-END2000-EFFLQ
                  ,MONT_END2000_DFZ       =
                :DFL-MONT-END2000-DFZ
                                       :IND-DFL-MONT-END2000-DFZ
                  ,MONT_END2006_CALC      =
                :DFL-MONT-END2006-CALC
                                       :IND-DFL-MONT-END2006-CALC
                  ,MONT_END2006_EFFLQ     =
                :DFL-MONT-END2006-EFFLQ
                                       :IND-DFL-MONT-END2006-EFFLQ
                  ,MONT_END2006_DFZ       =
                :DFL-MONT-END2006-DFZ
                                       :IND-DFL-MONT-END2006-DFZ
                  ,MONT_DAL2007_CALC      =
                :DFL-MONT-DAL2007-CALC
                                       :IND-DFL-MONT-DAL2007-CALC
                  ,MONT_DAL2007_EFFLQ     =
                :DFL-MONT-DAL2007-EFFLQ
                                       :IND-DFL-MONT-DAL2007-EFFLQ
                  ,MONT_DAL2007_DFZ       =
                :DFL-MONT-DAL2007-DFZ
                                       :IND-DFL-MONT-DAL2007-DFZ
                  ,IIMPST_PRVR_CALC       =
                :DFL-IIMPST-PRVR-CALC
                                       :IND-DFL-IIMPST-PRVR-CALC
                  ,IIMPST_PRVR_EFFLQ      =
                :DFL-IIMPST-PRVR-EFFLQ
                                       :IND-DFL-IIMPST-PRVR-EFFLQ
                  ,IIMPST_PRVR_DFZ        =
                :DFL-IIMPST-PRVR-DFZ
                                       :IND-DFL-IIMPST-PRVR-DFZ
                  ,IIMPST_252_CALC        =
                :DFL-IIMPST-252-CALC
                                       :IND-DFL-IIMPST-252-CALC
                  ,IIMPST_252_EFFLQ       =
                :DFL-IIMPST-252-EFFLQ
                                       :IND-DFL-IIMPST-252-EFFLQ
                  ,IIMPST_252_DFZ         =
                :DFL-IIMPST-252-DFZ
                                       :IND-DFL-IIMPST-252-DFZ
                  ,IMPST_252_CALC         =
                :DFL-IMPST-252-CALC
                                       :IND-DFL-IMPST-252-CALC
                  ,IMPST_252_EFFLQ        =
                :DFL-IMPST-252-EFFLQ
                                       :IND-DFL-IMPST-252-EFFLQ
                  ,RIT_TFR_CALC           =
                :DFL-RIT-TFR-CALC
                                       :IND-DFL-RIT-TFR-CALC
                  ,RIT_TFR_EFFLQ          =
                :DFL-RIT-TFR-EFFLQ
                                       :IND-DFL-RIT-TFR-EFFLQ
                  ,RIT_TFR_DFZ            =
                :DFL-RIT-TFR-DFZ
                                       :IND-DFL-RIT-TFR-DFZ
                  ,CNBT_INPSTFM_CALC      =
                :DFL-CNBT-INPSTFM-CALC
                                       :IND-DFL-CNBT-INPSTFM-CALC
                  ,CNBT_INPSTFM_EFFLQ     =
                :DFL-CNBT-INPSTFM-EFFLQ
                                       :IND-DFL-CNBT-INPSTFM-EFFLQ
                  ,CNBT_INPSTFM_DFZ       =
                :DFL-CNBT-INPSTFM-DFZ
                                       :IND-DFL-CNBT-INPSTFM-DFZ
                  ,ICNB_INPSTFM_CALC      =
                :DFL-ICNB-INPSTFM-CALC
                                       :IND-DFL-ICNB-INPSTFM-CALC
                  ,ICNB_INPSTFM_EFFLQ     =
                :DFL-ICNB-INPSTFM-EFFLQ
                                       :IND-DFL-ICNB-INPSTFM-EFFLQ
                  ,ICNB_INPSTFM_DFZ       =
                :DFL-ICNB-INPSTFM-DFZ
                                       :IND-DFL-ICNB-INPSTFM-DFZ
                  ,CNDE_END2000_CALC      =
                :DFL-CNDE-END2000-CALC
                                       :IND-DFL-CNDE-END2000-CALC
                  ,CNDE_END2000_EFFLQ     =
                :DFL-CNDE-END2000-EFFLQ
                                       :IND-DFL-CNDE-END2000-EFFLQ
                  ,CNDE_END2000_DFZ       =
                :DFL-CNDE-END2000-DFZ
                                       :IND-DFL-CNDE-END2000-DFZ
                  ,CNDE_END2006_CALC      =
                :DFL-CNDE-END2006-CALC
                                       :IND-DFL-CNDE-END2006-CALC
                  ,CNDE_END2006_EFFLQ     =
                :DFL-CNDE-END2006-EFFLQ
                                       :IND-DFL-CNDE-END2006-EFFLQ
                  ,CNDE_END2006_DFZ       =
                :DFL-CNDE-END2006-DFZ
                                       :IND-DFL-CNDE-END2006-DFZ
                  ,CNDE_DAL2007_CALC      =
                :DFL-CNDE-DAL2007-CALC
                                       :IND-DFL-CNDE-DAL2007-CALC
                  ,CNDE_DAL2007_EFFLQ     =
                :DFL-CNDE-DAL2007-EFFLQ
                                       :IND-DFL-CNDE-DAL2007-EFFLQ
                  ,CNDE_DAL2007_DFZ       =
                :DFL-CNDE-DAL2007-DFZ
                                       :IND-DFL-CNDE-DAL2007-DFZ
                  ,AA_CNBZ_END2000_EF     =
                :DFL-AA-CNBZ-END2000-EF
                                       :IND-DFL-AA-CNBZ-END2000-EF
                  ,AA_CNBZ_END2006_EF     =
                :DFL-AA-CNBZ-END2006-EF
                                       :IND-DFL-AA-CNBZ-END2006-EF
                  ,AA_CNBZ_DAL2007_EF     =
                :DFL-AA-CNBZ-DAL2007-EF
                                       :IND-DFL-AA-CNBZ-DAL2007-EF
                  ,MM_CNBZ_END2000_EF     =
                :DFL-MM-CNBZ-END2000-EF
                                       :IND-DFL-MM-CNBZ-END2000-EF
                  ,MM_CNBZ_END2006_EF     =
                :DFL-MM-CNBZ-END2006-EF
                                       :IND-DFL-MM-CNBZ-END2006-EF
                  ,MM_CNBZ_DAL2007_EF     =
                :DFL-MM-CNBZ-DAL2007-EF
                                       :IND-DFL-MM-CNBZ-DAL2007-EF
                  ,IMPST_DA_RIMB_EFF      =
                :DFL-IMPST-DA-RIMB-EFF
                                       :IND-DFL-IMPST-DA-RIMB-EFF
                  ,ALQ_TAX_SEP_CALC       =
                :DFL-ALQ-TAX-SEP-CALC
                                       :IND-DFL-ALQ-TAX-SEP-CALC
                  ,ALQ_TAX_SEP_EFFLQ      =
                :DFL-ALQ-TAX-SEP-EFFLQ
                                       :IND-DFL-ALQ-TAX-SEP-EFFLQ
                  ,ALQ_TAX_SEP_DFZ        =
                :DFL-ALQ-TAX-SEP-DFZ
                                       :IND-DFL-ALQ-TAX-SEP-DFZ
                  ,ALQ_CNBT_INPSTFM_C     =
                :DFL-ALQ-CNBT-INPSTFM-C
                                       :IND-DFL-ALQ-CNBT-INPSTFM-C
                  ,ALQ_CNBT_INPSTFM_E     =
                :DFL-ALQ-CNBT-INPSTFM-E
                                       :IND-DFL-ALQ-CNBT-INPSTFM-E
                  ,ALQ_CNBT_INPSTFM_D     =
                :DFL-ALQ-CNBT-INPSTFM-D
                                       :IND-DFL-ALQ-CNBT-INPSTFM-D
                  ,DS_RIGA                =
                :DFL-DS-RIGA
                  ,DS_OPER_SQL            =
                :DFL-DS-OPER-SQL
                  ,DS_VER                 =
                :DFL-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :DFL-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :DFL-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :DFL-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :DFL-DS-STATO-ELAB
                  ,IMPB_VIS_1382011C      =
                :DFL-IMPB-VIS-1382011C
                                       :IND-DFL-IMPB-VIS-1382011C
                  ,IMPB_VIS_1382011D      =
                :DFL-IMPB-VIS-1382011D
                                       :IND-DFL-IMPB-VIS-1382011D
                  ,IMPB_VIS_1382011L      =
                :DFL-IMPB-VIS-1382011L
                                       :IND-DFL-IMPB-VIS-1382011L
                  ,IMPST_VIS_1382011C     =
                :DFL-IMPST-VIS-1382011C
                                       :IND-DFL-IMPST-VIS-1382011C
                  ,IMPST_VIS_1382011D     =
                :DFL-IMPST-VIS-1382011D
                                       :IND-DFL-IMPST-VIS-1382011D
                  ,IMPST_VIS_1382011L     =
                :DFL-IMPST-VIS-1382011L
                                       :IND-DFL-IMPST-VIS-1382011L
                  ,IMPB_IS_1382011C       =
                :DFL-IMPB-IS-1382011C
                                       :IND-DFL-IMPB-IS-1382011C
                  ,IMPB_IS_1382011D       =
                :DFL-IMPB-IS-1382011D
                                       :IND-DFL-IMPB-IS-1382011D
                  ,IMPB_IS_1382011L       =
                :DFL-IMPB-IS-1382011L
                                       :IND-DFL-IMPB-IS-1382011L
                  ,IS_1382011C            =
                :DFL-IS-1382011C
                                       :IND-DFL-IS-1382011C
                  ,IS_1382011D            =
                :DFL-IS-1382011D
                                       :IND-DFL-IS-1382011D
                  ,IS_1382011L            =
                :DFL-IS-1382011L
                                       :IND-DFL-IS-1382011L
                  ,IMP_INTR_RIT_PAG_C     =
                :DFL-IMP-INTR-RIT-PAG-C
                                       :IND-DFL-IMP-INTR-RIT-PAG-C
                  ,IMP_INTR_RIT_PAG_D     =
                :DFL-IMP-INTR-RIT-PAG-D
                                       :IND-DFL-IMP-INTR-RIT-PAG-D
                  ,IMP_INTR_RIT_PAG_L     =
                :DFL-IMP-INTR-RIT-PAG-L
                                       :IND-DFL-IMP-INTR-RIT-PAG-L
                  ,IMPB_BOLLO_DETT_C      =
                :DFL-IMPB-BOLLO-DETT-C
                                       :IND-DFL-IMPB-BOLLO-DETT-C
                  ,IMPB_BOLLO_DETT_D      =
                :DFL-IMPB-BOLLO-DETT-D
                                       :IND-DFL-IMPB-BOLLO-DETT-D
                  ,IMPB_BOLLO_DETT_L      =
                :DFL-IMPB-BOLLO-DETT-L
                                       :IND-DFL-IMPB-BOLLO-DETT-L
                  ,IMPST_BOLLO_DETT_C     =
                :DFL-IMPST-BOLLO-DETT-C
                                       :IND-DFL-IMPST-BOLLO-DETT-C
                  ,IMPST_BOLLO_DETT_D     =
                :DFL-IMPST-BOLLO-DETT-D
                                       :IND-DFL-IMPST-BOLLO-DETT-D
                  ,IMPST_BOLLO_DETT_L     =
                :DFL-IMPST-BOLLO-DETT-L
                                       :IND-DFL-IMPST-BOLLO-DETT-L
                  ,IMPST_BOLLO_TOT_VC     =
                :DFL-IMPST-BOLLO-TOT-VC
                                       :IND-DFL-IMPST-BOLLO-TOT-VC
                  ,IMPST_BOLLO_TOT_VD     =
                :DFL-IMPST-BOLLO-TOT-VD
                                       :IND-DFL-IMPST-BOLLO-TOT-VD
                  ,IMPST_BOLLO_TOT_VL     =
                :DFL-IMPST-BOLLO-TOT-VL
                                       :IND-DFL-IMPST-BOLLO-TOT-VL
                  ,IMPB_VIS_662014C       =
                :DFL-IMPB-VIS-662014C
                                       :IND-DFL-IMPB-VIS-662014C
                  ,IMPB_VIS_662014D       =
                :DFL-IMPB-VIS-662014D
                                       :IND-DFL-IMPB-VIS-662014D
                  ,IMPB_VIS_662014L       =
                :DFL-IMPB-VIS-662014L
                                       :IND-DFL-IMPB-VIS-662014L
                  ,IMPST_VIS_662014C      =
                :DFL-IMPST-VIS-662014C
                                       :IND-DFL-IMPST-VIS-662014C
                  ,IMPST_VIS_662014D      =
                :DFL-IMPST-VIS-662014D
                                       :IND-DFL-IMPST-VIS-662014D
                  ,IMPST_VIS_662014L      =
                :DFL-IMPST-VIS-662014L
                                       :IND-DFL-IMPST-VIS-662014L
                  ,IMPB_IS_662014C        =
                :DFL-IMPB-IS-662014C
                                       :IND-DFL-IMPB-IS-662014C
                  ,IMPB_IS_662014D        =
                :DFL-IMPB-IS-662014D
                                       :IND-DFL-IMPB-IS-662014D
                  ,IMPB_IS_662014L        =
                :DFL-IMPB-IS-662014L
                                       :IND-DFL-IMPB-IS-662014L
                  ,IS_662014C             =
                :DFL-IS-662014C
                                       :IND-DFL-IS-662014C
                  ,IS_662014D             =
                :DFL-IS-662014D
                                       :IND-DFL-IS-662014D
                  ,IS_662014L             =
                :DFL-IS-662014L
                                       :IND-DFL-IS-662014L
                WHERE     DS_RIGA = :DFL-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-DFL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-DFL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-DFL
           INTO
                :DFL-ID-D-FORZ-LIQ
               ,:DFL-ID-LIQ
               ,:DFL-ID-MOVI-CRZ
               ,:DFL-ID-MOVI-CHIU
                :IND-DFL-ID-MOVI-CHIU
               ,:DFL-COD-COMP-ANIA
               ,:DFL-DT-INI-EFF-DB
               ,:DFL-DT-END-EFF-DB
               ,:DFL-IMP-LRD-CALC
                :IND-DFL-IMP-LRD-CALC
               ,:DFL-IMP-LRD-DFZ
                :IND-DFL-IMP-LRD-DFZ
               ,:DFL-IMP-LRD-EFFLQ
                :IND-DFL-IMP-LRD-EFFLQ
               ,:DFL-IMP-NET-CALC
                :IND-DFL-IMP-NET-CALC
               ,:DFL-IMP-NET-DFZ
                :IND-DFL-IMP-NET-DFZ
               ,:DFL-IMP-NET-EFFLQ
                :IND-DFL-IMP-NET-EFFLQ
               ,:DFL-IMPST-PRVR-CALC
                :IND-DFL-IMPST-PRVR-CALC
               ,:DFL-IMPST-PRVR-DFZ
                :IND-DFL-IMPST-PRVR-DFZ
               ,:DFL-IMPST-PRVR-EFFLQ
                :IND-DFL-IMPST-PRVR-EFFLQ
               ,:DFL-IMPST-VIS-CALC
                :IND-DFL-IMPST-VIS-CALC
               ,:DFL-IMPST-VIS-DFZ
                :IND-DFL-IMPST-VIS-DFZ
               ,:DFL-IMPST-VIS-EFFLQ
                :IND-DFL-IMPST-VIS-EFFLQ
               ,:DFL-RIT-ACC-CALC
                :IND-DFL-RIT-ACC-CALC
               ,:DFL-RIT-ACC-DFZ
                :IND-DFL-RIT-ACC-DFZ
               ,:DFL-RIT-ACC-EFFLQ
                :IND-DFL-RIT-ACC-EFFLQ
               ,:DFL-RIT-IRPEF-CALC
                :IND-DFL-RIT-IRPEF-CALC
               ,:DFL-RIT-IRPEF-DFZ
                :IND-DFL-RIT-IRPEF-DFZ
               ,:DFL-RIT-IRPEF-EFFLQ
                :IND-DFL-RIT-IRPEF-EFFLQ
               ,:DFL-IMPST-SOST-CALC
                :IND-DFL-IMPST-SOST-CALC
               ,:DFL-IMPST-SOST-DFZ
                :IND-DFL-IMPST-SOST-DFZ
               ,:DFL-IMPST-SOST-EFFLQ
                :IND-DFL-IMPST-SOST-EFFLQ
               ,:DFL-TAX-SEP-CALC
                :IND-DFL-TAX-SEP-CALC
               ,:DFL-TAX-SEP-DFZ
                :IND-DFL-TAX-SEP-DFZ
               ,:DFL-TAX-SEP-EFFLQ
                :IND-DFL-TAX-SEP-EFFLQ
               ,:DFL-INTR-PREST-CALC
                :IND-DFL-INTR-PREST-CALC
               ,:DFL-INTR-PREST-DFZ
                :IND-DFL-INTR-PREST-DFZ
               ,:DFL-INTR-PREST-EFFLQ
                :IND-DFL-INTR-PREST-EFFLQ
               ,:DFL-ACCPRE-SOST-CALC
                :IND-DFL-ACCPRE-SOST-CALC
               ,:DFL-ACCPRE-SOST-DFZ
                :IND-DFL-ACCPRE-SOST-DFZ
               ,:DFL-ACCPRE-SOST-EFFLQ
                :IND-DFL-ACCPRE-SOST-EFFLQ
               ,:DFL-ACCPRE-VIS-CALC
                :IND-DFL-ACCPRE-VIS-CALC
               ,:DFL-ACCPRE-VIS-DFZ
                :IND-DFL-ACCPRE-VIS-DFZ
               ,:DFL-ACCPRE-VIS-EFFLQ
                :IND-DFL-ACCPRE-VIS-EFFLQ
               ,:DFL-ACCPRE-ACC-CALC
                :IND-DFL-ACCPRE-ACC-CALC
               ,:DFL-ACCPRE-ACC-DFZ
                :IND-DFL-ACCPRE-ACC-DFZ
               ,:DFL-ACCPRE-ACC-EFFLQ
                :IND-DFL-ACCPRE-ACC-EFFLQ
               ,:DFL-RES-PRSTZ-CALC
                :IND-DFL-RES-PRSTZ-CALC
               ,:DFL-RES-PRSTZ-DFZ
                :IND-DFL-RES-PRSTZ-DFZ
               ,:DFL-RES-PRSTZ-EFFLQ
                :IND-DFL-RES-PRSTZ-EFFLQ
               ,:DFL-RES-PRE-ATT-CALC
                :IND-DFL-RES-PRE-ATT-CALC
               ,:DFL-RES-PRE-ATT-DFZ
                :IND-DFL-RES-PRE-ATT-DFZ
               ,:DFL-RES-PRE-ATT-EFFLQ
                :IND-DFL-RES-PRE-ATT-EFFLQ
               ,:DFL-IMP-EXCONTR-EFF
                :IND-DFL-IMP-EXCONTR-EFF
               ,:DFL-IMPB-VIS-CALC
                :IND-DFL-IMPB-VIS-CALC
               ,:DFL-IMPB-VIS-EFFLQ
                :IND-DFL-IMPB-VIS-EFFLQ
               ,:DFL-IMPB-VIS-DFZ
                :IND-DFL-IMPB-VIS-DFZ
               ,:DFL-IMPB-RIT-ACC-CALC
                :IND-DFL-IMPB-RIT-ACC-CALC
               ,:DFL-IMPB-RIT-ACC-EFFLQ
                :IND-DFL-IMPB-RIT-ACC-EFFLQ
               ,:DFL-IMPB-RIT-ACC-DFZ
                :IND-DFL-IMPB-RIT-ACC-DFZ
               ,:DFL-IMPB-TFR-CALC
                :IND-DFL-IMPB-TFR-CALC
               ,:DFL-IMPB-TFR-EFFLQ
                :IND-DFL-IMPB-TFR-EFFLQ
               ,:DFL-IMPB-TFR-DFZ
                :IND-DFL-IMPB-TFR-DFZ
               ,:DFL-IMPB-IS-CALC
                :IND-DFL-IMPB-IS-CALC
               ,:DFL-IMPB-IS-EFFLQ
                :IND-DFL-IMPB-IS-EFFLQ
               ,:DFL-IMPB-IS-DFZ
                :IND-DFL-IMPB-IS-DFZ
               ,:DFL-IMPB-TAX-SEP-CALC
                :IND-DFL-IMPB-TAX-SEP-CALC
               ,:DFL-IMPB-TAX-SEP-EFFLQ
                :IND-DFL-IMPB-TAX-SEP-EFFLQ
               ,:DFL-IMPB-TAX-SEP-DFZ
                :IND-DFL-IMPB-TAX-SEP-DFZ
               ,:DFL-IINT-PREST-CALC
                :IND-DFL-IINT-PREST-CALC
               ,:DFL-IINT-PREST-EFFLQ
                :IND-DFL-IINT-PREST-EFFLQ
               ,:DFL-IINT-PREST-DFZ
                :IND-DFL-IINT-PREST-DFZ
               ,:DFL-MONT-END2000-CALC
                :IND-DFL-MONT-END2000-CALC
               ,:DFL-MONT-END2000-EFFLQ
                :IND-DFL-MONT-END2000-EFFLQ
               ,:DFL-MONT-END2000-DFZ
                :IND-DFL-MONT-END2000-DFZ
               ,:DFL-MONT-END2006-CALC
                :IND-DFL-MONT-END2006-CALC
               ,:DFL-MONT-END2006-EFFLQ
                :IND-DFL-MONT-END2006-EFFLQ
               ,:DFL-MONT-END2006-DFZ
                :IND-DFL-MONT-END2006-DFZ
               ,:DFL-MONT-DAL2007-CALC
                :IND-DFL-MONT-DAL2007-CALC
               ,:DFL-MONT-DAL2007-EFFLQ
                :IND-DFL-MONT-DAL2007-EFFLQ
               ,:DFL-MONT-DAL2007-DFZ
                :IND-DFL-MONT-DAL2007-DFZ
               ,:DFL-IIMPST-PRVR-CALC
                :IND-DFL-IIMPST-PRVR-CALC
               ,:DFL-IIMPST-PRVR-EFFLQ
                :IND-DFL-IIMPST-PRVR-EFFLQ
               ,:DFL-IIMPST-PRVR-DFZ
                :IND-DFL-IIMPST-PRVR-DFZ
               ,:DFL-IIMPST-252-CALC
                :IND-DFL-IIMPST-252-CALC
               ,:DFL-IIMPST-252-EFFLQ
                :IND-DFL-IIMPST-252-EFFLQ
               ,:DFL-IIMPST-252-DFZ
                :IND-DFL-IIMPST-252-DFZ
               ,:DFL-IMPST-252-CALC
                :IND-DFL-IMPST-252-CALC
               ,:DFL-IMPST-252-EFFLQ
                :IND-DFL-IMPST-252-EFFLQ
               ,:DFL-RIT-TFR-CALC
                :IND-DFL-RIT-TFR-CALC
               ,:DFL-RIT-TFR-EFFLQ
                :IND-DFL-RIT-TFR-EFFLQ
               ,:DFL-RIT-TFR-DFZ
                :IND-DFL-RIT-TFR-DFZ
               ,:DFL-CNBT-INPSTFM-CALC
                :IND-DFL-CNBT-INPSTFM-CALC
               ,:DFL-CNBT-INPSTFM-EFFLQ
                :IND-DFL-CNBT-INPSTFM-EFFLQ
               ,:DFL-CNBT-INPSTFM-DFZ
                :IND-DFL-CNBT-INPSTFM-DFZ
               ,:DFL-ICNB-INPSTFM-CALC
                :IND-DFL-ICNB-INPSTFM-CALC
               ,:DFL-ICNB-INPSTFM-EFFLQ
                :IND-DFL-ICNB-INPSTFM-EFFLQ
               ,:DFL-ICNB-INPSTFM-DFZ
                :IND-DFL-ICNB-INPSTFM-DFZ
               ,:DFL-CNDE-END2000-CALC
                :IND-DFL-CNDE-END2000-CALC
               ,:DFL-CNDE-END2000-EFFLQ
                :IND-DFL-CNDE-END2000-EFFLQ
               ,:DFL-CNDE-END2000-DFZ
                :IND-DFL-CNDE-END2000-DFZ
               ,:DFL-CNDE-END2006-CALC
                :IND-DFL-CNDE-END2006-CALC
               ,:DFL-CNDE-END2006-EFFLQ
                :IND-DFL-CNDE-END2006-EFFLQ
               ,:DFL-CNDE-END2006-DFZ
                :IND-DFL-CNDE-END2006-DFZ
               ,:DFL-CNDE-DAL2007-CALC
                :IND-DFL-CNDE-DAL2007-CALC
               ,:DFL-CNDE-DAL2007-EFFLQ
                :IND-DFL-CNDE-DAL2007-EFFLQ
               ,:DFL-CNDE-DAL2007-DFZ
                :IND-DFL-CNDE-DAL2007-DFZ
               ,:DFL-AA-CNBZ-END2000-EF
                :IND-DFL-AA-CNBZ-END2000-EF
               ,:DFL-AA-CNBZ-END2006-EF
                :IND-DFL-AA-CNBZ-END2006-EF
               ,:DFL-AA-CNBZ-DAL2007-EF
                :IND-DFL-AA-CNBZ-DAL2007-EF
               ,:DFL-MM-CNBZ-END2000-EF
                :IND-DFL-MM-CNBZ-END2000-EF
               ,:DFL-MM-CNBZ-END2006-EF
                :IND-DFL-MM-CNBZ-END2006-EF
               ,:DFL-MM-CNBZ-DAL2007-EF
                :IND-DFL-MM-CNBZ-DAL2007-EF
               ,:DFL-IMPST-DA-RIMB-EFF
                :IND-DFL-IMPST-DA-RIMB-EFF
               ,:DFL-ALQ-TAX-SEP-CALC
                :IND-DFL-ALQ-TAX-SEP-CALC
               ,:DFL-ALQ-TAX-SEP-EFFLQ
                :IND-DFL-ALQ-TAX-SEP-EFFLQ
               ,:DFL-ALQ-TAX-SEP-DFZ
                :IND-DFL-ALQ-TAX-SEP-DFZ
               ,:DFL-ALQ-CNBT-INPSTFM-C
                :IND-DFL-ALQ-CNBT-INPSTFM-C
               ,:DFL-ALQ-CNBT-INPSTFM-E
                :IND-DFL-ALQ-CNBT-INPSTFM-E
               ,:DFL-ALQ-CNBT-INPSTFM-D
                :IND-DFL-ALQ-CNBT-INPSTFM-D
               ,:DFL-DS-RIGA
               ,:DFL-DS-OPER-SQL
               ,:DFL-DS-VER
               ,:DFL-DS-TS-INI-CPTZ
               ,:DFL-DS-TS-END-CPTZ
               ,:DFL-DS-UTENTE
               ,:DFL-DS-STATO-ELAB
               ,:DFL-IMPB-VIS-1382011C
                :IND-DFL-IMPB-VIS-1382011C
               ,:DFL-IMPB-VIS-1382011D
                :IND-DFL-IMPB-VIS-1382011D
               ,:DFL-IMPB-VIS-1382011L
                :IND-DFL-IMPB-VIS-1382011L
               ,:DFL-IMPST-VIS-1382011C
                :IND-DFL-IMPST-VIS-1382011C
               ,:DFL-IMPST-VIS-1382011D
                :IND-DFL-IMPST-VIS-1382011D
               ,:DFL-IMPST-VIS-1382011L
                :IND-DFL-IMPST-VIS-1382011L
               ,:DFL-IMPB-IS-1382011C
                :IND-DFL-IMPB-IS-1382011C
               ,:DFL-IMPB-IS-1382011D
                :IND-DFL-IMPB-IS-1382011D
               ,:DFL-IMPB-IS-1382011L
                :IND-DFL-IMPB-IS-1382011L
               ,:DFL-IS-1382011C
                :IND-DFL-IS-1382011C
               ,:DFL-IS-1382011D
                :IND-DFL-IS-1382011D
               ,:DFL-IS-1382011L
                :IND-DFL-IS-1382011L
               ,:DFL-IMP-INTR-RIT-PAG-C
                :IND-DFL-IMP-INTR-RIT-PAG-C
               ,:DFL-IMP-INTR-RIT-PAG-D
                :IND-DFL-IMP-INTR-RIT-PAG-D
               ,:DFL-IMP-INTR-RIT-PAG-L
                :IND-DFL-IMP-INTR-RIT-PAG-L
               ,:DFL-IMPB-BOLLO-DETT-C
                :IND-DFL-IMPB-BOLLO-DETT-C
               ,:DFL-IMPB-BOLLO-DETT-D
                :IND-DFL-IMPB-BOLLO-DETT-D
               ,:DFL-IMPB-BOLLO-DETT-L
                :IND-DFL-IMPB-BOLLO-DETT-L
               ,:DFL-IMPST-BOLLO-DETT-C
                :IND-DFL-IMPST-BOLLO-DETT-C
               ,:DFL-IMPST-BOLLO-DETT-D
                :IND-DFL-IMPST-BOLLO-DETT-D
               ,:DFL-IMPST-BOLLO-DETT-L
                :IND-DFL-IMPST-BOLLO-DETT-L
               ,:DFL-IMPST-BOLLO-TOT-VC
                :IND-DFL-IMPST-BOLLO-TOT-VC
               ,:DFL-IMPST-BOLLO-TOT-VD
                :IND-DFL-IMPST-BOLLO-TOT-VD
               ,:DFL-IMPST-BOLLO-TOT-VL
                :IND-DFL-IMPST-BOLLO-TOT-VL
               ,:DFL-IMPB-VIS-662014C
                :IND-DFL-IMPB-VIS-662014C
               ,:DFL-IMPB-VIS-662014D
                :IND-DFL-IMPB-VIS-662014D
               ,:DFL-IMPB-VIS-662014L
                :IND-DFL-IMPB-VIS-662014L
               ,:DFL-IMPST-VIS-662014C
                :IND-DFL-IMPST-VIS-662014C
               ,:DFL-IMPST-VIS-662014D
                :IND-DFL-IMPST-VIS-662014D
               ,:DFL-IMPST-VIS-662014L
                :IND-DFL-IMPST-VIS-662014L
               ,:DFL-IMPB-IS-662014C
                :IND-DFL-IMPB-IS-662014C
               ,:DFL-IMPB-IS-662014D
                :IND-DFL-IMPB-IS-662014D
               ,:DFL-IMPB-IS-662014L
                :IND-DFL-IMPB-IS-662014L
               ,:DFL-IS-662014C
                :IND-DFL-IS-662014C
               ,:DFL-IS-662014D
                :IND-DFL-IS-662014D
               ,:DFL-IS-662014L
                :IND-DFL-IS-662014L
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_D_FORZ_LIQ
                ,ID_LIQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,COD_COMP_ANIA
                ,DT_INI_EFF
                ,DT_END_EFF
                ,IMP_LRD_CALC
                ,IMP_LRD_DFZ
                ,IMP_LRD_EFFLQ
                ,IMP_NET_CALC
                ,IMP_NET_DFZ
                ,IMP_NET_EFFLQ
                ,IMPST_PRVR_CALC
                ,IMPST_PRVR_DFZ
                ,IMPST_PRVR_EFFLQ
                ,IMPST_VIS_CALC
                ,IMPST_VIS_DFZ
                ,IMPST_VIS_EFFLQ
                ,RIT_ACC_CALC
                ,RIT_ACC_DFZ
                ,RIT_ACC_EFFLQ
                ,RIT_IRPEF_CALC
                ,RIT_IRPEF_DFZ
                ,RIT_IRPEF_EFFLQ
                ,IMPST_SOST_CALC
                ,IMPST_SOST_DFZ
                ,IMPST_SOST_EFFLQ
                ,TAX_SEP_CALC
                ,TAX_SEP_DFZ
                ,TAX_SEP_EFFLQ
                ,INTR_PREST_CALC
                ,INTR_PREST_DFZ
                ,INTR_PREST_EFFLQ
                ,ACCPRE_SOST_CALC
                ,ACCPRE_SOST_DFZ
                ,ACCPRE_SOST_EFFLQ
                ,ACCPRE_VIS_CALC
                ,ACCPRE_VIS_DFZ
                ,ACCPRE_VIS_EFFLQ
                ,ACCPRE_ACC_CALC
                ,ACCPRE_ACC_DFZ
                ,ACCPRE_ACC_EFFLQ
                ,RES_PRSTZ_CALC
                ,RES_PRSTZ_DFZ
                ,RES_PRSTZ_EFFLQ
                ,RES_PRE_ATT_CALC
                ,RES_PRE_ATT_DFZ
                ,RES_PRE_ATT_EFFLQ
                ,IMP_EXCONTR_EFF
                ,IMPB_VIS_CALC
                ,IMPB_VIS_EFFLQ
                ,IMPB_VIS_DFZ
                ,IMPB_RIT_ACC_CALC
                ,IMPB_RIT_ACC_EFFLQ
                ,IMPB_RIT_ACC_DFZ
                ,IMPB_TFR_CALC
                ,IMPB_TFR_EFFLQ
                ,IMPB_TFR_DFZ
                ,IMPB_IS_CALC
                ,IMPB_IS_EFFLQ
                ,IMPB_IS_DFZ
                ,IMPB_TAX_SEP_CALC
                ,IMPB_TAX_SEP_EFFLQ
                ,IMPB_TAX_SEP_DFZ
                ,IINT_PREST_CALC
                ,IINT_PREST_EFFLQ
                ,IINT_PREST_DFZ
                ,MONT_END2000_CALC
                ,MONT_END2000_EFFLQ
                ,MONT_END2000_DFZ
                ,MONT_END2006_CALC
                ,MONT_END2006_EFFLQ
                ,MONT_END2006_DFZ
                ,MONT_DAL2007_CALC
                ,MONT_DAL2007_EFFLQ
                ,MONT_DAL2007_DFZ
                ,IIMPST_PRVR_CALC
                ,IIMPST_PRVR_EFFLQ
                ,IIMPST_PRVR_DFZ
                ,IIMPST_252_CALC
                ,IIMPST_252_EFFLQ
                ,IIMPST_252_DFZ
                ,IMPST_252_CALC
                ,IMPST_252_EFFLQ
                ,RIT_TFR_CALC
                ,RIT_TFR_EFFLQ
                ,RIT_TFR_DFZ
                ,CNBT_INPSTFM_CALC
                ,CNBT_INPSTFM_EFFLQ
                ,CNBT_INPSTFM_DFZ
                ,ICNB_INPSTFM_CALC
                ,ICNB_INPSTFM_EFFLQ
                ,ICNB_INPSTFM_DFZ
                ,CNDE_END2000_CALC
                ,CNDE_END2000_EFFLQ
                ,CNDE_END2000_DFZ
                ,CNDE_END2006_CALC
                ,CNDE_END2006_EFFLQ
                ,CNDE_END2006_DFZ
                ,CNDE_DAL2007_CALC
                ,CNDE_DAL2007_EFFLQ
                ,CNDE_DAL2007_DFZ
                ,AA_CNBZ_END2000_EF
                ,AA_CNBZ_END2006_EF
                ,AA_CNBZ_DAL2007_EF
                ,MM_CNBZ_END2000_EF
                ,MM_CNBZ_END2006_EF
                ,MM_CNBZ_DAL2007_EF
                ,IMPST_DA_RIMB_EFF
                ,ALQ_TAX_SEP_CALC
                ,ALQ_TAX_SEP_EFFLQ
                ,ALQ_TAX_SEP_DFZ
                ,ALQ_CNBT_INPSTFM_C
                ,ALQ_CNBT_INPSTFM_E
                ,ALQ_CNBT_INPSTFM_D
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,IMPB_VIS_1382011C
                ,IMPB_VIS_1382011D
                ,IMPB_VIS_1382011L
                ,IMPST_VIS_1382011C
                ,IMPST_VIS_1382011D
                ,IMPST_VIS_1382011L
                ,IMPB_IS_1382011C
                ,IMPB_IS_1382011D
                ,IMPB_IS_1382011L
                ,IS_1382011C
                ,IS_1382011D
                ,IS_1382011L
                ,IMP_INTR_RIT_PAG_C
                ,IMP_INTR_RIT_PAG_D
                ,IMP_INTR_RIT_PAG_L
                ,IMPB_BOLLO_DETT_C
                ,IMPB_BOLLO_DETT_D
                ,IMPB_BOLLO_DETT_L
                ,IMPST_BOLLO_DETT_C
                ,IMPST_BOLLO_DETT_D
                ,IMPST_BOLLO_DETT_L
                ,IMPST_BOLLO_TOT_VC
                ,IMPST_BOLLO_TOT_VD
                ,IMPST_BOLLO_TOT_VL
                ,IMPB_VIS_662014C
                ,IMPB_VIS_662014D
                ,IMPB_VIS_662014L
                ,IMPST_VIS_662014C
                ,IMPST_VIS_662014D
                ,IMPST_VIS_662014L
                ,IMPB_IS_662014C
                ,IMPB_IS_662014D
                ,IMPB_IS_662014L
                ,IS_662014C
                ,IS_662014D
                ,IS_662014L
             INTO
                :DFL-ID-D-FORZ-LIQ
               ,:DFL-ID-LIQ
               ,:DFL-ID-MOVI-CRZ
               ,:DFL-ID-MOVI-CHIU
                :IND-DFL-ID-MOVI-CHIU
               ,:DFL-COD-COMP-ANIA
               ,:DFL-DT-INI-EFF-DB
               ,:DFL-DT-END-EFF-DB
               ,:DFL-IMP-LRD-CALC
                :IND-DFL-IMP-LRD-CALC
               ,:DFL-IMP-LRD-DFZ
                :IND-DFL-IMP-LRD-DFZ
               ,:DFL-IMP-LRD-EFFLQ
                :IND-DFL-IMP-LRD-EFFLQ
               ,:DFL-IMP-NET-CALC
                :IND-DFL-IMP-NET-CALC
               ,:DFL-IMP-NET-DFZ
                :IND-DFL-IMP-NET-DFZ
               ,:DFL-IMP-NET-EFFLQ
                :IND-DFL-IMP-NET-EFFLQ
               ,:DFL-IMPST-PRVR-CALC
                :IND-DFL-IMPST-PRVR-CALC
               ,:DFL-IMPST-PRVR-DFZ
                :IND-DFL-IMPST-PRVR-DFZ
               ,:DFL-IMPST-PRVR-EFFLQ
                :IND-DFL-IMPST-PRVR-EFFLQ
               ,:DFL-IMPST-VIS-CALC
                :IND-DFL-IMPST-VIS-CALC
               ,:DFL-IMPST-VIS-DFZ
                :IND-DFL-IMPST-VIS-DFZ
               ,:DFL-IMPST-VIS-EFFLQ
                :IND-DFL-IMPST-VIS-EFFLQ
               ,:DFL-RIT-ACC-CALC
                :IND-DFL-RIT-ACC-CALC
               ,:DFL-RIT-ACC-DFZ
                :IND-DFL-RIT-ACC-DFZ
               ,:DFL-RIT-ACC-EFFLQ
                :IND-DFL-RIT-ACC-EFFLQ
               ,:DFL-RIT-IRPEF-CALC
                :IND-DFL-RIT-IRPEF-CALC
               ,:DFL-RIT-IRPEF-DFZ
                :IND-DFL-RIT-IRPEF-DFZ
               ,:DFL-RIT-IRPEF-EFFLQ
                :IND-DFL-RIT-IRPEF-EFFLQ
               ,:DFL-IMPST-SOST-CALC
                :IND-DFL-IMPST-SOST-CALC
               ,:DFL-IMPST-SOST-DFZ
                :IND-DFL-IMPST-SOST-DFZ
               ,:DFL-IMPST-SOST-EFFLQ
                :IND-DFL-IMPST-SOST-EFFLQ
               ,:DFL-TAX-SEP-CALC
                :IND-DFL-TAX-SEP-CALC
               ,:DFL-TAX-SEP-DFZ
                :IND-DFL-TAX-SEP-DFZ
               ,:DFL-TAX-SEP-EFFLQ
                :IND-DFL-TAX-SEP-EFFLQ
               ,:DFL-INTR-PREST-CALC
                :IND-DFL-INTR-PREST-CALC
               ,:DFL-INTR-PREST-DFZ
                :IND-DFL-INTR-PREST-DFZ
               ,:DFL-INTR-PREST-EFFLQ
                :IND-DFL-INTR-PREST-EFFLQ
               ,:DFL-ACCPRE-SOST-CALC
                :IND-DFL-ACCPRE-SOST-CALC
               ,:DFL-ACCPRE-SOST-DFZ
                :IND-DFL-ACCPRE-SOST-DFZ
               ,:DFL-ACCPRE-SOST-EFFLQ
                :IND-DFL-ACCPRE-SOST-EFFLQ
               ,:DFL-ACCPRE-VIS-CALC
                :IND-DFL-ACCPRE-VIS-CALC
               ,:DFL-ACCPRE-VIS-DFZ
                :IND-DFL-ACCPRE-VIS-DFZ
               ,:DFL-ACCPRE-VIS-EFFLQ
                :IND-DFL-ACCPRE-VIS-EFFLQ
               ,:DFL-ACCPRE-ACC-CALC
                :IND-DFL-ACCPRE-ACC-CALC
               ,:DFL-ACCPRE-ACC-DFZ
                :IND-DFL-ACCPRE-ACC-DFZ
               ,:DFL-ACCPRE-ACC-EFFLQ
                :IND-DFL-ACCPRE-ACC-EFFLQ
               ,:DFL-RES-PRSTZ-CALC
                :IND-DFL-RES-PRSTZ-CALC
               ,:DFL-RES-PRSTZ-DFZ
                :IND-DFL-RES-PRSTZ-DFZ
               ,:DFL-RES-PRSTZ-EFFLQ
                :IND-DFL-RES-PRSTZ-EFFLQ
               ,:DFL-RES-PRE-ATT-CALC
                :IND-DFL-RES-PRE-ATT-CALC
               ,:DFL-RES-PRE-ATT-DFZ
                :IND-DFL-RES-PRE-ATT-DFZ
               ,:DFL-RES-PRE-ATT-EFFLQ
                :IND-DFL-RES-PRE-ATT-EFFLQ
               ,:DFL-IMP-EXCONTR-EFF
                :IND-DFL-IMP-EXCONTR-EFF
               ,:DFL-IMPB-VIS-CALC
                :IND-DFL-IMPB-VIS-CALC
               ,:DFL-IMPB-VIS-EFFLQ
                :IND-DFL-IMPB-VIS-EFFLQ
               ,:DFL-IMPB-VIS-DFZ
                :IND-DFL-IMPB-VIS-DFZ
               ,:DFL-IMPB-RIT-ACC-CALC
                :IND-DFL-IMPB-RIT-ACC-CALC
               ,:DFL-IMPB-RIT-ACC-EFFLQ
                :IND-DFL-IMPB-RIT-ACC-EFFLQ
               ,:DFL-IMPB-RIT-ACC-DFZ
                :IND-DFL-IMPB-RIT-ACC-DFZ
               ,:DFL-IMPB-TFR-CALC
                :IND-DFL-IMPB-TFR-CALC
               ,:DFL-IMPB-TFR-EFFLQ
                :IND-DFL-IMPB-TFR-EFFLQ
               ,:DFL-IMPB-TFR-DFZ
                :IND-DFL-IMPB-TFR-DFZ
               ,:DFL-IMPB-IS-CALC
                :IND-DFL-IMPB-IS-CALC
               ,:DFL-IMPB-IS-EFFLQ
                :IND-DFL-IMPB-IS-EFFLQ
               ,:DFL-IMPB-IS-DFZ
                :IND-DFL-IMPB-IS-DFZ
               ,:DFL-IMPB-TAX-SEP-CALC
                :IND-DFL-IMPB-TAX-SEP-CALC
               ,:DFL-IMPB-TAX-SEP-EFFLQ
                :IND-DFL-IMPB-TAX-SEP-EFFLQ
               ,:DFL-IMPB-TAX-SEP-DFZ
                :IND-DFL-IMPB-TAX-SEP-DFZ
               ,:DFL-IINT-PREST-CALC
                :IND-DFL-IINT-PREST-CALC
               ,:DFL-IINT-PREST-EFFLQ
                :IND-DFL-IINT-PREST-EFFLQ
               ,:DFL-IINT-PREST-DFZ
                :IND-DFL-IINT-PREST-DFZ
               ,:DFL-MONT-END2000-CALC
                :IND-DFL-MONT-END2000-CALC
               ,:DFL-MONT-END2000-EFFLQ
                :IND-DFL-MONT-END2000-EFFLQ
               ,:DFL-MONT-END2000-DFZ
                :IND-DFL-MONT-END2000-DFZ
               ,:DFL-MONT-END2006-CALC
                :IND-DFL-MONT-END2006-CALC
               ,:DFL-MONT-END2006-EFFLQ
                :IND-DFL-MONT-END2006-EFFLQ
               ,:DFL-MONT-END2006-DFZ
                :IND-DFL-MONT-END2006-DFZ
               ,:DFL-MONT-DAL2007-CALC
                :IND-DFL-MONT-DAL2007-CALC
               ,:DFL-MONT-DAL2007-EFFLQ
                :IND-DFL-MONT-DAL2007-EFFLQ
               ,:DFL-MONT-DAL2007-DFZ
                :IND-DFL-MONT-DAL2007-DFZ
               ,:DFL-IIMPST-PRVR-CALC
                :IND-DFL-IIMPST-PRVR-CALC
               ,:DFL-IIMPST-PRVR-EFFLQ
                :IND-DFL-IIMPST-PRVR-EFFLQ
               ,:DFL-IIMPST-PRVR-DFZ
                :IND-DFL-IIMPST-PRVR-DFZ
               ,:DFL-IIMPST-252-CALC
                :IND-DFL-IIMPST-252-CALC
               ,:DFL-IIMPST-252-EFFLQ
                :IND-DFL-IIMPST-252-EFFLQ
               ,:DFL-IIMPST-252-DFZ
                :IND-DFL-IIMPST-252-DFZ
               ,:DFL-IMPST-252-CALC
                :IND-DFL-IMPST-252-CALC
               ,:DFL-IMPST-252-EFFLQ
                :IND-DFL-IMPST-252-EFFLQ
               ,:DFL-RIT-TFR-CALC
                :IND-DFL-RIT-TFR-CALC
               ,:DFL-RIT-TFR-EFFLQ
                :IND-DFL-RIT-TFR-EFFLQ
               ,:DFL-RIT-TFR-DFZ
                :IND-DFL-RIT-TFR-DFZ
               ,:DFL-CNBT-INPSTFM-CALC
                :IND-DFL-CNBT-INPSTFM-CALC
               ,:DFL-CNBT-INPSTFM-EFFLQ
                :IND-DFL-CNBT-INPSTFM-EFFLQ
               ,:DFL-CNBT-INPSTFM-DFZ
                :IND-DFL-CNBT-INPSTFM-DFZ
               ,:DFL-ICNB-INPSTFM-CALC
                :IND-DFL-ICNB-INPSTFM-CALC
               ,:DFL-ICNB-INPSTFM-EFFLQ
                :IND-DFL-ICNB-INPSTFM-EFFLQ
               ,:DFL-ICNB-INPSTFM-DFZ
                :IND-DFL-ICNB-INPSTFM-DFZ
               ,:DFL-CNDE-END2000-CALC
                :IND-DFL-CNDE-END2000-CALC
               ,:DFL-CNDE-END2000-EFFLQ
                :IND-DFL-CNDE-END2000-EFFLQ
               ,:DFL-CNDE-END2000-DFZ
                :IND-DFL-CNDE-END2000-DFZ
               ,:DFL-CNDE-END2006-CALC
                :IND-DFL-CNDE-END2006-CALC
               ,:DFL-CNDE-END2006-EFFLQ
                :IND-DFL-CNDE-END2006-EFFLQ
               ,:DFL-CNDE-END2006-DFZ
                :IND-DFL-CNDE-END2006-DFZ
               ,:DFL-CNDE-DAL2007-CALC
                :IND-DFL-CNDE-DAL2007-CALC
               ,:DFL-CNDE-DAL2007-EFFLQ
                :IND-DFL-CNDE-DAL2007-EFFLQ
               ,:DFL-CNDE-DAL2007-DFZ
                :IND-DFL-CNDE-DAL2007-DFZ
               ,:DFL-AA-CNBZ-END2000-EF
                :IND-DFL-AA-CNBZ-END2000-EF
               ,:DFL-AA-CNBZ-END2006-EF
                :IND-DFL-AA-CNBZ-END2006-EF
               ,:DFL-AA-CNBZ-DAL2007-EF
                :IND-DFL-AA-CNBZ-DAL2007-EF
               ,:DFL-MM-CNBZ-END2000-EF
                :IND-DFL-MM-CNBZ-END2000-EF
               ,:DFL-MM-CNBZ-END2006-EF
                :IND-DFL-MM-CNBZ-END2006-EF
               ,:DFL-MM-CNBZ-DAL2007-EF
                :IND-DFL-MM-CNBZ-DAL2007-EF
               ,:DFL-IMPST-DA-RIMB-EFF
                :IND-DFL-IMPST-DA-RIMB-EFF
               ,:DFL-ALQ-TAX-SEP-CALC
                :IND-DFL-ALQ-TAX-SEP-CALC
               ,:DFL-ALQ-TAX-SEP-EFFLQ
                :IND-DFL-ALQ-TAX-SEP-EFFLQ
               ,:DFL-ALQ-TAX-SEP-DFZ
                :IND-DFL-ALQ-TAX-SEP-DFZ
               ,:DFL-ALQ-CNBT-INPSTFM-C
                :IND-DFL-ALQ-CNBT-INPSTFM-C
               ,:DFL-ALQ-CNBT-INPSTFM-E
                :IND-DFL-ALQ-CNBT-INPSTFM-E
               ,:DFL-ALQ-CNBT-INPSTFM-D
                :IND-DFL-ALQ-CNBT-INPSTFM-D
               ,:DFL-DS-RIGA
               ,:DFL-DS-OPER-SQL
               ,:DFL-DS-VER
               ,:DFL-DS-TS-INI-CPTZ
               ,:DFL-DS-TS-END-CPTZ
               ,:DFL-DS-UTENTE
               ,:DFL-DS-STATO-ELAB
               ,:DFL-IMPB-VIS-1382011C
                :IND-DFL-IMPB-VIS-1382011C
               ,:DFL-IMPB-VIS-1382011D
                :IND-DFL-IMPB-VIS-1382011D
               ,:DFL-IMPB-VIS-1382011L
                :IND-DFL-IMPB-VIS-1382011L
               ,:DFL-IMPST-VIS-1382011C
                :IND-DFL-IMPST-VIS-1382011C
               ,:DFL-IMPST-VIS-1382011D
                :IND-DFL-IMPST-VIS-1382011D
               ,:DFL-IMPST-VIS-1382011L
                :IND-DFL-IMPST-VIS-1382011L
               ,:DFL-IMPB-IS-1382011C
                :IND-DFL-IMPB-IS-1382011C
               ,:DFL-IMPB-IS-1382011D
                :IND-DFL-IMPB-IS-1382011D
               ,:DFL-IMPB-IS-1382011L
                :IND-DFL-IMPB-IS-1382011L
               ,:DFL-IS-1382011C
                :IND-DFL-IS-1382011C
               ,:DFL-IS-1382011D
                :IND-DFL-IS-1382011D
               ,:DFL-IS-1382011L
                :IND-DFL-IS-1382011L
               ,:DFL-IMP-INTR-RIT-PAG-C
                :IND-DFL-IMP-INTR-RIT-PAG-C
               ,:DFL-IMP-INTR-RIT-PAG-D
                :IND-DFL-IMP-INTR-RIT-PAG-D
               ,:DFL-IMP-INTR-RIT-PAG-L
                :IND-DFL-IMP-INTR-RIT-PAG-L
               ,:DFL-IMPB-BOLLO-DETT-C
                :IND-DFL-IMPB-BOLLO-DETT-C
               ,:DFL-IMPB-BOLLO-DETT-D
                :IND-DFL-IMPB-BOLLO-DETT-D
               ,:DFL-IMPB-BOLLO-DETT-L
                :IND-DFL-IMPB-BOLLO-DETT-L
               ,:DFL-IMPST-BOLLO-DETT-C
                :IND-DFL-IMPST-BOLLO-DETT-C
               ,:DFL-IMPST-BOLLO-DETT-D
                :IND-DFL-IMPST-BOLLO-DETT-D
               ,:DFL-IMPST-BOLLO-DETT-L
                :IND-DFL-IMPST-BOLLO-DETT-L
               ,:DFL-IMPST-BOLLO-TOT-VC
                :IND-DFL-IMPST-BOLLO-TOT-VC
               ,:DFL-IMPST-BOLLO-TOT-VD
                :IND-DFL-IMPST-BOLLO-TOT-VD
               ,:DFL-IMPST-BOLLO-TOT-VL
                :IND-DFL-IMPST-BOLLO-TOT-VL
               ,:DFL-IMPB-VIS-662014C
                :IND-DFL-IMPB-VIS-662014C
               ,:DFL-IMPB-VIS-662014D
                :IND-DFL-IMPB-VIS-662014D
               ,:DFL-IMPB-VIS-662014L
                :IND-DFL-IMPB-VIS-662014L
               ,:DFL-IMPST-VIS-662014C
                :IND-DFL-IMPST-VIS-662014C
               ,:DFL-IMPST-VIS-662014D
                :IND-DFL-IMPST-VIS-662014D
               ,:DFL-IMPST-VIS-662014L
                :IND-DFL-IMPST-VIS-662014L
               ,:DFL-IMPB-IS-662014C
                :IND-DFL-IMPB-IS-662014C
               ,:DFL-IMPB-IS-662014D
                :IND-DFL-IMPB-IS-662014D
               ,:DFL-IMPB-IS-662014L
                :IND-DFL-IMPB-IS-662014L
               ,:DFL-IS-662014C
                :IND-DFL-IS-662014C
               ,:DFL-IS-662014D
                :IND-DFL-IS-662014D
               ,:DFL-IS-662014L
                :IND-DFL-IS-662014L
             FROM D_FORZ_LIQ
             WHERE     ID_D_FORZ_LIQ = :DFL-ID-D-FORZ-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-DFL-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO DFL-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-DFL-IMP-LRD-CALC = -1
              MOVE HIGH-VALUES TO DFL-IMP-LRD-CALC-NULL
           END-IF
           IF IND-DFL-IMP-LRD-DFZ = -1
              MOVE HIGH-VALUES TO DFL-IMP-LRD-DFZ-NULL
           END-IF
           IF IND-DFL-IMP-LRD-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-IMP-LRD-EFFLQ-NULL
           END-IF
           IF IND-DFL-IMP-NET-CALC = -1
              MOVE HIGH-VALUES TO DFL-IMP-NET-CALC-NULL
           END-IF
           IF IND-DFL-IMP-NET-DFZ = -1
              MOVE HIGH-VALUES TO DFL-IMP-NET-DFZ-NULL
           END-IF
           IF IND-DFL-IMP-NET-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-IMP-NET-EFFLQ-NULL
           END-IF
           IF IND-DFL-IMPST-PRVR-CALC = -1
              MOVE HIGH-VALUES TO DFL-IMPST-PRVR-CALC-NULL
           END-IF
           IF IND-DFL-IMPST-PRVR-DFZ = -1
              MOVE HIGH-VALUES TO DFL-IMPST-PRVR-DFZ-NULL
           END-IF
           IF IND-DFL-IMPST-PRVR-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-IMPST-PRVR-EFFLQ-NULL
           END-IF
           IF IND-DFL-IMPST-VIS-CALC = -1
              MOVE HIGH-VALUES TO DFL-IMPST-VIS-CALC-NULL
           END-IF
           IF IND-DFL-IMPST-VIS-DFZ = -1
              MOVE HIGH-VALUES TO DFL-IMPST-VIS-DFZ-NULL
           END-IF
           IF IND-DFL-IMPST-VIS-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-IMPST-VIS-EFFLQ-NULL
           END-IF
           IF IND-DFL-RIT-ACC-CALC = -1
              MOVE HIGH-VALUES TO DFL-RIT-ACC-CALC-NULL
           END-IF
           IF IND-DFL-RIT-ACC-DFZ = -1
              MOVE HIGH-VALUES TO DFL-RIT-ACC-DFZ-NULL
           END-IF
           IF IND-DFL-RIT-ACC-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-RIT-ACC-EFFLQ-NULL
           END-IF
           IF IND-DFL-RIT-IRPEF-CALC = -1
              MOVE HIGH-VALUES TO DFL-RIT-IRPEF-CALC-NULL
           END-IF
           IF IND-DFL-RIT-IRPEF-DFZ = -1
              MOVE HIGH-VALUES TO DFL-RIT-IRPEF-DFZ-NULL
           END-IF
           IF IND-DFL-RIT-IRPEF-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-RIT-IRPEF-EFFLQ-NULL
           END-IF
           IF IND-DFL-IMPST-SOST-CALC = -1
              MOVE HIGH-VALUES TO DFL-IMPST-SOST-CALC-NULL
           END-IF
           IF IND-DFL-IMPST-SOST-DFZ = -1
              MOVE HIGH-VALUES TO DFL-IMPST-SOST-DFZ-NULL
           END-IF
           IF IND-DFL-IMPST-SOST-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-IMPST-SOST-EFFLQ-NULL
           END-IF
           IF IND-DFL-TAX-SEP-CALC = -1
              MOVE HIGH-VALUES TO DFL-TAX-SEP-CALC-NULL
           END-IF
           IF IND-DFL-TAX-SEP-DFZ = -1
              MOVE HIGH-VALUES TO DFL-TAX-SEP-DFZ-NULL
           END-IF
           IF IND-DFL-TAX-SEP-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-TAX-SEP-EFFLQ-NULL
           END-IF
           IF IND-DFL-INTR-PREST-CALC = -1
              MOVE HIGH-VALUES TO DFL-INTR-PREST-CALC-NULL
           END-IF
           IF IND-DFL-INTR-PREST-DFZ = -1
              MOVE HIGH-VALUES TO DFL-INTR-PREST-DFZ-NULL
           END-IF
           IF IND-DFL-INTR-PREST-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-INTR-PREST-EFFLQ-NULL
           END-IF
           IF IND-DFL-ACCPRE-SOST-CALC = -1
              MOVE HIGH-VALUES TO DFL-ACCPRE-SOST-CALC-NULL
           END-IF
           IF IND-DFL-ACCPRE-SOST-DFZ = -1
              MOVE HIGH-VALUES TO DFL-ACCPRE-SOST-DFZ-NULL
           END-IF
           IF IND-DFL-ACCPRE-SOST-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-ACCPRE-SOST-EFFLQ-NULL
           END-IF
           IF IND-DFL-ACCPRE-VIS-CALC = -1
              MOVE HIGH-VALUES TO DFL-ACCPRE-VIS-CALC-NULL
           END-IF
           IF IND-DFL-ACCPRE-VIS-DFZ = -1
              MOVE HIGH-VALUES TO DFL-ACCPRE-VIS-DFZ-NULL
           END-IF
           IF IND-DFL-ACCPRE-VIS-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-ACCPRE-VIS-EFFLQ-NULL
           END-IF
           IF IND-DFL-ACCPRE-ACC-CALC = -1
              MOVE HIGH-VALUES TO DFL-ACCPRE-ACC-CALC-NULL
           END-IF
           IF IND-DFL-ACCPRE-ACC-DFZ = -1
              MOVE HIGH-VALUES TO DFL-ACCPRE-ACC-DFZ-NULL
           END-IF
           IF IND-DFL-ACCPRE-ACC-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-ACCPRE-ACC-EFFLQ-NULL
           END-IF
           IF IND-DFL-RES-PRSTZ-CALC = -1
              MOVE HIGH-VALUES TO DFL-RES-PRSTZ-CALC-NULL
           END-IF
           IF IND-DFL-RES-PRSTZ-DFZ = -1
              MOVE HIGH-VALUES TO DFL-RES-PRSTZ-DFZ-NULL
           END-IF
           IF IND-DFL-RES-PRSTZ-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-RES-PRSTZ-EFFLQ-NULL
           END-IF
           IF IND-DFL-RES-PRE-ATT-CALC = -1
              MOVE HIGH-VALUES TO DFL-RES-PRE-ATT-CALC-NULL
           END-IF
           IF IND-DFL-RES-PRE-ATT-DFZ = -1
              MOVE HIGH-VALUES TO DFL-RES-PRE-ATT-DFZ-NULL
           END-IF
           IF IND-DFL-RES-PRE-ATT-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-RES-PRE-ATT-EFFLQ-NULL
           END-IF
           IF IND-DFL-IMP-EXCONTR-EFF = -1
              MOVE HIGH-VALUES TO DFL-IMP-EXCONTR-EFF-NULL
           END-IF
           IF IND-DFL-IMPB-VIS-CALC = -1
              MOVE HIGH-VALUES TO DFL-IMPB-VIS-CALC-NULL
           END-IF
           IF IND-DFL-IMPB-VIS-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-IMPB-VIS-EFFLQ-NULL
           END-IF
           IF IND-DFL-IMPB-VIS-DFZ = -1
              MOVE HIGH-VALUES TO DFL-IMPB-VIS-DFZ-NULL
           END-IF
           IF IND-DFL-IMPB-RIT-ACC-CALC = -1
              MOVE HIGH-VALUES TO DFL-IMPB-RIT-ACC-CALC-NULL
           END-IF
           IF IND-DFL-IMPB-RIT-ACC-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-IMPB-RIT-ACC-EFFLQ-NULL
           END-IF
           IF IND-DFL-IMPB-RIT-ACC-DFZ = -1
              MOVE HIGH-VALUES TO DFL-IMPB-RIT-ACC-DFZ-NULL
           END-IF
           IF IND-DFL-IMPB-TFR-CALC = -1
              MOVE HIGH-VALUES TO DFL-IMPB-TFR-CALC-NULL
           END-IF
           IF IND-DFL-IMPB-TFR-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-IMPB-TFR-EFFLQ-NULL
           END-IF
           IF IND-DFL-IMPB-TFR-DFZ = -1
              MOVE HIGH-VALUES TO DFL-IMPB-TFR-DFZ-NULL
           END-IF
           IF IND-DFL-IMPB-IS-CALC = -1
              MOVE HIGH-VALUES TO DFL-IMPB-IS-CALC-NULL
           END-IF
           IF IND-DFL-IMPB-IS-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-IMPB-IS-EFFLQ-NULL
           END-IF
           IF IND-DFL-IMPB-IS-DFZ = -1
              MOVE HIGH-VALUES TO DFL-IMPB-IS-DFZ-NULL
           END-IF
           IF IND-DFL-IMPB-TAX-SEP-CALC = -1
              MOVE HIGH-VALUES TO DFL-IMPB-TAX-SEP-CALC-NULL
           END-IF
           IF IND-DFL-IMPB-TAX-SEP-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-IMPB-TAX-SEP-EFFLQ-NULL
           END-IF
           IF IND-DFL-IMPB-TAX-SEP-DFZ = -1
              MOVE HIGH-VALUES TO DFL-IMPB-TAX-SEP-DFZ-NULL
           END-IF
           IF IND-DFL-IINT-PREST-CALC = -1
              MOVE HIGH-VALUES TO DFL-IINT-PREST-CALC-NULL
           END-IF
           IF IND-DFL-IINT-PREST-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-IINT-PREST-EFFLQ-NULL
           END-IF
           IF IND-DFL-IINT-PREST-DFZ = -1
              MOVE HIGH-VALUES TO DFL-IINT-PREST-DFZ-NULL
           END-IF
           IF IND-DFL-MONT-END2000-CALC = -1
              MOVE HIGH-VALUES TO DFL-MONT-END2000-CALC-NULL
           END-IF
           IF IND-DFL-MONT-END2000-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-MONT-END2000-EFFLQ-NULL
           END-IF
           IF IND-DFL-MONT-END2000-DFZ = -1
              MOVE HIGH-VALUES TO DFL-MONT-END2000-DFZ-NULL
           END-IF
           IF IND-DFL-MONT-END2006-CALC = -1
              MOVE HIGH-VALUES TO DFL-MONT-END2006-CALC-NULL
           END-IF
           IF IND-DFL-MONT-END2006-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-MONT-END2006-EFFLQ-NULL
           END-IF
           IF IND-DFL-MONT-END2006-DFZ = -1
              MOVE HIGH-VALUES TO DFL-MONT-END2006-DFZ-NULL
           END-IF
           IF IND-DFL-MONT-DAL2007-CALC = -1
              MOVE HIGH-VALUES TO DFL-MONT-DAL2007-CALC-NULL
           END-IF
           IF IND-DFL-MONT-DAL2007-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-MONT-DAL2007-EFFLQ-NULL
           END-IF
           IF IND-DFL-MONT-DAL2007-DFZ = -1
              MOVE HIGH-VALUES TO DFL-MONT-DAL2007-DFZ-NULL
           END-IF
           IF IND-DFL-IIMPST-PRVR-CALC = -1
              MOVE HIGH-VALUES TO DFL-IIMPST-PRVR-CALC-NULL
           END-IF
           IF IND-DFL-IIMPST-PRVR-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-IIMPST-PRVR-EFFLQ-NULL
           END-IF
           IF IND-DFL-IIMPST-PRVR-DFZ = -1
              MOVE HIGH-VALUES TO DFL-IIMPST-PRVR-DFZ-NULL
           END-IF
           IF IND-DFL-IIMPST-252-CALC = -1
              MOVE HIGH-VALUES TO DFL-IIMPST-252-CALC-NULL
           END-IF
           IF IND-DFL-IIMPST-252-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-IIMPST-252-EFFLQ-NULL
           END-IF
           IF IND-DFL-IIMPST-252-DFZ = -1
              MOVE HIGH-VALUES TO DFL-IIMPST-252-DFZ-NULL
           END-IF
           IF IND-DFL-IMPST-252-CALC = -1
              MOVE HIGH-VALUES TO DFL-IMPST-252-CALC-NULL
           END-IF
           IF IND-DFL-IMPST-252-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-IMPST-252-EFFLQ-NULL
           END-IF
           IF IND-DFL-RIT-TFR-CALC = -1
              MOVE HIGH-VALUES TO DFL-RIT-TFR-CALC-NULL
           END-IF
           IF IND-DFL-RIT-TFR-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-RIT-TFR-EFFLQ-NULL
           END-IF
           IF IND-DFL-RIT-TFR-DFZ = -1
              MOVE HIGH-VALUES TO DFL-RIT-TFR-DFZ-NULL
           END-IF
           IF IND-DFL-CNBT-INPSTFM-CALC = -1
              MOVE HIGH-VALUES TO DFL-CNBT-INPSTFM-CALC-NULL
           END-IF
           IF IND-DFL-CNBT-INPSTFM-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-CNBT-INPSTFM-EFFLQ-NULL
           END-IF
           IF IND-DFL-CNBT-INPSTFM-DFZ = -1
              MOVE HIGH-VALUES TO DFL-CNBT-INPSTFM-DFZ-NULL
           END-IF
           IF IND-DFL-ICNB-INPSTFM-CALC = -1
              MOVE HIGH-VALUES TO DFL-ICNB-INPSTFM-CALC-NULL
           END-IF
           IF IND-DFL-ICNB-INPSTFM-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-ICNB-INPSTFM-EFFLQ-NULL
           END-IF
           IF IND-DFL-ICNB-INPSTFM-DFZ = -1
              MOVE HIGH-VALUES TO DFL-ICNB-INPSTFM-DFZ-NULL
           END-IF
           IF IND-DFL-CNDE-END2000-CALC = -1
              MOVE HIGH-VALUES TO DFL-CNDE-END2000-CALC-NULL
           END-IF
           IF IND-DFL-CNDE-END2000-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-CNDE-END2000-EFFLQ-NULL
           END-IF
           IF IND-DFL-CNDE-END2000-DFZ = -1
              MOVE HIGH-VALUES TO DFL-CNDE-END2000-DFZ-NULL
           END-IF
           IF IND-DFL-CNDE-END2006-CALC = -1
              MOVE HIGH-VALUES TO DFL-CNDE-END2006-CALC-NULL
           END-IF
           IF IND-DFL-CNDE-END2006-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-CNDE-END2006-EFFLQ-NULL
           END-IF
           IF IND-DFL-CNDE-END2006-DFZ = -1
              MOVE HIGH-VALUES TO DFL-CNDE-END2006-DFZ-NULL
           END-IF
           IF IND-DFL-CNDE-DAL2007-CALC = -1
              MOVE HIGH-VALUES TO DFL-CNDE-DAL2007-CALC-NULL
           END-IF
           IF IND-DFL-CNDE-DAL2007-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-CNDE-DAL2007-EFFLQ-NULL
           END-IF
           IF IND-DFL-CNDE-DAL2007-DFZ = -1
              MOVE HIGH-VALUES TO DFL-CNDE-DAL2007-DFZ-NULL
           END-IF
           IF IND-DFL-AA-CNBZ-END2000-EF = -1
              MOVE HIGH-VALUES TO DFL-AA-CNBZ-END2000-EF-NULL
           END-IF
           IF IND-DFL-AA-CNBZ-END2006-EF = -1
              MOVE HIGH-VALUES TO DFL-AA-CNBZ-END2006-EF-NULL
           END-IF
           IF IND-DFL-AA-CNBZ-DAL2007-EF = -1
              MOVE HIGH-VALUES TO DFL-AA-CNBZ-DAL2007-EF-NULL
           END-IF
           IF IND-DFL-MM-CNBZ-END2000-EF = -1
              MOVE HIGH-VALUES TO DFL-MM-CNBZ-END2000-EF-NULL
           END-IF
           IF IND-DFL-MM-CNBZ-END2006-EF = -1
              MOVE HIGH-VALUES TO DFL-MM-CNBZ-END2006-EF-NULL
           END-IF
           IF IND-DFL-MM-CNBZ-DAL2007-EF = -1
              MOVE HIGH-VALUES TO DFL-MM-CNBZ-DAL2007-EF-NULL
           END-IF
           IF IND-DFL-IMPST-DA-RIMB-EFF = -1
              MOVE HIGH-VALUES TO DFL-IMPST-DA-RIMB-EFF-NULL
           END-IF
           IF IND-DFL-ALQ-TAX-SEP-CALC = -1
              MOVE HIGH-VALUES TO DFL-ALQ-TAX-SEP-CALC-NULL
           END-IF
           IF IND-DFL-ALQ-TAX-SEP-EFFLQ = -1
              MOVE HIGH-VALUES TO DFL-ALQ-TAX-SEP-EFFLQ-NULL
           END-IF
           IF IND-DFL-ALQ-TAX-SEP-DFZ = -1
              MOVE HIGH-VALUES TO DFL-ALQ-TAX-SEP-DFZ-NULL
           END-IF
           IF IND-DFL-ALQ-CNBT-INPSTFM-C = -1
              MOVE HIGH-VALUES TO DFL-ALQ-CNBT-INPSTFM-C-NULL
           END-IF
           IF IND-DFL-ALQ-CNBT-INPSTFM-E = -1
              MOVE HIGH-VALUES TO DFL-ALQ-CNBT-INPSTFM-E-NULL
           END-IF
           IF IND-DFL-ALQ-CNBT-INPSTFM-D = -1
              MOVE HIGH-VALUES TO DFL-ALQ-CNBT-INPSTFM-D-NULL
           END-IF
           IF IND-DFL-IMPB-VIS-1382011C = -1
              MOVE HIGH-VALUES TO DFL-IMPB-VIS-1382011C-NULL
           END-IF
           IF IND-DFL-IMPB-VIS-1382011D = -1
              MOVE HIGH-VALUES TO DFL-IMPB-VIS-1382011D-NULL
           END-IF
           IF IND-DFL-IMPB-VIS-1382011L = -1
              MOVE HIGH-VALUES TO DFL-IMPB-VIS-1382011L-NULL
           END-IF
           IF IND-DFL-IMPST-VIS-1382011C = -1
              MOVE HIGH-VALUES TO DFL-IMPST-VIS-1382011C-NULL
           END-IF
           IF IND-DFL-IMPST-VIS-1382011D = -1
              MOVE HIGH-VALUES TO DFL-IMPST-VIS-1382011D-NULL
           END-IF
           IF IND-DFL-IMPST-VIS-1382011L = -1
              MOVE HIGH-VALUES TO DFL-IMPST-VIS-1382011L-NULL
           END-IF
           IF IND-DFL-IMPB-IS-1382011C = -1
              MOVE HIGH-VALUES TO DFL-IMPB-IS-1382011C-NULL
           END-IF
           IF IND-DFL-IMPB-IS-1382011D = -1
              MOVE HIGH-VALUES TO DFL-IMPB-IS-1382011D-NULL
           END-IF
           IF IND-DFL-IMPB-IS-1382011L = -1
              MOVE HIGH-VALUES TO DFL-IMPB-IS-1382011L-NULL
           END-IF
           IF IND-DFL-IS-1382011C = -1
              MOVE HIGH-VALUES TO DFL-IS-1382011C-NULL
           END-IF
           IF IND-DFL-IS-1382011D = -1
              MOVE HIGH-VALUES TO DFL-IS-1382011D-NULL
           END-IF
           IF IND-DFL-IS-1382011L = -1
              MOVE HIGH-VALUES TO DFL-IS-1382011L-NULL
           END-IF
           IF IND-DFL-IMP-INTR-RIT-PAG-C = -1
              MOVE HIGH-VALUES TO DFL-IMP-INTR-RIT-PAG-C-NULL
           END-IF
           IF IND-DFL-IMP-INTR-RIT-PAG-D = -1
              MOVE HIGH-VALUES TO DFL-IMP-INTR-RIT-PAG-D-NULL
           END-IF
           IF IND-DFL-IMP-INTR-RIT-PAG-L = -1
              MOVE HIGH-VALUES TO DFL-IMP-INTR-RIT-PAG-L-NULL
           END-IF
           IF IND-DFL-IMPB-BOLLO-DETT-C = -1
              MOVE HIGH-VALUES TO DFL-IMPB-BOLLO-DETT-C-NULL
           END-IF
           IF IND-DFL-IMPB-BOLLO-DETT-D = -1
              MOVE HIGH-VALUES TO DFL-IMPB-BOLLO-DETT-D-NULL
           END-IF
           IF IND-DFL-IMPB-BOLLO-DETT-L = -1
              MOVE HIGH-VALUES TO DFL-IMPB-BOLLO-DETT-L-NULL
           END-IF
           IF IND-DFL-IMPST-BOLLO-DETT-C = -1
              MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-DETT-C-NULL
           END-IF
           IF IND-DFL-IMPST-BOLLO-DETT-D = -1
              MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-DETT-D-NULL
           END-IF
           IF IND-DFL-IMPST-BOLLO-DETT-L = -1
              MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-DETT-L-NULL
           END-IF
           IF IND-DFL-IMPST-BOLLO-TOT-VC = -1
              MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-TOT-VC-NULL
           END-IF
           IF IND-DFL-IMPST-BOLLO-TOT-VD = -1
              MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-TOT-VD-NULL
           END-IF
           IF IND-DFL-IMPST-BOLLO-TOT-VL = -1
              MOVE HIGH-VALUES TO DFL-IMPST-BOLLO-TOT-VL-NULL
           END-IF
           IF IND-DFL-IMPB-VIS-662014C = -1
              MOVE HIGH-VALUES TO DFL-IMPB-VIS-662014C-NULL
           END-IF
           IF IND-DFL-IMPB-VIS-662014D = -1
              MOVE HIGH-VALUES TO DFL-IMPB-VIS-662014D-NULL
           END-IF
           IF IND-DFL-IMPB-VIS-662014L = -1
              MOVE HIGH-VALUES TO DFL-IMPB-VIS-662014L-NULL
           END-IF
           IF IND-DFL-IMPST-VIS-662014C = -1
              MOVE HIGH-VALUES TO DFL-IMPST-VIS-662014C-NULL
           END-IF
           IF IND-DFL-IMPST-VIS-662014D = -1
              MOVE HIGH-VALUES TO DFL-IMPST-VIS-662014D-NULL
           END-IF
           IF IND-DFL-IMPST-VIS-662014L = -1
              MOVE HIGH-VALUES TO DFL-IMPST-VIS-662014L-NULL
           END-IF
           IF IND-DFL-IMPB-IS-662014C = -1
              MOVE HIGH-VALUES TO DFL-IMPB-IS-662014C-NULL
           END-IF
           IF IND-DFL-IMPB-IS-662014D = -1
              MOVE HIGH-VALUES TO DFL-IMPB-IS-662014D-NULL
           END-IF
           IF IND-DFL-IMPB-IS-662014L = -1
              MOVE HIGH-VALUES TO DFL-IMPB-IS-662014L-NULL
           END-IF
           IF IND-DFL-IS-662014C = -1
              MOVE HIGH-VALUES TO DFL-IS-662014C-NULL
           END-IF
           IF IND-DFL-IS-662014D = -1
              MOVE HIGH-VALUES TO DFL-IS-662014D-NULL
           END-IF
           IF IND-DFL-IS-662014L = -1
              MOVE HIGH-VALUES TO DFL-IS-662014L-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO DFL-DS-OPER-SQL
           MOVE 0                   TO DFL-DS-VER
           MOVE IDSV0003-USER-NAME TO DFL-DS-UTENTE
           MOVE '1'                   TO DFL-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO DFL-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO DFL-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF DFL-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-DFL-ID-MOVI-CHIU
           END-IF
           IF DFL-IMP-LRD-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMP-LRD-CALC
           ELSE
              MOVE 0 TO IND-DFL-IMP-LRD-CALC
           END-IF
           IF DFL-IMP-LRD-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMP-LRD-DFZ
           ELSE
              MOVE 0 TO IND-DFL-IMP-LRD-DFZ
           END-IF
           IF DFL-IMP-LRD-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMP-LRD-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-IMP-LRD-EFFLQ
           END-IF
           IF DFL-IMP-NET-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMP-NET-CALC
           ELSE
              MOVE 0 TO IND-DFL-IMP-NET-CALC
           END-IF
           IF DFL-IMP-NET-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMP-NET-DFZ
           ELSE
              MOVE 0 TO IND-DFL-IMP-NET-DFZ
           END-IF
           IF DFL-IMP-NET-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMP-NET-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-IMP-NET-EFFLQ
           END-IF
           IF DFL-IMPST-PRVR-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-PRVR-CALC
           ELSE
              MOVE 0 TO IND-DFL-IMPST-PRVR-CALC
           END-IF
           IF DFL-IMPST-PRVR-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-PRVR-DFZ
           ELSE
              MOVE 0 TO IND-DFL-IMPST-PRVR-DFZ
           END-IF
           IF DFL-IMPST-PRVR-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-PRVR-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-IMPST-PRVR-EFFLQ
           END-IF
           IF DFL-IMPST-VIS-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-VIS-CALC
           ELSE
              MOVE 0 TO IND-DFL-IMPST-VIS-CALC
           END-IF
           IF DFL-IMPST-VIS-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-VIS-DFZ
           ELSE
              MOVE 0 TO IND-DFL-IMPST-VIS-DFZ
           END-IF
           IF DFL-IMPST-VIS-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-VIS-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-IMPST-VIS-EFFLQ
           END-IF
           IF DFL-RIT-ACC-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-RIT-ACC-CALC
           ELSE
              MOVE 0 TO IND-DFL-RIT-ACC-CALC
           END-IF
           IF DFL-RIT-ACC-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-RIT-ACC-DFZ
           ELSE
              MOVE 0 TO IND-DFL-RIT-ACC-DFZ
           END-IF
           IF DFL-RIT-ACC-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-RIT-ACC-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-RIT-ACC-EFFLQ
           END-IF
           IF DFL-RIT-IRPEF-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-RIT-IRPEF-CALC
           ELSE
              MOVE 0 TO IND-DFL-RIT-IRPEF-CALC
           END-IF
           IF DFL-RIT-IRPEF-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-RIT-IRPEF-DFZ
           ELSE
              MOVE 0 TO IND-DFL-RIT-IRPEF-DFZ
           END-IF
           IF DFL-RIT-IRPEF-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-RIT-IRPEF-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-RIT-IRPEF-EFFLQ
           END-IF
           IF DFL-IMPST-SOST-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-SOST-CALC
           ELSE
              MOVE 0 TO IND-DFL-IMPST-SOST-CALC
           END-IF
           IF DFL-IMPST-SOST-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-SOST-DFZ
           ELSE
              MOVE 0 TO IND-DFL-IMPST-SOST-DFZ
           END-IF
           IF DFL-IMPST-SOST-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-SOST-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-IMPST-SOST-EFFLQ
           END-IF
           IF DFL-TAX-SEP-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-TAX-SEP-CALC
           ELSE
              MOVE 0 TO IND-DFL-TAX-SEP-CALC
           END-IF
           IF DFL-TAX-SEP-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-TAX-SEP-DFZ
           ELSE
              MOVE 0 TO IND-DFL-TAX-SEP-DFZ
           END-IF
           IF DFL-TAX-SEP-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-TAX-SEP-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-TAX-SEP-EFFLQ
           END-IF
           IF DFL-INTR-PREST-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-INTR-PREST-CALC
           ELSE
              MOVE 0 TO IND-DFL-INTR-PREST-CALC
           END-IF
           IF DFL-INTR-PREST-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-INTR-PREST-DFZ
           ELSE
              MOVE 0 TO IND-DFL-INTR-PREST-DFZ
           END-IF
           IF DFL-INTR-PREST-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-INTR-PREST-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-INTR-PREST-EFFLQ
           END-IF
           IF DFL-ACCPRE-SOST-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ACCPRE-SOST-CALC
           ELSE
              MOVE 0 TO IND-DFL-ACCPRE-SOST-CALC
           END-IF
           IF DFL-ACCPRE-SOST-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ACCPRE-SOST-DFZ
           ELSE
              MOVE 0 TO IND-DFL-ACCPRE-SOST-DFZ
           END-IF
           IF DFL-ACCPRE-SOST-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ACCPRE-SOST-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-ACCPRE-SOST-EFFLQ
           END-IF
           IF DFL-ACCPRE-VIS-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ACCPRE-VIS-CALC
           ELSE
              MOVE 0 TO IND-DFL-ACCPRE-VIS-CALC
           END-IF
           IF DFL-ACCPRE-VIS-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ACCPRE-VIS-DFZ
           ELSE
              MOVE 0 TO IND-DFL-ACCPRE-VIS-DFZ
           END-IF
           IF DFL-ACCPRE-VIS-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ACCPRE-VIS-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-ACCPRE-VIS-EFFLQ
           END-IF
           IF DFL-ACCPRE-ACC-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ACCPRE-ACC-CALC
           ELSE
              MOVE 0 TO IND-DFL-ACCPRE-ACC-CALC
           END-IF
           IF DFL-ACCPRE-ACC-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ACCPRE-ACC-DFZ
           ELSE
              MOVE 0 TO IND-DFL-ACCPRE-ACC-DFZ
           END-IF
           IF DFL-ACCPRE-ACC-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ACCPRE-ACC-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-ACCPRE-ACC-EFFLQ
           END-IF
           IF DFL-RES-PRSTZ-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-RES-PRSTZ-CALC
           ELSE
              MOVE 0 TO IND-DFL-RES-PRSTZ-CALC
           END-IF
           IF DFL-RES-PRSTZ-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-RES-PRSTZ-DFZ
           ELSE
              MOVE 0 TO IND-DFL-RES-PRSTZ-DFZ
           END-IF
           IF DFL-RES-PRSTZ-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-RES-PRSTZ-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-RES-PRSTZ-EFFLQ
           END-IF
           IF DFL-RES-PRE-ATT-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-RES-PRE-ATT-CALC
           ELSE
              MOVE 0 TO IND-DFL-RES-PRE-ATT-CALC
           END-IF
           IF DFL-RES-PRE-ATT-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-RES-PRE-ATT-DFZ
           ELSE
              MOVE 0 TO IND-DFL-RES-PRE-ATT-DFZ
           END-IF
           IF DFL-RES-PRE-ATT-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-RES-PRE-ATT-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-RES-PRE-ATT-EFFLQ
           END-IF
           IF DFL-IMP-EXCONTR-EFF-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMP-EXCONTR-EFF
           ELSE
              MOVE 0 TO IND-DFL-IMP-EXCONTR-EFF
           END-IF
           IF DFL-IMPB-VIS-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-VIS-CALC
           ELSE
              MOVE 0 TO IND-DFL-IMPB-VIS-CALC
           END-IF
           IF DFL-IMPB-VIS-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-VIS-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-IMPB-VIS-EFFLQ
           END-IF
           IF DFL-IMPB-VIS-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-VIS-DFZ
           ELSE
              MOVE 0 TO IND-DFL-IMPB-VIS-DFZ
           END-IF
           IF DFL-IMPB-RIT-ACC-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-RIT-ACC-CALC
           ELSE
              MOVE 0 TO IND-DFL-IMPB-RIT-ACC-CALC
           END-IF
           IF DFL-IMPB-RIT-ACC-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-RIT-ACC-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-IMPB-RIT-ACC-EFFLQ
           END-IF
           IF DFL-IMPB-RIT-ACC-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-RIT-ACC-DFZ
           ELSE
              MOVE 0 TO IND-DFL-IMPB-RIT-ACC-DFZ
           END-IF
           IF DFL-IMPB-TFR-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-TFR-CALC
           ELSE
              MOVE 0 TO IND-DFL-IMPB-TFR-CALC
           END-IF
           IF DFL-IMPB-TFR-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-TFR-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-IMPB-TFR-EFFLQ
           END-IF
           IF DFL-IMPB-TFR-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-TFR-DFZ
           ELSE
              MOVE 0 TO IND-DFL-IMPB-TFR-DFZ
           END-IF
           IF DFL-IMPB-IS-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-IS-CALC
           ELSE
              MOVE 0 TO IND-DFL-IMPB-IS-CALC
           END-IF
           IF DFL-IMPB-IS-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-IS-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-IMPB-IS-EFFLQ
           END-IF
           IF DFL-IMPB-IS-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-IS-DFZ
           ELSE
              MOVE 0 TO IND-DFL-IMPB-IS-DFZ
           END-IF
           IF DFL-IMPB-TAX-SEP-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-TAX-SEP-CALC
           ELSE
              MOVE 0 TO IND-DFL-IMPB-TAX-SEP-CALC
           END-IF
           IF DFL-IMPB-TAX-SEP-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-TAX-SEP-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-IMPB-TAX-SEP-EFFLQ
           END-IF
           IF DFL-IMPB-TAX-SEP-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-TAX-SEP-DFZ
           ELSE
              MOVE 0 TO IND-DFL-IMPB-TAX-SEP-DFZ
           END-IF
           IF DFL-IINT-PREST-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IINT-PREST-CALC
           ELSE
              MOVE 0 TO IND-DFL-IINT-PREST-CALC
           END-IF
           IF DFL-IINT-PREST-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IINT-PREST-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-IINT-PREST-EFFLQ
           END-IF
           IF DFL-IINT-PREST-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IINT-PREST-DFZ
           ELSE
              MOVE 0 TO IND-DFL-IINT-PREST-DFZ
           END-IF
           IF DFL-MONT-END2000-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-MONT-END2000-CALC
           ELSE
              MOVE 0 TO IND-DFL-MONT-END2000-CALC
           END-IF
           IF DFL-MONT-END2000-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-MONT-END2000-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-MONT-END2000-EFFLQ
           END-IF
           IF DFL-MONT-END2000-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-MONT-END2000-DFZ
           ELSE
              MOVE 0 TO IND-DFL-MONT-END2000-DFZ
           END-IF
           IF DFL-MONT-END2006-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-MONT-END2006-CALC
           ELSE
              MOVE 0 TO IND-DFL-MONT-END2006-CALC
           END-IF
           IF DFL-MONT-END2006-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-MONT-END2006-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-MONT-END2006-EFFLQ
           END-IF
           IF DFL-MONT-END2006-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-MONT-END2006-DFZ
           ELSE
              MOVE 0 TO IND-DFL-MONT-END2006-DFZ
           END-IF
           IF DFL-MONT-DAL2007-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-MONT-DAL2007-CALC
           ELSE
              MOVE 0 TO IND-DFL-MONT-DAL2007-CALC
           END-IF
           IF DFL-MONT-DAL2007-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-MONT-DAL2007-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-MONT-DAL2007-EFFLQ
           END-IF
           IF DFL-MONT-DAL2007-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-MONT-DAL2007-DFZ
           ELSE
              MOVE 0 TO IND-DFL-MONT-DAL2007-DFZ
           END-IF
           IF DFL-IIMPST-PRVR-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IIMPST-PRVR-CALC
           ELSE
              MOVE 0 TO IND-DFL-IIMPST-PRVR-CALC
           END-IF
           IF DFL-IIMPST-PRVR-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IIMPST-PRVR-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-IIMPST-PRVR-EFFLQ
           END-IF
           IF DFL-IIMPST-PRVR-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IIMPST-PRVR-DFZ
           ELSE
              MOVE 0 TO IND-DFL-IIMPST-PRVR-DFZ
           END-IF
           IF DFL-IIMPST-252-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IIMPST-252-CALC
           ELSE
              MOVE 0 TO IND-DFL-IIMPST-252-CALC
           END-IF
           IF DFL-IIMPST-252-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IIMPST-252-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-IIMPST-252-EFFLQ
           END-IF
           IF DFL-IIMPST-252-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IIMPST-252-DFZ
           ELSE
              MOVE 0 TO IND-DFL-IIMPST-252-DFZ
           END-IF
           IF DFL-IMPST-252-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-252-CALC
           ELSE
              MOVE 0 TO IND-DFL-IMPST-252-CALC
           END-IF
           IF DFL-IMPST-252-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-252-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-IMPST-252-EFFLQ
           END-IF
           IF DFL-RIT-TFR-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-RIT-TFR-CALC
           ELSE
              MOVE 0 TO IND-DFL-RIT-TFR-CALC
           END-IF
           IF DFL-RIT-TFR-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-RIT-TFR-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-RIT-TFR-EFFLQ
           END-IF
           IF DFL-RIT-TFR-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-RIT-TFR-DFZ
           ELSE
              MOVE 0 TO IND-DFL-RIT-TFR-DFZ
           END-IF
           IF DFL-CNBT-INPSTFM-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-CNBT-INPSTFM-CALC
           ELSE
              MOVE 0 TO IND-DFL-CNBT-INPSTFM-CALC
           END-IF
           IF DFL-CNBT-INPSTFM-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-CNBT-INPSTFM-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-CNBT-INPSTFM-EFFLQ
           END-IF
           IF DFL-CNBT-INPSTFM-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-CNBT-INPSTFM-DFZ
           ELSE
              MOVE 0 TO IND-DFL-CNBT-INPSTFM-DFZ
           END-IF
           IF DFL-ICNB-INPSTFM-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ICNB-INPSTFM-CALC
           ELSE
              MOVE 0 TO IND-DFL-ICNB-INPSTFM-CALC
           END-IF
           IF DFL-ICNB-INPSTFM-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ICNB-INPSTFM-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-ICNB-INPSTFM-EFFLQ
           END-IF
           IF DFL-ICNB-INPSTFM-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ICNB-INPSTFM-DFZ
           ELSE
              MOVE 0 TO IND-DFL-ICNB-INPSTFM-DFZ
           END-IF
           IF DFL-CNDE-END2000-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-CNDE-END2000-CALC
           ELSE
              MOVE 0 TO IND-DFL-CNDE-END2000-CALC
           END-IF
           IF DFL-CNDE-END2000-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-CNDE-END2000-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-CNDE-END2000-EFFLQ
           END-IF
           IF DFL-CNDE-END2000-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-CNDE-END2000-DFZ
           ELSE
              MOVE 0 TO IND-DFL-CNDE-END2000-DFZ
           END-IF
           IF DFL-CNDE-END2006-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-CNDE-END2006-CALC
           ELSE
              MOVE 0 TO IND-DFL-CNDE-END2006-CALC
           END-IF
           IF DFL-CNDE-END2006-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-CNDE-END2006-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-CNDE-END2006-EFFLQ
           END-IF
           IF DFL-CNDE-END2006-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-CNDE-END2006-DFZ
           ELSE
              MOVE 0 TO IND-DFL-CNDE-END2006-DFZ
           END-IF
           IF DFL-CNDE-DAL2007-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-CNDE-DAL2007-CALC
           ELSE
              MOVE 0 TO IND-DFL-CNDE-DAL2007-CALC
           END-IF
           IF DFL-CNDE-DAL2007-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-CNDE-DAL2007-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-CNDE-DAL2007-EFFLQ
           END-IF
           IF DFL-CNDE-DAL2007-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-CNDE-DAL2007-DFZ
           ELSE
              MOVE 0 TO IND-DFL-CNDE-DAL2007-DFZ
           END-IF
           IF DFL-AA-CNBZ-END2000-EF-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-AA-CNBZ-END2000-EF
           ELSE
              MOVE 0 TO IND-DFL-AA-CNBZ-END2000-EF
           END-IF
           IF DFL-AA-CNBZ-END2006-EF-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-AA-CNBZ-END2006-EF
           ELSE
              MOVE 0 TO IND-DFL-AA-CNBZ-END2006-EF
           END-IF
           IF DFL-AA-CNBZ-DAL2007-EF-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-AA-CNBZ-DAL2007-EF
           ELSE
              MOVE 0 TO IND-DFL-AA-CNBZ-DAL2007-EF
           END-IF
           IF DFL-MM-CNBZ-END2000-EF-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-MM-CNBZ-END2000-EF
           ELSE
              MOVE 0 TO IND-DFL-MM-CNBZ-END2000-EF
           END-IF
           IF DFL-MM-CNBZ-END2006-EF-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-MM-CNBZ-END2006-EF
           ELSE
              MOVE 0 TO IND-DFL-MM-CNBZ-END2006-EF
           END-IF
           IF DFL-MM-CNBZ-DAL2007-EF-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-MM-CNBZ-DAL2007-EF
           ELSE
              MOVE 0 TO IND-DFL-MM-CNBZ-DAL2007-EF
           END-IF
           IF DFL-IMPST-DA-RIMB-EFF-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-DA-RIMB-EFF
           ELSE
              MOVE 0 TO IND-DFL-IMPST-DA-RIMB-EFF
           END-IF
           IF DFL-ALQ-TAX-SEP-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ALQ-TAX-SEP-CALC
           ELSE
              MOVE 0 TO IND-DFL-ALQ-TAX-SEP-CALC
           END-IF
           IF DFL-ALQ-TAX-SEP-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ALQ-TAX-SEP-EFFLQ
           ELSE
              MOVE 0 TO IND-DFL-ALQ-TAX-SEP-EFFLQ
           END-IF
           IF DFL-ALQ-TAX-SEP-DFZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ALQ-TAX-SEP-DFZ
           ELSE
              MOVE 0 TO IND-DFL-ALQ-TAX-SEP-DFZ
           END-IF
           IF DFL-ALQ-CNBT-INPSTFM-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ALQ-CNBT-INPSTFM-C
           ELSE
              MOVE 0 TO IND-DFL-ALQ-CNBT-INPSTFM-C
           END-IF
           IF DFL-ALQ-CNBT-INPSTFM-E-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ALQ-CNBT-INPSTFM-E
           ELSE
              MOVE 0 TO IND-DFL-ALQ-CNBT-INPSTFM-E
           END-IF
           IF DFL-ALQ-CNBT-INPSTFM-D-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-ALQ-CNBT-INPSTFM-D
           ELSE
              MOVE 0 TO IND-DFL-ALQ-CNBT-INPSTFM-D
           END-IF
           IF DFL-IMPB-VIS-1382011C-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-VIS-1382011C
           ELSE
              MOVE 0 TO IND-DFL-IMPB-VIS-1382011C
           END-IF
           IF DFL-IMPB-VIS-1382011D-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-VIS-1382011D
           ELSE
              MOVE 0 TO IND-DFL-IMPB-VIS-1382011D
           END-IF
           IF DFL-IMPB-VIS-1382011L-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-VIS-1382011L
           ELSE
              MOVE 0 TO IND-DFL-IMPB-VIS-1382011L
           END-IF
           IF DFL-IMPST-VIS-1382011C-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-VIS-1382011C
           ELSE
              MOVE 0 TO IND-DFL-IMPST-VIS-1382011C
           END-IF
           IF DFL-IMPST-VIS-1382011D-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-VIS-1382011D
           ELSE
              MOVE 0 TO IND-DFL-IMPST-VIS-1382011D
           END-IF
           IF DFL-IMPST-VIS-1382011L-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-VIS-1382011L
           ELSE
              MOVE 0 TO IND-DFL-IMPST-VIS-1382011L
           END-IF
           IF DFL-IMPB-IS-1382011C-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-IS-1382011C
           ELSE
              MOVE 0 TO IND-DFL-IMPB-IS-1382011C
           END-IF
           IF DFL-IMPB-IS-1382011D-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-IS-1382011D
           ELSE
              MOVE 0 TO IND-DFL-IMPB-IS-1382011D
           END-IF
           IF DFL-IMPB-IS-1382011L-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-IS-1382011L
           ELSE
              MOVE 0 TO IND-DFL-IMPB-IS-1382011L
           END-IF
           IF DFL-IS-1382011C-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IS-1382011C
           ELSE
              MOVE 0 TO IND-DFL-IS-1382011C
           END-IF
           IF DFL-IS-1382011D-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IS-1382011D
           ELSE
              MOVE 0 TO IND-DFL-IS-1382011D
           END-IF
           IF DFL-IS-1382011L-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IS-1382011L
           ELSE
              MOVE 0 TO IND-DFL-IS-1382011L
           END-IF
           IF DFL-IMP-INTR-RIT-PAG-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMP-INTR-RIT-PAG-C
           ELSE
              MOVE 0 TO IND-DFL-IMP-INTR-RIT-PAG-C
           END-IF
           IF DFL-IMP-INTR-RIT-PAG-D-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMP-INTR-RIT-PAG-D
           ELSE
              MOVE 0 TO IND-DFL-IMP-INTR-RIT-PAG-D
           END-IF
           IF DFL-IMP-INTR-RIT-PAG-L-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMP-INTR-RIT-PAG-L
           ELSE
              MOVE 0 TO IND-DFL-IMP-INTR-RIT-PAG-L
           END-IF
           IF DFL-IMPB-BOLLO-DETT-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-BOLLO-DETT-C
           ELSE
              MOVE 0 TO IND-DFL-IMPB-BOLLO-DETT-C
           END-IF
           IF DFL-IMPB-BOLLO-DETT-D-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-BOLLO-DETT-D
           ELSE
              MOVE 0 TO IND-DFL-IMPB-BOLLO-DETT-D
           END-IF
           IF DFL-IMPB-BOLLO-DETT-L-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-BOLLO-DETT-L
           ELSE
              MOVE 0 TO IND-DFL-IMPB-BOLLO-DETT-L
           END-IF
           IF DFL-IMPST-BOLLO-DETT-C-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-BOLLO-DETT-C
           ELSE
              MOVE 0 TO IND-DFL-IMPST-BOLLO-DETT-C
           END-IF
           IF DFL-IMPST-BOLLO-DETT-D-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-BOLLO-DETT-D
           ELSE
              MOVE 0 TO IND-DFL-IMPST-BOLLO-DETT-D
           END-IF
           IF DFL-IMPST-BOLLO-DETT-L-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-BOLLO-DETT-L
           ELSE
              MOVE 0 TO IND-DFL-IMPST-BOLLO-DETT-L
           END-IF
           IF DFL-IMPST-BOLLO-TOT-VC-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-BOLLO-TOT-VC
           ELSE
              MOVE 0 TO IND-DFL-IMPST-BOLLO-TOT-VC
           END-IF
           IF DFL-IMPST-BOLLO-TOT-VD-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-BOLLO-TOT-VD
           ELSE
              MOVE 0 TO IND-DFL-IMPST-BOLLO-TOT-VD
           END-IF
           IF DFL-IMPST-BOLLO-TOT-VL-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-BOLLO-TOT-VL
           ELSE
              MOVE 0 TO IND-DFL-IMPST-BOLLO-TOT-VL
           END-IF
           IF DFL-IMPB-VIS-662014C-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-VIS-662014C
           ELSE
              MOVE 0 TO IND-DFL-IMPB-VIS-662014C
           END-IF
           IF DFL-IMPB-VIS-662014D-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-VIS-662014D
           ELSE
              MOVE 0 TO IND-DFL-IMPB-VIS-662014D
           END-IF
           IF DFL-IMPB-VIS-662014L-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-VIS-662014L
           ELSE
              MOVE 0 TO IND-DFL-IMPB-VIS-662014L
           END-IF
           IF DFL-IMPST-VIS-662014C-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-VIS-662014C
           ELSE
              MOVE 0 TO IND-DFL-IMPST-VIS-662014C
           END-IF
           IF DFL-IMPST-VIS-662014D-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-VIS-662014D
           ELSE
              MOVE 0 TO IND-DFL-IMPST-VIS-662014D
           END-IF
           IF DFL-IMPST-VIS-662014L-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPST-VIS-662014L
           ELSE
              MOVE 0 TO IND-DFL-IMPST-VIS-662014L
           END-IF
           IF DFL-IMPB-IS-662014C-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-IS-662014C
           ELSE
              MOVE 0 TO IND-DFL-IMPB-IS-662014C
           END-IF
           IF DFL-IMPB-IS-662014D-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-IS-662014D
           ELSE
              MOVE 0 TO IND-DFL-IMPB-IS-662014D
           END-IF
           IF DFL-IMPB-IS-662014L-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IMPB-IS-662014L
           ELSE
              MOVE 0 TO IND-DFL-IMPB-IS-662014L
           END-IF
           IF DFL-IS-662014C-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IS-662014C
           ELSE
              MOVE 0 TO IND-DFL-IS-662014C
           END-IF
           IF DFL-IS-662014D-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IS-662014D
           ELSE
              MOVE 0 TO IND-DFL-IS-662014D
           END-IF
           IF DFL-IS-662014L-NULL = HIGH-VALUES
              MOVE -1 TO IND-DFL-IS-662014L
           ELSE
              MOVE 0 TO IND-DFL-IS-662014L
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : DFL-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE D-FORZ-LIQ TO WS-BUFFER-TABLE.

           MOVE DFL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO DFL-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO DFL-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO DFL-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO DFL-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO DFL-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO DFL-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO DFL-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE D-FORZ-LIQ TO WS-BUFFER-TABLE.

           MOVE DFL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO D-FORZ-LIQ.

           MOVE WS-ID-MOVI-CRZ  TO DFL-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO DFL-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO DFL-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO DFL-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO DFL-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO DFL-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO DFL-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE DFL-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO DFL-DT-INI-EFF-DB
           MOVE DFL-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO DFL-DT-END-EFF-DB.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE DFL-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO DFL-DT-INI-EFF
           MOVE DFL-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO DFL-DT-END-EFF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSP670 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  11 MAR 2014.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDP670 END-EXEC.
           EXEC SQL INCLUDE IDBVP672 END-EXEC.
           EXEC SQL INCLUDE IDBVP673 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVP671 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 EST-POLI-CPI-PR.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSP670'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'EST_POLI_CPI_PR' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_EST_POLI_CPI_PR
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,COD_PROD_ESTNO
                ,CPT_FIN
                ,NUM_TST_FIN
                ,TS_FINANZ
                ,DUR_MM_FINANZ
                ,DT_END_FINANZ
                ,AMM_1A_RAT
                ,VAL_RISC_BENE
                ,AMM_RAT_END
                ,TS_CRE_RAT_FINANZ
                ,IMP_FIN_REVOLVING
                ,IMP_UTIL_C_REV
                ,IMP_RAT_REVOLVING
                ,DT_1O_UTLZ_C_REV
                ,IMP_ASSTO
                ,PRE_VERS
                ,DT_SCAD_COP
                ,GG_DEL_MM_SCAD_RAT
                ,FL_PRE_FIN
                ,DT_SCAD_1A_RAT
                ,DT_EROG_FINANZ
                ,DT_STIPULA_FINANZ
                ,MM_PREAMM
                ,IMP_DEB_RES
                ,IMP_RAT_FINANZ
                ,IMP_CANONE_ANTIC
                ,PER_RAT_FINANZ
                ,TP_MOD_PAG_RAT
                ,TP_FINANZ_ER
                ,CAT_FINANZ_ER
                ,VAL_RISC_END_LEAS
                ,DT_EST_FINANZ
                ,DT_MAN_COP
                ,NUM_FINANZ
                ,TP_MOD_ACQS
             INTO
                :P67-ID-EST-POLI-CPI-PR
               ,:P67-ID-MOVI-CRZ
               ,:P67-ID-MOVI-CHIU
                :IND-P67-ID-MOVI-CHIU
               ,:P67-DT-INI-EFF-DB
               ,:P67-DT-END-EFF-DB
               ,:P67-COD-COMP-ANIA
               ,:P67-IB-OGG
               ,:P67-DS-RIGA
               ,:P67-DS-OPER-SQL
               ,:P67-DS-VER
               ,:P67-DS-TS-INI-CPTZ
               ,:P67-DS-TS-END-CPTZ
               ,:P67-DS-UTENTE
               ,:P67-DS-STATO-ELAB
               ,:P67-COD-PROD-ESTNO
               ,:P67-CPT-FIN
                :IND-P67-CPT-FIN
               ,:P67-NUM-TST-FIN
                :IND-P67-NUM-TST-FIN
               ,:P67-TS-FINANZ
                :IND-P67-TS-FINANZ
               ,:P67-DUR-MM-FINANZ
                :IND-P67-DUR-MM-FINANZ
               ,:P67-DT-END-FINANZ-DB
                :IND-P67-DT-END-FINANZ
               ,:P67-AMM-1A-RAT
                :IND-P67-AMM-1A-RAT
               ,:P67-VAL-RISC-BENE
                :IND-P67-VAL-RISC-BENE
               ,:P67-AMM-RAT-END
                :IND-P67-AMM-RAT-END
               ,:P67-TS-CRE-RAT-FINANZ
                :IND-P67-TS-CRE-RAT-FINANZ
               ,:P67-IMP-FIN-REVOLVING
                :IND-P67-IMP-FIN-REVOLVING
               ,:P67-IMP-UTIL-C-REV
                :IND-P67-IMP-UTIL-C-REV
               ,:P67-IMP-RAT-REVOLVING
                :IND-P67-IMP-RAT-REVOLVING
               ,:P67-DT-1O-UTLZ-C-REV-DB
                :IND-P67-DT-1O-UTLZ-C-REV
               ,:P67-IMP-ASSTO
                :IND-P67-IMP-ASSTO
               ,:P67-PRE-VERS
                :IND-P67-PRE-VERS
               ,:P67-DT-SCAD-COP-DB
                :IND-P67-DT-SCAD-COP
               ,:P67-GG-DEL-MM-SCAD-RAT
                :IND-P67-GG-DEL-MM-SCAD-RAT
               ,:P67-FL-PRE-FIN
               ,:P67-DT-SCAD-1A-RAT-DB
                :IND-P67-DT-SCAD-1A-RAT
               ,:P67-DT-EROG-FINANZ-DB
                :IND-P67-DT-EROG-FINANZ
               ,:P67-DT-STIPULA-FINANZ-DB
                :IND-P67-DT-STIPULA-FINANZ
               ,:P67-MM-PREAMM
                :IND-P67-MM-PREAMM
               ,:P67-IMP-DEB-RES
                :IND-P67-IMP-DEB-RES
               ,:P67-IMP-RAT-FINANZ
                :IND-P67-IMP-RAT-FINANZ
               ,:P67-IMP-CANONE-ANTIC
                :IND-P67-IMP-CANONE-ANTIC
               ,:P67-PER-RAT-FINANZ
                :IND-P67-PER-RAT-FINANZ
               ,:P67-TP-MOD-PAG-RAT
                :IND-P67-TP-MOD-PAG-RAT
               ,:P67-TP-FINANZ-ER
                :IND-P67-TP-FINANZ-ER
               ,:P67-CAT-FINANZ-ER
                :IND-P67-CAT-FINANZ-ER
               ,:P67-VAL-RISC-END-LEAS
                :IND-P67-VAL-RISC-END-LEAS
               ,:P67-DT-EST-FINANZ-DB
                :IND-P67-DT-EST-FINANZ
               ,:P67-DT-MAN-COP-DB
                :IND-P67-DT-MAN-COP
               ,:P67-NUM-FINANZ
                :IND-P67-NUM-FINANZ
               ,:P67-TP-MOD-ACQS
                :IND-P67-TP-MOD-ACQS
             FROM EST_POLI_CPI_PR
             WHERE     DS_RIGA = :P67-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO EST_POLI_CPI_PR
                     (
                        ID_EST_POLI_CPI_PR
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,IB_OGG
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,COD_PROD_ESTNO
                       ,CPT_FIN
                       ,NUM_TST_FIN
                       ,TS_FINANZ
                       ,DUR_MM_FINANZ
                       ,DT_END_FINANZ
                       ,AMM_1A_RAT
                       ,VAL_RISC_BENE
                       ,AMM_RAT_END
                       ,TS_CRE_RAT_FINANZ
                       ,IMP_FIN_REVOLVING
                       ,IMP_UTIL_C_REV
                       ,IMP_RAT_REVOLVING
                       ,DT_1O_UTLZ_C_REV
                       ,IMP_ASSTO
                       ,PRE_VERS
                       ,DT_SCAD_COP
                       ,GG_DEL_MM_SCAD_RAT
                       ,FL_PRE_FIN
                       ,DT_SCAD_1A_RAT
                       ,DT_EROG_FINANZ
                       ,DT_STIPULA_FINANZ
                       ,MM_PREAMM
                       ,IMP_DEB_RES
                       ,IMP_RAT_FINANZ
                       ,IMP_CANONE_ANTIC
                       ,PER_RAT_FINANZ
                       ,TP_MOD_PAG_RAT
                       ,TP_FINANZ_ER
                       ,CAT_FINANZ_ER
                       ,VAL_RISC_END_LEAS
                       ,DT_EST_FINANZ
                       ,DT_MAN_COP
                       ,NUM_FINANZ
                       ,TP_MOD_ACQS
                     )
                 VALUES
                     (
                       :P67-ID-EST-POLI-CPI-PR
                       ,:P67-ID-MOVI-CRZ
                       ,:P67-ID-MOVI-CHIU
                        :IND-P67-ID-MOVI-CHIU
                       ,:P67-DT-INI-EFF-DB
                       ,:P67-DT-END-EFF-DB
                       ,:P67-COD-COMP-ANIA
                       ,:P67-IB-OGG
                       ,:P67-DS-RIGA
                       ,:P67-DS-OPER-SQL
                       ,:P67-DS-VER
                       ,:P67-DS-TS-INI-CPTZ
                       ,:P67-DS-TS-END-CPTZ
                       ,:P67-DS-UTENTE
                       ,:P67-DS-STATO-ELAB
                       ,:P67-COD-PROD-ESTNO
                       ,:P67-CPT-FIN
                        :IND-P67-CPT-FIN
                       ,:P67-NUM-TST-FIN
                        :IND-P67-NUM-TST-FIN
                       ,:P67-TS-FINANZ
                        :IND-P67-TS-FINANZ
                       ,:P67-DUR-MM-FINANZ
                        :IND-P67-DUR-MM-FINANZ
                       ,:P67-DT-END-FINANZ-DB
                        :IND-P67-DT-END-FINANZ
                       ,:P67-AMM-1A-RAT
                        :IND-P67-AMM-1A-RAT
                       ,:P67-VAL-RISC-BENE
                        :IND-P67-VAL-RISC-BENE
                       ,:P67-AMM-RAT-END
                        :IND-P67-AMM-RAT-END
                       ,:P67-TS-CRE-RAT-FINANZ
                        :IND-P67-TS-CRE-RAT-FINANZ
                       ,:P67-IMP-FIN-REVOLVING
                        :IND-P67-IMP-FIN-REVOLVING
                       ,:P67-IMP-UTIL-C-REV
                        :IND-P67-IMP-UTIL-C-REV
                       ,:P67-IMP-RAT-REVOLVING
                        :IND-P67-IMP-RAT-REVOLVING
                       ,:P67-DT-1O-UTLZ-C-REV-DB
                        :IND-P67-DT-1O-UTLZ-C-REV
                       ,:P67-IMP-ASSTO
                        :IND-P67-IMP-ASSTO
                       ,:P67-PRE-VERS
                        :IND-P67-PRE-VERS
                       ,:P67-DT-SCAD-COP-DB
                        :IND-P67-DT-SCAD-COP
                       ,:P67-GG-DEL-MM-SCAD-RAT
                        :IND-P67-GG-DEL-MM-SCAD-RAT
                       ,:P67-FL-PRE-FIN
                       ,:P67-DT-SCAD-1A-RAT-DB
                        :IND-P67-DT-SCAD-1A-RAT
                       ,:P67-DT-EROG-FINANZ-DB
                        :IND-P67-DT-EROG-FINANZ
                       ,:P67-DT-STIPULA-FINANZ-DB
                        :IND-P67-DT-STIPULA-FINANZ
                       ,:P67-MM-PREAMM
                        :IND-P67-MM-PREAMM
                       ,:P67-IMP-DEB-RES
                        :IND-P67-IMP-DEB-RES
                       ,:P67-IMP-RAT-FINANZ
                        :IND-P67-IMP-RAT-FINANZ
                       ,:P67-IMP-CANONE-ANTIC
                        :IND-P67-IMP-CANONE-ANTIC
                       ,:P67-PER-RAT-FINANZ
                        :IND-P67-PER-RAT-FINANZ
                       ,:P67-TP-MOD-PAG-RAT
                        :IND-P67-TP-MOD-PAG-RAT
                       ,:P67-TP-FINANZ-ER
                        :IND-P67-TP-FINANZ-ER
                       ,:P67-CAT-FINANZ-ER
                        :IND-P67-CAT-FINANZ-ER
                       ,:P67-VAL-RISC-END-LEAS
                        :IND-P67-VAL-RISC-END-LEAS
                       ,:P67-DT-EST-FINANZ-DB
                        :IND-P67-DT-EST-FINANZ
                       ,:P67-DT-MAN-COP-DB
                        :IND-P67-DT-MAN-COP
                       ,:P67-NUM-FINANZ
                        :IND-P67-NUM-FINANZ
                       ,:P67-TP-MOD-ACQS
                        :IND-P67-TP-MOD-ACQS
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE EST_POLI_CPI_PR SET

                   ID_EST_POLI_CPI_PR     =
                :P67-ID-EST-POLI-CPI-PR
                  ,ID_MOVI_CRZ            =
                :P67-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :P67-ID-MOVI-CHIU
                                       :IND-P67-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :P67-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :P67-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :P67-COD-COMP-ANIA
                  ,IB_OGG                 =
                :P67-IB-OGG
                  ,DS_RIGA                =
                :P67-DS-RIGA
                  ,DS_OPER_SQL            =
                :P67-DS-OPER-SQL
                  ,DS_VER                 =
                :P67-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :P67-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :P67-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :P67-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :P67-DS-STATO-ELAB
                  ,COD_PROD_ESTNO         =
                :P67-COD-PROD-ESTNO
                  ,CPT_FIN                =
                :P67-CPT-FIN
                                       :IND-P67-CPT-FIN
                  ,NUM_TST_FIN            =
                :P67-NUM-TST-FIN
                                       :IND-P67-NUM-TST-FIN
                  ,TS_FINANZ              =
                :P67-TS-FINANZ
                                       :IND-P67-TS-FINANZ
                  ,DUR_MM_FINANZ          =
                :P67-DUR-MM-FINANZ
                                       :IND-P67-DUR-MM-FINANZ
                  ,DT_END_FINANZ          =
           :P67-DT-END-FINANZ-DB
                                       :IND-P67-DT-END-FINANZ
                  ,AMM_1A_RAT             =
                :P67-AMM-1A-RAT
                                       :IND-P67-AMM-1A-RAT
                  ,VAL_RISC_BENE          =
                :P67-VAL-RISC-BENE
                                       :IND-P67-VAL-RISC-BENE
                  ,AMM_RAT_END            =
                :P67-AMM-RAT-END
                                       :IND-P67-AMM-RAT-END
                  ,TS_CRE_RAT_FINANZ      =
                :P67-TS-CRE-RAT-FINANZ
                                       :IND-P67-TS-CRE-RAT-FINANZ
                  ,IMP_FIN_REVOLVING      =
                :P67-IMP-FIN-REVOLVING
                                       :IND-P67-IMP-FIN-REVOLVING
                  ,IMP_UTIL_C_REV         =
                :P67-IMP-UTIL-C-REV
                                       :IND-P67-IMP-UTIL-C-REV
                  ,IMP_RAT_REVOLVING      =
                :P67-IMP-RAT-REVOLVING
                                       :IND-P67-IMP-RAT-REVOLVING
                  ,DT_1O_UTLZ_C_REV       =
           :P67-DT-1O-UTLZ-C-REV-DB
                                       :IND-P67-DT-1O-UTLZ-C-REV
                  ,IMP_ASSTO              =
                :P67-IMP-ASSTO
                                       :IND-P67-IMP-ASSTO
                  ,PRE_VERS               =
                :P67-PRE-VERS
                                       :IND-P67-PRE-VERS
                  ,DT_SCAD_COP            =
           :P67-DT-SCAD-COP-DB
                                       :IND-P67-DT-SCAD-COP
                  ,GG_DEL_MM_SCAD_RAT     =
                :P67-GG-DEL-MM-SCAD-RAT
                                       :IND-P67-GG-DEL-MM-SCAD-RAT
                  ,FL_PRE_FIN             =
                :P67-FL-PRE-FIN
                  ,DT_SCAD_1A_RAT         =
           :P67-DT-SCAD-1A-RAT-DB
                                       :IND-P67-DT-SCAD-1A-RAT
                  ,DT_EROG_FINANZ         =
           :P67-DT-EROG-FINANZ-DB
                                       :IND-P67-DT-EROG-FINANZ
                  ,DT_STIPULA_FINANZ      =
           :P67-DT-STIPULA-FINANZ-DB
                                       :IND-P67-DT-STIPULA-FINANZ
                  ,MM_PREAMM              =
                :P67-MM-PREAMM
                                       :IND-P67-MM-PREAMM
                  ,IMP_DEB_RES            =
                :P67-IMP-DEB-RES
                                       :IND-P67-IMP-DEB-RES
                  ,IMP_RAT_FINANZ         =
                :P67-IMP-RAT-FINANZ
                                       :IND-P67-IMP-RAT-FINANZ
                  ,IMP_CANONE_ANTIC       =
                :P67-IMP-CANONE-ANTIC
                                       :IND-P67-IMP-CANONE-ANTIC
                  ,PER_RAT_FINANZ         =
                :P67-PER-RAT-FINANZ
                                       :IND-P67-PER-RAT-FINANZ
                  ,TP_MOD_PAG_RAT         =
                :P67-TP-MOD-PAG-RAT
                                       :IND-P67-TP-MOD-PAG-RAT
                  ,TP_FINANZ_ER           =
                :P67-TP-FINANZ-ER
                                       :IND-P67-TP-FINANZ-ER
                  ,CAT_FINANZ_ER          =
                :P67-CAT-FINANZ-ER
                                       :IND-P67-CAT-FINANZ-ER
                  ,VAL_RISC_END_LEAS      =
                :P67-VAL-RISC-END-LEAS
                                       :IND-P67-VAL-RISC-END-LEAS
                  ,DT_EST_FINANZ          =
           :P67-DT-EST-FINANZ-DB
                                       :IND-P67-DT-EST-FINANZ
                  ,DT_MAN_COP             =
           :P67-DT-MAN-COP-DB
                                       :IND-P67-DT-MAN-COP
                  ,NUM_FINANZ             =
                :P67-NUM-FINANZ
                                       :IND-P67-NUM-FINANZ
                  ,TP_MOD_ACQS            =
                :P67-TP-MOD-ACQS
                                       :IND-P67-TP-MOD-ACQS
                WHERE     DS_RIGA = :P67-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM EST_POLI_CPI_PR
                WHERE     DS_RIGA = :P67-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-P67 CURSOR FOR
              SELECT
                     ID_EST_POLI_CPI_PR
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IB_OGG
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,COD_PROD_ESTNO
                    ,CPT_FIN
                    ,NUM_TST_FIN
                    ,TS_FINANZ
                    ,DUR_MM_FINANZ
                    ,DT_END_FINANZ
                    ,AMM_1A_RAT
                    ,VAL_RISC_BENE
                    ,AMM_RAT_END
                    ,TS_CRE_RAT_FINANZ
                    ,IMP_FIN_REVOLVING
                    ,IMP_UTIL_C_REV
                    ,IMP_RAT_REVOLVING
                    ,DT_1O_UTLZ_C_REV
                    ,IMP_ASSTO
                    ,PRE_VERS
                    ,DT_SCAD_COP
                    ,GG_DEL_MM_SCAD_RAT
                    ,FL_PRE_FIN
                    ,DT_SCAD_1A_RAT
                    ,DT_EROG_FINANZ
                    ,DT_STIPULA_FINANZ
                    ,MM_PREAMM
                    ,IMP_DEB_RES
                    ,IMP_RAT_FINANZ
                    ,IMP_CANONE_ANTIC
                    ,PER_RAT_FINANZ
                    ,TP_MOD_PAG_RAT
                    ,TP_FINANZ_ER
                    ,CAT_FINANZ_ER
                    ,VAL_RISC_END_LEAS
                    ,DT_EST_FINANZ
                    ,DT_MAN_COP
                    ,NUM_FINANZ
                    ,TP_MOD_ACQS
              FROM EST_POLI_CPI_PR
              WHERE     ID_EST_POLI_CPI_PR = :P67-ID-EST-POLI-CPI-PR
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_EST_POLI_CPI_PR
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,COD_PROD_ESTNO
                ,CPT_FIN
                ,NUM_TST_FIN
                ,TS_FINANZ
                ,DUR_MM_FINANZ
                ,DT_END_FINANZ
                ,AMM_1A_RAT
                ,VAL_RISC_BENE
                ,AMM_RAT_END
                ,TS_CRE_RAT_FINANZ
                ,IMP_FIN_REVOLVING
                ,IMP_UTIL_C_REV
                ,IMP_RAT_REVOLVING
                ,DT_1O_UTLZ_C_REV
                ,IMP_ASSTO
                ,PRE_VERS
                ,DT_SCAD_COP
                ,GG_DEL_MM_SCAD_RAT
                ,FL_PRE_FIN
                ,DT_SCAD_1A_RAT
                ,DT_EROG_FINANZ
                ,DT_STIPULA_FINANZ
                ,MM_PREAMM
                ,IMP_DEB_RES
                ,IMP_RAT_FINANZ
                ,IMP_CANONE_ANTIC
                ,PER_RAT_FINANZ
                ,TP_MOD_PAG_RAT
                ,TP_FINANZ_ER
                ,CAT_FINANZ_ER
                ,VAL_RISC_END_LEAS
                ,DT_EST_FINANZ
                ,DT_MAN_COP
                ,NUM_FINANZ
                ,TP_MOD_ACQS
             INTO
                :P67-ID-EST-POLI-CPI-PR
               ,:P67-ID-MOVI-CRZ
               ,:P67-ID-MOVI-CHIU
                :IND-P67-ID-MOVI-CHIU
               ,:P67-DT-INI-EFF-DB
               ,:P67-DT-END-EFF-DB
               ,:P67-COD-COMP-ANIA
               ,:P67-IB-OGG
               ,:P67-DS-RIGA
               ,:P67-DS-OPER-SQL
               ,:P67-DS-VER
               ,:P67-DS-TS-INI-CPTZ
               ,:P67-DS-TS-END-CPTZ
               ,:P67-DS-UTENTE
               ,:P67-DS-STATO-ELAB
               ,:P67-COD-PROD-ESTNO
               ,:P67-CPT-FIN
                :IND-P67-CPT-FIN
               ,:P67-NUM-TST-FIN
                :IND-P67-NUM-TST-FIN
               ,:P67-TS-FINANZ
                :IND-P67-TS-FINANZ
               ,:P67-DUR-MM-FINANZ
                :IND-P67-DUR-MM-FINANZ
               ,:P67-DT-END-FINANZ-DB
                :IND-P67-DT-END-FINANZ
               ,:P67-AMM-1A-RAT
                :IND-P67-AMM-1A-RAT
               ,:P67-VAL-RISC-BENE
                :IND-P67-VAL-RISC-BENE
               ,:P67-AMM-RAT-END
                :IND-P67-AMM-RAT-END
               ,:P67-TS-CRE-RAT-FINANZ
                :IND-P67-TS-CRE-RAT-FINANZ
               ,:P67-IMP-FIN-REVOLVING
                :IND-P67-IMP-FIN-REVOLVING
               ,:P67-IMP-UTIL-C-REV
                :IND-P67-IMP-UTIL-C-REV
               ,:P67-IMP-RAT-REVOLVING
                :IND-P67-IMP-RAT-REVOLVING
               ,:P67-DT-1O-UTLZ-C-REV-DB
                :IND-P67-DT-1O-UTLZ-C-REV
               ,:P67-IMP-ASSTO
                :IND-P67-IMP-ASSTO
               ,:P67-PRE-VERS
                :IND-P67-PRE-VERS
               ,:P67-DT-SCAD-COP-DB
                :IND-P67-DT-SCAD-COP
               ,:P67-GG-DEL-MM-SCAD-RAT
                :IND-P67-GG-DEL-MM-SCAD-RAT
               ,:P67-FL-PRE-FIN
               ,:P67-DT-SCAD-1A-RAT-DB
                :IND-P67-DT-SCAD-1A-RAT
               ,:P67-DT-EROG-FINANZ-DB
                :IND-P67-DT-EROG-FINANZ
               ,:P67-DT-STIPULA-FINANZ-DB
                :IND-P67-DT-STIPULA-FINANZ
               ,:P67-MM-PREAMM
                :IND-P67-MM-PREAMM
               ,:P67-IMP-DEB-RES
                :IND-P67-IMP-DEB-RES
               ,:P67-IMP-RAT-FINANZ
                :IND-P67-IMP-RAT-FINANZ
               ,:P67-IMP-CANONE-ANTIC
                :IND-P67-IMP-CANONE-ANTIC
               ,:P67-PER-RAT-FINANZ
                :IND-P67-PER-RAT-FINANZ
               ,:P67-TP-MOD-PAG-RAT
                :IND-P67-TP-MOD-PAG-RAT
               ,:P67-TP-FINANZ-ER
                :IND-P67-TP-FINANZ-ER
               ,:P67-CAT-FINANZ-ER
                :IND-P67-CAT-FINANZ-ER
               ,:P67-VAL-RISC-END-LEAS
                :IND-P67-VAL-RISC-END-LEAS
               ,:P67-DT-EST-FINANZ-DB
                :IND-P67-DT-EST-FINANZ
               ,:P67-DT-MAN-COP-DB
                :IND-P67-DT-MAN-COP
               ,:P67-NUM-FINANZ
                :IND-P67-NUM-FINANZ
               ,:P67-TP-MOD-ACQS
                :IND-P67-TP-MOD-ACQS
             FROM EST_POLI_CPI_PR
             WHERE     ID_EST_POLI_CPI_PR = :P67-ID-EST-POLI-CPI-PR
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE EST_POLI_CPI_PR SET

                   ID_EST_POLI_CPI_PR     =
                :P67-ID-EST-POLI-CPI-PR
                  ,ID_MOVI_CRZ            =
                :P67-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :P67-ID-MOVI-CHIU
                                       :IND-P67-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :P67-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :P67-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :P67-COD-COMP-ANIA
                  ,IB_OGG                 =
                :P67-IB-OGG
                  ,DS_RIGA                =
                :P67-DS-RIGA
                  ,DS_OPER_SQL            =
                :P67-DS-OPER-SQL
                  ,DS_VER                 =
                :P67-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :P67-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :P67-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :P67-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :P67-DS-STATO-ELAB
                  ,COD_PROD_ESTNO         =
                :P67-COD-PROD-ESTNO
                  ,CPT_FIN                =
                :P67-CPT-FIN
                                       :IND-P67-CPT-FIN
                  ,NUM_TST_FIN            =
                :P67-NUM-TST-FIN
                                       :IND-P67-NUM-TST-FIN
                  ,TS_FINANZ              =
                :P67-TS-FINANZ
                                       :IND-P67-TS-FINANZ
                  ,DUR_MM_FINANZ          =
                :P67-DUR-MM-FINANZ
                                       :IND-P67-DUR-MM-FINANZ
                  ,DT_END_FINANZ          =
           :P67-DT-END-FINANZ-DB
                                       :IND-P67-DT-END-FINANZ
                  ,AMM_1A_RAT             =
                :P67-AMM-1A-RAT
                                       :IND-P67-AMM-1A-RAT
                  ,VAL_RISC_BENE          =
                :P67-VAL-RISC-BENE
                                       :IND-P67-VAL-RISC-BENE
                  ,AMM_RAT_END            =
                :P67-AMM-RAT-END
                                       :IND-P67-AMM-RAT-END
                  ,TS_CRE_RAT_FINANZ      =
                :P67-TS-CRE-RAT-FINANZ
                                       :IND-P67-TS-CRE-RAT-FINANZ
                  ,IMP_FIN_REVOLVING      =
                :P67-IMP-FIN-REVOLVING
                                       :IND-P67-IMP-FIN-REVOLVING
                  ,IMP_UTIL_C_REV         =
                :P67-IMP-UTIL-C-REV
                                       :IND-P67-IMP-UTIL-C-REV
                  ,IMP_RAT_REVOLVING      =
                :P67-IMP-RAT-REVOLVING
                                       :IND-P67-IMP-RAT-REVOLVING
                  ,DT_1O_UTLZ_C_REV       =
           :P67-DT-1O-UTLZ-C-REV-DB
                                       :IND-P67-DT-1O-UTLZ-C-REV
                  ,IMP_ASSTO              =
                :P67-IMP-ASSTO
                                       :IND-P67-IMP-ASSTO
                  ,PRE_VERS               =
                :P67-PRE-VERS
                                       :IND-P67-PRE-VERS
                  ,DT_SCAD_COP            =
           :P67-DT-SCAD-COP-DB
                                       :IND-P67-DT-SCAD-COP
                  ,GG_DEL_MM_SCAD_RAT     =
                :P67-GG-DEL-MM-SCAD-RAT
                                       :IND-P67-GG-DEL-MM-SCAD-RAT
                  ,FL_PRE_FIN             =
                :P67-FL-PRE-FIN
                  ,DT_SCAD_1A_RAT         =
           :P67-DT-SCAD-1A-RAT-DB
                                       :IND-P67-DT-SCAD-1A-RAT
                  ,DT_EROG_FINANZ         =
           :P67-DT-EROG-FINANZ-DB
                                       :IND-P67-DT-EROG-FINANZ
                  ,DT_STIPULA_FINANZ      =
           :P67-DT-STIPULA-FINANZ-DB
                                       :IND-P67-DT-STIPULA-FINANZ
                  ,MM_PREAMM              =
                :P67-MM-PREAMM
                                       :IND-P67-MM-PREAMM
                  ,IMP_DEB_RES            =
                :P67-IMP-DEB-RES
                                       :IND-P67-IMP-DEB-RES
                  ,IMP_RAT_FINANZ         =
                :P67-IMP-RAT-FINANZ
                                       :IND-P67-IMP-RAT-FINANZ
                  ,IMP_CANONE_ANTIC       =
                :P67-IMP-CANONE-ANTIC
                                       :IND-P67-IMP-CANONE-ANTIC
                  ,PER_RAT_FINANZ         =
                :P67-PER-RAT-FINANZ
                                       :IND-P67-PER-RAT-FINANZ
                  ,TP_MOD_PAG_RAT         =
                :P67-TP-MOD-PAG-RAT
                                       :IND-P67-TP-MOD-PAG-RAT
                  ,TP_FINANZ_ER           =
                :P67-TP-FINANZ-ER
                                       :IND-P67-TP-FINANZ-ER
                  ,CAT_FINANZ_ER          =
                :P67-CAT-FINANZ-ER
                                       :IND-P67-CAT-FINANZ-ER
                  ,VAL_RISC_END_LEAS      =
                :P67-VAL-RISC-END-LEAS
                                       :IND-P67-VAL-RISC-END-LEAS
                  ,DT_EST_FINANZ          =
           :P67-DT-EST-FINANZ-DB
                                       :IND-P67-DT-EST-FINANZ
                  ,DT_MAN_COP             =
           :P67-DT-MAN-COP-DB
                                       :IND-P67-DT-MAN-COP
                  ,NUM_FINANZ             =
                :P67-NUM-FINANZ
                                       :IND-P67-NUM-FINANZ
                  ,TP_MOD_ACQS            =
                :P67-TP-MOD-ACQS
                                       :IND-P67-TP-MOD-ACQS
                WHERE     DS_RIGA = :P67-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-P67
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-P67
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-P67
           INTO
                :P67-ID-EST-POLI-CPI-PR
               ,:P67-ID-MOVI-CRZ
               ,:P67-ID-MOVI-CHIU
                :IND-P67-ID-MOVI-CHIU
               ,:P67-DT-INI-EFF-DB
               ,:P67-DT-END-EFF-DB
               ,:P67-COD-COMP-ANIA
               ,:P67-IB-OGG
               ,:P67-DS-RIGA
               ,:P67-DS-OPER-SQL
               ,:P67-DS-VER
               ,:P67-DS-TS-INI-CPTZ
               ,:P67-DS-TS-END-CPTZ
               ,:P67-DS-UTENTE
               ,:P67-DS-STATO-ELAB
               ,:P67-COD-PROD-ESTNO
               ,:P67-CPT-FIN
                :IND-P67-CPT-FIN
               ,:P67-NUM-TST-FIN
                :IND-P67-NUM-TST-FIN
               ,:P67-TS-FINANZ
                :IND-P67-TS-FINANZ
               ,:P67-DUR-MM-FINANZ
                :IND-P67-DUR-MM-FINANZ
               ,:P67-DT-END-FINANZ-DB
                :IND-P67-DT-END-FINANZ
               ,:P67-AMM-1A-RAT
                :IND-P67-AMM-1A-RAT
               ,:P67-VAL-RISC-BENE
                :IND-P67-VAL-RISC-BENE
               ,:P67-AMM-RAT-END
                :IND-P67-AMM-RAT-END
               ,:P67-TS-CRE-RAT-FINANZ
                :IND-P67-TS-CRE-RAT-FINANZ
               ,:P67-IMP-FIN-REVOLVING
                :IND-P67-IMP-FIN-REVOLVING
               ,:P67-IMP-UTIL-C-REV
                :IND-P67-IMP-UTIL-C-REV
               ,:P67-IMP-RAT-REVOLVING
                :IND-P67-IMP-RAT-REVOLVING
               ,:P67-DT-1O-UTLZ-C-REV-DB
                :IND-P67-DT-1O-UTLZ-C-REV
               ,:P67-IMP-ASSTO
                :IND-P67-IMP-ASSTO
               ,:P67-PRE-VERS
                :IND-P67-PRE-VERS
               ,:P67-DT-SCAD-COP-DB
                :IND-P67-DT-SCAD-COP
               ,:P67-GG-DEL-MM-SCAD-RAT
                :IND-P67-GG-DEL-MM-SCAD-RAT
               ,:P67-FL-PRE-FIN
               ,:P67-DT-SCAD-1A-RAT-DB
                :IND-P67-DT-SCAD-1A-RAT
               ,:P67-DT-EROG-FINANZ-DB
                :IND-P67-DT-EROG-FINANZ
               ,:P67-DT-STIPULA-FINANZ-DB
                :IND-P67-DT-STIPULA-FINANZ
               ,:P67-MM-PREAMM
                :IND-P67-MM-PREAMM
               ,:P67-IMP-DEB-RES
                :IND-P67-IMP-DEB-RES
               ,:P67-IMP-RAT-FINANZ
                :IND-P67-IMP-RAT-FINANZ
               ,:P67-IMP-CANONE-ANTIC
                :IND-P67-IMP-CANONE-ANTIC
               ,:P67-PER-RAT-FINANZ
                :IND-P67-PER-RAT-FINANZ
               ,:P67-TP-MOD-PAG-RAT
                :IND-P67-TP-MOD-PAG-RAT
               ,:P67-TP-FINANZ-ER
                :IND-P67-TP-FINANZ-ER
               ,:P67-CAT-FINANZ-ER
                :IND-P67-CAT-FINANZ-ER
               ,:P67-VAL-RISC-END-LEAS
                :IND-P67-VAL-RISC-END-LEAS
               ,:P67-DT-EST-FINANZ-DB
                :IND-P67-DT-EST-FINANZ
               ,:P67-DT-MAN-COP-DB
                :IND-P67-DT-MAN-COP
               ,:P67-NUM-FINANZ
                :IND-P67-NUM-FINANZ
               ,:P67-TP-MOD-ACQS
                :IND-P67-TP-MOD-ACQS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IBO-EFF-P67 CURSOR FOR
              SELECT
                     ID_EST_POLI_CPI_PR
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IB_OGG
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,COD_PROD_ESTNO
                    ,CPT_FIN
                    ,NUM_TST_FIN
                    ,TS_FINANZ
                    ,DUR_MM_FINANZ
                    ,DT_END_FINANZ
                    ,AMM_1A_RAT
                    ,VAL_RISC_BENE
                    ,AMM_RAT_END
                    ,TS_CRE_RAT_FINANZ
                    ,IMP_FIN_REVOLVING
                    ,IMP_UTIL_C_REV
                    ,IMP_RAT_REVOLVING
                    ,DT_1O_UTLZ_C_REV
                    ,IMP_ASSTO
                    ,PRE_VERS
                    ,DT_SCAD_COP
                    ,GG_DEL_MM_SCAD_RAT
                    ,FL_PRE_FIN
                    ,DT_SCAD_1A_RAT
                    ,DT_EROG_FINANZ
                    ,DT_STIPULA_FINANZ
                    ,MM_PREAMM
                    ,IMP_DEB_RES
                    ,IMP_RAT_FINANZ
                    ,IMP_CANONE_ANTIC
                    ,PER_RAT_FINANZ
                    ,TP_MOD_PAG_RAT
                    ,TP_FINANZ_ER
                    ,CAT_FINANZ_ER
                    ,VAL_RISC_END_LEAS
                    ,DT_EST_FINANZ
                    ,DT_MAN_COP
                    ,NUM_FINANZ
                    ,TP_MOD_ACQS
              FROM EST_POLI_CPI_PR
              WHERE     IB_OGG = :P67-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_EST_POLI_CPI_PR ASC

           END-EXEC.

       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_EST_POLI_CPI_PR
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,COD_PROD_ESTNO
                ,CPT_FIN
                ,NUM_TST_FIN
                ,TS_FINANZ
                ,DUR_MM_FINANZ
                ,DT_END_FINANZ
                ,AMM_1A_RAT
                ,VAL_RISC_BENE
                ,AMM_RAT_END
                ,TS_CRE_RAT_FINANZ
                ,IMP_FIN_REVOLVING
                ,IMP_UTIL_C_REV
                ,IMP_RAT_REVOLVING
                ,DT_1O_UTLZ_C_REV
                ,IMP_ASSTO
                ,PRE_VERS
                ,DT_SCAD_COP
                ,GG_DEL_MM_SCAD_RAT
                ,FL_PRE_FIN
                ,DT_SCAD_1A_RAT
                ,DT_EROG_FINANZ
                ,DT_STIPULA_FINANZ
                ,MM_PREAMM
                ,IMP_DEB_RES
                ,IMP_RAT_FINANZ
                ,IMP_CANONE_ANTIC
                ,PER_RAT_FINANZ
                ,TP_MOD_PAG_RAT
                ,TP_FINANZ_ER
                ,CAT_FINANZ_ER
                ,VAL_RISC_END_LEAS
                ,DT_EST_FINANZ
                ,DT_MAN_COP
                ,NUM_FINANZ
                ,TP_MOD_ACQS
             INTO
                :P67-ID-EST-POLI-CPI-PR
               ,:P67-ID-MOVI-CRZ
               ,:P67-ID-MOVI-CHIU
                :IND-P67-ID-MOVI-CHIU
               ,:P67-DT-INI-EFF-DB
               ,:P67-DT-END-EFF-DB
               ,:P67-COD-COMP-ANIA
               ,:P67-IB-OGG
               ,:P67-DS-RIGA
               ,:P67-DS-OPER-SQL
               ,:P67-DS-VER
               ,:P67-DS-TS-INI-CPTZ
               ,:P67-DS-TS-END-CPTZ
               ,:P67-DS-UTENTE
               ,:P67-DS-STATO-ELAB
               ,:P67-COD-PROD-ESTNO
               ,:P67-CPT-FIN
                :IND-P67-CPT-FIN
               ,:P67-NUM-TST-FIN
                :IND-P67-NUM-TST-FIN
               ,:P67-TS-FINANZ
                :IND-P67-TS-FINANZ
               ,:P67-DUR-MM-FINANZ
                :IND-P67-DUR-MM-FINANZ
               ,:P67-DT-END-FINANZ-DB
                :IND-P67-DT-END-FINANZ
               ,:P67-AMM-1A-RAT
                :IND-P67-AMM-1A-RAT
               ,:P67-VAL-RISC-BENE
                :IND-P67-VAL-RISC-BENE
               ,:P67-AMM-RAT-END
                :IND-P67-AMM-RAT-END
               ,:P67-TS-CRE-RAT-FINANZ
                :IND-P67-TS-CRE-RAT-FINANZ
               ,:P67-IMP-FIN-REVOLVING
                :IND-P67-IMP-FIN-REVOLVING
               ,:P67-IMP-UTIL-C-REV
                :IND-P67-IMP-UTIL-C-REV
               ,:P67-IMP-RAT-REVOLVING
                :IND-P67-IMP-RAT-REVOLVING
               ,:P67-DT-1O-UTLZ-C-REV-DB
                :IND-P67-DT-1O-UTLZ-C-REV
               ,:P67-IMP-ASSTO
                :IND-P67-IMP-ASSTO
               ,:P67-PRE-VERS
                :IND-P67-PRE-VERS
               ,:P67-DT-SCAD-COP-DB
                :IND-P67-DT-SCAD-COP
               ,:P67-GG-DEL-MM-SCAD-RAT
                :IND-P67-GG-DEL-MM-SCAD-RAT
               ,:P67-FL-PRE-FIN
               ,:P67-DT-SCAD-1A-RAT-DB
                :IND-P67-DT-SCAD-1A-RAT
               ,:P67-DT-EROG-FINANZ-DB
                :IND-P67-DT-EROG-FINANZ
               ,:P67-DT-STIPULA-FINANZ-DB
                :IND-P67-DT-STIPULA-FINANZ
               ,:P67-MM-PREAMM
                :IND-P67-MM-PREAMM
               ,:P67-IMP-DEB-RES
                :IND-P67-IMP-DEB-RES
               ,:P67-IMP-RAT-FINANZ
                :IND-P67-IMP-RAT-FINANZ
               ,:P67-IMP-CANONE-ANTIC
                :IND-P67-IMP-CANONE-ANTIC
               ,:P67-PER-RAT-FINANZ
                :IND-P67-PER-RAT-FINANZ
               ,:P67-TP-MOD-PAG-RAT
                :IND-P67-TP-MOD-PAG-RAT
               ,:P67-TP-FINANZ-ER
                :IND-P67-TP-FINANZ-ER
               ,:P67-CAT-FINANZ-ER
                :IND-P67-CAT-FINANZ-ER
               ,:P67-VAL-RISC-END-LEAS
                :IND-P67-VAL-RISC-END-LEAS
               ,:P67-DT-EST-FINANZ-DB
                :IND-P67-DT-EST-FINANZ
               ,:P67-DT-MAN-COP-DB
                :IND-P67-DT-MAN-COP
               ,:P67-NUM-FINANZ
                :IND-P67-NUM-FINANZ
               ,:P67-TP-MOD-ACQS
                :IND-P67-TP-MOD-ACQS
             FROM EST_POLI_CPI_PR
             WHERE     IB_OGG = :P67-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           EXEC SQL
                OPEN C-IBO-EFF-P67
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           EXEC SQL
                CLOSE C-IBO-EFF-P67
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           EXEC SQL
                FETCH C-IBO-EFF-P67
           INTO
                :P67-ID-EST-POLI-CPI-PR
               ,:P67-ID-MOVI-CRZ
               ,:P67-ID-MOVI-CHIU
                :IND-P67-ID-MOVI-CHIU
               ,:P67-DT-INI-EFF-DB
               ,:P67-DT-END-EFF-DB
               ,:P67-COD-COMP-ANIA
               ,:P67-IB-OGG
               ,:P67-DS-RIGA
               ,:P67-DS-OPER-SQL
               ,:P67-DS-VER
               ,:P67-DS-TS-INI-CPTZ
               ,:P67-DS-TS-END-CPTZ
               ,:P67-DS-UTENTE
               ,:P67-DS-STATO-ELAB
               ,:P67-COD-PROD-ESTNO
               ,:P67-CPT-FIN
                :IND-P67-CPT-FIN
               ,:P67-NUM-TST-FIN
                :IND-P67-NUM-TST-FIN
               ,:P67-TS-FINANZ
                :IND-P67-TS-FINANZ
               ,:P67-DUR-MM-FINANZ
                :IND-P67-DUR-MM-FINANZ
               ,:P67-DT-END-FINANZ-DB
                :IND-P67-DT-END-FINANZ
               ,:P67-AMM-1A-RAT
                :IND-P67-AMM-1A-RAT
               ,:P67-VAL-RISC-BENE
                :IND-P67-VAL-RISC-BENE
               ,:P67-AMM-RAT-END
                :IND-P67-AMM-RAT-END
               ,:P67-TS-CRE-RAT-FINANZ
                :IND-P67-TS-CRE-RAT-FINANZ
               ,:P67-IMP-FIN-REVOLVING
                :IND-P67-IMP-FIN-REVOLVING
               ,:P67-IMP-UTIL-C-REV
                :IND-P67-IMP-UTIL-C-REV
               ,:P67-IMP-RAT-REVOLVING
                :IND-P67-IMP-RAT-REVOLVING
               ,:P67-DT-1O-UTLZ-C-REV-DB
                :IND-P67-DT-1O-UTLZ-C-REV
               ,:P67-IMP-ASSTO
                :IND-P67-IMP-ASSTO
               ,:P67-PRE-VERS
                :IND-P67-PRE-VERS
               ,:P67-DT-SCAD-COP-DB
                :IND-P67-DT-SCAD-COP
               ,:P67-GG-DEL-MM-SCAD-RAT
                :IND-P67-GG-DEL-MM-SCAD-RAT
               ,:P67-FL-PRE-FIN
               ,:P67-DT-SCAD-1A-RAT-DB
                :IND-P67-DT-SCAD-1A-RAT
               ,:P67-DT-EROG-FINANZ-DB
                :IND-P67-DT-EROG-FINANZ
               ,:P67-DT-STIPULA-FINANZ-DB
                :IND-P67-DT-STIPULA-FINANZ
               ,:P67-MM-PREAMM
                :IND-P67-MM-PREAMM
               ,:P67-IMP-DEB-RES
                :IND-P67-IMP-DEB-RES
               ,:P67-IMP-RAT-FINANZ
                :IND-P67-IMP-RAT-FINANZ
               ,:P67-IMP-CANONE-ANTIC
                :IND-P67-IMP-CANONE-ANTIC
               ,:P67-PER-RAT-FINANZ
                :IND-P67-PER-RAT-FINANZ
               ,:P67-TP-MOD-PAG-RAT
                :IND-P67-TP-MOD-PAG-RAT
               ,:P67-TP-FINANZ-ER
                :IND-P67-TP-FINANZ-ER
               ,:P67-CAT-FINANZ-ER
                :IND-P67-CAT-FINANZ-ER
               ,:P67-VAL-RISC-END-LEAS
                :IND-P67-VAL-RISC-END-LEAS
               ,:P67-DT-EST-FINANZ-DB
                :IND-P67-DT-EST-FINANZ
               ,:P67-DT-MAN-COP-DB
                :IND-P67-DT-MAN-COP
               ,:P67-NUM-FINANZ
                :IND-P67-NUM-FINANZ
               ,:P67-TP-MOD-ACQS
                :IND-P67-TP-MOD-ACQS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A570-CLOSE-CURSOR-IBO     THRU A570-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_EST_POLI_CPI_PR
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,COD_PROD_ESTNO
                ,CPT_FIN
                ,NUM_TST_FIN
                ,TS_FINANZ
                ,DUR_MM_FINANZ
                ,DT_END_FINANZ
                ,AMM_1A_RAT
                ,VAL_RISC_BENE
                ,AMM_RAT_END
                ,TS_CRE_RAT_FINANZ
                ,IMP_FIN_REVOLVING
                ,IMP_UTIL_C_REV
                ,IMP_RAT_REVOLVING
                ,DT_1O_UTLZ_C_REV
                ,IMP_ASSTO
                ,PRE_VERS
                ,DT_SCAD_COP
                ,GG_DEL_MM_SCAD_RAT
                ,FL_PRE_FIN
                ,DT_SCAD_1A_RAT
                ,DT_EROG_FINANZ
                ,DT_STIPULA_FINANZ
                ,MM_PREAMM
                ,IMP_DEB_RES
                ,IMP_RAT_FINANZ
                ,IMP_CANONE_ANTIC
                ,PER_RAT_FINANZ
                ,TP_MOD_PAG_RAT
                ,TP_FINANZ_ER
                ,CAT_FINANZ_ER
                ,VAL_RISC_END_LEAS
                ,DT_EST_FINANZ
                ,DT_MAN_COP
                ,NUM_FINANZ
                ,TP_MOD_ACQS
             INTO
                :P67-ID-EST-POLI-CPI-PR
               ,:P67-ID-MOVI-CRZ
               ,:P67-ID-MOVI-CHIU
                :IND-P67-ID-MOVI-CHIU
               ,:P67-DT-INI-EFF-DB
               ,:P67-DT-END-EFF-DB
               ,:P67-COD-COMP-ANIA
               ,:P67-IB-OGG
               ,:P67-DS-RIGA
               ,:P67-DS-OPER-SQL
               ,:P67-DS-VER
               ,:P67-DS-TS-INI-CPTZ
               ,:P67-DS-TS-END-CPTZ
               ,:P67-DS-UTENTE
               ,:P67-DS-STATO-ELAB
               ,:P67-COD-PROD-ESTNO
               ,:P67-CPT-FIN
                :IND-P67-CPT-FIN
               ,:P67-NUM-TST-FIN
                :IND-P67-NUM-TST-FIN
               ,:P67-TS-FINANZ
                :IND-P67-TS-FINANZ
               ,:P67-DUR-MM-FINANZ
                :IND-P67-DUR-MM-FINANZ
               ,:P67-DT-END-FINANZ-DB
                :IND-P67-DT-END-FINANZ
               ,:P67-AMM-1A-RAT
                :IND-P67-AMM-1A-RAT
               ,:P67-VAL-RISC-BENE
                :IND-P67-VAL-RISC-BENE
               ,:P67-AMM-RAT-END
                :IND-P67-AMM-RAT-END
               ,:P67-TS-CRE-RAT-FINANZ
                :IND-P67-TS-CRE-RAT-FINANZ
               ,:P67-IMP-FIN-REVOLVING
                :IND-P67-IMP-FIN-REVOLVING
               ,:P67-IMP-UTIL-C-REV
                :IND-P67-IMP-UTIL-C-REV
               ,:P67-IMP-RAT-REVOLVING
                :IND-P67-IMP-RAT-REVOLVING
               ,:P67-DT-1O-UTLZ-C-REV-DB
                :IND-P67-DT-1O-UTLZ-C-REV
               ,:P67-IMP-ASSTO
                :IND-P67-IMP-ASSTO
               ,:P67-PRE-VERS
                :IND-P67-PRE-VERS
               ,:P67-DT-SCAD-COP-DB
                :IND-P67-DT-SCAD-COP
               ,:P67-GG-DEL-MM-SCAD-RAT
                :IND-P67-GG-DEL-MM-SCAD-RAT
               ,:P67-FL-PRE-FIN
               ,:P67-DT-SCAD-1A-RAT-DB
                :IND-P67-DT-SCAD-1A-RAT
               ,:P67-DT-EROG-FINANZ-DB
                :IND-P67-DT-EROG-FINANZ
               ,:P67-DT-STIPULA-FINANZ-DB
                :IND-P67-DT-STIPULA-FINANZ
               ,:P67-MM-PREAMM
                :IND-P67-MM-PREAMM
               ,:P67-IMP-DEB-RES
                :IND-P67-IMP-DEB-RES
               ,:P67-IMP-RAT-FINANZ
                :IND-P67-IMP-RAT-FINANZ
               ,:P67-IMP-CANONE-ANTIC
                :IND-P67-IMP-CANONE-ANTIC
               ,:P67-PER-RAT-FINANZ
                :IND-P67-PER-RAT-FINANZ
               ,:P67-TP-MOD-PAG-RAT
                :IND-P67-TP-MOD-PAG-RAT
               ,:P67-TP-FINANZ-ER
                :IND-P67-TP-FINANZ-ER
               ,:P67-CAT-FINANZ-ER
                :IND-P67-CAT-FINANZ-ER
               ,:P67-VAL-RISC-END-LEAS
                :IND-P67-VAL-RISC-END-LEAS
               ,:P67-DT-EST-FINANZ-DB
                :IND-P67-DT-EST-FINANZ
               ,:P67-DT-MAN-COP-DB
                :IND-P67-DT-MAN-COP
               ,:P67-NUM-FINANZ
                :IND-P67-NUM-FINANZ
               ,:P67-TP-MOD-ACQS
                :IND-P67-TP-MOD-ACQS
             FROM EST_POLI_CPI_PR
             WHERE     ID_EST_POLI_CPI_PR = :P67-ID-EST-POLI-CPI-PR
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IBO-CPZ-P67 CURSOR FOR
              SELECT
                     ID_EST_POLI_CPI_PR
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IB_OGG
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,COD_PROD_ESTNO
                    ,CPT_FIN
                    ,NUM_TST_FIN
                    ,TS_FINANZ
                    ,DUR_MM_FINANZ
                    ,DT_END_FINANZ
                    ,AMM_1A_RAT
                    ,VAL_RISC_BENE
                    ,AMM_RAT_END
                    ,TS_CRE_RAT_FINANZ
                    ,IMP_FIN_REVOLVING
                    ,IMP_UTIL_C_REV
                    ,IMP_RAT_REVOLVING
                    ,DT_1O_UTLZ_C_REV
                    ,IMP_ASSTO
                    ,PRE_VERS
                    ,DT_SCAD_COP
                    ,GG_DEL_MM_SCAD_RAT
                    ,FL_PRE_FIN
                    ,DT_SCAD_1A_RAT
                    ,DT_EROG_FINANZ
                    ,DT_STIPULA_FINANZ
                    ,MM_PREAMM
                    ,IMP_DEB_RES
                    ,IMP_RAT_FINANZ
                    ,IMP_CANONE_ANTIC
                    ,PER_RAT_FINANZ
                    ,TP_MOD_PAG_RAT
                    ,TP_FINANZ_ER
                    ,CAT_FINANZ_ER
                    ,VAL_RISC_END_LEAS
                    ,DT_EST_FINANZ
                    ,DT_MAN_COP
                    ,NUM_FINANZ
                    ,TP_MOD_ACQS
              FROM EST_POLI_CPI_PR
              WHERE     IB_OGG = :P67-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_EST_POLI_CPI_PR ASC

           END-EXEC.

       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_EST_POLI_CPI_PR
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,COD_PROD_ESTNO
                ,CPT_FIN
                ,NUM_TST_FIN
                ,TS_FINANZ
                ,DUR_MM_FINANZ
                ,DT_END_FINANZ
                ,AMM_1A_RAT
                ,VAL_RISC_BENE
                ,AMM_RAT_END
                ,TS_CRE_RAT_FINANZ
                ,IMP_FIN_REVOLVING
                ,IMP_UTIL_C_REV
                ,IMP_RAT_REVOLVING
                ,DT_1O_UTLZ_C_REV
                ,IMP_ASSTO
                ,PRE_VERS
                ,DT_SCAD_COP
                ,GG_DEL_MM_SCAD_RAT
                ,FL_PRE_FIN
                ,DT_SCAD_1A_RAT
                ,DT_EROG_FINANZ
                ,DT_STIPULA_FINANZ
                ,MM_PREAMM
                ,IMP_DEB_RES
                ,IMP_RAT_FINANZ
                ,IMP_CANONE_ANTIC
                ,PER_RAT_FINANZ
                ,TP_MOD_PAG_RAT
                ,TP_FINANZ_ER
                ,CAT_FINANZ_ER
                ,VAL_RISC_END_LEAS
                ,DT_EST_FINANZ
                ,DT_MAN_COP
                ,NUM_FINANZ
                ,TP_MOD_ACQS
             INTO
                :P67-ID-EST-POLI-CPI-PR
               ,:P67-ID-MOVI-CRZ
               ,:P67-ID-MOVI-CHIU
                :IND-P67-ID-MOVI-CHIU
               ,:P67-DT-INI-EFF-DB
               ,:P67-DT-END-EFF-DB
               ,:P67-COD-COMP-ANIA
               ,:P67-IB-OGG
               ,:P67-DS-RIGA
               ,:P67-DS-OPER-SQL
               ,:P67-DS-VER
               ,:P67-DS-TS-INI-CPTZ
               ,:P67-DS-TS-END-CPTZ
               ,:P67-DS-UTENTE
               ,:P67-DS-STATO-ELAB
               ,:P67-COD-PROD-ESTNO
               ,:P67-CPT-FIN
                :IND-P67-CPT-FIN
               ,:P67-NUM-TST-FIN
                :IND-P67-NUM-TST-FIN
               ,:P67-TS-FINANZ
                :IND-P67-TS-FINANZ
               ,:P67-DUR-MM-FINANZ
                :IND-P67-DUR-MM-FINANZ
               ,:P67-DT-END-FINANZ-DB
                :IND-P67-DT-END-FINANZ
               ,:P67-AMM-1A-RAT
                :IND-P67-AMM-1A-RAT
               ,:P67-VAL-RISC-BENE
                :IND-P67-VAL-RISC-BENE
               ,:P67-AMM-RAT-END
                :IND-P67-AMM-RAT-END
               ,:P67-TS-CRE-RAT-FINANZ
                :IND-P67-TS-CRE-RAT-FINANZ
               ,:P67-IMP-FIN-REVOLVING
                :IND-P67-IMP-FIN-REVOLVING
               ,:P67-IMP-UTIL-C-REV
                :IND-P67-IMP-UTIL-C-REV
               ,:P67-IMP-RAT-REVOLVING
                :IND-P67-IMP-RAT-REVOLVING
               ,:P67-DT-1O-UTLZ-C-REV-DB
                :IND-P67-DT-1O-UTLZ-C-REV
               ,:P67-IMP-ASSTO
                :IND-P67-IMP-ASSTO
               ,:P67-PRE-VERS
                :IND-P67-PRE-VERS
               ,:P67-DT-SCAD-COP-DB
                :IND-P67-DT-SCAD-COP
               ,:P67-GG-DEL-MM-SCAD-RAT
                :IND-P67-GG-DEL-MM-SCAD-RAT
               ,:P67-FL-PRE-FIN
               ,:P67-DT-SCAD-1A-RAT-DB
                :IND-P67-DT-SCAD-1A-RAT
               ,:P67-DT-EROG-FINANZ-DB
                :IND-P67-DT-EROG-FINANZ
               ,:P67-DT-STIPULA-FINANZ-DB
                :IND-P67-DT-STIPULA-FINANZ
               ,:P67-MM-PREAMM
                :IND-P67-MM-PREAMM
               ,:P67-IMP-DEB-RES
                :IND-P67-IMP-DEB-RES
               ,:P67-IMP-RAT-FINANZ
                :IND-P67-IMP-RAT-FINANZ
               ,:P67-IMP-CANONE-ANTIC
                :IND-P67-IMP-CANONE-ANTIC
               ,:P67-PER-RAT-FINANZ
                :IND-P67-PER-RAT-FINANZ
               ,:P67-TP-MOD-PAG-RAT
                :IND-P67-TP-MOD-PAG-RAT
               ,:P67-TP-FINANZ-ER
                :IND-P67-TP-FINANZ-ER
               ,:P67-CAT-FINANZ-ER
                :IND-P67-CAT-FINANZ-ER
               ,:P67-VAL-RISC-END-LEAS
                :IND-P67-VAL-RISC-END-LEAS
               ,:P67-DT-EST-FINANZ-DB
                :IND-P67-DT-EST-FINANZ
               ,:P67-DT-MAN-COP-DB
                :IND-P67-DT-MAN-COP
               ,:P67-NUM-FINANZ
                :IND-P67-NUM-FINANZ
               ,:P67-TP-MOD-ACQS
                :IND-P67-TP-MOD-ACQS
             FROM EST_POLI_CPI_PR
             WHERE     IB_OGG = :P67-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           EXEC SQL
                OPEN C-IBO-CPZ-P67
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           EXEC SQL
                CLOSE C-IBO-CPZ-P67
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           EXEC SQL
                FETCH C-IBO-CPZ-P67
           INTO
                :P67-ID-EST-POLI-CPI-PR
               ,:P67-ID-MOVI-CRZ
               ,:P67-ID-MOVI-CHIU
                :IND-P67-ID-MOVI-CHIU
               ,:P67-DT-INI-EFF-DB
               ,:P67-DT-END-EFF-DB
               ,:P67-COD-COMP-ANIA
               ,:P67-IB-OGG
               ,:P67-DS-RIGA
               ,:P67-DS-OPER-SQL
               ,:P67-DS-VER
               ,:P67-DS-TS-INI-CPTZ
               ,:P67-DS-TS-END-CPTZ
               ,:P67-DS-UTENTE
               ,:P67-DS-STATO-ELAB
               ,:P67-COD-PROD-ESTNO
               ,:P67-CPT-FIN
                :IND-P67-CPT-FIN
               ,:P67-NUM-TST-FIN
                :IND-P67-NUM-TST-FIN
               ,:P67-TS-FINANZ
                :IND-P67-TS-FINANZ
               ,:P67-DUR-MM-FINANZ
                :IND-P67-DUR-MM-FINANZ
               ,:P67-DT-END-FINANZ-DB
                :IND-P67-DT-END-FINANZ
               ,:P67-AMM-1A-RAT
                :IND-P67-AMM-1A-RAT
               ,:P67-VAL-RISC-BENE
                :IND-P67-VAL-RISC-BENE
               ,:P67-AMM-RAT-END
                :IND-P67-AMM-RAT-END
               ,:P67-TS-CRE-RAT-FINANZ
                :IND-P67-TS-CRE-RAT-FINANZ
               ,:P67-IMP-FIN-REVOLVING
                :IND-P67-IMP-FIN-REVOLVING
               ,:P67-IMP-UTIL-C-REV
                :IND-P67-IMP-UTIL-C-REV
               ,:P67-IMP-RAT-REVOLVING
                :IND-P67-IMP-RAT-REVOLVING
               ,:P67-DT-1O-UTLZ-C-REV-DB
                :IND-P67-DT-1O-UTLZ-C-REV
               ,:P67-IMP-ASSTO
                :IND-P67-IMP-ASSTO
               ,:P67-PRE-VERS
                :IND-P67-PRE-VERS
               ,:P67-DT-SCAD-COP-DB
                :IND-P67-DT-SCAD-COP
               ,:P67-GG-DEL-MM-SCAD-RAT
                :IND-P67-GG-DEL-MM-SCAD-RAT
               ,:P67-FL-PRE-FIN
               ,:P67-DT-SCAD-1A-RAT-DB
                :IND-P67-DT-SCAD-1A-RAT
               ,:P67-DT-EROG-FINANZ-DB
                :IND-P67-DT-EROG-FINANZ
               ,:P67-DT-STIPULA-FINANZ-DB
                :IND-P67-DT-STIPULA-FINANZ
               ,:P67-MM-PREAMM
                :IND-P67-MM-PREAMM
               ,:P67-IMP-DEB-RES
                :IND-P67-IMP-DEB-RES
               ,:P67-IMP-RAT-FINANZ
                :IND-P67-IMP-RAT-FINANZ
               ,:P67-IMP-CANONE-ANTIC
                :IND-P67-IMP-CANONE-ANTIC
               ,:P67-PER-RAT-FINANZ
                :IND-P67-PER-RAT-FINANZ
               ,:P67-TP-MOD-PAG-RAT
                :IND-P67-TP-MOD-PAG-RAT
               ,:P67-TP-FINANZ-ER
                :IND-P67-TP-FINANZ-ER
               ,:P67-CAT-FINANZ-ER
                :IND-P67-CAT-FINANZ-ER
               ,:P67-VAL-RISC-END-LEAS
                :IND-P67-VAL-RISC-END-LEAS
               ,:P67-DT-EST-FINANZ-DB
                :IND-P67-DT-EST-FINANZ
               ,:P67-DT-MAN-COP-DB
                :IND-P67-DT-MAN-COP
               ,:P67-NUM-FINANZ
                :IND-P67-NUM-FINANZ
               ,:P67-TP-MOD-ACQS
                :IND-P67-TP-MOD-ACQS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B570-CLOSE-CURSOR-IBO-CPZ     THRU B570-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-P67-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO P67-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-P67-CPT-FIN = -1
              MOVE HIGH-VALUES TO P67-CPT-FIN-NULL
           END-IF
           IF IND-P67-NUM-TST-FIN = -1
              MOVE HIGH-VALUES TO P67-NUM-TST-FIN-NULL
           END-IF
           IF IND-P67-TS-FINANZ = -1
              MOVE HIGH-VALUES TO P67-TS-FINANZ-NULL
           END-IF
           IF IND-P67-DUR-MM-FINANZ = -1
              MOVE HIGH-VALUES TO P67-DUR-MM-FINANZ-NULL
           END-IF
           IF IND-P67-DT-END-FINANZ = -1
              MOVE HIGH-VALUES TO P67-DT-END-FINANZ-NULL
           END-IF
           IF IND-P67-AMM-1A-RAT = -1
              MOVE HIGH-VALUES TO P67-AMM-1A-RAT-NULL
           END-IF
           IF IND-P67-VAL-RISC-BENE = -1
              MOVE HIGH-VALUES TO P67-VAL-RISC-BENE-NULL
           END-IF
           IF IND-P67-AMM-RAT-END = -1
              MOVE HIGH-VALUES TO P67-AMM-RAT-END-NULL
           END-IF
           IF IND-P67-TS-CRE-RAT-FINANZ = -1
              MOVE HIGH-VALUES TO P67-TS-CRE-RAT-FINANZ-NULL
           END-IF
           IF IND-P67-IMP-FIN-REVOLVING = -1
              MOVE HIGH-VALUES TO P67-IMP-FIN-REVOLVING-NULL
           END-IF
           IF IND-P67-IMP-UTIL-C-REV = -1
              MOVE HIGH-VALUES TO P67-IMP-UTIL-C-REV-NULL
           END-IF
           IF IND-P67-IMP-RAT-REVOLVING = -1
              MOVE HIGH-VALUES TO P67-IMP-RAT-REVOLVING-NULL
           END-IF
           IF IND-P67-DT-1O-UTLZ-C-REV = -1
              MOVE HIGH-VALUES TO P67-DT-1O-UTLZ-C-REV-NULL
           END-IF
           IF IND-P67-IMP-ASSTO = -1
              MOVE HIGH-VALUES TO P67-IMP-ASSTO-NULL
           END-IF
           IF IND-P67-PRE-VERS = -1
              MOVE HIGH-VALUES TO P67-PRE-VERS-NULL
           END-IF
           IF IND-P67-DT-SCAD-COP = -1
              MOVE HIGH-VALUES TO P67-DT-SCAD-COP-NULL
           END-IF
           IF IND-P67-GG-DEL-MM-SCAD-RAT = -1
              MOVE HIGH-VALUES TO P67-GG-DEL-MM-SCAD-RAT-NULL
           END-IF
           IF IND-P67-DT-SCAD-1A-RAT = -1
              MOVE HIGH-VALUES TO P67-DT-SCAD-1A-RAT-NULL
           END-IF
           IF IND-P67-DT-EROG-FINANZ = -1
              MOVE HIGH-VALUES TO P67-DT-EROG-FINANZ-NULL
           END-IF
           IF IND-P67-DT-STIPULA-FINANZ = -1
              MOVE HIGH-VALUES TO P67-DT-STIPULA-FINANZ-NULL
           END-IF
           IF IND-P67-MM-PREAMM = -1
              MOVE HIGH-VALUES TO P67-MM-PREAMM-NULL
           END-IF
           IF IND-P67-IMP-DEB-RES = -1
              MOVE HIGH-VALUES TO P67-IMP-DEB-RES-NULL
           END-IF
           IF IND-P67-IMP-RAT-FINANZ = -1
              MOVE HIGH-VALUES TO P67-IMP-RAT-FINANZ-NULL
           END-IF
           IF IND-P67-IMP-CANONE-ANTIC = -1
              MOVE HIGH-VALUES TO P67-IMP-CANONE-ANTIC-NULL
           END-IF
           IF IND-P67-PER-RAT-FINANZ = -1
              MOVE HIGH-VALUES TO P67-PER-RAT-FINANZ-NULL
           END-IF
           IF IND-P67-TP-MOD-PAG-RAT = -1
              MOVE HIGH-VALUES TO P67-TP-MOD-PAG-RAT-NULL
           END-IF
           IF IND-P67-TP-FINANZ-ER = -1
              MOVE HIGH-VALUES TO P67-TP-FINANZ-ER-NULL
           END-IF
           IF IND-P67-CAT-FINANZ-ER = -1
              MOVE HIGH-VALUES TO P67-CAT-FINANZ-ER-NULL
           END-IF
           IF IND-P67-VAL-RISC-END-LEAS = -1
              MOVE HIGH-VALUES TO P67-VAL-RISC-END-LEAS-NULL
           END-IF
           IF IND-P67-DT-EST-FINANZ = -1
              MOVE HIGH-VALUES TO P67-DT-EST-FINANZ-NULL
           END-IF
           IF IND-P67-DT-MAN-COP = -1
              MOVE HIGH-VALUES TO P67-DT-MAN-COP-NULL
           END-IF
           IF IND-P67-NUM-FINANZ = -1
              MOVE HIGH-VALUES TO P67-NUM-FINANZ-NULL
           END-IF
           IF IND-P67-TP-MOD-ACQS = -1
              MOVE HIGH-VALUES TO P67-TP-MOD-ACQS-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO P67-DS-OPER-SQL
           MOVE 0                   TO P67-DS-VER
           MOVE IDSV0003-USER-NAME TO P67-DS-UTENTE
           MOVE '1'                   TO P67-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO P67-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO P67-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF P67-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-P67-ID-MOVI-CHIU
           END-IF
           IF P67-CPT-FIN-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-CPT-FIN
           ELSE
              MOVE 0 TO IND-P67-CPT-FIN
           END-IF
           IF P67-NUM-TST-FIN-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-NUM-TST-FIN
           ELSE
              MOVE 0 TO IND-P67-NUM-TST-FIN
           END-IF
           IF P67-TS-FINANZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-TS-FINANZ
           ELSE
              MOVE 0 TO IND-P67-TS-FINANZ
           END-IF
           IF P67-DUR-MM-FINANZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-DUR-MM-FINANZ
           ELSE
              MOVE 0 TO IND-P67-DUR-MM-FINANZ
           END-IF
           IF P67-DT-END-FINANZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-DT-END-FINANZ
           ELSE
              MOVE 0 TO IND-P67-DT-END-FINANZ
           END-IF
           IF P67-AMM-1A-RAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-AMM-1A-RAT
           ELSE
              MOVE 0 TO IND-P67-AMM-1A-RAT
           END-IF
           IF P67-VAL-RISC-BENE-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-VAL-RISC-BENE
           ELSE
              MOVE 0 TO IND-P67-VAL-RISC-BENE
           END-IF
           IF P67-AMM-RAT-END-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-AMM-RAT-END
           ELSE
              MOVE 0 TO IND-P67-AMM-RAT-END
           END-IF
           IF P67-TS-CRE-RAT-FINANZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-TS-CRE-RAT-FINANZ
           ELSE
              MOVE 0 TO IND-P67-TS-CRE-RAT-FINANZ
           END-IF
           IF P67-IMP-FIN-REVOLVING-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-IMP-FIN-REVOLVING
           ELSE
              MOVE 0 TO IND-P67-IMP-FIN-REVOLVING
           END-IF
           IF P67-IMP-UTIL-C-REV-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-IMP-UTIL-C-REV
           ELSE
              MOVE 0 TO IND-P67-IMP-UTIL-C-REV
           END-IF
           IF P67-IMP-RAT-REVOLVING-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-IMP-RAT-REVOLVING
           ELSE
              MOVE 0 TO IND-P67-IMP-RAT-REVOLVING
           END-IF
           IF P67-DT-1O-UTLZ-C-REV-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-DT-1O-UTLZ-C-REV
           ELSE
              MOVE 0 TO IND-P67-DT-1O-UTLZ-C-REV
           END-IF
           IF P67-IMP-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-IMP-ASSTO
           ELSE
              MOVE 0 TO IND-P67-IMP-ASSTO
           END-IF
           IF P67-PRE-VERS-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-PRE-VERS
           ELSE
              MOVE 0 TO IND-P67-PRE-VERS
           END-IF
           IF P67-DT-SCAD-COP-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-DT-SCAD-COP
           ELSE
              MOVE 0 TO IND-P67-DT-SCAD-COP
           END-IF
           IF P67-GG-DEL-MM-SCAD-RAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-GG-DEL-MM-SCAD-RAT
           ELSE
              MOVE 0 TO IND-P67-GG-DEL-MM-SCAD-RAT
           END-IF
           IF P67-DT-SCAD-1A-RAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-DT-SCAD-1A-RAT
           ELSE
              MOVE 0 TO IND-P67-DT-SCAD-1A-RAT
           END-IF
           IF P67-DT-EROG-FINANZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-DT-EROG-FINANZ
           ELSE
              MOVE 0 TO IND-P67-DT-EROG-FINANZ
           END-IF
           IF P67-DT-STIPULA-FINANZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-DT-STIPULA-FINANZ
           ELSE
              MOVE 0 TO IND-P67-DT-STIPULA-FINANZ
           END-IF
           IF P67-MM-PREAMM-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-MM-PREAMM
           ELSE
              MOVE 0 TO IND-P67-MM-PREAMM
           END-IF
           IF P67-IMP-DEB-RES-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-IMP-DEB-RES
           ELSE
              MOVE 0 TO IND-P67-IMP-DEB-RES
           END-IF
           IF P67-IMP-RAT-FINANZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-IMP-RAT-FINANZ
           ELSE
              MOVE 0 TO IND-P67-IMP-RAT-FINANZ
           END-IF
           IF P67-IMP-CANONE-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-IMP-CANONE-ANTIC
           ELSE
              MOVE 0 TO IND-P67-IMP-CANONE-ANTIC
           END-IF
           IF P67-PER-RAT-FINANZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-PER-RAT-FINANZ
           ELSE
              MOVE 0 TO IND-P67-PER-RAT-FINANZ
           END-IF
           IF P67-TP-MOD-PAG-RAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-TP-MOD-PAG-RAT
           ELSE
              MOVE 0 TO IND-P67-TP-MOD-PAG-RAT
           END-IF
           IF P67-TP-FINANZ-ER-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-TP-FINANZ-ER
           ELSE
              MOVE 0 TO IND-P67-TP-FINANZ-ER
           END-IF
           IF P67-CAT-FINANZ-ER-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-CAT-FINANZ-ER
           ELSE
              MOVE 0 TO IND-P67-CAT-FINANZ-ER
           END-IF
           IF P67-VAL-RISC-END-LEAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-VAL-RISC-END-LEAS
           ELSE
              MOVE 0 TO IND-P67-VAL-RISC-END-LEAS
           END-IF
           IF P67-DT-EST-FINANZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-DT-EST-FINANZ
           ELSE
              MOVE 0 TO IND-P67-DT-EST-FINANZ
           END-IF
           IF P67-DT-MAN-COP-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-DT-MAN-COP
           ELSE
              MOVE 0 TO IND-P67-DT-MAN-COP
           END-IF
           IF P67-NUM-FINANZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-NUM-FINANZ
           ELSE
              MOVE 0 TO IND-P67-NUM-FINANZ
           END-IF
           IF P67-TP-MOD-ACQS-NULL = HIGH-VALUES
              MOVE -1 TO IND-P67-TP-MOD-ACQS
           ELSE
              MOVE 0 TO IND-P67-TP-MOD-ACQS
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : P67-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE EST-POLI-CPI-PR TO WS-BUFFER-TABLE.

           MOVE P67-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO P67-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO P67-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO P67-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO P67-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO P67-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO P67-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO P67-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE EST-POLI-CPI-PR TO WS-BUFFER-TABLE.

           MOVE P67-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO EST-POLI-CPI-PR.

           MOVE WS-ID-MOVI-CRZ  TO P67-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO P67-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO P67-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO P67-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO P67-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO P67-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO P67-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE P67-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO P67-DT-INI-EFF-DB
           MOVE P67-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO P67-DT-END-EFF-DB
           IF IND-P67-DT-END-FINANZ = 0
               MOVE P67-DT-END-FINANZ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO P67-DT-END-FINANZ-DB
           END-IF
           IF IND-P67-DT-1O-UTLZ-C-REV = 0
               MOVE P67-DT-1O-UTLZ-C-REV TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO P67-DT-1O-UTLZ-C-REV-DB
           END-IF
           IF IND-P67-DT-SCAD-COP = 0
               MOVE P67-DT-SCAD-COP TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO P67-DT-SCAD-COP-DB
           END-IF
           IF IND-P67-DT-SCAD-1A-RAT = 0
               MOVE P67-DT-SCAD-1A-RAT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO P67-DT-SCAD-1A-RAT-DB
           END-IF
           IF IND-P67-DT-EROG-FINANZ = 0
               MOVE P67-DT-EROG-FINANZ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO P67-DT-EROG-FINANZ-DB
           END-IF
           IF IND-P67-DT-STIPULA-FINANZ = 0
               MOVE P67-DT-STIPULA-FINANZ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO P67-DT-STIPULA-FINANZ-DB
           END-IF
           IF IND-P67-DT-EST-FINANZ = 0
               MOVE P67-DT-EST-FINANZ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO P67-DT-EST-FINANZ-DB
           END-IF
           IF IND-P67-DT-MAN-COP = 0
               MOVE P67-DT-MAN-COP TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO P67-DT-MAN-COP-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE P67-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO P67-DT-INI-EFF
           MOVE P67-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO P67-DT-END-EFF
           IF IND-P67-DT-END-FINANZ = 0
               MOVE P67-DT-END-FINANZ-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO P67-DT-END-FINANZ
           END-IF
           IF IND-P67-DT-1O-UTLZ-C-REV = 0
               MOVE P67-DT-1O-UTLZ-C-REV-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO P67-DT-1O-UTLZ-C-REV
           END-IF
           IF IND-P67-DT-SCAD-COP = 0
               MOVE P67-DT-SCAD-COP-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO P67-DT-SCAD-COP
           END-IF
           IF IND-P67-DT-SCAD-1A-RAT = 0
               MOVE P67-DT-SCAD-1A-RAT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO P67-DT-SCAD-1A-RAT
           END-IF
           IF IND-P67-DT-EROG-FINANZ = 0
               MOVE P67-DT-EROG-FINANZ-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO P67-DT-EROG-FINANZ
           END-IF
           IF IND-P67-DT-STIPULA-FINANZ = 0
               MOVE P67-DT-STIPULA-FINANZ-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO P67-DT-STIPULA-FINANZ
           END-IF
           IF IND-P67-DT-EST-FINANZ = 0
               MOVE P67-DT-EST-FINANZ-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO P67-DT-EST-FINANZ
           END-IF
           IF IND-P67-DT-MAN-COP = 0
               MOVE P67-DT-MAN-COP-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO P67-DT-MAN-COP
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

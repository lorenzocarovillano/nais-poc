      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0560.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2010.
       DATE-COMPILED.
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0560'.
       01  LDBS4910                         PIC X(008) VALUE 'LDBS4910'.
       01  LDBS6000                         PIC X(008) VALUE 'LDBS6000'.
       01  LDBS5990                         PIC X(008) VALUE 'LDBS5990'.
       01  LDBS1530                         PIC X(008) VALUE 'LDBS1530'.
       01  LDBS5950                         PIC X(008) VALUE 'LDBS5950'.
42561  01  IDBSRIF0                         PIC X(008) VALUE 'IDBSRIF0'.
42561  01  IDBSRDF0                         PIC X(008) VALUE 'IDBSRDF0'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-VAL-FONDO                  PIC S9(12)V9(3) COMP-3 VALUE 0.
       01  WK-VAL-FONDO-2DEC             PIC S9(12)V9(2) COMP-3 VALUE 0.
      *
       01  WK-ID-COD-LIV-FLAG            PIC X(001).
           88 WK-ID-COD-TROVATO          VALUE 'S'.
           88 WK-ID-COD-NON-TROVATO      VALUE 'N'.
      *
       01 FLAG-FONDO-TROVATO             PIC X(001).
          88 FONDO-TROVATO-NO            VALUE 'N'.
          88 FONDO-TROVATO-SI            VALUE 'S'.
      *
       01 WK-ANN-COM-RIS-TOT             PIC 9(005) VALUE 93012.
       01 WK-ANN-LIQ-RIS-TOT             PIC 9(005) VALUE 95010.
       01 WK-ANN-COM-SIN                 PIC 9(005) VALUE 93019.
       01 WK-ANN-LIQ-SIN                 PIC 9(005) VALUE 95018.

       01 WK-ID-MOVI-FINRIO              PIC S9(009)V COMP-3.

       01 WK-TP-VAL-AST                  PIC X(002).
          88  VALORE-POSITIVO            VALUE 'VP'.
          88  ANNULLO-POSITIVO           VALUE 'AP'.
          88  VALORE-NEGATIVO            VALUE 'VN'.
          88  ANNULLO-NEGATIVO           VALUE 'AN'.
          88  VAS-LIQUIDAZIONE           VALUE 'LQ'.
          88  ANNULLO-LIQUIDAZIONE       VALUE 'AL'.

       01 WK-APPO-DATE.
          05 WK-APPO-DATA-INIZIO-EFFETTO PIC S9(008) COMP-3.
          05 WK-APPO-DATA-FINE-EFFETTO   PIC S9(008) COMP-3.
          05 WK-APPO-DATA-COMPETENZA     PIC S9(018) COMP-3.

       01 WK-TEMP-COD-FND                PIC X(020).

      *---------------------------------------------------------------*
      *  COPY TIPOLOGICHE
      *---------------------------------------------------------------*
10819      COPY LCCVXMVZ.
10819      COPY LCCVXMV2.
10819      COPY LCCVXMV5.

      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA VALORE ASSET
      *---------------------------------------------------------------*
           COPY IDBVVAS1.
           COPY IDBVMOV1.
           COPY IDBVMFZ1.
42561      COPY IDBVRIF1.
42561      COPY IDBVRDF1.

      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBS
      *---------------------------------------------------------------*
           COPY LDBV6001.
           COPY LDBV5991.
           COPY LDBV4911.

      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-TGA.
          03 DTGA-AREA-TGA.
             04 DTGA-ELE-TGA-MAX       PIC S9(04) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==DTGA==.
             COPY LCCVTGA1              REPLACING ==(SF)== BY ==DTGA==.

       01 AREA-IO-GAR.
          03 DGRZ-AREA-GRA.
             04 DGRZ-ELE-GAR-MAX        PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==DGRZ==.
             COPY LCCVGRZ1              REPLACING ==(SF)== BY ==DGRZ==.

       01 AREA-IO-L19.
          02 DL19-AREA-QUOTA.
             COPY LCCVL197              REPLACING ==(SF)== BY ==DL19==.
             COPY LCCVL191              REPLACING ==(SF)== BY ==DL19==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP VALUE 0.
           03 IX-TAB-GRZ                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-L19                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-TGA                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-VAS                     PIC S9(04) COMP VALUE 0.
           03 IX-GUIDA-GRZ                   PIC S9(04) COMP VALUE 0.
           03 IX-TAB-0501                    PIC S9(04) COMP VALUE 0.
           03 IX-TAB-TEMP                    PIC S9(04) COMP VALUE 0.
           03 IX-TAB-POS                     PIC S9(04) COMP VALUE 0.
      *----------------------------------------------------------------*
      *      per modulo di stringatura variabili                       *
      *----------------------------------------------------------------*
           COPY IDSV0501.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0213.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

       COPY IVVC0501.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003
                                INPUT-LVVS0213
                                IVVC0501-OUTPUT.
      *----------------------------------------------------------------*
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000
      *
           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.
      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT
                                             IDSV0501-INPUT
                                             IVVC0501-OUTPUT.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           INITIALIZE VAL-AST
                      AREA-IO-TGA
                      AREA-IO-GAR
                      AREA-IO-L19.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

           MOVE IVVC0213-TIPO-MOVI-ORIG
             TO WS-MOVIMENTO.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1200-CONTROLLO-DATI
                  THRU S1200-CONTROLLO-DATI-EX
      *
               IF  IDSV0003-SUCCESSFUL-RC
               AND IDSV0003-SUCCESSFUL-SQL
                   IF DGRZ-TP-INVST(IX-GUIDA-GRZ) = 7 OR 8
???I                  IF DL19-ELE-FND-MAX EQUAL ZERO
???                      CONTINUE
???F                  ELSE
                         IF LIQUI-RISPAR-POLIND
10819X                   OR LIQUI-RPP-REDDITO-PROGR
                         OR LIQUI-RISPAR-ADE
10819                    OR LIQUI-RPP-TAKE-PROFIT
                            PERFORM S1230-RECUP-ID-COMUN
                               THRU S1230-EX
31466                       PERFORM S1250-CALCOLA-QUOTE
31466                          THRU S1250-EX
31466                     IF WK-APPO-DATA-INIZIO-EFFETTO > ZEROES
31466                         MOVE WK-APPO-DATA-INIZIO-EFFETTO
31466                            TO IDSV0003-DATA-INIZIO-EFFETTO
31466                     END-IF
31466                     IF WK-APPO-DATA-FINE-EFFETTO > ZEROES
31466                        MOVE WK-APPO-DATA-FINE-EFFETTO
31466                           TO IDSV0003-DATA-FINE-EFFETTO
31466                     END-IF
31466                     IF WK-APPO-DATA-COMPETENZA > ZEROES
31466                        MOVE WK-APPO-DATA-COMPETENZA
31466                          TO IDSV0003-DATA-COMPETENZA
31466                     END-IF
                         ELSE
                            MOVE ZEROES
                              TO WK-ID-MOVI-FINRIO
31466                       PERFORM S1250-CALCOLA-QUOTE
31466                         THRU S1250-EX
                         END-IF
31466 *                  PERFORM S1250-CALCOLA-QUOTE     THRU S1250-EX
      *                  IF  IDSV0003-SUCCESSFUL-RC
      *                  AND IDSV0003-SUCCESSFUL-SQL
      *                     PERFORM S1260-SOTTRAI-ANNULLO
      *                        THRU S1260-EX
      *                  END-IF
                      END-IF
                  END-IF
               END-IF
           END-IF.
      *
      *    VALORIZZA LA VARIABILE A LISTA
           IF IDSV0003-SUCCESSFUL-RC
              PERFORM S1350-VAR-POSITIVA
                 THRU S1350-EX
           END-IF.
      *
      *    VALORIZZA LA VARIABILE A LISTA
           IF IDSV0003-SUCCESSFUL-RC
              PERFORM S1400-VALORIZZA-VARIABILE
                 THRU S1400-EX
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-TRCH-GAR
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DTGA-AREA-TGA
           END-IF.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-GARANZIA
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DGRZ-AREA-GRA
           END-IF.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-QOTAZ-FON
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DL19-AREA-QUOTA
           END-IF.
      *
       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO DATI DCLGEN
      *----------------------------------------------------------------*
       S1200-CONTROLLO-DATI.
      *
           SET WK-ID-COD-NON-TROVATO              TO TRUE.
      *
           IF DGRZ-ELE-GAR-MAX GREATER ZERO
      *
              PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
                UNTIL IX-TAB-GRZ > DGRZ-ELE-GAR-MAX
                   OR WK-ID-COD-TROVATO
                   IF DTGA-ID-GAR(IVVC0213-IX-TABB) =
                      DGRZ-ID-GAR(IX-TAB-GRZ)
                      SET WK-ID-COD-TROVATO     TO TRUE
                      MOVE IX-TAB-GRZ           TO IX-GUIDA-GRZ
                   END-IF
              END-PERFORM
      *
              IF WK-ID-COD-NON-TROVATO
                 SET  IDSV0003-INVALID-OPER               TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'GARANZIA NON VALORIZZATA PER CALCOLO QUOTE'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
      *
           ELSE
              SET  IDSV0003-INVALID-OPER                  TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'GARANZIA NON VALORIZZATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.
      *
       S1200-CONTROLLO-DATI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  RECUPERO L'ID DEL MOVIMENTO DI COMUNICAZIONE
      *----------------------------------------------------------------*
       S1230-RECUP-ID-COMUN.
      *
           INITIALIZE                      MOVI.
           SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.

           SET IDSV0003-SELECT             TO TRUE.
           SET IDSV0003-WHERE-CONDITION    TO TRUE.
           SET IDSV0003-TRATT-SENZA-STOR   TO TRUE.

           IF LIQUI-RISPAR-POLIND
              MOVE IVVC0213-ID-POLIZZA     TO MOV-ID-OGG
              MOVE 'PO'                    TO MOV-TP-OGG
              SET COMUN-RISPAR-IND         TO TRUE
              MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
           END-IF.

10819X     IF LIQUI-RPP-REDDITO-PROGR
10819X        MOVE IVVC0213-ID-POLIZZA     TO MOV-ID-OGG
10819X        MOVE 'PO'                    TO MOV-TP-OGG
10819X        SET RPP-REDDITO-PROGRAMMATO  TO TRUE
10819X        MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
10819X     END-IF.

           IF LIQUI-RISPAR-ADE
              MOVE IVVC0213-ID-ADESIONE    TO MOV-ID-OGG
              MOVE 'AD'                    TO MOV-TP-OGG
              SET COMUN-RISPAR-ADE         TO TRUE
              MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
           END-IF.

10819      IF LIQUI-RPP-TAKE-PROFIT
10819         MOVE IVVC0213-ID-POLIZZA     TO MOV-ID-OGG
10819         MOVE 'PO'                    TO MOV-TP-OGG
10819         SET RPP-TAKE-PROFIT          TO TRUE
10819         MOVE WS-MOVIMENTO            TO MOV-TP-MOVI
10819      END-IF.

           MOVE MOVI                       TO IDSV0003-BUFFER-WHERE-COND

           CALL LDBS1530  USING  IDSV0003 MOVI
           ON EXCEPTION
              MOVE LDBS1530
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'ERRORE CALL LDBS1530'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL

           EVALUATE TRUE
               WHEN IDSV0003-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CON SUCCESSO

                    PERFORM S1235-RECUP-MOVI-FINRIO
                       THRU S1235-EX

               WHEN IDSV0003-NOT-FOUND
      *-->     CHIAVE NON TROVATA
                    MOVE ZEROES
                      TO WK-ID-MOVI-FINRIO

               WHEN OTHER
      *-->     ERRORE DB
                  SET IDSV0003-INVALID-OPER  TO TRUE
                  MOVE WK-PGM
                    TO IDSV0003-COD-SERVIZIO-BE
                  STRING 'ERRORE LETTURA TABELLA LDBS1530 ;'
                        IDSV0003-RETURN-CODE ';'
                        IDSV0003-SQLCODE
                    DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                  END-STRING
           END-EVALUATE.
      *
           SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
      *
       S1230-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  RECUPERO L'ID DELLA MOVIMENTO FINANZIARIO DEL RISC. PARZIALE
      *----------------------------------------------------------------*
       S1235-RECUP-MOVI-FINRIO.
      *
           INITIALIZE                      MOVI-FINRIO.
           SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.

           SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.
           SET IDSV0003-WHERE-CONDITION    TO TRUE.
           SET IDSV0003-SELECT             TO TRUE.

           MOVE MOV-ID-MOVI                TO MFZ-ID-MOVI-CRZ

      *--> SALVO LE DATE DI CONTESTO PER RIPRISTINARLE DOPO L'ACCESSO
           INITIALIZE WK-APPO-DATE
           IF  IDSV0003-DATA-INIZIO-EFFETTO IS NUMERIC
           AND IDSV0003-DATA-INIZIO-EFFETTO > ZEROES
               MOVE IDSV0003-DATA-INIZIO-EFFETTO
                 TO WK-APPO-DATA-INIZIO-EFFETTO
           END-IF
           IF  IDSV0003-DATA-FINE-EFFETTO IS NUMERIC
           AND IDSV0003-DATA-FINE-EFFETTO > ZEROES
               MOVE IDSV0003-DATA-FINE-EFFETTO
                 TO WK-APPO-DATA-FINE-EFFETTO
           END-IF
           IF  IDSV0003-DATA-COMPETENZA IS NUMERIC
           AND IDSV0003-DATA-COMPETENZA > ZEROES
               MOVE IDSV0003-DATA-COMPETENZA
                 TO WK-APPO-DATA-COMPETENZA
           END-IF

           MOVE MOV-DT-EFF               TO IDSV0003-DATA-INIZIO-EFFETTO
                                            IDSV0003-DATA-FINE-EFFETTO
           MOVE MOV-DS-TS-CPTZ           TO IDSV0003-DATA-COMPETENZA

           MOVE MOVI-FINRIO                TO IDSV0003-BUFFER-WHERE-COND

           CALL LDBS5950  USING  IDSV0003 MOVI-FINRIO
           ON EXCEPTION
              MOVE LDBS5950
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'ERRORE CALL LDBS5950'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL

           EVALUATE TRUE
               WHEN IDSV0003-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CON SUCCESSO
                    MOVE MFZ-ID-MOVI-FINRIO
                      TO WK-ID-MOVI-FINRIO

               WHEN IDSV0003-NOT-FOUND
      *-->     CHIAVE NON TROVATA
                    MOVE ZEROES
                      TO WK-ID-MOVI-FINRIO

               WHEN OTHER
      *-->     ERRORE DB
                  SET IDSV0003-INVALID-OPER  TO TRUE
                  MOVE WK-PGM
                    TO IDSV0003-COD-SERVIZIO-BE
                  STRING 'ERRORE LETTURA TABELLA LDBS1530 ;'
                        IDSV0003-RETURN-CODE ';'
                        IDSV0003-SQLCODE
                    DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                  END-STRING
           END-EVALUATE.

      *--> RIPRISTINO LE DATE DI CONTESTO
31466 *    IF WK-APPO-DATA-INIZIO-EFFETTO > ZEROES
31466 *       MOVE WK-APPO-DATA-INIZIO-EFFETTO
31466 *         TO IDSV0003-DATA-INIZIO-EFFETTO
31466 *    END-IF.
31466 *    IF WK-APPO-DATA-FINE-EFFETTO > ZEROES
31466 *       MOVE WK-APPO-DATA-FINE-EFFETTO
31466 *         TO IDSV0003-DATA-FINE-EFFETTO
31466 *    END-IF.
31466 *    IF WK-APPO-DATA-COMPETENZA > ZEROES
31466 *       MOVE WK-APPO-DATA-COMPETENZA
31466 *         TO IDSV0003-DATA-COMPETENZA
31466 *    END-IF.
      *
       S1235-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATA 1
      *----------------------------------------------------------------*
       S1250-CALCOLA-QUOTE.
      *
           SET IDSV0003-WHERE-CONDITION        TO TRUE.
           SET IDSV0003-FETCH-FIRST            TO TRUE.
           SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE.
      *
           INITIALIZE LDBV4911
           MOVE IVVC0213-ID-TRANCHE   TO LDBV4911-ID-TRCH-DI-GAR
           MOVE 'LQ'                  TO LDBV4911-TP-VAL-AST-1
           MOVE 'AL'                  TO LDBV4911-TP-VAL-AST-2
           MOVE LDBV4911              TO IDSV0003-BUFFER-WHERE-COND.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR NOT IDSV0003-SUCCESSFUL-SQL
      *
              CALL LDBS4910  USING  IDSV0003 VAL-AST
              ON EXCEPTION
                 MOVE LDBS4910
                   TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'ERRORE CALL LDBS4910'
                   TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER  TO TRUE
              END-CALL
      *
              EVALUATE TRUE
                 WHEN IDSV0003-NOT-FOUND
      *-->          CHIAVE NON TROVATA
                    IF IDSV0003-FETCH-FIRST
                       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                       MOVE WK-PGM
                         TO IDSV0003-COD-SERVIZIO-BE
                       MOVE 'LETTURA VALORE ASSETT - SQLCODE = 100 '
                         TO IDSV0003-DESCRIZ-ERR-DB2
                    END-IF
                 WHEN IDSV0003-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                    PERFORM S1251-CALCOLA-QUOTE
                       THRU S1251-EX

42561               SET IDSV0003-WHERE-CONDITION        TO TRUE
                    SET IDSV0003-FETCH-NEXT TO TRUE
      *
                 WHEN OTHER
                    SET IDSV0003-INVALID-OPER  TO TRUE
                    MOVE WK-PGM
                      TO IDSV0003-COD-SERVIZIO-BE
                    STRING 'ERRORE LETTURA TABELLA VALORE ASSET ;'
                          IDSV0003-RETURN-CODE ';'
                          IDSV0003-SQLCODE
                      DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    END-STRING
              END-EVALUATE
      *
           END-PERFORM.
      *
       S1250-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   LETTURA VALORIZZAZIONE VALORE ASSETT
      *----------------------------------------------------------------*
       S1251-CALCOLA-QUOTE.
      *
           SET WK-ID-COD-NON-TROVATO           TO TRUE.
      *
           PERFORM VARYING IX-TAB-L19 FROM 1 BY 1
             UNTIL IX-TAB-L19 > DL19-ELE-FND-MAX
                OR WK-ID-COD-TROVATO

                IF VAS-COD-FND = DL19-COD-FND(IX-TAB-L19)
                   SET WK-ID-COD-TROVATO       TO TRUE
      *
                   IF DL19-VAL-QUO(IX-TAB-L19) IS NUMERIC

      *--> PER IL PRIMO FONDO CHE HA NUM QUOTE A NULL ESCE DAL CICLO
                      IF VAS-NUM-QUO IS NUMERIC
                         IF VAS-ID-MOVI-FINRIO = WK-ID-MOVI-FINRIO
                            CONTINUE
                         ELSE
                            MOVE ZERO          TO WK-VAL-FONDO
                            COMPUTE WK-VAL-FONDO = (VAS-NUM-QUO *
                                              DL19-VAL-QUO(IX-TAB-L19))
                            COMPUTE WK-VAL-FONDO-2DEC ROUNDED =
                                    WK-VAL-FONDO * 1

42561                       PERFORM S1254-VALORIZZA-LISTA
42561                          THRU S1254-EX

                         END-IF
42561                 ELSE

42561                    IF  VAS-ID-RICH-INVST-FND IS NUMERIC
42561                    AND VAS-ID-RICH-INVST-FND > ZERO
42561                        PERFORM S1252-RECUP-IMP-INVES
42561                           THRU S1252-EX
42561                    END-IF
42561                    IF  VAS-ID-RICH-DIS-FND IS NUMERIC
42561                    AND VAS-ID-RICH-DIS-FND > ZERO
42561                        PERFORM S1253-RECUP-IMP-DISIN
42561                           THRU S1253-EX
42561                    END-IF

                      END-IF
      *
                   ELSE
      *
                      MOVE LDBS4910
                        TO IDSV0003-COD-SERVIZIO-BE
                      MOVE 'NUM-QUO / VAL-QUO NON NUMERICI'
                        TO IDSV0003-DESCRIZ-ERR-DB2
                       SET IDSV0003-INVALID-OPER  TO TRUE
                   END-IF
                END-IF
           END-PERFORM.
      *
       S1251-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RECUPERA RICHIESTA DI INVESTIMENTO
      *----------------------------------------------------------------*
42561  S1252-RECUP-IMP-INVES.

           INITIALIZE RICH-INVST-FND.

           SET  IDSV0003-ID                 TO TRUE.
           SET  IDSV0003-SELECT             TO TRUE.
           SET  IDSV0003-SUCCESSFUL-RC      TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL     TO TRUE.
           SET  IDSV0003-TRATT-X-COMPETENZA TO TRUE.

           MOVE VAS-ID-RICH-INVST-FND TO RIF-ID-RICH-INVST-FND.

           CALL   IDBSRIF0 USING IDSV0003 RICH-INVST-FND.

           IF IDSV0003-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSV0003-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       IF RIF-IMP-MOVTO IS NUMERIC
                       AND RIF-IMP-MOVTO > ZERO
                          MOVE RIF-IMP-MOVTO
                            TO WK-VAL-FONDO-2DEC
                          PERFORM S1254-VALORIZZA-LISTA
                             THRU S1254-EX
                       END-IF
                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                    SET IDSV0003-INVALID-OPER  TO TRUE
                    MOVE WK-PGM
                      TO IDSV0003-COD-SERVIZIO-BE
                    STRING 'ERRORE LETTURA TABELLA RICH_INVST_FND ;'
                          IDSV0003-RETURN-CODE ';'
                          IDSV0003-SQLCODE
                      DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    END-STRING
              END-EVALUATE
           ELSE
      *--> GESTIRE ERRORE
              SET IDSV0003-INVALID-OPER  TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING 'ERRORE LETTURA TABELLA RICH_INVST_FND ;'
                    IDSV0003-RETURN-CODE ';'
                    IDSV0003-SQLCODE
                DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.

       S1252-EX.
           EXIT.
      **---------------------------------------------------------------*
      *    RECUPERA RICHIESTA DI DISINVESTIMENTO
      *----------------------------------------------------------------*
42561  S1253-RECUP-IMP-DISIN.

           INITIALIZE RICH-DIS-FND.

           SET  IDSV0003-ID                 TO TRUE.
           SET  IDSV0003-SELECT             TO TRUE.
           SET  IDSV0003-SUCCESSFUL-RC      TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL     TO TRUE.
           SET  IDSV0003-TRATT-X-COMPETENZA TO TRUE.

           MOVE VAS-ID-RICH-DIS-FND
             TO RDF-ID-RICH-DIS-FND.

           CALL   IDBSRDF0 USING IDSV0003 RICH-DIS-FND.

           IF IDSV0003-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSV0003-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       IF RDF-IMP-MOVTO IS NUMERIC
                       AND RDF-IMP-MOVTO > ZERO
                          MOVE RDF-IMP-MOVTO
                            TO WK-VAL-FONDO-2DEC
                          PERFORM S1254-VALORIZZA-LISTA
                             THRU S1254-EX
                       END-IF
                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                    SET IDSV0003-INVALID-OPER  TO TRUE
                    MOVE WK-PGM
                      TO IDSV0003-COD-SERVIZIO-BE
                    STRING 'ERRORE LETTURA TABELLA RICH_DISINV_FND ;'
                          IDSV0003-RETURN-CODE ';'
                          IDSV0003-SQLCODE
                      DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    END-STRING
              END-EVALUATE
           ELSE
      *--> GESTIRE ERRORE
              SET IDSV0003-INVALID-OPER  TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING 'ERRORE LETTURA TABELLA RICH_DISINV_FND ;'
                    IDSV0003-RETURN-CODE ';'
                    IDSV0003-SQLCODE
                DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.

       S1253-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA VARIABILE A LISTA
      *----------------------------------------------------------------*
42561  S1254-VALORIZZA-LISTA.

           MOVE VAS-TP-VAL-AST TO WK-TP-VAL-AST

           SET FONDO-TROVATO-NO TO TRUE
           PERFORM VARYING IX-TAB-TEMP FROM 1 BY 1
           UNTIL IX-TAB-TEMP > IX-TAB-0501
           OR FONDO-TROVATO-SI
              IF IDSV0501-TP-DATO(IX-TAB-TEMP) = 'L'
                 MOVE IDSV0501-VAL-STR(IX-TAB-TEMP)
                   TO WK-TEMP-COD-FND
                 IF VAS-COD-FND = WK-TEMP-COD-FND
                    SET FONDO-TROVATO-SI TO TRUE
                    ADD 1 TO IX-TAB-TEMP
                    PERFORM S1310-VALOR-LISTA-IMP
                       THRU S1310-EX
                 END-IF
              END-IF
           END-PERFORM.

           IF FONDO-TROVATO-NO
              PERFORM S1300-VALOR-VARLIST
                 THRU S1300-EX
           END-IF.

       S1254-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ESTRAI MOVIMENTI
      *----------------------------------------------------------------*
       S1260-SOTTRAI-ANNULLO.

           INITIALIZE                       LDBV5991.
           SET IDSV0003-SUCCESSFUL-RC       TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL      TO TRUE.
           SET IDSV0003-SELECT              TO TRUE.

           SET IDSV0003-WHERE-CONDITION     TO TRUE
           SET IDSV0003-TRATT-SENZA-STOR    TO TRUE

           MOVE IVVC0213-ID-POLIZZA         TO MOV-ID-OGG
           MOVE 'PO'                        TO MOV-TP-OGG
           MOVE WK-ANN-COM-RIS-TOT          TO LDBV5991-TP-MOV1
           MOVE WK-ANN-LIQ-RIS-TOT          TO LDBV5991-TP-MOV2
           MOVE WK-ANN-COM-SIN              TO LDBV5991-TP-MOV3
           MOVE WK-ANN-LIQ-SIN              TO LDBV5991-TP-MOV4
           MOVE ZEROES                      TO LDBV5991-TP-MOV5
                                               LDBV5991-TP-MOV6
                                               LDBV5991-TP-MOV7
                                               LDBV5991-TP-MOV8
                                               LDBV5991-TP-MOV9
                                               LDBV5991-TP-MOV10

           MOVE LDBV5991                   TO IDSV0003-BUFFER-WHERE-COND

           CALL LDBS5990  USING  IDSV0003 MOVI
           ON EXCEPTION
              MOVE LDBS5990
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'ERRORE CALL LDBS5990'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL

           EVALUATE TRUE
               WHEN IDSV0003-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CON SUCCESSO
                    PERFORM S1261-SOTTRAI-QUOTE
                       THRU S1261-EX

               WHEN IDSV0003-NOT-FOUND
      *-->     CHIAVE NON TROVATA
                  CONTINUE

               WHEN OTHER
      *-->     ERRORE DB
                  SET IDSV0003-INVALID-OPER  TO TRUE
                  MOVE WK-PGM
                    TO IDSV0003-COD-SERVIZIO-BE
                  STRING 'ERRORE LETTURA TABELLA LDBS5990 ;'
                        IDSV0003-RETURN-CODE ';'
                        IDSV0003-SQLCODE
                    DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                  END-STRING
           END-EVALUATE.

       S1260-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATA 1
      *----------------------------------------------------------------*
       S1261-SOTTRAI-QUOTE.
      *
           SET IDSV0003-WHERE-CONDITION       TO TRUE.
           SET IDSV0003-FETCH-FIRST           TO TRUE.
           SET IDSV0003-TRATT-X-COMPETENZA    TO TRUE.
      *
           INITIALIZE LDBV6001
           MOVE IVVC0213-ID-TRANCHE     TO LDBV6001-ID-TRCH-DI-GAR
           MOVE 'LQ'                    TO LDBV6001-TP-VAL-AST-1
           MOVE 'AL'                    TO LDBV6001-TP-VAL-AST-2
           MOVE MOV-DS-TS-CPTZ          TO LDBV6001-ANN-DS-TS-CPTZ
           MOVE LDBV6001                TO IDSV0003-BUFFER-WHERE-COND.
      *
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR NOT IDSV0003-SUCCESSFUL-SQL
      *
              CALL LDBS6000  USING  IDSV0003 VAL-AST
              ON EXCEPTION
                 MOVE LDBS6000
                   TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'ERRORE CALL LDBS6000'
                   TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER  TO TRUE
              END-CALL
      *
              EVALUATE TRUE
                 WHEN IDSV0003-NOT-FOUND
      *-->          CHIAVE NON TROVATA
                    CONTINUE
                 WHEN IDSV0003-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                    PERFORM S1262-CALCOLA-QUOTE
                       THRU S1262-EX

                    SET IDSV0003-FETCH-NEXT TO TRUE
      *
                 WHEN OTHER
                    SET IDSV0003-INVALID-OPER  TO TRUE
                    MOVE WK-PGM
                      TO IDSV0003-COD-SERVIZIO-BE
                    STRING 'ERRORE LETTURA TABELLA VALORE ASSET ;'
                          IDSV0003-RETURN-CODE ';'
                          IDSV0003-SQLCODE
                      DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    END-STRING
              END-EVALUATE
      *
           END-PERFORM.
      *
       S1261-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   LETTURA VALORIZZAZIONE VALORE ASSETT
      *----------------------------------------------------------------*
       S1262-CALCOLA-QUOTE.
      *
           SET WK-ID-COD-NON-TROVATO         TO TRUE.
      *
           PERFORM VARYING IX-TAB-L19 FROM 1 BY 1
             UNTIL IX-TAB-L19 > DL19-ELE-FND-MAX
                OR WK-ID-COD-TROVATO

                IF VAS-COD-FND = DL19-COD-FND(IX-TAB-L19)
                   SET WK-ID-COD-TROVATO        TO TRUE
      *
                   IF DL19-VAL-QUO(IX-TAB-L19) IS NUMERIC
      *--> PER IL PRIMO FONDO CHE HA NUM QUOTE A NULL ESCE DAL CICLO
                      IF VAS-NUM-QUO IS NUMERIC
                         MOVE ZERO              TO WK-VAL-FONDO
                         COMPUTE WK-VAL-FONDO = (VAS-NUM-QUO *
                                              DL19-VAL-QUO(IX-TAB-L19))
                         COMPUTE WK-VAL-FONDO-2DEC ROUNDED =
                                 WK-VAL-FONDO * 1

                         SET FONDO-TROVATO-NO TO TRUE
                         PERFORM VARYING IX-TAB-TEMP FROM 1 BY 1
                         UNTIL IX-TAB-TEMP > IX-TAB-0501
                         OR FONDO-TROVATO-SI
                            IF IDSV0501-TP-DATO(IX-TAB-TEMP) = 'L'
                               MOVE IDSV0501-VAL-STR(IX-TAB-TEMP)
                                 TO WK-TEMP-COD-FND
                               IF VAS-COD-FND = WK-TEMP-COD-FND
                                  SET FONDO-TROVATO-SI TO TRUE
                                  ADD 1 TO IX-TAB-TEMP
                                  PERFORM S1320-SOTTR-ANNULLO-LISTA
                                     THRU S1320-EX
                               END-IF
                            END-IF
                         END-PERFORM
                      END-IF
                   ELSE
                      MOVE LDBS4910
                        TO IDSV0003-COD-SERVIZIO-BE
                      MOVE 'NUM-QUO / VAL-QUO NON NUMERICI'
                        TO IDSV0003-DESCRIZ-ERR-DB2
                       SET IDSV0003-INVALID-OPER  TO TRUE
                   END-IF
                END-IF
           END-PERFORM.
      *
       S1262-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   VALORIZZA VARIABILI A LISTA
      *----------------------------------------------------------------*
       S1300-VALOR-VARLIST.
      *
           IF IX-TAB-0501 < LIMITE-ARRAY-INPUT
              ADD 1                    TO IX-TAB-0501
              MOVE 'L'                 TO IDSV0501-TP-DATO(IX-TAB-0501)
              MOVE VAS-COD-FND
                TO IDSV0501-VAL-STR(IX-TAB-0501)
           ELSE
              MOVE 'VARLIST'
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'LVVS0560 - LIMITE-ARRAY-INPUT SUPERATO'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER       TO TRUE
           END-IF.

           IF IX-TAB-0501 < LIMITE-ARRAY-INPUT
              ADD 1                    TO IX-TAB-0501
              MOVE 'R'                 TO IDSV0501-TP-DATO(IX-TAB-0501)
              IF VALORE-POSITIVO
              OR ANNULLO-NEGATIVO
                 COMPUTE IDSV0501-VAL-IMP(IX-TAB-0501) =
                         IDSV0501-VAL-IMP(IX-TAB-0501) +
                         WK-VAL-FONDO-2DEC
              ELSE
                 IF VALORE-NEGATIVO
                 OR ANNULLO-POSITIVO
                    COMPUTE IDSV0501-VAL-IMP(IX-TAB-0501) =
                            IDSV0501-VAL-IMP(IX-TAB-0501) -
                            WK-VAL-FONDO-2DEC
                 END-IF
              END-IF
           ELSE
              MOVE 'VARLIST'
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'LVVS0560 - LIMITE-ARRAY-INPUT SUPERATO'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER       TO TRUE
           END-IF.
      *
       S1300-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   VALORIZZA VARIABILI A LISTA DI TIPO IMPORTO
      *----------------------------------------------------------------*
       S1310-VALOR-LISTA-IMP.

           IF VALORE-POSITIVO
           OR ANNULLO-NEGATIVO
              COMPUTE IDSV0501-VAL-IMP(IX-TAB-TEMP) =
                      IDSV0501-VAL-IMP(IX-TAB-TEMP) +
                      WK-VAL-FONDO-2DEC
           ELSE
              IF VALORE-NEGATIVO
              OR ANNULLO-POSITIVO
                 COMPUTE IDSV0501-VAL-IMP(IX-TAB-TEMP) =
                         IDSV0501-VAL-IMP(IX-TAB-TEMP) -
                         WK-VAL-FONDO-2DEC
              END-IF
           END-IF.
      *
       S1310-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   VALORIZZA VARIABILI A LISTA DI TIPO IMPORTO
      *----------------------------------------------------------------*
       S1320-SOTTR-ANNULLO-LISTA.
      *
           COMPUTE IDSV0501-VAL-IMP(IX-TAB-TEMP) =
                   IDSV0501-VAL-IMP(IX-TAB-TEMP) -
                   WK-VAL-FONDO-2DEC.
      *
       S1320-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   VALORIZZO LA VARIABILE A LISTA
      *----------------------------------------------------------------*
       S1350-VAR-POSITIVA.
      *
           PERFORM VARYING IX-TAB-POS FROM 1 BY 1
             UNTIL IX-TAB-POS > IX-TAB-0501
                OR IDSV0501-TP-DATO(IX-TAB-POS) = SPACES
                   IF  IDSV0501-VAL-IMP(IX-TAB-POS) IS NUMERIC
                   AND IDSV0501-VAL-IMP(IX-TAB-POS) < ZERO
                       COMPUTE IDSV0501-VAL-IMP(IX-TAB-POS) =
                               IDSV0501-VAL-IMP(IX-TAB-POS) * -1
                   END-IF
           END-PERFORM.
      *
       S1350-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   VALORIZZO LA VARIABILE A LISTA
      *----------------------------------------------------------------*
       S1400-VALORIZZA-VARIABILE.

           SET IDSV0003-SUCCESSFUL-RC  TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL TO TRUE.

           IF IDSV0003-SUCCESSFUL-RC
              PERFORM STRINGA-X-VALORIZZATORE
                 THRU STRINGA-X-VALORIZZATORE-EX

              IF IDSV0501-SUCCESSFUL-RC
                 MOVE IDSV0501-OUTPUT
                   TO IVVC0501-OUTPUT
              ELSE
                 SET IDSV0003-FIELD-NOT-VALUED TO TRUE
              END-IF
           END-IF.

       S1400-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *   CODICE PER STRINGATURA VARIABILI
      *----------------------------------------------------------------*
           COPY IVVP0501.

      *****************************************************************
      **                                                              *
      **    PORTAFOGLIO VITA ITALIA VER. 1.0                          *
      **                                                              *
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LCCS0005.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      **--------------------------------------------------------------**
      *    PROGRAMMA ..... LCCS0005
      *    TIPOLOGIA...... EOC COMUNE - Per tutte le funzionalita'
      *    PROCESSO....... XXX
      *    FUNZIONE....... XXX
      *    DESCRIZIONE.... Gestione del movimento, richiesta e Oggetti
      *                    Deroga
      **--------------------------------------------------------------**
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01 WK-PGM                        PIC X(08) VALUE 'LCCS0005'.
       01 LCCS0025                      PIC X(08) VALUE 'LCCS0025'.
       01 LCCS0070                      PIC X(08) VALUE 'LCCS0070'.

       77 PGM-IDSS0150                  PIC X(08) VALUE 'IDSS0150'.
       77 IDBSP040                      PIC X(08) VALUE 'IDBSP040'.
       77 LDBS8170                      PIC X(08) VALUE 'LDBS8170'.
       77 IDBSP010                      PIC X(08) VALUE 'IDBSP010'.

      *--  COPY INFRASTRUTTURALE
           COPY IDSV0010.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01 WK-TABELLA                    PIC X(30) VALUE SPACES.
       01 WK-ID-OGG                      PIC S9(009)V COMP-3 VALUE ZERO.
       01 WK-TP-OGG                      PIC X(002) VALUE SPACES.
       01 WK-STAT-OGG-WF                 PIC X(002) VALUE SPACES.
       01 WK-STAT-END-WF                 PIC X(001) VALUE SPACES.
       01 TP-COLLETTIVA                  PIC X(002) VALUE 'CO'.
       01 TP-INDIVIDUALE                 PIC X(002) VALUE 'IN'.
       01 WK-COD-LIV-AUT-SUP             PIC S9(05) COMP-3.
       01 WK-COD-GR-AUT-SUP              PIC S9(10) COMP-3.
       01 WS-GAR-ID-PTF                  PIC S9(09) COMP-3 VALUE ZEROES.
       01 WK-LIVELLO-DEBUG               PIC  9.

       01 WS-TP-OGG.
          05 WS-POLIZZA                  PIC X(02) VALUE 'PO'.
          05 WS-ADESIONE                 PIC X(02) VALUE 'AD'.
          05 WS-GARANZIA                 PIC X(02) VALUE 'GA'.
          05 WS-TRANCHE                  PIC X(02) VALUE 'TG'.

       01 WS-CICLO                        PIC X(002) VALUE SPACES.
          88 WS-SI-FINE-CICLO                        VALUE 'SI'.
          88 WS-NO-FINE-CICLO                        VALUE 'NO'.

       01 WS-TROVATO                      PIC X(002) VALUE SPACES.
          88 TROVATO-SI                              VALUE 'SI'.
          88 TROVATO-NO                              VALUE 'NO'.

       01  WK-VARIABILI.
           03 WS-OGGETTO.
             05 WS-COD-COMPAGNIA            PIC X(005) VALUE SPACES.
             05 WS-SEQUENCE-PTF             PIC X(009) JUST RIGHT
                                                       VALUE SPACES.
           03 WS-RICHIESTA                  PIC X(040) JUST RIGHT
                                                       VALUE SPACES.
           03 WS-MOVIMENTO-APPO              PIC X(040) JUST RIGHT
                                                       VALUE SPACES.
           03 WS-ID-RICH-PTF                PIC 9(009) VALUE ZERO.
           03 WS-ELE-ULT-PROG               PIC S9(004) COMP.
           03 WS-ID-MOVI-PTF                PIC 9(009) VALUE ZERO.

      *--  OGGETTO DEROGA
           03 WODE-AREA-OGG-DEROGA.
              04 WODE-ELE-ODE-MAX             PIC S9(04) COMP.
              04 WODE-TAB-ODE.
              COPY LCCVODE1                   REPLACING ==(SF)==
                                              BY ==WODE==.
      *--  MOTIVI DEROGA
           03 WMDE-AREA-MOT-DEROGA.
              04 WMDE-ELE-MDE-MAX             PIC S9(04) COMP.
              04 WMDE-TAB-MDE                 OCCURS 20.
              COPY LCCVMDE1                   REPLACING ==(SF)==
                                              BY ==WMDE==.

      *--> AREA PREVENTIVO
           03 WL27-AREA-PREVENTIVO.
              04 WL27-ELE-PREVENTIVO-MAX   PIC S9(04) COMP.
              04 WL27-TAB-PREVENTIVO.
              COPY LCCVL271                REPLACING ==(SF)==
                                           BY ==WL27==.
      *----------------------------------------------------------------*
      *    DEFINIZIONE DI INDICI
      *----------------------------------------------------------------*
       01 WS-INDICI.
          05 IX-TAB-MDE                    PIC S9(004) COMP VALUE ZERO.
          05 IX-TAB-TGA                    PIC S9(004) COMP VALUE ZERO.
          05 IX-TAB-GRZ                    PIC S9(004) COMP VALUE ZERO.

      *----------------------------------------------------------------*
      *    Gestioned delle Tipo Logiche
      *----------------------------------------------------------------*
           COPY LCCVXMV0.
           COPY LCCC0006.
           COPY LCCVXCS0.
      *----------------------------------------------------------------*
      *    COPY DISPATCHER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
           COPY IDSI0011.
           COPY IDSO0011.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.

      *----------------------------------------------------------------
      *    MODULI CHIAMATI
      *----------------------------------------------------------------
      *------  ESTRAZIONE SEQUENCE
       01  LCCS0090                         PIC X(008) VALUE 'LCCS0090'.
      *------  ESTRAZIONE OGGETTO PTF
       01  LCCS0234                         PIC X(008) VALUE 'LCCS0234'.
      *--> Estrazione Ib-Oggetto PREVENTIVO
       01 IDSS0160                          PIC X(008) VALUE 'IDSS0160'.
      *----------------------------------------------------------------*
      *    AREE MODULI CHIAMATI
      *----------------------------------------------------------------*
      *------- Estrazione Sequence     -------------------------------
       01  AREA-IO-LCCS0090.
           COPY LCCC0090                 REPLACING ==(SF)==
                                         BY ==S090==.

      *------- Estrazione Oggetto PTF (Oggetto Master) ----------------*
      *--> AREA I/O LCCS0234
       01 S234-DATI-INPUT.
            04 S234-ID-OGG-EOC         PIC S9(009).
            04 S234-TIPO-OGG-EOC       PIC X(002).
       01 S234-DATI-OUTPUT.
            04 S234-ID-OGG-PTF-EOC     PIC S9(009).
            04 S234-IB-OGG-PTF-EOC     PIC X(040).

      *------- Estrazione IB-Oggetto   -------------------------------
      *--  AREA LIQUIDAZIONE
       01 S089-AREA-LIQUIDAZIONE.
              04 S089-ELE-LIQUID-MAX   PIC S9(04) COMP.
                COPY LCCVLQUA REPLACING   ==(SF)==  BY ==S089==.
              COPY LCCVLQU1            REPLACING ==(SF)== BY ==S089==.

      *----------------------------------------------------------------*
       01 LCCC0025-AREA-COMUNICAZ.
          COPY LCCC0025.

      *----------------------------------------------------------------
       01  AREA-IO-LCCS0070.
           COPY LCCC0070.

      *----------------------------------------------------------------*
      *    VARIABILI DI WORKING PER ROUTINES OPERAZIONI SULLE STRINGHE
      *----------------------------------------------------------------*
           COPY LVEC0202.

      *----------------------------------------------------------------*
      *--> GESTIONE IB_OGG
      *----------------------------------------------------------------*
       01 AREA-ESTRAZIONE-IB.
           COPY IDSV0161               REPLACING ==(SF)== BY ==C161==.
      *
       01 AREA-ALIAS.
           COPY IVVC0218               REPLACING ==(SF)== BY ==C161==.

      *----------------------------------------------------------------*
      *    COPY TABELLE DB2
      *----------------------------------------------------------------*
           COPY IDBVRIC1.
           COPY IDBVMOV1.
           COPY IDBVSTW1.
           COPY IDBVMDE1.
           COPY IDBVODE1.
           COPY IDBVP041.
           COPY IDBVP011.
           COPY IDBVNOT1.

      *---------------------------------------------------------------*
       LINKAGE SECTION.
      *---------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.
      *----------------------------------------------------------------*
      *    COMMAREA SERVIZIO
      *----------------------------------------------------------------*
      *--  COMUNE
       01  WCOM-AREA-STATI.
           COPY LCCC0001                   REPLACING ==(SF)==
                                           BY ==WCOM==.
      *--  MOVIMENTO
       01  WMOV-AREA-MOVIMENTO.
           04 WMOV-ELE-MOV-MAX             PIC S9(004) COMP.
           04 WMOV-TAB-MOV.
           COPY LCCVMOV1                   REPLACING ==(SF)==
                                           BY ==WMOV==.
      *--  RICHIESTA
       01  WRIC-AREA-RICHIESTA.
           04 WRIC-ELE-RICH-MAX            PIC S9(004) COMP.
           04 WRIC-TAB-RICH.
              COPY LCCVRIC1                REPLACING ==(SF)==
                                           BY ==WRIC==.
      *--  POLIZZA
       01  WPOL-AREA-POLIZZA.
           04 WPOL-ELE-POLI-MAX            PIC S9(04) COMP.
           04 WPOL-TAB-POLI.
           COPY LCCVPOL1                   REPLACING ==(SF)==
                                           BY ==WPOL==.
      *--  ADESIONE
       01  WADE-AREA-ADESIONE.
           04 WADE-ELE-ADES-MAX            PIC S9(004) COMP.
           04 WADE-TAB-ADES.
           COPY LCCVADE1                   REPLACING ==(SF)==
                                           BY ==WADE==.
      *--  GARANZIA
       01  WGRZ-AREA-GARANZIA.
           04 WGRZ-ELE-GAR-MAX             PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
           COPY LCCVGRZ1                   REPLACING ==(SF)==
                                           BY ==WGRZ==.
      *--  TRANCHE DI GARANZIA
       01  WTGA-AREA-TRANCHE.
           04 WTGA-ELE-TRAN-MAX            PIC S9(04) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==WTGA==.
           COPY LCCVTGA1                   REPLACING ==(SF)==
                                           BY ==WTGA==.
      *--  Interfaccia servizio
       01  WCOM-LCCS0005.
           COPY LCCV0005.

      *--  AREA RAPP-RETE
       01  WRRE-AREA-RAPP-RETE.
           04 WRRE-ELE-RAPP-RETE-MAX       PIC S9(04) COMP.
           04 WRRE-TAB-RRE                 OCCURS 20.
           COPY LCCVRRE1                   REPLACING ==(SF)==
                                           BY ==WRRE==.
      *--  AREA NOTE OGGETTO
       01  WNOT-AREA-NOTE-OGG.
           04 WNOT-ELE-NOTE-OGG-MAX        PIC S9(004) COMP.
           04 WNOT-TAB-NOTE-OGG.
           COPY LCCVNOT1                   REPLACING ==(SF)==
                                           BY ==WNOT==.

      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                WMOV-AREA-MOVIMENTO
                                WRIC-AREA-RICHIESTA
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TRANCHE
                                WCOM-LCCS0005
                                WRRE-AREA-RAPP-RETE
                                WNOT-AREA-NOTE-OGG.
      *---------------------------------------------------------------*
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------
      *    OPERAZIONI INIZIALI
      *----------------------------------------------------------------
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                         WK-VARIABILI
                                              WS-INDICI
                                              RICH
                                              MOVI
                                              STAT-OGG-WF
                                              OGG-DEROGA
                                              MOT-DEROGA.

      *--  calcolo data competenza
           IF NOT IDSV0001-ON-LINE
              MOVE IDSV0001-LIVELLO-DEBUG  TO WK-LIVELLO-DEBUG
              PERFORM B020-ESTRAI-DT-COMPETENZA  THRU B020-EX
              MOVE WK-LIVELLO-DEBUG        TO IDSV0001-LIVELLO-DEBUG
           END-IF.

           IF IDSV0001-ESITO-OK
              MOVE IDSV0001-TIPO-MOVIMENTO    TO WS-MOVIMENTO

              IF WCOM-ID-OGG-DEROGA NOT NUMERIC
                 MOVE ZEROES                  TO WCOM-ID-OGG-DEROGA
              END-IF
           END-IF.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> AGGIORNA RICHIESTA
           IF LCCV0005-RICHIESTA-SI
              PERFORM AGGIORNA-RICH
                 THRU AGGIORNA-RICH-EX
           END-IF

      *--> AGGIORNA MOVIMENTO
           IF IDSV0001-ESITO-OK
              IF NOT WMOV-ST-INV
                 IF LCCV0005-PREVENTIVAZIONE
                    IF REVIS-INDIVI
                       MOVE WMOV-ID-MOVI
                         TO MOV-ID-MOVI
                    END-IF

                    IF IDSV0001-ESITO-OK
                       PERFORM AGGIORNA-MOVI
                          THRU AGGIORNA-MOVI-EX
                    END-IF
                 END-IF

                 IF LCCV0005-VENDITA
                    PERFORM S1006-CALC-IB-OGGETTO
                       THRU S1006-EX
                 END-IF

                 IF IDSV0001-ESITO-OK
                    IF NOT LCCV0005-PREVENTIVAZIONE
                       PERFORM AGGIORNA-MOVI
                          THRU AGGIORNA-MOVI-EX
                    END-IF
                 END-IF
              ELSE
                 MOVE WMOV-ID-MOVI
                   TO WMOV-ID-PTF
              END-IF
           END-IF.

      *--> CAMPO DELL'AREA STATI UTILIZZATO DAL PROCESSO DI STAMPA
      *--> F.E. - INOLTRE PER IL MOVIMENTO FITTIZIO 1006 NON VA
      *--> PASSATO L'ID
           IF IDSV0001-ESITO-OK
              MOVE WMOV-TP-MOVI
                TO WS-MOVIMENTO

              IF NOT EMISS-POLIZZA
                 MOVE WMOV-ID-PTF
                   TO WCOM-ID-MOVI-CRZ
              END-IF

      *-->    RIPRISTINO IL TIPO MOVIMENTO DI PARTENZA
              MOVE IDSV0001-TIPO-MOVIMENTO
                TO WS-MOVIMENTO
           END-IF.

      *--> AGGIORNA STATO OGGETTO WORK-FLOW - MOVIMENTO
           IF IDSV0001-ESITO-OK
              PERFORM S1010-AGGIORNA-WORK-FLOW-MO
                 THRU EX-S1010
           END-IF.

      *--> AGGIORNA NOTE OGGETTO - MOVIMENTO
           IF IDSV0001-ESITO-OK
              PERFORM AGGIORNA-NOTE-OGG
                 THRU AGGIORNA-NOTE-OGG-EX
           END-IF.

      *--> SETTIAMO LO STATUS DELLA NOTE OGGETTO A 'INVARIATO' PER
      *--> EVITARE DI SCRIVERE LA STESSA NOTA LEGATA A PIU' MOVIMENTI
           IF IDSV0001-ESITO-OK
              SET WNOT-ST-INV     TO TRUE
           END-IF.

      *--> SE CI SONO DEROGHE
           IF WCOM-NUM-ELE-MOT-DEROGA > 0
      *-->    AGGIORNA DEROGHE
              IF IDSV0001-ESITO-OK
                 PERFORM S1060-AGGIORNA-DEROGHE
                    THRU EX-S1060
              END-IF

      *-->    AGGIORNA STATO OGGETTO WORK-FLOW - OGGETTO DEROGA
              IF IDSV0001-ESITO-OK
                 PERFORM S1090-AGGIORNA-WORK-FLOW-DE
                    THRU EX-S1090
              END-IF
           END-IF.

      *--> SE C'E' UNA RICHIESTA ESTERNA
           IF  WCOM-ID-RICH-EST IS NUMERIC
           AND WCOM-ID-RICH-EST > 0
               IF IDSV0001-ESITO-OK
      *-->        AGGIORNA STATO RICHIESTA ESTERNA
                  PERFORM S1100-AGGIORNA-STATO-RIC-EST
                     THRU EX-S1100
               END-IF

               IF IDSV0001-ESITO-OK
      *-->        AGGIORNA RICHIESTA ESTERNA
                  PERFORM S1200-AGGIORNA-RIC-EST
                     THRU EX-S1200
               END-IF
           END-IF.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    CALCOLA IB-OGGETTO
      *----------------------------------------------------------------*
       S1006-CALC-IB-OGGETTO.

      *    Estrazione del sequence della polizza o adesione per il
      *    calcolo del relativo IB-OGGETTO che viene salvato nel campo
      *    IB-OGGETTO del Movimento
           EVALUATE TRUE
               WHEN CREAZ-COLLET
                    PERFORM ESTR-SEQUENCE-POLI
                       THRU ESTR-SEQUENCE-POLI-EX
                    IF IDSV0001-ESITO-OK
      *                ESTRAZIONE IB PROPOSTA POLIZZA
                       PERFORM ESTRAZIONE-IB-PROPOSTA
                          THRU ESTRAZIONE-IB-PROPOSTA-EX
                    END-IF

               WHEN CREAZ-INDIVI
               WHEN TRFOR-INDIVI
               WHEN CREAZ-INDIVI-REINV
                    PERFORM ESTR-SEQUENCE-POLI
                       THRU ESTR-SEQUENCE-POLI-EX
                    IF IDSV0001-ESITO-OK
      *                ESTRAZIONE IB PROPOSTA POLIZZA
                       PERFORM ESTRAZIONE-IB-PROPOSTA
                          THRU ESTRAZIONE-IB-PROPOSTA-EX
                    END-IF
                    IF IDSV0001-ESITO-OK
                       PERFORM ESTR-SEQUENCE-ADES
                          THRU ESTR-SEQUENCE-ADES-EX
                    END-IF
                    IF IDSV0001-ESITO-OK
      *                ESTRAZIONE IB OGGETTO ADESIONE
                       PERFORM ESTR-IB-OGG-ADE
                          THRU ESTR-IB-OGG-ADE-EX
                    END-IF

               WHEN CREAZ-ADESIO
               WHEN TRASF-ADESIO
               WHEN TRASFER-TESTA-POS
                    PERFORM ESTR-SEQUENCE-ADES
                       THRU ESTR-SEQUENCE-ADES-EX
                    IF IDSV0001-ESITO-OK
      *                ESTRAZIONE IB OGGETTO ADESIONE
                       PERFORM ESTR-IB-OGG-ADE
                          THRU ESTR-IB-OGG-ADE-EX
                    END-IF

               WHEN ATTIV-COLLET
               WHEN ATTIV-INDIVI
      *             ESTRAZIONE IB-OGGETTO DELLA POLIZZA
                    PERFORM ESTRAZIONE-IB-OGGETTO
                       THRU ESTRAZIONE-IB-OGGETTO-EX

           END-EVALUATE.

       S1006-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE STATO OGGETTO WORK FLOW MOVIMENTO
      *----------------------------------------------------------------*
       S1010-AGGIORNA-WORK-FLOW-MO.

           IF WMOV-ST-ADD
              MOVE WCOM-STATO-MOVIMENTO           TO WK-STAT-OGG-WF
              MOVE WCOM-MOV-FLAG-ST-FINALE        TO WK-STAT-END-WF
              SET IDSI0011-AGG-STORICO-SOLO-INS   TO TRUE
              MOVE WMOV-ID-PTF                    TO WK-ID-OGG
              MOVE 'MO'                           TO WK-TP-OGG
              PERFORM S1040-INSERT-STW
                 THRU EX-S1040
           ELSE
      *-->    LETTURA STATO OGGETTO WORK-FLOW - OGGETTO MOVIMENTO
              MOVE WMOV-ID-PTF                    TO WK-ID-OGG
              MOVE 'MO'                           TO WK-TP-OGG

              PERFORM S1020-SELECT-STW
                 THRU EX-S1020

      *--     AGGIORNA STATO OGGETTO WORK-FLOW - OGGETTO MOVIMENTO
              IF IDSV0001-ESITO-OK
      *--        IMPOSTAZIONE DELLO STATO WORK FLOW - MOVIMENTO
                 MOVE WCOM-STATO-MOVIMENTO        TO WK-STAT-OGG-WF
                 MOVE WCOM-MOV-FLAG-ST-FINALE     TO WK-STAT-END-WF

                 EVALUATE TRUE
      *-->         OCCORRENZA NON TROVATA (SQLCODE = +100)
                   WHEN IDSO0011-NOT-FOUND
                      SET IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
                      PERFORM S1040-INSERT-STW
                         THRU EX-S1040
      *-->         OCCORRENZA TROVATA (SQLCODE = +0)
                   WHEN IDSO0011-SUCCESSFUL-SQL
                      PERFORM S1030-UPDATE-STW
                         THRU EX-S1030
                 END-EVALUATE
              END-IF
           END-IF.

       EX-S1010.
           EXIT.
      *----------------------------------------------------------------*
      *    ESTRAZIONE SEQUENCE PREVENTIVO
      *----------------------------------------------------------------*
       S1011-ESTR-SEQUENCE-PREV.

           MOVE 'PREV' TO WK-TABELLA

           PERFORM ESTR-SEQUENCE
              THRU ESTR-SEQUENCE-EX

           IF IDSV0001-ESITO-OK
              MOVE S090-SEQ-TABELLA      TO WL27-ID-PTF
           END-IF.

       EX-S1011.
           EXIT.
      *----------------------------------------------------------------*
      *    CALCOLA IB OGGETTO PREVENTIVO
      *----------------------------------------------------------------*
       S1012-CALC-IB-OGG-PREV.

           INITIALIZE AREA-ESTRAZIONE-IB.
      *
            SET C161-NUM-PREV                 TO TRUE.

            SET C161-AUTOMATICA               TO TRUE.

            PERFORM VALORIZZA-AREE-S0088
               THRU VALORIZZA-AREE-S0088-EX.

            CALL IDSS0160  USING  AREA-IDSV0001
                                  AREA-ESTRAZIONE-IB
            ON EXCEPTION
               MOVE IDSS0160
                 TO IEAI9901-COD-SERVIZIO-BE
               MOVE 'ERRORE CHIAMATA IDSS0160 - S1001-IB-PROPOSTA-POL'
                 TO IEAI9901-LABEL-ERR
               MOVE '001114'
                TO IEAI9901-COD-ERRORE
               PERFORM S0300-RICERCA-GRAVITA-ERRORE
                  THRU EX-S0300
            END-CALL.

            IF IDSV0001-ESITO-OK
              MOVE C161-IB-OGGETTO           TO WL27-IB-OGG
      *        MOVE WL27-ID-PTF               TO WMOV-ID-OGG
              MOVE WL27-IB-OGG               TO WMOV-IB-OGG
           END-IF.

       EX-S1012.
           EXIT.

      *----------------------------------------------------------------*
      *    SELECT STATO OGGETTO WORK-FLOW (ID-OGGETTO/TP-OGGETTO)
      *----------------------------------------------------------------*
       S1020-SELECT-STW.

           MOVE 'STAT-OGG-WF'          TO WK-TABELLA.

      *--> VALORIZZAZIONE DELLA CHIAVE DI LETTURA
           MOVE WK-ID-OGG              TO STW-ID-OGG.
           MOVE WK-TP-OGG              TO STW-TP-OGG.

      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
           MOVE ZERO                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                          IDSI0011-DATA-FINE-EFFETTO
                                          IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-X-EFFETTO
                                       TO TRUE.

      *--> NOME TABELLA FISICA DB
           MOVE WK-TABELLA             TO IDSI0011-CODICE-STR-DATO.

      *--> DCLGEN TABELLA
           MOVE STAT-OGG-WF            TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT         TO TRUE.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID-OGGETTO    TO TRUE.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
      *-->    GESTIONE ERRORE DB
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                     CONTINUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--            OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI
                       TO STAT-OGG-WF
                  WHEN OTHER
      *--            ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM
                       TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1150-SELECT-STB'
                       TO IEAI9901-LABEL-ERR
                     MOVE '005016'
                       TO IEAI9901-COD-ERRORE
                     STRING WK-TABELLA           ';'
                            IDSO0011-RETURN-CODE ';'
                            IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    GESTIONE ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1150-SELECT-STB'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1020.
           EXIT.
      *----------------------------------------------------------------*
      *    UPDATE STATO OGGETTO WORK-FLOW
      *----------------------------------------------------------------*
       S1030-UPDATE-STW.

           MOVE 'STAT-OGG-WF'           TO WK-TABELLA.

      *--  CAMPI VARIATI DELLA STATO OGGETTO WORK FLOW
           MOVE WMOV-ID-PTF             TO STW-ID-MOVI-CRZ
           MOVE WK-STAT-OGG-WF          TO STW-STAT-OGG-WF
           MOVE WK-STAT-END-WF          TO STW-FL-STAT-END

      *--> DATA EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO
                                           IDSI0011-DATA-COMPETENZA

      *--> TABELLA STORICA
           SET  IDSI0011-TRATT-X-EFFETTO  TO TRUE

      *--> DCLGEN TABELLA
           MOVE STAT-OGG-WF               TO IDSI0011-BUFFER-DATI

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID               TO TRUE

      *--> TIPO OPERAZIONE
           SET IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *--> CALL DISPATCHER PER AGGIORNAMENTO
           PERFORM AGGIORNA-TABELLA
              THRU AGGIORNA-TABELLA-EX.

       EX-S1030.
           EXIT.
      *----------------------------------------------------------------*
      *    INSERT STATO OGGETTO WORK-FLOW
      *----------------------------------------------------------------*
       S1040-INSERT-STW.

           MOVE 'STAT-OGG-WF'           TO WK-TABELLA.

      *--> VALORIZZAZIONE DCLGEN STATO OGGETTO WORK FLOW

           PERFORM ESTR-SEQUENCE
              THRU ESTR-SEQUENCE-EX.

           IF IDSV0001-ESITO-OK

              MOVE S090-SEQ-TABELLA            TO STW-ID-STAT-OGG-WF

              MOVE WK-ID-OGG                   TO STW-ID-OGG
              MOVE WK-TP-OGG                   TO STW-TP-OGG

              MOVE WMOV-ID-PTF                 TO STW-ID-MOVI-CRZ
              MOVE HIGH-VALUE                  TO STW-ID-MOVI-CHIU-NULL
              MOVE IDSV0001-COD-COMPAGNIA-ANIA TO STW-COD-COMP-ANIA
              MOVE SPACES                      TO STW-COD-PRCS

              IF WK-TP-OGG = 'MO'
                 MOVE 'MOV'                    TO STW-COD-PRCS
              END-IF

              IF WK-TP-OGG = 'DE'
                 MOVE WCOM-COD-PROCESSO-WF
                   TO STW-COD-PRCS
              END-IF

              MOVE 'ATT01'                     TO STW-COD-ATTVT
              MOVE WK-STAT-OGG-WF              TO STW-STAT-OGG-WF
              MOVE WK-STAT-END-WF              TO STW-FL-STAT-END

      *-->    DATA EFFETTO
              MOVE WMOV-DT-EFF           TO IDSI0011-DATA-INIZIO-EFFETTO
              MOVE ZERO                  TO IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA

      *-->    TABELLA STORICA
              SET  IDSI0011-TRATT-X-EFFETTO  TO TRUE

      *-->    DCLGEN TABELLA
              MOVE STAT-OGG-WF               TO IDSI0011-BUFFER-DATI

      *-->    MODALITA DI ACCESSO
              SET  IDSI0011-ID               TO TRUE

      *-->    CALL DISPATCHER PER AGGIORNAMENTO
              PERFORM AGGIORNA-TABELLA
                 THRU AGGIORNA-TABELLA-EX
           END-IF.

       EX-S1040.
           EXIT.
      *----------------------------------------------------------------*
      *    AGGIORMENTO OGGETTO DEROGA E MOTIVI DEROGA
      *----------------------------------------------------------------*
       S1060-AGGIORNA-DEROGHE.

           MOVE ZEROES                 TO WK-COD-GR-AUT-SUP
           MOVE ZEROES                 TO WK-COD-LIV-AUT-SUP

           IF WCOM-ID-OGG-DEROGA GREATER ZERO
              PERFORM S1061-LEGGI-OGG-DEROGA
                 THRU EX-S1061
           END-IF

      *--> VALORIZZAZIONE OGGETTO DEROGA
           PERFORM S1070-VAL-AREA-ODE
              THRU EX-S1070

      *--> AGGIORNA OGGETTO DEROGA
           PERFORM AGGIORNA-OGG-DEROGA
              THRU AGGIORNA-OGG-DEROGA-EX

12807      IF IDSV0001-ESITO-OK
12807         MOVE WODE-ID-PTF
12807           TO WCOM-ID-OGG-DEROGA
12807      END-IF

           IF IDSV0001-ESITO-OK
      *-->    VALORIZZAZIONE MOTIVI DEROGA
              PERFORM VARYING IX-TAB-MDE FROM 1 BY 1
                        UNTIL IX-TAB-MDE > WCOM-NUM-ELE-MOT-DEROGA
                 PERFORM S1080-VAL-AREA-MDE
                    THRU EX-S1080
              END-PERFORM

      *-->    AGGIORNA MOTIVI DEROGA
              PERFORM VARYING IX-TAB-MDE FROM 1 BY 1
                        UNTIL IX-TAB-MDE > WMDE-ELE-MDE-MAX
                    PERFORM AGGIORNA-MOT-DEROGA
                       THRU AGGIORNA-MOT-DEROGA-EX
             END-PERFORM
           END-IF.

       EX-S1060.
           EXIT.
      *----------------------------------------------------------------*
      *    LEGGI OGGETTO DEROGA
      *----------------------------------------------------------------*
       S1061-LEGGI-OGG-DEROGA.

           MOVE 'OGG-DEROGA'           TO WK-TABELLA.

      *--> VALORIZZAZIONE DELLA CHIAVE DI LETTURA
           MOVE WCOM-ID-OGG-DEROGA     TO ODE-ID-OGG-DEROGA

      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
           MOVE ZERO                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                          IDSI0011-DATA-FINE-EFFETTO
                                          IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-X-EFFETTO
                                       TO TRUE.

      *--> NOME TABELLA FISICA DB
           MOVE WK-TABELLA             TO IDSI0011-CODICE-STR-DATO.

      *--> DCLGEN TABELLA
           MOVE OGG-DEROGA             TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT         TO TRUE.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-ID             TO TRUE.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
      *-->    GESTIRE ERRORE DB
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *-->           GESTIRE ERRORE DISPATCHER
                     MOVE WK-PGM
                       TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1061-LEGGI-OGG-DEROGA'
                       TO IEAI9901-LABEL-ERR
                     MOVE '005016'
                       TO IEAI9901-COD-ERRORE
                     STRING WK-TABELLA           ';'
                            IDSO0011-RETURN-CODE ';'
                            IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--            OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI TO OGG-DEROGA
                     MOVE ODE-COD-GR-AUT-SUP   TO WK-COD-GR-AUT-SUP
                     MOVE ODE-COD-LIV-AUT-SUP  TO WK-COD-LIV-AUT-SUP

              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1061-LEGGI-OGG-DEROGA'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1061.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE OGGETTO DEROGA
      *----------------------------------------------------------------*
       S1070-VAL-AREA-ODE.

           INITIALIZE WODE-AREA-OGG-DEROGA

           MOVE WCOM-ID-OGG-DEROGA           TO WODE-ID-OGG-DEROGA
                                                WODE-ID-PTF
           IF WODE-ID-OGG-DEROGA = ZERO
              SET WODE-ST-ADD                TO TRUE
           ELSE
              IF WCOM-ST-DE-MOD
                 SET WODE-ST-MOD             TO TRUE
                 MOVE WMOV-ID-PTF            TO WODE-ID-MOVI-CRZ
              ELSE
                 SET WODE-ST-INV             TO TRUE
              END-IF
           END-IF.

           IF WPOL-TP-FRM-ASSVA = TP-INDIVIDUALE
              MOVE WPOL-ID-PTF               TO WODE-ID-OGG
              MOVE WS-POLIZZA                TO WODE-TP-OGG
           ELSE
              IF WMOV-TP-OGG = WS-POLIZZA OR WS-ADESIONE
                 MOVE WMOV-ID-OGG            TO WODE-ID-OGG
                 MOVE WMOV-TP-OGG            TO WODE-TP-OGG
              ELSE
                 MOVE WADE-ID-PTF            TO WODE-ID-OGG
                 MOVE WS-ADESIONE            TO WODE-TP-OGG
              END-IF
           END-IF

           MOVE HIGH-VALUE                   TO WODE-ID-MOVI-CHIU-NULL
           MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO WODE-COD-COMP-ANIA
           MOVE WCOM-IB-OGG                  TO WODE-IB-OGG
           MOVE WCOM-TIPO-DEROGA             TO WODE-TP-DEROGA
           IF WCOM-ST-DE-MOD
              MOVE ODE-COD-GR-AUT-SUP        TO WODE-COD-GR-AUT-SUP
              MOVE ODE-COD-LIV-AUT-SUP       TO WODE-COD-LIV-AUT-SUP
              MOVE ODE-COD-GR-AUT-APPRT      TO WODE-COD-GR-AUT-APPRT
              MOVE ODE-COD-LIV-AUT-APPRT     TO WODE-COD-LIV-AUT-APPRT
           ELSE
              MOVE WCOM-COD-GRU-AUT-APPARTZA TO WODE-COD-GR-AUT-APPRT
              MOVE WCOM-COD-LIV-AUT-APPARTZA TO WODE-COD-LIV-AUT-APPRT
           END-IF.

           IF (WK-COD-GR-AUT-SUP  NOT EQUAL WCOM-COD-GRU-AUT-SUPER)
           OR (WK-COD-LIV-AUT-SUP NOT EQUAL WCOM-COD-LIV-AUT-SUPER)
              MOVE WCOM-COD-GRU-AUT-SUPER   TO WODE-COD-GR-AUT-SUP
              MOVE WCOM-COD-LIV-AUT-SUPER   TO WODE-COD-LIV-AUT-SUP
              IF NOT WODE-ST-ADD
                 SET WODE-ST-MOD            TO TRUE
13232            MOVE WMOV-ID-PTF           TO WODE-ID-MOVI-CRZ
              END-IF
           END-IF

           MOVE WCOM-DS-VER                 TO WODE-DS-VER

           ADD 1 TO WODE-ELE-ODE-MAX.

       EX-S1070.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE MOTIVI DEROGA
      *----------------------------------------------------------------*
       S1080-VAL-AREA-MDE.

           IF WCOM-ST-DE-MOD
              MOVE WCOM-ID-MOT-DEROGA  (IX-TAB-MDE)
                TO WMDE-ID-PTF  (IX-TAB-MDE)
           END-IF

           MOVE WCOM-ID-MOT-DEROGA  (IX-TAB-MDE)
             TO WMDE-ID-MOT-DEROGA  (IX-TAB-MDE)

           MOVE WODE-ID-PTF
             TO WMDE-ID-OGG-DEROGA  (IX-TAB-MDE)

           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO WMDE-COD-COMP-ANIA  (IX-TAB-MDE)

           MOVE WCOM-TP-MOT-DEROGA  (IX-TAB-MDE)
             TO WMDE-TP-MOT-DEROGA  (IX-TAB-MDE)

           MOVE WCOM-COD-LIV-AUTORIZZATIVO (IX-TAB-MDE)
             TO WMDE-COD-LIV-AUT           (IX-TAB-MDE)

           MOVE WCOM-COD-ERR        (IX-TAB-MDE)
             TO WMDE-COD-ERR        (IX-TAB-MDE)

           MOVE WCOM-TP-ERR         (IX-TAB-MDE)
             TO WMDE-TP-ERR         (IX-TAB-MDE)

           MOVE WCOM-DESCRIZIONE-ERR (IX-TAB-MDE)
             TO WMDE-DESC-ERR-BREVE  (IX-TAB-MDE)
                WMDE-DESC-ERR-EST    (IX-TAB-MDE)

            MOVE WCOM-STATUS        (IX-TAB-MDE)
              TO WMDE-STATUS        (IX-TAB-MDE).

            ADD 1 TO WMDE-ELE-MDE-MAX.

       EX-S1080.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE STATO OGGETTO WORK FLOW DEROGA
      *----------------------------------------------------------------*
       S1090-AGGIORNA-WORK-FLOW-DE.

      *--> LETTURA STATO OGGETTO WORK-FLOW - OGGETTO DEROGA
           MOVE WODE-ID-PTF            TO WK-ID-OGG
           MOVE 'DE'                   TO WK-TP-OGG

           PERFORM S1020-SELECT-STW
              THRU EX-S1020

           IF IDSV0001-ESITO-OK
      *-->    IMPOSTAZIONE DELLO STATO WORK FLOW - OGGETTO DEROGA
              MOVE WCOM-STATO-OGG-DEROGA   TO WK-STAT-OGG-WF
              MOVE WCOM-DER-FLAG-ST-FINALE TO WK-STAT-END-WF
              EVALUATE TRUE
      **--      OCCORRENZA NON TROVATA (SQLCODE = +100)
                WHEN IDSO0011-NOT-FOUND
      *-->         TIPO OPERAZIONE
                   SET IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
                   PERFORM S1040-INSERT-STW
                      THRU EX-S1040
      *-->      OCCORRENZA TROVATA (SQLCODE = +0)
                WHEN IDSO0011-SUCCESSFUL-SQL
                   PERFORM S1030-UPDATE-STW
                      THRU EX-S1030
              END-EVALUATE
           END-IF.

       EX-S1090.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE STATO RICHIESTA ESTERNA
      *----------------------------------------------------------------*
       S1100-AGGIORNA-STATO-RIC-EST.

      * --> LETTURA STATO RICHIESTA ESTERNA
           PERFORM S1120-SELECT-STATO-RIC
              THRU EX-S1120.

           IF IDSV0001-ESITO-OK
      *-->    IMPOSTAZIONE DELLO STATO RICHIESTA ESTERNA
      *-->    INSERISCO
              EVALUATE TRUE
                WHEN IDSO0011-NOT-FOUND
                   PERFORM S1140-INSERT-STATO-RIC
                      THRU EX-S1140

      *-->    CHIUDO ULTIMO STATO RICHIESTA ESTERNA
      *-->    INSERISCO ULTIMO STATO APERTO DELLA RICHIESTA
                WHEN IDSO0011-SUCCESSFUL-SQL
                   PERFORM S1130-UPDATE-STATO-RIC
                      THRU EX-S1130
                   IF IDSV0001-ESITO-OK
                      PERFORM S1140-INSERT-STATO-RIC
                         THRU EX-S1140
                   END-IF
              END-EVALUATE
           END-IF.

       EX-S1100.
           EXIT.
      *----------------------------------------------------------------*
      *    SELECT STATO DELLA RICHIESTA
      *----------------------------------------------------------------*
       S1120-SELECT-STATO-RIC.

           MOVE 'STAT-RICH-EST'            TO WK-TABELLA.

           MOVE ZERO                       TO IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-SENZA-STOR   TO TRUE.
           SET IDSI0011-WHERE-CONDITION    TO TRUE.
           SET IDSI0011-SELECT             TO TRUE.

           MOVE WCOM-ID-RICH-EST           TO P04-ID-RICH-EST.

           MOVE LDBS8170                   TO IDSI0011-CODICE-STR-DATO
           MOVE STAT-RICH-EST              TO IDSI0011-BUFFER-DATI.
           MOVE SPACES                     TO IDSI0011-BUFFER-WHERE-COND


           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
      *-->    GESTIONE ERRORE DB
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                     CONTINUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--            OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI
                       TO STAT-RICH-EST

                  WHEN OTHER
      *--            ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM
                       TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1120-SELECT-STATO-RIC'
                       TO IEAI9901-LABEL-ERR
                     MOVE '005016'
                       TO IEAI9901-COD-ERRORE
                     STRING WK-TABELLA           ';'
                            IDSO0011-RETURN-CODE ';'
                            IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    GESTIONE ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1120-SELECT-STATO-RIC'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1120.
           EXIT.
      *----------------------------------------------------------------*
      *    UPDATE STATO DELLA RICHIESTA
      *----------------------------------------------------------------*
       S1130-UPDATE-STATO-RIC.

           MOVE 'STAT-RICH-EST'             TO WK-TABELLA.
      *     MOVE WCOM-STATO-RICHIESTA-EST    TO P04-STAT-RICH-EST
           MOVE IDSV0001-DATA-COMPETENZA    TO P04-TS-END-VLDT.
           SET  IDSI0011-TRATT-SENZA-STOR   TO TRUE.
           MOVE STAT-RICH-EST               TO IDSI0011-BUFFER-DATI.
           SET  IDSI0011-UPDATE             TO TRUE.
           SET  IDSI0011-PRIMARY-KEY        TO TRUE.
           MOVE WK-TABELLA                  TO
                                               IDSI0011-CODICE-STR-DATO.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
                 EVALUATE TRUE
                    WHEN IDSO0011-SUCCESSFUL-SQL
                         CONTINUE

                    WHEN OTHER
                         MOVE WK-PGM      TO IEAI9901-COD-SERVIZIO-BE
                         MOVE 'S1130-UPDATE-STATO-RIC'
                                          TO IEAI9901-LABEL-ERR
                         MOVE '005016'    TO IEAI9901-COD-ERRORE
                         MOVE SPACES      TO IEAI9901-PARAMETRI-ERR
                         STRING IDBSP040             ';'
                                IDSO0011-RETURN-CODE ';'
                                IDSO0011-SQLCODE
                                DELIMITED BY SIZE
                                INTO IEAI9901-PARAMETRI-ERR
                         END-STRING
                         PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            THRU EX-S0300
                 END-EVALUATE
           ELSE
      *-->    GESTIONE ERRORE DISPATCHER
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S1130-UPDATE-STATO-RIC'
                                       TO IEAI9901-LABEL-ERR
                 MOVE '005016'
                   TO IEAI9901-COD-ERRORE
                 STRING WK-TABELLA           ';'
                        IDSO0011-RETURN-CODE ';'
                        IDSO0011-SQLCODE
                 DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
           END-IF.

       EX-S1130.
           EXIT.
      *----------------------------------------------------------------*
      *    INSERT STATO DELLA RICHIESTA
      *----------------------------------------------------------------*
       S1140-INSERT-STATO-RIC.

           IF IDSV0001-MAX-ELE-ERRORI  >  0
              CONTINUE
           ELSE
              MOVE SPACES                 TO P04-DESC-ERR
                                             P04-COD-ERR-SCARTO
           END-IF
           MOVE HIGH-VALUE                TO P04-TP-CAUS-SCARTO-NULL.
           MOVE 'STAT-RICH-EST'           TO WK-TABELLA.

      *--> VALORIZZAZIONE DCLGEN STATO DELLA RICHIESTA

           PERFORM ESTR-SEQUENCE
              THRU ESTR-SEQUENCE-EX.

           IF IDSV0001-ESITO-OK
              MOVE S090-SEQ-TABELLA            TO P04-ID-STAT-RICH-EST
              MOVE WCOM-ID-RICH-EST            TO P04-ID-RICH-EST
              MOVE IDSV0001-COD-COMPAGNIA-ANIA TO P04-COD-COMP-ANIA
              MOVE 'GRE'                       TO P04-COD-PRCS
              MOVE WCOM-COD-ATTIVITA-WF        TO P04-COD-ATTVT
              MOVE WCOM-STATO-RICHIESTA-EST    TO P04-STAT-RICH-EST
              MOVE WMOV-ID-PTF                 TO P04-ID-MOVI-CRZ


              IF WCOM-STATO-RICHIESTA-EST = 'DE'
                 SET SCARTO-DEROGHE            TO TRUE
                 MOVE WS-TP-CAUS-SCARTO        TO P04-TP-CAUS-SCARTO
              END-IF

              MOVE IDSV0001-USER-NAME          TO P04-UTENTE-INS-AGG
              MOVE WCOM-RIC-FLAG-ST-FINALE     TO P04-FL-STAT-END

              MOVE IDSV0001-DATA-COMPETENZA    TO P04-TS-INI-VLDT
              MOVE WS-TS-INFINITO              TO P04-TS-END-VLDT

              SET  IDSI0011-TRATT-SENZA-STOR   TO TRUE

              SET  IDSI0011-INSERT             TO TRUE
              SET  IDSI0011-PRIMARY-KEY        TO TRUE

              MOVE WK-TABELLA                  TO
                                                IDSI0011-CODICE-STR-DATO
              MOVE  STAT-RICH-EST              TO IDSI0011-BUFFER-DATI

              PERFORM CALL-DISPATCHER
                 THRU CALL-DISPATCHER-EX

              IF IDSO0011-SUCCESSFUL-RC
                 EVALUATE TRUE
                    WHEN IDSO0011-SUCCESSFUL-SQL
                         CONTINUE

                    WHEN OTHER
                         MOVE WK-PGM      TO IEAI9901-COD-SERVIZIO-BE
                         MOVE 'S1140-INSERT-STATO-RIC'
                                          TO IEAI9901-LABEL-ERR
                         MOVE '005016'    TO IEAI9901-COD-ERRORE
                         MOVE SPACES      TO IEAI9901-PARAMETRI-ERR
                         STRING IDBSP040             ';'
                                IDSO0011-RETURN-CODE ';'
                                IDSO0011-SQLCODE
                                DELIMITED BY SIZE
                                INTO IEAI9901-PARAMETRI-ERR
                         END-STRING
                         PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            THRU EX-S0300
                 END-EVALUATE
              ELSE
      *-->    GESTIONE ERRORE DISPATCHER
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S1140-INSERT-STATO-RIC'
                                       TO IEAI9901-LABEL-ERR
                 MOVE '005016'
                   TO IEAI9901-COD-ERRORE
                 STRING WK-TABELLA           ';'
                        IDSO0011-RETURN-CODE ';'
                        IDSO0011-SQLCODE
                 DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

       EX-S1140.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE RICHIESTA ESTERNA
      *----------------------------------------------------------------*
       S1200-AGGIORNA-RIC-EST.

      * --> LETTURA RICHIESTA ESTERNA
           PERFORM S1220-SELECT-RIC
              THRU EX-S1220.

           IF IDSV0001-ESITO-OK
              PERFORM S1230-UPDATE-RIC
                 THRU EX-S1230
           END-IF.

       EX-S1200.
           EXIT.
      *----------------------------------------------------------------*
      *    SELECT RICHIESTA
      *----------------------------------------------------------------*
       S1220-SELECT-RIC.

           MOVE 'RICH-EST'            TO WK-TABELLA.

           SET IDSI0011-TRATT-SENZA-STOR   TO TRUE.
           SET IDSI0011-SELECT             TO TRUE.
           SET IDSI0011-PRIMARY-KEY        TO TRUE.

           MOVE WCOM-ID-RICH-EST           TO P01-ID-RICH-EST.

           MOVE 'RICH-EST'                 TO IDSI0011-CODICE-STR-DATO.
           MOVE RICH-EST                   TO IDSI0011-BUFFER-DATI.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
      *-->    GESTIONE ERRORE DB
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1220-SELECT-RIC'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING WK-TABELLA           ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE INTO
                              IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--            OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI
                         TO RICH-EST
                  WHEN OTHER
      *--              ERRORE DI ACCESSO AL DB
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1220-SELECT-RIC'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING WK-TABELLA           ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE INTO
                              IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
                  END-EVALUATE
           ELSE
      *-->    GESTIONE ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1220-SELECT-RIC'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1220.
           EXIT.
      *----------------------------------------------------------------*
      *    UPDATE RICHIESTA
      *----------------------------------------------------------------*
       S1230-UPDATE-RIC.

           MOVE 'RICH-EST'                  TO WK-TABELLA.

           SET IDSI0011-TRATT-SENZA-STOR   TO TRUE.
           SET IDSI0011-UPDATE             TO TRUE.
           SET IDSI0011-PRIMARY-KEY        TO TRUE.

           MOVE WCOM-ID-RICH-EST           TO P01-ID-RICH-EST.

           MOVE 'RICH-EST'                 TO IDSI0011-CODICE-STR-DATO.
           MOVE RICH-EST                   TO IDSI0011-BUFFER-DATI.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
                 EVALUATE TRUE
                    WHEN IDSO0011-SUCCESSFUL-SQL
                         CONTINUE

                    WHEN OTHER
                         MOVE WK-PGM      TO IEAI9901-COD-SERVIZIO-BE
                         MOVE 'S1230-UPDATE-RIC'
                                          TO IEAI9901-LABEL-ERR
                         MOVE '005016'    TO IEAI9901-COD-ERRORE
                         MOVE SPACES      TO IEAI9901-PARAMETRI-ERR
                         STRING IDBSP010             ';'
                                IDSO0011-RETURN-CODE ';'
                                IDSO0011-SQLCODE
                                DELIMITED BY SIZE
                                INTO IEAI9901-PARAMETRI-ERR
                         END-STRING
                         PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            THRU EX-S0300
                 END-EVALUATE
           ELSE
      *-->    GESTIONE ERRORE DISPATCHER
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S1230-UPDATE-RIC'
                                       TO IEAI9901-LABEL-ERR
                 MOVE '005016'
                   TO IEAI9901-COD-ERRORE
                 STRING WK-TABELLA           ';'
                        IDSO0011-RETURN-CODE ';'
                        IDSO0011-SQLCODE
                 DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
           END-IF.

       EX-S1230.
           EXIT.
      *----------------------------------------------------------------*
      *    CALL SERVIZIO ESTRAZIONE OGGETTO PTF LCCS0234
      *----------------------------------------------------------------*
       CALL-LCCS0234.

           CALL LCCS0234 USING AREA-IDSV0001
                               WPOL-AREA-POLIZZA
                               WADE-AREA-ADESIONE
                               WGRZ-AREA-GARANZIA
                               WTGA-AREA-TRANCHE
                               S234-DATI-INPUT
                               S234-DATI-OUTPUT
             ON EXCEPTION
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'IL REPERIMENTO ID PORTAFOGLIO'
                    TO CALL-DESC
                  MOVE 'S1100-CALL-LCCS0234'
                    TO IEAI9901-LABEL-ERR
                  PERFORM S0290-ERRORE-DI-SISTEMA
                     THRU EX-S0290
           END-CALL.

       CALL-LCCS0234-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESTRAZIONE DATA COMPETENZA
      *----------------------------------------------------------------*
       B020-ESTRAI-DT-COMPETENZA.
      *
           CALL PGM-IDSS0150               USING AREA-IDSV0001
           ON EXCEPTION
                  MOVE WK-PGM              TO IEAI9901-COD-SERVIZIO-BE

                  STRING 'ERRORE CHIAMATA MODULO : '
                      DELIMITED BY SIZE
                      PGM-IDSS0150
                      DELIMITED BY SIZE
                      ' - '
                       DELIMITED BY SIZE INTO
                      CALL-DESC
                  END-STRING

                  MOVE 'B020-ESTRAI-DT-COMPETENZA'
                                              TO IEAI9901-LABEL-ERR
                  PERFORM S0290-ERRORE-DI-SISTEMA     THRU EX-S0290

           END-CALL.
      *
       B020-EX.
           EXIT.
      *---------------------------------------------------------------
      *    OPERAZIONI FINALI
      *---------------------------------------------------------------
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES PER AREA DINAMICA IDBV0161.
      *----------------------------------------------------------------*
           COPY IDSP0161                 REPLACING ==(SF)== BY ==C161==.
      *---------------------------------------------------------------
      *    ROUTINES GESTIONE ERRORI
      *---------------------------------------------------------------
           COPY IERP9902.
           COPY IERP9901.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.
      *----------------------------------------------------------------*
      *    COPY PROCEDURE PER NUMERAZIONE OGGETTI
      *----------------------------------------------------------------*
           COPY LCCP0089.
           COPY LCCP0090.
      *----------------------------------------------------------------*
      *    ROUTINES OPERAZIONI SULLE STRINGHE
      *----------------------------------------------------------------*
           COPY LVEP0202.
      *----------------------------------------------------------------*
      *    COPY PROCEDURE AGGIORNAMENTO PORTAFOGLIO
      *----------------------------------------------------------------*
      *--> AGGIORNA TABELLA
           COPY LCCP0001.
      *--> ESTRAZIONE SEQUENCE
           COPY LCCP0002.
      *----------------------------------------------------------------*
      *    ROUTINES AGGIORNAMENTO DB
      *----------------------------------------------------------------*
      *--  MOVIMENTO
           COPY LCCVMOV5                 REPLACING ==(SF)== BY ==WMOV==.
           COPY LCCVMOV6.
      *--  RICHIESTA
           COPY LCCVRIC5                 REPLACING ==(SF)== BY ==WRIC==.
           COPY LCCVRIC6.
      *--  OGGETTO DEROGA
           COPY LCCVODE5                 REPLACING ==(SF)== BY ==WODE==.
           COPY LCCVODE6.
      *--  MOTIVI DEROGA
           COPY LCCVMDE5                 REPLACING ==(SF)== BY ==WMDE==.
           COPY LCCVMDE6.
      *--  NOTE OGGETTO
           COPY LCCVNOT5                 REPLACING ==(SF)== BY ==WNOT==.
           COPY LCCVNOT6.

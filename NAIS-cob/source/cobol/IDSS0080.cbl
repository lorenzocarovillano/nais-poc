       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDSS0080 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  21 NOV 2007.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDADA0 END-EXEC.
           EXEC SQL INCLUDE IDBVADA2 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVADA1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 ANAG-DATO.

           PERFORM A000-INIZIO              THRU A000-EX.

           PERFORM A300-ELABORA             THRU A300-EX



           GOBACK.

       A000-INIZIO.
           MOVE 'IDSS0080'               TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'ANAG_DATO'              TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle
      *----
      ******************************************************************
       A300-ELABORA.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT                 THRU A310-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR            THRU A360-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR           THRU A370-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST            THRU A380-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT             THRU A390-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER           TO TRUE
           END-EVALUATE.

       A300-EX.
           EXIT.

    ******************************************************************
       A305-DECLARE-CURSOR.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE CUR-ADA CURSOR WITH HOLD FOR

              SELECT
                COD_COMPAGNIA_ANIA
                ,COD_DATO
                ,DESC_DATO
                ,TIPO_DATO
                ,LUNGHEZZA_DATO
                ,PRECISIONE_DATO
                ,COD_DOMINIO
                ,FORMATTAZIONE_DATO
                ,DS_UTENTE
             FROM ANAG_DATO
             WHERE     COD_COMPAGNIA_ANIA = :ADA-COD-COMPAGNIA-ANIA
                   AND COD_DATO           = :ADA-COD-DATO

           END-EXEC.

       A305-EX.
           EXIT.
      ******************************************************************
       A310-SELECT.
      *
           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                COD_COMPAGNIA_ANIA
                ,COD_DATO
                ,DESC_DATO
                ,TIPO_DATO
                ,LUNGHEZZA_DATO
                ,PRECISIONE_DATO
                ,COD_DOMINIO
                ,FORMATTAZIONE_DATO
                ,DS_UTENTE
             INTO
                :ADA-COD-COMPAGNIA-ANIA
               ,:ADA-COD-DATO
               ,:ADA-DESC-DATO-VCHAR
                :IND-ADA-DESC-DATO
               ,:ADA-TIPO-DATO
                :IND-ADA-TIPO-DATO
               ,:ADA-LUNGHEZZA-DATO
                :IND-ADA-LUNGHEZZA-DATO
               ,:ADA-PRECISIONE-DATO
                :IND-ADA-PRECISIONE-DATO
               ,:ADA-COD-DOMINIO
                :IND-ADA-COD-DOMINIO
               ,:ADA-FORMATTAZIONE-DATO
                :IND-ADA-FORMATTAZIONE-DATO
               ,:ADA-DS-UTENTE
             FROM ANAG_DATO
             WHERE     COD_COMPAGNIA_ANIA = :ADA-COD-COMPAGNIA-ANIA
                   AND COD_DATO           = :ADA-COD-DATO

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
      *
       A310-EX.
           EXIT.
      ******************************************************************
       A360-OPEN-CURSOR.

           PERFORM A305-DECLARE-CURSOR THRU A305-EX.

           EXEC SQL
                OPEN CUR-ADA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A360-EX.
           EXIT.
      ******************************************************************
       A370-CLOSE-CURSOR.

           EXEC SQL
                CLOSE CUR-ADA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A370-EX.
           EXIT.
      ******************************************************************
       A380-FETCH-FIRST.

           PERFORM A360-OPEN-CURSOR    THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT THRU A390-EX
           END-IF.

       A380-EX.
           EXIT.
      ******************************************************************
       A390-FETCH-NEXT.

           EXEC SQL
                FETCH CUR-ADA
           INTO
                :ADA-COD-COMPAGNIA-ANIA
               ,:ADA-COD-DATO
               ,:ADA-DESC-DATO-VCHAR
                :IND-ADA-DESC-DATO
               ,:ADA-TIPO-DATO
                :IND-ADA-TIPO-DATO
               ,:ADA-LUNGHEZZA-DATO
                :IND-ADA-LUNGHEZZA-DATO
               ,:ADA-PRECISIONE-DATO
                :IND-ADA-PRECISIONE-DATO
               ,:ADA-COD-DOMINIO
                :IND-ADA-COD-DOMINIO
               ,:ADA-FORMATTAZIONE-DATO
                :IND-ADA-FORMATTAZIONE-DATO
               ,:ADA-DS-UTENTE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       A390-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-ADA-DESC-DATO = -1
              MOVE HIGH-VALUES TO ADA-DESC-DATO
           END-IF
           IF IND-ADA-TIPO-DATO = -1
              MOVE HIGH-VALUES TO ADA-TIPO-DATO-NULL
           END-IF
           IF IND-ADA-LUNGHEZZA-DATO = -1
              MOVE HIGH-VALUES TO ADA-LUNGHEZZA-DATO-NULL
           END-IF
           IF IND-ADA-PRECISIONE-DATO = -1
              MOVE HIGH-VALUES TO ADA-PRECISIONE-DATO-NULL
           END-IF
           IF IND-ADA-COD-DOMINIO = -1
              MOVE HIGH-VALUES TO ADA-COD-DOMINIO
           END-IF
           IF IND-ADA-FORMATTAZIONE-DATO = -1
              MOVE HIGH-VALUES TO ADA-FORMATTAZIONE-DATO
           END-IF.

       Z100-EX.
           EXIT.

      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.

           CONTINUE.
       Z950-EX.
           EXIT.

      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           MOVE LENGTH OF ADA-DESC-DATO
                       TO ADA-DESC-DATO-LEN.

       Z960-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.


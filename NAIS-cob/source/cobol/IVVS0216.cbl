      ******************************************************************
      **                                                        ********
      **    PORTAFOGLIO VITA ITALIA  VER 1.0                    ********
      **                                                        ********
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         IVVS0216.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *    PROGRAMMA ..... IVVS0216
      *    TIPOLOGIA...... TRASVERSALE
      *    PROCESSO....... XXX
      *    FUNZIONE....... VALORIZZATORE VARIABILI
      *    DESCRIZIONE.... VALORIZZAZIONE VARIABILI POST VENDITA
      *----------------------------------------------------------------*
      * MARKING 2404
      * MODIFICA EFFETTUATA PER EVALUZIONE DEL TRATTAMENTO
      * GARANZIE DI OPZIONE
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'IVVS0216'.
       01  WK-IDSS0010                      PIC X(008) VALUE 'IDSS0010'.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
        01 WKS-INDICI.
          03 CONT-FETCH                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-UNIV                    PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-ADE                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-ALL                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-BEP                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-CLT                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-COG                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-DCO                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-DFA                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-DEQ                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-DER                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-DTC                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-GRZ                     PIC 9(4) COMP VALUE ZEROES.
2404      03 IX-TAB-GOP                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-ISO                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-MOV                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-NOT                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-OCS                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-PGE                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-PCO                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-PCA                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-PMO                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-POG                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-PLI                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-POL                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-PRE                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-PVT                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-QUE                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-RAN                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-E15                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-RRE                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-RIC                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-RFI                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-RST                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-RPG                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-STB                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-STW                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-SDI                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-TIT                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-TGA                     PIC 9(4) COMP VALUE ZEROES.
2404      03 IX-TAB-TOP                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-TLI                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-SPG                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-BEL                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-LQU                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-DAD                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-MFZ                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-RIF                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-TDR                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-DTR                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-OCO                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-RCA                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-DLQ                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-DFL                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-GRL                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-L19                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-E12                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-L30                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-L23                     PIC 9(4) COMP VALUE ZEROES.
NEWFIS    03 IX-TAB-P61                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-P67                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-DATO                        PIC 9(4) COMP VALUE ZEROES.
          03 IX-TABB                        PIC 9(4) COMP VALUE ZEROES.
          03 IX-APP-DEQ                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-APP-QUE                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-AP-TABB                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-AP-DATO                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-IDSS0020                    PIC 9(4) COMP VALUE ZEROES.
          03 IX-CONTA                       PIC 9(4) COMP VALUE ZEROES.
          03 IX-VAL-VAR                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-COD-VAR                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-COD-VAR-OUT                 PIC 9(4) COMP VALUE ZEROES.
          03 IX-COD-VAR-OUT-SAL             PIC 9(4) COMP VALUE ZEROES.
          03 IX-COD-MVV                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-INTERN                      PIC 9(4) COMP VALUE ZEROES.
          03 IX-GUIDA-GRZ                   PIC 9(4) COMP VALUE ZEROES.
          03 IX-DOM                         PIC 9(4) COMP VALUE ZEROES.
          03 IX-CTRL-FITT                   PIC 9(4) COMP VALUE ZEROES.
          03 IX-VAR-LIS                     PIC 9(4) COMP VALUE ZEROES.
          03 IX-TAB-P01                     PIC 9(4) COMP VALUE ZEROES.
      *----------------------------------------------------------------*
      *    COPY ELE MAX
      *----------------------------------------------------------------*
           COPY LCCVGRZZ.
      *
       77  IX-APPO-OVERF                    PIC 9(4) COMP VALUE ZEROES.
       77  WK-APPO-ASSTO                    PIC S9(009) COMP-3.
       77  WK-APPO-ERR                      PIC X(200) VALUE SPACES.
       77  WK-APPO-TP-OGG                   PIC X(002) VALUE SPACES.
       77  WK-COD-CAN                       PIC S9(5)V COMP-3 VALUE 0.
      *
       01  WKS-AREA-TABB-APPO               PIC X(2500) VALUE SPACES.
       01  WKS-APPO-IMP                     PIC  S9(11)V9(07).
       01  WKS-APPO-PRE-NET                 PIC  S9(11)V9(03).
       01  WKS-APPO-PER                     PIC  9(05)V9(06).
       01  WKS-APPO-DATA                    PIC  9(008).
       01  WKS-APPO-STR                     PIC  X(012).
       01  WKS-APPO-STOR                    PIC  X(003).
       01  WKS-COD-SOPR                     PIC  X(012).
       01  WK-ID-ASSIC                      PIC S9(09) COMP-3.
      *
       01  WK-ID-OGG                        PIC S9(09) COMP-3.
       01  WK-TP-OGG                        PIC X(002) VALUE SPACES.
       01  WK-COD-VARIABILE                 PIC X(012) VALUE SPACES.

       01 AREA-POINTER.
          05 FXT-ADDRESS            POINTER.
12969     05 VXG-ADDRESS            POINTER.

       01 WK-VARIABILI.
          03 WK-APPO-LUNGHEZZA              PIC S9(09) COMP-3.
          03 WK-LUNGHEZZA-TOT               PIC S9(09) COMP-3.
      *----------------------------------------------------------------*
      * COPY PER VARIABILI A LISTA
      *----------------------------------------------------------------*
       01 WK1-VAR-STR.
          05 WK1-MAX-TAB-STRING              PIC S9(02) COMP.
      *   05 WK1-TAB-STRINGHE-TOT           OCCURS 60.
          05 WK1-TAB-STRINGHE-TOT           OCCURS 75.
             10 WK1-COD-VARIABILE           PIC  X(12).
             10 WK1-TP-STRINGA              PIC  X(01).
             10 WK1-STRINGA-TOT             PIC  X(60).
      *----------------------------------------------------------------*
           COPY IVVC0501.
      *
      *----------------------------------------------------------------*
      *  AREA ALIAS                                                    *
      *----------------------------------------------------------------*
       01  AREA-ALIAS.
           COPY IVVC0218                 REPLACING ==(SF)== BY ==C214==.
      *
       01 AREA-IO-CALCOLI.
          COPY IVVC0213                  REPLACING ==(SF)== BY ==C214==.

       01 AREA-QUESTIONARI.
          COPY IVVC0221.

      *----------------------------------------------------------------*
      *  FLAGS
      *----------------------------------------------------------------*
2404   01 FLAG-SKEDA-OPZIONE             PIC X(01).
2404      88 SKEDA-OPZIONE-SI            VALUE 'S'.
2404      88 SKEDA-OPZIONE-NO            VALUE 'N'.
      *
      *----------------------------------------------------------------*
      *    COPY DELLE TABELLE DB2
      *----------------------------------------------------------------*
           COPY IDBVADE1.
           COPY IDBVBEP1.
           COPY IDBVISO1.
           COPY IDBVCND1.
           COPY IDBVDCO1.
           COPY IDBVDFA1.
           COPY IDBVDEQ1.
           COPY IDBVDER1.
           COPY IDBVDTC1.
           COPY IDBVGRZ1.
           COPY IDBVMOV1.
           COPY IDBVNOT1.
           COPY IDBVOCS1.
           COPY IDBVPCO1.
           COPY IDBVPCA1.
           COPY IDBVPMO1.
           COPY IDBVPOG1.
           COPY IDBVPOL1.
           COPY IDBVPVT1.
           COPY IDBVQUE1.
           COPY IDBVRAN1.
           COPY IDBVE151.
           COPY IDBVRRE1.
           COPY IDBVRIC1.
           COPY IDBVRST1.
           COPY IDBVSPG1.
           COPY IDBVSTB1.
           COPY IDBVSTW1.
           COPY IDBVSDI1.
           COPY IDBVTIT1.
           COPY IDBVTGA1.
           COPY IDBVBEL1.
           COPY IDBVLQU1.
           COPY IDBVDAD1.
           COPY IDBVPRE1.
           COPY IDBVMFZ1.
           COPY IDBVRIF1.
      *     COPY IDBVTDR1.
      *     COPY IDBVDTR1.
           COPY IDBVOCO1.
      *     COPY IDBVRCA1.
           COPY IDBVTLI1.
           COPY IDBVPLI1.
           COPY IDBVGRL1.
RW16  *     COPY IDBVDLQ1.
RW16       COPY IDBVDFL1.
           COPY IDBVQ041.
           COPY IDBVQ051.
           COPY IDBVE121.
           COPY IDBVL301.
           COPY IDBVL231.
NEWFIS     COPY IDBVP611.
           COPY IDBVP671.
           COPY IDBVP011.
      *----------------------------------------------------------------*
      *    VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
      *
       01 AREA-IDSV0141.
           COPY IDSV0141.
      *----------------------------------------------------------------*
      *    COPY INPUT PER LA LETTURA IN PTF
      *----------------------------------------------------------------*
       01 WCOM-FLAG-OVERFLOW                   PIC X(002).
          88 WCOM-OVERFLOW-YES                 VALUE 'SI'.
          88 WCOM-OVERFLOW-NO                  VALUE 'NO'.
      *
       01  WK-VARIABILE-FLAG                PIC X(001).
           88 WK-VAR-TROVATA                 VALUE 'S'.
           88 WK-VAR-NON-TROVATA             VALUE 'N'.
       01  WK-COD-LIVELLO-FLAG              PIC X(001).
           88 WK-COD-TROVATO                 VALUE 'S'.
           88 WK-COD-NON-TROVATO             VALUE 'N'.
       01  WK-ID-COD-LIV-FLAG               PIC X(001).
           88 WK-ID-COD-TROVATO             VALUE 'S'.
           88 WK-ID-COD-NON-TROVATO         VALUE 'N'.
       01  WK-ID-LIVELLO-FLAG               PIC X(001).
           88 WK-ID-TROVATO                  VALUE 'S'.
           88 WK-ID-NON-TROVATO              VALUE 'N'.
       01  WK-VALORE-FLAG                   PIC X(001).
           88 WK-VAL-TROVATO                 VALUE 'S'.
           88 WK-VAL-NON-TROVATO             VALUE 'N'.
       01  WK-DESC-FLAG                     PIC X(001).
           88 WK-DESC-TROVATO                VALUE 'S'.
           88 WK-DESC-NON-TROVATO            VALUE 'N'.
       01  WK-TITOLO-FLAG                   PIC X(001).
           88 WK-TIT-TROVATO                 VALUE 'S'.
           88 WK-TIT-NON-TROVATO             VALUE 'N'.
       01  WK-TRANCHE-FLAG                  PIC X(001).
           88 WK-TRAN-TROVATA                VALUE 'S'.
           88 WK-TRAN-NON-TROVATA            VALUE 'N'.
       01  WK-CALCOLI                        PIC X(001).
           88 WK-CALCOLI-SI                  VALUE 'S'.
           88 WK-CALCOLI-NO                  VALUE 'N'.
       01  WK-FIND-CAMPO                     PIC X(001).
           88 WK-CAMPO-FIND-SI               VALUE 'S'.
           88 WK-CAMPO-FIND-NO               VALUE 'N'.
       01  WK-FIND-INTERN                    PIC X(001).
           88 WK-INTERN-FIND-SI              VALUE 'S'.
           88 WK-INTERN-FIND-NO              VALUE 'N'.
       01  WK-FIND-LETTO                     PIC X(001).
           88 WK-LETTO-SI                    VALUE 'S'.
           88 WK-LETTO-NO                    VALUE 'N'.
       01  WK-FIND-FIGLIA                    PIC X(001).
           88 WK-FIGLIA-SI                   VALUE 'S'.
           88 WK-FIGLIA-NO                   VALUE 'N'.
       01  WK-DEROGA-OK                      PIC X(001).
           88 WK-DEROGA-SI                   VALUE 'S'.
           88 WK-DEROGA-NO                   VALUE 'N'.
       01  WK-GESTIONE-CALL                  PIC X(001).
           88 WK-ESEGUI-CALL-SI              VALUE 'S'.
           88 WK-ESEGUI-CALL-NO              VALUE 'N'.
       01  WK-CONTESTO                       PIC X(001).
           88 WK-CONTESTO-SI                 VALUE 'S'.
           88 WK-CONTESTO-NO                 VALUE 'N'.
       01  WK-LETURA-MMV                     PIC X(001).
           88 WK-MMV-SI                      VALUE 'S'.
           88 WK-MMV-NO                      VALUE 'N'.
       01  WK-CONTESTO-POG                   PIC X(001).
           88 WK-CONTESTO-SI-POG             VALUE 'S'.
           88 WK-CONTESTO-NO-POG             VALUE 'N'.
        01  WK-CALL-CALCOLO                  PIC X(001).
           88 WK-CALL-CALCOLO-SI             VALUE 'S'.
           88 WK-CALL-CALCOLO-NO             VALUE 'N'.
       01  WK-APP-ASSICURATO                 PIC X(005).
           88 PRIMO-ASS                      VALUE 'PRIMO'.
           88 SECONDO-ASS                    VALUE 'SECON'.
           88 TERZO-ASS                      VALUE 'TERZO'.
           88 TP-OGG-PO                      VALUE 'TPOPO'.
           88 TP-OGG-AD                      VALUE 'TPOAD'.
           88 TP-DEC-PO                      VALUE 'DECPO'.
           88 TP-DEC-AD                      VALUE 'DECAD'.
       01  WK-ERRORE                         PIC X(002).
           88 WK-KO-YES                      VALUE 'SI'.
           88 WK-KO-NO                       VALUE 'NO'.
       01  WK-VALORE-RIS                     PIC X(001).
           88 WK-RIS-TROVATO                 VALUE 'S'.
           88 WK-RIS-NON-TROVATO             VALUE 'N'.
       01  WK-OUT-SCRITTO                    PIC X(001).
           88 WK-OUT-SCRITTO-SI              VALUE 'S'.
           88 WK-OUT-SCRITTO-NO              VALUE 'N'.
       01  WK-COD-DOM                        PIC X(020) VALUE SPACES.
       01  WK-TP-QUEST                       PIC X(002) VALUE SPACES.

       01  WK-ELE-VARIABILI-MAX              PIC 9(003).

       01  FLAG-ID-TROVATO                   PIC X(001).
           88 ID-TROVATO-SI                  VALUE 'S'.
           88 ID-TROVATO-NO                  VALUE 'N'.

       01 FLAG-GESTIONE                 PIC X(02).
          88 GESTIONE-PRODOTTO          VALUE 'PR'.
          88 GESTIONE-GARANZIE          VALUE 'GA'.

       01 FLAG-VAR-IN-CHIARO            PIC X(01).
          88 VAR-IN-CHIARO-SI           VALUE 'S'.
          88 VAR-IN-CHIARO-NO           VALUE 'N'.

      ******************************************************************
      * STRUTTURA PER GESTIONE CACHE COMP_STR_DATO / ANAG_DATO
      ******************************************************************

           COPY IABC0010.

       01 IX-ADDRESS                        PIC 9(2) COMP.

       01 WK-ELE-MAX-ACTU                   PIC S9(05).
       01 WK-IND-SERV                       PIC 9(5) COMP.

      ******************************************************************
      *    COPY TIPOLOGICA DEI MOVIMENTI
      ******************************************************************
           COPY LCCC0006.
           COPY LCCVXMV0.

      ******************************************************************
      *    COPY DISPATCHER
      ******************************************************************
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      ** COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      ** COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.

       01 INPUT-IDSS0020.
           COPY IDSI0021.

       01 OUTPUT-IDSS0020.
           COPY IDSO0021.
      *----------------------------------------------------------------
      * LETTURA MAT-VAL-VAR
      *----------------------------------------------------------------
           COPY LDBV1391.
      *----------------------------------------------------------------
      * LETTURA PARAM-MOV
      *----------------------------------------------------------------
           COPY LDBV1471.
      *----------------------------------------------------------------
      * LETTURA RAPP-ANA
      *----------------------------------------------------------------
           COPY LDBV1291.
      *----------------------------------------------------------------
      * LETTURA DETT-QUE
      *----------------------------------------------------------------
           COPY LDBV2171.
           COPY LDBV5001.
      *----------------------------------------------------------------
      * LETTURA QUEST
      *----------------------------------------------------------------
           COPY LDBV2321.
      *----------------------------------------------------------------
      * LETTURA TRCH-DI-GAR
      *----------------------------------------------------------------
           COPY LDBV0011.

      *----------------------------------------------------------------
      * LETTURA PARAM-OGG
      *----------------------------------------------------------------
53263      COPY LDBV1131.

      * AREA CALL IDSS0020
       01 AREA-CALL.
           COPY IDSV0044.
      *----------------------------------------------------------------*
      *    MODULI CHIAMATI
      *----------------------------------------------------------------*
       01  WKS-NOME-TABELLA                PIC X(030).
       01  WKS-NOME-TABELLA-1              PIC X(030).
       01  PGM-CHIAMATO                    PIC X(008) VALUE SPACES.
       01  PGM-CALCOLI                     PIC X(008) VALUE SPACES.
       01  INTERNAL                        PIC X(008) VALUE 'INTERNAL'.
      *----------------------------------------------------------------*
      *    AREA CALCOLO DIFFERENZA TRA DATE
      *----------------------------------------------------------------*
       01  FORMATO                 PIC X(01).
       01  DATA-INFERIORE.
           05 GG-INF               PIC 9(02) VALUE ZERO.
           05 MM-INF               PIC 9(02) VALUE ZERO.
           05 AAAA-INF             PIC 9(04) VALUE ZERO.
       01  DATA-SUPERIORE.
           05 GG-SUP               PIC 9(02) VALUE ZERO.
           05 MM-SUP               PIC 9(02) VALUE ZERO.
           05 AAAA-SUP             PIC 9(04) VALUE ZERO.
       01  GG-DIFF                 PIC 9(05) VALUE ZERO.
       01  CODICE-RITORNO          PIC X(01).
       01  WK-PARAM-OGG-APPO       PIC X(60000) VALUE SPACES.
      *
       01  WS-DT-EFF-APPO-1        PIC 9(08) VALUE ZERO.
       01  WS-DT-EFF-APPO.
           05 WS-AAAA-AP           PIC 9(04) VALUE ZERO.
           05 WS-MM-AP             PIC 9(02) VALUE ZERO.
           05 WS-GG-AP             PIC 9(02) VALUE ZERO.
      *
      *----------------------------------------------------------------*
      *    AREEA ESTRAZIONE FATTORI
      *----------------------------------------------------------------*
      * 01  AREA-IO-LCCS0045.
      *    02 S045-AREA-INPUT.
      *--  AREA PARAMETRO OGGETTO
      *       03 S045-AREA-PARAM-OGG.
      *          04 S045-ELE-PARAM-OGG-MAX   PIC S9(04) COMP.
      *          04 S045-TAB-PARAM-OGG       OCCURS 60.
      *             COPY LCCVPOG1        REPLACING ==(SF)== BY ==S045==.
      *       03 S045-FUNZIONALITA           PIC  9(005).
      *       03 S045-ID-POLIZZA             PIC  9(009).
      *       03 S045-ID-ADESIONE            PIC  9(009).
      *       03 S045-ID-GARANZIA            PIC  9(009).
      *       03 S045-ID-TRANCHE             PIC  9(009).
      *       03 S045-COD-FATTORE            PIC  X(012).
      *    02 S045-AREA-OUTPUT.
      *       03 S045-VAL-IMP-OUT            PIC 9(015)V9(003).
      *       03 S045-VAL-DT-OUT             PIC 9(008).
      *       03 S045-VAL-TS-OUT             PIC S9(5)V9(9) COMP-3.
      *       03 S045-VAL-TXT-OUT            PIC X(500).
      *       03 S045-VAL-FL-OUT             PIC X(001).
      *       03 S045-VAL-NUM-OUT            PIC 9(005).
      *       03 S045-VAL-PC-OUT             PIC 9(006)V9(003).
      *       03 S045-TIPO-DATO-OUT          PIC X(001).
      *       03 S045-FLAG-TROVATO-OUT       PIC X(001).
      *----------------------------------------------------------------*
      *     AREA struttura tabelle
      *----------------------------------------------------------------*
       01 WKS-STRUTTURA-TAB.
          02 WKS-ELE-MAX-TABB              PIC S9(04) COMP-3.
          02 WKS-AREA-TABB                 OCCURS 60.
             05 WKS-NOME-TABB              PIC X(030).
             05 WKS-ELE-MAX-DATO           PIC S9(04) COMP-3.
             05 WKS-ELEMENTS-STR-DATO OCCURS 250.
                10 WKS-CODICE-DATO        PIC X(030).
                10 WKS-TIPO-DATO          PIC X(002).
                10 WKS-INI-POSI           PIC 9(05) COMP.
                10 WKS-LUN-DATO           PIC 9(05) COMP.
                10 WKS-LUNGHEZZA-DATO-NOM PIC 9(05) COMP.
                10 WKS-PRECISIONE-DATO    PIC 9(02) COMP.
      *
       01 WKS-TAB-NOT-EXI.
          02 WKS-IX-MAX-NOT-EXI           PIC S9(04) COMP-3.

       01 WKS-TROVATO-TABB                 PIC X(02).
          88 WKS-TROVATO-TABB-SI           VALUE 'SI'.
          88 WKS-TROVATO-TABB-NO           VALUE 'NO'.
      *
       01 WKS-TROVATO-DATO                 PIC X(02).
          88 WKS-TROVATO-DATO-SI           VALUE 'SI'.
          88 WKS-TROVATO-DATO-NO           VALUE 'NO'.

      *--  AREA TRANCHE DI GARANZIA
       01 WTGA-AREA-TRANCHE.
           04 WTGA-ELE-TRAN-MAX       PIC S9(04) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==WTGA==.
           COPY LCCVTGA1              REPLACING ==(SF)== BY ==WTGA==.

       01 WPCO-AREA-PARA-COM.
           02 WPCO-ELE-PCO-MAX       PIC S9(04) COMP.
           02 WPCO-TAB-PCO.
           COPY LCCVPCO1             REPLACING ==(SF)== BY ==WPCO==.

       01 WE12-AREA-ESTRA.
           04 WE12-ELE-ESTRA-MAX     PIC S9(04) COMP.
           COPY LCCVE12B REPLACING   ==(SF)==  BY ==WE12==.
           COPY LCCVE121             REPLACING ==(SF)== BY ==WE12==.

       LINKAGE SECTION.
      *----------------------------------------------------------------*
      *    COMMAREA SERVIZIO
      *----------------------------------------------------------------*
      *
           COPY IDSV0003.

       01  IVVC0216-FLG-AREA                      PIC  X(02).
           88 IVVC0216-AREA-VE                    VALUE 'VE'.
           88 IVVC0216-AREA-PV                    VALUE 'PV'.

       01  AREA-INPUT.
           COPY IVVC0200                 REPLACING ==(SF)== BY ==C216==.
      *    COPY IVVC0216                 REPLACING ==(SF)== BY ==C216==.

      *--  AREA SCHEDA VARIABILI
       01  AREA-WARNING-IVVC0216.
           COPY IVVC0215                 REPLACING ==(SF)== BY ==C216==.

      *--  AREA DCLGEN
      *----------------------------------------------------------------*
      *--  AREA DCLGEN
       01  AREA-BUSINESS.
      *----------------------------------------------------------------*
      *   AREA INPUT VARIABILI
      *   PORTAFOGLIO VITA - PROCESSO VENDITA
      *   LUNG.
      *----------------------------------------------------------------*
      *--  AREA ADESIONE
           02 WADE-AREA-ADESIONE.
              04 WADE-ELE-ADES-MAX       PIC S9(04) COMP.
              04 WADE-TAB-ADES           OCCURS 1.
              COPY LCCVADE1              REPLACING ==(SF)== BY ==WADE==.
      *--  AREA BENEFICIARI
           02 WBEP-AREA-BENEF.
              04 WBEP-ELE-BENEF-MAX      PIC S9(04) COMP.
                COPY LCCVBEPA REPLACING   ==(SF)==  BY ==WBEP==.
              COPY LCCVBEP1              REPLACING ==(SF)== BY ==WBEP==.
      *--  AREA BENEFICIARIO DI LIQUIDAZIONE
           02 WBEL-AREA-BENEF-LIQ.
              04 WBEL-ELE-BENEF-LIQ-MAX  PIC S9(04) COMP.
                COPY LCCVBELA REPLACING   ==(SF)==  BY ==WBEL==.
              COPY LCCVBEL1              REPLACING ==(SF)== BY ==WBEL==.
      *--  AREA DATI COLLETTIVA
           02 WDCO-AREA-DT-COLLETTIVA.
              04 WDCO-ELE-COLL-MAX       PIC S9(004) COMP.
              04 WDCO-TAB-COLL.
              COPY LCCVDCO1              REPLACING ==(SF)== BY ==WDCO==.
      *--  AREA DATI FISCALE ADESIONE
           02 WDFA-AREA-DT-FISC-ADES.
              04 WDFA-ELE-FISC-ADES-MAX  PIC S9(04) COMP.
              04 WDFA-TAB-FISC-ADES.
              COPY LCCVDFA1              REPLACING ==(SF)== BY ==WDFA==.
      *--  AREA DETTAGLIO QUESTIONARIO
           02 WDEQ-AREA-DETT-QUEST.
              04 WDEQ-ELE-DETT-QUEST-MAX PIC S9(04) COMP.
              04 WDEQ-TAB-DETT-QUEST     OCCURS 180.
              COPY LCCVDEQ1              REPLACING ==(SF)== BY ==WDEQ==.
      *--  AREA DETTAGLIO TITOLO CONTABILE
           02 WDTC-AREA-DETT-TIT-CONT.
              04 WDTC-ELE-DETT-TIT-MAX   PIC S9(04) COMP.
                COPY LCCVDTCA REPLACING   ==(SF)==  BY ==WDTC==.
              COPY LCCVDTC1              REPLACING ==(SF)== BY ==WDTC==.
      *--  AREA GARANZIA
           02 WGRZ-AREA-GARANZIA.
              04 WGRZ-ELE-GARANZIA-MAX   PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
              COPY LCCVGRZ1              REPLACING ==(SF)== BY ==WGRZ==.
      *--  AREA GARANZIA DI LIQUIDAZIONE
           02 WGRL-AREA-GARANZIA-LIQ.
              04 WGRL-ELE-GAR-LIQ-MAX    PIC S9(04) COMP.
                COPY LCCVGRLA REPLACING   ==(SF)==  BY ==WGRL==.
              COPY LCCVGRL1              REPLACING ==(SF)== BY ==WGRL==.
      *--  AREA IMPOSTA SOSTITUTIVA
           02 WISO-AREA-IMPOSTA-SOST.
              04 WISO-ELE-IMP-SOST-MAX   PIC S9(04) COMP.
              04 WISO-TAB-IMP-SOST       OCCURS 1.
              COPY LCCVISO1              REPLACING ==(SF)== BY ==WISO==.
      *--  AREA LIQUIDAZIONE
           02 WLQU-AREA-LIQUIDAZIONE.
              04 WLQU-ELE-LIQ-MAX        PIC S9(04) COMP.
                COPY LCCVLQUB REPLACING   ==(SF)==  BY ==WLQU==.
              COPY LCCVLQU1              REPLACING ==(SF)== BY ==WLQU==.
      *--  AREA MOVIMENTO
           02 WMOV-AREA-MOVIMENTO.
              04 WMOV-ELE-MOVI-MAX       PIC S9(04) COMP.
              04 WMOV-TAB-MOVI.
              COPY LCCVMOV1              REPLACING ==(SF)== BY ==WMOV==.
      *--  AREA MOVIMENTO FINANZIARIO
           02 WMFZ-AREA-MOVI-FINRIO.
              04 WMFZ-ELE-MOVI-FINRIO-MAX  PIC S9(04) COMP.
              04 WMFZ-TAB-MOVI-FINRIO    OCCURS 10.
                 COPY LCCVMFZ1           REPLACING ==(SF)== BY ==WMFZ==.
      *--  AREA PARAMETRO OGGETTO
           02 WPOG-AREA-PARAM-OGG.
              04 WPOG-ELE-PARAM-OGG-MAX  PIC S9(04) COMP.
                COPY LCCVPOGB REPLACING   ==(SF)==  BY ==WPOG==.
              COPY LCCVPOG1              REPLACING ==(SF)== BY ==WPOG==.
      *--  AREA PARAMETRO MOVIMENTO
           02 WPMO-AREA-PARAM-MOV.
              04 WPMO-ELE-PARAM-MOV-MAX  PIC S9(04) COMP.
                COPY LCCVPMOB REPLACING   ==(SF)==  BY ==WPMO==.
              COPY LCCVPMO1              REPLACING ==(SF)== BY ==WPMO==.
      *--  AREA PERCIPIENTE LIQUIDAZIONE
           02 WPLI-AREA-PERC-LIQ.
              04 WPLI-ELE-PERC-LIQ-MAX   PIC S9(04) COMP.
                COPY LCCVPLIA REPLACING   ==(SF)==  BY ==WPLI==.
              COPY LCCVPLI1              REPLACING ==(SF)== BY ==WPLI==.
      *--  AREA POLIZZA
           02 WPOL-AREA-POLIZZA.
              04 WPOL-ELE-POLI-MAX       PIC S9(04) COMP.
              04 WPOL-TAB-POLI.
              COPY LCCVPOL1              REPLACING ==(SF)== BY ==WPOL==.
      *--  AREA PRESTITI
           02 WPRE-AREA-PRESTITI.
              04 WPRE-ELE-PRESTITI-MAX   PIC S9(04) COMP.
              04 WPRE-TAB-PRESTITI       OCCURS 10.
              COPY LCCVPRE1              REPLACING ==(SF)== BY ==WPRE==.
      **--  AREA PROVVIGIONI DI TRANCHE
           02 WPVT-AREA-PROV.
              04 WPVT-ELE-PROV-MAX       PIC S9(04) COMP.
                COPY LCCVPVTA REPLACING   ==(SF)==  BY ==WPVT==.
              COPY LCCVPVT1              REPLACING ==(SF)== BY ==WPVT==.
      *--  AREA QUESTIONARIO
           02 WQUE-AREA-QUEST.
              04 WQUE-ELE-QUEST-MAX      PIC S9(04) COMP.
              04 WQUE-TAB-QUEST          OCCURS 12.
              COPY LCCVQUE1              REPLACING ==(SF)== BY ==WQUE==.
      *--  AREA RICHIESTA
           02 WRIC-AREA-RICH.
              04 WRIC-ELE-RICH-MAX       PIC S9(04) COMP.
              04 WRIC-TAB-RICH.
              COPY LCCVRIC1              REPLACING ==(SF)== BY ==WRIC==.
      *--  AREA RICHIESTA DISINVESTIMENTO FONDO
           02 WRDF-AREA-RICH-DISINV-FND.
                04 WRDF-ELE-RIC-INV-MAX      PIC S9(04) COMP.
                COPY LCCVRDFA REPLACING   ==(SF)==  BY ==WRDF==.
                 COPY LCCVRDF1           REPLACING ==(SF)== BY ==WRDF==.
      *--  AREA RICHIESTA INVESTIMENTO FONDO
           02 WRIF-AREA-RICH-INV-FND.
                04 WRIF-ELE-RIC-INV-MAX      PIC S9(04) COMP.
                COPY LCCVRIFA REPLACING   ==(SF)==  BY ==WRIF==.
                 COPY LCCVRIF1           REPLACING ==(SF)== BY ==WRIF==.
      *--  AREA RAPPORTO ANAGRAFICO
           02 WRAN-AREA-RAPP-ANAG.
              04 WRAN-ELE-RAPP-ANAG-MAX  PIC S9(04) COMP.
                COPY LCCVRANA REPLACING   ==(SF)==  BY ==WRAN==.
              COPY LCCVRAN1              REPLACING ==(SF)== BY ==WRAN==.
      *--  AREA RAPPORTO ANAGRAFICO
           02 WE15-AREA-EST-RAPP-ANAG.
              04 WE15-ELE-EST-RAPP-ANAG-MAX  PIC S9(04) COMP.
                COPY LCCVE15A REPLACING   ==(SF)==  BY ==WE15==.
              COPY LCCVE151              REPLACING ==(SF)== BY ==WE15==.
      *--  AREA RAPPORTO RETE
           02 WRRE-AREA-RAPP-RETE.
              04 WRRE-ELE-RAPP-RETE-MAX  PIC S9(04) COMP.
              04 WRRE-TAB-RAPP-RETE      OCCURS 10.
              COPY LCCVRRE1              REPLACING ==(SF)== BY ==WRRE==.
      *--  AREA SOPRAPREMIO DI GARANZIA
           02 WSPG-AREA-SOPRAP-GAR.
              04 WSPG-ELE-SOPRAP-GAR-MAX PIC S9(04) COMP.
                COPY LCCVSPGA REPLACING   ==(SF)==  BY ==WSPG==.
              COPY LCCVSPG1              REPLACING ==(SF)== BY ==WSPG==.
      *--  AREA STRATEGIA DI INVESTIMENTO
           02 WSDI-AREA-STRA-INV.
              04 WSDI-ELE-STRA-INV-MAX   PIC S9(04) COMP.
              04 WSDI-TAB-STRA-INV       OCCURS 20.
              COPY LCCVSDI1              REPLACING ==(SF)== BY ==WSDI==.
      *--  AREA TITOLO CONTABILE
           02 WTIT-AREA-TIT-CONT.
              04 WTIT-ELE-TIT-CONT-MAX   PIC S9(04) COMP.
                COPY LCCVTITA REPLACING   ==(SF)==  BY ==WTIT==.
              COPY LCCVTIT1              REPLACING ==(SF)== BY ==WTIT==.
      *--  AREA TITOLO DI LIQUIDAZIONE
           02 WTCL-AREA-TIT-LIQ.
              04 WTCL-ELE-TIT-LIQ-MAX    PIC S9(04) COMP.
              04 WTCL-TAB-TIT-LIQ.
              COPY LCCVTCL1              REPLACING ==(SF)== BY ==WTCL==.
      *--  AREA TRANCHE DI GARANZIA
           02 W1TGA-AREA-TRANCHE.
              04 W1TGA-ELE-TRAN-MAX       PIC S9(04) COMP.
                COPY LCCVTGAC REPLACING   ==(SF)==  BY ==W1TGA==.
              COPY LCCVTGA1             REPLACING ==(SF)== BY ==W1TGA==.
      *-- AREA TRANCHE GARANZIA DI LIQUIDAZIONE
           02 WTLI-AREA-TRCH-LIQ.
              04 WTLI-ELE-TRCH-LIQ-MAX   PIC S9(04) COMP.
                COPY LCCVTLIB REPLACING   ==(SF)==  BY ==WTLI==.
              COPY LCCVTLI1              REPLACING ==(SF)== BY ==WTLI==.
      *-- AREA DEFAULT ADESIONE
           02 WDAD-AREA-DEFAULT-ADES.
              04 WDAD-ELE-DFLT-ADES-MAX  PIC S9(04) COMP.
              04 WDAD-TAB-DFLT-ADES.
              COPY LCCVDAD1              REPLACING ==(SF)== BY ==WDAD==.
      *-- AREA OGGETTO COLLEGATO
           02 WOCO-AREA-OGG-COLL.
              04 WOCO-ELE-OGG-COLL-MAX   PIC S9(04) COMP.
              04 WOCO-TAB-OGG-COLL       OCCURS 60.
              COPY LCCVOCO1              REPLACING ==(SF)== BY ==WOCO==.
      *-- AREA DATI FORZATI DI LIQUIDAZIONE
           02 WDFL-AREA-DFL.
              04 WDFL-ELE-DFL-MAX        PIC S9(004) COMP.
              04 WDFL-TAB-DFL.
              COPY LCCVDFL1              REPLACING ==(SF)== BY ==WDFL==.
      *-- AREA RISERVA DI TRANCHE
           02 WRST-AREA-RST.
              04 WRST-ELE-RST-MAX        PIC S9(004) COMP.
              04 WRST-TAB-RST            OCCURS 10.
              COPY LCCVRST1              REPLACING ==(SF)== BY ==WRST==.
      *-- AREA QUOTAZ FONDI UNIT
           02 WL19-AREA-QUOTE.
              COPY LCCVL197              REPLACING ==(SF)== BY ==WL19==.
              COPY LCCVL191              REPLACING ==(SF)== BY ==WL19==.
      *-- AREA REINVEST
           02 WL30-AREA-REINVST-POLI.
              04 WL30-ELE-REINVST-POLI-MAX PIC S9(04) COMP.
              04 WL30-TAB-REINVST-POLI   OCCURS 40.
              COPY LCCVL301              REPLACING ==(SF)== BY ==WL30==.
      *--  VINCOLO PEGNO
           02 WL23-AREA-VINC-PEG.
              04 WL23-ELE-VINC-PEG-MAX   PIC S9(04) COMP.
              04 WL23-TAB-VINC-PEG       OCCURS 10.
              COPY LCCVL231              REPLACING ==(SF)== BY ==WL23==.
      *-- AREA OPZIONI
           02 WOPZ-AREA-OPZIONI.
              COPY IVVC0217              REPLACING ==(SF)== BY ==WOPZ==.
2404  *--  AREA GARANZIA DI OPZIONE
2404       02 WGOP-AREA-GARANZIA-OPZ.
2404          04 WGOP-ELE-GARANZIA-OPZ-MAX   PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGOP==.
2404          COPY LCCVGRZ1              REPLACING ==(SF)== BY ==WGOP==.
2404  *--  AREA TRANCHE DI GARANZIADI OPZIONE
2404       02 WTOP-AREA-TRANCHE-OPZ.
2404          04 WTOP-ELE-TRAN-OPZ-MAX       PIC S9(04) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==WTOP==.
2404          COPY LCCVTGA1              REPLACING ==(SF)== BY ==WTOP==.
      *--  AREA DATI CONTESTUALI
           02 WCNT-AREA-DATI-CONTEST.
              COPY IVVC0212              REPLACING ==(SF)== BY ==WCNT==.
      *--  AREA CLAUSOLA DI TESTO
           02 WCLT-AREA-CLAU-TXT.
              04 WCLT-ELE-CLAU-TXT-MAX       PIC S9(04) COMP.
              04 WCLT-TAB-CLAU-TXT           OCCURS 54.
              COPY LCCVCLT1              REPLACING ==(SF)== BY ==WCLT==.
NEW   *--  IMPOSTA DI BOLLO
NEW        02 WP58-AREA-IMPOSTA-BOLLO.
NEW           04 WP58-ELE-MAX-IMPST-BOL PIC S9(04) COMP.
52921 *       04 WP58-TAB-IMPST-BOL OCCURS 50.
52921         04 WP58-TAB-IMPST-BOL OCCURS 75.
NEW           COPY LCCVP581              REPLACING ==(SF)== BY ==WP58==.
NEW01 *--  DATI CRISTALLIZZATI
NEW01      02 WP61-AREA-D-CRIST.
NEW01         04 WP61-ELE-MAX-D-CRIST   PIC S9(04) COMP.
NEW01         04 WP61-TAB-D-CRIST.
NEW01         COPY LCCVP611              REPLACING ==(SF)== BY ==WP61==.
      *--  DATI AGGIUNTIVI PER POLIZZE CPI
           02 WP67-AREA-EST-POLI-CPI-PR.
              04 WP67-ELE-MAX-EST-POLI-CPI-PR   PIC S9(04) COMP.
              04 WP67-TAB-EST-POLI-CPI-PR.
              COPY LCCVP671              REPLACING ==(SF)== BY ==WP67==.
      *--  RICHIESTA ESTERNA
           02 WP01-AREA-RICH-EST.
              04 WP01-ELE-MAX-RICH-EST   PIC S9(04) COMP.
              04 WP01-TAB-RICH-EST.
              COPY LCCVP011              REPLACING ==(SF)== BY ==WP01==.
      *--  AREA CONTROVALORI FONDI X TRANCE
           02 IVVC0222-AREA-FND-X-TRANCHE.
              04 IVVC0222-ELE-TRCH-MAX       PIC S9(04) COMP.
              COPY LCCVTGAC          REPLACING ==(SF)== BY ==IVVC0222==.
              COPY IVVC0222          REPLACING ==(SF)== BY ==IVVC0222==.
      *-- MOTIVO LIQUIDAZIONE
           02 WP86-AREA-MOT-LIQ.
              04 WP86-ELE-MOT-LIQ-MAX    PIC S9(04) COMP.
              04 WP86-TAB-MOT-LIQ        OCCURS 01.
              COPY LCCVP861              REPLACING ==(SF)== BY ==WP86==.
12969 *--  AREA VARIABILI PER GARANZIA
12969      02 AREA-IVVC0223.
              COPY IVVC0223          REPLACING ==(SF)== BY ==IVVC0223==.
      *--  VARIABILE PER APPOGGIO MOVIMENTO DI ORIGINE (X LIQUIDAZIONE)
           02 WK-MOVI-ORIG                   PIC 9(05).
      *--  ATTIVAZIONE SERVIZI VALORE
           02 WP88-AREA-SERV-VAL.
              04 WP88-ELE-SER-VAL-MAX         PIC S9(04) COMP.
              04 WP88-TAB-SERV-VAL                 OCCURS 10.
              COPY LCCVP881                   REPLACING ==(SF)==
                                              BY ==WP88==.
      *--  DETTAGLIO ATTIVAZIONE SERVIZI VALORE
           02 WP89-AREA-DSERV-VAL.
              04 WP89-ELE-DSERV-VAL-MAX        PIC S9(04) COMP.
48945 *       04 WP89-TAB-DSERV-VAL                 OCCURS 250.
48945         04 WP89-TAB-DSERV-VAL                 OCCURS 350.
              COPY LCCVP891                   REPLACING ==(SF)==
                                              BY ==WP89==.
13385 *--  QUESTIONARIO ADEGUATA VERIFICA
13385      02 WP56-AREA-QUEST-ADEG-VER.
13385         04 WP56-QUEST-ADEG-VER-MAX    PIC S9(004) COMP.
             COPY LCCVP56A REPLACING   ==(SF)==  BY ==WP56==.
13385         COPY LCCVP561                 REPLACING ==(SF)==
13385                                              BY ==WP56==.

16324 *--  AREA AREA FONDI COMMISSIONI GESTIONE
16324      02 WCDG-AREA-COMMIS-GEST.
16324         COPY IVVC0224                 REPLACING ==(SF)==
16324                                              BY ==WCDG==.

      ******************************************************************
      * STRUTTURA PER GESTIONE CACHE COMP_STR_DATO / ANAG_DATO
      ******************************************************************
       01 AREA-IDSV0101.
          COPY IDSV0101.

       01 AREA-IDSV0102.
          COPY IDSV0102.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003
                                IVVC0216-FLG-AREA
                                AREA-INPUT
                                AREA-WARNING-IVVC0216
                                AREA-BUSINESS
                                AREA-IDSV0101.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.
      *
           INITIALIZE WKS-INDICI
                      C214-AREA-VARIABILI
                      WE12-AREA-ESTRA
                      WK1-VAR-STR
                      WKS-STRUTTURA-TAB.
      *
           MOVE IDSV0003-TIPO-MOVIMENTO        TO WS-MOVIMENTO.
      *
           SET IDSV0003-SUCCESSFUL-RC          TO TRUE.
           SET SKEDA-OPZIONE-NO                TO TRUE.

           SET ADDRESS-CACHE-VAL-VAR-NO        TO TRUE.

           IF  IDSV0003-BATCH-INFR
               SET ADDRESS-CACHE-VAL-VAR       TO TRUE
               PERFORM VARYING IX-ADDRESS FROM 1 BY 1
                       UNTIL   IX-ADDRESS > 5 OR
                               ADDRESS-CACHE-VAL-VAR-SI
                       IF TIPO-ADDRESS = IDSV0101-ADDRESS-TYPE
                                         (IX-ADDRESS)
                          SET ADDRESS OF AREA-IDSV0102 TO
                              IDSV0101-ADDRESS (IX-ADDRESS)
                          SET ADDRESS-CACHE-VAL-VAR-SI TO TRUE
                       END-IF
               END-PERFORM
           END-IF.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.
      *--> ELABORAZIONE INPUT

           SET GESTIONE-PRODOTTO         TO TRUE
           PERFORM VARYING IX-VAL-VAR FROM 1 BY 1
             UNTIL IX-VAL-VAR > C216-ELE-LIVELLO-MAX-P
                OR NOT IDSV0003-SUCCESSFUL-RC
      *
2404            IF C216-COD-TIPO-OPZIONE-P(IX-VAL-VAR) NOT =
2404               SPACES AND LOW-VALUES AND HIGH-VALUES
2404               SET SKEDA-OPZIONE-SI        TO TRUE
2404            END-IF

               IF C216-NOME-SERVIZIO-P(IX-VAL-VAR) = SPACES
                  PERFORM S1110-COD-VAR         THRU EX-S1110
                    VARYING IX-COD-VAR FROM 1 BY 1
                      UNTIL IX-COD-VAR >
                            C216-ELE-VARIABILI-MAX-P(IX-VAL-VAR)
                         OR NOT IDSV0003-SUCCESSFUL-RC
               END-IF
      *
               PERFORM S2001-AZZERA-VARIABILI
                  THRU S2001-EX
      *
2404           SET SKEDA-OPZIONE-NO           TO TRUE
           END-PERFORM.

           SET GESTIONE-GARANZIE         TO TRUE
           PERFORM VARYING IX-VAL-VAR FROM 1 BY 1
             UNTIL IX-VAL-VAR > C216-ELE-LIVELLO-MAX-T
                OR NOT IDSV0003-SUCCESSFUL-RC
      *
                IF C216-COD-TIPO-OPZIONE-T(IX-VAL-VAR) NOT =
                   SPACES AND LOW-VALUES AND HIGH-VALUES
                   SET SKEDA-OPZIONE-SI        TO TRUE
                END-IF

               IF C216-NOME-SERVIZIO-T(IX-VAL-VAR) = SPACES
                  PERFORM S1110-COD-VAR         THRU EX-S1110
                    VARYING IX-COD-VAR FROM 1 BY 1
                      UNTIL IX-COD-VAR >
                            C216-ELE-VARIABILI-MAX-T(IX-VAL-VAR)
                         OR NOT IDSV0003-SUCCESSFUL-RC
               END-IF
      *
               PERFORM S2001-AZZERA-VARIABILI
                  THRU S2001-EX
      *
               SET SKEDA-OPZIONE-NO           TO TRUE
           END-PERFORM.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    AZZERAMETNO VARIABILI IN USCITA SE NON VALORIZZATE
      *----------------------------------------------------------------*
       S2001-AZZERA-VARIABILI.
      *
           IF GESTIONE-PRODOTTO
              PERFORM VARYING IX-VAR-LIS FROM 1 BY 1
                UNTIL IX-VAR-LIS > WK1-MAX-TAB-STRING
                  ADD 1                    TO IX-COD-VAR-OUT
                  MOVE WK1-COD-VARIABILE(IX-VAR-LIS)
                    TO C216-COD-VARIABILE-P(IX-VAL-VAR, IX-COD-VAR-OUT)
                  MOVE WK1-TP-STRINGA(IX-VAR-LIS)
                    TO C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR-OUT)
                  MOVE WK1-STRINGA-TOT(IX-VAR-LIS)
                    TO C216-VAL-STR-P(IX-VAL-VAR, IX-COD-VAR-OUT)
              END-PERFORM

              PERFORM  VARYING IX-COD-VAR FROM 1 BY 1
                  UNTIL IX-COD-VAR > C216-NUM-MAX-VARIABILI-P
                  IF IX-COD-VAR GREATER IX-COD-VAR-OUT
                     MOVE SPACES TO  C216-AREA-VARIABILE-P
                                (IX-VAL-VAR, IX-COD-VAR)
                  END-IF
              END-PERFORM
      *
              MOVE IX-COD-VAR-OUT
                TO C216-ELE-VARIABILI-MAX-P(IX-VAL-VAR)
           ELSE
              PERFORM VARYING IX-VAR-LIS FROM 1 BY 1
                UNTIL IX-VAR-LIS > WK1-MAX-TAB-STRING
                  ADD 1                    TO IX-COD-VAR-OUT
                  MOVE WK1-COD-VARIABILE(IX-VAR-LIS)
                    TO C216-COD-VARIABILE-T(IX-VAL-VAR, IX-COD-VAR-OUT)
                  MOVE WK1-TP-STRINGA(IX-VAR-LIS)
                    TO C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR-OUT)
                  MOVE WK1-STRINGA-TOT(IX-VAR-LIS)
                    TO C216-VAL-STR-T(IX-VAL-VAR, IX-COD-VAR-OUT)
              END-PERFORM

              PERFORM  VARYING IX-COD-VAR FROM 1 BY 1
                  UNTIL IX-COD-VAR > C216-NUM-MAX-VARIABILI-T
                  IF IX-COD-VAR GREATER IX-COD-VAR-OUT
                     MOVE SPACES TO  C216-AREA-VARIABILE-T
                                (IX-VAL-VAR, IX-COD-VAR)
                  END-IF
              END-PERFORM
      *
              MOVE IX-COD-VAR-OUT
                TO C216-ELE-VARIABILI-MAX-T(IX-VAL-VAR)
           END-IF
      *
           MOVE ZEROES                          TO IX-COD-VAR-OUT.
           INITIALIZE                              WK1-VAR-STR.
      *
           IF IDSV0003-FIELD-NOT-VALUED
              PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
           END-IF.
      *
       S2001-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE COD-VARIABILE
      *----------------------------------------------------------------*
       S1110-COD-VAR.
      *
           MOVE 0                                 TO IX-CONTA.
           SET WK-CALCOLI-NO                      TO TRUE.
           SET WK-CALL-CALCOLO-NO                 TO TRUE.
           MOVE 1                                 TO IX-COD-MVV.
           SET C214-SALTA-NO                      TO TRUE.

           IF  IDSV0003-BATCH-INFR
               SET TROVATO-DATO-IN-CACHE-NO   TO TRUE
               IF ADDRESS-CACHE-VAL-VAR-SI
                  PERFORM Z300-RICERCA-DATO-IN-CACHE THRU Z300-EX
               END-IF
               IF TROVATO-DATO-IN-CACHE-NO
                  SET  CACHE-PIENA-NO                TO TRUE
                  PERFORM T0000-TRATTA-MATRICE        THRU T0000-EX

                  IF IDSO0011-SUCCESSFUL-RC AND
                     IDSO0011-SUCCESSFUL-SQL
                     IF ADDRESS-CACHE-VAL-VAR-SI
                        COMPUTE WK-ELE-MAX-ACTU =
                                IDSV0102A-ELE-MAX-ACTU +
                                V1391-ELE-MAX-ACTU
                        IF WK-ELE-MAX-ACTU > IDSV0102A-LIMITE-MAX
                           SET CACHE-PIENA-SI   TO TRUE
                           MOVE WK-PGM       TO IDSV0003-COD-SERVIZIO-BE
                           MOVE 'OVERFLOW TABELLA CACHE X CSD-ADA'
                                             TO IDSV0003-DESCRIZ-ERR-DB2
                        ELSE
                           IF GESTIONE-PRODOTTO
                             PERFORM Z500-CARICA-CACHE    THRU Z500-EX
                                     VARYING WK-IND-SERV FROM 1 BY 1
                                     UNTIL WK-IND-SERV >
                                           C216-NUM-MAX-VARIABILI-P
                                     OR WK-IND-SERV > V1391-ELE-MAX-ACTU
                           ELSE
                             PERFORM Z500-CARICA-CACHE    THRU Z500-EX
                                     VARYING WK-IND-SERV FROM 1 BY 1
                                     UNTIL WK-IND-SERV >
                                           C216-NUM-MAX-VARIABILI-T
                                     OR WK-IND-SERV > V1391-ELE-MAX-ACTU
                           END-IF
                        END-IF
                     END-IF
                  END-IF
               ELSE
                  MOVE  ZERO    TO  WK-IND-SERV
                  INITIALIZE V1391-AREA-ACTU
                  SET WK-MMV-SI                    TO TRUE
                  MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                       TO V1391-COD-COMPAGNIA-ANIA
                  IF GESTIONE-PRODOTTO
                     MOVE 'PO'                     TO WK-APPO-TP-OGG
                     MOVE C216-COD-VARIABILE-P(IX-VAL-VAR, IX-COD-VAR)
                       TO V1391-COD-ACTU
                     PERFORM Z400-CARICA-TAB-V1391   THRU Z400-EX
                       VARYING IX-CACHE FROM IDSV0102A-SEARCH-INDEX
                       BY 1
                       UNTIL IX-CACHE     > IDSV0102A-ELE-MAX-ACTU
                       OR IDSV0102A-COD-DATO-EXT(IX-CACHE) NOT =
                          C216-COD-VARIABILE-P(IX-VAL-VAR, IX-COD-VAR)
                  ELSE
                     MOVE 'GA'                     TO WK-APPO-TP-OGG
                     MOVE C216-COD-VARIABILE-T(IX-VAL-VAR, IX-COD-VAR)
                       TO V1391-COD-ACTU
                     PERFORM Z400-CARICA-TAB-V1391   THRU Z400-EX
                       VARYING IX-CACHE FROM IDSV0102A-SEARCH-INDEX
                       BY 1
                       UNTIL IX-CACHE     > IDSV0102A-ELE-MAX-ACTU
                       OR IDSV0102A-COD-DATO-EXT(IX-CACHE) NOT =
                          C216-COD-VARIABILE-T(IX-VAL-VAR, IX-COD-VAR)
                  END-IF

               END-IF
           ELSE
               PERFORM T0000-TRATTA-MATRICE        THRU T0000-EX
           END-IF
      *
           IF IDSV0003-SUCCESSFUL-RC

              PERFORM U0000-VERIFICA-LETT            THRU U0000-EX
      *
              PERFORM VARYING IX-COD-MVV FROM 1 BY 1
                UNTIL IX-COD-MVV > V1391-ELE-MAX-ACTU
                  OR NOT IDSV0003-SUCCESSFUL-RC
                IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV)
                                                     NOT = HIGH-VALUES
                   SET WK-CALCOLI-SI                 TO TRUE
                END-IF
      *
                IF  V1391-VALORE-DEFAULT(IX-COD-MVV)  NOT = HIGH-VALUES
                AND V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) NOT = '$$'
                AND V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) NOT = '**'
                     PERFORM T0001-ELABORA-DEFAULT  THRU T0001-EX
                ELSE
                     PERFORM T0002-ELABORA-DATO     THRU T0002-EX
                END-IF

              END-PERFORM

              IF V1391-ELE-MAX-ACTU > 1
              AND IDSV0003-SUCCESSFUL-RC
                 MOVE V1391-ELE-MAX-ACTU           TO IX-COD-MVV
      *
                 IF WK-CALCOLI-SI
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM U0010-VALORIZZA-DATO    THRU U0010-EX
                 END-IF
              END-IF
      *
           END-IF.
      *
       EX-S1110.
           EXIT.
      *----------------------------------------------------------------*
      *    RICERCA IN CACHE
      *----------------------------------------------------------------*
       Z300-RICERCA-DATO-IN-CACHE.

           SET IDSV0102A-SEARCH-INDEX                TO 1

           IF GESTIONE-PRODOTTO
              SEARCH IDSV0102A-TAB-PARAM

                 AT END
                      SET TROVATO-DATO-IN-CACHE-NO       TO TRUE

                 WHEN IDSV0102A-COD-DATO-EXT (IDSV0102A-SEARCH-INDEX) =
                      C216-COD-VARIABILE-P(IX-VAL-VAR, IX-COD-VAR)
                      SET TROVATO-DATO-IN-CACHE-SI       TO TRUE

                      ADD 1            TO IDSV0102A-CONTA-CACHE-ADA-MVV

                 WHEN IDSV0102A-COD-DATO-EXT (IDSV0102A-SEARCH-INDEX) =
                      SPACES OR HIGH-VALUE OR LOW-VALUE
                      CONTINUE

              END-SEARCH
           ELSE
              SEARCH IDSV0102A-TAB-PARAM

                 AT END
                      SET TROVATO-DATO-IN-CACHE-NO       TO TRUE

                 WHEN IDSV0102A-COD-DATO-EXT (IDSV0102A-SEARCH-INDEX) =
                      C216-COD-VARIABILE-T(IX-VAL-VAR, IX-COD-VAR)
                      SET TROVATO-DATO-IN-CACHE-SI       TO TRUE

                      ADD 1            TO IDSV0102A-CONTA-CACHE-ADA-MVV

                 WHEN IDSV0102A-COD-DATO-EXT (IDSV0102A-SEARCH-INDEX) =
                      SPACES OR HIGH-VALUE OR LOW-VALUE
                      CONTINUE

              END-SEARCH
           END-IF.

       Z300-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    CARICA TABELLA DI APPOGGIO V1391
      *----------------------------------------------------------------*
       Z400-CARICA-TAB-V1391.

           ADD   1               TO  WK-IND-SERV
                                     V1391-ELE-MAX-ACTU.

           IF IDSV0102A-TIPO-DATO-NULL(IX-CACHE) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO V1391-TIPO-DATO-NULL(WK-IND-SERV)
           ELSE
              MOVE IDSV0102A-TIPO-DATO(IX-CACHE)
                TO V1391-TIPO-DATO(WK-IND-SERV)
           END-IF.

           IF IDSV0102A-LUNGHEZZA-DATO-NULL(IX-CACHE) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO V1391-LUNGHEZZA-DATO-NULL(WK-IND-SERV)
           ELSE
              MOVE IDSV0102A-LUNGHEZZA-DATO(IX-CACHE)
                TO V1391-LUNGHEZZA-DATO(WK-IND-SERV)
           END-IF.

           IF IDSV0102A-PRECISIONE-DATO-NULL(IX-CACHE) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO V1391-PRECISIONE-DATO-NULL(WK-IND-SERV)
           ELSE
              MOVE IDSV0102A-PRECISIONE-DATO(IX-CACHE)
                TO V1391-PRECISIONE-DATO(WK-IND-SERV)
           END-IF.

           MOVE IDSV0102A-FORMATTAZIONE-DATO(IX-CACHE)
             TO V1391-FORMATTAZIONE-DATO(WK-IND-SERV).
      *
           MOVE IDSV0102A-ID-MATR-VAL-VAR(IX-CACHE)
             TO V1391-ID-MATR-VAL-VAR(WK-IND-SERV).

           IF IDSV0102A-IDP-MAT-VAL-VAR-NULL(IX-CACHE) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO V1391-IDP-MATR-VAL-VAR-NULL(WK-IND-SERV)
           ELSE
              MOVE IDSV0102A-IDP-MATR-VAL-VAR(IX-CACHE)
                TO V1391-IDP-MATR-VAL-VAR(WK-IND-SERV)
           END-IF.

           IF IDSV0102A-TIPO-MOVIMENTO-NULL(IX-CACHE) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO V1391-TIPO-MOVIMENTO-NULL(WK-IND-SERV)
           ELSE
              MOVE IDSV0102A-TIPO-MOVIMENTO(IX-CACHE)
                TO V1391-TIPO-MOVIMENTO(WK-IND-SERV)
           END-IF.

           MOVE IDSV0102A-COD-DATO-EXT(IX-CACHE)
             TO V1391-COD-DATO-EXT(WK-IND-SERV).

           IF IDSV0102A-OBBLIGATORIETA-NULL(IX-CACHE) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO V1391-OBBLIGATORIETA-NULL(WK-IND-SERV)
           ELSE
              MOVE IDSV0102A-OBBLIGATORIETA(IX-CACHE)
                TO V1391-OBBLIGATORIETA(WK-IND-SERV)
           END-IF.

           MOVE IDSV0102A-VALORE-DEFAULT(IX-CACHE)
             TO V1391-VALORE-DEFAULT(WK-IND-SERV).
           MOVE IDSV0102A-COD-STR-DATO-PTF(IX-CACHE)
             TO V1391-COD-STR-DATO-PTF(WK-IND-SERV).
           MOVE IDSV0102A-COD-DATO-PTF(IX-CACHE)
             TO V1391-COD-DATO-PTF(WK-IND-SERV).
           MOVE IDSV0102A-COD-PARAMETRO(IX-CACHE)
             TO V1391-COD-PARAMETRO(WK-IND-SERV).
           MOVE IDSV0102A-OPERAZIONE(IX-CACHE)
             TO V1391-OPERAZIONE(WK-IND-SERV).

           IF IDSV0102A-LIV-OPERAZIONE-NULL(IX-CACHE) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO V1391-LIVELLO-OPERAZIONE-NULL(WK-IND-SERV)
           ELSE
              MOVE IDSV0102A-LIV-OPERAZIONE(IX-CACHE)
                TO V1391-LIVELLO-OPERAZIONE(WK-IND-SERV)
           END-IF.

           IF IDSV0102A-TIPO-OGGETTO-NULL(IX-CACHE) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO V1391-TIPO-OGGETTO-NULL(WK-IND-SERV)
           ELSE
              MOVE IDSV0102A-TIPO-OGGETTO(IX-CACHE)
                TO V1391-TIPO-OGGETTO(WK-IND-SERV)
           END-IF.

           MOVE IDSV0102A-WHERE-CONDITION(IX-CACHE)
             TO V1391-WHERE-CONDITION(WK-IND-SERV).

           IF IDSV0102A-SERV-LETTURA-NULL(IX-CACHE) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO V1391-SERVIZIO-LETTURA-NULL(WK-IND-SERV)
           ELSE
              MOVE IDSV0102A-SERVIZIO-LETTURA(IX-CACHE)
                TO V1391-SERVIZIO-LETTURA(WK-IND-SERV)
           END-IF.

           IF IDSV0102A-MODULO-CALCOLO-NULL(IX-CACHE) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO V1391-MODULO-CALCOLO-NULL(WK-IND-SERV)
           ELSE
              MOVE IDSV0102A-MODULO-CALCOLO(IX-CACHE)
                TO V1391-MODULO-CALCOLO(WK-IND-SERV)
           END-IF.

           MOVE IDSV0102A-COD-DATO-INTERNO(IX-CACHE)
             TO V1391-COD-DATO-INTERNO(WK-IND-SERV).

       Z400-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CARICA CACHE X MVV
      *----------------------------------------------------------------*
       Z500-CARICA-CACHE.

           ADD 1              TO IDSV0102A-ELE-MAX-ACTU.

           IF V1391-TIPO-DATO-NULL(WK-IND-SERV) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO IDSV0102A-TIPO-DATO-NULL(IDSV0102A-ELE-MAX-ACTU)
           ELSE
              MOVE V1391-TIPO-DATO(WK-IND-SERV)
                TO IDSV0102A-TIPO-DATO(IDSV0102A-ELE-MAX-ACTU)
           END-IF.

           IF V1391-LUNGHEZZA-DATO-NULL(WK-IND-SERV) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO IDSV0102A-LUNGHEZZA-DATO-NULL(IDSV0102A-ELE-MAX-ACTU)
           ELSE
              MOVE V1391-LUNGHEZZA-DATO(WK-IND-SERV)
                TO IDSV0102A-LUNGHEZZA-DATO(IDSV0102A-ELE-MAX-ACTU)
           END-IF.

           IF V1391-PRECISIONE-DATO-NULL(WK-IND-SERV) = HIGH-VALUES
              MOVE HIGH-VALUE
               TO IDSV0102A-PRECISIONE-DATO-NULL(IDSV0102A-ELE-MAX-ACTU)
           ELSE
              MOVE V1391-PRECISIONE-DATO(WK-IND-SERV)
                TO IDSV0102A-PRECISIONE-DATO(IDSV0102A-ELE-MAX-ACTU)
           END-IF.

           MOVE V1391-FORMATTAZIONE-DATO(WK-IND-SERV)
             TO IDSV0102A-FORMATTAZIONE-DATO(IDSV0102A-ELE-MAX-ACTU).
      *
           MOVE V1391-ID-MATR-VAL-VAR(WK-IND-SERV)
             TO IDSV0102A-ID-MATR-VAL-VAR(IDSV0102A-ELE-MAX-ACTU).

           IF V1391-IDP-MATR-VAL-VAR-NULL(WK-IND-SERV) = HIGH-VALUES
              MOVE HIGH-VALUE
              TO IDSV0102A-IDP-MAT-VAL-VAR-NULL(IDSV0102A-ELE-MAX-ACTU)
           ELSE
              MOVE V1391-IDP-MATR-VAL-VAR(WK-IND-SERV)
                TO IDSV0102A-IDP-MATR-VAL-VAR(IDSV0102A-ELE-MAX-ACTU)
           END-IF.

           IF V1391-TIPO-MOVIMENTO-NULL(WK-IND-SERV) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO IDSV0102A-TIPO-MOVIMENTO-NULL(IDSV0102A-ELE-MAX-ACTU)
           ELSE
              MOVE V1391-TIPO-MOVIMENTO(WK-IND-SERV)
                TO IDSV0102A-TIPO-MOVIMENTO(IDSV0102A-ELE-MAX-ACTU)
           END-IF.

           MOVE V1391-COD-DATO-EXT(WK-IND-SERV)
             TO IDSV0102A-COD-DATO-EXT(IDSV0102A-ELE-MAX-ACTU)

           IF V1391-OBBLIGATORIETA-NULL(WK-IND-SERV) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO IDSV0102A-OBBLIGATORIETA-NULL(IDSV0102A-ELE-MAX-ACTU)
           ELSE
              MOVE V1391-OBBLIGATORIETA(WK-IND-SERV)
                TO IDSV0102A-OBBLIGATORIETA(IDSV0102A-ELE-MAX-ACTU)
           END-IF.

           MOVE V1391-VALORE-DEFAULT(WK-IND-SERV)
             TO IDSV0102A-VALORE-DEFAULT(IDSV0102A-ELE-MAX-ACTU)
           MOVE V1391-COD-STR-DATO-PTF(WK-IND-SERV)
             TO IDSV0102A-COD-STR-DATO-PTF(IDSV0102A-ELE-MAX-ACTU)
           MOVE V1391-COD-DATO-PTF(WK-IND-SERV)
             TO IDSV0102A-COD-DATO-PTF(IDSV0102A-ELE-MAX-ACTU)
           MOVE V1391-COD-PARAMETRO(WK-IND-SERV)
             TO IDSV0102A-COD-PARAMETRO(IDSV0102A-ELE-MAX-ACTU)
           MOVE V1391-OPERAZIONE(WK-IND-SERV)
             TO IDSV0102A-OPERAZIONE(IDSV0102A-ELE-MAX-ACTU)

           IF V1391-LIVELLO-OPERAZIONE-NULL(WK-IND-SERV) = HIGH-VALUES
              MOVE HIGH-VALUE TO
              IDSV0102A-LIV-OPERAZIONE-NULL(IDSV0102A-ELE-MAX-ACTU)
           ELSE
              MOVE V1391-LIVELLO-OPERAZIONE(WK-IND-SERV)
                TO IDSV0102A-LIV-OPERAZIONE(IDSV0102A-ELE-MAX-ACTU)
           END-IF.

           IF V1391-TIPO-OGGETTO-NULL(WK-IND-SERV) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO IDSV0102A-TIPO-OGGETTO-NULL(IDSV0102A-ELE-MAX-ACTU)
           ELSE
              MOVE V1391-TIPO-OGGETTO(WK-IND-SERV)
                TO IDSV0102A-TIPO-OGGETTO(IDSV0102A-ELE-MAX-ACTU)
           END-IF.

           MOVE V1391-WHERE-CONDITION(WK-IND-SERV)
             TO IDSV0102A-WHERE-CONDITION(IDSV0102A-ELE-MAX-ACTU)

           IF V1391-SERVIZIO-LETTURA-NULL(WK-IND-SERV) = HIGH-VALUES
              MOVE HIGH-VALUE
              TO IDSV0102A-SERV-LETTURA-NULL(IDSV0102A-ELE-MAX-ACTU)
           ELSE
              MOVE V1391-SERVIZIO-LETTURA(WK-IND-SERV)
                TO IDSV0102A-SERVIZIO-LETTURA(IDSV0102A-ELE-MAX-ACTU)
           END-IF.

           IF V1391-MODULO-CALCOLO-NULL(WK-IND-SERV) = HIGH-VALUES
              MOVE HIGH-VALUE
                TO IDSV0102A-MODULO-CALCOLO-NULL(IDSV0102A-ELE-MAX-ACTU)
           ELSE
              MOVE V1391-MODULO-CALCOLO(WK-IND-SERV)
                TO IDSV0102A-MODULO-CALCOLO(IDSV0102A-ELE-MAX-ACTU)
           END-IF.

           MOVE V1391-COD-DATO-INTERNO(WK-IND-SERV)
             TO IDSV0102A-COD-DATO-INTERNO(IDSV0102A-ELE-MAX-ACTU).

       Z500-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA MATR_VAL_VAR PER GESTIONE INPUT
      *----------------------------------------------------------------*
       T0000-TRATTA-MATRICE.
      *
           INITIALIZE V1391-AREA-ACTU.

           IF GESTIONE-PRODOTTO
              MOVE 'PO'                     TO WK-APPO-TP-OGG
              MOVE C216-COD-VARIABILE-P(IX-VAL-VAR, IX-COD-VAR)
                TO V1391-COD-ACTU
           ELSE
              MOVE 'GA'                     TO WK-APPO-TP-OGG
              MOVE C216-COD-VARIABILE-T(IX-VAL-VAR, IX-COD-VAR)
                TO V1391-COD-ACTU
           END-IF

           SET WK-MMV-SI                    TO TRUE.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
             TO V1391-COD-COMPAGNIA-ANIA.
      *
           MOVE 'LDBS1390'                  TO PGM-CHIAMATO.
      *
           CALL PGM-CHIAMATO  USING  DISPATCHER-VARIABLES
                                    V1391-AREA-ACTU
           ON EXCEPTION
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS1390 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF NOT IDSO0011-SUCCESSFUL-SQL
              MOVE PGM-CHIAMATO             TO IDSV0003-COD-SERVIZIO-BE

              MOVE IDSO0011-NOME-TABELLA
                TO IDSV0003-NOME-TABELLA

              MOVE IDSO0011-DESCRIZ-ERR-DB2
                TO IDSV0003-DESCRIZ-ERR-DB2

              IF IDSO0011-NOT-FOUND
                 SET WK-MMV-NO                        TO TRUE
                 MOVE 'N'   TO V1391-OBBLIGATORIETA(IX-COD-MVV)
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                 PERFORM S9004-GESTIONE-ERRORE        THRU S9004-EX
              ELSE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 STRING 'T0000-TRATTA-MATRICE - VARIABILE ;'
                           V1391-COD-ACTU ';'
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING

                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF

           END-IF.

      *
       T0000-EX.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE TIPO LEABORAZIONE COMPOSTA                            *
      *----------------------------------------------------------------*
       T0001-ELABORA-DEFAULT.
      *
             IF GESTIONE-PRODOTTO
                MOVE V1391-FORMATTAZIONE-DATO(IX-COD-MVV)(1:1)
                  TO C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR)
      *
                EVALUATE C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR)
                  WHEN 'A'
                  WHEN 'N'
                  WHEN 'I'
                    SET IDSV0141-MITT-NUM-SEGN  TO TRUE
                    MOVE V1391-LUNGHEZZA-DATO(IX-COD-MVV)
                      TO IDSV0141-LUNGHEZZA-DATO-MITT
                    MOVE V1391-LUNGHEZZA-DATO(IX-COD-MVV)
                      TO IDSV0141-LUNGHEZZA-DATO-STR
                    MOVE V1391-PRECISIONE-DATO(IX-COD-MVV)
                      TO IDSV0141-PRECISIONE-DATO-MITT
                    MOVE V1391-VALORE-DEFAULT(IX-COD-MVV)
                      TO IDSV0141-CAMPO-MITT
      *
                    PERFORM Y9999-CHIAMA-IDSS0140    THRU Y9999-EX
      *
                    IF IDSV0003-SUCCESSFUL-RC
                       ADD 1                        TO IX-COD-VAR-OUT
                       MOVE IDSV0141-CAMPO-DEST
                         TO C216-VAL-IMP-P(IX-VAL-VAR, IX-COD-VAR-OUT)
                       MOVE C216-COD-VARIABILE-P(IX-VAL-VAR, IX-COD-VAR)
                         TO C216-COD-VARIABILE-P
                            (IX-VAL-VAR, IX-COD-VAR-OUT)
                       MOVE C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR)
                         TO C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR-OUT)
                    ELSE
                       PERFORM S9004-GESTIONE-ERRORE THRU S9004-EX
                    END-IF
                  WHEN 'D'
                  WHEN 'S'
                    ADD 1                        TO IX-COD-VAR-OUT
                    MOVE V1391-VALORE-DEFAULT(IX-COD-MVV)
                      TO C216-VAL-STR-P(IX-VAL-VAR, IX-COD-VAR-OUT)
                    MOVE C216-COD-VARIABILE-P(IX-VAL-VAR, IX-COD-VAR)
                      TO C216-COD-VARIABILE-P
                         (IX-VAL-VAR, IX-COD-VAR-OUT)
                    MOVE C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR)
                      TO C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR-OUT)
                  WHEN 'P'
                  WHEN 'M'
11250 *         WHEN 'A'
                    SET IDSV0141-MITT-NUM-SEGN  TO TRUE
                    MOVE V1391-LUNGHEZZA-DATO(IX-COD-MVV)
                      TO IDSV0141-LUNGHEZZA-DATO-MITT
                    MOVE V1391-LUNGHEZZA-DATO(IX-COD-MVV)
                      TO IDSV0141-LUNGHEZZA-DATO-STR
                    MOVE V1391-PRECISIONE-DATO(IX-COD-MVV)
                      TO IDSV0141-PRECISIONE-DATO-MITT
                    MOVE V1391-VALORE-DEFAULT(IX-COD-MVV)
                      TO IDSV0141-CAMPO-MITT
      *
                    PERFORM Y9999-CHIAMA-IDSS0140    THRU Y9999-EX
      *
                    IF IDSV0003-SUCCESSFUL-RC
                       ADD 1                         TO IX-COD-VAR-OUT
                       MOVE IDSV0141-CAMPO-DEST
                         TO C216-VAL-PERC-P(IX-VAL-VAR, IX-COD-VAR-OUT)
                       MOVE C216-COD-VARIABILE-P(IX-VAL-VAR, IX-COD-VAR)
                         TO C216-COD-VARIABILE-P
                            (IX-VAL-VAR, IX-COD-VAR-OUT)
                       MOVE C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR)
                         TO C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR-OUT)
                    ELSE
                       PERFORM S9004-GESTIONE-ERRORE THRU S9004-EX
                    END-IF

                END-EVALUATE
             ELSE
                MOVE V1391-FORMATTAZIONE-DATO(IX-COD-MVV)(1:1)
                  TO C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR)
      *
                EVALUATE C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR)
                  WHEN 'A'
                  WHEN 'N'
                  WHEN 'I'
                    SET IDSV0141-MITT-NUM-SEGN  TO TRUE
                    MOVE V1391-LUNGHEZZA-DATO(IX-COD-MVV)
                      TO IDSV0141-LUNGHEZZA-DATO-MITT
                    MOVE V1391-LUNGHEZZA-DATO(IX-COD-MVV)
                      TO IDSV0141-LUNGHEZZA-DATO-STR
                    MOVE V1391-PRECISIONE-DATO(IX-COD-MVV)
                      TO IDSV0141-PRECISIONE-DATO-MITT
                    MOVE V1391-VALORE-DEFAULT(IX-COD-MVV)
                      TO IDSV0141-CAMPO-MITT
      *
                    PERFORM Y9999-CHIAMA-IDSS0140    THRU Y9999-EX
      *
                    IF IDSV0003-SUCCESSFUL-RC
                       ADD 1                        TO IX-COD-VAR-OUT
                       MOVE IDSV0141-CAMPO-DEST
                         TO C216-VAL-IMP-T(IX-VAL-VAR, IX-COD-VAR-OUT)
                       MOVE C216-COD-VARIABILE-T(IX-VAL-VAR, IX-COD-VAR)
                         TO C216-COD-VARIABILE-T
                            (IX-VAL-VAR, IX-COD-VAR-OUT)
                       MOVE C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR)
                         TO C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR-OUT)
                    ELSE
                       PERFORM S9004-GESTIONE-ERRORE THRU S9004-EX
                    END-IF
                  WHEN 'D'
                  WHEN 'S'
                    ADD 1                        TO IX-COD-VAR-OUT
                    MOVE V1391-VALORE-DEFAULT(IX-COD-MVV)
                      TO C216-VAL-STR-T(IX-VAL-VAR, IX-COD-VAR-OUT)
                    MOVE C216-COD-VARIABILE-T(IX-VAL-VAR, IX-COD-VAR)
                      TO C216-COD-VARIABILE-T
                         (IX-VAL-VAR, IX-COD-VAR-OUT)
                    MOVE C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR)
                      TO C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR-OUT)
                  WHEN 'P'
                  WHEN 'M'
11250 *         WHEN 'A'
                    SET IDSV0141-MITT-NUM-SEGN  TO TRUE
                    MOVE V1391-LUNGHEZZA-DATO(IX-COD-MVV)
                      TO IDSV0141-LUNGHEZZA-DATO-MITT
                    MOVE V1391-LUNGHEZZA-DATO(IX-COD-MVV)
                      TO IDSV0141-LUNGHEZZA-DATO-STR
                    MOVE V1391-PRECISIONE-DATO(IX-COD-MVV)
                      TO IDSV0141-PRECISIONE-DATO-MITT
                    MOVE V1391-VALORE-DEFAULT(IX-COD-MVV)
                      TO IDSV0141-CAMPO-MITT
      *
                    PERFORM Y9999-CHIAMA-IDSS0140    THRU Y9999-EX
      *
                    IF IDSV0003-SUCCESSFUL-RC
                       ADD 1                         TO IX-COD-VAR-OUT
                       MOVE IDSV0141-CAMPO-DEST
                         TO C216-VAL-PERC-T(IX-VAL-VAR, IX-COD-VAR-OUT)
                       MOVE C216-COD-VARIABILE-T(IX-VAL-VAR, IX-COD-VAR)
                         TO C216-COD-VARIABILE-T
                            (IX-VAL-VAR, IX-COD-VAR-OUT)
                       MOVE C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR)
                         TO C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR-OUT)
                    ELSE
                       PERFORM S9004-GESTIONE-ERRORE THRU S9004-EX
                    END-IF

                END-EVALUATE
             END-IF.
      *
       T0001-EX.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE TIPO ELABORAZIONE SEMPLICE                            *
      *----------------------------------------------------------------*
       T0002-ELABORA-DATO.
      *
           IF V1391-COD-DATO-INTERNO(IX-COD-MVV)    NOT = HIGH-VALUES
              PERFORM U0012-VALORIZZA-INT           THRU U0012-EX
              IF WK-INTERN-FIND-NO
                 IF V1391-COD-STR-DATO-PTF(IX-COD-MVV) = SPACES
                    OR LOW-VALUES OR HIGH-VALUES
                    SET IDSV0003-FIELD-NOT-VALUED   TO TRUE
                    PERFORM S9004-GESTIONE-ERRORE   THRU S9004-EX
                 ELSE
                    PERFORM T0002A-VALORIZZA-EXT    THRU T0002A-EX
                 END-IF
              END-IF
           ELSE
              PERFORM T0002A-VALORIZZA-EXT          THRU T0002A-EX
           END-IF.
      *
       T0002-EX.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE VARIABILE ESTERNA                                     *
      *----------------------------------------------------------------*
       T0002A-VALORIZZA-EXT.
      *
           IF IDSV0003-SUCCESSFUL-RC
              PERFORM U0008-VERIFICA-DATO     THRU U0008-EX
           END-IF.
      *
           IF IDSV0003-SUCCESSFUL-RC
              PERFORM U0010-VALORIZZA-DATO    THRU U0010-EX
           END-IF.
      *
       T0002A-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICA CONTROLLO LETTURA
      *----------------------------------------------------------------*
       U0000-VERIFICA-LETT.
      *
2404       IF SKEDA-OPZIONE-NO
              IF  GESTIONE-GARANZIE
              AND C216-TP-LIVELLO-T(IX-VAL-VAR) = 'G'
                 SET WK-ID-COD-NON-TROVATO            TO TRUE

                 IF WGRZ-ELE-GARANZIA-MAX = 0
                    PERFORM U0003-LEGGI-GRZ          THRU U0003-EX
                 END-IF
      *
                 IF WGRZ-ELE-GARANZIA-MAX = 1
                    MOVE 1                            TO IX-GUIDA-GRZ
                 ELSE
                    PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
                      UNTIL IX-TAB-GRZ > WGRZ-ELE-GARANZIA-MAX
                         OR WK-ID-COD-TROVATO
                         IF C216-ID-GAR-T(IX-VAL-VAR) =
                            WGRZ-ID-GAR(IX-TAB-GRZ)
                            SET WK-ID-COD-TROVATO     TO TRUE
                            MOVE IX-TAB-GRZ           TO IX-GUIDA-GRZ
                         END-IF
                    END-PERFORM
                 END-IF
              END-IF
2404       END-IF.
      *
       U0000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE RAN
      *----------------------------------------------------------------*
       U0001-LEGGI-RAN.
      *
           PERFORM U0007-IMPOSTA-GENERALE       THRU U0007-EX.
      *
           MOVE 1                                TO IX-APPO-OVERF.
           MOVE 0                                TO IX-TAB-RAN.
      *
           IF IX-GUIDA-GRZ = ZERO
              PERFORM S9005-ERRORE-LIV-VAR     THRU S9005-EX
           ELSE
              EVALUATE TRUE
                WHEN PRIMO-ASS
                  IF WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ)
                       TO RAN-ID-RAPP-ANA
                  END-IF

                WHEN SECONDO-ASS
                  IF WGRZ-ID-2O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-2O-ASSTO(IX-GUIDA-GRZ)
                       TO RAN-ID-RAPP-ANA
                  END-IF

                WHEN TERZO-ASS
                  IF WGRZ-ID-3O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-3O-ASSTO(IX-GUIDA-GRZ)
                       TO RAN-ID-RAPP-ANA
                  END-IF
                WHEN OTHER
                  IF WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ)
                       TO RAN-ID-RAPP-ANA
                  END-IF
              END-EVALUATE
      *
              MOVE  RAPP-ANA                  TO IDSI0011-BUFFER-DATI
      *
              PERFORM Z0000-LETTURA-GEN       THRU Z0000-EX
      *
              MOVE IX-TAB-RAN                 TO WRAN-ELE-RAPP-ANAG-MAX
      *
           END-IF.
      *
       U0001-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE RAN ID/TP OGG
      *----------------------------------------------------------------*
       U00A1-LEGGI-RAN2.
      *
           PERFORM U0007-IMPOSTA-GENERALE       THRU U0007-EX.
      *
           IF V1391-SERVIZIO-LETTURA(IX-COD-MVV) = 'LDBS1290'

              INITIALIZE LDBV1291

              MOVE RAN-ID-OGG                   TO LDBV1291-ID-OGG
              MOVE RAN-TP-OGG                   TO LDBV1291-TP-OGG

              MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(7:2)
                                                TO LDBV1291-TP-1
              MOVE LDBV1291
                TO IDSI0011-BUFFER-WHERE-COND

           END-IF.
      *
           MOVE 10                              TO IX-APPO-OVERF.
           MOVE 0                               TO IX-TAB-RAN.
      *
           MOVE  RAPP-ANA                  TO IDSI0011-BUFFER-DATI.
      *
           PERFORM Z0000-LETTURA-GEN       THRU Z0000-EX.
      *
           MOVE IX-TAB-RAN                 TO WRAN-ELE-RAPP-ANAG-MAX.
      *
       U00A1-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE DTC
      *----------------------------------------------------------------*
       U0002-LEGGI-DTC.
      *
           MOVE ZEROES                          TO IX-TAB-DTC.
           MOVE 10                              TO IX-APPO-OVERF.
           PERFORM U0007-IMPOSTA-GENERALE       THRU U0007-EX.
      *
           IF GESTIONE-GARANZIE
              MOVE C216-ID-LIVELLO-T(IX-VAL-VAR)
                                                TO DTC-ID-OGG
           END-IF
           MOVE 'TG'                            TO DTC-TP-OGG.
      *
           MOVE  DETT-TIT-CONT                  TO IDSI0011-BUFFER-DATI.
      *
           PERFORM Z0000-LETTURA-GEN            THRU Z0000-EX.
      *
           MOVE IX-TAB-DTC                    TO WDTC-ELE-DETT-TIT-MAX.
      *
       U0002-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE RAN
      *----------------------------------------------------------------*
       U0003-LEGGI-GRZ.
      *
           MOVE ZERO                  TO IDSI0011-DATA-INIZIO-EFFETTO
                                         IDSI0011-DATA-FINE-EFFETTO
                                         IDSI0011-DATA-COMPETENZA
                                         IDSI0011-DATA-COMP-AGG-STOR.
      *
           SET IDSI0011-ID                 TO TRUE.
           SET IDSI0011-SELECT             TO TRUE.
           MOVE IDSV0003-TRATTAMENTO-STORICITA
             TO IDSI0011-TRATTAMENTO-STORICITA.
      *
           MOVE 'GAR'                      TO IDSI0011-CODICE-STR-DATO
                                              WKS-NOME-TABELLA.
      *
           MOVE C216-ID-GAR-t(IX-VAL-VAR)
                                           TO GRZ-ID-GAR.
      *
           MOVE 10                         TO IX-APPO-OVERF.
      *
           MOVE  GAR                       TO IDSI0011-BUFFER-DATI.
      *
           PERFORM Z0000-LETTURA-GEN       THRU Z0000-EX.
      *
           MOVE IX-TAB-GRZ                 TO WGRZ-ELE-GARANZIA-MAX.
      *
       U0003-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE RAN
      *----------------------------------------------------------------*
       U0004-LEGGI-TGA.
      *
           INITIALIZE LDBV0011.

           PERFORM U0007-IMPOSTA-GENERALE       THRU U0007-EX.
      *
           MOVE 0                               TO IX-TAB-TGA.
      *
           IF IDSI0011-LIVELLO-OPERAZIONE = 'WC'
              IF GESTIONE-GARANZIE
                 MOVE C216-ID-GAR-T(IX-VAL-VAR)  TO LDBV0011-ID-GAR
              END-IF
              MOVE LDBV0011                TO IDSI0011-BUFFER-WHERE-COND
              MOVE SPACES                  TO IDSI0011-BUFFER-DATI
           ELSE
              MOVE C216-ID-LIVELLO-T(IX-VAL-VAR)  TO TGA-ID-TRCH-DI-GAR
              MOVE TRCH-DI-GAR                  TO IDSI0011-BUFFER-DATI
           END-IF.
      *
           MOVE  0                              TO IX-TAB-TGA.
           MOVE 10                              TO IX-APPO-OVERF.
      *
           PERFORM Z0000-LETTURA-GEN            THRU Z0000-EX.
      *
           MOVE IX-TAB-TGA                      TO WTGA-ELE-TRAN-MAX.
      *
       U0004-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE RAN
      *----------------------------------------------------------------*
       U0005-LEGGI-RRE.
      *
           PERFORM U0007-IMPOSTA-GENERALE     THRU U0007-EX.
      *
           MOVE WPOL-ID-POLI                  TO RRE-ID-OGG.
           MOVE 'PO'                          TO RRE-TP-OGG.
      *
           MOVE  RAPP-RETE                    TO IDSI0011-BUFFER-DATI.
      *
           MOVE ZEROES                        TO IX-TAB-RRE.
           MOVE 2                             TO IX-APPO-OVERF.
      *
           PERFORM Z0000-LETTURA-GEN          THRU Z0000-EX.
      *
           MOVE IX-TAB-RRE                    TO WRRE-ELE-RAPP-RETE-MAX.

       U0005-EX.
           EXIT.
      *----------------------------------------------------------------*
      * LETTURA RRE ADOC
      *----------------------------------------------------------------*
       U00A5-LEGGI-RRE.
      *
           MOVE ZERO                  TO IDSI0011-DATA-INIZIO-EFFETTO
                                         IDSI0011-DATA-FINE-EFFETTO
                                         IDSI0011-DATA-COMPETENZA
                                         IDSI0011-DATA-COMP-AGG-STOR.
      *
           SET IDSO0011-SUCCESSFUL-SQL TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC  TO TRUE.
           SET IDSI0011-FETCH-FIRST    TO TRUE.
           SET IDSI0011-ID-OGGETTO     TO TRUE.
           MOVE 'RAPP-RETE'            TO IDSI0011-CODICE-STR-DATO
                                          WKS-NOME-TABELLA.
      *
           SET IDSI0011-TRATT-X-COMPETENZA TO TRUE.
      *
           MOVE WPOL-ID-POLI                  TO RRE-ID-OGG.
           MOVE 'PO'                          TO RRE-TP-OGG.
      *
           MOVE  RAPP-RETE                    TO IDSI0011-BUFFER-DATI.
      *
           MOVE ZEROES                        TO IX-TAB-RRE.
           MOVE 2                             TO IX-APPO-OVERF.
      *
           PERFORM Z0000-LETTURA-GEN          THRU Z0000-EX.
      *
           MOVE IX-TAB-RRE                    TO WRRE-ELE-RAPP-RETE-MAX.
       U00A5-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE RAN
      *----------------------------------------------------------------*
       U0006-LEGGI-SPG.
      *
           PERFORM U0007-IMPOSTA-GENERALE       THRU U0007-EX.
      *
           IF GESTIONE-GARANZIE
              MOVE C216-ID-GAR-T(IX-VAL-VAR) TO SPG-ID-GAR
           END-IF

           IF IDSI0011-WHERE-CONDITION
           AND V1391-WHERE-CONDITION(IX-COD-MVV) NOT = SPACES
              MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(1:2)
                                                TO SPG-COD-SOPR
           END-IF.

MC         MOVE 10                              TO IX-APPO-OVERF .
      *
           MOVE  SOPR-DI-GAR                    TO IDSI0011-BUFFER-DATI.
      *
           MOVE WSPG-ELE-SOPRAP-GAR-MAX         TO IX-TAB-SPG.
      *    MOVE 0                               TO IX-TAB-SPG.
MC    *
           PERFORM Z0000-LETTURA-GEN            THRU Z0000-EX.
      *
MC         MOVE IX-TAB-SPG                   TO WSPG-ELE-SOPRAP-GAR-MAX.
      *
       U0006-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE ISO
      *----------------------------------------------------------------*
       U0015-LEGGI-ISO.
      *
           PERFORM U0007-IMPOSTA-GENERALE       THRU U0007-EX.
      *
           MOVE WADE-ID-ADES(1)                 TO ISO-ID-OGG.
           MOVE 'AD'                            TO ISO-TP-OGG.
      *
MC         MOVE 10                              TO IX-APPO-OVERF.
      *
           MOVE  IMPST-SOST                     TO IDSI0011-BUFFER-DATI.
      *
           MOVE 0                               TO IX-TAB-ISO.
MC    *
           PERFORM Z0000-LETTURA-GEN            THRU Z0000-EX.
      *
MC         MOVE IX-TAB-ISO                     TO WISO-ELE-IMP-SOST-MAX.
      *
       U0015-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   IMPOSTAZIONI GENERALI PER LETTURA.
      *----------------------------------------------------------------*
       U0013-LEGGI-ADE.
      *
           PERFORM U0007-IMPOSTA-GENERALE     THRU U0007-EX.
      *
           MOVE ZEROES                        TO ADE-ID-ADES.
           IF WGRZ-ELE-GARANZIA-MAX = 1
              MOVE WGRZ-ID-ADES(1)            TO ADE-ID-ADES
           ELSE
              IF GESTIONE-PRODOTTO
                 PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
                   UNTIL IX-TAB-GRZ > WGRZ-ELE-GARANZIA-MAX
                   IF C216-ID-POL-P(IX-VAL-VAR) =
                      WGRZ-ID-GAR(IX-TAB-GRZ)
                      MOVE WGRZ-ID-ADES(IX-TAB-GRZ) TO ADE-ID-ADES
                   END-IF
                 END-PERFORM
              ELSE
                 PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
                   UNTIL IX-TAB-GRZ > WGRZ-ELE-GARANZIA-MAX
                   IF C216-ID-GAR-T(IX-VAL-VAR) =
                      WGRZ-ID-GAR(IX-TAB-GRZ)
                      MOVE WGRZ-ID-ADES(IX-TAB-GRZ) TO ADE-ID-ADES
                   END-IF
                 END-PERFORM
              END-IF
           END-IF.
      *
           IF ADE-ID-ADES GREATER 0
              MOVE  ADES                      TO IDSI0011-BUFFER-DATI
      *
              MOVE ZEROES                        TO IX-TAB-ADE
              MOVE 1                             TO IX-APPO-OVERF
      *
              PERFORM Z0000-LETTURA-GEN          THRU Z0000-EX
      *
              MOVE IX-TAB-ADE                    TO WADE-ELE-ADES-MAX
           ELSE
              IF NOT IDSV0003-INVALID-CONVERSION
                 SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
              END-IF
              PERFORM S9004-GESTIONE-ERRORE      THRU S9004-EX
           END-IF.
      *
       U0013-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURA PARAMETRO MOVI
      *----------------------------------------------------------------*
       U0014-LEGGI-PMO.
      *
           PERFORM U0007-IMPOSTA-GENERALE       THRU U0007-EX.
      *
           IF GESTIONE-PRODOTTO
              MOVE WPOL-ID-POLI              TO PMO-ID-OGG
              MOVE 'PO'                      TO PMO-TP-OGG
           ELSE
              MOVE WGRZ-ID-GAR(IX-GUIDA-GRZ) TO PMO-ID-OGG
              MOVE 'GA'                      TO PMO-TP-OGG
           END-IF.
      *
           MOVE  PARAM-MOVI                  TO IDSI0011-BUFFER-DATI.
      *
           MOVE 50                           TO IX-APPO-OVERF.
29107 *    MOVE 0                            TO IX-TAB-PMO.
29107      MOVE WPMO-ELE-PARAM-MOV-MAX       TO IX-TAB-PMO.
MC    *
           MOVE LDBV1471
             TO IDSI0011-BUFFER-WHERE-COND.
      *
           PERFORM Z0000-LETTURA-GEN            THRU Z0000-EX.
      *
MC         MOVE IX-TAB-PMO                   TO WPMO-ELE-PARAM-MOV-MAX.
       U0014-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURA TIT CONT
      *----------------------------------------------------------------*
       U0016-LEGGI-TIT.
      *
           PERFORM U0007-IMPOSTA-GENERALE       THRU U0007-EX.
      *
           IF WPOL-TP-LIV-GENZ-TIT = 'PO'
              MOVE WPOL-ID-POLI              TO TIT-ID-OGG
              MOVE 'PO'                      TO TIT-TP-OGG
           ELSE
              MOVE WADE-ID-ADES(1)           TO TIT-ID-OGG
              MOVE 'AD'                      TO TIT-TP-OGG
           END-IF.
      *
           MOVE  TIT-CONT                    TO IDSI0011-BUFFER-DATI.
      *
           MOVE 10                           TO IX-APPO-OVERF.
           MOVE 0                            TO IX-TAB-TIT.
      *
           PERFORM Z0000-LETTURA-GEN         THRU Z0000-EX.
      *
MC         MOVE IX-TAB-TIT                   TO WTIT-ELE-TIT-CONT-MAX.
      *
       U0016-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE ISO
      *----------------------------------------------------------------*
       U0017-LEGGI-DFA.
      *
           PERFORM U0007-IMPOSTA-GENERALE       THRU U0007-EX.
      *
           MOVE WADE-ID-ADES(1)                 TO DFA-ID-ADES.
      *
MC         MOVE 1                               TO IX-APPO-OVERF.
      *
           MOVE  D-FISC-ADES                    TO IDSI0011-BUFFER-DATI.
      *
           MOVE 0                               TO IX-TAB-DFA.
MC    *
           PERFORM Z0000-LETTURA-GEN            THRU Z0000-EX.
      *
MC         MOVE IX-TAB-DFA                    TO WDFA-ELE-FISC-ADES-MAX.
      *
       U0017-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   LETTURA DETTAGLIO QUESTIONARIO DA DB
      *----------------------------------------------------------------*
       U0018-LEGGI-DEQ-DB-CO.
      *
           INITIALIZE LDBV2171.
           PERFORM U0007-IMPOSTA-GENERALE        THRU U0007-EX.
           MOVE 1                                TO IX-APPO-OVERF.
           MOVE 0                                TO IX-TAB-DEQ.
           SET WK-LETTO-NO                       TO TRUE.
      *
           MOVE ZEROES                           TO WK-ID-ASSIC.
           MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(4:5)
                                                 TO WK-APP-ASSICURATO.
           MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(10:20)
                                                 TO WK-COD-DOM.
      *
           IF IX-GUIDA-GRZ = ZERO
              PERFORM S9005-ERRORE-LIV-VAR     THRU S9005-EX
           ELSE
              EVALUATE TRUE
                WHEN PRIMO-ASS
                  IF WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ)
                       TO LDBV2171-ID-RAPP-ANA
                  END-IF

                WHEN SECONDO-ASS
                  IF WGRZ-ID-2O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-2O-ASSTO(IX-GUIDA-GRZ)
                       TO LDBV2171-ID-RAPP-ANA
                  END-IF

                WHEN TERZO-ASS
                  IF WGRZ-ID-3O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-3O-ASSTO(IX-GUIDA-GRZ)
                       TO LDBV2171-ID-RAPP-ANA
                  END-IF
              END-EVALUATE
      *
              MOVE WK-COD-DOM                TO LDBV2171-COD-DOM
              MOVE WGRZ-ID-GAR(IX-GUIDA-GRZ) TO LDBV2171-ID-GAR
      *
              MOVE LDBV2171               TO IDSI0011-BUFFER-WHERE-COND
              MOVE DETT-QUEST                TO IDSI0011-BUFFER-DATI
      *
              PERFORM Z0000-LETTURA-GEN      THRU Z0000-EX
      *
              MOVE IX-TAB-DEQ                TO WDEQ-ELE-DETT-QUEST-MAX
           END-IF.
      *
       U0018-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   LETTURA DETTAGLIO QUESTIONARIO DA DB INDIVUDUALI
      *----------------------------------------------------------------*
       U0026-LEGGI-DEQ-DB-IN.
      *
           INITIALIZE COMP-QUEST
                      LDBV5001
                      DOMANDA-COLLG.
           PERFORM U0007-IMPOSTA-GENERALE        THRU U0007-EX.
           MOVE 1                                TO IX-APPO-OVERF.
           MOVE 0                                TO IX-TAB-DEQ.
           SET WK-LETTO-NO                       TO TRUE.
      *
      *     PERFORM U0028-LEGGI-RRE-Q04           THRU U0028-EX.
      *
           IF WK-COD-CAN IS NUMERIC
              IF WK-COD-CAN GREATER ZERO
                 MOVE WK-COD-CAN       TO Q04-COD-CAN
              ELSE
                 MOVE HIGH-VALUE       TO Q04-COD-CAN-NULL
              END-IF
           ELSE
              MOVE HIGH-VALUE          TO Q04-COD-CAN-NULL
           END-IF.

           PERFORM U028A-GEST-COMP-QUEST      THRU U028A-EX.
      **
           PERFORM U9999-CLOSE-ALL            THRU U9999-EX.
      *
       U0026-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   LETTURA DETTAGLIO QUESTIONARIO DA CONTESTO
      *----------------------------------------------------------------*
       U0019-LEGGI-DEQ-CONT-CO.
      *
           MOVE ZEROES                           TO WK-ID-ASSIC
                                                    IX-APP-DEQ.
           MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(4:5)
                                                 TO WK-APP-ASSICURATO.
           MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(10:20)
                                                 TO WK-COD-DOM.
           SET WK-LETTO-NO                       TO TRUE.
      *
           IF IX-GUIDA-GRZ = ZERO
              CONTINUE
           ELSE
              EVALUATE TRUE
                WHEN PRIMO-ASS
                  IF WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ)
                       TO WK-ID-ASSIC
                  END-IF

                WHEN SECONDO-ASS
                  IF WGRZ-ID-2O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-2O-ASSTO(IX-GUIDA-GRZ)
                       TO WK-ID-ASSIC
                  END-IF

                WHEN TERZO-ASS
                  IF WGRZ-ID-3O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-3O-ASSTO(IX-GUIDA-GRZ)
                       TO WK-ID-ASSIC
                  END-IF

              END-EVALUATE
      *
              IF WK-ID-ASSIC IS NUMERIC
              AND WK-ID-ASSIC GREATER ZEROES
      *
                PERFORM VARYING IX-TAB-QUE FROM 1 BY 1
                   UNTIL IX-TAB-QUE > WQUE-ELE-QUEST-MAX
                      OR WK-LETTO-SI
                   IF WQUE-ID-RAPP-ANA(IX-TAB-QUE) =  WK-ID-ASSIC
                      PERFORM VARYING IX-TAB-DEQ FROM 1 BY 1
                        UNTIL IX-TAB-DEQ > WDEQ-ELE-DETT-QUEST-MAX
                           OR WK-LETTO-SI
                        IF WDEQ-ID-QUEST(IX-TAB-DEQ) =
                           WQUE-ID-QUEST(IX-TAB-QUE)
                        AND WDEQ-COD-DOM(IX-TAB-DEQ) =
                           WK-COD-DOM
                           MOVE IX-TAB-DEQ            TO IX-APP-DEQ
                           SET WK-LETTO-SI            TO TRUE
                        END-IF
                      END-PERFORM
                   END-IF
                END-PERFORM
      *
              END-IF
           END-IF.
      *
       U0019-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   LETTURA DETTAGLIO QUESTIONARIO DA DB INDIVUDUALI
      *----------------------------------------------------------------*
       U0027-LEGGI-DEQ-CONT-IN.
      *
           INITIALIZE COMP-QUEST
                      DOMANDA-COLLG.
           MOVE ZEROES                           TO WK-ID-ASSIC.
           SET WK-LETTO-NO                       TO TRUE.
           SET WK-FIGLIA-NO                      TO TRUE.
      *
           IF IX-GUIDA-GRZ = ZERO
              CONTINUE
           ELSE
              IF WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                 MOVE WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ)
                   TO WK-ID-ASSIC
              END-IF
           END-IF.
      *
           IF WK-ID-ASSIC IS NUMERIC
           AND WK-ID-ASSIC GREATER ZEROES
      *
      *        PERFORM U0028-LEGGI-RRE-Q04           THRU U0028-EX
      *
              IF WK-COD-CAN IS NUMERIC
                 IF WK-COD-CAN GREATER ZERO
                    MOVE WK-COD-CAN       TO Q04-COD-CAN
                 ELSE
                    MOVE HIGH-VALUE       TO Q04-COD-CAN-NULL
                 END-IF
              ELSE
                 MOVE HIGH-VALUE          TO Q04-COD-CAN-NULL
              END-IF

              SET WK-LETTO-NO                  TO TRUE
              MOVE ZEROES                      TO IX-TAB-DEQ

              PERFORM U028A-GEST-COMP-QUEST
                 THRU U028A-EX

           END-IF.
      *
           PERFORM U9999-CLOSE-ALL            THRU U9999-EX.
      *
       U0027-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   LETTURA COMP QUEST
      *----------------------------------------------------------------*
       U0028-LEGGI-RRE-Q04.
      *
           IF WRRE-ELE-RAPP-RETE-MAX = 0
              PERFORM U00A5-LEGGI-RRE        THRU U00A5-EX
           END-IF.
      *
           PERFORM VARYING IX-TAB-RRE FROM 1 BY 1
             UNTIL IX-TAB-RRE > WRRE-ELE-RAPP-RETE-MAX
              IF WRRE-TP-RETE(IX-TAB-RRE) = 'GE'
                 MOVE WRRE-COD-CAN(IX-TAB-RRE)
                                           TO WK-COD-CAN
              END-IF
           END-PERFORM.
      *
       U0028-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   GESTIONE COMP-QUEST PER TUTTR LE DOMANDE
      *----------------------------------------------------------------*
       U028A-GEST-COMP-QUEST.
      *
           SET IDSV0003-SUCCESSFUL-SQL          TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC           TO TRUE.
           MOVE IDSV0003-TRATTAMENTO-STORICITA  TO WKS-APPO-STOR.
      *
           SET IDSV0003-TRATT-SENZA-STOR        TO TRUE.
           SET IDSV0003-FETCH-FIRST             TO TRUE.
           SET IDSV0003-WHERE-CONDITION         TO TRUE.

      *- ADEDOS e' un codice questionario fittizio utilizzato per unipol
      *- per gestire sia il questionario per persona fisica che giuridica
           IF IVVC0221-COD-QUEST = 'ADEDOS'
              MOVE 'ADEFIS'                     TO Q04-COD-QUEST
           ELSE
              MOVE IVVC0221-COD-QUEST           TO Q04-COD-QUEST
           END-IF.
           MOVE IVVC0221-COD-DOMANDA(IX-DOM)    TO Q04-COD-DOMANDA.
      *
           MOVE 'LDBS4990'                      TO PGM-CHIAMATO.
           CALL PGM-CHIAMATO                    USING IDSV0003
                                                      COMP-QUEST
           ON EXCEPTION
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-LDBS1390 ERRORE CHIAMATA - LDBS4990'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           IF IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
              PERFORM U028B-VERIFICA-RISP       THRU U028B-EX
              IF WK-DEROGA-NO
                 PERFORM U0030-DOMANDA-COLL     THRU U0030-EX
              ELSE
                 MOVE 'LDBS4990'                TO PGM-CHIAMATO
                 PERFORM U0029-CLOSE-CURSOR     THRU U0029-EX
              END-IF
           ELSE
              IF IDSV0003-NOT-FOUND
              AND IVVC0221-COD-QUEST = 'ADEDOS'
                 PERFORM U028A1-COMP-QUEST-DOS  THRU U028A1-EX
              ELSE
                 MOVE 'LDBS4990'                   TO PGM-CHIAMATO
                 PERFORM U0029-CLOSE-CURSOR        THRU U0029-EX
                 SET IDSV0003-FIELD-NOT-VALUED     TO TRUE
           END-IF.

           IF WK-DEROGA-NO
           AND IVVC0221-COD-QUEST = 'ADEDOS'
              PERFORM U028A1-COMP-QUEST-DOS        THRU U028A1-EX
           END-IF.
      *
           MOVE WKS-APPO-STOR
             TO IDSV0003-TRATTAMENTO-STORICITA.
      *
       U028A-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   VERIFICA ESISTENZA QUESTIONARIO GIURIDICO
      *----------------------------------------------------------------*
       U028A1-COMP-QUEST-DOS.
      *
           SET IDSV0003-SUCCESSFUL-SQL          TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC           TO TRUE.
           SET IDSV0003-TRATT-SENZA-STOR        TO TRUE.
           SET IDSV0003-FETCH-FIRST             TO TRUE.
           SET IDSV0003-WHERE-CONDITION         TO TRUE.
      *
           MOVE 'ADEGIU'                        TO Q04-COD-QUEST.
           MOVE IVVC0221-COD-DOMANDA(IX-DOM)    TO Q04-COD-DOMANDA.
      *
           MOVE 'LDBS4990'                      TO PGM-CHIAMATO.
           CALL PGM-CHIAMATO                    USING IDSV0003
                                                      COMP-QUEST
           ON EXCEPTION
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-LDBS4990 ERRORE CHIAMATA - LDBS4990'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           IF IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
              PERFORM U028B-VERIFICA-RISP       THRU U028B-EX
              IF WK-DEROGA-NO
                 PERFORM U0030-DOMANDA-COLL     THRU U0030-EX
              ELSE
                 MOVE 'LDBS4990'                TO PGM-CHIAMATO
                 PERFORM U0029-CLOSE-CURSOR     THRU U0029-EX
              END-IF
           ELSE
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED     TO TRUE
              ELSE
                 MOVE 'LDBS4990'                   TO PGM-CHIAMATO
                 PERFORM U0029-CLOSE-CURSOR        THRU U0029-EX
                 SET IDSV0003-FIELD-NOT-VALUED     TO TRUE
              END-IF
           END-IF.
      *
       U028A1-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   VERIFICA RISPOSTA A CONTESTO
      *----------------------------------------------------------------*
       U028B-VERIFICA-RISP.
      *
           SET WK-LETTO-NO                        TO TRUE.
      *
           IF WK-CONTESTO-NO
              PERFORM U028C-LEGGI-QUEST           THRU U028C-EX
           END-IF.
      *
           PERFORM VARYING IX-TAB-QUE FROM 1 BY 1
             UNTIL IX-TAB-QUE > WQUE-ELE-QUEST-MAX
                OR WK-LETTO-SI

                PERFORM VARYING IX-TAB-DEQ FROM 1 BY 1
                  UNTIL IX-TAB-DEQ > WDEQ-ELE-DETT-QUEST-MAX
                     OR WK-LETTO-SI

                     IF WDEQ-ID-QUEST(IX-TAB-DEQ) =
                        WQUE-ID-QUEST(IX-TAB-QUE)
                     AND WDEQ-ID-COMP-QUEST(IX-TAB-DEQ) =
                         Q04-ID-COMP-QUEST
                        MOVE IX-TAB-DEQ            TO IX-APP-DEQ
                        SET WK-LETTO-SI            TO TRUE
                     END-IF

                END-PERFORM
      *
           END-PERFORM.
      *
           IF WK-LETTO-SI
              PERFORM S1031-GEST-VARIABILI-QUE     THRU S1031-EX
           END-IF.
      *
       U028B-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   VERIFICA RISPOSTA DELLA DOMANDA COLLEGATA
      *----------------------------------------------------------------*
       U028D-VERIFICA-DOM-COLL.
      *
           SET WK-FIGLIA-NO                         TO TRUE.
      *
           IF Q05-ID-COMP-QUEST IS NUMERIC
             IF Q05-ID-COMP-QUEST GREATER ZEROES

              PERFORM VARYING IX-TAB-QUE FROM 1 BY 1
                UNTIL IX-TAB-QUE > WQUE-ELE-QUEST-MAX
                   OR WK-FIGLIA-SI

                   PERFORM VARYING IX-TAB-DEQ FROM 1 BY 1
                     UNTIL IX-TAB-DEQ > WDEQ-ELE-DETT-QUEST-MAX
                        OR WK-FIGLIA-SI

                        IF WDEQ-ID-QUEST(IX-TAB-DEQ) =
                           WQUE-ID-QUEST(IX-TAB-QUE)
                        AND WDEQ-ID-COMP-QUEST(IX-TAB-DEQ) =
                            Q05-ID-COMP-QUEST
                           MOVE IX-TAB-DEQ            TO IX-APP-DEQ
                           SET WK-FIGLIA-SI           TO TRUE
                        END-IF

                   END-PERFORM

              END-PERFORM
            END-IF
           END-IF.
      *
           IF WK-FIGLIA-SI
              PERFORM S1031-GEST-VARIABILI-QUE        THRU S1031-EX
           END-IF.
      *
       U028D-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   GESTIONE DOMANDA COLLEGATA
      *----------------------------------------------------------------*
       U028C-LEGGI-QUEST.
      *
           IF IX-GUIDA-GRZ = ZERO
              PERFORM S9005-ERRORE-LIV-VAR     THRU S9005-EX
           ELSE
              MOVE ZERO               TO IX-TAB-DEQ
              SET IDSO0011-SUCCESSFUL-RC       TO TRUE
              SET IDSO0011-SUCCESSFUL-SQL      TO TRUE
      *
              MOVE Q04-ID-COMP-QUEST
                TO LDBV5001-ID-COMP-QUEST

              IF WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                 MOVE WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ)
                   TO LDBV5001-ID-RAPP-ANA
              END-IF
              SET IDSI0011-WHERE-CONDITION
                                      TO TRUE
              SET IDSI0011-SELECT     TO TRUE
              MOVE LDBV5001           TO IDSI0011-BUFFER-WHERE-COND
              MOVE DETT-QUEST         TO IDSI0011-BUFFER-DATI
              MOVE 'LDBS5000'         TO IDSI0011-CODICE-STR-DATO
      *
              PERFORM Z0000-LETTURA-GEN   THRU Z0000-EX
      *
              IF IDSO0011-SUCCESSFUL-RC
              AND IDSO0011-SUCCESSFUL-SQL
                 MOVE IX-TAB-DEQ      TO WDEQ-ELE-DETT-QUEST-MAX
                                         WQUE-ELE-QUEST-MAX
                 MOVE WDEQ-ID-QUEST(IX-TAB-DEQ)
                   TO WQUE-ID-QUEST(IX-TAB-DEQ)
      *
                 IF Q05-ID-COMP-QUEST GREATER ZEROES
                    MOVE Q05-ID-COMP-QUEST
                      TO LDBV5001-ID-COMP-QUEST
                    MOVE LDBV5001      TO IDSI0011-BUFFER-WHERE-COND
                    MOVE DETT-QUEST    TO IDSI0011-BUFFER-DATI
      *
                    PERFORM Z0000-LETTURA-GEN   THRU Z0000-EX
      *
                    IF IDSO0011-SUCCESSFUL-RC
                    AND IDSO0011-SUCCESSFUL-SQL
                       MOVE IX-TAB-DEQ      TO WDEQ-ELE-DETT-QUEST-MAX
                                               WQUE-ELE-QUEST-MAX
                       MOVE WDEQ-ID-QUEST(IX-TAB-DEQ)
                         TO WQUE-ID-QUEST(IX-TAB-DEQ)
                    END-IF
                 END-IF
              END-IF
           END-IF.
      *
       U028C-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   GESTIONE DOMANDA COLLEGATA
      *----------------------------------------------------------------*
       U0030-DOMANDA-COLL.
      *
           INITIALIZE DOMANDA-COLLG.
           SET IDSV0003-FETCH-FIRST      TO TRUE.

           MOVE Q04-ID-COMP-QUEST        TO Q05-ID-COMP-QUEST-PAD

           MOVE 'LDBS5020'                      TO PGM-CHIAMATO.
           CALL PGM-CHIAMATO                    USING IDSV0003
                                                      DOMANDA-COLLG
           ON EXCEPTION
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-LDBS5020 ERRORE CHIAMATA - LDBS5020'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
      *
              PERFORM U028D-VERIFICA-DOM-COLL   THRU U028D-EX
              IF WK-DEROGA-NO
                 PERFORM U0032A-GESIONE-DOMCOLL THRU U0032A-EX
                 IF WK-DEROGA-NO
                    PERFORM U0031-GESIONE-QUEST
                       THRU U0031-EX
                 END-IF
              ELSE
                 MOVE 'LDBS5020'                TO PGM-CHIAMATO
                 PERFORM U0029-CLOSE-CURSOR     THRU U0029-EX
                 MOVE 'LDBS4990'                TO PGM-CHIAMATO
                 PERFORM U0029-CLOSE-CURSOR     THRU U0029-EX
              END-IF
      *
           ELSE
              IF IDSV0003-NOT-FOUND
                 IF WK-DEROGA-NO
                    PERFORM U0031-GESIONE-QUEST
                       THRU U0031-EX
                 END-IF
              ELSE
                 MOVE 'LDBS4990'                TO PGM-CHIAMATO
                 PERFORM U0029-CLOSE-CURSOR     THRU U0029-EX
                 SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
              END-IF
           END-IF.
      *
       U0030-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CLOSE CURSOR SU COMP_QUEST
      *----------------------------------------------------------------*
       U0031-GESIONE-QUEST.
      *
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
      *
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR NOT IDSV0003-SUCCESSFUL-SQL
                      OR WK-DEROGA-SI
               SET IDSV0003-FETCH-NEXT       TO TRUE

               MOVE 'LDBS4990'               TO PGM-CHIAMATO
               CALL PGM-CHIAMATO                USING IDSV0003
                                                      COMP-QUEST
               ON EXCEPTION
                  MOVE WK-PGM
                    TO IDSV0003-COD-SERVIZIO-BE
                  MOVE 'CALL-LDBS4990 ERRORE CHIAMATA - LDBS4990'
                    TO IDSV0003-DESCRIZ-ERR-DB2
                    SET IDSV0003-INVALID-OPER  TO TRUE
               END-CALL

               EVALUATE TRUE
                  WHEN IDSV0003-NOT-FOUND
                     CONTINUE
                  WHEN IDSV0003-SUCCESSFUL-SQL
                    PERFORM U028B-VERIFICA-RISP       THRU U028B-EX
                    IF WK-DEROGA-NO
                       PERFORM U0032-LEGGI-DOMANDA    THRU U0032-EX
                    END-IF
                  WHEN OTHER
                     SET IDSV0003-FIELD-NOT-VALUED   TO TRUE
               END-EVALUATE

           END-PERFORM.
      *
           IF IDSV0003-FIELD-NOT-VALUED
              PERFORM S9004-GESTIONE-ERRORE          THRU S9004-EX
           END-IF.
      *
       U0031-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CLOSE CURSOR SU COMP_QUEST
      *----------------------------------------------------------------*
       U0032A-GESIONE-DOMCOLL.
      *
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
      *
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR NOT IDSV0003-SUCCESSFUL-SQL
                      OR WK-DEROGA-SI
               SET IDSV0003-FETCH-NEXT       TO TRUE

               MOVE 'LDBS5020'               TO PGM-CHIAMATO
               CALL PGM-CHIAMATO                USING IDSV0003
                                                      DOMANDA-COLLG
               ON EXCEPTION
                  MOVE WK-PGM
                    TO IDSV0003-COD-SERVIZIO-BE
                  MOVE 'CALL-LDBS5020 ERRORE CHIAMATA - LDBS5020'
                    TO IDSV0003-DESCRIZ-ERR-DB2
                    SET IDSV0003-INVALID-OPER  TO TRUE
               END-CALL

               EVALUATE TRUE
                  WHEN IDSV0003-NOT-FOUND
                     CONTINUE
                  WHEN IDSV0003-SUCCESSFUL-SQL
                     PERFORM U028D-VERIFICA-DOM-COLL THRU U028D-EX
                  WHEN OTHER
                     SET IDSV0003-FIELD-NOT-VALUED   TO TRUE
               END-EVALUATE

           END-PERFORM.
      *
           IF WK-DEROGA-SI
              IF IDSV0003-NOT-FOUND
                 MOVE 'LDBS4990'                TO PGM-CHIAMATO
                 PERFORM U0029-CLOSE-CURSOR     THRU U0029-EX
              ELSE
                 MOVE 'LDBS5020'                TO PGM-CHIAMATO
                 PERFORM U0029-CLOSE-CURSOR     THRU U0029-EX
                 MOVE 'LDBS4990'                TO PGM-CHIAMATO
                 PERFORM U0029-CLOSE-CURSOR     THRU U0029-EX
              END-IF
           ELSE
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-SUCCESSFUL-SQL    TO TRUE
      *           MOVE 'LDBS4990'                TO PGM-CHIAMATO
      *           PERFORM U0029-CLOSE-CURSOR     THRU U0029-EX
              ELSE
      *           MOVE 'LDBS4990'                TO PGM-CHIAMATO
      *           PERFORM U0029-CLOSE-CURSOR     THRU U0029-EX
                 MOVE 'LDBS5020'                TO PGM-CHIAMATO
                 PERFORM U0029-CLOSE-CURSOR     THRU U0029-EX
              END-IF
           END-IF.
      *
       U0032A-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CLOSE CURSOR SU COMP_QUEST
      *----------------------------------------------------------------*
       U0032-LEGGI-DOMANDA.
      *
           INITIALIZE DOMANDA-COLLG.
           SET IDSV0003-FETCH-FIRST      TO TRUE.

           MOVE Q04-ID-COMP-QUEST        TO Q05-ID-COMP-QUEST-PAD

           MOVE 'LDBS5020'                      TO PGM-CHIAMATO.
           CALL PGM-CHIAMATO                    USING IDSV0003
                                                      DOMANDA-COLLG
           ON EXCEPTION
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-LDBS5020 ERRORE CHIAMATA - LDBS5020'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
              PERFORM U028D-VERIFICA-DOM-COLL   THRU U028D-EX
              IF WK-DEROGA-NO
                 PERFORM U0032A-GESIONE-DOMCOLL THRU U0032A-EX
              ELSE
                 SET WK-RIS-TROVATO             TO TRUE
                 MOVE 'LDBS5020'                TO PGM-CHIAMATO
                 PERFORM U0029-CLOSE-CURSOR     THRU U0029-EX
                 MOVE 'LDBS4990'                TO PGM-CHIAMATO
                 PERFORM U0029-CLOSE-CURSOR     THRU U0029-EX
              END-IF
           ELSE
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-SUCCESSFUL-RC     TO TRUE
                 SET IDSV0003-SUCCESSFUL-SQL    TO TRUE
              ELSE
                 SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
              END-IF
           END-IF.
      *
       U0032-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CLOSE CURSOR SU COMP_QUEST
      *----------------------------------------------------------------*
       U0029-CLOSE-CURSOR.
      *
           SET IDSV0003-CLOSE-CURSOR            TO TRUE.

           CALL PGM-CHIAMATO                    USING IDSV0003
                                                      COMP-QUEST
           ON EXCEPTION
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'U0029-CLOSE-CURSOR'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
       U0029-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   LETTURA DETTAGLIO QUESTIONARIO DA DB
      *----------------------------------------------------------------*
       U0020-LEGGI-QUE-DB.
      *
           INITIALIZE LDBV2321.
           PERFORM U0007-IMPOSTA-GENERALE        THRU U0007-EX.
           MOVE 1                                TO IX-APPO-OVERF.
           MOVE 0                                TO IX-TAB-QUE.
      *
           MOVE ZEROES                           TO WK-ID-ASSIC.
           MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(1:5)
                                                 TO WK-APP-ASSICURATO.
           MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(7:2)
                                                 TO WK-TP-QUEST.
      *
           IF IX-GUIDA-GRZ = ZERO
              PERFORM S9005-ERRORE-LIV-VAR     THRU S9005-EX
           ELSE
              EVALUATE TRUE
                WHEN PRIMO-ASS
                  IF WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ)
                       TO LDBV2321-ID-RAPP-ANA
                  END-IF

                WHEN SECONDO-ASS
                  IF WGRZ-ID-2O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-2O-ASSTO(IX-GUIDA-GRZ)
                       TO LDBV2321-ID-RAPP-ANA
                  END-IF

                WHEN TERZO-ASS
                  IF WGRZ-ID-3O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-3O-ASSTO(IX-GUIDA-GRZ)
                       TO LDBV2321-ID-RAPP-ANA
                  END-IF
              END-EVALUATE
      *
              MOVE WK-TP-QUEST               TO LDBV2321-TP-QUEST
              MOVE WGRZ-ID-GAR(IX-GUIDA-GRZ) TO LDBV2321-ID-GAR
      *
              MOVE LDBV2321               TO IDSI0011-BUFFER-WHERE-COND
              MOVE QUEST                     TO IDSI0011-BUFFER-DATI
      *
              PERFORM Z0000-LETTURA-GEN      THRU Z0000-EX
      *
              MOVE IX-TAB-QUE                TO WQUE-ELE-QUEST-MAX
           END-IF.
      *
       U0020-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   LETTURA DETTAGLIO QUESTIONARIO DA CONTESTO
      *----------------------------------------------------------------*
       U0021-LEGGI-QUE-CONT.
      *
           MOVE ZEROES                           TO WK-ID-ASSIC
                                                    IX-APP-QUE.
           MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(1:5)
                                                 TO WK-APP-ASSICURATO.
           MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(7:2)
                                                 TO WK-TP-QUEST.
           SET WK-LETTO-NO                       TO TRUE.
      *
           IF IX-GUIDA-GRZ = ZERO
              PERFORM S9005-ERRORE-LIV-VAR       THRU S9005-EX
           ELSE
              EVALUATE TRUE
                WHEN PRIMO-ASS
                  IF WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ)
                       TO WK-ID-ASSIC
                  END-IF

                WHEN SECONDO-ASS
                  IF WGRZ-ID-2O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-2O-ASSTO(IX-GUIDA-GRZ)
                       TO WK-ID-ASSIC
                  END-IF

                WHEN TERZO-ASS
                  IF WGRZ-ID-3O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-3O-ASSTO(IX-GUIDA-GRZ)
                       TO WK-ID-ASSIC
                  END-IF

              END-EVALUATE
      *
              IF WK-ID-ASSIC IS NUMERIC
              AND WK-ID-ASSIC GREATER ZEROES
      *
                PERFORM VARYING IX-TAB-QUE FROM 1 BY 1
                   UNTIL IX-TAB-QUE > WQUE-ELE-QUEST-MAX
                      OR WK-LETTO-SI
                   IF WQUE-ID-RAPP-ANA(IX-TAB-QUE) =  WK-ID-ASSIC
                   AND WQUE-TP-QUEST(IX-TAB-QUE) = WK-TP-QUEST
                       MOVE IX-TAB-QUE            TO IX-APP-QUE
                       SET WK-LETTO-SI            TO TRUE
                   END-IF
                END-PERFORM
      *
              END-IF
           END-IF.
      *
       U0021-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE ISO
      *----------------------------------------------------------------*
       U0022-LEGGI-POG.
      *
           PERFORM U0007-IMPOSTA-GENERALE       THRU U0007-EX.
      *
39534 *    MOVE 60                              TO IX-APPO-OVERF.
39534      MOVE 160                             TO IX-APPO-OVERF.
      *
           MOVE  PARAM-OGG                      TO IDSI0011-BUFFER-DATI.
      *
29023 *    MOVE 0                               TO IX-TAB-POG.
MC    *
           PERFORM Z0000-LETTURA-GEN            THRU Z0000-EX.
      *
MC         MOVE IX-TAB-POG                    TO WPOG-ELE-PARAM-OGG-MAX.
      *
       U0022-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE ISO
      *----------------------------------------------------------------*
       U0023-LEGGI-LQU.
      *
           PERFORM U0007-IMPOSTA-GENERALE       THRU U0007-EX.
      *
           MOVE V1391-TIPO-OGGETTO(IX-COD-MVV)  TO LQU-TP-OGG.
           MOVE WADE-ID-ADES(1)                 TO LQU-ID-OGG.
      *
MC         MOVE 10                              TO IX-APPO-OVERF.
      *
           MOVE  LIQ                            TO IDSI0011-BUFFER-DATI.
      *
           MOVE 0                               TO IX-TAB-LQU.
MC    *
           PERFORM Z0000-LETTURA-GEN            THRU Z0000-EX.
      *
MC         MOVE IX-TAB-LQU                      TO WLQU-ELE-LIQ-MAX.
      *
       U0023-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE ISO
      *----------------------------------------------------------------*
       U0024-LEGGI-PCO.
      *
           MOVE HIGH-VALUE                      TO PARAM-COMP.
      *
           PERFORM U0007-IMPOSTA-GENERALE       THRU U0007-EX.
      *
           MOVE IDSV0003-TRATTAMENTO-STORICITA  TO WKS-APPO-STOR.
      *
           SET IDSV0003-TRATT-SENZA-STOR        TO TRUE.
      *
           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA  TO PCO-COD-COMP-ANIA.
      *
           MOVE  PARAM-COMP                     TO IDSI0011-BUFFER-DATI.
      *
           PERFORM Z0000-LETTURA-GEN            THRU Z0000-EX.
      *
           MOVE IX-TAB-PCO                      TO WPCO-ELE-PCO-MAX.
      *
           MOVE WKS-APPO-STOR
             TO IDSV0003-TRATTAMENTO-STORICITA.
      *
       U0024-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE RST
      *----------------------------------------------------------------*
       U0025-LEGGI-RST-DB.
      *
           PERFORM U0007-IMPOSTA-GENERALE       THRU U0007-EX.
      *
           MOVE C216-ID-LIVELLO-T(IX-VAL-VAR)     TO RST-ID-TRCH-DI-GAR
      *
           MOVE RIS-DI-TRCH                     TO IDSI0011-BUFFER-DATI.
      *
           PERFORM Z0000-LETTURA-GEN            THRU Z0000-EX.
      *
           MOVE IX-TAB-RST                      TO WRST-ELE-RST-MAX.
      *
       U0025-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE RST
      *----------------------------------------------------------------*
       U0033-LEGGI-E12.
      *
           PERFORM U0007-IMPOSTA-GENERALE     THRU U0007-EX.
      *
           MOVE 1                             TO IX-APPO-OVERF.
      *
           MOVE C216-ID-LIVELLO-T(IX-VAL-VAR)   TO E12-ID-TRCH-DI-GAR.
      *
           MOVE EST-TRCH-DI-GAR               TO IDSI0011-BUFFER-DATI.
      *
           PERFORM Z0000-LETTURA-GEN          THRU Z0000-EX.
      *
           MOVE IX-TAB-E12                    TO WE12-ELE-ESTRA-MAX.
      *
       U0033-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE RST
      *----------------------------------------------------------------*
       U0034-LEGGI-L30.
      *
           PERFORM U0007-IMPOSTA-GENERALE     THRU U0007-EX.
      *
           MOVE 1                             TO IX-APPO-OVERF.
      *
           MOVE WPOL-IB-OGG                   TO L30-IB-POLI.
      *
           MOVE REINVST-POLI-LQ               TO IDSI0011-BUFFER-DATI.
      *
           PERFORM Z0000-LETTURA-GEN          THRU Z0000-EX.
      *
           MOVE IX-TAB-L30
             TO WL30-ELE-REINVST-POLI-MAX.
      *
       U0034-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE L23
      *----------------------------------------------------------------*
       U0035-LEGGI-L23.
      *
           PERFORM U0007-IMPOSTA-GENERALE     THRU U0007-EX.
      *
           MOVE 1                             TO IX-APPO-OVERF.
      *
           MOVE WK-ID-ASSIC                   TO L23-ID-RAPP-ANA.
      *
           MOVE VINC-PEG                      TO IDSI0011-BUFFER-DATI.
      *
           PERFORM Z0000-LETTURA-GEN          THRU Z0000-EX.
      *
           MOVE IX-TAB-L23
             TO WL23-ELE-VINC-PEG-MAX.
      *
       U0035-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE PVT
      *----------------------------------------------------------------*
       U0036-LEGGI-PVT.
      *
           PERFORM U0007-IMPOSTA-GENERALE     THRU U0007-EX.
      *
           MOVE 10                            TO IX-APPO-OVERF.
      *
           MOVE WGRZ-ID-GAR(IX-GUIDA-GRZ)     TO PVT-ID-GAR.
      *
           MOVE PROV-DI-GAR                   TO IDSI0011-BUFFER-DATI.
      *
           PERFORM Z0000-LETTURA-GEN          THRU Z0000-EX.
      *
           MOVE IX-TAB-PVT
             TO WPVT-ELE-PROV-MAX.
      *
       U0036-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE PRE
      *----------------------------------------------------------------*
       U0037-LEGGI-PRE.
      *
           PERFORM U0007-IMPOSTA-GENERALE     THRU U0007-EX.
      *
           MOVE 10                            TO IX-APPO-OVERF.
      *
           MOVE WADE-ID-ADES(1)               TO PRE-ID-OGG.
           MOVE 'AD'                          TO PRE-TP-OGG.
      *
           MOVE PREST                         TO IDSI0011-BUFFER-DATI.
      *
           PERFORM Z0000-LETTURA-GEN          THRU Z0000-EX.
      *
           MOVE IX-TAB-PRE
             TO WPRE-ELE-PRESTITI-MAX.
      *
       U0037-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  LETTURE P67
      *----------------------------------------------------------------*
       U0039-LEGGI-P67.
      *
           PERFORM U0007-IMPOSTA-GENERALE     THRU U0007-EX.
      *
           MOVE 1                             TO IX-APPO-OVERF.
      *
           MOVE WPOL-ID-POLI                  TO P67-ID-EST-POLI-CPI-PR.
      *
           MOVE EST-POLI-CPI-PR               TO IDSI0011-BUFFER-DATI.
      *
           PERFORM Z0000-LETTURA-GEN          THRU Z0000-EX.
      *
           MOVE IX-TAB-P67
             TO WP67-ELE-MAX-EST-POLI-CPI-PR.
      *
       U0039-EX.
           EXIT.
NEWFIS*----------------------------------------------------------------*
NEWFIS*  LETTURE DATI CRISTALLIZATI
NEWFIS*----------------------------------------------------------------*
NEWFIS U0050-LEGGI-P61.
NEWFIS*
NEWFIS     PERFORM U0007-IMPOSTA-GENERALE     THRU U0007-EX.
NEWFIS*
NEWFIS     MOVE 1                             TO IX-APPO-OVERF.
NEWFIS*
NEWFIS     MOVE WPOL-ID-POLI                  TO P61-ID-POLI.
NEWFIS*
NEWFIS     MOVE D-CRIST               TO IDSI0011-BUFFER-DATI.
NEWFIS*
NEWFIS     PERFORM Z0000-LETTURA-GEN          THRU Z0000-EX.
NEWFIS*
NEWFIS     MOVE IX-TAB-P61
NEWFIS       TO WP61-ELE-MAX-D-CRIST.
NEWFIS*
NEWFIS U0050-EX.
NEWFIS     EXIT.
      *----------------------------------------------------------------*
      *   IMPOSTAZIONI GENERALI PER LETTURA.
      *----------------------------------------------------------------*
       U0007-IMPOSTA-GENERALE.
      *
           MOVE ZERO                  TO IDSI0011-DATA-INIZIO-EFFETTO
                                         IDSI0011-DATA-FINE-EFFETTO
                                         IDSI0011-DATA-COMPETENZA
                                         IDSI0011-DATA-COMP-AGG-STOR.
      *
           MOVE V1391-OPERAZIONE(IX-COD-MVV)
                                      TO IDSI0011-OPERAZIONE.
           MOVE V1391-LIVELLO-OPERAZIONE(IX-COD-MVV)
                                      TO IDSI0011-LIVELLO-OPERAZIONE.
      *
           IF IDSI0011-WHERE-CONDITION
              MOVE V1391-SERVIZIO-LETTURA(IX-COD-MVV)
                                      TO IDSI0011-CODICE-STR-DATO
           ELSE
              MOVE V1391-COD-STR-DATO-PTF(IX-COD-MVV)
                                      TO IDSI0011-CODICE-STR-DATO
           END-IF.
      *
           MOVE V1391-COD-STR-DATO-PTF(IX-COD-MVV) TO WKS-NOME-TABELLA.
      *
           MOVE IDSV0003-TRATTAMENTO-STORICITA
             TO IDSI0011-TRATTAMENTO-STORICITA.
      *
       U0007-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLO STRUTTURA TABELLA SE NON E' GIA STATA PRECEDENTEMENTE
      *  CARICATA
      *----------------------------------------------------------------*
       U0008-VERIFICA-DATO.
      *
            SET WKS-TROVATO-TABB-NO           TO TRUE.
            SET TROVATO-DATO-IN-CACHE-NO      TO TRUE.
            SET WK-CAMPO-FIND-NO              TO TRUE.
            MOVE 1                            TO IX-AP-TABB.
      *
            PERFORM VARYING IX-TABB FROM 1 BY 1
              UNTIL IX-TABB > WKS-ELE-MAX-TABB
                OR WKS-TROVATO-TABB-SI
      *
                IF V1391-COD-STR-DATO-PTF(IX-COD-MVV) =
                   WKS-NOME-TABB(IX-TABB)
                   SET WKS-TROVATO-TABB-SI    TO TRUE
      * MI SALVO L'INDICE PER CAPIRE QUAL'E LA TABELLA DA UTILIZZARE
      * PER REPERIRE IL DATO
                   MOVE IX-TABB               TO IX-AP-TABB
                END-IF
            END-PERFORM.

           IF  IDSV0003-BATCH-INFR
            IF WKS-TROVATO-TABB-NO
               SET TROVATO-DATO-IN-CACHE-NO   TO TRUE
               IF ADDRESS-CACHE-VAL-VAR-SI
                  PERFORM Z100-RICERCA-DATO-IN-CACHE THRU Z100-EX
               END-IF
               IF TROVATO-DATO-IN-CACHE-NO
                  SET  CACHE-PIENA-NO                TO TRUE
                  PERFORM U0009-CALL-IDSS0020        THRU U0009-EX

                  IF IDSV0003-SUCCESSFUL-RC
                     IF ADDRESS-CACHE-VAL-VAR-SI AND
                        CACHE-PIENA-NO
                        PERFORM Z200-CARICA-CACHE    THRU Z200-EX
                     END-IF
                  END-IF
               ELSE
ALEX              MOVE ZERO           TO IDSI0011-DATA-INIZIO-EFFETTO
                                         IDSI0011-DATA-FINE-EFFETTO
                                         IDSI0011-DATA-COMPETENZA
                                         IDSI0011-DATA-COMP-AGG-STOR
                  PERFORM VARYING IX-DATO FROM 1 BY 1
                   UNTIL IX-DATO > WKS-ELE-MAX-DATO(IX-AP-TABB)
                      OR WK-CAMPO-FIND-SI
                      IF WKS-CODICE-DATO(IX-AP-TABB, IX-DATO) =
                         V1391-COD-DATO-PTF(IX-COD-MVV)
      *
                         SET WK-CAMPO-FIND-SI         TO TRUE
                         MOVE IX-DATO                 TO IX-AP-DATO
                      END-IF
                  END-PERFORM
               END-IF
            ELSE
               PERFORM VARYING IX-DATO FROM 1 BY 1
                UNTIL IX-DATO > WKS-ELE-MAX-DATO(IX-AP-TABB)
                   OR WK-CAMPO-FIND-SI
                   IF WKS-CODICE-DATO(IX-AP-TABB, IX-DATO) =
                      V1391-COD-DATO-PTF(IX-COD-MVV)
      *
                      SET WK-CAMPO-FIND-SI         TO TRUE
                      MOVE IX-DATO                 TO IX-AP-DATO
                   END-IF
               END-PERFORM
            END-IF
           ELSE
            IF WKS-TROVATO-TABB-SI
               PERFORM VARYING IX-DATO FROM 1 BY 1
                UNTIL IX-DATO > WKS-ELE-MAX-DATO(IX-AP-TABB)
                   OR WK-CAMPO-FIND-SI
                   IF WKS-CODICE-DATO(IX-AP-TABB, IX-DATO) =
                      V1391-COD-DATO-PTF(IX-COD-MVV)
      *
                      SET WK-CAMPO-FIND-SI         TO TRUE
                      MOVE IX-DATO                 TO IX-AP-DATO
                   END-IF

               END-PERFORM
            END-IF
      *
            IF WKS-TROVATO-TABB-NO
            OR WK-CAMPO-FIND-NO
               PERFORM U0009-CALL-IDSS0020    THRU U0009-EX
            END-IF
           END-IF.
      *
       U0008-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    RICERCA IN CACHE
      *----------------------------------------------------------------*
       Z100-RICERCA-DATO-IN-CACHE.

           SET IDSV0102-SEARCH-INDEX                TO 1
           SEARCH IDSV0102-AREA-COD-STR-DATO

              AT END
                 SET TROVATO-DATO-IN-CACHE-NO       TO TRUE

              WHEN IDSV0102-COD-STR-DATO (IDSV0102-SEARCH-INDEX) =
                   V1391-COD-STR-DATO-PTF(IX-COD-MVV)
                   SET TROVATO-DATO-IN-CACHE-SI       TO TRUE
                   SET IX-CACHE           TO IDSV0102-SEARCH-INDEX

                   ADD 1                  TO IDSV0102-CONTA-CACHE-CSV

                   ADD 1                       TO WKS-ELE-MAX-TABB
                   MOVE WKS-ELE-MAX-TABB       TO IX-TABB
                                                  IX-AP-TABB
                   MOVE 0                      TO IX-DATO
                                                  IX-AP-DATO

                   MOVE V1391-COD-STR-DATO-PTF(IX-COD-MVV)
                                            TO WKS-NOME-TABB(IX-AP-TABB)
                   PERFORM VARYING IX-CACHE FROM IX-CACHE BY 1
                      UNTIL IX-CACHE     > IDSV0102-LIMITE-MAX
                         OR IDSV0102-COD-STR-DATO(IX-CACHE) NOT =
                            V1391-COD-STR-DATO-PTF(IX-COD-MVV)
                         OR IDSV0102-CODICE-DATO (IX-CACHE)
                                                 = HIGH-VALUES OR SPACES
                         OR NOT IDSV0003-SUCCESSFUL-RC

                         IF WKS-ELE-MAX-DATO(IX-AP-TABB) >= 250
                            MOVE WK-PGM      TO IDSV0003-COD-SERVIZIO-BE
                            MOVE 'OVERFLOW TABELLA WKS- X CSD-ADA'
                                         TO IDSV0003-DESCRIZ-ERR-DB2
                            SET IDSV0003-INVALID-OPER  TO TRUE
                         ELSE
                            ADD 1        TO WKS-ELE-MAX-DATO(IX-AP-TABB)
                                                 IX-DATO

                            MOVE IDSV0102-CODICE-DATO       (IX-CACHE)
                                TO WKS-CODICE-DATO (IX-AP-TABB, IX-DATO)
                            MOVE IDSV0102-TIPO-DATO    (IX-CACHE)
                                TO WKS-TIPO-DATO   (IX-AP-TABB, IX-DATO)
                            MOVE IDSV0102-INI-POSI     (IX-CACHE)
                                TO WKS-INI-POSI    (IX-AP-TABB, IX-DATO)
                            MOVE IDSV0102-LUN-DATO     (IX-CACHE)
                                TO WKS-LUN-DATO    (IX-AP-TABB, IX-DATO)
                            MOVE IDSV0102-LUNGHEZZA-DATO-NOM(IX-CACHE)
                                TO WKS-LUNGHEZZA-DATO-NOM
                                                   (IX-AP-TABB, IX-DATO)
                            MOVE IDSV0102-PRECISIONE-DATO   (IX-CACHE)
                                TO WKS-PRECISIONE-DATO
                                                   (IX-AP-TABB, IX-DATO)
                         END-IF
                   END-PERFORM

              WHEN IDSV0102-COD-STR-DATO (IDSV0102-SEARCH-INDEX) =
                   SPACES OR HIGH-VALUE OR LOW-VALUE
                   CONTINUE

           END-SEARCH.

       Z100-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    CARICA IN CACHE
      *----------------------------------------------------------------*
       Z200-CARICA-CACHE.

           PERFORM VARYING IX-IDSS0020 FROM 1 BY 1
              UNTIL IX-IDSS0020 > IDSO0021-LIMITE-STR-MAX
                 OR IDSO0021-CODICE-DATO(IX-IDSS0020)
                                            = HIGH-VALUES OR SPACES

                    ADD 1              TO IDSV0102-ELE-MAX-COD-STR-DATO

                    MOVE V1391-COD-STR-DATO-PTF(IX-COD-MVV)
                                       TO IDSV0102-COD-STR-DATO
                                         (IDSV0102-ELE-MAX-COD-STR-DATO)
                    MOVE IDSO0021-CODICE-DATO(IX-IDSS0020)
                                       TO IDSV0102-CODICE-DATO
                                         (IDSV0102-ELE-MAX-COD-STR-DATO)
                    MOVE IDSO0021-TIPO-DATO (IX-IDSS0020)
                                       TO IDSV0102-TIPO-DATO
                                         (IDSV0102-ELE-MAX-COD-STR-DATO)
                    MOVE IDSO0021-INIZIO-POSIZIONE(IX-IDSS0020)
                                       TO IDSV0102-INI-POSI
                                         (IDSV0102-ELE-MAX-COD-STR-DATO)
                    MOVE IDSO0021-LUNGHEZZA-DATO(IX-IDSS0020)
                                       TO IDSV0102-LUN-DATO
                                         (IDSV0102-ELE-MAX-COD-STR-DATO)
                    MOVE IDSO0021-LUNGHEZZA-DATO-NOM(IX-IDSS0020)
                                       TO IDSV0102-LUNGHEZZA-DATO-NOM
                                         (IDSV0102-ELE-MAX-COD-STR-DATO)
                    MOVE IDSO0021-PRECISIONE-DATO(IX-IDSS0020)
                                       TO IDSV0102-PRECISIONE-DATO
                                         (IDSV0102-ELE-MAX-COD-STR-DATO)
           END-PERFORM.

       Z200-EX.
           EXIT.

      *----------------------------------------------------------------*
      *  CHIAMATA A MODULO IDSS0020 PER RECUPERARE LA STRUTTURA TAB.
      *----------------------------------------------------------------*
       U0009-CALL-IDSS0020.
      *
           MOVE SPACES                TO INPUT-IDSS0020.
      *
           MOVE ZERO                  TO IDSI0011-DATA-INIZIO-EFFETTO
                                         IDSI0011-DATA-FINE-EFFETTO
                                         IDSI0011-DATA-COMPETENZA
                                         IDSI0011-DATA-COMP-AGG-STOR.
      *
           SET IDSI0021-INITIALIZE-SI TO TRUE
           SET IDSI0021-ON-LINE       TO TRUE.
           MOVE 9999                  TO IDSI0021-TIPO-MOVIMENTO.
           SET IDSI0021-PTF-NEWLIFE   TO TRUE.
      *
           MOVE V1391-COD-COMPAGNIA-ANIA
                                      TO IDSI0021-CODICE-COMPAGNIA-ANIA.
      *
           MOVE V1391-COD-STR-DATO-PTF(IX-COD-MVV)
                                      TO IDSI0021-CODICE-STR-DATO.
      *
           MOVE SPACES                TO AREA-CALL.
           MOVE INPUT-IDSS0020        TO AREA-CALL.
      *
           MOVE 'IDSS0020'            TO PGM-CHIAMATO.
           CALL PGM-CHIAMATO          USING AREA-CALL
           ON EXCEPTION
              MOVE PGM-CHIAMATO
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-IDSS0020 ERRORE CHIAMATA - IDSS0020'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *

           MOVE SPACES                TO OUTPUT-IDSS0020.
           MOVE AREA-CALL             TO OUTPUT-IDSS0020.
           MOVE 250                   TO IDSO0021-LIMITE-STR-MAX.
      *
           IF  IDSV0003-BATCH-INFR AND
               ADDRESS-CACHE-VAL-VAR-SI
               COMPUTE IDSO0021-NUM-ELEM =
                       IDSV0102-ELE-MAX-COD-STR-DATO +
                       IDSO0021-NUM-ELEM

               IF IDSO0021-NUM-ELEM > IDSV0102-LIMITE-MAX
                  SET CACHE-PIENA-SI         TO TRUE
                  MOVE WK-PGM                TO IDSV0003-COD-SERVIZIO-BE
                  MOVE 'OVERFLOW TABELLA CACHE X CSD-ADA'
                                             TO IDSV0003-DESCRIZ-ERR-DB2
               END-IF
           END-IF.

           IF IDSO0021-SUCCESSFUL-RC
              PERFORM U0011-VALORIZZA-AREAT
                                      THRU U0011-EX
           ELSE
              SET IDSV0003-INVALID-OPER     TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING 'ERR CHIAMATA IDSS0020 PER LA VARIABILE = '
                V1391-COD-ACTU '; RC = '
                IDSO0021-RETURN-CODE '; '
                DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.
      *
       U0009-EX.
           EXIT.
      *------------------------------------------------------------*
      *  VALORIZZAZIONI AREA TBELLA                                *
      *------------------------------------------------------------*
       U0011-VALORIZZA-AREAT.
      *
           SET WK-CAMPO-FIND-NO        TO TRUE.
           ADD 1                       TO WKS-ELE-MAX-TABB.
           MOVE WKS-ELE-MAX-TABB       TO IX-TABB
                                          IX-AP-TABB.
           MOVE 0                      TO IX-DATO
                                          IX-AP-DATO.
      *
           MOVE V1391-COD-STR-DATO-PTF(IX-COD-MVV)
                                       TO WKS-NOME-TABB(IX-AP-TABB).
           PERFORM VARYING IX-IDSS0020 FROM 1 BY 1
              UNTIL IX-IDSS0020 > 250
                 OR IDSO0021-CODICE-DATO(IX-IDSS0020)
                 = HIGH-VALUES OR SPACES
      *
                 ADD 1                 TO WKS-ELE-MAX-DATO(IX-AP-TABB)
                 ADD 1                 TO IX-DATO
      *
                 MOVE IDSO0021-CODICE-DATO(IX-IDSS0020)
                   TO WKS-CODICE-DATO(IX-AP-TABB, IX-DATO)
                 MOVE IDSO0021-TIPO-DATO(IX-IDSS0020)
                   TO WKS-TIPO-DATO(IX-AP-TABB, IX-DATO)
                 MOVE IDSO0021-INIZIO-POSIZIONE(IX-IDSS0020)
                   TO WKS-INI-POSI(IX-AP-TABB, IX-DATO)
                 MOVE IDSO0021-LUNGHEZZA-DATO(IX-IDSS0020)
                   TO WKS-LUN-DATO(IX-AP-TABB, IX-DATO)
                 MOVE IDSO0021-LUNGHEZZA-DATO-NOM(IX-IDSS0020)
                   TO WKS-LUNGHEZZA-DATO-NOM(IX-AP-TABB, IX-DATO)
                 MOVE IDSO0021-PRECISIONE-DATO(IX-IDSS0020)
                   TO WKS-PRECISIONE-DATO(IX-AP-TABB, IX-DATO)
      *
           END-PERFORM.
      *
           PERFORM VARYING IX-DATO FROM 1 BY 1
             UNTIL IX-DATO > WKS-ELE-MAX-DATO(IX-AP-TABB)
                OR WK-CAMPO-FIND-SI
                IF WKS-CODICE-DATO(IX-AP-TABB, IX-DATO) =
                   V1391-COD-DATO-PTF(IX-COD-MVV)
      *
                   SET WK-CAMPO-FIND-SI         TO TRUE
                   MOVE IX-DATO                 TO IX-AP-DATO
                END-IF

           END-PERFORM.
      *
       U0011-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   VALORIZZA AREA OUTPUT TRAMITE VARIABILI CHE ARRIVANO
      *   NELL'AREA DI CONTESTO IVVC0212.
      *----------------------------------------------------------------*
       U0012-VALORIZZA-INT.
      *
           SET WK-INTERN-FIND-NO               TO TRUE.
           MOVE 0                              TO IX-INTERN.
      *
           IF GESTIONE-PRODOTTO
              PERFORM VARYING IX-INTERN FROM 1 BY 1
                UNTIL IX-INTERN > WCNT-ELE-VAR-CONT-MAX
                OR WK-INTERN-FIND-SI
                IF WCNT-TP-LIVELLO(IX-INTERN) NOT = SPACES
                AND WCNT-ID-LIVELLO(IX-INTERN) GREATER ZERO

                   IF V1391-COD-DATO-INTERNO(IX-COD-MVV) =
                      WCNT-COD-VAR-CONT(IX-INTERN)
                      IF (WCNT-TP-LIVELLO(IX-INTERN) = 'P'
                          AND WCNT-TP-LIVELLO(IX-INTERN) =
                              C216-TP-LIVELLO-P(IX-VAL-VAR)
                          AND WCNT-ID-LIVELLO(IX-INTERN) =
                              C216-ID-POL-P(IX-VAL-VAR))

                            SET WK-INTERN-FIND-SI     TO TRUE
      *
                            ADD 1                     TO IX-COD-VAR-OUT
      *
                            MOVE V1391-FORMATTAZIONE-DATO
                                 (IX-COD-MVV)(1:1)
                              TO C216-TP-DATO-P
                                 (IX-VAL-VAR, IX-COD-VAR-OUT)

                            MOVE WCNT-COD-VAR-CONT(IX-INTERN)
                              TO C216-COD-VARIABILE-P
                                 (IX-VAL-VAR, IX-COD-VAR-OUT)
      *
                            EVALUATE C216-TP-DATO-P
                                    (IX-VAL-VAR, IX-COD-VAR-OUT)
                                WHEN 'A'
                                WHEN 'N'
                                WHEN 'I'
                                     MOVE WCNT-VAL-IMP-CONT(IX-INTERN)
                                       TO C216-VAL-IMP-P
                                          (IX-VAL-VAR, IX-COD-VAR-OUT)
                                WHEN 'P'
                                WHEN 'M'
                                     MOVE WCNT-VAL-PERC-CONT(IX-INTERN)
                                       TO C216-VAL-PERC-P
                                          (IX-VAL-VAR, IX-COD-VAR-OUT)


                                WHEN 'D'
                                WHEN 'S'
                                WHEN 'X'
                                     MOVE WCNT-VAL-STR-CONT(IX-INTERN)
                                       TO C216-VAL-STR-P
                                          (IX-VAL-VAR, IX-COD-VAR-OUT)
                            END-EVALUATE
                      END-IF

                   END-IF

                ELSE

                   IF V1391-COD-DATO-INTERNO(IX-COD-MVV) =
                      WCNT-COD-VAR-CONT(IX-INTERN)
                          SET WK-INTERN-FIND-SI        TO TRUE
      *
                          ADD 1                        TO IX-COD-VAR-OUT
      *
                          MOVE V1391-FORMATTAZIONE-DATO(IX-COD-MVV)(1:1)
                            TO C216-TP-DATO-P
                               (IX-VAL-VAR, IX-COD-VAR-OUT)
      *
                          MOVE WCNT-COD-VAR-CONT(IX-INTERN)
                          TO C216-COD-VARIABILE-P
                             (IX-VAL-VAR, IX-COD-VAR-OUT)
      *
                          EVALUATE C216-TP-DATO-P
                                  (IX-VAL-VAR, IX-COD-VAR-OUT)
                              WHEN 'A'
                              WHEN 'N'
                              WHEN 'I'
                                   MOVE WCNT-VAL-IMP-CONT(IX-INTERN)
                                     TO C216-VAL-IMP-P
                                        (IX-VAL-VAR, IX-COD-VAR-OUT)
                              WHEN 'P'
                              WHEN 'M'
                                   MOVE WCNT-VAL-PERC-CONT(IX-INTERN)
                                     TO C216-VAL-PERC-P
                                        (IX-VAL-VAR, IX-COD-VAR-OUT)
                              WHEN 'D'
                              WHEN 'S'
                              WHEN 'X'
                                   MOVE WCNT-VAL-STR-CONT(IX-INTERN)
                                     TO C216-VAL-STR-P
                                        (IX-VAL-VAR, IX-COD-VAR-OUT)
                          END-EVALUATE
      *
                   END-IF
                  END-IF

              END-PERFORM
           ELSE
              PERFORM VARYING IX-INTERN FROM 1 BY 1
                UNTIL IX-INTERN > WCNT-ELE-VAR-CONT-MAX
                OR WK-INTERN-FIND-SI

                IF WCNT-TP-LIVELLO(IX-INTERN) NOT = SPACES
                AND WCNT-ID-LIVELLO(IX-INTERN) GREATER ZERO

                   IF V1391-COD-DATO-INTERNO(IX-COD-MVV) =
                      WCNT-COD-VAR-CONT(IX-INTERN)
                      IF (WCNT-TP-LIVELLO(IX-INTERN) = 'G'
                       AND WCNT-TP-LIVELLO(IX-INTERN) =
                           C216-TP-LIVELLO-T(IX-VAL-VAR)
                       AND WCNT-ID-LIVELLO(IX-INTERN) =
                           C216-ID-LIVELLO-T(IX-VAL-VAR))

                            SET WK-INTERN-FIND-SI     TO TRUE
      *
                            ADD 1                     TO IX-COD-VAR-OUT
      *
                            MOVE V1391-FORMATTAZIONE-DATO
                                 (IX-COD-MVV)(1:1)
                              TO C216-TP-DATO-T
                                 (IX-VAL-VAR, IX-COD-VAR-OUT)

                            MOVE WCNT-COD-VAR-CONT(IX-INTERN)
                              TO C216-COD-VARIABILE-T
                                 (IX-VAL-VAR, IX-COD-VAR-OUT)
      *
                            EVALUATE C216-TP-DATO-T
                                    (IX-VAL-VAR, IX-COD-VAR-OUT)
                                WHEN 'A'
                                WHEN 'N'
                                WHEN 'I'
                                     MOVE WCNT-VAL-IMP-CONT(IX-INTERN)
                                       TO C216-VAL-IMP-T
                                          (IX-VAL-VAR, IX-COD-VAR-OUT)
                                WHEN 'P'
                                WHEN 'M'
                                     MOVE WCNT-VAL-PERC-CONT(IX-INTERN)
                                       TO C216-VAL-PERC-T
                                          (IX-VAL-VAR, IX-COD-VAR-OUT)


                                WHEN 'D'
                                WHEN 'S'
                                WHEN 'X'
                                     MOVE WCNT-VAL-STR-CONT(IX-INTERN)
                                       TO C216-VAL-STR-T
                                          (IX-VAL-VAR, IX-COD-VAR-OUT)
                            END-EVALUATE
                      END-IF

                   END-IF

                ELSE

                   IF V1391-COD-DATO-INTERNO(IX-COD-MVV) =
                      WCNT-COD-VAR-CONT(IX-INTERN)
                          SET WK-INTERN-FIND-SI        TO TRUE
      *
                          ADD 1                        TO IX-COD-VAR-OUT
      *
                          MOVE V1391-FORMATTAZIONE-DATO(IX-COD-MVV)(1:1)
                            TO C216-TP-DATO-T
                               (IX-VAL-VAR, IX-COD-VAR-OUT)
      *
                          MOVE WCNT-COD-VAR-CONT(IX-INTERN)
                          TO C216-COD-VARIABILE-T
                             (IX-VAL-VAR, IX-COD-VAR-OUT)
      *
                          EVALUATE C216-TP-DATO-T
                                  (IX-VAL-VAR, IX-COD-VAR-OUT)
                              WHEN 'A'
                              WHEN 'N'
                              WHEN 'I'
                                   MOVE WCNT-VAL-IMP-CONT(IX-INTERN)
                                     TO C216-VAL-IMP-T
                                        (IX-VAL-VAR, IX-COD-VAR-OUT)
                              WHEN 'P'
                              WHEN 'M'
                                   MOVE WCNT-VAL-PERC-CONT(IX-INTERN)
                                     TO C216-VAL-PERC-T
                                        (IX-VAL-VAR, IX-COD-VAR-OUT)
                              WHEN 'D'
                              WHEN 'S'
                              WHEN 'X'
                                   MOVE WCNT-VAL-STR-CONT(IX-INTERN)
                                     TO C216-VAL-STR-T
                                        (IX-VAL-VAR, IX-COD-VAR-OUT)
                          END-EVALUATE
      *
                   END-IF
                  END-IF

              END-PERFORM
           END-IF.
      *
       U0012-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *    CHIUSURA CURSORI PER GESTIONE DEROGHE DA QUESTIONARI
      *----------------------------------------------------------------*
       U9999-CLOSE-ALL.
      *
           SET IDSV0003-SUCCESSFUL-SQL          TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC           TO TRUE.
           MOVE 'LDBS5020'                TO PGM-CHIAMATO.
           PERFORM U0029-CLOSE-CURSOR     THRU U0029-EX.
      *
           SET IDSV0003-SUCCESSFUL-SQL          TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC           TO TRUE.
           MOVE 'LDBS4990'                TO PGM-CHIAMATO.
           PERFORM U0029-CLOSE-CURSOR     THRU U0029-EX.

           SET IDSV0003-SUCCESSFUL-SQL          TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC           TO TRUE.
      *
       U9999-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CHIAMATA MODULO PER FORMATTAZIONE CAMPI COMP
      *----------------------------------------------------------------*
       Y9999-CHIAMA-IDSS0140.

           MOVE 'IDSS0140'            TO PGM-CHIAMATO.
      *
           CALL PGM-CHIAMATO          USING AREA-IDSV0141
             ON EXCEPTION
                MOVE 'IVVS0216'
                  TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'CALL-LDBS1390 ERRORE CHIAMATA - IDSS0140'
                  TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           IF IDSV0141-UNSUCCESSFUL-RC
              SET IDSV0003-FIELD-NOT-VALUED     TO TRUE
           END-IF.

       Y9999-EX.
           EXIT.
      *------------------------------------------------------------*
      *   PORTAFOGLIO VITA - PROCESSO VENDITA                      *
      *   CHIAMATA  AL DISPATCHER PER OPRAZIONI DI SELECT O FETCH  *
      *         E GESTIONE ERRORE ADES  (TABELLA ADESIONE)         *
      *   ULTIMO AGG.  07 FEB 2007                                 *
      *------------------------------------------------------------*
       Z0000-LETTURA-GEN.

           IF IDSI0011-SELECT
              PERFORM Z0001-SELECT-GEN        THRU Z0001-EX
           ELSE
              PERFORM Z0002-FETCH-GEN         THRU Z0002-EX
           END-IF.

       Z0000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *                         SELECT                                 *
      *----------------------------------------------------------------*
       Z0001-SELECT-GEN.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--->       OPERAZIONE ESEGUITA CORRETTAMENTE
                       PERFORM S1500-VALOUTORIZZA-DCLGEN
                          THRU S1500-EX
                       PERFORM S1600-VALORIZZA-OUTPUT
                          THRU S1600-EX
                  WHEN IDSO0011-NOT-FOUND
      *--->       CHIAVE NON TROVATA
                       IF V1391-COD-STR-DATO-PTF(IX-COD-MVV) = 'LIQ'
                       AND (V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '$$'
                       OR V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '**' )
                            MOVE IDSO0011-SQLCODE-SIGNED
                              TO IDSV0003-SQLCODE
                       ELSE
                           SET  IDSV0003-FIELD-NOT-VALUED TO TRUE
                           MOVE WK-PGM
                             TO IDSV0003-COD-SERVIZIO-BE
                           STRING WKS-NOME-TABELLA       ';'
                             IDSO0011-RETURN-CODE ';'
                             IDSO0011-SQLCODE DELIMITED BY SIZE
                           INTO IDSV0003-DESCRIZ-ERR-DB2
                           END-STRING
                       END-IF
                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                     SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                     MOVE WK-PGM
                       TO IDSV0003-COD-SERVIZIO-BE
                     STRING WKS-NOME-TABELLA       ';'
                        IDSO0011-RETURN-CODE ';'
                        IDSO0011-SQLCODE
                        DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                     END-STRING
              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              SET IDSV0003-INVALID-OPER TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              STRING WKS-NOME-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.

       Z0001-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         FETCH                                  *
      *----------------------------------------------------------------*
       Z0002-FETCH-GEN.

           SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
           SET  WCOM-OVERFLOW-NO                  TO TRUE.
           MOVE ZERO                              TO IX-TAB-UNIV.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR WCOM-OVERFLOW-YES

               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *-->             CHIAVE NON TROVATA
                       IF IDSI0011-FETCH-FIRST
                          IF V1391-COD-STR-DATO-PTF(IX-COD-MVV) = 'LIQ'
                       AND (V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '$$'
                       OR V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '**' )
                            MOVE IDSO0011-SQLCODE-SIGNED
                              TO IDSV0003-SQLCODE
                          ELSE
                           SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                           MOVE WK-PGM
                             TO IDSV0003-COD-SERVIZIO-BE
                           MOVE 'IVVS0216 - VALORIZZATORE - FETCH-FIRST'
                             TO IDSV0003-DESCRIZ-ERR-DB2
                          END-IF
                       END-IF
                      WHEN IDSO0011-SUCCESSFUL-SQL
      *-->                 OPERAZIONE ESEGUITA CORRETTAMENTE
                           ADD 1 TO IX-TAB-UNIV
                           IF IX-TAB-UNIV > IX-APPO-OVERF
                              SET WCOM-OVERFLOW-YES   TO TRUE
39534                         SET IDSI0011-CLOSE-CURSOR TO TRUE
39534                         PERFORM CALL-DISPATCHER
39534                            THRU CALL-DISPATCHER-EX
39534                         IF  NOT IDSO0011-SUCCESSFUL-RC
39534                         OR  NOT IDSO0011-SUCCESSFUL-SQL
39534                             MOVE WK-PGM
39534                               TO IDSV0003-COD-SERVIZIO-BE
39534                             STRING WKS-NOME-TABELLA           ';'
39534                                    IDSO0011-RETURN-CODE ';'
39534                                    IDSO0011-SQLCODE
39534                             DELIMITED BY SIZE
39534                             INTO IDSV0003-DESCRIZ-ERR-DB2
39534                             END-STRING
39534                         END-IF
                           ELSE
                              PERFORM S1500-VALOUTORIZZA-DCLGEN
                                 THRU S1500-EX
                              PERFORM S1600-VALORIZZA-OUTPUT
                                 THRU S1600-EX
                              SET IDSI0011-FETCH-NEXT TO TRUE
                           END-IF
                      WHEN OTHER
                         SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                         MOVE WK-PGM
                           TO IDSV0003-COD-SERVIZIO-BE
                         STRING WKS-NOME-TABELLA           ';'
                                IDSO0011-RETURN-CODE ';'
                                IDSO0011-SQLCODE
                         DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                         END-STRING
                  END-EVALUATE
               ELSE
      *-->        ERRORE DISPATCHER
                  SET IDSV0003-INVALID-OPER TO TRUE
                  MOVE WK-PGM
                    TO IDSV0003-COD-SERVIZIO-BE
                  STRING WKS-NOME-TABELLA           ';'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                  DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                  END-STRING
               END-IF
           END-PERFORM.

       Z0002-EX.
           EXIT.
      *****************************************************************
      *         CALL DISPATCHER
*     *****************************************************************
       CALL-DISPATCHER.

           MOVE ZEROES        TO IDSI0011-ID-MOVI-ANNULLATO

           MOVE IDSV0003-MODALITA-ESECUTIVA
                              TO IDSI0011-MODALITA-ESECUTIVA
           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
           MOVE IDSV0003-COD-MAIN-BATCH
                              TO IDSI0011-COD-MAIN-BATCH
           MOVE IDSV0003-TIPO-MOVIMENTO
                              TO IDSI0011-TIPO-MOVIMENTO
           MOVE IDSV0003-SESSIONE
                              TO IDSI0011-SESSIONE
           MOVE IDSV0003-USER-NAME
                              TO IDSI0011-USER-NAME

           IF IDSI0011-DATA-INIZIO-EFFETTO = 0
              MOVE IDSV0003-DATA-INIZIO-EFFETTO
                TO IDSI0011-DATA-INIZIO-EFFETTO
           END-IF

           IF IDSI0011-DATA-FINE-EFFETTO = 0
              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
           END-IF

           IF IDSI0011-DATA-COMPETENZA = 0
              MOVE IDSV0003-DATA-COMPETENZA
                              TO IDSI0011-DATA-COMPETENZA
           END-IF

           MOVE IDSV0003-DATA-COMP-AGG-STOR
                              TO IDSI0011-DATA-COMP-AGG-STOR


           MOVE IDSV0003-TRATTAMENTO-STORICITA
                              TO IDSI0011-TRATTAMENTO-STORICITA

           MOVE IDSV0003-FORMATO-DATA-DB
                              TO IDSI0011-FORMATO-DATA-DB

           SET IDSI0011-PTF-NEWLIFE    TO TRUE

           SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE

      *    MOVE IDSI0011-AREA          TO IDSV0012-AREA-I-O

      *    SET WS-ADDRESS-DIS          TO ADDRESS OF IDSV0012

           CALL WK-IDSS0010           USING IDSI0011-AREA
                                            IDSO0011-AREA.

      *    MOVE IDSV0012-AREA-I-O      TO IDSO0011-AREA.

           MOVE IDSO0011-SQLCODE-SIGNED
                                  TO IDSO0011-SQLCODE.


       CALL-DISPATCHER-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE TABELLE DATI
      *----------------------------------------------------------------*
       U0010-VALORIZZA-DATO.
      *
           EVALUATE V1391-COD-STR-DATO-PTF(IX-COD-MVV)

              WHEN 'MOVI'
               PERFORM S1001-TRATTA-MOV       THRU S1001-EX

              WHEN 'DFLT-ADES'
               PERFORM S1001A-TRATTA-DAD      THRU S1001A-EX

              WHEN 'POLI'
               PERFORM S1002-TRATTA-POL       THRU S1002-EX

              WHEN 'ADES'
               PERFORM S1003-TRATTA-ADE       THRU S1003-EX

              WHEN 'GAR'
               PERFORM S1004-TRATTA-GAR       THRU S1004-EX

              WHEN 'TRCH-DI-GAR'
S1046          PERFORM S1005-TRATTA-TGA       THRU S1005-EX

              WHEN 'RAPP-ANA'
               PERFORM S1027-TRATTA-RAN       THRU S1027-EX

              WHEN 'EST-RAPP-ANA'
               PERFORM S1056-TRATTA-EST-RAN   THRU S1056-EX

              WHEN 'RAPP-RETE'
               PERFORM S1028-TRATTA-RRE       THRU S1028-EX

              WHEN 'BNFIC'
               PERFORM S1008-TRATTA-BEP       THRU S1008-EX

              WHEN 'STRA-DI-INVST'
S1044          PERFORM S1009-TRATTA-SDI       THRU S1009-EX

              WHEN 'AST-ALLOC'
               PERFORM S1010-TRATTA-ALL       THRU S1010-EX

              WHEN 'D-COLL'
               PERFORM S1012-TRATTA-DCO       THRU S1012-EX

              WHEN 'D-FISC-ADES'
               PERFORM S1015-TRATTA-DFA       THRU S1015-EX

              WHEN 'PARAM-MOVI'
               PERFORM S1021-TRATTA-PMO       THRU S1021-EX

              WHEN 'PARAM-OGG'
               PERFORM S1022-TRATTA-POG       THRU S1022-EX

              WHEN 'SOPR-DI-GAR'
               PERFORM S1023-TRATTA-SPG       THRU S1023-EX

              WHEN 'PREST'
               PERFORM S1025-TRATTA-PRE       THRU S1025-EX

              WHEN 'IMPST-SOST'
               PERFORM S1026-TRATTA-ISO       THRU S1026-EX

              WHEN 'QUEST'
               PERFORM S1029-TRATTA-QUE       THRU S1029-EX

              WHEN 'DETT-QUEST'
               PERFORM S1030-TRATTA-DEQ       THRU S1030-EX

              WHEN 'RIS-DI-TRCH'
               PERFORM S1032-TRATTA-RST       THRU S1032-EX

              WHEN 'TIT-CONT'
               PERFORM S1033-TRATTA-TIT       THRU S1033-EX

              WHEN 'DETT-TIT-CONT'
               PERFORM S1034-TRATTA-DTC       THRU S1034-EX

              WHEN 'LIQ'
               PERFORM S1035-TRATTA-LQU       THRU S1035-EX

              WHEN 'BNFICR-LIQ'
               PERFORM S1036-TRATTA-BEL       THRU S1036-EX

              WHEN 'MOVI-FINRIO'
               PERFORM S1038-TRATTA-MFZ       THRU S1038-EX

              WHEN 'RICH-INVST-FND'
               PERFORM S1039-TRATTA-RIF       THRU S1039-EX

              WHEN 'TIT-RAT'
               PERFORM S1040-TRATTA-TDR       THRU S1040-EX

              WHEN 'DETT-TIT-DI-RAT'
               PERFORM S1041-TRATTA-DTR       THRU S1041-EX

              WHEN 'OGG-COLLG'
               PERFORM S1042-TRATTA-OCO       THRU S1042-EX

              WHEN 'RIP-COASS-AGE'
               PERFORM S1043-TRATTA-RCA       THRU S1043-EX

              WHEN 'TRCH-LIQ'
               PERFORM S1044-TRATTA-TLI       THRU S1044-EX

              WHEN 'DOCTO-LIQ'
RW16           PERFORM S1045-TRATTA-DLQ       THRU S1045-EX

              WHEN 'D-FORZ-LIQ'
RW16           PERFORM S1046-TRATTA-DFL       THRU S1046-EX

              WHEN 'GAR-LIQ'
               PERFORM S1047-TRATTA-GRL       THRU S1047-EX

              WHEN 'PERC-LIQ'
               PERFORM S1048-TRATTA-PLI       THRU S1048-EX

              WHEN 'PARAM-COMP'
               PERFORM S1049-TRATTA-PCO       THRU S1049-EX

              WHEN 'PARAM-DI-CALC'
               PERFORM S1050-TRATTA-PCA       THRU S1050-EX

              WHEN 'EST-TRCH-DI-GAR'
               PERFORM S1051-TRATTA-E12       THRU S1051-EX

              WHEN 'REINVST-POLI-LQ'
               PERFORM S1052-TRATTA-L30       THRU S1052-EX

              WHEN 'VINC-PEG'
               PERFORM S1053-TRATTA-L23       THRU S1053-EX

              WHEN 'PROV-DI-GAR'
               PERFORM S1054-TRATTA-PVT       THRU S1054-EX

ALEX1         WHEN 'AREA-FND-X-TRANCHE'
               PERFORM S1055-TRATTA-FXT       THRU S1055-EX

NEW           WHEN 'IMPST-BOLLO'
NEW            PERFORM S1057-TRATTA-P58       THRU S1057-EX

NEW           WHEN 'D-CRIST'
NEW            PERFORM S1058-TRATTA-P61       THRU S1058-EX

              WHEN 'EST-POLI-CPI-PR'
               PERFORM S1059-TRATTA-P67       THRU S1059-EX

              WHEN 'RICH-EST'
               PERFORM S1060-TRATTA-P01       THRU S1060-EX

16324         WHEN 'AREA-FND-X-CDG'
16324          IF WPOL-FL-POLI-IFP NOT = 'P'
16324             PERFORM S1061-TRATTA-FXC    THRU EX-S1061
16324          END-IF
      *
           END-EVALUATE.
      *
       U0010-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA AREA DATI DEFAULT ADESIONE
      *----------------------------------------------------------------*
       S1001A-TRATTA-DAD.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
           MOVE WDAD-DATI                    TO WKS-AREA-TABB-APPO
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
      *    CHIAMA MODULO DI CALCOLO
              SET WK-CALCOLI-SI              TO TRUE
      *
              PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
              PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
      *
              PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
      *
           END-IF.
      *
       S1001A-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  MOVIMENTO
      *----------------------------------------------------------------*
       S1001-TRATTA-MOV.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
           IF WMOV-DT-EFF IS NUMERIC
              IF WMOV-DT-EFF EQUAL ZEROES
                 MOVE 1                            TO WMOV-ELE-MOVI-MAX
                 MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WMOV-DT-EFF
              END-IF
           ELSE
              MOVE 1                               TO WMOV-ELE-MOVI-MAX
              MOVE IDSV0003-DATA-INIZIO-EFFETTO    TO WMOV-DT-EFF
           END-IF.

           MOVE WMOV-DATI                    TO WKS-AREA-TABB-APPO.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
      *
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           END-IF.
      *
       S1001-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  POLIZZA
      *----------------------------------------------------------------*
       S1002-TRATTA-POL.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
           MOVE  WPOL-DATI                   TO WKS-AREA-TABB-APPO.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                END-IF
                PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           END-IF.
      *
       S1002-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORA  ADESIONE
      *----------------------------------------------------------------*
       S1003-TRATTA-ADE.
      *
           MOVE SPACES                         TO WKS-AREA-TABB-APPO.
      *
           IF WADE-ELE-ADES-MAX = 0
              PERFORM U0013-LEGGI-ADE          THRU U0013-EX
              IF IDSV0003-SUCCESSFUL-RC
                 MOVE WADE-DATI(1)             TO WKS-AREA-TABB-APPO
              END-IF
           ELSE
              MOVE WADE-DATI(1)                TO WKS-AREA-TABB-APPO
           END-IF.

      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                END-IF
                PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           END-IF.
      *
       S1003-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  GARANZIA
      *----------------------------------------------------------------*
       S1004-TRATTA-GAR.
      *
           MOVE ZEROES                         TO C214-IX-TABB.
           MOVE SPACES                         TO WKS-AREA-TABB-APPO.
      *
           SET WK-COD-NON-TROVATO              TO TRUE
      *
2404       IF SKEDA-OPZIONE-NO

              IF GESTIONE-GARANZIE
                 PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
                   UNTIL IX-TAB-GRZ > WGRZ-ELE-GARANZIA-MAX
                      OR WK-COD-TROVATO
                      IF  WGRZ-COD-TARI(IX-TAB-GRZ) =
                          C216-COD-LIVELLO-T(IX-VAL-VAR)
                      AND NOT WGRZ-ST-DEL(IX-TAB-GRZ)
                          SET WK-COD-TROVATO         TO TRUE
                          MOVE IX-TAB-GRZ            TO C214-IX-TABB
                          MOVE WGRZ-DATI(IX-TAB-GRZ)
                            TO WKS-AREA-TABB-APPO
                      END-IF
                 END-PERFORM
              END-IF

2404       ELSE
              IF GESTIONE-GARANZIE
                 PERFORM VARYING IX-TAB-GOP FROM 1 BY 1
                   UNTIL IX-TAB-GOP > WGOP-ELE-GARANZIA-OPZ-MAX
                      OR WK-COD-TROVATO
                      IF  WGOP-COD-TARI(IX-TAB-GOP) =
                          C216-COD-LIVELLO-T(IX-VAL-VAR)
                      AND NOT WGOP-ST-DEL(IX-TAB-GOP)
                          SET WK-COD-TROVATO         TO TRUE
                          MOVE IX-TAB-GOP            TO C214-IX-TABB
                          MOVE WGOP-DATI(IX-TAB-GOP)
                            TO WKS-AREA-TABB-APPO
                      END-IF
                 END-PERFORM
              END-IF

2404       END-IF
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
              END-IF
           END-IF.
      *
       S1004-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  TRANCHE-DI-GARANZIA
      *----------------------------------------------------------------*
       S1005-TRATTA-TGA.
      *
           MOVE SPACES                           TO WKS-AREA-TABB-APPO.
           SET WK-CONTESTO-SI                    TO TRUE.
           MOVE ZEROES                           TO C214-IX-TABB.
      *
2404       IF SKEDA-OPZIONE-NO

              IF W1TGA-ELE-TRAN-MAX = 0
                 SET WK-CONTESTO-NO             TO TRUE
                 PERFORM U0004-LEGGI-TGA        THRU U0004-EX
                 IF IDSV0003-SUCCESSFUL-RC
                    MOVE WTGA-DATI(IX-TAB-TGA)  TO WKS-AREA-TABB-APPO
                    MOVE IX-TAB-TGA             TO C214-IX-TABB
                 END-IF
              ELSE
      *
                 SET WK-ID-NON-TROVATO       TO TRUE
                 PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
                   UNTIL (IX-TAB-TGA > W1TGA-ELE-TRAN-MAX)
                      OR WK-ID-TROVATO
                   IF  W1TGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
                       C216-ID-LIVELLO-T(IX-VAL-VAR)
                   AND NOT W1TGA-ST-DEL(IX-TAB-TGA)
                       SET WK-ID-TROVATO           TO TRUE
                       MOVE W1TGA-DATI(IX-TAB-TGA) TO WKS-AREA-TABB-APPO
                       MOVE W1TGA-DATI(IX-TAB-TGA) TO WTGA-DATI(1)
                       MOVE 1                      TO WTGA-ELE-TRAN-MAX
                                                      C214-IX-TABB
                   END-IF
                 END-PERFORM
              END-IF

2404       ELSE

2404          SET WK-ID-NON-TROVATO       TO TRUE
2404          PERFORM VARYING IX-TAB-TOP FROM 1 BY 1
2404            UNTIL (IX-TAB-TOP > WTOP-ELE-TRAN-OPZ-MAX)
2404               OR WK-ID-TROVATO
2404            IF  WTOP-ID-TRCH-DI-GAR(IX-TAB-TOP) =
2404                                       C216-ID-LIVELLO-T(IX-VAL-VAR)
2404            AND NOT WTOP-ST-DEL(IX-TAB-TOP)
2404                SET WK-ID-TROVATO           TO TRUE
2404                MOVE WTOP-DATI(IX-TAB-TOP)  TO WKS-AREA-TABB-APPO
2404                MOVE WTOP-DATI(IX-TAB-TOP)  TO WTGA-DATI(1)
2404                MOVE 1                      TO WTGA-ELE-TRAN-MAX
2404                                               C214-IX-TABB
2404            END-IF
2404          END-PERFORM

2404       END-IF.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
              END-IF
           END-IF.
      *
           IF WK-CONTESTO-NO
              INITIALIZE WTGA-AREA-TRANCHE
           END-IF.
      *
       S1005-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  BNFICIARIO
      *----------------------------------------------------------------*
       S1008-TRATTA-BEP.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
           MOVE WBEP-DATI(1)                TO WKS-AREA-TABB-APPO.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
              END-IF
           END-IF.
      *
       S1008-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  STRATEGIA_DI_INVESTIMENTO
      *----------------------------------------------------------------*
       S1009-TRATTA-SDI.
      *
           MOVE SPACES                      TO WKS-AREA-TABB-APPO.
      *
           MOVE WSDI-DATI(1)                TO WKS-AREA-TABB-APPO.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
              END-IF
           END-IF.
      *
       S1009-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  ASSET_ALLOCATION
      *----------------------------------------------------------------*
       S1010-TRATTA-ALL.
      *
           CONTINUE.
      *
       S1010-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA DATI_COLLETTIVA
      *----------------------------------------------------------------*
       S1012-TRATTA-DCO.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
           MOVE WDCO-DATI                    TO WKS-AREA-TABB-APPO.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
              END-IF
           END-IF.
      *
       S1012-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  DATO_FISCALE_ADESIONE
      *----------------------------------------------------------------*
       S1015-TRATTA-DFA.
      *
           MOVE SPACES                         TO WKS-AREA-TABB-APPO.
      *
           IF WDFA-ELE-FISC-ADES-MAX = 0
              PERFORM U0017-LEGGI-DFA          THRU U0017-EX
              IF IDSV0003-SUCCESSFUL-RC
                 MOVE WDFA-DATI                TO WKS-AREA-TABB-APPO
              END-IF
           ELSE
              MOVE WDFA-DATI                   TO WKS-AREA-TABB-APPO
           END-IF.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
               IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
              END-IF
           END-IF.
      *
       S1015-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  PARAMETRO MOVIMENTO
      *----------------------------------------------------------------*
       S1021-TRATTA-PMO.
      *
           INITIALIZE LDBV1471.
      *
           MOVE SPACES                        TO WKS-AREA-TABB-APPO.
           MOVE ZEROES                        TO C214-IX-TABB.
      *
           IF V1391-WHERE-CONDITION(IX-COD-MVV)(1:4) is numeric
              MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(1:4)
                TO LDBV1471-TP-MOVI-01
           END-IF.
           IF V1391-WHERE-CONDITION(IX-COD-MVV)(6:4) IS NUMERIC
              MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(6:4)
                TO LDBV1471-TP-MOVI-02
           END-IF.
           IF V1391-WHERE-CONDITION(IX-COD-MVV)(11:4) IS NUMERIC
              MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(11:4)
                TO LDBV1471-TP-MOVI-03
           END-IF.
      *
           SET WK-ID-COD-NON-TROVATO        TO TRUE.

           PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
                     UNTIL IX-TAB-PMO > WPMO-ELE-PARAM-MOV-MAX
                        OR WK-ID-COD-TROVATO
                 IF LDBV1471-TP-MOVI-01 = WPMO-TP-MOVI(IX-TAB-PMO)
                 OR LDBV1471-TP-MOVI-02 = WPMO-TP-MOVI(IX-TAB-PMO)
                 OR LDBV1471-TP-MOVI-03 = WPMO-TP-MOVI(IX-TAB-PMO)
                    SET WK-ID-COD-TROVATO        TO TRUE
                    MOVE WPMO-DATI(IX-TAB-PMO)   TO WKS-AREA-TABB-APPO
                    MOVE IX-TAB-PMO              TO C214-IX-TABB
                 END-IF

           END-PERFORM.

           IF WK-ID-COD-NON-TROVATO
              PERFORM U0014-LEGGI-PMO      THRU U0014-EX
           END-IF.

           PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
             UNTIL IX-TAB-PMO > WPMO-ELE-PARAM-MOV-MAX
              OR WK-ID-COD-TROVATO
              IF LDBV1471-TP-MOVI-01 = WPMO-TP-MOVI(IX-TAB-PMO)
              OR LDBV1471-TP-MOVI-02 = WPMO-TP-MOVI(IX-TAB-PMO)
              OR LDBV1471-TP-MOVI-03 = WPMO-TP-MOVI(IX-TAB-PMO)
                 SET WK-ID-COD-TROVATO        TO TRUE
                 MOVE WPMO-DATI(IX-TAB-PMO)   TO WKS-AREA-TABB-APPO
                 MOVE IX-TAB-PMO              TO C214-IX-TABB
              END-IF
      *
           END-PERFORM.

44943      IF WK-ID-COD-NON-TROVATO
44943         MOVE WPMO-DATI(1)   TO WKS-AREA-TABB-APPO
44943      END-IF.

           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
              END-IF
           END-IF.
      *
       S1021-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  PARAMETRO OGGETTO
      *----------------------------------------------------------------*
       S1022-TRATTA-POG.
      *
           MOVE SPACES                         TO WKS-AREA-TABB-APPO
                                                  WK-PARAM-OGG-APPO.
           SET WK-ID-COD-NON-TROVATO           TO TRUE.
           SET WK-CONTESTO-NO-POG              TO TRUE.
           MOVE ZEROES                         TO C214-IX-TABB.
      *
           IF GESTIONE-PRODOTTO
              MOVE C216-ID-POL-P(IX-VAL-VAR)
                TO WK-ID-OGG
           ELSE
              MOVE C216-ID-GAR-T(IX-VAL-VAR)
                TO WK-ID-OGG
           END-IF
           MOVE WK-APPO-TP-OGG
             TO WK-TP-OGG

           IF WPOG-ELE-PARAM-OGG-MAX = 0
29023         MOVE 0                           TO IX-TAB-POG
              IF GESTIONE-PRODOTTO
              AND C216-TP-LIVELLO-P(IX-VAL-VAR) = 'P'
                 MOVE WPOL-ID-POLI             TO POG-ID-OGG
                 MOVE 'PO'                     TO POG-TP-OGG
                 PERFORM U0022-LEGGI-POG       THRU U0022-EX
      *
                 IF IDSV0003-SUCCESSFUL-RC
                    PERFORM X0001-MATCH-POG    THRU X0001-EX
                 END-IF
              ELSE
                 MOVE WGRZ-ID-GAR(IX-GUIDA-GRZ)   TO POG-ID-OGG
                 MOVE 'GA'                        TO POG-TP-OGG
                 PERFORM U0022-LEGGI-POG          THRU U0022-EX
      *
                 IF IDSV0003-SUCCESSFUL-RC
                    PERFORM X0001-MATCH-POG    THRU X0001-EX
                 END-IF

                 IF WK-ID-COD-NON-TROVATO
                    MOVE WPOL-ID-POLI          TO POG-ID-OGG
                    MOVE 'PO'                  TO POG-TP-OGG
                    PERFORM U0022-LEGGI-POG    THRU U0022-EX

                    IF IDSV0003-SUCCESSFUL-RC
                       MOVE WPOL-ID-POLI
                         TO WK-ID-OGG
                       MOVE 'PO'
                         TO WK-TP-OGG
                       PERFORM X0001-MATCH-POG THRU X0001-EX
                    END-IF
                 END-IF
              END-IF
           ELSE
              SET WK-CONTESTO-SI-POG               TO TRUE
              MOVE V1391-TIPO-OGGETTO(IX-COD-MVV)
                TO WK-TP-OGG
              EVALUATE WK-TP-OGG
                  WHEN 'PO'
                    MOVE WPOL-ID-POLI               TO WK-ID-OGG
                  WHEN 'AD'
                    MOVE WADE-ID-ADES(1)            TO WK-ID-OGG
                  WHEN 'GA'
                    MOVE WGRZ-ID-GAR(IX-GUIDA-GRZ)  TO WK-ID-OGG
              END-EVALUATE
              PERFORM X0001-MATCH-POG           THRU X0001-EX
53263         IF WK-ID-COD-NON-TROVATO
53263            PERFORM LEGGI-COD-PARAM        THRU LEGGI-COD-PARAM-EX
53263            IF IDSV0003-SUCCESSFUL-RC
53263               PERFORM X0001-MATCH-POG    THRU X0001-EX
53263            END-IF
53263         END-IF
           END-IF.

           IF WK-ID-COD-NON-TROVATO
           AND WK-CONTESTO-SI-POG
              PERFORM X0002-MATCH-POG-TP-OGG    THRU X0002-EX

              IF WK-ID-COD-TROVATO
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              ELSE

                 MOVE WPOG-ELE-PARAM-OGG-MAX    TO IX-TAB-POG

                 EVALUATE V1391-TIPO-OGGETTO(IX-COD-MVV)
                    WHEN 'PO'
                      MOVE WPOL-ID-POLI             TO POG-ID-OGG
                      MOVE 'PO'                     TO POG-TP-OGG
                    WHEN 'AD'
                      MOVE WADE-ID-ADES(1)          TO POG-ID-OGG
                      MOVE 'AD'                     TO POG-TP-OGG
                    WHEN 'GA'
                      MOVE WGRZ-ID-GAR(IX-GUIDA-GRZ)  TO POG-ID-OGG
                      MOVE 'GA'                       TO POG-TP-OGG
                 END-EVALUATE

                 MOVE WPOG-AREA-PARAM-OGG          TO WK-PARAM-OGG-APPO

                 PERFORM U0022-LEGGI-POG    THRU U0022-EX

                 IF IDSV0003-SUCCESSFUL-RC
                    PERFORM X0001-MATCH-POG    THRU X0001-EX
                 END-IF

                 IF NOT IDSV0003-INVALID-CONVERSION
                    PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
                 END-IF

              END-IF
           END-IF.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
      *
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
              END-IF
           END-IF.
      *
           IF WK-PARAM-OGG-APPO NOT = SPACES
              MOVE WK-PARAM-OGG-APPO
                TO WPOG-AREA-PARAM-OGG
           END-IF.
      *
       S1022-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LEGGE IL CODICE PARAMETRO SULLA TABELLA PARAM_OGG
      *----------------------------------------------------------------*
53263  LEGGI-COD-PARAM.

           INITIALIZE  AREA-LDBV1131
                       PARAM-OGG.
           MOVE 160                             TO IX-APPO-OVERF.
           SET  WCOM-OVERFLOW-NO                TO TRUE.
           MOVE WPOG-ELE-PARAM-OGG-MAX          TO IX-TAB-UNIV.

           SET IDSV0003-SUCCESSFUL-SQL          TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC           TO TRUE.
      *
           SET IDSV0003-TRATT-X-COMPETENZA      TO TRUE.
           SET IDSV0003-SELECT                  TO TRUE.
           SET IDSV0003-WHERE-CONDITION         TO TRUE.

           MOVE WK-ID-OGG                       TO LDBV1131-ID-OGG
           MOVE WK-TP-OGG                       TO LDBV1131-TP-OGG
           MOVE V1391-COD-PARAMETRO(IX-COD-MVV) TO LDBV1131-COD-PARAM
           MOVE AREA-LDBV1131        TO IDSV0003-BUFFER-WHERE-COND
      *
           MOVE 'LDBS1130'                      TO PGM-CHIAMATO.
           CALL PGM-CHIAMATO                    USING IDSV0003
                                                      PARAM-OGG
           ON EXCEPTION
              MOVE PGM-CHIAMATO
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-LDBS1130 ERRORE CHIAMATA - LDBS1130'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-SUCCESSFUL-SQL
                 ADD 1 TO IX-TAB-UNIV
                 IF IX-TAB-UNIV > IX-APPO-OVERF
                    SET WCOM-OVERFLOW-YES   TO TRUE
                 ELSE
                    MOVE WPOG-ELE-PARAM-OGG-MAX  TO IX-TAB-POG
16916 *             PERFORM S1500-VALOUTORIZZA-DCLGEN
16916 *                THRU S1500-EX
                    MOVE V1391-COD-STR-DATO-PTF(IX-COD-MVV)
                      TO WKS-NOME-TABELLA
                    PERFORM S1600-VALORIZZA-OUTPUT
                       THRU S1600-EX
                    MOVE IX-TAB-POG   TO WPOG-ELE-PARAM-OGG-MAX
                 END-IF
              ELSE
                 IF IDSV0003-NOT-FOUND
                    SET IDSV0003-FIELD-NOT-VALUED     TO TRUE
                    MOVE PGM-CHIAMATO
                      TO IDSV0003-COD-SERVIZIO-BE
                    STRING PGM-CHIAMATO       ';'
                           IDSV0003-RETURN-CODE ';'
                           IDSV0003-SQLCODE DELIMITED BY SIZE
                           INTO IDSV0003-DESCRIZ-ERR-DB2
                    END-STRING
                 ELSE
                    SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                    MOVE PGM-CHIAMATO
                      TO IDSV0003-COD-SERVIZIO-BE
                    STRING PGM-CHIAMATO       ';'
                        IDSV0003-RETURN-CODE ';'
                        IDSV0003-SQLCODE
                        DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    END-STRING
                 END-IF
              END-IF
           ELSE
              SET IDSV0003-INVALID-OPER TO TRUE
              MOVE PGM-CHIAMATO
                TO IDSV0003-COD-SERVIZIO-BE
              STRING PGM-CHIAMATO           ';'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.

       LEGGI-COD-PARAM-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ESEGUE IL PRIMO MATCH PER VERIFICARE IL PARAMETRO IN INPUT
      *    LEGATO ALL'OGGETTO DI BUSINESS
      *----------------------------------------------------------------*
       X0001-MATCH-POG.
      *
           PERFORM VARYING IX-TAB-POG FROM 1 BY 1
                     UNTIL IX-TAB-POG > WPOG-ELE-PARAM-OGG-MAX
                        OR WK-ID-COD-TROVATO
      *
                IF  V1391-COD-PARAMETRO(IX-COD-MVV) =
                    WPOG-COD-PARAM(IX-TAB-POG)
                AND WK-ID-OGG = WPOG-ID-OGG(IX-TAB-POG)
                AND WK-TP-OGG = WPOG-TP-OGG(IX-TAB-POG)

                   SET WK-ID-COD-TROVATO        TO TRUE
                   MOVE WPOG-DATI(IX-TAB-POG)
                      TO WKS-AREA-TABB-APPO
                   MOVE IX-TAB-POG              TO C214-IX-TABB

                END-IF
      *
           END-PERFORM.
      *
       X0001-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ESEGUE IL PRIMO MATCH PER VERIFICARE IL PARAMETRO IN INPUT
      *    LEGATO ALLA POLI
      *----------------------------------------------------------------*
       X0002-MATCH-POG-TP-OGG.
      *
           PERFORM VARYING IX-TAB-POG FROM 1 BY 1
             UNTIL IX-TAB-POG > WPOG-ELE-PARAM-OGG-MAX
                OR WK-ID-COD-TROVATO
                IF V1391-TIPO-OGGETTO(IX-COD-MVV) =
                   WPOG-TP-OGG(IX-TAB-POG)
                   SET WK-ID-COD-TROVATO        TO TRUE
                END-IF
           END-PERFORM.
      *
       X0002-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  SOVRAPREMIO_DI_GARANZIA
      *----------------------------------------------------------------*
       S1023-TRATTA-SPG.
      *
            MOVE SPACES                        TO WKS-AREA-TABB-APPO.
            MOVE ZEROES                        TO C214-IX-TABB.
      *
            IF WSPG-ELE-SOPRAP-GAR-MAX = 0
              PERFORM U0006-LEGGI-SPG          THRU U0006-EX
              IF IDSV0003-SUCCESSFUL-RC
                MOVE WSPG-DATI(IX-TAB-SPG)     TO WKS-AREA-TABB-APPO
                MOVE IX-TAB-SPG                TO C214-IX-TABB
              END-IF
            ELSE
              SET WK-LETTO-NO                  TO TRUE
      *
              MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(1:2)
                                               TO WKS-COD-SOPR
              IF GESTIONE-GARANZIE
                 PERFORM VARYING IX-TAB-SPG FROM 1 BY 1
                   UNTIL IX-TAB-SPG > WSPG-ELE-SOPRAP-GAR-MAX
                      OR WK-LETTO-SI

                      IF C216-ID-GAR-T(IX-VAL-VAR)
                       = WSPG-ID-GAR(IX-TAB-SPG)
                      AND WSPG-COD-SOPR(IX-TAB-SPG)
                       = WKS-COD-SOPR
                         SET WK-LETTO-SI              TO TRUE

                         IF WSPG-FL-ESCL-SOPR(IX-TAB-SPG) = 'N'
                            MOVE WSPG-DATI(IX-TAB-SPG)
                              TO WKS-AREA-TABB-APPO
                            MOVE IX-TAB-SPG
                              TO C214-IX-TABB
                         END-IF
                      END-IF
                 END-PERFORM
              END-IF
      *
              IF WK-LETTO-NO
                 PERFORM U0006-LEGGI-SPG           THRU U0006-EX
                 IF IDSV0003-SUCCESSFUL-RC
                    MOVE WSPG-DATI(IX-TAB-SPG)     TO WKS-AREA-TABB-APPO
                    MOVE IX-TAB-SPG                TO C214-IX-TABB
                 END-IF
              END-IF
            END-IF.
      *
            IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
               IF WKS-AREA-TABB-APPO NOT = SPACES
                  PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
               ELSE
                  IF NOT IDSV0003-INVALID-CONVERSION
                     SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                  END-IF
                  PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
               END-IF
            ELSE
               IF WKS-AREA-TABB-APPO NOT = SPACES
                  IF IX-COD-MVV = 1
                     PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                     PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                     PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                  ELSE
                     PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                  END-IF
               ELSE
                  IF NOT IDSV0003-INVALID-CONVERSION
                     SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                  END-IF
                  PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
               END-IF
            END-IF.
      *
       S1023-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  PRESTITO
      *----------------------------------------------------------------*
       S1025-TRATTA-PRE.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
           SET WK-LETTO-NO                   TO TRUE.
      *
           IF WPRE-ELE-PRESTITI-MAX = ZEROES

              PERFORM U0037-LEGGI-PRE        THRU U0037-EX
              IF IDSV0003-SUCCESSFUL-RC
                MOVE WPRE-DATI(IX-TAB-PRE)    TO WKS-AREA-TABB-APPO
                MOVE IX-TAB-PRE               TO C214-IX-TABB
              END-IF

           ELSE

              PERFORM VARYING IX-TAB-PRE FROM 1 BY 1
                UNTIL IX-TAB-PRE > WPRE-ELE-PRESTITI-MAX
                   OR WK-LETTO-SI

                   IF WADE-ID-ADES(1) = WPRE-ID-OGG(IX-TAB-PRE)
                   AND WPRE-TP-OGG(IX-TAB-PRE) = 'AD'
                      SET WK-LETTO-SI              TO TRUE
                      MOVE WPRE-DATI(IX-TAB-PRE)   TO WKS-AREA-TABB-APPO
                      MOVE IX-TAB-PRE              TO C214-IX-TABB
                   END-IF

              END-PERFORM

           END-IF.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
              END-IF
           END-IF.
      *
       S1025-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA IMPOSTA SOSTITUTIVA
      *----------------------------------------------------------------*
       S1026-TRATTA-ISO.
      *
23165       SET WK-CONTESTO-SI                  TO TRUE.
            MOVE SPACES                        TO WKS-AREA-TABB-APPO.
23165       MOVE ZEROES                         TO C214-IX-TABB.
      *
            IF WISO-ELE-IMP-SOST-MAX = 0
23165          SET WK-CONTESTO-NO               TO TRUE
               PERFORM U0015-LEGGI-ISO          THRU U0015-EX
               IF IDSV0003-SUCCESSFUL-RC
                  MOVE WISO-DATI(IX-TAB-ISO)     TO WKS-AREA-TABB-APPO
                  MOVE IX-TAB-ISO                TO C214-IX-TABB
               END-IF
            ELSE
              SET WK-LETTO-NO                  TO TRUE
      *
              PERFORM VARYING IX-TAB-ISO FROM 1 BY 1
                UNTIL IX-TAB-ISO > WISO-ELE-IMP-SOST-MAX
                   OR WK-LETTO-SI
23165              IF V1391-TIPO-OGGETTO(IX-COD-MVV) = 'TG'
23165                 IF WISO-ID-OGG(IX-TAB-ISO) =
23165                     C216-ID-LIVELLO-T(IX-VAL-VAR) AND
23165                    WISO-TP-OGG(IX-TAB-ISO) =
23165                     V1391-TIPO-OGGETTO(IX-COD-MVV)

23165                    SET WK-LETTO-SI       TO TRUE
23165                    MOVE WISO-DATI(IX-TAB-ISO)
23165                                          TO WKS-AREA-TABB-APPO
23165                    MOVE IX-TAB-ISO       TO C214-IX-TABB

23165                 END-IF
23165              ELSE
                   IF WISO-ID-OGG(IX-TAB-ISO) = WADE-ID-ADES(1)
                   AND WISO-TP-OGG(IX-TAB-ISO) = 'AD'
                      SET WK-LETTO-SI              TO TRUE
                         MOVE WISO-DATI(IX-TAB-ISO)
                                               TO WKS-AREA-TABB-APPO
                      MOVE IX-TAB-ISO              TO C214-IX-TABB
                   END-IF
23165              END-IF
              END-PERFORM
      *
            END-IF.
      *
            IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
               IF WKS-AREA-TABB-APPO NOT = SPACES
                  PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
               ELSE
                  IF NOT IDSV0003-INVALID-CONVERSION
                     SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                  END-IF
                  PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
               END-IF
            ELSE
               IF WKS-AREA-TABB-APPO NOT = SPACES
                  IF IX-COD-MVV = 1
                     PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                     PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                     PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                  ELSE
                     PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                  END-IF
               ELSE
                  IF NOT IDSV0003-INVALID-CONVERSION
                     SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                  END-IF
                  PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
               END-IF
            END-IF.

23165       IF WK-CONTESTO-NO
23165          INITIALIZE WISO-AREA-IMPOSTA-SOST
23165       END-IF.
      *
       S1026-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA RAPPORTO_ANAGRAFICO
      *----------------------------------------------------------------*
       S1027-TRATTA-RAN.
      *
           INITIALIZE                           RAPP-ANA.
           MOVE SPACES                          TO WKS-AREA-TABB-APPO.
      *
           MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(1:5)
                                                TO WK-APP-ASSICURATO.
           IF WRAN-ELE-RAPP-ANAG-MAX = 0
              EVALUATE TRUE
                 WHEN TP-OGG-PO
                 WHEN TP-DEC-PO
                   MOVE WPOL-ID-POLI                 TO RAN-ID-OGG
                   MOVE 'PO'                         TO RAN-TP-OGG
                   PERFORM U00A1-LEGGI-RAN2          THRU U00A1-EX
                 WHEN TP-OGG-AD
                 WHEN TP-DEC-AD
                   MOVE WADE-ID-ADES(1)              TO RAN-ID-OGG
                   MOVE 'AD'                         TO RAN-TP-OGG
                   PERFORM U00A1-LEGGI-RAN2          THRU U00A1-EX
                 WHEN OTHER
                   PERFORM U0001-LEGGI-RAN           THRU U0001-EX
              END-EVALUATE
              IF IDSV0003-SUCCESSFUL-RC
                 MOVE WRAN-DATI(IX-TAB-RAN)     TO WKS-AREA-TABB-APPO
                 MOVE IX-TAB-RAN                TO C214-IX-TABB
              END-IF
           ELSE
              EVALUATE TRUE
                 WHEN PRIMO-ASS
                 WHEN SECONDO-ASS
                 WHEN TERZO-ASS
                   PERFORM S4027-GEST-RAN-ASS        THRU S4027-EX
                 WHEN OTHER
                   IF  GESTIONE-GARANZIE
                   AND C216-TP-LIVELLO-T(IX-VAL-VAR) = 'G'
                      PERFORM S2027-RAN-GAR          THRU S2027-EX
                   ELSE
                      IF WK-APP-ASSICURATO = 'CO'
                         PERFORM S3027A-RAN-POL-CO   THRU S3027A-EX
                      ELSE
                         PERFORM S3027-RAN-POL       THRU S3027-EX
                      END-IF
                   END-IF
              END-EVALUATE
           END-IF.

           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                END-IF
                PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                END-IF
                PERFORM S9004-GESTIONE-ERRORE      THRU S9004-EX
              END-IF
           END-IF.
      *
       S1027-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  RAPP ANA GAR
      *----------------------------------------------------------------*
       S2027-RAN-GAR.
      *
           SET WK-LETTO-NO                TO TRUE.
      *
           PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
             UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX
                OR WK-LETTO-SI
                IF WRAN-TP-OGG(IX-TAB-RAN) = 'GA'
                  IF C216-ID-GAR-T(IX-VAL-VAR)
                   = WRAN-ID-OGG(IX-TAB-RAN)
                     SET WK-LETTO-SI              TO TRUE
                     MOVE WRAN-DATI(IX-TAB-RAN)
                       TO WKS-AREA-TABB-APPO
                     MOVE IX-TAB-RAN
                       TO C214-IX-TABB
                  END-IF
                END-IF
           END-PERFORM.
      *
           IF WK-LETTO-NO
              PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
                UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX
                   OR WK-LETTO-SI
                   IF WRAN-TP-OGG(IX-TAB-RAN) = 'AD'
                     IF WADE-ID-ADES(1)
                      = WRAN-ID-OGG(IX-TAB-RAN)
                        SET WK-LETTO-SI              TO TRUE
                        MOVE WRAN-DATI(IX-TAB-RAN)
                          TO WKS-AREA-TABB-APPO
                        MOVE IX-TAB-RAN
                          TO C214-IX-TABB
                     END-IF
                   END-IF
              END-PERFORM
           END-IF.
      *
       S2027-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  RAPP ANA GAR
      *----------------------------------------------------------------*
       S3027-RAN-POL.
      *
           SET WK-LETTO-NO                TO TRUE.
      *
           PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
             UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX
                OR WK-LETTO-SI
                IF WRAN-TP-OGG(IX-TAB-RAN) = 'PO'
                  IF C216-ID-POL-P(IX-VAL-VAR)
                   = WRAN-ID-OGG(IX-TAB-RAN)
                     SET WK-LETTO-SI              TO TRUE
                     MOVE WRAN-DATI(IX-TAB-RAN)
                       TO WKS-AREA-TABB-APPO
                     MOVE IX-TAB-RAN
                       TO C214-IX-TABB
                  END-IF
                END-IF
           END-PERFORM.
      *
       S3027-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LEGGI RAN LEGATI AL CONTRAENTE
      *----------------------------------------------------------------*
       S3027A-RAN-POL-CO.
      *
           SET WK-LETTO-NO                TO TRUE.
      *
           PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
             UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX
               OR WK-LETTO-SI
               IF WRAN-TP-OGG(IX-TAB-RAN) = 'PO'
               AND C216-ID-POL-P(IX-VAL-VAR) = WRAN-ID-OGG(IX-TAB-RAN)
               AND WRAN-TP-RAPP-ANA(IX-TAB-RAN) = 'CO'
                   SET WK-LETTO-SI              TO TRUE
                   MOVE WRAN-DATI(IX-TAB-RAN)
                     TO WKS-AREA-TABB-APPO
                   MOVE IX-TAB-RAN
                     TO C214-IX-TABB
               END-IF
           END-PERFORM.
      *
       S3027A-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  RAPP ANA PER ASSICUTATI
      *----------------------------------------------------------------*
       S4027-GEST-RAN-ASS.
      *
           IF IX-GUIDA-GRZ = ZERO
              PERFORM S9005-ERRORE-LIV-VAR     THRU S9005-EX
           ELSE
              EVALUATE TRUE
                WHEN PRIMO-ASS
                  IF WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ)
                       TO RAN-ID-RAPP-ANA
                  END-IF

                WHEN SECONDO-ASS
                  IF WGRZ-ID-2O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-2O-ASSTO(IX-GUIDA-GRZ)
                       TO RAN-ID-RAPP-ANA
                  END-IF

                WHEN TERZO-ASS
                  IF WGRZ-ID-3O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-3O-ASSTO(IX-GUIDA-GRZ)
                       TO RAN-ID-RAPP-ANA
                  END-IF
              END-EVALUATE
      *
              SET WK-LETTO-NO                TO TRUE
      *
              IF RAN-ID-RAPP-ANA IS NUMERIC
                 IF RAN-ID-RAPP-ANA GREATER ZERO
                    PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
                      UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX
                         OR WK-LETTO-SI
                         IF WRAN-ID-RAPP-ANA(IX-TAB-RAN)
                           = RAN-ID-RAPP-ANA
                            SET WK-LETTO-SI                TO TRUE
                            MOVE WRAN-DATI(IX-TAB-RAN)
                              TO WKS-AREA-TABB-APPO
                            MOVE IX-TAB-RAN             TO C214-IX-TABB
                         END-IF
                    END-PERFORM
                    IF WK-LETTO-NO
                       MOVE WRAN-ELE-RAPP-ANAG-MAX  TO IX-TAB-RAN
                       PERFORM U0001-LEGGI-RAN      THRU U0001-EX
                       IF IDSV0003-SUCCESSFUL-RC
                          MOVE WRAN-DATI(IX-TAB-RAN)
                            TO WKS-AREA-TABB-APPO
                          MOVE IX-TAB-RAN           TO C214-IX-TABB
                       END-IF
                    END-IF
                 END-IF
              END-IF
           END-IF.
      *
       S4027-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  RAPPORTO_DI_RETE
      *----------------------------------------------------------------*
       S1028-TRATTA-RRE.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
           IF WRRE-ELE-RAPP-RETE-MAX = 0
              PERFORM U0005-LEGGI-RRE        THRU U0005-EX
           END-IF.
      *
           PERFORM VARYING IX-TAB-RRE FROM 1 BY 1
             UNTIL IX-TAB-RRE > WRRE-ELE-RAPP-RETE-MAX
              IF WRRE-TP-RETE(IX-TAB-RRE) = 'GE'
                  MOVE WRRE-DATI(IX-TAB-RRE)  TO WKS-AREA-TABB-APPO
                  MOVE IX-TAB-RRE             TO C214-IX-TABB
              END-IF
           END-PERFORM.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
              END-IF
           END-IF.
      *
       S1028-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  QUESTIONARIO
      *----------------------------------------------------------------*
       S1029-TRATTA-QUE.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
           IF WQUE-ELE-QUEST-MAX = 0
      *--> LETTURA A DB
              PERFORM U0020-LEGGI-QUE-DB     THRU U0020-EX
              IF IDSO0011-SUCCESSFUL-SQL
                 MOVE  WQUE-DATI(IX-TAB-QUE) TO WKS-AREA-TABB-APPO
                 MOVE  IX-TAB-QUE            TO IX-APP-QUE
              END-IF
           ELSE
      *--> LETTURA A CONTESTO
              PERFORM U0021-LEGGI-QUE-CONT   THRU U0021-EX
              IF WK-LETTO-SI
                 MOVE  WQUE-DATI(IX-APP-QUE) TO WKS-AREA-TABB-APPO
              END-IF
           END-IF.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    MOVE IX-APP-QUE             TO C214-IX-TABB
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    MOVE IX-APP-QUE             TO C214-IX-TABB
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
              END-IF
           END-IF.
      *
       S1029-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  DETTAGLIO_QUESTIONARIO
      *----------------------------------------------------------------*
       S1030-TRATTA-DEQ.
      *
           SET WK-DEROGA-NO                  TO TRUE.
           SET WK-ESEGUI-CALL-SI             TO TRUE.
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
           MOVE V1391-WHERE-CONDITION(IX-COD-MVV)
                                             TO IVVC0221-DETT-QUEST-WC
                                                C214-WHERE-COND.
      *
           SET WK-OUT-SCRITTO-NO             TO TRUE.
      *
           PERFORM VARYING IX-DOM FROM 1 BY 1
             UNTIL IX-DOM > IVVC0221-MAX-DOMANDA
                OR WK-DEROGA-SI
                OR WK-OUT-SCRITTO-SI
      *
             IF WDEQ-ELE-DETT-QUEST-MAX = 0
                SET WK-CONTESTO-NO                  TO TRUE
      *--> LETTURA A DB
                IF IVVC0221-INDIVIDUALE
                   PERFORM U0026-LEGGI-DEQ-DB-IN    THRU U0026-EX
                ELSE
                   PERFORM U0018-LEGGI-DEQ-DB-CO    THRU U0018-EX
                END-IF
                IF WK-LETTO-SI
                AND IDSO0011-SUCCESSFUL-SQL
                AND IDSV0003-SUCCESSFUL-RC
                   MOVE  WDEQ-DATI(IX-TAB-DEQ) TO WKS-AREA-TABB-APPO
                   MOVE  IX-TAB-QUE            TO IX-APP-QUE
                END-IF
             ELSE
      *--> LETTURA A CONTESTO
                SET WK-CONTESTO-SI             TO TRUE
                IF IVVC0221-INDIVIDUALE
                   PERFORM U0027-LEGGI-DEQ-CONT-IN   THRU U0027-EX
                ELSE
                   PERFORM U0019-LEGGI-DEQ-CONT-CO   THRU U0019-EX
                END-IF
                IF WK-LETTO-SI
                AND IDSV0003-SUCCESSFUL-RC
                   MOVE  WDEQ-DATI(IX-APP-DEQ) TO WKS-AREA-TABB-APPO
                END-IF
             END-IF
      *
             IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
                IF WKS-AREA-TABB-APPO NOT = SPACES
                   PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                ELSE
                   IF NOT IDSV0003-INVALID-CONVERSION
                      SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                   END-IF
                   PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
                END-IF
             ELSE
                IF WK-ESEGUI-CALL-SI
                   IF WKS-AREA-TABB-APPO NOT = SPACES
                      IF IX-COD-MVV = 1
                         MOVE IX-APP-DEQ                TO C214-IX-TABB
                         PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                         PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                         PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                      ELSE
                         MOVE IX-APP-DEQ                TO C214-IX-TABB
                         PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                      END-IF
                   ELSE
                      IF NOT IDSV0003-INVALID-CONVERSION
                         SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                      END-IF
                      PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
                   END-IF
                END-IF
             END-IF
           END-PERFORM.
      *
           IF IDSV0003-FIELD-NOT-VALUED
              PERFORM S9004-GESTIONE-ERRORE          THRU S9004-EX
           END-IF.
      *
       S1030-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  RIPARTIZIONE_FONDO_INVESTIMENTO
      *----------------------------------------------------------------*
       S1031-GEST-VARIABILI-QUE.
      *
           SET WK-ESEGUI-CALL-NO          TO TRUE
      *
           IF IX-COD-MVV = 1
              MOVE  WDEQ-DATI(IX-APP-DEQ) TO WKS-AREA-TABB-APPO
              MOVE IX-APP-DEQ             TO C214-IX-TABB
              PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
              PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
              IF C214-VAL-IMP-O = 1
              AND WK-OUT-SCRITTO-NO
                 SET WK-DEROGA-SI            TO TRUE
                 SET WK-OUT-SCRITTO-SI       TO TRUE
                 PERFORM S0099-VALORIZZA-OUTPUT
                    THRU S0099-EX
              END-IF
           ELSE
              MOVE IX-APP-DEQ             TO C214-IX-TABB
              PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
           END-IF.
      *
           IF WK-DEROGA-NO
           AND IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
           AND IX-DOM = IVVC0221-MAX-DOMANDA
           AND WK-OUT-SCRITTO-NO
              SET WK-OUT-SCRITTO-SI          TO TRUE
              PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
           END-IF.
      *
       S1031-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  RISERVA_DI_TRANCHE
      *----------------------------------------------------------------*
       S1032-TRATTA-RST.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
           IF WRST-ELE-RST-MAX = 0
      *--> LETTURA A DB
              PERFORM U0025-LEGGI-RST-DB     THRU U0025-EX
              IF IDSO0011-SUCCESSFUL-SQL
                 MOVE  WRST-DATI(IX-TAB-RST) TO WKS-AREA-TABB-APPO
                 MOVE  IX-TAB-RST            TO C214-IX-TABB
              END-IF
           ELSE
      *--> LETTURA A CONTESTO
              SET WK-ID-NON-TROVATO       TO TRUE
              PERFORM VARYING IX-TAB-RST FROM 1 BY 1
                UNTIL (IX-TAB-RST > WRST-ELE-RST-MAX)
                   OR WK-ID-TROVATO
                IF  WRST-ID-TRCH-DI-GAR(IX-TAB-RST) =
                    C216-ID-LIVELLO-T(IX-VAL-VAR)
                AND NOT WRST-ST-DEL(IX-TAB-RST)
                    SET WK-ID-TROVATO             TO TRUE
                    MOVE WRST-DATI(IX-TAB-RST)    TO WKS-AREA-TABB-APPO
                    MOVE IX-TAB-RST               TO C214-IX-TABB
                END-IF
              END-PERFORM
           END-IF.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                 PERFORM S90A1-GESTIONE-WC         THRU S90A1-EX

                 IF WK-CALL-CALCOLO-SI
                    IF IX-COD-MVV = 1
                       PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                       PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                       PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                    ELSE
                       PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    END-IF
                 ELSE
                    IF NOT IDSV0003-INVALID-CONVERSION
                       SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                    END-IF
                    PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
                 END-IF

              END-IF
           END-IF.
      *
       S1032-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  TITOLO_CONTABILE
      *----------------------------------------------------------------*
       S1033-TRATTA-TIT.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
           IF WTIT-ELE-TIT-CONT-MAX = 0
              PERFORM U0016-LEGGI-TIT        THRU U0016-EX
              IF IDSV0003-SUCCESSFUL-RC
                 MOVE WTIT-DATI(IX-TAB-TIT) TO WKS-AREA-TABB-APPO
                 MOVE IX-TAB-TIT            TO C214-IX-TABB
              ELSE
                 MOVE 1                     TO WKS-AREA-TABB-APPO
                 MOVE 1                     TO C214-IX-TABB
              END-IF
           ELSE
              IF WPOL-TP-LIV-GENZ-TIT = 'PO'
                 PERFORM VARYING IX-TAB-TIT FROM 1 BY 1
                   UNTIL IX-TAB-TIT > WTIT-ELE-TIT-CONT-MAX
                     IF WTIT-ID-OGG(IX-TAB-TIT) = WPOL-ID-POLI
                     AND WTIT-TP-OGG(IX-TAB-TIT) = 'PO'
                         MOVE WTIT-DATI(IX-TAB-TIT)
                           TO WKS-AREA-TABB-APPO
                         MOVE IX-TAB-TIT
                           TO C214-IX-TABB
                     END-IF
                 END-PERFORM
              ELSE
                 PERFORM VARYING IX-TAB-TIT FROM 1 BY 1
                   UNTIL IX-TAB-TIT > WTIT-ELE-TIT-CONT-MAX
                     IF WTIT-ID-OGG(IX-TAB-TIT) = WADE-ID-ADES(1)
                     AND WTIT-TP-OGG(IX-TAB-TIT) = 'AD'
                         MOVE WTIT-DATI(IX-TAB-TIT)
                           TO WKS-AREA-TABB-APPO
                         MOVE IX-TAB-TIT
                           TO C214-IX-TABB
                     END-IF
                 END-PERFORM
              END-IF
           END-IF.
      *
           IF C214-WHERE-COND = 'VATI'
              MOVE WTIT-ID-TIT-CONT(C214-IX-TABB)
                TO C214-ID-TIT-CONT
           ELSE
              IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
                 IF WKS-AREA-TABB-APPO NOT = SPACES
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    IF NOT IDSV0003-INVALID-CONVERSION
                       SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                    END-IF
                    PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
                 END-IF
              ELSE
                 IF WKS-AREA-TABB-APPO NOT = SPACES
                    IF IX-COD-MVV = 1
                       PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                       PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                       PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                    ELSE
                       PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    END-IF
                 ELSE
                    IF NOT IDSV0003-INVALID-CONVERSION
                       SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                    END-IF
                    PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
                 END-IF
              END-IF
           END-IF.
      *
       S1033-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  DETTAGLIO_TITOLO_CONTABILE
      *----------------------------------------------------------------*
       S1034-TRATTA-DTC.
      *
           IF WDTC-ELE-DETT-TIT-MAX = 0
              PERFORM U0002-LEGGI-DTC          THRU U0002-EX
           END-IF.

           MOVE WDTC-DATI(1)                 TO WKS-AREA-TABB-APPO.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
           ELSE
              SET WK-CALCOLI-SI              TO TRUE
      *
              MOVE V1391-COD-PARAMETRO(IX-COD-MVV)
                                             TO C214-COD-PARAMETRO
              PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
              PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
      *
              PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
           END-IF.
      *
       S1034-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  LIQUIDAZIONE
      *----------------------------------------------------------------*
       S1035-TRATTA-LQU.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
           MOVE 1                            TO C214-IX-TABB.
      *
           IF WLQU-ELE-LIQ-MAX = 0
23012         IF V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) NOT = '**'
                 PERFORM U0023-LEGGI-LQU        THRU U0023-EX
                 IF IDSV0003-SUCCESSFUL-RC
                 AND IDSO0011-SUCCESSFUL-RC
                 AND IDSO0011-SUCCESSFUL-SQL
                   MOVE WLQU-DATI(IX-TAB-LQU) TO WKS-AREA-TABB-APPO
                   MOVE IX-TAB-LQU           TO C214-IX-TABB
                 END-IF
23012         ELSE
23012            SET IDSV0003-NOT-FOUND      TO TRUE
23012         END-IF
           ELSE
              PERFORM VARYING IX-TAB-LQU FROM 1 BY 1
                UNTIL IX-TAB-LQU > WLQU-ELE-LIQ-MAX
                  IF WLQU-ID-OGG(IX-TAB-LQU) = WADE-ID-ADES(1)
                  AND WLQU-TP-OGG(IX-TAB-LQU) = 'AD'
                      MOVE WLQU-DATI(IX-TAB-LQU)
                        TO WKS-AREA-TABB-APPO
                      MOVE IX-TAB-LQU
                        TO C214-IX-TABB
                  END-IF
              END-PERFORM
           END-IF.
      *
           IF IDSV0003-SUCCESSFUL-RC
              IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
                 IF WKS-AREA-TABB-APPO NOT = SPACES
                    IF V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '$$' OR
                       V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '**'
                       PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                       MOVE V1391-VALORE-DEFAULT(IX-COD-MVV)(3:8)
                         TO PGM-CALCOLI
                       PERFORM S9002-CALL-CALCOLI  THRU S9002-EX
                       SET WK-CALCOLI-SI           TO TRUE
                    END-IF
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                    SET WK-CALCOLI-NO              TO TRUE
                 ELSE
                    IF V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '$$' OR
                       V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '**'
                       PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                       MOVE V1391-VALORE-DEFAULT(IX-COD-MVV)(3:8)
                         TO PGM-CALCOLI
                       PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                       SET WK-CALCOLI-SI              TO TRUE
                       PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                       SET WK-CALCOLI-NO              TO TRUE
                    ELSE
                       IF NOT IDSV0003-INVALID-CONVERSION
                          SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                       END-IF
                       PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
                    END-IF
                 END-IF
              ELSE
                 IF WKS-AREA-TABB-APPO NOT = SPACES
                    IF IX-COD-MVV = 1
                       PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                       PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                       PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                    ELSE
                       PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    END-IF
                 ELSE
                    IF V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '$$' OR
                       V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '**'
                       PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                       MOVE V1391-VALORE-DEFAULT(IX-COD-MVV)(3:8)
                         TO PGM-CALCOLI
                       PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                       PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                    ELSE
                       IF NOT IDSV0003-INVALID-CONVERSION
                          SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                       END-IF
                       PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
                    END-IF
                 END-IF
              END-IF
           ELSE
              PERFORM S9004-GESTIONE-ERRORE              THRU S9004-EX
           END-IF.
      *
       S1035-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA AREA BENEFICIARIO LIQUIDAZIONE
      *----------------------------------------------------------------*
       S1036-TRATTA-BEL.
      *
           CONTINUE.
      *
       S1036-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA AREA MOVI FINANZ
      *----------------------------------------------------------------*
       S1038-TRATTA-MFZ.
      *
           CONTINUE.
      *
       S1038-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA AREA MOVI FINANZ
      *----------------------------------------------------------------*
       S1039-TRATTA-RIF.
      *
           CONTINUE.
      *
       S1039-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA AREA MOVI FINANZ
      *----------------------------------------------------------------*
       S1040-TRATTA-TDR.
      *
           CONTINUE.
      *
       S1040-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA AREA MOVI FINANZ
      *----------------------------------------------------------------*
       S1041-TRATTA-DTR.
      *
           CONTINUE.
      *
       S1041-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  OGGETTO_COLLEGATO
      *----------------------------------------------------------------*
       S1042-TRATTA-OCO.
      *
            MOVE SPACES                         TO WKS-AREA-TABB-APPO.
      *
            IF WOCO-ELE-OGG-COLL-MAX = 0
               MOVE 1                           TO WKS-AREA-TABB-APPO
               MOVE 1                           TO C214-IX-TABB
            ELSE
               MOVE WOCO-DATI(1)                TO WKS-AREA-TABB-APPO
            END-IF.
      *
            IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
               IF WKS-AREA-TABB-APPO NOT = SPACES
                  PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
               ELSE
                  IF NOT IDSV0003-INVALID-CONVERSION
                     SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                  END-IF
                  PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
               END-IF
            ELSE
               IF WKS-AREA-TABB-APPO NOT = SPACES
                  IF IX-COD-MVV = 1
                     PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                     PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                     PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                  ELSE
                     PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                  END-IF
               ELSE
                  IF NOT IDSV0003-INVALID-CONVERSION
                     SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                  END-IF
                  PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
               END-IF
            END-IF.
      *
       S1042-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  RIPARTO IN COASSICURAZIONE
      *----------------------------------------------------------------*
       S1043-TRATTA-RCA.
      *
           CONTINUE.
      *
       S1043-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  RIPARTO IN COASSICURAZIONE
      *----------------------------------------------------------------*
       S1044-TRATTA-TLI.
      *
           CONTINUE.
      *
       S1044-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  DOCUMENTI IN LIQUIDAZIONE
      *----------------------------------------------------------------*
       S1045-TRATTA-DLQ.
      *
           CONTINUE.
      *
       S1045-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  DOC FORZATI LIQUIDAZIONE
      *----------------------------------------------------------------*
       S1046-TRATTA-DFL.

           CONTINUE.
       S1046-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  RIPARTO IN COASSICURAZIONE
      *----------------------------------------------------------------*
       S1047-TRATTA-GRL.
      *
           CONTINUE.
      *
       S1047-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  PERCIPIENTE DI LIQUIDAZIONE
      *----------------------------------------------------------------*
       S1048-TRATTA-PLI.
      *
           CONTINUE.
      *
       S1048-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  PARAM-COMP
      *----------------------------------------------------------------*
       S1049-TRATTA-PCO.
      *
           PERFORM U0024-LEGGI-PCO              THRU U0024-EX.
      *
           MOVE WPCO-DATI                       TO WKS-AREA-TABB-APPO.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
              END-IF
           END-IF.
      *
       S1049-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  PARAM-CALCOLO
      *----------------------------------------------------------------*
       S1050-TRATTA-PCA.
      *
           IF IX-COD-MVV = 1
              PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
              PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
              PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
           ELSE
              PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
           END-IF.
      *
       S1050-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  PARAM-CALCOLO
      *----------------------------------------------------------------*
       S1051-TRATTA-E12.
      *
           MOVE SPACES                         TO WKS-AREA-TABB-APPO.
      *
           IF WE12-ELE-ESTRA-MAX = 0

              PERFORM U0033-LEGGI-E12          THRU U0033-EX

              IF IDSV0003-SUCCESSFUL-RC
                 MOVE WE12-DATI(1)             TO WKS-AREA-TABB-APPO
              END-IF

              MOVE ZERO                        TO WE12-ELE-ESTRA-MAX
                                                  IX-TAB-E12

           ELSE
              MOVE WE12-DATI(1)                TO WKS-AREA-TABB-APPO
           END-IF.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                END-IF
                PERFORM S9004-GESTIONE-ERRORE      THRU S9004-EX
              END-IF
           END-IF.
      *
       S1051-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  PARAM-CALCOLO
      *----------------------------------------------------------------*
       S1052-TRATTA-L30.
      *
           MOVE SPACES                         TO WKS-AREA-TABB-APPO.
      *
           IF WL30-ELE-REINVST-POLI-MAX = 0
              PERFORM U0034-LEGGI-L30          THRU U0034-EX
              IF IDSV0003-SUCCESSFUL-RC
                 MOVE WL30-DATI(1)             TO WKS-AREA-TABB-APPO
              END-IF
           ELSE
              SET WK-ID-NON-TROVATO            TO TRUE
              PERFORM VARYING IX-TAB-L30 FROM 1 BY 1
                UNTIL IX-TAB-L30 > WL30-ELE-REINVST-POLI-MAX
                   OR WK-ID-TROVATO
                   IF WPOL-IB-OGG = WL30-IB-POLI(IX-TAB-L30)
                      MOVE WL30-DATI(IX-TAB-L30) TO WKS-AREA-TABB-APPO
                      MOVE IX-TAB-L30            TO C214-IX-TABB
                      SET WK-ID-TROVATO          TO TRUE
                   END-IF
              END-PERFORM

           END-IF.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                END-IF
                PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           END-IF.
      *
       S1052-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   GESTIONE VINCOLO E PEGNO
      *----------------------------------------------------------------*
       S1053-TRATTA-L23.
      *
           MOVE ZERO                           TO WK-ID-ASSIC.
           MOVE SPACES                         TO WKS-AREA-TABB-APPO.
      *
           MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(1:5)
                                               TO WK-APP-ASSICURATO.
      *
           IF IX-GUIDA-GRZ = ZERO
              CONTINUE
           ELSE
              EVALUATE TRUE
                WHEN PRIMO-ASS
                  IF WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-1O-ASSTO(IX-GUIDA-GRZ)
                       TO WK-ID-ASSIC
                  END-IF

                WHEN SECONDO-ASS
                  IF WGRZ-ID-2O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-2O-ASSTO(IX-GUIDA-GRZ)
                       TO WK-ID-ASSIC
                  END-IF

                WHEN TERZO-ASS
                  IF WGRZ-ID-3O-ASSTO(IX-GUIDA-GRZ) IS NUMERIC
                     MOVE WGRZ-ID-3O-ASSTO(IX-GUIDA-GRZ)
                       TO WK-ID-ASSIC
                  END-IF

              END-EVALUATE

              PERFORM S1053A-TRATTA-L23-DETT        THRU S1053A-EX

           END-IF.
      *
       S1053-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   GESTIONE VINCOLO E PEGNO
      *----------------------------------------------------------------*
       S1053A-TRATTA-L23-DETT.
      *
           IF WL23-ELE-VINC-PEG-MAX = 0
              PERFORM U0035-LEGGI-L23          THRU U0035-EX
              IF IDSV0003-SUCCESSFUL-RC
                 MOVE WL23-DATI(1)             TO WKS-AREA-TABB-APPO
              END-IF
           ELSE
              SET WK-ID-NON-TROVATO            TO TRUE
              PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
                UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX
                   OR WK-ID-TROVATO

                IF WRAN-ID-OGG(IX-TAB-RAN) = WADE-ID-ADES(1)
                AND WRAN-TP-OGG(IX-TAB-RAN) = 'AD'

                    PERFORM VARYING IX-TAB-L23 FROM 1 BY 1
                      UNTIL IX-TAB-L23 > WL23-ELE-VINC-PEG-MAX
                         OR WK-ID-TROVATO

                         IF WRAN-ID-RAPP-ANA(IX-TAB-RAN)
                         EQUAL WL23-ID-RAPP-ANA(IX-TAB-L23)
                            MOVE WL23-DATI(IX-TAB-L23)
                              TO WKS-AREA-TABB-APPO
                            MOVE IX-TAB-L23
                              TO C214-IX-TABB
                            SET WK-ID-TROVATO          TO TRUE
                         END-IF

                    END-PERFORM

                END-IF
              END-PERFORM

           END-IF.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                END-IF
                PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           END-IF.
      *
       S1053A-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   GESTIONE ESTENSIONE DI TRANCHE
      *----------------------------------------------------------------*
       S1054-TRATTA-PVT.
      *
           MOVE SPACES                          TO WKS-AREA-TABB-APPO.
      *
           IF WPVT-ELE-PROV-MAX = 0
              PERFORM U0036-LEGGI-PVT           THRU U0036-EX
              IF IDSV0003-SUCCESSFUL-RC
                 MOVE WPVT-DATI(1)              TO WKS-AREA-TABB-APPO
              END-IF
           ELSE
              SET WK-ID-NON-TROVATO            TO TRUE
              PERFORM VARYING IX-TAB-PVT FROM 1 BY 1
                UNTIL IX-TAB-PVT > WPVT-ELE-PROV-MAX
                   OR WK-ID-TROVATO
                   IF WGRZ-ID-GAR(IX-GUIDA-GRZ) =
                      WPVT-ID-GAR(IX-TAB-PVT)
                      MOVE WPVT-DATI(IX-TAB-PVT) TO WKS-AREA-TABB-APPO
                      MOVE IX-TAB-PVT            TO C214-IX-TABB
                      SET WK-ID-TROVATO          TO TRUE
                   END-IF
              END-PERFORM

           END-IF.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                END-IF
                PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           END-IF.
      *
       S1054-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  AREA FONDI X TRANCHE
      *----------------------------------------------------------------*
ALEX1  S1055-TRATTA-FXT.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
           MOVE IVVC0222-AREA-FND-X-TRANCHE(1:2500)
             TO WKS-AREA-TABB-APPO.
           SET FXT-ADDRESS   TO ADDRESS OF IVVC0222-AREA-FND-X-TRANCHE.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF NOT IDSV0003-INVALID-CONVERSION
                 SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
              END-IF
              PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    SET WK-CALCOLI-SI              TO TRUE
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                END-IF
                PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           END-IF.
      *
       S1055-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  IMPOSTA DI BOLLO
      *----------------------------------------------------------------*
NEW    S1057-TRATTA-P58.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
           MOVE WP58-DATI(1)                 TO WKS-AREA-TABB-APPO.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF NOT IDSV0003-INVALID-CONVERSION
                 SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
              END-IF
              PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
           ELSE
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
           END-IF.
      *
       S1057-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  DATI CRISTALLIZATI
      *----------------------------------------------------------------*
NEW    S1058-TRATTA-P61.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
NEWFIS     IF WP61-ELE-MAX-D-CRIST EQUAL ZERO
NEWFIS        PERFORM U0050-LEGGI-P61 THRU U0050-EX
NEWFIS        IF IDSV0003-SUCCESSFUL-RC
NEWFIS           MOVE WP61-DATI              TO WKS-AREA-TABB-APPO
NEWFIS        END-IF
NEWFIS     END-IF
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
                IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                END-IF
                PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
           ELSE
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
              END-IF
           END-IF.
      *
       S1058-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  DATI AGGIUNTIVI X POLIZZE CPI
      *----------------------------------------------------------------*
       S1059-TRATTA-P67.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
           IF WP67-ELE-MAX-EST-POLI-CPI-PR = 0
              PERFORM U0039-LEGGI-P67        THRU U0039-EX
              IF IDSV0003-SUCCESSFUL-RC
                 MOVE WP67-DATI              TO WKS-AREA-TABB-APPO
              END-IF
           ELSE
              MOVE WP67-DATI                 TO WKS-AREA-TABB-APPO
           END-IF.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                 IF NOT IDSV0003-INVALID-CONVERSION
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                 END-IF
                 PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
              END-IF
           END-IF.
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
       S1059-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  RICHIESTA ESTERNA
      *----------------------------------------------------------------*
       S1060-TRATTA-P01.
      *
           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
           MOVE 1                            TO C214-IX-TABB.
      *
           IF WP01-ELE-MAX-RICH-EST = 0
              PERFORM U0040-LEGGI-P01        THRU U0040-EX
              IF IDSV0003-SUCCESSFUL-RC
              AND IDSO0011-SUCCESSFUL-RC
              AND IDSO0011-SUCCESSFUL-SQL
                MOVE WP01-TAB-RICH-EST    TO WKS-AREA-TABB-APPO
              END-IF
           ELSE
                MOVE WP01-TAB-RICH-EST    TO WKS-AREA-TABB-APPO
           END-IF.
      *
           IF IDSV0003-SUCCESSFUL-RC
              IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
                 IF WKS-AREA-TABB-APPO NOT = SPACES
                    IF V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '$$' OR
                       V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '**'
                       PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                       MOVE V1391-VALORE-DEFAULT(IX-COD-MVV)(3:8)
                         TO PGM-CALCOLI
                       PERFORM S9002-CALL-CALCOLI  THRU S9002-EX
                       SET WK-CALCOLI-SI           TO TRUE
                    END-IF
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                    SET WK-CALCOLI-NO              TO TRUE
                 ELSE
                    IF V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '$$' OR
                       V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '**'
                       PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                       MOVE V1391-VALORE-DEFAULT(IX-COD-MVV)(3:8)
                         TO PGM-CALCOLI
                       PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                       SET WK-CALCOLI-SI              TO TRUE
                       PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                       SET WK-CALCOLI-NO              TO TRUE
                    ELSE
                       IF NOT IDSV0003-INVALID-CONVERSION
                          SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                       END-IF
                       PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
                    END-IF
                 END-IF
              ELSE
                 IF WKS-AREA-TABB-APPO NOT = SPACES
                    IF IX-COD-MVV = 1
                       PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                       PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                       PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                    ELSE
                       PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    END-IF
                 ELSE
                    IF V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '$$' OR
                       V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = '**'
                       PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                       MOVE V1391-VALORE-DEFAULT(IX-COD-MVV)(3:8)
                         TO PGM-CALCOLI
                       PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                       PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                    ELSE
                       IF NOT IDSV0003-INVALID-CONVERSION
                          SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                       END-IF
                       PERFORM S9004-GESTIONE-ERRORE     THRU S9004-EX
                    END-IF
                 END-IF
              END-IF
           ELSE
              PERFORM S9004-GESTIONE-ERRORE              THRU S9004-EX
           END-IF.
      *
       S1060-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   ELABORA  AREA FONDI PER RECUPERO COMMISSIONI GESTIONE
      *----------------------------------------------------------------*
16324  S1061-TRATTA-FXC.

           MOVE SPACES                       TO WKS-AREA-TABB-APPO.
      *
           MOVE WCDG-AREA-COMMIS-GEST-VV     TO WKS-AREA-TABB-APPO.
      *
           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF NOT IDSV0003-INVALID-CONVERSION
                 SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
              END-IF
              PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
           ELSE
              IF IX-COD-MVV = 1
                 PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                 PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
              END-IF
           END-IF.

16324  EX-S1061.
16324      EXIT.

      *----------------------------------------------------------------*
      *   IMPOSTA OUTPUT
      *----------------------------------------------------------------*
       S0098-IMPOSTA-OUT.
      *
           SET IDSV0141-MITT-COMP-3  TO TRUE.
           MOVE V1391-LUNGHEZZA-DATO(IX-COD-MVV)
             TO IDSV0141-LUNGHEZZA-DATO-MITT.
           MOVE WKS-LUN-DATO(IX-AP-TABB, IX-AP-DATO)
             TO IDSV0141-LUNGHEZZA-DATO-STR.
           MOVE V1391-PRECISIONE-DATO(IX-COD-MVV)
             TO IDSV0141-PRECISIONE-DATO-MITT.
           MOVE WKS-AREA-TABB-APPO(WKS-INI-POSI(IX-AP-TABB, IX-AP-DATO):
                                   WKS-LUN-DATO(IX-AP-TABB, IX-AP-DATO))
             TO IDSV0141-CAMPO-MITT.
      *
           IF GESTIONE-PRODOTTO
              EVALUATE C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR)
                WHEN 'A'
                WHEN 'N'
                WHEN 'I'
                  PERFORM Y9999-CHIAMA-IDSS0140        THRU Y9999-EX
                  IF IDSV0003-SUCCESSFUL-RC
                     MOVE IDSV0141-CAMPO-DEST
                     TO WKS-APPO-IMP
                  END-IF
                WHEN 'D'
                  PERFORM Y9999-CHIAMA-IDSS0140        THRU Y9999-EX
                  IF IDSV0003-SUCCESSFUL-RC
                     MOVE IDSV0141-CAMPO-DEST
                       TO WKS-APPO-DATA
                  END-IF
                WHEN 'P'
11250 *         WHEN 'A'
                WHEN 'M'
                  PERFORM Y9999-CHIAMA-IDSS0140        THRU Y9999-EX
                  IF IDSV0003-SUCCESSFUL-RC
                     MOVE IDSV0141-CAMPO-DEST
                       TO WKS-APPO-PER
                  END-IF
                WHEN 'S'
                  IF IDSV0141-CAMPO-MITT
                  (1:WKS-LUN-DATO(IX-AP-TABB, IX-AP-DATO)) = HIGH-VALUE
                  OR SPACES
                     SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                  ELSE
                     MOVE IDSV0141-CAMPO-MITT
                       TO WKS-APPO-STR
                  END-IF
              END-EVALUATE
           ELSE
              EVALUATE C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR)
                WHEN 'A'
                WHEN 'N'
                WHEN 'I'
                  PERFORM Y9999-CHIAMA-IDSS0140        THRU Y9999-EX
                  IF IDSV0003-SUCCESSFUL-RC
                     MOVE IDSV0141-CAMPO-DEST
                     TO WKS-APPO-IMP
                  END-IF
                WHEN 'D'
                  PERFORM Y9999-CHIAMA-IDSS0140        THRU Y9999-EX
                  IF IDSV0003-SUCCESSFUL-RC
                     MOVE IDSV0141-CAMPO-DEST
                       TO WKS-APPO-DATA
                  END-IF
                WHEN 'P'
11250 *         WHEN 'A'
                WHEN 'M'
                  PERFORM Y9999-CHIAMA-IDSS0140        THRU Y9999-EX
                  IF IDSV0003-SUCCESSFUL-RC
                     MOVE IDSV0141-CAMPO-DEST
                       TO WKS-APPO-PER
                  END-IF
                WHEN 'S'
                  IF IDSV0141-CAMPO-MITT
                  (1:WKS-LUN-DATO(IX-AP-TABB, IX-AP-DATO)) = HIGH-VALUE
                  OR SPACES
                     SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                  ELSE
                     MOVE IDSV0141-CAMPO-MITT
                       TO WKS-APPO-STR
                  END-IF
              END-EVALUATE
           END-IF.
      *
       S0098-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA DI OUTPUT.
      *----------------------------------------------------------------*
       S0099-VALORIZZA-OUTPUT.
      *
           IF IDSV0003-SUCCESSFUL-RC

              IF C214-SALTA-NO

                 IF GESTIONE-PRODOTTO
                    MOVE V1391-FORMATTAZIONE-DATO(IX-COD-MVV)(1:1)
                      TO C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR)
                 ELSE
                    MOVE V1391-FORMATTAZIONE-DATO(IX-COD-MVV)(1:1)
                      TO C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR)
                 END-IF

                 IF WK-CALCOLI-SI

                    PERFORM S1099-VALORIZZA-OUTPUT-MC  THRU S1099-EX

                 ELSE
                    PERFORM S0098-IMPOSTA-OUT        THRU S0098-EX
      *
                    IF IDSV0003-SUCCESSFUL-RC

                       PERFORM S2099-VAL-OUT-DIN     THRU S2099-EX
                    ELSE
                       PERFORM S9004-GESTIONE-ERRORE THRU S9004-EX
                    END-IF
                 END-IF
              END-IF

           ELSE
              PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX

           END-IF.

      *
       S0099-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VERIFICO SE E' UNA VARIABILE DA ESPORTARE IN CHIARO
      *----------------------------------------------------------------*
       S0100-VER-VAR-IN-CHIARO.

           SET VAR-IN-CHIARO-NO          TO TRUE

           IF GESTIONE-PRODOTTO
              IF WK-COD-VARIABILE = 'DEE'
                 SET VAR-IN-CHIARO-SI    TO TRUE
              END-IF
           ELSE
              IF WK-COD-VARIABILE = 'ITN'
              OR WK-COD-VARIABILE = 'TIPOTRANCHE'
                 SET VAR-IN-CHIARO-SI    TO TRUE
              END-IF
           END-IF.

       S0100-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA DI OUTPUT DEI MODULI DI CALCOLO
      *----------------------------------------------------------------*
       S1099-VALORIZZA-OUTPUT-MC.
      *
      *- Le var a lista vengono salvate in un'area di appoggio e poi
      *- passate in output

           IF IVVC0501-MAX-TAB-STR GREATER ZERO

              PERFORM VARYING IX-VAR-LIS FROM 1 BY 1
                UNTIL IX-VAR-LIS > IVVC0501-MAX-TAB-STR

                ADD 1 TO            WK1-MAX-TAB-STRING
                MOVE C214-COD-VARIABILE-O
                  TO WK1-COD-VARIABILE(WK1-MAX-TAB-STRING)
                MOVE IVVC0501-TP-STRINGA
                  TO WK1-TP-STRINGA(WK1-MAX-TAB-STRING)
                MOVE IVVC0501-STRINGA-TOT(IX-VAR-LIS)
                  TO WK1-STRINGA-TOT(WK1-MAX-TAB-STRING)

              END-PERFORM
           ELSE
      *
              IF GESTIONE-PRODOTTO

                 MOVE SPACES               TO WK-COD-VARIABILE
                 MOVE C214-COD-VARIABILE-O TO WK-COD-VARIABILE
                 PERFORM S0100-VER-VAR-IN-CHIARO
                    THRU S0100-EX

                 IF VAR-IN-CHIARO-SI
                    PERFORM GESTIONE-VAR-P
                       THRU GESTIONE-VAR-P-EX
                 ELSE

                    ADD 1                  TO IX-COD-VAR-OUT

                    MOVE C214-COD-VARIABILE-O
                      TO C216-COD-VARIABILE-P
                         (IX-VAL-VAR, IX-COD-VAR-OUT)

                    MOVE C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR)
                      TO C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR-OUT)

                    EVALUATE C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR-OUT)
                        WHEN 'A'
                        WHEN 'N'
                        WHEN 'I'
                           IF C214-VAL-IMP-O IS NUMERIC
                              MOVE C214-VAL-IMP-O
                                TO C216-VAL-IMP-P
                                   (IX-VAL-VAR, IX-COD-VAR-OUT)
                           ELSE
                              MOVE ZEROS
                                TO C216-VAL-IMP-P
                                   (IX-VAL-VAR, IX-COD-VAR-OUT)
                           END-IF

                        WHEN 'P'
                        WHEN 'M'
                           IF C214-VAL-PERC-O IS NUMERIC
                              MOVE C214-VAL-PERC-O
                                TO C216-VAL-PERC-P
                                   (IX-VAL-VAR, IX-COD-VAR-OUT)
                           ELSE
                              MOVE ZEROES
                                TO C216-VAL-PERC-P
                                   (IX-VAL-VAR, IX-COD-VAR-OUT)
                           END-IF
                        WHEN 'D'
                        WHEN 'S'
                        WHEN 'X'
                           MOVE C214-VAL-STR-O
                             TO C216-VAL-STR-P
                               (IX-VAL-VAR, IX-COD-VAR-OUT)
                    END-EVALUATE
                 END-IF

              ELSE

                 MOVE SPACES               TO WK-COD-VARIABILE
                 MOVE C214-COD-VARIABILE-O TO WK-COD-VARIABILE
                 PERFORM S0100-VER-VAR-IN-CHIARO
                    THRU S0100-EX

                 IF VAR-IN-CHIARO-SI
                    PERFORM GESTIONE-VAR-T
                       THRU GESTIONE-VAR-T-EX
                 ELSE

                    ADD 1                  TO IX-COD-VAR-OUT

                    MOVE C214-COD-VARIABILE-O
                      TO C216-COD-VARIABILE-T
                         (IX-VAL-VAR, IX-COD-VAR-OUT)

                    MOVE C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR)
                      TO C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR-OUT)

                    EVALUATE C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR-OUT)
                        WHEN 'A'
                        WHEN 'N'
                        WHEN 'I'
                           IF C214-VAL-IMP-O IS NUMERIC
                              MOVE C214-VAL-IMP-O
                                TO C216-VAL-IMP-T
                                   (IX-VAL-VAR, IX-COD-VAR-OUT)
                           ELSE
                              MOVE ZEROS
                                TO C216-VAL-IMP-T
                                   (IX-VAL-VAR, IX-COD-VAR-OUT)
                           END-IF

                        WHEN 'P'
                        WHEN 'M'
                           IF C214-VAL-PERC-O IS NUMERIC
                              MOVE C214-VAL-PERC-O
                                TO C216-VAL-PERC-T
                                   (IX-VAL-VAR, IX-COD-VAR-OUT)
                           ELSE
                              MOVE ZEROES
                                TO C216-VAL-PERC-T
                                   (IX-VAL-VAR, IX-COD-VAR-OUT)
                           END-IF
                        WHEN 'D'
                        WHEN 'S'
                        WHEN 'X'
                           MOVE C214-VAL-STR-O
                             TO C216-VAL-STR-T
                               (IX-VAL-VAR, IX-COD-VAR-OUT)
                    END-EVALUATE
                 END-IF
              END-IF

           END-IF.
      *
       S1099-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA DI OUTPUT DEI MODULI DI CALCOLO
      *----------------------------------------------------------------*
       S2099-VAL-OUT-DIN.
      *
           IF GESTIONE-PRODOTTO

              MOVE SPACES              TO WK-COD-VARIABILE
              MOVE C216-COD-VARIABILE-P(IX-VAL-VAR, IX-COD-VAR)
                TO WK-COD-VARIABILE
              PERFORM S0100-VER-VAR-IN-CHIARO
                 THRU S0100-EX

              IF VAR-IN-CHIARO-SI
                 PERFORM GEST-VAR-P-OUT-DIN
                    THRU GEST-VAR-P-OUT-DIN-EX
              ELSE

                 ADD 1                       TO IX-COD-VAR-OUT

                 EVALUATE C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR)
                   WHEN 'A'
                   WHEN 'N'
                   WHEN 'I'
                     MOVE WKS-APPO-IMP
                       TO C216-VAL-IMP-P
                            (IX-VAL-VAR, IX-COD-VAR-OUT)
                   WHEN 'D'
                     MOVE WKS-APPO-DATA
                       TO C216-VAL-STR-P
                            (IX-VAL-VAR, IX-COD-VAR-OUT)
                   WHEN 'P'
                   WHEN 'M'
11250 *            WHEN 'A'
                     MOVE WKS-APPO-PER
                      TO C216-VAL-PERC-P
                           (IX-VAL-VAR, IX-COD-VAR-OUT)
                   WHEN 'S'
                     MOVE WKS-APPO-STR
                       TO C216-VAL-STR-P
                           (IX-VAL-VAR, IX-COD-VAR-OUT)
                 END-EVALUATE
      *
                 MOVE C216-COD-VARIABILE-P
                          (IX-VAL-VAR, IX-COD-VAR)
                   TO C216-COD-VARIABILE-P
                          (IX-VAL-VAR, IX-COD-VAR-OUT)
                 MOVE C216-TP-DATO-P
                          (IX-VAL-VAR, IX-COD-VAR)
                   TO C216-TP-DATO-P
                          (IX-VAL-VAR, IX-COD-VAR-OUT)
              END-IF
           ELSE

              MOVE SPACES              TO WK-COD-VARIABILE
              MOVE C216-COD-VARIABILE-T(IX-VAL-VAR, IX-COD-VAR)
                TO WK-COD-VARIABILE
              PERFORM S0100-VER-VAR-IN-CHIARO
                 THRU S0100-EX

              IF VAR-IN-CHIARO-SI
                 PERFORM GEST-VAR-T-OUT-DIN
                    THRU GEST-VAR-T-OUT-DIN-EX
              ELSE

                 ADD 1                       TO IX-COD-VAR-OUT

                 EVALUATE C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR)
                   WHEN 'A'
                   WHEN 'N'
                   WHEN 'I'
                     MOVE WKS-APPO-IMP
                       TO C216-VAL-IMP-T
                            (IX-VAL-VAR, IX-COD-VAR-OUT)
                   WHEN 'D'
                     MOVE WKS-APPO-DATA
                       TO C216-VAL-STR-T
                            (IX-VAL-VAR, IX-COD-VAR-OUT)
                   WHEN 'P'
                   WHEN 'M'
11250 *            WHEN 'A'
                     MOVE WKS-APPO-PER
                      TO C216-VAL-PERC-T
                           (IX-VAL-VAR, IX-COD-VAR-OUT)
                   WHEN 'S'
                     MOVE WKS-APPO-STR
                       TO C216-VAL-STR-T
                           (IX-VAL-VAR, IX-COD-VAR-OUT)
                 END-EVALUATE
      *
                 MOVE C216-COD-VARIABILE-T
                          (IX-VAL-VAR, IX-COD-VAR)
                   TO C216-COD-VARIABILE-T
                          (IX-VAL-VAR, IX-COD-VAR-OUT)
                 MOVE C216-TP-DATO-T
                          (IX-VAL-VAR, IX-COD-VAR)
                   TO C216-TP-DATO-T
                          (IX-VAL-VAR, IX-COD-VAR-OUT)
              END-IF
           END-IF.
      *
       S2099-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   IMPOSTA AREA UNICA PER CHIAMATA AL MODULO DI CALCOLO
      *----------------------------------------------------------------*
       S9001-IMPOSTA-CALCOLO.
      *
           INITIALIZE C214-AREA-VARIABILI.
      *
           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                              TO C214-COD-COMPAGNIA-ANIA.
           MOVE IDSV0003-TIPO-MOVIMENTO
                              TO C214-TIPO-MOVIMENTO.
           MOVE WK-MOVI-ORIG  TO C214-TIPO-MOVI-ORIG
           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                              TO C214-DATA-EFFETTO.
           MOVE IDSV0003-DATA-COMPETENZA
                              TO C214-DATA-COMPETENZA.
           MOVE IDSV0003-DATA-COMP-AGG-STOR
                              TO C214-DATA-COMP-AGG-STOR.
      *
           MOVE WPOL-ID-POLI              TO C214-ID-POLIZZA.
           MOVE WADE-ID-ADES(1)           TO C214-ID-ADESIONE.
           MOVE IVVC0216-FLG-AREA         TO C214-FLG-AREA.
      *
           IF GESTIONE-GARANZIE
              IF C216-TP-LIVELLO-T(IX-VAL-VAR) = 'G'
                 IF C216-ID-GAR-T(IX-VAL-VAR) IS NUMERIC
                     MOVE C216-ID-GAR-T(IX-VAL-VAR)
                                             TO C214-ID-GARANZIA
                 END-IF
              END-IF
           END-IF
      *
           IF GESTIONE-GARANZIE
              IF C216-ID-LIVELLO-T(IX-VAL-VAR) IS NUMERIC
                 IF C216-ID-LIVELLO-T(IX-VAL-VAR) GREATER ZEROES
                    MOVE C216-ID-LIVELLO-T(IX-VAL-VAR)
                                             TO C214-ID-TRANCHE
                 END-IF
              END-IF
           END-IF

           PERFORM X999-CTRL-ID-FITTIZIO  THRU X999-EX

           MOVE V1391-COD-PARAMETRO(IX-COD-MVV)
                                        TO C214-COD-PARAMETRO
      *
           IF V1391-VALORE-DEFAULT(IX-COD-MVV)(1:2) = ('$$' OR '**')
              MOVE V1391-VALORE-DEFAULT(IX-COD-MVV)(3:8)
                   TO PGM-CALCOLI
           ELSE
              MOVE V1391-MODULO-CALCOLO(IX-COD-MVV)
                                        TO PGM-CALCOLI
           END-IF.
      *
           MOVE V1391-COD-STR-DATO-PTF(IX-COD-MVV)
                                        TO WKS-NOME-TABELLA
      *
           PERFORM S9003-VALORIZZA-SK   THRU S9003-EX

           PERFORM VALORIZZA-AREA-VAR   THRU VALORIZZA-AREA-VAR-EX
      *
           SET IDSV0003-WHERE-CONDITION TO TRUE
           SET IDSV0003-SELECT          TO TRUE
      *
           PERFORM S90A1-GESTIONE-WC    THRU S90A1-EX
      *
           IF IX-CONTA GREATER ZEROES
              COMPUTE WK-LUNGHEZZA-TOT = C214-POSIZ-INI(IX-CONTA) +
                                         C214-LUNGHEZZA(IX-CONTA)
      *
              MOVE WK-LUNGHEZZA-TOT       TO C214-LUNGHEZZA-TOT
           ELSE
              IF WKS-AREA-TABB-APPO = SPACES
                 CONTINUE
              ELSE
                 PERFORM S90A2-VERIFICA-OBB  THRU S90A2-EX
      *
                 IF WK-KO-YES
                    SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                    MOVE WK-PGM
                      TO IDSV0003-COD-SERVIZIO-BE
                    MOVE 'ERRORE VALORIZZAZIONE AREA MODULO CALCOLO'
                      TO IDSV0003-DESCRIZ-ERR-DB2
                 END-IF
              END-IF
           END-IF.
      *
       S9001-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE WHERE COND
      *----------------------------------------------------------------*
       S90A1-GESTIONE-WC.
      *
           MOVE V1391-WHERE-CONDITION(IX-COD-MVV)
                                                   TO C214-WHERE-COND.

      *--> VIENE IMPOSTATO SULLA TABELLA MATR_VAL_VAR NEL CAMPO WHERE
      *--> CONDITION UN VALORE CHE GUIDA L'ELABORAZIONE.
      *--> 'VATI' (VALORIZZA TITOLO) CHE INDICA LA
      *--> VALORIZZAZIONE OBBLIGATORIA DEL TITOLO CONTABILE PER LA VAR
      *--> RIABILE CHE STIAMO ELABORANDO.
      *--> DECOR VALORIZZA LA DECORRENZA DA PASSARE AL MODULO DI CALCOLO

           EVALUATE C214-WHERE-COND
              WHEN 'CALCOLO-RST'
                SET WK-CALL-CALCOLO-SI            TO TRUE

              WHEN 'VATI'
                PERFORM S1033-TRATTA-TIT          THRU S1033-EX

              WHEN 'DECPO'
      *       WHEN '6031'
                MOVE WPOL-DT-DECOR                TO C214-DT-DECOR

              WHEN '6031'
                MOVE WTGA-DT-DECOR(C214-IX-TABB)  TO C214-DT-DECOR

              WHEN 'DECORAD'
                MOVE WADE-DT-DECOR(1)             TO C214-DT-DECOR

              WHEN 'DECORGA'
                MOVE WGRZ-DT-DECOR(IX-GUIDA-GRZ)  TO C214-DT-DECOR

              WHEN 'DECORTG'
                MOVE WTGA-DT-DECOR(C214-IX-TABB)  TO C214-DT-DECOR

              WHEN OTHER
                PERFORM S90B1-GESTIONE-WC2         THRU S90B1-EX

           END-EVALUATE.
      *
       S90A1-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICA EVENTUALI GESTIONI PARTICOLARI DOVE NON OCCORRE
      *    PASSARE AL MODULO DI CALCOLO L'AREA DI INPUT.
      *----------------------------------------------------------------*
       S90A2-VERIFICA-OBB.
      *
           SET WK-KO-YES                   TO TRUE.
      *
           EVALUATE V1391-COD-STR-DATO-PTF(IX-COD-MVV)
             WHEN 'PARAM-DI-CALC'
               SET WK-KO-NO                TO TRUE

             WHEN 'IMPST-BOLLO'
               SET WK-KO-NO                TO TRUE

NEW01        WHEN 'D-CRIST'
NEW01          SET WK-KO-NO                TO TRUE


             WHEN OTHER
               SET WK-KO-YES               TO TRUE

           END-EVALUATE.
      *
       S90A2-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE WHERE COND
      *----------------------------------------------------------------*
       S90B1-GESTIONE-WC2.
      *
           IF C214-WHERE-COND(1:5) EQUAL 'PRIMO' OR 'SECON' OR 'TERZO'

              EVALUATE C214-WHERE-COND(7:7)

                 WHEN 'DECPO'
                   MOVE WPOL-DT-DECOR                TO C214-DT-DECOR

                 WHEN 'DECORAD'
                   MOVE WADE-DT-DECOR(1)             TO C214-DT-DECOR

                 WHEN 'DECORGA'
                   MOVE WGRZ-DT-DECOR(IX-GUIDA-GRZ)  TO C214-DT-DECOR

                 WHEN 'DECORTG'
                   MOVE WTGA-DT-DECOR(C214-IX-TABB)  TO C214-DT-DECOR

              END-EVALUATE
           END-IF.
      *
       S90B1-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CALL ROUTINE DI CALCOLO
      *----------------------------------------------------------------*
       S9002-CALL-CALCOLI.
      *
           INITIALIZE IVVC0501-OUTPUT.
      *
           MOVE IDSV0003-TRATTAMENTO-STORICITA  TO WKS-APPO-STOR.
      *
           SET C214-SALTA-NO                    TO TRUE

           CALL PGM-CALCOLI USING IDSV0003
                                  AREA-IO-CALCOLI
                                  IVVC0501-OUTPUT
           ON EXCEPTION
              MOVE PGM-CALCOLI
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-CALCOLI ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           MOVE WKS-APPO-STOR   TO IDSV0003-TRATTAMENTO-STORICITA.
      *
       S9002-EX.
           EXIT.
      *----------------------------------------------------------------*
      * VALORIZZA INPUT PER MODULO DI CALCOLO
      *----------------------------------------------------------------*
       S9003-VALORIZZA-SK.
      *
            IF GESTIONE-PRODOTTO
               MOVE C216-ID-LIVELLO-P(IX-VAL-VAR)
                 TO C214-ID-LIVELLO
               MOVE C216-TP-LIVELLO-P(IX-VAL-VAR)
                 TO C214-TP-LIVELLO
               MOVE C216-COD-LIVELLO-P(IX-VAL-VAR)
                 TO C214-COD-LIVELLO
               MOVE C216-ID-POL-P(IX-VAL-VAR)
                 TO C214-ID-POL-GAR
               MOVE C216-DT-INIZ-PROD-P(IX-VAL-VAR)
                 TO C214-DT-INIZ-PROD
               MOVE C216-COD-RGM-FISC-P(IX-VAL-VAR)
                 TO C214-COD-RGM-FISC
               MOVE C216-COD-VARIABILE-P(IX-VAL-VAR, IX-COD-VAR)
                 TO C214-COD-VARIABILE
               MOVE C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR)
                 TO C214-TP-DATO
            ELSE
               MOVE C216-ID-LIVELLO-T(IX-VAL-VAR)
                 TO C214-ID-LIVELLO
               MOVE C216-TP-LIVELLO-T(IX-VAL-VAR)
                 TO C214-TP-LIVELLO
               MOVE C216-COD-LIVELLO-T(IX-VAL-VAR)
                 TO C214-COD-LIVELLO
               MOVE C216-ID-GAR-T(IX-VAL-VAR)
                 TO C214-ID-POL-GAR
               MOVE C216-DT-INIZ-TARI-T(IX-VAL-VAR)
                 TO C214-DT-INIZ-TARI
               MOVE C216-COD-RGM-FISC-T(IX-VAL-VAR)
                 TO C214-COD-RGM-FISC
               MOVE C216-DT-DECOR-TRCH-T(IX-VAL-VAR)
                 TO C214-DT-DECOR
               MOVE C216-COD-VARIABILE-T(IX-VAL-VAR, IX-COD-VAR)
                 TO C214-COD-VARIABILE
               MOVE C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR)
                 TO C214-TP-DATO
            END-IF.
      *
       S9003-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  GESTIONE CONTROLLATA DELL'ERRORE
      *----------------------------------------------------------------*
       S9004-GESTIONE-ERRORE.
      *
      *--> QUALORA LA VARIABILE NON E' OBBLIGATORIA VIENE POPOLATA
      *--> UN'AREA PER PASSARE AL FRONT-END LE VARIABILI SCARTATE.

            IF V1391-OBBLIGATORIETA(IX-COD-MVV) = 'N'
            AND IDSV0003-FIELD-NOT-VALUED
               INITIALIZE          IDSV0003-CAMPI-ESITO
               SET IDSV0003-SUCCESSFUL-RC       TO TRUE
               IF WK-MMV-NO
                  IF C216-ELE-MAX-NOT-FOUND LESS 5
                     ADD 1
                       TO C216-ELE-MAX-NOT-FOUND
                     IF GESTIONE-PRODOTTO
                       MOVE C216-COD-VARIABILE-P(IX-VAL-VAR, IX-COD-VAR)
                         TO C216-VAR-NOT-FOUND(C216-ELE-MAX-NOT-FOUND)
                     ELSE
                       MOVE C216-COD-VARIABILE-T(IX-VAL-VAR, IX-COD-VAR)
                         TO C216-VAR-NOT-FOUND(C216-ELE-MAX-NOT-FOUND)
                     END-IF
                     MOVE ';'
                       TO C216-VAR-NOT-FOUND-SP(C216-ELE-MAX-NOT-FOUND)
                     END-IF
               ELSE
                  IF C216-ELE-MAX-CALC-KO LESS 5
                     ADD 1
                       TO C216-ELE-MAX-CALC-KO
                     IF GESTIONE-PRODOTTO
                       MOVE C216-COD-VARIABILE-P(IX-VAL-VAR, IX-COD-VAR)
                         TO C216-VAR-CALCOLO-KO(C216-ELE-MAX-CALC-KO)
                     ELSE
                       MOVE C216-COD-VARIABILE-T(IX-VAL-VAR, IX-COD-VAR)
                         TO C216-VAR-CALCOLO-KO(C216-ELE-MAX-CALC-KO)
                     END-IF
                     MOVE ';'
                       TO C216-VAR-NOT-FOUND-SP(C216-ELE-MAX-CALC-KO)
                  END-IF
               END-IF
            ELSE
CRMA           IF IDSV0003-FIELD-NOT-VALUED
CRMA              INITIALIZE          IDSV0003-CAMPI-ESITO
CRMA              SET IDSV0003-SUCCESSFUL-RC       TO TRUE
CRMA           ELSE
                  IF IDSV0003-DESCRIZ-ERR-DB2 EQUAL SPACES
                     MOVE IDSV0003-DESCRIZ-ERR-DB2
                       TO WK-APPO-ERR
                     IF GESTIONE-PRODOTTO
                        STRING ' VARIABILE :'
                            C216-COD-VARIABILE-P
                            (IX-VAL-VAR, IX-COD-VAR) ' ;'
                            WK-APPO-ERR
                        DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                        END-STRING
                     ELSE
                        STRING ' VARIABILE :'
                            C216-COD-VARIABILE-T
                            (IX-VAL-VAR, IX-COD-VAR) ' ;'
                            WK-APPO-ERR
                        DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                        END-STRING
                     END-IF
                  END-IF
CRMA           END-IF
      *
            END-IF.
      *
       S9004-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           IF IDSV0003-SUCCESSFUL-RC
              IF C216-ELE-MAX-NOT-FOUND GREATER ZEROES
              OR C216-ELE-MAX-CALC-KO GREATER ZEROES
                 MOVE 'CA'               TO IDSV0003-RETURN-CODE
              END-IF
              IF CACHE-PIENA-SI
                 SET IDSV0003-CACHE-PIENA   TO TRUE
              END-IF
           END-IF.
      *
           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *  GESTIONE CONTROLLATA DELL'ERRORE
      *----------------------------------------------------------------*
       S9005-ERRORE-LIV-VAR.
      *
           MOVE 'VAR-FUNZ-DI-CALC'
             TO IDSV0003-NOME-TABELLA
           MOVE 'VA CARICATA A LIVELLO DI GRARANZIA NELLA TABELLA '
             TO WK-APPO-ERR
           IF GESTIONE-PRODOTTO
              STRING 'LA VARIABILE '
                     C216-COD-VARIABILE-P(IX-VAL-VAR, IX-COD-VAR) ' ;'
                     WK-APPO-ERR
              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           ELSE
              STRING 'LA VARIABILE '
                     C216-COD-VARIABILE-T(IX-VAL-VAR, IX-COD-VAR) ' ;'
                     WK-APPO-ERR
              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF
      *
           SET IDSV0003-INVALID-CONVERSION          TO TRUE.
      *
       S9005-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLO ID FITTIZIO
      *----------------------------------------------------------------*
       X999-CTRL-ID-FITTIZIO.
      *
           SET ID-TROVATO-NO                      TO TRUE
           SET C214-ID-POL-AUTENTICO              TO TRUE
           IF C214-ID-POLIZZA IS NUMERIC AND
              C214-ID-POLIZZA NOT = ZEROES
              PERFORM VARYING IX-CTRL-FITT FROM 1 BY 1
                UNTIL IX-CTRL-FITT > WPOL-ELE-POLI-MAX
                   OR ID-TROVATO-SI

                   IF  WPOL-ID-POLI =
                       C214-ID-POLIZZA
                       IF WPOL-ST-ADD
                          SET ID-TROVATO-SI       TO TRUE
                          SET C214-ID-POL-FITTZIO TO TRUE
                       END-IF
                   END-IF

              END-PERFORM
           END-IF

           SET ID-TROVATO-NO                      TO TRUE
           SET C214-ID-ADE-AUTENTICO              TO TRUE
           IF C214-ID-ADESIONE IS NUMERIC AND
              C214-ID-ADESIONE NOT = ZEROES
              PERFORM VARYING IX-CTRL-FITT FROM 1 BY 1
                UNTIL IX-CTRL-FITT > WADE-ELE-ADES-MAX
                   OR ID-TROVATO-SI

                   IF  WADE-ID-ADES(IX-CTRL-FITT) =
                       C214-ID-ADESIONE
                       IF WADE-ST-ADD(IX-CTRL-FITT)
                          SET ID-TROVATO-SI       TO TRUE
                          SET C214-ID-ADE-FITTZIO TO TRUE
                       END-IF
                   END-IF

              END-PERFORM
           END-IF

           SET ID-TROVATO-NO                      TO TRUE
           SET C214-ID-GRZ-AUTENTICO              TO TRUE
           IF C214-ID-GARANZIA IS NUMERIC AND
              C214-ID-GARANZIA NOT = ZEROES
              PERFORM VARYING IX-CTRL-FITT FROM 1 BY 1
                UNTIL IX-CTRL-FITT > WGRZ-ELE-GARANZIA-MAX
                   OR ID-TROVATO-SI

                   IF  WGRZ-ID-GAR(IX-CTRL-FITT) =
                       C214-ID-GARANZIA
                       IF WGRZ-ST-ADD(IX-CTRL-FITT)
                          SET ID-TROVATO-SI       TO TRUE
                          SET C214-ID-GRZ-FITTZIO TO TRUE
                       END-IF
                   END-IF

              END-PERFORM
           END-IF

           SET ID-TROVATO-NO                      TO TRUE
           SET C214-ID-TGA-AUTENTICO              TO TRUE
           IF C214-ID-TRANCHE IS NUMERIC AND
              C214-ID-TRANCHE NOT = ZEROES
              PERFORM VARYING IX-CTRL-FITT FROM 1 BY 1
                UNTIL IX-CTRL-FITT > W1TGA-ELE-TRAN-MAX
                   OR ID-TROVATO-SI

                   IF  W1TGA-ID-TRCH-DI-GAR(IX-CTRL-FITT) =
                       C214-ID-TRANCHE
                       IF W1TGA-ST-ADD(IX-CTRL-FITT)
                          SET ID-TROVATO-SI       TO TRUE
                          SET C214-ID-TGA-FITTZIO TO TRUE
                       END-IF
                   END-IF

              END-PERFORM
           END-IF.
      *
       X999-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA EST_RAPPORTO_ANAGRAFICO
      *----------------------------------------------------------------*
       S1056-TRATTA-EST-RAN.
      *
           INITIALIZE                           EST-RAPP-ANA.
           MOVE SPACES                          TO WKS-AREA-TABB-APPO.
      *
           MOVE V1391-WHERE-CONDITION(IX-COD-MVV)(1:5)
                                                TO WK-APP-ASSICURATO.
           IF WE15-ELE-EST-RAPP-ANAG-MAX = 0
              EVALUATE TRUE
                 WHEN TP-OGG-PO
                 WHEN TP-DEC-PO
                   MOVE WPOL-ID-POLI                 TO E15-ID-OGG
                   MOVE 'PO'                         TO E15-TP-OGG
                   PERFORM U0038-LEGGI-EST-RAN       THRU U0038-EX
                 WHEN TP-OGG-AD
                 WHEN TP-DEC-AD
                   MOVE WADE-ID-ADES(1)              TO E15-ID-OGG
                   MOVE 'AD'                         TO E15-TP-OGG
                   PERFORM U0038-LEGGI-EST-RAN       THRU U0038-EX
                 WHEN OTHER
                   MOVE WPOL-ID-POLI                 TO E15-ID-OGG
                   MOVE 'PO'                         TO E15-TP-OGG
                   PERFORM U0038-LEGGI-EST-RAN       THRU U0038-EX
              END-EVALUATE
              IF IDSV0003-SUCCESSFUL-RC
                 MOVE WE15-DATI(IX-TAB-RAN)     TO WKS-AREA-TABB-APPO
                 MOVE IX-TAB-E15                TO C214-IX-TABB
              END-IF
           ELSE
              IF WK-APP-ASSICURATO = 'CO'
                 PERFORM S1156-EST-RAN-CO   THRU S1156-EX
              END-IF
           END-IF.

           IF V1391-MODULO-CALCOLO-NULL(IX-COD-MVV) = HIGH-VALUE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
              ELSE
                IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                END-IF
                PERFORM S9004-GESTIONE-ERRORE  THRU S9004-EX
              END-IF
           ELSE
              IF WKS-AREA-TABB-APPO NOT = SPACES
                 IF IX-COD-MVV = 1
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                    PERFORM S9002-CALL-CALCOLI     THRU S9002-EX
                    PERFORM S0099-VALORIZZA-OUTPUT THRU S0099-EX
                 ELSE
                    PERFORM S9001-IMPOSTA-CALCOLO  THRU S9001-EX
                 END-IF
              ELSE
                IF NOT IDSV0003-INVALID-CONVERSION
                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
                END-IF
                PERFORM S9004-GESTIONE-ERRORE      THRU S9004-EX
              END-IF
           END-IF.
      *
       S1056-EX.
           EXIT.

      *----------------------------------------------------------------*
      *  LETTURE RAN ID/TP OGG
      *----------------------------------------------------------------*
       U0038-LEGGI-EST-RAN.
      *
           PERFORM U0007-IMPOSTA-GENERALE       THRU U0007-EX.
      *

           MOVE 10                              TO IX-APPO-OVERF.
           MOVE 0                               TO IX-TAB-E15.
      *
           MOVE  EST-RAPP-ANA              TO IDSI0011-BUFFER-DATI.
      *
           PERFORM Z0000-LETTURA-GEN       THRU Z0000-EX.
      *
           MOVE IX-TAB-E15              TO WE15-ELE-EST-RAPP-ANAG-MAX.
      *
       U0038-EX.
           EXIT.

      *----------------------------------------------------------------*
      *  LETTURA TABELLA RICHIESTA ESTERNA
      *----------------------------------------------------------------*
       U0040-LEGGI-P01.
      *
           PERFORM U0007-IMPOSTA-GENERALE       THRU U0007-EX.
      *
           MOVE IDSV0003-TRATTAMENTO-STORICITA  TO WKS-APPO-STOR.
      *
           SET IDSV0003-TRATT-SENZA-STOR        TO TRUE.

           MOVE 10                              TO IX-APPO-OVERF.
           MOVE 0                               TO IX-TAB-P01.
      *
           MOVE  ZEROES                 TO P01-ID-RICH-EST.
           MOVE  RICH-EST               TO IDSI0011-BUFFER-DATI.
      *
           PERFORM Z0000-LETTURA-GEN       THRU Z0000-EX.
      *
           MOVE IX-TAB-P01              TO WP01-ELE-MAX-RICH-EST.

           MOVE WKS-APPO-STOR
             TO IDSV0003-TRATTAMENTO-STORICITA.
      *
       U0040-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    LEGGI EST RAN LEGATI AL CONTRAENTE
      *----------------------------------------------------------------*
       S1156-EST-RAN-CO.
      *
           SET WK-LETTO-NO                TO TRUE.
      *
           PERFORM VARYING IX-TAB-E15 FROM 1 BY 1
             UNTIL IX-TAB-E15 > WE15-ELE-EST-RAPP-ANAG-MAX
               OR WK-LETTO-SI
               IF WE15-TP-OGG(IX-TAB-E15) = 'PO'
               AND C216-ID-POL-P(IX-VAL-VAR) = WE15-ID-OGG(IX-TAB-E15)
               AND WE15-TP-RAPP-ANA(IX-TAB-RAN) = 'CO'
                   SET WK-LETTO-SI              TO TRUE
                   MOVE WE15-DATI(IX-TAB-E15)
                     TO WKS-AREA-TABB-APPO
                   MOVE IX-TAB-E15
                     TO C214-IX-TABB
               END-IF
           END-PERFORM.
      *
       S1156-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE VARIABILE DEE
      *----------------------------------------------------------------*
       GESTIONE-VAR-P.

           IF C214-COD-VARIABILE-O = 'DEE'
           EVALUATE C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR)
               WHEN 'D'
               WHEN 'S'
               WHEN 'X'
                  MOVE C214-VAL-STR-O
                    TO C216-DEE
              END-EVALUATE

           END-IF.

       GESTIONE-VAR-P-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE VARIABILE DEE
      *----------------------------------------------------------------*
       GEST-VAR-P-OUT-DIN.

           IF C216-COD-VARIABILE-P(IX-VAL-VAR, IX-COD-VAR) = 'DEE'
              EVALUATE C216-TP-DATO-P(IX-VAL-VAR, IX-COD-VAR)
                WHEN 'D'
                  MOVE WKS-APPO-DATA
                    TO C216-DEE
              END-EVALUATE
           END-IF.

       GEST-VAR-P-OUT-DIN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE VARIABILI TRANCHE
      *----------------------------------------------------------------*
       GEST-VAR-T-OUT-DIN.

           IF C216-COD-VARIABILE-T(IX-VAL-VAR, IX-COD-VAR) = 'ITN'
              EVALUATE C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR)
                  WHEN 'A'
                  WHEN 'N'
                  WHEN 'I'
                     IF WKS-APPO-IMP IS NUMERIC
                        MOVE WKS-APPO-IMP
                          TO C216-FLG-ITN(IX-VAL-VAR)
                     ELSE
                        MOVE ZEROS
                          TO C216-FLG-ITN(IX-VAL-VAR)
                     END-IF

              END-EVALUATE
           END-IF

           IF C216-COD-VARIABILE-T(IX-VAL-VAR, IX-COD-VAR) =
                                                'TIPOTRANCHE'
              EVALUATE C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR)
                  WHEN 'A'
                  WHEN 'N'
                  WHEN 'I'
                     IF WKS-APPO-IMP IS NUMERIC
                        MOVE WKS-APPO-IMP
                          TO C216-TIPO-TRCH(IX-VAL-VAR)
                     ELSE
                        MOVE ZEROS
                          TO C216-TIPO-TRCH(IX-VAL-VAR)
                     END-IF

              END-EVALUATE
           END-IF.

       GEST-VAR-T-OUT-DIN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE VARIABILI TRANCHE
      *----------------------------------------------------------------*
       GESTIONE-VAR-T.

           IF C214-COD-VARIABILE-O = 'ITN'
              EVALUATE C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR)
                  WHEN 'A'
                  WHEN 'N'
                  WHEN 'I'
                     IF C214-VAL-IMP-O IS NUMERIC
                        MOVE C214-VAL-IMP-O
                          TO C216-FLG-ITN(IX-VAL-VAR)
                     ELSE
                        MOVE ZEROS
                          TO C216-FLG-ITN(IX-VAL-VAR)
                     END-IF

              END-EVALUATE

           END-IF

           IF C214-COD-VARIABILE-O = 'TIPOTRANCHE'
              EVALUATE C216-TP-DATO-T(IX-VAL-VAR, IX-COD-VAR)
                  WHEN 'A'
                  WHEN 'N'
                  WHEN 'I'
                     IF C214-VAL-IMP-O IS NUMERIC
                        MOVE C214-VAL-IMP-O
                          TO C216-TIPO-TRCH(IX-VAL-VAR)
                     ELSE
                        MOVE ZEROS
                          TO C216-TIPO-TRCH(IX-VAL-VAR)
                     END-IF

              END-EVALUATE

           END-IF.

       GESTIONE-VAR-T-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *    LCCC0005 VALORIZZA  TAB. OUTPUT
      *    LA SEGUENTE COPY VIENE UTLIZZARE PER GESTIRE LA VALORIZZAZIO
      *    NE PARAMETRICA DI TUTTE LE AREE DA VALORIZZARE IN OUTPUT
      *----------------------------------------------------------------*
           COPY IVVC0219.
      *----------------------------------------------------------------*
      *    LCCC0012 VALOUTORIZZA DCLGEN
      *    LA COPY SEGUENTE SERVE PER LA VALORIZZAZIONE PARAMETRICA
      *    DELL'AREA DI OUTPUT VALORIZZATA DAL DISPATCHER SULLE DCLGEN
      *----------------------------------------------------------------*
           COPY IVVC0220.

      *----------------------------------------------------------------*
      *    ROUTINES MOULI CALCOLO
      *----------------------------------------------------------------*
           COPY IVVC0214                REPLACING ==(SF)== BY ==C214==.
      *----------------------------------------------------------------*
      *    COPY PER LE LETTURE IN PTF
      *----------------------------------------------------------------*
      *
           COPY LCCVADE3                REPLACING ==(SF)== BY ==WADE==.
           COPY LCCVBEL3                REPLACING ==(SF)== BY ==WBEL==.
           COPY LCCVDCO3                REPLACING ==(SF)== BY ==WDCO==.
           COPY LCCVDFA3                REPLACING ==(SF)== BY ==WDFA==.
           COPY LCCVDEQ3                REPLACING ==(SF)== BY ==WDEQ==.
           COPY LCCVDTC3                REPLACING ==(SF)== BY ==WDTC==.
           COPY LCCVGRZ3                REPLACING ==(SF)== BY ==WGRZ==.
           COPY LCCVLQU3                REPLACING ==(SF)== BY ==WLQU==.
           COPY LCCVMOV3                REPLACING ==(SF)== BY ==WMOV==.
           COPY LCCVOCO3                REPLACING ==(SF)== BY ==WOCO==.
           COPY LCCVPMO3                REPLACING ==(SF)== BY ==WPMO==.
           COPY LCCVPOG3                REPLACING ==(SF)== BY ==WPOG==.
           COPY LCCVPOL3                REPLACING ==(SF)== BY ==WPOL==.
           COPY LCCVPRE3                REPLACING ==(SF)== BY ==WPRE==.
           COPY LCCVPVT3                REPLACING ==(SF)== BY ==WPVT==.
           COPY LCCVQUE3                REPLACING ==(SF)== BY ==WQUE==.
           COPY LCCVRAN3                REPLACING ==(SF)== BY ==WRAN==.
           COPY LCCVE153                REPLACING ==(SF)== BY ==WE15==.
           COPY LCCVRRE3                REPLACING ==(SF)== BY ==WRRE==.
           COPY LCCVRIC3                REPLACING ==(SF)== BY ==WRIC==.
           COPY LCCVSPG3                REPLACING ==(SF)== BY ==WSPG==.
           COPY LCCVSDI3                REPLACING ==(SF)== BY ==WSDI==.
           COPY LCCVTIT3                REPLACING ==(SF)== BY ==WTIT==.
           COPY LCCVTGA3                REPLACING ==(SF)== BY ==WTGA==.
           COPY LCCVBEP3                REPLACING ==(SF)== BY ==WBEP==.
           COPY LCCVDAD3                REPLACING ==(SF)== BY ==WDAD==.
           COPY LCCVMFZ3                REPLACING ==(SF)== BY ==WMFZ==.
           COPY LCCVRIF3                REPLACING ==(SF)== BY ==WRIF==.
           COPY LCCVTLI3                REPLACING ==(SF)== BY ==WTLI==.
           COPY LCCVPLI3                REPLACING ==(SF)== BY ==WPLI==.
           COPY LCCVGRL3                REPLACING ==(SF)== BY ==WGRL==.
RW16       COPY LCCVDFL3                REPLACING ==(SF)== BY ==WDFL==.
           COPY LCCVISO3                REPLACING ==(SF)== BY ==WISO==.
           COPY LCCVPCO3                REPLACING ==(SF)== BY ==WPCO==.
           COPY LCCVRST3                REPLACING ==(SF)== BY ==WRST==.
           COPY LCCVE123                REPLACING ==(SF)== BY ==WE12==.
           COPY LCCVL303                REPLACING ==(SF)== BY ==WL30==.
           COPY LCCVL233                REPLACING ==(SF)== BY ==WL23==.
NEWFIS     COPY LCCVP613                REPLACING ==(SF)== BY ==WP61==.
           COPY LCCVP673                REPLACING ==(SF)== BY ==WP67==.
           COPY LCCVP013                REPLACING ==(SF)== BY ==WP01==.

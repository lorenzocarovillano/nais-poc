      *  ============================================================= *
      *                                                                *
      *        PORTAFOGLIO VITA ITALIA  VER 1.0                        *
      *                                                                *
      *  ============================================================= *
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LOAS0310.
       AUTHOR.             ATS.
       DATE-WRITTEN.       SETTEMBRE 2007.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *     PROGRAMMA ..... LOAS0310                                   *
      *     TIPOLOGIA...... OPERAZIONI AUTOMATICHE                     *
      *     PROCESSO....... ADEGUAMENTO PREMIO PRESTAZIONE             *
      *     FUNZIONE....... BATCH                                      *
      *     DESCRIZIONE.... MODULO DI SECONDO LIVELLO (ELABORAZIONE)   *
      *     PAGINA WEB..... N.A.                                       *
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
      *
           SELECT OUTRIVA   ASSIGN    TO OUTRIVA
           FILE STATUS  IS FS-OUT.
      *
       DATA DIVISION.
       FILE SECTION.
      *
       FD  OUTRIVA
           LABEL RECORD STANDARD
           BLOCK  0  RECORDS.
       01  OUTRIVA-REC                    PIC X(500).
      *
       WORKING-STORAGE SECTION.
      *
      *----------------------------------------------------------------*
      *     FLAGS X GESTIONE DEBUG
      *----------------------------------------------------------------*
      *
      * 01 WK-DISPLAY-ATTIVA               PIC X(1) VALUE 'S'.
       01 TEST-I             PIC S9(4) VALUE 0.
       01 TEST-J             PIC S9(4) VALUE 0.
       01 WK-DATA            PIC 9(8)  VALUE 0.
       01 WK-ID              PIC 9(9)  VALUE 0.

      *----------------------------------------------------------------*
      *       COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
      *
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IEAV9903.
           COPY IDSV0002.

      *----------------------------------------------------------------*
      *    CAMPI X ROUTINES DI GESTIONE DISPLAY IDSP8888
      *----------------------------------------------------------------*
           COPY IDSV8888.
      *
      *----------------------------------------------------------------*
      *     COPY DELLE TABELLE DB2 PER IL DISPATCHER DATI
      *----------------------------------------------------------------*
      *
      *  --> MOVIMENTO
           COPY IDBVMOV1.
      *  --> AREA POLIZZA
           COPY IDBVPOL1.
      *  --> ADESIONE
           COPY IDBVADE1.
      *  --> GARANZIA
           COPY IDBVGRZ1.
      *  --> TRANCHE DI GARANZIA
           COPY IDBVTGA1.
      *  --> PARAMETRO COMPAGNIA
           COPY IDBVPCO1.
      *  --> STATO OGGETTO BUSINESS
           COPY IDBVSTB1.
      *  --> PARAMETRO OGGETTO
           COPY IDBVPOG1.
      *  --> RAPPORTO RETE
           COPY IDBVRRE1.
      *  --> RAPPORTO ANAGRAFICO
           COPY IDBVRAN1.
      *  --> TITOLO CONTABILE
           COPY IDBVTIT1.
      *  --> OGGETTO DEROGA
42781      COPY IDBVODE1.

      *  --> Where condition ad hoc pEr lettura Parametro Oggetto
           COPY LDBV1131.
      *  --> lettura Tranche Di Garanzia
           COPY LDBV0011.
      *  --> Where condition ad hoc per lettura Rapporto Rete
           COPY LDBV1211.
      *  --> Where condition ad hoc per lettura Rapporto Anagrafico
           COPY LDBV1291.
      *  --> Where condition ad hoc per lettura Titolo Contabile
13148      COPY LDBVD601.
      *  --> Where condition ad hoc per lettura Oggetto Deroga
42781      COPY LDBV3361.

      *  --> DETTAGLIO TITOLO CONTABILE
           COPY IDBVDTC1.

      *----------------------------------------------------------------*
      *   INDICI
      *----------------------------------------------------------------*
      *
       01  IX-INDICI.
      *
           03 IX-TAB-PMO                   PIC S9(04) COMP.
           03 IX-TAB-PMO-2                 PIC S9(04) COMP.
           03 IX-TAB-BLC                   PIC S9(04) COMP.
           03 IX-TAB-MOV                   PIC S9(04) COMP.
           03 IX-TAB-ADE                   PIC S9(04) COMP.
           03 IX-TAB-GRZ                   PIC S9(04) COMP.
           03 IX-TAB-TGA                   PIC S9(04) COMP.
           03 IX-TAB-POG                   PIC S9(04) COMP.
           03 IX-TAB-RRE                   PIC S9(04) COMP.
           03 IX-TAB-RAN                   PIC S9(04) COMP.
           03 IX-GRZ                       PIC S9(04) COMP.
      *
           03 IX-TAB-ERR                   PIC S9(04) COMP.
      *
           03 IX-AREA-SCHEDA               PIC S9(04) COMP.
           03 IX-TAB-VAR                   PIC S9(04) COMP.
      *
           03 IX-X-RIV-INC                 PIC S9(04) COMP.
      *
           03 IX-VPMO                      PIC S9(04) COMP.
           03 IX-W670                      PIC S9(04) COMP.
      *
           03 IX-TGA-W870                  PIC S9(04) COMP.
           03 IX-TIT-W870                  PIC S9(04) COMP.
      *
           03 IX-WK-VAR                    PIC S9(04) COMP.
           03 IX-TAB-PREC                  PIC S9(04) COMP.
      *
PERF       03 IX-WK-0040                   PIC S9(04) COMP.
      *----------------------------------------------------------------*
      *    DEFINIZIONE DI COSTANTI
      *----------------------------------------------------------------*
      *
      * --> Costanti per numero elementi massime aree tabelle
      *
       01  WK-COSTANTI-TABELLE.
           03 WK-MAX-ADE                     PIC S9(04) COMP VALUE 1.
           03 WK-MAX-MOV                     PIC S9(04) COMP VALUE 1.
           03 WK-TGA-MAX-C-W870              PIC S9(04) COMP VALUE 300.
           03 WK-MAX-TIT-W870                PIC S9(04) COMP VALUE 12.
           03 WK-MAX-VAR                     PIC S9(04) COMP VALUE 70.
      *
      * --> Comodi per string errore
      *
       01  WK-OGGETTO-9                      PIC 9(09).
       01  WK-OGGETTO-X                      PIC X(09).
       01  WK-LIV-DEROGA                     PIC X(10).
      *
      * --> Flag per scarto Garanzia x esito ko stato o causale
      *
       01  WK-SCARTA-GAR                PIC X.
           88 WK-SCARTA-GAR-SI            VALUE 'S'.
           88 WK-SCARTA-GAR-NO            VALUE 'N'.
      *
      * --> FLAG PER CARICARE AREA DEL VALORIZZATORE PER
      * --> RIVALUTAZIONE DI INCASSO
      *
       01  WK-TRANCHE-DA-CARICARE       PIC X.
           88  WK-TRANCHE-NON-CARICATA    VALUE 'N'.
           88  WK-TRANCHE-CARICATA        VALUE 'S'.
      *
      * --> Flag per scarto Tranche x esito ko stato o causale
      *
       01  WK-SCARTA-TGA                PIC X.
           88 WK-SCARTA-TGA-SI            VALUE 'S'.
           88 WK-SCARTA-TGA-NO            VALUE 'N'.

      * --> Ricerca occorrenza Blocco attiva
       01  WK-BLC-TROVATO               PIC X.
           88 WK-BLC-TROVATO-SI           VALUE 'S'.
           88 WK-BLC-TROVATO-NO           VALUE 'N'.

      * --> Ricerca occorrenza Stato Oggetto Business
       01  WK-STB-TROVATO               PIC X.
           88 WK-STB-TROV-SI              VALUE 'S'.
           88 WK-STB-TROV-NO              VALUE 'N'.

      * --> Ricerca occorrenza
       01  WK-TROVATO                   PIC X.
           88 WK-TROV-SI                  VALUE 'S'.
           88 WK-TROV-NO                  VALUE 'N'.
      *
      *   Rivalutazione per Incasso o per Emesso
      *
       01 WK-ORIG-RIV                   PIC X(02).
          88 WK-ORIG-RIV-IN               VALUE 'IN'.
          88 WK-ORIG-RIV-EM               VALUE 'EM'.
      *
      *   Flag Overflow
      *
       01 WCOM-OVERFLOW                 PIC X(02).
          88 WCOM-OVERFLOW-NO             VALUE 'NO'.
          88 WCOM-OVERFLOW-YES            VALUE 'SI'.
      *
      * -->  Flag fine fetch
      *
       01 WK-FINE-FETCH                 PIC X(01).
          88 WK-FINE-FETCH-SI             VALUE 'Y'.
          88 WK-FINE-FETCH-NO             VALUE 'N'.
      *
      * -->  Flag fine ciclo
      *
       01 WK-FINE-CICLO                 PIC X(01).
          88 WK-FINE-CICLO-SI             VALUE 'Y'.
          88 WK-FINE-CICLO-NO             VALUE 'N'.

      *
      * --> Ricerca occorrenza Titolo Contabile
      *
       01  WK-TIT-TROVATO               PIC X.
           88 WK-TIT-SI                   VALUE 'S'.
           88 WK-TIT-NO                   VALUE 'N'.

      * --> Flag garanzie di rendita
29401 *01 FL-GAR-REND                   PIC X(001) VALUE 'N'.
29401 *   88 SI-GAR-REND                  VALUE 'S'.
29401 *   88 NO-GAR-REND                  VALUE 'N'.
      *
ID67  * --> Flag per movimento gi trovato
      *
       01 FL-MOV-FOUND                  PIC X(001) VALUE 'N'.
          88 MOVIM-FOUND                  VALUE 'S'.
ID67      88 MOVIM-NOT-FOUND              VALUE 'N'.
      *
       01  WK-RICERCA-PERIODADEG             PIC X(001) VALUE 'N'.
           88 WK-TROVATO-PERIODADEG                     VALUE 'S'.
           88 WK-NON-TROVATO-PERIODADEG                 VALUE 'N'.

       01  WK-FRAZ-DECR-CPT                  PIC S9(5)V COMP-3 VALUE 0.
      * --> Ricerca occorrenza Garanzia
      *
       01  WK-GRZ-TROVATA               PIC X.
           88 WK-GRZ-SI                   VALUE 'S'.
           88 WK-GRZ-NO                   VALUE 'N'.

12888  01  WK-PREC-TROVATO              PIC X.
           88 PREC-TROVATO-NO             VALUE 'N'.
           88 PREC-TROVATO-SI             VALUE 'S'.

      * --> Flag fine cursore dettaglio titolo contabile
       01 SW-CUR-DTC                              PIC X.
          88 INIZ-CUR-DTC                         VALUE 'S'.
          88 FINE-CUR-DTC                         VALUE 'N'.

      * --> Flag fine cursore dettaglio titolo contabile
       01 SW-TROVATO-DTC                          PIC X.
          88 DETTAGLIO-TROVATO                    VALUE 'S'.
          88 DETTAGLIO-NON-TROVATO                VALUE 'N'.

PERF  *--> VERIFICA PRESENZA AREA CACHE PRODOTTO
PERF   01 WK-FLAG-PROD                      PIC X(002).
PERF      88 PROD-OK                       VALUE 'SI'.
PERF      88 PROD-KO                       VALUE 'NO'.
PERF  *
PERF  *-----------------------------------------------------------------
PERF  *  AREA CACHE DATI PRODOTTO
PERF  *-----------------------------------------------------------------
PERF   01 WK-AREA-DATI-PROD.
PERF      03 WK-0040-ELE-MAX                PIC S9(04) COMP.
PERF      03 WK-0040-TABELLA                OCCURS 500.
PERF      05 WK-0040-DATI-INPUT.
PERF         07 WK-0040-COD-COMPAGNIA                  PIC 9(005).
PERF         07 WK-0040-COD-PRODOTTO                   PIC X(012).
PERF         07 WK-0040-COD-CONVENZIONE                PIC X(012).
PERF         07 WK-0040-DATA-INIZ-VALID-CONV           PIC X(008).
PERF         07 WK-0040-DATA-RIFERIMENTO               PIC X(008).
PERF         07 WK-0040-DATA-EMISSIONE                 PIC X(008).
PERF         07 WK-0040-DATA-PROPOSTA                  PIC X(008).
PERF         07 WK-0040-LIVELLO-UTENTE                 PIC 9(002).
PERF         07 WK-0040-FUNZIONALITA                   PIC 9(005).
PERF         07 WK-0040-SESSION-ID                     PIC X(020).
PERF      05 WK-0040-DATI-OUTPUT.
PERF         07 WK-0040-DATA-VERSIONE-PROD             PIC X(008).

PERF      01 WS-0040-DATI-INPUT.
PERF         03 WS-0040-COD-COMPAGNIA                  PIC 9(005).
PERF         03 WS-0040-COD-PRODOTTO                   PIC X(012).
PERF         03 WS-0040-COD-CONVENZIONE                PIC X(012).
PERF         03 WS-0040-DATA-INIZ-VALID-CONV           PIC X(008).
PERF         03 WS-0040-DATA-RIFERIMENTO               PIC X(008).
PERF         03 WS-0040-DATA-EMISSIONE                 PIC X(008).
PERF         03 WS-0040-DATA-PROPOSTA                  PIC X(008).
PERF         03 WS-0040-LIVELLO-UTENTE                 PIC 9(002).
PERF         03 WS-0040-FUNZIONALITA                   PIC 9(005).
PERF         03 WS-0040-SESSION-ID                     PIC X(020).
      *----------------------------------------------------------------*
      *     AREE VARIE
      *----------------------------------------------------------------*
      *
      * --> Area di appoggio per i messaggi di errore
      *
       01 WK-GESTIONE-MSG-ERR.
          03 WK-COD-ERR                 PIC X(06) VALUE ZEROES.
          03 WK-LABEL-ERR               PIC X(30) VALUE SPACES.
          03 WK-STRING                  PIC X(85) VALUE SPACES.
      *
ALPO   01 WK-APPO-OGB-ID-OGG-BLOCCO     PIC S9(9) VALUE ZEROES.
ALPO   01 WK-DT-EFF-BLC                 PIC S9(8) VALUE ZEROES.
      *
       01 WK-APPO-DT-RICOR-SUCC         PIC 9(08) VALUE ZEROES.
      *
       01 WK-APPO-DT-NUM                PIC 9(08) VALUE ZEROES.
      *
21315  01 WK-DT-RICOR-PREC              PIC 9(8) VALUE ZEROES.
 "     01 WK-DT-RICOR-SUCC              PIC 9(8) VALUE ZEROES.
      *
       01 WK-STB-ID-OGG                 PIC S9(09) COMP-3.
       01 WK-STB-TP-OGG                 PIC  X(02).
      *
       01 WK-APPO-DT.
          03 WK-APPO-DT-AA              PIC 9(04) VALUE ZEROES.
          03 WK-APPO-DT-MM              PIC 9(02) VALUE ZEROES.
          03 WK-APPO-DT-GG              PIC 9(02) VALUE ZEROES.
      *
       01 WK-APPO-MM                    PIC S9(02) VALUE ZEROES.
      *
       01 WK-TABELLA                    PIC X(8) VALUE SPACES.
      *
       01 WK-PGM-CALL                   PIC X(08).
      *
      * --> File Status file di output
perf  *
perf   77 WK-0040-ELEMENTI-MAX          PIC 9(003)  VALUE 500.
perf  *
       77 FS-OUT                        PIC X(002)  VALUE '00'.
      * changes SIR FCTVI00011410 starts here
       01 WS-FS-OUT                     PIC X(002)  VALUE '00'.
      * changes SIR FCTVI00011410 ends here
      *
      * --> Nome del programma Businesss Service
      *
       01 WK-PGM                        PIC X(08) VALUE 'LOAS0310'.
      *
      * --> Area di accept della data dalla variabile di sistema DATE
      *
       01 WK-CURRENT-DATE               PIC 9(08) VALUE ZEROES.
      *
      * --> Area di appoggio per gli identificativi
      * --> degli oggetti lavorati
      *
       01 WK-APPO-ID-GAR                PIC S9(9) COMP-3 VALUE ZEROES.
      *
      * --> Area di appoggio per gestione Management Fee
      *
       01 WK-GG-INC                     PIC 9(05).
       01 WK-PERMANFEE                  PIC 9(05).
       01 WK-MESIDIFFMFEE               PIC 9(05).
      *
       01 WK-TOT-MANFEE                 PIC S9(12)V9(3) COMP-3.
      *
       01 WK-ID-POLI-DISPLAY            PIC 9(09).
       01 WK-IB-POLI-DISPLAY            PIC X(11).
      *
      *----------------------------------------------------------------*
      * DEFINIZIONE AREA DI APPOGGIO PER FORMATTAZIONE DATE
      *----------------------------------------------------------------*
      *
       01 WK-DATA-AMG.
          05 AAAA-SYS                   PIC X(004) VALUE ZEROES.
          05 MM-SYS                     PIC X(002) VALUE ZEROES.
          05 GG-SYS                     PIC X(002) VALUE ZEROES.
      *
       01 WK-ORA-HMS.
          05 HH-SYS                     PIC X(002) VALUE ZEROES.
          05 MI-SYS                     PIC X(002) VALUE ZEROES.
          05 SS-SYS                     PIC X(002) VALUE ZEROES.
          05 CS-SYS                     PIC X(002) VALUE ZEROES.
      *
       01 WK-DATA-SISTEMA.
          05 GG-SYS                     PIC X(002) VALUE SPACES.
          05 FILLER                     PIC X(001) VALUE '/'.
          05 MM-SYS                     PIC X(002) VALUE SPACES.
          05 FILLER                     PIC X(001) VALUE '/'.
          05 AAAA-SYS                   PIC X(004).
      *
       01 WK-ORA-SISTEMA.
          05 HH-SYS                     PIC X(002) VALUE SPACES.
          05 FILLER                     PIC X(001) VALUE '.'.
          05 MI-SYS                     PIC X(002) VALUE SPACES.
          05 FILLER                     PIC X(001) VALUE '.'.
          05 SS-SYS                     PIC X(002).
      *
      *----------------------------------------------------------------*
      *    AREA CALCOLO DIFFERENZA TRA DATE
      *----------------------------------------------------------------*
      *
      *  --> Area per gestione della differenza tra date
      *
       01 FORMATO                       PIC X(01) VALUE ZEROES.
       01 DATA-INFERIORE.
          05 AAAA-INF                   PIC 9(04) VALUE ZEROES.
          05 MM-INF                     PIC 9(02) VALUE ZEROES.
          05 GG-INF                     PIC 9(02) VALUE ZEROES.
       01 DATA-SUPERIORE.
          05 AAAA-SUP                   PIC 9(04) VALUE ZEROES.
          05 MM-SUP                     PIC 9(02) VALUE ZEROES.
          05 GG-SUP                     PIC 9(02) VALUE ZEROES.
       01 GG-DIFF                       PIC 9(05) VALUE ZEROES.
       01 CODICE-RITORNO                PIC X(01) VALUE ZEROES.
       01 WK-COM-AAMM.
          05 WK-COM-AA                  PIC 9(04) VALUE ZEROES.
          05 WK-COM-MM                  PIC 9(02) VALUE ZEROES.
       01 DURATA.
          05 DUR-AAAA                   PIC 9(04) VALUE ZEROES.
          05 DUR-MM                     PIC 9(02) VALUE ZEROES.
          05 DUR-GG                     PIC 9(02) VALUE ZEROES.
      *
      *  --> Area per gestione resti operazioni
      *
       01 WK-OPERAZIONI.
          03 WK-APPO-RESTO              PIC S9(5)V9(5) VALUE ZEROES.
          03 WK-APPO-RESTO1             PIC S9(5)V9(5) VALUE ZEROES.
      *
      *----------------------------------------------------------------*
      *     AREE NECESSARIE AL VALORIZZATORE VARIABILI
      *----------------------------------------------------------------*
      *
      *  -- AREA SCHEDA PER SERVIZI DI PRODOTTO
       01 WSKD-AREA-SCHEDA.
              COPY IVVC0216              REPLACING ==(SF)== BY ==WSKD==.
      *  -- AREA OPZIONI
       01 WOPZ-AREA-OPZIONI.
              COPY IVVC0217              REPLACING ==(SF)== BY ==WOPZ==.
      *  -- AREA DATI CONTESTUALI
       01 WCNT-AREA-DATI-CONTEST.
              COPY IVVC0212              REPLACING ==(SF)== BY ==WCNT==.
      *  --> AREA IO VALORIZZATORE VARIABILE
       01 AREA-IO-IVVS0211.
              COPY IVVC0211              REPLACING ==(SF)== BY ==S211==.
      *  --> AREA ALIAS
       01 AREA-ALIAS.
              COPY IVVC0218              REPLACING ==(SF)== BY ==S211==.

      *
       01  WK-VARIABILI.
           03 WK-APPO-LUNGHEZZA          PIC 9(08).
      *  -->  AREA VARIABILI
           03 WK-AREA-VAR.
              COPY LCCC0211              REPLACING ==(SF)== BY ==WK==.
      *
      *----------------------------------------------------------------*
      *  AREE PER LA GESTIONE DEL FILE DI OUTPUT
      *----------------------------------------------------------------*
      *
      * --> Copy del tracciato record del file di output
      *
       01 W-REC-OUTRIVA.
           COPY LOAR0171.

      * COPY ELE MAX PER TAB PTF
           COPY LCCVGRZZ.
           COPY LCCVTGAZ.

      *----------------------------------------------------------------*
      *     MODULI CHIAMATI
      *----------------------------------------------------------------*
      *
      *  --> SERVIZIO ADEGUAMENTO PREMIO PRESTAZIONE
       01  LOAS0660                         PIC X(8) VALUE 'LOAS0660'.
      *  --> SERVIZIO DETERMINA TIPO RIVALUTAZIONE INCASSO - COLLETT.
       01  LOAS0670                         PIC X(8) VALUE 'LOAS0670'.
      *  --> SERVIZIO DETERMINA TIPO RIVALUTAZIONE INCASSO - INDIVID.
       01  LOAS0870                         PIC X(8) VALUE 'LOAS0870'.
      *  --> SERVIZIO SCRITTURA FILE MANAGEMENT FEE
       01  LOAS0820                         PIC X(8) VALUE 'LOAS0820'.
      *  --> MAIN VALORIZZATORE VARIABILI
       01  LCCS0211                         PIC X(8) VALUE 'LCCS0211'.
      *  --> MODULO EOC
       01  LOAS0320                         PIC X(8) VALUE 'LOAS0320'.
      *  --> MODULO PER CALCOLI SU DATE
       01  LCCS0003                         PIC X(8) VALUE 'LCCS0003'.
      *  --> MODULO PER VERIFICA PRESENZA BLOCCHI
       01  LCCS0022                         PIC X(8) VALUE 'LCCS0022'.
      *  --> MODULO PER VERIFICA PRESENZA MOVIMENTI FUTURI BLOCCANTI
       01  LCCS0023                         PIC X(8) VALUE 'LCCS0023'.
      *  --> MODULO PER VERIFICA PRESENZA DEROGA BLOCCANTE
       01  LOAS0280                         PIC X(8) VALUE 'LOAS0280'.
      *  --> MODULO CALCOLO RICORRENZA SUCCESSIVA
       01  LCCS0320                         PIC X(8) VALUE 'LCCS0320'.
      *  --> MODULO PER LETTURA TABELLA OGGETTO DEROGA
42781  01  LDBS3360                         PIC X(8) VALUE 'LDBS3360'.
      *
ESPE   01  ISPS0040                         PIC X(8) VALUE 'ISPS0040'.
      *
      *----------------------------------------------------------------*
      * AREA DI INPUT SERVIZIO ADEGUAMENTO PREMIO PRESTAZIONE
      *----------------------------------------------------------------*
      *
       01 W660-AREA-LOAS0660.
       COPY LOAC0660                     REPLACING ==(SF)== BY ==W660==.
      *
      *----------------------------------------------------------------*
      * AREA DI INPUT SERVIZIO DETERMINA TIPO RIVALUTAZIONE RICH90
      *----------------------------------------------------------------*
      *
       01 W670-AREA-LOAS0670.
       COPY LOAC0670                     REPLACING ==(SF)== BY ==W670==.
      *
      *----------------------------------------------------------------*
      * AREA DI INPUT SERVIZIO DETERMINA TIPO RIVALUTAZIONE RICH90
      *----------------------------------------------------------------*
      *
       01 W870-AREA-LOAS0870.
       COPY LOAC0870                     REPLACING ==(SF)== BY ==W870==.
      *
      *----------------------------------------------------------------*
      *    AREA DI INPUT ROUTINE  DI GESTIONE DEROGHE BLOCCANTI
      *----------------------------------------------------------------*
42781      COPY LOAC0002.
      *----------------------------------------------------------------*
      *     COPY DISPATCHER
      *----------------------------------------------------------------*
      *
       01  IDSV0012.
           COPY IDSV0012.
      *
       01  DISPATCHER-VARIABLES.
      *  COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *  COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *
       COPY IDSV0003.
      *
      *----------------------------------------------------------------*
      *     AREA SERVIZIO DI PRODOTTO  "DATI PRODOTTO"
      *----------------------------------------------------------------*
      *
       01 AREA-IO-ISPS0040.
           COPY ISPC0040.
      *
      *-----------------------------------------------------------------
      * AREA COPY PROC. LOAP0001
      *-----------------------------------------------------------------
      *
      *-->  Identificativo Polizza
       01 LOAP1-ID-POLI                     PIC S9(09) COMP-3.
      *-->  Identificativo Adesione
       01 LOAP1-ID-ADES                     PIC S9(09) COMP-3.
      *
      *--> AREA PER MODULO CONVERSIONE STRINGA
      *
       01  AREA-CALL-IWFS0050.
           COPY IWFI0051.
           COPY IWFO0051.
      *
      *----------------------------------------------------------------*
      *    CAMPO PER CHIAMATA AL MODULO LOAS0820
      *----------------------------------------------------------------*
       01  WAPPL-NUM-ELAB.
           03  WAPPL-ELAB-INIZIATA       PIC X(1).
               88   WAPPL-INIZIO         VALUE '1'.

      *----------------------------------------------------------------*
      *    COPY PER SERVIZIO VERIFICA PRESENZA BLOCCHI
      *    IL SERVIZIO E' INOVCATO NELLA COPY DI PROCEDURE LOAP0001
      *----------------------------------------------------------------*
      *
       01 AREA-IO-LCCS0022.
          COPY LCCC0022.
      *
      *----------------------------------------------------------------*
      *    COPY PER SERVIZIO VERIFICA PRESENZA MOVIMENTI FUTURI
      *    IL SERVIZIO E' INOVCATO NELLA COPY DI PROCEDURE LOAP0001
      *----------------------------------------------------------------*
      *
       01 AREA-IO-LCCS0023.
          COPY LCCC0023.
      *
      *----------------------------------------------------------------*
      *    COPY PER SERVIZIO VERIFICA PRESENZA DEROGHE
      *    IL SERVIZIO E' INOVCATO NELLA COPY DI PROCEDURE LOAP0001
      *----------------------------------------------------------------*
      *
       01 AREA-IO-LOAS0280.
          COPY LOAC0280.
      *
      *----------------------------------------------------------------*
      *    AREA IO SERVIZIO CALCOLI SULLE DATE
      *----------------------------------------------------------------*
      *
          COPY LCCC0003.
      *
       01 IN-RCODE                       PIC 9(2).
       01 WK-DATA-CALCOLATA              PIC 9(8) VALUE ZEROES.
      *
      *----------------------------------------------------------------*
      *  AREE TIPOLOGICHE DI PORTAFOGLIO
      *----------------------------------------------------------------*
      *
           COPY LCCVXFA0.
           COPY LCCVXOG0.
           COPY LCCVXCA0.
           COPY LCCVXSB0.

VSTEF *    MODIFICA SIR CQPrd00019650
           COPY ISPV0000.

      *
      *----------------------------------------------------------------*
      *    WORK-COMMAREA DI APPOGGIO PER CONTROLLI E CALCOLI
      *----------------------------------------------------------------*
      *
       01  WORK-COMMAREA.
      * -- AREA APPOGGIO PARAMETRO MOVIMENTO
          03 VPMO-AREA-PARAM-MOVI.
             04 VPMO-ELE-PARAM-MOV-MAX   PIC S9(04) COMP.
                COPY LCCVPMOA REPLACING   ==(SF)==  BY ==VPMO==.
             COPY LCCVPMO1               REPLACING ==(SF)== BY ==VPMO==.
      *----------------------------------------------------------------*
      *    AREA TABELLE RIVALUTAZIONE DA PASSARE AL VALORIZZATORE
      *----------------------------------------------------------------*
      *
      * --  AREA GARANZIA
       01 VGRZ-AREA-GARANZIA.
          04 VGRZ-ELE-GAR-MAX        PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==VGRZ==.
             COPY LCCVGRZ1              REPLACING ==(SF)== BY ==VGRZ==.

      *--  AREA TRANCHE DI GARANZIA
       01 VTGA-AREA-TRANCHE.
          03 VTGA-ELE-TRAN-MAX       PIC S9(04) COMP.
          03  VTGA-TAB.
            COPY LCCVTGAC REPLACING   ==(SF)==  BY ==VTGA==.
           COPY LCCVTGA1              REPLACING ==(SF)== BY ==VTGA==.
          03  VTGA-TAB-R REDEFINES VTGA-TAB.
           04 FILLER                  PIC X(911).
           04 VTGA-RESTO-TAB          PIC X(272389).
      *
      *----------------------------------------------------------------*
      *    AREA TABELLE RIVALUTAZIONE CARICATE E CALCOLATE
      *----------------------------------------------------------------*
      *
      * --  AREA GARANZIA
       01 WGRZ-AREA-GARANZIA.
          04 WGRZ-ELE-GAR-MAX        PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
             COPY LCCVGRZ1              REPLACING ==(SF)== BY ==WGRZ==.

      *--  AREA TRANCHE DI GARANZIA
       01 WTGA-AREA-TRANCHE.
          03 WTGA-ELE-TRAN-MAX       PIC S9(04) COMP.
          03  WTGA-TAB.
             COPY LCCVTGAC REPLACING   ==(SF)==  BY ==WTGA==.
           COPY LCCVTGA1              REPLACING ==(SF)== BY ==WTGA==.
          03  WTGA-TAB-R REDEFINES WTGA-TAB.
           04 FILLER                  PIC X(911).
           04 WTGA-RESTO-TAB          PIC X(1046739).
      *
      *--  AREA TRANCHE DI GARANZIA CONTENENTE LA PRESTAZIONE PRE-RIVAL
12888  01 WPREC-AREA-TRANCHE.
          03  WPREC-TAB.
           04 WPREC-TAB-TRAN           OCCURS 300.
              07 WPREC-ID-TRCH-DI-GAR  PIC S9(9)     COMP-3.
              07 WPREC-PRSTZ-ULT       PIC S9(12)V9(3) COMP-3.
              07 WPREC-PRSTZ-ULT-NULL  REDEFINES
                 WPREC-PRSTZ-ULT       PIC X(8).

      *
      *--  RAPPORTO ANAGRAFICO
       01 WRAN-AREA-RAPP-ANAG.
          04 WRAN-ELE-RAPP-ANAG-MAX     PIC S9(004) COMP.
                COPY LCCVRANA REPLACING   ==(SF)==  BY ==WRAN==.
             COPY LCCVRAN1              REPLACING ==(SF)==
                                               BY ==WRAN==.
      * --> Area di appoggio tabella MOVI
42781  01 ZMOV-AREA-MOVIMENTO.
42781     04 ZMOV-ELE-MOVI-MAX        PIC S9(04) COMP.
42781     04 ZMOV-TAB-MOV.
42781     COPY LCCVMOV1              REPLACING ==(SF)== BY ==ZMOV==.
      *----------------------------------------------------------------*
      *    INTERFACCIA PER SERVIZIO GESTORE RENDITE
      *----------------------------------------------------------------*
29401 *01 AREA-IO-LREC0003.
29401 *   COPY LREC0003.
VSTEF *----------------------------------------------------------------*
      *    COPY IDSV0015
      *----------------------------------------------------------------*
           COPY IDSV0015.
      *----------------------------------------------------------------*
      *        L I N K A G E   S E C T I O N
      *----------------------------------------------------------------*
      *
       LINKAGE SECTION.
      *
      *----------------------------------------------------------------*
      *   AREA-IDSV0001
      *----------------------------------------------------------------*
      *
       01  AREA-IDSV0001.
           COPY IDSV0001.
      *
      *----------------------------------------------------------------*
      *  AREA DATI
      *----------------------------------------------------------------*
      *
       01  IABV0006.
           COPY IABV0006.
      *
      *----------------------------------------------------------------*
      *  AREA DI PASSAGGIO DA-A MAIN
      *----------------------------------------------------------------*
      *
       01 AREA-MAIN.
      * -- Area parametro movimento
          03 WPMO-AREA-PARAM-MOVI.
             04 WPMO-ELE-PARAM-MOV-MAX   PIC S9(04) COMP.
                COPY LCCVPMOA REPLACING   ==(SF)==  BY ==WPMO==.
             COPY LCCVPMO1               REPLACING ==(SF)== BY ==WPMO==.
      * -- Area polizza
          03 WPOL-AREA-POLIZZA.
             04 WPOL-ELE-POLI-MAX        PIC S9(04) COMP.
             04 WPOL-TAB-POL.
             COPY LCCVPOL1               REPLACING ==(SF)== BY ==WPOL==.
      * -- Area Adesione
          03 WADE-AREA-ADESIONE.
             04 WADE-ELE-ADES-MAX        PIC S9(04) COMP.
             04 WADE-TAB-ADE             OCCURS 1.
             COPY LCCVADE1               REPLACING ==(SF)== BY ==WADE==.
      * --> Area Movimento
          03 WMOV-AREA-MOVIMENTO.
             04 WMOV-ELE-MOVI-MAX        PIC S9(04) COMP.
             04 WMOV-TAB-MOV.
             COPY LCCVMOV1              REPLACING ==(SF)== BY ==WMOV==.
      *
      * -- Intervallo Di Elaborazione
      *
       01 WCOM-INTERVALLO-ELAB.
           03 WCOM-PERIODO-ELAB-DA      PIC 9(08).
           03 WCOM-PERIODO-ELAB-A       PIC 9(08).
      *
       01 WCOM-IO-STATI.
           03 WCOM-AREA-STATI.
              COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.
           03 WCOM-AREA-LCCC0261.
              COPY LCCC0261              REPLACING ==(SF)== BY ==WCOM==.
      *
      *  ====================  M A I N    L I N E  =================== *
      *
       PROCEDURE DIVISION USING AREA-IDSV0001
                                IABV0006
                                AREA-MAIN
                                WCOM-INTERVALLO-ELAB
                                WCOM-IO-STATI.
      *
           IF IABV0006-ULTIMO-LANCIO
              PERFORM S90200-CLOSE-OUT
                 THRU S90200-CLOSE-OUT-EX
           ELSE
              PERFORM S00000-OPERAZ-INIZIALI
                 THRU S00000-OPERAZ-INIZIALI-EX
      *
              IF IDSV0001-ESITO-OK
              AND WCOM-WRITE-OK
      *
                 PERFORM S10000-ELABORAZIONE
                    THRU S10000-ELABORAZIONE-EX
      *
              END-IF
      *
              PERFORM S90000-OPERAZ-FINALI
                 THRU S90000-OPERAZ-FINALI-EX
           END-IF.
      *
           GOBACK.
      *
      * ============================================================== *
      * -->           O P E R Z I O N I   I N I Z I A L I          <-- *
      * ============================================================== *
      *
       S00000-OPERAZ-INIZIALI.
      *
      *    DISPLAY IDSV0001-LIVELLO-DEBUG
           MOVE 'S00000-OPERAZ-INIZIALI'   TO WK-LABEL-ERR
      *
ALEX  *    INITIALIZE                         WCNT-AREA-DATI-CONTEST
      *
      *     Inizializzazione area dati contestuali
      *
           INITIALIZE                         WCNT-ELE-VAR-CONT-MAX
                                              WCNT-TAB-VAR-CONT(1).

           MOVE  WCNT-TAB-VAR              TO WCNT-RESTO-TAB-VAR.
13573 *    SET NO-GAR-REND                 TO TRUE.
29401 *    SET NO-GAR-REND                 TO TRUE
           MOVE WPMO-ID-OGG(1)
             TO IABV0006-OGG-BUSINESS
           MOVE WPMO-TP-OGG(1)
             TO IABV0006-TP-OGG-BUSINESS
           MOVE WPMO-ID-POLI(1)
             TO IABV0006-ID-POLI.
           MOVE WPMO-ID-ADES(1)
             TO IABV0006-ID-ADES.
           MOVE WPMO-COD-RAMO(1)
             TO ISPV0000-COD-RAMO.
           MOVE IDSV0001-DATA-EFFETTO
             TO IABV0006-DT-EFF-BUSINESS.
      *
DBG        PERFORM DISPLAY-LABEL
DBG           THRU DISPLAY-LABEL-EX.
      *
      * --> Inizializzazione dell'area comune
      *
ALPO       PERFORM S00090-INIZ-AREA-COMUNE
ALPO          THRU S00090-INIZ-AREA-COMUNE-EX.


      * --> Inizializazione dell'area errori di tipo deroga
      *
           PERFORM INIZIA-AREA-ERR-DEROGA
              THRU INIZIA-AREA-ERR-DEROGA-EX.
      *
      * --> Inizializazione delle aree di working storage
      *
           PERFORM S00200-INIZIA-AREE-WS
              THRU S00200-INIZIA-AREE-WS-EX.
      *
      * --> Inizializazione di tutte le tabelle che verranno scritte
      *
           PERFORM S00300-INIZ-AREE-TABELLE
              THRU S00300-INIZ-AREE-TABELLE-EX.
      *
ALPO       IF ALPO-CALL
      *
ALPO          PERFORM S00340-TRATTA-MOVI
ALPO             THRU S00340-TRATTA-MOVI-EX
      *
ALPO       ELSE
      *
ALPO          PERFORM S00350-CREA-MOVI-FITTIZIO
ALPO             THRU S00350-CREA-MOVI-FITTIZIO-EX

ALPO       END-IF.
      *
      * --> CONTROLLI
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S00400-CONTROLLI
                 THRU S00400-CONTROLLI-EX
      *
           END-IF.
      *
PERF       IF IABV0006-PRIMO-LANCIO
PERF  *
PERF          INITIALIZE WK-AREA-DATI-PROD
PERF  *
PERF       END-IF.
      *
      * --> Determinazione della Data Validit Prodotto
      *
           IF IDSV0001-ESITO-OK
           AND WCOM-WRITE-OK
      *
              PERFORM S00600-GESTIONE-DATA-PROD
                 THRU S00600-GESTIONE-DATA-PROD-EX
           END-IF.
      *
      * --> Determinazione del tipo di Rivalutazione:
      * --> "Per Incasso" o "Per Emesso"
      *
           IF IDSV0001-ESITO-OK
           AND WCOM-WRITE-OK

              PERFORM S00700-DETERMINA-ORIG-RIV
                 THRU S00700-DETERMINA-ORIG-RIV-EX

           END-IF.
      *
      * --> Apertura del file di output
      *
           IF IABV0006-PRIMO-LANCIO
      *
              PERFORM S00800-OPEN-OUT
                 THRU S00800-OPEN-OUT-EX

           END-IF.
      *
           PERFORM S00900-VALOR-IB-OGG-MOV
              THRU S00900-VALOR-IB-OGG-MOV-EX.
      *
DBG        PERFORM DISPLAY-IB-OGG
DBG           THRU DISPLAY-IB-OGG-EX.
      *
           MOVE WPMO-TP-FRM-ASSVA(1)
             TO WS-TP-FRM-ASSVA.
      *
       S00000-OPERAZ-INIZIALI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *       LETTURA MOVIMENTO
      *----------------------------------------------------------------*
      *
ALPO   S00340-TRATTA-MOVI.
ALPO        MOVE 'S00340-TRATTA-MOVI'   TO WK-LABEL-ERR.
DBG         PERFORM DISPLAY-LABEL
DBG            THRU DISPLAY-LABEL-EX.

ALPO        PERFORM S00342-PREPARA-MOVI
ALPO           THRU S00342-PREPARA-MOVI-EX.

ALPO        PERFORM S00344-LEGGI-MOVIMENTO
ALPO           THRU S00344-LEGGI-MOVIMENTO-EX.

ALPO   S00340-TRATTA-MOVI-EX.
ALPO        EXIT.
      *----------------------------------------------------------------*
      * PREPARA AREA LETTURA MOVIMENTO
      *----------------------------------------------------------------*
ALPO   S00342-PREPARA-MOVI.
ALPO       MOVE 'S00342-PREPARA-MOVI'   TO WK-LABEL-ERR.
DBG        PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX.

ALPO       INITIALIZE                       MOVI.
ALPO       SET IDSI0011-SELECT           TO TRUE.
ALPO       SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
ALPO       SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
ALPO       SET IDSI0011-PRIMARY-KEY      TO TRUE.
ALPO       SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
ALPO       MOVE WCOM-ID-MOVI-CREAZ       TO MOV-ID-MOVI.
ALPO       MOVE 'MOVI'                   TO IDSI0011-CODICE-STR-DATO.
ALPO       MOVE MOVI                     TO IDSI0011-BUFFER-DATI.
ALPO       MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
ALPO       MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
ALPO                                        IDSI0011-DATA-FINE-EFFETTO
ALPO                                        IDSI0011-DATA-COMPETENZA.
ALPO   S00342-PREPARA-MOVI-EX.
            EXIT.
      *----------------------------------------------------------------*
      * LETTURA MOVIMENTO                                              *
      *----------------------------------------------------------------*
ALPO   S00344-LEGGI-MOVIMENTO.
ALPO       MOVE 'S00344-LEGGI-MOVIMENTO'  TO WK-LABEL-ERR.
ALPO       PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX.
ALPO       MOVE 'MOVI'          TO WK-TABELLA.

ALPO       PERFORM CALL-DISPATCHER
ALPO          THRU CALL-DISPATCHER-EX.

ALPO          IF IDSO0011-SUCCESSFUL-RC
ALPO            EVALUATE TRUE
ALPO                 WHEN IDSO0011-SUCCESSFUL-SQL
ALPO  *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
ALPO                    MOVE IDSO0011-BUFFER-DATI
ALPO                      TO MOVI
ALPO                    MOVE 1
ALPO                      TO WMOV-ELE-MOVI-MAX
ALPO                    PERFORM VALORIZZA-OUTPUT-MOV
ALPO                       THRU VALORIZZA-OUTPUT-MOV-EX
ALPO                    SET WMOV-ST-INV
ALPO                     TO TRUE
ALPO                 WHEN IDSO0011-NOT-FOUND
ALPO  *--->          CAMPO $ NON TROVATO
ALPO                        MOVE WK-PGM
ALPO                         TO IEAI9901-COD-SERVIZIO-BE
ALPO                       MOVE WK-LABEL-ERR
ALPO                         TO IEAI9901-LABEL-ERR
ALPO                       MOVE '005019'
ALPO                         TO IEAI9901-COD-ERRORE
ALPO                       MOVE 'ID-MOVI'
ALPO                         TO IEAI9901-PARAMETRI-ERR
ALPO                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
ALPO                          THRU EX-S0300
ALPO                  WHEN OTHER
ALPO  *--->          ERRORE DI ACCESSO AL DB
ALPO                    MOVE WK-PGM
ALPO                                    TO IEAI9901-COD-SERVIZIO-BE
ALPO                    MOVE WK-LABEL-ERR
ALPO                                    TO IEAI9901-LABEL-ERR
ALPO                    MOVE '005015'   TO IEAI9901-COD-ERRORE
ALPO                    MOVE WK-TABELLA TO IEAI9901-PARAMETRI-ERR
ALPO                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
ALPO                       THRU EX-S0300
ALPO             END-EVALUATE
ALPO          ELSE
ALPO  *-->       GESTIRE ERRORE DISPATCHER
ALPO             MOVE WK-PGM
ALPO               TO IEAI9901-COD-SERVIZIO-BE
ALPO             MOVE WK-LABEL-ERR
ALPO               TO IEAI9901-LABEL-ERR
ALPO             MOVE '005016'
ALPO               TO IEAI9901-COD-ERRORE
ALPO             MOVE SPACES
ALPO               TO IEAI9901-PARAMETRI-ERR
ALPO             PERFORM S0300-RICERCA-GRAVITA-ERRORE
ALPO                THRU EX-S0300
ALPO          END-IF.

ALPO   S00344-LEGGI-MOVIMENTO-EX.
ALPO          EXIT.
      *----------------------------------------------------------------*
      *                    CREAZIONE DEL MOVIMENTO FITTIZIO            *
      *----------------------------------------------------------------*
      *
       S00350-CREA-MOVI-FITTIZIO.
      *
           MOVE 'S00350-CREA-MOVI-FITTIZIO'
             TO WK-LABEL-ERR.
      *
DBG        PERFORM DISPLAY-LABEL
DBG           THRU DISPLAY-LABEL-EX.
      *
           SET WMOV-ST-ADD
             TO TRUE.
      *
           MOVE 1
             TO WMOV-ELE-MOVI-MAX
      *
           MOVE 1
             TO WMOV-ID-MOVI.
      *
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO WMOV-COD-COMP-ANIA.
      *
           IF WPMO-TP-FRM-ASSVA(1) = 'IN'
      *
              MOVE WPMO-ID-POLI(1)
                TO WMOV-ID-OGG
              MOVE 'PO'
                TO WMOV-TP-OGG
      *
           ELSE
      *
              MOVE WPMO-ID-ADES(1)
                TO WMOV-ID-OGG
              MOVE 'AD'
                TO WMOV-TP-OGG
      *
           END-IF.
      *
           MOVE IDSV0001-TIPO-MOVIMENTO
             TO WMOV-TP-MOVI.
      *
           MOVE IDSV0001-DATA-EFFETTO
             TO WMOV-DT-EFF.
      *
           IF ISPV0000-IN-CC-ASSICURATIVO
              CONTINUE
           ELSE
              MOVE IABV0006-ID-RICH
                TO WMOV-ID-RICH
           END-IF.
      *
       S00350-CREA-MOVI-FITTIZIO-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * INIZIALIZZAZIONE AREA COMUNE                                   *
      *----------------------------------------------------------------*
      *
ALPO   S00090-INIZ-AREA-COMUNE.
      *
ALPO  * --> Valorizzazione Id-Adesione e Id-Polizza dell'area
ALPO  * --> passaggio al Main
ALPO  *
ALPO       MOVE WPMO-ID-POLI(1)
ALPO         TO WCOM-ID-POLIZZA.
ALPO  *
ALPO       MOVE WPMO-ID-ADES(1)
ALPO         TO WCOM-ID-ADES.
ALPO  *
ALPO       MOVE WPMO-TP-FRM-ASSVA(1)
ALPO         TO WCOM-TP-FRM-ASSVA.
ALPO  *
ALPO       MOVE 1
ALPO         TO WCOM-STEP-ELAB.
ALPO  *
ALPO   S00090-INIZ-AREA-COMUNE-EX.
ALPO       EXIT.
      *
      *----------------------------------------------------------------*
      *                        DISPLAY INIZIALI                        *
      *----------------------------------------------------------------*
      *
       S00100-DISPLAY-INIZIO.
      *
      * --> SEGNALAZIONE DI INIZIO BATCH
      *
           ACCEPT WK-DATA-AMG
             FROM DATE YYYYMMDD.
           ACCEPT WK-ORA-HMS
             FROM TIME.
           MOVE CORR WK-DATA-AMG
             TO WK-DATA-SISTEMA.
           MOVE CORR WK-ORA-HMS
             TO WK-ORA-SISTEMA.
      *
       S00100-DISPLAY-INIZIO-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *               APERTURA DEL FILE DI OUTPUT OUTRIVA              *
      *----------------------------------------------------------------*
      *
       S00800-OPEN-OUT.
      *
           OPEN OUTPUT OUTRIVA.
      *
           IF FS-OUT NOT = '00'
      *
              MOVE 'S00800-OPEN-OUT'
                TO WK-LABEL-ERR
              MOVE 'ERRORE APERTURA FILE OUTPUT OUTRIVA'
                TO WK-STRING
              MOVE '001114'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S00800-OPEN-OUT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     INIZIALIZZAZIONE DELLE AREE DI WORKING STORAGE             *
      *----------------------------------------------------------------*
      *
       S00900-VALOR-IB-OGG-MOV.
      *
           IF WPMO-TP-FRM-ASSVA(1) = 'IN'
      *
              IF WPOL-ELE-POLI-MAX > 0
      *
                 MOVE WPOL-IB-OGG
                   TO WMOV-IB-OGG
      *
              END-IF
      *
           ELSE
      *
              IF WADE-ELE-ADES-MAX > 0
      *
                 MOVE WADE-IB-OGG(1)
                   TO WMOV-IB-OGG
      *
              END-IF
      *
           END-IF.
      *
       S00900-VALOR-IB-OGG-MOV-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     INIZIALIZZAZIONE DELLE AREE DI WORKING STORAGE             *
      *----------------------------------------------------------------*
      *
       S00200-INIZIA-AREE-WS.
      *
      *  Inizializazione di tutti gli indici e le aree di WS
      *
ALEX  *     INITIALIZE WK-VARIABILI.
      *
           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LOAS0310'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Iniz. aree di working'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           INITIALIZE WORK-COMMAREA
                      IX-INDICI.
      *                AREA-IO-ISPS0040.
      *
      *    INIZIALIZZAZIONE AREA WK-VARIABILI
      *
           INITIALIZE WK-ELE-LIVELLO-MAX
                      WK-COD-COMP-ANIA(1)
                      WK-TP-LIVELLO   (1)
                      WK-COD-LIVELLO  (1)
                      WK-ID-LIVELLO   (1)
                      WK-ELE-VARIABILI-MAX(1)
                      WK-NOME-SERVIZIO(1).

ALEX       PERFORM VARYING IX-WK-VAR FROM 1 BY 1
             UNTIL IX-WK-VAR > WK-MAX-VAR
             INITIALIZE   WK-TAB-VARIABILI(1,IX-WK-VAR)
           END-PERFORM.

           MOVE WK-TAB-VAR       TO WK-RESTO-TAB-VAR.

      *
      *   Arrotolamento copy Servizio Dati Prodotto
      *
           INITIALIZE            ISPC0040-DATI-INPUT
                                 ISPC0040-DATI-OUTPUT1
                                 ISPC0040-DATI-OUTPUT2
                                 ISPC0040-AREA-ERRORI
                                 ISPC0040-COD-FONDI(1)
                                 ISPC0040-DESC-FOND(1).

           MOVE ISPC0040-TAB-FONDI  TO ISPC0040-RESTO-TAB-FONDI.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LOAS0310'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Iniz. aree di working'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           SET  IDSV0001-ESITO-OK
             TO TRUE.
           SET  WCOM-WRITE-OK
             TO TRUE.

ALPO       MOVE 99991231
ALPO         TO WK-DT-EFF-BLC
ALPO       MOVE 00000000
ALPO         TO WK-APPO-OGB-ID-OGG-BLOCCO
ALPO       SET WK-BLC-TROVATO-NO
ALPO         TO TRUE
      *
           MOVE WPMO-AREA-PARAM-MOVI
             TO VPMO-AREA-PARAM-MOVI.
      *
           MOVE WPMO-ELE-PARAM-MOV-MAX
             TO VPMO-ELE-PARAM-MOV-MAX.
      *
       S00200-INIZIA-AREE-WS-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     INIZIALIZZAZIONE DELLE TABELLE CHE VERRANNO SCRITTE        *
      *----------------------------------------------------------------*
      *
       S00300-INIZ-AREE-TABELLE.
      *
           MOVE ZEROES TO WMOV-ELE-MOVI-MAX
                          WPOL-ELE-POLI-MAX
                          WADE-ELE-ADES-MAX
                          WGRZ-ELE-GAR-MAX
                          WTGA-ELE-TRAN-MAX
                          VGRZ-ELE-GAR-MAX
                          VTGA-ELE-TRAN-MAX.
      *
      * --> Inizializzazione Area Movimento
      *
           PERFORM INIZIA-TOT-MOV
              THRU INIZIA-TOT-MOV-EX.
      *
      * --> Inizializzazione Area Polizza
      *
           PERFORM INIZIA-TOT-POL
              THRU INIZIA-TOT-POL-EX.
      *
      * --> Inizializzazione Area Adesione
      *
           PERFORM INIZIA-TOT-ADE
              THRU INIZIA-TOT-ADE-EX
           VARYING IX-TAB-ADE FROM 1 BY 1
             UNTIL IX-TAB-ADE > WK-MAX-ADE

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LOAS0310'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Iniz. tab. working TGA'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *    INITIALIZE             WGRZ-AREA-GARANZIA

21901      PERFORM INIZIA-TOT-GRZ
  ..          THRU INIZIA-TOT-GRZ-EX
  ..       VARYING IX-TAB-GRZ FROM 1 BY 1
  ..         UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX.

           MOVE WGRZ-AREA-GARANZIA TO VGRZ-AREA-GARANZIA.

ALEX  *--  INIZIALIZZAZIONE AREA TRANCHE DI GARANZIA

           INITIALIZE             WTGA-TAB-TRAN(1).

12888      INITIALIZE             WPREC-AREA-TRANCHE.

           MOVE  WTGA-TAB         TO WTGA-RESTO-TAB.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LOAS0310'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Iniz. tab. working GRZ'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

ALEX  *--  INIZIALIZZAZIONE AREA TRANCHE DI GARANZIA VALORIZZATORE

           INITIALIZE             VTGA-TAB-TRAN(1).

           MOVE  VTGA-TAB         TO VTGA-RESTO-TAB.

      *
           INITIALIZE IX-INDICI.
      *
           MOVE ZEROES
             TO IX-X-RIV-INC.
      *
       S00300-INIZ-AREE-TABELLE-EX.
           EXIT.
      *
      * ============================================================== *
      * -->                     C O N T R O L L I                  <-- *
      * ============================================================== *
      *
       S00400-CONTROLLI.
      *
           MOVE 'S00400-CONTROLLI'
             TO WK-LABEL-ERR.
      *
DBG        PERFORM DISPLAY-LABEL
DBG           THRU DISPLAY-LABEL-EX.
      *
ID67       IF IDSV0001-ESITO-OK
      *
      *---> Verifichiamo che il movimento non sia stato gi eseguito
              PERFORM S00390-CHIAMA-LDBS2200
                 THRU S00390-CHIAMA-LDBS2200-EX

              IF IDSV0001-ESITO-OK
      * --> Se wcom-write-niente, forzatura su idsv0001-esito-ko
      * --> per skippare tutta l'elaborazione
                 IF MOVIM-NOT-FOUND
                    CONTINUE
                 ELSE
                    SET WCOM-WRITE-NIENTE
                     TO TRUE
      *             SET IDSV0001-ESITO-KO
      *              TO TRUE
                    MOVE WK-PGM
                      TO IEAI9901-COD-SERVIZIO-BE
                    MOVE WK-LABEL-ERR
                      TO IEAI9901-LABEL-ERR
                    MOVE '005247'
                      TO IEAI9901-COD-ERRORE
                    MOVE WPMO-ID-POLI(1)
                      TO WK-ID-POLI-DISPLAY
                    STRING 'POLIZZA ' WK-ID-POLI-DISPLAY
                           ' NON ELABORATA PER: '
                           'MOVIMENTO GIA'' ESEGUITO'
                    DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
                       THRU EX-S0300
                 END-IF
              END-IF
      *
           END-IF
ID67  *
           IF  IDSV0001-ESITO-OK
           AND WCOM-WRITE-OK
      *
              PERFORM S00440-LEGGI-POLIZZA
                 THRU S00440-LEGGI-POLIZZA-EX
      *
           END-IF.

           IF IDSV0001-ESITO-OK
           AND WCOM-WRITE-OK
      *
              PERFORM S00410-CTRL-STATO-E-CAUS
                 THRU S00410-CTRL-STATO-E-CAUS-EX
      *
           END-IF.
      *

           IF IDSV0001-ESITO-OK
           AND WCOM-WRITE-OK
      *
              PERFORM S00460-LEGGI-ADESIONE
                 THRU S00460-LEGGI-ADESIONE-EX
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
           AND WCOM-WRITE-OK
      *
              MOVE WPOL-IB-OGG
                TO IABV0006-IB-OGG-POLI
              MOVE WADE-IB-OGG(1)
                TO IABV0006-IB-OGG-ADES
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
           AND WCOM-WRITE-OK
      *
              PERFORM S00570-CTRL-TP-DT-RIVAL
                 THRU S00570-CTRL-TP-DT-RIVAL-EX
      *
           END-IF.
      *
       S00400-CONTROLLI-EX.
           EXIT.
      *
ID67  *----------------------------------------------------------------*
      *    ESTRAIAMO DALL'ENTITA' MOVI
      *----------------------------------------------------------------*
       S00390-CHIAMA-LDBS2200.
      *
           MOVE 'S00390-CHIAMA-LDBS2200'
             TO WK-LABEL-ERR
      *
DBG        PERFORM DISPLAY-LABEL
DBG           THRU DISPLAY-LABEL-EX.
      *
           INITIALIZE                       MOVI.

      *--> DATA EFFETTO
           MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

      *--> TRATTAMENTO STORICITA'
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE

      *--> MODALITA' DI ACCESSO
           SET IDSI0011-WHERE-CONDITION  TO TRUE.

      *--> Valorizziamo DCLGEN TABELLA
           MOVE WPMO-ID-ADES(1)          TO MOV-ID-OGG
           MOVE 'AD'                     TO MOV-TP-OGG
           MOVE '6006'                   TO MOV-TP-MOVI

      *--> NOME TABELLA FISICA DB
           MOVE 'LDBS2200'               TO IDSI0011-CODICE-STR-DATO.

      *--> DCLGEN TABELLA
           MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
           MOVE MOVI                     TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET  IDSI0011-FETCH-FIRST     TO TRUE.

      *--> INIZIALIZZAZIONE FLAGS
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI
                       TO MOVI

      *-->           Controlliamo DT_EFF
                     IF MOV-DT-EFF = IDSV0001-DATA-EFFETTO
                        SET MOVIM-FOUND     TO TRUE
                     ELSE
                        SET MOVIM-NOT-FOUND TO TRUE
                     END-IF

                     PERFORM S00395-CLOSE-CUR-LDBS2200
                        THRU S00395-CLOSE-CUR-LDBS2200-EX

                  WHEN IDSO0011-NOT-FOUND
      *-->        CHIAVE NON TROVATA
                     SET MOVIM-NOT-FOUND    TO TRUE

                  WHEN OTHER
      *-->        ERRORE DB
                     MOVE WK-PGM
                       TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S00390-CHIAMA-LDBS2200'
                       TO IEAI9901-LABEL-ERR
                     MOVE '005016'
                       TO IEAI9901-COD-ERRORE
                     STRING 'MOVI'             ';'
                          IDSO0011-RETURN-CODE ';'
                          IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300

              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S00390-CHIAMA-LDBS2200'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'MOVI'               ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S00390-CHIAMA-LDBS2200-EX.
           EXIT.
ID67  *
ID67  *----------------------------------------------------------------*
      *    CHIUSURA DEL CURSORE
      *----------------------------------------------------------------*
       S00395-CLOSE-CUR-LDBS2200.

           SET IDSI0011-CLOSE-CURSOR     TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           INITIALIZE                       MOVI.
           MOVE 'LDBS2200'               TO IDSI0011-CODICE-STR-DATO.
           MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                 WHEN IDSO0011-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    CONTINUE

                 WHEN OTHER
      *--->      ERRORE DI ACCESSO AL DB
                    MOVE WK-PGM
                      TO IEAI9901-COD-SERVIZIO-BE
                    MOVE 'S00395-CLOSE-CUR-LDBS2200'
                      TO IEAI9901-LABEL-ERR
                    MOVE '005016'
                      TO IEAI9901-COD-ERRORE
                    STRING 'MOVI'               ';'
                           IDSO0011-RETURN-CODE ';'
                           IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO
                           IEAI9901-PARAMETRI-ERR
                    END-STRING
                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
                       THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S00395-CLOSE-CUR-LDBS2200'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'MOVI'               ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO
                     IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S00395-CLOSE-CUR-LDBS2200-EX.
           EXIT.
ID67  *
      *----------------------------------------------------------------*
      *        CONTROLLA LO STATO OGGETTO BUSINESS DELLA POLIZZA       *
      *----------------------------------------------------------------*
      *
       S00410-CTRL-STATO-E-CAUS.
      *
           MOVE 'S00410-CTRL-STATO-E-CAUS'
             TO WK-LABEL-ERR
      *
DBG        PERFORM DISPLAY-LABEL
DBG           THRU DISPLAY-LABEL-EX.
      *
      *  Controllo Stato Contratto
      *
           MOVE WPMO-ID-ADES(1)
             TO WK-STB-ID-OGG.
           MOVE 'AD'
             TO WK-STB-TP-OGG.
      *
           PERFORM S00420-IMPOSTA-STB
              THRU S00420-IMPOSTA-STB-EX.
      *
           PERFORM S00430-LEGGI-STB
              THRU S00430-LEGGI-STB-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              MOVE STB-TP-STAT-BUS
                TO WS-TP-STAT-BUS
              MOVE STB-TP-CAUS
                TO WS-TP-CAUS
      *
      * --> Se wcom-write-niente, forzatura su idsv0001-esito-ko
      * --> per skippare tutta l'elaborazione
      *
A3365         IF IN-VIGORE
      *
                 CONTINUE
      *
A3365         ELSE
      *
A3365            SET WCOM-WRITE-NIENTE
A3365             TO TRUE
A3365 *          SET IDSV0001-ESITO-KO
A3365 *           TO TRUE
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR
                   TO IEAI9901-LABEL-ERR
                 MOVE '005247'
                   TO IEAI9901-COD-ERRORE
                 MOVE WPOL-IB-OGG(1:11)
                   TO WK-IB-POLI-DISPLAY
                 STRING 'POLIZZA ' WK-IB-POLI-DISPLAY
                        ' NON ELABORATA PER: '
                        'ADESIONE NON IN VIGORE'
                 DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
      *
A3365         END-IF
      *
           END-IF.
      *
       S00410-CTRL-STATO-E-CAUS-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  VALORIZZAZIONI PER LETTURA STATO OGGETTO BUSINESS
      *----------------------------------------------------------------*
      *
       S00420-IMPOSTA-STB.
      *
           INITIALIZE STAT-OGG-BUS.
      *
           MOVE WK-STB-ID-OGG
             TO STB-ID-OGG.
           MOVE WK-STB-TP-OGG
             TO STB-TP-OGG.
      *
           SET WK-STB-TROV-NO
             TO TRUE.
      *
           MOVE ZEROES
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
                IDSI0011-DATA-COMPETENZA.
      *
            SET IDSI0011-TRATT-DEFAULT
             TO TRUE.
      *
           MOVE 'STAT-OGG-BUS'
             TO IDSI0011-CODICE-STR-DATO.
      *
           MOVE STAT-OGG-BUS
             TO IDSI0011-BUFFER-DATI.
      *
            SET IDSI0011-SELECT
             TO TRUE.
            SET IDSI0011-ID-OGGETTO
             TO TRUE.
      *
            SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
            SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S00420-IMPOSTA-STB-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  LETTURA DELLA TABELLA STATO OGGETTO BUSINESS
      *----------------------------------------------------------------*
      *
       S00430-LEGGI-STB.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata
      *
                       MOVE 'S00430-LEGGI-STB'
                         TO WK-LABEL-ERR
                       MOVE 'STAT-OGG-BUS NON TROVATA'
                         TO WK-STRING
                       MOVE '005069'
                         TO WK-COD-ERR
                       PERFORM GESTIONE-ERR-STD
                          THRU GESTIONE-ERR-STD-EX
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
                       MOVE IDSO0011-BUFFER-DATI
                         TO STAT-OGG-BUS
      *
                       SET WK-STB-TROV-SI
                         TO TRUE
      *
                  WHEN OTHER
      *
      *  --> Chiave non trovata
      *
                       MOVE 'S00430-LEGGI-STB'
                         TO WK-LABEL-ERR
                       MOVE 'ERRORE LETTURA STAT-OGG-BUS'
                         TO WK-STRING
                       MOVE '001114'
                         TO WK-COD-ERR
                       PERFORM GESTIONE-ERR-STD
                          THRU GESTIONE-ERR-STD-EX
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore Dispatcher
      *
              MOVE 'S00430-LEGGI-STB'
                TO WK-LABEL-ERR
              MOVE 'ERRORE DISPATCHER LETTURA STAT-OGG-BUS'
                TO WK-STRING
              MOVE '001114'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S00430-LEGGI-STB-EX.
           EXIT.
      *
      * -------------------------------------------------------------- *
      *    LETTURA DELLA TABELLA POLIZZA                               *
      * -------------------------------------------------------------- *
      *
       S00440-LEGGI-POLIZZA.
      *
           MOVE 'S00440-LEGGI-POLIZZA'
             TO WK-LABEL-ERR
      *
DBG        PERFORM DISPLAY-LABEL
DBG           THRU DISPLAY-LABEL-EX.
      *
           PERFORM S00450-IMPOSTA-POLIZZA
              THRU S00450-IMPOSTA-POLIZZA-EX
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata
      *
                       MOVE 'S00440-LEGGI-POLIZZA'
                         TO WK-LABEL-ERR
                       MOVE 'POLIZZA NON TROVATA'
                         TO WK-STRING
                       MOVE '005069'
                         TO WK-COD-ERR
                       PERFORM GESTIONE-ERR-STD
                          THRU GESTIONE-ERR-STD-EX
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
      *  --> Operazione eseguita correttamente
      *
                       MOVE 1
                         TO WPOL-ELE-POLI-MAX
                       MOVE IDSO0011-BUFFER-DATI
                         TO POLI
                       PERFORM VALORIZZA-OUTPUT-POL
                          THRU VALORIZZA-OUTPUT-POL-EX
                       SET WPOL-ST-INV
                         TO TRUE
                       MOVE WPOL-IB-OGG
                         TO WCOM-POL-IB-OGG
      *
                  WHEN OTHER
      *
      *  --> Errore di accesso al db
      *
                       MOVE 'S00440-LEGGI-POLIZZA'
                         TO WK-LABEL-ERR
                       MOVE 'ERRORE LETTURA POLIZZA'
                         TO WK-STRING
                       MOVE '005016'
                         TO WK-COD-ERR
                       PERFORM GESTIONE-ERR-STD
                          THRU GESTIONE-ERR-STD-EX
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore dispatcher
      *
              MOVE 'S00440-LEGGI-POLIZZA'
                TO WK-LABEL-ERR
              MOVE 'ERRORE DISPATCHER LETTURA POLIZZA'
                TO WK-STRING
              MOVE '005016'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S00440-LEGGI-POLIZZA-EX.
           EXIT.
      *
      * -------------------------------------------------------------- *
      *    VALORIZZAZIONE PER LETTURA DELLA TABELLA POLIZZA            *
      * -------------------------------------------------------------- *
      *
       S00450-IMPOSTA-POLIZZA.
      *
           INITIALIZE POLI.
      *
           MOVE WPMO-ID-POLI(1)
             TO POL-ID-POLI.
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE ZERO
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
                IDSI0011-DATA-COMPETENZA.
      *
      *  --> Nome tabella fisica db
      *
           MOVE 'POLI'
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE POLI
             TO IDSI0011-BUFFER-DATI.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-SELECT
             TO TRUE.
      *
      *  --> Tipo livello
      *
           SET IDSI0011-ID
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSI0011-TRATT-DEFAULT
             TO TRUE.
      *
      *  --> Inizializzazione Return Code
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S00450-IMPOSTA-POLIZZA-EX.
           EXIT.
      *
      * -------------------------------------------------------------- *
      *    LETTURA DELLA TABELLA ADESIONE                              *
      * -------------------------------------------------------------- *
      *
       S00460-LEGGI-ADESIONE.
DBG        MOVE 'S00460-LEGGI-ADESIONE' TO WK-LABEL-ERR
DBG        PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX

      *
           PERFORM S00470-IMPOSTA-ADESIONE
              THRU S00470-IMPOSTA-ADESIONE-EX
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata
      *
                       MOVE 'S00460-LEGGI-ADESIONE'
                         TO WK-LABEL-ERR
                       MOVE 'ADESIONE NON TROVATA'
                         TO WK-STRING
                       MOVE '005069'
                         TO WK-COD-ERR
                       PERFORM GESTIONE-ERR-STD
                          THRU GESTIONE-ERR-STD-EX
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
      *  --> Operazione eseguita correttamente
      *
                       MOVE 1
                         TO IX-TAB-ADE
                            WADE-ELE-ADES-MAX
                       MOVE IDSO0011-BUFFER-DATI
                         TO ADES
                       PERFORM VALORIZZA-OUTPUT-ADE
                          THRU VALORIZZA-OUTPUT-ADE-EX
                       SET WADE-ST-INV(1)
                         TO TRUE
                       MOVE WADE-IB-OGG(1)
                         TO WCOM-ADE-IB-OGG
      *
                  WHEN OTHER
      *
      *  --> Errore di accesso al db
      *
                       MOVE 'S00460-LEGGI-ADESIONE'
                         TO WK-LABEL-ERR
                       MOVE 'ERRORE LETTURA ADESIONE'
                         TO WK-STRING
                       MOVE '005016'
                         TO WK-COD-ERR
                       PERFORM GESTIONE-ERR-STD
                          THRU GESTIONE-ERR-STD-EX
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore dispatcher
      *
              MOVE 'S00460-LEGGI-ADESIONE'
                TO WK-LABEL-ERR
              MOVE 'ERRORE DISPATCHER LETTURA ADESIONE'
                TO WK-STRING
              MOVE '005016'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S00460-LEGGI-ADESIONE-EX.
           EXIT.
      *
      * -------------------------------------------------------------- *
      *    VALORIZZAZIONE PER LETTURA DELLA TABELLA ADESIONE
      * -------------------------------------------------------------- *
      *
       S00470-IMPOSTA-ADESIONE.
      *
           INITIALIZE ADES.
      *
           MOVE WPMO-ID-ADES(1)
             TO ADE-ID-ADES.
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE ZERO
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
                IDSI0011-DATA-COMPETENZA.
      *
      *  --> Nome tabella fisica db
      *
           MOVE 'ADES'
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE ADES
             TO IDSI0011-BUFFER-DATI.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-SELECT
             TO TRUE.
      *
      *  --> Tipo livello
      *
           SET IDSI0011-ID
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSI0011-TRATT-DEFAULT
             TO TRUE.
      *
      *  --> Inizializzazione Return Code
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S00470-IMPOSTA-ADESIONE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *        CONTROLLA IL TIPO DI DATA RIVALUTAZIONE                 *
      *----------------------------------------------------------------*
      *
       S00570-CTRL-TP-DT-RIVAL.
      *
DBG        MOVE 'S00570-CTRL-TP-DT-RIVAL' TO WK-LABEL-ERR
DBG        PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX

      * --> Verifica che tutte le occorrenze lette dalla tabella
      * --> guida Parametro Movimento abbiano lo stesso tipo
      * --> oggetto di rivalutazione, se non h cosl viene genarato
      * --> un errore bloccante
      *
           PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
             UNTIL IX-TAB-PMO > WPMO-ELE-PARAM-MOV-MAX
      *
                IF WPMO-TP-OGG-RIVAL(1) NOT =
                   WPMO-TP-OGG-RIVAL(IX-TAB-PMO)
      *
                   MOVE 'S00570-CTRL-TP-DT-RIVAL'
                     TO WK-LABEL-ERR
                   MOVE 'TP-OGG-RIVAL DIVERSO NELLA STESSA ADESIONE'
                     TO WK-STRING
                   MOVE '001114'
                     TO WK-COD-ERR
                   PERFORM GESTIONE-ERR-STD
                      THRU GESTIONE-ERR-STD-EX
      *
                END-IF
      *
           END-PERFORM.
      *
       S00570-CTRL-TP-DT-RIVAL-EX.
           EXIT.
      *
      * -------------------------------------------------------------- *
      *       DETERMINAZIONE DELLA DATA INIZIO VALIDITA' PRODOTTO      *
      * -------------------------------------------------------------- *
      *
       S00600-GESTIONE-DATA-PROD.
DBG        MOVE 'S00600-GESTIONE-DATA-PROD' TO WK-LABEL-ERR
DBG        PERFORM DISPLAY-LABEL            THRU DISPLAY-LABEL-EX

      *
      *  --> Chiamata al servizio di prodotto ISPS0040 per il
      *  --> reperimento della data di validit prodotto
      *
           IF WCOM-DT-ULT-VERS-PROD = 0
      *
              IF WPOL-FL-VER-PROD = 'E'
      *
                 MOVE WPOL-DT-INI-VLDT-PROD
                   TO WCOM-DT-ULT-VERS-PROD
      *
              ELSE
PERF  *
PERF  *          PERFORM S00610-PREPARA-ISPS0040
PERF  *             THRU S00610-PREPARA-ISPS0040-EX
PERF  *
PERF  *          PERFORM S00620-CALL-ISPS0040
PERF  *             THRU S00620-CALL-ISPS0040-EX
PERF  *
PERF             SET PROD-KO TO TRUE

PERF             PERFORM S00610-PREPARA-ISPS0040
PERF                THRU S00610-PREPARA-ISPS0040-EX

PERF             PERFORM VERIFICA-PROD
PERF                THRU VERIFICA-PROD-EX


PERF             IF PROD-KO
PERF                PERFORM S00620-CALL-ISPS0040
PERF                   THRU S00620-CALL-ISPS0040-EX

PERF                IF IDSV0001-ESITO-OK
PERF                AND PROD-KO
PERF                AND WK-0040-ELE-MAX <  WK-0040-ELEMENTI-MAX
PERF                      ADD 1 TO WK-0040-ELE-MAX
PERF                      MOVE WS-0040-COD-COMPAGNIA
PERF                        TO WK-0040-COD-COMPAGNIA(WK-0040-ELE-MAX)
PERF                      MOVE WS-0040-COD-PRODOTTO
PERF                        TO WK-0040-COD-PRODOTTO(WK-0040-ELE-MAX)
PERF                      MOVE WS-0040-COD-CONVENZIONE
PERF                        TO WK-0040-COD-CONVENZIONE(WK-0040-ELE-MAX)
PERF                      MOVE WS-0040-DATA-INIZ-VALID-CONV
PERF                    TO WK-0040-DATA-INIZ-VALID-CONV(WK-0040-ELE-MAX)
PERF                      MOVE WS-0040-DATA-RIFERIMENTO
PERF                      TO WK-0040-DATA-RIFERIMENTO(WK-0040-ELE-MAX)
PERF                      MOVE WS-0040-LIVELLO-UTENTE
PERF                       TO WK-0040-LIVELLO-UTENTE(WK-0040-ELE-MAX)
PERF                      MOVE WS-0040-SESSION-ID
PERF                        TO WK-0040-SESSION-ID(WK-0040-ELE-MAX)
PERF                      MOVE WS-0040-FUNZIONALITA
PERF                        TO WK-0040-FUNZIONALITA(WK-0040-ELE-MAX)
PERF                      MOVE WCOM-DT-ULT-VERS-PROD
PERF                      TO WK-0040-DATA-VERSIONE-PROD(WK-0040-ELE-MAX)
PERF                   END-IF
PERF                END-IF
PERF          END-IF
      *
           END-IF.
      *
       S00600-GESTIONE-DATA-PROD-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     PREPAREA AREA SERVIZIO DATI PRODOTTO ISPS0040              *
      *----------------------------------------------------------------*
      *
       S00610-PREPARA-ISPS0040.
      *

      *
      *  --> Valorizzazione dell'Area di I/O del servizio chiamato
      *
ESPE       INITIALIZE AREA-IO-ISPS0040.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO ISPC0040-COD-COMPAGNIA.
      *
           MOVE WPOL-COD-PROD
             TO ISPC0040-COD-PRODOTTO.
      *
           IF WPOL-COD-CONV-NULL = HIGH-VALUES
      *
              MOVE SPACES
                TO ISPC0040-COD-CONVENZIONE
      *
           ELSE
      *
              MOVE WPOL-COD-CONV
                TO ISPC0040-COD-CONVENZIONE
      *
           END-IF.
      *
           IF WPOL-DT-INI-VLDT-CONV-NULL = HIGH-VALUE
      *
              MOVE SPACES
                TO ISPC0040-DATA-INIZ-VALID-CONV
      *
           ELSE
      *
              MOVE WPOL-DT-INI-VLDT-CONV
                TO ISPC0040-DATA-INIZ-VALID-CONV
      *
           END-IF.
      *
A3365 *     MOVE IDSV0001-DATA-EFFETTO
            MOVE WPOL-DT-INI-VLDT-PROD
             TO ISPC0040-DATA-RIFERIMENTO.
      *
           MOVE WCOM-COD-LIV-AUT-PROFIL
             TO ISPC0040-LIVELLO-UTENTE.
      *
           MOVE IDSV0001-SESSIONE
             TO ISPC0040-SESSION-ID.

      *
           MOVE '1001'
             TO ISPC0040-FUNZIONALITA.
      *
       S00610-PREPARA-ISPS0040-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     CHIAMATA A SERVIZIO DATI PRODOTTO ISPS0040                 *
      *----------------------------------------------------------------*
      *
       S00620-CALL-ISPS0040.
      *

      *
           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'ISPS0040'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Servizio dati prodotto'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           CALL ISPS0040                 USING AREA-IDSV0001
                                               WCOM-AREA-STATI
                                               AREA-IO-ISPS0040
           ON EXCEPTION
      *
                 MOVE 'ISPS0040'
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'SERVIZIO DATI PRODOTTO'
                   TO CALL-DESC
                 MOVE 'CALL-DATI-PROD'
                   TO WK-LABEL-ERR
      *
                 PERFORM GESTIONE-ERR-SIST
                    THRU GESTIONE-ERR-SIST-EX
      *
           END-CALL.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'ISPS0040'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Servizio dati prodotto'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

ESPE       IF IDSV0001-ESITO-OK
ESPE          MOVE ISPC0040-DATA-VERSIONE-PROD
ESPE            TO WCOM-DT-ULT-VERS-PROD
ESPE       END-IF.
      *

      *
       S00620-CALL-ISPS0040-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *   DETERMINAZIONE DEL TIPO DI RIVALUTAZIONE EMESSO/INCASSO      *
      *----------------------------------------------------------------*
      *
       S00700-DETERMINA-ORIG-RIV.
DBG        MOVE 'S00700-DETERMINA-ORIG-RIV' TO WK-LABEL-ERR
DBG        PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
      *
      * --> Accesso alla tabella Parametro Compagnia per
      * --> determinare se la rivalutazione h da effettuarsi
      * --> "Per Incasso" o "Per Emesso"
      *
           PERFORM S00710-VERIFICA-INC-EMESSO
              THRU S00710-VERIFICA-INC-EMESSO-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              IF PCO-TP-MOD-RIVAL = 'IN'
      *
                 SET WK-ORIG-RIV-IN
                   TO TRUE
      *
              ELSE
      *
                 SET WK-ORIG-RIV-EM
                   TO TRUE
      *
              END-IF
      *
           END-IF.
      *
       S00700-DETERMINA-ORIG-RIV-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *              ACCESSO ALLA TABELLA PARAMETRO COMPAGNIA          *
      *----------------------------------------------------------------*
      *
       S00710-VERIFICA-INC-EMESSO.
DBG        MOVE 'S00720-VERIFICA-INC-EMESSO' TO WK-LABEL-ERR
DBG        PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX

      *
           PERFORM S00720-IMPOSTA-PARAM-COMP
              THRU S00720-IMPOSTA-PARAM-COMP-EX.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata
      *
                       MOVE 'S00710-VERIFICA-INC-EMESSO'
                         TO WK-LABEL-ERR
                       MOVE 'COMPAGNIA NON TROVATA'
                         TO WK-STRING
                       MOVE '005069'
                         TO WK-COD-ERR
                       PERFORM GESTIONE-ERR-STD
                          THRU GESTIONE-ERR-STD-EX
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
      *  --> Operazione eseguita correttamente
      *
                       MOVE IDSO0011-BUFFER-DATI
                         TO PARAM-COMP
      *
                  WHEN OTHER
      *
      *  --> Errore di accesso al db
      *
                       MOVE 'S00710-VERIFICA-INC-EMESSO'
                         TO WK-LABEL-ERR
                       MOVE 'ERRORE LETTURA PARAMETRO COMPAGNIA'
                         TO WK-STRING
                       MOVE '005016'
                         TO WK-COD-ERR
                       PERFORM GESTIONE-ERR-STD
                          THRU GESTIONE-ERR-STD-EX
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore dispatcher
      *
              MOVE 'S00710-VERIFICA-INC-EMESSO'
                TO WK-LABEL-ERR
              MOVE 'ERRORE DISPATCHER LETTURA PARAMETRO COMPAGNIA'
                TO WK-STRING
              MOVE '005016'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S00710-VERIFICA-INC-EMESSO-EX.
           EXIT.
      *
      * -------------------------------------------------------------- *
      * VALORIZZAZIONE DELLE CHIAVI DELLA TABELLA PARAMETRO COMPAGNIA  *
      * -------------------------------------------------------------- *
      *
       S00720-IMPOSTA-PARAM-COMP.
      *
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO PCO-COD-COMP-ANIA.
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE ZERO
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
                IDSI0011-DATA-COMPETENZA.
      *
      *  --> Nome tabella fisica db
      *
           MOVE 'PARAM-COMP'
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE PARAM-COMP
             TO IDSI0011-BUFFER-DATI.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-SELECT
             TO TRUE.
      *
      *  --> Tipo livello
      *
           SET IDSI0011-PRIMARY-KEY
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSI0011-TRATT-SENZA-STOR
             TO TRUE.
      *
      *  --> Inizializzazione Return Code
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S00720-IMPOSTA-PARAM-COMP-EX.
           EXIT.
      *
      * ============================================================== *
      * -->               E L A B O R A Z I O N E                  <-- *
      * ============================================================== *
      *
       S10000-ELABORAZIONE.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10100-ACQUISIZ-GAR-TRANCHE
                 THRU S10100-ACQUISIZ-GAR-TRANCHE-EX
      *
           END-IF
      *
           IF WCOM-WRITE-OK
           PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
             UNTIL IX-TAB-PMO > WPMO-ELE-PARAM-MOV-MAX
               IF WPMO-ST-ADD(IX-TAB-PMO)
                  MOVE VPMO-ELE-PARAM-MOV-MAX
                    TO IX-VPMO
                  ADD 1
                    TO IX-VPMO
                  MOVE IX-VPMO
                    TO VPMO-ELE-PARAM-MOV-MAX
                  MOVE WPMO-TAB-PARAM-MOV(IX-TAB-PMO)
                    TO VPMO-TAB-PARAM-MOV(IX-VPMO)
               END-IF
           END-PERFORM
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10450-GEST-RIC-SUCC-PMO
                 THRU S10450-GEST-RIC-SUCC-PMO-EX
      *
           END-IF
      *
           IF IDSV0001-ESITO-OK
      *
              IF WK-ORIG-RIV-IN
      *
                 IF COLLETTIVA
      *
                    PERFORM S10200-RIVAL-X-INCASSO-COL
                       THRU S10200-RIVAL-X-INCASSO-COL-EX
      *
                 ELSE
      *
                    PERFORM S10300-RIVAL-X-INCASSO-IND
                       THRU S10300-RIVAL-X-INCASSO-IND-EX
      *
              END-IF
      *
           END-IF
      *
      * Controllo Blocco - Movimento Futuro - Presenza Deroga
      *
           IF IDSV0001-ESITO-OK
      *
              MOVE WPOL-TP-FRM-ASSVA
                TO WS-TP-FRM-ASSVA
              MOVE WPOL-ID-POLI
                TO LOAP1-ID-POLI
              MOVE WADE-ID-ADES(1)
                TO LOAP1-ID-ADES
      *
              PERFORM LOAP0001-CONTROLLI
                 THRU LOAP0001-CONTROLLI-EX
      *
42781         IF  IDSV0001-ESITO-KO
42781         AND LOAC0280-DEROGA-BLOCCANTE-SI
42781             MOVE MOVI        TO ZMOV-DATI
42781             PERFORM LOAP0002-CONTROLLI
42781                THRU LOAP0002-CONTROLLI-EX
42781             MOVE ZMOV-DATI   TO MOVI
42781         END-IF
           END-IF
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10500-GEST-VALORIZZATORE
                 THRU S10500-GEST-VALORIZZATORE-EX
      *
           END-IF
      ******************************************************************
      ******************************************************************
      *  INIZIO CALL MODULO LOAS0660
      ******************************************************************
           IF IDSV0001-ESITO-OK
12888         PERFORM S10599-SALVA-PRSTZ-PREC
12888            THRU S10599-SALVA-PRSTZ-PREC-EX
      *
              PERFORM S10600-PREP-CALL-LOAS0660
                 THRU S10600-PREP-CALL-LOAS0660-EX
      *
           END-IF
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10700-CALL-LOAS0660
                 THRU S10700-CALL-LOAS0660
      *
           END-IF
      ******************************************************************
      *  FINE CALL MODULO LOAS0660
      ******************************************************************
      ******************************************************************


21315      IF IDSV0001-ESITO-OK
              IF WPMO-DT-RICOR-PREC-NULL(1) = HIGH-VALUES
                 MOVE 0                       TO WK-DT-RICOR-PREC
              ELSE
                 MOVE WPMO-DT-RICOR-PREC(1)   TO WK-DT-RICOR-PREC
              END-IF

              IF WPMO-DT-RICOR-SUCC-NULL(1) = HIGH-VALUES
                 MOVE 0                       TO WK-DT-RICOR-SUCC
              ELSE
                 MOVE WPMO-DT-RICOR-SUCC(1)   TO WK-DT-RICOR-SUCC
              END-IF

           END-IF


           IF IDSV0001-ESITO-OK         AND
              IABV0006-SIMULAZIONE-NO
      *
              PERFORM S10900-GESTIONE-EOC
                 THRU S10900-GESTIONE-EOC-EX
      *
           END-IF
      *
           IF IDSV0001-ESITO-OK

              PERFORM S10950-GESTIONE-FILEOUT
                 THRU S10950-GESTIONE-FILEOUT-EX

           END-IF
      *
           IF IDSV0001-ESITO-OK
      *
              IF W660-MF-CALCOLATO-SI
      *
                 PERFORM S11000-GESTIONE-FILE-MF
                    THRU S11000-GESTIONE-FILE-MF-EX
      *
              END-IF
      *
           END-IF
           END-IF.
      *
      *-->  GESTIONE CHIAMATA AL TRANSITORIO PER GESTIONE RENDITE
29401 *    IF IDSV0001-ESITO-OK
29401 *
29401 *       PERFORM S12000-GEST-TRANSITORIO
29401 *          THRU S12000-GEST-TRANSITORIO-EX
29401 *
29401 *    END-IF.
      *
       S10000-ELABORAZIONE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *      ACQUSIZIONE GARANZIE E TRANCHE DI GARANZIA                *
      *----------------------------------------------------------------*
      *
       S10100-ACQUISIZ-GAR-TRANCHE.
      *
           MOVE 'S10100-ACQUISIZ-GAR-TRANCHE'
             TO WK-LABEL-ERR.
      *
DBG        PERFORM DISPLAY-LABEL
DBG           THRU DISPLAY-LABEL-EX.
      *
      *
      * Ci potrebbero essere piy righe di pmo per una stessa Garanzia.
      * Viene salvato l'id-gar per effettuare le letture sulle
      * tabelle GAR e TRCH_DI_GAR una sola volta per ogni Garanzia
           IF WPMO-ELE-PARAM-MOV-MAX > 1
              MOVE ZERO
                TO WK-APPO-ID-GAR

              PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
                UNTIL IX-TAB-PMO > WPMO-ELE-PARAM-MOV-MAX
                    OR IDSV0001-ESITO-KO

                    COMPUTE IX-TAB-PMO-2 = IX-TAB-PMO + 1

                    PERFORM UNTIL IX-TAB-PMO-2 > WPMO-ELE-PARAM-MOV-MAX
                        OR IDSV0001-ESITO-KO

                      IF WPMO-ID-OGG(IX-TAB-PMO) =
                         WPMO-ID-OGG(IX-TAB-PMO-2)
                         MOVE 'ERRORE - PRESENTI GARANZIE DUPLICATE'
                           TO WK-STRING
                         MOVE '001114'
                           TO WK-COD-ERR
                         PERFORM GESTIONE-ERR-STD
                            THRU GESTIONE-ERR-STD-EX
                      END-IF
                      COMPUTE IX-TAB-PMO-2 = IX-TAB-PMO-2 + 1
                    END-PERFORM
               END-PERFORM
           END-IF
      *
           PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
             UNTIL IX-TAB-PMO > WPMO-ELE-PARAM-MOV-MAX
                OR IDSV0001-ESITO-KO
      *
                   PERFORM S10110-LEGGI-GARANZIA
                      THRU S10110-LEGGI-GARANZIA-EX
      *
                   IF IDSV0001-ESITO-OK
      *
                      IF WK-SCARTA-GAR-NO
      *
                         PERFORM S10130-ACQUISIZ-TRANCHE
                            THRU S10130-ACQUISIZ-TRANCHE-EX
      *
                      END-IF
      *
                END-IF

           END-PERFORM.
      *
           IF IDSV0001-ESITO-OK
              PERFORM S10170-VERIFICA-GARANZIE
                 THRU S10170-VERIFICA-GARANZIE-EX
           END-IF.
      *
       S10100-ACQUISIZ-GAR-TRANCHE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *       VERIFICA LEGAME GARANZIE TRANCHE
      *----------------------------------------------------------------*
      *
       S10170-VERIFICA-GARANZIE.
DBG        MOVE 'S10170-VERIFICA-GARANZIE' TO WK-LABEL-ERR
DBG        PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
      *
      * --> Verifica che epr ogni Garanzia sia stata caricata
      * --> almeno una Tranche
      *
           IF WGRZ-ELE-GAR-MAX = 0
                 SET WCOM-WRITE-NIENTE TO TRUE
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR
                   TO IEAI9901-LABEL-ERR
                 MOVE '005247'
                   TO IEAI9901-COD-ERRORE
                 MOVE WPOL-IB-OGG(1:11)
                   TO WK-IB-POLI-DISPLAY
                 STRING 'POLIZZA ' WK-IB-POLI-DISPLAY
                        ' NON ELABORATA PER: '
                        'GARANZIE NON IN VIGORE'
                 DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
           END-IF

           PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
             UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
      *
               SET WK-TROV-NO
                 TO TRUE
      *
               PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
                 UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
      *
                   IF WGRZ-ID-GAR(IX-TAB-GRZ) =
                      WTGA-ID-GAR(IX-TAB-TGA)
      *
                      SET WK-TROV-SI
                        TO TRUE
      *
                   END-IF
      *
               END-PERFORM
      *
               IF WK-TROV-NO
      *
                  MOVE 'NESSUNA TRANCHE DA ELAB. PER LA GARANZIA'
                    TO WK-STRING
                  MOVE '001114'
                    TO WK-COD-ERR
                  PERFORM GESTIONE-ERR-STD
                     THRU GESTIONE-ERR-STD-EX
      *
               END-IF
      *
           END-PERFORM.
      *
       S10170-VERIFICA-GARANZIE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CARICA TRANCHE IN WORKING
      *----------------------------------------------------------------*
       S10180-CARICA-TRANCHE.
           MOVE 'S10180-CARICA-TRANCHE'  TO WK-LABEL-ERR

??         MOVE WTGA-ELE-TRAN-MAX        TO IX-TAB-TGA

           ADD 1                         TO IX-TAB-TGA

           IF IX-TAB-TGA > WK-TGA-MAX-C

             MOVE 'OVERFLOW CARICAMENTO TRANCHE DI GARANZIA'
                TO WK-STRING
             MOVE '005059'
                TO WK-COD-ERR
             PERFORM GESTIONE-ERR-STD
                THRU GESTIONE-ERR-STD-EX

           ELSE

             MOVE IX-TAB-TGA             TO WTGA-ELE-TRAN-MAX

             PERFORM VALORIZZA-OUTPUT-TGA
                THRU VALORIZZA-OUTPUT-TGA-EX

             SET WTGA-ST-INV(IX-TAB-TGA) TO TRUE

           END-IF.


       S10180-CARICA-TRANCHE-EX.
            EXIT.
      *----------------------------------------------------------------*
      *       LETTURA DELLA TABELLA GAR                                *
      *----------------------------------------------------------------*
      *
       S10110-LEGGI-GARANZIA.
DBG        MOVE 'S10110-LEGGI-GARANZIA' TO WK-LABEL-ERR
DBG        PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
      *
           PERFORM S10120-IMPOSTA-GARANZIA
              THRU S10120-IMPOSTA-GARANZIA-EX.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata
      *
                       MOVE 'GARANZIA NON TROVATA'
                         TO WK-STRING
                       MOVE '005069'
                         TO WK-COD-ERR
                       PERFORM GESTIONE-ERR-STD
                          THRU GESTIONE-ERR-STD-EX
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
      *  --> Operazione eseguita correttamente
      *

                       MOVE IDSO0011-BUFFER-DATI
                         TO GAR
      *
                       PERFORM S10125-CTRL-STATO-GAR
                          THRU S10125-CTRL-STATO-GAR-EX
      *
                       IF WK-SCARTA-GAR-NO
A3365                     ADD 1              TO IX-TAB-GRZ
                          IF IX-TAB-GRZ > WK-GRZ-MAX-B
                             MOVE 'OVERFLOW CARICAMENTO GARANZIA'
                               TO WK-STRING
                             MOVE '005059'
                               TO WK-COD-ERR
                             PERFORM GESTIONE-ERR-STD
                                THRU GESTIONE-ERR-STD-EX

                          ELSE
                             MOVE IX-TAB-GRZ  TO WGRZ-ELE-GAR-MAX
      *
                             PERFORM VALORIZZA-OUTPUT-GRZ
                                THRU VALORIZZA-OUTPUT-GRZ-EX
      *
                             SET WGRZ-ST-INV(IX-TAB-GRZ)
                              TO TRUE
      *
                          END-IF
                       END-IF
      *
                  WHEN OTHER
      *
      *  --> Errore di accesso al db
      *
                       MOVE 'ERRORE LETTURA GARANZIA'
                         TO WK-STRING
                       MOVE '005016'
                         TO WK-COD-ERR
                       PERFORM GESTIONE-ERR-STD
                          THRU GESTIONE-ERR-STD-EX
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore dispatcher
      *
              MOVE 'S10110-LEGGI-GARANZIA'
                TO WK-LABEL-ERR
              MOVE 'ERRORE DISPATCHER LETTURA GAR'
                TO WK-STRING
              MOVE '005016'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S10110-LEGGI-GARANZIA-EX.
           EXIT.
      *
      * -------------------------------------------------------------- *
      *       VALORIZZAZIONE DELLE CHIAVI DELLA TABELLA GAR            *
      * -------------------------------------------------------------- *
      *
       S10120-IMPOSTA-GARANZIA.
      *
           MOVE WPMO-ID-OGG(IX-TAB-PMO)
             TO GRZ-ID-GAR.
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE ZERO
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
                IDSI0011-DATA-COMPETENZA.
      *
      *  --> Nome tabella fisica db
      *
           MOVE 'GAR'
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE GAR
             TO IDSI0011-BUFFER-DATI.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-SELECT
             TO TRUE.
      *
      *  --> Tipo livello
      *
           SET IDSI0011-ID
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSI0011-TRATT-DEFAULT
             TO TRUE.
      *
      *  --> Inizializzazione Return Code
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S10120-IMPOSTA-GARANZIA-EX.
           EXIT.
      *
      * -------------------------------------------------------------- *
      *   CONTROLLO STATO E CASUSALE DELLA GARANZIA                    *
      * -------------------------------------------------------------- *
      *
       S10125-CTRL-STATO-GAR.
DBG        MOVE 'S10125-CTRL-STATO-GAR' TO WK-LABEL-ERR
DBG        PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
      *
      *  Controllo Stato Contratto
      *
           MOVE GRZ-ID-GAR
             TO WK-STB-ID-OGG.
           MOVE 'GA'
             TO WK-STB-TP-OGG.
      *
           SET WK-SCARTA-GAR-NO
             TO TRUE.
      *
           PERFORM S00420-IMPOSTA-STB
              THRU S00420-IMPOSTA-STB-EX.
      *
           PERFORM S00430-LEGGI-STB
              THRU S00430-LEGGI-STB-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              MOVE STB-TP-STAT-BUS
                TO WS-TP-STAT-BUS
              MOVE STB-TP-CAUS
                TO WS-TP-CAUS
      *
      * --> Se garanzia h in stato Vigore ed Erogazione Rendita,
      * -->  imposta flag
      *
A3365         IF IN-VIGORE
      *
29401 *          IF RENDITA-EROGAZIONE
A3365               CONTINUE
29401 *             SET SI-GAR-REND
A3365 *               TO TRUE
29401 *               TO TRUE
      *
A3365 *          END-IF
29401 *          END-IF
A3365         ELSE
      *
A3365            SET WK-SCARTA-GAR-SI
A3365             TO TRUE
      *
A3365         END-IF
      *
           END-IF.
      *
       S10125-CTRL-STATO-GAR-EX.
           EXIT.
      *
      * -------------------------------------------------------------- *
      *   GESTIONE DELL'ACQUISIZIONE DELLE TRANCHE DI GARANZIA         *
      * -------------------------------------------------------------- *
      *
       S10130-ACQUISIZ-TRANCHE.
      *
      * -->> Indicatore di fine fetch
      *
           SET WK-FINE-FETCH-NO
             TO TRUE.
      *
           PERFORM S10140-IMPOSTA-TRANCHE
              THRU S10140-IMPOSTA-TRANCHE-EX

           SET IDSI0011-FETCH-FIRST
             TO TRUE.

           PERFORM S10150-LEGGI-TRANCHE
              THRU S10150-LEGGI-TRANCHE-EX
             UNTIL WK-FINE-FETCH-SI
                OR IDSV0001-ESITO-KO.
      *
       S10130-ACQUISIZ-TRANCHE-EX.
           EXIT.
      *
      * -------------------------------------------------------------- *
      *       VALORIZZAZIONE DELLE CHIAVI DELLA TABELLA TRCH_DI_GAR    *
      * -------------------------------------------------------------- *
      *
       S10140-IMPOSTA-TRANCHE.
      *
           INITIALIZE LDBV0011

           MOVE WGRZ-ID-GAR(IX-TAB-GRZ)
             TO LDBV0011-ID-GAR.

VSTEF *    MODIFICA SIR CQPrd00019650
           MOVE WGRZ-TP-PER-PRE(IX-TAB-GRZ)
             TO ISPV0000-PERIODO-PREM
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE ZERO
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
                IDSI0011-DATA-COMPETENZA.
      *
      *  --> Nome tabella fisica db
      *
           MOVE 'LDBS0130'
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE SPACES
             TO IDSI0011-BUFFER-DATI.

A3365      MOVE LDBV0011
             TO IDSI0011-BUFFER-WHERE-COND.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-WHERE-CONDITION
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSI0011-TRATT-DEFAULT
             TO TRUE.
      *
      *  --> Inizializzazione Return Code
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S10140-IMPOSTA-TRANCHE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *            LETTURA DELLE TABELLA TRANCHE DI GARANZIA           *
      *----------------------------------------------------------------*
      *
       S10150-LEGGI-TRANCHE.
      *

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata
      *
                       IF IDSI0011-FETCH-FIRST
      *
                          MOVE 'S10150-LEGGI-TRANCHE'
                            TO WK-LABEL-ERR
                          MOVE 'TRANCHE NON TROVATA'
                            TO WK-STRING
                          MOVE '005069'
                            TO WK-COD-ERR
                          PERFORM GESTIONE-ERR-STD
                             THRU GESTIONE-ERR-STD-EX
      *
                       END-IF
      *
      *  --> Fine occorrenze fetch
      *
                          SET WK-FINE-FETCH-SI
                            TO TRUE
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
      *  --> Operazione eseguita correttamente
      *
                       MOVE IDSO0011-BUFFER-DATI
                         TO TRCH-DI-GAR
      *
A3365                  SET WK-SCARTA-TGA-NO
A3365                    TO TRUE
      *
                       IF WK-SCARTA-TGA-NO
      *
A3365                     PERFORM S10145-CTRL-STATO-TGA
A3365                        THRU S10145-CTRL-STATO-TGA-EX
      *
                       END-IF
      *
A3365                  IF WK-SCARTA-TGA-NO
      *
                          PERFORM S10160-GESTIONE-DECOR
                             THRU S10160-GESTIONE-DECOR-EX
      *
A3365                  END-IF
      *
                       IF WK-SCARTA-TGA-NO
      *
                          PERFORM S10180-CARICA-TRANCHE
                             THRU S10180-CARICA-TRANCHE-EX
      *
                       END-IF
      *
                       PERFORM S10140-IMPOSTA-TRANCHE
                          THRU S10140-IMPOSTA-TRANCHE-EX
      *
                      SET IDSI0011-FETCH-NEXT
                         TO TRUE
      *
                  WHEN OTHER
      *
      *  --> Errore di accesso al db
      *
                       MOVE 'S10150-LEGGI-TRANCHE'
                         TO WK-LABEL-ERR
                       MOVE 'ERRORE LETTURA TRANCHE'
                         TO WK-STRING
                       MOVE '005016'
                         TO WK-COD-ERR
                       PERFORM GESTIONE-ERR-STD
                          THRU GESTIONE-ERR-STD-EX
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore dispatcher
      *
              MOVE 'S10150-LEGGI-TRANCHE'
                TO WK-LABEL-ERR
              MOVE 'ERRORE DISPATCHER LETTURA TRANCHE'
                TO WK-STRING
              MOVE '005016'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S10150-LEGGI-TRANCHE-EX.
           EXIT.
      *
      * -------------------------------------------------------------- *
      *   CONTROLLO STATO E CASUSALE DELLA TRANCHE DI GARANZIA
      * -------------------------------------------------------------- *
      *
A3365  S10145-CTRL-STATO-TGA.
      *
      *  Controllo Stato Contratto
      *
A3365      MOVE TGA-ID-TRCH-DI-GAR
A3365        TO WK-STB-ID-OGG.
A3365      MOVE 'TG'
A3365        TO WK-STB-TP-OGG.
      *

A3365      PERFORM S00420-IMPOSTA-STB
A3365         THRU S00420-IMPOSTA-STB-EX.
      *
A3365      PERFORM S00430-LEGGI-STB
A3365         THRU S00430-LEGGI-STB-EX.
      *
A3365      IF IDSV0001-ESITO-OK
      *
A3365         MOVE STB-TP-STAT-BUS
A3365           TO WS-TP-STAT-BUS
A3365         MOVE STB-TP-CAUS
A3365           TO WS-TP-CAUS
      *
      * --> Se wcom-write-niente, forzatura su idsv0001-esito-ko
      * --> per skippare tutta l'elaborazione
      *
              IF IN-VIGORE
      *
22911 *          CONTINUE
22911            IF WK-ORIG-RIV-IN           AND
22911               ATTESA-PERFEZIONAMENTO   AND
22911              (WGRZ-TP-PER-PRE (1) = 'R' OR 'U')
22911               SET WK-SCARTA-TGA-SI TO TRUE
22911            END-IF
      *
              ELSE
      *
                 SET WK-SCARTA-TGA-SI
                  TO TRUE
      *
              END-IF
      *
           END-IF.
      *
A3365  S10145-CTRL-STATO-TGA-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  SCARTO TRANCHE CON DATA DECOR = A DATA DI RICORRENZA          *
      *----------------------------------------------------------------*
      *
       S10160-GESTIONE-DECOR.
      *
14195 *    IF WPMO-DT-RICOR-SUCC(IX-TAB-PMO) = TGA-DT-DECOR
14195      IF WPMO-DT-RICOR-SUCC(IX-TAB-PMO) <= TGA-DT-DECOR

              SET WK-SCARTA-TGA-SI
                TO TRUE
      *
           ELSE
      *
              IF WPMO-TP-OGG-RIVAL(1) = 'TG'
      *
                 PERFORM S10165-DECOR-X-RIVAL-TGA
                    THRU S10165-DECOR-X-RIVAL-TGA-EX
      *
              END-IF
      *
           END-IF.
      *
       S10160-GESTIONE-DECOR-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  SCARTO TRANCHE CON DATA DECOR (MM-GG) <> DA DATA RICORRENZA   *
      *         SOLO PER RIVALUTAZIONE A RICORRENZA DI TRANCHE         *
      *----------------------------------------------------------------*
      *
       S10165-DECOR-X-RIVAL-TGA.
      *
           MOVE ZEROES                         TO WK-APPO-DT-NUM
           MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO) TO WK-APPO-DT-NUM
           MOVE WK-APPO-DT-NUM                 TO WK-APPO-DT
      *
           MOVE ZEROES                         TO WK-APPO-DT-NUM
           MOVE TGA-DT-DECOR                   TO WK-APPO-DT-NUM
      *       TO WK-DATA-AMG.
           MOVE WK-APPO-DT-NUM
             TO WK-DATA-AMG.
      *
           IF MM-SYS OF WK-DATA-AMG NOT = WK-APPO-DT-MM OR
              GG-SYS OF WK-DATA-AMG NOT = WK-APPO-DT-GG

               SET WK-SCARTA-TGA-SI
                TO TRUE
      *
           END-IF.
      *
       S10165-DECOR-X-RIVAL-TGA-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * GESTIONE DELLA DATA RICORRENZA SUCCESSIVA DELLA PMO
      *----------------------------------------------------------------*
      *
       S10450-GEST-RIC-SUCC-PMO.
      *
           MOVE 'S10450-GEST-RIC-SUCC-PMO'
             TO WK-LABEL-ERR.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LCCS0320'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'DT Ricorr. Succ. PMO'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           CALL LCCS0320 USING AREA-IDSV0001
                               VPMO-AREA-PARAM-MOVI
                               WPOL-AREA-POLIZZA
                               WADE-AREA-ADESIONE
                               WGRZ-AREA-GARANZIA
                               WTGA-AREA-TRANCHE
      *
           ON EXCEPTION
      *
                MOVE 'LCCS0320'
                  TO IEAI9901-COD-SERVIZIO-BE
                MOVE 'ERRORE CALL MODULO LCCS0320'
                  TO CALL-DESC
      *
                PERFORM GESTIONE-ERR-SIST
                   THRU GESTIONE-ERR-SIST-EX
      *
           END-CALL.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LCCS0320'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'DT RICORR. SUCC. PMO'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
       S10450-GEST-RIC-SUCC-PMO-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *              GESTIONE DEL TIPO DI RIVALUTAZIONE                *
      *----------------------------------------------------------------*
      *
       S10200-RIVAL-X-INCASSO-COL.
      *
DBG        MOVE 'S10200-RIVAL-X-INCASSO-COL'
             TO WK-LABEL-ERR.
      *
           PERFORM S10210-PREP-CALL-LOAS0670
              THRU S10210-PREP-CALL-LOAS0670-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10220-CALL-LOAS0670
                 THRU S10220-CALL-LOAS0670-EX
      *
           END-IF.
      *
       S10200-RIVAL-X-INCASSO-COL-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  PREPARA AREA DATI PER CALL SERVIZIO LOAS0670                  *
      *----------------------------------------------------------------*
      *
       S10210-PREP-CALL-LOAS0670.
      *
DBG        MOVE 'S10210-PREP-CALL-LOAS0670' TO WK-LABEL-ERR
DBG        PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
      *
           SET W670-GGCONTESTO-CONT-NO   TO TRUE
           MOVE ZEROES                   TO W670-GGINCASSO
MM                                          W670-NUM-MAX-ELE.
      *
       S10210-PREP-CALL-LOAS0670-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *            CALL SERVIZIO LOAS0670                              *
      *----------------------------------------------------------------*
      *
       S10220-CALL-LOAS0670.
      *
            MOVE 'S10220-CALL-LOAS0670'
              TO WK-LABEL-ERR.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LOAS0670'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Rivalut x incasso coll.'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
            CALL LOAS0670        USING AREA-IDSV0001
                                       WCOM-AREA-STATI
                                       WPMO-AREA-PARAM-MOVI
                                       WPOL-AREA-POLIZZA
                                       WADE-AREA-ADESIONE
                                       WGRZ-AREA-GARANZIA
                                       WTGA-AREA-TRANCHE
                                       W670-AREA-LOAS0670
      *
            ON EXCEPTION
      *
                 MOVE 'LOAS0670'
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'ERRORE CALL MODULO LOAS0670'
                   TO CALL-DESC
                 MOVE 'S10220-CALL-LOAS0670'
                   TO WK-LABEL-ERR
      *
                 PERFORM GESTIONE-ERR-SIST
                    THRU GESTIONE-ERR-SIST-EX
      *
            END-CALL.


           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LOAS0670'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Rivalut x incasso coll.'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
       S10220-CALL-LOAS0670-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *              GESTIONE DEL TIPO DI RIVALUTAZIONE                *
      *----------------------------------------------------------------*
      *
       S10300-RIVAL-X-INCASSO-IND.
      *
DBG        MOVE 'S10300-RIVAL-X-INCASSO-IND'
             TO WK-LABEL-ERR.
      *
           PERFORM S10310-PREP-CALL-LOAS0870
              THRU S10310-PREP-CALL-LOAS0870-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10320-CALL-LOAS0870
                 THRU S10320-CALL-LOAS0870-EX
      *
           END-IF.
      *
       S10300-RIVAL-X-INCASSO-IND-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  PREPARA AREA DATI PER CALL SERVIZIO LOAS0870                  *
      *----------------------------------------------------------------*
      *
       S10310-PREP-CALL-LOAS0870.
      *
           MOVE 'S10310-PREP-CALL-LOAS0870'
             TO WK-LABEL-ERR.
      *
DBG        PERFORM DISPLAY-LABEL
DBG           THRU DISPLAY-LABEL-EX
      *
           SET W870-GGCONTESTO-CONT-NO
             TO TRUE
           MOVE ZEROES
             TO W870-GGINCASSO
                W870-TGA-NUM-MAX-ELE.
      *
      *   INIZIALIZZAZIONE AREA DATI PER CALL SERVIZIO LOAS0870
      *
ALEX       INITIALIZE  W870-ID-TRCH-DI-GAR  (1)
                       W870-TIT-NUM-MAX-ELE (1).

ALEX       PERFORM VARYING IX-TIT-W870 FROM 1 BY 1
             UNTIL IX-TIT-W870 > WK-MAX-TIT-W870
             INITIALIZE   W870-DATI-TIT(1,IX-TIT-W870)
           END-PERFORM.

           MOVE  W870-TAB-TIT     TO W870-RESTO-TAB-TIT.

      *
       S10310-PREP-CALL-LOAS0870-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *            CALL SERVIZIO LOAS0870                              *
      *----------------------------------------------------------------*
      *
       S10320-CALL-LOAS0870.
      *
            MOVE 'S10320-CALL-LOAS0870'
              TO WK-LABEL-ERR.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LOAS0870'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Rivalut x incasso ind.'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
            CALL LOAS0870        USING AREA-IDSV0001
                                       WCOM-IO-STATI
                                       WPMO-AREA-PARAM-MOVI
                                       WPOL-AREA-POLIZZA
                                       WADE-AREA-ADESIONE
                                       WGRZ-AREA-GARANZIA
                                       WTGA-AREA-TRANCHE
                                       W870-AREA-LOAS0870
      *
            ON EXCEPTION
      *
                 MOVE 'LOAS0870'
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'ERRORE CALL MODULO LOAS0870'
                   TO CALL-DESC
                 MOVE 'S10220-CALL-LOAS0870'
                   TO WK-LABEL-ERR
      *
                 PERFORM GESTIONE-ERR-SIST
                    THRU GESTIONE-ERR-SIST-EX
      *
           END-CALL.
      *
           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LOAS0870'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Rivalut x incasso ind.'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
       S10320-CALL-LOAS0870-EX.
           EXIT.
      *
      *
      *----------------------------------------------------------------*
      *            GESTIONE CHIAMATA AL VALORIZZATORE VARIABILI        *
      *----------------------------------------------------------------*
      *
       S10500-GEST-VALORIZZATORE.
      *
           MOVE 'S10500-GEST-VALORIZZATORE'
             TO WK-LABEL-ERR.
DBG        PERFORM DISPLAY-LABEL
DBG           THRU DISPLAY-LABEL-EX.

      *
ALEX  *     INITIALIZE WSKD-AREA-SCHEDA.
      *
      *--  INIZIALIZZAZIONE AREA SCHEDA VARIABILI

           INITIALIZE             WSKD-ELE-LIVELLO-MAX-P
                                  WSKD-DEE
                                  WSKD-ID-POL-P            (1)
                                  WSKD-COD-TIPO-OPZIONE-P  (1)
                                  WSKD-TP-LIVELLO-P        (1)
                                  WSKD-COD-LIVELLO-P       (1)
                                  WSKD-ID-LIVELLO-P        (1)
                                  WSKD-DT-INIZ-PROD-P      (1)
                                  WSKD-COD-RGM-FISC-P      (1)
                                  WSKD-NOME-SERVIZIO-P     (1)
                                  WSKD-ELE-VARIABILI-MAX-P (1)
                                  WSKD-AREA-VARIABILI-P    (1)
                                  WSKD-VAR-AUT-OPER.

           MOVE WSKD-TAB-VAL-P TO WSKD-RESTO-TAB-VAL-P.

           INITIALIZE             WSKD-ELE-LIVELLO-MAX-T
                                  WSKD-ID-GAR-T            (1)
                                  WSKD-COD-TIPO-OPZIONE-T  (1)
                                  WSKD-TP-LIVELLO-T        (1)
                                  WSKD-COD-LIVELLO-T       (1)
                                  WSKD-ID-LIVELLO-T        (1)
                                  WSKD-DT-DECOR-TRCH-T     (1)
                                  WSKD-DT-INIZ-TARI-T      (1)
                                  WSKD-COD-RGM-FISC-T      (1)
                                  WSKD-NOME-SERVIZIO-T     (1)
                                  WSKD-ELE-VARIABILI-MAX-T (1)
                                  WSKD-AREA-VARIABILI-T    (1)

            MOVE WSKD-TAB-VAL-T TO WSKD-RESTO-TAB-VAL-T.

           PERFORM S10510-PREPARA-AREA-VAL
              THRU S10510-PREPARA-AREA-VAL-EX.
      *
           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'IVVS0211'                     TO IDSV8888-NOME-PGM
           MOVE 'Valoriz. variab. step A'      TO IDSV8888-DESC-PGM
           SET  IDSV8888-INIZIO                TO TRUE
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM CALL-VALORIZZATORE
              THRU CALL-VALORIZZATORE-EX.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'IVVS0211'                     TO IDSV8888-NOME-PGM
           MOVE 'Valoriz. variab. step A'      TO IDSV8888-DESC-PGM
           SET  IDSV8888-FINE                  TO TRUE
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *

           IF IDSV0001-ESITO-OK
      *
47672         SET  S211-FL-AREA-VAR-EXTRA-NO TO TRUE
              MOVE S211-BUFFER-DATI
                TO WSKD-AREA-SCHEDA
      *
           ELSE
      *
              MOVE 'S10500-GEST-VALORIZZATORE'
                TO WK-LABEL-ERR
              MOVE 'ERRORE CHIAMATA VALORIZZATORE'
                TO WK-STRING
              MOVE '001114'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S10500-GEST-VALORIZZATORE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *             VALORIZZAZIONE DELL'AREA DI INPUT DEL              *
      *               SERVIZIO VALORIZZATORE VARIABILI                 *
      *----------------------------------------------------------------*
      *
       S10510-PREPARA-AREA-VAL.
      *
           MOVE 'S10510-PREPARA-AREA-VAL'
             TO WK-LABEL-ERR.
      *
      * --> Valorizzazione fittizia della dclgen della tabella
      * --> Movimento per consentire l'eventuale calcolo delle
      * --> variabili  DEE e DEEL
      *
      *    Inizializzazione area io valorizzatore variabile
      *
ALEX  *     INITIALIZE AREA-IO-IVVS0211.

           INITIALIZE S211-DATI-INPUT
                      S211-RESTO-DATI
                      S211-TAB-INFO(1).

           MOVE  S211-TAB-INFO1     TO  S211-RESTO-TAB-INFO1.

           IF WK-ORIG-RIV-IN AND
              WPOL-TP-FRM-ASSVA = 'CO'
              PERFORM S10520-CARICA-GRZ-E-TRCH
                 THRU S10520-EX
           ELSE
22200        IF  WK-ORIG-RIV-IN AND
  =              WPOL-TP-FRM-ASSVA = 'IN'
  =              PERFORM S10520-CARICA-GRZ-E-TRCH
  =                 THRU S10520-EX
22200        ELSE
                 MOVE WGRZ-AREA-GARANZIA
                   TO VGRZ-AREA-GARANZIA
                 MOVE WTGA-AREA-TRANCHE
                   TO VTGA-AREA-TRANCHE
              END-IF
           END-IF
      *
           MOVE IDSI0011-TRATTAMENTO-STORICITA
             TO S211-TRATTAMENTO-STORICITA.
      *
           SET S211-BATCH                      TO TRUE.
           SET S211-AREA-PV                    TO TRUE.
      *
           SET S211-IN-CONV
             TO TRUE.
      *
           MOVE WCOM-DT-ULT-VERS-PROD
             TO S211-DATA-ULT-VERS-PROD.
      *
           MOVE IDSV0001-TIPO-MOVIMENTO
             TO S211-TIPO-MOVIMENTO.
      *
           MOVE SPACES
             TO S211-COD-MAIN-BATCH.
           MOVE SPACES
             TO S211-COD-SERVIZIO-BE.
      *
           MOVE ZEROES
             TO S211-DATA-COMPETENZA.
      *
           MOVE 1
             TO WK-APPO-LUNGHEZZA.
      *
      *  -->  Valorizzazione della struttura di mapping
      *
           MOVE 6
             TO S211-ELE-INFO-MAX.
      *
      *  --> Occorrenza 1
      *
           MOVE S211-ALIAS-POLI
             TO S211-TAB-ALIAS(1).
           MOVE WK-APPO-LUNGHEZZA
             TO S211-POSIZ-INI(1).
           MOVE LENGTH OF WPOL-AREA-POLIZZA
             TO S211-LUNGHEZZA(1).
      *
           COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                       S211-LUNGHEZZA(1) + 1.
      *
      *  --> Occorrenza 2
      *
           MOVE S211-ALIAS-ADES
             TO S211-TAB-ALIAS(2).
           MOVE WK-APPO-LUNGHEZZA
             TO S211-POSIZ-INI(2).
           MOVE LENGTH OF WADE-AREA-ADESIONE
             TO S211-LUNGHEZZA(2).
      *
           COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                       S211-LUNGHEZZA(2) + 1
      *
      *  --> Occorrenza 3
      *
           MOVE S211-ALIAS-GARANZIA
             TO S211-TAB-ALIAS(3)
           MOVE WK-APPO-LUNGHEZZA
             TO S211-POSIZ-INI(3)
           MOVE LENGTH OF VGRZ-AREA-GARANZIA
             TO S211-LUNGHEZZA(3)
      *
           COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                       S211-LUNGHEZZA(3) + 1
      *
      *  --> Occorrenza 4
      *
           MOVE S211-ALIAS-TRCH-GAR
             TO S211-TAB-ALIAS(4)
           MOVE WK-APPO-LUNGHEZZA
             TO S211-POSIZ-INI(4)
           MOVE LENGTH OF VTGA-AREA-TRANCHE
             TO S211-LUNGHEZZA(4)
      *
           COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                       S211-LUNGHEZZA(4) + 1
      *
      *  --> Occorrenza 5
      *
           MOVE S211-ALIAS-MOVIMENTO
             TO S211-TAB-ALIAS(5)
           MOVE WK-APPO-LUNGHEZZA
             TO S211-POSIZ-INI(5)
           MOVE LENGTH OF WMOV-AREA-MOVIMENTO
             TO S211-LUNGHEZZA(5)
      *
           COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                       S211-LUNGHEZZA(5) + 1
      *
      *  --> Occorrenza 6
      *
           MOVE S211-ALIAS-PARAM-MOV
             TO S211-TAB-ALIAS(6)
           MOVE WK-APPO-LUNGHEZZA
             TO S211-POSIZ-INI(6)
           MOVE LENGTH OF WPMO-AREA-PARAM-MOVI
             TO S211-LUNGHEZZA(6)
      *
           COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                       S211-LUNGHEZZA(6) + 1
      *
      *  -->  Valorizzazione del buffer dati
      *
           MOVE WPOL-AREA-POLIZZA
             TO S211-BUFFER-DATI(S211-POSIZ-INI(1): S211-LUNGHEZZA(1)).
      *
           MOVE WADE-AREA-ADESIONE
             TO S211-BUFFER-DATI(S211-POSIZ-INI(2): S211-LUNGHEZZA(2)).
      *
           MOVE VGRZ-AREA-GARANZIA
             TO S211-BUFFER-DATI(S211-POSIZ-INI(3): S211-LUNGHEZZA(3)).
      *
           MOVE VTGA-AREA-TRANCHE
             TO S211-BUFFER-DATI(S211-POSIZ-INI(4): S211-LUNGHEZZA(4)).
      *
           MOVE WMOV-AREA-MOVIMENTO
             TO S211-BUFFER-DATI(S211-POSIZ-INI(5): S211-LUNGHEZZA(5)).
      *
           MOVE WPMO-AREA-PARAM-MOVI
             TO S211-BUFFER-DATI(S211-POSIZ-INI(6): S211-LUNGHEZZA(6)).
      *
47672      SET S211-FL-AREA-VAR-EXTRA-SI   TO TRUE.

       S10510-PREPARA-AREA-VAL-EX.
           EXIT.
      *---------------------------------------------------------------
      * CARICAMENTO AREA  TRANCHE DI GARANZIA E GARANZIA
      * PER VALORIZZATORE
      *---------------------------------------------------------------
       S10520-CARICA-GRZ-E-TRCH.
           MOVE 'S10520-CARICA-GRZ-E-TRCH'
             TO WK-LABEL-ERR.

              PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
               UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
                 SET WK-TRANCHE-NON-CARICATA TO TRUE
                 PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
                    UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX

                       IF WTGA-TP-RIVAL(IX-TAB-TGA) NOT = 'NU'
                             AND
                          WTGA-ID-GAR(IX-TAB-TGA) =
                          WGRZ-ID-GAR(IX-TAB-GRZ)

                          ADD 1 TO VTGA-ELE-TRAN-MAX
                          MOVE WTGA-TAB-TRAN(IX-TAB-TGA)
                            TO VTGA-TAB-TRAN(VTGA-ELE-TRAN-MAX)
                          SET WK-TRANCHE-CARICATA TO TRUE

                       END-IF

                 END-PERFORM
                 IF WK-TRANCHE-CARICATA
                    ADD 1 TO VGRZ-ELE-GAR-MAX
                    MOVE WGRZ-TAB-GAR(IX-TAB-GRZ)
                      TO VGRZ-TAB-GAR(VGRZ-ELE-GAR-MAX)
                 END-IF
              END-PERFORM.

       S10520-EX.
           EXIT.
      *
       S10599-SALVA-PRSTZ-PREC.
           MOVE 'S10599-SALVA-PRSTZ-PREC'
             TO WK-LABEL-ERR.

           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
                MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                      TO WPREC-ID-TRCH-DI-GAR(IX-TAB-TGA)
                IF WTGA-PRSTZ-ULT-NULL(IX-TAB-TGA) NOT = HIGH-VALUE
                   MOVE WTGA-PRSTZ-ULT(IX-TAB-TGA)
                      TO WPREC-PRSTZ-ULT(IX-TAB-TGA)
                ELSE
                   MOVE ZEROES
                      TO  WPREC-PRSTZ-ULT(IX-TAB-TGA)
                END-IF
           END-PERFORM.

       S10599-SALVA-PRSTZ-PREC-EX.
           EXIT.
      * -------------------------------------------------------------- *
      * SECTION DI PREPARAZIONE ALLA CALL DEL SERVIZIO DI RIVALUTAZIONE
      * -------------------------------------------------------------- *
      *
       S10600-PREP-CALL-LOAS0660.
      *
           MOVE 'S10600-PREP-CALL-LOAS0660'
             TO WK-LABEL-ERR.
      *
           SET W660-MANFEE-CONT-NO
             TO TRUE.
      *
           MOVE ZEROES
             TO W660-PERMANFEE
                W660-MESIDIFFMFEE
                W660-DECADELMFEE.
      *
           SET W660-MF-CALCOLATO-NO
             TO TRUE.
      *
       S10600-PREP-CALL-LOAS0660-EX.
           EXIT.
      *
      * -------------------------------------------------------------- *
      * CALL DEL SERVIZIO DI RIVALUTAZIONE
      * -------------------------------------------------------------- *
      *
       S10700-CALL-LOAS0660.
      *
           MOVE 'S10700-CALL-LOAS0660'
             TO WK-LABEL-ERR.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LOAS0660'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Serv. di rivalutazione'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           CALL LOAS0660         USING AREA-IDSV0001
                                       WCOM-IO-STATI
                                       WPMO-AREA-PARAM-MOVI
                                       WMOV-AREA-MOVIMENTO
                                       WPOL-AREA-POLIZZA
                                       WADE-AREA-ADESIONE
                                       WGRZ-AREA-GARANZIA
                                       WTGA-AREA-TRANCHE
                                       WSKD-AREA-SCHEDA
                                       W660-AREA-LOAS0660
      *
            ON EXCEPTION
      *
                 MOVE 'LOAS0660'
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'ERRORE CALL MODULO LOAS0660'
                   TO CALL-DESC
                 MOVE 'S10700-CALL-LOAS0660'
                   TO WK-LABEL-ERR
      *
                 PERFORM GESTIONE-ERR-SIST
                    THRU GESTIONE-ERR-SIST-EX
      *
            END-CALL.


           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LOAS0660'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Serv. di rivalutazione'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
       S10700-CALL-LOAS0660-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *           GESTIONE DELLA CHIAMATA AL MODULO DI EOC             *
      *----------------------------------------------------------------*
      *
       S10900-GESTIONE-EOC.
      *
      * --> Merge tra l'area di PMO con le date calcolate e
      * --> quella con le nuove occorrenze
      *
      *
      *    CONTROLLO SE E' STATO CREATO UN MOVIMENTO DI MANFEE
      *    SE E' STATO CREATO ACCODO IL MOVIMENTO 6006 ALL'ARRAY
      *    ALTRIMENTI VADO A RICOPRIRE L'INTERA AREA
22227 *       MOVE VPMO-AREA-PARAM-MOVI
22227 *         TO WPMO-AREA-PARAM-MOVI

22584 *       MOVE VPMO-TAB-PARAM-MOV(1)
22584 *         TO WPMO-TAB-PARAM-MOV(1)

22584      PERFORM
  =           VARYING IX-VPMO FROM 1 BY 1
  =             UNTIL IX-VPMO GREATER VPMO-ELE-PARAM-MOV-MAX
  =               IF NOT VPMO-ST-INV(IX-VPMO)
  =                  MOVE VPMO-TAB-PARAM-MOV(IX-VPMO)
  =                    TO WPMO-TAB-PARAM-MOV(IX-VPMO)
  =               END-IF
22584      END-PERFORM

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LOAS0320'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Servizio di EOC'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           CALL LOAS0320         USING AREA-IDSV0001
                                       WGRZ-AREA-GARANZIA
                                       WTGA-AREA-TRANCHE
                                       AREA-MAIN
                                       WCOM-IO-STATI
A3227                                  IABV0006
      *
           ON EXCEPTION
      *
                 MOVE 'LOAS0320'
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'SERVIZIO LOAS0320'
                   TO CALL-DESC
                 MOVE 'S10940-CALL-LOAS0320'
                   TO WK-LABEL-ERR
      *
                 PERFORM GESTIONE-ERR-SIST
                    THRU GESTIONE-ERR-SIST-EX
      *
           END-CALL.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LOAS0320'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Servizio di EOC'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
       S10900-GESTIONE-EOC-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                 GESTIONE DEL FILE DI OUTPUT                    *
      *----------------------------------------------------------------*
      *
       S10950-GESTIONE-FILEOUT.
      *
A3365      INITIALIZE W-REC-OUTRIVA
      *
A3365      PERFORM S10960-VALORIZZA-REC-GEN
A3365         THRU S10960-VALORIZZA-REC-GEN-EX.
      *
           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
                OR IDSV0001-ESITO-KO
      *
                 INITIALIZE W-REC-OUTRIVA
      *
                 PERFORM S10970-VALORIZZA-REC-DET
                    THRU S10970-VALORIZZA-REC-DET-EX
      *
           END-PERFORM.
      *
       S10950-GESTIONE-FILEOUT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                 GESTIONE DEL FILE DI OUTPUT   - REC GEN -      *
      *----------------------------------------------------------------*
      *
       S10960-VALORIZZA-REC-GEN.
      *
           MOVE 'GEN'
             TO WREC-TIPO-REC.
           MOVE 'OA'
             TO WREC-MACROFUNZ.
           MOVE IDSV0001-TIPO-MOVIMENTO
             TO WREC-FUNZ.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO WREC-COD-COMP.
           MOVE WCOM-PERIODO-ELAB-DA
             TO WREC-DT-RICH-DA.
           MOVE WCOM-PERIODO-ELAB-A
             TO WREC-DT-RICH-A.

           IF WPOL-COD-RAMO-NULL = HIGH-VALUES
              MOVE SPACES
                TO WREC-RAMO
           ELSE
              MOVE WPOL-COD-RAMO
                TO WREC-RAMO
           END-IF
      *
           IF WPOL-TP-FRM-ASSVA = 'IN'
      *
              IF WPOL-IB-OGG-NULL = HIGH-VALUES
                 MOVE SPACES
                   TO WREC-NUM-POL-IND OF WREC-GEN
              ELSE
                 MOVE WPOL-IB-OGG
                   TO WREC-NUM-POL-IND OF WREC-GEN
              END-IF
      *
           ELSE
      *
              IF WPOL-IB-OGG-NULL = HIGH-VALUES
                 MOVE SPACES
                   TO WREC-NUM-POL-COLL
              ELSE
                 MOVE WPOL-IB-OGG
                   TO WREC-NUM-POL-COLL
              END-IF

              IF WADE-IB-OGG-NULL(1) = HIGH-VALUES
                 MOVE SPACES
                   TO WREC-NUM-ADESIONE
              ELSE
                 MOVE WADE-IB-OGG(1)
                   TO WREC-NUM-ADESIONE
              END-IF

      *
           END-IF.
      *
A3365      MOVE IDSV0001-DATA-COMPETENZA(1:8)
A3365        TO WREC-DT-COMP-RIVA.

12700      INSPECT W-REC-OUTRIVA REPLACING ALL LOW-VALUE BY SPACES
12700      INSPECT W-REC-OUTRIVA REPLACING ALL HIGH-VALUE BY SPACES
      *
           WRITE OUTRIVA-REC FROM W-REC-OUTRIVA.
      *
           IF FS-OUT NOT = '00'
      *
              MOVE 'S10960-VALORIZZA-REC-GEN'
                TO WK-LABEL-ERR
              MOVE 'ERRORE WRITE FILE DI OUTPUT'
                TO WK-STRING
              MOVE '001114'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S10960-VALORIZZA-REC-GEN-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                 GESTIONE DEL FILE DI OUTPUT   - REC DET -      *
      *----------------------------------------------------------------*
      *
       S10970-VALORIZZA-REC-DET.
      *
           MOVE 'DET'
             TO WREC-TIPO-REC.

12699      IF WPOL-TP-FRM-ASSVA = 'IN' AND
              WPOL-IB-OGG-NULL NOT = HIGH-VALUES
              MOVE WPOL-IB-OGG
                   TO WREC-NUM-POL-IND OF WREC-DET
           ELSE
              MOVE SPACES
                   TO WREC-NUM-POL-IND OF WREC-DET
12699      END-IF.

           PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
             UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
      *
                IF WGRZ-ID-GAR(IX-TAB-GRZ) =
                   WTGA-ID-GAR(IX-TAB-TGA)
      *
                   MOVE WGRZ-COD-TARI(IX-TAB-GRZ)
                     TO WREC-COD-GAR-TRCH
      *
                END-IF
      *
           END-PERFORM.
      *
11295      IF WTGA-IB-OGG-NULL(IX-TAB-TGA) = HIGH-VALUES
11295         MOVE ALL SPACES              TO WREC-NUM-TRCH
11295      ELSE
              MOVE WTGA-IB-OGG(IX-TAB-TGA)
                TO WREC-NUM-TRCH
11295      END-IF.

12881      MOVE WTGA-DT-DECOR(IX-TAB-TGA)
12881        TO WREC-DT-DECOR-TRCH
      *
A3365      IF WTGA-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA) = HIGH-VALUES
A3365         MOVE 0
A3365           TO WREC-DT-ULT-RIVA-TRCH
A3365      ELSE
A3365         MOVE WTGA-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
A3365           TO WREC-DT-ULT-RIVA-TRCH
A3365      END-IF.
      *
A3365       IF WTGA-PRE-RIVTO-NULL(IX-TAB-TGA) = HIGH-VALUES
A3365          MOVE 0
A3365            TO WREC-PRE-RIVTO

A3365       ELSE
A3365          MOVE WTGA-PRE-RIVTO(IX-TAB-TGA)
A3365            TO WREC-PRE-RIVTO
A3365       END-IF.


12888      SET PREC-TROVATO-NO TO TRUE
12888      PERFORM VARYING IX-TAB-PREC FROM 1 BY 1 UNTIL
                           IX-TAB-PREC > WTGA-ELE-TRAN-MAX OR
                           PREC-TROVATO-SI
              IF WPREC-ID-TRCH-DI-GAR(IX-TAB-PREC) =
                 WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                 MOVE WPREC-PRSTZ-ULT(IX-TAB-PREC)
                   TO WREC-PRSTZ-PREC
                 SET PREC-TROVATO-SI
                   TO TRUE
              END-IF
           END-PERFORM
      *
           IF WTGA-PRSTZ-ULT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-PRSTZ-ULT(IX-TAB-TGA)
                TO WREC-PRSTZ-ULT
      *
           ELSE
      *
              MOVE 0
                TO WREC-PRSTZ-ULT
      *
           END-IF.
      *
           IF WTGA-PRE-PP-ULT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
              MOVE WTGA-PRE-PP-ULT(IX-TAB-TGA)
                TO WREC-PRE-PP-ULT
           ELSE
              MOVE ZEROES
                TO WREC-PRE-PP-ULT
           END-IF.
      *
           IF WTGA-IMP-SOPR-SAN-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-IMP-SOPR-SAN(IX-TAB-TGA)
                TO WREC-IMP-SOPR-SAN
      *
           ELSE
      *
              MOVE 0
                TO WREC-IMP-SOPR-SAN
      *
           END-IF.
      *
           IF WTGA-IMP-SOPR-PROF-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-IMP-SOPR-PROF(IX-TAB-TGA)
                TO WREC-IMP-SOPR-PROF
      *
           ELSE
      *
              MOVE 0
                TO WREC-IMP-SOPR-PROF
      *
           END-IF.
      *
           IF WTGA-IMP-SOPR-SPO-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-IMP-SOPR-SPO(IX-TAB-TGA)
                TO WREC-IMP-SOPR-SPO
      *
           ELSE
      *
              MOVE 0
                TO WREC-IMP-SOPR-SPO
      *
           END-IF.
      *
           IF WTGA-IMP-SOPR-TEC-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-IMP-SOPR-TEC(IX-TAB-TGA)
                TO WREC-IMP-SOPR-TEC
      *
           ELSE
      *
              MOVE 0
                TO WREC-IMP-SOPR-TEC
      *
           END-IF.
      *
           IF WTGA-IMP-ALT-SOPR-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-IMP-ALT-SOPR(IX-TAB-TGA)
                TO WREC-IMP-ALT-SOPR
      *
           ELSE
      *
              MOVE 0
                TO WREC-IMP-ALT-SOPR
      *
           END-IF.
      *
           IF WTGA-PRE-UNI-RIVTO-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-PRE-UNI-RIVTO(IX-TAB-TGA)
                TO WREC-PRE-UNI-RIVTO
      *
           ELSE
      *
              MOVE 0
                TO WREC-PRE-UNI-RIVTO
      *
           END-IF.
      *
           IF WTGA-PRE-INVRIO-ULT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-PRE-INVRIO-ULT(IX-TAB-TGA)
                TO WREC-PRE-INVRIO-ULT
      *
           ELSE
      *
              MOVE 0
                TO WREC-PRE-INVRIO-ULT
      *
           END-IF.
      *
           IF WTGA-RIS-MAT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
               MOVE WTGA-RIS-MAT(IX-TAB-TGA)
                 TO WREC-RIS-MAT
      *
            ELSE
      *
               MOVE 0
                 TO WREC-RIS-MAT
      *
            END-IF.
      *
           IF WTGA-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
                TO WREC-CPT-IN-OPZ-RIVTO
      *
           ELSE
      *
              MOVE 0
                TO WREC-CPT-IN-OPZ-RIVTO
      *
           END-IF.
      *
           IF WTGA-RENDTO-LRD-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
31397 *       MOVE WTGA-RENDTO-LRD(IX-TAB-TGA)
31397 *         TO WREC-RENDTO-LRD
31397         COMPUTE WREC-RENDTO-LRD ROUNDED =
31397             WTGA-RENDTO-LRD(IX-TAB-TGA)
      *
           ELSE
      *
              MOVE 0
                TO WREC-RENDTO-LRD
      *
           END-IF.
      *
           IF WTGA-PC-RETR-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
31397 *       MOVE WTGA-PC-RETR(IX-TAB-TGA)
31397 *         TO WREC-PC-RETR
31397         COMPUTE WREC-PC-RETR    ROUNDED =
31397             WTGA-PC-RETR(IX-TAB-TGA)
      *
           ELSE
      *
              MOVE 0
                TO WREC-PC-RETR
      *
           END-IF.
      *
           IF WTGA-MIN-TRNUT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
31397 *       MOVE WTGA-MIN-TRNUT(IX-TAB-TGA)
31397 *         TO WREC-MIN-TRNUT
31397         COMPUTE WREC-MIN-TRNUT  ROUNDED =
31397             WTGA-MIN-TRNUT(IX-TAB-TGA)
      *
           ELSE
      *
              MOVE 0
                TO WREC-MIN-TRNUT
      *
           END-IF.
      *
           IF WTGA-MIN-GARTO-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
31397 *       MOVE WTGA-MIN-GARTO(IX-TAB-TGA)
31397 *         TO WREC-MIN-GARTO
31397         COMPUTE WREC-MIN-GARTO  ROUNDED =
31397             WTGA-MIN-GARTO(IX-TAB-TGA)
      *
           ELSE
      *
              MOVE 0
                TO WREC-MIN-GARTO
      *
           END-IF.
      *
           IF WTGA-RENDTO-RETR-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
XGODI *       MOVE WTGA-RENDTO-RETR(IX-TAB-TGA)
XGODI *         TO WREC-RENDTO-RETR
XGODI         COMPUTE WREC-RENDTO-RETR ROUNDED =
XGODI             WTGA-RENDTO-RETR(IX-TAB-TGA)
      *
           ELSE
      *
              MOVE 0
                TO WREC-RENDTO-RETR
      *
           END-IF.
      *
A3365      IF WTGA-TS-RIVAL-NET-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
A3365 *       MOVE WTGA-TS-RIVAL-NET(IX-TAB-TGA)
A3365 *         TO WREC-RENDTO-NET
31397         COMPUTE WREC-RENDTO-NET ROUNDED =
31397             WTGA-TS-RIVAL-NET(IX-TAB-TGA)
      *
A3365      ELSE
      *
A3365         MOVE 0
A3365           TO WREC-RENDTO-NET
      *
A3365      END-IF.
      *
           IF WTGA-CPT-MIN-SCAD-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-CPT-MIN-SCAD(IX-TAB-TGA)
                TO WREC-CPT-GARTO-A-SCAD
      *
           ELSE
      *
              MOVE 0
                TO WREC-CPT-GARTO-A-SCAD
      *
           END-IF.
      *
           IF WTGA-PRE-CASO-MOR-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-PRE-CASO-MOR(IX-TAB-TGA)
                TO WREC-PRE-CASO-MOR
      *
           ELSE
      *
              MOVE 0
                TO WREC-PRE-CASO-MOR
      *
           END-IF.
      *
           IF WTGA-ABB-ANNU-ULT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-ABB-ANNU-ULT(IX-TAB-TGA)
                TO WREC-ABB-ANNU-ULT
      *
           ELSE
      *
              MOVE 0
                TO WREC-ABB-ANNU-ULT
      *
           END-IF.
      *
      *
      *    IF WTGA-COMMIS-GEST-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
23209      IF WTGA-PC-COMMIS-GEST-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
      *       MOVE WTGA-COMMIS-GEST(IX-TAB-TGA)
23209 *       MOVE WTGA-PC-COMMIS-GEST(IX-TAB-TGA)
      *         TO WREC-COMM-GEST
31397         COMPUTE WREC-COMM-GEST  ROUNDED =
31397             WTGA-PC-COMMIS-GEST(IX-TAB-TGA)
           ELSE
      *
              MOVE 0
                TO WREC-COMM-GEST
      *
           END-IF.
      *
           IF WTGA-INCR-PRSTZ-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-INCR-PRSTZ(IX-TAB-TGA)
                TO WREC-INCR-PRSTZ
      *
           ELSE
      *
              MOVE 0
                TO WREC-INCR-PRSTZ
      *
           END-IF.
      *
           IF WTGA-INCR-PRE-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-INCR-PRE(IX-TAB-TGA)
                TO WREC-INCR-PRE
      *
           ELSE
      *
              MOVE 0
                TO WREC-INCR-PRE
      *
           END-IF.
      *
           IF WTGA-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-PRSTZ-AGG-ULT(IX-TAB-TGA)
                TO WREC-PRSTZ-AGG-ULT
      *
           ELSE
      *
              MOVE 0
                TO WREC-PRSTZ-AGG-ULT
      *
           END-IF.
      *
           IF WTGA-MANFEE-RICOR-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-MANFEE-RICOR(IX-TAB-TGA)
                TO WREC-MANFEE-RICOR
      *
           ELSE
      *
              MOVE 0
                TO WREC-MANFEE-RICOR
      *
           END-IF.

           IF WTGA-NUM-GG-RIVAL-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
      *
              MOVE WTGA-NUM-GG-RIVAL(IX-TAB-TGA)
                TO WREC-NUM-GG-RIVAL
      *
           ELSE
      *
              MOVE 0
                TO WREC-NUM-GG-RIVAL
      *
           END-IF.

           IF WTGA-INTR-MORA-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE ZERO
                TO WREC-INT-MORA
           ELSE
              MOVE WTGA-INTR-MORA(IX-TAB-TGA)
                TO WREC-INT-MORA
           END-IF.

           MOVE ZEROES              TO WREC-DT-VLT-TIT

      *    SIR CQPrd00014405: LA DATA VALUTA DEVE ESSERE VALORIZZATA
      *    INDIPENDENTEMENTE DAL VALORE ASSUNTO DAL CAMPO TP_MOD_RIVAL
      *    AND WK-ORIG-RIV-IN
           IF COLLETTIVA

              IF WK-ORIG-RIV-IN
      *
                 PERFORM VARYING IX-W670 FROM 1 BY 1
                           UNTIL IX-W670 > W670-NUM-MAX-ELE
      *
                   IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
                      W670-ID-TRCH-DI-GAR(IX-W670)
      *
                      IF W670-DT-VLT-NULL(IX-W670) NOT = HIGH-VALUES
      *
                         MOVE W670-DT-VLT(IX-W670)
                           TO WREC-DT-VLT-TIT
      *
                      END-IF
      *
                   END-IF
      *
                 END-PERFORM

              ELSE

                 PERFORM S10980-RICERCA-TITOLO
                    THRU S10980-RICERCA-TITOLO-EX

      *BNL2288
                 IF IDSV0001-ESITO-OK AND WK-TIT-SI

                    IF TIT-DT-VLT-NULL NOT = HIGH-VALUES
      *
                       MOVE TIT-DT-VLT
                         TO WREC-DT-VLT-TIT
      *
                    END-IF

                 END-IF

              END-IF

           ELSE
              IF WK-ORIG-RIV-IN

                 PERFORM VARYING IX-TGA-W870 FROM 1 BY 1
                           UNTIL IX-TGA-W870 > W870-TGA-NUM-MAX-ELE
                     IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
                        W870-ID-TRCH-DI-GAR(IX-TGA-W870)

                         PERFORM RICERCA-GAR
                            THRU RICERCA-GAR-EX

                         IF  WK-GRZ-SI
                         AND (ISPV0000-UNICO
                          OR ISPV0000-UNICO-RICORRENTE)
                            IF W870-DT-VLT-NULL(IX-TGA-W870, 1)
                               NOT = HIGH-VALUES

                               MOVE W870-DT-VLT(IX-TGA-W870, 1)
                                 TO WREC-DT-VLT-TIT

                            END-IF

                         END-IF

                     END-IF

                 END-PERFORM

21901         ELSE
  ..  * data valuta deve essere valorizzata anche per emesso individ.
  ..             PERFORM RICERCA-GAR
  ..                THRU RICERCA-GAR-EX
  ..  *
  ..             IF  WK-GRZ-SI
  ..             AND (ISPV0000-UNICO
  ..              OR ISPV0000-UNICO-RICORRENTE)
  ..                 PERFORM S10980-RICERCA-TITOLO
  ..                    THRU S10980-RICERCA-TITOLO-EX
  ..  *
      *BNL2288
                     IF IDSV0001-ESITO-OK AND WK-TIT-SI
  ..  *
  ..                    IF TIT-DT-VLT-NULL NOT = HIGH-VALUES
  ..  *
  ..                       MOVE TIT-DT-VLT
  ..                         TO WREC-DT-VLT-TIT
  ..  *
  ..                    END-IF
  ..  *
  ..                 END-IF
  ..  *
21901            END-IF
      *
              END-IF

           END-IF.
      *
           MOVE ZERO TO WREC-NUM-GG-RIT-PAG

           IF WK-ORIG-RIV-IN
              IF IDSV0001-ESITO-OK
                 PERFORM S10990-LEGGE-GG-RIT-PAG
                    THRU S10990-EX
                 IF  IDSV0001-ESITO-OK
                 AND DETTAGLIO-TROVATO
                     MOVE DTC-NUM-GG-RITARDO-PAG
                       TO WREC-NUM-GG-RIT-PAG
                 END-IF
              END-IF
           END-IF.

12700      INSPECT W-REC-OUTRIVA REPLACING ALL LOW-VALUE BY SPACES.
12700      INSPECT W-REC-OUTRIVA REPLACING ALL HIGH-VALUE BY SPACES.

           WRITE OUTRIVA-REC FROM W-REC-OUTRIVA.
      *
           IF FS-OUT NOT = '00'
      *
              MOVE 'S10970-VALORIZZA-REC-DET'
                TO WK-LABEL-ERR
              MOVE 'ERRORE WRITE FILE DI OUTPUT'
                TO WK-STRING
              MOVE '001114'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S10970-VALORIZZA-REC-DET-EX.
           EXIT.


       S10990-LEGGE-GG-RIT-PAG.
      *
           MOVE 'S10990-LEGGE-GG-RIT-PAG'
             TO WK-LABEL-ERR.

           SET IDSI0011-FETCH-FIRST       TO TRUE.

           SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.

           SET INIZ-CUR-DTC               TO TRUE.

           SET IDSI0011-TRATT-DEFAULT  TO TRUE.

           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) TO DTC-ID-OGG.

           SET TRANCHE                 TO TRUE.
           MOVE WS-TP-OGG              TO DTC-TP-OGG.

           SET IDSI0011-ID-OGGETTO     TO TRUE.

           MOVE 'DETT-TIT-CONT'        TO IDSI0011-CODICE-STR-DATO.
           MOVE DETT-TIT-CONT          TO IDSI0011-BUFFER-DATI.

           MOVE ZERO TO IDSI0011-DATA-INIZIO-EFFETTO
                        IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO TO IDSI0011-DATA-COMPETENZA.

           SET DETTAGLIO-NON-TROVATO TO TRUE.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR IDSV0001-ESITO-KO
                      OR FINE-CUR-DTC
                      OR DETTAGLIO-TROVATO

                   PERFORM CALL-DISPATCHER
                      THRU CALL-DISPATCHER-EX

                   IF IDSV0001-ESITO-OK
                      IF IDSO0011-SUCCESSFUL-RC
                         EVALUATE TRUE
                                  WHEN IDSO0011-NOT-FOUND
      *-->    NESSUN DATO ESTRATTO DALLA TABELLA
                                      SET FINE-CUR-DTC TO TRUE


                         WHEN IDSO0011-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                             MOVE IDSO0011-BUFFER-DATI TO DETT-TIT-CONT
      *                      IF DTC-ID-TIT-CONT = TIT-ID-TIT-CONT
      *                         SET DETTAGLIO-TROVATO TO TRUE
      *                      END-IF
      *
      * N.B.
      *       ANCHE A FRONTE DI UNA GARANZIA ANNUALE, PER LA QUALE
      *       E' PLAUSIBILE LA PRESENZA DI PIU' TITOLI,
      *       VIENE SEMPRE CONSIDERATO IL PRIMO DETTAGLIO TITOLO
      *       DISPONIBILE

                             SET DETTAGLIO-TROVATO TO TRUE
                             PERFORM S10991-CHIUDI-CURSORE
                                THRU S10991-EX

                         WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                             MOVE 'IDBSDTC0'
                               TO IEAI9901-COD-SERVIZIO-BE
                             MOVE 'S10990-LEGGE-GG-RIT-PAG'
                               TO IEAI9901-LABEL-ERR
                             MOVE '005166'
                               TO IEAI9901-COD-ERRORE
                             MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                             STRING IDSI0011-CODICE-STR-DATO ';'
                                    IDSO0011-RETURN-CODE     ';'
                                    IDSO0011-SQLCODE
                                    DELIMITED BY SIZE
                                    INTO IEAI9901-PARAMETRI-ERR
                             END-STRING
                             PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                THRU EX-S0300
                         END-EVALUATE
                      ELSE
      *--> GESTIRE ERRORE
                           MOVE 'IDBSDTC0'
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'S10990-LEGGE-GG-RIT-PAG'
                             TO IEAI9901-LABEL-ERR
                           MOVE '005166'
                             TO IEAI9901-COD-ERRORE
                           MOVE SPACES
                             TO IEAI9901-PARAMETRI-ERR
                           STRING IDSI0011-CODICE-STR-DATO ';'
                                  IDSO0011-RETURN-CODE     ';'
                                  IDSO0011-SQLCODE
                                  DELIMITED BY SIZE
                                  INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                      END-IF
                   ELSE
      *--> GESTIRE ERRORE
                       MOVE 'IDBSDTC0'
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S10990-LEGGE-GG-RIT-PAG'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005166'
                         TO IEAI9901-COD-ERRORE
                       MOVE SPACES
                         TO IEAI9901-PARAMETRI-ERR
                       STRING IDSI0011-CODICE-STR-DATO ';'
                              IDSO0011-RETURN-CODE     ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
                   END-IF
           END-PERFORM.
      *
       S10990-EX.
           EXIT.


       S10991-CHIUDI-CURSORE.
      *
           MOVE 'S10991-CHIUDI-CURSORE'
             TO WK-LABEL-ERR.

           SET IDSI0011-CLOSE-CURSOR  TO TRUE.
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSV0001-ESITO-OK
              IF IDSO0011-SUCCESSFUL-RC
                         EVALUATE TRUE
                         WHEN IDSO0011-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                              CONTINUE

                         WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                             MOVE 'IDBSDTC0'
                               TO IEAI9901-COD-SERVIZIO-BE
                             MOVE 'S10991-CHIUDI-CURSORE'
                               TO IEAI9901-LABEL-ERR
                             MOVE '005166'
                               TO IEAI9901-COD-ERRORE
                             MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                             STRING IDSI0011-CODICE-STR-DATO ';'
                                    IDSO0011-RETURN-CODE     ';'
                                    IDSO0011-SQLCODE
                                    DELIMITED BY SIZE
                                    INTO IEAI9901-PARAMETRI-ERR
                             END-STRING
                             PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                THRU EX-S0300
                         END-EVALUATE
              ELSE
      *--> GESTIRE ERRORE
                   MOVE 'IDBSDTC0'
                     TO IEAI9901-COD-SERVIZIO-BE
                   MOVE 'S10991-CHIUDI-CURSORE'
                     TO IEAI9901-LABEL-ERR
                   MOVE '005166'
                     TO IEAI9901-COD-ERRORE
                   MOVE SPACES
                     TO IEAI9901-PARAMETRI-ERR
                   STRING IDSI0011-CODICE-STR-DATO ';'
                          IDSO0011-RETURN-CODE     ';'
                          IDSO0011-SQLCODE
                          DELIMITED BY SIZE
                          INTO IEAI9901-PARAMETRI-ERR
                   END-STRING
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
              END-IF

           ELSE
      *--> GESTIRE ERRORE
              MOVE 'IDBSDTC0'
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S10991-CHIUDI-CURSORE'
                TO IEAI9901-LABEL-ERR
              MOVE '005166'
                TO IEAI9901-COD-ERRORE
              MOVE SPACES
                TO IEAI9901-PARAMETRI-ERR
              STRING IDSI0011-CODICE-STR-DATO ';'
                     IDSO0011-RETURN-CODE     ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

      *
       S10991-EX.
            EXIT.

      *----------------------------------------------------------------*
      *                     RICERCA TITOLI CONTABILI                   *
      *----------------------------------------------------------------*
      *
       S10980-RICERCA-TITOLO.

      * -->> Indicatore di fine fetch
      *
           SET WK-FINE-FETCH-NO
             TO TRUE.
      *
      * -->> Flag Ricerca Titolo Contabile Incassato
      *
           SET WK-TIT-NO
             TO TRUE.
      *
13148      INITIALIZE LDBVD601.

           MOVE 'IN'
13148        TO LDBVD601-TP-STA-TIT.
      *
           SET IDSI0011-FETCH-FIRST
             TO TRUE.
      *
           PERFORM S10990-IMPOSTA-TIT-CONT
              THRU S10990-IMPOSTA-TIT-CONT-EX
      *
           PERFORM S11000-LEGGI-TIT-CONT
              THRU S11000-LEGGI-TIT-CONT-EX
             UNTIL WK-FINE-FETCH-SI
                OR IDSV0001-ESITO-KO
      *
      * --> Non esistono Titoli Contabili con stato INCASSATO
      *
           IF IDSV0001-ESITO-OK
      *
              IF WK-TIT-NO
      *
                 SET WK-FINE-FETCH-NO
                   TO TRUE
      *
                 MOVE 'EM'
13148              TO LDBVD601-TP-STA-TIT
      *
                 SET IDSI0011-FETCH-FIRST
                   TO TRUE
      *
                 PERFORM S10990-IMPOSTA-TIT-CONT
                    THRU S10990-IMPOSTA-TIT-CONT-EX
      *
                 PERFORM S11000-LEGGI-TIT-CONT
                    THRU S11000-LEGGI-TIT-CONT-EX
                   UNTIL WK-FINE-FETCH-SI
                      OR IDSV0001-ESITO-KO
      *
           END-IF.
      *
       S10980-RICERCA-TITOLO-EX.
             EXIT.

      *----------------------------------------------------------------*
      *     VALORIZZAZIONI PER LA LETTURA CON WHERE CONDITION AD HOC   *
      *----------------------------------------------------------------*
      *
       S10990-IMPOSTA-TIT-CONT.
      *
           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
13148        TO LDBVD601-ID-OGG.
           MOVE 'TG'
13148        TO LDBVD601-TP-OGG.
           MOVE 'PR'
13148        TO LDBVD601-TP-TIT.

VSTEF *    SIR CQPrd00020801: LA DATA INCASSO NON VIENE VALORIZZATA
      *    NEL FILE DI OUTPUT

VSTEF      MOVE WS-DT-INFINITO-1-N
             TO IDSI0011-DATA-INIZIO-EFFETTO
VSTEF      MOVE WS-TS-INFINITO-1-N
             TO IDSI0011-DATA-COMPETENZA
VSTEF      MOVE ZEROES
             TO IDSI0011-DATA-FINE-EFFETTO
      *
      *  --> Nome tabella fisica db
      *
13148      MOVE 'LDBSD600'
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE SPACES
             TO IDSI0011-BUFFER-DATI.
13148      MOVE LDBVD601
             TO IDSI0011-BUFFER-WHERE-COND.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-WHERE-CONDITION
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSI0011-TRATT-DEFAULT
             TO TRUE.
      *
      *  --> Inizializzazione Return Code
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S10990-IMPOSTA-TIT-CONT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *            LETTURA DELLE TABELLA TRANCHE DI GARANZIA           *
      *----------------------------------------------------------------*
      *
       S11000-LEGGI-TIT-CONT.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata, fine occorrenze fetch
      *
                       SET WK-FINE-FETCH-SI
                        TO TRUE
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
      *  --> Operazione eseguita correttamente
      *
                       SET WK-TIT-SI
                        TO TRUE
      *
                       SET IDSI0011-FETCH-NEXT
                        TO TRUE
      *
                       MOVE IDSO0011-BUFFER-DATI
                         TO TIT-CONT
      *
                  WHEN OTHER
      *
      *  --> Errore di accesso al db

                       MOVE 'S11000-LEGGI-TIT-CONT'
                         TO WK-LABEL-ERR
                       MOVE 'ERRORE LETTURA TITOLO CONTABILE'
                         TO WK-STRING
                       MOVE '005016'
                         TO WK-COD-ERR
                       PERFORM GESTIONE-ERR-STD
                          THRU GESTIONE-ERR-STD-EX
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore dispatcher
      *
              MOVE 'S11000-LEGGI-TIT-CONT'
                TO WK-LABEL-ERR
              MOVE 'ERRORE LETTURA TITOLO CONTABILE'
                TO WK-STRING
              MOVE '005016'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S11000-LEGGI-TIT-CONT-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                 GESTIONE DEL FILE DI OUTPUT   - MANAG. FEE -   *
      *----------------------------------------------------------------*
      *
       S11000-GESTIONE-FILE-MF.
      *
           MOVE 'S11000-GESTIONE-FILE-MF'
             TO WK-LABEL-ERR.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LOAS0820'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Gestione file MF'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           CALL LOAS0820  USING AREA-IDSV0001
                                WCOM-IO-STATI
                                WPMO-AREA-PARAM-MOVI
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TRANCHE
                                WAPPL-NUM-ELAB
      *
            ON EXCEPTION
      *
                 MOVE 'LOAS0820'
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'ERRORE CALL MODULO LOAS0820'
                   TO CALL-DESC
                 MOVE 'S11000-GESTIONE-FILE-MF'
                   TO WK-LABEL-ERR
      *
                 PERFORM GESTIONE-ERR-SIST
                    THRU GESTIONE-ERR-SIST-EX
      *
            END-CALL.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LOAS0820'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Gestione file MF'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
       S11000-GESTIONE-FILE-MF-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *    GESTIONE CHIAMATA ALLA ROUTINE DEL TRANSITORIO
      *----------------------------------------------------------------*
29401 *S12000-GEST-TRANSITORIO.
29401 *
29401 *    IF IDSV0001-ESITO-OK
29401 *--     SERVIZIO SCRITTURA TRANSITORIO PER GESTIONE RENDITE
29401 *       IF SI-GAR-REND
29401 *          PERFORM S12100-PREPARA-LRES0002
29401 *             THRU S12100-PREPARA-LRES0002-EX
29401 *          PERFORM S12200-CALL-LRES0002
29401 *             THRU S12200-CALL-LRES0002-EX
29401 *      END-IF
29401 *    END-IF.
29401 *
29401 *S12000-GEST-TRANSITORIO-EX.
29401 *    EXIT.
      *----------------------------------------------------------------*
      *  PREPARA AREA SERVIZIO SCRITTURA TRANSITORIO GESTIONE RENDITE
      *----------------------------------------------------------------*
29401 *S12100-PREPARA-LRES0002.
29401 *
29401 *    INITIALIZE                        WRAN-AREA-RAPP-ANAG
29401 *                                      AREA-IO-LREC0003.
29401 *
13573 *    SET LREC0003-FUNZ-VAR-ANAGRAFE    TO TRUE.
13573 *    SET LREC0003-FUNZ-RIVALUTAZIONE   TO TRUE.
29401 *    SET LREC0003-FUNZ-RIVALUTAZIONE   TO TRUE.
29401 *S12100-PREPARA-LRES0002-EX.
29401 *    EXIT.
      *----------------------------------------------------------------*
      *  CALL AL SERVIZIO SCRITTURA TRANSITORIO GESTIONE RENDITE
      *----------------------------------------------------------------*
29401 * S12200-CALL-LRES0002.
29401 *
29401 *     MOVE 'LRES0002'                   TO WK-PGM-CALL.
29401 *
29401 *     CALL WK-PGM-CALL USING AREA-IDSV0001
29401 *                            WCOM-AREA-STATI
29401 *                            WMOV-AREA-MOVIMENTO
29401 *                            WPOL-AREA-POLIZZA
29401 *                            WADE-AREA-ADESIONE
29401 *                            WGRZ-AREA-GARANZIA
29401 *                            WRAN-AREA-RAPP-ANAG
29401 *                            AREA-IO-LREC0003
29401 *     ON EXCEPTION
29401 *           MOVE WK-PGM
29401 *             TO IEAI9901-COD-SERVIZIO-BE
29401 *           MOVE 'CALL SERVIZIO SCRITTURA TRANSITORIO'
29401 *             TO CALL-DESC
29401 *           MOVE 'S12200-CALL-LRES0002'
29401 *             TO IEAI9901-LABEL-ERR
29401 *           PERFORM S0290-ERRORE-DI-SISTEMA
29401 *              THRU EX-S0290
29401 *     END-CALL.
29401 *
29401 *
29401 *
29401 * S12200-CALL-LRES0002-EX.
29401 *     EXIT.

      * ============================================================== *
      * -->         O P E R A Z I O N I   F I N A L I              <-- *
      * ============================================================== *
      *
       S90000-OPERAZ-FINALI.
           MOVE 'S90000-OPERAZ-FINALI'  TO WK-LABEL-ERR.
DBG        PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX.
      *
21901 *    MOVE VPMO-AREA-PARAM-MOVI
      *      TO WPMO-AREA-PARAM-MOVI.
      *
      *    IF WCOM-WRITE-NIENTE
      *
      *       SET IDSV0001-ESITO-OK
      *         TO TRUE
      *
      *    END-IF.
      *
           IF IDSV0001-ESITO-KO
      *
              SET WCOM-WRITE-ERR
                TO TRUE
      *
              IF WPOL-TP-FRM-ASSVA = 'IN'
      *
                 SET WCOM-POLIZZA-MBS
                   TO TRUE
                 SET WCOM-POLIZZA-L11
                   TO TRUE
      *
              ELSE
      *
                 SET WCOM-ADESIONE-MBS
                   TO TRUE
                 SET WCOM-ADESIONE-L11
                   TO TRUE
      *
              END-IF
      *
              SET WMOV-ST-ADD
               TO TRUE
      *
22584         INITIALIZE  WPMO-AREA-PARAM-MOVI
  =           PERFORM
  =              VARYING IX-VPMO FROM 1 BY 1
  =                UNTIL IX-VPMO GREATER VPMO-ELE-PARAM-MOV-MAX
  =                  IF NOT VPMO-ST-INV(IX-VPMO)
  =                     MOVE VPMO-TAB-PARAM-MOV(IX-VPMO)
  =                       TO WPMO-TAB-PARAM-MOV(IX-VPMO)
  =                  END-IF
  =           END-PERFORM
22584         MOVE VPMO-ELE-PARAM-MOV-MAX  TO WPMO-ELE-PARAM-MOV-MAX
      *
           END-IF.
      *
       S90000-OPERAZ-FINALI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *               CHIUSURA DEL FILE DI OUTPUT OUTRIVA              *
      *----------------------------------------------------------------*
      *
       S90200-CLOSE-OUT.
      *
           CLOSE OUTRIVA
      *
           IF FS-OUT NOT = '00'
      * changes SIR FCTVI00011410 starts here
              MOVE FS-OUT TO WS-FS-OUT
      * changes SIR FCTVI00011410 ends here
              MOVE 'S90200-CLOSE-OUT' TO WK-LABEL-ERR
      * changes SIR FCTVI00011410 starts here
              STRING 'ERRORE CLOSE FILE DI OUTPUT' ';'  WS-FS-OUT
                     DELIMITED BY SIZE INTO WK-STRING
              END-STRING
      * changes SIR FCTVI00011410 ends here
              MOVE '001114' TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S90200-CLOSE-OUT-EX.
           EXIT.
      *
VSTEF *----------------------------------------------------------------*
  ..  *    RICERCA GARANZIA
  ..  *----------------------------------------------------------------*
  ..   RICERCA-GAR.

  ..       SET WK-GRZ-NO       TO TRUE

  ..       PERFORM VARYING IX-GRZ FROM 1 BY 1
  ..                 UNTIL IX-GRZ > WGRZ-ELE-GAR-MAX
  ..                    OR WK-GRZ-SI

  ..          IF WGRZ-ID-GAR(IX-GRZ) = WTGA-ID-GAR(IX-TAB-TGA)

  ..             SET WK-GRZ-SI               TO TRUE

  ..             MOVE WGRZ-TP-PER-PRE(IX-GRZ)
  ..               TO ISPV0000-PERIODO-PREM

  ..          END-IF

  ..       END-PERFORM.

  ..   RICERCA-GAR-EX.
VSTEF        EXIT.
      *
      *----------------------------------------------------------------*
      *                        DISPLAY FINALI                          *
      *----------------------------------------------------------------*
      *
       S90100-DISPLAY-FINE.
      *
      * --> SEGNALAZIONE DI FINE BATCH
      *
           ACCEPT WK-DATA-AMG          FROM DATE YYYYMMDD.
           ACCEPT WK-ORA-HMS           FROM TIME.
           MOVE CORR WK-DATA-AMG       TO WK-DATA-SISTEMA.
           MOVE CORR WK-ORA-HMS        TO WK-ORA-SISTEMA.
      *
       S90100-DISPLAY-FINE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                GESTIONE STANDARD DELL'ERRORE                   *
      *----------------------------------------------------------------*
      *
       GESTIONE-ERR-STD.
      *
           MOVE WK-PGM
             TO IEAI9901-COD-SERVIZIO-BE.
           MOVE WK-LABEL-ERR
             TO IEAI9901-LABEL-ERR.
           MOVE WK-COD-ERR
             TO IEAI9901-COD-ERRORE.
           STRING WK-STRING ';'
                  IDSO0011-RETURN-CODE ';'
                  IDSO0011-SQLCODE
                  DELIMITED BY SIZE
                  INTO IEAI9901-PARAMETRI-ERR
           END-STRING.
      *
           PERFORM S0300-RICERCA-GRAVITA-ERRORE
              THRU EX-S0300.
      *
       GESTIONE-ERR-STD-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *           GESTIONE STANDARD DELL'ERRORE DI SISTEMA             *
      *----------------------------------------------------------------*
      *
       GESTIONE-ERR-SIST.
      *
           MOVE WK-LABEL-ERR
             TO IEAI9901-LABEL-ERR
      *
           PERFORM S0290-ERRORE-DI-SISTEMA
              THRU EX-S0290.
      *
       GESTIONE-ERR-SIST-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     COPY VALORIZZA OUTPUT TABELLE                              *
      *----------------------------------------------------------------*
      *  --> MOVIMENTO
ALPO        COPY LCCVMOV3               REPLACING ==(SF)== BY ==WMOV==.
      *  --> POLIZZA
            COPY LCCVPOL3               REPLACING ==(SF)== BY ==WPOL==.
      *  --> ADESIONE
            COPY LCCVADE3               REPLACING ==(SF)== BY ==WADE==.
      *  --> GARANZIA
            COPY LCCVGRZ3               REPLACING ==(SF)== BY ==WGRZ==.
      *  --> TRANCHE DI GARANZIA
            COPY LCCVTGA3               REPLACING ==(SF)== BY ==WTGA==.
      *
      *----------------------------------------------------------------*
      *    COPY INIZIALIZZAZIONE TABELLE
      *----------------------------------------------------------------*
      *
      * --> POLIZZA
            COPY LCCVPOL4               REPLACING ==(SF)== BY ==WPOL==.
      * --> ADESIONE
            COPY LCCVADE4               REPLACING ==(SF)== BY ==WADE==.
      *  --> MOVIMENTO
            COPY LCCVMOV4               REPLACING ==(SF)== BY ==WMOV==.
      * --> TRANCHE DI GARANZIA
            COPY LCCVTGA4               REPLACING ==(SF)== BY ==WTGA==.
      * --> GARANZIA
            COPY LCCVGRZ4               REPLACING ==(SF)== BY ==WGRZ==.
      *
      *----------------------------------------------------------------*
      *     ROUTINES CALL DISPATCHER                                   *
      *----------------------------------------------------------------*
      *
           COPY IDSP0012.
      *
      *----------------------------------------------------------------*
      *  ROUTINES GESTIONE ERRORI                                      *
      *----------------------------------------------------------------*
      *
           COPY IERP9901.
           COPY IERP9902.
           COPY IERP9903.
      *
      *  ---------------------------------------------------------------
      *  ROUTINES CALL AL VALORIZZATORE VARIABILI
      *  ---------------------------------------------------------------
      *
           COPY IVVP0210              REPLACING ==(SF)== BY == S211==.
      *
      *  ---------------------------------------------------------------
      *     INIZIALIZZAZIONE CAMPI ERRORE DEROGA
      *  ---------------------------------------------------------------
      *
           COPY LOAP0005              REPLACING ==(SF)== BY ==WCOM==.
      *
           COPY LOAP0001.
      *
      ******************************************************************
      ******************************************************************
      *-----------------------------------------------------------------
      *-----------------------------------------------------------------
      *                   ROUTINE PER DEBUG
      *-----------------------------------------------------------------
      *-----------------------------------------------------------------
      ******************************************************************
      ******************************************************************
DBG    DISPLAY-TIME.
            IF IDSV0001-DEBUG-ESASPERATO
      *
      * --> SEGNALAZIONE DI INIZIO BATCH
      *
DBG   *     DISPLAY '**************************************************'
DBG         ACCEPT WK-ORA-HMS
DBG          FROM TIME
DBG         MOVE CORR WK-DATA-AMG
DBG          TO WK-DATA-SISTEMA
DBG         MOVE CORR WK-ORA-HMS
DBG          TO WK-ORA-SISTEMA
      *
DBG   *     DISPLAY '* ORA  ELABORAZIONE = ' WK-ORA-SISTEMA
DBG   *     DISPLAY '**************************************************'
DBG   *     DISPLAY ALL SPACES
           END-IF.

DBG    DISPLAY-TIME-EX.
DBG         EXIT.
      *-----------------------------------------------------------------
DBG    DISPLAY-LABEL.
DBG         IF IDSV0001-DEBUG-BASSO OR
DBG            IDSV0001-DEBUG-ESASPERATO
DBG   *        DISPLAY WK-LABEL-ERR
DBG            CONTINUE
DBG         END-IF.
DBG    DISPLAY-LABEL-EX.
DBG         EXIT.
      *----------------------------------------------------------------
      *
DBG    DISPLAY-PARAMETRI.
              IF IDSV0001-DEBUG-ESASPERATO OR
                 IDSV0001-DEBUG-BASSO OR
                 IDSV0001-DEBUG-MEDIO

DBG   *        DISPLAY 'ID-POLIZZA       : ' WPMO-ID-POLI(1)
DBG   *        DISPLAY 'ID-ADESIONE      : ' WPMO-ID-ADES(1)
DBG            MOVE WPMO-DT-RICOR-SUCC(1)  TO WK-DATA
DBG   *        DISPLAY 'DT-RICORRENZA-SUC: ' WK-DATA
              END-IF.

DBG    DISPLAY-PARAMETRI-EX.
            EXIT.
      *
DBG    DISPLAY-IB-OGG.

               IF IDSV0001-DEBUG-ESASPERATO OR
                  IDSV0001-DEBUG-MEDIO
DBG                MOVE WADE-IB-OGG(1)  TO WK-LABEL-ERR
DBG                PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
DBG                MOVE WPOL-IB-OGG     TO WK-LABEL-ERR
DBG                PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX
DBG            END-IF.

DBG    DISPLAY-IB-OGG-EX.
DBG            EXIT.

PERF  *----------------------------------------------------------------*
PERF  *     VERIFICA PRESENZA PRODOTTO IN AREA DI CACHE                *
PERF  *----------------------------------------------------------------*
PERF   VERIFICA-PROD.

PERF       PERFORM VARYING IX-WK-0040 FROM 1 BY 1
PERF         UNTIL IX-WK-0040 > WK-0040-ELE-MAX
PERF            OR PROD-OK

PERF            IF   WK-0040-COD-COMPAGNIA(IX-WK-0040) =
PERF                 ISPC0040-COD-COMPAGNIA
PERF            AND  WK-0040-COD-PRODOTTO(IX-WK-0040) =
PERF                 ISPC0040-COD-PRODOTTO
PERF            AND  WK-0040-COD-CONVENZIONE(IX-WK-0040) =
PERF                 ISPC0040-COD-CONVENZIONE
PERF            AND  WK-0040-DATA-INIZ-VALID-CONV(IX-WK-0040) =
PERF                 ISPC0040-DATA-INIZ-VALID-CONV
PERF            AND  WK-0040-DATA-RIFERIMENTO(IX-WK-0040) =
PERF                 ISPC0040-DATA-RIFERIMENTO
PERF            AND  WK-0040-LIVELLO-UTENTE(IX-WK-0040) =
PERF                 ISPC0040-LIVELLO-UTENTE
PERF            AND  WK-0040-SESSION-ID(IX-WK-0040) =
PERF                 ISPC0040-SESSION-ID
PERF            AND  WK-0040-FUNZIONALITA(IX-WK-0040) =
PERF                 ISPC0040-FUNZIONALITA
PERF                 SET PROD-OK TO TRUE
PERF                 MOVE WK-0040-DATA-VERSIONE-PROD(IX-WK-0040)
PERF                   TO WCOM-DT-ULT-VERS-PROD
PERF            END-IF
PERF       END-PERFORM.
PERF  *
PERF       IF PROD-KO
PERF          INITIALIZE WS-0040-DATI-INPUT
PERF          MOVE ISPC0040-COD-COMPAGNIA
PERF            TO WS-0040-COD-COMPAGNIA
PERF          MOVE ISPC0040-COD-PRODOTTO
PERF            TO WS-0040-COD-PRODOTTO
PERF          MOVE ISPC0040-COD-CONVENZIONE
PERF            TO WS-0040-COD-CONVENZIONE
PERF          MOVE ISPC0040-DATA-INIZ-VALID-CONV
PERF            TO WS-0040-DATA-INIZ-VALID-CONV
PERF          MOVE ISPC0040-DATA-RIFERIMENTO
PERF            TO WS-0040-DATA-RIFERIMENTO
PERF          MOVE ISPC0040-LIVELLO-UTENTE
PERF           TO WS-0040-LIVELLO-UTENTE
PERF          MOVE ISPC0040-SESSION-ID
PERF            TO WS-0040-SESSION-ID
PERF          MOVE ISPC0040-FUNZIONALITA
PERF            TO WS-0040-FUNZIONALITA
PERF       END-IF.

PERF   VERIFICA-PROD-EX.
PERF       EXIT.
      *
      *----------------------------------------------------------------*
      *    ROUTINE  DI GESTIONE DEROGHE BLOCCANTI
      *----------------------------------------------------------------*
42781      COPY LOAP0002.
      *----------------------------------------------------------------*
      *    ROUTINES DI GESTIONE DISPLAY
      *----------------------------------------------------------------*
           COPY IDSP8888.

      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS3040.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2014.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS3040                                      *
      *  TIPOLOGIA...... SERVIZIO                                      *
      *  PROCESSO....... XXX                                           *
      *  FUNZIONE....... XXX                                           *
      *  DESCRIZIONE.... MODULO CALCOLO NUOVA VARIABILE CUMPREVERS     *
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI                                                    *
      *----------------------------------------------------------------*
       01  WK-PGM                          PIC X(008) VALUE 'LVVS3040'.
       01  WK-PGM-CALLED                   PIC X(008) VALUE 'LDBSF110'.
      *----------------------------------------------------------------*
      *    VARIABILI                                                   *
      *----------------------------------------------------------------*

      *----------------------------------------------------------------*
      *    COPY DI INPUT PER LA CHIAMATA AL LVVS0000                   *
      *----------------------------------------------------------------*
       01  INPUT-LVVS0000.
           COPY LVVC0000        REPLACING ==(SF)== BY ==LVVC0000==.
      *----------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBSF110                                    *
      *----------------------------------------------------------------*
           COPY LDBVF111.
      *----------------------------------------------------------------*
      *--> AREA TRANCHE                                                *
      *----------------------------------------------------------------*
       01 AREA-IO-TGA.
          03 DTGA-AREA-TRANCHE.
             04 DTGA-ELE-TRCH-MAX       PIC S9(04) COMP.
             04 DTGA-TAB-TRCH.
             COPY LCCVTGA1              REPLACING ==(SF)== BY ==DTGA==.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN                                           *
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.
      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI                                                      *
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS3040.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS3040.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           INITIALIZE LDBVF111.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE                                                *
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM A000-PREPARA-CALL-LDBSF110
                  THRU A000-EX
           END-IF
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM A010-CALL-LDBSF110
                  THRU A010-EX
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE            *
      *    RISPETTIVE AREE DCLGEN IN WORKING                           *
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-TRCH-GAR
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DTGA-AREA-TRANCHE
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  PREPARA CHIAMATA AL MODULO LDBSF110                           *
      *----------------------------------------------------------------*
       A000-PREPARA-CALL-LDBSF110.
      *
           MOVE ZEROES                     TO IVVC0213-VAL-IMP-O.
      *
           INITIALIZE LDBVF111.
      *
           SET IDSV0003-SELECT             TO TRUE.
           SET IDSV0003-WHERE-CONDITION    TO TRUE.
           SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.

           MOVE DTGA-ID-POLI               TO LDBVF111-ID-OGG.
           MOVE 'EM'                       TO LDBVF111-TP-STAT-TIT1.
           MOVE 'IN'                       TO LDBVF111-TP-STAT-TIT2.
           MOVE SPACES                     TO LDBVF111-TP-STAT-TIT3
                                              LDBVF111-TP-STAT-TIT4
                                              LDBVF111-TP-STAT-TIT5.
           MOVE DTGA-DT-DECOR              TO LDBVF111-DT-DECOR-TRCH.

           MOVE LDBVF111
             TO IDSV0003-BUFFER-WHERE-COND.

       A000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLO NUOVA VARIABILE CUMPREVERS                            *
      *  === SOMMA TOT-PRE-TOT CON DATA INIZIO COPERTURA MINORE ===    *
      *  === DT-DECOR-TRCH E TITOLO IN STATO EMESSO O INCASSATO ===    *
      *----------------------------------------------------------------*
       A010-CALL-LDBSF110.

           CALL WK-PGM-CALLED  USING  IDSV0003 LDBVF111

           ON EXCEPTION
              MOVE WK-PGM-CALLED
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL LDBSF110 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE LDBVF111-CUM-PRE-VERS  TO IVVC0213-VAL-IMP-O
           ELSE
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER     TO TRUE
                 MOVE WK-PGM-CALLED       TO IDSV0003-COD-SERVIZIO-BE

                 STRING 'CHIAMATA LDBSF110 ;'
                        IDSV0003-RETURN-CODE ';'
                        IDSV0003-SQLCODE
                        DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING
              END-IF
           END-IF.

       A010-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

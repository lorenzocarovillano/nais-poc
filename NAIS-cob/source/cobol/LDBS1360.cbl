       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LDBS1360 IS INITIAL.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.  .
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         :
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.

       01 WK-ID-ADES-DA           PIC S9(9)V     COMP-3.
       01 WK-ID-ADES-A            PIC S9(9)V     COMP-3.

       01 WK-ID-GAR-DA            PIC S9(9)V     COMP-3.
       01 WK-ID-GAR-A             PIC S9(9)V     COMP-3.


      *------------------------------------------------------------
      * FLAGS
      *------------------------------------------------------------

       01 FLAG-STATO              PIC X(01).
          88 NOT-STATO            VALUE 'N'.
          88 STATO                VALUE 'S'.

       01 FLAG-CAUSALE            PIC X(01).
          88 NOT-CAUSALE          VALUE 'N'.
          88 CAUSALE              VALUE 'S'.


      *------------------------------------------------------------
      * COSTANTI
      *------------------------------------------------------------

       01 NEGAZIONE               PIC X(03) VALUE 'NOT'.

      *------------------------------------------------------------
      * COPY
      *------------------------------------------------------------

           EXEC SQL INCLUDE IDSV0010 END-EXEC.

           EXEC SQL INCLUDE LDBV1361 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDTGA0 END-EXEC.
           EXEC SQL INCLUDE IDBVTGA2 END-EXEC.
           EXEC SQL INCLUDE IDBVTGA3 END-EXEC.

           EXEC SQL INCLUDE IDBDSTB0 END-EXEC.
           EXEC SQL INCLUDE IDBVSTB1 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVTGA1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 TRCH-DI-GAR.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 IF LDBV1361-PRODOTTO OR LDBV1361-OPZIONE

                    EVALUATE TRUE ALSO TRUE

                      WHEN STATO ALSO CAUSALE
                        PERFORM A401-ELABORA-EFF THRU A401-EX

                      WHEN NOT-STATO ALSO CAUSALE
                        PERFORM C401-ELABORA-EFF-NS THRU C401-EX

                      WHEN STATO ALSO NOT-CAUSALE
                        PERFORM E401-ELABORA-EFF-NC THRU E401-EX

                      WHEN NOT-STATO ALSO NOT-CAUSALE
                        PERFORM G401-ELABORA-EFF-NS-NC THRU G401-EX

                    END-EVALUATE

                 ELSE
                    EVALUATE TRUE ALSO TRUE

                      WHEN STATO ALSO CAUSALE
                        PERFORM A400-ELABORA-EFF       THRU A400-EX

                      WHEN NOT-STATO ALSO CAUSALE
                        PERFORM C400-ELABORA-EFF-NS    THRU C400-EX

                      WHEN STATO ALSO NOT-CAUSALE
                        PERFORM E400-ELABORA-EFF-NC    THRU E400-EX

                      WHEN NOT-STATO ALSO NOT-CAUSALE
                        PERFORM G400-ELABORA-EFF-NS-NC THRU G400-EX

                    END-EVALUATE

                 END-IF

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   IF LDBV1361-PRODOTTO OR LDBV1361-OPZIONE
                      EVALUATE TRUE ALSO TRUE

                        WHEN STATO ALSO CAUSALE
                            PERFORM B401-ELABORA-CPZ       THRU B401-EX

                        WHEN NOT-STATO ALSO CAUSALE
                            PERFORM D401-ELABORA-CPZ-NS    THRU D401-EX

                        WHEN STATO ALSO NOT-CAUSALE
                            PERFORM F401-ELABORA-CPZ-NC    THRU F401-EX

                        WHEN NOT-STATO ALSO NOT-CAUSALE
                            PERFORM H401-ELABORA-CPZ-NS-NC THRU H401-EX

                      END-EVALUATE
                   ELSE
                      EVALUATE TRUE ALSO TRUE

                        WHEN STATO ALSO CAUSALE
                          PERFORM B400-ELABORA-CPZ       THRU B400-EX

                        WHEN NOT-STATO ALSO CAUSALE
                          PERFORM D400-ELABORA-CPZ-NS    THRU D400-EX

                        WHEN STATO ALSO NOT-CAUSALE
                          PERFORM F400-ELABORA-CPZ-NC    THRU F400-EX

                        WHEN NOT-STATO ALSO NOT-CAUSALE
                          PERFORM H400-ELABORA-CPZ-NS-NC THRU H400-EX

                      END-EVALUATE

                   END-IF
                ELSE

                  SET IDSV0003-INVALID-LEVEL-OPER       TO TRUE

               END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'LDBS1360 '   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'TGA_STB'     TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           MOVE 000000000                TO WK-ID-ADES-DA
                                            WK-ID-GAR-DA
           MOVE 999999999                TO WK-ID-ADES-A
                                            WK-ID-GAR-A

           SET STATO                     TO TRUE
           SET CAUSALE                   TO TRUE

           MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV1361

           PERFORM V010-VERIFICA-WHERE-COND   THRU V010-EX

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       V010-VERIFICA-WHERE-COND.


           IF TGA-ID-ADES IS NUMERIC AND
              TGA-ID-ADES NOT = ZERO
              MOVE TGA-ID-ADES      TO WK-ID-ADES-DA
                                       WK-ID-ADES-A
           END-IF.

           IF TGA-ID-GAR IS NUMERIC AND
              TGA-ID-GAR NOT = ZERO
              MOVE TGA-ID-GAR       TO WK-ID-GAR-DA
                                       WK-ID-GAR-A
           END-IF.

           PERFORM V020-VERIF-OPERATORI-LOGICI THRU V020-EX.

       V010-EX.
           EXIT.

       V020-VERIF-OPERATORI-LOGICI.

           IF LDBV1361-OPER-LOG-STAT-BUS = NEGAZIONE
              SET NOT-STATO               TO TRUE
           END-IF


           IF LDBV1361-OPER-LOG-CAUS = NEGAZIONE
              SET NOT-CAUSALE             TO TRUE
           END-IF.

       V020-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.

       A400-ELABORA-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.

       A401-ELABORA-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A461-OPEN-CURSOR-EFF  THRU A461-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A471-CLOSE-CURSOR-EFF THRU A471-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A481-FETCH-FIRST-EFF  THRU A481-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A491-FETCH-NEXT-EFF   THRU A491-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A401-EX.
           EXIT.


       B400-ELABORA-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B401-ELABORA-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B461-OPEN-CURSOR-CPZ  THRU B461-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B471-CLOSE-CURSOR-CPZ THRU B471-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B481-FETCH-FIRST-CPZ  THRU B481-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B491-FETCH-NEXT-CPZ   THRU B491-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B401-EX.
           EXIT.

       C400-ELABORA-EFF-NS.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM C460-OPEN-CURSOR-EFF-NS  THRU C460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM C470-CLOSE-CURSOR-EFF-NS THRU C470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM C480-FETCH-FIRST-EFF-NS  THRU C480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM C490-FETCH-NEXT-EFF-NS   THRU C490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       C400-EX.
           EXIT.

       C401-ELABORA-EFF-NS.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM C461-OPEN-CURSOR-EFF-NS  THRU C461-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM C471-CLOSE-CURSOR-EFF-NS THRU C471-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM C481-FETCH-FIRST-EFF-NS  THRU C481-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM C491-FETCH-NEXT-EFF-NS   THRU C491-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       C401-EX.
           EXIT.


       D400-ELABORA-CPZ-NS.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM D460-OPEN-CURSOR-CPZ-NS  THRU D460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM D470-CLOSE-CURSOR-CPZ-NS THRU D470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM D480-FETCH-FIRST-CPZ-NS  THRU D480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM D490-FETCH-NEXT-CPZ-NS   THRU D490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       D400-EX.
           EXIT.

       D401-ELABORA-CPZ-NS.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM D461-OPEN-CURSOR-CPZ-NS  THRU D461-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM D471-CLOSE-CURSOR-CPZ-NS THRU D471-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM D481-FETCH-FIRST-CPZ-NS  THRU D481-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM D491-FETCH-NEXT-CPZ-NS   THRU D491-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       D401-EX.
           EXIT.

       E400-ELABORA-EFF-NC.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM E460-OPEN-CURSOR-EFF-NC  THRU E460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM E470-CLOSE-CURSOR-EFF-NC THRU E470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM E480-FETCH-FIRST-EFF-NC  THRU E480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM E490-FETCH-NEXT-EFF-NC   THRU E490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       E400-EX.
           EXIT.

       E401-ELABORA-EFF-NC.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM E461-OPEN-CURSOR-EFF-NC  THRU E461-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM E471-CLOSE-CURSOR-EFF-NC THRU E471-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM E481-FETCH-FIRST-EFF-NC  THRU E481-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM E491-FETCH-NEXT-EFF-NC   THRU E491-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       E401-EX.
           EXIT.


       F400-ELABORA-CPZ-NC.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM F460-OPEN-CURSOR-CPZ-NC  THRU F460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM F470-CLOSE-CURSOR-CPZ-NC THRU F470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM F480-FETCH-FIRST-CPZ-NC  THRU F480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM F490-FETCH-NEXT-CPZ-NC   THRU F490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       F400-EX.
           EXIT.

       F401-ELABORA-CPZ-NC.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM F461-OPEN-CURSOR-CPZ-NC  THRU F461-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM F471-CLOSE-CURSOR-CPZ-NC THRU F471-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM F481-FETCH-FIRST-CPZ-NC  THRU F481-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM F491-FETCH-NEXT-CPZ-NC   THRU F491-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       F401-EX.
           EXIT.

       G400-ELABORA-EFF-NS-NC.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM G460-OPEN-CURSOR-EFF-NS-NC  THRU G460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM G470-CLOSE-CURSOR-EFF-NS-NC THRU G470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM G480-FETCH-FIRST-EFF-NS-NC  THRU G480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM G490-FETCH-NEXT-EFF-NS-NC   THRU G490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       G400-EX.
           EXIT.

       G401-ELABORA-EFF-NS-NC.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM G461-OPEN-CURSOR-EFF-NS-NC  THRU G461-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM G471-CLOSE-CURSOR-EFF-NS-NC THRU G471-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM G481-FETCH-FIRST-EFF-NS-NC  THRU G481-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM G491-FETCH-NEXT-EFF-NS-NC   THRU G491-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       G401-EX.
           EXIT.


       H400-ELABORA-CPZ-NS-NC.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM H460-OPEN-CURSOR-CPZ-NS-NC  THRU H460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM H470-CLOSE-CURSOR-CPZ-NS-NC THRU H470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM H480-FETCH-FIRST-CPZ-NS-NC  THRU H480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM H490-FETCH-NEXT-CPZ-NS-NC   THRU H490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       H400-EX.
           EXIT.

       H401-ELABORA-CPZ-NS-NC.
           EVALUATE TRUE
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM H461-OPEN-CURSOR-CPZ-NS-NC  THRU H461-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM H471-CLOSE-CURSOR-CPZ-NS-NC THRU H471-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM H481-FETCH-FIRST-CPZ-NS-NC  THRU H481-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM H491-FETCH-NEXT-CPZ-NS-NC   THRU H491-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       H401-EX.
           EXIT.

      *----
      *----  gestione Effetto STATO / CAUSALE
      *----
       A405-DECLARE-CURSOR-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-EFF-TGA CURSOR FOR
              SELECT
                 A.ID_TRCH_DI_GAR
                ,A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.IB_OGG
                ,A.TP_RGM_FISC
                ,A.DT_EMIS
                ,A.TP_TRCH
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.PRE_CASO_MOR
                ,A.PC_INTR_RIAT
                ,A.IMP_BNS_ANTIC
                ,A.PRE_INI_NET
                ,A.PRE_PP_INI
                ,A.PRE_PP_ULT
                ,A.PRE_TARI_INI
                ,A.PRE_TARI_ULT
                ,A.PRE_INVRIO_INI
                ,A.PRE_INVRIO_ULT
                ,A.PRE_RIVTO
                ,A.IMP_SOPR_PROF
                ,A.IMP_SOPR_SAN
                ,A.IMP_SOPR_SPO
                ,A.IMP_SOPR_TEC
                ,A.IMP_ALT_SOPR
                ,A.PRE_STAB
                ,A.DT_EFF_STAB
                ,A.TS_RIVAL_FIS
                ,A.TS_RIVAL_INDICIZ
                ,A.OLD_TS_TEC
                ,A.RAT_LRD
                ,A.PRE_LRD
                ,A.PRSTZ_INI
                ,A.PRSTZ_ULT
                ,A.CPT_IN_OPZ_RIVTO
                ,A.PRSTZ_INI_STAB
                ,A.CPT_RSH_MOR
                ,A.PRSTZ_RID_INI
                ,A.FL_CAR_CONT
                ,A.BNS_GIA_LIQTO
                ,A.IMP_BNS
                ,A.COD_DVS
                ,A.PRSTZ_INI_NEWFIS
                ,A.IMP_SCON
                ,A.ALQ_SCON
                ,A.IMP_CAR_ACQ
                ,A.IMP_CAR_INC
                ,A.IMP_CAR_GEST
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.RENDTO_LRD
                ,A.PC_RETR
                ,A.RENDTO_RETR
                ,A.MIN_GARTO
                ,A.MIN_TRNUT
                ,A.PRE_ATT_DI_TRCH
                ,A.MATU_END2000
                ,A.ABB_TOT_INI
                ,A.ABB_TOT_ULT
                ,A.ABB_ANNU_ULT
                ,A.DUR_ABB
                ,A.TP_ADEG_ABB
                ,A.MOD_CALC
                ,A.IMP_AZ
                ,A.IMP_ADER
                ,A.IMP_TFR
                ,A.IMP_VOLO
                ,A.VIS_END2000
                ,A.DT_VLDT_PROD
                ,A.DT_INI_VAL_TAR
                ,A.IMPB_VIS_END2000
                ,A.REN_INI_TS_TEC_0
                ,A.PC_RIP_PRE
                ,A.FL_IMPORTI_FORZ
                ,A.PRSTZ_INI_NFORZ
                ,A.VIS_END2000_NFORZ
                ,A.INTR_MORA
                ,A.MANFEE_ANTIC
                ,A.MANFEE_RICOR
                ,A.PRE_UNI_RIVTO
                ,A.PROV_1AA_ACQ
                ,A.PROV_2AA_ACQ
                ,A.PROV_RICOR
                ,A.PROV_INC
                ,A.ALQ_PROV_ACQ
                ,A.ALQ_PROV_INC
                ,A.ALQ_PROV_RICOR
                ,A.IMPB_PROV_ACQ
                ,A.IMPB_PROV_INC
                ,A.IMPB_PROV_RICOR
                ,A.FL_PROV_FORZ
                ,A.PRSTZ_AGG_INI
                ,A.INCR_PRE
                ,A.INCR_PRSTZ
                ,A.DT_ULT_ADEG_PRE_PR
                ,A.PRSTZ_AGG_ULT
                ,A.TS_RIVAL_NET
                ,A.PRE_PATTUITO
                ,A.TP_RIVAL
                ,A.RIS_MAT
                ,A.CPT_MIN_SCAD
                ,A.COMMIS_GEST
                ,A.TP_MANFEE_APPL
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.PC_COMMIS_GEST
                ,A.NUM_GG_RIVAL
                ,A.IMP_TRASFE
                ,A.IMP_TFR_STRC
                ,A.ACQ_EXP
                ,A.REMUN_ASS
                ,A.COMMIS_INTER
                ,A.ALQ_REMUN_ASS
                ,A.ALQ_COMMIS_INTER
                ,A.IMPB_REMUN_ASS
                ,A.IMPB_COMMIS_INTER
                ,A.COS_RUN_ASSVA
                ,A.COS_RUN_ASSVA_IDC
              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )

                    AND B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.ID_TRCH_DI_GAR ASC
           END-EXEC.
           CONTINUE.
       A405-EX.
           EXIT.

       A460-OPEN-CURSOR-EFF.

           PERFORM A405-DECLARE-CURSOR-EFF THRU A405-EX.

           EXEC SQL
                OPEN C-EFF-TGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-EFF.
           EXEC SQL
                CLOSE C-EFF-TGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-EFF.
           PERFORM A460-OPEN-CURSOR-EFF  THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-EFF.
           EXEC SQL
                FETCH C-EFF-TGA
           INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A470-CLOSE-CURSOR-EFF THRU A470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A490-EX.
           EXIT.
      *----
      *----  gestione WH Effetto STATO / CAUSALE
      *----
       A406-DECLARE-CURSOR-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-EFF-TGA-DI CURSOR FOR

              SELECT DISTINCT(A.DT_VLDT_PROD)

              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )

                    AND B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.DT_VLDT_PROD ASC
           END-EXEC.
           CONTINUE.
       A406-EX.
           EXIT.


       A461-OPEN-CURSOR-EFF.

           PERFORM A406-DECLARE-CURSOR-EFF THRU A406-EX.

           EXEC SQL
                OPEN C-EFF-TGA-DI
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A461-EX.
           EXIT.

       A471-CLOSE-CURSOR-EFF.
           EXEC SQL
                CLOSE C-EFF-TGA-DI
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A471-EX.
           EXIT.

       A481-FETCH-FIRST-EFF.
           PERFORM A461-OPEN-CURSOR-EFF  THRU A461-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A491-FETCH-NEXT-EFF THRU A491-EX
           END-IF.
       A481-EX.
           EXIT.

       A491-FETCH-NEXT-EFF.
           EXEC SQL
                FETCH C-EFF-TGA-DI
             INTO
                  :TGA-DT-VLDT-PROD-DB
                  :IND-TGA-DT-VLDT-PROD
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A471-CLOSE-CURSOR-EFF THRU A471-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A491-EX.
           EXIT.

      *----
      *----  gestione FA Competenza STATO / CAUSALE
      *----
       B405-DECLARE-CURSOR-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-CPZ-TGA CURSOR FOR
              SELECT
                 A.ID_TRCH_DI_GAR
                ,A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.IB_OGG
                ,A.TP_RGM_FISC
                ,A.DT_EMIS
                ,A.TP_TRCH
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.PRE_CASO_MOR
                ,A.PC_INTR_RIAT
                ,A.IMP_BNS_ANTIC
                ,A.PRE_INI_NET
                ,A.PRE_PP_INI
                ,A.PRE_PP_ULT
                ,A.PRE_TARI_INI
                ,A.PRE_TARI_ULT
                ,A.PRE_INVRIO_INI
                ,A.PRE_INVRIO_ULT
                ,A.PRE_RIVTO
                ,A.IMP_SOPR_PROF
                ,A.IMP_SOPR_SAN
                ,A.IMP_SOPR_SPO
                ,A.IMP_SOPR_TEC
                ,A.IMP_ALT_SOPR
                ,A.PRE_STAB
                ,A.DT_EFF_STAB
                ,A.TS_RIVAL_FIS
                ,A.TS_RIVAL_INDICIZ
                ,A.OLD_TS_TEC
                ,A.RAT_LRD
                ,A.PRE_LRD
                ,A.PRSTZ_INI
                ,A.PRSTZ_ULT
                ,A.CPT_IN_OPZ_RIVTO
                ,A.PRSTZ_INI_STAB
                ,A.CPT_RSH_MOR
                ,A.PRSTZ_RID_INI
                ,A.FL_CAR_CONT
                ,A.BNS_GIA_LIQTO
                ,A.IMP_BNS
                ,A.COD_DVS
                ,A.PRSTZ_INI_NEWFIS
                ,A.IMP_SCON
                ,A.ALQ_SCON
                ,A.IMP_CAR_ACQ
                ,A.IMP_CAR_INC
                ,A.IMP_CAR_GEST
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.RENDTO_LRD
                ,A.PC_RETR
                ,A.RENDTO_RETR
                ,A.MIN_GARTO
                ,A.MIN_TRNUT
                ,A.PRE_ATT_DI_TRCH
                ,A.MATU_END2000
                ,A.ABB_TOT_INI
                ,A.ABB_TOT_ULT
                ,A.ABB_ANNU_ULT
                ,A.DUR_ABB
                ,A.TP_ADEG_ABB
                ,A.MOD_CALC
                ,A.IMP_AZ
                ,A.IMP_ADER
                ,A.IMP_TFR
                ,A.IMP_VOLO
                ,A.VIS_END2000
                ,A.DT_VLDT_PROD
                ,A.DT_INI_VAL_TAR
                ,A.IMPB_VIS_END2000
                ,A.REN_INI_TS_TEC_0
                ,A.PC_RIP_PRE
                ,A.FL_IMPORTI_FORZ
                ,A.PRSTZ_INI_NFORZ
                ,A.VIS_END2000_NFORZ
                ,A.INTR_MORA
                ,A.MANFEE_ANTIC
                ,A.MANFEE_RICOR
                ,A.PRE_UNI_RIVTO
                ,A.PROV_1AA_ACQ
                ,A.PROV_2AA_ACQ
                ,A.PROV_RICOR
                ,A.PROV_INC
                ,A.ALQ_PROV_ACQ
                ,A.ALQ_PROV_INC
                ,A.ALQ_PROV_RICOR
                ,A.IMPB_PROV_ACQ
                ,A.IMPB_PROV_INC
                ,A.IMPB_PROV_RICOR
                ,A.FL_PROV_FORZ
                ,A.PRSTZ_AGG_INI
                ,A.INCR_PRE
                ,A.INCR_PRSTZ
                ,A.DT_ULT_ADEG_PRE_PR
                ,A.PRSTZ_AGG_ULT
                ,A.TS_RIVAL_NET
                ,A.PRE_PATTUITO
                ,A.TP_RIVAL
                ,A.RIS_MAT
                ,A.CPT_MIN_SCAD
                ,A.COMMIS_GEST
                ,A.TP_MANFEE_APPL
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.PC_COMMIS_GEST
                ,A.NUM_GG_RIVAL
                ,A.IMP_TRASFE
                ,A.IMP_TFR_STRC
                ,A.ACQ_EXP
                ,A.REMUN_ASS
                ,A.COMMIS_INTER
                ,A.ALQ_REMUN_ASS
                ,A.ALQ_COMMIS_INTER
                ,A.IMPB_REMUN_ASS
                ,A.IMPB_COMMIS_INTER
                ,A.COS_RUN_ASSVA
                ,A.COS_RUN_ASSVA_IDC
              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )

                    AND B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.ID_TRCH_DI_GAR ASC
           END-EXEC.
           CONTINUE.
       B405-EX.
           EXIT.

       B460-OPEN-CURSOR-CPZ.

           PERFORM B405-DECLARE-CURSOR-CPZ THRU B405-EX.

           EXEC SQL
                OPEN C-CPZ-TGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-CPZ.
           EXEC SQL
                CLOSE C-CPZ-TGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-CPZ.
           PERFORM B460-OPEN-CURSOR-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-CPZ.
           EXEC SQL
                FETCH C-CPZ-TGA
           INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B470-CLOSE-CURSOR-CPZ THRU B470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B490-EX.
           EXIT.

      *----
      *----  gestione WH Competenza STATO / CAUSALE
      *----
       B406-DECLARE-CURSOR-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-CPZ-TGA-DI CURSOR FOR
              SELECT DISTINCT(A.DT_VLDT_PROD)
              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )

                    AND B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.DT_VLDT_PROD ASC
           END-EXEC.
           CONTINUE.
       B406-EX.
           EXIT.

       B461-OPEN-CURSOR-CPZ.

           PERFORM B406-DECLARE-CURSOR-CPZ THRU B406-EX.

           EXEC SQL
                OPEN C-CPZ-TGA-DI
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B461-EX.
           EXIT.

       B471-CLOSE-CURSOR-CPZ.
           EXEC SQL
                CLOSE C-CPZ-TGA-DI
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B471-EX.
           EXIT.

       B481-FETCH-FIRST-CPZ.
           PERFORM B461-OPEN-CURSOR-CPZ    THRU B461-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B491-FETCH-NEXT-CPZ THRU B491-EX
           END-IF.
       B481-EX.
           EXIT.

       B491-FETCH-NEXT-CPZ.
           EXEC SQL
                FETCH C-CPZ-TGA-DI
                INTO
                  :TGA-DT-VLDT-PROD-DB
                  :IND-TGA-DT-VLDT-PROD
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B471-CLOSE-CURSOR-CPZ THRU B471-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B491-EX.
           EXIT.

      *----
      *----  gestione Effetto NOT STATO / CAUSALE
      *----
       C405-DECLARE-CURSOR-EFF-NS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-EFF-TGA-NS CURSOR FOR
              SELECT
                 A.ID_TRCH_DI_GAR
                ,A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.IB_OGG
                ,A.TP_RGM_FISC
                ,A.DT_EMIS
                ,A.TP_TRCH
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.PRE_CASO_MOR
                ,A.PC_INTR_RIAT
                ,A.IMP_BNS_ANTIC
                ,A.PRE_INI_NET
                ,A.PRE_PP_INI
                ,A.PRE_PP_ULT
                ,A.PRE_TARI_INI
                ,A.PRE_TARI_ULT
                ,A.PRE_INVRIO_INI
                ,A.PRE_INVRIO_ULT
                ,A.PRE_RIVTO
                ,A.IMP_SOPR_PROF
                ,A.IMP_SOPR_SAN
                ,A.IMP_SOPR_SPO
                ,A.IMP_SOPR_TEC
                ,A.IMP_ALT_SOPR
                ,A.PRE_STAB
                ,A.DT_EFF_STAB
                ,A.TS_RIVAL_FIS
                ,A.TS_RIVAL_INDICIZ
                ,A.OLD_TS_TEC
                ,A.RAT_LRD
                ,A.PRE_LRD
                ,A.PRSTZ_INI
                ,A.PRSTZ_ULT
                ,A.CPT_IN_OPZ_RIVTO
                ,A.PRSTZ_INI_STAB
                ,A.CPT_RSH_MOR
                ,A.PRSTZ_RID_INI
                ,A.FL_CAR_CONT
                ,A.BNS_GIA_LIQTO
                ,A.IMP_BNS
                ,A.COD_DVS
                ,A.PRSTZ_INI_NEWFIS
                ,A.IMP_SCON
                ,A.ALQ_SCON
                ,A.IMP_CAR_ACQ
                ,A.IMP_CAR_INC
                ,A.IMP_CAR_GEST
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.RENDTO_LRD
                ,A.PC_RETR
                ,A.RENDTO_RETR
                ,A.MIN_GARTO
                ,A.MIN_TRNUT
                ,A.PRE_ATT_DI_TRCH
                ,A.MATU_END2000
                ,A.ABB_TOT_INI
                ,A.ABB_TOT_ULT
                ,A.ABB_ANNU_ULT
                ,A.DUR_ABB
                ,A.TP_ADEG_ABB
                ,A.MOD_CALC
                ,A.IMP_AZ
                ,A.IMP_ADER
                ,A.IMP_TFR
                ,A.IMP_VOLO
                ,A.VIS_END2000
                ,A.DT_VLDT_PROD
                ,A.DT_INI_VAL_TAR
                ,A.IMPB_VIS_END2000
                ,A.REN_INI_TS_TEC_0
                ,A.PC_RIP_PRE
                ,A.FL_IMPORTI_FORZ
                ,A.PRSTZ_INI_NFORZ
                ,A.VIS_END2000_NFORZ
                ,A.INTR_MORA
                ,A.MANFEE_ANTIC
                ,A.MANFEE_RICOR
                ,A.PRE_UNI_RIVTO
                ,A.PROV_1AA_ACQ
                ,A.PROV_2AA_ACQ
                ,A.PROV_RICOR
                ,A.PROV_INC
                ,A.ALQ_PROV_ACQ
                ,A.ALQ_PROV_INC
                ,A.ALQ_PROV_RICOR
                ,A.IMPB_PROV_ACQ
                ,A.IMPB_PROV_INC
                ,A.IMPB_PROV_RICOR
                ,A.FL_PROV_FORZ
                ,A.PRSTZ_AGG_INI
                ,A.INCR_PRE
                ,A.INCR_PRSTZ
                ,A.DT_ULT_ADEG_PRE_PR
                ,A.PRSTZ_AGG_ULT
                ,A.TS_RIVAL_NET
                ,A.PRE_PATTUITO
                ,A.TP_RIVAL
                ,A.RIS_MAT
                ,A.CPT_MIN_SCAD
                ,A.COMMIS_GEST
                ,A.TP_MANFEE_APPL
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.PC_COMMIS_GEST
                ,A.NUM_GG_RIVAL
                ,A.IMP_TRASFE
                ,A.IMP_TFR_STRC
                ,A.ACQ_EXP
                ,A.REMUN_ASS
                ,A.COMMIS_INTER
                ,A.ALQ_REMUN_ASS
                ,A.ALQ_COMMIS_INTER
                ,A.IMPB_REMUN_ASS
                ,A.IMPB_COMMIS_INTER
                ,A.COS_RUN_ASSVA
                ,A.COS_RUN_ASSVA_IDC
              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )
                    AND B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.ID_TRCH_DI_GAR ASC
           END-EXEC.
           CONTINUE.
       C405-EX.
           EXIT.

       C460-OPEN-CURSOR-EFF-NS.

           PERFORM C405-DECLARE-CURSOR-EFF-NS THRU C405-EX.

           EXEC SQL
                OPEN C-EFF-TGA-NS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       C460-EX.
           EXIT.

       C470-CLOSE-CURSOR-EFF-NS.
           EXEC SQL
                CLOSE C-EFF-TGA-NS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       C470-EX.
           EXIT.

       C480-FETCH-FIRST-EFF-NS.
           PERFORM C460-OPEN-CURSOR-EFF-NS  THRU C460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM C490-FETCH-NEXT-EFF-NS THRU C490-EX
           END-IF.
       C480-EX.
           EXIT.

       C490-FETCH-NEXT-EFF-NS.
           EXEC SQL
                FETCH C-EFF-TGA-NS
           INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM C470-CLOSE-CURSOR-EFF-NS THRU C470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       C490-EX.
           EXIT.

      *----
      *----  gestione WH Effetto NOT STATO / CAUSALE
      *----
       C406-DECLARE-CURSOR-EFF-NS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-EFF-TGA-DI-NS CURSOR FOR

              SELECT DISTINCT(A.DT_VLDT_PROD)

              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )
                    AND B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.DT_VLDT_PROD ASC
           END-EXEC.
           CONTINUE.
       C406-EX.
           EXIT.


       C461-OPEN-CURSOR-EFF-NS.

           PERFORM C406-DECLARE-CURSOR-EFF-NS THRU C406-EX.

           EXEC SQL
                OPEN C-EFF-TGA-DI-NS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       C461-EX.
           EXIT.

       C471-CLOSE-CURSOR-EFF-NS.
           EXEC SQL
                CLOSE C-EFF-TGA-DI-NS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       C471-EX.
           EXIT.

       C481-FETCH-FIRST-EFF-NS.
           PERFORM C461-OPEN-CURSOR-EFF-NS  THRU C461-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM C491-FETCH-NEXT-EFF-NS THRU C491-EX
           END-IF.
       C481-EX.
           EXIT.

       C491-FETCH-NEXT-EFF-NS.
           EXEC SQL
                FETCH C-EFF-TGA-DI-NS
             INTO
                  :TGA-DT-VLDT-PROD-DB
                  :IND-TGA-DT-VLDT-PROD
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM C471-CLOSE-CURSOR-EFF-NS THRU C471-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       C491-EX.
           EXIT.

      *----
      *----  gestione FA Competenza NOT STATO / CAUSALE
      *----
       D405-DECLARE-CURSOR-CPZ-NS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-CPZ-TGA-NS CURSOR FOR
              SELECT
                 A.ID_TRCH_DI_GAR
                ,A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.IB_OGG
                ,A.TP_RGM_FISC
                ,A.DT_EMIS
                ,A.TP_TRCH
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.PRE_CASO_MOR
                ,A.PC_INTR_RIAT
                ,A.IMP_BNS_ANTIC
                ,A.PRE_INI_NET
                ,A.PRE_PP_INI
                ,A.PRE_PP_ULT
                ,A.PRE_TARI_INI
                ,A.PRE_TARI_ULT
                ,A.PRE_INVRIO_INI
                ,A.PRE_INVRIO_ULT
                ,A.PRE_RIVTO
                ,A.IMP_SOPR_PROF
                ,A.IMP_SOPR_SAN
                ,A.IMP_SOPR_SPO
                ,A.IMP_SOPR_TEC
                ,A.IMP_ALT_SOPR
                ,A.PRE_STAB
                ,A.DT_EFF_STAB
                ,A.TS_RIVAL_FIS
                ,A.TS_RIVAL_INDICIZ
                ,A.OLD_TS_TEC
                ,A.RAT_LRD
                ,A.PRE_LRD
                ,A.PRSTZ_INI
                ,A.PRSTZ_ULT
                ,A.CPT_IN_OPZ_RIVTO
                ,A.PRSTZ_INI_STAB
                ,A.CPT_RSH_MOR
                ,A.PRSTZ_RID_INI
                ,A.FL_CAR_CONT
                ,A.BNS_GIA_LIQTO
                ,A.IMP_BNS
                ,A.COD_DVS
                ,A.PRSTZ_INI_NEWFIS
                ,A.IMP_SCON
                ,A.ALQ_SCON
                ,A.IMP_CAR_ACQ
                ,A.IMP_CAR_INC
                ,A.IMP_CAR_GEST
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.RENDTO_LRD
                ,A.PC_RETR
                ,A.RENDTO_RETR
                ,A.MIN_GARTO
                ,A.MIN_TRNUT
                ,A.PRE_ATT_DI_TRCH
                ,A.MATU_END2000
                ,A.ABB_TOT_INI
                ,A.ABB_TOT_ULT
                ,A.ABB_ANNU_ULT
                ,A.DUR_ABB
                ,A.TP_ADEG_ABB
                ,A.MOD_CALC
                ,A.IMP_AZ
                ,A.IMP_ADER
                ,A.IMP_TFR
                ,A.IMP_VOLO
                ,A.VIS_END2000
                ,A.DT_VLDT_PROD
                ,A.DT_INI_VAL_TAR
                ,A.IMPB_VIS_END2000
                ,A.REN_INI_TS_TEC_0
                ,A.PC_RIP_PRE
                ,A.FL_IMPORTI_FORZ
                ,A.PRSTZ_INI_NFORZ
                ,A.VIS_END2000_NFORZ
                ,A.INTR_MORA
                ,A.MANFEE_ANTIC
                ,A.MANFEE_RICOR
                ,A.PRE_UNI_RIVTO
                ,A.PROV_1AA_ACQ
                ,A.PROV_2AA_ACQ
                ,A.PROV_RICOR
                ,A.PROV_INC
                ,A.ALQ_PROV_ACQ
                ,A.ALQ_PROV_INC
                ,A.ALQ_PROV_RICOR
                ,A.IMPB_PROV_ACQ
                ,A.IMPB_PROV_INC
                ,A.IMPB_PROV_RICOR
                ,A.FL_PROV_FORZ
                ,A.PRSTZ_AGG_INI
                ,A.INCR_PRE
                ,A.INCR_PRSTZ
                ,A.DT_ULT_ADEG_PRE_PR
                ,A.PRSTZ_AGG_ULT
                ,A.TS_RIVAL_NET
                ,A.PRE_PATTUITO
                ,A.TP_RIVAL
                ,A.RIS_MAT
                ,A.CPT_MIN_SCAD
                ,A.COMMIS_GEST
                ,A.TP_MANFEE_APPL
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.PC_COMMIS_GEST
                ,A.NUM_GG_RIVAL
                ,A.IMP_TRASFE
                ,A.IMP_TFR_STRC
                ,A.ACQ_EXP
                ,A.REMUN_ASS
                ,A.COMMIS_INTER
                ,A.ALQ_REMUN_ASS
                ,A.ALQ_COMMIS_INTER
                ,A.IMPB_REMUN_ASS
                ,A.IMPB_COMMIS_INTER
                ,A.COS_RUN_ASSVA
                ,A.COS_RUN_ASSVA_IDC
              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )
                    AND B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.ID_TRCH_DI_GAR ASC
           END-EXEC.
           CONTINUE.
       D405-EX.
           EXIT.

       D460-OPEN-CURSOR-CPZ-NS.

           PERFORM D405-DECLARE-CURSOR-CPZ-NS THRU D405-EX.

           EXEC SQL
                OPEN C-CPZ-TGA-NS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       D460-EX.
           EXIT.

       D470-CLOSE-CURSOR-CPZ-NS.
           EXEC SQL
                CLOSE C-CPZ-TGA-NS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       D470-EX.
           EXIT.

       D480-FETCH-FIRST-CPZ-NS.
           PERFORM D460-OPEN-CURSOR-CPZ-NS    THRU D460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM D490-FETCH-NEXT-CPZ-NS THRU D490-EX
           END-IF.
       D480-EX.
           EXIT.

       D490-FETCH-NEXT-CPZ-NS.
           EXEC SQL
                FETCH C-CPZ-TGA-NS
           INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM D470-CLOSE-CURSOR-CPZ-NS THRU D470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       D490-EX.
           EXIT.

      *----
      *----  gestione WH Competenza NOT STATO / CAUSALE
      *----
       D406-DECLARE-CURSOR-CPZ-NS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-CPZ-TGA-DI-NS CURSOR FOR
              SELECT DISTINCT(A.DT_VLDT_PROD)
              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )
                    AND B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.DT_VLDT_PROD ASC
           END-EXEC.
           CONTINUE.
       D406-EX.
           EXIT.

       D461-OPEN-CURSOR-CPZ-NS.

           PERFORM D406-DECLARE-CURSOR-CPZ-NS THRU D406-EX.

           EXEC SQL
                OPEN C-CPZ-TGA-DI-NS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       D461-EX.
           EXIT.

       D471-CLOSE-CURSOR-CPZ-NS.
           EXEC SQL
                CLOSE C-CPZ-TGA-DI-NS
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       D471-EX.
           EXIT.

       D481-FETCH-FIRST-CPZ-NS.
           PERFORM D461-OPEN-CURSOR-CPZ-NS    THRU D461-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM D491-FETCH-NEXT-CPZ-NS THRU D491-EX
           END-IF.
       D481-EX.
           EXIT.

       D491-FETCH-NEXT-CPZ-NS.
           EXEC SQL
                FETCH C-CPZ-TGA-DI-NS
                INTO
                  :TGA-DT-VLDT-PROD-DB
                  :IND-TGA-DT-VLDT-PROD
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM D471-CLOSE-CURSOR-CPZ-NS THRU D471-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       D491-EX.
           EXIT.

      *----
      *----  gestione Effetto STATO / NOT CAUSALE
      *----
       E405-DECLARE-CURSOR-EFF-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-EFF-TGA-NC CURSOR FOR
              SELECT
                 A.ID_TRCH_DI_GAR
                ,A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.IB_OGG
                ,A.TP_RGM_FISC
                ,A.DT_EMIS
                ,A.TP_TRCH
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.PRE_CASO_MOR
                ,A.PC_INTR_RIAT
                ,A.IMP_BNS_ANTIC
                ,A.PRE_INI_NET
                ,A.PRE_PP_INI
                ,A.PRE_PP_ULT
                ,A.PRE_TARI_INI
                ,A.PRE_TARI_ULT
                ,A.PRE_INVRIO_INI
                ,A.PRE_INVRIO_ULT
                ,A.PRE_RIVTO
                ,A.IMP_SOPR_PROF
                ,A.IMP_SOPR_SAN
                ,A.IMP_SOPR_SPO
                ,A.IMP_SOPR_TEC
                ,A.IMP_ALT_SOPR
                ,A.PRE_STAB
                ,A.DT_EFF_STAB
                ,A.TS_RIVAL_FIS
                ,A.TS_RIVAL_INDICIZ
                ,A.OLD_TS_TEC
                ,A.RAT_LRD
                ,A.PRE_LRD
                ,A.PRSTZ_INI
                ,A.PRSTZ_ULT
                ,A.CPT_IN_OPZ_RIVTO
                ,A.PRSTZ_INI_STAB
                ,A.CPT_RSH_MOR
                ,A.PRSTZ_RID_INI
                ,A.FL_CAR_CONT
                ,A.BNS_GIA_LIQTO
                ,A.IMP_BNS
                ,A.COD_DVS
                ,A.PRSTZ_INI_NEWFIS
                ,A.IMP_SCON
                ,A.ALQ_SCON
                ,A.IMP_CAR_ACQ
                ,A.IMP_CAR_INC
                ,A.IMP_CAR_GEST
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.RENDTO_LRD
                ,A.PC_RETR
                ,A.RENDTO_RETR
                ,A.MIN_GARTO
                ,A.MIN_TRNUT
                ,A.PRE_ATT_DI_TRCH
                ,A.MATU_END2000
                ,A.ABB_TOT_INI
                ,A.ABB_TOT_ULT
                ,A.ABB_ANNU_ULT
                ,A.DUR_ABB
                ,A.TP_ADEG_ABB
                ,A.MOD_CALC
                ,A.IMP_AZ
                ,A.IMP_ADER
                ,A.IMP_TFR
                ,A.IMP_VOLO
                ,A.VIS_END2000
                ,A.DT_VLDT_PROD
                ,A.DT_INI_VAL_TAR
                ,A.IMPB_VIS_END2000
                ,A.REN_INI_TS_TEC_0
                ,A.PC_RIP_PRE
                ,A.FL_IMPORTI_FORZ
                ,A.PRSTZ_INI_NFORZ
                ,A.VIS_END2000_NFORZ
                ,A.INTR_MORA
                ,A.MANFEE_ANTIC
                ,A.MANFEE_RICOR
                ,A.PRE_UNI_RIVTO
                ,A.PROV_1AA_ACQ
                ,A.PROV_2AA_ACQ
                ,A.PROV_RICOR
                ,A.PROV_INC
                ,A.ALQ_PROV_ACQ
                ,A.ALQ_PROV_INC
                ,A.ALQ_PROV_RICOR
                ,A.IMPB_PROV_ACQ
                ,A.IMPB_PROV_INC
                ,A.IMPB_PROV_RICOR
                ,A.FL_PROV_FORZ
                ,A.PRSTZ_AGG_INI
                ,A.INCR_PRE
                ,A.INCR_PRSTZ
                ,A.DT_ULT_ADEG_PRE_PR
                ,A.PRSTZ_AGG_ULT
                ,A.TS_RIVAL_NET
                ,A.PRE_PATTUITO
                ,A.TP_RIVAL
                ,A.RIS_MAT
                ,A.CPT_MIN_SCAD
                ,A.COMMIS_GEST
                ,A.TP_MANFEE_APPL
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.PC_COMMIS_GEST
                ,A.NUM_GG_RIVAL
                ,A.IMP_TRASFE
                ,A.IMP_TFR_STRC
                ,A.ACQ_EXP
                ,A.REMUN_ASS
                ,A.COMMIS_INTER
                ,A.ALQ_REMUN_ASS
                ,A.ALQ_COMMIS_INTER
                ,A.IMPB_REMUN_ASS
                ,A.IMPB_COMMIS_INTER
                ,A.COS_RUN_ASSVA
                ,A.COS_RUN_ASSVA_IDC
              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.ID_TRCH_DI_GAR ASC
           END-EXEC.
           CONTINUE.
       E405-EX.
           EXIT.

       E460-OPEN-CURSOR-EFF-NC.

           PERFORM E405-DECLARE-CURSOR-EFF-NC THRU E405-EX.

           EXEC SQL
                OPEN C-EFF-TGA-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       E460-EX.
           EXIT.

       E470-CLOSE-CURSOR-EFF-NC.
           EXEC SQL
                CLOSE C-EFF-TGA-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       E470-EX.
           EXIT.

       E480-FETCH-FIRST-EFF-NC.
           PERFORM E460-OPEN-CURSOR-EFF-NC  THRU E460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM E490-FETCH-NEXT-EFF-NC THRU E490-EX
           END-IF.
       E480-EX.
           EXIT.

       E490-FETCH-NEXT-EFF-NC.
           EXEC SQL
                FETCH C-EFF-TGA-NC
           INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM E470-CLOSE-CURSOR-EFF-NC THRU E470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       E490-EX.
           EXIT.

      *----
      *----  gestione WH Effetto STATO / NOT CAUSALE
      *----
       E406-DECLARE-CURSOR-EFF-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-EFF-TGA-DI-NC CURSOR FOR

              SELECT DISTINCT(A.DT_VLDT_PROD)

              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.DT_VLDT_PROD ASC
           END-EXEC.
           CONTINUE.
       E406-EX.
           EXIT.


       E461-OPEN-CURSOR-EFF-NC.

           PERFORM E406-DECLARE-CURSOR-EFF-NC THRU E406-EX.

           EXEC SQL
                OPEN C-EFF-TGA-DI-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       E461-EX.
           EXIT.

       E471-CLOSE-CURSOR-EFF-NC.
           EXEC SQL
                CLOSE C-EFF-TGA-DI-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       E471-EX.
           EXIT.

       E481-FETCH-FIRST-EFF-NC.
           PERFORM E461-OPEN-CURSOR-EFF-NC  THRU E461-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM E491-FETCH-NEXT-EFF-NC THRU E491-EX
           END-IF.
       E481-EX.
           EXIT.

       E491-FETCH-NEXT-EFF-NC.
           EXEC SQL
                FETCH C-EFF-TGA-DI-NC
             INTO
                  :TGA-DT-VLDT-PROD-DB
                  :IND-TGA-DT-VLDT-PROD
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM E471-CLOSE-CURSOR-EFF-NC THRU E471-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       E491-EX.
           EXIT.

      *----
      *----  gestione FA Competenza STATO / NOT CAUSALE
      *----
       F405-DECLARE-CURSOR-CPZ-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-CPZ-TGA-NC CURSOR FOR
              SELECT
                 A.ID_TRCH_DI_GAR
                ,A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.IB_OGG
                ,A.TP_RGM_FISC
                ,A.DT_EMIS
                ,A.TP_TRCH
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.PRE_CASO_MOR
                ,A.PC_INTR_RIAT
                ,A.IMP_BNS_ANTIC
                ,A.PRE_INI_NET
                ,A.PRE_PP_INI
                ,A.PRE_PP_ULT
                ,A.PRE_TARI_INI
                ,A.PRE_TARI_ULT
                ,A.PRE_INVRIO_INI
                ,A.PRE_INVRIO_ULT
                ,A.PRE_RIVTO
                ,A.IMP_SOPR_PROF
                ,A.IMP_SOPR_SAN
                ,A.IMP_SOPR_SPO
                ,A.IMP_SOPR_TEC
                ,A.IMP_ALT_SOPR
                ,A.PRE_STAB
                ,A.DT_EFF_STAB
                ,A.TS_RIVAL_FIS
                ,A.TS_RIVAL_INDICIZ
                ,A.OLD_TS_TEC
                ,A.RAT_LRD
                ,A.PRE_LRD
                ,A.PRSTZ_INI
                ,A.PRSTZ_ULT
                ,A.CPT_IN_OPZ_RIVTO
                ,A.PRSTZ_INI_STAB
                ,A.CPT_RSH_MOR
                ,A.PRSTZ_RID_INI
                ,A.FL_CAR_CONT
                ,A.BNS_GIA_LIQTO
                ,A.IMP_BNS
                ,A.COD_DVS
                ,A.PRSTZ_INI_NEWFIS
                ,A.IMP_SCON
                ,A.ALQ_SCON
                ,A.IMP_CAR_ACQ
                ,A.IMP_CAR_INC
                ,A.IMP_CAR_GEST
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.RENDTO_LRD
                ,A.PC_RETR
                ,A.RENDTO_RETR
                ,A.MIN_GARTO
                ,A.MIN_TRNUT
                ,A.PRE_ATT_DI_TRCH
                ,A.MATU_END2000
                ,A.ABB_TOT_INI
                ,A.ABB_TOT_ULT
                ,A.ABB_ANNU_ULT
                ,A.DUR_ABB
                ,A.TP_ADEG_ABB
                ,A.MOD_CALC
                ,A.IMP_AZ
                ,A.IMP_ADER
                ,A.IMP_TFR
                ,A.IMP_VOLO
                ,A.VIS_END2000
                ,A.DT_VLDT_PROD
                ,A.DT_INI_VAL_TAR
                ,A.IMPB_VIS_END2000
                ,A.REN_INI_TS_TEC_0
                ,A.PC_RIP_PRE
                ,A.FL_IMPORTI_FORZ
                ,A.PRSTZ_INI_NFORZ
                ,A.VIS_END2000_NFORZ
                ,A.INTR_MORA
                ,A.MANFEE_ANTIC
                ,A.MANFEE_RICOR
                ,A.PRE_UNI_RIVTO
                ,A.PROV_1AA_ACQ
                ,A.PROV_2AA_ACQ
                ,A.PROV_RICOR
                ,A.PROV_INC
                ,A.ALQ_PROV_ACQ
                ,A.ALQ_PROV_INC
                ,A.ALQ_PROV_RICOR
                ,A.IMPB_PROV_ACQ
                ,A.IMPB_PROV_INC
                ,A.IMPB_PROV_RICOR
                ,A.FL_PROV_FORZ
                ,A.PRSTZ_AGG_INI
                ,A.INCR_PRE
                ,A.INCR_PRSTZ
                ,A.DT_ULT_ADEG_PRE_PR
                ,A.PRSTZ_AGG_ULT
                ,A.TS_RIVAL_NET
                ,A.PRE_PATTUITO
                ,A.TP_RIVAL
                ,A.RIS_MAT
                ,A.CPT_MIN_SCAD
                ,A.COMMIS_GEST
                ,A.TP_MANFEE_APPL
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.PC_COMMIS_GEST
                ,A.NUM_GG_RIVAL
                ,A.IMP_TRASFE
                ,A.IMP_TFR_STRC
                ,A.ACQ_EXP
                ,A.REMUN_ASS
                ,A.COMMIS_INTER
                ,A.ALQ_REMUN_ASS
                ,A.ALQ_COMMIS_INTER
                ,A.IMPB_REMUN_ASS
                ,A.IMPB_COMMIS_INTER
                ,A.COS_RUN_ASSVA
                ,A.COS_RUN_ASSVA_IDC
              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.ID_TRCH_DI_GAR ASC
           END-EXEC.
           CONTINUE.
       F405-EX.
           EXIT.

       F460-OPEN-CURSOR-CPZ-NC.

           PERFORM F405-DECLARE-CURSOR-CPZ-NC THRU F405-EX.

           EXEC SQL
                OPEN C-CPZ-TGA-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       F460-EX.
           EXIT.

       F470-CLOSE-CURSOR-CPZ-NC.
           EXEC SQL
                CLOSE C-CPZ-TGA-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       F470-EX.
           EXIT.

       F480-FETCH-FIRST-CPZ-NC.
           PERFORM F460-OPEN-CURSOR-CPZ-NC    THRU F460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM F490-FETCH-NEXT-CPZ-NC THRU F490-EX
           END-IF.
       F480-EX.
           EXIT.

       F490-FETCH-NEXT-CPZ-NC.
           EXEC SQL
                FETCH C-CPZ-TGA-NC
           INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM F470-CLOSE-CURSOR-CPZ-NC THRU F470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       F490-EX.
           EXIT.

      *----
      *----  gestione WH Competenza STATO / NOT CAUSALE
      *----
       F406-DECLARE-CURSOR-CPZ-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-CPZ-TGA-DI-NC CURSOR FOR
              SELECT DISTINCT(A.DT_VLDT_PROD)
              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.DT_VLDT_PROD ASC
           END-EXEC.
           CONTINUE.
       F406-EX.
           EXIT.

       F461-OPEN-CURSOR-CPZ-NC.

           PERFORM F406-DECLARE-CURSOR-CPZ-NC THRU F406-EX.

           EXEC SQL
                OPEN C-CPZ-TGA-DI-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       F461-EX.
           EXIT.

       F471-CLOSE-CURSOR-CPZ-NC.
           EXEC SQL
                CLOSE C-CPZ-TGA-DI-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       F471-EX.
           EXIT.

       F481-FETCH-FIRST-CPZ-NC.
           PERFORM F461-OPEN-CURSOR-CPZ-NC    THRU F461-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM F491-FETCH-NEXT-CPZ-NC THRU F491-EX
           END-IF.
       F481-EX.
           EXIT.

       F491-FETCH-NEXT-CPZ-NC.
           EXEC SQL
                FETCH C-CPZ-TGA-DI-NC
                INTO
                  :TGA-DT-VLDT-PROD-DB
                  :IND-TGA-DT-VLDT-PROD
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM F471-CLOSE-CURSOR-CPZ-NC THRU F471-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       F491-EX.
           EXIT.

      *----
      *----  gestione Effetto NOT STATO / NOT CAUSALE
      *----
       G405-DECLARE-CURSOR-EFF-NS-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-EFF-TGA-NS-NC CURSOR FOR
              SELECT
                 A.ID_TRCH_DI_GAR
                ,A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.IB_OGG
                ,A.TP_RGM_FISC
                ,A.DT_EMIS
                ,A.TP_TRCH
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.PRE_CASO_MOR
                ,A.PC_INTR_RIAT
                ,A.IMP_BNS_ANTIC
                ,A.PRE_INI_NET
                ,A.PRE_PP_INI
                ,A.PRE_PP_ULT
                ,A.PRE_TARI_INI
                ,A.PRE_TARI_ULT
                ,A.PRE_INVRIO_INI
                ,A.PRE_INVRIO_ULT
                ,A.PRE_RIVTO
                ,A.IMP_SOPR_PROF
                ,A.IMP_SOPR_SAN
                ,A.IMP_SOPR_SPO
                ,A.IMP_SOPR_TEC
                ,A.IMP_ALT_SOPR
                ,A.PRE_STAB
                ,A.DT_EFF_STAB
                ,A.TS_RIVAL_FIS
                ,A.TS_RIVAL_INDICIZ
                ,A.OLD_TS_TEC
                ,A.RAT_LRD
                ,A.PRE_LRD
                ,A.PRSTZ_INI
                ,A.PRSTZ_ULT
                ,A.CPT_IN_OPZ_RIVTO
                ,A.PRSTZ_INI_STAB
                ,A.CPT_RSH_MOR
                ,A.PRSTZ_RID_INI
                ,A.FL_CAR_CONT
                ,A.BNS_GIA_LIQTO
                ,A.IMP_BNS
                ,A.COD_DVS
                ,A.PRSTZ_INI_NEWFIS
                ,A.IMP_SCON
                ,A.ALQ_SCON
                ,A.IMP_CAR_ACQ
                ,A.IMP_CAR_INC
                ,A.IMP_CAR_GEST
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.RENDTO_LRD
                ,A.PC_RETR
                ,A.RENDTO_RETR
                ,A.MIN_GARTO
                ,A.MIN_TRNUT
                ,A.PRE_ATT_DI_TRCH
                ,A.MATU_END2000
                ,A.ABB_TOT_INI
                ,A.ABB_TOT_ULT
                ,A.ABB_ANNU_ULT
                ,A.DUR_ABB
                ,A.TP_ADEG_ABB
                ,A.MOD_CALC
                ,A.IMP_AZ
                ,A.IMP_ADER
                ,A.IMP_TFR
                ,A.IMP_VOLO
                ,A.VIS_END2000
                ,A.DT_VLDT_PROD
                ,A.DT_INI_VAL_TAR
                ,A.IMPB_VIS_END2000
                ,A.REN_INI_TS_TEC_0
                ,A.PC_RIP_PRE
                ,A.FL_IMPORTI_FORZ
                ,A.PRSTZ_INI_NFORZ
                ,A.VIS_END2000_NFORZ
                ,A.INTR_MORA
                ,A.MANFEE_ANTIC
                ,A.MANFEE_RICOR
                ,A.PRE_UNI_RIVTO
                ,A.PROV_1AA_ACQ
                ,A.PROV_2AA_ACQ
                ,A.PROV_RICOR
                ,A.PROV_INC
                ,A.ALQ_PROV_ACQ
                ,A.ALQ_PROV_INC
                ,A.ALQ_PROV_RICOR
                ,A.IMPB_PROV_ACQ
                ,A.IMPB_PROV_INC
                ,A.IMPB_PROV_RICOR
                ,A.FL_PROV_FORZ
                ,A.PRSTZ_AGG_INI
                ,A.INCR_PRE
                ,A.INCR_PRSTZ
                ,A.DT_ULT_ADEG_PRE_PR
                ,A.PRSTZ_AGG_ULT
                ,A.TS_RIVAL_NET
                ,A.PRE_PATTUITO
                ,A.TP_RIVAL
                ,A.RIS_MAT
                ,A.CPT_MIN_SCAD
                ,A.COMMIS_GEST
                ,A.TP_MANFEE_APPL
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.PC_COMMIS_GEST
                ,A.NUM_GG_RIVAL
                ,A.IMP_TRASFE
                ,A.IMP_TFR_STRC
                ,A.ACQ_EXP
                ,A.REMUN_ASS
                ,A.COMMIS_INTER
                ,A.ALQ_REMUN_ASS
                ,A.ALQ_COMMIS_INTER
                ,A.IMPB_REMUN_ASS
                ,A.IMPB_COMMIS_INTER
                ,A.COS_RUN_ASSVA
                ,A.COS_RUN_ASSVA_IDC
              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.ID_TRCH_DI_GAR ASC
           END-EXEC.
           CONTINUE.
       G405-EX.
           EXIT.

       G460-OPEN-CURSOR-EFF-NS-NC.

           PERFORM G405-DECLARE-CURSOR-EFF-NS-NC THRU G405-EX.

           EXEC SQL
                OPEN C-EFF-TGA-NS-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       G460-EX.
           EXIT.

       G470-CLOSE-CURSOR-EFF-NS-NC.
           EXEC SQL
                CLOSE C-EFF-TGA-NS-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       G470-EX.
           EXIT.

       G480-FETCH-FIRST-EFF-NS-NC.
           PERFORM G460-OPEN-CURSOR-EFF-NS-NC  THRU G460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM G490-FETCH-NEXT-EFF-NS-NC THRU G490-EX
           END-IF.
       G480-EX.
           EXIT.

       G490-FETCH-NEXT-EFF-NS-NC.
           EXEC SQL
                FETCH C-EFF-TGA-NS-NC
           INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM G470-CLOSE-CURSOR-EFF-NS-NC THRU G470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       G490-EX.
           EXIT.

      *----
      *----  gestione WH Effetto NOT STATO / NOT CAUSALE
      *----
       G406-DECLARE-CURSOR-EFF-NS-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-EFF-TGA-DI-NS-NC CURSOR FOR

              SELECT DISTINCT(A.DT_VLDT_PROD)

              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.ID_MOVI_CHIU IS NULL

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.ID_MOVI_CHIU IS NULL

                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.DT_VLDT_PROD ASC
           END-EXEC.
           CONTINUE.
       G406-EX.
           EXIT.


       G461-OPEN-CURSOR-EFF-NS-NC.

           PERFORM G406-DECLARE-CURSOR-EFF-NS-NC THRU G406-EX.

           EXEC SQL
                OPEN C-EFF-TGA-DI-NS-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       G461-EX.
           EXIT.

       G471-CLOSE-CURSOR-EFF-NS-NC.
           EXEC SQL
                CLOSE C-EFF-TGA-DI-NS-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       G471-EX.
           EXIT.

       G481-FETCH-FIRST-EFF-NS-NC.
           PERFORM G461-OPEN-CURSOR-EFF-NS-NC  THRU G461-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM G491-FETCH-NEXT-EFF-NS-NC THRU G491-EX
           END-IF.
       G481-EX.
           EXIT.

       G491-FETCH-NEXT-EFF-NS-NC.
           EXEC SQL
                FETCH C-EFF-TGA-DI-NS-NC
             INTO
                  :TGA-DT-VLDT-PROD-DB
                  :IND-TGA-DT-VLDT-PROD
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM G471-CLOSE-CURSOR-EFF-NS-NC THRU G471-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       G491-EX.
           EXIT.

      *----
      *----  gestione FA Competenza NOT STATO / NOT CAUSALE
      *----
       H405-DECLARE-CURSOR-CPZ-NS-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-CPZ-TGA-NS-NC CURSOR FOR
              SELECT
                 A.ID_TRCH_DI_GAR
                ,A.ID_GAR
                ,A.ID_ADES
                ,A.ID_POLI
                ,A.ID_MOVI_CRZ
                ,A.ID_MOVI_CHIU
                ,A.DT_INI_EFF
                ,A.DT_END_EFF
                ,A.COD_COMP_ANIA
                ,A.DT_DECOR
                ,A.DT_SCAD
                ,A.IB_OGG
                ,A.TP_RGM_FISC
                ,A.DT_EMIS
                ,A.TP_TRCH
                ,A.DUR_AA
                ,A.DUR_MM
                ,A.DUR_GG
                ,A.PRE_CASO_MOR
                ,A.PC_INTR_RIAT
                ,A.IMP_BNS_ANTIC
                ,A.PRE_INI_NET
                ,A.PRE_PP_INI
                ,A.PRE_PP_ULT
                ,A.PRE_TARI_INI
                ,A.PRE_TARI_ULT
                ,A.PRE_INVRIO_INI
                ,A.PRE_INVRIO_ULT
                ,A.PRE_RIVTO
                ,A.IMP_SOPR_PROF
                ,A.IMP_SOPR_SAN
                ,A.IMP_SOPR_SPO
                ,A.IMP_SOPR_TEC
                ,A.IMP_ALT_SOPR
                ,A.PRE_STAB
                ,A.DT_EFF_STAB
                ,A.TS_RIVAL_FIS
                ,A.TS_RIVAL_INDICIZ
                ,A.OLD_TS_TEC
                ,A.RAT_LRD
                ,A.PRE_LRD
                ,A.PRSTZ_INI
                ,A.PRSTZ_ULT
                ,A.CPT_IN_OPZ_RIVTO
                ,A.PRSTZ_INI_STAB
                ,A.CPT_RSH_MOR
                ,A.PRSTZ_RID_INI
                ,A.FL_CAR_CONT
                ,A.BNS_GIA_LIQTO
                ,A.IMP_BNS
                ,A.COD_DVS
                ,A.PRSTZ_INI_NEWFIS
                ,A.IMP_SCON
                ,A.ALQ_SCON
                ,A.IMP_CAR_ACQ
                ,A.IMP_CAR_INC
                ,A.IMP_CAR_GEST
                ,A.ETA_AA_1O_ASSTO
                ,A.ETA_MM_1O_ASSTO
                ,A.ETA_AA_2O_ASSTO
                ,A.ETA_MM_2O_ASSTO
                ,A.ETA_AA_3O_ASSTO
                ,A.ETA_MM_3O_ASSTO
                ,A.RENDTO_LRD
                ,A.PC_RETR
                ,A.RENDTO_RETR
                ,A.MIN_GARTO
                ,A.MIN_TRNUT
                ,A.PRE_ATT_DI_TRCH
                ,A.MATU_END2000
                ,A.ABB_TOT_INI
                ,A.ABB_TOT_ULT
                ,A.ABB_ANNU_ULT
                ,A.DUR_ABB
                ,A.TP_ADEG_ABB
                ,A.MOD_CALC
                ,A.IMP_AZ
                ,A.IMP_ADER
                ,A.IMP_TFR
                ,A.IMP_VOLO
                ,A.VIS_END2000
                ,A.DT_VLDT_PROD
                ,A.DT_INI_VAL_TAR
                ,A.IMPB_VIS_END2000
                ,A.REN_INI_TS_TEC_0
                ,A.PC_RIP_PRE
                ,A.FL_IMPORTI_FORZ
                ,A.PRSTZ_INI_NFORZ
                ,A.VIS_END2000_NFORZ
                ,A.INTR_MORA
                ,A.MANFEE_ANTIC
                ,A.MANFEE_RICOR
                ,A.PRE_UNI_RIVTO
                ,A.PROV_1AA_ACQ
                ,A.PROV_2AA_ACQ
                ,A.PROV_RICOR
                ,A.PROV_INC
                ,A.ALQ_PROV_ACQ
                ,A.ALQ_PROV_INC
                ,A.ALQ_PROV_RICOR
                ,A.IMPB_PROV_ACQ
                ,A.IMPB_PROV_INC
                ,A.IMPB_PROV_RICOR
                ,A.FL_PROV_FORZ
                ,A.PRSTZ_AGG_INI
                ,A.INCR_PRE
                ,A.INCR_PRSTZ
                ,A.DT_ULT_ADEG_PRE_PR
                ,A.PRSTZ_AGG_ULT
                ,A.TS_RIVAL_NET
                ,A.PRE_PATTUITO
                ,A.TP_RIVAL
                ,A.RIS_MAT
                ,A.CPT_MIN_SCAD
                ,A.COMMIS_GEST
                ,A.TP_MANFEE_APPL
                ,A.DS_RIGA
                ,A.DS_OPER_SQL
                ,A.DS_VER
                ,A.DS_TS_INI_CPTZ
                ,A.DS_TS_END_CPTZ
                ,A.DS_UTENTE
                ,A.DS_STATO_ELAB
                ,A.PC_COMMIS_GEST
                ,A.NUM_GG_RIVAL
                ,A.IMP_TRASFE
                ,A.IMP_TFR_STRC
                ,A.ACQ_EXP
                ,A.REMUN_ASS
                ,A.COMMIS_INTER
                ,A.ALQ_REMUN_ASS
                ,A.ALQ_COMMIS_INTER
                ,A.IMPB_REMUN_ASS
                ,A.IMPB_COMMIS_INTER
                ,A.COS_RUN_ASSVA
                ,A.COS_RUN_ASSVA_IDC
              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI
                    AND A.ID_ADES BETWEEN
                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
                    AND A.ID_GAR BETWEEN
                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.ID_TRCH_DI_GAR ASC
           END-EXEC.
           CONTINUE.
       H405-EX.
           EXIT.

       H460-OPEN-CURSOR-CPZ-NS-NC.

           PERFORM H405-DECLARE-CURSOR-CPZ-NS-NC THRU H405-EX.

           EXEC SQL
                OPEN C-CPZ-TGA-NS-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       H460-EX.
           EXIT.

       H470-CLOSE-CURSOR-CPZ-NS-NC.
           EXEC SQL
                CLOSE C-CPZ-TGA-NS-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       H470-EX.
           EXIT.

       H480-FETCH-FIRST-CPZ-NS-NC.
           PERFORM H460-OPEN-CURSOR-CPZ-NS-NC    THRU H460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM H490-FETCH-NEXT-CPZ-NS-NC THRU H490-EX
           END-IF.
       H480-EX.
           EXIT.

       H490-FETCH-NEXT-CPZ-NS-NC.
           EXEC SQL
                FETCH C-CPZ-TGA-NS-NC
           INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM H470-CLOSE-CURSOR-CPZ-NS-NC THRU H470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       H490-EX.
           EXIT.

      *----
      *----  gestione WH Competenza NOT STATO / NOT CAUSALE
      *----
       H406-DECLARE-CURSOR-CPZ-NS-NC.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX

           EXEC SQL
                DECLARE C-CPZ-TGA-DI-NS-NC CURSOR FOR
              SELECT DISTINCT(A.DT_VLDT_PROD)
              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
              WHERE     A.ID_POLI = :TGA-ID-POLI

                    AND A.COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND A.COD_COMP_ANIA =
                        B.COD_COMP_ANIA

                    AND A.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND A.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND A.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND A.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND B.DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB

                    AND B.DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND B.DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

                    AND B.TP_OGG      = 'TG'
                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR

                    AND
                    NOT B.TP_STAT_BUS IN (
                                         :LDBV1361-TP-STAT-BUS-01,
                                         :LDBV1361-TP-STAT-BUS-02,
                                         :LDBV1361-TP-STAT-BUS-03,
                                         :LDBV1361-TP-STAT-BUS-04,
                                         :LDBV1361-TP-STAT-BUS-05,
                                         :LDBV1361-TP-STAT-BUS-06,
                                         :LDBV1361-TP-STAT-BUS-07
                                         )
                    AND
                    NOT B.TP_CAUS     IN (
                                         :LDBV1361-TP-CAUS-01,
                                         :LDBV1361-TP-CAUS-02,
                                         :LDBV1361-TP-CAUS-03,
                                         :LDBV1361-TP-CAUS-04,
                                         :LDBV1361-TP-CAUS-05,
                                         :LDBV1361-TP-CAUS-06,
                                         :LDBV1361-TP-CAUS-07
                                         )

              ORDER BY A.DT_VLDT_PROD ASC
           END-EXEC.
           CONTINUE.
       H406-EX.
           EXIT.

       H461-OPEN-CURSOR-CPZ-NS-NC.

           PERFORM H406-DECLARE-CURSOR-CPZ-NS-NC THRU H406-EX.

           EXEC SQL
                OPEN C-CPZ-TGA-DI-NS-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       H461-EX.
           EXIT.

       H471-CLOSE-CURSOR-CPZ-NS-NC.
           EXEC SQL
                CLOSE C-CPZ-TGA-DI-NS-NC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       H471-EX.
           EXIT.

       H481-FETCH-FIRST-CPZ-NS-NC.
           PERFORM H461-OPEN-CURSOR-CPZ-NS-NC    THRU H461-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM H491-FETCH-NEXT-CPZ-NS-NC THRU H491-EX
           END-IF.
       H481-EX.
           EXIT.

       H491-FETCH-NEXT-CPZ-NS-NC.
           EXEC SQL
                FETCH C-CPZ-TGA-DI-NS-NC
                INTO
                  :TGA-DT-VLDT-PROD-DB
                  :IND-TGA-DT-VLDT-PROD
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM H471-CLOSE-CURSOR-CPZ-NS-NC THRU H471-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       H491-EX.
           EXIT.


       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-TGA-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO TGA-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-TGA-DT-SCAD = -1
              MOVE HIGH-VALUES TO TGA-DT-SCAD-NULL
           END-IF
           IF IND-TGA-IB-OGG = -1
              MOVE HIGH-VALUES TO TGA-IB-OGG-NULL
           END-IF
           IF IND-TGA-DT-EMIS = -1
              MOVE HIGH-VALUES TO TGA-DT-EMIS-NULL
           END-IF
           IF IND-TGA-DUR-AA = -1
              MOVE HIGH-VALUES TO TGA-DUR-AA-NULL
           END-IF
           IF IND-TGA-DUR-MM = -1
              MOVE HIGH-VALUES TO TGA-DUR-MM-NULL
           END-IF
           IF IND-TGA-DUR-GG = -1
              MOVE HIGH-VALUES TO TGA-DUR-GG-NULL
           END-IF
           IF IND-TGA-PRE-CASO-MOR = -1
              MOVE HIGH-VALUES TO TGA-PRE-CASO-MOR-NULL
           END-IF
           IF IND-TGA-PC-INTR-RIAT = -1
              MOVE HIGH-VALUES TO TGA-PC-INTR-RIAT-NULL
           END-IF
           IF IND-TGA-IMP-BNS-ANTIC = -1
              MOVE HIGH-VALUES TO TGA-IMP-BNS-ANTIC-NULL
           END-IF
           IF IND-TGA-PRE-INI-NET = -1
              MOVE HIGH-VALUES TO TGA-PRE-INI-NET-NULL
           END-IF
           IF IND-TGA-PRE-PP-INI = -1
              MOVE HIGH-VALUES TO TGA-PRE-PP-INI-NULL
           END-IF
           IF IND-TGA-PRE-PP-ULT = -1
              MOVE HIGH-VALUES TO TGA-PRE-PP-ULT-NULL
           END-IF
           IF IND-TGA-PRE-TARI-INI = -1
              MOVE HIGH-VALUES TO TGA-PRE-TARI-INI-NULL
           END-IF
           IF IND-TGA-PRE-TARI-ULT = -1
              MOVE HIGH-VALUES TO TGA-PRE-TARI-ULT-NULL
           END-IF
           IF IND-TGA-PRE-INVRIO-INI = -1
              MOVE HIGH-VALUES TO TGA-PRE-INVRIO-INI-NULL
           END-IF
           IF IND-TGA-PRE-INVRIO-ULT = -1
              MOVE HIGH-VALUES TO TGA-PRE-INVRIO-ULT-NULL
           END-IF
           IF IND-TGA-PRE-RIVTO = -1
              MOVE HIGH-VALUES TO TGA-PRE-RIVTO-NULL
           END-IF
           IF IND-TGA-IMP-SOPR-PROF = -1
              MOVE HIGH-VALUES TO TGA-IMP-SOPR-PROF-NULL
           END-IF
           IF IND-TGA-IMP-SOPR-SAN = -1
              MOVE HIGH-VALUES TO TGA-IMP-SOPR-SAN-NULL
           END-IF
           IF IND-TGA-IMP-SOPR-SPO = -1
              MOVE HIGH-VALUES TO TGA-IMP-SOPR-SPO-NULL
           END-IF
           IF IND-TGA-IMP-SOPR-TEC = -1
              MOVE HIGH-VALUES TO TGA-IMP-SOPR-TEC-NULL
           END-IF
           IF IND-TGA-IMP-ALT-SOPR = -1
              MOVE HIGH-VALUES TO TGA-IMP-ALT-SOPR-NULL
           END-IF
           IF IND-TGA-PRE-STAB = -1
              MOVE HIGH-VALUES TO TGA-PRE-STAB-NULL
           END-IF
           IF IND-TGA-DT-EFF-STAB = -1
              MOVE HIGH-VALUES TO TGA-DT-EFF-STAB-NULL
           END-IF
           IF IND-TGA-TS-RIVAL-FIS = -1
              MOVE HIGH-VALUES TO TGA-TS-RIVAL-FIS-NULL
           END-IF
           IF IND-TGA-TS-RIVAL-INDICIZ = -1
              MOVE HIGH-VALUES TO TGA-TS-RIVAL-INDICIZ-NULL
           END-IF
           IF IND-TGA-OLD-TS-TEC = -1
              MOVE HIGH-VALUES TO TGA-OLD-TS-TEC-NULL
           END-IF
           IF IND-TGA-RAT-LRD = -1
              MOVE HIGH-VALUES TO TGA-RAT-LRD-NULL
           END-IF
           IF IND-TGA-PRE-LRD = -1
              MOVE HIGH-VALUES TO TGA-PRE-LRD-NULL
           END-IF
           IF IND-TGA-PRSTZ-INI = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NULL
           END-IF
           IF IND-TGA-PRSTZ-ULT = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-ULT-NULL
           END-IF
           IF IND-TGA-CPT-IN-OPZ-RIVTO = -1
              MOVE HIGH-VALUES TO TGA-CPT-IN-OPZ-RIVTO-NULL
           END-IF
           IF IND-TGA-PRSTZ-INI-STAB = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-STAB-NULL
           END-IF
           IF IND-TGA-CPT-RSH-MOR = -1
              MOVE HIGH-VALUES TO TGA-CPT-RSH-MOR-NULL
           END-IF
           IF IND-TGA-PRSTZ-RID-INI = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-RID-INI-NULL
           END-IF
           IF IND-TGA-FL-CAR-CONT = -1
              MOVE HIGH-VALUES TO TGA-FL-CAR-CONT-NULL
           END-IF
           IF IND-TGA-BNS-GIA-LIQTO = -1
              MOVE HIGH-VALUES TO TGA-BNS-GIA-LIQTO-NULL
           END-IF
           IF IND-TGA-IMP-BNS = -1
              MOVE HIGH-VALUES TO TGA-IMP-BNS-NULL
           END-IF
           IF IND-TGA-PRSTZ-INI-NEWFIS = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NEWFIS-NULL
           END-IF
           IF IND-TGA-IMP-SCON = -1
              MOVE HIGH-VALUES TO TGA-IMP-SCON-NULL
           END-IF
           IF IND-TGA-ALQ-SCON = -1
              MOVE HIGH-VALUES TO TGA-ALQ-SCON-NULL
           END-IF
           IF IND-TGA-IMP-CAR-ACQ = -1
              MOVE HIGH-VALUES TO TGA-IMP-CAR-ACQ-NULL
           END-IF
           IF IND-TGA-IMP-CAR-INC = -1
              MOVE HIGH-VALUES TO TGA-IMP-CAR-INC-NULL
           END-IF
           IF IND-TGA-IMP-CAR-GEST = -1
              MOVE HIGH-VALUES TO TGA-IMP-CAR-GEST-NULL
           END-IF
           IF IND-TGA-ETA-AA-1O-ASSTO = -1
              MOVE HIGH-VALUES TO TGA-ETA-AA-1O-ASSTO-NULL
           END-IF
           IF IND-TGA-ETA-MM-1O-ASSTO = -1
              MOVE HIGH-VALUES TO TGA-ETA-MM-1O-ASSTO-NULL
           END-IF
           IF IND-TGA-ETA-AA-2O-ASSTO = -1
              MOVE HIGH-VALUES TO TGA-ETA-AA-2O-ASSTO-NULL
           END-IF
           IF IND-TGA-ETA-MM-2O-ASSTO = -1
              MOVE HIGH-VALUES TO TGA-ETA-MM-2O-ASSTO-NULL
           END-IF
           IF IND-TGA-ETA-AA-3O-ASSTO = -1
              MOVE HIGH-VALUES TO TGA-ETA-AA-3O-ASSTO-NULL
           END-IF
           IF IND-TGA-ETA-MM-3O-ASSTO = -1
              MOVE HIGH-VALUES TO TGA-ETA-MM-3O-ASSTO-NULL
           END-IF
           IF IND-TGA-RENDTO-LRD = -1
              MOVE HIGH-VALUES TO TGA-RENDTO-LRD-NULL
           END-IF
           IF IND-TGA-PC-RETR = -1
              MOVE HIGH-VALUES TO TGA-PC-RETR-NULL
           END-IF
           IF IND-TGA-RENDTO-RETR = -1
              MOVE HIGH-VALUES TO TGA-RENDTO-RETR-NULL
           END-IF
           IF IND-TGA-MIN-GARTO = -1
              MOVE HIGH-VALUES TO TGA-MIN-GARTO-NULL
           END-IF
           IF IND-TGA-MIN-TRNUT = -1
              MOVE HIGH-VALUES TO TGA-MIN-TRNUT-NULL
           END-IF
           IF IND-TGA-PRE-ATT-DI-TRCH = -1
              MOVE HIGH-VALUES TO TGA-PRE-ATT-DI-TRCH-NULL
           END-IF
           IF IND-TGA-MATU-END2000 = -1
              MOVE HIGH-VALUES TO TGA-MATU-END2000-NULL
           END-IF
           IF IND-TGA-ABB-TOT-INI = -1
              MOVE HIGH-VALUES TO TGA-ABB-TOT-INI-NULL
           END-IF
           IF IND-TGA-ABB-TOT-ULT = -1
              MOVE HIGH-VALUES TO TGA-ABB-TOT-ULT-NULL
           END-IF
           IF IND-TGA-ABB-ANNU-ULT = -1
              MOVE HIGH-VALUES TO TGA-ABB-ANNU-ULT-NULL
           END-IF
           IF IND-TGA-DUR-ABB = -1
              MOVE HIGH-VALUES TO TGA-DUR-ABB-NULL
           END-IF
           IF IND-TGA-TP-ADEG-ABB = -1
              MOVE HIGH-VALUES TO TGA-TP-ADEG-ABB-NULL
           END-IF
           IF IND-TGA-MOD-CALC = -1
              MOVE HIGH-VALUES TO TGA-MOD-CALC-NULL
           END-IF
           IF IND-TGA-IMP-AZ = -1
              MOVE HIGH-VALUES TO TGA-IMP-AZ-NULL
           END-IF
           IF IND-TGA-IMP-ADER = -1
              MOVE HIGH-VALUES TO TGA-IMP-ADER-NULL
           END-IF
           IF IND-TGA-IMP-TFR = -1
              MOVE HIGH-VALUES TO TGA-IMP-TFR-NULL
           END-IF
           IF IND-TGA-IMP-VOLO = -1
              MOVE HIGH-VALUES TO TGA-IMP-VOLO-NULL
           END-IF
           IF IND-TGA-VIS-END2000 = -1
              MOVE HIGH-VALUES TO TGA-VIS-END2000-NULL
           END-IF
           IF IND-TGA-DT-VLDT-PROD = -1
              MOVE HIGH-VALUES TO TGA-DT-VLDT-PROD-NULL
           END-IF
           IF IND-TGA-DT-INI-VAL-TAR = -1
              MOVE HIGH-VALUES TO TGA-DT-INI-VAL-TAR-NULL
           END-IF
           IF IND-TGA-IMPB-VIS-END2000 = -1
              MOVE HIGH-VALUES TO TGA-IMPB-VIS-END2000-NULL
           END-IF
           IF IND-TGA-REN-INI-TS-TEC-0 = -1
              MOVE HIGH-VALUES TO TGA-REN-INI-TS-TEC-0-NULL
           END-IF
           IF IND-TGA-PC-RIP-PRE = -1
              MOVE HIGH-VALUES TO TGA-PC-RIP-PRE-NULL
           END-IF
           IF IND-TGA-FL-IMPORTI-FORZ = -1
              MOVE HIGH-VALUES TO TGA-FL-IMPORTI-FORZ-NULL
           END-IF
           IF IND-TGA-PRSTZ-INI-NFORZ = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NFORZ-NULL
           END-IF
           IF IND-TGA-VIS-END2000-NFORZ = -1
              MOVE HIGH-VALUES TO TGA-VIS-END2000-NFORZ-NULL
           END-IF
           IF IND-TGA-INTR-MORA = -1
              MOVE HIGH-VALUES TO TGA-INTR-MORA-NULL
           END-IF
           IF IND-TGA-MANFEE-ANTIC = -1
              MOVE HIGH-VALUES TO TGA-MANFEE-ANTIC-NULL
           END-IF
           IF IND-TGA-MANFEE-RICOR = -1
              MOVE HIGH-VALUES TO TGA-MANFEE-RICOR-NULL
           END-IF
           IF IND-TGA-PRE-UNI-RIVTO = -1
              MOVE HIGH-VALUES TO TGA-PRE-UNI-RIVTO-NULL
           END-IF
           IF IND-TGA-PROV-1AA-ACQ = -1
              MOVE HIGH-VALUES TO TGA-PROV-1AA-ACQ-NULL
           END-IF
           IF IND-TGA-PROV-2AA-ACQ = -1
              MOVE HIGH-VALUES TO TGA-PROV-2AA-ACQ-NULL
           END-IF
           IF IND-TGA-PROV-RICOR = -1
              MOVE HIGH-VALUES TO TGA-PROV-RICOR-NULL
           END-IF
           IF IND-TGA-PROV-INC = -1
              MOVE HIGH-VALUES TO TGA-PROV-INC-NULL
           END-IF
           IF IND-TGA-ALQ-PROV-ACQ = -1
              MOVE HIGH-VALUES TO TGA-ALQ-PROV-ACQ-NULL
           END-IF
           IF IND-TGA-ALQ-PROV-INC = -1
              MOVE HIGH-VALUES TO TGA-ALQ-PROV-INC-NULL
           END-IF
           IF IND-TGA-ALQ-PROV-RICOR = -1
              MOVE HIGH-VALUES TO TGA-ALQ-PROV-RICOR-NULL
           END-IF
           IF IND-TGA-IMPB-PROV-ACQ = -1
              MOVE HIGH-VALUES TO TGA-IMPB-PROV-ACQ-NULL
           END-IF
           IF IND-TGA-IMPB-PROV-INC = -1
              MOVE HIGH-VALUES TO TGA-IMPB-PROV-INC-NULL
           END-IF
           IF IND-TGA-IMPB-PROV-RICOR = -1
              MOVE HIGH-VALUES TO TGA-IMPB-PROV-RICOR-NULL
           END-IF
           IF IND-TGA-FL-PROV-FORZ = -1
              MOVE HIGH-VALUES TO TGA-FL-PROV-FORZ-NULL
           END-IF
           IF IND-TGA-PRSTZ-AGG-INI = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-INI-NULL
           END-IF
           IF IND-TGA-INCR-PRE = -1
              MOVE HIGH-VALUES TO TGA-INCR-PRE-NULL
           END-IF
           IF IND-TGA-INCR-PRSTZ = -1
              MOVE HIGH-VALUES TO TGA-INCR-PRSTZ-NULL
           END-IF
           IF IND-TGA-DT-ULT-ADEG-PRE-PR = -1
              MOVE HIGH-VALUES TO TGA-DT-ULT-ADEG-PRE-PR-NULL
           END-IF
           IF IND-TGA-PRSTZ-AGG-ULT = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-ULT-NULL
           END-IF
           IF IND-TGA-TS-RIVAL-NET = -1
              MOVE HIGH-VALUES TO TGA-TS-RIVAL-NET-NULL
           END-IF
           IF IND-TGA-PRE-PATTUITO = -1
              MOVE HIGH-VALUES TO TGA-PRE-PATTUITO-NULL
           END-IF
           IF IND-TGA-TP-RIVAL = -1
              MOVE HIGH-VALUES TO TGA-TP-RIVAL-NULL
           END-IF
           IF IND-TGA-RIS-MAT = -1
              MOVE HIGH-VALUES TO TGA-RIS-MAT-NULL
           END-IF
           IF IND-TGA-CPT-MIN-SCAD = -1
              MOVE HIGH-VALUES TO TGA-CPT-MIN-SCAD-NULL
           END-IF
           IF IND-TGA-COMMIS-GEST = -1
              MOVE HIGH-VALUES TO TGA-COMMIS-GEST-NULL
           END-IF
           IF IND-TGA-TP-MANFEE-APPL = -1
              MOVE HIGH-VALUES TO TGA-TP-MANFEE-APPL-NULL
           END-IF
           IF IND-TGA-PC-COMMIS-GEST = -1
              MOVE HIGH-VALUES TO TGA-PC-COMMIS-GEST-NULL
           END-IF
           IF IND-TGA-NUM-GG-RIVAL = -1
              MOVE HIGH-VALUES TO TGA-NUM-GG-RIVAL-NULL
           END-IF
           IF IND-TGA-IMP-TRASFE = -1
              MOVE HIGH-VALUES TO TGA-IMP-TRASFE-NULL
           END-IF
           IF IND-TGA-IMP-TFR-STRC = -1
              MOVE HIGH-VALUES TO TGA-IMP-TFR-STRC-NULL
           END-IF
           IF IND-TGA-ACQ-EXP = -1
              MOVE HIGH-VALUES TO TGA-ACQ-EXP-NULL
           END-IF
           IF IND-TGA-REMUN-ASS = -1
              MOVE HIGH-VALUES TO TGA-REMUN-ASS-NULL
           END-IF
           IF IND-TGA-COMMIS-INTER = -1
              MOVE HIGH-VALUES TO TGA-COMMIS-INTER-NULL
           END-IF
           IF IND-TGA-ALQ-REMUN-ASS = -1
              MOVE HIGH-VALUES TO TGA-ALQ-REMUN-ASS-NULL
           END-IF
           IF IND-TGA-ALQ-COMMIS-INTER = -1
              MOVE HIGH-VALUES TO TGA-ALQ-COMMIS-INTER-NULL
           END-IF
           IF IND-TGA-IMPB-REMUN-ASS = -1
              MOVE HIGH-VALUES TO TGA-IMPB-REMUN-ASS-NULL
           END-IF
           IF IND-TGA-IMPB-COMMIS-INTER = -1
              MOVE HIGH-VALUES TO TGA-IMPB-COMMIS-INTER-NULL
           END-IF.
           IF IND-TGA-COS-RUN-ASSVA = -1
              MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-NULL
           END-IF
           IF IND-TGA-COS-RUN-ASSVA-IDC = -1
              MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-IDC-NULL
           END-IF.


       Z100-EX.
           EXIT.


      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.

           MOVE TGA-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO TGA-DT-INI-EFF
           MOVE TGA-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO TGA-DT-END-EFF
           MOVE TGA-DT-DECOR-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO TGA-DT-DECOR
           IF IND-TGA-DT-SCAD = 0
               MOVE TGA-DT-SCAD-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TGA-DT-SCAD
           END-IF
           IF IND-TGA-DT-EMIS = 0
               MOVE TGA-DT-EMIS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TGA-DT-EMIS
           END-IF
           IF IND-TGA-DT-EFF-STAB = 0
               MOVE TGA-DT-EFF-STAB-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TGA-DT-EFF-STAB
           END-IF
           IF IND-TGA-DT-VLDT-PROD = 0
               MOVE TGA-DT-VLDT-PROD-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TGA-DT-VLDT-PROD
           END-IF
           IF IND-TGA-DT-INI-VAL-TAR = 0
               MOVE TGA-DT-INI-VAL-TAR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TGA-DT-INI-VAL-TAR
           END-IF
           IF IND-TGA-DT-ULT-ADEG-PRE-PR = 0
               MOVE TGA-DT-ULT-ADEG-PRE-PR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TGA-DT-ULT-ADEG-PRE-PR
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.


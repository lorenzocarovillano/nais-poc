      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA VER. 1.0                          **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LLBS0250.
       AUTHOR.             IASS.
       DATE-WRITTEN.       GIUGNO 2008.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *    PROGRAMMA ..... LLBS0250
      *    TIPOLOGIA...... DRIVER EOC
      *    PROCESSO....... LEGGE E BILANCIO
      *    FUNZIONE....... ESTRAZIONE RISERVA MATEMATICA
      *    DESCRIZIONE....
      *    PAGINA WEB.....
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
      *
       DATA DIVISION.
       FILE SECTION.
      *
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LLBS0250'.
       01  WK-TABELLA                       PIC X(020) VALUE SPACES.
      *----------------------------------------------------------------*
      *    COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IEAV9903.
           COPY IDSV0002.
      *
      * ----------------------------------------------------------------
      *   AREA ESTRAZIONE SEQUENCE
      * ----------------------------------------------------------------
       01  AREA-IO-LCCS0090.
           COPY LCCC0090                 REPLACING ==(SF)== BY ==S090==.
      * ---------------------------------------------------------------
      *    COPY DISPATCHER
      * ---------------------------------------------------------------
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      * COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      * COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *----------------------------------------------------------------*
      *    COPY DELLE TABELLE DB2
      *----------------------------------------------------------------*
      *--> AREA BIL-VAR-DI-CALC
           COPY IDBVB051.
      *
      *----------------------------------------------------------------*
      * CAMPI DI APPOGGIO PER LA COMPILAZIONE.
      *----------------------------------------------------------------*
      *
       01  LCCS0090                         PIC X(008) VALUE 'LCCS0090'.

      *----------------------------------------------------------------*
      *    DEFINIZIONE DI INDICI
      *----------------------------------------------------------------*
      *
       01  IX-INDICI.
           03 IX-TAB-B05                     PIC S9(04) COMP VALUE 0.
      *
      *---------------------------------------------------------------*
       LINKAGE SECTION.
      *---------------------------------------------------------------*
      *
       01  AREA-IDSV0001.
           COPY IDSV0001.
      *
       01 AREA-OUT.
           03 WB05-AREA-B05.
              04 WB05-ELE-B05-MAX        PIC S9(04) COMP.
              04 WB05-TAB-B05.
              COPY LCCVB051              REPLACING ==(SF)== BY ==WB05==.
      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING     AREA-IDSV0001
                                    AREA-OUT.
      *---------------------------------------------------------------*
      *
           PERFORM S0000-OPERAZIONI-INIZIALI        THRU EX-S0000.
      *
           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE            THRU EX-S1000
           END-IF.
      *
           PERFORM S9000-OPERAZIONI-FINALI          THRU EX-S9000.
      *
      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.
      *
           INITIALIZE IX-INDICI
                      BILA-VAR-CALC-T.

           MOVE  'BILA-VAR-CALC-T'              TO WK-TABELLA.

      *
       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.
      *
           IF IDSV0001-ESITO-OK
              PERFORM SCRIVI-BIL-VAR-DI-CALC-T
                 THRU SCRIVI-BIL-VAR-DI-CALC-T-EX
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.
      *  ---------------------------------------------------------------
      *     CONTIENE STATEMENTS PER LA FASE DI EOC
      *  ---------------------------------------------------------------
      *
      *--> VARIABILI DI CALCOLO RISERVA
            COPY LCCVB055              REPLACING ==(SF)== BY ==WB05==.
            COPY LCCVB056              REPLACING ==(SF)== BY ==WB05==.
      * ----------------------------------------------------------------
      *  ROUTINES GESTIONE ERRORI
      * ----------------------------------------------------------------
           COPY IERP9901.
           COPY IERP9902.
      *----------------------------------------------------------------*
      *    ROUTINES DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.
           COPY LCCP0001.
           COPY LCCP0002.

       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LDBM0170.
       AUTHOR.        ATS.
       DATE-WRITTEN   2007.
       DATE-COMPILED.
      * --------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : SERVIZIO ESTRAZIONE OCCORRENZE              *
      *                   DA TABELLA PARAMETRO MOVIMENTO.             *
      * FUNZIONE        : ADEGUAMENTO PREMIO PRESTAZIONE              *
      * --------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *
       77  DESCRIZ-ERR-DB2             PIC X(300)  VALUE SPACES.
       77  WS-BUFFER-TABLE             PIC X(2000) VALUE SPACES.
       77  WS-ID-MOVI-CRZ              PIC 9(09)   VALUE ZEROES.
      *
       01  WS-TIMESTAMP                PIC X(18).
       01  WS-TIMESTAMP-NUM            REDEFINES
           WS-TIMESTAMP                PIC 9(18).
      *
           EXEC SQL INCLUDE IDSV0010 END-EXEC.
      *
      * ---------------------------------------------------------------*
      * STRUTTURA WHERE CONDITION                                      *
      * ---------------------------------------------------------------*
      *
       01  WK-ID-PARAM-MOVI            PIC  9(09).
       01  WS-TP-MOVI                  PIC S9(05) COMP-3.
       01  WS-GARANZIA                 PIC  X(02).
       01  WS-FORMA1                   PIC  X(02).
       01  WS-FORMA2                   PIC  X(02).
       01  WS-DATA-INI                 PIC  X(10).
       01  WS-DATA-END                 PIC  X(10).
       01  WS-DATA-EFF                 PIC  X(10).
       01  WS-DATA-EFF-9               PIC  9(08).
       01  WS-DT-ELAB-DA-DB            PIC  X(10).
       01  WS-DT-ELAB-A-DB             PIC  X(10).
ID67   01  WS-COD-RAMO                 PIC  X(12).
       01  WK-CC-ASSICURATIVO          PIC  X(12) VALUE '312'.
       01  WS-DATA-STRUTTURA.
           03 WS-DATA-STRUT-AA         PIC  9(04).
           03 WS-DATA-STRUT-MM         PIC  9(02).
           03 WS-DATA-STRUT-GG         PIC  9(02).
      *
       01  FLAG-ACCESSO-X-RANGE        PIC X(001) VALUE SPACES.
           88 ACCESSO-X-RANGE-SI                  VALUE 'S'.
           88 ACCESSO-X-RANGE-NO                  VALUE 'N'.

      * ---------------------------------------------------------------*
      * STRUTTURA WHERE CONDITION PROVENIENTE DA SCHEDA PARAMETRO      *
      * ---------------------------------------------------------------*
      *
       01  BUFFER-WHERE-CONDITION.
----->     05 BUFFER-WH-COD-RAMO       PIC  X(12).
      *
      * ---------------------------------------------------------------*
      * STRUTTURA WHERE CONDITION PROVENIENTE DA PRENOTAZIONE SU       *
      * DETTAGLIO RICHIESTA                                            *
      * ---------------------------------------------------------------*
      *
       01  WK-AREA-PRENOTAZIONE.
           COPY LOAC0560 REPLACING ==(SF)== BY ==WPRE==.
      *
      *----------------------------------------------------------------*
      *    TIPOLOGICHE DI PRODOTTO                                     *
      *----------------------------------------------------------------*
      *
           COPY ISPV0000.
      *
      * ---------------------------------------------------------------*
      *  AREA TABELLE DB2                                              *
      * ---------------------------------------------------------------*
      *
      * TABELLA PARAMETRO MOVIMENTO.
           EXEC SQL INCLUDE IDBDPMO0 END-EXEC.
           EXEC SQL INCLUDE IDBVPMO2 END-EXEC.
           EXEC SQL INCLUDE IDBVPMO3 END-EXEC.
      *
           EXEC SQL INCLUDE SQLCA    END-EXEC.
      *
       LINKAGE SECTION.
      *
      * ---------------------------------------------------------------*
      *                  CAMPI DI ESITO, AREE DI SERVIZIO              *
      * ---------------------------------------------------------------*
      *
            EXEC SQL INCLUDE IDSV0003 END-EXEC.
      *
      * ---------------------------------------------------------------*
      *                         AREA STATI                             *
      * ---------------------------------------------------------------*
      *
            EXEC SQL INCLUDE IABV0002 END-EXEC.
      *
       01 WS-ID-POLI           PIC S9(9) COMP-3.
MIGR   01 WS-ID-ADES           PIC S9(9) COMP-3.
      * ---------------------------------------------------------------*
      * AREA DATI    ( BLOB da 1M)
      * ---------------------------------------------------------------*
      *
      *
            EXEC SQL INCLUDE IDBVPMO1 END-EXEC.
      *
       PROCEDURE DIVISION USING IDSV0003
                                IABV0002
ID67                            WS-ID-POLI
MIGR                            WS-ID-ADES
                                PARAM-MOVI.
      *
           PERFORM A000-INIZIO
              THRU A000-EX.
      *
           PERFORM A025-CNTL-INPUT
              THRU A025-EX.

           PERFORM A040-CARICA-WHERE-CONDITION
              THRU A040-EX.

           PERFORM A300-ELABORA
              THRU A300-EX

           PERFORM A350-CTRL-COMMIT
              THRU A350-EX.

           PERFORM A400-FINE
              THRU A400-EX.
      *
           GOBACK.
      *
      * ****************************************************************
      *                         OPERAZIONI INIZIALI                    *
      * ****************************************************************
      *
       A000-INIZIO.
      *
           MOVE 'LDBM0170'
             TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'PARAM-MOVI'
             TO IDSV0003-NOME-TABELLA.
           MOVE '00'
             TO IDSV0003-RETURN-CODE.
           MOVE ZEROES
             TO IDSV0003-SQLCODE
                IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES
             TO IDSV0003-DESCRIZ-ERR-DB2
                IDSV0003-KEY-TABELLA.
           MOVE ZEROES
             TO WS-TIMESTAMP-NUM.

           PERFORM A001-TRATTA-DATE-TIMESTAMP
              THRU A001-EX.
      *
           SET ACCESSO-X-RANGE-NO       TO   TRUE.

       A000-EX.
           EXIT.
      *
      ******************************************************************
       A025-CNTL-INPUT.

           IF IABV0009-ID-OGG-DA IS NUMERIC AND
              IABV0009-ID-OGG-DA NOT = ZEROES

              IF IABV0009-ID-OGG-A  IS NUMERIC AND
                 IABV0009-ID-OGG-A  NOT = ZEROES

                 SET ACCESSO-X-RANGE-SI    TO TRUE

              END-IF

           END-IF.

       A025-EX.
           EXIT.

      * ****************************************************************
      *             VALORIZZAZZIONE CAMPI WHERE CONDITION              *
      * ****************************************************************
      *
       A040-CARICA-WHERE-CONDITION.
      *
      *--> PER CONTO CORRENTE ASSICURATIVO NON ABBIAMO PRENOTAZIONE
      *--> LE INFORMAZIONI CI ARRIVANO DALLA SCHEDA PARAMETRO
      *
           MOVE IDSV0003-BUFFER-WHERE-COND
             TO BUFFER-WHERE-CONDITION.
      *
           MOVE BUFFER-WH-COD-RAMO
             TO ISPV0000-COD-RAMO.
      *
           IF ISPV0000-IN-CC-ASSICURATIVO
      *
              MOVE 'IN'
                TO WS-FORMA1
      *
              MOVE SPACES
                TO WS-FORMA2
      *
              ACCEPT WS-DATA-EFF-9     FROM DATE YYYYMMDD
      *
              MOVE WS-DATA-EFF-9
                TO WS-DATA-STRUTTURA
              MOVE 12
                TO WS-DATA-STRUT-MM
              MOVE 31
                TO WS-DATA-STRUT-GG
              MOVE WS-DATA-STRUTTURA
                TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X
                 THRU Z700-EX
              MOVE WS-DATE-X
                TO WS-DATA-EFF
      *
              MOVE IDSV0003-TIPO-MOVIMENTO
                TO WS-TP-MOVI
      *
              MOVE 'GA'
                TO WS-GARANZIA
      *
              MOVE BUFFER-WH-COD-RAMO
                TO WS-COD-RAMO
      *
           ELSE
      *
      *--> LE INFORMAZIONI CI ARRIVANO DALLA PRENOTAZIONE SULLA
      *--> DETTAGLIO RICHIESTA
      *
              MOVE IABV0009-BLOB-DATA-REC
                TO WPRE-AREA-TAB-BATCH-PRES(1)
      *
              MOVE WPRE-DT-ELAB-A(1)
                TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X
                 THRU Z700-EX
              MOVE WS-DATE-X
                TO WS-DT-ELAB-A-DB
      *
              MOVE WPRE-DT-ELAB-DA(1)
                TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X
                 THRU Z700-EX
              MOVE WS-DATE-X
                TO WS-DT-ELAB-DA-DB
      *
              MOVE WPRE-TP-FRM-ASSVA(1)
                TO WS-FORMA1 WS-FORMA2
      *
              IF WS-FORMA1 = 'EN'
                 MOVE 'IN'
                   TO WS-FORMA1
                 MOVE 'CO'
                   TO WS-FORMA2
              END-IF
      *
22584 *       MOVE WPRE-DT-ELAB-A(1)
  =   *         TO WS-DATE-N
  =   *       PERFORM Z700-DT-N-TO-X
  =   *          THRU Z700-EX
  =   *       MOVE WS-DATE-X
22584         MOVE WS-DT-INFINITO-1
                TO WS-DATA-EFF
      *
              MOVE IDSV0003-TIPO-MOVIMENTO
                TO WS-TP-MOVI
      *
              MOVE 'GA'
                TO WS-GARANZIA
      *
              IF WPRE-RAMO(1) > SPACES
      *
ID67             MOVE WPRE-RAMO(1)
ID67               TO WS-COD-RAMO
      *
              ELSE
      *
      *--> PER L'ELABORAZIONE MASSIVA ESCLUDIAMO LE POLIZZE DI CONTO
      *--> CORRENTE ASSICURATIVO
      *
                 MOVE WK-CC-ASSICURATIVO
                   TO WS-COD-RAMO
      *
              END-IF
      *
           END-IF.
      *
       A040-EX.
           EXIT.
      * ****************************************************************
      * CONTROLLO RETURN CODE
      * ****************************************************************
      *
       A100-CHECK-RETURN-CODE.
      *
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE
                TO IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2
                TO IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                       CONTINUE
                  WHEN +100
                     IF IDSV0003-SELECT        OR
                        IDSV0003-FETCH-FIRST   OR
                        IDSV0003-FETCH-NEXT
                         CONTINUE
                     ELSE
                        SET IDSV0003-SQL-ERROR
                         TO TRUE
                     END-IF
                  WHEN OTHER
                     SET IDSV0003-SQL-ERROR
                       TO TRUE
              END-EVALUATE

           END-IF.
      *
       A100-EX.
           EXIT.
      *
      * ****************************************************************
      * ELABORAZIONE PER ESTRARRE LE OCCORRENZE DA ELABORARE
      * ****************************************************************
      *
       A300-ELABORA.
      *
           EVALUATE TRUE

              WHEN IDSV0003-WHERE-CONDITION-01
CCAS               PERFORM SC01-SELECTION-CURSOR-01 THRU SC01-EX
              WHEN IDSV0003-WHERE-CONDITION-02
                   PERFORM SC02-SELECTION-CURSOR-02 THRU SC02-EX
              WHEN IDSV0003-WHERE-CONDITION-03
ID67               PERFORM SC03-SELECTION-CURSOR-03 THRU SC03-EX
              WHEN IDSV0003-WHERE-CONDITION-04
ID67               PERFORM SC04-SELECTION-CURSOR-04 THRU SC04-EX
              WHEN IDSV0003-WHERE-CONDITION-05
MIGR               PERFORM SC05-SELECTION-CURSOR-05 THRU SC05-EX
              WHEN IDSV0003-WHERE-CONDITION-06
MIGR               PERFORM SC06-SELECTION-CURSOR-06 THRU SC06-EX
              WHEN IDSV0003-WHERE-CONDITION-07
MIGR               PERFORM SC07-SELECTION-CURSOR-07 THRU SC07-EX
              WHEN IDSV0003-WHERE-CONDITION-08
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
              WHEN IDSV0003-WHERE-CONDITION-09
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
              WHEN IDSV0003-WHERE-CONDITION-10
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE

           END-EVALUATE.
      *
       A300-EX.
           EXIT.
      *
      * ****************************************************************
      * DICHIARAZIONE CURSORE PER ESTRAZIONE DATI AD HOC.
      * ****************************************************************
      *
       A301-DECLARE-CURSOR-SC01.
      *
      *--> PER L'ELABORAZIONE DELLE POLIZZE DI CONTO CORRENTE
      *--> ASSICURATIVO
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   DECLARE CUR-PMO-CCAS-RANGE CURSOR WITH HOLD FOR
                 SELECT
                        ID_PARAM_MOVI
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_MOVI
                       ,FRQ_MOVI
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,DT_RICOR_PREC
                       ,DT_RICOR_SUCC
                       ,PC_INTR_FRAZ
                       ,IMP_BNS_DA_SCO_TOT
                       ,IMP_BNS_DA_SCO
                       ,PC_ANTIC_BNS
                       ,TP_RINN_COLL
                       ,TP_RIVAL_PRE
                       ,TP_RIVAL_PRSTZ
                       ,FL_EVID_RIVAL
                       ,ULT_PC_PERD
                       ,TOT_AA_GIA_PROR
                       ,TP_OPZ
                       ,AA_REN_CER
                       ,PC_REVRSB
                       ,IMP_RISC_PARZ_PRGT
                       ,IMP_LRD_DI_RAT
                       ,IB_OGG
                       ,COS_ONER
                       ,SPE_PC
                       ,FL_ATTIV_GAR
                       ,CAMBIO_VER_PROD
                       ,MM_DIFF
                       ,IMP_RAT_MANFEE
                       ,DT_ULT_EROG_MANFEE
                       ,TP_OGG_RIVAL
                       ,SOM_ASSTA_GARAC
                       ,PC_APPLZ_OPZ
                       ,ID_ADES
                       ,ID_POLI
                       ,TP_FRM_ASSVA
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_ESTR_CNT
ID67                   ,COD_RAMO
                       ,GEN_DA_SIN
                       ,COD_TARI
                       ,NUM_RAT_PAG_PRE
                       ,PC_SERV_VAL
                       ,ETA_AA_SOGL_BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA     =
                       :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI           = :WS-TP-MOVI
                   AND TP_OGG            = :WS-GARANZIA
      *            AND ID_POLI   BETWEEN   :IABV0009-ID-OGG-DA AND
                   AND DS_VER    BETWEEN   :IABV0009-ID-OGG-DA AND
                                           :IABV0009-ID-OGG-A
                   AND TP_FRM_ASSVA      = :WS-FORMA1
                   AND DT_RICOR_SUCC     = :WS-DATA-EFF
                   AND DT_INI_EFF       <= :WS-DATA-EFF
                   AND DT_END_EFF       >  :WS-DATA-EFF
                   AND DS_TS_INI_CPTZ   <= :WS-TS-COMPETENZA
                   AND DS_TS_END_CPTZ   >  :WS-TS-COMPETENZA
                   AND DS_STATO_ELAB   IN (
                                           :IABV0002-STATE-01,
                                           :IABV0002-STATE-02,
                                           :IABV0002-STATE-03,
                                           :IABV0002-STATE-04,
                                           :IABV0002-STATE-05,
                                           :IABV0002-STATE-06,
                                           :IABV0002-STATE-07,
                                           :IABV0002-STATE-08,
                                           :IABV0002-STATE-09,
                                           :IABV0002-STATE-10
                                         )
CCAS               AND COD_RAMO = :WS-COD-RAMO
                 ORDER BY TP_FRM_ASSVA,
                          ID_POLI,
                          ID_ADES,
                          DT_RICOR_SUCC
              END-EXEC
              CONTINUE
           ELSE
              EXEC SQL
                   DECLARE CUR-PMO-CCAS CURSOR WITH HOLD FOR
                 SELECT
                        ID_PARAM_MOVI
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_MOVI
                       ,FRQ_MOVI
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,DT_RICOR_PREC
                       ,DT_RICOR_SUCC
                       ,PC_INTR_FRAZ
                       ,IMP_BNS_DA_SCO_TOT
                       ,IMP_BNS_DA_SCO
                       ,PC_ANTIC_BNS
                       ,TP_RINN_COLL
                       ,TP_RIVAL_PRE
                       ,TP_RIVAL_PRSTZ
                       ,FL_EVID_RIVAL
                       ,ULT_PC_PERD
                       ,TOT_AA_GIA_PROR
                       ,TP_OPZ
                       ,AA_REN_CER
                       ,PC_REVRSB
                       ,IMP_RISC_PARZ_PRGT
                       ,IMP_LRD_DI_RAT
                       ,IB_OGG
                       ,COS_ONER
                       ,SPE_PC
                       ,FL_ATTIV_GAR
                       ,CAMBIO_VER_PROD
                       ,MM_DIFF
                       ,IMP_RAT_MANFEE
                       ,DT_ULT_EROG_MANFEE
                       ,TP_OGG_RIVAL
                       ,SOM_ASSTA_GARAC
                       ,PC_APPLZ_OPZ
                       ,ID_ADES
                       ,ID_POLI
                       ,TP_FRM_ASSVA
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_ESTR_CNT
ID67                   ,COD_RAMO
                       ,GEN_DA_SIN
                       ,COD_TARI
                       ,NUM_RAT_PAG_PRE
                       ,PC_SERV_VAL
                       ,ETA_AA_SOGL_BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA     =
                       :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI           = :WS-TP-MOVI
                   AND TP_OGG            = :WS-GARANZIA
                   AND TP_FRM_ASSVA      = :WS-FORMA1
                   AND DT_RICOR_SUCC     = :WS-DATA-EFF
                   AND DT_INI_EFF       <= :WS-DATA-EFF
                   AND DT_END_EFF       >  :WS-DATA-EFF
                   AND DS_TS_INI_CPTZ   <= :WS-TS-COMPETENZA
                   AND DS_TS_END_CPTZ   >  :WS-TS-COMPETENZA
                   AND DS_STATO_ELAB   IN (
                                           :IABV0002-STATE-01,
                                           :IABV0002-STATE-02,
                                           :IABV0002-STATE-03,
                                           :IABV0002-STATE-04,
                                           :IABV0002-STATE-05,
                                           :IABV0002-STATE-06,
                                           :IABV0002-STATE-07,
                                           :IABV0002-STATE-08,
                                           :IABV0002-STATE-09,
                                           :IABV0002-STATE-10
                                         )
CCAS               AND COD_RAMO = :WS-COD-RAMO
                 ORDER BY TP_FRM_ASSVA,
                          ID_POLI,
                          ID_ADES,
                          DT_RICOR_SUCC
              END-EXEC
              CONTINUE
           END-IF.
      *
       A301-SC01-EX.
           EXIT.
      *
      * ****************************************************************
      * DICHIARAZIONE CURSORE PER ESTRAZIONE DATI AD HOC.
      * ****************************************************************
      *
       A305-DECLARE-CURSOR-SC02.
      *
      * SIR FCTVI00011304 STARTS
      *                     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                      >= :WS-DT-ELAB-DA-DB
      *                        AND
      *                       (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                <=   :WS-DT-ELAB-A-DB
      *                      )
      * SIR FCTVI00011304 ENDS
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   DECLARE CUR-PMO-RANGE CURSOR WITH HOLD FOR
                 SELECT
                        ID_PARAM_MOVI
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_MOVI
                       ,FRQ_MOVI
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,DT_RICOR_PREC
                       ,DT_RICOR_SUCC
                       ,PC_INTR_FRAZ
                       ,IMP_BNS_DA_SCO_TOT
                       ,IMP_BNS_DA_SCO
                       ,PC_ANTIC_BNS
                       ,TP_RINN_COLL
                       ,TP_RIVAL_PRE
                       ,TP_RIVAL_PRSTZ
                       ,FL_EVID_RIVAL
                       ,ULT_PC_PERD
                       ,TOT_AA_GIA_PROR
                       ,TP_OPZ
                       ,AA_REN_CER
                       ,PC_REVRSB
                       ,IMP_RISC_PARZ_PRGT
                       ,IMP_LRD_DI_RAT
                       ,IB_OGG
                       ,COS_ONER
                       ,SPE_PC
                       ,FL_ATTIV_GAR
                       ,CAMBIO_VER_PROD
                       ,MM_DIFF
                       ,IMP_RAT_MANFEE
                       ,DT_ULT_EROG_MANFEE
                       ,TP_OGG_RIVAL
                       ,SOM_ASSTA_GARAC
                       ,PC_APPLZ_OPZ
                       ,ID_ADES
                       ,ID_POLI
                       ,TP_FRM_ASSVA
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_ESTR_CNT
ID67                   ,COD_RAMO
                       ,GEN_DA_SIN
                       ,COD_TARI
                       ,NUM_RAT_PAG_PRE
                       ,PC_SERV_VAL
                       ,ETA_AA_SOGL_BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA     =
                      :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI           = :WS-TP-MOVI
                   AND TP_OGG            = :WS-GARANZIA
      *            AND ID_POLI   BETWEEN   :IABV0009-ID-OGG-DA AND
                   AND DS_VER    BETWEEN   :IABV0009-ID-OGG-DA AND
                                           :IABV0009-ID-OGG-A
                   AND TP_FRM_ASSVA    IN (:WS-FORMA1 , :WS-FORMA2)
                   AND     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                                            >= :WS-DT-ELAB-DA-DB
                              AND
                             (DT_RICOR_SUCC + MM_DIFF MONTH)
                                      <=   :WS-DT-ELAB-A-DB
                            )
                   AND DT_INI_EFF       <= :WS-DATA-EFF
                   AND DT_END_EFF       >  :WS-DATA-EFF
22584              AND DS_TS_INI_CPTZ   <= :WS-TS-INFINITO-1
22584              AND DS_TS_END_CPTZ   >  :WS-TS-INFINITO-1
                   AND DS_STATO_ELAB   IN (
                                           :IABV0002-STATE-01,
                                           :IABV0002-STATE-02,
                                           :IABV0002-STATE-03,
                                           :IABV0002-STATE-04,
                                           :IABV0002-STATE-05,
                                           :IABV0002-STATE-06,
                                           :IABV0002-STATE-07,
                                           :IABV0002-STATE-08,
                                           :IABV0002-STATE-09,
                                           :IABV0002-STATE-10
                                         )
      *-   -> PER L'ELABORAZIONE MASSIVA ESCLUDO LE POLIZZE DI CONTO
      *-   -> CORRENTE ASSICURATIVO
CCAS               AND COD_RAMO NOT IN (:WS-COD-RAMO)
                 ORDER BY TP_FRM_ASSVA,
                          ID_POLI,
                          ID_ADES,
                          DT_RICOR_SUCC
              END-EXEC
              CONTINUE
           ELSE
              EXEC SQL
                   DECLARE CUR-PMO CURSOR WITH HOLD FOR
                 SELECT
                        ID_PARAM_MOVI
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_MOVI
                       ,FRQ_MOVI
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,DT_RICOR_PREC
                       ,DT_RICOR_SUCC
                       ,PC_INTR_FRAZ
                       ,IMP_BNS_DA_SCO_TOT
                       ,IMP_BNS_DA_SCO
                       ,PC_ANTIC_BNS
                       ,TP_RINN_COLL
                       ,TP_RIVAL_PRE
                       ,TP_RIVAL_PRSTZ
                       ,FL_EVID_RIVAL
                       ,ULT_PC_PERD
                       ,TOT_AA_GIA_PROR
                       ,TP_OPZ
                       ,AA_REN_CER
                       ,PC_REVRSB
                       ,IMP_RISC_PARZ_PRGT
                       ,IMP_LRD_DI_RAT
                       ,IB_OGG
                       ,COS_ONER
                       ,SPE_PC
                       ,FL_ATTIV_GAR
                       ,CAMBIO_VER_PROD
                       ,MM_DIFF
                       ,IMP_RAT_MANFEE
                       ,DT_ULT_EROG_MANFEE
                       ,TP_OGG_RIVAL
                       ,SOM_ASSTA_GARAC
                       ,PC_APPLZ_OPZ
                       ,ID_ADES
                       ,ID_POLI
                       ,TP_FRM_ASSVA
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_ESTR_CNT
ID67                   ,COD_RAMO
                       ,GEN_DA_SIN
                       ,COD_TARI
                       ,NUM_RAT_PAG_PRE
                       ,PC_SERV_VAL
                       ,ETA_AA_SOGL_BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA     =
                      :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI           = :WS-TP-MOVI
                   AND TP_OGG            = :WS-GARANZIA
                   AND TP_FRM_ASSVA    IN (:WS-FORMA1 , :WS-FORMA2)
                   AND     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                                            >= :WS-DT-ELAB-DA-DB
                              AND
                             (DT_RICOR_SUCC + MM_DIFF MONTH)
                                      <=   :WS-DT-ELAB-A-DB
                            )
                   AND DT_INI_EFF       <= :WS-DATA-EFF
                   AND DT_END_EFF       >  :WS-DATA-EFF
22584              AND DS_TS_INI_CPTZ   <= :WS-TS-INFINITO-1
22584              AND DS_TS_END_CPTZ   >  :WS-TS-INFINITO-1
                   AND DS_STATO_ELAB   IN (
                                           :IABV0002-STATE-01,
                                           :IABV0002-STATE-02,
                                           :IABV0002-STATE-03,
                                           :IABV0002-STATE-04,
                                           :IABV0002-STATE-05,
                                           :IABV0002-STATE-06,
                                           :IABV0002-STATE-07,
                                           :IABV0002-STATE-08,
                                           :IABV0002-STATE-09,
                                           :IABV0002-STATE-10
                                         )
      *-   -> PER L'ELABORAZIONE MASSIVA ESCLUDO LE POLIZZE DI CONTO
      *-   -> CORRENTE ASSICURATIVO
CCAS               AND COD_RAMO NOT IN (:WS-COD-RAMO)
                 ORDER BY TP_FRM_ASSVA,
                          ID_POLI,
                          ID_ADES,
                          DT_RICOR_SUCC
              END-EXEC
              CONTINUE
           END-IF.
      *
       A305-SC02-EX.
           EXIT.
      *
      * ****************************************************************
      * DICHIARAZIONE CURSORE PER ESTRAZIONE DATI AD HOC.
      * ****************************************************************
      *
      * SIR FCTVI00011304 STARTS
      *                     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                      >= :WS-DT-ELAB-DA-DB
      *                        AND
      *                       (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                <=   :WS-DT-ELAB-A-DB
      *                      )
      * SIR FCTVI00011304 ENDS
ID67   A305-DECLARE-CURSOR-SC03.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   DECLARE CUR-PMO-RAMO-RANGE CURSOR WITH HOLD FOR
                 SELECT
                        ID_PARAM_MOVI
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_MOVI
                       ,FRQ_MOVI
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,DT_RICOR_PREC
                       ,DT_RICOR_SUCC
                       ,PC_INTR_FRAZ
                       ,IMP_BNS_DA_SCO_TOT
                       ,IMP_BNS_DA_SCO
                       ,PC_ANTIC_BNS
                       ,TP_RINN_COLL
                       ,TP_RIVAL_PRE
                       ,TP_RIVAL_PRSTZ
                       ,FL_EVID_RIVAL
                       ,ULT_PC_PERD
                       ,TOT_AA_GIA_PROR
                       ,TP_OPZ
                       ,AA_REN_CER
                       ,PC_REVRSB
                       ,IMP_RISC_PARZ_PRGT
                       ,IMP_LRD_DI_RAT
                       ,IB_OGG
                       ,COS_ONER
                       ,SPE_PC
                       ,FL_ATTIV_GAR
                       ,CAMBIO_VER_PROD
                       ,MM_DIFF
                       ,IMP_RAT_MANFEE
                       ,DT_ULT_EROG_MANFEE
                       ,TP_OGG_RIVAL
                       ,SOM_ASSTA_GARAC
                       ,PC_APPLZ_OPZ
                       ,ID_ADES
                       ,ID_POLI
                       ,TP_FRM_ASSVA
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_ESTR_CNT
ID67                   ,COD_RAMO
                       ,GEN_DA_SIN
                       ,COD_TARI
                       ,NUM_RAT_PAG_PRE
                       ,PC_SERV_VAL
                       ,ETA_AA_SOGL_BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA     =
                       :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI           = :WS-TP-MOVI
                   AND TP_OGG            = :WS-GARANZIA
      *            AND ID_POLI   BETWEEN   :IABV0009-ID-OGG-DA AND
                   AND DS_VER    BETWEEN   :IABV0009-ID-OGG-DA AND
                                           :IABV0009-ID-OGG-A
                   AND TP_FRM_ASSVA    IN (:WS-FORMA1 , :WS-FORMA2)
                   AND     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                                            >= :WS-DT-ELAB-DA-DB
                              AND
                             (DT_RICOR_SUCC + MM_DIFF MONTH)
                                      <=   :WS-DT-ELAB-A-DB
                            )
                   AND DT_INI_EFF       <= :WS-DATA-EFF
                   AND DT_END_EFF       >  :WS-DATA-EFF
22584              AND DS_TS_INI_CPTZ   <= :WS-TS-INFINITO-1
22584              AND DS_TS_END_CPTZ   >  :WS-TS-INFINITO-1
                   AND DS_STATO_ELAB   IN (
                                           :IABV0002-STATE-01,
                                           :IABV0002-STATE-02,
                                           :IABV0002-STATE-03,
                                           :IABV0002-STATE-04,
                                           :IABV0002-STATE-05,
                                           :IABV0002-STATE-06,
                                           :IABV0002-STATE-07,
                                           :IABV0002-STATE-08,
                                           :IABV0002-STATE-09,
                                           :IABV0002-STATE-10
                                         )
ID67               AND COD_RAMO = :WS-COD-RAMO
                 ORDER BY TP_FRM_ASSVA,
                          ID_POLI,
                          ID_ADES,
                          DT_RICOR_SUCC
              END-EXEC
              CONTINUE
           ELSE
              EXEC SQL
                   DECLARE CUR-PMO-RAMO CURSOR WITH HOLD FOR
                 SELECT
                        ID_PARAM_MOVI
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_MOVI
                       ,FRQ_MOVI
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,DT_RICOR_PREC
                       ,DT_RICOR_SUCC
                       ,PC_INTR_FRAZ
                       ,IMP_BNS_DA_SCO_TOT
                       ,IMP_BNS_DA_SCO
                       ,PC_ANTIC_BNS
                       ,TP_RINN_COLL
                       ,TP_RIVAL_PRE
                       ,TP_RIVAL_PRSTZ
                       ,FL_EVID_RIVAL
                       ,ULT_PC_PERD
                       ,TOT_AA_GIA_PROR
                       ,TP_OPZ
                       ,AA_REN_CER
                       ,PC_REVRSB
                       ,IMP_RISC_PARZ_PRGT
                       ,IMP_LRD_DI_RAT
                       ,IB_OGG
                       ,COS_ONER
                       ,SPE_PC
                       ,FL_ATTIV_GAR
                       ,CAMBIO_VER_PROD
                       ,MM_DIFF
                       ,IMP_RAT_MANFEE
                       ,DT_ULT_EROG_MANFEE
                       ,TP_OGG_RIVAL
                       ,SOM_ASSTA_GARAC
                       ,PC_APPLZ_OPZ
                       ,ID_ADES
                       ,ID_POLI
                       ,TP_FRM_ASSVA
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_ESTR_CNT
ID67                   ,COD_RAMO
                       ,GEN_DA_SIN
                       ,COD_TARI
                       ,NUM_RAT_PAG_PRE
                       ,PC_SERV_VAL
                       ,ETA_AA_SOGL_BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA     =
                       :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI           = :WS-TP-MOVI
                   AND TP_OGG            = :WS-GARANZIA
                   AND TP_FRM_ASSVA    IN (:WS-FORMA1 , :WS-FORMA2)
                   AND     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                                            >= :WS-DT-ELAB-DA-DB
                              AND
                             (DT_RICOR_SUCC + MM_DIFF MONTH)
                                      <=   :WS-DT-ELAB-A-DB
                            )
                   AND DT_INI_EFF       <= :WS-DATA-EFF
                   AND DT_END_EFF       >  :WS-DATA-EFF
22584              AND DS_TS_INI_CPTZ   <= :WS-TS-INFINITO-1
22584              AND DS_TS_END_CPTZ   >  :WS-TS-INFINITO-1
                   AND DS_STATO_ELAB   IN (
                                           :IABV0002-STATE-01,
                                           :IABV0002-STATE-02,
                                           :IABV0002-STATE-03,
                                           :IABV0002-STATE-04,
                                           :IABV0002-STATE-05,
                                           :IABV0002-STATE-06,
                                           :IABV0002-STATE-07,
                                           :IABV0002-STATE-08,
                                           :IABV0002-STATE-09,
                                           :IABV0002-STATE-10
                                         )
ID67               AND COD_RAMO = :WS-COD-RAMO
                 ORDER BY TP_FRM_ASSVA,
                          ID_POLI,
                          ID_ADES,
                          DT_RICOR_SUCC
              END-EXEC
              CONTINUE
           END-IF.
      *
       A305-SC03-EX.
           EXIT.
      *
      * ****************************************************************
      * DICHIARAZIONE CURSORE PER ESTRAZIONE DATI AD HOC.
      * ****************************************************************
      *
      * SIR FCTVI00011304 STARTS
      *                     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                      >= :WS-DT-ELAB-DA-DB
      *                        AND
      *                       (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                <=   :WS-DT-ELAB-A-DB
      *                      )
      * SIR FCTVI00011304 ENDS
ID67   A305-DECLARE-CURSOR-SC04.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   DECLARE CUR-PMO-POL-RANGE   CURSOR WITH HOLD FOR
                 SELECT
                        ID_PARAM_MOVI
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_MOVI
                       ,FRQ_MOVI
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,DT_RICOR_PREC
                       ,DT_RICOR_SUCC
                       ,PC_INTR_FRAZ
                       ,IMP_BNS_DA_SCO_TOT
                       ,IMP_BNS_DA_SCO
                       ,PC_ANTIC_BNS
                       ,TP_RINN_COLL
                       ,TP_RIVAL_PRE
                       ,TP_RIVAL_PRSTZ
                       ,FL_EVID_RIVAL
                       ,ULT_PC_PERD
                       ,TOT_AA_GIA_PROR
                       ,TP_OPZ
                       ,AA_REN_CER
                       ,PC_REVRSB
                       ,IMP_RISC_PARZ_PRGT
                       ,IMP_LRD_DI_RAT
                       ,IB_OGG
                       ,COS_ONER
                       ,SPE_PC
                       ,FL_ATTIV_GAR
                       ,CAMBIO_VER_PROD
                       ,MM_DIFF
                       ,IMP_RAT_MANFEE
                       ,DT_ULT_EROG_MANFEE
                       ,TP_OGG_RIVAL
                       ,SOM_ASSTA_GARAC
                       ,PC_APPLZ_OPZ
                       ,ID_ADES
                       ,ID_POLI
                       ,TP_FRM_ASSVA
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_ESTR_CNT
ID67                   ,COD_RAMO
                       ,GEN_DA_SIN
                       ,COD_TARI
                       ,NUM_RAT_PAG_PRE
                       ,PC_SERV_VAL
                       ,ETA_AA_SOGL_BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA     =
                       :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI           = :WS-TP-MOVI
                   AND TP_OGG            = :WS-GARANZIA
      *            AND ID_POLI   BETWEEN   :IABV0009-ID-OGG-DA AND
                   AND DS_VER    BETWEEN   :IABV0009-ID-OGG-DA AND
                                           :IABV0009-ID-OGG-A
                   AND TP_FRM_ASSVA    IN (:WS-FORMA1 , :WS-FORMA2)
                   AND     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                                            >= :WS-DT-ELAB-DA-DB
                              AND
                             (DT_RICOR_SUCC + MM_DIFF MONTH)
                                      <=   :WS-DT-ELAB-A-DB
                            )
                   AND DT_INI_EFF       <= :WS-DATA-EFF
                   AND DT_END_EFF       >  :WS-DATA-EFF
22584              AND DS_TS_INI_CPTZ   <= :WS-TS-INFINITO-1
22584              AND DS_TS_END_CPTZ   >  :WS-TS-INFINITO-1
                   AND DS_STATO_ELAB   IN (
                                           :IABV0002-STATE-01,
                                           :IABV0002-STATE-02,
                                           :IABV0002-STATE-03,
                                           :IABV0002-STATE-04,
                                           :IABV0002-STATE-05,
                                           :IABV0002-STATE-06,
                                           :IABV0002-STATE-07,
                                           :IABV0002-STATE-08,
                                           :IABV0002-STATE-09,
                                           :IABV0002-STATE-10
                                         )
ID67               AND ID_POLI = :WS-ID-POLI
                 ORDER BY TP_FRM_ASSVA,
                          ID_POLI,
                          ID_ADES,
                          DT_RICOR_SUCC
              END-EXEC
              CONTINUE
           ELSE
              EXEC SQL
                   DECLARE CUR-PMO-POL   CURSOR WITH HOLD FOR
                 SELECT
                        ID_PARAM_MOVI
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_MOVI
                       ,FRQ_MOVI
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,DT_RICOR_PREC
                       ,DT_RICOR_SUCC
                       ,PC_INTR_FRAZ
                       ,IMP_BNS_DA_SCO_TOT
                       ,IMP_BNS_DA_SCO
                       ,PC_ANTIC_BNS
                       ,TP_RINN_COLL
                       ,TP_RIVAL_PRE
                       ,TP_RIVAL_PRSTZ
                       ,FL_EVID_RIVAL
                       ,ULT_PC_PERD
                       ,TOT_AA_GIA_PROR
                       ,TP_OPZ
                       ,AA_REN_CER
                       ,PC_REVRSB
                       ,IMP_RISC_PARZ_PRGT
                       ,IMP_LRD_DI_RAT
                       ,IB_OGG
                       ,COS_ONER
                       ,SPE_PC
                       ,FL_ATTIV_GAR
                       ,CAMBIO_VER_PROD
                       ,MM_DIFF
                       ,IMP_RAT_MANFEE
                       ,DT_ULT_EROG_MANFEE
                       ,TP_OGG_RIVAL
                       ,SOM_ASSTA_GARAC
                       ,PC_APPLZ_OPZ
                       ,ID_ADES
                       ,ID_POLI
                       ,TP_FRM_ASSVA
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_ESTR_CNT
ID67                   ,COD_RAMO
                       ,GEN_DA_SIN
                       ,COD_TARI
                       ,NUM_RAT_PAG_PRE
                       ,PC_SERV_VAL
                       ,ETA_AA_SOGL_BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA     =
                       :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI           = :WS-TP-MOVI
                   AND TP_OGG            = :WS-GARANZIA
                   AND TP_FRM_ASSVA    IN (:WS-FORMA1 , :WS-FORMA2)
                   AND     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                                            >= :WS-DT-ELAB-DA-DB
                              AND
                             (DT_RICOR_SUCC + MM_DIFF MONTH)
                                      <=   :WS-DT-ELAB-A-DB
                            )
                   AND DT_INI_EFF       <= :WS-DATA-EFF
                   AND DT_END_EFF       >  :WS-DATA-EFF
22584              AND DS_TS_INI_CPTZ   <= :WS-TS-INFINITO-1
22584              AND DS_TS_END_CPTZ   >  :WS-TS-INFINITO-1
                   AND DS_STATO_ELAB   IN (
                                           :IABV0002-STATE-01,
                                           :IABV0002-STATE-02,
                                           :IABV0002-STATE-03,
                                           :IABV0002-STATE-04,
                                           :IABV0002-STATE-05,
                                           :IABV0002-STATE-06,
                                           :IABV0002-STATE-07,
                                           :IABV0002-STATE-08,
                                           :IABV0002-STATE-09,
                                           :IABV0002-STATE-10
                                         )
ID67               AND ID_POLI = :WS-ID-POLI
                 ORDER BY TP_FRM_ASSVA,
                          ID_POLI,
                          ID_ADES,
                          DT_RICOR_SUCC
              END-EXEC
              CONTINUE
           END-IF.
      *
       A305-SC04-EX.
           EXIT.
      *
      * ****************************************************************
      * DICHIARAZIONE CURSORE PER ESTRAZIONE DATI AD HOC.
      * ****************************************************************
      *
MIGR   A305-DECLARE-CURSOR-SC05.
      *
      * SIR FCTVI00011304 STARTS
      *                     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                      >= :WS-DT-ELAB-DA-DB
      *                        AND
      *                       (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                <=   :WS-DT-ELAB-A-DB
      *                      )
      * SIR FCTVI00011304 ENDS
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   DECLARE CUR-PMO-RANGE-C CURSOR WITH HOLD FOR
                 SELECT
                        ID_PARAM_MOVI
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_MOVI
                       ,FRQ_MOVI
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,DT_RICOR_PREC
                       ,DT_RICOR_SUCC
                       ,PC_INTR_FRAZ
                       ,IMP_BNS_DA_SCO_TOT
                       ,IMP_BNS_DA_SCO
                       ,PC_ANTIC_BNS
                       ,TP_RINN_COLL
                       ,TP_RIVAL_PRE
                       ,TP_RIVAL_PRSTZ
                       ,FL_EVID_RIVAL
                       ,ULT_PC_PERD
                       ,TOT_AA_GIA_PROR
                       ,TP_OPZ
                       ,AA_REN_CER
                       ,PC_REVRSB
                       ,IMP_RISC_PARZ_PRGT
                       ,IMP_LRD_DI_RAT
                       ,IB_OGG
                       ,COS_ONER
                       ,SPE_PC
                       ,FL_ATTIV_GAR
                       ,CAMBIO_VER_PROD
                       ,MM_DIFF
                       ,IMP_RAT_MANFEE
                       ,DT_ULT_EROG_MANFEE
                       ,TP_OGG_RIVAL
                       ,SOM_ASSTA_GARAC
                       ,PC_APPLZ_OPZ
                       ,ID_ADES
                       ,ID_POLI
                       ,TP_FRM_ASSVA
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_ESTR_CNT
ID67                   ,COD_RAMO
                       ,GEN_DA_SIN
                       ,COD_TARI
                       ,NUM_RAT_PAG_PRE
                       ,PC_SERV_VAL
                       ,ETA_AA_SOGL_BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA     =
                      :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI           = :WS-TP-MOVI
                   AND TP_OGG            = :WS-GARANZIA
                   AND ID_ADES   BETWEEN   :IABV0009-ID-OGG-DA AND
                                           :IABV0009-ID-OGG-A
                   AND TP_FRM_ASSVA    IN (:WS-FORMA1 , :WS-FORMA2)
                   AND     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                                            >= :WS-DT-ELAB-DA-DB
                              AND
                             (DT_RICOR_SUCC + MM_DIFF MONTH)
                                      <=   :WS-DT-ELAB-A-DB
                            )
                   AND DT_INI_EFF       <= :WS-DATA-EFF
                   AND DT_END_EFF       >  :WS-DATA-EFF
22584              AND DS_TS_INI_CPTZ   <= :WS-TS-INFINITO-1
22584              AND DS_TS_END_CPTZ   >  :WS-TS-INFINITO-1
                   AND DS_STATO_ELAB   IN (
                                           :IABV0002-STATE-01,
                                           :IABV0002-STATE-02,
                                           :IABV0002-STATE-03,
                                           :IABV0002-STATE-04,
                                           :IABV0002-STATE-05,
                                           :IABV0002-STATE-06,
                                           :IABV0002-STATE-07,
                                           :IABV0002-STATE-08,
                                           :IABV0002-STATE-09,
                                           :IABV0002-STATE-10
                                         )
      *-   -> PER L'ELABORAZIONE MASSIVA ESCLUDO LE POLIZZE DI CONTO
      *-   -> CORRENTE ASSICURATIVO
CCAS               AND COD_RAMO NOT IN (:WS-COD-RAMO)
                 ORDER BY TP_FRM_ASSVA,
                          ID_POLI,
                          ID_ADES,
                          DT_RICOR_SUCC
              END-EXEC
              CONTINUE
           ELSE
              EXEC SQL
                   DECLARE CUR-PMO-C CURSOR WITH HOLD FOR
                 SELECT
                        ID_PARAM_MOVI
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_MOVI
                       ,FRQ_MOVI
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,DT_RICOR_PREC
                       ,DT_RICOR_SUCC
                       ,PC_INTR_FRAZ
                       ,IMP_BNS_DA_SCO_TOT
                       ,IMP_BNS_DA_SCO
                       ,PC_ANTIC_BNS
                       ,TP_RINN_COLL
                       ,TP_RIVAL_PRE
                       ,TP_RIVAL_PRSTZ
                       ,FL_EVID_RIVAL
                       ,ULT_PC_PERD
                       ,TOT_AA_GIA_PROR
                       ,TP_OPZ
                       ,AA_REN_CER
                       ,PC_REVRSB
                       ,IMP_RISC_PARZ_PRGT
                       ,IMP_LRD_DI_RAT
                       ,IB_OGG
                       ,COS_ONER
                       ,SPE_PC
                       ,FL_ATTIV_GAR
                       ,CAMBIO_VER_PROD
                       ,MM_DIFF
                       ,IMP_RAT_MANFEE
                       ,DT_ULT_EROG_MANFEE
                       ,TP_OGG_RIVAL
                       ,SOM_ASSTA_GARAC
                       ,PC_APPLZ_OPZ
                       ,ID_ADES
                       ,ID_POLI
                       ,TP_FRM_ASSVA
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_ESTR_CNT
ID67                   ,COD_RAMO
                       ,GEN_DA_SIN
                       ,COD_TARI
                       ,NUM_RAT_PAG_PRE
                       ,PC_SERV_VAL
                       ,ETA_AA_SOGL_BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA     =
                      :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI           = :WS-TP-MOVI
                   AND TP_OGG            = :WS-GARANZIA
                   AND TP_FRM_ASSVA    IN (:WS-FORMA1 , :WS-FORMA2)
                   AND     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                                            >= :WS-DT-ELAB-DA-DB
                              AND
                             (DT_RICOR_SUCC + MM_DIFF MONTH)
                                      <=   :WS-DT-ELAB-A-DB
                            )
                   AND DT_INI_EFF       <= :WS-DATA-EFF
                   AND DT_END_EFF       >  :WS-DATA-EFF
22584              AND DS_TS_INI_CPTZ   <= :WS-TS-INFINITO-1
22584              AND DS_TS_END_CPTZ   >  :WS-TS-INFINITO-1
                   AND DS_STATO_ELAB   IN (
                                           :IABV0002-STATE-01,
                                           :IABV0002-STATE-02,
                                           :IABV0002-STATE-03,
                                           :IABV0002-STATE-04,
                                           :IABV0002-STATE-05,
                                           :IABV0002-STATE-06,
                                           :IABV0002-STATE-07,
                                           :IABV0002-STATE-08,
                                           :IABV0002-STATE-09,
                                           :IABV0002-STATE-10
                                         )
      *-   -> PER L'ELABORAZIONE MASSIVA ESCLUDO LE POLIZZE DI CONTO
      *-   -> CORRENTE ASSICURATIVO
CCAS               AND COD_RAMO NOT IN (:WS-COD-RAMO)
                 ORDER BY TP_FRM_ASSVA,
                          ID_POLI,
                          ID_ADES,
                          DT_RICOR_SUCC
              END-EXEC
              CONTINUE
           END-IF.
      *
       A305-SC05-EX.
           EXIT.
      *
      * ****************************************************************
      * DICHIARAZIONE CURSORE PER ESTRAZIONE DATI AD HOC.
      * ****************************************************************
      *
      * SIR FCTVI00011304 STARTS
      *                     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                      >= :WS-DT-ELAB-DA-DB
      *                        AND
      *                       (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                <=   :WS-DT-ELAB-A-DB
      *                      )
      * SIR FCTVI00011304 ENDS
MIGR   A305-DECLARE-CURSOR-SC06.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   DECLARE CUR-PMO-RAMO-RANGE-C CURSOR WITH HOLD FOR
                 SELECT
                        ID_PARAM_MOVI
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_MOVI
                       ,FRQ_MOVI
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,DT_RICOR_PREC
                       ,DT_RICOR_SUCC
                       ,PC_INTR_FRAZ
                       ,IMP_BNS_DA_SCO_TOT
                       ,IMP_BNS_DA_SCO
                       ,PC_ANTIC_BNS
                       ,TP_RINN_COLL
                       ,TP_RIVAL_PRE
                       ,TP_RIVAL_PRSTZ
                       ,FL_EVID_RIVAL
                       ,ULT_PC_PERD
                       ,TOT_AA_GIA_PROR
                       ,TP_OPZ
                       ,AA_REN_CER
                       ,PC_REVRSB
                       ,IMP_RISC_PARZ_PRGT
                       ,IMP_LRD_DI_RAT
                       ,IB_OGG
                       ,COS_ONER
                       ,SPE_PC
                       ,FL_ATTIV_GAR
                       ,CAMBIO_VER_PROD
                       ,MM_DIFF
                       ,IMP_RAT_MANFEE
                       ,DT_ULT_EROG_MANFEE
                       ,TP_OGG_RIVAL
                       ,SOM_ASSTA_GARAC
                       ,PC_APPLZ_OPZ
                       ,ID_ADES
                       ,ID_POLI
                       ,TP_FRM_ASSVA
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_ESTR_CNT
ID67                   ,COD_RAMO
                       ,GEN_DA_SIN
                       ,COD_TARI
                       ,NUM_RAT_PAG_PRE
                       ,PC_SERV_VAL
                       ,ETA_AA_SOGL_BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA     =
                       :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI           = :WS-TP-MOVI
                   AND TP_OGG            = :WS-GARANZIA
                   AND ID_ADES   BETWEEN   :IABV0009-ID-OGG-DA AND
                                           :IABV0009-ID-OGG-A
                   AND TP_FRM_ASSVA    IN (:WS-FORMA1 , :WS-FORMA2)
                   AND     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                                            >= :WS-DT-ELAB-DA-DB
                              AND
                             (DT_RICOR_SUCC + MM_DIFF MONTH)
                                      <=   :WS-DT-ELAB-A-DB
                            )
                   AND DT_INI_EFF       <= :WS-DATA-EFF
                   AND DT_END_EFF       >  :WS-DATA-EFF
22584              AND DS_TS_INI_CPTZ   <= :WS-TS-INFINITO-1
22584              AND DS_TS_END_CPTZ   >  :WS-TS-INFINITO-1
                   AND DS_STATO_ELAB   IN (
                                           :IABV0002-STATE-01,
                                           :IABV0002-STATE-02,
                                           :IABV0002-STATE-03,
                                           :IABV0002-STATE-04,
                                           :IABV0002-STATE-05,
                                           :IABV0002-STATE-06,
                                           :IABV0002-STATE-07,
                                           :IABV0002-STATE-08,
                                           :IABV0002-STATE-09,
                                           :IABV0002-STATE-10
                                         )
ID67               AND COD_RAMO = :WS-COD-RAMO
                 ORDER BY TP_FRM_ASSVA,
                          ID_POLI,
                          ID_ADES,
                          DT_RICOR_SUCC
              END-EXEC
              CONTINUE
           ELSE
              EXEC SQL
                   DECLARE CUR-PMO-RAMO-C CURSOR WITH HOLD FOR
                 SELECT
                        ID_PARAM_MOVI
                       ,ID_OGG
                       ,TP_OGG
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_MOVI
                       ,FRQ_MOVI
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,DT_RICOR_PREC
                       ,DT_RICOR_SUCC
                       ,PC_INTR_FRAZ
                       ,IMP_BNS_DA_SCO_TOT
                       ,IMP_BNS_DA_SCO
                       ,PC_ANTIC_BNS
                       ,TP_RINN_COLL
                       ,TP_RIVAL_PRE
                       ,TP_RIVAL_PRSTZ
                       ,FL_EVID_RIVAL
                       ,ULT_PC_PERD
                       ,TOT_AA_GIA_PROR
                       ,TP_OPZ
                       ,AA_REN_CER
                       ,PC_REVRSB
                       ,IMP_RISC_PARZ_PRGT
                       ,IMP_LRD_DI_RAT
                       ,IB_OGG
                       ,COS_ONER
                       ,SPE_PC
                       ,FL_ATTIV_GAR
                       ,CAMBIO_VER_PROD
                       ,MM_DIFF
                       ,IMP_RAT_MANFEE
                       ,DT_ULT_EROG_MANFEE
                       ,TP_OGG_RIVAL
                       ,SOM_ASSTA_GARAC
                       ,PC_APPLZ_OPZ
                       ,ID_ADES
                       ,ID_POLI
                       ,TP_FRM_ASSVA
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_ESTR_CNT
ID67                   ,COD_RAMO
                       ,GEN_DA_SIN
                       ,COD_TARI
                       ,NUM_RAT_PAG_PRE
                       ,PC_SERV_VAL
                       ,ETA_AA_SOGL_BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA     =
                       :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI           = :WS-TP-MOVI
                   AND TP_OGG            = :WS-GARANZIA
                   AND TP_FRM_ASSVA    IN (:WS-FORMA1 , :WS-FORMA2)
                   AND     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                                            >= :WS-DT-ELAB-DA-DB
                              AND
                             (DT_RICOR_SUCC + MM_DIFF MONTH)
                                      <=   :WS-DT-ELAB-A-DB
                            )
                   AND DT_INI_EFF       <= :WS-DATA-EFF
                   AND DT_END_EFF       >  :WS-DATA-EFF
22584              AND DS_TS_INI_CPTZ   <= :WS-TS-INFINITO-1
22584              AND DS_TS_END_CPTZ   >  :WS-TS-INFINITO-1
                   AND DS_STATO_ELAB   IN (
                                           :IABV0002-STATE-01,
                                           :IABV0002-STATE-02,
                                           :IABV0002-STATE-03,
                                           :IABV0002-STATE-04,
                                           :IABV0002-STATE-05,
                                           :IABV0002-STATE-06,
                                           :IABV0002-STATE-07,
                                           :IABV0002-STATE-08,
                                           :IABV0002-STATE-09,
                                           :IABV0002-STATE-10
                                         )
ID67               AND COD_RAMO = :WS-COD-RAMO
                 ORDER BY TP_FRM_ASSVA,
                          ID_POLI,
                          ID_ADES,
                          DT_RICOR_SUCC
              END-EXEC
              CONTINUE
           END-IF.
      *
       A305-SC06-EX.
           EXIT.
      *
      * ****************************************************************
      * DICHIARAZIONE CURSORE PER ESTRAZIONE DATI AD HOC.
      * ****************************************************************
      *
      * SIR FCTVI00011304 STARTS
      *                     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                      >= :WS-DT-ELAB-DA-DB
      *                        AND
      *                       (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                <=   :WS-DT-ELAB-A-DB
      *                      )
      * SIR FCTVI00011304 ENDS
MIGR   A305-DECLARE-CURSOR-SC07.
      *
           EXEC SQL
                DECLARE CUR-PMO-POL-C CURSOR WITH HOLD FOR
              SELECT
                     ID_PARAM_MOVI
                    ,ID_OGG
                    ,TP_OGG
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_MOVI
                    ,FRQ_MOVI
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,DT_RICOR_PREC
                    ,DT_RICOR_SUCC
                    ,PC_INTR_FRAZ
                    ,IMP_BNS_DA_SCO_TOT
                    ,IMP_BNS_DA_SCO
                    ,PC_ANTIC_BNS
                    ,TP_RINN_COLL
                    ,TP_RIVAL_PRE
                    ,TP_RIVAL_PRSTZ
                    ,FL_EVID_RIVAL
                    ,ULT_PC_PERD
                    ,TOT_AA_GIA_PROR
                    ,TP_OPZ
                    ,AA_REN_CER
                    ,PC_REVRSB
                    ,IMP_RISC_PARZ_PRGT
                    ,IMP_LRD_DI_RAT
                    ,IB_OGG
                    ,COS_ONER
                    ,SPE_PC
                    ,FL_ATTIV_GAR
                    ,CAMBIO_VER_PROD
                    ,MM_DIFF
                    ,IMP_RAT_MANFEE
                    ,DT_ULT_EROG_MANFEE
                    ,TP_OGG_RIVAL
                    ,SOM_ASSTA_GARAC
                    ,PC_APPLZ_OPZ
                    ,ID_ADES
                    ,ID_POLI
                    ,TP_FRM_ASSVA
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TP_ESTR_CNT
ID67                ,COD_RAMO
                    ,GEN_DA_SIN
                    ,COD_TARI
                    ,NUM_RAT_PAG_PRE
                    ,PC_SERV_VAL
                    ,ETA_AA_SOGL_BNFICR
              FROM PARAM_MOVI
              WHERE COD_COMP_ANIA        =
                    :IDSV0003-CODICE-COMPAGNIA-ANIA
                AND TP_MOVI              = :WS-TP-MOVI
                AND TP_OGG               = :WS-GARANZIA
                AND TP_FRM_ASSVA       IN (:WS-FORMA1 , :WS-FORMA2)
                AND        ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                                         >= :WS-DT-ELAB-DA-DB
                           AND
                          (DT_RICOR_SUCC + MM_DIFF MONTH)
                                   <=      :WS-DT-ELAB-A-DB
                         )
                AND DT_INI_EFF          <= :WS-DATA-EFF
                AND DT_END_EFF          >  :WS-DATA-EFF
22584           AND DS_TS_INI_CPTZ      <= :WS-TS-INFINITO-1
22584           AND DS_TS_END_CPTZ      >  :WS-TS-INFINITO-1
                AND DS_STATO_ELAB      IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                      )
ID67            AND ID_ADES = :WS-ID-ADES
              ORDER BY TP_FRM_ASSVA,
                       ID_POLI,
                       ID_ADES,
                       DT_RICOR_SUCC
           END-EXEC.
      *
       A305-SC07-EX.
           EXIT.
      *
      * ****************************************************************
      *
      * ****************************************************************
       A311-SELECT-SC01.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                SELECT
                   ID_PARAM_MOVI
                  ,ID_OGG
                  ,TP_OGG
                  ,ID_MOVI_CRZ
                  ,ID_MOVI_CHIU
                  ,DT_INI_EFF
                  ,DT_END_EFF
                  ,COD_COMP_ANIA
                  ,TP_MOVI
                  ,FRQ_MOVI
                  ,DUR_AA
                  ,DUR_MM
                  ,DUR_GG
                  ,DT_RICOR_PREC
                  ,DT_RICOR_SUCC
                  ,PC_INTR_FRAZ
                  ,IMP_BNS_DA_SCO_TOT
                  ,IMP_BNS_DA_SCO
                  ,PC_ANTIC_BNS
                  ,TP_RINN_COLL
                  ,TP_RIVAL_PRE
                  ,TP_RIVAL_PRSTZ
                  ,FL_EVID_RIVAL
                  ,ULT_PC_PERD
                  ,TOT_AA_GIA_PROR
                  ,TP_OPZ
                  ,AA_REN_CER
                  ,PC_REVRSB
                  ,IMP_RISC_PARZ_PRGT
                  ,IMP_LRD_DI_RAT
                  ,IB_OGG
                  ,COS_ONER
                  ,SPE_PC
                  ,FL_ATTIV_GAR
                  ,CAMBIO_VER_PROD
                  ,MM_DIFF
                  ,IMP_RAT_MANFEE
                  ,DT_ULT_EROG_MANFEE
                  ,TP_OGG_RIVAL
                  ,SOM_ASSTA_GARAC
                  ,PC_APPLZ_OPZ
                  ,ID_ADES
                  ,ID_POLI
                  ,TP_FRM_ASSVA
                  ,DS_RIGA
                  ,DS_OPER_SQL
                  ,DS_VER
                  ,DS_TS_INI_CPTZ
                  ,DS_TS_END_CPTZ
                  ,DS_UTENTE
                  ,DS_STATO_ELAB
                  ,TP_ESTR_CNT
ID67              ,COD_RAMO
                  ,GEN_DA_SIN
                  ,COD_TARI
                  ,NUM_RAT_PAG_PRE
                  ,PC_SERV_VAL
                  ,ETA_AA_SOGL_BNFICR
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA   = :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI         = :WS-TP-MOVI
                   AND TP_OGG          = :WS-GARANZIA
      *            AND ID_POLI   BETWEEN :IABV0009-ID-OGG-DA AND
                   AND DS_VER    BETWEEN   :IABV0009-ID-OGG-DA AND
                                         :IABV0009-ID-OGG-A
                   AND TP_FRM_ASSVA    = :WS-FORMA1
                   AND DT_RICOR_SUCC   = :WS-DATA-EFF
                   AND DT_INI_EFF     <= :WS-DATA-EFF
                   AND DT_END_EFF     >  :WS-DATA-EFF
                   AND DS_TS_INI_CPTZ <= :WS-TS-COMPETENZA
                   AND DS_TS_END_CPTZ >  :WS-TS-COMPETENZA
                   AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                        )
CCAS               AND COD_RAMO = :WS-COD-RAMO
                 FETCH FIRST ROW ONLY
              END-EXEC
           ELSE
              EXEC SQL
                SELECT
                   ID_PARAM_MOVI
                  ,ID_OGG
                  ,TP_OGG
                  ,ID_MOVI_CRZ
                  ,ID_MOVI_CHIU
                  ,DT_INI_EFF
                  ,DT_END_EFF
                  ,COD_COMP_ANIA
                  ,TP_MOVI
                  ,FRQ_MOVI
                  ,DUR_AA
                  ,DUR_MM
                  ,DUR_GG
                  ,DT_RICOR_PREC
                  ,DT_RICOR_SUCC
                  ,PC_INTR_FRAZ
                  ,IMP_BNS_DA_SCO_TOT
                  ,IMP_BNS_DA_SCO
                  ,PC_ANTIC_BNS
                  ,TP_RINN_COLL
                  ,TP_RIVAL_PRE
                  ,TP_RIVAL_PRSTZ
                  ,FL_EVID_RIVAL
                  ,ULT_PC_PERD
                  ,TOT_AA_GIA_PROR
                  ,TP_OPZ
                  ,AA_REN_CER
                  ,PC_REVRSB
                  ,IMP_RISC_PARZ_PRGT
                  ,IMP_LRD_DI_RAT
                  ,IB_OGG
                  ,COS_ONER
                  ,SPE_PC
                  ,FL_ATTIV_GAR
                  ,CAMBIO_VER_PROD
                  ,MM_DIFF
                  ,IMP_RAT_MANFEE
                  ,DT_ULT_EROG_MANFEE
                  ,TP_OGG_RIVAL
                  ,SOM_ASSTA_GARAC
                  ,PC_APPLZ_OPZ
                  ,ID_ADES
                  ,ID_POLI
                  ,TP_FRM_ASSVA
                  ,DS_RIGA
                  ,DS_OPER_SQL
                  ,DS_VER
                  ,DS_TS_INI_CPTZ
                  ,DS_TS_END_CPTZ
                  ,DS_UTENTE
                  ,DS_STATO_ELAB
                  ,TP_ESTR_CNT
ID67              ,COD_RAMO
                  ,GEN_DA_SIN
                  ,COD_TARI
                  ,NUM_RAT_PAG_PRE
                  ,PC_SERV_VAL
                  ,ETA_AA_SOGL_BNFICR
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA   = :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI         = :WS-TP-MOVI
                   AND TP_OGG          = :WS-GARANZIA
                   AND TP_FRM_ASSVA    = :WS-FORMA1
                   AND DT_RICOR_SUCC   = :WS-DATA-EFF
                   AND DT_INI_EFF     <= :WS-DATA-EFF
                   AND DT_END_EFF     >  :WS-DATA-EFF
                   AND DS_TS_INI_CPTZ <= :WS-TS-COMPETENZA
                   AND DS_TS_END_CPTZ >  :WS-TS-COMPETENZA
                   AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                        )
CCAS               AND COD_RAMO = :WS-COD-RAMO
                 FETCH FIRST ROW ONLY
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM Z100-SET-COLONNE-NULL
                 THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N
                 THRU Z950-EX
           END-IF.

       A311-SC01-EX.
           EXIT.
      *
      * ****************************************************************
      *
      * ****************************************************************
       A310-SELECT-SC02.
      *
      * SIR FCTVI00011304 STARTS
      *                     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                      >= :WS-DT-ELAB-DA-DB
      *                        AND
      *                       (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                <=   :WS-DT-ELAB-A-DB
      *                      )
      * SIR FCTVI00011304 ENDS
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                SELECT
                   ID_PARAM_MOVI
                  ,ID_OGG
                  ,TP_OGG
                  ,ID_MOVI_CRZ
                  ,ID_MOVI_CHIU
                  ,DT_INI_EFF
                  ,DT_END_EFF
                  ,COD_COMP_ANIA
                  ,TP_MOVI
                  ,FRQ_MOVI
                  ,DUR_AA
                  ,DUR_MM
                  ,DUR_GG
                  ,DT_RICOR_PREC
                  ,DT_RICOR_SUCC
                  ,PC_INTR_FRAZ
                  ,IMP_BNS_DA_SCO_TOT
                  ,IMP_BNS_DA_SCO
                  ,PC_ANTIC_BNS
                  ,TP_RINN_COLL
                  ,TP_RIVAL_PRE
                  ,TP_RIVAL_PRSTZ
                  ,FL_EVID_RIVAL
                  ,ULT_PC_PERD
                  ,TOT_AA_GIA_PROR
                  ,TP_OPZ
                  ,AA_REN_CER
                  ,PC_REVRSB
                  ,IMP_RISC_PARZ_PRGT
                  ,IMP_LRD_DI_RAT
                  ,IB_OGG
                  ,COS_ONER
                  ,SPE_PC
                  ,FL_ATTIV_GAR
                  ,CAMBIO_VER_PROD
                  ,MM_DIFF
                  ,IMP_RAT_MANFEE
                  ,DT_ULT_EROG_MANFEE
                  ,TP_OGG_RIVAL
                  ,SOM_ASSTA_GARAC
                  ,PC_APPLZ_OPZ
                  ,ID_ADES
                  ,ID_POLI
                  ,TP_FRM_ASSVA
                  ,DS_RIGA
                  ,DS_OPER_SQL
                  ,DS_VER
                  ,DS_TS_INI_CPTZ
                  ,DS_TS_END_CPTZ
                  ,DS_UTENTE
                  ,DS_STATO_ELAB
                  ,TP_ESTR_CNT
                  ,COD_RAMO
                  ,GEN_DA_SIN
                  ,COD_TARI
                  ,NUM_RAT_PAG_PRE
                  ,PC_SERV_VAL
                  ,ETA_AA_SOGL_BNFICR
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
                  ,:PMO-COD-RAMO
                   :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA   = :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI         = :WS-TP-MOVI
                   AND TP_OGG          = :WS-GARANZIA
      *            AND ID_POLI   BETWEEN :IABV0009-ID-OGG-DA AND
                   AND DS_VER    BETWEEN   :IABV0009-ID-OGG-DA AND
                                         :IABV0009-ID-OGG-A
                   AND TP_FRM_ASSVA  IN (:WS-FORMA1 , :WS-FORMA2)
                   AND   ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                              >=  :WS-DT-ELAB-DA-DB
                            AND
                           (DT_RICOR_SUCC + MM_DIFF MONTH)
                              <=  :WS-DT-ELAB-A-DB
                         )
                   AND DT_INI_EFF     <= :WS-DATA-EFF
                   AND DT_END_EFF     >  :WS-DATA-EFF
                   AND DS_TS_INI_CPTZ <= :WS-TS-COMPETENZA
                   AND DS_TS_END_CPTZ >  :WS-TS-COMPETENZA
                   AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                        )
      *      > PER L'ELABORAZIONE MASSIVA ESCLUDO LE POLIZZE DI CONTO
      *      > CORRENTE ASSICURATIVO
                   AND COD_RAMO NOT IN (:WS-COD-RAMO)
                 FETCH FIRST ROW ONLY
              END-EXEC
           ELSE
              EXEC SQL
                SELECT
                   ID_PARAM_MOVI
                  ,ID_OGG
                  ,TP_OGG
                  ,ID_MOVI_CRZ
                  ,ID_MOVI_CHIU
                  ,DT_INI_EFF
                  ,DT_END_EFF
                  ,COD_COMP_ANIA
                  ,TP_MOVI
                  ,FRQ_MOVI
                  ,DUR_AA
                  ,DUR_MM
                  ,DUR_GG
                  ,DT_RICOR_PREC
                  ,DT_RICOR_SUCC
                  ,PC_INTR_FRAZ
                  ,IMP_BNS_DA_SCO_TOT
                  ,IMP_BNS_DA_SCO
                  ,PC_ANTIC_BNS
                  ,TP_RINN_COLL
                  ,TP_RIVAL_PRE
                  ,TP_RIVAL_PRSTZ
                  ,FL_EVID_RIVAL
                  ,ULT_PC_PERD
                  ,TOT_AA_GIA_PROR
                  ,TP_OPZ
                  ,AA_REN_CER
                  ,PC_REVRSB
                  ,IMP_RISC_PARZ_PRGT
                  ,IMP_LRD_DI_RAT
                  ,IB_OGG
                  ,COS_ONER
                  ,SPE_PC
                  ,FL_ATTIV_GAR
                  ,CAMBIO_VER_PROD
                  ,MM_DIFF
                  ,IMP_RAT_MANFEE
                  ,DT_ULT_EROG_MANFEE
                  ,TP_OGG_RIVAL
                  ,SOM_ASSTA_GARAC
                  ,PC_APPLZ_OPZ
                  ,ID_ADES
                  ,ID_POLI
                  ,TP_FRM_ASSVA
                  ,DS_RIGA
                  ,DS_OPER_SQL
                  ,DS_VER
                  ,DS_TS_INI_CPTZ
                  ,DS_TS_END_CPTZ
                  ,DS_UTENTE
                  ,DS_STATO_ELAB
                  ,TP_ESTR_CNT
                  ,COD_RAMO
                  ,GEN_DA_SIN
                  ,COD_TARI
                  ,NUM_RAT_PAG_PRE
                  ,PC_SERV_VAL
                  ,ETA_AA_SOGL_BNFICR
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
                  ,:PMO-COD-RAMO
                   :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA   = :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI         = :WS-TP-MOVI
                   AND TP_OGG          = :WS-GARANZIA
                   AND TP_FRM_ASSVA  IN (:WS-FORMA1 , :WS-FORMA2)
                   AND   ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                              >=  :WS-DT-ELAB-DA-DB
                            AND
                           (DT_RICOR_SUCC + MM_DIFF MONTH)
                              <=  :WS-DT-ELAB-A-DB
                         )
                   AND DT_INI_EFF     <= :WS-DATA-EFF
                   AND DT_END_EFF     >  :WS-DATA-EFF
                   AND DS_TS_INI_CPTZ <= :WS-TS-COMPETENZA
                   AND DS_TS_END_CPTZ >  :WS-TS-COMPETENZA
                   AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                        )
      *      > PER L'ELABORAZIONE MASSIVA ESCLUDO LE POLIZZE DI CONTO
      *      > CORRENTE ASSICURATIVO
                   AND COD_RAMO NOT IN (:WS-COD-RAMO)
                 FETCH FIRST ROW ONLY
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM Z100-SET-COLONNE-NULL
                 THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N
                 THRU Z950-EX
           END-IF.

       A310-SC02-EX.
           EXIT.
      *
      * ****************************************************************
      *
      * ****************************************************************
ID67   A310-SELECT-SC03.
      *
      * SIR FCTVI00011304 STARTS
      *                     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                      >= :WS-DT-ELAB-DA-DB
      *                        AND
      *                       (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                <=   :WS-DT-ELAB-A-DB
      *                      )
      * SIR FCTVI00011304 ENDS
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                SELECT
                   ID_PARAM_MOVI
                  ,ID_OGG
                  ,TP_OGG
                  ,ID_MOVI_CRZ
                  ,ID_MOVI_CHIU
                  ,DT_INI_EFF
                  ,DT_END_EFF
                  ,COD_COMP_ANIA
                  ,TP_MOVI
                  ,FRQ_MOVI
                  ,DUR_AA
                  ,DUR_MM
                  ,DUR_GG
                  ,DT_RICOR_PREC
                  ,DT_RICOR_SUCC
                  ,PC_INTR_FRAZ
                  ,IMP_BNS_DA_SCO_TOT
                  ,IMP_BNS_DA_SCO
                  ,PC_ANTIC_BNS
                  ,TP_RINN_COLL
                  ,TP_RIVAL_PRE
                  ,TP_RIVAL_PRSTZ
                  ,FL_EVID_RIVAL
                  ,ULT_PC_PERD
                  ,TOT_AA_GIA_PROR
                  ,TP_OPZ
                  ,AA_REN_CER
                  ,PC_REVRSB
                  ,IMP_RISC_PARZ_PRGT
                  ,IMP_LRD_DI_RAT
                  ,IB_OGG
                  ,COS_ONER
                  ,SPE_PC
                  ,FL_ATTIV_GAR
                  ,CAMBIO_VER_PROD
                  ,MM_DIFF
                  ,IMP_RAT_MANFEE
                  ,DT_ULT_EROG_MANFEE
                  ,TP_OGG_RIVAL
                  ,SOM_ASSTA_GARAC
                  ,PC_APPLZ_OPZ
                  ,ID_ADES
                  ,ID_POLI
                  ,TP_FRM_ASSVA
                  ,DS_RIGA
                  ,DS_OPER_SQL
                  ,DS_VER
                  ,DS_TS_INI_CPTZ
                  ,DS_TS_END_CPTZ
                  ,DS_UTENTE
                  ,DS_STATO_ELAB
                  ,TP_ESTR_CNT
ID67              ,COD_RAMO
                  ,GEN_DA_SIN
                  ,COD_TARI
                  ,NUM_RAT_PAG_PRE
                  ,PC_SERV_VAL
                  ,ETA_AA_SOGL_BNFICR
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA   = :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI         = :WS-TP-MOVI
                   AND TP_OGG          = :WS-GARANZIA
      *            AND ID_POLI   BETWEEN :IABV0009-ID-OGG-DA AND
                   AND DS_VER    BETWEEN   :IABV0009-ID-OGG-DA AND
                                         :IABV0009-ID-OGG-A
                   AND TP_FRM_ASSVA  IN (:WS-FORMA1 , :WS-FORMA2)
                   AND   ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                              >=  :WS-DT-ELAB-DA-DB
                            AND
                           (DT_RICOR_SUCC + MM_DIFF MONTH)
                              <=  :WS-DT-ELAB-A-DB
                         )
                   AND DT_INI_EFF     <= :WS-DATA-EFF
                   AND DT_END_EFF     >  :WS-DATA-EFF
                   AND DS_TS_INI_CPTZ <= :WS-TS-COMPETENZA
                   AND DS_TS_END_CPTZ >  :WS-TS-COMPETENZA
                   AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                        )
ID67               AND COD_RAMO = :WS-COD-RAMO
                 FETCH FIRST ROW ONLY
              END-EXEC
           ELSE
              EXEC SQL
                SELECT
                   ID_PARAM_MOVI
                  ,ID_OGG
                  ,TP_OGG
                  ,ID_MOVI_CRZ
                  ,ID_MOVI_CHIU
                  ,DT_INI_EFF
                  ,DT_END_EFF
                  ,COD_COMP_ANIA
                  ,TP_MOVI
                  ,FRQ_MOVI
                  ,DUR_AA
                  ,DUR_MM
                  ,DUR_GG
                  ,DT_RICOR_PREC
                  ,DT_RICOR_SUCC
                  ,PC_INTR_FRAZ
                  ,IMP_BNS_DA_SCO_TOT
                  ,IMP_BNS_DA_SCO
                  ,PC_ANTIC_BNS
                  ,TP_RINN_COLL
                  ,TP_RIVAL_PRE
                  ,TP_RIVAL_PRSTZ
                  ,FL_EVID_RIVAL
                  ,ULT_PC_PERD
                  ,TOT_AA_GIA_PROR
                  ,TP_OPZ
                  ,AA_REN_CER
                  ,PC_REVRSB
                  ,IMP_RISC_PARZ_PRGT
                  ,IMP_LRD_DI_RAT
                  ,IB_OGG
                  ,COS_ONER
                  ,SPE_PC
                  ,FL_ATTIV_GAR
                  ,CAMBIO_VER_PROD
                  ,MM_DIFF
                  ,IMP_RAT_MANFEE
                  ,DT_ULT_EROG_MANFEE
                  ,TP_OGG_RIVAL
                  ,SOM_ASSTA_GARAC
                  ,PC_APPLZ_OPZ
                  ,ID_ADES
                  ,ID_POLI
                  ,TP_FRM_ASSVA
                  ,DS_RIGA
                  ,DS_OPER_SQL
                  ,DS_VER
                  ,DS_TS_INI_CPTZ
                  ,DS_TS_END_CPTZ
                  ,DS_UTENTE
                  ,DS_STATO_ELAB
                  ,TP_ESTR_CNT
ID67              ,COD_RAMO
                  ,GEN_DA_SIN
                  ,COD_TARI
                  ,NUM_RAT_PAG_PRE
                  ,PC_SERV_VAL
                  ,ETA_AA_SOGL_BNFICR
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA   = :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI         = :WS-TP-MOVI
                   AND TP_OGG          = :WS-GARANZIA
                   AND TP_FRM_ASSVA  IN (:WS-FORMA1 , :WS-FORMA2)
                   AND ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                              >=  :WS-DT-ELAB-DA-DB
                            AND
                           (DT_RICOR_SUCC + MM_DIFF MONTH)
                              <=  :WS-DT-ELAB-A-DB
                         )
                   AND DT_INI_EFF     <= :WS-DATA-EFF
                   AND DT_END_EFF     >  :WS-DATA-EFF
                   AND DS_TS_INI_CPTZ <= :WS-TS-COMPETENZA
                   AND DS_TS_END_CPTZ >  :WS-TS-COMPETENZA
                   AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                        )
ID67               AND COD_RAMO = :WS-COD-RAMO
                 FETCH FIRST ROW ONLY
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM Z100-SET-COLONNE-NULL
                 THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N
                 THRU Z950-EX
           END-IF.

       A310-SC03-EX.
           EXIT.
      *
      * ****************************************************************
      *
      * ****************************************************************
ID67   A310-SELECT-SC04.
      *
      * SIR FCTVI00011304 STARTS
      *                     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                      >= :WS-DT-ELAB-DA-DB
      *                        AND
      *                       (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                <=   :WS-DT-ELAB-A-DB
      *                      )
      * SIR FCTVI00011304 ENDS
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                SELECT
                   ID_PARAM_MOVI
                  ,ID_OGG
                  ,TP_OGG
                  ,ID_MOVI_CRZ
                  ,ID_MOVI_CHIU
                  ,DT_INI_EFF
                  ,DT_END_EFF
                  ,COD_COMP_ANIA
                  ,TP_MOVI
                  ,FRQ_MOVI
                  ,DUR_AA
                  ,DUR_MM
                  ,DUR_GG
                  ,DT_RICOR_PREC
                  ,DT_RICOR_SUCC
                  ,PC_INTR_FRAZ
                  ,IMP_BNS_DA_SCO_TOT
                  ,IMP_BNS_DA_SCO
                  ,PC_ANTIC_BNS
                  ,TP_RINN_COLL
                  ,TP_RIVAL_PRE
                  ,TP_RIVAL_PRSTZ
                  ,FL_EVID_RIVAL
                  ,ULT_PC_PERD
                  ,TOT_AA_GIA_PROR
                  ,TP_OPZ
                  ,AA_REN_CER
                  ,PC_REVRSB
                  ,IMP_RISC_PARZ_PRGT
                  ,IMP_LRD_DI_RAT
                  ,IB_OGG
                  ,COS_ONER
                  ,SPE_PC
                  ,FL_ATTIV_GAR
                  ,CAMBIO_VER_PROD
                  ,MM_DIFF
                  ,IMP_RAT_MANFEE
                  ,DT_ULT_EROG_MANFEE
                  ,TP_OGG_RIVAL
                  ,SOM_ASSTA_GARAC
                  ,PC_APPLZ_OPZ
                  ,ID_ADES
                  ,ID_POLI
                  ,TP_FRM_ASSVA
                  ,DS_RIGA
                  ,DS_OPER_SQL
                  ,DS_VER
                  ,DS_TS_INI_CPTZ
                  ,DS_TS_END_CPTZ
                  ,DS_UTENTE
                  ,DS_STATO_ELAB
                  ,TP_ESTR_CNT
ID67              ,COD_RAMO
                  ,GEN_DA_SIN
                  ,COD_TARI
                  ,NUM_RAT_PAG_PRE
                  ,PC_SERV_VAL
                  ,ETA_AA_SOGL_BNFICR
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA   = :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI         = :WS-TP-MOVI
                   AND TP_OGG          = :WS-GARANZIA
      *            AND ID_POLI   BETWEEN :IABV0009-ID-OGG-DA AND
                   AND DS_VER    BETWEEN   :IABV0009-ID-OGG-DA AND
                                         :IABV0009-ID-OGG-A
                   AND TP_FRM_ASSVA  IN (:WS-FORMA1 , :WS-FORMA2)
                   AND   ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                              >=  :WS-DT-ELAB-DA-DB
                            AND
                           (DT_RICOR_SUCC + MM_DIFF MONTH)
                              <=  :WS-DT-ELAB-A-DB
                         )
                   AND DT_INI_EFF     <= :WS-DATA-EFF
                   AND DT_END_EFF     >  :WS-DATA-EFF
                   AND DS_TS_INI_CPTZ <= :WS-TS-COMPETENZA
                   AND DS_TS_END_CPTZ >  :WS-TS-COMPETENZA
                   AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                        )
ID67               AND ID_POLI = :WS-ID-POLI
                 FETCH FIRST ROW ONLY
              END-EXEC
           ELSE
              EXEC SQL
                SELECT
                   ID_PARAM_MOVI
                  ,ID_OGG
                  ,TP_OGG
                  ,ID_MOVI_CRZ
                  ,ID_MOVI_CHIU
                  ,DT_INI_EFF
                  ,DT_END_EFF
                  ,COD_COMP_ANIA
                  ,TP_MOVI
                  ,FRQ_MOVI
                  ,DUR_AA
                  ,DUR_MM
                  ,DUR_GG
                  ,DT_RICOR_PREC
                  ,DT_RICOR_SUCC
                  ,PC_INTR_FRAZ
                  ,IMP_BNS_DA_SCO_TOT
                  ,IMP_BNS_DA_SCO
                  ,PC_ANTIC_BNS
                  ,TP_RINN_COLL
                  ,TP_RIVAL_PRE
                  ,TP_RIVAL_PRSTZ
                  ,FL_EVID_RIVAL
                  ,ULT_PC_PERD
                  ,TOT_AA_GIA_PROR
                  ,TP_OPZ
                  ,AA_REN_CER
                  ,PC_REVRSB
                  ,IMP_RISC_PARZ_PRGT
                  ,IMP_LRD_DI_RAT
                  ,IB_OGG
                  ,COS_ONER
                  ,SPE_PC
                  ,FL_ATTIV_GAR
                  ,CAMBIO_VER_PROD
                  ,MM_DIFF
                  ,IMP_RAT_MANFEE
                  ,DT_ULT_EROG_MANFEE
                  ,TP_OGG_RIVAL
                  ,SOM_ASSTA_GARAC
                  ,PC_APPLZ_OPZ
                  ,ID_ADES
                  ,ID_POLI
                  ,TP_FRM_ASSVA
                  ,DS_RIGA
                  ,DS_OPER_SQL
                  ,DS_VER
                  ,DS_TS_INI_CPTZ
                  ,DS_TS_END_CPTZ
                  ,DS_UTENTE
                  ,DS_STATO_ELAB
                  ,TP_ESTR_CNT
ID67              ,COD_RAMO
                  ,GEN_DA_SIN
                  ,COD_TARI
                  ,NUM_RAT_PAG_PRE
                  ,PC_SERV_VAL
                  ,ETA_AA_SOGL_BNFICR
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA   = :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI         = :WS-TP-MOVI
                   AND TP_OGG          = :WS-GARANZIA
                   AND TP_FRM_ASSVA  IN (:WS-FORMA1 , :WS-FORMA2)
                   AND ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                              >=  :WS-DT-ELAB-DA-DB
                            AND
                           (DT_RICOR_SUCC + MM_DIFF MONTH)
                              <=  :WS-DT-ELAB-A-DB
                         )
                   AND DT_INI_EFF     <= :WS-DATA-EFF
                   AND DT_END_EFF     >  :WS-DATA-EFF
                   AND DS_TS_INI_CPTZ <= :WS-TS-COMPETENZA
                   AND DS_TS_END_CPTZ >  :WS-TS-COMPETENZA
                   AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                        )
ID67               AND ID_POLI = :WS-ID-POLI
                 FETCH FIRST ROW ONLY
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM Z100-SET-COLONNE-NULL
                 THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N
                 THRU Z950-EX
           END-IF.

       A310-SC04-EX.
           EXIT.
      * ****************************************************************
      *
      * ****************************************************************
MIGR   A310-SELECT-SC05.
      *
      * SIR FCTVI00011304 STARTS
      *                     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                      >= :WS-DT-ELAB-DA-DB
      *                        AND
      *                       (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                <=   :WS-DT-ELAB-A-DB
      *                      )
      * SIR FCTVI00011304 ENDS
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                SELECT
                   ID_PARAM_MOVI
                  ,ID_OGG
                  ,TP_OGG
                  ,ID_MOVI_CRZ
                  ,ID_MOVI_CHIU
                  ,DT_INI_EFF
                  ,DT_END_EFF
                  ,COD_COMP_ANIA
                  ,TP_MOVI
                  ,FRQ_MOVI
                  ,DUR_AA
                  ,DUR_MM
                  ,DUR_GG
                  ,DT_RICOR_PREC
                  ,DT_RICOR_SUCC
                  ,PC_INTR_FRAZ
                  ,IMP_BNS_DA_SCO_TOT
                  ,IMP_BNS_DA_SCO
                  ,PC_ANTIC_BNS
                  ,TP_RINN_COLL
                  ,TP_RIVAL_PRE
                  ,TP_RIVAL_PRSTZ
                  ,FL_EVID_RIVAL
                  ,ULT_PC_PERD
                  ,TOT_AA_GIA_PROR
                  ,TP_OPZ
                  ,AA_REN_CER
                  ,PC_REVRSB
                  ,IMP_RISC_PARZ_PRGT
                  ,IMP_LRD_DI_RAT
                  ,IB_OGG
                  ,COS_ONER
                  ,SPE_PC
                  ,FL_ATTIV_GAR
                  ,CAMBIO_VER_PROD
                  ,MM_DIFF
                  ,IMP_RAT_MANFEE
                  ,DT_ULT_EROG_MANFEE
                  ,TP_OGG_RIVAL
                  ,SOM_ASSTA_GARAC
                  ,PC_APPLZ_OPZ
                  ,ID_ADES
                  ,ID_POLI
                  ,TP_FRM_ASSVA
                  ,DS_RIGA
                  ,DS_OPER_SQL
                  ,DS_VER
                  ,DS_TS_INI_CPTZ
                  ,DS_TS_END_CPTZ
                  ,DS_UTENTE
                  ,DS_STATO_ELAB
                  ,TP_ESTR_CNT
                  ,COD_RAMO
                  ,GEN_DA_SIN
                  ,COD_TARI
                  ,NUM_RAT_PAG_PRE
                  ,PC_SERV_VAL
                  ,ETA_AA_SOGL_BNFICR
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
                  ,:PMO-COD-RAMO
                   :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA   = :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI         = :WS-TP-MOVI
                   AND TP_OGG          = :WS-GARANZIA
                   AND ID_ADES   BETWEEN :IABV0009-ID-OGG-DA AND
                                         :IABV0009-ID-OGG-A
                   AND TP_FRM_ASSVA  IN (:WS-FORMA1 , :WS-FORMA2)
                   AND   ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                              >=  :WS-DT-ELAB-DA-DB
                            AND
                           (DT_RICOR_SUCC + MM_DIFF MONTH)
                              <=  :WS-DT-ELAB-A-DB
                         )
                   AND DT_INI_EFF     <= :WS-DATA-EFF
                   AND DT_END_EFF     >  :WS-DATA-EFF
                   AND DS_TS_INI_CPTZ <= :WS-TS-COMPETENZA
                   AND DS_TS_END_CPTZ >  :WS-TS-COMPETENZA
                   AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                        )
      *      > PER L'ELABORAZIONE MASSIVA ESCLUDO LE POLIZZE DI CONTO
      *      > CORRENTE ASSICURATIVO
                   AND COD_RAMO NOT IN (:WS-COD-RAMO)
                 FETCH FIRST ROW ONLY
              END-EXEC
           ELSE
              EXEC SQL
                SELECT
                   ID_PARAM_MOVI
                  ,ID_OGG
                  ,TP_OGG
                  ,ID_MOVI_CRZ
                  ,ID_MOVI_CHIU
                  ,DT_INI_EFF
                  ,DT_END_EFF
                  ,COD_COMP_ANIA
                  ,TP_MOVI
                  ,FRQ_MOVI
                  ,DUR_AA
                  ,DUR_MM
                  ,DUR_GG
                  ,DT_RICOR_PREC
                  ,DT_RICOR_SUCC
                  ,PC_INTR_FRAZ
                  ,IMP_BNS_DA_SCO_TOT
                  ,IMP_BNS_DA_SCO
                  ,PC_ANTIC_BNS
                  ,TP_RINN_COLL
                  ,TP_RIVAL_PRE
                  ,TP_RIVAL_PRSTZ
                  ,FL_EVID_RIVAL
                  ,ULT_PC_PERD
                  ,TOT_AA_GIA_PROR
                  ,TP_OPZ
                  ,AA_REN_CER
                  ,PC_REVRSB
                  ,IMP_RISC_PARZ_PRGT
                  ,IMP_LRD_DI_RAT
                  ,IB_OGG
                  ,COS_ONER
                  ,SPE_PC
                  ,FL_ATTIV_GAR
                  ,CAMBIO_VER_PROD
                  ,MM_DIFF
                  ,IMP_RAT_MANFEE
                  ,DT_ULT_EROG_MANFEE
                  ,TP_OGG_RIVAL
                  ,SOM_ASSTA_GARAC
                  ,PC_APPLZ_OPZ
                  ,ID_ADES
                  ,ID_POLI
                  ,TP_FRM_ASSVA
                  ,DS_RIGA
                  ,DS_OPER_SQL
                  ,DS_VER
                  ,DS_TS_INI_CPTZ
                  ,DS_TS_END_CPTZ
                  ,DS_UTENTE
                  ,DS_STATO_ELAB
                  ,TP_ESTR_CNT
                  ,COD_RAMO
                  ,GEN_DA_SIN
                  ,COD_TARI
                  ,NUM_RAT_PAG_PRE
                  ,PC_SERV_VAL
                  ,ETA_AA_SOGL_BNFICR
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
                  ,:PMO-COD-RAMO
                   :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA   = :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI         = :WS-TP-MOVI
                   AND TP_OGG          = :WS-GARANZIA
                   AND TP_FRM_ASSVA  IN (:WS-FORMA1 , :WS-FORMA2)
                   AND   ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                              >=  :WS-DT-ELAB-DA-DB
                            AND
                           (DT_RICOR_SUCC + MM_DIFF MONTH)
                              <=  :WS-DT-ELAB-A-DB
                         )
                   AND DT_INI_EFF     <= :WS-DATA-EFF
                   AND DT_END_EFF     >  :WS-DATA-EFF
                   AND DS_TS_INI_CPTZ <= :WS-TS-COMPETENZA
                   AND DS_TS_END_CPTZ >  :WS-TS-COMPETENZA
                   AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                        )
      *      > PER L'ELABORAZIONE MASSIVA ESCLUDO LE POLIZZE DI CONTO
      *      > CORRENTE ASSICURATIVO
                   AND COD_RAMO NOT IN (:WS-COD-RAMO)
                 FETCH FIRST ROW ONLY
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM Z100-SET-COLONNE-NULL
                 THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N
                 THRU Z950-EX
           END-IF.

       A310-SC05-EX.
           EXIT.
      *
      * ****************************************************************
      *
      * ****************************************************************
MIGR   A310-SELECT-SC06.
      *
      * SIR FCTVI00011304 STARTS
      *                     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                      >= :WS-DT-ELAB-DA-DB
      *                        AND
      *                       (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                <=   :WS-DT-ELAB-A-DB
      *                      )
      * SIR FCTVI00011304 ENDS
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                SELECT
                   ID_PARAM_MOVI
                  ,ID_OGG
                  ,TP_OGG
                  ,ID_MOVI_CRZ
                  ,ID_MOVI_CHIU
                  ,DT_INI_EFF
                  ,DT_END_EFF
                  ,COD_COMP_ANIA
                  ,TP_MOVI
                  ,FRQ_MOVI
                  ,DUR_AA
                  ,DUR_MM
                  ,DUR_GG
                  ,DT_RICOR_PREC
                  ,DT_RICOR_SUCC
                  ,PC_INTR_FRAZ
                  ,IMP_BNS_DA_SCO_TOT
                  ,IMP_BNS_DA_SCO
                  ,PC_ANTIC_BNS
                  ,TP_RINN_COLL
                  ,TP_RIVAL_PRE
                  ,TP_RIVAL_PRSTZ
                  ,FL_EVID_RIVAL
                  ,ULT_PC_PERD
                  ,TOT_AA_GIA_PROR
                  ,TP_OPZ
                  ,AA_REN_CER
                  ,PC_REVRSB
                  ,IMP_RISC_PARZ_PRGT
                  ,IMP_LRD_DI_RAT
                  ,IB_OGG
                  ,COS_ONER
                  ,SPE_PC
                  ,FL_ATTIV_GAR
                  ,CAMBIO_VER_PROD
                  ,MM_DIFF
                  ,IMP_RAT_MANFEE
                  ,DT_ULT_EROG_MANFEE
                  ,TP_OGG_RIVAL
                  ,SOM_ASSTA_GARAC
                  ,PC_APPLZ_OPZ
                  ,ID_ADES
                  ,ID_POLI
                  ,TP_FRM_ASSVA
                  ,DS_RIGA
                  ,DS_OPER_SQL
                  ,DS_VER
                  ,DS_TS_INI_CPTZ
                  ,DS_TS_END_CPTZ
                  ,DS_UTENTE
                  ,DS_STATO_ELAB
                  ,TP_ESTR_CNT
ID67              ,COD_RAMO
                  ,GEN_DA_SIN
                  ,COD_TARI
                  ,NUM_RAT_PAG_PRE
                  ,PC_SERV_VAL
                  ,ETA_AA_SOGL_BNFICR
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA   = :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI         = :WS-TP-MOVI
                   AND TP_OGG          = :WS-GARANZIA
                   AND ID_ADES   BETWEEN :IABV0009-ID-OGG-DA AND
                                         :IABV0009-ID-OGG-A
                   AND TP_FRM_ASSVA  IN (:WS-FORMA1 , :WS-FORMA2)
                   AND   ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                              >=  :WS-DT-ELAB-DA-DB
                            AND
                           (DT_RICOR_SUCC + MM_DIFF MONTH)
                              <=  :WS-DT-ELAB-A-DB
                         )
                   AND DT_INI_EFF     <= :WS-DATA-EFF
                   AND DT_END_EFF     >  :WS-DATA-EFF
                   AND DS_TS_INI_CPTZ <= :WS-TS-COMPETENZA
                   AND DS_TS_END_CPTZ >  :WS-TS-COMPETENZA
                   AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                        )
ID67               AND COD_RAMO = :WS-COD-RAMO
                 FETCH FIRST ROW ONLY
              END-EXEC
           ELSE
              EXEC SQL
                SELECT
                   ID_PARAM_MOVI
                  ,ID_OGG
                  ,TP_OGG
                  ,ID_MOVI_CRZ
                  ,ID_MOVI_CHIU
                  ,DT_INI_EFF
                  ,DT_END_EFF
                  ,COD_COMP_ANIA
                  ,TP_MOVI
                  ,FRQ_MOVI
                  ,DUR_AA
                  ,DUR_MM
                  ,DUR_GG
                  ,DT_RICOR_PREC
                  ,DT_RICOR_SUCC
                  ,PC_INTR_FRAZ
                  ,IMP_BNS_DA_SCO_TOT
                  ,IMP_BNS_DA_SCO
                  ,PC_ANTIC_BNS
                  ,TP_RINN_COLL
                  ,TP_RIVAL_PRE
                  ,TP_RIVAL_PRSTZ
                  ,FL_EVID_RIVAL
                  ,ULT_PC_PERD
                  ,TOT_AA_GIA_PROR
                  ,TP_OPZ
                  ,AA_REN_CER
                  ,PC_REVRSB
                  ,IMP_RISC_PARZ_PRGT
                  ,IMP_LRD_DI_RAT
                  ,IB_OGG
                  ,COS_ONER
                  ,SPE_PC
                  ,FL_ATTIV_GAR
                  ,CAMBIO_VER_PROD
                  ,MM_DIFF
                  ,IMP_RAT_MANFEE
                  ,DT_ULT_EROG_MANFEE
                  ,TP_OGG_RIVAL
                  ,SOM_ASSTA_GARAC
                  ,PC_APPLZ_OPZ
                  ,ID_ADES
                  ,ID_POLI
                  ,TP_FRM_ASSVA
                  ,DS_RIGA
                  ,DS_OPER_SQL
                  ,DS_VER
                  ,DS_TS_INI_CPTZ
                  ,DS_TS_END_CPTZ
                  ,DS_UTENTE
                  ,DS_STATO_ELAB
                  ,TP_ESTR_CNT
ID67              ,COD_RAMO
                  ,GEN_DA_SIN
                  ,COD_TARI
                  ,NUM_RAT_PAG_PRE
                  ,PC_SERV_VAL
                  ,ETA_AA_SOGL_BNFICR
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
                 FROM PARAM_MOVI
                 WHERE COD_COMP_ANIA   = :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND TP_MOVI         = :WS-TP-MOVI
                   AND TP_OGG          = :WS-GARANZIA
                   AND TP_FRM_ASSVA  IN (:WS-FORMA1 , :WS-FORMA2)
                   AND ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                              >=  :WS-DT-ELAB-DA-DB
                            AND
                           (DT_RICOR_SUCC + MM_DIFF MONTH)
                              <=  :WS-DT-ELAB-A-DB
                         )
                   AND DT_INI_EFF     <= :WS-DATA-EFF
                   AND DT_END_EFF     >  :WS-DATA-EFF
                   AND DS_TS_INI_CPTZ <= :WS-TS-COMPETENZA
                   AND DS_TS_END_CPTZ >  :WS-TS-COMPETENZA
                   AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                        )
ID67               AND COD_RAMO = :WS-COD-RAMO
                 FETCH FIRST ROW ONLY
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM Z100-SET-COLONNE-NULL
                 THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N
                 THRU Z950-EX
           END-IF.

       A310-SC06-EX.
           EXIT.
      *
      * ****************************************************************
      *
      * ****************************************************************
MIGR   A310-SELECT-SC07.
      *
      * SIR FCTVI00011304 STARTS
      *                     ( (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                      >= :WS-DT-ELAB-DA-DB
      *                        AND
      *                       (DT_RICOR_SUCC + MM_DIFF MONTH)
      *                                <=   :WS-DT-ELAB-A-DB
      *                      )
      * SIR FCTVI00011304 ENDS
           EXEC SQL
             SELECT
                ID_PARAM_MOVI
               ,ID_OGG
               ,TP_OGG
               ,ID_MOVI_CRZ
               ,ID_MOVI_CHIU
               ,DT_INI_EFF
               ,DT_END_EFF
               ,COD_COMP_ANIA
               ,TP_MOVI
               ,FRQ_MOVI
               ,DUR_AA
               ,DUR_MM
               ,DUR_GG
               ,DT_RICOR_PREC
               ,DT_RICOR_SUCC
               ,PC_INTR_FRAZ
               ,IMP_BNS_DA_SCO_TOT
               ,IMP_BNS_DA_SCO
               ,PC_ANTIC_BNS
               ,TP_RINN_COLL
               ,TP_RIVAL_PRE
               ,TP_RIVAL_PRSTZ
               ,FL_EVID_RIVAL
               ,ULT_PC_PERD
               ,TOT_AA_GIA_PROR
               ,TP_OPZ
               ,AA_REN_CER
               ,PC_REVRSB
               ,IMP_RISC_PARZ_PRGT
               ,IMP_LRD_DI_RAT
               ,IB_OGG
               ,COS_ONER
               ,SPE_PC
               ,FL_ATTIV_GAR
               ,CAMBIO_VER_PROD
               ,MM_DIFF
               ,IMP_RAT_MANFEE
               ,DT_ULT_EROG_MANFEE
               ,TP_OGG_RIVAL
               ,SOM_ASSTA_GARAC
               ,PC_APPLZ_OPZ
               ,ID_ADES
               ,ID_POLI
               ,TP_FRM_ASSVA
               ,DS_RIGA
               ,DS_OPER_SQL
               ,DS_VER
               ,DS_TS_INI_CPTZ
               ,DS_TS_END_CPTZ
               ,DS_UTENTE
               ,DS_STATO_ELAB
               ,TP_ESTR_CNT
ID67           ,COD_RAMO
               ,GEN_DA_SIN
               ,COD_TARI
               ,NUM_RAT_PAG_PRE
               ,PC_SERV_VAL
               ,ETA_AA_SOGL_BNFICR
           INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
ID67           ,:PMO-COD-RAMO
ID67            :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR
              FROM PARAM_MOVI
              WHERE COD_COMP_ANIA      = :IDSV0003-CODICE-COMPAGNIA-ANIA
                AND TP_MOVI            = :WS-TP-MOVI
                AND TP_OGG             = :WS-GARANZIA
                AND TP_FRM_ASSVA     IN (:WS-FORMA1 , :WS-FORMA2)
                AND ( (DT_RICOR_SUCC + MM_DIFF MONTH)
                           >=     :WS-DT-ELAB-DA-DB
                         AND
                        (DT_RICOR_SUCC + MM_DIFF MONTH)
                           <=     :WS-DT-ELAB-A-DB
                      )
                AND DT_INI_EFF        <= :WS-DATA-EFF
                AND DT_END_EFF        >  :WS-DATA-EFF
                AND DS_TS_INI_CPTZ <= :WS-TS-COMPETENZA
                AND DS_TS_END_CPTZ >     :WS-TS-COMPETENZA
                AND DS_STATO_ELAB IN (
                                     :IABV0002-STATE-01,
                                     :IABV0002-STATE-02,
                                     :IABV0002-STATE-03,
                                     :IABV0002-STATE-04,
                                     :IABV0002-STATE-05,
                                     :IABV0002-STATE-06,
                                     :IABV0002-STATE-07,
                                     :IABV0002-STATE-08,
                                     :IABV0002-STATE-09,
                                     :IABV0002-STATE-10
                                     )
ID67            AND ID_ADES = :WS-ID-ADES
              FETCH FIRST ROW ONLY
           END-EXEC

           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM Z100-SET-COLONNE-NULL
                 THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N
                 THRU Z950-EX
           END-IF.

       A310-SC07-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
       A321-UPDATE-SC01.
      *
           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
               PERFORM A331-UPDATE-PK-SC01
                  THRU A331-SC01-EX
              WHEN IDSV0003-WHERE-CONDITION
                   PERFORM A341-UPDATE-WHERE-COND-SC01
                      THRU A341-SC01-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A346-UPDATE-FIRST-ACTION-SC01
                      THRU A346-SC01-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER
                    TO TRUE
           END-EVALUATE.
      *
       A321-SC01-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
       A320-UPDATE-SC02.
      *
           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
               PERFORM A330-UPDATE-PK-SC02
                  THRU A330-SC02-EX
              WHEN IDSV0003-WHERE-CONDITION
                   PERFORM A340-UPDATE-WHERE-COND-SC02
                      THRU A340-SC02-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A345-UPDATE-FIRST-ACTION-SC02
                      THRU A345-SC02-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER
                    TO TRUE
           END-EVALUATE.
      *
       A320-SC02-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
ID67   A320-UPDATE-SC03.
      *
           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
               PERFORM A330-UPDATE-PK-SC03
                  THRU A330-SC03-EX
              WHEN IDSV0003-WHERE-CONDITION
                   PERFORM A340-UPDATE-WHERE-COND-SC03
                      THRU A340-SC03-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A345-UPDATE-FIRST-ACTION-SC03
                      THRU A345-SC03-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER
                    TO TRUE
           END-EVALUATE.
      *
       A320-SC03-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
ID67   A320-UPDATE-SC04.
      *
           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
               PERFORM A330-UPDATE-PK-SC04
                  THRU A330-SC04-EX
              WHEN IDSV0003-WHERE-CONDITION
                   PERFORM A340-UPDATE-WHERE-COND-SC04
                      THRU A340-SC04-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A345-UPDATE-FIRST-ACTION-SC04
                      THRU A345-SC04-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER
                    TO TRUE
           END-EVALUATE.
      *
       A320-SC04-EX.
           EXIT.
      * ****************************************************************
      * UPDATE TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
MIGR   A320-UPDATE-SC05.
      *
           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
               PERFORM A330-UPDATE-PK-SC05
                  THRU A330-SC05-EX
              WHEN IDSV0003-WHERE-CONDITION
                   PERFORM A340-UPDATE-WHERE-COND-SC05
                      THRU A340-SC05-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A345-UPDATE-FIRST-ACTION-SC05
                      THRU A345-SC05-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER
                    TO TRUE
           END-EVALUATE.
      *
       A320-SC05-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
MIGR   A320-UPDATE-SC06.
      *
           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
               PERFORM A330-UPDATE-PK-SC06
                  THRU A330-SC06-EX
              WHEN IDSV0003-WHERE-CONDITION
                   PERFORM A340-UPDATE-WHERE-COND-SC06
                      THRU A340-SC06-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A345-UPDATE-FIRST-ACTION-SC06
                      THRU A345-SC06-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER
                    TO TRUE
           END-EVALUATE.
      *
       A320-SC06-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
MIGR   A320-UPDATE-SC07.
      *
           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
               PERFORM A330-UPDATE-PK-SC07
                  THRU A330-SC07-EX
              WHEN IDSV0003-WHERE-CONDITION
                   PERFORM A340-UPDATE-WHERE-COND-SC07
                      THRU A340-SC07-EX
              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A345-UPDATE-FIRST-ACTION-SC07
                      THRU A345-SC07-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER
                    TO TRUE
           END-EVALUATE.
      *
       A320-SC07-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE PARAMETRO MOVIMENTO PER CHIAVE PRIMARIA
      * ****************************************************************
      *
       A331-UPDATE-PK-SC01.
      *
           PERFORM Z150-VALORIZZA-DATA-SERVICES
              THRU Z150-EX.

           PERFORM Z960-LENGTH-VCHAR
              THRU Z960-EX
      *
           EXEC SQL
                UPDATE PARAM_MOVI SET
                   DS_OPER_SQL         = :PMO-DS-OPER-SQL
      *           ,DS_VER              = :IABV0009-VERSIONING
                  ,DS_UTENTE           = :PMO-DS-UTENTE
                  ,DS_STATO_ELAB       = :IABV0002-STATE-CURRENT
                WHERE DS_RIGA          = :PMO-DS-RIGA
           END-EXEC.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A331-SC01-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE PARAMETRO MOVIMENTO PER CHIAVE PRIMARIA
      * ****************************************************************
      *
       A330-UPDATE-PK-SC02.
      *
           PERFORM Z150-VALORIZZA-DATA-SERVICES
              THRU Z150-EX.

           PERFORM Z960-LENGTH-VCHAR
              THRU Z960-EX
      *
           EXEC SQL
                UPDATE PARAM_MOVI SET
                   DS_OPER_SQL         = :PMO-DS-OPER-SQL
      *           ,DS_VER              = :IABV0009-VERSIONING
                  ,DS_UTENTE           = :PMO-DS-UTENTE
                  ,DS_STATO_ELAB       = :IABV0002-STATE-CURRENT
                WHERE DS_RIGA          = :PMO-DS-RIGA
           END-EXEC.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A330-SC02-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE PARAMETRO MOVIMENTO PER CHIAVE PRIMARIA
      * ****************************************************************
      *
ID67   A330-UPDATE-PK-SC03.
      *
           PERFORM Z150-VALORIZZA-DATA-SERVICES
              THRU Z150-EX.

           PERFORM Z960-LENGTH-VCHAR
              THRU Z960-EX
      *
           EXEC SQL
                UPDATE PARAM_MOVI SET
                   DS_OPER_SQL         = :PMO-DS-OPER-SQL
      *           ,DS_VER              = :IABV0009-VERSIONING
                  ,DS_UTENTE           = :PMO-DS-UTENTE
                  ,DS_STATO_ELAB       = :IABV0002-STATE-CURRENT
                WHERE DS_RIGA          = :PMO-DS-RIGA
           END-EXEC.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A330-SC03-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE PARAMETRO MOVIMENTO PER CHIAVE PRIMARIA
      * ****************************************************************
      *
ID67   A330-UPDATE-PK-SC04.
      *
           PERFORM Z150-VALORIZZA-DATA-SERVICES
              THRU Z150-EX.

           PERFORM Z960-LENGTH-VCHAR
              THRU Z960-EX
      *
           EXEC SQL
                UPDATE PARAM_MOVI SET
                   DS_OPER_SQL         = :PMO-DS-OPER-SQL
      *           ,DS_VER              = :IABV0009-VERSIONING
                  ,DS_UTENTE           = :PMO-DS-UTENTE
                  ,DS_STATO_ELAB       = :IABV0002-STATE-CURRENT
                WHERE DS_RIGA          = :PMO-DS-RIGA
           END-EXEC.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A330-SC04-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE PARAMETRO MOVIMENTO PER CHIAVE PRIMARIA
      * ****************************************************************
      *
MIGR   A330-UPDATE-PK-SC05.
      *
           PERFORM Z150-VALORIZZA-DATA-SERVICES
              THRU Z150-EX.

           PERFORM Z960-LENGTH-VCHAR
              THRU Z960-EX
      *
           EXEC SQL
                UPDATE PARAM_MOVI SET
                   DS_OPER_SQL         = :PMO-DS-OPER-SQL
      *           ,DS_VER              = :IABV0009-VERSIONING
                  ,DS_UTENTE           = :PMO-DS-UTENTE
                  ,DS_STATO_ELAB       = :IABV0002-STATE-CURRENT
                WHERE DS_RIGA          = :PMO-DS-RIGA
           END-EXEC.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A330-SC05-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE PARAMETRO MOVIMENTO PER CHIAVE PRIMARIA
      * ****************************************************************
      *
MIGR   A330-UPDATE-PK-SC06.
      *
           PERFORM Z150-VALORIZZA-DATA-SERVICES
              THRU Z150-EX.

           PERFORM Z960-LENGTH-VCHAR
              THRU Z960-EX
      *
           EXEC SQL
                UPDATE PARAM_MOVI SET
                   DS_OPER_SQL         = :PMO-DS-OPER-SQL
      *           ,DS_VER              = :IABV0009-VERSIONING
                  ,DS_UTENTE           = :PMO-DS-UTENTE
                  ,DS_STATO_ELAB       = :IABV0002-STATE-CURRENT
                WHERE DS_RIGA          = :PMO-DS-RIGA
           END-EXEC.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A330-SC06-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE PARAMETRO MOVIMENTO PER CHIAVE PRIMARIA
      * ****************************************************************
      *
MIGR   A330-UPDATE-PK-SC07.
      *
           PERFORM Z150-VALORIZZA-DATA-SERVICES
              THRU Z150-EX.

           PERFORM Z960-LENGTH-VCHAR
              THRU Z960-EX
      *
           EXEC SQL
                UPDATE PARAM_MOVI SET
                   DS_OPER_SQL         = :PMO-DS-OPER-SQL
      *           ,DS_VER              = :IABV0009-VERSIONING
                  ,DS_UTENTE           = :PMO-DS-UTENTE
                  ,DS_STATO_ELAB       = :IABV0002-STATE-CURRENT
                WHERE DS_RIGA          = :PMO-DS-RIGA
           END-EXEC.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A330-SC07-EX.
           EXIT.
      *
      *
      * ****************************************************************
      * UPDATE PARAMETRO MOVIMENTO PER WHERE CONDITION AD HOC
      * ****************************************************************
      *
       A341-UPDATE-WHERE-COND-SC01.
      *
           CONTINUE.
      *
       A341-SC01-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE PARAMETRO MOVIMENTO PER WHERE CONDITION AD HOC
      * ****************************************************************
      *
       A340-UPDATE-WHERE-COND-SC02.
      *
           CONTINUE.
      *
       A340-SC02-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE PARAMETRO MOVIMENTO PER WHERE CONDITION AD HOC
      * ****************************************************************
      *
ID67   A340-UPDATE-WHERE-COND-SC03.
      *
           CONTINUE.
      *
       A340-SC03-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE PARAMETRO MOVIMENTO PER WHERE CONDITION AD HOC
      * ****************************************************************
      *
ID67   A340-UPDATE-WHERE-COND-SC04.
      *
           CONTINUE.
      *
       A340-SC04-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE PARAMETRO MOVIMENTO PER WHERE CONDITION AD HOC
      * ****************************************************************
      *
MIGR   A340-UPDATE-WHERE-COND-SC05.
      *
           CONTINUE.
      *
       A340-SC05-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE PARAMETRO MOVIMENTO PER WHERE CONDITION AD HOC
      * ****************************************************************
      *
MIGR   A340-UPDATE-WHERE-COND-SC06.
      *
           CONTINUE.
      *
       A340-SC06-EX.
           EXIT.
      *
      * ****************************************************************
      * UPDATE PARAMETRO MOVIMENTO PER WHERE CONDITION AD HOC
      * ****************************************************************
      *
MIGR   A340-UPDATE-WHERE-COND-SC07.
      *
           CONTINUE.
      *
       A340-SC07-EX.
           EXIT.
      *
      ******************************************************************
      *
       A346-UPDATE-FIRST-ACTION-SC01.
      *
           CONTINUE.
      *
       A346-SC01-EX.
           EXIT.
      *
      ******************************************************************
      *
       A345-UPDATE-FIRST-ACTION-SC02.
      *
           CONTINUE.
      *
       A345-SC02-EX.
           EXIT.
      *
      ******************************************************************
      *
ID67   A345-UPDATE-FIRST-ACTION-SC03.
      *
           CONTINUE.
      *
       A345-SC03-EX.
           EXIT.
      *
      ******************************************************************
      *
ID67   A345-UPDATE-FIRST-ACTION-SC04.
      *
           CONTINUE.
      *
       A345-SC04-EX.
           EXIT.
      *
      ******************************************************************
      *
MIGR   A345-UPDATE-FIRST-ACTION-SC05.
      *
           CONTINUE.
      *
       A345-SC05-EX.
           EXIT.
      *
      ******************************************************************
      *
MIGR   A345-UPDATE-FIRST-ACTION-SC06.
      *
           CONTINUE.
      *
       A345-SC06-EX.
           EXIT.
      *
      ******************************************************************
      *
MIGR   A345-UPDATE-FIRST-ACTION-SC07.
      *
           CONTINUE.
      *
       A345-SC07-EX.
           EXIT.
      *
      * ****************************************************************
      * CONTROLLA COMMIT
      * ****************************************************************
       A350-CTRL-COMMIT.

           CONTINUE.

       A350-EX.
           EXIT.
      *
      * ****************************************************************
      * APERTURA CURSORE
      * ****************************************************************
      *
       A361-OPEN-CURSOR-SC01.
      *
           PERFORM A301-DECLARE-CURSOR-SC01
              THRU A301-SC01-EX.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   OPEN CUR-PMO-CCAS-RANGE
              END-EXEC
           ELSE
              EXEC SQL
                   OPEN CUR-PMO-CCAS
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A361-SC01-EX.
           EXIT.
      *
      * ****************************************************************
      * APERTURA CURSORE
      * ****************************************************************
      *
       A360-OPEN-CURSOR-SC02.
      *
           PERFORM A305-DECLARE-CURSOR-SC02
              THRU A305-SC02-EX.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   OPEN CUR-PMO-RANGE
              END-EXEC
           ELSE
              EXEC SQL
                   OPEN CUR-PMO
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A360-SC02-EX.
           EXIT.
      *
      * ****************************************************************
      * APERTURA CURSORE
      * ****************************************************************
      *
ID67   A360-OPEN-CURSOR-SC03.
      *
           PERFORM A305-DECLARE-CURSOR-SC03
              THRU A305-SC03-EX.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   OPEN CUR-PMO-RAMO-RANGE
              END-EXEC
           ELSE
              EXEC SQL
ID67               OPEN CUR-PMO-RAMO
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A360-SC03-EX.
           EXIT.
      *
      * ****************************************************************
      * APERTURA CURSORE
      * ****************************************************************
      *
ID67   A360-OPEN-CURSOR-SC04.
      *
           PERFORM A305-DECLARE-CURSOR-SC04
              THRU A305-SC04-EX.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   OPEN CUR-PMO-RANGE
              END-EXEC
           ELSE
              EXEC SQL
ID67               OPEN CUR-PMO
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A360-SC04-EX.
           EXIT.
      *
      * ****************************************************************
      * APERTURA CURSORE
      * ****************************************************************
      *
MIGR   A360-OPEN-CURSOR-SC05.
      *
           PERFORM A305-DECLARE-CURSOR-SC05
              THRU A305-SC05-EX.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   OPEN CUR-PMO-RANGE-C
              END-EXEC
           ELSE
              EXEC SQL
                   OPEN CUR-PMO-C
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A360-SC05-EX.
           EXIT.
      *
      * ****************************************************************
      * APERTURA CURSORE
      * ****************************************************************
      *
MIGR   A360-OPEN-CURSOR-SC06.
      *
           PERFORM A305-DECLARE-CURSOR-SC06
              THRU A305-SC06-EX.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   OPEN CUR-PMO-RAMO-RANGE-C
              END-EXEC
           ELSE
              EXEC SQL
ID67               OPEN CUR-PMO-RAMO-C
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A360-SC06-EX.
           EXIT.
      *
      * ****************************************************************
      * APERTURA CURSORE
      * ****************************************************************
      *
MIGR   A360-OPEN-CURSOR-SC07.
      *
           PERFORM A305-DECLARE-CURSOR-SC07
              THRU A305-SC07-EX.
      *
           EXEC SQL
ID67            OPEN CUR-PMO-POL-C
           END-EXEC
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A360-SC07-EX.
           EXIT.
      *
      * ****************************************************************
      * CHIUSURA CURSORE
      * ****************************************************************
      *
       A371-CLOSE-CURSOR-SC01.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   CLOSE CUR-PMO-CCAS-RANGE
              END-EXEC
           ELSE
              EXEC SQL
ID67               CLOSE CUR-PMO-CCAS
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A371-SC01-EX.
           EXIT.
      *
      * ****************************************************************
      * CHIUSURA CURSORE
      * ****************************************************************
      *
       A370-CLOSE-CURSOR-SC02.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   CLOSE CUR-PMO-RANGE
              END-EXEC
           ELSE
              EXEC SQL
ID67               CLOSE CUR-PMO
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A370-SC02-EX.
           EXIT.
      *
      * ****************************************************************
      * CHIUSURA CURSORE
      * ****************************************************************
      *
ID67   A370-CLOSE-CURSOR-SC03.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   CLOSE CUR-PMO-RAMO-RANGE
              END-EXEC
           ELSE
              EXEC SQL
ID67               CLOSE CUR-PMO-RAMO
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A370-SC03-EX.
           EXIT.
      *
      * ****************************************************************
      * CHIUSURA CURSORE
      * ****************************************************************
      *
ID67   A370-CLOSE-CURSOR-SC04.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   CLOSE CUR-PMO-POL-RANGE
              END-EXEC
           ELSE
              EXEC SQL
ID67               CLOSE CUR-PMO-POL
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A370-SC04-EX.
           EXIT.
      *
      * ****************************************************************
      * CHIUSURA CURSORE
      * ****************************************************************
      *
MIGR   A370-CLOSE-CURSOR-SC05.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   CLOSE CUR-PMO-RANGE-C
              END-EXEC
           ELSE
              EXEC SQL
ID67               CLOSE CUR-PMO-C
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A370-SC05-EX.
           EXIT.
      *
      * ****************************************************************
      * CHIUSURA CURSORE
      * ****************************************************************
      *
MIGR   A370-CLOSE-CURSOR-SC06.
      *
           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   CLOSE CUR-PMO-RAMO-RANGE-C
              END-EXEC
           ELSE
              EXEC SQL
ID67               CLOSE CUR-PMO-RAMO-C
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A370-SC06-EX.
           EXIT.
      *
      * ****************************************************************
      * CHIUSURA CURSORE
      * ****************************************************************
      *
MIGR   A370-CLOSE-CURSOR-SC07.
      *
           EXEC SQL
ID67            CLOSE CUR-PMO-POL-C
           END-EXEC
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
       A370-SC07-EX.
           EXIT.
      *
      * ****************************************************************
      * PRIMA FETCH SU TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
       A381-FETCH-FIRST-SC01.
      *
           PERFORM A361-OPEN-CURSOR-SC01
              THRU A361-SC01-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A391-FETCH-NEXT-SC01
                 THRU A391-SC01-EX
           END-IF.
      *
       A381-SC01-EX.
           EXIT.
      *
      * ****************************************************************
      * PRIMA FETCH SU TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
       A380-FETCH-FIRST-SC02.
      *
           PERFORM A360-OPEN-CURSOR-SC02
              THRU A360-SC02-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-SC02
                 THRU A390-SC02-EX
           END-IF.
      *
       A380-SC02-EX.
           EXIT.
      *
      * ****************************************************************
      * PRIMA FETCH SU TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
ID67   A380-FETCH-FIRST-SC03.
      *
           PERFORM A360-OPEN-CURSOR-SC03
              THRU A360-SC03-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-SC03
                 THRU A390-SC03-EX
           END-IF.
      *
       A380-SC03-EX.
           EXIT.
      *
      * ****************************************************************
      * PRIMA FETCH SU TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
ID67   A380-FETCH-FIRST-SC04.
      *
           PERFORM A360-OPEN-CURSOR-SC04
              THRU A360-SC04-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-SC04
                 THRU A390-SC04-EX
           END-IF.
      *
       A380-SC04-EX.
           EXIT.
      *
      * ****************************************************************
      * PRIMA FETCH SU TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
MIGR   A380-FETCH-FIRST-SC05.
      *
           PERFORM A360-OPEN-CURSOR-SC05
              THRU A360-SC05-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-SC05
                 THRU A390-SC05-EX
           END-IF.
      *
       A380-SC05-EX.
           EXIT.
      *
      * ****************************************************************
      * PRIMA FETCH SU TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
MIGR   A380-FETCH-FIRST-SC06.
      *
           PERFORM A360-OPEN-CURSOR-SC06
              THRU A360-SC06-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-SC06
                 THRU A390-SC06-EX
           END-IF.
      *
       A380-SC06-EX.
           EXIT.
      *
      * ****************************************************************
      * PRIMA FETCH SU TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
MIGR   A380-FETCH-FIRST-SC07.
      *
           PERFORM A360-OPEN-CURSOR-SC07
              THRU A360-SC07-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-SC07
                 THRU A390-SC07-EX
           END-IF.
      *
       A380-SC07-EX.
           EXIT.
      *
      * ****************************************************************
      * FETCH SUCCESSIVE SULLA TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
       A391-FETCH-NEXT-SC01.

           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   FETCH CUR-PMO-CCAS-RANGE
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
              END-EXEC
           ELSE
              EXEC SQL
                   FETCH CUR-PMO-CCAS
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL
                 THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N
                 THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A371-CLOSE-CURSOR-SC01
                   THRU A371-SC01-EX

                IF IDSV0003-SUCCESSFUL-SQL

                    SET IDSV0003-NOT-FOUND
                     TO TRUE

                END-IF
             END-IF
           END-IF.
      *
       A391-SC01-EX.
           EXIT.
      *
      * ****************************************************************
      * FETCH SUCCESSIVE SULLA TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
       A390-FETCH-NEXT-SC02.

           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   FETCH CUR-PMO-RANGE
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
              END-EXEC
           ELSE
              EXEC SQL
                   FETCH CUR-PMO
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL
                 THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N
                 THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC02
                   THRU A370-SC02-EX

                IF IDSV0003-SUCCESSFUL-SQL

                    SET IDSV0003-NOT-FOUND
                     TO TRUE

                END-IF
             END-IF
           END-IF.
      *
       A390-SC02-EX.
           EXIT.
      *
      * ****************************************************************
      * FETCH SUCCESSIVE SULLA TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
ID67   A390-FETCH-NEXT-SC03.

           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   FETCH CUR-PMO-RAMO-RANGE
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
              END-EXEC
           ELSE
              EXEC SQL
                   FETCH CUR-PMO-RAMO
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL
                 THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N
                 THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC03
                   THRU A370-SC03-EX

                IF IDSV0003-SUCCESSFUL-SQL

                    SET IDSV0003-NOT-FOUND
                     TO TRUE

                END-IF
             END-IF
           END-IF.
      *
       A390-SC03-EX.
           EXIT.
      *
      * ****************************************************************
      * FETCH SUCCESSIVE SULLA TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
ID67   A390-FETCH-NEXT-SC04.

           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   FETCH CUR-PMO-POL-RANGE
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
              END-EXEC
           ELSE
              EXEC SQL
                   FETCH CUR-PMO-POL
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL
                 THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N
                 THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC04
                   THRU A370-SC04-EX

                IF IDSV0003-SUCCESSFUL-SQL

                    SET IDSV0003-NOT-FOUND
                     TO TRUE

                END-IF
             END-IF
           END-IF.
      *
       A390-SC04-EX.
           EXIT.
      *
      * ****************************************************************
      * FETCH SUCCESSIVE SULLA TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
MIGR   A390-FETCH-NEXT-SC05.

           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   FETCH CUR-PMO-RANGE-C
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
              END-EXEC
           ELSE
              EXEC SQL
                   FETCH CUR-PMO-C
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL
                 THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N
                 THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC05
                   THRU A370-SC05-EX

                IF IDSV0003-SUCCESSFUL-SQL

                    SET IDSV0003-NOT-FOUND
                     TO TRUE

                END-IF
             END-IF
           END-IF.
      *
       A390-SC05-EX.
           EXIT.
      *
      * ****************************************************************
      * FETCH SUCCESSIVE SULLA TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
MIGR   A390-FETCH-NEXT-SC06.

           IF ACCESSO-X-RANGE-SI
              EXEC SQL
                   FETCH CUR-PMO-RAMO-RANGE-C
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
              END-EXEC
           ELSE
              EXEC SQL
                   FETCH CUR-PMO-RAMO-C
              INTO
                   :PMO-ID-PARAM-MOVI
                  ,:PMO-ID-OGG
                  ,:PMO-TP-OGG
                  ,:PMO-ID-MOVI-CRZ
                  ,:PMO-ID-MOVI-CHIU
                   :IND-PMO-ID-MOVI-CHIU
                  ,:PMO-DT-INI-EFF-DB
                  ,:PMO-DT-END-EFF-DB
                  ,:PMO-COD-COMP-ANIA
                  ,:PMO-TP-MOVI
                   :IND-PMO-TP-MOVI
                  ,:PMO-FRQ-MOVI
                   :IND-PMO-FRQ-MOVI
                  ,:PMO-DUR-AA
                   :IND-PMO-DUR-AA
                  ,:PMO-DUR-MM
                   :IND-PMO-DUR-MM
                  ,:PMO-DUR-GG
                   :IND-PMO-DUR-GG
                  ,:PMO-DT-RICOR-PREC-DB
                   :IND-PMO-DT-RICOR-PREC
                  ,:PMO-DT-RICOR-SUCC-DB
                   :IND-PMO-DT-RICOR-SUCC
                  ,:PMO-PC-INTR-FRAZ
                   :IND-PMO-PC-INTR-FRAZ
                  ,:PMO-IMP-BNS-DA-SCO-TOT
                   :IND-PMO-IMP-BNS-DA-SCO-TOT
                  ,:PMO-IMP-BNS-DA-SCO
                   :IND-PMO-IMP-BNS-DA-SCO
                  ,:PMO-PC-ANTIC-BNS
                   :IND-PMO-PC-ANTIC-BNS
                  ,:PMO-TP-RINN-COLL
                   :IND-PMO-TP-RINN-COLL
                  ,:PMO-TP-RIVAL-PRE
                   :IND-PMO-TP-RIVAL-PRE
                  ,:PMO-TP-RIVAL-PRSTZ
                   :IND-PMO-TP-RIVAL-PRSTZ
                  ,:PMO-FL-EVID-RIVAL
                   :IND-PMO-FL-EVID-RIVAL
                  ,:PMO-ULT-PC-PERD
                   :IND-PMO-ULT-PC-PERD
                  ,:PMO-TOT-AA-GIA-PROR
                   :IND-PMO-TOT-AA-GIA-PROR
                  ,:PMO-TP-OPZ
                   :IND-PMO-TP-OPZ
                  ,:PMO-AA-REN-CER
                   :IND-PMO-AA-REN-CER
                  ,:PMO-PC-REVRSB
                   :IND-PMO-PC-REVRSB
                  ,:PMO-IMP-RISC-PARZ-PRGT
                   :IND-PMO-IMP-RISC-PARZ-PRGT
                  ,:PMO-IMP-LRD-DI-RAT
                   :IND-PMO-IMP-LRD-DI-RAT
                  ,:PMO-IB-OGG
                   :IND-PMO-IB-OGG
                  ,:PMO-COS-ONER
                   :IND-PMO-COS-ONER
                  ,:PMO-SPE-PC
                   :IND-PMO-SPE-PC
                  ,:PMO-FL-ATTIV-GAR
                   :IND-PMO-FL-ATTIV-GAR
                  ,:PMO-CAMBIO-VER-PROD
                   :IND-PMO-CAMBIO-VER-PROD
                  ,:PMO-MM-DIFF
                   :IND-PMO-MM-DIFF
                  ,:PMO-IMP-RAT-MANFEE
                   :IND-PMO-IMP-RAT-MANFEE
                  ,:PMO-DT-ULT-EROG-MANFEE-DB
                   :IND-PMO-DT-ULT-EROG-MANFEE
                  ,:PMO-TP-OGG-RIVAL
                   :IND-PMO-TP-OGG-RIVAL
                  ,:PMO-SOM-ASSTA-GARAC
                   :IND-PMO-SOM-ASSTA-GARAC
                  ,:PMO-PC-APPLZ-OPZ
                   :IND-PMO-PC-APPLZ-OPZ
                  ,:PMO-ID-ADES
                   :IND-PMO-ID-ADES
                  ,:PMO-ID-POLI
                  ,:PMO-TP-FRM-ASSVA
                  ,:PMO-DS-RIGA
                  ,:PMO-DS-OPER-SQL
                  ,:PMO-DS-VER
                  ,:PMO-DS-TS-INI-CPTZ
                  ,:PMO-DS-TS-END-CPTZ
                  ,:PMO-DS-UTENTE
                  ,:PMO-DS-STATO-ELAB
                  ,:PMO-TP-ESTR-CNT
                   :IND-PMO-TP-ESTR-CNT
ID67              ,:PMO-COD-RAMO
ID67               :IND-PMO-COD-RAMO
                  ,:PMO-GEN-DA-SIN
                   :IND-PMO-GEN-DA-SIN
                  ,:PMO-COD-TARI
                   :IND-PMO-COD-TARI
                  ,:PMO-NUM-RAT-PAG-PRE
                   :IND-PMO-NUM-RAT-PAG-PRE
                  ,:PMO-PC-SERV-VAL
                   :IND-PMO-PC-SERV-VAL
                  ,:PMO-ETA-AA-SOGL-BNFICR
                   :IND-PMO-ETA-AA-SOGL-BNFICR
              END-EXEC
           END-IF.
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL
                 THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N
                 THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC06
                   THRU A370-SC06-EX

                IF IDSV0003-SUCCESSFUL-SQL

                    SET IDSV0003-NOT-FOUND
                     TO TRUE

                END-IF
             END-IF
           END-IF.
      *
       A390-SC06-EX.
           EXIT.
      *
      * ****************************************************************
      * FETCH SUCCESSIVE SULLA TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
MIGR   A390-FETCH-NEXT-SC07.

           EXEC SQL
                FETCH CUR-PMO-POL-C
           INTO
                :PMO-ID-PARAM-MOVI
               ,:PMO-ID-OGG
               ,:PMO-TP-OGG
               ,:PMO-ID-MOVI-CRZ
               ,:PMO-ID-MOVI-CHIU
                :IND-PMO-ID-MOVI-CHIU
               ,:PMO-DT-INI-EFF-DB
               ,:PMO-DT-END-EFF-DB
               ,:PMO-COD-COMP-ANIA
               ,:PMO-TP-MOVI
                :IND-PMO-TP-MOVI
               ,:PMO-FRQ-MOVI
                :IND-PMO-FRQ-MOVI
               ,:PMO-DUR-AA
                :IND-PMO-DUR-AA
               ,:PMO-DUR-MM
                :IND-PMO-DUR-MM
               ,:PMO-DUR-GG
                :IND-PMO-DUR-GG
               ,:PMO-DT-RICOR-PREC-DB
                :IND-PMO-DT-RICOR-PREC
               ,:PMO-DT-RICOR-SUCC-DB
                :IND-PMO-DT-RICOR-SUCC
               ,:PMO-PC-INTR-FRAZ
                :IND-PMO-PC-INTR-FRAZ
               ,:PMO-IMP-BNS-DA-SCO-TOT
                :IND-PMO-IMP-BNS-DA-SCO-TOT
               ,:PMO-IMP-BNS-DA-SCO
                :IND-PMO-IMP-BNS-DA-SCO
               ,:PMO-PC-ANTIC-BNS
                :IND-PMO-PC-ANTIC-BNS
               ,:PMO-TP-RINN-COLL
                :IND-PMO-TP-RINN-COLL
               ,:PMO-TP-RIVAL-PRE
                :IND-PMO-TP-RIVAL-PRE
               ,:PMO-TP-RIVAL-PRSTZ
                :IND-PMO-TP-RIVAL-PRSTZ
               ,:PMO-FL-EVID-RIVAL
                :IND-PMO-FL-EVID-RIVAL
               ,:PMO-ULT-PC-PERD
                :IND-PMO-ULT-PC-PERD
               ,:PMO-TOT-AA-GIA-PROR
                :IND-PMO-TOT-AA-GIA-PROR
               ,:PMO-TP-OPZ
                :IND-PMO-TP-OPZ
               ,:PMO-AA-REN-CER
                :IND-PMO-AA-REN-CER
               ,:PMO-PC-REVRSB
                :IND-PMO-PC-REVRSB
               ,:PMO-IMP-RISC-PARZ-PRGT
                :IND-PMO-IMP-RISC-PARZ-PRGT
               ,:PMO-IMP-LRD-DI-RAT
                :IND-PMO-IMP-LRD-DI-RAT
               ,:PMO-IB-OGG
                :IND-PMO-IB-OGG
               ,:PMO-COS-ONER
                :IND-PMO-COS-ONER
               ,:PMO-SPE-PC
                :IND-PMO-SPE-PC
               ,:PMO-FL-ATTIV-GAR
                :IND-PMO-FL-ATTIV-GAR
               ,:PMO-CAMBIO-VER-PROD
                :IND-PMO-CAMBIO-VER-PROD
               ,:PMO-MM-DIFF
                :IND-PMO-MM-DIFF
               ,:PMO-IMP-RAT-MANFEE
                :IND-PMO-IMP-RAT-MANFEE
               ,:PMO-DT-ULT-EROG-MANFEE-DB
                :IND-PMO-DT-ULT-EROG-MANFEE
               ,:PMO-TP-OGG-RIVAL
                :IND-PMO-TP-OGG-RIVAL
               ,:PMO-SOM-ASSTA-GARAC
                :IND-PMO-SOM-ASSTA-GARAC
               ,:PMO-PC-APPLZ-OPZ
                :IND-PMO-PC-APPLZ-OPZ
               ,:PMO-ID-ADES
                :IND-PMO-ID-ADES
               ,:PMO-ID-POLI
               ,:PMO-TP-FRM-ASSVA
               ,:PMO-DS-RIGA
               ,:PMO-DS-OPER-SQL
               ,:PMO-DS-VER
               ,:PMO-DS-TS-INI-CPTZ
               ,:PMO-DS-TS-END-CPTZ
               ,:PMO-DS-UTENTE
               ,:PMO-DS-STATO-ELAB
               ,:PMO-TP-ESTR-CNT
                :IND-PMO-TP-ESTR-CNT
ID67           ,:PMO-COD-RAMO
ID67            :IND-PMO-COD-RAMO
               ,:PMO-GEN-DA-SIN
                :IND-PMO-GEN-DA-SIN
               ,:PMO-COD-TARI
                :IND-PMO-COD-TARI
               ,:PMO-NUM-RAT-PAG-PRE
                :IND-PMO-NUM-RAT-PAG-PRE
               ,:PMO-PC-SERV-VAL
                :IND-PMO-PC-SERV-VAL
               ,:PMO-ETA-AA-SOGL-BNFICR
                :IND-PMO-ETA-AA-SOGL-BNFICR
           END-EXEC
      *
           PERFORM A100-CHECK-RETURN-CODE
              THRU A100-EX.
      *
           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL
                 THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N
                 THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC07
                   THRU A370-SC07-EX

                IF IDSV0003-SUCCESSFUL-SQL

                    SET IDSV0003-NOT-FOUND
                     TO TRUE

                END-IF
             END-IF
           END-IF.
      *
       A390-SC07-EX.
           EXIT.
      *
      * ****************************************************************
      * OPERAZIONI FINALI
      * ****************************************************************
      *
       A400-FINE.
      *
           CONTINUE.
      *
       A400-EX.
           EXIT.
      ******************************************************************
       SC01-SELECTION-CURSOR-01.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A311-SELECT-SC01
                    THRU A311-SC01-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A361-OPEN-CURSOR-SC01
                    THRU A361-SC01-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A371-CLOSE-CURSOR-SC01
                    THRU A371-SC01-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A381-FETCH-FIRST-SC01
                    THRU A381-SC01-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A391-FETCH-NEXT-SC01
                    THRU A391-SC01-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A321-UPDATE-SC01
                    THRU A321-SC01-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER
                  TO TRUE
           END-EVALUATE.

       SC01-EX.
           EXIT.
      *
      ******************************************************************
       SC02-SELECTION-CURSOR-02.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC02
                    THRU A310-SC02-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC02
                    THRU A360-SC02-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC02
                    THRU A370-SC02-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC02
                    THRU A380-SC02-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC02
                    THRU A390-SC02-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC02
                    THRU A320-SC02-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER
                  TO TRUE
           END-EVALUATE.

       SC02-EX.
           EXIT.
      *
      ******************************************************************
ID67   SC03-SELECTION-CURSOR-03.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC03
                    THRU A310-SC03-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC03
                    THRU A360-SC03-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC03
                    THRU A370-SC03-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC03
                    THRU A380-SC03-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC03
                    THRU A390-SC03-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC03
                    THRU A320-SC03-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER
                  TO TRUE
           END-EVALUATE.

       SC03-EX.
           EXIT.
      *
      ******************************************************************
ID67   SC04-SELECTION-CURSOR-04.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC04
                    THRU A310-SC04-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC04
                    THRU A360-SC04-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC04
                    THRU A370-SC04-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC04
                    THRU A380-SC04-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC04
                    THRU A390-SC04-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC04
                    THRU A320-SC04-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER
                  TO TRUE
           END-EVALUATE.

       SC04-EX.
           EXIT.
      *
      ******************************************************************
MIGR   SC05-SELECTION-CURSOR-05.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC05
                    THRU A310-SC05-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC05
                    THRU A360-SC05-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC05
                    THRU A370-SC05-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC05
                    THRU A380-SC05-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC05
                    THRU A390-SC05-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC05
                    THRU A320-SC05-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER
                  TO TRUE
           END-EVALUATE.

       SC05-EX.
           EXIT.
      *
      ******************************************************************
MIGR   SC06-SELECTION-CURSOR-06.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC06
                    THRU A310-SC06-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC06
                    THRU A360-SC06-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC06
                    THRU A370-SC06-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC06
                    THRU A380-SC06-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC06
                    THRU A390-SC06-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC06
                    THRU A320-SC06-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER
                  TO TRUE
           END-EVALUATE.

       SC06-EX.
           EXIT.
      *
      ******************************************************************
MIGR   SC07-SELECTION-CURSOR-07.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC07
                    THRU A310-SC07-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC07
                    THRU A360-SC07-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC07
                    THRU A370-SC07-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC07
                    THRU A380-SC07-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC07
                    THRU A390-SC07-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC07
                    THRU A320-SC07-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER
                  TO TRUE
           END-EVALUATE.

       SC07-EX.
           EXIT.
      *
      * ****************************************************************
      * SETTAGGIO CAMPI NULL DELLA TABELLA PARAMETRO MOVIMENTO
      * ****************************************************************
      *
       Z100-SET-COLONNE-NULL.
      *
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
      *
      *  --> PARAMETRO MOVIMENTO
      *
           IF IND-PMO-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO PMO-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-PMO-TP-MOVI = -1
              MOVE HIGH-VALUES TO PMO-TP-MOVI-NULL
           END-IF
           IF IND-PMO-FRQ-MOVI = -1
              MOVE HIGH-VALUES TO PMO-FRQ-MOVI-NULL
           END-IF
           IF IND-PMO-DUR-AA = -1
              MOVE HIGH-VALUES TO PMO-DUR-AA-NULL
           END-IF
           IF IND-PMO-DUR-MM = -1
              MOVE HIGH-VALUES TO PMO-DUR-MM-NULL
           END-IF
           IF IND-PMO-DUR-GG = -1
              MOVE HIGH-VALUES TO PMO-DUR-GG-NULL
           END-IF
           IF IND-PMO-DT-RICOR-PREC = -1
              MOVE HIGH-VALUES TO PMO-DT-RICOR-PREC-NULL
           END-IF
           IF IND-PMO-DT-RICOR-SUCC = -1
              MOVE HIGH-VALUES TO PMO-DT-RICOR-SUCC-NULL
           END-IF
           IF IND-PMO-PC-INTR-FRAZ = -1
              MOVE HIGH-VALUES TO PMO-PC-INTR-FRAZ-NULL
           END-IF
           IF IND-PMO-IMP-BNS-DA-SCO-TOT = -1
              MOVE HIGH-VALUES TO PMO-IMP-BNS-DA-SCO-TOT-NULL
           END-IF
           IF IND-PMO-IMP-BNS-DA-SCO = -1
              MOVE HIGH-VALUES TO PMO-IMP-BNS-DA-SCO-NULL
           END-IF
           IF IND-PMO-PC-ANTIC-BNS = -1
              MOVE HIGH-VALUES TO PMO-PC-ANTIC-BNS-NULL
           END-IF
           IF IND-PMO-TP-RINN-COLL = -1
              MOVE HIGH-VALUES TO PMO-TP-RINN-COLL-NULL
           END-IF
           IF IND-PMO-TP-RIVAL-PRE = -1
              MOVE HIGH-VALUES TO PMO-TP-RIVAL-PRE-NULL
           END-IF
           IF IND-PMO-TP-RIVAL-PRSTZ = -1
              MOVE HIGH-VALUES TO PMO-TP-RIVAL-PRSTZ-NULL
           END-IF
           IF IND-PMO-FL-EVID-RIVAL = -1
              MOVE HIGH-VALUES TO PMO-FL-EVID-RIVAL-NULL
           END-IF
           IF IND-PMO-ULT-PC-PERD = -1
              MOVE HIGH-VALUES TO PMO-ULT-PC-PERD-NULL
           END-IF
           IF IND-PMO-TOT-AA-GIA-PROR = -1
              MOVE HIGH-VALUES TO PMO-TOT-AA-GIA-PROR-NULL
           END-IF
           IF IND-PMO-TP-OPZ = -1
              MOVE HIGH-VALUES TO PMO-TP-OPZ-NULL
           END-IF
           IF IND-PMO-AA-REN-CER = -1
              MOVE HIGH-VALUES TO PMO-AA-REN-CER-NULL
           END-IF
           IF IND-PMO-PC-REVRSB = -1
              MOVE HIGH-VALUES TO PMO-PC-REVRSB-NULL
           END-IF
           IF IND-PMO-IMP-RISC-PARZ-PRGT = -1
              MOVE HIGH-VALUES TO PMO-IMP-RISC-PARZ-PRGT-NULL
           END-IF
           IF IND-PMO-IMP-LRD-DI-RAT = -1
              MOVE HIGH-VALUES TO PMO-IMP-LRD-DI-RAT-NULL
           END-IF
           IF IND-PMO-IB-OGG = -1
              MOVE HIGH-VALUES TO PMO-IB-OGG-NULL
           END-IF
           IF IND-PMO-COS-ONER = -1
              MOVE HIGH-VALUES TO PMO-COS-ONER-NULL
           END-IF
           IF IND-PMO-SPE-PC = -1
              MOVE HIGH-VALUES TO PMO-SPE-PC-NULL
           END-IF
           IF IND-PMO-FL-ATTIV-GAR = -1
              MOVE HIGH-VALUES TO PMO-FL-ATTIV-GAR-NULL
           END-IF
           IF IND-PMO-CAMBIO-VER-PROD = -1
              MOVE HIGH-VALUES TO PMO-CAMBIO-VER-PROD-NULL
           END-IF
           IF IND-PMO-MM-DIFF = -1
              MOVE HIGH-VALUES TO PMO-MM-DIFF-NULL
           END-IF
           IF IND-PMO-IMP-RAT-MANFEE = -1
              MOVE HIGH-VALUES TO PMO-IMP-RAT-MANFEE-NULL
           END-IF
           IF IND-PMO-DT-ULT-EROG-MANFEE = -1
              MOVE HIGH-VALUES TO PMO-DT-ULT-EROG-MANFEE-NULL
           END-IF
           IF IND-PMO-TP-OGG-RIVAL = -1
              MOVE HIGH-VALUES TO PMO-TP-OGG-RIVAL-NULL
           END-IF
           IF IND-PMO-SOM-ASSTA-GARAC = -1
              MOVE HIGH-VALUES TO PMO-SOM-ASSTA-GARAC-NULL
           END-IF
           IF IND-PMO-PC-APPLZ-OPZ = -1
              MOVE HIGH-VALUES TO PMO-PC-APPLZ-OPZ-NULL
           END-IF
           IF IND-PMO-ID-ADES = -1
              MOVE HIGH-VALUES TO PMO-ID-ADES-NULL
           END-IF
           IF IND-PMO-TP-ESTR-CNT = -1
              MOVE HIGH-VALUES TO PMO-TP-ESTR-CNT-NULL
           END-IF
ID67       IF IND-PMO-COD-RAMO = -1
              MOVE HIGH-VALUES TO PMO-COD-RAMO-NULL
ID67       END-IF
           IF IND-PMO-GEN-DA-SIN = -1
              MOVE HIGH-VALUES TO PMO-GEN-DA-SIN-NULL
           END-IF
           IF IND-PMO-COD-TARI = -1
              MOVE HIGH-VALUES TO PMO-COD-TARI-NULL
           END-IF
           IF IND-PMO-NUM-RAT-PAG-PRE = -1
              MOVE HIGH-VALUES TO PMO-NUM-RAT-PAG-PRE-NULL
           END-IF
           IF IND-PMO-PC-SERV-VAL = -1
              MOVE HIGH-VALUES TO PMO-PC-SERV-VAL-NULL
           END-IF
           IF IND-PMO-ETA-AA-SOGL-BNFICR = -1
              MOVE HIGH-VALUES TO PMO-ETA-AA-SOGL-BNFICR-NULL
           END-IF.
      *
       Z100-EX.
           EXIT.
      *
      * ****************************************************************
      * VALORIZZAZZIONE CAMPI DATA SERVICES.
      * ****************************************************************
      *
       Z150-VALORIZZA-DATA-SERVICES.
      *
----->     MOVE IDSV0003-OPERAZIONE     TO PMO-DS-OPER-SQL.

----->*    MOVE 1                       TO PMO-DS-VER.

----->     MOVE IDSV0003-USER-NAME      TO PMO-DS-UTENTE.
      *
       Z150-EX.
           EXIT.
      *
       Z200-SET-INDICATORI-NULL.
      *
      *  --> PARAMETRO MOVIMENTO
      *
           IF PMO-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-PMO-ID-MOVI-CHIU
           END-IF
           IF PMO-TP-MOVI-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-MOVI
           ELSE
              MOVE 0 TO IND-PMO-TP-MOVI
           END-IF
           IF PMO-FRQ-MOVI-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-FRQ-MOVI
           ELSE
              MOVE 0 TO IND-PMO-FRQ-MOVI
           END-IF
           IF PMO-DUR-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DUR-AA
           ELSE
              MOVE 0 TO IND-PMO-DUR-AA
           END-IF
           IF PMO-DUR-MM-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DUR-MM
           ELSE
              MOVE 0 TO IND-PMO-DUR-MM
           END-IF
           IF PMO-DUR-GG-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DUR-GG
           ELSE
              MOVE 0 TO IND-PMO-DUR-GG
           END-IF
           IF PMO-DT-RICOR-PREC-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DT-RICOR-PREC
           ELSE
              MOVE 0 TO IND-PMO-DT-RICOR-PREC
           END-IF
           IF PMO-DT-RICOR-SUCC-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DT-RICOR-SUCC
           ELSE
              MOVE 0 TO IND-PMO-DT-RICOR-SUCC
           END-IF
           IF PMO-PC-INTR-FRAZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-PC-INTR-FRAZ
           ELSE
              MOVE 0 TO IND-PMO-PC-INTR-FRAZ
           END-IF
           IF PMO-IMP-BNS-DA-SCO-TOT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IMP-BNS-DA-SCO-TOT
           ELSE
              MOVE 0 TO IND-PMO-IMP-BNS-DA-SCO-TOT
           END-IF
           IF PMO-IMP-BNS-DA-SCO-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IMP-BNS-DA-SCO
           ELSE
              MOVE 0 TO IND-PMO-IMP-BNS-DA-SCO
           END-IF
           IF PMO-PC-ANTIC-BNS-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-PC-ANTIC-BNS
           ELSE
              MOVE 0 TO IND-PMO-PC-ANTIC-BNS
           END-IF
           IF PMO-TP-RINN-COLL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-RINN-COLL
           ELSE
              MOVE 0 TO IND-PMO-TP-RINN-COLL
           END-IF
           IF PMO-TP-RIVAL-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-RIVAL-PRE
           ELSE
              MOVE 0 TO IND-PMO-TP-RIVAL-PRE
           END-IF
           IF PMO-TP-RIVAL-PRSTZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-RIVAL-PRSTZ
           ELSE
              MOVE 0 TO IND-PMO-TP-RIVAL-PRSTZ
           END-IF
           IF PMO-FL-EVID-RIVAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-FL-EVID-RIVAL
           ELSE
              MOVE 0 TO IND-PMO-FL-EVID-RIVAL
           END-IF
           IF PMO-ULT-PC-PERD-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-ULT-PC-PERD
           ELSE
              MOVE 0 TO IND-PMO-ULT-PC-PERD
           END-IF
           IF PMO-TOT-AA-GIA-PROR-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TOT-AA-GIA-PROR
           ELSE
              MOVE 0 TO IND-PMO-TOT-AA-GIA-PROR
           END-IF
           IF PMO-TP-OPZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-OPZ
           ELSE
              MOVE 0 TO IND-PMO-TP-OPZ
           END-IF
           IF PMO-AA-REN-CER-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-AA-REN-CER
           ELSE
              MOVE 0 TO IND-PMO-AA-REN-CER
           END-IF
           IF PMO-PC-REVRSB-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-PC-REVRSB
           ELSE
              MOVE 0 TO IND-PMO-PC-REVRSB
           END-IF
           IF PMO-IMP-RISC-PARZ-PRGT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IMP-RISC-PARZ-PRGT
           ELSE
              MOVE 0 TO IND-PMO-IMP-RISC-PARZ-PRGT
           END-IF
           IF PMO-IMP-LRD-DI-RAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IMP-LRD-DI-RAT
           ELSE
              MOVE 0 TO IND-PMO-IMP-LRD-DI-RAT
           END-IF
           IF PMO-IB-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IB-OGG
           ELSE
              MOVE 0 TO IND-PMO-IB-OGG
           END-IF
           IF PMO-COS-ONER-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-COS-ONER
           ELSE
              MOVE 0 TO IND-PMO-COS-ONER
           END-IF
           IF PMO-SPE-PC-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-SPE-PC
           ELSE
              MOVE 0 TO IND-PMO-SPE-PC
           END-IF
           IF PMO-FL-ATTIV-GAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-FL-ATTIV-GAR
           ELSE
              MOVE 0 TO IND-PMO-FL-ATTIV-GAR
           END-IF
           IF PMO-CAMBIO-VER-PROD-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-CAMBIO-VER-PROD
           ELSE
              MOVE 0 TO IND-PMO-CAMBIO-VER-PROD
           END-IF
           IF PMO-MM-DIFF-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-MM-DIFF
           ELSE
              MOVE 0 TO IND-PMO-MM-DIFF
           END-IF
           IF PMO-IMP-RAT-MANFEE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-IMP-RAT-MANFEE
           ELSE
              MOVE 0 TO IND-PMO-IMP-RAT-MANFEE
           END-IF
           IF PMO-DT-ULT-EROG-MANFEE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-DT-ULT-EROG-MANFEE
           ELSE
              MOVE 0 TO IND-PMO-DT-ULT-EROG-MANFEE
           END-IF
           IF PMO-TP-OGG-RIVAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-OGG-RIVAL
           ELSE
              MOVE 0 TO IND-PMO-TP-OGG-RIVAL
           END-IF
           IF PMO-SOM-ASSTA-GARAC-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-SOM-ASSTA-GARAC
           ELSE
              MOVE 0 TO IND-PMO-SOM-ASSTA-GARAC
           END-IF
           IF PMO-PC-APPLZ-OPZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-PC-APPLZ-OPZ
           ELSE
              MOVE 0 TO IND-PMO-PC-APPLZ-OPZ
           END-IF
           IF PMO-ID-ADES-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-ID-ADES
           ELSE
              MOVE 0 TO IND-PMO-ID-ADES
           END-IF
ID67       IF PMO-TP-ESTR-CNT-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-TP-ESTR-CNT
           ELSE
              MOVE 0 TO IND-PMO-TP-ESTR-CNT
ID67       END-IF
ID67       IF PMO-COD-RAMO-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-COD-RAMO
           ELSE
              MOVE 0 TO IND-PMO-COD-RAMO
ID67       END-IF
           IF PMO-GEN-DA-SIN-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-GEN-DA-SIN
           ELSE
              MOVE 0 TO IND-PMO-GEN-DA-SIN
           END-IF
           IF PMO-COD-TARI-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-COD-TARI
           ELSE
              MOVE 0 TO IND-PMO-COD-TARI
           END-IF
           IF PMO-NUM-RAT-PAG-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-NUM-RAT-PAG-PRE
           ELSE
              MOVE 0 TO IND-PMO-NUM-RAT-PAG-PRE
           END-IF
           IF PMO-PC-SERV-VAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-PC-SERV-VAL
           ELSE
              MOVE 0 TO IND-PMO-PC-SERV-VAL
           END-IF
           IF PMO-ETA-AA-SOGL-BNFICR-NULL = HIGH-VALUES
              MOVE -1 TO IND-PMO-ETA-AA-SOGL-BNFICR
           ELSE
              MOVE 0 TO IND-PMO-ETA-AA-SOGL-BNFICR
           END-IF.
      *
       Z200-EX.
           EXIT.
      *
       Z900-CONVERTI-N-TO-X.
      *
      * --> PARAMETRO MOVIMENTO
      *
           MOVE PMO-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO PMO-DT-INI-EFF-DB
           MOVE PMO-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO PMO-DT-END-EFF-DB
           IF IND-PMO-DT-RICOR-PREC = 0
               MOVE PMO-DT-RICOR-PREC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PMO-DT-RICOR-PREC-DB
           END-IF
           IF IND-PMO-DT-RICOR-SUCC = 0
               MOVE PMO-DT-RICOR-SUCC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PMO-DT-RICOR-SUCC-DB
           END-IF
           IF IND-PMO-DT-ULT-EROG-MANFEE = 0
               MOVE PMO-DT-ULT-EROG-MANFEE TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO PMO-DT-ULT-EROG-MANFEE-DB
           END-IF.
      *
       Z900-EX.
           EXIT.

       Z950-CONVERTI-X-TO-N.
      *
      * --> PARAMETRO MOVIMENTO
      *
           MOVE PMO-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO PMO-DT-INI-EFF
           MOVE PMO-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO PMO-DT-END-EFF
           IF IND-PMO-DT-RICOR-PREC = 0
               MOVE PMO-DT-RICOR-PREC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PMO-DT-RICOR-PREC
           END-IF
           IF IND-PMO-DT-RICOR-SUCC = 0
               MOVE PMO-DT-RICOR-SUCC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PMO-DT-RICOR-SUCC
           END-IF
           IF IND-PMO-DT-ULT-EROG-MANFEE = 0
               MOVE PMO-DT-ULT-EROG-MANFEE-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO PMO-DT-ULT-EROG-MANFEE
           END-IF.
      *
       Z950-EX.
           EXIT.
      *
       Z960-LENGTH-VCHAR.
      *
           CONTINUE.
      *
       Z960-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
      *
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

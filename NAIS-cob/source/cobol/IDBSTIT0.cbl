       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSTIT0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  05 DIC 2014.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDTIT0 END-EXEC.
           EXEC SQL INCLUDE IDBVTIT2 END-EXEC.
           EXEC SQL INCLUDE IDBVTIT3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVTIT1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 TIT-CONT.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSTIT0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'TIT_CONT' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TIT_CONT
                ,ID_OGG
                ,TP_OGG
                ,IB_RICH
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_TIT
                ,PROG_TIT
                ,TP_PRE_TIT
                ,TP_STAT_TIT
                ,DT_INI_COP
                ,DT_END_COP
                ,IMP_PAG
                ,FL_SOLL
                ,FRAZ
                ,DT_APPLZ_MORA
                ,FL_MORA
                ,ID_RAPP_RETE
                ,ID_RAPP_ANA
                ,COD_DVS
                ,DT_EMIS_TIT
                ,DT_ESI_TIT
                ,TOT_PRE_NET
                ,TOT_INTR_FRAZ
                ,TOT_INTR_MORA
                ,TOT_INTR_PREST
                ,TOT_INTR_RETDT
                ,TOT_INTR_RIAT
                ,TOT_DIR
                ,TOT_SPE_MED
                ,TOT_TAX
                ,TOT_SOPR_SAN
                ,TOT_SOPR_TEC
                ,TOT_SOPR_SPO
                ,TOT_SOPR_PROF
                ,TOT_SOPR_ALT
                ,TOT_PRE_TOT
                ,TOT_PRE_PP_IAS
                ,TOT_CAR_ACQ
                ,TOT_CAR_GEST
                ,TOT_CAR_INC
                ,TOT_PRE_SOLO_RSH
                ,TOT_PROV_ACQ_1AA
                ,TOT_PROV_ACQ_2AA
                ,TOT_PROV_RICOR
                ,TOT_PROV_INC
                ,TOT_PROV_DA_REC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,TOT_MANFEE_ANTIC
                ,TOT_MANFEE_RICOR
                ,TOT_MANFEE_REC
                ,TP_MEZ_PAG_ADD
                ,ESTR_CNT_CORR_ADD
                ,DT_VLT
                ,FL_FORZ_DT_VLT
                ,DT_CAMBIO_VLT
                ,TOT_SPE_AGE
                ,TOT_CAR_IAS
                ,NUM_RAT_ACCORPATE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_TIT_DA_REINVST
                ,DT_RICH_ADD_RID
                ,TP_ESI_RID
                ,COD_IBAN
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,DT_CERT_FISC
                ,TP_CAUS_STOR
                ,TP_CAUS_DISP_STOR
                ,TP_TIT_MIGRAZ
                ,TOT_ACQ_EXP
                ,TOT_REMUN_ASS
                ,TOT_COMMIS_INTER
                ,TP_CAUS_RIMB
                ,TOT_CNBT_ANTIRAC
                ,FL_INC_AUTOGEN
             INTO
                :TIT-ID-TIT-CONT
               ,:TIT-ID-OGG
               ,:TIT-TP-OGG
               ,:TIT-IB-RICH
                :IND-TIT-IB-RICH
               ,:TIT-ID-MOVI-CRZ
               ,:TIT-ID-MOVI-CHIU
                :IND-TIT-ID-MOVI-CHIU
               ,:TIT-DT-INI-EFF-DB
               ,:TIT-DT-END-EFF-DB
               ,:TIT-COD-COMP-ANIA
               ,:TIT-TP-TIT
               ,:TIT-PROG-TIT
                :IND-TIT-PROG-TIT
               ,:TIT-TP-PRE-TIT
               ,:TIT-TP-STAT-TIT
               ,:TIT-DT-INI-COP-DB
                :IND-TIT-DT-INI-COP
               ,:TIT-DT-END-COP-DB
                :IND-TIT-DT-END-COP
               ,:TIT-IMP-PAG
                :IND-TIT-IMP-PAG
               ,:TIT-FL-SOLL
                :IND-TIT-FL-SOLL
               ,:TIT-FRAZ
                :IND-TIT-FRAZ
               ,:TIT-DT-APPLZ-MORA-DB
                :IND-TIT-DT-APPLZ-MORA
               ,:TIT-FL-MORA
                :IND-TIT-FL-MORA
               ,:TIT-ID-RAPP-RETE
                :IND-TIT-ID-RAPP-RETE
               ,:TIT-ID-RAPP-ANA
                :IND-TIT-ID-RAPP-ANA
               ,:TIT-COD-DVS
                :IND-TIT-COD-DVS
               ,:TIT-DT-EMIS-TIT-DB
                :IND-TIT-DT-EMIS-TIT
               ,:TIT-DT-ESI-TIT-DB
                :IND-TIT-DT-ESI-TIT
               ,:TIT-TOT-PRE-NET
                :IND-TIT-TOT-PRE-NET
               ,:TIT-TOT-INTR-FRAZ
                :IND-TIT-TOT-INTR-FRAZ
               ,:TIT-TOT-INTR-MORA
                :IND-TIT-TOT-INTR-MORA
               ,:TIT-TOT-INTR-PREST
                :IND-TIT-TOT-INTR-PREST
               ,:TIT-TOT-INTR-RETDT
                :IND-TIT-TOT-INTR-RETDT
               ,:TIT-TOT-INTR-RIAT
                :IND-TIT-TOT-INTR-RIAT
               ,:TIT-TOT-DIR
                :IND-TIT-TOT-DIR
               ,:TIT-TOT-SPE-MED
                :IND-TIT-TOT-SPE-MED
               ,:TIT-TOT-TAX
                :IND-TIT-TOT-TAX
               ,:TIT-TOT-SOPR-SAN
                :IND-TIT-TOT-SOPR-SAN
               ,:TIT-TOT-SOPR-TEC
                :IND-TIT-TOT-SOPR-TEC
               ,:TIT-TOT-SOPR-SPO
                :IND-TIT-TOT-SOPR-SPO
               ,:TIT-TOT-SOPR-PROF
                :IND-TIT-TOT-SOPR-PROF
               ,:TIT-TOT-SOPR-ALT
                :IND-TIT-TOT-SOPR-ALT
               ,:TIT-TOT-PRE-TOT
                :IND-TIT-TOT-PRE-TOT
               ,:TIT-TOT-PRE-PP-IAS
                :IND-TIT-TOT-PRE-PP-IAS
               ,:TIT-TOT-CAR-ACQ
                :IND-TIT-TOT-CAR-ACQ
               ,:TIT-TOT-CAR-GEST
                :IND-TIT-TOT-CAR-GEST
               ,:TIT-TOT-CAR-INC
                :IND-TIT-TOT-CAR-INC
               ,:TIT-TOT-PRE-SOLO-RSH
                :IND-TIT-TOT-PRE-SOLO-RSH
               ,:TIT-TOT-PROV-ACQ-1AA
                :IND-TIT-TOT-PROV-ACQ-1AA
               ,:TIT-TOT-PROV-ACQ-2AA
                :IND-TIT-TOT-PROV-ACQ-2AA
               ,:TIT-TOT-PROV-RICOR
                :IND-TIT-TOT-PROV-RICOR
               ,:TIT-TOT-PROV-INC
                :IND-TIT-TOT-PROV-INC
               ,:TIT-TOT-PROV-DA-REC
                :IND-TIT-TOT-PROV-DA-REC
               ,:TIT-IMP-AZ
                :IND-TIT-IMP-AZ
               ,:TIT-IMP-ADER
                :IND-TIT-IMP-ADER
               ,:TIT-IMP-TFR
                :IND-TIT-IMP-TFR
               ,:TIT-IMP-VOLO
                :IND-TIT-IMP-VOLO
               ,:TIT-TOT-MANFEE-ANTIC
                :IND-TIT-TOT-MANFEE-ANTIC
               ,:TIT-TOT-MANFEE-RICOR
                :IND-TIT-TOT-MANFEE-RICOR
               ,:TIT-TOT-MANFEE-REC
                :IND-TIT-TOT-MANFEE-REC
               ,:TIT-TP-MEZ-PAG-ADD
                :IND-TIT-TP-MEZ-PAG-ADD
               ,:TIT-ESTR-CNT-CORR-ADD
                :IND-TIT-ESTR-CNT-CORR-ADD
               ,:TIT-DT-VLT-DB
                :IND-TIT-DT-VLT
               ,:TIT-FL-FORZ-DT-VLT
                :IND-TIT-FL-FORZ-DT-VLT
               ,:TIT-DT-CAMBIO-VLT-DB
                :IND-TIT-DT-CAMBIO-VLT
               ,:TIT-TOT-SPE-AGE
                :IND-TIT-TOT-SPE-AGE
               ,:TIT-TOT-CAR-IAS
                :IND-TIT-TOT-CAR-IAS
               ,:TIT-NUM-RAT-ACCORPATE
                :IND-TIT-NUM-RAT-ACCORPATE
               ,:TIT-DS-RIGA
               ,:TIT-DS-OPER-SQL
               ,:TIT-DS-VER
               ,:TIT-DS-TS-INI-CPTZ
               ,:TIT-DS-TS-END-CPTZ
               ,:TIT-DS-UTENTE
               ,:TIT-DS-STATO-ELAB
               ,:TIT-FL-TIT-DA-REINVST
                :IND-TIT-FL-TIT-DA-REINVST
               ,:TIT-DT-RICH-ADD-RID-DB
                :IND-TIT-DT-RICH-ADD-RID
               ,:TIT-TP-ESI-RID
                :IND-TIT-TP-ESI-RID
               ,:TIT-COD-IBAN
                :IND-TIT-COD-IBAN
               ,:TIT-IMP-TRASFE
                :IND-TIT-IMP-TRASFE
               ,:TIT-IMP-TFR-STRC
                :IND-TIT-IMP-TFR-STRC
               ,:TIT-DT-CERT-FISC-DB
                :IND-TIT-DT-CERT-FISC
               ,:TIT-TP-CAUS-STOR
                :IND-TIT-TP-CAUS-STOR
               ,:TIT-TP-CAUS-DISP-STOR
                :IND-TIT-TP-CAUS-DISP-STOR
               ,:TIT-TP-TIT-MIGRAZ
                :IND-TIT-TP-TIT-MIGRAZ
               ,:TIT-TOT-ACQ-EXP
                :IND-TIT-TOT-ACQ-EXP
               ,:TIT-TOT-REMUN-ASS
                :IND-TIT-TOT-REMUN-ASS
               ,:TIT-TOT-COMMIS-INTER
                :IND-TIT-TOT-COMMIS-INTER
               ,:TIT-TP-CAUS-RIMB
                :IND-TIT-TP-CAUS-RIMB
               ,:TIT-TOT-CNBT-ANTIRAC
                :IND-TIT-TOT-CNBT-ANTIRAC
               ,:TIT-FL-INC-AUTOGEN
                :IND-TIT-FL-INC-AUTOGEN
             FROM TIT_CONT
             WHERE     DS_RIGA = :TIT-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO TIT_CONT
                     (
                        ID_TIT_CONT
                       ,ID_OGG
                       ,TP_OGG
                       ,IB_RICH
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_TIT
                       ,PROG_TIT
                       ,TP_PRE_TIT
                       ,TP_STAT_TIT
                       ,DT_INI_COP
                       ,DT_END_COP
                       ,IMP_PAG
                       ,FL_SOLL
                       ,FRAZ
                       ,DT_APPLZ_MORA
                       ,FL_MORA
                       ,ID_RAPP_RETE
                       ,ID_RAPP_ANA
                       ,COD_DVS
                       ,DT_EMIS_TIT
                       ,DT_ESI_TIT
                       ,TOT_PRE_NET
                       ,TOT_INTR_FRAZ
                       ,TOT_INTR_MORA
                       ,TOT_INTR_PREST
                       ,TOT_INTR_RETDT
                       ,TOT_INTR_RIAT
                       ,TOT_DIR
                       ,TOT_SPE_MED
                       ,TOT_TAX
                       ,TOT_SOPR_SAN
                       ,TOT_SOPR_TEC
                       ,TOT_SOPR_SPO
                       ,TOT_SOPR_PROF
                       ,TOT_SOPR_ALT
                       ,TOT_PRE_TOT
                       ,TOT_PRE_PP_IAS
                       ,TOT_CAR_ACQ
                       ,TOT_CAR_GEST
                       ,TOT_CAR_INC
                       ,TOT_PRE_SOLO_RSH
                       ,TOT_PROV_ACQ_1AA
                       ,TOT_PROV_ACQ_2AA
                       ,TOT_PROV_RICOR
                       ,TOT_PROV_INC
                       ,TOT_PROV_DA_REC
                       ,IMP_AZ
                       ,IMP_ADER
                       ,IMP_TFR
                       ,IMP_VOLO
                       ,TOT_MANFEE_ANTIC
                       ,TOT_MANFEE_RICOR
                       ,TOT_MANFEE_REC
                       ,TP_MEZ_PAG_ADD
                       ,ESTR_CNT_CORR_ADD
                       ,DT_VLT
                       ,FL_FORZ_DT_VLT
                       ,DT_CAMBIO_VLT
                       ,TOT_SPE_AGE
                       ,TOT_CAR_IAS
                       ,NUM_RAT_ACCORPATE
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,FL_TIT_DA_REINVST
                       ,DT_RICH_ADD_RID
                       ,TP_ESI_RID
                       ,COD_IBAN
                       ,IMP_TRASFE
                       ,IMP_TFR_STRC
                       ,DT_CERT_FISC
                       ,TP_CAUS_STOR
                       ,TP_CAUS_DISP_STOR
                       ,TP_TIT_MIGRAZ
                       ,TOT_ACQ_EXP
                       ,TOT_REMUN_ASS
                       ,TOT_COMMIS_INTER
                       ,TP_CAUS_RIMB
                       ,TOT_CNBT_ANTIRAC
                       ,FL_INC_AUTOGEN
                     )
                 VALUES
                     (
                       :TIT-ID-TIT-CONT
                       ,:TIT-ID-OGG
                       ,:TIT-TP-OGG
                       ,:TIT-IB-RICH
                        :IND-TIT-IB-RICH
                       ,:TIT-ID-MOVI-CRZ
                       ,:TIT-ID-MOVI-CHIU
                        :IND-TIT-ID-MOVI-CHIU
                       ,:TIT-DT-INI-EFF-DB
                       ,:TIT-DT-END-EFF-DB
                       ,:TIT-COD-COMP-ANIA
                       ,:TIT-TP-TIT
                       ,:TIT-PROG-TIT
                        :IND-TIT-PROG-TIT
                       ,:TIT-TP-PRE-TIT
                       ,:TIT-TP-STAT-TIT
                       ,:TIT-DT-INI-COP-DB
                        :IND-TIT-DT-INI-COP
                       ,:TIT-DT-END-COP-DB
                        :IND-TIT-DT-END-COP
                       ,:TIT-IMP-PAG
                        :IND-TIT-IMP-PAG
                       ,:TIT-FL-SOLL
                        :IND-TIT-FL-SOLL
                       ,:TIT-FRAZ
                        :IND-TIT-FRAZ
                       ,:TIT-DT-APPLZ-MORA-DB
                        :IND-TIT-DT-APPLZ-MORA
                       ,:TIT-FL-MORA
                        :IND-TIT-FL-MORA
                       ,:TIT-ID-RAPP-RETE
                        :IND-TIT-ID-RAPP-RETE
                       ,:TIT-ID-RAPP-ANA
                        :IND-TIT-ID-RAPP-ANA
                       ,:TIT-COD-DVS
                        :IND-TIT-COD-DVS
                       ,:TIT-DT-EMIS-TIT-DB
                        :IND-TIT-DT-EMIS-TIT
                       ,:TIT-DT-ESI-TIT-DB
                        :IND-TIT-DT-ESI-TIT
                       ,:TIT-TOT-PRE-NET
                        :IND-TIT-TOT-PRE-NET
                       ,:TIT-TOT-INTR-FRAZ
                        :IND-TIT-TOT-INTR-FRAZ
                       ,:TIT-TOT-INTR-MORA
                        :IND-TIT-TOT-INTR-MORA
                       ,:TIT-TOT-INTR-PREST
                        :IND-TIT-TOT-INTR-PREST
                       ,:TIT-TOT-INTR-RETDT
                        :IND-TIT-TOT-INTR-RETDT
                       ,:TIT-TOT-INTR-RIAT
                        :IND-TIT-TOT-INTR-RIAT
                       ,:TIT-TOT-DIR
                        :IND-TIT-TOT-DIR
                       ,:TIT-TOT-SPE-MED
                        :IND-TIT-TOT-SPE-MED
                       ,:TIT-TOT-TAX
                        :IND-TIT-TOT-TAX
                       ,:TIT-TOT-SOPR-SAN
                        :IND-TIT-TOT-SOPR-SAN
                       ,:TIT-TOT-SOPR-TEC
                        :IND-TIT-TOT-SOPR-TEC
                       ,:TIT-TOT-SOPR-SPO
                        :IND-TIT-TOT-SOPR-SPO
                       ,:TIT-TOT-SOPR-PROF
                        :IND-TIT-TOT-SOPR-PROF
                       ,:TIT-TOT-SOPR-ALT
                        :IND-TIT-TOT-SOPR-ALT
                       ,:TIT-TOT-PRE-TOT
                        :IND-TIT-TOT-PRE-TOT
                       ,:TIT-TOT-PRE-PP-IAS
                        :IND-TIT-TOT-PRE-PP-IAS
                       ,:TIT-TOT-CAR-ACQ
                        :IND-TIT-TOT-CAR-ACQ
                       ,:TIT-TOT-CAR-GEST
                        :IND-TIT-TOT-CAR-GEST
                       ,:TIT-TOT-CAR-INC
                        :IND-TIT-TOT-CAR-INC
                       ,:TIT-TOT-PRE-SOLO-RSH
                        :IND-TIT-TOT-PRE-SOLO-RSH
                       ,:TIT-TOT-PROV-ACQ-1AA
                        :IND-TIT-TOT-PROV-ACQ-1AA
                       ,:TIT-TOT-PROV-ACQ-2AA
                        :IND-TIT-TOT-PROV-ACQ-2AA
                       ,:TIT-TOT-PROV-RICOR
                        :IND-TIT-TOT-PROV-RICOR
                       ,:TIT-TOT-PROV-INC
                        :IND-TIT-TOT-PROV-INC
                       ,:TIT-TOT-PROV-DA-REC
                        :IND-TIT-TOT-PROV-DA-REC
                       ,:TIT-IMP-AZ
                        :IND-TIT-IMP-AZ
                       ,:TIT-IMP-ADER
                        :IND-TIT-IMP-ADER
                       ,:TIT-IMP-TFR
                        :IND-TIT-IMP-TFR
                       ,:TIT-IMP-VOLO
                        :IND-TIT-IMP-VOLO
                       ,:TIT-TOT-MANFEE-ANTIC
                        :IND-TIT-TOT-MANFEE-ANTIC
                       ,:TIT-TOT-MANFEE-RICOR
                        :IND-TIT-TOT-MANFEE-RICOR
                       ,:TIT-TOT-MANFEE-REC
                        :IND-TIT-TOT-MANFEE-REC
                       ,:TIT-TP-MEZ-PAG-ADD
                        :IND-TIT-TP-MEZ-PAG-ADD
                       ,:TIT-ESTR-CNT-CORR-ADD
                        :IND-TIT-ESTR-CNT-CORR-ADD
                       ,:TIT-DT-VLT-DB
                        :IND-TIT-DT-VLT
                       ,:TIT-FL-FORZ-DT-VLT
                        :IND-TIT-FL-FORZ-DT-VLT
                       ,:TIT-DT-CAMBIO-VLT-DB
                        :IND-TIT-DT-CAMBIO-VLT
                       ,:TIT-TOT-SPE-AGE
                        :IND-TIT-TOT-SPE-AGE
                       ,:TIT-TOT-CAR-IAS
                        :IND-TIT-TOT-CAR-IAS
                       ,:TIT-NUM-RAT-ACCORPATE
                        :IND-TIT-NUM-RAT-ACCORPATE
                       ,:TIT-DS-RIGA
                       ,:TIT-DS-OPER-SQL
                       ,:TIT-DS-VER
                       ,:TIT-DS-TS-INI-CPTZ
                       ,:TIT-DS-TS-END-CPTZ
                       ,:TIT-DS-UTENTE
                       ,:TIT-DS-STATO-ELAB
                       ,:TIT-FL-TIT-DA-REINVST
                        :IND-TIT-FL-TIT-DA-REINVST
                       ,:TIT-DT-RICH-ADD-RID-DB
                        :IND-TIT-DT-RICH-ADD-RID
                       ,:TIT-TP-ESI-RID
                        :IND-TIT-TP-ESI-RID
                       ,:TIT-COD-IBAN
                        :IND-TIT-COD-IBAN
                       ,:TIT-IMP-TRASFE
                        :IND-TIT-IMP-TRASFE
                       ,:TIT-IMP-TFR-STRC
                        :IND-TIT-IMP-TFR-STRC
                       ,:TIT-DT-CERT-FISC-DB
                        :IND-TIT-DT-CERT-FISC
                       ,:TIT-TP-CAUS-STOR
                        :IND-TIT-TP-CAUS-STOR
                       ,:TIT-TP-CAUS-DISP-STOR
                        :IND-TIT-TP-CAUS-DISP-STOR
                       ,:TIT-TP-TIT-MIGRAZ
                        :IND-TIT-TP-TIT-MIGRAZ
                       ,:TIT-TOT-ACQ-EXP
                        :IND-TIT-TOT-ACQ-EXP
                       ,:TIT-TOT-REMUN-ASS
                        :IND-TIT-TOT-REMUN-ASS
                       ,:TIT-TOT-COMMIS-INTER
                        :IND-TIT-TOT-COMMIS-INTER
                       ,:TIT-TP-CAUS-RIMB
                        :IND-TIT-TP-CAUS-RIMB
                       ,:TIT-TOT-CNBT-ANTIRAC
                        :IND-TIT-TOT-CNBT-ANTIRAC
                       ,:TIT-FL-INC-AUTOGEN
                        :IND-TIT-FL-INC-AUTOGEN
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE TIT_CONT SET

                   ID_TIT_CONT            =
                :TIT-ID-TIT-CONT
                  ,ID_OGG                 =
                :TIT-ID-OGG
                  ,TP_OGG                 =
                :TIT-TP-OGG
                  ,IB_RICH                =
                :TIT-IB-RICH
                                       :IND-TIT-IB-RICH
                  ,ID_MOVI_CRZ            =
                :TIT-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :TIT-ID-MOVI-CHIU
                                       :IND-TIT-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :TIT-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :TIT-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :TIT-COD-COMP-ANIA
                  ,TP_TIT                 =
                :TIT-TP-TIT
                  ,PROG_TIT               =
                :TIT-PROG-TIT
                                       :IND-TIT-PROG-TIT
                  ,TP_PRE_TIT             =
                :TIT-TP-PRE-TIT
                  ,TP_STAT_TIT            =
                :TIT-TP-STAT-TIT
                  ,DT_INI_COP             =
           :TIT-DT-INI-COP-DB
                                       :IND-TIT-DT-INI-COP
                  ,DT_END_COP             =
           :TIT-DT-END-COP-DB
                                       :IND-TIT-DT-END-COP
                  ,IMP_PAG                =
                :TIT-IMP-PAG
                                       :IND-TIT-IMP-PAG
                  ,FL_SOLL                =
                :TIT-FL-SOLL
                                       :IND-TIT-FL-SOLL
                  ,FRAZ                   =
                :TIT-FRAZ
                                       :IND-TIT-FRAZ
                  ,DT_APPLZ_MORA          =
           :TIT-DT-APPLZ-MORA-DB
                                       :IND-TIT-DT-APPLZ-MORA
                  ,FL_MORA                =
                :TIT-FL-MORA
                                       :IND-TIT-FL-MORA
                  ,ID_RAPP_RETE           =
                :TIT-ID-RAPP-RETE
                                       :IND-TIT-ID-RAPP-RETE
                  ,ID_RAPP_ANA            =
                :TIT-ID-RAPP-ANA
                                       :IND-TIT-ID-RAPP-ANA
                  ,COD_DVS                =
                :TIT-COD-DVS
                                       :IND-TIT-COD-DVS
                  ,DT_EMIS_TIT            =
           :TIT-DT-EMIS-TIT-DB
                                       :IND-TIT-DT-EMIS-TIT
                  ,DT_ESI_TIT             =
           :TIT-DT-ESI-TIT-DB
                                       :IND-TIT-DT-ESI-TIT
                  ,TOT_PRE_NET            =
                :TIT-TOT-PRE-NET
                                       :IND-TIT-TOT-PRE-NET
                  ,TOT_INTR_FRAZ          =
                :TIT-TOT-INTR-FRAZ
                                       :IND-TIT-TOT-INTR-FRAZ
                  ,TOT_INTR_MORA          =
                :TIT-TOT-INTR-MORA
                                       :IND-TIT-TOT-INTR-MORA
                  ,TOT_INTR_PREST         =
                :TIT-TOT-INTR-PREST
                                       :IND-TIT-TOT-INTR-PREST
                  ,TOT_INTR_RETDT         =
                :TIT-TOT-INTR-RETDT
                                       :IND-TIT-TOT-INTR-RETDT
                  ,TOT_INTR_RIAT          =
                :TIT-TOT-INTR-RIAT
                                       :IND-TIT-TOT-INTR-RIAT
                  ,TOT_DIR                =
                :TIT-TOT-DIR
                                       :IND-TIT-TOT-DIR
                  ,TOT_SPE_MED            =
                :TIT-TOT-SPE-MED
                                       :IND-TIT-TOT-SPE-MED
                  ,TOT_TAX                =
                :TIT-TOT-TAX
                                       :IND-TIT-TOT-TAX
                  ,TOT_SOPR_SAN           =
                :TIT-TOT-SOPR-SAN
                                       :IND-TIT-TOT-SOPR-SAN
                  ,TOT_SOPR_TEC           =
                :TIT-TOT-SOPR-TEC
                                       :IND-TIT-TOT-SOPR-TEC
                  ,TOT_SOPR_SPO           =
                :TIT-TOT-SOPR-SPO
                                       :IND-TIT-TOT-SOPR-SPO
                  ,TOT_SOPR_PROF          =
                :TIT-TOT-SOPR-PROF
                                       :IND-TIT-TOT-SOPR-PROF
                  ,TOT_SOPR_ALT           =
                :TIT-TOT-SOPR-ALT
                                       :IND-TIT-TOT-SOPR-ALT
                  ,TOT_PRE_TOT            =
                :TIT-TOT-PRE-TOT
                                       :IND-TIT-TOT-PRE-TOT
                  ,TOT_PRE_PP_IAS         =
                :TIT-TOT-PRE-PP-IAS
                                       :IND-TIT-TOT-PRE-PP-IAS
                  ,TOT_CAR_ACQ            =
                :TIT-TOT-CAR-ACQ
                                       :IND-TIT-TOT-CAR-ACQ
                  ,TOT_CAR_GEST           =
                :TIT-TOT-CAR-GEST
                                       :IND-TIT-TOT-CAR-GEST
                  ,TOT_CAR_INC            =
                :TIT-TOT-CAR-INC
                                       :IND-TIT-TOT-CAR-INC
                  ,TOT_PRE_SOLO_RSH       =
                :TIT-TOT-PRE-SOLO-RSH
                                       :IND-TIT-TOT-PRE-SOLO-RSH
                  ,TOT_PROV_ACQ_1AA       =
                :TIT-TOT-PROV-ACQ-1AA
                                       :IND-TIT-TOT-PROV-ACQ-1AA
                  ,TOT_PROV_ACQ_2AA       =
                :TIT-TOT-PROV-ACQ-2AA
                                       :IND-TIT-TOT-PROV-ACQ-2AA
                  ,TOT_PROV_RICOR         =
                :TIT-TOT-PROV-RICOR
                                       :IND-TIT-TOT-PROV-RICOR
                  ,TOT_PROV_INC           =
                :TIT-TOT-PROV-INC
                                       :IND-TIT-TOT-PROV-INC
                  ,TOT_PROV_DA_REC        =
                :TIT-TOT-PROV-DA-REC
                                       :IND-TIT-TOT-PROV-DA-REC
                  ,IMP_AZ                 =
                :TIT-IMP-AZ
                                       :IND-TIT-IMP-AZ
                  ,IMP_ADER               =
                :TIT-IMP-ADER
                                       :IND-TIT-IMP-ADER
                  ,IMP_TFR                =
                :TIT-IMP-TFR
                                       :IND-TIT-IMP-TFR
                  ,IMP_VOLO               =
                :TIT-IMP-VOLO
                                       :IND-TIT-IMP-VOLO
                  ,TOT_MANFEE_ANTIC       =
                :TIT-TOT-MANFEE-ANTIC
                                       :IND-TIT-TOT-MANFEE-ANTIC
                  ,TOT_MANFEE_RICOR       =
                :TIT-TOT-MANFEE-RICOR
                                       :IND-TIT-TOT-MANFEE-RICOR
                  ,TOT_MANFEE_REC         =
                :TIT-TOT-MANFEE-REC
                                       :IND-TIT-TOT-MANFEE-REC
                  ,TP_MEZ_PAG_ADD         =
                :TIT-TP-MEZ-PAG-ADD
                                       :IND-TIT-TP-MEZ-PAG-ADD
                  ,ESTR_CNT_CORR_ADD      =
                :TIT-ESTR-CNT-CORR-ADD
                                       :IND-TIT-ESTR-CNT-CORR-ADD
                  ,DT_VLT                 =
           :TIT-DT-VLT-DB
                                       :IND-TIT-DT-VLT
                  ,FL_FORZ_DT_VLT         =
                :TIT-FL-FORZ-DT-VLT
                                       :IND-TIT-FL-FORZ-DT-VLT
                  ,DT_CAMBIO_VLT          =
           :TIT-DT-CAMBIO-VLT-DB
                                       :IND-TIT-DT-CAMBIO-VLT
                  ,TOT_SPE_AGE            =
                :TIT-TOT-SPE-AGE
                                       :IND-TIT-TOT-SPE-AGE
                  ,TOT_CAR_IAS            =
                :TIT-TOT-CAR-IAS
                                       :IND-TIT-TOT-CAR-IAS
                  ,NUM_RAT_ACCORPATE      =
                :TIT-NUM-RAT-ACCORPATE
                                       :IND-TIT-NUM-RAT-ACCORPATE
                  ,DS_RIGA                =
                :TIT-DS-RIGA
                  ,DS_OPER_SQL            =
                :TIT-DS-OPER-SQL
                  ,DS_VER                 =
                :TIT-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :TIT-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :TIT-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :TIT-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :TIT-DS-STATO-ELAB
                  ,FL_TIT_DA_REINVST      =
                :TIT-FL-TIT-DA-REINVST
                                       :IND-TIT-FL-TIT-DA-REINVST
                  ,DT_RICH_ADD_RID        =
           :TIT-DT-RICH-ADD-RID-DB
                                       :IND-TIT-DT-RICH-ADD-RID
                  ,TP_ESI_RID             =
                :TIT-TP-ESI-RID
                                       :IND-TIT-TP-ESI-RID
                  ,COD_IBAN               =
                :TIT-COD-IBAN
                                       :IND-TIT-COD-IBAN
                  ,IMP_TRASFE             =
                :TIT-IMP-TRASFE
                                       :IND-TIT-IMP-TRASFE
                  ,IMP_TFR_STRC           =
                :TIT-IMP-TFR-STRC
                                       :IND-TIT-IMP-TFR-STRC
                  ,DT_CERT_FISC           =
           :TIT-DT-CERT-FISC-DB
                                       :IND-TIT-DT-CERT-FISC
                  ,TP_CAUS_STOR           =
                :TIT-TP-CAUS-STOR
                                       :IND-TIT-TP-CAUS-STOR
                  ,TP_CAUS_DISP_STOR      =
                :TIT-TP-CAUS-DISP-STOR
                                       :IND-TIT-TP-CAUS-DISP-STOR
                  ,TP_TIT_MIGRAZ          =
                :TIT-TP-TIT-MIGRAZ
                                       :IND-TIT-TP-TIT-MIGRAZ
                  ,TOT_ACQ_EXP            =
                :TIT-TOT-ACQ-EXP
                                       :IND-TIT-TOT-ACQ-EXP
                  ,TOT_REMUN_ASS          =
                :TIT-TOT-REMUN-ASS
                                       :IND-TIT-TOT-REMUN-ASS
                  ,TOT_COMMIS_INTER       =
                :TIT-TOT-COMMIS-INTER
                                       :IND-TIT-TOT-COMMIS-INTER
                  ,TP_CAUS_RIMB           =
                :TIT-TP-CAUS-RIMB
                                       :IND-TIT-TP-CAUS-RIMB
                  ,TOT_CNBT_ANTIRAC       =
                :TIT-TOT-CNBT-ANTIRAC
                                       :IND-TIT-TOT-CNBT-ANTIRAC
                  ,FL_INC_AUTOGEN         =
                :TIT-FL-INC-AUTOGEN
                                       :IND-TIT-FL-INC-AUTOGEN
                WHERE     DS_RIGA = :TIT-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM TIT_CONT
                WHERE     DS_RIGA = :TIT-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-TIT CURSOR FOR
              SELECT
                     ID_TIT_CONT
                    ,ID_OGG
                    ,TP_OGG
                    ,IB_RICH
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_TIT
                    ,PROG_TIT
                    ,TP_PRE_TIT
                    ,TP_STAT_TIT
                    ,DT_INI_COP
                    ,DT_END_COP
                    ,IMP_PAG
                    ,FL_SOLL
                    ,FRAZ
                    ,DT_APPLZ_MORA
                    ,FL_MORA
                    ,ID_RAPP_RETE
                    ,ID_RAPP_ANA
                    ,COD_DVS
                    ,DT_EMIS_TIT
                    ,DT_ESI_TIT
                    ,TOT_PRE_NET
                    ,TOT_INTR_FRAZ
                    ,TOT_INTR_MORA
                    ,TOT_INTR_PREST
                    ,TOT_INTR_RETDT
                    ,TOT_INTR_RIAT
                    ,TOT_DIR
                    ,TOT_SPE_MED
                    ,TOT_TAX
                    ,TOT_SOPR_SAN
                    ,TOT_SOPR_TEC
                    ,TOT_SOPR_SPO
                    ,TOT_SOPR_PROF
                    ,TOT_SOPR_ALT
                    ,TOT_PRE_TOT
                    ,TOT_PRE_PP_IAS
                    ,TOT_CAR_ACQ
                    ,TOT_CAR_GEST
                    ,TOT_CAR_INC
                    ,TOT_PRE_SOLO_RSH
                    ,TOT_PROV_ACQ_1AA
                    ,TOT_PROV_ACQ_2AA
                    ,TOT_PROV_RICOR
                    ,TOT_PROV_INC
                    ,TOT_PROV_DA_REC
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,TOT_MANFEE_ANTIC
                    ,TOT_MANFEE_RICOR
                    ,TOT_MANFEE_REC
                    ,TP_MEZ_PAG_ADD
                    ,ESTR_CNT_CORR_ADD
                    ,DT_VLT
                    ,FL_FORZ_DT_VLT
                    ,DT_CAMBIO_VLT
                    ,TOT_SPE_AGE
                    ,TOT_CAR_IAS
                    ,NUM_RAT_ACCORPATE
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,FL_TIT_DA_REINVST
                    ,DT_RICH_ADD_RID
                    ,TP_ESI_RID
                    ,COD_IBAN
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,DT_CERT_FISC
                    ,TP_CAUS_STOR
                    ,TP_CAUS_DISP_STOR
                    ,TP_TIT_MIGRAZ
                    ,TOT_ACQ_EXP
                    ,TOT_REMUN_ASS
                    ,TOT_COMMIS_INTER
                    ,TP_CAUS_RIMB
                    ,TOT_CNBT_ANTIRAC
                    ,FL_INC_AUTOGEN
              FROM TIT_CONT
              WHERE     ID_TIT_CONT = :TIT-ID-TIT-CONT
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TIT_CONT
                ,ID_OGG
                ,TP_OGG
                ,IB_RICH
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_TIT
                ,PROG_TIT
                ,TP_PRE_TIT
                ,TP_STAT_TIT
                ,DT_INI_COP
                ,DT_END_COP
                ,IMP_PAG
                ,FL_SOLL
                ,FRAZ
                ,DT_APPLZ_MORA
                ,FL_MORA
                ,ID_RAPP_RETE
                ,ID_RAPP_ANA
                ,COD_DVS
                ,DT_EMIS_TIT
                ,DT_ESI_TIT
                ,TOT_PRE_NET
                ,TOT_INTR_FRAZ
                ,TOT_INTR_MORA
                ,TOT_INTR_PREST
                ,TOT_INTR_RETDT
                ,TOT_INTR_RIAT
                ,TOT_DIR
                ,TOT_SPE_MED
                ,TOT_TAX
                ,TOT_SOPR_SAN
                ,TOT_SOPR_TEC
                ,TOT_SOPR_SPO
                ,TOT_SOPR_PROF
                ,TOT_SOPR_ALT
                ,TOT_PRE_TOT
                ,TOT_PRE_PP_IAS
                ,TOT_CAR_ACQ
                ,TOT_CAR_GEST
                ,TOT_CAR_INC
                ,TOT_PRE_SOLO_RSH
                ,TOT_PROV_ACQ_1AA
                ,TOT_PROV_ACQ_2AA
                ,TOT_PROV_RICOR
                ,TOT_PROV_INC
                ,TOT_PROV_DA_REC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,TOT_MANFEE_ANTIC
                ,TOT_MANFEE_RICOR
                ,TOT_MANFEE_REC
                ,TP_MEZ_PAG_ADD
                ,ESTR_CNT_CORR_ADD
                ,DT_VLT
                ,FL_FORZ_DT_VLT
                ,DT_CAMBIO_VLT
                ,TOT_SPE_AGE
                ,TOT_CAR_IAS
                ,NUM_RAT_ACCORPATE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_TIT_DA_REINVST
                ,DT_RICH_ADD_RID
                ,TP_ESI_RID
                ,COD_IBAN
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,DT_CERT_FISC
                ,TP_CAUS_STOR
                ,TP_CAUS_DISP_STOR
                ,TP_TIT_MIGRAZ
                ,TOT_ACQ_EXP
                ,TOT_REMUN_ASS
                ,TOT_COMMIS_INTER
                ,TP_CAUS_RIMB
                ,TOT_CNBT_ANTIRAC
                ,FL_INC_AUTOGEN
             INTO
                :TIT-ID-TIT-CONT
               ,:TIT-ID-OGG
               ,:TIT-TP-OGG
               ,:TIT-IB-RICH
                :IND-TIT-IB-RICH
               ,:TIT-ID-MOVI-CRZ
               ,:TIT-ID-MOVI-CHIU
                :IND-TIT-ID-MOVI-CHIU
               ,:TIT-DT-INI-EFF-DB
               ,:TIT-DT-END-EFF-DB
               ,:TIT-COD-COMP-ANIA
               ,:TIT-TP-TIT
               ,:TIT-PROG-TIT
                :IND-TIT-PROG-TIT
               ,:TIT-TP-PRE-TIT
               ,:TIT-TP-STAT-TIT
               ,:TIT-DT-INI-COP-DB
                :IND-TIT-DT-INI-COP
               ,:TIT-DT-END-COP-DB
                :IND-TIT-DT-END-COP
               ,:TIT-IMP-PAG
                :IND-TIT-IMP-PAG
               ,:TIT-FL-SOLL
                :IND-TIT-FL-SOLL
               ,:TIT-FRAZ
                :IND-TIT-FRAZ
               ,:TIT-DT-APPLZ-MORA-DB
                :IND-TIT-DT-APPLZ-MORA
               ,:TIT-FL-MORA
                :IND-TIT-FL-MORA
               ,:TIT-ID-RAPP-RETE
                :IND-TIT-ID-RAPP-RETE
               ,:TIT-ID-RAPP-ANA
                :IND-TIT-ID-RAPP-ANA
               ,:TIT-COD-DVS
                :IND-TIT-COD-DVS
               ,:TIT-DT-EMIS-TIT-DB
                :IND-TIT-DT-EMIS-TIT
               ,:TIT-DT-ESI-TIT-DB
                :IND-TIT-DT-ESI-TIT
               ,:TIT-TOT-PRE-NET
                :IND-TIT-TOT-PRE-NET
               ,:TIT-TOT-INTR-FRAZ
                :IND-TIT-TOT-INTR-FRAZ
               ,:TIT-TOT-INTR-MORA
                :IND-TIT-TOT-INTR-MORA
               ,:TIT-TOT-INTR-PREST
                :IND-TIT-TOT-INTR-PREST
               ,:TIT-TOT-INTR-RETDT
                :IND-TIT-TOT-INTR-RETDT
               ,:TIT-TOT-INTR-RIAT
                :IND-TIT-TOT-INTR-RIAT
               ,:TIT-TOT-DIR
                :IND-TIT-TOT-DIR
               ,:TIT-TOT-SPE-MED
                :IND-TIT-TOT-SPE-MED
               ,:TIT-TOT-TAX
                :IND-TIT-TOT-TAX
               ,:TIT-TOT-SOPR-SAN
                :IND-TIT-TOT-SOPR-SAN
               ,:TIT-TOT-SOPR-TEC
                :IND-TIT-TOT-SOPR-TEC
               ,:TIT-TOT-SOPR-SPO
                :IND-TIT-TOT-SOPR-SPO
               ,:TIT-TOT-SOPR-PROF
                :IND-TIT-TOT-SOPR-PROF
               ,:TIT-TOT-SOPR-ALT
                :IND-TIT-TOT-SOPR-ALT
               ,:TIT-TOT-PRE-TOT
                :IND-TIT-TOT-PRE-TOT
               ,:TIT-TOT-PRE-PP-IAS
                :IND-TIT-TOT-PRE-PP-IAS
               ,:TIT-TOT-CAR-ACQ
                :IND-TIT-TOT-CAR-ACQ
               ,:TIT-TOT-CAR-GEST
                :IND-TIT-TOT-CAR-GEST
               ,:TIT-TOT-CAR-INC
                :IND-TIT-TOT-CAR-INC
               ,:TIT-TOT-PRE-SOLO-RSH
                :IND-TIT-TOT-PRE-SOLO-RSH
               ,:TIT-TOT-PROV-ACQ-1AA
                :IND-TIT-TOT-PROV-ACQ-1AA
               ,:TIT-TOT-PROV-ACQ-2AA
                :IND-TIT-TOT-PROV-ACQ-2AA
               ,:TIT-TOT-PROV-RICOR
                :IND-TIT-TOT-PROV-RICOR
               ,:TIT-TOT-PROV-INC
                :IND-TIT-TOT-PROV-INC
               ,:TIT-TOT-PROV-DA-REC
                :IND-TIT-TOT-PROV-DA-REC
               ,:TIT-IMP-AZ
                :IND-TIT-IMP-AZ
               ,:TIT-IMP-ADER
                :IND-TIT-IMP-ADER
               ,:TIT-IMP-TFR
                :IND-TIT-IMP-TFR
               ,:TIT-IMP-VOLO
                :IND-TIT-IMP-VOLO
               ,:TIT-TOT-MANFEE-ANTIC
                :IND-TIT-TOT-MANFEE-ANTIC
               ,:TIT-TOT-MANFEE-RICOR
                :IND-TIT-TOT-MANFEE-RICOR
               ,:TIT-TOT-MANFEE-REC
                :IND-TIT-TOT-MANFEE-REC
               ,:TIT-TP-MEZ-PAG-ADD
                :IND-TIT-TP-MEZ-PAG-ADD
               ,:TIT-ESTR-CNT-CORR-ADD
                :IND-TIT-ESTR-CNT-CORR-ADD
               ,:TIT-DT-VLT-DB
                :IND-TIT-DT-VLT
               ,:TIT-FL-FORZ-DT-VLT
                :IND-TIT-FL-FORZ-DT-VLT
               ,:TIT-DT-CAMBIO-VLT-DB
                :IND-TIT-DT-CAMBIO-VLT
               ,:TIT-TOT-SPE-AGE
                :IND-TIT-TOT-SPE-AGE
               ,:TIT-TOT-CAR-IAS
                :IND-TIT-TOT-CAR-IAS
               ,:TIT-NUM-RAT-ACCORPATE
                :IND-TIT-NUM-RAT-ACCORPATE
               ,:TIT-DS-RIGA
               ,:TIT-DS-OPER-SQL
               ,:TIT-DS-VER
               ,:TIT-DS-TS-INI-CPTZ
               ,:TIT-DS-TS-END-CPTZ
               ,:TIT-DS-UTENTE
               ,:TIT-DS-STATO-ELAB
               ,:TIT-FL-TIT-DA-REINVST
                :IND-TIT-FL-TIT-DA-REINVST
               ,:TIT-DT-RICH-ADD-RID-DB
                :IND-TIT-DT-RICH-ADD-RID
               ,:TIT-TP-ESI-RID
                :IND-TIT-TP-ESI-RID
               ,:TIT-COD-IBAN
                :IND-TIT-COD-IBAN
               ,:TIT-IMP-TRASFE
                :IND-TIT-IMP-TRASFE
               ,:TIT-IMP-TFR-STRC
                :IND-TIT-IMP-TFR-STRC
               ,:TIT-DT-CERT-FISC-DB
                :IND-TIT-DT-CERT-FISC
               ,:TIT-TP-CAUS-STOR
                :IND-TIT-TP-CAUS-STOR
               ,:TIT-TP-CAUS-DISP-STOR
                :IND-TIT-TP-CAUS-DISP-STOR
               ,:TIT-TP-TIT-MIGRAZ
                :IND-TIT-TP-TIT-MIGRAZ
               ,:TIT-TOT-ACQ-EXP
                :IND-TIT-TOT-ACQ-EXP
               ,:TIT-TOT-REMUN-ASS
                :IND-TIT-TOT-REMUN-ASS
               ,:TIT-TOT-COMMIS-INTER
                :IND-TIT-TOT-COMMIS-INTER
               ,:TIT-TP-CAUS-RIMB
                :IND-TIT-TP-CAUS-RIMB
               ,:TIT-TOT-CNBT-ANTIRAC
                :IND-TIT-TOT-CNBT-ANTIRAC
               ,:TIT-FL-INC-AUTOGEN
                :IND-TIT-FL-INC-AUTOGEN
             FROM TIT_CONT
             WHERE     ID_TIT_CONT = :TIT-ID-TIT-CONT
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE TIT_CONT SET

                   ID_TIT_CONT            =
                :TIT-ID-TIT-CONT
                  ,ID_OGG                 =
                :TIT-ID-OGG
                  ,TP_OGG                 =
                :TIT-TP-OGG
                  ,IB_RICH                =
                :TIT-IB-RICH
                                       :IND-TIT-IB-RICH
                  ,ID_MOVI_CRZ            =
                :TIT-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :TIT-ID-MOVI-CHIU
                                       :IND-TIT-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :TIT-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :TIT-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :TIT-COD-COMP-ANIA
                  ,TP_TIT                 =
                :TIT-TP-TIT
                  ,PROG_TIT               =
                :TIT-PROG-TIT
                                       :IND-TIT-PROG-TIT
                  ,TP_PRE_TIT             =
                :TIT-TP-PRE-TIT
                  ,TP_STAT_TIT            =
                :TIT-TP-STAT-TIT
                  ,DT_INI_COP             =
           :TIT-DT-INI-COP-DB
                                       :IND-TIT-DT-INI-COP
                  ,DT_END_COP             =
           :TIT-DT-END-COP-DB
                                       :IND-TIT-DT-END-COP
                  ,IMP_PAG                =
                :TIT-IMP-PAG
                                       :IND-TIT-IMP-PAG
                  ,FL_SOLL                =
                :TIT-FL-SOLL
                                       :IND-TIT-FL-SOLL
                  ,FRAZ                   =
                :TIT-FRAZ
                                       :IND-TIT-FRAZ
                  ,DT_APPLZ_MORA          =
           :TIT-DT-APPLZ-MORA-DB
                                       :IND-TIT-DT-APPLZ-MORA
                  ,FL_MORA                =
                :TIT-FL-MORA
                                       :IND-TIT-FL-MORA
                  ,ID_RAPP_RETE           =
                :TIT-ID-RAPP-RETE
                                       :IND-TIT-ID-RAPP-RETE
                  ,ID_RAPP_ANA            =
                :TIT-ID-RAPP-ANA
                                       :IND-TIT-ID-RAPP-ANA
                  ,COD_DVS                =
                :TIT-COD-DVS
                                       :IND-TIT-COD-DVS
                  ,DT_EMIS_TIT            =
           :TIT-DT-EMIS-TIT-DB
                                       :IND-TIT-DT-EMIS-TIT
                  ,DT_ESI_TIT             =
           :TIT-DT-ESI-TIT-DB
                                       :IND-TIT-DT-ESI-TIT
                  ,TOT_PRE_NET            =
                :TIT-TOT-PRE-NET
                                       :IND-TIT-TOT-PRE-NET
                  ,TOT_INTR_FRAZ          =
                :TIT-TOT-INTR-FRAZ
                                       :IND-TIT-TOT-INTR-FRAZ
                  ,TOT_INTR_MORA          =
                :TIT-TOT-INTR-MORA
                                       :IND-TIT-TOT-INTR-MORA
                  ,TOT_INTR_PREST         =
                :TIT-TOT-INTR-PREST
                                       :IND-TIT-TOT-INTR-PREST
                  ,TOT_INTR_RETDT         =
                :TIT-TOT-INTR-RETDT
                                       :IND-TIT-TOT-INTR-RETDT
                  ,TOT_INTR_RIAT          =
                :TIT-TOT-INTR-RIAT
                                       :IND-TIT-TOT-INTR-RIAT
                  ,TOT_DIR                =
                :TIT-TOT-DIR
                                       :IND-TIT-TOT-DIR
                  ,TOT_SPE_MED            =
                :TIT-TOT-SPE-MED
                                       :IND-TIT-TOT-SPE-MED
                  ,TOT_TAX                =
                :TIT-TOT-TAX
                                       :IND-TIT-TOT-TAX
                  ,TOT_SOPR_SAN           =
                :TIT-TOT-SOPR-SAN
                                       :IND-TIT-TOT-SOPR-SAN
                  ,TOT_SOPR_TEC           =
                :TIT-TOT-SOPR-TEC
                                       :IND-TIT-TOT-SOPR-TEC
                  ,TOT_SOPR_SPO           =
                :TIT-TOT-SOPR-SPO
                                       :IND-TIT-TOT-SOPR-SPO
                  ,TOT_SOPR_PROF          =
                :TIT-TOT-SOPR-PROF
                                       :IND-TIT-TOT-SOPR-PROF
                  ,TOT_SOPR_ALT           =
                :TIT-TOT-SOPR-ALT
                                       :IND-TIT-TOT-SOPR-ALT
                  ,TOT_PRE_TOT            =
                :TIT-TOT-PRE-TOT
                                       :IND-TIT-TOT-PRE-TOT
                  ,TOT_PRE_PP_IAS         =
                :TIT-TOT-PRE-PP-IAS
                                       :IND-TIT-TOT-PRE-PP-IAS
                  ,TOT_CAR_ACQ            =
                :TIT-TOT-CAR-ACQ
                                       :IND-TIT-TOT-CAR-ACQ
                  ,TOT_CAR_GEST           =
                :TIT-TOT-CAR-GEST
                                       :IND-TIT-TOT-CAR-GEST
                  ,TOT_CAR_INC            =
                :TIT-TOT-CAR-INC
                                       :IND-TIT-TOT-CAR-INC
                  ,TOT_PRE_SOLO_RSH       =
                :TIT-TOT-PRE-SOLO-RSH
                                       :IND-TIT-TOT-PRE-SOLO-RSH
                  ,TOT_PROV_ACQ_1AA       =
                :TIT-TOT-PROV-ACQ-1AA
                                       :IND-TIT-TOT-PROV-ACQ-1AA
                  ,TOT_PROV_ACQ_2AA       =
                :TIT-TOT-PROV-ACQ-2AA
                                       :IND-TIT-TOT-PROV-ACQ-2AA
                  ,TOT_PROV_RICOR         =
                :TIT-TOT-PROV-RICOR
                                       :IND-TIT-TOT-PROV-RICOR
                  ,TOT_PROV_INC           =
                :TIT-TOT-PROV-INC
                                       :IND-TIT-TOT-PROV-INC
                  ,TOT_PROV_DA_REC        =
                :TIT-TOT-PROV-DA-REC
                                       :IND-TIT-TOT-PROV-DA-REC
                  ,IMP_AZ                 =
                :TIT-IMP-AZ
                                       :IND-TIT-IMP-AZ
                  ,IMP_ADER               =
                :TIT-IMP-ADER
                                       :IND-TIT-IMP-ADER
                  ,IMP_TFR                =
                :TIT-IMP-TFR
                                       :IND-TIT-IMP-TFR
                  ,IMP_VOLO               =
                :TIT-IMP-VOLO
                                       :IND-TIT-IMP-VOLO
                  ,TOT_MANFEE_ANTIC       =
                :TIT-TOT-MANFEE-ANTIC
                                       :IND-TIT-TOT-MANFEE-ANTIC
                  ,TOT_MANFEE_RICOR       =
                :TIT-TOT-MANFEE-RICOR
                                       :IND-TIT-TOT-MANFEE-RICOR
                  ,TOT_MANFEE_REC         =
                :TIT-TOT-MANFEE-REC
                                       :IND-TIT-TOT-MANFEE-REC
                  ,TP_MEZ_PAG_ADD         =
                :TIT-TP-MEZ-PAG-ADD
                                       :IND-TIT-TP-MEZ-PAG-ADD
                  ,ESTR_CNT_CORR_ADD      =
                :TIT-ESTR-CNT-CORR-ADD
                                       :IND-TIT-ESTR-CNT-CORR-ADD
                  ,DT_VLT                 =
           :TIT-DT-VLT-DB
                                       :IND-TIT-DT-VLT
                  ,FL_FORZ_DT_VLT         =
                :TIT-FL-FORZ-DT-VLT
                                       :IND-TIT-FL-FORZ-DT-VLT
                  ,DT_CAMBIO_VLT          =
           :TIT-DT-CAMBIO-VLT-DB
                                       :IND-TIT-DT-CAMBIO-VLT
                  ,TOT_SPE_AGE            =
                :TIT-TOT-SPE-AGE
                                       :IND-TIT-TOT-SPE-AGE
                  ,TOT_CAR_IAS            =
                :TIT-TOT-CAR-IAS
                                       :IND-TIT-TOT-CAR-IAS
                  ,NUM_RAT_ACCORPATE      =
                :TIT-NUM-RAT-ACCORPATE
                                       :IND-TIT-NUM-RAT-ACCORPATE
                  ,DS_RIGA                =
                :TIT-DS-RIGA
                  ,DS_OPER_SQL            =
                :TIT-DS-OPER-SQL
                  ,DS_VER                 =
                :TIT-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :TIT-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :TIT-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :TIT-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :TIT-DS-STATO-ELAB
                  ,FL_TIT_DA_REINVST      =
                :TIT-FL-TIT-DA-REINVST
                                       :IND-TIT-FL-TIT-DA-REINVST
                  ,DT_RICH_ADD_RID        =
           :TIT-DT-RICH-ADD-RID-DB
                                       :IND-TIT-DT-RICH-ADD-RID
                  ,TP_ESI_RID             =
                :TIT-TP-ESI-RID
                                       :IND-TIT-TP-ESI-RID
                  ,COD_IBAN               =
                :TIT-COD-IBAN
                                       :IND-TIT-COD-IBAN
                  ,IMP_TRASFE             =
                :TIT-IMP-TRASFE
                                       :IND-TIT-IMP-TRASFE
                  ,IMP_TFR_STRC           =
                :TIT-IMP-TFR-STRC
                                       :IND-TIT-IMP-TFR-STRC
                  ,DT_CERT_FISC           =
           :TIT-DT-CERT-FISC-DB
                                       :IND-TIT-DT-CERT-FISC
                  ,TP_CAUS_STOR           =
                :TIT-TP-CAUS-STOR
                                       :IND-TIT-TP-CAUS-STOR
                  ,TP_CAUS_DISP_STOR      =
                :TIT-TP-CAUS-DISP-STOR
                                       :IND-TIT-TP-CAUS-DISP-STOR
                  ,TP_TIT_MIGRAZ          =
                :TIT-TP-TIT-MIGRAZ
                                       :IND-TIT-TP-TIT-MIGRAZ
                  ,TOT_ACQ_EXP            =
                :TIT-TOT-ACQ-EXP
                                       :IND-TIT-TOT-ACQ-EXP
                  ,TOT_REMUN_ASS          =
                :TIT-TOT-REMUN-ASS
                                       :IND-TIT-TOT-REMUN-ASS
                  ,TOT_COMMIS_INTER       =
                :TIT-TOT-COMMIS-INTER
                                       :IND-TIT-TOT-COMMIS-INTER
                  ,TP_CAUS_RIMB           =
                :TIT-TP-CAUS-RIMB
                                       :IND-TIT-TP-CAUS-RIMB
                  ,TOT_CNBT_ANTIRAC       =
                :TIT-TOT-CNBT-ANTIRAC
                                       :IND-TIT-TOT-CNBT-ANTIRAC
                  ,FL_INC_AUTOGEN         =
                :TIT-FL-INC-AUTOGEN
                                       :IND-TIT-FL-INC-AUTOGEN
                WHERE     DS_RIGA = :TIT-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-TIT
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-TIT
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-TIT
           INTO
                :TIT-ID-TIT-CONT
               ,:TIT-ID-OGG
               ,:TIT-TP-OGG
               ,:TIT-IB-RICH
                :IND-TIT-IB-RICH
               ,:TIT-ID-MOVI-CRZ
               ,:TIT-ID-MOVI-CHIU
                :IND-TIT-ID-MOVI-CHIU
               ,:TIT-DT-INI-EFF-DB
               ,:TIT-DT-END-EFF-DB
               ,:TIT-COD-COMP-ANIA
               ,:TIT-TP-TIT
               ,:TIT-PROG-TIT
                :IND-TIT-PROG-TIT
               ,:TIT-TP-PRE-TIT
               ,:TIT-TP-STAT-TIT
               ,:TIT-DT-INI-COP-DB
                :IND-TIT-DT-INI-COP
               ,:TIT-DT-END-COP-DB
                :IND-TIT-DT-END-COP
               ,:TIT-IMP-PAG
                :IND-TIT-IMP-PAG
               ,:TIT-FL-SOLL
                :IND-TIT-FL-SOLL
               ,:TIT-FRAZ
                :IND-TIT-FRAZ
               ,:TIT-DT-APPLZ-MORA-DB
                :IND-TIT-DT-APPLZ-MORA
               ,:TIT-FL-MORA
                :IND-TIT-FL-MORA
               ,:TIT-ID-RAPP-RETE
                :IND-TIT-ID-RAPP-RETE
               ,:TIT-ID-RAPP-ANA
                :IND-TIT-ID-RAPP-ANA
               ,:TIT-COD-DVS
                :IND-TIT-COD-DVS
               ,:TIT-DT-EMIS-TIT-DB
                :IND-TIT-DT-EMIS-TIT
               ,:TIT-DT-ESI-TIT-DB
                :IND-TIT-DT-ESI-TIT
               ,:TIT-TOT-PRE-NET
                :IND-TIT-TOT-PRE-NET
               ,:TIT-TOT-INTR-FRAZ
                :IND-TIT-TOT-INTR-FRAZ
               ,:TIT-TOT-INTR-MORA
                :IND-TIT-TOT-INTR-MORA
               ,:TIT-TOT-INTR-PREST
                :IND-TIT-TOT-INTR-PREST
               ,:TIT-TOT-INTR-RETDT
                :IND-TIT-TOT-INTR-RETDT
               ,:TIT-TOT-INTR-RIAT
                :IND-TIT-TOT-INTR-RIAT
               ,:TIT-TOT-DIR
                :IND-TIT-TOT-DIR
               ,:TIT-TOT-SPE-MED
                :IND-TIT-TOT-SPE-MED
               ,:TIT-TOT-TAX
                :IND-TIT-TOT-TAX
               ,:TIT-TOT-SOPR-SAN
                :IND-TIT-TOT-SOPR-SAN
               ,:TIT-TOT-SOPR-TEC
                :IND-TIT-TOT-SOPR-TEC
               ,:TIT-TOT-SOPR-SPO
                :IND-TIT-TOT-SOPR-SPO
               ,:TIT-TOT-SOPR-PROF
                :IND-TIT-TOT-SOPR-PROF
               ,:TIT-TOT-SOPR-ALT
                :IND-TIT-TOT-SOPR-ALT
               ,:TIT-TOT-PRE-TOT
                :IND-TIT-TOT-PRE-TOT
               ,:TIT-TOT-PRE-PP-IAS
                :IND-TIT-TOT-PRE-PP-IAS
               ,:TIT-TOT-CAR-ACQ
                :IND-TIT-TOT-CAR-ACQ
               ,:TIT-TOT-CAR-GEST
                :IND-TIT-TOT-CAR-GEST
               ,:TIT-TOT-CAR-INC
                :IND-TIT-TOT-CAR-INC
               ,:TIT-TOT-PRE-SOLO-RSH
                :IND-TIT-TOT-PRE-SOLO-RSH
               ,:TIT-TOT-PROV-ACQ-1AA
                :IND-TIT-TOT-PROV-ACQ-1AA
               ,:TIT-TOT-PROV-ACQ-2AA
                :IND-TIT-TOT-PROV-ACQ-2AA
               ,:TIT-TOT-PROV-RICOR
                :IND-TIT-TOT-PROV-RICOR
               ,:TIT-TOT-PROV-INC
                :IND-TIT-TOT-PROV-INC
               ,:TIT-TOT-PROV-DA-REC
                :IND-TIT-TOT-PROV-DA-REC
               ,:TIT-IMP-AZ
                :IND-TIT-IMP-AZ
               ,:TIT-IMP-ADER
                :IND-TIT-IMP-ADER
               ,:TIT-IMP-TFR
                :IND-TIT-IMP-TFR
               ,:TIT-IMP-VOLO
                :IND-TIT-IMP-VOLO
               ,:TIT-TOT-MANFEE-ANTIC
                :IND-TIT-TOT-MANFEE-ANTIC
               ,:TIT-TOT-MANFEE-RICOR
                :IND-TIT-TOT-MANFEE-RICOR
               ,:TIT-TOT-MANFEE-REC
                :IND-TIT-TOT-MANFEE-REC
               ,:TIT-TP-MEZ-PAG-ADD
                :IND-TIT-TP-MEZ-PAG-ADD
               ,:TIT-ESTR-CNT-CORR-ADD
                :IND-TIT-ESTR-CNT-CORR-ADD
               ,:TIT-DT-VLT-DB
                :IND-TIT-DT-VLT
               ,:TIT-FL-FORZ-DT-VLT
                :IND-TIT-FL-FORZ-DT-VLT
               ,:TIT-DT-CAMBIO-VLT-DB
                :IND-TIT-DT-CAMBIO-VLT
               ,:TIT-TOT-SPE-AGE
                :IND-TIT-TOT-SPE-AGE
               ,:TIT-TOT-CAR-IAS
                :IND-TIT-TOT-CAR-IAS
               ,:TIT-NUM-RAT-ACCORPATE
                :IND-TIT-NUM-RAT-ACCORPATE
               ,:TIT-DS-RIGA
               ,:TIT-DS-OPER-SQL
               ,:TIT-DS-VER
               ,:TIT-DS-TS-INI-CPTZ
               ,:TIT-DS-TS-END-CPTZ
               ,:TIT-DS-UTENTE
               ,:TIT-DS-STATO-ELAB
               ,:TIT-FL-TIT-DA-REINVST
                :IND-TIT-FL-TIT-DA-REINVST
               ,:TIT-DT-RICH-ADD-RID-DB
                :IND-TIT-DT-RICH-ADD-RID
               ,:TIT-TP-ESI-RID
                :IND-TIT-TP-ESI-RID
               ,:TIT-COD-IBAN
                :IND-TIT-COD-IBAN
               ,:TIT-IMP-TRASFE
                :IND-TIT-IMP-TRASFE
               ,:TIT-IMP-TFR-STRC
                :IND-TIT-IMP-TFR-STRC
               ,:TIT-DT-CERT-FISC-DB
                :IND-TIT-DT-CERT-FISC
               ,:TIT-TP-CAUS-STOR
                :IND-TIT-TP-CAUS-STOR
               ,:TIT-TP-CAUS-DISP-STOR
                :IND-TIT-TP-CAUS-DISP-STOR
               ,:TIT-TP-TIT-MIGRAZ
                :IND-TIT-TP-TIT-MIGRAZ
               ,:TIT-TOT-ACQ-EXP
                :IND-TIT-TOT-ACQ-EXP
               ,:TIT-TOT-REMUN-ASS
                :IND-TIT-TOT-REMUN-ASS
               ,:TIT-TOT-COMMIS-INTER
                :IND-TIT-TOT-COMMIS-INTER
               ,:TIT-TP-CAUS-RIMB
                :IND-TIT-TP-CAUS-RIMB
               ,:TIT-TOT-CNBT-ANTIRAC
                :IND-TIT-TOT-CNBT-ANTIRAC
               ,:TIT-FL-INC-AUTOGEN
                :IND-TIT-FL-INC-AUTOGEN
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DCL-CUR-IBS-RICH.
           EXEC SQL
                DECLARE C-IBS-EFF-TIT-0 CURSOR FOR
              SELECT
                     ID_TIT_CONT
                    ,ID_OGG
                    ,TP_OGG
                    ,IB_RICH
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_TIT
                    ,PROG_TIT
                    ,TP_PRE_TIT
                    ,TP_STAT_TIT
                    ,DT_INI_COP
                    ,DT_END_COP
                    ,IMP_PAG
                    ,FL_SOLL
                    ,FRAZ
                    ,DT_APPLZ_MORA
                    ,FL_MORA
                    ,ID_RAPP_RETE
                    ,ID_RAPP_ANA
                    ,COD_DVS
                    ,DT_EMIS_TIT
                    ,DT_ESI_TIT
                    ,TOT_PRE_NET
                    ,TOT_INTR_FRAZ
                    ,TOT_INTR_MORA
                    ,TOT_INTR_PREST
                    ,TOT_INTR_RETDT
                    ,TOT_INTR_RIAT
                    ,TOT_DIR
                    ,TOT_SPE_MED
                    ,TOT_TAX
                    ,TOT_SOPR_SAN
                    ,TOT_SOPR_TEC
                    ,TOT_SOPR_SPO
                    ,TOT_SOPR_PROF
                    ,TOT_SOPR_ALT
                    ,TOT_PRE_TOT
                    ,TOT_PRE_PP_IAS
                    ,TOT_CAR_ACQ
                    ,TOT_CAR_GEST
                    ,TOT_CAR_INC
                    ,TOT_PRE_SOLO_RSH
                    ,TOT_PROV_ACQ_1AA
                    ,TOT_PROV_ACQ_2AA
                    ,TOT_PROV_RICOR
                    ,TOT_PROV_INC
                    ,TOT_PROV_DA_REC
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,TOT_MANFEE_ANTIC
                    ,TOT_MANFEE_RICOR
                    ,TOT_MANFEE_REC
                    ,TP_MEZ_PAG_ADD
                    ,ESTR_CNT_CORR_ADD
                    ,DT_VLT
                    ,FL_FORZ_DT_VLT
                    ,DT_CAMBIO_VLT
                    ,TOT_SPE_AGE
                    ,TOT_CAR_IAS
                    ,NUM_RAT_ACCORPATE
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,FL_TIT_DA_REINVST
                    ,DT_RICH_ADD_RID
                    ,TP_ESI_RID
                    ,COD_IBAN
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,DT_CERT_FISC
                    ,TP_CAUS_STOR
                    ,TP_CAUS_DISP_STOR
                    ,TP_TIT_MIGRAZ
                    ,TOT_ACQ_EXP
                    ,TOT_REMUN_ASS
                    ,TOT_COMMIS_INTER
                    ,TP_CAUS_RIMB
                    ,TOT_CNBT_ANTIRAC
                    ,FL_INC_AUTOGEN
              FROM TIT_CONT
              WHERE     IB_RICH = :TIT-IB-RICH
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_TIT_CONT ASC

           END-EXEC.
       A605-RICH-EX.
           EXIT.


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF TIT-IB-RICH NOT = HIGH-VALUES
               PERFORM A605-DCL-CUR-IBS-RICH
                  THRU A605-RICH-EX
           END-IF.

       A605-EX.
           EXIT.


       A610-SELECT-IBS-RICH.
           EXEC SQL
             SELECT
                ID_TIT_CONT
                ,ID_OGG
                ,TP_OGG
                ,IB_RICH
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_TIT
                ,PROG_TIT
                ,TP_PRE_TIT
                ,TP_STAT_TIT
                ,DT_INI_COP
                ,DT_END_COP
                ,IMP_PAG
                ,FL_SOLL
                ,FRAZ
                ,DT_APPLZ_MORA
                ,FL_MORA
                ,ID_RAPP_RETE
                ,ID_RAPP_ANA
                ,COD_DVS
                ,DT_EMIS_TIT
                ,DT_ESI_TIT
                ,TOT_PRE_NET
                ,TOT_INTR_FRAZ
                ,TOT_INTR_MORA
                ,TOT_INTR_PREST
                ,TOT_INTR_RETDT
                ,TOT_INTR_RIAT
                ,TOT_DIR
                ,TOT_SPE_MED
                ,TOT_TAX
                ,TOT_SOPR_SAN
                ,TOT_SOPR_TEC
                ,TOT_SOPR_SPO
                ,TOT_SOPR_PROF
                ,TOT_SOPR_ALT
                ,TOT_PRE_TOT
                ,TOT_PRE_PP_IAS
                ,TOT_CAR_ACQ
                ,TOT_CAR_GEST
                ,TOT_CAR_INC
                ,TOT_PRE_SOLO_RSH
                ,TOT_PROV_ACQ_1AA
                ,TOT_PROV_ACQ_2AA
                ,TOT_PROV_RICOR
                ,TOT_PROV_INC
                ,TOT_PROV_DA_REC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,TOT_MANFEE_ANTIC
                ,TOT_MANFEE_RICOR
                ,TOT_MANFEE_REC
                ,TP_MEZ_PAG_ADD
                ,ESTR_CNT_CORR_ADD
                ,DT_VLT
                ,FL_FORZ_DT_VLT
                ,DT_CAMBIO_VLT
                ,TOT_SPE_AGE
                ,TOT_CAR_IAS
                ,NUM_RAT_ACCORPATE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_TIT_DA_REINVST
                ,DT_RICH_ADD_RID
                ,TP_ESI_RID
                ,COD_IBAN
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,DT_CERT_FISC
                ,TP_CAUS_STOR
                ,TP_CAUS_DISP_STOR
                ,TP_TIT_MIGRAZ
                ,TOT_ACQ_EXP
                ,TOT_REMUN_ASS
                ,TOT_COMMIS_INTER
                ,TP_CAUS_RIMB
                ,TOT_CNBT_ANTIRAC
                ,FL_INC_AUTOGEN
             INTO
                :TIT-ID-TIT-CONT
               ,:TIT-ID-OGG
               ,:TIT-TP-OGG
               ,:TIT-IB-RICH
                :IND-TIT-IB-RICH
               ,:TIT-ID-MOVI-CRZ
               ,:TIT-ID-MOVI-CHIU
                :IND-TIT-ID-MOVI-CHIU
               ,:TIT-DT-INI-EFF-DB
               ,:TIT-DT-END-EFF-DB
               ,:TIT-COD-COMP-ANIA
               ,:TIT-TP-TIT
               ,:TIT-PROG-TIT
                :IND-TIT-PROG-TIT
               ,:TIT-TP-PRE-TIT
               ,:TIT-TP-STAT-TIT
               ,:TIT-DT-INI-COP-DB
                :IND-TIT-DT-INI-COP
               ,:TIT-DT-END-COP-DB
                :IND-TIT-DT-END-COP
               ,:TIT-IMP-PAG
                :IND-TIT-IMP-PAG
               ,:TIT-FL-SOLL
                :IND-TIT-FL-SOLL
               ,:TIT-FRAZ
                :IND-TIT-FRAZ
               ,:TIT-DT-APPLZ-MORA-DB
                :IND-TIT-DT-APPLZ-MORA
               ,:TIT-FL-MORA
                :IND-TIT-FL-MORA
               ,:TIT-ID-RAPP-RETE
                :IND-TIT-ID-RAPP-RETE
               ,:TIT-ID-RAPP-ANA
                :IND-TIT-ID-RAPP-ANA
               ,:TIT-COD-DVS
                :IND-TIT-COD-DVS
               ,:TIT-DT-EMIS-TIT-DB
                :IND-TIT-DT-EMIS-TIT
               ,:TIT-DT-ESI-TIT-DB
                :IND-TIT-DT-ESI-TIT
               ,:TIT-TOT-PRE-NET
                :IND-TIT-TOT-PRE-NET
               ,:TIT-TOT-INTR-FRAZ
                :IND-TIT-TOT-INTR-FRAZ
               ,:TIT-TOT-INTR-MORA
                :IND-TIT-TOT-INTR-MORA
               ,:TIT-TOT-INTR-PREST
                :IND-TIT-TOT-INTR-PREST
               ,:TIT-TOT-INTR-RETDT
                :IND-TIT-TOT-INTR-RETDT
               ,:TIT-TOT-INTR-RIAT
                :IND-TIT-TOT-INTR-RIAT
               ,:TIT-TOT-DIR
                :IND-TIT-TOT-DIR
               ,:TIT-TOT-SPE-MED
                :IND-TIT-TOT-SPE-MED
               ,:TIT-TOT-TAX
                :IND-TIT-TOT-TAX
               ,:TIT-TOT-SOPR-SAN
                :IND-TIT-TOT-SOPR-SAN
               ,:TIT-TOT-SOPR-TEC
                :IND-TIT-TOT-SOPR-TEC
               ,:TIT-TOT-SOPR-SPO
                :IND-TIT-TOT-SOPR-SPO
               ,:TIT-TOT-SOPR-PROF
                :IND-TIT-TOT-SOPR-PROF
               ,:TIT-TOT-SOPR-ALT
                :IND-TIT-TOT-SOPR-ALT
               ,:TIT-TOT-PRE-TOT
                :IND-TIT-TOT-PRE-TOT
               ,:TIT-TOT-PRE-PP-IAS
                :IND-TIT-TOT-PRE-PP-IAS
               ,:TIT-TOT-CAR-ACQ
                :IND-TIT-TOT-CAR-ACQ
               ,:TIT-TOT-CAR-GEST
                :IND-TIT-TOT-CAR-GEST
               ,:TIT-TOT-CAR-INC
                :IND-TIT-TOT-CAR-INC
               ,:TIT-TOT-PRE-SOLO-RSH
                :IND-TIT-TOT-PRE-SOLO-RSH
               ,:TIT-TOT-PROV-ACQ-1AA
                :IND-TIT-TOT-PROV-ACQ-1AA
               ,:TIT-TOT-PROV-ACQ-2AA
                :IND-TIT-TOT-PROV-ACQ-2AA
               ,:TIT-TOT-PROV-RICOR
                :IND-TIT-TOT-PROV-RICOR
               ,:TIT-TOT-PROV-INC
                :IND-TIT-TOT-PROV-INC
               ,:TIT-TOT-PROV-DA-REC
                :IND-TIT-TOT-PROV-DA-REC
               ,:TIT-IMP-AZ
                :IND-TIT-IMP-AZ
               ,:TIT-IMP-ADER
                :IND-TIT-IMP-ADER
               ,:TIT-IMP-TFR
                :IND-TIT-IMP-TFR
               ,:TIT-IMP-VOLO
                :IND-TIT-IMP-VOLO
               ,:TIT-TOT-MANFEE-ANTIC
                :IND-TIT-TOT-MANFEE-ANTIC
               ,:TIT-TOT-MANFEE-RICOR
                :IND-TIT-TOT-MANFEE-RICOR
               ,:TIT-TOT-MANFEE-REC
                :IND-TIT-TOT-MANFEE-REC
               ,:TIT-TP-MEZ-PAG-ADD
                :IND-TIT-TP-MEZ-PAG-ADD
               ,:TIT-ESTR-CNT-CORR-ADD
                :IND-TIT-ESTR-CNT-CORR-ADD
               ,:TIT-DT-VLT-DB
                :IND-TIT-DT-VLT
               ,:TIT-FL-FORZ-DT-VLT
                :IND-TIT-FL-FORZ-DT-VLT
               ,:TIT-DT-CAMBIO-VLT-DB
                :IND-TIT-DT-CAMBIO-VLT
               ,:TIT-TOT-SPE-AGE
                :IND-TIT-TOT-SPE-AGE
               ,:TIT-TOT-CAR-IAS
                :IND-TIT-TOT-CAR-IAS
               ,:TIT-NUM-RAT-ACCORPATE
                :IND-TIT-NUM-RAT-ACCORPATE
               ,:TIT-DS-RIGA
               ,:TIT-DS-OPER-SQL
               ,:TIT-DS-VER
               ,:TIT-DS-TS-INI-CPTZ
               ,:TIT-DS-TS-END-CPTZ
               ,:TIT-DS-UTENTE
               ,:TIT-DS-STATO-ELAB
               ,:TIT-FL-TIT-DA-REINVST
                :IND-TIT-FL-TIT-DA-REINVST
               ,:TIT-DT-RICH-ADD-RID-DB
                :IND-TIT-DT-RICH-ADD-RID
               ,:TIT-TP-ESI-RID
                :IND-TIT-TP-ESI-RID
               ,:TIT-COD-IBAN
                :IND-TIT-COD-IBAN
               ,:TIT-IMP-TRASFE
                :IND-TIT-IMP-TRASFE
               ,:TIT-IMP-TFR-STRC
                :IND-TIT-IMP-TFR-STRC
               ,:TIT-DT-CERT-FISC-DB
                :IND-TIT-DT-CERT-FISC
               ,:TIT-TP-CAUS-STOR
                :IND-TIT-TP-CAUS-STOR
               ,:TIT-TP-CAUS-DISP-STOR
                :IND-TIT-TP-CAUS-DISP-STOR
               ,:TIT-TP-TIT-MIGRAZ
                :IND-TIT-TP-TIT-MIGRAZ
               ,:TIT-TOT-ACQ-EXP
                :IND-TIT-TOT-ACQ-EXP
               ,:TIT-TOT-REMUN-ASS
                :IND-TIT-TOT-REMUN-ASS
               ,:TIT-TOT-COMMIS-INTER
                :IND-TIT-TOT-COMMIS-INTER
               ,:TIT-TP-CAUS-RIMB
                :IND-TIT-TP-CAUS-RIMB
               ,:TIT-TOT-CNBT-ANTIRAC
                :IND-TIT-TOT-CNBT-ANTIRAC
               ,:TIT-FL-INC-AUTOGEN
                :IND-TIT-FL-INC-AUTOGEN
             FROM TIT_CONT
             WHERE     IB_RICH = :TIT-IB-RICH
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.
       A610-RICH-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF TIT-IB-RICH NOT = HIGH-VALUES
               PERFORM A610-SELECT-IBS-RICH
                  THRU A610-RICH-EX
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           IF TIT-IB-RICH NOT = HIGH-VALUES
              EXEC SQL
                   OPEN C-IBS-EFF-TIT-0
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           IF TIT-IB-RICH NOT = HIGH-VALUES
              EXEC SQL
                   CLOSE C-IBS-EFF-TIT-0
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FN-IBS-RICH.
           EXEC SQL
                FETCH C-IBS-EFF-TIT-0
           INTO
                :TIT-ID-TIT-CONT
               ,:TIT-ID-OGG
               ,:TIT-TP-OGG
               ,:TIT-IB-RICH
                :IND-TIT-IB-RICH
               ,:TIT-ID-MOVI-CRZ
               ,:TIT-ID-MOVI-CHIU
                :IND-TIT-ID-MOVI-CHIU
               ,:TIT-DT-INI-EFF-DB
               ,:TIT-DT-END-EFF-DB
               ,:TIT-COD-COMP-ANIA
               ,:TIT-TP-TIT
               ,:TIT-PROG-TIT
                :IND-TIT-PROG-TIT
               ,:TIT-TP-PRE-TIT
               ,:TIT-TP-STAT-TIT
               ,:TIT-DT-INI-COP-DB
                :IND-TIT-DT-INI-COP
               ,:TIT-DT-END-COP-DB
                :IND-TIT-DT-END-COP
               ,:TIT-IMP-PAG
                :IND-TIT-IMP-PAG
               ,:TIT-FL-SOLL
                :IND-TIT-FL-SOLL
               ,:TIT-FRAZ
                :IND-TIT-FRAZ
               ,:TIT-DT-APPLZ-MORA-DB
                :IND-TIT-DT-APPLZ-MORA
               ,:TIT-FL-MORA
                :IND-TIT-FL-MORA
               ,:TIT-ID-RAPP-RETE
                :IND-TIT-ID-RAPP-RETE
               ,:TIT-ID-RAPP-ANA
                :IND-TIT-ID-RAPP-ANA
               ,:TIT-COD-DVS
                :IND-TIT-COD-DVS
               ,:TIT-DT-EMIS-TIT-DB
                :IND-TIT-DT-EMIS-TIT
               ,:TIT-DT-ESI-TIT-DB
                :IND-TIT-DT-ESI-TIT
               ,:TIT-TOT-PRE-NET
                :IND-TIT-TOT-PRE-NET
               ,:TIT-TOT-INTR-FRAZ
                :IND-TIT-TOT-INTR-FRAZ
               ,:TIT-TOT-INTR-MORA
                :IND-TIT-TOT-INTR-MORA
               ,:TIT-TOT-INTR-PREST
                :IND-TIT-TOT-INTR-PREST
               ,:TIT-TOT-INTR-RETDT
                :IND-TIT-TOT-INTR-RETDT
               ,:TIT-TOT-INTR-RIAT
                :IND-TIT-TOT-INTR-RIAT
               ,:TIT-TOT-DIR
                :IND-TIT-TOT-DIR
               ,:TIT-TOT-SPE-MED
                :IND-TIT-TOT-SPE-MED
               ,:TIT-TOT-TAX
                :IND-TIT-TOT-TAX
               ,:TIT-TOT-SOPR-SAN
                :IND-TIT-TOT-SOPR-SAN
               ,:TIT-TOT-SOPR-TEC
                :IND-TIT-TOT-SOPR-TEC
               ,:TIT-TOT-SOPR-SPO
                :IND-TIT-TOT-SOPR-SPO
               ,:TIT-TOT-SOPR-PROF
                :IND-TIT-TOT-SOPR-PROF
               ,:TIT-TOT-SOPR-ALT
                :IND-TIT-TOT-SOPR-ALT
               ,:TIT-TOT-PRE-TOT
                :IND-TIT-TOT-PRE-TOT
               ,:TIT-TOT-PRE-PP-IAS
                :IND-TIT-TOT-PRE-PP-IAS
               ,:TIT-TOT-CAR-ACQ
                :IND-TIT-TOT-CAR-ACQ
               ,:TIT-TOT-CAR-GEST
                :IND-TIT-TOT-CAR-GEST
               ,:TIT-TOT-CAR-INC
                :IND-TIT-TOT-CAR-INC
               ,:TIT-TOT-PRE-SOLO-RSH
                :IND-TIT-TOT-PRE-SOLO-RSH
               ,:TIT-TOT-PROV-ACQ-1AA
                :IND-TIT-TOT-PROV-ACQ-1AA
               ,:TIT-TOT-PROV-ACQ-2AA
                :IND-TIT-TOT-PROV-ACQ-2AA
               ,:TIT-TOT-PROV-RICOR
                :IND-TIT-TOT-PROV-RICOR
               ,:TIT-TOT-PROV-INC
                :IND-TIT-TOT-PROV-INC
               ,:TIT-TOT-PROV-DA-REC
                :IND-TIT-TOT-PROV-DA-REC
               ,:TIT-IMP-AZ
                :IND-TIT-IMP-AZ
               ,:TIT-IMP-ADER
                :IND-TIT-IMP-ADER
               ,:TIT-IMP-TFR
                :IND-TIT-IMP-TFR
               ,:TIT-IMP-VOLO
                :IND-TIT-IMP-VOLO
               ,:TIT-TOT-MANFEE-ANTIC
                :IND-TIT-TOT-MANFEE-ANTIC
               ,:TIT-TOT-MANFEE-RICOR
                :IND-TIT-TOT-MANFEE-RICOR
               ,:TIT-TOT-MANFEE-REC
                :IND-TIT-TOT-MANFEE-REC
               ,:TIT-TP-MEZ-PAG-ADD
                :IND-TIT-TP-MEZ-PAG-ADD
               ,:TIT-ESTR-CNT-CORR-ADD
                :IND-TIT-ESTR-CNT-CORR-ADD
               ,:TIT-DT-VLT-DB
                :IND-TIT-DT-VLT
               ,:TIT-FL-FORZ-DT-VLT
                :IND-TIT-FL-FORZ-DT-VLT
               ,:TIT-DT-CAMBIO-VLT-DB
                :IND-TIT-DT-CAMBIO-VLT
               ,:TIT-TOT-SPE-AGE
                :IND-TIT-TOT-SPE-AGE
               ,:TIT-TOT-CAR-IAS
                :IND-TIT-TOT-CAR-IAS
               ,:TIT-NUM-RAT-ACCORPATE
                :IND-TIT-NUM-RAT-ACCORPATE
               ,:TIT-DS-RIGA
               ,:TIT-DS-OPER-SQL
               ,:TIT-DS-VER
               ,:TIT-DS-TS-INI-CPTZ
               ,:TIT-DS-TS-END-CPTZ
               ,:TIT-DS-UTENTE
               ,:TIT-DS-STATO-ELAB
               ,:TIT-FL-TIT-DA-REINVST
                :IND-TIT-FL-TIT-DA-REINVST
               ,:TIT-DT-RICH-ADD-RID-DB
                :IND-TIT-DT-RICH-ADD-RID
               ,:TIT-TP-ESI-RID
                :IND-TIT-TP-ESI-RID
               ,:TIT-COD-IBAN
                :IND-TIT-COD-IBAN
               ,:TIT-IMP-TRASFE
                :IND-TIT-IMP-TRASFE
               ,:TIT-IMP-TFR-STRC
                :IND-TIT-IMP-TFR-STRC
               ,:TIT-DT-CERT-FISC-DB
                :IND-TIT-DT-CERT-FISC
               ,:TIT-TP-CAUS-STOR
                :IND-TIT-TP-CAUS-STOR
               ,:TIT-TP-CAUS-DISP-STOR
                :IND-TIT-TP-CAUS-DISP-STOR
               ,:TIT-TP-TIT-MIGRAZ
                :IND-TIT-TP-TIT-MIGRAZ
               ,:TIT-TOT-ACQ-EXP
                :IND-TIT-TOT-ACQ-EXP
               ,:TIT-TOT-REMUN-ASS
                :IND-TIT-TOT-REMUN-ASS
               ,:TIT-TOT-COMMIS-INTER
                :IND-TIT-TOT-COMMIS-INTER
               ,:TIT-TP-CAUS-RIMB
                :IND-TIT-TP-CAUS-RIMB
               ,:TIT-TOT-CNBT-ANTIRAC
                :IND-TIT-TOT-CNBT-ANTIRAC
               ,:TIT-FL-INC-AUTOGEN
                :IND-TIT-FL-INC-AUTOGEN
           END-EXEC.
       A690-RICH-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           IF TIT-IB-RICH NOT = HIGH-VALUES
               PERFORM A690-FN-IBS-RICH
                  THRU A690-RICH-EX
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A670-CLOSE-CURSOR-IBS     THRU A670-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDO-EFF-TIT CURSOR FOR
              SELECT
                     ID_TIT_CONT
                    ,ID_OGG
                    ,TP_OGG
                    ,IB_RICH
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_TIT
                    ,PROG_TIT
                    ,TP_PRE_TIT
                    ,TP_STAT_TIT
                    ,DT_INI_COP
                    ,DT_END_COP
                    ,IMP_PAG
                    ,FL_SOLL
                    ,FRAZ
                    ,DT_APPLZ_MORA
                    ,FL_MORA
                    ,ID_RAPP_RETE
                    ,ID_RAPP_ANA
                    ,COD_DVS
                    ,DT_EMIS_TIT
                    ,DT_ESI_TIT
                    ,TOT_PRE_NET
                    ,TOT_INTR_FRAZ
                    ,TOT_INTR_MORA
                    ,TOT_INTR_PREST
                    ,TOT_INTR_RETDT
                    ,TOT_INTR_RIAT
                    ,TOT_DIR
                    ,TOT_SPE_MED
                    ,TOT_TAX
                    ,TOT_SOPR_SAN
                    ,TOT_SOPR_TEC
                    ,TOT_SOPR_SPO
                    ,TOT_SOPR_PROF
                    ,TOT_SOPR_ALT
                    ,TOT_PRE_TOT
                    ,TOT_PRE_PP_IAS
                    ,TOT_CAR_ACQ
                    ,TOT_CAR_GEST
                    ,TOT_CAR_INC
                    ,TOT_PRE_SOLO_RSH
                    ,TOT_PROV_ACQ_1AA
                    ,TOT_PROV_ACQ_2AA
                    ,TOT_PROV_RICOR
                    ,TOT_PROV_INC
                    ,TOT_PROV_DA_REC
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,TOT_MANFEE_ANTIC
                    ,TOT_MANFEE_RICOR
                    ,TOT_MANFEE_REC
                    ,TP_MEZ_PAG_ADD
                    ,ESTR_CNT_CORR_ADD
                    ,DT_VLT
                    ,FL_FORZ_DT_VLT
                    ,DT_CAMBIO_VLT
                    ,TOT_SPE_AGE
                    ,TOT_CAR_IAS
                    ,NUM_RAT_ACCORPATE
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,FL_TIT_DA_REINVST
                    ,DT_RICH_ADD_RID
                    ,TP_ESI_RID
                    ,COD_IBAN
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,DT_CERT_FISC
                    ,TP_CAUS_STOR
                    ,TP_CAUS_DISP_STOR
                    ,TP_TIT_MIGRAZ
                    ,TOT_ACQ_EXP
                    ,TOT_REMUN_ASS
                    ,TOT_COMMIS_INTER
                    ,TP_CAUS_RIMB
                    ,TOT_CNBT_ANTIRAC
                    ,FL_INC_AUTOGEN
              FROM TIT_CONT
              WHERE     ID_OGG = :TIT-ID-OGG
                    AND TP_OGG = :TIT-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_TIT_CONT ASC

           END-EXEC.

       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TIT_CONT
                ,ID_OGG
                ,TP_OGG
                ,IB_RICH
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_TIT
                ,PROG_TIT
                ,TP_PRE_TIT
                ,TP_STAT_TIT
                ,DT_INI_COP
                ,DT_END_COP
                ,IMP_PAG
                ,FL_SOLL
                ,FRAZ
                ,DT_APPLZ_MORA
                ,FL_MORA
                ,ID_RAPP_RETE
                ,ID_RAPP_ANA
                ,COD_DVS
                ,DT_EMIS_TIT
                ,DT_ESI_TIT
                ,TOT_PRE_NET
                ,TOT_INTR_FRAZ
                ,TOT_INTR_MORA
                ,TOT_INTR_PREST
                ,TOT_INTR_RETDT
                ,TOT_INTR_RIAT
                ,TOT_DIR
                ,TOT_SPE_MED
                ,TOT_TAX
                ,TOT_SOPR_SAN
                ,TOT_SOPR_TEC
                ,TOT_SOPR_SPO
                ,TOT_SOPR_PROF
                ,TOT_SOPR_ALT
                ,TOT_PRE_TOT
                ,TOT_PRE_PP_IAS
                ,TOT_CAR_ACQ
                ,TOT_CAR_GEST
                ,TOT_CAR_INC
                ,TOT_PRE_SOLO_RSH
                ,TOT_PROV_ACQ_1AA
                ,TOT_PROV_ACQ_2AA
                ,TOT_PROV_RICOR
                ,TOT_PROV_INC
                ,TOT_PROV_DA_REC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,TOT_MANFEE_ANTIC
                ,TOT_MANFEE_RICOR
                ,TOT_MANFEE_REC
                ,TP_MEZ_PAG_ADD
                ,ESTR_CNT_CORR_ADD
                ,DT_VLT
                ,FL_FORZ_DT_VLT
                ,DT_CAMBIO_VLT
                ,TOT_SPE_AGE
                ,TOT_CAR_IAS
                ,NUM_RAT_ACCORPATE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_TIT_DA_REINVST
                ,DT_RICH_ADD_RID
                ,TP_ESI_RID
                ,COD_IBAN
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,DT_CERT_FISC
                ,TP_CAUS_STOR
                ,TP_CAUS_DISP_STOR
                ,TP_TIT_MIGRAZ
                ,TOT_ACQ_EXP
                ,TOT_REMUN_ASS
                ,TOT_COMMIS_INTER
                ,TP_CAUS_RIMB
                ,TOT_CNBT_ANTIRAC
                ,FL_INC_AUTOGEN
             INTO
                :TIT-ID-TIT-CONT
               ,:TIT-ID-OGG
               ,:TIT-TP-OGG
               ,:TIT-IB-RICH
                :IND-TIT-IB-RICH
               ,:TIT-ID-MOVI-CRZ
               ,:TIT-ID-MOVI-CHIU
                :IND-TIT-ID-MOVI-CHIU
               ,:TIT-DT-INI-EFF-DB
               ,:TIT-DT-END-EFF-DB
               ,:TIT-COD-COMP-ANIA
               ,:TIT-TP-TIT
               ,:TIT-PROG-TIT
                :IND-TIT-PROG-TIT
               ,:TIT-TP-PRE-TIT
               ,:TIT-TP-STAT-TIT
               ,:TIT-DT-INI-COP-DB
                :IND-TIT-DT-INI-COP
               ,:TIT-DT-END-COP-DB
                :IND-TIT-DT-END-COP
               ,:TIT-IMP-PAG
                :IND-TIT-IMP-PAG
               ,:TIT-FL-SOLL
                :IND-TIT-FL-SOLL
               ,:TIT-FRAZ
                :IND-TIT-FRAZ
               ,:TIT-DT-APPLZ-MORA-DB
                :IND-TIT-DT-APPLZ-MORA
               ,:TIT-FL-MORA
                :IND-TIT-FL-MORA
               ,:TIT-ID-RAPP-RETE
                :IND-TIT-ID-RAPP-RETE
               ,:TIT-ID-RAPP-ANA
                :IND-TIT-ID-RAPP-ANA
               ,:TIT-COD-DVS
                :IND-TIT-COD-DVS
               ,:TIT-DT-EMIS-TIT-DB
                :IND-TIT-DT-EMIS-TIT
               ,:TIT-DT-ESI-TIT-DB
                :IND-TIT-DT-ESI-TIT
               ,:TIT-TOT-PRE-NET
                :IND-TIT-TOT-PRE-NET
               ,:TIT-TOT-INTR-FRAZ
                :IND-TIT-TOT-INTR-FRAZ
               ,:TIT-TOT-INTR-MORA
                :IND-TIT-TOT-INTR-MORA
               ,:TIT-TOT-INTR-PREST
                :IND-TIT-TOT-INTR-PREST
               ,:TIT-TOT-INTR-RETDT
                :IND-TIT-TOT-INTR-RETDT
               ,:TIT-TOT-INTR-RIAT
                :IND-TIT-TOT-INTR-RIAT
               ,:TIT-TOT-DIR
                :IND-TIT-TOT-DIR
               ,:TIT-TOT-SPE-MED
                :IND-TIT-TOT-SPE-MED
               ,:TIT-TOT-TAX
                :IND-TIT-TOT-TAX
               ,:TIT-TOT-SOPR-SAN
                :IND-TIT-TOT-SOPR-SAN
               ,:TIT-TOT-SOPR-TEC
                :IND-TIT-TOT-SOPR-TEC
               ,:TIT-TOT-SOPR-SPO
                :IND-TIT-TOT-SOPR-SPO
               ,:TIT-TOT-SOPR-PROF
                :IND-TIT-TOT-SOPR-PROF
               ,:TIT-TOT-SOPR-ALT
                :IND-TIT-TOT-SOPR-ALT
               ,:TIT-TOT-PRE-TOT
                :IND-TIT-TOT-PRE-TOT
               ,:TIT-TOT-PRE-PP-IAS
                :IND-TIT-TOT-PRE-PP-IAS
               ,:TIT-TOT-CAR-ACQ
                :IND-TIT-TOT-CAR-ACQ
               ,:TIT-TOT-CAR-GEST
                :IND-TIT-TOT-CAR-GEST
               ,:TIT-TOT-CAR-INC
                :IND-TIT-TOT-CAR-INC
               ,:TIT-TOT-PRE-SOLO-RSH
                :IND-TIT-TOT-PRE-SOLO-RSH
               ,:TIT-TOT-PROV-ACQ-1AA
                :IND-TIT-TOT-PROV-ACQ-1AA
               ,:TIT-TOT-PROV-ACQ-2AA
                :IND-TIT-TOT-PROV-ACQ-2AA
               ,:TIT-TOT-PROV-RICOR
                :IND-TIT-TOT-PROV-RICOR
               ,:TIT-TOT-PROV-INC
                :IND-TIT-TOT-PROV-INC
               ,:TIT-TOT-PROV-DA-REC
                :IND-TIT-TOT-PROV-DA-REC
               ,:TIT-IMP-AZ
                :IND-TIT-IMP-AZ
               ,:TIT-IMP-ADER
                :IND-TIT-IMP-ADER
               ,:TIT-IMP-TFR
                :IND-TIT-IMP-TFR
               ,:TIT-IMP-VOLO
                :IND-TIT-IMP-VOLO
               ,:TIT-TOT-MANFEE-ANTIC
                :IND-TIT-TOT-MANFEE-ANTIC
               ,:TIT-TOT-MANFEE-RICOR
                :IND-TIT-TOT-MANFEE-RICOR
               ,:TIT-TOT-MANFEE-REC
                :IND-TIT-TOT-MANFEE-REC
               ,:TIT-TP-MEZ-PAG-ADD
                :IND-TIT-TP-MEZ-PAG-ADD
               ,:TIT-ESTR-CNT-CORR-ADD
                :IND-TIT-ESTR-CNT-CORR-ADD
               ,:TIT-DT-VLT-DB
                :IND-TIT-DT-VLT
               ,:TIT-FL-FORZ-DT-VLT
                :IND-TIT-FL-FORZ-DT-VLT
               ,:TIT-DT-CAMBIO-VLT-DB
                :IND-TIT-DT-CAMBIO-VLT
               ,:TIT-TOT-SPE-AGE
                :IND-TIT-TOT-SPE-AGE
               ,:TIT-TOT-CAR-IAS
                :IND-TIT-TOT-CAR-IAS
               ,:TIT-NUM-RAT-ACCORPATE
                :IND-TIT-NUM-RAT-ACCORPATE
               ,:TIT-DS-RIGA
               ,:TIT-DS-OPER-SQL
               ,:TIT-DS-VER
               ,:TIT-DS-TS-INI-CPTZ
               ,:TIT-DS-TS-END-CPTZ
               ,:TIT-DS-UTENTE
               ,:TIT-DS-STATO-ELAB
               ,:TIT-FL-TIT-DA-REINVST
                :IND-TIT-FL-TIT-DA-REINVST
               ,:TIT-DT-RICH-ADD-RID-DB
                :IND-TIT-DT-RICH-ADD-RID
               ,:TIT-TP-ESI-RID
                :IND-TIT-TP-ESI-RID
               ,:TIT-COD-IBAN
                :IND-TIT-COD-IBAN
               ,:TIT-IMP-TRASFE
                :IND-TIT-IMP-TRASFE
               ,:TIT-IMP-TFR-STRC
                :IND-TIT-IMP-TFR-STRC
               ,:TIT-DT-CERT-FISC-DB
                :IND-TIT-DT-CERT-FISC
               ,:TIT-TP-CAUS-STOR
                :IND-TIT-TP-CAUS-STOR
               ,:TIT-TP-CAUS-DISP-STOR
                :IND-TIT-TP-CAUS-DISP-STOR
               ,:TIT-TP-TIT-MIGRAZ
                :IND-TIT-TP-TIT-MIGRAZ
               ,:TIT-TOT-ACQ-EXP
                :IND-TIT-TOT-ACQ-EXP
               ,:TIT-TOT-REMUN-ASS
                :IND-TIT-TOT-REMUN-ASS
               ,:TIT-TOT-COMMIS-INTER
                :IND-TIT-TOT-COMMIS-INTER
               ,:TIT-TP-CAUS-RIMB
                :IND-TIT-TP-CAUS-RIMB
               ,:TIT-TOT-CNBT-ANTIRAC
                :IND-TIT-TOT-CNBT-ANTIRAC
               ,:TIT-FL-INC-AUTOGEN
                :IND-TIT-FL-INC-AUTOGEN
             FROM TIT_CONT
             WHERE     ID_OGG = :TIT-ID-OGG
                    AND TP_OGG = :TIT-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           EXEC SQL
                OPEN C-IDO-EFF-TIT
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           EXEC SQL
                CLOSE C-IDO-EFF-TIT
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           EXEC SQL
                FETCH C-IDO-EFF-TIT
           INTO
                :TIT-ID-TIT-CONT
               ,:TIT-ID-OGG
               ,:TIT-TP-OGG
               ,:TIT-IB-RICH
                :IND-TIT-IB-RICH
               ,:TIT-ID-MOVI-CRZ
               ,:TIT-ID-MOVI-CHIU
                :IND-TIT-ID-MOVI-CHIU
               ,:TIT-DT-INI-EFF-DB
               ,:TIT-DT-END-EFF-DB
               ,:TIT-COD-COMP-ANIA
               ,:TIT-TP-TIT
               ,:TIT-PROG-TIT
                :IND-TIT-PROG-TIT
               ,:TIT-TP-PRE-TIT
               ,:TIT-TP-STAT-TIT
               ,:TIT-DT-INI-COP-DB
                :IND-TIT-DT-INI-COP
               ,:TIT-DT-END-COP-DB
                :IND-TIT-DT-END-COP
               ,:TIT-IMP-PAG
                :IND-TIT-IMP-PAG
               ,:TIT-FL-SOLL
                :IND-TIT-FL-SOLL
               ,:TIT-FRAZ
                :IND-TIT-FRAZ
               ,:TIT-DT-APPLZ-MORA-DB
                :IND-TIT-DT-APPLZ-MORA
               ,:TIT-FL-MORA
                :IND-TIT-FL-MORA
               ,:TIT-ID-RAPP-RETE
                :IND-TIT-ID-RAPP-RETE
               ,:TIT-ID-RAPP-ANA
                :IND-TIT-ID-RAPP-ANA
               ,:TIT-COD-DVS
                :IND-TIT-COD-DVS
               ,:TIT-DT-EMIS-TIT-DB
                :IND-TIT-DT-EMIS-TIT
               ,:TIT-DT-ESI-TIT-DB
                :IND-TIT-DT-ESI-TIT
               ,:TIT-TOT-PRE-NET
                :IND-TIT-TOT-PRE-NET
               ,:TIT-TOT-INTR-FRAZ
                :IND-TIT-TOT-INTR-FRAZ
               ,:TIT-TOT-INTR-MORA
                :IND-TIT-TOT-INTR-MORA
               ,:TIT-TOT-INTR-PREST
                :IND-TIT-TOT-INTR-PREST
               ,:TIT-TOT-INTR-RETDT
                :IND-TIT-TOT-INTR-RETDT
               ,:TIT-TOT-INTR-RIAT
                :IND-TIT-TOT-INTR-RIAT
               ,:TIT-TOT-DIR
                :IND-TIT-TOT-DIR
               ,:TIT-TOT-SPE-MED
                :IND-TIT-TOT-SPE-MED
               ,:TIT-TOT-TAX
                :IND-TIT-TOT-TAX
               ,:TIT-TOT-SOPR-SAN
                :IND-TIT-TOT-SOPR-SAN
               ,:TIT-TOT-SOPR-TEC
                :IND-TIT-TOT-SOPR-TEC
               ,:TIT-TOT-SOPR-SPO
                :IND-TIT-TOT-SOPR-SPO
               ,:TIT-TOT-SOPR-PROF
                :IND-TIT-TOT-SOPR-PROF
               ,:TIT-TOT-SOPR-ALT
                :IND-TIT-TOT-SOPR-ALT
               ,:TIT-TOT-PRE-TOT
                :IND-TIT-TOT-PRE-TOT
               ,:TIT-TOT-PRE-PP-IAS
                :IND-TIT-TOT-PRE-PP-IAS
               ,:TIT-TOT-CAR-ACQ
                :IND-TIT-TOT-CAR-ACQ
               ,:TIT-TOT-CAR-GEST
                :IND-TIT-TOT-CAR-GEST
               ,:TIT-TOT-CAR-INC
                :IND-TIT-TOT-CAR-INC
               ,:TIT-TOT-PRE-SOLO-RSH
                :IND-TIT-TOT-PRE-SOLO-RSH
               ,:TIT-TOT-PROV-ACQ-1AA
                :IND-TIT-TOT-PROV-ACQ-1AA
               ,:TIT-TOT-PROV-ACQ-2AA
                :IND-TIT-TOT-PROV-ACQ-2AA
               ,:TIT-TOT-PROV-RICOR
                :IND-TIT-TOT-PROV-RICOR
               ,:TIT-TOT-PROV-INC
                :IND-TIT-TOT-PROV-INC
               ,:TIT-TOT-PROV-DA-REC
                :IND-TIT-TOT-PROV-DA-REC
               ,:TIT-IMP-AZ
                :IND-TIT-IMP-AZ
               ,:TIT-IMP-ADER
                :IND-TIT-IMP-ADER
               ,:TIT-IMP-TFR
                :IND-TIT-IMP-TFR
               ,:TIT-IMP-VOLO
                :IND-TIT-IMP-VOLO
               ,:TIT-TOT-MANFEE-ANTIC
                :IND-TIT-TOT-MANFEE-ANTIC
               ,:TIT-TOT-MANFEE-RICOR
                :IND-TIT-TOT-MANFEE-RICOR
               ,:TIT-TOT-MANFEE-REC
                :IND-TIT-TOT-MANFEE-REC
               ,:TIT-TP-MEZ-PAG-ADD
                :IND-TIT-TP-MEZ-PAG-ADD
               ,:TIT-ESTR-CNT-CORR-ADD
                :IND-TIT-ESTR-CNT-CORR-ADD
               ,:TIT-DT-VLT-DB
                :IND-TIT-DT-VLT
               ,:TIT-FL-FORZ-DT-VLT
                :IND-TIT-FL-FORZ-DT-VLT
               ,:TIT-DT-CAMBIO-VLT-DB
                :IND-TIT-DT-CAMBIO-VLT
               ,:TIT-TOT-SPE-AGE
                :IND-TIT-TOT-SPE-AGE
               ,:TIT-TOT-CAR-IAS
                :IND-TIT-TOT-CAR-IAS
               ,:TIT-NUM-RAT-ACCORPATE
                :IND-TIT-NUM-RAT-ACCORPATE
               ,:TIT-DS-RIGA
               ,:TIT-DS-OPER-SQL
               ,:TIT-DS-VER
               ,:TIT-DS-TS-INI-CPTZ
               ,:TIT-DS-TS-END-CPTZ
               ,:TIT-DS-UTENTE
               ,:TIT-DS-STATO-ELAB
               ,:TIT-FL-TIT-DA-REINVST
                :IND-TIT-FL-TIT-DA-REINVST
               ,:TIT-DT-RICH-ADD-RID-DB
                :IND-TIT-DT-RICH-ADD-RID
               ,:TIT-TP-ESI-RID
                :IND-TIT-TP-ESI-RID
               ,:TIT-COD-IBAN
                :IND-TIT-COD-IBAN
               ,:TIT-IMP-TRASFE
                :IND-TIT-IMP-TRASFE
               ,:TIT-IMP-TFR-STRC
                :IND-TIT-IMP-TFR-STRC
               ,:TIT-DT-CERT-FISC-DB
                :IND-TIT-DT-CERT-FISC
               ,:TIT-TP-CAUS-STOR
                :IND-TIT-TP-CAUS-STOR
               ,:TIT-TP-CAUS-DISP-STOR
                :IND-TIT-TP-CAUS-DISP-STOR
               ,:TIT-TP-TIT-MIGRAZ
                :IND-TIT-TP-TIT-MIGRAZ
               ,:TIT-TOT-ACQ-EXP
                :IND-TIT-TOT-ACQ-EXP
               ,:TIT-TOT-REMUN-ASS
                :IND-TIT-TOT-REMUN-ASS
               ,:TIT-TOT-COMMIS-INTER
                :IND-TIT-TOT-COMMIS-INTER
               ,:TIT-TP-CAUS-RIMB
                :IND-TIT-TP-CAUS-RIMB
               ,:TIT-TOT-CNBT-ANTIRAC
                :IND-TIT-TOT-CNBT-ANTIRAC
               ,:TIT-FL-INC-AUTOGEN
                :IND-TIT-FL-INC-AUTOGEN
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TIT_CONT
                ,ID_OGG
                ,TP_OGG
                ,IB_RICH
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_TIT
                ,PROG_TIT
                ,TP_PRE_TIT
                ,TP_STAT_TIT
                ,DT_INI_COP
                ,DT_END_COP
                ,IMP_PAG
                ,FL_SOLL
                ,FRAZ
                ,DT_APPLZ_MORA
                ,FL_MORA
                ,ID_RAPP_RETE
                ,ID_RAPP_ANA
                ,COD_DVS
                ,DT_EMIS_TIT
                ,DT_ESI_TIT
                ,TOT_PRE_NET
                ,TOT_INTR_FRAZ
                ,TOT_INTR_MORA
                ,TOT_INTR_PREST
                ,TOT_INTR_RETDT
                ,TOT_INTR_RIAT
                ,TOT_DIR
                ,TOT_SPE_MED
                ,TOT_TAX
                ,TOT_SOPR_SAN
                ,TOT_SOPR_TEC
                ,TOT_SOPR_SPO
                ,TOT_SOPR_PROF
                ,TOT_SOPR_ALT
                ,TOT_PRE_TOT
                ,TOT_PRE_PP_IAS
                ,TOT_CAR_ACQ
                ,TOT_CAR_GEST
                ,TOT_CAR_INC
                ,TOT_PRE_SOLO_RSH
                ,TOT_PROV_ACQ_1AA
                ,TOT_PROV_ACQ_2AA
                ,TOT_PROV_RICOR
                ,TOT_PROV_INC
                ,TOT_PROV_DA_REC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,TOT_MANFEE_ANTIC
                ,TOT_MANFEE_RICOR
                ,TOT_MANFEE_REC
                ,TP_MEZ_PAG_ADD
                ,ESTR_CNT_CORR_ADD
                ,DT_VLT
                ,FL_FORZ_DT_VLT
                ,DT_CAMBIO_VLT
                ,TOT_SPE_AGE
                ,TOT_CAR_IAS
                ,NUM_RAT_ACCORPATE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_TIT_DA_REINVST
                ,DT_RICH_ADD_RID
                ,TP_ESI_RID
                ,COD_IBAN
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,DT_CERT_FISC
                ,TP_CAUS_STOR
                ,TP_CAUS_DISP_STOR
                ,TP_TIT_MIGRAZ
                ,TOT_ACQ_EXP
                ,TOT_REMUN_ASS
                ,TOT_COMMIS_INTER
                ,TP_CAUS_RIMB
                ,TOT_CNBT_ANTIRAC
                ,FL_INC_AUTOGEN
             INTO
                :TIT-ID-TIT-CONT
               ,:TIT-ID-OGG
               ,:TIT-TP-OGG
               ,:TIT-IB-RICH
                :IND-TIT-IB-RICH
               ,:TIT-ID-MOVI-CRZ
               ,:TIT-ID-MOVI-CHIU
                :IND-TIT-ID-MOVI-CHIU
               ,:TIT-DT-INI-EFF-DB
               ,:TIT-DT-END-EFF-DB
               ,:TIT-COD-COMP-ANIA
               ,:TIT-TP-TIT
               ,:TIT-PROG-TIT
                :IND-TIT-PROG-TIT
               ,:TIT-TP-PRE-TIT
               ,:TIT-TP-STAT-TIT
               ,:TIT-DT-INI-COP-DB
                :IND-TIT-DT-INI-COP
               ,:TIT-DT-END-COP-DB
                :IND-TIT-DT-END-COP
               ,:TIT-IMP-PAG
                :IND-TIT-IMP-PAG
               ,:TIT-FL-SOLL
                :IND-TIT-FL-SOLL
               ,:TIT-FRAZ
                :IND-TIT-FRAZ
               ,:TIT-DT-APPLZ-MORA-DB
                :IND-TIT-DT-APPLZ-MORA
               ,:TIT-FL-MORA
                :IND-TIT-FL-MORA
               ,:TIT-ID-RAPP-RETE
                :IND-TIT-ID-RAPP-RETE
               ,:TIT-ID-RAPP-ANA
                :IND-TIT-ID-RAPP-ANA
               ,:TIT-COD-DVS
                :IND-TIT-COD-DVS
               ,:TIT-DT-EMIS-TIT-DB
                :IND-TIT-DT-EMIS-TIT
               ,:TIT-DT-ESI-TIT-DB
                :IND-TIT-DT-ESI-TIT
               ,:TIT-TOT-PRE-NET
                :IND-TIT-TOT-PRE-NET
               ,:TIT-TOT-INTR-FRAZ
                :IND-TIT-TOT-INTR-FRAZ
               ,:TIT-TOT-INTR-MORA
                :IND-TIT-TOT-INTR-MORA
               ,:TIT-TOT-INTR-PREST
                :IND-TIT-TOT-INTR-PREST
               ,:TIT-TOT-INTR-RETDT
                :IND-TIT-TOT-INTR-RETDT
               ,:TIT-TOT-INTR-RIAT
                :IND-TIT-TOT-INTR-RIAT
               ,:TIT-TOT-DIR
                :IND-TIT-TOT-DIR
               ,:TIT-TOT-SPE-MED
                :IND-TIT-TOT-SPE-MED
               ,:TIT-TOT-TAX
                :IND-TIT-TOT-TAX
               ,:TIT-TOT-SOPR-SAN
                :IND-TIT-TOT-SOPR-SAN
               ,:TIT-TOT-SOPR-TEC
                :IND-TIT-TOT-SOPR-TEC
               ,:TIT-TOT-SOPR-SPO
                :IND-TIT-TOT-SOPR-SPO
               ,:TIT-TOT-SOPR-PROF
                :IND-TIT-TOT-SOPR-PROF
               ,:TIT-TOT-SOPR-ALT
                :IND-TIT-TOT-SOPR-ALT
               ,:TIT-TOT-PRE-TOT
                :IND-TIT-TOT-PRE-TOT
               ,:TIT-TOT-PRE-PP-IAS
                :IND-TIT-TOT-PRE-PP-IAS
               ,:TIT-TOT-CAR-ACQ
                :IND-TIT-TOT-CAR-ACQ
               ,:TIT-TOT-CAR-GEST
                :IND-TIT-TOT-CAR-GEST
               ,:TIT-TOT-CAR-INC
                :IND-TIT-TOT-CAR-INC
               ,:TIT-TOT-PRE-SOLO-RSH
                :IND-TIT-TOT-PRE-SOLO-RSH
               ,:TIT-TOT-PROV-ACQ-1AA
                :IND-TIT-TOT-PROV-ACQ-1AA
               ,:TIT-TOT-PROV-ACQ-2AA
                :IND-TIT-TOT-PROV-ACQ-2AA
               ,:TIT-TOT-PROV-RICOR
                :IND-TIT-TOT-PROV-RICOR
               ,:TIT-TOT-PROV-INC
                :IND-TIT-TOT-PROV-INC
               ,:TIT-TOT-PROV-DA-REC
                :IND-TIT-TOT-PROV-DA-REC
               ,:TIT-IMP-AZ
                :IND-TIT-IMP-AZ
               ,:TIT-IMP-ADER
                :IND-TIT-IMP-ADER
               ,:TIT-IMP-TFR
                :IND-TIT-IMP-TFR
               ,:TIT-IMP-VOLO
                :IND-TIT-IMP-VOLO
               ,:TIT-TOT-MANFEE-ANTIC
                :IND-TIT-TOT-MANFEE-ANTIC
               ,:TIT-TOT-MANFEE-RICOR
                :IND-TIT-TOT-MANFEE-RICOR
               ,:TIT-TOT-MANFEE-REC
                :IND-TIT-TOT-MANFEE-REC
               ,:TIT-TP-MEZ-PAG-ADD
                :IND-TIT-TP-MEZ-PAG-ADD
               ,:TIT-ESTR-CNT-CORR-ADD
                :IND-TIT-ESTR-CNT-CORR-ADD
               ,:TIT-DT-VLT-DB
                :IND-TIT-DT-VLT
               ,:TIT-FL-FORZ-DT-VLT
                :IND-TIT-FL-FORZ-DT-VLT
               ,:TIT-DT-CAMBIO-VLT-DB
                :IND-TIT-DT-CAMBIO-VLT
               ,:TIT-TOT-SPE-AGE
                :IND-TIT-TOT-SPE-AGE
               ,:TIT-TOT-CAR-IAS
                :IND-TIT-TOT-CAR-IAS
               ,:TIT-NUM-RAT-ACCORPATE
                :IND-TIT-NUM-RAT-ACCORPATE
               ,:TIT-DS-RIGA
               ,:TIT-DS-OPER-SQL
               ,:TIT-DS-VER
               ,:TIT-DS-TS-INI-CPTZ
               ,:TIT-DS-TS-END-CPTZ
               ,:TIT-DS-UTENTE
               ,:TIT-DS-STATO-ELAB
               ,:TIT-FL-TIT-DA-REINVST
                :IND-TIT-FL-TIT-DA-REINVST
               ,:TIT-DT-RICH-ADD-RID-DB
                :IND-TIT-DT-RICH-ADD-RID
               ,:TIT-TP-ESI-RID
                :IND-TIT-TP-ESI-RID
               ,:TIT-COD-IBAN
                :IND-TIT-COD-IBAN
               ,:TIT-IMP-TRASFE
                :IND-TIT-IMP-TRASFE
               ,:TIT-IMP-TFR-STRC
                :IND-TIT-IMP-TFR-STRC
               ,:TIT-DT-CERT-FISC-DB
                :IND-TIT-DT-CERT-FISC
               ,:TIT-TP-CAUS-STOR
                :IND-TIT-TP-CAUS-STOR
               ,:TIT-TP-CAUS-DISP-STOR
                :IND-TIT-TP-CAUS-DISP-STOR
               ,:TIT-TP-TIT-MIGRAZ
                :IND-TIT-TP-TIT-MIGRAZ
               ,:TIT-TOT-ACQ-EXP
                :IND-TIT-TOT-ACQ-EXP
               ,:TIT-TOT-REMUN-ASS
                :IND-TIT-TOT-REMUN-ASS
               ,:TIT-TOT-COMMIS-INTER
                :IND-TIT-TOT-COMMIS-INTER
               ,:TIT-TP-CAUS-RIMB
                :IND-TIT-TP-CAUS-RIMB
               ,:TIT-TOT-CNBT-ANTIRAC
                :IND-TIT-TOT-CNBT-ANTIRAC
               ,:TIT-FL-INC-AUTOGEN
                :IND-TIT-FL-INC-AUTOGEN
             FROM TIT_CONT
             WHERE     ID_TIT_CONT = :TIT-ID-TIT-CONT
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DCL-CUR-IBS-RICH.
           EXEC SQL
                DECLARE C-IBS-CPZ-TIT-0 CURSOR FOR
              SELECT
                     ID_TIT_CONT
                    ,ID_OGG
                    ,TP_OGG
                    ,IB_RICH
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_TIT
                    ,PROG_TIT
                    ,TP_PRE_TIT
                    ,TP_STAT_TIT
                    ,DT_INI_COP
                    ,DT_END_COP
                    ,IMP_PAG
                    ,FL_SOLL
                    ,FRAZ
                    ,DT_APPLZ_MORA
                    ,FL_MORA
                    ,ID_RAPP_RETE
                    ,ID_RAPP_ANA
                    ,COD_DVS
                    ,DT_EMIS_TIT
                    ,DT_ESI_TIT
                    ,TOT_PRE_NET
                    ,TOT_INTR_FRAZ
                    ,TOT_INTR_MORA
                    ,TOT_INTR_PREST
                    ,TOT_INTR_RETDT
                    ,TOT_INTR_RIAT
                    ,TOT_DIR
                    ,TOT_SPE_MED
                    ,TOT_TAX
                    ,TOT_SOPR_SAN
                    ,TOT_SOPR_TEC
                    ,TOT_SOPR_SPO
                    ,TOT_SOPR_PROF
                    ,TOT_SOPR_ALT
                    ,TOT_PRE_TOT
                    ,TOT_PRE_PP_IAS
                    ,TOT_CAR_ACQ
                    ,TOT_CAR_GEST
                    ,TOT_CAR_INC
                    ,TOT_PRE_SOLO_RSH
                    ,TOT_PROV_ACQ_1AA
                    ,TOT_PROV_ACQ_2AA
                    ,TOT_PROV_RICOR
                    ,TOT_PROV_INC
                    ,TOT_PROV_DA_REC
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,TOT_MANFEE_ANTIC
                    ,TOT_MANFEE_RICOR
                    ,TOT_MANFEE_REC
                    ,TP_MEZ_PAG_ADD
                    ,ESTR_CNT_CORR_ADD
                    ,DT_VLT
                    ,FL_FORZ_DT_VLT
                    ,DT_CAMBIO_VLT
                    ,TOT_SPE_AGE
                    ,TOT_CAR_IAS
                    ,NUM_RAT_ACCORPATE
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,FL_TIT_DA_REINVST
                    ,DT_RICH_ADD_RID
                    ,TP_ESI_RID
                    ,COD_IBAN
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,DT_CERT_FISC
                    ,TP_CAUS_STOR
                    ,TP_CAUS_DISP_STOR
                    ,TP_TIT_MIGRAZ
                    ,TOT_ACQ_EXP
                    ,TOT_REMUN_ASS
                    ,TOT_COMMIS_INTER
                    ,TP_CAUS_RIMB
                    ,TOT_CNBT_ANTIRAC
                    ,FL_INC_AUTOGEN
              FROM TIT_CONT
              WHERE     IB_RICH = :TIT-IB-RICH
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_TIT_CONT ASC

           END-EXEC.
       B605-RICH-EX.
           EXIT.


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF TIT-IB-RICH NOT = HIGH-VALUES
               PERFORM B605-DCL-CUR-IBS-RICH
                  THRU B605-RICH-EX
           END-IF.

       B605-EX.
           EXIT.


       B610-SELECT-IBS-RICH.
           EXEC SQL
             SELECT
                ID_TIT_CONT
                ,ID_OGG
                ,TP_OGG
                ,IB_RICH
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_TIT
                ,PROG_TIT
                ,TP_PRE_TIT
                ,TP_STAT_TIT
                ,DT_INI_COP
                ,DT_END_COP
                ,IMP_PAG
                ,FL_SOLL
                ,FRAZ
                ,DT_APPLZ_MORA
                ,FL_MORA
                ,ID_RAPP_RETE
                ,ID_RAPP_ANA
                ,COD_DVS
                ,DT_EMIS_TIT
                ,DT_ESI_TIT
                ,TOT_PRE_NET
                ,TOT_INTR_FRAZ
                ,TOT_INTR_MORA
                ,TOT_INTR_PREST
                ,TOT_INTR_RETDT
                ,TOT_INTR_RIAT
                ,TOT_DIR
                ,TOT_SPE_MED
                ,TOT_TAX
                ,TOT_SOPR_SAN
                ,TOT_SOPR_TEC
                ,TOT_SOPR_SPO
                ,TOT_SOPR_PROF
                ,TOT_SOPR_ALT
                ,TOT_PRE_TOT
                ,TOT_PRE_PP_IAS
                ,TOT_CAR_ACQ
                ,TOT_CAR_GEST
                ,TOT_CAR_INC
                ,TOT_PRE_SOLO_RSH
                ,TOT_PROV_ACQ_1AA
                ,TOT_PROV_ACQ_2AA
                ,TOT_PROV_RICOR
                ,TOT_PROV_INC
                ,TOT_PROV_DA_REC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,TOT_MANFEE_ANTIC
                ,TOT_MANFEE_RICOR
                ,TOT_MANFEE_REC
                ,TP_MEZ_PAG_ADD
                ,ESTR_CNT_CORR_ADD
                ,DT_VLT
                ,FL_FORZ_DT_VLT
                ,DT_CAMBIO_VLT
                ,TOT_SPE_AGE
                ,TOT_CAR_IAS
                ,NUM_RAT_ACCORPATE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_TIT_DA_REINVST
                ,DT_RICH_ADD_RID
                ,TP_ESI_RID
                ,COD_IBAN
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,DT_CERT_FISC
                ,TP_CAUS_STOR
                ,TP_CAUS_DISP_STOR
                ,TP_TIT_MIGRAZ
                ,TOT_ACQ_EXP
                ,TOT_REMUN_ASS
                ,TOT_COMMIS_INTER
                ,TP_CAUS_RIMB
                ,TOT_CNBT_ANTIRAC
                ,FL_INC_AUTOGEN
             INTO
                :TIT-ID-TIT-CONT
               ,:TIT-ID-OGG
               ,:TIT-TP-OGG
               ,:TIT-IB-RICH
                :IND-TIT-IB-RICH
               ,:TIT-ID-MOVI-CRZ
               ,:TIT-ID-MOVI-CHIU
                :IND-TIT-ID-MOVI-CHIU
               ,:TIT-DT-INI-EFF-DB
               ,:TIT-DT-END-EFF-DB
               ,:TIT-COD-COMP-ANIA
               ,:TIT-TP-TIT
               ,:TIT-PROG-TIT
                :IND-TIT-PROG-TIT
               ,:TIT-TP-PRE-TIT
               ,:TIT-TP-STAT-TIT
               ,:TIT-DT-INI-COP-DB
                :IND-TIT-DT-INI-COP
               ,:TIT-DT-END-COP-DB
                :IND-TIT-DT-END-COP
               ,:TIT-IMP-PAG
                :IND-TIT-IMP-PAG
               ,:TIT-FL-SOLL
                :IND-TIT-FL-SOLL
               ,:TIT-FRAZ
                :IND-TIT-FRAZ
               ,:TIT-DT-APPLZ-MORA-DB
                :IND-TIT-DT-APPLZ-MORA
               ,:TIT-FL-MORA
                :IND-TIT-FL-MORA
               ,:TIT-ID-RAPP-RETE
                :IND-TIT-ID-RAPP-RETE
               ,:TIT-ID-RAPP-ANA
                :IND-TIT-ID-RAPP-ANA
               ,:TIT-COD-DVS
                :IND-TIT-COD-DVS
               ,:TIT-DT-EMIS-TIT-DB
                :IND-TIT-DT-EMIS-TIT
               ,:TIT-DT-ESI-TIT-DB
                :IND-TIT-DT-ESI-TIT
               ,:TIT-TOT-PRE-NET
                :IND-TIT-TOT-PRE-NET
               ,:TIT-TOT-INTR-FRAZ
                :IND-TIT-TOT-INTR-FRAZ
               ,:TIT-TOT-INTR-MORA
                :IND-TIT-TOT-INTR-MORA
               ,:TIT-TOT-INTR-PREST
                :IND-TIT-TOT-INTR-PREST
               ,:TIT-TOT-INTR-RETDT
                :IND-TIT-TOT-INTR-RETDT
               ,:TIT-TOT-INTR-RIAT
                :IND-TIT-TOT-INTR-RIAT
               ,:TIT-TOT-DIR
                :IND-TIT-TOT-DIR
               ,:TIT-TOT-SPE-MED
                :IND-TIT-TOT-SPE-MED
               ,:TIT-TOT-TAX
                :IND-TIT-TOT-TAX
               ,:TIT-TOT-SOPR-SAN
                :IND-TIT-TOT-SOPR-SAN
               ,:TIT-TOT-SOPR-TEC
                :IND-TIT-TOT-SOPR-TEC
               ,:TIT-TOT-SOPR-SPO
                :IND-TIT-TOT-SOPR-SPO
               ,:TIT-TOT-SOPR-PROF
                :IND-TIT-TOT-SOPR-PROF
               ,:TIT-TOT-SOPR-ALT
                :IND-TIT-TOT-SOPR-ALT
               ,:TIT-TOT-PRE-TOT
                :IND-TIT-TOT-PRE-TOT
               ,:TIT-TOT-PRE-PP-IAS
                :IND-TIT-TOT-PRE-PP-IAS
               ,:TIT-TOT-CAR-ACQ
                :IND-TIT-TOT-CAR-ACQ
               ,:TIT-TOT-CAR-GEST
                :IND-TIT-TOT-CAR-GEST
               ,:TIT-TOT-CAR-INC
                :IND-TIT-TOT-CAR-INC
               ,:TIT-TOT-PRE-SOLO-RSH
                :IND-TIT-TOT-PRE-SOLO-RSH
               ,:TIT-TOT-PROV-ACQ-1AA
                :IND-TIT-TOT-PROV-ACQ-1AA
               ,:TIT-TOT-PROV-ACQ-2AA
                :IND-TIT-TOT-PROV-ACQ-2AA
               ,:TIT-TOT-PROV-RICOR
                :IND-TIT-TOT-PROV-RICOR
               ,:TIT-TOT-PROV-INC
                :IND-TIT-TOT-PROV-INC
               ,:TIT-TOT-PROV-DA-REC
                :IND-TIT-TOT-PROV-DA-REC
               ,:TIT-IMP-AZ
                :IND-TIT-IMP-AZ
               ,:TIT-IMP-ADER
                :IND-TIT-IMP-ADER
               ,:TIT-IMP-TFR
                :IND-TIT-IMP-TFR
               ,:TIT-IMP-VOLO
                :IND-TIT-IMP-VOLO
               ,:TIT-TOT-MANFEE-ANTIC
                :IND-TIT-TOT-MANFEE-ANTIC
               ,:TIT-TOT-MANFEE-RICOR
                :IND-TIT-TOT-MANFEE-RICOR
               ,:TIT-TOT-MANFEE-REC
                :IND-TIT-TOT-MANFEE-REC
               ,:TIT-TP-MEZ-PAG-ADD
                :IND-TIT-TP-MEZ-PAG-ADD
               ,:TIT-ESTR-CNT-CORR-ADD
                :IND-TIT-ESTR-CNT-CORR-ADD
               ,:TIT-DT-VLT-DB
                :IND-TIT-DT-VLT
               ,:TIT-FL-FORZ-DT-VLT
                :IND-TIT-FL-FORZ-DT-VLT
               ,:TIT-DT-CAMBIO-VLT-DB
                :IND-TIT-DT-CAMBIO-VLT
               ,:TIT-TOT-SPE-AGE
                :IND-TIT-TOT-SPE-AGE
               ,:TIT-TOT-CAR-IAS
                :IND-TIT-TOT-CAR-IAS
               ,:TIT-NUM-RAT-ACCORPATE
                :IND-TIT-NUM-RAT-ACCORPATE
               ,:TIT-DS-RIGA
               ,:TIT-DS-OPER-SQL
               ,:TIT-DS-VER
               ,:TIT-DS-TS-INI-CPTZ
               ,:TIT-DS-TS-END-CPTZ
               ,:TIT-DS-UTENTE
               ,:TIT-DS-STATO-ELAB
               ,:TIT-FL-TIT-DA-REINVST
                :IND-TIT-FL-TIT-DA-REINVST
               ,:TIT-DT-RICH-ADD-RID-DB
                :IND-TIT-DT-RICH-ADD-RID
               ,:TIT-TP-ESI-RID
                :IND-TIT-TP-ESI-RID
               ,:TIT-COD-IBAN
                :IND-TIT-COD-IBAN
               ,:TIT-IMP-TRASFE
                :IND-TIT-IMP-TRASFE
               ,:TIT-IMP-TFR-STRC
                :IND-TIT-IMP-TFR-STRC
               ,:TIT-DT-CERT-FISC-DB
                :IND-TIT-DT-CERT-FISC
               ,:TIT-TP-CAUS-STOR
                :IND-TIT-TP-CAUS-STOR
               ,:TIT-TP-CAUS-DISP-STOR
                :IND-TIT-TP-CAUS-DISP-STOR
               ,:TIT-TP-TIT-MIGRAZ
                :IND-TIT-TP-TIT-MIGRAZ
               ,:TIT-TOT-ACQ-EXP
                :IND-TIT-TOT-ACQ-EXP
               ,:TIT-TOT-REMUN-ASS
                :IND-TIT-TOT-REMUN-ASS
               ,:TIT-TOT-COMMIS-INTER
                :IND-TIT-TOT-COMMIS-INTER
               ,:TIT-TP-CAUS-RIMB
                :IND-TIT-TP-CAUS-RIMB
               ,:TIT-TOT-CNBT-ANTIRAC
                :IND-TIT-TOT-CNBT-ANTIRAC
               ,:TIT-FL-INC-AUTOGEN
                :IND-TIT-FL-INC-AUTOGEN
             FROM TIT_CONT
             WHERE     IB_RICH = :TIT-IB-RICH
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.
       B610-RICH-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF TIT-IB-RICH NOT = HIGH-VALUES
               PERFORM B610-SELECT-IBS-RICH
                  THRU B610-RICH-EX
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           IF TIT-IB-RICH NOT = HIGH-VALUES
              EXEC SQL
                   OPEN C-IBS-CPZ-TIT-0
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           IF TIT-IB-RICH NOT = HIGH-VALUES
              EXEC SQL
                   CLOSE C-IBS-CPZ-TIT-0
              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FN-IBS-RICH.
           EXEC SQL
                FETCH C-IBS-CPZ-TIT-0
           INTO
                :TIT-ID-TIT-CONT
               ,:TIT-ID-OGG
               ,:TIT-TP-OGG
               ,:TIT-IB-RICH
                :IND-TIT-IB-RICH
               ,:TIT-ID-MOVI-CRZ
               ,:TIT-ID-MOVI-CHIU
                :IND-TIT-ID-MOVI-CHIU
               ,:TIT-DT-INI-EFF-DB
               ,:TIT-DT-END-EFF-DB
               ,:TIT-COD-COMP-ANIA
               ,:TIT-TP-TIT
               ,:TIT-PROG-TIT
                :IND-TIT-PROG-TIT
               ,:TIT-TP-PRE-TIT
               ,:TIT-TP-STAT-TIT
               ,:TIT-DT-INI-COP-DB
                :IND-TIT-DT-INI-COP
               ,:TIT-DT-END-COP-DB
                :IND-TIT-DT-END-COP
               ,:TIT-IMP-PAG
                :IND-TIT-IMP-PAG
               ,:TIT-FL-SOLL
                :IND-TIT-FL-SOLL
               ,:TIT-FRAZ
                :IND-TIT-FRAZ
               ,:TIT-DT-APPLZ-MORA-DB
                :IND-TIT-DT-APPLZ-MORA
               ,:TIT-FL-MORA
                :IND-TIT-FL-MORA
               ,:TIT-ID-RAPP-RETE
                :IND-TIT-ID-RAPP-RETE
               ,:TIT-ID-RAPP-ANA
                :IND-TIT-ID-RAPP-ANA
               ,:TIT-COD-DVS
                :IND-TIT-COD-DVS
               ,:TIT-DT-EMIS-TIT-DB
                :IND-TIT-DT-EMIS-TIT
               ,:TIT-DT-ESI-TIT-DB
                :IND-TIT-DT-ESI-TIT
               ,:TIT-TOT-PRE-NET
                :IND-TIT-TOT-PRE-NET
               ,:TIT-TOT-INTR-FRAZ
                :IND-TIT-TOT-INTR-FRAZ
               ,:TIT-TOT-INTR-MORA
                :IND-TIT-TOT-INTR-MORA
               ,:TIT-TOT-INTR-PREST
                :IND-TIT-TOT-INTR-PREST
               ,:TIT-TOT-INTR-RETDT
                :IND-TIT-TOT-INTR-RETDT
               ,:TIT-TOT-INTR-RIAT
                :IND-TIT-TOT-INTR-RIAT
               ,:TIT-TOT-DIR
                :IND-TIT-TOT-DIR
               ,:TIT-TOT-SPE-MED
                :IND-TIT-TOT-SPE-MED
               ,:TIT-TOT-TAX
                :IND-TIT-TOT-TAX
               ,:TIT-TOT-SOPR-SAN
                :IND-TIT-TOT-SOPR-SAN
               ,:TIT-TOT-SOPR-TEC
                :IND-TIT-TOT-SOPR-TEC
               ,:TIT-TOT-SOPR-SPO
                :IND-TIT-TOT-SOPR-SPO
               ,:TIT-TOT-SOPR-PROF
                :IND-TIT-TOT-SOPR-PROF
               ,:TIT-TOT-SOPR-ALT
                :IND-TIT-TOT-SOPR-ALT
               ,:TIT-TOT-PRE-TOT
                :IND-TIT-TOT-PRE-TOT
               ,:TIT-TOT-PRE-PP-IAS
                :IND-TIT-TOT-PRE-PP-IAS
               ,:TIT-TOT-CAR-ACQ
                :IND-TIT-TOT-CAR-ACQ
               ,:TIT-TOT-CAR-GEST
                :IND-TIT-TOT-CAR-GEST
               ,:TIT-TOT-CAR-INC
                :IND-TIT-TOT-CAR-INC
               ,:TIT-TOT-PRE-SOLO-RSH
                :IND-TIT-TOT-PRE-SOLO-RSH
               ,:TIT-TOT-PROV-ACQ-1AA
                :IND-TIT-TOT-PROV-ACQ-1AA
               ,:TIT-TOT-PROV-ACQ-2AA
                :IND-TIT-TOT-PROV-ACQ-2AA
               ,:TIT-TOT-PROV-RICOR
                :IND-TIT-TOT-PROV-RICOR
               ,:TIT-TOT-PROV-INC
                :IND-TIT-TOT-PROV-INC
               ,:TIT-TOT-PROV-DA-REC
                :IND-TIT-TOT-PROV-DA-REC
               ,:TIT-IMP-AZ
                :IND-TIT-IMP-AZ
               ,:TIT-IMP-ADER
                :IND-TIT-IMP-ADER
               ,:TIT-IMP-TFR
                :IND-TIT-IMP-TFR
               ,:TIT-IMP-VOLO
                :IND-TIT-IMP-VOLO
               ,:TIT-TOT-MANFEE-ANTIC
                :IND-TIT-TOT-MANFEE-ANTIC
               ,:TIT-TOT-MANFEE-RICOR
                :IND-TIT-TOT-MANFEE-RICOR
               ,:TIT-TOT-MANFEE-REC
                :IND-TIT-TOT-MANFEE-REC
               ,:TIT-TP-MEZ-PAG-ADD
                :IND-TIT-TP-MEZ-PAG-ADD
               ,:TIT-ESTR-CNT-CORR-ADD
                :IND-TIT-ESTR-CNT-CORR-ADD
               ,:TIT-DT-VLT-DB
                :IND-TIT-DT-VLT
               ,:TIT-FL-FORZ-DT-VLT
                :IND-TIT-FL-FORZ-DT-VLT
               ,:TIT-DT-CAMBIO-VLT-DB
                :IND-TIT-DT-CAMBIO-VLT
               ,:TIT-TOT-SPE-AGE
                :IND-TIT-TOT-SPE-AGE
               ,:TIT-TOT-CAR-IAS
                :IND-TIT-TOT-CAR-IAS
               ,:TIT-NUM-RAT-ACCORPATE
                :IND-TIT-NUM-RAT-ACCORPATE
               ,:TIT-DS-RIGA
               ,:TIT-DS-OPER-SQL
               ,:TIT-DS-VER
               ,:TIT-DS-TS-INI-CPTZ
               ,:TIT-DS-TS-END-CPTZ
               ,:TIT-DS-UTENTE
               ,:TIT-DS-STATO-ELAB
               ,:TIT-FL-TIT-DA-REINVST
                :IND-TIT-FL-TIT-DA-REINVST
               ,:TIT-DT-RICH-ADD-RID-DB
                :IND-TIT-DT-RICH-ADD-RID
               ,:TIT-TP-ESI-RID
                :IND-TIT-TP-ESI-RID
               ,:TIT-COD-IBAN
                :IND-TIT-COD-IBAN
               ,:TIT-IMP-TRASFE
                :IND-TIT-IMP-TRASFE
               ,:TIT-IMP-TFR-STRC
                :IND-TIT-IMP-TFR-STRC
               ,:TIT-DT-CERT-FISC-DB
                :IND-TIT-DT-CERT-FISC
               ,:TIT-TP-CAUS-STOR
                :IND-TIT-TP-CAUS-STOR
               ,:TIT-TP-CAUS-DISP-STOR
                :IND-TIT-TP-CAUS-DISP-STOR
               ,:TIT-TP-TIT-MIGRAZ
                :IND-TIT-TP-TIT-MIGRAZ
               ,:TIT-TOT-ACQ-EXP
                :IND-TIT-TOT-ACQ-EXP
               ,:TIT-TOT-REMUN-ASS
                :IND-TIT-TOT-REMUN-ASS
               ,:TIT-TOT-COMMIS-INTER
                :IND-TIT-TOT-COMMIS-INTER
               ,:TIT-TP-CAUS-RIMB
                :IND-TIT-TP-CAUS-RIMB
               ,:TIT-TOT-CNBT-ANTIRAC
                :IND-TIT-TOT-CNBT-ANTIRAC
               ,:TIT-FL-INC-AUTOGEN
                :IND-TIT-FL-INC-AUTOGEN
           END-EXEC.
       B690-RICH-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           IF TIT-IB-RICH NOT = HIGH-VALUES
               PERFORM B690-FN-IBS-RICH
                  THRU B690-RICH-EX
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B670-CLOSE-CURSOR-IBS-CPZ     THRU B670-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDO-CPZ-TIT CURSOR FOR
              SELECT
                     ID_TIT_CONT
                    ,ID_OGG
                    ,TP_OGG
                    ,IB_RICH
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_TIT
                    ,PROG_TIT
                    ,TP_PRE_TIT
                    ,TP_STAT_TIT
                    ,DT_INI_COP
                    ,DT_END_COP
                    ,IMP_PAG
                    ,FL_SOLL
                    ,FRAZ
                    ,DT_APPLZ_MORA
                    ,FL_MORA
                    ,ID_RAPP_RETE
                    ,ID_RAPP_ANA
                    ,COD_DVS
                    ,DT_EMIS_TIT
                    ,DT_ESI_TIT
                    ,TOT_PRE_NET
                    ,TOT_INTR_FRAZ
                    ,TOT_INTR_MORA
                    ,TOT_INTR_PREST
                    ,TOT_INTR_RETDT
                    ,TOT_INTR_RIAT
                    ,TOT_DIR
                    ,TOT_SPE_MED
                    ,TOT_TAX
                    ,TOT_SOPR_SAN
                    ,TOT_SOPR_TEC
                    ,TOT_SOPR_SPO
                    ,TOT_SOPR_PROF
                    ,TOT_SOPR_ALT
                    ,TOT_PRE_TOT
                    ,TOT_PRE_PP_IAS
                    ,TOT_CAR_ACQ
                    ,TOT_CAR_GEST
                    ,TOT_CAR_INC
                    ,TOT_PRE_SOLO_RSH
                    ,TOT_PROV_ACQ_1AA
                    ,TOT_PROV_ACQ_2AA
                    ,TOT_PROV_RICOR
                    ,TOT_PROV_INC
                    ,TOT_PROV_DA_REC
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,TOT_MANFEE_ANTIC
                    ,TOT_MANFEE_RICOR
                    ,TOT_MANFEE_REC
                    ,TP_MEZ_PAG_ADD
                    ,ESTR_CNT_CORR_ADD
                    ,DT_VLT
                    ,FL_FORZ_DT_VLT
                    ,DT_CAMBIO_VLT
                    ,TOT_SPE_AGE
                    ,TOT_CAR_IAS
                    ,NUM_RAT_ACCORPATE
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,FL_TIT_DA_REINVST
                    ,DT_RICH_ADD_RID
                    ,TP_ESI_RID
                    ,COD_IBAN
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,DT_CERT_FISC
                    ,TP_CAUS_STOR
                    ,TP_CAUS_DISP_STOR
                    ,TP_TIT_MIGRAZ
                    ,TOT_ACQ_EXP
                    ,TOT_REMUN_ASS
                    ,TOT_COMMIS_INTER
                    ,TP_CAUS_RIMB
                    ,TOT_CNBT_ANTIRAC
                    ,FL_INC_AUTOGEN
              FROM TIT_CONT
              WHERE     ID_OGG = :TIT-ID-OGG
           AND TP_OGG = :TIT-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_TIT_CONT ASC

           END-EXEC.

       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TIT_CONT
                ,ID_OGG
                ,TP_OGG
                ,IB_RICH
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_TIT
                ,PROG_TIT
                ,TP_PRE_TIT
                ,TP_STAT_TIT
                ,DT_INI_COP
                ,DT_END_COP
                ,IMP_PAG
                ,FL_SOLL
                ,FRAZ
                ,DT_APPLZ_MORA
                ,FL_MORA
                ,ID_RAPP_RETE
                ,ID_RAPP_ANA
                ,COD_DVS
                ,DT_EMIS_TIT
                ,DT_ESI_TIT
                ,TOT_PRE_NET
                ,TOT_INTR_FRAZ
                ,TOT_INTR_MORA
                ,TOT_INTR_PREST
                ,TOT_INTR_RETDT
                ,TOT_INTR_RIAT
                ,TOT_DIR
                ,TOT_SPE_MED
                ,TOT_TAX
                ,TOT_SOPR_SAN
                ,TOT_SOPR_TEC
                ,TOT_SOPR_SPO
                ,TOT_SOPR_PROF
                ,TOT_SOPR_ALT
                ,TOT_PRE_TOT
                ,TOT_PRE_PP_IAS
                ,TOT_CAR_ACQ
                ,TOT_CAR_GEST
                ,TOT_CAR_INC
                ,TOT_PRE_SOLO_RSH
                ,TOT_PROV_ACQ_1AA
                ,TOT_PROV_ACQ_2AA
                ,TOT_PROV_RICOR
                ,TOT_PROV_INC
                ,TOT_PROV_DA_REC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,TOT_MANFEE_ANTIC
                ,TOT_MANFEE_RICOR
                ,TOT_MANFEE_REC
                ,TP_MEZ_PAG_ADD
                ,ESTR_CNT_CORR_ADD
                ,DT_VLT
                ,FL_FORZ_DT_VLT
                ,DT_CAMBIO_VLT
                ,TOT_SPE_AGE
                ,TOT_CAR_IAS
                ,NUM_RAT_ACCORPATE
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,FL_TIT_DA_REINVST
                ,DT_RICH_ADD_RID
                ,TP_ESI_RID
                ,COD_IBAN
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,DT_CERT_FISC
                ,TP_CAUS_STOR
                ,TP_CAUS_DISP_STOR
                ,TP_TIT_MIGRAZ
                ,TOT_ACQ_EXP
                ,TOT_REMUN_ASS
                ,TOT_COMMIS_INTER
                ,TP_CAUS_RIMB
                ,TOT_CNBT_ANTIRAC
                ,FL_INC_AUTOGEN
             INTO
                :TIT-ID-TIT-CONT
               ,:TIT-ID-OGG
               ,:TIT-TP-OGG
               ,:TIT-IB-RICH
                :IND-TIT-IB-RICH
               ,:TIT-ID-MOVI-CRZ
               ,:TIT-ID-MOVI-CHIU
                :IND-TIT-ID-MOVI-CHIU
               ,:TIT-DT-INI-EFF-DB
               ,:TIT-DT-END-EFF-DB
               ,:TIT-COD-COMP-ANIA
               ,:TIT-TP-TIT
               ,:TIT-PROG-TIT
                :IND-TIT-PROG-TIT
               ,:TIT-TP-PRE-TIT
               ,:TIT-TP-STAT-TIT
               ,:TIT-DT-INI-COP-DB
                :IND-TIT-DT-INI-COP
               ,:TIT-DT-END-COP-DB
                :IND-TIT-DT-END-COP
               ,:TIT-IMP-PAG
                :IND-TIT-IMP-PAG
               ,:TIT-FL-SOLL
                :IND-TIT-FL-SOLL
               ,:TIT-FRAZ
                :IND-TIT-FRAZ
               ,:TIT-DT-APPLZ-MORA-DB
                :IND-TIT-DT-APPLZ-MORA
               ,:TIT-FL-MORA
                :IND-TIT-FL-MORA
               ,:TIT-ID-RAPP-RETE
                :IND-TIT-ID-RAPP-RETE
               ,:TIT-ID-RAPP-ANA
                :IND-TIT-ID-RAPP-ANA
               ,:TIT-COD-DVS
                :IND-TIT-COD-DVS
               ,:TIT-DT-EMIS-TIT-DB
                :IND-TIT-DT-EMIS-TIT
               ,:TIT-DT-ESI-TIT-DB
                :IND-TIT-DT-ESI-TIT
               ,:TIT-TOT-PRE-NET
                :IND-TIT-TOT-PRE-NET
               ,:TIT-TOT-INTR-FRAZ
                :IND-TIT-TOT-INTR-FRAZ
               ,:TIT-TOT-INTR-MORA
                :IND-TIT-TOT-INTR-MORA
               ,:TIT-TOT-INTR-PREST
                :IND-TIT-TOT-INTR-PREST
               ,:TIT-TOT-INTR-RETDT
                :IND-TIT-TOT-INTR-RETDT
               ,:TIT-TOT-INTR-RIAT
                :IND-TIT-TOT-INTR-RIAT
               ,:TIT-TOT-DIR
                :IND-TIT-TOT-DIR
               ,:TIT-TOT-SPE-MED
                :IND-TIT-TOT-SPE-MED
               ,:TIT-TOT-TAX
                :IND-TIT-TOT-TAX
               ,:TIT-TOT-SOPR-SAN
                :IND-TIT-TOT-SOPR-SAN
               ,:TIT-TOT-SOPR-TEC
                :IND-TIT-TOT-SOPR-TEC
               ,:TIT-TOT-SOPR-SPO
                :IND-TIT-TOT-SOPR-SPO
               ,:TIT-TOT-SOPR-PROF
                :IND-TIT-TOT-SOPR-PROF
               ,:TIT-TOT-SOPR-ALT
                :IND-TIT-TOT-SOPR-ALT
               ,:TIT-TOT-PRE-TOT
                :IND-TIT-TOT-PRE-TOT
               ,:TIT-TOT-PRE-PP-IAS
                :IND-TIT-TOT-PRE-PP-IAS
               ,:TIT-TOT-CAR-ACQ
                :IND-TIT-TOT-CAR-ACQ
               ,:TIT-TOT-CAR-GEST
                :IND-TIT-TOT-CAR-GEST
               ,:TIT-TOT-CAR-INC
                :IND-TIT-TOT-CAR-INC
               ,:TIT-TOT-PRE-SOLO-RSH
                :IND-TIT-TOT-PRE-SOLO-RSH
               ,:TIT-TOT-PROV-ACQ-1AA
                :IND-TIT-TOT-PROV-ACQ-1AA
               ,:TIT-TOT-PROV-ACQ-2AA
                :IND-TIT-TOT-PROV-ACQ-2AA
               ,:TIT-TOT-PROV-RICOR
                :IND-TIT-TOT-PROV-RICOR
               ,:TIT-TOT-PROV-INC
                :IND-TIT-TOT-PROV-INC
               ,:TIT-TOT-PROV-DA-REC
                :IND-TIT-TOT-PROV-DA-REC
               ,:TIT-IMP-AZ
                :IND-TIT-IMP-AZ
               ,:TIT-IMP-ADER
                :IND-TIT-IMP-ADER
               ,:TIT-IMP-TFR
                :IND-TIT-IMP-TFR
               ,:TIT-IMP-VOLO
                :IND-TIT-IMP-VOLO
               ,:TIT-TOT-MANFEE-ANTIC
                :IND-TIT-TOT-MANFEE-ANTIC
               ,:TIT-TOT-MANFEE-RICOR
                :IND-TIT-TOT-MANFEE-RICOR
               ,:TIT-TOT-MANFEE-REC
                :IND-TIT-TOT-MANFEE-REC
               ,:TIT-TP-MEZ-PAG-ADD
                :IND-TIT-TP-MEZ-PAG-ADD
               ,:TIT-ESTR-CNT-CORR-ADD
                :IND-TIT-ESTR-CNT-CORR-ADD
               ,:TIT-DT-VLT-DB
                :IND-TIT-DT-VLT
               ,:TIT-FL-FORZ-DT-VLT
                :IND-TIT-FL-FORZ-DT-VLT
               ,:TIT-DT-CAMBIO-VLT-DB
                :IND-TIT-DT-CAMBIO-VLT
               ,:TIT-TOT-SPE-AGE
                :IND-TIT-TOT-SPE-AGE
               ,:TIT-TOT-CAR-IAS
                :IND-TIT-TOT-CAR-IAS
               ,:TIT-NUM-RAT-ACCORPATE
                :IND-TIT-NUM-RAT-ACCORPATE
               ,:TIT-DS-RIGA
               ,:TIT-DS-OPER-SQL
               ,:TIT-DS-VER
               ,:TIT-DS-TS-INI-CPTZ
               ,:TIT-DS-TS-END-CPTZ
               ,:TIT-DS-UTENTE
               ,:TIT-DS-STATO-ELAB
               ,:TIT-FL-TIT-DA-REINVST
                :IND-TIT-FL-TIT-DA-REINVST
               ,:TIT-DT-RICH-ADD-RID-DB
                :IND-TIT-DT-RICH-ADD-RID
               ,:TIT-TP-ESI-RID
                :IND-TIT-TP-ESI-RID
               ,:TIT-COD-IBAN
                :IND-TIT-COD-IBAN
               ,:TIT-IMP-TRASFE
                :IND-TIT-IMP-TRASFE
               ,:TIT-IMP-TFR-STRC
                :IND-TIT-IMP-TFR-STRC
               ,:TIT-DT-CERT-FISC-DB
                :IND-TIT-DT-CERT-FISC
               ,:TIT-TP-CAUS-STOR
                :IND-TIT-TP-CAUS-STOR
               ,:TIT-TP-CAUS-DISP-STOR
                :IND-TIT-TP-CAUS-DISP-STOR
               ,:TIT-TP-TIT-MIGRAZ
                :IND-TIT-TP-TIT-MIGRAZ
               ,:TIT-TOT-ACQ-EXP
                :IND-TIT-TOT-ACQ-EXP
               ,:TIT-TOT-REMUN-ASS
                :IND-TIT-TOT-REMUN-ASS
               ,:TIT-TOT-COMMIS-INTER
                :IND-TIT-TOT-COMMIS-INTER
               ,:TIT-TP-CAUS-RIMB
                :IND-TIT-TP-CAUS-RIMB
               ,:TIT-TOT-CNBT-ANTIRAC
                :IND-TIT-TOT-CNBT-ANTIRAC
               ,:TIT-FL-INC-AUTOGEN
                :IND-TIT-FL-INC-AUTOGEN
             FROM TIT_CONT
             WHERE     ID_OGG = :TIT-ID-OGG
                    AND TP_OGG = :TIT-TP-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           EXEC SQL
                OPEN C-IDO-CPZ-TIT
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           EXEC SQL
                CLOSE C-IDO-CPZ-TIT
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           EXEC SQL
                FETCH C-IDO-CPZ-TIT
           INTO
                :TIT-ID-TIT-CONT
               ,:TIT-ID-OGG
               ,:TIT-TP-OGG
               ,:TIT-IB-RICH
                :IND-TIT-IB-RICH
               ,:TIT-ID-MOVI-CRZ
               ,:TIT-ID-MOVI-CHIU
                :IND-TIT-ID-MOVI-CHIU
               ,:TIT-DT-INI-EFF-DB
               ,:TIT-DT-END-EFF-DB
               ,:TIT-COD-COMP-ANIA
               ,:TIT-TP-TIT
               ,:TIT-PROG-TIT
                :IND-TIT-PROG-TIT
               ,:TIT-TP-PRE-TIT
               ,:TIT-TP-STAT-TIT
               ,:TIT-DT-INI-COP-DB
                :IND-TIT-DT-INI-COP
               ,:TIT-DT-END-COP-DB
                :IND-TIT-DT-END-COP
               ,:TIT-IMP-PAG
                :IND-TIT-IMP-PAG
               ,:TIT-FL-SOLL
                :IND-TIT-FL-SOLL
               ,:TIT-FRAZ
                :IND-TIT-FRAZ
               ,:TIT-DT-APPLZ-MORA-DB
                :IND-TIT-DT-APPLZ-MORA
               ,:TIT-FL-MORA
                :IND-TIT-FL-MORA
               ,:TIT-ID-RAPP-RETE
                :IND-TIT-ID-RAPP-RETE
               ,:TIT-ID-RAPP-ANA
                :IND-TIT-ID-RAPP-ANA
               ,:TIT-COD-DVS
                :IND-TIT-COD-DVS
               ,:TIT-DT-EMIS-TIT-DB
                :IND-TIT-DT-EMIS-TIT
               ,:TIT-DT-ESI-TIT-DB
                :IND-TIT-DT-ESI-TIT
               ,:TIT-TOT-PRE-NET
                :IND-TIT-TOT-PRE-NET
               ,:TIT-TOT-INTR-FRAZ
                :IND-TIT-TOT-INTR-FRAZ
               ,:TIT-TOT-INTR-MORA
                :IND-TIT-TOT-INTR-MORA
               ,:TIT-TOT-INTR-PREST
                :IND-TIT-TOT-INTR-PREST
               ,:TIT-TOT-INTR-RETDT
                :IND-TIT-TOT-INTR-RETDT
               ,:TIT-TOT-INTR-RIAT
                :IND-TIT-TOT-INTR-RIAT
               ,:TIT-TOT-DIR
                :IND-TIT-TOT-DIR
               ,:TIT-TOT-SPE-MED
                :IND-TIT-TOT-SPE-MED
               ,:TIT-TOT-TAX
                :IND-TIT-TOT-TAX
               ,:TIT-TOT-SOPR-SAN
                :IND-TIT-TOT-SOPR-SAN
               ,:TIT-TOT-SOPR-TEC
                :IND-TIT-TOT-SOPR-TEC
               ,:TIT-TOT-SOPR-SPO
                :IND-TIT-TOT-SOPR-SPO
               ,:TIT-TOT-SOPR-PROF
                :IND-TIT-TOT-SOPR-PROF
               ,:TIT-TOT-SOPR-ALT
                :IND-TIT-TOT-SOPR-ALT
               ,:TIT-TOT-PRE-TOT
                :IND-TIT-TOT-PRE-TOT
               ,:TIT-TOT-PRE-PP-IAS
                :IND-TIT-TOT-PRE-PP-IAS
               ,:TIT-TOT-CAR-ACQ
                :IND-TIT-TOT-CAR-ACQ
               ,:TIT-TOT-CAR-GEST
                :IND-TIT-TOT-CAR-GEST
               ,:TIT-TOT-CAR-INC
                :IND-TIT-TOT-CAR-INC
               ,:TIT-TOT-PRE-SOLO-RSH
                :IND-TIT-TOT-PRE-SOLO-RSH
               ,:TIT-TOT-PROV-ACQ-1AA
                :IND-TIT-TOT-PROV-ACQ-1AA
               ,:TIT-TOT-PROV-ACQ-2AA
                :IND-TIT-TOT-PROV-ACQ-2AA
               ,:TIT-TOT-PROV-RICOR
                :IND-TIT-TOT-PROV-RICOR
               ,:TIT-TOT-PROV-INC
                :IND-TIT-TOT-PROV-INC
               ,:TIT-TOT-PROV-DA-REC
                :IND-TIT-TOT-PROV-DA-REC
               ,:TIT-IMP-AZ
                :IND-TIT-IMP-AZ
               ,:TIT-IMP-ADER
                :IND-TIT-IMP-ADER
               ,:TIT-IMP-TFR
                :IND-TIT-IMP-TFR
               ,:TIT-IMP-VOLO
                :IND-TIT-IMP-VOLO
               ,:TIT-TOT-MANFEE-ANTIC
                :IND-TIT-TOT-MANFEE-ANTIC
               ,:TIT-TOT-MANFEE-RICOR
                :IND-TIT-TOT-MANFEE-RICOR
               ,:TIT-TOT-MANFEE-REC
                :IND-TIT-TOT-MANFEE-REC
               ,:TIT-TP-MEZ-PAG-ADD
                :IND-TIT-TP-MEZ-PAG-ADD
               ,:TIT-ESTR-CNT-CORR-ADD
                :IND-TIT-ESTR-CNT-CORR-ADD
               ,:TIT-DT-VLT-DB
                :IND-TIT-DT-VLT
               ,:TIT-FL-FORZ-DT-VLT
                :IND-TIT-FL-FORZ-DT-VLT
               ,:TIT-DT-CAMBIO-VLT-DB
                :IND-TIT-DT-CAMBIO-VLT
               ,:TIT-TOT-SPE-AGE
                :IND-TIT-TOT-SPE-AGE
               ,:TIT-TOT-CAR-IAS
                :IND-TIT-TOT-CAR-IAS
               ,:TIT-NUM-RAT-ACCORPATE
                :IND-TIT-NUM-RAT-ACCORPATE
               ,:TIT-DS-RIGA
               ,:TIT-DS-OPER-SQL
               ,:TIT-DS-VER
               ,:TIT-DS-TS-INI-CPTZ
               ,:TIT-DS-TS-END-CPTZ
               ,:TIT-DS-UTENTE
               ,:TIT-DS-STATO-ELAB
               ,:TIT-FL-TIT-DA-REINVST
                :IND-TIT-FL-TIT-DA-REINVST
               ,:TIT-DT-RICH-ADD-RID-DB
                :IND-TIT-DT-RICH-ADD-RID
               ,:TIT-TP-ESI-RID
                :IND-TIT-TP-ESI-RID
               ,:TIT-COD-IBAN
                :IND-TIT-COD-IBAN
               ,:TIT-IMP-TRASFE
                :IND-TIT-IMP-TRASFE
               ,:TIT-IMP-TFR-STRC
                :IND-TIT-IMP-TFR-STRC
               ,:TIT-DT-CERT-FISC-DB
                :IND-TIT-DT-CERT-FISC
               ,:TIT-TP-CAUS-STOR
                :IND-TIT-TP-CAUS-STOR
               ,:TIT-TP-CAUS-DISP-STOR
                :IND-TIT-TP-CAUS-DISP-STOR
               ,:TIT-TP-TIT-MIGRAZ
                :IND-TIT-TP-TIT-MIGRAZ
               ,:TIT-TOT-ACQ-EXP
                :IND-TIT-TOT-ACQ-EXP
               ,:TIT-TOT-REMUN-ASS
                :IND-TIT-TOT-REMUN-ASS
               ,:TIT-TOT-COMMIS-INTER
                :IND-TIT-TOT-COMMIS-INTER
               ,:TIT-TP-CAUS-RIMB
                :IND-TIT-TP-CAUS-RIMB
               ,:TIT-TOT-CNBT-ANTIRAC
                :IND-TIT-TOT-CNBT-ANTIRAC
               ,:TIT-FL-INC-AUTOGEN
                :IND-TIT-FL-INC-AUTOGEN
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B770-CLOSE-CURSOR-IDO-CPZ     THRU B770-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-TIT-IB-RICH = -1
              MOVE HIGH-VALUES TO TIT-IB-RICH-NULL
           END-IF
           IF IND-TIT-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO TIT-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-TIT-PROG-TIT = -1
              MOVE HIGH-VALUES TO TIT-PROG-TIT-NULL
           END-IF
           IF IND-TIT-DT-INI-COP = -1
              MOVE HIGH-VALUES TO TIT-DT-INI-COP-NULL
           END-IF
           IF IND-TIT-DT-END-COP = -1
              MOVE HIGH-VALUES TO TIT-DT-END-COP-NULL
           END-IF
           IF IND-TIT-IMP-PAG = -1
              MOVE HIGH-VALUES TO TIT-IMP-PAG-NULL
           END-IF
           IF IND-TIT-FL-SOLL = -1
              MOVE HIGH-VALUES TO TIT-FL-SOLL-NULL
           END-IF
           IF IND-TIT-FRAZ = -1
              MOVE HIGH-VALUES TO TIT-FRAZ-NULL
           END-IF
           IF IND-TIT-DT-APPLZ-MORA = -1
              MOVE HIGH-VALUES TO TIT-DT-APPLZ-MORA-NULL
           END-IF
           IF IND-TIT-FL-MORA = -1
              MOVE HIGH-VALUES TO TIT-FL-MORA-NULL
           END-IF
           IF IND-TIT-ID-RAPP-RETE = -1
              MOVE HIGH-VALUES TO TIT-ID-RAPP-RETE-NULL
           END-IF
           IF IND-TIT-ID-RAPP-ANA = -1
              MOVE HIGH-VALUES TO TIT-ID-RAPP-ANA-NULL
           END-IF
           IF IND-TIT-COD-DVS = -1
              MOVE HIGH-VALUES TO TIT-COD-DVS-NULL
           END-IF
           IF IND-TIT-DT-EMIS-TIT = -1
              MOVE HIGH-VALUES TO TIT-DT-EMIS-TIT-NULL
           END-IF
           IF IND-TIT-DT-ESI-TIT = -1
              MOVE HIGH-VALUES TO TIT-DT-ESI-TIT-NULL
           END-IF
           IF IND-TIT-TOT-PRE-NET = -1
              MOVE HIGH-VALUES TO TIT-TOT-PRE-NET-NULL
           END-IF
           IF IND-TIT-TOT-INTR-FRAZ = -1
              MOVE HIGH-VALUES TO TIT-TOT-INTR-FRAZ-NULL
           END-IF
           IF IND-TIT-TOT-INTR-MORA = -1
              MOVE HIGH-VALUES TO TIT-TOT-INTR-MORA-NULL
           END-IF
           IF IND-TIT-TOT-INTR-PREST = -1
              MOVE HIGH-VALUES TO TIT-TOT-INTR-PREST-NULL
           END-IF
           IF IND-TIT-TOT-INTR-RETDT = -1
              MOVE HIGH-VALUES TO TIT-TOT-INTR-RETDT-NULL
           END-IF
           IF IND-TIT-TOT-INTR-RIAT = -1
              MOVE HIGH-VALUES TO TIT-TOT-INTR-RIAT-NULL
           END-IF
           IF IND-TIT-TOT-DIR = -1
              MOVE HIGH-VALUES TO TIT-TOT-DIR-NULL
           END-IF
           IF IND-TIT-TOT-SPE-MED = -1
              MOVE HIGH-VALUES TO TIT-TOT-SPE-MED-NULL
           END-IF
           IF IND-TIT-TOT-TAX = -1
              MOVE HIGH-VALUES TO TIT-TOT-TAX-NULL
           END-IF
           IF IND-TIT-TOT-SOPR-SAN = -1
              MOVE HIGH-VALUES TO TIT-TOT-SOPR-SAN-NULL
           END-IF
           IF IND-TIT-TOT-SOPR-TEC = -1
              MOVE HIGH-VALUES TO TIT-TOT-SOPR-TEC-NULL
           END-IF
           IF IND-TIT-TOT-SOPR-SPO = -1
              MOVE HIGH-VALUES TO TIT-TOT-SOPR-SPO-NULL
           END-IF
           IF IND-TIT-TOT-SOPR-PROF = -1
              MOVE HIGH-VALUES TO TIT-TOT-SOPR-PROF-NULL
           END-IF
           IF IND-TIT-TOT-SOPR-ALT = -1
              MOVE HIGH-VALUES TO TIT-TOT-SOPR-ALT-NULL
           END-IF
           IF IND-TIT-TOT-PRE-TOT = -1
              MOVE HIGH-VALUES TO TIT-TOT-PRE-TOT-NULL
           END-IF
           IF IND-TIT-TOT-PRE-PP-IAS = -1
              MOVE HIGH-VALUES TO TIT-TOT-PRE-PP-IAS-NULL
           END-IF
           IF IND-TIT-TOT-CAR-ACQ = -1
              MOVE HIGH-VALUES TO TIT-TOT-CAR-ACQ-NULL
           END-IF
           IF IND-TIT-TOT-CAR-GEST = -1
              MOVE HIGH-VALUES TO TIT-TOT-CAR-GEST-NULL
           END-IF
           IF IND-TIT-TOT-CAR-INC = -1
              MOVE HIGH-VALUES TO TIT-TOT-CAR-INC-NULL
           END-IF
           IF IND-TIT-TOT-PRE-SOLO-RSH = -1
              MOVE HIGH-VALUES TO TIT-TOT-PRE-SOLO-RSH-NULL
           END-IF
           IF IND-TIT-TOT-PROV-ACQ-1AA = -1
              MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-1AA-NULL
           END-IF
           IF IND-TIT-TOT-PROV-ACQ-2AA = -1
              MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-2AA-NULL
           END-IF
           IF IND-TIT-TOT-PROV-RICOR = -1
              MOVE HIGH-VALUES TO TIT-TOT-PROV-RICOR-NULL
           END-IF
           IF IND-TIT-TOT-PROV-INC = -1
              MOVE HIGH-VALUES TO TIT-TOT-PROV-INC-NULL
           END-IF
           IF IND-TIT-TOT-PROV-DA-REC = -1
              MOVE HIGH-VALUES TO TIT-TOT-PROV-DA-REC-NULL
           END-IF
           IF IND-TIT-IMP-AZ = -1
              MOVE HIGH-VALUES TO TIT-IMP-AZ-NULL
           END-IF
           IF IND-TIT-IMP-ADER = -1
              MOVE HIGH-VALUES TO TIT-IMP-ADER-NULL
           END-IF
           IF IND-TIT-IMP-TFR = -1
              MOVE HIGH-VALUES TO TIT-IMP-TFR-NULL
           END-IF
           IF IND-TIT-IMP-VOLO = -1
              MOVE HIGH-VALUES TO TIT-IMP-VOLO-NULL
           END-IF
           IF IND-TIT-TOT-MANFEE-ANTIC = -1
              MOVE HIGH-VALUES TO TIT-TOT-MANFEE-ANTIC-NULL
           END-IF
           IF IND-TIT-TOT-MANFEE-RICOR = -1
              MOVE HIGH-VALUES TO TIT-TOT-MANFEE-RICOR-NULL
           END-IF
           IF IND-TIT-TOT-MANFEE-REC = -1
              MOVE HIGH-VALUES TO TIT-TOT-MANFEE-REC-NULL
           END-IF
           IF IND-TIT-TP-MEZ-PAG-ADD = -1
              MOVE HIGH-VALUES TO TIT-TP-MEZ-PAG-ADD-NULL
           END-IF
           IF IND-TIT-ESTR-CNT-CORR-ADD = -1
              MOVE HIGH-VALUES TO TIT-ESTR-CNT-CORR-ADD-NULL
           END-IF
           IF IND-TIT-DT-VLT = -1
              MOVE HIGH-VALUES TO TIT-DT-VLT-NULL
           END-IF
           IF IND-TIT-FL-FORZ-DT-VLT = -1
              MOVE HIGH-VALUES TO TIT-FL-FORZ-DT-VLT-NULL
           END-IF
           IF IND-TIT-DT-CAMBIO-VLT = -1
              MOVE HIGH-VALUES TO TIT-DT-CAMBIO-VLT-NULL
           END-IF
           IF IND-TIT-TOT-SPE-AGE = -1
              MOVE HIGH-VALUES TO TIT-TOT-SPE-AGE-NULL
           END-IF
           IF IND-TIT-TOT-CAR-IAS = -1
              MOVE HIGH-VALUES TO TIT-TOT-CAR-IAS-NULL
           END-IF
           IF IND-TIT-NUM-RAT-ACCORPATE = -1
              MOVE HIGH-VALUES TO TIT-NUM-RAT-ACCORPATE-NULL
           END-IF
           IF IND-TIT-FL-TIT-DA-REINVST = -1
              MOVE HIGH-VALUES TO TIT-FL-TIT-DA-REINVST-NULL
           END-IF
           IF IND-TIT-DT-RICH-ADD-RID = -1
              MOVE HIGH-VALUES TO TIT-DT-RICH-ADD-RID-NULL
           END-IF
           IF IND-TIT-TP-ESI-RID = -1
              MOVE HIGH-VALUES TO TIT-TP-ESI-RID-NULL
           END-IF
           IF IND-TIT-COD-IBAN = -1
              MOVE HIGH-VALUES TO TIT-COD-IBAN-NULL
           END-IF
           IF IND-TIT-IMP-TRASFE = -1
              MOVE HIGH-VALUES TO TIT-IMP-TRASFE-NULL
           END-IF
           IF IND-TIT-IMP-TFR-STRC = -1
              MOVE HIGH-VALUES TO TIT-IMP-TFR-STRC-NULL
           END-IF
           IF IND-TIT-DT-CERT-FISC = -1
              MOVE HIGH-VALUES TO TIT-DT-CERT-FISC-NULL
           END-IF
           IF IND-TIT-TP-CAUS-STOR = -1
              MOVE HIGH-VALUES TO TIT-TP-CAUS-STOR-NULL
           END-IF
           IF IND-TIT-TP-CAUS-DISP-STOR = -1
              MOVE HIGH-VALUES TO TIT-TP-CAUS-DISP-STOR-NULL
           END-IF
           IF IND-TIT-TP-TIT-MIGRAZ = -1
              MOVE HIGH-VALUES TO TIT-TP-TIT-MIGRAZ-NULL
           END-IF
           IF IND-TIT-TOT-ACQ-EXP = -1
              MOVE HIGH-VALUES TO TIT-TOT-ACQ-EXP-NULL
           END-IF
           IF IND-TIT-TOT-REMUN-ASS = -1
              MOVE HIGH-VALUES TO TIT-TOT-REMUN-ASS-NULL
           END-IF
           IF IND-TIT-TOT-COMMIS-INTER = -1
              MOVE HIGH-VALUES TO TIT-TOT-COMMIS-INTER-NULL
           END-IF
           IF IND-TIT-TP-CAUS-RIMB = -1
              MOVE HIGH-VALUES TO TIT-TP-CAUS-RIMB-NULL
           END-IF
           IF IND-TIT-TOT-CNBT-ANTIRAC = -1
              MOVE HIGH-VALUES TO TIT-TOT-CNBT-ANTIRAC-NULL
           END-IF
           IF IND-TIT-FL-INC-AUTOGEN = -1
              MOVE HIGH-VALUES TO TIT-FL-INC-AUTOGEN-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO TIT-DS-OPER-SQL
           MOVE 0                   TO TIT-DS-VER
           MOVE IDSV0003-USER-NAME TO TIT-DS-UTENTE
           MOVE '1'                   TO TIT-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO TIT-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO TIT-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF TIT-IB-RICH-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-IB-RICH
           ELSE
              MOVE 0 TO IND-TIT-IB-RICH
           END-IF
           IF TIT-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-TIT-ID-MOVI-CHIU
           END-IF
           IF TIT-PROG-TIT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-PROG-TIT
           ELSE
              MOVE 0 TO IND-TIT-PROG-TIT
           END-IF
           IF TIT-DT-INI-COP-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-DT-INI-COP
           ELSE
              MOVE 0 TO IND-TIT-DT-INI-COP
           END-IF
           IF TIT-DT-END-COP-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-DT-END-COP
           ELSE
              MOVE 0 TO IND-TIT-DT-END-COP
           END-IF
           IF TIT-IMP-PAG-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-IMP-PAG
           ELSE
              MOVE 0 TO IND-TIT-IMP-PAG
           END-IF
           IF TIT-FL-SOLL-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-FL-SOLL
           ELSE
              MOVE 0 TO IND-TIT-FL-SOLL
           END-IF
           IF TIT-FRAZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-FRAZ
           ELSE
              MOVE 0 TO IND-TIT-FRAZ
           END-IF
           IF TIT-DT-APPLZ-MORA-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-DT-APPLZ-MORA
           ELSE
              MOVE 0 TO IND-TIT-DT-APPLZ-MORA
           END-IF
           IF TIT-FL-MORA-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-FL-MORA
           ELSE
              MOVE 0 TO IND-TIT-FL-MORA
           END-IF
           IF TIT-ID-RAPP-RETE-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-ID-RAPP-RETE
           ELSE
              MOVE 0 TO IND-TIT-ID-RAPP-RETE
           END-IF
           IF TIT-ID-RAPP-ANA-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-ID-RAPP-ANA
           ELSE
              MOVE 0 TO IND-TIT-ID-RAPP-ANA
           END-IF
           IF TIT-COD-DVS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-COD-DVS
           ELSE
              MOVE 0 TO IND-TIT-COD-DVS
           END-IF
           IF TIT-DT-EMIS-TIT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-DT-EMIS-TIT
           ELSE
              MOVE 0 TO IND-TIT-DT-EMIS-TIT
           END-IF
           IF TIT-DT-ESI-TIT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-DT-ESI-TIT
           ELSE
              MOVE 0 TO IND-TIT-DT-ESI-TIT
           END-IF
           IF TIT-TOT-PRE-NET-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-PRE-NET
           ELSE
              MOVE 0 TO IND-TIT-TOT-PRE-NET
           END-IF
           IF TIT-TOT-INTR-FRAZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-INTR-FRAZ
           ELSE
              MOVE 0 TO IND-TIT-TOT-INTR-FRAZ
           END-IF
           IF TIT-TOT-INTR-MORA-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-INTR-MORA
           ELSE
              MOVE 0 TO IND-TIT-TOT-INTR-MORA
           END-IF
           IF TIT-TOT-INTR-PREST-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-INTR-PREST
           ELSE
              MOVE 0 TO IND-TIT-TOT-INTR-PREST
           END-IF
           IF TIT-TOT-INTR-RETDT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-INTR-RETDT
           ELSE
              MOVE 0 TO IND-TIT-TOT-INTR-RETDT
           END-IF
           IF TIT-TOT-INTR-RIAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-INTR-RIAT
           ELSE
              MOVE 0 TO IND-TIT-TOT-INTR-RIAT
           END-IF
           IF TIT-TOT-DIR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-DIR
           ELSE
              MOVE 0 TO IND-TIT-TOT-DIR
           END-IF
           IF TIT-TOT-SPE-MED-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-SPE-MED
           ELSE
              MOVE 0 TO IND-TIT-TOT-SPE-MED
           END-IF
           IF TIT-TOT-TAX-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-TAX
           ELSE
              MOVE 0 TO IND-TIT-TOT-TAX
           END-IF
           IF TIT-TOT-SOPR-SAN-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-SOPR-SAN
           ELSE
              MOVE 0 TO IND-TIT-TOT-SOPR-SAN
           END-IF
           IF TIT-TOT-SOPR-TEC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-SOPR-TEC
           ELSE
              MOVE 0 TO IND-TIT-TOT-SOPR-TEC
           END-IF
           IF TIT-TOT-SOPR-SPO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-SOPR-SPO
           ELSE
              MOVE 0 TO IND-TIT-TOT-SOPR-SPO
           END-IF
           IF TIT-TOT-SOPR-PROF-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-SOPR-PROF
           ELSE
              MOVE 0 TO IND-TIT-TOT-SOPR-PROF
           END-IF
           IF TIT-TOT-SOPR-ALT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-SOPR-ALT
           ELSE
              MOVE 0 TO IND-TIT-TOT-SOPR-ALT
           END-IF
           IF TIT-TOT-PRE-TOT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-PRE-TOT
           ELSE
              MOVE 0 TO IND-TIT-TOT-PRE-TOT
           END-IF
           IF TIT-TOT-PRE-PP-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-PRE-PP-IAS
           ELSE
              MOVE 0 TO IND-TIT-TOT-PRE-PP-IAS
           END-IF
           IF TIT-TOT-CAR-ACQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-CAR-ACQ
           ELSE
              MOVE 0 TO IND-TIT-TOT-CAR-ACQ
           END-IF
           IF TIT-TOT-CAR-GEST-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-CAR-GEST
           ELSE
              MOVE 0 TO IND-TIT-TOT-CAR-GEST
           END-IF
           IF TIT-TOT-CAR-INC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-CAR-INC
           ELSE
              MOVE 0 TO IND-TIT-TOT-CAR-INC
           END-IF
           IF TIT-TOT-PRE-SOLO-RSH-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-PRE-SOLO-RSH
           ELSE
              MOVE 0 TO IND-TIT-TOT-PRE-SOLO-RSH
           END-IF
           IF TIT-TOT-PROV-ACQ-1AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-PROV-ACQ-1AA
           ELSE
              MOVE 0 TO IND-TIT-TOT-PROV-ACQ-1AA
           END-IF
           IF TIT-TOT-PROV-ACQ-2AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-PROV-ACQ-2AA
           ELSE
              MOVE 0 TO IND-TIT-TOT-PROV-ACQ-2AA
           END-IF
           IF TIT-TOT-PROV-RICOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-PROV-RICOR
           ELSE
              MOVE 0 TO IND-TIT-TOT-PROV-RICOR
           END-IF
           IF TIT-TOT-PROV-INC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-PROV-INC
           ELSE
              MOVE 0 TO IND-TIT-TOT-PROV-INC
           END-IF
           IF TIT-TOT-PROV-DA-REC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-PROV-DA-REC
           ELSE
              MOVE 0 TO IND-TIT-TOT-PROV-DA-REC
           END-IF
           IF TIT-IMP-AZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-IMP-AZ
           ELSE
              MOVE 0 TO IND-TIT-IMP-AZ
           END-IF
           IF TIT-IMP-ADER-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-IMP-ADER
           ELSE
              MOVE 0 TO IND-TIT-IMP-ADER
           END-IF
           IF TIT-IMP-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-IMP-TFR
           ELSE
              MOVE 0 TO IND-TIT-IMP-TFR
           END-IF
           IF TIT-IMP-VOLO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-IMP-VOLO
           ELSE
              MOVE 0 TO IND-TIT-IMP-VOLO
           END-IF
           IF TIT-TOT-MANFEE-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-MANFEE-ANTIC
           ELSE
              MOVE 0 TO IND-TIT-TOT-MANFEE-ANTIC
           END-IF
           IF TIT-TOT-MANFEE-RICOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-MANFEE-RICOR
           ELSE
              MOVE 0 TO IND-TIT-TOT-MANFEE-RICOR
           END-IF
           IF TIT-TOT-MANFEE-REC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-MANFEE-REC
           ELSE
              MOVE 0 TO IND-TIT-TOT-MANFEE-REC
           END-IF
           IF TIT-TP-MEZ-PAG-ADD-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TP-MEZ-PAG-ADD
           ELSE
              MOVE 0 TO IND-TIT-TP-MEZ-PAG-ADD
           END-IF
           IF TIT-ESTR-CNT-CORR-ADD-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-ESTR-CNT-CORR-ADD
           ELSE
              MOVE 0 TO IND-TIT-ESTR-CNT-CORR-ADD
           END-IF
           IF TIT-DT-VLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-DT-VLT
           ELSE
              MOVE 0 TO IND-TIT-DT-VLT
           END-IF
           IF TIT-FL-FORZ-DT-VLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-FL-FORZ-DT-VLT
           ELSE
              MOVE 0 TO IND-TIT-FL-FORZ-DT-VLT
           END-IF
           IF TIT-DT-CAMBIO-VLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-DT-CAMBIO-VLT
           ELSE
              MOVE 0 TO IND-TIT-DT-CAMBIO-VLT
           END-IF
           IF TIT-TOT-SPE-AGE-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-SPE-AGE
           ELSE
              MOVE 0 TO IND-TIT-TOT-SPE-AGE
           END-IF
           IF TIT-TOT-CAR-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-CAR-IAS
           ELSE
              MOVE 0 TO IND-TIT-TOT-CAR-IAS
           END-IF
           IF TIT-NUM-RAT-ACCORPATE-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-NUM-RAT-ACCORPATE
           ELSE
              MOVE 0 TO IND-TIT-NUM-RAT-ACCORPATE
           END-IF
           IF TIT-FL-TIT-DA-REINVST-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-FL-TIT-DA-REINVST
           ELSE
              MOVE 0 TO IND-TIT-FL-TIT-DA-REINVST
           END-IF
           IF TIT-DT-RICH-ADD-RID-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-DT-RICH-ADD-RID
           ELSE
              MOVE 0 TO IND-TIT-DT-RICH-ADD-RID
           END-IF
           IF TIT-TP-ESI-RID-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TP-ESI-RID
           ELSE
              MOVE 0 TO IND-TIT-TP-ESI-RID
           END-IF
           IF TIT-COD-IBAN-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-COD-IBAN
           ELSE
              MOVE 0 TO IND-TIT-COD-IBAN
           END-IF
           IF TIT-IMP-TRASFE-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-IMP-TRASFE
           ELSE
              MOVE 0 TO IND-TIT-IMP-TRASFE
           END-IF
           IF TIT-IMP-TFR-STRC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-IMP-TFR-STRC
           ELSE
              MOVE 0 TO IND-TIT-IMP-TFR-STRC
           END-IF
           IF TIT-DT-CERT-FISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-DT-CERT-FISC
           ELSE
              MOVE 0 TO IND-TIT-DT-CERT-FISC
           END-IF
           IF TIT-TP-CAUS-STOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TP-CAUS-STOR
           ELSE
              MOVE 0 TO IND-TIT-TP-CAUS-STOR
           END-IF
           IF TIT-TP-CAUS-DISP-STOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TP-CAUS-DISP-STOR
           ELSE
              MOVE 0 TO IND-TIT-TP-CAUS-DISP-STOR
           END-IF
           IF TIT-TP-TIT-MIGRAZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TP-TIT-MIGRAZ
           ELSE
              MOVE 0 TO IND-TIT-TP-TIT-MIGRAZ
           END-IF
           IF TIT-TOT-ACQ-EXP-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-ACQ-EXP
           ELSE
              MOVE 0 TO IND-TIT-TOT-ACQ-EXP
           END-IF
           IF TIT-TOT-REMUN-ASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-REMUN-ASS
           ELSE
              MOVE 0 TO IND-TIT-TOT-REMUN-ASS
           END-IF
           IF TIT-TOT-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-COMMIS-INTER
           ELSE
              MOVE 0 TO IND-TIT-TOT-COMMIS-INTER
           END-IF
           IF TIT-TP-CAUS-RIMB-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TP-CAUS-RIMB
           ELSE
              MOVE 0 TO IND-TIT-TP-CAUS-RIMB
           END-IF
           IF TIT-TOT-CNBT-ANTIRAC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-TOT-CNBT-ANTIRAC
           ELSE
              MOVE 0 TO IND-TIT-TOT-CNBT-ANTIRAC
           END-IF
           IF TIT-FL-INC-AUTOGEN-NULL = HIGH-VALUES
              MOVE -1 TO IND-TIT-FL-INC-AUTOGEN
           ELSE
              MOVE 0 TO IND-TIT-FL-INC-AUTOGEN
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : TIT-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE TIT-CONT TO WS-BUFFER-TABLE.

           MOVE TIT-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO TIT-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO TIT-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO TIT-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO TIT-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO TIT-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO TIT-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO TIT-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE TIT-CONT TO WS-BUFFER-TABLE.

           MOVE TIT-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO TIT-CONT.

           MOVE WS-ID-MOVI-CRZ  TO TIT-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO TIT-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO TIT-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO TIT-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO TIT-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO TIT-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO TIT-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE TIT-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO TIT-DT-INI-EFF-DB
           MOVE TIT-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO TIT-DT-END-EFF-DB
           IF IND-TIT-DT-INI-COP = 0
               MOVE TIT-DT-INI-COP TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TIT-DT-INI-COP-DB
           END-IF
           IF IND-TIT-DT-END-COP = 0
               MOVE TIT-DT-END-COP TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TIT-DT-END-COP-DB
           END-IF
           IF IND-TIT-DT-APPLZ-MORA = 0
               MOVE TIT-DT-APPLZ-MORA TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TIT-DT-APPLZ-MORA-DB
           END-IF
           IF IND-TIT-DT-EMIS-TIT = 0
               MOVE TIT-DT-EMIS-TIT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TIT-DT-EMIS-TIT-DB
           END-IF
           IF IND-TIT-DT-ESI-TIT = 0
               MOVE TIT-DT-ESI-TIT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TIT-DT-ESI-TIT-DB
           END-IF
           IF IND-TIT-DT-VLT = 0
               MOVE TIT-DT-VLT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TIT-DT-VLT-DB
           END-IF
           IF IND-TIT-DT-CAMBIO-VLT = 0
               MOVE TIT-DT-CAMBIO-VLT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TIT-DT-CAMBIO-VLT-DB
           END-IF
           IF IND-TIT-DT-RICH-ADD-RID = 0
               MOVE TIT-DT-RICH-ADD-RID TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TIT-DT-RICH-ADD-RID-DB
           END-IF
           IF IND-TIT-DT-CERT-FISC = 0
               MOVE TIT-DT-CERT-FISC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TIT-DT-CERT-FISC-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE TIT-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO TIT-DT-INI-EFF
           MOVE TIT-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO TIT-DT-END-EFF
           IF IND-TIT-DT-INI-COP = 0
               MOVE TIT-DT-INI-COP-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TIT-DT-INI-COP
           END-IF
           IF IND-TIT-DT-END-COP = 0
               MOVE TIT-DT-END-COP-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TIT-DT-END-COP
           END-IF
           IF IND-TIT-DT-APPLZ-MORA = 0
               MOVE TIT-DT-APPLZ-MORA-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TIT-DT-APPLZ-MORA
           END-IF
           IF IND-TIT-DT-EMIS-TIT = 0
               MOVE TIT-DT-EMIS-TIT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TIT-DT-EMIS-TIT
           END-IF
           IF IND-TIT-DT-ESI-TIT = 0
               MOVE TIT-DT-ESI-TIT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TIT-DT-ESI-TIT
           END-IF
           IF IND-TIT-DT-VLT = 0
               MOVE TIT-DT-VLT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TIT-DT-VLT
           END-IF
           IF IND-TIT-DT-CAMBIO-VLT = 0
               MOVE TIT-DT-CAMBIO-VLT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TIT-DT-CAMBIO-VLT
           END-IF
           IF IND-TIT-DT-RICH-ADD-RID = 0
               MOVE TIT-DT-RICH-ADD-RID-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TIT-DT-RICH-ADD-RID
           END-IF
           IF IND-TIT-DT-CERT-FISC = 0
               MOVE TIT-DT-CERT-FISC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TIT-DT-CERT-FISC
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

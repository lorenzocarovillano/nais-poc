      *================================================================*
      *
      *    PORTAFOGLIO VITA ITALIA
      *
      *================================================================*
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LLBS0230.
       AUTHOR.             AISS.
       DATE-WRITTEN.       Maggio 2008.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LLBS0230
      *  TIPOLOGIA...... Servizio di Business
      *  PROCESSO....... Bilancio
      *  FUNZIONE....... Estrazione/Aggiorn.dati per riserve matematiche
      *  DESCRIZIONE.... Estrazione dati da portare a riserva
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

      * BILA_VAR_CALC_T
           SELECT FILESQS1 ASSIGN TO FILESQS1
           ACCESS IS SEQUENTIAL
           FILE STATUS IS FS-FILESQS1.

      * BILA_TRCH_ESTR
           SELECT FILESQS2 ASSIGN TO FILESQS2
           ACCESS IS SEQUENTIAL
           FILE STATUS IS FS-FILESQS2.

      * BILA_FND_ESTR
           SELECT FILESQS3 ASSIGN TO FILESQS3
           ACCESS IS SEQUENTIAL
           FILE STATUS IS FS-FILESQS3.

      * BILA_VAR_CALC_P
           SELECT FILESQS4 ASSIGN TO FILESQS4
           ACCESS IS SEQUENTIAL
           FILE STATUS IS FS-FILESQS4.

      * SCARTI
           SELECT FILESQS5 ASSIGN TO FILESQS5
           ACCESS IS SEQUENTIAL
           FILE STATUS IS FS-FILESQS5.

       DATA DIVISION.
       FILE SECTION.

      *-->   BILA_VAR_CALC_T (B05)
       FD  FILESQS1
           RECORDING MODE V
           BLOCK CONTAINS 0
           RECORD IS VARYING IN SIZE
           FROM 1 TO 4242
           DEPENDING ON WK-VAR-REC-SIZE-F01.
       01  FILESQS1-REC                   PIC X(4242).

      *-->   BILA_TRCH_ESTR (B03)
       FD  FILESQS2
           RECORDING MODE V
           BLOCK CONTAINS 0
           RECORD IS VARYING IN SIZE
ITRFO      FROM 1 TO 1819
      *    FROM 1 TO 1691
      *    FROM 1 TO 1705
           DEPENDING ON WK-VAR-REC-SIZE-F02.
ITRFO  01  FILESQS2-REC                   PIC X(1819).
ITRFO *01  FILESQS2-REC                   PIC X(1815).
      *01  FILESQS2-REC                   PIC X(1691).
      *01  FILESQS2-REC                   PIC X(1705).

      *-->  BILA_FND_ESTR (B01)
       FD  FILESQS3
           RECORDING MODE V
           BLOCK CONTAINS 0
           RECORD IS VARYING IN SIZE
           FROM 1 TO 172
      *    FROM 1 TO 180
           DEPENDING ON WK-VAR-REC-SIZE-F03.
       01  FILESQS3-REC                   PIC X(172).
      *01  FILESQS3-REC                   PIC X(180).

      *-->   BILA_VAR_CALC_P (B04)
       FD  FILESQS4
           RECORDING MODE V
           BLOCK CONTAINS 0
           RECORD IS VARYING IN SIZE
           FROM 1 TO 4210
           DEPENDING ON WK-VAR-REC-SIZE-F04.
       01  FILESQS4-REC                   PIC X(4210).

      *-->   SCARTI
       FD  FILESQS5.
       01  FILESQS5-REC                   PIC X(300).

       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LLBS0230'.
      *----------------------------------------------------------------*
      *    CAMPO CONTENENTE LUNGHEZZA DEI FILES VARIABILI
      *----------------------------------------------------------------*
       01  WK-VAR-REC-SIZE-F01              PIC 9(004) COMP-5.
       01  WK-VAR-REC-SIZE-F02              PIC 9(004) COMP-5.
       01  WK-VAR-REC-SIZE-F03              PIC 9(004) COMP-5.
       01  WK-VAR-REC-SIZE-F04              PIC 9(004) COMP-5.
      *----------------------------------------------------------------*
      *    COPY DELLE TABELLE DB2
      *----------------------------------------------------------------*
      *--> GARANZIA
           COPY IDBVGRZ1.
      *--> TRANCHE DI GARANZIA
           COPY IDBVTGA1.
      *--> TITOLO CONTABILE
           COPY IDBVTIT1.
      *--> DETTAGLIO TITOLO CONTABILE
           COPY IDBVDTC1.
      *--> PARAMETRO MOVIMENTO
           COPY IDBVPMO1.
      *--> DATI FISCALI ADESIONE
           COPY IDBVDFA1.
      *--> LIQUIDAZIONE
           COPY IDBVLQU1.
      *--> RISERVA DI TRANCHE
           COPY IDBVRST1.
      *--> MOVIMENTO
           COPY IDBVMOV1.
      *--> RAPPORTO ANAGRAFICO
           COPY IDBVRAN1.
      *--> PARAMETRO OGGETTO
           COPY IDBVPOG1.
      *--> RAPPORTO RETE
           COPY IDBVRRE1.
      *--> BIL-ESTRATTI
           COPY IDBVB031.
      *--> BIL-VAR-DI-CALC-P
           COPY IDBVB041.
      *--> BIL-VAR-DI-CALC-T
           COPY IDBVB051.
      *--> OGGETTO COLLEGATO
           COPY IDBVOCO1.
      *--> PARAM-DI-CALC
           COPY IDBVPCA1.
      *--> RICH_DIS_FND
           COPY IDBVRDF1.
      *--> VALORE_ASSET
           COPY IDBVVAS1.
      *--> QUOTZ_AGG_FND
           COPY IDBVL411.
      *--> PERS
           COPY IDBVA251.
      *--> BILA-FND-ESTR
           COPY IDBVB011.
      *--> ESTENSIONE TRANCHE DI GAR
           COPY IDBVE121.
      *--> QUOTAZIONE FONDI UNIT
           COPY IDBVL191.
      *--> PARAM COMP
           COPY IDBVPCO1.
ITRFO *--> ESTENSIONE POLIZZA CPI PR
ITRFO      COPY IDBVP671.
ITRFO *--> ACCORDO COMMERCIALE
ITRFO      COPY IDBVP631.
      *-----------------------------------------------------------------
      *    WHERE CONDITION AD HOC ESTRAZIONE GARANZIE
      *-----------------------------------------------------------------
      *--> AREA WHERE CONDITION AD HOC PER CONTROLLO GARANZIE
           COPY LDBV3401.
      *--> AREA WHERE CONDITION AD HOC PER ESTRAZIONE GARANZIE
           COPY LDBV3411.
      *--> AREA WHERE CONDITION AD HOC PER CONTROLLO GARANZIE
           COPY LDBV9091.
      *--> AREA WHERE CONDITION AD HOC PER ESTRAZIONE TRANCHES
           COPY LDBV3421.
      *--> AREA WHERE CONDITION AD HOC PER ACCESSO PARAM_OGG
           COPY LDBV1471.
      *--> AREA WHERE CONDITION AD HOC PER ACCESSO PARAMETRO OGGETTO
           COPY LDBV1131.
      *--> AREA WHERE CONDITION AD HOC PER ACCCESO OGGETTO COLLEGATO
           COPY LDBV0721.
      *--> AREA WHERE CONDITION AD HOC PER LDBS2650
           COPY LDBV2651.
      *--> AREA WHERE CONDITION AD HOC PER LDBS2630
           COPY LDBV2631.
      *--> AREA WHERE CONDITION AD HOC PER LDBS0130
           COPY LDBV0011.
      *--> AREA WHERE CONDITION AD HOC PER LDBS1240
           COPY LDBV1241.
      *--> AREA WHERE CONDITION AD HOC PER LDBS4150
           COPY LDBV4151.
      *       261 -->  Costi di emissione
      *--> AREA WHERE CONDITION AD HOC PER LDBS0370
           COPY LDBV0371.
      *       261 -->  Costi di emissione FINE
      *--> AREA WHERE CONDITION AD HOC PER LDBS5470
           COPY LDBV5471.
      *--> AREA MODULO LCCS0450
           COPY LCCC0450.
      *--> AREA MODULO LDBS1300
           COPY LDBV1301.
      *--> AREA WHERE CONDITION AD HOC PER LDBSC590
           COPY LDBVC591.
      *--> AREA WHERE CONDITION AD HOC PER LDBSC600
           COPY LDBVC601.
29398 *--> AREA WHERE CONDITION LDBS0640
29398      COPY LDBV0641.
MIGCOL     COPY LDBVH601.
      *----------------------------------------------------------------
      * AREE DA FORNIRE IN INPUT ALLA FASE DI EOC
      *----------------------------------------------------------------
       01 AREA-OUT-B03.
           03 WB03-AREA-B03.
              04 WB03-ELE-B03-MAX       PIC S9(04) COMP VALUE 0.
              04 WB03-TAB-B03.
              COPY LCCVB031             REPLACING ==(SF)== BY ==WB03==.
       01 AREA-OUT-B04.
           03 WB04-AREA-B04.
              04 WB04-ELE-B04-MAX       PIC S9(04) COMP VALUE 0.
              04 WB04-TAB-B04.
              COPY LCCVB041             REPLACING ==(SF)== BY ==WB04==.
       01 AREA-OUT-B05.
           03 WB05-AREA-B05.
              04 WB05-ELE-B05-MAX       PIC S9(04) COMP VALUE 0.
              04 WB05-TAB-B05.
              COPY LCCVB051             REPLACING ==(SF)== BY ==WB05==.

      *----------------------------------------------------------------*
      *   AREA FLUSSO DI OUTPUT (FILESQS1) BILA_VAR_CALC_T
      *----------------------------------------------------------------*
       01 AREA-OUT-W-B05.
           COPY LCCVB058              REPLACING ==(SF)== BY ==W-B05==.

      *----------------------------------------------------------------*
      *   AREA FLUSSO DI OUTPUT (FILESQS2) BILA_TRCH_ESTR
      *----------------------------------------------------------------*
        01 AREA-OUT-W-B03.
            COPY LCCVB038              REPLACING ==(SF)== BY ==W-B03==.

      *----------------------------------------------------------------*
      *   AREA FLUSSO DI OUTPUT (FILESQS3) BILA_FND_ESTR
      *----------------------------------------------------------------*
        01 AREA-OUT-W-B01.
            COPY LCCVB018              REPLACING ==(SF)== BY ==W-B01==.

      *----------------------------------------------------------------*
      *   AREA FLUSSO DI OUTPUT (FILESQS4) BILA_VAR_CALC_P
      *----------------------------------------------------------------*
        01 AREA-OUT-W-B04.
            COPY LCCVB048              REPLACING ==(SF)== BY ==W-B04==.


      *----------------------------------------------------------------*
      *   AREA FLUSSO DI OUTPUT (FILESQS5) FILE SCARTI LREC. 300
      *----------------------------------------------------------------*
        01 AREA-OUT-SCARTI.
               05 SCA-POL-IB-OGG                 PIC X(40).
               05 SCA-POL-ID-POLI                PIC 9(09).
               05 SCA-POL-ID-POLI-ALF REDEFINES
                  SCA-POL-ID-POLI                PIC X(09).
               05 SCA-ADE-ID-ADES                PIC 9(09).
               05 SCA-ADE-ID-ADES-ALF REDEFINES
                  SCA-ADE-ID-ADES                PIC X(09).
               05 FILLER                         PIC X(242).

      *---------------------------------------------------------------*
      * AREA PER CONTATORI PERSONALIZZATI PER SCARTI
      *---------------------------------------------------------------*
       01 WS-AREA-CONTATORI.
          03 WS-CTR-1.
             05 DESC-CTR-1           PIC X(050)
                 VALUE '-- DI CUI SCRITTE NEL REC. SCARTI'.
             05 REC-SCARTATI-1       PIC 9(002) VALUE 1.

           COPY IDSV0010.

      *----------------------------------------------------------------*
      *    COPY PER GESTIONE FILE SEQUENZIALI
      *----------------------------------------------------------------*
           COPY IABVSQS1.
           COPY IABCSQ99.

      *----------------------------------------------------------------*
      *    CAMPI X ROUTINES DI GESTIONE DISPLAY IDSP8888
      *----------------------------------------------------------------*
           COPY IDSV8888.
      *----------------------------------------------------------------*
           COPY LDBV4911.
           COPY LDBV1211.

      ******************************************************************
      *    COPY DISPATCHER
      ******************************************************************
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      *  COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *  COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IEAV9903.
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    COPY LLBV0000 - DICHIARAZIONE VARIABILI DI WORKING COMUNI
      *----------------------------------------------------------------*
           COPY LLBV0000.
      *----------------------------------------------------------------*
      *      per modulo di stringatura variabili                       *
      *----------------------------------------------------------------*
           COPY IDSV0501.
           COPY IVVC0501.
      *----------------------------------------------------------------*
      *--> Area per modulo conversione stringa
      *----------------------------------------------------------------*
       01  AREA-IWFS0050.
           COPY IWFI0051.
           COPY IWFO0051.
      *----------------------------------------------------------------*
      *--> Area per stringatura variabili
      *----------------------------------------------------------------*
           COPY IDSV0503.
      *----------------------------------------------------------------*
      * MODULI CHIAMATI                                                *
      *----------------------------------------------------------------*
      *--> MODULO CONTROLLO AD HOC RAMO GARANZIA.
       01  LDBS3400                         PIC X(8) VALUE 'LDBS3400'.
      *--> MODULO LETTURA AD HOC GARANZIA.
       01  LDBS3410                         PIC X(8) VALUE 'LDBS3410'.
      *--> MODULO LETTURA AD HOC TRANCHE.
       01  LDBS3420                         PIC X(8) VALUE 'LDBS3420'.
      *--> MODULO LETTURA AD HOC DATI FISCALI ADESIONE.
       01  LDBS3430                         PIC X(8) VALUE 'LDBS3430'.
      *--> MODULO LETTURA AD HOC PARAMETRO OGGETTO.
       01  LDBS1130                         PIC X(8) VALUE 'LDBS1130'.
      *--> MODULO LETTURA AD HOC PARAMETRO MOVIMENTO.
       01  LDBS1470                         PIC X(8) VALUE 'LDBS1470'.
      *--> MODULO LETTURA AD HOC DETTAGLIO TITOLO CONTABILE
       01  LDBS4150                         PIC X(8) VALUE 'LDBS4150'.
      *--> MODULO LETTURA AD HOC VALORE ASSET
       01  LDBS1210                         PIC X(8) VALUE 'LDBS1210'.
      *--> MODULO LETTURA AD HOC DETTAGLIO TITOLO CONTABILE
       01  LDBS5900                         PIC X(8) VALUE 'LDBS5900'.
      *--> MODULO EOC BIL_ESTRATTI
       01  LLBS0240                         PIC X(8) VALUE 'LLBS0240'.
      *--> MODULO EOC BIL_VAR_DI_CALC_T
       01  LLBS0250                         PIC X(8) VALUE 'LLBS0250'.
      *--> MODULO EOC BIL_VAR_DI_CALC_P
       01  LLBS0266                         PIC X(8) VALUE 'LLBS0266'.
      *--> MODULO CALCOLO DATE
       01  LCCS0010                         PIC X(8) VALUE 'LCCS0010'.
      *--> MODULO CALCOLO DATE 365
       01  LCCS0017                         PIC X(8) VALUE 'LCCS0017'.
      *--> MODULO CALCOLO DATE
       01  LCCS0003                         PIC X(8) VALUE 'LCCS0003'.
      *--> ROUTINE FONDI
       01  LCCS0450                         PIC X(8) VALUE 'LCCS0450'.
      *--> ESTRAZIONE SEQUENCE
       01  LCCS0090                         PIC X(8) VALUE 'LCCS0090'.
      *--> Conversione Stringa a Numerico
       01  PGM-IWFS0050                     PIC X(8) VALUE 'IWFS0050'.
      *--> MODULO LETTURA AD HOC VALORE ASSET PER ID TRANCHE
       01  LDBS4910                         PIC X(8) VALUE 'LDBS4910'.
      *--> MODULO LETTURA LIQ
       01  IDBSLQU0                         PIC X(8) VALUE 'IDBSLQU0'.
ITRFO *--> MODULO LETTURA EST_POLI_CPI_PR
ITRFO  01  PGM-IDBSP670                     PIC X(8) VALUE 'IDBSP670'.
ITRFO *--> MODULO LETTURA LIQ
ITRFO  01  PGM-IDBSP630                     PIC X(8) VALUE 'IDBSP630'.

      *------- Estrazione Sequence     -------------------------------
       01  AREA-IO-LCCS0090.
           COPY LCCC0090                 REPLACING ==(SF)== BY ==S090==.

      *----------------------------------------------------------------*
      *    DEFINIZIONE DI INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-TAB-POL                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-ADE                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-GRZ                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-TGA                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-STB                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-PMO                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-POG                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-RST                     PIC S9(04) COMP VALUE 0.
           03 IX-AREA-SCHEDA                 PIC S9(04) COMP VALUE 0.
           03 IX-TAB-VAR                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-B03                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-B04                     PIC S9(04) COMP VALUE 0.
           03 IX-CONTA                       PIC S9(04) COMP VALUE 0.
           03 IX-TAB-STR                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-TGA-STR                 PIC S9(04) COMP VALUE 0.
           03 IX-TAB-TGA1                    PIC S9(04) COMP VALUE 0.
           03 IX-TAB-501                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-450                     PIC S9(04) COMP VALUE 0.
           03 IX-TAB-LISTA                   PIC S9(04) COMP VALUE 0.
           03 IX-TAB-CAUS                    PIC S9(04) COMP VALUE 0.
           03 IX-CNT                         PIC S9(04) COMP VALUE 0.
           03 IX-MAX-VAR                     PIC S9(04) COMP VALUE 0.
      *----------------------------------------------------------------*
      *    CAMPI DI APPOGGIO
      *----------------------------------------------------------------*
      *  FLAG SCRITTURA SULLE 'TABELLE BILA'/'FILE SEQUENZIALI'
      *
       01 WK-STRADA                    PIC X(01) VALUE 'S'.
          88 WK-STRADA-SEQ             VALUE 'S'.
          88 WK-STRADA-TAB             VALUE 'T'.
      *
      *  FLAG ABILITAZIONE ALLA STRINGATURA DELLE VARIABILI DA PASSARE
      *  ALLA FASE DI CALCOLO
      *
       01 WK-STRINGATURA               PIC X(01) VALUE 'S'.
          88 WK-STRINGATURA-SI         VALUE 'S'.
          88 WK-STRINGATURA-NO         VALUE 'N'.

       01 WK-NUM-VAR                   PIC 9(02).
       01 WK-LUNGH-STRINGA             PIC 9(04).

45630 *  FLAG ABILITAZIONE SCRITTURA DI UNA TRANCHE DI RAMO III SOLO
45630 *  SE VIENE SCRITTA L'OCCORRENZA SULLA TAB. FND_ESTR
45630 *
45630  01 WS-FL-SCRIVI-TRCH            PIC X(01) VALUE 'S'.
45630     88 WS-FL-SCRIVI-TRCH-SI      VALUE 'S'.
45630     88 WS-FL-SCRIVI-TRCH-NO      VALUE 'N'.

45630 *  FLAG VERIFICA SE TUTTI I FONDI DI UNA TRANCHE HANNO SALDO
45630 *  QUOTE UGUALE A ZERO
45630 *
45630  01 WS-FL-TUTTI-FND-A-0          PIC X(01) VALUE 'S'.
45630     88 WS-FL-TUTTI-FND-A-0-SI    VALUE 'S'.
45630     88 WS-FL-TUTTI-FND-A-0-NO    VALUE 'N'.

ALEX   01 WK-ALQ-MARG-RIS              PIC S9(3)V9(3) COMP-3.
ALEX   01 WK-ALQ-MARG-RIS-NULL         REDEFINES
ALEX       WK-ALQ-MARG-RIS             PIC X(4).
ALEX   01 WK-ALQ-MARG-C-SUBRSH         PIC S9(3)V9(3) COMP-3.
ALEX   01 WK-ALQ-MARG-C-SUBRSH-NULL    REDEFINES
ALEX       WK-ALQ-MARG-C-SUBRSH        PIC X(4).
       01 WK-MESI-CARENZA              PIC S9(05) COMP-3.
       01 WK-MESI-CARENZA-NULL         REDEFINES
           WK-MESI-CARENZA              PIC X(3).

       01 WK-DATA-APPO                 PIC  9(08).
       01 WK-DATA-APPO-N               REDEFINES
             WK-DATA-APPO.
          03 WK-DATA-APPO-N-AA         PIC  X(04).
          03 WK-DATA-APPO-N-MM         PIC  X(02).
          03 WK-DATA-APPO-N-GG         PIC  X(02).
       01 WK-DATA-APPO-9-AA            PIC  9(04).
       01 WK-DATA-APPO-X.
          03 WK-DATA-APPO-X-AA         PIC  9(04).
          03 WK-DATA-FILLER            PIC  X(01) VALUE '-'.
          03 WK-DATA-APPO-X-MM         PIC  9(02).
          03 WK-DATA-FILLER            PIC  X(01) VALUE '-'.
          03 WK-DATA-APPO-X-GG         PIC  9(02).

       01 WK-DT-DECOR-TRCH             PIC 9(008) VALUE ZEROES.

       01 WK-DATA-CALCOLATA            PIC 9(008) VALUE ZEROES.
       01 WK-DT-DECOR-POL              PIC 9(008) VALUE ZEROES.

       01 WS-DATA-RISERVA-COMP         PIC S9(08)V COMP-3.
       01 WS-DATA-RISERVA-N            REDEFINES
             WS-DATA-RISERVA-COMP      PIC  9(08).

       01 WK-APPO-AREA-STB             PIC  X(92).
       01 WS-NUM-AA-PAG-PRE            PIC  9(05).
       01 WS-FRQ-MOVI                  PIC  9(05).
       01 WS-FRAZ-INI-EROG-REN         PIC  9(05).
       01 WS-PRSTZ-ULT                 PIC  9(12).
       01 WK-DT-ULT-ADEG-PRE-PR        PIC  9(08).

       01 WS-DT-SCAD-PAG-PRE           PIC  9(08).
       01 WS-DT-SCAD-PAG-PRE-COMP      REDEFINES
             WS-DT-SCAD-PAG-PRE        PIC S9(8) COMP-3.
       01 WS-DUR-RES-DT-CALC           PIC  9(05).
       01 WS-DUR-RES-DT-CALC-COMP      REDEFINES
             WS-DUR-RES-DT-CALC        PIC S9(5) COMP-3.

       01 WS-NUM-PRE-PATT              PIC  9(05).
       01 WS-NUM-PRE-PATT-COMP         REDEFINES
             WS-NUM-PRE-PATT           PIC S9(5) COMP-3.

       01 WS-RAT-DI-REN-COMP           PIC S9(12)V9(3) COMP-3.
       01 WS-RAT-DI-REN                REDEFINES
             WS-RAT-DI-REN-COMP        PIC  9(12).
       01 WS-VAL-D                     PIC S9(11)V9(07).
       01 WS-VAL-D-X                   REDEFINES
             WS-VAL-D                  PIC X(20).

       01 WK-DATA-APPO2.
          03 WK-DATA-TGA-SEP-AA        PIC  9(04).
          03 WK-DATA-TGA-FILLER        PIC  X(01) VALUE '-'.
          03 WK-DATA-TGA-SEP-MM        PIC  9(02).
          03 WK-DATA-TGA-FILLER        PIC  X(01) VALUE '-'.
          03 WK-DATA-TGA-SEP-GG        PIC  9(02).

       01 WK-COD-PARAM                 PIC  X(20).

       01 WK-LETT-MOV-RISC-PARZ-FUTURO   PIC  X(01) VALUE 'N'.
          88 WK-LETT-MOV-RISC-PARZ-FUTURO-N         VALUE 'N'.
          88 WK-LETT-MOV-RISC-PARZ-FUTURO-S         VALUE 'S'.

       01 WK-TIPO-ELABORAZIONE         PIC  9 VALUE 2.
          88 GESTIONE-RISERVE-RAMO-OPERAZ     VALUE 1.
          88 GESTIONE-RISERVE-STANDARD        VALUE 2.

       01 WK-DA-SCARTARE               PIC  X VALUE 'N'.
          88 WK-DA-SCARTARE-SI         VALUE 'S'.
          88 WK-DA-SCARTARE-NO         VALUE 'N'.

       01 WK-RISC-PARZ-TROVATO         PIC  X VALUE 'N'.
          88 RISC-PARZ-TROVATO-SI      VALUE 'S'.
          88 RISC-PARZ-TROVATO-NO      VALUE 'N'.

       01 WK-EL-RISC-PARZ.
          05 WK-MOV-ID-MOVI            PIC  S9(9)V COMP-3.
          05 WK-MOV-DT-EFF             PIC  S9(8)V COMP-3.
          05 WK-MOV-DS-TS-CPTZ         PIC S9(18)V COMP-3.

       01 WS-STAT-CAUS-POL.
          05 WS-TP-STAT-BUS-POL        PIC X(02) VALUE SPACES.
          05 WS-TP-CAUS-POL            PIC X(02) VALUE SPACES.

       01 WS-STAT-CAUS-ADE.
          05 WS-TP-STAT-BUS-ADE        PIC X(02) VALUE SPACES.
          05 WS-TP-CAUS-ADE            PIC X(02) VALUE SPACES.

       01 WS-STAT-CAUS-TGA.
          05 WS-TP-STAT-BUS-TGA        PIC X(02) VALUE SPACES.
          05 WS-TP-CAUS-TGA            PIC X(02) VALUE SPACES.

       01 WS-ANNO                      PIC 9(04).
       01 WS-DT-N.
          05 WS-DT-N-AA                PIC 9(04).
          05 WS-DT-N-MM                PIC 9(02).
          05 WS-DT-N-GG                PIC 9(02).
       01 WS-DT-COMP                   REDEFINES
             WS-DT-N                   PIC S9(08) COMP-3.

       01 WS-DT-X.
          05 WS-DT-X-AA                PIC 9(04).
          05 FILLER                    PIC X(01)  VALUE '-'.
          05 WS-DT-X-MM                PIC 9(02).
          05 FILLER                    PIC X(01)  VALUE '-'.
          05 WS-DT-X-GG                PIC 9(02).

       01 WS-DT-RST.
          05 WS-DT-RST-AA              PIC 9(04).
          05 WS-DT-RST-MM              PIC 9(02)  VALUE 12 .
          05 WS-DT-RST-GG              PIC 9(02)  VALUE 31 .

       01 WS-DT-RST-9                  PIC 9(08).

       01 WS-DT-INI.
          05 WS-DT-INI-AA              PIC 9(04).
          05 WS-DT-INI-MM              PIC 9(02)  VALUE 01 .
          05 WS-DT-INI-GG              PIC 9(02)  VALUE 01 .

       01 WS-DT-INI-9                  PIC 9(08).
       01 WK-ADESIONE-9                PIC 9(08).
       01 WS-DT-PTF-X                  PIC X(10).
       01 WS-DT-TS-PTF                 PIC S9(18) COMP-3.

       01 WS-DATA-COMPETENZA.
          03 WS-DATA-COMPETENZA-X.
             05 WS-DT-COMPETENZA-8     PIC 9(08).
             05 WS-LT-40               PIC X(02).
             05 WS-LT-ORA-COMPETENZA-8 PIC X(08).
          03 WS-DATA-COMPETENZA-9      REDEFINES
             WS-DATA-COMPETENZA-X      PIC 9(18).

       01 WS-DATA-COMP-RST.
          03 WS-DATA-COMP-RST-X.
             05 WS-DT-COMP-RST-8       PIC 9(08).
             05 WS-LT-40-RST           PIC X(02).
             05 WS-LT-ORA-COMP-RST-8   PIC X(08).
          03 WS-DATA-COMP-RST-9        REDEFINES
             WS-DATA-COMP-RST-X        PIC 9(18).

       01 WS-SOMMA-IMPO-LIQ            PIC S9(12)V9(3) COMP-3.

       01 WS-APPO-NUM-18               PIC 9(18) VALUE ZERO.

       01 WK-ID-MOVI-CRZ               PIC 9(09) VALUE ZEROES.

       01  WK-TRACE-ON                 PIC X(001) VALUE 'S'.
           88 SI-TRACE                            VALUE 'S'.
           88 NO-TRACE                            VALUE 'N'.

VSTEF *--> Tipo periodo premio (Garanzia)
       01  WS-TP-PER-PREMIO                       PIC X(001).
           88 WS-PREMIO-UNICO                     VALUE 'U'.
           88 WS-PREMIO-RICORRENTE                VALUE 'R'.
           88 WS-PREMIO-ANNUALE                   VALUE 'A'.

      *-- PREMIO TOTALE (DETTAGLIO TITOLO CONTABILE)
VSTEF  01  WS-PRE-TOT                   PIC S9(12)V9(3) COMP-3.
       01  WS-PRE-TOT-NULL              REDEFINES
           WS-PRE-TOT                   PIC X(8).

      *-- TAX TOTALE (DETTAGLIO TITOLO CONTABILE)
       01  WS-TAX-TOT                   PIC S9(12)V9(3) COMP-3.
       01  WS-TAX-TOT-NULL              REDEFINES
           WS-TAX-TOT                   PIC X(8).

      *-- DIFF. TRA IL TOTALE PREMIO E IL TOT. TAX
       01  WS-DIFF-PRE                  PIC S9(12)V9(3) COMP-3.
       01  WS-DIFF-PRE-NULL             REDEFINES
           WS-DIFF-PRE                   PIC X(8).

      *-- SOMMA TRA LE WTGA-PROV-1AA-ACQ
      *                WTGA-PROV-2AA-ACQ
      * DELLA TRANCHE DI GARANZIA
       01 WS-TOT-PROV-ACQ-WTGA          PIC S9(12)V9(3) COMP-3.

       01 WS-DT-EFFETTO                     PIC 9(08).

       01  WK-DATA-ULT-ADEG      PIC 9(08).

       01  WK-DATA-DECO-CALC     PIC 9(08).
       01  WK-DATA-DECO-CALC-RED REDEFINES WK-DATA-DECO-CALC.
           05 WK-ANNO-DECO       PIC 9(04).
           05 WK-MESE-DECO       PIC 9(02).
           05 WK-GIORNO-DECO     PIC 9(02).

       01  WK-DATA-OUTPUT                  PIC 9(04)V9(03).
       01  WK-DATA-OUTPUT-V REDEFINES WK-DATA-OUTPUT.
           03 WK-NUMERICI                  PIC 9(04).
           03 WK-DECIMALI                  PIC 9(03).

       01  WK-VARIABILE                    PIC X(12).
       01  WK-TIPO-DATO                    PIC X(1).

      *    TABELLA DI WORKING PER GESTIRE CAUSALE BUSINESS TRANCHE
       01  WK-TAB-CAUSALI.
           03 WK-TAB-CAUS-ELE-MAX          PIC S9(04) COMP.
           03 WK-TAB-CAUS1.
              COPY LCCVTGAC REPLACING   ==(SF)==  BY ==WK==.
                 07 WK-TC-ID-TRCH             PIC S9(9)     COMP-3.
                 07 WK-TC-TP-STAT-DISINV      PIC X(2).
           03 WK-TAB-CAUS1-R REDEFINES WK-TAB-CAUS1.
              06 FILLER                    PIC X(7).
              06 WK-RESTO-TAB-CAUS1        PIC X(8743).

      *    COSTANTE PER CAUSALE DI BUSINESS PER TRANCHE CON
      *    RICHIESTA DI DISINVESTIMENTO NON CONCLUSA
       01  WK-COST-CAUSALE-TRCH            PIC X(2) VALUE '99'.

       01  WK-ID-ASSTO                     PIC S9(9)     COMP-3.
       01  WK-ID-STRINGA                   PIC X(20) VALUE SPACES.

       01  WK-PERCENTUALE                  PIC S9(3)V9(9) COMP-3.

       01  WK-LQU-ID-LIQ                   PIC S9(9)     COMP-3.

      *COPY RAPP-ANA JOIN PERS

       01 WK-RANPERS.
        04 IX-WRAN                        PIC 9(9).
        04 IX-WRAN-MAX                    PIC 9(9).
        COPY LCCVRANA REPLACING   ==(SF)==  BY ==WRAPP==.
          05 WRAN-ID-RAPP-ANA PIC S9(9)V     COMP-3.
          05 WRAN-ID-RAPP-ANA-COLLG PIC S9(9)V     COMP-3.
          05 WRAN-ID-RAPP-ANA-COLLG-NULL REDEFINES
             WRAN-ID-RAPP-ANA-COLLG   PIC X(5).
          05 WRAN-ID-OGG PIC S9(9)V     COMP-3.
          05 WRAN-TP-OGG PIC X(2).
          05 WRAN-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
          05 WRAN-ID-MOVI-CHIU PIC S9(9)V     COMP-3.
          05 WRAN-ID-MOVI-CHIU-NULL REDEFINES
             WRAN-ID-MOVI-CHIU   PIC X(5).
          05 WRAN-DT-INI-EFF   PIC S9(8)V COMP-3.
          05 WRAN-DT-END-EFF   PIC S9(8)V COMP-3.
          05 WRAN-COD-COMP-ANIA PIC S9(5)V     COMP-3.
          05 WRAN-COD-SOGG PIC X(20).
          05 WRAN-COD-SOGG-NULL REDEFINES
             WRAN-COD-SOGG   PIC X(20).
          05 WRAN-TP-RAPP-ANA PIC X(2).
          05 WRAN-TP-PERS PIC X(1).
          05 WRAN-TP-PERS-NULL REDEFINES
             WRAN-TP-PERS   PIC X(1).
          05 WRAN-SEX PIC X(1).
          05 WRAN-SEX-NULL REDEFINES
             WRAN-SEX   PIC X(1).
          05 WRAN-DT-NASC   PIC S9(8)V COMP-3.
          05 WRAN-DT-NASC-NULL REDEFINES
             WRAN-DT-NASC   PIC X(5).
          05 WRAN-FL-ESTAS PIC X(1).
          05 WRAN-FL-ESTAS-NULL REDEFINES
             WRAN-FL-ESTAS   PIC X(1).
          05 WRAN-INDIR-1 PIC X(20).
          05 WRAN-INDIR-1-NULL REDEFINES
             WRAN-INDIR-1   PIC X(20).
          05 WRAN-INDIR-2 PIC X(20).
          05 WRAN-INDIR-2-NULL REDEFINES
             WRAN-INDIR-2   PIC X(20).
          05 WRAN-INDIR-3 PIC X(20).
          05 WRAN-INDIR-3-NULL REDEFINES
             WRAN-INDIR-3   PIC X(20).
          05 WRAN-TP-UTLZ-INDIR-1 PIC X(2).
          05 WRAN-TP-UTLZ-INDIR-1-NULL REDEFINES
             WRAN-TP-UTLZ-INDIR-1   PIC X(2).
          05 WRAN-TP-UTLZ-INDIR-2 PIC X(2).
          05 WRAN-TP-UTLZ-INDIR-2-NULL REDEFINES
             WRAN-TP-UTLZ-INDIR-2   PIC X(2).
          05 WRAN-TP-UTLZ-INDIR-3 PIC X(2).
          05 WRAN-TP-UTLZ-INDIR-3-NULL REDEFINES
             WRAN-TP-UTLZ-INDIR-3   PIC X(2).
          05 WRAN-ESTR-CNT-CORR-ACCR PIC X(20).
          05 WRAN-ESTR-CNT-CORR-ACCR-NULL REDEFINES
             WRAN-ESTR-CNT-CORR-ACCR   PIC X(20).
          05 WRAN-ESTR-CNT-CORR-ADD PIC X(20).
          05 WRAN-ESTR-CNT-CORR-ADD-NULL REDEFINES
             WRAN-ESTR-CNT-CORR-ADD   PIC X(20).
          05 WRAN-ESTR-DOCTO PIC X(20).
          05 WRAN-ESTR-DOCTO-NULL REDEFINES
             WRAN-ESTR-DOCTO   PIC X(20).
          05 WRAN-PC-NEL-RAPP PIC S9(3)V9(3) COMP-3.
          05 WRAN-PC-NEL-RAPP-NULL REDEFINES
             WRAN-PC-NEL-RAPP   PIC X(4).
          05 WRAN-TP-MEZ-PAG-ADD PIC X(2).
          05 WRAN-TP-MEZ-PAG-ADD-NULL REDEFINES
             WRAN-TP-MEZ-PAG-ADD   PIC X(2).
          05 WRAN-TP-MEZ-PAG-ACCR PIC X(2).
          05 WRAN-TP-MEZ-PAG-ACCR-NULL REDEFINES
             WRAN-TP-MEZ-PAG-ACCR   PIC X(2).
          05 WRAN-COD-MATR PIC X(20).
          05 WRAN-COD-MATR-NULL REDEFINES
             WRAN-COD-MATR   PIC X(20).
          05 WRAN-TP-ADEGZ PIC X(2).
          05 WRAN-TP-ADEGZ-NULL REDEFINES
             WRAN-TP-ADEGZ   PIC X(2).
          05 WRAN-FL-TST-RSH PIC X(1).
          05 WRAN-FL-TST-RSH-NULL REDEFINES
             WRAN-FL-TST-RSH   PIC X(1).
          05 WRAN-COD-AZ PIC X(30).
          05 WRAN-COD-AZ-NULL REDEFINES
             WRAN-COD-AZ   PIC X(30).
          05 WRAN-IND-PRINC PIC X(2).
          05 WRAN-IND-PRINC-NULL REDEFINES
             WRAN-IND-PRINC   PIC X(2).
          05 WRAN-DT-DELIBERA-CDA   PIC S9(8)V COMP-3.
          05 WRAN-DT-DELIBERA-CDA-NULL REDEFINES
             WRAN-DT-DELIBERA-CDA   PIC X(5).
          05 WRAN-DLG-AL-RISC PIC X(1).
          05 WRAN-DLG-AL-RISC-NULL REDEFINES
             WRAN-DLG-AL-RISC   PIC X(1).
          05 WRAN-LEGALE-RAPPR-PRINC PIC X(1).
          05 WRAN-LEGALE-RAPPR-PRINC-NULL REDEFINES
             WRAN-LEGALE-RAPPR-PRINC   PIC X(1).
          05 WRAN-TP-LEGALE-RAPPR PIC X(2).
          05 WRAN-TP-LEGALE-RAPPR-NULL REDEFINES
             WRAN-TP-LEGALE-RAPPR   PIC X(2).
          05 WRAN-TP-IND-PRINC PIC X(2).
          05 WRAN-TP-IND-PRINC-NULL REDEFINES
             WRAN-TP-IND-PRINC   PIC X(2).
          05 WRAN-TP-STAT-RID PIC X(2).
          05 WRAN-TP-STAT-RID-NULL REDEFINES
             WRAN-TP-STAT-RID   PIC X(2).
          05 WRAN-NOME-INT-RID-VCHAR.
            49 WRAN-NOME-INT-RID-LEN PIC S9(4) COMP-5.
            49 WRAN-NOME-INT-RID PIC X(100).
          05 WRAN-COGN-INT-RID-VCHAR.
            49 WRAN-COGN-INT-RID-LEN PIC S9(4) COMP-5.
            49 WRAN-COGN-INT-RID PIC X(100).
          05 WRAN-COGN-INT-TRATT-VCHAR.
            49 WRAN-COGN-INT-TRATT-LEN PIC S9(4) COMP-5.
            49 WRAN-COGN-INT-TRATT PIC X(100).
          05 WRAN-NOME-INT-TRATT-VCHAR.
            49 WRAN-NOME-INT-TRATT-LEN PIC S9(4) COMP-5.
            49 WRAN-NOME-INT-TRATT PIC X(100).
          05 WRAN-CF-INT-RID PIC X(16).
          05 WRAN-CF-INT-RID-NULL REDEFINES
             WRAN-CF-INT-RID   PIC X(16).
          05 WRAN-FL-COINC-DIP-CNTR PIC X(1).
          05 WRAN-FL-COINC-DIP-CNTR-NULL REDEFINES
             WRAN-FL-COINC-DIP-CNTR   PIC X(1).
          05 WRAN-FL-COINC-INT-CNTR PIC X(1).
          05 WRAN-FL-COINC-INT-CNTR-NULL REDEFINES
             WRAN-FL-COINC-INT-CNTR   PIC X(1).
          05 WRAN-DT-DECES   PIC S9(8)V COMP-3.
          05 WRAN-DT-DECES-NULL REDEFINES
             WRAN-DT-DECES   PIC X(5).
          05 WRAN-FL-FUMATORE PIC X(1).
          05 WRAN-FL-FUMATORE-NULL REDEFINES
             WRAN-FL-FUMATORE   PIC X(1).
          05 WRAN-DS-RIGA PIC S9(10)V     COMP-3.
          05 WRAN-DS-OPER-SQL PIC X(1).
          05 WRAN-DS-VER PIC S9(9)V     COMP-3.
          05 WRAN-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
          05 WRAN-DS-TS-END-CPTZ PIC S9(18)V     COMP-3.
          05 WRAN-DS-UTENTE PIC X(20).
          05 WRAN-DS-STATO-ELAB PIC X(1).
          05 WRAN-FL-LAV-DIP PIC X(1).
          05 WRAN-FL-LAV-DIP-NULL REDEFINES
             WRAN-FL-LAV-DIP   PIC X(1).
          05 WRAN-TP-VARZ-PAGAT PIC X(2).
          05 WRAN-TP-VARZ-PAGAT-NULL REDEFINES
             WRAN-TP-VARZ-PAGAT   PIC X(2).
          05 WRAN-COD-RID PIC X(11).
          05 WRAN-COD-RID-NULL REDEFINES
             WRAN-COD-RID   PIC X(11).
          05 WRAN-TP-CAUS-RID PIC X(2).
          05 WRAN-TP-CAUS-RID-NULL REDEFINES
             WRAN-TP-CAUS-RID   PIC X(2).

        02 WPERS OCCURS 100.
          05 WA25-IND-CLI PIC X(1).
          05 WA25-IND-CLI-NULL REDEFINES
             WA25-IND-CLI   PIC X(1).
          05 WA25-COD-FISC PIC X(16).
          05 WA25-COD-FISC-NULL REDEFINES
             WA25-COD-FISC   PIC X(16).
          05 WA25-COD-PRT-IVA PIC X(11).
          05 WA25-COD-PRT-IVA-NULL REDEFINES
             WA25-COD-PRT-IVA   PIC X(11).




       77  WS-TS-INFINITO    PIC S9(18) COMP-3 VALUE 999912314023595999.
       77  WS-TS-INFINITO-1  PIC S9(18) COMP-3 VALUE 999912304023595999.
       77  WS-HH-INFINITO    PIC 9(08) VALUE 23595999.
       77  WS-DT-INFINITO    PIC 9(08) VALUE 99991231.
       77  WS-DT-INFINITO-1  PIC X(10) VALUE '9999-12-30'.





      *----------------------------------------------------------------*
      *     SWITCHES
      *----------------------------------------------------------------*
       01  SWITCHES.
           03  SW-SCARTA-GAR           PIC X.
               88 SCARTA-GAR           VALUE 'S'.
               88 SCARTA-GAR-NO        VALUE 'N'.
           03  SW-SCARTA-TGA           PIC X.
               88 SCARTA-TGA           VALUE 'S'.
               88 SCARTA-TGA-NO        VALUE 'N'.
           03  SW-FINE-GRZ             PIC X.
               88 FINE-GRZ             VALUE 'S'.
               88 FINE-GRZ-NO          VALUE 'N'.
           03  SW-FINE-TGA             PIC X.
               88 FINE-TGA             VALUE 'S'.
               88 FINE-TGA-NO          VALUE 'N'.
           03  SW-RAMO-RICH            PIC X.
               88 RAMO-RICH-SI         VALUE 'S'.
               88 RAMO-RICH-NO         VALUE 'N'.
           03  SW-TROVATO              PIC X.
               88 TROVATO              VALUE 'S'.
               88 TROVATO-NO           VALUE 'N'.

           03  SW-6101                 PIC X.
               88 TROVATO-6101         VALUE 'S'.
               88 NO-TROVATO-6101      VALUE 'N'.

           03  SW-6002                 PIC X.
               88 TROVATO-6002         VALUE 'S'.
               88 NO-TROVATO-6002      VALUE 'N'.

29398      03  SW-6003                 PIC X.
29398          88 TROVATO-6003         VALUE 'S'.
29398          88 NO-TROVATO-6003      VALUE 'N'.

           03  SW-6009                 PIC X.
               88 TROVATO-6009         VALUE 'S'.
               88 NO-TROVATO-6009      VALUE 'N'.

           03  SW-TAB                  PIC X.
               88 TAB-RIS-OK           VALUE 'S'.
               88 TAB-RIS-KO           VALUE 'N'.

           03  SW-DFA                  PIC X.
               88 DFA-OK               VALUE 'S'.
               88 DFA-KO               VALUE 'N'.

           03  SW-OCO                  PIC X.
               88 OCO-OK               VALUE 'S'.
               88 OCO-KO               VALUE 'N'.

           03  SW-RISCATT              PIC X.
               88 RISCATT-OK           VALUE 'S'.
               88 RISCATT-KO           VALUE 'N'.

           03  SW-DTC1                 PIC X.
               88 DTC1-OK              VALUE 'S'.
               88 DTC1-KO              VALUE 'N'.

      *       262 -->  data esito titolo
           03  SW-DTC2                 PIC X.
               88 DTC2-OK              VALUE 'S'.
               88 DTC2-KO              VALUE 'N'.
      *       262 -->  data esito titolo

           03  SW-STB                  PIC X.
               88 STB-OK               VALUE 'S'.
               88 STB-KO               VALUE 'N'.

           03  SW-TRANCHE              PIC X.
               88 TRANCHE-TROVATA      VALUE 'S'.
               88 TRANCHE-NO-TROVATA   VALUE 'N'.

           03  SW-ST-ADES              PIC X.
               88 ADES-STORNATA        VALUE 'S'.
               88 ADES-NO-STORNATA     VALUE 'N'.

           03  SW-ST-GAR               PIC X.
               88 TRANCHE-ST           VALUE 'S'.
               88 TRANCHE-NO-ST        VALUE 'N'.

           03  SW-POG                  PIC X.
               88 POG-OK               VALUE 'S'.
               88 POG-KO               VALUE 'N'.

           03  FL-GAR-RAMO-III         PIC X(02) VALUE SPACES.
               88 SI-GAR-RAMO-III      VALUE 'SI'.
               88 NO-GAR-RAMO-III      VALUE 'NO'.

           03  SW-FINE-DISIN             PIC X.
               88 FINE-DISIN             VALUE 'S'.
               88 FINE-DISIN-NO          VALUE 'N'.

           03  SW-FINE-MOVI              PIC X.
               88 FINE-MOVI              VALUE 'S'.
               88 FINE-MOVI-NO           VALUE 'N'.

           03  SW-SAVE-TRANCHE           PIC X.
               88 SI-SAVE-TRANCHE        VALUE 'S'.
               88 NO-SAVE-TRANCHE        VALUE 'N'.

           03  SW-GAR-INS                PIC X.
               88 SI-GAR-INS             VALUE 'S'.
               88 NO-GAR-INS             VALUE 'N'.

           03  SW-VARIABILE              PIC X.
               88 SI-TROVATA-VARIABILE   VALUE 'S'.
               88 NO-TROVATA-VARIABILE   VALUE 'N'.

           03  SW-TITOLO                 PIC X.
               88 SI-TITOLO-COLLEGATO    VALUE 'S'.
               88 NO-TITOLO-COLLEGATO    VALUE 'N'.

           03  SW-VAL-AST                PIC X.
               88 SI-VAL-AST             VALUE 'S'.
               88 NO-VAL-AST             VALUE 'N'.

           03  SW-CUR-VAS                PIC X.
               88 INIT-CUR-VAS           VALUE 'S'.
               88 FINE-CUR-VAS           VALUE 'N'.

           03  SW-TP-TRCH                PIC X.
               88 TRANCHE-POSITIVA       VALUE 'S'.
               88 TRANCHE-NEGATIVA       VALUE 'N'.

           03  SW-ZERO-OPZIONI           PIC X.
               88 ZERO-OPZIONI-OK        VALUE 'S'.
               88 ZERO-OPZIONI-KO        VALUE 'N'.

      *       261 -->  Costi di emissione
           03  SW-DIR                    PIC X.
               88 DIR-OK                 VALUE 'S'.
               88 DIR-KO                 VALUE 'N'.
      *       261 -->  Costi di emissione

      * FETCH RAPP-ANA
          03 FINE-FETCH                  PIC X.
              88 FINE-FETCH-RAN-SI           VALUE 'S'.
              88 FINE-FETCH-RAN-NO           VALUE 'N'.
      *--------------------------------------------------------------*
      *    AREA CALCOLO DIFFERENZA TRA DATE                          *
      *--------------------------------------------------------------*
      *
       01  FORMATO                          PIC X(01).
       01  DATA-INFERIORE.
           05 AAAA-INF                      PIC 9(04).
           05 MM-INF                        PIC 9(02).
           05 GG-INF                        PIC 9(02).
       01  DATA-SUPERIORE.
           05 AAAA-SUP                      PIC 9(04).
           05 MM-SUP                        PIC 9(02).
           05 GG-SUP                        PIC 9(02).
       01  GG-DIFF                          PIC 9(05).
       01  CODICE-RITORNO                   PIC X(01).
       01  DURATA.
           05 DUR-AAAA                      PIC 9(04).
           05 DUR-MM                        PIC 9(02).
           05 DUR-GG                        PIC 9(02).
      *
      *--> AREA PER GESTIONE RESTI OPERAZIONI
      *
       01  WK-OPERAZIONI.
           03 WK-APPO-RESTO1                   PIC S9(5)V9(5).
           03 WK-APPO-RESTO2                   PIC S9(5)V9(5).
      *
       01  WK-VARIABILI.
           03 WK-APPO-LUNGHEZZA              PIC  9(09).
      *
       01 WK-GESTIONE-MSG-ERR.
           03 WK-LABEL-ERR                   PIC X(50).
           03 WK-STRING                      PIC X(85).
      *
       01  WK-TABELLA                        PIC  X(20).
      *
       01 WS-TIMESTAMP                       PIC X(18).
       01 WS-TIMESTAMP-NUM REDEFINES
            WS-TIMESTAMP                     PIC 9(18).

       01 WS-DATA-TS                         PIC X(18).
       01 WS-DATA-TS-NUM   REDEFINES
            WS-DATA-TS                       PIC 9(18).
      *
       01 WK-APPO-DATA                       PIC 9(08).
MIGCOL 01 WK-NOME-PGM                        PIC X(08) VALUE SPACES.
      *
      * COPY PER TESTARE IL TIPO DATO DI RITORNO DAI SERVIZI.
      *
           COPY LCCC0006.
           COPY LCCVXSB0.
           COPY LCCVXRA0.
           COPY LCCVXOG0.
10819 *    COPY LCCVXMV0.
10819      COPY LCCVXMVZ.
10819      COPY LCCVXMV1.
10819      COPY LCCVXMV2.
10819      COPY LCCVXMV3.
10819      COPY LCCVXMV4.
10819      COPY LCCVXMV5.
10819      COPY LCCVXMV6.
10819      COPY LCCVXMV7.
10819      COPY LCCVXMV8.
10819      COPY LCCVXMV9.
10819      COPY LCCVXMVA.
10819      COPY LCCVXMVB.
10819      COPY LCCVXMVC.
           COPY LCCVXFA0.
           COPY LCCVXDA0.
           COPY LCCVXCA0.
           COPY LCCVXTH0.
      *
       01 WCOM-FLAG-OVERFLOW                   PIC X(002).
          88 WCOM-OVERFLOW-YES                 VALUE 'SI'.
          88 WCOM-OVERFLOW-NO                  VALUE 'NO'.

      *   FLAG PER VERIFICARE SE E' STATA EFFETTUATA LA LETTURA SULLA
      *   RAPPORTO ANAGRAFICO DELL'ASSICURATO
       01 WS-FL-ASSICURATO                     PIC X(001) VALUE 'N'.
          88 WS-FL-ASSIC-PRES                  VALUE 'S'.
          88 WS-FL-ASSIC-NON-PRES              VALUE 'N'.

      *   FLAG PER VERIFICARE SE E' STATA EFFETTUATA LA LETTURA SULLA
      *   RAPPORTO ANAGRAFICO DELL'ADERENTE
       01 WS-FL-ADERENTE                       PIC X(001) VALUE 'N'.
          88 WS-FL-ADER-PRES                   VALUE 'S'.
          88 WS-FL-ADER-NON-PRES               VALUE 'N'.

ALFR  *   FLAG PER VERIFICARE LA PRESENZA DEL CAMPO PREMIO TOTALE SULLA
      *   DETTAGLIO TITOLO CONTABILE
       01 WS-FL-PREMIO-TOTALE                  PIC X(001) VALUE 'S'.
          88 WK-FOUND-PRM-TOT                  VALUE 'S'.
          88 WK-NOT-FOUND-PRM-TOT              VALUE 'N'.

       01 WS-DT-CALCOLO                         PIC X(08).
       01 WS-DT-CALCOLO-NUM   REDEFINES
             WS-DT-CALCOLO                      PIC 9(08).

       01  WS-DATA-APPO-NUM                 PIC 9(008).
       01  WS-DT-QUOT                       PIC 9(008).

       01 WS-COD-FND                        PIC X(020).

       01 WS-DATA-APPO-AAAAMMGG.
          03 WS-DATA-APPO-AA                PIC 9(004).
          03 WS-DATA-APPO-MM                PIC 9(002).
          03 WS-DATA-APPO-GG                PIC 9(002).

       01 WS-FL-SCARTA-ADES                 PIC X(001) VALUE 'N'.
          88 WS-FL-SCARTA-ADE-SI            VALUE 'S'.
          88 WS-FL-SCARTA-ADE-NO            VALUE 'N'.

VSTEF  01 WS-ETA-RAGGN-DT-CALC           PIC S9(4)V9(3).
VSTEF  01 WS-ETA-RAGGN-DT-CALC-V         REDEFINES WS-ETA-RAGGN-DT-CALC.
          03 WS-ETA-RAGGN-DT-CALC-NUM    PIC 9(04).
          03 WS-ETA-RAGGN-DT-CALC-DEC    PIC 9(03).


       01 WCOM-DATA-DISINVESTIMENTO     PIC S9(8) COMP-3.

       01 WCOM-DISINVESTIMENTO          PIC X(1).
           88 WCOM-PRE-DISINVESTIMENTO  VALUE '0'.
           88 WCOM-POST-DISINVESTIMENTO VALUE '1'.


      *-->  AREA GARANZIA
       01 AREA-GARANZIE-STORNATE.
           03 WSTR-AREA-GARANZIA.
              04 WSTR-ELE-GRZ-MAX      PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WSTR==.
              COPY LCCVGRZ1            REPLACING ==(SF)==
                                       BY ==WSTR==.

       01 AREA-TRANCHE-STORNATE.
           02 WTGS-AREA-TGS.
              03 WTGS-ELE-TGS-MAX      PIC S9(04) COMP.
              03 WTGS-TAB-TGS1.
                 COPY LCCVTGAC REPLACING   ==(SF)==  BY ==WTGS==.
                    07 WTGS-ID-TRCH-DI-GAR  PIC S9(9)     COMP-3.
                    07 WTGS-ID-GAR          PIC S9(9)     COMP-3.
              03 WTGS-TAB-TGS1-R REDEFINES WTGS-TAB-TGS1.
                 05 FILLER                  PIC X(10).
                 05 WTGS-RESTO-TAB-TGS1     PIC X(12490).


      *--> AREA DI COMUNICAZIONE CON LA ROUTINE CALCOLO DATA
           COPY LCCC0003.
       01  IN-RCODE                        PIC 9(2).

      ******************************************************************
      *    TP_QTZ (Tipo Quotazione)
      ******************************************************************
           COPY LCCVL420.
      *----------------------------------------------------------------*
      *     AREA WORKING TABELLE DB2
      *----------------------------------------------------------------*
       01  WORK-COMMAREA.
      *--  AREA POLIZZA
           02 WPOL-AREA-POLIZZA.
              04 WPOL-ELE-POL-MAX        PIC S9(04) COMP.
              04 WPOL-TAB-POL.
              COPY LCCVPOL1              REPLACING ==(SF)== BY ==WPOL==.
      *-->  AREA ADESIONE
           02 WADE-AREA-ADESIONE.
              04 WADE-ELE-ADE-MAX        PIC S9(04) COMP.
              04 WADE-TAB-ADE            OCCURS 1.
              COPY LCCVADE1              REPLACING ==(SF)== BY ==WADE==.
      *-->  AREA GARANZIA
           02 WGRZ-AREA-GARANZIA.
              04 WGRZ-ELE-GRZ-MAX        PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
              COPY LCCVGRZ1              REPLACING ==(SF)== BY ==WGRZ==.
      *-->  AREA TRANCHE DI GARANZIA
           02 WTGA-AREA-TGA.
              04 WTGA-ELE-TGA-MAX        PIC S9(04) COMP.
            03 WTGA-TAB.
                COPY LCCVTGAC REPLACING   ==(SF)==  BY ==WTGA==.
              COPY LCCVTGA1              REPLACING ==(SF)== BY ==WTGA==.
            03 WTGA-TAB-R                REDEFINES
               WTGA-TAB.
              04 FILLER                  PIC X(911).
              04 WTGA-RESTO-TAB          PIC X(1137839).
      *-->  AREA TRANCHE DI GARANZIA X LCCS0450
           02 ATGA-AREA-TGA.
              04 ATGA-ELE-TGA-MAX        PIC S9(04) COMP.
            03 ATGA-TAB.
                COPY LCCVTGAC REPLACING   ==(SF)==  BY ==ATGA==.
              COPY LCCVTGA1              REPLACING ==(SF)== BY ==ATGA==.
            03 ATGA-TAB-R                REDEFINES
               ATGA-TAB.
              04 FILLER                  PIC X(911).
              04 ATGA-RESTO-TAB          PIC X(1137839).
      *--  AREA STATO OGGETTO BUSINESS TRANCHES
           02 WSTB-AREA-STB.
              04 WSTB-ELE-STB-MAX        PIC S9(04) COMP.
            03 WSTB-TAB.
                COPY LCCVSTBB REPLACING   ==(SF)==  BY ==WSTB==.
              COPY LCCVSTB1              REPLACING ==(SF)== BY ==WSTB==.
            03 WSTB-TAB-R                REDEFINES
               WSTB-TAB.
              04 FILLER                  PIC X(98).
              04 WSTB-RESTO-TAB          PIC X(127302).
      *--> AREA TITOLO CONTABILE
           02 WTIT-AREA-TIT.
              04 WTIT-ELE-TIT-MAX        PIC S9(04) COMP.
                COPY LCCVTITA REPLACING   ==(SF)==  BY ==WTIT==.
              COPY LCCVTIT1              REPLACING ==(SF)== BY ==WTIT==.
      *--> AREA DETTAGLIO TITOLO CONTABILE
           02 WDTC-AREA-DTC.
              04 WDTC-ELE-DTC-MAX        PIC S9(04) COMP.
                COPY LCCVDTCA REPLACING   ==(SF)==  BY ==WDTC==.
              COPY LCCVDTC1              REPLACING ==(SF)== BY ==WDTC==.
      *--> AREA PARAMETRO MOVIMENTO
           02 WPMO-AREA-PMO.
              04 WPMO-ELE-PMO-MAX        PIC S9(04) COMP.
            03 WPMO-TAB.
                COPY LCCVPMOB REPLACING   ==(SF)==  BY ==WPMO==.
           COPY LCCVPMO1                 REPLACING ==(SF)== BY ==WPMO==.
            03 WPMO-TAB-R               REDEFINES
               WPMO-TAB.
              04 FILLER                  PIC X(323).
              04 WPMO-RESTO-TAB          PIC X(31977).
      *--> AREA LIQUIDAZIONE
           02 WLQU-AREA-LQU.
              04 WLQU-ELE-LQU-MAX        PIC S9(04) COMP.
                COPY LCCVLQUB REPLACING   ==(SF)==  BY ==WLQU==.
           COPY LCCVLQU1                 REPLACING ==(SF)== BY ==WLQU==.
      *--> AREA RISERVA DI TRANCHE
           02 WRST-AREA-RST.
              04 WRST-ELE-RST-MAX        PIC S9(04) COMP.
              04 WRST-TAB-RST            OCCURS 10.
           COPY LCCVRST1                 REPLACING ==(SF)== BY ==WRST==.
      *--> AREA MOVIMENTO
           02 WMOV-AREA-MOV.
              04 WMOV-ELE-MOV-MAX        PIC S9(04) COMP.
              04 WMOV-TAB-MOV            OCCURS 10.
           COPY LCCVMOV1                 REPLACING ==(SF)== BY ==WMOV==.
      *--> AREA DATI FISCALI ADESIONE
           02 WDFA-AREA-DFA.
              04 WDFA-ELE-DFA-MAX        PIC S9(04) COMP.
              04 WDFA-TAB-DFA            OCCURS 10.
           COPY LCCVDFA1                 REPLACING ==(SF)== BY ==WDFA==.
      *--> AREA RAPPORTO ANAGRAFICO
      *    02 WRAN-AREA-RAN.
      *       04 WRAN-ELE-RAN-MAX        PIC S9(04) COMP.
      *       04 WRAN-TAB-RAN.
      *    COPY LCCVRAN1                 REPLACING ==(SF)== BY ==WRAN==.
      *--> AREA PARAMETRO OGGETTO
           02 WPOG-AREA-POG.
              04 WPOG-ELE-POG-MAX        PIC S9(04) COMP.
                COPY LCCVPOGA REPLACING   ==(SF)==  BY ==WPOG==.
           COPY LCCVPOG1                 REPLACING ==(SF)== BY ==WPOG==.
      *--> AREA RAPPORTO RETE
           02 WRRE-AREA-RRE.
              04 WRRE-ELE-RRE-MAX        PIC S9(04) COMP.
              04 WRRE-TAB-RRE            OCCURS 10.
           COPY LCCVRRE1                 REPLACING ==(SF)== BY ==WRRE==.
      *--  AREA GARANZIA
           02 WL19-AREA-FONDI.
              COPY LCCVL197              REPLACING ==(SF)== BY ==WL19==.
              COPY LCCVL191              REPLACING ==(SF)== BY ==WL19==.
              03 WL19-TABELLA1           REDEFINES
                 WL19-TABELLA.
                 04 WL19-EL-TABELLA      OCCURS 250  PIC X(86).
              03 WL19-TABELLA-R          REDEFINES
                 WL19-TABELLA.
                 04 FILLER               PIC X(86).
                 04 WL19-RESTO-TABELLA   PIC X(21414).
ITRFO *--  AREA ACCORDO COMMERCIALE.
ITRFO      02 WP63-ACC-COMM.
ITRFO         04 WP63-ELE-ACC-COMM-MAX   PIC S9(004) COMP.
ITRFO         04 WP63-TAB-ACC-COMM.
ITRFO         COPY LCCVP631              REPLACING ==(SF)== BY ==WP63==.
ITRFO *--  AREA ESTENSIONE POLIZZA CPI E PR
ITRFO      02 WP67-TAB-EST-POLI-CPI-PR.
ITRFO         04 WP67-ELE-TAB-EST-POLI-CPI-MAX PIC S9(004) COMP.
ITRFO         04 WP67-TAB-EST-POLI-CPI-PR.
ITRFO         COPY LCCVP671              REPLACING ==(SF)== BY ==WP67==.
      *--  AREA PAGINA
           02 WCOM-AREA-PAGINA.
              COPY LSTC0072              REPLACING ==(SF)== BY ==WPAG==.
      *--  AREA COMUNE
           02 WCOM-AREA-STATI.
              COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.
      *--> PARAMETRO_COMPAGNIA
           02 WPCO-AREA-PARA-COM.
              04 WPCO-ELE-PARA-COM-MAX   PIC 9(04) COMP.
              04 WPCO-TAB-PARA-COM.
              COPY LCCVPCO1              REPLACING ==(SF)== BY ==WPCO==.
      *----------------------------------------------------------------*
      *     Copy per il valorizzatore variabile
      *----------------------------------------------------------------*
      *--> AREA SCHEDA PER GESTIONE OUTPUT VALORIZZATORE VARIABILI
       01  WSKD-AREA-SCHEDA.
           COPY IVVC0216                REPLACING ==(SF)== BY ==WSKD==.
      *--> AREA OPZIONI
       01  WOPZ-AREA-OPZIONI.
           COPY IVVC0217                REPLACING ==(SF)== BY ==WOPZ==.
      *--> AREA DATI CONTESTUALI
       01  WCNT-AREA-DATI-CONTEST.
           COPY IVVC0212                REPLACING ==(SF)== BY ==WCNT==.
      *--> AREA IO VALORIZZATORE VARIABILE
       01  AREA-IO-IVVS0211.
           COPY IVVC0211                REPLACING ==(SF)== BY ==S211==.
      *--> AREA ALIAS
       01  AREA-ALIAS.
           COPY IVVC0218                REPLACING ==(SF)== BY ==S211==.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01   AREA-IDSV0001.
            COPY IDSV0001.
       01   IABV0006.
            COPY IABV0006.
      *--> AREA ADESIONE
            COPY IDBVADE1.
      *--> AREA POLIZZA
            COPY IDBVPOL1.
      *--> AREA STATO OGGETTO BUSINESS
            COPY IDBVSTB1.
      *--> AREA BUFFER DI PRENOTAZIONE
            COPY LLBO0261.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                IABV0006
                                ADES
                                POLI
                                STAT-OGG-BUS
                                WLB-REC-PREN.
      *----------------------------------------------------------------*

           SET IDSV8888-DEBUG-BASSO         TO TRUE
           MOVE '------ INIZIO BUSINESS SERVICE LLBS0230  ------'
                                    TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

            IF IABV0006-ULTIMO-LANCIO
               PERFORM S1500-CLOSE-FILEOUT
                  THRU EX-S1500
            ELSE
               PERFORM S0000-OPERAZIONI-INIZIALI
                  THRU EX-S0000

               IF IDSV0001-ESITO-OK
                  PERFORM S0100-ELABORAZIONE
                     THRU EX-S0100
               END-IF

               PERFORM S9000-OPERAZIONI-FINALI
                  THRU EX-S9000
           END-IF.

           SET IDSV8888-DEBUG-BASSO         TO TRUE
           MOVE '------ FINE BUSINESS SERVICE LLBS0230  ------'
                                    TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           GOBACK.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.
      *
           MOVE 'S0000-OPERAZIONI-INIZIALI' TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY           THRU ESEGUI-DISPLAY-EX.

           SET  IDSV8888-BUSINESS-DBG       TO TRUE
           MOVE 'LLBS0230'                  TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO             TO TRUE
           MOVE SPACES                      TO IDSV8888-DESC-PGM
           STRING 'Iniz. area di working'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY           THRU ESEGUI-DISPLAY-EX.

           INITIALIZE                       WGRZ-AREA-GARANZIA
                                            WADE-AREA-ADESIONE
                                            WTIT-AREA-TIT
                                            WDTC-AREA-DTC
                                            WLQU-AREA-LQU
                                            WRST-AREA-RST
                                            WMOV-AREA-MOV
                                            WDFA-AREA-DFA
      *                                     WRAN-AREA-RAN
                                            WK-RANPERS
                                            WPOG-AREA-POG
                                            WRRE-AREA-RRE
                                            WCOM-AREA-PAGINA
                                            WCOM-AREA-STATI
                                            WTGA-ELE-TGA-MAX
                                            ATGA-ELE-TGA-MAX
                                            WSTB-ELE-STB-MAX
                                            WPMO-ELE-PMO-MAX
                                            WL19-ELE-FND-MAX
                                            IX-INDICI
                                            WS-STAT-CAUS-POL
                                            WS-STAT-CAUS-ADE
                                            WS-STAT-CAUS-TGA
                                            AREA-GARANZIE-STORNATE
                                            WTGS-ELE-TGS-MAX
                                            WK-TAB-CAUS-ELE-MAX.

      *--  INIZIALIZZAZIONE AREA TRANCHE STORNATE

           INITIALIZE             WTGS-TAB-TRAN(1).

           MOVE  WTGS-TAB-TGS1    TO WTGS-RESTO-TAB-TGS1.
      *
      *--  INIZIALIZZAZIONE AREA TABELLA CAUSALI
      *
           INITIALIZE             WK-TAB-TRAN(1).

           MOVE  WK-TAB-CAUS1     TO WK-RESTO-TAB-CAUS1.

      *--  INIZIALIZZAZIONE AREA TRANCHE DI GARANZIA

           INITIALIZE             WTGA-TAB-TRAN(1).

           MOVE  WTGA-TAB         TO WTGA-RESTO-TAB.

      *--  INIZIALIZZAZIONE AREA TRANCHE DI GARANZIA X LCCS0450

           INITIALIZE             ATGA-TAB-TRAN(1).

           MOVE  ATGA-TAB         TO ATGA-RESTO-TAB.

      *--  INIZIALIZZAZIONE AREA STATO OGGETTO BUSINESS TRANCHES

           INITIALIZE             WSTB-TAB-STAT-OGG(1).

           MOVE  WSTB-TAB         TO WSTB-RESTO-TAB.

      *-- INIZIALIZZAZIONE AREA PARAMETRO MOVIMENTO

           INITIALIZE             WPMO-TAB-PARAM-MOV(1).

           MOVE  WPMO-TAB         TO WPMO-RESTO-TAB.

      *-- INIZIALIZZAZIONE AREA QUOTAZ FONDI UNIT

           INITIALIZE             WL19-TAB-FND(1).

           MOVE  WL19-TABELLA     TO WL19-RESTO-TABELLA.

           SET  SCARTA-GAR-NO               TO TRUE.
           SET  SCARTA-TGA-NO               TO TRUE.
           SET  RAMO-RICH-SI                TO TRUE.
           SET  FINE-GRZ-NO                 TO TRUE.
           SET  FINE-TGA-NO                 TO TRUE.
           SET  TROVATO-NO                  TO TRUE.
           SET  ADES-NO-STORNATA            TO TRUE.
           SET  TRANCHE-ST                  TO TRUE.
           SET  IDSV0001-ESITO-OK           TO TRUE.
           SET  WS-FL-SCARTA-ADE-NO         TO TRUE.
           SET  NO-GAR-RAMO-III             TO TRUE

           ACCEPT WS-TIMESTAMP(1:8)         FROM DATE YYYYMMDD.
           ACCEPT WS-TIMESTAMP(9:6)         FROM TIME.

           MOVE WLB-DT-RISERVA              TO WS-DATA-RISERVA-COMP
                                               WS-DT-CALCOLO.

      *--> MODIFICA PER CORRETTA VALORIZZAZIONE DELLA VARIABILE
      *--> DI PRODOTTO "DEE"
           MOVE WLB-DT-RISERVA              TO IDSV0001-DATA-EFFETTO

           MOVE WLB-DT-RISERVA              TO WS-DT-N.
           MOVE WS-DT-N-AA                  TO WS-DT-X-AA.
           MOVE WS-DT-N-MM                  TO WS-DT-X-MM.
           MOVE WS-DT-N-GG                  TO WS-DT-X-GG.
           MOVE WS-DT-X                     TO WS-DT-PTF-X.

      *--> DATA EFFETTO UTILIZZATA NELLE QUERY
           MOVE WLB-DT-PRODUZIONE           TO WS-DT-EFFETTO

      *--> DATA COMPETENZA UTILIZZATA NELLE QUERY
           MOVE WLB-TS-COMPETENZA           TO WS-DT-TS-PTF
                                               IDSV0001-DATA-COMPETENZA.


      *--> VALORIZZAZIONI PER REPORT DEL BATCH
           MOVE ADE-ID-ADES                 TO IABV0006-OGG-BUSINESS.
           MOVE 'AD'                        TO IABV0006-TP-OGG-BUSINESS.
           MOVE POL-IB-OGG                  TO IABV0006-IB-OGG-POLI.
           MOVE ADE-IB-OGG                  TO IABV0006-IB-OGG-ADES.
           MOVE POL-ID-POLI                 TO IABV0006-ID-POLI.
           MOVE ADE-ID-ADES                 TO IABV0006-ID-ADES.
           MOVE IDSV0001-DATA-EFFETTO       TO IABV0006-DT-EFF-BUSINESS.

           IF IABV0006-PRIMO-LANCIO
              PERFORM S1490-OPEN-FILEOUT
                 THRU EX-S1490
           END-IF.

           PERFORM INIZIA-TOT-POL
              THRU INIZIA-TOT-POL-EX.

           MOVE 1 TO WPOL-ELE-POL-MAX
           PERFORM VALORIZZA-OUTPUT-POL
              THRU VALORIZZA-OUTPUT-POL-EX.

           MOVE 1 TO IX-TAB-ADE.
           PERFORM INIZIA-TOT-ADE
              THRU INIZIA-TOT-ADE-EX.

           MOVE 1 TO WADE-ELE-ADE-MAX
           PERFORM VALORIZZA-OUTPUT-ADE
              THRU VALORIZZA-OUTPUT-ADE-EX.

           MOVE STB-TP-STAT-BUS             TO WS-TP-STAT-BUS-ADE.
           MOVE STB-TP-CAUS                 TO WS-TP-CAUS-ADE.

           SET  IDSV8888-BUSINESS-DBG       TO TRUE
           MOVE 'LLBS0230'                  TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE               TO TRUE
           MOVE SPACES                      TO IDSV8888-DESC-PGM
           STRING 'Iniz. area di working'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY           THRU ESEGUI-DISPLAY-EX.

           PERFORM S0010-CTRL-STAT-POL
              THRU EX-S0010.

       EX-S0000.
           EXIT.
      * ----------------------------------------------------------------
      * Controllo che la polizza in esame sia in vigore
      * ----------------------------------------------------------------
       S0010-CTRL-STAT-POL.

           PERFORM S0011-PREPARA-ACCESSO-STB
              THRU EX-S0011.
           PERFORM S0012-LEGGI-STB
              THRU EX-S0012.

       EX-S0010.
           EXIT.
      * ----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella STAT_OGG_BUS
      * ----------------------------------------------------------------
       S0011-PREPARA-ACCESSO-STB.

           MOVE POL-ID-POLI              TO STB-ID-OGG.
           MOVE 'PO'                     TO STB-TP-OGG.
           MOVE 'VI'                     TO STB-TP-STAT-BUS.

      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
           MOVE WS-DT-EFFETTO         TO IDSI0011-DATA-INIZIO-EFFETTO
                                          IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF           TO IDSI0011-DATA-COMPETENZA.
      *--> NOME TABELLA FISICA DB
           MOVE 'STAT-OGG-BUS'             TO IDSI0011-CODICE-STR-DATO.
      *--> DCLGEN TABELLA
PERFOR*    MOVE SPACES                     TO IDSI0011-BUFFER-DATI.
           MOVE STAT-OGG-BUS               TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT              TO TRUE.
           SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.
           SET IDSI0011-ID-OGGETTO          TO TRUE.
      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL      TO TRUE.

       EX-S0011.
           EXIT.
      * ----------------------------------------------------------------
      * Chiamata al dispatcher dati per l'accesso alla STAT_OGG_BUS
      * ----------------------------------------------------------------
       S0012-LEGGI-STB.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IDBSSTB0'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett. STAT_OGG_BUS'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER         THRU CALL-DISPATCHER-EX

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IDBSSTB0'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett. STAT_OGG_BUS'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *--> CHIAVE NON TROVATA
                       MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'SS0012-LEGGI-STB'
                                      TO IEAI9901-LABEL-ERR
                       MOVE '005056'  TO IEAI9901-COD-ERRORE
                       MOVE 'STAT-OGG-BUS;'
                         TO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI
                         TO STAT-OGG-BUS
                       MOVE STB-TP-STAT-BUS
                         TO WS-TP-STAT-BUS-POL
                       MOVE STB-TP-CAUS
                         TO WS-TP-CAUS-POL
                      WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'SS0012-LEGGI-STB'
                                          TO IEAI9901-LABEL-ERR
                           MOVE '005016'  TO IEAI9901-COD-ERRORE
                           STRING 'STAT-OGG-BUS;'
                                  IDSO0011-RETURN-CODE ';'
                                  IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      *--> ERRORE DISPATCHER

                  MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S0012-LEGGI-STB'
                                          TO IEAI9901-LABEL-ERR
                  MOVE '005016'           TO IEAI9901-COD-ERRORE
                  STRING 'STAT-OGG-BUS;'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                      DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF.

       EX-S0012.
           EXIT.
      * ----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella STAT_OGG_BUS
      * ----------------------------------------------------------------
ALEX1  S0013-PREPARA-ACCESSO-STB.

      *    MOVE 'ST'                     TO STB-TP-STAT-BUS.

      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
           MOVE WS-DT-EFFETTO          TO IDSI0011-DATA-INIZIO-EFFETTO
                                          IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF           TO IDSI0011-DATA-COMPETENZA.
      *--> NOME TABELLA FISICA DB
           MOVE 'LDBSC580'             TO IDSI0011-CODICE-STR-DATO.
      *--> DCLGEN TABELLA
           MOVE STAT-OGG-BUS               TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT              TO TRUE.
           SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.
           SET IDSI0011-WHERE-CONDITION     TO TRUE.
      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL      TO TRUE.

       EX-S0013.
           EXIT.
      * ----------------------------------------------------------------
      * Chiamata al dispatcher dati per l'accesso alla STAT_OGG_BUS
      * ----------------------------------------------------------------
ALEX1  S0014-LEGGI-STB.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBSC580'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett. STAT_OGG_BUS'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER         THRU CALL-DISPATCHER-EX

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBSC580'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett. STAT_OGG_BUS'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *--> CHIAVE NON TROVATA
                       CONTINUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI TO STAT-OGG-BUS
                       SET STORNATO     TO TRUE
                       IF STB-TP-STAT-BUS = WS-TP-STAT-BUS
                          MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                          MOVE 'S0014-LEGGI-STB'
                                         TO IEAI9901-LABEL-ERR
      *--> ERRORE DI GRAVITA'  2
                          MOVE '005247'  TO IEAI9901-COD-ERRORE
                        STRING 'GARANZIA RAMO I,V'
                            ' STORNATA CON DATA EFFETTO > DATA CALCOLO'
                        DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                        END-STRING
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
                        SET   WK-DA-SCARTARE-SI       TO TRUE
                       END-IF
                  WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'SS0014-LEGGI-STB'
                                          TO IEAI9901-LABEL-ERR
                           MOVE '005016'  TO IEAI9901-COD-ERRORE
                           STRING 'STAT-OGG-BUS;'
                                  IDSO0011-RETURN-CODE ';'
                                  IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> ERRORE DISPATCHER

              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0014-LEGGI-STB'
                                      TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              STRING 'STAT-OGG-BUS;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S0014.
           EXIT.
      * ----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella STAT_OGG_BUS
      * ----------------------------------------------------------------
ALEX1  S0015-PREPARA-ACCESSO-STB.

           MOVE L3421-ID-TRCH-DI-GAR    TO LC601-ID-TRCH-DI-GAR
           MOVE WK-MOV-ID-MOVI          TO LC601-ID-MOVI-CRZ

      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
           MOVE WS-DT-TS-PTF           TO IDSI0011-DATA-COMPETENZA.
      *--> NOME TABELLA FISICA DB
           MOVE 'LDBSC600'             TO IDSI0011-CODICE-STR-DATO.
      *--> DCLGEN TABELLA
           MOVE LDBVC601               TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT              TO TRUE.
           SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.
           SET IDSI0011-WHERE-CONDITION     TO TRUE.
      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL      TO TRUE.

       EX-S0015.
           EXIT.
      * ----------------------------------------------------------------
      * Chiamata al dispatcher dati per l'accesso alla STAT_OGG_BUS
      * ----------------------------------------------------------------
ALEX1  S0016-LEGGI-STB.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBSC600'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett. STAT_OGG_BUS'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER         THRU CALL-DISPATCHER-EX

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBSC600'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett. STAT_OGG_BUS'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *--> CHIAVE NON TROVATA
                     CONTINUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                       SET STORNATO     TO TRUE
                       IF LC601-TP-STAT-BUS = WS-TP-STAT-BUS
                         MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                         MOVE 'SS0016-LEGGI-STB'
                                      TO IEAI9901-LABEL-ERR
      *--> ERRORE DI GRAVITA' 2
                        MOVE '005247'  TO IEAI9901-COD-ERRORE
                        STRING 'TRANCHE DI GARANZIA RAMO I,V'
                            ' STORNATA CON DATA EFFETTO > DATA CALCOLO'
                        DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                        END-STRING
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
                        SET   WK-DA-SCARTARE-SI       TO TRUE
                       ELSE
                         MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                         MOVE 'SS0016-LEGGI-STB'
                                      TO IEAI9901-LABEL-ERR
      *--> ERRORE DI GRAVITA' 2
                        MOVE '005247'  TO IEAI9901-COD-ERRORE
                        STRING 'TRANCHE DI GARANZIA RAMO I,V'
                            ' AGGIORNATA DA UN MOVIMENTO DI RISCATTO '
                            'PARZIALE FUTURO'
                        DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                        END-STRING
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *                  MOVE IDSO0011-BUFFER-DATI TO LDBVC601
      *                  MOVE LDBVC601-TGA-OUTPUT TO LDBV3421-TGA-OUTPUT
                       END-IF
                  WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'SS0016-LEGGI-STB'
                                          TO IEAI9901-LABEL-ERR
                           MOVE '005016'  TO IEAI9901-COD-ERRORE
                           STRING 'STAT-OGG-BUS;'
                                  IDSO0011-RETURN-CODE ';'
                                  IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> ERRORE DISPATCHER

              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0016-LEGGI-STB'
                                      TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              STRING 'STAT-OGG-BUS;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S0016.
           EXIT.
      *-----------------------------------------------------------------
      * La routine verifica l'esistenza di almeno una garanzia relativa
      * ad uno o piu' rami assicurativi di cui sia stata richiesta la
      * riserva matematica (solo in caso di estrazione massiva).
      * In caso di esistenza viene impostato a OK lo switch rami-rich,
      * altrimenti vie bloccata l'elaborazione della adesione in esame
      *-----------------------------------------------------------------
       S0020-CNTL-RAMO.

           MOVE 'S0020-CNTL-RAMO'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           PERFORM S0021-PREPARA-CALL-LDBS3400
              THRU EX-S0021.

           PERFORM S0022-CALL-LDBS3400
              THRU EX-S0022.

           MOVE IX-TAB-STR
             TO WSTR-ELE-GRZ-MAX.
           MOVE IX-TAB-GRZ
             TO WGRZ-ELE-GRZ-MAX.

       EX-S0020.
           EXIT.
      * ----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella GAR
      * ----------------------------------------------------------------
       S0021-PREPARA-CALL-LDBS3400.

           MOVE 'S0021-PREPARA-CALL-LDBS3400'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE LDBV3401.

           MOVE WPOL-ID-POLI
             TO LDBV3401-ID-POLI.
           MOVE WADE-ID-ADES (IX-TAB-ADE)
             TO LDBV3401-ID-ADES.
           MOVE 'GA'
             TO LDBV3401-TP-OGG.

           MOVE 'VI'                      TO LDBV3401-TP-STAT-BUS-1
           MOVE 'ST'                      TO LDBV3401-TP-STAT-BUS-2

      *
      * DA DEFINIRE I CAMPI PROVENIENTI DA PRENOTAZIONE

           MOVE WLB-RAMO-BILA
             TO LDBV3401-RAMO1.

           MOVE WS-DT-EFFETTO
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO.
           MOVE WS-DT-TS-PTF
             TO IDSI0011-DATA-COMPETENZA.
           MOVE 'LDBS3400'
             TO IDSI0011-CODICE-STR-DATO.
           MOVE SPACES
             TO IDSI0011-BUFFER-WHERE-COND.
           MOVE LDBV3401
             TO IDSI0011-BUFFER-DATI.
            SET IDSI0011-WHERE-CONDITION
             TO TRUE.
            SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
            SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
            SET IDSI0011-TRATT-X-COMPETENZA
             TO TRUE.
            SET IDSI0011-FETCH-FIRST
             TO TRUE.

       EX-S0021.
           EXIT.
      * ----------------------------------------------------------------
      * CHIAMATA AL MODULO DI VERIFICA GARANZIE PER ESTRAZIONE MASSIVA
      * ----------------------------------------------------------------
       S0022-CALL-LDBS3400.

           MOVE 'S0022-CALL-LDBS3400'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE 0
             TO IX-TAB-GRZ.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS3400'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura tab. GAR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSV0001-ESITO-OK
                      OR NOT FINE-GRZ-NO


               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *--> CHIAVE NON TROVATA
                           SET FINE-GRZ
                            TO TRUE
                           IF IDSI0011-FETCH-FIRST
                               SET RAMO-RICH-NO
                                TO TRUE
                               MOVE WK-PGM   TO IEAI9901-COD-SERVIZIO-BE
                               MOVE 'S0022-CALL-LDBS3400'
                                              TO IEAI9901-LABEL-ERR
23638 *                        MOVE '005056'  TO IEAI9901-COD-ERRORE
23638 *                        MOVE 'LDBS3400'
23638 *                          TO IEAI9901-PARAMETRI-ERR
23638                          MOVE '005166' TO IEAI9901-COD-ERRORE
23638               STRING 'LDBS3400 - NON CI SONO GARANZIE DA '
23638                      'ELABORARE PER IL RAMO SELEZIONATO'
23638               DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                               PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                  THRU EX-S0300
                           END-IF
                      WHEN IDSO0011-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                           MOVE IDSO0011-BUFFER-DATI
                             TO LDBV3401
      *                    SET IDSI0011-FETCH-NEXT
      *                      TO TRUE
                           MOVE L3401-TP-STAT-BUS
                             TO WS-TP-STAT-BUS
                               PERFORM S0250-VAL-OUT-GRZ
                                  THRU EX-S0250
                      WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'S0022-CALL-LDBS3400'
                                          TO IEAI9901-LABEL-ERR
                           MOVE '005016'  TO IEAI9901-COD-ERRORE
                           STRING 'LDBS3400;'
                                  IDSO0011-RETURN-CODE ';'
                                  IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      *--> ERRORE DISPATCHER

                  MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S0022-CALL-LDBS3400'
                                          TO IEAI9901-LABEL-ERR
                  MOVE '005016'           TO IEAI9901-COD-ERRORE
                  STRING 'LDBS3400;'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                      DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF

           END-PERFORM.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS3400'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura tab. GAR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S0022.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       S0025-CNTL-RAMO-INVST.

           MOVE 'S0025-CNTL-RAMO-INVST'     TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           PERFORM S0126-PREPARA-CALL-LDBS9090
              THRU EX-S0126.

           PERFORM S0127-CALL-LDBS9090
              THRU EX-S0127.

           MOVE IX-TAB-STR
             TO WSTR-ELE-GRZ-MAX.
           MOVE IX-TAB-GRZ
             TO WGRZ-ELE-GRZ-MAX.

       EX-S0025.
           EXIT.
      * ----------------------------------------------------------------

      * ----------------------------------------------------------------
       S0126-PREPARA-CALL-LDBS9090.

           MOVE 'S0026-PREPARA-CALL-LDBS9090' TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO           TO TRUE
           MOVE WK-LABEL-ERR                  TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE LDBV9091.

           MOVE WPOL-ID-POLI
             TO LDBV9091-ID-POLI.
           MOVE WADE-ID-ADES (IX-TAB-ADE)
             TO LDBV9091-ID-ADES.
           MOVE 'GA'
             TO LDBV9091-TP-OGG.

           MOVE 'VI'                      TO LDBV9091-TP-STAT-BUS-1
           MOVE 'ST'                      TO LDBV9091-TP-STAT-BUS-2

      *
      * DA DEFINIRE I CAMPI PROVENIENTI DA PRENOTAZIONE

           MOVE WLB-RAMO-BILA
             TO LDBV9091-RAMO1.
           MOVE WLB-TP-INVST
             TO LDBV9091-TP-INVST1.


           MOVE WS-DT-EFFETTO
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO.
           MOVE WS-DT-TS-PTF
             TO IDSI0011-DATA-COMPETENZA.
           MOVE 'LDBS9090'
             TO IDSI0011-CODICE-STR-DATO.
           MOVE SPACES
             TO IDSI0011-BUFFER-WHERE-COND.
           MOVE LDBV9091
             TO IDSI0011-BUFFER-DATI.
            SET IDSI0011-WHERE-CONDITION
             TO TRUE.
            SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
            SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
            SET IDSI0011-TRATT-X-COMPETENZA
             TO TRUE.
            SET IDSI0011-FETCH-FIRST
             TO TRUE.

       EX-S0126.
           EXIT.
      * ----------------------------------------------------------------
      *
      * ----------------------------------------------------------------
       S0127-CALL-LDBS9090.

           MOVE 'S0027-CALL-LDBS9090'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE 0
             TO IX-TAB-GRZ.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS9090'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura tab. GAR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSV0001-ESITO-OK
                      OR NOT FINE-GRZ-NO


               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *--> CHIAVE NON TROVATA
                           SET FINE-GRZ
                            TO TRUE
                           IF IDSI0011-FETCH-FIRST
                               SET RAMO-RICH-NO
                                TO TRUE
                               MOVE WK-PGM
                                 TO IEAI9901-COD-SERVIZIO-BE
                               MOVE WK-LABEL-ERR
                                 TO IEAI9901-LABEL-ERR
                               MOVE '005166'
                                 TO IEAI9901-COD-ERRORE
                    MOVE 'LDBS9090 - NON CI SONO GARANZIE DA ELABORARE '
                                 TO IEAI9901-PARAMETRI-ERR
                               PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                  THRU EX-S0300
                           END-IF
                      WHEN IDSO0011-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                           MOVE IDSO0011-BUFFER-DATI
                             TO LDBV9091
                           SET IDSI0011-FETCH-NEXT
                             TO TRUE
                           MOVE L9091-TP-STAT-BUS
                             TO WS-TP-STAT-BUS
                               PERFORM S0028-VAL-OUT-GRZ
                                  THRU EX-S0028
                      WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'S0027-CALL-LDBS9090'
                                          TO IEAI9901-LABEL-ERR
                           MOVE '005016'  TO IEAI9901-COD-ERRORE
                           STRING 'LDBS9090;'
                                  IDSO0011-RETURN-CODE ';'
                                  IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      *--> ERRORE DISPATCHER

                  MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S0027-CALL-LDBS9090'
                                          TO IEAI9901-LABEL-ERR
                  MOVE '005016'           TO IEAI9901-COD-ERRORE
                  STRING 'LDBS9090;'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                      DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF

           END-PERFORM.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS9090'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura tab. GAR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S0127.
           EXIT.
      *
      *-----------------------------------------------------------------
      *    ELABORAZIONE
      *-----------------------------------------------------------------
       S0100-ELABORAZIONE.

           MOVE 'S0100-ELABORAZIONE'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      * IN FASE DI AGGIORNAMENTO CANCELLO I RECORD PRESENTI SULLE
      * TABELLE VAR_T E VAR_P
           IF  IDSV0001-ESITO-OK
               IF WLB-TP-RICH = 'B2'
                  PERFORM S9100-DELETE-VAR-T
                     THRU EX-S9100
                  PERFORM S9200-DELETE-VAR-P
                     THRU EX-S9200
               END-IF
           END-IF.

      * IN FASE DI AGGIORNAMENTO CONTROLLO SE ADESIONE E STORNATA
      * SE STORNATA UPDATE STATI TRANCHE BILA-TRANCHE-ESTRATTE
      * LEGATE ALL'ADESIONE
           IF WLB-TP-RICH = 'B2'
              MOVE WS-TP-STAT-BUS-ADE TO WS-TP-STAT-BUS
              IF STORNATO
                 PERFORM S9400-UPD-STATI-TRCH-2770
                    THRU EX-S9400
      *          SET ADES-STORNATA TO TRUE
              END-IF
           END-IF.

      * ESTRAZIONE GARANZIE IN CASO DI GARANZIA NON STORNATA SIA IN
      * FASE DI ESTRAZIONE CHE DI AGGIORNAMENTO
           IF  IDSV0001-ESITO-OK
           AND ADES-NO-STORNATA
               PERFORM S0110-TRATTA-DATI-GAR
                  THRU EX-S0110
           END-IF.

      * IN FASE DI AGGIORNAMENTO SE CI SONO GARANZIE STORNATE LEGGO
      * LE TRANCHE ASSOCIATE + UPDATE STATI DELLA BILA-TRANCHE-ESTRATTE
           IF  WSTR-ELE-GRZ-MAX > 0
           AND WLB-TP-RICH = 'B2'
      *    AND ADES-NO-STORNATA
      *        PERFORM S0135-TRATTA-DATI-TGA-STR
      *           THRU EX-S0135
               PERFORM S9470-UPD-STATI-TRCH-2930
                  THRU EX-S9470
           END-IF

      * ESTRAZIONE GARANZIE IN CASO DI GARANZIA NON STORNATA SIA IN
      * FASE DI ESTRAZIONE CHE DI AGGIORNAMENTO
           IF  IDSV0001-ESITO-OK
      *    AND ADES-NO-STORNATA
           AND WGRZ-ELE-GRZ-MAX > 0
               PERFORM S0125-TRATTA-DATI-TGA
                  THRU EX-S0125
           END-IF.

      * VERIFICA SE DA L'ESTRAZIONE DELLE GARANZIE STORNATE ESISTONO
      * GARANZIE DI RAMO III PER LE QUALI NON SONO STATE DISINVESTITE
      * LE QUOTE
           IF  IDSV0001-ESITO-OK
      *    AND ADES-NO-STORNATA
           AND WSTR-ELE-GRZ-MAX > 0
           AND WLB-TP-RICH = 'B2'
               PERFORM S0137-TRATTA-TGA-ST
                  THRU EX-S0137
           END-IF.

      * CONTROLLO DI TRANCHE ATTIVE
      * PRIMA DELLA CHIAMATA AL VALORIZZATORE SE NON CI SONO TRANCHE
      * SEGNALAZIONE ERRORE
           IF  IDSV0001-ESITO-OK
               PERFORM CTRL-NUM-TRCH-ATT
                  THRU EX-CTRL-NUM-TRCH-ATT
           END-IF.

      * SE CI SONO GARANZIE DI RAMO III LEGGE FONDI ASSOCIATI
           IF IDSV0001-ESITO-OK
           AND ADES-NO-STORNATA
           AND WS-FL-SCARTA-ADE-NO
               IF SI-GAR-RAMO-III
      *--> PASSIAMO TUTTA L'AREA DELLE TGA
           MOVE WTGA-AREA-TGA                  TO ATGA-AREA-TGA
                  PERFORM LEGGI-QUOTAZ-FONDI
                     THRU LEGGI-QUOTAZ-FONDI-EX
               END-IF
           END-IF


      * CALL AL VALORIZZATORE VARIABILI
           IF  IDSV0001-ESITO-OK
           AND ADES-NO-STORNATA
           AND WS-FL-SCARTA-ADE-NO
               PERFORM S0500-CALL-VAL-VAR
                  THRU EX-S0500
           END-IF.

      * VALORIZZAZIONE TABELLE BIL_ESTRATTE , VAR_T , VAR_P
           IF  IDSV0001-ESITO-OK
           AND ADES-NO-STORNATA
           AND WS-FL-SCARTA-ADE-NO
               PERFORM S1050-VALORIZZA-OUT
                  THRU EX-S1050
           END-IF.

           IF IDSV0001-ESITO-KO
               ADD 1 TO IABV0006-CUSTOM-COUNT(REC-SCARTATI-1)
               PERFORM WRITE-FILEOUT-SCARTI
                  THRU WRITE-FILEOUT-SCARTI-EX
           END-IF.

       EX-S0100.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA PARAMETRO OGGETTO
      *----------------------------------------------------------------*
       S0111-TRATTA-POG.

           MOVE 'S0111-TRATTA-POG'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

             MOVE 'ANNIRENCERTA'
             TO WK-COD-PARAM.

           PERFORM S0026-IMPOSTA-POG
              THRU EX-S0026.

           PERFORM S0027-READ-POG
              THRU EX-S0027.
      *
       EX-S0111.
           EXIT.
      *----------------------------------------------------------------*
      *    IMPOSTAZIONE AREA LETTURA PARAMETRO OGGETTO
      *----------------------------------------------------------------*
       S0026-IMPOSTA-POG.

           MOVE 'S0026-IMPOSTA-POG'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE POL-ID-POLI
             TO LDBV1131-ID-OGG.
           MOVE 'PO'
             TO LDBV1131-TP-OGG.
           MOVE WK-COD-PARAM
             TO LDBV1131-COD-PARAM.
           MOVE WS-DT-EFFETTO
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF
             TO IDSI0011-DATA-COMPETENZA.
            SET IDSI0011-TRATT-X-COMPETENZA
             TO TRUE.
           MOVE 'LDBS1130'
             TO IDSI0011-CODICE-STR-DATO.
           MOVE AREA-LDBV1131
             TO IDSI0011-BUFFER-WHERE-COND.
           MOVE SPACES
             TO IDSI0011-BUFFER-DATI.
            SET IDSI0011-SELECT
             TO TRUE.
            SET IDSI0011-WHERE-CONDITION
             TO TRUE.
            SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
            SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
            SET POG-KO
             TO TRUE.
      *
       EX-S0026.
           EXIT.
      *----------------------------------------------------------------*
      * LETTURA TABELLA PARAM_OGG PER REPERIMENTO DURATA PAGAMENTO PREMI
      *----------------------------------------------------------------*
       S0027-READ-POG.

           MOVE 'S0027-READ-POG'            TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY           THRU ESEGUI-DISPLAY-EX.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       MOVE HIGH-VALUES TO WB03-DUR-PAG-REN-NULL
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       SET  POG-OK                  TO TRUE
                       MOVE IDSO0011-BUFFER-DATI    TO PARAM-OGG
                  WHEN OTHER
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0027-READ-POG'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'LDBS1130;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
           ELSE
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0027-READ-POG'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'LDBS1130;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
       EX-S0027.
           EXIT.

      *----------------------------------------------------------------*
      * IMPOSTAZIONE AREA LETTURA PARAMETRO OGGETTO
      *----------------------------------------------------------------*
       S0216-IMPOSTA-POG.

           MOVE 'S0026-IMPOSTA-POG'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE WGRZ-ID-GAR(IX-TAB-GRZ)
             TO LDBV1131-ID-OGG.
           MOVE 'GA'
             TO LDBV1131-TP-OGG.
           MOVE WK-COD-PARAM
             TO LDBV1131-COD-PARAM.
           MOVE WS-DT-EFFETTO
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF
             TO IDSI0011-DATA-COMPETENZA.
            SET IDSI0011-TRATT-X-COMPETENZA
             TO TRUE.
           MOVE 'LDBS1130'
             TO IDSI0011-CODICE-STR-DATO.
           MOVE AREA-LDBV1131
             TO IDSI0011-BUFFER-WHERE-COND.
           MOVE SPACES
             TO IDSI0011-BUFFER-DATI.
            SET IDSI0011-SELECT
             TO TRUE.
            SET IDSI0011-WHERE-CONDITION
             TO TRUE.
            SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
            SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS1130'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura PARAM_OGG'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS1130'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura PARAM_OGG'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S0216.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S0110-TRATTA-DATI-GAR.

           MOVE 'S0110-TRATTA-DATI-GAR'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

            IF  IDSV0001-ESITO-OK
                 IF WLB-RAMO-BILA = '00' OR SPACES
                    PERFORM S0120-ESTRAZ-GRZ
                       THRU EX-S0120
                 ELSE
                    IF  WLB-RAMO-BILA = '3'
                    AND WLB-TP-INVST = 7 OR 8
                        PERFORM S0025-CNTL-RAMO-INVST
                          THRU EX-S0025
                    ELSE
                        PERFORM S0020-CNTL-RAMO
                           THRU EX-S0020
                    END-IF
                 END-IF
           END-IF.

       EX-S0110.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       S0125-TRATTA-DATI-TGA.

           MOVE 'S0125-TRATTA-DATI-TGA'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           IF IDSV0001-ESITO-OK
              SET  IDSV8888-BUSINESS-DBG      TO TRUE
              MOVE 'LDBS3420'                 TO IDSV8888-NOME-PGM
              SET  IDSV8888-INIZIO            TO TRUE
              MOVE SPACES                     TO IDSV8888-DESC-PGM
              STRING 'Lettura TRCH_DI_GAR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
              END-STRING
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

               PERFORM S0180-ESTRAZ-TGA
                  THRU EX-S0180
               VARYING IX-TAB-GRZ FROM 1 BY 1
                 UNTIL IX-TAB-GRZ > WGRZ-ELE-GRZ-MAX
                    OR IDSV0001-ESITO-KO

              SET  IDSV8888-BUSINESS-DBG      TO TRUE
              MOVE 'LDBS3420'                 TO IDSV8888-NOME-PGM
              SET  IDSV8888-FINE              TO TRUE
              MOVE SPACES                     TO IDSV8888-DESC-PGM
              STRING 'Lettura TRCH_DI_GAR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
              END-STRING
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
           END-IF.

       EX-S0125.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       S0137-TRATTA-TGA-ST.

           MOVE 'S0137-TRATTA-TGA-ST'        TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           IF IDSV0001-ESITO-OK
              SET  IDSV8888-BUSINESS-DBG      TO TRUE
              MOVE 'LDBS3420'                 TO IDSV8888-NOME-PGM
              SET  IDSV8888-INIZIO            TO TRUE
              MOVE SPACES                     TO IDSV8888-DESC-PGM
              STRING 'Lettura st. TRCH_DI_GAR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
              END-STRING
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

               PERFORM S0181-ESTRAZ-TGA
                  THRU EX-S0181
               VARYING IX-TAB-STR FROM 1 BY 1
                 UNTIL IX-TAB-STR > WSTR-ELE-GRZ-MAX
                    OR IDSV0001-ESITO-KO

              SET  IDSV8888-BUSINESS-DBG      TO TRUE
              MOVE 'LDBS3420'                 TO IDSV8888-NOME-PGM
              SET  IDSV8888-FINE              TO TRUE
              MOVE SPACES                     TO IDSV8888-DESC-PGM
              STRING 'Lettura st. TRCH_DI_GAR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
              END-STRING
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
           END-IF.

       EX-S0137.
           EXIT.
      *-----------------------------------------------------------------
      * ESTRAZIONE DI TUTTE LE TRANCHE LEGATE ALLA GARANZIA STORNATA
      *-----------------------------------------------------------------
       S9470-UPD-STATI-TRCH-2930.

           MOVE 'S9470-UPD-STATI-TRCH-2930'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           IF IDSV0001-ESITO-OK
              SET  IDSV8888-BUSINESS-DBG      TO TRUE
              MOVE 'LDBS2930'                 TO IDSV8888-NOME-PGM
              SET  IDSV8888-INIZIO            TO TRUE
              MOVE SPACES                     TO IDSV8888-DESC-PGM
              STRING 'Update BILA_TRCH_ESTR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
              END-STRING
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

               PERFORM S0280-ESTRAZ-TGA-STR
                  THRU EX-S0280
               VARYING IX-TAB-STR FROM 1 BY 1
                 UNTIL IX-TAB-STR > WSTR-ELE-GRZ-MAX
                    OR IDSV0001-ESITO-KO

              SET  IDSV8888-BUSINESS-DBG      TO TRUE
              MOVE 'LDBS2930'                 TO IDSV8888-NOME-PGM
              SET  IDSV8888-FINE              TO TRUE
              MOVE SPACES                     TO IDSV8888-DESC-PGM
              STRING 'Update BILA_TRCH_ESTR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
              END-STRING
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           END-IF.

       EX-S9470.
           EXIT.
      * ----------------------------------------------------------------
      * Reperimento del TIPO ISCRITTO dalla tabella D_FISC_ADES
      * ----------------------------------------------------------------
       S0113-TRATTA-DFA.

           MOVE 'S0113-TRATTA-DFA'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           PERFORM S0114-PREPARA-ACCESSO-DFA
              THRU EX-S0114.
           PERFORM S0115-LEGGI-DFA
              THRU EX-S0115.

       EX-S0113.
           EXIT.
      * ----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella D_FISC_ADE
      * ----------------------------------------------------------------
       S0114-PREPARA-ACCESSO-DFA.

           MOVE 'S0114-PREPARA-ACCESSO-DFA'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.


      *-------------- POPOLAMENTO AREA INPUT DISPATCHER ---------------*
      *--> DATA EFFETTO
           MOVE WS-DT-EFFETTO          TO IDSI0011-DATA-INIZIO-EFFETTO
                                           IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF            TO IDSI0011-DATA-COMPETENZA.
           SET IDSI0011-TRATT-X-COMPETENZA   TO TRUE
      *--> TABELLA STORICA

      *--> MODALITA DI ACCESSO
           SET IDSI0011-ID-PADRE        TO TRUE.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT          TO TRUE.

      *--> NOME TABELLA FISICA DB
           MOVE 'D-FISC-ADES'           TO IDSI0011-CODICE-STR-DATO.

           INITIALIZE D-FISC-ADES.

      *--> VALORIZZA DCLGEN TABELLA
           MOVE WADE-ID-ADES(IX-TAB-ADE) TO DFA-ID-ADES.

      *--> DCLGEN TABELLA
           MOVE D-FISC-ADES             TO IDSI0011-BUFFER-DATI.

       EX-S0114.
           EXIT.
      * ----------------------------------------------------------------
      * Chiamata al dispatcher dati per l'accesso alla D_FISC_ADES
      * ----------------------------------------------------------------
       S0115-LEGGI-DFA.

           MOVE 'S0115-LEGGI-DFA'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IDBSDFA0'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura D_FISC_ADES'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           SET DFA-OK TO TRUE
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *  --> Chiave non trovata
                       SET DFA-KO TO TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *  --> Operazione eseguita correttamente
                      MOVE IDSO0011-BUFFER-DATI
                        TO D-FISC-ADES
                  WHEN OTHER
      *  --> Errore di accesso al db
                       MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0115-LEGGI-DFA'
                                          TO IEAI9901-LABEL-ERR
                       MOVE '005016'      TO IEAI9901-COD-ERRORE
                       STRING 'D-FISC-ADES;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *  --> Errore dispatcher
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0115-LEGGI-DFA'      TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              STRING 'D-FISC-ADES;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                      DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IDBSDFA0'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura D_FISC_ADES'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S0115.
           EXIT.

      *----------------------------------------------------------------*
      *    FETCH PARAMETRO MOVIMENTO
      *----------------------------------------------------------------*
       S1900-LETTURA-PARAM-MOVI.

MIGCOL     INITIALIZE WK-NOME-PGM
      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
           MOVE WS-DT-EFFETTO     TO IDSI0011-DATA-INIZIO-EFFETTO
                                      IDSI0011-DATA-FINE-EFFETTO.
           MOVE WS-DT-TS-PTF       TO IDSI0011-DATA-COMPETENZA.

      *VIENE VALORIZZATO DAL CHIAMANTE
           SET IDSI0011-TRATT-X-COMPETENZA
                                        TO TRUE.
      *--> TIPO OPERAZIONE
           SET IDSI0011-FETCH-FIRST     TO TRUE.

           SET IDSI0011-WHERE-CONDITION TO TRUE.

      *--> NOME TABELLA FISICA DB
MIGCOL     IF WPOL-TP-FRM-ASSVA = 'CO'
MIGCOL        MOVE 'LDBSH600'              TO IDSI0011-CODICE-STR-DATO
MIGCOL                                        WK-NOME-PGM
MIGCOL     ELSE
              MOVE 'LDBS0640'              TO IDSI0011-CODICE-STR-DATO
MIGCOL                                        WK-NOME-PGM
MIGCOL     END-IF

           INITIALIZE                      PARAM-MOVI
                                           LDBV0641
                                           LDBVH601
MIGCOL     IF WPOL-TP-FRM-ASSVA = 'CO'
MIGCOL        MOVE WADE-ID-ADES (1)
MIGCOL          TO LDBVH601-ID-ADES
MIGCOL        SET VARIA-OPZION             TO TRUE
MIGCOL        MOVE WS-MOVIMENTO            TO LDBVH601-TP-MOVI-1
MIGCOL     ELSE
              MOVE WPOL-ID-POLI            TO LDBV0641-ID-POLI
              SET MOVIM-QUIETA             TO TRUE
              MOVE WS-MOVIMENTO            TO LDBV0641-TP-MOVI-1
              SET GENER-TRANCH             TO TRUE
              MOVE WS-MOVIMENTO            TO LDBV0641-TP-MOVI-2
              SET GENER-TRAINC             TO TRUE
              MOVE WS-MOVIMENTO            TO LDBV0641-TP-MOVI-3
              SET VARIA-OPZION             TO TRUE
              MOVE WS-MOVIMENTO            TO LDBV0641-TP-MOVI-4
MIGCOL     END-IF

      *--> DCLGEN TABELLA
           MOVE PARAM-MOVI              TO IDSI0011-BUFFER-DATI.
           MOVE LDBV0641                TO IDSI0011-BUFFER-WHERE-COND


      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSO0011-SUCCESSFUL-RC  TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL TO TRUE.

VSTEF      MOVE ZEROES TO IX-TAB-PMO

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE WK-NOME-PGM                TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura PARAM_MOVI'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

      *----------------------------------------------------------------*
           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC  OR
                         NOT IDSO0011-SUCCESSFUL-SQL

                 PERFORM CALL-DISPATCHER
                    THRU CALL-DISPATCHER-EX

                 IF IDSO0011-SUCCESSFUL-RC
                    EVALUATE TRUE
                        WHEN IDSO0011-SUCCESSFUL-SQL
      *-->              OPERAZIONE ESEGUITA CORRETTAMENTE
                           MOVE IDSO0011-BUFFER-DATI
                             TO PARAM-MOVI
                           ADD 1
                             TO IX-TAB-PMO
                           PERFORM INIZIA-TOT-PMO
                              THRU INIZIA-TOT-PMO-EX

                           PERFORM VALORIZZA-OUTPUT-PMO
                              THRU VALORIZZA-OUTPUT-PMO-EX

                           MOVE IX-TAB-PMO TO WPMO-ELE-PMO-MAX

                           SET IDSI0011-FETCH-NEXT   TO TRUE
                        WHEN IDSO0011-NOT-FOUND
      *--->             CAMPO $ NON TROVATO
                           IF IDSI0011-FETCH-FIRST
                              CONTINUE
                           END-IF
                        WHEN OTHER
      *--->             ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM      TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'S1900-LETTURA-PARAM-MOVI'
                                            TO IEAI9901-LABEL-ERR
                           MOVE '005015'    TO IEAI9901-COD-ERRORE
                           STRING 'PARAM-MOVI '        ';'
                                  IDSO0011-RETURN-CODE ';'
                                  IDSO0011-SQLCODE
                                  DELIMITED BY SIZE INTO
                                  IEAI9901-PARAMETRI-ERR
                           END-STRING

                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                    END-EVALUATE
                 ELSE
      *-->          GESTIRE ERRORE DISPATCHER
                    MOVE WK-PGM
                      TO IEAI9901-COD-SERVIZIO-BE
                    MOVE 'S1900-LETTURA-PARAM-MOVI'
                      TO IEAI9901-LABEL-ERR
                    MOVE '005016'
                      TO IEAI9901-COD-ERRORE
                    STRING 'PARAM-MOVI '       ';'
                                  IDSO0011-RETURN-CODE ';'
                                  IDSO0011-SQLCODE
                                  DELIMITED BY SIZE INTO
                                  IEAI9901-PARAMETRI-ERR
                    END-STRING

                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
                       THRU EX-S0300
                 END-IF
           END-PERFORM.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE WK-NOME-PGM                TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura PARAM_MOVI'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S1900.
           EXIT.
      * ----------------------------------------------------------------
      * REPERIMENTO GARANZIE INTERESSATE ALLA RISERVA MATEMATICA
      * ----------------------------------------------------------------
       S0120-ESTRAZ-GRZ.

           MOVE 'S0120-ESTRAZ-GRZ'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           PERFORM S0130-PREPARA-ACCESSO-GAR
              THRU EX-S0130.
           PERFORM S0140-FETCH-GRZ
              THRU EX-S0140.
           MOVE IX-TAB-GRZ
             TO WGRZ-ELE-GRZ-MAX.
           MOVE IX-TAB-STR
             TO WSTR-ELE-GRZ-MAX.
      *
       EX-S0120.
           EXIT.
      * ----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella GAR
      * ----------------------------------------------------------------
       S0130-PREPARA-ACCESSO-GAR.

           MOVE 'S0130-PREPARA-ACCESSO-GAR'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE LDBV3411.

           MOVE WPOL-ID-POLI              TO LDBV3411-ID-POLI.
           MOVE WADE-ID-ADES (IX-TAB-ADE) TO LDBV3411-ID-ADES.
           MOVE 'GA'                      TO LDBV3411-TP-OGG.

           MOVE 'VI'                      TO LDBV3411-TP-STAT-BUS-1
           MOVE 'ST'                      TO LDBV3411-TP-STAT-BUS-2

      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
      * DA DEFINIRE I CAMPI PROVENIENTI DA PRENOTAZIONE
           MOVE WS-DT-EFFETTO           TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO.

      * DA DEFINIRE I CAMPI PROVENIENTI DA PRENOTAZIONE
           MOVE WS-DT-TS-PTF             TO IDSI0011-DATA-COMPETENZA.

      *--> NOME TABELLA FISICA DB
           MOVE 'LDBS3410'               TO IDSI0011-CODICE-STR-DATO.
      *--> DCLGEN TABELLA
           MOVE LDBV3411                 TO IDSI0011-BUFFER-DATI.
           MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.

      *--> TIPO OPERAZIONE
           SET IDSI0011-FETCH-FIRST         TO TRUE.
           SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.
           SET IDSI0011-WHERE-CONDITION     TO TRUE.

      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL      TO TRUE.

       EX-S0130.
           EXIT.
      * ----------------------------------------------------------------
      * FETCH DELLE SINGOLE GARANZIE SELEZIONATE
      * ----------------------------------------------------------------
       S0140-FETCH-GRZ.

           MOVE 'S0140-FETCH-GRZ'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE 0
             TO IX-TAB-GRZ.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS3410'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura tab. GAR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSV0001-ESITO-OK
                      OR NOT FINE-GRZ-NO

               PERFORM CALL-DISPATCHER         THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *--> CHIAVE NON TROVATA
                            SET FINE-GRZ
                             TO TRUE
                           IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM   TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'S0140-FETCH-GRZ'
                                             TO IEAI9901-LABEL-ERR
                              MOVE '005056'  TO IEAI9901-COD-ERRORE
                              MOVE 'LDBS3410'
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                           END-IF
                      WHEN IDSO0011-SUCCESSFUL-SQL

                           MOVE IDSO0011-BUFFER-DATI
                             TO LDBV3411
                            SET IDSI0011-FETCH-NEXT
                             TO TRUE
                            MOVE L3411-TP-STAT-BUS
                              TO WS-TP-STAT-BUS
                            PERFORM S0150-VAL-OUT-GRZ
                               THRU EX-S0150
                      WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'S0140-FETCH-GRZ'
                                          TO IEAI9901-LABEL-ERR
                           MOVE '005016'  TO IEAI9901-COD-ERRORE
                           STRING 'LDBS3410;'
                                  IDSO0011-RETURN-CODE ';'
                                  IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      *--> ERRORE DISPATCHER

                  MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S0140-FETCH-GRZ'
                                          TO IEAI9901-LABEL-ERR
                  MOVE '005016'           TO IEAI9901-COD-ERRORE
                  STRING 'LDBS3410;'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                      DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS3410'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura tab. GAR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S0140.
           EXIT.
      * ----------------------------------------------------------------
      * valorizza la workarea di output delle garanzie
      * ----------------------------------------------------------------
       S0150-VAL-OUT-GRZ.

           MOVE 'S0150-VAL-OUT-GRZ-3400'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

            IF STORNATO
               ADD 1  TO IX-TAB-STR
               PERFORM INIZIA-TOT-GRZ-ST
                  THRU INIZIA-TOT-GRZ-ST-EX
               PERFORM VAL-OUT-ST-GRZ-3410
                  THRU VAL-OUT-ST-GRZ-3410-EX
            END-IF

            IF IN-VIGORE
               ADD 1  TO IX-TAB-GRZ
               IF  L3411-RAMO-BILA-NULL NOT = HIGH-VALUES
                   IF L3411-RAMO-BILA = '3'
                      SET SI-GAR-RAMO-III TO TRUE
                   END-IF

               END-IF
               IF IDSV0001-ESITO-OK
                  PERFORM VALORIZZA-OUTPUT-GRZ-3410
                     THRU VALORIZZA-OUTPUT-GRZ-3410-EX
               END-IF
            END-IF.

       EX-S0150.
           EXIT.
      *----------------------------------------------------------------*
      * LETTURA DELLA TABELLA MOVIMENTO PER LEGGERE L'EVENTUALE        *
      * OPERAZIONE DI RISCATTO PARZIALE SOLO SE NEL CONTRATTO          *
      * SONO PRESENTI GARANZIE DI RAMO '1' O '5'                       *
      *----------------------------------------------------------------*
ALEX1  S0155-LEGGI-MOVI.
      *
           MOVE WPOL-TP-FRM-ASSVA           TO WS-TP-FRM-ASSVA.
           IF INDIVIDUALE
              MOVE WPOL-ID-POLI             TO MOV-ID-OGG
              MOVE 'PO'                     TO MOV-TP-OGG
              SET  COMUN-RISPAR-IND         TO TRUE
              MOVE WS-MOVIMENTO             TO LDBVC591-TP-MOVI-1
              SET  LIQUI-RISPAR-POLIND      TO TRUE
              MOVE WS-MOVIMENTO             TO LDBVC591-TP-MOVI-2
           ELSE
              MOVE WADE-ID-ADES(IX-TAB-ADE) TO MOV-ID-OGG
              MOVE 'AD'                     TO MOV-TP-OGG
              SET  COMUN-RISPAR-ADE         TO TRUE
              MOVE WS-MOVIMENTO             TO LDBVC591-TP-MOVI-1
              SET  LIQUI-RISPAR-ADE         TO TRUE
              MOVE WS-MOVIMENTO             TO LDBVC591-TP-MOVI-2
           END-IF.

           MOVE LDBVC591           TO IDSI0011-BUFFER-WHERE-COND.


           MOVE WLB-DT-PRODUZIONE      TO IDSI0011-DATA-INIZIO-EFFETTO
                                          IDSI0011-DATA-FINE-EFFETTO
                                          MOV-DT-EFF
           MOVE WLB-TS-COMPETENZA      TO IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-SENZA-STOR       TO TRUE.
           MOVE 'LDBSC590'         TO IDSI0011-CODICE-STR-DATO.
           MOVE MOVI               TO IDSI0011-BUFFER-DATI.

           SET IDSI0011-SELECT          TO TRUE.
           SET IDSI0011-WHERE-CONDITION TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL  TO TRUE.
      *
           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBSC590'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura MOVI'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND

                       CONTINUE

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *  --> Operazione eseguita correttamente
                       MOVE IDSO0011-BUFFER-DATI
                         TO MOVI
                       MOVE MOV-ID-MOVI
                         TO WK-MOV-ID-MOVI
                       MOVE MOV-DT-EFF
                         TO WK-MOV-DT-EFF
                       MOVE MOV-DS-TS-CPTZ
                         TO WK-MOV-DS-TS-CPTZ
                       SET RISC-PARZ-TROVATO-SI  TO TRUE
                  WHEN OTHER
      *  --> Errore di accesso al db
                       MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0155-LEGGI-MOVI'
                                          TO IEAI9901-LABEL-ERR
                       MOVE '005016'      TO IEAI9901-COD-ERRORE
                       STRING 'MOVI;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
                  END-EVALUATE
           ELSE
      *  --> Errore dispatcher
                  MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S0155-LEGGI-MOVI'    TO IEAI9901-LABEL-ERR
                  MOVE '005016'              TO IEAI9901-COD-ERRORE
                  STRING 'MOVI;'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                         DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
           END-IF.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBSC590'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura MOVI'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S0155.
           EXIT.
      * ----------------------------------------------------------------
      * valorizza la workarea di output delle garanzie
      * ----------------------------------------------------------------
       S0250-VAL-OUT-GRZ.

           MOVE 'S0150-VAL-OUT-GRZ-3400'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

ALEX1 *    IF  GESTIONE-RISERVE-RAMO-OPERAZ
      *        IF  L3411-RAMO-BILA-NULL NOT = HIGH-VALUE AND
      *            (L3411-RAMO-BILA  = '1' OR '5')        AND
      *             WK-LETT-MOV-RISC-PARZ-FUTURO-N
      *             SET WK-LETT-MOV-RISC-PARZ-FUTURO-S TO TRUE
      *             INITIALIZE  WK-EL-RISC-PARZ
      *             PERFORM S0155-LEGGI-MOVI         THRU EX-S0155
      *        END-IF
      *    END-IF

            IF STORNATO
               ADD 1  TO IX-TAB-STR
               PERFORM INIZIA-TOT-GRZ-ST
                  THRU INIZIA-TOT-GRZ-ST-EX
               PERFORM VAL-OUT-ST-GRZ-3400
                  THRU VAL-OUT-ST-GRZ-3400-EX
            END-IF

            IF IN-VIGORE
               ADD 1  TO IX-TAB-GRZ
               IF  L3401-RAMO-BILA-NULL NOT = HIGH-VALUES
                   IF L3401-RAMO-BILA = '3'
                      SET SI-GAR-RAMO-III TO TRUE
                   END-IF
                   SET   WK-DA-SCARTARE-NO       TO TRUE
ALEX1              IF  GESTIONE-RISERVE-RAMO-OPERAZ
                       IF L3401-RAMO-BILA = '1' OR '5'
                          MOVE STAT-OGG-BUS       TO  WK-APPO-AREA-STB
                          MOVE L3401-ID-GAR       TO STB-ID-OGG
                          MOVE 'GA'               TO STB-TP-OGG
                         PERFORM S0013-PREPARA-ACCESSO-STB THRU EX-S0013
                          PERFORM S0014-LEGGI-STB THRU EX-S0014
                          MOVE WK-APPO-AREA-STB   TO  STAT-OGG-BUS
                       END-IF
                   END-IF
               END-IF
               IF WK-DA-SCARTARE-NO
                  PERFORM VALORIZZA-OUTPUT-GRZ-3400
                     THRU VALORIZZA-OUTPUT-GRZ-3400-EX
               END-IF
            END-IF.

           MOVE WS-DT-EFFETTO           TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO.

           MOVE WS-DT-TS-PTF             TO IDSI0011-DATA-COMPETENZA.

      *--> NOME TABELLA FISICA DB
           MOVE 'LDBS3400'               TO IDSI0011-CODICE-STR-DATO.
      *--> DCLGEN TABELLA
           MOVE LDBV3401                 TO IDSI0011-BUFFER-DATI.
           MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.

      *--> TIPO OPERAZIONE
           SET IDSI0011-FETCH-NEXT          TO TRUE.
           SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.
           SET IDSI0011-WHERE-CONDITION     TO TRUE.

      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL      TO TRUE.

       EX-S0250.
           EXIT.
      * ----------------------------------------------------------------
      * valorizza la workarea di output delle garanzie
      * ----------------------------------------------------------------
       S0028-VAL-OUT-GRZ.

           MOVE 'S0028-VAL-OUT-GRZ'         TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

            IF STORNATO
               ADD 1  TO IX-TAB-STR
               PERFORM INIZIA-TOT-GRZ-ST
                  THRU INIZIA-TOT-GRZ-ST-EX
               PERFORM VAL-OUT-ST-GRZ-9090
                  THRU VAL-OUT-ST-GRZ-9090-EX
            END-IF

            IF IN-VIGORE
               ADD 1  TO IX-TAB-GRZ
               IF  L9091-RAMO-BILA-NULL NOT = HIGH-VALUES
                   IF L9091-RAMO-BILA = '3'
                      SET SI-GAR-RAMO-III TO TRUE
                   END-IF
               END-IF
               PERFORM VALORIZZA-OUTPUT-GRZ-9090
                  THRU VALORIZZA-OUTPUT-GRZ-9090-EX
            END-IF.

       EX-S0028.
           EXIT.
      * ----------------------------------------------------------------
      * REPERIMENTO DELLE TRANCHES INTERESSATE ALLA RISERVA MATEMATICA
      * ----------------------------------------------------------------
       S0180-ESTRAZ-TGA.

           MOVE 'S0180-ESTRAZ-TGA'          TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY           THRU ESEGUI-DISPLAY-EX.

           PERFORM S0185-PREPARA-ACCESSO-TGA
              THRU EX-S0185.
           PERFORM S0190-FETCH-TGA
              THRU EX-S0190.
           MOVE IX-TAB-TGA
             TO WTGA-ELE-TGA-MAX.
      *
       EX-S0180.
           EXIT.
      * ----------------------------------------------------------------
      *
      * ----------------------------------------------------------------
       S0181-ESTRAZ-TGA.

           MOVE 'S0181-ESTRAZ-TGA'          TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY           THRU ESEGUI-DISPLAY-EX.

           IF WSTR-RAMO-BILA-NULL(IX-TAB-STR) NOT = HIGH-VALUES
              IF WSTR-RAMO-BILA(IX-TAB-STR) = '3'
                 PERFORM S0185-PREPARA-ACCESSO-TGA
                    THRU EX-S0185
                 PERFORM S0192-FETCH-TGA
                    THRU EX-S0192
              END-IF
           END-IF.
      *
       EX-S0181.
           EXIT.
      * ----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella TRCH_DI_GAR
      * ----------------------------------------------------------------
       S0185-PREPARA-ACCESSO-TGA.

           MOVE 'S0185-PREPARA-ACCESSO-TGA'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> TIPO OPERAZIONE
           SET IDSI0011-FETCH-FIRST        TO TRUE

      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC      TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL     TO TRUE.

       EX-S0185.
           EXIT.
      * ----------------------------------------------------------------
      * FETCH DELLE SINGOLE TRANCHES DI GARANZIA
      * ----------------------------------------------------------------
       S0190-FETCH-TGA.

           MOVE 'S0190-FETCH-TGA'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET  FINE-TGA-NO TO TRUE.
           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSV0001-ESITO-OK
                      OR NOT FINE-TGA-NO

                MOVE WPOL-ID-POLI              TO LDBV3421-ID-POLI
                MOVE WADE-ID-ADES (IX-TAB-ADE) TO LDBV3421-ID-ADES
                MOVE WGRZ-ID-GAR(IX-TAB-GRZ)   TO LDBV3421-ID-GAR
                MOVE 'TG'                      TO LDBV3421-TP-OGG

      *---SE ESTRAZIONE MASSIVA SOLO TRANCHE IN VIGORE
                IF WLB-TP-RICH = 'B1'
                   MOVE 'VI'                   TO LDBV3421-TP-STAT-BUS-1
                   MOVE SPACES                 TO LDBV3421-TP-STAT-BUS-2
                END-IF

      *---SE AGGIORNAMENTO  TRANCHE IN VIGORE + STORNATE
                IF WLB-TP-RICH = 'B2'
                   MOVE 'VI'                   TO LDBV3421-TP-STAT-BUS-1
                   MOVE 'ST'                   TO LDBV3421-TP-STAT-BUS-2
                END-IF

      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
                MOVE WS-DT-EFFETTO     TO IDSI0011-DATA-INIZIO-EFFETTO
                                           IDSI0011-DATA-FINE-EFFETTO
                MOVE WS-DT-TS-PTF       TO IDSI0011-DATA-COMPETENZA
      *--> NOME TABELLA FISICA DB
                MOVE 'LDBS3420'         TO IDSI0011-CODICE-STR-DATO
      *--> DCLGEN TABELLA
                MOVE LDBV3421           TO IDSI0011-BUFFER-DATI
                MOVE SPACES             TO IDSI0011-BUFFER-WHERE-COND

                SET IDSI0011-TRATT-X-COMPETENZA TO TRUE
                SET IDSI0011-WHERE-CONDITION    TO TRUE

                PERFORM CALL-DISPATCHER
                   THRU CALL-DISPATCHER-EX
      *
                IF IDSO0011-SUCCESSFUL-RC
                   EVALUATE TRUE
                       WHEN IDSO0011-NOT-FOUND
                             SET FINE-TGA
                              TO TRUE
                       WHEN IDSO0011-SUCCESSFUL-SQL
                            PERFORM S0195-VAL-OUT-TGA-STB
                               THRU EX-S0195
                             SET IDSI0011-FETCH-NEXT
                              TO TRUE
                       WHEN OTHER
                            MOVE WK-PGM
                              TO IEAI9901-COD-SERVIZIO-BE
                            MOVE 'S0190-FETCH-TGA'
                              TO IEAI9901-LABEL-ERR
                            MOVE '005016'
                              TO IEAI9901-COD-ERRORE
                            STRING 'LDBS3420;'
                                   IDSO0011-RETURN-CODE ';'
                                   IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                            END-STRING
                            PERFORM S0300-RICERCA-GRAVITA-ERRORE
                               THRU EX-S0300
                   END-EVALUATE
                ELSE
                   MOVE WK-PGM
                     TO IEAI9901-COD-SERVIZIO-BE
                   MOVE 'S0190-FETCH-TGA'
                     TO IEAI9901-LABEL-ERR
                   MOVE '005016'
                     TO IEAI9901-COD-ERRORE
                   STRING 'LDBS3420;'
                          IDSO0011-RETURN-CODE ';'
                          IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
                   END-STRING
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
                END-IF
           END-PERFORM.
      *
       EX-S0190.
           EXIT.
      * ----------------------------------------------------------------
      *
      * ----------------------------------------------------------------
       S0192-FETCH-TGA.

           MOVE 'S0192-FETCH-TGA'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET  FINE-TGA-NO TO TRUE.
           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSV0001-ESITO-OK
                      OR NOT FINE-TGA-NO


                MOVE WPOL-ID-POLI              TO LDBV3421-ID-POLI
                MOVE WADE-ID-ADES(IX-TAB-ADE)  TO LDBV3421-ID-ADES
                MOVE WSTR-ID-GAR(IX-TAB-STR)   TO LDBV3421-ID-GAR
                MOVE 'TG'                      TO LDBV3421-TP-OGG
                MOVE 'ST'                      TO LDBV3421-TP-STAT-BUS-1
                MOVE SPACES                    TO LDBV3421-TP-STAT-BUS-2


      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
                MOVE WS-DT-EFFETTO     TO IDSI0011-DATA-INIZIO-EFFETTO
                                           IDSI0011-DATA-FINE-EFFETTO
                MOVE WS-DT-TS-PTF       TO IDSI0011-DATA-COMPETENZA
      *--> NOME TABELLA FISICA DB
                MOVE 'LDBS3420'         TO IDSI0011-CODICE-STR-DATO
      *--> DCLGEN TABELLA
                MOVE LDBV3421           TO IDSI0011-BUFFER-DATI
                MOVE SPACES             TO IDSI0011-BUFFER-WHERE-COND

                SET IDSI0011-TRATT-X-COMPETENZA TO TRUE
                SET IDSI0011-WHERE-CONDITION    TO TRUE

                PERFORM CALL-DISPATCHER
                   THRU CALL-DISPATCHER-EX
      *
                IF IDSO0011-SUCCESSFUL-RC
                   EVALUATE TRUE
                       WHEN IDSO0011-NOT-FOUND
                             SET FINE-TGA
                              TO TRUE
                       WHEN IDSO0011-SUCCESSFUL-SQL
                             PERFORM S0197-VAL-OUT-TGA-STB
                                THRU EX-S0197
                             SET IDSI0011-FETCH-NEXT
                              TO TRUE
                       WHEN OTHER
                            MOVE WK-PGM
                              TO IEAI9901-COD-SERVIZIO-BE
                            MOVE 'S0192-FETCH-TGA'
                              TO IEAI9901-LABEL-ERR
                            MOVE '005016'
                              TO IEAI9901-COD-ERRORE
                            STRING 'LDBS3420;'
                                   IDSO0011-RETURN-CODE ';'
                                   IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                            END-STRING
                            PERFORM S0300-RICERCA-GRAVITA-ERRORE
                               THRU EX-S0300
                   END-EVALUATE
                ELSE
                   MOVE WK-PGM
                     TO IEAI9901-COD-SERVIZIO-BE
                   MOVE 'S0192-FETCH-TGA'
                     TO IEAI9901-LABEL-ERR
                   MOVE '005016'
                     TO IEAI9901-COD-ERRORE
                   STRING 'LDBS3420;'
                          IDSO0011-RETURN-CODE ';'
                          IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
                   END-STRING
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
                END-IF
           END-PERFORM.
      *
       EX-S0192.
           EXIT.
      * ----------------------------------------------------------------
      * REPERIMENTO DELLE TRANCHES LEGATE ALLA GARANZIA STORNATA
      * ----------------------------------------------------------------
       S0280-ESTRAZ-TGA-STR.

           MOVE 'S0280-ESTRAZ-TGA-STR'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE BILA-TRCH-ESTR.

           MOVE WLB-ID-RICH-COLL         TO B03-ID-RICH-ESTRAZ-MAS
           MOVE WPOL-ID-POLI             TO B03-ID-POLI
           MOVE WADE-ID-ADES(IX-TAB-ADE) TO B03-ID-ADES
           MOVE WSTR-ID-GAR(IX-TAB-STR)  TO B03-ID-GAR
           SET STORNATO                  TO TRUE
           MOVE WS-TP-STAT-BUS           TO B03-TP-STAT-BUS-TRCH
           MOVE SPACES                   TO B03-TP-CAUS-TRCH
           MOVE WLB-ID-RICH              TO B03-ID-RICH-ESTRAZ-AGG

      *--> NOME TABELLA FISICA DB
           MOVE 'LDBS2930'               TO IDSI0011-CODICE-STR-DATO.
      *--> DCLGEN TABELLA
PERFOR*    MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE BILA-TRCH-ESTR           TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-UPDATE              TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR    TO TRUE.
           SET IDSI0011-WHERE-CONDITION     TO TRUE.
      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL      TO TRUE.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *--> CHIAVE NON TROVATA
                     CONTINUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                     CONTINUE
                  WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S0280-ESTRAZ-TGA-STR'
                                    TO IEAI9901-LABEL-ERR
                     MOVE '005016'  TO IEAI9901-COD-ERRORE
                     STRING 'UPDATE BILA-TRCH-ESTR;'
                            IDSO0011-RETURN-CODE ';'
                            IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
                  END-EVALUATE
               ELSE
      *--> ERRORE DISPATCHER

                  MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S0280-ESTRAZ-TGA-STR'
                                          TO IEAI9901-LABEL-ERR
                  MOVE '005016'           TO IEAI9901-COD-ERRORE
                  STRING 'UPDATE BILA-TRCH-ESTR;'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                      DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF.

       EX-S0280.
           EXIT.
      * ----------------------------------------------------------------
      * valorizza la workarea di output delle tranche e degli stati
      * ----------------------------------------------------------------
       S0195-VAL-OUT-TGA-STB.

           MOVE 'S0195-VAL-OUT-TGA-STB'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE IDSO0011-BUFFER-DATI         TO LDBV3421.

           SET IN-VIGORE TO TRUE
           IF L3421-TP-STAT-BUS = WS-TP-STAT-BUS

              SET SI-VAL-AST TO TRUE
              IF L3421-TP-TRCH = '9' AND SI-GAR-RAMO-III
                 PERFORM S0210-RECUP-VALORE-ASSET
                    THRU EX-S0210
              END-IF

              SET   WK-DA-SCARTARE-NO       TO TRUE
ALEX1 *       IF  GESTIONE-RISERVE-RAMO-OPERAZ
      *           IF WGRZ-RAMO-BILA(IX-TAB-GRZ) = ( '1' OR '5' ) AND
      *              RISC-PARZ-TROVATO-SI
      *              MOVE STAT-OGG-BUS            TO WK-APPO-AREA-STB
      *              PERFORM S0015-PREPARA-ACCESSO-STB THRU EX-S0015
      *              PERFORM S0016-LEGGI-STB THRU EX-S0016
      *              MOVE WK-APPO-AREA-STB        TO STAT-OGG-BUS
      *           END-IF
      *       END-IF

              IF IDSV0001-ESITO-OK AND SI-VAL-AST AND
                 WK-DA-SCARTARE-NO
      *-- VALORIZZA AREA TRANCHE DI GARANZIA
                 ADD 1   TO IX-TAB-TGA
                 PERFORM INIZIA-TOT-TGA
                    THRU INIZIA-TOT-TGA-EX
                 PERFORM VALORIZZA-TGA
                    THRU VALORIZZA-TGA-EX
                 MOVE IX-TAB-TGA
                   TO WTGA-ELE-TGA-MAX

      *-- VALORIZZA AREA STATO-OGGETTO-BUSINESS
                 ADD 1     TO IX-TAB-STB
                 PERFORM INIZIA-TOT-STB
                    THRU INIZIA-TOT-STB-EX
                 PERFORM VALORIZZA-STB
                    THRU VALORIZZA-STB-EX
                 MOVE IX-TAB-STB
                   TO WSTB-ELE-STB-MAX
              END-IF
           END-IF.

           SET STORNATO TO TRUE
           IF  L3421-TP-STAT-BUS = WS-TP-STAT-BUS
           AND WLB-TP-RICH       = 'B2'
              PERFORM S9600-UPD-STATI-TRCH-2780
                 THRU EX-S9600
           END-IF.

           MOVE WS-DT-EFFETTO     TO IDSI0011-DATA-INIZIO-EFFETTO
                                     IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF       TO IDSI0011-DATA-COMPETENZA
      *--> NOME TABELLA FISICA DB
           MOVE 'LDBS3420'         TO IDSI0011-CODICE-STR-DATO
      *--> DCLGEN TABELLA
           MOVE LDBV3421           TO IDSI0011-BUFFER-DATI
           MOVE SPACES             TO IDSI0011-BUFFER-WHERE-COND

           SET IDSI0011-TRATT-X-COMPETENZA TO TRUE
           SET IDSI0011-WHERE-CONDITION    TO TRUE

      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC      TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL     TO TRUE.

       EX-S0195.
           EXIT.
      * ----------------------------------------------------------------
      * valorizza la workarea di output delle tranche e degli stati
      * ----------------------------------------------------------------
       S0197-VAL-OUT-TGA-STB.

           MOVE 'S0197-VAL-OUT-TGA-STB'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE IDSO0011-BUFFER-DATI         TO LDBV3421.

           SET STORNATO TO TRUE
           IF L3421-TP-STAT-BUS = WS-TP-STAT-BUS
              PERFORM S0198-VER-DISIN-QUOTE
                 THRU EX-S0198
              IF IDSV0001-ESITO-OK
              AND SI-SAVE-TRANCHE
                 SET SI-GAR-RAMO-III TO TRUE

                  SET SI-VAL-AST TO TRUE
                  IF L3421-TP-TRCH = '9'
                     PERFORM S0210-RECUP-VALORE-ASSET
                        THRU EX-S0210
                  END-IF

                  IF IDSV0001-ESITO-OK AND SI-VAL-AST
      *-->           VALORIZZA AREA TRANCHE DI GARANZIA
                     ADD 1   TO IX-TAB-TGA
                     PERFORM INIZIA-TOT-TGA
                        THRU INIZIA-TOT-TGA-EX
                     PERFORM VALORIZZA-TGA
                        THRU VALORIZZA-TGA-EX
                     MOVE IX-TAB-TGA
                       TO WTGA-ELE-TGA-MAX

      *-->       SALVATAGGIO DEL TIPO STATO DISINVESTIMENTO SU UN'AREA
      *-->       DI APPOGGIO. MI SERVIRA' POI PER VALORIZZARE LA CAUSALE
      *-->       DELLA TRANCHE IN FASE DI SCRITTURA DELLA TABELLA
      *-->       BILA-TRCH-ESTR
                     ADD 1
                       TO IX-TAB-CAUS
                     MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                       TO WK-TC-ID-TRCH(IX-TAB-CAUS)
                     MOVE RDF-TP-STAT
                       TO WK-TC-TP-STAT-DISINV(IX-TAB-CAUS)
                     MOVE IX-TAB-CAUS
                       TO WK-TAB-CAUS-ELE-MAX

      *-- VALORIZZA AREA STATO-OGGETTO-BUSINESS
                     ADD 1
                       TO IX-TAB-STB
                     PERFORM INIZIA-TOT-STB
                        THRU INIZIA-TOT-STB-EX
                     PERFORM VALORIZZA-STB
                        THRU VALORIZZA-STB-EX
                     MOVE IX-TAB-STB
                       TO WSTB-ELE-STB-MAX

                     SET SI-GAR-INS     TO TRUE
                     PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
                       UNTIL IX-TAB-GRZ > WGRZ-ELE-GRZ-MAX
                          OR NO-GAR-INS

                          IF WGRZ-ID-GAR(IX-TAB-GRZ) =
                             WSTR-ID-GAR(IX-TAB-GRZ)
                             SET NO-GAR-INS   TO TRUE
                          END-IF
                     END-PERFORM

                     IF SI-GAR-INS
                        ADD 1 TO WGRZ-ELE-GRZ-MAX
                        MOVE WGRZ-ELE-GRZ-MAX TO IX-TAB-GRZ
                        MOVE WSTR-TAB-GAR(IX-TAB-STR)
                          TO WGRZ-TAB-GAR(IX-TAB-GRZ)
                     END-IF
                  END-IF
              END-IF
           END-IF.

       EX-S0197.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       S0198-VER-DISIN-QUOTE.

           MOVE 'S0198-VER-DISIN-QUOTE'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE VAL-AST
                      LDBV5471.
           MOVE L3421-ID-TRCH-DI-GAR    TO LDBV5471-ID-TRCH-DI-GAR.

      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
      * DA DEFINIRE I CAMPI PROVENIENTI DA PRENOTAZIONE
           MOVE WS-DT-EFFETTO           TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO.

      * DA DEFINIRE I CAMPI PROVENIENTI DA PRENOTAZIONE
           MOVE WS-DT-TS-PTF             TO IDSI0011-DATA-COMPETENZA.

      *--> NOME TABELLA FISICA DB
           MOVE 'LDBS5470'               TO IDSI0011-CODICE-STR-DATO.
      *--> DCLGEN TABELLA
           MOVE RICH-DIS-FND             TO IDSI0011-BUFFER-DATI.
           MOVE LDBV5471                 TO IDSI0011-BUFFER-WHERE-COND.

      *--> TIPO OPERAZIONE
           SET IDSI0011-FETCH-FIRST         TO TRUE.
           SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.
           SET IDSI0011-WHERE-CONDITION     TO TRUE.

      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL      TO TRUE.
           SET FINE-DISIN-NO                TO TRUE.
           SET NO-SAVE-TRANCHE              TO TRUE.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS5470'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett. tab. RICH_DIS_FND'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSV0001-ESITO-OK
                      OR NOT FINE-DISIN-NO

               PERFORM CALL-DISPATCHER         THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *--> CHIAVE NON TROVATA
                            SET FINE-DISIN
                             TO TRUE
                           IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM   TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'S0198-VER-DISIN-QUOTE'
                                             TO IEAI9901-LABEL-ERR
                              MOVE '005056'  TO IEAI9901-COD-ERRORE
                              MOVE 'LDBS5470'
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                           END-IF
                      WHEN IDSO0011-SUCCESSFUL-SQL
                           MOVE IDSO0011-BUFFER-DATI
                             TO RICH-DIS-FND
                           PERFORM S0199-CTRL-TRANCHE
                              THRU EX-S0199

                            SET IDSI0011-FETCH-NEXT
                             TO TRUE
                      WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'S0198-VER-DISIN-QUOTE'
                                          TO IEAI9901-LABEL-ERR
                           MOVE '005016'  TO IEAI9901-COD-ERRORE
                           STRING 'LDBS5470;'
                                  IDSO0011-RETURN-CODE ';'
                                  IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      *--> ERRORE DISPATCHER

                  MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S0198-VER-DISIN-QUOTE'
                                          TO IEAI9901-LABEL-ERR
                  MOVE '005016'           TO IEAI9901-COD-ERRORE
                  STRING 'LDBS5470;'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                      DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS5470'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett. tab. RICH_DIS_FND'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S0198.
           EXIT.

      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       S0199-CTRL-TRANCHE.

           MOVE 'S0199-CTRL-TRANCHE'        TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           IF RDF-TP-STAT-NULL NOT = HIGH-VALUES
              IF RDF-TP-STAT NOT = 'CL'
                 SET SI-SAVE-TRANCHE TO TRUE
              END-IF
           END-IF.

           IF RDF-TP-STAT-NULL NOT = HIGH-VALUES
              IF RDF-TP-STAT  = 'CL'
                 IF RDF-DT-DIS-NULL NOT = HIGH-VALUES
                    IF RDF-DT-DIS > WS-DT-EFFETTO
                       SET SI-SAVE-TRANCHE TO TRUE
                    END-IF
                 END-IF
              END-IF
           END-IF.

       EX-S0199.
           EXIT.

      *-----------------------------------------------------------------
      * Verifica presenza della valore asset per le tranche negative
      *-----------------------------------------------------------------
       S0210-RECUP-VALORE-ASSET.

           MOVE 'S0210-RECUP-VALORE-ASSET' TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET INIT-CUR-VAS               TO TRUE.

           INITIALIZE IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-FETCH-FIRST       TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS4910'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura tab. VAL_AST'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR IDSV0001-ESITO-KO
                      OR FINE-CUR-VAS

              INITIALIZE VAL-AST
                         LDBV4911

              MOVE WS-DT-EFFETTO     TO IDSI0011-DATA-INIZIO-EFFETTO
                                        IDSI0011-DATA-FINE-EFFETTO
              MOVE WS-DT-TS-PTF      TO IDSI0011-DATA-COMPETENZA

              MOVE L3421-ID-TRCH-DI-GAR  TO LDBV4911-ID-TRCH-DI-GAR

              SET IDSI0011-TRATT-X-COMPETENZA TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSI0011-WHERE-CONDITION TO TRUE

              MOVE LDBV4911                TO IDSI0011-BUFFER-WHERE-COND

              MOVE 'LDBS4910'              TO IDSI0011-CODICE-STR-DATO

              MOVE VAL-AST                 TO IDSI0011-BUFFER-DATI

              PERFORM CALL-DISPATCHER      THRU CALL-DISPATCHER-EX

              IF IDSV0001-ESITO-OK
                 IF IDSO0011-SUCCESSFUL-RC
                    EVALUATE TRUE
                        WHEN IDSO0011-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA
                             SET FINE-CUR-VAS TO TRUE
                             IF IDSI0011-FETCH-FIRST
                                SET NO-VAL-AST TO TRUE
                             END-IF
                        WHEN IDSO0011-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                             SET SI-VAL-AST TO TRUE
                             SET IDSI0011-FETCH-NEXT TO TRUE
                        WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                          MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                          MOVE WK-LABEL-ERR TO IEAI9901-LABEL-ERR
                          MOVE '005016'     TO IEAI9901-COD-ERRORE
                          STRING 'RECUPERO VALORE ASSET ' ';'
                                 IDSO0011-RETURN-CODE ';'
                                 IDSO0011-SQLCODE
                                 DELIMITED BY SIZE
                                 INTO IEAI9901-PARAMETRI-ERR
                          END-STRING
                          PERFORM S0300-RICERCA-GRAVITA-ERRORE
                             THRU EX-S0300
                    END-EVALUATE
                 ELSE
      *--> GESTIRE ERRORE
                    MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
                    MOVE WK-LABEL-ERR     TO IEAI9901-LABEL-ERR
                    MOVE '005016'         TO IEAI9901-COD-ERRORE
                    STRING 'RECUPERO VALORE ASSET ' ';'
                           IDSO0011-RETURN-CODE ';'
                           IDSO0011-SQLCODE
                           DELIMITED BY SIZE
                           INTO IEAI9901-PARAMETRI-ERR
                    END-STRING
                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
                       THRU EX-S0300
                 END-IF
              ELSE
      *--> GESTIRE ERRORE
                 MOVE WK-PGM              TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR        TO IEAI9901-LABEL-ERR
                 MOVE '005016'            TO IEAI9901-COD-ERRORE
                 STRING 'RECUPERO VALORE ASSET ' ';'
                        IDSO0011-RETURN-CODE ';'
                        IDSO0011-SQLCODE
                        DELIMITED BY SIZE
                        INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-PERFORM.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS4910'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura tab. VAL_AST'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S0210.
           EXIT.

      *-----------------------------------------------------------------
      * Reperimento del sesso e della data di nascita del 1o assicurato
      *-----------------------------------------------------------------
       S1150-TRATTA-RAPP-ANA.

           MOVE 'S1150-TRATTA-RAPP-ANA'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE HIGH-VALUE            TO WB03-COD-FISC-ASSTO1-NULL
                                         WB03-COD-FISC-ASSTO2-NULL
                                         WB03-COD-FISC-ASSTO3-NULL
                                         WB03-STAT-TBGC-ASSTO-1-NULL
                                         WB03-STAT-TBGC-ASSTO-2-NULL
                                         WB03-STAT-TBGC-ASSTO-3-NULL
                                         WB03-DT-NASC-1O-ASSTO-NULL
                                         WB03-SEX-1O-ASSTO-NULL
                                         WB03-ETA-RAGGN-DT-CALC-NULL.

           SET WS-FL-ASSIC-NON-PRES   TO TRUE
           SET WS-FL-ADER-NON-PRES    TO TRUE

VSTEF *--> EFFETTUO LA LETTURA DELLA RAPPORTO ANAGRAFICO LEGATA
      *    ALL'ASSICURATO (SE PRESENTE)
ALEX       IF IDSV0001-ESITO-OK
              IF WGRZ-ID-1O-ASSTO-NULL(IX-TAB-GRZ) NOT = HIGH-VALUES
                 MOVE WGRZ-ID-1O-ASSTO(IX-TAB-GRZ)
                   TO WK-ID-ASSTO
                 PERFORM RICERCA-RAN
                    THRU RICERCA-RAN-EX

      *    252 --> Data decesso Assicurato 10 Ass
                 IF IDSV0001-ESITO-OK
                 AND WS-FL-ASSIC-PRES
                   IF WGRZ-TP-PRSTZ-ASSTA(IX-TAB-GRZ) = 'TF'
                    IF RAN-DT-DECES-NULL NOT = HIGH-VALUES
                       AND RAN-DT-DECES > 0
                       MOVE 'M' TO WB03-STAT-TBGC-ASSTO-1
                    ELSE
                       MOVE 'V' TO WB03-STAT-TBGC-ASSTO-1
                    END-IF
                   END-IF
      *    263 --> Stato tabagico assicurato  10 Ass
                    IF RAN-FL-FUMATORE-NULL NOT = HIGH-VALUES
                       MOVE RAN-FL-FUMATORE TO
                                    WB03-STAT-TBGC-ASSTO-1
                    END-IF
      *    263 --> Stato tabagico assicurato  10 Ass FINE
                 END-IF

      *    252 --> Data decesso Assicurato 10 Ass
      *          IF IDSV0001-ESITO-OK
      *          AND WS-FL-ASSIC-PRES
      *             PERFORM S0200-PREP-LETT-PERS
      *                THRU EX-S0200
      *          END-IF
      *          IF  IDSV0001-ESITO-OK
      *          AND WS-FL-ASSIC-PRES
      *             PERFORM S0201-LEGGI-PERS
      *                THRU EX-S0201
      *          END-IF

                 IF  IDSV0001-ESITO-OK
                 AND WS-FL-ASSIC-PRES
                    IF WA25-IND-CLI(IX-WRAN) = 'F'
                       IF WA25-COD-FISC-NULL(IX-WRAN) NOT = HIGH-VALUES
                          MOVE WA25-COD-FISC(IX-WRAN)
                                           TO WB03-COD-FISC-ASSTO1
                       END-IF
                    END-IF
                 END-IF

              END-IF
           END-IF

VSTEF *    I CAMPI RELATIVI ALL'ASSICURATO VANNO VALORIZZATI SOLO SE
      *    E' EFFETTIVAMENTE PRESENTE L'ASSICURATO
ALEX       IF WS-FL-ASSIC-PRES

      *------ DATA NASCITA PRIMO ASSICURATO
           IF RAN-DT-NASC-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-DT-NASC-1O-ASSTO-NULL
           ELSE
              MOVE RAN-DT-NASC
                TO WB03-DT-NASC-1O-ASSTO
           END-IF

      *------ SESSO PRIMO ASSICURATO
           IF RAN-SEX-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-SEX-1O-ASSTO-NULL
           ELSE
              MOVE RAN-SEX
                TO WB03-SEX-1O-ASSTO
           END-IF

           IF RAN-DT-NASC-NULL NOT = HIGH-VALUES

      *-------   ETA' RAGGIUNTA DATA CALCOLO
              MOVE ZERO                        TO WS-DATA-APPO-NUM
                                                  DATA-SUPERIORE
                                                  DATA-INFERIORE
                                                  GG-DIFF
              MOVE WS-DT-CALCOLO-NUM           TO DATA-SUPERIORE
              MOVE RAN-DT-NASC                 TO WS-DATA-APPO-NUM
              MOVE WS-DATA-APPO-NUM            TO DATA-INFERIORE
              MOVE 'A'                         TO FORMATO
              INITIALIZE                       WS-ETA-RAGGN-DT-CALC
                                               WS-ETA-RAGGN-DT-CALC-V

              PERFORM S2900-CALCOLO-DIFF-DATE
                 THRU EX-S2900

              IF CODICE-RITORNO = '0'

VSTEF            DIVIDE GG-DIFF BY 360 GIVING DUR-AAAA
                                    REMAINDER WK-APPO-RESTO1

                 IF WK-APPO-RESTO1 > 0
                    DIVIDE WK-APPO-RESTO1 BY 30 GIVING DUR-MM
                 ELSE
                    MOVE 0                TO DUR-MM
                 END-IF

                 IF DUR-MM = 12
                    ADD  1                TO DUR-AAAA
                    MOVE 0                TO DUR-MM
                 END-IF

                 MOVE DUR-AAAA            TO WS-ETA-RAGGN-DT-CALC-NUM
                 COMPUTE WS-ETA-RAGGN-DT-CALC-DEC = DUR-MM * 10
                 MOVE WS-ETA-RAGGN-DT-CALC TO WB03-ETA-RAGGN-DT-CALC
              ELSE
                 MOVE HIGH-VALUES  TO WB03-ETA-RAGGN-DT-CALC-NULL
              END-IF
???   *      ELSE
???   *        MOVE HIGH-VALUES     TO WB03-ETA-RAGGN-DT-CALC-NULL
???   *        END-IF
           ELSE
              MOVE HIGH-VALUES
                TO WB03-DT-NASC-1O-ASSTO-NULL
              MOVE HIGH-VALUES
                TO WB03-SEX-1O-ASSTO-NULL
              MOVE HIGH-VALUES
                TO WB03-ETA-RAGGN-DT-CALC-NULL
           END-IF.

VSTEF *     SIR: VEDI EMAIL DI CRESPO DEL 23/12/2009
      *     I CAMPI DEVONO RIPORTARE I DATI DELL'ASSICURATO,
      *     PERTANTO SE NON SONO PRESENTI I DATI DELL'ASSICURATO
      *     QUESTI CAMPI NON DEVONO ESSERE VALORIZZATI

VSTEF **--> SE NON E' PRESENTE L'ASSICURATO LEGGO LA RAPP-ANAG
      **    DELL'ADERENTE (SE PRESENTE)
      *     IF IDSV0001-ESITO-OK  AND  WS-FL-ASSIC-NON-PRES
      *        PERFORM S0161-PREP-ACC-RAN-ADER
      *           THRU EX-S0161
      *        PERFORM S0165-LEGGI-RAPP-ANA
      *           THRU EX-S0165
      *     END-IF.
      *
VSTEF **--> SE NON E' PRESENTE L'ADERENTE LEGGO LA RAPP-ANAG
      **    DEL CONTRAENTE
      *     IF  IDSV0001-ESITO-OK
      *     AND WS-FL-ASSIC-NON-PRES
      *     AND WS-FL-ADER-NON-PRES
      *        PERFORM S0162-PREP-ACC-RAN-CONT
      *           THRU EX-S0162
      *        PERFORM S0165-LEGGI-RAPP-ANA
      *           THRU EX-S0165
      *     END-IF.

ALEX       IF IDSV0001-ESITO-OK
              IF WGRZ-ID-2O-ASSTO-NULL(IX-TAB-GRZ) NOT = HIGH-VALUES
                 MOVE WGRZ-ID-2O-ASSTO(IX-TAB-GRZ)
                   TO WK-ID-ASSTO
                 PERFORM RICERCA-RAN
                    THRU RICERCA-RAN-EX

      *    253 --> Data decesso Assicurato 20 Ass
                 IF IDSV0001-ESITO-OK
                 AND WS-FL-ASSIC-PRES
                   IF WGRZ-TP-PRSTZ-ASSTA(IX-TAB-GRZ) = 'TF'
                    IF RAN-DT-DECES-NULL NOT = HIGH-VALUES
                       AND RAN-DT-DECES > 0
                       MOVE 'M' TO WB03-STAT-TBGC-ASSTO-2
                    ELSE
                       MOVE 'V' TO WB03-STAT-TBGC-ASSTO-2
                    END-IF
                   END-IF
      *    264 --> Stato tabagico assicurato  20 Ass
                    IF RAN-FL-FUMATORE-NULL NOT = HIGH-VALUES
                       MOVE RAN-FL-FUMATORE TO
                                    WB03-STAT-TBGC-ASSTO-2
                    END-IF
      *    264 --> Stato tabagico assicurato  20 Ass FINE
                 END-IF
      *    253 --> Data decesso Assicurato 20 Ass

      *          IF IDSV0001-ESITO-OK
      *          AND WS-FL-ASSIC-PRES
      *             PERFORM S0200-PREP-LETT-PERS
      *                THRU EX-S0200
      *          END-IF
      *          IF  IDSV0001-ESITO-OK
      *          AND WS-FL-ASSIC-PRES
      *             PERFORM S0201-LEGGI-PERS
      *                THRU EX-S0201
      *          END-IF

      *   valorizzazione codice fiscale presente in unipol
      *           IF  IDSV0001-ESITO-OK
      *           AND WS-FL-ASSIC-PRES
      *              IF WA25-IND-CLI(IX-WRAN) = 'F'
      *                 IF WA25-COD-FISC-NULL(IX-WRAN) NOT = HIGH-VALUES
      *                    MOVE WA25-COD-FISC(IX-WRAN)
      *                                TO  WB03-COD-FISC-ASSTO2
      *                 END-IF
      *             END-IF
      *           END-IF
              END-IF
           END-IF

ALEX       IF IDSV0001-ESITO-OK
              IF WGRZ-ID-3O-ASSTO-NULL(IX-TAB-GRZ) NOT = HIGH-VALUES
                 MOVE WGRZ-ID-3O-ASSTO(IX-TAB-GRZ)
                   TO WK-ID-ASSTO
                 PERFORM RICERCA-RAN
                    THRU RICERCA-RAN-EX

      *    254 --> Data decesso Assicurato 30 Ass
                 IF IDSV0001-ESITO-OK
                 AND WS-FL-ASSIC-PRES
                   IF WGRZ-TP-PRSTZ-ASSTA(IX-TAB-GRZ) = 'TF'
                    IF RAN-DT-DECES-NULL NOT = HIGH-VALUES
                       AND RAN-DT-DECES > 0
                       MOVE 'M' TO WB03-STAT-TBGC-ASSTO-3
                    ELSE
                       MOVE 'V' TO WB03-STAT-TBGC-ASSTO-3
                    END-IF
                   END-IF
      *    265 --> Stato tabagico assicurato  30 Ass
                    IF RAN-FL-FUMATORE-NULL NOT = HIGH-VALUES
                       MOVE RAN-FL-FUMATORE TO
                                    WB03-STAT-TBGC-ASSTO-3
                    END-IF
      *    265 --> Stato tabagico assicurato  30 Ass FINE
                 END-IF
      *    254 --> Data decesso Assicurato 30 Ass

      *          IF IDSV0001-ESITO-OK
      *          AND WS-FL-ASSIC-PRES
      *             PERFORM S0200-PREP-LETT-PERS
      *                THRU EX-S0200
      *          END-IF
      *          IF  IDSV0001-ESITO-OK
      *          AND WS-FL-ASSIC-PRES
      *             PERFORM S0201-LEGGI-PERS
      *                THRU EX-S0201
      *          END-IF
      *   valorizzazione codice fiscale presente in unipol
      *           IF  IDSV0001-ESITO-OK
      *           AND WS-FL-ASSIC-PRES
      *              IF WA25-IND-CLI(IX-WRAN) = 'F'
      *                 IF WA25-COD-FISC-NULL(IX-WRAN) NOT = HIGH-VALUES
      *                    MOVE WA25-COD-FISC(IX-WRAN)
      *                                      TO WB03-COD-FISC-ASSTO3
      *                 END-IF
      *             END-IF
      *           END-IF

              END-IF
           END-IF.


       EX-S1150.
           EXIT.
      *-----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella RAPP_ANA
      *-----------------------------------------------------------------
       S0160-PREP-ACC-RAN-ASS.

           MOVE 'S0160-PREP-ACC-RAN-ASS'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> TIPO RAPPORTO ANAGRAFICO
           SET ASSICURATO                     TO TRUE.

      *--> VALORIZZAZIONE DCLGEN
           INITIALIZE                            RAPP-ANA.
           MOVE WK-ID-ASSTO                   TO RAN-ID-RAPP-ANA.
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE WS-DT-EFFETTO          TO IDSI0011-DATA-INIZIO-EFFETTO
                                           IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF            TO IDSI0011-DATA-COMPETENZA.
      *--> TRATTAMENTO-STORICITA
           SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.
      *
      *  --> Nome tabella fisica db
           MOVE 'RAPP-ANA'              TO IDSI0011-CODICE-STR-DATO.
      *  --> Dclgen tabella
PERFOR*    MOVE SPACES                  TO IDSI0011-BUFFER-DATI.
           MOVE RAPP-ANA                TO IDSI0011-BUFFER-DATI.
      *--> TIPO OPERAZIONE
           SET  IDSI0011-SELECT         TO TRUE.
      *--> LIVELLO OPERAZIONE
           SET  IDSI0011-ID             TO TRUE.
      *  --> Modalita di accesso
           SET IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL  TO TRUE.

       EX-S0160.
           EXIT.

      *-----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella RAPP_ANA
      *-----------------------------------------------------------------
       S0161-PREP-ACC-RAN-ADER.

           MOVE 'S0161-PREP-ACC-RAN-ADER'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> TIPO RAPPORTO ANAGRAFICO
           SET ADERENTE                        TO TRUE.

      *--> VALORIZZAZIONE DCLGEN
           INITIALIZE                          RAPP-ANA.

           MOVE WADE-ID-ADES(IX-TAB-ADE)    TO LDBV1241-ID-OGG.
           MOVE 'AD'                        TO LDBV1241-TP-OGG.
           MOVE WS-TP-RAPP-ANA              TO LDBV1241-TP-RAPP-ANA.

      *  --> La data effetto viene sempre valorizzata
           MOVE WS-DT-EFFETTO          TO IDSI0011-DATA-INIZIO-EFFETTO
                                           IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF            TO IDSI0011-DATA-COMPETENZA.

      *--> TRATTAMENTO PER STORICITA
           SET IDSI0011-TRATT-DEFAULT   TO TRUE.

      *--> LIVELLO OPERAZIONE
           SET IDSI0011-WHERE-CONDITION TO TRUE.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT          TO TRUE.

      *  --> Nome tabella fisica db
           MOVE 'LDBS1240'              TO IDSI0011-CODICE-STR-DATO.

      *  --> Dclgen tabella
           MOVE SPACES                  TO IDSI0011-BUFFER-WHERE-COND.
           MOVE LDBV1241                TO IDSI0011-BUFFER-WHERE-COND.

           SET IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL  TO TRUE.

       EX-S0161.
           EXIT.

      *-----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella RAPP_ANA
      *-----------------------------------------------------------------
       S0162-PREP-ACC-RAN-CONT.

           MOVE 'S0162-PREP-ACC-RAN-CONT'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> TIPO RAPPORTO ANAGRAFICO
           SET CONTRAENTE                      TO TRUE.

      *--> VALORIZZAZIONE DCLGEN
           INITIALIZE                          RAPP-ANA.

           MOVE WPOL-ID-POLI                TO LDBV1241-ID-OGG.
           MOVE 'PO'                        TO LDBV1241-TP-OGG.
           MOVE WS-TP-RAPP-ANA              TO LDBV1241-TP-RAPP-ANA.

      *  --> La data effetto viene sempre valorizzata
           MOVE WS-DT-EFFETTO          TO IDSI0011-DATA-INIZIO-EFFETTO
                                           IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF            TO IDSI0011-DATA-COMPETENZA.

      *--> TRATTAMENTO PER STORICITA
           SET IDSI0011-TRATT-DEFAULT   TO TRUE.

      *--> LIVELLO OPERAZIONE
           SET IDSI0011-WHERE-CONDITION TO TRUE.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT          TO TRUE.

      *  --> Nome tabella fisica db
           MOVE 'LDBS1240'              TO IDSI0011-CODICE-STR-DATO.

      *  --> Dclgen tabella
           MOVE SPACES                  TO IDSI0011-BUFFER-WHERE-COND.
           MOVE LDBV1241                TO IDSI0011-BUFFER-WHERE-COND.

           SET IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL  TO TRUE.

       EX-S0162.
           EXIT.

      * ----------------------------------------------------------------
      * Chiamata al dispatcher dati per l'accesso alla RAPP_ANA
      * ----------------------------------------------------------------
       S0165-LEGGI-RAPP-ANA.

           MOVE 'S0165-LEGGI-RAPP-ANA'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IDBSRAN0'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura RAPP_ANA'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IDBSRAN0'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura RAPP_ANA'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND

                       EVALUATE TRUE
                           WHEN ASSICURATO
                                SET WS-FL-ASSIC-NON-PRES   TO TRUE
                           WHEN ADERENTE
                                SET WS-FL-ADER-NON-PRES    TO TRUE
                           WHEN CONTRAENTE
      *-->                      CHIAVE NON TROVATA
                                MOVE WK-PGM
                                  TO IEAI9901-COD-SERVIZIO-BE
                                MOVE 'S0165-LEGGI-RAPP-ANA'
                                  TO IEAI9901-LABEL-ERR
                                MOVE '005056'
                                  TO IEAI9901-COD-ERRORE
                                MOVE 'RAPP-ANA'
                                  TO IEAI9901-PARAMETRI-ERR
                                PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                   THRU EX-S0300
                       END-EVALUATE

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *  --> Operazione eseguita correttamente
                       MOVE IDSO0011-BUFFER-DATI
                         TO RAPP-ANA

                       EVALUATE TRUE
                           WHEN ASSICURATO
                                SET WS-FL-ASSIC-PRES   TO TRUE
                           WHEN ADERENTE
                                SET WS-FL-ADER-PRES    TO TRUE
                           WHEN CONTRAENTE
                                CONTINUE
                       END-EVALUATE

                  WHEN OTHER
      *  --> Errore di accesso al db
                       MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0165-LEGGI-RAPP-ANA'
                                          TO IEAI9901-LABEL-ERR
                       MOVE '005016'      TO IEAI9901-COD-ERRORE
                       STRING 'RAPP-ANA;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *  --> Errore dispatcher
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0165-LEGGI-RAPP-ANA' TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              STRING 'RAPP-ANA;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S0165.
           EXIT.
      * ----------------------------------------------------------------
      * PREPARAZIONE AREA E CHIAMATA AL VALORIZZATORE VARIABILI
      * ----------------------------------------------------------------
       S0500-CALL-VAL-VAR.

           MOVE 'S0500-CALL-VAL-VAR'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           IF  IDSV0001-ESITO-OK
           AND IABV0006-PRIMO-LANCIO
              PERFORM S0499-RECUP-PARAM-COMP
                 THRU EX-S0499
           END-IF.

           IF IDSV0001-ESITO-OK
              PERFORM S0510-PREPARA-AREA-VV
                 THRU EX-S0510
           END-IF.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IVVS0211'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'VV STEP:'
                   DELIMITED BY SIZE
                   S211-STEP-ELAB
                   DELIMITED BY SIZE
                   ' TP-MOVI:'
                   DELIMITED BY SIZE
                   S211-TIPO-MOVIMENTO
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
      *
           IF IDSV0001-ESITO-OK
              PERFORM CALL-VALORIZZATORE
                 THRU CALL-VALORIZZATORE-EX
           END-IF.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IVVS0211'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'VV STEP:'
                   DELIMITED BY SIZE
                   S211-STEP-ELAB
                   DELIMITED BY SIZE
                   ' TP-MOVI:'
                   DELIMITED BY SIZE
                   S211-TIPO-MOVIMENTO
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
      *
           IF IDSV0001-ESITO-OK
              MOVE S211-BUFFER-DATI
                TO WSKD-AREA-SCHEDA
           ELSE
             MOVE WADE-ID-ADES(IX-TAB-ADE) TO WK-ADESIONE-9
             MOVE WK-PGM
               TO IEAI9901-COD-SERVIZIO-BE
             MOVE 'S0500-CALL-VAL-VAR'
               TO IEAI9901-LABEL-ERR
             MOVE '001114'
               TO IEAI9901-COD-ERRORE
             MOVE SPACES
               TO IEAI9901-PARAMETRI-ERR
             STRING 'ERRORE CHIAMATA VALORIZZATORE;'
                    'ID-ADESIONE: ' WK-ADESIONE-9
             DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
             END-STRING
             PERFORM S0300-RICERCA-GRAVITA-ERRORE
                THRU EX-S0300
           END-IF.
      *
       EX-S0500.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA LA VARIABILE DI CONTESTO ISPROIEZ$
      *----------------------------------------------------------------*
       S0505-PREP-AREA-CONTESTO.

           ADD  1        TO IX-CNT WCNT-ELE-VAR-CONT-MAX
           MOVE 'ISPROIEZ$'
             TO WCNT-COD-VAR-CONT(IX-CNT)
           MOVE 'S'
             TO WCNT-TP-DATO-CONT(IX-CNT)
           MOVE WLB-FL-CALCOLO-A-DATA-FUTURA
             TO WCNT-VAL-STR-CONT(IX-CNT).
      *
       EX-S0505.
           EXIT.
      * ----------------------------------------------------------------
      *    PREPAREA AREA INPUT PER IL VALORIZZATORE VARIABILI
      * ----------------------------------------------------------------
       S0510-PREPARA-AREA-VV.

           MOVE 'S0510-PREPARA-AREA-VV'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *
      *     Inizializzazione area dati contestuali
      *
           INITIALIZE                         WCNT-ELE-VAR-CONT-MAX
                                              WCNT-TAB-VAR-CONT(1).

           MOVE  WCNT-TAB-VAR              TO WCNT-RESTO-TAB-VAR.

           IF IDSV0001-ESITO-OK
              PERFORM S0505-PREP-AREA-CONTESTO
                 THRU EX-S0505
           END-IF.
      *
      *    Inizializzazione area io valorizzatore variabile
      *
           INITIALIZE S211-DATI-INPUT
                      S211-RESTO-DATI
                      S211-TAB-INFO(1).

           MOVE  S211-TAB-INFO1     TO  S211-RESTO-TAB-INFO1.
      *
           SET S211-AREA-PV                    TO TRUE.
           SET S211-IN-CONV                    TO TRUE.
           MOVE WPOL-DT-INI-VLDT-PROD
             TO S211-DATA-ULT-VERS-PROD


      *--> Valorizzazione della struttura di mapping
           MOVE ZERO                        TO IX-CONTA
           MOVE 1                           TO WK-APPO-LUNGHEZZA

      *
           MOVE 6204
             TO S211-TIPO-MOVIMENTO.
           MOVE SPACES
             TO S211-COD-MAIN-BATCH.
           MOVE SPACES
             TO S211-COD-SERVIZIO-BE.
      *
           MOVE ZEROES
             TO S211-DATA-COMPETENZA.

      *    IF GESTIONE-RISERVE-RAMO-OPERAZ
      *       IF RISC-PARZ-TROVATO-SI
      *          MOVE WK-MOV-DT-EFF   TO  S211-DATA-EFFETTO
      *       END-IF
      *    END-IF
      *

      *
      *--> OCCORRENZA 1
           ADD  1                           TO IX-CONTA
           MOVE S211-ALIAS-POLI             TO S211-TAB-ALIAS(IX-CONTA)
           MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-CONTA)
           MOVE LENGTH OF WPOL-AREA-POLIZZA TO S211-LUNGHEZZA(IX-CONTA)

           COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                       S211-LUNGHEZZA(IX-CONTA) + 1
      *-->  Valorizzazione del buffer dati

           MOVE WPOL-AREA-POLIZZA
             TO S211-BUFFER-DATI(S211-POSIZ-INI(IX-CONTA):
                                 S211-LUNGHEZZA(IX-CONTA))

      *--> OCCORRENZA 2
           ADD  1                           TO IX-CONTA
           MOVE S211-ALIAS-ADES             TO S211-TAB-ALIAS(IX-CONTA)
           MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-CONTA)
           MOVE LENGTH OF WADE-AREA-ADESIONE
                                            TO S211-LUNGHEZZA(IX-CONTA)

           COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                       S211-LUNGHEZZA(IX-CONTA) + 1
      *--> Valorizzazione del buffer dati

           MOVE WADE-AREA-ADESIONE
             TO S211-BUFFER-DATI(S211-POSIZ-INI(IX-CONTA):
                                  S211-LUNGHEZZA(IX-CONTA))
      **--> OCCORRENZA 3
           ADD 1                            TO IX-CONTA
           MOVE S211-ALIAS-GARANZIA         TO S211-TAB-ALIAS(IX-CONTA)
           MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-CONTA)
           MOVE LENGTH OF WGRZ-AREA-GARANZIA
                                            TO S211-LUNGHEZZA(IX-CONTA)

           COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                       S211-LUNGHEZZA(IX-CONTA) + 1
      *--> Valorizzazione del buffer dati
           MOVE WGRZ-AREA-GARANZIA
             TO S211-BUFFER-DATI(S211-POSIZ-INI(IX-CONTA):
                                 S211-LUNGHEZZA(IX-CONTA))

      *--> OCCORRENZA 4
           ADD 1                            TO IX-CONTA
           MOVE S211-ALIAS-TRCH-GAR         TO S211-TAB-ALIAS(IX-CONTA)
           MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-CONTA)
           MOVE LENGTH OF WTGA-AREA-TGA
                                            TO S211-LUNGHEZZA(IX-CONTA)

           COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                          S211-LUNGHEZZA(IX-CONTA) + 1
      *-->  Valorizzazione del buffer dati
           MOVE WTGA-AREA-TGA
             TO S211-BUFFER-DATI(S211-POSIZ-INI(IX-CONTA):
                                 S211-LUNGHEZZA(IX-CONTA))

           IF SI-GAR-RAMO-III
      *-->    OCCORRENZA 05
              ADD 1                          TO IX-CONTA
              MOVE S211-ALIAS-QOTAZ-FON      TO S211-TAB-ALIAS(IX-CONTA)
              MOVE WK-APPO-LUNGHEZZA         TO S211-POSIZ-INI(IX-CONTA)
              MOVE LENGTH OF WL19-AREA-FONDI TO S211-LUNGHEZZA(IX-CONTA)

              COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                          S211-LUNGHEZZA(IX-CONTA) + 1

      *-->     Valorizzazione del buffer dati
              MOVE WL19-AREA-FONDI
                TO S211-BUFFER-DATI(S211-POSIZ-INI(IX-CONTA):
                                    S211-LUNGHEZZA(IX-CONTA))
           END-IF.

      *--> OCCORRENZA 6
           ADD 1                            TO IX-CONTA
           MOVE S211-ALIAS-PARAM-COMP       TO S211-TAB-ALIAS(IX-CONTA)
           MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-CONTA)
           MOVE LENGTH OF WPCO-AREA-PARA-COM
                                            TO S211-LUNGHEZZA(IX-CONTA)

           COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                          S211-LUNGHEZZA(IX-CONTA) + 1
      *-->  Valorizzazione del buffer dati
           MOVE WPCO-AREA-PARA-COM
             TO S211-BUFFER-DATI(S211-POSIZ-INI(IX-CONTA):
                                 S211-LUNGHEZZA(IX-CONTA))

      *--> SE E' VALORIZZATA L'AREA DATI CONTESTUALI
           IF WCNT-ELE-VAR-CONT-MAX > ZEROES

      *--> OCCORRENZA 7
              ADD 1 TO IX-CONTA
              MOVE S211-ALIAS-DATI-CONTEST  TO S211-TAB-ALIAS(IX-CONTA)
              MOVE WK-APPO-LUNGHEZZA        TO S211-POSIZ-INI(IX-CONTA)
              MOVE LENGTH OF WCNT-AREA-DATI-CONTEST
                                            TO S211-LUNGHEZZA(IX-CONTA)

              COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                          S211-LUNGHEZZA(IX-CONTA) + 1

      *-->    Valorizzazione del buffer dati
              MOVE WCNT-AREA-DATI-CONTEST
                TO S211-BUFFER-DATI(S211-POSIZ-INI(IX-CONTA):
                                    S211-LUNGHEZZA(IX-CONTA))
           END-IF.

      *--> Valorizzazione numero di elementi
            MOVE IX-CONTA                     TO S211-ELE-INFO-MAX.

       EX-S0510.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE TABELLE BIL_ESTRATTE , VAR_T , VAR_P
      *----------------------------------------------------------------*
       S1050-VALORIZZA-OUT.

           MOVE 'S1050-VALORIZZA-OUT'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      * INIZIALIZZAZIONE
           PERFORM INIZIA-TOT-B03
              THRU INIZIA-TOT-B03-EX.

           IF IDSV0001-ESITO-OK
              PERFORM S0113-TRATTA-DFA
                 THRU EX-S0113
           END-IF.

           IF IDSV0001-ESITO-OK
              PERFORM S2800-LEGGI-RAPP-RETE
                 THRU EX-S2800
           END-IF.

ALEX  *    QUESTO ACCESSO VIENE UTILIZZATO PER PRELEVARE IL CODICE
      *    FISCALE DEL CONTRAENTE IN UNIPOL .
           IF IDSV0001-ESITO-OK
MIGCOL        IF WPOL-TP-FRM-ASSVA = 'CO'
MIGCOL           PERFORM S0161-PREP-ACC-RAN-ADER
MIGCOL              THRU EX-S0161
MIGCOL        ELSE
ALEX             PERFORM S0162-PREP-ACC-RAN-CONT
                    THRU EX-S0162
MIGCOL        END-IF
              PERFORM S0165-LEGGI-RAPP-ANA
                 THRU EX-S0165

              IF IDSV0001-ESITO-OK
                 PERFORM RRE-WB03
                    THRU RRE-WB03-EX
              END-IF

              IF IDSV0001-ESITO-OK
                 PERFORM S0200-PREP-LETT-PERS
                    THRU EX-S0200
                 IF IDSV0001-ESITO-OK
                    PERFORM S0201-LEGGI-PERS
                       THRU EX-S0201
                 END-IF
              END-IF
      *    QUESTA PARTE h PRESENTE NELLA VERSIONE DI UNIPOL
              MOVE HIGH-VALUE       TO WB03-COD-FISC-CNTR-NULL
              IF IDSV0001-ESITO-OK
                 IF A25-IND-CLI = 'F'
      *         se persona fisica
                    IF A25-COD-FISC-NULL NOT = HIGH-VALUES
                       MOVE A25-COD-FISC      TO WB03-COD-FISC-CNTR
                    END-IF
ALFR             ELSE
  "   *         se persona giuridica
  "                 IF A25-COD-PRT-IVA-NULL NOT = HIGH-VALUES
  "                    MOVE A25-COD-PRT-IVA   TO WB03-COD-FISC-CNTR
  "                 END-IF
                 END-IF
              END-IF
           END-IF.
      **********************************
      *MODIFICA X PERFORMANCE - MICHELE
      ***********************************
           IF IDSV0001-ESITO-OK
              PERFORM S0170-LETTURA-WRAN THRU S0170-EX
           END-IF

      * CUMULO DEI RISCATTI
           IF IDSV0001-ESITO-OK
              PERFORM S0580-RISCATTI THRU S0580-EX
           END-IF

      * DATA EFFETTO RIDUZIONE
           IF IDSV0001-ESITO-OK
              PERFORM S0600-DT-RIDUZIONE THRU S0600-EX
           END-IF

      *251 --> Produttore
           IF IDSV0001-ESITO-OK
               PERFORM S2801-LEGGI-RAPP-RETE-AC
               IF RRE-COD-ACQS-CNTRT-NULL NOT = HIGH-VALUES
                    MOVE RRE-COD-ACQS-CNTRT TO WB03-COD-PRDT
               END-IF
           END-IF
      *251 --> Produttore FINE
      *****************************************************************
      *****************************************************************


           PERFORM S1100-CICLO-GRZ
              THRU EX-S1100
           VARYING IX-TAB-GRZ FROM 1 BY 1
             UNTIL IX-TAB-GRZ > WGRZ-ELE-GRZ-MAX
                OR IDSV0001-ESITO-KO.

           IF IDSV0001-ESITO-OK
              PERFORM S1310-VALORIZZA-OUT-B04
                 THRU EX-S1310
           END-IF.

       EX-S1050.
           EXIT.
      * ----------------------------------------------------------------
      * Ciclo di lettura delle garanzie memorizzate in workarea
      * ----------------------------------------------------------------
       S1100-CICLO-GRZ.

           MOVE 'S1100-CICLO-GRZ'           TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY           THRU ESEGUI-DISPLAY-EX.

      *--> LEGGE LA RAPPORTO ANAGRAFICO LEGATA ALL'ASSICURATO
      *    O ALL'ADERENTE O AL CONTRAENTE
           IF IX-TAB-GRZ EQUAL 1
              PERFORM S1150-TRATTA-RAPP-ANA
                 THRU EX-S1150
           END-IF.

ALEX1      IF IDSV0001-ESITO-OK
ALEX          PERFORM S1110-CALCOLA-MARGINI
                 THRU EX-S1110
ALEX1      END-IF.

ALEX1      IF IDSV0001-ESITO-OK
ALEX1         PERFORM S1115-VALORIZZA-CARENZA
ALEX1            THRU EX-S1115
ALEX1      END-IF.

      * L'accesso alla tabella riduce le performance del batch.
      * L'accesso viene effettuato con una select puntuale nella routine
      * S1010-LETTURA-PMO
29398      IF IDSV0001-ESITO-OK
29398         PERFORM S1900-LETTURA-PARAM-MOVI
29398           THRU EX-S1900
29398      END-IF.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LLBS0230'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-INIZIO            TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'S1200-CICLO-TGA      '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM S1200-CICLO-TGA
              THRU EX-S1200
           VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TGA-MAX
                OR IDSV0001-ESITO-KO.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LLBS0230'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-FINE              TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'S1200-CICLO-TGA      '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S1100.
           EXIT.
      * ----------------------------------------------------------------
      * Ciclo di lettura delle tranches memorizzate in workarea
      * ----------------------------------------------------------------
       S1200-CICLO-TGA.

           MOVE 'S1200-CICLO-TGA'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

45630      SET  WS-FL-SCRIVI-TRCH-SI     TO TRUE

           IF WTGA-ID-GAR(IX-TAB-TGA) = WGRZ-ID-GAR(IX-TAB-GRZ)

              IF IDSV0001-ESITO-OK
                 PERFORM S1201-TRATTA-RST
                    THRU EX-S1201
              END-IF

              IF IDSV0001-ESITO-OK
                 SET  IDSV8888-BUSINESS-DBG      TO TRUE
                 MOVE 'LOOP0001'            TO IDSV8888-NOME-PGM
                 SET  IDSV8888-INIZIO            TO TRUE
                 MOVE SPACES                     TO IDSV8888-DESC-PGM
                 STRING 'Loop su WSTB-AREA-STB'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
                 END-STRING
                 PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

                 PERFORM VARYING IX-TAB-STB FROM 1 BY 1
                 UNTIL IX-TAB-STB > WSTB-ELE-STB-MAX

                      IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
                         WSTB-ID-OGG(IX-TAB-STB)
                         MOVE WSTB-TP-STAT-BUS(IX-TAB-STB)
                           TO WS-TP-STAT-BUS-TGA
                         MOVE WSTB-TP-CAUS(IX-TAB-STB)
                           TO WS-TP-CAUS-TGA
                      END-IF

                 END-PERFORM
                 SET  IDSV8888-BUSINESS-DBG      TO TRUE
                 MOVE 'LOOP0001'            TO IDSV8888-NOME-PGM
                 SET  IDSV8888-FINE              TO TRUE
                 MOVE SPACES                     TO IDSV8888-DESC-PGM
                 STRING 'Loop su WSTB-AREA-STB'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
                 END-STRING
                 PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

              END-IF

      *-->    LETTURA DETTAGLIO TITOLO CONTABILE
      *-->    PER RECUPERARE IL PREMIO TOTALE
              IF IDSV0001-ESITO-OK
                 MOVE WGRZ-TP-PER-PRE(IX-TAB-GRZ)    TO WS-TP-PER-PREMIO
                 MOVE HIGH-VALUE                     TO WS-PRE-TOT-NULL

                 EVALUATE TRUE
      *-->         IN CASO DI PREMIO UNICO O RICORRENTE ACCEDIAMO ALLA
      *-->         TABELLA DETTAGLIO TITOLO CONTABILE IN SELECT
      *-->         PER ESTRARRE  L'OCCORRENZA RELATIVA ALLA TRANCHE
      *-->         E VALORIZZO IL PREMIO TOTALE

                   WHEN WS-PREMIO-UNICO
                   WHEN WS-PREMIO-RICORRENTE
ALFR                    SET WK-FOUND-PRM-TOT   TO TRUE
 "                      PERFORM S1620-LEGGI-PREMIO
 "                         THRU EX-S1620

 "                      IF IDSV0001-ESITO-OK AND
 "                         WK-NOT-FOUND-PRM-TOT

 "                         MOVE WTGA-PRE-LRD-NULL(IX-TAB-TGA)
 "                           TO WS-PRE-TOT-NULL
 "                      END-IF

      *-->         IN CASO DI PREMIO ANNUALE ACCEDIAMO ALLA
      *-->         TABELLA DETTAGLIO TITOLO CONTABILE IN SELECT
      *-->         PER EFFETTUARE UNA SOMMA DI TUTTI I PREMI TOTALI
      *-->         INSERITI IN PORTAFOGLIO DAL PRIMO GENNAIO DELL'ANNO
      *-->         DELLA DATA RISERVA ALLA DATA RISERVA
                   WHEN WS-PREMIO-ANNUALE
                        PERFORM S1621-LEGGI-PREMIO-TOTALE
                           THRU EX-S1621

                 END-EVALUATE
              END-IF

      *-->    LETTURA TABELLA ESTENSIONE TRANCHE DI GARANZIA
              IF IDSV0001-ESITO-OK
                 PERFORM S0220-PREP-LET-EST-TRANCHE
                    THRU EX-S0220
                 PERFORM S0230-LET-EST-TRANCHE
                    THRU EX-S0230
              END-IF



              IF IDSV0001-ESITO-OK
                 PERFORM S1250-VALORIZZA-OUT-B03
                    THRU EX-S1250
              END-IF

      *--> B03
45630 *       IF IDSV0001-ESITO-OK
45630 *          IF WK-STRADA-TAB
45630 *                PERFORM S1260-CALL-EOC-B03
45630 *                   THRU EX-S1260
45630 *          ELSE
45630 *                PERFORM CALL-EOC-B03-SEQ
45630 *                   THRU CALL-EOC-B03-SEQ-EX
45630 *          END-IF
45630 *       END-IF

              IF IDSV0001-ESITO-OK
                 PERFORM S1300-VALORIZZA-OUT-B05
                    THRU EX-S1300
              END-IF

      *--> B03
45630         IF IDSV0001-ESITO-OK
45630           IF WS-FL-SCRIVI-TRCH-SI
45630            IF WK-STRADA-TAB
45630                  PERFORM S1260-CALL-EOC-B03
45630                     THRU EX-S1260
45630            ELSE
45630                  PERFORM CALL-EOC-B03-SEQ
45630                     THRU CALL-EOC-B03-SEQ-EX
45630            END-IF
45630           END-IF
45630         END-IF
           END-IF.

       EX-S1200.
           EXIT.

ALEX  * ----------------------------------------------------------------
      *  CALCOLA ALIQUOTE MARGINI RISERVA E CAP SOTTO RISCHIO
      * ----------------------------------------------------------------
       S1110-CALCOLA-MARGINI.

      *----ALIQUOTA MARGINE RISERVA
           MOVE 'PERCMARGSOLV'
             TO WK-COD-PARAM.
           PERFORM S0216-IMPOSTA-POG
              THRU EX-S0216.
      *
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       MOVE HIGH-VALUES TO WK-ALQ-MARG-RIS-NULL
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI    TO PARAM-OGG
                       MOVE HIGH-VALUES TO WK-ALQ-MARG-RIS-NULL
                       MOVE POG-TP-D    TO WS-TP-DATO
                       EVALUATE TRUE
                         WHEN TD-IMPORTO
                              MOVE POG-VAL-IMP    TO WK-ALQ-MARG-RIS
                         WHEN TD-TASSO
                              MOVE POG-VAL-TS     TO WK-ALQ-MARG-RIS
                         WHEN TD-NUMERICO
                              MOVE POG-VAL-NUM    TO WK-ALQ-MARG-RIS
                       END-EVALUATE
                  WHEN OTHER
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0216-IMPOSTA-POG'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'LDBS1130;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
           ELSE
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0216-IMPOSTA-POG'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'LDBS1130;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.

      *----ALIQUOTA MARGINE CAP SOTTO RISCHIO
           MOVE 'MARGCAPSRIS'
             TO WK-COD-PARAM.
           PERFORM S0216-IMPOSTA-POG
              THRU EX-S0216.
      *
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       MOVE HIGH-VALUES
                         TO WK-ALQ-MARG-C-SUBRSH-NULL

                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI
                         TO PARAM-OGG
                       MOVE HIGH-VALUES
                         TO WK-ALQ-MARG-C-SUBRSH-NULL
                       MOVE POG-TP-D
                         TO WS-TP-DATO
                       EVALUATE TRUE
                         WHEN TD-IMPORTO
                              MOVE POG-VAL-IMP
                                TO WK-ALQ-MARG-C-SUBRSH
                         WHEN TD-TASSO
                              MOVE POG-VAL-TS
                                TO WK-ALQ-MARG-C-SUBRSH
                         WHEN TD-NUMERICO
                              MOVE POG-VAL-NUM
                                TO WK-ALQ-MARG-C-SUBRSH
                       END-EVALUATE

                  WHEN OTHER
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0216-IMPOSTA-POG'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'LDBS1130;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
           ELSE
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0216-IMPOSTA-POG'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'LDBS1130;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.

       EX-S1110.
           EXIT.
      * ----------------------------------------------------------------
      *  VALORIZZA I MESI DI CARENZA ( MMCARENZA )
      * ----------------------------------------------------------------
       S1115-VALORIZZA-CARENZA.

      *----NUMERO MESI CARENZA
           MOVE 'MMCARENZA'
             TO WK-COD-PARAM.
           PERFORM S0216-IMPOSTA-POG
              THRU EX-S0216.
      *
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       MOVE HIGH-VALUES TO WK-MESI-CARENZA-NULL
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI    TO PARAM-OGG
                       MOVE POG-VAL-NUM TO WK-MESI-CARENZA
                  WHEN OTHER
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0216-IMPOSTA-POG'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'LDBS1130;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
           ELSE
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0216-IMPOSTA-POG'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'LDBS1130;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.

       EX-S1115.
           EXIT.
      * ----------------------------------------------------------------
      * Chiamata al modulo di scrittura squenziale BILA_TRCH_ESTR
      * ----------------------------------------------------------------
       CALL-EOC-B03-SEQ.

           MOVE 'CALL-EOC-B03-SEQ'    TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.


           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'FILESQS2'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Scritt. BILA_TRCH_ESTR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

      *     MOVE 'BILA_TRCH_ESTR' TO WK-TABELLA.

      *     PERFORM ESTR-SEQUENCE  THRU ESTR-SEQUENCE-EX.

           IF IDSV0001-ESITO-OK
              PERFORM WRITE-FILEOUT-B03  THRU
                      WRITE-FILEOUT-B03-EX
           END-IF.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'FILESQS2'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Scritt. BILA_TRCH_ESTR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.


       CALL-EOC-B03-SEQ-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    SCRITTURA FILE SEQUENZIALE X CARICAMENTO TAB. BILA_TRCH_ESTR
      *----------------------------------------------------------------*
       WRITE-FILEOUT-B03.

           MOVE 'WRITE-FILEOUT-B03'
             TO  IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY
              THRU ESEGUI-DISPLAY-EX

           PERFORM S1290-VAL-DCLGEN-B03 THRU EX-S1290.

           INITIALIZE WK-VAR-REC-SIZE-F02 W-B03-LEN-TOT
           COMPUTE WK-VAR-REC-SIZE-F02 = LENGTH OF W-B03-REC-FISSO
                                       + 2

           MOVE WK-VAR-REC-SIZE-F02            TO W-B03-LEN-TOT

           MOVE AREA-OUT-W-B03                 TO FILESQS2-REC

           SET WRITE-FILESQS-OPER              TO TRUE
           SET WRITE-FILESQS2-SI               TO TRUE

           PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX

           IF SEQUENTIAL-ESITO-KO
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'WRITE-FILEOUT-B03'     TO IEAI9901-LABEL-ERR
              MOVE '005166'                  TO IEAI9901-COD-ERRORE
              MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
              MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       WRITE-FILEOUT-B03-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZAZIONE SEQUENZIALE  TAB. BILA_TRCH_ESTR
      *----------------------------------------------------------------*
       S1290-VAL-DCLGEN-B03.


           INITIALIZE  AREA-OUT-W-B03
      *                FILESQS2-REC.

            MOVE ZERO
               TO W-B03-ID-BILA-TRCH-ESTR
           MOVE WB03-COD-COMP-ANIA
              TO W-B03-COD-COMP-ANIA
           MOVE WB03-ID-RICH-ESTRAZ-MAS
              TO W-B03-ID-RICH-ESTRAZ-MAS
           IF WB03-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-ID-RICH-ESTRAZ-AGG-NULL
              MOVE X'6F'
              TO W-B03-ID-RICH-ESTRAZ-AGG-IND
           ELSE
              MOVE WB03-ID-RICH-ESTRAZ-AGG
              TO W-B03-ID-RICH-ESTRAZ-AGG
              MOVE X'F0'
              TO W-B03-ID-RICH-ESTRAZ-AGG-IND
           END-IF
           IF WB03-FL-SIMULAZIONE-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-FL-SIMULAZIONE-NULL
              MOVE X'6F'
              TO W-B03-FL-SIMULAZIONE-IND
           ELSE
              MOVE WB03-FL-SIMULAZIONE
              TO W-B03-FL-SIMULAZIONE
              MOVE X'F0'
              TO W-B03-FL-SIMULAZIONE-IND
           END-IF

           MOVE WB03-DT-RIS TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X   TO W-B03-DT-RIS

           MOVE WB03-DT-PRODUZIONE TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X   TO W-B03-DT-PRODUZIONE

           MOVE WB03-ID-POLI
              TO W-B03-ID-POLI
           MOVE WB03-ID-ADES
              TO W-B03-ID-ADES
           MOVE WB03-ID-GAR
              TO W-B03-ID-GAR
           MOVE WB03-ID-TRCH-DI-GAR
              TO W-B03-ID-TRCH-DI-GAR
           MOVE WB03-TP-FRM-ASSVA
              TO W-B03-TP-FRM-ASSVA
           MOVE WB03-TP-RAMO-BILA
              TO W-B03-TP-RAMO-BILA
           MOVE WB03-TP-CALC-RIS
              TO W-B03-TP-CALC-RIS
           MOVE WB03-COD-RAMO
              TO W-B03-COD-RAMO
           MOVE WB03-COD-TARI
              TO W-B03-COD-TARI
           IF WB03-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-INI-VAL-TAR-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-INI-VAL-TAR-NULL
           ELSE
             IF WB03-DT-INI-VAL-TAR = ZERO
                MOVE X'6F'
                TO W-B03-DT-INI-VAL-TAR-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-INI-VAL-TAR-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-INI-VAL-TAR-IND
              MOVE WB03-DT-INI-VAL-TAR TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-INI-VAL-TAR
             END-IF
           END-IF
           IF WB03-COD-PROD-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-COD-PROD-NULL
              MOVE X'6F'
              TO W-B03-COD-PROD-IND
           ELSE
              MOVE WB03-COD-PROD
              TO W-B03-COD-PROD
              MOVE X'F0'
              TO W-B03-COD-PROD-IND
           END-IF
           MOVE WB03-DT-INI-VLDT-PROD TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X   TO W-B03-DT-INI-VLDT-PROD
           IF WB03-COD-TARI-ORGN-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-COD-TARI-ORGN-NULL
              MOVE X'6F'
              TO W-B03-COD-TARI-ORGN-IND
           ELSE
              MOVE WB03-COD-TARI-ORGN
              TO W-B03-COD-TARI-ORGN
              MOVE X'F0'
              TO W-B03-COD-TARI-ORGN-IND
           END-IF
           IF WB03-MIN-GARTO-T-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-MIN-GARTO-T-NULL
              MOVE X'6F'
              TO W-B03-MIN-GARTO-T-IND
           ELSE
              MOVE WB03-MIN-GARTO-T
              TO W-B03-MIN-GARTO-T
              MOVE X'F0'
              TO W-B03-MIN-GARTO-T-IND
           END-IF
           IF WB03-TP-TARI-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-TP-TARI-NULL
              MOVE X'6F'
              TO W-B03-TP-TARI-IND
           ELSE
              MOVE WB03-TP-TARI
              TO W-B03-TP-TARI
              MOVE X'F0'
              TO W-B03-TP-TARI-IND
           END-IF
           IF WB03-TP-PRE-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-TP-PRE-NULL
              MOVE X'6F'
              TO W-B03-TP-PRE-IND
           ELSE
              MOVE WB03-TP-PRE
              TO W-B03-TP-PRE
              MOVE X'F0'
              TO W-B03-TP-PRE-IND
           END-IF
           IF WB03-TP-ADEG-PRE-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-TP-ADEG-PRE-NULL
              MOVE X'6F'
              TO W-B03-TP-ADEG-PRE-IND
           ELSE
              MOVE WB03-TP-ADEG-PRE
              TO W-B03-TP-ADEG-PRE
              MOVE X'F0'
              TO W-B03-TP-ADEG-PRE-IND
           END-IF
           IF WB03-TP-RIVAL-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-TP-RIVAL-NULL
              MOVE X'6F'
              TO W-B03-TP-RIVAL-IND
           ELSE
              MOVE WB03-TP-RIVAL
              TO W-B03-TP-RIVAL
              MOVE X'F0'
              TO W-B03-TP-RIVAL-IND
           END-IF
           IF WB03-FL-DA-TRASF-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-FL-DA-TRASF-NULL
              MOVE X'6F'
              TO W-B03-FL-DA-TRASF-IND
           ELSE
              MOVE WB03-FL-DA-TRASF
              TO W-B03-FL-DA-TRASF
              MOVE X'F0'
              TO W-B03-FL-DA-TRASF-IND
           END-IF
           IF WB03-FL-CAR-CONT-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-FL-CAR-CONT-NULL
              MOVE X'6F'
              TO W-B03-FL-CAR-CONT-IND
           ELSE
              MOVE WB03-FL-CAR-CONT
              TO W-B03-FL-CAR-CONT
              MOVE X'F0'
              TO W-B03-FL-CAR-CONT-IND
           END-IF
           IF WB03-FL-PRE-DA-RIS-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-FL-PRE-DA-RIS-NULL
              MOVE X'6F'
              TO W-B03-FL-PRE-DA-RIS-IND
           ELSE
              MOVE WB03-FL-PRE-DA-RIS
              TO W-B03-FL-PRE-DA-RIS
              MOVE X'F0'
              TO W-B03-FL-PRE-DA-RIS-IND
           END-IF
           IF WB03-FL-PRE-AGG-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-FL-PRE-AGG-NULL
              MOVE X'6F'
              TO W-B03-FL-PRE-AGG-IND
           ELSE
              MOVE WB03-FL-PRE-AGG
              TO W-B03-FL-PRE-AGG
              MOVE X'F0'
              TO W-B03-FL-PRE-AGG-IND
           END-IF
           IF WB03-TP-TRCH-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-TP-TRCH-NULL
              MOVE X'6F'
              TO W-B03-TP-TRCH-IND
           ELSE
              MOVE WB03-TP-TRCH
              TO W-B03-TP-TRCH
              MOVE X'F0'
              TO W-B03-TP-TRCH-IND
           END-IF
            IF WB03-TP-TST-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-TP-TST-NULL
              MOVE X'6F'
              TO W-B03-TP-TST-IND
           ELSE
              MOVE WB03-TP-TST
              TO W-B03-TP-TST
              MOVE X'F0'
              TO W-B03-TP-TST-IND
           END-IF
           IF WB03-COD-CONV-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-COD-CONV-NULL
              MOVE X'6F'
              TO W-B03-COD-CONV-IND
           ELSE
              MOVE WB03-COD-CONV
              TO W-B03-COD-CONV
              MOVE X'F0'
              TO W-B03-COD-CONV-IND
           END-IF
           MOVE WB03-DT-DECOR-POLI TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X   TO W-B03-DT-DECOR-POLI

           IF WB03-DT-DECOR-ADES-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-DECOR-ADES-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-DECOR-ADES-NULL
           ELSE
             IF WB03-DT-DECOR-ADES = ZERO
                MOVE X'6F'
                TO W-B03-DT-DECOR-ADES-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-DECOR-ADES-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-DECOR-ADES-IND
              MOVE WB03-DT-DECOR-ADES TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-DECOR-ADES
             END-IF
           END-IF

           MOVE WB03-DT-DECOR-TRCH TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X   TO W-B03-DT-DECOR-TRCH

           MOVE WB03-DT-EMIS-POLI TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X   TO W-B03-DT-EMIS-POLI

           IF WB03-DT-EMIS-TRCH-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-EMIS-TRCH-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-EMIS-TRCH-NULL
           ELSE
             IF WB03-DT-EMIS-TRCH = ZERO
                MOVE X'6F'
                TO W-B03-DT-EMIS-TRCH-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-EMIS-TRCH-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-EMIS-TRCH-IND
              MOVE WB03-DT-EMIS-TRCH TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-EMIS-TRCH
             END-IF
           END-IF

           IF WB03-DT-SCAD-TRCH-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-SCAD-TRCH-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-SCAD-TRCH-NULL
           ELSE
             IF WB03-DT-SCAD-TRCH = ZERO
                MOVE X'6F'
                TO W-B03-DT-SCAD-TRCH-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-SCAD-TRCH-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-SCAD-TRCH-IND
              MOVE WB03-DT-SCAD-TRCH TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-SCAD-TRCH
             END-IF
           END-IF

           IF WB03-DT-SCAD-INTMD-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-SCAD-INTMD-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-SCAD-INTMD-NULL
           ELSE
             IF WB03-DT-SCAD-INTMD = ZERO
                MOVE X'6F'
                TO W-B03-DT-SCAD-INTMD-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-SCAD-INTMD-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-SCAD-INTMD-IND
              MOVE WB03-DT-SCAD-INTMD TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-SCAD-INTMD
             END-IF
           END-IF

           IF WB03-DT-SCAD-PAG-PRE-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-SCAD-PAG-PRE-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-SCAD-PAG-PRE-NULL
           ELSE
             IF WB03-DT-SCAD-PAG-PRE = ZERO
                MOVE X'6F'
                TO W-B03-DT-SCAD-PAG-PRE-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-SCAD-PAG-PRE-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-SCAD-PAG-PRE-IND
              MOVE WB03-DT-SCAD-PAG-PRE TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-SCAD-PAG-PRE
             END-IF
           END-IF

           IF WB03-DT-ULT-PRE-PAG-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-ULT-PRE-PAG-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-ULT-PRE-PAG-NULL
           ELSE
             IF WB03-DT-ULT-PRE-PAG = ZERO
                MOVE X'6F'
                TO W-B03-DT-ULT-PRE-PAG-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-ULT-PRE-PAG-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-ULT-PRE-PAG-IND
              MOVE WB03-DT-ULT-PRE-PAG TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-ULT-PRE-PAG
             END-IF
           END-IF

           IF WB03-DT-NASC-1O-ASSTO-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-NASC-1O-ASSTO-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-NASC-1O-ASSTO-NULL
           ELSE
             IF WB03-DT-NASC-1O-ASSTO = ZERO
                MOVE X'6F'
                TO W-B03-DT-NASC-1O-ASSTO-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-NASC-1O-ASSTO-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-NASC-1O-ASSTO-IND
              MOVE WB03-DT-NASC-1O-ASSTO TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-NASC-1O-ASSTO
             END-IF
           END-IF

           IF WB03-SEX-1O-ASSTO-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-SEX-1O-ASSTO-IND
              MOVE HIGH-VALUE
              TO W-B03-SEX-1O-ASSTO-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-SEX-1O-ASSTO-IND
              MOVE WB03-SEX-1O-ASSTO
                TO W-B03-SEX-1O-ASSTO
             END-IF

           IF WB03-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-ETA-AA-1O-ASSTO-IND
              MOVE HIGH-VALUE
              TO W-B03-ETA-AA-1O-ASSTO-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-ETA-AA-1O-ASSTO-IND
              MOVE WB03-ETA-AA-1O-ASSTO TO
                   W-B03-ETA-AA-1O-ASSTO
           END-IF

           IF WB03-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-ETA-MM-1O-ASSTO-IND
              MOVE HIGH-VALUE
              TO W-B03-ETA-MM-1O-ASSTO-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-ETA-MM-1O-ASSTO-IND
              MOVE WB03-ETA-MM-1O-ASSTO TO
                    W-B03-ETA-MM-1O-ASSTO
           END-IF

           IF WB03-ETA-RAGGN-DT-CALC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-ETA-RAGGN-DT-CALC-IND
              MOVE HIGH-VALUE
              TO W-B03-ETA-RAGGN-DT-CALC-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-ETA-RAGGN-DT-CALC-IND
              MOVE WB03-ETA-RAGGN-DT-CALC
                  TO W-B03-ETA-RAGGN-DT-CALC
           END-IF

           IF WB03-DUR-AA-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DUR-AA-IND
              MOVE HIGH-VALUE
              TO W-B03-DUR-AA-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-DUR-AA-IND
              MOVE WB03-DUR-AA
                  TO W-B03-DUR-AA
           END-IF

           IF WB03-DUR-MM-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DUR-MM-IND
              MOVE HIGH-VALUE
              TO W-B03-DUR-MM-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-DUR-MM-IND
              MOVE WB03-DUR-MM
                  TO W-B03-DUR-MM
           END-IF

           IF WB03-DUR-GG-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DUR-GG-IND
              MOVE HIGH-VALUE
              TO W-B03-DUR-GG-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-DUR-GG-IND
              MOVE WB03-DUR-GG
                  TO W-B03-DUR-GG
           END-IF

           IF WB03-DUR-1O-PER-AA-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DUR-1O-PER-AA-IND
              MOVE HIGH-VALUE
              TO W-B03-DUR-1O-PER-AA-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-DUR-1O-PER-AA-IND
              MOVE WB03-DUR-1O-PER-AA
                  TO W-B03-DUR-1O-PER-AA
           END-IF

           IF WB03-DUR-1O-PER-MM-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DUR-1O-PER-MM-IND
              MOVE HIGH-VALUE
              TO W-B03-DUR-1O-PER-MM-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-DUR-1O-PER-MM-IND
              MOVE WB03-DUR-1O-PER-MM
                  TO W-B03-DUR-1O-PER-MM
           END-IF

           IF WB03-DUR-1O-PER-GG-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DUR-1O-PER-GG-IND
              MOVE HIGH-VALUE
              TO W-B03-DUR-1O-PER-GG-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-DUR-1O-PER-GG-IND
              MOVE WB03-DUR-1O-PER-GG
                  TO W-B03-DUR-1O-PER-GG
           END-IF

           IF WB03-ANTIDUR-RICOR-PREC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-ANTIDUR-RICOR-PREC-IND
              MOVE HIGH-VALUE
              TO W-B03-ANTIDUR-RICOR-PREC-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-ANTIDUR-RICOR-PREC-IND
              MOVE WB03-ANTIDUR-RICOR-PREC
                  TO W-B03-ANTIDUR-RICOR-PREC
           END-IF

           IF WB03-DUR-RES-DT-CALC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DUR-RES-DT-CALC-IND
              MOVE HIGH-VALUE
              TO W-B03-DUR-RES-DT-CALC-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-DUR-RES-DT-CALC-IND
              MOVE WB03-DUR-RES-DT-CALC
                  TO W-B03-DUR-RES-DT-CALC
           END-IF

           MOVE WB03-TP-STAT-BUS-POLI
                 TO W-B03-TP-STAT-BUS-POLI

           MOVE WB03-TP-CAUS-POLI
                 TO W-B03-TP-CAUS-POLI

           MOVE WB03-TP-STAT-BUS-ADES
                 TO W-B03-TP-STAT-BUS-ADES

           MOVE WB03-TP-CAUS-ADES
              TO W-B03-TP-CAUS-ADES

           MOVE WB03-TP-STAT-BUS-TRCH
              TO W-B03-TP-STAT-BUS-TRCH

           MOVE WB03-TP-CAUS-TRCH
              TO W-B03-TP-CAUS-TRCH

           IF WB03-DT-EFF-CAMB-STAT-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-EFF-CAMB-STAT-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-EFF-CAMB-STAT-NULL
           ELSE
             IF WB03-DT-EFF-CAMB-STAT = ZERO
                MOVE X'6F'
                TO W-B03-DT-EFF-CAMB-STAT-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-EFF-CAMB-STAT-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-EFF-CAMB-STAT-IND
              MOVE WB03-DT-EFF-CAMB-STAT TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-EFF-CAMB-STAT
             END-IF
           END-IF

           IF WB03-DT-EMIS-CAMB-STAT-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-EMIS-CAMB-STAT-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-EMIS-CAMB-STAT-NULL
           ELSE
             IF WB03-DT-EMIS-CAMB-STAT = ZERO
                MOVE X'6F'
                TO W-B03-DT-EMIS-CAMB-STAT-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-EMIS-CAMB-STAT-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-EMIS-CAMB-STAT-IND
              MOVE WB03-DT-EMIS-CAMB-STAT TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-EMIS-CAMB-STAT
             END-IF
           END-IF

           IF WB03-DT-EFF-STAB-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-EFF-STAB-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-EFF-STAB-NULL
           ELSE
             IF WB03-DT-EFF-STAB = ZERO
                MOVE X'6F'
                TO W-B03-DT-EFF-STAB-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-EFF-STAB-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-EFF-STAB-IND
              MOVE WB03-DT-EFF-STAB TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-EFF-STAB
             END-IF
           END-IF

           IF WB03-CPT-DT-STAB-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-CPT-DT-STAB-IND
              MOVE HIGH-VALUE
              TO W-B03-CPT-DT-STAB-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-CPT-DT-STAB-IND
              MOVE WB03-CPT-DT-STAB
                  TO W-B03-CPT-DT-STAB
           END-IF

           IF WB03-DT-EFF-RIDZ-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-EFF-RIDZ-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-EFF-RIDZ-NULL
           ELSE
             IF WB03-DT-EFF-RIDZ = ZERO
                MOVE X'6F'
                TO W-B03-DT-EFF-RIDZ-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-EFF-RIDZ-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-EFF-RIDZ-IND
              MOVE WB03-DT-EFF-RIDZ TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-EFF-RIDZ
             END-IF
           END-IF

           IF WB03-DT-EMIS-RIDZ-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-EMIS-RIDZ-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-EMIS-RIDZ-NULL
           ELSE
             IF WB03-DT-EMIS-RIDZ = ZERO
                MOVE X'6F'
                TO W-B03-DT-EMIS-RIDZ-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-EMIS-RIDZ-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-EMIS-RIDZ-IND
              MOVE WB03-DT-EMIS-RIDZ TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-EMIS-RIDZ
             END-IF
           END-IF

           IF WB03-CPT-DT-RIDZ-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-CPT-DT-RIDZ-IND
              MOVE HIGH-VALUE
              TO W-B03-CPT-DT-RIDZ-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-CPT-DT-RIDZ-IND
              MOVE WB03-CPT-DT-RIDZ
                  TO W-B03-CPT-DT-RIDZ
           END-IF

           IF WB03-FRAZ-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-FRAZ-IND
              MOVE HIGH-VALUE
              TO W-B03-FRAZ-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-FRAZ-IND
              MOVE WB03-FRAZ
                  TO W-B03-FRAZ
           END-IF

           IF WB03-DUR-PAG-PRE-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DUR-PAG-PRE-IND
              MOVE HIGH-VALUE
              TO W-B03-DUR-PAG-PRE-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-DUR-PAG-PRE-IND
              MOVE WB03-DUR-PAG-PRE
                  TO W-B03-DUR-PAG-PRE
           END-IF

           IF WB03-NUM-PRE-PATT-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-NUM-PRE-PATT-IND
              MOVE HIGH-VALUE
              TO W-B03-NUM-PRE-PATT-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-NUM-PRE-PATT-IND
              MOVE WB03-NUM-PRE-PATT
                  TO W-B03-NUM-PRE-PATT
           END-IF

           IF WB03-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-FRAZ-INI-EROG-REN-IND
              MOVE HIGH-VALUE
              TO W-B03-FRAZ-INI-EROG-REN-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-FRAZ-INI-EROG-REN-IND
              MOVE WB03-FRAZ-INI-EROG-REN
                  TO W-B03-FRAZ-INI-EROG-REN
           END-IF

           IF WB03-AA-REN-CER-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-AA-REN-CER-IND
              MOVE HIGH-VALUE
              TO W-B03-AA-REN-CER-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-AA-REN-CER-IND
              MOVE WB03-AA-REN-CER
                  TO W-B03-AA-REN-CER
           END-IF

           IF WB03-RAT-REN-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-RAT-REN-IND
              MOVE HIGH-VALUE
              TO W-B03-RAT-REN-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-RAT-REN-IND
              MOVE WB03-RAT-REN
                  TO W-B03-RAT-REN
           END-IF

           IF WB03-COD-DIV-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-COD-DIV-IND
              MOVE HIGH-VALUE
              TO W-B03-COD-DIV-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-COD-DIV-IND
              MOVE WB03-COD-DIV
                  TO W-B03-COD-DIV
           END-IF

           IF WB03-RISCPAR-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-RISCPAR-IND
              MOVE HIGH-VALUE
              TO W-B03-RISCPAR-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-RISCPAR-IND
              MOVE WB03-RISCPAR
                  TO W-B03-RISCPAR
           END-IF

           IF WB03-CUM-RISCPAR-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-CUM-RISCPAR-IND
              MOVE HIGH-VALUE
              TO W-B03-CUM-RISCPAR-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-CUM-RISCPAR-IND
              MOVE WB03-CUM-RISCPAR
                  TO W-B03-CUM-RISCPAR
           END-IF

           IF WB03-ULT-RM-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-ULT-RM-IND
              MOVE HIGH-VALUE
              TO W-B03-ULT-RM-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-ULT-RM-IND
              MOVE WB03-ULT-RM
                  TO W-B03-ULT-RM
           END-IF

           IF WB03-TS-RENDTO-T-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TS-RENDTO-T-IND
              MOVE HIGH-VALUE
              TO W-B03-TS-RENDTO-T-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TS-RENDTO-T-IND
              MOVE WB03-TS-RENDTO-T
                  TO W-B03-TS-RENDTO-T
           END-IF

           IF WB03-ALQ-RETR-T-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-ALQ-RETR-T-IND
              MOVE HIGH-VALUE
              TO W-B03-ALQ-RETR-T-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-ALQ-RETR-T-IND
              MOVE WB03-ALQ-RETR-T
                  TO W-B03-ALQ-RETR-T
           END-IF

           IF WB03-MIN-TRNUT-T-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-MIN-TRNUT-T-IND
              MOVE HIGH-VALUE
              TO W-B03-MIN-TRNUT-T-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-MIN-TRNUT-T-IND
              MOVE WB03-MIN-TRNUT-T
                  TO W-B03-MIN-TRNUT-T
           END-IF

           IF WB03-TS-NET-T-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TS-NET-T-IND
              MOVE HIGH-VALUE
              TO W-B03-TS-NET-T-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TS-NET-T-IND
              MOVE WB03-TS-NET-T
                  TO W-B03-TS-NET-T
           END-IF

           IF WB03-DT-ULT-RIVAL-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-ULT-RIVAL-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-ULT-RIVAL-NULL
           ELSE
             IF WB03-DT-ULT-RIVAL = ZERO
                MOVE X'6F'
                TO W-B03-DT-ULT-RIVAL-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-ULT-RIVAL-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-ULT-RIVAL-IND
              MOVE WB03-DT-ULT-RIVAL TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-ULT-RIVAL
             END-IF
           END-IF

           IF WB03-PRSTZ-INI-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PRSTZ-INI-IND
              MOVE HIGH-VALUE
              TO W-B03-PRSTZ-INI-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PRSTZ-INI-IND
              MOVE WB03-PRSTZ-INI
                  TO W-B03-PRSTZ-INI
           END-IF

           IF WB03-PRSTZ-AGG-INI-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PRSTZ-AGG-INI-IND
              MOVE HIGH-VALUE
              TO W-B03-PRSTZ-AGG-INI-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PRSTZ-AGG-INI-IND
              MOVE WB03-PRSTZ-AGG-INI
                  TO W-B03-PRSTZ-AGG-INI
           END-IF

           IF WB03-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PRSTZ-AGG-ULT-IND
              MOVE HIGH-VALUE
              TO W-B03-PRSTZ-AGG-ULT-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PRSTZ-AGG-ULT-IND
              MOVE WB03-PRSTZ-AGG-ULT
                  TO W-B03-PRSTZ-AGG-ULT
           END-IF

           IF WB03-RAPPEL-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-RAPPEL-IND
              MOVE HIGH-VALUE
              TO W-B03-RAPPEL-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-RAPPEL-IND
              MOVE WB03-RAPPEL
                  TO W-B03-RAPPEL
           END-IF

           IF WB03-PRE-PATTUITO-INI-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PRE-PATTUITO-INI-IND
              MOVE HIGH-VALUE
              TO W-B03-PRE-PATTUITO-INI-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PRE-PATTUITO-INI-IND
              MOVE WB03-PRE-PATTUITO-INI
                  TO W-B03-PRE-PATTUITO-INI
           END-IF

           IF WB03-PRE-DOV-INI-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PRE-DOV-INI-IND
              MOVE HIGH-VALUE
              TO W-B03-PRE-DOV-INI-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PRE-DOV-INI-IND
              MOVE WB03-PRE-DOV-INI
                  TO W-B03-PRE-DOV-INI
           END-IF

           IF WB03-PRE-DOV-RIVTO-T-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PRE-DOV-RIVTO-T-IND
              MOVE HIGH-VALUE
              TO W-B03-PRE-DOV-RIVTO-T-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PRE-DOV-RIVTO-T-IND
              MOVE WB03-PRE-DOV-RIVTO-T
                  TO W-B03-PRE-DOV-RIVTO-T
           END-IF

           IF WB03-PRE-ANNUALIZ-RICOR-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PRE-ANNUALIZ-RICOR-IND
              MOVE HIGH-VALUE
              TO W-B03-PRE-ANNUALIZ-RICOR-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PRE-ANNUALIZ-RICOR-IND
              MOVE WB03-PRE-ANNUALIZ-RICOR
                  TO W-B03-PRE-ANNUALIZ-RICOR
           END-IF

           IF WB03-PRE-CONT-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PRE-CONT-IND
              MOVE HIGH-VALUE
              TO W-B03-PRE-CONT-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PRE-CONT-IND
              MOVE WB03-PRE-CONT
                  TO W-B03-PRE-CONT
           END-IF

           IF WB03-PRE-PP-INI-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PRE-PP-INI-IND
              MOVE HIGH-VALUE
              TO W-B03-PRE-PP-INI-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PRE-PP-INI-IND
              MOVE WB03-PRE-PP-INI
                  TO W-B03-PRE-PP-INI
           END-IF

           IF WB03-RIS-PURA-T-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-RIS-PURA-T-IND
              MOVE HIGH-VALUE
              TO W-B03-RIS-PURA-T-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-RIS-PURA-T-IND
              MOVE WB03-RIS-PURA-T
                  TO W-B03-RIS-PURA-T
           END-IF

           IF WB03-PROV-ACQ-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PROV-ACQ-IND
              MOVE HIGH-VALUE
              TO W-B03-PROV-ACQ-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PROV-ACQ-IND
              MOVE WB03-PROV-ACQ
                  TO W-B03-PROV-ACQ
           END-IF

           IF WB03-PROV-ACQ-RICOR-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PROV-ACQ-RICOR-IND
              MOVE HIGH-VALUE
              TO W-B03-PROV-ACQ-RICOR-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PROV-ACQ-RICOR-IND
              MOVE WB03-PROV-ACQ-RICOR
                  TO W-B03-PROV-ACQ-RICOR
           END-IF

           IF WB03-PROV-INC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PROV-INC-IND
              MOVE HIGH-VALUE
              TO W-B03-PROV-INC-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PROV-INC-IND
              MOVE WB03-PROV-INC
                  TO W-B03-PROV-INC
           END-IF

           IF WB03-CAR-ACQ-NON-SCON-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-CAR-ACQ-NON-SCON-IND
              MOVE HIGH-VALUE
              TO W-B03-CAR-ACQ-NON-SCON-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-CAR-ACQ-NON-SCON-IND
              MOVE WB03-CAR-ACQ-NON-SCON
                  TO W-B03-CAR-ACQ-NON-SCON
           END-IF

           IF WB03-OVER-COMM-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-OVER-COMM-IND
              MOVE HIGH-VALUE
              TO W-B03-OVER-COMM-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-OVER-COMM-IND
              MOVE WB03-OVER-COMM
                  TO W-B03-OVER-COMM
           END-IF

           IF WB03-CAR-ACQ-PRECONTATO-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-CAR-ACQ-PRECONTATO-IND
              MOVE HIGH-VALUE
              TO W-B03-CAR-ACQ-PRECONTATO-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-CAR-ACQ-PRECONTATO-IND
              MOVE WB03-CAR-ACQ-PRECONTATO
                  TO W-B03-CAR-ACQ-PRECONTATO
           END-IF

           IF WB03-RIS-ACQ-T-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-RIS-ACQ-T-IND
              MOVE HIGH-VALUE
              TO W-B03-RIS-ACQ-T-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-RIS-ACQ-T-IND
              MOVE WB03-RIS-ACQ-T
                  TO W-B03-RIS-ACQ-T
           END-IF

           IF WB03-RIS-ZIL-T-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-RIS-ZIL-T-IND
              MOVE HIGH-VALUE
              TO W-B03-RIS-ZIL-T-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-RIS-ZIL-T-IND
              MOVE WB03-RIS-ZIL-T
                  TO W-B03-RIS-ZIL-T
           END-IF

           IF WB03-CAR-GEST-NON-SCON-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-CAR-GEST-NON-SCON-IND
              MOVE HIGH-VALUE
              TO W-B03-CAR-GEST-NON-SCON-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-CAR-GEST-NON-SCON-IND
              MOVE WB03-CAR-GEST-NON-SCON
                  TO W-B03-CAR-GEST-NON-SCON
           END-IF

           IF WB03-CAR-GEST-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-CAR-GEST-IND
              MOVE HIGH-VALUE
              TO W-B03-CAR-GEST-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-CAR-GEST-IND
              MOVE WB03-CAR-GEST
                  TO W-B03-CAR-GEST
           END-IF

           IF WB03-RIS-SPE-T-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-RIS-SPE-T-IND
              MOVE HIGH-VALUE
              TO W-B03-RIS-SPE-T-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-RIS-SPE-T-IND
              MOVE WB03-RIS-SPE-T
                  TO W-B03-RIS-SPE-T
           END-IF

           IF WB03-CAR-INC-NON-SCON-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-CAR-INC-NON-SCON-IND
              MOVE HIGH-VALUE
              TO W-B03-CAR-INC-NON-SCON-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-CAR-INC-NON-SCON-IND
              MOVE WB03-CAR-INC-NON-SCON
                  TO W-B03-CAR-INC-NON-SCON
           END-IF

           IF WB03-CAR-INC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-CAR-INC-IND
              MOVE HIGH-VALUE
              TO W-B03-CAR-INC-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-CAR-INC-IND
              MOVE WB03-CAR-INC
                  TO W-B03-CAR-INC
           END-IF

           IF WB03-RIS-RISTORNI-CAP-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-RIS-RISTORNI-CAP-IND
              MOVE HIGH-VALUE
              TO W-B03-RIS-RISTORNI-CAP-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-RIS-RISTORNI-CAP-IND
              MOVE WB03-RIS-RISTORNI-CAP
                  TO W-B03-RIS-RISTORNI-CAP
           END-IF

           IF WB03-INTR-TECN-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-INTR-TECN-IND
              MOVE HIGH-VALUE
              TO W-B03-INTR-TECN-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-INTR-TECN-IND
              MOVE WB03-INTR-TECN
                  TO W-B03-INTR-TECN
           END-IF

           IF WB03-CPT-RSH-MOR-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-CPT-RSH-MOR-IND
              MOVE HIGH-VALUE
              TO W-B03-CPT-RSH-MOR-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-CPT-RSH-MOR-IND
              MOVE WB03-CPT-RSH-MOR
                  TO W-B03-CPT-RSH-MOR
           END-IF

           IF WB03-C-SUBRSH-T-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-C-SUBRSH-T-IND
              MOVE HIGH-VALUE
              TO W-B03-C-SUBRSH-T-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-C-SUBRSH-T-IND
              MOVE WB03-C-SUBRSH-T
                  TO W-B03-C-SUBRSH-T
           END-IF

           IF WB03-PRE-RSH-T-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PRE-RSH-T-IND
              MOVE HIGH-VALUE
              TO W-B03-PRE-RSH-T-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PRE-RSH-T-IND
              MOVE WB03-PRE-RSH-T
                  TO W-B03-PRE-RSH-T
           END-IF

           IF WB03-ALQ-MARG-RIS-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-ALQ-MARG-RIS-IND
              MOVE HIGH-VALUE
              TO W-B03-ALQ-MARG-RIS-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-ALQ-MARG-RIS-IND
              MOVE WB03-ALQ-MARG-RIS
                  TO W-B03-ALQ-MARG-RIS
           END-IF

           IF WB03-ALQ-MARG-C-SUBRSH-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-ALQ-MARG-C-SUBRSH-IND
              MOVE HIGH-VALUE
              TO W-B03-ALQ-MARG-C-SUBRSH-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-ALQ-MARG-C-SUBRSH-IND
              MOVE WB03-ALQ-MARG-C-SUBRSH
                  TO W-B03-ALQ-MARG-C-SUBRSH
           END-IF

           IF WB03-TS-RENDTO-SPPR-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TS-RENDTO-SPPR-IND
              MOVE HIGH-VALUE
              TO W-B03-TS-RENDTO-SPPR-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TS-RENDTO-SPPR-IND
              MOVE WB03-TS-RENDTO-SPPR
                  TO W-B03-TS-RENDTO-SPPR
           END-IF

           IF WB03-TP-IAS-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TP-IAS-IND
              MOVE HIGH-VALUE
              TO W-B03-TP-IAS-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TP-IAS-IND
              MOVE WB03-TP-IAS
                  TO W-B03-TP-IAS
           END-IF

           IF WB03-NS-QUO-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-NS-QUO-IND
              MOVE HIGH-VALUE
              TO W-B03-NS-QUO-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-NS-QUO-IND
              MOVE WB03-NS-QUO
                  TO W-B03-NS-QUO
           END-IF

           IF WB03-TS-MEDIO-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TS-MEDIO-IND
              MOVE HIGH-VALUE
              TO W-B03-TS-MEDIO-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TS-MEDIO-IND
              MOVE WB03-TS-MEDIO
                  TO W-B03-TS-MEDIO
           END-IF

           IF WB03-CPT-RIASTO-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-CPT-RIASTO-IND
              MOVE HIGH-VALUE
              TO W-B03-CPT-RIASTO-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-CPT-RIASTO-IND
              MOVE WB03-CPT-RIASTO
                  TO W-B03-CPT-RIASTO
           END-IF

           IF WB03-PRE-RIASTO-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PRE-RIASTO-IND
              MOVE HIGH-VALUE
              TO W-B03-PRE-RIASTO-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PRE-RIASTO-IND
              MOVE WB03-PRE-RIASTO
                  TO W-B03-PRE-RIASTO
           END-IF

           IF WB03-RIS-RIASTA-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-RIS-RIASTA-IND
              MOVE HIGH-VALUE
              TO W-B03-RIS-RIASTA-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-RIS-RIASTA-IND
              MOVE WB03-RIS-RIASTA
                  TO W-B03-RIS-RIASTA
           END-IF

           IF WB03-CPT-RIASTO-ECC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-CPT-RIASTO-ECC-IND
              MOVE HIGH-VALUE
              TO W-B03-CPT-RIASTO-ECC-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-CPT-RIASTO-ECC-IND
              MOVE WB03-CPT-RIASTO-ECC
                  TO W-B03-CPT-RIASTO-ECC
           END-IF

           IF WB03-PRE-RIASTO-ECC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PRE-RIASTO-ECC-IND
              MOVE HIGH-VALUE
              TO W-B03-PRE-RIASTO-ECC-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PRE-RIASTO-ECC-IND
              MOVE WB03-PRE-RIASTO-ECC
                  TO W-B03-PRE-RIASTO-ECC
           END-IF

           IF WB03-RIS-RIASTA-ECC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-RIS-RIASTA-ECC-IND
              MOVE HIGH-VALUE
              TO W-B03-RIS-RIASTA-ECC-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-RIS-RIASTA-ECC-IND
              MOVE WB03-RIS-RIASTA-ECC
                  TO W-B03-RIS-RIASTA-ECC
           END-IF

           IF WB03-COD-AGE-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-COD-AGE-IND
              MOVE HIGH-VALUE
              TO W-B03-COD-AGE-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-COD-AGE-IND
              MOVE WB03-COD-AGE
                  TO W-B03-COD-AGE
           END-IF

           IF WB03-COD-SUBAGE-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-COD-SUBAGE-IND
              MOVE HIGH-VALUE
              TO W-B03-COD-SUBAGE-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-COD-SUBAGE-IND
              MOVE WB03-COD-SUBAGE
                  TO W-B03-COD-SUBAGE
           END-IF

           IF WB03-COD-CAN-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-COD-CAN-IND
              MOVE HIGH-VALUE
              TO W-B03-COD-CAN-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-COD-CAN-IND
              MOVE WB03-COD-CAN
                  TO W-B03-COD-CAN
           END-IF

           IF WB03-IB-POLI-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-IB-POLI-IND
              MOVE HIGH-VALUE
              TO W-B03-IB-POLI-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-IB-POLI-IND
              MOVE WB03-IB-POLI
                  TO W-B03-IB-POLI
           END-IF

           IF WB03-IB-ADES-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-IB-ADES-IND
              MOVE HIGH-VALUE
              TO W-B03-IB-ADES-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-IB-ADES-IND
              MOVE WB03-IB-ADES
                  TO W-B03-IB-ADES
           END-IF

           IF WB03-IB-TRCH-DI-GAR-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-IB-TRCH-DI-GAR-IND
              MOVE HIGH-VALUE
              TO W-B03-IB-TRCH-DI-GAR-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-IB-TRCH-DI-GAR-IND
              MOVE WB03-IB-TRCH-DI-GAR
                  TO W-B03-IB-TRCH-DI-GAR
           END-IF

           IF WB03-TP-PRSTZ-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TP-PRSTZ-IND
              MOVE HIGH-VALUE
              TO W-B03-TP-PRSTZ-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TP-PRSTZ-IND
              MOVE WB03-TP-PRSTZ
                  TO W-B03-TP-PRSTZ
           END-IF

           IF WB03-TP-TRASF-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TP-TRASF-IND
              MOVE HIGH-VALUE
              TO W-B03-TP-TRASF-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TP-TRASF-IND
              MOVE WB03-TP-PRSTZ
                  TO W-B03-TP-TRASF
           END-IF

           IF WB03-PP-INVRIO-TARI-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PP-INVRIO-TARI-IND
              MOVE HIGH-VALUE
              TO W-B03-PP-INVRIO-TARI-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PP-INVRIO-TARI-IND
              MOVE WB03-PP-INVRIO-TARI
                  TO W-B03-PP-INVRIO-TARI
           END-IF

           IF WB03-COEFF-OPZ-REN-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-COEFF-OPZ-REN-IND
              MOVE HIGH-VALUE
              TO W-B03-COEFF-OPZ-REN-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-COEFF-OPZ-REN-IND
              MOVE WB03-COEFF-OPZ-REN
                  TO W-B03-COEFF-OPZ-REN
           END-IF

           IF WB03-COEFF-OPZ-CPT-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-COEFF-OPZ-CPT-IND
              MOVE HIGH-VALUE
              TO W-B03-COEFF-OPZ-CPT-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-COEFF-OPZ-CPT-IND
              MOVE WB03-COEFF-OPZ-CPT
                  TO W-B03-COEFF-OPZ-CPT
           END-IF

           IF WB03-DUR-PAG-REN-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DUR-PAG-REN-IND
              MOVE HIGH-VALUE
              TO W-B03-DUR-PAG-REN-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-DUR-PAG-REN-IND
              MOVE WB03-DUR-PAG-REN
                  TO W-B03-DUR-PAG-REN
           END-IF

           IF WB03-VLT-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-VLT-IND
              MOVE HIGH-VALUE
              TO W-B03-VLT-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-VLT-IND
              MOVE WB03-VLT
                  TO W-B03-VLT
           END-IF

           IF WB03-RIS-MAT-CHIU-PREC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-RIS-MAT-CHIU-PREC-IND
              MOVE HIGH-VALUE
              TO W-B03-RIS-MAT-CHIU-PREC-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-RIS-MAT-CHIU-PREC-IND
              MOVE WB03-RIS-MAT-CHIU-PREC
                  TO W-B03-RIS-MAT-CHIU-PREC
           END-IF

           IF WB03-COD-FND-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-COD-FND-IND
              MOVE HIGH-VALUE
              TO W-B03-COD-FND-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-COD-FND-IND
              MOVE WB03-COD-FND
                  TO W-B03-COD-FND
           END-IF

           IF WB03-PRSTZ-T-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PRSTZ-T-IND
              MOVE HIGH-VALUE
              TO W-B03-PRSTZ-T-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PRSTZ-T-IND
              MOVE WB03-PRSTZ-T
                  TO W-B03-PRSTZ-T
           END-IF

           IF WB03-TS-TARI-DOV-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TS-TARI-DOV-IND
              MOVE HIGH-VALUE
              TO W-B03-TS-TARI-DOV-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TS-TARI-DOV-IND
              MOVE WB03-TS-TARI-DOV
                  TO W-B03-TS-TARI-DOV
           END-IF

           IF WB03-TS-TARI-SCON-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TS-TARI-SCON-IND
              MOVE HIGH-VALUE
              TO W-B03-TS-TARI-SCON-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TS-TARI-SCON-IND
              MOVE WB03-TS-TARI-SCON
                  TO W-B03-TS-TARI-SCON
           END-IF

           IF WB03-TS-PP-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TS-PP-IND
              MOVE HIGH-VALUE
              TO W-B03-TS-PP-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TS-PP-IND
              MOVE WB03-TS-PP
                  TO W-B03-TS-PP
           END-IF

           IF WB03-COEFF-RIS-1-T-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-COEFF-RIS-1-T-IND
              MOVE HIGH-VALUE
              TO W-B03-COEFF-RIS-1-T-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-COEFF-RIS-1-T-IND
              MOVE WB03-COEFF-RIS-1-T
                  TO W-B03-COEFF-RIS-1-T
           END-IF

           IF WB03-COEFF-RIS-2-T-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-COEFF-RIS-2-T-IND
              MOVE HIGH-VALUE
              TO W-B03-COEFF-RIS-2-T-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-COEFF-RIS-2-T-IND
              MOVE WB03-COEFF-RIS-2-T
                  TO W-B03-COEFF-RIS-2-T
           END-IF

           IF WB03-ABB-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-ABB-IND
              MOVE HIGH-VALUE
              TO W-B03-ABB-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-ABB-IND
              MOVE WB03-ABB
                  TO W-B03-ABB
           END-IF

           IF WB03-TP-COASS-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TP-COASS-IND
              MOVE HIGH-VALUE
              TO W-B03-TP-COASS-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TP-COASS-IND
              MOVE WB03-TP-COASS
                  TO W-B03-TP-COASS
           END-IF

           IF WB03-TRAT-RIASS-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TRAT-RIASS-IND
              MOVE HIGH-VALUE
              TO W-B03-TRAT-RIASS-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TRAT-RIASS-IND
              MOVE WB03-TRAT-RIASS
                  TO W-B03-TRAT-RIASS
           END-IF

           IF WB03-TRAT-RIASS-ECC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TRAT-RIASS-ECC-IND
              MOVE HIGH-VALUE
              TO W-B03-TRAT-RIASS-ECC-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TRAT-RIASS-ECC-IND
              MOVE WB03-TRAT-RIASS-ECC
                  TO W-B03-TRAT-RIASS-ECC
           END-IF

           MOVE 'I'
              TO W-B03-DS-OPER-SQL
           IF WB03-DS-VER NOT NUMERIC
              MOVE 0 TO W-B03-DS-VER
           ELSE
              MOVE WB03-DS-VER
              TO W-B03-DS-VER
           END-IF
           MOVE IDSV0001-DATA-COMP-AGG-STOR
              TO W-B03-DS-TS-CPTZ
           MOVE IDSV0001-USER-NAME
              TO W-B03-DS-UTENTE
           MOVE '1'
              TO W-B03-DS-STATO-ELAB.

           IF WB03-TP-RGM-FISC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TP-RGM-FISC-IND
              MOVE HIGH-VALUE
              TO W-B03-TP-RGM-FISC-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TP-RGM-FISC-IND
              MOVE WB03-TP-RGM-FISC
                  TO W-B03-TP-RGM-FISC
           END-IF

           IF WB03-DUR-GAR-AA-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DUR-GAR-AA-IND
              MOVE HIGH-VALUE
              TO W-B03-DUR-GAR-AA-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-DUR-GAR-AA-IND
              MOVE WB03-DUR-GAR-AA
                  TO W-B03-DUR-GAR-AA
           END-IF

           IF WB03-DUR-GAR-MM-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DUR-GAR-MM-IND
              MOVE HIGH-VALUE
              TO W-B03-DUR-GAR-MM-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-DUR-GAR-MM-IND
              MOVE WB03-DUR-GAR-MM
                  TO W-B03-DUR-GAR-MM
           END-IF

           IF WB03-DUR-GAR-GG-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DUR-GAR-GG-IND
              MOVE HIGH-VALUE
              TO W-B03-DUR-GAR-GG-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-DUR-GAR-GG-IND
              MOVE WB03-DUR-GAR-GG
                  TO W-B03-DUR-GAR-GG
           END-IF

           IF WB03-ANTIDUR-CALC-365-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-ANTIDUR-CALC-365-IND
              MOVE HIGH-VALUE
              TO W-B03-ANTIDUR-CALC-365-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-ANTIDUR-CALC-365-IND
              MOVE WB03-ANTIDUR-CALC-365
                  TO W-B03-ANTIDUR-CALC-365
           END-IF

           IF WB03-ANTIDUR-DT-CALC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-ANTIDUR-DT-CALC-IND
              MOVE HIGH-VALUE
              TO W-B03-ANTIDUR-DT-CALC-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-ANTIDUR-DT-CALC-IND
              MOVE WB03-ANTIDUR-DT-CALC
                  TO W-B03-ANTIDUR-DT-CALC
           END-IF

           IF WB03-COD-FISC-CNTR-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-COD-FISC-CNTR-IND
              MOVE HIGH-VALUE
              TO W-B03-COD-FISC-CNTR-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-COD-FISC-CNTR-IND
              MOVE WB03-COD-FISC-CNTR
                  TO W-B03-COD-FISC-CNTR
           END-IF

           IF WB03-COD-FISC-ASSTO1-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-COD-FISC-ASSTO1-IND
              MOVE HIGH-VALUE
              TO W-B03-COD-FISC-ASSTO1-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-COD-FISC-ASSTO1-IND
              MOVE WB03-COD-FISC-ASSTO1
                  TO W-B03-COD-FISC-ASSTO1
           END-IF

            IF WB03-COD-FISC-ASSTO2-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-COD-FISC-ASSTO2-IND
              MOVE HIGH-VALUE
              TO W-B03-COD-FISC-ASSTO2-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-COD-FISC-ASSTO2-IND
              MOVE WB03-COD-FISC-ASSTO2
                  TO W-B03-COD-FISC-ASSTO2
           END-IF

           IF WB03-COD-FISC-ASSTO3-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-COD-FISC-ASSTO3-IND
              MOVE HIGH-VALUE
              TO W-B03-COD-FISC-ASSTO3-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-COD-FISC-ASSTO3-IND
              MOVE WB03-COD-FISC-ASSTO3
                  TO W-B03-COD-FISC-ASSTO3
           END-IF

GP         IF WB03-CAUS-SCON = HIGH-VALUES
GP            MOVE X'6F'
GP            TO W-B03-CAUS-SCON-IND
GP    *        MOVE HIGH-VALUE
GP    *        TO W-B03-CAUS-SCON
GP    *        MOVE LENGTH OF W-B03-CAUS-SCON
GP    *                 TO W-B03-CAUS-SCON-LEN

GP         ELSE
GP            MOVE X'F0'
GP            TO W-B03-CAUS-SCON-IND
GP            MOVE WB03-CAUS-SCON
GP                TO W-B03-CAUS-SCON
GP            MOVE LENGTH OF W-B03-CAUS-SCON
GP                     TO W-B03-CAUS-SCON-LEN

GP         END-IF

GP          IF WB03-EMIT-TIT-OPZ = HIGH-VALUES
GP            MOVE X'6F'
GP            TO W-B03-EMIT-TIT-OPZ-IND
GP    *        MOVE HIGH-VALUE
GP    *        TO W-B03-EMIT-TIT-OPZ
GP    *        MOVE LENGTH OF W-B03-EMIT-TIT-OPZ
GP    *                 TO W-B03-EMIT-TIT-OPZ-LEN
GP         ELSE
GP            MOVE X'F0'
GP            TO W-B03-EMIT-TIT-OPZ-IND
GP            MOVE WB03-EMIT-TIT-OPZ
GP                TO W-B03-EMIT-TIT-OPZ
GP            MOVE LENGTH OF W-B03-EMIT-TIT-OPZ
GP                     TO W-B03-EMIT-TIT-OPZ-LEN

GP         END-IF

           IF WB03-QTZ-SP-Z-COUP-EMIS-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-QTZ-SP-Z-COUP-EMIS-IND
              MOVE HIGH-VALUE
              TO W-B03-QTZ-SP-Z-COUP-EMIS-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-QTZ-SP-Z-COUP-EMIS-IND
              MOVE WB03-QTZ-SP-Z-COUP-EMIS
              TO W-B03-QTZ-SP-Z-COUP-EMIS
           END-IF

           IF WB03-QTZ-SP-Z-OPZ-EMIS-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-QTZ-SP-Z-OPZ-EMIS-IND
              MOVE HIGH-VALUE
              TO W-B03-QTZ-SP-Z-OPZ-EMIS-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-QTZ-SP-Z-OPZ-EMIS-IND
              MOVE WB03-QTZ-SP-Z-OPZ-EMIS
              TO W-B03-QTZ-SP-Z-OPZ-EMIS
           END-IF

           IF WB03-QTZ-SP-Z-COUP-DT-C-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-QTZ-SP-Z-COUP-DT-C-IND
              MOVE HIGH-VALUE
              TO W-B03-QTZ-SP-Z-COUP-DT-C-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-QTZ-SP-Z-COUP-DT-C-IND
              MOVE WB03-QTZ-SP-Z-COUP-DT-C
              TO W-B03-QTZ-SP-Z-COUP-DT-C
           END-IF

           IF WB03-QTZ-SP-Z-OPZ-DT-CA-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-QTZ-SP-Z-OPZ-DT-CA-IND
              MOVE HIGH-VALUE
              TO W-B03-QTZ-SP-Z-OPZ-DT-CA-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-QTZ-SP-Z-OPZ-DT-CA-IND
              MOVE WB03-QTZ-SP-Z-OPZ-DT-CA
              TO W-B03-QTZ-SP-Z-OPZ-DT-CA
           END-IF

           IF WB03-QTZ-TOT-EMIS-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-QTZ-TOT-EMIS-IND
              MOVE HIGH-VALUE
              TO W-B03-QTZ-TOT-EMIS-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-QTZ-TOT-EMIS-IND
              MOVE WB03-QTZ-TOT-EMIS
              TO W-B03-QTZ-TOT-EMIS
           END-IF

           IF WB03-QTZ-TOT-DT-CALC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-QTZ-TOT-DT-CALC-IND
              MOVE HIGH-VALUE
              TO W-B03-QTZ-TOT-DT-CALC-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-QTZ-TOT-DT-CALC-IND
              MOVE WB03-QTZ-TOT-DT-CALC
              TO W-B03-QTZ-TOT-DT-CALC
           END-IF

           IF WB03-QTZ-TOT-DT-ULT-BIL-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-QTZ-TOT-DT-ULT-BIL-IND
              MOVE HIGH-VALUE
              TO W-B03-QTZ-TOT-DT-ULT-BIL-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-QTZ-TOT-DT-ULT-BIL-IND
              MOVE WB03-QTZ-TOT-DT-ULT-BIL
              TO W-B03-QTZ-TOT-DT-ULT-BIL
           END-IF

           IF WB03-DT-QTZ-EMIS-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-QTZ-EMIS-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-QTZ-EMIS-NULL
           ELSE
             IF WB03-DT-QTZ-EMIS = ZERO
                MOVE X'6F'
                TO W-B03-DT-QTZ-EMIS-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-QTZ-EMIS-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-QTZ-EMIS-IND
              MOVE WB03-DT-QTZ-EMIS TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-QTZ-EMIS
             END-IF
           END-IF

           IF WB03-PC-CAR-ACQ-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PC-CAR-ACQ-IND
              MOVE HIGH-VALUE
              TO W-B03-PC-CAR-ACQ-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PC-CAR-ACQ-IND
              MOVE WB03-PC-CAR-ACQ
              TO W-B03-PC-CAR-ACQ
           END-IF

           IF WB03-IMP-CAR-CASO-MOR-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-IMP-CAR-CASO-MOR-IND
              MOVE HIGH-VALUE
              TO W-B03-IMP-CAR-CASO-MOR-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-IMP-CAR-CASO-MOR-IND
              MOVE WB03-IMP-CAR-CASO-MOR
              TO W-B03-IMP-CAR-CASO-MOR
           END-IF

           IF WB03-PC-CAR-MOR-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PC-CAR-MOR-IND
              MOVE HIGH-VALUE
              TO W-B03-PC-CAR-MOR-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PC-CAR-MOR-IND
              MOVE WB03-PC-CAR-MOR
              TO W-B03-PC-CAR-MOR
           END-IF

           IF WB03-TP-VERS-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TP-VERS-IND
              MOVE HIGH-VALUE
              TO W-B03-TP-VERS-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TP-VERS-IND
              MOVE WB03-TP-VERS
              TO W-B03-TP-VERS
           END-IF

           IF WB03-FL-SWITCH-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-FL-SWITCH-IND
              MOVE HIGH-VALUE
              TO W-B03-FL-SWITCH-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-FL-SWITCH-IND
              MOVE WB03-FL-SWITCH
              TO W-B03-FL-SWITCH
           END-IF

           IF WB03-FL-IAS-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-FL-IAS-IND
              MOVE HIGH-VALUE
              TO W-B03-FL-IAS-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-FL-IAS-IND
              MOVE WB03-FL-IAS
              TO W-B03-FL-IAS
           END-IF

           IF WB03-DIR-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DIR-IND
              MOVE HIGH-VALUE
              TO W-B03-DIR-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-DIR-IND
              MOVE WB03-DIR
              TO W-B03-DIR
           END-IF

           IF WB03-TP-COP-CASO-MOR-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TP-COP-CASO-MOR-IND
              MOVE HIGH-VALUE
              TO W-B03-TP-COP-CASO-MOR-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TP-COP-CASO-MOR-IND
              MOVE WB03-TP-COP-CASO-MOR
              TO W-B03-TP-COP-CASO-MOR
           END-IF

           IF WB03-MET-RISC-SPCL-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-MET-RISC-SPCL-IND
              MOVE HIGH-VALUE
              TO W-B03-MET-RISC-SPCL-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-MET-RISC-SPCL-IND
              MOVE WB03-MET-RISC-SPCL
              TO W-B03-MET-RISC-SPCL
           END-IF

           IF WB03-TP-STAT-INVST-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TP-STAT-INVST-IND
              MOVE HIGH-VALUE
              TO W-B03-TP-STAT-INVST-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TP-STAT-INVST-IND
              MOVE WB03-TP-STAT-INVST
              TO W-B03-TP-STAT-INVST
           END-IF

           IF WB03-COD-PRDT-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-COD-PRDT-IND
              MOVE HIGH-VALUE
              TO W-B03-COD-PRDT-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-COD-PRDT-IND
              MOVE WB03-COD-PRDT
              TO W-B03-COD-PRDT
           END-IF

           IF WB03-STAT-ASSTO-1-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-STAT-ASSTO-1-IND
              MOVE HIGH-VALUE
              TO W-B03-STAT-ASSTO-1-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-STAT-ASSTO-1-IND
              MOVE WB03-STAT-ASSTO-1
              TO W-B03-STAT-ASSTO-1
           END-IF

           IF WB03-STAT-ASSTO-2-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-STAT-ASSTO-2-IND
              MOVE HIGH-VALUE
              TO W-B03-STAT-ASSTO-2-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-STAT-ASSTO-2-IND
              MOVE WB03-STAT-ASSTO-2
              TO W-B03-STAT-ASSTO-2
           END-IF

           IF WB03-STAT-ASSTO-3-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-STAT-ASSTO-3-IND
              MOVE HIGH-VALUE
              TO W-B03-STAT-ASSTO-3-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-STAT-ASSTO-3-IND
              MOVE WB03-STAT-ASSTO-3
              TO W-B03-STAT-ASSTO-3
           END-IF


           IF WB03-CPT-ASSTO-INI-MOR-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-CPT-ASSTO-INI-MOR-IND
              MOVE HIGH-VALUE
              TO W-B03-CPT-ASSTO-INI-MOR-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-CPT-ASSTO-INI-MOR-IND
              MOVE WB03-CPT-ASSTO-INI-MOR
              TO W-B03-CPT-ASSTO-INI-MOR
           END-IF

           IF WB03-TS-STAB-PRE-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-TS-STAB-PRE-IND
              MOVE HIGH-VALUE
              TO W-B03-TS-STAB-PRE-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-TS-STAB-PRE-IND
              MOVE WB03-TS-STAB-PRE
              TO W-B03-TS-STAB-PRE
           END-IF

           IF WB03-DIR-EMIS-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DIR-EMIS-IND
              MOVE HIGH-VALUE
              TO W-B03-DIR-EMIS-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-DIR-EMIS-IND
              MOVE WB03-DIR-EMIS
              TO W-B03-DIR-EMIS
           END-IF

           IF WB03-DT-INC-ULT-PRE-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-DT-INC-ULT-PRE-IND
              MOVE HIGH-VALUE
              TO W-B03-DT-INC-ULT-PRE-NULL
           ELSE
             IF WB03-DT-INC-ULT-PRE = ZERO
                MOVE X'6F'
                TO W-B03-DT-INC-ULT-PRE-IND
                MOVE HIGH-VALUE
                TO W-B03-DT-INC-ULT-PRE-NULL
             ELSE
              MOVE X'F0'
              TO W-B03-DT-INC-ULT-PRE-IND
              MOVE WB03-DT-INC-ULT-PRE TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B03-DT-INC-ULT-PRE
             END-IF
           END-IF

           IF WB03-STAT-TBGC-ASSTO-1-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-STAT-TBGC-ASSTO-1-IND
              MOVE HIGH-VALUE
              TO W-B03-STAT-TBGC-ASSTO-1-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-STAT-TBGC-ASSTO-1-IND
              MOVE WB03-STAT-TBGC-ASSTO-1
              TO W-B03-STAT-TBGC-ASSTO-1
           END-IF

           IF WB03-STAT-TBGC-ASSTO-2-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-STAT-TBGC-ASSTO-2-IND
              MOVE HIGH-VALUE
              TO W-B03-STAT-TBGC-ASSTO-2-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-STAT-TBGC-ASSTO-2-IND
              MOVE WB03-STAT-TBGC-ASSTO-2
              TO W-B03-STAT-TBGC-ASSTO-2
           END-IF

           IF WB03-STAT-TBGC-ASSTO-3-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-STAT-TBGC-ASSTO-3-IND
              MOVE HIGH-VALUE
              TO W-B03-STAT-TBGC-ASSTO-3-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-STAT-TBGC-ASSTO-3-IND
              MOVE WB03-STAT-TBGC-ASSTO-3
              TO W-B03-STAT-TBGC-ASSTO-3
           END-IF

           IF WB03-FRAZ-DECR-CPT-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-FRAZ-DECR-CPT-IND
              MOVE HIGH-VALUE
              TO W-B03-FRAZ-DECR-CPT-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-FRAZ-DECR-CPT-IND
              MOVE WB03-FRAZ-DECR-CPT
              TO W-B03-FRAZ-DECR-CPT
           END-IF

           IF WB03-PRE-PP-ULT-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B03-PRE-PP-ULT-IND
              MOVE HIGH-VALUE
              TO W-B03-PRE-PP-ULT-NULL
           ELSE
              MOVE X'F0'
              TO W-B03-PRE-PP-ULT-IND
              MOVE WB03-PRE-PP-ULT
              TO W-B03-PRE-PP-ULT
           END-IF.

ITRFO      IF WB03-ACQ-EXP-NULL = HIGH-VALUES
ITRFO         MOVE X'6F'
ITRFO         TO W-B03-ACQ-EXP-IND
ITRFO         MOVE HIGH-VALUE
ITRFO         TO W-B03-ACQ-EXP-NULL
ITRFO      ELSE
ITRFO         MOVE X'F0'
ITRFO         TO W-B03-ACQ-EXP-IND
ITRFO         MOVE WB03-ACQ-EXP
ITRFO         TO W-B03-ACQ-EXP
ITRFO      END-IF.

ITRFO      IF WB03-REMUN-ASS-NULL = HIGH-VALUES
ITRFO         MOVE X'6F'
ITRFO         TO W-B03-REMUN-ASS-IND
ITRFO         MOVE HIGH-VALUE
ITRFO         TO W-B03-REMUN-ASS-NULL
ITRFO      ELSE
ITRFO         MOVE X'F0'
ITRFO         TO W-B03-REMUN-ASS-IND
ITRFO         MOVE WB03-REMUN-ASS
ITRFO         TO W-B03-REMUN-ASS
ITRFO      END-IF.

ITRFO      IF WB03-COMMIS-INTER-NULL = HIGH-VALUES
ITRFO         MOVE X'6F'
ITRFO         TO W-B03-COMMIS-INTER-IND
ITRFO         MOVE HIGH-VALUE
ITRFO         TO W-B03-COMMIS-INTER-NULL
ITRFO      ELSE
ITRFO         MOVE X'F0'
ITRFO         TO W-B03-COMMIS-INTER-IND
ITRFO         MOVE WB03-COMMIS-INTER
ITRFO         TO W-B03-COMMIS-INTER
ITRFO      END-IF.

ITRFO      IF WB03-NUM-FINANZ-NULL = HIGH-VALUES
ITRFO         MOVE X'6F'
ITRFO         TO W-B03-NUM-FINANZ-IND
ITRFO         MOVE HIGH-VALUE
ITRFO         TO W-B03-NUM-FINANZ-NULL
ITRFO      ELSE
ITRFO         MOVE X'F0'
ITRFO         TO W-B03-NUM-FINANZ-IND
ITRFO         MOVE WB03-NUM-FINANZ
ITRFO         TO W-B03-NUM-FINANZ
ITRFO      END-IF.

ITRFO      IF WB03-TP-ACC-COMM-NULL = HIGH-VALUES
ITRFO         MOVE X'6F'
ITRFO         TO W-B03-TP-ACC-COMM-IND
ITRFO         MOVE HIGH-VALUE
ITRFO         TO W-B03-TP-ACC-COMM-NULL
ITRFO      ELSE
ITRFO         MOVE X'F0'
ITRFO         TO W-B03-TP-ACC-COMM-IND
ITRFO         MOVE WB03-TP-ACC-COMM
ITRFO         TO W-B03-TP-ACC-COMM
ITRFO      END-IF.

ITRFO      IF WB03-IB-ACC-COMM-NULL = HIGH-VALUES
ITRFO         MOVE X'6F'
ITRFO         TO W-B03-IB-ACC-COMM-IND
ITRFO         MOVE HIGH-VALUE
ITRFO         TO W-B03-IB-ACC-COMM-NULL
ITRFO      ELSE
ITRFO         MOVE X'F0'
ITRFO         TO W-B03-IB-ACC-COMM-IND
ITRFO         MOVE WB03-IB-ACC-COMM
ITRFO         TO W-B03-IB-ACC-COMM
ITRFO      END-IF.

ITRFO      MOVE WB03-RAMO-BILA
ITRFO        TO W-B03-RAMO-BILA.

           IF WB03-CARZ-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
              TO W-B03-CARZ-NULL
              MOVE X'6F'
              TO W-B03-CARZ-IND
           ELSE
              MOVE WB03-CARZ
              TO W-B03-CARZ
              MOVE X'F0'
              TO W-B03-CARZ-IND
           END-IF.

       EX-S1290.
           EXIT.


      * ----------------------------------------------------------------
      * Reperimento della riserva matematica al 31/12 dell'anno
      * precedente
      * ----------------------------------------------------------------
       S1201-TRATTA-RST.

           MOVE 'S1201-TRATTA-RST'       TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO      TO TRUE
           MOVE WK-LABEL-ERR             TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY        THRU ESEGUI-DISPLAY-EX.

           SET TAB-RIS-OK                TO TRUE
           SET IDSI0011-TRATT-DEFAULT    TO TRUE
      *--> LIVELLO OPERAZIONE
           SET IDSI0011-ID-PADRE          TO TRUE
           SET IDSI0011-SELECT           TO TRUE
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE

           INITIALIZE                       RIS-DI-TRCH.
           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                                         TO RST-ID-TRCH-DI-GAR

           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA
           MOVE 'RIS-DI-TRCH'            TO IDSI0011-CODICE-STR-DATO
           MOVE RIS-DI-TRCH              TO IDSI0011-BUFFER-DATI


           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IDBSRST0'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura RIS_DI_TRCH'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IDBSRST0'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura RIS_DI_TRCH'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *-->        CHIAVE NON TROVATA
                       SET TAB-RIS-KO TO TRUE

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI
                         TO RIS-DI-TRCH

                  WHEN OTHER
      *-->        ERRORE DI ACCESSO AL DB
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1201-TRATTA-RST'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'IDBSRST0 - RIS-DI-TRCH;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE INTO
                              IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1201-TRATTA-RST'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'IDBSRST0 - RIS-DI-TRCH;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO
                     IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1201.
           EXIT.
      *-----------------------------------------------------------------
      *  CALCOLO DIFFERENZA DATE
      *-----------------------------------------------------------------
       S2900-CALCOLO-DIFF-DATE.

           MOVE 'S2900-CALCOLO-DIFF-DATE'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LCCS0010'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-INIZIO            TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'CALCOLO-DIFF-DATE    '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           CALL LCCS0010 USING FORMATO,
                               DATA-INFERIORE,
                               DATA-SUPERIORE,
                               GG-DIFF,
                               CODICE-RITORNO
           ON EXCEPTION
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'T0048-RECESSO-MAX-TRENTA'
                    TO CALL-DESC
                  MOVE 'T0048-RECESSO-MAX-TRENTA'
                    TO IEAI9901-LABEL-ERR
                  PERFORM S0290-ERRORE-DI-SISTEMA
                     THRU EX-S0290
           END-CALL.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LCCS0010'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-FINE              TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'CALCOLO-DIFF-DATE    '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S2900.
           EXIT.
      *-----------------------------------------------------------------
      *  CALCOLO DIFFERENZA DATE 365
      *-----------------------------------------------------------------
       S2950-CALCOLO-DIFF-DATE-365.
           MOVE 'S2950-CALCOLO-DIFF-DATE-365'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LCCS0017'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-INIZIO            TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'CALCOLO-DIFF-DATE-365'
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           CALL LCCS0017 USING FORMATO,
                               DATA-INFERIORE,
                               DATA-SUPERIORE,
                               GG-DIFF,
                               CODICE-RITORNO
           ON EXCEPTION
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'CALL LCCS0017 - ERRORE CHIAMATA'
                    TO CALL-DESC
                  MOVE 'S2950-CALCOLO-DIFF-DATE-365'
                    TO IEAI9901-LABEL-ERR
                  PERFORM S0290-ERRORE-DI-SISTEMA
                     THRU EX-S0290
           END-CALL.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LCCS0017'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-INIZIO            TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'CALCOLO-DIFF-DATE-365'
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S2950.
           EXIT.
      *----------------------------------------------------------------*
      *  CHIAMATA AL SERVIZIO    CALCOLA DATA
      *----------------------------------------------------------------*
       A9200-CALL-AREA-CALCOLA-DATA.

           MOVE 'A9200-CALL-AREA-CALCOLA-DATA'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LCCS0003'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-INIZIO            TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'CALL-AREA-CALCOLA-DAT'
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           CALL LCCS0003 USING IO-A2K-LCCC0003 IN-RCODE
           ON EXCEPTION
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'ADD GIORNI AD UNA DATA'
                TO CALL-DESC
              MOVE 'A9200-CALL-AREA-CALCOLA-DATA'
                TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LCCS0003'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-FINE              TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'CALL-AREA-CALCOLA-DAT'
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF IN-RCODE NOT = '00'
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'A9200-CALL-AREA-CALCOLA-DATA'
                TO IEAI9901-LABEL-ERR
              MOVE '005166'
                TO IEAI9901-COD-ERRORE
              STRING 'LCCS0003;'
                      IN-RCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-A9200.
           EXIT.
      * ----------------------------------------------------------------
      * Chiamata al modulo di scrittura della tabella ESTRATTI
----->* STRUTTURA BASE DATI PROVVISORIA
      * ----------------------------------------------------------------
       S1260-CALL-EOC-B03.

           MOVE 'S1260-CALL-EOC-B03'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LLBS0240'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Scritt. BILA_TRCH_ESTR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           CALL LLBS0240    USING AREA-IDSV0001
                                  AREA-OUT-B03
           ON EXCEPTION
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'PROGRAMMA DI AGGIORNAMENTO TABELLE'
                TO CALL-DESC
              MOVE 'S1260-CALL-EOC-B03'
                TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LLBS0240'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Scritt. BILA_TRCH_ESTR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S1260.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPAREA AREA "VARIABILI DI CALCOLO TRANCHE"
      *----------------------------------------------------------------*
       S1300-VALORIZZA-OUT-B05.

           MOVE 'S1300-VALORIZZA-OUT-B05'   TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE.
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY           THRU ESEGUI-DISPLAY-EX.

      *--> VALORIZZAZIONE OCCURS AREA INPUT SERVIZIO CALCOLI E CONTROLLI
      *--> CON L'OUTPUT DEL VALORIZZATORE VARIABILI
45630 *    PERFORM S1350-AREA-SCHEDA
45630 *       THRU EX-S1350
45630 *    VARYING IX-AREA-SCHEDA FROM 1 BY 1
45630 *      UNTIL IX-AREA-SCHEDA > WSKD-ELE-LIVELLO-MAX.
45630 *
           IF WGRZ-RAMO-BILA(IX-TAB-GRZ) = '3'
              SET SI-GAR-RAMO-III TO TRUE
           ELSE
              SET NO-GAR-RAMO-III TO TRUE
           END-IF.

           IF  IDSV0001-ESITO-OK
           AND SI-GAR-RAMO-III
      *-->     SCRITTURA VARIABILI A LISTA
      *-->    (OPERAZIONE DISABILITATA - QUESTE INFORMAZIONI ADESSO
      *-->     SARANNO SCRITTE SULLA TABELLA BILA_FND_ESTR)
      *        PERFORM WRITE-VAR-LISTA
      *           THRU EX-WRITE-VAR-LISTA
               PERFORM S3000-SCRIVI-BILA-FND-ESTR
                  THRU S3000-EX
           END-IF.

      *--> VALORIZZAZIONE OCCURS AREA INPUT SERVIZIO CALCOLI E CONTROLLI
      *--> CON L'OUTPUT DEL VALORIZZATORE VARIABILI
45630      IF  WS-FL-SCRIVI-TRCH-SI
45630      AND IDSV0001-ESITO-OK
45630         PERFORM S1350-AREA-SCHEDA
45630            THRU EX-S1350
45630         VARYING IX-AREA-SCHEDA FROM 1 BY 1
45630           UNTIL IX-AREA-SCHEDA > WSKD-ELE-LIVELLO-MAX-T
45630      END-IF.

       EX-S1300.
           EXIT.
      *----------------------------------------------------------------*
      * CICLO DI VALORIZZAZIONE DCLGEN TABELLA "VARIABILI DI CALCOLO"
      * CON L'OUTPUT DEL VALORIZZATORE VARIABILI
      *----------------------------------------------------------------*
       S1350-AREA-SCHEDA.

           MOVE 'S1350-AREA-SCHEDA'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.
      *
      * --> VALORIZZAZIONE DCLGEN BIL_VAR_DI_CALC A PARITA' DI TRANCHE
      *
           IF  WSKD-ID-LIVELLO-T(IX-AREA-SCHEDA) EQUAL
               WTGA-ID-TRCH-DI-GAR (IX-TAB-TGA)
           AND WSKD-TP-LIVELLO-T(IX-AREA-SCHEDA) = 'G'
      * AGGIUNGO ALLE VARIABILI DA STRINGARE LE VARIABILI ITN E
      * TIPOTRANCHE ESPORTATE IN CHIARO DAL VALORIZZATORE VARIABILI

              ADD 1
               TO WSKD-ELE-VARIABILI-MAX-T(IX-AREA-SCHEDA)
              MOVE WSKD-ELE-VARIABILI-MAX-T(IX-AREA-SCHEDA)
                TO IX-MAX-VAR
              MOVE 'ITN'
                TO WSKD-COD-VARIABILE-T
                   (IX-AREA-SCHEDA, IX-MAX-VAR)
              MOVE 'I'
                TO WSKD-TP-DATO-T
                   (IX-AREA-SCHEDA, IX-MAX-VAR)
              MOVE WSKD-FLG-ITN(IX-AREA-SCHEDA)
                TO WSKD-VAL-IMP-T
                   (IX-AREA-SCHEDA, IX-MAX-VAR)

              ADD 1
               TO WSKD-ELE-VARIABILI-MAX-T(IX-AREA-SCHEDA)
              MOVE WSKD-ELE-VARIABILI-MAX-T(IX-AREA-SCHEDA)
                TO IX-MAX-VAR
              MOVE 'TIPOTRANCHE'
                TO WSKD-COD-VARIABILE-T
                   (IX-AREA-SCHEDA, IX-MAX-VAR)
              MOVE 'N'
                TO WSKD-TP-DATO-T
                   (IX-AREA-SCHEDA, IX-MAX-VAR)
              MOVE WSKD-TIPO-TRCH(IX-AREA-SCHEDA)
                TO WSKD-VAL-IMP-T
                   (IX-AREA-SCHEDA, IX-MAX-VAR)
      *
              IF WK-STRINGATURA-SI
                 PERFORM S1400-AREA-VARIABILI
                    THRU EX-S1400
                 VARYING IX-TAB-VAR FROM 1 BY 1
                   UNTIL IX-TAB-VAR > 1
                      OR IDSV0001-ESITO-KO
              ELSE
                 PERFORM S1400-AREA-VARIABILI
                    THRU EX-S1400
                 VARYING IX-TAB-VAR FROM 1 BY 1
                   UNTIL IX-TAB-VAR >
                         WSKD-ELE-VARIABILI-MAX-T(IX-AREA-SCHEDA)
                      OR IDSV0001-ESITO-KO
              END-IF
           END-IF.
      *
       EX-S1350.
           EXIT.
      *----------------------------------------------------------------*
      * CICLO DI VALORIZZAZIONE DELL'AREA VARIABILI DELLA TABELLA
      * "VARIABILI DI CALCOLO"
      *----------------------------------------------------------------*
       S1400-AREA-VARIABILI.

           MOVE 'S1400-AREA-VARIABILI'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           PERFORM INIZIA-TOT-B05
              THRU INIZIA-TOT-B05-EX.

           SET  WB05-ST-ADD                 TO TRUE
           MOVE 1                           TO WB05-ELE-B05-MAX

           MOVE WPOL-COD-COMP-ANIA
             TO WB05-COD-COMP-ANIA

      *     MOVE WB03-ID-BILA-TRCH-ESTR
      *       TO WB05-ID-BILA-TRCH-ESTR

           IF WLB-TP-RICH = 'B1'
              MOVE WLB-ID-RICH           TO WB05-ID-RICH-ESTRAZ-MAS
              MOVE HIGH-VALUES           TO WB05-ID-RICH-ESTRAZ-AGG-NULL
           END-IF

           IF WLB-TP-RICH = 'B2'
              MOVE WLB-ID-RICH-COLL      TO WB05-ID-RICH-ESTRAZ-MAS
              MOVE WLB-ID-RICH           TO WB05-ID-RICH-ESTRAZ-AGG
           END-IF

           MOVE WLB-DT-RISERVA
             TO WB05-DT-RIS

           MOVE WPOL-ID-POLI
             TO WB05-ID-POLI
           MOVE WADE-ID-ADES(IX-TAB-ADE)
             TO WB05-ID-ADES
           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
             TO WB05-ID-TRCH-DI-GAR
           MOVE 1
             TO WB05-PROG-SCHEDA-VALOR

           MOVE  WSKD-DT-INIZ-TARI-T(IX-AREA-SCHEDA)
             TO WB05-DT-INI-VLDT-TARI
           MOVE WSKD-COD-RGM-FISC-T(IX-AREA-SCHEDA)
             TO WB05-TP-RGM-FISC
           MOVE WSKD-DT-INIZ-PROD-P(1)
             TO WB05-DT-INI-VLDT-PROD
           MOVE  WSKD-DT-DECOR-TRCH-T(IX-AREA-SCHEDA)
             TO WB05-DT-DECOR-TRCH

      * AREA VARIABILI
           IF WK-STRINGATURA-SI
              MOVE WSKD-AREA-VARIABILI-T(IX-AREA-SCHEDA)
                TO IDSV0503-INPUT

              SET  IDSV8888-BUSINESS-DBG      TO TRUE
              MOVE 'STRINGA1'                 TO IDSV8888-NOME-PGM
              SET  IDSV8888-INIZIO            TO TRUE
              MOVE SPACES                     TO IDSV8888-DESC-PGM
              STRING 'String. BILA_VAR_CALC_T'
                      DELIMITED BY SIZE
                      INTO IDSV8888-DESC-PGM
              END-STRING
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

              PERFORM S503-CALL-STRINGATURA
                 THRU S503-CALL-STRINGATURA-EX

              SET  IDSV8888-BUSINESS-DBG      TO TRUE
      *       MOVE 'STRINGA1'                 TO IDSV8888-NOME-PGM
              SET  IDSV8888-FINE              TO TRUE
              MOVE SPACES                     TO IDSV8888-DESC-PGM
              MOVE IDSV0503-ELE-VARIABILI-MAX TO WK-NUM-VAR
              MOVE IDSV0503-LEN-STRINGA-TOT   TO WK-LUNGH-STRINGA
              STRING 'STRINGA1 '
                     'NumVar ' WK-NUM-VAR  ' LunStr ' WK-LUNGH-STRINGA
                      DELIMITED BY SIZE
                      INTO IDSV8888-NOME-PGM
              END-STRING
              STRING 'String. BILA_VAR_CALC_T'
                      DELIMITED BY SIZE
                      INTO IDSV8888-DESC-PGM
              END-STRING
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

              MOVE IDSV0503-LEN-STRINGA-TOT TO WB05-AREA-D-VALOR-VAR-LEN
              MOVE IDSV0503-STRINGA-TOT     TO WB05-AREA-D-VALOR-VAR
           ELSE
              MOVE WSKD-COD-VARIABILE-T(IX-AREA-SCHEDA, IX-TAB-VAR)
                TO WB05-COD-VAR
              MOVE WSKD-TP-DATO-T(IX-AREA-SCHEDA, IX-TAB-VAR)
                TO WB05-TP-D

              EVALUATE WSKD-TP-DATO-T(IX-AREA-SCHEDA, IX-TAB-VAR)
                  WHEN 'D'
                  WHEN 'S'
                  WHEN 'X'
                     MOVE WSKD-VAL-STR-T(IX-AREA-SCHEDA, IX-TAB-VAR)
                TO WB05-VAL-STRINGA

                  WHEN 'A'
                  WHEN 'N'
                  WHEN 'I'
                     MOVE WSKD-VAL-IMP-T(IX-AREA-SCHEDA, IX-TAB-VAR)
                TO WB05-VAL-IMP

                  WHEN 'P'
                  WHEN 'M'
                     MOVE WSKD-VAL-PERC-T(IX-AREA-SCHEDA, IX-TAB-VAR)
                TO WB05-VAL-PC

              END-EVALUATE
           END-IF.

           IF WK-STRADA-TAB
              PERFORM S1450-CALL-EOC-B05
                 THRU EX-S1450
           ELSE
              PERFORM S1460-CALL-EOC-B05
                 THRU EX-S1460
           END-IF.
      *
       EX-S1400.
           EXIT.
      * ----------------------------------------------------------------
      * Chiamata al modulo di scrittura della tabella VAR_DI_CALC_T
      * ----------------------------------------------------------------
       S1450-CALL-EOC-B05.

           MOVE 'S1450-CALL-EOC-B05'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LLBS0250'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Scrittura BILA_VAR_CALC_T'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           CALL LLBS0250    USING AREA-IDSV0001
                                  AREA-OUT-B05
           ON EXCEPTION
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'PROGRAMMA DI AGGIORNAMENTO TABELLE'
                TO CALL-DESC
              MOVE 'S1450-CALL-EOC-B05'
                TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LLBS0250'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Scrittura BILA_VAR_CALC_T'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S1450.
           EXIT.
      * ----------------------------------------------------------------
      * Chiamata al modulo di scrittura della tabella VAR_DI_CALC_T
      * ----------------------------------------------------------------
       S1460-CALL-EOC-B05.

           MOVE 'S1460-CALL-EOC-B05'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE 'S1450-CALL-EOC-B05'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'FILESQS1'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Scritt. BILA_VAR_CALC_T'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

      *     MOVE 'BILA-VAR-CALC-T' TO WK-TABELLA.

      *     PERFORM ESTR-SEQUENCE  THRU ESTR-SEQUENCE-EX.

           IF IDSV0001-ESITO-OK
              PERFORM S1480-WRITE-FILEOUT  THRU
                      EX-S1480
           END-IF.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'FILESQS1'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Scritt. BILA_VAR_CALC_T'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.


       EX-S1460.
           EXIT.
      *-----------------------------------------------------------------
      *   SCRITTURA TABELLA BILA-FND-ESTR
      *-----------------------------------------------------------------
       S3000-SCRIVI-BILA-FND-ESTR.

           MOVE 'S3000-SCRIVI-BILA-FND-ESTR'    TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE 'BILA-FND-ESTR'                 TO WK-TABELLA

           IF SI-GAR-RAMO-III
      *-->    PREPARA AREA TRANCHE DI GARANZIA X LCCS0450
              PERFORM PREPARA-ATGA-FND
                 THRU PREPARA-ATGA-FND-EX

      *-->    ESTRAI QUOTAZIONE FONDI X TRANCHE
              PERFORM LEGGI-QUOTAZ-FONDI
                 THRU LEGGI-QUOTAZ-FONDI-EX
           END-IF.

           MOVE WGRZ-TP-GAR(IX-TAB-GRZ)         TO ACT-TP-GARANZIA

45630      SET   WS-FL-TUTTI-FND-A-0-SI   TO TRUE
           PERFORM VARYING IX-TAB-450 FROM 1 BY 1
             UNTIL IX-TAB-450 > LCCC0450-ELE-TAB-MAX
                OR IDSV0001-ESITO-KO

             IF  LCCC0450-NUM-QUOTE     (IX-TAB-450) NOT = 0
             AND LCCC0450-ID-TRCH-DI-GAR(IX-TAB-450) =
                 WTGA-ID-TRCH-DI-GAR    (IX-TAB-TGA)
             AND NOT TP-GAR-ACCESSORIA

                  IF  LCCC0450-NUM-QUOTE(IX-TAB-450) < 0
                      COMPUTE LCCC0450-NUM-QUOTE(IX-TAB-450) =
                      LCCC0450-NUM-QUOTE(IX-TAB-450) * (-1)
                  END-IF

      *           PERFORM ESTR-SEQUENCE
      *              THRU ESTR-SEQUENCE-EX

                 IF IDSV0001-ESITO-OK
                    PERFORM S3050-VAL-BILA-FND-ESTR
                       THRU S3050-EX
                    IF WK-STRADA-TAB
                       PERFORM AGGIORNA-BILA-FND-ESTR
                          THRU AGGIORNA-BILA-FND-ESTR-EX
                    ELSE
                       PERFORM CALL-EOC-B01-SEQ
                          THRU CALL-EOC-B01-SEQ-EX
                    END-IF
                 END-IF
45630            IF IDSV0001-ESITO-OK
45630               SET   WS-FL-TUTTI-FND-A-0-NO   TO TRUE
45630            END-IF
45630        END-IF
           END-PERFORM.

45630      IF  IDSV0001-ESITO-OK
45630      AND SI-GAR-RAMO-III
45630      AND NOT TP-GAR-ACCESSORIA
45630      AND WS-FL-TUTTI-FND-A-0-SI
45630          SET WS-FL-SCRIVI-TRCH-NO    TO TRUE
45630      END-IF.

       S3000-EX.
           EXIT.

      * ----------------------------------------------------------------
      * Chiamata al modulo di scrittura squenziale BILA_FND_ESTR
      * ----------------------------------------------------------------
       CALL-EOC-B01-SEQ.

           MOVE 'CALL-EOC-B01-SEQ'    TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.


           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'FILESQS3'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Scritt. BILA_FND_ESTR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

      **--> IL SEQUENCE VIENE GIA ESTRATTO ESTERNAMENTE A QUESTA PERFORM
      **   MOVE 'BILA_TRCH_ESTR' TO WK-TABELLA.
      **   PERFORM ESTR-SEQUENCE  THRU ESTR-SEQUENCE-EX.
      **
           IF IDSV0001-ESITO-OK
              PERFORM WRITE-FILEOUT-B01  THRU
                      WRITE-FILEOUT-B01-EX
           END-IF.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'FILESQS3'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Scritt. BILA_FND_ESTR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.


       CALL-EOC-B01-SEQ-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    SCRITTURA FILE SEQUENZIALE X CARICAMENTO SEQ. BILA_FND_ESTR
      *----------------------------------------------------------------*
       WRITE-FILEOUT-B01.

           MOVE 'WRITE-FILEOUT-B01'
             TO  IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY
              THRU ESEGUI-DISPLAY-EX

           PERFORM S3030-VAL-DCLGEN-B01 THRU EX-S3030.

           INITIALIZE WK-VAR-REC-SIZE-F03 W-B01-LEN-TOT
           COMPUTE WK-VAR-REC-SIZE-F03 = LENGTH OF W-B01-REC-FISSO
                                       + 2

           MOVE WK-VAR-REC-SIZE-F03            TO W-B01-LEN-TOT

           MOVE AREA-OUT-W-B01                 TO FILESQS3-REC

           SET WRITE-FILESQS-OPER              TO TRUE
           SET WRITE-FILESQS3-SI               TO TRUE

           PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX

           IF SEQUENTIAL-ESITO-KO
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'WRITE-FILEOUT-B01'       TO IEAI9901-LABEL-ERR
              MOVE '005166'                  TO IEAI9901-COD-ERRORE
              MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
              MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       WRITE-FILEOUT-B01-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE SEQ. BILA_FND_ESTR
      *----------------------------------------------------------------*
       S3030-VAL-DCLGEN-B01.

           INITIALIZE  AREA-OUT-W-B01
      *                FILESQS3-REC.

            MOVE ZERO
               TO W-B01-ID-BILA-FND-ESTR
            MOVE ZERO
               TO W-B01-ID-BILA-TRCH-ESTR
           MOVE B01-COD-COMP-ANIA
              TO W-B01-COD-COMP-ANIA
           MOVE B01-ID-RICH-ESTRAZ-MAS
              TO W-B01-ID-RICH-ESTRAZ-MAS
           IF B01-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B01-ID-RICH-ESTRAZ-AGG-IND
              MOVE HIGH-VALUE
              TO W-B01-ID-RICH-ESTRAZ-AGG-NULL
           ELSE
              MOVE X'F0'
              TO W-B01-ID-RICH-ESTRAZ-AGG-IND
              MOVE B01-ID-RICH-ESTRAZ-AGG
              TO W-B01-ID-RICH-ESTRAZ-AGG
           END-IF
           MOVE B01-DT-RIS TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X   TO W-B01-DT-RIS
           MOVE B01-ID-POLI
              TO W-B01-ID-POLI
           MOVE B01-ID-ADES
              TO W-B01-ID-ADES
           MOVE B01-ID-GAR
              TO W-B01-ID-GAR
           MOVE B01-ID-TRCH-DI-GAR
              TO W-B01-ID-TRCH-DI-GAR
           MOVE B01-COD-FND
              TO W-B01-COD-FND
           IF B01-DT-QTZ-INI-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B01-DT-QTZ-INI-IND
              MOVE HIGH-VALUE
              TO W-B01-DT-QTZ-INI-NULL
           ELSE
             IF B01-DT-QTZ-INI = ZERO
                MOVE X'6F'
                TO W-B01-DT-QTZ-INI-IND
                MOVE HIGH-VALUE
                TO W-B01-DT-QTZ-INI-NULL
             ELSE
              MOVE X'F0'
              TO W-B01-DT-QTZ-INI-IND
              MOVE B01-DT-QTZ-INI TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B01-DT-QTZ-INI
             END-IF
           END-IF
           IF B01-NUM-QUO-INI-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B01-NUM-QUO-INI-IND
              MOVE HIGH-VALUE
              TO W-B01-NUM-QUO-INI-NULL
           ELSE
              MOVE X'F0'
              TO W-B01-NUM-QUO-INI-IND
              MOVE B01-NUM-QUO-INI
              TO W-B01-NUM-QUO-INI
           END-IF
           IF B01-NUM-QUO-DT-CALC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B01-NUM-QUO-DT-CALC-IND
              MOVE HIGH-VALUE
              TO W-B01-NUM-QUO-DT-CALC-NULL
           ELSE
              MOVE X'F0'
              TO W-B01-NUM-QUO-DT-CALC-IND
              MOVE B01-NUM-QUO-DT-CALC
              TO W-B01-NUM-QUO-DT-CALC
           END-IF
           IF B01-VAL-QUO-INI-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B01-VAL-QUO-INI-IND
              MOVE HIGH-VALUE
              TO W-B01-VAL-QUO-INI-NULL
           ELSE
              MOVE X'F0'
              TO W-B01-VAL-QUO-INI-IND
              MOVE B01-VAL-QUO-INI
              TO W-B01-VAL-QUO-INI
           END-IF

           IF B01-DT-VALZZ-QUO-DT-CA-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B01-DT-VALZZ-QUO-DT-CA-IND
              MOVE HIGH-VALUE
              TO W-B01-DT-VALZZ-QUO-DT-CA-NULL
           ELSE
             IF B01-DT-VALZZ-QUO-DT-CA = ZERO
                MOVE X'6F'
                TO W-B01-DT-VALZZ-QUO-DT-CA-IND
                MOVE HIGH-VALUE
                TO W-B01-DT-VALZZ-QUO-DT-CA-NULL
             ELSE
              MOVE X'F0'
              TO W-B01-DT-VALZZ-QUO-DT-CA-IND
              MOVE B01-DT-VALZZ-QUO-DT-CA TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B01-DT-VALZZ-QUO-DT-CA
             END-IF
           END-IF

           IF B01-VAL-QUO-DT-CALC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B01-VAL-QUO-DT-CALC-IND
              MOVE HIGH-VALUE
              TO W-B01-VAL-QUO-DT-CALC-NULL
           ELSE
              MOVE X'F0'
              TO W-B01-VAL-QUO-DT-CALC-IND
              MOVE B01-VAL-QUO-DT-CALC
              TO W-B01-VAL-QUO-DT-CALC
           END-IF

           IF B01-VAL-QUO-T-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B01-VAL-QUO-T-IND
              MOVE HIGH-VALUE
              TO W-B01-VAL-QUO-T-NULL
           ELSE
              MOVE X'F0'
              TO W-B01-VAL-QUO-T-IND
              MOVE B01-VAL-QUO-T
              TO W-B01-VAL-QUO-T
           END-IF

           IF B01-PC-INVST-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B01-PC-INVST-IND
              MOVE HIGH-VALUE
              TO W-B01-PC-INVST-NULL
           ELSE
              MOVE X'F0'
              TO W-B01-PC-INVST-IND
              MOVE B01-PC-INVST
              TO W-B01-PC-INVST
           END-IF

           MOVE 'I'
              TO W-B01-DS-OPER-SQL
           IF B01-DS-VER NOT NUMERIC
              MOVE 0 TO W-B01-DS-VER
           ELSE
              MOVE B01-DS-VER
              TO W-B01-DS-VER
           END-IF
           MOVE IDSV0001-DATA-COMP-AGG-STOR
              TO W-B01-DS-TS-CPTZ
           MOVE IDSV0001-USER-NAME
              TO W-B01-DS-UTENTE
           MOVE '1'
              TO W-B01-DS-STATO-ELAB.

       EX-S3030.
           EXIT.

      *----------------------------------------------------------------*
      *    PREPARA AREA TRANCHE DI GARANZIA X LCCS0450
      *----------------------------------------------------------------*
       PREPARA-ATGA-FND.

PERFOR*--> INIZIALIZZAZIONE AREA TRANCHE DI GARANZIA X LCCS0450
PERFOR*    INITIALIZE                             ATGA-TAB-TRAN(1).
PERFOR*
PERFOR*    MOVE ATGA-TAB                       TO ATGA-RESTO-TAB.

      *--> PASSIAMO UNA TRANCHE X VOLTA
           MOVE 1                              TO ATGA-ELE-TGA-MAX.
           MOVE WTGA-TAB-TRAN(IX-TAB-TGA)       TO ATGA-TAB-TRAN(1).

       PREPARA-ATGA-FND-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ESTRAZIONE DEL VALORE SINGOLA QUOTA DALL'ENTITA' QUOTAZIONI
      *    FONDI UNIT
      *----------------------------------------------------------------*
       LEGGI-QUOTAZ-FONDI.

           MOVE 'LEGGI-QUOTAZ-FONDI'       TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO        TO TRUE.
           MOVE WK-LABEL-ERR               TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY          THRU ESEGUI-DISPLAY-EX.

           MOVE WS-DT-EFFETTO              TO LCCC0450-DATA-EFFETTO.
           MOVE WS-DT-TS-PTF               TO LCCC0450-DATA-COMPETENZA.
           MOVE ZEROES                     TO LCCC0450-DATA-RICORR.
           MOVE 'TG'                       TO LCCC0450-FLAG-OPER.
           MOVE WLB-DT-RISERVA             TO LCCC0450-DATA-RISERVA.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE.
           MOVE 'LCCS0450'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE.
           MOVE SPACES                     TO IDSV8888-DESC-PGM.
           STRING 'ESTR. QUOTA FONDI UNIT'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING.
           PERFORM ESEGUI-DISPLAY          THRU ESEGUI-DISPLAY-EX.

           CALL LCCS0450 USING  AREA-IDSV0001
                                WGRZ-AREA-GARANZIA
                                ATGA-AREA-TGA
                                WL19-AREA-FONDI
                                LCCC0450
           ON EXCEPTION
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'LCCS0450'
                    TO CALL-DESC
                  MOVE 'LEGGI-QUOTAZ-FONDI'
                    TO IEAI9901-LABEL-ERR
                  PERFORM S0290-ERRORE-DI-SISTEMA
                     THRU EX-S0290
           END-CALL.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE.
           MOVE 'LCCS0450'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE.
           MOVE SPACES                     TO IDSV8888-DESC-PGM.
           STRING 'ESTR. QUOTA FONDI UNIT'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING.
           PERFORM ESEGUI-DISPLAY          THRU ESEGUI-DISPLAY-EX.

           IF IDSV0001-ESITO-OK
              IF LCCC0450-ELE-TAB-MAX > 0
                 CONTINUE
              ELSE
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR
                   TO IEAI9901-LABEL-ERR
                 MOVE '005166'
                   TO IEAI9901-COD-ERRORE
                 MOVE 'LCCS0450 NON CI SONO FONDI DA ELABORARE'
                   TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

       LEGGI-QUOTAZ-FONDI-EX.
           EXIT.
      *-----------------------------------------------------------------
      *   AGGIORNA TABELLA BILA-FND-ESTR
      *-----------------------------------------------------------------
       AGGIORNA-BILA-FND-ESTR.

      *--> TIPO OPERAZIONE DISPATCHER
           SET IDSI0011-INSERT             TO TRUE.

      *--> DCLGEN TABELLA
           MOVE BILA-FND-ESTR              TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-PRIMARY-KEY        TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-SENZA-STOR  TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE IDSV0001-DATA-EFFETTO   TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

      *--> CALL DISPATCHER PER AGGIORNAMENTO
           PERFORM AGGIORNA-TABELLA
              THRU AGGIORNA-TABELLA-EX.

       AGGIORNA-BILA-FND-ESTR-EX.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
      *WRITE-VAR-LISTA.
      *
      *    MOVE 'WRITE-VAR-LISTA'      TO WK-LABEL-ERR.
      *    SET IDSV8888-DEBUG-MEDIO         TO TRUE
      *    MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
      *    PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.
      *
      *    IF LCCC0450-ELE-TAB-MAX > 0
      *
      *       PERFORM STRING-VAR-LCODFONDO
      *          THRU EX-STRING-VAR-LCODFONDO
      *
      *       PERFORM STRING-VAR-LNUMQUOTE
      *          THRU EX-STRING-VAR-LNUMQUOTE
      *
      *       PERFORM STRING-VAR-LVALQUOTE
      *          THRU EX-STRING-VAR-LVALQUOTE
      *
      *       PERFORM STRING-VAR-LVALQUOTEINI
      *          THRU EX-STRING-VAR-LVALQUOTEINI
      *
      *       PERFORM STRING-VAR-LPERCINVF
      *          THRU EX-STRING-VAR-LPERCINVF
      *
      *       PERFORM STRING-VAR-LVALQUOTET
      *          THRU EX-STRING-VAR-LVALQUOTET
      *
      *    END-IF.
      *
      *EX-WRITE-VAR-LISTA.
      *    EXIT.
      * ----------------------------------------------------------------
      *
      * ----------------------------------------------------------------
       STRING-VAR-LCODFONDO.

           MOVE 'STRING-VAR-LCODFONDO'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE WK-VARIABILE
                      WK-TIPO-DATO
                      IDSV0501-INPUT.
           SET NO-TROVATA-VARIABILE TO TRUE.
           MOVE ZERO TO IX-TAB-501.
           PERFORM VARYING IX-TAB-450 FROM 1 BY 1
           UNTIL IX-TAB-450 > LCCC0450-ELE-TAB-MAX

             IF  LCCC0450-NUM-QUOTE(IX-TAB-450) > 0
             AND LCCC0450-ID-TRCH-DI-GAR(IX-TAB-450) =
                 WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                 ADD 1                  TO IX-TAB-501
                 MOVE 'L'               TO IDSV0501-TP-DATO(IX-TAB-501)
                 MOVE LCCC0450-COD-FONDO(IX-TAB-450)
                   TO IDSV0501-VAL-STR(IX-TAB-501)
                 SET SI-TROVATA-VARIABILE  TO TRUE
             END-IF

           END-PERFORM.

           IF  IDSV0001-ESITO-OK
           AND SI-TROVATA-VARIABILE
              PERFORM STRINGA-X-VALORIZZATORE
                 THRU STRINGA-X-VALORIZZATORE-EX

              IF IDSV0501-SUCCESSFUL-RC
                 MOVE IDSV0501-OUTPUT
                   TO IVVC0501-OUTPUT
                 MOVE 'LCODFONDO'
                   TO WK-VARIABILE
                 MOVE 'L'
                   TO WK-TIPO-DATO
                 PERFORM WRITE-VARIABILE
                    THRU EX-WRITE-VARIABILE
              ELSE
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'STRING-VAR-LCODFONDO'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005016'
                   TO IEAI9901-COD-ERRORE
                 STRING 'IDSV501;'
                        IDSV0501-RETURN-CODE ';'
                        IDSV0501-DESCRIZ-ERR
                   DELIMITED BY SIZE
                   INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

       EX-STRING-VAR-LCODFONDO.
           EXIT.
      * ----------------------------------------------------------------
      *
      * ----------------------------------------------------------------
       STRING-VAR-LNUMQUOTE.

           MOVE 'STRING-VAR-LNUMQUOTE'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE WK-VARIABILE
                      IDSV0501-INPUT.
           SET NO-TROVATA-VARIABILE TO TRUE.
           MOVE ZERO TO IX-TAB-501.
           PERFORM VARYING IX-TAB-450 FROM 1 BY 1
           UNTIL IX-TAB-450 > LCCC0450-ELE-TAB-MAX

             IF  LCCC0450-NUM-QUOTE(IX-TAB-450) > 0
             AND LCCC0450-ID-TRCH-DI-GAR(IX-TAB-450) =
                 WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                 ADD 1                  TO IX-TAB-501
                 MOVE 'R'               TO IDSV0501-TP-DATO(IX-TAB-501)
                 MOVE LCCC0450-NUM-QUOTE(IX-TAB-450)
                   TO IDSV0501-VAL-IMP(IX-TAB-501)
                 SET SI-TROVATA-VARIABILE  TO TRUE
             END-IF

           END-PERFORM.

           IF  IDSV0001-ESITO-OK
           AND SI-TROVATA-VARIABILE
              PERFORM STRINGA-X-VALORIZZATORE
                 THRU STRINGA-X-VALORIZZATORE-EX

              IF IDSV0501-SUCCESSFUL-RC
                 MOVE IDSV0501-OUTPUT
                   TO IVVC0501-OUTPUT
                 MOVE 'LNUMQUOTE'
                   TO WK-VARIABILE
                 MOVE 'R'
                   TO WK-TIPO-DATO
                 PERFORM WRITE-VARIABILE
                    THRU EX-WRITE-VARIABILE
              ELSE
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'STRING-VAR-LNUMQUOTE'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005016'
                   TO IEAI9901-COD-ERRORE
                 STRING 'IDSV501;'
                        IDSV0501-RETURN-CODE ';'
                        IDSV0501-DESCRIZ-ERR
                   DELIMITED BY SIZE
                   INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

       EX-STRING-VAR-LNUMQUOTE.
           EXIT.
      * ----------------------------------------------------------------
      *
      * ----------------------------------------------------------------
       STRING-VAR-LVALQUOTE.

           MOVE 'STRING-VAR-LVALQUOTE'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE WK-VARIABILE
                      IDSV0501-INPUT.
           SET NO-TROVATA-VARIABILE TO TRUE.
           MOVE ZERO TO IX-TAB-501.
           PERFORM VARYING IX-TAB-450 FROM 1 BY 1
           UNTIL IX-TAB-450 > LCCC0450-ELE-TAB-MAX

             IF  LCCC0450-NUM-QUOTE(IX-TAB-450) > 0
             AND LCCC0450-ID-TRCH-DI-GAR(IX-TAB-450) =
                 WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                 ADD 1                  TO IX-TAB-501
                 MOVE 'R'               TO IDSV0501-TP-DATO(IX-TAB-501)
                 MOVE LCCC0450-VAL-QUOTA(IX-TAB-450)
                   TO IDSV0501-VAL-IMP(IX-TAB-501)
                 SET SI-TROVATA-VARIABILE  TO TRUE
             END-IF

           END-PERFORM.

           IF  IDSV0001-ESITO-OK
           AND SI-TROVATA-VARIABILE
              PERFORM STRINGA-X-VALORIZZATORE
                 THRU STRINGA-X-VALORIZZATORE-EX

              IF IDSV0501-SUCCESSFUL-RC
                 MOVE IDSV0501-OUTPUT
                   TO IVVC0501-OUTPUT
                 MOVE 'LVALQUOTE'
                   TO WK-VARIABILE
                 MOVE 'R'
                   TO WK-TIPO-DATO
                 PERFORM WRITE-VARIABILE
                    THRU EX-WRITE-VARIABILE
              ELSE
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'STRING-VAR-LVALQUOTE'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005016'
                   TO IEAI9901-COD-ERRORE
                 STRING 'IDSV501;'
                        IDSV0501-RETURN-CODE ';'
                        IDSV0501-DESCRIZ-ERR
                   DELIMITED BY SIZE
                   INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

       EX-STRING-VAR-LVALQUOTE.
           EXIT.
      * ----------------------------------------------------------------
      *
      * ----------------------------------------------------------------
       STRING-VAR-LVALQUOTEINI.

           MOVE 'STRING-VAR-LVALQUOTEINI'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE WK-VARIABILE
                      IDSV0501-INPUT.
           SET NO-TROVATA-VARIABILE TO TRUE.
           MOVE ZERO TO IX-TAB-501.
           PERFORM VARYING IX-TAB-450 FROM 1 BY 1
           UNTIL IX-TAB-450 > LCCC0450-ELE-TAB-MAX

             IF  LCCC0450-NUM-QUOTE(IX-TAB-450) > 0
             AND LCCC0450-ID-TRCH-DI-GAR(IX-TAB-450) =
                 WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                 ADD 1                  TO IX-TAB-501
                 MOVE 'R'               TO IDSV0501-TP-DATO(IX-TAB-501)
                 MOVE LCCC0450-VAL-QUOTA-INI(IX-TAB-450)
                   TO IDSV0501-VAL-IMP(IX-TAB-501)
                 SET SI-TROVATA-VARIABILE  TO TRUE
             END-IF

           END-PERFORM.

           IF  IDSV0001-ESITO-OK
           AND SI-TROVATA-VARIABILE
              PERFORM STRINGA-X-VALORIZZATORE
                 THRU STRINGA-X-VALORIZZATORE-EX

              IF IDSV0501-SUCCESSFUL-RC
                 MOVE IDSV0501-OUTPUT
                   TO IVVC0501-OUTPUT
                 MOVE 'LVALQUOTEINI'
                   TO WK-VARIABILE
                 MOVE 'R'
                   TO WK-TIPO-DATO
                 PERFORM WRITE-VARIABILE
                    THRU EX-WRITE-VARIABILE
              ELSE
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'STRING-VAR-LVALQUOTEINI'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005016'
                   TO IEAI9901-COD-ERRORE
                 STRING 'IDSV501;'
                        IDSV0501-RETURN-CODE ';'
                        IDSV0501-DESCRIZ-ERR
                   DELIMITED BY SIZE
                   INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

       EX-STRING-VAR-LVALQUOTEINI.
           EXIT.
      * ----------------------------------------------------------------
      *
      * ----------------------------------------------------------------
       STRING-VAR-LPERCINVF.

           MOVE 'STRING-VAR-LPERCINVF'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE WK-VARIABILE
                      IDSV0501-INPUT.
           SET NO-TROVATA-VARIABILE TO TRUE.
           MOVE ZERO TO IX-TAB-501.
           PERFORM VARYING IX-TAB-450 FROM 1 BY 1
           UNTIL IX-TAB-450 > LCCC0450-ELE-TAB-MAX

             IF  LCCC0450-NUM-QUOTE(IX-TAB-450) > 0
             AND LCCC0450-ID-TRCH-DI-GAR(IX-TAB-450) =
                 WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                 ADD 1                  TO IX-TAB-501
                 MOVE 'O'               TO IDSV0501-TP-DATO(IX-TAB-501)
                 MOVE LCCC0450-PERCENT-INV(IX-TAB-450)
                   TO IDSV0501-VAL-PERC(IX-TAB-501)
                 SET SI-TROVATA-VARIABILE  TO TRUE
             END-IF

           END-PERFORM.

           IF  IDSV0001-ESITO-OK
           AND SI-TROVATA-VARIABILE
              PERFORM STRINGA-X-VALORIZZATORE
                 THRU STRINGA-X-VALORIZZATORE-EX

              IF IDSV0501-SUCCESSFUL-RC
                 MOVE IDSV0501-OUTPUT
                   TO IVVC0501-OUTPUT
                 MOVE 'LPERCINVF'
                   TO WK-VARIABILE
                 MOVE 'O'
                   TO WK-TIPO-DATO
                 PERFORM WRITE-VARIABILE
                    THRU EX-WRITE-VARIABILE
              ELSE
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'STRING-VAR-LPERCINVF'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005016'
                   TO IEAI9901-COD-ERRORE
                 STRING 'IDSV501;'
                        IDSV0501-RETURN-CODE ';'
                        IDSV0501-DESCRIZ-ERR
                   DELIMITED BY SIZE
                   INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

       EX-STRING-VAR-LPERCINVF.
           EXIT.
      *-----------------------------------------------------------------
      *    VARIABILE LVALQUOTET
      *-----------------------------------------------------------------
       STRING-VAR-LVALQUOTET.

           MOVE 'STRING-VAR-LVALQUOTET'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE WK-VARIABILE
                      IDSV0501-INPUT.

           SET NO-TROVATA-VARIABILE TO TRUE.
           MOVE ZERO TO IX-TAB-501.
           PERFORM VARYING IX-TAB-450 FROM 1 BY 1
           UNTIL IX-TAB-450 > LCCC0450-ELE-TAB-MAX

             IF  LCCC0450-NUM-QUOTE(IX-TAB-450) > 0
             AND LCCC0450-ID-TRCH-DI-GAR(IX-TAB-450) =
                 WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                 ADD 1                  TO IX-TAB-501
                 MOVE 'R'               TO IDSV0501-TP-DATO(IX-TAB-501)
                 MOVE LCCC0450-VAL-QUOTA-T(IX-TAB-450)
                   TO IDSV0501-VAL-IMP(IX-TAB-501)
                 SET SI-TROVATA-VARIABILE  TO TRUE
             END-IF

           END-PERFORM.

           IF  IDSV0001-ESITO-OK
           AND SI-TROVATA-VARIABILE
              PERFORM STRINGA-X-VALORIZZATORE
                 THRU STRINGA-X-VALORIZZATORE-EX

              IF IDSV0501-SUCCESSFUL-RC
                 MOVE IDSV0501-OUTPUT
                   TO IVVC0501-OUTPUT
                 MOVE 'LVALQUOTET'
                   TO WK-VARIABILE
                 MOVE 'R'
                   TO WK-TIPO-DATO
                 PERFORM WRITE-VARIABILE
                    THRU EX-WRITE-VARIABILE
              ELSE
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'STRING-VAR-LVALQUOTET'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005016'
                   TO IEAI9901-COD-ERRORE
                 STRING 'IDSV0501;'
                        IDSV0501-RETURN-CODE ';'
                        IDSV0501-DESCRIZ-ERR
                   DELIMITED BY SIZE
                   INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

       EX-STRING-VAR-LVALQUOTET.
           EXIT.
      * ----------------------------------------------------------------
      *
      * ----------------------------------------------------------------
       WRITE-VARIABILE.

           MOVE 'WRITE-VARIABILE'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           PERFORM VARYING IX-TAB-LISTA FROM 1 BY 1
           UNTIL IX-TAB-LISTA > IVVC0501-MAX-TAB-STR

              PERFORM INIZIA-TOT-B05
                 THRU INIZIA-TOT-B05-EX

              SET  WB05-ST-ADD                 TO TRUE
              MOVE 1                           TO WB05-ELE-B05-MAX

              MOVE WPOL-COD-COMP-ANIA
                TO WB05-COD-COMP-ANIA

      *        MOVE WB03-ID-BILA-TRCH-ESTR
      *          TO WB05-ID-BILA-TRCH-ESTR

              IF WLB-TP-RICH = 'B1'
                 MOVE WLB-ID-RICH       TO WB05-ID-RICH-ESTRAZ-MAS
                 MOVE HIGH-VALUES       TO WB05-ID-RICH-ESTRAZ-AGG-NULL
              END-IF

              IF WLB-TP-RICH = 'B2'
                 MOVE WLB-ID-RICH-COLL  TO WB05-ID-RICH-ESTRAZ-MAS
                 MOVE WLB-ID-RICH       TO WB05-ID-RICH-ESTRAZ-AGG
              END-IF

              MOVE WLB-DT-RISERVA
                TO WB05-DT-RIS

              MOVE WPOL-ID-POLI
                TO WB05-ID-POLI
              MOVE WADE-ID-ADES(IX-TAB-ADE)
                TO WB05-ID-ADES
              MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                TO WB05-ID-TRCH-DI-GAR
              MOVE 0
                TO WB05-PROG-SCHEDA-VALOR

              IF WGRZ-DT-INI-VAL-TAR-NULL(IX-TAB-GRZ) = HIGH-VALUES
      *da rivedere
                 MOVE WS-DT-EFFETTO
                   TO WB05-DT-INI-VLDT-TARI
              ELSE
                 MOVE WGRZ-DT-INI-VAL-TAR(IX-TAB-GRZ)
                   TO WB05-DT-INI-VLDT-TARI
              END-IF

              MOVE WTGA-TP-RGM-FISC(IX-TAB-TGA) TO WB05-TP-RGM-FISC


              MOVE WPOL-DT-INI-VLDT-PROD        TO WB05-DT-INI-VLDT-PROD

              MOVE WTGA-DT-DECOR(IX-TAB-TGA)    TO WB05-DT-DECOR-TRCH

      * AREA VARIABILI
              MOVE WK-VARIABILE
                TO WB05-COD-VAR
              MOVE WK-TIPO-DATO
                TO WB05-TP-D
              MOVE IVVC0501-STRINGA-TOT(IX-TAB-LISTA)
                TO WB05-VAL-STRINGA

              PERFORM S1450-CALL-EOC-B05
                 THRU EX-S1450

           END-PERFORM.

       EX-WRITE-VARIABILE.
           EXIT.
      * ----------------------------------------------------------------
      * Chiamata al modulo di scrittura della tabella VAR_DI_CALC_P
      * ----------------------------------------------------------------
       S1470-CALL-EOC-B04.

           MOVE 'S1470-CALL-EOC-B04'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LLBS0266'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Scritt. BILA_VAR_CALC_P'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           CALL LLBS0266    USING AREA-IDSV0001
                                  AREA-OUT-B04
           ON EXCEPTION
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'PROGRAMMA DI AGGIORNAMENTO TABELLE'
                TO CALL-DESC
              MOVE 'S1450-CALL-EOC-B04'
                TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LLBS0266'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Scritt. BILA_VAR_CALC_P'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S1470.
           EXIT.

      *----------------------------------------------------------------*
      *    SCRITTURA FILE SEQUENZIALE X CARICAMENTO TAB. BILA_TRCH_ESTR
      *----------------------------------------------------------------*
       S1480-WRITE-FILEOUT.

           MOVE 'S1480-WRITE-FILEOUT'
             TO  IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY
              THRU ESEGUI-DISPLAY-EX

           PERFORM S1510-VAL-DCLGEN-B05 THRU EX-S1510.

           INITIALIZE WK-VAR-REC-SIZE-F01 W-B05-LEN-TOT
           COMPUTE WK-VAR-REC-SIZE-F01 = LENGTH OF W-B05-REC-FISSO
                                       + W-B05-AREA-D-VALOR-VAR-LEN
                                       + 5

           MOVE WK-VAR-REC-SIZE-F01            TO W-B05-LEN-TOT

           MOVE AREA-OUT-W-B05                 TO FILESQS1-REC

           SET WRITE-FILESQS-OPER              TO TRUE
           SET WRITE-FILESQS1-SI               TO TRUE

           PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX

           IF SEQUENTIAL-ESITO-KO
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1480-WRITE-FILEOUT'     TO IEAI9901-LABEL-ERR
              MOVE '005166'                  TO IEAI9901-COD-ERRORE
              MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
              MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1480.
           EXIT.
      *----------------------------------------------------------------*
      * OPEN FILE SEQUENZIALE
      *----------------------------------------------------------------*
       S1490-OPEN-FILEOUT.

           SET IDSV8888-DEBUG-BASSO         TO TRUE
           MOVE 'S1490-OPEN-FILEOUT'           TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY
              THRU ESEGUI-DISPLAY-EX.

           SET OPEN-FILESQS-OPER      TO TRUE
           SET OPEN-FILESQS-ALL-SI    TO TRUE
           SET OUTPUT-FILESQS-ALL     TO TRUE

           PERFORM CALL-FILESEQ       THRU CALL-FILESEQ-EX

           IF SEQUENTIAL-ESITO-KO
              MOVE WK-PGM               TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1490-OPEN-FILEOUT' TO IEAI9901-LABEL-ERR
              MOVE '005166'             TO IEAI9901-COD-ERRORE
              MOVE SPACES TO IEAI9901-PARAMETRI-ERR
              MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                TO IEAI9901-PARAMETRI-ERR

              PERFORM S0300-RICERCA-GRAVITA-ERRORE  THRU EX-S0300
           END-IF.

      *
       EX-S1490.
           EXIT.
      *----------------------------------------------------------------*
      * CLOSE FILE SEQUENZIALE
      *----------------------------------------------------------------*
       S1500-CLOSE-FILEOUT.

           SET  IDSV8888-DEBUG-BASSO           TO TRUE
           MOVE 'S1500-CLOSE-FILEOUT'       TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY
              THRU ESEGUI-DISPLAY-EX.

           SET CLOSE-FILESQS-OPER     TO TRUE
           SET CLOSE-FILESQS-ALL-SI   TO TRUE

           PERFORM CALL-FILESEQ       THRU CALL-FILESEQ-EX

           IF SEQUENTIAL-ESITO-KO
              MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1500-CLOSE-FILEOUT' TO IEAI9901-LABEL-ERR
              MOVE '005166'              TO IEAI9901-COD-ERRORE
              MOVE SPACES TO IEAI9901-PARAMETRI-ERR
              MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                TO IEAI9901-PARAMETRI-ERR

              PERFORM S0300-RICERCA-GRAVITA-ERRORE  THRU EX-S0300
           END-IF.


       EX-S1500.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA TRACCIATO DI OUTPUT
      *----------------------------------------------------------------*
       S1510-VAL-DCLGEN-B05.

           INITIALIZE  AREA-OUT-W-B05
      *                FILESQS1-REC.

            MOVE ZERO
               TO W-B05-ID-BILA-VAR-CALC-T
           MOVE WB05-COD-COMP-ANIA
              TO W-B05-COD-COMP-ANIA
            MOVE ZERO
               TO W-B05-ID-BILA-TRCH-ESTR
           MOVE WB05-ID-RICH-ESTRAZ-MAS
              TO W-B05-ID-RICH-ESTRAZ-MAS
           IF WB05-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B05-ID-RICH-ESTRAZ-AGG-IND
              MOVE HIGH-VALUE
              TO W-B05-ID-RICH-ESTRAZ-AGG-NULL
           ELSE
              MOVE X'F0'
              TO W-B05-ID-RICH-ESTRAZ-AGG-IND
              MOVE WB05-ID-RICH-ESTRAZ-AGG
              TO W-B05-ID-RICH-ESTRAZ-AGG
           END-IF
           IF WB05-DT-RIS-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B05-DT-RIS-IND
              MOVE HIGH-VALUE
              TO W-B05-DT-RIS
           ELSE
             IF WB05-DT-RIS = ZERO
                MOVE X'6F'
                TO W-B05-DT-RIS-IND
                MOVE HIGH-VALUE
                TO W-B05-DT-RIS
             ELSE
              MOVE X'F0'
              TO W-B05-DT-RIS-IND
              MOVE WB05-DT-RIS TO WS-DATE-N
              PERFORM Z700-DT-N-TO-X THRU Z700-EX
              MOVE WS-DATE-X   TO W-B05-DT-RIS
             END-IF
           END-IF
           MOVE WB05-ID-POLI
              TO W-B05-ID-POLI
           MOVE WB05-ID-ADES
              TO W-B05-ID-ADES
           MOVE WB05-ID-TRCH-DI-GAR
              TO W-B05-ID-TRCH-DI-GAR
           IF WB05-PROG-SCHEDA-VALOR-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B05-PROG-SCHEDA-VALOR-IND
              MOVE HIGH-VALUE
              TO W-B05-PROG-SCHEDA-VALOR-NULL
           ELSE
              MOVE X'F0'
              TO W-B05-PROG-SCHEDA-VALOR-IND
              MOVE WB05-PROG-SCHEDA-VALOR
              TO W-B05-PROG-SCHEDA-VALOR
           END-IF
           MOVE WB05-DT-INI-VLDT-TARI    TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X   TO W-B05-DT-INI-VLDT-TARI
           MOVE WB05-TP-RGM-FISC
              TO W-B05-TP-RGM-FISC
           MOVE WB05-DT-INI-VLDT-PROD    TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X   TO W-B05-DT-INI-VLDT-PROD
           MOVE WB05-DT-DECOR-TRCH    TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X   TO W-B05-DT-DECOR-TRCH
           MOVE WB05-COD-VAR
              TO W-B05-COD-VAR
           MOVE WB05-TP-D
              TO W-B05-TP-D
           IF WB05-VAL-IMP-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B05-VAL-IMP-IND
              MOVE HIGH-VALUE
              TO W-B05-VAL-IMP-NULL
           ELSE
              MOVE X'F0'
              TO W-B05-VAL-IMP-IND
              MOVE WB05-VAL-IMP
              TO W-B05-VAL-IMP
           END-IF
           IF WB05-VAL-PC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B05-VAL-PC-IND
              MOVE HIGH-VALUE
              TO W-B05-VAL-PC-NULL
           ELSE
              MOVE X'F0'
              TO W-B05-VAL-PC-IND
              MOVE WB05-VAL-PC
              TO W-B05-VAL-PC
           END-IF
           IF WB05-VAL-STRINGA-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B05-VAL-STRINGA-IND
              MOVE HIGH-VALUE
              TO W-B05-VAL-STRINGA-NULL
           ELSE
              MOVE X'F0'
              TO W-B05-VAL-STRINGA-IND
              MOVE WB05-VAL-STRINGA
              TO W-B05-VAL-STRINGA
           END-IF
           MOVE 'I'
              TO W-B05-DS-OPER-SQL
           IF WB05-DS-VER NOT NUMERIC
              MOVE 0 TO W-B05-DS-VER
           ELSE
              MOVE WB05-DS-VER
              TO W-B05-DS-VER
           END-IF
           MOVE IDSV0001-DATA-COMP-AGG-STOR
              TO W-B05-DS-TS-CPTZ
           MOVE IDSV0001-USER-NAME
              TO W-B05-DS-UTENTE
           MOVE '1'
              TO W-B05-DS-STATO-ELAB.

           IF WB05-AREA-D-VALOR-VAR = HIGH-VALUE
              MOVE X'6F'
                TO W-B05-AREA-D-VALOR-VAR-IND
           ELSE
              IF WK-STRINGATURA-SI
                 MOVE WB05-AREA-D-VALOR-VAR-LEN
                   TO W-B05-AREA-D-VALOR-VAR-LEN
                 MOVE WB05-AREA-D-VALOR-VAR
                   TO W-B05-AREA-D-VALOR-VAR
              ELSE
                 MOVE 1
                   TO W-B05-AREA-D-VALOR-VAR-LEN
                 MOVE WB05-AREA-D-VALOR-VAR
                   TO W-B05-AREA-D-VALOR-VAR
              END-IF
           END-IF.

       EX-S1510.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPAREA AREA "VARIABILI DI CALCOLO PRODOTTO"
      *----------------------------------------------------------------*
       S1310-VALORIZZA-OUT-B04.

           MOVE 'S1310-VALORIZZA-OUT-B04'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> VALORIZZAZIONE OCCURS AREA INPUT SERVIZIO CALCOLI E CONTROLLI
      *--> CON L'OUTPUT DEL VALORIZZATORE VARIABILI
           PERFORM S1360-AREA-SCHEDA
              THRU EX-S1360
           VARYING IX-AREA-SCHEDA FROM 1 BY 1
             UNTIL IX-AREA-SCHEDA > WSKD-ELE-LIVELLO-MAX-P.

       EX-S1310.
           EXIT.
      *----------------------------------------------------------------*
      * CICLO DI VALORIZZAZIONE DCLGEN TABELLA "VARIABILI DI CALCOLO_P"
      * CON L'OUTPUT DEL VALORIZZATORE VARIABILI
      *----------------------------------------------------------------*
       S1360-AREA-SCHEDA.

           MOVE 'S1360-AREA-SCHEDA'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.


           IF WSKD-TP-LIVELLO-P(IX-AREA-SCHEDA) = 'P'
      * AGGIUNGO ALLE VARIABILI DA STRINGARE LA VARIABILE DEE
      * ESPORTATA IN CHIARO DAL VALORIZZATORE VARIABILI

              ADD 1
               TO WSKD-ELE-VARIABILI-MAX-P(IX-AREA-SCHEDA)
              MOVE WSKD-ELE-VARIABILI-MAX-P(IX-AREA-SCHEDA)
                TO IX-MAX-VAR
              MOVE 'DEE'
                TO WSKD-COD-VARIABILE-P
              (IX-AREA-SCHEDA, IX-MAX-VAR)

              MOVE 'D'
                TO WSKD-TP-DATO-P
                   (IX-AREA-SCHEDA, IX-MAX-VAR)
              MOVE WSKD-DEE
                TO WSKD-VAL-STR-P
                   (IX-AREA-SCHEDA, IX-MAX-VAR)
      *
              IF WK-STRINGATURA-SI
                 PERFORM S1410-AREA-VARIABILI
                    THRU EX-S1410
                 VARYING IX-TAB-VAR FROM 1 BY 1
                   UNTIL IX-TAB-VAR > 1
              ELSE
                 PERFORM S1410-AREA-VARIABILI
                    THRU EX-S1410
                 VARYING IX-TAB-VAR FROM 1 BY 1
                   UNTIL IX-TAB-VAR >
                         WSKD-ELE-VARIABILI-MAX-P(IX-AREA-SCHEDA)
              END-IF
           END-IF.
      *
       EX-S1360.
           EXIT.
      *----------------------------------------------------------------*
      * CICLO DI VALORIZZAZIONE DELL'AREA VARIABILI DELLA TABELLA
      * "VARIABILI DI CALCOLO PRODOTTO"
      *----------------------------------------------------------------*
       S1410-AREA-VARIABILI.

           MOVE 'S1410-AREA-VARIABILI'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           PERFORM INIZIA-TOT-B04
              THRU INIZIA-TOT-B04-EX.

           SET  WB04-ST-ADD                 TO TRUE
           MOVE 1                           TO WB04-ELE-B04-MAX

           MOVE WPOL-COD-COMP-ANIA
             TO WB04-COD-COMP-ANIA


           IF WLB-TP-RICH = 'B1'
              MOVE WLB-ID-RICH           TO WB04-ID-RICH-ESTRAZ-MAS
              MOVE HIGH-VALUES           TO WB04-ID-RICH-ESTRAZ-AGG-NULL
           END-IF

           IF WLB-TP-RICH = 'B2'
              MOVE WLB-ID-RICH-COLL      TO WB04-ID-RICH-ESTRAZ-MAS
              MOVE WLB-ID-RICH           TO WB04-ID-RICH-ESTRAZ-AGG
           END-IF

           MOVE WLB-DT-RISERVA
             TO WB04-DT-RIS

           MOVE WPOL-ID-POLI
             TO WB04-ID-POLI

           MOVE WADE-ID-ADES(IX-TAB-ADE)
             TO WB04-ID-ADES

           ADD 1
             TO WB04-PROG-SCHEDA-VALOR

           MOVE WSKD-DT-INIZ-PROD-P(IX-AREA-SCHEDA)
             TO WB04-DT-INI-VLDT-PROD
           MOVE WSKD-COD-RGM-FISC-P(IX-AREA-SCHEDA)
             TO WB04-TP-RGM-FISC

      * AREA VARIABILI
           IF WK-STRINGATURA-SI


              MOVE WSKD-AREA-VARIABILI-P(IX-AREA-SCHEDA)
                TO IDSV0503-INPUT

              SET  IDSV8888-BUSINESS-DBG      TO TRUE
              MOVE 'STRINGA2'                 TO IDSV8888-NOME-PGM
              SET  IDSV8888-INIZIO            TO TRUE
              MOVE SPACES                     TO IDSV8888-DESC-PGM
              STRING 'String. BILA_VAR_CALC_P'
                     DELIMITED BY SIZE
                     INTO IDSV8888-DESC-PGM
              END-STRING
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

              PERFORM S503-CALL-STRINGATURA
                 THRU S503-CALL-STRINGATURA-EX

              SET  IDSV8888-BUSINESS-DBG      TO TRUE
      *       MOVE 'STRINGA2'                 TO IDSV8888-NOME-PGM
              SET  IDSV8888-FINE              TO TRUE
              MOVE SPACES                     TO IDSV8888-DESC-PGM
              MOVE IDSV0503-ELE-VARIABILI-MAX TO WK-NUM-VAR
              MOVE IDSV0503-LEN-STRINGA-TOT   TO WK-LUNGH-STRINGA
              STRING 'STRINGA2 '
                     'NumVar ' WK-NUM-VAR  ' LunStr ' WK-LUNGH-STRINGA
                      DELIMITED BY SIZE
                      INTO IDSV8888-NOME-PGM
              END-STRING
              STRING 'String. BILA_VAR_CALC_P'
                      DELIMITED BY SIZE
                      INTO IDSV8888-DESC-PGM
              END-STRING
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

              MOVE IDSV0503-LEN-STRINGA-TOT TO WB04-AREA-D-VALOR-VAR-LEN
              MOVE IDSV0503-STRINGA-TOT     TO WB04-AREA-D-VALOR-VAR
           ELSE
              MOVE WSKD-COD-VARIABILE-P(IX-AREA-SCHEDA, IX-TAB-VAR)
                TO WB04-COD-VAR
              MOVE WSKD-TP-DATO-P(IX-AREA-SCHEDA, IX-TAB-VAR)
                TO WB04-TP-D

              EVALUATE WSKD-TP-DATO-P(IX-AREA-SCHEDA, IX-TAB-VAR)
                  WHEN 'D'
                  WHEN 'S'
                  WHEN 'X'
                     MOVE WSKD-VAL-STR-P(IX-AREA-SCHEDA, IX-TAB-VAR)
                TO WB04-VAL-STRINGA

                  WHEN 'A'
                  WHEN 'N'
                  WHEN 'I'
                     MOVE WSKD-VAL-IMP-P(IX-AREA-SCHEDA, IX-TAB-VAR)
                TO WB04-VAL-IMP

                  WHEN 'P'
                  WHEN 'M'
                     MOVE WSKD-VAL-PERC-P(IX-AREA-SCHEDA, IX-TAB-VAR)
                TO WB04-VAL-PC

              END-EVALUATE
           END-IF.

      *--> B04
           IF WK-STRADA-TAB
              PERFORM S1470-CALL-EOC-B04
                 THRU EX-S1470
           ELSE
              PERFORM CALL-EOC-B04-SEQ
                 THRU CALL-EOC-B04-SEQ-EX
           END-IF.
      *
       EX-S1410.
           EXIT.


      * ----------------------------------------------------------------
      * Chiamata al modulo di scrittura squenziale BILA_VAR_CALC_P
      * ----------------------------------------------------------------
       CALL-EOC-B04-SEQ.

           MOVE 'CALL-EOC-B04-SEQ'    TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.


           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'FILESQS4'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Scritt. BILA_VAR_CALC_P'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX


      *     MOVE 'BILA_VAR_CALC_P' TO WK-TABELLA.

      *     PERFORM ESTR-SEQUENCE  THRU ESTR-SEQUENCE-EX.

           IF IDSV0001-ESITO-OK
              PERFORM WRITE-FILEOUT-B04  THRU
                      WRITE-FILEOUT-B04-EX
           END-IF.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'FILESQS4'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Scritt. BILA_VAR_CALC_P'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.


       CALL-EOC-B04-SEQ-EX.
           EXIT.


      *----------------------------------------------------------------*
      *    SCRITTURA FILE SEQUENZIALE X CARICAMENTO TAB. BILA_VAR_CALC_P
      *----------------------------------------------------------------*
       WRITE-FILEOUT-B04.

           MOVE 'WRITE-FILEOUT-B04'
             TO  IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY
              THRU ESEGUI-DISPLAY-EX

           PERFORM S1495-VAL-DCLGEN-B04 THRU EX-S1495.

           INITIALIZE WK-VAR-REC-SIZE-F04 W-B04-LEN-TOT
           COMPUTE WK-VAR-REC-SIZE-F04 = LENGTH OF W-B04-REC-FISSO
                                       + W-B04-AREA-D-VALOR-VAR-LEN
                                       + 5

           MOVE WK-VAR-REC-SIZE-F04            TO W-B04-LEN-TOT

           MOVE AREA-OUT-W-B04                 TO FILESQS4-REC

           SET WRITE-FILESQS-OPER              TO TRUE
           SET WRITE-FILESQS4-SI               TO TRUE

           PERFORM CALL-FILESEQ              THRU CALL-FILESEQ-EX

           IF SEQUENTIAL-ESITO-KO
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'WRITE-FILEOUT-B04'       TO IEAI9901-LABEL-ERR
              MOVE '005166'                  TO IEAI9901-COD-ERRORE
              MOVE SPACES                    TO IEAI9901-PARAMETRI-ERR
              MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       WRITE-FILEOUT-B04-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    SCRITTURA FILE SEQUENZIALE X SCARTI
      *----------------------------------------------------------------*
       WRITE-FILEOUT-SCARTI.

           MOVE 'WRITE-FILEOUT-SCARTI'
             TO  IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY
              THRU ESEGUI-DISPLAY-EX
      *
      *
           INITIALIZE AREA-OUT-SCARTI
           MOVE ADE-ID-ADES        TO SCA-ADE-ID-ADES
           MOVE POL-ID-POLI        TO SCA-POL-ID-POLI
           MOVE POL-IB-OGG         TO SCA-POL-IB-OGG

           MOVE AREA-OUT-SCARTI    TO FILESQS5-REC

           SET WRITE-FILESQS-OPER  TO TRUE
           SET WRITE-FILESQS5-SI   TO TRUE

           PERFORM CALL-FILESEQ    THRU CALL-FILESEQ-EX

           IF SEQUENTIAL-ESITO-KO
              MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'WRITE-FILEOUT-SCARTI'  TO IEAI9901-LABEL-ERR
              MOVE '005166'                TO IEAI9901-COD-ERRORE
              MOVE SPACES                  TO IEAI9901-PARAMETRI-ERR
              MOVE SEQUENTIAL-DESC-ERRORE-ESTESA
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       WRITE-FILEOUT-SCARTI-EX.
           EXIT.

       S1495-VAL-DCLGEN-B04.

           INITIALIZE  AREA-OUT-W-B04
      *                FILESQS4-REC.

            MOVE ZERO
               TO W-B04-ID-BILA-VAR-CALC-P
           MOVE WB04-COD-COMP-ANIA
              TO W-B04-COD-COMP-ANIA
           MOVE WB04-ID-RICH-ESTRAZ-MAS
              TO W-B04-ID-RICH-ESTRAZ-MAS
           IF WB04-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B04-ID-RICH-ESTRAZ-AGG-IND
              MOVE HIGH-VALUE
              TO W-B04-ID-RICH-ESTRAZ-AGG-NULL
           ELSE
              MOVE X'F0'
              TO W-B04-ID-RICH-ESTRAZ-AGG-IND
              MOVE WB04-ID-RICH-ESTRAZ-AGG
              TO W-B04-ID-RICH-ESTRAZ-AGG
           END-IF
           MOVE WB04-DT-RIS   TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X     TO W-B04-DT-RIS
           MOVE WB04-ID-POLI
                              TO W-B04-ID-POLI
           MOVE WB04-ID-ADES
                              TO W-B04-ID-ADES
           MOVE WB04-PROG-SCHEDA-VALOR
                      TO W-B04-PROG-SCHEDA-VALOR
           MOVE WB04-TP-RGM-FISC
                              TO W-B04-TP-RGM-FISC
           MOVE WB04-DT-INI-VLDT-PROD  TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X    TO W-B04-DT-INI-VLDT-PROD

           MOVE WB04-COD-VAR
                              TO W-B04-COD-VAR

           MOVE WB04-TP-D
                              TO W-B04-TP-D

           IF WB04-VAL-IMP-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B04-VAL-IMP-IND
              MOVE HIGH-VALUE
              TO W-B04-VAL-IMP-NULL
           ELSE
              MOVE X'F0'
              TO W-B04-VAL-IMP-IND
              MOVE WB04-VAL-IMP
              TO W-B04-VAL-IMP
           END-IF

           IF WB04-VAL-PC-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B04-VAL-PC-IND
              MOVE HIGH-VALUE
              TO W-B04-VAL-PC-NULL
           ELSE
              MOVE X'F0'
              TO W-B04-VAL-PC-IND
              MOVE WB04-VAL-PC
              TO W-B04-VAL-PC
           END-IF

           IF WB04-VAL-STRINGA-NULL = HIGH-VALUES
              MOVE X'6F'
              TO W-B04-VAL-STRINGA-IND
              MOVE HIGH-VALUE
              TO W-B04-VAL-STRINGA-NULL
           ELSE
              MOVE X'F0'
              TO W-B04-VAL-STRINGA-IND
              MOVE WB04-VAL-STRINGA
              TO W-B04-VAL-STRINGA
           END-IF
           MOVE 'I'
              TO W-B04-DS-OPER-SQL
           IF WB04-DS-VER NOT NUMERIC
              MOVE 0 TO W-B04-DS-VER
           ELSE
              MOVE WB04-DS-VER
              TO W-B04-DS-VER
           END-IF
           MOVE IDSV0001-DATA-COMP-AGG-STOR
              TO W-B04-DS-TS-CPTZ
           MOVE IDSV0001-USER-NAME
              TO W-B04-DS-UTENTE
           MOVE '1'
              TO W-B04-DS-STATO-ELAB.

           IF WB04-AREA-D-VALOR-VAR = HIGH-VALUE
              MOVE X'6F'
                TO W-B04-AREA-D-VALOR-VAR-IND
           ELSE
              IF WK-STRINGATURA-SI
                 MOVE WB04-AREA-D-VALOR-VAR-LEN
                   TO W-B04-AREA-D-VALOR-VAR-LEN
                 MOVE WB04-AREA-D-VALOR-VAR
                   TO W-B04-AREA-D-VALOR-VAR
              ELSE
                 MOVE 1
                   TO W-B04-AREA-D-VALOR-VAR-LEN
                 MOVE WB04-AREA-D-VALOR-VAR
                   TO W-B04-AREA-D-VALOR-VAR
              END-IF
           END-IF.


       EX-S1495.
           EXIT.

      *----------------------------------------------------------------*
      * LETTURA DELLA TABELLE RAPPORTO RETE CON WC AD HOC              *
      *----------------------------------------------------------------*
      *
       S2800-LEGGI-RAPP-RETE.
      *

           MOVE 'S2800-LEGGI-RAPP-RETE'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           PERFORM S2810-IMPOSTA-RAPP-RETE
              THRU EX-S2810.


           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS4240'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura RAPP_RETE'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX


      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX
      *
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S2800-LEGGI-RAPP-RETE'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005056'
                         TO IEAI9901-COD-ERRORE
                       MOVE 'RAPPORTO RETE'
                         TO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI
                         TO RAPP-RETE

                  WHEN OTHER
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S2800-LEGGI-RAPP-RETE'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'RAPPORTO RETE'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

              END-EVALUATE
           ELSE
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S2800-LEGGI-RAPP-RETE'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'RAPPORTO RETE'  ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS4240'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura RAPP_RETE'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.



      *
       EX-S2800.
           EXIT.
      *----------------------------------------------------------------*
      * LETTURA DELLA TABELLE RAPPORTO RETE CON WC AD HOC              *
      *----------------------------------------------------------------*
       S2810-IMPOSTA-RAPP-RETE.
      *
PERFOR*    MOVE SPACES             TO IDSI0011-BUFFER-DATI
PERFOR*                               IDSI0011-BUFFER-WHERE-COND

           MOVE WPOL-ID-POLI       TO RRE-ID-OGG.
           MOVE 'PO'               TO RRE-TP-OGG.
           MOVE 'GE'               TO RRE-TP-RETE.

           MOVE WS-DT-EFFETTO      TO IDSI0011-DATA-INIZIO-EFFETTO
                                      IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF       TO IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-X-COMPETENZA       TO TRUE.
           MOVE 'LDBS4240'         TO IDSI0011-CODICE-STR-DATO.
           MOVE RAPP-RETE          TO IDSI0011-BUFFER-DATI.

           SET IDSI0011-SELECT          TO TRUE.
           SET IDSI0011-WHERE-CONDITION TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL  TO TRUE.
      *
       EX-S2810.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1600-OGGETTI-COLLEGATI.

           INITIALIZE OGG-COLLG

           SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.
           MOVE WS-DT-EFFETTO       TO IDSI0011-DATA-INIZIO-EFFETTO
                                       IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF        TO IDSI0011-DATA-COMPETENZA

           SET IDSI0011-SELECT                TO TRUE
           SET IDSI0011-ID-OGGETTO            TO TRUE
           SET IDSI0011-TRATT-X-COMPETENZA    TO TRUE

           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                                         TO LDBV0721-ID-OGG-COLLG
           MOVE 'TG'                     TO LDBV0721-TP-OGG-COLLG

           MOVE 'LDBS0720'               TO IDSI0011-CODICE-STR-DATO
           MOVE LDBV0721                 TO IDSI0011-BUFFER-WHERE-COND

           SET IDSI0011-WHERE-CONDITION  TO TRUE
           SET OCO-OK                    TO TRUE

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
             EVALUATE TRUE
                 WHEN IDSO0011-NOT-FOUND
                    SET OCO-KO TO TRUE

                 WHEN IDSO0011-SUCCESSFUL-SQL
                    MOVE IDSO0011-BUFFER-DATI
                      TO OGG-COLLG

                WHEN OTHER
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'LETTURA-OCO'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005016'
                    TO IEAI9901-COD-ERRORE
                  STRING 'OGGETTO COLLEGATO'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                    DELIMITED BY SIZE
                    INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300

             END-EVALUATE
           ELSE
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'LETTURA-OCO'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005016'
                    TO IEAI9901-COD-ERRORE
                  STRING 'OGGETTO COLLEGATO'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                    DELIMITED BY SIZE
                    INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
           END-IF.


       EX-S1600.
           EXIT.
      *       251 --> Produttore
      *----------------------------------------------------------------*
      * LETTURA DELLA TABELLE RAPPORTO RETE CON WC AD HOC              *
      *----------------------------------------------------------------*
      *
       S2801-LEGGI-RAPP-RETE-AC.
      *
           PERFORM S2811-IMPOSTA-RAPP-RETE-AC
              THRU EX-S2811.
      *
           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS4240'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett. tab. RAPP_RETE  '
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX
      *
           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS4240'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett. tab. RAPP_RETE  '
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING

           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S2801-LEGGI-RAPP-RETE-AC'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005056'
                         TO IEAI9901-COD-ERRORE
                       MOVE 'RAPPORTO RETE'
                         TO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI
                         TO RAPP-RETE

                  WHEN OTHER
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S2801-LEGGI-RAPP-RETE-AC'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'RAPPORTO RETE'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

              END-EVALUATE
           ELSE
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S2801-LEGGI-RAPP-RETE-AC'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'RAPPORTO RETE'  ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.
      *
       EX-S2801.
           EXIT.
      *----------------------------------------------------------------*
      * LETTURA DELLA TABELLE RAPPORTO RETE CON WC AD HOC              *
      *----------------------------------------------------------------*
       S2811-IMPOSTA-RAPP-RETE-AC.
      *
           INITIALIZE RAPP-RETE.
PERFOR*    MOVE SPACES             TO IDSI0011-BUFFER-DATI
PERFOR*                               IDSI0011-BUFFER-WHERE-COND

           MOVE WPOL-ID-POLI       TO RRE-ID-OGG.
           MOVE 'PO'               TO RRE-TP-OGG.
           MOVE 'AC'               TO RRE-TP-RETE.

           MOVE WS-DT-EFFETTO      TO IDSI0011-DATA-INIZIO-EFFETTO
                                      IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF       TO IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-X-COMPETENZA       TO TRUE.
           MOVE 'LDBS4240'         TO IDSI0011-CODICE-STR-DATO.
           MOVE RAPP-RETE          TO IDSI0011-BUFFER-DATI.

           SET IDSI0011-SELECT          TO TRUE.
           SET IDSI0011-WHERE-CONDITION TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL  TO TRUE.
      *
       EX-S2811.
           EXIT.
      *       251 --> Produttore FINE
      *----------------------------------------------------------------*
      *       261 -->  Costi di emissione
      *----------------------------------------------------------------*
       S2812-DIRITTI-EMIS-PERFEZ.

           INITIALIZE LDBV0371

           SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.
           MOVE WS-DT-EFFETTO       TO IDSI0011-DATA-INIZIO-EFFETTO
                                       IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF        TO IDSI0011-DATA-COMPETENZA

           SET IDSI0011-SELECT                TO TRUE

           SET IDSI0011-TRATT-X-COMPETENZA    TO TRUE

           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                                         TO LDBV0371-ID-OGG
           MOVE 'TG'                     TO LDBV0371-TP-OGG

           MOVE 'PE'                     TO LDBV0371-TP-PRE-TIT

22017      MOVE 'IN'                     TO LDBV0371-TP-STAT-TIT1
22017      MOVE 'EM'                     TO LDBV0371-TP-STAT-TIT2

           MOVE 'LDBS0370'               TO IDSI0011-CODICE-STR-DATO
PERFOR*    MOVE SPACES                   TO IDSI0011-BUFFER-DATI
           MOVE LDBV0371                 TO IDSI0011-BUFFER-DATI

           SET IDSI0011-WHERE-CONDITION  TO TRUE
           SET DIR-OK                    TO TRUE

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS0370'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett. diritti emiss.'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS0370'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett. diritti emiss.'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC
             EVALUATE TRUE
                 WHEN IDSO0011-NOT-FOUND
                    SET DIR-KO TO TRUE

                 WHEN IDSO0011-SUCCESSFUL-SQL
                    MOVE IDSO0011-BUFFER-DATI
                      TO LDBV0371

                WHEN OTHER
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S2812-DIRITTI-EMIS-PERFEZ'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005016'
                    TO IEAI9901-COD-ERRORE
                  STRING 'LDBS0370'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                    DELIMITED BY SIZE
                    INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300

             END-EVALUATE
           ELSE
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S2812-DIRITTI-EMIS-PERFEZ'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005016'
                    TO IEAI9901-COD-ERRORE
                  STRING 'LDBS0370'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                    DELIMITED BY SIZE
                    INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
           END-IF.


       EX-S2812.
           EXIT.
      *       261 -->  Costi di emissione FINE
      *-----------------------------------------------------------------
      *       262 -->  data esito titolo*
      *-----------------------------------------------------------------
       S2813-MAX-DATA-ESI-TIT.

           MOVE 'S2813-MAX-DATA-ESI-TIT'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> VALORIZZAZIONE DCLGEN
           INITIALIZE    DETT-TIT-CONT.

           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) TO DTC-ID-OGG
           MOVE 'TG'                            TO DTC-TP-OGG
           MOVE 'IN'
                                            TO DTC-TP-STAT-TIT

           MOVE WS-DT-EFFETTO       TO IDSI0011-DATA-INIZIO-EFFETTO
                                       IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF        TO IDSI0011-DATA-COMPETENZA
      *--> TRATTAMENTO-STORICITA
            SET IDSI0011-TRATT-X-COMPETENZA
             TO TRUE.
      *
      *  --> Nome tabella fisica db
           MOVE 'LDBS7300'               TO IDSI0011-CODICE-STR-DATO.
      *  --> Dclgen tabella
PERFOR*    MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE DETT-TIT-CONT            TO IDSI0011-BUFFER-DATI.
      *--> TIPO OPERAZIONE
           SET  IDSI0011-SELECT          TO TRUE.
           SET  IDSI0011-WHERE-CONDITION TO TRUE.
           SET  DTC2-KO                  TO TRUE.

      *  --> Modalita di accesso
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LDBS7300'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-INIZIO            TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'MAX-DATA-ESI-TIT     '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LDBS7300'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-FINE              TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'MAX-DATA-ESI-TIT     '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       CONTINUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                        MOVE IDSO0011-BUFFER-DATI
                        TO DETT-TIT-CONT
                        SET DTC2-OK TO TRUE
                  WHEN OTHER
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S2813-MAX-DATA-ESI-TIT'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'DETT TIT CONT (MAX);'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
           ELSE
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S2813-MAX-DATA-ESI-TIT'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'DETT TIT CONT (MAX);'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.

       EX-S2813.
           EXIT.
      *       262 -->  data esito titolo FINE

      *-----------------------------------------------------------------
      *    LETTURA DELLA TABELLA DETTAGLIO TITOLO CONTABILE
      *-----------------------------------------------------------------
       S1620-LEGGI-PREMIO.

           MOVE 'S1620-LEGGI-PREMIO'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> VALORIZZAZIONE DCLGEN
           INITIALIZE    DETT-TIT-CONT.

           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) TO DTC-ID-OGG
           MOVE 'TG'                            TO DTC-TP-OGG
           MOVE 'SE'                            TO DTC-TP-STAT-TIT

            MOVE WS-DT-EFFETTO       TO IDSI0011-DATA-INIZIO-EFFETTO
                                        IDSI0011-DATA-FINE-EFFETTO
            MOVE WS-DT-TS-PTF        TO IDSI0011-DATA-COMPETENZA
      *--> TRATTAMENTO-STORICITA
            SET IDSI0011-TRATT-X-COMPETENZA
             TO TRUE.
      *
      *  --> Nome tabella fisica db
           MOVE 'LDBS2660'               TO IDSI0011-CODICE-STR-DATO.
      *  --> Dclgen tabella
PERFOR*    MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE DETT-TIT-CONT            TO IDSI0011-BUFFER-DATI.
      *--> TIPO OPERAZIONE
           SET  IDSI0011-SELECT          TO TRUE.
           SET  IDSI0011-WHERE-CONDITION TO TRUE.

      *  --> Modalita di accesso
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS2660'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura DETT_TIT_CONT'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS2660'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura DETT_TIT_CONT'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
      *
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
ALFR                    SET WK-NOT-FOUND-PRM-TOT  TO TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                        MOVE IDSO0011-BUFFER-DATI
                        TO DETT-TIT-CONT
                        MOVE DTC-PRE-TOT-NULL
                          TO WS-PRE-TOT-NULL

                  WHEN OTHER
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1620-LEGGI-PREMIO'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'DETTAGLIO TITOLO CONTABILE'   ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
           ELSE
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1620-LEGGI-PREMIO'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'DETTAGLIO TITOLO CONTABILE'   ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.

       EX-S1620.
           EXIT.
      *-----------------------------------------------------------------
      *    LETTURA DELLA TABELLA DETTAGLIO TITOLO CONTABILE (SOMMA)
      *-----------------------------------------------------------------
       S1621-LEGGI-PREMIO-TOTALE.

           MOVE 'S1621-LEGGI-PREMIO-TOTALE'  TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO          TO TRUE
           MOVE WK-LABEL-ERR                 TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY            THRU ESEGUI-DISPLAY-EX.

ALEX  *    MOVE WLB-DT-RISERVA
ALEX  *      TO WK-DATA-APPO-N
           MOVE '01'
             TO WK-DATA-APPO-N-GG
           MOVE '01'
             TO WK-DATA-APPO-N-MM
ALEX       MOVE '1900'
ALEX         TO WK-DATA-APPO-N-AA

      *--> VALORIZZAZIONE DCLGEN
           INITIALIZE     LDBV4151.

           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) TO LDBV4151-ID-OGG
           MOVE 'TG'                            TO LDBV4151-TP-OGG
           MOVE 'SE'                            TO LDBV4151-TP-STAT-TIT
           MOVE WK-DATA-APPO             TO LDBV4151-DATA-INIZIO-PERIODO
      *    MOVE WLB-DT-RISERVA           TO LDBV4151-DATA-FINE-PERIODO

           MOVE WLB-DT-RISERVA          TO IDSI0011-DATA-INIZIO-EFFETTO
                                           IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF            TO IDSI0011-DATA-COMPETENZA.

      *--> TRATTAMENTO-STORICITA
           SET IDSI0011-TRATT-X-COMPETENZA          TO TRUE.

      *  --> Nome tabella fisica db
           MOVE 'LDBS4150'               TO IDSI0011-CODICE-STR-DATO.

      *  --> Dclgen tabella
PERFOR*    MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE LDBV4151                 TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET  IDSI0011-SELECT          TO TRUE.
           SET  IDSI0011-WHERE-CONDITION TO TRUE.

      *  --> Modalita di accesso
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS4150'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura DETT_TIT_CONT'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS4150'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura DETT_TIT_CONT'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       MOVE HIGH-VALUE
                         TO WS-PRE-TOT-NULL
                  WHEN IDSO0011-SUCCESSFUL-SQL
                        MOVE IDSO0011-BUFFER-DATI
                          TO LDBV4151
                        IF LDBV4151-PRE-TOT = ZEROES
                           MOVE HIGH-VALUE
                             TO WS-PRE-TOT-NULL
                        ELSE
                           MOVE LDBV4151-PRE-TOT
                             TO WS-PRE-TOT
                        END-IF
                  WHEN OTHER
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1621-LEGGI-PREMIO-TOTALE'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'DETT TIT CONTABILE;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

              END-EVALUATE
           ELSE
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1621-LEGGI-PREMIO-TOTALE'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'DETT TIT CONTABILE;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300

           END-IF.

       EX-S1621.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       S1625-PREP-P-RISCATTI.

           MOVE 'S1625-PREP-P-RISCATTI'     TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE.
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE LDBV2651.
           MOVE WPOL-TP-FRM-ASSVA           TO WS-TP-FRM-ASSVA.
           IF INDIVIDUALE
              MOVE WPOL-ID-POLI             TO LDBV2651-ID-OGG
              MOVE 'PO'                     TO LDBV2651-TP-OGG
      *       SET  LIQUI-RISPAR-POLIND      TO TRUE
              SET COMUN-RISPAR-IND          TO TRUE
              MOVE WS-MOVIMENTO             TO LDBV2651-TP-MOV1
10819         SET RPP-TAKE-PROFIT           TO TRUE
10819         MOVE WS-MOVIMENTO             TO LDBV2651-TP-MOV2
10819X        SET RPP-REDDITO-PROGRAMMATO   TO TRUE
10819X        MOVE WS-MOVIMENTO             TO LDBV2651-TP-MOV3
           ELSE
              MOVE WADE-ID-ADES(IX-TAB-ADE) TO LDBV2651-ID-OGG
              MOVE 'AD'                     TO LDBV2651-TP-OGG
      *       SET  LIQUI-RISPAR-ADE         TO TRUE
              SET COMUN-RISPAR-ADE          TO TRUE
              MOVE WS-MOVIMENTO             TO LDBV2651-TP-MOV1
           END-IF.

           PERFORM S1630-CALC-RISCATTI
              THRU EX-S1630.

       EX-S1625.
           EXIT.

      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       S1630-CALC-RISCATTI.

           MOVE 'S1630-CALC-RISCATTI'    TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO      TO TRUE.
           MOVE WK-LABEL-ERR             TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY        THRU ESEGUI-DISPLAY-EX.

      * CALCOLO DEL 01/01 DELL'ANNO IN CORSO
           MOVE ZERO                     TO WS-DT-INI-9.
           MOVE WS-DT-CALCOLO-NUM(1:4)   TO WS-DT-INI-AA.
           MOVE WS-DT-INI                TO WS-DT-INI-9.
           MOVE WS-DT-INI-9              TO LDBV2651-DT-INIZ.

      * CALCOLO 31/12 DELL'ANNO IN CORSO
           MOVE ZERO                     TO WS-DT-RST-9.
           MOVE WS-DT-CALCOLO-NUM(1:4)   TO WS-DT-RST-AA.
           MOVE WS-DT-RST                TO WS-DT-RST-9.
           MOVE WS-DT-RST-9              TO LDBV2651-DT-FINE.

           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
           SET IDSI0011-FETCH-FIRST      TO TRUE.

           SET FINE-MOVI-NO              TO TRUE.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LDBS2650'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-INIZIO            TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'LOOP CALC-RISCATTI   '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSV0001-ESITO-OK
                      OR NOT FINE-MOVI-NO

               MOVE 'LDBS2650'           TO IDSI0011-CODICE-STR-DATO
               MOVE LDBV2651             TO IDSI0011-BUFFER-WHERE-COND
PERFOR*        MOVE SPACES               TO IDSI0011-BUFFER-DATI
               MOVE MOVI                 TO IDSI0011-BUFFER-DATI

               SET IDSI0011-WHERE-CONDITION  TO TRUE
               SET IDSI0011-TRATT-SENZA-STOR TO TRUE

               SET IDSO0011-SUCCESSFUL-RC    TO TRUE
               SET IDSO0011-SUCCESSFUL-SQL   TO TRUE

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LDBS2650'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-INIZIO            TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'call pgrm LDBS2650   '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LDBS2650'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-FINE              TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'call pgrm LDBS2650   '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *--> CHIAVE NON TROVATA
                           SET FINE-MOVI  TO TRUE
                           SET RISCATT-OK TO TRUE
                      WHEN IDSO0011-SUCCESSFUL-SQL
                           MOVE IDSO0011-BUFFER-DATI TO MOVI
                           PERFORM S1631-LETTURA-LIQ
                              THRU EX-S1631
                           SET IDSI0011-FETCH-NEXT   TO TRUE
                      WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                           SET RISCATT-KO TO TRUE
                           MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'S1630-CALC-RISCATTI'
                                          TO IEAI9901-LABEL-ERR
                           MOVE '005016'  TO IEAI9901-COD-ERRORE
                           STRING 'LDBS2650;'
                                  IDSO0011-RETURN-CODE ';'
                                  IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      *--> ERRORE DISPATCHER
                  SET RISCATT-KO          TO TRUE
                  MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S1630-CALC-RISCATTI'
                                          TO IEAI9901-LABEL-ERR
                  MOVE '005016'           TO IEAI9901-COD-ERRORE
                  STRING 'LDBS2650;'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                      DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LDBS2650'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-FINE              TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'LOOP CALC-RISCATTI   '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S1630.
           EXIT.

      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       S1631-LETTURA-LIQ.

           MOVE 'S1631-LETTURA-LIQ'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO      TO TRUE.
           MOVE WK-LABEL-ERR             TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY        THRU ESEGUI-DISPLAY-EX.

      *--> VALORIZZAZIONE DCLGEN
           INITIALIZE LIQ.

           MOVE MOV-ID-OGG               TO LQU-ID-OGG.
           MOVE 'AD'                     TO LQU-TP-OGG.
           MOVE MOV-ID-MOVI              TO LQU-ID-MOVI-CRZ.
      * sir 21521 si accede per competenza e con data del movimento per
      *     leggere la LIQ creata dal movimento, anche se storicizzata.
           MOVE MOV-DT-EFF               TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO.
           MOVE MOV-DS-TS-CPTZ           TO IDSI0011-DATA-COMPETENZA.

      *--> TRATTAMENTO-STORICITA
           SET IDSI0011-TRATT-X-COMPETENZA TO TRUE.

      *--> Nome tabella fisica db
           MOVE 'LDBS6540'               TO IDSI0011-CODICE-STR-DATO.

      *--> Dclgen tabella
PERFOR*    MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE LIQ                      TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT           TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.

           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LDBS6540'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-INIZIO            TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'LETTURA-LIQ          '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LDBS6540'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-FINE              TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'LETTURA-LIQ          '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       SET RISCATT-OK            TO TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI TO LIQ
                       SET RISCATT-OK            TO TRUE
      * SIR 21219 PRENDO SOLO LE LIQUIDAZIONI DA RISCATTO PARZIALE
                       IF LQU-TP-LIQ = 'RP'
      * sir 21521 si legge la LIQ creata ma con lo stato attivo ad oggi.
                          PERFORM S1635-LEGGI-LIQ-ATTIVA
                             THRU EX-S1635
                          IF IDSV0001-ESITO-OK
                             ADD LQU-TOT-IMP-LRD-LIQTO
                              TO WS-SOMMA-IMPO-LIQ
                          END-IF
                       END-IF
                  WHEN OTHER
                       SET RISCATT-KO            TO TRUE
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1631-LETTURA-LIQ'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'LIQ;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
              SET RISCATT-KO            TO TRUE
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1631-LETTURA-LIQ'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'LIQ;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1631.
           EXIT.

      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       S1635-LEGGI-LIQ-ATTIVA.

           MOVE 'S1635-LEGGI-LIQ-ATTIVA' TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO      TO TRUE.
           MOVE WK-LABEL-ERR             TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY        THRU ESEGUI-DISPLAY-EX.

      *--> VALORIZZAZIONE DCLGEN
           MOVE LQU-ID-LIQ               TO WK-LQU-ID-LIQ.
PERFOR*    INITIALIZE IDSI0011-BUFFER-DATI
BUFFER     INITIALIZE LIQ

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT           TO TRUE.
           SET IDSI0011-ID               TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.
      *
           MOVE IDSV0001-COD-COMPAGNIA-ANIA TO LQU-COD-COMP-ANIA.
           MOVE WK-LQU-ID-LIQ            TO LQU-ID-LIQ.
           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                     TO IDSI0011-DATA-COMPETENZA.

      *--> TRATTAMENTO-STORICITA
           SET IDSI0011-TRATT-X-COMPETENZA TO TRUE.

           MOVE 'LIQ'                    TO IDSI0011-CODICE-STR-DATO
           MOVE LIQ                      TO IDSI0011-BUFFER-DATI
           MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.

      *--> Dclgen tabella

      *    SET IDSI0011-WHERE-CONDITION  TO TRUE.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       SET RISCATT-OK            TO TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI TO LIQ
                       SET RISCATT-OK            TO TRUE
                       IF LQU-TOT-IMP-LRD-LIQTO-NULL = HIGH-VALUES
                          MOVE ZERO TO LQU-TOT-IMP-LRD-LIQTO
                       END-IF
                  WHEN OTHER
                       SET RISCATT-KO            TO TRUE
                       MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR TO IEAI9901-LABEL-ERR
                       MOVE '005016'     TO IEAI9901-COD-ERRORE
                       MOVE SPACES       TO IEAI9901-PARAMETRI-ERR
                       STRING 'LIQ;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
              SET RISCATT-KO            TO TRUE
              MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR TO IEAI9901-LABEL-ERR
              MOVE '005016'     TO IEAI9901-COD-ERRORE
              MOVE SPACES       TO IEAI9901-PARAMETRI-ERR
              STRING 'LIQ;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1635.
           EXIT.

      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       S1640-LEGGI-ULT-TIT.

           MOVE 'S1640-LEGGI-ULT-TIT'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> VALORIZZAZIONE DCLGEN
           INITIALIZE    DETT-TIT-CONT.

           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) TO DTC-ID-OGG
           MOVE 'TG'                            TO DTC-TP-OGG
           MOVE 'IN'                            TO DTC-TP-STAT-TIT

            MOVE WS-DT-EFFETTO       TO IDSI0011-DATA-INIZIO-EFFETTO
                                        IDSI0011-DATA-FINE-EFFETTO
            MOVE WS-DT-TS-PTF        TO IDSI0011-DATA-COMPETENZA
      *--> TRATTAMENTO-STORICITA
            SET IDSI0011-TRATT-X-COMPETENZA
             TO TRUE.
      *
      *  --> Nome tabella fisica db
           MOVE 'LDBS2620'               TO IDSI0011-CODICE-STR-DATO.
      *  --> Dclgen tabella
PERFOR*    MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE DETT-TIT-CONT            TO IDSI0011-BUFFER-DATI.
      *--> TIPO OPERAZIONE
           SET  IDSI0011-SELECT          TO TRUE.
           SET  IDSI0011-WHERE-CONDITION TO TRUE.
           SET  DTC1-KO                  TO TRUE.

      *  --> Modalita di accesso
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LDBS2620'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-INIZIO            TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'LEGGI-ULT-TIT        '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LDBS2620'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-FINE              TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'LEGGI-ULT-TIT        '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       CONTINUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                        MOVE IDSO0011-BUFFER-DATI
                        TO DETT-TIT-CONT
                        SET DTC1-OK TO TRUE
                  WHEN OTHER
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1640-LEGGI-ULT-TIT'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'DETT TIT CONTABILE;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
           ELSE
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1640-LEGGI-ULT-TIT'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'DETT TIT CONTABILE;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.

       EX-S1640.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       S1650-LEGGI-DT-RIDUZIONE.

           MOVE 'S1650-LEGGI-DT-RIDUZIONE'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.


           INITIALIZE LDBV2631.
           MOVE WPOL-TP-FRM-ASSVA         TO WS-TP-FRM-ASSVA
           IF INDIVIDUALE
              MOVE WPOL-ID-POLI             TO LDBV2631-ID-OGG
              MOVE 'PO'                     TO LDBV2631-TP-OGG
           ELSE
              MOVE WADE-ID-ADES(IX-TAB-ADE) TO LDBV2631-ID-OGG
              MOVE 'AD'                     TO LDBV2631-TP-OGG
           END-IF.

           SET VARIA-RIDUZI                 TO TRUE.
           MOVE WS-MOVIMENTO                TO LDBV2631-TP-MOV1

           MOVE WS-DT-EFFETTO        TO IDSI0011-DATA-INIZIO-EFFETTO
                                         IDSI0011-DATA-FINE-EFFETTO.
           MOVE WS-DT-TS-PTF          TO IDSI0011-DATA-COMPETENZA.
           MOVE 'LDBS2630'            TO IDSI0011-CODICE-STR-DATO.
           MOVE LDBV2631              TO IDSI0011-BUFFER-WHERE-COND.
PERFOR*    MOVE SPACES                TO IDSI0011-BUFFER-DATI.
           SET IDSI0011-WHERE-CONDITION
            TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC
            TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
            TO TRUE.
           SET IDSI0011-TRATT-X-COMPETENZA
            TO TRUE.
           SET IDSI0011-SELECT
            TO TRUE.

           SET  STB-OK                  TO TRUE.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LDBS2630'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-INIZIO            TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'LEGGI-DT-RIDUZIONE   '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'LDBS2630'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-FINE              TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'LEGGI-DT-RIDUZIONE   '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                        SET STB-KO TO TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                        MOVE IDSO0011-BUFFER-DATI
                          TO STAT-OGG-BUS
                  WHEN OTHER
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1650-LEGGI-DT-RIDUZIONE'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'STATO OGG BUS;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
           ELSE
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1650-LEGGI-DT-RIDUZIONE'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'STATO OGG BUS;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.

       EX-S1650.
           EXIT.
      *-----------------------------------------------------------------
      *    LETTURA PARAMETRO DI CALCOLO
      *-----------------------------------------------------------------
       S1660-CALL-LDBS1650.

           MOVE 'S1660-CALL-LDBS1650'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
             TO PCA-ID-OGG.
      *
           MOVE 'TG'
             TO PCA-TP-OGG.
      *
           MOVE WS-DT-EFFETTO       TO IDSI0011-DATA-INIZIO-EFFETTO
                                       IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF        TO IDSI0011-DATA-COMPETENZA
           MOVE 'PARAM-DI-CALC'       TO WK-TABELLA.
           MOVE 'LDBS1650'            TO IDSI0011-CODICE-STR-DATO.
           MOVE PARAM-DI-CALC         TO IDSI0011-BUFFER-DATI.
           MOVE SPACES                TO IDSI0011-BUFFER-WHERE-COND.

           SET IDSI0011-WHERE-CONDITION
            TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC
            TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
            TO TRUE.
           SET IDSI0011-TRATT-X-COMPETENZA
            TO TRUE.
           SET IDSI0011-SELECT
            TO TRUE.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS1650'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura PARAM_DI_CALC'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS1650'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura PARAM_DI_CALC'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       EX-S1660.
           EXIT.


      *-----------------------------------------------------------------
      *    PREPARA DATI PER LETTURA DALLA TAB. TRANCHE
      *    ESTRAE IMMAGINE PRECEDENTE ALLA DATA DI RISERVA
      *-----------------------------------------------------------------
       S1670-PREP-LET-TGA.

           MOVE 'S1670-PREP-LET-TGA'        TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE.
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY           THRU ESEGUI-DISPLAY-EX.

           INITIALIZE                       IO-A2K-LCCC0003
                                            IN-RCODE.
           MOVE '03'                        TO A2K-FUNZ.
           MOVE '03'                        TO A2K-INFO.
           MOVE 1                           TO A2K-DELTA.
           MOVE ' '                         TO A2K-TDELTA.
           MOVE '0'                         TO A2K-FISLAV.
           MOVE 0                           TO A2K-INICON.

           MOVE WLB-DT-RISERVA              TO A2K-INDATA.

           PERFORM A9200-CALL-AREA-CALCOLA-DATA
              THRU EX-A9200.

           IF IDSV0001-ESITO-OK
              INITIALIZE        TRCH-DI-GAR
              MOVE A2K-OUAMG
                TO WS-DATA-RISERVA-COMP
              MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                TO TGA-ID-TRCH-DI-GAR
              MOVE WS-DATA-RISERVA-COMP
                TO IDSI0011-DATA-INIZIO-EFFETTO
                   IDSI0011-DATA-FINE-EFFETTO
              MOVE WS-DT-TS-PTF
                TO IDSI0011-DATA-COMPETENZA

              SET  IDSI0011-TRATT-DEFAULT   TO TRUE
              SET  IDSI0011-ID              TO TRUE
              SET  IDSI0011-SELECT          TO TRUE

              MOVE 'TRCH-DI-GAR'
                TO IDSI0011-CODICE-STR-DATO
              MOVE TRCH-DI-GAR
                TO IDSI0011-BUFFER-DATI
           END-IF.

       EX-S1670.
           EXIT.
      *-----------------------------------------------------------------
      *    LETTURA DALLA TAB. TRANCHE
      *    ESTRAE IMMAGINE PRECEDENTE ALLA DATA DI RISERVA
      *-----------------------------------------------------------------
       S1680-LETTURA-TGA.
           MOVE 'S1680-LETTURA-TGA'         TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE.
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'IDBSTGA0'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-INIZIO            TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'IMM PREC A DT RISERVA'
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'IDBSTGA0'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-FINE              TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'IMM PREC A DT RISERVA'
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR
                         TO IEAI9901-LABEL-ERR
                       MOVE '005056'
                         TO IEAI9901-COD-ERRORE
                       STRING 'TRANCHE DI GAR.;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

                  WHEN IDSO0011-SUCCESSFUL-SQL
                        MOVE IDSO0011-BUFFER-DATI
                          TO TRCH-DI-GAR

                        IF TGA-PRSTZ-ULT-NULL = HIGH-VALUES
                           MOVE TGA-PRSTZ-ULT-NULL
                             TO WB03-PRSTZ-T-NULL
                        ELSE
                           MOVE TGA-PRSTZ-ULT
                             TO WB03-PRSTZ-T
                        END-IF

                  WHEN OTHER
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'TRANCHE DI GAR;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
           ELSE
                MOVE WK-PGM
                  TO IEAI9901-COD-SERVIZIO-BE
                MOVE WK-LABEL-ERR
                  TO IEAI9901-LABEL-ERR
                MOVE '005016'
                  TO IEAI9901-COD-ERRORE
                STRING 'TRANCHE DI GAR;'
                       IDSO0011-RETURN-CODE ';'
                       IDSO0011-SQLCODE
                       DELIMITED BY SIZE
                       INTO IEAI9901-PARAMETRI-ERR
                END-STRING
                PERFORM S0300-RICERCA-GRAVITA-ERRORE
                   THRU EX-S0300
           END-IF.

       EX-S1680.
           EXIT.



      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       S9100-DELETE-VAR-T.

           MOVE 'S9100-DELETE-VAR-T'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE WLB-ID-RICH-COLL         TO B05-ID-RICH-ESTRAZ-MAS
           MOVE WPOL-ID-POLI             TO B05-ID-POLI
           MOVE WADE-ID-ADES(IX-TAB-ADE) TO B05-ID-ADES

      *--> NOME TABELLA FISICA DB
           MOVE 'LDBS2720'        TO IDSI0011-CODICE-STR-DATO.
      *--> DCLGEN TABELLA
PERFOR*    MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE BILA-VAR-CALC-T          TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-DELETE              TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR    TO TRUE.
           SET IDSI0011-WHERE-CONDITION     TO TRUE.
      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL      TO TRUE.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS2720'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Delete BILA_VAR_CALC_T'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER         THRU CALL-DISPATCHER-EX

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS2720'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Delete BILA_VAR_CALC_T'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *--> CHIAVE NON TROVATA
                     CONTINUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                     CONTINUE
                  WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S9100-DELETE-VAR-T'
                                    TO IEAI9901-LABEL-ERR
                     MOVE '005016'  TO IEAI9901-COD-ERRORE
                     STRING 'BILA-VAR-CALC-T;'
                            IDSO0011-RETURN-CODE ';'
                            IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
                  END-EVALUATE
               ELSE
      *--> ERRORE DISPATCHER

                  MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S9100-DELETE-VAR-T'
                                          TO IEAI9901-LABEL-ERR
                  MOVE '005016'           TO IEAI9901-COD-ERRORE
                  STRING 'BILA-VAR-CALC-T;'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                      DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF.

       EX-S9100.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       S9200-DELETE-VAR-P.

           MOVE 'S9200-DELETE-VAR-P'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE WLB-ID-RICH-COLL         TO B04-ID-RICH-ESTRAZ-MAS
           MOVE WPOL-ID-POLI             TO B04-ID-POLI
           MOVE WADE-ID-ADES(IX-TAB-ADE) TO B04-ID-ADES

      *--> NOME TABELLA FISICA DB
           MOVE 'LDBS2740'               TO IDSI0011-CODICE-STR-DATO.
      *--> DCLGEN TABELLA
PERFOR*    MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE BILA-VAR-CALC-P          TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-DELETE              TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR    TO TRUE.
           SET IDSI0011-WHERE-CONDITION     TO TRUE.
      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL      TO TRUE.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS2740'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Delete BILA_VAR_CALC_P'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER         THRU CALL-DISPATCHER-EX

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS2740'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Delete BILA_VAR_CALC_P'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *--> CHIAVE NON TROVATA
                     CONTINUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                     CONTINUE
                  WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S9200-DELETE-VAR-P'
                                    TO IEAI9901-LABEL-ERR
                     MOVE '005016'  TO IEAI9901-COD-ERRORE
                     STRING 'BILA-VAR-CALC-P;'
                            IDSO0011-RETURN-CODE ';'
                            IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
                  END-EVALUATE
               ELSE
      *--> ERRORE DISPATCHER

                  MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S9200-DELETE-VAR-P'
                                          TO IEAI9901-LABEL-ERR
                  MOVE '005016'           TO IEAI9901-COD-ERRORE
                  STRING 'BILA-VAR-CALC-P;'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                      DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF.

       EX-S9200.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       S9300-READ-TRANCHE-ESTR.

           MOVE 'S9300-READ-TRANCHE-ESTR'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE WLB-ID-RICH-COLL         TO B03-ID-RICH-ESTRAZ-MAS
           MOVE WPOL-ID-POLI             TO B03-ID-POLI
           MOVE WADE-ID-ADES(IX-TAB-ADE) TO B03-ID-ADES
           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                                         TO B03-ID-TRCH-DI-GAR

      *--> NOME TABELLA FISICA DB
           MOVE 'LDBS2730'               TO IDSI0011-CODICE-STR-DATO.
      *--> DCLGEN TABELLA
PERFOR*    MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE BILA-TRCH-ESTR           TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT              TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR    TO TRUE.
           SET IDSI0011-WHERE-CONDITION     TO TRUE.
      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL      TO TRUE.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS2730'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett.tab.BILA_TRCH_ESTR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM CALL-DISPATCHER         THRU CALL-DISPATCHER-EX

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS2730'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lett.tab.BILA_TRCH_ESTR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *--> CHIAVE NON TROVATA
                       SET TRANCHE-NO-TROVATA TO TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI
                         TO BILA-TRCH-ESTR
                       SET TRANCHE-TROVATA    TO TRUE
                  WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S9300-READ-TRANCHE-ESTR'
                                    TO IEAI9901-LABEL-ERR
                     MOVE '005016'  TO IEAI9901-COD-ERRORE
                     STRING 'BILA-TRCH-ESTR;'
                            IDSO0011-RETURN-CODE ';'
                            IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
                  END-EVALUATE
               ELSE
      *--> ERRORE DISPATCHER

                  MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S9300-READ-TRANCHE-ESTR'
                                          TO IEAI9901-LABEL-ERR
                  MOVE '005016'           TO IEAI9901-COD-ERRORE
                  STRING 'BILA-TRCH-ESTR;'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                      DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF.

       EX-S9300.
           EXIT.
      *-----------------------------------------------------------------
      *  UPDATE TRANCHE LEGATE ALL'ADESIONE
      *-----------------------------------------------------------------
       S9400-UPD-STATI-TRCH-2770.

           MOVE 'S9400-UPD-STATI-TRCH-2770'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE BILA-TRCH-ESTR.

           MOVE WLB-ID-RICH-COLL         TO B03-ID-RICH-ESTRAZ-MAS
           MOVE WPOL-ID-POLI             TO B03-ID-POLI
           MOVE WADE-ID-ADES(IX-TAB-ADE) TO B03-ID-ADES
           MOVE WS-TP-STAT-BUS-ADE       TO B03-TP-STAT-BUS-TRCH
           MOVE WS-TP-CAUS-ADE           TO B03-TP-CAUS-TRCH
           MOVE WS-TP-STAT-BUS-ADE       TO B03-TP-STAT-BUS-ADES
           MOVE WS-TP-CAUS-ADE           TO B03-TP-CAUS-ADES
           MOVE WS-TP-STAT-BUS-POL       TO B03-TP-STAT-BUS-POLI
           MOVE WS-TP-CAUS-POL           TO B03-TP-CAUS-POLI
           MOVE WLB-ID-RICH              TO B03-ID-RICH-ESTRAZ-AGG

      *--> NOME TABELLA FISICA DB
           MOVE 'LDBS2770'               TO IDSI0011-CODICE-STR-DATO.
      *--> DCLGEN TABELLA
PERFOR*    MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE BILA-TRCH-ESTR           TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-UPDATE              TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR    TO TRUE.
           SET IDSI0011-WHERE-CONDITION     TO TRUE.
      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL      TO TRUE.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS2770'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Update BILA_TRCH_ESTR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER         THRU CALL-DISPATCHER-EX

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS2770'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Update BILA_TRCH_ESTR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                WHEN IDSO0011-NOT-FOUND
      *-->      CHIAVE NON TROVATA
                   CONTINUE
                WHEN IDSO0011-SUCCESSFUL-SQL
      *-->      OPERAZIONE ESEGUITA CORRETTAMENTE
                   CONTINUE
                WHEN OTHER
      *-->      ERRORE DI ACCESSO AL DB
                   MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                   MOVE 'S9400-LEGGI-ADE-B03'
                                  TO IEAI9901-LABEL-ERR
                   MOVE '005016'  TO IEAI9901-COD-ERRORE
                   STRING 'BILA-TRCH-ESTR (UPDATE);'
                          IDSO0011-RETURN-CODE ';'
                          IDSO0011-SQLCODE
                   DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                   END-STRING
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> ERRORE DISPATCHER
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S9400-LEGGI-ADE-B03'
                                      TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              STRING 'BILA-TRCH-ESTR (UPDATE);'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S9400.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       S9600-UPD-STATI-TRCH-2780.

           MOVE 'S9600-UPD-STATI-TRCH-2780'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE BILA-TRCH-ESTR.

           MOVE WLB-ID-RICH-COLL         TO B03-ID-RICH-ESTRAZ-MAS
           MOVE WPOL-ID-POLI             TO B03-ID-POLI
           MOVE WADE-ID-ADES(IX-TAB-ADE) TO B03-ID-ADES
           MOVE L3421-ID-TRCH-DI-GAR     TO B03-ID-TRCH-DI-GAR

           MOVE L3421-TP-STAT-BUS        TO B03-TP-STAT-BUS-TRCH
           MOVE L3421-TP-CAUS            TO B03-TP-CAUS-TRCH
           MOVE WS-TP-STAT-BUS-ADE       TO B03-TP-STAT-BUS-ADES
           MOVE WS-TP-CAUS-ADE           TO B03-TP-CAUS-ADES
           MOVE WS-TP-STAT-BUS-POL       TO B03-TP-STAT-BUS-POLI
           MOVE WS-TP-CAUS-POL           TO B03-TP-CAUS-POLI

           MOVE WLB-ID-RICH              TO B03-ID-RICH-ESTRAZ-AGG

      *--> NOME TABELLA FISICA DB
           MOVE 'LDBS2780'               TO IDSI0011-CODICE-STR-DATO.
      *--> DCLGEN TABELLA
PERFOR*    MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE BILA-TRCH-ESTR           TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-UPDATE              TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR    TO TRUE.
           SET IDSI0011-WHERE-CONDITION     TO TRUE.
      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL      TO TRUE.

           PERFORM CALL-DISPATCHER         THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                WHEN IDSO0011-NOT-FOUND
      *-->      CHIAVE NON TROVATA
                   CONTINUE
                WHEN IDSO0011-SUCCESSFUL-SQL
      *-->      OPERAZIONE ESEGUITA CORRETTAMENTE
                   CONTINUE
                WHEN OTHER
      *-->      ERRORE DI ACCESSO AL DB
                   MOVE WK-PGM
                     TO IEAI9901-COD-SERVIZIO-BE
                   MOVE 'S9600-UPD-STATI-TRCH-2780'
                     TO IEAI9901-LABEL-ERR
                   MOVE '005016'
                     TO IEAI9901-COD-ERRORE
                   STRING 'BILA-TRCH-ESTR (UPDATE);'
                          IDSO0011-RETURN-CODE ';'
                          IDSO0011-SQLCODE
                   DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                   END-STRING
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S9600-UPD-STATI-TRCH-2780'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'BILA-TRCH-ESTR (UPDATE);'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S9600.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       CTRL-NUM-TRCH-ATT.

           MOVE 'CTRL-NUM-TRCH-ATT'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *    SIR FCTVI00006121
      *    NEL CASO IN CUI NON SIANO PRESENTI TRANCHE ATTIVE PER
      *    L'ADESIONE CHE STIAMO ELABORANDO, QUEST'ULTIMA DEV'ESSERE
      *    SCARTATA DAL FLUSSO DI ELABORAZIONE E DEVE ESSERE GENERATO
      *    UN MESSAGGIO DI WARNING

           IF  WTGA-ELE-TGA-MAX NOT GREATER ZEROES
                SET WS-FL-SCARTA-ADE-SI TO TRUE
                MOVE WK-PGM
                  TO IEAI9901-COD-SERVIZIO-BE
                MOVE 'S0125-TRATTA-DATI-TGA'
                  TO IEAI9901-LABEL-ERR
                MOVE '005247'
                  TO IEAI9901-COD-ERRORE
                MOVE SPACES
                  TO IEAI9901-PARAMETRI-ERR
                STRING 'ADESIONE SCARTATA - NON ESISTONO '
                       'TRANCHE ATTIVE '
                DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                END-STRING
                PERFORM S0300-RICERCA-GRAVITA-ERRORE
                   THRU EX-S0300
           END-IF.

       EX-CTRL-NUM-TRCH-ATT.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       VALORIZZA-TGA.

           MOVE 'VALORIZZA-TGA'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE L3421-ID-TRCH-DI-GAR
             TO WTGA-ID-PTF(IX-TAB-TGA)
           MOVE L3421-ID-TRCH-DI-GAR
             TO WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
           MOVE L3421-ID-GAR
             TO WTGA-ID-GAR(IX-TAB-TGA)
           MOVE L3421-ID-ADES
             TO WTGA-ID-ADES(IX-TAB-TGA)
           MOVE L3421-ID-POLI
             TO WTGA-ID-POLI(IX-TAB-TGA)
           MOVE L3421-ID-MOVI-CRZ
             TO WTGA-ID-MOVI-CRZ(IX-TAB-TGA)
           IF L3421-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE L3421-ID-MOVI-CHIU-NULL
                TO WTGA-ID-MOVI-CHIU-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-ID-MOVI-CHIU
                TO WTGA-ID-MOVI-CHIU(IX-TAB-TGA)
           END-IF
           MOVE L3421-DT-INI-EFF
             TO WTGA-DT-INI-EFF(IX-TAB-TGA)
           MOVE L3421-DT-END-EFF
             TO WTGA-DT-END-EFF(IX-TAB-TGA)
           MOVE L3421-COD-COMP-ANIA
             TO WTGA-COD-COMP-ANIA(IX-TAB-TGA)
           MOVE L3421-DT-DECOR
             TO WTGA-DT-DECOR(IX-TAB-TGA)
           IF L3421-DT-SCAD-NULL = HIGH-VALUES
              MOVE L3421-DT-SCAD-NULL
                TO WTGA-DT-SCAD-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-DT-SCAD
                TO WTGA-DT-SCAD(IX-TAB-TGA)
           END-IF
           IF L3421-IB-OGG-NULL = HIGH-VALUES
              MOVE L3421-IB-OGG-NULL
                TO WTGA-IB-OGG-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IB-OGG
                TO WTGA-IB-OGG(IX-TAB-TGA)
           END-IF
           MOVE L3421-TP-RGM-FISC
             TO WTGA-TP-RGM-FISC(IX-TAB-TGA)
           IF L3421-DT-EMIS-NULL = HIGH-VALUES
              MOVE L3421-DT-EMIS-NULL
                TO WTGA-DT-EMIS-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-DT-EMIS
                TO WTGA-DT-EMIS(IX-TAB-TGA)
           END-IF
           MOVE L3421-TP-TRCH
             TO WTGA-TP-TRCH(IX-TAB-TGA)
           IF L3421-DUR-AA-NULL = HIGH-VALUES
              MOVE L3421-DUR-AA-NULL
                TO WTGA-DUR-AA-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-DUR-AA
                TO WTGA-DUR-AA(IX-TAB-TGA)
           END-IF
           IF L3421-DUR-MM-NULL = HIGH-VALUES
              MOVE L3421-DUR-MM-NULL
                TO WTGA-DUR-MM-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-DUR-MM
                TO WTGA-DUR-MM(IX-TAB-TGA)
           END-IF
           IF L3421-DUR-GG-NULL = HIGH-VALUES
              MOVE L3421-DUR-GG-NULL
                TO WTGA-DUR-GG-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-DUR-GG
                TO WTGA-DUR-GG(IX-TAB-TGA)
           END-IF
           IF L3421-PRE-CASO-MOR-NULL = HIGH-VALUES
              MOVE L3421-PRE-CASO-MOR-NULL
                TO WTGA-PRE-CASO-MOR-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRE-CASO-MOR
                TO WTGA-PRE-CASO-MOR(IX-TAB-TGA)
           END-IF
           IF L3421-PC-INTR-RIAT-NULL = HIGH-VALUES
              MOVE L3421-PC-INTR-RIAT-NULL
                TO WTGA-PC-INTR-RIAT-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PC-INTR-RIAT
                TO WTGA-PC-INTR-RIAT(IX-TAB-TGA)
           END-IF
           IF L3421-IMP-BNS-ANTIC-NULL = HIGH-VALUES
              MOVE L3421-IMP-BNS-ANTIC-NULL
                TO WTGA-IMP-BNS-ANTIC-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMP-BNS-ANTIC
                TO WTGA-IMP-BNS-ANTIC(IX-TAB-TGA)
           END-IF
           IF L3421-PRE-INI-NET-NULL = HIGH-VALUES
              MOVE L3421-PRE-INI-NET-NULL
                TO WTGA-PRE-INI-NET-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRE-INI-NET
                TO WTGA-PRE-INI-NET(IX-TAB-TGA)
           END-IF
           IF L3421-PRE-PP-INI-NULL = HIGH-VALUES
              MOVE L3421-PRE-PP-INI-NULL
                TO WTGA-PRE-PP-INI-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRE-PP-INI
                TO WTGA-PRE-PP-INI(IX-TAB-TGA)
           END-IF
           IF L3421-PRE-PP-ULT-NULL = HIGH-VALUES
              MOVE L3421-PRE-PP-ULT-NULL
                TO WTGA-PRE-PP-ULT-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRE-PP-ULT
                TO WTGA-PRE-PP-ULT(IX-TAB-TGA)
           END-IF
           IF L3421-PRE-TARI-INI-NULL = HIGH-VALUES
              MOVE L3421-PRE-TARI-INI-NULL
                TO WTGA-PRE-TARI-INI-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRE-TARI-INI
                TO WTGA-PRE-TARI-INI(IX-TAB-TGA)
           END-IF
           IF L3421-PRE-TARI-ULT-NULL = HIGH-VALUES
              MOVE L3421-PRE-TARI-ULT-NULL
                TO WTGA-PRE-TARI-ULT-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRE-TARI-ULT
                TO WTGA-PRE-TARI-ULT(IX-TAB-TGA)
           END-IF
           IF L3421-PRE-INVRIO-INI-NULL = HIGH-VALUES
              MOVE L3421-PRE-INVRIO-INI-NULL
                TO WTGA-PRE-INVRIO-INI-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRE-INVRIO-INI
                TO WTGA-PRE-INVRIO-INI(IX-TAB-TGA)
           END-IF
           IF L3421-PRE-INVRIO-ULT-NULL = HIGH-VALUES
              MOVE L3421-PRE-INVRIO-ULT-NULL
                TO WTGA-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRE-INVRIO-ULT
                TO WTGA-PRE-INVRIO-ULT(IX-TAB-TGA)
           END-IF
           IF L3421-PRE-RIVTO-NULL = HIGH-VALUES
              MOVE L3421-PRE-RIVTO-NULL
                TO WTGA-PRE-RIVTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRE-RIVTO
                TO WTGA-PRE-RIVTO(IX-TAB-TGA)
           END-IF
           IF L3421-IMP-SOPR-PROF-NULL = HIGH-VALUES
              MOVE L3421-IMP-SOPR-PROF-NULL
                TO WTGA-IMP-SOPR-PROF-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMP-SOPR-PROF
                TO WTGA-IMP-SOPR-PROF(IX-TAB-TGA)
           END-IF
           IF L3421-IMP-SOPR-SAN-NULL = HIGH-VALUES
              MOVE L3421-IMP-SOPR-SAN-NULL
                TO WTGA-IMP-SOPR-SAN-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMP-SOPR-SAN
                TO WTGA-IMP-SOPR-SAN(IX-TAB-TGA)
           END-IF
           IF L3421-IMP-SOPR-SPO-NULL = HIGH-VALUES
              MOVE L3421-IMP-SOPR-SPO-NULL
                TO WTGA-IMP-SOPR-SPO-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMP-SOPR-SPO
                TO WTGA-IMP-SOPR-SPO(IX-TAB-TGA)
           END-IF
           IF L3421-IMP-SOPR-TEC-NULL = HIGH-VALUES
              MOVE L3421-IMP-SOPR-TEC-NULL
                TO WTGA-IMP-SOPR-TEC-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMP-SOPR-TEC
                TO WTGA-IMP-SOPR-TEC(IX-TAB-TGA)
           END-IF
           IF L3421-IMP-ALT-SOPR-NULL = HIGH-VALUES
              MOVE L3421-IMP-ALT-SOPR-NULL
                TO WTGA-IMP-ALT-SOPR-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMP-ALT-SOPR
                TO WTGA-IMP-ALT-SOPR(IX-TAB-TGA)
           END-IF
           IF L3421-PRE-STAB-NULL = HIGH-VALUES
              MOVE L3421-PRE-STAB-NULL
                TO WTGA-PRE-STAB-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRE-STAB
                TO WTGA-PRE-STAB(IX-TAB-TGA)
           END-IF
           IF L3421-DT-EFF-STAB-NULL = HIGH-VALUES
              MOVE L3421-DT-EFF-STAB-NULL
                TO WTGA-DT-EFF-STAB-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-DT-EFF-STAB
                TO WTGA-DT-EFF-STAB(IX-TAB-TGA)
           END-IF
           IF L3421-TS-RIVAL-FIS-NULL = HIGH-VALUES
              MOVE L3421-TS-RIVAL-FIS-NULL
                TO WTGA-TS-RIVAL-FIS-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-TS-RIVAL-FIS
                TO WTGA-TS-RIVAL-FIS(IX-TAB-TGA)
           END-IF
           IF L3421-TS-RIVAL-INDICIZ-NULL = HIGH-VALUES
              MOVE L3421-TS-RIVAL-INDICIZ-NULL
                TO WTGA-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-TS-RIVAL-INDICIZ
                TO WTGA-TS-RIVAL-INDICIZ(IX-TAB-TGA)
           END-IF
           IF L3421-OLD-TS-TEC-NULL = HIGH-VALUES
              MOVE L3421-OLD-TS-TEC-NULL
                TO WTGA-OLD-TS-TEC-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-OLD-TS-TEC
                TO WTGA-OLD-TS-TEC(IX-TAB-TGA)
           END-IF
           IF L3421-RAT-LRD-NULL = HIGH-VALUES
              MOVE L3421-RAT-LRD-NULL
                TO WTGA-RAT-LRD-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-RAT-LRD
                TO WTGA-RAT-LRD(IX-TAB-TGA)
           END-IF
           IF L3421-PRE-LRD-NULL = HIGH-VALUES
              MOVE L3421-PRE-LRD-NULL
                TO WTGA-PRE-LRD-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRE-LRD
                TO WTGA-PRE-LRD(IX-TAB-TGA)
           END-IF
           IF L3421-PRSTZ-INI-NULL = HIGH-VALUES
              MOVE L3421-PRSTZ-INI-NULL
                TO WTGA-PRSTZ-INI-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRSTZ-INI
                TO WTGA-PRSTZ-INI(IX-TAB-TGA)
           END-IF
           IF L3421-PRSTZ-ULT-NULL = HIGH-VALUES
              MOVE L3421-PRSTZ-ULT-NULL
                TO WTGA-PRSTZ-ULT-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRSTZ-ULT
                TO WTGA-PRSTZ-ULT(IX-TAB-TGA)
           END-IF
           IF L3421-CPT-IN-OPZ-RIVTO-NULL = HIGH-VALUES
              MOVE L3421-CPT-IN-OPZ-RIVTO-NULL
                TO WTGA-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-CPT-IN-OPZ-RIVTO
                TO WTGA-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
           END-IF
           IF L3421-PRSTZ-INI-STAB-NULL = HIGH-VALUES
              MOVE L3421-PRSTZ-INI-STAB-NULL
                TO WTGA-PRSTZ-INI-STAB-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRSTZ-INI-STAB
                TO WTGA-PRSTZ-INI-STAB(IX-TAB-TGA)
           END-IF
           IF L3421-CPT-RSH-MOR-NULL = HIGH-VALUES
              MOVE L3421-CPT-RSH-MOR-NULL
                TO WTGA-CPT-RSH-MOR-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-CPT-RSH-MOR
                TO WTGA-CPT-RSH-MOR(IX-TAB-TGA)
           END-IF
           IF L3421-PRSTZ-RID-INI-NULL = HIGH-VALUES
              MOVE L3421-PRSTZ-RID-INI-NULL
                TO WTGA-PRSTZ-RID-INI-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRSTZ-RID-INI
                TO WTGA-PRSTZ-RID-INI(IX-TAB-TGA)
           END-IF
           IF L3421-FL-CAR-CONT-NULL = HIGH-VALUES
              MOVE L3421-FL-CAR-CONT-NULL
                TO WTGA-FL-CAR-CONT-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-FL-CAR-CONT
                TO WTGA-FL-CAR-CONT(IX-TAB-TGA)
           END-IF
           IF L3421-BNS-GIA-LIQTO-NULL = HIGH-VALUES
              MOVE L3421-BNS-GIA-LIQTO-NULL
                TO WTGA-BNS-GIA-LIQTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-BNS-GIA-LIQTO
                TO WTGA-BNS-GIA-LIQTO(IX-TAB-TGA)
           END-IF
           IF L3421-IMP-BNS-NULL = HIGH-VALUES
              MOVE L3421-IMP-BNS-NULL
                TO WTGA-IMP-BNS-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMP-BNS
                TO WTGA-IMP-BNS(IX-TAB-TGA)
           END-IF
           MOVE L3421-COD-DVS
             TO WTGA-COD-DVS(IX-TAB-TGA)
           IF L3421-PRSTZ-INI-NEWFIS-NULL = HIGH-VALUES
              MOVE L3421-PRSTZ-INI-NEWFIS-NULL
                TO WTGA-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRSTZ-INI-NEWFIS
                TO WTGA-PRSTZ-INI-NEWFIS(IX-TAB-TGA)
           END-IF
           IF L3421-IMP-SCON-NULL = HIGH-VALUES
              MOVE L3421-IMP-SCON-NULL
                TO WTGA-IMP-SCON-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMP-SCON
                TO WTGA-IMP-SCON(IX-TAB-TGA)
           END-IF
           IF L3421-ALQ-SCON-NULL = HIGH-VALUES
              MOVE L3421-ALQ-SCON-NULL
                TO WTGA-ALQ-SCON-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-ALQ-SCON
                TO WTGA-ALQ-SCON(IX-TAB-TGA)
           END-IF
           IF L3421-IMP-CAR-ACQ-NULL = HIGH-VALUES
              MOVE L3421-IMP-CAR-ACQ-NULL
                TO WTGA-IMP-CAR-ACQ-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMP-CAR-ACQ
                TO WTGA-IMP-CAR-ACQ(IX-TAB-TGA)
           END-IF
           IF L3421-IMP-CAR-INC-NULL = HIGH-VALUES
              MOVE L3421-IMP-CAR-INC-NULL
                TO WTGA-IMP-CAR-INC-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMP-CAR-INC
                TO WTGA-IMP-CAR-INC(IX-TAB-TGA)
           END-IF
           IF L3421-IMP-CAR-GEST-NULL = HIGH-VALUES
              MOVE L3421-IMP-CAR-GEST-NULL
                TO WTGA-IMP-CAR-GEST-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMP-CAR-GEST
                TO WTGA-IMP-CAR-GEST(IX-TAB-TGA)
           END-IF
           IF L3421-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L3421-ETA-AA-1O-ASSTO-NULL
                TO WTGA-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-ETA-AA-1O-ASSTO
                TO WTGA-ETA-AA-1O-ASSTO(IX-TAB-TGA)
           END-IF
           IF L3421-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L3421-ETA-MM-1O-ASSTO-NULL
                TO WTGA-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-ETA-MM-1O-ASSTO
                TO WTGA-ETA-MM-1O-ASSTO(IX-TAB-TGA)
           END-IF
           IF L3421-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L3421-ETA-AA-2O-ASSTO-NULL
                TO WTGA-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-ETA-AA-2O-ASSTO
                TO WTGA-ETA-AA-2O-ASSTO(IX-TAB-TGA)
           END-IF
           IF L3421-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L3421-ETA-MM-2O-ASSTO-NULL
                TO WTGA-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-ETA-MM-2O-ASSTO
                TO WTGA-ETA-MM-2O-ASSTO(IX-TAB-TGA)
           END-IF
           IF L3421-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L3421-ETA-AA-3O-ASSTO-NULL
                TO WTGA-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-ETA-AA-3O-ASSTO
                TO WTGA-ETA-AA-3O-ASSTO(IX-TAB-TGA)
           END-IF
           IF L3421-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L3421-ETA-MM-3O-ASSTO-NULL
                TO WTGA-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-ETA-MM-3O-ASSTO
                TO WTGA-ETA-MM-3O-ASSTO(IX-TAB-TGA)
           END-IF
           IF L3421-RENDTO-LRD-NULL = HIGH-VALUES
              MOVE L3421-RENDTO-LRD-NULL
                TO WTGA-RENDTO-LRD-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-RENDTO-LRD
                TO WTGA-RENDTO-LRD(IX-TAB-TGA)
           END-IF
           IF L3421-PC-RETR-NULL = HIGH-VALUES
              MOVE L3421-PC-RETR-NULL
                TO WTGA-PC-RETR-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PC-RETR
                TO WTGA-PC-RETR(IX-TAB-TGA)
           END-IF
           IF L3421-RENDTO-RETR-NULL = HIGH-VALUES
              MOVE L3421-RENDTO-RETR-NULL
                TO WTGA-RENDTO-RETR-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-RENDTO-RETR
                TO WTGA-RENDTO-RETR(IX-TAB-TGA)
           END-IF
           IF L3421-MIN-GARTO-NULL = HIGH-VALUES
              MOVE L3421-MIN-GARTO-NULL
                TO WTGA-MIN-GARTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-MIN-GARTO
                TO WTGA-MIN-GARTO(IX-TAB-TGA)
           END-IF
           IF L3421-MIN-TRNUT-NULL = HIGH-VALUES
              MOVE L3421-MIN-TRNUT-NULL
                TO WTGA-MIN-TRNUT-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-MIN-TRNUT
                TO WTGA-MIN-TRNUT(IX-TAB-TGA)
           END-IF
           IF L3421-PRE-ATT-DI-TRCH-NULL = HIGH-VALUES
              MOVE L3421-PRE-ATT-DI-TRCH-NULL
                TO WTGA-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRE-ATT-DI-TRCH
                TO WTGA-PRE-ATT-DI-TRCH(IX-TAB-TGA)
           END-IF
           IF L3421-MATU-END2000-NULL = HIGH-VALUES
              MOVE L3421-MATU-END2000-NULL
                TO WTGA-MATU-END2000-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-MATU-END2000
                TO WTGA-MATU-END2000(IX-TAB-TGA)
           END-IF
           IF L3421-ABB-TOT-INI-NULL = HIGH-VALUES
              MOVE L3421-ABB-TOT-INI-NULL
                TO WTGA-ABB-TOT-INI-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-ABB-TOT-INI
                TO WTGA-ABB-TOT-INI(IX-TAB-TGA)
           END-IF
           IF L3421-ABB-TOT-ULT-NULL = HIGH-VALUES
              MOVE L3421-ABB-TOT-ULT-NULL
                TO WTGA-ABB-TOT-ULT-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-ABB-TOT-ULT
                TO WTGA-ABB-TOT-ULT(IX-TAB-TGA)
           END-IF
           IF L3421-ABB-ANNU-ULT-NULL = HIGH-VALUES
              MOVE L3421-ABB-ANNU-ULT-NULL
                TO WTGA-ABB-ANNU-ULT-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-ABB-ANNU-ULT
                TO WTGA-ABB-ANNU-ULT(IX-TAB-TGA)
           END-IF
           IF L3421-DUR-ABB-NULL = HIGH-VALUES
              MOVE L3421-DUR-ABB-NULL
                TO WTGA-DUR-ABB-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-DUR-ABB
                TO WTGA-DUR-ABB(IX-TAB-TGA)
           END-IF
           IF L3421-TP-ADEG-ABB-NULL = HIGH-VALUES
              MOVE L3421-TP-ADEG-ABB-NULL
                TO WTGA-TP-ADEG-ABB-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-TP-ADEG-ABB
                TO WTGA-TP-ADEG-ABB(IX-TAB-TGA)
           END-IF
           IF L3421-MOD-CALC-NULL = HIGH-VALUES
              MOVE L3421-MOD-CALC-NULL
                TO WTGA-MOD-CALC-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-MOD-CALC
                TO WTGA-MOD-CALC(IX-TAB-TGA)
           END-IF
           IF L3421-IMP-AZ-NULL = HIGH-VALUES
              MOVE L3421-IMP-AZ-NULL
                TO WTGA-IMP-AZ-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMP-AZ
                TO WTGA-IMP-AZ(IX-TAB-TGA)
           END-IF
           IF L3421-IMP-ADER-NULL = HIGH-VALUES
              MOVE L3421-IMP-ADER-NULL
                TO WTGA-IMP-ADER-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMP-ADER
                TO WTGA-IMP-ADER(IX-TAB-TGA)
           END-IF
           IF L3421-IMP-TFR-NULL = HIGH-VALUES
              MOVE L3421-IMP-TFR-NULL
                TO WTGA-IMP-TFR-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMP-TFR
                TO WTGA-IMP-TFR(IX-TAB-TGA)
           END-IF
           IF L3421-IMP-VOLO-NULL = HIGH-VALUES
              MOVE L3421-IMP-VOLO-NULL
                TO WTGA-IMP-VOLO-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMP-VOLO
                TO WTGA-IMP-VOLO(IX-TAB-TGA)
           END-IF
           IF L3421-VIS-END2000-NULL = HIGH-VALUES
              MOVE L3421-VIS-END2000-NULL
                TO WTGA-VIS-END2000-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-VIS-END2000
                TO WTGA-VIS-END2000(IX-TAB-TGA)
           END-IF
           IF L3421-DT-VLDT-PROD-NULL = HIGH-VALUES
              MOVE L3421-DT-VLDT-PROD-NULL
                TO WTGA-DT-VLDT-PROD-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-DT-VLDT-PROD
                TO WTGA-DT-VLDT-PROD(IX-TAB-TGA)
           END-IF
           IF L3421-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE L3421-DT-INI-VAL-TAR-NULL
                TO WTGA-DT-INI-VAL-TAR-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-DT-INI-VAL-TAR
                TO WTGA-DT-INI-VAL-TAR(IX-TAB-TGA)
           END-IF
           IF L3421-IMPB-VIS-END2000-NULL = HIGH-VALUES
              MOVE L3421-IMPB-VIS-END2000-NULL
                TO WTGA-IMPB-VIS-END2000-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMPB-VIS-END2000
                TO WTGA-IMPB-VIS-END2000(IX-TAB-TGA)
           END-IF
           IF L3421-REN-INI-TS-TEC-0-NULL = HIGH-VALUES
              MOVE L3421-REN-INI-TS-TEC-0-NULL
                TO WTGA-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-REN-INI-TS-TEC-0
                TO WTGA-REN-INI-TS-TEC-0(IX-TAB-TGA)
           END-IF
           IF L3421-PC-RIP-PRE-NULL = HIGH-VALUES
              MOVE L3421-PC-RIP-PRE-NULL
                TO WTGA-PC-RIP-PRE-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PC-RIP-PRE
                TO WTGA-PC-RIP-PRE(IX-TAB-TGA)
           END-IF
           IF L3421-FL-IMPORTI-FORZ-NULL = HIGH-VALUES
              MOVE L3421-FL-IMPORTI-FORZ-NULL
                TO WTGA-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-FL-IMPORTI-FORZ
                TO WTGA-FL-IMPORTI-FORZ(IX-TAB-TGA)
           END-IF
           IF L3421-PRSTZ-INI-NFORZ-NULL = HIGH-VALUES
              MOVE L3421-PRSTZ-INI-NFORZ-NULL
                TO WTGA-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRSTZ-INI-NFORZ
                TO WTGA-PRSTZ-INI-NFORZ(IX-TAB-TGA)
           END-IF
           IF L3421-VIS-END2000-NFORZ-NULL = HIGH-VALUES
              MOVE L3421-VIS-END2000-NFORZ-NULL
                TO WTGA-VIS-END2000-NFORZ-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-VIS-END2000-NFORZ
                TO WTGA-VIS-END2000-NFORZ(IX-TAB-TGA)
           END-IF
           IF L3421-INTR-MORA-NULL = HIGH-VALUES
              MOVE L3421-INTR-MORA-NULL
                TO WTGA-INTR-MORA-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-INTR-MORA
                TO WTGA-INTR-MORA(IX-TAB-TGA)
           END-IF
           IF L3421-MANFEE-ANTIC-NULL = HIGH-VALUES
              MOVE L3421-MANFEE-ANTIC-NULL
                TO WTGA-MANFEE-ANTIC-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-MANFEE-ANTIC
                TO WTGA-MANFEE-ANTIC(IX-TAB-TGA)
           END-IF
           IF L3421-MANFEE-RICOR-NULL = HIGH-VALUES
              MOVE L3421-MANFEE-RICOR-NULL
                TO WTGA-MANFEE-RICOR-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-MANFEE-RICOR
                TO WTGA-MANFEE-RICOR(IX-TAB-TGA)
           END-IF
           IF L3421-PRE-UNI-RIVTO-NULL = HIGH-VALUES
              MOVE L3421-PRE-UNI-RIVTO-NULL
                TO WTGA-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRE-UNI-RIVTO
                TO WTGA-PRE-UNI-RIVTO(IX-TAB-TGA)
           END-IF
           IF L3421-PROV-1AA-ACQ-NULL = HIGH-VALUES
              MOVE L3421-PROV-1AA-ACQ-NULL
                TO WTGA-PROV-1AA-ACQ-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PROV-1AA-ACQ
                TO WTGA-PROV-1AA-ACQ(IX-TAB-TGA)
           END-IF
           IF L3421-PROV-2AA-ACQ-NULL = HIGH-VALUES
              MOVE L3421-PROV-2AA-ACQ-NULL
                TO WTGA-PROV-2AA-ACQ-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PROV-2AA-ACQ
                TO WTGA-PROV-2AA-ACQ(IX-TAB-TGA)
           END-IF
           IF L3421-PROV-RICOR-NULL = HIGH-VALUES
              MOVE L3421-PROV-RICOR-NULL
                TO WTGA-PROV-RICOR-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PROV-RICOR
                TO WTGA-PROV-RICOR(IX-TAB-TGA)
           END-IF
           IF L3421-PROV-INC-NULL = HIGH-VALUES
              MOVE L3421-PROV-INC-NULL
                TO WTGA-PROV-INC-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PROV-INC
                TO WTGA-PROV-INC(IX-TAB-TGA)
           END-IF
           IF L3421-ALQ-PROV-ACQ-NULL = HIGH-VALUES
              MOVE L3421-ALQ-PROV-ACQ-NULL
                TO WTGA-ALQ-PROV-ACQ-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-ALQ-PROV-ACQ
                TO WTGA-ALQ-PROV-ACQ(IX-TAB-TGA)
           END-IF
           IF L3421-ALQ-PROV-INC-NULL = HIGH-VALUES
              MOVE L3421-ALQ-PROV-INC-NULL
                TO WTGA-ALQ-PROV-INC-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-ALQ-PROV-INC
                TO WTGA-ALQ-PROV-INC(IX-TAB-TGA)
           END-IF
           IF L3421-ALQ-PROV-RICOR-NULL = HIGH-VALUES
              MOVE L3421-ALQ-PROV-RICOR-NULL
                TO WTGA-ALQ-PROV-RICOR-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-ALQ-PROV-RICOR
                TO WTGA-ALQ-PROV-RICOR(IX-TAB-TGA)
           END-IF
           IF L3421-IMPB-PROV-ACQ-NULL = HIGH-VALUES
              MOVE L3421-IMPB-PROV-ACQ-NULL
                TO WTGA-IMPB-PROV-ACQ-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMPB-PROV-ACQ
                TO WTGA-IMPB-PROV-ACQ(IX-TAB-TGA)
           END-IF
           IF L3421-IMPB-PROV-INC-NULL = HIGH-VALUES
              MOVE L3421-IMPB-PROV-INC-NULL
                TO WTGA-IMPB-PROV-INC-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMPB-PROV-INC
                TO WTGA-IMPB-PROV-INC(IX-TAB-TGA)
           END-IF
           IF L3421-IMPB-PROV-RICOR-NULL = HIGH-VALUES
              MOVE L3421-IMPB-PROV-RICOR-NULL
                TO WTGA-IMPB-PROV-RICOR-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-IMPB-PROV-RICOR
                TO WTGA-IMPB-PROV-RICOR(IX-TAB-TGA)
           END-IF
           IF L3421-FL-PROV-FORZ-NULL = HIGH-VALUES
              MOVE L3421-FL-PROV-FORZ-NULL
                TO WTGA-FL-PROV-FORZ-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-FL-PROV-FORZ
                TO WTGA-FL-PROV-FORZ(IX-TAB-TGA)
           END-IF
           IF L3421-PRSTZ-AGG-INI-NULL = HIGH-VALUES
              MOVE L3421-PRSTZ-AGG-INI-NULL
                TO WTGA-PRSTZ-AGG-INI-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRSTZ-AGG-INI
                TO WTGA-PRSTZ-AGG-INI(IX-TAB-TGA)
           END-IF
           IF L3421-INCR-PRE-NULL = HIGH-VALUES
              MOVE L3421-INCR-PRE-NULL
                TO WTGA-INCR-PRE-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-INCR-PRE
                TO WTGA-INCR-PRE(IX-TAB-TGA)
           END-IF
           IF L3421-INCR-PRSTZ-NULL = HIGH-VALUES
              MOVE L3421-INCR-PRSTZ-NULL
                TO WTGA-INCR-PRSTZ-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-INCR-PRSTZ
                TO WTGA-INCR-PRSTZ(IX-TAB-TGA)
           END-IF
           IF L3421-DT-ULT-ADEG-PRE-PR-NULL = HIGH-VALUES
              MOVE L3421-DT-ULT-ADEG-PRE-PR-NULL
                TO WTGA-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-DT-ULT-ADEG-PRE-PR
                TO WTGA-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
           END-IF
           IF L3421-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
              MOVE L3421-PRSTZ-AGG-ULT-NULL
                TO WTGA-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRSTZ-AGG-ULT
                TO WTGA-PRSTZ-AGG-ULT(IX-TAB-TGA)
           END-IF
           IF L3421-TS-RIVAL-NET-NULL = HIGH-VALUES
              MOVE L3421-TS-RIVAL-NET-NULL
                TO WTGA-TS-RIVAL-NET-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-TS-RIVAL-NET
                TO WTGA-TS-RIVAL-NET(IX-TAB-TGA)
           END-IF
           IF L3421-PRE-PATTUITO-NULL = HIGH-VALUES
              MOVE L3421-PRE-PATTUITO-NULL
                TO WTGA-PRE-PATTUITO-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PRE-PATTUITO
                TO WTGA-PRE-PATTUITO(IX-TAB-TGA)
           END-IF
           IF L3421-TP-RIVAL-NULL = HIGH-VALUES
              MOVE L3421-TP-RIVAL-NULL
                TO WTGA-TP-RIVAL-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-TP-RIVAL
                TO WTGA-TP-RIVAL(IX-TAB-TGA)
           END-IF
           IF L3421-RIS-MAT-NULL = HIGH-VALUES
              MOVE L3421-RIS-MAT-NULL
                TO WTGA-RIS-MAT-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-RIS-MAT
                TO WTGA-RIS-MAT(IX-TAB-TGA)
           END-IF
           MOVE L3421-DS-RIGA
             TO WTGA-DS-RIGA(IX-TAB-TGA)
           MOVE L3421-DS-OPER-SQL
             TO WTGA-DS-OPER-SQL(IX-TAB-TGA)
           MOVE L3421-DS-VER
             TO WTGA-DS-VER(IX-TAB-TGA)
           MOVE L3421-DS-TS-INI-CPTZ
             TO WTGA-DS-TS-INI-CPTZ(IX-TAB-TGA)
           MOVE L3421-DS-TS-END-CPTZ
             TO WTGA-DS-TS-END-CPTZ(IX-TAB-TGA)
           MOVE L3421-DS-UTENTE
             TO WTGA-DS-UTENTE(IX-TAB-TGA)
           MOVE L3421-DS-STATO-ELAB
             TO WTGA-DS-STATO-ELAB(IX-TAB-TGA).
           IF L3421-PC-COMMIS-GEST-NULL = HIGH-VALUES
              MOVE L3421-PC-COMMIS-GEST-NULL
                TO WTGA-PC-COMMIS-GEST-NULL(IX-TAB-TGA)
           ELSE
              MOVE L3421-PC-COMMIS-GEST
                TO WTGA-PC-COMMIS-GEST(IX-TAB-TGA)
           END-IF.
ITRFO      IF L3421-NUM-GG-RIVAL-NULL = HIGH-VALUES
ITRFO         MOVE L3421-NUM-GG-RIVAL-NULL
ITRFO           TO WTGA-NUM-GG-RIVAL-NULL(IX-TAB-TGA)
ITRFO      ELSE
ITRFO         MOVE L3421-NUM-GG-RIVAL
ITRFO           TO WTGA-NUM-GG-RIVAL(IX-TAB-TGA)
ITRFO      END-IF.
ITRFO      IF L3421-IMP-TRASFE-NULL = HIGH-VALUES
ITRFO         MOVE L3421-IMP-TRASFE-NULL
ITRFO           TO WTGA-IMP-TRASFE-NULL(IX-TAB-TGA)
ITRFO      ELSE
ITRFO         MOVE L3421-IMP-TRASFE
ITRFO           TO WTGA-IMP-TRASFE(IX-TAB-TGA)
ITRFO      END-IF.
ITRFO      IF L3421-IMP-TFR-STRC-NULL = HIGH-VALUES
ITRFO         MOVE L3421-IMP-TFR-STRC-NULL
ITRFO           TO WTGA-IMP-TFR-STRC-NULL(IX-TAB-TGA)
ITRFO      ELSE
ITRFO         MOVE L3421-IMP-TFR-STRC
ITRFO           TO WTGA-IMP-TFR-STRC(IX-TAB-TGA)
ITRFO      END-IF.
ITRFO      IF L3421-ACQ-EXP-NULL = HIGH-VALUES
ITRFO         MOVE L3421-ACQ-EXP-NULL
ITRFO           TO WTGA-ACQ-EXP-NULL(IX-TAB-TGA)
ITRFO      ELSE
ITRFO         MOVE L3421-ACQ-EXP
ITRFO           TO WTGA-ACQ-EXP(IX-TAB-TGA)
ITRFO      END-IF.
ITRFO      IF L3421-REMUN-ASS-NULL = HIGH-VALUES
ITRFO         MOVE L3421-REMUN-ASS-NULL
ITRFO           TO WTGA-REMUN-ASS-NULL(IX-TAB-TGA)
ITRFO      ELSE
ITRFO         MOVE L3421-REMUN-ASS
ITRFO           TO WTGA-REMUN-ASS(IX-TAB-TGA)
ITRFO      END-IF.
ITRFO      IF L3421-COMMIS-INTER-NULL = HIGH-VALUES
ITRFO         MOVE L3421-COMMIS-INTER-NULL
ITRFO           TO WTGA-COMMIS-INTER-NULL(IX-TAB-TGA)
ITRFO      ELSE
ITRFO         MOVE L3421-COMMIS-INTER
ITRFO           TO WTGA-COMMIS-INTER(IX-TAB-TGA)
ITRFO      END-IF.
ITRFO      IF L3421-ALQ-REMUN-ASS-NULL = HIGH-VALUES
ITRFO         MOVE L3421-ALQ-REMUN-ASS-NULL
ITRFO           TO WTGA-ALQ-REMUN-ASS-NULL(IX-TAB-TGA)
ITRFO      ELSE
ITRFO         MOVE L3421-ALQ-REMUN-ASS
ITRFO           TO WTGA-ALQ-REMUN-ASS(IX-TAB-TGA)
ITRFO      END-IF.
ITRFO      IF L3421-ALQ-COMMIS-INTER-NULL = HIGH-VALUES
ITRFO         MOVE L3421-ALQ-COMMIS-INTER-NULL
ITRFO           TO WTGA-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA)
ITRFO      ELSE
ITRFO         MOVE L3421-ALQ-COMMIS-INTER
ITRFO           TO WTGA-ALQ-COMMIS-INTER(IX-TAB-TGA)
ITRFO      END-IF.
ITRFO      IF L3421-IMPB-REMUN-ASS-NULL = HIGH-VALUES
ITRFO         MOVE L3421-IMPB-REMUN-ASS-NULL
ITRFO           TO WTGA-IMPB-REMUN-ASS-NULL(IX-TAB-TGA)
ITRFO      ELSE
ITRFO         MOVE L3421-IMPB-REMUN-ASS
ITRFO           TO WTGA-IMPB-REMUN-ASS(IX-TAB-TGA)
ITRFO      END-IF.
ITRFO      IF L3421-IMPB-COMMIS-INTER-NULL = HIGH-VALUES
ITRFO         MOVE L3421-IMPB-COMMIS-INTER-NULL
ITRFO           TO WTGA-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA)
ITRFO      ELSE
ITRFO         MOVE L3421-IMPB-COMMIS-INTER
ITRFO           TO WTGA-IMPB-COMMIS-INTER(IX-TAB-TGA)
ITRFO      END-IF.
400747* INC000006400747
400747     IF L3421-COS-RUN-ASSVA-NULL = HIGH-VALUES
400747        MOVE L3421-COS-RUN-ASSVA-NULL
400747          TO WTGA-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
400747     ELSE
400747        MOVE L3421-COS-RUN-ASSVA
400747          TO WTGA-COS-RUN-ASSVA(IX-TAB-TGA)
400747     END-IF.
400747     IF L3421-COS-RUN-ASSVA-IDC-NULL = HIGH-VALUES
400747        MOVE L3421-COS-RUN-ASSVA-IDC-NULL
400747          TO WTGA-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA)
400747     ELSE
400747        MOVE L3421-COS-RUN-ASSVA-IDC
400747          TO WTGA-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
400747     END-IF.

       VALORIZZA-TGA-EX.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       VALORIZZA-STB.

           MOVE 'VALORIZZA-STB'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE L3421-ID-TRCH-DI-GAR
             TO WSTB-ID-OGG(IX-TAB-STB)
           MOVE 'TG'
             TO WSTB-TP-OGG(IX-TAB-STB)
           MOVE L3421-TP-STAT-BUS
             TO WSTB-TP-STAT-BUS(IX-TAB-STB)
           MOVE L3421-TP-CAUS
             TO WSTB-TP-CAUS(IX-TAB-STB).

       VALORIZZA-STB-EX.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       INIZIA-TOT-GRZ-ST.

           MOVE 'INIZIA-TOT-GRZ-ST'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           PERFORM INIZIA-ZEROES-GRZ-ST THRU INIZIA-ZEROES-GRZ-ST-EX

           PERFORM INIZIA-SPACES-GRZ-ST THRU INIZIA-SPACES-GRZ-ST-EX

           PERFORM INIZIA-NULL-GRZ-ST THRU INIZIA-NULL-GRZ-ST-EX.

       INIZIA-TOT-GRZ-ST-EX.
           EXIT.

       INIZIA-NULL-GRZ-ST.
           MOVE HIGH-VALUES TO WSTR-ID-ADES-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-ID-MOVI-CHIU-NULL(IX-TAB-STR)

           MOVE HIGH-VALUES TO WSTR-IB-OGG-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-DT-DECOR-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-DT-SCAD-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-COD-SEZ-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-RAMO-BILA-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-DT-INI-VAL-TAR-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-ID-1O-ASSTO-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-ID-2O-ASSTO-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-ID-3O-ASSTO-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-TP-GAR-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-TP-RSH-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-TP-INVST-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-MOD-PAG-GARCOL-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-TP-PER-PRE-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-ETA-AA-1O-ASSTO-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-ETA-MM-1O-ASSTO-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-ETA-AA-2O-ASSTO-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-ETA-MM-2O-ASSTO-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-ETA-AA-3O-ASSTO-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-ETA-MM-3O-ASSTO-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-TP-EMIS-PUR-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-ETA-A-SCAD-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-TP-CALC-PRE-PRSTZ-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-TP-PRE-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-TP-DUR-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-DUR-AA-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-DUR-MM-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-DUR-GG-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-NUM-AA-PAG-PRE-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-AA-PAG-PRE-UNI-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-MM-PAG-PRE-UNI-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-FRAZ-INI-EROG-REN-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-MM-1O-RAT-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-PC-1O-RAT-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-TP-PRSTZ-ASSTA-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-DT-END-CARZ-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-PC-RIP-PRE-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-COD-FND-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-AA-REN-CER-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-PC-REVRSB-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-TP-PC-RIP-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-PC-OPZ-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-TP-IAS-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-TP-STAB-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-TP-ADEG-PRE-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-DT-VARZ-TP-IAS-NULL(IX-TAB-STR)
           MOVE HIGH-VALUES TO WSTR-FRAZ-DECR-CPT-NULL(IX-TAB-STR).
      *       256 --> Tasso stabilizzazione limitata
           MOVE HIGH-VALUES TO WSTR-TS-STAB-LIMITATA-NULL(IX-TAB-STR).
      *       256 --> Tasso stabilizzazione limitata fine
       INIZIA-NULL-GRZ-ST-EX.
           EXIT.

       INIZIA-ZEROES-GRZ-ST.
           MOVE 0 TO WSTR-ID-GAR(IX-TAB-STR)
           MOVE 0 TO WSTR-ID-POLI(IX-TAB-STR)
           MOVE 0 TO WSTR-ID-MOVI-CRZ(IX-TAB-STR)
           MOVE 0 TO WSTR-DT-INI-EFF(IX-TAB-STR)
           MOVE 0 TO WSTR-DT-END-EFF(IX-TAB-STR)
           MOVE 0 TO WSTR-COD-COMP-ANIA(IX-TAB-STR)
           MOVE 0 TO WSTR-DS-RIGA(IX-TAB-STR)
           MOVE 0 TO WSTR-DS-VER(IX-TAB-STR)
           MOVE 0 TO WSTR-DS-TS-INI-CPTZ(IX-TAB-STR)
           MOVE 0 TO WSTR-DS-TS-END-CPTZ(IX-TAB-STR).
       INIZIA-ZEROES-GRZ-ST-EX.
           EXIT.

       INIZIA-SPACES-GRZ-ST.
           MOVE SPACES TO WSTR-COD-TARI(IX-TAB-STR)
           MOVE SPACES TO WSTR-DS-OPER-SQL(IX-TAB-STR)
           MOVE SPACES TO WSTR-DS-UTENTE(IX-TAB-STR)
           MOVE SPACES TO WSTR-DS-STATO-ELAB(IX-TAB-STR).
       INIZIA-SPACES-GRZ-ST-EX.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       VAL-OUT-ST-GRZ-3400.
           MOVE L3401-ID-GAR
             TO WSTR-ID-PTF(IX-TAB-STR)
           MOVE L3401-ID-GAR
             TO WSTR-ID-GAR(IX-TAB-STR)
           IF L3401-ID-ADES-NULL = HIGH-VALUES
              MOVE L3401-ID-ADES-NULL
                TO WSTR-ID-ADES-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-ID-ADES
                TO WSTR-ID-ADES(IX-TAB-STR)
           END-IF
           MOVE L3401-ID-POLI
             TO WSTR-ID-POLI(IX-TAB-STR)
           MOVE L3401-ID-MOVI-CRZ
             TO WSTR-ID-MOVI-CRZ(IX-TAB-STR)
           IF L3401-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE L3401-ID-MOVI-CHIU-NULL
                TO WSTR-ID-MOVI-CHIU-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-ID-MOVI-CHIU
                TO WSTR-ID-MOVI-CHIU(IX-TAB-STR)
           END-IF
           MOVE L3401-DT-INI-EFF
             TO WSTR-DT-INI-EFF(IX-TAB-STR)
           MOVE L3401-DT-END-EFF
             TO WSTR-DT-END-EFF(IX-TAB-STR)
           MOVE L3401-COD-COMP-ANIA
             TO WSTR-COD-COMP-ANIA(IX-TAB-STR)
           IF L3401-IB-OGG-NULL = HIGH-VALUES
              MOVE L3401-IB-OGG-NULL
                TO WSTR-IB-OGG-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-IB-OGG
                TO WSTR-IB-OGG(IX-TAB-STR)
           END-IF
           IF L3401-DT-DECOR-NULL = HIGH-VALUES
              MOVE L3401-DT-DECOR-NULL
                TO WSTR-DT-DECOR-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-DT-DECOR
                TO WSTR-DT-DECOR(IX-TAB-STR)
           END-IF
           IF L3401-DT-SCAD-NULL = HIGH-VALUES
              MOVE L3401-DT-SCAD-NULL
                TO WSTR-DT-SCAD-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-DT-SCAD
                TO WSTR-DT-SCAD(IX-TAB-STR)
           END-IF
           IF L3401-COD-SEZ-NULL = HIGH-VALUES
              MOVE L3401-COD-SEZ-NULL
                TO WSTR-COD-SEZ-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-COD-SEZ
                TO WSTR-COD-SEZ(IX-TAB-STR)
           END-IF
           MOVE L3401-COD-TARI
             TO WSTR-COD-TARI(IX-TAB-STR)
           IF L3401-RAMO-BILA-NULL = HIGH-VALUES
              MOVE L3401-RAMO-BILA-NULL
                TO WSTR-RAMO-BILA-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-RAMO-BILA
                TO WSTR-RAMO-BILA(IX-TAB-STR)
           END-IF
           IF L3401-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE L3401-DT-INI-VAL-TAR-NULL
                TO WSTR-DT-INI-VAL-TAR-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-DT-INI-VAL-TAR
                TO WSTR-DT-INI-VAL-TAR(IX-TAB-STR)
           END-IF
           IF L3401-ID-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ID-1O-ASSTO-NULL
                TO WSTR-ID-1O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-ID-1O-ASSTO
                TO WSTR-ID-1O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3401-ID-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ID-2O-ASSTO-NULL
                TO WSTR-ID-2O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-ID-2O-ASSTO
                TO WSTR-ID-2O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3401-ID-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ID-3O-ASSTO-NULL
                TO WSTR-ID-3O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-ID-3O-ASSTO
                TO WSTR-ID-3O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3401-TP-GAR-NULL = HIGH-VALUES
              MOVE L3401-TP-GAR-NULL
                TO WSTR-TP-GAR-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-TP-GAR
                TO WSTR-TP-GAR(IX-TAB-STR)
           END-IF
           IF L3401-TP-RSH-NULL = HIGH-VALUES
              MOVE L3401-TP-RSH-NULL
                TO WSTR-TP-RSH-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-TP-RSH
                TO WSTR-TP-RSH(IX-TAB-STR)
           END-IF
           IF L3401-TP-INVST-NULL = HIGH-VALUES
              MOVE L3401-TP-INVST-NULL
                TO WSTR-TP-INVST-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-TP-INVST
                TO WSTR-TP-INVST(IX-TAB-STR)
           END-IF
           IF L3401-MOD-PAG-GARCOL-NULL = HIGH-VALUES
              MOVE L3401-MOD-PAG-GARCOL-NULL
                TO WSTR-MOD-PAG-GARCOL-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-MOD-PAG-GARCOL
                TO WSTR-MOD-PAG-GARCOL(IX-TAB-STR)
           END-IF
           IF L3401-TP-PER-PRE-NULL = HIGH-VALUES
              MOVE L3401-TP-PER-PRE-NULL
                TO WSTR-TP-PER-PRE-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-TP-PER-PRE
                TO WSTR-TP-PER-PRE(IX-TAB-STR)
           END-IF
           IF L3401-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ETA-AA-1O-ASSTO-NULL
                TO WSTR-ETA-AA-1O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-ETA-AA-1O-ASSTO
                TO WSTR-ETA-AA-1O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3401-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ETA-MM-1O-ASSTO-NULL
                TO WSTR-ETA-MM-1O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-ETA-MM-1O-ASSTO
                TO WSTR-ETA-MM-1O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3401-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ETA-AA-2O-ASSTO-NULL
                TO WSTR-ETA-AA-2O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-ETA-AA-2O-ASSTO
                TO WSTR-ETA-AA-2O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3401-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ETA-MM-2O-ASSTO-NULL
                TO WSTR-ETA-MM-2O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-ETA-MM-2O-ASSTO
                TO WSTR-ETA-MM-2O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3401-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ETA-AA-3O-ASSTO-NULL
                TO WSTR-ETA-AA-3O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-ETA-AA-3O-ASSTO
                TO WSTR-ETA-AA-3O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3401-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ETA-MM-3O-ASSTO-NULL
                TO WSTR-ETA-MM-3O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-ETA-MM-3O-ASSTO
                TO WSTR-ETA-MM-3O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3401-TP-EMIS-PUR-NULL = HIGH-VALUES
              MOVE L3401-TP-EMIS-PUR-NULL
                TO WSTR-TP-EMIS-PUR-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-TP-EMIS-PUR
                TO WSTR-TP-EMIS-PUR(IX-TAB-STR)
           END-IF
           IF L3401-ETA-A-SCAD-NULL = HIGH-VALUES
              MOVE L3401-ETA-A-SCAD-NULL
                TO WSTR-ETA-A-SCAD-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-ETA-A-SCAD
                TO WSTR-ETA-A-SCAD(IX-TAB-STR)
           END-IF
           IF L3401-TP-CALC-PRE-PRSTZ-NULL = HIGH-VALUES
              MOVE L3401-TP-CALC-PRE-PRSTZ-NULL
                TO WSTR-TP-CALC-PRE-PRSTZ-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-TP-CALC-PRE-PRSTZ
                TO WSTR-TP-CALC-PRE-PRSTZ(IX-TAB-STR)
           END-IF
           IF L3401-TP-PRE-NULL = HIGH-VALUES
              MOVE L3401-TP-PRE-NULL
                TO WSTR-TP-PRE-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-TP-PRE
                TO WSTR-TP-PRE(IX-TAB-STR)
           END-IF
           IF L3401-TP-DUR-NULL = HIGH-VALUES
              MOVE L3401-TP-DUR-NULL
                TO WSTR-TP-DUR-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-TP-DUR
                TO WSTR-TP-DUR(IX-TAB-STR)
           END-IF
           IF L3401-DUR-AA-NULL = HIGH-VALUES
              MOVE L3401-DUR-AA-NULL
                TO WSTR-DUR-AA-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-DUR-AA
                TO WSTR-DUR-AA(IX-TAB-STR)
           END-IF
           IF L3401-DUR-MM-NULL = HIGH-VALUES
              MOVE L3401-DUR-MM-NULL
                TO WSTR-DUR-MM-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-DUR-MM
                TO WSTR-DUR-MM(IX-TAB-STR)
           END-IF
           IF L3401-DUR-GG-NULL = HIGH-VALUES
              MOVE L3401-DUR-GG-NULL
                TO WSTR-DUR-GG-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-DUR-GG
                TO WSTR-DUR-GG(IX-TAB-STR)
           END-IF
           IF L3401-NUM-AA-PAG-PRE-NULL = HIGH-VALUES
              MOVE L3401-NUM-AA-PAG-PRE-NULL
                TO WSTR-NUM-AA-PAG-PRE-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-NUM-AA-PAG-PRE
                TO WSTR-NUM-AA-PAG-PRE(IX-TAB-STR)
           END-IF
           IF L3401-AA-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE L3401-AA-PAG-PRE-UNI-NULL
                TO WSTR-AA-PAG-PRE-UNI-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-AA-PAG-PRE-UNI
                TO WSTR-AA-PAG-PRE-UNI(IX-TAB-STR)
           END-IF
           IF L3401-MM-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE L3401-MM-PAG-PRE-UNI-NULL
                TO WSTR-MM-PAG-PRE-UNI-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-MM-PAG-PRE-UNI
                TO WSTR-MM-PAG-PRE-UNI(IX-TAB-STR)
           END-IF
           IF L3401-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
              MOVE L3401-FRAZ-INI-EROG-REN-NULL
                TO WSTR-FRAZ-INI-EROG-REN-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-FRAZ-INI-EROG-REN
                TO WSTR-FRAZ-INI-EROG-REN(IX-TAB-STR)
           END-IF
           IF L3401-MM-1O-RAT-NULL = HIGH-VALUES
              MOVE L3401-MM-1O-RAT-NULL
                TO WSTR-MM-1O-RAT-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-MM-1O-RAT
                TO WSTR-MM-1O-RAT(IX-TAB-STR)
           END-IF
           IF L3401-PC-1O-RAT-NULL = HIGH-VALUES
              MOVE L3401-PC-1O-RAT-NULL
                TO WSTR-PC-1O-RAT-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-PC-1O-RAT
                TO WSTR-PC-1O-RAT(IX-TAB-STR)
           END-IF
           IF L3401-TP-PRSTZ-ASSTA-NULL = HIGH-VALUES
              MOVE L3401-TP-PRSTZ-ASSTA-NULL
                TO WSTR-TP-PRSTZ-ASSTA-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-TP-PRSTZ-ASSTA
                TO WSTR-TP-PRSTZ-ASSTA(IX-TAB-STR)
           END-IF
           IF L3401-DT-END-CARZ-NULL = HIGH-VALUES
              MOVE L3401-DT-END-CARZ-NULL
                TO WSTR-DT-END-CARZ-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-DT-END-CARZ
                TO WSTR-DT-END-CARZ(IX-TAB-STR)
           END-IF
           IF L3401-PC-RIP-PRE-NULL = HIGH-VALUES
              MOVE L3401-PC-RIP-PRE-NULL
                TO WSTR-PC-RIP-PRE-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-PC-RIP-PRE
                TO WSTR-PC-RIP-PRE(IX-TAB-STR)
           END-IF
           IF L3401-COD-FND-NULL = HIGH-VALUES
              MOVE L3401-COD-FND-NULL
                TO WSTR-COD-FND-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-COD-FND
                TO WSTR-COD-FND(IX-TAB-STR)
           END-IF
           IF L3401-AA-REN-CER-NULL = HIGH-VALUES
              MOVE L3401-AA-REN-CER-NULL
                TO WSTR-AA-REN-CER-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-AA-REN-CER
                TO WSTR-AA-REN-CER(IX-TAB-STR)
           END-IF
           IF L3401-PC-REVRSB-NULL = HIGH-VALUES
              MOVE L3401-PC-REVRSB-NULL
                TO WSTR-PC-REVRSB-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-PC-REVRSB
                TO WSTR-PC-REVRSB(IX-TAB-STR)
           END-IF
           IF L3401-TP-PC-RIP-NULL = HIGH-VALUES
              MOVE L3401-TP-PC-RIP-NULL
                TO WSTR-TP-PC-RIP-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-TP-PC-RIP
                TO WSTR-TP-PC-RIP(IX-TAB-STR)
           END-IF
           IF L3401-PC-OPZ-NULL = HIGH-VALUES
              MOVE L3401-PC-OPZ-NULL
                TO WSTR-PC-OPZ-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-PC-OPZ
                TO WSTR-PC-OPZ(IX-TAB-STR)
           END-IF
           IF L3401-TP-IAS-NULL = HIGH-VALUES
              MOVE L3401-TP-IAS-NULL
                TO WSTR-TP-IAS-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-TP-IAS
                TO WSTR-TP-IAS(IX-TAB-STR)
           END-IF
           IF L3401-TP-STAB-NULL = HIGH-VALUES
              MOVE L3401-TP-STAB-NULL
                TO WSTR-TP-STAB-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-TP-STAB
                TO WSTR-TP-STAB(IX-TAB-STR)
           END-IF
           IF L3401-TP-ADEG-PRE-NULL = HIGH-VALUES
              MOVE L3401-TP-ADEG-PRE-NULL
                TO WSTR-TP-ADEG-PRE-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-TP-ADEG-PRE
                TO WSTR-TP-ADEG-PRE(IX-TAB-STR)
           END-IF
           IF L3401-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
              MOVE L3401-DT-VARZ-TP-IAS-NULL
                TO WSTR-DT-VARZ-TP-IAS-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-DT-VARZ-TP-IAS
                TO WSTR-DT-VARZ-TP-IAS(IX-TAB-STR)
           END-IF
           IF L3401-FRAZ-DECR-CPT-NULL = HIGH-VALUES
              MOVE L3401-FRAZ-DECR-CPT-NULL
                TO WSTR-FRAZ-DECR-CPT-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-FRAZ-DECR-CPT
                TO WSTR-FRAZ-DECR-CPT(IX-TAB-STR)
           END-IF
           MOVE L3401-DS-RIGA
             TO WSTR-DS-RIGA(IX-TAB-STR)
           MOVE L3401-DS-OPER-SQL
             TO WSTR-DS-OPER-SQL(IX-TAB-STR)
           MOVE L3401-DS-VER
             TO WSTR-DS-VER(IX-TAB-STR)
           MOVE L3401-DS-TS-INI-CPTZ
             TO WSTR-DS-TS-INI-CPTZ(IX-TAB-STR)
           MOVE L3401-DS-TS-END-CPTZ
             TO WSTR-DS-TS-END-CPTZ(IX-TAB-STR)
           MOVE L3401-DS-UTENTE
             TO WSTR-DS-UTENTE(IX-TAB-STR)
           MOVE L3401-DS-STATO-ELAB
             TO WSTR-DS-STATO-ELAB(IX-TAB-STR)
      *       256 --> Tasso stabilizzazione limitata
           IF L3401-TS-STAB-LIMITATA-NULL = HIGH-VALUES
              MOVE L3401-TS-STAB-LIMITATA-NULL
                TO WSTR-TS-STAB-LIMITATA-NULL(IX-TAB-STR)
           ELSE
              MOVE L3401-TS-STAB-LIMITATA
                TO WSTR-TS-STAB-LIMITATA(IX-TAB-STR)
           END-IF.
ITRFO      IF L3401-RSH-INVST-NULL = HIGH-VALUES
ITRFO         MOVE L3401-RSH-INVST-NULL
ITRFO           TO WSTR-RSH-INVST-NULL(IX-TAB-STR)
ITRFO      ELSE
ITRFO         MOVE L3401-RSH-INVST
ITRFO           TO WSTR-RSH-INVST(IX-TAB-STR)
ITRFO      END-IF
ITRFO      MOVE L3401-TP-RAMO-BILA
ITRFO        TO WSTR-TP-RAMO-BILA(IX-TAB-STR).
      *       256 --> Tasso stabilizzazione limitata fine
       VAL-OUT-ST-GRZ-3400-EX.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       VAL-OUT-ST-GRZ-3410.
           MOVE L3411-ID-GAR
             TO WSTR-ID-PTF(IX-TAB-STR)
           MOVE L3411-ID-GAR
             TO WSTR-ID-GAR(IX-TAB-STR)
           IF L3411-ID-ADES-NULL = HIGH-VALUES
              MOVE L3411-ID-ADES-NULL
                TO WSTR-ID-ADES-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-ID-ADES
                TO WSTR-ID-ADES(IX-TAB-STR)
           END-IF
           MOVE L3411-ID-POLI
             TO WSTR-ID-POLI(IX-TAB-STR)
           MOVE L3411-ID-MOVI-CRZ
             TO WSTR-ID-MOVI-CRZ(IX-TAB-STR)
           IF L3411-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE L3411-ID-MOVI-CHIU-NULL
                TO WSTR-ID-MOVI-CHIU-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-ID-MOVI-CHIU
                TO WSTR-ID-MOVI-CHIU(IX-TAB-STR)
           END-IF
           MOVE L3411-DT-INI-EFF
             TO WSTR-DT-INI-EFF(IX-TAB-STR)
           MOVE L3411-DT-END-EFF
             TO WSTR-DT-END-EFF(IX-TAB-STR)
           MOVE L3411-COD-COMP-ANIA
             TO WSTR-COD-COMP-ANIA(IX-TAB-STR)
           IF L3411-IB-OGG-NULL = HIGH-VALUES
              MOVE L3411-IB-OGG-NULL
                TO WSTR-IB-OGG-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-IB-OGG
                TO WSTR-IB-OGG(IX-TAB-STR)
           END-IF
           IF L3411-DT-DECOR-NULL = HIGH-VALUES
              MOVE L3411-DT-DECOR-NULL
                TO WSTR-DT-DECOR-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-DT-DECOR
                TO WSTR-DT-DECOR(IX-TAB-STR)
           END-IF
           IF L3411-DT-SCAD-NULL = HIGH-VALUES
              MOVE L3411-DT-SCAD-NULL
                TO WSTR-DT-SCAD-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-DT-SCAD
                TO WSTR-DT-SCAD(IX-TAB-STR)
           END-IF
           IF L3411-COD-SEZ-NULL = HIGH-VALUES
              MOVE L3411-COD-SEZ-NULL
                TO WSTR-COD-SEZ-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-COD-SEZ
                TO WSTR-COD-SEZ(IX-TAB-STR)
           END-IF
           MOVE L3411-COD-TARI
             TO WSTR-COD-TARI(IX-TAB-STR)
           IF L3411-RAMO-BILA-NULL = HIGH-VALUES
              MOVE L3411-RAMO-BILA-NULL
                TO WSTR-RAMO-BILA-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-RAMO-BILA
                TO WSTR-RAMO-BILA(IX-TAB-STR)
           END-IF
           IF L3411-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE L3411-DT-INI-VAL-TAR-NULL
                TO WSTR-DT-INI-VAL-TAR-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-DT-INI-VAL-TAR
                TO WSTR-DT-INI-VAL-TAR(IX-TAB-STR)
           END-IF
           IF L3411-ID-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ID-1O-ASSTO-NULL
                TO WSTR-ID-1O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-ID-1O-ASSTO
                TO WSTR-ID-1O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3411-ID-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ID-2O-ASSTO-NULL
                TO WSTR-ID-2O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-ID-2O-ASSTO
                TO WSTR-ID-2O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3411-ID-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ID-3O-ASSTO-NULL
                TO WSTR-ID-3O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-ID-3O-ASSTO
                TO WSTR-ID-3O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3411-TP-GAR-NULL = HIGH-VALUES
              MOVE L3411-TP-GAR-NULL
                TO WSTR-TP-GAR-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-TP-GAR
                TO WSTR-TP-GAR(IX-TAB-STR)
           END-IF
           IF L3411-TP-RSH-NULL = HIGH-VALUES
              MOVE L3411-TP-RSH-NULL
                TO WSTR-TP-RSH-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-TP-RSH
                TO WSTR-TP-RSH(IX-TAB-STR)
           END-IF
           IF L3411-TP-INVST-NULL = HIGH-VALUES
              MOVE L3411-TP-INVST-NULL
                TO WSTR-TP-INVST-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-TP-INVST
                TO WSTR-TP-INVST(IX-TAB-STR)
           END-IF
           IF L3411-MOD-PAG-GARCOL-NULL = HIGH-VALUES
              MOVE L3411-MOD-PAG-GARCOL-NULL
                TO WSTR-MOD-PAG-GARCOL-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-MOD-PAG-GARCOL
                TO WSTR-MOD-PAG-GARCOL(IX-TAB-STR)
           END-IF
           IF L3411-TP-PER-PRE-NULL = HIGH-VALUES
              MOVE L3411-TP-PER-PRE-NULL
                TO WSTR-TP-PER-PRE-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-TP-PER-PRE
                TO WSTR-TP-PER-PRE(IX-TAB-STR)
           END-IF
           IF L3411-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ETA-AA-1O-ASSTO-NULL
                TO WSTR-ETA-AA-1O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-ETA-AA-1O-ASSTO
                TO WSTR-ETA-AA-1O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3411-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ETA-MM-1O-ASSTO-NULL
                TO WSTR-ETA-MM-1O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-ETA-MM-1O-ASSTO
                TO WSTR-ETA-MM-1O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3411-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ETA-AA-2O-ASSTO-NULL
                TO WSTR-ETA-AA-2O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-ETA-AA-2O-ASSTO
                TO WSTR-ETA-AA-2O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3411-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ETA-MM-2O-ASSTO-NULL
                TO WSTR-ETA-MM-2O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-ETA-MM-2O-ASSTO
                TO WSTR-ETA-MM-2O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3411-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ETA-AA-3O-ASSTO-NULL
                TO WSTR-ETA-AA-3O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-ETA-AA-3O-ASSTO
                TO WSTR-ETA-AA-3O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3411-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ETA-MM-3O-ASSTO-NULL
                TO WSTR-ETA-MM-3O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-ETA-MM-3O-ASSTO
                TO WSTR-ETA-MM-3O-ASSTO(IX-TAB-STR)
           END-IF
           IF L3411-TP-EMIS-PUR-NULL = HIGH-VALUES
              MOVE L3411-TP-EMIS-PUR-NULL
                TO WSTR-TP-EMIS-PUR-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-TP-EMIS-PUR
                TO WSTR-TP-EMIS-PUR(IX-TAB-STR)
           END-IF
           IF L3411-ETA-A-SCAD-NULL = HIGH-VALUES
              MOVE L3411-ETA-A-SCAD-NULL
                TO WSTR-ETA-A-SCAD-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-ETA-A-SCAD
                TO WSTR-ETA-A-SCAD(IX-TAB-STR)
           END-IF
           IF L3411-TP-CALC-PRE-PRSTZ-NULL = HIGH-VALUES
              MOVE L3411-TP-CALC-PRE-PRSTZ-NULL
                TO WSTR-TP-CALC-PRE-PRSTZ-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-TP-CALC-PRE-PRSTZ
                TO WSTR-TP-CALC-PRE-PRSTZ(IX-TAB-STR)
           END-IF
           IF L3411-TP-PRE-NULL = HIGH-VALUES
              MOVE L3411-TP-PRE-NULL
                TO WSTR-TP-PRE-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-TP-PRE
                TO WSTR-TP-PRE(IX-TAB-STR)
           END-IF
           IF L3411-TP-DUR-NULL = HIGH-VALUES
              MOVE L3411-TP-DUR-NULL
                TO WSTR-TP-DUR-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-TP-DUR
                TO WSTR-TP-DUR(IX-TAB-STR)
           END-IF
           IF L3411-DUR-AA-NULL = HIGH-VALUES
              MOVE L3411-DUR-AA-NULL
                TO WSTR-DUR-AA-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-DUR-AA
                TO WSTR-DUR-AA(IX-TAB-STR)
           END-IF
           IF L3411-DUR-MM-NULL = HIGH-VALUES
              MOVE L3411-DUR-MM-NULL
                TO WSTR-DUR-MM-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-DUR-MM
                TO WSTR-DUR-MM(IX-TAB-STR)
           END-IF
           IF L3411-DUR-GG-NULL = HIGH-VALUES
              MOVE L3411-DUR-GG-NULL
                TO WSTR-DUR-GG-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-DUR-GG
                TO WSTR-DUR-GG(IX-TAB-STR)
           END-IF
           IF L3411-NUM-AA-PAG-PRE-NULL = HIGH-VALUES
              MOVE L3411-NUM-AA-PAG-PRE-NULL
                TO WSTR-NUM-AA-PAG-PRE-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-NUM-AA-PAG-PRE
                TO WSTR-NUM-AA-PAG-PRE(IX-TAB-STR)
           END-IF
           IF L3411-AA-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE L3411-AA-PAG-PRE-UNI-NULL
                TO WSTR-AA-PAG-PRE-UNI-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-AA-PAG-PRE-UNI
                TO WSTR-AA-PAG-PRE-UNI(IX-TAB-STR)
           END-IF
           IF L3411-MM-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE L3411-MM-PAG-PRE-UNI-NULL
                TO WSTR-MM-PAG-PRE-UNI-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-MM-PAG-PRE-UNI
                TO WSTR-MM-PAG-PRE-UNI(IX-TAB-STR)
           END-IF
           IF L3411-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
              MOVE L3411-FRAZ-INI-EROG-REN-NULL
                TO WSTR-FRAZ-INI-EROG-REN-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-FRAZ-INI-EROG-REN
                TO WSTR-FRAZ-INI-EROG-REN(IX-TAB-STR)
           END-IF
           IF L3411-MM-1O-RAT-NULL = HIGH-VALUES
              MOVE L3411-MM-1O-RAT-NULL
                TO WSTR-MM-1O-RAT-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-MM-1O-RAT
                TO WSTR-MM-1O-RAT(IX-TAB-STR)
           END-IF
           IF L3411-PC-1O-RAT-NULL = HIGH-VALUES
              MOVE L3411-PC-1O-RAT-NULL
                TO WSTR-PC-1O-RAT-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-PC-1O-RAT
                TO WSTR-PC-1O-RAT(IX-TAB-STR)
           END-IF
           IF L3411-TP-PRSTZ-ASSTA-NULL = HIGH-VALUES
              MOVE L3411-TP-PRSTZ-ASSTA-NULL
                TO WSTR-TP-PRSTZ-ASSTA-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-TP-PRSTZ-ASSTA
                TO WSTR-TP-PRSTZ-ASSTA(IX-TAB-STR)
           END-IF
           IF L3411-DT-END-CARZ-NULL = HIGH-VALUES
              MOVE L3411-DT-END-CARZ-NULL
                TO WSTR-DT-END-CARZ-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-DT-END-CARZ
                TO WSTR-DT-END-CARZ(IX-TAB-STR)
           END-IF
           IF L3411-PC-RIP-PRE-NULL = HIGH-VALUES
              MOVE L3411-PC-RIP-PRE-NULL
                TO WSTR-PC-RIP-PRE-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-PC-RIP-PRE
                TO WSTR-PC-RIP-PRE(IX-TAB-STR)
           END-IF
           IF L3411-COD-FND-NULL = HIGH-VALUES
              MOVE L3411-COD-FND-NULL
                TO WSTR-COD-FND-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-COD-FND
                TO WSTR-COD-FND(IX-TAB-STR)
           END-IF
           IF L3411-AA-REN-CER-NULL = HIGH-VALUES
              MOVE L3411-AA-REN-CER-NULL
                TO WSTR-AA-REN-CER-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-AA-REN-CER
                TO WSTR-AA-REN-CER(IX-TAB-STR)
           END-IF
           IF L3411-PC-REVRSB-NULL = HIGH-VALUES
              MOVE L3411-PC-REVRSB-NULL
                TO WSTR-PC-REVRSB-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-PC-REVRSB
                TO WSTR-PC-REVRSB(IX-TAB-STR)
           END-IF
           IF L3411-TP-PC-RIP-NULL = HIGH-VALUES
              MOVE L3411-TP-PC-RIP-NULL
                TO WSTR-TP-PC-RIP-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-TP-PC-RIP
                TO WSTR-TP-PC-RIP(IX-TAB-STR)
           END-IF
           IF L3411-PC-OPZ-NULL = HIGH-VALUES
              MOVE L3411-PC-OPZ-NULL
                TO WSTR-PC-OPZ-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-PC-OPZ
                TO WSTR-PC-OPZ(IX-TAB-STR)
           END-IF
           IF L3411-TP-IAS-NULL = HIGH-VALUES
              MOVE L3411-TP-IAS-NULL
                TO WSTR-TP-IAS-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-TP-IAS
                TO WSTR-TP-IAS(IX-TAB-STR)
           END-IF
           IF L3411-TP-STAB-NULL = HIGH-VALUES
              MOVE L3411-TP-STAB-NULL
                TO WSTR-TP-STAB-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-TP-STAB
                TO WSTR-TP-STAB(IX-TAB-STR)
           END-IF
           IF L3411-TP-ADEG-PRE-NULL = HIGH-VALUES
              MOVE L3411-TP-ADEG-PRE-NULL
                TO WSTR-TP-ADEG-PRE-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-TP-ADEG-PRE
                TO WSTR-TP-ADEG-PRE(IX-TAB-STR)
           END-IF
           IF L3411-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
              MOVE L3411-DT-VARZ-TP-IAS-NULL
                TO WSTR-DT-VARZ-TP-IAS-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-DT-VARZ-TP-IAS
                TO WSTR-DT-VARZ-TP-IAS(IX-TAB-STR)
           END-IF
           IF L3411-FRAZ-DECR-CPT-NULL = HIGH-VALUES
              MOVE L3411-FRAZ-DECR-CPT-NULL
                TO WSTR-FRAZ-DECR-CPT-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-FRAZ-DECR-CPT
                TO WSTR-FRAZ-DECR-CPT(IX-TAB-STR)
           END-IF
           MOVE L3411-DS-RIGA
             TO WSTR-DS-RIGA(IX-TAB-STR)
           MOVE L3411-DS-OPER-SQL
             TO WSTR-DS-OPER-SQL(IX-TAB-STR)
           MOVE L3411-DS-VER
             TO WSTR-DS-VER(IX-TAB-STR)
           MOVE L3411-DS-TS-INI-CPTZ
             TO WSTR-DS-TS-INI-CPTZ(IX-TAB-STR)
           MOVE L3411-DS-TS-END-CPTZ
             TO WSTR-DS-TS-END-CPTZ(IX-TAB-STR)
           MOVE L3411-DS-UTENTE
             TO WSTR-DS-UTENTE(IX-TAB-STR)
           MOVE L3411-DS-STATO-ELAB
             TO WSTR-DS-STATO-ELAB(IX-TAB-STR)
      *       256 --> Tasso stabilizzazione limitata
           IF L3411-TS-STAB-LIMITATA-NULL = HIGH-VALUES
              MOVE L3411-TS-STAB-LIMITATA-NULL
                TO WSTR-TS-STAB-LIMITATA-NULL(IX-TAB-STR)
           ELSE
              MOVE L3411-TS-STAB-LIMITATA
                TO WSTR-TS-STAB-LIMITATA(IX-TAB-STR)
           END-IF.
ITRFO      IF L3411-RSH-INVST-NULL = HIGH-VALUES
ITRFO         MOVE L3411-RSH-INVST-NULL
ITRFO           TO WSTR-RSH-INVST-NULL(IX-TAB-STR)
ITRFO      ELSE
ITRFO         MOVE L3411-RSH-INVST
ITRFO           TO WSTR-RSH-INVST(IX-TAB-STR)
ITRFO      END-IF
ITRFO      MOVE L3411-TP-RAMO-BILA
ITRFO        TO WSTR-TP-RAMO-BILA(IX-TAB-STR).
      *       256 --> Tasso stabilizzazione limitata fine
       VAL-OUT-ST-GRZ-3410-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       VALORIZZA-OUTPUT-GRZ-3410.

           MOVE L3411-ID-GAR
             TO WGRZ-ID-PTF(IX-TAB-GRZ)
           MOVE L3411-ID-GAR
             TO WGRZ-ID-GAR(IX-TAB-GRZ)
           IF L3411-ID-ADES-NULL = HIGH-VALUES
              MOVE L3411-ID-ADES-NULL
                TO WGRZ-ID-ADES-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-ID-ADES
                TO WGRZ-ID-ADES(IX-TAB-GRZ)
           END-IF
           MOVE L3411-ID-POLI
             TO WGRZ-ID-POLI(IX-TAB-GRZ)
           MOVE L3411-ID-MOVI-CRZ
             TO WGRZ-ID-MOVI-CRZ(IX-TAB-GRZ)
           IF L3411-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE L3411-ID-MOVI-CHIU-NULL
                TO WGRZ-ID-MOVI-CHIU-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-ID-MOVI-CHIU
                TO WGRZ-ID-MOVI-CHIU(IX-TAB-GRZ)
           END-IF
           MOVE L3411-DT-INI-EFF
             TO WGRZ-DT-INI-EFF(IX-TAB-GRZ)
           MOVE L3411-DT-END-EFF
             TO WGRZ-DT-END-EFF(IX-TAB-GRZ)
           MOVE L3411-COD-COMP-ANIA
             TO WGRZ-COD-COMP-ANIA(IX-TAB-GRZ)
           IF L3411-IB-OGG-NULL = HIGH-VALUES
              MOVE L3411-IB-OGG-NULL
                TO WGRZ-IB-OGG-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-IB-OGG
                TO WGRZ-IB-OGG(IX-TAB-GRZ)
           END-IF
           IF L3411-DT-DECOR-NULL = HIGH-VALUES
              MOVE L3411-DT-DECOR-NULL
                TO WGRZ-DT-DECOR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-DT-DECOR
                TO WGRZ-DT-DECOR(IX-TAB-GRZ)
           END-IF
           IF L3411-DT-SCAD-NULL = HIGH-VALUES
              MOVE L3411-DT-SCAD-NULL
                TO WGRZ-DT-SCAD-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-DT-SCAD
                TO WGRZ-DT-SCAD(IX-TAB-GRZ)
           END-IF
           IF L3411-COD-SEZ-NULL = HIGH-VALUES
              MOVE L3411-COD-SEZ-NULL
                TO WGRZ-COD-SEZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-COD-SEZ
                TO WGRZ-COD-SEZ(IX-TAB-GRZ)
           END-IF
           MOVE L3411-COD-TARI
             TO WGRZ-COD-TARI(IX-TAB-GRZ)
           IF L3411-RAMO-BILA-NULL = HIGH-VALUES
              MOVE L3411-RAMO-BILA-NULL
                TO WGRZ-RAMO-BILA-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-RAMO-BILA
                TO WGRZ-RAMO-BILA(IX-TAB-GRZ)
           END-IF
           IF L3411-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE L3411-DT-INI-VAL-TAR-NULL
                TO WGRZ-DT-INI-VAL-TAR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-DT-INI-VAL-TAR
                TO WGRZ-DT-INI-VAL-TAR(IX-TAB-GRZ)
           END-IF
           IF L3411-ID-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ID-1O-ASSTO-NULL
                TO WGRZ-ID-1O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-ID-1O-ASSTO
                TO WGRZ-ID-1O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3411-ID-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ID-2O-ASSTO-NULL
                TO WGRZ-ID-2O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-ID-2O-ASSTO
                TO WGRZ-ID-2O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3411-ID-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ID-3O-ASSTO-NULL
                TO WGRZ-ID-3O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-ID-3O-ASSTO
                TO WGRZ-ID-3O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3411-TP-GAR-NULL = HIGH-VALUES
              MOVE L3411-TP-GAR-NULL
                TO WGRZ-TP-GAR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-TP-GAR
                TO WGRZ-TP-GAR(IX-TAB-GRZ)
           END-IF
           IF L3411-TP-RSH-NULL = HIGH-VALUES
              MOVE L3411-TP-RSH-NULL
                TO WGRZ-TP-RSH-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-TP-RSH
                TO WGRZ-TP-RSH(IX-TAB-GRZ)
           END-IF
           IF L3411-TP-INVST-NULL = HIGH-VALUES
              MOVE L3411-TP-INVST-NULL
                TO WGRZ-TP-INVST-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-TP-INVST
                TO WGRZ-TP-INVST(IX-TAB-GRZ)
           END-IF
           IF L3411-MOD-PAG-GARCOL-NULL = HIGH-VALUES
              MOVE L3411-MOD-PAG-GARCOL-NULL
                TO WGRZ-MOD-PAG-GARCOL-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-MOD-PAG-GARCOL
                TO WGRZ-MOD-PAG-GARCOL(IX-TAB-GRZ)
           END-IF
           IF L3411-TP-PER-PRE-NULL = HIGH-VALUES
              MOVE L3411-TP-PER-PRE-NULL
                TO WGRZ-TP-PER-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-TP-PER-PRE
                TO WGRZ-TP-PER-PRE(IX-TAB-GRZ)
           END-IF
           IF L3411-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ETA-AA-1O-ASSTO-NULL
                TO WGRZ-ETA-AA-1O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-ETA-AA-1O-ASSTO
                TO WGRZ-ETA-AA-1O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3411-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ETA-MM-1O-ASSTO-NULL
                TO WGRZ-ETA-MM-1O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-ETA-MM-1O-ASSTO
                TO WGRZ-ETA-MM-1O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3411-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ETA-AA-2O-ASSTO-NULL
                TO WGRZ-ETA-AA-2O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-ETA-AA-2O-ASSTO
                TO WGRZ-ETA-AA-2O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3411-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ETA-MM-2O-ASSTO-NULL
                TO WGRZ-ETA-MM-2O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-ETA-MM-2O-ASSTO
                TO WGRZ-ETA-MM-2O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3411-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ETA-AA-3O-ASSTO-NULL
                TO WGRZ-ETA-AA-3O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-ETA-AA-3O-ASSTO
                TO WGRZ-ETA-AA-3O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3411-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L3411-ETA-MM-3O-ASSTO-NULL
                TO WGRZ-ETA-MM-3O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-ETA-MM-3O-ASSTO
                TO WGRZ-ETA-MM-3O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3411-TP-EMIS-PUR-NULL = HIGH-VALUES
              MOVE L3411-TP-EMIS-PUR-NULL
                TO WGRZ-TP-EMIS-PUR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-TP-EMIS-PUR
                TO WGRZ-TP-EMIS-PUR(IX-TAB-GRZ)
           END-IF
           IF L3411-ETA-A-SCAD-NULL = HIGH-VALUES
              MOVE L3411-ETA-A-SCAD-NULL
                TO WGRZ-ETA-A-SCAD-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-ETA-A-SCAD
                TO WGRZ-ETA-A-SCAD(IX-TAB-GRZ)
           END-IF
           IF L3411-TP-CALC-PRE-PRSTZ-NULL = HIGH-VALUES
              MOVE L3411-TP-CALC-PRE-PRSTZ-NULL
                TO WGRZ-TP-CALC-PRE-PRSTZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-TP-CALC-PRE-PRSTZ
                TO WGRZ-TP-CALC-PRE-PRSTZ(IX-TAB-GRZ)
           END-IF
           IF L3411-TP-PRE-NULL = HIGH-VALUES
              MOVE L3411-TP-PRE-NULL
                TO WGRZ-TP-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-TP-PRE
                TO WGRZ-TP-PRE(IX-TAB-GRZ)
           END-IF
           IF L3411-TP-DUR-NULL = HIGH-VALUES
              MOVE L3411-TP-DUR-NULL
                TO WGRZ-TP-DUR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-TP-DUR
                TO WGRZ-TP-DUR(IX-TAB-GRZ)
           END-IF
           IF L3411-DUR-AA-NULL = HIGH-VALUES
              MOVE L3411-DUR-AA-NULL
                TO WGRZ-DUR-AA-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-DUR-AA
                TO WGRZ-DUR-AA(IX-TAB-GRZ)
           END-IF
           IF L3411-DUR-MM-NULL = HIGH-VALUES
              MOVE L3411-DUR-MM-NULL
                TO WGRZ-DUR-MM-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-DUR-MM
                TO WGRZ-DUR-MM(IX-TAB-GRZ)
           END-IF
           IF L3411-DUR-GG-NULL = HIGH-VALUES
              MOVE L3411-DUR-GG-NULL
                TO WGRZ-DUR-GG-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-DUR-GG
                TO WGRZ-DUR-GG(IX-TAB-GRZ)
           END-IF
           IF L3411-NUM-AA-PAG-PRE-NULL = HIGH-VALUES
              MOVE L3411-NUM-AA-PAG-PRE-NULL
                TO WGRZ-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-NUM-AA-PAG-PRE
                TO WGRZ-NUM-AA-PAG-PRE(IX-TAB-GRZ)
           END-IF
           IF L3411-AA-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE L3411-AA-PAG-PRE-UNI-NULL
                TO WGRZ-AA-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-AA-PAG-PRE-UNI
                TO WGRZ-AA-PAG-PRE-UNI(IX-TAB-GRZ)
           END-IF
           IF L3411-MM-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE L3411-MM-PAG-PRE-UNI-NULL
                TO WGRZ-MM-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-MM-PAG-PRE-UNI
                TO WGRZ-MM-PAG-PRE-UNI(IX-TAB-GRZ)
           END-IF
           IF L3411-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
              MOVE L3411-FRAZ-INI-EROG-REN-NULL
                TO WGRZ-FRAZ-INI-EROG-REN-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-FRAZ-INI-EROG-REN
                TO WGRZ-FRAZ-INI-EROG-REN(IX-TAB-GRZ)
           END-IF
           IF L3411-MM-1O-RAT-NULL = HIGH-VALUES
              MOVE L3411-MM-1O-RAT-NULL
                TO WGRZ-MM-1O-RAT-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-MM-1O-RAT
                TO WGRZ-MM-1O-RAT(IX-TAB-GRZ)
           END-IF
           IF L3411-PC-1O-RAT-NULL = HIGH-VALUES
              MOVE L3411-PC-1O-RAT-NULL
                TO WGRZ-PC-1O-RAT-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-PC-1O-RAT
                TO WGRZ-PC-1O-RAT(IX-TAB-GRZ)
           END-IF
           IF L3411-TP-PRSTZ-ASSTA-NULL = HIGH-VALUES
              MOVE L3411-TP-PRSTZ-ASSTA-NULL
                TO WGRZ-TP-PRSTZ-ASSTA-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-TP-PRSTZ-ASSTA
                TO WGRZ-TP-PRSTZ-ASSTA(IX-TAB-GRZ)
           END-IF
           IF L3411-DT-END-CARZ-NULL = HIGH-VALUES
              MOVE L3411-DT-END-CARZ-NULL
                TO WGRZ-DT-END-CARZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-DT-END-CARZ
                TO WGRZ-DT-END-CARZ(IX-TAB-GRZ)
           END-IF
           IF L3411-PC-RIP-PRE-NULL = HIGH-VALUES
              MOVE L3411-PC-RIP-PRE-NULL
                TO WGRZ-PC-RIP-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-PC-RIP-PRE
                TO WGRZ-PC-RIP-PRE(IX-TAB-GRZ)
           END-IF
           IF L3411-COD-FND-NULL = HIGH-VALUES
              MOVE L3411-COD-FND-NULL
                TO WGRZ-COD-FND-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-COD-FND
                TO WGRZ-COD-FND(IX-TAB-GRZ)
           END-IF
           IF L3411-AA-REN-CER-NULL = HIGH-VALUES
              MOVE L3411-AA-REN-CER-NULL
                TO WGRZ-AA-REN-CER-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-AA-REN-CER
                TO WGRZ-AA-REN-CER(IX-TAB-GRZ)
           END-IF
           IF L3411-PC-REVRSB-NULL = HIGH-VALUES
              MOVE L3411-PC-REVRSB-NULL
                TO WGRZ-PC-REVRSB-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-PC-REVRSB
                TO WGRZ-PC-REVRSB(IX-TAB-GRZ)
           END-IF
           IF L3411-TP-PC-RIP-NULL = HIGH-VALUES
              MOVE L3411-TP-PC-RIP-NULL
                TO WGRZ-TP-PC-RIP-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-TP-PC-RIP
                TO WGRZ-TP-PC-RIP(IX-TAB-GRZ)
           END-IF
           IF L3411-PC-OPZ-NULL = HIGH-VALUES
              MOVE L3411-PC-OPZ-NULL
                TO WGRZ-PC-OPZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-PC-OPZ
                TO WGRZ-PC-OPZ(IX-TAB-GRZ)
           END-IF
           IF L3411-TP-IAS-NULL = HIGH-VALUES
              MOVE L3411-TP-IAS-NULL
                TO WGRZ-TP-IAS-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-TP-IAS
                TO WGRZ-TP-IAS(IX-TAB-GRZ)
           END-IF
           IF L3411-TP-STAB-NULL = HIGH-VALUES
              MOVE L3411-TP-STAB-NULL
                TO WGRZ-TP-STAB-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-TP-STAB
                TO WGRZ-TP-STAB(IX-TAB-GRZ)
           END-IF
           IF L3411-TP-ADEG-PRE-NULL = HIGH-VALUES
              MOVE L3411-TP-ADEG-PRE-NULL
                TO WGRZ-TP-ADEG-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-TP-ADEG-PRE
                TO WGRZ-TP-ADEG-PRE(IX-TAB-GRZ)
           END-IF
           IF L3411-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
              MOVE L3411-DT-VARZ-TP-IAS-NULL
                TO WGRZ-DT-VARZ-TP-IAS-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-DT-VARZ-TP-IAS
                TO WGRZ-DT-VARZ-TP-IAS(IX-TAB-GRZ)
           END-IF
           IF L3411-FRAZ-DECR-CPT-NULL = HIGH-VALUES
              MOVE L3411-FRAZ-DECR-CPT-NULL
                TO WGRZ-FRAZ-DECR-CPT-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3411-FRAZ-DECR-CPT
                TO WGRZ-FRAZ-DECR-CPT(IX-TAB-GRZ)
           END-IF
           MOVE L3411-DS-RIGA
             TO WGRZ-DS-RIGA(IX-TAB-GRZ)
           MOVE L3411-DS-OPER-SQL
             TO WGRZ-DS-OPER-SQL(IX-TAB-GRZ)
           MOVE L3411-DS-VER
             TO WGRZ-DS-VER(IX-TAB-GRZ)
           MOVE L3411-DS-TS-INI-CPTZ
             TO WGRZ-DS-TS-INI-CPTZ(IX-TAB-GRZ)
           MOVE L3411-DS-TS-END-CPTZ
             TO WGRZ-DS-TS-END-CPTZ(IX-TAB-GRZ)
           MOVE L3411-DS-UTENTE
             TO WGRZ-DS-UTENTE(IX-TAB-GRZ)
           MOVE L3411-DS-STATO-ELAB
             TO WGRZ-DS-STATO-ELAB(IX-TAB-GRZ)
      *       256 --> Tasso stabilizzazione limitata
           IF L3411-TS-STAB-LIMITATA-NULL = HIGH-VALUES
             MOVE L3411-TS-STAB-LIMITATA-NULL TO
                  WGRZ-TS-STAB-LIMITATA-NULL(IX-TAB-GRZ)
           ELSE
             MOVE L3411-TS-STAB-LIMITATA
                 TO WGRZ-TS-STAB-LIMITATA(IX-TAB-GRZ)
           END-IF.
      *       256 --> Tasso stabilizzazione limitata FINE
ITRFO      IF L3411-RSH-INVST-NULL = HIGH-VALUES
ITRFO         MOVE L3411-RSH-INVST-NULL
ITRFO           TO WGRZ-RSH-INVST-NULL(IX-TAB-GRZ)
ITRFO      ELSE
ITRFO         MOVE L3411-RSH-INVST
ITRFO           TO WGRZ-RSH-INVST(IX-TAB-GRZ)
ITRFO      END-IF
ITRFO      MOVE L3411-TP-RAMO-BILA
ITRFO        TO WGRZ-TP-RAMO-BILA(IX-TAB-GRZ).

       VALORIZZA-OUTPUT-GRZ-3410-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       VALORIZZA-OUTPUT-GRZ-3400.

           MOVE L3401-ID-GAR
             TO WGRZ-ID-PTF(IX-TAB-GRZ)
           MOVE L3401-ID-GAR
             TO WGRZ-ID-GAR(IX-TAB-GRZ)
           IF L3401-ID-ADES-NULL = HIGH-VALUES
              MOVE L3401-ID-ADES-NULL
                TO WGRZ-ID-ADES-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-ID-ADES
                TO WGRZ-ID-ADES(IX-TAB-GRZ)
           END-IF
           MOVE L3401-ID-POLI
             TO WGRZ-ID-POLI(IX-TAB-GRZ)
           MOVE L3401-ID-MOVI-CRZ
             TO WGRZ-ID-MOVI-CRZ(IX-TAB-GRZ)
           IF L3401-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE L3401-ID-MOVI-CHIU-NULL
                TO WGRZ-ID-MOVI-CHIU-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-ID-MOVI-CHIU
                TO WGRZ-ID-MOVI-CHIU(IX-TAB-GRZ)
           END-IF
           MOVE L3401-DT-INI-EFF
             TO WGRZ-DT-INI-EFF(IX-TAB-GRZ)
           MOVE L3401-DT-END-EFF
             TO WGRZ-DT-END-EFF(IX-TAB-GRZ)
           MOVE L3401-COD-COMP-ANIA
             TO WGRZ-COD-COMP-ANIA(IX-TAB-GRZ)
           IF L3401-IB-OGG-NULL = HIGH-VALUES
              MOVE L3401-IB-OGG-NULL
                TO WGRZ-IB-OGG-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-IB-OGG
                TO WGRZ-IB-OGG(IX-TAB-GRZ)
           END-IF
           IF L3401-DT-DECOR-NULL = HIGH-VALUES
              MOVE L3401-DT-DECOR-NULL
                TO WGRZ-DT-DECOR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-DT-DECOR
                TO WGRZ-DT-DECOR(IX-TAB-GRZ)
           END-IF
           IF L3401-DT-SCAD-NULL = HIGH-VALUES
              MOVE L3401-DT-SCAD-NULL
                TO WGRZ-DT-SCAD-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-DT-SCAD
                TO WGRZ-DT-SCAD(IX-TAB-GRZ)
           END-IF
           IF L3401-COD-SEZ-NULL = HIGH-VALUES
              MOVE L3401-COD-SEZ-NULL
                TO WGRZ-COD-SEZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-COD-SEZ
                TO WGRZ-COD-SEZ(IX-TAB-GRZ)
           END-IF
           MOVE L3401-COD-TARI
             TO WGRZ-COD-TARI(IX-TAB-GRZ)
           IF L3401-RAMO-BILA-NULL = HIGH-VALUES
              MOVE L3401-RAMO-BILA-NULL
                TO WGRZ-RAMO-BILA-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-RAMO-BILA
                TO WGRZ-RAMO-BILA(IX-TAB-GRZ)
           END-IF
           IF L3401-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE L3401-DT-INI-VAL-TAR-NULL
                TO WGRZ-DT-INI-VAL-TAR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-DT-INI-VAL-TAR
                TO WGRZ-DT-INI-VAL-TAR(IX-TAB-GRZ)
           END-IF
           IF L3401-ID-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ID-1O-ASSTO-NULL
                TO WGRZ-ID-1O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-ID-1O-ASSTO
                TO WGRZ-ID-1O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3401-ID-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ID-2O-ASSTO-NULL
                TO WGRZ-ID-2O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-ID-2O-ASSTO
                TO WGRZ-ID-2O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3401-ID-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ID-3O-ASSTO-NULL
                TO WGRZ-ID-3O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-ID-3O-ASSTO
                TO WGRZ-ID-3O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3401-TP-GAR-NULL = HIGH-VALUES
              MOVE L3401-TP-GAR-NULL
                TO WGRZ-TP-GAR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-TP-GAR
                TO WGRZ-TP-GAR(IX-TAB-GRZ)
           END-IF
           IF L3401-TP-RSH-NULL = HIGH-VALUES
              MOVE L3401-TP-RSH-NULL
                TO WGRZ-TP-RSH-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-TP-RSH
                TO WGRZ-TP-RSH(IX-TAB-GRZ)
           END-IF
           IF L3401-TP-INVST-NULL = HIGH-VALUES
              MOVE L3401-TP-INVST-NULL
                TO WGRZ-TP-INVST-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-TP-INVST
                TO WGRZ-TP-INVST(IX-TAB-GRZ)
           END-IF
           IF L3401-MOD-PAG-GARCOL-NULL = HIGH-VALUES
              MOVE L3401-MOD-PAG-GARCOL-NULL
                TO WGRZ-MOD-PAG-GARCOL-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-MOD-PAG-GARCOL
                TO WGRZ-MOD-PAG-GARCOL(IX-TAB-GRZ)
           END-IF
           IF L3401-TP-PER-PRE-NULL = HIGH-VALUES
              MOVE L3401-TP-PER-PRE-NULL
                TO WGRZ-TP-PER-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-TP-PER-PRE
                TO WGRZ-TP-PER-PRE(IX-TAB-GRZ)
           END-IF
           IF L3401-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ETA-AA-1O-ASSTO-NULL
                TO WGRZ-ETA-AA-1O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-ETA-AA-1O-ASSTO
                TO WGRZ-ETA-AA-1O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3401-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ETA-MM-1O-ASSTO-NULL
                TO WGRZ-ETA-MM-1O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-ETA-MM-1O-ASSTO
                TO WGRZ-ETA-MM-1O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3401-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ETA-AA-2O-ASSTO-NULL
                TO WGRZ-ETA-AA-2O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-ETA-AA-2O-ASSTO
                TO WGRZ-ETA-AA-2O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3401-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ETA-MM-2O-ASSTO-NULL
                TO WGRZ-ETA-MM-2O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-ETA-MM-2O-ASSTO
                TO WGRZ-ETA-MM-2O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3401-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ETA-AA-3O-ASSTO-NULL
                TO WGRZ-ETA-AA-3O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-ETA-AA-3O-ASSTO
                TO WGRZ-ETA-AA-3O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3401-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L3401-ETA-MM-3O-ASSTO-NULL
                TO WGRZ-ETA-MM-3O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-ETA-MM-3O-ASSTO
                TO WGRZ-ETA-MM-3O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L3401-TP-EMIS-PUR-NULL = HIGH-VALUES
              MOVE L3401-TP-EMIS-PUR-NULL
                TO WGRZ-TP-EMIS-PUR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-TP-EMIS-PUR
                TO WGRZ-TP-EMIS-PUR(IX-TAB-GRZ)
           END-IF
           IF L3401-ETA-A-SCAD-NULL = HIGH-VALUES
              MOVE L3401-ETA-A-SCAD-NULL
                TO WGRZ-ETA-A-SCAD-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-ETA-A-SCAD
                TO WGRZ-ETA-A-SCAD(IX-TAB-GRZ)
           END-IF
           IF L3401-TP-CALC-PRE-PRSTZ-NULL = HIGH-VALUES
              MOVE L3401-TP-CALC-PRE-PRSTZ-NULL
                TO WGRZ-TP-CALC-PRE-PRSTZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-TP-CALC-PRE-PRSTZ
                TO WGRZ-TP-CALC-PRE-PRSTZ(IX-TAB-GRZ)
           END-IF
           IF L3401-TP-PRE-NULL = HIGH-VALUES
              MOVE L3401-TP-PRE-NULL
                TO WGRZ-TP-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-TP-PRE
                TO WGRZ-TP-PRE(IX-TAB-GRZ)
           END-IF
           IF L3401-TP-DUR-NULL = HIGH-VALUES
              MOVE L3401-TP-DUR-NULL
                TO WGRZ-TP-DUR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-TP-DUR
                TO WGRZ-TP-DUR(IX-TAB-GRZ)
           END-IF
           IF L3401-DUR-AA-NULL = HIGH-VALUES
              MOVE L3401-DUR-AA-NULL
                TO WGRZ-DUR-AA-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-DUR-AA
                TO WGRZ-DUR-AA(IX-TAB-GRZ)
           END-IF
           IF L3401-DUR-MM-NULL = HIGH-VALUES
              MOVE L3401-DUR-MM-NULL
                TO WGRZ-DUR-MM-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-DUR-MM
                TO WGRZ-DUR-MM(IX-TAB-GRZ)
           END-IF
           IF L3401-DUR-GG-NULL = HIGH-VALUES
              MOVE L3401-DUR-GG-NULL
                TO WGRZ-DUR-GG-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-DUR-GG
                TO WGRZ-DUR-GG(IX-TAB-GRZ)
           END-IF
           IF L3401-NUM-AA-PAG-PRE-NULL = HIGH-VALUES
              MOVE L3401-NUM-AA-PAG-PRE-NULL
                TO WGRZ-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-NUM-AA-PAG-PRE
                TO WGRZ-NUM-AA-PAG-PRE(IX-TAB-GRZ)
           END-IF
           IF L3401-AA-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE L3401-AA-PAG-PRE-UNI-NULL
                TO WGRZ-AA-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-AA-PAG-PRE-UNI
                TO WGRZ-AA-PAG-PRE-UNI(IX-TAB-GRZ)
           END-IF
           IF L3401-MM-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE L3401-MM-PAG-PRE-UNI-NULL
                TO WGRZ-MM-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-MM-PAG-PRE-UNI
                TO WGRZ-MM-PAG-PRE-UNI(IX-TAB-GRZ)
           END-IF
           IF L3401-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
              MOVE L3401-FRAZ-INI-EROG-REN-NULL
                TO WGRZ-FRAZ-INI-EROG-REN-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-FRAZ-INI-EROG-REN
                TO WGRZ-FRAZ-INI-EROG-REN(IX-TAB-GRZ)
           END-IF
           IF L3401-MM-1O-RAT-NULL = HIGH-VALUES
              MOVE L3401-MM-1O-RAT-NULL
                TO WGRZ-MM-1O-RAT-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-MM-1O-RAT
                TO WGRZ-MM-1O-RAT(IX-TAB-GRZ)
           END-IF
           IF L3401-PC-1O-RAT-NULL = HIGH-VALUES
              MOVE L3401-PC-1O-RAT-NULL
                TO WGRZ-PC-1O-RAT-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-PC-1O-RAT
                TO WGRZ-PC-1O-RAT(IX-TAB-GRZ)
           END-IF
           IF L3401-TP-PRSTZ-ASSTA-NULL = HIGH-VALUES
              MOVE L3401-TP-PRSTZ-ASSTA-NULL
                TO WGRZ-TP-PRSTZ-ASSTA-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-TP-PRSTZ-ASSTA
                TO WGRZ-TP-PRSTZ-ASSTA(IX-TAB-GRZ)
           END-IF
           IF L3401-DT-END-CARZ-NULL = HIGH-VALUES
              MOVE L3401-DT-END-CARZ-NULL
                TO WGRZ-DT-END-CARZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-DT-END-CARZ
                TO WGRZ-DT-END-CARZ(IX-TAB-GRZ)
           END-IF
           IF L3401-PC-RIP-PRE-NULL = HIGH-VALUES
              MOVE L3401-PC-RIP-PRE-NULL
                TO WGRZ-PC-RIP-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-PC-RIP-PRE
                TO WGRZ-PC-RIP-PRE(IX-TAB-GRZ)
           END-IF
           IF L3401-COD-FND-NULL = HIGH-VALUES
              MOVE L3401-COD-FND-NULL
                TO WGRZ-COD-FND-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-COD-FND
                TO WGRZ-COD-FND(IX-TAB-GRZ)
           END-IF
           IF L3401-AA-REN-CER-NULL = HIGH-VALUES
              MOVE L3401-AA-REN-CER-NULL
                TO WGRZ-AA-REN-CER-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-AA-REN-CER
                TO WGRZ-AA-REN-CER(IX-TAB-GRZ)
           END-IF
           IF L3401-PC-REVRSB-NULL = HIGH-VALUES
              MOVE L3401-PC-REVRSB-NULL
                TO WGRZ-PC-REVRSB-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-PC-REVRSB
                TO WGRZ-PC-REVRSB(IX-TAB-GRZ)
           END-IF
           IF L3401-TP-PC-RIP-NULL = HIGH-VALUES
              MOVE L3401-TP-PC-RIP-NULL
                TO WGRZ-TP-PC-RIP-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-TP-PC-RIP
                TO WGRZ-TP-PC-RIP(IX-TAB-GRZ)
           END-IF
           IF L3401-PC-OPZ-NULL = HIGH-VALUES
              MOVE L3401-PC-OPZ-NULL
                TO WGRZ-PC-OPZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-PC-OPZ
                TO WGRZ-PC-OPZ(IX-TAB-GRZ)
           END-IF
           IF L3401-TP-IAS-NULL = HIGH-VALUES
              MOVE L3401-TP-IAS-NULL
                TO WGRZ-TP-IAS-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-TP-IAS
                TO WGRZ-TP-IAS(IX-TAB-GRZ)
           END-IF
           IF L3401-TP-STAB-NULL = HIGH-VALUES
              MOVE L3401-TP-STAB-NULL
                TO WGRZ-TP-STAB-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-TP-STAB
                TO WGRZ-TP-STAB(IX-TAB-GRZ)
           END-IF
           IF L3401-TP-ADEG-PRE-NULL = HIGH-VALUES
              MOVE L3401-TP-ADEG-PRE-NULL
                TO WGRZ-TP-ADEG-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-TP-ADEG-PRE
                TO WGRZ-TP-ADEG-PRE(IX-TAB-GRZ)
           END-IF
           IF L3401-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
              MOVE L3401-DT-VARZ-TP-IAS-NULL
                TO WGRZ-DT-VARZ-TP-IAS-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-DT-VARZ-TP-IAS
                TO WGRZ-DT-VARZ-TP-IAS(IX-TAB-GRZ)
           END-IF
           IF L3401-FRAZ-DECR-CPT-NULL = HIGH-VALUES
              MOVE L3401-FRAZ-DECR-CPT-NULL
                TO WGRZ-FRAZ-DECR-CPT-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L3401-FRAZ-DECR-CPT
                TO WGRZ-FRAZ-DECR-CPT(IX-TAB-GRZ)
           END-IF
           MOVE L3401-DS-RIGA
             TO WGRZ-DS-RIGA(IX-TAB-GRZ)
           MOVE L3401-DS-OPER-SQL
             TO WGRZ-DS-OPER-SQL(IX-TAB-GRZ)
           MOVE L3401-DS-VER
             TO WGRZ-DS-VER(IX-TAB-GRZ)
           MOVE L3401-DS-TS-INI-CPTZ
             TO WGRZ-DS-TS-INI-CPTZ(IX-TAB-GRZ)
           MOVE L3401-DS-TS-END-CPTZ
             TO WGRZ-DS-TS-END-CPTZ(IX-TAB-GRZ)
           MOVE L3401-DS-UTENTE
             TO WGRZ-DS-UTENTE(IX-TAB-GRZ)
           MOVE L3401-DS-STATO-ELAB
             TO WGRZ-DS-STATO-ELAB(IX-TAB-GRZ)
      *       256 --> Tasso stabilizzazione limitata
           IF L3401-TS-STAB-LIMITATA-NULL = HIGH-VALUES
             MOVE L3401-TS-STAB-LIMITATA-NULL TO
                  WGRZ-TS-STAB-LIMITATA-NULL(IX-TAB-GRZ)
           ELSE
             MOVE L3401-TS-STAB-LIMITATA
                 TO WGRZ-TS-STAB-LIMITATA(IX-TAB-GRZ)
           END-IF.
      *       256 --> Tasso stabilizzazione limitata FINE
ITRFO      IF L3401-RSH-INVST-NULL = HIGH-VALUES
ITRFO         MOVE L3401-RSH-INVST-NULL
ITRFO           TO WGRZ-RSH-INVST-NULL(IX-TAB-GRZ)
ITRFO      ELSE
ITRFO         MOVE L3401-RSH-INVST
ITRFO           TO WGRZ-RSH-INVST(IX-TAB-GRZ)
ITRFO      END-IF
ITRFO      MOVE L3401-TP-RAMO-BILA
ITRFO        TO WGRZ-TP-RAMO-BILA(IX-TAB-GRZ).

       VALORIZZA-OUTPUT-GRZ-3400-EX.
           EXIT.
      *-----------------------------------------------------------------
      *
      *-----------------------------------------------------------------
       VAL-OUT-ST-GRZ-9090.
           MOVE L9091-ID-GAR
             TO WSTR-ID-PTF(IX-TAB-STR)
           MOVE L9091-ID-GAR
             TO WSTR-ID-GAR(IX-TAB-STR)
           IF L9091-ID-ADES-NULL = HIGH-VALUES
              MOVE L9091-ID-ADES-NULL
                TO WSTR-ID-ADES-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-ID-ADES
                TO WSTR-ID-ADES(IX-TAB-STR)
           END-IF
           MOVE L9091-ID-POLI
             TO WSTR-ID-POLI(IX-TAB-STR)
           MOVE L9091-ID-MOVI-CRZ
             TO WSTR-ID-MOVI-CRZ(IX-TAB-STR)
           IF L9091-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE L9091-ID-MOVI-CHIU-NULL
                TO WSTR-ID-MOVI-CHIU-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-ID-MOVI-CHIU
                TO WSTR-ID-MOVI-CHIU(IX-TAB-STR)
           END-IF
           MOVE L9091-DT-INI-EFF
             TO WSTR-DT-INI-EFF(IX-TAB-STR)
           MOVE L9091-DT-END-EFF
             TO WSTR-DT-END-EFF(IX-TAB-STR)
           MOVE L9091-COD-COMP-ANIA
             TO WSTR-COD-COMP-ANIA(IX-TAB-STR)
           IF L9091-IB-OGG-NULL = HIGH-VALUES
              MOVE L9091-IB-OGG-NULL
                TO WSTR-IB-OGG-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-IB-OGG
                TO WSTR-IB-OGG(IX-TAB-STR)
           END-IF
           IF L9091-DT-DECOR-NULL = HIGH-VALUES
              MOVE L9091-DT-DECOR-NULL
                TO WSTR-DT-DECOR-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-DT-DECOR
                TO WSTR-DT-DECOR(IX-TAB-STR)
           END-IF
           IF L9091-DT-SCAD-NULL = HIGH-VALUES
              MOVE L9091-DT-SCAD-NULL
                TO WSTR-DT-SCAD-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-DT-SCAD
                TO WSTR-DT-SCAD(IX-TAB-STR)
           END-IF
           IF L9091-COD-SEZ-NULL = HIGH-VALUES
              MOVE L9091-COD-SEZ-NULL
                TO WSTR-COD-SEZ-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-COD-SEZ
                TO WSTR-COD-SEZ(IX-TAB-STR)
           END-IF
           MOVE L9091-COD-TARI
             TO WSTR-COD-TARI(IX-TAB-STR)
           IF L9091-RAMO-BILA-NULL = HIGH-VALUES
              MOVE L9091-RAMO-BILA-NULL
                TO WSTR-RAMO-BILA-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-RAMO-BILA
                TO WSTR-RAMO-BILA(IX-TAB-STR)
           END-IF
           IF L9091-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE L9091-DT-INI-VAL-TAR-NULL
                TO WSTR-DT-INI-VAL-TAR-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-DT-INI-VAL-TAR
                TO WSTR-DT-INI-VAL-TAR(IX-TAB-STR)
           END-IF
           IF L9091-ID-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ID-1O-ASSTO-NULL
                TO WSTR-ID-1O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-ID-1O-ASSTO
                TO WSTR-ID-1O-ASSTO(IX-TAB-STR)
           END-IF
           IF L9091-ID-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ID-2O-ASSTO-NULL
                TO WSTR-ID-2O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-ID-2O-ASSTO
                TO WSTR-ID-2O-ASSTO(IX-TAB-STR)
           END-IF
           IF L9091-ID-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ID-3O-ASSTO-NULL
                TO WSTR-ID-3O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-ID-3O-ASSTO
                TO WSTR-ID-3O-ASSTO(IX-TAB-STR)
           END-IF
           IF L9091-TP-GAR-NULL = HIGH-VALUES
              MOVE L9091-TP-GAR-NULL
                TO WSTR-TP-GAR-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-TP-GAR
                TO WSTR-TP-GAR(IX-TAB-STR)
           END-IF
           IF L9091-TP-RSH-NULL = HIGH-VALUES
              MOVE L9091-TP-RSH-NULL
                TO WSTR-TP-RSH-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-TP-RSH
                TO WSTR-TP-RSH(IX-TAB-STR)
           END-IF
           IF L9091-TP-INVST-NULL = HIGH-VALUES
              MOVE L9091-TP-INVST-NULL
                TO WSTR-TP-INVST-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-TP-INVST
                TO WSTR-TP-INVST(IX-TAB-STR)
           END-IF
           IF L9091-MOD-PAG-GARCOL-NULL = HIGH-VALUES
              MOVE L9091-MOD-PAG-GARCOL-NULL
                TO WSTR-MOD-PAG-GARCOL-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-MOD-PAG-GARCOL
                TO WSTR-MOD-PAG-GARCOL(IX-TAB-STR)
           END-IF
           IF L9091-TP-PER-PRE-NULL = HIGH-VALUES
              MOVE L9091-TP-PER-PRE-NULL
                TO WSTR-TP-PER-PRE-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-TP-PER-PRE
                TO WSTR-TP-PER-PRE(IX-TAB-STR)
           END-IF
           IF L9091-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ETA-AA-1O-ASSTO-NULL
                TO WSTR-ETA-AA-1O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-ETA-AA-1O-ASSTO
                TO WSTR-ETA-AA-1O-ASSTO(IX-TAB-STR)
           END-IF
           IF L9091-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ETA-MM-1O-ASSTO-NULL
                TO WSTR-ETA-MM-1O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-ETA-MM-1O-ASSTO
                TO WSTR-ETA-MM-1O-ASSTO(IX-TAB-STR)
           END-IF
           IF L9091-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ETA-AA-2O-ASSTO-NULL
                TO WSTR-ETA-AA-2O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-ETA-AA-2O-ASSTO
                TO WSTR-ETA-AA-2O-ASSTO(IX-TAB-STR)
           END-IF
           IF L9091-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ETA-MM-2O-ASSTO-NULL
                TO WSTR-ETA-MM-2O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-ETA-MM-2O-ASSTO
                TO WSTR-ETA-MM-2O-ASSTO(IX-TAB-STR)
           END-IF
           IF L9091-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ETA-AA-3O-ASSTO-NULL
                TO WSTR-ETA-AA-3O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-ETA-AA-3O-ASSTO
                TO WSTR-ETA-AA-3O-ASSTO(IX-TAB-STR)
           END-IF
           IF L9091-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ETA-MM-3O-ASSTO-NULL
                TO WSTR-ETA-MM-3O-ASSTO-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-ETA-MM-3O-ASSTO
                TO WSTR-ETA-MM-3O-ASSTO(IX-TAB-STR)
           END-IF
           IF L9091-TP-EMIS-PUR-NULL = HIGH-VALUES
              MOVE L9091-TP-EMIS-PUR-NULL
                TO WSTR-TP-EMIS-PUR-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-TP-EMIS-PUR
                TO WSTR-TP-EMIS-PUR(IX-TAB-STR)
           END-IF
           IF L9091-ETA-A-SCAD-NULL = HIGH-VALUES
              MOVE L9091-ETA-A-SCAD-NULL
                TO WSTR-ETA-A-SCAD-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-ETA-A-SCAD
                TO WSTR-ETA-A-SCAD(IX-TAB-STR)
           END-IF
           IF L9091-TP-CALC-PRE-PRSTZ-NULL = HIGH-VALUES
              MOVE L9091-TP-CALC-PRE-PRSTZ-NULL
                TO WSTR-TP-CALC-PRE-PRSTZ-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-TP-CALC-PRE-PRSTZ
                TO WSTR-TP-CALC-PRE-PRSTZ(IX-TAB-STR)
           END-IF
           IF L9091-TP-PRE-NULL = HIGH-VALUES
              MOVE L9091-TP-PRE-NULL
                TO WSTR-TP-PRE-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-TP-PRE
                TO WSTR-TP-PRE(IX-TAB-STR)
           END-IF
           IF L9091-TP-DUR-NULL = HIGH-VALUES
              MOVE L9091-TP-DUR-NULL
                TO WSTR-TP-DUR-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-TP-DUR
                TO WSTR-TP-DUR(IX-TAB-STR)
           END-IF
           IF L9091-DUR-AA-NULL = HIGH-VALUES
              MOVE L9091-DUR-AA-NULL
                TO WSTR-DUR-AA-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-DUR-AA
                TO WSTR-DUR-AA(IX-TAB-STR)
           END-IF
           IF L9091-DUR-MM-NULL = HIGH-VALUES
              MOVE L9091-DUR-MM-NULL
                TO WSTR-DUR-MM-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-DUR-MM
                TO WSTR-DUR-MM(IX-TAB-STR)
           END-IF
           IF L9091-DUR-GG-NULL = HIGH-VALUES
              MOVE L9091-DUR-GG-NULL
                TO WSTR-DUR-GG-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-DUR-GG
                TO WSTR-DUR-GG(IX-TAB-STR)
           END-IF
           IF L9091-NUM-AA-PAG-PRE-NULL = HIGH-VALUES
              MOVE L9091-NUM-AA-PAG-PRE-NULL
                TO WSTR-NUM-AA-PAG-PRE-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-NUM-AA-PAG-PRE
                TO WSTR-NUM-AA-PAG-PRE(IX-TAB-STR)
           END-IF
           IF L9091-AA-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE L9091-AA-PAG-PRE-UNI-NULL
                TO WSTR-AA-PAG-PRE-UNI-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-AA-PAG-PRE-UNI
                TO WSTR-AA-PAG-PRE-UNI(IX-TAB-STR)
           END-IF
           IF L9091-MM-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE L9091-MM-PAG-PRE-UNI-NULL
                TO WSTR-MM-PAG-PRE-UNI-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-MM-PAG-PRE-UNI
                TO WSTR-MM-PAG-PRE-UNI(IX-TAB-STR)
           END-IF
           IF L9091-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
              MOVE L9091-FRAZ-INI-EROG-REN-NULL
                TO WSTR-FRAZ-INI-EROG-REN-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-FRAZ-INI-EROG-REN
                TO WSTR-FRAZ-INI-EROG-REN(IX-TAB-STR)
           END-IF
           IF L9091-MM-1O-RAT-NULL = HIGH-VALUES
              MOVE L9091-MM-1O-RAT-NULL
                TO WSTR-MM-1O-RAT-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-MM-1O-RAT
                TO WSTR-MM-1O-RAT(IX-TAB-STR)
           END-IF
           IF L9091-PC-1O-RAT-NULL = HIGH-VALUES
              MOVE L9091-PC-1O-RAT-NULL
                TO WSTR-PC-1O-RAT-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-PC-1O-RAT
                TO WSTR-PC-1O-RAT(IX-TAB-STR)
           END-IF
           IF L9091-TP-PRSTZ-ASSTA-NULL = HIGH-VALUES
              MOVE L9091-TP-PRSTZ-ASSTA-NULL
                TO WSTR-TP-PRSTZ-ASSTA-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-TP-PRSTZ-ASSTA
                TO WSTR-TP-PRSTZ-ASSTA(IX-TAB-STR)
           END-IF
           IF L9091-DT-END-CARZ-NULL = HIGH-VALUES
              MOVE L9091-DT-END-CARZ-NULL
                TO WSTR-DT-END-CARZ-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-DT-END-CARZ
                TO WSTR-DT-END-CARZ(IX-TAB-STR)
           END-IF
           IF L9091-PC-RIP-PRE-NULL = HIGH-VALUES
              MOVE L9091-PC-RIP-PRE-NULL
                TO WSTR-PC-RIP-PRE-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-PC-RIP-PRE
                TO WSTR-PC-RIP-PRE(IX-TAB-STR)
           END-IF
           IF L9091-COD-FND-NULL = HIGH-VALUES
              MOVE L9091-COD-FND-NULL
                TO WSTR-COD-FND-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-COD-FND
                TO WSTR-COD-FND(IX-TAB-STR)
           END-IF
           IF L9091-AA-REN-CER-NULL = HIGH-VALUES
              MOVE L9091-AA-REN-CER-NULL
                TO WSTR-AA-REN-CER-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-AA-REN-CER
                TO WSTR-AA-REN-CER(IX-TAB-STR)
           END-IF
           IF L9091-PC-REVRSB-NULL = HIGH-VALUES
              MOVE L9091-PC-REVRSB-NULL
                TO WSTR-PC-REVRSB-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-PC-REVRSB
                TO WSTR-PC-REVRSB(IX-TAB-STR)
           END-IF
           IF L9091-TP-PC-RIP-NULL = HIGH-VALUES
              MOVE L9091-TP-PC-RIP-NULL
                TO WSTR-TP-PC-RIP-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-TP-PC-RIP
                TO WSTR-TP-PC-RIP(IX-TAB-STR)
           END-IF
           IF L9091-PC-OPZ-NULL = HIGH-VALUES
              MOVE L9091-PC-OPZ-NULL
                TO WSTR-PC-OPZ-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-PC-OPZ
                TO WSTR-PC-OPZ(IX-TAB-STR)
           END-IF
           IF L9091-TP-IAS-NULL = HIGH-VALUES
              MOVE L9091-TP-IAS-NULL
                TO WSTR-TP-IAS-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-TP-IAS
                TO WSTR-TP-IAS(IX-TAB-STR)
           END-IF
           IF L9091-TP-STAB-NULL = HIGH-VALUES
              MOVE L9091-TP-STAB-NULL
                TO WSTR-TP-STAB-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-TP-STAB
                TO WSTR-TP-STAB(IX-TAB-STR)
           END-IF
           IF L9091-TP-ADEG-PRE-NULL = HIGH-VALUES
              MOVE L9091-TP-ADEG-PRE-NULL
                TO WSTR-TP-ADEG-PRE-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-TP-ADEG-PRE
                TO WSTR-TP-ADEG-PRE(IX-TAB-STR)
           END-IF
           IF L9091-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
              MOVE L9091-DT-VARZ-TP-IAS-NULL
                TO WSTR-DT-VARZ-TP-IAS-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-DT-VARZ-TP-IAS
                TO WSTR-DT-VARZ-TP-IAS(IX-TAB-STR)
           END-IF
           IF L9091-FRAZ-DECR-CPT-NULL = HIGH-VALUES
              MOVE L9091-FRAZ-DECR-CPT-NULL
                TO WSTR-FRAZ-DECR-CPT-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-FRAZ-DECR-CPT
                TO WSTR-FRAZ-DECR-CPT(IX-TAB-STR)
           END-IF
           MOVE L9091-DS-RIGA
             TO WSTR-DS-RIGA(IX-TAB-STR)
           MOVE L9091-DS-OPER-SQL
             TO WSTR-DS-OPER-SQL(IX-TAB-STR)
           MOVE L9091-DS-VER
             TO WSTR-DS-VER(IX-TAB-STR)
           MOVE L9091-DS-TS-INI-CPTZ
             TO WSTR-DS-TS-INI-CPTZ(IX-TAB-STR)
           MOVE L9091-DS-TS-END-CPTZ
             TO WSTR-DS-TS-END-CPTZ(IX-TAB-STR)
           MOVE L9091-DS-UTENTE
             TO WSTR-DS-UTENTE(IX-TAB-STR)
           MOVE L9091-DS-STATO-ELAB
             TO WSTR-DS-STATO-ELAB(IX-TAB-STR)
      *       256 --> Tasso stabilizzazione limitata
           IF L9091-TS-STAB-LIMITATA-NULL = HIGH-VALUES
              MOVE L9091-TS-STAB-LIMITATA-NULL
                TO WSTR-TS-STAB-LIMITATA-NULL(IX-TAB-STR)
           ELSE
              MOVE L9091-TS-STAB-LIMITATA
                TO WSTR-TS-STAB-LIMITATA(IX-TAB-STR)
           END-IF.
ITRFO      IF L9091-RSH-INVST-NULL = HIGH-VALUES
ITRFO         MOVE L9091-RSH-INVST-NULL
ITRFO           TO WSTR-RSH-INVST-NULL(IX-TAB-STR)
ITRFO      ELSE
ITRFO         MOVE L9091-RSH-INVST
ITRFO           TO WSTR-RSH-INVST(IX-TAB-STR)
ITRFO      END-IF
ITRFO      MOVE L9091-TP-RAMO-BILA
ITRFO        TO WSTR-TP-RAMO-BILA(IX-TAB-STR).
      *       256 --> Tasso stabilizzazione limitata fine
       VAL-OUT-ST-GRZ-9090-EX.
           EXIT.

      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       VALORIZZA-OUTPUT-GRZ-9090.

           MOVE L9091-ID-GAR
             TO WGRZ-ID-PTF(IX-TAB-GRZ)
           MOVE L9091-ID-GAR
             TO WGRZ-ID-GAR(IX-TAB-GRZ)
           IF L9091-ID-ADES-NULL = HIGH-VALUES
              MOVE L9091-ID-ADES-NULL
                TO WGRZ-ID-ADES-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-ID-ADES
                TO WGRZ-ID-ADES(IX-TAB-GRZ)
           END-IF
           MOVE L9091-ID-POLI
             TO WGRZ-ID-POLI(IX-TAB-GRZ)
           MOVE L9091-ID-MOVI-CRZ
             TO WGRZ-ID-MOVI-CRZ(IX-TAB-GRZ)
           IF L9091-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE L9091-ID-MOVI-CHIU-NULL
                TO WGRZ-ID-MOVI-CHIU-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-ID-MOVI-CHIU
                TO WGRZ-ID-MOVI-CHIU(IX-TAB-GRZ)
           END-IF
           MOVE L9091-DT-INI-EFF
             TO WGRZ-DT-INI-EFF(IX-TAB-GRZ)
           MOVE L9091-DT-END-EFF
             TO WGRZ-DT-END-EFF(IX-TAB-GRZ)
           MOVE L9091-COD-COMP-ANIA
             TO WGRZ-COD-COMP-ANIA(IX-TAB-GRZ)
           IF L9091-IB-OGG-NULL = HIGH-VALUES
              MOVE L9091-IB-OGG-NULL
                TO WGRZ-IB-OGG-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-IB-OGG
                TO WGRZ-IB-OGG(IX-TAB-GRZ)
           END-IF
           IF L9091-DT-DECOR-NULL = HIGH-VALUES
              MOVE L9091-DT-DECOR-NULL
                TO WGRZ-DT-DECOR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-DT-DECOR
                TO WGRZ-DT-DECOR(IX-TAB-GRZ)
           END-IF
           IF L9091-DT-SCAD-NULL = HIGH-VALUES
              MOVE L9091-DT-SCAD-NULL
                TO WGRZ-DT-SCAD-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-DT-SCAD
                TO WGRZ-DT-SCAD(IX-TAB-GRZ)
           END-IF
           IF L9091-COD-SEZ-NULL = HIGH-VALUES
              MOVE L9091-COD-SEZ-NULL
                TO WGRZ-COD-SEZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-COD-SEZ
                TO WGRZ-COD-SEZ(IX-TAB-GRZ)
           END-IF
           MOVE L9091-COD-TARI
             TO WGRZ-COD-TARI(IX-TAB-GRZ)
           IF L9091-RAMO-BILA-NULL = HIGH-VALUES
              MOVE L9091-RAMO-BILA-NULL
                TO WGRZ-RAMO-BILA-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-RAMO-BILA
                TO WGRZ-RAMO-BILA(IX-TAB-GRZ)
           END-IF
           IF L9091-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE L9091-DT-INI-VAL-TAR-NULL
                TO WGRZ-DT-INI-VAL-TAR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-DT-INI-VAL-TAR
                TO WGRZ-DT-INI-VAL-TAR(IX-TAB-GRZ)
           END-IF
           IF L9091-ID-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ID-1O-ASSTO-NULL
                TO WGRZ-ID-1O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-ID-1O-ASSTO
                TO WGRZ-ID-1O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L9091-ID-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ID-2O-ASSTO-NULL
                TO WGRZ-ID-2O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-ID-2O-ASSTO
                TO WGRZ-ID-2O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L9091-ID-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ID-3O-ASSTO-NULL
                TO WGRZ-ID-3O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-ID-3O-ASSTO
                TO WGRZ-ID-3O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L9091-TP-GAR-NULL = HIGH-VALUES
              MOVE L9091-TP-GAR-NULL
                TO WGRZ-TP-GAR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-TP-GAR
                TO WGRZ-TP-GAR(IX-TAB-GRZ)
           END-IF
           IF L9091-TP-RSH-NULL = HIGH-VALUES
              MOVE L9091-TP-RSH-NULL
                TO WGRZ-TP-RSH-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-TP-RSH
                TO WGRZ-TP-RSH(IX-TAB-GRZ)
           END-IF
           IF L9091-TP-INVST-NULL = HIGH-VALUES
              MOVE L9091-TP-INVST-NULL
                TO WGRZ-TP-INVST-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-TP-INVST
                TO WGRZ-TP-INVST(IX-TAB-GRZ)
           END-IF
           IF L9091-MOD-PAG-GARCOL-NULL = HIGH-VALUES
              MOVE L9091-MOD-PAG-GARCOL-NULL
                TO WGRZ-MOD-PAG-GARCOL-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-MOD-PAG-GARCOL
                TO WGRZ-MOD-PAG-GARCOL(IX-TAB-GRZ)
           END-IF
           IF L9091-TP-PER-PRE-NULL = HIGH-VALUES
              MOVE L9091-TP-PER-PRE-NULL
                TO WGRZ-TP-PER-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-TP-PER-PRE
                TO WGRZ-TP-PER-PRE(IX-TAB-GRZ)
           END-IF
           IF L9091-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ETA-AA-1O-ASSTO-NULL
                TO WGRZ-ETA-AA-1O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-ETA-AA-1O-ASSTO
                TO WGRZ-ETA-AA-1O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L9091-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ETA-MM-1O-ASSTO-NULL
                TO WGRZ-ETA-MM-1O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-ETA-MM-1O-ASSTO
                TO WGRZ-ETA-MM-1O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L9091-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ETA-AA-2O-ASSTO-NULL
                TO WGRZ-ETA-AA-2O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-ETA-AA-2O-ASSTO
                TO WGRZ-ETA-AA-2O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L9091-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ETA-MM-2O-ASSTO-NULL
                TO WGRZ-ETA-MM-2O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-ETA-MM-2O-ASSTO
                TO WGRZ-ETA-MM-2O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L9091-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ETA-AA-3O-ASSTO-NULL
                TO WGRZ-ETA-AA-3O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-ETA-AA-3O-ASSTO
                TO WGRZ-ETA-AA-3O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L9091-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
              MOVE L9091-ETA-MM-3O-ASSTO-NULL
                TO WGRZ-ETA-MM-3O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-ETA-MM-3O-ASSTO
                TO WGRZ-ETA-MM-3O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF L9091-TP-EMIS-PUR-NULL = HIGH-VALUES
              MOVE L9091-TP-EMIS-PUR-NULL
                TO WGRZ-TP-EMIS-PUR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-TP-EMIS-PUR
                TO WGRZ-TP-EMIS-PUR(IX-TAB-GRZ)
           END-IF
           IF L9091-ETA-A-SCAD-NULL = HIGH-VALUES
              MOVE L9091-ETA-A-SCAD-NULL
                TO WGRZ-ETA-A-SCAD-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-ETA-A-SCAD
                TO WGRZ-ETA-A-SCAD(IX-TAB-GRZ)
           END-IF
           IF L9091-TP-CALC-PRE-PRSTZ-NULL = HIGH-VALUES
              MOVE L9091-TP-CALC-PRE-PRSTZ-NULL
                TO WGRZ-TP-CALC-PRE-PRSTZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-TP-CALC-PRE-PRSTZ
                TO WGRZ-TP-CALC-PRE-PRSTZ(IX-TAB-GRZ)
           END-IF
           IF L9091-TP-PRE-NULL = HIGH-VALUES
              MOVE L9091-TP-PRE-NULL
                TO WGRZ-TP-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-TP-PRE
                TO WGRZ-TP-PRE(IX-TAB-GRZ)
           END-IF
           IF L9091-TP-DUR-NULL = HIGH-VALUES
              MOVE L9091-TP-DUR-NULL
                TO WGRZ-TP-DUR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-TP-DUR
                TO WGRZ-TP-DUR(IX-TAB-GRZ)
           END-IF
           IF L9091-DUR-AA-NULL = HIGH-VALUES
              MOVE L9091-DUR-AA-NULL
                TO WGRZ-DUR-AA-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-DUR-AA
                TO WGRZ-DUR-AA(IX-TAB-GRZ)
           END-IF
           IF L9091-DUR-MM-NULL = HIGH-VALUES
              MOVE L9091-DUR-MM-NULL
                TO WGRZ-DUR-MM-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-DUR-MM
                TO WGRZ-DUR-MM(IX-TAB-GRZ)
           END-IF
           IF L9091-DUR-GG-NULL = HIGH-VALUES
              MOVE L9091-DUR-GG-NULL
                TO WGRZ-DUR-GG-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-DUR-GG
                TO WGRZ-DUR-GG(IX-TAB-GRZ)
           END-IF
           IF L9091-NUM-AA-PAG-PRE-NULL = HIGH-VALUES
              MOVE L9091-NUM-AA-PAG-PRE-NULL
                TO WGRZ-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-NUM-AA-PAG-PRE
                TO WGRZ-NUM-AA-PAG-PRE(IX-TAB-GRZ)
           END-IF
           IF L9091-AA-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE L9091-AA-PAG-PRE-UNI-NULL
                TO WGRZ-AA-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-AA-PAG-PRE-UNI
                TO WGRZ-AA-PAG-PRE-UNI(IX-TAB-GRZ)
           END-IF
           IF L9091-MM-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE L9091-MM-PAG-PRE-UNI-NULL
                TO WGRZ-MM-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-MM-PAG-PRE-UNI
                TO WGRZ-MM-PAG-PRE-UNI(IX-TAB-GRZ)
           END-IF
           IF L9091-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
              MOVE L9091-FRAZ-INI-EROG-REN-NULL
                TO WGRZ-FRAZ-INI-EROG-REN-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-FRAZ-INI-EROG-REN
                TO WGRZ-FRAZ-INI-EROG-REN(IX-TAB-GRZ)
           END-IF
           IF L9091-MM-1O-RAT-NULL = HIGH-VALUES
              MOVE L9091-MM-1O-RAT-NULL
                TO WGRZ-MM-1O-RAT-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-MM-1O-RAT
                TO WGRZ-MM-1O-RAT(IX-TAB-GRZ)
           END-IF
           IF L9091-PC-1O-RAT-NULL = HIGH-VALUES
              MOVE L9091-PC-1O-RAT-NULL
                TO WGRZ-PC-1O-RAT-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-PC-1O-RAT
                TO WGRZ-PC-1O-RAT(IX-TAB-GRZ)
           END-IF
           IF L9091-TP-PRSTZ-ASSTA-NULL = HIGH-VALUES
              MOVE L9091-TP-PRSTZ-ASSTA-NULL
                TO WGRZ-TP-PRSTZ-ASSTA-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-TP-PRSTZ-ASSTA
                TO WGRZ-TP-PRSTZ-ASSTA(IX-TAB-GRZ)
           END-IF
           IF L9091-DT-END-CARZ-NULL = HIGH-VALUES
              MOVE L9091-DT-END-CARZ-NULL
                TO WGRZ-DT-END-CARZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-DT-END-CARZ
                TO WGRZ-DT-END-CARZ(IX-TAB-GRZ)
           END-IF
           IF L9091-PC-RIP-PRE-NULL = HIGH-VALUES
              MOVE L9091-PC-RIP-PRE-NULL
                TO WGRZ-PC-RIP-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-PC-RIP-PRE
                TO WGRZ-PC-RIP-PRE(IX-TAB-GRZ)
           END-IF
           IF L9091-COD-FND-NULL = HIGH-VALUES
              MOVE L9091-COD-FND-NULL
                TO WGRZ-COD-FND-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-COD-FND
                TO WGRZ-COD-FND(IX-TAB-GRZ)
           END-IF
           IF L9091-AA-REN-CER-NULL = HIGH-VALUES
              MOVE L9091-AA-REN-CER-NULL
                TO WGRZ-AA-REN-CER-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-AA-REN-CER
                TO WGRZ-AA-REN-CER(IX-TAB-GRZ)
           END-IF
           IF L9091-PC-REVRSB-NULL = HIGH-VALUES
              MOVE L9091-PC-REVRSB-NULL
                TO WGRZ-PC-REVRSB-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-PC-REVRSB
                TO WGRZ-PC-REVRSB(IX-TAB-GRZ)
           END-IF
           IF L9091-TP-PC-RIP-NULL = HIGH-VALUES
              MOVE L9091-TP-PC-RIP-NULL
                TO WGRZ-TP-PC-RIP-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-TP-PC-RIP
                TO WGRZ-TP-PC-RIP(IX-TAB-GRZ)
           END-IF
           IF L9091-PC-OPZ-NULL = HIGH-VALUES
              MOVE L9091-PC-OPZ-NULL
                TO WGRZ-PC-OPZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-PC-OPZ
                TO WGRZ-PC-OPZ(IX-TAB-GRZ)
           END-IF
           IF L9091-TP-IAS-NULL = HIGH-VALUES
              MOVE L9091-TP-IAS-NULL
                TO WGRZ-TP-IAS-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-TP-IAS
                TO WGRZ-TP-IAS(IX-TAB-GRZ)
           END-IF
           IF L9091-TP-STAB-NULL = HIGH-VALUES
              MOVE L9091-TP-STAB-NULL
                TO WGRZ-TP-STAB-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-TP-STAB
                TO WGRZ-TP-STAB(IX-TAB-GRZ)
           END-IF
           IF L9091-TP-ADEG-PRE-NULL = HIGH-VALUES
              MOVE L9091-TP-ADEG-PRE-NULL
                TO WGRZ-TP-ADEG-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-TP-ADEG-PRE
                TO WGRZ-TP-ADEG-PRE(IX-TAB-GRZ)
           END-IF
           IF L9091-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
              MOVE L9091-DT-VARZ-TP-IAS-NULL
                TO WGRZ-DT-VARZ-TP-IAS-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-DT-VARZ-TP-IAS
                TO WGRZ-DT-VARZ-TP-IAS(IX-TAB-GRZ)
           END-IF
           IF L9091-FRAZ-DECR-CPT-NULL = HIGH-VALUES
              MOVE L9091-FRAZ-DECR-CPT-NULL
                TO WGRZ-FRAZ-DECR-CPT-NULL(IX-TAB-GRZ)
           ELSE
              MOVE L9091-FRAZ-DECR-CPT
                TO WGRZ-FRAZ-DECR-CPT(IX-TAB-GRZ)
           END-IF
           MOVE L9091-DS-RIGA
             TO WGRZ-DS-RIGA(IX-TAB-GRZ)
           MOVE L9091-DS-OPER-SQL
             TO WGRZ-DS-OPER-SQL(IX-TAB-GRZ)
           MOVE L9091-DS-VER
             TO WGRZ-DS-VER(IX-TAB-GRZ)
           MOVE L9091-DS-TS-INI-CPTZ
             TO WGRZ-DS-TS-INI-CPTZ(IX-TAB-GRZ)
           MOVE L9091-DS-TS-END-CPTZ
             TO WGRZ-DS-TS-END-CPTZ(IX-TAB-GRZ)
           MOVE L9091-DS-UTENTE
             TO WGRZ-DS-UTENTE(IX-TAB-GRZ)
           MOVE L9091-DS-STATO-ELAB
             TO WGRZ-DS-STATO-ELAB(IX-TAB-GRZ)
      *       256 --> Tasso stabilizzazione limitata
           IF L9091-TS-STAB-LIMITATA-NULL = HIGH-VALUES
             MOVE L9091-TS-STAB-LIMITATA-NULL TO
                  WGRZ-TS-STAB-LIMITATA-NULL(IX-TAB-GRZ)
           ELSE
             MOVE L9091-TS-STAB-LIMITATA
                 TO WGRZ-TS-STAB-LIMITATA(IX-TAB-GRZ)
           END-IF.
      *       256 --> Tasso stabilizzazione limitata FINE
ITRFO      IF L9091-RSH-INVST-NULL = HIGH-VALUES
ITRFO         MOVE L9091-RSH-INVST-NULL
ITRFO           TO WGRZ-RSH-INVST-NULL(IX-TAB-GRZ)
ITRFO      ELSE
ITRFO         MOVE L9091-RSH-INVST
ITRFO           TO WGRZ-RSH-INVST(IX-TAB-GRZ)
ITRFO      END-IF
ITRFO      MOVE L9091-TP-RAMO-BILA
ITRFO        TO WGRZ-TP-RAMO-BILA(IX-TAB-GRZ).

       VALORIZZA-OUTPUT-GRZ-9090-EX.
           EXIT.
      *-----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella PERS (ANAGRAFE)
      *-----------------------------------------------------------------
       S0200-PREP-LETT-PERS.

           MOVE 'S0200-PREP-LETT-PERS'          TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> VALORIZZAZIONE DCLGEN
           INITIALIZE                           LDBV1301.

           MOVE RAN-COD-SOGG                    TO WK-ID-STRINGA.
      *    CONVERSIONE DA STRINGA A NUMERICO
           PERFORM Z900-CONVERTI-CHAR
              THRU Z900-EX.

           IF IDSV0001-ESITO-OK
              MOVE IWFO0051-CAMPO-OUTPUT-DEFI   TO LDBV1301-ID-TAB
              MOVE WS-DT-TS-PTF                 TO LDBV1301-TIMESTAMP

      *--> La data effetto viene sempre valorizzata
              MOVE WS-DT-EFFETTO         TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
              MOVE WS-DT-TS-PTF          TO IDSI0011-DATA-COMPETENZA

      *--> TRATTAMENTO PER STORICITA
              SET IDSI0011-TRATT-SENZA-STOR TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSI0011-PRIMARY-KEY     TO TRUE

      *--> TIPO OPERAZIONE
              SET IDSI0011-SELECT          TO TRUE

      *  --> Nome tabella fisica db
              MOVE 'LDBS1300'              TO IDSI0011-CODICE-STR-DATO

      *  --> Dclgen tabella
              MOVE SPACES                  TO IDSI0011-BUFFER-WHERE-COND
              MOVE LDBV1301                TO IDSI0011-BUFFER-WHERE-COND

              SET IDSO0011-SUCCESSFUL-RC   TO TRUE
              SET IDSO0011-SUCCESSFUL-SQL  TO TRUE
           END-IF.

       EX-S0200.
           EXIT.

      * ----------------------------------------------------------------
      * Chiamata al dispatcher dati per l'accesso alla PERS (ANAGRAFE)
      * ----------------------------------------------------------------
       S0201-LEGGI-PERS.

           MOVE 'S0201-LEGGI-PERS'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS1300'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura PERS'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS1300'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura PERS'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *-->             CHIAVE NON TROVATA
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0201-LEGGI-PERS'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005056'
                         TO IEAI9901-COD-ERRORE
                       MOVE 'PERS (CONTRAENTE)'
                         TO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *  --> Operazione eseguita correttamente
                       MOVE IDSO0011-BUFFER-DATI
                         TO PERS

                  WHEN OTHER
      *  --> Errore di accesso al db
                       MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0201-LEGGI-PERS'
                                          TO IEAI9901-LABEL-ERR
                       MOVE '005016'      TO IEAI9901-COD-ERRORE
                       STRING 'PERS (CONTRAENTE);'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *  --> Errore dispatcher
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0201-LEGGI-PERS'     TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              STRING 'PERS (CONTRAENTE);'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S0201.
           EXIT.

      *-----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella QUOTAZIONE AGGIUNTIVA FONDI
      *-----------------------------------------------------------------
       S0202-PREP-LET-QUOT-AGG-FND.

           MOVE 'S0202-PREP-LET-QUOT-AGG-FND'   TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> VALORIZZAZIONE DCLGEN
           INITIALIZE                           QUOTZ-AGG-FND.

           MOVE WPOL-COD-COMP-ANIA
             TO L41-COD-COMP-ANIA.

           MOVE WS-TP-QTZ
             TO L41-TP-QTZ

           MOVE WS-COD-FND
             TO L41-COD-FND

           MOVE WS-DT-QUOT
             TO L41-DT-QTZ

      *--> La data effetto viene sempre valorizzata
           MOVE WS-DT-EFFETTO           TO IDSI0011-DATA-INIZIO-EFFETTO
                                           IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF            TO IDSI0011-DATA-COMPETENZA.

      *--> TRATTAMENTO PER STORICITA
           SET IDSI0011-TRATT-SENZA-STOR     TO TRUE.

      *--> LIVELLO OPERAZIONE
           SET IDSI0011-PRIMARY-KEY     TO TRUE.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT          TO TRUE.

      *--> Nome tabella fisica db
           MOVE 'QUOTZ-AGG-FND'         TO IDSI0011-CODICE-STR-DATO.

      *--> Dclgen tabella
           MOVE SPACES                  TO IDSI0011-BUFFER-WHERE-COND.
           MOVE QUOTZ-AGG-FND           TO IDSI0011-BUFFER-DATI.

           SET IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL  TO TRUE.

       EX-S0202.
           EXIT.
      * ----------------------------------------------------------------
      * Chiamata al dispatcher dati per l'accesso alla PERS (ANAGRAFE)
      * ----------------------------------------------------------------
       S0203-LETTURA-QUOT-AGG-FND.

           MOVE 'S0203-LETTURA-QUOT-AGG-FND'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IDBS0002'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura QUOTZ_AGG_FND'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IDBS0002'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura QUOTZ_AGG_FND'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *-->             CHIAVE NON TROVATA
                       IF WS-QTZ-ZER-COUP-SOT-FND-INDEX
                       OR WS-QTZ-OPZ-SOT-FND-INDEX
                          SET ZERO-OPZIONI-KO TO TRUE
                       END-IF

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *  --> Operazione eseguita correttamente
                       MOVE IDSO0011-BUFFER-DATI
                         TO QUOTZ-AGG-FND

                  WHEN OTHER
      *  --> Errore di accesso al db
                       MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0203-LETTURA-QUOT-AGG-FND'
                                          TO IEAI9901-LABEL-ERR
                       MOVE '005016'      TO IEAI9901-COD-ERRORE
                       STRING 'QUOTZ-AGG-FND;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *  --> Errore dispatcher
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0203-LETTURA-QUOT-AGG-FND'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              STRING 'QUOTZ-AGG-FND;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S0203.
           EXIT.


      *-----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella QUOTAZIONE FONDI UNIT
      *-----------------------------------------------------------------
       S0204-PREP-LET-QUOT-FND-UNIT.
           MOVE 'S0204-PREP-LET-QUOT-FND'   TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE QUOTZ-FND-UNIT
PERFOR*               IDSI0011-BUFFER-DATI.

           MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.
           SET  IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET  IDSI0011-WHERE-CONDITION  TO TRUE.
           SET  IDSI0011-SELECT           TO TRUE.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                          TO L19-COD-COMP-ANIA.
           MOVE WS-COD-FND
                                          TO L19-COD-FND.
           MOVE WS-DT-QUOT                TO L19-DT-QTZ.
           MOVE SPACES                    TO IDSI0011-BUFFER-WHERE-COND.
           MOVE 'LDBS2080'                TO IDSI0011-CODICE-STR-DATO
           MOVE QUOTZ-FND-UNIT            TO IDSI0011-BUFFER-DATI.

       EX-S0204.
           EXIT.


       S0205-LETTURA-QUOT-FND-UNIT.

           MOVE 'S0205-LETTURA-QUOT-FND-UNIT' TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO           TO TRUE
           MOVE WK-LABEL-ERR                  TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS2080'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura VAL_AST'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS2080'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura VAL_AST'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    MOVE IDSO0011-BUFFER-DATI TO QUOTZ-FND-UNIT

                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                    MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    MOVE WK-LABEL-ERR  TO IEAI9901-LABEL-ERR
                    MOVE '005016'      TO IEAI9901-COD-ERRORE
                    STRING 'LDBS2080'         ';'
                           IDSO0011-RETURN-CODE ';'
                           IDSO0011-SQLCODE
                           DELIMITED BY SIZE
                           INTO IEAI9901-PARAMETRI-ERR
                    END-STRING
                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
                       THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> GESTIONE ERRORE
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR       TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              STRING 'LDBS2080'         ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S0205.
           EXIT.


      *-----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella DETTAGLIO TITOLO
      *-----------------------------------------------------------------
       S0206-PREP-LET-DETT-TIT.

           MOVE 'S0206-PREP-LET-DETT-TIT' TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> VALORIZZAZIONE DCLGEN
           INITIALIZE                   DETT-TIT-CONT.

           MOVE WTGA-ID-TRCH-DI-GAR (IX-TAB-TGA)
             TO DTC-ID-OGG.

           SET TRANCHE                  TO TRUE.
           MOVE WS-TP-OGG               TO DTC-TP-OGG.

           MOVE WS-DT-CALCOLO-NUM       TO DTC-DT-INI-COP.

      *--> La data effetto viene sempre valorizzata
           MOVE WS-DT-EFFETTO       TO IDSI0011-DATA-INIZIO-EFFETTO
                                       IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF        TO IDSI0011-DATA-COMPETENZA

      *--> TRATTAMENTO PER STORICITA
           SET IDSI0011-TRATT-X-COMPETENZA TO TRUE.

      *--> LIVELLO OPERAZIONE
           SET IDSI0011-WHERE-CONDITION TO TRUE.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT          TO TRUE.

      *--> Nome tabella fisica db
           MOVE 'LDBS5900'              TO IDSI0011-CODICE-STR-DATO.

      *--> Dclgen tabella
           MOVE SPACES                  TO IDSI0011-BUFFER-WHERE-COND.
           MOVE DETT-TIT-CONT           TO IDSI0011-BUFFER-DATI.

           SET IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL  TO TRUE.

       EX-S0206.
           EXIT.

      * ----------------------------------------------------------------
      * Chiamata al dispatcher dati per l'accesso alla DETT-TIT-CONT
      * ----------------------------------------------------------------
       S0207-LETTURA-DETT-TIT.

           MOVE 'S0207-LETTURA-DETT-TIT'  TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS5900'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura DETT_TIT_CONT'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS5900'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura DETT_TIT_CONT'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *-->             CHIAVE NON TROVATA
                       SET NO-TITOLO-COLLEGATO TO TRUE

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *  --> Operazione eseguita correttamente
                       SET SI-TITOLO-COLLEGATO TO TRUE
                       MOVE IDSO0011-BUFFER-DATI
                         TO DETT-TIT-CONT

                  WHEN OTHER
      *  --> Errore di accesso al db
                       MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0207-LETTURA-DETT-TIT'
                                          TO IEAI9901-LABEL-ERR
                       MOVE '005016'      TO IEAI9901-COD-ERRORE
                       STRING 'DETT-TIT-CONT;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *  --> Errore dispatcher
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0207-LETTURA-DETT-TIT'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              STRING 'DETT-TIT-CONT;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S0207.
           EXIT.

      *-----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella TITOLO CONTABILE
      *-----------------------------------------------------------------
       S0208-PREP-LET-TITOLO.

           MOVE 'S0208-PREP-LET-TITOLO' TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> VALORIZZAZIONE DCLGEN
           INITIALIZE                   TIT-CONT.

           MOVE DTC-ID-TIT-CONT         TO TIT-ID-TIT-CONT.

      *--> La data effetto viene sempre valorizzata
           MOVE WS-DT-EFFETTO           TO IDSI0011-DATA-INIZIO-EFFETTO
                                           IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF            TO IDSI0011-DATA-COMPETENZA.

      *--> TRATTAMENTO PER STORICITA
           SET IDSI0011-TRATT-X-COMPETENZA TO TRUE.

      *--> LIVELLO OPERAZIONE
           SET IDSI0011-ID              TO TRUE.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT          TO TRUE.

      *--> Nome tabella fisica db
           MOVE 'TIT-CONT'              TO IDSI0011-CODICE-STR-DATO.

      *--> Dclgen tabella
           MOVE SPACES                  TO IDSI0011-BUFFER-WHERE-COND.
           MOVE TIT-CONT                TO IDSI0011-BUFFER-DATI.

           SET IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL  TO TRUE.

       EX-S0208.
           EXIT.

      * ----------------------------------------------------------------
      * Chiamata al dispatcher dati per l'accesso alla TITOLO CONTABILE
      * ----------------------------------------------------------------
       S0209-LETTURA-TITOLO.

           MOVE 'S0209-LETTURA-TITOLO'  TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IDBSTIT0'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura TIT_CONT'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IDBSTIT0'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura TIT_CONT'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *-->             CHIAVE NON TROVATA
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0209-LETTURA-TITOLO'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005056'
                         TO IEAI9901-COD-ERRORE
                       MOVE 'TIT-CONT'
                         TO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *  --> Operazione eseguita correttamente
                       MOVE IDSO0011-BUFFER-DATI
                         TO TIT-CONT

                  WHEN OTHER
      *  --> Errore di accesso al db
                       MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0209-LETTURA-TITOLO'
                                          TO IEAI9901-LABEL-ERR
                       MOVE '005016'      TO IEAI9901-COD-ERRORE
                       STRING 'TIT-CONT;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *  --> Errore dispatcher
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0209-LETTURA-TITOLO'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              STRING 'TIT-CONT;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S0209.
           EXIT.

      *-----------------------------------------------------------------
      * Valorizzazione dei campi necessari al dispatcher dati per
      * l'accesso alla tabella ESTENSIONE TRANCHE DI GARANZIA
      *-----------------------------------------------------------------
       S0220-PREP-LET-EST-TRANCHE.

           MOVE 'S0220-PREP-LET-EST-TRANCHE'   TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> VALORIZZAZIONE DCLGEN
           INITIALIZE                             EST-TRCH-DI-GAR.

           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
             TO E12-ID-TRCH-DI-GAR

      *--> La data effetto viene sempre valorizzata
           MOVE WS-DT-EFFETTO       TO IDSI0011-DATA-INIZIO-EFFETTO
                                       IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF        TO IDSI0011-DATA-COMPETENZA

      *--> TRATTAMENTO PER COMPETENZA
           SET IDSI0011-TRATT-X-COMPETENZA     TO TRUE.

      *--> LIVELLO OPERAZIONE
           SET IDSI0011-ID-PADRE               TO TRUE.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT           TO TRUE.

      *--> Nome tabella fisica db
           MOVE 'EST-TRCH-DI-GAR'        TO IDSI0011-CODICE-STR-DATO.

      *--> Dclgen tabella
           MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
           MOVE EST-TRCH-DI-GAR          TO IDSI0011-BUFFER-DATI.

           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

       EX-S0220.
           EXIT.
      *-----------------------------------------------------------------
      * Chiamata al dispatcher dati per l'accesso alla PERS (ANAGRAFE)
      *-----------------------------------------------------------------
       S0230-LET-EST-TRANCHE.

           MOVE 'S0230-LET-EST-TRANCHE'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IDBSE120'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura EST_TRCH_DI_GAR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'IDBSE120'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura EST_TRCH_DI_GAR'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           IF IDSO0011-SUCCESSFUL-RC

              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *-->             CHIAVE NON TROVATA
                       CONTINUE

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *  --> Operazione eseguita correttamente
                       MOVE IDSO0011-BUFFER-DATI
                         TO EST-TRCH-DI-GAR

                  WHEN OTHER
      *  --> Errore di accesso al db
                       MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR  TO IEAI9901-LABEL-ERR
                       MOVE '005016'      TO IEAI9901-COD-ERRORE
                       STRING IDSI0011-CODICE-STR-DATO ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *  --> Errore dispatcher
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR           TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              STRING IDSI0011-CODICE-STR-DATO ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S0230.
           EXIT.
      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           MOVE 'S9000-OPERAZIONI-FINALI'   TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY           THRU ESEGUI-DISPLAY-EX.

       EX-S9000.
           EXIT.


      ********************************************************************
      ** Prepara lettura rapp-ana (Adesione/Assicurato)
      *******************************************************************
       S0167-PREP-RAN-ADE-ASS.

           MOVE 'S0167-PREP-RAN-ADE-ASS'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> TIPO RAPPORTO ANAGRAFICO
           SET ASSICURATO                      TO TRUE.

      *--> VALORIZZAZIONE DCLGEN
           INITIALIZE                          RAPP-ANA.

           MOVE WADE-ID-ADES(IX-TAB-ADE)    TO RAN-ID-OGG.
           MOVE 'AD'                        TO RAN-TP-OGG.
           MOVE WS-TP-RAPP-ANA              TO RAN-TP-RAPP-ANA.

      *  --> La data effetto viene sempre valorizzata
           MOVE WS-DT-EFFETTO          TO IDSI0011-DATA-INIZIO-EFFETTO
                                           IDSI0011-DATA-FINE-EFFETTO
           MOVE WS-DT-TS-PTF            TO IDSI0011-DATA-COMPETENZA.

      *--> TRATTAMENTO PER STORICITA
           SET IDSI0011-TRATT-DEFAULT   TO TRUE.

      *--> LIVELLO OPERAZIONE
           SET IDSI0011-WHERE-CONDITION TO TRUE.


      *  --> Nome tabella fisica db
           MOVE 'LDBS8200'              TO IDSI0011-CODICE-STR-DATO.

      *  --> Dclgen tabella
           MOVE SPACES                  TO IDSI0011-BUFFER-WHERE-COND.

           MOVE RAPP-ANA                TO IDSI0011-BUFFER-DATI.

           SET IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL  TO TRUE.




       S0167-EX.
           EXIT.
      *******************************************************************
      ** Lettura rapp-ana (Adesione/Assicurato)
      *******************************************************************
       S0169-LETT-RAN-ADE-ASS.

           MOVE 'S0169-LETT-RAN-ADE-ASS'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS8200'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura RAPP_ANA'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       SET FINE-FETCH-RAN-SI TO TRUE



                  WHEN IDSO0011-SUCCESSFUL-SQL
      *  --> Operazione eseguita correttamente
                       MOVE IDSO0011-BUFFER-DATI
                         TO RAPP-ANA
                       SET IDSI0011-FETCH-NEXT     TO TRUE

                  WHEN OTHER
      *  --> Errore di accesso al db
                       MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0169-LETT-RAN-ADE-ASS'
                                          TO IEAI9901-LABEL-ERR
                       MOVE '005016'      TO IEAI9901-COD-ERRORE
                       STRING 'RAPP-ANA;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *  --> Errore dispatcher
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0169-LETT-RAN-ADE-ASS' TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              STRING 'RAPP-ANA;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF


           SET  IDSV8888-BUSINESS-DBG      TO TRUE
           MOVE 'LDBS8200'                 TO IDSV8888-NOME-PGM
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Lettura RAPP_ANA'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.


       S0169-EX.
           EXIT.
      *******************************************************************
      ** Lettura RAPP-ANA
      *******************************************************************
       S0170-LETTURA-WRAN.

           MOVE 'S0170-LETTURA-WRAN'        TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> Inizializzazione variabili
           MOVE 0 TO IX-WRAN

      *--> TIPO OPERAZIONE
           SET IDSI0011-FETCH-FIRST     TO TRUE.

           SET FINE-FETCH-RAN-NO TO TRUE

           PERFORM UNTIL IDSV0001-ESITO-KO
                      OR FINE-FETCH-RAN-SI

                      PERFORM S0167-PREP-RAN-ADE-ASS THRU S0167-EX

                      PERFORM S0169-LETT-RAN-ADE-ASS THRU S0169-EX

                      IF IDSV0001-ESITO-OK AND FINE-FETCH-RAN-NO

                        ADD 1 TO IX-WRAN
                        MOVE IX-WRAN TO IX-WRAN-MAX
                        MOVE RAPP-ANA TO WRAPP-TAB-RAPP-ANAG(IX-WRAN)

                        PERFORM S0200-PREP-LETT-PERS
                           THRU EX-S0200
                        IF IDSV0001-ESITO-OK
                          PERFORM S0201-LEGGI-PERS
                              THRU EX-S0201
                        END-IF
                        IF A25-IND-CLI-NULL NOT = HIGH-VALUES
                          MOVE A25-IND-CLI TO WA25-IND-CLI(IX-WRAN)
                        ELSE
                          MOVE HIGH-VALUES TO WA25-IND-CLI-NULL(IX-WRAN)
                        END-IF
                        IF A25-COD-FISC-NULL NOT = HIGH-VALUES
                          MOVE A25-COD-FISC TO WA25-COD-FISC(IX-WRAN)
                        ELSE
                          MOVE HIGH-VALUES
                                        TO WA25-COD-FISC-NULL(IX-WRAN)
                        END-IF

                        SET IDSI0011-FETCH-NEXT     TO TRUE

                      END-IF


           END-PERFORM.

       S0170-EX.
           EXIT.



      *******************************************************************
      *----RISCATTI PARZIALI
      *----CUMULO RISCATTI PARZIALI
      *******************************************************************
       S0580-RISCATTI.

           MOVE ZERO                  TO WS-SOMMA-IMPO-LIQ.

           SET RISCATT-KO             TO TRUE.
           PERFORM S1625-PREP-P-RISCATTI
              THRU EX-S1625.

           IF RISCATT-OK
              MOVE HIGH-VALUES               TO WB03-RISCPAR-NULL
              MOVE WS-SOMMA-IMPO-LIQ         TO WB03-CUM-RISCPAR
           ELSE
              MOVE HIGH-VALUES               TO WB03-RISCPAR-NULL
                                                WB03-CUM-RISCPAR-NULL
           END-IF.



       S0580-EX.
           EXIT.

      *******************************************************************
      *-----DATA EFFETTO RIDUZIONE
      *-----DATA EMISSIONE RIDUZIONE
      *******************************************************************
       S0600-DT-RIDUZIONE.

           MOVE ZERO TO WS-APPO-NUM-18
           PERFORM S1650-LEGGI-DT-RIDUZIONE
              THRU EX-S1650.
           IF STB-OK
              MOVE STB-DT-INI-EFF          TO WB03-DT-EFF-RIDZ
              MOVE STB-DS-TS-INI-CPTZ      TO WS-APPO-NUM-18
              MOVE WS-APPO-NUM-18(1:8)     TO WB03-DT-EMIS-RIDZ
           ELSE
              MOVE HIGH-VALUES             TO WB03-DT-EFF-RIDZ-NULL
                                              WB03-DT-EMIS-RIDZ-NULL
           END-IF.


       S0600-EX.
           EXIT.


      *******************************************************************
      *   RICERCA RAPP-ANA
      *******************************************************************
       RICERCA-RAN.

           MOVE 1 TO IX-WRAN
           SET WS-FL-ASSIC-NON-PRES TO TRUE
           PERFORM UNTIL WS-FL-ASSIC-PRES
                      OR IX-WRAN > IX-WRAN-MAX
                   IF WK-ID-ASSTO IS EQUAL WRAN-ID-RAPP-ANA(IX-WRAN)
                      SET WS-FL-ASSIC-PRES TO TRUE
                   ELSE
                      ADD 1 TO IX-WRAN
                   END-IF
           END-PERFORM
           IF WS-FL-ASSIC-PRES
              INITIALIZE RAPP-ANA
              MOVE WRAPP-TAB-RAPP-ANAG(IX-WRAN) TO RAPP-ANA
           END-IF.

       RICERCA-RAN-EX.
           EXIT.
      *******************************************************************
      * ASSEGNA CAMPI RAPP-ANA CON TP-RETE = 'GE'
      ******************************************************************
       RRE-WB03.

      *----AGENZIA
           IF RRE-COD-PNT-RETE-INI-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-COD-AGE-NULL
           ELSE
              MOVE RRE-COD-PNT-RETE-INI
                TO WB03-COD-AGE
           END-IF.

      *----SUBAGENZIA
           IF RRE-COD-PNT-RETE-END-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-COD-SUBAGE-NULL
           ELSE
              MOVE RRE-COD-PNT-RETE-END
                TO WB03-COD-SUBAGE
           END-IF.

      *----CANALE
           IF RRE-COD-CAN-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-COD-CAN-NULL
           ELSE
              MOVE RRE-COD-CAN
                TO WB03-COD-CAN
           END-IF.


       RRE-WB03-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CONVERTI VALORE DA STRINGA IN NUMERICO
      *----------------------------------------------------------------*
       Z900-CONVERTI-CHAR.

           MOVE 'Z900-CONVERTI-CHAR'        TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY           THRU ESEGUI-DISPLAY-EX.

           MOVE HIGH-VALUE           TO AREA-IWFS0050.
           MOVE WK-ID-STRINGA        TO IWFI0051-ARRAY-STRINGA-INPUT.

           CALL PGM-IWFS0050         USING AREA-IWFS0050
           ON EXCEPTION
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'IWFS0050'
                TO CALL-DESC
              MOVE 'Z900-CONVERTI-CHAR'
                TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL

           IF IDSV0001-ESITO-OK
              IF IWFO0051-ESITO-KO
                 MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR  TO IEAI9901-LABEL-ERR
                 MOVE '005166'      TO IEAI9901-COD-ERRORE
                 MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                 STRING PGM-IWFS0050
                        DELIMITED BY SIZE
                        ' - '
                        DELIMITED BY SIZE
                        IWFO0051-DESC-ERRORE-ESTESA
                        DELIMITED BY SIZE
                        INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

       Z900-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
       Z700-DT-N-TO-X.

           MOVE WS-STR-DATE-N(1:4)
                TO WS-DATE-X(1:4)
           MOVE WS-STR-DATE-N(5:2)
                TO WS-DATE-X(6:2)
           MOVE WS-STR-DATE-N(7:2)
                TO WS-DATE-X(9:2)

           MOVE '-'
                TO WS-DATE-X(5:1)
                   WS-DATE-X(8:1).

       Z700-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Lettura in ptf della parametro compagnia
      *----------------------------------------------------------------*
       S0499-RECUP-PARAM-COMP.

           MOVE HIGH-VALUE              TO PARAM-COMP.

           MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO
                                           IDSI0011-DATA-FINE-EFFETTO
                                           IDSI0011-DATA-COMPETENZA.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA TO PCO-COD-COMP-ANIA.

           MOVE 'PARAM-COMP'                TO IDSI0011-CODICE-STR-DATO
                                               WK-TABELLA.
           MOVE PARAM-COMP                  TO IDSI0011-BUFFER-DATI.


           SET IDSI0011-SELECT              TO TRUE.
           SET IDSI0011-PRIMARY-KEY         TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR    TO TRUE.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'IDBSPCO0'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-INIZIO            TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'RECUP-PARAM-COMP     '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

TRACE1     SET  IDSV8888-BUSINESS-DBG      TO TRUE
TRACE1     MOVE 'IDBSPCO0'                 TO IDSV8888-NOME-PGM
TRACE1     SET  IDSV8888-FINE              TO TRUE
TRACE1     MOVE SPACES                     TO IDSV8888-DESC-PGM
TRACE1     STRING 'RECUP-PARAM-COMP     '
TRACE1             DELIMITED BY SIZE
TRACE1             INTO IDSV8888-DESC-PGM
TRACE1     END-STRING
TRACE1     PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       MOVE WK-PGM          TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0010-LETTURA-PCO'
                                            TO IEAI9901-LABEL-ERR
                       MOVE '005069'        TO IEAI9901-COD-ERRORE
                       STRING WK-TABELLA           ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
                  WHEN IDSO0011-SUCCESSFUL-SQL
                     MOVE IDSO0011-BUFFER-DATI TO PARAM-COMP
                     MOVE 1 TO WPCO-ELE-PARA-COM-MAX
                     PERFORM VALORIZZA-OUTPUT-PCO
                        THRU VALORIZZA-OUTPUT-PCO-EX
              END-EVALUATE
           ELSE
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0010-LETTURA-PCO'
                                      TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S0499.
           EXIT.
      * ----------------------------------------------------------------
      * VALORIZZAZIONE DCLGEN TABELLA "ESTRATTI DI BILANCIO" CON I DATI
      * PREVENTIVAMENTE SELEZIONATI DALLE TABELLE DI PORTAFOGLIO
      * ----------------------------------------------------------------
           COPY LLBP0231.
      *-----------------------------------------------------------------
      *   VALORIZZAZIONI TABELLA BILA-FND-ESTR
      *-----------------------------------------------------------------
           COPY LLBP0232.
      * ----------------------------------------------------------------
      * ROUTINE-CALL-VALORIZZATORE-VARIABILI.
      * ----------------------------------------------------------------
           COPY IVVP0210               REPLACING ==(SF)== BY ==S211==.
      * ----------------------------------------------------------------
           COPY LCCP0001.
      * ----------------------------------------------------------------
      *--> Estrazione sequence
      * ----------------------------------------------------------------
           COPY LCCP0002.
      *----------------------------------------------------------------*
      *    COPY VALORIZZA OUTPUT TABELLE
      *----------------------------------------------------------------*
      *--> POLIZZA
           COPY LCCVPOL3               REPLACING ==(SF)== BY ==WPOL==.
      *--> ADESIONE
           COPY LCCVADE3               REPLACING ==(SF)== BY ==WADE==.
      *--> GARANZIA
           COPY LCCVGRZ3               REPLACING ==(SF)== BY ==WGRZ==.
      *--> TRANCHE DI GARANZIA
           COPY LCCVTGA3               REPLACING ==(SF)== BY ==WTGA==.
      *--> STATO OGGETTO BUSINESS
           COPY LCCVSTB3               REPLACING ==(SF)== BY ==WSTB==.
      *--> PARAMETRO OGGETTO
           COPY LCCVPOG3               REPLACING ==(SF)== BY ==WPOG==.
      *--> PARAMETRO MOVIMENTO
           COPY LCCVPMO3               REPLACING ==(SF)== BY ==WPMO==.
      *--> RISERVA DI TRANCHE
           COPY LCCVRST3               REPLACING ==(SF)== BY ==WRST==.
      *--> PARAMETRO COMPAGNIA
           COPY LCCVPCO3               REPLACING ==(SF)== BY ==WPCO==.
ITRFO *--> ESTENSIONE POLIZZA CPI PR
ITRFO      COPY LCCVP673               REPLACING ==(SF)== BY ==WP67==.
ITRFO *--> ACCORDO COMMERCIALE
ITRFO      COPY LCCVP633               REPLACING ==(SF)== BY ==WP63==.
      *----------------------------------------------------------------*
      *    COPY INIZIALIZZAZIONE AREE OUTPUT TABELLE
      *----------------------------------------------------------------*
      *--> POLIZZA
           COPY LCCVPOL4               REPLACING ==(SF)== BY ==WPOL==.
      *--> ADESIONE
           COPY LCCVADE4               REPLACING ==(SF)== BY ==WADE==.
      *--> GARANZIA
           COPY LCCVGRZ4               REPLACING ==(SF)== BY ==WGRZ==.
      *--> TRANCHE DI GARANZIA
           COPY LCCVTGA4               REPLACING ==(SF)== BY ==WTGA==.
      *--> STATO OGGETTO BUSINESS
           COPY LCCVSTB4               REPLACING ==(SF)== BY ==WSTB==.
      *--> PARAMETRO OGGETTO
           COPY LCCVPOG4               REPLACING ==(SF)== BY ==WPOG==.
      *--> PARAMETRO MOVIMENTO
           COPY LCCVPMO4               REPLACING ==(SF)== BY ==WPMO==.
      *--> ESTRATTI RISERVA
           COPY LCCVB034               REPLACING ==(SF)== BY ==WB03==.
      *--> VARIABILI DI CALCOLO RISERVA
           COPY LCCVB044               REPLACING ==(SF)== BY ==WB04==.
      *--> VARIABILI DI CALCOLO RISERVA
           COPY LCCVB054               REPLACING ==(SF)== BY ==WB05==.
ITRFO *--> ESTENSIONE POLIZZA CPI PR
ITRFO      COPY LCCVP674               REPLACING ==(SF)== BY ==WP67==.
ITRFO *--> ACCORDO COMMERCIALE
ITRFO      COPY LCCVP634               REPLACING ==(SF)== BY ==WP63==.
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IERP9901.
           COPY IERP9902.
      *----------------------------------------------------------------*
      *    ROUTINES DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.
      *----------------------------------------------------------------*
      *    routine di stringatura variabili
      *----------------------------------------------------------------*
           COPY IVVP0501.
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE FILE SEQUENZIALI
      *----------------------------------------------------------------*
           COPY IABPSQS5.
      *----------------------------------------------------------------*
      *    ROUTINES DI GESTIONE DISPLAY
      *----------------------------------------------------------------*
           COPY IDSP8888.
      *----------------------------------------------------------------*
      *--> routine di stringatura variabili
      *----------------------------------------------------------------*
           COPY IDSP0503.

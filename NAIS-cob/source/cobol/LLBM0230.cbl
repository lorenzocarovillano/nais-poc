************************************************************************
********                                                        ********
********                                                        ********
********                   BATCH EXECUTOR                       ********
********                                                        ********
************************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LLBM0230.
       AUTHOR.    AISS.
       DATE-WRITTEN.
       DATE-COMPILED.
      *REMARKS.
      ******************************************************************
      *    PROGRAAMMA .... LLBM0230
      *    FUNZIONE ...... BATCH EXECUTOR
      ******************************************************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
      *---> FILE PARAM DI INPUT
           SELECT PBTCEXEC ASSIGN    TO PBTCEXEC
           ACCESS IS SEQUENTIAL
           FILE STATUS  IS FS-PARA.

      *---> FILE REPORT01 DI OUTPUT
           SELECT REPORT01 ASSIGN    TO REPORT01
           ACCESS IS SEQUENTIAL
           FILE STATUS  IS FS-REPORT01.

      *---> FILE GUIDA SEQUENZIALE
           SELECT SEQGUIDE ASSIGN    TO SEQGUIDE
           ACCESS IS SEQUENTIAL
           FILE STATUS  IS FS-SEQGUIDE.

       DATA DIVISION.
       FILE SECTION.
       FD  PBTCEXEC.
       01  PARAM-REC              PIC X(80).

       FD  REPORT01.
       01  REPORT01-REC           PIC X(500).

       FD  SEQGUIDE.
       01  SEQGUIDE-REC           PIC X(300).

      *************************************************
      * W O R K I N G   S T O R A G E   S E C T I O N *
      *************************************************
       WORKING-STORAGE SECTION.
       77  WK-PGM                         PIC X(008)  VALUE 'LLBM0230'.
       77  WK-FILE-INP                    PIC X(008)  VALUE 'PBTCEXEC'.

      ******************************************************************
      * -- STRUTTURA MESSAGGIO X FILES SEQUENZIALI
      ******************************************************************
           COPY IABCSQ99.
      *--
      *--    valorizzare i campi PGM
      *--    per un 'eventuale servizio in caso di rottura chiave
      *--
       77  WK-PGM-SERV-ROTTURA            PIC X(008) VALUE  '        '.
       77  WK-PGM-SERV-SECON-BUS          PIC X(008) VALUE  '        '.
       77  WK-PGM-SERV-SECON-ROTT         PIC X(008) VALUE  '        '.
       77  WK-PGM-PRE-GUIDE               PIC X(008) VALUE  'LLBS0269'.
      *-- Fine PGM-USED

          COPY LLBO0261.
      *
          COPY LDBVC821.
      *

      ******************************************************************
      * CAMPI COMODO PER GESTIONE ROTTURA CHIAVE
      ******************************************************************
       01 WK-CHIAVE-ROTTURA.
          05 WK-ID-PADRE1-OLD     PIC S9(9)V COMP-3 VALUE ZEROES.

       01 WK-NEW-IB-OGG           PIC X(40) VALUE SPACES.
       01 WK-OLD-IB-OGG           PIC X(40) VALUE SPACES.
       01 NUM-ID-POLI             PIC 9(09) VALUE ZEROES.
       01 WS-FINE-PARAMETRI-ERR   PIC X(33) VALUE SPACES.

      *---------------------------------------------------------------*
      * AREA PER CONTATORI PERSONALIZZATI PER SCARTI
      *---------------------------------------------------------------*
       01 WS-AREA-CONTATORI.
          03 WS-CTR-1.
             05 DESC-CTR-1           PIC X(050)
                 VALUE '-- DI CUI SCRITTE NEL REC. SCARTI'.
             05 REC-SCARTATI-1       PIC 9(002) VALUE 1.

      ******************************************************************
      * STRUTTURA PER GESTIONE CACHE COMP_STR_DATO / ANAG_DATO
      ******************************************************************

       01 AREA-IDSV0102.
          COPY IDSV0102.

      *-- Inizio Include Batch Executor
      *---------------------------------------------------------------*
      * COPY INFRASTRUTTURALE PER BATCH EXECUTOR
      *---------------------------------------------------------------*
           EXEC SQL INCLUDE IABV0007 END-EXEC.
           EXEC SQL INCLUDE IABV0008 END-EXEC.

      *---------------------------------------------------------------*
      * AREA TABELLE DB2 PER BATCH EXECUTOR
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

           EXEC SQL INCLUDE IDBVBBS1 END-EXEC.
           EXEC SQL INCLUDE IDBVBBT1 END-EXEC.
           EXEC SQL INCLUDE IDBVBEM1 END-EXEC.
           EXEC SQL INCLUDE IDBVBES1 END-EXEC.
           EXEC SQL INCLUDE IDBVBJE1 END-EXEC.
           EXEC SQL INCLUDE IDBVBJS1 END-EXEC.
           EXEC SQL INCLUDE IDBVBRS1 END-EXEC.
           EXEC SQL INCLUDE IDBVBTC1 END-EXEC.
           EXEC SQL INCLUDE IDBVPCO1 END-EXEC.
           EXEC SQL INCLUDE IDBVLOR1 END-EXEC.
           EXEC SQL INCLUDE IDBVGRU1 END-EXEC.
           EXEC SQL INCLUDE IDBVBPA1 END-EXEC.
      *-- Fine Include Batch Executor

       01  IABV0006.
           COPY IABV0006.

      ******************************************************************
      * INCLUDE DB2 PORTAFOGLIO
      ******************************************************************
      *--    inserire le dclgen necessarie al Driver

           EXEC SQL INCLUDE IDBVPOL1 END-EXEC.
           EXEC SQL INCLUDE IDBVADE1 END-EXEC.
           EXEC SQL INCLUDE IDBVSTB1 END-EXEC.
           EXEC SQL INCLUDE IDBVRIC1 END-EXEC.

      *---------------------------------------------------------------*
      * AREA Output da dare in Input ad un 'eventuale
      * Servizio Secondario
      *---------------------------------------------------------------*
       01 ITSO0041.
           COPY ITSO0041.

           COPY LLBV0269.

      *----------------------------------------------------------------*
      *    COPY GESTIONE ERRORI EXTRA
      *----------------------------------------------------------------*
           COPY IEAV9904.

      ******************************************************************
      *    COPY DISPATCHER
      ******************************************************************
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      *  COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *  COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.

      ******************************************************************
      * --          GESTIONE GUIDA DA SEQUENZIALE
      ******************************************************************
           COPY IABVSQG1.
      *----------------------------------------------------------------*
      *   AREA FLUSSO DI INPUT FILE SCARTI LREC. 300
      *----------------------------------------------------------------*
       01 AREA-INP-SCARTI.
             05 SCA-POL-IB-OGG                 PIC X(40).
             05 SCA-POL-ID-POLI                PIC 9(09).
             05 SCA-POL-ID-POLI-ALF REDEFINES
                SCA-POL-ID-POLI                PIC X(09).
             05 SCA-ADE-ID-ADES                PIC 9(09).
             05 SCA-ADE-ID-ADES-ALF REDEFINES
                SCA-ADE-ID-ADES                PIC X(09).
             05 FILLER                         PIC X(242).

      ******************************************************************
      * P R O C E D U R E   D I V I S I O N                            *
      ******************************************************************
       PROCEDURE DIVISION.

           MOVE 'PROCEDURE DIVISION'     TO WK-LABEL.

           PERFORM A000-OPERAZ-INIZ      THRU A000-EX.

           IF WK-ERRORE-NO
              PERFORM B000-ELABORA-MACRO THRU B000-EX
           END-IF.

           PERFORM Z000-OPERAZ-FINALI    THRU Z000-OPERAZ-FINALI-FINE.

           STOP RUN.
      *----------------------------------------------------------------*
      * OPERAZIONI INIZIALI                                            *
      *----------------------------------------------------------------*
       A001-OPERAZIONI-INIZIALI.
      *
           MOVE 'A001-OPERAZ-INIZIALI'    TO WK-LABEL.
           INITIALIZE IABV0006-CUSTOM-COUNTERS.
           MOVE 10                     TO IABV0006-LIM-CUSTOM-COUNT-MAX
           MOVE 1                      TO IABV0006-MAX-ELE-CUSTOM-COUNT
           SET  IABV0006-COUNTER-STD-YES TO TRUE
           SET  IABV0006-GUIDE-ROWS-READ-YES TO TRUE
      *--> CONTATORE 1
           SET  IABV0006-COUNTER-DESC(REC-SCARTATI-1)
             TO TRUE
           MOVE ZEROES
             TO IABV0006-CUSTOM-COUNT(REC-SCARTATI-1)
      *
      *--
      *--    eventuali statements da eseguire in fase iniziale
      *--
      *
           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LLBM0230'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO            TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Iniz. area di cache'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           INITIALIZE             IDSV0102-ELE-MAX-COD-STR-DATO
                                  IDSV0102A-ELE-MAX-ACTU
                                  IDSV0102-CONTA-CACHE-CSV
                                  IDSV0102A-CONTA-CACHE-ADA-MVV
                                  IDSV0102-AREA-COD-STR-DATO(1)
                                  IDSV0102A-TAB-PARAM(1).

           MOVE IDSV0102-STR-COD-STR-DATO  TO  IDSV0102-RESTO-TABELLA.
           MOVE IDSV0102A-ADA-MVV          TO  IDSV0102A-RESTO-TABELLA.

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LLBM0230'                 TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE              TO TRUE
           MOVE SPACES                     TO IDSV8888-DESC-PGM
           STRING 'Iniz. area di cache'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.


       A001-EX.
           EXIT.
      *----------------------------------------------------------------*
      * OPERAZIONI INIZIALI                                            *
      *----------------------------------------------------------------*
       Z001-OPERAZIONI-FINALI.
      *
           MOVE 'Z001-OPERAZIONI-FINALI'    TO WK-LABEL.
      *
      *--
      *--    eventuali statements da eseguire in fase finale
      *--
      *
CACHE *--  DISPLAY PER STATISTICHE SULLA CACHE
CACHE *--
CACHE      DISPLAY '*------------------------------------------------*'
CACHE      DISPLAY '*     Cache Valorizzatore (Tipo Address "A")     *'
CACHE      DISPLAY '*------------------------------------------------*'
CACHE      DISPLAY '* Num. elem. Struttura Dato caricati ..:     '
CACHE               IDSV0102-ELE-MAX-COD-STR-DATO
CACHE      DISPLAY '* Num. elem. Struttura Dato trovati ...:     '
CACHE               IDSV0102-CONTA-CACHE-CSV
CACHE      DISPLAY '* Num. elem. Matr. Val. Var. caricati .:     '
CACHE               IDSV0102A-ELE-MAX-ACTU
CACHE      DISPLAY '* Num. elem. Matr. Val. Var. trovati ..:     '
CACHE               IDSV0102A-CONTA-CACHE-ADA-MVV
CACHE *    DISPLAY '*------------------------------------------------*'
CACHE *    DISPLAY '*     Cache Actuator      (Tipo Address "B")     *'
CACHE *    DISPLAY '*------------------------------------------------*'
CACHE *    DISPLAY '* Num. elem. caricati .................: '
CACHE *             ACTCACHE-TOT-ELE-CARICATI
CACHE *    DISPLAY '* Num. elem. trovati ..................: '
CACHE *             ACTCACHE-CONTA-CACHE
CACHE *    DISPLAY '*------------------------------------------------*'
CACHE *    DISPLAY '*     Cache Anagrafe      (Tipo Address "C")     *'
CACHE *    DISPLAY '*------------------------------------------------*'
CACHE *    DISPLAY '* Num. elem. caricati .................: '
CACHE *             ANACACHE-NUM-ELE-CARICATI
CACHE *    DISPLAY '* Num. elem. trovati ..................: '
CACHE *             ANACACHE-CONTA-CACHE
CACHE      DISPLAY '*------------------------------------------------*'.

       Z001-EX.
           EXIT.
      *----------------------------------------------------------------*
      * OPERAZIONI FINALI X SINGOLA RICHIESTA DI ELABORAZIONE (ID-BATCH)
      *----------------------------------------------------------------*
       Z002-OPERAZ-FINALI-X-BATCH.
      *
           MOVE 'Z002-OPERAZ-FINALI-X-BATCH'
             TO WK-LABEL.
      *
       Z002-EX.
           EXIT.
      *----------------------------------------------------------------*
      * DISPLAY INIZIO ELABORAZIONE
      *----------------------------------------------------------------*
       A101-DISPLAY-INIZIO-ELABORA.
      *
           DISPLAY '***********************************************'.
           DISPLAY '* NOME PROGRAMMA    = ' WK-PGM.
           DISPLAY '* DESCRIZIONE       = Inizio elaborazione di '
           DISPLAY '* BATCH EXECUTOR '.
           DISPLAY '* DATA ELABORAZIONE = ' WK-DATA-SISTEMA.
           DISPLAY '* ORA  ELABORAZIONE = ' WK-ORA-SISTEMA.
           DISPLAY '************************************************'.
           DISPLAY ALL SPACES.
      *
       A101-EX.
           EXIT.
      *----------------------------------------------------------------*
      * DISPLAY FINE ELABORAZIONE
      *----------------------------------------------------------------*
       A102-DISPLAY-FINE-ELABORA.
      *
           DISPLAY '***********************************************'.
           DISPLAY '* NOME PROGRAMMA    = ' WK-PGM.
           DISPLAY '* DESCRIZIONE       = Fine elaborazione di '
           DISPLAY '* BATCH EXECUTOR '.
           DISPLAY '* DATA ELABORAZIONE = ' WK-DATA-SISTEMA.
           DISPLAY '* ORA  ELABORAZIONE = ' WK-ORA-SISTEMA.
           DISPLAY '************************************************'.
           DISPLAY ALL SPACES.
      *
       A102-EX.
           EXIT.

      *----------------------------------------------------------------*
      * PRE BUSINESS
      *----------------------------------------------------------------*
       L500-PRE-BUSINESS.
      *
           MOVE 'L500-PRE-BUSINESS'    TO WK-LABEL.

      *
      *    RICERCA DELL' EVENTUALE AREA CACHE DA UTILIZZARE
      *
           PERFORM VARYING IX-ADDRESS FROM 1 BY 1
                   UNTIL   IX-ADDRESS > 5
                   MOVE IDSV0001-ADDRESS-TYPE(IX-ADDRESS) TO
                        TIPO-ADDRESS
                   EVALUATE TRUE
                   WHEN ADDRESS-CACHE-VAL-VAR
                        SET IDSV0001-ADDRESS        (IX-ADDRESS)
                                             TO ADDRESS OF AREA-IDSV0102
                   END-EVALUATE
           END-PERFORM.

           IF SEQUENTIAL-GUIDE
              MOVE SEQGUIDE-REC           TO AREA-INP-SCARTI
              IF (SCA-POL-ID-POLI-ALF = SPACES
              AND SCA-ADE-ID-ADES-ALF = SPACES)
                 PERFORM  S0000-LEGGI-POLI     THRU S0000-EX
                 IF IDSV0001-ESITO-OK
                    MOVE POL-ID-POLI TO SCA-POL-ID-POLI
                    PERFORM  S0001-LEGGI-ADES  THRU S0001-EX
                 END-IF
              ELSE
                 PERFORM  S0001-LEGGI-ADES  THRU S0001-EX
              END-IF
           END-IF.
       L500-EX.
           EXIT.
      * ----------------------------------------------------------------
      * Chiamata al dispatcher dati per l'accesso alla tab. POLI
      * ----------------------------------------------------------------
       S0000-LEGGI-POLI.

           INITIALIZE  IDSI0011-AREA.

      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
           MOVE WLB-DT-PRODUZIONE      TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
           MOVE WLB-TS-COMPETENZA      TO IDSI0011-DATA-COMPETENZA.
      *--> NOME TABELLA FISICA DB
           MOVE 'POLI'                 TO IDSI0011-CODICE-STR-DATO.
      *--> DCLGEN TABELLA
           MOVE SCA-POL-IB-OGG         TO POL-IB-OGG
           MOVE POLI                   TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.
           SET IDSI0011-IB-OGGETTO          TO TRUE.
           SET IDSI0011-SELECT              TO TRUE.
      *
           SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL      TO TRUE

           PERFORM CALL-DISPATCHER         THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
             EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI   TO POLI
                       MOVE POL-ID-POLI            TO SCA-POL-ID-POLI
                  WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                       MOVE SCA-POL-IB-OGG
                         TO IABV0006-IB-OGG-POLI
                       MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0000-LEGGI-POLI'
                                      TO IEAI9901-LABEL-ERR
                       MOVE '005016'  TO IEAI9901-COD-ERRORE
                       STRING IDSO0011-SQLCODE ' POL-IB-OGG: '
                              SCA-POL-IB-OGG DELIMITED BY SIZE INTO
                              WS-FINE-PARAMETRI-ERR
                       END-STRING
                       STRING 'POLI;'
                              IDSO0011-RETURN-CODE ';'
                              WS-FINE-PARAMETRI-ERR
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> ERRORE DISPATCHER
              MOVE SCA-POL-IB-OGG
                TO IABV0006-IB-OGG-POLI
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0000-LEGGI-POLI'
                                      TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE

              STRING IDSO0011-SQLCODE ' POL-IB-OGG: '
                     SCA-POL-IB-OGG DELIMITED BY SIZE INTO
                     WS-FINE-PARAMETRI-ERR
              END-STRING
              STRING 'POLI;'
                     IDSO0011-RETURN-CODE ';'
                     WS-FINE-PARAMETRI-ERR
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300

           END-IF.

        S0000-EX.
           EXIT.
      * ----------------------------------------------------------------
      * Chiamata al dispatcher dati per l'accesso alla tab. ADES
      * ----------------------------------------------------------------
       S0001-LEGGI-ADES.

           INITIALIZE  IDSI0011-AREA.

      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
           MOVE WLB-DT-PRODUZIONE      TO IDSI0011-DATA-INIZIO-EFFETTO
                                          IDSI0011-DATA-FINE-EFFETTO
           MOVE WLB-TS-COMPETENZA      TO IDSI0011-DATA-COMPETENZA.
      *--> NOME TABELLA FISICA DB
           MOVE 'LDBSC820'             TO IDSI0011-CODICE-STR-DATO.
      *--> DCLGEN TABELLA
           IF  WLB-TP-FRM-ASSVA EQUAL 'EN'
               MOVE 'IN'               TO LC821-TP-FRM-ASSVA1
               MOVE 'CO'               TO LC821-TP-FRM-ASSVA2
           ELSE
               MOVE WLB-TP-FRM-ASSVA   TO LC821-TP-FRM-ASSVA1
               MOVE SPACES             TO LC821-TP-FRM-ASSVA2
           END-IF.
           MOVE SCA-POL-ID-POLI        TO LC821-ID-POLI
           MOVE LDBVC821               TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT              TO TRUE.
           SET IDSI0011-TRATT-X-COMPETENZA  TO TRUE.
           SET IDSI0011-WHERE-CONDITION     TO TRUE.
      *--> TIPO LETTURA
           SET IDSO0011-SUCCESSFUL-RC       TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL      TO TRUE.

           PERFORM CALL-DISPATCHER         THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI   TO LDBVC821
                       MOVE LDBVC821-ADES          TO ADES
                       MOVE LDBVC821-POLI          TO POLI
                       MOVE LDBVC821-STAT-OGG-BUS  TO STAT-OGG-BUS
                  WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                       MOVE SCA-POL-IB-OGG
                         TO IABV0006-IB-OGG-POLI
                       MOVE SCA-POL-ID-POLI
                         TO IABV0006-ID-POLI
                       MOVE WK-PGM    TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0001-LEGGI-ADES'
                                      TO IEAI9901-LABEL-ERR
                       MOVE '005016'  TO IEAI9901-COD-ERRORE
                       STRING IDSO0011-SQLCODE ' POL-IB-OGG: '
                              SCA-POL-IB-OGG DELIMITED BY SIZE INTO
                              WS-FINE-PARAMETRI-ERR
                       STRING 'ADES;'
                              IDSO0011-RETURN-CODE ';'
                              WS-FINE-PARAMETRI-ERR
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> ERRORE DISPATCHER
      *
              MOVE SCA-POL-IB-OGG
                TO IABV0006-IB-OGG-POLI
              MOVE SCA-POL-ID-POLI
                TO IABV0006-ID-POLI
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0001-LEGGI-ADES'
                                      TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              STRING IDSO0011-SQLCODE ' POL-IB-OGG: '
                     SCA-POL-IB-OGG DELIMITED BY SIZE INTO
                     WS-FINE-PARAMETRI-ERR
              STRING 'ADES;'
                     IDSO0011-RETURN-CODE ';'
                     WS-FINE-PARAMETRI-ERR
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S0001-EX.
           EXIT.
      *----------------------------------------------------------------*
      * POST BUSINESS
      *----------------------------------------------------------------*
       L700-POST-BUSINESS.
      *
           MOVE 'L700-POST-BUSINESS'    TO WK-LABEL.

       L700-EX.
           EXIT.
      *----------------------------------------------------------------*
      * POST BUSINESS
      *----------------------------------------------------------------*
       L800-ESEGUI-CALL-BUS.
      *
           MOVE 'L800-ESEGUI-CALL-BUS'    TO WK-LABEL.
      *
      *--
      *--    adeguare la chiamata con le dclgen
      *--    necessarie al Business Service
      *--
      *
           IF IABV0006-GUIDE-AD-HOC
              CALL WK-PGM-BUSINESS       USING AREA-IDSV0001
                                               IABV0006
                                               ADES
                                               POLI
                                               STAT-OGG-BUS
                                               WLB-REC-PREN

           ELSE
              CALL WK-PGM-BUSINESS       USING AREA-IDSV0001
                                               IABV0006
                                               IABV0003
                                               ITSO0041
           END-IF.
      *
       L800-EX.
           EXIT.

      *----------------------------------------------------------------*
      * SOSPENDI STATI
      *----------------------------------------------------------------*
       L801-SOSPENDI-STATI.

           MOVE 'L801-SOSPENDI-STATI'    TO WK-LABEL.


       L801-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CARICA STATI SOSPESI
      *----------------------------------------------------------------*
       L802-CARICA-STATI-SOSPESI.

           MOVE 'L802-CARICA-STATI-SOSPESI'    TO WK-LABEL.

       L802-EX.
           EXIT.
      *----------------------------------------------------------------*
      * PRE SERVIZIO SECONDARIO DI BUSINESS
      *----------------------------------------------------------------*
       L901-PRE-SERV-SECOND-BUS.
      *
           MOVE 'L901-PRE-SERV-SECOND-BUS'    TO WK-LABEL.

       L901-EX.
           EXIT.
      *----------------------------------------------------------------*
      * POST SERVIZIO SECONDARIO DI BUSINESS
      *----------------------------------------------------------------*
       L902-POST-SERV-SECOND-BUS.
      *
           MOVE 'L902-POST-SERV-SECOND-BUS'    TO WK-LABEL.

       L902-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CALL EVENTUALE SERVIZIO SECONDARIO
      * IN FASE DI ESITO KO
      * DEL SERVIZIO BUSINESS
      *----------------------------------------------------------------*
       L900-CALL-SERV-SECOND-BUS.
      *
           MOVE 'L900-CALL-SERV-SECOND-BUS'    TO WK-LABEL.


      *
       L900-EX.
           EXIT.
      *----------------------------------------------------------------*
      * PRE SERVIZIO SECONDARIO DI ROTTURA
      *----------------------------------------------------------------*
       L951-PRE-SERV-SECOND-ROTT.
      *
           MOVE 'L951-PRE-SERV-SECOND-ROTT'    TO WK-LABEL.


       L951-EX.
           EXIT.
      *----------------------------------------------------------------*
      * POST SERVIZIO SECONDARIO DI ROTTURA
      *----------------------------------------------------------------*
       L952-POST-SERV-SECOND-ROTT.
      *
           MOVE 'L952-POST-SERV-SECOND-ROTT'    TO WK-LABEL.

       L952-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CALL EVENTUALE SERVIZIO SECONDARIO
      * IN FASE DI ESITO KO
      * DEL SERVIZIO DI ROTTURA
      *----------------------------------------------------------------*
       L950-CALL-SERV-SECOND-ROTT.
      *
           MOVE 'L950-CALL-SERV-SECOND-ROTT'    TO WK-LABEL.

       L950-EX.
           EXIT.
      *----------------------------------------------------------------*
      * PRE GUIDE AD HOC
      *----------------------------------------------------------------*
       N501-PRE-GUIDE-AD-HOC.
      *
           MOVE 'N501-PRE-GUIDE'    TO WK-LABEL.
      *
           INITIALIZE WLB-REC-PREN.
           MOVE IABV0009-BLOB-DATA-REC        TO WLB-REC-PREN.
           MOVE IABV0006-ID-RICH              TO WLB-ID-RICH.

           CALL WK-PGM-PRE-GUIDE      USING AREA-IDSV0001
                                            WLB-REC-PREN
                                            LLBV0269

      *    AGGIORNO IL RECORD DI PRENOTAZIONE PERCHE' NEL PROGRAMMA
      *    INVOCATO IN PRECEDENZA QUELL'AREA VIENE SPORCATA CON ALTRI
      *    DATI
           MOVE IABV0009-BLOB-DATA-REC        TO WLB-REC-PREN.
           MOVE IABV0006-ID-RICH              TO WLB-ID-RICH.

           IF WLB-TP-RICH = 'B1'

              IF WLB-TP-FRM-ASSVA NOT = SPACES
                 SET IDSV0003-WHERE-CONDITION-01 TO TRUE
              END-IF
              IF WLB-COD-PROD NOT = SPACES
                 SET IDSV0003-WHERE-CONDITION-02 TO TRUE
              END-IF
              IF WLB-IB-POLI-LAST NOT = SPACES
                 SET IDSV0003-WHERE-CONDITION-03 TO TRUE
              END-IF
              IF WLB-IB-ADE-FIRST NOT = SPACES
                 SET IDSV0003-WHERE-CONDITION-04 TO TRUE
              END-IF
NS.           IF WLB-RAMO-BILA NOT = SPACES
                 SET IDSV0003-WHERE-CONDITION-09 TO TRUE
              END-IF
           END-IF

           IF WLB-TP-RICH = 'B2'
              SET IDSV0003-WHERE-CONDITION-05 TO TRUE
           END-IF.

           MOVE WLB-REC-PREN              TO IABV0009-BLOB-DATA-REC.

       N501-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CALL GUIDE
      *----------------------------------------------------------------*
       N502-CALL-GUIDE-AD-HOC.
      *
           MOVE 'N502-CALL-GUIDE-AD-HOC'    TO WK-LABEL.
      *
      *--
      *--    adeguare la chiamata con le dclgen
      *--    necessarie al Guide Service
      *--
      *
           CALL WK-PGM-GUIDA          USING IDSV0003
                                            IABV0002
                                            ADES
                                            POLI
                                            STAT-OGG-BUS
                                            LLBV0269.

      *
       N502-EX.
           EXIT.
      *----------------------------------------------------------------*
      * POST GUIDE
      *----------------------------------------------------------------*
       N504-POST-GUIDE-AD-HOC.
      *
           MOVE 'N504-POST-GUIDE-AD-HOC'    TO WK-LABEL.


       N504-EX.
           EXIT.
      *----------------------------------------------------------------*
      * POST GUIDE
      *----------------------------------------------------------------*
       N506-CNTL-ROTTURA-AD-HOC.
      *
           MOVE 'N506-CNTL-ROTTURA-AD-HOC'    TO WK-LABEL.

       N506-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CONTROLLO IN BASE ALLA SELEZIONE DEL CURSORE GUIDE SERVICE
      *----------------------------------------------------------------*
       N508-CNTL-SELEZIONA-CURSORE.
      *
           MOVE 'N508-CNTL-SELEZIONA-CURSORE'    TO WK-LABEL.

       N508-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CONTROLLA ROTTURA DA ALIMENTAZIONE FLUSSO ESTERNO
      *
      *----------------------------------------------------------------*
       E602-CNTL-ROTTURA-EST.
      *
           MOVE 'E602-CNTL-ROTTURA-EST'          TO WK-LABEL.

       E602-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CALL SERVIZIO ROTTURA
      *----------------------------------------------------------------*
       Q500-CALL-SERV-ROTTURA.
      *
           MOVE 'Q500-CALL-SERV-ROTTURA'    TO WK-LABEL.

       Q500-EX.
           EXIT.
      *----------------------------------------------------------------*
      * PRE SERVIZIO ROTTURA
      *----------------------------------------------------------------*
       Q501-PRE-SERV-ROTTURA.
      *
           MOVE 'Q501-PRE-SERV-ROTTURA'    TO WK-LABEL.

       Q501-EX.
           EXIT.
      *----------------------------------------------------------------*
      * POST SERVIZIO ROTTURA
      *----------------------------------------------------------------*
       Q502-POST-SERV-ROTTURA.
      *
           MOVE 'Q502-POST-SERV-ROTTURA'    TO WK-LABEL.

       Q502-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTIENE STATEMENTS PER LA FASE MACRO DEL BATCH EXECUTOR
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IABP0011 END-EXEC.

      *----------------------------------------------------------------*
      *    CONTIENE STATEMENTS PER LA FASE MICRO DEL BATCH EXECUTOR
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IABP0012 END-EXEC.

      **************************************************************
      * COPY DA UTILIZZARE PRETTAMENTE
      *        SENZA Gestione Guida Sequenziale
      * da inserire in caso di gestione guida da sequenziale
      * con la copy IABPSEQn (n = numero di Sequenziali da UTILIZZARE)
      **************************************************************
      *     COPY IABPSQG0.
            COPY IABPSQG1.
      *----------------------------------------------------------------*
      *    ROUTINES DISPATCHER
      *----------------------------------------------------------------*
            COPY IDSP0012.
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.


      ******************************************************************
      **                                                        ********
      **                                                        ********
      **                   BATCH EXECUTOR                       ********
      **                                                        ********
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LRGM0380.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.  2014.
       DATE-COMPILED.
      ******************************************************************
      *    PROGRAAMMA .... LRGM0380
      *    FUNZIONE ...... BATCH EXECUTOR
      ******************************************************************
      * GAP BNL - ESTRATTO CONTO (11-Reporting Tecnico Gestionale)
      * Batch Diagnostica E/C
      ******************************************************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.

      *---> FILE PARAM DI INPUT
           SELECT PBTCEXEC ASSIGN    TO PBTCEXEC
           ACCESS IS SEQUENTIAL
           FILE STATUS  IS FS-PARA.

      *---> FILE REPORT01 DI OUTPUT
           SELECT REPORT01 ASSIGN    TO REPORT01
           ACCESS IS SEQUENTIAL
           FILE STATUS  IS FS-REPORT01.


      ******************************************************************
      *--          GESTIONE GUIDA DA SEQUENZIALE
      ******************************************************************
      * da inserire in caso di gestione guida da sequenziale
      ******************************************************************
      *---> FILE INPUT GUIDE
           SELECT SEQGUIDE ASSIGN    TO SEQGUIDE
           ACCESS IS SEQUENTIAL
           FILE STATUS  IS FS-SEQGUIDE.

      ******************************************************************

       DATA DIVISION.
       FILE SECTION.
       FD  PBTCEXEC.
       01  PARAM-REC                      PIC X(80).

       FD  REPORT01.
       01  REPORT01-REC                   PIC X(500).


      ******************************************************************
      *--          GESTIONE GUIDA DA SEQUENZIALE
      ******************************************************************
      * da inserire in caso di gestione guida da sequenziale
      ******************************************************************
       FD  SEQGUIDE.
       01  SEQGUIDE-REC                   PIC X(2500).

      *************************************************
      * W O R K I N G   S T O R A G E   S E C T I O N *
      *************************************************
       WORKING-STORAGE SECTION.
       77  WK-PGM                         PIC X(008)  VALUE 'LRGM0380'.
       77  WK-FILE-INP                    PIC X(008)  VALUE 'PBTCEXEC'.

      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-IND                      PIC S9(04).

      ******************************************************************
      *--          Gestione Guida da Sequenziale
      ******************************************************************
      * da inserire in caso di gestione Guida da Sequenziale
      ******************************************************************
           COPY IABVSQG1.

      ******************************************************************
      * STRUTTURA MESSAGGIO X FILES SEQUENZIALI
      ******************************************************************
           COPY IABCSQ99.

      ******************************************************************
      * GESTIONE DI ILTERIORI FILE IN INPUT
      ******************************************************************
           COPY IABVSQM1.

      *-- Inizio PGM-USED
      *--
      *--    valorizzare i campi PGM
      *--    per un 'eventuale servizio in caso di rottura chiave
      *--
       77  WK-PGM-SERV-ROTTURA            PIC X(008) VALUE  SPACES.
       77  WK-PGM-SERV-SECON-BUS          PIC X(008) VALUE  SPACES.
       77  WK-PGM-SERV-SECON-ROTT         PIC X(008) VALUE  SPACES.
      *-- Fine PGM-USED

       77  LCCS0005                       PIC X(008) VALUE  'LCCS0005'.
      *---------------------------------------------------------------*
      * STRUTTURA WHERE CONDITION
      * in caso di gestione MULTICURSORE
      *---------------------------------------------------------------*

      *-- BUFFER DA SCHEDA PARAMETRO
       01 BUFFER-WHERE-CONDITION.
          05 WK-DT-RICOR-DA               PIC 9(008).
          05 WK-DT-RICOR-A                PIC 9(008).
          05 FILLER                       PIC X(284).

      *01 AREA-CONTATORI.
      *   03 WS-NUM-LETTI                PIC 9(09).
      *   03 WS-NUM-SCART                PIC 9(09).
      ******************************************************************
      * CAMPI COMODO PER GESTIONE ROTTURA CHIAVE
      ******************************************************************
       01 WK-CHIAVE-ROTTURA.
          05 WK-ID-PADRE1-OLD             PIC S9(9)V COMP-3.
          05 WK-ID-PADRE2-OLD             PIC S9(9)V COMP-3.

       01 WK-NEW-IB-OGG                   PIC X(40) VALUE SPACES.
       01 WK-OLD-IB-OGG                   PIC X(40) VALUE SPACES.
NEW    01 FLAG-LANCIO PIC X(2).
NEW       88 PRIMO-LANCIO-SI          VALUE 'SI'.
NEW       88 PRIMO-LANCIO-NO          VALUE 'NO'.

      *---------------------------------------------------------------*
      * AREA DI APPOGGIO PER SCAMBIO DATI CON BUSINESS
      *---------------------------------------------------------------*
       01 WK-APPO-DATA-JOB.
          05 WK-APPO-ELE-MAX                     PIC 9(0003).
          05 WK-APPO-BLOB-DATA                OCCURS 1000 TIMES.
             10 WK-APPO-BLOB-DATA-REC            PIC X(2500).

      *---------------------------------------------------------------*
      * COPY AREE DI APPOGGIO
      *---------------------------------------------------------------*
      *--  COMUNE
       01  WCOM-IO-STATI.
           COPY LCCC0001             REPLACING ==(SF)==  BY ==WCOM==.
      * -- MOVIMENTO
       01  WMOV-AREA-MOVIMENTO.
           04 WMOV-ELE-MOV-MAX             PIC S9(004) COMP.
           04 WMOV-TAB-MOV.
           COPY LCCVMOV1             REPLACING ==(SF)==  BY ==WMOV==.
      * -- RICHIESTA
       01  WRIC-AREA-RICHIESTA.
           04 WRIC-ELE-RICH-MAX            PIC S9(004) COMP.
           04 WRIC-TAB-RICH.
           COPY LCCVRIC1             REPLACING ==(SF)==  BY ==WRIC==.
      * -- POLIZZA
       01  WPOL-AREA-POLIZZA.
           04 WPOL-ELE-POLI-MAX            PIC S9(04) COMP.
           04 WPOL-TAB-POLI.
           COPY LCCVPOL1              REPLACING ==(SF)==   BY ==WPOL==.
      * -- ADESIONE
       01  WADE-AREA-ADESIONE.
           04 WADE-ELE-ADES-MAX            PIC S9(004) COMP.
           04 WADE-TAB-ADES.
           COPY LCCVADE1              REPLACING ==(SF)==   BY ==WADE==.
      * -- GARANZIA
       01  WGRZ-AREA-GARANZIA.
           04 WGRZ-ELE-GAR-MAX             PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
           COPY LCCVGRZ1              REPLACING ==(SF)==   BY ==WGRZ==.
      * -- TRANCHE DI GARANZIA
       01  WTGA-AREA-TRANCHE.
           04 WTGA-ELE-TRAN-MAX            PIC S9(04) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==WTGA==.
           COPY LCCVTGA1              REPLACING ==(SF)==   BY ==WTGA==.
      * -- RAPPORTO RETE
       01  WRRE-AREA-RAPP-RETE.
           04 WRRE-ELE-RAPP-RETE-MAX       PIC S9(04) COMP.
           04 WRRE-TAB-RAPP-RETE      OCCURS 20.
           COPY LCCVRRE1              REPLACING ==(SF)== BY ==WRRE==.
      * --  AREA NOTE OGGETTO
       01  WNOT-AREA-NOTE-OGG.
           04 WNOT-ELE-NOTE-OGG-MAX        PIC S9(004) COMP.
           04 WNOT-TAB-NOTE-OGG.
           COPY LCCVNOT1              REPLACING ==(SF)== BY ==WNOT==.
      *
       01  AREA-LCCV0005.
      * -- Interfaccia servizio EOC comune
           COPY LCCV0005.

      *-- Inizio Include Batch Executor
      *---------------------------------------------------------------*
      * COPY INFRASTRUTTURALE PER BATCH EXECUTOR
      *---------------------------------------------------------------*
           EXEC SQL INCLUDE IABV0007 END-EXEC.
           EXEC SQL INCLUDE IABV0008 END-EXEC.

      *---------------------------------------------------------------*
      * AREA TABELLE DB2 PER BATCH EXECUTOR
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

           EXEC SQL INCLUDE IDBVBBS1 END-EXEC.
           EXEC SQL INCLUDE IDBVBBT1 END-EXEC.
           EXEC SQL INCLUDE IDBVBEM1 END-EXEC.
           EXEC SQL INCLUDE IDBVBES1 END-EXEC.
           EXEC SQL INCLUDE IDBVBJE1 END-EXEC.
           EXEC SQL INCLUDE IDBVBJS1 END-EXEC.
           EXEC SQL INCLUDE IDBVBRS1 END-EXEC.
           EXEC SQL INCLUDE IDBVBTC1 END-EXEC.
           EXEC SQL INCLUDE IDBVPCO1 END-EXEC.
           EXEC SQL INCLUDE IDBVLOR1 END-EXEC.
           EXEC SQL INCLUDE IDBVGRU1 END-EXEC.
           EXEC SQL INCLUDE IDBVBPA1 END-EXEC.
           EXEC SQL INCLUDE IDBVRIC1 END-EXEC.

      *-- Fine Include Batch Executor

       01  IABV0006.
           COPY IABV0006.

      *----------------------------------------------------------------*
      *    Flusso dati Emesso Bancassurance
      *    COPY TRACCIATO RECORD INPUT
      *----------------------------------------------------------------*
       01  WRIN-REC-FILEIN.
           COPY LRGC0031                 REPLACING ==(SF)== BY ==WRIN==.

           COPY LRGC0041.
      *---------------------------------------------------------------*
      * AREA PER CONTATORI PERSONALIZZATI
      *---------------------------------------------------------------*
       01 WK-AREA-CONTATORI.
          03 WK-CTR-1.
             05 DESC-CTR-1           PIC X(050)
                 VALUE '   POLIZZE LETTE          '.
             05 REC-ELABORATI        PIC 9(002) VALUE 1.
          03 WK-CTR-2.
             05 DESC-CTR-2           PIC X(050)
                 VALUE '   POLIZZE DA SCARTARE '.
             05 REC-SCARTATI         PIC 9(002) VALUE 2.
          03 WK-CTR-3.
             05 DESC-CTR-3           PIC X(050)
                 VALUE 'TOTALE PERCENTUALE INVIABILI  %               '.
             05 REC-PERC             PIC 9(002) VALUE 3.
      *----------------------------------------------------------------*
      *    COPY GESTIONE ERRORI EXTRA
      *----------------------------------------------------------------*
           COPY IEAV9904.

      ******************************************************************
      * P R O C E D U R E   D I V I S I O N                            *
      ******************************************************************
       PROCEDURE DIVISION.

           MOVE 'PROCEDURE DIVISION'     TO WK-LABEL.

           PERFORM A000-OPERAZ-INIZ      THRU A000-EX.

           IF WK-ERRORE-NO
              PERFORM B000-ELABORA-MACRO THRU B000-EX
           END-IF.

           PERFORM Z000-OPERAZ-FINALI    THRU Z000-OPERAZ-FINALI-FINE.

           STOP RUN.

      *----------------------------------------------------------------*
      * OPERAZIONI INIZIALI                                            *
      *----------------------------------------------------------------*
       A001-OPERAZIONI-INIZIALI.

           MOVE 'A001-OPERAZ-INIZIALI'       TO WK-LABEL.

           INITIALIZE                           IX-INDICI.
           SET PRIMO-LANCIO-SI TO TRUE.

      *--> VALORIZZO AREA PER LA GESTIONE PERSONALIZZATA DEI CONTATORI
           INITIALIZE IABV0006-CUSTOM-COUNTERS.
           MOVE 10                     TO IABV0006-LIM-CUSTOM-COUNT-MAX
           MOVE 10                     TO IABV0006-MAX-ELE-CUSTOM-COUNT
           SET  IABV0006-COUNTER-STD-YES TO TRUE
           SET  IABV0006-GUIDE-ROWS-READ-YES TO TRUE

      *--> CONTATORE 1
           SET  IABV0006-COUNTER-DESC(REC-ELABORATI)
             TO TRUE
           MOVE DESC-CTR-1
             TO IABV0006-CUSTOM-COUNT-DESC(REC-ELABORATI)
           MOVE ZEROES
             TO IABV0006-CUSTOM-COUNT(REC-ELABORATI).

      *--> CONTATORE 2
           SET  IABV0006-COUNTER-DESC(REC-SCARTATI)
             TO TRUE
           MOVE DESC-CTR-2
             TO IABV0006-CUSTOM-COUNT-DESC(REC-SCARTATI)
           MOVE ZEROES
             TO IABV0006-CUSTOM-COUNT(REC-SCARTATI).

      *--> CONTATORE 3
           SET  IABV0006-COUNTER-DESC(REC-PERC)
             TO TRUE
           MOVE DESC-CTR-3
             TO IABV0006-CUSTOM-COUNT-DESC(REC-PERC)
           MOVE ZEROES
             TO IABV0006-CUSTOM-COUNT(REC-PERC).

       A001-EX.
           EXIT.
      *----------------------------------------------------------------*
      * OPERAZIONI FINALI                                              *
      *----------------------------------------------------------------*
       Z001-OPERAZIONI-FINALI.

           MOVE 'Z001-OPERAZIONI-FINALI'     TO WK-LABEL.

       Z001-EX.
           EXIT.
      *----------------------------------------------------------------*
      * OPERAZIONI FINALI X SINGOLA RICHIESTA DI ELABORAZIONE(ID-BATCH)*
      *----------------------------------------------------------------*
       Z002-OPERAZ-FINALI-X-BATCH.
      *
           MOVE 'Z002-OPERAZ-FINALI-X-BATCH' TO WK-LABEL.
      *
      *--
      *--    eventuali statements da eseguire x ID-BATCH
      *--
      *--

       Z002-EX.
           EXIT.
      *----------------------------------------------------------------*
      * DISPLAY INIZIO ELABORAZIONE
      *----------------------------------------------------------------*
       A101-DISPLAY-INIZIO-ELABORA.

           DISPLAY '***********************************************'.
           DISPLAY '* NOME PROGRAMMA    = ' WK-PGM.
           DISPLAY '* DESCRIZIONE       = Inizio elaborazione di '
           DISPLAY '* BATCH EXECUTOR '.
           DISPLAY '* DATA ELABORAZIONE = ' WK-DATA-SISTEMA.
           DISPLAY '* ORA  ELABORAZIONE = ' WK-ORA-SISTEMA.
           DISPLAY '************************************************'.
           DISPLAY ALL SPACES.

       A101-EX.
           EXIT.
      *----------------------------------------------------------------*
      * DISPLAY FINE ELABORAZIONE
      *----------------------------------------------------------------*
       A102-DISPLAY-FINE-ELABORA.

           DISPLAY '***********************************************'.
           DISPLAY '* NOME PROGRAMMA    = ' WK-PGM.
           DISPLAY '* DESCRIZIONE       = Fine elaborazione di '
           DISPLAY '* BATCH EXECUTOR '.
           DISPLAY '* DATA ELABORAZIONE = ' WK-DATA-SISTEMA.
           DISPLAY '* ORA  ELABORAZIONE = ' WK-ORA-SISTEMA.
           DISPLAY '************************************************'.
           DISPLAY ALL SPACES.

       A102-EX.
           EXIT.
      *----------------------------------------------------------------*
      * PRE BUSINESS
      *----------------------------------------------------------------*
       L500-PRE-BUSINESS.
           MOVE 'L500-PRE-BUSINESS'                TO WK-LABEL.

           MOVE IDSV0003-BUFFER-WHERE-COND
             TO BUFFER-WHERE-CONDITION

           IF WK-FINE-ALIMENTAZIONE-YES
              CONTINUE
           ELSE
              MOVE SEQGUIDE-REC                    TO WRIN-REC-FILEIN
              IF PRIMA-VOLTA-SUSP-YES
                 SET PRIMA-VOLTA-SUSP-NO         TO TRUE
                 SET GESTIONE-SUSPEND            TO TRUE
                 SET WK-LANCIA-BUSINESS-NO       TO TRUE
                 ADD  1                          TO IX-IND
                 MOVE IX-IND                     TO WK-APPO-ELE-MAX
                 MOVE WRIN-REC-FILEIN
                   TO WK-APPO-BLOB-DATA-REC(IX-IND)
              ELSE
                 IF WRIN-TP-RECORD = '0001'
                    IF PRIMO-LANCIO-SI
NEW                    SET  PRIMO-LANCIO-NO         TO TRUE
                    ELSE
                       SET WK-LANCIA-BUSINESS-YES   TO TRUE
                    END-IF
      *             WRIN-TP-RECORD = '0006' OR
      *             WRIN-TP-RECORD = '0007' OR
      *             WRIN-TP-RECORD = '0008' OR
      *             WRIN-TP-RECORD = '0009' OR
      *             WRIN-TP-RECORD = '0010'
                 ELSE
                    SET WK-LANCIA-BUSINESS-NO    TO TRUE
                    ADD  1                       TO IX-IND
                    MOVE IX-IND                  TO WK-APPO-ELE-MAX
                    MOVE WRIN-REC-FILEIN
                      TO WK-APPO-BLOB-DATA-REC(IX-IND)
                 END-IF
              END-IF
           END-IF.

       L500-EX.
           EXIT.
      *----------------------------------------------------------------*
      * POST BUSINESS
      *----------------------------------------------------------------*
       L700-POST-BUSINESS.

           MOVE 'L700-POST-BUSINESS'    TO WK-LABEL.

      *--
      *--    eventuali statements da eseguire
      *--    dopo l' esecuzione del Business Service
      *    DISPLAY '2-IABV0006-CUSTOM-COUNT(1): '
      *             IABV0006-CUSTOM-COUNT(1)    .
      *    DISPLAY '2-IABV0006-CUSTOM-COUNT(2): '
      *             IABV0006-CUSTOM-COUNT(2)    .

       L700-EX.
           EXIT.
      *----------------------------------------------------------------*
      * POST BUSINESS
      *----------------------------------------------------------------*
       L800-ESEGUI-CALL-BUS.

           MOVE 'L800-ESEGUI-CALL-BUS'    TO WK-LABEL.

           CALL WK-PGM-BUSINESS           USING AREA-IDSV0001
                                                WCOM-IO-STATI
                                                IABV0006
                                                WK-APPO-DATA-JOB
      *                                         AREA-CONTATORI
           ON EXCEPTION
              STRING 'ERRORE CHIAMATA MODULO : '
                      DELIMITED BY SIZE
                      WK-PGM-BUSINESS
                      DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              SET WK-ERRORE-BLOCCANTE     TO TRUE
              MOVE 4                      TO LOR-ID-GRAVITA-ERRORE
              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
           END-CALL.

           INITIALIZE                WK-APPO-DATA-JOB.
           MOVE 1                 TO IX-IND.
           MOVE IX-IND            TO WK-APPO-ELE-MAX.
           MOVE WRIN-REC-FILEIN   TO WK-APPO-BLOB-DATA-REC(IX-IND).
           IF WRIN-TP-RECORD = '0001'
               INITIALIZE WK-AP-GAR-FND
           END-IF.
       L800-EX.
           EXIT.
      *----------------------------------------------------------------*
      * SOSPENDI STATI
      *----------------------------------------------------------------*
       L801-SOSPENDI-STATI.

           MOVE 'L801-SOSPENDI-STATI'    TO WK-LABEL.

      *--    ��� SOLO CON GUIDE SERVICE DA DB ���
      *--    adeguare la valorizzazione
      *--    del campo WK-STATI-SOSP-PK(IND-STATI-SOSP) con il campo PK
      *--    della dclgen necessaria al Guide Service

      *     MOVE XXX-DS-RIGA        TO WK-STATI-SOSP-PK(IND-STATI-SOSP).

       L801-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CARICA STATI SOSPESI
      *----------------------------------------------------------------*
       L802-CARICA-STATI-SOSPESI.

           MOVE 'L802-CARICA-STATI-SOSPESI'    TO WK-LABEL.

      *--    ��� SOLO CON GUIDE SERVICE DA DB ���
      *--    adeguare la valorizzazione con il campo PK
      *--    della dclgen necessaria al Guide Service
      *--    conil campo WK-STATI-SOSP-PK(IND-STATI-SOSP)

      *     MOVE WK-STATI-SOSP-PK(IND-STATI-SOSP) TO XXX-DS-RIGA.

       L802-EX.
           EXIT.
      *----------------------------------------------------------------*
      * PRE SERVIZIO SECONDARIO DI BUSINESS
      *----------------------------------------------------------------*
       L901-PRE-SERV-SECOND-BUS.

           MOVE 'L901-PRE-SERV-SECOND-BUS'    TO WK-LABEL.

      *--
      *--    eventuali statements da eseguire
      *--    prima dell' esecuzione del Servizio Secondario
      *--    (es. si puo' passare un eventuale input
      *--     al Servizio Secondario    )

       L901-EX.
           EXIT.
      *----------------------------------------------------------------*
      * POST SERVIZIO SECONDARIO DI BUSINESS
      *----------------------------------------------------------------*
       L902-POST-SERV-SECOND-BUS.

           MOVE 'L902-POST-SERV-SECOND-BUS'    TO WK-LABEL.

      *--
      *--    eventuali statements da eseguire
      *--    dopo l' esecuzione del Servizio Secondario
      *--
      *

       L902-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CALL EVENTUALE SERVIZIO SECONDARIO
      * IN FASE DI ESITO KO
      * DEL SERVIZIO BUSINESS
      *----------------------------------------------------------------*
       L900-CALL-SERV-SECOND-BUS.

           MOVE 'L900-CALL-SERV-SECOND-BUS'    TO WK-LABEL.

      *--
      *--    adeguare la chiamata con le aree
      *--    necessarie al Servizio Secondario
      *--    (SIA ALIM. FLUSSI ESTERNI , SIA ALIM. PORTAFOGLIO VITA)
      *--
      *
      *
      *--
      *--    VALORIZZARE IL CAMPO  WK-PGM-SERV-SECON-BUS
      *--    per un 'eventuale servizio in caso di rottura chiave
      *--
      *

       L900-EX.
           EXIT.
      *----------------------------------------------------------------*
      * PRE SERVIZIO SECONDARIO DI ROTTURA
      *----------------------------------------------------------------*
       L951-PRE-SERV-SECOND-ROTT.

           MOVE 'L951-PRE-SERV-SECOND-ROTT'    TO WK-LABEL.

      *--
      *--    eventuali statements da eseguire
      *--    prima dell' esecuzione del Servizio Secondario
      *--    (es. si puo' passare un eventuale input
      *--     al Servizio Secondario    )
      *

       L951-EX.
           EXIT.
      *----------------------------------------------------------------*
      * POST SERVIZIO SECONDARIO DI ROTTURA
      *----------------------------------------------------------------*
       L952-POST-SERV-SECOND-ROTT.

           MOVE 'L952-POST-SERV-SECOND-ROTT'    TO WK-LABEL.
      *
      *--
      *--    eventuali statements da eseguire
      *--    dopo l' esecuzione del Servizio Secondario
      *--

       L952-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CALL EVENTUALE SERVIZIO SECONDARIO
      * IN FASE DI ESITO KO
      * DEL SERVIZIO DI ROTTURA
      *----------------------------------------------------------------*
       L950-CALL-SERV-SECOND-ROTT.

           MOVE 'L950-CALL-SERV-SECOND-ROTT'    TO WK-LABEL.

      *--
      *--    adeguare la chiamata con le aree
      *--    necessarie al Servizio Secondario
      *--    (SIA ALIM. FLUSSI ESTERNI , SIA ALIM. PORTAFOGLIO VITA)
      *--
      *

       L950-EX.
           EXIT.
      *----------------------------------------------------------------*
      * PRE GUIDE AD HOC
      *----------------------------------------------------------------*
       N501-PRE-GUIDE-AD-HOC.

           MOVE 'N501-PRE-GUIDE'    TO WK-LABEL.

      *--
      *--  eventuali statements da eseguire
      *--  prima dell' esecuzione del Guide Service
      *--
      *
      *--
      *--  eventuale selezione del cursore dichiarato
      *--  nel Guide Service con preventivo mapping dell' area buffer
      *--  della scheda parametro
      *--  INIZIO ESEMPIO :
      *--

       N501-EX.
           EXIT.
      ******************************************************************
      *--          GESTIONE GUIDA DA SEQUENZIALE
      ******************************************************************
      * In caso di Gestione Guida da Sequenziale NON si rende necessaria
      * l' utilizzo di questa label (N502-), pertanto basta inserire
      * solo un "CONTINUE" nella suddetta label.
      ******************************************************************
       N502-CALL-GUIDE-AD-HOC.

           MOVE 'N502-CALL-GUIDE-AD-HOC'    TO WK-LABEL.

      *--
      *--    adeguare la chiamata con le dclgen
      *--    necessarie al Guide Service
      *--

       N502-EX.
           EXIT.
      *----------------------------------------------------------------*
      * POST GUIDE
      *----------------------------------------------------------------*
       N504-POST-GUIDE-AD-HOC.

           MOVE 'N504-POST-GUIDE-AD-HOC'    TO WK-LABEL.

       N504-EX.
           EXIT.
      *----------------------------------------------------------------*
      * POST GUIDE
      *----------------------------------------------------------------*
       N506-CNTL-ROTTURA-AD-HOC.

           MOVE 'N506-CNTL-ROTTURA-AD-HOC'  TO WK-LABEL.

       N506-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CONTROLLO IN BASE ALLA SELEZIONE DEL CURSORE GUIDE SERVICE
      *----------------------------------------------------------------*
       N508-CNTL-SELEZIONA-CURSORE.

           MOVE 'N508-CNTL-SELEZIONA-CURSORE'    TO WK-LABEL.
      *
      *--
      *--    eventuale controllo in base alla selezione
      *--    del cursore dichiarato nel Guide Service
      *--      (ESEMPIO :
      *--
      *--  IF IDSV0003-CURSOR-01
      *--     MOVE .............
      *--  END-IF.

       N508-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CONTROLLA ROTTURA DA ALIMENTAZIONE FLUSSO ESTERNO
      *----------------------------------------------------------------*
       E602-CNTL-ROTTURA-EST.

           MOVE 'E602-CNTL-ROTTURA-EST'         TO WK-LABEL.

       E602-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CALL SERVIZIO ROTTURA
      *----------------------------------------------------------------*
       Q500-CALL-SERV-ROTTURA.

           MOVE 'Q500-CALL-SERV-ROTTURA'        TO WK-LABEL.

       Q500-EX.
           EXIT.
      *----------------------------------------------------------------*
      * PRE SERVIZIO ROTTURA
      *----------------------------------------------------------------*
       Q501-PRE-SERV-ROTTURA.

           MOVE 'Q501-PRE-SERV-ROTTURA'         TO WK-LABEL.

       Q501-EX.
           EXIT.
      *----------------------------------------------------------------*
      * POST SERVIZIO ROTTURA
      *----------------------------------------------------------------*
       Q502-POST-SERV-ROTTURA.

           MOVE 'Q502-POST-SERV-ROTTURA'        TO WK-LABEL.

       Q502-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTIENE STATEMENTS PER LA FASE MACRO DEL BATCH EXECUTOR
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IABP0011 END-EXEC.

      *----------------------------------------------------------------*
      *    CONTIENE STATEMENTS PER LA FASE MICRO DEL BATCH EXECUTOR
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IABP0012 END-EXEC.

      ******************************************************************
      *--          GESTIONE FILE SEQUENZIALI
      ******************************************************************
      * - Copy da inserire X Guida da Sequenziale (SEQGUIDE) : IABPSQG1
      ******************************************************************
           COPY IABPSQG1.

      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.


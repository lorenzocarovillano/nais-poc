       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSP560 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  26 MAR 2019.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDP560 END-EXEC.
           EXEC SQL INCLUDE IDBVP562 END-EXEC.
           EXEC SQL INCLUDE IDBVP563 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVP561 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 QUEST-ADEG-VER.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSP560'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'QUEST_ADEG_VER' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_QUEST_ADEG_VER
                ,COD_COMP_ANIA
                ,ID_MOVI_CRZ
                ,ID_RAPP_ANA
                ,ID_POLI
                ,NATURA_OPRZ
                ,ORGN_FND
                ,COD_QLFC_PROF
                ,COD_NAZ_QLFC_PROF
                ,COD_PRV_QLFC_PROF
                ,FNT_REDD
                ,REDD_FATT_ANNU
                ,COD_ATECO
                ,VALUT_COLL
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,LUOGO_COSTITUZIONE
                ,TP_MOVI
                ,FL_RAG_RAPP
                ,FL_PRSZ_TIT_EFF
                ,TP_MOT_RISC
                ,TP_PNT_VND
                ,TP_ADEG_VER
                ,TP_RELA_ESEC
                ,TP_SCO_FIN_RAPP
                ,FL_PRSZ_3O_PAGAT
                ,AREA_GEO_PROV_FND
                ,TP_DEST_FND
                ,FL_PAESE_RESID_AUT
                ,FL_PAESE_CIT_AUT
                ,FL_PAESE_NAZ_AUT
                ,COD_PROF_PREC
                ,FL_AUT_PEP
                ,FL_IMP_CAR_PUB
                ,FL_LIS_TERR_SORV
                ,TP_SIT_FIN_PAT
                ,TP_SIT_FIN_PAT_CON
                ,IMP_TOT_AFF_UTIL
                ,IMP_TOT_FIN_UTIL
                ,IMP_TOT_AFF_ACC
                ,IMP_TOT_FIN_ACC
                ,TP_FRM_GIUR_SAV
                ,REG_COLL_POLI
                ,NUM_TEL
                ,NUM_DIP
                ,TP_SIT_FAM_CONV
                ,COD_PROF_CON
                ,FL_ES_PROC_PEN
                ,TP_COND_CLIENTE
                ,COD_SAE
                ,TP_OPER_ESTERO
                ,STAT_OPER_ESTERO
                ,COD_PRV_SVOL_ATT
                ,COD_STAT_SVOL_ATT
                ,TP_SOC
                ,FL_IND_SOC_QUOT
                ,TP_SIT_GIUR
                ,PC_QUO_DET_TIT_EFF
                ,TP_PRFL_RSH_PEP
                ,TP_PEP
                ,FL_NOT_PREG
                ,DT_INI_FNT_REDD
                ,FNT_REDD_2
                ,DT_INI_FNT_REDD_2
                ,FNT_REDD_3
                ,DT_INI_FNT_REDD_3
                ,MOT_ASS_TIT_EFF
                ,FIN_COSTITUZIONE
                ,DESC_IMP_CAR_PUB
                ,DESC_SCO_FIN_RAPP
                ,DESC_PROC_PNL
                ,DESC_NOT_PREG
                ,ID_ASSICURATI
                ,REDD_CON
                ,DESC_LIB_MOT_RISC
                ,TP_MOT_ASS_TIT_EFF
                ,TP_RAG_RAPP
                ,COD_CAN
                ,TP_FIN_COST
                ,NAZ_DEST_FND
                ,FL_AU_FATCA_AEOI
                ,TP_CAR_FIN_GIUR
                ,TP_CAR_FIN_GIUR_AT
                ,TP_CAR_FIN_GIUR_PA
                ,FL_ISTITUZ_FIN
                ,TP_ORI_FND_TIT_EFF
                ,PC_ESP_AG_PA_MSC
                ,FL_PR_TR_USA
                ,FL_PR_TR_NO_USA
                ,PC_RIP_PAT_AS_VITA
                ,PC_RIP_PAT_IM
                ,PC_RIP_PAT_SET_IM
                ,TP_STATUS_AEOI
                ,TP_STATUS_FATCA
                ,FL_RAPP_PA_MSC
                ,COD_COMUN_SVOL_ATT
                ,TP_DT_1O_CON_CLI
                ,TP_MOD_EN_RELA_INT
                ,TP_REDD_ANNU_LRD
                ,TP_REDD_CON
                ,TP_OPER_SOC_FID
                ,COD_PA_ESP_MSC_1
                ,IMP_PA_ESP_MSC_1
                ,COD_PA_ESP_MSC_2
                ,IMP_PA_ESP_MSC_2
                ,COD_PA_ESP_MSC_3
                ,IMP_PA_ESP_MSC_3
                ,COD_PA_ESP_MSC_4
                ,IMP_PA_ESP_MSC_4
                ,COD_PA_ESP_MSC_5
                ,IMP_PA_ESP_MSC_5
                ,DESC_ORGN_FND
                ,COD_AUT_DUE_DIL
                ,FL_PR_QUEST_FATCA
                ,FL_PR_QUEST_AEOI
                ,FL_PR_QUEST_OFAC
                ,FL_PR_QUEST_KYC
                ,FL_PR_QUEST_MSCQ
                ,TP_NOT_PREG
                ,TP_PROC_PNL
                ,COD_IMP_CAR_PUB
                ,OPRZ_SOSPETTE
                ,ULT_FATT_ANNU
                ,DESC_PEP
                ,NUM_TEL_2
                ,IMP_AFI
                ,FL_NEW_PRO
                ,TP_MOT_CAMBIO_CNTR
                ,DESC_MOT_CAMBIO_CN
                ,COD_SOGG
             INTO
                :P56-ID-QUEST-ADEG-VER
               ,:P56-COD-COMP-ANIA
               ,:P56-ID-MOVI-CRZ
               ,:P56-ID-RAPP-ANA
                :IND-P56-ID-RAPP-ANA
               ,:P56-ID-POLI
               ,:P56-NATURA-OPRZ
                :IND-P56-NATURA-OPRZ
               ,:P56-ORGN-FND
                :IND-P56-ORGN-FND
               ,:P56-COD-QLFC-PROF
                :IND-P56-COD-QLFC-PROF
               ,:P56-COD-NAZ-QLFC-PROF
                :IND-P56-COD-NAZ-QLFC-PROF
               ,:P56-COD-PRV-QLFC-PROF
                :IND-P56-COD-PRV-QLFC-PROF
               ,:P56-FNT-REDD
                :IND-P56-FNT-REDD
               ,:P56-REDD-FATT-ANNU
                :IND-P56-REDD-FATT-ANNU
               ,:P56-COD-ATECO
                :IND-P56-COD-ATECO
               ,:P56-VALUT-COLL
                :IND-P56-VALUT-COLL
               ,:P56-DS-OPER-SQL
               ,:P56-DS-VER
               ,:P56-DS-TS-CPTZ
               ,:P56-DS-UTENTE
               ,:P56-DS-STATO-ELAB
               ,:P56-LUOGO-COSTITUZIONE-VCHAR
                :IND-P56-LUOGO-COSTITUZIONE
               ,:P56-TP-MOVI
                :IND-P56-TP-MOVI
               ,:P56-FL-RAG-RAPP
                :IND-P56-FL-RAG-RAPP
               ,:P56-FL-PRSZ-TIT-EFF
                :IND-P56-FL-PRSZ-TIT-EFF
               ,:P56-TP-MOT-RISC
                :IND-P56-TP-MOT-RISC
               ,:P56-TP-PNT-VND
                :IND-P56-TP-PNT-VND
               ,:P56-TP-ADEG-VER
                :IND-P56-TP-ADEG-VER
               ,:P56-TP-RELA-ESEC
                :IND-P56-TP-RELA-ESEC
               ,:P56-TP-SCO-FIN-RAPP
                :IND-P56-TP-SCO-FIN-RAPP
               ,:P56-FL-PRSZ-3O-PAGAT
                :IND-P56-FL-PRSZ-3O-PAGAT
               ,:P56-AREA-GEO-PROV-FND
                :IND-P56-AREA-GEO-PROV-FND
               ,:P56-TP-DEST-FND
                :IND-P56-TP-DEST-FND
               ,:P56-FL-PAESE-RESID-AUT
                :IND-P56-FL-PAESE-RESID-AUT
               ,:P56-FL-PAESE-CIT-AUT
                :IND-P56-FL-PAESE-CIT-AUT
               ,:P56-FL-PAESE-NAZ-AUT
                :IND-P56-FL-PAESE-NAZ-AUT
               ,:P56-COD-PROF-PREC
                :IND-P56-COD-PROF-PREC
               ,:P56-FL-AUT-PEP
                :IND-P56-FL-AUT-PEP
               ,:P56-FL-IMP-CAR-PUB
                :IND-P56-FL-IMP-CAR-PUB
               ,:P56-FL-LIS-TERR-SORV
                :IND-P56-FL-LIS-TERR-SORV
               ,:P56-TP-SIT-FIN-PAT
                :IND-P56-TP-SIT-FIN-PAT
               ,:P56-TP-SIT-FIN-PAT-CON
                :IND-P56-TP-SIT-FIN-PAT-CON
               ,:P56-IMP-TOT-AFF-UTIL
                :IND-P56-IMP-TOT-AFF-UTIL
               ,:P56-IMP-TOT-FIN-UTIL
                :IND-P56-IMP-TOT-FIN-UTIL
               ,:P56-IMP-TOT-AFF-ACC
                :IND-P56-IMP-TOT-AFF-ACC
               ,:P56-IMP-TOT-FIN-ACC
                :IND-P56-IMP-TOT-FIN-ACC
               ,:P56-TP-FRM-GIUR-SAV
                :IND-P56-TP-FRM-GIUR-SAV
               ,:P56-REG-COLL-POLI
                :IND-P56-REG-COLL-POLI
               ,:P56-NUM-TEL
                :IND-P56-NUM-TEL
               ,:P56-NUM-DIP
                :IND-P56-NUM-DIP
               ,:P56-TP-SIT-FAM-CONV
                :IND-P56-TP-SIT-FAM-CONV
               ,:P56-COD-PROF-CON
                :IND-P56-COD-PROF-CON
               ,:P56-FL-ES-PROC-PEN
                :IND-P56-FL-ES-PROC-PEN
               ,:P56-TP-COND-CLIENTE
                :IND-P56-TP-COND-CLIENTE
               ,:P56-COD-SAE
                :IND-P56-COD-SAE
               ,:P56-TP-OPER-ESTERO
                :IND-P56-TP-OPER-ESTERO
               ,:P56-STAT-OPER-ESTERO
                :IND-P56-STAT-OPER-ESTERO
               ,:P56-COD-PRV-SVOL-ATT
                :IND-P56-COD-PRV-SVOL-ATT
               ,:P56-COD-STAT-SVOL-ATT
                :IND-P56-COD-STAT-SVOL-ATT
               ,:P56-TP-SOC
                :IND-P56-TP-SOC
               ,:P56-FL-IND-SOC-QUOT
                :IND-P56-FL-IND-SOC-QUOT
               ,:P56-TP-SIT-GIUR
                :IND-P56-TP-SIT-GIUR
               ,:P56-PC-QUO-DET-TIT-EFF
                :IND-P56-PC-QUO-DET-TIT-EFF
               ,:P56-TP-PRFL-RSH-PEP
                :IND-P56-TP-PRFL-RSH-PEP
               ,:P56-TP-PEP
                :IND-P56-TP-PEP
               ,:P56-FL-NOT-PREG
                :IND-P56-FL-NOT-PREG
               ,:P56-DT-INI-FNT-REDD-DB
                :IND-P56-DT-INI-FNT-REDD
               ,:P56-FNT-REDD-2
                :IND-P56-FNT-REDD-2
               ,:P56-DT-INI-FNT-REDD-2-DB
                :IND-P56-DT-INI-FNT-REDD-2
               ,:P56-FNT-REDD-3
                :IND-P56-FNT-REDD-3
               ,:P56-DT-INI-FNT-REDD-3-DB
                :IND-P56-DT-INI-FNT-REDD-3
               ,:P56-MOT-ASS-TIT-EFF-VCHAR
                :IND-P56-MOT-ASS-TIT-EFF
               ,:P56-FIN-COSTITUZIONE-VCHAR
                :IND-P56-FIN-COSTITUZIONE
               ,:P56-DESC-IMP-CAR-PUB-VCHAR
                :IND-P56-DESC-IMP-CAR-PUB
               ,:P56-DESC-SCO-FIN-RAPP-VCHAR
                :IND-P56-DESC-SCO-FIN-RAPP
               ,:P56-DESC-PROC-PNL-VCHAR
                :IND-P56-DESC-PROC-PNL
               ,:P56-DESC-NOT-PREG-VCHAR
                :IND-P56-DESC-NOT-PREG
               ,:P56-ID-ASSICURATI
                :IND-P56-ID-ASSICURATI
               ,:P56-REDD-CON
                :IND-P56-REDD-CON
               ,:P56-DESC-LIB-MOT-RISC-VCHAR
                :IND-P56-DESC-LIB-MOT-RISC
               ,:P56-TP-MOT-ASS-TIT-EFF
                :IND-P56-TP-MOT-ASS-TIT-EFF
               ,:P56-TP-RAG-RAPP
                :IND-P56-TP-RAG-RAPP
               ,:P56-COD-CAN
                :IND-P56-COD-CAN
               ,:P56-TP-FIN-COST
                :IND-P56-TP-FIN-COST
               ,:P56-NAZ-DEST-FND
                :IND-P56-NAZ-DEST-FND
               ,:P56-FL-AU-FATCA-AEOI
                :IND-P56-FL-AU-FATCA-AEOI
               ,:P56-TP-CAR-FIN-GIUR
                :IND-P56-TP-CAR-FIN-GIUR
               ,:P56-TP-CAR-FIN-GIUR-AT
                :IND-P56-TP-CAR-FIN-GIUR-AT
               ,:P56-TP-CAR-FIN-GIUR-PA
                :IND-P56-TP-CAR-FIN-GIUR-PA
               ,:P56-FL-ISTITUZ-FIN
                :IND-P56-FL-ISTITUZ-FIN
               ,:P56-TP-ORI-FND-TIT-EFF
                :IND-P56-TP-ORI-FND-TIT-EFF
               ,:P56-PC-ESP-AG-PA-MSC
                :IND-P56-PC-ESP-AG-PA-MSC
               ,:P56-FL-PR-TR-USA
                :IND-P56-FL-PR-TR-USA
               ,:P56-FL-PR-TR-NO-USA
                :IND-P56-FL-PR-TR-NO-USA
               ,:P56-PC-RIP-PAT-AS-VITA
                :IND-P56-PC-RIP-PAT-AS-VITA
               ,:P56-PC-RIP-PAT-IM
                :IND-P56-PC-RIP-PAT-IM
               ,:P56-PC-RIP-PAT-SET-IM
                :IND-P56-PC-RIP-PAT-SET-IM
               ,:P56-TP-STATUS-AEOI
                :IND-P56-TP-STATUS-AEOI
               ,:P56-TP-STATUS-FATCA
                :IND-P56-TP-STATUS-FATCA
               ,:P56-FL-RAPP-PA-MSC
                :IND-P56-FL-RAPP-PA-MSC
               ,:P56-COD-COMUN-SVOL-ATT
                :IND-P56-COD-COMUN-SVOL-ATT
               ,:P56-TP-DT-1O-CON-CLI
                :IND-P56-TP-DT-1O-CON-CLI
               ,:P56-TP-MOD-EN-RELA-INT
                :IND-P56-TP-MOD-EN-RELA-INT
               ,:P56-TP-REDD-ANNU-LRD
                :IND-P56-TP-REDD-ANNU-LRD
               ,:P56-TP-REDD-CON
                :IND-P56-TP-REDD-CON
               ,:P56-TP-OPER-SOC-FID
                :IND-P56-TP-OPER-SOC-FID
               ,:P56-COD-PA-ESP-MSC-1
                :IND-P56-COD-PA-ESP-MSC-1
               ,:P56-IMP-PA-ESP-MSC-1
                :IND-P56-IMP-PA-ESP-MSC-1
               ,:P56-COD-PA-ESP-MSC-2
                :IND-P56-COD-PA-ESP-MSC-2
               ,:P56-IMP-PA-ESP-MSC-2
                :IND-P56-IMP-PA-ESP-MSC-2
               ,:P56-COD-PA-ESP-MSC-3
                :IND-P56-COD-PA-ESP-MSC-3
               ,:P56-IMP-PA-ESP-MSC-3
                :IND-P56-IMP-PA-ESP-MSC-3
               ,:P56-COD-PA-ESP-MSC-4
                :IND-P56-COD-PA-ESP-MSC-4
               ,:P56-IMP-PA-ESP-MSC-4
                :IND-P56-IMP-PA-ESP-MSC-4
               ,:P56-COD-PA-ESP-MSC-5
                :IND-P56-COD-PA-ESP-MSC-5
               ,:P56-IMP-PA-ESP-MSC-5
                :IND-P56-IMP-PA-ESP-MSC-5
               ,:P56-DESC-ORGN-FND-VCHAR
                :IND-P56-DESC-ORGN-FND
               ,:P56-COD-AUT-DUE-DIL
                :IND-P56-COD-AUT-DUE-DIL
               ,:P56-FL-PR-QUEST-FATCA
                :IND-P56-FL-PR-QUEST-FATCA
               ,:P56-FL-PR-QUEST-AEOI
                :IND-P56-FL-PR-QUEST-AEOI
               ,:P56-FL-PR-QUEST-OFAC
                :IND-P56-FL-PR-QUEST-OFAC
               ,:P56-FL-PR-QUEST-KYC
                :IND-P56-FL-PR-QUEST-KYC
               ,:P56-FL-PR-QUEST-MSCQ
                :IND-P56-FL-PR-QUEST-MSCQ
               ,:P56-TP-NOT-PREG
                :IND-P56-TP-NOT-PREG
               ,:P56-TP-PROC-PNL
                :IND-P56-TP-PROC-PNL
               ,:P56-COD-IMP-CAR-PUB
                :IND-P56-COD-IMP-CAR-PUB
               ,:P56-OPRZ-SOSPETTE
                :IND-P56-OPRZ-SOSPETTE
               ,:P56-ULT-FATT-ANNU
                :IND-P56-ULT-FATT-ANNU
               ,:P56-DESC-PEP-VCHAR
                :IND-P56-DESC-PEP
               ,:P56-NUM-TEL-2
                :IND-P56-NUM-TEL-2
               ,:P56-IMP-AFI
                :IND-P56-IMP-AFI
               ,:P56-FL-NEW-PRO
                :IND-P56-FL-NEW-PRO
               ,:P56-TP-MOT-CAMBIO-CNTR
                :IND-P56-TP-MOT-CAMBIO-CNTR
               ,:P56-DESC-MOT-CAMBIO-CN-VCHAR
                :IND-P56-DESC-MOT-CAMBIO-CN
               ,:P56-COD-SOGG
                :IND-P56-COD-SOGG
             FROM QUEST_ADEG_VER
             WHERE     ID_QUEST_ADEG_VER = :P56-ID-QUEST-ADEG-VER

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO QUEST_ADEG_VER
                     (
                        ID_QUEST_ADEG_VER
                       ,COD_COMP_ANIA
                       ,ID_MOVI_CRZ
                       ,ID_RAPP_ANA
                       ,ID_POLI
                       ,NATURA_OPRZ
                       ,ORGN_FND
                       ,COD_QLFC_PROF
                       ,COD_NAZ_QLFC_PROF
                       ,COD_PRV_QLFC_PROF
                       ,FNT_REDD
                       ,REDD_FATT_ANNU
                       ,COD_ATECO
                       ,VALUT_COLL
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,LUOGO_COSTITUZIONE
                       ,TP_MOVI
                       ,FL_RAG_RAPP
                       ,FL_PRSZ_TIT_EFF
                       ,TP_MOT_RISC
                       ,TP_PNT_VND
                       ,TP_ADEG_VER
                       ,TP_RELA_ESEC
                       ,TP_SCO_FIN_RAPP
                       ,FL_PRSZ_3O_PAGAT
                       ,AREA_GEO_PROV_FND
                       ,TP_DEST_FND
                       ,FL_PAESE_RESID_AUT
                       ,FL_PAESE_CIT_AUT
                       ,FL_PAESE_NAZ_AUT
                       ,COD_PROF_PREC
                       ,FL_AUT_PEP
                       ,FL_IMP_CAR_PUB
                       ,FL_LIS_TERR_SORV
                       ,TP_SIT_FIN_PAT
                       ,TP_SIT_FIN_PAT_CON
                       ,IMP_TOT_AFF_UTIL
                       ,IMP_TOT_FIN_UTIL
                       ,IMP_TOT_AFF_ACC
                       ,IMP_TOT_FIN_ACC
                       ,TP_FRM_GIUR_SAV
                       ,REG_COLL_POLI
                       ,NUM_TEL
                       ,NUM_DIP
                       ,TP_SIT_FAM_CONV
                       ,COD_PROF_CON
                       ,FL_ES_PROC_PEN
                       ,TP_COND_CLIENTE
                       ,COD_SAE
                       ,TP_OPER_ESTERO
                       ,STAT_OPER_ESTERO
                       ,COD_PRV_SVOL_ATT
                       ,COD_STAT_SVOL_ATT
                       ,TP_SOC
                       ,FL_IND_SOC_QUOT
                       ,TP_SIT_GIUR
                       ,PC_QUO_DET_TIT_EFF
                       ,TP_PRFL_RSH_PEP
                       ,TP_PEP
                       ,FL_NOT_PREG
                       ,DT_INI_FNT_REDD
                       ,FNT_REDD_2
                       ,DT_INI_FNT_REDD_2
                       ,FNT_REDD_3
                       ,DT_INI_FNT_REDD_3
                       ,MOT_ASS_TIT_EFF
                       ,FIN_COSTITUZIONE
                       ,DESC_IMP_CAR_PUB
                       ,DESC_SCO_FIN_RAPP
                       ,DESC_PROC_PNL
                       ,DESC_NOT_PREG
                       ,ID_ASSICURATI
                       ,REDD_CON
                       ,DESC_LIB_MOT_RISC
                       ,TP_MOT_ASS_TIT_EFF
                       ,TP_RAG_RAPP
                       ,COD_CAN
                       ,TP_FIN_COST
                       ,NAZ_DEST_FND
                       ,FL_AU_FATCA_AEOI
                       ,TP_CAR_FIN_GIUR
                       ,TP_CAR_FIN_GIUR_AT
                       ,TP_CAR_FIN_GIUR_PA
                       ,FL_ISTITUZ_FIN
                       ,TP_ORI_FND_TIT_EFF
                       ,PC_ESP_AG_PA_MSC
                       ,FL_PR_TR_USA
                       ,FL_PR_TR_NO_USA
                       ,PC_RIP_PAT_AS_VITA
                       ,PC_RIP_PAT_IM
                       ,PC_RIP_PAT_SET_IM
                       ,TP_STATUS_AEOI
                       ,TP_STATUS_FATCA
                       ,FL_RAPP_PA_MSC
                       ,COD_COMUN_SVOL_ATT
                       ,TP_DT_1O_CON_CLI
                       ,TP_MOD_EN_RELA_INT
                       ,TP_REDD_ANNU_LRD
                       ,TP_REDD_CON
                       ,TP_OPER_SOC_FID
                       ,COD_PA_ESP_MSC_1
                       ,IMP_PA_ESP_MSC_1
                       ,COD_PA_ESP_MSC_2
                       ,IMP_PA_ESP_MSC_2
                       ,COD_PA_ESP_MSC_3
                       ,IMP_PA_ESP_MSC_3
                       ,COD_PA_ESP_MSC_4
                       ,IMP_PA_ESP_MSC_4
                       ,COD_PA_ESP_MSC_5
                       ,IMP_PA_ESP_MSC_5
                       ,DESC_ORGN_FND
                       ,COD_AUT_DUE_DIL
                       ,FL_PR_QUEST_FATCA
                       ,FL_PR_QUEST_AEOI
                       ,FL_PR_QUEST_OFAC
                       ,FL_PR_QUEST_KYC
                       ,FL_PR_QUEST_MSCQ
                       ,TP_NOT_PREG
                       ,TP_PROC_PNL
                       ,COD_IMP_CAR_PUB
                       ,OPRZ_SOSPETTE
                       ,ULT_FATT_ANNU
                       ,DESC_PEP
                       ,NUM_TEL_2
                       ,IMP_AFI
                       ,FL_NEW_PRO
                       ,TP_MOT_CAMBIO_CNTR
                       ,DESC_MOT_CAMBIO_CN
                       ,COD_SOGG
                     )
                 VALUES
                     (
                       :P56-ID-QUEST-ADEG-VER
                       ,:P56-COD-COMP-ANIA
                       ,:P56-ID-MOVI-CRZ
                       ,:P56-ID-RAPP-ANA
                        :IND-P56-ID-RAPP-ANA
                       ,:P56-ID-POLI
                       ,:P56-NATURA-OPRZ
                        :IND-P56-NATURA-OPRZ
                       ,:P56-ORGN-FND
                        :IND-P56-ORGN-FND
                       ,:P56-COD-QLFC-PROF
                        :IND-P56-COD-QLFC-PROF
                       ,:P56-COD-NAZ-QLFC-PROF
                        :IND-P56-COD-NAZ-QLFC-PROF
                       ,:P56-COD-PRV-QLFC-PROF
                        :IND-P56-COD-PRV-QLFC-PROF
                       ,:P56-FNT-REDD
                        :IND-P56-FNT-REDD
                       ,:P56-REDD-FATT-ANNU
                        :IND-P56-REDD-FATT-ANNU
                       ,:P56-COD-ATECO
                        :IND-P56-COD-ATECO
                       ,:P56-VALUT-COLL
                        :IND-P56-VALUT-COLL
                       ,:P56-DS-OPER-SQL
                       ,:P56-DS-VER
                       ,:P56-DS-TS-CPTZ
                       ,:P56-DS-UTENTE
                       ,:P56-DS-STATO-ELAB
                       ,:P56-LUOGO-COSTITUZIONE-VCHAR
                        :IND-P56-LUOGO-COSTITUZIONE
                       ,:P56-TP-MOVI
                        :IND-P56-TP-MOVI
                       ,:P56-FL-RAG-RAPP
                        :IND-P56-FL-RAG-RAPP
                       ,:P56-FL-PRSZ-TIT-EFF
                        :IND-P56-FL-PRSZ-TIT-EFF
                       ,:P56-TP-MOT-RISC
                        :IND-P56-TP-MOT-RISC
                       ,:P56-TP-PNT-VND
                        :IND-P56-TP-PNT-VND
                       ,:P56-TP-ADEG-VER
                        :IND-P56-TP-ADEG-VER
                       ,:P56-TP-RELA-ESEC
                        :IND-P56-TP-RELA-ESEC
                       ,:P56-TP-SCO-FIN-RAPP
                        :IND-P56-TP-SCO-FIN-RAPP
                       ,:P56-FL-PRSZ-3O-PAGAT
                        :IND-P56-FL-PRSZ-3O-PAGAT
                       ,:P56-AREA-GEO-PROV-FND
                        :IND-P56-AREA-GEO-PROV-FND
                       ,:P56-TP-DEST-FND
                        :IND-P56-TP-DEST-FND
                       ,:P56-FL-PAESE-RESID-AUT
                        :IND-P56-FL-PAESE-RESID-AUT
                       ,:P56-FL-PAESE-CIT-AUT
                        :IND-P56-FL-PAESE-CIT-AUT
                       ,:P56-FL-PAESE-NAZ-AUT
                        :IND-P56-FL-PAESE-NAZ-AUT
                       ,:P56-COD-PROF-PREC
                        :IND-P56-COD-PROF-PREC
                       ,:P56-FL-AUT-PEP
                        :IND-P56-FL-AUT-PEP
                       ,:P56-FL-IMP-CAR-PUB
                        :IND-P56-FL-IMP-CAR-PUB
                       ,:P56-FL-LIS-TERR-SORV
                        :IND-P56-FL-LIS-TERR-SORV
                       ,:P56-TP-SIT-FIN-PAT
                        :IND-P56-TP-SIT-FIN-PAT
                       ,:P56-TP-SIT-FIN-PAT-CON
                        :IND-P56-TP-SIT-FIN-PAT-CON
                       ,:P56-IMP-TOT-AFF-UTIL
                        :IND-P56-IMP-TOT-AFF-UTIL
                       ,:P56-IMP-TOT-FIN-UTIL
                        :IND-P56-IMP-TOT-FIN-UTIL
                       ,:P56-IMP-TOT-AFF-ACC
                        :IND-P56-IMP-TOT-AFF-ACC
                       ,:P56-IMP-TOT-FIN-ACC
                        :IND-P56-IMP-TOT-FIN-ACC
                       ,:P56-TP-FRM-GIUR-SAV
                        :IND-P56-TP-FRM-GIUR-SAV
                       ,:P56-REG-COLL-POLI
                        :IND-P56-REG-COLL-POLI
                       ,:P56-NUM-TEL
                        :IND-P56-NUM-TEL
                       ,:P56-NUM-DIP
                        :IND-P56-NUM-DIP
                       ,:P56-TP-SIT-FAM-CONV
                        :IND-P56-TP-SIT-FAM-CONV
                       ,:P56-COD-PROF-CON
                        :IND-P56-COD-PROF-CON
                       ,:P56-FL-ES-PROC-PEN
                        :IND-P56-FL-ES-PROC-PEN
                       ,:P56-TP-COND-CLIENTE
                        :IND-P56-TP-COND-CLIENTE
                       ,:P56-COD-SAE
                        :IND-P56-COD-SAE
                       ,:P56-TP-OPER-ESTERO
                        :IND-P56-TP-OPER-ESTERO
                       ,:P56-STAT-OPER-ESTERO
                        :IND-P56-STAT-OPER-ESTERO
                       ,:P56-COD-PRV-SVOL-ATT
                        :IND-P56-COD-PRV-SVOL-ATT
                       ,:P56-COD-STAT-SVOL-ATT
                        :IND-P56-COD-STAT-SVOL-ATT
                       ,:P56-TP-SOC
                        :IND-P56-TP-SOC
                       ,:P56-FL-IND-SOC-QUOT
                        :IND-P56-FL-IND-SOC-QUOT
                       ,:P56-TP-SIT-GIUR
                        :IND-P56-TP-SIT-GIUR
                       ,:P56-PC-QUO-DET-TIT-EFF
                        :IND-P56-PC-QUO-DET-TIT-EFF
                       ,:P56-TP-PRFL-RSH-PEP
                        :IND-P56-TP-PRFL-RSH-PEP
                       ,:P56-TP-PEP
                        :IND-P56-TP-PEP
                       ,:P56-FL-NOT-PREG
                        :IND-P56-FL-NOT-PREG
                       ,:P56-DT-INI-FNT-REDD-DB
                        :IND-P56-DT-INI-FNT-REDD
                       ,:P56-FNT-REDD-2
                        :IND-P56-FNT-REDD-2
                       ,:P56-DT-INI-FNT-REDD-2-DB
                        :IND-P56-DT-INI-FNT-REDD-2
                       ,:P56-FNT-REDD-3
                        :IND-P56-FNT-REDD-3
                       ,:P56-DT-INI-FNT-REDD-3-DB
                        :IND-P56-DT-INI-FNT-REDD-3
                       ,:P56-MOT-ASS-TIT-EFF-VCHAR
                        :IND-P56-MOT-ASS-TIT-EFF
                       ,:P56-FIN-COSTITUZIONE-VCHAR
                        :IND-P56-FIN-COSTITUZIONE
                       ,:P56-DESC-IMP-CAR-PUB-VCHAR
                        :IND-P56-DESC-IMP-CAR-PUB
                       ,:P56-DESC-SCO-FIN-RAPP-VCHAR
                        :IND-P56-DESC-SCO-FIN-RAPP
                       ,:P56-DESC-PROC-PNL-VCHAR
                        :IND-P56-DESC-PROC-PNL
                       ,:P56-DESC-NOT-PREG-VCHAR
                        :IND-P56-DESC-NOT-PREG
                       ,:P56-ID-ASSICURATI
                        :IND-P56-ID-ASSICURATI
                       ,:P56-REDD-CON
                        :IND-P56-REDD-CON
                       ,:P56-DESC-LIB-MOT-RISC-VCHAR
                        :IND-P56-DESC-LIB-MOT-RISC
                       ,:P56-TP-MOT-ASS-TIT-EFF
                        :IND-P56-TP-MOT-ASS-TIT-EFF
                       ,:P56-TP-RAG-RAPP
                        :IND-P56-TP-RAG-RAPP
                       ,:P56-COD-CAN
                        :IND-P56-COD-CAN
                       ,:P56-TP-FIN-COST
                        :IND-P56-TP-FIN-COST
                       ,:P56-NAZ-DEST-FND
                        :IND-P56-NAZ-DEST-FND
                       ,:P56-FL-AU-FATCA-AEOI
                        :IND-P56-FL-AU-FATCA-AEOI
                       ,:P56-TP-CAR-FIN-GIUR
                        :IND-P56-TP-CAR-FIN-GIUR
                       ,:P56-TP-CAR-FIN-GIUR-AT
                        :IND-P56-TP-CAR-FIN-GIUR-AT
                       ,:P56-TP-CAR-FIN-GIUR-PA
                        :IND-P56-TP-CAR-FIN-GIUR-PA
                       ,:P56-FL-ISTITUZ-FIN
                        :IND-P56-FL-ISTITUZ-FIN
                       ,:P56-TP-ORI-FND-TIT-EFF
                        :IND-P56-TP-ORI-FND-TIT-EFF
                       ,:P56-PC-ESP-AG-PA-MSC
                        :IND-P56-PC-ESP-AG-PA-MSC
                       ,:P56-FL-PR-TR-USA
                        :IND-P56-FL-PR-TR-USA
                       ,:P56-FL-PR-TR-NO-USA
                        :IND-P56-FL-PR-TR-NO-USA
                       ,:P56-PC-RIP-PAT-AS-VITA
                        :IND-P56-PC-RIP-PAT-AS-VITA
                       ,:P56-PC-RIP-PAT-IM
                        :IND-P56-PC-RIP-PAT-IM
                       ,:P56-PC-RIP-PAT-SET-IM
                        :IND-P56-PC-RIP-PAT-SET-IM
                       ,:P56-TP-STATUS-AEOI
                        :IND-P56-TP-STATUS-AEOI
                       ,:P56-TP-STATUS-FATCA
                        :IND-P56-TP-STATUS-FATCA
                       ,:P56-FL-RAPP-PA-MSC
                        :IND-P56-FL-RAPP-PA-MSC
                       ,:P56-COD-COMUN-SVOL-ATT
                        :IND-P56-COD-COMUN-SVOL-ATT
                       ,:P56-TP-DT-1O-CON-CLI
                        :IND-P56-TP-DT-1O-CON-CLI
                       ,:P56-TP-MOD-EN-RELA-INT
                        :IND-P56-TP-MOD-EN-RELA-INT
                       ,:P56-TP-REDD-ANNU-LRD
                        :IND-P56-TP-REDD-ANNU-LRD
                       ,:P56-TP-REDD-CON
                        :IND-P56-TP-REDD-CON
                       ,:P56-TP-OPER-SOC-FID
                        :IND-P56-TP-OPER-SOC-FID
                       ,:P56-COD-PA-ESP-MSC-1
                        :IND-P56-COD-PA-ESP-MSC-1
                       ,:P56-IMP-PA-ESP-MSC-1
                        :IND-P56-IMP-PA-ESP-MSC-1
                       ,:P56-COD-PA-ESP-MSC-2
                        :IND-P56-COD-PA-ESP-MSC-2
                       ,:P56-IMP-PA-ESP-MSC-2
                        :IND-P56-IMP-PA-ESP-MSC-2
                       ,:P56-COD-PA-ESP-MSC-3
                        :IND-P56-COD-PA-ESP-MSC-3
                       ,:P56-IMP-PA-ESP-MSC-3
                        :IND-P56-IMP-PA-ESP-MSC-3
                       ,:P56-COD-PA-ESP-MSC-4
                        :IND-P56-COD-PA-ESP-MSC-4
                       ,:P56-IMP-PA-ESP-MSC-4
                        :IND-P56-IMP-PA-ESP-MSC-4
                       ,:P56-COD-PA-ESP-MSC-5
                        :IND-P56-COD-PA-ESP-MSC-5
                       ,:P56-IMP-PA-ESP-MSC-5
                        :IND-P56-IMP-PA-ESP-MSC-5
                       ,:P56-DESC-ORGN-FND-VCHAR
                        :IND-P56-DESC-ORGN-FND
                       ,:P56-COD-AUT-DUE-DIL
                        :IND-P56-COD-AUT-DUE-DIL
                       ,:P56-FL-PR-QUEST-FATCA
                        :IND-P56-FL-PR-QUEST-FATCA
                       ,:P56-FL-PR-QUEST-AEOI
                        :IND-P56-FL-PR-QUEST-AEOI
                       ,:P56-FL-PR-QUEST-OFAC
                        :IND-P56-FL-PR-QUEST-OFAC
                       ,:P56-FL-PR-QUEST-KYC
                        :IND-P56-FL-PR-QUEST-KYC
                       ,:P56-FL-PR-QUEST-MSCQ
                        :IND-P56-FL-PR-QUEST-MSCQ
                       ,:P56-TP-NOT-PREG
                        :IND-P56-TP-NOT-PREG
                       ,:P56-TP-PROC-PNL
                        :IND-P56-TP-PROC-PNL
                       ,:P56-COD-IMP-CAR-PUB
                        :IND-P56-COD-IMP-CAR-PUB
                       ,:P56-OPRZ-SOSPETTE
                        :IND-P56-OPRZ-SOSPETTE
                       ,:P56-ULT-FATT-ANNU
                        :IND-P56-ULT-FATT-ANNU
                       ,:P56-DESC-PEP-VCHAR
                        :IND-P56-DESC-PEP
                       ,:P56-NUM-TEL-2
                        :IND-P56-NUM-TEL-2
                       ,:P56-IMP-AFI
                        :IND-P56-IMP-AFI
                       ,:P56-FL-NEW-PRO
                        :IND-P56-FL-NEW-PRO
                       ,:P56-TP-MOT-CAMBIO-CNTR
                        :IND-P56-TP-MOT-CAMBIO-CNTR
                       ,:P56-DESC-MOT-CAMBIO-CN-VCHAR
                        :IND-P56-DESC-MOT-CAMBIO-CN
                       ,:P56-COD-SOGG
                        :IND-P56-COD-SOGG
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE QUEST_ADEG_VER SET

                   ID_QUEST_ADEG_VER      =
                :P56-ID-QUEST-ADEG-VER
                  ,COD_COMP_ANIA          =
                :P56-COD-COMP-ANIA
                  ,ID_MOVI_CRZ            =
                :P56-ID-MOVI-CRZ
                  ,ID_RAPP_ANA            =
                :P56-ID-RAPP-ANA
                                       :IND-P56-ID-RAPP-ANA
                  ,ID_POLI                =
                :P56-ID-POLI
                  ,NATURA_OPRZ            =
                :P56-NATURA-OPRZ
                                       :IND-P56-NATURA-OPRZ
                  ,ORGN_FND               =
                :P56-ORGN-FND
                                       :IND-P56-ORGN-FND
                  ,COD_QLFC_PROF          =
                :P56-COD-QLFC-PROF
                                       :IND-P56-COD-QLFC-PROF
                  ,COD_NAZ_QLFC_PROF      =
                :P56-COD-NAZ-QLFC-PROF
                                       :IND-P56-COD-NAZ-QLFC-PROF
                  ,COD_PRV_QLFC_PROF      =
                :P56-COD-PRV-QLFC-PROF
                                       :IND-P56-COD-PRV-QLFC-PROF
                  ,FNT_REDD               =
                :P56-FNT-REDD
                                       :IND-P56-FNT-REDD
                  ,REDD_FATT_ANNU         =
                :P56-REDD-FATT-ANNU
                                       :IND-P56-REDD-FATT-ANNU
                  ,COD_ATECO              =
                :P56-COD-ATECO
                                       :IND-P56-COD-ATECO
                  ,VALUT_COLL             =
                :P56-VALUT-COLL
                                       :IND-P56-VALUT-COLL
                  ,DS_OPER_SQL            =
                :P56-DS-OPER-SQL
                  ,DS_VER                 =
                :P56-DS-VER
                  ,DS_TS_CPTZ             =
                :P56-DS-TS-CPTZ
                  ,DS_UTENTE              =
                :P56-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :P56-DS-STATO-ELAB
                  ,LUOGO_COSTITUZIONE     =
                :P56-LUOGO-COSTITUZIONE-VCHAR
                                       :IND-P56-LUOGO-COSTITUZIONE
                  ,TP_MOVI                =
                :P56-TP-MOVI
                                       :IND-P56-TP-MOVI
                  ,FL_RAG_RAPP            =
                :P56-FL-RAG-RAPP
                                       :IND-P56-FL-RAG-RAPP
                  ,FL_PRSZ_TIT_EFF        =
                :P56-FL-PRSZ-TIT-EFF
                                       :IND-P56-FL-PRSZ-TIT-EFF
                  ,TP_MOT_RISC            =
                :P56-TP-MOT-RISC
                                       :IND-P56-TP-MOT-RISC
                  ,TP_PNT_VND             =
                :P56-TP-PNT-VND
                                       :IND-P56-TP-PNT-VND
                  ,TP_ADEG_VER            =
                :P56-TP-ADEG-VER
                                       :IND-P56-TP-ADEG-VER
                  ,TP_RELA_ESEC           =
                :P56-TP-RELA-ESEC
                                       :IND-P56-TP-RELA-ESEC
                  ,TP_SCO_FIN_RAPP        =
                :P56-TP-SCO-FIN-RAPP
                                       :IND-P56-TP-SCO-FIN-RAPP
                  ,FL_PRSZ_3O_PAGAT       =
                :P56-FL-PRSZ-3O-PAGAT
                                       :IND-P56-FL-PRSZ-3O-PAGAT
                  ,AREA_GEO_PROV_FND      =
                :P56-AREA-GEO-PROV-FND
                                       :IND-P56-AREA-GEO-PROV-FND
                  ,TP_DEST_FND            =
                :P56-TP-DEST-FND
                                       :IND-P56-TP-DEST-FND
                  ,FL_PAESE_RESID_AUT     =
                :P56-FL-PAESE-RESID-AUT
                                       :IND-P56-FL-PAESE-RESID-AUT
                  ,FL_PAESE_CIT_AUT       =
                :P56-FL-PAESE-CIT-AUT
                                       :IND-P56-FL-PAESE-CIT-AUT
                  ,FL_PAESE_NAZ_AUT       =
                :P56-FL-PAESE-NAZ-AUT
                                       :IND-P56-FL-PAESE-NAZ-AUT
                  ,COD_PROF_PREC          =
                :P56-COD-PROF-PREC
                                       :IND-P56-COD-PROF-PREC
                  ,FL_AUT_PEP             =
                :P56-FL-AUT-PEP
                                       :IND-P56-FL-AUT-PEP
                  ,FL_IMP_CAR_PUB         =
                :P56-FL-IMP-CAR-PUB
                                       :IND-P56-FL-IMP-CAR-PUB
                  ,FL_LIS_TERR_SORV       =
                :P56-FL-LIS-TERR-SORV
                                       :IND-P56-FL-LIS-TERR-SORV
                  ,TP_SIT_FIN_PAT         =
                :P56-TP-SIT-FIN-PAT
                                       :IND-P56-TP-SIT-FIN-PAT
                  ,TP_SIT_FIN_PAT_CON     =
                :P56-TP-SIT-FIN-PAT-CON
                                       :IND-P56-TP-SIT-FIN-PAT-CON
                  ,IMP_TOT_AFF_UTIL       =
                :P56-IMP-TOT-AFF-UTIL
                                       :IND-P56-IMP-TOT-AFF-UTIL
                  ,IMP_TOT_FIN_UTIL       =
                :P56-IMP-TOT-FIN-UTIL
                                       :IND-P56-IMP-TOT-FIN-UTIL
                  ,IMP_TOT_AFF_ACC        =
                :P56-IMP-TOT-AFF-ACC
                                       :IND-P56-IMP-TOT-AFF-ACC
                  ,IMP_TOT_FIN_ACC        =
                :P56-IMP-TOT-FIN-ACC
                                       :IND-P56-IMP-TOT-FIN-ACC
                  ,TP_FRM_GIUR_SAV        =
                :P56-TP-FRM-GIUR-SAV
                                       :IND-P56-TP-FRM-GIUR-SAV
                  ,REG_COLL_POLI          =
                :P56-REG-COLL-POLI
                                       :IND-P56-REG-COLL-POLI
                  ,NUM_TEL                =
                :P56-NUM-TEL
                                       :IND-P56-NUM-TEL
                  ,NUM_DIP                =
                :P56-NUM-DIP
                                       :IND-P56-NUM-DIP
                  ,TP_SIT_FAM_CONV        =
                :P56-TP-SIT-FAM-CONV
                                       :IND-P56-TP-SIT-FAM-CONV
                  ,COD_PROF_CON           =
                :P56-COD-PROF-CON
                                       :IND-P56-COD-PROF-CON
                  ,FL_ES_PROC_PEN         =
                :P56-FL-ES-PROC-PEN
                                       :IND-P56-FL-ES-PROC-PEN
                  ,TP_COND_CLIENTE        =
                :P56-TP-COND-CLIENTE
                                       :IND-P56-TP-COND-CLIENTE
                  ,COD_SAE                =
                :P56-COD-SAE
                                       :IND-P56-COD-SAE
                  ,TP_OPER_ESTERO         =
                :P56-TP-OPER-ESTERO
                                       :IND-P56-TP-OPER-ESTERO
                  ,STAT_OPER_ESTERO       =
                :P56-STAT-OPER-ESTERO
                                       :IND-P56-STAT-OPER-ESTERO
                  ,COD_PRV_SVOL_ATT       =
                :P56-COD-PRV-SVOL-ATT
                                       :IND-P56-COD-PRV-SVOL-ATT
                  ,COD_STAT_SVOL_ATT      =
                :P56-COD-STAT-SVOL-ATT
                                       :IND-P56-COD-STAT-SVOL-ATT
                  ,TP_SOC                 =
                :P56-TP-SOC
                                       :IND-P56-TP-SOC
                  ,FL_IND_SOC_QUOT        =
                :P56-FL-IND-SOC-QUOT
                                       :IND-P56-FL-IND-SOC-QUOT
                  ,TP_SIT_GIUR            =
                :P56-TP-SIT-GIUR
                                       :IND-P56-TP-SIT-GIUR
                  ,PC_QUO_DET_TIT_EFF     =
                :P56-PC-QUO-DET-TIT-EFF
                                       :IND-P56-PC-QUO-DET-TIT-EFF
                  ,TP_PRFL_RSH_PEP        =
                :P56-TP-PRFL-RSH-PEP
                                       :IND-P56-TP-PRFL-RSH-PEP
                  ,TP_PEP                 =
                :P56-TP-PEP
                                       :IND-P56-TP-PEP
                  ,FL_NOT_PREG            =
                :P56-FL-NOT-PREG
                                       :IND-P56-FL-NOT-PREG
                  ,DT_INI_FNT_REDD        =
           :P56-DT-INI-FNT-REDD-DB
                                       :IND-P56-DT-INI-FNT-REDD
                  ,FNT_REDD_2             =
                :P56-FNT-REDD-2
                                       :IND-P56-FNT-REDD-2
                  ,DT_INI_FNT_REDD_2      =
           :P56-DT-INI-FNT-REDD-2-DB
                                       :IND-P56-DT-INI-FNT-REDD-2
                  ,FNT_REDD_3             =
                :P56-FNT-REDD-3
                                       :IND-P56-FNT-REDD-3
                  ,DT_INI_FNT_REDD_3      =
           :P56-DT-INI-FNT-REDD-3-DB
                                       :IND-P56-DT-INI-FNT-REDD-3
                  ,MOT_ASS_TIT_EFF        =
                :P56-MOT-ASS-TIT-EFF-VCHAR
                                       :IND-P56-MOT-ASS-TIT-EFF
                  ,FIN_COSTITUZIONE       =
                :P56-FIN-COSTITUZIONE-VCHAR
                                       :IND-P56-FIN-COSTITUZIONE
                  ,DESC_IMP_CAR_PUB       =
                :P56-DESC-IMP-CAR-PUB-VCHAR
                                       :IND-P56-DESC-IMP-CAR-PUB
                  ,DESC_SCO_FIN_RAPP      =
                :P56-DESC-SCO-FIN-RAPP-VCHAR
                                       :IND-P56-DESC-SCO-FIN-RAPP
                  ,DESC_PROC_PNL          =
                :P56-DESC-PROC-PNL-VCHAR
                                       :IND-P56-DESC-PROC-PNL
                  ,DESC_NOT_PREG          =
                :P56-DESC-NOT-PREG-VCHAR
                                       :IND-P56-DESC-NOT-PREG
                  ,ID_ASSICURATI          =
                :P56-ID-ASSICURATI
                                       :IND-P56-ID-ASSICURATI
                  ,REDD_CON               =
                :P56-REDD-CON
                                       :IND-P56-REDD-CON
                  ,DESC_LIB_MOT_RISC      =
                :P56-DESC-LIB-MOT-RISC-VCHAR
                                       :IND-P56-DESC-LIB-MOT-RISC
                  ,TP_MOT_ASS_TIT_EFF     =
                :P56-TP-MOT-ASS-TIT-EFF
                                       :IND-P56-TP-MOT-ASS-TIT-EFF
                  ,TP_RAG_RAPP            =
                :P56-TP-RAG-RAPP
                                       :IND-P56-TP-RAG-RAPP
                  ,COD_CAN                =
                :P56-COD-CAN
                                       :IND-P56-COD-CAN
                  ,TP_FIN_COST            =
                :P56-TP-FIN-COST
                                       :IND-P56-TP-FIN-COST
                  ,NAZ_DEST_FND           =
                :P56-NAZ-DEST-FND
                                       :IND-P56-NAZ-DEST-FND
                  ,FL_AU_FATCA_AEOI       =
                :P56-FL-AU-FATCA-AEOI
                                       :IND-P56-FL-AU-FATCA-AEOI
                  ,TP_CAR_FIN_GIUR        =
                :P56-TP-CAR-FIN-GIUR
                                       :IND-P56-TP-CAR-FIN-GIUR
                  ,TP_CAR_FIN_GIUR_AT     =
                :P56-TP-CAR-FIN-GIUR-AT
                                       :IND-P56-TP-CAR-FIN-GIUR-AT
                  ,TP_CAR_FIN_GIUR_PA     =
                :P56-TP-CAR-FIN-GIUR-PA
                                       :IND-P56-TP-CAR-FIN-GIUR-PA
                  ,FL_ISTITUZ_FIN         =
                :P56-FL-ISTITUZ-FIN
                                       :IND-P56-FL-ISTITUZ-FIN
                  ,TP_ORI_FND_TIT_EFF     =
                :P56-TP-ORI-FND-TIT-EFF
                                       :IND-P56-TP-ORI-FND-TIT-EFF
                  ,PC_ESP_AG_PA_MSC       =
                :P56-PC-ESP-AG-PA-MSC
                                       :IND-P56-PC-ESP-AG-PA-MSC
                  ,FL_PR_TR_USA           =
                :P56-FL-PR-TR-USA
                                       :IND-P56-FL-PR-TR-USA
                  ,FL_PR_TR_NO_USA        =
                :P56-FL-PR-TR-NO-USA
                                       :IND-P56-FL-PR-TR-NO-USA
                  ,PC_RIP_PAT_AS_VITA     =
                :P56-PC-RIP-PAT-AS-VITA
                                       :IND-P56-PC-RIP-PAT-AS-VITA
                  ,PC_RIP_PAT_IM          =
                :P56-PC-RIP-PAT-IM
                                       :IND-P56-PC-RIP-PAT-IM
                  ,PC_RIP_PAT_SET_IM      =
                :P56-PC-RIP-PAT-SET-IM
                                       :IND-P56-PC-RIP-PAT-SET-IM
                  ,TP_STATUS_AEOI         =
                :P56-TP-STATUS-AEOI
                                       :IND-P56-TP-STATUS-AEOI
                  ,TP_STATUS_FATCA        =
                :P56-TP-STATUS-FATCA
                                       :IND-P56-TP-STATUS-FATCA
                  ,FL_RAPP_PA_MSC         =
                :P56-FL-RAPP-PA-MSC
                                       :IND-P56-FL-RAPP-PA-MSC
                  ,COD_COMUN_SVOL_ATT     =
                :P56-COD-COMUN-SVOL-ATT
                                       :IND-P56-COD-COMUN-SVOL-ATT
                  ,TP_DT_1O_CON_CLI       =
                :P56-TP-DT-1O-CON-CLI
                                       :IND-P56-TP-DT-1O-CON-CLI
                  ,TP_MOD_EN_RELA_INT     =
                :P56-TP-MOD-EN-RELA-INT
                                       :IND-P56-TP-MOD-EN-RELA-INT
                  ,TP_REDD_ANNU_LRD       =
                :P56-TP-REDD-ANNU-LRD
                                       :IND-P56-TP-REDD-ANNU-LRD
                  ,TP_REDD_CON            =
                :P56-TP-REDD-CON
                                       :IND-P56-TP-REDD-CON
                  ,TP_OPER_SOC_FID        =
                :P56-TP-OPER-SOC-FID
                                       :IND-P56-TP-OPER-SOC-FID
                  ,COD_PA_ESP_MSC_1       =
                :P56-COD-PA-ESP-MSC-1
                                       :IND-P56-COD-PA-ESP-MSC-1
                  ,IMP_PA_ESP_MSC_1       =
                :P56-IMP-PA-ESP-MSC-1
                                       :IND-P56-IMP-PA-ESP-MSC-1
                  ,COD_PA_ESP_MSC_2       =
                :P56-COD-PA-ESP-MSC-2
                                       :IND-P56-COD-PA-ESP-MSC-2
                  ,IMP_PA_ESP_MSC_2       =
                :P56-IMP-PA-ESP-MSC-2
                                       :IND-P56-IMP-PA-ESP-MSC-2
                  ,COD_PA_ESP_MSC_3       =
                :P56-COD-PA-ESP-MSC-3
                                       :IND-P56-COD-PA-ESP-MSC-3
                  ,IMP_PA_ESP_MSC_3       =
                :P56-IMP-PA-ESP-MSC-3
                                       :IND-P56-IMP-PA-ESP-MSC-3
                  ,COD_PA_ESP_MSC_4       =
                :P56-COD-PA-ESP-MSC-4
                                       :IND-P56-COD-PA-ESP-MSC-4
                  ,IMP_PA_ESP_MSC_4       =
                :P56-IMP-PA-ESP-MSC-4
                                       :IND-P56-IMP-PA-ESP-MSC-4
                  ,COD_PA_ESP_MSC_5       =
                :P56-COD-PA-ESP-MSC-5
                                       :IND-P56-COD-PA-ESP-MSC-5
                  ,IMP_PA_ESP_MSC_5       =
                :P56-IMP-PA-ESP-MSC-5
                                       :IND-P56-IMP-PA-ESP-MSC-5
                  ,DESC_ORGN_FND          =
                :P56-DESC-ORGN-FND-VCHAR
                                       :IND-P56-DESC-ORGN-FND
                  ,COD_AUT_DUE_DIL        =
                :P56-COD-AUT-DUE-DIL
                                       :IND-P56-COD-AUT-DUE-DIL
                  ,FL_PR_QUEST_FATCA      =
                :P56-FL-PR-QUEST-FATCA
                                       :IND-P56-FL-PR-QUEST-FATCA
                  ,FL_PR_QUEST_AEOI       =
                :P56-FL-PR-QUEST-AEOI
                                       :IND-P56-FL-PR-QUEST-AEOI
                  ,FL_PR_QUEST_OFAC       =
                :P56-FL-PR-QUEST-OFAC
                                       :IND-P56-FL-PR-QUEST-OFAC
                  ,FL_PR_QUEST_KYC        =
                :P56-FL-PR-QUEST-KYC
                                       :IND-P56-FL-PR-QUEST-KYC
                  ,FL_PR_QUEST_MSCQ       =
                :P56-FL-PR-QUEST-MSCQ
                                       :IND-P56-FL-PR-QUEST-MSCQ
                  ,TP_NOT_PREG            =
                :P56-TP-NOT-PREG
                                       :IND-P56-TP-NOT-PREG
                  ,TP_PROC_PNL            =
                :P56-TP-PROC-PNL
                                       :IND-P56-TP-PROC-PNL
                  ,COD_IMP_CAR_PUB        =
                :P56-COD-IMP-CAR-PUB
                                       :IND-P56-COD-IMP-CAR-PUB
                  ,OPRZ_SOSPETTE          =
                :P56-OPRZ-SOSPETTE
                                       :IND-P56-OPRZ-SOSPETTE
                  ,ULT_FATT_ANNU          =
                :P56-ULT-FATT-ANNU
                                       :IND-P56-ULT-FATT-ANNU
                  ,DESC_PEP               =
                :P56-DESC-PEP-VCHAR
                                       :IND-P56-DESC-PEP
                  ,NUM_TEL_2              =
                :P56-NUM-TEL-2
                                       :IND-P56-NUM-TEL-2
                  ,IMP_AFI                =
                :P56-IMP-AFI
                                       :IND-P56-IMP-AFI
                  ,FL_NEW_PRO             =
                :P56-FL-NEW-PRO
                                       :IND-P56-FL-NEW-PRO
                  ,TP_MOT_CAMBIO_CNTR     =
                :P56-TP-MOT-CAMBIO-CNTR
                                       :IND-P56-TP-MOT-CAMBIO-CNTR
                  ,DESC_MOT_CAMBIO_CN     =
                :P56-DESC-MOT-CAMBIO-CN-VCHAR
                                       :IND-P56-DESC-MOT-CAMBIO-CN
                  ,COD_SOGG               =
                :P56-COD-SOGG
                                       :IND-P56-COD-SOGG
                WHERE     ID_QUEST_ADEG_VER = :P56-ID-QUEST-ADEG-VER

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM QUEST_ADEG_VER
                WHERE     ID_QUEST_ADEG_VER = :P56-ID-QUEST-ADEG-VER

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-P56-ID-RAPP-ANA = -1
              MOVE HIGH-VALUES TO P56-ID-RAPP-ANA-NULL
           END-IF
           IF IND-P56-NATURA-OPRZ = -1
              MOVE HIGH-VALUES TO P56-NATURA-OPRZ-NULL
           END-IF
           IF IND-P56-ORGN-FND = -1
              MOVE HIGH-VALUES TO P56-ORGN-FND-NULL
           END-IF
           IF IND-P56-COD-QLFC-PROF = -1
              MOVE HIGH-VALUES TO P56-COD-QLFC-PROF-NULL
           END-IF
           IF IND-P56-COD-NAZ-QLFC-PROF = -1
              MOVE HIGH-VALUES TO P56-COD-NAZ-QLFC-PROF-NULL
           END-IF
           IF IND-P56-COD-PRV-QLFC-PROF = -1
              MOVE HIGH-VALUES TO P56-COD-PRV-QLFC-PROF-NULL
           END-IF
           IF IND-P56-FNT-REDD = -1
              MOVE HIGH-VALUES TO P56-FNT-REDD-NULL
           END-IF
           IF IND-P56-REDD-FATT-ANNU = -1
              MOVE HIGH-VALUES TO P56-REDD-FATT-ANNU-NULL
           END-IF
           IF IND-P56-COD-ATECO = -1
              MOVE HIGH-VALUES TO P56-COD-ATECO-NULL
           END-IF
           IF IND-P56-VALUT-COLL = -1
              MOVE HIGH-VALUES TO P56-VALUT-COLL-NULL
           END-IF
           IF IND-P56-LUOGO-COSTITUZIONE = -1
              MOVE HIGH-VALUES TO P56-LUOGO-COSTITUZIONE
           END-IF
           IF IND-P56-TP-MOVI = -1
              MOVE HIGH-VALUES TO P56-TP-MOVI-NULL
           END-IF
           IF IND-P56-FL-RAG-RAPP = -1
              MOVE HIGH-VALUES TO P56-FL-RAG-RAPP-NULL
           END-IF
           IF IND-P56-FL-PRSZ-TIT-EFF = -1
              MOVE HIGH-VALUES TO P56-FL-PRSZ-TIT-EFF-NULL
           END-IF
           IF IND-P56-TP-MOT-RISC = -1
              MOVE HIGH-VALUES TO P56-TP-MOT-RISC-NULL
           END-IF
           IF IND-P56-TP-PNT-VND = -1
              MOVE HIGH-VALUES TO P56-TP-PNT-VND-NULL
           END-IF
           IF IND-P56-TP-ADEG-VER = -1
              MOVE HIGH-VALUES TO P56-TP-ADEG-VER-NULL
           END-IF
           IF IND-P56-TP-RELA-ESEC = -1
              MOVE HIGH-VALUES TO P56-TP-RELA-ESEC-NULL
           END-IF
           IF IND-P56-TP-SCO-FIN-RAPP = -1
              MOVE HIGH-VALUES TO P56-TP-SCO-FIN-RAPP-NULL
           END-IF
           IF IND-P56-FL-PRSZ-3O-PAGAT = -1
              MOVE HIGH-VALUES TO P56-FL-PRSZ-3O-PAGAT-NULL
           END-IF
           IF IND-P56-AREA-GEO-PROV-FND = -1
              MOVE HIGH-VALUES TO P56-AREA-GEO-PROV-FND-NULL
           END-IF
           IF IND-P56-TP-DEST-FND = -1
              MOVE HIGH-VALUES TO P56-TP-DEST-FND-NULL
           END-IF
           IF IND-P56-FL-PAESE-RESID-AUT = -1
              MOVE HIGH-VALUES TO P56-FL-PAESE-RESID-AUT-NULL
           END-IF
           IF IND-P56-FL-PAESE-CIT-AUT = -1
              MOVE HIGH-VALUES TO P56-FL-PAESE-CIT-AUT-NULL
           END-IF
           IF IND-P56-FL-PAESE-NAZ-AUT = -1
              MOVE HIGH-VALUES TO P56-FL-PAESE-NAZ-AUT-NULL
           END-IF
           IF IND-P56-COD-PROF-PREC = -1
              MOVE HIGH-VALUES TO P56-COD-PROF-PREC-NULL
           END-IF
           IF IND-P56-FL-AUT-PEP = -1
              MOVE HIGH-VALUES TO P56-FL-AUT-PEP-NULL
           END-IF
           IF IND-P56-FL-IMP-CAR-PUB = -1
              MOVE HIGH-VALUES TO P56-FL-IMP-CAR-PUB-NULL
           END-IF
           IF IND-P56-FL-LIS-TERR-SORV = -1
              MOVE HIGH-VALUES TO P56-FL-LIS-TERR-SORV-NULL
           END-IF
           IF IND-P56-TP-SIT-FIN-PAT = -1
              MOVE HIGH-VALUES TO P56-TP-SIT-FIN-PAT-NULL
           END-IF
           IF IND-P56-TP-SIT-FIN-PAT-CON = -1
              MOVE HIGH-VALUES TO P56-TP-SIT-FIN-PAT-CON-NULL
           END-IF
           IF IND-P56-IMP-TOT-AFF-UTIL = -1
              MOVE HIGH-VALUES TO P56-IMP-TOT-AFF-UTIL-NULL
           END-IF
           IF IND-P56-IMP-TOT-FIN-UTIL = -1
              MOVE HIGH-VALUES TO P56-IMP-TOT-FIN-UTIL-NULL
           END-IF
           IF IND-P56-IMP-TOT-AFF-ACC = -1
              MOVE HIGH-VALUES TO P56-IMP-TOT-AFF-ACC-NULL
           END-IF
           IF IND-P56-IMP-TOT-FIN-ACC = -1
              MOVE HIGH-VALUES TO P56-IMP-TOT-FIN-ACC-NULL
           END-IF
           IF IND-P56-TP-FRM-GIUR-SAV = -1
              MOVE HIGH-VALUES TO P56-TP-FRM-GIUR-SAV-NULL
           END-IF
           IF IND-P56-REG-COLL-POLI = -1
              MOVE HIGH-VALUES TO P56-REG-COLL-POLI-NULL
           END-IF
           IF IND-P56-NUM-TEL = -1
              MOVE HIGH-VALUES TO P56-NUM-TEL-NULL
           END-IF
           IF IND-P56-NUM-DIP = -1
              MOVE HIGH-VALUES TO P56-NUM-DIP-NULL
           END-IF
           IF IND-P56-TP-SIT-FAM-CONV = -1
              MOVE HIGH-VALUES TO P56-TP-SIT-FAM-CONV-NULL
           END-IF
           IF IND-P56-COD-PROF-CON = -1
              MOVE HIGH-VALUES TO P56-COD-PROF-CON-NULL
           END-IF
           IF IND-P56-FL-ES-PROC-PEN = -1
              MOVE HIGH-VALUES TO P56-FL-ES-PROC-PEN-NULL
           END-IF
           IF IND-P56-TP-COND-CLIENTE = -1
              MOVE HIGH-VALUES TO P56-TP-COND-CLIENTE-NULL
           END-IF
           IF IND-P56-COD-SAE = -1
              MOVE HIGH-VALUES TO P56-COD-SAE-NULL
           END-IF
           IF IND-P56-TP-OPER-ESTERO = -1
              MOVE HIGH-VALUES TO P56-TP-OPER-ESTERO-NULL
           END-IF
           IF IND-P56-STAT-OPER-ESTERO = -1
              MOVE HIGH-VALUES TO P56-STAT-OPER-ESTERO-NULL
           END-IF
           IF IND-P56-COD-PRV-SVOL-ATT = -1
              MOVE HIGH-VALUES TO P56-COD-PRV-SVOL-ATT-NULL
           END-IF
           IF IND-P56-COD-STAT-SVOL-ATT = -1
              MOVE HIGH-VALUES TO P56-COD-STAT-SVOL-ATT-NULL
           END-IF
           IF IND-P56-TP-SOC = -1
              MOVE HIGH-VALUES TO P56-TP-SOC-NULL
           END-IF
           IF IND-P56-FL-IND-SOC-QUOT = -1
              MOVE HIGH-VALUES TO P56-FL-IND-SOC-QUOT-NULL
           END-IF
           IF IND-P56-TP-SIT-GIUR = -1
              MOVE HIGH-VALUES TO P56-TP-SIT-GIUR-NULL
           END-IF
           IF IND-P56-PC-QUO-DET-TIT-EFF = -1
              MOVE HIGH-VALUES TO P56-PC-QUO-DET-TIT-EFF-NULL
           END-IF
           IF IND-P56-TP-PRFL-RSH-PEP = -1
              MOVE HIGH-VALUES TO P56-TP-PRFL-RSH-PEP-NULL
           END-IF
           IF IND-P56-TP-PEP = -1
              MOVE HIGH-VALUES TO P56-TP-PEP-NULL
           END-IF
           IF IND-P56-FL-NOT-PREG = -1
              MOVE HIGH-VALUES TO P56-FL-NOT-PREG-NULL
           END-IF
           IF IND-P56-DT-INI-FNT-REDD = -1
              MOVE HIGH-VALUES TO P56-DT-INI-FNT-REDD-NULL
           END-IF
           IF IND-P56-FNT-REDD-2 = -1
              MOVE HIGH-VALUES TO P56-FNT-REDD-2-NULL
           END-IF
           IF IND-P56-DT-INI-FNT-REDD-2 = -1
              MOVE HIGH-VALUES TO P56-DT-INI-FNT-REDD-2-NULL
           END-IF
           IF IND-P56-FNT-REDD-3 = -1
              MOVE HIGH-VALUES TO P56-FNT-REDD-3-NULL
           END-IF
           IF IND-P56-DT-INI-FNT-REDD-3 = -1
              MOVE HIGH-VALUES TO P56-DT-INI-FNT-REDD-3-NULL
           END-IF
           IF IND-P56-MOT-ASS-TIT-EFF = -1
              MOVE HIGH-VALUES TO P56-MOT-ASS-TIT-EFF
           END-IF
           IF IND-P56-FIN-COSTITUZIONE = -1
              MOVE HIGH-VALUES TO P56-FIN-COSTITUZIONE
           END-IF
           IF IND-P56-DESC-IMP-CAR-PUB = -1
              MOVE HIGH-VALUES TO P56-DESC-IMP-CAR-PUB
           END-IF
           IF IND-P56-DESC-SCO-FIN-RAPP = -1
              MOVE HIGH-VALUES TO P56-DESC-SCO-FIN-RAPP
           END-IF
           IF IND-P56-DESC-PROC-PNL = -1
              MOVE HIGH-VALUES TO P56-DESC-PROC-PNL
           END-IF
           IF IND-P56-DESC-NOT-PREG = -1
              MOVE HIGH-VALUES TO P56-DESC-NOT-PREG
           END-IF
           IF IND-P56-ID-ASSICURATI = -1
              MOVE HIGH-VALUES TO P56-ID-ASSICURATI-NULL
           END-IF
           IF IND-P56-REDD-CON = -1
              MOVE HIGH-VALUES TO P56-REDD-CON-NULL
           END-IF
           IF IND-P56-DESC-LIB-MOT-RISC = -1
              MOVE HIGH-VALUES TO P56-DESC-LIB-MOT-RISC
           END-IF
           IF IND-P56-TP-MOT-ASS-TIT-EFF = -1
              MOVE HIGH-VALUES TO P56-TP-MOT-ASS-TIT-EFF-NULL
           END-IF
           IF IND-P56-TP-RAG-RAPP = -1
              MOVE HIGH-VALUES TO P56-TP-RAG-RAPP-NULL
           END-IF
           IF IND-P56-COD-CAN = -1
              MOVE HIGH-VALUES TO P56-COD-CAN-NULL
           END-IF
           IF IND-P56-TP-FIN-COST = -1
              MOVE HIGH-VALUES TO P56-TP-FIN-COST-NULL
           END-IF
           IF IND-P56-NAZ-DEST-FND = -1
              MOVE HIGH-VALUES TO P56-NAZ-DEST-FND-NULL
           END-IF
           IF IND-P56-FL-AU-FATCA-AEOI = -1
              MOVE HIGH-VALUES TO P56-FL-AU-FATCA-AEOI-NULL
           END-IF
           IF IND-P56-TP-CAR-FIN-GIUR = -1
              MOVE HIGH-VALUES TO P56-TP-CAR-FIN-GIUR-NULL
           END-IF
           IF IND-P56-TP-CAR-FIN-GIUR-AT = -1
              MOVE HIGH-VALUES TO P56-TP-CAR-FIN-GIUR-AT-NULL
           END-IF
           IF IND-P56-TP-CAR-FIN-GIUR-PA = -1
              MOVE HIGH-VALUES TO P56-TP-CAR-FIN-GIUR-PA-NULL
           END-IF
           IF IND-P56-FL-ISTITUZ-FIN = -1
              MOVE HIGH-VALUES TO P56-FL-ISTITUZ-FIN-NULL
           END-IF
           IF IND-P56-TP-ORI-FND-TIT-EFF = -1
              MOVE HIGH-VALUES TO P56-TP-ORI-FND-TIT-EFF-NULL
           END-IF
           IF IND-P56-PC-ESP-AG-PA-MSC = -1
              MOVE HIGH-VALUES TO P56-PC-ESP-AG-PA-MSC-NULL
           END-IF
           IF IND-P56-FL-PR-TR-USA = -1
              MOVE HIGH-VALUES TO P56-FL-PR-TR-USA-NULL
           END-IF
           IF IND-P56-FL-PR-TR-NO-USA = -1
              MOVE HIGH-VALUES TO P56-FL-PR-TR-NO-USA-NULL
           END-IF
           IF IND-P56-PC-RIP-PAT-AS-VITA = -1
              MOVE HIGH-VALUES TO P56-PC-RIP-PAT-AS-VITA-NULL
           END-IF
           IF IND-P56-PC-RIP-PAT-IM = -1
              MOVE HIGH-VALUES TO P56-PC-RIP-PAT-IM-NULL
           END-IF
           IF IND-P56-PC-RIP-PAT-SET-IM = -1
              MOVE HIGH-VALUES TO P56-PC-RIP-PAT-SET-IM-NULL
           END-IF
           IF IND-P56-TP-STATUS-AEOI = -1
              MOVE HIGH-VALUES TO P56-TP-STATUS-AEOI-NULL
           END-IF
           IF IND-P56-TP-STATUS-FATCA = -1
              MOVE HIGH-VALUES TO P56-TP-STATUS-FATCA-NULL
           END-IF
           IF IND-P56-FL-RAPP-PA-MSC = -1
              MOVE HIGH-VALUES TO P56-FL-RAPP-PA-MSC-NULL
           END-IF
           IF IND-P56-COD-COMUN-SVOL-ATT = -1
              MOVE HIGH-VALUES TO P56-COD-COMUN-SVOL-ATT-NULL
           END-IF
           IF IND-P56-TP-DT-1O-CON-CLI = -1
              MOVE HIGH-VALUES TO P56-TP-DT-1O-CON-CLI-NULL
           END-IF
           IF IND-P56-TP-MOD-EN-RELA-INT = -1
              MOVE HIGH-VALUES TO P56-TP-MOD-EN-RELA-INT-NULL
           END-IF
           IF IND-P56-TP-REDD-ANNU-LRD = -1
              MOVE HIGH-VALUES TO P56-TP-REDD-ANNU-LRD-NULL
           END-IF
           IF IND-P56-TP-REDD-CON = -1
              MOVE HIGH-VALUES TO P56-TP-REDD-CON-NULL
           END-IF
           IF IND-P56-TP-OPER-SOC-FID = -1
              MOVE HIGH-VALUES TO P56-TP-OPER-SOC-FID-NULL
           END-IF
           IF IND-P56-COD-PA-ESP-MSC-1 = -1
              MOVE HIGH-VALUES TO P56-COD-PA-ESP-MSC-1-NULL
           END-IF
           IF IND-P56-IMP-PA-ESP-MSC-1 = -1
              MOVE HIGH-VALUES TO P56-IMP-PA-ESP-MSC-1-NULL
           END-IF
           IF IND-P56-COD-PA-ESP-MSC-2 = -1
              MOVE HIGH-VALUES TO P56-COD-PA-ESP-MSC-2-NULL
           END-IF
           IF IND-P56-IMP-PA-ESP-MSC-2 = -1
              MOVE HIGH-VALUES TO P56-IMP-PA-ESP-MSC-2-NULL
           END-IF
           IF IND-P56-COD-PA-ESP-MSC-3 = -1
              MOVE HIGH-VALUES TO P56-COD-PA-ESP-MSC-3-NULL
           END-IF
           IF IND-P56-IMP-PA-ESP-MSC-3 = -1
              MOVE HIGH-VALUES TO P56-IMP-PA-ESP-MSC-3-NULL
           END-IF
           IF IND-P56-COD-PA-ESP-MSC-4 = -1
              MOVE HIGH-VALUES TO P56-COD-PA-ESP-MSC-4-NULL
           END-IF
           IF IND-P56-IMP-PA-ESP-MSC-4 = -1
              MOVE HIGH-VALUES TO P56-IMP-PA-ESP-MSC-4-NULL
           END-IF
           IF IND-P56-COD-PA-ESP-MSC-5 = -1
              MOVE HIGH-VALUES TO P56-COD-PA-ESP-MSC-5-NULL
           END-IF
           IF IND-P56-IMP-PA-ESP-MSC-5 = -1
              MOVE HIGH-VALUES TO P56-IMP-PA-ESP-MSC-5-NULL
           END-IF
           IF IND-P56-DESC-ORGN-FND = -1
              MOVE HIGH-VALUES TO P56-DESC-ORGN-FND
           END-IF
           IF IND-P56-COD-AUT-DUE-DIL = -1
              MOVE HIGH-VALUES TO P56-COD-AUT-DUE-DIL-NULL
           END-IF
           IF IND-P56-FL-PR-QUEST-FATCA = -1
              MOVE HIGH-VALUES TO P56-FL-PR-QUEST-FATCA-NULL
           END-IF
           IF IND-P56-FL-PR-QUEST-AEOI = -1
              MOVE HIGH-VALUES TO P56-FL-PR-QUEST-AEOI-NULL
           END-IF
           IF IND-P56-FL-PR-QUEST-OFAC = -1
              MOVE HIGH-VALUES TO P56-FL-PR-QUEST-OFAC-NULL
           END-IF
           IF IND-P56-FL-PR-QUEST-KYC = -1
              MOVE HIGH-VALUES TO P56-FL-PR-QUEST-KYC-NULL
           END-IF
           IF IND-P56-FL-PR-QUEST-MSCQ = -1
              MOVE HIGH-VALUES TO P56-FL-PR-QUEST-MSCQ-NULL
           END-IF
           IF IND-P56-TP-NOT-PREG = -1
              MOVE HIGH-VALUES TO P56-TP-NOT-PREG-NULL
           END-IF
           IF IND-P56-TP-PROC-PNL = -1
              MOVE HIGH-VALUES TO P56-TP-PROC-PNL-NULL
           END-IF
           IF IND-P56-COD-IMP-CAR-PUB = -1
              MOVE HIGH-VALUES TO P56-COD-IMP-CAR-PUB-NULL
           END-IF
           IF IND-P56-OPRZ-SOSPETTE = -1
              MOVE HIGH-VALUES TO P56-OPRZ-SOSPETTE-NULL
           END-IF
           IF IND-P56-ULT-FATT-ANNU = -1
              MOVE HIGH-VALUES TO P56-ULT-FATT-ANNU-NULL
           END-IF
           IF IND-P56-DESC-PEP = -1
              MOVE HIGH-VALUES TO P56-DESC-PEP
           END-IF
           IF IND-P56-NUM-TEL-2 = -1
              MOVE HIGH-VALUES TO P56-NUM-TEL-2-NULL
           END-IF
           IF IND-P56-IMP-AFI = -1
              MOVE HIGH-VALUES TO P56-IMP-AFI-NULL
           END-IF
           IF IND-P56-FL-NEW-PRO = -1
              MOVE HIGH-VALUES TO P56-FL-NEW-PRO-NULL
           END-IF
           IF IND-P56-TP-MOT-CAMBIO-CNTR = -1
              MOVE HIGH-VALUES TO P56-TP-MOT-CAMBIO-CNTR-NULL
           END-IF
           IF IND-P56-DESC-MOT-CAMBIO-CN = -1
              MOVE HIGH-VALUES TO P56-DESC-MOT-CAMBIO-CN
           END-IF
           IF IND-P56-COD-SOGG = -1
              MOVE HIGH-VALUES TO P56-COD-SOGG-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO P56-DS-OPER-SQL
           MOVE 0                   TO P56-DS-VER
           MOVE IDSV0003-USER-NAME TO P56-DS-UTENTE
           MOVE '1'                   TO P56-DS-STATO-ELAB
           MOVE WS-TS-COMPETENZA-AGG-STOR TO P56-DS-TS-CPTZ.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO P56-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO P56-DS-UTENTE
           MOVE WS-TS-COMPETENZA-AGG-STOR TO P56-DS-TS-CPTZ.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF P56-ID-RAPP-ANA-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-ID-RAPP-ANA
           ELSE
              MOVE 0 TO IND-P56-ID-RAPP-ANA
           END-IF
           IF P56-NATURA-OPRZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-NATURA-OPRZ
           ELSE
              MOVE 0 TO IND-P56-NATURA-OPRZ
           END-IF
           IF P56-ORGN-FND-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-ORGN-FND
           ELSE
              MOVE 0 TO IND-P56-ORGN-FND
           END-IF
           IF P56-COD-QLFC-PROF-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-QLFC-PROF
           ELSE
              MOVE 0 TO IND-P56-COD-QLFC-PROF
           END-IF
           IF P56-COD-NAZ-QLFC-PROF-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-NAZ-QLFC-PROF
           ELSE
              MOVE 0 TO IND-P56-COD-NAZ-QLFC-PROF
           END-IF
           IF P56-COD-PRV-QLFC-PROF-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-PRV-QLFC-PROF
           ELSE
              MOVE 0 TO IND-P56-COD-PRV-QLFC-PROF
           END-IF
           IF P56-FNT-REDD-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FNT-REDD
           ELSE
              MOVE 0 TO IND-P56-FNT-REDD
           END-IF
           IF P56-REDD-FATT-ANNU-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-REDD-FATT-ANNU
           ELSE
              MOVE 0 TO IND-P56-REDD-FATT-ANNU
           END-IF
           IF P56-COD-ATECO-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-ATECO
           ELSE
              MOVE 0 TO IND-P56-COD-ATECO
           END-IF
           IF P56-VALUT-COLL-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-VALUT-COLL
           ELSE
              MOVE 0 TO IND-P56-VALUT-COLL
           END-IF
           IF P56-LUOGO-COSTITUZIONE = HIGH-VALUES
              MOVE -1 TO IND-P56-LUOGO-COSTITUZIONE
           ELSE
              MOVE 0 TO IND-P56-LUOGO-COSTITUZIONE
           END-IF
           IF P56-TP-MOVI-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-MOVI
           ELSE
              MOVE 0 TO IND-P56-TP-MOVI
           END-IF
           IF P56-FL-RAG-RAPP-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-RAG-RAPP
           ELSE
              MOVE 0 TO IND-P56-FL-RAG-RAPP
           END-IF
           IF P56-FL-PRSZ-TIT-EFF-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-PRSZ-TIT-EFF
           ELSE
              MOVE 0 TO IND-P56-FL-PRSZ-TIT-EFF
           END-IF
           IF P56-TP-MOT-RISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-MOT-RISC
           ELSE
              MOVE 0 TO IND-P56-TP-MOT-RISC
           END-IF
           IF P56-TP-PNT-VND-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-PNT-VND
           ELSE
              MOVE 0 TO IND-P56-TP-PNT-VND
           END-IF
           IF P56-TP-ADEG-VER-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-ADEG-VER
           ELSE
              MOVE 0 TO IND-P56-TP-ADEG-VER
           END-IF
           IF P56-TP-RELA-ESEC-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-RELA-ESEC
           ELSE
              MOVE 0 TO IND-P56-TP-RELA-ESEC
           END-IF
           IF P56-TP-SCO-FIN-RAPP-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-SCO-FIN-RAPP
           ELSE
              MOVE 0 TO IND-P56-TP-SCO-FIN-RAPP
           END-IF
           IF P56-FL-PRSZ-3O-PAGAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-PRSZ-3O-PAGAT
           ELSE
              MOVE 0 TO IND-P56-FL-PRSZ-3O-PAGAT
           END-IF
           IF P56-AREA-GEO-PROV-FND-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-AREA-GEO-PROV-FND
           ELSE
              MOVE 0 TO IND-P56-AREA-GEO-PROV-FND
           END-IF
           IF P56-TP-DEST-FND-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-DEST-FND
           ELSE
              MOVE 0 TO IND-P56-TP-DEST-FND
           END-IF
           IF P56-FL-PAESE-RESID-AUT-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-PAESE-RESID-AUT
           ELSE
              MOVE 0 TO IND-P56-FL-PAESE-RESID-AUT
           END-IF
           IF P56-FL-PAESE-CIT-AUT-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-PAESE-CIT-AUT
           ELSE
              MOVE 0 TO IND-P56-FL-PAESE-CIT-AUT
           END-IF
           IF P56-FL-PAESE-NAZ-AUT-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-PAESE-NAZ-AUT
           ELSE
              MOVE 0 TO IND-P56-FL-PAESE-NAZ-AUT
           END-IF
           IF P56-COD-PROF-PREC-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-PROF-PREC
           ELSE
              MOVE 0 TO IND-P56-COD-PROF-PREC
           END-IF
           IF P56-FL-AUT-PEP-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-AUT-PEP
           ELSE
              MOVE 0 TO IND-P56-FL-AUT-PEP
           END-IF
           IF P56-FL-IMP-CAR-PUB-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-IMP-CAR-PUB
           ELSE
              MOVE 0 TO IND-P56-FL-IMP-CAR-PUB
           END-IF
           IF P56-FL-LIS-TERR-SORV-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-LIS-TERR-SORV
           ELSE
              MOVE 0 TO IND-P56-FL-LIS-TERR-SORV
           END-IF
           IF P56-TP-SIT-FIN-PAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-SIT-FIN-PAT
           ELSE
              MOVE 0 TO IND-P56-TP-SIT-FIN-PAT
           END-IF
           IF P56-TP-SIT-FIN-PAT-CON-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-SIT-FIN-PAT-CON
           ELSE
              MOVE 0 TO IND-P56-TP-SIT-FIN-PAT-CON
           END-IF
           IF P56-IMP-TOT-AFF-UTIL-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-IMP-TOT-AFF-UTIL
           ELSE
              MOVE 0 TO IND-P56-IMP-TOT-AFF-UTIL
           END-IF
           IF P56-IMP-TOT-FIN-UTIL-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-IMP-TOT-FIN-UTIL
           ELSE
              MOVE 0 TO IND-P56-IMP-TOT-FIN-UTIL
           END-IF
           IF P56-IMP-TOT-AFF-ACC-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-IMP-TOT-AFF-ACC
           ELSE
              MOVE 0 TO IND-P56-IMP-TOT-AFF-ACC
           END-IF
           IF P56-IMP-TOT-FIN-ACC-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-IMP-TOT-FIN-ACC
           ELSE
              MOVE 0 TO IND-P56-IMP-TOT-FIN-ACC
           END-IF
           IF P56-TP-FRM-GIUR-SAV-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-FRM-GIUR-SAV
           ELSE
              MOVE 0 TO IND-P56-TP-FRM-GIUR-SAV
           END-IF
           IF P56-REG-COLL-POLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-REG-COLL-POLI
           ELSE
              MOVE 0 TO IND-P56-REG-COLL-POLI
           END-IF
           IF P56-NUM-TEL-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-NUM-TEL
           ELSE
              MOVE 0 TO IND-P56-NUM-TEL
           END-IF
           IF P56-NUM-DIP-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-NUM-DIP
           ELSE
              MOVE 0 TO IND-P56-NUM-DIP
           END-IF
           IF P56-TP-SIT-FAM-CONV-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-SIT-FAM-CONV
           ELSE
              MOVE 0 TO IND-P56-TP-SIT-FAM-CONV
           END-IF
           IF P56-COD-PROF-CON-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-PROF-CON
           ELSE
              MOVE 0 TO IND-P56-COD-PROF-CON
           END-IF
           IF P56-FL-ES-PROC-PEN-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-ES-PROC-PEN
           ELSE
              MOVE 0 TO IND-P56-FL-ES-PROC-PEN
           END-IF
           IF P56-TP-COND-CLIENTE-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-COND-CLIENTE
           ELSE
              MOVE 0 TO IND-P56-TP-COND-CLIENTE
           END-IF
           IF P56-COD-SAE-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-SAE
           ELSE
              MOVE 0 TO IND-P56-COD-SAE
           END-IF
           IF P56-TP-OPER-ESTERO-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-OPER-ESTERO
           ELSE
              MOVE 0 TO IND-P56-TP-OPER-ESTERO
           END-IF
           IF P56-STAT-OPER-ESTERO-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-STAT-OPER-ESTERO
           ELSE
              MOVE 0 TO IND-P56-STAT-OPER-ESTERO
           END-IF
           IF P56-COD-PRV-SVOL-ATT-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-PRV-SVOL-ATT
           ELSE
              MOVE 0 TO IND-P56-COD-PRV-SVOL-ATT
           END-IF
           IF P56-COD-STAT-SVOL-ATT-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-STAT-SVOL-ATT
           ELSE
              MOVE 0 TO IND-P56-COD-STAT-SVOL-ATT
           END-IF
           IF P56-TP-SOC-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-SOC
           ELSE
              MOVE 0 TO IND-P56-TP-SOC
           END-IF
           IF P56-FL-IND-SOC-QUOT-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-IND-SOC-QUOT
           ELSE
              MOVE 0 TO IND-P56-FL-IND-SOC-QUOT
           END-IF
           IF P56-TP-SIT-GIUR-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-SIT-GIUR
           ELSE
              MOVE 0 TO IND-P56-TP-SIT-GIUR
           END-IF
           IF P56-PC-QUO-DET-TIT-EFF-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-PC-QUO-DET-TIT-EFF
           ELSE
              MOVE 0 TO IND-P56-PC-QUO-DET-TIT-EFF
           END-IF
           IF P56-TP-PRFL-RSH-PEP-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-PRFL-RSH-PEP
           ELSE
              MOVE 0 TO IND-P56-TP-PRFL-RSH-PEP
           END-IF
           IF P56-TP-PEP-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-PEP
           ELSE
              MOVE 0 TO IND-P56-TP-PEP
           END-IF
           IF P56-FL-NOT-PREG-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-NOT-PREG
           ELSE
              MOVE 0 TO IND-P56-FL-NOT-PREG
           END-IF
           IF P56-DT-INI-FNT-REDD-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-DT-INI-FNT-REDD
           ELSE
              MOVE 0 TO IND-P56-DT-INI-FNT-REDD
           END-IF
           IF P56-FNT-REDD-2-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FNT-REDD-2
           ELSE
              MOVE 0 TO IND-P56-FNT-REDD-2
           END-IF
           IF P56-DT-INI-FNT-REDD-2-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-DT-INI-FNT-REDD-2
           ELSE
              MOVE 0 TO IND-P56-DT-INI-FNT-REDD-2
           END-IF
           IF P56-FNT-REDD-3-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FNT-REDD-3
           ELSE
              MOVE 0 TO IND-P56-FNT-REDD-3
           END-IF
           IF P56-DT-INI-FNT-REDD-3-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-DT-INI-FNT-REDD-3
           ELSE
              MOVE 0 TO IND-P56-DT-INI-FNT-REDD-3
           END-IF
           IF P56-MOT-ASS-TIT-EFF = HIGH-VALUES
              MOVE -1 TO IND-P56-MOT-ASS-TIT-EFF
           ELSE
              MOVE 0 TO IND-P56-MOT-ASS-TIT-EFF
           END-IF
           IF P56-FIN-COSTITUZIONE = HIGH-VALUES
              MOVE -1 TO IND-P56-FIN-COSTITUZIONE
           ELSE
              MOVE 0 TO IND-P56-FIN-COSTITUZIONE
           END-IF
           IF P56-DESC-IMP-CAR-PUB = HIGH-VALUES
              MOVE -1 TO IND-P56-DESC-IMP-CAR-PUB
           ELSE
              MOVE 0 TO IND-P56-DESC-IMP-CAR-PUB
           END-IF
           IF P56-DESC-SCO-FIN-RAPP = HIGH-VALUES
              MOVE -1 TO IND-P56-DESC-SCO-FIN-RAPP
           ELSE
              MOVE 0 TO IND-P56-DESC-SCO-FIN-RAPP
           END-IF
           IF P56-DESC-PROC-PNL = HIGH-VALUES
              MOVE -1 TO IND-P56-DESC-PROC-PNL
           ELSE
              MOVE 0 TO IND-P56-DESC-PROC-PNL
           END-IF
           IF P56-DESC-NOT-PREG = HIGH-VALUES
              MOVE -1 TO IND-P56-DESC-NOT-PREG
           ELSE
              MOVE 0 TO IND-P56-DESC-NOT-PREG
           END-IF
           IF P56-ID-ASSICURATI-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-ID-ASSICURATI
           ELSE
              MOVE 0 TO IND-P56-ID-ASSICURATI
           END-IF
           IF P56-REDD-CON-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-REDD-CON
           ELSE
              MOVE 0 TO IND-P56-REDD-CON
           END-IF
           IF P56-DESC-LIB-MOT-RISC = HIGH-VALUES
              MOVE -1 TO IND-P56-DESC-LIB-MOT-RISC
           ELSE
              MOVE 0 TO IND-P56-DESC-LIB-MOT-RISC
           END-IF
           IF P56-TP-MOT-ASS-TIT-EFF-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-MOT-ASS-TIT-EFF
           ELSE
              MOVE 0 TO IND-P56-TP-MOT-ASS-TIT-EFF
           END-IF
           IF P56-TP-RAG-RAPP-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-RAG-RAPP
           ELSE
              MOVE 0 TO IND-P56-TP-RAG-RAPP
           END-IF
           IF P56-COD-CAN-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-CAN
           ELSE
              MOVE 0 TO IND-P56-COD-CAN
           END-IF
           IF P56-TP-FIN-COST-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-FIN-COST
           ELSE
              MOVE 0 TO IND-P56-TP-FIN-COST
           END-IF
           IF P56-NAZ-DEST-FND-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-NAZ-DEST-FND
           ELSE
              MOVE 0 TO IND-P56-NAZ-DEST-FND
           END-IF
           IF P56-FL-AU-FATCA-AEOI-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-AU-FATCA-AEOI
           ELSE
              MOVE 0 TO IND-P56-FL-AU-FATCA-AEOI
           END-IF
           IF P56-TP-CAR-FIN-GIUR-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-CAR-FIN-GIUR
           ELSE
              MOVE 0 TO IND-P56-TP-CAR-FIN-GIUR
           END-IF
           IF P56-TP-CAR-FIN-GIUR-AT-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-CAR-FIN-GIUR-AT
           ELSE
              MOVE 0 TO IND-P56-TP-CAR-FIN-GIUR-AT
           END-IF
           IF P56-TP-CAR-FIN-GIUR-PA-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-CAR-FIN-GIUR-PA
           ELSE
              MOVE 0 TO IND-P56-TP-CAR-FIN-GIUR-PA
           END-IF
           IF P56-FL-ISTITUZ-FIN-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-ISTITUZ-FIN
           ELSE
              MOVE 0 TO IND-P56-FL-ISTITUZ-FIN
           END-IF
           IF P56-TP-ORI-FND-TIT-EFF-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-ORI-FND-TIT-EFF
           ELSE
              MOVE 0 TO IND-P56-TP-ORI-FND-TIT-EFF
           END-IF
           IF P56-PC-ESP-AG-PA-MSC-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-PC-ESP-AG-PA-MSC
           ELSE
              MOVE 0 TO IND-P56-PC-ESP-AG-PA-MSC
           END-IF
           IF P56-FL-PR-TR-USA-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-PR-TR-USA
           ELSE
              MOVE 0 TO IND-P56-FL-PR-TR-USA
           END-IF
           IF P56-FL-PR-TR-NO-USA-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-PR-TR-NO-USA
           ELSE
              MOVE 0 TO IND-P56-FL-PR-TR-NO-USA
           END-IF
           IF P56-PC-RIP-PAT-AS-VITA-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-PC-RIP-PAT-AS-VITA
           ELSE
              MOVE 0 TO IND-P56-PC-RIP-PAT-AS-VITA
           END-IF
           IF P56-PC-RIP-PAT-IM-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-PC-RIP-PAT-IM
           ELSE
              MOVE 0 TO IND-P56-PC-RIP-PAT-IM
           END-IF
           IF P56-PC-RIP-PAT-SET-IM-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-PC-RIP-PAT-SET-IM
           ELSE
              MOVE 0 TO IND-P56-PC-RIP-PAT-SET-IM
           END-IF
           IF P56-TP-STATUS-AEOI-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-STATUS-AEOI
           ELSE
              MOVE 0 TO IND-P56-TP-STATUS-AEOI
           END-IF
           IF P56-TP-STATUS-FATCA-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-STATUS-FATCA
           ELSE
              MOVE 0 TO IND-P56-TP-STATUS-FATCA
           END-IF
           IF P56-FL-RAPP-PA-MSC-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-RAPP-PA-MSC
           ELSE
              MOVE 0 TO IND-P56-FL-RAPP-PA-MSC
           END-IF
           IF P56-COD-COMUN-SVOL-ATT-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-COMUN-SVOL-ATT
           ELSE
              MOVE 0 TO IND-P56-COD-COMUN-SVOL-ATT
           END-IF
           IF P56-TP-DT-1O-CON-CLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-DT-1O-CON-CLI
           ELSE
              MOVE 0 TO IND-P56-TP-DT-1O-CON-CLI
           END-IF
           IF P56-TP-MOD-EN-RELA-INT-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-MOD-EN-RELA-INT
           ELSE
              MOVE 0 TO IND-P56-TP-MOD-EN-RELA-INT
           END-IF
           IF P56-TP-REDD-ANNU-LRD-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-REDD-ANNU-LRD
           ELSE
              MOVE 0 TO IND-P56-TP-REDD-ANNU-LRD
           END-IF
           IF P56-TP-REDD-CON-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-REDD-CON
           ELSE
              MOVE 0 TO IND-P56-TP-REDD-CON
           END-IF
           IF P56-TP-OPER-SOC-FID-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-OPER-SOC-FID
           ELSE
              MOVE 0 TO IND-P56-TP-OPER-SOC-FID
           END-IF
           IF P56-COD-PA-ESP-MSC-1-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-PA-ESP-MSC-1
           ELSE
              MOVE 0 TO IND-P56-COD-PA-ESP-MSC-1
           END-IF
           IF P56-IMP-PA-ESP-MSC-1-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-IMP-PA-ESP-MSC-1
           ELSE
              MOVE 0 TO IND-P56-IMP-PA-ESP-MSC-1
           END-IF
           IF P56-COD-PA-ESP-MSC-2-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-PA-ESP-MSC-2
           ELSE
              MOVE 0 TO IND-P56-COD-PA-ESP-MSC-2
           END-IF
           IF P56-IMP-PA-ESP-MSC-2-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-IMP-PA-ESP-MSC-2
           ELSE
              MOVE 0 TO IND-P56-IMP-PA-ESP-MSC-2
           END-IF
           IF P56-COD-PA-ESP-MSC-3-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-PA-ESP-MSC-3
           ELSE
              MOVE 0 TO IND-P56-COD-PA-ESP-MSC-3
           END-IF
           IF P56-IMP-PA-ESP-MSC-3-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-IMP-PA-ESP-MSC-3
           ELSE
              MOVE 0 TO IND-P56-IMP-PA-ESP-MSC-3
           END-IF
           IF P56-COD-PA-ESP-MSC-4-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-PA-ESP-MSC-4
           ELSE
              MOVE 0 TO IND-P56-COD-PA-ESP-MSC-4
           END-IF
           IF P56-IMP-PA-ESP-MSC-4-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-IMP-PA-ESP-MSC-4
           ELSE
              MOVE 0 TO IND-P56-IMP-PA-ESP-MSC-4
           END-IF
           IF P56-COD-PA-ESP-MSC-5-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-PA-ESP-MSC-5
           ELSE
              MOVE 0 TO IND-P56-COD-PA-ESP-MSC-5
           END-IF
           IF P56-IMP-PA-ESP-MSC-5-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-IMP-PA-ESP-MSC-5
           ELSE
              MOVE 0 TO IND-P56-IMP-PA-ESP-MSC-5
           END-IF
           IF P56-DESC-ORGN-FND = HIGH-VALUES
              MOVE -1 TO IND-P56-DESC-ORGN-FND
           ELSE
              MOVE 0 TO IND-P56-DESC-ORGN-FND
           END-IF
           IF P56-COD-AUT-DUE-DIL-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-AUT-DUE-DIL
           ELSE
              MOVE 0 TO IND-P56-COD-AUT-DUE-DIL
           END-IF
           IF P56-FL-PR-QUEST-FATCA-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-PR-QUEST-FATCA
           ELSE
              MOVE 0 TO IND-P56-FL-PR-QUEST-FATCA
           END-IF
           IF P56-FL-PR-QUEST-AEOI-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-PR-QUEST-AEOI
           ELSE
              MOVE 0 TO IND-P56-FL-PR-QUEST-AEOI
           END-IF
           IF P56-FL-PR-QUEST-OFAC-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-PR-QUEST-OFAC
           ELSE
              MOVE 0 TO IND-P56-FL-PR-QUEST-OFAC
           END-IF
           IF P56-FL-PR-QUEST-KYC-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-PR-QUEST-KYC
           ELSE
              MOVE 0 TO IND-P56-FL-PR-QUEST-KYC
           END-IF
           IF P56-FL-PR-QUEST-MSCQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-PR-QUEST-MSCQ
           ELSE
              MOVE 0 TO IND-P56-FL-PR-QUEST-MSCQ
           END-IF
           IF P56-TP-NOT-PREG-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-NOT-PREG
           ELSE
              MOVE 0 TO IND-P56-TP-NOT-PREG
           END-IF
           IF P56-TP-PROC-PNL-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-PROC-PNL
           ELSE
              MOVE 0 TO IND-P56-TP-PROC-PNL
           END-IF
           IF P56-COD-IMP-CAR-PUB-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-IMP-CAR-PUB
           ELSE
              MOVE 0 TO IND-P56-COD-IMP-CAR-PUB
           END-IF
           IF P56-OPRZ-SOSPETTE-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-OPRZ-SOSPETTE
           ELSE
              MOVE 0 TO IND-P56-OPRZ-SOSPETTE
           END-IF
           IF P56-ULT-FATT-ANNU-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-ULT-FATT-ANNU
           ELSE
              MOVE 0 TO IND-P56-ULT-FATT-ANNU
           END-IF
           IF P56-DESC-PEP = HIGH-VALUES
              MOVE -1 TO IND-P56-DESC-PEP
           ELSE
              MOVE 0 TO IND-P56-DESC-PEP
           END-IF
           IF P56-NUM-TEL-2-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-NUM-TEL-2
           ELSE
              MOVE 0 TO IND-P56-NUM-TEL-2
           END-IF
           IF P56-IMP-AFI-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-IMP-AFI
           ELSE
              MOVE 0 TO IND-P56-IMP-AFI
           END-IF
           IF P56-FL-NEW-PRO-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-FL-NEW-PRO
           ELSE
              MOVE 0 TO IND-P56-FL-NEW-PRO
           END-IF
           IF P56-TP-MOT-CAMBIO-CNTR-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-TP-MOT-CAMBIO-CNTR
           ELSE
              MOVE 0 TO IND-P56-TP-MOT-CAMBIO-CNTR
           END-IF
           IF P56-DESC-MOT-CAMBIO-CN = HIGH-VALUES
              MOVE -1 TO IND-P56-DESC-MOT-CAMBIO-CN
           ELSE
              MOVE 0 TO IND-P56-DESC-MOT-CAMBIO-CN
           END-IF
           IF P56-COD-SOGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-P56-COD-SOGG
           ELSE
              MOVE 0 TO IND-P56-COD-SOGG
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.

           CONTINUE.
       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.
           CONTINUE.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.
           CONTINUE.
       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.
           CONTINUE.
       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           IF IND-P56-DT-INI-FNT-REDD = 0
               MOVE P56-DT-INI-FNT-REDD TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO P56-DT-INI-FNT-REDD-DB
           END-IF
           IF IND-P56-DT-INI-FNT-REDD-2 = 0
               MOVE P56-DT-INI-FNT-REDD-2 TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO P56-DT-INI-FNT-REDD-2-DB
           END-IF
           IF IND-P56-DT-INI-FNT-REDD-3 = 0
               MOVE P56-DT-INI-FNT-REDD-3 TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO P56-DT-INI-FNT-REDD-3-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           IF IND-P56-DT-INI-FNT-REDD = 0
               MOVE P56-DT-INI-FNT-REDD-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO P56-DT-INI-FNT-REDD
           END-IF
           IF IND-P56-DT-INI-FNT-REDD-2 = 0
               MOVE P56-DT-INI-FNT-REDD-2-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO P56-DT-INI-FNT-REDD-2
           END-IF
           IF IND-P56-DT-INI-FNT-REDD-3 = 0
               MOVE P56-DT-INI-FNT-REDD-3-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO P56-DT-INI-FNT-REDD-3
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.


           MOVE LENGTH OF P56-LUOGO-COSTITUZIONE
                       TO P56-LUOGO-COSTITUZIONE-LEN
           MOVE LENGTH OF P56-MOT-ASS-TIT-EFF
                       TO P56-MOT-ASS-TIT-EFF-LEN
           MOVE LENGTH OF P56-FIN-COSTITUZIONE
                       TO P56-FIN-COSTITUZIONE-LEN
           MOVE LENGTH OF P56-DESC-IMP-CAR-PUB
                       TO P56-DESC-IMP-CAR-PUB-LEN
           MOVE LENGTH OF P56-DESC-SCO-FIN-RAPP
                       TO P56-DESC-SCO-FIN-RAPP-LEN
           MOVE LENGTH OF P56-DESC-PROC-PNL
                       TO P56-DESC-PROC-PNL-LEN
           MOVE LENGTH OF P56-DESC-NOT-PREG
                       TO P56-DESC-NOT-PREG-LEN
           MOVE LENGTH OF P56-DESC-LIB-MOT-RISC
                       TO P56-DESC-LIB-MOT-RISC-LEN
           MOVE LENGTH OF P56-DESC-ORGN-FND
                       TO P56-DESC-ORGN-FND-LEN
           MOVE LENGTH OF P56-DESC-PEP
                       TO P56-DESC-PEP-LEN
           MOVE LENGTH OF P56-DESC-MOT-CAMBIO-CN
                       TO P56-DESC-MOT-CAMBIO-CN-LEN.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

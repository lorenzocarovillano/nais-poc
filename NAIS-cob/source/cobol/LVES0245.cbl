      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA VER. 1.0                          **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVES0245.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2009.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *    PROGRAMMA ..... LVES0245
      *    TIPOLOGIA......
      *    PROCESSO....... Emissione polizza Individuale
      *    FUNZIONE....... XXX
      *    DESCRIZIONE.... Gestione di Business della parametro
      *                    oggetto
      *    PAGINA WEB.....
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVES0245'.

      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WS-VARIABILI.
           03 WK-FRQ-MOVI                  PIC 9(05).
           03 WK-FRAZ-MM                   PIC 9(05).
              88 WK-FRAZ-ANN                 VALUE 12.
              88 WK-FRAZ-MEN                 VALUE 1.
              88 WK-FRAZ-BIM                 VALUE 6.
              88 WK-FRAZ-TRI                 VALUE 4.
              88 WK-FRAZ-QUA                 VALUE 3.
              88 WK-FRAZ-SEM                 VALUE 2.
      *--  numero rate anticipate
           03 WK-RATE-ANTIC                PIC S9(14) COMP-3.


           03 WK-PARAM-OGG                 PIC X(012).
              88 WK-PR-RATEANTIC             VALUE 'RATEANTIC'.

      *----------------------------------------------------------------*
      *    COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *--  Tipologiche di PTF
      *----------------------------------------------------------------*
           COPY LCCVXMV0.
           COPY LCCVXSB0.
           COPY LCCVXRA0.
           COPY LCCVXMZ0.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01 IX-INDICI.
          03 IX-TAB-PMO                     PIC S9(04) COMP.
          03 IX-TAB-POG                     PIC S9(04) COMP.
          03 IX-RIC-PMO                     PIC S9(04) COMP.
          03 IX-RIC-POG                     PIC S9(04) COMP.
          03 IX-RIC-RAN                     PIC S9(04) COMP.
      *----------------------------------------------------------------*
      *    FLAG E SWITCH
      *----------------------------------------------------------------*
       01 FL-RICERCA                        PIC X(002) VALUE SPACES.
          88 TROVATO                          VALUE 'SI'.
          88 NON-TROVATO                      VALUE 'NO'.

       01 WK-QUIETANZAMENTO                 PIC X(01) VALUE 'N'.
          88 QUIETANZAMENTO-SI                VALUE 'S'.
          88 QUIETANZAMENTO-NO                VALUE 'N'.

       01 WK-GETRA                          PIC X(01) VALUE 'N'.
          88 GETRA-SI                         VALUE 'S'.
          88 GETRA-NO                         VALUE 'N'.

       01  WS-COMPAGNIA                     PIC 9(05).
           88 UNIPOL                        VALUE 1.
           88 AURORA                        VALUE 6.

      *---------------------------------------------------------------*
       LINKAGE SECTION.
      *---------------------------------------------------------------*
       01 AREA-IDSV0001.
          COPY IDSV0001.

       01 WCOM-AREA-STATI.
          COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.

      *-- POLIZZA
       01 WPOL-AREA-POLIZZA.
          04 WPOL-ELE-POLI-MAX       PIC S9(04) COMP.
          04 WPOL-TAB-POLI.
          COPY LCCVPOL1              REPLACING ==(SF)== BY ==WPOL==.

      *-- ADESIONE
       01 WADE-AREA-ADESIONE.
          04 WADE-ELE-ADES-MAX       PIC S9(04) COMP.
          04 WADE-TAB-ADES.
          COPY LCCVADE1              REPLACING ==(SF)== BY ==WADE==.

      *-- PARAMETRO MOVIMENTO
       01 WPMO-AREA-PARAM-MOV.
          04 WPMO-ELE-PARAM-MOV-MAX   PIC S9(04) COMP.
                COPY LCCVPMOB REPLACING   ==(SF)==  BY ==WPMO==.
          COPY LCCVPMO1               REPLACING ==(SF)== BY ==WPMO==.

      *-- PARAMETRO OGGETTO
       01 WPOG-AREA-PARAM-OGG.
          04 WPOG-ELE-PARAM-OGG-MAX   PIC S9(04) COMP.
                COPY LCCVPOGB REPLACING   ==(SF)==  BY ==WPOG==.
          COPY LCCVPOG1               REPLACING ==(SF)== BY ==WPOG==.

      *--  RAPPORTO ANAGRAFICO
       01 WRAN-AREA-RAPP-ANAG.
          04 WRAN-ELE-RAPP-ANAG-MAX   PIC S9(04) COMP.
                COPY LCCVRANA REPLACING   ==(SF)==  BY ==WRAN==.
          COPY LCCVRAN1               REPLACING ==(SF)== BY ==WRAN==.
      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WPMO-AREA-PARAM-MOV
                                WPOG-AREA-PARAM-OGG
                                WRAN-AREA-RAPP-ANAG.
      *---------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           MOVE IDSV0001-TIPO-MOVIMENTO     TO WS-MOVIMENTO.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA TO WS-COMPAGNIA.

           INITIALIZE  WS-VARIABILI
                       IX-INDICI.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           SET QUIETANZAMENTO-NO  TO TRUE
           SET MOVIM-QUIETA       TO TRUE

           PERFORM RIC-TAB-PMO
              THRU RIC-TAB-PMO-EX

           IF TROVATO
              SET QUIETANZAMENTO-SI   TO TRUE
           ELSE
              SET GETRA-NO      TO TRUE
              SET GENER-TRANCH  TO TRUE
              PERFORM RIC-TAB-PMO
                 THRU RIC-TAB-PMO-EX
              IF TROVATO
                 SET GETRA-SI TO TRUE
              END-IF
           END-IF

      *--  Calcola il frazionamento / periodicita di polizza
           IF QUIETANZAMENTO-SI OR GETRA-SI
              IF WPMO-FRQ-MOVI-NULL(IX-RIC-PMO) = HIGH-VALUE
                 MOVE ZERO  TO WK-FRQ-MOVI
              ELSE
                 MOVE WPMO-FRQ-MOVI(IX-RIC-PMO) TO WK-FRQ-MOVI
                 COMPUTE WK-FRAZ-MM = 12 / WK-FRQ-MOVI
           END-IF

      *--  Ricerca del parametro rateantic
           SET WK-PR-RATEANTIC TO TRUE
           PERFORM RIC-TAB-POG
              THRU RIC-TAB-POG-EX

           IF TROVATO
              MOVE WPOG-VAL-NUM(IX-RIC-POG) TO WK-RATE-ANTIC
              EVALUATE TRUE
      *--         annuale
                  WHEN WK-FRAZ-ANN
                     PERFORM RATEANTIC-ZERO
                        THRU RATEANTIC-ZERO-EX
      *--         mensile
                  WHEN WK-FRAZ-MEN
                     PERFORM GEST-FRAZ-MEN
                        THRU GEST-FRAZ-MEN-EX
      *--         bimestrale
                  WHEN WK-FRAZ-BIM
                     PERFORM RATEANTIC-ZERO
                        THRU RATEANTIC-ZERO-EX
      *--         Trimestrale
                  WHEN WK-FRAZ-TRI
                     PERFORM RATEANTIC-ZERO
                        THRU RATEANTIC-ZERO-EX
      *--         Quadrimestrale
                  WHEN WK-FRAZ-QUA
                     PERFORM RATEANTIC-ZERO
                        THRU RATEANTIC-ZERO-EX
      *--         Semestrale
                  WHEN WK-FRAZ-SEM
                     PERFORM RATEANTIC-ZERO
                        THRU RATEANTIC-ZERO-EX
              END-EVALUATE
           END-IF.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    Gestione Rate anticipate per frazionamento mensile
      *----------------------------------------------------------------*
       GEST-FRAZ-MEN.

      *--  Ricerca codice soggetto del contraente della polizza emessa
           SET CONTRAENTE       TO TRUE
           PERFORM RIC-TAB-RAN
              THRU RIC-TAB-RAN-EX
           IF TROVATO
              MOVE WRAN-TP-MEZ-PAG-ADD(IX-RIC-RAN) TO WS-TP-MEZ-PAG
              EVALUATE TRUE
      *--         Pagamento RID
                  WHEN MEZ-PAG-RID
                     IF WK-RATE-ANTIC < 2
                        PERFORM RATEANTIC-DUE
                           THRU RATEANTIC-DUE-EX
                     END-IF
      *--         Pagamento Trattenuta da stipendio
                  WHEN MEZ-PAG-TRAT-STIP
                     PERFORM RATEANTIC-TRAT-STIP
                        THRU RATEANTIC-TRAT-STIP-EX
              END-EVALUATE
           END-IF.

       GEST-FRAZ-MEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE A ZERO DI RATEANTIC
      *----------------------------------------------------------------*
       RATEANTIC-ZERO.

            MOVE ZERO TO WPOG-VAL-NUM(IX-RIC-POG).

       RATEANTIC-ZERO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE A DUE DI RATEANTIC
      *----------------------------------------------------------------*
       RATEANTIC-DUE.

            MOVE 2 TO WPOG-VAL-NUM(IX-RIC-POG).

       RATEANTIC-DUE-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Ricerca della tabella parametro movimento
      *    Chiave di ricerca : Tipo Movimento
      *----------------------------------------------------------------*
       RIC-TAB-PMO.

           SET NON-TROVATO TO TRUE
           MOVE ZERO          TO IX-RIC-PMO

           PERFORM UNTIL IX-RIC-PMO >= WPMO-ELE-PARAM-MOV-MAX
                      OR TROVATO

              ADD 1 TO IX-RIC-PMO
              IF WPMO-TP-MOVI(IX-RIC-PMO) = WS-MOVIMENTO
                 SET TROVATO TO TRUE
              END-IF

           END-PERFORM.

       RIC-TAB-PMO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Ricerca della tabella parametro oggetto
      *    Chiave di ricerca : Codice parametro
      *----------------------------------------------------------------*
       RIC-TAB-POG.

           SET NON-TROVATO TO TRUE

           MOVE ZERO TO IX-RIC-POG

           PERFORM UNTIL IX-RIC-POG >= WPOG-ELE-PARAM-OGG-MAX
                      OR TROVATO

              ADD 1 TO IX-RIC-POG

              IF WPOG-COD-PARAM(IX-RIC-POG) = WK-PARAM-OGG
                 SET TROVATO TO TRUE
              END-IF

           END-PERFORM.

       RIC-TAB-POG-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Ricerca della tabella Rapporto Anagrafico
      *    Tipologia Ricerca
      *    Chiave di ricerca
      *                    - Tipo rapporto anagrafico
      *----------------------------------------------------------------*
       RIC-TAB-RAN.

           SET NON-TROVATO TO TRUE
           MOVE ZERO          TO IX-RIC-RAN

           PERFORM UNTIL IX-RIC-RAN >= WRAN-ELE-RAPP-ANAG-MAX
                      OR TROVATO

              ADD 1 TO IX-RIC-RAN
              IF WRAN-TP-RAPP-ANA(IX-RIC-RAN) = WS-TP-RAPP-ANA
                 SET TROVATO TO TRUE
              END-IF

           END-PERFORM.

       RIC-TAB-RAN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RATEANTIC PER LA TRATTENUTA DA STIPENDIO
      *----------------------------------------------------------------*
           COPY LVEP0368.
      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.

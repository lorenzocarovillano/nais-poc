      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVES0268.
       AUTHOR.             ATS.
       DATE-WRITTEN.       MARZO 2008.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *    PROGRAMMA ..... LVES0268
      *    TIPOLOGIA ..... MAIN
      *    PROCESSO....... XXX
      *    FUNZIONE....... XXX
      *    DESCRIZIONE.... CREAZIONE PROPOSTA INDIVIDUALE
      *    PAGINA WEB..... CALCOLI
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                            PIC X(08) VALUE 'LVES0268'.
      *----------------------------------------------------------------*
      *    VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    MODULI CHIAMATI
      *----------------------------------------------------------------*
       01  LVES0269                          PIC X(08) VALUE 'LVES0269'.
       01  LVES0270                          PIC X(08) VALUE 'LVES0270'.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.

      *----------------------------------------------------------------*
      *     COMMAREA SERVIZIO
      *----------------------------------------------------------------*
       01 WORK-COMMAREA.
      *--> AREA COMUNE.
           02 WCOM-AREA-STATI.
              COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.
      *--> AREA POLIZZA
           02 WPOL-AREA-POLIZZA.
              04 WPOL-ELE-POLI-MAX       PIC S9(04) COMP.
              04 WPOL-TAB-POLI.
              COPY LCCVPOL1              REPLACING ==(SF)== BY ==WPOL==.
      *--> AREA DATI COLLETTIVA
           02 WDCO-AREA-DT-COLL.
              04 WDCO-ELE-COLL-MAX       PIC S9(04) COMP.
              04 WDCO-TAB-COLL.
              COPY LCCVDCO1              REPLACING ==(SF)== BY ==WDCO==.
      *--> AREA ADESIONE
           02 WADE-AREA-ADESIONE.
              04 WADE-ELE-ADES-MAX       PIC S9(04) COMP.
              04 WADE-TAB-ADES.
              COPY LCCVADE1              REPLACING ==(SF)== BY ==WADE==.
      *--  AREA DEFAULT ADESIONE
           02 WDAD-AREA-DEF-ADES.
              04 WDAD-ELE-DEF-ADES-MAX   PIC S9(04) COMP.
              04 WDAD-TAB-DEF-ADES.
              COPY LCCVDAD1              REPLACING ==(SF)== BY ==WDAD==.
      *--> AREA RAPPORTO ANAGRAFICO
           02 WRAN-AREA-RAPP-ANAG.
              04 WRAN-ELE-RAPP-ANAG-MAX  PIC S9(04) COMP.
                COPY LCCVRANA REPLACING   ==(SF)==  BY ==WRAN==.
              COPY LCCVRAN1              REPLACING ==(SF)== BY ==WRAN==.
      *--> AREA DATI FISCALE ADESIONE
           02 WDFA-AREA-DT-FISC-ADES.
              04 WDFA-ELE-FISC-ADES-MAX  PIC S9(04) COMP.
              04 WDFA-TAB-FISC-ADES.
              COPY LCCVDFA1              REPLACING ==(SF)== BY ==WDFA==.
      *--> AREA GARANZIA
           02 WGRZ-AREA-GARANZIA.
              04 WGRZ-ELE-GARANZIA-MAX   PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
              COPY LCCVGRZ1              REPLACING ==(SF)== BY ==WGRZ==.
      *--> AREA TRANCHE DI GARANZIA
           02 WTGA-AREA-TRANCHE.
              04 WTGA-ELE-TGAR-MAX       PIC S9(04) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==WTGA==.
              COPY LCCVTGA1              REPLACING ==(SF)== BY ==WTGA==.
      *--> AREA SOPRAPREMIO DI GARANZIA
           02 WSPG-AREA-SOPRAP-GAR.
              04 WSPG-ELE-SOPRAP-GAR-MAX PIC S9(04) COMP.
                COPY LCCVSPGA REPLACING   ==(SF)==  BY ==WSPG==.
              COPY LCCVSPG1              REPLACING ==(SF)== BY ==WSPG==.
      *--> AREA STRATEGIA DI INVESTIMENTO
           02 WSDI-AREA-STRA-INV.
              04 WSDI-ELE-STRA-INV-MAX   PIC S9(04) COMP.
              04 WSDI-TAB-STRA-INV       OCCURS 20.
              COPY LCCVSDI1              REPLACING ==(SF)== BY ==WSDI==.
      *--> AREA ASSET ALLOCATION
           02 WALL-AREA-ASSET.
              COPY LCCVALL7.
              COPY LCCVALL1              REPLACING ==(SF)== BY ==WALL==.
      **--   PARAMETRO MOVIMENTO
           02 WPMO-AREA-PARAM-MOV.
              04 WPMO-ELE-PARAM-MOV-MAX  PIC S9(04) COMP.
                COPY LCCVPMOB REPLACING   ==(SF)==  BY ==WPMO==.
              COPY LCCVPMO1              REPLACING ==(SF)== BY ==WPMO==.
      *-- PARAMETRO OGGETTO
           02 WPOG-AREA-PARAM-OGG.
              04 WPOG-ELE-PARAM-OGG-MAX  PIC S9(04) COMP.
                COPY LCCVPOGB REPLACING   ==(SF)==  BY ==WPOG==.
             COPY LCCVPOG1               REPLACING ==(SF)== BY ==WPOG==.
      *--  BENEFICIARI
           02 WBEP-AREA-BENEFICIARI.
              04 WBEP-ELE-BENEFICIARI    PIC S9(04) COMP.
                COPY LCCVBEPA REPLACING   ==(SF)==  BY ==WBEP==.
              COPY LCCVBEP1              REPLACING ==(SF)== BY ==WBEP==.
      *---- QUESTIONARIO
           02 WQUE-AREA-QUEST.
               04 WQUE-ELE-QUEST-MAX     PIC S9(04) COMP.
               04 WQUE-TAB-QUEST         OCCURS 12.
               COPY LCCVQUE1             REPLACING ==(SF)== BY ==WQUE==.
      *---- DETTAGLIO QUESTIONARIO
           02 WDEQ-AREA-DETT-QUEST.
              04 WDEQ-ELE-DETT-QUEST-MAX PIC S9(04) COMP.
              04 WDEQ-TAB-DETT-QUEST     OCCURS 180.
              COPY LCCVDEQ1              REPLACING ==(SF)== BY ==WDEQ==.
      *--  CLAUSOLE
           02 WCLT-AREA-CLAUSOLE.
              04 WCLT-ELE-CLAU-TEST-MAX  PIC S9(004) COMP.
              04 WCLT-TAB-CLAU-TEST      OCCURS 54.
              COPY LCCVCLT1              REPLACING ==(SF)== BY ==WCLT==.
      *--  OGGETTO COLLEGATO
           02 WOCO-AREA-OGG-COLLG.
              04 WOCO-ELE-OGG-COLLG-MAX  PIC S9(04) COMP.
              04 WOCO-TAB-OGG-COLLG      OCCURS 60.
              COPY LCCVOCO1              REPLACING ==(SF)== BY ==WOCO==.
      *--  VINCOLO PEGNO
           02 WL23-AREA-VINC-PEG.
              04 WL23-ELE-VINC-PEG-MAX   PIC S9(04) COMP.
              04 WL23-TAB-VINC-PEG       OCCURS 10.
              COPY LCCVL231              REPLACING ==(SF)== BY ==WL23==.
      *--> GESTIONE RAPPORTO RETE
           02 WRRE-AREA-RAP-RETE.
              04 WRRE-ELE-RAP-RETE-MAX   PIC S9(04) COMP.
              04 WRRE-TAB-RAP-RETE       OCCURS 20.
              COPY LCCVRRE1              REPLACING ==(SF)== BY ==WRRE==.
ITRAT *-- ESTENZIONE POLIZZA CPI PR
ITRAT      02 WP67-AREA-EST-POLI-CPI-PR.
ITRAT         04 WP67-EST-POLI-CPI-PR-MAX PIC S9(04) COMP.
ITRAT         04 WP67-TAB-EST-POLI-CPI-PR.
ITRAT         COPY LCCVP671              REPLACING ==(SF)== BY ==WP67==.

      *--> AREA DATI PAGINA
           02 WPAG-AREA-PAGINA.
              COPY LVEC0268              REPLACING ==(SF)== BY ==WCOM==.
      *--> AREA DATI PAGINA IAS
           02 WPAG-AREA-IAS.
              COPY LVEC0069              REPLACING ==(SF)== BY ==WPAG==.

      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WORK-COMMAREA.
      *---------------------------------------------------------------*
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI                                         *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           IF WALL-ELE-ASSET-ALL-MAX < 0
              INITIALIZE  WALL-AREA-ASSET
           END-IF.


       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           EVALUATE WCOM-TIPO-OPERAZIONE
           WHEN '00'
               PERFORM S1100-CALL-LVES0269
                  THRU EX-S1100
           WHEN '01'
               PERFORM S1101-CALL-LVES0270
                  THRU EX-S1101
           WHEN '02'
           WHEN '03'
           WHEN '04'
           WHEN '05'
               CONTINUE
           END-EVALUATE.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    SERVIZIO PREPARA MAPPA
      *----------------------------------------------------------------*
       S1100-CALL-LVES0269.

           CALL LVES0269 USING  AREA-IDSV0001
                                WCOM-AREA-STATI
                                WPOL-AREA-POLIZZA
                                WDCO-AREA-DT-COLL
                                WADE-AREA-ADESIONE
                                WDAD-AREA-DEF-ADES
                                WRAN-AREA-RAPP-ANAG
                                WDFA-AREA-DT-FISC-ADES
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TRANCHE
                                WSPG-AREA-SOPRAP-GAR
                                WSDI-AREA-STRA-INV
                                WALL-AREA-ASSET
                                WPMO-AREA-PARAM-MOV
                                WPOG-AREA-PARAM-OGG
                                WBEP-AREA-BENEFICIARI
                                WQUE-AREA-QUEST
                                WDEQ-AREA-DETT-QUEST
                                WCLT-AREA-CLAUSOLE
                                WOCO-AREA-OGG-COLLG
                                WRRE-AREA-RAP-RETE
                                WL23-AREA-VINC-PEG
                                WP67-AREA-EST-POLI-CPI-PR
                                WPAG-AREA-PAGINA
                                WPAG-AREA-IAS
           ON EXCEPTION
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'INIZIALIZZAZIONE DELLA PAGINA'
                                             TO CALL-DESC
              MOVE 'S1100-CALL-LVES0269'     TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

       EX-S1100.
           EXIT.
      *----------------------------------------------------------------*
      *    SERVIZIO CONTROLLI
      *----------------------------------------------------------------*
       S1101-CALL-LVES0270.

           CALL LVES0270 USING  AREA-IDSV0001
                                WCOM-AREA-STATI
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TRANCHE
                                WL23-AREA-VINC-PEG
                                WALL-AREA-ASSET
                                WPOG-AREA-PARAM-OGG
      *MOD20110512
                                WPMO-AREA-PARAM-MOV
                                WBEP-AREA-BENEFICIARI
      *MOD20110512
           ON EXCEPTION
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'INIZIALIZZAZIONE DELLA PAGINA'
                                             TO CALL-DESC
              MOVE 'S1100-CALL-LVES0269'     TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

       EX-S1101.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      * ROUTINES GESTIONE ERRORI                                       *
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.

       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSADE0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  07 DIC 2010.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDADE0 END-EXEC.
           EXEC SQL INCLUDE IDBVADE2 END-EXEC.
           EXEC SQL INCLUDE IDBVADE3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVADE1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 ADES.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSADE0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'ADES' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,IB_PREV
                ,IB_OGG
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,ETA_A_SCAD
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,TP_RGM_FISC
                ,TP_RIAT
                ,TP_MOD_PAG_TIT
                ,TP_IAS
                ,DT_VARZ_TP_IAS
                ,PRE_NET_IND
                ,PRE_LRD_IND
                ,RAT_LRD_IND
                ,PRSTZ_INI_IND
                ,FL_COINC_ASSTO
                ,IB_DFLT
                ,MOD_CALC
                ,TP_FNT_CNBTVA
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,PC_AZ
                ,PC_ADER
                ,PC_TFR
                ,PC_VOLO
                ,DT_NOVA_RGM_FISC
                ,FL_ATTIV
                ,IMP_REC_RIT_VIS
                ,IMP_REC_RIT_ACC
                ,FL_VARZ_STAT_TBGC
                ,FL_PROVZA_MIGRAZ
                ,IMPB_VIS_DA_REC
                ,DT_DECOR_PREST_BAN
                ,DT_EFF_VARZ_STAT_T
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CUM_CNBT_CAP
                ,IMP_GAR_CNBT
                ,DT_ULT_CONS_CNBT
                ,IDEN_ISC_FND
                ,NUM_RAT_PIAN
                ,DT_PRESC
                ,CONCS_PREST
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
             FROM ADES
             WHERE     DS_RIGA = :ADE-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO ADES
                     (
                        ID_ADES
                       ,ID_POLI
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,IB_PREV
                       ,IB_OGG
                       ,COD_COMP_ANIA
                       ,DT_DECOR
                       ,DT_SCAD
                       ,ETA_A_SCAD
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,TP_RGM_FISC
                       ,TP_RIAT
                       ,TP_MOD_PAG_TIT
                       ,TP_IAS
                       ,DT_VARZ_TP_IAS
                       ,PRE_NET_IND
                       ,PRE_LRD_IND
                       ,RAT_LRD_IND
                       ,PRSTZ_INI_IND
                       ,FL_COINC_ASSTO
                       ,IB_DFLT
                       ,MOD_CALC
                       ,TP_FNT_CNBTVA
                       ,IMP_AZ
                       ,IMP_ADER
                       ,IMP_TFR
                       ,IMP_VOLO
                       ,PC_AZ
                       ,PC_ADER
                       ,PC_TFR
                       ,PC_VOLO
                       ,DT_NOVA_RGM_FISC
                       ,FL_ATTIV
                       ,IMP_REC_RIT_VIS
                       ,IMP_REC_RIT_ACC
                       ,FL_VARZ_STAT_TBGC
                       ,FL_PROVZA_MIGRAZ
                       ,IMPB_VIS_DA_REC
                       ,DT_DECOR_PREST_BAN
                       ,DT_EFF_VARZ_STAT_T
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,CUM_CNBT_CAP
                       ,IMP_GAR_CNBT
                       ,DT_ULT_CONS_CNBT
                       ,IDEN_ISC_FND
                       ,NUM_RAT_PIAN
                       ,DT_PRESC
                       ,CONCS_PREST
                     )
                 VALUES
                     (
                       :ADE-ID-ADES
                       ,:ADE-ID-POLI
                       ,:ADE-ID-MOVI-CRZ
                       ,:ADE-ID-MOVI-CHIU
                        :IND-ADE-ID-MOVI-CHIU
                       ,:ADE-DT-INI-EFF-DB
                       ,:ADE-DT-END-EFF-DB
                       ,:ADE-IB-PREV
                        :IND-ADE-IB-PREV
                       ,:ADE-IB-OGG
                        :IND-ADE-IB-OGG
                       ,:ADE-COD-COMP-ANIA
                       ,:ADE-DT-DECOR-DB
                        :IND-ADE-DT-DECOR
                       ,:ADE-DT-SCAD-DB
                        :IND-ADE-DT-SCAD
                       ,:ADE-ETA-A-SCAD
                        :IND-ADE-ETA-A-SCAD
                       ,:ADE-DUR-AA
                        :IND-ADE-DUR-AA
                       ,:ADE-DUR-MM
                        :IND-ADE-DUR-MM
                       ,:ADE-DUR-GG
                        :IND-ADE-DUR-GG
                       ,:ADE-TP-RGM-FISC
                       ,:ADE-TP-RIAT
                        :IND-ADE-TP-RIAT
                       ,:ADE-TP-MOD-PAG-TIT
                       ,:ADE-TP-IAS
                        :IND-ADE-TP-IAS
                       ,:ADE-DT-VARZ-TP-IAS-DB
                        :IND-ADE-DT-VARZ-TP-IAS
                       ,:ADE-PRE-NET-IND
                        :IND-ADE-PRE-NET-IND
                       ,:ADE-PRE-LRD-IND
                        :IND-ADE-PRE-LRD-IND
                       ,:ADE-RAT-LRD-IND
                        :IND-ADE-RAT-LRD-IND
                       ,:ADE-PRSTZ-INI-IND
                        :IND-ADE-PRSTZ-INI-IND
                       ,:ADE-FL-COINC-ASSTO
                        :IND-ADE-FL-COINC-ASSTO
                       ,:ADE-IB-DFLT
                        :IND-ADE-IB-DFLT
                       ,:ADE-MOD-CALC
                        :IND-ADE-MOD-CALC
                       ,:ADE-TP-FNT-CNBTVA
                        :IND-ADE-TP-FNT-CNBTVA
                       ,:ADE-IMP-AZ
                        :IND-ADE-IMP-AZ
                       ,:ADE-IMP-ADER
                        :IND-ADE-IMP-ADER
                       ,:ADE-IMP-TFR
                        :IND-ADE-IMP-TFR
                       ,:ADE-IMP-VOLO
                        :IND-ADE-IMP-VOLO
                       ,:ADE-PC-AZ
                        :IND-ADE-PC-AZ
                       ,:ADE-PC-ADER
                        :IND-ADE-PC-ADER
                       ,:ADE-PC-TFR
                        :IND-ADE-PC-TFR
                       ,:ADE-PC-VOLO
                        :IND-ADE-PC-VOLO
                       ,:ADE-DT-NOVA-RGM-FISC-DB
                        :IND-ADE-DT-NOVA-RGM-FISC
                       ,:ADE-FL-ATTIV
                        :IND-ADE-FL-ATTIV
                       ,:ADE-IMP-REC-RIT-VIS
                        :IND-ADE-IMP-REC-RIT-VIS
                       ,:ADE-IMP-REC-RIT-ACC
                        :IND-ADE-IMP-REC-RIT-ACC
                       ,:ADE-FL-VARZ-STAT-TBGC
                        :IND-ADE-FL-VARZ-STAT-TBGC
                       ,:ADE-FL-PROVZA-MIGRAZ
                        :IND-ADE-FL-PROVZA-MIGRAZ
                       ,:ADE-IMPB-VIS-DA-REC
                        :IND-ADE-IMPB-VIS-DA-REC
                       ,:ADE-DT-DECOR-PREST-BAN-DB
                        :IND-ADE-DT-DECOR-PREST-BAN
                       ,:ADE-DT-EFF-VARZ-STAT-T-DB
                        :IND-ADE-DT-EFF-VARZ-STAT-T
                       ,:ADE-DS-RIGA
                       ,:ADE-DS-OPER-SQL
                       ,:ADE-DS-VER
                       ,:ADE-DS-TS-INI-CPTZ
                       ,:ADE-DS-TS-END-CPTZ
                       ,:ADE-DS-UTENTE
                       ,:ADE-DS-STATO-ELAB
                       ,:ADE-CUM-CNBT-CAP
                        :IND-ADE-CUM-CNBT-CAP
                       ,:ADE-IMP-GAR-CNBT
                        :IND-ADE-IMP-GAR-CNBT
                       ,:ADE-DT-ULT-CONS-CNBT-DB
                        :IND-ADE-DT-ULT-CONS-CNBT
                       ,:ADE-IDEN-ISC-FND
                        :IND-ADE-IDEN-ISC-FND
                       ,:ADE-NUM-RAT-PIAN
                        :IND-ADE-NUM-RAT-PIAN
                       ,:ADE-DT-PRESC-DB
                        :IND-ADE-DT-PRESC
                       ,:ADE-CONCS-PREST
                        :IND-ADE-CONCS-PREST
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE ADES SET

                   ID_ADES                =
                :ADE-ID-ADES
                  ,ID_POLI                =
                :ADE-ID-POLI
                  ,ID_MOVI_CRZ            =
                :ADE-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :ADE-ID-MOVI-CHIU
                                       :IND-ADE-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :ADE-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :ADE-DT-END-EFF-DB
                  ,IB_PREV                =
                :ADE-IB-PREV
                                       :IND-ADE-IB-PREV
                  ,IB_OGG                 =
                :ADE-IB-OGG
                                       :IND-ADE-IB-OGG
                  ,COD_COMP_ANIA          =
                :ADE-COD-COMP-ANIA
                  ,DT_DECOR               =
           :ADE-DT-DECOR-DB
                                       :IND-ADE-DT-DECOR
                  ,DT_SCAD                =
           :ADE-DT-SCAD-DB
                                       :IND-ADE-DT-SCAD
                  ,ETA_A_SCAD             =
                :ADE-ETA-A-SCAD
                                       :IND-ADE-ETA-A-SCAD
                  ,DUR_AA                 =
                :ADE-DUR-AA
                                       :IND-ADE-DUR-AA
                  ,DUR_MM                 =
                :ADE-DUR-MM
                                       :IND-ADE-DUR-MM
                  ,DUR_GG                 =
                :ADE-DUR-GG
                                       :IND-ADE-DUR-GG
                  ,TP_RGM_FISC            =
                :ADE-TP-RGM-FISC
                  ,TP_RIAT                =
                :ADE-TP-RIAT
                                       :IND-ADE-TP-RIAT
                  ,TP_MOD_PAG_TIT         =
                :ADE-TP-MOD-PAG-TIT
                  ,TP_IAS                 =
                :ADE-TP-IAS
                                       :IND-ADE-TP-IAS
                  ,DT_VARZ_TP_IAS         =
           :ADE-DT-VARZ-TP-IAS-DB
                                       :IND-ADE-DT-VARZ-TP-IAS
                  ,PRE_NET_IND            =
                :ADE-PRE-NET-IND
                                       :IND-ADE-PRE-NET-IND
                  ,PRE_LRD_IND            =
                :ADE-PRE-LRD-IND
                                       :IND-ADE-PRE-LRD-IND
                  ,RAT_LRD_IND            =
                :ADE-RAT-LRD-IND
                                       :IND-ADE-RAT-LRD-IND
                  ,PRSTZ_INI_IND          =
                :ADE-PRSTZ-INI-IND
                                       :IND-ADE-PRSTZ-INI-IND
                  ,FL_COINC_ASSTO         =
                :ADE-FL-COINC-ASSTO
                                       :IND-ADE-FL-COINC-ASSTO
                  ,IB_DFLT                =
                :ADE-IB-DFLT
                                       :IND-ADE-IB-DFLT
                  ,MOD_CALC               =
                :ADE-MOD-CALC
                                       :IND-ADE-MOD-CALC
                  ,TP_FNT_CNBTVA          =
                :ADE-TP-FNT-CNBTVA
                                       :IND-ADE-TP-FNT-CNBTVA
                  ,IMP_AZ                 =
                :ADE-IMP-AZ
                                       :IND-ADE-IMP-AZ
                  ,IMP_ADER               =
                :ADE-IMP-ADER
                                       :IND-ADE-IMP-ADER
                  ,IMP_TFR                =
                :ADE-IMP-TFR
                                       :IND-ADE-IMP-TFR
                  ,IMP_VOLO               =
                :ADE-IMP-VOLO
                                       :IND-ADE-IMP-VOLO
                  ,PC_AZ                  =
                :ADE-PC-AZ
                                       :IND-ADE-PC-AZ
                  ,PC_ADER                =
                :ADE-PC-ADER
                                       :IND-ADE-PC-ADER
                  ,PC_TFR                 =
                :ADE-PC-TFR
                                       :IND-ADE-PC-TFR
                  ,PC_VOLO                =
                :ADE-PC-VOLO
                                       :IND-ADE-PC-VOLO
                  ,DT_NOVA_RGM_FISC       =
           :ADE-DT-NOVA-RGM-FISC-DB
                                       :IND-ADE-DT-NOVA-RGM-FISC
                  ,FL_ATTIV               =
                :ADE-FL-ATTIV
                                       :IND-ADE-FL-ATTIV
                  ,IMP_REC_RIT_VIS        =
                :ADE-IMP-REC-RIT-VIS
                                       :IND-ADE-IMP-REC-RIT-VIS
                  ,IMP_REC_RIT_ACC        =
                :ADE-IMP-REC-RIT-ACC
                                       :IND-ADE-IMP-REC-RIT-ACC
                  ,FL_VARZ_STAT_TBGC      =
                :ADE-FL-VARZ-STAT-TBGC
                                       :IND-ADE-FL-VARZ-STAT-TBGC
                  ,FL_PROVZA_MIGRAZ       =
                :ADE-FL-PROVZA-MIGRAZ
                                       :IND-ADE-FL-PROVZA-MIGRAZ
                  ,IMPB_VIS_DA_REC        =
                :ADE-IMPB-VIS-DA-REC
                                       :IND-ADE-IMPB-VIS-DA-REC
                  ,DT_DECOR_PREST_BAN     =
           :ADE-DT-DECOR-PREST-BAN-DB
                                       :IND-ADE-DT-DECOR-PREST-BAN
                  ,DT_EFF_VARZ_STAT_T     =
           :ADE-DT-EFF-VARZ-STAT-T-DB
                                       :IND-ADE-DT-EFF-VARZ-STAT-T
                  ,DS_RIGA                =
                :ADE-DS-RIGA
                  ,DS_OPER_SQL            =
                :ADE-DS-OPER-SQL
                  ,DS_VER                 =
                :ADE-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :ADE-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :ADE-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :ADE-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :ADE-DS-STATO-ELAB
                  ,CUM_CNBT_CAP           =
                :ADE-CUM-CNBT-CAP
                                       :IND-ADE-CUM-CNBT-CAP
                  ,IMP_GAR_CNBT           =
                :ADE-IMP-GAR-CNBT
                                       :IND-ADE-IMP-GAR-CNBT
                  ,DT_ULT_CONS_CNBT       =
           :ADE-DT-ULT-CONS-CNBT-DB
                                       :IND-ADE-DT-ULT-CONS-CNBT
                  ,IDEN_ISC_FND           =
                :ADE-IDEN-ISC-FND
                                       :IND-ADE-IDEN-ISC-FND
                  ,NUM_RAT_PIAN           =
                :ADE-NUM-RAT-PIAN
                                       :IND-ADE-NUM-RAT-PIAN
                  ,DT_PRESC               =
           :ADE-DT-PRESC-DB
                                       :IND-ADE-DT-PRESC
                  ,CONCS_PREST            =
                :ADE-CONCS-PREST
                                       :IND-ADE-CONCS-PREST
                WHERE     DS_RIGA = :ADE-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM ADES
                WHERE     DS_RIGA = :ADE-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-ADE CURSOR FOR
              SELECT
                     ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,IB_PREV
                    ,IB_OGG
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_SCAD
                    ,ETA_A_SCAD
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,TP_RGM_FISC
                    ,TP_RIAT
                    ,TP_MOD_PAG_TIT
                    ,TP_IAS
                    ,DT_VARZ_TP_IAS
                    ,PRE_NET_IND
                    ,PRE_LRD_IND
                    ,RAT_LRD_IND
                    ,PRSTZ_INI_IND
                    ,FL_COINC_ASSTO
                    ,IB_DFLT
                    ,MOD_CALC
                    ,TP_FNT_CNBTVA
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,PC_AZ
                    ,PC_ADER
                    ,PC_TFR
                    ,PC_VOLO
                    ,DT_NOVA_RGM_FISC
                    ,FL_ATTIV
                    ,IMP_REC_RIT_VIS
                    ,IMP_REC_RIT_ACC
                    ,FL_VARZ_STAT_TBGC
                    ,FL_PROVZA_MIGRAZ
                    ,IMPB_VIS_DA_REC
                    ,DT_DECOR_PREST_BAN
                    ,DT_EFF_VARZ_STAT_T
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,CUM_CNBT_CAP
                    ,IMP_GAR_CNBT
                    ,DT_ULT_CONS_CNBT
                    ,IDEN_ISC_FND
                    ,NUM_RAT_PIAN
                    ,DT_PRESC
                    ,CONCS_PREST
              FROM ADES
              WHERE     ID_ADES = :ADE-ID-ADES
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,IB_PREV
                ,IB_OGG
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,ETA_A_SCAD
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,TP_RGM_FISC
                ,TP_RIAT
                ,TP_MOD_PAG_TIT
                ,TP_IAS
                ,DT_VARZ_TP_IAS
                ,PRE_NET_IND
                ,PRE_LRD_IND
                ,RAT_LRD_IND
                ,PRSTZ_INI_IND
                ,FL_COINC_ASSTO
                ,IB_DFLT
                ,MOD_CALC
                ,TP_FNT_CNBTVA
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,PC_AZ
                ,PC_ADER
                ,PC_TFR
                ,PC_VOLO
                ,DT_NOVA_RGM_FISC
                ,FL_ATTIV
                ,IMP_REC_RIT_VIS
                ,IMP_REC_RIT_ACC
                ,FL_VARZ_STAT_TBGC
                ,FL_PROVZA_MIGRAZ
                ,IMPB_VIS_DA_REC
                ,DT_DECOR_PREST_BAN
                ,DT_EFF_VARZ_STAT_T
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CUM_CNBT_CAP
                ,IMP_GAR_CNBT
                ,DT_ULT_CONS_CNBT
                ,IDEN_ISC_FND
                ,NUM_RAT_PIAN
                ,DT_PRESC
                ,CONCS_PREST
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
             FROM ADES
             WHERE     ID_ADES = :ADE-ID-ADES
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE ADES SET

                   ID_ADES                =
                :ADE-ID-ADES
                  ,ID_POLI                =
                :ADE-ID-POLI
                  ,ID_MOVI_CRZ            =
                :ADE-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :ADE-ID-MOVI-CHIU
                                       :IND-ADE-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :ADE-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :ADE-DT-END-EFF-DB
                  ,IB_PREV                =
                :ADE-IB-PREV
                                       :IND-ADE-IB-PREV
                  ,IB_OGG                 =
                :ADE-IB-OGG
                                       :IND-ADE-IB-OGG
                  ,COD_COMP_ANIA          =
                :ADE-COD-COMP-ANIA
                  ,DT_DECOR               =
           :ADE-DT-DECOR-DB
                                       :IND-ADE-DT-DECOR
                  ,DT_SCAD                =
           :ADE-DT-SCAD-DB
                                       :IND-ADE-DT-SCAD
                  ,ETA_A_SCAD             =
                :ADE-ETA-A-SCAD
                                       :IND-ADE-ETA-A-SCAD
                  ,DUR_AA                 =
                :ADE-DUR-AA
                                       :IND-ADE-DUR-AA
                  ,DUR_MM                 =
                :ADE-DUR-MM
                                       :IND-ADE-DUR-MM
                  ,DUR_GG                 =
                :ADE-DUR-GG
                                       :IND-ADE-DUR-GG
                  ,TP_RGM_FISC            =
                :ADE-TP-RGM-FISC
                  ,TP_RIAT                =
                :ADE-TP-RIAT
                                       :IND-ADE-TP-RIAT
                  ,TP_MOD_PAG_TIT         =
                :ADE-TP-MOD-PAG-TIT
                  ,TP_IAS                 =
                :ADE-TP-IAS
                                       :IND-ADE-TP-IAS
                  ,DT_VARZ_TP_IAS         =
           :ADE-DT-VARZ-TP-IAS-DB
                                       :IND-ADE-DT-VARZ-TP-IAS
                  ,PRE_NET_IND            =
                :ADE-PRE-NET-IND
                                       :IND-ADE-PRE-NET-IND
                  ,PRE_LRD_IND            =
                :ADE-PRE-LRD-IND
                                       :IND-ADE-PRE-LRD-IND
                  ,RAT_LRD_IND            =
                :ADE-RAT-LRD-IND
                                       :IND-ADE-RAT-LRD-IND
                  ,PRSTZ_INI_IND          =
                :ADE-PRSTZ-INI-IND
                                       :IND-ADE-PRSTZ-INI-IND
                  ,FL_COINC_ASSTO         =
                :ADE-FL-COINC-ASSTO
                                       :IND-ADE-FL-COINC-ASSTO
                  ,IB_DFLT                =
                :ADE-IB-DFLT
                                       :IND-ADE-IB-DFLT
                  ,MOD_CALC               =
                :ADE-MOD-CALC
                                       :IND-ADE-MOD-CALC
                  ,TP_FNT_CNBTVA          =
                :ADE-TP-FNT-CNBTVA
                                       :IND-ADE-TP-FNT-CNBTVA
                  ,IMP_AZ                 =
                :ADE-IMP-AZ
                                       :IND-ADE-IMP-AZ
                  ,IMP_ADER               =
                :ADE-IMP-ADER
                                       :IND-ADE-IMP-ADER
                  ,IMP_TFR                =
                :ADE-IMP-TFR
                                       :IND-ADE-IMP-TFR
                  ,IMP_VOLO               =
                :ADE-IMP-VOLO
                                       :IND-ADE-IMP-VOLO
                  ,PC_AZ                  =
                :ADE-PC-AZ
                                       :IND-ADE-PC-AZ
                  ,PC_ADER                =
                :ADE-PC-ADER
                                       :IND-ADE-PC-ADER
                  ,PC_TFR                 =
                :ADE-PC-TFR
                                       :IND-ADE-PC-TFR
                  ,PC_VOLO                =
                :ADE-PC-VOLO
                                       :IND-ADE-PC-VOLO
                  ,DT_NOVA_RGM_FISC       =
           :ADE-DT-NOVA-RGM-FISC-DB
                                       :IND-ADE-DT-NOVA-RGM-FISC
                  ,FL_ATTIV               =
                :ADE-FL-ATTIV
                                       :IND-ADE-FL-ATTIV
                  ,IMP_REC_RIT_VIS        =
                :ADE-IMP-REC-RIT-VIS
                                       :IND-ADE-IMP-REC-RIT-VIS
                  ,IMP_REC_RIT_ACC        =
                :ADE-IMP-REC-RIT-ACC
                                       :IND-ADE-IMP-REC-RIT-ACC
                  ,FL_VARZ_STAT_TBGC      =
                :ADE-FL-VARZ-STAT-TBGC
                                       :IND-ADE-FL-VARZ-STAT-TBGC
                  ,FL_PROVZA_MIGRAZ       =
                :ADE-FL-PROVZA-MIGRAZ
                                       :IND-ADE-FL-PROVZA-MIGRAZ
                  ,IMPB_VIS_DA_REC        =
                :ADE-IMPB-VIS-DA-REC
                                       :IND-ADE-IMPB-VIS-DA-REC
                  ,DT_DECOR_PREST_BAN     =
           :ADE-DT-DECOR-PREST-BAN-DB
                                       :IND-ADE-DT-DECOR-PREST-BAN
                  ,DT_EFF_VARZ_STAT_T     =
           :ADE-DT-EFF-VARZ-STAT-T-DB
                                       :IND-ADE-DT-EFF-VARZ-STAT-T
                  ,DS_RIGA                =
                :ADE-DS-RIGA
                  ,DS_OPER_SQL            =
                :ADE-DS-OPER-SQL
                  ,DS_VER                 =
                :ADE-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :ADE-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :ADE-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :ADE-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :ADE-DS-STATO-ELAB
                  ,CUM_CNBT_CAP           =
                :ADE-CUM-CNBT-CAP
                                       :IND-ADE-CUM-CNBT-CAP
                  ,IMP_GAR_CNBT           =
                :ADE-IMP-GAR-CNBT
                                       :IND-ADE-IMP-GAR-CNBT
                  ,DT_ULT_CONS_CNBT       =
           :ADE-DT-ULT-CONS-CNBT-DB
                                       :IND-ADE-DT-ULT-CONS-CNBT
                  ,IDEN_ISC_FND           =
                :ADE-IDEN-ISC-FND
                                       :IND-ADE-IDEN-ISC-FND
                  ,NUM_RAT_PIAN           =
                :ADE-NUM-RAT-PIAN
                                       :IND-ADE-NUM-RAT-PIAN
                  ,DT_PRESC               =
           :ADE-DT-PRESC-DB
                                       :IND-ADE-DT-PRESC
                  ,CONCS_PREST            =
                :ADE-CONCS-PREST
                                       :IND-ADE-CONCS-PREST
                WHERE     DS_RIGA = :ADE-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-ADE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-ADE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-ADE
           INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-EFF-ADE CURSOR FOR
              SELECT
                     ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,IB_PREV
                    ,IB_OGG
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_SCAD
                    ,ETA_A_SCAD
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,TP_RGM_FISC
                    ,TP_RIAT
                    ,TP_MOD_PAG_TIT
                    ,TP_IAS
                    ,DT_VARZ_TP_IAS
                    ,PRE_NET_IND
                    ,PRE_LRD_IND
                    ,RAT_LRD_IND
                    ,PRSTZ_INI_IND
                    ,FL_COINC_ASSTO
                    ,IB_DFLT
                    ,MOD_CALC
                    ,TP_FNT_CNBTVA
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,PC_AZ
                    ,PC_ADER
                    ,PC_TFR
                    ,PC_VOLO
                    ,DT_NOVA_RGM_FISC
                    ,FL_ATTIV
                    ,IMP_REC_RIT_VIS
                    ,IMP_REC_RIT_ACC
                    ,FL_VARZ_STAT_TBGC
                    ,FL_PROVZA_MIGRAZ
                    ,IMPB_VIS_DA_REC
                    ,DT_DECOR_PREST_BAN
                    ,DT_EFF_VARZ_STAT_T
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,CUM_CNBT_CAP
                    ,IMP_GAR_CNBT
                    ,DT_ULT_CONS_CNBT
                    ,IDEN_ISC_FND
                    ,NUM_RAT_PIAN
                    ,DT_PRESC
                    ,CONCS_PREST
              FROM ADES
              WHERE     ID_POLI = :ADE-ID-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_ADES ASC

           END-EXEC.

       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,IB_PREV
                ,IB_OGG
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,ETA_A_SCAD
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,TP_RGM_FISC
                ,TP_RIAT
                ,TP_MOD_PAG_TIT
                ,TP_IAS
                ,DT_VARZ_TP_IAS
                ,PRE_NET_IND
                ,PRE_LRD_IND
                ,RAT_LRD_IND
                ,PRSTZ_INI_IND
                ,FL_COINC_ASSTO
                ,IB_DFLT
                ,MOD_CALC
                ,TP_FNT_CNBTVA
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,PC_AZ
                ,PC_ADER
                ,PC_TFR
                ,PC_VOLO
                ,DT_NOVA_RGM_FISC
                ,FL_ATTIV
                ,IMP_REC_RIT_VIS
                ,IMP_REC_RIT_ACC
                ,FL_VARZ_STAT_TBGC
                ,FL_PROVZA_MIGRAZ
                ,IMPB_VIS_DA_REC
                ,DT_DECOR_PREST_BAN
                ,DT_EFF_VARZ_STAT_T
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CUM_CNBT_CAP
                ,IMP_GAR_CNBT
                ,DT_ULT_CONS_CNBT
                ,IDEN_ISC_FND
                ,NUM_RAT_PIAN
                ,DT_PRESC
                ,CONCS_PREST
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
             FROM ADES
             WHERE     ID_POLI = :ADE-ID-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           EXEC SQL
                OPEN C-IDP-EFF-ADE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           EXEC SQL
                CLOSE C-IDP-EFF-ADE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           EXEC SQL
                FETCH C-IDP-EFF-ADE
           INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IBO-EFF-ADE CURSOR FOR
              SELECT
                     ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,IB_PREV
                    ,IB_OGG
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_SCAD
                    ,ETA_A_SCAD
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,TP_RGM_FISC
                    ,TP_RIAT
                    ,TP_MOD_PAG_TIT
                    ,TP_IAS
                    ,DT_VARZ_TP_IAS
                    ,PRE_NET_IND
                    ,PRE_LRD_IND
                    ,RAT_LRD_IND
                    ,PRSTZ_INI_IND
                    ,FL_COINC_ASSTO
                    ,IB_DFLT
                    ,MOD_CALC
                    ,TP_FNT_CNBTVA
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,PC_AZ
                    ,PC_ADER
                    ,PC_TFR
                    ,PC_VOLO
                    ,DT_NOVA_RGM_FISC
                    ,FL_ATTIV
                    ,IMP_REC_RIT_VIS
                    ,IMP_REC_RIT_ACC
                    ,FL_VARZ_STAT_TBGC
                    ,FL_PROVZA_MIGRAZ
                    ,IMPB_VIS_DA_REC
                    ,DT_DECOR_PREST_BAN
                    ,DT_EFF_VARZ_STAT_T
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,CUM_CNBT_CAP
                    ,IMP_GAR_CNBT
                    ,DT_ULT_CONS_CNBT
                    ,IDEN_ISC_FND
                    ,NUM_RAT_PIAN
                    ,DT_PRESC
                    ,CONCS_PREST
              FROM ADES
              WHERE     IB_OGG = :ADE-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_ADES ASC

           END-EXEC.

       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,IB_PREV
                ,IB_OGG
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,ETA_A_SCAD
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,TP_RGM_FISC
                ,TP_RIAT
                ,TP_MOD_PAG_TIT
                ,TP_IAS
                ,DT_VARZ_TP_IAS
                ,PRE_NET_IND
                ,PRE_LRD_IND
                ,RAT_LRD_IND
                ,PRSTZ_INI_IND
                ,FL_COINC_ASSTO
                ,IB_DFLT
                ,MOD_CALC
                ,TP_FNT_CNBTVA
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,PC_AZ
                ,PC_ADER
                ,PC_TFR
                ,PC_VOLO
                ,DT_NOVA_RGM_FISC
                ,FL_ATTIV
                ,IMP_REC_RIT_VIS
                ,IMP_REC_RIT_ACC
                ,FL_VARZ_STAT_TBGC
                ,FL_PROVZA_MIGRAZ
                ,IMPB_VIS_DA_REC
                ,DT_DECOR_PREST_BAN
                ,DT_EFF_VARZ_STAT_T
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CUM_CNBT_CAP
                ,IMP_GAR_CNBT
                ,DT_ULT_CONS_CNBT
                ,IDEN_ISC_FND
                ,NUM_RAT_PIAN
                ,DT_PRESC
                ,CONCS_PREST
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
             FROM ADES
             WHERE     IB_OGG = :ADE-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           EXEC SQL
                OPEN C-IBO-EFF-ADE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           EXEC SQL
                CLOSE C-IBO-EFF-ADE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           EXEC SQL
                FETCH C-IBO-EFF-ADE
           INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A570-CLOSE-CURSOR-IBO     THRU A570-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DCL-CUR-IBS-PREV.
           EXEC SQL
                DECLARE C-IBS-EFF-ADE-0 CURSOR FOR
              SELECT
                     ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,IB_PREV
                    ,IB_OGG
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_SCAD
                    ,ETA_A_SCAD
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,TP_RGM_FISC
                    ,TP_RIAT
                    ,TP_MOD_PAG_TIT
                    ,TP_IAS
                    ,DT_VARZ_TP_IAS
                    ,PRE_NET_IND
                    ,PRE_LRD_IND
                    ,RAT_LRD_IND
                    ,PRSTZ_INI_IND
                    ,FL_COINC_ASSTO
                    ,IB_DFLT
                    ,MOD_CALC
                    ,TP_FNT_CNBTVA
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,PC_AZ
                    ,PC_ADER
                    ,PC_TFR
                    ,PC_VOLO
                    ,DT_NOVA_RGM_FISC
                    ,FL_ATTIV
                    ,IMP_REC_RIT_VIS
                    ,IMP_REC_RIT_ACC
                    ,FL_VARZ_STAT_TBGC
                    ,FL_PROVZA_MIGRAZ
                    ,IMPB_VIS_DA_REC
                    ,DT_DECOR_PREST_BAN
                    ,DT_EFF_VARZ_STAT_T
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,CUM_CNBT_CAP
                    ,IMP_GAR_CNBT
                    ,DT_ULT_CONS_CNBT
                    ,IDEN_ISC_FND
                    ,NUM_RAT_PIAN
                    ,DT_PRESC
                    ,CONCS_PREST
              FROM ADES
              WHERE     IB_PREV = :ADE-IB-PREV
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_ADES ASC

           END-EXEC.
       A605-PREV-EX.
           EXIT.

       A605-DCL-CUR-IBS-DFLT.
           EXEC SQL
                DECLARE C-IBS-EFF-ADE-1 CURSOR FOR
              SELECT
                     ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,IB_PREV
                    ,IB_OGG
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_SCAD
                    ,ETA_A_SCAD
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,TP_RGM_FISC
                    ,TP_RIAT
                    ,TP_MOD_PAG_TIT
                    ,TP_IAS
                    ,DT_VARZ_TP_IAS
                    ,PRE_NET_IND
                    ,PRE_LRD_IND
                    ,RAT_LRD_IND
                    ,PRSTZ_INI_IND
                    ,FL_COINC_ASSTO
                    ,IB_DFLT
                    ,MOD_CALC
                    ,TP_FNT_CNBTVA
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,PC_AZ
                    ,PC_ADER
                    ,PC_TFR
                    ,PC_VOLO
                    ,DT_NOVA_RGM_FISC
                    ,FL_ATTIV
                    ,IMP_REC_RIT_VIS
                    ,IMP_REC_RIT_ACC
                    ,FL_VARZ_STAT_TBGC
                    ,FL_PROVZA_MIGRAZ
                    ,IMPB_VIS_DA_REC
                    ,DT_DECOR_PREST_BAN
                    ,DT_EFF_VARZ_STAT_T
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,CUM_CNBT_CAP
                    ,IMP_GAR_CNBT
                    ,DT_ULT_CONS_CNBT
                    ,IDEN_ISC_FND
                    ,NUM_RAT_PIAN
                    ,DT_PRESC
                    ,CONCS_PREST
              FROM ADES
              WHERE     IB_DFLT = :ADE-IB-DFLT
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_ADES ASC

           END-EXEC.
       A605-DFLT-EX.
           EXIT.


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF ADE-IB-PREV NOT = HIGH-VALUES
               PERFORM A605-DCL-CUR-IBS-PREV
                  THRU A605-PREV-EX
           ELSE
           IF ADE-IB-DFLT NOT = HIGH-VALUES
               PERFORM A605-DCL-CUR-IBS-DFLT
                  THRU A605-DFLT-EX
           END-IF
           END-IF.

       A605-EX.
           EXIT.


       A610-SELECT-IBS-PREV.
           EXEC SQL
             SELECT
                ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,IB_PREV
                ,IB_OGG
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,ETA_A_SCAD
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,TP_RGM_FISC
                ,TP_RIAT
                ,TP_MOD_PAG_TIT
                ,TP_IAS
                ,DT_VARZ_TP_IAS
                ,PRE_NET_IND
                ,PRE_LRD_IND
                ,RAT_LRD_IND
                ,PRSTZ_INI_IND
                ,FL_COINC_ASSTO
                ,IB_DFLT
                ,MOD_CALC
                ,TP_FNT_CNBTVA
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,PC_AZ
                ,PC_ADER
                ,PC_TFR
                ,PC_VOLO
                ,DT_NOVA_RGM_FISC
                ,FL_ATTIV
                ,IMP_REC_RIT_VIS
                ,IMP_REC_RIT_ACC
                ,FL_VARZ_STAT_TBGC
                ,FL_PROVZA_MIGRAZ
                ,IMPB_VIS_DA_REC
                ,DT_DECOR_PREST_BAN
                ,DT_EFF_VARZ_STAT_T
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CUM_CNBT_CAP
                ,IMP_GAR_CNBT
                ,DT_ULT_CONS_CNBT
                ,IDEN_ISC_FND
                ,NUM_RAT_PIAN
                ,DT_PRESC
                ,CONCS_PREST
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
             FROM ADES
             WHERE     IB_PREV = :ADE-IB-PREV
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.
       A610-PREV-EX.
           EXIT.

       A610-SELECT-IBS-DFLT.
           EXEC SQL
             SELECT
                ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,IB_PREV
                ,IB_OGG
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,ETA_A_SCAD
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,TP_RGM_FISC
                ,TP_RIAT
                ,TP_MOD_PAG_TIT
                ,TP_IAS
                ,DT_VARZ_TP_IAS
                ,PRE_NET_IND
                ,PRE_LRD_IND
                ,RAT_LRD_IND
                ,PRSTZ_INI_IND
                ,FL_COINC_ASSTO
                ,IB_DFLT
                ,MOD_CALC
                ,TP_FNT_CNBTVA
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,PC_AZ
                ,PC_ADER
                ,PC_TFR
                ,PC_VOLO
                ,DT_NOVA_RGM_FISC
                ,FL_ATTIV
                ,IMP_REC_RIT_VIS
                ,IMP_REC_RIT_ACC
                ,FL_VARZ_STAT_TBGC
                ,FL_PROVZA_MIGRAZ
                ,IMPB_VIS_DA_REC
                ,DT_DECOR_PREST_BAN
                ,DT_EFF_VARZ_STAT_T
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CUM_CNBT_CAP
                ,IMP_GAR_CNBT
                ,DT_ULT_CONS_CNBT
                ,IDEN_ISC_FND
                ,NUM_RAT_PIAN
                ,DT_PRESC
                ,CONCS_PREST
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
             FROM ADES
             WHERE     IB_DFLT = :ADE-IB-DFLT
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.
       A610-DFLT-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF ADE-IB-PREV NOT = HIGH-VALUES
               PERFORM A610-SELECT-IBS-PREV
                  THRU A610-PREV-EX
           ELSE
           IF ADE-IB-DFLT NOT = HIGH-VALUES
               PERFORM A610-SELECT-IBS-DFLT
                  THRU A610-DFLT-EX
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           IF ADE-IB-PREV NOT = HIGH-VALUES
              EXEC SQL
                   OPEN C-IBS-EFF-ADE-0
              END-EXEC
           ELSE
           IF ADE-IB-DFLT NOT = HIGH-VALUES
              EXEC SQL
                   OPEN C-IBS-EFF-ADE-1
              END-EXEC
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           IF ADE-IB-PREV NOT = HIGH-VALUES
              EXEC SQL
                   CLOSE C-IBS-EFF-ADE-0
              END-EXEC
           ELSE
           IF ADE-IB-DFLT NOT = HIGH-VALUES
              EXEC SQL
                   CLOSE C-IBS-EFF-ADE-1
              END-EXEC
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FN-IBS-PREV.
           EXEC SQL
                FETCH C-IBS-EFF-ADE-0
           INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
           END-EXEC.
       A690-PREV-EX.
           EXIT.

       A690-FN-IBS-DFLT.
           EXEC SQL
                FETCH C-IBS-EFF-ADE-1
           INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
           END-EXEC.
       A690-DFLT-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           IF ADE-IB-PREV NOT = HIGH-VALUES
               PERFORM A690-FN-IBS-PREV
                  THRU A690-PREV-EX
           ELSE
           IF ADE-IB-DFLT NOT = HIGH-VALUES
               PERFORM A690-FN-IBS-DFLT
                  THRU A690-DFLT-EX
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A670-CLOSE-CURSOR-IBS     THRU A670-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,IB_PREV
                ,IB_OGG
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,ETA_A_SCAD
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,TP_RGM_FISC
                ,TP_RIAT
                ,TP_MOD_PAG_TIT
                ,TP_IAS
                ,DT_VARZ_TP_IAS
                ,PRE_NET_IND
                ,PRE_LRD_IND
                ,RAT_LRD_IND
                ,PRSTZ_INI_IND
                ,FL_COINC_ASSTO
                ,IB_DFLT
                ,MOD_CALC
                ,TP_FNT_CNBTVA
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,PC_AZ
                ,PC_ADER
                ,PC_TFR
                ,PC_VOLO
                ,DT_NOVA_RGM_FISC
                ,FL_ATTIV
                ,IMP_REC_RIT_VIS
                ,IMP_REC_RIT_ACC
                ,FL_VARZ_STAT_TBGC
                ,FL_PROVZA_MIGRAZ
                ,IMPB_VIS_DA_REC
                ,DT_DECOR_PREST_BAN
                ,DT_EFF_VARZ_STAT_T
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CUM_CNBT_CAP
                ,IMP_GAR_CNBT
                ,DT_ULT_CONS_CNBT
                ,IDEN_ISC_FND
                ,NUM_RAT_PIAN
                ,DT_PRESC
                ,CONCS_PREST
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
             FROM ADES
             WHERE     ID_ADES = :ADE-ID-ADES
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-CPZ-ADE CURSOR FOR
              SELECT
                     ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,IB_PREV
                    ,IB_OGG
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_SCAD
                    ,ETA_A_SCAD
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,TP_RGM_FISC
                    ,TP_RIAT
                    ,TP_MOD_PAG_TIT
                    ,TP_IAS
                    ,DT_VARZ_TP_IAS
                    ,PRE_NET_IND
                    ,PRE_LRD_IND
                    ,RAT_LRD_IND
                    ,PRSTZ_INI_IND
                    ,FL_COINC_ASSTO
                    ,IB_DFLT
                    ,MOD_CALC
                    ,TP_FNT_CNBTVA
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,PC_AZ
                    ,PC_ADER
                    ,PC_TFR
                    ,PC_VOLO
                    ,DT_NOVA_RGM_FISC
                    ,FL_ATTIV
                    ,IMP_REC_RIT_VIS
                    ,IMP_REC_RIT_ACC
                    ,FL_VARZ_STAT_TBGC
                    ,FL_PROVZA_MIGRAZ
                    ,IMPB_VIS_DA_REC
                    ,DT_DECOR_PREST_BAN
                    ,DT_EFF_VARZ_STAT_T
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,CUM_CNBT_CAP
                    ,IMP_GAR_CNBT
                    ,DT_ULT_CONS_CNBT
                    ,IDEN_ISC_FND
                    ,NUM_RAT_PIAN
                    ,DT_PRESC
                    ,CONCS_PREST
              FROM ADES
              WHERE     ID_POLI = :ADE-ID-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_ADES ASC

           END-EXEC.

       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,IB_PREV
                ,IB_OGG
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,ETA_A_SCAD
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,TP_RGM_FISC
                ,TP_RIAT
                ,TP_MOD_PAG_TIT
                ,TP_IAS
                ,DT_VARZ_TP_IAS
                ,PRE_NET_IND
                ,PRE_LRD_IND
                ,RAT_LRD_IND
                ,PRSTZ_INI_IND
                ,FL_COINC_ASSTO
                ,IB_DFLT
                ,MOD_CALC
                ,TP_FNT_CNBTVA
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,PC_AZ
                ,PC_ADER
                ,PC_TFR
                ,PC_VOLO
                ,DT_NOVA_RGM_FISC
                ,FL_ATTIV
                ,IMP_REC_RIT_VIS
                ,IMP_REC_RIT_ACC
                ,FL_VARZ_STAT_TBGC
                ,FL_PROVZA_MIGRAZ
                ,IMPB_VIS_DA_REC
                ,DT_DECOR_PREST_BAN
                ,DT_EFF_VARZ_STAT_T
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CUM_CNBT_CAP
                ,IMP_GAR_CNBT
                ,DT_ULT_CONS_CNBT
                ,IDEN_ISC_FND
                ,NUM_RAT_PIAN
                ,DT_PRESC
                ,CONCS_PREST
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
             FROM ADES
             WHERE     ID_POLI = :ADE-ID-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           EXEC SQL
                OPEN C-IDP-CPZ-ADE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           EXEC SQL
                CLOSE C-IDP-CPZ-ADE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           EXEC SQL
                FETCH C-IDP-CPZ-ADE
           INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IBO-CPZ-ADE CURSOR FOR
              SELECT
                     ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,IB_PREV
                    ,IB_OGG
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_SCAD
                    ,ETA_A_SCAD
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,TP_RGM_FISC
                    ,TP_RIAT
                    ,TP_MOD_PAG_TIT
                    ,TP_IAS
                    ,DT_VARZ_TP_IAS
                    ,PRE_NET_IND
                    ,PRE_LRD_IND
                    ,RAT_LRD_IND
                    ,PRSTZ_INI_IND
                    ,FL_COINC_ASSTO
                    ,IB_DFLT
                    ,MOD_CALC
                    ,TP_FNT_CNBTVA
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,PC_AZ
                    ,PC_ADER
                    ,PC_TFR
                    ,PC_VOLO
                    ,DT_NOVA_RGM_FISC
                    ,FL_ATTIV
                    ,IMP_REC_RIT_VIS
                    ,IMP_REC_RIT_ACC
                    ,FL_VARZ_STAT_TBGC
                    ,FL_PROVZA_MIGRAZ
                    ,IMPB_VIS_DA_REC
                    ,DT_DECOR_PREST_BAN
                    ,DT_EFF_VARZ_STAT_T
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,CUM_CNBT_CAP
                    ,IMP_GAR_CNBT
                    ,DT_ULT_CONS_CNBT
                    ,IDEN_ISC_FND
                    ,NUM_RAT_PIAN
                    ,DT_PRESC
                    ,CONCS_PREST
              FROM ADES
              WHERE     IB_OGG = :ADE-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_ADES ASC

           END-EXEC.

       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,IB_PREV
                ,IB_OGG
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,ETA_A_SCAD
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,TP_RGM_FISC
                ,TP_RIAT
                ,TP_MOD_PAG_TIT
                ,TP_IAS
                ,DT_VARZ_TP_IAS
                ,PRE_NET_IND
                ,PRE_LRD_IND
                ,RAT_LRD_IND
                ,PRSTZ_INI_IND
                ,FL_COINC_ASSTO
                ,IB_DFLT
                ,MOD_CALC
                ,TP_FNT_CNBTVA
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,PC_AZ
                ,PC_ADER
                ,PC_TFR
                ,PC_VOLO
                ,DT_NOVA_RGM_FISC
                ,FL_ATTIV
                ,IMP_REC_RIT_VIS
                ,IMP_REC_RIT_ACC
                ,FL_VARZ_STAT_TBGC
                ,FL_PROVZA_MIGRAZ
                ,IMPB_VIS_DA_REC
                ,DT_DECOR_PREST_BAN
                ,DT_EFF_VARZ_STAT_T
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CUM_CNBT_CAP
                ,IMP_GAR_CNBT
                ,DT_ULT_CONS_CNBT
                ,IDEN_ISC_FND
                ,NUM_RAT_PIAN
                ,DT_PRESC
                ,CONCS_PREST
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
             FROM ADES
             WHERE     IB_OGG = :ADE-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           EXEC SQL
                OPEN C-IBO-CPZ-ADE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           EXEC SQL
                CLOSE C-IBO-CPZ-ADE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           EXEC SQL
                FETCH C-IBO-CPZ-ADE
           INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B570-CLOSE-CURSOR-IBO-CPZ     THRU B570-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DCL-CUR-IBS-PREV.
           EXEC SQL
                DECLARE C-IBS-CPZ-ADE-0 CURSOR FOR
              SELECT
                     ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,IB_PREV
                    ,IB_OGG
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_SCAD
                    ,ETA_A_SCAD
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,TP_RGM_FISC
                    ,TP_RIAT
                    ,TP_MOD_PAG_TIT
                    ,TP_IAS
                    ,DT_VARZ_TP_IAS
                    ,PRE_NET_IND
                    ,PRE_LRD_IND
                    ,RAT_LRD_IND
                    ,PRSTZ_INI_IND
                    ,FL_COINC_ASSTO
                    ,IB_DFLT
                    ,MOD_CALC
                    ,TP_FNT_CNBTVA
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,PC_AZ
                    ,PC_ADER
                    ,PC_TFR
                    ,PC_VOLO
                    ,DT_NOVA_RGM_FISC
                    ,FL_ATTIV
                    ,IMP_REC_RIT_VIS
                    ,IMP_REC_RIT_ACC
                    ,FL_VARZ_STAT_TBGC
                    ,FL_PROVZA_MIGRAZ
                    ,IMPB_VIS_DA_REC
                    ,DT_DECOR_PREST_BAN
                    ,DT_EFF_VARZ_STAT_T
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,CUM_CNBT_CAP
                    ,IMP_GAR_CNBT
                    ,DT_ULT_CONS_CNBT
                    ,IDEN_ISC_FND
                    ,NUM_RAT_PIAN
                    ,DT_PRESC
                    ,CONCS_PREST
              FROM ADES
              WHERE     IB_PREV = :ADE-IB-PREV
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_ADES ASC

           END-EXEC.
       B605-PREV-EX.
           EXIT.

       B605-DCL-CUR-IBS-DFLT.
           EXEC SQL
                DECLARE C-IBS-CPZ-ADE-1 CURSOR FOR
              SELECT
                     ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,IB_PREV
                    ,IB_OGG
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_SCAD
                    ,ETA_A_SCAD
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,TP_RGM_FISC
                    ,TP_RIAT
                    ,TP_MOD_PAG_TIT
                    ,TP_IAS
                    ,DT_VARZ_TP_IAS
                    ,PRE_NET_IND
                    ,PRE_LRD_IND
                    ,RAT_LRD_IND
                    ,PRSTZ_INI_IND
                    ,FL_COINC_ASSTO
                    ,IB_DFLT
                    ,MOD_CALC
                    ,TP_FNT_CNBTVA
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,PC_AZ
                    ,PC_ADER
                    ,PC_TFR
                    ,PC_VOLO
                    ,DT_NOVA_RGM_FISC
                    ,FL_ATTIV
                    ,IMP_REC_RIT_VIS
                    ,IMP_REC_RIT_ACC
                    ,FL_VARZ_STAT_TBGC
                    ,FL_PROVZA_MIGRAZ
                    ,IMPB_VIS_DA_REC
                    ,DT_DECOR_PREST_BAN
                    ,DT_EFF_VARZ_STAT_T
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,CUM_CNBT_CAP
                    ,IMP_GAR_CNBT
                    ,DT_ULT_CONS_CNBT
                    ,IDEN_ISC_FND
                    ,NUM_RAT_PIAN
                    ,DT_PRESC
                    ,CONCS_PREST
              FROM ADES
              WHERE     IB_DFLT = :ADE-IB-DFLT
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_ADES ASC

           END-EXEC.
       B605-DFLT-EX.
           EXIT.


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF ADE-IB-PREV NOT = HIGH-VALUES
               PERFORM B605-DCL-CUR-IBS-PREV
                  THRU B605-PREV-EX
           ELSE
           IF ADE-IB-DFLT NOT = HIGH-VALUES
               PERFORM B605-DCL-CUR-IBS-DFLT
                  THRU B605-DFLT-EX
           END-IF
           END-IF.

       B605-EX.
           EXIT.


       B610-SELECT-IBS-PREV.
           EXEC SQL
             SELECT
                ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,IB_PREV
                ,IB_OGG
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,ETA_A_SCAD
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,TP_RGM_FISC
                ,TP_RIAT
                ,TP_MOD_PAG_TIT
                ,TP_IAS
                ,DT_VARZ_TP_IAS
                ,PRE_NET_IND
                ,PRE_LRD_IND
                ,RAT_LRD_IND
                ,PRSTZ_INI_IND
                ,FL_COINC_ASSTO
                ,IB_DFLT
                ,MOD_CALC
                ,TP_FNT_CNBTVA
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,PC_AZ
                ,PC_ADER
                ,PC_TFR
                ,PC_VOLO
                ,DT_NOVA_RGM_FISC
                ,FL_ATTIV
                ,IMP_REC_RIT_VIS
                ,IMP_REC_RIT_ACC
                ,FL_VARZ_STAT_TBGC
                ,FL_PROVZA_MIGRAZ
                ,IMPB_VIS_DA_REC
                ,DT_DECOR_PREST_BAN
                ,DT_EFF_VARZ_STAT_T
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CUM_CNBT_CAP
                ,IMP_GAR_CNBT
                ,DT_ULT_CONS_CNBT
                ,IDEN_ISC_FND
                ,NUM_RAT_PIAN
                ,DT_PRESC
                ,CONCS_PREST
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
             FROM ADES
             WHERE     IB_PREV = :ADE-IB-PREV
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.
       B610-PREV-EX.
           EXIT.

       B610-SELECT-IBS-DFLT.
           EXEC SQL
             SELECT
                ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,IB_PREV
                ,IB_OGG
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,ETA_A_SCAD
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,TP_RGM_FISC
                ,TP_RIAT
                ,TP_MOD_PAG_TIT
                ,TP_IAS
                ,DT_VARZ_TP_IAS
                ,PRE_NET_IND
                ,PRE_LRD_IND
                ,RAT_LRD_IND
                ,PRSTZ_INI_IND
                ,FL_COINC_ASSTO
                ,IB_DFLT
                ,MOD_CALC
                ,TP_FNT_CNBTVA
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,PC_AZ
                ,PC_ADER
                ,PC_TFR
                ,PC_VOLO
                ,DT_NOVA_RGM_FISC
                ,FL_ATTIV
                ,IMP_REC_RIT_VIS
                ,IMP_REC_RIT_ACC
                ,FL_VARZ_STAT_TBGC
                ,FL_PROVZA_MIGRAZ
                ,IMPB_VIS_DA_REC
                ,DT_DECOR_PREST_BAN
                ,DT_EFF_VARZ_STAT_T
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,CUM_CNBT_CAP
                ,IMP_GAR_CNBT
                ,DT_ULT_CONS_CNBT
                ,IDEN_ISC_FND
                ,NUM_RAT_PIAN
                ,DT_PRESC
                ,CONCS_PREST
             INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
             FROM ADES
             WHERE     IB_DFLT = :ADE-IB-DFLT
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.
       B610-DFLT-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           IF ADE-IB-PREV NOT = HIGH-VALUES
               PERFORM B610-SELECT-IBS-PREV
                  THRU B610-PREV-EX
           ELSE
           IF ADE-IB-DFLT NOT = HIGH-VALUES
               PERFORM B610-SELECT-IBS-DFLT
                  THRU B610-DFLT-EX
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           IF ADE-IB-PREV NOT = HIGH-VALUES
              EXEC SQL
                   OPEN C-IBS-CPZ-ADE-0
              END-EXEC
           ELSE
           IF ADE-IB-DFLT NOT = HIGH-VALUES
              EXEC SQL
                   OPEN C-IBS-CPZ-ADE-1
              END-EXEC
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           IF ADE-IB-PREV NOT = HIGH-VALUES
              EXEC SQL
                   CLOSE C-IBS-CPZ-ADE-0
              END-EXEC
           ELSE
           IF ADE-IB-DFLT NOT = HIGH-VALUES
              EXEC SQL
                   CLOSE C-IBS-CPZ-ADE-1
              END-EXEC
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FN-IBS-PREV.
           EXEC SQL
                FETCH C-IBS-CPZ-ADE-0
           INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
           END-EXEC.
       B690-PREV-EX.
           EXIT.

       B690-FN-IBS-DFLT.
           EXEC SQL
                FETCH C-IBS-CPZ-ADE-1
           INTO
                :ADE-ID-ADES
               ,:ADE-ID-POLI
               ,:ADE-ID-MOVI-CRZ
               ,:ADE-ID-MOVI-CHIU
                :IND-ADE-ID-MOVI-CHIU
               ,:ADE-DT-INI-EFF-DB
               ,:ADE-DT-END-EFF-DB
               ,:ADE-IB-PREV
                :IND-ADE-IB-PREV
               ,:ADE-IB-OGG
                :IND-ADE-IB-OGG
               ,:ADE-COD-COMP-ANIA
               ,:ADE-DT-DECOR-DB
                :IND-ADE-DT-DECOR
               ,:ADE-DT-SCAD-DB
                :IND-ADE-DT-SCAD
               ,:ADE-ETA-A-SCAD
                :IND-ADE-ETA-A-SCAD
               ,:ADE-DUR-AA
                :IND-ADE-DUR-AA
               ,:ADE-DUR-MM
                :IND-ADE-DUR-MM
               ,:ADE-DUR-GG
                :IND-ADE-DUR-GG
               ,:ADE-TP-RGM-FISC
               ,:ADE-TP-RIAT
                :IND-ADE-TP-RIAT
               ,:ADE-TP-MOD-PAG-TIT
               ,:ADE-TP-IAS
                :IND-ADE-TP-IAS
               ,:ADE-DT-VARZ-TP-IAS-DB
                :IND-ADE-DT-VARZ-TP-IAS
               ,:ADE-PRE-NET-IND
                :IND-ADE-PRE-NET-IND
               ,:ADE-PRE-LRD-IND
                :IND-ADE-PRE-LRD-IND
               ,:ADE-RAT-LRD-IND
                :IND-ADE-RAT-LRD-IND
               ,:ADE-PRSTZ-INI-IND
                :IND-ADE-PRSTZ-INI-IND
               ,:ADE-FL-COINC-ASSTO
                :IND-ADE-FL-COINC-ASSTO
               ,:ADE-IB-DFLT
                :IND-ADE-IB-DFLT
               ,:ADE-MOD-CALC
                :IND-ADE-MOD-CALC
               ,:ADE-TP-FNT-CNBTVA
                :IND-ADE-TP-FNT-CNBTVA
               ,:ADE-IMP-AZ
                :IND-ADE-IMP-AZ
               ,:ADE-IMP-ADER
                :IND-ADE-IMP-ADER
               ,:ADE-IMP-TFR
                :IND-ADE-IMP-TFR
               ,:ADE-IMP-VOLO
                :IND-ADE-IMP-VOLO
               ,:ADE-PC-AZ
                :IND-ADE-PC-AZ
               ,:ADE-PC-ADER
                :IND-ADE-PC-ADER
               ,:ADE-PC-TFR
                :IND-ADE-PC-TFR
               ,:ADE-PC-VOLO
                :IND-ADE-PC-VOLO
               ,:ADE-DT-NOVA-RGM-FISC-DB
                :IND-ADE-DT-NOVA-RGM-FISC
               ,:ADE-FL-ATTIV
                :IND-ADE-FL-ATTIV
               ,:ADE-IMP-REC-RIT-VIS
                :IND-ADE-IMP-REC-RIT-VIS
               ,:ADE-IMP-REC-RIT-ACC
                :IND-ADE-IMP-REC-RIT-ACC
               ,:ADE-FL-VARZ-STAT-TBGC
                :IND-ADE-FL-VARZ-STAT-TBGC
               ,:ADE-FL-PROVZA-MIGRAZ
                :IND-ADE-FL-PROVZA-MIGRAZ
               ,:ADE-IMPB-VIS-DA-REC
                :IND-ADE-IMPB-VIS-DA-REC
               ,:ADE-DT-DECOR-PREST-BAN-DB
                :IND-ADE-DT-DECOR-PREST-BAN
               ,:ADE-DT-EFF-VARZ-STAT-T-DB
                :IND-ADE-DT-EFF-VARZ-STAT-T
               ,:ADE-DS-RIGA
               ,:ADE-DS-OPER-SQL
               ,:ADE-DS-VER
               ,:ADE-DS-TS-INI-CPTZ
               ,:ADE-DS-TS-END-CPTZ
               ,:ADE-DS-UTENTE
               ,:ADE-DS-STATO-ELAB
               ,:ADE-CUM-CNBT-CAP
                :IND-ADE-CUM-CNBT-CAP
               ,:ADE-IMP-GAR-CNBT
                :IND-ADE-IMP-GAR-CNBT
               ,:ADE-DT-ULT-CONS-CNBT-DB
                :IND-ADE-DT-ULT-CONS-CNBT
               ,:ADE-IDEN-ISC-FND
                :IND-ADE-IDEN-ISC-FND
               ,:ADE-NUM-RAT-PIAN
                :IND-ADE-NUM-RAT-PIAN
               ,:ADE-DT-PRESC-DB
                :IND-ADE-DT-PRESC
               ,:ADE-CONCS-PREST
                :IND-ADE-CONCS-PREST
           END-EXEC.
       B690-DFLT-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           IF ADE-IB-PREV NOT = HIGH-VALUES
               PERFORM B690-FN-IBS-PREV
                  THRU B690-PREV-EX
           ELSE
           IF ADE-IB-DFLT NOT = HIGH-VALUES
               PERFORM B690-FN-IBS-DFLT
                  THRU B690-DFLT-EX
           END-IF
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B670-CLOSE-CURSOR-IBS-CPZ     THRU B670-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-ADE-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO ADE-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-ADE-IB-PREV = -1
              MOVE HIGH-VALUES TO ADE-IB-PREV-NULL
           END-IF
           IF IND-ADE-IB-OGG = -1
              MOVE HIGH-VALUES TO ADE-IB-OGG-NULL
           END-IF
           IF IND-ADE-DT-DECOR = -1
              MOVE HIGH-VALUES TO ADE-DT-DECOR-NULL
           END-IF
           IF IND-ADE-DT-SCAD = -1
              MOVE HIGH-VALUES TO ADE-DT-SCAD-NULL
           END-IF
           IF IND-ADE-ETA-A-SCAD = -1
              MOVE HIGH-VALUES TO ADE-ETA-A-SCAD-NULL
           END-IF
           IF IND-ADE-DUR-AA = -1
              MOVE HIGH-VALUES TO ADE-DUR-AA-NULL
           END-IF
           IF IND-ADE-DUR-MM = -1
              MOVE HIGH-VALUES TO ADE-DUR-MM-NULL
           END-IF
           IF IND-ADE-DUR-GG = -1
              MOVE HIGH-VALUES TO ADE-DUR-GG-NULL
           END-IF
           IF IND-ADE-TP-RIAT = -1
              MOVE HIGH-VALUES TO ADE-TP-RIAT-NULL
           END-IF
           IF IND-ADE-TP-IAS = -1
              MOVE HIGH-VALUES TO ADE-TP-IAS-NULL
           END-IF
           IF IND-ADE-DT-VARZ-TP-IAS = -1
              MOVE HIGH-VALUES TO ADE-DT-VARZ-TP-IAS-NULL
           END-IF
           IF IND-ADE-PRE-NET-IND = -1
              MOVE HIGH-VALUES TO ADE-PRE-NET-IND-NULL
           END-IF
           IF IND-ADE-PRE-LRD-IND = -1
              MOVE HIGH-VALUES TO ADE-PRE-LRD-IND-NULL
           END-IF
           IF IND-ADE-RAT-LRD-IND = -1
              MOVE HIGH-VALUES TO ADE-RAT-LRD-IND-NULL
           END-IF
           IF IND-ADE-PRSTZ-INI-IND = -1
              MOVE HIGH-VALUES TO ADE-PRSTZ-INI-IND-NULL
           END-IF
           IF IND-ADE-FL-COINC-ASSTO = -1
              MOVE HIGH-VALUES TO ADE-FL-COINC-ASSTO-NULL
           END-IF
           IF IND-ADE-IB-DFLT = -1
              MOVE HIGH-VALUES TO ADE-IB-DFLT-NULL
           END-IF
           IF IND-ADE-MOD-CALC = -1
              MOVE HIGH-VALUES TO ADE-MOD-CALC-NULL
           END-IF
           IF IND-ADE-TP-FNT-CNBTVA = -1
              MOVE HIGH-VALUES TO ADE-TP-FNT-CNBTVA-NULL
           END-IF
           IF IND-ADE-IMP-AZ = -1
              MOVE HIGH-VALUES TO ADE-IMP-AZ-NULL
           END-IF
           IF IND-ADE-IMP-ADER = -1
              MOVE HIGH-VALUES TO ADE-IMP-ADER-NULL
           END-IF
           IF IND-ADE-IMP-TFR = -1
              MOVE HIGH-VALUES TO ADE-IMP-TFR-NULL
           END-IF
           IF IND-ADE-IMP-VOLO = -1
              MOVE HIGH-VALUES TO ADE-IMP-VOLO-NULL
           END-IF
           IF IND-ADE-PC-AZ = -1
              MOVE HIGH-VALUES TO ADE-PC-AZ-NULL
           END-IF
           IF IND-ADE-PC-ADER = -1
              MOVE HIGH-VALUES TO ADE-PC-ADER-NULL
           END-IF
           IF IND-ADE-PC-TFR = -1
              MOVE HIGH-VALUES TO ADE-PC-TFR-NULL
           END-IF
           IF IND-ADE-PC-VOLO = -1
              MOVE HIGH-VALUES TO ADE-PC-VOLO-NULL
           END-IF
           IF IND-ADE-DT-NOVA-RGM-FISC = -1
              MOVE HIGH-VALUES TO ADE-DT-NOVA-RGM-FISC-NULL
           END-IF
           IF IND-ADE-FL-ATTIV = -1
              MOVE HIGH-VALUES TO ADE-FL-ATTIV-NULL
           END-IF
           IF IND-ADE-IMP-REC-RIT-VIS = -1
              MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-VIS-NULL
           END-IF
           IF IND-ADE-IMP-REC-RIT-ACC = -1
              MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-ACC-NULL
           END-IF
           IF IND-ADE-FL-VARZ-STAT-TBGC = -1
              MOVE HIGH-VALUES TO ADE-FL-VARZ-STAT-TBGC-NULL
           END-IF
           IF IND-ADE-FL-PROVZA-MIGRAZ = -1
              MOVE HIGH-VALUES TO ADE-FL-PROVZA-MIGRAZ-NULL
           END-IF
           IF IND-ADE-IMPB-VIS-DA-REC = -1
              MOVE HIGH-VALUES TO ADE-IMPB-VIS-DA-REC-NULL
           END-IF
           IF IND-ADE-DT-DECOR-PREST-BAN = -1
              MOVE HIGH-VALUES TO ADE-DT-DECOR-PREST-BAN-NULL
           END-IF
           IF IND-ADE-DT-EFF-VARZ-STAT-T = -1
              MOVE HIGH-VALUES TO ADE-DT-EFF-VARZ-STAT-T-NULL
           END-IF
           IF IND-ADE-CUM-CNBT-CAP = -1
              MOVE HIGH-VALUES TO ADE-CUM-CNBT-CAP-NULL
           END-IF
           IF IND-ADE-IMP-GAR-CNBT = -1
              MOVE HIGH-VALUES TO ADE-IMP-GAR-CNBT-NULL
           END-IF
           IF IND-ADE-DT-ULT-CONS-CNBT = -1
              MOVE HIGH-VALUES TO ADE-DT-ULT-CONS-CNBT-NULL
           END-IF
           IF IND-ADE-IDEN-ISC-FND = -1
              MOVE HIGH-VALUES TO ADE-IDEN-ISC-FND-NULL
           END-IF
           IF IND-ADE-NUM-RAT-PIAN = -1
              MOVE HIGH-VALUES TO ADE-NUM-RAT-PIAN-NULL
           END-IF
           IF IND-ADE-DT-PRESC = -1
              MOVE HIGH-VALUES TO ADE-DT-PRESC-NULL
           END-IF
           IF IND-ADE-CONCS-PREST = -1
              MOVE HIGH-VALUES TO ADE-CONCS-PREST-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO ADE-DS-OPER-SQL
           MOVE 0                   TO ADE-DS-VER
           MOVE IDSV0003-USER-NAME TO ADE-DS-UTENTE
           MOVE '1'                   TO ADE-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO ADE-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO ADE-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF ADE-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-ADE-ID-MOVI-CHIU
           END-IF
           IF ADE-IB-PREV-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IB-PREV
           ELSE
              MOVE 0 TO IND-ADE-IB-PREV
           END-IF
           IF ADE-IB-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IB-OGG
           ELSE
              MOVE 0 TO IND-ADE-IB-OGG
           END-IF
           IF ADE-DT-DECOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-DECOR
           ELSE
              MOVE 0 TO IND-ADE-DT-DECOR
           END-IF
           IF ADE-DT-SCAD-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-SCAD
           ELSE
              MOVE 0 TO IND-ADE-DT-SCAD
           END-IF
           IF ADE-ETA-A-SCAD-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-ETA-A-SCAD
           ELSE
              MOVE 0 TO IND-ADE-ETA-A-SCAD
           END-IF
           IF ADE-DUR-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DUR-AA
           ELSE
              MOVE 0 TO IND-ADE-DUR-AA
           END-IF
           IF ADE-DUR-MM-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DUR-MM
           ELSE
              MOVE 0 TO IND-ADE-DUR-MM
           END-IF
           IF ADE-DUR-GG-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DUR-GG
           ELSE
              MOVE 0 TO IND-ADE-DUR-GG
           END-IF
           IF ADE-TP-RIAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-TP-RIAT
           ELSE
              MOVE 0 TO IND-ADE-TP-RIAT
           END-IF
           IF ADE-TP-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-TP-IAS
           ELSE
              MOVE 0 TO IND-ADE-TP-IAS
           END-IF
           IF ADE-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-VARZ-TP-IAS
           ELSE
              MOVE 0 TO IND-ADE-DT-VARZ-TP-IAS
           END-IF
           IF ADE-PRE-NET-IND-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-PRE-NET-IND
           ELSE
              MOVE 0 TO IND-ADE-PRE-NET-IND
           END-IF
           IF ADE-PRE-LRD-IND-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-PRE-LRD-IND
           ELSE
              MOVE 0 TO IND-ADE-PRE-LRD-IND
           END-IF
           IF ADE-RAT-LRD-IND-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-RAT-LRD-IND
           ELSE
              MOVE 0 TO IND-ADE-RAT-LRD-IND
           END-IF
           IF ADE-PRSTZ-INI-IND-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-PRSTZ-INI-IND
           ELSE
              MOVE 0 TO IND-ADE-PRSTZ-INI-IND
           END-IF
           IF ADE-FL-COINC-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-FL-COINC-ASSTO
           ELSE
              MOVE 0 TO IND-ADE-FL-COINC-ASSTO
           END-IF
           IF ADE-IB-DFLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IB-DFLT
           ELSE
              MOVE 0 TO IND-ADE-IB-DFLT
           END-IF
           IF ADE-MOD-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-MOD-CALC
           ELSE
              MOVE 0 TO IND-ADE-MOD-CALC
           END-IF
           IF ADE-TP-FNT-CNBTVA-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-TP-FNT-CNBTVA
           ELSE
              MOVE 0 TO IND-ADE-TP-FNT-CNBTVA
           END-IF
           IF ADE-IMP-AZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMP-AZ
           ELSE
              MOVE 0 TO IND-ADE-IMP-AZ
           END-IF
           IF ADE-IMP-ADER-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMP-ADER
           ELSE
              MOVE 0 TO IND-ADE-IMP-ADER
           END-IF
           IF ADE-IMP-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMP-TFR
           ELSE
              MOVE 0 TO IND-ADE-IMP-TFR
           END-IF
           IF ADE-IMP-VOLO-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMP-VOLO
           ELSE
              MOVE 0 TO IND-ADE-IMP-VOLO
           END-IF
           IF ADE-PC-AZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-PC-AZ
           ELSE
              MOVE 0 TO IND-ADE-PC-AZ
           END-IF
           IF ADE-PC-ADER-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-PC-ADER
           ELSE
              MOVE 0 TO IND-ADE-PC-ADER
           END-IF
           IF ADE-PC-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-PC-TFR
           ELSE
              MOVE 0 TO IND-ADE-PC-TFR
           END-IF
           IF ADE-PC-VOLO-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-PC-VOLO
           ELSE
              MOVE 0 TO IND-ADE-PC-VOLO
           END-IF
           IF ADE-DT-NOVA-RGM-FISC-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-NOVA-RGM-FISC
           ELSE
              MOVE 0 TO IND-ADE-DT-NOVA-RGM-FISC
           END-IF
           IF ADE-FL-ATTIV-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-FL-ATTIV
           ELSE
              MOVE 0 TO IND-ADE-FL-ATTIV
           END-IF
           IF ADE-IMP-REC-RIT-VIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMP-REC-RIT-VIS
           ELSE
              MOVE 0 TO IND-ADE-IMP-REC-RIT-VIS
           END-IF
           IF ADE-IMP-REC-RIT-ACC-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMP-REC-RIT-ACC
           ELSE
              MOVE 0 TO IND-ADE-IMP-REC-RIT-ACC
           END-IF
           IF ADE-FL-VARZ-STAT-TBGC-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-FL-VARZ-STAT-TBGC
           ELSE
              MOVE 0 TO IND-ADE-FL-VARZ-STAT-TBGC
           END-IF
           IF ADE-FL-PROVZA-MIGRAZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-FL-PROVZA-MIGRAZ
           ELSE
              MOVE 0 TO IND-ADE-FL-PROVZA-MIGRAZ
           END-IF
           IF ADE-IMPB-VIS-DA-REC-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMPB-VIS-DA-REC
           ELSE
              MOVE 0 TO IND-ADE-IMPB-VIS-DA-REC
           END-IF
           IF ADE-DT-DECOR-PREST-BAN-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-DECOR-PREST-BAN
           ELSE
              MOVE 0 TO IND-ADE-DT-DECOR-PREST-BAN
           END-IF
           IF ADE-DT-EFF-VARZ-STAT-T-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-EFF-VARZ-STAT-T
           ELSE
              MOVE 0 TO IND-ADE-DT-EFF-VARZ-STAT-T
           END-IF
           IF ADE-CUM-CNBT-CAP-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-CUM-CNBT-CAP
           ELSE
              MOVE 0 TO IND-ADE-CUM-CNBT-CAP
           END-IF
           IF ADE-IMP-GAR-CNBT-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IMP-GAR-CNBT
           ELSE
              MOVE 0 TO IND-ADE-IMP-GAR-CNBT
           END-IF
           IF ADE-DT-ULT-CONS-CNBT-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-ULT-CONS-CNBT
           ELSE
              MOVE 0 TO IND-ADE-DT-ULT-CONS-CNBT
           END-IF
           IF ADE-IDEN-ISC-FND-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-IDEN-ISC-FND
           ELSE
              MOVE 0 TO IND-ADE-IDEN-ISC-FND
           END-IF
           IF ADE-NUM-RAT-PIAN-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-NUM-RAT-PIAN
           ELSE
              MOVE 0 TO IND-ADE-NUM-RAT-PIAN
           END-IF
           IF ADE-DT-PRESC-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-DT-PRESC
           ELSE
              MOVE 0 TO IND-ADE-DT-PRESC
           END-IF
           IF ADE-CONCS-PREST-NULL = HIGH-VALUES
              MOVE -1 TO IND-ADE-CONCS-PREST
           ELSE
              MOVE 0 TO IND-ADE-CONCS-PREST
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : ADE-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE ADES TO WS-BUFFER-TABLE.

           MOVE ADE-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO ADE-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO ADE-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO ADE-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO ADE-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO ADE-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO ADE-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO ADE-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE ADES TO WS-BUFFER-TABLE.

           MOVE ADE-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO ADES.

           MOVE WS-ID-MOVI-CRZ  TO ADE-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO ADE-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO ADE-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO ADE-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO ADE-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO ADE-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO ADE-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE ADE-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO ADE-DT-INI-EFF-DB
           MOVE ADE-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO ADE-DT-END-EFF-DB
           IF IND-ADE-DT-DECOR = 0
               MOVE ADE-DT-DECOR TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-DECOR-DB
           END-IF
           IF IND-ADE-DT-SCAD = 0
               MOVE ADE-DT-SCAD TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-SCAD-DB
           END-IF
           IF IND-ADE-DT-VARZ-TP-IAS = 0
               MOVE ADE-DT-VARZ-TP-IAS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-VARZ-TP-IAS-DB
           END-IF
           IF IND-ADE-DT-NOVA-RGM-FISC = 0
               MOVE ADE-DT-NOVA-RGM-FISC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-NOVA-RGM-FISC-DB
           END-IF
           IF IND-ADE-DT-DECOR-PREST-BAN = 0
               MOVE ADE-DT-DECOR-PREST-BAN TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-DECOR-PREST-BAN-DB
           END-IF
           IF IND-ADE-DT-EFF-VARZ-STAT-T = 0
               MOVE ADE-DT-EFF-VARZ-STAT-T TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-EFF-VARZ-STAT-T-DB
           END-IF
           IF IND-ADE-DT-ULT-CONS-CNBT = 0
               MOVE ADE-DT-ULT-CONS-CNBT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-ULT-CONS-CNBT-DB
           END-IF
           IF IND-ADE-DT-PRESC = 0
               MOVE ADE-DT-PRESC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO ADE-DT-PRESC-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE ADE-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO ADE-DT-INI-EFF
           MOVE ADE-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO ADE-DT-END-EFF
           IF IND-ADE-DT-DECOR = 0
               MOVE ADE-DT-DECOR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-DECOR
           END-IF
           IF IND-ADE-DT-SCAD = 0
               MOVE ADE-DT-SCAD-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-SCAD
           END-IF
           IF IND-ADE-DT-VARZ-TP-IAS = 0
               MOVE ADE-DT-VARZ-TP-IAS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-VARZ-TP-IAS
           END-IF
           IF IND-ADE-DT-NOVA-RGM-FISC = 0
               MOVE ADE-DT-NOVA-RGM-FISC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-NOVA-RGM-FISC
           END-IF
           IF IND-ADE-DT-DECOR-PREST-BAN = 0
               MOVE ADE-DT-DECOR-PREST-BAN-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-DECOR-PREST-BAN
           END-IF
           IF IND-ADE-DT-EFF-VARZ-STAT-T = 0
               MOVE ADE-DT-EFF-VARZ-STAT-T-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-EFF-VARZ-STAT-T
           END-IF
           IF IND-ADE-DT-ULT-CONS-CNBT = 0
               MOVE ADE-DT-ULT-CONS-CNBT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-ULT-CONS-CNBT
           END-IF
           IF IND-ADE-DT-PRESC = 0
               MOVE ADE-DT-PRESC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO ADE-DT-PRESC
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

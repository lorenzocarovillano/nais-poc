      ******************************************************************
      **                                                        ********
      **    PORTAFOGLIO VITA ITALIA  VER 1.0                    ********
      **                                                        ********
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         ISPS0211.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2009.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *    PROGRAMMA ..... ISPS0211
      *    TIPOLOGIA...... ACTUATOR
      *    PROCESSO....... XXX
      *    FUNZIONE....... XXX
      *    DESCRIZIONE.... SERVIZIO CALCOLI NOTEVOLI E LIQUIDAZIONE
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    VARIABILI DI WORKING
      *----------------------------------------------------------------*
       01  WK-PGM                         PIC X(008) VALUE 'ISPS0211'.

      *----------------------------------------------------------------*
      *    PUNTATORE AREA PRODOTTO
      *----------------------------------------------------------------*
       01  MQ01-ADDRESS.
           05 WS-ADDRESS USAGE POINTER.

      *----------------------------------------------------------------*
      *    DEFINIZIONE DI INDICI                                       *
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-TAB-ERR                  PIC S9(04) COMP VALUE 0.

      *----------------------------------------------------------------*
      * CHIAMATA AL SERVIZIO INFRASTRUTTURALE PER COLLEGAMENTO AL
      * SERVIZI DI PRODOTTO
      *----------------------------------------------------------------*
           COPY IJCCMQ03.

      *----------------------------------------------------------------*
      *    COPY VARIBILI PER LA GESTIONE ERRORI                        *
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
           COPY IEAV9903.

      *----------------------------------------------------------------*
      * MODULI CHIAMATI                                                *
      *----------------------------------------------------------------*
       01  INTERF-MQSERIES                    PIC X(8) VALUE 'IJCSMQ03'.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.

       01  WCOM-AREA-STATI.
           COPY LCCC0001             REPLACING ==(SF)== BY ==WCOM==.

       01  AREA-IO-ISPS0211.
           COPY ISPC0211.

      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                AREA-IO-ISPS0211.
      *---------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      * OPERAZIONI INIZIALI                                            *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           CONTINUE.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *  ELABORAZIONE                                                  *
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> VALORIZZA AREA SERVIZIO ISPS0211
           PERFORM S1050-PREP-AREA-ISPS0211
              THRU EX-S1050.

      *--> CALL SERVIZIO ISPS0211
           PERFORM S1100-CALL-ISPS0211
              THRU EX-S1100.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA SERVIZIO DATI ISPS0211
      *----------------------------------------------------------------*
       S1050-PREP-AREA-ISPS0211.

           MOVE LOW-VALUES
             TO AREA-PRODUCT-SERVICES.

           MOVE IDSV0001-MODALITA-ESECUTIVA
             TO IJCCMQ03-MODALITA-ESECUTIVA.

           SET IJCCMQ03-ESITO-OK TO TRUE.

           MOVE 'ISPS0211'       TO IJCCMQ03-JAVA-SERVICE-NAME.

           MOVE ZERO             TO IJCCMQ00-LIVELLO-DEBUG.

       EX-S1050.
           EXIT.

      *----------------------------------------------------------------*
      *  CHIAMATA AL SERVIZIO ISPS0211
      *----------------------------------------------------------------*
       S1100-CALL-ISPS0211.

      *     MOVE LENGTH OF AREA-IO-ISPS0211
      *       TO IJCCMQ03-LENGTH-DATI-SERVIZIO.

      *    COMPUTE IJCCMQ03-LENGTH-DATI-SERVIZIO =
      *           LENGTH OF ISPC0211-DATI-INPUT  +
      *           LENGTH OF ISPC0211-AREA-ERRORI +
      *            (LENGTH OF ISPC0211-SCHEDA(1) *
      *             ISPC0211-DATI-NUM-MAX-ELE ).

      *    IF IDSV0001-KEY-BUSINESS5 = 'ALL_LENGTH'
      *       MOVE LENGTH OF AREA-IO-ISPS0211
      *         TO IJCCMQ03-LENGTH-DATI-SERVIZIO
      *    ELSE
      *    COMPUTE IJCCMQ03-LENGTH-DATI-SERVIZIO =
      *           LENGTH OF ISPC0211-DATI-INPUT  +
      *           LENGTH OF ISPC0211-AREA-ERRORI +
      *            (LENGTH OF ISPC0211-SCHEDA(1) *
      *                ISPC0211-DATI-NUM-MAX-ELE )
      *    END-IF.

      * Passiamo ad ACT come lunghezza dell'area input
      * la length dell'area input + la length dell'area errori
      * + la length dell'area schede P
      * + la length dell'area schede T per il numero di schede T
      * + una contingency di 20 schede T nel caso di applicazione di
      * opzioni (1 scheda per Tariffa)

           COMPUTE IJCCMQ03-LENGTH-DATI-SERVIZIO =
                  LENGTH OF ISPC0211-DATI-INPUT  +
                  LENGTH OF ISPC0211-AREA-ERRORI +
                  LENGTH OF ISPC0211-ELE-MAX-SCHEDA-P +
                  LENGTH OF ISPC0211-TAB-VAL-P +
                  LENGTH OF ISPC0211-ELE-MAX-SCHEDA-T +
                  (LENGTH OF ISPC0211-TAB-SCHEDA-T(1) *
                             ISPC0211-ELE-MAX-SCHEDA-T ) +
                  (LENGTH OF ISPC0211-TAB-SCHEDA-T(1) * 20 )



           MOVE AREA-IO-ISPS0211
             TO IJCCMQ03-AREA-DATI-SERVIZIO.

           SET WS-ADDRESS TO ADDRESS OF AREA-PRODUCT-SERVICES.

           MOVE IDSV0001-LIVELLO-DEBUG   TO IJCCMQ00-LIVELLO-DEBUG
           MOVE IDSV0001-AREA-ADDRESSES  TO IJCCMQ03-AREA-ADDRESSES
           MOVE IDSV0001-USER-NAME       TO IJCCMQ03-USER-NAME

           CALL INTERF-MQSERIES USING MQ01-ADDRESS
                                      AREA-PRODUCT-SERVICES
           ON EXCEPTION
              MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SERVIZIO DATI PRODOTTO' TO CALL-DESC
              MOVE 'S1100-CALL-ISPS0211'    TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

      *--> RETURNCODE ACTUATOR
           IF IJCCMQ03-ESITO-OK
              MOVE IJCCMQ03-AREA-DATI-SERVIZIO
                TO AREA-IO-ISPS0211

              MOVE ISPC0211-AREA-ERRORI TO ISPC0001-AREA-ERRORI

              PERFORM S0320-OUTPUT-PRODOTTO
                 THRU EX-S0320-OUTPUT-PRODOTTO
           ELSE
      *--> GESTIONE ERRORI AREA IJCSMQ01
              SET IDSV0001-ESITO-KO TO TRUE
              MOVE IJCCMQ03-MAX-ELE-ERRORI
                                TO IDSV0001-MAX-ELE-ERRORI
              PERFORM VARYING IX-TAB-ERR FROM 1 BY 1
                        UNTIL IX-TAB-ERR > IJCCMQ03-MAX-ELE-ERRORI
                  MOVE IJCCMQ03-COD-ERRORE(IX-TAB-ERR)
                    TO IDSV0001-COD-ERRORE(IX-TAB-ERR)
                  MOVE IJCCMQ03-DESC-ERRORE(IX-TAB-ERR)
                    TO IDSV0001-DESC-ERRORE(IX-TAB-ERR)
                  MOVE IJCCMQ03-LIV-GRAVITA-BE(IX-TAB-ERR)
                    TO IDSV0001-LIV-GRAVITA-BE(IX-TAB-ERR)
              END-PERFORM
           END-IF.

       EX-S1100.
           EXIT.

      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
           COPY IERP9903.

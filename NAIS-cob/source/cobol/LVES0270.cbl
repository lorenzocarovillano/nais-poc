      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA VER. 1.0                          **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVES0270.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2008.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *    PROGRAMMA ..... LVES0270
      *    TIPOLOGIA...... CONTROLLI
      *    PROCESSO....... XXX
      *    FUNZIONE....... XXX
      *    DESCRIZIONE.... EMISSIONE CONTRATTO INDIVIDUALE
      *    PAGINA WEB..... CALCOLI
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    VARIABILI DI WORKING
      *----------------------------------------------------------------*
       01 WK-PGM                            PIC X(008) VALUE 'LVES0270'.
       01 LCCS0019                          PIC X(008) VALUE 'LCCS0019'.
       01 IDSS0300                          PIC X(008) VALUE 'IDSS0300'.
      *----------------------------------------------------------------*
      *    VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *      COPY PER gestione tabella temp                            *
      *----------------------------------------------------------------*
           COPY IDSV0301.
           COPY IDSV0303.
      *----------------------------------------------------------------*
      *--> DEFINIZIONE DI INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-TAB-TGA                  PIC S9(04) COMP.
           03 IX-TAB-POG                  PIC S9(04) COMP.
           03 IX-TAB-GRZ                  PIC S9(04) COMP.
           03 IX-FONDI                    PIC S9(04) COMP.
           03 IX-ALL                      PIC S9(04) COMP.
      *MOD20110512
           03 IX-TAB-PMO                  PIC S9(04) COMP.
           03 IX-TAB-BEP                  PIC S9(04) COMP.
       01 MOVIMENTO-CEDOLA                PIC X(01).
          88 MOVIMENTO-CEDOLA-NO            VALUE 'N'.
          88 MOVIMENTO-CEDOLA-SI            VALUE 'S'.
       01 BENEFICIARIO-CEDOLA             PIC X(01).
          88 BENEFICIARIO-CEDOLA-NO         VALUE 'N'.
          88 BENEFICIARIO-CEDOLA-SI         VALUE 'S'.

      *MOD20110512
      *----------------------------------------------------------------*
      * COSTANI                                                *
      *----------------------------------------------------------------*
       77 WK-PREST-INI-TOT               PIC S9(12)V9(3) COMP-3 VALUE 0.
      *----------------------------------------------------------------*
      *    AREA VERIFICA PLATFOND
      *----------------------------------------------------------------*
       01  AREA-LCCC0019.
           COPY LCCC0019             REPLACING ==(SF)== BY ==LCCC0019==.
      *---------------------------------------------------------------*
       LINKAGE SECTION.
      *---------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.

      *-- AREA COMUNE.
       01 WCOM-AREA-STATI.
          COPY LCCC0001               REPLACING ==(SF)== BY ==WCOM==.

      *-- POLIZZA
       01 WPOL-AREA-POLIZZA.
          04 WPOL-ELE-POLI-MAX       PIC S9(04) COMP.
          04 WPOL-TAB-POLI.
             COPY LCCVPOL1           REPLACING ==(SF)== BY ==WPOL==.

      *-- AREA ADESIONE
       01 WADE-AREA-ADESIONE.
          04 WADE-ELE-ADES-MAX       PIC S9(04) COMP.
          04 WADE-TAB-ADES.
             COPY LCCVADE1           REPLACING ==(SF)== BY ==WADE==.

      *-- AREA GARANZIA
       01 WGRZ-AREA-GARANZIA.
          04 WGRZ-ELE-GAR-MAX        PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
             COPY LCCVGRZ1           REPLACING ==(SF)== BY ==WGRZ==.

      *-- AREA TRANCHE DI GARANZIA
       01 WTGA-AREA-TRANCHE.
          04 WTGA-ELE-TRAN-MAX       PIC S9(04) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==WTGA==.
             COPY LCCVTGA1           REPLACING ==(SF)== BY ==WTGA==.

      *-- VINCOLO PEGNO
       01 WL23-AREA-VINC-PEG.
          04 WL23-ELE-VINC-PEG-MAX   PIC S9(04) COMP.
          04 WL23-TAB-VINC-PEG       OCCURS 10.
             COPY LCCVL231           REPLACING ==(SF)== BY ==WL23==.

      *-- AREA ASSET ALLOCATION
       01 WALL-AREA-ASSET.
             COPY LCCVALL7.
             COPY LCCVALL1           REPLACING ==(SF)== BY ==WALL==.

      *-- PARAMETRO OGGETTO
       01 WPOG-AREA-PARAM-OGG.
          04 WPOG-ELE-PARAM-OGG-MAX  PIC S9(04) COMP.
                COPY LCCVPOGB REPLACING   ==(SF)==  BY ==WPOG==.
             COPY LCCVPOG1               REPLACING ==(SF)== BY ==WPOG==.
      *MOD20110512
      **--   PARAMETRO MOVIMENTO
       01 WPMO-AREA-PARAM-MOV.
          04 WPMO-ELE-PARAM-MOV-MAX  PIC S9(04) COMP.
                COPY LCCVPMOB REPLACING   ==(SF)==  BY ==WPMO==.
             COPY LCCVPMO1              REPLACING ==(SF)== BY ==WPMO==.
      *--  BENEFICIARI
       01 WBEP-AREA-BENEFICIARI.
          04 WBEP-ELE-BENEFICIARI    PIC S9(04) COMP.
                COPY LCCVBEPA REPLACING   ==(SF)==  BY ==WBEP==.
             COPY LCCVBEP1              REPLACING ==(SF)== BY ==WBEP==.
      *MOD20110512
      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TRANCHE
                                WL23-AREA-VINC-PEG
                                WALL-AREA-ASSET
                                WPOG-AREA-PARAM-OGG
      *MOD20110512
                                WPMO-AREA-PARAM-MOV
                                WBEP-AREA-BENEFICIARI.
      *MOD20110512
      *---------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *                                                                *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                     AREA-LCCC0019
                                          IX-INDICI.
           IF WALL-ELE-ASSET-ALL-MAX < 0
              INITIALIZE                  WALL-AREA-ASSET
           END-IF.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *   CORPO ELABORATIVO                                            *
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           PERFORM S1100-CONTROLLI-BUSINESS
              THRU EX-S1100.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLO DATI INPUT
      *----------------------------------------------------------------*
       S1100-CONTROLLI-BUSINESS.

      * -- CONTROLLI CAMPI VINCOLO

           IF NOT WL23-ST-DEL(1)

              IF WL23-DT-CHIU-VINPG(1) IS NUMERIC
              AND WL23-DT-CHIU-VINPG(1) GREATER ZEROES

                  IF WL23-DT-CHIU-VINPG(1) NOT GREATER
                     WPOL-DT-DECOR

                     MOVE WK-PGM
                       TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1100-CONTROLLI-BUSINESS'
                       TO IEAI9901-LABEL-ERR
                     MOVE '005021'
                       TO IEAI9901-COD-ERRORE
                     STRING 'LA DATA CHIUSURA VINCOLO ;'
                            'DELLA DATA DECORRENZA POLIZZA ;'
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
                  END-IF

                  IF WPOL-DT-SCAD IS NUMERIC
                   IF WL23-DT-CHIU-VINPG(1) NOT LESS
                     WPOL-DT-SCAD

                     MOVE WK-PGM
                       TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1100-CONTROLLI-BUSINESS'
                       TO IEAI9901-LABEL-ERR
                     MOVE '005022'
                       TO IEAI9901-COD-ERRORE
                     STRING 'LA DATA CHIUSURA VINCOLO ;'
                            'DELLA DATA SCADENZA POLIZZA ;'
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
                   END-IF
                  END-IF

              END-IF

              IF  WL23-FL-VINPG-INT-PRSTZ(1) = 'N'
              AND WL23-CPT-VINCTO-PIGN-NULL(1) NOT = HIGH-VALUE

                 PERFORM S1200-CALCOLA-PREST-INI      THRU S1200-EX

                 IF WK-PREST-INI-TOT IS NUMERIC
                 AND WK-PREST-INI-TOT GREATER ZEROES

                     IF WL23-CPT-VINCTO-PIGN(1) GREATER
                        WK-PREST-INI-TOT
                        MOVE WK-PGM
                          TO IEAI9901-COD-SERVIZIO-BE
                        MOVE 'S1100-CONTROLLI-BUSINESS'
                          TO IEAI9901-LABEL-ERR
                        MOVE '005022'
                          TO IEAI9901-COD-ERRORE
                        STRING 'L IMPORTO LIBERO DEL VINCOLO ;'
                               'DEL PREMIO INIZIALE ;'
                          DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                        END-STRING
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                           THRU EX-S0300
                     END-IF

                 END-IF
              END-IF
           END-IF.

      *    IF IDSV0001-ESITO-OK
      *-->    PER LINEAR I CONTROLLI SUL PLATFOND AVVENGONO VIA WEB
      *-->    E NON DEVONO ESSERE FATTI IN FASE DI CREAZIONE PROPOSTA
      *       IF IDSV0001-COD-COMPAGNIA-ANIA = 8
      *          CONTINUE
      *       ELSE
      *       IF WCOM-ELE-MAX-PLATFOND GREATER ZERO
      *       AND WALL-ELE-ASSET-ALL-MAX   GREATER ZERO
      *          SET LCCC0019-LETTURA-VPL      TO TRUE
      *          SET LCCC0019-CONTROLLO-SI     TO TRUE
      *          PERFORM VERIFICA-PLATFOND
      *             THRU VERIFICA-PLATFOND-EX
      *
      *-- IN CASO DI NON DISPONIBILITA' DI PLAFOND SEGNALO ERRORE
      *
      *          IF LCCC0019-GENERIC-ERROR
      *             MOVE LCCC0019-COD-SERVIZIO-BE
      *               TO IEAI9901-COD-SERVIZIO-BE
      *             MOVE 'S1100-CONTROLLI-BUSINESS'
      *               TO IEAI9901-LABEL-ERR
      *             MOVE LCCC0019-COD-ERRORE
      *               TO IEAI9901-COD-ERRORE
      *             MOVE LCCC0019-PARAMETRI-ERR
      *               TO IEAI9901-PARAMETRI-ERR
      *             PERFORM S0300-RICERCA-GRAVITA-ERRORE
      *                THRU EX-S0300
      *          END-IF
      *       END-IF
      *    END-IF.
      *MOD20110512
           IF IDSV0001-ESITO-OK
             AND WPOL-ST-ADD
              PERFORM S1170-CTRL-BENEFICIARI
                 THRU S1170-EX
            END-IF.
      *MOD20110512

       EX-S1100.
           EXIT.

      *MOD20110512
      *----------------------------------------------------------------*
      * Controllo beneficiario
      *----------------------------------------------------------------*
       S1170-CTRL-BENEFICIARI.

           SET MOVIMENTO-CEDOLA-NO    TO TRUE.
           SET BENEFICIARIO-CEDOLA-NO TO TRUE.

           PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
             UNTIL IX-TAB-PMO GREATER WPMO-ELE-PARAM-MOV-MAX
                OR IDSV0001-ESITO-KO
             IF WPMO-TP-MOVI(IX-TAB-PMO) IS NUMERIC AND
                WPMO-TP-MOVI(IX-TAB-PMO) = 6007
                SET MOVIMENTO-CEDOLA-SI  TO TRUE
                PERFORM VARYING IX-TAB-BEP FROM 1 BY 1
                  UNTIL IX-TAB-BEP GREATER WBEP-ELE-BENEFICIARI
                     IF WBEP-COD-BNFIC(IX-TAB-BEP) IS NUMERIC AND
                        WBEP-COD-BNFIC(IX-TAB-BEP) = 99
                        SET MOVIMENTO-CEDOLA-SI  TO TRUE
                        SET BENEFICIARIO-CEDOLA-SI TO TRUE
                     END-IF
                END-PERFORM
              END-IF
           END-PERFORM.

           IF MOVIMENTO-CEDOLA-SI     AND
              BENEFICIARIO-CEDOLA-NO
              MOVE WK-PGM  TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1100-CONTROLLI-BUSINESS' TO IEAI9901-LABEL-ERR
              MOVE '005166'                   TO IEAI9901-COD-ERRORE
              STRING 'BENEFICIARIO CEDOLE NON PRESENTE ;'
               DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                  THRU EX-S0300
           END-IF.

       S1170-EX.
           EXIT.
      *MOD20110512

      *----------------------------------------------------------------*
      *    IMPOSTA AREA PER CALL TEMP
      *----------------------------------------------------------------*
       S1160-IMPOSTA-TEMP.
      *
           INITIALIZE IDSV0301
                      IDSV0303.
           MOVE 'ALL'                   TO IDSV0301-ALIAS-STR-DATO.
           MOVE LENGTH OF WALL-TABELLA
                                        TO IDSV0301-BUFFER-DATA-LEN.
           SET IDSV0301-READ            TO TRUE.
           SET IDSV0301-ESITO-OK        TO TRUE.
           SET IDSV0301-SUCCESSFUL-SQL  TO TRUE.
           SET IDSV0301-ADDRESS         TO ADDRESS OF WALL-TABELLA.
      *
       EX-S1160.
           EXIT.
      *----------------------------------------------------------------*
      *   SOMMA LE PRESTAZIONI INIZIALI DELLA TRANCHE
      *----------------------------------------------------------------*
       S1200-CALCOLA-PREST-INI.
      *
           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA GREATER WTGA-ELE-TRAN-MAX
      *
             IF WTGA-PRSTZ-INI(IX-TAB-TGA) IS NUMERIC
                COMPUTE WK-PREST-INI-TOT = WK-PREST-INI-TOT +
                                           WTGA-PRSTZ-INI(IX-TAB-TGA)
             END-IF
      *
           END-PERFORM.
      *
       S1200-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      * ROUTINES GESTIONE ERRORI                                       *
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
      *----------------------------------------------------------------*
      *    COPY PER LA VERIFICA DEL PLATFOND
      *----------------------------------------------------------------*
           COPY LCCP0019 REPLACING ==(SF)== BY ==WTGA==.
      *----------------------------------------------------------------*
      *    COPY PER LA GESTIONE DELLA TABELLA TEMP.
      *----------------------------------------------------------------*
           COPY IDSP0301.
           COPY IDSP0302.

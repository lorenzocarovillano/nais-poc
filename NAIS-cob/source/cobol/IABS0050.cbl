       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IABS0050 IS INITIAL.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE
      * F A S E         : GESTIONE TABELLA BTC_BATCH_TYPE
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

           EXEC SQL INCLUDE IDSV0010 END-EXEC.

      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDBBT0 END-EXEC.

           EXEC SQL INCLUDE IDBVBBT2 END-EXEC.


      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVBBT1 END-EXEC.

      *****************************************************************

       PROCEDURE DIVISION USING IDSV0003 BTC-BATCH-TYPE.

           PERFORM A000-INIZIO              THRU A000-EX.

           PERFORM A300-ELABORA             THRU A300-EX

           GOBACK.
      ******************************************************************
       A000-INIZIO.

           MOVE 'IABS0050'              TO   IDSV0003-COD-SERVIZIO-BE.
           MOVE 'BTC_BATCH_TYPE'        TO   IDSV0003-NOME-TABELLA.

           MOVE '00'                    TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                  TO   IDSV0003-SQLCODE
                                             IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2
                                             IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.
      ******************************************************************
       A100-CHECK-RETURN-CODE.

           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      ******************************************************************
       A300-ELABORA.

           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
                 PERFORM A301-ELABORA-PK             THRU A301-EX
              WHEN IDSV0003-WHERE-CONDITION
                 PERFORM A302-ELABORA-WC             THRU A302-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
           END-EVALUATE.

       A300-EX.
           EXIT.
      ******************************************************************
       A301-ELABORA-PK.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-PK             THRU A310-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.

       A301-EX.
           EXIT.
      ******************************************************************
       A302-ELABORA-WC.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM W000-SELECT-WC             THRU W000-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.

       A302-EX.
           EXIT.
      ******************************************************************
       A310-SELECT-PK.

           EXEC SQL
             SELECT
                COD_BATCH_TYPE
                ,DES
                ,DEF_CONTENT_TYPE
                ,COMMIT_FREQUENCY
                ,SERVICE_NAME
                ,SERVICE_GUIDE_NAME
                ,SERVICE_ALPO_NAME
                ,COD_MACROFUNCT
                ,TP_MOVI
             INTO
                :BBT-COD-BATCH-TYPE
               ,:BBT-DES-VCHAR
               ,:BBT-DEF-CONTENT-TYPE
                :IND-BBT-DEF-CONTENT-TYPE
               ,:BBT-COMMIT-FREQUENCY
                :IND-BBT-COMMIT-FREQUENCY
               ,:BBT-SERVICE-NAME
                :IND-BBT-SERVICE-NAME
               ,:BBT-SERVICE-GUIDE-NAME
                :IND-BBT-SERVICE-GUIDE-NAME
               ,:BBT-SERVICE-ALPO-NAME
                :IND-BBT-SERVICE-ALPO-NAME
               ,:BBT-COD-MACROFUNCT
                :IND-BBT-COD-MACROFUNCT
               ,:BBT-TP-MOVI
                :IND-BBT-TP-MOVI
             FROM  BTC_BATCH_TYPE
             WHERE COD_BATCH_TYPE = :BBT-COD-BATCH-TYPE

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

           END-IF.

       A310-EX.
           EXIT.

      ******************************************************************

       W000-SELECT-WC.

           PERFORM Z200-SET-INDICATORI-NULL THRU Z200-EX.

           EXEC SQL
             SELECT
                COD_BATCH_TYPE
                ,DES
                ,DEF_CONTENT_TYPE
                ,COMMIT_FREQUENCY
                ,SERVICE_NAME
                ,SERVICE_GUIDE_NAME
                ,SERVICE_ALPO_NAME
                ,COD_MACROFUNCT
                ,TP_MOVI
             INTO
                :BBT-COD-BATCH-TYPE
               ,:BBT-DES-VCHAR
               ,:BBT-DEF-CONTENT-TYPE
                :IND-BBT-DEF-CONTENT-TYPE
               ,:BBT-COMMIT-FREQUENCY
                :IND-BBT-COMMIT-FREQUENCY
               ,:BBT-SERVICE-NAME
                :IND-BBT-SERVICE-NAME
               ,:BBT-SERVICE-GUIDE-NAME
                :IND-BBT-SERVICE-GUIDE-NAME
               ,:BBT-SERVICE-ALPO-NAME
                :IND-BBT-SERVICE-ALPO-NAME
               ,:BBT-COD-MACROFUNCT
                :IND-BBT-COD-MACROFUNCT
               ,:BBT-TP-MOVI
                :IND-BBT-TP-MOVI
             FROM  BTC_BATCH_TYPE
             WHERE COD_MACROFUNCT   = :BBT-COD-MACROFUNCT
                                      :IND-BBT-COD-MACROFUNCT AND
                   TP_MOVI          = :BBT-TP-MOVI
                                      :IND-BBT-TP-MOVI        AND
                   SUBSTR(DEF_CONTENT_TYPE, 1, 2)
                                    = :BBT-DEF-CONTENT-TYPE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

           END-IF.

       W000-EX.
           EXIT.


      ******************************************************************
       Z100-SET-COLONNE-NULL.

           IF IND-BBT-DEF-CONTENT-TYPE = -1
              MOVE HIGH-VALUES TO BBT-DEF-CONTENT-TYPE-NULL
           END-IF
           IF IND-BBT-COMMIT-FREQUENCY = -1
              MOVE HIGH-VALUES TO BBT-COMMIT-FREQUENCY-NULL
           END-IF
           IF IND-BBT-SERVICE-NAME = -1
              MOVE HIGH-VALUES TO BBT-SERVICE-NAME-NULL
           END-IF
           IF IND-BBT-SERVICE-GUIDE-NAME = -1
              MOVE HIGH-VALUES TO BBT-SERVICE-GUIDE-NAME-NULL
           END-IF
           IF IND-BBT-SERVICE-ALPO-NAME = -1
              MOVE HIGH-VALUES TO BBT-SERVICE-ALPO-NAME-NULL
           END-IF
           IF IND-BBT-COD-MACROFUNCT = -1
              MOVE HIGH-VALUES TO BBT-COD-MACROFUNCT-NULL
           END-IF
           IF IND-BBT-TP-MOVI = -1
              MOVE HIGH-VALUES TO BBT-TP-MOVI-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF BBT-DEF-CONTENT-TYPE-NULL = HIGH-VALUES
              MOVE -1 TO IND-BBT-DEF-CONTENT-TYPE
           ELSE
              MOVE 0 TO IND-BBT-DEF-CONTENT-TYPE
           END-IF
           IF BBT-COMMIT-FREQUENCY-NULL = HIGH-VALUES
              MOVE -1 TO IND-BBT-COMMIT-FREQUENCY
           ELSE
              MOVE 0 TO IND-BBT-COMMIT-FREQUENCY
           END-IF
           IF BBT-SERVICE-NAME-NULL = HIGH-VALUES
              MOVE -1 TO IND-BBT-SERVICE-NAME
           ELSE
              MOVE 0 TO IND-BBT-SERVICE-NAME
           END-IF
           IF BBT-SERVICE-GUIDE-NAME-NULL = HIGH-VALUES
              MOVE -1 TO IND-BBT-SERVICE-GUIDE-NAME
           ELSE
              MOVE 0 TO IND-BBT-SERVICE-GUIDE-NAME
           END-IF
           IF BBT-SERVICE-ALPO-NAME-NULL = HIGH-VALUES
              MOVE -1 TO IND-BBT-SERVICE-ALPO-NAME
           ELSE
              MOVE 0 TO IND-BBT-SERVICE-ALPO-NAME
           END-IF
           IF BBT-COD-MACROFUNCT-NULL = HIGH-VALUES
              MOVE -1 TO IND-BBT-COD-MACROFUNCT
           ELSE
              MOVE 0 TO IND-BBT-COD-MACROFUNCT
           END-IF
           IF BBT-TP-MOVI-NULL = HIGH-VALUES
              MOVE -1 TO IND-BBT-TP-MOVI
           ELSE
              MOVE 0 TO IND-BBT-TP-MOVI
           END-IF.

       Z200-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.

           CONTINUE.
       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.

           CONTINUE.
       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.


           MOVE LENGTH OF BBT-DES
                       TO BBT-DES-LEN.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
----->     EXEC SQL INCLUDE IDSP0003 END-EXEC.

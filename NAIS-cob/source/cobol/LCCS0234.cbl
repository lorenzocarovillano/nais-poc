      ******************************************************************
      **                                                        ********
      **    PORTAFOGLIO VITA ITALIA  VER 1.0                    ********
      **                                                        ********
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LCCS0234.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *    PROGRAMMA ..... LCCS0234
      *    TIPOLOGIA......
      *    PROCESSO....... Componenti Comuni
      *    FUNZIONE....... XXXX
      *    DESCRIZIONE.... Estrazione degli ID relativi a gli oggetti
      *                    Polizza,adesione, Garanzia e Tranche.
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
      *--> NOME PROGRAMMA
       01 WK-PGM                            PIC X(008) VALUE 'LCCS0234'.

      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01 IX-INDICI.
          03 IX-GRZ                         PIC S9(04) COMP.
          03 IX-TGA                         PIC S9(04) COMP.

      *----------------------------------------------------------------*
      *    FLAGS
      *----------------------------------------------------------------*
       01  FL-RICERCA                   PIC X(001) VALUE SPACES.
           88 NO-TROVATO                VALUE '0'.
           88 SI-TROVATO                VALUE '1'.

      *----------------------------------------------------------------*
      *    VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.

      *----------------------------------------------------------------*
      *    COPY DISPATCHER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      *--  COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *--  COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.


      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.

      *----------------------------------------------------------------*
      *    COMMAREA SERVIZIO
      *----------------------------------------------------------------*
      *--  POLIZZA
       01 WPOL-AREA-POLIZZA.
          04 WPOL-ELE-POLI-MAX       PIC S9(04) COMP VALUE 0.
          04 WPOL-TAB-POLI.
             COPY LCCVPOL1              REPLACING ==(SF)== BY ==WPOL==.
      *--- ADESIONE
       01 WADE-AREA-ADESIONE.
          04 WADE-ELE-ADES-MAX       PIC S9(04) COMP VALUE 0.
          04 WADE-TAB-ADES.
             COPY LCCVADE1              REPLACING ==(SF)== BY ==WADE==.
      *--  AREA GARANZIA
       01 WGRZ-AREA-GARANZIA.
          04 WGRZ-ELE-GAR-MAX        PIC S9(04) COMP VALUE 0.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
             COPY LCCVGRZ1              REPLACING ==(SF)== BY ==WGRZ==.
      *--  AREA TRANCHE DI GARANZIA
       01 WTGA-AREA-SEZ-TRANCHE.
          04 WTGA-ELE-TRAN-MAX       PIC S9(04) COMP VALUE 0.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==WTGA==.
             COPY LCCVTGA1              REPLACING ==(SF)== BY ==WTGA==.
       01 WCOM-DATI-INPUT.
          04 WCOM-ID-OGG-EOC         PIC S9(9).
          04 WCOM-TIPO-OGG-EOC       PIC X(2).

       01 WCOM-DATI-OUTPUT.
          04 WCOM-ID-OGG-PTF-EOC     PIC S9(9).
STORNI    04 WCOM-IB-OGG-PTF-EOC     PIC X(40).
GAR       04 WCOM-ID-POLI-PTF        PIC S9(9).
GAR       04 WCOM-ID-ADES-PTF        PIC S9(9).

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-SEZ-TRANCHE
                                WCOM-DATI-INPUT
                                WCOM-DATI-OUTPUT.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE        WCOM-DATI-OUTPUT.
           INITIALIZE        IX-INDICI.

           PERFORM S0005-CTRL-DATI-INPUT
              THRU EX-S0005.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    CONTROLLI DATI INPUT
      *----------------------------------------------------------------*
       S0005-CTRL-DATI-INPUT.

      *--  controllare che il campo WCOM-ID-OGG-EOC   Obbligatorio
      *--  controllare che il campo WCOM-TIPO-OGG-EOC Obbligatorio

           CONTINUE.

       EX-S0005.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           EVALUATE WCOM-TIPO-OGG-EOC

               WHEN 'PO'
                    PERFORM S1100-ELABORA-POLIZZA
                       THRU EX-S1100

               WHEN 'AD'
                    PERFORM S1200-ELABORA-ADESIONE
                       THRU EX-S1200

               WHEN 'GA'
                    PERFORM S1300-ELABORA-GARANZIE
                       THRU EX-S1300

               WHEN 'TG'
                    PERFORM S1400-ELABORA-TRANCHE
                       THRU EX-S1400

               WHEN 'LI'
               WHEN 'TL'
                    PERFORM S1500-ELABORA-LIQ
                       THRU EX-S1500

           END-EVALUATE.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    RICERCA DELL'OGGETTO PTF IN TABELLA POLIZZA
      *----------------------------------------------------------------*
       S1100-ELABORA-POLIZZA.

           IF WPOL-ID-POLI = WCOM-ID-OGG-EOC
              MOVE WPOL-ID-PTF              TO WCOM-ID-OGG-PTF-EOC
STORNI        MOVE WPOL-IB-OGG              TO WCOM-IB-OGG-PTF-EOC
GAR           MOVE WPOL-ID-PTF              TO WCOM-ID-POLI-PTF
           ELSE
      *--> OGGETTO PTF NON TROVATO
              MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1100-ELABORA-POLIZZA'  TO IEAI9901-LABEL-ERR
              MOVE '005027'                 TO IEAI9901-COD-ERRORE
              MOVE 'POLIZZA'                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1100.
           EXIT.

      *----------------------------------------------------------------*
      *    RICERCA DELL'OGGETTO PTF IN TABELLA ADESIONE
      *----------------------------------------------------------------*
       S1200-ELABORA-ADESIONE.

           IF WADE-ID-ADES = WCOM-ID-OGG-EOC
              MOVE WADE-ID-PTF           TO WCOM-ID-OGG-PTF-EOC
STORNI        MOVE WADE-IB-OGG           TO WCOM-IB-OGG-PTF-EOC
GAR           MOVE WADE-ID-PTF           TO WCOM-ID-ADES-PTF
GAR           MOVE WPOL-ID-PTF           TO WCOM-ID-POLI-PTF
              SET  SI-TROVATO            TO TRUE
           ELSE
      *--> OGGETTO PTF NON TROVATO
              MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1200-ELABORA-ADESIONE' TO IEAI9901-LABEL-ERR
              MOVE '005027'                 TO IEAI9901-COD-ERRORE
              MOVE 'ADESIONE'               TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1200.
           EXIT.

      *----------------------------------------------------------------*
      *    RICERCA DELL'OGGETTO PTF IN TABELLA GARANZIE
      *----------------------------------------------------------------*
       S1300-ELABORA-GARANZIE.

           SET NO-TROVATO        TO TRUE.

           PERFORM VARYING IX-GRZ FROM 1 BY 1
                     UNTIL IX-GRZ > WGRZ-ELE-GAR-MAX
                        OR SI-TROVATO

                IF WGRZ-ID-GAR(IX-GRZ) = WCOM-ID-OGG-EOC
                   MOVE WGRZ-ID-PTF(IX-GRZ) TO WCOM-ID-OGG-PTF-EOC
STORNI             MOVE WGRZ-IB-OGG(IX-GRZ) TO WCOM-IB-OGG-PTF-EOC
GAR                MOVE WADE-ID-PTF         TO WCOM-ID-ADES-PTF
GAR                MOVE WPOL-ID-PTF         TO WCOM-ID-POLI-PTF
                   SET  SI-TROVATO          TO TRUE
                END-IF

           END-PERFORM.

           IF NO-TROVATO
      *--> OGGETTO PTF NON TROVATO
              MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1300-ELABORA-GARANZIE' TO IEAI9901-LABEL-ERR
              MOVE '005027'                 TO IEAI9901-COD-ERRORE
              MOVE 'GARANZIA'               TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1300.
           EXIT.

      *----------------------------------------------------------------*
      *    RICERCA DELL'OGGETTO PTF IN TABELLA TRANCHE GARANZIE
      *----------------------------------------------------------------*
       S1400-ELABORA-TRANCHE.

           SET NO-TROVATO        TO TRUE.

           PERFORM VARYING IX-TGA FROM 1 BY 1
                     UNTIL IX-TGA > WTGA-ELE-TRAN-MAX
                        OR SI-TROVATO

                IF WTGA-ID-TRCH-DI-GAR(IX-TGA) = WCOM-ID-OGG-EOC
                   MOVE WTGA-ID-PTF(IX-TGA) TO WCOM-ID-OGG-PTF-EOC
STORNI             MOVE WTGA-IB-OGG(IX-TGA) TO WCOM-IB-OGG-PTF-EOC
12443              MOVE WADE-ID-PTF         TO WCOM-ID-ADES-PTF
12443              MOVE WPOL-ID-PTF         TO WCOM-ID-POLI-PTF
                   SET SI-TROVATO           TO TRUE
                END-IF

           END-PERFORM.

           IF NO-TROVATO
      *--> OGGETTO PTF NON TROVATO
              MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1400-ELABORA-TRANCHE' TO IEAI9901-LABEL-ERR
              MOVE '005027'                TO IEAI9901-COD-ERRORE
              MOVE 'TRCH-DI-GAR'           TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1400.
           EXIT.

      *----------------------------------------------------------------*
      *    RICERCA DELL'OGGETTO PTF IN TABELLA LIQUIDAZIONE
      *----------------------------------------------------------------*
       S1500-ELABORA-LIQ.

           MOVE WCOM-ID-OGG-EOC              TO WCOM-ID-OGG-PTF-EOC
STORNI     MOVE WCOM-TIPO-OGG-EOC            TO WCOM-IB-OGG-PTF-EOC
           MOVE WCOM-ID-OGG-EOC              TO WCOM-ID-POLI-PTF.


       EX-S1500.
           EXIT.

      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.

      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.


      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0015.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0015
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE DATA NASCITA RAPP-ANA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0015'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DATA-OUTPUT                  PIC 9(04)V9(07).
       01  WK-DATA-OUTPUT-V REDEFINES WK-DATA-OUTPUT.
           03 WK-NUMERICI                  PIC 9(04).
           03 WK-DECIMALI                  PIC 9(07).

       01 WK-DATA-X-12.
          03 WK-X-AA                          PIC X(04).
          03 WK-VIROGLA                       PIC X(01) VALUE ','.
          03 WK-X-GG                          PIC X(07).

       01 WK-FIND-LETTO                       PIC X(001).
          88 WK-LETTO-SI                      VALUE 'S'.
          88 WK-LETTO-NO                      VALUE 'N'.

      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                       PIC S9(04) COMP.
           03 IX-TAB-RAN                      PIC S9(04) COMP.
      *----------------------------------------------------------------*
      *    COPY DI INPUT PER LA CHIAMATA AL LVVS0000
      *----------------------------------------------------------------*
       01  INPUT-LVVS0000.
           COPY LVVC0000        REPLACING ==(SF)== BY ==LVVC0000==.
      *---------------------------------------------------------------*
      *  LISTA MOVIMENTI IMPLEMENTATI LCCC0006
      *---------------------------------------------------------------*
           COPY LCCC0006.

      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
      * 01 AREA-IO-POL.
      *   03 DRAN-AREA-RAPP-ANAG.
      *        04 DRAN-ELE-RAP-ANAG-MAX          PIC S9(04) COMP.
      *        04 DRAN-TAB-RAP-ANAG.
      *        COPY LCCVRAN1                     REPLACING ==(SF)==
      *                                                 BY ==DRAN==.

       01 AREA-IO-RAPP-ANA.
      *
          03 DRAN-AREA-RAPP-ANAG.
             04 DRAN-ELE-RAPP-ANAG-MAX  PIC S9(04) COMP.
                COPY LCCVRANA REPLACING   ==(SF)==  BY ==DRAN==.
             COPY LCCVRAN1              REPLACING ==(SF)== BY ==DRAN==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0015.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0015.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-RAPP-ANA
                      WK-DATA-OUTPUT
                      WK-DATA-X-12.


      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE

      *--> PERFORM DI CONTROLLI SUI I CAMPI CARICATI NELLE
      *--> DCLGEN DI WORKING
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1200-CONTROLLO-DATI
                  THRU S1200-CONTROLLO-DATI-EX
           END-IF.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
      *--> CALL MODULO PER RECUPERO DATA
               PERFORM S1300-CALL-LVVS0000
                  THRU S1300-CALL-LVVS0000-EX

               IF IDSV0003-SUCCESSFUL-RC
                EVALUATE TRUE

                    WHEN IDSV0003-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                         MOVE LVVC0000-DATA-OUTPUT
                           TO IVVC0213-VAL-IMP-O

                    WHEN OTHER
      *--->         ERRORE DI ACCESSO AL DB
                         SET IDSV0003-INVALID-OPER TO TRUE
                         MOVE WK-PGM
                           TO IDSV0003-COD-SERVIZIO-BE
                         STRING IDSV0003-RETURN-CODE  ';'
                                IDSV0003-SQLCODE
                         DELIMITED BY SIZE
                         INTO IDSV0003-DESCRIZ-ERR-DB2
                         END-STRING

                 END-EVALUATE
               ELSE
      *-->       GESTIRE ERRORE DISPATCHER
                 SET IDSV0003-INVALID-OPER TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 STRING IDSV0003-RETURN-CODE  ';'
                        IDSV0003-SQLCODE
                 DELIMITED BY SIZE
                 INTO IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING
               END-IF

           END-IF.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-RAPP-ANAG
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DRAN-AREA-RAPP-ANAG
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO DATI DCLGEN
      *----------------------------------------------------------------*
       S1200-CONTROLLO-DATI.

           IF DRAN-DT-NASC(IVVC0213-IX-TABB) NOT NUMERIC
              SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'DATA-NASCITA-RAPP-ANA NON NUMERICA'
                TO IDSV0003-DESCRIZ-ERR-DB2
           ELSE
              IF DRAN-DT-NASC(IVVC0213-IX-TABB) = 0
                 SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                 MOVE WK-PGM
                   TO IDSV0003-COD-SERVIZIO-BE
                 MOVE 'DATA-DATA-NASCITA-RAPP-ANA NON VALORIZZATA'
                   TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
           END-IF.

       S1200-CONTROLLO-DATI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CHIAMATA LVVS0000
      *----------------------------------------------------------------*
       S1300-CALL-LVVS0000.

           INITIALIZE INPUT-LVVS0000.
           SET LVVC0000-AAA-V-GGG         TO TRUE.
           MOVE DRAN-DT-NASC(IVVC0213-IX-TABB)
                                          TO LVVC0000-DATA-INPUT-1.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.
      *
           MOVE 'LVVS0000'                TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM USING         IDSV0003
                                          INPUT-LVVS0000.

       S1300-CALL-LVVS0000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           MOVE SPACES                     TO IVVC0213-VAL-STR-O.
           MOVE 0                          TO IVVC0213-VAL-PERC-O.
      *
           GOBACK.

       EX-S9000.
           EXIT.

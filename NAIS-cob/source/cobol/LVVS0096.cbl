      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0096.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0007
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0096'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
       01  WK-SQLCODE                       PIC --------9.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DATA-OUTPUT                  PIC 9(04)V9(07).
       01  WK-DATA-OUTPUT-V REDEFINES WK-DATA-OUTPUT.
           03 WK-NUMERICI                  PIC 9(04).
           03 WK-DECIMALI                  PIC 9(07).

       01 WK-DT-INI-COP                    PIC 9(08) VALUE ZEROES.
       01 WK-DT-END-COP                    PIC 9(08) VALUE ZEROES.

      *-- AREA ROUTINE PER IL CALCOLO
       01  FORMATO                 PIC X(01).
       01  DATA-INFERIORE.
           05 AAAA-INF             PIC 9(04).
           05 MM-INF               PIC 9(02).
           05 GG-INF               PIC 9(02).
       01  DATA-SUPERIORE.
           05 AAAA-SUP             PIC 9(04).
           05 MM-SUP               PIC 9(02).
           05 GG-SUP               PIC 9(02).
       01  GG-DIFF                 PIC 9(05).
       01  GG-DIFF-1               PIC 9(05).
       01  CODICE-RITORNO          PIC X(01).

      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBS
      *---------------------------------------------------------------*
           COPY LDBV2991.
           COPY LDBV4011.
      *---------------------------------------------------------------*
      *
           COPY IDBVSTB1.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-TIT.
          03 DTIT-AREA-TIT.
             04 DTIT-ELE-TIT-MAX        PIC S9(04) COMP.
                COPY LCCVTITA REPLACING   ==(SF)==  BY ==DTIT==.
             COPY LCCVTIT1              REPLACING ==(SF)== BY ==DTIT==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-TIT                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           INITIALIZE LDBV4011.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-TIT
                      WK-DATA-OUTPUT.
      *
      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
      *         IF IVVC0213-TP-LIVELLO = 'P'
                  PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
      *         ELSE
      *            PERFORM S1255-CALCOLA-DATA2         THRU S1255-EX
      *         END-IF
           END-IF.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1260-CALCOLA-DIFF          THRU S1260-EX
           END-IF.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1265-CALCOLA-DIFF-1        THRU S1265-EX
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-TIT-CONT
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DTIT-AREA-TIT
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATAULTADEGP
      *----------------------------------------------------------------*
       S1250-CALCOLA-DATA1.
      *
           INITIALIZE LDBV4011.
           MOVE DTIT-ID-TIT-CONT(IVVC0213-IX-TABB)
                                               TO LDBV4011-ID-TIT-CONT.
           SET IDSV0003-WHERE-CONDITION        TO TRUE.
           MOVE 'LDBS4010'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 LDBV4011
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS4010 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           MOVE IDSV0003-SQLCODE
             TO WK-SQLCODE

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE LDBV4011-DT-INI-COP            TO WK-DT-INI-COP
              MOVE LDBV4011-DT-END-COP            TO WK-DT-END-COP
           ELSE
              IF IDSV0003-SQLCODE = +100
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER        TO TRUE
                 MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
                 STRING 'CHIAMATA LDBS4010 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING
              END-IF
           END-IF.
      *
       S1250-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATA 1
      *----------------------------------------------------------------*
       S1260-CALCOLA-DIFF.
      *
           MOVE 'A'                               TO FORMATO.
           MOVE IVVC0213-DATA-EFFETTO             TO DATA-INFERIORE.
           MOVE WK-DT-END-COP                     TO DATA-SUPERIORE.
           MOVE 'LCCS0010'                        TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING FORMATO,
                                   DATA-INFERIORE,
                                   DATA-SUPERIORE,
                                   GG-DIFF,
                                   CODICE-RITORNO
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS1660 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           IF CODICE-RITORNO GREATER ZERO
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LCCS0010 COD-RIT:'
                      CODICE-RITORNO ';'
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
           END-IF.
      *
       S1260-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATA 1
      *----------------------------------------------------------------*
       S1265-CALCOLA-DIFF-1.
      *
           MOVE 'A'                               TO FORMATO.
           MOVE WK-DT-INI-COP                     TO DATA-INFERIORE.
           MOVE WK-DT-END-COP                     TO DATA-SUPERIORE.
           MOVE 'LCCS0010'                        TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING FORMATO,
                                   DATA-INFERIORE,
                                   DATA-SUPERIORE,
                                   GG-DIFF-1,
                                   CODICE-RITORNO
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS1660 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           IF CODICE-RITORNO = ZERO
              COMPUTE IVVC0213-VAL-IMP-O = GG-DIFF / GG-DIFF-1
           ELSE
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LCCS0010 COD-RIT:'
                      CODICE-RITORNO ';'
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
           END-IF.
      *
       S1265-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

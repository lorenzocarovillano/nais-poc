      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0039.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0039
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CALCOLO VARIABILE CUMPRENETTI
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0039'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DATA-OUTPUT                  PIC 9(04)V9(07).
       01  WK-DATA-OUTPUT-V REDEFINES WK-DATA-OUTPUT.
           03 WK-NUMERICI                  PIC 9(04).
           03 WK-DECIMALI                  PIC 9(07).

       01 WK-DATA-X-12.
          03 WK-X-AA                          PIC X(04).
          03 WK-VIROGLA                       PIC X(01) VALUE ','.
          03 WK-X-GG                          PIC X(07).

      *-- AREA ROUTINE PER IL CALCOLO
       01  FORMATO                 PIC X(01).
       01  DATA-INFERIORE.
           05 AAAA-INF             PIC 9(04).
           05 MM-INF               PIC 9(02).
           05 GG-INF               PIC 9(02).
       01  DATA-SUPERIORE.
           05 AAAA-SUP             PIC 9(04).
           05 MM-SUP               PIC 9(02).
           05 GG-SUP               PIC 9(02).
       01  GG-DIFF                 PIC 9(05).
       01  CODICE-RITORNO          PIC X(01).


12969  01 WK-APPO-LUNGHEZZA              PIC S9(09) COMP-3.

       01  WK-FIND-LETTO                     PIC X(001).
           88 WK-LETTO-SI                    VALUE 'S'.
           88 WK-LETTO-NO                    VALUE 'N'.

12969  01  TROVATA-GAR                     PIC X(001).
12969      88 TROVATA-GAR-SI                    VALUE 'S'.
12969      88 TROVATA-GAR-NO                    VALUE 'N'.

12969  01  TROVATA-VAR                     PIC X(001).
12969      88 TROVATA-VAR-SI                    VALUE 'S'.
12969      88 TROVATA-VAR-NO                    VALUE 'N'.

12969  01  TROVATA-AREA                    PIC X(001).
12969      88 TROVATA-AREA-SI                   VALUE 'S'.
12969      88 TROVATA-AREA-NO                   VALUE 'N'.

      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBSF980
      *---------------------------------------------------------------*
           COPY LDBVF981.
           COPY IDBVDTC1.
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-POL.
          03 DPOL-AREA-POL.
             04 DPOL-ELE-POL-MAX        PIC S9(04) COMP.
             04 DPOL-TAB-POL.
             COPY LCCVPOL1              REPLACING ==(SF)== BY ==DPOL==.

       01 AREA-POINTER.
          05 FXT-ADDRESS            POINTER.
12969     05 VXG-ADDRESS            POINTER.

12969 *01  AREA-IVVC0223.
12969 *      COPY IVVC0223        REPLACING ==(SF)== BY ==IVVC0223==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-TGA                     PIC S9(04) COMP.
12969      03 IX-TAB-GRZ                     PIC S9(04) COMP.
12969      03 IX-TAB-VAR                     PIC S9(04) COMP.
12969      03 IX-TAB-TEMP                    PIC S9(04) COMP.
12969      03 IX-TAB-TEMP2                   PIC S9(04) COMP.
12969      03 IX-CONTA                       PIC S9(04) COMP.

       01 IMPORTO-TOTALE                     PIC S9(12)V9(3) COMP-3.

      *---------------------------------------------------------------*
      *  COPY TIPOLOGICHE
      *---------------------------------------------------------------*
11849      COPY LCCVXMVZ.
11849      COPY LCCVXMV1.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0039.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

12969  01  AREA-IVVC0223.
12969        COPY IVVC0223        REPLACING ==(SF)== BY ==IVVC0223==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0039.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           INITIALIZE LDBVF981
                      IMPORTO-TOTALE
                      DETT-TIT-CONT.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

12969      SET ADDRESS OF AREA-IVVC0223
12969                  TO VXG-ADDRESS

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1200-CONTROLLO-DATI
                  THRU S1200-CONTROLLO-DATI-EX
           END-IF.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
11849        MOVE IDSV0003-TIPO-MOVIMENTO TO WS-MOVIMENTO
11849        IF CREAZ-INDIVI
11849          MOVE ZEROES                TO IVVC0213-VAL-IMP-O
11849        ELSE
12969          PERFORM S1240-CONTROLLA-GAR
12969             THRU EX-S1250
12969          IF TROVATA-VAR-NO
12969             PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
                  IF  NOT IDSV0003-INVALID-OPER
                     PERFORM S1260-CARICA-AREA-GAR
                        THRU EX-S1260
                  END-IF
12969          END-IF
11849        END-IF
           END-IF.
      *
       EX-S1000.
           EXIT.
12969 *----------------------------------------------------------------*
      *--> CONTROLLO L ID GAR DI CONTESTO CON L ARIA DELLE GARANZIE PER
      *--> EVITARE LETTURE MULTIPLE SULLA STESSA GARANZIA
      *----------------------------------------------------------------*
       S1240-CONTROLLA-GAR.

           SET TROVATA-GAR-NO           TO TRUE
           SET TROVATA-VAR-NO           TO TRUE

           PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
             UNTIL IX-TAB-GRZ > IVVC0223-ELE-MAX-AREA-GAR
                OR TROVATA-GAR-SI
                   IF IVVC0213-ID-GARANZIA =
                      IVVC0223-ID-GARANZIA(IX-TAB-GRZ)

                      SET  TROVATA-GAR-SI         TO TRUE
                      MOVE IX-TAB-GRZ TO IX-TAB-TEMP

                      PERFORM VARYING IX-TAB-VAR FROM 1 BY 1
                        UNTIL IX-TAB-VAR >
                              IVVC0223-ELE-MAX-VAR-GAR(IX-TAB-GRZ)
                           OR TROVATA-VAR-SI

                              IF IVVC0223-COD-VARIABILE
                                 (IX-TAB-GRZ, IX-TAB-VAR) =
                                 IVVC0213-COD-VARIABILE
                                 SET  TROVATA-VAR-SI    TO TRUE
                                 MOVE IVVC0223-VAL-IMP
                                      (IX-TAB-GRZ, IX-TAB-VAR)
                                   TO IVVC0213-VAL-IMP-O
                              END-IF

                      END-PERFORM

                   END-IF
           END-PERFORM.

12969  EX-S1250.
12969      EXIT.
12969 *----------------------------------------------------------------*
      *--> VALORIZZAZIONE AREA DI SCAMBIO TRA VALORIZZATORE E MODULO DI
      *--> CALCOLO PER EVITARE LETTURE MULTIPLE
      *----------------------------------------------------------------*
       S1260-CARICA-AREA-GAR.

           IF TROVATA-GAR-NO
              ADD 1
                 TO IVVC0223-ELE-MAX-AREA-GAR
              MOVE IVVC0223-ELE-MAX-AREA-GAR
                TO IX-TAB-TEMP
              MOVE IVVC0213-ID-GARANZIA
                 TO IVVC0223-ID-GARANZIA(IX-TAB-TEMP)
              MOVE IVVC0213-COD-LIVELLO
                 TO IVVC0223-COD-TARI(IX-TAB-TEMP)


              MOVE IMPORTO-TOTALE
                TO IVVC0213-VAL-IMP-O
           END-IF

           ADD 1
             TO IVVC0223-ELE-MAX-VAR-GAR(IX-TAB-TEMP)
           MOVE IVVC0223-ELE-MAX-VAR-GAR(IX-TAB-TEMP)
             TO IX-TAB-TEMP2
           MOVE IVVC0213-COD-VARIABILE
             TO IVVC0223-COD-VARIABILE(IX-TAB-TEMP, IX-TAB-TEMP2)

           MOVE 'N'
             TO IVVC0223-TP-DATO(IX-TAB-TEMP, IX-TAB-TEMP2)

           MOVE IMPORTO-TOTALE
             TO IVVC0223-VAL-IMP(IX-TAB-TEMP, IX-TAB-TEMP2).

           SET TROVATA-AREA-NO        TO TRUE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
      *    PERFORM S1200-CARICA-DCLGEN
      *       THRU S1200-CARICA-DCLGEN-EX
      *    VARYING IX-DCLGEN FROM 1 BY 1
      *      UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
      *         OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
      *            SPACES OR LOW-VALUE OR HIGH-VALUE
      *         OR TROVATA-AREA-SI.
      *
      *    IF TROVATA-AREA-NO
      *
      *       COMPUTE WK-APPO-LUNGHEZZA =
      *               IVVC0213-POSIZ-INI(IVVC0213-ELE-INFO-MAX) +
      *               IVVC0213-LUNGHEZZA(IVVC0213-ELE-INFO-MAX) + 1
      *
      *       ADD 1                        TO IVVC0213-ELE-INFO-MAX
      *       MOVE IVVC0213-ELE-INFO-MAX   TO IX-CONTA
      *       MOVE IVVC0218-ALIAS-AREA-VAR-X-GAR
      *         TO IVVC0213-TAB-ALIAS(IX-CONTA)
      *       MOVE IVVC0223-ELE-MAX-AREA-GAR
      *                           TO IVVC0213-NUM-OCCORRENZE(IX-CONTA)
      *
      *       MOVE WK-APPO-LUNGHEZZA    TO IVVC0213-POSIZ-INI(IX-CONTA)
      *       MOVE LENGTH OF IVVC0223-AREA-VARIABILI-GAR
      *                                 TO IVVC0213-LUNGHEZZA(IX-CONTA)
      *
      *       COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
      *                              IVVC0213-LUNGHEZZA(IX-CONTA) + 1
      *-->      Valorizzazione del buffer dati
      *       MOVE IVVC0223-AREA-VARIABILI-GAR
      *         TO IVVC0213-BUFFER-DATI(IVVC0213-POSIZ-INI(IX-CONTA):
      *                                 IVVC0213-LUNGHEZZA(IX-CONTA))
      *
      *    END-IF.

       EX-S1260.
12969      EXIT.
      *----------------------------------------------------------------*
      *    CARICO AREA NEL BUFFER
      *----------------------------------------------------------------*
12696  S1200-CARICA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-AREA-VAR-X-GAR

              SET TROVATA-AREA-SI TO TRUE
              MOVE AREA-IVVC0223
                TO IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))

           END-IF.

       S1200-CARICA-DCLGEN-EX.
12969      EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-POLI
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPOL-AREA-POL
           END-IF.

12969      IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
12969         IVVC0218-ALIAS-AREA-VAR-X-GAR
12969         MOVE IVVC0213-BUFFER-DATI
12969             (IVVC0213-POSIZ-INI(IX-DCLGEN) :
12969              IVVC0213-LUNGHEZZA(IX-DCLGEN))
12969 *         TO AREA-IVVC0223
12969           TO AREA-POINTER
12969      END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA DATA 1
      *----------------------------------------------------------------*
       S1250-CALCOLA-DATA1.
      *

           SET IDSV0003-SUCCESSFUL-RC        TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE
           SET IDSV0003-FETCH-FIRST          TO TRUE
           SET IDSV0003-TRATT-X-COMPETENZA   TO TRUE

           MOVE IVVC0213-ID-GARANZIA         TO LDBVF981-ID-GAR

           MOVE 'EM'                         TO LDBVF981-TP-STAT-TIT-1
           MOVE 'IN'                         TO LDBVF981-TP-STAT-TIT-2
           MOVE SPACES                       TO LDBVF981-TP-STAT-TIT-3
           MOVE SPACES                       TO LDBVF981-TP-STAT-TIT-4
           MOVE SPACES                       TO LDBVF981-TP-STAT-TIT-5
           MOVE IDSV0003-DATA-INIZIO-EFFETTO TO LDBVF981-DT-INI-COP
           MOVE LDBVF981
             TO IDSV0003-BUFFER-WHERE-COND
           MOVE 'LDBSF980'                   TO WK-CALL-PGM
      *
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC OR
                         NOT IDSV0003-SUCCESSFUL-SQL

            CALL WK-CALL-PGM  USING  IDSV0003 DETT-TIT-CONT
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'ERRORE CHIAMATA LDBSF980 - LVVS0039'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
            END-CALL

            EVALUATE TRUE

              WHEN IDSV0003-SUCCESSFUL-SQL
                   SET IDSV0003-FETCH-NEXT    TO TRUE
              PERFORM Z000-CALCOLA       THRU Z000-EX

              WHEN IDSV0003-NOT-FOUND
                 IF IDSV0003-FETCH-FIRST
                    SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                 END-IF

              WHEN OTHER
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
                 STRING 'CHIAMATA LDBSF980 ;'
                     IDSV0003-RETURN-CODE '-'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
                 SET IDSV0003-INVALID-OPER            TO TRUE

              END-EVALUATE
           END-PERFORM.
      *
       S1250-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO DATI DCLGEN
      *----------------------------------------------------------------*
       S1200-CONTROLLO-DATI.

      *    MOVE DPOL-TP-FRM-ASSVA            TO WS-TP-FRM-ASSVA
           IF IVVC0213-ID-TRANCHE IS NUMERIC
              IF IVVC0213-ID-GARANZIA = 0
                SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                MOVE WK-PGM
                  TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'ID-GARANZIA NON VALORIZZATO'
                  TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
           ELSE
              SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ID-GARANZIA NON VALORIZZATO'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.

       S1200-CONTROLLO-DATI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CALCOLA IMPORTO DEFINITIVO
      *----------------------------------------------------------------*
       Z000-CALCOLA.

           IF DTC-PRE-TOT-NULL NOT = HIGH-VALUE AND LOW-VALUE AND SPACE
              ADD DTC-PRE-TOT
                 TO IMPORTO-TOTALE
           END-IF.
       Z000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

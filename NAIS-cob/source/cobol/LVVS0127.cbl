      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0127.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2009.
       DATE-COMPILED.
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0127'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
11626 *  --> MODULO PER CALCOLO DATA FINE MESE
  "    01  LCCS0004                         PIC X(8) VALUE 'LCCS0004'.

      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
11626  01 WK-APPO-DT.
          03 WK-APPO-AAAA               PIC 9(04).
          03 WK-APPO-MM                 PIC 9(02).
          03 WK-APPO-GG                 PIC 9(02).
      *
11626  01 IX-CICLO                      PIC 9(01) VALUE ZEROES.
      *
      *----------------------------------------------------------------*
      * AREA LCCS0004
      *----------------------------------------------------------------*
      *
11626  01  PARAM                          PIC 9.
  "    01  X-DATA.
  "        05 X-GG                        PIC 9(2).
  "        05 X-MM                        PIC 9(2).
  "        05 X-AAAA                      PIC 9(4).

      *
       01  IN-RCODE                         PIC 9(002).
       01  WK-DATA-9                        PIC 9(008).
       01  WK-DATA-X                        PIC X(008).
11626  01  WK-DT-CALC                       PIC 9(008).

      *----------------------------------------------------------------*
      * DEFINIZIONE AREA DI APPOGGIO PER FORMATTAZIONE DATE
      *----------------------------------------------------------------*
      * PER DATA DECORRENZA POLIZZA E ADESIONE
       01  WK-DATA-AMG-POL.
           05 WK-AAAA-POL                   PIC 9(004) VALUE ZEROES.
           05 WK-MM-POL                     PIC 9(002) VALUE ZEROES.
           05 WK-GG-POL                     PIC 9(002) VALUE ZEROES.
       01  WK-DATA-AMG-POL-R REDEFINES WK-DATA-AMG-POL PIC 9(008).
      * PER DATA DECORRENZA TRANCHE
       01  WK-DATA-AMG-TGA.
           05 WK-AAAA-TGA                   PIC 9(004) VALUE ZEROES.
           05 WK-MM-TGA                     PIC 9(002) VALUE ZEROES.
           05 WK-GG-TGA                     PIC 9(002) VALUE ZEROES.
       01  WK-DATA-AMG-TGA-R REDEFINES WK-DATA-AMG-TGA PIC 9(008).

      *----------------------------------------------------------------*
      * AREA TABELLE DB2
      *----------------------------------------------------------------*
           COPY IDBVPOL1.
           COPY IDBVPOG1.
           COPY IDBVADE1.

      *----------------------------------------------------------------*
      *--> AREA TRANCHE DI GARANZIA
      *----------------------------------------------------------------*
       01 WTGA-AREA-TRANCHE.
           04 WTGA-ELE-TRAN-MAX          PIC S9(04) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==WTGA==.
           COPY LCCVTGA1                 REPLACING ==(SF)== BY ==WTGA==.

      *----------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBS1130
      *----------------------------------------------------------------*
           COPY LDBV1131.

      *--  COPY PER SERVIZIO VERIFICA CALCOLI SU DATE
           COPY LCCC0003.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                  PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0127.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0127.
      *----------------------------------------------------------------*
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

           PERFORM S0001-VALORIZZA-DCLGEN
              THRU S0001-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S0001-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-TRCH-GAR
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO WTGA-AREA-TRANCHE
           END-IF.

       S0001-VALORIZZA-DCLGEN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           IF WTGA-DT-ULT-ADEG-PRE-PR-NULL(IVVC0213-IX-TABB)
              = HIGH-VALUES
              PERFORM S1100-LETTURA-POG
                      THRU EX-S1100
           ELSE
              IF WTGA-DT-ULT-ADEG-PRE-PR(IVVC0213-IX-TABB) IS NUMERIC
                IF WTGA-DT-ULT-ADEG-PRE-PR(IVVC0213-IX-TABB) = ZERO
                   PERFORM S1100-LETTURA-POG
                      THRU EX-S1100
                ELSE
                   MOVE WTGA-DT-ULT-ADEG-PRE-PR(IVVC0213-IX-TABB)
                     TO WK-DATA-9
                   PERFORM S1120-CALCOLO-DATA THRU EX-S1120
                END-IF
              END-IF
           END-IF.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *   LETTURA PARAMETRO OGGETTO PER ID-OGG, TP-OGG E COD-PARAMETRO
      *----------------------------------------------------------------*
       S1100-LETTURA-POG.

           INITIALIZE AREA-LDBV1131.

           MOVE IVVC0213-ID-POLIZZA       TO LDBV1131-ID-OGG.
           MOVE 'PO'                      TO LDBV1131-TP-OGG.
           MOVE 'DATARIVALFX$'            TO LDBV1131-COD-PARAM.
           MOVE AREA-LDBV1131             TO IDSV0003-BUFFER-WHERE-COND.
           MOVE 'LDBS1130'                TO WK-CALL-PGM.

      *  --> Tipo operazione
           SET IDSV0003-SELECT            TO TRUE.
      *  --> Tipo livello
           SET IDSV0003-WHERE-CONDITION   TO TRUE.

           CALL WK-CALL-PGM USING IDSV0003 PARAM-OGG

           ON EXCEPTION
              MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-LDBS1130 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER   TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              IF POG-VAL-TXT > '0001'
                 MOVE WTGA-DT-DECOR(IVVC0213-IX-TABB) TO WK-DATA-X
                 MOVE WK-DATA-X(1:4)           TO WK-AAAA-TGA
                 MOVE POG-VAL-TXT(1:2)         TO WK-GG-TGA
                 MOVE POG-VAL-TXT(3:2)         TO WK-MM-TGA
11626            MOVE WK-DATA-AMG-TGA-R     TO WK-DT-CALC
 "               MOVE WK-DT-CALC            TO WK-APPO-DT
 "               PERFORM S10000-CTRL-FINE-MESE
 "                  THRU S10000-CTRL-FINE-MESE-EX
 "               MOVE WK-DT-CALC   TO WK-DATA-AMG-TGA-R
                 IF WK-DATA-AMG-TGA-R > WTGA-DT-DECOR(IVVC0213-IX-TABB)
11626               MOVE WK-DATA-AMG-TGA-R     TO IVVC0213-VAL-STR-O
                 ELSE
                    MOVE WK-DATA-AMG-TGA-R     TO WK-DATA-9
                    PERFORM S1120-CALCOLO-DATA THRU EX-S1120
                 END-IF
              ELSE
                 IF POG-VAL-TXT = '0000'
                    MOVE WTGA-DT-DECOR(IVVC0213-IX-TABB) TO WK-DATA-X
                    MOVE WK-DATA-X(1:4)                  TO WK-AAAA-TGA
                    MOVE WK-DATA-X(5:2)                  TO WK-MM-TGA
                    MOVE WK-DATA-X(7:2)                  TO WK-GG-TGA
                    MOVE WK-DATA-AMG-TGA-R               TO WK-DATA-9
                    PERFORM S1120-CALCOLO-DATA           THRU EX-S1120
                 ELSE
                    IF POG-VAL-TXT = '0001'
                       PERFORM S1130-LETTURA-ADESIONE
                          THRU EX-S1130
                       IF IDSV0003-INVALID-OPER
                          CONTINUE
                       ELSE
                          MOVE WTGA-DT-DECOR(IVVC0213-IX-TABB)
                            TO WK-DATA-X
                          MOVE WK-DATA-X(1:4)         TO WK-AAAA-TGA
                          MOVE WK-DATA-X(5:2)         TO WK-MM-TGA
                          MOVE WK-DATA-X(7:2)         TO WK-GG-TGA
                          MOVE ADE-DT-DECOR           TO WK-DATA-X
                          MOVE WK-DATA-X(1:4)         TO WK-AAAA-POL
                          MOVE WK-DATA-X(5:2)         TO WK-MM-POL
                          MOVE WK-DATA-X(7:2)         TO WK-GG-POL
                          MOVE WK-AAAA-TGA            TO WK-AAAA-POL
11626                     MOVE WK-DATA-AMG-POL-R     TO WK-DT-CALC
 "                        MOVE WK-DT-CALC            TO WK-APPO-DT
 "                        PERFORM S10000-CTRL-FINE-MESE
 "                           THRU S10000-CTRL-FINE-MESE-EX
 "                        MOVE WK-DT-CALC TO  WK-DATA-AMG-POL-R
                          IF WK-DATA-AMG-POL-R >
                             WTGA-DT-DECOR(IVVC0213-IX-TABB)
                             MOVE WK-DATA-AMG-POL-R
                               TO IVVC0213-VAL-STR-O
                          ELSE
                             MOVE WK-DATA-AMG-POL-R     TO WK-DATA-9
                             PERFORM S1120-CALCOLO-DATA THRU EX-S1120
                          END-IF
                       END-IF
                    END-IF
                 END-IF
              END-IF
           ELSE
              IF IDSV0003-NOT-FOUND
                 PERFORM S1110-LETTURA-POLI
                    THRU EX-S1110
                 IF IDSV0003-INVALID-OPER
                    CONTINUE
                 ELSE
                    IF POL-DT-DECOR <= WTGA-DT-DECOR(IVVC0213-IX-TABB)
                       MOVE POL-DT-DECOR             TO WK-DATA-X
                       MOVE WK-DATA-X(1:4)           TO WK-AAAA-POL
                       MOVE WK-DATA-X(5:2)           TO WK-MM-POL
                       MOVE WK-DATA-X(7:2)           TO WK-GG-POL
                       MOVE WTGA-DT-DECOR(IVVC0213-IX-TABB)
                         TO WK-DATA-X
                       MOVE WK-DATA-X(1:4)           TO WK-AAAA-TGA
                       MOVE WK-DATA-X(5:2)           TO WK-MM-TGA
                       MOVE WK-DATA-X(7:2)           TO WK-GG-TGA
                       MOVE WK-AAAA-TGA              TO WK-AAAA-POL
11626                  MOVE WK-DATA-AMG-POL-R     TO WK-DT-CALC
 "                     MOVE WK-DT-CALC            TO WK-APPO-DT
 "                     PERFORM S10000-CTRL-FINE-MESE
 "                        THRU S10000-CTRL-FINE-MESE-EX
 "                     MOVE WK-DT-CALC    TO  WK-DATA-AMG-POL-R
                       IF WK-DATA-AMG-POL-R >
                          WTGA-DT-DECOR(IVVC0213-IX-TABB)
                          MOVE WK-DATA-AMG-POL-R
                            TO IVVC0213-VAL-STR-O
                       ELSE
                          MOVE WK-DATA-AMG-POL-R     TO WK-DATA-9
                          PERFORM S1120-CALCOLO-DATA THRU EX-S1120
                       END-IF
                    END-IF
                 END-IF
              ELSE
                 MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
                 STRING 'CHIAMATA LDBS1130 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING
                 IF IDSV0003-NOT-FOUND
                    SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                 ELSE
                    SET IDSV0003-INVALID-OPER     TO TRUE
                 END-IF
              END-IF
           END-IF.

       EX-S1100.
           EXIT.
      *----------------------------------------------------------------*
      * VALIDAZIONE DATA FINE MESE
      *----------------------------------------------------------------*
      *
11626  S10100-VALIDA-FINE-MESE.
      *
      *
           ADD 1
             TO IX-CICLO.
      *
           MOVE WK-APPO-AAAA
             TO X-AAAA.
           MOVE WK-APPO-MM
             TO X-MM.
           MOVE WK-APPO-GG
             TO X-GG.
      *
           CALL LCCS0004         USING PARAM
                                       X-DATA
           ON EXCEPTION
      *
              MOVE WK-CALL-PGM               TO IDSV0003-COD-SERVIZIO-BE
              STRING 'ERRORE CHIAMATA LCCS0004 ;'
                     PARAM
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
      *
           END-CALL.
      *
           IF PARAM NOT = ZEROES
      *
              SUBTRACT 1
                  FROM WK-APPO-GG
      *
           END-IF.
      *
       S10100-VALIDA-FINE-MESE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *   LETTURA POLIZZA PER ID
      *----------------------------------------------------------------*
       S1110-LETTURA-POLI.

           INITIALIZE POLI.

           MOVE IVVC0213-ID-POLIZZA       TO POL-ID-POLI.

           MOVE 'IDBSPOL0'                TO WK-CALL-PGM.

      *  --> Tipo operazione
           SET IDSV0003-SELECT            TO TRUE.
      *  --> Tipo livello
           SET IDSV0003-ID                TO TRUE.

           CALL WK-CALL-PGM USING IDSV0003 POLI

           ON EXCEPTION
              MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-IDBSPOL0 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER   TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              CONTINUE
           ELSE
              MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA IDBSPOL0 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER     TO TRUE
              END-IF
           END-IF.

       EX-S1110.
           EXIT.

      *----------------------------------------------------------------*
      * AGGIUNGE AD UNA DATA 1 ANNO.
      *----------------------------------------------------------------*
       S1120-CALCOLO-DATA.

           INITIALIZE IO-A2K-LCCC0003.
           MOVE '02'                         TO A2K-FUNZ.
           MOVE '03'                         TO A2K-INFO.
           MOVE 1                            TO A2K-DELTA.
           MOVE 'A'                          TO A2K-TDELTA.
           MOVE WK-DATA-9                    TO A2K-INDATA.
           MOVE '0'                          TO A2K-FISLAV
                                                A2K-INICON.

           PERFORM S1121-CALL-LCCS0003       THRU EX-S1121.

           IF IN-RCODE NOT = '00'
              MOVE WK-CALL-PGM               TO IDSV0003-COD-SERVIZIO-BE
              STRING 'ERRORE CHIAMATA LCCS0003 ;'
                     IN-RCODE
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
           ELSE
              MOVE A2K-OUAMG                 TO IVVC0213-VAL-STR-O
           END-IF.

       EX-S1120.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINE PER IL CALCOLO SULLE DATE
      *----------------------------------------------------------------*
       S1121-CALL-LCCS0003.

           MOVE 'LCCS0003'                 TO WK-CALL-PGM.

           CALL WK-CALL-PGM USING IO-A2K-LCCC0003 IN-RCODE
           ON EXCEPTION
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-LCCS0003 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER    TO TRUE
           END-CALL.

       EX-S1121.
           EXIT.

      *----------------------------------------------------------------*
      *   LETTURA ADESIONE PER ID
      *----------------------------------------------------------------*
       S1130-LETTURA-ADESIONE.

           INITIALIZE ADES.

           MOVE IVVC0213-ID-ADESIONE      TO ADE-ID-ADES.

           MOVE 'IDBSADE0'                TO WK-CALL-PGM.

      *  --> Tipo operazione
           SET IDSV0003-SELECT            TO TRUE.
      *  --> Tipo livello
           SET IDSV0003-ID                TO TRUE.

           CALL WK-CALL-PGM USING IDSV0003 ADES

           ON EXCEPTION
              MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-IDBSADE0 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER   TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              CONTINUE
           ELSE
              MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA IDBSADE0 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER     TO TRUE
              END-IF
           END-IF.

       EX-S1130.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.
      *---------------------------------------------------------------*
11626 *-- validazione della data di fine mese
      *---------------------------------------------------------------*
      *
       S10000-CTRL-FINE-MESE.
      *
           MOVE ZEROES TO IX-CICLO.
      *
           MOVE 1      TO PARAM.
      *
           MOVE 31     TO WK-APPO-GG.
      *
           PERFORM S10100-VALIDA-FINE-MESE
              THRU S10100-VALIDA-FINE-MESE-EX
             UNTIL PARAM = 0
                OR IX-CICLO > 4.
      *
      * --> Se Entro La Quarta Chiamata La Data Non H Stata Calcolata
      * --> Viene Imposta L'uscita Attrraverso Un Indice Di Chiamate
      *
              IF IX-CICLO > 4
      *
                 MOVE LCCS0004         TO IDSV0003-COD-SERVIZIO-BE
                 STRING 'IMPOSSIBILE CALCOLARE LA DATA FINE MESE';
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING
                 SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
      *
              ELSE
      *
                 IF WK-APPO-DT   < WK-DT-CALC
                   MOVE WK-APPO-DT   TO WK-DT-CALC
                 END-IF
      *
              END-IF.
      *
       S10000-CTRL-FINE-MESE-EX.
           EXIT.

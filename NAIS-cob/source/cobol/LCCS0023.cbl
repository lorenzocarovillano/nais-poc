      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA  VER 1.0                          **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LCCS0023.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2008.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *    PROGRAMMA ..... LCCS0023
      *    TIPOLOGIA...... AUTORIZZAZIONI
      *    PROCESSO....... XXXX
      *    FUNZIONE....... XXXX
      *    DESCRIZIONE.... VERIFICA PRESENZA MOVIMENTI FUTURI
      *    PAGINA WEB..... N/A
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                        PIC X(08) VALUE 'LCCS0023'.
FNZF2  01  PGM-IDBSPOL0                  PIC X(08) VALUE 'IDBSPOL0'.
52142  01  PGM-IDBSADE0                  PIC X(08) VALUE 'IDBSADE0'.
52142  01  PGM-IDBSTGA0                  PIC X(08) VALUE 'IDBSTGA0'.
       01  WK-ELE-MOV-MAX                PIC 9(04) VALUE 30.
      *----------------------------------------------------------------*
      *    COPY INPUT PER LA LETTURA IN PTF
      *----------------------------------------------------------------*
       01  WCOM-DATI-INPUT.
           COPY LCCC0007                 REPLACING ==(SF)== BY ==WCOM==.
      *----------------------------------------------------------------*
      *    FLAG
      *----------------------------------------------------------------*
       01  WS-FG-DEFAULT-GRAVITA         PIC X(01).
           88 WS-DEF-BLOCCANTE           VALUE 'B'.
           88 WS-DEF-AMMISSIBILE         VALUE 'A'.

       01  WS-FG-MOVI-ANN                PIC X(01).
           88 SI-MOVI-ANN                VALUE 'S'.
           88 NO-MOVI-ANN                VALUE 'N'.

11992  01  WS-FG-ANN-VAG                 PIC X(01).
11992      88 ANNULLO-VERS-SI            VALUE 'S'.
11992      88 ANNULLO-VERS-NO            VALUE 'N'.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-TAB-MOV                 PIC S9(04) COMP.
      *----------------------------------------------------------------*
      *    TIPOLOGICHE DI PORTAFOGLIO
      *----------------------------------------------------------------*
           COPY LCCVXOG0.
           COPY LCCVL510.
11992      COPY LCCVXMV0.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-VARIABILI.
           03 WK-TABELLA                 PIC X(20).
           03 WS-TP-GRAVITA-MOVIMENTO    PIC X(02).
11992      03 WK-DT-EFF                  PIC S9(8).

      *--  AREA D'APPOGGIO PER MOVIMENTO
       01  WMOV-AREA-MOVIMENTO.
           03 WMOV-ELE-MOVI-MAX          PIC S9(04) COMP VALUE ZEROES.
           03 WMOV-TAB-MOVI              OCCURS 30.
              COPY LCCVMOV1              REPLACING ==(SF)== BY ==WMOV==.
      ******************************************************************
      *    COPY DISPATCHER
      ******************************************************************
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      *--> COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *--> COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *----------------------------------------------------------------*
      *    COPY AREA MOVIMENTO - AMMISIBILITA FUNZIONE FUNZIONE
      *----------------------------------------------------------------*
           COPY IDBVMOV1.
FNZF2      COPY IDBVPOL1.
52142      COPY IDBVADE1.
52142      COPY IDBVTGA1.
           COPY IDBVL051.
      *----------------------------------------------------------------*
      *    COPY LDBS
      *----------------------------------------------------------------*
           COPY LDBV2441.
FNZF2      COPY LDBVG781.
      *----------------------------------------------------------------*
      *    COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
           COPY IEAV9903.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01   AREA-IDSV0001.
            COPY IDSV0001.
      *----------------------------------------------------------------*
      *    AREA DI COMUNICAZIONE
      *----------------------------------------------------------------*
      *--  AREA COMUNE.
       01  WCOM-AREA-STATI.
           COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.
      *--  AREA INPUT/OUTPUT
       01  AREA-IO-LCCS0023.
           COPY LCCC0023.

      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                AREA-IO-LCCS0023.
      *---------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI                                         *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

      *--> INIZIALIZZAZIONE AREA D'APPOGGIO PER MOVIMENTI E AREA DI PAG.

           INITIALIZE         IX-INDICI
                              WK-VARIABILI.

           MOVE ZEROES     TO LCCC0023-ELE-MOV-FUTURI-MAX
                              WMOV-ELE-MOVI-MAX.
           MOVE 'N'        TO LCCC0023-PRE-GRAV-BLOCCANTE
                              LCCC0023-PRE-GRAV-ANNULLABILE.

           PERFORM VARYING IX-TAB-MOV FROM 1 BY 1
             UNTIL IX-TAB-MOV > WK-ELE-MOV-MAX

      *-->   INIZIALIZZAZIONE CAMPI NULL
             MOVE HIGH-VALUES TO WMOV-ID-OGG-NULL(IX-TAB-MOV)
                                 LCCC0023-ID-OGG-NULL(IX-TAB-MOV)
                                 WMOV-IB-OGG-NULL(IX-TAB-MOV)
                                 LCCC0023-IB-OGG-NULL(IX-TAB-MOV)
                                 WMOV-IB-MOVI-NULL(IX-TAB-MOV)
                                 LCCC0023-IB-MOVI-NULL(IX-TAB-MOV)
                                 WMOV-TP-OGG-NULL(IX-TAB-MOV)
                                 LCCC0023-TP-OGG-NULL(IX-TAB-MOV)
                                 WMOV-ID-RICH-NULL(IX-TAB-MOV)
                                 LCCC0023-ID-RICH-NULL(IX-TAB-MOV)
                                 WMOV-TP-MOVI-NULL(IX-TAB-MOV)
                                 LCCC0023-TP-MOVI-NULL(IX-TAB-MOV)
                                 WMOV-ID-MOVI-ANN-NULL(IX-TAB-MOV)
                                 LCCC0023-ID-MOVI-ANN-NULL(IX-TAB-MOV)

      *-->   INIZIALIZZAZIONE CAMPI NUMERICI NOT NULL
             MOVE ZEROES TO WMOV-ID-MOVI(IX-TAB-MOV)
                            LCCC0023-ID-MOVI(IX-TAB-MOV)
                            WMOV-COD-COMP-ANIA(IX-TAB-MOV)
                            WMOV-DT-EFF(IX-TAB-MOV)
                            WMOV-DT-EFF(IX-TAB-MOV)
                            WMOV-DS-VER(IX-TAB-MOV)
                            WMOV-DS-TS-CPTZ(IX-TAB-MOV)

      *-->   INIZIALIZZAZIONE CAMPI ALFANUMERICI
             MOVE SPACES TO WMOV-DS-OPER-SQL(IX-TAB-MOV)
                            WMOV-DS-UTENTE(IX-TAB-MOV)
                            WMOV-DS-STATO-ELAB(IX-TAB-MOV)

           END-PERFORM.

           PERFORM S0005-CTRL-DATI-INPUT
              THRU EX-S0005.

11992      MOVE IDSV0001-TIPO-MOVIMENTO TO WS-MOVIMENTO.
11992 *
11992      IF ANN-VERSAM-AGGIUNTIVO
11992         SET ANNULLO-VERS-SI TO TRUE
11992      ELSE
11992         SET ANNULLO-VERS-NO TO TRUE
11992      END-IF.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLI INPUT
      *----------------------------------------------------------------*
       S0005-CTRL-DATI-INPUT.

      *--  il campo ID-OGGETTO deve essere un numerico
           IF LCCC0023-ID-OGG-PTF NOT NUMERIC
              MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0005-CTRL-DATI-INPUT' TO IEAI9901-LABEL-ERR
              MOVE '005076'                TO IEAI9901-COD-ERRORE
              MOVE 'LCCC0023-ID-OGG-PTF'   TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF

      *--  il campo ID-OGGETTO deve essere un numerico > zero
           IF IDSV0001-ESITO-OK
              IF LCCC0023-ID-OGG-PTF NOT > ZERO
                 MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0005-CTRL-DATI-INPUT'
                                            TO IEAI9901-LABEL-ERR
                 MOVE '005018'              TO IEAI9901-COD-ERRORE
                 MOVE 'LCCC0023-ID-OGG-PTF' TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF

      *--  il campo TP-OGGETTO deve essere diverso da spaces e deve
      *--  essere un valore compreso nei valori di dominio
           IF IDSV0001-ESITO-OK
              IF LCCC0023-TP-OGG-PTF = SPACES
                                    OR HIGH-VALUE OR LOW-VALUE
                 MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0005-CTRL-DATI-INPUT'
                                            TO IEAI9901-LABEL-ERR
                 MOVE '005007'              TO IEAI9901-COD-ERRORE
                 MOVE 'LCCC0023-TP-OGG-PTF' TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF

           IF IDSV0001-ESITO-OK
              MOVE LCCC0023-TP-OGG-PTF      TO WS-TP-OGG
              IF  (NOT POLIZZA) AND (NOT ADESIONE)
              AND (NOT TRANCHE) AND (NOT PREVENTIVO)
                 MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0005-CTRL-DATI-INPUT'
                                            TO IEAI9901-LABEL-ERR
                 MOVE '005018'              TO IEAI9901-COD-ERRORE
                 MOVE 'LCCC0023-TP-OGG-PTF' TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

       EX-S0005.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> LETTURA MOVIMENTI FUTURI PER :CODICE_COMPAGNIA, TIPO_OGGETTO,
      *--> ID_OGGETTO, STATO_ELABORAZIONE = 'concluso'
      *--> e DATA EFFETTO > data effetto corrente;
           PERFORM S1100-LETTURA-MOV-FUTURI
              THRU EX-S1100.

52142      INITIALIZE POLI.
52142      EVALUATE LCCC0023-TP-OGG-PTF
52142          WHEN 'PO'
52142               MOVE LCCC0023-ID-OGG-PTF    TO POL-ID-POLI
52142               PERFORM S1110-LEGGI-POLI
52142                  THRU EX-S1110
52142          WHEN 'AD'
52142               PERFORM S1111-LEGGI-ADES
52142                  THRU EX-S1111
52142               IF IDSV0001-ESITO-OK
52142                  PERFORM S1110-LEGGI-POLI
52142                     THRU EX-S1110
52142               END-IF
52142          WHEN 'TG'
52142               PERFORM S1112-LEGGI-TRCH
52142                  THRU EX-S1112
52142               IF IDSV0001-ESITO-OK
52142                  PERFORM S1110-LEGGI-POLI
52142                     THRU EX-S1110
52142               END-IF
52142          WHEN OTHER
52142               CONTINUE
52142      END-EVALUATE.

FNZF2 *    IF IDSV0001-ESITO-OK
FNZF2 *       PERFORM S1110-LEGGI-POLI
FNZF2 *          THRU EX-S1110
FNZF2 *    END-IF

           IF IDSV0001-ESITO-OK
      *-->    SE WMOV-ELE-MOVI-MAX VALE ZERO SIGNIFICA CHE NON CI SONO
      *-->    MOVIMENTI FUTURI
              IF WMOV-ELE-MOVI-MAX NOT = ZERO
      *-->       CONTROLLIAMO IL DEFAULT SE E' PRESENTE
FNZF2            IF POL-FL-POLI-IFP = 'P'
FNZF2               PERFORM S1210-GESTIONE-MOV-FUTURI-FNZ
FNZF2                  THRU EX-S1210
FNZF2            ELSE
                    PERFORM S1200-GESTIONE-MOV-FUTURI-DEF
                       THRU EX-S1200
FNZF2            END-IF
              END-IF
           END-IF.

      *--> RICERCA I MOVIMENTI FUTURI RECUPERATI NELLA TABELLA
      *--> AMMISSIBILITA' FUNZIONE FUNZIONE
11992      PERFORM VARYING IX-TAB-MOV FROM 1 BY 1
11992        UNTIL IX-TAB-MOV > WMOV-ELE-MOVI-MAX
11992           OR IDSV0001-ESITO-KO
11992 *
11992           IF ANNULLO-VERS-SI
11992              IF WMOV-DT-EFF(IX-TAB-MOV) >= WK-DT-EFF
FNZF2                 IF POL-FL-POLI-IFP = 'P'
FNZF2                    PERFORM S1310-GESTIONE-MOV-FNZ
FNZF2                       THRU EX-S1310
FNZF2                 ELSE
11992                    PERFORM S1300-GESTIONE-MOV-FUTURI
11992                       THRU EX-S1300
FNZF2                 END-IF
11992              END-IF
11992           ELSE
                   PERFORM S1300-GESTIONE-MOV-FUTURI
                      THRU EX-S1300
11992           END-IF
11992 *
11992      END-PERFORM.

       EX-S1000.
           EXIT.
FNZF2 *----------------------------------------------------------------*
FNZF2 *    LETTURA TABELLA MOVIMENTO
FNZF2 *----------------------------------------------------------------*
FNZF2  S1110-LEGGI-POLI.

      *-- Inizializzazione area di lettura
52142 *    INITIALIZE POLI.

      *-- La data effetto/competenza viene sempre valorizzata
           MOVE ZEROES                 TO IDSI0011-DATA-INIZIO-EFFETTO
                                          IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZEROES                 TO IDSI0011-DATA-COMPETENZA.

      *-- Setto i valori di output per il controllo degli errori
           SET IDSO0011-SUCCESSFUL-RC  TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL TO TRUE.

      *-- Impostazione codice di ricerca
52142 *    MOVE MOV-ID-OGG             TO POL-ID-POLI.

      *-- Parametri per l'estrazione
           SET IDSI0011-TRATT-X-COMPETENZA
                                       TO TRUE.
           SET IDSI0011-ID             TO TRUE.
           SET IDSI0011-SELECT         TO TRUE.

      *-- Passaggio dei campi
           MOVE 'POLI'                 TO IDSI0011-CODICE-STR-DATO.
           MOVE SPACES                 TO IDSI0011-BUFFER-DATI.
           MOVE POLI                   TO IDSI0011-BUFFER-DATI.

      *-- Chiamata al dispatcher
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

      *-- Controllo il risultato
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--         OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI TO POLI

                  WHEN OTHER
      *--         NESSUNA OCCORRENZA TROVATA
                     MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1110-LEGGI-POLI'
                                        TO IEAI9901-LABEL-ERR
                     MOVE '005069'      TO IEAI9901-COD-ERRORE
                     MOVE PGM-IDBSPOL0  TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *--    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1110-LEGGI-POLI'      TO IEAI9901-LABEL-ERR
              MOVE '005016'                TO IEAI9901-COD-ERRORE
              STRING PGM-IDBSPOL0         ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

FNZF2  EX-S1110.
FNZF2      EXIT.
      *----------------------------------------------------------------*
      *    LETTURA TABELLA ADESIONE
      *----------------------------------------------------------------*
52142  S1111-LEGGI-ADES.

      *-- Inizializzazione area di lettura
           INITIALIZE ADES.

      *-- La data effetto/competenza viene sempre valorizzata
           MOVE ZEROES                 TO IDSI0011-DATA-INIZIO-EFFETTO
                                          IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZEROES                 TO IDSI0011-DATA-COMPETENZA.

      *-- Setto i valori di output per il controllo degli errori
           SET IDSO0011-SUCCESSFUL-RC  TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL TO TRUE.

      *-- Impostazione codice di ricerca
           MOVE LCCC0023-ID-OGG-PTF    TO ADE-ID-ADES.

      *-- Parametri per l'estrazione
           SET IDSI0011-TRATT-X-COMPETENZA
                                       TO TRUE.
           SET IDSI0011-ID             TO TRUE.
           SET IDSI0011-SELECT         TO TRUE.

      *-- Passaggio dei campi
           MOVE 'ADES'                 TO IDSI0011-CODICE-STR-DATO.
           MOVE SPACES                 TO IDSI0011-BUFFER-DATI.
           MOVE ADES                   TO IDSI0011-BUFFER-DATI.

      *-- Chiamata al dispatcher
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

      *-- Controllo il risultato
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--         OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI TO ADES
                     MOVE ADE-ID-POLI    TO POL-ID-POLI

                  WHEN OTHER
      *--         NESSUNA OCCORRENZA TROVATA
                     MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1111-LEGGI-ADES'
                                        TO IEAI9901-LABEL-ERR
                     MOVE '005069'      TO IEAI9901-COD-ERRORE
                     MOVE PGM-IDBSADE0  TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *--    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1111-LEGGI-ADES'      TO IEAI9901-LABEL-ERR
              MOVE '005016'                TO IEAI9901-COD-ERRORE
              STRING PGM-IDBSADE0         ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1111.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA TABELLA TRANCHE
      *----------------------------------------------------------------*
52142  S1112-LEGGI-TRCH.

      *-- Inizializzazione area di lettura
           INITIALIZE TRCH-DI-GAR.

      *-- La data effetto/competenza viene sempre valorizzata
           MOVE ZEROES                 TO IDSI0011-DATA-INIZIO-EFFETTO
                                          IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZEROES                 TO IDSI0011-DATA-COMPETENZA.

      *-- Setto i valori di output per il controllo degli errori
           SET IDSO0011-SUCCESSFUL-RC  TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL TO TRUE.

      *-- Impostazione codice di ricerca
           MOVE LCCC0023-ID-OGG-PTF    TO TGA-ID-TRCH-DI-GAR.

      *-- Parametri per l'estrazione
           SET IDSI0011-TRATT-X-COMPETENZA
                                       TO TRUE.
           SET IDSI0011-ID             TO TRUE.
           SET IDSI0011-SELECT         TO TRUE.

      *-- Passaggio dei campi
           MOVE 'TRCH-DI-GAR'          TO IDSI0011-CODICE-STR-DATO.
           MOVE SPACES                 TO IDSI0011-BUFFER-DATI.
           MOVE TRCH-DI-GAR            TO IDSI0011-BUFFER-DATI.

      *-- Chiamata al dispatcher
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

      *-- Controllo il risultato
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--         OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI TO TRCH-DI-GAR
                     MOVE TGA-ID-POLI   TO POL-ID-POLI

                  WHEN OTHER
      *--         NESSUNA OCCORRENZA TROVATA
                     MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1112-LEGGI-TRCH'
                                        TO IEAI9901-LABEL-ERR
                     MOVE '005069'      TO IEAI9901-COD-ERRORE
                     MOVE PGM-IDBSTGA0  TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *--    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1112-LEGGI-TRCH'      TO IEAI9901-LABEL-ERR
              MOVE '005016'                TO IEAI9901-COD-ERRORE
              STRING PGM-IDBSTGA0         ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1112.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA TABELLA MOVIMENTO
      *----------------------------------------------------------------*
       S1100-LETTURA-MOV-FUTURI.

           INITIALIZE IDSI0011-BUFFER-DATI.

           SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.
           SET  IDSV0001-ESITO-OK        TO TRUE.
           SET  WCOM-OVERFLOW-NO         TO TRUE.
           SET IDSI0011-TRATT-DEFAULT    TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSI0011-FETCH-FIRST      TO TRUE.

           MOVE 'LDBS2430'               TO IDSI0011-CODICE-STR-DATO
                                            WK-TABELLA.
           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                         TO MOV-COD-COMP-ANIA
           MOVE IDSV0001-DATA-EFFETTO    TO MOV-DT-EFF
           MOVE LCCC0023-ID-OGG-PTF      TO MOV-ID-OGG
           MOVE LCCC0023-TP-OGG-PTF      TO MOV-TP-OGG
           MOVE MOVI                     TO IDSI0011-BUFFER-DATI.

           PERFORM S1150-FETCH-DISPATCHER
              THRU EX-S1150.

       EX-S1100.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA TABELLA MOVIMENTO
      *----------------------------------------------------------------*
       S1140-PREP-LET-MOV-FUTURI.

           SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.

           SET IDSI0011-TRATT-DEFAULT    TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSI0011-FETCH-NEXT       TO TRUE.

           MOVE 'LDBS2430'               TO IDSI0011-CODICE-STR-DATO
                                            WK-TABELLA.

       EX-S1140.
           EXIT.
      *----------------------------------------------------------------*
      *   CALL DISPATCHER PER FETCH
      *----------------------------------------------------------------*
       S1150-FETCH-DISPATCHER.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR NOT IDSV0001-ESITO-OK
                      OR WCOM-OVERFLOW-YES

               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *-->            NON CI SONO MOVIMENTI FUTURI
                           IF IDSI0011-FETCH-FIRST
                              MOVE ZEROES
                                TO LCCC0023-ELE-MOV-FUTURI-MAX
                           END-IF

                      WHEN IDSO0011-SUCCESSFUL-SQL
      *-->            OPERAZIONE ESEGUITA CORRETTAMENTE
                           MOVE IDSO0011-BUFFER-DATI     TO MOVI
      *--> i movimenti futuri che gia sono stati annullati devono
      *--> essere scartati
                           IF MOV-ID-MOVI-ANN-NULL   = HIGH-VALUE
19797 * Prima di salvare la movi verifico se e' un movimento di annullo
19797                         SET NO-MOVI-ANN TO TRUE
19797                         PERFORM S1160-VERIF-MOVI-ANN
19797                            THRU EX-S1160
11717 * La riga seguente h asteriscata per risolvere la sir BNL
11717 * FCTVI00011717. Per ulteriori chiarimenti rivolgersi a
11717 * Nicola Esposito - Espedito Gioia
11717 *                    AND MOV-ID-MOVI-COLLG-NULL = HIGH-VALUE
19797                         IF NO-MOVI-ANN
                                 ADD  1  TO WMOV-ELE-MOVI-MAX
      *-->  Se il contatore di lettura h maggiore della capienza
      *-->  prevista per la TAB.MOVIMENTI FUTURI dell'area di
      *-->  pagina allora siamo in overflow
                                 IF WMOV-ELE-MOVI-MAX > WK-ELE-MOV-MAX
                                    SET WCOM-OVERFLOW-YES   TO TRUE
                                    COMPUTE WMOV-ELE-MOVI-MAX =
                                            WMOV-ELE-MOVI-MAX - 1
37045                            PERFORM S11860-CLOSE-CURSOR
37045                              THRU S11860-CLOSE-CURSOR-EX
                                 ELSE
                                    MOVE MOVI
                                      TO WMOV-DATI(WMOV-ELE-MOVI-MAX)
11992                               MOVE MOV-TP-MOVI
11992                                 TO WS-MOVIMENTO
11992                               IF VERSAM-AGGIUNTIVO
11992                               AND ANNULLO-VERS-SI
11992                                  MOVE MOV-DT-EFF
11992                                    TO WK-DT-EFF
11992                               END-IF
                                 END-IF
                              END-IF
                           END-IF
                           IF IDSV0001-ESITO-OK
                              PERFORM S1140-PREP-LET-MOV-FUTURI
                                 THRU EX-S1140
                           END-IF

                  END-EVALUATE
               ELSE
      *-->        ERRORE DISPATCHER
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S1150-FETCH-DISPATCHER'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005016'
                    TO IEAI9901-COD-ERRORE
                  STRING WK-TABELLA            ';'
                         IDSO0011-RETURN-CODE  ';'
                         IDSO0011-SQLCODE
                         DELIMITED BY SIZE INTO
                         IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

       EX-S1150.
           EXIT.
       S11860-CLOSE-CURSOR.
      *
      *
           SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.

           SET IDSI0011-TRATT-DEFAULT    TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.

           MOVE 'LDBS2430'               TO IDSI0011-CODICE-STR-DATO
                                            WK-TABELLA.

           SET IDSI0011-CLOSE-CURSOR
             TO TRUE.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
                     CONTINUE
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
                     CONTINUE
      *
                  WHEN OTHER
      *
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S11860-CLOSE-CURSOR'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING WK-TABELLA   ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
      *
           ELSE
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S11860-CLOSE-CURSOR'
                TO IEAI9901-LABEL-ERR
              MOVE '005056'
                TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA  ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
       S11860-CLOSE-CURSOR-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICO ESISTENZA MOVIMENTO DI ANNULLO
      *----------------------------------------------------------------*
       S1160-VERIF-MOVI-ANN.

           INITIALIZE IDSI0011-BUFFER-DATI.

           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
           SET IDSV0001-ESITO-OK         TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSI0011-SELECT           TO TRUE.

           MOVE 'LDBS2440'               TO IDSI0011-CODICE-STR-DATO
                                            WK-TABELLA.
           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                         TO L05-COD-COMP-ANIA.
           MOVE MOV-TP-MOVI              TO L05-TP-MOVI-ESEC
                                            L05-TP-MOVI-RIFTO.
           MOVE 'AN'                     TO LDBV2441-GRAV-FUNZ-FUNZ-1.
           MOVE LDBV2441                 TO IDSI0011-BUFFER-WHERE-COND.
           MOVE AMMB-FUNZ-FUNZ           TO IDSI0011-BUFFER-DATI.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       SET SI-MOVI-ANN   TO TRUE

                  WHEN IDSO0011-NOT-FOUND
      *-->        CHIAVE NON TROVATA
                       SET NO-MOVI-ANN   TO TRUE

                  WHEN OTHER
      *-->        ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $ RC=$
      *-->        SQLCODE=$
                       MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1160-VERIF-MOVI-ANN'
                                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'     TO IEAI9901-COD-ERRORE
                       STRING WK-TABELLA           ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE INTO
                              IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *->     GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1160-VERIF-MOVI-ANN'
                                         TO IEAI9901-LABEL-ERR
              MOVE '005016'              TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO
                     IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1160.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE MOVIMENTI FUTURI DEFAULT
      *----------------------------------------------------------------*
       S1200-GESTIONE-MOV-FUTURI-DEF.
           INITIALIZE IDSI0011-BUFFER-DATI
16326                 LDBVG781.
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
           SET IDSV0001-ESITO-OK         TO TRUE.
           SET WCOM-OVERFLOW-NO          TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSI0011-SELECT           TO TRUE.

           MOVE 'LDBSG780'               TO IDSI0011-CODICE-STR-DATO
                                            WK-TABELLA.
           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                         TO L05-COD-COMP-ANIA.
           MOVE IDSV0001-TIPO-MOVIMENTO  TO L05-TP-MOVI-ESEC.
           MOVE ZEROES                   TO L05-TP-MOVI-RIFTO.
16326      MOVE 'G'                      TO LDBVG781-FL-POLI-IFP
           MOVE 'BL'                     TO LDBVG781-GRAV-FUNZ-FUNZ-1.
           MOVE 'AM'                     TO LDBVG781-GRAV-FUNZ-FUNZ-2.
           MOVE LDBVG781                 TO IDSI0011-BUFFER-WHERE-COND.
           MOVE AMMB-FUNZ-FUNZ           TO IDSI0011-BUFFER-DATI.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI
                         TO AMMB-FUNZ-FUNZ

                       MOVE L05-GRAV-FUNZ-FUNZ        TO WS-TP-ANNULLO
                       EVALUATE TRUE
                           WHEN TA-BLOCCANTE
                              SET WS-DEF-BLOCCANTE    TO TRUE
                           WHEN TA-AMMISSIBILE
                              SET WS-DEF-AMMISSIBILE  TO TRUE
                       END-EVALUATE

                  WHEN IDSO0011-NOT-FOUND
      *-->        CHIAVE NON TROVATA
                       SET WS-DEF-AMMISSIBILE         TO TRUE

                  WHEN OTHER
      *-->        ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $ RC=$
      *-->        SQLCODE=$
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1200-GESTIONE-MOV-FUTURI-DEF'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING WK-TABELLA           ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE INTO
                              IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *->     GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1200-GESTIONE-MOV-FUTURI-DEF'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO
                     IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1200.
           EXIT.
FNZF2 *----------------------------------------------------------------*
FNZF2 *    GESTIONE MOVIMENTI FUTURI FNZ
FNZF2 *----------------------------------------------------------------*
FNZF2  S1210-GESTIONE-MOV-FUTURI-FNZ.

           INITIALIZE IDSI0011-BUFFER-DATI.

           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
           SET IDSV0001-ESITO-OK         TO TRUE.
           SET WCOM-OVERFLOW-NO          TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSI0011-SELECT           TO TRUE.

           MOVE 'LDBSG780'               TO IDSI0011-CODICE-STR-DATO
                                            WK-TABELLA.
           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                         TO L05-COD-COMP-ANIA.
           MOVE IDSV0001-TIPO-MOVIMENTO  TO L05-TP-MOVI-ESEC.
           MOVE ZEROES                   TO L05-TP-MOVI-RIFTO.
           MOVE 'P'                      TO L05-FL-POLI-IFP.
           MOVE 'BL'                     TO LDBVG781-GRAV-FUNZ-FUNZ-1.
           MOVE 'AM'                     TO LDBVG781-GRAV-FUNZ-FUNZ-2.
           MOVE LDBVG781                 TO IDSI0011-BUFFER-WHERE-COND.
           MOVE AMMB-FUNZ-FUNZ           TO IDSI0011-BUFFER-DATI.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI
                         TO AMMB-FUNZ-FUNZ

                       MOVE L05-GRAV-FUNZ-FUNZ        TO WS-TP-ANNULLO
                       EVALUATE TRUE
                           WHEN TA-BLOCCANTE
                              SET WS-DEF-BLOCCANTE    TO TRUE
                           WHEN TA-AMMISSIBILE
                              SET WS-DEF-AMMISSIBILE  TO TRUE
                       END-EVALUATE

                  WHEN IDSO0011-NOT-FOUND
      *-->        CHIAVE NON TROVATA
                       PERFORM S1200-GESTIONE-MOV-FUTURI-DEF
                          THRU EX-S1200

                  WHEN OTHER
      *-->        ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $ RC=$
      *-->        SQLCODE=$
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1210-GESTIONE-MOV-FUTURI-FNZ'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING WK-TABELLA           ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE INTO
                              IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *->     GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1210-GESTIONE-MOV-FUTURI-FNZ'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO
                     IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

FNZF2  EX-S1210.
FNZF2      EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE MOVIMENTI FUTURI
      *----------------------------------------------------------------*
       S1300-GESTIONE-MOV-FUTURI.

           INITIALIZE IDSI0011-BUFFER-DATI.

           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
           SET IDSV0001-ESITO-OK         TO TRUE.
           SET WCOM-OVERFLOW-NO          TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSI0011-SELECT           TO TRUE.

           MOVE 'LDBSG780'               TO IDSI0011-CODICE-STR-DATO
                                            WK-TABELLA.
           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                         TO L05-COD-COMP-ANIA.
           MOVE IDSV0001-TIPO-MOVIMENTO  TO L05-TP-MOVI-ESEC.
           MOVE WMOV-TP-MOVI(IX-TAB-MOV) TO L05-TP-MOVI-RIFTO.
FNZF2      MOVE 'G'                      TO L05-FL-POLI-IFP.
           MOVE 'BL'                     TO LDBVG781-GRAV-FUNZ-FUNZ-1.
           MOVE 'AM'                     TO LDBVG781-GRAV-FUNZ-FUNZ-2.
           MOVE LDBVG781                 TO IDSI0011-BUFFER-WHERE-COND.
           MOVE AMMB-FUNZ-FUNZ           TO IDSI0011-BUFFER-DATI.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI
                         TO AMMB-FUNZ-FUNZ

                      MOVE L05-GRAV-FUNZ-FUNZ
                        TO WS-TP-GRAVITA-MOVIMENTO
                       PERFORM S1350-VALORIZZA-PAG
                          THRU EX-S1350

                  WHEN IDSO0011-NOT-FOUND
      *-->        CHIAVE NON TROVATA
                       EVALUATE TRUE
                           WHEN WS-DEF-BLOCCANTE
                              SET TA-BLOCCANTE    TO TRUE
                           WHEN WS-DEF-AMMISSIBILE
                              SET TA-AMMISSIBILE  TO TRUE
                       END-EVALUATE

                      MOVE WS-TP-ANNULLO
                        TO WS-TP-GRAVITA-MOVIMENTO
                       PERFORM S1350-VALORIZZA-PAG
                          THRU EX-S1350

                  WHEN OTHER
      *-->        ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $ RC=$
      *-->        SQLCODE=$
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1300-GESTIONE-MOV-FUTURI'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING WK-TABELLA           ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE INTO
                              IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *->     GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1300-GESTIONE-MOV-FUTURI'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO
                     IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S1300.
           EXIT.
FNZF2 *----------------------------------------------------------------*
FNZF2 *    GESTIONE MOVIMENTI FUTURI
FNZF2 *----------------------------------------------------------------*
FNZF2  S1310-GESTIONE-MOV-FNZ.

           INITIALIZE IDSI0011-BUFFER-DATI.

           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
           SET IDSV0001-ESITO-OK         TO TRUE.
           SET WCOM-OVERFLOW-NO          TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-WHERE-CONDITION  TO TRUE.
           SET IDSI0011-SELECT           TO TRUE.

           MOVE 'LDBSG780'               TO IDSI0011-CODICE-STR-DATO
                                            WK-TABELLA.
           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                         TO L05-COD-COMP-ANIA.
           MOVE IDSV0001-TIPO-MOVIMENTO  TO L05-TP-MOVI-ESEC.
           MOVE WMOV-TP-MOVI(IX-TAB-MOV) TO L05-TP-MOVI-RIFTO.
           MOVE 'P'                      TO L05-FL-POLI-IFP.
           MOVE 'BL'                     TO LDBVG781-GRAV-FUNZ-FUNZ-1.
           MOVE 'AM'                     TO LDBVG781-GRAV-FUNZ-FUNZ-2.
           MOVE LDBVG781                 TO IDSI0011-BUFFER-WHERE-COND.
           MOVE AMMB-FUNZ-FUNZ           TO IDSI0011-BUFFER-DATI.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI
                         TO AMMB-FUNZ-FUNZ

                      MOVE L05-GRAV-FUNZ-FUNZ
                        TO WS-TP-GRAVITA-MOVIMENTO
                       PERFORM S1350-VALORIZZA-PAG
                          THRU EX-S1350

                  WHEN IDSO0011-NOT-FOUND
      *-->        CHIAVE NON TROVATA
                       PERFORM S1300-GESTIONE-MOV-FUTURI
                          THRU EX-S1300

                  WHEN OTHER
      *-->        ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $ RC=$
      *-->        SQLCODE=$
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1310-GESTIONE-MOV-FNZ'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING WK-TABELLA           ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE INTO
                              IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *->     GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1310-GESTIONE-MOV-FNZ'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO
                     IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

FNZF2  EX-S1310.
FNZF2      EXIT.
      *----------------------------------------------------------------*
      *    SALVATAGGIO DEL MOVIMENTO NELL'AREA DI INTERFACCIA
      *----------------------------------------------------------------*
       S1350-VALORIZZA-PAG.

           ADD  1
             TO LCCC0023-ELE-MOV-FUTURI-MAX
           MOVE WMOV-ID-MOVI(IX-TAB-MOV)
             TO LCCC0023-ID-MOVI
               (LCCC0023-ELE-MOV-FUTURI-MAX)
           MOVE WMOV-ID-OGG(IX-TAB-MOV)
             TO LCCC0023-ID-OGG
               (LCCC0023-ELE-MOV-FUTURI-MAX)

           IF WMOV-IB-OGG-NULL(IX-TAB-MOV) = HIGH-VALUE
              MOVE WMOV-IB-OGG-NULL(IX-TAB-MOV)
                TO LCCC0023-IB-OGG-NULL
                  (LCCC0023-ELE-MOV-FUTURI-MAX)
           ELSE
              MOVE WMOV-IB-OGG-NULL(IX-TAB-MOV)
                TO LCCC0023-IB-OGG
                  (LCCC0023-ELE-MOV-FUTURI-MAX)
           END-IF

           IF WMOV-IB-MOVI-NULL(IX-TAB-MOV) = HIGH-VALUE
              MOVE WMOV-IB-MOVI-NULL(IX-TAB-MOV)
                TO LCCC0023-IB-MOVI-NULL
                  (LCCC0023-ELE-MOV-FUTURI-MAX)
           ELSE
              MOVE WMOV-IB-MOVI(IX-TAB-MOV)
                TO LCCC0023-IB-MOVI
                  (LCCC0023-ELE-MOV-FUTURI-MAX)
           END-IF

           IF WMOV-TP-OGG-NULL(IX-TAB-MOV) = HIGH-VALUE
              MOVE WMOV-TP-OGG-NULL(IX-TAB-MOV)
                TO LCCC0023-TP-OGG-NULL
                  (LCCC0023-ELE-MOV-FUTURI-MAX)
           ELSE
              MOVE WMOV-TP-OGG(IX-TAB-MOV)
                TO LCCC0023-TP-OGG
                  (LCCC0023-ELE-MOV-FUTURI-MAX)
           END-IF

           IF WMOV-ID-RICH-NULL(IX-TAB-MOV) = HIGH-VALUE
              MOVE WMOV-ID-RICH-NULL(IX-TAB-MOV)
                TO LCCC0023-ID-RICH-NULL
                  (LCCC0023-ELE-MOV-FUTURI-MAX)
           ELSE
              MOVE WMOV-ID-RICH(IX-TAB-MOV)
                TO LCCC0023-ID-RICH
                  (LCCC0023-ELE-MOV-FUTURI-MAX)
           END-IF

           IF WMOV-TP-MOVI-NULL(IX-TAB-MOV) = HIGH-VALUE
              MOVE WMOV-TP-MOVI-NULL(IX-TAB-MOV)
                TO LCCC0023-TP-MOVI-NULL
                  (LCCC0023-ELE-MOV-FUTURI-MAX)
           ELSE
              MOVE WMOV-TP-MOVI(IX-TAB-MOV)
                TO LCCC0023-TP-MOVI
                  (LCCC0023-ELE-MOV-FUTURI-MAX)
           END-IF

           MOVE WMOV-DT-EFF(IX-TAB-MOV)
             TO LCCC0023-DT-EFF
               (LCCC0023-ELE-MOV-FUTURI-MAX)

           IF WMOV-ID-MOVI-ANN-NULL(IX-TAB-MOV) = HIGH-VALUE
              MOVE WMOV-ID-MOVI-ANN-NULL(IX-TAB-MOV)
                TO LCCC0023-ID-MOVI-ANN-NULL
                  (LCCC0023-ELE-MOV-FUTURI-MAX)
           ELSE
              MOVE WMOV-ID-MOVI-ANN(IX-TAB-MOV)
                TO LCCC0023-ID-MOVI-ANN
                  (LCCC0023-ELE-MOV-FUTURI-MAX)
           END-IF

           MOVE WMOV-DS-TS-CPTZ(IX-TAB-MOV)
             TO LCCC0023-DS-TS-CPTZ
               (LCCC0023-ELE-MOV-FUTURI-MAX)

           MOVE WS-TP-GRAVITA-MOVIMENTO
             TO LCCC0023-TP-GRAVITA-MOVIMENTO
               (LCCC0023-ELE-MOV-FUTURI-MAX).

           MOVE WS-TP-GRAVITA-MOVIMENTO
             TO WS-TP-ANNULLO
           EVALUATE TRUE
               WHEN TA-BLOCCANTE
                  MOVE 'S'    TO LCCC0023-PRE-GRAV-BLOCCANTE

               WHEN TA-AMMISSIBILE
      *-->        SE C'E' ALMENO UN MOVIMENTO FUTURO BLOCCANTE, NON
      *-->        CONTROLLO L'ANNULLABILITA' IMPLICITA
                  IF LCCC0023-PRE-GRAV-BLOCCANTE = 'N'
                     MOVE 'S' TO LCCC0023-PRE-GRAV-ANNULLABILE
                  END-IF
           END-EVALUATE.

       EX-S1350.
           EXIT.
      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
           COPY IERP9903.
      *----------------------------------------------------------------*
      * GESTIONE DISPATCHER
      *----------------------------------------------------------------*
      *--> OPERAZIONI INIZIALI
           COPY LCCPIDSP.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.

       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSGRZ0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  31 OTT 2013.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDGRZ0 END-EXEC.
           EXEC SQL INCLUDE IDBVGRZ2 END-EXEC.
           EXEC SQL INCLUDE IDBVGRZ3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVGRZ1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 GAR.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSGRZ0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'GAR' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_GAR
                ,ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,DT_DECOR
                ,DT_SCAD
                ,COD_SEZ
                ,COD_TARI
                ,RAMO_BILA
                ,DT_INI_VAL_TAR
                ,ID_1O_ASSTO
                ,ID_2O_ASSTO
                ,ID_3O_ASSTO
                ,TP_GAR
                ,TP_RSH
                ,TP_INVST
                ,MOD_PAG_GARCOL
                ,TP_PER_PRE
                ,ETA_AA_1O_ASSTO
                ,ETA_MM_1O_ASSTO
                ,ETA_AA_2O_ASSTO
                ,ETA_MM_2O_ASSTO
                ,ETA_AA_3O_ASSTO
                ,ETA_MM_3O_ASSTO
                ,TP_EMIS_PUR
                ,ETA_A_SCAD
                ,TP_CALC_PRE_PRSTZ
                ,TP_PRE
                ,TP_DUR
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,NUM_AA_PAG_PRE
                ,AA_PAG_PRE_UNI
                ,MM_PAG_PRE_UNI
                ,FRAZ_INI_EROG_REN
                ,MM_1O_RAT
                ,PC_1O_RAT
                ,TP_PRSTZ_ASSTA
                ,DT_END_CARZ
                ,PC_RIP_PRE
                ,COD_FND
                ,AA_REN_CER
                ,PC_REVRSB
                ,TP_PC_RIP
                ,PC_OPZ
                ,TP_IAS
                ,TP_STAB
                ,TP_ADEG_PRE
                ,DT_VARZ_TP_IAS
                ,FRAZ_DECR_CPT
                ,COD_TRAT_RIASS
                ,TP_DT_EMIS_RIASS
                ,TP_CESS_RIASS
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,AA_STAB
                ,TS_STAB_LIMITATA
                ,DT_PRESC
                ,RSH_INVST
                ,TP_RAMO_BILA
             INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
             FROM GAR
             WHERE     DS_RIGA = :GRZ-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO GAR
                     (
                        ID_GAR
                       ,ID_ADES
                       ,ID_POLI
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,IB_OGG
                       ,DT_DECOR
                       ,DT_SCAD
                       ,COD_SEZ
                       ,COD_TARI
                       ,RAMO_BILA
                       ,DT_INI_VAL_TAR
                       ,ID_1O_ASSTO
                       ,ID_2O_ASSTO
                       ,ID_3O_ASSTO
                       ,TP_GAR
                       ,TP_RSH
                       ,TP_INVST
                       ,MOD_PAG_GARCOL
                       ,TP_PER_PRE
                       ,ETA_AA_1O_ASSTO
                       ,ETA_MM_1O_ASSTO
                       ,ETA_AA_2O_ASSTO
                       ,ETA_MM_2O_ASSTO
                       ,ETA_AA_3O_ASSTO
                       ,ETA_MM_3O_ASSTO
                       ,TP_EMIS_PUR
                       ,ETA_A_SCAD
                       ,TP_CALC_PRE_PRSTZ
                       ,TP_PRE
                       ,TP_DUR
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,NUM_AA_PAG_PRE
                       ,AA_PAG_PRE_UNI
                       ,MM_PAG_PRE_UNI
                       ,FRAZ_INI_EROG_REN
                       ,MM_1O_RAT
                       ,PC_1O_RAT
                       ,TP_PRSTZ_ASSTA
                       ,DT_END_CARZ
                       ,PC_RIP_PRE
                       ,COD_FND
                       ,AA_REN_CER
                       ,PC_REVRSB
                       ,TP_PC_RIP
                       ,PC_OPZ
                       ,TP_IAS
                       ,TP_STAB
                       ,TP_ADEG_PRE
                       ,DT_VARZ_TP_IAS
                       ,FRAZ_DECR_CPT
                       ,COD_TRAT_RIASS
                       ,TP_DT_EMIS_RIASS
                       ,TP_CESS_RIASS
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,AA_STAB
                       ,TS_STAB_LIMITATA
                       ,DT_PRESC
                       ,RSH_INVST
                       ,TP_RAMO_BILA
                     )
                 VALUES
                     (
                       :GRZ-ID-GAR
                       ,:GRZ-ID-ADES
                        :IND-GRZ-ID-ADES
                       ,:GRZ-ID-POLI
                       ,:GRZ-ID-MOVI-CRZ
                       ,:GRZ-ID-MOVI-CHIU
                        :IND-GRZ-ID-MOVI-CHIU
                       ,:GRZ-DT-INI-EFF-DB
                       ,:GRZ-DT-END-EFF-DB
                       ,:GRZ-COD-COMP-ANIA
                       ,:GRZ-IB-OGG
                        :IND-GRZ-IB-OGG
                       ,:GRZ-DT-DECOR-DB
                        :IND-GRZ-DT-DECOR
                       ,:GRZ-DT-SCAD-DB
                        :IND-GRZ-DT-SCAD
                       ,:GRZ-COD-SEZ
                        :IND-GRZ-COD-SEZ
                       ,:GRZ-COD-TARI
                       ,:GRZ-RAMO-BILA
                        :IND-GRZ-RAMO-BILA
                       ,:GRZ-DT-INI-VAL-TAR-DB
                        :IND-GRZ-DT-INI-VAL-TAR
                       ,:GRZ-ID-1O-ASSTO
                        :IND-GRZ-ID-1O-ASSTO
                       ,:GRZ-ID-2O-ASSTO
                        :IND-GRZ-ID-2O-ASSTO
                       ,:GRZ-ID-3O-ASSTO
                        :IND-GRZ-ID-3O-ASSTO
                       ,:GRZ-TP-GAR
                        :IND-GRZ-TP-GAR
                       ,:GRZ-TP-RSH
                        :IND-GRZ-TP-RSH
                       ,:GRZ-TP-INVST
                        :IND-GRZ-TP-INVST
                       ,:GRZ-MOD-PAG-GARCOL
                        :IND-GRZ-MOD-PAG-GARCOL
                       ,:GRZ-TP-PER-PRE
                        :IND-GRZ-TP-PER-PRE
                       ,:GRZ-ETA-AA-1O-ASSTO
                        :IND-GRZ-ETA-AA-1O-ASSTO
                       ,:GRZ-ETA-MM-1O-ASSTO
                        :IND-GRZ-ETA-MM-1O-ASSTO
                       ,:GRZ-ETA-AA-2O-ASSTO
                        :IND-GRZ-ETA-AA-2O-ASSTO
                       ,:GRZ-ETA-MM-2O-ASSTO
                        :IND-GRZ-ETA-MM-2O-ASSTO
                       ,:GRZ-ETA-AA-3O-ASSTO
                        :IND-GRZ-ETA-AA-3O-ASSTO
                       ,:GRZ-ETA-MM-3O-ASSTO
                        :IND-GRZ-ETA-MM-3O-ASSTO
                       ,:GRZ-TP-EMIS-PUR
                        :IND-GRZ-TP-EMIS-PUR
                       ,:GRZ-ETA-A-SCAD
                        :IND-GRZ-ETA-A-SCAD
                       ,:GRZ-TP-CALC-PRE-PRSTZ
                        :IND-GRZ-TP-CALC-PRE-PRSTZ
                       ,:GRZ-TP-PRE
                        :IND-GRZ-TP-PRE
                       ,:GRZ-TP-DUR
                        :IND-GRZ-TP-DUR
                       ,:GRZ-DUR-AA
                        :IND-GRZ-DUR-AA
                       ,:GRZ-DUR-MM
                        :IND-GRZ-DUR-MM
                       ,:GRZ-DUR-GG
                        :IND-GRZ-DUR-GG
                       ,:GRZ-NUM-AA-PAG-PRE
                        :IND-GRZ-NUM-AA-PAG-PRE
                       ,:GRZ-AA-PAG-PRE-UNI
                        :IND-GRZ-AA-PAG-PRE-UNI
                       ,:GRZ-MM-PAG-PRE-UNI
                        :IND-GRZ-MM-PAG-PRE-UNI
                       ,:GRZ-FRAZ-INI-EROG-REN
                        :IND-GRZ-FRAZ-INI-EROG-REN
                       ,:GRZ-MM-1O-RAT
                        :IND-GRZ-MM-1O-RAT
                       ,:GRZ-PC-1O-RAT
                        :IND-GRZ-PC-1O-RAT
                       ,:GRZ-TP-PRSTZ-ASSTA
                        :IND-GRZ-TP-PRSTZ-ASSTA
                       ,:GRZ-DT-END-CARZ-DB
                        :IND-GRZ-DT-END-CARZ
                       ,:GRZ-PC-RIP-PRE
                        :IND-GRZ-PC-RIP-PRE
                       ,:GRZ-COD-FND
                        :IND-GRZ-COD-FND
                       ,:GRZ-AA-REN-CER
                        :IND-GRZ-AA-REN-CER
                       ,:GRZ-PC-REVRSB
                        :IND-GRZ-PC-REVRSB
                       ,:GRZ-TP-PC-RIP
                        :IND-GRZ-TP-PC-RIP
                       ,:GRZ-PC-OPZ
                        :IND-GRZ-PC-OPZ
                       ,:GRZ-TP-IAS
                        :IND-GRZ-TP-IAS
                       ,:GRZ-TP-STAB
                        :IND-GRZ-TP-STAB
                       ,:GRZ-TP-ADEG-PRE
                        :IND-GRZ-TP-ADEG-PRE
                       ,:GRZ-DT-VARZ-TP-IAS-DB
                        :IND-GRZ-DT-VARZ-TP-IAS
                       ,:GRZ-FRAZ-DECR-CPT
                        :IND-GRZ-FRAZ-DECR-CPT
                       ,:GRZ-COD-TRAT-RIASS
                        :IND-GRZ-COD-TRAT-RIASS
                       ,:GRZ-TP-DT-EMIS-RIASS
                        :IND-GRZ-TP-DT-EMIS-RIASS
                       ,:GRZ-TP-CESS-RIASS
                        :IND-GRZ-TP-CESS-RIASS
                       ,:GRZ-DS-RIGA
                       ,:GRZ-DS-OPER-SQL
                       ,:GRZ-DS-VER
                       ,:GRZ-DS-TS-INI-CPTZ
                       ,:GRZ-DS-TS-END-CPTZ
                       ,:GRZ-DS-UTENTE
                       ,:GRZ-DS-STATO-ELAB
                       ,:GRZ-AA-STAB
                        :IND-GRZ-AA-STAB
                       ,:GRZ-TS-STAB-LIMITATA
                        :IND-GRZ-TS-STAB-LIMITATA
                       ,:GRZ-DT-PRESC-DB
                        :IND-GRZ-DT-PRESC
                       ,:GRZ-RSH-INVST
                        :IND-GRZ-RSH-INVST
                       ,:GRZ-TP-RAMO-BILA
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE GAR SET

                   ID_GAR                 =
                :GRZ-ID-GAR
                  ,ID_ADES                =
                :GRZ-ID-ADES
                                       :IND-GRZ-ID-ADES
                  ,ID_POLI                =
                :GRZ-ID-POLI
                  ,ID_MOVI_CRZ            =
                :GRZ-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :GRZ-ID-MOVI-CHIU
                                       :IND-GRZ-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :GRZ-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :GRZ-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :GRZ-COD-COMP-ANIA
                  ,IB_OGG                 =
                :GRZ-IB-OGG
                                       :IND-GRZ-IB-OGG
                  ,DT_DECOR               =
           :GRZ-DT-DECOR-DB
                                       :IND-GRZ-DT-DECOR
                  ,DT_SCAD                =
           :GRZ-DT-SCAD-DB
                                       :IND-GRZ-DT-SCAD
                  ,COD_SEZ                =
                :GRZ-COD-SEZ
                                       :IND-GRZ-COD-SEZ
                  ,COD_TARI               =
                :GRZ-COD-TARI
                  ,RAMO_BILA              =
                :GRZ-RAMO-BILA
                                       :IND-GRZ-RAMO-BILA
                  ,DT_INI_VAL_TAR         =
           :GRZ-DT-INI-VAL-TAR-DB
                                       :IND-GRZ-DT-INI-VAL-TAR
                  ,ID_1O_ASSTO            =
                :GRZ-ID-1O-ASSTO
                                       :IND-GRZ-ID-1O-ASSTO
                  ,ID_2O_ASSTO            =
                :GRZ-ID-2O-ASSTO
                                       :IND-GRZ-ID-2O-ASSTO
                  ,ID_3O_ASSTO            =
                :GRZ-ID-3O-ASSTO
                                       :IND-GRZ-ID-3O-ASSTO
                  ,TP_GAR                 =
                :GRZ-TP-GAR
                                       :IND-GRZ-TP-GAR
                  ,TP_RSH                 =
                :GRZ-TP-RSH
                                       :IND-GRZ-TP-RSH
                  ,TP_INVST               =
                :GRZ-TP-INVST
                                       :IND-GRZ-TP-INVST
                  ,MOD_PAG_GARCOL         =
                :GRZ-MOD-PAG-GARCOL
                                       :IND-GRZ-MOD-PAG-GARCOL
                  ,TP_PER_PRE             =
                :GRZ-TP-PER-PRE
                                       :IND-GRZ-TP-PER-PRE
                  ,ETA_AA_1O_ASSTO        =
                :GRZ-ETA-AA-1O-ASSTO
                                       :IND-GRZ-ETA-AA-1O-ASSTO
                  ,ETA_MM_1O_ASSTO        =
                :GRZ-ETA-MM-1O-ASSTO
                                       :IND-GRZ-ETA-MM-1O-ASSTO
                  ,ETA_AA_2O_ASSTO        =
                :GRZ-ETA-AA-2O-ASSTO
                                       :IND-GRZ-ETA-AA-2O-ASSTO
                  ,ETA_MM_2O_ASSTO        =
                :GRZ-ETA-MM-2O-ASSTO
                                       :IND-GRZ-ETA-MM-2O-ASSTO
                  ,ETA_AA_3O_ASSTO        =
                :GRZ-ETA-AA-3O-ASSTO
                                       :IND-GRZ-ETA-AA-3O-ASSTO
                  ,ETA_MM_3O_ASSTO        =
                :GRZ-ETA-MM-3O-ASSTO
                                       :IND-GRZ-ETA-MM-3O-ASSTO
                  ,TP_EMIS_PUR            =
                :GRZ-TP-EMIS-PUR
                                       :IND-GRZ-TP-EMIS-PUR
                  ,ETA_A_SCAD             =
                :GRZ-ETA-A-SCAD
                                       :IND-GRZ-ETA-A-SCAD
                  ,TP_CALC_PRE_PRSTZ      =
                :GRZ-TP-CALC-PRE-PRSTZ
                                       :IND-GRZ-TP-CALC-PRE-PRSTZ
                  ,TP_PRE                 =
                :GRZ-TP-PRE
                                       :IND-GRZ-TP-PRE
                  ,TP_DUR                 =
                :GRZ-TP-DUR
                                       :IND-GRZ-TP-DUR
                  ,DUR_AA                 =
                :GRZ-DUR-AA
                                       :IND-GRZ-DUR-AA
                  ,DUR_MM                 =
                :GRZ-DUR-MM
                                       :IND-GRZ-DUR-MM
                  ,DUR_GG                 =
                :GRZ-DUR-GG
                                       :IND-GRZ-DUR-GG
                  ,NUM_AA_PAG_PRE         =
                :GRZ-NUM-AA-PAG-PRE
                                       :IND-GRZ-NUM-AA-PAG-PRE
                  ,AA_PAG_PRE_UNI         =
                :GRZ-AA-PAG-PRE-UNI
                                       :IND-GRZ-AA-PAG-PRE-UNI
                  ,MM_PAG_PRE_UNI         =
                :GRZ-MM-PAG-PRE-UNI
                                       :IND-GRZ-MM-PAG-PRE-UNI
                  ,FRAZ_INI_EROG_REN      =
                :GRZ-FRAZ-INI-EROG-REN
                                       :IND-GRZ-FRAZ-INI-EROG-REN
                  ,MM_1O_RAT              =
                :GRZ-MM-1O-RAT
                                       :IND-GRZ-MM-1O-RAT
                  ,PC_1O_RAT              =
                :GRZ-PC-1O-RAT
                                       :IND-GRZ-PC-1O-RAT
                  ,TP_PRSTZ_ASSTA         =
                :GRZ-TP-PRSTZ-ASSTA
                                       :IND-GRZ-TP-PRSTZ-ASSTA
                  ,DT_END_CARZ            =
           :GRZ-DT-END-CARZ-DB
                                       :IND-GRZ-DT-END-CARZ
                  ,PC_RIP_PRE             =
                :GRZ-PC-RIP-PRE
                                       :IND-GRZ-PC-RIP-PRE
                  ,COD_FND                =
                :GRZ-COD-FND
                                       :IND-GRZ-COD-FND
                  ,AA_REN_CER             =
                :GRZ-AA-REN-CER
                                       :IND-GRZ-AA-REN-CER
                  ,PC_REVRSB              =
                :GRZ-PC-REVRSB
                                       :IND-GRZ-PC-REVRSB
                  ,TP_PC_RIP              =
                :GRZ-TP-PC-RIP
                                       :IND-GRZ-TP-PC-RIP
                  ,PC_OPZ                 =
                :GRZ-PC-OPZ
                                       :IND-GRZ-PC-OPZ
                  ,TP_IAS                 =
                :GRZ-TP-IAS
                                       :IND-GRZ-TP-IAS
                  ,TP_STAB                =
                :GRZ-TP-STAB
                                       :IND-GRZ-TP-STAB
                  ,TP_ADEG_PRE            =
                :GRZ-TP-ADEG-PRE
                                       :IND-GRZ-TP-ADEG-PRE
                  ,DT_VARZ_TP_IAS         =
           :GRZ-DT-VARZ-TP-IAS-DB
                                       :IND-GRZ-DT-VARZ-TP-IAS
                  ,FRAZ_DECR_CPT          =
                :GRZ-FRAZ-DECR-CPT
                                       :IND-GRZ-FRAZ-DECR-CPT
                  ,COD_TRAT_RIASS         =
                :GRZ-COD-TRAT-RIASS
                                       :IND-GRZ-COD-TRAT-RIASS
                  ,TP_DT_EMIS_RIASS       =
                :GRZ-TP-DT-EMIS-RIASS
                                       :IND-GRZ-TP-DT-EMIS-RIASS
                  ,TP_CESS_RIASS          =
                :GRZ-TP-CESS-RIASS
                                       :IND-GRZ-TP-CESS-RIASS
                  ,DS_RIGA                =
                :GRZ-DS-RIGA
                  ,DS_OPER_SQL            =
                :GRZ-DS-OPER-SQL
                  ,DS_VER                 =
                :GRZ-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :GRZ-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :GRZ-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :GRZ-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :GRZ-DS-STATO-ELAB
                  ,AA_STAB                =
                :GRZ-AA-STAB
                                       :IND-GRZ-AA-STAB
                  ,TS_STAB_LIMITATA       =
                :GRZ-TS-STAB-LIMITATA
                                       :IND-GRZ-TS-STAB-LIMITATA
                  ,DT_PRESC               =
           :GRZ-DT-PRESC-DB
                                       :IND-GRZ-DT-PRESC
                  ,RSH_INVST              =
                :GRZ-RSH-INVST
                                       :IND-GRZ-RSH-INVST
                  ,TP_RAMO_BILA           =
                :GRZ-TP-RAMO-BILA
                WHERE     DS_RIGA = :GRZ-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM GAR
                WHERE     DS_RIGA = :GRZ-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-GRZ CURSOR FOR
              SELECT
                     ID_GAR
                    ,ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IB_OGG
                    ,DT_DECOR
                    ,DT_SCAD
                    ,COD_SEZ
                    ,COD_TARI
                    ,RAMO_BILA
                    ,DT_INI_VAL_TAR
                    ,ID_1O_ASSTO
                    ,ID_2O_ASSTO
                    ,ID_3O_ASSTO
                    ,TP_GAR
                    ,TP_RSH
                    ,TP_INVST
                    ,MOD_PAG_GARCOL
                    ,TP_PER_PRE
                    ,ETA_AA_1O_ASSTO
                    ,ETA_MM_1O_ASSTO
                    ,ETA_AA_2O_ASSTO
                    ,ETA_MM_2O_ASSTO
                    ,ETA_AA_3O_ASSTO
                    ,ETA_MM_3O_ASSTO
                    ,TP_EMIS_PUR
                    ,ETA_A_SCAD
                    ,TP_CALC_PRE_PRSTZ
                    ,TP_PRE
                    ,TP_DUR
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,NUM_AA_PAG_PRE
                    ,AA_PAG_PRE_UNI
                    ,MM_PAG_PRE_UNI
                    ,FRAZ_INI_EROG_REN
                    ,MM_1O_RAT
                    ,PC_1O_RAT
                    ,TP_PRSTZ_ASSTA
                    ,DT_END_CARZ
                    ,PC_RIP_PRE
                    ,COD_FND
                    ,AA_REN_CER
                    ,PC_REVRSB
                    ,TP_PC_RIP
                    ,PC_OPZ
                    ,TP_IAS
                    ,TP_STAB
                    ,TP_ADEG_PRE
                    ,DT_VARZ_TP_IAS
                    ,FRAZ_DECR_CPT
                    ,COD_TRAT_RIASS
                    ,TP_DT_EMIS_RIASS
                    ,TP_CESS_RIASS
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,AA_STAB
                    ,TS_STAB_LIMITATA
                    ,DT_PRESC
                    ,RSH_INVST
                    ,TP_RAMO_BILA
              FROM GAR
              WHERE     ID_GAR = :GRZ-ID-GAR
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_GAR
                ,ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,DT_DECOR
                ,DT_SCAD
                ,COD_SEZ
                ,COD_TARI
                ,RAMO_BILA
                ,DT_INI_VAL_TAR
                ,ID_1O_ASSTO
                ,ID_2O_ASSTO
                ,ID_3O_ASSTO
                ,TP_GAR
                ,TP_RSH
                ,TP_INVST
                ,MOD_PAG_GARCOL
                ,TP_PER_PRE
                ,ETA_AA_1O_ASSTO
                ,ETA_MM_1O_ASSTO
                ,ETA_AA_2O_ASSTO
                ,ETA_MM_2O_ASSTO
                ,ETA_AA_3O_ASSTO
                ,ETA_MM_3O_ASSTO
                ,TP_EMIS_PUR
                ,ETA_A_SCAD
                ,TP_CALC_PRE_PRSTZ
                ,TP_PRE
                ,TP_DUR
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,NUM_AA_PAG_PRE
                ,AA_PAG_PRE_UNI
                ,MM_PAG_PRE_UNI
                ,FRAZ_INI_EROG_REN
                ,MM_1O_RAT
                ,PC_1O_RAT
                ,TP_PRSTZ_ASSTA
                ,DT_END_CARZ
                ,PC_RIP_PRE
                ,COD_FND
                ,AA_REN_CER
                ,PC_REVRSB
                ,TP_PC_RIP
                ,PC_OPZ
                ,TP_IAS
                ,TP_STAB
                ,TP_ADEG_PRE
                ,DT_VARZ_TP_IAS
                ,FRAZ_DECR_CPT
                ,COD_TRAT_RIASS
                ,TP_DT_EMIS_RIASS
                ,TP_CESS_RIASS
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,AA_STAB
                ,TS_STAB_LIMITATA
                ,DT_PRESC
                ,RSH_INVST
                ,TP_RAMO_BILA
             INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
             FROM GAR
             WHERE     ID_GAR = :GRZ-ID-GAR
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE GAR SET

                   ID_GAR                 =
                :GRZ-ID-GAR
                  ,ID_ADES                =
                :GRZ-ID-ADES
                                       :IND-GRZ-ID-ADES
                  ,ID_POLI                =
                :GRZ-ID-POLI
                  ,ID_MOVI_CRZ            =
                :GRZ-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :GRZ-ID-MOVI-CHIU
                                       :IND-GRZ-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :GRZ-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :GRZ-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :GRZ-COD-COMP-ANIA
                  ,IB_OGG                 =
                :GRZ-IB-OGG
                                       :IND-GRZ-IB-OGG
                  ,DT_DECOR               =
           :GRZ-DT-DECOR-DB
                                       :IND-GRZ-DT-DECOR
                  ,DT_SCAD                =
           :GRZ-DT-SCAD-DB
                                       :IND-GRZ-DT-SCAD
                  ,COD_SEZ                =
                :GRZ-COD-SEZ
                                       :IND-GRZ-COD-SEZ
                  ,COD_TARI               =
                :GRZ-COD-TARI
                  ,RAMO_BILA              =
                :GRZ-RAMO-BILA
                                       :IND-GRZ-RAMO-BILA
                  ,DT_INI_VAL_TAR         =
           :GRZ-DT-INI-VAL-TAR-DB
                                       :IND-GRZ-DT-INI-VAL-TAR
                  ,ID_1O_ASSTO            =
                :GRZ-ID-1O-ASSTO
                                       :IND-GRZ-ID-1O-ASSTO
                  ,ID_2O_ASSTO            =
                :GRZ-ID-2O-ASSTO
                                       :IND-GRZ-ID-2O-ASSTO
                  ,ID_3O_ASSTO            =
                :GRZ-ID-3O-ASSTO
                                       :IND-GRZ-ID-3O-ASSTO
                  ,TP_GAR                 =
                :GRZ-TP-GAR
                                       :IND-GRZ-TP-GAR
                  ,TP_RSH                 =
                :GRZ-TP-RSH
                                       :IND-GRZ-TP-RSH
                  ,TP_INVST               =
                :GRZ-TP-INVST
                                       :IND-GRZ-TP-INVST
                  ,MOD_PAG_GARCOL         =
                :GRZ-MOD-PAG-GARCOL
                                       :IND-GRZ-MOD-PAG-GARCOL
                  ,TP_PER_PRE             =
                :GRZ-TP-PER-PRE
                                       :IND-GRZ-TP-PER-PRE
                  ,ETA_AA_1O_ASSTO        =
                :GRZ-ETA-AA-1O-ASSTO
                                       :IND-GRZ-ETA-AA-1O-ASSTO
                  ,ETA_MM_1O_ASSTO        =
                :GRZ-ETA-MM-1O-ASSTO
                                       :IND-GRZ-ETA-MM-1O-ASSTO
                  ,ETA_AA_2O_ASSTO        =
                :GRZ-ETA-AA-2O-ASSTO
                                       :IND-GRZ-ETA-AA-2O-ASSTO
                  ,ETA_MM_2O_ASSTO        =
                :GRZ-ETA-MM-2O-ASSTO
                                       :IND-GRZ-ETA-MM-2O-ASSTO
                  ,ETA_AA_3O_ASSTO        =
                :GRZ-ETA-AA-3O-ASSTO
                                       :IND-GRZ-ETA-AA-3O-ASSTO
                  ,ETA_MM_3O_ASSTO        =
                :GRZ-ETA-MM-3O-ASSTO
                                       :IND-GRZ-ETA-MM-3O-ASSTO
                  ,TP_EMIS_PUR            =
                :GRZ-TP-EMIS-PUR
                                       :IND-GRZ-TP-EMIS-PUR
                  ,ETA_A_SCAD             =
                :GRZ-ETA-A-SCAD
                                       :IND-GRZ-ETA-A-SCAD
                  ,TP_CALC_PRE_PRSTZ      =
                :GRZ-TP-CALC-PRE-PRSTZ
                                       :IND-GRZ-TP-CALC-PRE-PRSTZ
                  ,TP_PRE                 =
                :GRZ-TP-PRE
                                       :IND-GRZ-TP-PRE
                  ,TP_DUR                 =
                :GRZ-TP-DUR
                                       :IND-GRZ-TP-DUR
                  ,DUR_AA                 =
                :GRZ-DUR-AA
                                       :IND-GRZ-DUR-AA
                  ,DUR_MM                 =
                :GRZ-DUR-MM
                                       :IND-GRZ-DUR-MM
                  ,DUR_GG                 =
                :GRZ-DUR-GG
                                       :IND-GRZ-DUR-GG
                  ,NUM_AA_PAG_PRE         =
                :GRZ-NUM-AA-PAG-PRE
                                       :IND-GRZ-NUM-AA-PAG-PRE
                  ,AA_PAG_PRE_UNI         =
                :GRZ-AA-PAG-PRE-UNI
                                       :IND-GRZ-AA-PAG-PRE-UNI
                  ,MM_PAG_PRE_UNI         =
                :GRZ-MM-PAG-PRE-UNI
                                       :IND-GRZ-MM-PAG-PRE-UNI
                  ,FRAZ_INI_EROG_REN      =
                :GRZ-FRAZ-INI-EROG-REN
                                       :IND-GRZ-FRAZ-INI-EROG-REN
                  ,MM_1O_RAT              =
                :GRZ-MM-1O-RAT
                                       :IND-GRZ-MM-1O-RAT
                  ,PC_1O_RAT              =
                :GRZ-PC-1O-RAT
                                       :IND-GRZ-PC-1O-RAT
                  ,TP_PRSTZ_ASSTA         =
                :GRZ-TP-PRSTZ-ASSTA
                                       :IND-GRZ-TP-PRSTZ-ASSTA
                  ,DT_END_CARZ            =
           :GRZ-DT-END-CARZ-DB
                                       :IND-GRZ-DT-END-CARZ
                  ,PC_RIP_PRE             =
                :GRZ-PC-RIP-PRE
                                       :IND-GRZ-PC-RIP-PRE
                  ,COD_FND                =
                :GRZ-COD-FND
                                       :IND-GRZ-COD-FND
                  ,AA_REN_CER             =
                :GRZ-AA-REN-CER
                                       :IND-GRZ-AA-REN-CER
                  ,PC_REVRSB              =
                :GRZ-PC-REVRSB
                                       :IND-GRZ-PC-REVRSB
                  ,TP_PC_RIP              =
                :GRZ-TP-PC-RIP
                                       :IND-GRZ-TP-PC-RIP
                  ,PC_OPZ                 =
                :GRZ-PC-OPZ
                                       :IND-GRZ-PC-OPZ
                  ,TP_IAS                 =
                :GRZ-TP-IAS
                                       :IND-GRZ-TP-IAS
                  ,TP_STAB                =
                :GRZ-TP-STAB
                                       :IND-GRZ-TP-STAB
                  ,TP_ADEG_PRE            =
                :GRZ-TP-ADEG-PRE
                                       :IND-GRZ-TP-ADEG-PRE
                  ,DT_VARZ_TP_IAS         =
           :GRZ-DT-VARZ-TP-IAS-DB
                                       :IND-GRZ-DT-VARZ-TP-IAS
                  ,FRAZ_DECR_CPT          =
                :GRZ-FRAZ-DECR-CPT
                                       :IND-GRZ-FRAZ-DECR-CPT
                  ,COD_TRAT_RIASS         =
                :GRZ-COD-TRAT-RIASS
                                       :IND-GRZ-COD-TRAT-RIASS
                  ,TP_DT_EMIS_RIASS       =
                :GRZ-TP-DT-EMIS-RIASS
                                       :IND-GRZ-TP-DT-EMIS-RIASS
                  ,TP_CESS_RIASS          =
                :GRZ-TP-CESS-RIASS
                                       :IND-GRZ-TP-CESS-RIASS
                  ,DS_RIGA                =
                :GRZ-DS-RIGA
                  ,DS_OPER_SQL            =
                :GRZ-DS-OPER-SQL
                  ,DS_VER                 =
                :GRZ-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :GRZ-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :GRZ-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :GRZ-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :GRZ-DS-STATO-ELAB
                  ,AA_STAB                =
                :GRZ-AA-STAB
                                       :IND-GRZ-AA-STAB
                  ,TS_STAB_LIMITATA       =
                :GRZ-TS-STAB-LIMITATA
                                       :IND-GRZ-TS-STAB-LIMITATA
                  ,DT_PRESC               =
           :GRZ-DT-PRESC-DB
                                       :IND-GRZ-DT-PRESC
                  ,RSH_INVST              =
                :GRZ-RSH-INVST
                                       :IND-GRZ-RSH-INVST
                  ,TP_RAMO_BILA           =
                :GRZ-TP-RAMO-BILA
                WHERE     DS_RIGA = :GRZ-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-GRZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-GRZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-GRZ
           INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-EFF-GRZ CURSOR FOR
              SELECT
                     ID_GAR
                    ,ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IB_OGG
                    ,DT_DECOR
                    ,DT_SCAD
                    ,COD_SEZ
                    ,COD_TARI
                    ,RAMO_BILA
                    ,DT_INI_VAL_TAR
                    ,ID_1O_ASSTO
                    ,ID_2O_ASSTO
                    ,ID_3O_ASSTO
                    ,TP_GAR
                    ,TP_RSH
                    ,TP_INVST
                    ,MOD_PAG_GARCOL
                    ,TP_PER_PRE
                    ,ETA_AA_1O_ASSTO
                    ,ETA_MM_1O_ASSTO
                    ,ETA_AA_2O_ASSTO
                    ,ETA_MM_2O_ASSTO
                    ,ETA_AA_3O_ASSTO
                    ,ETA_MM_3O_ASSTO
                    ,TP_EMIS_PUR
                    ,ETA_A_SCAD
                    ,TP_CALC_PRE_PRSTZ
                    ,TP_PRE
                    ,TP_DUR
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,NUM_AA_PAG_PRE
                    ,AA_PAG_PRE_UNI
                    ,MM_PAG_PRE_UNI
                    ,FRAZ_INI_EROG_REN
                    ,MM_1O_RAT
                    ,PC_1O_RAT
                    ,TP_PRSTZ_ASSTA
                    ,DT_END_CARZ
                    ,PC_RIP_PRE
                    ,COD_FND
                    ,AA_REN_CER
                    ,PC_REVRSB
                    ,TP_PC_RIP
                    ,PC_OPZ
                    ,TP_IAS
                    ,TP_STAB
                    ,TP_ADEG_PRE
                    ,DT_VARZ_TP_IAS
                    ,FRAZ_DECR_CPT
                    ,COD_TRAT_RIASS
                    ,TP_DT_EMIS_RIASS
                    ,TP_CESS_RIASS
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,AA_STAB
                    ,TS_STAB_LIMITATA
                    ,DT_PRESC
                    ,RSH_INVST
                    ,TP_RAMO_BILA
              FROM GAR
              WHERE     ID_POLI = :GRZ-ID-POLI
                    AND ID_ADES = :GRZ-ID-ADES
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_GAR ASC

           END-EXEC.

       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_GAR
                ,ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,DT_DECOR
                ,DT_SCAD
                ,COD_SEZ
                ,COD_TARI
                ,RAMO_BILA
                ,DT_INI_VAL_TAR
                ,ID_1O_ASSTO
                ,ID_2O_ASSTO
                ,ID_3O_ASSTO
                ,TP_GAR
                ,TP_RSH
                ,TP_INVST
                ,MOD_PAG_GARCOL
                ,TP_PER_PRE
                ,ETA_AA_1O_ASSTO
                ,ETA_MM_1O_ASSTO
                ,ETA_AA_2O_ASSTO
                ,ETA_MM_2O_ASSTO
                ,ETA_AA_3O_ASSTO
                ,ETA_MM_3O_ASSTO
                ,TP_EMIS_PUR
                ,ETA_A_SCAD
                ,TP_CALC_PRE_PRSTZ
                ,TP_PRE
                ,TP_DUR
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,NUM_AA_PAG_PRE
                ,AA_PAG_PRE_UNI
                ,MM_PAG_PRE_UNI
                ,FRAZ_INI_EROG_REN
                ,MM_1O_RAT
                ,PC_1O_RAT
                ,TP_PRSTZ_ASSTA
                ,DT_END_CARZ
                ,PC_RIP_PRE
                ,COD_FND
                ,AA_REN_CER
                ,PC_REVRSB
                ,TP_PC_RIP
                ,PC_OPZ
                ,TP_IAS
                ,TP_STAB
                ,TP_ADEG_PRE
                ,DT_VARZ_TP_IAS
                ,FRAZ_DECR_CPT
                ,COD_TRAT_RIASS
                ,TP_DT_EMIS_RIASS
                ,TP_CESS_RIASS
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,AA_STAB
                ,TS_STAB_LIMITATA
                ,DT_PRESC
                ,RSH_INVST
                ,TP_RAMO_BILA
             INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
             FROM GAR
             WHERE     ID_POLI = :GRZ-ID-POLI
                    AND ID_ADES = :GRZ-ID-ADES
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           EXEC SQL
                OPEN C-IDP-EFF-GRZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           EXEC SQL
                CLOSE C-IDP-EFF-GRZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           EXEC SQL
                FETCH C-IDP-EFF-GRZ
           INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IBO-EFF-GRZ CURSOR FOR
              SELECT
                     ID_GAR
                    ,ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IB_OGG
                    ,DT_DECOR
                    ,DT_SCAD
                    ,COD_SEZ
                    ,COD_TARI
                    ,RAMO_BILA
                    ,DT_INI_VAL_TAR
                    ,ID_1O_ASSTO
                    ,ID_2O_ASSTO
                    ,ID_3O_ASSTO
                    ,TP_GAR
                    ,TP_RSH
                    ,TP_INVST
                    ,MOD_PAG_GARCOL
                    ,TP_PER_PRE
                    ,ETA_AA_1O_ASSTO
                    ,ETA_MM_1O_ASSTO
                    ,ETA_AA_2O_ASSTO
                    ,ETA_MM_2O_ASSTO
                    ,ETA_AA_3O_ASSTO
                    ,ETA_MM_3O_ASSTO
                    ,TP_EMIS_PUR
                    ,ETA_A_SCAD
                    ,TP_CALC_PRE_PRSTZ
                    ,TP_PRE
                    ,TP_DUR
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,NUM_AA_PAG_PRE
                    ,AA_PAG_PRE_UNI
                    ,MM_PAG_PRE_UNI
                    ,FRAZ_INI_EROG_REN
                    ,MM_1O_RAT
                    ,PC_1O_RAT
                    ,TP_PRSTZ_ASSTA
                    ,DT_END_CARZ
                    ,PC_RIP_PRE
                    ,COD_FND
                    ,AA_REN_CER
                    ,PC_REVRSB
                    ,TP_PC_RIP
                    ,PC_OPZ
                    ,TP_IAS
                    ,TP_STAB
                    ,TP_ADEG_PRE
                    ,DT_VARZ_TP_IAS
                    ,FRAZ_DECR_CPT
                    ,COD_TRAT_RIASS
                    ,TP_DT_EMIS_RIASS
                    ,TP_CESS_RIASS
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,AA_STAB
                    ,TS_STAB_LIMITATA
                    ,DT_PRESC
                    ,RSH_INVST
                    ,TP_RAMO_BILA
              FROM GAR
              WHERE     IB_OGG = :GRZ-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_GAR ASC

           END-EXEC.

       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_GAR
                ,ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,DT_DECOR
                ,DT_SCAD
                ,COD_SEZ
                ,COD_TARI
                ,RAMO_BILA
                ,DT_INI_VAL_TAR
                ,ID_1O_ASSTO
                ,ID_2O_ASSTO
                ,ID_3O_ASSTO
                ,TP_GAR
                ,TP_RSH
                ,TP_INVST
                ,MOD_PAG_GARCOL
                ,TP_PER_PRE
                ,ETA_AA_1O_ASSTO
                ,ETA_MM_1O_ASSTO
                ,ETA_AA_2O_ASSTO
                ,ETA_MM_2O_ASSTO
                ,ETA_AA_3O_ASSTO
                ,ETA_MM_3O_ASSTO
                ,TP_EMIS_PUR
                ,ETA_A_SCAD
                ,TP_CALC_PRE_PRSTZ
                ,TP_PRE
                ,TP_DUR
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,NUM_AA_PAG_PRE
                ,AA_PAG_PRE_UNI
                ,MM_PAG_PRE_UNI
                ,FRAZ_INI_EROG_REN
                ,MM_1O_RAT
                ,PC_1O_RAT
                ,TP_PRSTZ_ASSTA
                ,DT_END_CARZ
                ,PC_RIP_PRE
                ,COD_FND
                ,AA_REN_CER
                ,PC_REVRSB
                ,TP_PC_RIP
                ,PC_OPZ
                ,TP_IAS
                ,TP_STAB
                ,TP_ADEG_PRE
                ,DT_VARZ_TP_IAS
                ,FRAZ_DECR_CPT
                ,COD_TRAT_RIASS
                ,TP_DT_EMIS_RIASS
                ,TP_CESS_RIASS
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,AA_STAB
                ,TS_STAB_LIMITATA
                ,DT_PRESC
                ,RSH_INVST
                ,TP_RAMO_BILA
             INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
             FROM GAR
             WHERE     IB_OGG = :GRZ-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           EXEC SQL
                OPEN C-IBO-EFF-GRZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           EXEC SQL
                CLOSE C-IBO-EFF-GRZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           EXEC SQL
                FETCH C-IBO-EFF-GRZ
           INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A570-CLOSE-CURSOR-IBO     THRU A570-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_GAR
                ,ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,DT_DECOR
                ,DT_SCAD
                ,COD_SEZ
                ,COD_TARI
                ,RAMO_BILA
                ,DT_INI_VAL_TAR
                ,ID_1O_ASSTO
                ,ID_2O_ASSTO
                ,ID_3O_ASSTO
                ,TP_GAR
                ,TP_RSH
                ,TP_INVST
                ,MOD_PAG_GARCOL
                ,TP_PER_PRE
                ,ETA_AA_1O_ASSTO
                ,ETA_MM_1O_ASSTO
                ,ETA_AA_2O_ASSTO
                ,ETA_MM_2O_ASSTO
                ,ETA_AA_3O_ASSTO
                ,ETA_MM_3O_ASSTO
                ,TP_EMIS_PUR
                ,ETA_A_SCAD
                ,TP_CALC_PRE_PRSTZ
                ,TP_PRE
                ,TP_DUR
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,NUM_AA_PAG_PRE
                ,AA_PAG_PRE_UNI
                ,MM_PAG_PRE_UNI
                ,FRAZ_INI_EROG_REN
                ,MM_1O_RAT
                ,PC_1O_RAT
                ,TP_PRSTZ_ASSTA
                ,DT_END_CARZ
                ,PC_RIP_PRE
                ,COD_FND
                ,AA_REN_CER
                ,PC_REVRSB
                ,TP_PC_RIP
                ,PC_OPZ
                ,TP_IAS
                ,TP_STAB
                ,TP_ADEG_PRE
                ,DT_VARZ_TP_IAS
                ,FRAZ_DECR_CPT
                ,COD_TRAT_RIASS
                ,TP_DT_EMIS_RIASS
                ,TP_CESS_RIASS
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,AA_STAB
                ,TS_STAB_LIMITATA
                ,DT_PRESC
                ,RSH_INVST
                ,TP_RAMO_BILA
             INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
             FROM GAR
             WHERE     ID_GAR = :GRZ-ID-GAR
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-CPZ-GRZ CURSOR FOR
              SELECT
                     ID_GAR
                    ,ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IB_OGG
                    ,DT_DECOR
                    ,DT_SCAD
                    ,COD_SEZ
                    ,COD_TARI
                    ,RAMO_BILA
                    ,DT_INI_VAL_TAR
                    ,ID_1O_ASSTO
                    ,ID_2O_ASSTO
                    ,ID_3O_ASSTO
                    ,TP_GAR
                    ,TP_RSH
                    ,TP_INVST
                    ,MOD_PAG_GARCOL
                    ,TP_PER_PRE
                    ,ETA_AA_1O_ASSTO
                    ,ETA_MM_1O_ASSTO
                    ,ETA_AA_2O_ASSTO
                    ,ETA_MM_2O_ASSTO
                    ,ETA_AA_3O_ASSTO
                    ,ETA_MM_3O_ASSTO
                    ,TP_EMIS_PUR
                    ,ETA_A_SCAD
                    ,TP_CALC_PRE_PRSTZ
                    ,TP_PRE
                    ,TP_DUR
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,NUM_AA_PAG_PRE
                    ,AA_PAG_PRE_UNI
                    ,MM_PAG_PRE_UNI
                    ,FRAZ_INI_EROG_REN
                    ,MM_1O_RAT
                    ,PC_1O_RAT
                    ,TP_PRSTZ_ASSTA
                    ,DT_END_CARZ
                    ,PC_RIP_PRE
                    ,COD_FND
                    ,AA_REN_CER
                    ,PC_REVRSB
                    ,TP_PC_RIP
                    ,PC_OPZ
                    ,TP_IAS
                    ,TP_STAB
                    ,TP_ADEG_PRE
                    ,DT_VARZ_TP_IAS
                    ,FRAZ_DECR_CPT
                    ,COD_TRAT_RIASS
                    ,TP_DT_EMIS_RIASS
                    ,TP_CESS_RIASS
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,AA_STAB
                    ,TS_STAB_LIMITATA
                    ,DT_PRESC
                    ,RSH_INVST
                    ,TP_RAMO_BILA
              FROM GAR
              WHERE     ID_POLI = :GRZ-ID-POLI
           AND ID_ADES = :GRZ-ID-ADES
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_GAR ASC

           END-EXEC.

       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_GAR
                ,ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,DT_DECOR
                ,DT_SCAD
                ,COD_SEZ
                ,COD_TARI
                ,RAMO_BILA
                ,DT_INI_VAL_TAR
                ,ID_1O_ASSTO
                ,ID_2O_ASSTO
                ,ID_3O_ASSTO
                ,TP_GAR
                ,TP_RSH
                ,TP_INVST
                ,MOD_PAG_GARCOL
                ,TP_PER_PRE
                ,ETA_AA_1O_ASSTO
                ,ETA_MM_1O_ASSTO
                ,ETA_AA_2O_ASSTO
                ,ETA_MM_2O_ASSTO
                ,ETA_AA_3O_ASSTO
                ,ETA_MM_3O_ASSTO
                ,TP_EMIS_PUR
                ,ETA_A_SCAD
                ,TP_CALC_PRE_PRSTZ
                ,TP_PRE
                ,TP_DUR
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,NUM_AA_PAG_PRE
                ,AA_PAG_PRE_UNI
                ,MM_PAG_PRE_UNI
                ,FRAZ_INI_EROG_REN
                ,MM_1O_RAT
                ,PC_1O_RAT
                ,TP_PRSTZ_ASSTA
                ,DT_END_CARZ
                ,PC_RIP_PRE
                ,COD_FND
                ,AA_REN_CER
                ,PC_REVRSB
                ,TP_PC_RIP
                ,PC_OPZ
                ,TP_IAS
                ,TP_STAB
                ,TP_ADEG_PRE
                ,DT_VARZ_TP_IAS
                ,FRAZ_DECR_CPT
                ,COD_TRAT_RIASS
                ,TP_DT_EMIS_RIASS
                ,TP_CESS_RIASS
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,AA_STAB
                ,TS_STAB_LIMITATA
                ,DT_PRESC
                ,RSH_INVST
                ,TP_RAMO_BILA
             INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
             FROM GAR
             WHERE     ID_POLI = :GRZ-ID-POLI
                    AND ID_ADES = :GRZ-ID-ADES
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           EXEC SQL
                OPEN C-IDP-CPZ-GRZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           EXEC SQL
                CLOSE C-IDP-CPZ-GRZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           EXEC SQL
                FETCH C-IDP-CPZ-GRZ
           INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IBO-CPZ-GRZ CURSOR FOR
              SELECT
                     ID_GAR
                    ,ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,IB_OGG
                    ,DT_DECOR
                    ,DT_SCAD
                    ,COD_SEZ
                    ,COD_TARI
                    ,RAMO_BILA
                    ,DT_INI_VAL_TAR
                    ,ID_1O_ASSTO
                    ,ID_2O_ASSTO
                    ,ID_3O_ASSTO
                    ,TP_GAR
                    ,TP_RSH
                    ,TP_INVST
                    ,MOD_PAG_GARCOL
                    ,TP_PER_PRE
                    ,ETA_AA_1O_ASSTO
                    ,ETA_MM_1O_ASSTO
                    ,ETA_AA_2O_ASSTO
                    ,ETA_MM_2O_ASSTO
                    ,ETA_AA_3O_ASSTO
                    ,ETA_MM_3O_ASSTO
                    ,TP_EMIS_PUR
                    ,ETA_A_SCAD
                    ,TP_CALC_PRE_PRSTZ
                    ,TP_PRE
                    ,TP_DUR
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,NUM_AA_PAG_PRE
                    ,AA_PAG_PRE_UNI
                    ,MM_PAG_PRE_UNI
                    ,FRAZ_INI_EROG_REN
                    ,MM_1O_RAT
                    ,PC_1O_RAT
                    ,TP_PRSTZ_ASSTA
                    ,DT_END_CARZ
                    ,PC_RIP_PRE
                    ,COD_FND
                    ,AA_REN_CER
                    ,PC_REVRSB
                    ,TP_PC_RIP
                    ,PC_OPZ
                    ,TP_IAS
                    ,TP_STAB
                    ,TP_ADEG_PRE
                    ,DT_VARZ_TP_IAS
                    ,FRAZ_DECR_CPT
                    ,COD_TRAT_RIASS
                    ,TP_DT_EMIS_RIASS
                    ,TP_CESS_RIASS
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,AA_STAB
                    ,TS_STAB_LIMITATA
                    ,DT_PRESC
                    ,RSH_INVST
                    ,TP_RAMO_BILA
              FROM GAR
              WHERE     IB_OGG = :GRZ-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_GAR ASC

           END-EXEC.

       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_GAR
                ,ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,IB_OGG
                ,DT_DECOR
                ,DT_SCAD
                ,COD_SEZ
                ,COD_TARI
                ,RAMO_BILA
                ,DT_INI_VAL_TAR
                ,ID_1O_ASSTO
                ,ID_2O_ASSTO
                ,ID_3O_ASSTO
                ,TP_GAR
                ,TP_RSH
                ,TP_INVST
                ,MOD_PAG_GARCOL
                ,TP_PER_PRE
                ,ETA_AA_1O_ASSTO
                ,ETA_MM_1O_ASSTO
                ,ETA_AA_2O_ASSTO
                ,ETA_MM_2O_ASSTO
                ,ETA_AA_3O_ASSTO
                ,ETA_MM_3O_ASSTO
                ,TP_EMIS_PUR
                ,ETA_A_SCAD
                ,TP_CALC_PRE_PRSTZ
                ,TP_PRE
                ,TP_DUR
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,NUM_AA_PAG_PRE
                ,AA_PAG_PRE_UNI
                ,MM_PAG_PRE_UNI
                ,FRAZ_INI_EROG_REN
                ,MM_1O_RAT
                ,PC_1O_RAT
                ,TP_PRSTZ_ASSTA
                ,DT_END_CARZ
                ,PC_RIP_PRE
                ,COD_FND
                ,AA_REN_CER
                ,PC_REVRSB
                ,TP_PC_RIP
                ,PC_OPZ
                ,TP_IAS
                ,TP_STAB
                ,TP_ADEG_PRE
                ,DT_VARZ_TP_IAS
                ,FRAZ_DECR_CPT
                ,COD_TRAT_RIASS
                ,TP_DT_EMIS_RIASS
                ,TP_CESS_RIASS
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,AA_STAB
                ,TS_STAB_LIMITATA
                ,DT_PRESC
                ,RSH_INVST
                ,TP_RAMO_BILA
             INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
             FROM GAR
             WHERE     IB_OGG = :GRZ-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           EXEC SQL
                OPEN C-IBO-CPZ-GRZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           EXEC SQL
                CLOSE C-IBO-CPZ-GRZ
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           EXEC SQL
                FETCH C-IBO-CPZ-GRZ
           INTO
                :GRZ-ID-GAR
               ,:GRZ-ID-ADES
                :IND-GRZ-ID-ADES
               ,:GRZ-ID-POLI
               ,:GRZ-ID-MOVI-CRZ
               ,:GRZ-ID-MOVI-CHIU
                :IND-GRZ-ID-MOVI-CHIU
               ,:GRZ-DT-INI-EFF-DB
               ,:GRZ-DT-END-EFF-DB
               ,:GRZ-COD-COMP-ANIA
               ,:GRZ-IB-OGG
                :IND-GRZ-IB-OGG
               ,:GRZ-DT-DECOR-DB
                :IND-GRZ-DT-DECOR
               ,:GRZ-DT-SCAD-DB
                :IND-GRZ-DT-SCAD
               ,:GRZ-COD-SEZ
                :IND-GRZ-COD-SEZ
               ,:GRZ-COD-TARI
               ,:GRZ-RAMO-BILA
                :IND-GRZ-RAMO-BILA
               ,:GRZ-DT-INI-VAL-TAR-DB
                :IND-GRZ-DT-INI-VAL-TAR
               ,:GRZ-ID-1O-ASSTO
                :IND-GRZ-ID-1O-ASSTO
               ,:GRZ-ID-2O-ASSTO
                :IND-GRZ-ID-2O-ASSTO
               ,:GRZ-ID-3O-ASSTO
                :IND-GRZ-ID-3O-ASSTO
               ,:GRZ-TP-GAR
                :IND-GRZ-TP-GAR
               ,:GRZ-TP-RSH
                :IND-GRZ-TP-RSH
               ,:GRZ-TP-INVST
                :IND-GRZ-TP-INVST
               ,:GRZ-MOD-PAG-GARCOL
                :IND-GRZ-MOD-PAG-GARCOL
               ,:GRZ-TP-PER-PRE
                :IND-GRZ-TP-PER-PRE
               ,:GRZ-ETA-AA-1O-ASSTO
                :IND-GRZ-ETA-AA-1O-ASSTO
               ,:GRZ-ETA-MM-1O-ASSTO
                :IND-GRZ-ETA-MM-1O-ASSTO
               ,:GRZ-ETA-AA-2O-ASSTO
                :IND-GRZ-ETA-AA-2O-ASSTO
               ,:GRZ-ETA-MM-2O-ASSTO
                :IND-GRZ-ETA-MM-2O-ASSTO
               ,:GRZ-ETA-AA-3O-ASSTO
                :IND-GRZ-ETA-AA-3O-ASSTO
               ,:GRZ-ETA-MM-3O-ASSTO
                :IND-GRZ-ETA-MM-3O-ASSTO
               ,:GRZ-TP-EMIS-PUR
                :IND-GRZ-TP-EMIS-PUR
               ,:GRZ-ETA-A-SCAD
                :IND-GRZ-ETA-A-SCAD
               ,:GRZ-TP-CALC-PRE-PRSTZ
                :IND-GRZ-TP-CALC-PRE-PRSTZ
               ,:GRZ-TP-PRE
                :IND-GRZ-TP-PRE
               ,:GRZ-TP-DUR
                :IND-GRZ-TP-DUR
               ,:GRZ-DUR-AA
                :IND-GRZ-DUR-AA
               ,:GRZ-DUR-MM
                :IND-GRZ-DUR-MM
               ,:GRZ-DUR-GG
                :IND-GRZ-DUR-GG
               ,:GRZ-NUM-AA-PAG-PRE
                :IND-GRZ-NUM-AA-PAG-PRE
               ,:GRZ-AA-PAG-PRE-UNI
                :IND-GRZ-AA-PAG-PRE-UNI
               ,:GRZ-MM-PAG-PRE-UNI
                :IND-GRZ-MM-PAG-PRE-UNI
               ,:GRZ-FRAZ-INI-EROG-REN
                :IND-GRZ-FRAZ-INI-EROG-REN
               ,:GRZ-MM-1O-RAT
                :IND-GRZ-MM-1O-RAT
               ,:GRZ-PC-1O-RAT
                :IND-GRZ-PC-1O-RAT
               ,:GRZ-TP-PRSTZ-ASSTA
                :IND-GRZ-TP-PRSTZ-ASSTA
               ,:GRZ-DT-END-CARZ-DB
                :IND-GRZ-DT-END-CARZ
               ,:GRZ-PC-RIP-PRE
                :IND-GRZ-PC-RIP-PRE
               ,:GRZ-COD-FND
                :IND-GRZ-COD-FND
               ,:GRZ-AA-REN-CER
                :IND-GRZ-AA-REN-CER
               ,:GRZ-PC-REVRSB
                :IND-GRZ-PC-REVRSB
               ,:GRZ-TP-PC-RIP
                :IND-GRZ-TP-PC-RIP
               ,:GRZ-PC-OPZ
                :IND-GRZ-PC-OPZ
               ,:GRZ-TP-IAS
                :IND-GRZ-TP-IAS
               ,:GRZ-TP-STAB
                :IND-GRZ-TP-STAB
               ,:GRZ-TP-ADEG-PRE
                :IND-GRZ-TP-ADEG-PRE
               ,:GRZ-DT-VARZ-TP-IAS-DB
                :IND-GRZ-DT-VARZ-TP-IAS
               ,:GRZ-FRAZ-DECR-CPT
                :IND-GRZ-FRAZ-DECR-CPT
               ,:GRZ-COD-TRAT-RIASS
                :IND-GRZ-COD-TRAT-RIASS
               ,:GRZ-TP-DT-EMIS-RIASS
                :IND-GRZ-TP-DT-EMIS-RIASS
               ,:GRZ-TP-CESS-RIASS
                :IND-GRZ-TP-CESS-RIASS
               ,:GRZ-DS-RIGA
               ,:GRZ-DS-OPER-SQL
               ,:GRZ-DS-VER
               ,:GRZ-DS-TS-INI-CPTZ
               ,:GRZ-DS-TS-END-CPTZ
               ,:GRZ-DS-UTENTE
               ,:GRZ-DS-STATO-ELAB
               ,:GRZ-AA-STAB
                :IND-GRZ-AA-STAB
               ,:GRZ-TS-STAB-LIMITATA
                :IND-GRZ-TS-STAB-LIMITATA
               ,:GRZ-DT-PRESC-DB
                :IND-GRZ-DT-PRESC
               ,:GRZ-RSH-INVST
                :IND-GRZ-RSH-INVST
               ,:GRZ-TP-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B570-CLOSE-CURSOR-IBO-CPZ     THRU B570-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-GRZ-ID-ADES = -1
              MOVE HIGH-VALUES TO GRZ-ID-ADES-NULL
           END-IF
           IF IND-GRZ-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO GRZ-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-GRZ-IB-OGG = -1
              MOVE HIGH-VALUES TO GRZ-IB-OGG-NULL
           END-IF
           IF IND-GRZ-DT-DECOR = -1
              MOVE HIGH-VALUES TO GRZ-DT-DECOR-NULL
           END-IF
           IF IND-GRZ-DT-SCAD = -1
              MOVE HIGH-VALUES TO GRZ-DT-SCAD-NULL
           END-IF
           IF IND-GRZ-COD-SEZ = -1
              MOVE HIGH-VALUES TO GRZ-COD-SEZ-NULL
           END-IF
           IF IND-GRZ-RAMO-BILA = -1
              MOVE HIGH-VALUES TO GRZ-RAMO-BILA-NULL
           END-IF
           IF IND-GRZ-DT-INI-VAL-TAR = -1
              MOVE HIGH-VALUES TO GRZ-DT-INI-VAL-TAR-NULL
           END-IF
           IF IND-GRZ-ID-1O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ID-1O-ASSTO-NULL
           END-IF
           IF IND-GRZ-ID-2O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ID-2O-ASSTO-NULL
           END-IF
           IF IND-GRZ-ID-3O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ID-3O-ASSTO-NULL
           END-IF
           IF IND-GRZ-TP-GAR = -1
              MOVE HIGH-VALUES TO GRZ-TP-GAR-NULL
           END-IF
           IF IND-GRZ-TP-RSH = -1
              MOVE HIGH-VALUES TO GRZ-TP-RSH-NULL
           END-IF
           IF IND-GRZ-TP-INVST = -1
              MOVE HIGH-VALUES TO GRZ-TP-INVST-NULL
           END-IF
           IF IND-GRZ-MOD-PAG-GARCOL = -1
              MOVE HIGH-VALUES TO GRZ-MOD-PAG-GARCOL-NULL
           END-IF
           IF IND-GRZ-TP-PER-PRE = -1
              MOVE HIGH-VALUES TO GRZ-TP-PER-PRE-NULL
           END-IF
           IF IND-GRZ-ETA-AA-1O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ETA-AA-1O-ASSTO-NULL
           END-IF
           IF IND-GRZ-ETA-MM-1O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ETA-MM-1O-ASSTO-NULL
           END-IF
           IF IND-GRZ-ETA-AA-2O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ETA-AA-2O-ASSTO-NULL
           END-IF
           IF IND-GRZ-ETA-MM-2O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ETA-MM-2O-ASSTO-NULL
           END-IF
           IF IND-GRZ-ETA-AA-3O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ETA-AA-3O-ASSTO-NULL
           END-IF
           IF IND-GRZ-ETA-MM-3O-ASSTO = -1
              MOVE HIGH-VALUES TO GRZ-ETA-MM-3O-ASSTO-NULL
           END-IF
           IF IND-GRZ-TP-EMIS-PUR = -1
              MOVE HIGH-VALUES TO GRZ-TP-EMIS-PUR-NULL
           END-IF
           IF IND-GRZ-ETA-A-SCAD = -1
              MOVE HIGH-VALUES TO GRZ-ETA-A-SCAD-NULL
           END-IF
           IF IND-GRZ-TP-CALC-PRE-PRSTZ = -1
              MOVE HIGH-VALUES TO GRZ-TP-CALC-PRE-PRSTZ-NULL
           END-IF
           IF IND-GRZ-TP-PRE = -1
              MOVE HIGH-VALUES TO GRZ-TP-PRE-NULL
           END-IF
           IF IND-GRZ-TP-DUR = -1
              MOVE HIGH-VALUES TO GRZ-TP-DUR-NULL
           END-IF
           IF IND-GRZ-DUR-AA = -1
              MOVE HIGH-VALUES TO GRZ-DUR-AA-NULL
           END-IF
           IF IND-GRZ-DUR-MM = -1
              MOVE HIGH-VALUES TO GRZ-DUR-MM-NULL
           END-IF
           IF IND-GRZ-DUR-GG = -1
              MOVE HIGH-VALUES TO GRZ-DUR-GG-NULL
           END-IF
           IF IND-GRZ-NUM-AA-PAG-PRE = -1
              MOVE HIGH-VALUES TO GRZ-NUM-AA-PAG-PRE-NULL
           END-IF
           IF IND-GRZ-AA-PAG-PRE-UNI = -1
              MOVE HIGH-VALUES TO GRZ-AA-PAG-PRE-UNI-NULL
           END-IF
           IF IND-GRZ-MM-PAG-PRE-UNI = -1
              MOVE HIGH-VALUES TO GRZ-MM-PAG-PRE-UNI-NULL
           END-IF
           IF IND-GRZ-FRAZ-INI-EROG-REN = -1
              MOVE HIGH-VALUES TO GRZ-FRAZ-INI-EROG-REN-NULL
           END-IF
           IF IND-GRZ-MM-1O-RAT = -1
              MOVE HIGH-VALUES TO GRZ-MM-1O-RAT-NULL
           END-IF
           IF IND-GRZ-PC-1O-RAT = -1
              MOVE HIGH-VALUES TO GRZ-PC-1O-RAT-NULL
           END-IF
           IF IND-GRZ-TP-PRSTZ-ASSTA = -1
              MOVE HIGH-VALUES TO GRZ-TP-PRSTZ-ASSTA-NULL
           END-IF
           IF IND-GRZ-DT-END-CARZ = -1
              MOVE HIGH-VALUES TO GRZ-DT-END-CARZ-NULL
           END-IF
           IF IND-GRZ-PC-RIP-PRE = -1
              MOVE HIGH-VALUES TO GRZ-PC-RIP-PRE-NULL
           END-IF
           IF IND-GRZ-COD-FND = -1
              MOVE HIGH-VALUES TO GRZ-COD-FND-NULL
           END-IF
           IF IND-GRZ-AA-REN-CER = -1
              MOVE HIGH-VALUES TO GRZ-AA-REN-CER-NULL
           END-IF
           IF IND-GRZ-PC-REVRSB = -1
              MOVE HIGH-VALUES TO GRZ-PC-REVRSB-NULL
           END-IF
           IF IND-GRZ-TP-PC-RIP = -1
              MOVE HIGH-VALUES TO GRZ-TP-PC-RIP-NULL
           END-IF
           IF IND-GRZ-PC-OPZ = -1
              MOVE HIGH-VALUES TO GRZ-PC-OPZ-NULL
           END-IF
           IF IND-GRZ-TP-IAS = -1
              MOVE HIGH-VALUES TO GRZ-TP-IAS-NULL
           END-IF
           IF IND-GRZ-TP-STAB = -1
              MOVE HIGH-VALUES TO GRZ-TP-STAB-NULL
           END-IF
           IF IND-GRZ-TP-ADEG-PRE = -1
              MOVE HIGH-VALUES TO GRZ-TP-ADEG-PRE-NULL
           END-IF
           IF IND-GRZ-DT-VARZ-TP-IAS = -1
              MOVE HIGH-VALUES TO GRZ-DT-VARZ-TP-IAS-NULL
           END-IF
           IF IND-GRZ-FRAZ-DECR-CPT = -1
              MOVE HIGH-VALUES TO GRZ-FRAZ-DECR-CPT-NULL
           END-IF
           IF IND-GRZ-COD-TRAT-RIASS = -1
              MOVE HIGH-VALUES TO GRZ-COD-TRAT-RIASS-NULL
           END-IF
           IF IND-GRZ-TP-DT-EMIS-RIASS = -1
              MOVE HIGH-VALUES TO GRZ-TP-DT-EMIS-RIASS-NULL
           END-IF
           IF IND-GRZ-TP-CESS-RIASS = -1
              MOVE HIGH-VALUES TO GRZ-TP-CESS-RIASS-NULL
           END-IF
           IF IND-GRZ-AA-STAB = -1
              MOVE HIGH-VALUES TO GRZ-AA-STAB-NULL
           END-IF
           IF IND-GRZ-TS-STAB-LIMITATA = -1
              MOVE HIGH-VALUES TO GRZ-TS-STAB-LIMITATA-NULL
           END-IF
           IF IND-GRZ-DT-PRESC = -1
              MOVE HIGH-VALUES TO GRZ-DT-PRESC-NULL
           END-IF
           IF IND-GRZ-RSH-INVST = -1
              MOVE HIGH-VALUES TO GRZ-RSH-INVST-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO GRZ-DS-OPER-SQL
           MOVE 0                   TO GRZ-DS-VER
           MOVE IDSV0003-USER-NAME TO GRZ-DS-UTENTE
           MOVE '1'                   TO GRZ-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO GRZ-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO GRZ-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF GRZ-ID-ADES-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-ID-ADES
           ELSE
              MOVE 0 TO IND-GRZ-ID-ADES
           END-IF
           IF GRZ-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-GRZ-ID-MOVI-CHIU
           END-IF
           IF GRZ-IB-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-IB-OGG
           ELSE
              MOVE 0 TO IND-GRZ-IB-OGG
           END-IF
           IF GRZ-DT-DECOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-DT-DECOR
           ELSE
              MOVE 0 TO IND-GRZ-DT-DECOR
           END-IF
           IF GRZ-DT-SCAD-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-DT-SCAD
           ELSE
              MOVE 0 TO IND-GRZ-DT-SCAD
           END-IF
           IF GRZ-COD-SEZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-COD-SEZ
           ELSE
              MOVE 0 TO IND-GRZ-COD-SEZ
           END-IF
           IF GRZ-RAMO-BILA-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-RAMO-BILA
           ELSE
              MOVE 0 TO IND-GRZ-RAMO-BILA
           END-IF
           IF GRZ-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-DT-INI-VAL-TAR
           ELSE
              MOVE 0 TO IND-GRZ-DT-INI-VAL-TAR
           END-IF
           IF GRZ-ID-1O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-ID-1O-ASSTO
           ELSE
              MOVE 0 TO IND-GRZ-ID-1O-ASSTO
           END-IF
           IF GRZ-ID-2O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-ID-2O-ASSTO
           ELSE
              MOVE 0 TO IND-GRZ-ID-2O-ASSTO
           END-IF
           IF GRZ-ID-3O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-ID-3O-ASSTO
           ELSE
              MOVE 0 TO IND-GRZ-ID-3O-ASSTO
           END-IF
           IF GRZ-TP-GAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TP-GAR
           ELSE
              MOVE 0 TO IND-GRZ-TP-GAR
           END-IF
           IF GRZ-TP-RSH-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TP-RSH
           ELSE
              MOVE 0 TO IND-GRZ-TP-RSH
           END-IF
           IF GRZ-TP-INVST-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TP-INVST
           ELSE
              MOVE 0 TO IND-GRZ-TP-INVST
           END-IF
           IF GRZ-MOD-PAG-GARCOL-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-MOD-PAG-GARCOL
           ELSE
              MOVE 0 TO IND-GRZ-MOD-PAG-GARCOL
           END-IF
           IF GRZ-TP-PER-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TP-PER-PRE
           ELSE
              MOVE 0 TO IND-GRZ-TP-PER-PRE
           END-IF
           IF GRZ-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-ETA-AA-1O-ASSTO
           ELSE
              MOVE 0 TO IND-GRZ-ETA-AA-1O-ASSTO
           END-IF
           IF GRZ-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-ETA-MM-1O-ASSTO
           ELSE
              MOVE 0 TO IND-GRZ-ETA-MM-1O-ASSTO
           END-IF
           IF GRZ-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-ETA-AA-2O-ASSTO
           ELSE
              MOVE 0 TO IND-GRZ-ETA-AA-2O-ASSTO
           END-IF
           IF GRZ-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-ETA-MM-2O-ASSTO
           ELSE
              MOVE 0 TO IND-GRZ-ETA-MM-2O-ASSTO
           END-IF
           IF GRZ-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-ETA-AA-3O-ASSTO
           ELSE
              MOVE 0 TO IND-GRZ-ETA-AA-3O-ASSTO
           END-IF
           IF GRZ-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-ETA-MM-3O-ASSTO
           ELSE
              MOVE 0 TO IND-GRZ-ETA-MM-3O-ASSTO
           END-IF
           IF GRZ-TP-EMIS-PUR-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TP-EMIS-PUR
           ELSE
              MOVE 0 TO IND-GRZ-TP-EMIS-PUR
           END-IF
           IF GRZ-ETA-A-SCAD-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-ETA-A-SCAD
           ELSE
              MOVE 0 TO IND-GRZ-ETA-A-SCAD
           END-IF
           IF GRZ-TP-CALC-PRE-PRSTZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TP-CALC-PRE-PRSTZ
           ELSE
              MOVE 0 TO IND-GRZ-TP-CALC-PRE-PRSTZ
           END-IF
           IF GRZ-TP-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TP-PRE
           ELSE
              MOVE 0 TO IND-GRZ-TP-PRE
           END-IF
           IF GRZ-TP-DUR-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TP-DUR
           ELSE
              MOVE 0 TO IND-GRZ-TP-DUR
           END-IF
           IF GRZ-DUR-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-DUR-AA
           ELSE
              MOVE 0 TO IND-GRZ-DUR-AA
           END-IF
           IF GRZ-DUR-MM-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-DUR-MM
           ELSE
              MOVE 0 TO IND-GRZ-DUR-MM
           END-IF
           IF GRZ-DUR-GG-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-DUR-GG
           ELSE
              MOVE 0 TO IND-GRZ-DUR-GG
           END-IF
           IF GRZ-NUM-AA-PAG-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-NUM-AA-PAG-PRE
           ELSE
              MOVE 0 TO IND-GRZ-NUM-AA-PAG-PRE
           END-IF
           IF GRZ-AA-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-AA-PAG-PRE-UNI
           ELSE
              MOVE 0 TO IND-GRZ-AA-PAG-PRE-UNI
           END-IF
           IF GRZ-MM-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-MM-PAG-PRE-UNI
           ELSE
              MOVE 0 TO IND-GRZ-MM-PAG-PRE-UNI
           END-IF
           IF GRZ-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-FRAZ-INI-EROG-REN
           ELSE
              MOVE 0 TO IND-GRZ-FRAZ-INI-EROG-REN
           END-IF
           IF GRZ-MM-1O-RAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-MM-1O-RAT
           ELSE
              MOVE 0 TO IND-GRZ-MM-1O-RAT
           END-IF
           IF GRZ-PC-1O-RAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-PC-1O-RAT
           ELSE
              MOVE 0 TO IND-GRZ-PC-1O-RAT
           END-IF
           IF GRZ-TP-PRSTZ-ASSTA-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TP-PRSTZ-ASSTA
           ELSE
              MOVE 0 TO IND-GRZ-TP-PRSTZ-ASSTA
           END-IF
           IF GRZ-DT-END-CARZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-DT-END-CARZ
           ELSE
              MOVE 0 TO IND-GRZ-DT-END-CARZ
           END-IF
           IF GRZ-PC-RIP-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-PC-RIP-PRE
           ELSE
              MOVE 0 TO IND-GRZ-PC-RIP-PRE
           END-IF
           IF GRZ-COD-FND-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-COD-FND
           ELSE
              MOVE 0 TO IND-GRZ-COD-FND
           END-IF
           IF GRZ-AA-REN-CER-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-AA-REN-CER
           ELSE
              MOVE 0 TO IND-GRZ-AA-REN-CER
           END-IF
           IF GRZ-PC-REVRSB-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-PC-REVRSB
           ELSE
              MOVE 0 TO IND-GRZ-PC-REVRSB
           END-IF
           IF GRZ-TP-PC-RIP-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TP-PC-RIP
           ELSE
              MOVE 0 TO IND-GRZ-TP-PC-RIP
           END-IF
           IF GRZ-PC-OPZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-PC-OPZ
           ELSE
              MOVE 0 TO IND-GRZ-PC-OPZ
           END-IF
           IF GRZ-TP-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TP-IAS
           ELSE
              MOVE 0 TO IND-GRZ-TP-IAS
           END-IF
           IF GRZ-TP-STAB-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TP-STAB
           ELSE
              MOVE 0 TO IND-GRZ-TP-STAB
           END-IF
           IF GRZ-TP-ADEG-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TP-ADEG-PRE
           ELSE
              MOVE 0 TO IND-GRZ-TP-ADEG-PRE
           END-IF
           IF GRZ-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-DT-VARZ-TP-IAS
           ELSE
              MOVE 0 TO IND-GRZ-DT-VARZ-TP-IAS
           END-IF
           IF GRZ-FRAZ-DECR-CPT-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-FRAZ-DECR-CPT
           ELSE
              MOVE 0 TO IND-GRZ-FRAZ-DECR-CPT
           END-IF
           IF GRZ-COD-TRAT-RIASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-COD-TRAT-RIASS
           ELSE
              MOVE 0 TO IND-GRZ-COD-TRAT-RIASS
           END-IF
           IF GRZ-TP-DT-EMIS-RIASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TP-DT-EMIS-RIASS
           ELSE
              MOVE 0 TO IND-GRZ-TP-DT-EMIS-RIASS
           END-IF
           IF GRZ-TP-CESS-RIASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TP-CESS-RIASS
           ELSE
              MOVE 0 TO IND-GRZ-TP-CESS-RIASS
           END-IF
           IF GRZ-AA-STAB-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-AA-STAB
           ELSE
              MOVE 0 TO IND-GRZ-AA-STAB
           END-IF
           IF GRZ-TS-STAB-LIMITATA-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-TS-STAB-LIMITATA
           ELSE
              MOVE 0 TO IND-GRZ-TS-STAB-LIMITATA
           END-IF
           IF GRZ-DT-PRESC-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-DT-PRESC
           ELSE
              MOVE 0 TO IND-GRZ-DT-PRESC
           END-IF
           IF GRZ-RSH-INVST-NULL = HIGH-VALUES
              MOVE -1 TO IND-GRZ-RSH-INVST
           ELSE
              MOVE 0 TO IND-GRZ-RSH-INVST
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : GRZ-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE GAR TO WS-BUFFER-TABLE.

           MOVE GRZ-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO GRZ-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO GRZ-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO GRZ-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO GRZ-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO GRZ-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO GRZ-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO GRZ-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE GAR TO WS-BUFFER-TABLE.

           MOVE GRZ-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO GAR.

           MOVE WS-ID-MOVI-CRZ  TO GRZ-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO GRZ-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO GRZ-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO GRZ-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO GRZ-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO GRZ-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO GRZ-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE GRZ-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO GRZ-DT-INI-EFF-DB
           MOVE GRZ-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO GRZ-DT-END-EFF-DB
           IF IND-GRZ-DT-DECOR = 0
               MOVE GRZ-DT-DECOR TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO GRZ-DT-DECOR-DB
           END-IF
           IF IND-GRZ-DT-SCAD = 0
               MOVE GRZ-DT-SCAD TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO GRZ-DT-SCAD-DB
           END-IF
           IF IND-GRZ-DT-INI-VAL-TAR = 0
               MOVE GRZ-DT-INI-VAL-TAR TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO GRZ-DT-INI-VAL-TAR-DB
           END-IF
           IF IND-GRZ-DT-END-CARZ = 0
               MOVE GRZ-DT-END-CARZ TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO GRZ-DT-END-CARZ-DB
           END-IF
           IF IND-GRZ-DT-VARZ-TP-IAS = 0
               MOVE GRZ-DT-VARZ-TP-IAS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO GRZ-DT-VARZ-TP-IAS-DB
           END-IF
           IF IND-GRZ-DT-PRESC = 0
               MOVE GRZ-DT-PRESC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO GRZ-DT-PRESC-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE GRZ-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO GRZ-DT-INI-EFF
           MOVE GRZ-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO GRZ-DT-END-EFF
           IF IND-GRZ-DT-DECOR = 0
               MOVE GRZ-DT-DECOR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO GRZ-DT-DECOR
           END-IF
           IF IND-GRZ-DT-SCAD = 0
               MOVE GRZ-DT-SCAD-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO GRZ-DT-SCAD
           END-IF
           IF IND-GRZ-DT-INI-VAL-TAR = 0
               MOVE GRZ-DT-INI-VAL-TAR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO GRZ-DT-INI-VAL-TAR
           END-IF
           IF IND-GRZ-DT-END-CARZ = 0
               MOVE GRZ-DT-END-CARZ-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO GRZ-DT-END-CARZ
           END-IF
           IF IND-GRZ-DT-VARZ-TP-IAS = 0
               MOVE GRZ-DT-VARZ-TP-IAS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO GRZ-DT-VARZ-TP-IAS
           END-IF
           IF IND-GRZ-DT-PRESC = 0
               MOVE GRZ-DT-PRESC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO GRZ-DT-PRESC
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

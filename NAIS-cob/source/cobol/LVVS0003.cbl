      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0003.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0003
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0003'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
29885 *--  Accesso MOVI
29885  77  PGM-LDBSE060                     PIC X(008) VALUE 'LDBSE060'.
28693 *--  Accesso TRCH_DI_GAR
28693  77  PGM-LDBSD980                     PIC X(008) VALUE 'LDBSD980'.
       01  WK-PGM-CALLED                    PIC X(08) VALUE '        '.
       77  LDBS7850                         PIC X(08) VALUE 'LDBS7850'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       77  WK-LABEL                        PIC X(030).
       01  WK-DATA-OUTPUT                  PIC 9(04)V9(07).
       01  WK-DATA-OUTPUT-V REDEFINES WK-DATA-OUTPUT.
           03 WK-NUMERICI                  PIC 9(04).
           03 WK-DECIMALI                  PIC 9(07).

       01 WK-MOV-ID-MOVI                   PIC S9(09)V COMP-3.
       01 WK-MOV-DT-EFF                    PIC S9(08)V COMP-3.
       01 WK-MOV-DS-TS-CPTZ                PIC S9(18)V COMP-3.
       01 WK-DATA-OUT                      PIC 9(08) VALUE ZEROES.
       01 IX-TAB-TGA                       PIC 9(4) COMP VALUE ZEROES.
      *----------------------------------------------------------------*
      *    FLAG
      *----------------------------------------------------------------*
       01 FLAG-MOVIMENTO                   PIC X(001).
          88 OK-MOVI                         VALUE 'S'.
          88 KO-MOVI                         VALUE 'N'.
      *----------------------------------------------------------------*
      *  COPY TIPOLOGICHE
      *----------------------------------------------------------------*
           COPY LCCVXMV0.
           COPY LCCVXOG0.
      *----------------------------------------------------------------*
      * AREA TABELLE DB2
      *----------------------------------------------------------------*
           COPY IDBVPOL1.
MIGR       COPY IDBVADE1.
           COPY IDBVMOV1.
           COPY IDBVTGA1.
           COPY IDBVPMO1.
29885      COPY LDBVE061.
29885      COPY LDBV7851.
      *----------------------------------------------------------------*
      *    COPY DI INPUT PER LA CHIAMATA AL LVVS0000
      *----------------------------------------------------------------*
       01  INPUT-LVVS0000.
           COPY LVVC0000        REPLACING ==(SF)== BY ==LVVC0000==.
      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBS1570
      *---------------------------------------------------------------*
           COPY LDBV1571.

      *----------------------------------------------------------------*
      *--> AREA TRCH-DI-GAR
      *----------------------------------------------------------------*

       01 AREA-IO-TRCH.
          03 DTGA-AREA-TRCH.
             04 DTGA-ELE-TRCH-MAX       PIC S9(04) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==DTGA==.
             COPY LCCVTGA1              REPLACING ==(SF)== BY ==DTGA==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0003.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0003.
      *----------------------------------------------------------------*
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-TRCH
                      WK-DATA-OUTPUT.
      *
      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.
      *
           PERFORM L450-LEGGI-POLIZZA
              THRU L450-LEGGI-POLIZZA-EX

MIGR       IF  IDSV0003-SUCCESSFUL-RC
MIGR       AND IDSV0003-SUCCESSFUL-SQL
MIGR        IF POL-TP-FRM-ASSVA = 'CO'
MIGR           PERFORM L460-LEGGI-ADESIONE
MIGR              THRU L460-LEGGI-ADESIONE-EX
MIGR        END-IF
MIGR       END-IF

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1253-RECUP-MOVI            THRU S1253-EX
               IF KO-MOVI
                  IF POL-TP-FRM-ASSVA = 'CO'
                     PERFORM LEGGI-PMO
                        THRU LEGGI-PMO-EX
                  END-IF
               END-IF
               IF IDSV0003-SUCCESSFUL-SQL
                  IF OK-MOVI
                     PERFORM S1400-CALC-DATA       THRU EX-S1400
                  ELSE
                     MOVE DTGA-DT-DECOR(IVVC0213-IX-TABB)
                       TO IVVC0213-VAL-STR-O
                          WK-DATA-OUT
MIGCOL*              IF IDSV0003-USER-NAME = 'MIGCOL'
SIR1           IF DTGA-DT-ULT-ADEG-PRE-PR-NULL(IVVC0213-IX-TABB)
SIR1                                           NOT = HIGH-VALUES
SIR1                                             AND LOW-VALUES
SIR1                                             AND SPACES
MIGCOL                  MOVE DTGA-DT-ULT-ADEG-PRE-PR(IVVC0213-IX-TABB)
MIGCOL                    TO IVVC0213-VAL-STR-O
MIGCOL                       WK-DATA-OUT
SIR1           END-IF
MIGCOL*              END-IF
                     EVALUATE IVVC0213-COD-VARIABILE-O
                         WHEN 'DATULTADEGPL'
                         WHEN 'DTULTADEGTL'
                             INITIALIZE INPUT-LVVS0000
                             MOVE WK-DATA-OUT
                               TO LVVC0000-DATA-INPUT-1
                             PERFORM S1300-CALL-LVVS0000
                                THRU S1300-EX
                     END-EVALUATE
                   END-IF
               END-IF
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *-- LETTURA DELLA POLIZZA
      *----------------------------------------------------------------*
       L450-LEGGI-POLIZZA.

           INITIALIZE POLI.

           MOVE IVVC0213-ID-POLIZZA       TO POL-ID-POLI.

           MOVE 'IDBSPOL0'                TO WK-PGM-CALLED.

      *  --> Tipo operazione
           SET IDSV0003-SELECT            TO TRUE.
      *  --> Tipo livello
           SET IDSV0003-ID                TO TRUE.

           CALL WK-PGM-CALLED USING IDSV0003 POLI

           ON EXCEPTION
              MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-IDBSPOL0 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER   TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              CONTINUE
           ELSE
              MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA IDBSPOL0 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                     DELIMITED BY SIZE INTO
                     IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER     TO TRUE
              END-IF
           END-IF.

       L450-LEGGI-POLIZZA-EX.
           EXIT.
MIGR  *----------------------------------------------------------------*
      *-- LETTURA DELL' ADESIONE
      *----------------------------------------------------------------*
       L460-LEGGI-ADESIONE.

           INITIALIZE ADES.

           MOVE IVVC0213-ID-ADESIONE      TO ADE-ID-ADES.

           MOVE 'IDBSADE0'                TO WK-PGM-CALLED.

      *  --> Tipo operazione
           SET IDSV0003-SELECT            TO TRUE.
      *  --> Tipo livello
           SET IDSV0003-ID                TO TRUE.

           CALL WK-PGM-CALLED USING IDSV0003 ADES

           ON EXCEPTION
              MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-IDBSADE0 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER   TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              CONTINUE
           ELSE
              MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA IDBSADE0 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                     DELIMITED BY SIZE INTO
                     IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER     TO TRUE
              END-IF
           END-IF.

       L460-LEGGI-ADESIONE-EX.
           EXIT.
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *-- LETTURA DELLA PARAM MOVI
      *----------------------------------------------------------------*
       LEGGI-PMO.
           INITIALIZE LDBV7851
           INITIALIZE PARAM-MOVI
           MOVE DTGA-ID-GAR(IVVC0213-IX-TABB)  TO LDBV7851-ID-OGG
           MOVE 'GA'                           TO LDBV7851-TP-OGG
           MOVE 06006                          TO LDBV7851-TP-MOVI-01
           MOVE ZERO                           TO LDBV7851-TP-MOVI-02
           MOVE ZERO                           TO LDBV7851-TP-MOVI-03
           MOVE LDBV7851                TO  IDSV0003-BUFFER-WHERE-COND
      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
           MOVE 'LDBS7850'                TO WK-PGM-CALLED.

      *  --> Tipo operazione
GRAV       SET IDSV0003-SELECT            TO TRUE.
      *  --> Tipo livello
      *--> LIVELLO OPERAZIONE
           SET IDSV0003-WHERE-CONDITION       TO TRUE
      *--> LIVELLO OPERAZIONE
           SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE

GRAV       CALL WK-PGM-CALLED USING IDSV0003 PARAM-MOVI
GRAV  *    CALL WK-PGM-CALLED USING IDSV0003 LDBV7851

           ON EXCEPTION
              MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-IDBSPMO0 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER   TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              SET OK-MOVI TO TRUE
           ELSE
              MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA IDBSPMO0 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                     DELIMITED BY SIZE INTO
                     IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER     TO TRUE
              END-IF
           END-IF.

       LEGGI-PMO-EX.
           EXIT.
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-TRCH-GAR
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DTGA-AREA-TRCH
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      * RECUPERO I DATI RELATIVI AL MOVIMENTO
      *----------------------------------------------------------------*
       S1253-RECUP-MOVI.

           SET KO-MOVI                     TO TRUE

           MOVE ZEROES                     TO WK-MOV-DT-EFF
                                              WK-MOV-DS-TS-CPTZ
           INITIALIZE MOVI

MIGR       IF POL-TP-FRM-ASSVA = 'CO'
MIGR          MOVE IVVC0213-ID-ADESIONE      TO LDBVE061-ID-OGG
MIGR          SET  ADESIONE                  TO TRUE
MIGR          MOVE WS-TP-OGG                 TO LDBVE061-TP-OGG
MIGR       ELSE
29885         MOVE IVVC0213-ID-POLIZZA       TO LDBVE061-ID-OGG
              SET  POLIZZA                   TO TRUE
29885         MOVE WS-TP-OGG                 TO LDBVE061-TP-OGG
MIGR       END-IF

           SET ADPRE-PRESTA               TO TRUE
29885      MOVE WS-MOVIMENTO              TO LDBVE061-TP-MOVI

29185      MOVE IDSV0003-DATA-INIZIO-EFFETTO  TO
29885                                LDBVE061-DT-EFF-I
29185      MOVE ZEROES TO                  IDSV0003-DATA-FINE-EFFETTO
29185      MOVE IDSV0003-DATA-COMPETENZA TO MOV-DS-TS-CPTZ


      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

           SET  IDSV0003-TRATT-SENZA-STOR TO TRUE

      *--> LIVELLO OPERAZIONE
           SET IDSV0003-WHERE-CONDITION   TO TRUE

      *--> TIPO OPERAZIONE
           SET  IDSV0003-SELECT           TO TRUE.
      *
29885      CALL PGM-LDBSE060  USING  IDSV0003 LDBVE061
      *
           ON EXCEPTION
29885         MOVE PGM-LDBSE060
                TO IDSV0003-COD-SERVIZIO-BE
29885        MOVE 'CALL-LDBSE060 - ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              SET OK-MOVI                 TO TRUE
29885         MOVE  LDBVE061-DT-EFF-O     TO WK-MOV-DT-EFF
29885         MOVE  LDBVE061-DS-CPTZ-O    TO WK-MOV-DS-TS-CPTZ
           ELSE
              IF IDSV0003-SQLCODE = +100
                 SET IDSV0003-SUCCESSFUL-SQL    TO TRUE
                 SET IDSV0003-SUCCESSFUL-RC     TO TRUE
                 MOVE ZERO                      TO IDSV0003-SQLCODE
                 SET KO-MOVI                    TO TRUE
              ELSE
29885            MOVE PGM-LDBSE060        TO IDSV0003-COD-SERVIZIO-BE
29885            STRING 'CHIAMATA LDBSE060  '
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING
                 SET IDSV0003-INVALID-OPER       TO TRUE
              END-IF
           END-IF.

       S1253-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CHIAMATA LVVS0000
      *----------------------------------------------------------------*
       S1300-CALL-LVVS0000.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET IDSV0003-SUCCESSFUL-RC     TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL    TO TRUE
           MOVE 'LVVS0000'                TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM USING         IDSV0003
                                          INPUT-LVVS0000.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               MOVE LVVC0000-DATA-OUTPUT  TO IVVC0213-VAL-IMP-O
           END-IF.
      *
       S1300-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CALCOLA DATA
      *----------------------------------------------------------------*
       S1400-CALC-DATA.
              IF POL-TP-FRM-ASSVA = 'IN'
                 IF WK-MOV-DT-EFF >
                    DTGA-DT-DECOR(IVVC0213-IX-TABB)
                      MOVE WK-MOV-DT-EFF
                        TO IVVC0213-VAL-STR-O
                           WK-DATA-OUT
                 ELSE
                      MOVE DTGA-DT-DECOR(IVVC0213-IX-TABB)
                        TO IVVC0213-VAL-STR-O
                           WK-DATA-OUT
                 END-IF
              ELSE
MIGR           IF DTGA-DT-ULT-ADEG-PRE-PR-NULL(IVVC0213-IX-TABB)
MIGR                                           NOT = HIGH-VALUES
MIGR                                             AND LOW-VALUES
MIGR                                             AND SPACES
MIGR             MOVE DTGA-DT-ULT-ADEG-PRE-PR(IVVC0213-IX-TABB)
MIGR               TO WK-DATA-OUT
MIGR           ELSE
                 MOVE PMO-DT-RICOR-SUCC
                   TO WK-DATA-OUT
MIGR           END-IF
              END-IF
           EVALUATE IVVC0213-COD-VARIABILE-O
              WHEN 'DATULTADEGPL'
              WHEN 'DTULTADEGTL'
                 INITIALIZE INPUT-LVVS0000
                 MOVE WK-DATA-OUT              TO LVVC0000-DATA-INPUT-1
                 PERFORM S1300-CALL-LVVS0000   THRU S1300-EX
           END-EVALUATE.

       EX-S1400.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           MOVE IVVC0213-DATA-EFFETTO       TO
                IDSV0003-DATA-INIZIO-EFFETTO

           MOVE IVVC0213-DATA-COMPETENZA    TO
                IDSV0003-DATA-COMPETENZA

           GOBACK.
      *
       EX-S9000.
           EXIT.


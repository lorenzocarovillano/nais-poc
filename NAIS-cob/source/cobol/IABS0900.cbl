       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IABS0900 IS INITIAL.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : SERVIZIO ESTRAZIONE OCCORRENZE              *
      *                   DA TABELLE GUIDA RICHIAMATO DAL BATCH EXEC. *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2       PIC X(300)      VALUE SPACES.

       77 PRENOTAZIONE          PIC X(002)      VALUE 'PR'.

       01 FLAG-ACCESSO                PIC X(01).
          88 ACCESSO-STD              VALUE '1'.
          88 ACCESSO-MCRFNCT          VALUE '2'.
          88 ACCESSO-TPMV             VALUE '3'.
          88 ACCESSO-MCRFNCT-TPMV     VALUE '4'.

       01 WK-ID-RICH               PIC 9(09).

           EXEC SQL INCLUDE IDSV0010 END-EXEC.


      *---------------------------------------------------------------*
      * STRUTTURA WHERE CONDITION
      *---------------------------------------------------------------*

       01 WS-WHERE-CONDITION.
          05 FILLER                  PIC X(300).


      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDRIC0 END-EXEC.
           EXEC SQL INCLUDE IDBVRIC2 END-EXEC.
           EXEC SQL INCLUDE IDBVRIC3 END-EXEC.

           EXEC SQL INCLUDE IDBDDER0 END-EXEC.
           EXEC SQL INCLUDE IDBVDER1 END-EXEC.
           EXEC SQL INCLUDE IDBVDER2 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IABV0002 END-EXEC.

           EXEC SQL INCLUDE IDBVRIC1 END-EXEC.

           EXEC SQL INCLUDE IABV0003 END-EXEC.

      *****************************************************************
       PROCEDURE DIVISION USING IDSV0003 IABV0002 RICH IABV0003.

           PERFORM A000-INIZIO                 THRU A000-EX.

           PERFORM A060-CNTL-INPUT             THRU A060-EX.

           PERFORM A300-ELABORA                THRU A300-EX.

           PERFORM A400-FINE                   THRU A400-EX.

           GOBACK.

      ******************************************************************
       A000-INIZIO.

           MOVE 'IABS0900'              TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'RICH'                  TO IDSV0003-NOME-TABELLA.

           MOVE '00'                    TO IDSV0003-RETURN-CODE.
           MOVE ZEROES                  TO IDSV0003-SQLCODE
                                           IDSV0003-NUM-RIGHE-LETTE
                                           WK-ID-RICH.
           MOVE SPACES                  TO IDSV0003-DESCRIZ-ERR-DB2
                                           IDSV0003-KEY-TABELLA.

           SET ACCESSO-STD              TO TRUE

----->     PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.
      ******************************************************************
       A060-CNTL-INPUT.

           IF IDSV0003-WHERE-CONDITION-02

              IF RIC-COD-MACROFUNCT NOT = SPACES     AND
                 RIC-COD-MACROFUNCT NOT = LOW-VALUE  AND
                 RIC-COD-MACROFUNCT NOT = HIGH-VALUE
                 SET ACCESSO-MCRFNCT          TO TRUE
              END-IF

              IF RIC-TP-MOVI IS NUMERIC AND
                 RIC-TP-MOVI NOT = ZERO
                 IF ACCESSO-MCRFNCT
                    SET ACCESSO-MCRFNCT-TPMV  TO TRUE
                 ELSE
                    SET ACCESSO-TPMV          TO TRUE
                 END-IF
              END-IF

           END-IF.

       A060-EX.
           EXIT.
      ******************************************************************
       A100-CHECK-RETURN-CODE.

           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-SELECT         OR
                        IDSV0003-FETCH-FIRST    OR
                        IDSV0003-FETCH-NEXT     OR
                        IDSV0003-UPDATE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE
           END-IF.

       A100-EX.
           EXIT.
      ******************************************************************
       A300-ELABORA.

           EVALUATE TRUE

              WHEN IDSV0003-WHERE-CONDITION-01
                   PERFORM SC01-SELECTION-CURSOR-01 THRU SC01-EX

              WHEN IDSV0003-WHERE-CONDITION-02
                   PERFORM SC02-SELECTION-CURSOR-02 THRU SC02-EX

              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE

           END-EVALUATE.

       A300-EX.
           EXIT.
      ******************************************************************
       A400-FINE.

           CONTINUE.

       A400-EX.
           EXIT.
      ******************************************************************
       D101-DCL-CUR-IDR-STD-SC01.

           EXEC SQL
              DECLARE CUR-RIC-IDR-STD CURSOR WITH HOLD FOR
              SELECT
                ID_RICH
                ,COD_COMP_ANIA
                ,TP_RICH
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_RICH
                ,DT_EFF
                ,DT_RGSTRZ_RICH
                ,DT_PERV_RICH
                ,DT_ESEC_RICH
                ,TS_EFF_ESEC_RICH
                ,ID_OGG
                ,TP_OGG
                ,IB_POLI
                ,IB_ADES
                ,IB_GAR
                ,IB_TRCH_DI_GAR
                ,ID_BATCH
                ,ID_JOB
                ,FL_SIMULAZIONE
                ,KEY_ORDINAMENTO
                ,ID_RICH_COLLG
                ,TP_RAMO_BILA
                ,TP_FRM_ASSVA
                ,TP_CALC_RIS
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RAMO_BILA
              FROM RICH
              WHERE    TP_RICH       =  :PRENOTAZIONE
                   AND COD_COMP_ANIA =  :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND ID_BATCH      =  :RIC-ID-BATCH
                   AND DT_ESEC_RICH <=  CURRENT DATE
                   AND DS_STATO_ELAB IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                         )
              ORDER BY ID_RICH ASC

           END-EXEC.

       D101-SC01-EX.
           EXIT.

      ******************************************************************
       D102-DCL-CUR-IDJ-MCRFNCT-SC01.

           EXEC SQL
              DECLARE CUR-RIC-IDR-MF CURSOR WITH HOLD FOR
              SELECT
                ID_RICH
                ,COD_COMP_ANIA
                ,TP_RICH
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_RICH
                ,DT_EFF
                ,DT_RGSTRZ_RICH
                ,DT_PERV_RICH
                ,DT_ESEC_RICH
                ,TS_EFF_ESEC_RICH
                ,ID_OGG
                ,TP_OGG
                ,IB_POLI
                ,IB_ADES
                ,IB_GAR
                ,IB_TRCH_DI_GAR
                ,ID_BATCH
                ,ID_JOB
                ,FL_SIMULAZIONE
                ,KEY_ORDINAMENTO
                ,ID_RICH_COLLG
                ,TP_RAMO_BILA
                ,TP_FRM_ASSVA
                ,TP_CALC_RIS
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RAMO_BILA
              FROM RICH
              WHERE    TP_RICH        =  :PRENOTAZIONE
                   AND COD_COMP_ANIA  =  :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND ID_BATCH       =  :RIC-ID-BATCH
                   AND COD_MACROFUNCT =  :RIC-COD-MACROFUNCT
                   AND DT_ESEC_RICH <=  CURRENT DATE
                   AND DS_STATO_ELAB IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                         )
              ORDER BY ID_RICH ASC

           END-EXEC.

       D102-SC01-EX.
           EXIT.
      ******************************************************************
       D103-DCL-CUR-IDR-TPMV-SC01.

           EXEC SQL
              DECLARE CUR-RIC-IDR-TM CURSOR WITH HOLD FOR
              SELECT
                ID_RICH
                ,COD_COMP_ANIA
                ,TP_RICH
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_RICH
                ,DT_EFF
                ,DT_RGSTRZ_RICH
                ,DT_PERV_RICH
                ,DT_ESEC_RICH
                ,TS_EFF_ESEC_RICH
                ,ID_OGG
                ,TP_OGG
                ,IB_POLI
                ,IB_ADES
                ,IB_GAR
                ,IB_TRCH_DI_GAR
                ,ID_BATCH
                ,ID_JOB
                ,FL_SIMULAZIONE
                ,KEY_ORDINAMENTO
                ,ID_RICH_COLLG
                ,TP_RAMO_BILA
                ,TP_FRM_ASSVA
                ,TP_CALC_RIS
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RAMO_BILA
              FROM RICH
              WHERE    TP_RICH       =  :PRENOTAZIONE
                   AND COD_COMP_ANIA =  :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND ID_BATCH      =  :RIC-ID-BATCH
                   AND TP_MOVI       =  :RIC-TP-MOVI
                   AND DT_ESEC_RICH <=  CURRENT DATE
                   AND DS_STATO_ELAB IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                         )
              ORDER BY ID_RICH ASC

           END-EXEC.

       D103-SC01-EX.
           EXIT.

      ******************************************************************
       D104-DCL-CUR-IDJ-MCRFNCT-TPMV.

           EXEC SQL
              DECLARE CUR-RIC-IDR-MF-TM CURSOR WITH HOLD FOR
              SELECT
                ID_RICH
                ,COD_COMP_ANIA
                ,TP_RICH
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_RICH
                ,DT_EFF
                ,DT_RGSTRZ_RICH
                ,DT_PERV_RICH
                ,DT_ESEC_RICH
                ,TS_EFF_ESEC_RICH
                ,ID_OGG
                ,TP_OGG
                ,IB_POLI
                ,IB_ADES
                ,IB_GAR
                ,IB_TRCH_DI_GAR
                ,ID_BATCH
                ,ID_JOB
                ,FL_SIMULAZIONE
                ,KEY_ORDINAMENTO
                ,ID_RICH_COLLG
                ,TP_RAMO_BILA
                ,TP_FRM_ASSVA
                ,TP_CALC_RIS
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RAMO_BILA
              FROM RICH
              WHERE    TP_RICH        =  :PRENOTAZIONE
                   AND COD_COMP_ANIA  =  :IDSV0003-CODICE-COMPAGNIA-ANIA
                   AND ID_BATCH       =  :RIC-ID-BATCH
                   AND TP_MOVI        =  :RIC-TP-MOVI
                   AND COD_MACROFUNCT =  :RIC-COD-MACROFUNCT
                   AND DT_ESEC_RICH  <=  CURRENT DATE
                   AND DS_STATO_ELAB IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                         )
              ORDER BY ID_RICH ASC

           END-EXEC.

       D104-SC01-EX.
           EXIT.

      ******************************************************************
       A305-DECLARE-CURSOR-IDR-SC01.

           EVALUATE TRUE

              WHEN ACCESSO-STD
                 PERFORM D101-DCL-CUR-IDR-STD-SC01    THRU D101-SC01-EX

                 EXEC SQL
                      OPEN CUR-RIC-IDR-STD
                 END-EXEC

              WHEN ACCESSO-MCRFNCT
                 PERFORM D102-DCL-CUR-IDJ-MCRFNCT-SC01 THRU D102-SC01-EX

                 EXEC SQL
                      OPEN CUR-RIC-IDR-MF
                 END-EXEC

              WHEN ACCESSO-TPMV
                 PERFORM D103-DCL-CUR-IDR-TPMV-SC01  THRU D103-SC01-EX

                 EXEC SQL
                      OPEN CUR-RIC-IDR-TM
                 END-EXEC

              WHEN ACCESSO-MCRFNCT-TPMV
                 PERFORM D104-DCL-CUR-IDJ-MCRFNCT-TPMV THRU D104-SC01-EX

                 EXEC SQL
                      OPEN CUR-RIC-IDR-MF-TM
                 END-EXEC

           END-EVALUATE.

       A305-SC01-EX.
           EXIT.

      ******************************************************************
       A310-SELECT-SC01.

           EVALUATE TRUE
              WHEN ACCESSO-STD
                 PERFORM A311-SELECT-STD-SC01     THRU A311-SC01-EX

              WHEN ACCESSO-MCRFNCT
                 PERFORM A312-SELECT-MCRFNCT-SC01 THRU A312-SC01-EX

              WHEN ACCESSO-TPMV
                 PERFORM A313-SELECT-TPMV-SC01    THRU A313-SC01-EX

              WHEN ACCESSO-MCRFNCT-TPMV
                 PERFORM A314-SELECT-MCRFNCT-TPMV-SC01
                                                  THRU A314-SC01-EX

           END-EVALUATE.

       A310-SC01-EX.
           EXIT.

      ******************************************************************
       A311-SELECT-STD-SC01.

           EXEC SQL
             SELECT
                ID_RICH
                ,COD_COMP_ANIA
                ,TP_RICH
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_RICH
                ,DT_EFF
                ,DT_RGSTRZ_RICH
                ,DT_PERV_RICH
                ,DT_ESEC_RICH
                ,TS_EFF_ESEC_RICH
                ,ID_OGG
                ,TP_OGG
                ,IB_POLI
                ,IB_ADES
                ,IB_GAR
                ,IB_TRCH_DI_GAR
                ,ID_BATCH
                ,ID_JOB
                ,FL_SIMULAZIONE
                ,KEY_ORDINAMENTO
                ,ID_RICH_COLLG
                ,TP_RAMO_BILA
                ,TP_FRM_ASSVA
                ,TP_CALC_RIS
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RAMO_BILA
             INTO
                :RIC-ID-RICH
               ,:RIC-COD-COMP-ANIA
               ,:RIC-TP-RICH
               ,:RIC-COD-MACROFUNCT
               ,:RIC-TP-MOVI
               ,:RIC-IB-RICH
                :IND-RIC-IB-RICH
               ,:RIC-DT-EFF-DB
               ,:RIC-DT-RGSTRZ-RICH-DB
               ,:RIC-DT-PERV-RICH-DB
               ,:RIC-DT-ESEC-RICH-DB
               ,:RIC-TS-EFF-ESEC-RICH
                :IND-RIC-TS-EFF-ESEC-RICH
               ,:RIC-ID-OGG
                :IND-RIC-ID-OGG
               ,:RIC-TP-OGG
                :IND-RIC-TP-OGG
               ,:RIC-IB-POLI
                :IND-RIC-IB-POLI
               ,:RIC-IB-ADES
                :IND-RIC-IB-ADES
               ,:RIC-IB-GAR
                :IND-RIC-IB-GAR
               ,:RIC-IB-TRCH-DI-GAR
                :IND-RIC-IB-TRCH-DI-GAR
               ,:RIC-ID-BATCH
                :IND-RIC-ID-BATCH
               ,:RIC-ID-JOB
                :IND-RIC-ID-JOB
               ,:RIC-FL-SIMULAZIONE
                :IND-RIC-FL-SIMULAZIONE
               ,:RIC-KEY-ORDINAMENTO-VCHAR
                :IND-RIC-KEY-ORDINAMENTO
               ,:RIC-ID-RICH-COLLG
                :IND-RIC-ID-RICH-COLLG
               ,:RIC-TP-RAMO-BILA
                :IND-RIC-TP-RAMO-BILA
               ,:RIC-TP-FRM-ASSVA
                :IND-RIC-TP-FRM-ASSVA
               ,:RIC-TP-CALC-RIS
                :IND-RIC-TP-CALC-RIS
               ,:RIC-DS-OPER-SQL
               ,:RIC-DS-VER
               ,:RIC-DS-TS-CPTZ
               ,:RIC-DS-UTENTE
               ,:RIC-DS-STATO-ELAB
               ,:RIC-RAMO-BILA
                :IND-RIC-RAMO-BILA
             FROM RICH
             WHERE  TP_RICH               = :PRENOTAZIONE
               AND  ID_BATCH              = :RIC-ID-BATCH
               AND  COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
               AND DT_ESEC_RICH          <=  CURRENT DATE
               AND  DS_STATO_ELAB IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                     )
              FETCH FIRST ROW ONLY


           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       A311-SC01-EX.
           EXIT.

      ******************************************************************
       A312-SELECT-MCRFNCT-SC01.

           EXEC SQL
             SELECT
                ID_RICH
                ,COD_COMP_ANIA
                ,TP_RICH
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_RICH
                ,DT_EFF
                ,DT_RGSTRZ_RICH
                ,DT_PERV_RICH
                ,DT_ESEC_RICH
                ,TS_EFF_ESEC_RICH
                ,ID_OGG
                ,TP_OGG
                ,IB_POLI
                ,IB_ADES
                ,IB_GAR
                ,IB_TRCH_DI_GAR
                ,ID_BATCH
                ,ID_JOB
                ,FL_SIMULAZIONE
                ,KEY_ORDINAMENTO
                ,ID_RICH_COLLG
                ,TP_RAMO_BILA
                ,TP_FRM_ASSVA
                ,TP_CALC_RIS
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RAMO_BILA
             INTO
                :RIC-ID-RICH
               ,:RIC-COD-COMP-ANIA
               ,:RIC-TP-RICH
               ,:RIC-COD-MACROFUNCT
               ,:RIC-TP-MOVI
               ,:RIC-IB-RICH
                :IND-RIC-IB-RICH
               ,:RIC-DT-EFF-DB
               ,:RIC-DT-RGSTRZ-RICH-DB
               ,:RIC-DT-PERV-RICH-DB
               ,:RIC-DT-ESEC-RICH-DB
               ,:RIC-TS-EFF-ESEC-RICH
                :IND-RIC-TS-EFF-ESEC-RICH
               ,:RIC-ID-OGG
                :IND-RIC-ID-OGG
               ,:RIC-TP-OGG
                :IND-RIC-TP-OGG
               ,:RIC-IB-POLI
                :IND-RIC-IB-POLI
               ,:RIC-IB-ADES
                :IND-RIC-IB-ADES
               ,:RIC-IB-GAR
                :IND-RIC-IB-GAR
               ,:RIC-IB-TRCH-DI-GAR
                :IND-RIC-IB-TRCH-DI-GAR
               ,:RIC-ID-BATCH
                :IND-RIC-ID-BATCH
               ,:RIC-ID-JOB
                :IND-RIC-ID-JOB
               ,:RIC-FL-SIMULAZIONE
                :IND-RIC-FL-SIMULAZIONE
               ,:RIC-KEY-ORDINAMENTO-VCHAR
                :IND-RIC-KEY-ORDINAMENTO
               ,:RIC-ID-RICH-COLLG
                :IND-RIC-ID-RICH-COLLG
               ,:RIC-TP-RAMO-BILA
                :IND-RIC-TP-RAMO-BILA
               ,:RIC-TP-FRM-ASSVA
                :IND-RIC-TP-FRM-ASSVA
               ,:RIC-TP-CALC-RIS
                :IND-RIC-TP-CALC-RIS
               ,:RIC-DS-OPER-SQL
               ,:RIC-DS-VER
               ,:RIC-DS-TS-CPTZ
               ,:RIC-DS-UTENTE
               ,:RIC-DS-STATO-ELAB
               ,:RIC-RAMO-BILA
                :IND-RIC-RAMO-BILA
             FROM RICH
             WHERE  TP_RICH               = :PRENOTAZIONE
               AND  ID_BATCH              = :RIC-ID-BATCH
               AND  COD_MACROFUNCT        = :RIC-COD-MACROFUNCT
               AND  COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
               AND DT_ESEC_RICH          <=  CURRENT DATE
               AND  DS_STATO_ELAB IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                      )
              FETCH FIRST ROW ONLY


           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       A312-SC01-EX.
           EXIT.

      ******************************************************************
       A313-SELECT-TPMV-SC01.

           EXEC SQL
             SELECT
                ID_RICH
                ,COD_COMP_ANIA
                ,TP_RICH
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_RICH
                ,DT_EFF
                ,DT_RGSTRZ_RICH
                ,DT_PERV_RICH
                ,DT_ESEC_RICH
                ,TS_EFF_ESEC_RICH
                ,ID_OGG
                ,TP_OGG
                ,IB_POLI
                ,IB_ADES
                ,IB_GAR
                ,IB_TRCH_DI_GAR
                ,ID_BATCH
                ,ID_JOB
                ,FL_SIMULAZIONE
                ,KEY_ORDINAMENTO
                ,ID_RICH_COLLG
                ,TP_RAMO_BILA
                ,TP_FRM_ASSVA
                ,TP_CALC_RIS
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RAMO_BILA
             INTO
                :RIC-ID-RICH
               ,:RIC-COD-COMP-ANIA
               ,:RIC-TP-RICH
               ,:RIC-COD-MACROFUNCT
               ,:RIC-TP-MOVI
               ,:RIC-IB-RICH
                :IND-RIC-IB-RICH
               ,:RIC-DT-EFF-DB
               ,:RIC-DT-RGSTRZ-RICH-DB
               ,:RIC-DT-PERV-RICH-DB
               ,:RIC-DT-ESEC-RICH-DB
               ,:RIC-TS-EFF-ESEC-RICH
                :IND-RIC-TS-EFF-ESEC-RICH
               ,:RIC-ID-OGG
                :IND-RIC-ID-OGG
               ,:RIC-TP-OGG
                :IND-RIC-TP-OGG
               ,:RIC-IB-POLI
                :IND-RIC-IB-POLI
               ,:RIC-IB-ADES
                :IND-RIC-IB-ADES
               ,:RIC-IB-GAR
                :IND-RIC-IB-GAR
               ,:RIC-IB-TRCH-DI-GAR
                :IND-RIC-IB-TRCH-DI-GAR
               ,:RIC-ID-BATCH
                :IND-RIC-ID-BATCH
               ,:RIC-ID-JOB
                :IND-RIC-ID-JOB
               ,:RIC-FL-SIMULAZIONE
                :IND-RIC-FL-SIMULAZIONE
               ,:RIC-KEY-ORDINAMENTO-VCHAR
                :IND-RIC-KEY-ORDINAMENTO
               ,:RIC-ID-RICH-COLLG
                :IND-RIC-ID-RICH-COLLG
               ,:RIC-TP-RAMO-BILA
                :IND-RIC-TP-RAMO-BILA
               ,:RIC-TP-FRM-ASSVA
                :IND-RIC-TP-FRM-ASSVA
               ,:RIC-TP-CALC-RIS
                :IND-RIC-TP-CALC-RIS
               ,:RIC-DS-OPER-SQL
               ,:RIC-DS-VER
               ,:RIC-DS-TS-CPTZ
               ,:RIC-DS-UTENTE
               ,:RIC-DS-STATO-ELAB
               ,:RIC-RAMO-BILA
                :IND-RIC-RAMO-BILA
             FROM RICH
             WHERE  TP_RICH               = :PRENOTAZIONE
               AND  ID_BATCH              = :RIC-ID-BATCH
               AND  TP_MOVI               = :RIC-TP-MOVI
               AND  COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
               AND DT_ESEC_RICH          <=  CURRENT DATE
               AND  DS_STATO_ELAB IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                      )
              FETCH FIRST ROW ONLY


           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       A313-SC01-EX.
           EXIT.

      ******************************************************************
       A314-SELECT-MCRFNCT-TPMV-SC01.

           EXEC SQL
             SELECT
                ID_RICH
                ,COD_COMP_ANIA
                ,TP_RICH
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_RICH
                ,DT_EFF
                ,DT_RGSTRZ_RICH
                ,DT_PERV_RICH
                ,DT_ESEC_RICH
                ,TS_EFF_ESEC_RICH
                ,ID_OGG
                ,TP_OGG
                ,IB_POLI
                ,IB_ADES
                ,IB_GAR
                ,IB_TRCH_DI_GAR
                ,ID_BATCH
                ,ID_JOB
                ,FL_SIMULAZIONE
                ,KEY_ORDINAMENTO
                ,ID_RICH_COLLG
                ,TP_RAMO_BILA
                ,TP_FRM_ASSVA
                ,TP_CALC_RIS
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RAMO_BILA
             INTO
                :RIC-ID-RICH
               ,:RIC-COD-COMP-ANIA
               ,:RIC-TP-RICH
               ,:RIC-COD-MACROFUNCT
               ,:RIC-TP-MOVI
               ,:RIC-IB-RICH
                :IND-RIC-IB-RICH
               ,:RIC-DT-EFF-DB
               ,:RIC-DT-RGSTRZ-RICH-DB
               ,:RIC-DT-PERV-RICH-DB
               ,:RIC-DT-ESEC-RICH-DB
               ,:RIC-TS-EFF-ESEC-RICH
                :IND-RIC-TS-EFF-ESEC-RICH
               ,:RIC-ID-OGG
                :IND-RIC-ID-OGG
               ,:RIC-TP-OGG
                :IND-RIC-TP-OGG
               ,:RIC-IB-POLI
                :IND-RIC-IB-POLI
               ,:RIC-IB-ADES
                :IND-RIC-IB-ADES
               ,:RIC-IB-GAR
                :IND-RIC-IB-GAR
               ,:RIC-IB-TRCH-DI-GAR
                :IND-RIC-IB-TRCH-DI-GAR
               ,:RIC-ID-BATCH
                :IND-RIC-ID-BATCH
               ,:RIC-ID-JOB
                :IND-RIC-ID-JOB
               ,:RIC-FL-SIMULAZIONE
                :IND-RIC-FL-SIMULAZIONE
               ,:RIC-KEY-ORDINAMENTO-VCHAR
                :IND-RIC-KEY-ORDINAMENTO
               ,:RIC-ID-RICH-COLLG
                :IND-RIC-ID-RICH-COLLG
               ,:RIC-TP-RAMO-BILA
                :IND-RIC-TP-RAMO-BILA
               ,:RIC-TP-FRM-ASSVA
                :IND-RIC-TP-FRM-ASSVA
               ,:RIC-TP-CALC-RIS
                :IND-RIC-TP-CALC-RIS
               ,:RIC-DS-OPER-SQL
               ,:RIC-DS-VER
               ,:RIC-DS-TS-CPTZ
               ,:RIC-DS-UTENTE
               ,:RIC-DS-STATO-ELAB
               ,:RIC-RAMO-BILA
                :IND-RIC-RAMO-BILA
             FROM RICH
             WHERE  TP_RICH               = :PRENOTAZIONE
               AND  ID_BATCH              = :RIC-ID-BATCH
               AND  TP_MOVI               = :RIC-TP-MOVI
               AND COD_MACROFUNCT         = :RIC-COD-MACROFUNCT
               AND  COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
               AND DT_ESEC_RICH          <=  CURRENT DATE
               AND  DS_STATO_ELAB IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                      )
              FETCH FIRST ROW ONLY


           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.

       A314-SC01-EX.
           EXIT.

      ******************************************************************
       A310-SELECT-SC02.

           EXEC SQL
             SELECT
                ID_RICH
                ,COD_COMP_ANIA
                ,TP_RICH
                ,COD_MACROFUNCT
                ,TP_MOVI
                ,IB_RICH
                ,DT_EFF
                ,DT_RGSTRZ_RICH
                ,DT_PERV_RICH
                ,DT_ESEC_RICH
                ,TS_EFF_ESEC_RICH
                ,ID_OGG
                ,TP_OGG
                ,IB_POLI
                ,IB_ADES
                ,IB_GAR
                ,IB_TRCH_DI_GAR
                ,ID_BATCH
                ,ID_JOB
                ,FL_SIMULAZIONE
                ,KEY_ORDINAMENTO
                ,ID_RICH_COLLG
                ,TP_RAMO_BILA
                ,TP_FRM_ASSVA
                ,TP_CALC_RIS
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,RAMO_BILA
             INTO
                :RIC-ID-RICH
               ,:RIC-COD-COMP-ANIA
               ,:RIC-TP-RICH
               ,:RIC-COD-MACROFUNCT
               ,:RIC-TP-MOVI
               ,:RIC-IB-RICH
                :IND-RIC-IB-RICH
               ,:RIC-DT-EFF-DB
               ,:RIC-DT-RGSTRZ-RICH-DB
               ,:RIC-DT-PERV-RICH-DB
               ,:RIC-DT-ESEC-RICH-DB
               ,:RIC-TS-EFF-ESEC-RICH
                :IND-RIC-TS-EFF-ESEC-RICH
               ,:RIC-ID-OGG
                :IND-RIC-ID-OGG
               ,:RIC-TP-OGG
                :IND-RIC-TP-OGG
               ,:RIC-IB-POLI
                :IND-RIC-IB-POLI
               ,:RIC-IB-ADES
                :IND-RIC-IB-ADES
               ,:RIC-IB-GAR
                :IND-RIC-IB-GAR
               ,:RIC-IB-TRCH-DI-GAR
                :IND-RIC-IB-TRCH-DI-GAR
               ,:RIC-ID-BATCH
                :IND-RIC-ID-BATCH
               ,:RIC-ID-JOB
                :IND-RIC-ID-JOB
               ,:RIC-FL-SIMULAZIONE
                :IND-RIC-FL-SIMULAZIONE
               ,:RIC-KEY-ORDINAMENTO-VCHAR
                :IND-RIC-KEY-ORDINAMENTO
               ,:RIC-ID-RICH-COLLG
                :IND-RIC-ID-RICH-COLLG
               ,:RIC-TP-RAMO-BILA
                :IND-RIC-TP-RAMO-BILA
               ,:RIC-TP-FRM-ASSVA
                :IND-RIC-TP-FRM-ASSVA
               ,:RIC-TP-CALC-RIS
                :IND-RIC-TP-CALC-RIS
               ,:RIC-DS-OPER-SQL
               ,:RIC-DS-VER
               ,:RIC-DS-TS-CPTZ
               ,:RIC-DS-UTENTE
               ,:RIC-DS-STATO-ELAB
               ,:RIC-RAMO-BILA
                :IND-RIC-RAMO-BILA
             FROM RICH
             WHERE  ID_RICH        =  :RIC-ID-RICH AND
                    DT_ESEC_RICH  <=  CURRENT DATE


           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

              PERFORM B001-SELECT-DER       THRU B001-EX

           END-IF.

       A310-SC02-EX.
           EXIT.
      ******************************************************************
       A320-UPDATE-SC01.
      *
           EVALUATE TRUE

              WHEN IDSV0003-PRIMARY-KEY
                   PERFORM A330-UPDATE-PK-SC01      THRU A330-SC01-EX

              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A340-UPDATE-FIRST-ACTION-SC01
                                                    THRU A340-SC01-EX

              WHEN IDSV0003-WHERE-CONDITION
                   PERFORM A350-UPDATE-WHERE-COND-SC01
                                                    THRU A350-SC01-EX

              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE

           END-EVALUATE.
      *
       A320-SC01-EX.
           EXIT.
      ******************************************************************
       A330-UPDATE-PK-SC01.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE RICH SET
                   TS_EFF_ESEC_RICH       =
                :RIC-TS-EFF-ESEC-RICH
                :IND-RIC-TS-EFF-ESEC-RICH
                  ,DS_OPER_SQL            =
                :RIC-DS-OPER-SQL
                  ,DS_VER                 =
                :RIC-DS-VER
                  ,DS_TS_CPTZ             =
                :RIC-DS-TS-CPTZ
                  ,DS_UTENTE              =
                :RIC-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :IABV0002-STATE-CURRENT

                WHERE   ID_RICH       =  :RIC-ID-RICH
                  AND   COD_COMP_ANIA =  :IDSV0003-CODICE-COMPAGNIA-ANIA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
           END-IF.

       A330-SC01-EX.
           EXIT.

      ******************************************************************
       A330-UPDATE-SC02.

           EVALUATE TRUE

              WHEN IDSV0003-FIRST-ACTION
                   PERFORM A331-UPDATE-FIRST-ACTION-SC02
                                                    THRU A331-SC02-EX
              WHEN IDSV0003-NONE-ACTION
                   PERFORM A332-UPDATE-NONE-ACTION-SC02
                                                    THRU A332-SC02-EX
              WHEN IDSV0003-ID
                   PERFORM A333-UPDATE-ID-SC02
                                                    THRU A333-SC02-EX
              WHEN OTHER
                   SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE

           END-EVALUATE.

       A330-SC02-EX.
           EXIT.
      ******************************************************************
       A331-UPDATE-FIRST-ACTION-SC02.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE RICH
                SET TS_EFF_ESEC_RICH = :RIC-TS-EFF-ESEC-RICH
                                       :IND-RIC-TS-EFF-ESEC-RICH
                WHERE   ID_RICH      = :RIC-ID-RICH
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE       THRU A100-EX.

       A331-SC02-EX.
           EXIT.
      ******************************************************************
       A332-UPDATE-NONE-ACTION-SC02.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE RICH
                SET DS_STATO_ELAB = :IABV0002-STATE-CURRENT
                WHERE   ID_RICH   = :RIC-ID-RICH
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE       THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT       TO RIC-DS-STATO-ELAB
           END-IF.

       A332-SC02-EX.
           EXIT.
      ******************************************************************
       A333-UPDATE-ID-SC02.

           MOVE WS-TS-COMPETENZA-AGG-STOR  TO RIC-DS-TS-CPTZ.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE RICH
                SET TP_RICH   = :RIC-TP-RICH
                   ,TP_MOVI   = :RIC-TP-MOVI
                   ,IB_RICH   = :RIC-IB-RICH
                                :IND-RIC-IB-RICH
                   ,DT_RGSTRZ_RICH
                              = :RIC-DT-RGSTRZ-RICH-DB
                   ,DT_PERV_RICH
                              = :RIC-DT-PERV-RICH-DB
                   ,DT_EFF    = :RIC-DT-EFF-DB
                   ,DT_ESEC_RICH
                                  = :RIC-DT-ESEC-RICH-DB
                   ,DS_TS_CPTZ    = :RIC-DS-TS-CPTZ
                WHERE   ID_RICH   = :RIC-ID-RICH
                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE       THRU A100-EX.

       A333-SC02-EX.
           EXIT.

      ******************************************************************
       A350-INSERT-SC02.


           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO RICH
                     (
                        ID_RICH
                       ,COD_COMP_ANIA
                       ,TP_RICH
                       ,COD_MACROFUNCT
                       ,TP_MOVI
                       ,IB_RICH
                       ,DT_EFF
                       ,DT_RGSTRZ_RICH
                       ,DT_PERV_RICH
                       ,DT_ESEC_RICH
                       ,TS_EFF_ESEC_RICH
                       ,ID_OGG
                       ,TP_OGG
                       ,IB_POLI
                       ,IB_ADES
                       ,IB_GAR
                       ,IB_TRCH_DI_GAR
                       ,ID_BATCH
                       ,ID_JOB
                       ,FL_SIMULAZIONE
                       ,KEY_ORDINAMENTO
                       ,ID_RICH_COLLG
                       ,TP_RAMO_BILA
                       ,TP_FRM_ASSVA
                       ,TP_CALC_RIS
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,RAMO_BILA
                     )
                 VALUES
                     (
                       :RIC-ID-RICH
                       ,:RIC-COD-COMP-ANIA
                       ,:RIC-TP-RICH
                       ,:RIC-COD-MACROFUNCT
                       ,:RIC-TP-MOVI
                       ,:RIC-IB-RICH
                        :IND-RIC-IB-RICH
                       ,:RIC-DT-EFF-DB
                       ,:RIC-DT-RGSTRZ-RICH-DB
                       ,:RIC-DT-PERV-RICH-DB
                       ,:RIC-DT-ESEC-RICH-DB
                       ,:RIC-TS-EFF-ESEC-RICH
                        :IND-RIC-TS-EFF-ESEC-RICH
                       ,:RIC-ID-OGG
                        :IND-RIC-ID-OGG
                       ,:RIC-TP-OGG
                        :IND-RIC-TP-OGG
                       ,:RIC-IB-POLI
                        :IND-RIC-IB-POLI
                       ,:RIC-IB-ADES
                        :IND-RIC-IB-ADES
                       ,:RIC-IB-GAR
                        :IND-RIC-IB-GAR
                       ,:RIC-IB-TRCH-DI-GAR
                        :IND-RIC-IB-TRCH-DI-GAR
                       ,:RIC-ID-BATCH
                        :IND-RIC-ID-BATCH
                       ,:RIC-ID-JOB
                        :IND-RIC-ID-JOB
                       ,:RIC-FL-SIMULAZIONE
                        :IND-RIC-FL-SIMULAZIONE
                       ,:RIC-KEY-ORDINAMENTO-VCHAR
                        :IND-RIC-KEY-ORDINAMENTO
                       ,:RIC-ID-RICH-COLLG
                        :IND-RIC-ID-RICH-COLLG
                       ,:RIC-TP-RAMO-BILA
                        :IND-RIC-TP-RAMO-BILA
                       ,:RIC-TP-FRM-ASSVA
                        :IND-RIC-TP-FRM-ASSVA
                       ,:RIC-TP-CALC-RIS
                        :IND-RIC-TP-CALC-RIS
                       ,:RIC-DS-OPER-SQL
                       ,:RIC-DS-VER
                       ,:RIC-DS-TS-CPTZ
                       ,:RIC-DS-UTENTE
                       ,:RIC-DS-STATO-ELAB
                       ,:RIC-RAMO-BILA
                        :IND-RIC-RAMO-BILA
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.


       A350-SC02-EX.
           EXIT.

      ******************************************************************
       A340-UPDATE-FIRST-ACTION-SC01.

           EVALUATE TRUE
              WHEN ACCESSO-STD
                 PERFORM A341-UPD-F-ACT-STD-SC01   THRU A341-SC01-EX

              WHEN ACCESSO-MCRFNCT
                 PERFORM A342-UPD-F-ACT-MFNCT-SC01 THRU A342-SC01-EX

              WHEN ACCESSO-TPMV
                 PERFORM A343-UPD-F-ACT-TPMV-SC01  THRU A343-SC01-EX

              WHEN ACCESSO-MCRFNCT-TPMV
                 PERFORM A344-UPD-F-ACT-MFNCT-TPMV-SC01
                                                   THRU A344-SC01-EX

           END-EVALUATE.

       A340-SC01-EX.
           EXIT.
      ******************************************************************
       A341-UPD-F-ACT-STD-SC01.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE RICH SET
                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT
              WHERE TP_RICH        = :PRENOTAZIONE
                AND ID_BATCH       = :RIC-ID-BATCH
                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
                AND DS_STATO_ELAB IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                     )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
           END-IF.

       A341-SC01-EX.
           EXIT.

      ******************************************************************
       A342-UPD-F-ACT-MFNCT-SC01.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE RICH SET
                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT
              WHERE TP_RICH        = :PRENOTAZIONE
                AND ID_BATCH       = :RIC-ID-BATCH
                AND COD_MACROFUNCT = :RIC-COD-MACROFUNCT
                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
                AND DS_STATO_ELAB IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                     )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
           END-IF.

       A342-SC01-EX.
           EXIT.

      ******************************************************************
       A343-UPD-F-ACT-TPMV-SC01.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE RICH SET
                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT
              WHERE TP_RICH        = :PRENOTAZIONE
                AND ID_BATCH       = :RIC-ID-BATCH
                AND TP_MOVI        = :RIC-TP-MOVI
                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
                AND DS_STATO_ELAB IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                     )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
           END-IF.

       A343-SC01-EX.
           EXIT.

      ******************************************************************
       A344-UPD-F-ACT-MFNCT-TPMV-SC01.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE RICH SET
                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT
              WHERE TP_RICH        = :PRENOTAZIONE
                AND ID_BATCH       = :RIC-ID-BATCH
                AND TP_MOVI        = :RIC-TP-MOVI
                AND COD_MACROFUNCT = :RIC-COD-MACROFUNCT
                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
                AND DS_STATO_ELAB IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                     )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
           END-IF.

       A344-SC01-EX.
           EXIT.

      ******************************************************************
       A350-UPDATE-WHERE-COND-SC01.

           EVALUATE TRUE
              WHEN ACCESSO-STD
                 PERFORM A351-UPD-WHC-STD-SC01   THRU A351-SC01-EX

              WHEN ACCESSO-MCRFNCT
                 PERFORM A352-UPD-WHC-MFNCT-SC01 THRU A352-SC01-EX

              WHEN ACCESSO-TPMV
                 PERFORM A353-UPD-WHC-TPMV-SC01  THRU A353-SC01-EX

              WHEN ACCESSO-MCRFNCT-TPMV
                 PERFORM A354-UPD-WHC-MFNCT-TPMV-SC01
                                                 THRU A354-SC01-EX

           END-EVALUATE.

       A350-SC01-EX.
           EXIT.

      ******************************************************************
       A351-UPD-WHC-STD-SC01.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE RICH SET
                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT

              WHERE TP_RICH        = :PRENOTAZIONE
                AND ID_BATCH       = :RIC-ID-BATCH
                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
                AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                     )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
           END-IF.

       A351-SC01-EX.
           EXIT.

      ******************************************************************
       A352-UPD-WHC-MFNCT-SC01.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE RICH SET
                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT

              WHERE TP_RICH        = :PRENOTAZIONE
                AND ID_BATCH       = :RIC-ID-BATCH
                AND COD_MACROFUNCT = :RIC-COD-MACROFUNCT
                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
                AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                      )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
           END-IF.

       A352-SC01-EX.
           EXIT.

      ******************************************************************
       A353-UPD-WHC-TPMV-SC01.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE RICH SET
                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT

              WHERE TP_RICH        = :PRENOTAZIONE
                AND ID_BATCH       = :RIC-ID-BATCH
                AND TP_MOVI        = :RIC-TP-MOVI
                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
                AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                      )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
           END-IF.

       A353-SC01-EX.
           EXIT.

      ******************************************************************
       A354-UPD-WHC-MFNCT-TPMV-SC01.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX

           EXEC SQL
                UPDATE RICH SET
                DS_STATO_ELAB      = :IABV0002-STATE-CURRENT

              WHERE TP_RICH        = :PRENOTAZIONE
                AND ID_BATCH       = :RIC-ID-BATCH
                AND TP_MOVI        = :RIC-TP-MOVI
                AND COD_MACROFUNCT = :RIC-COD-MACROFUNCT
                AND COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
                AND DS_STATO_ELAB IN (
                                        :IABV0002-STATE-01,
                                        :IABV0002-STATE-02,
                                        :IABV0002-STATE-03,
                                        :IABV0002-STATE-04,
                                        :IABV0002-STATE-05,
                                        :IABV0002-STATE-06,
                                        :IABV0002-STATE-07,
                                        :IABV0002-STATE-08,
                                        :IABV0002-STATE-09,
                                        :IABV0002-STATE-10
                                      )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              MOVE IABV0002-STATE-CURRENT TO RIC-DS-STATO-ELAB
           END-IF.

       A354-SC01-EX.
           EXIT.

      ******************************************************************
       A360-OPEN-CURSOR-SC01.

           EVALUATE TRUE
              WHEN IABV0002-ORDER-BY-ID-JOB
                   PERFORM A305-DECLARE-CURSOR-IDR-SC01
                                                   THRU A305-SC01-EX

              WHEN OTHER
                   SET IDSV0003-INVALID-OPER       TO TRUE

           END-EVALUATE.

           IF IDSV0003-SUCCESSFUL-RC
              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
           END-IF.

       A360-SC01-EX.
           EXIT.
      ******************************************************************
       A370-CLOSE-CURSOR-SC01.

           EVALUATE TRUE

              WHEN IABV0002-ORDER-BY-ID-JOB
                   PERFORM C100-CLOSE-IDR-SC01 THRU C100-SC01-EX

              WHEN OTHER
                   SET IDSV0003-INVALID-OPER       TO TRUE

           END-EVALUATE.

           IF IDSV0003-SUCCESSFUL-RC
              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
           END-IF.

       A370-SC01-EX.
           EXIT.
      ******************************************************************
       C100-CLOSE-IDR-SC01.

           EVALUATE TRUE
              WHEN ACCESSO-STD

                 EXEC SQL
                      CLOSE CUR-RIC-IDR-STD
                 END-EXEC

              WHEN ACCESSO-MCRFNCT

                 EXEC SQL
                      CLOSE CUR-RIC-IDR-MF
                 END-EXEC

              WHEN ACCESSO-TPMV

                 EXEC SQL
                      CLOSE CUR-RIC-IDR-TM
                 END-EXEC

              WHEN ACCESSO-MCRFNCT-TPMV

                 EXEC SQL
                      CLOSE CUR-RIC-IDR-MF-TM
                 END-EXEC

           END-EVALUATE.

       C100-SC01-EX.
           EXIT.
      ******************************************************************
       A380-FETCH-FIRST-SC01.

           PERFORM A360-OPEN-CURSOR-SC01   THRU A360-SC01-EX.

           IF IDSV0003-SUCCESSFUL-RC
              PERFORM A390-FETCH-NEXT-SC01 THRU A390-SC01-EX
           END-IF.

       A380-SC01-EX.
           EXIT.
      ******************************************************************
       A390-FETCH-NEXT-SC01.

           EVALUATE TRUE

              WHEN IABV0002-ORDER-BY-ID-JOB
                   PERFORM A391-FETCH-NEXT-IDR-SC01 THRU A391-SC01-EX

              WHEN OTHER
                   SET IDSV0003-INVALID-OPER       TO TRUE

           END-EVALUATE.

       A390-SC01-EX.
           EXIT.
      ******************************************************************
       A391-FETCH-NEXT-IDR-SC01.

           EVALUATE TRUE
              WHEN ACCESSO-STD
                 PERFORM F101-FET-NX-IDR-STD-SC01     THRU F101-SC01-EX

              WHEN ACCESSO-MCRFNCT
                 PERFORM F102-FET-NX-IDR-MFNCT-SC01   THRU F102-SC01-EX

              WHEN ACCESSO-TPMV
                 PERFORM F103-FET-NX-IDR-TPMV-SC01    THRU F103-SC01-EX

              WHEN ACCESSO-MCRFNCT-TPMV
                 PERFORM F104-FET-NX-IDR-MF-TPMV-SC01 THRU F104-SC01-EX

           END-EVALUATE.

       A391-SC01-EX.
           EXIT.

      ******************************************************************
       F101-FET-NX-IDR-STD-SC01.

           EXEC SQL
                FETCH CUR-RIC-IDR-STD
           INTO
                :RIC-ID-RICH
               ,:RIC-COD-COMP-ANIA
               ,:RIC-TP-RICH
               ,:RIC-COD-MACROFUNCT
               ,:RIC-TP-MOVI
               ,:RIC-IB-RICH
                :IND-RIC-IB-RICH
               ,:RIC-DT-EFF-DB
               ,:RIC-DT-RGSTRZ-RICH-DB
               ,:RIC-DT-PERV-RICH-DB
               ,:RIC-DT-ESEC-RICH-DB
               ,:RIC-TS-EFF-ESEC-RICH
                :IND-RIC-TS-EFF-ESEC-RICH
               ,:RIC-ID-OGG
                :IND-RIC-ID-OGG
               ,:RIC-TP-OGG
                :IND-RIC-TP-OGG
               ,:RIC-IB-POLI
                :IND-RIC-IB-POLI
               ,:RIC-IB-ADES
                :IND-RIC-IB-ADES
               ,:RIC-IB-GAR
                :IND-RIC-IB-GAR
               ,:RIC-IB-TRCH-DI-GAR
                :IND-RIC-IB-TRCH-DI-GAR
               ,:RIC-ID-BATCH
                :IND-RIC-ID-BATCH
               ,:RIC-ID-JOB
                :IND-RIC-ID-JOB
               ,:RIC-FL-SIMULAZIONE
                :IND-RIC-FL-SIMULAZIONE
               ,:RIC-KEY-ORDINAMENTO-VCHAR
                :IND-RIC-KEY-ORDINAMENTO
               ,:RIC-ID-RICH-COLLG
                :IND-RIC-ID-RICH-COLLG
               ,:RIC-TP-RAMO-BILA
                :IND-RIC-TP-RAMO-BILA
               ,:RIC-TP-FRM-ASSVA
                :IND-RIC-TP-FRM-ASSVA
               ,:RIC-TP-CALC-RIS
                :IND-RIC-TP-CALC-RIS
               ,:RIC-DS-OPER-SQL
               ,:RIC-DS-VER
               ,:RIC-DS-TS-CPTZ
               ,:RIC-DS-UTENTE
               ,:RIC-DS-STATO-ELAB
               ,:RIC-RAMO-BILA
                :IND-RIC-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

              PERFORM B000-FETCH-DER        THRU B000-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC01 THRU A370-SC01-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       F101-SC01-EX.
           EXIT.

      ******************************************************************
       F102-FET-NX-IDR-MFNCT-SC01.

           EXEC SQL
                FETCH CUR-RIC-IDR-MF
           INTO
                :RIC-ID-RICH
               ,:RIC-COD-COMP-ANIA
               ,:RIC-TP-RICH
               ,:RIC-COD-MACROFUNCT
               ,:RIC-TP-MOVI
               ,:RIC-IB-RICH
                :IND-RIC-IB-RICH
               ,:RIC-DT-EFF-DB
               ,:RIC-DT-RGSTRZ-RICH-DB
               ,:RIC-DT-PERV-RICH-DB
               ,:RIC-DT-ESEC-RICH-DB
               ,:RIC-TS-EFF-ESEC-RICH
                :IND-RIC-TS-EFF-ESEC-RICH
               ,:RIC-ID-OGG
                :IND-RIC-ID-OGG
               ,:RIC-TP-OGG
                :IND-RIC-TP-OGG
               ,:RIC-IB-POLI
                :IND-RIC-IB-POLI
               ,:RIC-IB-ADES
                :IND-RIC-IB-ADES
               ,:RIC-IB-GAR
                :IND-RIC-IB-GAR
               ,:RIC-IB-TRCH-DI-GAR
                :IND-RIC-IB-TRCH-DI-GAR
               ,:RIC-ID-BATCH
                :IND-RIC-ID-BATCH
               ,:RIC-ID-JOB
                :IND-RIC-ID-JOB
               ,:RIC-FL-SIMULAZIONE
                :IND-RIC-FL-SIMULAZIONE
               ,:RIC-KEY-ORDINAMENTO-VCHAR
                :IND-RIC-KEY-ORDINAMENTO
               ,:RIC-ID-RICH-COLLG
                :IND-RIC-ID-RICH-COLLG
               ,:RIC-TP-RAMO-BILA
                :IND-RIC-TP-RAMO-BILA
               ,:RIC-TP-FRM-ASSVA
                :IND-RIC-TP-FRM-ASSVA
               ,:RIC-TP-CALC-RIS
                :IND-RIC-TP-CALC-RIS
               ,:RIC-DS-OPER-SQL
               ,:RIC-DS-VER
               ,:RIC-DS-TS-CPTZ
               ,:RIC-DS-UTENTE
               ,:RIC-DS-STATO-ELAB
               ,:RIC-RAMO-BILA
                :IND-RIC-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

              PERFORM B000-FETCH-DER        THRU B000-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC01 THRU A370-SC01-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       F102-SC01-EX.
           EXIT.

      ******************************************************************
       F103-FET-NX-IDR-TPMV-SC01.

           EXEC SQL
                FETCH CUR-RIC-IDR-TM
           INTO
                :RIC-ID-RICH
               ,:RIC-COD-COMP-ANIA
               ,:RIC-TP-RICH
               ,:RIC-COD-MACROFUNCT
               ,:RIC-TP-MOVI
               ,:RIC-IB-RICH
                :IND-RIC-IB-RICH
               ,:RIC-DT-EFF-DB
               ,:RIC-DT-RGSTRZ-RICH-DB
               ,:RIC-DT-PERV-RICH-DB
               ,:RIC-DT-ESEC-RICH-DB
               ,:RIC-TS-EFF-ESEC-RICH
                :IND-RIC-TS-EFF-ESEC-RICH
               ,:RIC-ID-OGG
                :IND-RIC-ID-OGG
               ,:RIC-TP-OGG
                :IND-RIC-TP-OGG
               ,:RIC-IB-POLI
                :IND-RIC-IB-POLI
               ,:RIC-IB-ADES
                :IND-RIC-IB-ADES
               ,:RIC-IB-GAR
                :IND-RIC-IB-GAR
               ,:RIC-IB-TRCH-DI-GAR
                :IND-RIC-IB-TRCH-DI-GAR
               ,:RIC-ID-BATCH
                :IND-RIC-ID-BATCH
               ,:RIC-ID-JOB
                :IND-RIC-ID-JOB
               ,:RIC-FL-SIMULAZIONE
                :IND-RIC-FL-SIMULAZIONE
               ,:RIC-KEY-ORDINAMENTO-VCHAR
                :IND-RIC-KEY-ORDINAMENTO
               ,:RIC-ID-RICH-COLLG
                :IND-RIC-ID-RICH-COLLG
               ,:RIC-TP-RAMO-BILA
                :IND-RIC-TP-RAMO-BILA
               ,:RIC-TP-FRM-ASSVA
                :IND-RIC-TP-FRM-ASSVA
               ,:RIC-TP-CALC-RIS
                :IND-RIC-TP-CALC-RIS
               ,:RIC-DS-OPER-SQL
               ,:RIC-DS-VER
               ,:RIC-DS-TS-CPTZ
               ,:RIC-DS-UTENTE
               ,:RIC-DS-STATO-ELAB
               ,:RIC-RAMO-BILA
                :IND-RIC-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

              PERFORM B000-FETCH-DER        THRU B000-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC01 THRU A370-SC01-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       F103-SC01-EX.
           EXIT.

      ******************************************************************
       F104-FET-NX-IDR-MF-TPMV-SC01.

           EXEC SQL
                FETCH CUR-RIC-IDR-MF-TM
           INTO
                :RIC-ID-RICH
               ,:RIC-COD-COMP-ANIA
               ,:RIC-TP-RICH
               ,:RIC-COD-MACROFUNCT
               ,:RIC-TP-MOVI
               ,:RIC-IB-RICH
                :IND-RIC-IB-RICH
               ,:RIC-DT-EFF-DB
               ,:RIC-DT-RGSTRZ-RICH-DB
               ,:RIC-DT-PERV-RICH-DB
               ,:RIC-DT-ESEC-RICH-DB
               ,:RIC-TS-EFF-ESEC-RICH
                :IND-RIC-TS-EFF-ESEC-RICH
               ,:RIC-ID-OGG
                :IND-RIC-ID-OGG
               ,:RIC-TP-OGG
                :IND-RIC-TP-OGG
               ,:RIC-IB-POLI
                :IND-RIC-IB-POLI
               ,:RIC-IB-ADES
                :IND-RIC-IB-ADES
               ,:RIC-IB-GAR
                :IND-RIC-IB-GAR
               ,:RIC-IB-TRCH-DI-GAR
                :IND-RIC-IB-TRCH-DI-GAR
               ,:RIC-ID-BATCH
                :IND-RIC-ID-BATCH
               ,:RIC-ID-JOB
                :IND-RIC-ID-JOB
               ,:RIC-FL-SIMULAZIONE
                :IND-RIC-FL-SIMULAZIONE
               ,:RIC-KEY-ORDINAMENTO-VCHAR
                :IND-RIC-KEY-ORDINAMENTO
               ,:RIC-ID-RICH-COLLG
                :IND-RIC-ID-RICH-COLLG
               ,:RIC-TP-RAMO-BILA
                :IND-RIC-TP-RAMO-BILA
               ,:RIC-TP-FRM-ASSVA
                :IND-RIC-TP-FRM-ASSVA
               ,:RIC-TP-CALC-RIS
                :IND-RIC-TP-CALC-RIS
               ,:RIC-DS-OPER-SQL
               ,:RIC-DS-VER
               ,:RIC-DS-TS-CPTZ
               ,:RIC-DS-UTENTE
               ,:RIC-DS-STATO-ELAB
               ,:RIC-RAMO-BILA
                :IND-RIC-RAMO-BILA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

              PERFORM B000-FETCH-DER        THRU B000-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-SC01 THRU A370-SC01-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       F104-SC01-EX.
           EXIT.

      ******************************************************************

       B000-FETCH-DER.
      *
           MOVE RIC-ID-RICH               TO DER-ID-RICH.
           MOVE ZERO                      TO IABV0003-ELE-MAX.

           PERFORM B030-FETCH-FIRST-DER   THRU B030-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B040-FETCH-NEXT-DER
                 THRU B040-EX
                UNTIL IDSV0003-NOT-FOUND
                   OR NOT IDSV0003-SUCCESSFUL-SQL
           END-IF.

           IF IDSV0003-NOT-FOUND
              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
           END-IF.
      *
       B000-EX.
           EXIT.

      ********************************************************
       B001-SELECT-DER.
      *
           MOVE RIC-ID-RICH               TO DER-ID-RICH.

           EXEC SQL

              SELECT TP_AREA_D_RICH
                      ,AREA_D_RICH
              INTO
                     :DER-TP-AREA-D-RICH
                     :IND-DER-TP-AREA-D-RICH
                    ,:DER-AREA-D-RICH-VCHAR
                     :IND-DER-AREA-D-RICH
              FROM  DETT_RICH
              WHERE ID_RICH       = :DER-ID-RICH
              ORDER BY PROG_DETT_RICH

              FETCH FIRST ROW ONLY


           END-EXEC


           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              IF IND-DER-TP-AREA-D-RICH = -1
                 MOVE HIGH-VALUES TO DER-TP-AREA-D-RICH-NULL
              END-IF

              IF IND-DER-AREA-D-RICH = -1
                 MOVE HIGH-VALUES TO DER-AREA-D-RICH
              END-IF

              MOVE DER-TP-AREA-D-RICH
                                  TO IABV0009-BLOB-DATA-TYPE-REC
              MOVE DER-AREA-D-RICH
                                  TO IABV0009-BLOB-DATA-REC
           END-IF

           IF IDSV0003-NOT-FOUND

              SET IDSV0003-SQL-ERROR TO TRUE
              MOVE DER-ID-RICH       TO WK-ID-RICH

              MOVE SPACES TO IDSV0003-DESCRIZ-ERR-DB2
              STRING 'DETT_RICH NON TROVATA - ID-RICH :'
                     DELIMITED BY SIZE
                     WK-ID-RICH
                     DELIMITED BY SIZE
                     INTO
                     IDSV0003-DESCRIZ-ERR-DB2
              END-STRING

              MOVE 'IABS0900'  TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'DETT_RICH' TO IDSV0003-NOME-TABELLA

           END-IF.
      *
       B001-EX.
           EXIT.

      ********************************************************
       B010-DECLARE-CURSOR-DER.
           EXEC SQL
             DECLARE C-DER CURSOR FOR
              SELECT TP_AREA_D_RICH
                      ,AREA_D_RICH
                FROM  DETT_RICH
               WHERE ID_RICH       = :DER-ID-RICH
                 AND COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
               ORDER BY PROG_DETT_RICH

           END-EXEC.
       B010-EX.
           EXIT.

      ********************************************************
       B020-OPEN-CURSOR-DER.
           PERFORM B010-DECLARE-CURSOR-DER     THRU B010-EX.

           EXEC SQL
                OPEN C-DER
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B020-EX.
           EXIT.

      ********************************************************
       B030-FETCH-FIRST-DER.
           PERFORM B020-OPEN-CURSOR-DER        THRU B020-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B040-FETCH-NEXT-DER      THRU B040-EX
           END-IF.
       B030-EX.
           EXIT.

      ********************************************************
       B040-FETCH-NEXT-DER.
           EXEC SQL
                FETCH C-DER
           INTO
                :DER-TP-AREA-D-RICH
                :IND-DER-TP-AREA-D-RICH
               ,:DER-AREA-D-RICH-VCHAR
                :IND-DER-AREA-D-RICH
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              IF IND-DER-TP-AREA-D-RICH = -1
                 MOVE HIGH-VALUES TO DER-TP-AREA-D-RICH-NULL
              END-IF

              IF IND-DER-AREA-D-RICH = -1
                 MOVE HIGH-VALUES TO DER-AREA-D-RICH
              END-IF

              ADD  1
                TO IABV0003-ELE-MAX
              MOVE DER-TP-AREA-D-RICH
                TO IABV0003-BLOB-DATA-TYPE-REC(IABV0003-ELE-MAX)
              MOVE DER-AREA-D-RICH
                TO IABV0003-BLOB-DATA-REC(IABV0003-ELE-MAX)
           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B050-CLOSE-CURSOR-DER     THRU B050-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.

       B040-EX.
           EXIT.

      ******************************************************************
       B050-CLOSE-CURSOR-DER.

           EXEC SQL
                CLOSE C-DER
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       B050-EX.
           EXIT.

      ******************************************************************
       SC01-SELECTION-CURSOR-01.

           EVALUATE TRUE

              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC01            THRU A310-SC01-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A360-OPEN-CURSOR-SC01       THRU A360-SC01-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A370-CLOSE-CURSOR-SC01      THRU A370-SC01-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A380-FETCH-FIRST-SC01       THRU A380-SC01-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A390-FETCH-NEXT-SC01        THRU A390-SC01-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A320-UPDATE-SC01            THRU A320-SC01-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE

           END-EVALUATE.

       SC01-EX.
           EXIT.
      ******************************************************************
       SC02-SELECTION-CURSOR-02.

           EVALUATE TRUE

              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-SC02            THRU A310-SC02-EX

              WHEN IDSV0003-UPDATE
                 PERFORM A330-UPDATE-SC02            THRU A330-SC02-EX

              WHEN IDSV0003-INSERT
                 PERFORM A350-INSERT-SC02            THRU A350-SC02-EX

              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE

           END-EVALUATE.

       SC02-EX.
           EXIT.

      ******************************************************************
       Z100-SET-COLONNE-NULL.

           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.

           IF IND-RIC-IB-RICH = -1
              MOVE HIGH-VALUES TO RIC-IB-RICH-NULL
           END-IF
           IF IND-RIC-TS-EFF-ESEC-RICH = -1
              MOVE HIGH-VALUES TO RIC-TS-EFF-ESEC-RICH-NULL
           END-IF
           IF IND-RIC-ID-OGG = -1
              MOVE HIGH-VALUES TO RIC-ID-OGG-NULL
           END-IF
           IF IND-RIC-TP-OGG = -1
              MOVE HIGH-VALUES TO RIC-TP-OGG-NULL
           END-IF
           IF IND-RIC-IB-POLI = -1
              MOVE HIGH-VALUES TO RIC-IB-POLI-NULL
           END-IF
           IF IND-RIC-IB-ADES = -1
              MOVE HIGH-VALUES TO RIC-IB-ADES-NULL
           END-IF
           IF IND-RIC-IB-GAR = -1
              MOVE HIGH-VALUES TO RIC-IB-GAR-NULL
           END-IF
           IF IND-RIC-IB-TRCH-DI-GAR = -1
              MOVE HIGH-VALUES TO RIC-IB-TRCH-DI-GAR-NULL
           END-IF
           IF IND-RIC-ID-BATCH = -1
              MOVE HIGH-VALUES TO RIC-ID-BATCH-NULL
           END-IF
           IF IND-RIC-ID-JOB = -1
              MOVE HIGH-VALUES TO RIC-ID-JOB-NULL
           END-IF
           IF IND-RIC-FL-SIMULAZIONE = -1
              MOVE HIGH-VALUES TO RIC-FL-SIMULAZIONE-NULL
           END-IF
           IF IND-RIC-KEY-ORDINAMENTO = -1
              MOVE HIGH-VALUES TO RIC-KEY-ORDINAMENTO
           END-IF
           IF IND-RIC-ID-RICH-COLLG = -1
              MOVE HIGH-VALUES TO RIC-ID-RICH-COLLG-NULL
           END-IF
           IF IND-RIC-TP-RAMO-BILA = -1
              MOVE HIGH-VALUES TO RIC-TP-RAMO-BILA-NULL
           END-IF
           IF IND-RIC-TP-FRM-ASSVA = -1
              MOVE HIGH-VALUES TO RIC-TP-FRM-ASSVA-NULL
           END-IF
           IF IND-RIC-TP-CALC-RIS = -1
              MOVE HIGH-VALUES TO RIC-TP-CALC-RIS-NULL
           END-IF
           IF IND-RIC-RAMO-BILA = -1
              MOVE HIGH-VALUES TO RIC-RAMO-BILA-NULL
           END-IF.

       Z100-EX.
           EXIT.

      ******************************************************************
       Z150-VALORIZZA-DATA-SERVICES-I.

           MOVE IDSV0003-OPERAZIONE       TO RIC-DS-OPER-SQL
           MOVE 1                         TO RIC-DS-VER
           MOVE IDSV0003-USER-NAME        TO RIC-DS-UTENTE
           MOVE WS-TS-COMPETENZA-AGG-STOR TO RIC-DS-TS-CPTZ.

       Z150-EX.
           EXIT.

      ******************************************************************
       Z200-SET-INDICATORI-NULL.

           IF RIC-IB-RICH-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-IB-RICH
           ELSE
              MOVE 0 TO IND-RIC-IB-RICH
           END-IF
           IF RIC-TS-EFF-ESEC-RICH-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-TS-EFF-ESEC-RICH
           ELSE
              MOVE 0 TO IND-RIC-TS-EFF-ESEC-RICH
           END-IF
           IF RIC-ID-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-ID-OGG
           ELSE
              MOVE 0 TO IND-RIC-ID-OGG
           END-IF
           IF RIC-TP-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-TP-OGG
           ELSE
              MOVE 0 TO IND-RIC-TP-OGG
           END-IF
           IF RIC-IB-POLI-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-IB-POLI
           ELSE
              MOVE 0 TO IND-RIC-IB-POLI
           END-IF
           IF RIC-IB-ADES-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-IB-ADES
           ELSE
              MOVE 0 TO IND-RIC-IB-ADES
           END-IF
           IF RIC-IB-GAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-IB-GAR
           ELSE
              MOVE 0 TO IND-RIC-IB-GAR
           END-IF
           IF RIC-IB-TRCH-DI-GAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-IB-TRCH-DI-GAR
           ELSE
              MOVE 0 TO IND-RIC-IB-TRCH-DI-GAR
           END-IF
           IF RIC-ID-BATCH-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-ID-BATCH
           ELSE
              MOVE 0 TO IND-RIC-ID-BATCH
           END-IF
           IF RIC-ID-JOB-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-ID-JOB
           ELSE
              MOVE 0 TO IND-RIC-ID-JOB
           END-IF
           IF RIC-FL-SIMULAZIONE-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-FL-SIMULAZIONE
           ELSE
              MOVE 0 TO IND-RIC-FL-SIMULAZIONE
           END-IF
           IF RIC-KEY-ORDINAMENTO = HIGH-VALUES
              MOVE -1 TO IND-RIC-KEY-ORDINAMENTO
           ELSE
              MOVE 0 TO IND-RIC-KEY-ORDINAMENTO
           END-IF
           IF RIC-ID-RICH-COLLG-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-ID-RICH-COLLG
           ELSE
              MOVE 0 TO IND-RIC-ID-RICH-COLLG
           END-IF
           IF RIC-TP-RAMO-BILA-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-TP-RAMO-BILA
           ELSE
              MOVE 0 TO IND-RIC-TP-RAMO-BILA
           END-IF
           IF RIC-TP-FRM-ASSVA-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-TP-FRM-ASSVA
           ELSE
              MOVE 0 TO IND-RIC-TP-FRM-ASSVA
           END-IF
           IF RIC-TP-CALC-RIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-TP-CALC-RIS
           ELSE
              MOVE 0 TO IND-RIC-TP-CALC-RIS
           END-IF
           IF RIC-RAMO-BILA-NULL = HIGH-VALUES
              MOVE -1 TO IND-RIC-RAMO-BILA
           ELSE
              MOVE 0 TO IND-RIC-RAMO-BILA
           END-IF.

       Z200-EX.
           EXIT.

      ******************************************************************
       Z400-SEQ-RIGA.

           CONTINUE.

       Z400-EX.
           EXIT.

      ******************************************************************
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.

           MOVE RIC-DT-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO RIC-DT-EFF-DB
           MOVE RIC-DT-RGSTRZ-RICH TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO RIC-DT-RGSTRZ-RICH-DB
           MOVE RIC-DT-PERV-RICH TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO RIC-DT-PERV-RICH-DB
           MOVE RIC-DT-ESEC-RICH TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO RIC-DT-ESEC-RICH-DB.

       Z900-EX.
           EXIT.

      ******************************************************************
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.

           MOVE RIC-DT-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO RIC-DT-EFF
           MOVE RIC-DT-RGSTRZ-RICH-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO RIC-DT-RGSTRZ-RICH
           MOVE RIC-DT-PERV-RICH-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO RIC-DT-PERV-RICH
           MOVE RIC-DT-ESEC-RICH-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO RIC-DT-ESEC-RICH.

       Z950-EX.
           EXIT.

      ******************************************************************
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           MOVE LENGTH OF RIC-KEY-ORDINAMENTO
                       TO RIC-KEY-ORDINAMENTO-LEN.

       Z960-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

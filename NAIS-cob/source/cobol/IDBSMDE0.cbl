       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSMDE0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  04 SET 2008.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDMDE0 END-EXEC.
           EXEC SQL INCLUDE IDBVMDE2 END-EXEC.
           EXEC SQL INCLUDE IDBVMDE3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVMDE1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 MOT-DEROGA.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSMDE0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'MOT_DEROGA' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_MOT_DEROGA
                ,ID_OGG_DEROGA
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_MOT_DEROGA
                ,COD_LIV_AUT
                ,COD_ERR
                ,TP_ERR
                ,DESC_ERR_BREVE
                ,DESC_ERR_EST
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
             INTO
                :MDE-ID-MOT-DEROGA
               ,:MDE-ID-OGG-DEROGA
               ,:MDE-ID-MOVI-CRZ
               ,:MDE-ID-MOVI-CHIU
                :IND-MDE-ID-MOVI-CHIU
               ,:MDE-DT-INI-EFF-DB
               ,:MDE-DT-END-EFF-DB
               ,:MDE-COD-COMP-ANIA
               ,:MDE-TP-MOT-DEROGA
               ,:MDE-COD-LIV-AUT
               ,:MDE-COD-ERR
               ,:MDE-TP-ERR
               ,:MDE-DESC-ERR-BREVE-VCHAR
                :IND-MDE-DESC-ERR-BREVE
               ,:MDE-DESC-ERR-EST-VCHAR
                :IND-MDE-DESC-ERR-EST
               ,:MDE-DS-RIGA
               ,:MDE-DS-OPER-SQL
               ,:MDE-DS-VER
               ,:MDE-DS-TS-INI-CPTZ
               ,:MDE-DS-TS-END-CPTZ
               ,:MDE-DS-UTENTE
               ,:MDE-DS-STATO-ELAB
             FROM MOT_DEROGA
             WHERE     DS_RIGA = :MDE-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO MOT_DEROGA
                     (
                        ID_MOT_DEROGA
                       ,ID_OGG_DEROGA
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,TP_MOT_DEROGA
                       ,COD_LIV_AUT
                       ,COD_ERR
                       ,TP_ERR
                       ,DESC_ERR_BREVE
                       ,DESC_ERR_EST
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                     )
                 VALUES
                     (
                       :MDE-ID-MOT-DEROGA
                       ,:MDE-ID-OGG-DEROGA
                       ,:MDE-ID-MOVI-CRZ
                       ,:MDE-ID-MOVI-CHIU
                        :IND-MDE-ID-MOVI-CHIU
                       ,:MDE-DT-INI-EFF-DB
                       ,:MDE-DT-END-EFF-DB
                       ,:MDE-COD-COMP-ANIA
                       ,:MDE-TP-MOT-DEROGA
                       ,:MDE-COD-LIV-AUT
                       ,:MDE-COD-ERR
                       ,:MDE-TP-ERR
                       ,:MDE-DESC-ERR-BREVE-VCHAR
                        :IND-MDE-DESC-ERR-BREVE
                       ,:MDE-DESC-ERR-EST-VCHAR
                        :IND-MDE-DESC-ERR-EST
                       ,:MDE-DS-RIGA
                       ,:MDE-DS-OPER-SQL
                       ,:MDE-DS-VER
                       ,:MDE-DS-TS-INI-CPTZ
                       ,:MDE-DS-TS-END-CPTZ
                       ,:MDE-DS-UTENTE
                       ,:MDE-DS-STATO-ELAB
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE MOT_DEROGA SET

                   ID_MOT_DEROGA          =
                :MDE-ID-MOT-DEROGA
                  ,ID_OGG_DEROGA          =
                :MDE-ID-OGG-DEROGA
                  ,ID_MOVI_CRZ            =
                :MDE-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :MDE-ID-MOVI-CHIU
                                       :IND-MDE-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :MDE-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :MDE-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :MDE-COD-COMP-ANIA
                  ,TP_MOT_DEROGA          =
                :MDE-TP-MOT-DEROGA
                  ,COD_LIV_AUT            =
                :MDE-COD-LIV-AUT
                  ,COD_ERR                =
                :MDE-COD-ERR
                  ,TP_ERR                 =
                :MDE-TP-ERR
                  ,DESC_ERR_BREVE         =
                :MDE-DESC-ERR-BREVE-VCHAR
                                       :IND-MDE-DESC-ERR-BREVE
                  ,DESC_ERR_EST           =
                :MDE-DESC-ERR-EST-VCHAR
                                       :IND-MDE-DESC-ERR-EST
                  ,DS_RIGA                =
                :MDE-DS-RIGA
                  ,DS_OPER_SQL            =
                :MDE-DS-OPER-SQL
                  ,DS_VER                 =
                :MDE-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :MDE-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :MDE-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :MDE-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :MDE-DS-STATO-ELAB
                WHERE     DS_RIGA = :MDE-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM MOT_DEROGA
                WHERE     DS_RIGA = :MDE-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-MDE CURSOR FOR
              SELECT
                     ID_MOT_DEROGA
                    ,ID_OGG_DEROGA
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_MOT_DEROGA
                    ,COD_LIV_AUT
                    ,COD_ERR
                    ,TP_ERR
                    ,DESC_ERR_BREVE
                    ,DESC_ERR_EST
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
              FROM MOT_DEROGA
              WHERE     ID_MOT_DEROGA = :MDE-ID-MOT-DEROGA
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC
           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_MOT_DEROGA
                ,ID_OGG_DEROGA
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_MOT_DEROGA
                ,COD_LIV_AUT
                ,COD_ERR
                ,TP_ERR
                ,DESC_ERR_BREVE
                ,DESC_ERR_EST
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
             INTO
                :MDE-ID-MOT-DEROGA
               ,:MDE-ID-OGG-DEROGA
               ,:MDE-ID-MOVI-CRZ
               ,:MDE-ID-MOVI-CHIU
                :IND-MDE-ID-MOVI-CHIU
               ,:MDE-DT-INI-EFF-DB
               ,:MDE-DT-END-EFF-DB
               ,:MDE-COD-COMP-ANIA
               ,:MDE-TP-MOT-DEROGA
               ,:MDE-COD-LIV-AUT
               ,:MDE-COD-ERR
               ,:MDE-TP-ERR
               ,:MDE-DESC-ERR-BREVE-VCHAR
                :IND-MDE-DESC-ERR-BREVE
               ,:MDE-DESC-ERR-EST-VCHAR
                :IND-MDE-DESC-ERR-EST
               ,:MDE-DS-RIGA
               ,:MDE-DS-OPER-SQL
               ,:MDE-DS-VER
               ,:MDE-DS-TS-INI-CPTZ
               ,:MDE-DS-TS-END-CPTZ
               ,:MDE-DS-UTENTE
               ,:MDE-DS-STATO-ELAB
             FROM MOT_DEROGA
             WHERE     ID_MOT_DEROGA = :MDE-ID-MOT-DEROGA
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE MOT_DEROGA SET

                   ID_MOT_DEROGA          =
                :MDE-ID-MOT-DEROGA
                  ,ID_OGG_DEROGA          =
                :MDE-ID-OGG-DEROGA
                  ,ID_MOVI_CRZ            =
                :MDE-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :MDE-ID-MOVI-CHIU
                                       :IND-MDE-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :MDE-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :MDE-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :MDE-COD-COMP-ANIA
                  ,TP_MOT_DEROGA          =
                :MDE-TP-MOT-DEROGA
                  ,COD_LIV_AUT            =
                :MDE-COD-LIV-AUT
                  ,COD_ERR                =
                :MDE-COD-ERR
                  ,TP_ERR                 =
                :MDE-TP-ERR
                  ,DESC_ERR_BREVE         =
                :MDE-DESC-ERR-BREVE-VCHAR
                                       :IND-MDE-DESC-ERR-BREVE
                  ,DESC_ERR_EST           =
                :MDE-DESC-ERR-EST-VCHAR
                                       :IND-MDE-DESC-ERR-EST
                  ,DS_RIGA                =
                :MDE-DS-RIGA
                  ,DS_OPER_SQL            =
                :MDE-DS-OPER-SQL
                  ,DS_VER                 =
                :MDE-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :MDE-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :MDE-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :MDE-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :MDE-DS-STATO-ELAB
                WHERE     DS_RIGA = :MDE-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-MDE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-MDE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-MDE
           INTO
                :MDE-ID-MOT-DEROGA
               ,:MDE-ID-OGG-DEROGA
               ,:MDE-ID-MOVI-CRZ
               ,:MDE-ID-MOVI-CHIU
                :IND-MDE-ID-MOVI-CHIU
               ,:MDE-DT-INI-EFF-DB
               ,:MDE-DT-END-EFF-DB
               ,:MDE-COD-COMP-ANIA
               ,:MDE-TP-MOT-DEROGA
               ,:MDE-COD-LIV-AUT
               ,:MDE-COD-ERR
               ,:MDE-TP-ERR
               ,:MDE-DESC-ERR-BREVE-VCHAR
                :IND-MDE-DESC-ERR-BREVE
               ,:MDE-DESC-ERR-EST-VCHAR
                :IND-MDE-DESC-ERR-EST
               ,:MDE-DS-RIGA
               ,:MDE-DS-OPER-SQL
               ,:MDE-DS-VER
               ,:MDE-DS-TS-INI-CPTZ
               ,:MDE-DS-TS-END-CPTZ
               ,:MDE-DS-UTENTE
               ,:MDE-DS-STATO-ELAB
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-EFF-MDE CURSOR FOR
              SELECT
                     ID_MOT_DEROGA
                    ,ID_OGG_DEROGA
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_MOT_DEROGA
                    ,COD_LIV_AUT
                    ,COD_ERR
                    ,TP_ERR
                    ,DESC_ERR_BREVE
                    ,DESC_ERR_EST
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
              FROM MOT_DEROGA
              WHERE     ID_OGG_DEROGA = :MDE-ID-OGG-DEROGA
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_MOT_DEROGA ASC
           END-EXEC.

       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_MOT_DEROGA
                ,ID_OGG_DEROGA
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_MOT_DEROGA
                ,COD_LIV_AUT
                ,COD_ERR
                ,TP_ERR
                ,DESC_ERR_BREVE
                ,DESC_ERR_EST
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
             INTO
                :MDE-ID-MOT-DEROGA
               ,:MDE-ID-OGG-DEROGA
               ,:MDE-ID-MOVI-CRZ
               ,:MDE-ID-MOVI-CHIU
                :IND-MDE-ID-MOVI-CHIU
               ,:MDE-DT-INI-EFF-DB
               ,:MDE-DT-END-EFF-DB
               ,:MDE-COD-COMP-ANIA
               ,:MDE-TP-MOT-DEROGA
               ,:MDE-COD-LIV-AUT
               ,:MDE-COD-ERR
               ,:MDE-TP-ERR
               ,:MDE-DESC-ERR-BREVE-VCHAR
                :IND-MDE-DESC-ERR-BREVE
               ,:MDE-DESC-ERR-EST-VCHAR
                :IND-MDE-DESC-ERR-EST
               ,:MDE-DS-RIGA
               ,:MDE-DS-OPER-SQL
               ,:MDE-DS-VER
               ,:MDE-DS-TS-INI-CPTZ
               ,:MDE-DS-TS-END-CPTZ
               ,:MDE-DS-UTENTE
               ,:MDE-DS-STATO-ELAB
             FROM MOT_DEROGA
             WHERE     ID_OGG_DEROGA = :MDE-ID-OGG-DEROGA
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           EXEC SQL
                OPEN C-IDP-EFF-MDE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           EXEC SQL
                CLOSE C-IDP-EFF-MDE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           EXEC SQL
                FETCH C-IDP-EFF-MDE
           INTO
                :MDE-ID-MOT-DEROGA
               ,:MDE-ID-OGG-DEROGA
               ,:MDE-ID-MOVI-CRZ
               ,:MDE-ID-MOVI-CHIU
                :IND-MDE-ID-MOVI-CHIU
               ,:MDE-DT-INI-EFF-DB
               ,:MDE-DT-END-EFF-DB
               ,:MDE-COD-COMP-ANIA
               ,:MDE-TP-MOT-DEROGA
               ,:MDE-COD-LIV-AUT
               ,:MDE-COD-ERR
               ,:MDE-TP-ERR
               ,:MDE-DESC-ERR-BREVE-VCHAR
                :IND-MDE-DESC-ERR-BREVE
               ,:MDE-DESC-ERR-EST-VCHAR
                :IND-MDE-DESC-ERR-EST
               ,:MDE-DS-RIGA
               ,:MDE-DS-OPER-SQL
               ,:MDE-DS-VER
               ,:MDE-DS-TS-INI-CPTZ
               ,:MDE-DS-TS-END-CPTZ
               ,:MDE-DS-UTENTE
               ,:MDE-DS-STATO-ELAB
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_MOT_DEROGA
                ,ID_OGG_DEROGA
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_MOT_DEROGA
                ,COD_LIV_AUT
                ,COD_ERR
                ,TP_ERR
                ,DESC_ERR_BREVE
                ,DESC_ERR_EST
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
             INTO
                :MDE-ID-MOT-DEROGA
               ,:MDE-ID-OGG-DEROGA
               ,:MDE-ID-MOVI-CRZ
               ,:MDE-ID-MOVI-CHIU
                :IND-MDE-ID-MOVI-CHIU
               ,:MDE-DT-INI-EFF-DB
               ,:MDE-DT-END-EFF-DB
               ,:MDE-COD-COMP-ANIA
               ,:MDE-TP-MOT-DEROGA
               ,:MDE-COD-LIV-AUT
               ,:MDE-COD-ERR
               ,:MDE-TP-ERR
               ,:MDE-DESC-ERR-BREVE-VCHAR
                :IND-MDE-DESC-ERR-BREVE
               ,:MDE-DESC-ERR-EST-VCHAR
                :IND-MDE-DESC-ERR-EST
               ,:MDE-DS-RIGA
               ,:MDE-DS-OPER-SQL
               ,:MDE-DS-VER
               ,:MDE-DS-TS-INI-CPTZ
               ,:MDE-DS-TS-END-CPTZ
               ,:MDE-DS-UTENTE
               ,:MDE-DS-STATO-ELAB
             FROM MOT_DEROGA
             WHERE     ID_MOT_DEROGA = :MDE-ID-MOT-DEROGA
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-CPZ-MDE CURSOR FOR
              SELECT
                     ID_MOT_DEROGA
                    ,ID_OGG_DEROGA
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,TP_MOT_DEROGA
                    ,COD_LIV_AUT
                    ,COD_ERR
                    ,TP_ERR
                    ,DESC_ERR_BREVE
                    ,DESC_ERR_EST
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
              FROM MOT_DEROGA
              WHERE     ID_OGG_DEROGA = :MDE-ID-OGG-DEROGA
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_MOT_DEROGA ASC
           END-EXEC.

       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_MOT_DEROGA
                ,ID_OGG_DEROGA
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,TP_MOT_DEROGA
                ,COD_LIV_AUT
                ,COD_ERR
                ,TP_ERR
                ,DESC_ERR_BREVE
                ,DESC_ERR_EST
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
             INTO
                :MDE-ID-MOT-DEROGA
               ,:MDE-ID-OGG-DEROGA
               ,:MDE-ID-MOVI-CRZ
               ,:MDE-ID-MOVI-CHIU
                :IND-MDE-ID-MOVI-CHIU
               ,:MDE-DT-INI-EFF-DB
               ,:MDE-DT-END-EFF-DB
               ,:MDE-COD-COMP-ANIA
               ,:MDE-TP-MOT-DEROGA
               ,:MDE-COD-LIV-AUT
               ,:MDE-COD-ERR
               ,:MDE-TP-ERR
               ,:MDE-DESC-ERR-BREVE-VCHAR
                :IND-MDE-DESC-ERR-BREVE
               ,:MDE-DESC-ERR-EST-VCHAR
                :IND-MDE-DESC-ERR-EST
               ,:MDE-DS-RIGA
               ,:MDE-DS-OPER-SQL
               ,:MDE-DS-VER
               ,:MDE-DS-TS-INI-CPTZ
               ,:MDE-DS-TS-END-CPTZ
               ,:MDE-DS-UTENTE
               ,:MDE-DS-STATO-ELAB
             FROM MOT_DEROGA
             WHERE     ID_OGG_DEROGA = :MDE-ID-OGG-DEROGA
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           EXEC SQL
                OPEN C-IDP-CPZ-MDE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           EXEC SQL
                CLOSE C-IDP-CPZ-MDE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           EXEC SQL
                FETCH C-IDP-CPZ-MDE
           INTO
                :MDE-ID-MOT-DEROGA
               ,:MDE-ID-OGG-DEROGA
               ,:MDE-ID-MOVI-CRZ
               ,:MDE-ID-MOVI-CHIU
                :IND-MDE-ID-MOVI-CHIU
               ,:MDE-DT-INI-EFF-DB
               ,:MDE-DT-END-EFF-DB
               ,:MDE-COD-COMP-ANIA
               ,:MDE-TP-MOT-DEROGA
               ,:MDE-COD-LIV-AUT
               ,:MDE-COD-ERR
               ,:MDE-TP-ERR
               ,:MDE-DESC-ERR-BREVE-VCHAR
                :IND-MDE-DESC-ERR-BREVE
               ,:MDE-DESC-ERR-EST-VCHAR
                :IND-MDE-DESC-ERR-EST
               ,:MDE-DS-RIGA
               ,:MDE-DS-OPER-SQL
               ,:MDE-DS-VER
               ,:MDE-DS-TS-INI-CPTZ
               ,:MDE-DS-TS-END-CPTZ
               ,:MDE-DS-UTENTE
               ,:MDE-DS-STATO-ELAB
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-MDE-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO MDE-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-MDE-DESC-ERR-BREVE = -1
              MOVE HIGH-VALUES TO MDE-DESC-ERR-BREVE
           END-IF
           IF IND-MDE-DESC-ERR-EST = -1
              MOVE HIGH-VALUES TO MDE-DESC-ERR-EST
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO MDE-DS-OPER-SQL
           MOVE 0                   TO MDE-DS-VER
           MOVE IDSV0003-USER-NAME TO MDE-DS-UTENTE
           MOVE '1'                   TO MDE-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO MDE-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO MDE-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF MDE-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-MDE-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-MDE-ID-MOVI-CHIU
           END-IF
           IF MDE-DESC-ERR-BREVE = HIGH-VALUES
              MOVE -1 TO IND-MDE-DESC-ERR-BREVE
           ELSE
              MOVE 0 TO IND-MDE-DESC-ERR-BREVE
           END-IF
           IF MDE-DESC-ERR-EST = HIGH-VALUES
              MOVE -1 TO IND-MDE-DESC-ERR-EST
           ELSE
              MOVE 0 TO IND-MDE-DESC-ERR-EST
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : MDE-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE MOT-DEROGA TO WS-BUFFER-TABLE.

           MOVE MDE-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO MDE-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO MDE-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO MDE-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO MDE-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO MDE-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO MDE-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO MDE-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO MOT-DEROGA.

           MOVE WS-ID-MOVI-CRZ  TO MDE-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO MDE-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO MDE-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO MDE-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO MDE-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO MDE-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO MDE-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE MDE-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO MDE-DT-INI-EFF-DB
           MOVE MDE-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO MDE-DT-END-EFF-DB.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE MDE-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO MDE-DT-INI-EFF
           MOVE MDE-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO MDE-DT-END-EFF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.


           MOVE LENGTH OF MDE-DESC-ERR-BREVE
                       TO MDE-DESC-ERR-BREVE-LEN
           MOVE LENGTH OF MDE-DESC-ERR-EST
                       TO MDE-DESC-ERR-EST-LEN.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

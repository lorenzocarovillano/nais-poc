      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0098.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0098
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0098'.
       01  WK-APPO-DATA                     PIC 9(008) VALUE ZEROES.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
       01  WK-PRESTITO                      PIC X(02).
           88  PREST-ATTIVO              VALUE 'SI'.
           88  PREST-NON-ATTIVO          VALUE 'NO'.
       01  WK-DT-MAX                        PIC 9(08).
      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA LDBS1780
      *---------------------------------------------------------------*
           COPY LDBV6151.
      *---------------------------------------------------------------*
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-ADE.
          03 DADE-AREA-ADES.
             04 DADE-ELE-ADES-MAX        PIC S9(04) COMP.
             04 DADE-TAB-ADES.
             COPY LCCVADE1               REPLACING ==(SF)== BY ==DADE==.

      *----------------------------------------------------------------*
      *--> AREA TABELLE
      *----------------------------------------------------------------*
      *  TABELLA PRESTITO
         COPY IDBVPRE1.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *    COPY DISPATCHER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
           COPY IDSI0011.
           COPY IDSO0011.

      *----------------------------------------------------------------*
      *    COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-ADE                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0007.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0007.
      *----------------------------------------------------------------*
      *
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000
      *
           GOBACK.
      *
      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           INITIALIZE LDBV6151.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-ADE.

      *
      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1110-LEGGI-PRESTITO  THRU  S1110-EX
               IF  PREST-ATTIVO
                   MOVE  PRE-DT-DECOR-PREST
                            TO WK-DT-MAX
                   PERFORM S1255-CALCOLA-IMPORTO  THRU S1255-EX
                   IF IDSV0003-NOT-FOUND
                      MOVE  WK-DT-MAX
                            TO IVVC0213-VAL-STR-O
                   ELSE
                      CONTINUE
                   END-IF
               ELSE
                   MOVE  ZERO               TO IVVC0213-VAL-STR-O
               END-IF
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    LETTURA DELLA TABELLA PRESTITI CON DATA DECORRENZA PRESTITO
      *    MINORE
      *----------------------------------------------------------------*
       S1110-LEGGI-PRESTITO.

           SET PREST-NON-ATTIVO       TO TRUE

           INITIALIZE                     PREST.
           SET  IDSV0003-TRATT-X-EFFETTO TO TRUE.
           SET  IDSV0003-WHERE-CONDITION  TO TRUE.
21441      SET  IDSV0003-SELECT        TO TRUE
 "    *    SET  IDSV0003-FETCH-FIRST   TO TRUE.
           MOVE 'AD'                   TO PRE-TP-OGG
           MOVE DADE-ID-ADES           TO PRE-ID-OGG
           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                       TO PRE-COD-COMP-ANIA
      *    MOVE IDSV0003-DATA-INIZIO-EFFETTO
      *                                TO LDBV6151-DT-DECOR-PREST
           MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WK-APPO-DATA
           MOVE 99991230               TO IDSV0003-DATA-INIZIO-EFFETTO
      *    MOVE 999912314023595998     TO IDSV0003-DATA-COMPETENZA
      *
           MOVE 'LDBS6160'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 PREST
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS6160 ERRORE CHIAMATA '
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
           MOVE  WK-APPO-DATA          TO IDSV0003-DATA-INIZIO-EFFETTO

           IF IDSV0003-SUCCESSFUL-RC
              EVALUATE TRUE
                WHEN IDSV0003-SUCCESSFUL-SQL
                   SET PREST-ATTIVO      TO  TRUE
                WHEN IDSV0003-NOT-FOUND
                   CONTINUE
                WHEN OTHER
                   SET IDSV0003-INVALID-OPER            TO TRUE
                   MOVE WK-CALL-PGM        TO IDSV0003-COD-SERVIZIO-BE
                   STRING 'CHIAMATA LDBS6160 ;'
                      IDSV0003-RETURN-CODE ';'
                      IDSV0003-SQLCODE
                      DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                   END-STRING
              END-EVALUATE
           ELSE
              SET IDSV0003-INVALID-OPER            TO TRUE
              MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
              STRING 'ERRORE CHIAMATA LDBS6160'   ';'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                   DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.

       S1110-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-ADES
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DADE-AREA-ADES
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALCOLA
      *----------------------------------------------------------------*
       S1255-CALCOLA-IMPORTO.
      *
           INITIALIZE LDBV6151.
      *
           SET  IDSV0003-TRATT-X-EFFETTO TO TRUE.
           SET  IDSV0003-SELECT           TO TRUE.
           SET  IDSV0003-WHERE-CONDITION  TO TRUE.
           MOVE 'PR'                           TO LDBV6151-TP-TIT-01.
           MOVE 'IP'                           TO LDBV6151-TP-TIT-02.
           MOVE 'IN'                           TO LDBV6151-TP-STAT-TIT.
           MOVE 'PO'                           TO LDBV6151-TP-OGG.
           MOVE DADE-ID-POLI                   TO LDBV6151-ID-OGG.
           MOVE PRE-DT-DECOR-PREST
                                          TO LDBV6151-DT-DECOR-PREST.
      *
           MOVE 'LDBS6150'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 LDBV6151
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS6150 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-RC
            IF IDSV0003-SUCCESSFUL-SQL
               MOVE LDBV6151-DT-MAX               TO IVVC0213-VAL-STR-O
            ELSE
               IF IDSV0003-NOT-FOUND
                  CONTINUE
               ELSE
                IF IDSV0003-SQLCODE = -305
                   SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                ELSE
                   SET IDSV0003-INVALID-OPER            TO TRUE
                END-IF
      *
                MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
                STRING 'CHIAMATA LDBS6150 ;'
                  IDSV0003-RETURN-CODE ';'
                  IDSV0003-SQLCODE
                  DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                END-STRING
               END-IF
            END-IF
           ELSE
              SET IDSV0003-INVALID-OPER            TO TRUE
              MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
              STRING 'ERRORE CHIAMATA LDBS6150'   ';'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                   DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.
      *
       S1255-EX.
           EXIT.

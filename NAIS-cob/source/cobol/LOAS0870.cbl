      *  ============================================================= *
      *                                                                *
      *        PORTAFOGLIO VITA ITALIA  VER 1.0                        *
      *                                                                *
      *  ============================================================= *
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LOAS0870.
       AUTHOR.             ATS.
       DATE-WRITTEN.       SETTEMBRE 2007.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *     PROGRAMMA ..... LOAS0870                                   *
      *     TIPOLOGIA...... OPERAZIONI AUTOMATICHE                     *
      *     PROCESSO....... ADEGUAMENTO PREMIO PRESTAZIONE             *
      *     FUNZIONE....... BATCH                                      *
      *     DESCRIZIONE.... GAP RICH090 COLLETTIVE                     *
      *     PAGINA WEB..... N.A.                                       *
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
      *
       DATA DIVISION.
      *
       WORKING-STORAGE SECTION.
      *
      *----------------------------------------------------------------*
      *       COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
      *
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IEAV9903.
           COPY IDSV0002.
           COPY IDSV0015.
      *
      *----------------------------------------------------------------*
      *     COPY DELLE TABELLE DB2 PER IL DISPATCHER DATI
      *----------------------------------------------------------------*
      *
      *  --> TITOLO CONTABILE
           COPY IDBVTIT1.
      *  --> PARAMETRO COMPAGNIA
           COPY IDBVPCO1.

      *  --> Where condition ad hoc per lettura Titolo Contabile
           COPY LDBV7141.

      *  --> DETTAGLIO TITOLO CONTABILE
           COPY IDBVDTC1.

      *----------------------------------------------------------------*
      *  AREE TIPOLOGICHE OGGETTO
      *----------------------------------------------------------------*
           COPY LCCVXOG0.

      *----------------------------------------------------------------*
      *   INDICI
      *----------------------------------------------------------------*
      *
       01  IX-INDICI.
      *
           03 IX-TAB-TGA                   PIC S9(04) COMP.
      *
           03 IX-TGA-W870                  PIC S9(04) COMP.
           03 IX-TIT-W870                  PIC S9(04) COMP.
           03 IX-GRZ                       PIC S9(04) COMP.
           03 IX-PMO                       PIC S9(04) COMP.
           03 INDPMO                       PIC S9(04) COMP.
      *
           03 IX-TAB-ERR                   PIC S9(04) COMP.
           03 IX-TAB-FLAGS                 PIC S9(04) COMP.
       01 WK-INTERVALLO                   PIC 9(02) VALUE ZEROES.
      *
       01 WK-ID-TRCH-DI-GAR           PIC S9(9) COMP-3 VALUE ZEROES.
       01 WK-TIT-Z                    PIC Z(9).
       01 WK-TGA-Z                    PIC Z(9).
      *----------------------------------------------------------------*
      *    AREE DI COMODO
      *----------------------------------------------------------------*
      *
      * --> Ricerca occorrenza Titolo Contabile
      *
       01  WK-TIT-TROVATO               PIC X.
           88 WK-TIT-SI                   VALUE 'S'.
           88 WK-TIT-NO                   VALUE 'N'.
      *
      * --> Ricerca occorrenza Garanzia
      *
       01  WK-GRZ-TROVATA               PIC X.
           88 WK-GRZ-SI                   VALUE 'S'.
           88 WK-GRZ-NO                   VALUE 'N'.
      *
      * --> Ricerca occorrenza Parametro Garanzia
      *
       01  WK-PMO-TROVATA               PIC X.
           88 WK-PMO-SI                   VALUE 'S'.
           88 WK-PMO-NO                   VALUE 'N'.
      *
      * -->  Flag fine fetch
      *
       01 WK-FINE-FETCH                 PIC X(01).
          88 WK-FINE-FETCH-SI             VALUE 'Y'.
          88 WK-FINE-FETCH-NO             VALUE 'N'.
      *
      * --> Area di appoggio per i messaggi di errore
      *
       01 WK-GESTIONE-MSG-ERR.
          03 WK-COD-ERR                 PIC X(06) VALUE ZEROES.
          03 WK-LABEL-ERR               PIC X(30) VALUE SPACES.
          03 WK-STRING                  PIC X(85) VALUE SPACES.
      *
       01 WK-DT-INIZIO-CALCOLO          PIC 9(08) VALUE ZEROES.
       01 WK-DT-A                       PIC 9(08) VALUE ZEROES.
      *
       01 WK-DT-DA                      PIC 9(08) VALUE ZEROES.
      *
       01 WK-APPO-DT-NUM                PIC 9(08) VALUE ZEROES.
      *
       01 WK-APPO-DT.
          03 WK-APPO-DT-AA              PIC 9(04) VALUE ZEROES.
          03 WK-APPO-DT-MM              PIC 9(02) VALUE ZEROES.
          03 WK-APPO-DT-GG              PIC 9(02) VALUE ZEROES.
      *
       01 WMAX-TAB-TIT                  PIC 9(04) VALUE 12.
      *
      * --> Nome del programma Businesss Service
      *
       01 WK-PGM                        PIC X(08) VALUE 'LOAS0870'.
      *
      * --> Area di accept della data dalla variabile di sistema DATE
      *
       01 WK-CURRENT-DATE               PIC 9(08) VALUE ZEROES.
      *
      * --> Area di appoggio per gestione Management Fee
      *
VSTEF *--> Tipo periodo premio (Garanzia)
       01  WS-TP-PER-PREMIO                       PIC X(001).
           88 WS-PREMIO-UNICO                     VALUE 'U'.
           88 WS-PREMIO-RICORRENTE                VALUE 'R'.
           88 WS-PREMIO-ANNUALE                   VALUE 'A'.
      *--> NUMERO ANNI PAGAMENTO PREMIO (GARANZIA)
       01 WS-NUM-AA-PAG-PRE                       PIC S9(5)  COMP-3.
       01 WS-NUM-AA                               PIC  9(5).
      *--> FLAG CONTROLLO SUL NUMERO ANNI PAGAMENTO PREMI
       77 WK-NUM-AA-PAG                           PIC X(02).
          88 WK-NUM-AA-PAG-OK                     VALUE 'OK'.
          88 WK-NUM-AA-PAG-KO                     VALUE 'KO'.

       77 WK-TGA-INC                              PIC X.
          88 TIT-INCASSATO                        VALUE 'S'.
          88 TIT-NON-INCASSATO                    VALUE 'N'.

      *--> AREA FLAGS PER TGA
       01 WS-FLAGS-TGA.
          02 WS-FG OCCURS 300.
             03 WS-FLAG-INC-TGA                     PIC X.

      * --> Flag fine cursore dettaglio titolo contabile
       77 SW-CUR-DTC                              PIC X.
          88 INIT-CUR-DTC                         VALUE 'S'.
          88 FINE-CUR-DTC                         VALUE 'N'.

      * --> Flag fine cursore dettaglio titolo contabile
       77 SW-TROVATO-DTC                          PIC X.
          88 DETTAGLIO-TROVATO                    VALUE 'S'.
          88 DETTAGLIO-NON-TROVATO                VALUE 'N'.



      *----------------------------------------------------------------*
      *    AREA CALCOLO DIFFERENZA TRA DATE
      *----------------------------------------------------------------*
      *
      *  --> Area per gestione della differenza tra date
      *
       01 FORMATO                       PIC X(01) VALUE ZEROES.
       01 DATA-INFERIORE.
          05 AAAA-INF                   PIC 9(04) VALUE ZEROES.
          05 MM-INF                     PIC 9(02) VALUE ZEROES.
          05 GG-INF                     PIC 9(02) VALUE ZEROES.
       01 DATA-SUPERIORE.
          05 AAAA-SUP                   PIC 9(04) VALUE ZEROES.
          05 MM-SUP                     PIC 9(02) VALUE ZEROES.
          05 GG-SUP                     PIC 9(02) VALUE ZEROES.
       01 GG-DIFF                       PIC 9(05) VALUE ZEROES.
       01 CODICE-RITORNO                PIC X(01) VALUE ZEROES.
       01 WK-COM-AAMM.
          05 WK-COM-AA                  PIC 9(04) VALUE ZEROES.
          05 WK-COM-MM                  PIC 9(02) VALUE ZEROES.
       01 DURATA.
          05 DUR-AAAA                   PIC 9(04) VALUE ZEROES.
          05 DUR-MM                     PIC 9(02) VALUE ZEROES.
          05 DUR-GG                     PIC 9(02) VALUE ZEROES.
      *
      *  --> Area per gestione resti operazioni
      *
       01 WK-OPERAZIONI.
          03 WK-APPO-RESTO              PIC S9(5)V9(5) VALUE ZEROES.
          03 WK-APPO-RESTO1             PIC S9(5)V9(5) VALUE ZEROES.
      *
      *  --> GESTIONE DATE
       01  LCCS0010                         PIC X(8) VALUE 'LCCS0010'.
      *  --> MODULO PER CALCOLI SU DATE
       01  LCCS0003                         PIC X(8) VALUE 'LCCS0003'.
      *
      *----------------------------------------------------------------*
      *     COPY DISPATCHER
      *----------------------------------------------------------------*
      *
       01  IDSV0012.
           COPY IDSV0012.
      *
       01  DISPATCHER-VARIABLES.
      *  COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *  COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *
       COPY IDSV0003.
      *
      *----------------------------------------------------------------*
      *    AREA IO SERVIZIO CALCOLI SULLE DATE
      *----------------------------------------------------------------*
      *
          COPY LCCC0003.
      *
       01 IN-RCODE                       PIC 9(2).
      *
      *----------------------------------------------------------------*
      *  AREE TIPOLOGICHE DI PORTAFOGLIO
      *----------------------------------------------------------------*
      *
          COPY LCCC0006.
      *
      *----------------------------------------------------------------*
      *        L I N K A G E   S E C T I O N
      *----------------------------------------------------------------*
      *
       LINKAGE SECTION.
      *
      *----------------------------------------------------------------*
      *   AREA-IDSV0001
      *----------------------------------------------------------------*
      *
       01  AREA-IDSV0001.
           COPY IDSV0001.
      *
       01 WCOM-AREA-STATI.
          COPY LCCC0001               REPLACING ==(SF)== BY ==WCOM==.

      *----------------------------------------------------------------*
      *  AREA DI PASSAGGIO DA-A MAIN
      *----------------------------------------------------------------*
      *
      * -- Area Parametro Movimento
       01 WPMO-AREA-PARAM-MOVI.
          04 WPMO-ELE-PARAM-MOV-MAX      PIC S9(04) COMP.
             COPY LCCVPMOA REPLACING   ==(SF)==  BY ==WPMO==.
             COPY LCCVPMO1               REPLACING ==(SF)== BY ==WPMO==.

      * -- Area polizza
       01 WPOL-AREA-POLIZZA.
          04 WPOL-ELE-POLI-MAX           PIC S9(04) COMP.
          04 WPOL-TAB-POL.
             COPY LCCVPOL1               REPLACING ==(SF)== BY ==WPOL==.

      * -- Area Adesione
       01 WADE-AREA-ADESIONE.
          04 WADE-ELE-ADES-MAX           PIC S9(04) COMP.
          04 WADE-TAB-ADE                OCCURS 1.
             COPY LCCVADE1               REPLACING ==(SF)== BY ==WADE==.

      * -- Area Garanzia
       01 WGRZ-AREA-GARANZIA.
          04 WGRZ-ELE-GAR-MAX            PIC S9(04) COMP.
             COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
             COPY LCCVGRZ1               REPLACING ==(SF)== BY ==WGRZ==.

      * -- Area Tranche Di Garanzia
       01 WTGA-AREA-TRANCHE.
          04 WTGA-ELE-TRAN-MAX           PIC S9(04) COMP.
             COPY LCCVTGAC REPLACING   ==(SF)==  BY ==WTGA==.
             COPY LCCVTGA1               REPLACING ==(SF)== BY ==WTGA==.
      *
       01 W870-AREA-LOAS0870.
          COPY LOAC0870                  REPLACING ==(SF)== BY ==W870==.
      *
      *  ====================  M A I N    L I N E  =================== *
      *
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                WPMO-AREA-PARAM-MOVI
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TRANCHE
                                W870-AREA-LOAS0870.

      *
           PERFORM S00000-OPERAZ-INIZIALI
              THRU S00000-OPERAZ-INIZIALI-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10000-ELABORAZIONE
                 THRU S10000-ELABORAZIONE-EX
      *
           END-IF.
      *
           PERFORM S90000-OPERAZ-FINALI
              THRU S90000-OPERAZ-FINALI-EX.
      *
           GOBACK.
      *
      * ============================================================== *
      * -->           O P E R Z I O N I   I N I Z I A L I          <-- *
      * ============================================================== *
      *
       S00000-OPERAZ-INIZIALI.
      *
           MOVE 'S00000-OPERAZ-INIZIALI'
             TO WK-LABEL-ERR.
      *
      * --> Inizializazione delle aree di working storage
      *
           PERFORM S00100-INIZIA-AREE-WS
              THRU S00100-INIZIA-AREE-WS-EX.
      *
       S00000-OPERAZ-INIZIALI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     INIZIALIZZAZIONE DELLE AREE DI WORKING STORAGE             *
      *----------------------------------------------------------------*
      *
       S00100-INIZIA-AREE-WS.
      *
           MOVE 'S00100-INIZIA-AREE-WS'
             TO WK-LABEL-ERR.
      *
      *  Inizializazione di tutti gli indici e le aree di WS
      *
           INITIALIZE IX-INDICI.
      *
           SET  IDSV0001-ESITO-OK
             TO TRUE.
           MOVE  ZEROES  TO  WS-NUM-AA-PAG-PRE
                             WS-NUM-AA.
      *
       S00100-INIZIA-AREE-WS-EX.
           EXIT.
      *
      * ============================================================== *
      * -->               E L A B O R A Z I O N E                  <-- *
      * ============================================================== *
      *
       S10000-ELABORAZIONE.
      *
           MOVE 'S10000-ELABORAZIONE'
             TO WK-LABEL-ERR.
      *
           IF IDSV0001-ESITO-OK
              PERFORM S10050-LEGGI-PARAM-COMP
                 THRU S10050-EX
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              IF W870-TGA-NUM-MAX-ELE = ZEROES
      *
                 PERFORM S10200-CARICA-TIT-DB
                    THRU S10200-CARICA-TIT-DB-EX
      *
              END-IF
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10600-DETERMINA-TP-RIVAL
                 THRU S10600-DETERMINA-TP-RIVAL-EX
      *
           END-IF.
      *
       S10000-ELABORAZIONE-EX.
           EXIT.
      *----------------------------------------------------------------*
      *              ACCESSO ALLA TABELLA PARAMETRO COMPAGNIA          *
      *----------------------------------------------------------------*
      *
       S10050-LEGGI-PARAM-COMP.
           MOVE 'S10050-LEGGI-PARAM-COMP' TO WK-LABEL-ERR
      *
           PERFORM S10070-IMPOSTA-PARAM-COMP
              THRU S10070-IMPOSTA-PARAM-COMP-EX.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata
      *
                    MOVE 'COMPAGNIA NON TROVATA'
                      TO WK-STRING
                    MOVE '005069'
                      TO WK-COD-ERR
                    PERFORM GESTIONE-ERR-STD
                       THRU GESTIONE-ERR-STD-EX
      *
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
      *  --> Operazione eseguita correttamente
      *
                       MOVE IDSO0011-BUFFER-DATI
                         TO PARAM-COMP
                       IF PCO-NUM-MM-CALC-MORA-NULL = HIGH-VALUES
                          MOVE ZEROES TO PCO-NUM-MM-CALC-MORA
                       END-IF
      *
                  WHEN OTHER
      *
      *  --> Errore di accesso al db
      *
                       MOVE 'ERRORE LETTURA PARAMETRO COMPAGNIA'
                         TO WK-STRING
                       MOVE '005016'
                         TO WK-COD-ERR
                       PERFORM GESTIONE-ERR-STD
                          THRU GESTIONE-ERR-STD-EX
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore dispatcher
      *
              MOVE 'ERRORE DISPATCHER LETTURA PARAMETRO COMPAGNIA'
                TO WK-STRING
              MOVE '005016'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S10050-EX.
           EXIT.
      *
      * -------------------------------------------------------------- *
      * VALORIZZAZIONE DELLE CHIAVI DELLA TABELLA PARAMETRO COMPAGNIA  *
      * -------------------------------------------------------------- *
      *
       S10070-IMPOSTA-PARAM-COMP.
      *
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO PCO-COD-COMP-ANIA.
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE ZERO
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
                IDSI0011-DATA-COMPETENZA.
      *
      *  --> Nome tabella fisica db
      *
           MOVE 'PARAM-COMP'
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE PARAM-COMP
             TO IDSI0011-BUFFER-DATI.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-SELECT
             TO TRUE.
      *
      *  --> Tipo livello
      *
           SET IDSI0011-PRIMARY-KEY
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSI0011-TRATT-SENZA-STOR
             TO TRUE.
      *
      *  --> Inizializzazione Return Code
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S10070-IMPOSTA-PARAM-COMP-EX.
           EXIT.
      *

      *----------------------------------------------------------------*
      * CARICAMENTO TITOLO CONTABILE DA DB
      *----------------------------------------------------------------*
      *
       S10200-CARICA-TIT-DB.
      *
           MOVE 'S10200-CARICA-TIT-DB'
             TO WK-LABEL-ERR.
      *
           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
                OR IDSV0001-ESITO-KO
      *
                  ADD 1 TO IX-TAB-FLAGS
                  MOVE 'N' TO WS-FLAG-INC-TGA(IX-TAB-FLAGS)
                  PERFORM S10230-RICERCA-TITOLO
                     THRU S10230-RICERCA-TITOLO-EX
      *
           END-PERFORM.
      *
       S10200-CARICA-TIT-DB-EX.
           EXIT.
      *---------------------------------------------------------------
      * DETERMINA LA DATA INIZIO DA CUI DETERMINA L'INTERVALLO
      * DI ESTRAZIONE DEI TITOLI CONTABILI
      *---------------------------------------------------------------
       S10209-DET-DATA-INI-CALC.
      *
           MOVE 'S10209-DET-DATA-INI-CALC' TO WK-LABEL-ERR.
           IF WPMO-DT-RICOR-PREC-NULL(INDPMO) = HIGH-VALUES
              MOVE WTGA-DT-DECOR(IX-TAB-TGA)  TO WK-DT-INIZIO-CALCOLO
           ELSE
              MOVE WPMO-DT-RICOR-PREC(INDPMO) TO WK-DT-INIZIO-CALCOLO
21901      END-IF.
      *
       S10209-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VENGONO SOTTRATTI TRE MESI ALL'EFFETTO DI ELABORAZIONE
      *----------------------------------------------------------------*
      *
       S10210-DET-DATA-A.
      *
           MOVE 'S10210-DET-DATA-A'
             TO WK-LABEL-ERR.
      *
           MOVE 12 TO WK-INTERVALLO
           COMPUTE WK-INTERVALLO =
                   WK-INTERVALLO - PCO-NUM-MM-CALC-MORA - 1

           MOVE ZEROES
             TO WK-DT-A.
      *
      * --> A2K-DELTA -> Delta Da Sommare
      *
           MOVE WK-INTERVALLO
             TO A2K-DELTA.
      *
           MOVE 'M'
             TO A2K-TDELTA.
      *
      * --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
      *
           MOVE WK-DT-INIZIO-CALCOLO
             TO A2K-INAMG.
      *
      * --> A2K-FUNZ -> Tipo Funzione: Somma Delta
      *
           MOVE '02'
             TO A2K-FUNZ.
      *
      * --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
      *
           MOVE '03'
             TO A2K-INFO.
      *
      * --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
      *
           MOVE '0'
             TO A2K-INICON.
      *
      * --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
      *
           MOVE '0'
             TO A2K-FISLAV.
      *
           PERFORM S10220-CHIAMA-LCCS0003
              THRU S10220-CHIAMA-LCCS0003-EX.
      *
           IF IN-RCODE  = '00'
              MOVE A2K-OUAMG TO WK-DT-A
           END-IF.
      *
       S10210-DET-DATA-A-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * DATA-DA PER PREMIO ANNUO
      *----------------------------------------------------------------*
      *
       S10211-DET-DATA-DA.
      *
           MOVE 'S10211-DET-DATA-DA'
             TO WK-LABEL-ERR.
      *

           MOVE ZEROES                        TO WK-DT-DA.
      *
           MOVE PCO-NUM-MM-CALC-MORA       TO A2K-DELTA
           MOVE 'M'                        TO A2K-TDELTA
           MOVE WK-DT-INIZIO-CALCOLO       TO A2K-INAMG
           MOVE '03'                       TO A2K-FUNZ
           MOVE '03'                       TO A2K-INFO
           MOVE '0'                        TO A2K-INICON
           MOVE '0'                        TO A2K-FISLAV
           PERFORM S10220-CHIAMA-LCCS0003
              THRU S10220-CHIAMA-LCCS0003-EX
           IF IN-RCODE  = '00'
                 MOVE A2K-OUAMG               TO WK-DT-DA
           END-IF.
      *
       S10211-DET-DATA-DA-EX.
           EXIT.
      *
      *
      *----------------------------------------------------------------*
      * DATA-DA DATA-A PER PREMIO UNICO E UNICO RICORRENTE
      *----------------------------------------------------------------*
      *
       S10215-DET-DATE-PUR.
      *
           MOVE 'S10215-DET-DATE-PUR'
             TO WK-LABEL-ERR.
      *
           MOVE ZEROES TO WK-DT-DA.
           MOVE ZEROES TO WK-DT-A.
      *
           IF WPMO-DT-RICOR-PREC-NULL(INDPMO) = HIGH-VALUES
              MOVE WTGA-DT-DECOR(IX-TAB-TGA)  TO WK-DT-DA
           ELSE
              MOVE WPMO-DT-RICOR-PREC(INDPMO) TO WK-DT-DA
           END-IF
      *
           MOVE WPMO-DT-RICOR-SUCC(INDPMO) TO WK-DT-A.
      *
       S10215-DET-DATE-PUR-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                    CALL AL MODULO LCCS0003                     *
      *----------------------------------------------------------------*
      *
       S10220-CHIAMA-LCCS0003.
      *
           MOVE 'S10220-CHIAMA-LCCS0003'
             TO WK-LABEL-ERR.
      *
           CALL LCCS0003         USING IO-A2K-LCCC0003
                                       IN-RCODE
      *
           ON EXCEPTION
      *
                 MOVE 'LCCS0003'
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'ERRORE PROGRAMMA LCCS0003'
                   TO CALL-DESC
                 MOVE 'S10220-CHIAMA-LCCS0003'
                   TO WK-LABEL-ERR
      *
                 PERFORM GESTIONE-ERR-SIST
                    THRU GESTIONE-ERR-SIST-EX
      *
           END-CALL.
      *
           IF IN-RCODE NOT = '00'
              MOVE 'ERRORE SOTTRAZIONE TRE MESI'
                TO WK-STRING
              MOVE '001114'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
           END-IF.
      *
       S10220-CHIAMA-LCCS0003-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                     RICERCA TITOLI CONTABILI                   *
      *----------------------------------------------------------------*
      *
       S10230-RICERCA-TITOLO.
      *
           MOVE 'S10230-RICERCA-TITOLO'
             TO WK-LABEL-ERR.
      *
           ADD 1
             TO IX-TGA-W870
                W870-TGA-NUM-MAX-ELE.
      *
           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
             TO W870-ID-TRCH-DI-GAR(IX-TGA-W870)
                WK-ID-TRCH-DI-GAR.
      *
      * -->> Indicatore di fine fetch
      *
           SET WK-FINE-FETCH-NO
             TO TRUE.
      *
      * -->> Flag Ricerca Titolo Contabile Incassato
      *
           SET WK-TIT-NO
               TO TRUE.
      *
           MOVE 'IN'
             TO LDBV7141-TP-STA-TIT1.
      *
           MOVE 'EM'
             TO LDBV7141-TP-STA-TIT2.

           SET IDSI0011-FETCH-FIRST
             TO TRUE.
      *
21901      PERFORM RICERCA-GAR
  ..          THRU RICERCA-GAR-EX.
  ..  *
21901      PERFORM RICERCA-PMO
  ..          THRU RICERCA-PMO-EX.
  ..  *
  ..       IF  WK-GRZ-SI
  ..       AND (WS-PREMIO-UNICO OR WS-PREMIO-RICORRENTE)
  ..  *    Per le tariffe a PU o PUR i titoli considerati sono
  ..  *    con data effetto ompresa tra la decorrenza di polizza
  ..  *    e data effetto rivalutazione
  ..           PERFORM S10215-DET-DATE-PUR
  ..              THRU S10215-DET-DATE-PUR-EX
  ..       ELSE
  ..  *    Sia per le tariffe a premio annuo costante
  ..  *    Sia per le tariffe a premio annuo rivalutabile
  ..  *    sono selezionati tutti i titoli con effetto 3 mesi prima
  ..  *    della data rivalutazione e 3 mesi dopo la data ultima ri
  ..  *    valutazione
               PERFORM S10209-DET-DATA-INI-CALC
                  THRU S10209-EX
  ..           PERFORM S10211-DET-DATA-DA
  ..              THRU S10211-DET-DATA-DA-EX
  ..           PERFORM S10210-DET-DATA-A
  ..              THRU S10210-DET-DATA-A-EX
21901      END-IF.
      *
           PERFORM S10240-IMPOSTA-TIT-CONT
              THRU S10240-IMPOSTA-TIT-CONT-EX
      *
           PERFORM S10250-LEGGI-TIT-CONT
              THRU S10250-LEGGI-TIT-CONT-EX
             UNTIL WK-FINE-FETCH-SI
                OR IDSV0001-ESITO-KO

           IF IDSV0001-ESITO-OK
              IF W870-TIT-NUM-MAX-ELE(IX-TGA-W870) = 0
               MOVE ZEROES TO W870-ID-TRCH-DI-GAR(IX-TGA-W870)
               COMPUTE IX-TGA-W870   = IX-TGA-W870 - 1
               COMPUTE W870-TGA-NUM-MAX-ELE =
                       W870-TGA-NUM-MAX-ELE - 1
              END-IF
           END-IF.
      *
       S10230-RICERCA-TITOLO-EX.
             EXIT.
      *
VSTEF *----------------------------------------------------------------*
  ..  *    RICERCA GARANZIA
  ..  *----------------------------------------------------------------*
  ..   RICERCA-GAR.

  ..       SET WK-GRZ-NO       TO TRUE

  ..       PERFORM VARYING IX-GRZ FROM 1 BY 1
  ..                 UNTIL IX-GRZ > WGRZ-ELE-GAR-MAX
  ..                    OR WK-GRZ-SI

  ..          IF WGRZ-ID-GAR(IX-GRZ) = WTGA-ID-GAR(IX-TAB-TGA)

  ..             SET WK-GRZ-SI               TO TRUE

  ..             MOVE WGRZ-TP-PER-PRE(IX-GRZ)
  ..               TO WS-TP-PER-PREMIO

  ..          END-IF

  ..       END-PERFORM.

  ..       SUBTRACT 1 FROM IX-GRZ.

  ..   RICERCA-GAR-EX.
VSTEF        EXIT.
      *
      *
21901 *----------------------------------------------------------------*
  ..  * RICERCA PARAMETRO MOVIMENTO LEGATA ALLA GARANZIA DELLA TRANCHE
  ..  *----------------------------------------------------------------*
  ..   RICERCA-PMO.

  ..       SET WK-PMO-NO       TO TRUE

  ..       PERFORM VARYING IX-PMO FROM 1 BY 1
  ..                 UNTIL IX-PMO > WPMO-ELE-PARAM-MOV-MAX
  ..                    OR WK-PMO-SI

  ..          IF WPMO-ID-OGG(IX-PMO) = WTGA-ID-GAR(IX-TAB-TGA)
  ..         AND WPMO-TP-OGG(IX-PMO) = 'GA'
  ..             SET WK-PMO-SI TO TRUE
  ..             MOVE IX-PMO   TO INDPMO
  ..          END-IF

  ..       END-PERFORM.
  ..       SUBTRACT 1 FROM IX-PMO.

  ..   RICERCA-PMO-EX.
21901        EXIT.
      *
      *----------------------------------------------------------------*
      *     VALORIZZAZIONI PER LA LETTURA CON WHERE CONDITION AD HOC   *
      *----------------------------------------------------------------*
      *
       S10240-IMPOSTA-TIT-CONT.
      *
           MOVE 'S10240-IMPOSTA-TIT-CONT'
             TO WK-LABEL-ERR.
      *
      * --> Vengono Cercati I Titoli Con Effetto Compreso Tra L'Ultima
      * --> Ricorrenza Elaborata e L'Effetto di Elaborazione
      *
           MOVE WK-DT-DA
             TO LDBV7141-DT-DA.
           MOVE WK-DT-A
             TO LDBV7141-DT-A.
      *
           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
             TO LDBV7141-ID-OGG.
           MOVE 'TG'
             TO LDBV7141-TP-OGG.
           MOVE 'PR'
             TO LDBV7141-TP-TIT.
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE WS-DT-INFINITO-1-N
             TO IDSI0011-DATA-INIZIO-EFFETTO
22200      MOVE WS-TS-INFINITO-1-N
             TO IDSI0011-DATA-COMPETENZA
           MOVE ZERO
             TO IDSI0011-DATA-FINE-EFFETTO
      *
      *  --> Nome tabella fisica db
      *
           MOVE 'LDBS7140'
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE SPACES
             TO IDSI0011-BUFFER-DATI.
           MOVE LDBV7141
             TO IDSI0011-BUFFER-WHERE-COND.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-WHERE-CONDITION
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
22200 *    SET IDSI0011-TRATT-DEFAULT
22200      SET IDSI0011-TRATT-X-COMPETENZA
  =          TO TRUE.
      *
      *  --> Inizializzazione Return Code
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S10240-IMPOSTA-TIT-CONT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *            LETTURA DELLE TABELLA TRANCHE DI GARANZIA           *
      *----------------------------------------------------------------*
      *
       S10250-LEGGI-TIT-CONT.
      *
           MOVE 'S10250-LEGGI-TIT-CONT'
             TO WK-LABEL-ERR.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata, fine occorrenze fetch
      *
                       SET WK-FINE-FETCH-SI
                         TO TRUE
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
      *  --> Operazione eseguita correttamente
      *
                        SET WK-TIT-SI
                          TO TRUE
      *
                       SET IDSI0011-FETCH-NEXT
                         TO TRUE
      *
                       MOVE W870-TIT-NUM-MAX-ELE(IX-TGA-W870)
                         TO IX-TIT-W870
      *
                       ADD 1
                         TO IX-TIT-W870
      *
                       MOVE IX-TIT-W870
                         TO W870-TIT-NUM-MAX-ELE(IX-TGA-W870)
      *
                       IF IX-TIT-W870 > WMAX-TAB-TIT
      *
                          MOVE 'OVERFLOW CARICAMENTO TITOLI CONTABILI'
                            TO WK-STRING
                          MOVE '005059'
                            TO WK-COD-ERR
                          PERFORM GESTIONE-ERR-STD
                             THRU GESTIONE-ERR-STD-EX
      *
                       ELSE
      *
                          MOVE IDSO0011-BUFFER-DATI
                            TO TIT-CONT
      *
                          PERFORM S10260-CARICA-TAB-W870
                             THRU S10260-CARICA-TAB-W870-EX

                          IF  W870-TP-STAT-TIT
                             (IX-TGA-W870 , IX-TIT-W870) = 'IN'
                             MOVE 'S' TO WS-FLAG-INC-TGA(IX-TAB-FLAGS)
                          END-IF
      *
                          IF WS-PREMIO-ANNUALE
                             PERFORM S10500-CTRL-TIT
                                THRU S10500-CTRL-TIT-EX
                          END-IF
      *
                       END-IF
      *
                  WHEN OTHER
      *
      *  --> Errore di accesso al db
                       MOVE 'ERRORE LETTURA TITOLO CONTABILE'
                         TO WK-STRING
                       MOVE '005016'
                         TO WK-COD-ERR
                       PERFORM GESTIONE-ERR-STD
                          THRU GESTIONE-ERR-STD-EX
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore dispatcher
      *
              MOVE 'ERRORE DISPATCHER LETTURA TITOLO CONTABILE'
                TO WK-STRING
              MOVE '005016'
                TO WK-COD-ERR
              PERFORM GESTIONE-ERR-STD
                 THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S10250-LEGGI-TIT-CONT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  CARICAMENTO TABELLA WS
      *----------------------------------------------------------------*
      *
       S10260-CARICA-TAB-W870.
      *
           MOVE 'S10260-CARICA-TAB-W870'
             TO WK-LABEL-ERR.
      *
           MOVE TIT-ID-TIT-CONT
             TO W870-ID-TIT-CONT(IX-TGA-W870 , IX-TIT-W870).
      *
           IF TIT-DT-VLT-NULL = HIGH-VALUE OR LOW-VALUE
      *
              MOVE TIT-DT-VLT-NULL
                TO W870-DT-VLT-NULL(IX-TGA-W870 , IX-TIT-W870)
      *
           ELSE
      *
              MOVE TIT-DT-VLT
                TO W870-DT-VLT(IX-TGA-W870 , IX-TIT-W870)
      *
           END-IF.
      *
           MOVE TIT-DT-INI-COP
             TO W870-DT-INI-EFF(IX-TGA-W870 , IX-TIT-W870).
      *
           MOVE TIT-TP-STAT-TIT
             TO W870-TP-STAT-TIT(IX-TGA-W870 , IX-TIT-W870).
      *
       S10260-CARICA-TAB-W870-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CONTROLLO CHE PER OGNI TRANCHE VI SIA UN SOLO TITOLO CONTABILE
      *----------------------------------------------------------------*
      *
       S10500-CTRL-TIT.
      *
           MOVE 'S10500-CTRL-TIT'
             TO WK-LABEL-ERR.
      *
      * --> Viene gestito un errore bloccante se esistono
      * --> Titoli Cotabili non incassati per una Tranche
      * --> poichh essendo la rivalutazione per incasso
      * --> il processo dovr` essere eseguito non appena
      * --> arriva l'incasso o il titolo in emesso
      * --> viene stornato
      *
           IF (W870-TP-STAT-TIT
              (IX-TGA-W870 , IX-TIT-W870) = 'EM')
            OR
              (W870-DT-VLT-NULL
              (IX-TGA-W870 , IX-TIT-W870) = HIGH-VALUES
            OR
               W870-DT-VLT
              (IX-TGA-W870 , IX-TIT-W870) = ZEROES)

               MOVE 'TIT.CONT. NON INC., RIESEGUIRE RIVAL'
                 TO WK-STRING
               MOVE '001114'
                 TO WK-COD-ERR
               PERFORM GESTIONE-ERR-STD
                  THRU GESTIONE-ERR-STD-EX
      *
           END-IF.
      *
       S10500-CTRL-TIT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * GESTIONE DEL TIPO DI RIVALUTAZIONE
      *----------------------------------------------------------------*
      *
       S10600-DETERMINA-TP-RIVAL.
      *
           MOVE 'S10600-DETERMINA-TP-RIVAL'
             TO WK-LABEL-ERR.

      * --> Tipi Rivalutazione :
      *
      *     -> Piena
      *     -> Parziale
      *     -> Da Rieseguire
      *
           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
                OR IDSV0001-ESITO-KO
      *
               MOVE 'CO'          TO WTGA-TP-RIVAL(IX-TAB-TGA)
               MOVE IX-TAB-TGA
                 TO IX-TAB-FLAGS
               MOVE WS-FLAG-INC-TGA(IX-TAB-FLAGS)
                 TO WK-TGA-INC
21901 *
                  PERFORM VARYING IX-TGA-W870 FROM 1 BY 1
                     UNTIL IX-TGA-W870 > W870-TGA-NUM-MAX-ELE
      *
                       IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
                          W870-ID-TRCH-DI-GAR(IX-TGA-W870)
      *
                          PERFORM S10610-ALLINEA-AREA-TIT
                             THRU S10610-ALLINEA-AREA-TIT-EX
      *
                       END-IF
      *
                  END-PERFORM
           END-PERFORM.
      *
       S10600-DETERMINA-TP-RIVAL-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VIENE ALLINEATA L'AREA DEL TITOLO
      *----------------------------------------------------------------*
      *
       S10610-ALLINEA-AREA-TIT.
      *
           MOVE 'S10610-ALLINEA-AREA-TIT'
             TO WK-LABEL-ERR.
      *
           PERFORM VARYING IX-TIT-W870 FROM 1 BY 1
             UNTIL IX-TIT-W870 > W870-TIT-NUM-MAX-ELE(IX-TGA-W870)
      *
               PERFORM S10620-RIV-PIENA-PARZ-NULLA
                  THRU S10620-RIV-PIENA-PARZ-NULLA-EX
      *
           END-PERFORM.
      *
       S10610-ALLINEA-AREA-TIT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *          DETERMINA IL TIPO DI RIVALUTAZIONE DA ESEGUIRE
      *  PER I TITOLI INCASSATI : PIENA , PARZIALE , NULLL
      *----------------------------------------------------------------*
      *
       S10620-RIV-PIENA-PARZ-NULLA.
      *
           MOVE 'S10620-RIV-PIENA-PARZ-NULLA'
             TO WK-LABEL-ERR.


           IF TIT-NON-INCASSATO
              MOVE 'NU' TO WTGA-TP-RIVAL(IX-TAB-TGA)
           ELSE
              PERFORM S10630-LEGGE-GG-RIT-PAG
                 THRU S10630-EX
              IF DTC-NUM-GG-RITARDO-PAG-NULL = HIGH-VALUES
                 MOVE ZERO TO DTC-NUM-GG-RITARDO-PAG
              END-IF
              IF  IDSV0001-ESITO-OK
              AND DETTAGLIO-TROVATO
              AND DTC-NUM-GG-RITARDO-PAG > ZERO
                  MOVE 'PA' TO WTGA-TP-RIVAL(IX-TAB-TGA)
              END-IF
           END-IF.
      *
       S10620-RIV-PIENA-PARZ-NULLA-EX.
           EXIT.
      *
      *
       S10630-LEGGE-GG-RIT-PAG.
      *
           MOVE 'S10630-LEGGE-GG-RIT-PAG'
             TO WK-LABEL-ERR.

           SET IDSI0011-FETCH-FIRST       TO TRUE.

           SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.

           SET INIT-CUR-DTC               TO TRUE.

           SET IDSI0011-TRATT-DEFAULT  TO TRUE.

           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) TO DTC-ID-OGG.

           SET TRANCHE                 TO TRUE.
           MOVE WS-TP-OGG              TO DTC-TP-OGG.

           SET IDSI0011-ID-OGGETTO     TO TRUE.

           MOVE 'DETT-TIT-CONT'        TO IDSI0011-CODICE-STR-DATO.
           MOVE DETT-TIT-CONT          TO IDSI0011-BUFFER-DATI.

           MOVE ZERO TO IDSI0011-DATA-INIZIO-EFFETTO
                        IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO TO IDSI0011-DATA-COMPETENZA.

           SET DETTAGLIO-NON-TROVATO TO TRUE.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR IDSV0001-ESITO-KO
                      OR FINE-CUR-DTC
                      OR DETTAGLIO-TROVATO

                   PERFORM CALL-DISPATCHER
                      THRU CALL-DISPATCHER-EX

                   IF IDSV0001-ESITO-OK
                      IF IDSO0011-SUCCESSFUL-RC
                         EVALUATE TRUE
                                  WHEN IDSO0011-NOT-FOUND
      *-->    NESSUN DATO ESTRATTO DALLA TABELLA
                                  IF IDSI0011-FETCH-FIRST
                                     MOVE 'IDBSDTC0'
                                       TO IEAI9901-COD-SERVIZIO-BE
                                     MOVE 'S10630-LEGGE-GG-RIT-PAG'
                                       TO IEAI9901-LABEL-ERR
                                     MOVE '005166'
                                       TO IEAI9901-COD-ERRORE
                                     MOVE SPACES
                                       TO IEAI9901-PARAMETRI-ERR
                                     STRING IDSI0011-CODICE-STR-DATO ';'
                                            IDSO0011-RETURN-CODE     ';'
                                            IDSO0011-SQLCODE
                                            DELIMITED BY SIZE
                                            INTO IEAI9901-PARAMETRI-ERR
                                     END-STRING
                                     PERFORM
                                            S0300-RICERCA-GRAVITA-ERRORE
                                       THRU EX-S0300
                                  END-IF

                         WHEN IDSO0011-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                             MOVE IDSO0011-BUFFER-DATI TO DETT-TIT-CONT
                             IF DTC-ID-TIT-CONT =
                              W870-ID-TIT-CONT(IX-TGA-W870, IX-TIT-W870)
                                SET DETTAGLIO-TROVATO TO TRUE
                             END-IF

                         WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                             MOVE 'IDBSDTC0'
                               TO IEAI9901-COD-SERVIZIO-BE
                             MOVE 'S10630-LEGGE-GG-RIT-PAG'
                               TO IEAI9901-LABEL-ERR
                             MOVE '005166'
                               TO IEAI9901-COD-ERRORE
                             MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                             STRING IDSI0011-CODICE-STR-DATO ';'
                                    IDSO0011-RETURN-CODE     ';'
                                    IDSO0011-SQLCODE
                                    DELIMITED BY SIZE
                                    INTO IEAI9901-PARAMETRI-ERR
                             END-STRING
                             PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                THRU EX-S0300
                         END-EVALUATE
                      ELSE
      *--> GESTIRE ERRORE
                           MOVE 'IDBSDTC0'
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'S10630-LEGGE-GG-RIT-PAG'
                             TO IEAI9901-LABEL-ERR
                           MOVE '005166'
                             TO IEAI9901-COD-ERRORE
                           MOVE SPACES
                             TO IEAI9901-PARAMETRI-ERR
                           STRING IDSI0011-CODICE-STR-DATO ';'
                                  IDSO0011-RETURN-CODE     ';'
                                  IDSO0011-SQLCODE
                                  DELIMITED BY SIZE
                                  INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                      END-IF
                   ELSE
      *--> GESTIRE ERRORE
                       MOVE 'IDBSDTC0'
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S10630-LEGGE-GG-RIT-PAG'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005166'
                         TO IEAI9901-COD-ERRORE
                       MOVE SPACES
                         TO IEAI9901-PARAMETRI-ERR
                       STRING IDSI0011-CODICE-STR-DATO ';'
                              IDSO0011-RETURN-CODE     ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
                   END-IF
           END-PERFORM.
      *
       S10630-EX.
           EXIT.

      * ============================================================== *
      * -->           O P E R Z I O N I   F I N A L I              <-- *
      * ============================================================== *
       S90000-OPERAZ-FINALI.
      *
           MOVE 'S90000-OPERAZ-FINALI'
             TO WK-LABEL-ERR.
      *
       S90000-OPERAZ-FINALI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *                GESTIONE STANDARD DELL'ERRORE                   *
      *----------------------------------------------------------------*
      *
       GESTIONE-ERR-STD.
      *
           MOVE WK-PGM
             TO IEAI9901-COD-SERVIZIO-BE.
           MOVE WK-LABEL-ERR
             TO IEAI9901-LABEL-ERR.
           MOVE WK-COD-ERR
             TO IEAI9901-COD-ERRORE.
           STRING WK-STRING ';'
                  IDSO0011-RETURN-CODE ';'
                  IDSO0011-SQLCODE
                  DELIMITED BY SIZE
                  INTO IEAI9901-PARAMETRI-ERR
           END-STRING.
      *
           PERFORM S0300-RICERCA-GRAVITA-ERRORE
              THRU EX-S0300.
      *
       GESTIONE-ERR-STD-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *           GESTIONE STANDARD DELL'ERRORE DI SISTEMA             *
      *----------------------------------------------------------------*
      *
       GESTIONE-ERR-SIST.
      *
           MOVE WK-LABEL-ERR
             TO IEAI9901-LABEL-ERR
      *
           PERFORM S0290-ERRORE-DI-SISTEMA
              THRU EX-S0290.
      *
       GESTIONE-ERR-SIST-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *     ROUTINES CALL DISPATCHER                                   *
      *----------------------------------------------------------------*
      *
           COPY IDSP0012.
      *
      *----------------------------------------------------------------*
      *  ROUTINES GESTIONE ERRORI                                      *
      *----------------------------------------------------------------*
      *
           COPY IERP9901.
           COPY IERP9902.
           COPY IERP9903.
      *


      *  ============================================================= *
      *                                                                *
      *        PORTAFOGLIO VITA ITALIA  VER 1.0                        *
      *                                                                *
      *  ============================================================= *
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LOAS0820.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2009.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *     PROGRAMMA ..... LOAS0820                                   *
      *     TIPOLOGIA...... OPERAZIONI AUTOMATICHE                     *
      *     PROCESSO....... MANAGEMENT FEE                             *
      *     FUNZIONE....... BATCH                                      *
      *     DESCRIZIONE.... SERVIZIO SCRITTURA FLUSSO OUTPUT           *
      *     PAGINA WEB..... N.A.                                       *
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
      *
           SELECT OUMANFEE   ASSIGN    TO OUMANFEE
           FILE STATUS  IS FS-OUT.
      *
       DATA DIVISION.
       FILE SECTION.
      *
       FD  OUMANFEE
           LABEL RECORD STANDARD
           BLOCK  0  RECORDS.
       01  OUMANFEE-REC                PIC X(400).
      *
       WORKING-STORAGE SECTION.
      *
      *----------------------------------------------------------------*
      * COPY DISPATCHER
      *----------------------------------------------------------------*
      *
       01  IDSV0012.
           COPY IDSV0012.
      *
       01  DISPATCHER-VARIABLES.
      *  COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *  COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *
       COPY IDSV0003.
      *
      *----------------------------------------------------------------*
      *     COPY DELLE TABELLE DB2 PER IL DISPATCHER DATI
      *----------------------------------------------------------------*
      *
      *  --> RAPPORTO RETE
           COPY IDBVRRE1.
      *  --> RAPPORTO MOVI
           COPY IDBVMOV1.
      *  --> RAPPORTO TRANCHE DI LIQUIDAZIONE
           COPY IDBVTLI1.
      *  --> WHERE CONDITION MOVI
           COPY LDBV3611.
      *  --> WHERE CONDITION TRANCHE DI LIQUIDAZIONE
           COPY LDBV3621.
      *
      *----------------------------------------------------------------*
      * COSTANTI
      *----------------------------------------------------------------*
      *
       77 WK-PGM                           PIC X(08) VALUE 'LOAS0820'.
       01 LDBS4240                         PIC X(08) VALUE 'LDBS4240'.
       01 LDBS3610                         PIC X(08) VALUE 'LDBS3610'.
       01 LDBS3620                         PIC X(08) VALUE 'LDBS3620'.
      *
      *----------------------------------------------------------------*
      * AREA FILE
      *----------------------------------------------------------------*
      *
      *  -- Area Tracciato File
      *
       01 W820-AREA-FILE.
          COPY LOAR0820                  REPLACING ==(SF)== BY ==W820==.
      *
      *  -- File Status
      *
       77 FS-OUT                         PIC X(002)  VALUE '00'.
      *
      * --> Falg di stato apertura file di output
      *
       01 WK-OUTRIVA                     PIC X(01).
          88 WK-OPEN                       VALUE 'S'.
          88 WK-CLOSE                      VALUE 'N'.
      *
      *----------------------------------------------------------------*
      * COPY PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
      *
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IEAV9903.
           COPY IDSV0002.
      *
      *----------------------------------------------------------------*
      * VARIBILI E CAMPI DI COMODO
      *----------------------------------------------------------------*
      *
       01 WK-DT-ELAB                    PIC 9(08).
       01 WK-PRE-NET                    PIC S9(12)V9(03).
       01 WK-RIS-MAT                    PIC S9(12)V9(03).
       01 WK-PRSTZ-ULT                  PIC S9(12)V9(03).
       01 WK-INCR-PRSTZ                 PIC S9(12)V9(03).
       01 WK-IMP-LRD-EFFLQ              PIC S9(12)V9(03).
      *
      * --> Area di appoggio per i messaggi di errore
      *
       01 WK-GESTIONE-MSG-ERR.
          03 WK-COD-ERR                 PIC X(06) VALUE ZEROES.
          03 WK-LABEL-ERR               PIC X(30) VALUE SPACES.
          03 WK-STRING                  PIC X(85) VALUE SPACES.
      *
      * --> Formattazione Date
      *
       01 WK-APPO-DT-NO-SEGNO           PIC 9(08).
      *
       01 WK-APPO-DT-N.
          03 WK-DT-N-AAAA               PIC 9(04).
          03 WK-DT-N-MM                 PIC 9(02).
          03 WK-DT-N-GG                 PIC 9(02).
      *
       01 WK-APPO-DT-X.
          03 WK-DT-X-GG                 PIC 9(02).
          03 FILLER                     PIC X(01) VALUE '/'.
          03 WK-DT-X-MM                 PIC 9(02).
          03 FILLER                     PIC X(01) VALUE '/'.
          03 WK-DT-X-AAAA               PIC 9(04).
      *
       01 WK-APPO-DT-7.
          03 FILLER                     PIC X(03).
          03 WK-DT-7                    PIC X(07).
      *
       01 WK-FINE-MOV                 PIC X(01).
          88 WK-FINE-MOV-SI             VALUE 'Y'.
          88 WK-FINE-MOV-NO             VALUE 'N'.
      *
       01 WK-FINE-TLI                 PIC X(01).
          88 WK-FINE-TLI-SI             VALUE 'Y'.
          88 WK-FINE-TLI-NO             VALUE 'N'.
      *
       01 WK-VERS-AGG                   PIC X(01).
          88 WK-VERS-AGG-SI               VALUE 'S'.
          88 WK-VERS-AGG-NO               VALUE 'N'.
      *
       01 WK-RISC-PARZ                  PIC X(01).
          88 WK-RISC-PARZ-SI              VALUE 'S'.
          88 WK-RISC-PARZ-NO              VALUE 'N'.
      *
22702 *----------------------------------------------------------------*
22702 *   VETTORE PER LA MEMORIZZAZIONE DEI MOVIMENTI DI COMUNICAZIONE
22702 *----------------------------------------------------------------*
22702 *
22702  01 WK-MOV-VETTORE.
22702     05 WK-MOV-ELE OCCURS 100.
22702        07 WK-TP-MOVI           PIC X(005).
22702        07 WK-DT-EFF            PIC S9(8)V COMP-3.

22702  77 WK-IND-MOV                 PIC S9(04) COMP.
22702  77 WK-IND-MOV-MAX             PIC S9(04) COMP.

22702  01 WK-MOV                     PIC X(02).
22702     88 WK-MOV-TROVATO          VALUE 'S'.
22702     88 WK-MOV-NON-TROVATO      VALUE 'N'.
      *
      *----------------------------------------------------------------*
      *   INDICI
      *----------------------------------------------------------------*
      *
       01 IX-INDICI.
      *
          03 IX-TAB-PMO                 PIC S9(04) COMP.
          03 IX-TAB-GRZ                 PIC S9(04) COMP.
          03 IX-TAB-TGA                 PIC S9(04) COMP.
      *
      *----------------------------------------------------------------*
      *  AREE TIPOLOGICHE DI PORTAFOGLIO
      *----------------------------------------------------------------*
      *
          COPY LCCC0006.
          COPY LCCVXRE0.
          COPY LCCVXOG0.
          COPY LCCVXMV0.
      *
      *----------------------------------------------------------------*
      *        L I N K A G E   S E C T I O N
      *----------------------------------------------------------------*
      *
       LINKAGE SECTION.
      *
      * -- Area Contestuale
       01  AREA-IDSV0001.
           COPY IDSV0001.
      *
      * -- Area parametro movimento
       01 WPMO-AREA-PARAM-MOVI.
          04 WPMO-ELE-PARAM-MOV-MAX      PIC S9(04) COMP.
          COPY LCCVPMOA REPLACING   ==(SF)==  BY ==WPMO==.
          COPY LCCVPMO1                  REPLACING ==(SF)== BY ==WPMO==.
      *
      * -- Area polizza
       01 WPOL-AREA-POLIZZA.
          04 WPOL-ELE-POLI-MAX           PIC S9(04) COMP.
          04 WPOL-TAB-POL.
          COPY LCCVPOL1                  REPLACING ==(SF)== BY ==WPOL==.
      *
      * -- Area Adesione
       01 WADE-AREA-ADESIONE.
          04 WADE-ELE-ADES-MAX           PIC S9(04) COMP.
          04 WADE-TAB-ADE                OCCURS 1.
          COPY LCCVADE1                  REPLACING ==(SF)== BY ==WADE==.
      *
      * -- Area Garanzia
       01 WGRZ-AREA-GARANZIA.
          04 WGRZ-ELE-GAR-MAX           PIC S9(04) COMP.
          COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
          COPY LCCVGRZ1                 REPLACING ==(SF)== BY ==WGRZ==.
      *
      * -- Area Tranche Di Garanzia
       01 WTGA-AREA-TRANCHE.
          04 WTGA-ELE-TRAN-MAX          PIC S9(04) COMP.
            COPY LCCVTGAC REPLACING   ==(SF)==  BY ==WTGA==.
            COPY LCCVTGA1               REPLACING ==(SF)== BY ==WTGA==.
      *
      *  -- Area Infrastrutturale
       01 WCOM-IO-STATI.
           04 WCOM-AREA-STATI.
              COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.
           04 WCOM-AREA-LCCC0261.
              COPY LCCC0261              REPLACING ==(SF)== BY ==WCOM==.

       01  WAPPL-NUM-ELAB.
           04  WAPPL-ELAB-INIZIATA       PIC X(1).
               88   WAPPL-INIZIO         VALUE '1'.

      *
      *  ====================  M A I N    L I N E  =================== *
      *
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-IO-STATI
                                WPMO-AREA-PARAM-MOVI
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TRANCHE
                                WAPPL-NUM-ELAB.
      *
           PERFORM S00000-OPERAZ-INIZIALI
              THRU S00000-OPERAZ-INIZIALI-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S10000-ELABORAZIONE
                 THRU S10000-ELABORAZIONE-EX
      *
           END-IF.
      *
           PERFORM S90000-OPERAZ-FINALI
              THRU S90000-OPERAZ-FINALI-EX.
      *
           GOBACK.
      *
      * ============================================================== *
      * -->           O P E R Z I O N I   I N I Z I A L I          <-- *
      * ============================================================== *
      *
       S00000-OPERAZ-INIZIALI.
      *
           MOVE 'S00000-OPERAZ-INIZIALI'
             TO WK-LABEL-ERR.
      *
22702      INITIALIZE WK-MOV-VETTORE
22702                 WK-IND-MOV
22702                 WK-IND-MOV-MAX
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           PERFORM S00100-OPEN-OUT
              THRU S00100-OPEN-OUT-EX.
      *
           PERFORM S00200-CTRL-INPUT
              THRU S00200-CTRL-INPUT-EX.
      *
           ACCEPT WK-DT-ELAB
             FROM DATE YYYYMMDD.
      *
       S00000-OPERAZ-INIZIALI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * APERTURA DEL FILE DI OUTPUT OUMANFEE
      *----------------------------------------------------------------*
      *
       S00100-OPEN-OUT.
      *
           MOVE 'S00100-OPEN-OUT'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           OPEN OUTPUT OUMANFEE.
      *
           IF FS-OUT NOT = '00'
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'ERRORE APERTURA FILE DI OUTPUT'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           ELSE
      *
              SET WK-OPEN                 TO TRUE
      *
           END-IF.
      *
       S00100-OPEN-OUT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CONTROLLO DATI DI INPUT
      *----------------------------------------------------------------*
      *
       S00200-CTRL-INPUT.
      *
           MOVE 'S00200-CTRL-INPUT'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           MOVE IDSV0001-TIPO-MOVIMENTO
             TO WS-MOVIMENTO.
      *
           IF IDSV0001-TIPO-MOVIMENTO NOT = 6006 AND
                                      NOT = 6024 AND
                                      NOT = 6025 AND
                                      NOT = 6026 AND
                                      NOT = 1011
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'TIPO MOVIMENTO ERRATO'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           IF WPMO-ELE-PARAM-MOV-MAX = 0
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'NESSUNA OCCORRENZA DI PARAM. MOV. ELABORABILE'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           IF WPOL-ELE-POLI-MAX = 0
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'OCCORRENZA DI POLIZZA NON VALORIZZATA'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           IF WADE-ELE-ADES-MAX = 0
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'OCCORRENZA DI ADESIONE NON VALORIZZATA'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           IF WGRZ-ELE-GAR-MAX = 0
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'NESSUNA OCCORRENZA DI GARANZIA ELABORABILE'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           IF WTGA-ELE-TRAN-MAX = 0
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'NESSUNA OCCORRENZA DI TRANCHE ELABORABILE'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
       S00200-CTRL-INPUT-EX.
           EXIT.
      *
      * ============================================================== *
      * -->               E L A B O R A Z I O N E                  <-- *
      * ============================================================== *
      *
       S10000-ELABORAZIONE.
      *
           MOVE 'S10000-ELABORAZIONE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           IF WAPPL-INIZIO
               PERFORM S11000-RECORD-TESTATA
                  THRU S11000-RECORD-TESTATA-EX
               MOVE '0' TO        WAPPL-ELAB-INIZIATA
           END-IF

      *
           PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
             UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
      *
               PERFORM S12000-RECORD-DATI
                  THRU S12000-RECORD-DATI-EX
      *
           END-PERFORM.
      *
       S10000-ELABORAZIONE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * SCRITTURA DEL RECORD DI TESTATA
      *----------------------------------------------------------------*
      *
       S11000-RECORD-TESTATA.
      *
           MOVE 'S11000-RECORD-TESTATA'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           PERFORM S11999-WRITE-REC-T
              THRU S11999-WRITE-REC-T-EX.
      *
       S11000-RECORD-TESTATA-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * OPERAZIONE DI WRITE (TESTATA)
      *----------------------------------------------------------------*
      *
       S11999-WRITE-REC-T.
      *
           MOVE 'S11999-WRITE-REC-T'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           WRITE OUMANFEE-REC FROM W820-AREA-TESTATA.
      *
           IF FS-OUT NOT = '00'
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'ERRORE SCRITTURA FILE DI OUTPUT'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
       S11999-WRITE-REC-T-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * SCRITTURA DI UN RECORD PER OGNI GARANZIA ELABORATA
      *----------------------------------------------------------------*
      *
       S12000-RECORD-DATI.
      *
           MOVE 'S12000-RECORD-DATI'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           INITIALIZE W820-AREA-DATI.
      *
           MOVE WK-DT-ELAB
             TO WK-APPO-DT-NO-SEGNO.
           MOVE WK-APPO-DT-NO-SEGNO
             TO WK-APPO-DT-N.
           PERFORM S99999-CONV-N-TO-X
              THRU S99999-CONV-N-TO-X-EX
           MOVE WK-APPO-DT-X
             TO W820-DT-ELAB.
      *
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO W820-COD-ANIA.
      *
           IF WPOL-COD-RAMO-NULL NOT = HIGH-VALUES
      *
              MOVE WPOL-COD-RAMO
                TO W820-RAMO-GEST
      *
           END-IF.
      *
           IF WPOL-IB-OGG-NULL NOT = HIGH-VALUES
      *
              MOVE WPOL-IB-OGG(4:9)
                TO W820-NUM-POLI
      *
           END-IF.
      *
           MOVE WGRZ-COD-TARI(IX-TAB-GRZ)
             TO W820-COD-TARI.
      *
           MOVE WPOL-DT-EMIS
             TO WK-APPO-DT-NO-SEGNO.
           MOVE WK-APPO-DT-NO-SEGNO
             TO WK-APPO-DT-N.
           PERFORM S99999-CONV-N-TO-X
              THRU S99999-CONV-N-TO-X-EX
           MOVE WK-APPO-DT-X
             TO W820-DT-EMIS.
      *
           MOVE WPOL-DT-DECOR
             TO WK-APPO-DT-NO-SEGNO.
           MOVE WK-APPO-DT-NO-SEGNO
             TO WK-APPO-DT-N.
           PERFORM S99999-CONV-N-TO-X
              THRU S99999-CONV-N-TO-X-EX
           MOVE WK-APPO-DT-X
             TO W820-DT-EFF.
      *
           MOVE IDSV0001-DATA-EFFETTO
             TO WK-APPO-DT-NO-SEGNO.
           MOVE WK-APPO-DT-NO-SEGNO
             TO WK-APPO-DT-N.
           PERFORM S99999-CONV-N-TO-X
              THRU S99999-CONV-N-TO-X-EX
           MOVE WK-APPO-DT-X
             TO WK-APPO-DT-7
           MOVE WK-DT-7
             TO W820-DT-RICOR.
      *
           EVALUATE IDSV0001-TIPO-MOVIMENTO
      *
               WHEN 1011
                  MOVE 'ANTICIPATO'
                    TO W820-TP-ELAB
                  SET CREAZ-INDIVI TO TRUE
      *
               WHEN 6006
               WHEN 6024
                  MOVE 'A RICORRENZA'
                    TO W820-TP-ELAB
      *
               WHEN 6025
               WHEN 6026
                  MOVE 'CONGUAGLIO'
                    TO W820-TP-ELAB
      *
           END-EVALUATE.
      *
           MOVE ZEROES
             TO W820-PC-REMUNER.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S12100-DATI-PARAM-MOVI
                 THRU S12100-DATI-PARAM-MOVI-EX
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S12200-DATI-TRANCHE
                 THRU S12200-DATI-TRANCHE-EX
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S12300-DATI-RAPP-RETE
                 THRU S12300-DATI-RAPP-RETE-EX
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S12400-DATI-MOVI
                 THRU S12400-DATI-MOVI-EX
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM S12999-WRITE-REC-D
                 THRU S12999-WRITE-REC-D-EX
      *
           END-IF.
      *
       S12000-RECORD-DATI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * DATI PROVENIENTI DALLA TABELLA PARAMETRO MOVIMENTO
      *----------------------------------------------------------------*
      *
       S12100-DATI-PARAM-MOVI.
      *
           MOVE 'S12100-DATI-PARAM-MOVI'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
             UNTIL IX-TAB-PMO > WPMO-ELE-PARAM-MOV-MAX
      *
               IF WGRZ-ID-GAR(IX-TAB-GRZ) =
                  WPMO-ID-OGG(IX-TAB-PMO)
      *
                  IF ((WPMO-ST-INV(IX-TAB-PMO)           AND
      *               IDSV0001-TIPO-MOVIMENTO = 1011)   OR
                      CREAZ-INDIVI)                     OR
                      WPMO-ST-ADD(IX-TAB-PMO))
      *
                     IF WPMO-DT-RICOR-SUCC-NULL(IX-TAB-PMO)
                        NOT = HIGH-VALUES
      *
                        MOVE WPMO-DT-RICOR-SUCC(IX-TAB-PMO)
                          TO WK-APPO-DT-NO-SEGNO
                        MOVE WK-APPO-DT-NO-SEGNO
                          TO WK-APPO-DT-N
                        PERFORM S99999-CONV-N-TO-X
                           THRU S99999-CONV-N-TO-X-EX
                        MOVE WK-APPO-DT-X
                          TO W820-DT-PAG
      *
                     END-IF
      *
                     IF WPMO-IMP-RAT-MANFEE-NULL(IX-TAB-PMO)
                        NOT = HIGH-VALUES
      *
                        MOVE WPMO-IMP-RAT-MANFEE(IX-TAB-PMO)
                          TO W820-IMP-REMUNER
      *
                     END-IF
      *
                  END-IF
      *
               END-IF
      *
           END-PERFORM.
      *
       S12100-DATI-PARAM-MOVI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * DATI PROVENIENTI DALLA TABELLA TRANCHE DI GARANZIA
      *----------------------------------------------------------------*
      *
       S12200-DATI-TRANCHE.
      *
           MOVE 'S12200-DATI-TRANCHE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           MOVE ZEROES
             TO WK-PRE-NET
                WK-RIS-MAT
                WK-PRSTZ-ULT
                WK-INCR-PRSTZ.
      *
           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
      *
               IF WTGA-PRE-INI-NET-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
               AND WTGA-ID-GAR(IX-TAB-TGA) = WGRZ-ID-GAR(IX-TAB-GRZ)
      *
                  COMPUTE WK-PRE-NET =
                         (WK-PRE-NET + WTGA-PRE-INI-NET(IX-TAB-TGA))
      *
               END-IF
      *
               IF WTGA-RIS-MAT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
               AND WTGA-ID-GAR(IX-TAB-TGA) = WGRZ-ID-GAR(IX-TAB-GRZ)
      *
                  COMPUTE WK-RIS-MAT =
                         (WK-RIS-MAT + WTGA-RIS-MAT(IX-TAB-TGA))
      *
               END-IF
      *
               IF WTGA-PRSTZ-ULT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
               AND WTGA-ID-GAR(IX-TAB-TGA) = WGRZ-ID-GAR(IX-TAB-GRZ)
      *
                  COMPUTE WK-PRSTZ-ULT =
                         (WK-PRSTZ-ULT + WTGA-PRSTZ-ULT(IX-TAB-TGA))
      *
               END-IF
      *
               IF WTGA-INCR-PRSTZ-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
               AND WTGA-ID-GAR(IX-TAB-TGA) = WGRZ-ID-GAR(IX-TAB-GRZ)
      *
                  COMPUTE WK-INCR-PRSTZ =
                         (WK-INCR-PRSTZ + WTGA-INCR-PRSTZ(IX-TAB-TGA))
      *
               END-IF
      *
           END-PERFORM.
      *
           MOVE WK-PRE-NET
             TO W820-PREMIO-NET.
      *
           MOVE WK-RIS-MAT
             TO W820-RISERVA.
      *
           MOVE WK-PRSTZ-ULT
             TO W820-CPT-RIVTO.
      *
           MOVE WK-INCR-PRSTZ
             TO W820-ULT-INCR.
      *
       S12200-DATI-TRANCHE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * DATI PROVENIENTI DALLA TABELLA RAPPORTO RETE
      *----------------------------------------------------------------*
      *
       S12300-DATI-RAPP-RETE.
      *
           MOVE 'S12300-DATI-RAPP-RETE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           PERFORM S12310-IMPOSTA-RRE
              THRU S12310-IMPOSTA-RRE-EX.
      *
           PERFORM S12320-LEGGI-RRE
              THRU S12320-LEGGI-RRE-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              IF RRE-COD-PNT-RETE-INI-NULL NOT = HIGH-VALUES
      *
                 MOVE RRE-COD-PNT-RETE-INI
                   TO W820-COD-AGE
      *
              END-IF
      *
           END-IF.
      *
       S12300-DATI-RAPP-RETE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  VALORIZZAZIONI PER LETTURA RAPPORTO RETE
      *----------------------------------------------------------------*
      *
       S12310-IMPOSTA-RRE.
      *
           MOVE 'S12310-IMPOSTA-RRE'          TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           MOVE WPOL-ID-POLI                  TO RRE-ID-OGG.
      *
           SET POLIZZA                        TO TRUE
           MOVE WS-TP-OGG                     TO RRE-TP-OGG.
      *
           SET GESTIONE                       TO TRUE
           MOVE WS-TP-RETE                    TO RRE-TP-RETE.
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE ZEROES
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
                IDSI0011-DATA-COMPETENZA.
      *
      *  --> Nome tabella fisica db
      *
           MOVE LDBS4240                TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE SPACES                  TO IDSI0011-BUFFER-WHERE-COND
                                           IDSI0011-BUFFER-DATI.
           MOVE RAPP-RETE               TO IDSI0011-BUFFER-DATI.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-SELECT          TO TRUE.
           SET IDSI0011-TRATT-DEFAULT   TO TRUE.
           SET IDSI0011-WHERE-CONDITION TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL  TO TRUE.
      *
       S12310-IMPOSTA-RRE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  LETTURA DELLA TABELLA RAPPORTO RETE
      *----------------------------------------------------------------*
      *
       S12320-LEGGI-RRE.
      *
           MOVE 'S12320-LEGGI-RRE'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non trovata
      *
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR
                         TO IEAI9901-LABEL-ERR
                       MOVE '005019'
                         TO IEAI9901-COD-ERRORE
                       MOVE 'RAPPORTO RETE NON TROVATA'
                         TO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
                       MOVE IDSO0011-BUFFER-DATI
                         TO RAPP-RETE
      *
                  WHEN OTHER
      *
      *  --> Errore Lettura
      *
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR
                         TO IEAI9901-LABEL-ERR
                       MOVE '005015'
                         TO IEAI9901-COD-ERRORE
                       STRING 'ERRORE LETTURA RAPPORTO RETE ' ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE
                       INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore Dispatcher
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              MOVE 'ERRORE DISPATCHER LETTURA RAPPORTO RETE'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
       S12320-LEGGI-RRE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * DATI PROVENIENTI DALLA TABELLA MOVI
      *----------------------------------------------------------------*
      *
       S12400-DATI-MOVI.
      *
           MOVE 'S12400-DATI-MOVI'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           MOVE ZEROES
             TO WK-IMP-LRD-EFFLQ.
      *
           SET WK-VERS-AGG-NO
             TO TRUE.
      *
           SET WK-RISC-PARZ-NO
             TO TRUE.
      *
           SET WK-FINE-MOV-NO
             TO TRUE.
      *
           SET IDSI0011-FETCH-FIRST
             TO TRUE.
      *
           PERFORM S12410-IMPOSTA-MOVI
              THRU S12410-IMPOSTA-MOVI-EX.
      *
           PERFORM S12420-LEGGI-MOVI
              THRU S12420-LEGGI-MOVI-EX
             UNTIL WK-FINE-MOV-SI
                OR IDSV0001-ESITO-KO.
      *
           IF IDSV0001-ESITO-OK
      *
              IF WK-VERS-AGG-SI
      *
                 MOVE 'S'
                   TO W820-FLAG-VERS-AGG
      *
              END-IF
      *
              IF WK-RISC-PARZ-SI
      *
                 MOVE 'S'
                   TO W820-FLAG-RIS-PARZ
      *
              END-IF
      *
              MOVE WK-IMP-LRD-EFFLQ
                TO W820-IMP-RISC

      *
           END-IF.
      *
       S12400-DATI-MOVI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VALORIZZAZIONI PER LETTURA TABELLA MOVI
      *----------------------------------------------------------------*
      *
       S12410-IMPOSTA-MOVI.
      *
           MOVE 'S12410-IMPOSTA-MOVI'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           INITIALIZE LDBV3611.
      *
           MOVE WPOL-ID-POLI
             TO LDBV3611-ID-OGG.
      *
           SET POLIZZA
             TO TRUE
           MOVE WS-TP-OGG
             TO LDBV3611-TP-OGG.
      *
           MOVE IDSV0001-DATA-EFFETTO
             TO WK-APPO-DT-NO-SEGNO.
           MOVE WK-APPO-DT-NO-SEGNO
             TO WK-APPO-DT-N.
      *
           MOVE 01
             TO WK-DT-N-GG.
           MOVE 01
             TO WK-DT-N-MM.
           MOVE WK-APPO-DT-N
             TO WK-APPO-DT-NO-SEGNO.
           MOVE WK-APPO-DT-NO-SEGNO
             TO LDBV3611-DT-DA.
      *
           MOVE 31
             TO WK-DT-N-GG.
           MOVE 12
             TO WK-DT-N-MM.
           MOVE WK-APPO-DT-N
             TO WK-APPO-DT-NO-SEGNO.
           MOVE WK-APPO-DT-NO-SEGNO
             TO LDBV3611-DT-A.
      *
           MOVE 1065
             TO LDBV3611-TP-MOVI-1.
      *
           MOVE 5006
             TO LDBV3611-TP-MOVI-2.
      *
           MOVE 5007
             TO LDBV3611-TP-MOVI-3.
      *
           MOVE 6005
             TO LDBV3611-TP-MOVI-4.
      *
           MOVE 5005
             TO LDBV3611-TP-MOVI-5.

10819      MOVE 2318
10819        TO LDBV3611-TP-MOVI-6.

10819      MOVE 2319
10819        TO LDBV3611-TP-MOVI-7.

10819X     MOVE 2316
10819X       TO LDBV3611-TP-MOVI-8.


10819X     MOVE 2317
10819X       TO LDBV3611-TP-MOVI-9.

      *
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE ZEROES
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
                IDSI0011-DATA-COMPETENZA.
      *
      *  --> Nome tabella fisica db
      *
           MOVE LDBS3610
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE LDBV3611
             TO IDSI0011-BUFFER-WHERE-COND.
           MOVE SPACES
              TO IDSI0011-BUFFER-DATI.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-TRATT-SENZA-STOR
             TO TRUE.
           SET IDSI0011-WHERE-CONDITION
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S12410-IMPOSTA-MOVI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * LETTURA TABELLA MOVI
      *----------------------------------------------------------------*
      *
       S12420-LEGGI-MOVI.
      *
           MOVE 'S12420-LEGGI-MOVI'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non TROVATO
      *
                       CONTINUE
      *
      *  --> Fine occorrenze fetch
      *
                       SET WK-FINE-MOV-SI
                         TO TRUE
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
      *  --> Operazione eseguita correttamente
      *
                       MOVE IDSO0011-BUFFER-DATI
                         TO MOVI
      *
22702                  PERFORM S12430-TEST-MOVI
22702                     THRU S12430-TEST-MOVI-EX
22702 *
22702                  SET IDSI0011-FETCH-NEXT
22702                    TO TRUE
      *
                  WHEN OTHER
      *
      *  --> Errore di accesso al db
      *
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR
                         TO IEAI9901-LABEL-ERR
                       STRING 'ERRORE LETTURA MOVIMENTO' ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore dispatcher
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              STRING 'ERRORE DISPATCHER LETTURA MOVIMENTO' ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
           PERFORM S12410-IMPOSTA-MOVI
              THRU S12410-IMPOSTA-MOVI-EX.
      *
       S12420-LEGGI-MOVI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * TEST SUL TIPO MOVIMENTO ESTRATTO
      *----------------------------------------------------------------*
      *
       S12430-TEST-MOVI.
      *
           MOVE 'S12430-TEST-MOVI'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           IF MOV-TP-MOVI = 1065
      *
              SET WK-VERS-AGG-SI
                TO TRUE
      *
           ELSE
      *
              PERFORM S12440-CERCA-IMP-RISC-PARZ
                 THRU S12440-CERCA-IMP-RISC-PARZ-EX
      *
           END-IF.
      *
       S12430-TEST-MOVI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VENGONO CERCATE LE OCCORRENZE TRANCHE DI
      * LIQUIDAZIONE RELATIVE A TRANCHE DI GARANZIA
      * APPARTENENTI ALLA GARANZIA IN ESAME
      *----------------------------------------------------------------*
      *
       S12440-CERCA-IMP-RISC-PARZ.
      *
           MOVE 'S12440-CERCA-IMP-RISC-PARZ'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           SET WK-FINE-TLI-NO
             TO TRUE.
      *
           SET IDSI0011-FETCH-FIRST
             TO TRUE.
      *
           PERFORM S12450-IMPOSTA-TRANCHE-LIQ
              THRU S12450-IMPOSTA-TRANCHE-LIQ-EX
      *
           PERFORM S12460-LEGGI-TRANCHE-LIQ
              THRU S12460-LEGGI-TRANCHE-LIQ-EX
             UNTIL WK-FINE-TLI-SI
                OR IDSV0001-ESITO-KO.
      *
       S12440-CERCA-IMP-RISC-PARZ-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VALORIZZAZIONI PER LETTURA TRRANCHE DI LIQUIDAZIONE
      *----------------------------------------------------------------*
      *
       S12450-IMPOSTA-TRANCHE-LIQ.
      *
           MOVE 'S12450-IMPOSTA-TRANCHE-LIQ'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           INITIALIZE LDBV3621.
      *
           MOVE MOV-ID-MOVI
             TO LDBV3621-ID-MOVI-CRZ.
      *
      *  --> La data effetto viene sempre valorizzata
      *
           MOVE ZEROES
             TO IDSI0011-DATA-INIZIO-EFFETTO
                IDSI0011-DATA-FINE-EFFETTO
                IDSI0011-DATA-COMPETENZA.
      *
      *  --> Nome tabella fisica db
      *
           MOVE LDBS3620
             TO IDSI0011-CODICE-STR-DATO.
      *
      *  --> Dclgen tabella
      *
           MOVE LDBV3621
             TO IDSI0011-BUFFER-WHERE-COND.
           MOVE SPACES
              TO IDSI0011-BUFFER-DATI.
      *
      *  --> Tipo operazione
      *
           SET IDSI0011-TRATT-DEFAULT
             TO TRUE.
           SET IDSI0011-WHERE-CONDITION
             TO TRUE.
      *
      *  --> Modalita di accesso
      *
           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.
      *
       S12450-IMPOSTA-TRANCHE-LIQ-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * LETTURA TRRANCHE DI LIQUIDAZIONE
      *----------------------------------------------------------------*
      *
       S12460-LEGGI-TRANCHE-LIQ.
      *
           MOVE 'S12460-LEGGI-TRANCHE-LIQ'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX
      *
           IF IDSO0011-SUCCESSFUL-RC
      *
              EVALUATE TRUE
      *
                  WHEN IDSO0011-NOT-FOUND
      *
      *  --> Chiave non TROVATO
      *
                       IF IDSI0011-FETCH-FIRST
      *
22702                     SET WK-MOV-NON-TROVATO TO TRUE
22702 *
22702                     PERFORM S12465-CONTROLLA-MOVIMENTO
22702                        THRU S12465-EX
      *
22702                     IF WK-MOV-NON-TROVATO
                          MOVE WK-PGM
                            TO IEAI9901-COD-SERVIZIO-BE
                          MOVE WK-LABEL-ERR
                            TO IEAI9901-LABEL-ERR
                          STRING 'TRANCHE LIQ NON TROVATA' ';'
                                 IDSO0011-RETURN-CODE ';'
                                 IDSO0011-SQLCODE
                                 DELIMITED BY SIZE
                                 INTO IEAI9901-PARAMETRI-ERR
                          END-STRING
                          MOVE '005069'
                            TO IEAI9901-COD-ERRORE
                          PERFORM S0300-RICERCA-GRAVITA-ERRORE
                             THRU EX-S0300
      *
22702                     END-IF
                       END-IF
      *
      *  --> Fine occorrenze fetch
      *
                       SET WK-FINE-TLI-SI
                         TO TRUE
      *
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *
      *  --> Operazione eseguita correttamente
      *
22702                  PERFORM S12466-MOV-TROVATI
22702                     THRU S12466-EX
      *
                       MOVE IDSO0011-BUFFER-DATI
                         TO TRCH-LIQ
      *
                       SET IDSI0011-FETCH-NEXT
                         TO TRUE
      *
                       PERFORM S12470-TEST-LEGAME-GAR
                          THRU S12470-TEST-LEGAME-GAR-EX
      *
                  WHEN OTHER
      *
      *  --> Errore di accesso al db
      *
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR
                         TO IEAI9901-LABEL-ERR
                       STRING 'ERRORE LETTURA TRANCHE LIQ' ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
      *
           ELSE
      *
      *  --> Errore dispatcher
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              STRING 'ERRORE DISPATCHER LETTURA TRANCHE LIQ' ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
       S12460-LEGGI-TRANCHE-LIQ-EX.
           EXIT.
      *
22702 *----------------------------------------------------------------*
22702 * CONTROLLO IL MOVIMENTO NON TROVATO NELLE TRANCHE DI LIQUIDAZIONE
22702 *----------------------------------------------------------------*
22702 *
22702  S12465-CONTROLLA-MOVIMENTO.
22702 *
22702 * SE IL MOVIMENTO NON TROVATO E' DI LIQUIDAZIONE E' NECESSARIO
22702 * VERIFICARE L'ESISTENZA DEL MOVIMENTO DI COMUNICAZIONE PER
22702 * LA STESSA DATA EFFETTO
22702 *
22702      IF MOV-TP-MOVI = 5006
22702         PERFORM VARYING WK-IND-MOV FROM 1 BY 1
22702                   UNTIL WK-IND-MOV > WK-IND-MOV-MAX
22702                      OR WK-MOV-TROVATO
22702            IF WK-DT-EFF(WK-IND-MOV) = MOV-DT-EFF
10819               AND WK-TP-MOVI(WK-IND-MOV) = 5005
22702               SET WK-MOV-TROVATO TO TRUE
22702            END-IF
22702         END-PERFORM
22702      END-IF.

10819      IF MOV-TP-MOVI = 2319
10819         PERFORM VARYING WK-IND-MOV FROM 1 BY 1
10819                   UNTIL WK-IND-MOV > WK-IND-MOV-MAX
10819                      OR WK-MOV-TROVATO
10819            IF WK-DT-EFF(WK-IND-MOV) = MOV-DT-EFF
10819               AND WK-TP-MOVI(WK-IND-MOV) = 2318
10819               SET WK-MOV-TROVATO TO TRUE
10819            END-IF
10819         END-PERFORM
10819      END-IF.

10819X     IF MOV-TP-MOVI = 2317
10819X        PERFORM VARYING WK-IND-MOV FROM 1 BY 1
10819X                  UNTIL WK-IND-MOV > WK-IND-MOV-MAX
10819X                     OR WK-MOV-TROVATO
10819X           IF WK-DT-EFF(WK-IND-MOV) = MOV-DT-EFF
10819X              AND WK-TP-MOVI(WK-IND-MOV) = 2316
10819X              SET WK-MOV-TROVATO TO TRUE
10819X           END-IF
10819X        END-PERFORM
10819X     END-IF.

22702 * SE IL MOVIMENTO NON TROVATO E' DI COMUNICAZIONE SI IMPOSTA
22702 * IL MOVIMENTO A TROVATO PER PERMETTERE AL PROGRAMMA DI ELABORARE
22702 * IL MOVIMENTO DI LIQUIDAZIONE
22702      IF MOV-TP-MOVI = 5005
10819      OR MOV-TP-MOVI = 2318
10819X     OR MOV-TP-MOVI = 2316
22702         SET WK-MOV-TROVATO TO TRUE
22702      END-IF.
22702 *
22702  S12465-EX.
22702      EXIT.
22702 *
22702 *----------------------------------------------------------------*
22702 * CONTROLLO IL MOVIMENTO TROVATO NELLE TRANCHE DI LIQUIDAZIONE
22702 *----------------------------------------------------------------*
22702 *
22702  S12466-MOV-TROVATI.
22702 *
22702 * SE LA TRANCHE DI LIQUIDAZIONE E' STATA STORICIZZATA DA UN
22702 * MOVIMENTO DI COMUNICAZIONE LO SALVO IN UN VETTORE
22702 *
22702      IF MOV-TP-MOVI = 5005
10819      OR MOV-TP-MOVI = 2318
10819X     OR MOV-TP-MOVI = 2316
22702         ADD 1 TO WK-IND-MOV-MAX
22702         MOVE MOV-TP-MOVI TO WK-TP-MOVI(WK-IND-MOV-MAX)
22702         MOVE MOV-DT-EFF  TO WK-DT-EFF(WK-IND-MOV-MAX)
22702      END-IF.
22702 *
22702  S12466-EX.
22702      EXIT.
      *
      *----------------------------------------------------------------*
      * VENGONO CONSIDERATE SOLO LE TRANCHE DI LIQ. RELATIVE ALLA GAR
      *----------------------------------------------------------------*
      *
       S12470-TEST-LEGAME-GAR.
      *
           MOVE 'S12470-TEST-LEGAME-GAR'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
      *
               IF TLI-ID-TRCH-DI-GAR = WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
      *
                  IF WTGA-ID-GAR(IX-TAB-TGA) = WGRZ-ID-GAR(IX-TAB-GRZ)
      *
                     SET WK-RISC-PARZ-SI
                       TO TRUE
      *
                     COMPUTE WK-IMP-LRD-EFFLQ =
                            (WK-IMP-LRD-EFFLQ + TLI-IMP-LRD-EFFLQ)
      *
                  END-IF
      *
               END-IF
      *
           END-PERFORM.
      *
       S12470-TEST-LEGAME-GAR-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * OPERAZIONE DI WRITE (DATI)
      *----------------------------------------------------------------*
      *
       S12999-WRITE-REC-D.
      *
           MOVE 'S12999-WRITE-REC-D'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           WRITE OUMANFEE-REC FROM W820-AREA-DATI.
      *
           IF FS-OUT NOT = '00'
      *
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              MOVE 'ERRORE SCRITTURA FILE DI OUTPUT'
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
      *
           END-IF.
      *
       S12999-WRITE-REC-D-EX.
           EXIT.
      *
      * ============================================================== *
      * -->           O P E R Z I O N I   F I N A L I              <-- *
      * ============================================================== *
      *
       S90000-OPERAZ-FINALI.
      *
           MOVE 'S90000-OPERAZ-FINALI'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           PERFORM S90100-CLOSE-OUT
              THRU S90100-CLOSE-OUT-EX.
      *
       S90000-OPERAZ-FINALI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * CHIUSURA DEL FILE DI OUTPUT OUMANFEE
      *----------------------------------------------------------------*
      *
       S90100-CLOSE-OUT.
      *
           MOVE 'S90100-CLOSE-OUT'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           IF WK-OPEN
      *
              CLOSE OUMANFEE
      *
              IF FS-OUT NOT = '00'
      *
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR
                   TO IEAI9901-LABEL-ERR
                 MOVE '001114'
                   TO IEAI9901-COD-ERRORE
                 MOVE 'ERRORE CHIUSURA FILE DI OUTPUT'
                   TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
      *
              END-IF
      *
           END-IF.
      *
       S90100-CLOSE-OUT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
      *
       S99999-CONV-N-TO-X.
      *
           MOVE 'S99999-CONV-N-TO-X'
             TO WK-LABEL-ERR.
      *
           PERFORM DISPLAY-LABEL
              THRU DISPLAY-LABEL-EX.
      *
           MOVE WK-DT-N-AAAA
             TO WK-DT-X-AAAA.
      *
           MOVE WK-DT-N-MM
             TO WK-DT-X-MM.
      *
           MOVE WK-DT-N-GG
             TO WK-DT-X-GG.
      *
       S99999-CONV-N-TO-X-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
      *
           COPY IDSP0012.
      *
      *----------------------------------------------------------------*
      *  ROUTINES GESTIONE ERRORI                                      *
      *----------------------------------------------------------------*
      *
           COPY IERP9901.
           COPY IERP9902.
           COPY IERP9903.
      *
      *----------------------------------------------------------------*
      * DIPLAY LABEL
      *----------------------------------------------------------------*
      *
       DISPLAY-LABEL.
      *
           IF (IDSV0001-DEBUG-BASSO OR IDSV0001-DEBUG-ESASPERATO) AND
           NOT IDSV0001-ON-LINE
      *
              CONTINUE
      *       DISPLAY WK-LABEL-ERR
      *
           END-IF.
      *
       DISPLAY-LABEL-EX.
           EXIT.
      *


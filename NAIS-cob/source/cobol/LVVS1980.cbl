      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVSXXXX.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2012.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVSXXXX
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CALCOLO VARIABILE IMPPRESTNR
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVSXXXX'.
       01  IDBSPRE0                         PIC X(008) VALUE 'IDBSPRE0'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01 WS-SOM-IMP-PRE                    PIC S9(12)V9(3) COMP-3.
      *----------------------------------------------------------------*
      *    COPY TABELLE DB
      *----------------------------------------------------------------*
          COPY IDBVPRE1.

      *----------------------------------------------------------------*
      *--> AREA TRCH-DI-GAR
      *----------------------------------------------------------------*
       01 AREA-IO-ADES.
          03 DADE-AREA-ADES.
             04 DADE-ELE-MAX-ADE          PIC S9(04) COMP.
             04 DADE-TAB-ADE.
             COPY LCCVADE1              REPLACING ==(SF)== BY ==DADE==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS1000.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS1000.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT
                                             WS-SOM-IMP-PRE.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
              PERFORM S1200-CALCOLO-VAR
                 THRU S1200-EX
           END-IF

           MOVE WS-SOM-IMP-PRE
             TO IVVC0213-VAL-IMP-O.

      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-ADES
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DADE-AREA-ADES
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    SOMMATORIA IMP-PRE TABELLA PRESTITI
      *----------------------------------------------------------------*
       S1200-CALCOLO-VAR.

           INITIALIZE PREST

           SET IDSV0003-ID-OGGETTO             TO TRUE
           SET IDSV0003-FETCH-FIRST            TO TRUE
           SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE
           SET IDSV0003-SUCCESSFUL-RC          TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL         TO TRUE
      *
           MOVE IVVC0213-ID-ADESIONE  TO PRE-ID-OGG
           MOVE 'AD'                  TO PRE-TP-OGG

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR NOT IDSV0003-SUCCESSFUL-SQL
      *
              CALL IDBSPRE0  USING  IDSV0003 PREST
              ON EXCEPTION
                 MOVE IDBSPRE0
                   TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'ERRORE CALL IDBSPRE0'
                   TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER  TO TRUE
              END-CALL
      *
              EVALUATE TRUE
                 WHEN IDSV0003-NOT-FOUND
      *-->          CHIAVE NON TROVATA
                      CONTINUE

                 WHEN IDSV0003-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                       IF PRE-IMP-PREST-NULL = HIGH-VALUE
                                      OR LOW-VALUE OR SPACES
                          CONTINUE
                       ELSE
                         COMPUTE WS-SOM-IMP-PRE =
                                 WS-SOM-IMP-PRE +
                                 PRE-IMP-PREST
                        END-IF

                    SET IDSV0003-FETCH-NEXT TO TRUE
      *
                 WHEN OTHER
                    SET IDSV0003-INVALID-OPER  TO TRUE
                    MOVE WK-PGM
                      TO IDSV0003-COD-SERVIZIO-BE
                    STRING 'ERRORE LETTURA TABELLA PRESTITI ;'
                          IDSV0003-RETURN-CODE ';'
                          IDSV0003-SQLCODE
                      DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    END-STRING
              END-EVALUATE
      *
           END-PERFORM.

       S1200-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

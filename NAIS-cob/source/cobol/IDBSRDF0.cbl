       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSRDF0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  25 NOV 2019.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDRDF0 END-EXEC.
           EXEC SQL INCLUDE IDBVRDF2 END-EXEC.
           EXEC SQL INCLUDE IDBVRDF3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVRDF1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 RICH-DIS-FND.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSRDF0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'RICH_DIS_FND' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RICH_DIS_FND
                ,ID_MOVI_FINRIO
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,COD_FND
                ,NUM_QUO
                ,PC
                ,IMP_MOVTO
                ,DT_DIS
                ,COD_TARI
                ,TP_STAT
                ,TP_MOD_DIS
                ,COD_DIV
                ,DT_CAMBIO_VLT
                ,TP_FND
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,DT_DIS_CALC
                ,FL_CALC_DIS
                ,COMMIS_GEST
                ,NUM_QUO_CDG_FNZ
                ,NUM_QUO_CDGTOT_FNZ
                ,COS_RUN_ASSVA_IDC
                ,FL_SWM_BP2S
             INTO
                :RDF-ID-RICH-DIS-FND
               ,:RDF-ID-MOVI-FINRIO
               ,:RDF-ID-MOVI-CRZ
               ,:RDF-ID-MOVI-CHIU
                :IND-RDF-ID-MOVI-CHIU
               ,:RDF-DT-INI-EFF-DB
               ,:RDF-DT-END-EFF-DB
               ,:RDF-COD-COMP-ANIA
               ,:RDF-COD-FND
                :IND-RDF-COD-FND
               ,:RDF-NUM-QUO
                :IND-RDF-NUM-QUO
               ,:RDF-PC
                :IND-RDF-PC
               ,:RDF-IMP-MOVTO
                :IND-RDF-IMP-MOVTO
               ,:RDF-DT-DIS-DB
                :IND-RDF-DT-DIS
               ,:RDF-COD-TARI
                :IND-RDF-COD-TARI
               ,:RDF-TP-STAT
                :IND-RDF-TP-STAT
               ,:RDF-TP-MOD-DIS
                :IND-RDF-TP-MOD-DIS
               ,:RDF-COD-DIV
               ,:RDF-DT-CAMBIO-VLT-DB
                :IND-RDF-DT-CAMBIO-VLT
               ,:RDF-TP-FND
               ,:RDF-DS-RIGA
               ,:RDF-DS-OPER-SQL
               ,:RDF-DS-VER
               ,:RDF-DS-TS-INI-CPTZ
               ,:RDF-DS-TS-END-CPTZ
               ,:RDF-DS-UTENTE
               ,:RDF-DS-STATO-ELAB
               ,:RDF-DT-DIS-CALC-DB
                :IND-RDF-DT-DIS-CALC
               ,:RDF-FL-CALC-DIS
                :IND-RDF-FL-CALC-DIS
               ,:RDF-COMMIS-GEST
                :IND-RDF-COMMIS-GEST
               ,:RDF-NUM-QUO-CDG-FNZ
                :IND-RDF-NUM-QUO-CDG-FNZ
               ,:RDF-NUM-QUO-CDGTOT-FNZ
                :IND-RDF-NUM-QUO-CDGTOT-FNZ
               ,:RDF-COS-RUN-ASSVA-IDC
                :IND-RDF-COS-RUN-ASSVA-IDC
               ,:RDF-FL-SWM-BP2S
                :IND-RDF-FL-SWM-BP2S
             FROM RICH_DIS_FND
             WHERE     DS_RIGA = :RDF-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO RICH_DIS_FND
                     (
                        ID_RICH_DIS_FND
                       ,ID_MOVI_FINRIO
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,COD_FND
                       ,NUM_QUO
                       ,PC
                       ,IMP_MOVTO
                       ,DT_DIS
                       ,COD_TARI
                       ,TP_STAT
                       ,TP_MOD_DIS
                       ,COD_DIV
                       ,DT_CAMBIO_VLT
                       ,TP_FND
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,DT_DIS_CALC
                       ,FL_CALC_DIS
                       ,COMMIS_GEST
                       ,NUM_QUO_CDG_FNZ
                       ,NUM_QUO_CDGTOT_FNZ
                       ,COS_RUN_ASSVA_IDC
                       ,FL_SWM_BP2S
                     )
                 VALUES
                     (
                       :RDF-ID-RICH-DIS-FND
                       ,:RDF-ID-MOVI-FINRIO
                       ,:RDF-ID-MOVI-CRZ
                       ,:RDF-ID-MOVI-CHIU
                        :IND-RDF-ID-MOVI-CHIU
                       ,:RDF-DT-INI-EFF-DB
                       ,:RDF-DT-END-EFF-DB
                       ,:RDF-COD-COMP-ANIA
                       ,:RDF-COD-FND
                        :IND-RDF-COD-FND
                       ,:RDF-NUM-QUO
                        :IND-RDF-NUM-QUO
                       ,:RDF-PC
                        :IND-RDF-PC
                       ,:RDF-IMP-MOVTO
                        :IND-RDF-IMP-MOVTO
                       ,:RDF-DT-DIS-DB
                        :IND-RDF-DT-DIS
                       ,:RDF-COD-TARI
                        :IND-RDF-COD-TARI
                       ,:RDF-TP-STAT
                        :IND-RDF-TP-STAT
                       ,:RDF-TP-MOD-DIS
                        :IND-RDF-TP-MOD-DIS
                       ,:RDF-COD-DIV
                       ,:RDF-DT-CAMBIO-VLT-DB
                        :IND-RDF-DT-CAMBIO-VLT
                       ,:RDF-TP-FND
                       ,:RDF-DS-RIGA
                       ,:RDF-DS-OPER-SQL
                       ,:RDF-DS-VER
                       ,:RDF-DS-TS-INI-CPTZ
                       ,:RDF-DS-TS-END-CPTZ
                       ,:RDF-DS-UTENTE
                       ,:RDF-DS-STATO-ELAB
                       ,:RDF-DT-DIS-CALC-DB
                        :IND-RDF-DT-DIS-CALC
                       ,:RDF-FL-CALC-DIS
                        :IND-RDF-FL-CALC-DIS
                       ,:RDF-COMMIS-GEST
                        :IND-RDF-COMMIS-GEST
                       ,:RDF-NUM-QUO-CDG-FNZ
                        :IND-RDF-NUM-QUO-CDG-FNZ
                       ,:RDF-NUM-QUO-CDGTOT-FNZ
                        :IND-RDF-NUM-QUO-CDGTOT-FNZ
                       ,:RDF-COS-RUN-ASSVA-IDC
                        :IND-RDF-COS-RUN-ASSVA-IDC
                       ,:RDF-FL-SWM-BP2S
                        :IND-RDF-FL-SWM-BP2S
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE RICH_DIS_FND SET

                   ID_RICH_DIS_FND        =
                :RDF-ID-RICH-DIS-FND
                  ,ID_MOVI_FINRIO         =
                :RDF-ID-MOVI-FINRIO
                  ,ID_MOVI_CRZ            =
                :RDF-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :RDF-ID-MOVI-CHIU
                                       :IND-RDF-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :RDF-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :RDF-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :RDF-COD-COMP-ANIA
                  ,COD_FND                =
                :RDF-COD-FND
                                       :IND-RDF-COD-FND
                  ,NUM_QUO                =
                :RDF-NUM-QUO
                                       :IND-RDF-NUM-QUO
                  ,PC                     =
                :RDF-PC
                                       :IND-RDF-PC
                  ,IMP_MOVTO              =
                :RDF-IMP-MOVTO
                                       :IND-RDF-IMP-MOVTO
                  ,DT_DIS                 =
           :RDF-DT-DIS-DB
                                       :IND-RDF-DT-DIS
                  ,COD_TARI               =
                :RDF-COD-TARI
                                       :IND-RDF-COD-TARI
                  ,TP_STAT                =
                :RDF-TP-STAT
                                       :IND-RDF-TP-STAT
                  ,TP_MOD_DIS             =
                :RDF-TP-MOD-DIS
                                       :IND-RDF-TP-MOD-DIS
                  ,COD_DIV                =
                :RDF-COD-DIV
                  ,DT_CAMBIO_VLT          =
           :RDF-DT-CAMBIO-VLT-DB
                                       :IND-RDF-DT-CAMBIO-VLT
                  ,TP_FND                 =
                :RDF-TP-FND
                  ,DS_RIGA                =
                :RDF-DS-RIGA
                  ,DS_OPER_SQL            =
                :RDF-DS-OPER-SQL
                  ,DS_VER                 =
                :RDF-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :RDF-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :RDF-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :RDF-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :RDF-DS-STATO-ELAB
                  ,DT_DIS_CALC            =
           :RDF-DT-DIS-CALC-DB
                                       :IND-RDF-DT-DIS-CALC
                  ,FL_CALC_DIS            =
                :RDF-FL-CALC-DIS
                                       :IND-RDF-FL-CALC-DIS
                  ,COMMIS_GEST            =
                :RDF-COMMIS-GEST
                                       :IND-RDF-COMMIS-GEST
                  ,NUM_QUO_CDG_FNZ        =
                :RDF-NUM-QUO-CDG-FNZ
                                       :IND-RDF-NUM-QUO-CDG-FNZ
                  ,NUM_QUO_CDGTOT_FNZ     =
                :RDF-NUM-QUO-CDGTOT-FNZ
                                       :IND-RDF-NUM-QUO-CDGTOT-FNZ
                  ,COS_RUN_ASSVA_IDC      =
                :RDF-COS-RUN-ASSVA-IDC
                                       :IND-RDF-COS-RUN-ASSVA-IDC
                  ,FL_SWM_BP2S            =
                :RDF-FL-SWM-BP2S
                                       :IND-RDF-FL-SWM-BP2S
                WHERE     DS_RIGA = :RDF-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM RICH_DIS_FND
                WHERE     DS_RIGA = :RDF-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-RDF CURSOR FOR
              SELECT
                     ID_RICH_DIS_FND
                    ,ID_MOVI_FINRIO
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,COD_FND
                    ,NUM_QUO
                    ,PC
                    ,IMP_MOVTO
                    ,DT_DIS
                    ,COD_TARI
                    ,TP_STAT
                    ,TP_MOD_DIS
                    ,COD_DIV
                    ,DT_CAMBIO_VLT
                    ,TP_FND
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,DT_DIS_CALC
                    ,FL_CALC_DIS
                    ,COMMIS_GEST
                    ,NUM_QUO_CDG_FNZ
                    ,NUM_QUO_CDGTOT_FNZ
                    ,COS_RUN_ASSVA_IDC
                    ,FL_SWM_BP2S
              FROM RICH_DIS_FND
              WHERE     ID_RICH_DIS_FND = :RDF-ID-RICH-DIS-FND
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RICH_DIS_FND
                ,ID_MOVI_FINRIO
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,COD_FND
                ,NUM_QUO
                ,PC
                ,IMP_MOVTO
                ,DT_DIS
                ,COD_TARI
                ,TP_STAT
                ,TP_MOD_DIS
                ,COD_DIV
                ,DT_CAMBIO_VLT
                ,TP_FND
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,DT_DIS_CALC
                ,FL_CALC_DIS
                ,COMMIS_GEST
                ,NUM_QUO_CDG_FNZ
                ,NUM_QUO_CDGTOT_FNZ
                ,COS_RUN_ASSVA_IDC
                ,FL_SWM_BP2S
             INTO
                :RDF-ID-RICH-DIS-FND
               ,:RDF-ID-MOVI-FINRIO
               ,:RDF-ID-MOVI-CRZ
               ,:RDF-ID-MOVI-CHIU
                :IND-RDF-ID-MOVI-CHIU
               ,:RDF-DT-INI-EFF-DB
               ,:RDF-DT-END-EFF-DB
               ,:RDF-COD-COMP-ANIA
               ,:RDF-COD-FND
                :IND-RDF-COD-FND
               ,:RDF-NUM-QUO
                :IND-RDF-NUM-QUO
               ,:RDF-PC
                :IND-RDF-PC
               ,:RDF-IMP-MOVTO
                :IND-RDF-IMP-MOVTO
               ,:RDF-DT-DIS-DB
                :IND-RDF-DT-DIS
               ,:RDF-COD-TARI
                :IND-RDF-COD-TARI
               ,:RDF-TP-STAT
                :IND-RDF-TP-STAT
               ,:RDF-TP-MOD-DIS
                :IND-RDF-TP-MOD-DIS
               ,:RDF-COD-DIV
               ,:RDF-DT-CAMBIO-VLT-DB
                :IND-RDF-DT-CAMBIO-VLT
               ,:RDF-TP-FND
               ,:RDF-DS-RIGA
               ,:RDF-DS-OPER-SQL
               ,:RDF-DS-VER
               ,:RDF-DS-TS-INI-CPTZ
               ,:RDF-DS-TS-END-CPTZ
               ,:RDF-DS-UTENTE
               ,:RDF-DS-STATO-ELAB
               ,:RDF-DT-DIS-CALC-DB
                :IND-RDF-DT-DIS-CALC
               ,:RDF-FL-CALC-DIS
                :IND-RDF-FL-CALC-DIS
               ,:RDF-COMMIS-GEST
                :IND-RDF-COMMIS-GEST
               ,:RDF-NUM-QUO-CDG-FNZ
                :IND-RDF-NUM-QUO-CDG-FNZ
               ,:RDF-NUM-QUO-CDGTOT-FNZ
                :IND-RDF-NUM-QUO-CDGTOT-FNZ
               ,:RDF-COS-RUN-ASSVA-IDC
                :IND-RDF-COS-RUN-ASSVA-IDC
               ,:RDF-FL-SWM-BP2S
                :IND-RDF-FL-SWM-BP2S
             FROM RICH_DIS_FND
             WHERE     ID_RICH_DIS_FND = :RDF-ID-RICH-DIS-FND
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE RICH_DIS_FND SET

                   ID_RICH_DIS_FND        =
                :RDF-ID-RICH-DIS-FND
                  ,ID_MOVI_FINRIO         =
                :RDF-ID-MOVI-FINRIO
                  ,ID_MOVI_CRZ            =
                :RDF-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :RDF-ID-MOVI-CHIU
                                       :IND-RDF-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :RDF-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :RDF-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :RDF-COD-COMP-ANIA
                  ,COD_FND                =
                :RDF-COD-FND
                                       :IND-RDF-COD-FND
                  ,NUM_QUO                =
                :RDF-NUM-QUO
                                       :IND-RDF-NUM-QUO
                  ,PC                     =
                :RDF-PC
                                       :IND-RDF-PC
                  ,IMP_MOVTO              =
                :RDF-IMP-MOVTO
                                       :IND-RDF-IMP-MOVTO
                  ,DT_DIS                 =
           :RDF-DT-DIS-DB
                                       :IND-RDF-DT-DIS
                  ,COD_TARI               =
                :RDF-COD-TARI
                                       :IND-RDF-COD-TARI
                  ,TP_STAT                =
                :RDF-TP-STAT
                                       :IND-RDF-TP-STAT
                  ,TP_MOD_DIS             =
                :RDF-TP-MOD-DIS
                                       :IND-RDF-TP-MOD-DIS
                  ,COD_DIV                =
                :RDF-COD-DIV
                  ,DT_CAMBIO_VLT          =
           :RDF-DT-CAMBIO-VLT-DB
                                       :IND-RDF-DT-CAMBIO-VLT
                  ,TP_FND                 =
                :RDF-TP-FND
                  ,DS_RIGA                =
                :RDF-DS-RIGA
                  ,DS_OPER_SQL            =
                :RDF-DS-OPER-SQL
                  ,DS_VER                 =
                :RDF-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :RDF-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :RDF-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :RDF-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :RDF-DS-STATO-ELAB
                  ,DT_DIS_CALC            =
           :RDF-DT-DIS-CALC-DB
                                       :IND-RDF-DT-DIS-CALC
                  ,FL_CALC_DIS            =
                :RDF-FL-CALC-DIS
                                       :IND-RDF-FL-CALC-DIS
                  ,COMMIS_GEST            =
                :RDF-COMMIS-GEST
                                       :IND-RDF-COMMIS-GEST
                  ,NUM_QUO_CDG_FNZ        =
                :RDF-NUM-QUO-CDG-FNZ
                                       :IND-RDF-NUM-QUO-CDG-FNZ
                  ,NUM_QUO_CDGTOT_FNZ     =
                :RDF-NUM-QUO-CDGTOT-FNZ
                                       :IND-RDF-NUM-QUO-CDGTOT-FNZ
                  ,COS_RUN_ASSVA_IDC      =
                :RDF-COS-RUN-ASSVA-IDC
                                       :IND-RDF-COS-RUN-ASSVA-IDC
                  ,FL_SWM_BP2S            =
                :RDF-FL-SWM-BP2S
                                       :IND-RDF-FL-SWM-BP2S
                WHERE     DS_RIGA = :RDF-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-RDF
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-RDF
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-RDF
           INTO
                :RDF-ID-RICH-DIS-FND
               ,:RDF-ID-MOVI-FINRIO
               ,:RDF-ID-MOVI-CRZ
               ,:RDF-ID-MOVI-CHIU
                :IND-RDF-ID-MOVI-CHIU
               ,:RDF-DT-INI-EFF-DB
               ,:RDF-DT-END-EFF-DB
               ,:RDF-COD-COMP-ANIA
               ,:RDF-COD-FND
                :IND-RDF-COD-FND
               ,:RDF-NUM-QUO
                :IND-RDF-NUM-QUO
               ,:RDF-PC
                :IND-RDF-PC
               ,:RDF-IMP-MOVTO
                :IND-RDF-IMP-MOVTO
               ,:RDF-DT-DIS-DB
                :IND-RDF-DT-DIS
               ,:RDF-COD-TARI
                :IND-RDF-COD-TARI
               ,:RDF-TP-STAT
                :IND-RDF-TP-STAT
               ,:RDF-TP-MOD-DIS
                :IND-RDF-TP-MOD-DIS
               ,:RDF-COD-DIV
               ,:RDF-DT-CAMBIO-VLT-DB
                :IND-RDF-DT-CAMBIO-VLT
               ,:RDF-TP-FND
               ,:RDF-DS-RIGA
               ,:RDF-DS-OPER-SQL
               ,:RDF-DS-VER
               ,:RDF-DS-TS-INI-CPTZ
               ,:RDF-DS-TS-END-CPTZ
               ,:RDF-DS-UTENTE
               ,:RDF-DS-STATO-ELAB
               ,:RDF-DT-DIS-CALC-DB
                :IND-RDF-DT-DIS-CALC
               ,:RDF-FL-CALC-DIS
                :IND-RDF-FL-CALC-DIS
               ,:RDF-COMMIS-GEST
                :IND-RDF-COMMIS-GEST
               ,:RDF-NUM-QUO-CDG-FNZ
                :IND-RDF-NUM-QUO-CDG-FNZ
               ,:RDF-NUM-QUO-CDGTOT-FNZ
                :IND-RDF-NUM-QUO-CDGTOT-FNZ
               ,:RDF-COS-RUN-ASSVA-IDC
                :IND-RDF-COS-RUN-ASSVA-IDC
               ,:RDF-FL-SWM-BP2S
                :IND-RDF-FL-SWM-BP2S
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-EFF-RDF CURSOR FOR
              SELECT
                     ID_RICH_DIS_FND
                    ,ID_MOVI_FINRIO
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,COD_FND
                    ,NUM_QUO
                    ,PC
                    ,IMP_MOVTO
                    ,DT_DIS
                    ,COD_TARI
                    ,TP_STAT
                    ,TP_MOD_DIS
                    ,COD_DIV
                    ,DT_CAMBIO_VLT
                    ,TP_FND
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,DT_DIS_CALC
                    ,FL_CALC_DIS
                    ,COMMIS_GEST
                    ,NUM_QUO_CDG_FNZ
                    ,NUM_QUO_CDGTOT_FNZ
                    ,COS_RUN_ASSVA_IDC
                    ,FL_SWM_BP2S
              FROM RICH_DIS_FND
              WHERE     ID_MOVI_FINRIO = :RDF-ID-MOVI-FINRIO
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_RICH_DIS_FND ASC

           END-EXEC.

       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RICH_DIS_FND
                ,ID_MOVI_FINRIO
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,COD_FND
                ,NUM_QUO
                ,PC
                ,IMP_MOVTO
                ,DT_DIS
                ,COD_TARI
                ,TP_STAT
                ,TP_MOD_DIS
                ,COD_DIV
                ,DT_CAMBIO_VLT
                ,TP_FND
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,DT_DIS_CALC
                ,FL_CALC_DIS
                ,COMMIS_GEST
                ,NUM_QUO_CDG_FNZ
                ,NUM_QUO_CDGTOT_FNZ
                ,COS_RUN_ASSVA_IDC
                ,FL_SWM_BP2S
             INTO
                :RDF-ID-RICH-DIS-FND
               ,:RDF-ID-MOVI-FINRIO
               ,:RDF-ID-MOVI-CRZ
               ,:RDF-ID-MOVI-CHIU
                :IND-RDF-ID-MOVI-CHIU
               ,:RDF-DT-INI-EFF-DB
               ,:RDF-DT-END-EFF-DB
               ,:RDF-COD-COMP-ANIA
               ,:RDF-COD-FND
                :IND-RDF-COD-FND
               ,:RDF-NUM-QUO
                :IND-RDF-NUM-QUO
               ,:RDF-PC
                :IND-RDF-PC
               ,:RDF-IMP-MOVTO
                :IND-RDF-IMP-MOVTO
               ,:RDF-DT-DIS-DB
                :IND-RDF-DT-DIS
               ,:RDF-COD-TARI
                :IND-RDF-COD-TARI
               ,:RDF-TP-STAT
                :IND-RDF-TP-STAT
               ,:RDF-TP-MOD-DIS
                :IND-RDF-TP-MOD-DIS
               ,:RDF-COD-DIV
               ,:RDF-DT-CAMBIO-VLT-DB
                :IND-RDF-DT-CAMBIO-VLT
               ,:RDF-TP-FND
               ,:RDF-DS-RIGA
               ,:RDF-DS-OPER-SQL
               ,:RDF-DS-VER
               ,:RDF-DS-TS-INI-CPTZ
               ,:RDF-DS-TS-END-CPTZ
               ,:RDF-DS-UTENTE
               ,:RDF-DS-STATO-ELAB
               ,:RDF-DT-DIS-CALC-DB
                :IND-RDF-DT-DIS-CALC
               ,:RDF-FL-CALC-DIS
                :IND-RDF-FL-CALC-DIS
               ,:RDF-COMMIS-GEST
                :IND-RDF-COMMIS-GEST
               ,:RDF-NUM-QUO-CDG-FNZ
                :IND-RDF-NUM-QUO-CDG-FNZ
               ,:RDF-NUM-QUO-CDGTOT-FNZ
                :IND-RDF-NUM-QUO-CDGTOT-FNZ
               ,:RDF-COS-RUN-ASSVA-IDC
                        :IND-RDF-COS-RUN-ASSVA-IDC
               ,:RDF-FL-SWM-BP2S
                :IND-RDF-FL-SWM-BP2S
             FROM RICH_DIS_FND
             WHERE     ID_MOVI_FINRIO = :RDF-ID-MOVI-FINRIO
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           EXEC SQL
                OPEN C-IDP-EFF-RDF
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           EXEC SQL
                CLOSE C-IDP-EFF-RDF
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           EXEC SQL
                FETCH C-IDP-EFF-RDF
           INTO
                :RDF-ID-RICH-DIS-FND
               ,:RDF-ID-MOVI-FINRIO
               ,:RDF-ID-MOVI-CRZ
               ,:RDF-ID-MOVI-CHIU
                :IND-RDF-ID-MOVI-CHIU
               ,:RDF-DT-INI-EFF-DB
               ,:RDF-DT-END-EFF-DB
               ,:RDF-COD-COMP-ANIA
               ,:RDF-COD-FND
                :IND-RDF-COD-FND
               ,:RDF-NUM-QUO
                :IND-RDF-NUM-QUO
               ,:RDF-PC
                :IND-RDF-PC
               ,:RDF-IMP-MOVTO
                :IND-RDF-IMP-MOVTO
               ,:RDF-DT-DIS-DB
                :IND-RDF-DT-DIS
               ,:RDF-COD-TARI
                :IND-RDF-COD-TARI
               ,:RDF-TP-STAT
                :IND-RDF-TP-STAT
               ,:RDF-TP-MOD-DIS
                :IND-RDF-TP-MOD-DIS
               ,:RDF-COD-DIV
               ,:RDF-DT-CAMBIO-VLT-DB
                :IND-RDF-DT-CAMBIO-VLT
               ,:RDF-TP-FND
               ,:RDF-DS-RIGA
               ,:RDF-DS-OPER-SQL
               ,:RDF-DS-VER
               ,:RDF-DS-TS-INI-CPTZ
               ,:RDF-DS-TS-END-CPTZ
               ,:RDF-DS-UTENTE
               ,:RDF-DS-STATO-ELAB
               ,:RDF-DT-DIS-CALC-DB
                :IND-RDF-DT-DIS-CALC
               ,:RDF-FL-CALC-DIS
                :IND-RDF-FL-CALC-DIS
               ,:RDF-COMMIS-GEST
                :IND-RDF-COMMIS-GEST
               ,:RDF-NUM-QUO-CDG-FNZ
                :IND-RDF-NUM-QUO-CDG-FNZ
               ,:RDF-NUM-QUO-CDGTOT-FNZ
                :IND-RDF-NUM-QUO-CDGTOT-FNZ
               ,:RDF-COS-RUN-ASSVA-IDC
                        :IND-RDF-COS-RUN-ASSVA-IDC
               ,:RDF-FL-SWM-BP2S
                :IND-RDF-FL-SWM-BP2S
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RICH_DIS_FND
                ,ID_MOVI_FINRIO
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,COD_FND
                ,NUM_QUO
                ,PC
                ,IMP_MOVTO
                ,DT_DIS
                ,COD_TARI
                ,TP_STAT
                ,TP_MOD_DIS
                ,COD_DIV
                ,DT_CAMBIO_VLT
                ,TP_FND
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,DT_DIS_CALC
                ,FL_CALC_DIS
                ,COMMIS_GEST
                ,NUM_QUO_CDG_FNZ
                ,NUM_QUO_CDGTOT_FNZ
                ,COS_RUN_ASSVA_IDC
                ,FL_SWM_BP2S
             INTO
                :RDF-ID-RICH-DIS-FND
               ,:RDF-ID-MOVI-FINRIO
               ,:RDF-ID-MOVI-CRZ
               ,:RDF-ID-MOVI-CHIU
                :IND-RDF-ID-MOVI-CHIU
               ,:RDF-DT-INI-EFF-DB
               ,:RDF-DT-END-EFF-DB
               ,:RDF-COD-COMP-ANIA
               ,:RDF-COD-FND
                :IND-RDF-COD-FND
               ,:RDF-NUM-QUO
                :IND-RDF-NUM-QUO
               ,:RDF-PC
                :IND-RDF-PC
               ,:RDF-IMP-MOVTO
                :IND-RDF-IMP-MOVTO
               ,:RDF-DT-DIS-DB
                :IND-RDF-DT-DIS
               ,:RDF-COD-TARI
                :IND-RDF-COD-TARI
               ,:RDF-TP-STAT
                :IND-RDF-TP-STAT
               ,:RDF-TP-MOD-DIS
                :IND-RDF-TP-MOD-DIS
               ,:RDF-COD-DIV
               ,:RDF-DT-CAMBIO-VLT-DB
                :IND-RDF-DT-CAMBIO-VLT
               ,:RDF-TP-FND
               ,:RDF-DS-RIGA
               ,:RDF-DS-OPER-SQL
               ,:RDF-DS-VER
               ,:RDF-DS-TS-INI-CPTZ
               ,:RDF-DS-TS-END-CPTZ
               ,:RDF-DS-UTENTE
               ,:RDF-DS-STATO-ELAB
               ,:RDF-DT-DIS-CALC-DB
                :IND-RDF-DT-DIS-CALC
               ,:RDF-FL-CALC-DIS
                :IND-RDF-FL-CALC-DIS
               ,:RDF-COMMIS-GEST
                :IND-RDF-COMMIS-GEST
               ,:RDF-NUM-QUO-CDG-FNZ
                :IND-RDF-NUM-QUO-CDG-FNZ
               ,:RDF-NUM-QUO-CDGTOT-FNZ
                :IND-RDF-NUM-QUO-CDGTOT-FNZ
               ,:RDF-COS-RUN-ASSVA-IDC
                :IND-RDF-COS-RUN-ASSVA-IDC
               ,:RDF-FL-SWM-BP2S
                :IND-RDF-FL-SWM-BP2S
             FROM RICH_DIS_FND
             WHERE     ID_RICH_DIS_FND = :RDF-ID-RICH-DIS-FND
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-CPZ-RDF CURSOR FOR
              SELECT
                     ID_RICH_DIS_FND
                    ,ID_MOVI_FINRIO
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,COD_FND
                    ,NUM_QUO
                    ,PC
                    ,IMP_MOVTO
                    ,DT_DIS
                    ,COD_TARI
                    ,TP_STAT
                    ,TP_MOD_DIS
                    ,COD_DIV
                    ,DT_CAMBIO_VLT
                    ,TP_FND
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,DT_DIS_CALC
                    ,FL_CALC_DIS
                    ,COMMIS_GEST
                    ,NUM_QUO_CDG_FNZ
                    ,NUM_QUO_CDGTOT_FNZ
                    ,COS_RUN_ASSVA_IDC
                    ,FL_SWM_BP2S
              FROM RICH_DIS_FND
              WHERE     ID_MOVI_FINRIO = :RDF-ID-MOVI-FINRIO
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_RICH_DIS_FND ASC

           END-EXEC.

       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_RICH_DIS_FND
                ,ID_MOVI_FINRIO
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,COD_FND
                ,NUM_QUO
                ,PC
                ,IMP_MOVTO
                ,DT_DIS
                ,COD_TARI
                ,TP_STAT
                ,TP_MOD_DIS
                ,COD_DIV
                ,DT_CAMBIO_VLT
                ,TP_FND
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,DT_DIS_CALC
                ,FL_CALC_DIS
                ,COMMIS_GEST
                ,NUM_QUO_CDG_FNZ
                ,NUM_QUO_CDGTOT_FNZ
                ,COS_RUN_ASSVA_IDC
                ,FL_SWM_BP2S
             INTO
                :RDF-ID-RICH-DIS-FND
               ,:RDF-ID-MOVI-FINRIO
               ,:RDF-ID-MOVI-CRZ
               ,:RDF-ID-MOVI-CHIU
                :IND-RDF-ID-MOVI-CHIU
               ,:RDF-DT-INI-EFF-DB
               ,:RDF-DT-END-EFF-DB
               ,:RDF-COD-COMP-ANIA
               ,:RDF-COD-FND
                :IND-RDF-COD-FND
               ,:RDF-NUM-QUO
                :IND-RDF-NUM-QUO
               ,:RDF-PC
                :IND-RDF-PC
               ,:RDF-IMP-MOVTO
                :IND-RDF-IMP-MOVTO
               ,:RDF-DT-DIS-DB
                :IND-RDF-DT-DIS
               ,:RDF-COD-TARI
                :IND-RDF-COD-TARI
               ,:RDF-TP-STAT
                :IND-RDF-TP-STAT
               ,:RDF-TP-MOD-DIS
                :IND-RDF-TP-MOD-DIS
               ,:RDF-COD-DIV
               ,:RDF-DT-CAMBIO-VLT-DB
                :IND-RDF-DT-CAMBIO-VLT
               ,:RDF-TP-FND
               ,:RDF-DS-RIGA
               ,:RDF-DS-OPER-SQL
               ,:RDF-DS-VER
               ,:RDF-DS-TS-INI-CPTZ
               ,:RDF-DS-TS-END-CPTZ
               ,:RDF-DS-UTENTE
               ,:RDF-DS-STATO-ELAB
               ,:RDF-DT-DIS-CALC-DB
                :IND-RDF-DT-DIS-CALC
               ,:RDF-FL-CALC-DIS
                :IND-RDF-FL-CALC-DIS
               ,:RDF-COMMIS-GEST
                :IND-RDF-COMMIS-GEST
               ,:RDF-NUM-QUO-CDG-FNZ
                :IND-RDF-NUM-QUO-CDG-FNZ
               ,:RDF-NUM-QUO-CDGTOT-FNZ
                :IND-RDF-NUM-QUO-CDGTOT-FNZ
               ,:RDF-COS-RUN-ASSVA-IDC
                        :IND-RDF-COS-RUN-ASSVA-IDC
               ,:RDF-FL-SWM-BP2S
                :IND-RDF-FL-SWM-BP2S
             FROM RICH_DIS_FND
             WHERE     ID_MOVI_FINRIO = :RDF-ID-MOVI-FINRIO
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           EXEC SQL
                OPEN C-IDP-CPZ-RDF
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           EXEC SQL
                CLOSE C-IDP-CPZ-RDF
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           EXEC SQL
                FETCH C-IDP-CPZ-RDF
           INTO
                :RDF-ID-RICH-DIS-FND
               ,:RDF-ID-MOVI-FINRIO
               ,:RDF-ID-MOVI-CRZ
               ,:RDF-ID-MOVI-CHIU
                :IND-RDF-ID-MOVI-CHIU
               ,:RDF-DT-INI-EFF-DB
               ,:RDF-DT-END-EFF-DB
               ,:RDF-COD-COMP-ANIA
               ,:RDF-COD-FND
                :IND-RDF-COD-FND
               ,:RDF-NUM-QUO
                :IND-RDF-NUM-QUO
               ,:RDF-PC
                :IND-RDF-PC
               ,:RDF-IMP-MOVTO
                :IND-RDF-IMP-MOVTO
               ,:RDF-DT-DIS-DB
                :IND-RDF-DT-DIS
               ,:RDF-COD-TARI
                :IND-RDF-COD-TARI
               ,:RDF-TP-STAT
                :IND-RDF-TP-STAT
               ,:RDF-TP-MOD-DIS
                :IND-RDF-TP-MOD-DIS
               ,:RDF-COD-DIV
               ,:RDF-DT-CAMBIO-VLT-DB
                :IND-RDF-DT-CAMBIO-VLT
               ,:RDF-TP-FND
               ,:RDF-DS-RIGA
               ,:RDF-DS-OPER-SQL
               ,:RDF-DS-VER
               ,:RDF-DS-TS-INI-CPTZ
               ,:RDF-DS-TS-END-CPTZ
               ,:RDF-DS-UTENTE
               ,:RDF-DS-STATO-ELAB
               ,:RDF-DT-DIS-CALC-DB
                :IND-RDF-DT-DIS-CALC
               ,:RDF-FL-CALC-DIS
                :IND-RDF-FL-CALC-DIS
               ,:RDF-COMMIS-GEST
                :IND-RDF-COMMIS-GEST
               ,:RDF-NUM-QUO-CDG-FNZ
                :IND-RDF-NUM-QUO-CDG-FNZ
               ,:RDF-NUM-QUO-CDGTOT-FNZ
                :IND-RDF-NUM-QUO-CDGTOT-FNZ
               ,:RDF-COS-RUN-ASSVA-IDC
                        :IND-RDF-COS-RUN-ASSVA-IDC
               ,:RDF-FL-SWM-BP2S
                :IND-RDF-FL-SWM-BP2S
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-RDF-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO RDF-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-RDF-COD-FND = -1
              MOVE HIGH-VALUES TO RDF-COD-FND-NULL
           END-IF
           IF IND-RDF-NUM-QUO = -1
              MOVE HIGH-VALUES TO RDF-NUM-QUO-NULL
           END-IF
           IF IND-RDF-PC = -1
              MOVE HIGH-VALUES TO RDF-PC-NULL
           END-IF
           IF IND-RDF-IMP-MOVTO = -1
              MOVE HIGH-VALUES TO RDF-IMP-MOVTO-NULL
           END-IF
           IF IND-RDF-DT-DIS = -1
              MOVE HIGH-VALUES TO RDF-DT-DIS-NULL
           END-IF
           IF IND-RDF-COD-TARI = -1
              MOVE HIGH-VALUES TO RDF-COD-TARI-NULL
           END-IF
           IF IND-RDF-TP-STAT = -1
              MOVE HIGH-VALUES TO RDF-TP-STAT-NULL
           END-IF
           IF IND-RDF-TP-MOD-DIS = -1
              MOVE HIGH-VALUES TO RDF-TP-MOD-DIS-NULL
           END-IF
           IF IND-RDF-DT-CAMBIO-VLT = -1
              MOVE HIGH-VALUES TO RDF-DT-CAMBIO-VLT-NULL
           END-IF
           IF IND-RDF-DT-DIS-CALC = -1
              MOVE HIGH-VALUES TO RDF-DT-DIS-CALC-NULL
           END-IF
           IF IND-RDF-FL-CALC-DIS = -1
              MOVE HIGH-VALUES TO RDF-FL-CALC-DIS-NULL
           END-IF
           IF IND-RDF-COMMIS-GEST = -1
              MOVE HIGH-VALUES TO RDF-COMMIS-GEST-NULL
           END-IF
           IF IND-RDF-NUM-QUO-CDG-FNZ = -1
              MOVE HIGH-VALUES TO RDF-NUM-QUO-CDG-FNZ-NULL
           END-IF
           IF IND-RDF-NUM-QUO-CDGTOT-FNZ = -1
              MOVE HIGH-VALUES TO RDF-NUM-QUO-CDGTOT-FNZ-NULL
           END-IF
           IF IND-RDF-COS-RUN-ASSVA-IDC = -1
              MOVE HIGH-VALUES TO RDF-COS-RUN-ASSVA-IDC-NULL
           END-IF
           IF IND-RDF-FL-SWM-BP2S = -1
              MOVE HIGH-VALUES TO RDF-FL-SWM-BP2S-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO RDF-DS-OPER-SQL
           MOVE 0                   TO RDF-DS-VER
           MOVE IDSV0003-USER-NAME TO RDF-DS-UTENTE
           MOVE '1'                   TO RDF-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO RDF-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO RDF-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF RDF-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-RDF-ID-MOVI-CHIU
           END-IF
           IF RDF-COD-FND-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-COD-FND
           ELSE
              MOVE 0 TO IND-RDF-COD-FND
           END-IF
           IF RDF-NUM-QUO-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-NUM-QUO
           ELSE
              MOVE 0 TO IND-RDF-NUM-QUO
           END-IF
           IF RDF-PC-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-PC
           ELSE
              MOVE 0 TO IND-RDF-PC
           END-IF
           IF RDF-IMP-MOVTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-IMP-MOVTO
           ELSE
              MOVE 0 TO IND-RDF-IMP-MOVTO
           END-IF
           IF RDF-DT-DIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-DT-DIS
           ELSE
              MOVE 0 TO IND-RDF-DT-DIS
           END-IF
           IF RDF-COD-TARI-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-COD-TARI
           ELSE
              MOVE 0 TO IND-RDF-COD-TARI
           END-IF
           IF RDF-TP-STAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-TP-STAT
           ELSE
              MOVE 0 TO IND-RDF-TP-STAT
           END-IF
           IF RDF-TP-MOD-DIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-TP-MOD-DIS
           ELSE
              MOVE 0 TO IND-RDF-TP-MOD-DIS
           END-IF
           IF RDF-DT-CAMBIO-VLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-DT-CAMBIO-VLT
           ELSE
              MOVE 0 TO IND-RDF-DT-CAMBIO-VLT
           END-IF
           IF RDF-DT-DIS-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-DT-DIS-CALC
           ELSE
              MOVE 0 TO IND-RDF-DT-DIS-CALC
           END-IF
           IF RDF-FL-CALC-DIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-FL-CALC-DIS
           ELSE
              MOVE 0 TO IND-RDF-FL-CALC-DIS
           END-IF
           IF RDF-COMMIS-GEST-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-COMMIS-GEST
           ELSE
              MOVE 0 TO IND-RDF-COMMIS-GEST
           END-IF
           IF RDF-NUM-QUO-CDG-FNZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-NUM-QUO-CDG-FNZ
           ELSE
              MOVE 0 TO IND-RDF-NUM-QUO-CDG-FNZ
           END-IF
           IF RDF-NUM-QUO-CDGTOT-FNZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-NUM-QUO-CDGTOT-FNZ
           ELSE
              MOVE 0 TO IND-RDF-NUM-QUO-CDGTOT-FNZ
           END-IF
           IF RDF-COS-RUN-ASSVA-IDC-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-COS-RUN-ASSVA-IDC
           ELSE
              MOVE 0 TO IND-RDF-COS-RUN-ASSVA-IDC
           END-IF
           IF RDF-FL-SWM-BP2S-NULL = HIGH-VALUES
              MOVE -1 TO IND-RDF-FL-SWM-BP2S
           ELSE
              MOVE 0 TO IND-RDF-FL-SWM-BP2S
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : RDF-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE RICH-DIS-FND TO WS-BUFFER-TABLE.

           MOVE RDF-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO RDF-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO RDF-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO RDF-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO RDF-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO RDF-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO RDF-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO RDF-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE RICH-DIS-FND TO WS-BUFFER-TABLE.

           MOVE RDF-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO RICH-DIS-FND.

           MOVE WS-ID-MOVI-CRZ  TO RDF-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO RDF-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO RDF-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO RDF-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO RDF-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO RDF-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO RDF-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE RDF-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO RDF-DT-INI-EFF-DB
           MOVE RDF-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO RDF-DT-END-EFF-DB
           IF IND-RDF-DT-DIS = 0
               MOVE RDF-DT-DIS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO RDF-DT-DIS-DB
           END-IF
           IF IND-RDF-DT-CAMBIO-VLT = 0
               MOVE RDF-DT-CAMBIO-VLT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO RDF-DT-CAMBIO-VLT-DB
           END-IF
           IF IND-RDF-DT-DIS-CALC = 0
               MOVE RDF-DT-DIS-CALC TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO RDF-DT-DIS-CALC-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE RDF-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO RDF-DT-INI-EFF
           MOVE RDF-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO RDF-DT-END-EFF
           IF IND-RDF-DT-DIS = 0
               MOVE RDF-DT-DIS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO RDF-DT-DIS
           END-IF
           IF IND-RDF-DT-CAMBIO-VLT = 0
               MOVE RDF-DT-CAMBIO-VLT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO RDF-DT-CAMBIO-VLT
           END-IF
           IF IND-RDF-DT-DIS-CALC = 0
               MOVE RDF-DT-DIS-CALC-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO RDF-DT-DIS-CALC
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

      ******************************************************************
      **                                                        ********
      **    PORTAFOGLIO VITA ITALIA  VER 1.0                    ********
      **                                                        ********
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         ISPS0580.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2012.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *  PROGRAMMA ..... ISPS0580
      *  TIPOLOGIA...... Servizio Actuator
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... Programma di chiamata ai servizi Actuator
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    VARIABILI DI WORKING
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'ISPS0580'.

      *----------------------------------------------------------------*
      *    PUNTATORE AREA PRODOTTO
      *----------------------------------------------------------------*
       01  MQ01-ADDRESS.
           05 WS-ADDRESS USAGE POINTER.

      *----------------------------------------------------------------*
      *    DEFINIZIONE DI INDICI                                       *
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-TAB-ERR                    PIC S9(04) COMP VALUE 0.
           03 IX-DT-CALC                    PIC S9(04) COMP VALUE 0.
           03 IX-TAB-OPR                    PIC S9(04) COMP VALUE 0.
           03 IX-TAB-PRM                    PIC S9(04) COMP VALUE 0.
      *----------------------------------------------------------------*
      * CHIAMATA AL SERVIZIO INFRASTRUTTURALE PER COLLEGAMENTO AL
      * SERVIZI DI PRODOTTO
      *----------------------------------------------------------------*
           COPY IJCCMQ01.

      *----------------------------------------------------------------*
      *    COPY VARIBILI PER LA GESTIONE ERRORI                        *
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
           COPY IEAV9903.

      *----------------------------------------------------------------*
      * MODULI ACTUATOR RICHIAMATI                                     *
      *----------------------------------------------------------------*
       01  INTERF-MQSERIES                    PIC X(8) VALUE 'IJCSMQ01'.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.

       01  WCOM-AREA-STATI.
           COPY LCCC0001                 REPLACING ==(SF)== BY ==WCOM==.

       01  AREA-IO-ISPS0580.
           COPY ISPC0580.

      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                AREA-IO-ISPS0580.

      *---------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      * OPERAZIONI INIZIALI                                            *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           CONTINUE.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *  ELABORAZIONE                                                  *
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           PERFORM S1050-PREP-AREA-ISPS0580
              THRU EX-S1050.

           PERFORM S1100-CALL-ISPS0580
              THRU EX-S1100.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA SERVIZIO
      *----------------------------------------------------------------*
       S1050-PREP-AREA-ISPS0580.

           MOVE LOW-VALUES               TO AREA-PRODUCT-SERVICES.

           MOVE IDSV0001-MODALITA-ESECUTIVA
                                         TO IJCCMQ01-MODALITA-ESECUTIVA.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                         TO ISPC0580-COD-COMPAGNIA-ANIA.

           SET IJCCMQ01-ESITO-OK         TO TRUE.

           MOVE 'ISPS0580'               TO IJCCMQ01-JAVA-SERVICE-NAME.

       EX-S1050.
           EXIT.

      *----------------------------------------------------------------*
      *  CHIAMATA AL SERVIZIO
      *----------------------------------------------------------------*
       S1100-CALL-ISPS0580.

           MOVE LENGTH OF AREA-IO-ISPS0580
                                    TO IJCCMQ01-LENGTH-DATI-SERVIZIO.

           MOVE AREA-IO-ISPS0580
                                    TO IJCCMQ01-AREA-DATI-SERVIZIO.

           SET WS-ADDRESS           TO ADDRESS OF AREA-PRODUCT-SERVICES.

           MOVE IDSV0001-LIVELLO-DEBUG   TO IJCCMQ00-LIVELLO-DEBUG
           MOVE IDSV0001-AREA-ADDRESSES  TO IJCCMQ01-AREA-ADDRESSES
           MOVE IDSV0001-USER-NAME       TO IJCCMQ01-USER-NAME

           CALL INTERF-MQSERIES USING MQ01-ADDRESS
                                      AREA-PRODUCT-SERVICES
           ON EXCEPTION
              MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'ISPS0580'       TO CALL-DESC
              MOVE 'S1100-CALL-ISPS0580'
                                    TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

      *--> RETURNCODE ACTUATOR
           IF IJCCMQ01-ESITO-OK
              MOVE IJCCMQ01-AREA-DATI-SERVIZIO
                                    TO AREA-IO-ISPS0580

              MOVE ISPC0580-AREA-ERRORI
                                    TO ISPC0001-AREA-ERRORI

              PERFORM S0320-OUTPUT-PRODOTTO
                 THRU EX-S0320-OUTPUT-PRODOTTO
           ELSE
      *--> GESTIONE ERRORI AREA IJCSMQ01
              SET IDSV0001-ESITO-KO TO TRUE
              MOVE IJCCMQ01-MAX-ELE-ERRORI
                                    TO IDSV0001-MAX-ELE-ERRORI
              PERFORM VARYING IX-TAB-ERR FROM 1 BY 1
                        UNTIL IX-TAB-ERR > IJCCMQ01-MAX-ELE-ERRORI
                  MOVE IJCCMQ01-COD-ERRORE(IX-TAB-ERR)
                                    TO IDSV0001-COD-ERRORE(IX-TAB-ERR)
                  MOVE IJCCMQ01-DESC-ERRORE(IX-TAB-ERR)
                                    TO IDSV0001-DESC-ERRORE(IX-TAB-ERR)
                  MOVE IJCCMQ01-LIV-GRAVITA-BE(IX-TAB-ERR)
                                  TO IDSV0001-LIV-GRAVITA-BE(IX-TAB-ERR)
              END-PERFORM
           END-IF.

       EX-S1100.
           EXIT.

      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
           COPY IERP9903.

      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0005.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0005
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CALCOLO VARIABILE SESSO ASS SX,SY,SZ
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0005'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-APPOGGIO.
      *
           03 WRAN-AREA-RAPP-ANAG.
              04 WRAN-ELE-RAPP-ANAG-MAX  PIC S9(04) COMP.
                COPY LCCVRANA REPLACING   ==(SF)==  BY ==WRAN==.
              COPY LCCVRAN1              REPLACING ==(SF)== BY ==WRAN==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

       01  WK-FIND-LETTO                     PIC X(001).
           88 WK-LETTO-SI                    VALUE 'S'.
           88 WK-LETTO-NO                    VALUE 'N'.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                       PIC S9(04) COMP.
           03 IX-TAB-RAN                      PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0005.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0005.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI.
           INITIALIZE                        AREA-APPOGGIO.
           INITIALIZE                        IVVC0213-TAB-OUTPUT.
      *
           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.
      *
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.
      *
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
               VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           IF IVVC0213-TP-LIVELLO = 'G'
              PERFORM S2027-RAN-GAR          THRU S2027-EX
           ELSE
              PERFORM S3027-RAN-POL          THRU S3027-EX
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  RAPP ANA GAR
      *----------------------------------------------------------------*
       S2027-RAN-GAR.
      *

           IF WRAN-SEX(IVVC0213-IX-TABB) = 'M'
              MOVE 0      TO IVVC0213-VAL-IMP-O
           ELSE
              MOVE 1      TO IVVC0213-VAL-IMP-O
           END-IF.

      *
       S2027-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORA  RAPP ANA GAR
      *----------------------------------------------------------------*
       S3027-RAN-POL.
      *
           SET WK-LETTO-NO                TO TRUE.
      *
           PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
             UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX
                OR WK-LETTO-SI
                IF WRAN-TP-OGG(IX-TAB-RAN) = 'PO'
                  IF IVVC0213-ID-POL-GAR
                   = WRAN-ID-OGG(IX-TAB-RAN)
                     SET WK-LETTO-SI              TO TRUE
                     IF WRAN-SEX(IX-TAB-RAN) = 'M'
                        MOVE 0      TO IVVC0213-VAL-IMP-O
                     ELSE
                        MOVE 1      TO IVVC0213-VAL-IMP-O
                     END-IF
                  END-IF
                END-IF
           END-PERFORM.
      *
       S3027-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-RAPP-ANAG
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO WRAN-AREA-RAPP-ANAG
           END-IF.
       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS2750.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2013.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS2750
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... IMPOSTA DI BOLLO
      *                  CALCOLO DELLA VARIABILE B_3112_i (IMP3112POL)
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS2750'.
       01  LDBS6040                         PIC X(008) VALUE 'LDBS6040'.
       01  LDBSE590                         PIC X(008) VALUE 'LDBSE590'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DATA                         PIC 9(08).
       01  WK-DATA-SEPARATA REDEFINES WK-DATA.
           03 WK-ANNO                  PIC 9(04).
           03 WK-MESE                  PIC 9(02).
           03 WK-GG                    PIC 9(02).

       01 WK-DATA-INIZIO.
           03 WK-ANNO-INI              PIC 9(04).
           03 WK-MESE-INI              PIC 9(02).
           03 WK-GG-INI                PIC 9(02).

       01 WK-DATA-INIZIO-D             REDEFINES
             WK-DATA-INIZIO               PIC S9(8)V COMP-3.

       01 WK-DATA-FINE.
           03 WK-ANNO-FINE              PIC 9(04).
           03 WK-MESE-FINE              PIC 9(02).
           03 WK-GG-FINE                PIC 9(02).

       01 WK-DATA-FINE-D             REDEFINES
             WK-DATA-FINE               PIC S9(8)V COMP-3.

       01 FLAG-COMUN-TROV                    PIC X(01).
           88 COMUN-TROV-SI                  VALUE 'S'.
           88 COMUN-TROV-NO                  VALUE 'N'.

       01 FLAG-CUR-MOV                       PIC X(01).
           88 INIT-CUR-MOV                   VALUE 'S'.
           88 FINE-CUR-MOV                   VALUE 'N'.

       01 FLAG-ULTIMA-LETTURA               PIC X(01).
          88 SI-ULTIMA-LETTURA              VALUE 'S'.
          88 NO-ULTIMA-LETTURA              VALUE 'N'.

       01 FLAG-FINE-LETTURA-P58               PIC X(01).
          88 FINE-LETT-P58-SI               VALUE 'S'.
          88 FINE-LETT-P58-NO               VALUE 'N'.

14236  01  WK-DATA-MAX                     PIC S9(08).
       01  WK-VAR-MOVI-COMUN.
           05 WK-ID-MOVI-COMUN             PIC S9(09) COMP-3.
           05 WK-DATA-EFF-PREC             PIC S9(08) COMP-3.
           05 WK-DATA-CPTZ-PREC            PIC S9(18) COMP-3.
           05 WK-DATA-EFF-RIP              PIC S9(08) COMP-3.
           05 WK-DATA-CPTZ-RIP             PIC S9(18) COMP-3.

        01 IX-TAB-P58                      PIC 9(09) VALUE 0.
      *---------------------------------------------------------------*
      *  LISTA TABELLE E TIPOLOGICHE
      *---------------------------------------------------------------*
           COPY IDBVMOV1.
           COPY IDBVP581.
10819      COPY LCCVXMVZ.
10819      COPY LCCVXMV2.
10819      COPY LCCVXMV3.
10819      COPY LCCVXMV5.
10819      COPY LCCVXMV6.
      *---------------------------------------------------------------*
      *  LISTA MOVIMENTI IMPLEMENTATI LCCC0006
      *---------------------------------------------------------------*
           COPY LCCC0006.

      *----------------------------------------------------------------*
      *--> AREA IMPOSTA DI BOLLO
      *----------------------------------------------------------------*
       01 AREA-IO-P58.
          03 DP58-AREA-P58.
             04 DP58-ELE-P58-MAX         PIC S9(04) COMP.
52921 *      04 DP58-TAB-P58             OCCURS 50.
52921        04 DP58-TAB-P58             OCCURS 75.
             COPY LCCVP581               REPLACING ==(SF)== BY ==DP58==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIABILI PER LA GESTIONE ERRORI                     *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-ISO                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS2750.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS2750.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT
                                             WK-VAR-MOVI-COMUN.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           SET COMUN-TROV-NO                 TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-P58.


           PERFORM VERIFICA-MOVIMENTO
              THRU VERIFICA-MOVIMENTO-EX

      *--> PERFORM DI CALCOLO ANNO CORRENTE
           IF  IDSV0003-SUCCESSFUL-RC
      *    AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1150-CALCOLO-ANNO
                  THRU S1150-CALCOLO-ANNO-EX
           END-IF.


      *--> PERFORM DI CALCOLO DELL'IMPORTO
           IF  IDSV0003-SUCCESSFUL-RC
      *    AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1200-CALCOLO-IMPORTO
                  THRU S1200-CALCOLO-IMPORTO-EX
           END-IF.


       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICA MOVIMENTO
      *----------------------------------------------------------------*
       VERIFICA-MOVIMENTO.

      *--> SE CI TROVIAMO IN FASE DI LIQUIDAZIONE DEVO RECUPERARE LE
      *--> IMMAGINI IN FASE DI COMUNICAZIONE
NEW   *    MOVE IVVC0213-TIPO-MOVIMENTO
NEW   *      TO WS-MOVIMENTO
NEW        MOVE IVVC0213-TIPO-MOVI-ORIG
NEW          TO WS-MOVIMENTO

NEW        IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
NEW           LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND OR
10819         LIQUI-RPP-TAKE-PROFIT
10819X     OR LIQUI-RISTOT-INCAPIENZA
10819X     OR LIQUI-RPP-REDDITO-PROGR
10819      OR LIQUI-RPP-BENEFICIO-CONTR
FNZS2      OR LIQUI-RISTOT-INCAP
      *--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
NEW           PERFORM RECUP-MOVI-COMUN
NEW              THRU RECUP-MOVI-COMUN-EX

              IF COMUN-TROV-SI
                 MOVE IDSV0003-DATA-COMPETENZA
                   TO WK-DATA-CPTZ-RIP

                 MOVE IDSV0003-DATA-INIZIO-EFFETTO
                   TO WK-DATA-EFF-RIP

                 MOVE WK-DATA-CPTZ-PREC
                   TO IDSV0003-DATA-COMPETENZA

                 MOVE WK-DATA-EFF-PREC
                   TO IDSV0003-DATA-INIZIO-EFFETTO
                      IDSV0003-DATA-FINE-EFFETTO

                 PERFORM VARYING IX-TAB-P58 FROM 1 BY 1
52921 *             UNTIL IX-TAB-P58 > 10
52921               UNTIL IX-TAB-P58 > 75
                    PERFORM INIZIA-TOT-P58
                       THRU INIZIA-TOT-P58-EX
                 END-PERFORM

                 PERFORM LETTURA-IMPST-BOLLO
                    THRU LETTURA-IMPST-BOLLO-EX
14236 *-->       ESEGUO UNA VERIFICA SE TRA COMUNICAZIONE E LIQ
14236 *-->       E' PRESENTE UN NUOVO BOLLO A CAVALLO
14236            MOVE WK-DATA-CPTZ-RIP
14236              TO IDSV0003-DATA-COMPETENZA

14236            MOVE WK-DATA-EFF-PREC
14236              TO IDSV0003-DATA-INIZIO-EFFETTO
14236                 IDSV0003-DATA-FINE-EFFETTO

14236            PERFORM LETTURA-IMPST-BOLLO-2
14236               THRU LETTURA-IMPST-BOLLO-2-EX
14236 *-->
NEW           ELSE
      *-->       ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *-->       RISPETTIVE AREE DCLGEN IN WORKING
                 PERFORM S1100-VALORIZZA-DCLGEN
                    THRU S1100-VALORIZZA-DCLGEN-EX
                 VARYING IX-DCLGEN FROM 1 BY 1
                   UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                      OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                         SPACES OR LOW-VALUE OR HIGH-VALUE
53181            IF  DP58-ELE-P58-MAX = 0
53181            AND LIQUI-SCAPOL
53181                PERFORM LETTURA-IMPST-BOLLO
53181                   THRU LETTURA-IMPST-BOLLO-EX
53181            END-IF
NEW           END-IF
NEW        ELSE

      *-->    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *-->    RISPETTIVE AREE DCLGEN IN WORKING
              PERFORM S1100-VALORIZZA-DCLGEN
                 THRU S1100-VALORIZZA-DCLGEN-EX
              VARYING IX-DCLGEN FROM 1 BY 1
                UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                   OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                      SPACES OR LOW-VALUE OR HIGH-VALUE
53181         IF  DP58-ELE-P58-MAX = 0
53181         AND RISTO-INDIVI
53181             PERFORM LETTURA-IMPST-BOLLO
53181                THRU LETTURA-IMPST-BOLLO-EX
53181         END-IF
           END-IF.

       VERIFICA-MOVIMENTO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Recupero il movimento di comunicazione
      *----------------------------------------------------------------*
       RECUP-MOVI-COMUN.

           SET INIT-CUR-MOV               TO TRUE
           SET COMUN-TROV-NO              TO TRUE

           INITIALIZE MOVI

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
           SET  NO-ULTIMA-LETTURA         TO TRUE

      *
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR FINE-CUR-MOV
                      OR COMUN-TROV-SI

              INITIALIZE MOVI
      *
              MOVE IVVC0213-ID-POLIZZA    TO MOV-ID-OGG
              MOVE 'PO'                   TO MOV-TP-OGG

              SET IDSV0003-TRATT-SENZA-STOR   TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSV0003-WHERE-CONDITION TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL TO TRUE

              CALL  LDBS6040   USING IDSV0003 MOVI
                ON EXCEPTION
                   MOVE LDBS6040          TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                     TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER   TO TRUE
              END-CALL


                IF IDSV0003-SUCCESSFUL-RC

                    EVALUATE TRUE
                        WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA
      *-->     LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
      *-->     BOLLO IN INPUT
                          SET FINE-CUR-MOV TO TRUE

                        WHEN IDSV0003-SUCCESSFUL-SQL
      *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
      *                   MOVE IDSV0003-BUFFER-DATI TO MOVI
      *      TROVO IL MOVIMENTO DI COMUNICAZIONE RISCATTO PARZIALE
                          MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                          IF COMUN-RISPAR-IND
                          OR RISTO-INDIVI
                          OR VARIA-OPZION
                          OR SINIS-INDIVI
                          OR RECES-INDIVI
10819                     OR RPP-TAKE-PROFIT
10819X                    OR COMUN-RISTOT-INCAPIENZA
10819X                    OR RPP-REDDITO-PROGRAMMATO
10819                     OR RPP-BENEFICIO-CONTR
FNZS2                     OR COMUN-RISTOT-INCAP
                             MOVE MOV-ID-MOVI    TO WK-ID-MOVI-COMUN
                             SET SI-ULTIMA-LETTURA  TO TRUE
                             MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                             COMPUTE WK-DATA-CPTZ-PREC =  MOV-DS-TS-CPTZ
                                                       - 1
                             SET COMUN-TROV-SI   TO TRUE
                             PERFORM CLOSE-MOVI
                                THRU CLOSE-MOVI-EX
                             SET FINE-CUR-MOV   TO TRUE
                          ELSE
                             SET IDSV0003-FETCH-NEXT   TO TRUE
                          END-IF
                        WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                             MOVE LDBS6040
                               TO IDSV0003-COD-SERVIZIO-BE
                             MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                               TO IDSV0003-DESCRIZ-ERR-DB2
                             SET IDSV0003-INVALID-OPER   TO TRUE
                    END-EVALUATE
                 ELSE
      *--> GESTIRE ERRORE
                    MOVE LDBS6040
                      TO IDSV0003-COD-SERVIZIO-BE
                    MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                      TO IDSV0003-DESCRIZ-ERR-DB2
                    SET IDSV0003-INVALID-OPER   TO TRUE
                 END-IF
           END-PERFORM.
           MOVE IDSV0003-TIPO-MOVIMENTO TO WS-MOVIMENTO.

       RECUP-MOVI-COMUN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
      *----------------------------------------------------------------*
       CLOSE-MOVI.

           SET IDSV0003-CLOSE-CURSOR            TO TRUE.

           CALL LDBS6040 USING IDSV0003 MOVI
           ON EXCEPTION
              MOVE LDBS6040
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CALL LDBS6040 CLOSE'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER TO TRUE
           END-CALL.

           IF NOT IDSV0003-SUCCESSFUL-RC
           OR NOT IDSV0003-SUCCESSFUL-SQL
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CLOSE CURSORE LDBS6040'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER
                TO TRUE
           END-IF.
       CLOSE-MOVI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RECUPERO IMPOSTA DI BOLLO DA MOVIMENTO DI COMUNICAZIONE
      *----------------------------------------------------------------*
       LETTURA-IMPST-BOLLO.

           SET FINE-LETT-P58-NO           TO TRUE

           INITIALIZE IMPST-BOLLO

14236      MOVE 0 TO WK-DATA-MAX
           MOVE 0 TO DP58-ELE-P58-MAX
           MOVE 0 TO IX-TAB-P58

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

      *
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR FINE-LETT-P58-SI

              INITIALIZE IMPST-BOLLO
      *
              MOVE IVVC0213-ID-POLIZZA    TO P58-ID-POLI

              SET IDSV0003-TRATT-X-COMPETENZA TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSV0003-WHERE-CONDITION TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL TO TRUE

              CALL  LDBSE590   USING IDSV0003 IMPST-BOLLO
                ON EXCEPTION
                   MOVE LDBSE590          TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
                     TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER   TO TRUE
              END-CALL


                IF IDSV0003-SUCCESSFUL-RC

                    EVALUATE TRUE
                        WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA

      *                   IF IDSV0003-FETCH-FIRST
      *                      MOVE LDBSE590
      *                        TO IDSV0003-COD-SERVIZIO-BE
      *                      MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
      *                        TO IDSV0003-DESCRIZ-ERR-DB2
      *                      SET IDSV0003-INVALID-OPER   TO TRUE
      *                   END-IF
                          SET FINE-LETT-P58-SI TO TRUE

                        WHEN IDSV0003-SUCCESSFUL-SQL
      *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
14236                        IF WK-DATA-MAX > P58-DT-END-CALC
14236                           CONTINUE
14236                        ELSE
14236                           MOVE P58-DT-END-CALC
14236                             TO WK-DATA-MAX
14236                        END-IF
                             ADD 1 TO DP58-ELE-P58-MAX
                                      IX-TAB-P58

                             PERFORM VALORIZZA-OUTPUT-P58
                                THRU VALORIZZA-OUTPUT-P58-EX

                             SET IDSV0003-FETCH-NEXT   TO TRUE

                        WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                             MOVE LDBSE590
                               TO IDSV0003-COD-SERVIZIO-BE
                             MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
                               TO IDSV0003-DESCRIZ-ERR-DB2
                             SET IDSV0003-INVALID-OPER   TO TRUE
                             SET FINE-LETT-P58-SI TO TRUE
                    END-EVALUATE
                 ELSE
      *--> GESTIRE ERRORE
                    MOVE LDBSE590
                      TO IDSV0003-COD-SERVIZIO-BE
                    MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
                      TO IDSV0003-DESCRIZ-ERR-DB2
                    SET IDSV0003-INVALID-OPER   TO TRUE
                    SET FINE-LETT-P58-SI TO TRUE
                 END-IF
           END-PERFORM.

       LETTURA-IMPST-BOLLO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-IMPOSTA-BOLLO
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DP58-AREA-P58
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CALCOLO ANNO
      *----------------------------------------------------------------*
       S1150-CALCOLO-ANNO.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
             TO WK-DATA-FINE

           MOVE IVVC0213-DT-DECOR
             TO WK-DATA-INIZIO-D.

       S1150-CALCOLO-ANNO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CALCOLO IMPORTO
      *----------------------------------------------------------------*
       S1200-CALCOLO-IMPORTO.

           MOVE 0   TO IVVC0213-VAL-IMP-O

           PERFORM VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > DP58-ELE-P58-MAX

             IF DP58-TP-CAUS-BOLLO(IX-DCLGEN)  = 'AN'
                AND DP58-DT-END-CALC(IX-DCLGEN) >= WK-DATA-INIZIO-D
                AND DP58-DT-END-CALC(IX-DCLGEN) <= WK-DATA-FINE-D

                COMPUTE IVVC0213-VAL-IMP-O = IVVC0213-VAL-IMP-O
                                    + DP58-IMPST-BOLLO-TOT-R(IX-DCLGEN)

             END-IF

           END-PERFORM.
      *
       S1200-CALCOLO-IMPORTO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           MOVE SPACES                     TO IVVC0213-VAL-STR-O.
           MOVE 0                          TO IVVC0213-VAL-PERC-O.
NEW   *    IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
NEW   *       LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND
NEW        IF COMUN-TROV-SI
              MOVE WK-DATA-CPTZ-RIP
                TO IDSV0003-DATA-COMPETENZA

              MOVE WK-DATA-EFF-RIP
                TO IDSV0003-DATA-INIZIO-EFFETTO
                   IDSV0003-DATA-FINE-EFFETTO
           END-IF
           GOBACK.

       EX-S9000.
           EXIT.
14236 *----------------------------------------------------------------*
14236 *    RECUPERO IMPOSTA DI BOLLO A CAVALLO TRA COM E LIQ
14236 *----------------------------------------------------------------*
14236  LETTURA-IMPST-BOLLO-2.

           SET FINE-LETT-P58-NO           TO TRUE

           INITIALIZE IMPST-BOLLO

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

      *
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR FINE-LETT-P58-SI

              INITIALIZE IMPST-BOLLO
      *
              MOVE IVVC0213-ID-POLIZZA    TO P58-ID-POLI

              SET IDSV0003-TRATT-X-COMPETENZA TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSV0003-WHERE-CONDITION TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL TO TRUE

              CALL  LDBSE590   USING IDSV0003 IMPST-BOLLO
                ON EXCEPTION
                   MOVE LDBSE590          TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
                     TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER   TO TRUE
              END-CALL


                IF IDSV0003-SUCCESSFUL-RC

                    EVALUATE TRUE
                        WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA

                          SET FINE-LETT-P58-SI TO TRUE

                        WHEN IDSV0003-SUCCESSFUL-SQL
      *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                             IF P58-TP-CAUS-BOLLO ='AN'
      *--> PRENDO SOLO I BOLLI PRESENTI CON DATA MAGGIORE DI
      *--> QUELLI GIA' PRESI DALL'ACCESSO PRECEDENTE
                                IF P58-DT-END-CALC > WK-DATA-MAX
                                   ADD 1 TO DP58-ELE-P58-MAX
                                         IX-TAB-P58

                                   PERFORM VALORIZZA-OUTPUT-P58
                                      THRU VALORIZZA-OUTPUT-P58-EX
                                END-IF
                             END-IF
                             SET IDSV0003-FETCH-NEXT   TO TRUE

                        WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                             MOVE LDBSE590
                               TO IDSV0003-COD-SERVIZIO-BE
                             MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
                               TO IDSV0003-DESCRIZ-ERR-DB2
                             SET IDSV0003-INVALID-OPER   TO TRUE
                             SET FINE-LETT-P58-SI TO TRUE
                    END-EVALUATE
                 ELSE
      *--> GESTIRE ERRORE
                    MOVE LDBSE590
                      TO IDSV0003-COD-SERVIZIO-BE
                    MOVE 'CALL-LDBSE590 ERRORE CHIAMATA'
                      TO IDSV0003-DESCRIZ-ERR-DB2
                    SET IDSV0003-INVALID-OPER   TO TRUE
                    SET FINE-LETT-P58-SI TO TRUE
                 END-IF
           END-PERFORM.

14236  LETTURA-IMPST-BOLLO-2-EX.
14236      EXIT.
      *----------------------------------------------------------------*
      *   COPY IMPOSTA DI BOLLO
      *----------------------------------------------------------------*

            COPY LCCVP583               REPLACING ==(SF)== BY ==DP58==.
            COPY LCCVP584               REPLACING ==(SF)== BY ==DP58==.

      *****************************************************************
      *                                                               *
      *                   IDENTIFICATION DIVISION                     *
      *                                                               *
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IEAS9900.
       AUTHOR.        ACCENTURE.
       DATE-WRITTEN.  27/10/2006.

      *****************************************************************
      *                                                               *
      *    NOME :           IEAS9900                                  *
      *    TIPO :                                                     *
      *    DESCRIZIONE :    RICERCA GRAVITA' ERRORE                   *
      *                                                               *
      *    AREE DI PASSAGGIO DATI                                     *
      *                                                               *
      *    DATI DI INPUT/OUTPUT : IEAI9901/IEAO9901                   *
      *                                                               *
      *****************************************************************
      *                      LOG MODIFICHE                            *
      *****************************************************************
      *                                                               *
      * CODICE MODIF.            AUTORE          DATA                 *
      * ---------------  **   -----------   **   --/--/----           *
      *                                                               *
      * DESCRZIONE: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX           *
      *             XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX           *
      *                                                               *
      *****************************************************************
      *****************************************************************
      *                                                               *
      *                   ENVIRONMENT  DIVISION                       *
      *                                                               *
      *****************************************************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
      *    NULL-IND     Carattere usato per indicare campi NULL;
           SYMBOLIC CHARACTERS
              NULL-IND         IS 256
           DECIMAL-POINT IS COMMA.


      *****************************************************************
      *                                                               *
      *                   DATA  DIVISION                              *
      *                                                               *
      *****************************************************************
       DATA DIVISION.

      *****************************************************************
      *                                                               *
      *                   WORKING STORAGE SECTION                     *
      *                                                               *
      *****************************************************************
       WORKING-STORAGE SECTION.

           EXEC SQL
              INCLUDE SQLCA
           END-EXEC.

      *-- DCLGEN TABELLA GRAVITA_ERRORE
           EXEC SQL INCLUDE IDBDGER0 END-EXEC.
           EXEC SQL INCLUDE IDBVGER1 END-EXEC.
           EXEC SQL INCLUDE IDBVGER2 END-EXEC.


      *-- DCLGEN TABELLA LINGUA_ERRORE
           EXEC SQL INCLUDE IDBDLER0 END-EXEC.
           EXEC SQL INCLUDE IDBVLER1 END-EXEC.
           EXEC SQL INCLUDE IDBVLER2 END-EXEC.


      *---------------------------------------------------------------*
      *                   FLAGS AND SWITCHES                          *
      *---------------------------------------------------------------*

       01 SW-SWITCH.
      *--SWITCH PER ITER ELABORAZIONE
          05 SW-CORRETTO                   PIC X(01) VALUE 'N'.
              88 SI-CORRETTO                         VALUE 'S'.
              88 NO-CORRETTO                         VALUE 'N'.

          05 SW-TROVATO                    PIC X(01) VALUE 'N'.
              88 SI-TROVATO                          VALUE 'S'.
              88 NO-TROVATO                          VALUE 'N'.

          05 SW-ERRORE-TROVATO             PIC X(01).
              88 ERRORE-TROVATO-SI                   VALUE 'S'.
              88 ERRORE-TROVATO-NO                   VALUE 'N'.

          05 SW-PRIMA-VOLTA                PIC X(01) VALUE 'S'.
              88 SI-PRIMA-VOLTA                      VALUE 'S'.
              88 NO-PRIMA-VOLTA                      VALUE 'N'.
      *---------------------------------------------------------------*
      *                         CONSTANTI                             *
      *---------------------------------------------------------------*
       01 COSTANTI.
           05 WN-ACCESSI                   PIC 9(02) VALUE 13.
           05 LT-IEAS9700                  PIC X(08) VALUE 'IEAS9700'.
           05 LT-IEAS9800                  PIC X(08) VALUE 'IEAS9800'.

      *---------------------------------------------------------------*
      *                            INDICI                             *
      *---------------------------------------------------------------*
       01 INDICI.
           05 IX-IND                       PIC S9(02) COMP.
           05 IX-TAB-ERR                   PIC S9(02) COMP.

      *---------------------------------------------------------------*
      *                         VARIABILI                             *
      *---------------------------------------------------------------*
       01 WS-VARIABILI.
           05 WS-COMPAGNIA                 PIC  9(005).
           05 WS-DESC-ESTESA               PIC  X(200).
           05 WS-NUM-ACCESSO               PIC  9(002).
       01 WS-SQLCODE                       PIC -9(004).


       01 TAB-AREA-ERRORI.
          05 WK-ERRORI-NUM-MAX-ELE    PIC 9(02) VALUE ZERO.
          05 WK-TABELLA-ERRORI        OCCURS 20.
             10 WK-COD-ERRORE           PIC  9(006).
             10 WK-ID-GRAVITA-ERRORE    PIC  9(009).
             10 WK-LIV-GRAVITA          PIC S9(005).
             10 WK-TIPO-TRATT-FE        PIC  X(001).
             10 WK-DESC-ERRORE-BREVE    PIC  X(100).
             10 WK-DESC-ERRORE-ESTESA   PIC  X(200).
             10 WK-COD-ERRORE-990       PIC  9(006).
             10 WK-LABEL-ERR-990        PIC  X(030).

      *---------------------------------------------------------------*
      *                           COPY                                *
      *---------------------------------------------------------------*
      *-- COPY INPUT/OUTPUT PER CALL IEAS9700
          COPY IEAI9701.
          COPY IEAO9701.

      *-- COPY INPUT/OUTPUT PER CALL IEAS9800.
          COPY IEAI9801.
          COPY IEAO9801.

      *-- COPY INPUTOUTPUT ROUTINE IEAS9900.
      *    COPY IEAI9901.
      *    COPY IEAO9901.

      *****************************************************************
      *                                                               *
      *                   LINKAGE SECTION                             *
      *                                                               *
      *****************************************************************
       LINKAGE SECTION.
       01 AREA-IDSV0001.
          COPY IDSV0001.
      *-- COPY INPUTOUTPUT ROUTINE IEAS9900.
          COPY IEAI9901.
          COPY IEAO9901.
      *****************************************************************
      *                                                               *
      *                   PROCEDURE DIVISION                          *
      *                                                               *
      *****************************************************************
      *

       PROCEDURE DIVISION USING AREA-IDSV0001
                                IEAI9901-AREA
                                IEAO9901-AREA.

      *****************************************************************
      *                                                               *
      *                   1000-PRINCIPALE                             *
      *                                                               *
      *****************************************************************
       1000-PRINCIPALE.

           PERFORM 2000-APERTURA
              THRU 2000-APERTURA-FINE.

           IF SI-CORRETTO
      *        PERFORM 3000-PROCESSA
      *           THRU 3000-PROCESSA-FINE.
              PERFORM 4000-PROCESSA
                 THRU 4000-PROCESSA-FINE.

      *     MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
      *                            IEAI9901-LABEL-ERR
      *                             IEAI9901-PARAMETRI-ERR.

      *     MOVE ZEROES          TO IEAI9901-COD-ERRORE.


       1000-PRINCIPALE-FINE.
           GOBACK.


      ******************************************************************
      *                                                                *
      *                   2000-APERTURA                                *
      *                                                                *
      * INITIALIZZAZIONI AREE E VALIDAZIONE FORMALE DEI DATI DI INPUT  *
      ******************************************************************
       2000-APERTURA.
      *- INIZIALIZZO SWITCH E VARIABILI
           SET SI-CORRETTO                  TO TRUE.
           SET ERRORE-TROVATO-NO            TO TRUE.

           MOVE 1                           TO IX-IND.

           PERFORM 2010-VALIDAZIONE-DATI
              THRU 2010-VALIDAZIONE-DATI-FINE.

       2000-APERTURA-FINE.
           EXIT.


      ******************************************************************
      *                                                                *
      *                 2010-VALIDAZIONE-DATI.                         *
      *                                                                *
      * VALIDAZIONE DEI CAMPI IN INPUT                                 *
      ******************************************************************
       2010-VALIDAZIONE-DATI.
           IF IEAI9901-COD-SERVIZIO-BE = SPACES  OR
              IEAI9901-COD-SERVIZIO-BE = LOW-VALUE
              SET NO-CORRETTO         TO TRUE
              MOVE 99999              TO IEAO9901-LIV-GRAVITA
              MOVE 01                 TO IEAO9901-COD-ERRORE-990
              MOVE '2010-VALIDAZIONE-DATI'
                                      TO IEAO9901-LABEL-ERR-990
              MOVE 'CODICE SERVIZIO NON VALORIZZATO'
                                      TO IEAO9901-DESC-ERRORE-ESTESA.

           IF SI-CORRETTO
              IF IEAI9901-LABEL-ERR = SPACES    OR
                 IEAI9901-LABEL-ERR = LOW-VALUE
                 SET NO-CORRETTO      TO TRUE
                 MOVE 02              TO IEAO9901-COD-ERRORE-990
                 MOVE '2010-VALIDAZIONE-DATI'
                                      TO IEAO9901-LABEL-ERR-990
                 MOVE 'LABEL ERRORE NON VALORIZZATO'
                                      TO IEAO9901-DESC-ERRORE-ESTESA
              END-IF
           END-IF.

           IF SI-CORRETTO
              IF IEAI9901-COD-ERRORE = SPACES    OR
                 IEAI9901-COD-ERRORE = LOW-VALUE
                 SET NO-CORRETTO      TO TRUE
                 MOVE 03              TO IEAO9901-COD-ERRORE-990
                 MOVE '2010-VALIDAZIONE-DATI'
                                      TO IEAO9901-LABEL-ERR-990
                 MOVE 'CODICE ERRORE NON VALORIZZATO'
                                      TO IEAO9901-DESC-ERRORE-ESTESA
              END-IF
           END-IF.

           PERFORM 2015-INIZIALIZZA THRU 2015-INIZIALIZZA-EX.

       2010-VALIDAZIONE-DATI-FINE.
           EXIT.

      ******************************************************************
      *                                                                *
      *                   3000-PROCESSA                                *
      *                                                                *
      * -- RICERCA GRAVITA' ERRORE MEDIANTE L'ACCESSO ALLA TABELLA     *
      *    GRAVITA_ERRORE.                                             *
      *                                                                *
      ******************************************************************
       3000-PROCESSA.

           SET NO-TROVATO                    TO TRUE.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO WS-COMPAGNIA.

           PERFORM 3100-RICERCA-GRAVITA
              THRU 3100-RICERCA-GRAVITA-FINE
             UNTIL NO-CORRETTO
                OR SI-TROVATO
                OR IX-IND > WN-ACCESSI.


           IF SI-CORRETTO
              IF IX-IND > WN-ACCESSI AND
                 NO-TROVATO
                 MOVE 99999           TO IEAO9901-LIV-GRAVITA
                 MOVE 03              TO IEAO9901-COD-ERRORE-990
                 MOVE '3000-PROCESSA' TO IEAO9901-LABEL-ERR-990
                 STRING 'OCCORRENZA NON TROVATA SU GRAVITA_ERRORE'
                          DELIMITED BY SIZE
                          ' - COMPAGNIA :'
                          DELIMITED BY SIZE
                          WS-COMPAGNIA
                          DELIMITED BY SIZE
                          ' - COD ERR : '
                          DELIMITED BY SIZE
                          IEAI9901-COD-ERRORE
                          DELIMITED BY SIZE
                          INTO
                          IEAO9901-DESC-ERRORE-ESTESA
                 END-STRING

                 SET NO-CORRETTO      TO TRUE
               ELSE
                 IF SI-TROVATO
                    PERFORM 3200-SELEZ-DESC-ERRORE
                       THRU 3200-SELEZ-DESC-ERRORE-FINE
                 END-IF
              END-IF
           END-IF.

           IF SI-CORRETTO
              PERFORM 3230-VALORIZZA-OUTPUT
                 THRU 3230-VALORIZZA-OUTPUT-FINE
           END-IF.

           IF SI-CORRETTO AND IDSV0001-BATCH
              PERFORM 3240-SCRIVI-LOG
                 THRU 3240-SCRIVI-LOG-FINE
           END-IF.

       3000-PROCESSA-FINE.
           EXIT.
      ******************************************************************
      *                                                                *
      *                   4000-PROCESSA                                *
      *                                                                *
      * -- RICERCA GRAVITA' ERRORE MEDIANTE L'ACCESSO ALLA TABELLA     *
      *    GRAVITA_ERRORE.                                             *
      *                                                                *
      ******************************************************************
       4000-PROCESSA.

           SET NO-TROVATO                    TO TRUE.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO WS-COMPAGNIA.

           PERFORM 3800-SEARCH-GRAVITA
              THRU 3800-SEARCH-GRAVITA-EX.

           IF SI-CORRETTO
              IF IX-IND > WN-ACCESSI AND
                 NO-TROVATO
                 MOVE 99999           TO IEAO9901-LIV-GRAVITA
                 MOVE 03              TO IEAO9901-COD-ERRORE-990
                 MOVE '3000-PROCESSA' TO IEAO9901-LABEL-ERR-990
                 STRING 'OCCORRENZA NON TROVATA SU GRAVITA_ERRORE'
                          DELIMITED BY SIZE
                          ' - COMPAGNIA :'
                          DELIMITED BY SIZE
                          WS-COMPAGNIA
                          DELIMITED BY SIZE
                          ' - COD ERR : '
                          DELIMITED BY SIZE
                          IEAI9901-COD-ERRORE
                          DELIMITED BY SIZE
                          INTO
                          IEAO9901-DESC-ERRORE-ESTESA
                 END-STRING

                 SET NO-CORRETTO      TO TRUE
               ELSE
                 IF  SI-TROVATO
                 AND ERRORE-TROVATO-NO
                    PERFORM 3200-SELEZ-DESC-ERRORE
                       THRU 3200-SELEZ-DESC-ERRORE-FINE
                 END-IF
              END-IF
           END-IF.

           IF  SI-CORRETTO
              IF ERRORE-TROVATO-NO
                 PERFORM 3230-VALORIZZA-OUTPUT
                    THRU 3230-VALORIZZA-OUTPUT-FINE
              END-IF
           END-IF.

           IF SI-CORRETTO AND IDSV0001-BATCH
              PERFORM 3240-SCRIVI-LOG
                 THRU 3240-SCRIVI-LOG-FINE
           END-IF.

           IF  SI-CORRETTO
              IF ERRORE-TROVATO-NO
                 PERFORM 3270-VALORIZZA-TAB
                    THRU 3270-VALORIZZA-TAB-FINE
              END-IF
           END-IF.

       4000-PROCESSA-FINE.
           EXIT.
      ******************************************************************
      *                                                                *
      *                   3800-RICERCA-GRAVITA                         *
      *                                                                *
      ******************************************************************
       3800-SEARCH-GRAVITA.


           IF  WK-ERRORI-NUM-MAX-ELE > 0
           AND IEAI9901-PARAMETRI-ERR = SPACES

              PERFORM VARYING IX-TAB-ERR FROM 1 BY 1
              UNTIL IX-TAB-ERR > WK-ERRORI-NUM-MAX-ELE
              OR ERRORE-TROVATO-SI

                IF IEAI9901-COD-ERRORE = WK-COD-ERRORE(IX-TAB-ERR)
                   SET  ERRORE-TROVATO-SI TO TRUE
                   MOVE WK-ID-GRAVITA-ERRORE(IX-TAB-ERR)
                     TO IEAO9901-ID-GRAVITA-ERRORE
                   MOVE WK-LIV-GRAVITA(IX-TAB-ERR)
                     TO IEAO9901-LIV-GRAVITA
                   MOVE WK-TIPO-TRATT-FE(IX-TAB-ERR)
                     TO IEAO9901-TIPO-TRATT-FE
                   MOVE WK-DESC-ERRORE-BREVE(IX-TAB-ERR)
                     TO IEAO9901-DESC-ERRORE-BREVE
                   MOVE WK-DESC-ERRORE-ESTESA(IX-TAB-ERR)
                     TO IEAO9901-DESC-ERRORE-ESTESA
                   MOVE WK-COD-ERRORE-990(IX-TAB-ERR)
                     TO IEAO9901-COD-ERRORE-990
                   MOVE WK-LABEL-ERR-990(IX-TAB-ERR)
                     TO IEAO9901-LABEL-ERR-990
                END-IF

              END-PERFORM
           END-IF.

           IF  ERRORE-TROVATO-NO
               MOVE WS-COMPAGNIA              TO GER-COD-COMPAGNIA-ANIA
               MOVE IEAI9901-COD-ERRORE       TO GER-COD-ERRORE

               EXEC SQL
                 SELECT ID_GRAVITA_ERRORE,
                        LIVELLO_GRAVITA,
                        TIPO_TRATT_FE
                   INTO :GER-ID-GRAVITA-ERRORE
                       ,:GER-LIVELLO-GRAVITA
                       ,:GER-TIPO-TRATT-FE
                   FROM GRAVITA_ERRORE
                  WHERE COD_COMPAGNIA_ANIA   = :GER-COD-COMPAGNIA-ANIA
                    AND COD_ERRORE           = :GER-COD-ERRORE
                    FETCH FIRST ROW ONLY
               END-EXEC

               EVALUATE SQLCODE
                   WHEN ZEROES
                        SET SI-TROVATO        TO TRUE
                   WHEN +100
                        SET NO-TROVATO        TO TRUE
                   WHEN OTHER
                        MOVE 04               TO IEAO9901-COD-ERRORE-990
                        MOVE '3120-ACCEDI-GRAVITA'
                                              TO IEAO9901-LABEL-ERR-990
                        MOVE SQLCODE          TO WS-SQLCODE
                        STRING 'ERRORE ACCESSO GRAVITA_ERRORE SQLCODE: '
                            WS-SQLCODE DELIMITED BY SIZE
                          INTO IEAO9901-DESC-ERRORE-ESTESA
                        SET NO-CORRETTO       TO TRUE
               END-EVALUATE
           END-IF.

       3800-SEARCH-GRAVITA-EX.
           EXIT.
      ******************************************************************
      *                                                                *
      *                   3100-RICERCA-GRAVITA                         *
      *                                                                *
      ******************************************************************
       3100-RICERCA-GRAVITA.

           MOVE IX-IND                      TO WS-NUM-ACCESSO.

           IF SI-CORRETTO
              PERFORM 3110-VALORIZZA-HOST
                 THRU 3110-VALORIZZA-HOST-FINE.

           IF SI-CORRETTO
              ADD 1                         TO IX-IND.

       3100-RICERCA-GRAVITA-FINE.
           EXIT.

      ******************************************************************
      *                                                                *
      *                   3110-VALORIZZA-HOST                          *
      *                                                                *
      * VALORIZZAZIONE DELLE VARIABILI HOST PER ACCEDERE ALLA TABELLA  *
      *    GRAVITA_ERRORE                                              *
      *                                                                *
      ******************************************************************
       3110-VALORIZZA-HOST.

           MOVE WS-COMPAGNIA                 TO GER-COD-COMPAGNIA-ANIA.
           MOVE IEAI9901-COD-ERRORE          TO GER-COD-ERRORE.
           MOVE IDSV0001-COD-MAIN-BATCH      TO GER-COD-CONV-MAIN-BTC
           MOVE IEAI9901-COD-SERVIZIO-BE     TO GER-COD-PAGINA-SERV-BE
           MOVE IDSV0001-KEY-BUSINESS1       TO GER-KEY-FILTRO1
           MOVE IDSV0001-KEY-BUSINESS2       TO GER-KEY-FILTRO2
           MOVE IDSV0001-KEY-BUSINESS3       TO GER-KEY-FILTRO3
           MOVE IDSV0001-KEY-BUSINESS4       TO GER-KEY-FILTRO4
           MOVE IDSV0001-KEY-BUSINESS5       TO GER-KEY-FILTRO5

           EVALUATE WS-NUM-ACCESSO

               WHEN 1
           EXEC SQL
             SELECT ID_GRAVITA_ERRORE,
                    LIVELLO_GRAVITA,
                    TIPO_TRATT_FE
               INTO :GER-ID-GRAVITA-ERRORE
                   ,:GER-LIVELLO-GRAVITA
                   ,:GER-TIPO-TRATT-FE
               FROM GRAVITA_ERRORE
              WHERE COD_COMPAGNIA_ANIA   = :GER-COD-COMPAGNIA-ANIA
                AND COD_ERRORE           = :GER-COD-ERRORE
                AND COD_CONV_MAIN_BTC
                               = :GER-COD-CONV-MAIN-BTC
                AND COD_PAGINA_SERV_BE
                               = :GER-COD-PAGINA-SERV-BE
                AND KEY_FILTRO1          = :GER-KEY-FILTRO1
                AND KEY_FILTRO2          = :GER-KEY-FILTRO2
                AND KEY_FILTRO3          = :GER-KEY-FILTRO3
                AND KEY_FILTRO4          = :GER-KEY-FILTRO4
                AND KEY_FILTRO5          = :GER-KEY-FILTRO5
           END-EXEC

             WHEN 2
           EXEC SQL
              SELECT ID_GRAVITA_ERRORE,
                  LIVELLO_GRAVITA,
                    TIPO_TRATT_FE
              INTO :GER-ID-GRAVITA-ERRORE
                   ,:GER-LIVELLO-GRAVITA
                   ,:GER-TIPO-TRATT-FE
               FROM GRAVITA_ERRORE
              WHERE COD_COMPAGNIA_ANIA   = :GER-COD-COMPAGNIA-ANIA
                AND COD_ERRORE           = :GER-COD-ERRORE
                AND COD_CONV_MAIN_BTC
                               = :GER-COD-CONV-MAIN-BTC
                AND COD_PAGINA_SERV_BE
                               = :GER-COD-PAGINA-SERV-BE
                AND KEY_FILTRO1          = :GER-KEY-FILTRO1
                AND KEY_FILTRO2          = :GER-KEY-FILTRO2
                AND KEY_FILTRO3          = :GER-KEY-FILTRO3
                AND KEY_FILTRO4          = :GER-KEY-FILTRO4
                AND KEY_FILTRO5          IS NULL
           END-EXEC

               WHEN 3
           EXEC SQL
             SELECT ID_GRAVITA_ERRORE,
                    LIVELLO_GRAVITA,
                    TIPO_TRATT_FE
               INTO :GER-ID-GRAVITA-ERRORE
                   ,:GER-LIVELLO-GRAVITA
                   ,:GER-TIPO-TRATT-FE
               FROM GRAVITA_ERRORE
              WHERE COD_COMPAGNIA_ANIA   = :GER-COD-COMPAGNIA-ANIA
                AND COD_ERRORE           = :GER-COD-ERRORE
                AND COD_CONV_MAIN_BTC
                               = :GER-COD-CONV-MAIN-BTC
                AND COD_PAGINA_SERV_BE
                               = :GER-COD-PAGINA-SERV-BE
                AND KEY_FILTRO1          = :GER-KEY-FILTRO1
                AND KEY_FILTRO2          = :GER-KEY-FILTRO2
                AND KEY_FILTRO3          = :GER-KEY-FILTRO3
                AND KEY_FILTRO4          IS NULL
                AND KEY_FILTRO5          IS NULL
           END-EXEC

               WHEN 4
           EXEC SQL
             SELECT ID_GRAVITA_ERRORE,
                    LIVELLO_GRAVITA,
                    TIPO_TRATT_FE
               INTO :GER-ID-GRAVITA-ERRORE
                   ,:GER-LIVELLO-GRAVITA
                   ,:GER-TIPO-TRATT-FE
               FROM GRAVITA_ERRORE
              WHERE COD_COMPAGNIA_ANIA   = :GER-COD-COMPAGNIA-ANIA
                AND COD_ERRORE           = :GER-COD-ERRORE
                AND COD_CONV_MAIN_BTC
                               = :GER-COD-CONV-MAIN-BTC
                AND COD_PAGINA_SERV_BE
                               = :GER-COD-PAGINA-SERV-BE
                AND KEY_FILTRO1          = :GER-KEY-FILTRO1
                AND KEY_FILTRO2          = :GER-KEY-FILTRO2
                AND KEY_FILTRO3          IS NULL
                AND KEY_FILTRO4          IS NULL
                AND KEY_FILTRO5          IS NULL
           END-EXEC

               WHEN 5
           EXEC SQL
             SELECT ID_GRAVITA_ERRORE,
                    LIVELLO_GRAVITA,
                    TIPO_TRATT_FE
               INTO :GER-ID-GRAVITA-ERRORE
                   ,:GER-LIVELLO-GRAVITA
                   ,:GER-TIPO-TRATT-FE
               FROM GRAVITA_ERRORE
              WHERE COD_COMPAGNIA_ANIA   = :GER-COD-COMPAGNIA-ANIA
                AND COD_ERRORE           = :GER-COD-ERRORE
                AND COD_CONV_MAIN_BTC    = :GER-COD-CONV-MAIN-BTC
                AND COD_PAGINA_SERV_BE
                               = :GER-COD-PAGINA-SERV-BE
                AND KEY_FILTRO1          = :GER-KEY-FILTRO1
                AND KEY_FILTRO2          IS NULL
                AND KEY_FILTRO3          IS NULL
                AND KEY_FILTRO4          IS NULL
                AND KEY_FILTRO5          IS NULL
           END-EXEC

               WHEN 6
           EXEC SQL
             SELECT ID_GRAVITA_ERRORE,
                    LIVELLO_GRAVITA,
                    TIPO_TRATT_FE
               INTO :GER-ID-GRAVITA-ERRORE
                   ,:GER-LIVELLO-GRAVITA
                   ,:GER-TIPO-TRATT-FE
               FROM GRAVITA_ERRORE
              WHERE COD_COMPAGNIA_ANIA   = :GER-COD-COMPAGNIA-ANIA
                AND COD_ERRORE           = :GER-COD-ERRORE
                AND COD_CONV_MAIN_BTC
                               = :GER-COD-CONV-MAIN-BTC
                AND COD_PAGINA_SERV_BE
                               = :GER-COD-PAGINA-SERV-BE
                AND KEY_FILTRO1          IS NULL
                AND KEY_FILTRO2          IS NULL
                AND KEY_FILTRO3          IS NULL
                AND KEY_FILTRO4          IS NULL
                AND KEY_FILTRO5          IS NULL
           END-EXEC

               WHEN 7
           EXEC SQL
             SELECT ID_GRAVITA_ERRORE,
                    LIVELLO_GRAVITA,
                    TIPO_TRATT_FE
               INTO :GER-ID-GRAVITA-ERRORE
                   ,:GER-LIVELLO-GRAVITA
                   ,:GER-TIPO-TRATT-FE
               FROM GRAVITA_ERRORE
              WHERE COD_COMPAGNIA_ANIA   = :GER-COD-COMPAGNIA-ANIA
                AND COD_ERRORE           = :GER-COD-ERRORE
                AND COD_CONV_MAIN_BTC    IS NULL
                AND COD_PAGINA_SERV_BE
                               = :GER-COD-PAGINA-SERV-BE
                AND KEY_FILTRO1          IS NULL
                AND KEY_FILTRO2          IS NULL
                AND KEY_FILTRO3          IS NULL
                AND KEY_FILTRO4          IS NULL
                AND KEY_FILTRO5          IS NULL
           END-EXEC

               WHEN 8
           EXEC SQL
             SELECT ID_GRAVITA_ERRORE,
                    LIVELLO_GRAVITA,
                    TIPO_TRATT_FE
               INTO :GER-ID-GRAVITA-ERRORE
                   ,:GER-LIVELLO-GRAVITA
                   ,:GER-TIPO-TRATT-FE
               FROM GRAVITA_ERRORE
              WHERE COD_COMPAGNIA_ANIA   = :GER-COD-COMPAGNIA-ANIA
                AND COD_ERRORE           = :GER-COD-ERRORE
                AND COD_CONV_MAIN_BTC    IS NULL
                AND COD_PAGINA_SERV_BE   IS NULL
                AND KEY_FILTRO1          = :GER-KEY-FILTRO1
                AND KEY_FILTRO2          = :GER-KEY-FILTRO2
                AND KEY_FILTRO3          = :GER-KEY-FILTRO3
                AND KEY_FILTRO4          = :GER-KEY-FILTRO4
                AND KEY_FILTRO5          = :GER-KEY-FILTRO5
           END-EXEC

               WHEN 9
           EXEC SQL
             SELECT ID_GRAVITA_ERRORE,
                    LIVELLO_GRAVITA,
                    TIPO_TRATT_FE
               INTO :GER-ID-GRAVITA-ERRORE
                   ,:GER-LIVELLO-GRAVITA
                   ,:GER-TIPO-TRATT-FE
               FROM GRAVITA_ERRORE
              WHERE COD_COMPAGNIA_ANIA   = :GER-COD-COMPAGNIA-ANIA
                AND COD_ERRORE           = :GER-COD-ERRORE
                AND COD_CONV_MAIN_BTC    IS NULL
                AND COD_PAGINA_SERV_BE   IS NULL
                AND KEY_FILTRO1          = :GER-KEY-FILTRO1
                AND KEY_FILTRO2          = :GER-KEY-FILTRO2
                AND KEY_FILTRO3          = :GER-KEY-FILTRO3
                AND KEY_FILTRO4          = :GER-KEY-FILTRO4
                AND KEY_FILTRO5          IS NULL
           END-EXEC

               WHEN 10
           EXEC SQL
             SELECT ID_GRAVITA_ERRORE,
                    LIVELLO_GRAVITA,
                    TIPO_TRATT_FE
               INTO :GER-ID-GRAVITA-ERRORE
                   ,:GER-LIVELLO-GRAVITA
                   ,:GER-TIPO-TRATT-FE
               FROM GRAVITA_ERRORE
              WHERE COD_COMPAGNIA_ANIA   = :GER-COD-COMPAGNIA-ANIA
                AND COD_ERRORE           = :GER-COD-ERRORE
                AND COD_CONV_MAIN_BTC    IS NULL
                AND COD_PAGINA_SERV_BE   IS NULL
                AND KEY_FILTRO1          = :GER-KEY-FILTRO1
                AND KEY_FILTRO2          = :GER-KEY-FILTRO2
                AND KEY_FILTRO3          = :GER-KEY-FILTRO3
                AND KEY_FILTRO4          IS NULL
                AND KEY_FILTRO5          IS NULL
           END-EXEC

               WHEN 11
           EXEC SQL
             SELECT ID_GRAVITA_ERRORE,
                    LIVELLO_GRAVITA,
                    TIPO_TRATT_FE
               INTO :GER-ID-GRAVITA-ERRORE
                   ,:GER-LIVELLO-GRAVITA
                   ,:GER-TIPO-TRATT-FE
               FROM GRAVITA_ERRORE
              WHERE COD_COMPAGNIA_ANIA   = :GER-COD-COMPAGNIA-ANIA
                AND COD_ERRORE           = :GER-COD-ERRORE
                AND COD_CONV_MAIN_BTC    IS NULL
                AND COD_PAGINA_SERV_BE   IS NULL
                AND KEY_FILTRO1          = :GER-KEY-FILTRO1
                AND KEY_FILTRO2          = :GER-KEY-FILTRO2
                AND KEY_FILTRO3          IS NULL
                AND KEY_FILTRO4          IS NULL
                AND KEY_FILTRO5          IS NULL
           END-EXEC

               WHEN 12
           EXEC SQL
             SELECT ID_GRAVITA_ERRORE,
                    LIVELLO_GRAVITA,
                    TIPO_TRATT_FE
               INTO :GER-ID-GRAVITA-ERRORE
                   ,:GER-LIVELLO-GRAVITA
                   ,:GER-TIPO-TRATT-FE
               FROM GRAVITA_ERRORE
              WHERE COD_COMPAGNIA_ANIA   = :GER-COD-COMPAGNIA-ANIA
                AND COD_ERRORE           = :GER-COD-ERRORE
                AND COD_CONV_MAIN_BTC    IS NULL
                AND COD_PAGINA_SERV_BE   IS NULL
                AND KEY_FILTRO1          = :GER-KEY-FILTRO1
                AND KEY_FILTRO2          IS NULL
                AND KEY_FILTRO3          IS NULL
                AND KEY_FILTRO4          IS NULL
                AND KEY_FILTRO5          IS NULL
           END-EXEC

               WHEN 13
           EXEC SQL
             SELECT ID_GRAVITA_ERRORE,
                    LIVELLO_GRAVITA,
                    TIPO_TRATT_FE
               INTO :GER-ID-GRAVITA-ERRORE
                   ,:GER-LIVELLO-GRAVITA
                   ,:GER-TIPO-TRATT-FE
               FROM GRAVITA_ERRORE
              WHERE COD_COMPAGNIA_ANIA   = :GER-COD-COMPAGNIA-ANIA
                AND COD_ERRORE           = :GER-COD-ERRORE
                AND COD_CONV_MAIN_BTC    IS NULL
                AND COD_PAGINA_SERV_BE   IS NULL
                AND KEY_FILTRO1          IS NULL
                AND KEY_FILTRO2          IS NULL
                AND KEY_FILTRO3          IS NULL
                AND KEY_FILTRO4          IS NULL
                AND KEY_FILTRO5          IS NULL
           END-EXEC
           END-EVALUATE.

           EVALUATE SQLCODE
               WHEN ZEROES
                    SET SI-TROVATO        TO TRUE
               WHEN +100
                    SET NO-TROVATO        TO TRUE
               WHEN OTHER
                    MOVE 04               TO IEAO9901-COD-ERRORE-990
                    MOVE '3120-ACCEDI-GRAVITA'
                                          TO IEAO9901-LABEL-ERR-990
                    MOVE SQLCODE          TO WS-SQLCODE
                    STRING 'ERRORE ACCESSO GRAVITA_ERRORE SQLCODE: '
                        WS-SQLCODE DELIMITED BY SIZE
                      INTO IEAO9901-DESC-ERRORE-ESTESA
                    SET NO-CORRETTO       TO TRUE
           END-EVALUATE.

       3110-VALORIZZA-HOST-FINE.
           EXIT.

      ******************************************************************
      *                                                                *
      *                   3200-SELEZ-DESC-ERRORE                       *
      *                                                                *
      ******************************************************************
       3200-SELEZ-DESC-ERRORE.

           PERFORM 3210-ACCEDI-LNGERR
              THRU 3210-ACCEDI-LNGERR-FINE.

           IF SI-CORRETTO
              PERFORM 3220-SOST-PLACEHOLDER
                 THRU 3220-SOST-PLACEHOLDER-FINE.

       3200-SELEZ-DESC-ERRORE-FINE.
           EXIT.

      ******************************************************************
      *                                                                *
      *                   3210-ACCEDI-LNGERR                           *
      *                                                                *
      ******************************************************************
       3210-ACCEDI-LNGERR.

           MOVE IDSV0001-LINGUA           TO LER-LINGUA.
           MOVE IDSV0001-PAESE            TO LER-PAESE.

           INITIALIZE LER-DESC-ERRORE-BREVE
                      LER-DESC-ERRORE-ESTESA

           EXEC SQL
             SELECT DESC_ERRORE_BREVE,
                    DESC_ERRORE_ESTESA
               INTO :LER-DESC-ERRORE-BREVE-VCHAR
                   ,:LER-DESC-ERRORE-ESTESA-VCHAR
               FROM LINGUA_ERRORE
              WHERE COD_ERRORE         = :GER-COD-ERRORE
                AND COD_COMPAGNIA_ANIA = :GER-COD-COMPAGNIA-ANIA
                AND LINGUA             = :LER-LINGUA
                AND PAESE              = :LER-PAESE
           END-EXEC.

           EVALUATE SQLCODE
               WHEN ZEROES
                    CONTINUE
               WHEN +100
                    SET NO-CORRETTO       TO TRUE
                    MOVE 04               TO IEAO9901-COD-ERRORE-990
                    MOVE '3210-ACCEDI-LNGERR'
                                          TO IEAO9901-LABEL-ERR-990
                    MOVE 'DESCRIZIONE ASSENTE IN LINGUA_ERRORE'
                                          TO IEAO9901-DESC-ERRORE-ESTESA
               WHEN OTHER
                    SET NO-CORRETTO       TO TRUE
                    MOVE 04               TO IEAO9901-COD-ERRORE-990
                    MOVE '3210-ACCEDI-LNGERR'
                                          TO IEAO9901-LABEL-ERR-990
                    MOVE 'ERRORE ACCESSO LINGUA_ERRORE'
                                          TO IEAO9901-DESC-ERRORE-ESTESA
           END-EVALUATE.

       3210-ACCEDI-LNGERR-FINE.
           EXIT.


      ******************************************************************
      *                                                                *
      *                   3220-SOST-PLACEHOLDER                        *
      *                                                                *
      ******************************************************************
       3220-SOST-PLACEHOLDER.
      * USO IL PUNTO DAT PERCHE' NON COMPRENDE LA LUNGHEZZA
           MOVE LER-DESC-ERRORE-ESTESA   TO IEAI9801-DESC-ERRORE-ESTESA.
           MOVE IEAI9901-PARAMETRI-ERR   TO IEAI9801-PARAMETRI-ERR.

           CALL LT-IEAS9800                USING AREA-IDSV0001
                                                 IEAI9801-AREA
                                                 IEAO9801-AREA.

           IF IEAO9801-COD-ERRORE-990 = ZERO
              MOVE ZEROES                TO IEAO9901-COD-ERRORE-990
              MOVE IEAO9801-AREA         TO WS-DESC-ESTESA
           ELSE
              MOVE 04                    TO IEAO9901-COD-ERRORE-990
              MOVE '3220-SOST-PLACEHOLDER'
                                         TO IEAO9901-LABEL-ERR-990
              MOVE 'ERRORE ROUTINE IEAS9800'
                                         TO IEAO9901-DESC-ERRORE-ESTESA
           END-IF.

       3220-SOST-PLACEHOLDER-FINE.
           EXIT.

      ******************************************************************
      *                                                                *
      *                   3230-VALORIZZA-OUTPUT                        *
      *                                                                *
      ******************************************************************
       3230-VALORIZZA-OUTPUT.
           MOVE GER-ID-GRAVITA-ERRORE    TO IEAO9901-ID-GRAVITA-ERRORE.
           MOVE GER-LIVELLO-GRAVITA      TO IEAO9901-LIV-GRAVITA.
           MOVE GER-TIPO-TRATT-FE        TO IEAO9901-TIPO-TRATT-FE.
           MOVE IDSV0001-LABEL-ERR       TO IEAO9901-LABEL-ERR-990.
           MOVE LER-DESC-ERRORE-BREVE    TO IEAO9901-DESC-ERRORE-BREVE.
           MOVE WS-DESC-ESTESA           TO IEAO9901-DESC-ERRORE-ESTESA.
       3230-VALORIZZA-OUTPUT-FINE.
           EXIT.
      ******************************************************************
      *                                                                *
      *                   3270-VALORIZZA-TAB                           *
      *                                                                *
      ******************************************************************
       3270-VALORIZZA-TAB.

           IF WK-ERRORI-NUM-MAX-ELE < 20
              ADD 1 TO WK-ERRORI-NUM-MAX-ELE

              MOVE IEAI9901-COD-ERRORE
                TO WK-COD-ERRORE(WK-ERRORI-NUM-MAX-ELE)

              MOVE IEAO9901-ID-GRAVITA-ERRORE
                TO WK-ID-GRAVITA-ERRORE(WK-ERRORI-NUM-MAX-ELE)

              MOVE IEAO9901-LIV-GRAVITA
                TO WK-LIV-GRAVITA(WK-ERRORI-NUM-MAX-ELE)

              MOVE IEAO9901-TIPO-TRATT-FE
                TO WK-TIPO-TRATT-FE(WK-ERRORI-NUM-MAX-ELE)

              MOVE IEAO9901-DESC-ERRORE-BREVE
                TO WK-DESC-ERRORE-BREVE(WK-ERRORI-NUM-MAX-ELE)

              MOVE IEAO9901-DESC-ERRORE-ESTESA
                TO WK-DESC-ERRORE-ESTESA(WK-ERRORI-NUM-MAX-ELE)

              MOVE IEAO9901-COD-ERRORE-990
                TO WK-COD-ERRORE-990(WK-ERRORI-NUM-MAX-ELE)

              MOVE IEAO9901-LABEL-ERR-990
                TO WK-LABEL-ERR-990(WK-ERRORI-NUM-MAX-ELE)

           END-IF.


       3270-VALORIZZA-TAB-FINE.
           EXIT.
      ******************************************************************
      *                                                                *
      *                   3240-SCRIVI-LOG                              *
      *                                                                *
      ******************************************************************
       3240-SCRIVI-LOG.
           MOVE SPACES                   TO IEAI9701-AREA
                                            IEAO9701-AREA.
           MOVE  WS-DESC-ESTESA          TO IDSV0001-DESC-ERRORE-ESTESA
                                         OF IDSV0001-LOG-ERRORE
           MOVE GER-ID-GRAVITA-ERRORE    TO IEAI9701-ID-GRAVITA-ERRORE.

           CALL LT-IEAS9700           USING AREA-IDSV0001
                                            IEAI9701-AREA
                                            IEAO9701-AREA.

           IF IEAO9701-COD-ERRORE-990 = ZERO
              MOVE ZEROES                TO IEAO9901-COD-ERRORE-990
           ELSE
              MOVE 04                    TO IEAO9901-COD-ERRORE-990
              MOVE '3240-SCRIVI-LOG'     TO IEAO9901-LABEL-ERR-990
              MOVE 'ERRORE ROUTINE IEAS9700'
                                         TO IEAO9901-DESC-ERRORE-ESTESA
           END-IF.

       3240-SCRIVI-LOG-FINE.
           EXIT.

       2015-INIZIALIZZA.
      *----> INIZIALIZZO TUTTI I CAMPI DESTINATI A VALORIZZARE LE
      *----> VARIABILI HOST, CON I VALORI DI DEFAULT
      *----> ( : SPAZIO PER I CHAR , ZERO PER I NUMERICI)

           IF IDSV0001-COD-COMPAGNIA-ANIA = LOW-VALUE
              MOVE ZEROES TO IDSV0001-COD-COMPAGNIA-ANIA
           END-IF.

           IF IEAI9901-COD-ERRORE = LOW-VALUE
              MOVE ZEROES TO IEAI9901-COD-ERRORE
           END-IF.

           IF IDSV0001-COD-MAIN-BATCH = LOW-VALUE
              MOVE SPACES TO IDSV0001-COD-MAIN-BATCH
           END-IF.

           IF IEAI9901-COD-SERVIZIO-BE = LOW-VALUE
              MOVE SPACES TO IEAI9901-COD-SERVIZIO-BE
           END-IF.

           IF IDSV0001-KEY-BUSINESS1 = LOW-VALUE
              MOVE SPACES TO IDSV0001-KEY-BUSINESS1

           IF IDSV0001-KEY-BUSINESS2 = LOW-VALUE
              MOVE SPACES TO IDSV0001-KEY-BUSINESS2
           END-IF.

           IF IDSV0001-KEY-BUSINESS3 = LOW-VALUE
              MOVE SPACES TO IDSV0001-KEY-BUSINESS3
           END-IF.

           IF IDSV0001-KEY-BUSINESS4 = LOW-VALUE
              MOVE SPACES TO IDSV0001-KEY-BUSINESS4
           END-IF.

           IF IDSV0001-KEY-BUSINESS5 = LOW-VALUE
              MOVE SPACES TO IDSV0001-KEY-BUSINESS5
           END-IF.

      *PER ACCESSO A LINGUA_ERRORE
           IF IDSV0001-LINGUA = LOW-VALUE
              MOVE SPACES TO IDSV0001-LINGUA
           END-IF.

           IF IDSV0001-PAESE = LOW-VALUE
              MOVE SPACES TO IDSV0001-PAESE
           END-IF.

       2015-INIZIALIZZA-EX.
           EXIT.




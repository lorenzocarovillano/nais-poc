      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS2800.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2013.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS2800
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... NUOVA FISCALITA'
      *                  CALCOLO DELLA VARIABILE VLRDVISE2011
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS2800'.
       01  IDBSP610                         PIC X(008) VALUE 'IDBSP610'.
       01  LDBS6040                         PIC X(008) VALUE 'LDBS6040'.
MIGCOL 01  LDBSH650                         PIC X(008) VALUE 'LDBSH650'.
       01  WK-PGM-CALLED                    PIC X(08)  VALUE '        '.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-TOTALE                   PIC S9(11)V9(07).

       01 WK-DATA-FINE.
           03 WK-ANNO-FINE              PIC 9(04).
           03 WK-MESE-FINE              PIC 9(02).
           03 WK-GG-FINE                PIC 9(02).

       01 WK-DATA-FINE-D             REDEFINES
             WK-DATA-FINE               PIC S9(8)V COMP-3.

      *---------------------------------------------------------------*
      *  COPY TABELLE
      *---------------------------------------------------------------*
      *--> TABELLA MOVIMENTO
           COPY IDBVMOV1.
      *--> TABELLA D_CRIST
           COPY IDBVP611.
MIGCOL*--> TABELLA POLI
MIGCOL     COPY IDBVPOL1.
      *---------------------------------------------------------------*
      *  LISTA TABELLE E TIPOLOGICHE
      *---------------------------------------------------------------*
10819      COPY LCCVXMVZ.
10819      COPY LCCVXMV1.
10819      COPY LCCVXMV2.
10819      COPY LCCVXMV3.
10819      COPY LCCVXMV4.
10819      COPY LCCVXMV5.
10819      COPY LCCVXMV6.
      *---------------------------------------------------------------*
      *  LISTA MOVIMENTI IMPLEMENTATI LCCC0006
      *---------------------------------------------------------------*
           COPY LCCC0006.
      *----------------------------------------------------------------*
      *--> AREA DATI CRISTALLIZZATI
      *----------------------------------------------------------------*
       01 AREA-IO-D-CRIST.
          03 DP61-AREA-D-CRIST.
             04 DP61-ELE-D-CRIST-MAX        PIC S9(04) COMP.
             04 DP61-TAB-DCRIST.
             COPY LCCVP611              REPLACING ==(SF)== BY ==DP61==.

MIGCOL 01 AREA-IO-D-CRIST-COLL.
MIGCOL    03 WP61-AREA-D-CRIST.
MIGCOL       04 WP61-ELE-D-CRIST-MAX        PIC S9(04) COMP.
MIGCOL       04 WP61-TAB-DCRIST.
MIGCOL       COPY LCCVP611              REPLACING ==(SF)== BY ==WP61==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIABILI PER LA GESTIONE ERRORI                     *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-P61                     PIC 9(09) VALUE 0.
      *----------------------------------------------------------------*
      *    FLAG
      *----------------------------------------------------------------*
       01 FLAG-CUR-MOV                       PIC X(01).
           88 INIT-CUR-MOV                   VALUE 'S'.
           88 FINE-CUR-MOV                   VALUE 'N'.

       01 FLAG-ULTIMA-LETTURA               PIC X(01).
          88 SI-ULTIMA-LETTURA              VALUE 'S'.
          88 NO-ULTIMA-LETTURA              VALUE 'N'.

       01 FLAG-FINE-LETTURA-P61               PIC X(01).
          88 FINE-LETT-P61-SI               VALUE 'S'.
          88 FINE-LETT-P61-NO               VALUE 'N'.

       01 FLAG-COMUN-TROV                    PIC X(01).
           88 COMUN-TROV-SI                  VALUE 'S'.
           88 COMUN-TROV-NO                  VALUE 'N'.

MIGCOL 01 FL-TIPO-POLIZZA                    PIC X(01).
MIGCOL     88 FL-TIPO-COLLETTIVA              VALUE 'C'.
MIGCOL     88 FL-TIPO-INDIVIDUALE             VALUE 'I'.

41655  01 WS-MOVI-ORIG                       PIC 9(05).
41655      88 WS-LIQUI-RISTOT-IND               VALUE 5010.
41655      88 WS-LIQUI-RISPAR-POLIND            VALUE 5006.
41655      88 WS-LIQUI-SCAPOL                   VALUE 5026.
41655      88 WS-LIQUI-SININD                   VALUE 5018.
41655      88 WS-LIQUI-RECIND                   VALUE 5002.
10819      88 WS-LIQUI-TAKE-PROFIT              VALUE 2319.
10819X     88 WS-LIQUI-RISTOT-INCAPIENZA        VALUE 2325.
10819X     88 WS-LIQUI-RPP-REDDITO-PROGR        VALUE 2317.
10819      88 WS-LIQUI-RPP-BENEFICIO-CONTR      VALUE 2323.
FNZS2      88 WS-LIQUI-RISTOT-INC-DA-RP         VALUE 2333.

MIGCOL 01 WS-MOVI-ORI-COLL                   PIC 9(05).
MIGCOL    88  WS-LIQUI-RISPAR-ADE               VALUE 5008.
MIGCOL    88  WS-LIQUI-RISTOT-ADE               VALUE 5009.
MIGCOL    88  WS-LIQUI-SINADE                   VALUE 5017.
MIGCOL    88  WS-SCANT-ADESIO                   VALUE 5014.
MIGCOL    88  WS-LIQUI-CEDOLE                   VALUE 6018.


MIGCOL 01 WS-MOVI-ORI-COLL-COM              PIC 9(05).
MIGCOL    88 WS-COMUN-RISPAR-ADE                VALUE 5003.
MIGCOL    88 WS-RISTO-ADESIO                    VALUE 3010.
MIGCOL    88 WS-SINIS-ADESIO                    VALUE 3018.
MIGCOL    88 WS-VARIA-OPZION                    VALUE 6009.

       01  WK-VAR-MOVI-COMUN.
           05 WK-ID-MOVI-COMUN             PIC S9(09) COMP-3.
           05 WK-DATA-EFF-PREC             PIC S9(08) COMP-3.
           05 WK-DATA-CPTZ-PREC            PIC S9(18) COMP-3.
           05 WK-DATA-EFF-RIP              PIC S9(08) COMP-3.
           05 WK-DATA-CPTZ-RIP             PIC S9(18) COMP-3.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS2800.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS2800.
      *----------------------------------------------------------------*
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           SET COMUN-TROV-NO                 TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-D-CRIST
MIGCOL                AREA-IO-D-CRIST-COLL.

MIGCOL     PERFORM L450-LEGGI-POLIZZA
MIGCOL        THRU L450-LEGGI-POLIZZA-EX

MIGCOL     MOVE IVVC0213-TIPO-MOVIMENTO
MIGCOL       TO WS-MOVI-ORI-COLL-COM
MIGCOL          WS-MOVI-ORI-COLL
      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
      *    PERFORM S1100-VALORIZZA-DCLGEN
      *       THRU S1100-VALORIZZA-DCLGEN-EX
      *    VARYING IX-DCLGEN FROM 1 BY 1
      *      UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
      *         OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
      *            SPACES OR LOW-VALUE OR HIGH-VALUE
MIGCOL*    IF IVVC0213-TIPO-MOVIMENTO = WS-MOVI-ORI-COLL-COM
MIGCOL*    OR IVVC0213-TIPO-MOVIMENTO = WS-MOVI-ORI-COLL
MIGCOL*       SET FL-TIPO-COLLETTIVA   TO TRUE
MIGCOL*    ELSE
MIGCOL*       SET FL-TIPO-INDIVIDUALE  TO TRUE
MIGCOL*    END-IF
MIGCOL     IF FL-TIPO-INDIVIDUALE
              PERFORM VERIFICA-MOVIMENTO
                 THRU VERIFICA-MOVIMENTO-EX
           ELSE
              PERFORM LEGGI-D-CRIST-X-ADE
                 THRU LEGGI-D-CRIST-X-ADE-EX
           END-IF


      *--> PERFORM DI CALCOLO ANNO CORRENTE
MIGCOL*    IF FL-TIPO-INDIVIDUALE
              IF  IDSV0003-SUCCESSFUL-RC
      *       AND IDSV0003-SUCCESSFUL-SQL
                  PERFORM S1150-CALCOLO-ANNO
                     THRU S1150-CALCOLO-ANNO-EX
              END-IF
MIGCOL*    END-IF


      *--> PERFORM DI CALCOLO DELL'IMPORTO
MIGCOL     IF FL-TIPO-INDIVIDUALE
              IF  IDSV0003-SUCCESSFUL-RC
      *       AND IDSV0003-SUCCESSFUL-SQL
                  PERFORM S1200-CALCOLO-IMPORTO
                     THRU S1200-CALCOLO-IMPORTO-EX
              END-IF

           ELSE
              PERFORM S1201-CALC-IMP-COLL
                 THRU S1201-CALC-IMP-COLL-EX
           END-IF.

       EX-S1000.
           EXIT.
MIGCOL*----------------------------------------------------------------*
MIGCOL*-- LETTURA DELLA POLIZZA
MIGCOL*----------------------------------------------------------------*
MIGCOL L450-LEGGI-POLIZZA.

           INITIALIZE POLI.
           SET FL-TIPO-INDIVIDUALE TO TRUE
           MOVE IVVC0213-ID-POLIZZA       TO POL-ID-POLI.

           MOVE 'IDBSPOL0'                TO WK-PGM-CALLED.

      *  --> Tipo operazione
           SET IDSV0003-SELECT            TO TRUE.
      *  --> Tipo livello
           SET IDSV0003-ID                TO TRUE.

           CALL WK-PGM-CALLED USING IDSV0003 POLI

           ON EXCEPTION
              MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL-IDBSPOL0 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER   TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              IF POL-TP-FRM-ASSVA = 'IN'
                 SET FL-TIPO-INDIVIDUALE TO TRUE
              ELSE
                 SET FL-TIPO-COLLETTIVA TO TRUE
              END-IF
           ELSE
              MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA IDBSPOL0 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                     DELIMITED BY SIZE INTO
                     IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
                 SET IDSV0003-FIELD-NOT-VALUED TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER     TO TRUE
              END-IF
           END-IF.

MIGCOL L450-LEGGI-POLIZZA-EX.
MIGCOL     EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-DATI-CRIST
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DP61-AREA-D-CRIST
           END-IF.


       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CALCOLO ANNO
      *----------------------------------------------------------------*
       S1150-CALCOLO-ANNO.

           MOVE IVVC0213-DT-DECOR
             TO WK-DATA-FINE-D.
       S1150-CALCOLO-ANNO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CALCOLO IMPORTO
      *----------------------------------------------------------------*
       S1200-CALCOLO-IMPORTO.
           MOVE 0   TO IVVC0213-VAL-IMP-O
           MOVE 0   TO WK-TOTALE

      *    PERFORM VARYING IX-DCLGEN FROM 1 BY 1
      *      UNTIL IX-DCLGEN > DP61-ELE-D-CRIST-MAX
           IF DP61-ELE-D-CRIST-MAX > 0

             IF WK-DATA-FINE-D <= 20120101
                IF DP61-IMPB-VIS-RP-P2011-NULL
                   EQUAL HIGH-VALUES OR LOW-VALUES OR SPACES
                   CONTINUE
                ELSE
                   COMPUTE WK-TOTALE = DP61-IMPB-VIS-RP-P2011
                                     + WK-TOTALE
                END-IF
             END-IF

           END-IF.
      *    END-PERFORM.
      *
           MOVE WK-TOTALE TO IVVC0213-VAL-IMP-O.
      *
       S1200-CALCOLO-IMPORTO-EX.
           EXIT.
MIGCOL*----------------------------------------------------------------*
MIGCOL*   CALCOLO IMPORTO PER LE COLLETTIVE
MIGCOL*----------------------------------------------------------------*
MIGCOL S1201-CALC-IMP-COLL.

MIGCOL     MOVE 0   TO IVVC0213-VAL-IMP-O
MIGCOL*    MOVE 0   TO WK-TOTALE


MIGCOL     IF WK-DATA-FINE-D <= 20120101
MIGCOL        IF WK-TOTALE IS NUMERIC
MIGCOL        AND WK-TOTALE > ZEROES
MIGCOL            MOVE WK-TOTALE TO IVVC0213-VAL-IMP-O
MIGCOL        ELSE
MIGCOL           MOVE ZEROES   TO WK-TOTALE
MIGCOL        END-IF
MIGCOL     END-IF.
MIGCOL*
MIGCOL S1201-CALC-IMP-COLL-EX.
MIGCOL     EXIT.

      *----------------------------------------------------------------*
      *    VERIFICA MOVIMENTO
      *----------------------------------------------------------------*
       VERIFICA-MOVIMENTO.
      *--> SE CI TROVIAMO IN FASE DI LIQUIDAZIONE DEVO RECUPERARE LE
      *--> IMMAGINI IN FASE DI COMUNICAZIONE
NEW        MOVE IVVC0213-TIPO-MOVI-ORIG
NEW          TO WS-MOVIMENTO
41655           WS-MOVI-ORIG
MIGCOL          WS-MOVI-ORI-COLL

NEW        IF LIQUI-RISTOT-IND OR LIQUI-RISPAR-POLIND OR
NEW           LIQUI-SCAPOL OR LIQUI-SININD OR LIQUI-RECIND OR
10819         LIQUI-RPP-TAKE-PROFIT
10819X     OR LIQUI-RPP-REDDITO-PROGR
10819X     OR LIQUI-RISTOT-INCAPIENZA
10819      OR LIQUI-RPP-BENEFICIO-CONTR
FNZS2      OR LIQUI-RISTOT-INCAP
      *--> RECUPERO IL MOVIMENTO DI COMUNICAZIONE
NEW           PERFORM RECUP-MOVI-COMUN
NEW              THRU RECUP-MOVI-COMUN-EX

              IF COMUN-TROV-SI
                 MOVE IDSV0003-DATA-COMPETENZA
                   TO WK-DATA-CPTZ-RIP

                 MOVE IDSV0003-DATA-INIZIO-EFFETTO
                   TO WK-DATA-EFF-RIP

                 MOVE WK-DATA-CPTZ-PREC
                   TO IDSV0003-DATA-COMPETENZA

                 MOVE WK-DATA-EFF-PREC
                   TO IDSV0003-DATA-INIZIO-EFFETTO
                      IDSV0003-DATA-FINE-EFFETTO

                 PERFORM INIZIA-TOT-P61
                    THRU INIZIA-TOT-P61-EX

                 PERFORM LETTURA-D-CRIST
                    THRU LETTURA-D-CRIST-EX
NEW           ELSE
      *-->       ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *-->       RISPETTIVE AREE DCLGEN IN WORKING
                 PERFORM S1100-VALORIZZA-DCLGEN
                    THRU S1100-VALORIZZA-DCLGEN-EX
                 VARYING IX-DCLGEN FROM 1 BY 1
                   UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                      OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                         SPACES OR LOW-VALUE OR HIGH-VALUE
NEW           END-IF
NEW        ELSE

      *-->    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *-->    RISPETTIVE AREE DCLGEN IN WORKING
              PERFORM S1100-VALORIZZA-DCLGEN
                 THRU S1100-VALORIZZA-DCLGEN-EX
              VARYING IX-DCLGEN FROM 1 BY 1
                UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                   OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                      SPACES OR LOW-VALUE OR HIGH-VALUE
           END-IF.

       VERIFICA-MOVIMENTO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Recupero il movimento di comunicazione
      *----------------------------------------------------------------*
       RECUP-MOVI-COMUN.

           SET INIT-CUR-MOV               TO TRUE
           SET COMUN-TROV-NO              TO TRUE

           INITIALIZE MOVI

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
           SET  NO-ULTIMA-LETTURA         TO TRUE

      *
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR FINE-CUR-MOV
                      OR COMUN-TROV-SI

              INITIALIZE MOVI
      *
              MOVE IVVC0213-ID-POLIZZA    TO MOV-ID-OGG
              MOVE 'PO'                   TO MOV-TP-OGG

              SET IDSV0003-TRATT-SENZA-STOR   TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSV0003-WHERE-CONDITION TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL TO TRUE

              CALL  LDBS6040   USING IDSV0003 MOVI
                ON EXCEPTION
                   MOVE LDBS6040          TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                     TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER   TO TRUE
              END-CALL


                IF IDSV0003-SUCCESSFUL-RC

                    EVALUATE TRUE
                        WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA
      *-->     LIQUIDAZIONE CONTESTUALE - UTILIZZO L'AREA DEL
      *-->     BOLLO IN INPUT
                          SET FINE-CUR-MOV TO TRUE

                        WHEN IDSV0003-SUCCESSFUL-SQL
      *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
      *                   MOVE IDSV0003-BUFFER-DATI TO MOVI
      *      TROVO IL MOVIMENTO DI COMUNICAZIONE RISCATTO PARZIALE
                          MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
41655 *                   IF COMUN-RISPAR-IND
41655 *                   OR RISTO-INDIVI
41655 *                   OR VARIA-OPZION
41655 *                   OR SINIS-INDIVI
41655 *                   OR RECES-INDIVI
41655                   IF (COMUN-RISPAR-IND AND WS-LIQUI-RISPAR-POLIND)
41655                     OR ( RISTO-INDIVI AND WS-LIQUI-RISTOT-IND)
FNZS2                     OR ( COMUN-RISTOT-INCAP AND
FNZS2                          WS-LIQUI-RISTOT-INC-DA-RP)
P1655                     OR ( VARIA-OPZION AND WS-LIQUI-SCAPOL)
41655                     OR ( SINIS-INDIVI AND WS-LIQUI-SININD)
41655                     OR ( RECES-INDIVI AND WS-LIQUI-RECIND)
10819                     OR (RPP-TAKE-PROFIT AND WS-LIQUI-TAKE-PROFIT)
10819X                    OR (COMUN-RISTOT-INCAPIENZA
10819X                    AND WS-LIQUI-RISTOT-INCAPIENZA)
10819X                    OR (RPP-REDDITO-PROGRAMMATO
10819X                    AND WS-LIQUI-RPP-REDDITO-PROGR)
10819                     OR (RPP-BENEFICIO-CONTR
10819                     AND WS-LIQUI-RPP-BENEFICIO-CONTR)
                             MOVE MOV-ID-MOVI    TO WK-ID-MOVI-COMUN
                             SET SI-ULTIMA-LETTURA  TO TRUE
                             MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                             COMPUTE WK-DATA-CPTZ-PREC =  MOV-DS-TS-CPTZ
                                                       - 1
                             SET COMUN-TROV-SI   TO TRUE
                             PERFORM CLOSE-MOVI
                                THRU CLOSE-MOVI-EX
                             SET FINE-CUR-MOV   TO TRUE
                          ELSE
                             SET IDSV0003-FETCH-NEXT   TO TRUE
                          END-IF
                        WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                             MOVE LDBS6040
                               TO IDSV0003-COD-SERVIZIO-BE
                             MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                               TO IDSV0003-DESCRIZ-ERR-DB2
                             SET IDSV0003-INVALID-OPER   TO TRUE
                    END-EVALUATE
                 ELSE
      *--> GESTIRE ERRORE
                    MOVE LDBS6040
                      TO IDSV0003-COD-SERVIZIO-BE
                    MOVE 'CALL-LDBS6040 ERRORE CHIAMATA'
                      TO IDSV0003-DESCRIZ-ERR-DB2
                    SET IDSV0003-INVALID-OPER   TO TRUE
                 END-IF
           END-PERFORM.
           MOVE IDSV0003-TIPO-MOVIMENTO TO WS-MOVIMENTO.

       RECUP-MOVI-COMUN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RECUPERO DATI CRISTALLIZZATI DA MOVIMENTO DI COMUNICAZIONE
      *----------------------------------------------------------------*
       LETTURA-D-CRIST.

           SET FINE-LETT-P61-NO           TO TRUE

           INITIALIZE D-CRIST

           MOVE 0 TO DP61-ELE-D-CRIST-MAX
           MOVE 0 TO IX-TAB-P61

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

      *
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR FINE-LETT-P61-SI

              INITIALIZE D-CRIST
      *
              MOVE IVVC0213-ID-POLIZZA    TO P61-ID-POLI

              SET IDSV0003-TRATT-X-COMPETENZA TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSV0003-ID-PADRE        TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL TO TRUE

              CALL  IDBSP610   USING IDSV0003 D-CRIST
                ON EXCEPTION
                   MOVE IDBSP610          TO IDSV0003-COD-SERVIZIO-BE
                   MOVE 'CALL-IDBSP610 ERRORE CHIAMATA'
                     TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER   TO TRUE
              END-CALL


                IF IDSV0003-SUCCESSFUL-RC

                    EVALUATE TRUE
                        WHEN IDSV0003-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA

                          SET FINE-LETT-P61-SI           TO TRUE
37049                     IF NOT IDSV0003-FETCH-FIRST
37049                        SET IDSV0003-SUCCESSFUL-SQL TO TRUE
37049                        SET IDSV0003-SUCCESSFUL-RC  TO TRUE
37049                     END-IF

                        WHEN IDSV0003-SUCCESSFUL-SQL
      *-->   OPERAZIONE ESEGUITA CORRETTAMENTE
                             ADD 1 TO DP61-ELE-D-CRIST-MAX
                                      IX-TAB-P61

                             PERFORM VALORIZZA-OUTPUT-P61
                                THRU VALORIZZA-OUTPUT-P61-EX

                             SET IDSV0003-FETCH-NEXT   TO TRUE

                        WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB
                             MOVE IDBSP610
                               TO IDSV0003-COD-SERVIZIO-BE
                             MOVE 'CALL-IDBSP610 ERRORE CHIAMATA'
                               TO IDSV0003-DESCRIZ-ERR-DB2
                             SET IDSV0003-INVALID-OPER   TO TRUE
                             SET FINE-LETT-P61-SI TO TRUE
                    END-EVALUATE
                 ELSE
      *--> GESTIRE ERRORE
                    MOVE IDBSP610
                      TO IDSV0003-COD-SERVIZIO-BE
                    MOVE 'CALL-IDBSP610 ERRORE CHIAMATA'
                      TO IDSV0003-DESCRIZ-ERR-DB2
                    SET IDSV0003-INVALID-OPER   TO TRUE
                    SET FINE-LETT-P61-SI TO TRUE
                 END-IF
           END-PERFORM.

       LETTURA-D-CRIST-EX.
           EXIT.
MIGCOL*----------------------------------------------------------------*
MIGCOL*--> PER LE COLLETTIVE VIENE LETTA D CRISTALLIZATI PER ADESIONE
MIGCOL*----------------------------------------------------------------*
MIGCOL LEGGI-D-CRIST-X-ADE.
MIGCOL     SET FINE-LETT-P61-NO           TO TRUE

MIGCOL     INITIALIZE D-CRIST
MIGCOL     MOVE ZEROES
MIGCOL       TO WK-TOTALE

MIGCOL*--> TIPO OPERAZIONE
MIGCOL     SET IDSV0003-FETCH-FIRST       TO TRUE

MIGCOL*--> INIZIALIZZA CODICE DI RITORNO
MIGCOL     SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
MIGCOL     SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

MIGCOL*
MIGCOL     PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
MIGCOL                OR FINE-LETT-P61-SI

MIGCOL        INITIALIZE D-CRIST
MIGCOL*
MIGCOL        MOVE IVVC0213-ID-ADESIONE   TO P61-ID-ADES

MIGCOL        SET IDSV0003-TRATT-X-COMPETENZA TO TRUE

MIGCOL*--> LIVELLO OPERAZIONE
MIGCOL        SET IDSV0003-WHERE-CONDITION    TO TRUE

MIGCOL*--> INIZIALIZZA CODICE DI RITORNO
MIGCOL        SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
MIGCOL        SET  IDSV0003-SUCCESSFUL-SQL TO TRUE

MIGCOL        CALL  LDBSH650   USING IDSV0003 D-CRIST
MIGCOL          ON EXCEPTION
MIGCOL             MOVE LDBSH650          TO IDSV0003-COD-SERVIZIO-BE
MIGCOL             MOVE 'CALL-LDBSH650 ERRORE CHIAMATA'
MIGCOL               TO IDSV0003-DESCRIZ-ERR-DB2
MIGCOL             SET IDSV0003-INVALID-OPER   TO TRUE
MIGCOL        END-CALL


MIGCOL          IF IDSV0003-SUCCESSFUL-RC

MIGCOL              EVALUATE TRUE
MIGCOL                  WHEN IDSV0003-NOT-FOUND
MIGCOL*-->    NESSUN DATO IN TABELLA

MIGCOL                    SET FINE-LETT-P61-SI           TO TRUE
MIGCOL                    IF NOT IDSV0003-FETCH-FIRST
MIGCOL                       SET IDSV0003-SUCCESSFUL-SQL TO TRUE
MIGCOL                       SET IDSV0003-SUCCESSFUL-RC  TO TRUE
MIGCOL                    END-IF

MIGCOL                  WHEN IDSV0003-SUCCESSFUL-SQL
MIGCOL*-->   OPERAZIONE ESEGUITA CORRETTAMENTE
MIGCOL                   IF P61-IMPB-VIS-RP-P2011-NULL NOT = HIGH-VALUE
MIGCOL                                                 AND LOW-VALUES
MIGCOL                                                 AND SPACES

MIGCOL                       ADD P61-IMPB-VIS-RP-P2011
MIGCOL                         TO WK-TOTALE
MIGCOL                   END-IF
MIGCOL                       SET IDSV0003-FETCH-NEXT   TO TRUE

MIGCOL                  WHEN OTHER
MIGCOL*--->   ERRORE DI ACCESSO AL DB
MIGCOL                       MOVE LDBSH650
MIGCOL                         TO IDSV0003-COD-SERVIZIO-BE
MIGCOL                       MOVE 'CALL-LDBSH650 ERRORE CHIAMATA'
MIGCOL                         TO IDSV0003-DESCRIZ-ERR-DB2
MIGCOL                       SET IDSV0003-INVALID-OPER   TO TRUE
MIGCOL                       SET FINE-LETT-P61-SI TO TRUE
MIGCOL              END-EVALUATE
MIGCOL           ELSE
MIGCOL*--> GESTIRE ERRORE
MIGCOL              MOVE LDBSH650
MIGCOL                TO IDSV0003-COD-SERVIZIO-BE
MIGCOL              MOVE 'CALL-LDBSH650 ERRORE CHIAMATA'
MIGCOL                TO IDSV0003-DESCRIZ-ERR-DB2
MIGCOL              SET IDSV0003-INVALID-OPER   TO TRUE
MIGCOL              SET FINE-LETT-P61-SI TO TRUE
MIGCOL           END-IF
MIGCOL     END-PERFORM.
MIGCOL LEGGI-D-CRIST-X-ADE-EX.
MIGCOL     EXIT.
      *----------------------------------------------------------------*
      *    CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
      *----------------------------------------------------------------*
       CLOSE-MOVI.

           SET IDSV0003-CLOSE-CURSOR            TO TRUE.

           CALL LDBS6040 USING IDSV0003 MOVI
           ON EXCEPTION
              MOVE LDBS6040
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CALL LDBS6040 CLOSE'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER TO TRUE
           END-CALL.

           IF NOT IDSV0003-SUCCESSFUL-RC
           OR NOT IDSV0003-SUCCESSFUL-SQL
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ERRORE CLOSE CURSORE LDBS6040'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER
                TO TRUE
           END-IF.
       CLOSE-MOVI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           MOVE SPACES                     TO IVVC0213-VAL-STR-O.
           MOVE 0                          TO IVVC0213-VAL-PERC-O.

           IF COMUN-TROV-SI
              MOVE WK-DATA-CPTZ-RIP
                TO IDSV0003-DATA-COMPETENZA

              MOVE WK-DATA-EFF-RIP
                TO IDSV0003-DATA-INIZIO-EFFETTO
                   IDSV0003-DATA-FINE-EFFETTO
           END-IF
      *
           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *   COPY D-CRIST
      *----------------------------------------------------------------*

            COPY LCCVP613               REPLACING ==(SF)== BY ==DP61==.
            COPY LCCVP614               REPLACING ==(SF)== BY ==DP61==.


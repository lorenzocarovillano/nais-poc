      *****************************************************************
      *                                                               *
      *                   IDENTIFICATION DIVISION                     *
      *                                                               *
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDSS0140 IS INITIAL.
       AUTHOR.        ACCENTURE.
       DATE-WRITTEN.  04/12/2006.

      *****************************************************************
      *                                                               *
      *    NOME :           IDSS0140                                  *
      *    TIPO :           ROUTINE COBOL                             *
      *    DESCRIZIONE :    ROUTINE PER LA CONVERSIONE DEI DATI       *
      *                     TRA IL DB NAIS E I FILE VSAM LIFE         *
      *                     NUMERO --> NUMERO                         *
      *                                                               *
      *    AREE DI PASSAGGIO DATI                                     *
      *                                                               *
      *    DATI DI INPUT : IDSV0141                                   *
      *    DATI DI OUTPUT: IDSV0141                                   *
      *                                                               *
      *****************************************************************
      * --- TABELLINA DI DECODIFICA DATI DA VSAM A DB2 --- *
      * --- operazione di PRESA IN CARICO
      * -- ALFA NUMERICI
      * PIC  X(001)               PIC S9(002)V     COMP-3
      * PIC  X(003)               PIC  X(020)
      * PIC  X(005)               PIC S9(005)V     COMP-3
      * -- NUMERICI INTERI NON SEGNATI
      * PIC  9(002)               PIC S9(003)V9(3) COMP-3
      * PIC  9(002)               PIC S9(005)V     COMP-3
      * PIC  9(002)               PIC S9(006)V     COMP-3
      * PIC  9(002)V9(3)          PIC S9(003)V9(3) COMP-3
      * PIC  9(003)               PIC S9(005)V     COMP-3
      * PIC  9(004)               PIC S9(003)V     COMP-3
      * PIC  9(004)               PIC S9(005)V     COMP-3
      * PIC  9(004)               PIC  X(040)
      * PIC  9(008)               PIC S9(008)V     COMP-3
      * -- NUMERICI COMP-3
      * PIC  9(002)V9(2) COMP-3   PIC S9(003)V9(3) COMP-3
      * PIC  9(002)V9(3) COMP-3   PIC S9(003)V9(3) COMP-3
      * PIC  9(003)      COMP-3   PIC  X(020)
      * PIC  9(003)      COMP-3   PIC S9(005)V     COMP-3
      * PIC  9(003)V9(2) COMP-3   PIC S9(003)V9(3) COMP-3
      * PIC  9(003)V9(3) COMP-3   PIC S9(003)V9(2) COMP-3
      * PIC  9(005)      COMP-3   PIC  X(012)
      * PIC  9(005)      COMP-3   PIC  X(020)
      * PIC  9(005)      COMP-3   PIC S9(003)V9(3) COMP-3
      * PIC  9(005)V9(3) COMP-3   PIC S9(012)V9(3) COMP-3
      * PIC  9(007)V9(3) COMP-3   PIC S9(012)V9(3) COMP-3
      * PIC  9(007)      COMP-3   PIC S9(009)V     COMP-3
      * PIC  9(008)V9(3) COMP-3   PIC S9(012)V9(3) COMP-3
      * PIC S9(008)      COMP-3   PIC  X(020)
      * PIC  9(009)      COMP-3   PIC S9(005)V     COMP-3
      * PIC  9(009)      COMP-3   PIC  X(040)
      * PIC  9(009)V9(3) COMP-3   PIC S9(012)V9(3) COMP-3
      * PIC  9(011)V9(3) COMP-3   PIC S9(012)V9(3) COMP-3
      * -------------------------------------------------- *
      * -------------------------------------------------- *
      * --- TABELLINA DI DECODIFICA DATI DA DB2 A VSAM --- *
      * -- ALFA NUMERICI
      * PIC  X(012)               PIC  9(005)      COMP-3
      * PIC  X(020)               PIC  X(003)
      * PIC  X(020)               PIC  9(003)      COMP-3
      * PIC  X(020)               PIC  9(005)      COMP-3
      * PIC  X(020)               PIC S9(008)      COMP-3
      * PIC  X(040)               PIC  9(004)
      * PIC  X(040)               PIC  9(009)      COMP-3
      * -- NUMERICI COMP-3
      * PIC S9(002)V     COMP-3   PIC  X(001)
      * PIC S9(003)V9(2) COMP-3   PIC  9(003)V9(3) COMP-3
      * PIC S9(003)V9(3) COMP-3   PIC  9(002)
      * PIC S9(003)V9(3) COMP-3   PIC  9(002)V9(3)
      * PIC S9(003)V9(3) COMP-3   PIC  9(002)V9(2) COMP-3
      * PIC S9(003)V9(3) COMP-3   PIC  9(002)V9(3) COMP-3
      * PIC S9(003)V9(3) COMP-3   PIC  9(003)V9(2) COMP-3
      * PIC S9(003)V9(3) COMP-3   PIC  9(004)      COMP-3
      * PIC S9(003)V9(3) COMP-3   PIC  9(005)      COMP-3
      * PIC S9(005)V     COMP-3   PIC  X(005)
      * PIC S9(005)V     COMP-3   PIC  9(002)
      * PIC S9(005)V     COMP-3   PIC  9(003)
      * PIC S9(005)V     COMP-3   PIC  9(003)      COMP-3
      * PIC S9(005)V     COMP-3   PIC  9(004)
      * PIC S9(005)V     COMP-3   PIC  9(009)      COMP-3
      * PIC S9(006)V     COMP-3   PIC  9(002)
      * PIC S9(008)V     COMP-3   PIC  9(008)
      * PIC S9(009)V     COMP-3   PIC  9(007)      COMP-3
      * PIC S9(012)V9(3) COMP-3   PIC  9(005)V9(3) COMP-3
      * PIC S9(012)V9(3) COMP-3   PIC  9(007)V9(3) COMP-3
      * PIC S9(012)V9(3) COMP-3   PIC  9(008)V9(3) COMP-3
      * PIC S9(012)V9(3) COMP-3   PIC  9(009)V9(3) COMP-3
      * PIC S9(012)V9(3) COMP-3   PIC  9(011)V9(3) COMP-3
      * -------------------------------------------------- *
      *****************************************************************
      *****************************************************************
      *                                                               *
      *                   ENVIRONMENT  DIVISION                       *
      *                                                               *
      *****************************************************************
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
      *SOURCE-COMPUTER. IBM-370 WITH DEBUGGING MODE.
      *OBJECT-COMPUTER. IBM-370.
       SPECIAL-NAMES. DECIMAL-POINT IS COMMA.


      *****************************************************************
      *                                                               *
      *                   DATA  DIVISION                              *
      *                                                               *
      *****************************************************************
       DATA DIVISION.

      *****************************************************************
      *                                                               *
      *                   WORKING STORAGE SECTION                     *
      *                                                               *
      *****************************************************************
       WORKING-STORAGE SECTION.
      *****************************************************************
      *                                                               *
      *                   FLAG PER ESITO ELABORAZIONE                 *
      *                                                               *
      *****************************************************************
      *****************************************************************
      *                                                               *
      *                   CAMPI NUMERICI                              *
      *                                                               *
      *****************************************************************

       01 WS-CAMPI-APPOGGIO.
          02 PARTE-INTERA-MITT                PIC  9(003).
          02 PARTE-INTERA-DEST                PIC  9(003).

          02 WS-NUM-DI-1-STR.
             05 WS-NUM-DI-1                   PIC  9(001).
             05 FILLER                        PIC  X(039).

          02 WS-NUM-DI-3-STR.
             05 WS-NUM-DI-3                   PIC  9(003).
             05 FILLER                        PIC  X(037).

          02 WS-NUM-DI-5-STR.
             05 WS-NUM-DI-5                   PIC  9(005).
             05 FILLER                        PIC  X(035).

          02 WS-NUM-DI-8-STR.
             05 WS-NUM-DI-8                   PIC  9(008).
             05 FILLER                        PIC  X(032).

          02 WS-NUM-DI-9-STR.
             05 WS-NUM-DI-9                   PIC  9(009).
             05 FILLER                        PIC  X(031).

       01 CAMPI-NUMERICI.
          02 X-DI-1.
             05 CHR-X-DI-1                    PIC  X(001).
             05 FILLER                        PIC  X(039).

          02 X-DI-3.
             05 CHR-X-DI-3                    PIC  X(003).
             05 FILLER                        PIC  X(037).

          02 X-DI-20.
             05 CHR-X-DI-20                   PIC  X(020).
             05 FILLER                        PIC  X(020).

          02 X-DI-40.
             05 CHR-X-DI-40                   PIC  X(040).

          02 NOVE-DI-2.
             05 NUM-NOVE-DI-2                 PIC  9(002).
             05 FILLER                        PIC  X(038).

          02 S-NOVE-DI-1-V-COMP.
             05 NUM-S-NOVE-DI-1-V-COMP        PIC  S9(001)V.
             05 FILLER                        PIC  X(039).

          02 NOVE-DI-2-V2-COMP.
             05 NUM-NOVE-DI-2-V2-COMP         PIC  9(002)V9(2)  COMP-3.
             05 FILLER                        PIC  X(038).

          02 NOVE-DI-2-V3-COMP.
             05 NUM-NOVE-DI-2-V3-COMP         PIC  9(002)V9(3)  COMP-3.
             05 FILLER                        PIC  X(037).

          02 NOVE-DI-3-COMP.
             05 NUM-NOVE-DI-3-COMP            PIC  9(003)       COMP-3.
             05 FILLER                        PIC  X(038).

          02 NOVE-DI-3-V2-COMP.
             05 NUM-NOVE-DI-3-V2-COMP         PIC  9(003)V9(2)  COMP-3.
             05 FILLER                        PIC  X(037).

          02 NOVE-DI-3-V3-COMP.
             05 NUM-NOVE-DI-3-V3-COMP         PIC  9(003)V9(3)  COMP-3.
             05 FILLER                        PIC  X(037).

          02 NOVE-DI-5-COMP.
             05 NUM-NOVE-DI-5-COMP            PIC  9(005)       COMP-3.
             05 FILLER                        PIC  X(037).

          02 NOVE-DI-5-V3-COMP.
             05 NUM-NOVE-DI-5-V3-COMP         PIC  9(005)V9(3)  COMP-3.
             05 FILLER                        PIC  X(036).

          02 NOVE-DI-7-V3-COMP.
             05 NUM-NOVE-DI-7-V3-COMP         PIC  9(007)V9(3)  COMP-3.
             05 FILLER                        PIC  X(035).

          02 NOVE-DI-7-COMP.
             05 NUM-NOVE-DI-7-COMP            PIC  9(007)       COMP-3.
             05 FILLER                        PIC  X(036).

          02 NOVE-DI-8-V3-COMP.
             05 NUM-NOVE-DI-8-V3-COMP         PIC  9(008)V9(3)  COMP-3.
             05 FILLER                        PIC  X(034).

          02 NOVE-DI-9-COMP.
             05 NUM-NOVE-DI-9-COMP            PIC  9(009)       COMP-3.
             05 FILLER                        PIC  X(035).

          02 NOVE-DI-9-V3-COMP.
             05 NUM-NOVE-DI-9-V3-COMP         PIC  9(009)V9(3)  COMP-3.
             05 FILLER                        PIC  X(034).

          02 NOVE-DI-11-V3-COMP.
             05 NUM-NOVE-DI-11-V3-COMP        PIC  9(011)V9(3)  COMP-3.
             05 FILLER                        PIC  X(033).

          02 S-NOVE-DI-2-V-COMP.
             05 NUM-S-NOVE-DI-2-V-COMP        PIC  S9(002)      COMP-3.
             05 FILLER                        PIC  X(038).

          02 S-NOVE-DI-2-V2-COMP.
             05 NUM-S-NOVE-DI-2-V2-COMP       PIC  S9(002)V9(2)  COMP-3.
             05 FILLER                        PIC  X(038).

          02 S-NOVE-DI-2-V3-COMP.
             05 NUM-S-DI-2-V3-COMP            PIC  S9(002)V9(3)  COMP-3.
             05 FILLER                        PIC  X(037).

          02 S-NOVE-DI-3-V-COMP.
             05 NUM-S-NOVE-DI-3-V-COMP        PIC  S9(003)       COMP-3.
             05 FILLER                        PIC  X(038).

          02 S-NOVE-DI-3-V2-COMP.
             05 NUM-S-NOVE-DI-3-V2-COMP       PIC  S9(003)V9(2)  COMP-3.
             05 FILLER                        PIC  X(037).

          02 S-NOVE-DI-3-V3-COMP.
             05 NUM-S-NOVE-DI-3-V3-COMP       PIC  S9(003)V9(3)  COMP-3.
             05 FILLER                        PIC  X(037).

           02 S-NOVE-DI-4-V-COMP.
             05 NUM-S-NOVE-DI-4-V-COMP        PIC  S9(004)       COMP-3.
             05 FILLER                        PIC  X(037).

          02 S-NOVE-DI-5-V-COMP.
             05 NUM-S-NOVE-DI-5-V-COMP        PIC  S9(005)       COMP-3.
             05 FILLER                        PIC  X(037).

          02 S-NOVE-DI-5-V9-COMP.
             05 NUM-S-NOVE-DI-5-V9-COMP       PIC  S9(005)V9(9)  COMP-3.
             05 FILLER                        PIC  X(032).

          02 S-NOVE-DI-6-V-COMP.
             05 NUM-S-NOVE-DI-6-V-COMP        PIC  S9(006)       COMP-3.
             05 FILLER                        PIC  X(036).

          02 S-NOVE-DI-5-V3-COMP.
             05 NUM-S-NOVE-DI-5-V3-COMP       PIC  S9(005)V9(3)  COMP-3.
             05 FILLER                        PIC  X(035).

          02 S-NOVE-DI-7-V3-COMP.
             05 NUM-S-NOVE-DI-7-V3-COMP       PIC  S9(007)V9(3)  COMP-3.
             05 FILLER                        PIC  X(034).

          02 S-NOVE-DI-7-V5-COMP.
             05 NUM-S-NOVE-DI-7-V5-COMP       PIC  S9(007)V9(5)  COMP-3.
             05 FILLER                        PIC  X(033).

          02 S-NOVE-DI-7-COMP.
             05 NUM-S-NOVE-DI-7-COMP          PIC  S9(007)       COMP-3.
             05 FILLER                        PIC  X(036).

          02 S-NOVE-DI-8-V3-COMP.
             05 NUM-S-NOVE-DI-8-V3-COMP       PIC  S9(008)V9(3)  COMP-3.
             05 FILLER                        PIC  X(034).

          02 S-NOVE-DI-8-V-COMP.
             05 NUM-S-NOVE-DI-8-V-COMP        PIC  S9(008)  COMP-3.
             05 FILLER                        PIC  X(035).

          02 S-NOVE-DI-10-V-COMP.
             05 NUM-S-NOVE-DI-10-V-COMP       PIC  S9(010)  COMP-3.
             05 FILLER                        PIC  X(034).

          02 S-NOVE-DI-9-V-COMP.
             05 NUM-S-NOVE-DI-9-V-COMP        PIC  S9(009)  COMP-3.
             05 FILLER                        PIC  X(035).

          02 S-NOVE-DI-11-V3-COMP.
             05 NUM-S-NOVE-DI-11-V3-COMP      PIC  S9(011)V9(3) COMP-3.
             05 FILLER                        PIC  X(032).

          02 S-NOVE-DI-12-V3-COMP.
             05 NUM-S-NOVE-DI-12-V3-COMP      PIC  S9(012)V9(3) COMP-3.
             05 FILLER                        PIC  X(032).

           02 S-NOVE-DI-1-V.
             05 NUM-S-NOVE-DI-1-V             PIC  S9(001)V.
             05 FILLER                        PIC  X(039).

          02 S-NOVE-DI-2-V.
             05 NUM-S-NOVE-DI-2-V             PIC  S9(002)V.
             05 FILLER                        PIC  X(038).

          02 S-NOVE-DI-2-V2.
             05 NUM-S-NOVE-DI-2-V2            PIC  S9(002)V9(2).
             05 FILLER                        PIC  X(037).

          02 S-NOVE-DI-3-V.
             05 NUM-S-NOVE-DI-3-V             PIC  S9(003)V .
             05 FILLER                        PIC  X(038).

          02 S-NOVE-DI-3-V2.
             05 NUM-S-NOVE-DI-3-V2            PIC  S9(003)V9(2).
             05 FILLER                        PIC  X(037).

          02 S-NOVE-DI-5-V.
             05 NUM-S-NOVE-DI-5-V             PIC  S9(005)V.
             05 FILLER                        PIC  X(037).

          02 S-NOVE-DI-5-V9.
             05 NUM-S-NOVE-DI-5-V9            PIC  S9(005)V9(9).
             05 FILLER                        PIC  X(032).

          02 S-NOVE-DI-3-V2.
             05 NUM-S-NOVE-DI-3-V2            PIC  S9(003)V9(2).
             05 FILLER                        PIC  X(037).

          02 S-NOVE-DI-3-V3.
             05 NUM-S-NOVE-DI-3-V3            PIC  S9(003)V9(3).
             05 FILLER                        PIC  X(036).

          02 S-NOVE-DI-6-V.
             05 NUM-S-NOVE-DI-6-V             PIC  S9(006)V.
             05 FILLER                        PIC  X(036).

          02 S-NOVE-DI-8-V.
             05 NUM-S-NOVE-DI-8-V             PIC  S9(008)V.
             05 FILLER                        PIC  X(035).

          02 S-NOVE-DI-9-V.
             05 NUM-S-NOVE-DI-9-V             PIC  S9(009)V.
             05 FILLER                        PIC  X(035).

          02 S-NOVE-DI-8-V3.
             05 NUM-S-NOVE-DI-8-V3            PIC  S9(008)V9(3).
             05 FILLER                        PIC  X(028).

          02 S-NOVE-DI-11-COMP.
             05 NUM-S-NOVE-DI-11-COMP         PIC  S9(011).
             05 FILLER                        PIC  X(034).

          02 S-NOVE-DI-12-V3.
             05 NUM-S-NOVE-DI-12-V3           PIC  S9(012)V9(3).
             05 FILLER                        PIC  X(024).

          02 S-NOVE-DI-3-V7.
             05 NUM-S-NOVE-DI-3-V7           PIC  S9(003)V9(7).
             05 FILLER                        PIC  X(29).


      *****************************************************************
      *                                                               *
      *                   LINKAGE SECTION                             *
      *                                                               *
      *****************************************************************
       LINKAGE SECTION.

      *-- COMMAREA PER CHIAMATA DA CICS
       01 IDSS0140-LINK.
           COPY IDSV0141.

      *****************************************************************
      *                                                               *
      *                   PROCEDURE DIVISION                          *
      *                                                               *
      *****************************************************************
      *
       PROCEDURE DIVISION USING IDSS0140-LINK.

      *****************************************************************
      *                                                               *
      *                   000-PRINCIPALE                              *
      *                                                               *
      *****************************************************************
       000-PRINCIPALE.

      *-- INIZIALIZZO IL FLAG PER L'ESITO DELL'ELABORAZIONE
           SET IDSV0141-SUCCESSFUL-RC   TO TRUE.

           INITIALIZE                      IDSV0141-CAMPO-DEST
                                           CAMPI-NUMERICI
                                           WS-CAMPI-APPOGGIO.
           SET  IDSV0141-SUCCESSFUL-RC  TO TRUE

           IF IDSV0141-CAMPO-MITT(1:IDSV0141-LUNGHEZZA-DATO-STR)
              = HIGH-VALUE OR SPACES OR LOW-VALUE
              IF IDSV0141-MITT-ALFANUM
                 MOVE ALL ZERO          TO IDSV0141-CAMPO-MITT
              ELSE
                 MOVE 'FIELD-NOT-VALUED' TO IDSV0141-DESCRIZ-ERR-DB2
                 MOVE 'IDSS0140'         TO IDSV0141-COD-SERVIZIO-BE
                 SET IDSV0141-UNSUCCESSFUL-RC         TO TRUE
              END-IF
           END-IF.
      *
           IF IDSV0141-SUCCESSFUL-RC
              COMPUTE PARTE-INTERA-MITT = IDSV0141-LUNGHEZZA-DATO-MITT
                                       - IDSV0141-PRECISIONE-DATO-MITT
      *
              PERFORM 200-VALORIZZA-CAMPI
                 THRU 200-VALORIZZA-CAMPI-EX
           END-IF.

       999-FINE.
           GOBACK.

      *-- TRASFORMAZIONE DA NAIS A LIFE
       200-VALORIZZA-CAMPI.
      *
           MOVE 'IDSS0140'             TO IDSV0141-COD-SERVIZIO-BE
           SET IDSV0141-UNSUCCESSFUL-RC              TO TRUE

           IF IDSV0141-MITT-COMP-3
              EVALUATE PARTE-INTERA-MITT
                   WHEN 1
                      IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                         MOVE IDSV0141-CAMPO-MITT
                                        TO S-NOVE-DI-1-V-COMP
                         MOVE NUM-S-NOVE-DI-1-V-COMP
                                        TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                  WHEN 2
                      IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                         MOVE IDSV0141-CAMPO-MITT
                                        TO S-NOVE-DI-2-V-COMP
                         MOVE NUM-S-NOVE-DI-2-V-COMP
                                        TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                  WHEN 3
                  WHEN 4
                      IF IDSV0141-PRECISIONE-DATO-MITT = 3
                         MOVE IDSV0141-CAMPO-MITT
                           TO S-NOVE-DI-3-V3-COMP
                         MOVE NUM-S-NOVE-DI-3-V3-COMP
                           TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                      IF IDSV0141-PRECISIONE-DATO-MITT = 0
                         MOVE IDSV0141-CAMPO-MITT
                                        TO S-NOVE-DI-3-V-COMP
                         MOVE NUM-S-NOVE-DI-3-V-COMP
                                        TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                         TO TRUE
                      END-IF

                  WHEN 5
                      IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                         MOVE IDSV0141-CAMPO-MITT
                           TO S-NOVE-DI-5-V-COMP
                         MOVE NUM-S-NOVE-DI-5-V-COMP
                           TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                      IF IDSV0141-PRECISIONE-DATO-MITT = 9
                         MOVE IDSV0141-CAMPO-MITT
                           TO S-NOVE-DI-5-V9-COMP
                         MOVE NUM-S-NOVE-DI-5-V9-COMP
                           TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                  WHEN 6
                      IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                         MOVE IDSV0141-CAMPO-MITT
                           TO S-NOVE-DI-6-V-COMP
                         MOVE NUM-S-NOVE-DI-6-V-COMP
                                        TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                  WHEN 7
                     IF IDSV0141-PRECISIONE-DATO-MITT = 5
                        MOVE IDSV0141-CAMPO-MITT
                          TO S-NOVE-DI-7-V5-COMP
                        MOVE NUM-S-NOVE-DI-7-V5-COMP
                                       TO IDSV0141-CAMPO-DEST
                        SET  IDSV0141-SUCCESSFUL-RC
                                       TO TRUE
                     END-IF

                  WHEN 8
                      IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                         MOVE IDSV0141-CAMPO-MITT
                                        TO S-NOVE-DI-8-V-COMP
                         MOVE NUM-S-NOVE-DI-8-V-COMP
                                        TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                  WHEN 9
                      IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                         MOVE IDSV0141-CAMPO-MITT
                                        TO S-NOVE-DI-9-V-COMP
                         MOVE NUM-S-NOVE-DI-9-V-COMP
                                        TO IDSV0141-CAMPO-DEST
                         SET IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                  WHEN 10
                      IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                         MOVE IDSV0141-CAMPO-MITT
                                        TO S-NOVE-DI-10-V-COMP
                         MOVE NUM-S-NOVE-DI-10-V-COMP
                                        TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                  WHEN 12
                      IF IDSV0141-PRECISIONE-DATO-MITT = 3
                         MOVE IDSV0141-CAMPO-MITT
                           TO S-NOVE-DI-12-V3-COMP
                         MOVE NUM-S-NOVE-DI-12-V3-COMP
                           TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
              END-EVALUATE
           ELSE
              EVALUATE PARTE-INTERA-MITT
                  WHEN 1
                      IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                         MOVE IDSV0141-CAMPO-MITT
                                        TO S-NOVE-DI-1-V
                         MOVE NUM-S-NOVE-DI-1-V
                                        TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                  WHEN 2
                      IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                         MOVE IDSV0141-CAMPO-MITT
                                        TO S-NOVE-DI-2-V
                         MOVE NUM-S-NOVE-DI-2-V
                                        TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                  WHEN 3
                      EVALUATE IDSV0141-PRECISIONE-DATO-MITT
                       WHEN 3
                         MOVE IDSV0141-CAMPO-MITT
                           TO S-NOVE-DI-3-V3
                         MOVE NUM-S-NOVE-DI-3-V3
                           TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE

                       WHEN 0
                         MOVE IDSV0141-CAMPO-MITT
                                        TO S-NOVE-DI-3-V
                         MOVE NUM-S-NOVE-DI-3-V-COMP
                                        TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                         TO TRUE
                      WHEN 7
                         MOVE IDSV0141-CAMPO-MITT
                           TO S-NOVE-DI-3-V7
                         MOVE NUM-S-NOVE-DI-3-V7
                           TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-EVALUATE
                  WHEN 5
                      IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                         MOVE IDSV0141-CAMPO-MITT
                           TO S-NOVE-DI-5-V
                         MOVE NUM-S-NOVE-DI-5-V
                           TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                      IF IDSV0141-PRECISIONE-DATO-MITT = 9
                         MOVE IDSV0141-CAMPO-MITT
                           TO S-NOVE-DI-5-V9
                         MOVE NUM-S-NOVE-DI-5-V9
                           TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                  WHEN 6
                      IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                         MOVE IDSV0141-CAMPO-MITT
                           TO S-NOVE-DI-6-V
                         MOVE NUM-S-NOVE-DI-6-V
                                        TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                  WHEN 8
                  WHEN 10
                      IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                         MOVE IDSV0141-CAMPO-MITT
                                        TO S-NOVE-DI-8-V
                         MOVE NUM-S-NOVE-DI-8-V
                                        TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                  WHEN 9
                      IF IDSV0141-PRECISIONE-DATO-MITT = ZERO
                         MOVE IDSV0141-CAMPO-MITT
                                        TO S-NOVE-DI-9-V
                         MOVE NUM-S-NOVE-DI-9-V
                                        TO IDSV0141-CAMPO-DEST
                         SET IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
                  WHEN 12
                      IF IDSV0141-PRECISIONE-DATO-MITT = 3
                         MOVE IDSV0141-CAMPO-MITT
                           TO S-NOVE-DI-12-V3
                         MOVE NUM-S-NOVE-DI-12-V3
                           TO IDSV0141-CAMPO-DEST
                         SET  IDSV0141-SUCCESSFUL-RC
                                        TO TRUE
                      END-IF
              END-EVALUATE
           END-IF.

       200-VALORIZZA-CAMPI-EX.
           EXIT.

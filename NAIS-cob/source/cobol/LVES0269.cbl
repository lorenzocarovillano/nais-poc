      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA  VER 1.0                          **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVES0269.
       AUTHOR.             ATS.
       DATE-WRITTEN.       Marzo 2008.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *    PROGRAMMA ..... LVES0269
      *    TIPOLOGIA...... PREPARA MAPPA
      *    PROCESSO....... XXX
      *    FUNZIONE....... XXX
      *    DESCRIZIONE.... CREAZIONE PROPOSTA INDIVIDUALE
      *    PAGINA WEB..... CALCOLI
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
      *INPUT-OUTPUT SECTION.
      *FILE-CONTROL.
       DATA DIVISION.
      *FILE SECTION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(08) VALUE 'LVES0269'.
       01  WK-TABELLA                       PIC X(08) VALUE SPACES.

      *--  DIFFERENZA IN GIORNI DATE
       01  LCCS0003                         PIC X(8) VALUE 'LCCS0003'.
       01  LVES0245                         PIC X(8) VALUE 'LVES0245'.
       01  LCCS0062                         PIC X(8) VALUE 'LCCS0062'.
       01  LCCS0033                         PIC X(8) VALUE 'LCCS0033'.
       01  LCCS0490                         PIC X(8) VALUE 'LCCS0490'.
       77  IDSS0300                         PIC X(008) VALUE 'IDSS0300'.
12868  77  LCCS1900                         PIC X(008) VALUE 'LCCS1900'.
       77  ISPS0140                         PIC X(008) VALUE 'ISPS0140'.

      *----------------------------------------------------------------*
      *    Aree Input/Output servizi
      *----------------------------------------------------------------*
       01  AREA-IO-LCCS0033.
           COPY LCCC0033.

      * Area servizio calcolo Rateo
           COPY LCCC0490.

       01  WS-TP-DATO.
           05 WS-TASSO                     PIC X(01) VALUE 'A'.
           05 WS-IMPORTO                   PIC X(01) VALUE 'N'.
           05 WS-PERCENTUALE               PIC X(01) VALUE 'P'.
           05 WS-MILLESIMI                 PIC X(01) VALUE 'M'.
           05 WS-DATA                      PIC X(01) VALUE 'D'.
           05 WS-STRINGA                   PIC X(01) VALUE 'S'.
           05 WS-NUMERO                    PIC X(01) VALUE 'I'.
           05 WS-FLAG                      PIC X(01) VALUE 'F'.

       01  WS-COMPAGNIA                     PIC 9(05).
           88 UNIPOL                        VALUE 1.
           88 AURORA                        VALUE 6.

CAUSCO 01 WS-TP-LIVELLO.
          05 WS-LIV-PROD                 PIC X(01) VALUE 'P'.
          05 WS-LIV-GAR                  PIC X(01) VALUE 'G'.

AG     01 WS-TP-GAR                        PIC 9(02).
AG     01 WS-TP-TRCH                       PIC X(02).
      *----------------------------------------------------------------*
      *--  puntatore area prodotto
      *----------------------------------------------------------------*
       01  MQ01-ADDRESS.
           05  WS-ADDRESS USAGE POINTER.

      *--> SERVIZIO DI PRODOTTO
      *01  INTERF-MQSERIES              PIC X(08) VALUE 'IJCSMQ01'.
      *--> MAIN VALORIZZATORE VARIABILI
       01  IVVS0211                     PIC X(08) VALUE 'IVVS0211'.

       01  WK-CALCOLI-ELE-MAX           PIC 9(03) VALUE 5.
       01  WK-TIT-RATA-ELE-MAX          PIC 9(02) VALUE 12.
       01  WK-SOPR-GAR-ELE-MAX          PIC 9(03) VALUE 20.
       01  WK-ASSET-ELE-MAX             PIC 9(03) VALUE 250.

13382  01 WK-ELE-MAX-TIT-CONT                 PIC S9(04) VALUE 12.
13382  01 WK-ELE-MAX-TRCH-DI-GAR              PIC S9(04) VALUE 2.
13382  01 WK-ELE-MAX-PARAM-D-CALC             PIC S9(04) VALUE 20.
13382  01 WK-ELE-MAX-TIT-RAT                  PIC S9(04) VALUE 12.

       01 WK-ISPC0140-NUM-COMPON-MAX    PIC 9(003) VALUE 30.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-VARIABILI.
           03 WK-APPO-DT                   PIC  X(08).
           03 WK-APPO-DT-RED               REDEFINES
              WK-APPO-DT                   PIC  9(08).
BL7144     03 NEW-DT-VAL-QUOTE             PIC  X(08).
BL7144     03 NEW-DT-VAL-QUOTE-RED         REDEFINES
BL7144        NEW-DT-VAL-QUOTE             PIC  9(08).
           03 WK-APPO-LUNGHEZZA            PIC S9(09) COMP-3 VALUE ZERO.
           03 WK-ID-GAR                    PIC S9(9)  COMP-3 VALUE ZERO.
           03 WK-DT-DECORRENZA             PIC  9(8)  COMP-3 VALUE ZERO.
           03 WK-PREMIO-NEG                PIC S9(12)V9(3) COMP-3
                                                             VALUE ZERO.
           03 WK-DIFF-PROR                 PIC S9(02)V9(02).
           03 WK-DIFF-PROR-V REDEFINES WK-DIFF-PROR.
              05 WK-DIFF-AA                PIC 9(02).
              05 WK-DIFF-MM                PIC 9(02).
           03 WK-APPO-IAS                  PIC S9(11)V9(07).
           03 WK-DIFF-IAS-V REDEFINES WK-APPO-IAS.
              05 WK-IAS-1                  PIC 9(09).
              05 WK-IAS-2                  PIC 9(02).
              05 WK-IAS-DEC                PIC 9(07).
           03 WK-PARAMETRO                 PIC X(012).
              88 WK-PR-RATEANTIC             VALUE 'RATEANTIC'.
      *--  numero rate anticipate
RICH13     03 WK-RATE-ANTIC                PIC S9(14) COMP-3 VALUE ZERO.
      *--  numero rate da recuperare
RICH13     03 WK-RATE-RECUP                PIC S9(14) COMP-3 VALUE ZERO.
RICH13     03 WK-VAL-VAR                   PIC S9(14) COMP-3 VALUE ZERO.
RICH13     03 WK-DATA-LIMITE               PIC  9(8)  COMP-3 VALUE ZERO.
           03 WK-DATA-COMPETENZA           PIC  9(8)  COMP-3 VALUE ZERO.

RICH13     03 WK-FRQ-MOVI                  PIC 9(05).
RICH13     03 WK-FRAZ-MM                   PIC 9(05).
              88 WK-FRAZ-ANN                 VALUE 12.
              88 WK-FRAZ-MEN                 VALUE 1.
              88 WK-FRAZ-BIM                 VALUE 6.
              88 WK-FRAZ-TRI                 VALUE 4.
              88 WK-FRAZ-QUA                 VALUE 3.
              88 WK-FRAZ-SEM                 VALUE 2.

RICH13     03 WK-TP-RAT-PERF               PIC X(1).
              88 RECUPRATE-BATCH             VALUE 'B'.
              88 RECUPRATE-SEPARATE          VALUE 'S'.
              88 RECUPRATE-UNICA             VALUE 'U'.

           03 WK-TAB-GAR                   OCCURS 5.
              05 WK-COD-GAR                PIC  X(12).
              05 WK-TP-IAS                 PIC  X(02).

           03 WS-TOT-PREMIO-ANNUO          PIC S9(12)V9(3) COMP-3.
           03 WS-COD-TARIFFA               PIC X(12).
11174      03 WK-FL-INC-AUTOGEN            PIC X(1).
11174      03 WK-FL-INC-AUTOGEN-NULL       REDEFINES
11174         WK-FL-INC-AUTOGEN            PIC X(1).

      *----------------------------------------------------------------*
      *  --> Area per gestione della differenza tra date
      *----------------------------------------------------------------*
       01  FORMATO                         PIC X(01) VALUE ZEROES.
       01  DATA-INFERIORE.
           05 GG-INF                       PIC 9(02).
           05 MM-INF                       PIC 9(02).
           05 AAAA-INF                     PIC 9(04).
       01  DATA-SUPERIORE.
           05 GG-SUP                       PIC 9(02).
           05 MM-SUP                       PIC 9(02).
           05 AAAA-SUP                     PIC 9(04).
       01  XX-DIFF                         PIC 9(05).
       01  CODICE-RITORNO                  PIC X(01).
      *----------------------------------------------------------------*
      *    FLAG/SWITCH
      *----------------------------------------------------------------*
       01  WS-FLAG-RICERCA             PIC X(001).
           88 TROVATO                  VALUE 'S'.
           88 NON-TROVATO              VALUE 'N'.

       01 WS-FLAG-FINE-ELE             PIC 9(01).
           88 ALTRI-ELEMENTI           VALUE 0.
           88 FINE-ELEMENTI-ADESIONE   VALUE 1.
           88 FINE-ELEMENTI-TRANCHE    VALUE 2.

       01 WK-RIC-GRZ                   PIC X(01).
          88 WK-GRZ-TROVATA            VALUE 'S'.
          88 WK-GRZ-NON-TROVATA        VALUE 'N'.

       01 WK-RIC-TGA                   PIC X(01).
          88 WK-TGA-TROVATA            VALUE 'S'.
          88 WK-TGA-NON-TROVATA        VALUE 'N'.

       01 WK-RIC-SPG                   PIC X(01).
          88 WK-SPG-TROVATA            VALUE 'S'.
          88 WK-SPG-NON-TROVATA        VALUE 'N'.

       01 WK-FLG                       PIC  X(01).
          88 WK-TROVATO                VALUE 'S'.
          88 WK-NON-TROVATO            VALUE 'N'.

       01 WK-CICLO                     PIC  X(01) VALUE 'N'.
          88 FINE-CICLO-SI             VALUE 'S'.
          88 FINE-CICLO-NO             VALUE 'N'.

       01 WK-FLG-TRCH                  PIC  X(01).
          88 TRCH-NEG-SI               VALUE 'S'.
          88 TRCH-NEG-NO               VALUE 'N'.

       01 WK-QUESTSAN                  PIC X(01) VALUE 'N'.
          88 QUESTSAN-SI               VALUE 'S'.
          88 QUESTSAN-NO               VALUE 'N'.
          88 QUESTSAN-SPECIALE         VALUE 'F'.

       01 WK-VISITA-MEDICA             PIC 9(01).
          88 VMED-NON-PREVITA          VALUE 0.
          88 VMED-PREVISTA             VALUE 1.
          88 VMED-OBBLIGATORIA         VALUE 2.

       01 WK-QUIETANZAMENTO            PIC X(01) VALUE 'N'.
          88 QUIETANZAMENTO-SI         VALUE 'S'.
          88 QUIETANZAMENTO-NO         VALUE 'N'.

       01 WK-GETRA                     PIC X(01) VALUE 'N'.
          88 GETRA-SI                  VALUE 'S'.
          88 GETRA-NO                  VALUE 'N'.

       01  WK-COD-SOVRAP               PIC X(02).
           88 WK-SOPSAN                VALUE 'SA'.
           88 WK-SOPPRO                VALUE 'PR'.
           88 WK-SOPSPO                VALUE 'SP'.
           88 WK-SOPTEC                VALUE 'TE'.
           88 WK-SOPALT                VALUE 'AL'.

       01  WK-TP-FONDO                 PIC X(01).
           88 FND-UNIT-OICR            VALUE '0'.
           88 FND-UNIT-ASSIC           VALUE '1'.
           88 FND-INDEX                VALUE '2'.
           88 FND-GEST-SPECIALE        VALUE '3'.
           88 FND-SPEC-PROVVISTA       VALUE '4'.
           88 FND-INDICE               VALUE '5'.

AG     01 WK-FLAG-RATALO                    PIC X(01).
AG         88 FLAG-RATALO-SI                VALUE 'S'.
AG         88 FLAG-RATALO-NO                VALUE 'N'.

AG     01 WK-FLAG-GAR                       PIC X(01).
AG         88 FLAG-GAR-SI                   VALUE 'S'.
AG         88 FLAG-GAR-NO                   VALUE 'N'.

      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-TAB-GRZ                   PIC S9(04) COMP VALUE ZEROES.
           03 IX-TAB-TGA                   PIC S9(04) COMP VALUE ZEROES.
           03 IX-TAB-SPG                   PIC S9(04) COMP VALUE ZEROES.
           03 IX-TAB-ERR                   PIC S9(04) COMP VALUE ZEROES.
           03 IX-TAB-PAR                   PIC S9(04) COMP VALUE ZEROES.
           03 IX-TAB-RAN                   PIC S9(04) COMP VALUE ZEROES.
           03 IX-AREA-ISPC0140             PIC S9(04) COMP VALUE ZEROES.
           03 IX-AREA-PAGINA               PIC S9(04) COMP VALUE ZEROES.
           03 IX-TP-PREMIO                 PIC S9(04) COMP VALUE ZEROES.
           03 IX-COMPONENTE                PIC S9(04) COMP VALUE ZEROES.
           03 IX-AREA-SCHEDA               PIC S9(04) COMP VALUE ZEROES.
           03 IX-TAB-VAR                   PIC S9(04) COMP VALUE ZEROES.
           03 IX-TAB-CALC                  PIC S9(04) COMP VALUE ZEROES.
           03 IX-TAB-TIT                   PIC S9(04) COMP VALUE ZEROES.
           03 IX-TAB-RATA                  PIC S9(04) COMP VALUE ZEROES.
           03 IX-SDI                       PIC S9(04) COMP VALUE ZEROES.
           03 IX-ALL                       PIC S9(04) COMP VALUE ZEROES.
           03 IX-MAX                       PIC S9(04) COMP VALUE ZEROES.
           03 IX-APPO                      PIC S9(04) COMP VALUE ZEROES.
           03 IX-DCLGEN                    PIC S9(04) COMP VALUE ZEROES.
           03 IX-SPG                       PIC S9(04) COMP VALUE ZEROES.
           03 IX-APPO-GRZ                  PIC S9(04) COMP VALUE ZEROES.
           03 IX-APPO-SPG                  PIC S9(04) COMP VALUE ZEROES.
           03 IX-APPO-TGA                  PIC S9(04) COMP VALUE ZEROES.
           03 IX-RIC-GRZ                   PIC S9(04) COMP VALUE ZEROES.
           03 IX-TAB-ASS                   PIC S9(04) COMP VALUE ZEROES.
RICH13     03 IX-CNT                       PIC S9(04) COMP VALUE ZEROES.
RICH13     03 IX-RIC-PMO                   PIC S9(04) COMP VALUE ZEROES.
RICH13     03 IX-RIC-POG                   PIC S9(04) COMP VALUE ZEROES.
           03 IX-RIC-RAN                   PIC S9(04) COMP VALUE ZEROES.
           03 IX-TAB-LCCC0490              PIC S9(04) COMP VALUE ZEROES.
           03 IX-RIC-GAR                   PIC S9(04) COMP VALUE ZEROES.
           03 IX-PAG-IAS                   PIC S9(04) COMP.
AG         03 IX-IND-GAR                   PIC S9(04) COMP VALUE ZEROES.
           03 IX-AREA-SCHEDA-P             PIC S9(04) COMP.
           03 IX-AREA-SCHEDA-T             PIC S9(04) COMP.
           03 IX-TAB-VAR-P                 PIC S9(04) COMP.
           03 IX-TAB-VAR-T                 PIC S9(04) COMP.

      *----------------------------------------------------------------*
      *    AREE DI APPOGGIO PER IL VALORIZZATORE VARIABILI
      *----------------------------------------------------------------*
      *--> AREA RAPPORTO ANAGRAFICO
       01 VRAN-AREA-RAPP-ANAG.
          04 VRAN-ELE-RAPP-ANAG-MAX      PIC S9(04) COMP VALUE 0.
                COPY LCCVRANA REPLACING   ==(SF)==  BY ==VRAN==.
          COPY LCCVRAN1                  REPLACING ==(SF)== BY ==VRAN==.
      *--> AREA DATI FISCALE ADESIONE
       01 VDFA-AREA-DT-FISC-ADES.
          04 VDFA-ELE-FISC-ADES-MAX      PIC S9(04) COMP VALUE 0.
          04 VDFA-TAB-FISC-ADES.
          COPY LCCVDFA1                  REPLACING ==(SF)== BY ==VDFA==.
      *--> AREA GARANZIA
       01 VGRZ-AREA-GARANZIA.
          04 VGRZ-ELE-GARANZIA-MAX       PIC S9(04) COMP VALUE 0.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==VGRZ==.
          COPY LCCVGRZ1                  REPLACING ==(SF)== BY ==VGRZ==.
      *--> AREA TRANCHE DI GARANZIA
       01 VTGA-AREA-TRANCHE.
          04 VTGA-ELE-TRAN-MAX           PIC S9(04) COMP VALUE 0.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==VTGA==.
          COPY LCCVTGA1                  REPLACING ==(SF)== BY ==VTGA==.
      *--> AREA SOPRAPREMIO DI GARANZIA
       01 VSPG-AREA-SOPR-GAR.
          04 VSPG-ELE-SOPR-GAR-MAX       PIC S9(04) COMP VALUE 0.
                COPY LCCVSPGA REPLACING   ==(SF)==  BY ==VSPG==.
          COPY LCCVSPG1                  REPLACING ==(SF)== BY ==VSPG==.
      *--> AREA STRATEGIA DI INVESTIMENTO
       01 VSDI-AREA-STRA-INV.
          04 VSDI-ELE-STRA-INV-MAX       PIC S9(04) COMP VALUE 0.
          04 VSDI-TAB-STRA-INV           OCCURS 20.
          COPY LCCVSDI1                  REPLACING ==(SF)== BY ==VSDI==.
      *--  PARAMETRO MOVIMENTO
       01 VPMO-AREA-PARAM-MOV.
          04 VPMO-ELE-PARAM-MOV-MAX      PIC S9(04) COMP VALUE 0.
                COPY LCCVPMOB REPLACING   ==(SF)==  BY ==VPMO==.
          COPY LCCVPMO1                  REPLACING ==(SF)== BY ==VPMO==.
      *--  PARAMETRO OGGETTO
       01 VPOG-AREA-PARAM-OGG.
          04 VPOG-ELE-PARAM-OGG-MAX      PIC S9(04) COMP VALUE 0.
                COPY LCCVPOGB REPLACING   ==(SF)==  BY ==VPOG==.
          COPY LCCVPOG1                  REPLACING ==(SF)== BY ==VPOG==.
      *--  BENEFICIARI
       01 VBEP-AREA-BENEFICIARI.
          04 VBEP-ELE-BENEFICIARI        PIC S9(04) COMP VALUE 0.
                COPY LCCVBEPA REPLACING   ==(SF)==  BY ==VBEP==.
          COPY LCCVBEP1                  REPLACING ==(SF)== BY ==VBEP==.
      *---- QUESTIONARIO
       01 VQUE-AREA-QUEST.
          04 VQUE-ELE-QUEST-MAX          PIC S9(04) COMP VALUE 0.
          04 VQUE-TAB-QUEST              OCCURS 12.
          COPY LCCVQUE1                  REPLACING ==(SF)== BY ==VQUE==.
      *---- DETTAGLIO QUESTIONARIO
       01 VDEQ-AREA-DETT-QUEST.
          04 VDEQ-ELE-DETT-QUEST-MAX     PIC S9(04) COMP VALUE 0.
          04 VDEQ-TAB-DETT-QUEST         OCCURS 180.
          COPY LCCVDEQ1                  REPLACING ==(SF)== BY ==VDEQ==.
      *--  OGGETTO COLLEGATO
       01 VOCO-AREA-OGG-COLLG.
          04 VOCO-ELE-OGG-COLLG-MAX      PIC S9(04) COMP VALUE 0.
          04 VOCO-TAB-OGG-COLLG          OCCURS 60.
          COPY LCCVOCO1                  REPLACING ==(SF)== BY ==VOCO==.
      *-- MOVIMENTO
       01 VMOV-AREA-MOVIMENTO.
          04 VMOV-ELE-MOV-MAX            PIC S9(004) COMP VALUE 0.
          04 VMOV-TAB-MOV.
          COPY LCCVMOV1                  REPLACING ==(SF)== BY ==VMOV==.
      *
      *--> AREA GARANZIA
       01 VS-GRZ-AREA-GARANZIA.
          04 VS-GRZ-ELE-GARANZIA-MAX     PIC S9(04) COMP VALUE 0.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==VS-GRZ==.
          COPY LCCVGRZ1                  REPLACING ==(SF)==
                                                BY ==VS-GRZ==.
      *--> AREA SOPRAPREMIO DI GARANZIA
       01 VS-SPG-AREA-SOPR-GAR.
          04 VS-SPG-ELE-SOPR-GAR-MAX     PIC S9(04) COMP VALUE 0.
                COPY LCCVSPGA REPLACING   ==(SF)==  BY ==VS-SPG==.
          COPY LCCVSPG1                  REPLACING ==(SF)==
                                                BY ==VS-SPG==.
      *--> AREA TRANCHE DI GARANZIA
       01 VS-TGA-AREA-TRANCHE.
          04 VS-TGA-ELE-TRAN-MAX         PIC S9(04) COMP VALUE 0.
                COPY LCCVTGAC REPLACING   ==(SF)==  BY ==VS-TGA==.
          COPY LCCVTGA1                  REPLACING ==(SF)==
                                                BY ==VS-TGA==.
      *----------------------------------------------------------------*
      *--  Tipologiche di PTF
      *----------------------------------------------------------------*
           COPY LCCC0006.
           COPY LCCVXMV0.
           COPY LCCVXSB0.
           COPY LCCVXRA0.
           COPY LCCVXMZ0.
           COPY LCCVTGAZ.
           COPY LCCVSPGZ.
           COPY LCCVTITZ.
           COPY LCCVPCAZ.


       01 AREA-LCCC0062.
          COPY LCCC0062.

      *----------------------------------------------------------------*
      *    COPY I/O PER PGM LDBS0730
      *----------------------------------------------------------------*
           COPY LDBI0731.

       01 LDBO0731.
           COPY LDBO0731.
       01  IDSV0012.
           COPY IDSV0012.

12868 *----------------------------------------------------------------*
12868 *    AREA ROUTINE VERIFICA CONCOMITANZA GETRA
12868 *----------------------------------------------------------------*
12868  01 LCCC1901-AREA.
12868      COPY LCCC1901.

      *----------------------------------------------------------------*
      *    AREA GESTIONE DISPATCHER
      *----------------------------------------------------------------*
       01  DISPATCHER-VARIABLES.
           COPY IDSI0011.
           COPY IDSO0011.
      *----------------------------------------------------------------*
      *      COPY PER gestione tabella temp                            *
      *----------------------------------------------------------------*
           COPY IDSV0301.
           COPY IDSV0303.
      *----------------------------------------------------------------*
      *    COPY LETTURA TABELLE DB2
      *----------------------------------------------------------------*
           COPY IDBVTGA1.
RICH13     COPY IDBVPCO1.

      *----------------------------------------------------------------*
      *    COPY PER LETTURA MATRICE MOVIMENTO                          *
      *----------------------------------------------------------------*
           COPY LCCV0021.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
           COPY IEAV9903.

      *----------------------------------------------------------------*
      *    CHIAMATA AL SERVIZIO INFRASTRUTTURALE PER COLLEGAMENTO AL
      *    SERVIZI DI PRODOTTO
      *----------------------------------------------------------------*
      *    COPY IJCCMQ01.

      *----------------------------------------------------------------*
      *    AREA PER SERVIZIO DI PRODOTTO CALCOLI E CONTROLLI ISPS0140
      *----------------------------------------------------------------*
       01  AREA-IO-CALC-CONTR.
           COPY ISPC0140.

           COPY ISPC000Z.

      *----------------------------------------------------------------*
      *    COPY PER SERVIZIO VERIFICA CALCOLI SU DATE
      *----------------------------------------------------------------*

           COPY LCCC0003.

       01 IN-RCODE                       PIC 9(2) VALUE ZEROES.
      *----------------------------------------------------------------*
      *     Copy per il valorizzatore variabile
      *----------------------------------------------------------------*
      *--> AREA SCHEDA PER SERVIZI DI PRODOTTO
       01  WSKD-AREA-SCHEDA.
           COPY IVVC0216                REPLACING ==(SF)== BY ==WSKD==.
      *--> AREA OPZIONI
       01  WOPZ-AREA-OPZIONI.
           COPY IVVC0217                REPLACING ==(SF)== BY ==WOPZ==.
      *--> AREA DATI CONTESTUALI
       01  WCNT-AREA-DATI-CONTEST.
           COPY IVVC0212                REPLACING ==(SF)== BY ==WCNT==.
      *--> AREA IO VALORIZZATORE VARIABILE
       01  AREA-IO-IVVS0211.
           COPY IVVC0211                REPLACING ==(SF)== BY ==S211==.
      *--> AREA ALIAS
       01  AREA-ALIAS.
           COPY IVVC0218                REPLACING ==(SF)== BY ==S211==.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.

      *-- AREA COMUNE
       01 WCOM-AREA-STATI.
          COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.

      *-- AREA POLIZZA
       01 WPOL-AREA-POLIZZA.
          04 WPOL-ELE-POLI-MAX       PIC S9(04) COMP.
          04 WPOL-TAB-POLI.
             COPY LCCVPOL1           REPLACING ==(SF)== BY ==WPOL==.

      *-- AREA DATI COLLETTIVA
       01 WDCO-AREA-DT-COLLETTIVA.
          04 WDCO-ELE-COLL-MAX       PIC S9(04) COMP.
          04 WDCO-TAB-COLL.
             COPY LCCVDCO1           REPLACING ==(SF)== BY ==WDCO==.

      *-- AREA ADESIONE
       01 WADE-AREA-ADESIONE.
          04 WADE-ELE-ADES-MAX       PIC S9(04) COMP.
          04 WADE-TAB-ADES.
             COPY LCCVADE1           REPLACING ==(SF)== BY ==WADE==.

      *-- AREA DEFAULT ADESIONE
       01 WDAD-AREA-DEF-ADES.
          04 WDAD-ELE-DEF-ADES-MAX   PIC S9(04) COMP.
          04 WDAD-TAB-DEF-ADES.
             COPY LCCVDAD1           REPLACING ==(SF)== BY ==WDAD==.

      *-- AREA RAPPORTO ANAGRAFICO
       01 WRAN-AREA-RAPP-ANAG.
          04 WRAN-ELE-RAPP-ANAG-MAX  PIC S9(04) COMP.
                COPY LCCVRANA REPLACING   ==(SF)==  BY ==WRAN==.
             COPY LCCVRAN1           REPLACING ==(SF)== BY ==WRAN==.

      *-- AREA DATI FISCALE ADESIONE
       01 WDFA-AREA-DT-FISC-ADES.
          04 WDFA-ELE-FISC-ADES-MAX  PIC S9(04) COMP.
          04 WDFA-TAB-FISC-ADES.
             COPY LCCVDFA1           REPLACING ==(SF)== BY ==WDFA==.

      *-- AREA GARANZIE
       01 WGRZ-AREA-GARANZIA.
          04 WGRZ-ELE-GARANZIA-MAX   PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
             COPY LCCVGRZ1           REPLACING ==(SF)== BY ==WGRZ==.

      *-- AREA TRANCHE DI GARANZIA
       01 WTGA-AREA-TRANCHE.
          04 WTGA-ELE-TRAN-MAX       PIC S9(04) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==WTGA==.
             COPY LCCVTGA1           REPLACING ==(SF)== BY ==WTGA==.

      *-- AREA SOPRAPREMIO DI GARANZIA
       01 WSPG-AREA-SOPR-GAR.
          04 WSPG-ELE-SOPR-GAR-MAX   PIC S9(04) COMP.
                COPY LCCVSPGA REPLACING   ==(SF)==  BY ==WSPG==.
             COPY LCCVSPG1           REPLACING ==(SF)== BY ==WSPG==.

      *-- AREA STRATEGIA DI INVESTIMENTO
       01 WSDI-AREA-STRA-INV.
          04 WSDI-ELE-STRA-INV-MAX   PIC S9(04) COMP.
          04 WSDI-TAB-STRA-INV       OCCURS 20.
             COPY LCCVSDI1           REPLACING ==(SF)== BY ==WSDI==.

      *-- AREA ASSET ALLOCATION
       01 WALL-AREA-ASSET.
             COPY LCCVALL7.
             COPY LCCVALL1           REPLACING ==(SF)== BY ==WALL==.

      *-- PARAMETRO MOVIMENTO
       01 WPMO-AREA-PARAM-MOV.
          04 WPMO-ELE-PARAM-MOV-MAX  PIC S9(04) COMP.
                COPY LCCVPMOB REPLACING   ==(SF)==  BY ==WPMO==.
             COPY LCCVPMO1           REPLACING ==(SF)== BY ==WPMO==.

      *-- PARAMETRO OGGETTO
       01 WPOG-AREA-PARAM-OGG.
          04 WPOG-ELE-PARAM-OGG-MAX  PIC S9(04) COMP.
                COPY LCCVPOGB REPLACING   ==(SF)==  BY ==WPOG==.
             COPY LCCVPOG1           REPLACING ==(SF)== BY ==WPOG==.

      *-- BENEFICIARI
       01 WBEP-AREA-BENEFICIARI.
          04 WBEP-ELE-BENEFICIARI    PIC S9(04) COMP.
                COPY LCCVBEPA REPLACING   ==(SF)==  BY ==WBEP==.
             COPY LCCVBEP1              REPLACING ==(SF)== BY ==WBEP==.

      *-- QUESTIONARIO
       01 WQUE-AREA-QUEST.
          04 WQUE-ELE-QUEST-MAX      PIC S9(04) COMP.
          04 WQUE-TAB-QUEST          OCCURS 12.
             COPY LCCVQUE1           REPLACING ==(SF)== BY ==WQUE==.

      *-- DETTAGLIO QUESTIONARIO
       01 WDEQ-AREA-DETT-QUEST.
          04 WDEQ-ELE-DETT-QUEST-MAX PIC S9(04) COMP.
          04 WDEQ-TAB-DETT-QUEST     OCCURS 180.
             COPY LCCVDEQ1           REPLACING ==(SF)== BY ==WDEQ==.

      *-- CLAUSOLE
       01 WCLT-AREA-CLAUSOLE.
          04 WCLT-ELE-CLAU-TEST-MAX  PIC S9(04) COMP.
          04 WCLT-TAB-CLAU-TEST      OCCURS 54.
             COPY LCCVCLT1           REPLACING ==(SF)== BY ==WCLT==.

      *-- OGGETTO COLLEGATO
       01 WOCO-AREA-OGG-COLLG.
          04 WOCO-ELE-OGG-COLLG-MAX  PIC S9(04) COMP.
          04 WOCO-TAB-OGG-COLLG      OCCURS 60.
             COPY LCCVOCO1           REPLACING ==(SF)== BY ==WOCO==.

      *--> GESTIONE RAPPORTO RETE
       01 WRRE-AREA-RAP-RETE.
          04 WRRE-ELE-RAP-RETE-MAX   PIC S9(04) COMP.
          04 WRRE-TAB-RAP-RETE       OCCURS 20.
             COPY LCCVRRE1           REPLACING ==(SF)== BY ==WRRE==.

      *--  VINCOLO PEGNO
       01 WL23-AREA-VINC-PEG.
          04 WL23-ELE-VINC-PEG-MAX   PIC S9(04) COMP.
          04 WL23-TAB-VINC-PEG       OCCURS 10.
          COPY LCCVL231              REPLACING ==(SF)== BY ==WL23==.

ITRAT *-- ESTENZIONE POLIZZA CPI PR
ITRAT  01 WP67-AREA-EST-POLI-CPI-PR.
ITRAT     04 WP67-EST-POLI-CPI-PR-MAX PIC S9(04) COMP.
ITRAT     04 WP67-TAB-EST-POLI-CPI-PR.
ITRAT     COPY LCCVP671              REPLACING ==(SF)== BY ==WP67==.

      *-- AREA DATI PAGINA
       01 WPAG-AREA-PAGINA.
          COPY LVEC0268              REPLACING ==(SF)== BY ==WPAG==.

       01 WPAG-AREA-IAS.
          COPY LVEC0069              REPLACING ==(SF)== BY ==WPAG==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                WPOL-AREA-POLIZZA
                                WDCO-AREA-DT-COLLETTIVA
                                WADE-AREA-ADESIONE
                                WDAD-AREA-DEF-ADES
                                WRAN-AREA-RAPP-ANAG
                                WDFA-AREA-DT-FISC-ADES
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TRANCHE
                                WSPG-AREA-SOPR-GAR
                                WSDI-AREA-STRA-INV
                                WALL-AREA-ASSET
                                WPMO-AREA-PARAM-MOV
                                WPOG-AREA-PARAM-OGG
                                WBEP-AREA-BENEFICIARI
                                WQUE-AREA-QUEST
                                WDEQ-AREA-DETT-QUEST
                                WCLT-AREA-CLAUSOLE
                                WOCO-AREA-OGG-COLLG
                                WRRE-AREA-RAP-RETE
                                WL23-AREA-VINC-PEG
                                WP67-AREA-EST-POLI-CPI-PR
                                WPAG-AREA-PAGINA
                                WPAG-AREA-IAS.
      *----------------------------------------------------------------*

           PERFORM S00000-OPERAZIONI-INIZIALI
              THRU EX-S00000.

           IF IDSV0001-ESITO-OK
              PERFORM S10000-ELABORAZIONE
                 THRU EX-S10000
           END-IF.

           PERFORM S90000-OPERAZIONI-FINALI
              THRU EX-S90000.

      *----------------------------------------------------------------*
      *    OPERAZIONI INIZIALI
      *----------------------------------------------------------------*
       S00000-OPERAZIONI-INIZIALI.

           SET WK-GRZ-NON-TROVATA          TO TRUE.
           SET WK-TGA-NON-TROVATA          TO TRUE.

           INITIALIZE                        WK-VARIABILI
                                             WPAG-AREA-IAS
                                             WCNT-AREA-DATI-CONTEST.

           MOVE IDSV0001-TIPO-MOVIMENTO       TO WS-MOVIMENTO.
           MOVE IDSV0001-COD-COMPAGNIA-ANIA   TO WS-COMPAGNIA.
           MOVE IDSV0001-DATA-COMPETENZA(1:8) TO WK-DATA-COMPETENZA.

      *--  INIZIALIZZAZIONE DEI DATI DELL'AREA DI PAGINA
           PERFORM S00010-INITIALIZE-PAGINA
              THRU EX-S00010.

RICH13     PERFORM S10230-LETTURA-PCO    THRU EX-S10230.
RICH13     MOVE PCO-TP-RAT-PERF TO WK-TP-RAT-PERF.

       EX-S00000.
           EXIT.
      *----------------------------------------------------------------*
      *--> INIZIALIZZAZIONE DEI DATI DELL'AREA DI PAGINA
      *----------------------------------------------------------------*
       S00010-INITIALIZE-PAGINA.

      *--> INIZIALIZZAZIONE TABELLA ASSET
           MOVE ZEROES                TO WPAG-ELE-ASSET-MAX
           PERFORM VARYING IX-TAB-ASS  FROM 1 BY 1
                     UNTIL IX-TAB-ASS > WK-ASSET-ELE-MAX
             MOVE SPACES
               TO WPAG-COD-FONDO(IX-TAB-ASS)
                  WPAG-TP-FND(IX-TAB-ASS)
             MOVE HIGH-VALUE
               TO WPAG-PERC-FONDO-NULL(IX-TAB-ASS)
           END-PERFORM.

      *--> INIZIALIZZAZIONE DATI GARANZIA
           MOVE ZEROES                TO WPAG-ELE-CALCOLI-MAX
           PERFORM VARYING IX-TAB-CALC FROM 1 BY 1
                     UNTIL IX-TAB-CALC > WK-ELE-MAX-TRCH-DI-GAR

             MOVE SPACES       TO WPAG-COD-GARANZIA(IX-TAB-CALC)
             MOVE HIGH-VALUE   TO WPAG-IMP-MOVI-NULL(IX-TAB-CALC)
                                  WPAG-IMP-MOVI-NEG-NULL(IX-TAB-CALC)
             MOVE HIGH-VALUE   TO WPAG-AA-DIFF-PROR-NULL(IX-TAB-CALC)
             MOVE HIGH-VALUE   TO WPAG-MM-DIFF-PROR-NULL(IX-TAB-CALC)


      *--> INIZIALIZZAZIONE ELE MAX
             MOVE ZEROES       TO WPAG-ELE-TRANCHE-MAX (IX-TAB-CALC)
                                  WPAG-ELE-SOPR-GAR-MAX(IX-TAB-CALC)
                                  WPAG-ELE-PAR-CALC-MAX(IX-TAB-CALC)
                                  WPAG-ELE-TIT-CONT-MAX(IX-TAB-CALC)
                                  WPAG-ELE-TIT-RATA-MAX(IX-TAB-CALC)

      *-->  INIZIALIZZAZIONE DATI TRANCHE DI GARANZIA (OCCURS 2)
             PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
                     UNTIL IX-TAB-TGA > WK-ELE-MAX-TRCH-DI-GAR

               MOVE SPACES
                 TO WPAG-TP-RGM-FISC(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-FL-PREL-RIS(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-FL-RICALCOLO(IX-TAB-CALC, IX-TAB-TGA)

               MOVE HIGH-VALUE
                 TO WPAG-TP-TRCH-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-PRE-CASO-MOR-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-BNS-ANTIC-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-PRE-INI-NET-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-PRE-PP-INI-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-PRE-PP-ULT-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-PRE-TARI-INI-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-PRE-TARI-ULT-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-PRE-INVRIO-INI-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-PRE-INVRIO-ULT-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-SOPR-PROF-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-SOPR-SAN-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-SOPR-SPO-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-SOPR-TEC-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-ALT-SOPR-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-TS-RIVAL-FIS-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-RAT-LRD-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-PRE-LRD-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-PRSTZ-INI-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-PRSTZ-ULT-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-CPT-RSH-MOR-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-SCON-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-ALQ-SCON-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-CAR-ACQ-TGA-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-CAR-INC-TGA-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-CAR-GEST-TGA-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-AZ-TGA-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-ADER-TGA-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-TFR-TGA-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-VOLO-TGA-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-ABB-ANNO-ULT-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-ABB-TOT-INI-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-DUR-ABB-ANNI-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-PRE-PATTUITO-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-PREST-ULT-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-PRE-RIVAL-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-TGA-ALIQ-INCAS-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-TGA-ALIQ-ACQ-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-TGA-ALIQ-RICOR-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-TGA-IMP-INCAS-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-TGA-IMP-ACQ-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-TGA-IMP-RICOR-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-IMP-PRSTZ-AGG-INI-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-MANFEE-ANTIC-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-TGA-IMP-ACQ-EXP-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-TGA-IMP-REN-ASS-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-TGA-IMP-COMMIS-INT-NULL
                    (IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-TGA-ALIQ-REN-ASS-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-TGA-ALIQ-COMMIS-INT-NULL
                    (IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-TGA-IMPB-REN-ASS-NULL(IX-TAB-CALC, IX-TAB-TGA)
                    WPAG-TGA-IMPB-COMMIS-INT-NULL
                    (IX-TAB-CALC, IX-TAB-TGA)

             END-PERFORM

      *--> INIZIALIZZAZIONE DATI SOPRAPPREMI  (OCCURS 5)
             PERFORM VARYING IX-TAB-SPG FROM 1 BY 1
                       UNTIL IX-TAB-SPG > WK-SOPR-GAR-ELE-MAX

               MOVE SPACES
                 TO WPAG-COD-SOVRAP(IX-TAB-CALC, IX-TAB-SPG)
                    WPAG-TP-D(IX-TAB-CALC, IX-TAB-SPG)
               MOVE HIGH-VALUE
                 TO WPAG-PERC-SOVRAP-NULL(IX-TAB-CALC, IX-TAB-SPG)
                    WPAG-IMP-SOVRAP-NULL(IX-TAB-CALC, IX-TAB-SPG)
                    WPAG-SOVRAM-NULL(IX-TAB-CALC, IX-TAB-SPG)
             END-PERFORM

      *--> INIZIALIZZAZIONE DATI PARAMETRO DI CALCOLO  (OCCURS 20)
             PERFORM VARYING IX-TAB-PAR FROM 1 BY 1
                       UNTIL IX-TAB-PAR > WK-ELE-MAX-PARAM-D-CALC

               MOVE SPACES
                 TO WPAG-COD-PARAM(IX-TAB-CALC, IX-TAB-PAR)
                    WPAG-TP-D(IX-TAB-CALC, IX-TAB-PAR)
               MOVE HIGH-VALUE
                 TO WPAG-VAL-DT-NULL(IX-TAB-CALC, IX-TAB-PAR)
                    WPAG-VAL-IMP-NULL(IX-TAB-CALC, IX-TAB-PAR)
                    WPAG-VAL-TS-NULL(IX-TAB-CALC, IX-TAB-PAR)
                    WPAG-VAL-NUM-NULL(IX-TAB-CALC, IX-TAB-PAR)
                    WPAG-VAL-PC-NULL(IX-TAB-CALC, IX-TAB-PAR)

               MOVE SPACES
                 TO WPAG-VAL-STR(IX-TAB-CALC, IX-TAB-PAR)
                    WPAG-VAL-FL(IX-TAB-CALC, IX-TAB-PAR)
             END-PERFORM


      *-->   DATI PROVVIGIONI
             MOVE HIGH-VALUE
               TO WPAG-PRV-ACQ-1O-ANNO-NULL(IX-TAB-CALC)
                  WPAG-PRV-ACQ-2O-ANNO-NULL(IX-TAB-CALC)
                  WPAG-PRV-RICOR-NULL(IX-TAB-CALC)
                  WPAG-PRV-INCAS-NULL(IX-TAB-CALC)
                  WPAG-ALIQ-INCAS-NULL(IX-TAB-CALC)
                  WPAG-ALIQ-ACQ-NULL(IX-TAB-CALC)
                  WPAG-ALIQ-RICOR-NULL(IX-TAB-CALC)
                  WPAG-IMP-INCAS-NULL(IX-TAB-CALC)
                  WPAG-IMP-ACQ-NULL(IX-TAB-CALC)
                  WPAG-IMP-RICOR-NULL(IX-TAB-CALC)
                  WPAG-PRV-REN-ASS-NULL(IX-TAB-CALC)
                  WPAG-PRV-COMMIS-INT-NULL(IX-TAB-CALC)
                  WPAG-ALIQ-REN-ASS-NULL(IX-TAB-CALC)
                  WPAG-ALIQ-COMMIS-INT-NULL(IX-TAB-CALC)
                  WPAG-IMP-REN-ASS-NULL(IX-TAB-CALC)
                  WPAG-IMP-COMMIS-INT-NULL(IX-TAB-CALC)



             PERFORM VARYING IX-TAB-TIT FROM 1 BY 1
                       UNTIL IX-TAB-TIT > WK-ELE-MAX-TIT-CONT

      *-->     DATI TITOLO CONTABILE  (OCCURS 12)
               MOVE HIGH-VALUE
                 TO WPAG-CONT-PREMIO-NETTO-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-INT-FRAZ-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-INT-RETRODT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-DIRITTI-NULL(IX-TAB-CALC, IX-TAB-TIT)
                   WPAG-CONT-SPESE-MEDICHE-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-TASSE-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-SOPR-SANIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-SOPR-PROFES-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-SOPR-SPORT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-SOPR-TECN-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-ALTRI-SOPR-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-PREMIO-TOT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-PREMIO-PURO-IAS-NULL
                                              (IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-PREMIO-RISC-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-IMP-CAR-ACQ-TIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-IMP-CAR-INC-TIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-IMP-CAR-GEST-TIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-ACQ-1O-ANNO-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-ACQ-2O-ANNO-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-RICOR-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-INCAS-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-IMP-AZ-TIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-IMP-ADER-TIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-IMP-TFR-TIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-IMP-VOLO-TIT-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-IMP-ACQ-EXP-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-IMP-REN-ASS-NULL(IX-TAB-CALC, IX-TAB-TIT)
                    WPAG-CONT-IMP-COMMIS-INT-NULL
                    (IX-TAB-CALC, IX-TAB-TIT)

             END-PERFORM

      *-->   DATI TITOLO DI RATA
             PERFORM VARYING IX-TAB-RATA FROM 1 BY 1
                       UNTIL IX-TAB-RATA > WK-ELE-MAX-TIT-RAT
               MOVE HIGH-VALUE
                 TO WPAG-RATA-PREMIO-NETTO-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-INT-FRAZ-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-INT-RETRODT-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-DIRITTI-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-SPESE-MEDICHE-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-TASSE-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-SOPR-SANIT-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-SOPR-PROFES-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-SOPR-SPORT-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-SOPR-TECN-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-ALTRI-SOPR-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-PREMIO-TOT-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-PREMIO-PURO-IAS-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-PREMIO-RISC-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-IMP-CAR-ACQ-TDR-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-IMP-CAR-INC-TDR-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-IMP-CAR-GEST-TDR-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-ACQ-1O-ANNO-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-ACQ-2O-ANNO-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-RICOR-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-INCAS-NULL
                   (IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-IMP-AZ-TDR-NULL(IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-IMP-ADER-TDR-NULL(IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-IMP-TFR-TDR-NULL(IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-IMP-VOLO-TDR-NULL(IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-IMP-INT-RIATT-NULL(IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-IMP-ACQ-EXP-NULL(IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-IMP-REN-ASS-NULL(IX-TAB-CALC, IX-TAB-RATA)
                    WPAG-RATA-IMP-COMMIS-INT-NULL
                    (IX-TAB-CALC, IX-TAB-RATA)
             END-PERFORM

      *-->   DATI ALTRI
             MOVE SPACES
               TO WPAG-TP-IAS(IX-TAB-CALC)
                  WPAG-FL-FRAZ-PROVV(IX-TAB-CALC)
             MOVE HIGH-VALUE
               TO WPAG-DT-VAL-QUOTE-NULL(IX-TAB-CALC)

      *-->   DATI PARAMETRO MOVIMENTO
             MOVE HIGH-VALUE
               TO WPAG-DT-PROS-BNS-FED-NULL(IX-TAB-CALC)
                  WPAG-DT-PROS-BNS-RIC-NULL(IX-TAB-CALC)
                  WPAG-DT-PROS-CEDOLA-NULL(IX-TAB-CALC)

           END-PERFORM.

      *--> Data fine copertura titolo contabile
           MOVE HIGH-VALUE
             TO WPAG-DT-END-COP-TIT-NULL.

       EX-S00010.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S10000-ELABORAZIONE.
12868      MOVE IDSV0001-TIPO-MOVIMENTO
12868         TO WS-MOVIMENTO
12868      IF VERSAM-AGGIUNTIVO
12868      IF IDSV0001-ESITO-OK
12868         PERFORM S1110-CALL-LCCS1900
12868            THRU EX-S1110
12868         IF LCCC1901-FL-ESEGUIBILE-SI
12868            CONTINUE
12868         ELSE
12868               MOVE WK-PGM
12868                  TO IEAI9901-COD-SERVIZIO-BE
12868               MOVE 'S1000-ELABORAZIONE'
12868                        TO IEAI9901-LABEL-ERR
12868               MOVE '005404'
12868                        TO IEAI9901-COD-ERRORE
12868            PERFORM S0300-RICERCA-GRAVITA-ERRORE
12868               THRU EX-S0300
12868         END-IF
12868      END-IF
12868      END-IF

12868      IF IDSV0001-ESITO-OK
              PERFORM CALL-LVES0245
                THRU  CALL-LVES0245-EX

      *--  le informazioni da passare al valorizzatore variabili
      *--  vengono filtrate
              PERFORM S10100-FILTRO-DCLGEN
                 THRU EX-S10100

      *--  ROUTINE SERVIZIO VALORIZZATORE VARIABILI
              PERFORM S11000-PREPARA-AREA-IVVS0211
                 THRU EX-S11000
12868      END-IF

           IF IDSV0001-ESITO-OK
              PERFORM CALL-VALORIZZATORE
                 THRU CALL-VALORIZZATORE-EX
           END-IF

           IF IDSV0001-ESITO-OK
              MOVE S211-BUFFER-DATI    TO WSKD-AREA-SCHEDA
           END-IF

           IF IDSV0001-ESITO-OK
      *--     ROUTINE SERVIZIO DI PRODOTTO CALCOLI E CONTROLLI
              PERFORM S12000-PREPARA-AREA-CALC-CONTR
                 THRU EX-S12000
              IF IDSV0001-ESITO-OK
                 PERFORM S12500-CALL-CALC-CONTR
                    THRU EX-S12500
              END-IF
           END-IF.


           IF IDSV0001-ESITO-OK
      *-->    ROUTINE VALORIZZA AREA DATI PAGINA
              PERFORM S14000-VAL-AREA-PAGINA-P
                 THRU EX-S14000
              VARYING IX-AREA-ISPC0140 FROM 1 BY 1
                UNTIL IX-AREA-ISPC0140 > ISPC0140-DATI-NUM-MAX-ELE-P

              PERFORM S14005-VAL-AREA-PAGINA-T
                 THRU EX-S14005
              VARYING IX-AREA-ISPC0140 FROM 1 BY 1
                UNTIL IX-AREA-ISPC0140 > ISPC0140-DATI-NUM-MAX-ELE-T


11174         IF IDSV0001-ESITO-OK
11174            PERFORM VALORIZZA-FLAG-AUTOGEN
11174               THRU VALORIZZA-FLAG-AUTOGEN-EX
11174         END-IF

      *--> VALORIZZAZIONE AREA PAGINA PER IAS GARANZIE
              MOVE IDSV0001-TIPO-MOVIMENTO       TO WS-MOVIMENTO
              IF IDSV0001-ESITO-OK
                 IF INCLU-GARANZ         OR
                    VERSAM-AGGIUNTIVO    OR
                    VERSAM-AGGIUNTIVO-REINV
                    PERFORM S14100-AREA-PAGINA-IAS-GAR
                       THRU S14100-AREA-PAGINA-IAS-GAR-EX
                    VARYING IX-PAG-IAS FROM 1 BY 1
                      UNTIL IX-PAG-IAS > WGRZ-ELE-GARANZIA-MAX
                 END-IF
              END-IF

              IF IDSV0001-ESITO-OK
                 PERFORM S14500-GESTIONE-FONDI
                    THRU EX-S14500

                 PERFORM S14700-GESTIONE-RATE
                    THRU S14700-EX

      *-->    ROUTINE INIZIALIZZAZIONE TASTI
                 PERFORM S15000-INIZIALIZZA-TASTI
                    THRU EX-S15000
              END-IF
           END-IF.

       EX-S10000.
           EXIT.
12868 *----------------------------------------------------------------*
12868 *    CHIAMATA ROUTINE VERIFICA CONCOMITANZA GETRA
12868 *----------------------------------------------------------------*
12868  S1110-CALL-LCCS1900.

           INITIALIZE                        LCCC1901-AREA.

           PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
             UNTIL IX-TAB-GRZ > WGRZ-ELE-GARANZIA-MAX

             IF WGRZ-ST-ADD(IX-TAB-GRZ)
                ADD 1             TO LCCC1901-GAR-MAX

                MOVE WGRZ-COD-TARI(IX-TAB-GRZ)
                  TO LCCC1901-COD-TARI(LCCC1901-GAR-MAX)
             END-IF

           END-PERFORM.

           CALL LCCS1900 USING AREA-IDSV0001
                               WCOM-AREA-STATI
                               WPOL-AREA-POLIZZA
                               WADE-AREA-ADESIONE
                               LCCC1901-AREA

           ON EXCEPTION
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'FONDI'
                    TO CALL-DESC
                  MOVE 'S1110-CALL-LCCS1900'
                    TO IEAI9901-LABEL-ERR
                  PERFORM S0290-ERRORE-DI-SISTEMA
                     THRU EX-S0290
           END-CALL.

12868  EX-S1110.
12868      EXIT.
      *----------------------------------------------------------------*
      *    Forzatura Valore RATEANTIC nel caso di frazionamento annuale
      *----------------------------------------------------------------*
       CALL-LVES0245.

           CALL LVES0245 USING  AREA-IDSV0001
                                WCOM-AREA-STATI
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WPMO-AREA-PARAM-MOV
                                WPOG-AREA-PARAM-OGG
                                WRAN-AREA-RAPP-ANAG
           ON EXCEPTION
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'INIZIALIZZAZIONE DELLA PAGINA'
                                             TO CALL-DESC
              MOVE 'S10010-CALL-LVES0245'    TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

       CALL-LVES0245-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    FILTRO DELLE OCCORRENZE CON STATUS DIVERSO DA 'D'
      *----------------------------------------------------------------*
       S10100-FILTRO-DCLGEN.

      *--> AREA RAPPORTO ANAGRAFICO
           MOVE ZEROES     TO  IX-APPO.

           PERFORM VARYING IX-DCLGEN FROM 1 BY 1
           UNTIL IX-DCLGEN > WRAN-ELE-RAPP-ANAG-MAX

                IF  NOT WRAN-ST-DEL(IX-DCLGEN)
                AND NOT WRAN-ST-CON(IX-DCLGEN)
                   ADD 1     TO IX-APPO
                   MOVE WRAN-TAB-RAPP-ANAG(IX-DCLGEN)
                     TO VRAN-TAB-RAPP-ANAG(IX-APPO)
                END-IF

           END-PERFORM.
           MOVE IX-APPO      TO  VRAN-ELE-RAPP-ANAG-MAX.
           MOVE ZEROES       TO  IX-APPO.
      *--> AREA GARANZIE
           PERFORM VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > WGRZ-ELE-GARANZIA-MAX

                IF  NOT WGRZ-ST-DEL(IX-DCLGEN)
11986              IF (NOT WGRZ-ST-CON(IX-DCLGEN)) OR
11986                 (WGRZ-ST-CON(IX-DCLGEN) AND INCLU-GARANZ)
18098                IF     (VERSAM-AGGIUNTIVO  AND
18098                        WGRZ-ST-INV(IX-DCLGEN) AND
18098                     WGRZ-TP-GAR(IX-DCLGEN) = 4 )
18098                  CONTINUE
18098                ELSE
                       ADD 1     TO IX-APPO
                       MOVE WGRZ-TAB-GAR(IX-DCLGEN)
                        TO VGRZ-TAB-GAR(IX-APPO)
18098                END-IF
                   END-IF
                END-IF

           END-PERFORM.
           MOVE IX-APPO      TO  VGRZ-ELE-GARANZIA-MAX.
           MOVE ZEROES       TO  IX-APPO.

      *--> AREA TRANCHE DI GARANZIE
           PERFORM VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > WTGA-ELE-TRAN-MAX

                IF  NOT WTGA-ST-DEL(IX-DCLGEN)
                AND NOT WTGA-TP-TRCH(IX-DCLGEN) = '9'

11986              IF (NOT WTGA-ST-CON(IX-DCLGEN)) OR
11986                 (WTGA-ST-CON(IX-DCLGEN) AND INCLU-GARANZ)

                   ADD 1    TO IX-APPO
                   MOVE WTGA-TAB-TRAN(IX-DCLGEN)
                     TO VTGA-TAB-TRAN(IX-APPO)

                   END-IF
                END-IF

           END-PERFORM.
           MOVE IX-APPO      TO  VTGA-ELE-TRAN-MAX.
           MOVE ZEROES       TO  IX-APPO.

      *--> AREA SOPRAPREMIO DI GARANZIA
           PERFORM VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > WSPG-ELE-SOPR-GAR-MAX

      ****** ATTENZIONE: I SOPRAPREMI CHE SONO STATI ESCLUSI
      ******            (FL-ESCL-SOPR = 'S') NON DEVONO  ESSERE PRESI
      ******             IN CONSIDERAZIONE

                IF  NOT WSPG-ST-DEL(IX-DCLGEN)
                AND NOT WSPG-ST-CON(IX-DCLGEN)
                AND NOT WSPG-FL-ESCL-SOPR(IX-DCLGEN) = 'S'
                   ADD 1    TO IX-APPO
                   MOVE WSPG-TAB-SPG(IX-DCLGEN)
                     TO VSPG-TAB-SPG(IX-APPO)
                END-IF

           END-PERFORM.
           MOVE IX-APPO      TO  VSPG-ELE-SOPR-GAR-MAX.
           MOVE ZEROES       TO  IX-APPO.

      *--> AREA SRATEGIA D'INVESTIMENTO
           PERFORM VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > WSDI-ELE-STRA-INV-MAX

                IF  NOT WSDI-ST-DEL(IX-DCLGEN)
                AND NOT WSDI-ST-CON(IX-DCLGEN)
                   ADD 1    TO IX-APPO
                   MOVE WSDI-TAB-STRA-INV(IX-DCLGEN)
                     TO VSDI-TAB-STRA-INV(IX-APPO)
                END-IF

           END-PERFORM.
           MOVE IX-APPO      TO  VSDI-ELE-STRA-INV-MAX.
           MOVE ZEROES       TO  IX-APPO.

      *--> AREA PARAMETRO MOVIMENTO
           PERFORM VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > WPMO-ELE-PARAM-MOV-MAX

                IF  NOT WPMO-ST-DEL(IX-DCLGEN)
                AND NOT WPMO-ST-CON(IX-DCLGEN)
                   ADD 1    TO IX-APPO
                   MOVE WPMO-TAB-PARAM-MOV (IX-DCLGEN)
                     TO VPMO-TAB-PARAM-MOV (IX-APPO)
                END-IF

           END-PERFORM.
           MOVE IDSV0001-TIPO-MOVIMENTO    TO WS-MOVIMENTO.
           MOVE IX-APPO                    TO  VPMO-ELE-PARAM-MOV-MAX.
           MOVE ZEROES                     TO  IX-APPO.

      *--> AREA PARAMETRO OGGETTO
           PERFORM VARYING IX-DCLGEN FROM 1 BY 1
                     UNTIL IX-DCLGEN > WPOG-ELE-PARAM-OGG-MAX

                IF  NOT WPOG-ST-DEL(IX-DCLGEN)
                AND NOT WPOG-ST-CON(IX-DCLGEN)
                    ADD 1    TO IX-APPO
                    MOVE WPOG-TAB-PARAM-OGG (IX-DCLGEN)
                      TO VPOG-TAB-PARAM-OGG (IX-APPO)
                END-IF

           END-PERFORM.

           MOVE IX-APPO      TO  VPOG-ELE-PARAM-OGG-MAX .
           MOVE ZEROES       TO  IX-APPO.

      *--> AREA BENEFICIARI
           PERFORM VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > WBEP-ELE-BENEFICIARI

                IF  NOT WBEP-ST-DEL(IX-DCLGEN)
                AND NOT WBEP-ST-CON(IX-DCLGEN)
                   ADD 1    TO IX-APPO
                   MOVE WBEP-TAB-BENEFICIARI(IX-DCLGEN)
                     TO VBEP-TAB-BENEFICIARI (IX-APPO)
                END-IF

           END-PERFORM.
           MOVE IX-APPO      TO  VBEP-ELE-BENEFICIARI.
           MOVE ZEROES       TO  IX-APPO.

      *--> AREA QUESTIONARIO
           PERFORM VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > WQUE-ELE-QUEST-MAX

                IF  NOT WQUE-ST-DEL(IX-DCLGEN)
                AND NOT WQUE-ST-CON(IX-DCLGEN)
                   ADD 1    TO IX-APPO
                   MOVE WQUE-TAB-QUEST(IX-DCLGEN)
                     TO VQUE-TAB-QUEST (IX-APPO)
                END-IF

           END-PERFORM.
           MOVE IX-APPO      TO  VQUE-ELE-QUEST-MAX.
           MOVE ZEROES       TO  IX-APPO.

      *--> AREA DETTAGLIO QUESTIONARIO
           PERFORM VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > WDEQ-ELE-DETT-QUEST-MAX

                IF  NOT WDEQ-ST-DEL(IX-DCLGEN)
                AND NOT WDEQ-ST-CON(IX-DCLGEN)
                   ADD 1    TO IX-APPO
                   MOVE WDEQ-TAB-DETT-QUEST (IX-DCLGEN)
                     TO VDEQ-TAB-DETT-QUEST (IX-APPO)
                END-IF

           END-PERFORM.
           MOVE IX-APPO      TO  VDEQ-ELE-DETT-QUEST-MAX.
           MOVE ZEROES       TO  IX-APPO.

      *--> AREA OGGETTO COLLEGATO
           PERFORM VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > WOCO-ELE-OGG-COLLG-MAX

                IF  NOT WOCO-ST-DEL(IX-DCLGEN)
                AND NOT WOCO-ST-CON(IX-DCLGEN)
                   ADD 1    TO IX-APPO
                   MOVE WOCO-TAB-OGG-COLLG (IX-DCLGEN)
                     TO VOCO-TAB-OGG-COLLG (IX-APPO)
                END-IF

           END-PERFORM.
           MOVE IX-APPO      TO  VOCO-ELE-OGG-COLLG-MAX.
           MOVE ZEROES       TO  IX-APPO.

       EX-S10100.
           EXIT.
      *----------------------------------------------------------------*
      *    Lettura in ptf della parametro compagnia
      *----------------------------------------------------------------*
RICH13 S10230-LETTURA-PCO.

           MOVE HIGH-VALUE              TO PARAM-COMP.

RICH13     MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO
RICH13                                     IDSI0011-DATA-FINE-EFFETTO
RICH13                                     IDSI0011-DATA-COMPETENZA.

RICH13     MOVE IDSV0001-COD-COMPAGNIA-ANIA TO PCO-COD-COMP-ANIA.

RICH13     MOVE 'PARAM-COMP'                TO IDSI0011-CODICE-STR-DATO
RICH13                                         WK-TABELLA.
RICH13     MOVE PARAM-COMP                  TO IDSI0011-BUFFER-DATI.


RICH13     SET IDSI0011-SELECT              TO TRUE.
RICH13     SET IDSI0011-PRIMARY-KEY         TO TRUE.
RICH13     SET IDSI0011-TRATT-SENZA-STOR    TO TRUE.

RICH13     PERFORM CALL-DISPATCHER
RICH13        THRU CALL-DISPATCHER-EX.

RICH13     IF IDSO0011-SUCCESSFUL-RC
RICH13        EVALUATE TRUE
RICH13            WHEN IDSO0011-NOT-FOUND
RICH13                 MOVE WK-PGM          TO IEAI9901-COD-SERVIZIO-BE
RICH13                 MOVE 'S10230-LETTURA-PCO'
RICH13                                      TO IEAI9901-LABEL-ERR
RICH13                 MOVE '005069'        TO IEAI9901-COD-ERRORE
RICH13                 STRING WK-TABELLA           ';'
RICH13                        IDSO0011-RETURN-CODE ';'
RICH13                        IDSO0011-SQLCODE
RICH13                 DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
RICH13                 END-STRING
RICH13                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
RICH13                    THRU EX-S0300
RICH13            WHEN IDSO0011-SUCCESSFUL-SQL
RICH13               MOVE IDSO0011-BUFFER-DATI TO PARAM-COMP
RICH13        END-EVALUATE
RICH13     ELSE
RICH13        MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
RICH13        MOVE 'S10230-LETTURA-PCO'
RICH13                                TO IEAI9901-LABEL-ERR
RICH13        MOVE '005016'           TO IEAI9901-COD-ERRORE
RICH13        STRING WK-TABELLA           ';'
RICH13               IDSO0011-RETURN-CODE ';'
RICH13               IDSO0011-SQLCODE
RICH13        DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
RICH13        END-STRING
RICH13        PERFORM S0300-RICERCA-GRAVITA-ERRORE
RICH13           THRU EX-S0300
RICH13     END-IF.

RICH13 EX-S10230.
RICH13     EXIT.
      *----------------------------------------------------------------*
      *    PREPAREA AREA SERVIZIO VALORIZZATORE VARIABILI
      *----------------------------------------------------------------*
       S11000-PREPARA-AREA-IVVS0211.

           INITIALIZE                          AREA-IO-IVVS0211.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO S211-COD-COMPAGNIA-ANIA.
           MOVE IDSV0001-TIPO-MOVIMENTO      TO S211-TIPO-MOVIMENTO.
           MOVE WCOM-DT-ULT-VERS-PROD        TO S211-DATA-ULT-VERS-PROD

           IF INCLU-SOVRAP
           OR MOVIM-QUIETA
              SET S211-AREA-PV                 TO TRUE
              IF INCLU-SOVRAP
                 PERFORM S14600-GESTIONE-SOPRA THRU EX-S14600
              END-IF
           ELSE
              SET S211-AREA-VE                 TO TRUE
           END-IF.

           SET S211-IN-CONV                    TO TRUE.
           MOVE ZEROES                         TO IX-MAX
                                                  WK-APPO-LUNGHEZZA.

           SET S211-FLAG-GAR-OPZIONE-NO        TO TRUE.

      *-->  Valorizzazione della struttura di mapping

            PERFORM S11010-VAL-VAR-CONTESTO
               THRU S11010-EX

            IF IDSV0001-ESITO-OK
               PERFORM VAL-STR-DATI-PTF
                  THRU VAL-STR-DATI-PTF-EX
            END-IF.

       EX-S11000.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZAZIONE DELLE VARIBILI DI CONTESTO CALCOLATE
      *----------------------------------------------------------------*
       S11010-VAL-VAR-CONTESTO.

           MOVE ZERO    TO WCNT-ELE-VAR-CONT-MAX.
           MOVE ZERO    TO WK-FRQ-MOVI
           MOVE ZERO    TO WK-FRAZ-MM
           MOVE ZERO    TO WK-DATA-LIMITE

      *--> occorre verificare se h presente la pmo di quietanzamento
      *--> 6101 o la GETRA  6002 (se esiste una PMO non puo
      *--> esistee l'altra) per calcolare le rate da recuperare.
      *--  Recupero frazionamento polizza del movimento di
      *--  Quietanzamento
           SET QUIETANZAMENTO-NO  TO TRUE
           SET GETRA-NO           TO TRUE

      **** QUIETANZAMENTO
           SET MOVIM-QUIETA       TO TRUE
           PERFORM RIC-TAB-PMO
              THRU RIC-TAB-PMO-EX

           IF WK-TROVATO
              SET QUIETANZAMENTO-SI    TO TRUE
              MOVE PCO-DT-ULT-QTZO-IN  TO WK-DATA-LIMITE
           ELSE
      **** GENERAZIONE TRANCHE
              SET GENER-TRANCH         TO TRUE
              PERFORM RIC-TAB-PMO
                 THRU RIC-TAB-PMO-EX

              IF WK-TROVATO
                 SET  GETRA-SI         TO TRUE
                 MOVE PCO-DT-ULTGZ-TRCH-E-IN  TO WK-DATA-LIMITE
              END-IF
           END-IF

      *--  La variabile RATEDARECUP ha senso solo se la POLIZZA
      *--    Quietanzabile o se prevede la GETRA
           IF QUIETANZAMENTO-SI OR GETRA-SI

              IF WPMO-FRQ-MOVI-NULL(IX-RIC-PMO) = HIGH-VALUE
                 MOVE ZERO  TO WK-FRQ-MOVI
              ELSE
                 MOVE WPMO-FRQ-MOVI(IX-RIC-PMO) TO WK-FRQ-MOVI
                 COMPUTE WK-FRAZ-MM = 12 / WK-FRQ-MOVI
              END-IF

              PERFORM S11011-VAR-RATEDARECUP
                 THRU S11011-EX
           END-IF.

           IF IDSV0001-ESITO-OK
              PERFORM CALC-VAR-SUMPAPSTC
                 THRU CALC-VAR-SUMPAPSTC-EX
           END-IF.

           MOVE IDSV0001-TIPO-MOVIMENTO TO WS-MOVIMENTO.
           IF IDSV0001-ESITO-OK AND INCLU-GARANZ
              PERFORM CALCOLO-RATEO
                 THRU CALCOLO-RATEO-EX
           END-IF.

CAUSCO     IF IDSV0001-ESITO-OK
CAUSCO        PERFORM VALORIZZA-CAUSCONT
CAUSCO           THRU VALORIZZA-CAUSCONT-EX
CAUSCO     END-IF.

       S11010-EX.
           EXIT.
CAUSCO*----------------------------------------------------------------*
CAUSCO*
CAUSCO*----------------------------------------------------------------*
CAUSCO VALORIZZA-CAUSCONT.

           ADD  1                     TO WCNT-ELE-VAR-CONT-MAX
           MOVE WCNT-ELE-VAR-CONT-MAX TO IX-CNT

           MOVE 'CAUSCONT$'           TO WCNT-COD-VAR-CONT(IX-CNT)

           MOVE 'S'                   TO WCNT-TP-DATO-CONT(IX-CNT)

           MOVE ZEROES                TO WCNT-VAL-IMP-CONT(IX-CNT)
                                         WCNT-VAL-PERC-CONT(IX-CNT)

           MOVE WCOM-CODICE-INIZIATIVA
                                      TO WCNT-VAL-STR-CONT(IX-CNT)

           MOVE WS-LIV-PROD           TO WCNT-TP-LIVELLO(IX-CNT)

           MOVE SPACES                TO WCNT-COD-LIVELLO(IX-CNT)

           MOVE ZEROES                TO WCNT-ID-LIVELLO(IX-CNT).

CAUSCO VALORIZZA-CAUSCONT-EX.
CAUSCO     EXIT.
      *----------------------------------------------------------------*
      *--  In questa fase la variabile viene calcolata
      *--  come differenza tra la data dell'ultimo quietanzamento
      *--  oppure data ultima generazione tranche e la data
      *--  decorrenza/data effetto della polizza il tutto
      *--  rapportato al frazionamento
      *----------------------------------------------------------------*
       S11011-VAR-RATEDARECUP.

      *--  Recupero parametro RATENATIC
           SET WK-PR-RATEANTIC TO TRUE
           PERFORM RIC-TAB-POG
              THRU RIC-TAB-POG-EX
           IF WK-TROVATO
              EVALUATE WPOG-TP-D(IX-RIC-POG)
                  WHEN 'S'
                  WHEN 'I'
                  WHEN 'P'
                  WHEN 'N'
                      MOVE WPOG-VAL-NUM(IX-RIC-POG)
                        TO WK-RATE-ANTIC
              END-EVALUATE
           END-IF

           IF IDSV0001-ESITO-OK
      *--     calcola il numero di rate retrodatate e successive
      *--     compresa la rata di perfezionamento
              PERFORM CALL-LCCS0062
                 THRU CALL-LCCS0062-EX

           IF IDSV0001-ESITO-OK
              COMPUTE WK-RATE-RECUP = LCCC0062-TOT-NUM-RATE
                                    - WK-RATE-ANTIC - 1

              IF WK-RATE-RECUP < ZERO
                 MOVE ZERO           TO WK-RATE-RECUP
              END-IF

              IF RECUPRATE-UNICA
                 MOVE WK-RATE-RECUP     TO WK-VAL-VAR
                 ADD  1                 TO IX-CNT
                 MOVE 'RATEDARECUP'     TO WCNT-COD-VAR-CONT(IX-CNT)
                 MOVE WS-NUMERO         TO WCNT-TP-DATO-CONT(IX-CNT)
                 MOVE WK-VAL-VAR        TO WCNT-VAL-IMP-CONT(IX-CNT)
                 MOVE IX-CNT            TO WCNT-ELE-VAR-CONT-MAX
              END-IF
           END-IF.

       S11011-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Variabile utilizzata per il calcolo delle provvigioni
      *    tenendo conto di altre polizze di ptf.
      *----------------------------------------------------------------*
       CALC-VAR-SUMPAPSTC.

      *--  Ricerca codice soggetto del contraente della polizza emessa
           SET CONTRAENTE       TO TRUE

           PERFORM RIC-TAB-RAN
              THRU RIC-TAB-RAN-EX

           IF WK-TROVATO
              SET LCCC0033-ESTR-POL-REC-PROVV TO TRUE
              SET LCCC0033-VENDITA            TO TRUE

              MOVE WRAN-COD-SOGG(IX-RIC-RAN) TO LCCC0033-COD-SOGG
              MOVE 'CO'                      TO LCCC0033-TP-RAPP-ANA
              MOVE WPOL-DT-DECOR             TO LCCC0033-DT-DECOR-POL

              CALL LCCS0033 USING  AREA-IDSV0001
                                   WCOM-AREA-STATI
                                   AREA-IO-LCCS0033
              ON EXCEPTION
                 MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'VERIFICA-RECUP-PROVV' TO CALL-DESC
                 MOVE 'CALC-VAR-SUMPAPSTC'   TO IEAI9901-LABEL-ERR
                 PERFORM S0290-ERRORE-DI-SISTEMA
                    THRU EX-S0290
              END-CALL
              IF IDSV0001-ESITO-OK
                 COMPUTE WS-TOT-PREMIO-ANNUO =
                 FUNCTION SUM(LCCC0033-IMP-PREMIO-ANNUO(ALL))
                 ADD  1                    TO IX-CNT
                 MOVE 'SUMPAPSTC'          TO WCNT-COD-VAR-CONT(IX-CNT)
                 MOVE WS-IMPORTO           TO WCNT-TP-DATO-CONT(IX-CNT)
                 MOVE WS-TOT-PREMIO-ANNUO
                                           TO WCNT-VAL-IMP-CONT(IX-CNT)
                 MOVE IX-CNT               TO WCNT-ELE-VAR-CONT-MAX
              END-IF
           END-IF.

       CALC-VAR-SUMPAPSTC-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    Variabile utilizzata per il calcolo del rateo.
      *----------------------------------------------------------------*
       CALCOLO-RATEO.

           INITIALIZE LCCC0490-AREA-INPUT.

           MOVE WPOL-ID-POLI        TO LCCC0490-ID-POLI.
           MOVE WPOL-DT-DECOR       TO LCCC0490-DT-DECOR-POLI.

      * Ricerca garanzia base
           SET TP-GAR-BASE          TO TRUE.

           PERFORM RIC-TAB-GAR
              THRU RIC-TAB-GAR-EX.

      * Ricerca garanzia complementare
           SET TP-GAR-COMPLEM       TO TRUE.

           PERFORM RIC-TAB-GAR
              THRU RIC-TAB-GAR-EX.

      * Richiamo modulo calcolo rateo
           CALL LCCS0490 USING  AREA-IDSV0001
                                WCOM-AREA-STATI
                                LCCC0490
           ON EXCEPTION
              MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'CALCOLO RATEO'  TO CALL-DESC
              MOVE 'CALCOLO-RATEO'  TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

           IF IDSV0001-ESITO-OK

              MOVE LCCC0490-DT-FINE-COP    TO WPAG-DT-END-COP-TIT

              IF LCCC0490-SI-PRESENZA-RATEO
                 ADD  1                    TO IX-CNT
                 MOVE 'RATEO'              TO WCNT-COD-VAR-CONT(IX-CNT)
                 MOVE WS-NUMERO            TO WCNT-TP-DATO-CONT(IX-CNT)
                 ADD  1                    TO LCCC0490-RATEO
                 COMPUTE WCNT-VAL-IMP-CONT(IX-CNT) =
                         LCCC0490-RATEO / 360
                 MOVE IX-CNT               TO WCNT-ELE-VAR-CONT-MAX
              END-IF

              IF LCCC0490-SI-TIT-EMESSI
                 MOVE WK-PGM               TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S11010-VAL-VAR-CONTESTO' TO IEAI9901-LABEL-ERR
                 MOVE '005166'             TO IEAI9901-COD-ERRORE
                 MOVE SPACES               TO IEAI9901-PARAMETRI-ERR
                 STRING 'NON SI POSSONO INCLUDERE GARANZIE '
                        DELIMITED BY SIZE
                       'A QUESTA DATA EFFETTO PER LA PRESENZA DI TITOLI'
                        DELIMITED BY SIZE
                        ' EMESSI'
                        DELIMITED BY SIZE
                        INTO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF

           END-IF.

       CALCOLO-RATEO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       VAL-STR-DATI-PTF.

           IF WPOL-ELE-POLI-MAX > 0
              MOVE 1                           TO IX-MAX
              MOVE S211-ALIAS-POLI             TO S211-TAB-ALIAS(IX-MAX)
              MOVE 1                           TO S211-POSIZ-INI(IX-MAX)
              MOVE LENGTH OF WPOL-AREA-POLIZZA TO S211-LUNGHEZZA(IX-MAX)
              MOVE WPOL-AREA-POLIZZA
                TO S211-BUFFER-DATI
                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF WADE-ELE-ADES-MAX > 0
             COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                         S211-LUNGHEZZA(IX-MAX) + 1
             ADD 1                             TO IX-MAX
      *--> OCCORRENZA 2 AREA ADESIONE
             MOVE S211-ALIAS-ADES              TO S211-TAB-ALIAS(IX-MAX)
             MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
             MOVE LENGTH OF WADE-AREA-ADESIONE TO S211-LUNGHEZZA(IX-MAX)
             MOVE WADE-AREA-ADESIONE
               TO S211-BUFFER-DATI
                 (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.


           IF WDAD-ELE-DEF-ADES-MAX > 0

             COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                         S211-LUNGHEZZA(IX-MAX) + 1
             ADD 1                             TO IX-MAX
      *-->   OCCORRENZA 3 AREA DEFAULT ADESIONE
             MOVE S211-ALIAS-DFLT-ADES         TO S211-TAB-ALIAS(IX-MAX)
             MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
             MOVE LENGTH OF WDAD-AREA-DEF-ADES TO S211-LUNGHEZZA(IX-MAX)
             MOVE WDAD-AREA-DEF-ADES
                TO S211-BUFFER-DATI
                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF WDCO-ELE-COLL-MAX > 0
             COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                         S211-LUNGHEZZA(IX-MAX) + 1
             ADD 1                             TO IX-MAX
      *--> OCCORRENZA 4 AREA DATI COLLETTIVA
             MOVE S211-ALIAS-DT-COLL           TO S211-TAB-ALIAS(IX-MAX)
             MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
             MOVE LENGTH OF WDCO-AREA-DT-COLLETTIVA
                                               TO S211-LUNGHEZZA(IX-MAX)
             MOVE WDCO-AREA-DT-COLLETTIVA
               TO S211-BUFFER-DATI
                 (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF WRAN-ELE-RAPP-ANAG-MAX > 0

             COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                         S211-LUNGHEZZA(IX-MAX) + 1
             ADD 1                             TO IX-MAX
      *-->   OCCORRENZA 5 RAPPORTO ANAGRAFICO
             MOVE S211-ALIAS-RAPP-ANAG         TO S211-TAB-ALIAS(IX-MAX)
             MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
             MOVE LENGTH OF VRAN-AREA-RAPP-ANAG
                                               TO S211-LUNGHEZZA(IX-MAX)
             MOVE VRAN-AREA-RAPP-ANAG
               TO S211-BUFFER-DATI
                 (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF WDFA-ELE-FISC-ADES-MAX > 0

             COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                         S211-LUNGHEZZA(IX-MAX) + 1
             ADD 1                             TO IX-MAX
      *-->   OCCORRENZA 6 DATO FISCALE ADESIONE
             MOVE S211-ALIAS-DT-FISC-ADES      TO S211-TAB-ALIAS(IX-MAX)
             MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
             MOVE LENGTH OF VDFA-TAB-FISC-ADES TO S211-LUNGHEZZA(IX-MAX)
             MOVE VDFA-TAB-FISC-ADES
               TO S211-BUFFER-DATI
                 (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF WGRZ-ELE-GARANZIA-MAX > 0
             COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                         S211-LUNGHEZZA(IX-MAX) + 1
             ADD 1                             TO IX-MAX
      *-->   OCCORRENZA 7 AREA GARANZIA
             MOVE S211-ALIAS-GARANZIA          TO S211-TAB-ALIAS(IX-MAX)
             MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
             MOVE LENGTH OF VGRZ-AREA-GARANZIA TO S211-LUNGHEZZA(IX-MAX)
             MOVE VGRZ-AREA-GARANZIA
                TO S211-BUFFER-DATI
                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

38590 *    IF WTGA-ELE-TRAN-MAX > 0
38590      IF VTGA-ELE-TRAN-MAX > 0
38590         PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
38590           UNTIL IX-TAB-TGA > VTGA-ELE-TRAN-MAX
38590           MOVE HIGH-VALUES TO VTGA-PRE-INI-NET-NULL(IX-TAB-TGA)
38590                               VTGA-PRSTZ-INI-NULL(IX-TAB-TGA)
38590         END-PERFORM
              COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                          S211-LUNGHEZZA(IX-MAX) + 1
              ADD 1                            TO IX-MAX
      *-->    OCCORRENZA 8 AREA TRANCHE DI GARANZIA
              MOVE S211-ALIAS-TRCH-GAR         TO S211-TAB-ALIAS(IX-MAX)
              MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-MAX)
              MOVE LENGTH OF VTGA-AREA-TRANCHE TO S211-LUNGHEZZA(IX-MAX)
              MOVE VTGA-AREA-TRANCHE
                TO S211-BUFFER-DATI
                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF WSPG-ELE-SOPR-GAR-MAX > 0

              COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                          S211-LUNGHEZZA(IX-MAX) + 1
              ADD 1                            TO IX-MAX
      *-->    OCCORRENZA 9 AREA SOPRAPREMIO DI GAR
              MOVE S211-ALIAS-SOPRAP-GAR       TO S211-TAB-ALIAS(IX-MAX)
              MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-MAX)
              MOVE LENGTH OF VSPG-AREA-SOPR-GAR
                                               TO S211-LUNGHEZZA(IX-MAX)
              MOVE VSPG-AREA-SOPR-GAR
                TO S211-BUFFER-DATI
                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF WSDI-ELE-STRA-INV-MAX > 0

              COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                          S211-LUNGHEZZA(IX-MAX) + 1
              ADD 1                            TO IX-MAX
      *-->    OCCORRENZA 10 AREA STRATEGIA D'INVESTIMENTO
              MOVE S211-ALIAS-STRA-INV         TO S211-TAB-ALIAS(IX-MAX)
              MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-MAX)
              MOVE LENGTH OF VSDI-AREA-STRA-INV
                                               TO S211-LUNGHEZZA(IX-MAX)
              MOVE VSDI-AREA-STRA-INV
                TO S211-BUFFER-DATI
                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF WPMO-ELE-PARAM-MOV-MAX > 0

              COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                          S211-LUNGHEZZA(IX-MAX) + 1
              ADD 1                            TO IX-MAX
      *-->    OCCORRENZA 13 AREA PARAMETRO MOVIMENTO
              MOVE S211-ALIAS-PARAM-MOV        TO S211-TAB-ALIAS(IX-MAX)
              MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-MAX)
              MOVE LENGTH OF VPMO-AREA-PARAM-MOV
                                               TO S211-LUNGHEZZA(IX-MAX)
              MOVE VPMO-AREA-PARAM-MOV
                TO S211-BUFFER-DATI
                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF WPOG-ELE-PARAM-OGG-MAX > 0

             COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                         S211-LUNGHEZZA(IX-MAX) + 1
             ADD 1                             TO IX-MAX
      *-->   OCCORRENZA 14 AREA PARAMETRO OGGETTO
             MOVE S211-ALIAS-PARAM-OGG         TO S211-TAB-ALIAS(IX-MAX)
             MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
             MOVE LENGTH OF VPOG-AREA-PARAM-OGG
                                               TO S211-LUNGHEZZA(IX-MAX)
             MOVE VPOG-AREA-PARAM-OGG
               TO S211-BUFFER-DATI
                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF WBEP-ELE-BENEFICIARI > 0

             COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                         S211-LUNGHEZZA(IX-MAX) + 1
             ADD 1                             TO IX-MAX
      *-->   OCCORRENZA 15 AREA BENEFICIARI
             MOVE S211-ALIAS-BENEF             TO S211-TAB-ALIAS(IX-MAX)
             MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
             MOVE LENGTH OF VBEP-AREA-BENEFICIARI
                                               TO S211-LUNGHEZZA(IX-MAX)
             MOVE VBEP-AREA-BENEFICIARI
               TO S211-BUFFER-DATI
                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF WQUE-ELE-QUEST-MAX > 0

             COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                         S211-LUNGHEZZA(IX-MAX) + 1
             ADD 1                             TO IX-MAX
      *-->   OCCORRENZA 16 AREA QUESTIONARIO
             MOVE S211-ALIAS-QUEST             TO S211-TAB-ALIAS(IX-MAX)
             MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
             MOVE LENGTH OF VQUE-AREA-QUEST    TO S211-LUNGHEZZA(IX-MAX)
             MOVE VQUE-AREA-QUEST
               TO S211-BUFFER-DATI
                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF WDEQ-ELE-DETT-QUEST-MAX > 0

             COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                         S211-LUNGHEZZA(IX-MAX) + 1
             ADD 1                             TO IX-MAX
      *-->   OCCORRENZA 17 AREA DETTAGLIO QUESTIONARIO
             MOVE S211-ALIAS-DETT-QUEST        TO S211-TAB-ALIAS(IX-MAX)
             MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
             MOVE LENGTH OF VDEQ-AREA-DETT-QUEST
                                               TO S211-LUNGHEZZA(IX-MAX)
             MOVE VDEQ-AREA-DETT-QUEST
               TO S211-BUFFER-DATI
                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF VOCO-ELE-OGG-COLLG-MAX > 0
             COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                         S211-LUNGHEZZA(IX-MAX) + 1
             ADD 1                             TO IX-MAX
      *-->   OCCORRENZA 18 AREA OGGETTO COLLEGATO
             MOVE S211-ALIAS-OGG-COLL          TO S211-TAB-ALIAS(IX-MAX)
             MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
             MOVE LENGTH OF VOCO-AREA-OGG-COLLG
                                               TO S211-LUNGHEZZA(IX-MAX)
             MOVE VOCO-AREA-OGG-COLLG
               TO S211-BUFFER-DATI
                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF WRRE-ELE-RAP-RETE-MAX > 0
             COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                         S211-LUNGHEZZA(IX-MAX) + 1
             ADD 1                             TO IX-MAX
      *-->   OCCORRENZA 19 AREA RAPPORTO RETE
             MOVE S211-ALIAS-RAPP-RETE         TO S211-TAB-ALIAS(IX-MAX)
             MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
             MOVE LENGTH OF WRRE-AREA-RAP-RETE TO S211-LUNGHEZZA(IX-MAX)
             MOVE WRRE-AREA-RAP-RETE
               TO S211-BUFFER-DATI
                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF WL23-ELE-VINC-PEG-MAX > 0
             COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                         S211-LUNGHEZZA(IX-MAX) + 1
             ADD 1                             TO IX-MAX
      *-->   OCCORRENZA 19 AREA RAPPORTO RETE
             MOVE S211-ALIAS-VINC-PEGN         TO S211-TAB-ALIAS(IX-MAX)
             MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
             MOVE LENGTH OF WL23-AREA-VINC-PEG TO S211-LUNGHEZZA(IX-MAX)
             MOVE WL23-AREA-VINC-PEG
               TO S211-BUFFER-DATI
                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

           IF INCLU-SOVRAP
              PERFORM  VARYING IX-SPG FROM 1 BY 1
                UNTIL IX-SPG >  WSPG-ELE-SOPR-GAR-MAX
                   IF VSPG-ST-ADD(IX-SPG)
                      MOVE VSPG-DT-INI-EFF(IX-SPG)
                        TO VMOV-DT-EFF
                      MOVE VSPG-ELE-SOPR-GAR-MAX
                        TO IX-SPG
                   END-IF
              END-PERFORM

             COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                         S211-LUNGHEZZA(IX-MAX) + 1
             ADD 1                             TO IX-MAX
      *-->   OCCORRENZA 1 AREA MOVIMENTO
             MOVE S211-ALIAS-MOVIMENTO         TO S211-TAB-ALIAS(IX-MAX)
             MOVE WK-APPO-LUNGHEZZA            TO S211-POSIZ-INI(IX-MAX)
             MOVE LENGTH OF VMOV-AREA-MOVIMENTO
                                               TO S211-LUNGHEZZA(IX-MAX)
             MOVE VMOV-AREA-MOVIMENTO
               TO S211-BUFFER-DATI
                  (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
           END-IF.

      *--> OCCORRENZA 1 AREA VARIABILI CONTESTO
RICH13     IF WCNT-ELE-VAR-CONT-MAX > 0
              COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                          S211-LUNGHEZZA(IX-MAX) + 1
RICH13        ADD 1                            TO IX-MAX
RICH13        MOVE S211-ALIAS-DATI-CONTEST     TO S211-TAB-ALIAS(IX-MAX)
RICH13        MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-MAX)
RICH13        MOVE LENGTH OF WCNT-AREA-VARIABILI-CONT
RICH13                                         TO S211-LUNGHEZZA(IX-MAX)
RICH13        MOVE WCNT-AREA-VARIABILI-CONT
RICH13          TO S211-BUFFER-DATI
RICH13            (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
RICH13     END-IF.

ITRAT *--> OCCORRENZA 1 AREA VARIABILI CONTESTO
ITRAT      IF WP67-EST-POLI-CPI-PR-MAX > 0
ITRAT         COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
ITRAT                                     S211-LUNGHEZZA(IX-MAX) + 1
ITRAT         ADD 1                            TO IX-MAX
ITRAT         MOVE S211-ALIAS-EST-POLI-CPI-PR  TO S211-TAB-ALIAS(IX-MAX)
ITRAT         MOVE WK-APPO-LUNGHEZZA           TO S211-POSIZ-INI(IX-MAX)
ITRAT         MOVE LENGTH OF WP67-AREA-EST-POLI-CPI-PR
ITRAT                                          TO S211-LUNGHEZZA(IX-MAX)
ITRAT         MOVE WP67-AREA-EST-POLI-CPI-PR
ITRAT           TO S211-BUFFER-DATI
ITRAT             (S211-POSIZ-INI(IX-MAX): S211-LUNGHEZZA(IX-MAX))
ITRAT      END-IF.



           MOVE  IX-MAX          TO S211-ELE-INFO-MAX.

       VAL-STR-DATI-PTF-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPAREA AREA TABELLA GARANZIA DEL MODULO CALCOLO RATEO
      *----------------------------------------------------------------*
       PREP-TAB-LCCC0490.

           ADD 1 TO IX-TAB-LCCC0490.

           MOVE WGRZ-COD-TARI(IX-RIC-GAR)
             TO LCCC0490-COD-TARI(IX-TAB-LCCC0490).

           MOVE WGRZ-TP-GAR(IX-RIC-GAR)
             TO LCCC0490-TP-GAR(IX-TAB-LCCC0490).

           MOVE WK-FRAZ-MM
             TO LCCC0490-FRAZIONAMENTO(IX-TAB-LCCC0490).

           MOVE WGRZ-TP-PER-PRE(IX-RIC-GAR)
             TO LCCC0490-TP-PER-PRE(IX-TAB-LCCC0490).

           MOVE WGRZ-DT-DECOR(IX-RIC-GAR)
             TO LCCC0490-DT-DECOR(IX-TAB-LCCC0490).

           EVALUATE TRUE
            WHEN QUIETANZAMENTO-SI
              MOVE 'Q'
                TO LCCC0490-TIPO-RICORRENZA(IX-TAB-LCCC0490)
            WHEN GETRA-SI
              MOVE 'G'
                TO LCCC0490-TIPO-RICORRENZA(IX-TAB-LCCC0490)
           END-EVALUATE.

       PREP-TAB-LCCC0490-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    PREPAREA AREA SERVIZIO CALCOLI E CONTROLLI ISPC0140
      *----------------------------------------------------------------*
       S12000-PREPARA-AREA-CALC-CONTR.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO ISPC0140-COD-COMPAGNIA.

           MOVE WPOL-COD-PROD
             TO ISPC0140-COD-PRODOTTO.

           MOVE WSKD-DEE                     TO ISPC0140-DEE.

           IF WPOL-COD-CONV-NULL = HIGH-VALUES
              MOVE SPACES       TO ISPC0140-COD-CONVENZIONE
           ELSE
              MOVE WPOL-COD-CONV
                TO ISPC0140-COD-CONVENZIONE
           END-IF.

           IF WPOL-DT-INI-VLDT-CONV-NULL = HIGH-VALUE
              MOVE SPACES          TO ISPC0140-DATA-INIZ-VALID-CONV
           ELSE
              MOVE WPOL-DT-INI-VLDT-CONV
                                   TO ISPC0140-DATA-INIZ-VALID-CONV
           END-IF.

           MOVE IDSV0001-TIPO-MOVIMENTO       TO WS-MOVIMENTO.

           IF WPOL-FL-VER-PROD = 'E'
              MOVE WPOL-DT-INI-VLDT-PROD    TO ISPC0140-DATA-RIFERIMENTO
           ELSE
              IF WPOL-FL-VER-PROD = 'U'
                 MOVE WCOM-DT-ULT-VERS-PROD TO ISPC0140-DATA-RIFERIMENTO
              END-IF
           END-IF.

           MOVE WCOM-COD-LIV-AUT-PROFIL
             TO ISPC0140-LIVELLO-UTENTE.

           MOVE IDSV0001-SESSIONE
             TO ISPC0140-SESSION-ID.

31805      MOVE IDSV0001-TIPO-MOVIMENTO       TO WS-MOVIMENTO
31805      IF  VERSAM-AGGIUNTIVO    OR
31805          VERSAM-AGGIUNTIVO-REINV
31805          MOVE WPOL-DT-DECOR
31805            TO ISPC0140-DATA-DECORR-POLIZZA
31805      ELSE
18098 *    IF WGRZ-DT-DECOR-NULL(1) NOT = HIGH-VALUES
18098 *       MOVE WGRZ-DT-DECOR(1)
18098       IF VGRZ-DT-DECOR-NULL(1) NOT = HIGH-VALUES
18098         MOVE VGRZ-DT-DECOR(1)
                TO ISPC0140-DATA-DECORR-POLIZZA
31805       END-IF
           END-IF.

           MOVE 'G'      TO ISPC0140-MODALITA-CALCOLO.

      *--> CHIAMATA AL SERVIZIO DI TRASCODIFICA DEL TIPO-MOVIM-PTF CON
      *--> TIPO-MOVIM-ACTUATOR
           MOVE IDSV0001-TIPO-MOVIMENTO
             TO LCCV0021-TP-MOV-PTF.

           PERFORM CALL-MATR-MOVIMENTO
              THRU CALL-MATR-MOVIMENTO-EX.

           IF IDSV0001-ESITO-OK
              MOVE LCCV0021-TP-MOV-ACT
                TO ISPC0140-FUNZIONALITA
           END-IF.

      *--> VALORIZZAZIONE OCCURS AREA INPUT SERVIZIO CALCOLI E CONTROLLI
      *--> CON L'OUTPUT DEL VALORIZZATORE VARIABILI

           PERFORM VAL-SCHEDE-ISPC0140
              THRU VAL-SCHEDE-ISPC0140-EX.
      *-->    ROUTINE VALORIZZA AREA DATI PAGINA
              PERFORM S14000-VAL-AREA-PAGINA-P
                 THRU EX-S14000
              VARYING IX-AREA-ISPC0140 FROM 1 BY 1
                UNTIL IX-AREA-ISPC0140 > ISPC0140-DATI-NUM-MAX-ELE-P

              PERFORM S14005-VAL-AREA-PAGINA-T
                 THRU EX-S14005
              VARYING IX-AREA-ISPC0140 FROM 1 BY 1
             UNTIL IX-AREA-ISPC0140 > ISPC0140-DATI-NUM-MAX-ELE-T.

       EX-S12000.
           EXIT.
      *----------------------------------------------------------------*
      *    RICHIAMA IL SERVIZIO CALCOLI E CONTROLLI
      *----------------------------------------------------------------*
       S12500-CALL-CALC-CONTR.

      *    MOVE LENGTH OF AREA-IO-CALC-CONTR
      *      TO IJCCMQ01-LENGTH-DATI-SERVIZIO.
      *
      *    MOVE AREA-IO-CALC-CONTR
      *      TO IJCCMQ01-AREA-DATI-SERVIZIO.
      *
      *    SET WS-ADDRESS TO  ADDRESS OF AREA-PRODUCT-SERVICES.
      *
      *    MOVE IDSV0001-LIVELLO-DEBUG   TO IJCCMQ00-LIVELLO-DEBUG
      *    MOVE IDSV0001-AREA-ADDRESSES  TO IJCCMQ01-AREA-ADDRESSES
      *    MOVE IDSV0001-USER-NAME       TO IJCCMQ01-USER-NAME
      *
      *    CALL INTERF-MQSERIES USING MQ01-ADDRESS
      *                               AREA-PRODUCT-SERVICES
           CALL ISPS0140 USING AREA-IDSV0001
                               WCOM-AREA-STATI
                               AREA-IO-CALC-CONTR

           ON EXCEPTION
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SERVIZIO CALCOLI E CONTROLLI' TO CALL-DESC
              MOVE 'S12500-CALL-CALC-CONTR'       TO IEAI9901-LABEL-ERR
               PERFORM S0290-ERRORE-DI-SISTEMA
                  THRU EX-S0290
           END-CALL.

      *-->  ReturnCode ACTUATOR
      *    IF IJCCMQ01-ESITO-OK
      *       MOVE IJCCMQ01-AREA-DATI-SERVIZIO
      *         TO AREA-IO-CALC-CONTR
      *       MOVE ISPC0140-AREA-ERRORI   TO ISPC0001-AREA-ERRORI
      *       MOVE S211-STEP-ELAB         TO ISPC0001-STEP-ELAB
      *
      *       PERFORM S0320-OUTPUT-PRODOTTO
      *          THRU EX-S0320-OUTPUT-PRODOTTO
      *    ELSE
      *-->    Gestione Errori Area IJCSMQ01
      *       SET IDSV0001-ESITO-KO TO TRUE
      *       MOVE IJCCMQ01-MAX-ELE-ERRORI
      *         TO IDSV0001-MAX-ELE-ERRORI
      *       PERFORM VARYING IX-TAB-ERR FROM 1 BY 1
      *         UNTIL IX-TAB-ERR > IJCCMQ01-MAX-ELE-ERRORI
      *           MOVE IJCCMQ01-COD-ERRORE(IX-TAB-ERR)
      *             TO IDSV0001-COD-ERRORE(IX-TAB-ERR)
      *           MOVE IJCCMQ01-DESC-ERRORE(IX-TAB-ERR)
      *             TO IDSV0001-DESC-ERRORE(IX-TAB-ERR)
      *           MOVE IJCCMQ01-LIV-GRAVITA-BE(IX-TAB-ERR)
      *             TO IDSV0001-LIV-GRAVITA-BE(IX-TAB-ERR)
      *       END-PERFORM
      *    END-IF.

       EX-S12500.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE DELLA AREA PAGINA
      *----------------------------------------------------------------*
       S14000-VAL-AREA-PAGINA-P.

           IF ISPC0140-O-TIPO-LIVELLO-P(IX-AREA-ISPC0140) = 'P'
              PERFORM S14050-GEST-COMP-PRODOTTO
                 THRU S14050-EX
           END-IF.

       EX-S14000.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE DELLA AREA PAGINA
      *----------------------------------------------------------------*
       S14005-VAL-AREA-PAGINA-T.

           IF ISPC0140-O-TIPO-LIVELLO-T(IX-AREA-ISPC0140) = 'G'
              PERFORM S14025-GEST-COMP-GARANZIA
                 THRU S14025-EX
           END-IF.

       EX-S14005.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE DELLA AREA PAGINA E DELLA DCLGEN
      *    TRANCHE DI GARANZIA PER PREMIO ANNUO
      *----------------------------------------------------------------*
       S14025-GEST-COMP-GARANZIA.

      *--> PER L'EMISSIONE PROPOSTA INDIVIDUALE:
      *--> SOLO PER LA GARANZIA BASE
      *--> SE E' PRESENTE LA COMPONENTE 'PRESTNEG' OLTRE ALLA TRANCHE
      *--> POSITIVA SARA' SEMPRE ASSOCIATA UNA TRANCHE DI TIPO NEGATIVO
      *--> CON TIPO TRANCHE = 9(NEGATIVA PRELIEVO COSTI).
      *--> L'AREA DI PAGINA E' STATA MODIFICATA PER PREVEDERE 2 TRANCHE
      *--> PER OGNI GARANZIA ELABORATA

      *    SET QUESTSAN-NO TRCH-NEG-NO TO TRUE
           SET  TRCH-NEG-NO            TO TRUE
AG         SET  FLAG-RATALO-NO         TO TRUE
AG         MOVE SPACES                 TO WS-TP-TRCH

           MOVE ZEROES                 TO IX-TAB-PAR
                                          IX-TAB-SPG
           MOVE 1                      TO IX-TAB-TGA
           ADD  1                      TO IX-AREA-PAGINA
                                          WPAG-ELE-CALCOLI-MAX
           MOVE ISPC0140-O-CODICE-LIVELLO-T(IX-AREA-ISPC0140)
             TO WPAG-COD-GARANZIA(IX-AREA-PAGINA)
                WK-COD-GAR(IX-AREA-PAGINA)

      *--  CONTROLLO SU TIPO PREMIO (ANNUO-RATA-FIRMA)
           PERFORM VARYING IX-TP-PREMIO FROM 1 BY 1
                     UNTIL IX-TP-PREMIO > ISPC0140-PRE-NUM-MAX-ELE-T
                                         (IX-AREA-ISPC0140)

             EVALUATE ISPC0140-CODICE-TP-PREMIO-T
                     (IX-AREA-ISPC0140, IX-TP-PREMIO)
               WHEN 'ANNUO'
                 PERFORM S14100-PREMIO-ANNUO
                    THRU EX-S14100
                 VARYING IX-COMPONENTE FROM 1 BY 1
                   UNTIL IX-COMPONENTE > ISPC0140-O-NUM-COMP-MAX-ELE-T
                                     (IX-AREA-ISPC0140, IX-TP-PREMIO)

AG                IF FLAG-RATALO-SI
                     CONTINUE
                  ELSE
                     SET FLAG-GAR-NO TO TRUE
                     MOVE ZEROES     TO WS-TP-GAR

                     PERFORM VARYING IX-IND-GAR FROM 1 BY 1
                       UNTIL IX-IND-GAR > WGRZ-ELE-GARANZIA-MAX
                          OR FLAG-GAR-SI

                          IF NOT WGRZ-ST-DEL(IX-IND-GAR)
                             IF WGRZ-COD-TARI(IX-IND-GAR) =
                                WPAG-COD-GARANZIA(IX-AREA-PAGINA)

                                MOVE WGRZ-TP-GAR(IX-IND-GAR)
                                  TO WS-TP-GAR

                                SET FLAG-GAR-SI TO TRUE

                             END-IF
                          END-IF

                     END-PERFORM

                     IF FLAG-GAR-SI
                      AND WS-TP-GAR = 1 OR 2
                      AND WS-TP-TRCH = '01'

                        MOVE WPAG-PRE-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                          TO WPAG-RAT-LRD(IX-AREA-PAGINA, IX-TAB-TGA)

                     END-IF

AG                END-IF

      *--        SCRITTURA DELLA WPAG PER LA TRANCHE NEGATIVA
                 PERFORM S14180-VAL-TRCH-NEGATIVA
                    THRU EX-S14180
               WHEN 'RATA'
                 PERFORM S14200-PREMIO-RATA
                    THRU EX-S14200
                 VARYING IX-COMPONENTE FROM 1 BY 1
                   UNTIL IX-COMPONENTE > ISPC0140-O-NUM-COMP-MAX-ELE-T
                                     (IX-AREA-ISPC0140, IX-TP-PREMIO)
               WHEN 'FIRMA'
                 PERFORM S14300-PREMIO-FIRMA
                    THRU EX-S14300
                 VARYING IX-COMPONENTE FROM 1 BY 1
                   UNTIL IX-COMPONENTE > ISPC0140-O-NUM-COMP-MAX-ELE-T
                                     (IX-AREA-ISPC0140, IX-TP-PREMIO)
             END-EVALUATE

           END-PERFORM.

       S14025-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Gestione delle componenti di ACTUATOR generale a livello
      *    di prodotto
      *----------------------------------------------------------------*
       S14050-GEST-COMP-PRODOTTO.

      *--  CONTROLLO SU TIPO PREMIO (ANNUO-RATA-FIRMA)
           PERFORM VARYING IX-TP-PREMIO FROM 1 BY 1
                     UNTIL IX-TP-PREMIO > ISPC0140-PRE-NUM-MAX-ELE-P
                                         (IX-AREA-ISPC0140)

             EVALUATE ISPC0140-CODICE-TP-PREMIO-P
                     (IX-AREA-ISPC0140, IX-TP-PREMIO)
               WHEN 'ANNUO'
                 PERFORM S14052-PREMIO-ANNUO-PROD
                    THRU S14052-EX
                 VARYING IX-COMPONENTE FROM 1 BY 1
                   UNTIL IX-COMPONENTE > ISPC0140-O-NUM-COMP-MAX-ELE-P
                                     (IX-AREA-ISPC0140, IX-TP-PREMIO)
               WHEN 'RATA'
               WHEN 'FIRMA'
                  CONTINUE
             END-EVALUATE

           END-PERFORM.

           EVALUATE TRUE
             WHEN QUESTSAN-SI
22355          SET WPAG-FL-QUESTSAN-SPECIALE-NO  TO TRUE
               SET WPAG-FL-PARAM-QUESTSAN-SI     TO TRUE

             WHEN QUESTSAN-SPECIALE
               SET WPAG-FL-QUESTSAN-SPECIALE-SI  TO TRUE
22355          SET WPAG-FL-PARAM-QUESTSAN-NO     TO TRUE

             WHEN OTHER
22355          SET WPAG-FL-QUESTSAN-SPECIALE-NO  TO TRUE
               SET WPAG-FL-PARAM-QUESTSAN-NO     TO TRUE

           END-EVALUATE.

           IF VMED-OBBLIGATORIA
              SET WPAG-FL-VISITA-MEDICA-OBB  TO TRUE
           END-IF.

       S14050-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Gestione delle componenti relative al prodotto e premio
      *    Annuo
      *----------------------------------------------------------------*
       S14052-PREMIO-ANNUO-PROD.

           IF ISPC0140-CODICE-COMPONENTE-P
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'TIPIAS'
              EVALUATE ISPC0140-COMP-TIPO-DATO-P
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-P
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WK-APPO-IAS
                  MOVE WK-IAS-2
                    TO WPAG-TP-IAS-ADE
              END-EVALUATE
           END-IF.

      *--> SE IL VALORE DEL QUESTSAN h ZERO ALLORA IL QUESTIONARIO h
      *--> AMMESSO
           IF ISPC0140-CODICE-COMPONENTE-P
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'QUESTSAN'
              EVALUATE ISPC0140-COMP-TIPO-DATO-P
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
              WHEN 'N'
              WHEN 'I'
                 IF ISPC0140-COMP-VALORE-IMP-P
                    (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 1
                    SET  QUESTSAN-SI           TO TRUE
                 ELSE
                   IF ISPC0140-COMP-VALORE-IMP-P
                    (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 2
                      SET  QUESTSAN-SPECIALE   TO TRUE
                   END-IF
                 END-IF

              WHEN 'S'
                 IF ISPC0140-COMP-VALORE-STR-P
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = '1'
                    SET  QUESTSAN-SI           TO TRUE
                 ELSE
                    IF ISPC0140-COMP-VALORE-STR-P
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = '2'
                       SET  QUESTSAN-SPECIALE  TO TRUE
                    END-IF
                 END-IF
              END-EVALUATE
           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-P
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'VISITAMED'
              EVALUATE ISPC0140-COMP-TIPO-DATO-P
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
              WHEN 'N'
              WHEN 'I'
                 MOVE ISPC0140-COMP-VALORE-IMP-P
                    (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WK-VISITA-MEDICA
              END-EVALUATE
           END-IF.

11141      IF ISPC0140-CODICE-COMPONENTE-P
11141         (IX-AREA-ISPC0140, IX-TP-PREMIO,
11141          IX-COMPONENTE) = 'MODRICPROV'
11141         EVALUATE ISPC0140-COMP-TIPO-DATO-P
11141              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
11141           WHEN 'S'
11141           WHEN 'D'
11141                MOVE ISPC0140-COMP-VALORE-STR-P
11141                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
11141                TO WPAG-TP-MOD-PROV
11141         END-EVALUATE
11141      END-IF.

           MOVE IDSV0001-TIPO-MOVIMENTO       TO WS-MOVIMENTO

      * INIZIO SIR 11254 IN SEGUITO ALLA SEGNALAZIONE DI BNL
      * I CONTROLLI SUL TIPO IAS DI GARANZIA E ADESIONE DEVONO ESSERE
      * EFFETTUATI SOLO NEL CASO DI INCLUSIONE DI NUOVA GARANZIA
      * I DETT. DEI CONTROLLI  DA EFFETTUARE IN CASO DI INCLUSIONE
      * GARANZIA SARANNO DEFINITI SUCCESSIVAMENTE

           IF  VERSAM-AGGIUNTIVO    OR
              VERSAM-AGGIUNTIVO-REINV
      * REGRESSIONE DEL 04/12/2010 INIZIO ====>
               MOVE 1 TO WPAG-ELE-MAX-IAS
      * REGRESSIONE DEL 04/12/2010 FINE   <====
               SET WPAG-NO-MOD-IAS(WPAG-ELE-MAX-IAS) TO TRUE
           END-IF
      * FINE SIR 11254

           IF INCLU-GARANZ
11254 *    OR VERSAM-AGGIUNTIVO    OR
11254 *       VERSAM-AGGIUNTIVO-REINV
              IF ISPC0140-CODICE-COMPONENTE-P
              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'TIPIAS'
                 ADD  1                 TO WPAG-ELE-MAX-IAS
                 MOVE WADE-ID-ADES      TO WPAG-ID-OGG(WPAG-ELE-MAX-IAS)
                 MOVE 'AD'              TO WPAG-TP-OGG(WPAG-ELE-MAX-IAS)
                 MOVE WADE-TP-IAS
                   TO WPAG-TP-IAS-OLD(WPAG-ELE-MAX-IAS)
                 MOVE WPAG-TP-IAS-ADE
                   TO WPAG-TP-IAS-NEW(WPAG-ELE-MAX-IAS)
                 IF WPAG-TP-IAS-OLD(WPAG-ELE-MAX-IAS)  =
                    WPAG-TP-IAS-NEW(WPAG-ELE-MAX-IAS)
                    SET WPAG-NO-MOD-IAS(WPAG-ELE-MAX-IAS)      TO  TRUE
                 ELSE
                    SET WPAG-SI-MOD-IAS(WPAG-ELE-MAX-IAS)      TO  TRUE
                 END-IF
              END-IF
           END-IF.

11174      IF ISPC0140-CODICE-COMPONENTE-P
11174        (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
11174                                                       'INCAUTOGEN'
11174         EVALUATE ISPC0140-COMP-TIPO-DATO-P
11174                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
11174           WHEN 'S'
11174             MOVE ISPC0140-COMP-VALORE-STR-P
11174                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
11174               TO WK-FL-INC-AUTOGEN
11174         END-EVALUATE
11174      END-IF.

       S14052-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE DELLA AREA PAGINA E DELLA DCLGEN
      *    TRANCHE DI GARANZIA PER PREMIO ANNUO
      *----------------------------------------------------------------*
       S14100-PREMIO-ANNUO.

      *--> ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
      *--> VALORIZZARE LA DCLGEN TRANCHE DI GARANZIA
           PERFORM S14110-CONTR-COMP-TRANCHE
              THRU EX-S14110.

      *--> ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
      *--> VALORIZZARE I DATI PARAMETRO CALCOLO DELL'AREA DI PAGINA
           PERFORM S14120-CONTR-COMP-PAR-CALC
              THRU EX-S14120.

      *--> ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
      *--> VALORIZZARE I DATI PROVVIGIONI DELL'AREA DI PAGINA
           PERFORM S14130-CONTR-COMP-PROVV
              THRU EX-S14130.

      *--> ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
      *--> VALORIZZARE GLI ALTRI DATI DELL'AREA DI PAGINA
           PERFORM S14140-CONTR-COMP-ALTRI
              THRU EX-S14140.

      *--> ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
      *--> VALORIZZARE I DATI PARAMETRO MOVIMENTO DELL'AREA DI PAGINA
           PERFORM S14145-CONTR-COMP-PAR-MOVIM
              THRU EX-S14145.

      *--> ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER VALORIZ.
      *--> I DATI SOPRAPPREMI DI GARANZIA DELL'AREA DI PAGINA
           PERFORM S14150-CONTR-COMP-SOPR-GAR
              THRU EX-S14150.

       EX-S14100.
           EXIT.
      *----------------------------------------------------------------*
      *    PARAGRAFO DI VALORIZZAZIONE TRANCHE NEGATIVA
      *----------------------------------------------------------------*
       S14180-VAL-TRCH-NEGATIVA.
      *--> GESTIONE TRANCHE NEGATIVA
           IF TRCH-NEG-SI
              ADD 1 TO IX-TAB-TGA
              MOVE WK-PREMIO-NEG
                TO WPAG-PRE-INI-NET(IX-AREA-PAGINA, IX-TAB-TGA)
                   WPAG-RAT-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                   WPAG-PRE-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                   WPAG-PRSTZ-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                   WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                   WPAG-PRE-PP-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                   WPAG-PRE-PP-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                   WPAG-IMP-MOVI-NEG(IX-AREA-PAGINA)
              MOVE 'S'
                TO WPAG-FL-PREL-RIS(IX-AREA-PAGINA, IX-TAB-TGA)
              MOVE '9'
                TO WPAG-TP-TRCH(IX-AREA-PAGINA, IX-TAB-TGA)
           END-IF.
           MOVE IX-TAB-TGA   TO WPAG-ELE-TRANCHE-MAX(IX-AREA-PAGINA).

       EX-S14180.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
      *    VALORIZZARE LA DCLGEN TRANCHE DI GARANZIA
      *----------------------------------------------------------------*
       S14110-CONTR-COMP-TRANCHE.


      *--> interessi di frazionamento da visualizzare in elenco tranche
      *--> deve essere relativo al premio annuo.
           IF ISPC0140-CODICE-COMPONENTE-T
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'INTFRAZ'
              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
              WHEN 'N'
              WHEN 'I'
                 MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-TGA-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-TGA)

              END-EVALUATE
           END-IF.

      *--> DATI TRANCHE DI GARANZIA
           IF ISPC0140-CODICE-COMPONENTE-T
              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'CODREGFISC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                      (10:2)
                    TO WPAG-TP-RGM-FISC(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE
           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'TIPOTR'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-TP-TRCH(IX-AREA-PAGINA, IX-TAB-TGA)
AG                     WS-TP-TRCH

              END-EVALUATE
           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'PCASOM'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-CASO-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-CASO-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'A'
                WHEN 'P'
                WHEN 'M'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-CASO-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'BONANT'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-BNS-ANTIC(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-BNS-ANTIC(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-BNS-ANTIC(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'PNETTO'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-INI-NET(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-INI-NET(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-INI-NET(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'PPUROI'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-PP-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                       WPAG-PRE-PP-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-PP-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                       WPAG-PRE-PP-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-PP-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                       WPAG-PRE-PP-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'PTARIF'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-TARI-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                       WPAG-PRE-TARI-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-TARI-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                       WPAG-PRE-TARI-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-TARI-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                       WPAG-PRE-TARI-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'PINVEN'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-INVRIO-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                       WPAG-PRE-INVRIO-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-INVRIO-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                       WPAG-PRE-INVRIO-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-INVRIO-INI(IX-AREA-PAGINA, IX-TAB-TGA)
                       WPAG-PRE-INVRIO-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'SOPSAN'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-SOPR-SAN(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-SOPR-SAN(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-SOPR-SAN(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'SOPPRO'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-SOPR-PROF(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-SOPR-PROF(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-SOPR-PROF(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'SOPSPO'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-SOPR-SPO(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-SOPR-SPO(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-SOPR-SPO(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'SOPTEC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-SOPR-TEC(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-SOPR-TEC(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-SOPR-TEC(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'SOPALT'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-ALT-SOPR(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-ALT-SOPR(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-ALT-SOPR(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'RIVFIX'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-TS-RIVAL-FIS(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-TS-RIVAL-FIS(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-TS-RIVAL-FIS(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'RATALO'

AG            SET FLAG-RATALO-SI                    TO TRUE

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RAT-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RAT-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RAT-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PRETOT'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRE-LRD(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'PRESTINI'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRSTZ-INI(IX-AREA-PAGINA, IX-TAB-TGA)
      *                 WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRSTZ-INI(IX-AREA-PAGINA, IX-TAB-TGA)
      *                 WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRSTZ-INI(IX-AREA-PAGINA, IX-TAB-TGA)
      *                 WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'CAPINI'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

            END-IF.

            IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'CAPOPZ'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CPT-IN-OPZ-RIVTO(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CPT-IN-OPZ-RIVTO(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CPT-IN-OPZ-RIVTO(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'CAPMOR'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CPT-RSH-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CPT-RSH-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CPT-RSH-MOR(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'IMPSCO'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'ALISCO'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-ALQ-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-ALQ-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-ALQ-SCON(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'CARACQ'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-CAR-ACQ-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-CAR-ACQ-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-CAR-ACQ-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

ITRAT      IF ISPC0140-CODICE-COMPONENTE-T
ITRAT        (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT       = 'ACQEXP'

ITRAT         EVALUATE ISPC0140-COMP-TIPO-DATO-T
ITRAT              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT           WHEN 'S'
ITRAT           WHEN 'D'
ITRAT             MOVE ISPC0140-COMP-VALORE-STR-T
ITRAT              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-TGA-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-TGA)
ITRAT           WHEN 'N'
ITRAT           WHEN 'I'
ITRAT             MOVE ISPC0140-COMP-VALORE-IMP-T
ITRAT              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-TGA-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-TGA)
ITRAT           WHEN 'P'
ITRAT           WHEN 'M'
ITRAT           WHEN 'A'
ITRAT             MOVE ISPC0140-COMP-VALORE-PERC-T
ITRAT              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-TGA-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-TGA)
ITRAT         END-EVALUATE

ITRAT      END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'CARINC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-CAR-INC-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-CAR-INC-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-CAR-INC-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'CARGES'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                   MOVE ISPC0140-COMP-VALORE-STR-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-IMP-CAR-GEST-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'N'
                WHEN 'I'
                   MOVE ISPC0140-COMP-VALORE-IMP-T
                    (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-IMP-CAR-GEST-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                   MOVE ISPC0140-COMP-VALORE-PERC-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-IMP-CAR-GEST-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

      *--> IMPORTO AZIENDA (FONTI CONTRIBUTIVE)
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'IMPAZIENDA'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                   MOVE ISPC0140-COMP-VALORE-IMP-T
                    (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                     TO WPAG-IMP-AZ-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

      *--> IMPORTO ADERENTE (FONTI CONTRIBUTIVE)
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'IMPADERENTE'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                   MOVE ISPC0140-COMP-VALORE-IMP-T
                    (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                     TO WPAG-IMP-ADER-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

      *--> IMPORTO TFR (FONTI CONTRIBUTIVE)
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'IMPTFR'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                   MOVE ISPC0140-COMP-VALORE-IMP-T
                    (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                     TO WPAG-IMP-TFR-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

      *--> IMPORTO VOLONTARIO (FONTI CONTRIBUTIVE)
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'IMPVOLONT'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                   MOVE ISPC0140-COMP-VALORE-IMP-T
                    (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                     TO WPAG-IMP-VOLO-TGA(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

      *--> IMPORTO ABBUONO ANNUO ULTIMO
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'ABBANNUO'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                   MOVE ISPC0140-COMP-VALORE-IMP-T
                    (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-ABB-ANNO-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

      *--> IMPORTO ABBUONO TOTALE INIZIALE
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'ABBTOT'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                   MOVE ISPC0140-COMP-VALORE-IMP-T
                    (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                     TO WPAG-IMP-ABB-TOT-INI(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE
           END-IF.

      *--> DURATA ANNI ABBUONO
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'ANNIABB'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                   MOVE ISPC0140-COMP-VALORE-IMP-T
                    (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                     TO WPAG-DUR-ABB-ANNI(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE
           END-IF.

      *--> IMPORTO PREMIO PATTUITO
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'PATTUITO'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                   MOVE ISPC0140-COMP-VALORE-IMP-T
                    (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-PRE-PATTUITO(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE
           END-IF.

      *--> IMPORTO PRESTAZIONE ULTIMA
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'PRESTULT'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                   MOVE ISPC0140-COMP-VALORE-IMP-T
                    (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                     TO WPAG-IMP-PREST-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
                        WPAG-PRSTZ-ULT(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE
           END-IF.

      *--> IMPORTO PREMIO NETTO ULTIMO RIVALUTATO
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'PREULT'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                   MOVE ISPC0140-COMP-VALORE-IMP-T
                    (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                     TO WPAG-IMP-PRE-RIVAL(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE
           END-IF.

      *--> ALIQUOTA PROVVIGIONE INCASSO
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ALIQINC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-TGA-ALIQ-INCAS(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

      *--> ALIQUOTA PROVVIGIONE ACQUISTO
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ALIQACQ'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-TGA-ALIQ-ACQ(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

      *--> ALIQUOTA PROVVIGIONE RICORRENTE
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ALIQRIC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-TGA-ALIQ-RICOR(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

      *--> IMPORTO IMPONIBILE PROVVIGIONE INCASSO
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPOINC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-TGA-IMP-INCAS(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

      *--> IMPORTO IMPONIBILE PROVVIGIONE ACQUISTO
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPOACQ'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-TGA-IMP-ACQ(IX-AREA-PAGINA, IX-TAB-TGA)

              END-EVALUATE

           END-IF.

      *--> IMPORTO IMPONIBILE PROVVIGIONE RICORRENTE
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPORIC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-TGA-IMP-RICOR(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

      *--> IMPORTO PRESTAZIONE AGGIUNTIVA X TRASF.COMMERCIALE INIZIALE
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'PRESTAGG'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-IMP-PRSTZ-AGG-INI(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'PRESTNEG'
      *--> SE E' PRESENTE QUESTA COMPONENTE LA GARANZIA BASE PREVEDE
      *--> UNA TRANCHE NEGATIVA
              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
              WHEN 'I'
                 MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                  TO WK-PREMIO-NEG
                 SET TRCH-NEG-SI            TO TRUE
              END-EVALUATE
           END-IF.

      *--  Importo Managment fee
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'MANFEE'
              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
              WHEN 'I'
                 MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-MANFEE-ANTIC(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE
           END-IF.

      *--  Rendita Iniziale a Tasso 0
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'PRESTINI0'
              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                   MOVE ISPC0140-COMP-VALORE-IMP-T
                       (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-REN-INI-TS-TEC-0(IX-AREA-PAGINA, IX-TAB-TGA)
              END-EVALUATE
           END-IF.

           MOVE IX-TAB-TGA   TO WPAG-ELE-TRANCHE-MAX(IX-AREA-PAGINA).

       EX-S14110.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
      *    VALORIZZARE I DATI PARAMETRO CALCOLO DELL'AREA DI PAGINA
      *----------------------------------------------------------------*
       S14120-CONTR-COMP-PAR-CALC.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            =  'PPI' OR 'PII' OR 'PTI' OR 'PNI'

              ADD 1         TO IX-TAB-PAR

              MOVE ISPC0140-CODICE-COMPONENTE-T
                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WPAG-COD-PARAM(IX-AREA-PAGINA, IX-TAB-PAR)

      *        MOVE 'A' TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)
              MOVE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
      *---      date e stringhe
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-VAL-STR(IX-AREA-PAGINA, IX-TAB-PAR)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-VAL-IMP(IX-AREA-PAGINA, IX-TAB-PAR)
      *--       percentuali
                WHEN 'P'
                WHEN 'M'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
      *--       tasso
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-VAL-TS(IX-AREA-PAGINA, IX-TAB-PAR)
              END-EVALUATE

           END-IF.

      *--  Aliquote
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
             'ACARACQ' OR 'ACARINC' OR 'ACARGES'

              ADD 1         TO IX-TAB-PAR

              MOVE ISPC0140-CODICE-COMPONENTE-T
                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WPAG-COD-PARAM(IX-AREA-PAGINA, IX-TAB-PAR)

      *        MOVE 'P'      TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)
              MOVE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-VAL-STR(IX-AREA-PAGINA, IX-TAB-PAR)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-VAL-IMP(IX-AREA-PAGINA, IX-TAB-PAR)
                WHEN 'P'
                WHEN 'M'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-VAL-TS(IX-AREA-PAGINA, IX-TAB-PAR)
              END-EVALUATE
           END-IF.

      *--  Aliquote
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ALIQACQ'
              OR 'ALIQINC'  OR 'ALIQGES'

              ADD 1         TO IX-TAB-PAR

              MOVE ISPC0140-CODICE-COMPONENTE-T
                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WPAG-COD-PARAM(IX-AREA-PAGINA, IX-TAB-PAR)

      *        MOVE 'P'      TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)
              MOVE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)


              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
              END-EVALUATE

           END-IF.

      *--  Tasso Tecnico
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'TASTEC'

              ADD 1         TO IX-TAB-PAR

              MOVE ISPC0140-CODICE-COMPONENTE-T
                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WPAG-COD-PARAM(IX-AREA-PAGINA, IX-TAB-PAR)

              MOVE ISPC0140-COMP-TIPO-DATO-T
                   (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-VAL-PC(IX-AREA-PAGINA, IX-TAB-PAR)
              END-EVALUATE

           END-IF.

      *--  Tavola mortalit`
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
             'TAVMORTM' OR 'TAVMORTF' OR 'TAVMORTR'

              ADD 1         TO IX-TAB-PAR

              MOVE ISPC0140-CODICE-COMPONENTE-T
                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WPAG-COD-PARAM(IX-AREA-PAGINA, IX-TAB-PAR)

              MOVE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WPAG-TP-D(IX-AREA-PAGINA, IX-TAB-PAR)

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-VAL-STR(IX-AREA-PAGINA, IX-TAB-PAR)
              END-EVALUATE
           END-IF.

           MOVE IX-TAB-PAR    TO WPAG-ELE-PAR-CALC-MAX(IX-AREA-PAGINA).

       EX-S14120.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
      *    VALORIZZARE I DATI PROVVIGIONI DELL'AREA DI PAGINA
      *----------------------------------------------------------------*
       S14130-CONTR-COMP-PROVV.

      *--> DATI PROVVIGIONI
           IF ISPC0140-CODICE-COMPONENTE-T
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVACQ1A'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRV-ACQ-1O-ANNO(IX-AREA-PAGINA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRV-ACQ-1O-ANNO(IX-AREA-PAGINA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRV-ACQ-1O-ANNO(IX-AREA-PAGINA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVACQ2A'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRV-ACQ-2O-ANNO(IX-AREA-PAGINA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRV-ACQ-2O-ANNO(IX-AREA-PAGINA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRV-ACQ-2O-ANNO(IX-AREA-PAGINA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVRIC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRV-RICOR(IX-AREA-PAGINA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRV-RICOR(IX-AREA-PAGINA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRV-RICOR(IX-AREA-PAGINA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVINC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRV-INCAS(IX-AREA-PAGINA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRV-INCAS(IX-AREA-PAGINA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-PRV-INCAS(IX-AREA-PAGINA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ALIQINC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-ALIQ-INCAS(IX-AREA-PAGINA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-ALIQ-INCAS(IX-AREA-PAGINA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-ALIQ-INCAS(IX-AREA-PAGINA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ALIQACQ'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-ALIQ-ACQ(IX-AREA-PAGINA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-ALIQ-ACQ(IX-AREA-PAGINA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-ALIQ-ACQ(IX-AREA-PAGINA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ALIQRIC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-ALIQ-RICOR(IX-AREA-PAGINA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-ALIQ-RICOR(IX-AREA-PAGINA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-ALIQ-RICOR(IX-AREA-PAGINA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPOINC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-INCAS(IX-AREA-PAGINA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-INCAS(IX-AREA-PAGINA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-INCAS(IX-AREA-PAGINA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPOACQ'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-ACQ(IX-AREA-PAGINA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-ACQ(IX-AREA-PAGINA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-ACQ(IX-AREA-PAGINA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPORIC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-RICOR(IX-AREA-PAGINA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-RICOR(IX-AREA-PAGINA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-RICOR(IX-AREA-PAGINA)
              END-EVALUATE

           END-IF.

ITRAT      IF ISPC0140-CODICE-COMPONENTE-T
ITRAT      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
ITRAT       'ALIQASSICUR'

ITRAT         EVALUATE ISPC0140-COMP-TIPO-DATO-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT           WHEN 'S'
ITRAT           WHEN 'D'
ITRAT             MOVE ISPC0140-COMP-VALORE-STR-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-ALIQ-REN-ASS(IX-AREA-PAGINA)
ITRAT           WHEN 'N'
ITRAT           WHEN 'I'
ITRAT             MOVE ISPC0140-COMP-VALORE-IMP-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-ALIQ-REN-ASS(IX-AREA-PAGINA)
ITRAT           WHEN 'P'
ITRAT           WHEN 'M'
ITRAT           WHEN 'A'
ITRAT             MOVE ISPC0140-COMP-VALORE-PERC-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-ALIQ-REN-ASS(IX-AREA-PAGINA)
ITRAT         END-EVALUATE

ITRAT      END-IF.

ITRAT      IF ISPC0140-CODICE-COMPONENTE-T
ITRAT      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'REMUNASS'

ITRAT         EVALUATE ISPC0140-COMP-TIPO-DATO-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT           WHEN 'S'
ITRAT           WHEN 'D'
ITRAT             MOVE ISPC0140-COMP-VALORE-STR-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-PRV-REN-ASS(IX-AREA-PAGINA)
ITRAT           WHEN 'N'
ITRAT           WHEN 'I'
ITRAT             MOVE ISPC0140-COMP-VALORE-IMP-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-PRV-REN-ASS(IX-AREA-PAGINA)
ITRAT           WHEN 'P'
ITRAT           WHEN 'M'
ITRAT           WHEN 'A'
ITRAT             MOVE ISPC0140-COMP-VALORE-PERC-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-PRV-REN-ASS(IX-AREA-PAGINA)
ITRAT         END-EVALUATE

ITRAT      END-IF.

ITRAT      IF ISPC0140-CODICE-COMPONENTE-T
ITRAT      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
ITRAT       'ALIQCOMINT'

ITRAT         EVALUATE ISPC0140-COMP-TIPO-DATO-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT           WHEN 'S'
ITRAT           WHEN 'D'
ITRAT             MOVE ISPC0140-COMP-VALORE-STR-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-ALIQ-COMMIS-INT(IX-AREA-PAGINA)
ITRAT           WHEN 'N'
ITRAT           WHEN 'I'
ITRAT             MOVE ISPC0140-COMP-VALORE-IMP-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-ALIQ-COMMIS-INT(IX-AREA-PAGINA)
ITRAT           WHEN 'P'
ITRAT           WHEN 'M'
ITRAT           WHEN 'A'
ITRAT             MOVE ISPC0140-COMP-VALORE-PERC-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-ALIQ-COMMIS-INT(IX-AREA-PAGINA)
ITRAT         END-EVALUATE

ITRAT      END-IF.

ITRAT      IF ISPC0140-CODICE-COMPONENTE-T
ITRAT      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'COMMINTER'

ITRAT         EVALUATE ISPC0140-COMP-TIPO-DATO-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT           WHEN 'S'
ITRAT           WHEN 'D'
ITRAT             MOVE ISPC0140-COMP-VALORE-STR-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-PRV-COMMIS-INT(IX-AREA-PAGINA)
ITRAT           WHEN 'N'
ITRAT           WHEN 'I'
ITRAT             MOVE ISPC0140-COMP-VALORE-IMP-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-PRV-COMMIS-INT(IX-AREA-PAGINA)
ITRAT           WHEN 'P'
ITRAT           WHEN 'M'
ITRAT           WHEN 'A'
ITRAT             MOVE ISPC0140-COMP-VALORE-PERC-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-PRV-COMMIS-INT(IX-AREA-PAGINA)
ITRAT         END-EVALUATE

ITRAT      END-IF.

ITRAT      IF ISPC0140-CODICE-COMPONENTE-T
ITRAT      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
ITRAT      'IMPOREMUNASS'

ITRAT         EVALUATE ISPC0140-COMP-TIPO-DATO-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT           WHEN 'S'
ITRAT           WHEN 'D'
ITRAT             MOVE ISPC0140-COMP-VALORE-STR-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-IMP-REN-ASS(IX-AREA-PAGINA)
ITRAT           WHEN 'N'
ITRAT           WHEN 'I'
ITRAT             MOVE ISPC0140-COMP-VALORE-IMP-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-IMP-REN-ASS(IX-AREA-PAGINA)
ITRAT           WHEN 'P'
ITRAT           WHEN 'M'
ITRAT           WHEN 'A'
ITRAT             MOVE ISPC0140-COMP-VALORE-PERC-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-IMP-REN-ASS(IX-AREA-PAGINA)
ITRAT         END-EVALUATE

ITRAT      END-IF.

ITRAT      IF ISPC0140-CODICE-COMPONENTE-T
ITRAT      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
ITRAT      'IMPOCOMINT'

ITRAT         EVALUATE ISPC0140-COMP-TIPO-DATO-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT           WHEN 'S'
ITRAT           WHEN 'D'
ITRAT             MOVE ISPC0140-COMP-VALORE-STR-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-IMP-COMMIS-INT(IX-AREA-PAGINA)
ITRAT           WHEN 'N'
ITRAT           WHEN 'I'
ITRAT             MOVE ISPC0140-COMP-VALORE-IMP-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-IMP-COMMIS-INT(IX-AREA-PAGINA)
ITRAT           WHEN 'P'
ITRAT           WHEN 'M'
ITRAT           WHEN 'A'
ITRAT             MOVE ISPC0140-COMP-VALORE-PERC-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-IMP-COMMIS-INT(IX-AREA-PAGINA)
ITRAT         END-EVALUATE

ITRAT      END-IF.

       EX-S14130.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
      *    VALORIZZARE GLI ALTRI DATI DELL'AREA DI PAGINA
      *----------------------------------------------------------------*
       S14140-CONTR-COMP-ALTRI.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'TIPIAS'
              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WK-APPO-IAS
                  MOVE WK-IAS-2
                    TO WPAG-TP-IAS(IX-AREA-PAGINA)
                       WK-TP-IAS(IX-AREA-PAGINA)

              END-EVALUATE
           END-IF.
           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
                                                        'DATAVALQUOTE'
BL7144        IF VERSAM-AGGIUNTIVO OR VERSAM-AGGIUNTIVO-REINV
49026            MOVE WSKD-DEE
49026              TO NEW-DT-VAL-QUOTE
BL7144           IF NEW-DT-VAL-QUOTE-RED NUMERIC
BL7144              MOVE NEW-DT-VAL-QUOTE-RED
BL7144                TO WPAG-DT-VAL-QUOTE(IX-AREA-PAGINA)
BL7144           END-IF

BL7144        ELSE

                 MOVE ISPC0140-COMP-VALORE-STR-T
                     (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WK-APPO-DT

                 IF WK-APPO-DT-RED NUMERIC
                    MOVE WK-APPO-DT-RED
                      TO WPAG-DT-VAL-QUOTE(IX-AREA-PAGINA)
                 END-IF

BL7144        END-IF

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'FRAZPROVV'

              MOVE ISPC0140-TRATTAMENTO-PROVV-T
                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WPAG-FL-FRAZ-PROVV(IX-AREA-PAGINA)

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PINVEST'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-MOVI(IX-AREA-PAGINA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-MOVI(IX-AREA-PAGINA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-MOVI(IX-AREA-PAGINA)
              END-EVALUATE
           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
              (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'ANMESIPRO'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WK-DIFF-PROR
                  MOVE WK-DIFF-AA
                    TO WPAG-AA-DIFF-PROR(IX-AREA-PAGINA)
                  MOVE WK-DIFF-MM
                    TO WPAG-MM-DIFF-PROR(IX-AREA-PAGINA)
              END-EVALUATE
           END-IF.

       EX-S14140.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
      *    VALORIZZARE GLI ALTRI DATI DELL'AREA DI PAGINA
      *----------------------------------------------------------------*
       S14145-CONTR-COMP-PAR-MOVIM.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'DPROXBONFED'

              MOVE ISPC0140-COMP-VALORE-STR-T
                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WK-APPO-DT

              IF WK-APPO-DT-RED NUMERIC
                 MOVE WK-APPO-DT-RED
                   TO WPAG-DT-PROS-BNS-FED(IX-AREA-PAGINA)
              END-IF

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'DPROXBONRIC'

              MOVE ISPC0140-COMP-VALORE-STR-T
                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WK-APPO-DT

              IF WK-APPO-DT-RED NUMERIC
                 MOVE WK-APPO-DT-RED
                   TO WPAG-DT-PROS-BNS-RIC(IX-AREA-PAGINA)
              END-IF

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
            = 'DPROXCED'

              MOVE ISPC0140-COMP-VALORE-STR-T
                  (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WK-APPO-DT

              IF WK-APPO-DT-RED NUMERIC
                 MOVE WK-APPO-DT-RED
                   TO WPAG-DT-PROS-CEDOLA(IX-AREA-PAGINA)
              END-IF

           END-IF.

       EX-S14145.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER IL CONTROLLO DEL CODICE COMPONENTE PER
      *    VALORIZZARE I DATI SOPRAPPREMI DI GARANZIA DELL'AREA DI PAG.
      *----------------------------------------------------------------*
       S14150-CONTR-COMP-SOPR-GAR.

FORZA *--> IN ATTESA DELLA VERSIONE DEFINITIVA DI PRODOTTO
      *    FORZO LA PERCENTUALE PASSATAMI DAL F.E. NELLA DCL
           SET WK-GRZ-NON-TROVATA         TO TRUE
           PERFORM VARYING IX-APPO-GRZ FROM 1 BY 1
             UNTIL IX-APPO-GRZ > WGRZ-ELE-GARANZIA-MAX
                OR WK-GRZ-TROVATA
             IF WGRZ-COD-TARI(IX-APPO-GRZ)
                                    = WPAG-COD-GARANZIA(IX-AREA-PAGINA)
                MOVE WGRZ-ID-GAR(IX-APPO-GRZ) TO WK-ID-GAR
                SET WK-GRZ-TROVATA            TO TRUE
             END-IF
           END-PERFORM.
           IF WK-GRZ-NON-TROVATA
              MOVE ZEROES TO WK-ID-GAR
           END-IF.

      *--> ATTUALMENTE PRODOTTO PER I SOVRAPPREMI GESTISCE SOLO GLI
      *--> IMPORTI
      *--> A BREVE SARA' RILASCIATA UNA NUOVA GESTIONE CHE PREVEDE
      *--> L'UTILIZZO DELLA PERCENTUALE DI SOVRAMMORTALITA'

           IF ISPC0140-CODICE-COMPONENTE-T
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPSAN'

              ADD 1                       TO IX-TAB-SPG
              SET WK-SOPSAN               TO TRUE
              MOVE WK-COD-SOVRAP
                TO WPAG-COD-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  IF ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                     IS NUMERIC
                     MOVE ISPC0140-COMP-VALORE-IMP-T
                       (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                       TO WPAG-IMP-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
                  END-IF
              END-EVALUATE
FORZA *--> IN ATTESA DELLA VERSIONE DEFINITIVA DI PRODOTTO
      *    FORZO LA PERCENTUALE PASSATAMI DAL F.E. NELLA DCL
              SET WK-SPG-NON-TROVATA           TO TRUE
              PERFORM VARYING IX-APPO-SPG FROM 1 BY 1
                UNTIL IX-APPO-SPG > WK-SPG-MAX-A
                   OR WK-SPG-TROVATA
                IF WSPG-COD-SOPR(IX-APPO-SPG) = WK-COD-SOVRAP
                AND WSPG-ID-GAR(IX-APPO-SPG) = WK-ID-GAR
                AND WSPG-VAL-PC-NULL(IX-APPO-SPG) NOT = HIGH-VALUES
                   MOVE WSPG-VAL-PC-NULL(IX-APPO-SPG)
                    TO WPAG-PERC-SOVRAP-NULL(IX-AREA-PAGINA, IX-TAB-SPG)
                  SET WK-SPG-TROVATA           TO TRUE
                END-IF
              END-PERFORM
           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPPRO'

              ADD 1         TO IX-TAB-SPG

              SET WK-SOPPRO               TO TRUE
              MOVE WK-COD-SOVRAP
                TO WPAG-COD-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  IF ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                     IS NUMERIC
                     MOVE ISPC0140-COMP-VALORE-IMP-T
                       (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                       TO WPAG-IMP-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
                  END-IF
              END-EVALUATE

FORZA *--> IN ATTESA DELLA VERSIONE DEFINITIVA DI PRODOTTO
      *    FORZO LA PERCENTUALE PASSATAMI DAL F.E. NELLA DCL
              SET WK-SPG-NON-TROVATA           TO TRUE
              PERFORM VARYING IX-APPO-SPG FROM 1 BY 1
                UNTIL IX-APPO-SPG > WK-SPG-MAX-A
                   OR WK-SPG-TROVATA
                IF WSPG-COD-SOPR(IX-APPO-SPG) = WK-COD-SOVRAP
                AND WSPG-ID-GAR(IX-APPO-SPG) = WK-ID-GAR
                AND WSPG-VAL-PC-NULL(IX-APPO-SPG) NOT = HIGH-VALUES
                   MOVE WSPG-VAL-PC-NULL(IX-APPO-SPG)
                    TO WPAG-PERC-SOVRAP-NULL(IX-AREA-PAGINA, IX-TAB-SPG)
                  SET WK-SPG-TROVATA           TO TRUE
                END-IF
              END-PERFORM
           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPSPO'

              ADD 1         TO IX-TAB-SPG

              SET WK-SOPSPO               TO TRUE
              MOVE WK-COD-SOVRAP
                TO WPAG-COD-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  IF ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                     IS NUMERIC
                     MOVE ISPC0140-COMP-VALORE-IMP-T
                       (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                       TO WPAG-IMP-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
                  END-IF
              END-EVALUATE

FORZA *--> IN ATTESA DELLA VERSIONE DEFINITIVA DI PRODOTTO
      *    FORZO LA PERCENTUALE PASSATAMI DAL F.E. NELLA DCL
              SET WK-SPG-NON-TROVATA           TO TRUE
              PERFORM VARYING IX-APPO-SPG FROM 1 BY 1
                UNTIL IX-APPO-SPG > WK-SPG-MAX-A
                   OR WK-SPG-TROVATA
                IF WSPG-COD-SOPR(IX-APPO-SPG) = WK-COD-SOVRAP
                AND WSPG-ID-GAR(IX-APPO-SPG) = WK-ID-GAR
                AND WSPG-VAL-PC-NULL(IX-APPO-SPG) NOT = HIGH-VALUES
                   MOVE WSPG-VAL-PC-NULL(IX-APPO-SPG)
                    TO WPAG-PERC-SOVRAP-NULL(IX-AREA-PAGINA, IX-TAB-SPG)
                  SET WK-SPG-TROVATA           TO TRUE
                END-IF
              END-PERFORM
           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPTEC'

              ADD 1         TO IX-TAB-SPG

              SET WK-SOPTEC               TO TRUE
              MOVE WK-COD-SOVRAP
                TO WPAG-COD-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  IF ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                     IS NUMERIC
                     MOVE ISPC0140-COMP-VALORE-IMP-T
                       (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                       TO WPAG-IMP-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
                  END-IF
              END-EVALUATE

FORZA *--> IN ATTESA DELLA VERSIONE DEFINITIVA DI PRODOTTO
      *    FORZO LA PERCENTUALE PASSATAMI DAL F.E. NELLA DCL
              SET WK-SPG-NON-TROVATA           TO TRUE
              PERFORM VARYING IX-APPO-SPG FROM 1 BY 1
                UNTIL IX-APPO-SPG > WK-SPG-MAX-A
                   OR WK-SPG-TROVATA
                IF WSPG-COD-SOPR(IX-APPO-SPG) = WK-COD-SOVRAP
                AND WSPG-ID-GAR(IX-APPO-SPG) = WK-ID-GAR
                AND WSPG-VAL-PC-NULL(IX-APPO-SPG) NOT = HIGH-VALUES
                   MOVE WSPG-VAL-PC-NULL(IX-APPO-SPG)
                    TO WPAG-PERC-SOVRAP-NULL(IX-AREA-PAGINA, IX-TAB-SPG)
                  SET WK-SPG-TROVATA           TO TRUE
                END-IF
              END-PERFORM
           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPALT'

              ADD 1         TO IX-TAB-SPG

              SET WK-SOPALT               TO TRUE
              MOVE WK-COD-SOVRAP
                TO WPAG-COD-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  IF ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                     IS NUMERIC
                     MOVE ISPC0140-COMP-VALORE-IMP-T
                       (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                       TO WPAG-IMP-SOVRAP(IX-AREA-PAGINA, IX-TAB-SPG)
                  END-IF
              END-EVALUATE
FORZA *--> IN ATTESA DELLA VERSIONE DEFINITIVA DI PRODOTTO
      *    FORZO LA PERCENTUALE PASSATAMI DAL F.E. NELLA DCL
              SET WK-SPG-NON-TROVATA           TO TRUE
              PERFORM VARYING IX-APPO-SPG FROM 1 BY 1
                UNTIL IX-APPO-SPG > WK-SPG-MAX-A
                   OR WK-SPG-TROVATA
                IF WSPG-COD-SOPR(IX-APPO-SPG) = WK-COD-SOVRAP
                AND WSPG-ID-GAR(IX-APPO-SPG) = WK-ID-GAR
                AND WSPG-VAL-PC-NULL(IX-APPO-SPG) NOT = HIGH-VALUES
                   MOVE WSPG-VAL-PC-NULL(IX-APPO-SPG)
                    TO WPAG-PERC-SOVRAP-NULL(IX-AREA-PAGINA, IX-TAB-SPG)
                  SET WK-SPG-TROVATA           TO TRUE
                END-IF
              END-PERFORM
           END-IF.

           MOVE IX-TAB-SPG   TO WPAG-ELE-SOPR-GAR-MAX(IX-AREA-PAGINA).

       EX-S14150.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE DEI DATI TITOLO DI RATA DELL'AREA DI PAGINA
      *    PER PREMIO RATA
      *----------------------------------------------------------------*
       S14200-PREMIO-RATA.

           MOVE 1           TO WPAG-ELE-TIT-RATA-MAX(IX-AREA-PAGINA)
                               IX-TAB-RATA.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PNETTO'


              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                  TO WPAG-RATA-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                  TO WPAG-RATA-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                  TO WPAG-RATA-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'INTFRAZ'


              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'INTRET'


              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-RATA-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-RATA-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-RATA-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIREMI'

      *       IF WK-SI-PRIMO-RATA-DIR
              IF WPAG-RATA-DIRITTI-NULL(IX-AREA-PAGINA, IX-TAB-RATA)
                                                            = HIGH-VALUE
                 MOVE ZERO
                   TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)
      *          SET WK-NO-PRIMO-RATA-DIR    TO TRUE
              END-IF

              ADD ISPC0140-COMP-VALORE-IMP-T
                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIRQUI'

      *       IF WK-SI-PRIMO-RATA-DIR
              IF WPAG-RATA-DIRITTI-NULL(IX-AREA-PAGINA, IX-TAB-RATA)
                                                            = HIGH-VALUE
                 MOVE ZERO
                   TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)
      *          SET WK-NO-PRIMO-RATA-DIR    TO TRUE
              END-IF

              ADD ISPC0140-COMP-VALORE-IMP-T
                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIRVIS'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                 MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                 TO WPAG-RATA-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                 MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                 TO WPAG-RATA-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                 MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                 TO WPAG-RATA-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIRAGG'

              IF WPAG-RATA-DIRITTI-NULL(IX-AREA-PAGINA, IX-TAB-RATA)
               = HIGH-VALUE
                 MOVE ZERO
                   TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)
              END-IF

              ADD ISPC0140-COMP-VALORE-IMP-T
                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               TO WPAG-RATA-DIRITTI(IX-AREA-PAGINA, IX-TAB-RATA)

           END-IF.


           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPPRE'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-TASSE(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-TASSE(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-TASSE(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPSAN'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPPRO'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-RATA-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-RATA-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-RATA-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPSPO'


              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPTEC'


              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPALT'


              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PRETOT'


              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.



           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PPUROI'


              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                       (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-PREMIO-PURO-IAS
                       (IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-PREMIO-PURO-IAS
                      (IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-PREMIO-PURO-IAS
                      (IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PCASOM'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-RATA-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-RATA-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-RATA-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'CARACQ'


      *       IF WK-SI-PRIMO-RATA-CAR
              IF WPAG-IMP-CAR-ACQ-TDR-NULL(IX-AREA-PAGINA, IX-TAB-RATA)
               = HIGH-VALUE
                 MOVE ZERO
                   TO WPAG-IMP-CAR-ACQ-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
      *          SET WK-NO-PRIMO-RATA-CAR    TO TRUE
              END-IF

              ADD ISPC0140-COMP-VALORE-IMP-T
                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               TO WPAG-IMP-CAR-ACQ-TDR(IX-AREA-PAGINA, IX-TAB-RATA)

           END-IF.

ITRAT      IF ISPC0140-CODICE-COMPONENTE-T
ITRAT        (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ACQEXP'


ITRAT *       IF WK-SI-PRIMO-RATA-CAR
ITRAT         IF WPAG-RATA-IMP-ACQ-EXP-NULL(IX-AREA-PAGINA, IX-TAB-RATA)
ITRAT          = HIGH-VALUE
ITRAT            MOVE ZERO
ITRAT              TO WPAG-RATA-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-RATA)
ITRAT *          SET WK-NO-PRIMO-RATA-CAR    TO TRUE
ITRAT         END-IF

ITRAT         ADD ISPC0140-COMP-VALORE-IMP-T
ITRAT            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT          TO WPAG-RATA-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-RATA)

ITRAT      END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'CARINC'


      *       IF WK-SI-PRIMO-RATA-CAR
              IF WPAG-IMP-CAR-INC-TDR-NULL(IX-AREA-PAGINA, IX-TAB-RATA)
               = HIGH-VALUE
                 MOVE ZERO
                   TO WPAG-IMP-CAR-INC-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
      *          SET WK-NO-PRIMO-RATA-CAR    TO TRUE
              END-IF

              ADD ISPC0140-COMP-VALORE-IMP-T
                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               TO WPAG-IMP-CAR-INC-TDR(IX-AREA-PAGINA, IX-TAB-RATA)

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'CARGES'


      *       IF WK-SI-PRIMO-RATA-CAR
              IF WPAG-IMP-CAR-GEST-TDR-NULL(IX-AREA-PAGINA, IX-TAB-RATA)
               = HIGH-VALUE
                 MOVE ZERO
                   TO WPAG-IMP-CAR-GEST-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
      *          SET WK-NO-PRIMO-RATA-CAR    TO TRUE
              END-IF

              ADD ISPC0140-COMP-VALORE-IMP-T
                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               TO WPAG-IMP-CAR-GEST-TDR(IX-AREA-PAGINA, IX-TAB-RATA)

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVACQ1A'


              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-RATA-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-RATA-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-RATA-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVACQ2A'


              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-RATA-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-RATA-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-RATA-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVRIC'


              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-RICOR(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-RICOR(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-RICOR(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVINC'


              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-INCAS(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-INCAS(IX-AREA-PAGINA, IX-TAB-RATA)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-RATA-INCAS(IX-AREA-PAGINA, IX-TAB-RATA)
              END-EVALUATE

           END-IF.

      *--> IMPORTO AZIENDA (FONTI CONTRIBUTIVE)
           IF ISPC0140-CODICE-COMPONENTE-T
            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
            'IMPAZIENDA'

             EVALUATE ISPC0140-COMP-TIPO-DATO-T
                     (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               WHEN 'N'
               WHEN 'I'
                 MOVE ISPC0140-COMP-VALORE-IMP-T
                     (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-IMP-AZ-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
             END-EVALUATE

           END-IF.

      *--> IMPORTO ADERENTE (FONTI CONTRIBUTIVE)
           IF ISPC0140-CODICE-COMPONENTE-T
            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
            'IMPADERENTE'

             EVALUATE ISPC0140-COMP-TIPO-DATO-T
                     (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               WHEN 'N'
               WHEN 'I'
                 MOVE ISPC0140-COMP-VALORE-IMP-T
                     (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-IMP-ADER-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
             END-EVALUATE

           END-IF.

      *--> IMPORTO TFR (FONTI CONTRIBUTIVE)
           IF ISPC0140-CODICE-COMPONENTE-T
            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPTFR'

             EVALUATE ISPC0140-COMP-TIPO-DATO-T
                     (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               WHEN 'N'
               WHEN 'I'
                 MOVE ISPC0140-COMP-VALORE-IMP-T
                     (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-IMP-TFR-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
             END-EVALUATE

           END-IF.

      *--> IMPORTO VOLONTARIO (FONTI CONTRIBUTIVE)
           IF ISPC0140-CODICE-COMPONENTE-T
            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
            'IMPVOLONT'

             EVALUATE ISPC0140-COMP-TIPO-DATO-T
                     (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               WHEN 'N'
               WHEN 'I'
                 MOVE ISPC0140-COMP-VALORE-IMP-T
                     (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-IMP-VOLO-TDR(IX-AREA-PAGINA, IX-TAB-RATA)
             END-EVALUATE

           END-IF.

      *--> IMPORTO INTERESSI DI RIATTIVAZIONE
           IF ISPC0140-CODICE-COMPONENTE-T
            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
            'INTRIA'

             EVALUATE ISPC0140-COMP-TIPO-DATO-T
                     (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               WHEN 'N'
               WHEN 'I'
                 MOVE ISPC0140-COMP-VALORE-IMP-T
                     (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-IMP-INT-RIATT(IX-AREA-PAGINA, IX-TAB-RATA)
             END-EVALUATE

           END-IF.

ITRAT      IF ISPC0140-CODICE-COMPONENTE-T
ITRAT        (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
ITRAT        'REMUNASS'


ITRAT         EVALUATE ISPC0140-COMP-TIPO-DATO-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT           WHEN 'S'
ITRAT           WHEN 'D'
ITRAT             MOVE ISPC0140-COMP-VALORE-STR-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-RATA-IMP-REN-ASS
ITRAT                 (IX-AREA-PAGINA, IX-TAB-RATA)
ITRAT           WHEN 'N'
ITRAT           WHEN 'I'
ITRAT             MOVE ISPC0140-COMP-VALORE-IMP-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-RATA-IMP-REN-ASS
ITRAT                 (IX-AREA-PAGINA, IX-TAB-RATA)
ITRAT           WHEN 'P'
ITRAT           WHEN 'M'
ITRAT           WHEN 'A'
ITRAT             MOVE ISPC0140-COMP-VALORE-PERC-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-RATA-IMP-REN-ASS
ITRAT                 (IX-AREA-PAGINA, IX-TAB-RATA)
ITRAT         END-EVALUATE

ITRAT      END-IF.

ITRAT      IF ISPC0140-CODICE-COMPONENTE-T
ITRAT        (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
ITRAT        'COMMINTER'


ITRAT         EVALUATE ISPC0140-COMP-TIPO-DATO-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT           WHEN 'S'
ITRAT           WHEN 'D'
ITRAT             MOVE ISPC0140-COMP-VALORE-STR-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-RATA-IMP-COMMIS-INT
ITRAT                 (IX-AREA-PAGINA, IX-TAB-RATA)
ITRAT           WHEN 'N'
ITRAT           WHEN 'I'
ITRAT             MOVE ISPC0140-COMP-VALORE-IMP-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-RATA-IMP-COMMIS-INT
ITRAT                 (IX-AREA-PAGINA, IX-TAB-RATA)
ITRAT           WHEN 'P'
ITRAT           WHEN 'M'
ITRAT           WHEN 'A'
ITRAT             MOVE ISPC0140-COMP-VALORE-PERC-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-RATA-IMP-COMMIS-INT
ITRAT                 (IX-AREA-PAGINA, IX-TAB-RATA)
ITRAT         END-EVALUATE

ITRAT      END-IF.

11140      IF ISPC0140-CODICE-COMPONENTE-T
11140        (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
11140                                                     'CTRANTRACKET'

11140         EVALUATE ISPC0140-COMP-TIPO-DATO-T
11140                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
11140           WHEN 'S'
11140           WHEN 'D'
11140             MOVE ISPC0140-COMP-VALORE-STR-T
11140                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
11140               TO WPAG-RATA-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
11140           WHEN 'N'
11140           WHEN 'I'
11140             MOVE ISPC0140-COMP-VALORE-IMP-T
11140                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
11140               TO WPAG-RATA-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
11140           WHEN 'P'
11140           WHEN 'M'
11140           WHEN 'A'
11140             MOVE ISPC0140-COMP-VALORE-PERC-T
11140                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
11140               TO WPAG-RATA-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
11140         END-EVALUATE

11140      END-IF.

       EX-S14200.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE DEI DATI DETTAGLIO TITOLO CONTABILE
      *    DELL'AREA DI PAGINA PER PREMIO FIRMA
      *----------------------------------------------------------------*
       S14300-PREMIO-FIRMA.

           MOVE 1           TO WPAG-ELE-TIT-CONT-MAX(IX-AREA-PAGINA)
                               IX-TAB-TIT.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PNETTO'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-CONT-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-CONT-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                   TO WPAG-CONT-PREMIO-NETTO(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'INTFRAZ'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-INT-FRAZ(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'INTRET'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-INT-RETRODT(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIREMI'

      *       IF WK-SI-PRIMO-CONT-DIR
              IF WPAG-CONT-DIRITTI-NULL(IX-AREA-PAGINA, IX-TAB-TIT)
                                                            = HIGH-VALUE
                 MOVE ZERO
                   TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)
      *          SET WK-NO-PRIMO-CONT-DIR    TO TRUE
              END-IF

              ADD ISPC0140-COMP-VALORE-IMP-T
                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIRQUI'

      *       IF WK-SI-PRIMO-CONT-DIR
              IF WPAG-CONT-DIRITTI-NULL(IX-AREA-PAGINA, IX-TAB-TIT)
                                                            = HIGH-VALUE
                 MOVE 0
                   TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)
      *          SET WK-NO-PRIMO-CONT-DIR    TO TRUE
              END-IF

              ADD ISPC0140-COMP-VALORE-IMP-T
                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIRVIS'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                  TO WPAG-CONT-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                  TO WPAG-CONT-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                  TO WPAG-CONT-SPESE-MEDICHE(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'DIRAGG'

              IF WPAG-CONT-DIRITTI-NULL(IX-AREA-PAGINA, IX-TAB-TIT)
               = HIGH-VALUE
                 MOVE ZERO
                   TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)
              END-IF

              ADD ISPC0140-COMP-VALORE-IMP-T
                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               TO WPAG-CONT-DIRITTI(IX-AREA-PAGINA, IX-TAB-TIT)

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPPRE'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-TASSE(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-TASSE(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-TASSE(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPSAN'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-SOPR-SANIT(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPPRO'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-SOPR-PROFES(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPSPO'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-SOPR-SPORT(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPTEC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-SOPR-TECN(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'SOPALT'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-ALTRI-SOPR(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PRETOT'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-PREMIO-TOT(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PPUROI'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WPAG-CONT-PREMIO-PURO-IAS(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WPAG-CONT-PREMIO-PURO-IAS(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                TO WPAG-CONT-PREMIO-PURO-IAS(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PCASOM'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-PREMIO-RISC(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'CARACQ'

      *       IF WK-SI-PRIMO-CONT-CAR
              IF WPAG-IMP-CAR-ACQ-TIT-NULL(IX-AREA-PAGINA, IX-TAB-TIT)
               = HIGH-VALUE
                 MOVE ZERO
                   TO WPAG-IMP-CAR-ACQ-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
      *          SET WK-NO-PRIMO-CONT-CAR    TO TRUE
              END-IF

              ADD ISPC0140-COMP-VALORE-IMP-T
                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               TO WPAG-IMP-CAR-ACQ-TIT(IX-AREA-PAGINA, IX-TAB-TIT)

           END-IF.

ITRAT      IF ISPC0140-CODICE-COMPONENTE-T
ITRAT        (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'ACQEXP'

ITRAT *       IF WK-SI-PRIMO-CONT-CAR
ITRAT         IF WPAG-CONT-IMP-ACQ-EXP-NULL(IX-AREA-PAGINA, IX-TAB-TIT)
ITRAT          = HIGH-VALUE
ITRAT            MOVE ZERO
ITRAT              TO WPAG-CONT-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-TIT)
ITRAT *          SET WK-NO-PRIMO-CONT-CAR    TO TRUE
ITRAT         END-IF

ITRAT         ADD ISPC0140-COMP-VALORE-IMP-T
ITRAT            (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT          TO WPAG-CONT-IMP-ACQ-EXP(IX-AREA-PAGINA, IX-TAB-TIT)

ITRAT      END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'CARINC'

      *       IF WK-SI-PRIMO-CONT-CAR
              IF WPAG-IMP-CAR-INC-TIT-NULL(IX-AREA-PAGINA, IX-TAB-TIT)
               = HIGH-VALUE
                 MOVE ZERO
                   TO WPAG-IMP-CAR-INC-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
      *          SET WK-NO-PRIMO-CONT-CAR    TO TRUE
              END-IF

              ADD ISPC0140-COMP-VALORE-IMP-T
                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               TO WPAG-IMP-CAR-INC-TIT(IX-AREA-PAGINA, IX-TAB-TIT)

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'CARGES'

      *       IF WK-SI-PRIMO-CONT-CAR
              IF WPAG-IMP-CAR-GEST-TIT-NULL(IX-AREA-PAGINA, IX-TAB-TIT)
               = HIGH-VALUE
                 MOVE ZERO
                   TO WPAG-IMP-CAR-GEST-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
      *          SET WK-NO-PRIMO-CONT-CAR    TO TRUE
              END-IF

              ADD ISPC0140-COMP-VALORE-IMP-T
                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
               TO WPAG-IMP-CAR-GEST-TIT(IX-AREA-PAGINA, IX-TAB-TIT)

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVACQ1A'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-ACQ-1O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
           (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVACQ2A'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-ACQ-2O-ANNO(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVRIC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-RICOR(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-RICOR(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-RICOR(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'PROVINC'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'S'
                WHEN 'D'
                  MOVE ISPC0140-COMP-VALORE-STR-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-INCAS(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-INCAS(IX-AREA-PAGINA, IX-TAB-TIT)
                WHEN 'P'
                WHEN 'M'
                WHEN 'A'
                  MOVE ISPC0140-COMP-VALORE-PERC-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-CONT-INCAS(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
             'IMPAZIENDA'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-AZ-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
             'IMPADERENTE'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-ADER-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) = 'IMPTFR'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-TFR-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

           IF ISPC0140-CODICE-COMPONENTE-T
             (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
             'IMPVOLONT'

              EVALUATE ISPC0140-COMP-TIPO-DATO-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                WHEN 'N'
                WHEN 'I'
                  MOVE ISPC0140-COMP-VALORE-IMP-T
                      (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
                    TO WPAG-IMP-VOLO-TIT(IX-AREA-PAGINA, IX-TAB-TIT)
              END-EVALUATE

           END-IF.

ITRAT      IF ISPC0140-CODICE-COMPONENTE-T
ITRAT        (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
ITRAT        'REMUNASS'

ITRAT         EVALUATE ISPC0140-COMP-TIPO-DATO-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT           WHEN 'S'
ITRAT           WHEN 'D'
ITRAT             MOVE ISPC0140-COMP-VALORE-STR-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-CONT-IMP-REN-ASS(IX-AREA-PAGINA, IX-TAB-TIT)
ITRAT           WHEN 'N'
ITRAT           WHEN 'I'
ITRAT             MOVE ISPC0140-COMP-VALORE-IMP-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-CONT-IMP-REN-ASS(IX-AREA-PAGINA, IX-TAB-TIT)
ITRAT           WHEN 'P'
ITRAT           WHEN 'M'
ITRAT           WHEN 'A'
ITRAT             MOVE ISPC0140-COMP-VALORE-PERC-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-CONT-IMP-REN-ASS(IX-AREA-PAGINA, IX-TAB-TIT)
ITRAT         END-EVALUATE

ITRAT      END-IF.

ITRAT      IF ISPC0140-CODICE-COMPONENTE-T
ITRAT        (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
ITRAT        'COMMINTER'

ITRAT         EVALUATE ISPC0140-COMP-TIPO-DATO-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT           WHEN 'S'
ITRAT           WHEN 'D'
ITRAT             MOVE ISPC0140-COMP-VALORE-STR-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-CONT-IMP-COMMIS-INT
ITRAT                  (IX-AREA-PAGINA, IX-TAB-TIT)
ITRAT           WHEN 'N'
ITRAT           WHEN 'I'
ITRAT             MOVE ISPC0140-COMP-VALORE-IMP-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-CONT-IMP-COMMIS-INT
ITRAT                  (IX-AREA-PAGINA, IX-TAB-TIT)
ITRAT           WHEN 'P'
ITRAT           WHEN 'M'
ITRAT           WHEN 'A'
ITRAT             MOVE ISPC0140-COMP-VALORE-PERC-T
ITRAT                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
ITRAT               TO WPAG-CONT-IMP-COMMIS-INT
ITRAT                  (IX-AREA-PAGINA, IX-TAB-TIT)
ITRAT         END-EVALUATE

ITRAT      END-IF.

11140      IF ISPC0140-CODICE-COMPONENTE-T
11140        (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE) =
11140                                                     'CTRANTRACKET'

11140         EVALUATE ISPC0140-COMP-TIPO-DATO-T
11140                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
11140           WHEN 'S'
11140           WHEN 'D'
11140             MOVE ISPC0140-COMP-VALORE-STR-T
11140                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
11140               TO WPAG-CONT-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
11140           WHEN 'N'
11140           WHEN 'I'
11140             MOVE ISPC0140-COMP-VALORE-IMP-T
11140                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
11140               TO WPAG-CONT-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
11140           WHEN 'P'
11140           WHEN 'M'
11140           WHEN 'A'
11140             MOVE ISPC0140-COMP-VALORE-PERC-T
11140                 (IX-AREA-ISPC0140, IX-TP-PREMIO, IX-COMPONENTE)
11140               TO WPAG-CONT-ANTIRACKET(IX-AREA-PAGINA, IX-TAB-RATA)
11140         END-EVALUATE

11140      END-IF.

       EX-S14300.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZAZIONE DELLA AREA PAGINA IAS
      *----------------------------------------------------------------*
       S14100-AREA-PAGINA-IAS-GAR.

           IF WGRZ-COD-TARI(IX-PAG-IAS) = WK-COD-GAR(IX-PAG-IAS)
              ADD  1                      TO WPAG-ELE-MAX-IAS
              MOVE WGRZ-ID-GAR(IX-PAG-IAS)
                TO WPAG-ID-OGG(WPAG-ELE-MAX-IAS)
              MOVE 'GA'                 TO WPAG-TP-OGG(WPAG-ELE-MAX-IAS)
              MOVE WGRZ-TP-IAS(IX-PAG-IAS)
                TO WPAG-TP-IAS-OLD(WPAG-ELE-MAX-IAS)
              MOVE WK-TP-IAS(IX-PAG-IAS)
                TO WPAG-TP-IAS-NEW(WPAG-ELE-MAX-IAS)

              IF WPAG-TP-IAS-OLD(WPAG-ELE-MAX-IAS)  =
                 WPAG-TP-IAS-NEW(WPAG-ELE-MAX-IAS)
                 SET WPAG-NO-MOD-IAS(WPAG-ELE-MAX-IAS)         TO  TRUE
              ELSE
                 SET WPAG-SI-MOD-IAS(WPAG-ELE-MAX-IAS)         TO  TRUE
              END-IF
           END-IF.

       S14100-AREA-PAGINA-IAS-GAR-EX.
           EXIT.


      *----------------------------------------------------------------*
      *    VALORIZZA AREA DI PAGINA TAB ASSET
      *----------------------------------------------------------------*
       S14500-GESTIONE-FONDI.
      *
           MOVE ZERO TO IX-AREA-PAGINA.
      *
           IF WALL-ELE-ASSET-ALL-MAX EQUAL ZERO

              PERFORM S14501-GESTIONE-FONDI-TEMP THRU EX-S14501

           ELSE

              PERFORM S14502-GESTIONE-FONDI      THRU EX-S14502

           END-IF.
      *
       EX-S14500.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA DI PAGINA TAB ASSET
      *----------------------------------------------------------------*
       S14501-GESTIONE-FONDI-TEMP.
      *
           PERFORM S14503-IMPOSTA-TEMP      THRU EX-S14503.

           PERFORM UNTIL NOT IDSV0301-ESITO-OK
                      OR NOT IDSV0301-SUCCESSFUL-SQL

              PERFORM GESTIONE-TEMP-TABLE
                 THRU GESTIONE-TEMP-TABLE-EX

              IF IDSV0301-ESITO-OK

                 IF IDSV0301-SUCCESSFUL-SQL

                    PERFORM GESTIONE-ELE-MAX-TEMP
                       THRU GESTIONE-ELE-MAX-TEMP-EX

                    MOVE IDSV0303-ELE-MAX-TOT
                      TO WALL-ELE-ASSET-ALL-MAX

                    PERFORM S14502-GESTIONE-FONDI
                       THRU EX-S14502

                    INITIALIZE WALL-AREA-ASSET

                 END-IF

              END-IF

           END-PERFORM.

      *
       EX-S14501.
           EXIT.
      *----------------------------------------------------------------*
      *    IMPOSTA AREA PER CALL TEMP
      *----------------------------------------------------------------*
       S14503-IMPOSTA-TEMP.
      *
           INITIALIZE IDSV0301
                      IDSV0303.
           MOVE 'ALL'                   TO IDSV0301-ALIAS-STR-DATO.
           MOVE LENGTH OF WALL-TABELLA
                                        TO IDSV0301-BUFFER-DATA-LEN.
           SET IDSV0301-READ            TO TRUE.
           SET IDSV0301-ESITO-OK        TO TRUE.
           SET IDSV0301-SUCCESSFUL-SQL  TO TRUE.
           SET IDSV0301-ADDRESS         TO ADDRESS OF WALL-TABELLA.
      *
       EX-S14503.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA DI PAGINA TAB ASSET
      *----------------------------------------------------------------*
       S14502-GESTIONE-FONDI.

      *--  A partire dalla data decorrenza della garanzia vengono
      *--  estratti dal pft i fondi di tipo Unit/Index
      *--  Per la venidta la data decorrenza della Garanzia e per il
      *--  versamento aggiuntivo la data decorrenza della tranche

           MOVE WGRZ-DT-DECOR(1) TO WK-DT-DECORRENZA

           PERFORM VARYING IX-ALL FROM 1 BY 1
                     UNTIL IX-ALL > WALL-ELE-ASSET-ALL-MAX

             IF NOT WALL-ST-DEL(IX-ALL) AND NOT WALL-ST-CON(IX-ALL)
                IF   WK-DT-DECORRENZA  >=  WALL-DT-INI-VLDT(IX-ALL)
                AND (WALL-DT-END-VLDT-NULL(IX-ALL) = HIGH-VALUE OR
                     WK-DT-DECORRENZA  < WALL-DT-END-VLDT(IX-ALL))


                    MOVE WALL-TP-FND(IX-ALL) TO WK-TP-FONDO

                    IF FND-UNIT-OICR OR FND-UNIT-ASSIC OR FND-INDEX
                       ADD 1 TO IX-AREA-PAGINA
                       MOVE WALL-COD-FND(IX-ALL)
                                  TO WPAG-COD-FONDO(IX-AREA-PAGINA)
                       MOVE WALL-TP-FND(IX-ALL)
                                  TO WPAG-TP-FND(IX-AREA-PAGINA)
                       MOVE WALL-PC-RIP-AST(IX-ALL)
                                  TO WPAG-PERC-FONDO(IX-AREA-PAGINA)
                       IF WALL-COD-TARI-NULL(IX-ALL) = HIGH-VALUE
                          MOVE HIGH-VALUE
                            TO WPAG-COD-TARI-NULL(IX-AREA-PAGINA)
                       ELSE
                          MOVE WALL-COD-TARI(IX-ALL)
                            TO WPAG-COD-TARI(IX-AREA-PAGINA)
                       END-IF
                       ADD 1      TO    WPAG-ELE-ASSET-MAX
                    END-IF
                END-IF
             END-IF

           END-PERFORM.

       EX-S14502.
           EXIT.

      *----------------------------------------------------------------*
      *    GESTIONE INCLUSIONE SOPRAPPREMI
      *----------------------------------------------------------------*
       S14600-GESTIONE-SOPRA.
      *RAFFAELE
      *Estrazione di tutte le tranche di garanzia (in vigore) associate
      *alle adesione
           INITIALIZE                       VTGA-AREA-TRANCHE.

           PERFORM S1660-FETCH-FIRST-TRANCHE
              THRU S1660-EX
           PERFORM S1680-FETCH-NEXT-TRANCHE
              THRU S1680-EX
             UNTIL FINE-ELEMENTI-TRANCHE
                OR IDSV0001-ESITO-KO.

      *Individuazione delle garanzie sulle quali sono stati inclusi
      *soprappremi e delle tranche di garanzia in vigore all'inclusione
      *soprappremio
           MOVE ZEROES                   TO IX-APPO-GRZ
                                            IX-APPO-SPG
                                            IX-APPO-TGA
                                            VS-GRZ-ELE-GARANZIA-MAX

           PERFORM VARYING IX-SPG FROM 1 BY 1
             UNTIL IX-SPG > VSPG-ELE-SOPR-GAR-MAX
                IF VSPG-ST-ADD(IX-SPG)
                   ADD 1                 TO IX-APPO-SPG
                   MOVE VSPG-TAB-SPG(IX-SPG)
                     TO VS-SPG-TAB-SPG(IX-APPO-SPG)
                   SET WK-GRZ-NON-TROVATA
                                         TO TRUE
                   PERFORM S14620-GESTIONE-GAR-SOPRA
                      THRU EX-S14620
                   VARYING IX-TAB-GRZ FROM 1 BY 1
                     UNTIL IX-TAB-GRZ > VGRZ-ELE-GARANZIA-MAX
                        OR WK-GRZ-TROVATA
                END-IF
           END-PERFORM.
      *
           MOVE IX-APPO-SPG              TO VS-SPG-ELE-SOPR-GAR-MAX.
           MOVE IX-APPO-GRZ              TO VS-GRZ-ELE-GARANZIA-MAX.
           MOVE IX-APPO-TGA              TO VTGA-ELE-TRAN-MAX.
      *
           INITIALIZE                       VGRZ-AREA-GARANZIA
                                            VSPG-AREA-SOPR-GAR.

           MOVE VS-GRZ-AREA-GARANZIA     TO VGRZ-AREA-GARANZIA.
           MOVE VS-SPG-AREA-SOPR-GAR     TO VSPG-AREA-SOPR-GAR.
      *
       EX-S14600.
           EXIT.
      *----------------------------------------------------------------*
      *    GESTIONE GARANZIE INCLUSIONE SOPRAPPREMI
      *----------------------------------------------------------------*
       S14620-GESTIONE-GAR-SOPRA.
      *
           IF VSPG-ID-GAR(IX-SPG) = VGRZ-ID-GAR(IX-TAB-GRZ)
      *       RICERCA GARANZIA SU CUI E' GESTITO IL SOPRAPPREMIO
              PERFORM S14640-RICERCA-GAR-SOPRA
                 THRU EX-S14640
              VARYING IX-RIC-GRZ FROM 1 BY 1
                UNTIL IX-RIC-GRZ > VS-GRZ-ELE-GARANZIA-MAX
                   OR WK-GRZ-TROVATA
              IF WK-GRZ-NON-TROVATA
                 ADD 1                   TO IX-APPO-GRZ
                 MOVE IX-APPO-GRZ        TO VS-GRZ-ELE-GARANZIA-MAX
                 MOVE VGRZ-TAB-GAR(IX-TAB-GRZ)
                   TO VS-GRZ-TAB-GAR(IX-APPO-GRZ)
                 SET WK-GRZ-TROVATA      TO TRUE
                 SET WK-TGA-NON-TROVATA  TO TRUE
      *          RICERCA DELLA TRANCGE DI GARANZIA IN VIGORE
      *          ALL'INCLUSIONE SOPRAPPREMI
                 PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
                   UNTIL IX-TAB-TGA > VS-TGA-ELE-TRAN-MAX
                      OR WK-TGA-TROVATA
                         IF VGRZ-ID-GAR(IX-TAB-GRZ) =
                            VS-TGA-ID-GAR(IX-TAB-TGA)   AND
                            VSPG-DT-INI-EFF(IX-SPG) >=
                            VS-TGA-DT-DECOR(IX-TAB-TGA) AND
                            VSPG-DT-INI-EFF(IX-SPG) <=
                            VS-TGA-DT-SCAD(IX-TAB-TGA)  AND
                            VS-TGA-TP-TRCH(IX-TAB-TGA)  = '1'
                            ADD 1        TO IX-APPO-TGA
                            MOVE VS-TGA-TAB-TRAN(IX-TAB-TGA)
                              TO VTGA-TAB-TRAN(IX-APPO-TGA)
                            SET WK-TGA-TROVATA
                                         TO TRUE
                         END-IF
                 END-PERFORM
              END-IF
           END-IF.
       EX-S14620.
           EXIT.
      *----------------------------------------------------------------*
      *    RICERCA GARANZIE INCLUSIONE SOPRAPPREMI
      *----------------------------------------------------------------*
       S14640-RICERCA-GAR-SOPRA.
      *
           IF VGRZ-ID-GAR(IX-TAB-GRZ) = VS-GRZ-ID-GAR(IX-RIC-GRZ)
              SET WK-GRZ-TROVATA         TO TRUE
           END-IF.
      *
       EX-S14640.
           EXIT.
      *----------------------------------------------------------------*
      *    Calcolo del numero di rate che vengono accorpate nel TITOLO
      *    di perfezionamento
      *----------------------------------------------------------------*
       S14700-GESTIONE-RATE.

           EVALUATE TRUE
      *--      Gestione Unica
               WHEN RECUPRATE-UNICA
                  COMPUTE WPAG-NUM-RATE-ACC =
                          WK-RATE-ANTIC + WK-RATE-RECUP + 1

      *--      Gestione Separata
               WHEN RECUPRATE-SEPARATE
                  COMPUTE WPAG-NUM-RATE-ACC = WK-RATE-ANTIC + 1

      *--      Gestione Batch
               WHEN RECUPRATE-BATCH
                  MOVE 1 TO WPAG-NUM-RATE-ACC

           END-EVALUATE.

       S14700-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1660-FETCH-FIRST-TRANCHE.

           INITIALIZE LDBI0731.
           INITIALIZE LDBO0731.

      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
           MOVE IDSV0001-DATA-EFFETTO    TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
           MOVE IDSV0001-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA
      *
           MOVE VGRZ-ID-POLI(1)          TO LDBI0731-ID-POLI
           MOVE VGRZ-ID-ADES(1)          TO LDBI0731-ID-ADES
           MOVE '02'                     TO LDBI0731-TP-CALL
           SET IN-VIGORE                 TO TRUE
           MOVE WS-TP-STAT-BUS           TO LDBI0731-TP-STAT-BUS
           MOVE SPACES                   TO LDBI0731-TRASFORMATA.
           MOVE 'LDBS0730'               TO IDSI0011-CODICE-STR-DATO.
      *
           MOVE LDBI0731
             TO IDSI0011-BUFFER-WHERE-COND.

           SET IDSI0011-FETCH-FIRST      TO TRUE.

           PERFORM S1690-CALL-LDBS0730   THRU S1690-EX.
           MOVE IX-TAB-TGA               TO VS-TGA-ELE-TRAN-MAX.

       S1660-EX.
           EXIT.

      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1680-FETCH-NEXT-TRANCHE.

           INITIALIZE LDBI0731
           INITIALIZE LDBO0731

      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
           MOVE IDSV0001-DATA-EFFETTO
             TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZEROES
             TO IDSI0011-DATA-FINE-EFFETTO
           MOVE IDSV0001-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA
      *
           MOVE VGRZ-ID-POLI(1)          TO LDBI0731-ID-POLI
           MOVE VGRZ-ID-ADES(1)          TO LDBI0731-ID-ADES
           MOVE '02'                     TO LDBI0731-TP-CALL
           SET IN-VIGORE                 TO TRUE
           MOVE WS-TP-STAT-BUS           TO LDBI0731-TP-STAT-BUS
           MOVE SPACES                   TO LDBI0731-TRASFORMATA
           MOVE 'LDBS0730'               TO IDSI0011-CODICE-STR-DATO
      *
           MOVE LDBI0731
             TO IDSI0011-BUFFER-WHERE-COND

           SET IDSI0011-FETCH-NEXT       TO TRUE.

           PERFORM S1690-CALL-LDBS0730   THRU S1690-EX.
           MOVE IX-TAB-TGA               TO VS-TGA-ELE-TRAN-MAX.

       S1680-EX.
           EXIT.
      ******************************************************************
      * CHIAMATA AL PROGRAMMA LDBS0730 CHE EFFETTUA
      * L'ESTRAZIONE DEI DATI DALLA TABELLA  STATO-OGGETTO-BUSINESS
      ******************************************************************
       S1690-CALL-LDBS0730.

           SET IDSI0011-TRATT-X-EFFETTO TO TRUE.
           SET IDSI0011-WHERE-CONDITION TO TRUE.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
      *-->        NON TROVATA
                       SET FINE-ELEMENTI-TRANCHE
                                        TO TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       SET ALTRI-ELEMENTI
                                        TO TRUE
                       MOVE IDSO0011-BUFFER-DATI
                                        TO LDBO0731
                       MOVE LDBO0731-TGA
                                        TO TRCH-DI-GAR
                       ADD 1            TO IX-TAB-TGA
                       PERFORM VALORIZZA-OUTPUT-TGA
                          THRU VALORIZZA-OUTPUT-TGA-EX
              END-EVALUATE
           ELSE
      *-->    GESTIONE ERRORE DISPATCHER
              MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1690-CALL-LDBS0730' TO IEAI9901-LABEL-ERR
              MOVE '005016'              TO IEAI9901-COD-ERRORE
              STRING 'LDBS0730'           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S1690-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    INIZIALIZZAZIONE TASTI PER DATI GENERALI
      *----------------------------------------------------------------*
       S15000-INIZIALIZZA-TASTI.

           MOVE 'A'                  TO  WCOM-TASTO-ANNULLA
                                         WCOM-TASTO-AVANTI
                                         WCOM-TASTO-INDIETRO
                                         WCOM-TASTO-NOTE.

           MOVE 'D'                  TO  WCOM-TASTO-DA-AUTORIZZARE
                                         WCOM-TASTO-AUTORIZZA
                                         WCOM-TASTO-AGGIORNA-PTF
                                         WCOM-TASTO-RESPINGI
                                         WCOM-TASTO-ELIMINA.

       EX-S15000.
           EXIT.
      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S90000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S90000.
           EXIT.
      *----------------------------------------------------------------*
      *    Ricerca della tabella parametro movimento
      *    Chiave di ricerca : Tipo Movimento
      *----------------------------------------------------------------*
       RIC-TAB-PMO.

           SET WK-NON-TROVATO TO TRUE
           MOVE ZERO          TO IX-RIC-PMO

           PERFORM UNTIL IX-RIC-PMO >= WPMO-ELE-PARAM-MOV-MAX
                      OR WK-TROVATO

              ADD 1 TO IX-RIC-PMO
              IF WPMO-TP-MOVI(IX-RIC-PMO) = WS-MOVIMENTO
                 AND NOT WPMO-ST-INV(IX-RIC-PMO)
                 SET WK-TROVATO TO TRUE
              END-IF

           END-PERFORM.

       RIC-TAB-PMO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Ricerca della tabella parametro oggetto
      *    Chiave di ricerca : Codice parametro
      *----------------------------------------------------------------*
       RIC-TAB-POG.

           SET WK-NON-TROVATO TO TRUE

           MOVE ZERO TO IX-RIC-POG

           PERFORM UNTIL IX-RIC-POG >= WPOG-ELE-PARAM-OGG-MAX
                      OR WK-TROVATO

              ADD 1 TO IX-RIC-POG

              IF WPOG-COD-PARAM(IX-RIC-POG) = WK-PARAMETRO
                 SET WK-TROVATO TO TRUE
              END-IF

           END-PERFORM.

       RIC-TAB-POG-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Ricerca della tabella Rapporto Anagrafico
      *    Tipologia Ricerca
      *    Chiave di ricerca
      *                    - Tipo rapporto anagrafico
      *----------------------------------------------------------------*
       RIC-TAB-RAN.

           SET WK-NON-TROVATO TO TRUE
           MOVE ZERO          TO IX-RIC-RAN

           PERFORM UNTIL IX-RIC-RAN >= WRAN-ELE-RAPP-ANAG-MAX
                      OR WK-TROVATO

              ADD 1 TO IX-RIC-RAN
              IF WRAN-TP-RAPP-ANA(IX-RIC-RAN) = WS-TP-RAPP-ANA
                 SET WK-TROVATO TO TRUE
              END-IF

           END-PERFORM.

       RIC-TAB-RAN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    Ricerca della tabella garanzie
      *    Chiave di ricerca : Tipo Garanzia
      *    Per le garanzie complementari si verifica che almeno una sia
      *    da includere.
      *----------------------------------------------------------------*
       RIC-TAB-GAR.

           SET WK-NON-TROVATO TO TRUE.

           MOVE ZERO TO IX-RIC-GAR.

           PERFORM UNTIL IX-RIC-GAR >= WGRZ-ELE-GARANZIA-MAX
                      OR WK-TROVATO

              ADD 1 TO IX-RIC-GAR

              IF WGRZ-TP-GAR(IX-RIC-GAR) = ACT-TP-GARANZIA

                 IF TP-GAR-COMPLEM
                    MOVE WGRZ-COD-TARI(IX-RIC-GAR) TO WS-COD-TARIFFA
                    PERFORM CTRL-GAR-INCLUSA
                       THRU CTRL-GAR-INCLUSA-EX
                    IF TROVATO
                       IF WPAG-GAR-INCLUSA-SI(IX-RIC-GRZ)
                          SET WK-TROVATO TO TRUE
                          PERFORM PREP-TAB-LCCC0490
                             THRU PREP-TAB-LCCC0490-EX
                       END-IF
                    END-IF
                 ELSE
                    SET WK-TROVATO TO TRUE
                    PERFORM PREP-TAB-LCCC0490
                       THRU PREP-TAB-LCCC0490-EX
                 END-IF

              END-IF

           END-PERFORM.

       RIC-TAB-GAR-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINES PER IL CALCOLO DELLE RATE RETRODATATE/SUCCESSIVE
      *----------------------------------------------------------------*
       CALL-LCCS0062.

           INITIALIZE AREA-LCCC0062.

           MOVE WPOL-DT-DECOR              TO LCCC0062-DT-DECORRENZA
           MOVE WK-DATA-COMPETENZA         TO LCCC0062-DT-COMPETENZA

           IF QUIETANZAMENTO-SI
              MOVE PCO-DT-ULT-QTZO-IN      TO LCCC0062-DT-ULT-QTZO
              MOVE ZEROES                  TO LCCC0062-DT-ULTGZ-TRCH
           END-IF

           IF GETRA-SI
              MOVE PCO-DT-ULTGZ-TRCH-E-IN  TO LCCC0062-DT-ULTGZ-TRCH
              MOVE ZEROES                  TO LCCC0062-DT-ULT-QTZO
           END-IF

           MOVE WK-FRAZ-MM                 TO LCCC0062-FRAZIONAMENTO.

           CALL LCCS0062 USING  AREA-IDSV0001
                                WCOM-AREA-STATI
                                AREA-LCCC0062
           ON EXCEPTION
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SERVIZIO CALCOLA RATE'   TO CALL-DESC
              MOVE 'CALL-LCCS0062'           TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.


       CALL-LCCS0062-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES PER IL CALCOLO SULLE DATE
      *----------------------------------------------------------------*
       CALL-LCCS0003.

           MOVE '02'                        TO A2K-FUNZ.
           MOVE '03'                        TO A2K-INFO.
           MOVE '0'                         TO A2K-FISLAV
                                               A2K-INICON.

           CALL LCCS0003 USING IO-A2K-LCCC0003 IN-RCODE
           ON EXCEPTION
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SERVIZIO OPERAZIONI SULLE DATE'
                                             TO CALL-DESC
               MOVE 'CALL-LCCS0003'          TO IEAI9901-LABEL-ERR
               PERFORM S0290-ERRORE-DI-SISTEMA
                  THRU EX-S0290
           END-CALL.

           IF IN-RCODE  = '00'
              CONTINUE
      *       MOVE A2K-OUAMG              TO WS-DATA
           ELSE
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'CALL-LCCS0003'        TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       CALL-LCCS0003-EX.
           EXIT.
11174 *----------------------------------------------------------------*
      *    VALORIZZA FLAG AUTOGENERAZIONE TITOLI
      *----------------------------------------------------------------*
       VALORIZZA-FLAG-AUTOGEN.

           IF WK-FL-INC-AUTOGEN-NULL NOT = HIGH-VALUE AND LOW-VALUE
                                                      AND SPACES

              PERFORM VARYING IX-TAB-CALC FROM 1 BY 1
                        UNTIL IX-TAB-CALC > WPAG-ELE-CALCOLI-MAX

                PERFORM VARYING IX-AREA-ISPC0140 FROM 1 BY 1
                          UNTIL IX-AREA-ISPC0140 > WPAG-ELE-TIT-CONT-MAX
                                                   (IX-TAB-CALC)
                                MOVE WK-FL-INC-AUTOGEN
                                  TO WPAG-CONT-AUTOGEN-INC
                                     (IX-TAB-CALC, IX-AREA-ISPC0140)
                END-PERFORM

                PERFORM VARYING IX-AREA-ISPC0140 FROM 1 BY 1
                          UNTIL IX-AREA-ISPC0140 > WPAG-ELE-TIT-RATA-MAX
                                                   (IX-TAB-CALC)
                                MOVE WK-FL-INC-AUTOGEN
                                  TO WPAG-RATA-AUTOGEN-INC
                                     (IX-TAB-CALC, IX-AREA-ISPC0140)
                END-PERFORM

              END-PERFORM

           END-IF.

       VALORIZZA-FLAG-AUTOGEN-EX.
11174      EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
           COPY IERP9903.
      *----------------------------------------------------------------*
      *    ROUTINES CALL VALORIZZATORE VARIABILI
      *----------------------------------------------------------------*
           COPY IVVP0210 REPLACING ==(SF)== BY ==S211==.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONI DA IDBVTGA ---> LCCVTGA
      *----------------------------------------------------------------*
           COPY LCCVTGA3 REPLACING ==(SF)== BY ==VS-TGA==.
      *    COPY LCCVPCO3 REPLACING ==(SF)== BY ==VS-PCO==.
      *----------------------------------------------------------------*
      *    COPY PER LETTURA MATRICE MOVIMENTO
      *----------------------------------------------------------------*
           COPY LCCP0021.
      *----------------------------------------------------------------*
      *    COPY PER LA GESTIONE DELLA TABELLA TEMP.
      *----------------------------------------------------------------*
           COPY IDSP0301.
           COPY IDSP0302.
      *----------------------------------------------------------------*
      *    COPY PER VALORIZZAZIONE SCHEDE SERVIZIO ISPS0140
      *----------------------------------------------------------------*
           COPY ISPP0142.

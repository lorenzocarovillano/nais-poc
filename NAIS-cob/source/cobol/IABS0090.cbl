       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IABS0090 IS INITIAL.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.  MAGGIO 2007.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULI PER ACCESSO RISORSE DB               *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE     PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ      PIC 9(009)  VALUE ZEROES.
       77 FLAG-INSERT-NEW-ROW PIC X       VALUE SPACE.

       01 WS-TIMESTAMP          PIC X(18).
       01 WS-TIMESTAMP-NUM REDEFINES WS-TIMESTAMP PIC 9(18).

       01 PGM-LCCS0090          PIC X(08) VALUE 'LCCS0090'.

      ***************************************************************
      *  COPY AREA CALL
      ***************************************************************

       01 LCCC0090.
           COPY LCCC0090            REPLACING ==(SF)== BY ==LINK==.

      *----------------------------------------------------------------*
      *    VARIABILI HOST
      *----------------------------------------------------------------*
       01 VARIABILI-HOST.
          02 WS-ID-MESSAGE                    PIC S9(09) COMP-3.

      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDBEM0 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVBEM1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 BTC-EXE-MESSAGE.

           PERFORM A000-INIZIO                    THRU A000-EX.


           EVALUATE TRUE
              WHEN IDSV0003-PRIMARY-KEY
                 PERFORM A200-ELABORA-PK       THRU A200-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
           END-EVALUATE


           GOBACK.
      ******************************************************************
       A000-INIZIO.

           MOVE 'IABS0090'             TO   IDSV0003-COD-SERVIZIO-BE.
           MOVE 'BTC_EXE_MESSAGE'           TO   IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           MOVE ZEROES                  TO WS-TIMESTAMP-NUM.

           ACCEPT WS-TIMESTAMP(1:8)     FROM DATE YYYYMMDD.
           ACCEPT WS-TIMESTAMP(9:6)     FROM TIME.

       A000-EX.
           EXIT.
      ******************************************************************
       A100-CHECK-RETURN-CODE.

           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      ******************************************************************
       A200-ELABORA-PK.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.

       A200-EX.
           EXIT.
      ******************************************************************
       A210-SELECT-PK.

           EXEC SQL
             SELECT
                 ID_BATCH
                ,ID_JOB
                ,ID_EXECUTION
                ,ID_MESSAGE
                ,COD_MESSAGE
                ,MESSAGE
             INTO
                  :BEM-ID-BATCH
                 ,:BEM-ID-JOB
                 ,:BEM-ID-EXECUTION
                 ,:BEM-ID-MESSAGE
                 ,:BEM-COD-MESSAGE
                 ,:BEM-MESSAGE-VCHAR
             FROM BTC_EXE_MESSAGE
             WHERE     ID_BATCH     = :BEM-ID-BATCH
                   AND ID_JOB       = :BEM-ID-JOB
                   AND ID_EXECUTION = :BEM-ID-EXECUTION
                   AND ID_MESSAGE   = :BEM-ID-MESSAGE

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

           END-IF.
       A210-EX.
           EXIT.
      ******************************************************************
       A220-INSERT-PK.

           PERFORM Z080-ESTRAI-ID-MESSAGE     THRU Z080-EX.

           IF IDSV0003-SUCCESSFUL-RC

              PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX

              EXEC SQL
                 INSERT
                 INTO BTC_EXE_MESSAGE
                     (
                        ID_BATCH
                       ,ID_JOB
                       ,ID_EXECUTION
                       ,ID_MESSAGE
                       ,COD_MESSAGE
                       ,MESSAGE
                     )
                 VALUES
                     (
                        :BEM-ID-BATCH
                       ,:BEM-ID-JOB
                       ,:BEM-ID-EXECUTION
                       ,:BEM-ID-MESSAGE
                       ,:BEM-COD-MESSAGE
                       ,:BEM-MESSAGE-VCHAR
                     )

              END-EXEC
           END-IF.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A220-EX.
           EXIT.
      ******************************************************************
       A230-UPDATE-PK.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           EXEC SQL
                UPDATE BTC_EXE_MESSAGE SET

                   ID_BATCH               = :BEM-ID-BATCH
                  ,ID_JOB                 = :BEM-ID-JOB
                  ,ID_EXECUTION           = :BEM-ID-EXECUTION
                  ,ID_MESSAGE             = :BEM-ID-MESSAGE
                  ,COD_MESSAGE            = :BEM-COD-MESSAGE
                  ,MESSAGE                = :BEM-MESSAGE-VCHAR
                WHERE  ID_BATCH     = :BEM-ID-BATCH
                   AND ID_JOB       = :BEM-ID-JOB
                   AND ID_EXECUTION = :BEM-ID-EXECUTION
                   AND ID_MESSAGE   = :BEM-ID-MESSAGE

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A230-EX.
           EXIT.
      ******************************************************************
       A240-DELETE-PK.

           EXEC SQL
                DELETE
                FROM BTC_EXE_MESSAGE
                WHERE  ID_BATCH     = :BEM-ID-BATCH
                   AND ID_JOB       = :BEM-ID-JOB
                   AND ID_EXECUTION = :BEM-ID-EXECUTION
                   AND ID_MESSAGE   = :BEM-ID-MESSAGE

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A240-EX.
           EXIT.
      ******************************************************************
       Z080-ESTRAI-ID-MESSAGE.

      ***************************************************************
      * STACCO DI SEQUENCE
      ***************************************************************

           MOVE SPACES             TO LCCC0090
           MOVE 'SEQ_ID_MESSAGE'   TO LINK-NOME-TABELLA

           CALL PGM-LCCS0090       USING LCCC0090


           MOVE LINK-RETURN-CODE
                            TO IDSV0003-RETURN-CODE
           MOVE LINK-SQLCODE
                            TO IDSV0003-SQLCODE
           MOVE LINK-DESCRIZ-ERR-DB2
                            TO IDSV0003-DESCRIZ-ERR-DB2


           IF IDSV0003-SUCCESSFUL-RC
              MOVE LINK-SEQ-TABELLA TO BEM-ID-MESSAGE
           END-IF.

       Z080-EX.
           EXIT.
      ******************************************************************
       Z100-SET-COLONNE-NULL.

           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.

       Z100-EX.
           EXIT.
      ******************************************************************
       Z200-SET-INDICATORI-NULL.

           CONTINUE.

       Z200-EX.
           EXIT.


      *****************************************************************

      **                                                              *
      **    PORTAFOGLIO VITA ITALIA VER. 1.0                          *
      **                                                              *
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LCCS0024.
       AUTHOR.             ATS/NA.
       DATE-WRITTEN.       2008.
       DATE-COMPILED.
      **--------------------------------------------------------------**
      *
      *    PROGRAMMA ..... LCCS0024
      *    TIPOLOGIA...... Componenti Comuni
      *    PROCESSO.......
      *    FUNZIONE....... CREAZIONE OGGETTO BLOCCO
      *    DESCRIZIONE....
      *
      **--------------------------------------------------------------**
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                            PIC X(08) VALUE 'LCCS0024'.
       01  WK-TABELLA                        PIC X(20) VALUE SPACES.

      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
      *--  TIPO STATO BLOCCO
       01  WS-TP-STAT-BLOCCO                 PIC X(002).
           88 ST-BLOCCO-ATTIVO                 VALUE 'AT'.
           88 ST-BLOCCO-NON-ATTIVO             VALUE 'NA'.

      *--  PRESENZA BLOCCO IN PTF
       01  WS-PRESENZA-BLOCCO                PIC X(001).
           88 FL-BLOCCO-PTF-SI                 VALUE 'S'.
           88 FL-BLOCCO-PTF-NO                 VALUE 'N'.

      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01 IX-INDICI.
          03 IX-MOV-SOSP                  PIC S9(04) COMP VALUE ZEROES.
      *----------------------------------------------------------------*
      *  TP_MOVI (Tipi Movimenti)
      *----------------------------------------------------------------*
           COPY LCCC0006.
           COPY LCCVXMV0.
           COPY LCCVXOG0.

      *----------------------------------------------------------------*
      *    MODULI CHIAMATI
      *----------------------------------------------------------------*
      *--> ESTRAZIONE SEQUENCE
       01  LCCS0090                         PIC X(008) VALUE 'LCCS0090'.

      *----------------------------------------------------------------*
      *    AREA MODULI CHIAMATI
      *----------------------------------------------------------------*
       01  AREA-IO-LCCS0090.
           COPY LCCC0090                 REPLACING ==(SF)== BY ==S090==.

      *----------------------------------------------------------------*
      *    COPY DISPATCHER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
           COPY IDSI0011.
           COPY IDSO0011.

      *----------------------------------------------------------------
      *    COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
           COPY IEAV9903.

      *----------------------------------------------------------------*
      *    COPY TABELLE DB2
      *----------------------------------------------------------------*
      *--  ANAG_BLOCCO
           COPY IDBVXAB1.
      *--  OGG_BLOCCO
           COPY IDBVL111.
      *--  MOVIMENTI_BATCH_SOSPESI
           COPY IDBVMBS1.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.

       01  AREA-STATI.
           COPY LCCC0001                REPLACING ==(SF)== BY ==WCOM==.

       01  AREA-IO-LCCS0024.
           COPY LCCC0024.

      *----------------------------------------------------------------*
      *    P R O C E D U R E  -  D I V I S I O N
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                AREA-STATI
                                AREA-IO-LCCS0024.

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU S0000-EX.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------
      *    OPERAZIONI INIZIALI
      *----------------------------------------------------------------
       S0000-OPERAZIONI-INIZIALI.

           MOVE IDSV0001-TIPO-MOVIMENTO       TO WS-MOVIMENTO
           MOVE LCCC0024-TP-OGGETTO           TO WS-TP-OGG

           PERFORM S0005-CTRL-DATI-INPUT
              THRU S0005-EX.

       S0000-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLI SUI DATI DI INPUT DEL SERVIZIO
      *----------------------------------------------------------------*
       S0005-CTRL-DATI-INPUT.

      *--  il campo ID-OGGETTO deve essere un numerico
           IF LCCC0024-ID-OGGETTO NOT NUMERIC
              MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0005-CTRL-DATI-INPUT' TO IEAI9901-LABEL-ERR
              MOVE '005076'                TO IEAI9901-COD-ERRORE
              MOVE 'LCCC0024-ID-OGGETTO'   TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF

           IF IDSV0001-ESITO-OK
      *--     il campo ID-OGGETTO deve essere un numerico > zero
              IF LCCC0024-ID-OGGETTO NOT > ZERO
                 MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0005-CTRL-DATI-INPUT'
                                            TO IEAI9901-LABEL-ERR
                 MOVE '005018'              TO IEAI9901-COD-ERRORE
                 MOVE 'LCCC0024-ID-OGGETTO' TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF

      *--  il campo TP-OGGETTO deve essere diverso da spaces e deve
      *--  essere un valore compreso nei valori di dominio
           IF IDSV0001-ESITO-OK
              IF LCCC0024-TP-OGGETTO = SPACES
                                   OR HIGH-VALUE OR LOW-VALUE
                 MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0005-CTRL-DATI-INPUT'
                                            TO IEAI9901-LABEL-ERR
                 MOVE '005007'              TO IEAI9901-COD-ERRORE
                 MOVE 'LCCC0024-TP-OGGETTO' TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF

           IF IDSV0001-ESITO-OK
              IF  (NOT POLIZZA)
              AND (NOT ADESIONE)
                 MOVE WK-PGM  TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0020-CTRL-DATI-INPUT' TO IEAI9901-LABEL-ERR
                 MOVE '005018'                TO IEAI9901-COD-ERRORE
                 MOVE 'TIPO COMUNICAZIONE'    TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF

           IF  IDSV0001-ESITO-OK
           AND ADESIONE
               IF LCCC0024-ID-OGG-PRIM NOT NUMERIC
                  MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S0005-CTRL-DATI-INPUT'
                                             TO IEAI9901-LABEL-ERR
                  MOVE '005076'              TO IEAI9901-COD-ERRORE
                  MOVE 'LCCC0024-ID-OGG-PRIM'
                                             TO IEAI9901-PARAMETRI-ERR
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF

      *--      il campo ID-OGGETTO PRIMARIO deve essere
      *--      un numerico > zero
               IF IDSV0001-ESITO-OK
                  IF LCCC0024-ID-OGG-PRIM NOT > ZERO
                     MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S0005-CTRL-DATI-INPUT'
                                             TO IEAI9901-LABEL-ERR
                     MOVE '005018'           TO IEAI9901-COD-ERRORE
                     MOVE 'LCCC0024-ID-OGG-PRIM'
                                             TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
                  END-IF
               END-IF
           END-IF

      *--  il campo COD-BLOCCO deve essere diverso da spaces e da null
      *--  e deve essere un valore compreso nei valori di dominio
           IF IDSV0001-ESITO-OK
              IF LCCC0024-COD-BLOCCO = SPACES
                                   OR HIGH-VALUE OR LOW-VALUE
                 MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S0005-CTRL-DATI-INPUT'
                                            TO IEAI9901-LABEL-ERR
                 MOVE '005007'              TO IEAI9901-COD-ERRORE
                 MOVE 'LCCC0024-COD-BLOCCO' TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF

              IF IDSV0001-ESITO-OK
                 PERFORM S0006-CTRL-COD-BLOCCO
                    THRU S0006-EX
              END-IF
           END-IF.

       S0005-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VERIFICA ESISTENZA BLOCCO SULLA TABELLA ANAG_BLOCCO
      *----------------------------------------------------------------*
       S0006-CTRL-COD-BLOCCO.

           MOVE 'ANA-BLOCCO'           TO WK-TABELLA.

      *--> VALORIZZAZIONE DELLA CHIAVE DI LETTURA
           MOVE IDSV0001-COD-COMPAGNIA-ANIA    TO XAB-COD-COMP-ANIA
           MOVE LCCC0024-COD-BLOCCO            TO XAB-COD-BLOCCO

      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
           MOVE ZERO                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                          IDSI0011-DATA-FINE-EFFETTO
                                          IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-SENZA-STOR  TO TRUE

      *--> NOME TABELLA FISICA DB
           MOVE WK-TABELLA             TO IDSI0011-CODICE-STR-DATO

      *--> DCLGEN TABELLA
           MOVE ANA-BLOCCO             TO IDSI0011-BUFFER-DATI

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT         TO TRUE

      *--> MODALITA DI ACCESSO
           SET IDSI0011-PRIMARY-KEY    TO TRUE

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
      *-->    GESTIRE ERRORE DB
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                     MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S0006-CTRL-COD-BLOCCO'
                                        TO IEAI9901-LABEL-ERR
                     MOVE '005201'      TO IEAI9901-COD-ERRORE
                     STRING 'CODICE BLOCCO = '
                            LCCC0024-COD-BLOCCO
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
                  WHEN IDSO0011-SUCCESSFUL-SQL
                     MOVE IDSO0011-BUFFER-DATI TO ANA-BLOCCO
                  WHEN OTHER
                     MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S0006-CTRL-COD-BLOCCO'
                                        TO IEAI9901-LABEL-ERR
                     MOVE '005016'      TO IEAI9901-COD-ERRORE
                     STRING WK-TABELLA           ';'
                            IDSO0011-RETURN-CODE ';'
                            IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0006-CTRL-COD-BLOCCO' TO IEAI9901-LABEL-ERR
              MOVE '005016'                TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S0006-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

12431 *    PERFORM S1005-VERIF-PRESENZA-BLOCCO
12431 *       THRU S1005-EX

           IF IDSV0001-ESITO-OK
12431 *       IF FL-BLOCCO-PTF-NO
                 PERFORM S1010-CENSIMENTO-BLOCCO
                    THRU S1010-EX
12431 *       END-IF
           END-IF

           IF IDSV0001-ESITO-OK
              IF LCCC0024-COD-BLOCCO = 'DISPO'
                 PERFORM S1015-GEST-MOVI-SOSPESO
                    THRU S1015-EX
              END-IF
           END-IF.

           IF IDSV0001-ESITO-OK
              PERFORM S1020-VALORIZZA-OUTPUT
                 THRU S1020-EX
           END-IF.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    Verifica presenza Oggetto blocco in Ptf
      *----------------------------------------------------------------*
       S1005-VERIF-PRESENZA-BLOCCO.

           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                         TO L11-COD-COMP-ANIA


           SET  ST-BLOCCO-ATTIVO         TO TRUE
           MOVE WS-TP-STAT-BLOCCO        TO L11-TP-STAT-BLOCCO
           MOVE LCCC0024-ID-OGGETTO      TO L11-ID-OGG
           MOVE LCCC0024-TP-OGGETTO      TO L11-TP-OGG
           MOVE LCCC0024-COD-BLOCCO      TO L11-COD-BLOCCO

           MOVE 'LDBS2470'               TO IDSI0011-CODICE-STR-DATO
           MOVE SPACES                   TO IDSI0011-BUFFER-DATI
           MOVE OGG-BLOCCO               TO IDSI0011-BUFFER-DATI
           MOVE OGG-BLOCCO               TO IDSI0011-BUFFER-WHERE-COND

           SET IDSI0011-TRATT-SENZA-STOR TO TRUE
           SET IDSI0011-WHERE-CONDITION  TO TRUE
           SET IDSI0011-SELECT           TO TRUE

           SET IDSO0011-SUCCESSFUL-RC    TO TRUE
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                     MOVE IDSO0011-BUFFER-DATI TO OGG-BLOCCO
                     SET FL-BLOCCO-PTF-SI TO TRUE
                  WHEN IDSO0011-NOT-FOUND
                     SET FL-BLOCCO-PTF-NO TO TRUE
              END-EVALUATE
           ELSE
              MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1005-VERIF-PRESENZA-BLOCCO'
                                            TO IEAI9901-LABEL-ERR
              MOVE '005016'                 TO IEAI9901-COD-ERRORE
              STRING 'LDBS2470'           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S1005-EX.
           EXIT.

      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1010-CENSIMENTO-BLOCCO.

           PERFORM S1011-VAL-OGG-BLOCCO
              THRU S1011-EX

           PERFORM S1012-INSERT-OGG-BLOCCO
              THRU S1012-EX.

       S1010-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE AREA OGGETTO BLOCCO
      *----------------------------------------------------------------*
       S1011-VAL-OGG-BLOCCO.

           MOVE 'OGG-BLOCCO'           TO WK-TABELLA

           PERFORM ESTR-SEQUENCE
              THRU ESTR-SEQUENCE-EX

           IF IDSV0001-ESITO-OK
              MOVE S090-SEQ-TABELLA            TO L11-ID-OGG-BLOCCO
              MOVE LCCC0024-ID-OGG-PRIM        TO L11-ID-OGG-1RIO
              MOVE LCCC0024-TP-OGG-PRIM        TO L11-TP-OGG-1RIO
              MOVE IDSV0001-COD-COMPAGNIA-ANIA TO L11-COD-COMP-ANIA
              MOVE IDSV0001-TIPO-MOVIMENTO     TO L11-TP-MOVI
              MOVE LCCC0024-ID-OGGETTO         TO L11-ID-OGG
              MOVE LCCC0024-TP-OGGETTO         TO L11-TP-OGG
              MOVE LCCC0024-COD-BLOCCO         TO L11-COD-BLOCCO
              IF LCCC0024-ID-RICH IS NOT NUMERIC OR
                 LCCC0024-ID-RICH = ZERO
                 MOVE HIGH-VALUE               TO L11-ID-RICH-NULL
              ELSE
                 MOVE LCCC0024-ID-RICH         TO L11-ID-RICH
              END-IF

              SET ST-BLOCCO-ATTIVO             TO TRUE
              MOVE WS-TP-STAT-BLOCCO           TO L11-TP-STAT-BLOCCO
              MOVE IDSV0001-DATA-EFFETTO       TO L11-DT-EFF
           END-IF.

       S1011-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    INSERT OGGETTO BLOCCO
      *----------------------------------------------------------------*
       S1012-INSERT-OGG-BLOCCO.

           MOVE 'OGG-BLOCCO'           TO WK-TABELLA.

      *--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
           MOVE ZERO                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                          IDSI0011-DATA-FINE-EFFETTO
                                          IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-SENZA-STOR  TO TRUE

      *--> NOME TABELLA FISICA DB
           MOVE WK-TABELLA             TO IDSI0011-CODICE-STR-DATO

      *--> DCLGEN TABELLA
           MOVE OGG-BLOCCO             TO IDSI0011-BUFFER-DATI

      *--> MODALITA DI ACCESSO
           SET IDSI0011-PRIMARY-KEY    TO TRUE

      *--> TIPO OPERAZIONE
           SET IDSI0011-INSERT         TO TRUE

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                     CONTINUE
                  WHEN OTHER
                     MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1012-INSERT-OGG-BLOCCO'
                                        TO IEAI9901-LABEL-ERR
                     MOVE '005016'      TO IEAI9901-COD-ERRORE
                     STRING WK-TABELLA           ';'
                            IDSO0011-RETURN-CODE ';'
                            IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1012-INSERT-OGG-BLOCCO' TO IEAI9901-LABEL-ERR
              MOVE '005016'                  TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S1012-EX.
           EXIT.

      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1015-GEST-MOVI-SOSPESO.

           PERFORM VARYING IX-MOV-SOSP FROM 1 BY 1
             UNTIL IX-MOV-SOSP > LCCC0024-ELE-MOV-SOS-MAX

             PERFORM S1016-VAL-MOVI-SOSPESO
                THRU S1016-EX

             PERFORM S1017-INSERT-MOVI-SOSPESO
                THRU S1017-EX

           END-PERFORM.

       S1015-EX.
           EXIT.

      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1016-VAL-MOVI-SOSPESO.

           MOVE 'MOVI-BATCH-SOSP'              TO WK-TABELLA

           PERFORM ESTR-SEQUENCE
              THRU ESTR-SEQUENCE-EX

           IF IDSV0001-ESITO-OK
              MOVE S090-SEQ-TABELLA            TO MBS-ID-MOVI-BATCH-SOSP
              MOVE IDSV0001-COD-COMPAGNIA-ANIA TO MBS-COD-COMP-ANIA
              MOVE LCCC0024-ID-OGG-SOSP(IX-MOV-SOSP)
                                               TO MBS-ID-OGG
              MOVE LCCC0024-TP-OGG-SOSP(IX-MOV-SOSP)
                                               TO MBS-TP-OGG
              MOVE LCCC0024-ID-MOVI-SOSP(IX-MOV-SOSP)
                                               TO MBS-ID-MOVI

              IF LCCC0024-TP-MOVI-SOSP-NULL(IX-MOV-SOSP) = HIGH-VALUE
                 MOVE IDSV0001-TIPO-MOVIMENTO  TO MBS-TP-MOVI
              ELSE
                 MOVE LCCC0024-TP-MOVI-SOSP(IX-MOV-SOSP)
                                               TO MBS-TP-MOVI
              END-IF

              MOVE IDSV0001-DATA-EFFETTO       TO MBS-DT-EFF
              MOVE L11-ID-OGG-BLOCCO           TO MBS-ID-OGG-BLOCCO
              MOVE LCCC0024-TP-FRM-ASSVA(IX-MOV-SOSP)
                                               TO MBS-TP-FRM-ASSVA

              IF LCCC0024-ID-BATCH-NULL(IX-MOV-SOSP) = HIGH-VALUE
                 MOVE LCCC0024-ID-BATCH-NULL(IX-MOV-SOSP)
                                               TO MBS-ID-BATCH-NULL
              ELSE
                 MOVE LCCC0024-ID-BATCH(IX-MOV-SOSP)
                                               TO MBS-ID-BATCH
              END-IF

              IF LCCC0024-ID-JOB-NULL(IX-MOV-SOSP) = HIGH-VALUE
                 MOVE LCCC0024-ID-JOB-NULL(IX-MOV-SOSP)
                                               TO MBS-ID-JOB-NULL
              ELSE
                 MOVE LCCC0024-ID-JOB(IX-MOV-SOSP)
                                               TO MBS-ID-JOB
              END-IF

              MOVE LCCC0024-STEP-ELAB(IX-MOV-SOSP)
                                               TO MBS-STEP-ELAB
              MOVE 'S'                         TO MBS-FL-MOVI-SOSP
              MOVE LCCC0024-D-INPUT-MOVI-SOSP(IX-MOV-SOSP)
                                               TO MBS-D-INPUT-MOVI-SOSP
           END-IF.

       S1016-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1017-INSERT-MOVI-SOSPESO.

           MOVE ZERO                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                          IDSI0011-DATA-FINE-EFFETTO
                                          IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-TRATT-SENZA-STOR  TO TRUE

           MOVE WK-TABELLA             TO IDSI0011-CODICE-STR-DATO

           MOVE MOVI-BATCH-SOSP        TO IDSI0011-BUFFER-DATI

           SET IDSI0011-PRIMARY-KEY    TO TRUE

           SET IDSI0011-INSERT         TO TRUE

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                     CONTINUE
                  WHEN OTHER
                     MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1017-INSERT-MOVI-SOSPESO'
                                        TO IEAI9901-LABEL-ERR
                     MOVE '005016'      TO IEAI9901-COD-ERRORE
                     STRING WK-TABELLA           ';'
                            IDSO0011-RETURN-CODE ';'
                            IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1017-INSERT-MOVI-SOSPESO'
                                             TO IEAI9901-LABEL-ERR
              MOVE '005016'                  TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S1017-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    OUTPUT DEL SERVIZIO
      *----------------------------------------------------------------*
       S1020-VALORIZZA-OUTPUT.


           MOVE L11-ID-OGG-BLOCCO TO LCCC0024-ID-OGG-BLOCCO.

12431 *    IF FL-BLOCCO-PTF-SI
12431 *       SET LCCC0024-BLC-PRESENTE-PTF TO TRUE
12431 *    END-IF

12431 *    IF FL-BLOCCO-PTF-NO
12431 *       SET LCCC0024-BLC-CENSITO-PTF TO TRUE
12431 *    END-IF.

12431      SET LCCC0024-BLC-CENSITO-PTF TO TRUE.


       S1020-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IERP9901.
           COPY IERP9902.
           COPY IERP9903.

      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.

      *----------------------------------------------------------------*
      *    ROUTINES ESTRAZIONE SEQUENCE
      *----------------------------------------------------------------*
           COPY LCCP0002.


      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS3150.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2014.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS3150
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... NUOVA FISCALITA' 2014
      *                  CALCOLO DELLA VARIABILE RATEO_CED_2014
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS3150'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01  WK-DT-RICOR-PREC            PIC 9(08).
44372  01  WK-DT-RICOR-SUCC            PIC 9(08).
       01  WK-APPO-DT                  PIC 9(008) VALUE ZERO.

      *-- AREA ROUTINE PER IL CALCOLO DIFFERENZA TRA DATE
       01  FORMATO                 PIC X(01).
       01  DATA-INFERIORE.
           05 AAAA-INF             PIC 9(04).
           05 MM-INF               PIC 9(02).
           05 GG-INF               PIC 9(02).
       01  DATA-SUPERIORE.
           05 AAAA-SUP             PIC 9(04).
           05 MM-SUP               PIC 9(02).
           05 GG-SUP               PIC 9(02).
       01  GG-DIFF                 PIC 9(05).
       01  CODICE-RITORNO          PIC X(01).

      *---------------------------------------------------------------*
      *  LISTA MOVIMENTI IMPLEMENTATI LCCC0006
      *---------------------------------------------------------------*
           COPY LCCC0006.

      *----------------------------------------------------------------*
      *--> AREA POLIZZA
      *----------------------------------------------------------------*
       01 AREA-IO-POL.
          03 DPOL-AREA-POLIZZA.
             04 DPOL-ELE-POLI-MAX       PIC S9(04) COMP.
             04 DPOL-TAB-POLI.
             COPY LCCVPOL1              REPLACING ==(SF)== BY ==DPOL==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *    COPY TABELLE DB2
      *----------------------------------------------------------------*
           COPY IDBVPMO1.
           COPY LDBV5061.

      *----------------------------------------------------------------*
      *    COPY TIPOLOGICHE
      *----------------------------------------------------------------*
           COPY LCCVXMVZ.
           COPY LCCVXMV6.

      *----------------------------------------------------------------*
      *      COPY VARIABILI PER LA GESTIONE ERRORI                     *
      *----------------------------------------------------------------*
           COPY IDSV0002.

      *----------------------------------------------------------------*
      *      FLAG                                                      *
      *----------------------------------------------------------------*
       01 WK-PARAM-MOVI                     PIC X(01).
          88 WK-PARAM-MOVI-OK               VALUE 'S'.
          88 WK-PARAM-MOVI-KO               VALUE 'N'.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS3150.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS3150.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

41986      MOVE IVVC0213-TIPO-MOVIMENTO  TO WS-MOVIMENTO
41986      IF GENER-CEDOLA
             PERFORM S1000-ELABORAZIONE
                THRU EX-S1000
41986      ELSE
41986        SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
41986      END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           INITIALIZE AREA-IO-POL.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE

      *--> RECUPERA PARAMETRO MOVIMENTO
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1200-RECUP-PARAM-MOVI
                  THRU S1200-RECUP-PARAM-MOVI-EX
           END-IF.

      *--> CALCOLO RATEOCED2014
           IF  IDSV0003-SUCCESSFUL-RC
41986 *    AND IDSV0003-SUCCESSFUL-SQL
41986      AND IDSV0003-NOT-FOUND
           AND WK-PARAM-MOVI-OK
44372       IF  WK-DT-RICOR-SUCC >= 20140701
44372       AND WK-DT-RICOR-SUCC <= 20150629
               PERFORM S1300-CALCOLO-RATEOCED2014
                  THRU S1300-CALCOLO-RATEOCED2014-EX
44372       ELSE
44372          SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
44372       END-IF
           END-IF.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-POLI
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DPOL-AREA-POLIZZA
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   RECUPERA PARAMETRO MOVIMENTO
      *----------------------------------------------------------------*
       S1200-RECUP-PARAM-MOVI.

           SET WK-PARAM-MOVI-KO             TO TRUE

41986 *    SET IDSV0003-SELECT              TO TRUE
41986      SET IDSV0003-FETCH-FIRST         TO TRUE
           SET IDSV0003-WHERE-CONDITION     TO TRUE
           SET IDSV0003-TRATT-X-COMPETENZA  TO TRUE
      *
           INITIALIZE PARAM-MOVI
                      LDBV5061
                      IDSV0003-BUFFER-WHERE-COND
41986                 WK-DT-RICOR-PREC
44372                 WK-DT-RICOR-SUCC.
      *
           MOVE DPOL-ID-POLI             TO LDBV5061-ID-POLI.
           SET GENER-CEDOLA              TO TRUE.
           MOVE WS-MOVIMENTO             TO LDBV5061-TP-MOVI-01.
           MOVE LDBV5061                 TO IDSV0003-BUFFER-WHERE-COND.

           MOVE 'LDBS5060'               TO WK-CALL-PGM

41986      PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
41986                 OR NOT IDSV0003-SUCCESSFUL-SQL

           CALL WK-CALL-PGM  USING  IDSV0003 PARAM-MOVI
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS5060 ERRORE CHIAMATA '
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL
      *
           IF IDSV0003-SUCCESSFUL-RC
41986 *    AND IDSV0003-SUCCESSFUL-SQL
41986       IF IDSV0003-SUCCESSFUL-SQL
              IF  PMO-DT-RICOR-PREC-NULL NOT EQUAL HIGH-VALUE
              AND PMO-DT-RICOR-PREC-NULL NOT EQUAL LOW-VALUE
              AND PMO-DT-RICOR-PREC-NULL NOT EQUAL SPACES
41986 *          INITIALIZE WK-DT-RICOR-PREC
41986          IF PMO-DT-RICOR-PREC > WK-DT-RICOR-PREC
                 MOVE PMO-DT-RICOR-PREC
                   TO WK-DT-RICOR-PREC
41986 *          SET WK-PARAM-MOVI-OK
41986 *           TO TRUE
41986          END-IF
41986 *       ELSE
41986 *          INITIALIZE WK-DT-RICOR-PREC
41986 *          MOVE DPOL-DT-DECOR
41986 *            TO WK-DT-RICOR-PREC
41986 *          SET WK-PARAM-MOVI-OK
41986 *           TO TRUE
              END-IF
44372         IF  PMO-DT-RICOR-SUCC-NULL NOT EQUAL HIGH-VALUE
44372         AND PMO-DT-RICOR-SUCC-NULL NOT EQUAL LOW-VALUE
44372         AND PMO-DT-RICOR-SUCC-NULL NOT EQUAL SPACES
44372          IF PMO-DT-RICOR-SUCC > WK-DT-RICOR-SUCC
44372             MOVE PMO-DT-RICOR-SUCC
44372               TO WK-DT-RICOR-SUCC
44372          END-IF
44372         END-IF
41986         SET IDSV0003-FETCH-NEXT      TO TRUE
41986       ELSE
41986        IF IDSV0003-NOT-FOUND
41986         IF IDSV0003-FETCH-FIRST
41986            SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
41986         ELSE
41986          SET WK-PARAM-MOVI-OK TO TRUE
41986          IF WK-DT-RICOR-PREC = ZERO
41986            MOVE DPOL-DT-DECOR
41986              TO WK-DT-RICOR-PREC
41986          END-IF
41986         END-IF
41986        ELSE
41986         IF IDSV0003-SQLCODE = -305
41986            SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
41986         ELSE
41986            SET IDSV0003-INVALID-OPER            TO TRUE
41986            MOVE WK-CALL-PGM          TO IDSV0003-COD-SERVIZIO-BE
41986            STRING 'CHIAMATA LDBS5060 ;'
41986                   IDSV0003-RETURN-CODE ';'
41986                   IDSV0003-SQLCODE
41986            DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
41986            END-STRING
41986         END-IF
41986        END-IF
41986       END-IF
           ELSE
              MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBS5060 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING

              IF IDSV0003-NOT-FOUND
              OR IDSV0003-SQLCODE = -305
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF
           END-IF
41986      END-PERFORM.
      *
       S1200-RECUP-PARAM-MOVI-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   CALCOLO RATEOCED2014
      *----------------------------------------------------------------*
       S1300-CALCOLO-RATEOCED2014.

           INITIALIZE WK-APPO-DT

           MOVE 'A'                       TO FORMATO.
           MOVE 20140630                  TO DATA-SUPERIORE.
           MOVE WK-DT-RICOR-PREC          TO WK-APPO-DT.
           MOVE WK-APPO-DT                TO DATA-INFERIORE.
           MOVE 'LCCS0010'                TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING FORMATO,
                                   DATA-INFERIORE,
                                   DATA-SUPERIORE,
                                   GG-DIFF,
                                   CODICE-RITORNO
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'ERRORE CALL-LCCS0010 - CALCOLA DATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.
      *
           IF IDSV0003-SUCCESSFUL-RC
              IF CODICE-RITORNO EQUAL ZERO

                 COMPUTE IVVC0213-VAL-PERC-O ROUNDED = GG-DIFF / 360
                                                       * 100

              ELSE
                 MOVE WK-CALL-PGM         TO IDSV0003-COD-SERVIZIO-BE
                 STRING 'CHIAMATA LCCS0010 COD-RIT:'
                         CODICE-RITORNO ';'
                    DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING
                 SET IDSV0003-FIELD-NOT-VALUED TO TRUE
              END-IF
           END-IF.

       S1300-CALCOLO-RATEOCED2014-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           MOVE SPACES                     TO IVVC0213-VAL-STR-O.
           MOVE 0                          TO IVVC0213-VAL-IMP-O.
      *
           GOBACK.

       EX-S9000.
           EXIT.

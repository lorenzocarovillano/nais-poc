       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSTCL0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  02 LUG 2014.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDTCL0 END-EXEC.
           EXEC SQL INCLUDE IDBVTCL2 END-EXEC.
           EXEC SQL INCLUDE IDBVTCL3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVTCL1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 TCONT-LIQ.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSTCL0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'TCONT_LIQ' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TCONT_LIQ
                ,ID_PERC_LIQ
                ,ID_BNFICR_LIQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_VLT
                ,IMP_LRD_LIQTO
                ,IMP_PREST
                ,IMP_INTR_PREST
                ,IMP_RAT
                ,IMP_UTI
                ,IMP_RIT_TFR
                ,IMP_RIT_ACC
                ,IMP_RIT_VIS
                ,IMPB_TFR
                ,IMPB_ACC
                ,IMPB_VIS
                ,IMP_RIMB
                ,IMP_CORTVO
                ,IMPB_IMPST_PRVR
                ,IMPST_PRVR
                ,IMPB_IMPST_252
                ,IMPST_252
                ,IMP_IS
                ,IMP_DIR_LIQ
                ,IMP_NET_LIQTO
                ,IMP_EFFLQ
                ,COD_DVS
                ,TP_MEZ_PAG_ACCR
                ,ESTR_CNT_CORR_ACCR
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TP_STAT_TIT
                ,IMPB_VIS_1382011
                ,IMPST_VIS_1382011
                ,IMPST_SOST_1382011
                ,IMP_INTR_RIT_PAG
                ,IMPB_IS
                ,IMPB_IS_1382011
                ,IMPB_VIS_662014
                ,IMPST_VIS_662014
                ,IMPB_IS_662014
                ,IMPST_SOST_662014
             INTO
                :TCL-ID-TCONT-LIQ
               ,:TCL-ID-PERC-LIQ
               ,:TCL-ID-BNFICR-LIQ
               ,:TCL-ID-MOVI-CRZ
               ,:TCL-ID-MOVI-CHIU
                :IND-TCL-ID-MOVI-CHIU
               ,:TCL-DT-INI-EFF-DB
               ,:TCL-DT-END-EFF-DB
               ,:TCL-COD-COMP-ANIA
               ,:TCL-DT-VLT-DB
                :IND-TCL-DT-VLT
               ,:TCL-IMP-LRD-LIQTO
                :IND-TCL-IMP-LRD-LIQTO
               ,:TCL-IMP-PREST
                :IND-TCL-IMP-PREST
               ,:TCL-IMP-INTR-PREST
                :IND-TCL-IMP-INTR-PREST
               ,:TCL-IMP-RAT
                :IND-TCL-IMP-RAT
               ,:TCL-IMP-UTI
                :IND-TCL-IMP-UTI
               ,:TCL-IMP-RIT-TFR
                :IND-TCL-IMP-RIT-TFR
               ,:TCL-IMP-RIT-ACC
                :IND-TCL-IMP-RIT-ACC
               ,:TCL-IMP-RIT-VIS
                :IND-TCL-IMP-RIT-VIS
               ,:TCL-IMPB-TFR
                :IND-TCL-IMPB-TFR
               ,:TCL-IMPB-ACC
                :IND-TCL-IMPB-ACC
               ,:TCL-IMPB-VIS
                :IND-TCL-IMPB-VIS
               ,:TCL-IMP-RIMB
                :IND-TCL-IMP-RIMB
               ,:TCL-IMP-CORTVO
                :IND-TCL-IMP-CORTVO
               ,:TCL-IMPB-IMPST-PRVR
                :IND-TCL-IMPB-IMPST-PRVR
               ,:TCL-IMPST-PRVR
                :IND-TCL-IMPST-PRVR
               ,:TCL-IMPB-IMPST-252
                :IND-TCL-IMPB-IMPST-252
               ,:TCL-IMPST-252
                :IND-TCL-IMPST-252
               ,:TCL-IMP-IS
                :IND-TCL-IMP-IS
               ,:TCL-IMP-DIR-LIQ
                :IND-TCL-IMP-DIR-LIQ
               ,:TCL-IMP-NET-LIQTO
                :IND-TCL-IMP-NET-LIQTO
               ,:TCL-IMP-EFFLQ
                :IND-TCL-IMP-EFFLQ
               ,:TCL-COD-DVS
                :IND-TCL-COD-DVS
               ,:TCL-TP-MEZ-PAG-ACCR
                :IND-TCL-TP-MEZ-PAG-ACCR
               ,:TCL-ESTR-CNT-CORR-ACCR
                :IND-TCL-ESTR-CNT-CORR-ACCR
               ,:TCL-DS-RIGA
               ,:TCL-DS-OPER-SQL
               ,:TCL-DS-VER
               ,:TCL-DS-TS-INI-CPTZ
               ,:TCL-DS-TS-END-CPTZ
               ,:TCL-DS-UTENTE
               ,:TCL-DS-STATO-ELAB
               ,:TCL-TP-STAT-TIT
                :IND-TCL-TP-STAT-TIT
               ,:TCL-IMPB-VIS-1382011
                :IND-TCL-IMPB-VIS-1382011
               ,:TCL-IMPST-VIS-1382011
                :IND-TCL-IMPST-VIS-1382011
               ,:TCL-IMPST-SOST-1382011
                :IND-TCL-IMPST-SOST-1382011
               ,:TCL-IMP-INTR-RIT-PAG
                :IND-TCL-IMP-INTR-RIT-PAG
               ,:TCL-IMPB-IS
                :IND-TCL-IMPB-IS
               ,:TCL-IMPB-IS-1382011
                :IND-TCL-IMPB-IS-1382011
               ,:TCL-IMPB-VIS-662014
                :IND-TCL-IMPB-VIS-662014
               ,:TCL-IMPST-VIS-662014
                :IND-TCL-IMPST-VIS-662014
               ,:TCL-IMPB-IS-662014
                :IND-TCL-IMPB-IS-662014
               ,:TCL-IMPST-SOST-662014
                :IND-TCL-IMPST-SOST-662014
             FROM TCONT_LIQ
             WHERE     DS_RIGA = :TCL-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO TCONT_LIQ
                     (
                        ID_TCONT_LIQ
                       ,ID_PERC_LIQ
                       ,ID_BNFICR_LIQ
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,DT_VLT
                       ,IMP_LRD_LIQTO
                       ,IMP_PREST
                       ,IMP_INTR_PREST
                       ,IMP_RAT
                       ,IMP_UTI
                       ,IMP_RIT_TFR
                       ,IMP_RIT_ACC
                       ,IMP_RIT_VIS
                       ,IMPB_TFR
                       ,IMPB_ACC
                       ,IMPB_VIS
                       ,IMP_RIMB
                       ,IMP_CORTVO
                       ,IMPB_IMPST_PRVR
                       ,IMPST_PRVR
                       ,IMPB_IMPST_252
                       ,IMPST_252
                       ,IMP_IS
                       ,IMP_DIR_LIQ
                       ,IMP_NET_LIQTO
                       ,IMP_EFFLQ
                       ,COD_DVS
                       ,TP_MEZ_PAG_ACCR
                       ,ESTR_CNT_CORR_ACCR
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,TP_STAT_TIT
                       ,IMPB_VIS_1382011
                       ,IMPST_VIS_1382011
                       ,IMPST_SOST_1382011
                       ,IMP_INTR_RIT_PAG
                       ,IMPB_IS
                       ,IMPB_IS_1382011
                       ,IMPB_VIS_662014
                       ,IMPST_VIS_662014
                       ,IMPB_IS_662014
                       ,IMPST_SOST_662014
                     )
                 VALUES
                     (
                       :TCL-ID-TCONT-LIQ
                       ,:TCL-ID-PERC-LIQ
                       ,:TCL-ID-BNFICR-LIQ
                       ,:TCL-ID-MOVI-CRZ
                       ,:TCL-ID-MOVI-CHIU
                        :IND-TCL-ID-MOVI-CHIU
                       ,:TCL-DT-INI-EFF-DB
                       ,:TCL-DT-END-EFF-DB
                       ,:TCL-COD-COMP-ANIA
                       ,:TCL-DT-VLT-DB
                        :IND-TCL-DT-VLT
                       ,:TCL-IMP-LRD-LIQTO
                        :IND-TCL-IMP-LRD-LIQTO
                       ,:TCL-IMP-PREST
                        :IND-TCL-IMP-PREST
                       ,:TCL-IMP-INTR-PREST
                        :IND-TCL-IMP-INTR-PREST
                       ,:TCL-IMP-RAT
                        :IND-TCL-IMP-RAT
                       ,:TCL-IMP-UTI
                        :IND-TCL-IMP-UTI
                       ,:TCL-IMP-RIT-TFR
                        :IND-TCL-IMP-RIT-TFR
                       ,:TCL-IMP-RIT-ACC
                        :IND-TCL-IMP-RIT-ACC
                       ,:TCL-IMP-RIT-VIS
                        :IND-TCL-IMP-RIT-VIS
                       ,:TCL-IMPB-TFR
                        :IND-TCL-IMPB-TFR
                       ,:TCL-IMPB-ACC
                        :IND-TCL-IMPB-ACC
                       ,:TCL-IMPB-VIS
                        :IND-TCL-IMPB-VIS
                       ,:TCL-IMP-RIMB
                        :IND-TCL-IMP-RIMB
                       ,:TCL-IMP-CORTVO
                        :IND-TCL-IMP-CORTVO
                       ,:TCL-IMPB-IMPST-PRVR
                        :IND-TCL-IMPB-IMPST-PRVR
                       ,:TCL-IMPST-PRVR
                        :IND-TCL-IMPST-PRVR
                       ,:TCL-IMPB-IMPST-252
                        :IND-TCL-IMPB-IMPST-252
                       ,:TCL-IMPST-252
                        :IND-TCL-IMPST-252
                       ,:TCL-IMP-IS
                        :IND-TCL-IMP-IS
                       ,:TCL-IMP-DIR-LIQ
                        :IND-TCL-IMP-DIR-LIQ
                       ,:TCL-IMP-NET-LIQTO
                        :IND-TCL-IMP-NET-LIQTO
                       ,:TCL-IMP-EFFLQ
                        :IND-TCL-IMP-EFFLQ
                       ,:TCL-COD-DVS
                        :IND-TCL-COD-DVS
                       ,:TCL-TP-MEZ-PAG-ACCR
                        :IND-TCL-TP-MEZ-PAG-ACCR
                       ,:TCL-ESTR-CNT-CORR-ACCR
                        :IND-TCL-ESTR-CNT-CORR-ACCR
                       ,:TCL-DS-RIGA
                       ,:TCL-DS-OPER-SQL
                       ,:TCL-DS-VER
                       ,:TCL-DS-TS-INI-CPTZ
                       ,:TCL-DS-TS-END-CPTZ
                       ,:TCL-DS-UTENTE
                       ,:TCL-DS-STATO-ELAB
                       ,:TCL-TP-STAT-TIT
                        :IND-TCL-TP-STAT-TIT
                       ,:TCL-IMPB-VIS-1382011
                        :IND-TCL-IMPB-VIS-1382011
                       ,:TCL-IMPST-VIS-1382011
                        :IND-TCL-IMPST-VIS-1382011
                       ,:TCL-IMPST-SOST-1382011
                        :IND-TCL-IMPST-SOST-1382011
                       ,:TCL-IMP-INTR-RIT-PAG
                        :IND-TCL-IMP-INTR-RIT-PAG
                       ,:TCL-IMPB-IS
                        :IND-TCL-IMPB-IS
                       ,:TCL-IMPB-IS-1382011
                        :IND-TCL-IMPB-IS-1382011
                       ,:TCL-IMPB-VIS-662014
                        :IND-TCL-IMPB-VIS-662014
                       ,:TCL-IMPST-VIS-662014
                        :IND-TCL-IMPST-VIS-662014
                       ,:TCL-IMPB-IS-662014
                        :IND-TCL-IMPB-IS-662014
                       ,:TCL-IMPST-SOST-662014
                        :IND-TCL-IMPST-SOST-662014
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE TCONT_LIQ SET

                   ID_TCONT_LIQ           =
                :TCL-ID-TCONT-LIQ
                  ,ID_PERC_LIQ            =
                :TCL-ID-PERC-LIQ
                  ,ID_BNFICR_LIQ          =
                :TCL-ID-BNFICR-LIQ
                  ,ID_MOVI_CRZ            =
                :TCL-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :TCL-ID-MOVI-CHIU
                                       :IND-TCL-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :TCL-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :TCL-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :TCL-COD-COMP-ANIA
                  ,DT_VLT                 =
           :TCL-DT-VLT-DB
                                       :IND-TCL-DT-VLT
                  ,IMP_LRD_LIQTO          =
                :TCL-IMP-LRD-LIQTO
                                       :IND-TCL-IMP-LRD-LIQTO
                  ,IMP_PREST              =
                :TCL-IMP-PREST
                                       :IND-TCL-IMP-PREST
                  ,IMP_INTR_PREST         =
                :TCL-IMP-INTR-PREST
                                       :IND-TCL-IMP-INTR-PREST
                  ,IMP_RAT                =
                :TCL-IMP-RAT
                                       :IND-TCL-IMP-RAT
                  ,IMP_UTI                =
                :TCL-IMP-UTI
                                       :IND-TCL-IMP-UTI
                  ,IMP_RIT_TFR            =
                :TCL-IMP-RIT-TFR
                                       :IND-TCL-IMP-RIT-TFR
                  ,IMP_RIT_ACC            =
                :TCL-IMP-RIT-ACC
                                       :IND-TCL-IMP-RIT-ACC
                  ,IMP_RIT_VIS            =
                :TCL-IMP-RIT-VIS
                                       :IND-TCL-IMP-RIT-VIS
                  ,IMPB_TFR               =
                :TCL-IMPB-TFR
                                       :IND-TCL-IMPB-TFR
                  ,IMPB_ACC               =
                :TCL-IMPB-ACC
                                       :IND-TCL-IMPB-ACC
                  ,IMPB_VIS               =
                :TCL-IMPB-VIS
                                       :IND-TCL-IMPB-VIS
                  ,IMP_RIMB               =
                :TCL-IMP-RIMB
                                       :IND-TCL-IMP-RIMB
                  ,IMP_CORTVO             =
                :TCL-IMP-CORTVO
                                       :IND-TCL-IMP-CORTVO
                  ,IMPB_IMPST_PRVR        =
                :TCL-IMPB-IMPST-PRVR
                                       :IND-TCL-IMPB-IMPST-PRVR
                  ,IMPST_PRVR             =
                :TCL-IMPST-PRVR
                                       :IND-TCL-IMPST-PRVR
                  ,IMPB_IMPST_252         =
                :TCL-IMPB-IMPST-252
                                       :IND-TCL-IMPB-IMPST-252
                  ,IMPST_252              =
                :TCL-IMPST-252
                                       :IND-TCL-IMPST-252
                  ,IMP_IS                 =
                :TCL-IMP-IS
                                       :IND-TCL-IMP-IS
                  ,IMP_DIR_LIQ            =
                :TCL-IMP-DIR-LIQ
                                       :IND-TCL-IMP-DIR-LIQ
                  ,IMP_NET_LIQTO          =
                :TCL-IMP-NET-LIQTO
                                       :IND-TCL-IMP-NET-LIQTO
                  ,IMP_EFFLQ              =
                :TCL-IMP-EFFLQ
                                       :IND-TCL-IMP-EFFLQ
                  ,COD_DVS                =
                :TCL-COD-DVS
                                       :IND-TCL-COD-DVS
                  ,TP_MEZ_PAG_ACCR        =
                :TCL-TP-MEZ-PAG-ACCR
                                       :IND-TCL-TP-MEZ-PAG-ACCR
                  ,ESTR_CNT_CORR_ACCR     =
                :TCL-ESTR-CNT-CORR-ACCR
                                       :IND-TCL-ESTR-CNT-CORR-ACCR
                  ,DS_RIGA                =
                :TCL-DS-RIGA
                  ,DS_OPER_SQL            =
                :TCL-DS-OPER-SQL
                  ,DS_VER                 =
                :TCL-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :TCL-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :TCL-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :TCL-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :TCL-DS-STATO-ELAB
                  ,TP_STAT_TIT            =
                :TCL-TP-STAT-TIT
                                       :IND-TCL-TP-STAT-TIT
                  ,IMPB_VIS_1382011       =
                :TCL-IMPB-VIS-1382011
                                       :IND-TCL-IMPB-VIS-1382011
                  ,IMPST_VIS_1382011      =
                :TCL-IMPST-VIS-1382011
                                       :IND-TCL-IMPST-VIS-1382011
                  ,IMPST_SOST_1382011     =
                :TCL-IMPST-SOST-1382011
                                       :IND-TCL-IMPST-SOST-1382011
                  ,IMP_INTR_RIT_PAG       =
                :TCL-IMP-INTR-RIT-PAG
                                       :IND-TCL-IMP-INTR-RIT-PAG
                  ,IMPB_IS                =
                :TCL-IMPB-IS
                                       :IND-TCL-IMPB-IS
                  ,IMPB_IS_1382011        =
                :TCL-IMPB-IS-1382011
                                       :IND-TCL-IMPB-IS-1382011
                  ,IMPB_VIS_662014        =
                :TCL-IMPB-VIS-662014
                                       :IND-TCL-IMPB-VIS-662014
                  ,IMPST_VIS_662014       =
                :TCL-IMPST-VIS-662014
                                       :IND-TCL-IMPST-VIS-662014
                  ,IMPB_IS_662014         =
                :TCL-IMPB-IS-662014
                                       :IND-TCL-IMPB-IS-662014
                  ,IMPST_SOST_662014      =
                :TCL-IMPST-SOST-662014
                                       :IND-TCL-IMPST-SOST-662014
                WHERE     DS_RIGA = :TCL-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM TCONT_LIQ
                WHERE     DS_RIGA = :TCL-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-TCL CURSOR FOR
              SELECT
                     ID_TCONT_LIQ
                    ,ID_PERC_LIQ
                    ,ID_BNFICR_LIQ
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_VLT
                    ,IMP_LRD_LIQTO
                    ,IMP_PREST
                    ,IMP_INTR_PREST
                    ,IMP_RAT
                    ,IMP_UTI
                    ,IMP_RIT_TFR
                    ,IMP_RIT_ACC
                    ,IMP_RIT_VIS
                    ,IMPB_TFR
                    ,IMPB_ACC
                    ,IMPB_VIS
                    ,IMP_RIMB
                    ,IMP_CORTVO
                    ,IMPB_IMPST_PRVR
                    ,IMPST_PRVR
                    ,IMPB_IMPST_252
                    ,IMPST_252
                    ,IMP_IS
                    ,IMP_DIR_LIQ
                    ,IMP_NET_LIQTO
                    ,IMP_EFFLQ
                    ,COD_DVS
                    ,TP_MEZ_PAG_ACCR
                    ,ESTR_CNT_CORR_ACCR
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,TP_STAT_TIT
                    ,IMPB_VIS_1382011
                    ,IMPST_VIS_1382011
                    ,IMPST_SOST_1382011
                    ,IMP_INTR_RIT_PAG
                    ,IMPB_IS
                    ,IMPB_IS_1382011
                    ,IMPB_VIS_662014
                    ,IMPST_VIS_662014
                    ,IMPB_IS_662014
                    ,IMPST_SOST_662014
              FROM TCONT_LIQ
              WHERE     ID_TCONT_LIQ = :TCL-ID-TCONT-LIQ
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TCONT_LIQ
                ,ID_PERC_LIQ
                ,ID_BNFICR_LIQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_VLT
                ,IMP_LRD_LIQTO
                ,IMP_PREST
                ,IMP_INTR_PREST
                ,IMP_RAT
                ,IMP_UTI
                ,IMP_RIT_TFR
                ,IMP_RIT_ACC
                ,IMP_RIT_VIS
                ,IMPB_TFR
                ,IMPB_ACC
                ,IMPB_VIS
                ,IMP_RIMB
                ,IMP_CORTVO
                ,IMPB_IMPST_PRVR
                ,IMPST_PRVR
                ,IMPB_IMPST_252
                ,IMPST_252
                ,IMP_IS
                ,IMP_DIR_LIQ
                ,IMP_NET_LIQTO
                ,IMP_EFFLQ
                ,COD_DVS
                ,TP_MEZ_PAG_ACCR
                ,ESTR_CNT_CORR_ACCR
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TP_STAT_TIT
                ,IMPB_VIS_1382011
                ,IMPST_VIS_1382011
                ,IMPST_SOST_1382011
                ,IMP_INTR_RIT_PAG
                ,IMPB_IS
                ,IMPB_IS_1382011
                ,IMPB_VIS_662014
                ,IMPST_VIS_662014
                ,IMPB_IS_662014
                ,IMPST_SOST_662014
             INTO
                :TCL-ID-TCONT-LIQ
               ,:TCL-ID-PERC-LIQ
               ,:TCL-ID-BNFICR-LIQ
               ,:TCL-ID-MOVI-CRZ
               ,:TCL-ID-MOVI-CHIU
                :IND-TCL-ID-MOVI-CHIU
               ,:TCL-DT-INI-EFF-DB
               ,:TCL-DT-END-EFF-DB
               ,:TCL-COD-COMP-ANIA
               ,:TCL-DT-VLT-DB
                :IND-TCL-DT-VLT
               ,:TCL-IMP-LRD-LIQTO
                :IND-TCL-IMP-LRD-LIQTO
               ,:TCL-IMP-PREST
                :IND-TCL-IMP-PREST
               ,:TCL-IMP-INTR-PREST
                :IND-TCL-IMP-INTR-PREST
               ,:TCL-IMP-RAT
                :IND-TCL-IMP-RAT
               ,:TCL-IMP-UTI
                :IND-TCL-IMP-UTI
               ,:TCL-IMP-RIT-TFR
                :IND-TCL-IMP-RIT-TFR
               ,:TCL-IMP-RIT-ACC
                :IND-TCL-IMP-RIT-ACC
               ,:TCL-IMP-RIT-VIS
                :IND-TCL-IMP-RIT-VIS
               ,:TCL-IMPB-TFR
                :IND-TCL-IMPB-TFR
               ,:TCL-IMPB-ACC
                :IND-TCL-IMPB-ACC
               ,:TCL-IMPB-VIS
                :IND-TCL-IMPB-VIS
               ,:TCL-IMP-RIMB
                :IND-TCL-IMP-RIMB
               ,:TCL-IMP-CORTVO
                :IND-TCL-IMP-CORTVO
               ,:TCL-IMPB-IMPST-PRVR
                :IND-TCL-IMPB-IMPST-PRVR
               ,:TCL-IMPST-PRVR
                :IND-TCL-IMPST-PRVR
               ,:TCL-IMPB-IMPST-252
                :IND-TCL-IMPB-IMPST-252
               ,:TCL-IMPST-252
                :IND-TCL-IMPST-252
               ,:TCL-IMP-IS
                :IND-TCL-IMP-IS
               ,:TCL-IMP-DIR-LIQ
                :IND-TCL-IMP-DIR-LIQ
               ,:TCL-IMP-NET-LIQTO
                :IND-TCL-IMP-NET-LIQTO
               ,:TCL-IMP-EFFLQ
                :IND-TCL-IMP-EFFLQ
               ,:TCL-COD-DVS
                :IND-TCL-COD-DVS
               ,:TCL-TP-MEZ-PAG-ACCR
                :IND-TCL-TP-MEZ-PAG-ACCR
               ,:TCL-ESTR-CNT-CORR-ACCR
                :IND-TCL-ESTR-CNT-CORR-ACCR
               ,:TCL-DS-RIGA
               ,:TCL-DS-OPER-SQL
               ,:TCL-DS-VER
               ,:TCL-DS-TS-INI-CPTZ
               ,:TCL-DS-TS-END-CPTZ
               ,:TCL-DS-UTENTE
               ,:TCL-DS-STATO-ELAB
               ,:TCL-TP-STAT-TIT
                :IND-TCL-TP-STAT-TIT
               ,:TCL-IMPB-VIS-1382011
                :IND-TCL-IMPB-VIS-1382011
               ,:TCL-IMPST-VIS-1382011
                :IND-TCL-IMPST-VIS-1382011
               ,:TCL-IMPST-SOST-1382011
                :IND-TCL-IMPST-SOST-1382011
               ,:TCL-IMP-INTR-RIT-PAG
                :IND-TCL-IMP-INTR-RIT-PAG
               ,:TCL-IMPB-IS
                :IND-TCL-IMPB-IS
               ,:TCL-IMPB-IS-1382011
                :IND-TCL-IMPB-IS-1382011
               ,:TCL-IMPB-VIS-662014
                :IND-TCL-IMPB-VIS-662014
               ,:TCL-IMPST-VIS-662014
                :IND-TCL-IMPST-VIS-662014
               ,:TCL-IMPB-IS-662014
                :IND-TCL-IMPB-IS-662014
               ,:TCL-IMPST-SOST-662014
                :IND-TCL-IMPST-SOST-662014
             FROM TCONT_LIQ
             WHERE     ID_TCONT_LIQ = :TCL-ID-TCONT-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE TCONT_LIQ SET

                   ID_TCONT_LIQ           =
                :TCL-ID-TCONT-LIQ
                  ,ID_PERC_LIQ            =
                :TCL-ID-PERC-LIQ
                  ,ID_BNFICR_LIQ          =
                :TCL-ID-BNFICR-LIQ
                  ,ID_MOVI_CRZ            =
                :TCL-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :TCL-ID-MOVI-CHIU
                                       :IND-TCL-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :TCL-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :TCL-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :TCL-COD-COMP-ANIA
                  ,DT_VLT                 =
           :TCL-DT-VLT-DB
                                       :IND-TCL-DT-VLT
                  ,IMP_LRD_LIQTO          =
                :TCL-IMP-LRD-LIQTO
                                       :IND-TCL-IMP-LRD-LIQTO
                  ,IMP_PREST              =
                :TCL-IMP-PREST
                                       :IND-TCL-IMP-PREST
                  ,IMP_INTR_PREST         =
                :TCL-IMP-INTR-PREST
                                       :IND-TCL-IMP-INTR-PREST
                  ,IMP_RAT                =
                :TCL-IMP-RAT
                                       :IND-TCL-IMP-RAT
                  ,IMP_UTI                =
                :TCL-IMP-UTI
                                       :IND-TCL-IMP-UTI
                  ,IMP_RIT_TFR            =
                :TCL-IMP-RIT-TFR
                                       :IND-TCL-IMP-RIT-TFR
                  ,IMP_RIT_ACC            =
                :TCL-IMP-RIT-ACC
                                       :IND-TCL-IMP-RIT-ACC
                  ,IMP_RIT_VIS            =
                :TCL-IMP-RIT-VIS
                                       :IND-TCL-IMP-RIT-VIS
                  ,IMPB_TFR               =
                :TCL-IMPB-TFR
                                       :IND-TCL-IMPB-TFR
                  ,IMPB_ACC               =
                :TCL-IMPB-ACC
                                       :IND-TCL-IMPB-ACC
                  ,IMPB_VIS               =
                :TCL-IMPB-VIS
                                       :IND-TCL-IMPB-VIS
                  ,IMP_RIMB               =
                :TCL-IMP-RIMB
                                       :IND-TCL-IMP-RIMB
                  ,IMP_CORTVO             =
                :TCL-IMP-CORTVO
                                       :IND-TCL-IMP-CORTVO
                  ,IMPB_IMPST_PRVR        =
                :TCL-IMPB-IMPST-PRVR
                                       :IND-TCL-IMPB-IMPST-PRVR
                  ,IMPST_PRVR             =
                :TCL-IMPST-PRVR
                                       :IND-TCL-IMPST-PRVR
                  ,IMPB_IMPST_252         =
                :TCL-IMPB-IMPST-252
                                       :IND-TCL-IMPB-IMPST-252
                  ,IMPST_252              =
                :TCL-IMPST-252
                                       :IND-TCL-IMPST-252
                  ,IMP_IS                 =
                :TCL-IMP-IS
                                       :IND-TCL-IMP-IS
                  ,IMP_DIR_LIQ            =
                :TCL-IMP-DIR-LIQ
                                       :IND-TCL-IMP-DIR-LIQ
                  ,IMP_NET_LIQTO          =
                :TCL-IMP-NET-LIQTO
                                       :IND-TCL-IMP-NET-LIQTO
                  ,IMP_EFFLQ              =
                :TCL-IMP-EFFLQ
                                       :IND-TCL-IMP-EFFLQ
                  ,COD_DVS                =
                :TCL-COD-DVS
                                       :IND-TCL-COD-DVS
                  ,TP_MEZ_PAG_ACCR        =
                :TCL-TP-MEZ-PAG-ACCR
                                       :IND-TCL-TP-MEZ-PAG-ACCR
                  ,ESTR_CNT_CORR_ACCR     =
                :TCL-ESTR-CNT-CORR-ACCR
                                       :IND-TCL-ESTR-CNT-CORR-ACCR
                  ,DS_RIGA                =
                :TCL-DS-RIGA
                  ,DS_OPER_SQL            =
                :TCL-DS-OPER-SQL
                  ,DS_VER                 =
                :TCL-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :TCL-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :TCL-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :TCL-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :TCL-DS-STATO-ELAB
                  ,TP_STAT_TIT            =
                :TCL-TP-STAT-TIT
                                       :IND-TCL-TP-STAT-TIT
                  ,IMPB_VIS_1382011       =
                :TCL-IMPB-VIS-1382011
                                       :IND-TCL-IMPB-VIS-1382011
                  ,IMPST_VIS_1382011      =
                :TCL-IMPST-VIS-1382011
                                       :IND-TCL-IMPST-VIS-1382011
                  ,IMPST_SOST_1382011     =
                :TCL-IMPST-SOST-1382011
                                       :IND-TCL-IMPST-SOST-1382011
                  ,IMP_INTR_RIT_PAG       =
                :TCL-IMP-INTR-RIT-PAG
                                       :IND-TCL-IMP-INTR-RIT-PAG
                  ,IMPB_IS                =
                :TCL-IMPB-IS
                                       :IND-TCL-IMPB-IS
                  ,IMPB_IS_1382011        =
                :TCL-IMPB-IS-1382011
                                       :IND-TCL-IMPB-IS-1382011
                  ,IMPB_VIS_662014        =
                :TCL-IMPB-VIS-662014
                                       :IND-TCL-IMPB-VIS-662014
                  ,IMPST_VIS_662014       =
                :TCL-IMPST-VIS-662014
                                       :IND-TCL-IMPST-VIS-662014
                  ,IMPB_IS_662014         =
                :TCL-IMPB-IS-662014
                                       :IND-TCL-IMPB-IS-662014
                  ,IMPST_SOST_662014      =
                :TCL-IMPST-SOST-662014
                                       :IND-TCL-IMPST-SOST-662014
                WHERE     DS_RIGA = :TCL-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-TCL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-TCL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-TCL
           INTO
                :TCL-ID-TCONT-LIQ
               ,:TCL-ID-PERC-LIQ
               ,:TCL-ID-BNFICR-LIQ
               ,:TCL-ID-MOVI-CRZ
               ,:TCL-ID-MOVI-CHIU
                :IND-TCL-ID-MOVI-CHIU
               ,:TCL-DT-INI-EFF-DB
               ,:TCL-DT-END-EFF-DB
               ,:TCL-COD-COMP-ANIA
               ,:TCL-DT-VLT-DB
                :IND-TCL-DT-VLT
               ,:TCL-IMP-LRD-LIQTO
                :IND-TCL-IMP-LRD-LIQTO
               ,:TCL-IMP-PREST
                :IND-TCL-IMP-PREST
               ,:TCL-IMP-INTR-PREST
                :IND-TCL-IMP-INTR-PREST
               ,:TCL-IMP-RAT
                :IND-TCL-IMP-RAT
               ,:TCL-IMP-UTI
                :IND-TCL-IMP-UTI
               ,:TCL-IMP-RIT-TFR
                :IND-TCL-IMP-RIT-TFR
               ,:TCL-IMP-RIT-ACC
                :IND-TCL-IMP-RIT-ACC
               ,:TCL-IMP-RIT-VIS
                :IND-TCL-IMP-RIT-VIS
               ,:TCL-IMPB-TFR
                :IND-TCL-IMPB-TFR
               ,:TCL-IMPB-ACC
                :IND-TCL-IMPB-ACC
               ,:TCL-IMPB-VIS
                :IND-TCL-IMPB-VIS
               ,:TCL-IMP-RIMB
                :IND-TCL-IMP-RIMB
               ,:TCL-IMP-CORTVO
                :IND-TCL-IMP-CORTVO
               ,:TCL-IMPB-IMPST-PRVR
                :IND-TCL-IMPB-IMPST-PRVR
               ,:TCL-IMPST-PRVR
                :IND-TCL-IMPST-PRVR
               ,:TCL-IMPB-IMPST-252
                :IND-TCL-IMPB-IMPST-252
               ,:TCL-IMPST-252
                :IND-TCL-IMPST-252
               ,:TCL-IMP-IS
                :IND-TCL-IMP-IS
               ,:TCL-IMP-DIR-LIQ
                :IND-TCL-IMP-DIR-LIQ
               ,:TCL-IMP-NET-LIQTO
                :IND-TCL-IMP-NET-LIQTO
               ,:TCL-IMP-EFFLQ
                :IND-TCL-IMP-EFFLQ
               ,:TCL-COD-DVS
                :IND-TCL-COD-DVS
               ,:TCL-TP-MEZ-PAG-ACCR
                :IND-TCL-TP-MEZ-PAG-ACCR
               ,:TCL-ESTR-CNT-CORR-ACCR
                :IND-TCL-ESTR-CNT-CORR-ACCR
               ,:TCL-DS-RIGA
               ,:TCL-DS-OPER-SQL
               ,:TCL-DS-VER
               ,:TCL-DS-TS-INI-CPTZ
               ,:TCL-DS-TS-END-CPTZ
               ,:TCL-DS-UTENTE
               ,:TCL-DS-STATO-ELAB
               ,:TCL-TP-STAT-TIT
                :IND-TCL-TP-STAT-TIT
               ,:TCL-IMPB-VIS-1382011
                :IND-TCL-IMPB-VIS-1382011
               ,:TCL-IMPST-VIS-1382011
                :IND-TCL-IMPST-VIS-1382011
               ,:TCL-IMPST-SOST-1382011
                :IND-TCL-IMPST-SOST-1382011
               ,:TCL-IMP-INTR-RIT-PAG
                :IND-TCL-IMP-INTR-RIT-PAG
               ,:TCL-IMPB-IS
                :IND-TCL-IMPB-IS
               ,:TCL-IMPB-IS-1382011
                :IND-TCL-IMPB-IS-1382011
               ,:TCL-IMPB-VIS-662014
                :IND-TCL-IMPB-VIS-662014
               ,:TCL-IMPST-VIS-662014
                :IND-TCL-IMPST-VIS-662014
               ,:TCL-IMPB-IS-662014
                :IND-TCL-IMPB-IS-662014
               ,:TCL-IMPST-SOST-662014
                :IND-TCL-IMPST-SOST-662014
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TCONT_LIQ
                ,ID_PERC_LIQ
                ,ID_BNFICR_LIQ
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_VLT
                ,IMP_LRD_LIQTO
                ,IMP_PREST
                ,IMP_INTR_PREST
                ,IMP_RAT
                ,IMP_UTI
                ,IMP_RIT_TFR
                ,IMP_RIT_ACC
                ,IMP_RIT_VIS
                ,IMPB_TFR
                ,IMPB_ACC
                ,IMPB_VIS
                ,IMP_RIMB
                ,IMP_CORTVO
                ,IMPB_IMPST_PRVR
                ,IMPST_PRVR
                ,IMPB_IMPST_252
                ,IMPST_252
                ,IMP_IS
                ,IMP_DIR_LIQ
                ,IMP_NET_LIQTO
                ,IMP_EFFLQ
                ,COD_DVS
                ,TP_MEZ_PAG_ACCR
                ,ESTR_CNT_CORR_ACCR
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,TP_STAT_TIT
                ,IMPB_VIS_1382011
                ,IMPST_VIS_1382011
                ,IMPST_SOST_1382011
                ,IMP_INTR_RIT_PAG
                ,IMPB_IS
                ,IMPB_IS_1382011
                ,IMPB_VIS_662014
                ,IMPST_VIS_662014
                ,IMPB_IS_662014
                ,IMPST_SOST_662014
             INTO
                :TCL-ID-TCONT-LIQ
               ,:TCL-ID-PERC-LIQ
               ,:TCL-ID-BNFICR-LIQ
               ,:TCL-ID-MOVI-CRZ
               ,:TCL-ID-MOVI-CHIU
                :IND-TCL-ID-MOVI-CHIU
               ,:TCL-DT-INI-EFF-DB
               ,:TCL-DT-END-EFF-DB
               ,:TCL-COD-COMP-ANIA
               ,:TCL-DT-VLT-DB
                :IND-TCL-DT-VLT
               ,:TCL-IMP-LRD-LIQTO
                :IND-TCL-IMP-LRD-LIQTO
               ,:TCL-IMP-PREST
                :IND-TCL-IMP-PREST
               ,:TCL-IMP-INTR-PREST
                :IND-TCL-IMP-INTR-PREST
               ,:TCL-IMP-RAT
                :IND-TCL-IMP-RAT
               ,:TCL-IMP-UTI
                :IND-TCL-IMP-UTI
               ,:TCL-IMP-RIT-TFR
                :IND-TCL-IMP-RIT-TFR
               ,:TCL-IMP-RIT-ACC
                :IND-TCL-IMP-RIT-ACC
               ,:TCL-IMP-RIT-VIS
                :IND-TCL-IMP-RIT-VIS
               ,:TCL-IMPB-TFR
                :IND-TCL-IMPB-TFR
               ,:TCL-IMPB-ACC
                :IND-TCL-IMPB-ACC
               ,:TCL-IMPB-VIS
                :IND-TCL-IMPB-VIS
               ,:TCL-IMP-RIMB
                :IND-TCL-IMP-RIMB
               ,:TCL-IMP-CORTVO
                :IND-TCL-IMP-CORTVO
               ,:TCL-IMPB-IMPST-PRVR
                :IND-TCL-IMPB-IMPST-PRVR
               ,:TCL-IMPST-PRVR
                :IND-TCL-IMPST-PRVR
               ,:TCL-IMPB-IMPST-252
                :IND-TCL-IMPB-IMPST-252
               ,:TCL-IMPST-252
                :IND-TCL-IMPST-252
               ,:TCL-IMP-IS
                :IND-TCL-IMP-IS
               ,:TCL-IMP-DIR-LIQ
                :IND-TCL-IMP-DIR-LIQ
               ,:TCL-IMP-NET-LIQTO
                :IND-TCL-IMP-NET-LIQTO
               ,:TCL-IMP-EFFLQ
                :IND-TCL-IMP-EFFLQ
               ,:TCL-COD-DVS
                :IND-TCL-COD-DVS
               ,:TCL-TP-MEZ-PAG-ACCR
                :IND-TCL-TP-MEZ-PAG-ACCR
               ,:TCL-ESTR-CNT-CORR-ACCR
                :IND-TCL-ESTR-CNT-CORR-ACCR
               ,:TCL-DS-RIGA
               ,:TCL-DS-OPER-SQL
               ,:TCL-DS-VER
               ,:TCL-DS-TS-INI-CPTZ
               ,:TCL-DS-TS-END-CPTZ
               ,:TCL-DS-UTENTE
               ,:TCL-DS-STATO-ELAB
               ,:TCL-TP-STAT-TIT
                :IND-TCL-TP-STAT-TIT
               ,:TCL-IMPB-VIS-1382011
                :IND-TCL-IMPB-VIS-1382011
               ,:TCL-IMPST-VIS-1382011
                :IND-TCL-IMPST-VIS-1382011
               ,:TCL-IMPST-SOST-1382011
                :IND-TCL-IMPST-SOST-1382011
               ,:TCL-IMP-INTR-RIT-PAG
                :IND-TCL-IMP-INTR-RIT-PAG
               ,:TCL-IMPB-IS
                :IND-TCL-IMPB-IS
               ,:TCL-IMPB-IS-1382011
                :IND-TCL-IMPB-IS-1382011
               ,:TCL-IMPB-VIS-662014
                :IND-TCL-IMPB-VIS-662014
               ,:TCL-IMPST-VIS-662014
                :IND-TCL-IMPST-VIS-662014
               ,:TCL-IMPB-IS-662014
                :IND-TCL-IMPB-IS-662014
               ,:TCL-IMPST-SOST-662014
                :IND-TCL-IMPST-SOST-662014
             FROM TCONT_LIQ
             WHERE     ID_TCONT_LIQ = :TCL-ID-TCONT-LIQ
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-TCL-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO TCL-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-TCL-DT-VLT = -1
              MOVE HIGH-VALUES TO TCL-DT-VLT-NULL
           END-IF
           IF IND-TCL-IMP-LRD-LIQTO = -1
              MOVE HIGH-VALUES TO TCL-IMP-LRD-LIQTO-NULL
           END-IF
           IF IND-TCL-IMP-PREST = -1
              MOVE HIGH-VALUES TO TCL-IMP-PREST-NULL
           END-IF
           IF IND-TCL-IMP-INTR-PREST = -1
              MOVE HIGH-VALUES TO TCL-IMP-INTR-PREST-NULL
           END-IF
           IF IND-TCL-IMP-RAT = -1
              MOVE HIGH-VALUES TO TCL-IMP-RAT-NULL
           END-IF
           IF IND-TCL-IMP-UTI = -1
              MOVE HIGH-VALUES TO TCL-IMP-UTI-NULL
           END-IF
           IF IND-TCL-IMP-RIT-TFR = -1
              MOVE HIGH-VALUES TO TCL-IMP-RIT-TFR-NULL
           END-IF
           IF IND-TCL-IMP-RIT-ACC = -1
              MOVE HIGH-VALUES TO TCL-IMP-RIT-ACC-NULL
           END-IF
           IF IND-TCL-IMP-RIT-VIS = -1
              MOVE HIGH-VALUES TO TCL-IMP-RIT-VIS-NULL
           END-IF
           IF IND-TCL-IMPB-TFR = -1
              MOVE HIGH-VALUES TO TCL-IMPB-TFR-NULL
           END-IF
           IF IND-TCL-IMPB-ACC = -1
              MOVE HIGH-VALUES TO TCL-IMPB-ACC-NULL
           END-IF
           IF IND-TCL-IMPB-VIS = -1
              MOVE HIGH-VALUES TO TCL-IMPB-VIS-NULL
           END-IF
           IF IND-TCL-IMP-RIMB = -1
              MOVE HIGH-VALUES TO TCL-IMP-RIMB-NULL
           END-IF
           IF IND-TCL-IMP-CORTVO = -1
              MOVE HIGH-VALUES TO TCL-IMP-CORTVO-NULL
           END-IF
           IF IND-TCL-IMPB-IMPST-PRVR = -1
              MOVE HIGH-VALUES TO TCL-IMPB-IMPST-PRVR-NULL
           END-IF
           IF IND-TCL-IMPST-PRVR = -1
              MOVE HIGH-VALUES TO TCL-IMPST-PRVR-NULL
           END-IF
           IF IND-TCL-IMPB-IMPST-252 = -1
              MOVE HIGH-VALUES TO TCL-IMPB-IMPST-252-NULL
           END-IF
           IF IND-TCL-IMPST-252 = -1
              MOVE HIGH-VALUES TO TCL-IMPST-252-NULL
           END-IF
           IF IND-TCL-IMP-IS = -1
              MOVE HIGH-VALUES TO TCL-IMP-IS-NULL
           END-IF
           IF IND-TCL-IMP-DIR-LIQ = -1
              MOVE HIGH-VALUES TO TCL-IMP-DIR-LIQ-NULL
           END-IF
           IF IND-TCL-IMP-NET-LIQTO = -1
              MOVE HIGH-VALUES TO TCL-IMP-NET-LIQTO-NULL
           END-IF
           IF IND-TCL-IMP-EFFLQ = -1
              MOVE HIGH-VALUES TO TCL-IMP-EFFLQ-NULL
           END-IF
           IF IND-TCL-COD-DVS = -1
              MOVE HIGH-VALUES TO TCL-COD-DVS-NULL
           END-IF
           IF IND-TCL-TP-MEZ-PAG-ACCR = -1
              MOVE HIGH-VALUES TO TCL-TP-MEZ-PAG-ACCR-NULL
           END-IF
           IF IND-TCL-ESTR-CNT-CORR-ACCR = -1
              MOVE HIGH-VALUES TO TCL-ESTR-CNT-CORR-ACCR-NULL
           END-IF
           IF IND-TCL-TP-STAT-TIT = -1
              MOVE HIGH-VALUES TO TCL-TP-STAT-TIT-NULL
           END-IF
           IF IND-TCL-IMPB-VIS-1382011 = -1
              MOVE HIGH-VALUES TO TCL-IMPB-VIS-1382011-NULL
           END-IF
           IF IND-TCL-IMPST-VIS-1382011 = -1
              MOVE HIGH-VALUES TO TCL-IMPST-VIS-1382011-NULL
           END-IF
           IF IND-TCL-IMPST-SOST-1382011 = -1
              MOVE HIGH-VALUES TO TCL-IMPST-SOST-1382011-NULL
           END-IF
           IF IND-TCL-IMP-INTR-RIT-PAG = -1
              MOVE HIGH-VALUES TO TCL-IMP-INTR-RIT-PAG-NULL
           END-IF
           IF IND-TCL-IMPB-IS = -1
              MOVE HIGH-VALUES TO TCL-IMPB-IS-NULL
           END-IF
           IF IND-TCL-IMPB-IS-1382011 = -1
              MOVE HIGH-VALUES TO TCL-IMPB-IS-1382011-NULL
           END-IF
           IF IND-TCL-IMPB-VIS-662014 = -1
              MOVE HIGH-VALUES TO TCL-IMPB-VIS-662014-NULL
           END-IF
           IF IND-TCL-IMPST-VIS-662014 = -1
              MOVE HIGH-VALUES TO TCL-IMPST-VIS-662014-NULL
           END-IF
           IF IND-TCL-IMPB-IS-662014 = -1
              MOVE HIGH-VALUES TO TCL-IMPB-IS-662014-NULL
           END-IF
           IF IND-TCL-IMPST-SOST-662014 = -1
              MOVE HIGH-VALUES TO TCL-IMPST-SOST-662014-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO TCL-DS-OPER-SQL
           MOVE 0                   TO TCL-DS-VER
           MOVE IDSV0003-USER-NAME TO TCL-DS-UTENTE
           MOVE '1'                   TO TCL-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO TCL-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO TCL-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF TCL-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-TCL-ID-MOVI-CHIU
           END-IF
           IF TCL-DT-VLT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-DT-VLT
           ELSE
              MOVE 0 TO IND-TCL-DT-VLT
           END-IF
           IF TCL-IMP-LRD-LIQTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMP-LRD-LIQTO
           ELSE
              MOVE 0 TO IND-TCL-IMP-LRD-LIQTO
           END-IF
           IF TCL-IMP-PREST-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMP-PREST
           ELSE
              MOVE 0 TO IND-TCL-IMP-PREST
           END-IF
           IF TCL-IMP-INTR-PREST-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMP-INTR-PREST
           ELSE
              MOVE 0 TO IND-TCL-IMP-INTR-PREST
           END-IF
           IF TCL-IMP-RAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMP-RAT
           ELSE
              MOVE 0 TO IND-TCL-IMP-RAT
           END-IF
           IF TCL-IMP-UTI-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMP-UTI
           ELSE
              MOVE 0 TO IND-TCL-IMP-UTI
           END-IF
           IF TCL-IMP-RIT-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMP-RIT-TFR
           ELSE
              MOVE 0 TO IND-TCL-IMP-RIT-TFR
           END-IF
           IF TCL-IMP-RIT-ACC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMP-RIT-ACC
           ELSE
              MOVE 0 TO IND-TCL-IMP-RIT-ACC
           END-IF
           IF TCL-IMP-RIT-VIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMP-RIT-VIS
           ELSE
              MOVE 0 TO IND-TCL-IMP-RIT-VIS
           END-IF
           IF TCL-IMPB-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPB-TFR
           ELSE
              MOVE 0 TO IND-TCL-IMPB-TFR
           END-IF
           IF TCL-IMPB-ACC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPB-ACC
           ELSE
              MOVE 0 TO IND-TCL-IMPB-ACC
           END-IF
           IF TCL-IMPB-VIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPB-VIS
           ELSE
              MOVE 0 TO IND-TCL-IMPB-VIS
           END-IF
           IF TCL-IMP-RIMB-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMP-RIMB
           ELSE
              MOVE 0 TO IND-TCL-IMP-RIMB
           END-IF
           IF TCL-IMP-CORTVO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMP-CORTVO
           ELSE
              MOVE 0 TO IND-TCL-IMP-CORTVO
           END-IF
           IF TCL-IMPB-IMPST-PRVR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPB-IMPST-PRVR
           ELSE
              MOVE 0 TO IND-TCL-IMPB-IMPST-PRVR
           END-IF
           IF TCL-IMPST-PRVR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPST-PRVR
           ELSE
              MOVE 0 TO IND-TCL-IMPST-PRVR
           END-IF
           IF TCL-IMPB-IMPST-252-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPB-IMPST-252
           ELSE
              MOVE 0 TO IND-TCL-IMPB-IMPST-252
           END-IF
           IF TCL-IMPST-252-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPST-252
           ELSE
              MOVE 0 TO IND-TCL-IMPST-252
           END-IF
           IF TCL-IMP-IS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMP-IS
           ELSE
              MOVE 0 TO IND-TCL-IMP-IS
           END-IF
           IF TCL-IMP-DIR-LIQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMP-DIR-LIQ
           ELSE
              MOVE 0 TO IND-TCL-IMP-DIR-LIQ
           END-IF
           IF TCL-IMP-NET-LIQTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMP-NET-LIQTO
           ELSE
              MOVE 0 TO IND-TCL-IMP-NET-LIQTO
           END-IF
           IF TCL-IMP-EFFLQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMP-EFFLQ
           ELSE
              MOVE 0 TO IND-TCL-IMP-EFFLQ
           END-IF
           IF TCL-COD-DVS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-COD-DVS
           ELSE
              MOVE 0 TO IND-TCL-COD-DVS
           END-IF
           IF TCL-TP-MEZ-PAG-ACCR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-TP-MEZ-PAG-ACCR
           ELSE
              MOVE 0 TO IND-TCL-TP-MEZ-PAG-ACCR
           END-IF
           IF TCL-ESTR-CNT-CORR-ACCR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-ESTR-CNT-CORR-ACCR
           ELSE
              MOVE 0 TO IND-TCL-ESTR-CNT-CORR-ACCR
           END-IF
           IF TCL-TP-STAT-TIT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-TP-STAT-TIT
           ELSE
              MOVE 0 TO IND-TCL-TP-STAT-TIT
           END-IF
           IF TCL-IMPB-VIS-1382011-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPB-VIS-1382011
           ELSE
              MOVE 0 TO IND-TCL-IMPB-VIS-1382011
           END-IF
           IF TCL-IMPST-VIS-1382011-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPST-VIS-1382011
           ELSE
              MOVE 0 TO IND-TCL-IMPST-VIS-1382011
           END-IF
           IF TCL-IMPST-SOST-1382011-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPST-SOST-1382011
           ELSE
              MOVE 0 TO IND-TCL-IMPST-SOST-1382011
           END-IF
           IF TCL-IMP-INTR-RIT-PAG-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMP-INTR-RIT-PAG
           ELSE
              MOVE 0 TO IND-TCL-IMP-INTR-RIT-PAG
           END-IF
           IF TCL-IMPB-IS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPB-IS
           ELSE
              MOVE 0 TO IND-TCL-IMPB-IS
           END-IF
           IF TCL-IMPB-IS-1382011-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPB-IS-1382011
           ELSE
              MOVE 0 TO IND-TCL-IMPB-IS-1382011
           END-IF
           IF TCL-IMPB-VIS-662014-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPB-VIS-662014
           ELSE
              MOVE 0 TO IND-TCL-IMPB-VIS-662014
           END-IF
           IF TCL-IMPST-VIS-662014-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPST-VIS-662014
           ELSE
              MOVE 0 TO IND-TCL-IMPST-VIS-662014
           END-IF
           IF TCL-IMPB-IS-662014-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPB-IS-662014
           ELSE
              MOVE 0 TO IND-TCL-IMPB-IS-662014
           END-IF
           IF TCL-IMPST-SOST-662014-NULL = HIGH-VALUES
              MOVE -1 TO IND-TCL-IMPST-SOST-662014
           ELSE
              MOVE 0 TO IND-TCL-IMPST-SOST-662014
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : TCL-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE TCONT-LIQ TO WS-BUFFER-TABLE.

           MOVE TCL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO TCL-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO TCL-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO TCL-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO TCL-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO TCL-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO TCL-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO TCL-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE TCONT-LIQ TO WS-BUFFER-TABLE.

           MOVE TCL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO TCONT-LIQ.

           MOVE WS-ID-MOVI-CRZ  TO TCL-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO TCL-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO TCL-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO TCL-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO TCL-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO TCL-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO TCL-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE TCL-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO TCL-DT-INI-EFF-DB
           MOVE TCL-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO TCL-DT-END-EFF-DB
           IF IND-TCL-DT-VLT = 0
               MOVE TCL-DT-VLT TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TCL-DT-VLT-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE TCL-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO TCL-DT-INI-EFF
           MOVE TCL-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO TCL-DT-END-EFF
           IF IND-TCL-DT-VLT = 0
               MOVE TCL-DT-VLT-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TCL-DT-VLT
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

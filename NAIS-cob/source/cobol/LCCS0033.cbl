      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA VER. 1.0                          **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LCCS0033.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2009.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *    PROGRAMMA .....
      *    TIPOLOGIA...... COMPONENTE COMUNE
      *    PROCESSO....... XXX
      *    FUNZIONE....... XXX
      *    DESCRIZIONE.... VERIFICA/ESTRAZIONE POLIZZE PER RECUPERO
      *                    PROVVIGGIONALE
      *    PAGINA WEB.....
      *----------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01 WK-PGM                           PIC X(08) VALUE 'LCCS0033'.
       01 WK-TABELLA                       PIC X(08) VALUE SPACES.
       01 WS-DT-INFINITO-1                 PIC 9(08) VALUE 99991230.
       01 WK-ID-MAX                        PIC S9(9) COMP-3
                                                     VALUE 999999999.
       01 WK-MAX-POL-RECUP-PROVV           PIC 9(02) VALUE 15.


      *----------------------------------------------------------------*
      *    Moduli chiamati
      *----------------------------------------------------------------*
       01  LCCS0003                         PIC X(8) VALUE 'LCCS0003'.
      *----------------------------------------------------------------*
      *    Aree Input/Output servizi
      *----------------------------------------------------------------*
           COPY LCCC0003.
       01 IN-RCODE                       PIC 9(2) VALUE ZEROES.

      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       01  DATA-INF-AMG                    PIC 9(08).
       01  DATA-INFERIORE REDEFINES DATA-INF-AMG.
           05 AAAA-INF                     PIC 9(04).
           05 MM-INF                       PIC 9(02).
           05 GG-INF                       PIC 9(02).

       01  DATA-SUP-AMG                    PIC 9(08).
       01  DATA-SUPERIORE REDEFINES DATA-SUP-AMG.
           05 AAAA-SUP                     PIC 9(04).
           05 MM-SUP                       PIC 9(02).
           05 GG-SUP                       PIC 9(02).
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01 WS-VARIABILI.
          03  WK-ID-OGG                    PIC S9(09)  COMP-3.
          03  WS-PREMIO-ANNUO-TGA          PIC S9(12)V9(3) COMP-3.
          03  WS-PREMIO-ANNUO-CALC         PIC S9(12)V9(3) COMP-3.

      *----------------------------------------------------------------*
      *    FLAG/SWITCH
      *----------------------------------------------------------------*
       01 FL-CICLO-POL                     PIC X(01).
          88 FINE-CICLO-POL-SI               VALUE 'S'.
          88 FINE-CICLO-POL-NO               VALUE 'N'.

       01 FL-CICLO-GAR                     PIC X(01).
          88 FINE-CICLO-GAR-SI               VALUE 'S'.
          88 FINE-CICLO-GAR-NO               VALUE 'N'.

       01 FL-CICLO-TGA                     PIC X(01).
          88 FINE-CICLO-TGA-SI               VALUE 'S'.
          88 FINE-CICLO-TGA-NO               VALUE 'N'.

       01 WK-RICERCA                       PIC X(001).
          88 WK-TROVATO                      VALUE 'S'.
          88 WK-NON-TROVATO                  VALUE 'N'.

       01 WS-CONTRAENTE                    PIC X(01).
          88 WS-CONTRAENTE-SI                VALUE 'S'.
          88 WS-CONTRAENTE-NO                VALUE 'N'.

       01 WS-GARANZIA-BASE                 PIC X(01).
          88 WS-GARANZIA-BASE-SI             VALUE 'S'.
          88 WS-GARANZIA-BASE-NO             VALUE 'N'.

       01 FL-SCARTO-POL                     PIC X(01).
          88 SCARTO-POL-SI                    VALUE 'S'.
          88 SCARTO-POL-NO                    VALUE 'N'.

      *----------------------------------------------------------------*
      *--  Tipologiche di PTF
      *----------------------------------------------------------------*
      *--  TP_RAPP_ANA
           COPY LCCVXRA0.
      *--  TP_STAT_BUS
           COPY LCCVXSB0.
      *--  TP_CAUS
           COPY LCCVXCA0.
      *--  TP_OGG
           COPY LCCVXOG0.
      *--  TP_COLLGM
           COPY LCCVXCO0.

      *----------------------------------------------------------------*
      *--  Tipologiche di ACT
      *----------------------------------------------------------------*
       01  ACT-TP-GARANZIA                  PIC 9(001) VALUE ZERO.
           88 TP-GAR-BASE                              VALUE 1.
           88 TP-GAR-COMPLEM                           VALUE 2.
           88 TP-GAR-OPZIONE                           VALUE 3.
           88 TP-GAR-ACCESSORIA                        VALUE 4.
           88 TP-GAR-DIFFERIMENTO                      VALUE 5.
           88 TP-GAR-CEDOLA                            VALUE 6.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    COPY DISPATCHER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
           COPY IDSI0011.
           COPY IDSO0011.
      *----------------------------------------------------------------*
      *    COPY TABELLE DB2
      *----------------------------------------------------------------*
           COPY IDBVPCO1.
           COPY IDBVPOL1.
           COPY IDBVADE1.
           COPY IDBVGRZ1.
           COPY IDBVTGA1.
           COPY IDBVSTB1.
      *----------------------------------------------------------------*
      *--  COPY LDBS
      *----------------------------------------------------------------*
      *--  Interfaccia LDBS5560 - Estrazione Polizze stesso contraente
           COPY LDBV5561.
      *--  Interfaccia LDBS5570 - Sommatoria Imp_collegato dalla tab.
      *--  Oggetto collegato
           COPY LDBV5571.
      *--  Interfaccia LDBSD510 - Estrazione delle tranche di polizza
      *--  in un determinato stato e causale (Sostituisce LDBS4030)
           COPY LDBVD511.


      *----------------------------------------------------------------*
      *    DEFINIZIONE INDICI
      *----------------------------------------------------------------*
       01  WS-INDICI.
           03 IX-TAB-PVT                 PIC S9(04) COMP.
           03 IX-TAB-PAG                 PIC S9(04) COMP.
           03 IX-GAR-TARI                PIC S9(04) COMP.
           03 IX-RIC-RAN                 PIC S9(04) COMP.
      *---------------------------------------------------------------*
       LINKAGE SECTION.
      *---------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.

       01  WCOM-AREA-STATI.
           COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.

       01  AREA-IO-LCCS0033.
           COPY LCCC0033.

      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                AREA-IO-LCCS0033.

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE WS-INDICI
                      WS-VARIABILI
                      LCCC0033-DATI-OUTPUT.

           PERFORM S0005-CTRL-DATI-INPUT
              THRU EX-S0005.

           IF IDSV0001-ESITO-OK
              PERFORM S0010-LETTURA-PCO THRU S0010-EX
           END-IF.

           SET CONTRAENTE       TO TRUE
           SET WS-CONTRAENTE-NO TO TRUE

           IF LCCC0033-TP-RAPP-ANA = WS-TP-RAPP-ANA
              SET WS-CONTRAENTE-SI TO TRUE
           END-IF.


       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *  CONTROLLI DATI INPUT                                          *
      *----------------------------------------------------------------*
       S0005-CTRL-DATI-INPUT.

           CONTINUE.

       EX-S0005.
           EXIT.
      *----------------------------------------------------------------*
      *    Lettura in ptf della parametro compagnia
      *----------------------------------------------------------------*
       S0010-LETTURA-PCO.

           MOVE HIGH-VALUE              TO PARAM-COMP.

           MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO
                                           IDSI0011-DATA-FINE-EFFETTO
                                           IDSI0011-DATA-COMPETENZA.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA TO PCO-COD-COMP-ANIA.

           MOVE 'PARAM-COMP'                TO IDSI0011-CODICE-STR-DATO
                                               WK-TABELLA.
           MOVE PARAM-COMP                  TO IDSI0011-BUFFER-DATI.


           SET IDSI0011-SELECT              TO TRUE.
           SET IDSI0011-PRIMARY-KEY         TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR    TO TRUE.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       MOVE WK-PGM          TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S0010-LETTURA-PCO'
                                            TO IEAI9901-LABEL-ERR
                       MOVE '005069'        TO IEAI9901-COD-ERRORE
                       STRING WK-TABELLA           ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
                  WHEN IDSO0011-SUCCESSFUL-SQL
                     MOVE IDSO0011-BUFFER-DATI TO PARAM-COMP
              END-EVALUATE
           ELSE
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0010-LETTURA-PCO'
                                      TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S0010-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--  Estrazione dal PTF di tutte le polizze appartenenti allo
      *--  stesso contraente
           IF WS-CONTRAENTE-SI
              PERFORM S1015-ELAB-ALTRE-POL-CONTR
                 THRU S1015-EX
           END-IF.
           IF IDSV0001-ESITO-OK
              PERFORM S1025-ABILITA-TASTO
                 THRU S1025-ABILITA-TASTO-EX
           END-IF.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1015-ELAB-ALTRE-POL-CONTR.

           INITIALIZE IDSI0011-BUFFER-DATI

           MOVE 'LDBS5560'              TO WK-TABELLA

           SET IDSI0011-FETCH-FIRST     TO TRUE

           SET IDSO0011-SUCCESSFUL-RC   TO TRUE
           SET IDSO0011-SUCCESSFUL-SQL  TO TRUE

           SET FINE-CICLO-POL-NO        TO TRUE

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR IDSV0001-ESITO-KO
                      OR FINE-CICLO-POL-SI

             MOVE WS-DT-INFINITO-1   TO IDSI0011-DATA-INIZIO-EFFETTO
             MOVE ZERO               TO IDSI0011-DATA-FINE-EFFETTO
                                        IDSI0011-DATA-COMPETENZA

             SET IDSI0011-TRATT-X-COMPETENZA TO TRUE
             SET IDSI0011-WHERE-CONDITION    TO TRUE

             INITIALIZE LDBV5561

             MOVE LCCC0033-COD-SOGG      TO LDBV5561-COD-SOGG
             MOVE LDBV5561               TO IDSI0011-BUFFER-WHERE-COND

             MOVE 'LDBS5560'             TO IDSI0011-CODICE-STR-DATO
             MOVE SPACES                 TO IDSI0011-BUFFER-DATI

             PERFORM CALL-DISPATCHER
                THRU CALL-DISPATCHER-EX

             IF IDSV0001-ESITO-OK
                IF IDSO0011-SUCCESSFUL-RC
                   EVALUATE TRUE
                     WHEN IDSO0011-SUCCESSFUL-SQL
                          MOVE IDSO0011-BUFFER-DATI TO POLI
                          PERFORM S1020-CTRL-POL-REC-PROVV
                             THRU S1020-EX
                          IF  IDSV0001-ESITO-OK
                          AND SCARTO-POL-NO
                          AND LCCC0033-VERIFICA-REC-PROVV
                              SET LCCC0033-RECUP-PROVV-SI TO TRUE
                              SET FINE-CICLO-POL-SI       TO TRUE
      *                        PERFORM S8100-CLOSE-CURSOR
      *                           THRU S8100-EX
                          ELSE
                             SET IDSI0011-FETCH-NEXT  TO TRUE
                          END-IF
                     WHEN IDSO0011-NOT-FOUND
                          SET FINE-CICLO-POL-SI     TO TRUE
                   END-EVALUATE
                ELSE
                   MOVE WK-PGM               TO IEAI9901-COD-SERVIZIO-BE
                   MOVE 'S1015-ELAB-ALTRE-POL-CONTR'
                                             TO IEAI9901-LABEL-ERR
                   MOVE '005016'             TO IEAI9901-COD-ERRORE
                   STRING WK-TABELLA           ';'
                          IDSO0011-RETURN-CODE ';'
                          IDSO0011-SQLCODE
                   DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                   END-STRING
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
                END-IF
             END-IF
           END-PERFORM.

       S1015-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1020-CTRL-POL-REC-PROVV.

           SET SCARTO-POL-NO TO TRUE

      *--  Controllo lo stato e causale della polizza
           PERFORM S1025-CTRL-STATO-CAUSALE
              THRU S1025-EX

      *--  Controllo che la data effetto dello stato e causale
      *--  deve essere compresa entro i due anni dalla data decorrenza
      *--  del contratto in emissione
           IF  IDSV0001-ESITO-OK
           AND SCARTO-POL-NO
              IF LCCC0033-VENDITA
                 PERFORM S1030-CTRL-EFFETTO-STORNO
                    THRU S1030-EX
              ELSE
                 IF LCCC0033-STORNI
                    PERFORM S1032-CTRL-EFFETTO-EMIS
                       THRU S1032-EX
                 END-IF
              END-IF
           END-IF

      *--  Controllo che la polizza sia a premio Annuo e non una TCM
           IF  IDSV0001-ESITO-OK
           AND SCARTO-POL-NO
               PERFORM S1040-CTRL-PROD-PREMIO-ANNUO
                  THRU S1040-EX
           END-IF

      *--  Calcolo del premio annuo di polizza
           IF  IDSV0001-ESITO-OK
           AND SCARTO-POL-NO
               PERFORM S1045-CALC-PREMIO-ANNUO
                  THRU S1045-EX
           END-IF

           IF  IDSV0001-ESITO-OK
           AND SCARTO-POL-NO AND LCCC0033-ESTR-POL-REC-PROVV
               PERFORM S1055-VAL-LISTA-ALTRE-POL
                  THRU S1055-EX
           END-IF.

       S1020-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1025-CTRL-STATO-CAUSALE.

           MOVE POL-ID-POLI TO WK-ID-OGG
           SET POLIZZA TO TRUE
           PERFORM S1022-LETTURA-STB
              THRU S1022-EX

           IF IDSV0001-ESITO-OK
              MOVE STB-TP-STAT-BUS TO WS-TP-STAT-BUS
              MOVE STB-TP-CAUS     TO WS-TP-CAUS

              IF LCCC0033-VENDITA
                 EVALUATE TRUE ALSO TRUE
                     WHEN STORNATO   ALSO RISCATTO-TOTALE
                     WHEN COMPLETATA ALSO PAGAMENT-RISCATTO-TOTALE
                     WHEN IN-VIGORE  ALSO RIDOTTA
                     WHEN STORNATO   ALSO INSOLVENZA
                        SET SCARTO-POL-NO TO TRUE
                     WHEN OTHER
                        SET SCARTO-POL-SI TO TRUE
                 END-EVALUATE
              ELSE
                 IF LCCC0033-STORNI
                    EVALUATE TRUE ALSO TRUE
                        WHEN IN-VIGORE  ALSO ATTESA-PERFEZIONAMENTO
                        WHEN IN-VIGORE  ALSO PERFEZIONATA
                           SET SCARTO-POL-NO TO TRUE
                        WHEN OTHER
                           SET SCARTO-POL-SI TO TRUE
                    END-EVALUATE
                 END-IF
              END-IF
           END-IF.

       S1025-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1022-LETTURA-STB.

           MOVE 'STAT-OGG-BUS'              TO WK-TABELLA

           INITIALIZE STAT-OGG-BUS.

           MOVE WS-DT-INFINITO-1         TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA

           SET IDSI0011-SELECT           TO TRUE
           SET IDSI0011-ID-OGGETTO       TO TRUE
           SET IDSI0011-TRATT-DEFAULT    TO TRUE

           MOVE WK-ID-OGG                TO STB-ID-OGG
           MOVE WS-TP-OGG                TO STB-TP-OGG
           MOVE 'STAT-OGG-BUS'           TO IDSI0011-CODICE-STR-DATO

           MOVE STAT-OGG-BUS             TO IDSI0011-BUFFER-DATI

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                     MOVE IDSO0011-BUFFER-DATI  TO STAT-OGG-BUS
                     MOVE STB-TP-STAT-BUS  TO WS-TP-STAT-BUS
                  WHEN IDSO0011-NOT-FOUND
                     MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1022-LETTURA-STB' TO IEAI9901-LABEL-ERR
                     MOVE '005069'           TO IEAI9901-COD-ERRORE
                     MOVE WK-TABELLA         TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
              MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0001-LETTURA-STB'      TO IEAI9901-LABEL-ERR
              MOVE '005016'                 TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S1022-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1030-CTRL-EFFETTO-STORNO.

           MOVE '03'                        TO A2K-FUNZ
           MOVE '03'                        TO A2K-INFO
           MOVE LCCC0033-DT-DECOR-POL       TO A2K-INDATA
           MOVE 2                           TO A2K-DELTA
           MOVE 'A'                         TO A2K-TDELTA
           MOVE '0'                         TO A2K-FISLAV
                                               A2K-INICON
           PERFORM CALL-LCCS0003
              THRU CALL-LCCS0003-EX

           IF IDSV0001-ESITO-OK
              MOVE A2K-OUAMG              TO DATA-INF-AMG
              MOVE LCCC0033-DT-DECOR-POL  TO DATA-SUP-AMG
              IF  STB-DT-INI-EFF <= DATA-SUP-AMG
              AND STB-DT-INI-EFF >= DATA-INF-AMG
                 CONTINUE
              ELSE
                 SET SCARTO-POL-SI TO TRUE
              END-IF
           END-IF.

       S1030-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1032-CTRL-EFFETTO-EMIS.

           MOVE '03'                        TO A2K-FUNZ
           MOVE '03'                        TO A2K-INFO
           MOVE LCCC0033-DT-STORNO-POL      TO A2K-INDATA
           MOVE 2                           TO A2K-DELTA
           MOVE 'A'                         TO A2K-TDELTA
           MOVE '0'                         TO A2K-FISLAV
                                               A2K-INICON
           PERFORM CALL-LCCS0003
              THRU CALL-LCCS0003-EX

           IF IDSV0001-ESITO-OK
              MOVE A2K-OUAMG               TO DATA-INF-AMG
              MOVE LCCC0033-DT-STORNO-POL  TO DATA-SUP-AMG
              IF  POL-DT-DECOR <= DATA-SUP-AMG
              AND POL-DT-DECOR >= DATA-INF-AMG
                 CONTINUE
              ELSE
                 SET SCARTO-POL-SI TO TRUE
              END-IF
           END-IF.

       S1032-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1040-CTRL-PROD-PREMIO-ANNUO.

           PERFORM S1041-LETTURA-ADE
              THRU S1041-EX

           IF IDSV0001-ESITO-OK
              PERFORM S1042-LETTURA-GAR
                 THRU S1042-EX
           END-IF

           IF IDSV0001-ESITO-OK
              IF WS-GARANZIA-BASE-SI
  MC  *          IF  GRZ-TP-RSH NOT = 'TC'
  MC             IF  GRZ-TP-RSH NOT = 'MO'
                 AND GRZ-TP-PER-PRE = 'A'
                   CONTINUE
                 ELSE
                    SET SCARTO-POL-SI TO TRUE
                 END-IF
              ELSE
                 SET SCARTO-POL-SI TO TRUE
              END-IF
           END-IF.

       S1040-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1041-LETTURA-ADE.

           MOVE 'ADES'                   TO WK-TABELLA

           INITIALIZE ADES

           MOVE WS-DT-INFINITO-1         TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA

           SET IDSI0011-FETCH-FIRST      TO TRUE
           SET IDSI0011-ID-PADRE         TO TRUE
           SET IDSI0011-TRATT-DEFAULT    TO TRUE

           MOVE POL-ID-POLI              TO ADE-ID-POLI

           MOVE 'ADES'           TO IDSI0011-CODICE-STR-DATO
           MOVE ADES             TO IDSI0011-BUFFER-DATI

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI   TO ADES
                       PERFORM S8100-CLOSE-CURSOR
                          THRU S8100-EX
                  WHEN IDSO0011-NOT-FOUND
                       MOVE WK-PGM          TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1041-LETTURA-ADE' TO IEAI9901-LABEL-ERR
                       MOVE '005069'        TO IEAI9901-COD-ERRORE
                       MOVE WK-TABELLA      TO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
              MOVE WK-PGM               TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1041-LETTURA-ADE'  TO IEAI9901-LABEL-ERR
              MOVE '005016'             TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA         ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S1041-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1042-LETTURA-GAR.

           MOVE 'GAR'                   TO WK-TABELLA

           INITIALIZE GAR

           MOVE WS-DT-INFINITO-1         TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA

           SET IDSI0011-FETCH-FIRST      TO TRUE
           SET IDSI0011-ID-PADRE         TO TRUE
           SET IDSI0011-TRATT-DEFAULT    TO TRUE

           MOVE POL-ID-POLI              TO GRZ-ID-POLI
           MOVE ADE-ID-ADES              TO GRZ-ID-ADES
           MOVE 'GAR'                    TO IDSI0011-CODICE-STR-DATO
           MOVE GAR                      TO IDSI0011-BUFFER-DATI

           SET FINE-CICLO-GAR-NO TO TRUE
           SET WS-GARANZIA-BASE-NO  TO TRUE

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR IDSV0001-ESITO-KO
                      OR FINE-CICLO-GAR-SI

              PERFORM CALL-DISPATCHER
                 THRU CALL-DISPATCHER-EX

              IF IDSO0011-SUCCESSFUL-RC
                 EVALUATE TRUE
                     WHEN IDSO0011-SUCCESSFUL-SQL
                        MOVE IDSO0011-BUFFER-DATI  TO GAR
                        MOVE GRZ-TP-GAR TO ACT-TP-GARANZIA
                        IF TP-GAR-BASE
                           SET FINE-CICLO-GAR-SI   TO TRUE
                           SET WS-GARANZIA-BASE-SI TO TRUE
                           PERFORM S8100-CLOSE-CURSOR
                              THRU S8100-EX
                        ELSE
                           SET IDSI0011-FETCH-NEXT TO TRUE
                        END-IF
                     WHEN IDSO0011-NOT-FOUND
                        SET FINE-CICLO-GAR-SI TO TRUE
                 END-EVALUATE
              ELSE
                 MOVE WK-PGM               TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S1042-LETTURA-GAR'  TO IEAI9901-LABEL-ERR
                 MOVE '005016'             TO IEAI9901-COD-ERRORE
                 STRING WK-TABELLA           ';'
                        IDSO0011-RETURN-CODE ';'
                        IDSO0011-SQLCODE
                 DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-PERFORM.

       S1042-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1045-CALC-PREMIO-ANNUO.

           MOVE ZERO TO WS-PREMIO-ANNUO-CALC
           PERFORM S1048-LETTURA-TGA
              THRU S1048-EX

           IF  IDSV0001-ESITO-OK
           AND WS-PREMIO-ANNUO-TGA > ZERO
               PERFORM S1050-SUM-IMP-COLLG
                  THRU S1050-EX
               IF  IDSV0001-ESITO-OK
               AND LDBV5571-TOT-IMP-COLL >= ZERO
                  COMPUTE WS-PREMIO-ANNUO-CALC = WS-PREMIO-ANNUO-TGA -
                                                 LDBV5571-TOT-IMP-COLL
               END-IF
 MC            IF  WS-PREMIO-ANNUO-CALC <= ZERO
 MC                SET SCARTO-POL-SI TO TRUE
 MC            END-IF
           END-IF.

       S1045-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
       S1048-LETTURA-TGA.

           MOVE 'LDBSD510'               TO WK-TABELLA

           INITIALIZE LDBVD511.

           MOVE WS-DT-INFINITO-1         TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA
      *--  Stato Business di partenza
           MOVE WS-TP-STAT-BUS           TO LDBVD511-TP-STAT-BUS

      *--  Causale Business di partenza
           MOVE WS-TP-CAUS               TO LDBVD511-TP-CAUS
           SET  TRANCHE                  TO TRUE
           MOVE WS-TP-OGG                TO LDBVD511-TP-OGG.

           MOVE POL-ID-POLI              TO LDBVD511-ID-POLI
           MOVE ADE-ID-ADES              TO LDBVD511-ID-ADES

           MOVE 'LDBSD510'               TO IDSI0011-CODICE-STR-DATO.

           MOVE LDBVD511                 TO IDSI0011-BUFFER-WHERE-COND.

           SET IDSI0011-TRATT-DEFAULT    TO TRUE
           SET IDSI0011-FETCH-FIRST      TO TRUE
           SET IDSI0011-WHERE-CONDITION  TO TRUE

           SET IDSO0011-SUCCESSFUL-RC    TO TRUE
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE

           SET FINE-CICLO-TGA-NO         TO TRUE

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR IDSV0001-ESITO-KO
                      OR FINE-CICLO-TGA-SI

              PERFORM CALL-DISPATCHER
                 THRU CALL-DISPATCHER-EX

              IF IDSO0011-SUCCESSFUL-RC
                 EVALUATE TRUE
                     WHEN IDSO0011-SUCCESSFUL-SQL
                          MOVE IDSO0011-BUFFER-DATI TO TRCH-DI-GAR
                          IF TGA-PRE-TARI-INI-NULL = HIGH-VALUE
                             CONTINUE
                          ELSE
                             ADD TGA-PRE-TARI-INI TO WS-PREMIO-ANNUO-TGA
                          END-IF
                          SET IDSI0011-FETCH-NEXT TO TRUE
                     WHEN IDSO0011-NOT-FOUND
                          SET FINE-CICLO-TGA-SI     TO TRUE
                 END-EVALUATE
              ELSE
                 MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'S1048-LETTURA-TGA'    TO IEAI9901-LABEL-ERR
                 MOVE '005016'               TO IEAI9901-COD-ERRORE
                 STRING WK-TABELLA           ';'
                        IDSO0011-RETURN-CODE ';'
                        IDSO0011-SQLCODE
                        DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-PERFORM.

       S1048-EX.
           EXIT.

      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1050-SUM-IMP-COLLG.

           MOVE 'LDBS5570'               TO WK-TABELLA

           INITIALIZE LDBV5571.

           MOVE WS-DT-INFINITO-1         TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA


           MOVE 'LDBS5570'               TO IDSI0011-CODICE-STR-DATO.

           MOVE  POL-ID-POLI             TO LDBV5571-ID-OGG-DER
           SET POLIZZA TO TRUE
           MOVE  WS-TP-OGG               TO LDBV5571-TP-OGG-DER

           IF LCCC0033-VENDITA
              SET RECUP-PROVV-STORNI-ALRE-POL   TO TRUE
           ELSE
              IF LCCC0033-STORNI
                 SET RECUP-PROVV-EMISS-ALRE-POL TO TRUE
              END-IF
           END-IF

           MOVE WS-TP-COLLGM             TO LDBV5571-TP-COLL-1

           MOVE LDBV5571                 TO IDSI0011-BUFFER-DATI.

           SET IDSI0011-TRATT-DEFAULT    TO TRUE
           SET IDSI0011-SELECT           TO TRUE
           SET IDSI0011-WHERE-CONDITION  TO TRUE

           SET IDSO0011-SUCCESSFUL-RC    TO TRUE
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI TO LDBV5571
      *                 PERFORM S8100-CLOSE-CURSOR
      *                    THRU S8100-EX
              END-EVALUATE
           ELSE
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1050-SUM-IMP-COLLG'  TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S1050-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S1055-VAL-LISTA-ALTRE-POL.

           IF LCCC0033-ELE-MAX-RECUP-PROVV < WK-MAX-POL-RECUP-PROVV
              ADD 1 TO IX-TAB-PAG
              ADD 1 TO LCCC0033-ELE-MAX-RECUP-PROVV

              MOVE POL-ID-POLI    TO LCCC0033-ID-POLI(IX-TAB-PAG)
              MOVE POL-IB-OGG     TO LCCC0033-IB-OGG(IX-TAB-PAG)
              MOVE STB-TP-STAT-BUS
                                  TO LCCC0033-STATO(IX-TAB-PAG)
              MOVE STB-TP-CAUS    TO LCCC0033-CAUSALE(IX-TAB-PAG)
              MOVE POL-DT-DECOR   TO LCCC0033-DT-DECORRENZA(IX-TAB-PAG)
              MOVE STB-DT-INI-EFF TO LCCC0033-DT-STORNO(IX-TAB-PAG)
              MOVE WS-PREMIO-ANNUO-CALC
                TO LCCC0033-IMP-PREMIO-ANNUO(IX-TAB-PAG)
           END-IF.

       S1055-EX.
           EXIT.
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S8100-CLOSE-CURSOR.

           SET IDSI0011-CLOSE-CURSOR TO TRUE

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              CONTINUE
           ELSE
              MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S8110-CLOSE-CURSOR'
                                           TO IEAI9901-LABEL-ERR
              MOVE '005016'                TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       S8100-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES PER IL CALCOLO SULLE DATE
      *----------------------------------------------------------------*
       CALL-LCCS0003.

           CALL LCCS0003 USING IO-A2K-LCCC0003 IN-RCODE
           ON EXCEPTION
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SERVIZIO OPERAZIONI SULLE DATE'
                                             TO CALL-DESC
               MOVE 'CALL-LCCS0003'          TO IEAI9901-LABEL-ERR
               PERFORM S0290-ERRORE-DI-SISTEMA
                  THRU EX-S0290
           END-CALL.

           IF IN-RCODE  = '00'
              CONTINUE
      *       MOVE A2K-OUAMG              TO WS-DATA
           ELSE
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'CALL-LCCS0003'        TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       CALL-LCCS0003-EX.
           EXIT.
      *----------------------------------------------------------------*
      *          ABILITA TASTO
      *----------------------------------------------------------------*
       S1025-ABILITA-TASTO.

12201      IF LCCC0033-ESTR-POL-REC-PROVV
              IF LCCC0033-ELE-MAX-RECUP-PROVV > ZERO
                 SET LCCC0033-RECUP-PROVV-SI TO TRUE
              ELSE
                 SET LCCC0033-RECUP-PROVV-NO TO TRUE
              END-IF
           END-IF.

       S1025-ABILITA-TASTO-EX.
           EXIT.

      *----------------------------------------------------------------*
      * ROUTINES GESTIONE ERRORI                                       *
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.

      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.

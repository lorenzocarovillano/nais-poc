      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS1010.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2011.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS1010
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... ESTRAZIONE CUMULI PREMI ATTIVI
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS1010'.
       01  WK-CALL-PGM                      PIC X(008).
       01  LDBS6040                         PIC X(008) VALUE 'LDBS6040'.

      ******************************************************************
      *    COPY DISPATCHER
      ******************************************************************
       01  IDSV0012.
           COPY IDSV0012.

      *----------------------------------------------------------------*
      *    COPY DI INPUT PER LA CHIAMATA AL DISPATCHER WHERE CONDITION
      *----------------------------------------------------------------*
           COPY LDBV5141.

      *---------------------------------------------------------------*
      *  LISTA MOVIMENTI IMPLEMENTATI LCCC0006
      *---------------------------------------------------------------*
           COPY LCCC0006.
           COPY LCCVXTH0.
           COPY LCCVXFA0.
10819      COPY LCCVXMVZ.
10819      COPY LCCVXMV2.
10819      COPY LCCVXMV5.
      *----------------------------------------------------------------*
      *--> AREA ESTENSIONE DI TRC
      *----------------------------------------------------------------*
       01 AREA-IO-POL.
          03 DPOL-AREA-POLIZZA.
             04 DPOL-ELE-POLI-MAX       PIC S9(04) COMP.
             04 DPOL-TAB-POLI.
             COPY LCCVPOL1              REPLACING ==(SF)== BY ==DPOL==.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *    COPY TABELLE DB2
      *----------------------------------------------------------------*
           COPY IDBVMOV1.
           COPY IDBVE121.
      *----------------------------------------------------------------*
      *      VARIBILI
      *----------------------------------------------------------------*
       01 WS-TRCH-POS.
          05 WS-TP-TRCH-POS-1                    PIC X(02) VALUE '1'.
          05 WS-TP-TRCH-POS-2                    PIC X(02) VALUE '2'.
          05 WS-TP-TRCH-POS-3                    PIC X(02) VALUE '3'.
          05 WS-TP-TRCH-POS-4                    PIC X(02) VALUE '4'.
          05 WS-TP-TRCH-POS-5                    PIC X(02) VALUE '5'.
          05 WS-TP-TRCH-POS-6                    PIC X(02) VALUE '6'.
          05 WS-TP-TRCH-POS-7                    PIC X(02) VALUE '7'.
          05 WS-TP-TRCH-POS-8                    PIC X(02) VALUE '8'.
          05 WS-TP-TRCH-POS-16                   PIC X(02) VALUE '16'.
          05 WS-TP-TRCH-POS-17                   PIC X(02) VALUE '17'.

       01 WS-TRCH-NEG.
          05 WS-TP-TRCH-NEG-9                    PIC X(02) VALUE '9'.
          05 WS-TP-TRCH-NEG-10                   PIC X(02) VALUE '10'.
          05 WS-TP-TRCH-NEG-11                   PIC X(02) VALUE '11'.
          05 WS-TP-TRCH-NEG-13                   PIC X(02) VALUE '13'.
          05 WS-TP-TRCH-NEG-14                   PIC X(02) VALUE '14'.
          05 WS-TP-TRCH-NEG-15                   PIC X(02) VALUE '15'.
          05 FILLER                              PIC X(08) VALUE SPACES.

       77 WS-CUM-PRE-ATT                 PIC S9(12)V9(3) COMP-3 VALUE 0.

       77 WK-ID-MOVI-COMUN               PIC S9(09) COMP-3.

       77 WK-DT-INI-EFF-TEMP             PIC S9(08) COMP-3.
       77 WK-DT-FINE-EFF-TEMP            PIC S9(08) COMP-3.
       77 WK-DT-CPTZ-TEMP                PIC S9(18) COMP-3.

       01  WK-VAR-MOVI-COMUN.
           05 WK-DATA-EFF-PREC            PIC S9(08) COMP-3.
           05 WK-DATA-CPTZ-PREC           PIC S9(18) COMP-3.

       01 FLAG-ULTIMA-LETTURA               PIC X(01).
          88 SI-ULTIMA-LETTURA              VALUE 'S'.
          88 NO-ULTIMA-LETTURA              VALUE 'N'.

       01  FLAG-CUR-MOV                     PIC X(001).
           88 INIT-CUR-MOV                            VALUE 'S'.
           88 FINE-CUR-MOV                            VALUE 'N'.

       01  FLAG-COMUN-TROV                  PIC X(01).
           88 COMUN-TROV-SI                           VALUE 'S'.
           88 COMUN-TROV-NO                           VALUE 'N'.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.


      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS1010.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS1010.
      *----------------------------------------------------------------*
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1000-ELABORAZIONE
                  THRU EX-S1000
           END-IF.
           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

           IF IVVC0213-ID-ADE-FITTZIO
              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
           END-IF.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.
      *--> RECUPERO LE DATA PRIMA DEL MOVIMENTO DI COMUNICAZIONE
           PERFORM RECUP-MOVI-COMUN
              THRU RECUP-MOVI-COMUN-EX
           MOVE IVVC0213-TIPO-MOVI-ORIG      TO WS-MOVIMENTO
      *--> SE SONO IN FASE DI LIQUIDAZIONE DEVO ESCLUDERE DAL CALCOLO
      *--> IL VALORE CALCOLATO IN FASE DI COMUNICAZIONE
           IF (LIQUI-RISPAR-POLIND
           OR LIQUI-RISPAR-ADE
10819X     OR LIQUI-RPP-REDDITO-PROGR
10819      OR LIQUI-RPP-TAKE-PROFIT) AND COMUN-TROV-SI

               MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WK-DT-INI-EFF-TEMP
               IF IDSV0003-DATA-COMPETENZA IS NUMERIC
                  MOVE IDSV0003-DATA-COMPETENZA     TO WK-DT-CPTZ-TEMP
               END-IF

               MOVE WK-DATA-EFF-PREC     TO IDSV0003-DATA-INIZIO-EFFETTO
                                            IDSV0003-DATA-FINE-EFFETTO
               MOVE WK-DATA-CPTZ-PREC    TO IDSV0003-DATA-COMPETENZA
           END-IF

               PERFORM RECUP-CUM-PRE-ATT-TRA
              THRU RECUP-CUM-PRE-ATT-TRA-EX.

           IF (LIQUI-RISPAR-POLIND
           OR LIQUI-RISPAR-ADE
10819X     OR LIQUI-RPP-REDDITO-PROGR
10819      OR LIQUI-RPP-TAKE-PROFIT) AND COMUN-TROV-SI

               MOVE WK-DT-INI-EFF-TEMP  TO IDSV0003-DATA-INIZIO-EFFETTO
               MOVE WK-DT-CPTZ-TEMP     TO IDSV0003-DATA-COMPETENZA

           END-IF.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    Recupero il movimento di comunicazione
      *----------------------------------------------------------------*
       RECUP-MOVI-COMUN.

           SET IDSV0003-FETCH-FIRST           TO TRUE
           SET IDSV0003-SUCCESSFUL-RC         TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
           SET NO-ULTIMA-LETTURA              TO TRUE

           SET INIT-CUR-MOV                   TO TRUE
           SET COMUN-TROV-NO                  TO TRUE

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
                      OR FINE-CUR-MOV
                      OR COMUN-TROV-SI

              INITIALIZE MOVI

              MOVE IVVC0213-ID-POLIZZA            TO MOV-ID-OGG
              MOVE 'PO'                           TO MOV-TP-OGG

              SET IDSV0003-TRATT-SENZA-STOR       TO TRUE

      *--> LIVELLO OPERAZIONE
              SET IDSV0003-WHERE-CONDITION       TO TRUE
      *--> INIZIALIZZA CODICE DI RITORNO
              SET IDSV0003-SUCCESSFUL-RC         TO TRUE
              SET IDSV0003-SUCCESSFUL-SQL        TO TRUE

              MOVE 'LDBS6040'            TO WK-CALL-PGM
              CALL WK-CALL-PGM   USING  IDSV0003 MOVI

              ON EXCEPTION
                 MOVE 'LDBS6040'
                   TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'CALL-LDBS6040 ERRORE CHIAMATA '
                   TO IDSV0003-DESCRIZ-ERR-DB2
                   SET IDSV0003-INVALID-OPER  TO TRUE
              END-CALL

              IF IDSV0003-SUCCESSFUL-RC
                EVALUATE TRUE

                    WHEN IDSV0003-NOT-FOUND
                      SET FINE-CUR-MOV TO TRUE
                      IF IDSV0003-FETCH-FIRST
                         MOVE 'LDBS6040'     TO IDSV0003-COD-SERVIZIO-BE
                         STRING 'CHIAMATA LDBS6040 ;'
                                IDSV0003-RETURN-CODE ';'
                                IDSV0003-SQLCODE
                         DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                         END-STRING
                      ELSE
                         SET IDSV0003-SUCCESSFUL-RC         TO TRUE
                         SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
                      END-IF

                    WHEN IDSV0003-SUCCESSFUL-SQL
                      IF SI-ULTIMA-LETTURA
                         SET COMUN-TROV-SI        TO TRUE
                         PERFORM CLOSE-MOVI
                            THRU CLOSE-MOVI-EX
                         SET FINE-CUR-MOV   TO TRUE
                      ELSE
                         SET IDSV0003-FETCH-NEXT   TO TRUE
                      END-IF
                      MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                      IF COMUN-RISPAR-IND
                      OR COMUN-RISPAR-ADE
10819                 OR RPP-TAKE-PROFIT
10819X                OR RPP-REDDITO-PROGRAMMATO
                         SET SI-ULTIMA-LETTURA  TO TRUE
                         MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                         COMPUTE WK-DATA-CPTZ-PREC =  MOV-DS-TS-CPTZ
                                                   - 1
                      ELSE
                         IF (LIQUI-RISPAR-POLIND
                         OR  LIQUI-RISPAR-ADE
10819X                   OR LIQUI-RPP-REDDITO-PROGR
10819                    OR  LIQUI-RPP-TAKE-PROFIT)
                         PERFORM CLOSE-MOVI
                            THRU CLOSE-MOVI-EX
                         SET FINE-CUR-MOV   TO TRUE
                         END-IF
                      END-IF

                    WHEN OTHER
                        SET IDSV0003-INVALID-OPER  TO TRUE
                        MOVE WK-CALL-PGM
                          TO IDSV0003-COD-SERVIZIO-BE
                        STRING 'ERRORE RECUP MOVI COMUN ;'
                               IDSV0003-RETURN-CODE ';'
                               IDSV0003-SQLCODE
                               DELIMITED BY SIZE INTO
                               IDSV0003-DESCRIZ-ERR-DB2
                        END-STRING

                END-EVALUATE
              ELSE
                MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
                STRING 'CHIAMATA LDBS6040 ;'
                       IDSV0003-RETURN-CODE ';'
                       IDSV0003-SQLCODE
                   DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                END-STRING
                IF IDSV0003-NOT-FOUND
                OR IDSV0003-SQLCODE = -305
                   SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                ELSE
                   SET IDSV0003-INVALID-OPER            TO TRUE
                END-IF
              END-IF
           END-PERFORM.

       RECUP-MOVI-COMUN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
      *----------------------------------------------------------------*
       CLOSE-MOVI.

           SET IDSV0003-TRATT-SENZA-STOR   TO TRUE

           MOVE SPACES                     TO IDSV0003-BUFFER-WHERE-COND
      *
           SET IDSV0003-CLOSE-CURSOR       TO TRUE.
           SET IDSV0003-WHERE-CONDITION    TO TRUE.
      *
           SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.

           MOVE 'LDBS6040'            TO WK-CALL-PGM
           CALL WK-CALL-PGM    USING  IDSV0003 MOVI

           ON EXCEPTION
              MOVE 'LDBS6040'
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBS6040 ERRORE CHIAMATA '
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL

           IF IDSV0003-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSV0003-SUCCESSFUL-SQL
                      CONTINUE
                  WHEN OTHER
                      MOVE 'LDBS6040'       TO IDSV0003-COD-SERVIZIO-BE
                      STRING 'CHIAMATA LDBS5140 ;'
                             IDSV0003-RETURN-CODE ';'
                             IDSV0003-SQLCODE
                         DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                      END-STRING
                      IF IDSV0003-NOT-FOUND
                      OR IDSV0003-SQLCODE = -305
                         SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                      ELSE
                         SET IDSV0003-INVALID-OPER            TO TRUE
                      END-IF
              END-EVALUATE
           ELSE
              MOVE 'LDBS6040'             TO IDSV0003-COD-SERVIZIO-BE
              STRING 'CHIAMATA LDBS5140 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                 DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
              IF IDSV0003-NOT-FOUND
              OR IDSV0003-SQLCODE = -305
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER            TO TRUE
              END-IF

           END-IF.

       CLOSE-MOVI-EX.
           EXIT.

      *----------------------------------------------------------------*
      *  CALCOLO LA VARIABILE CUMPREATTRA PER LA FASE DI LIQUIDAZIONE
      *  RISCATTO PARZIALE
      *----------------------------------------------------------------*
       RECUP-CUM-PRE-ATT-TRA.

           INITIALIZE                   EST-TRCH-DI-GAR.

           SET IDSV0003-SUCCESSFUL-RC             TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL            TO TRUE


           SET IDSV0003-TRATT-X-COMPETENZA        TO TRUE
           SET IDSV0003-SELECT                    TO TRUE
           SET IDSV0003-ID-PADRE                  TO TRUE

           MOVE IVVC0213-ID-TRANCHE         TO E12-ID-TRCH-DI-GAR

           MOVE 'IDBSE120'          TO WK-CALL-PGM
           CALL WK-CALL-PGM  USING  IDSV0003 EST-TRCH-DI-GAR
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'RECUP-CUM-PRE-ATT-TRA'
                 TO IDSV0003-DESCRIZ-ERR-DB2
                 SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL

           IF IDSV0003-SUCCESSFUL-RC
              EVALUATE TRUE
                 WHEN IDSV0003-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    IF E12-CUM-PRE-ATT-NULL = HIGH-VALUES
                        MOVE ZEROES             TO IVVC0213-VAL-IMP-O
                    ELSE
                        MOVE E12-CUM-PRE-ATT    TO IVVC0213-VAL-IMP-O
                    END-IF
NEW               WHEN OTHER
                      MOVE 'IDBSE120'       TO IDSV0003-COD-SERVIZIO-BE
                      STRING 'LETTURA EST-TRCH-DI-GAR;'
                             IDSV0003-RETURN-CODE ';'
                             IDSV0003-SQLCODE
                         DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                      END-STRING
                      IF IDSV0003-NOT-FOUND
                         SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                      ELSE
                         SET IDSV0003-INVALID-OPER            TO TRUE
                      END-IF
              END-EVALUATE
NEW        ELSE
                  MOVE 'IDBSE120'       TO IDSV0003-COD-SERVIZIO-BE
                  STRING 'LETTURA EST-TRCH-DI-GAR;'
                         IDSV0003-RETURN-CODE ';'
                         IDSV0003-SQLCODE
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                  END-STRING
                  IF IDSV0003-NOT-FOUND
                     SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                  ELSE
                     SET IDSV0003-INVALID-OPER            TO TRUE
                  END-IF
           END-IF.

       RECUP-CUM-PRE-ATT-TRA-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.

************************************************************************
********                                                        ********
********   'LIFE'   VERSIONE    01.00.00    DATA 12 GIUGNO 2007 ********
********                                                        ********
************************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.        LCCS0004.
       AUTHOR.            ATS.
       DATE-WRITTEN.      12 GIUGNO 2007.
      *****************************************************************
      *            F U N Z I O N I   D E L   P R O G R A M M A        *
      *****************************************************************
      *             CONTROLLO E/O INVERSIONI DELLA DATA               *
      *                                                               *
      *                                                               *
      *      PARAM = 1 SOLO CONTROLLO DATA GGMMAAAA                   *
      *      PARAM = 2 CONTROLLO ED INVERSIONE DA GGMMAAAA            *
      *                                        IN AAAAMMGG            *
      *      PARAM = 3 INVERSIONE DA AAAAMMGG IN GGMMAAAA             *
      *      LA DATA PUO' ESSERE PASSATA ALLA ROUTINE O NELLA         *
      *      FORMA GGMMAAAA O VICEVERSA AAAAMMGG.                     *
      *      PARAM = 4 SOLO CONTROLLO DATA AAAAMMGG                   *
      *      PARAM = 0 IN OUTPUT IN CASO DI DATA VALIDA.              *
      *****************************************************************
      *                                                               *
      *  INFINE,DOPO AVER AGGIORNATO L'AREA COMUNE,IL CONTROLLO       *
      *  VIENE CEDUTO AL PROGRAMMA CHIAMANTE.                         *
      *                                                               *
      *****************************************************************
      *                                                               *
      *            R O U T I N E         B A T C H                    *
      *                                                               *
      *       C O N T R O L L O   I N V E R S I O N E   D A T A       *
      *                                                               *
      *            ( PROGRAMMA COBOL CHIAMATO CON UNA CALL)           *
      *                                                               *
      *****************************************************************
       EJECT
       DATE-COMPILED.
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SOURCE-COMPUTER.
       OBJECT-COMPUTER.
       DATA DIVISION.
      *****************************************************************
      *                                                               *
      *      W O R K I N G - S T O R A G E   S E C T I O N            *
      *                                                               *
      *****************************************************************
       WORKING-STORAGE SECTION.
       01  FILLER                         PIC X(30)         VALUE
           '***INIZIO WORKING-STORAGE***'.
       77  RESTO                          PIC 9.
       77  RISULT                         PIC 9(3).
       01  TAB-GIORNI                     PIC X(24) VALUE
           '312831303130313130313031'.
       01  FILLER REDEFINES TAB-GIORNI.
           05 T-GG                        PIC 9(2) OCCURS 12.

       01  COM-DATA.
           05 C-AAAA                      PIC 9(4).
           05 FILLER REDEFINES C-AAAA.
               10 C-AA1                   PIC 9(2).
               10 C-AA2                   PIC 9(2).
           05 C-MM                        PIC 9(2).
           05 C-GG                        PIC 9(2).
       EJECT
      *****************************************************************
      **                L I N K A G E    S E C T I O N               **
      *****************************************************************
       LINKAGE SECTION.
       01  PARAM                          PIC 9.
       01  X-DATA.
           05 X-GG                        PIC 9(2).
           05 X-MM                        PIC 9(2).
           05 X-AAAA                      PIC 9(4).
           05 FILLER REDEFINES X-AAAA.
               10 X-AA1                   PIC 9(2).
               10 X-AA2                   PIC 9(2).
      *****************************************************************
      *                                                               *
      *             P R O C E D U R E   D I V I S I O N               *
      *                                                               *
      *****************************************************************
      *PROCEDURE DIVISION USING
       01  X-DATANUM   REDEFINES X-DATA    PIC 9(8).
       EJECT
       PROCEDURE DIVISION USING
                                PARAM X-DATA.
       INIZIO.
           IF X-DATANUM NOT NUMERIC OR
              X-AAAA EQUAL ZERO         GO TO FINE.
           GO TO   CONTROLLO
                   CONTROLLO-INVERSIONE
                   INVERSIONE-OPPOSTA
                   CONTROLLO-AAAAMMGG
           DEPENDING ON  PARAM.
       CONTROLLO.
           DIVIDE X-AAAA BY 4 GIVING RISULT REMAINDER RESTO.
           IF RESTO = 0
              MOVE 29 TO T-GG (2)
           ELSE
              MOVE 28 TO T-GG (2).
           IF X-MM GREATER 12 OR X-MM LESS 1
                        OR X-GG GREATER T-GG (X-MM) OR X-GG LESS 1
           GO TO FINE
           ELSE         MOVE ZERO TO PARAM
           GO TO FINE.
       CONTROLLO-INVERSIONE.
           DIVIDE X-AAAA BY 4 GIVING RISULT REMAINDER RESTO.
           IF RESTO = 0
              MOVE 29 TO T-GG (2)
           ELSE
              MOVE 28 TO T-GG (2).
           IF X-MM GREATER 12 OR X-MM LESS 1
                        OR X-GG GREATER T-GG (X-MM) OR X-GG LESS 1
               GO TO FINE
           ELSE
               MOVE X-GG     TO C-GG
               MOVE X-MM     TO C-MM
               MOVE X-AAAA   TO C-AAAA
               MOVE COM-DATA TO X-DATA
               MOVE ZERO     TO PARAM.
           GO TO FINE.
        INVERSIONE-OPPOSTA.
           MOVE    X-GG      TO C-MM.
           MOVE    X-MM      TO C-GG.
           MOVE    X-AA1     TO C-AA2.
           MOVE    X-AA2     TO C-AA1.
           MOVE    COM-DATA  TO X-DATA.
           MOVE    ZERO      TO PARAM.
       CONTROLLO-AAAAMMGG.
           MOVE    X-DATA    TO COM-DATA.
           DIVIDE C-AAAA BY 4 GIVING RISULT REMAINDER RESTO.
           IF RESTO = 0
              MOVE 29 TO T-GG (2)
           ELSE
              MOVE 28 TO T-GG (2).
           IF C-MM GREATER 12 OR C-MM LESS 1
                        OR C-GG GREATER T-GG (C-MM) OR C-GG LESS 1
           GO TO FINE
           ELSE         MOVE ZERO TO PARAM
           GO TO FINE.
       FINE.
           GOBACK.

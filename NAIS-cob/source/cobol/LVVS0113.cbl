      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0113.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0113
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0113'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01 WS-DT-RST.
          05 WS-DT-RST-AA              PIC 9(04).
          05 WS-DT-RST-MM              PIC 9(02)  VALUE 12 .
          05 WS-DT-RST-GG              PIC 9(02)  VALUE 31 .

       01 WS-DT-RST-9                  PIC 9(08) VALUE ZERO.
       01 WS-DT-RST-AP                 PIC 9(08) VALUE ZERO.
       01 WS-DATA-COMPETENZA           PIC S9(018) COMP-3.
      *
       01 WS-DATA-COMP-RST.
          03 WS-DATA-COMP-RST-X.
             05 WS-DT-COMP-RST-8       PIC 9(08).
             05 WS-LT-40-RST           PIC X(02).
             05 WS-LT-ORA-COMP-RST-8   PIC X(08).
          03 WS-DATA-COMP-RST-9        REDEFINES
             WS-DATA-COMP-RST-X        PIC 9(18).
      *
       01 WS-DT-9999MMGG.
          05 WS-DT-AA                PIC 9(04) VALUE ZERO.
          05 WS-DT-MM                PIC 9(02) VALUE ZERO.
          05 WS-DT-GG                PIC 9(02) VALUE ZERO.
      *----------------------------------------------------------------*
      *  COPY PER CHIAMATA IDBSRST0
      *----------------------------------------------------------------*
           COPY IDBVRST1.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.
           03 IX-TAB-ADE                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0113.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0113.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000

           GOBACK.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.
      *
           INITIALIZE RIS-DI-TRCH.

      *--> DATA EFFETTO
           MOVE ZERO                  TO WS-DT-RST-9.
           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                      TO WS-DT-RST-AP.
           MOVE IDSV0003-DATA-COMPETENZA
                                      TO WS-DATA-COMPETENZA.
           MOVE WS-DT-RST-AP          TO WS-DT-9999MMGG.
           COMPUTE WS-DT-RST-AA = WS-DT-AA - 1
           MOVE WS-DT-RST             TO WS-DT-RST-9.
           MOVE ZEROES                TO IDSV0003-DATA-FINE-EFFETTO.
           MOVE WS-DT-RST-9           TO IDSV0003-DATA-INIZIO-EFFETTO.
           SET IDSV0003-SELECT        TO TRUE.
           SET IDSV0003-ID-PADRE      TO TRUE.
      *
           MOVE WS-DT-RST-9
             TO WS-DT-COMP-RST-8.
           MOVE '40'
             TO WS-LT-40-RST.
           MOVE '23595999'
             TO WS-LT-ORA-COMP-RST-8.
           MOVE WS-DATA-COMP-RST-9    TO IDSV0003-DATA-COMPETENZA

      *--> VALORRIZZA DCLGEN TABELLA
           MOVE IVVC0213-ID-TRANCHE   TO RST-ID-TRCH-DI-GAR

      *
           MOVE 'IDBSRST0'                     TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM  USING  IDSV0003 RIS-DI-TRCH
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-IDBSRST0 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
           AND IDSV0003-SUCCESSFUL-RC
              MOVE RST-ULT-COEFF-RIS            TO IVVC0213-VAL-PERC-O
           ELSE
              IF IDSV0003-SQLCODE = +100
                 SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
              ELSE
                 SET IDSV0003-INVALID-OPER        TO TRUE
                 MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
                 STRING 'CHIAMATA IDBSRST0 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING
              END-IF
           END-IF.
      *
           MOVE WS-DT-RST-AP
             TO IDSV0003-DATA-INIZIO-EFFETTO.
      *
           MOVE WS-DATA-COMPETENZA
             TO IDSV0003-DATA-COMPETENZA.
      *
       EX-S1000.
           EXIT.

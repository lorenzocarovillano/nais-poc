       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDSS8880 IS INITIAL.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE
      * F A S E         : CALL DISPLAY X ONLINE
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       LINKAGE SECTION.

       01 WK-ADDRESS                        POINTER.  
       
       01 WK-AREA-DISPLAY                   PIC X(125).


      *****************************************************************

       PROCEDURE DIVISION USING WK-ADDRESS WK-AREA-DISPLAY.
       
           SET ADDRESS OF WK-AREA-DISPLAY TO WK-ADDRESS.
           
           DISPLAY WK-AREA-DISPLAY.

           GOBACK.        

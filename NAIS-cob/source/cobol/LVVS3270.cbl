      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS3270.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2017.
       DATE-COMPILED.
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS3270'.
       01  PGM-IDBSSDI0                     PIC X(008) VALUE 'IDBSSDI0'.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
       01 WK-LABEL                          PIC X(30).
      *----------------------------------------------------------------*
      *    FLAG
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *    COPY GESTIONE ERRORI
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
      *---------------------------------------------------------------*
      *  COPY TIPOLOGICHE
      *---------------------------------------------------------------*
           COPY LCCVXOG0.
           COPY LCCVXMVZ.
           COPY LCCVXMV2.
           COPY LCCVXTH0.
           COPY LCCVXSB0.

      *---------------------------------------------------------------*
      * AREA HOST VARIABLES
      *---------------------------------------------------------------*
      *--> DCLGEN STRATEGIA DI INVESTIMENTO
           COPY IDBVSDI1.

      *----------------------------------------------------------------*
      *--> AREA ADESIONE
      *----------------------------------------------------------------*
       01 AREA-IO-ADES.
          03 DADE-AREA-ADES.
             04 DADE-ELE-ADES-MAX       PIC S9(04) COMP.
             04 DADE-TAB-ADES.
             COPY LCCVADE1              REPLACING ==(SF)== BY ==DADE==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    COPY DISPATHCER
      *----------------------------------------------------------------*
       01  IDSV0012.
           COPY IDSV0012.

       01  DISPATCHER-VARIABLES.
      *--> COPY DI INPUT  ALLA  CHIAMATA DEL MODULO DISPATCHER
           COPY IDSI0011.
      *--> COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *----------------------------------------------------------------*
      *      CAMPI DI ESITO, AREE DI SERVIZIO                          *
      *----------------------------------------------------------------*
       01 AREA-IDSV0001.
           COPY IDSV0001.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP VALUE 0.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS3270.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS3270.
      *----------------------------------------------------------------*
           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000
      *
           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.
      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.
           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

           MOVE IVVC0213-TIPO-MOVI-ORIG
             TO WS-MOVIMENTO.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.
      *
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
      * --     LETTURA STRATEGIA D'INVESTIMENTO
               PERFORM A400-LEGGI-SDI
                  THRU A400-LEGGI-SDI-EX

               IF IDSV0003-SUCCESSFUL-RC
                  MOVE SDI-TP-STRA
                    TO IVVC0213-VAL-STR-O
               END-IF
           END-IF.
      *
       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *   LETTURA STRATEGIA DI INVESTIMENTO
      *----------------------------------------------------------------*
       A400-LEGGI-SDI.
           INITIALIZE STRA-DI-INVST.

           MOVE DADE-ID-ADES             TO SDI-ID-OGG
           SET ADESIONE                  TO TRUE
           MOVE  WS-TP-OGG               TO SDI-TP-OGG
           MOVE SPACES                   TO IDSV0003-BUFFER-WHERE-COND.

           SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.
           SET IDSV0003-ID-OGGETTO         TO TRUE.
           SET IDSV0003-SELECT             TO TRUE.

           SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.

           CALL PGM-IDBSSDI0  USING  IDSV0003 STRA-DI-INVST

           ON EXCEPTION
              MOVE PGM-IDBSSDI0
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'CALL LDBSF110 ERRORE CHIAMATA'
                TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-INVALID-OPER TO TRUE
           END-CALL.

           IF IDSV0003-SUCCESSFUL-SQL
              CONTINUE
           ELSE
      * SIR RISERVE DI NOVEMBRE 2018
SIR          IF IDSV0003-NOT-FOUND
SIR             SET IDSV0003-FIELD-NOT-VALUED TO TRUE
SIR             MOVE WK-PGM   TO IDSV0003-COD-SERVIZIO-BE
SIR             MOVE 'STRATEGIA NON TROVATA' TO IDSV0003-DESCRIZ-ERR-DB2
SIR          ELSE
               SET IDSV0003-INVALID-OPER     TO TRUE
               MOVE PGM-IDBSSDI0             TO IDSV0003-COD-SERVIZIO-BE

               STRING 'CHIAMATA IDBSSDI0 ;'
                     IDSV0003-RETURN-CODE ';'
                     IDSV0003-SQLCODE
                     DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                       END-STRING
SIR          END-IF
           END-IF.

       A400-LEGGI-SDI-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-ADES
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DADE-AREA-ADES
           END-IF.
      *
       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
      *----------------------------------------------------------------*
      *    ROUTINES CALL DISPATCHER
      *----------------------------------------------------------------*
           COPY IDSP0012.

      ******************************************************************
      **                                                        ********
      **    PORTAFOGLIO VITA ITALIA  VER 1.0                    ********
      **                                                        ********
      ******************************************************************
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LCCS0062.
       AUTHOR.             ATS.
       DATE-WRITTEN.       02/2008.
       DATE-COMPILED.
      *----------------------------------------------------------------*
      *    PROGRAMMA ..... LCCS0062
      *    TIPOLOGIA...... SERVIZIO TRASVERSALE
      *    PROCESSO....... XXX
      *    FUNZIONE....... XXX
      *    DESCRIZIONE.... CALCOLO RATE DI POLIZZA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                     PIC X(008) VALUE 'LCCS0062'.
       01  LCCS0003                   PIC X(008) VALUE 'LCCS0003'.
       01  WK-ELE-MAX-RATE            PIC 9(002) VALUE 12.

      *----------------------------------------------------------------*
      *    VARIABILI DI WORKING
      *----------------------------------------------------------------*
       01 WK-VARIABILI.
          03 WK-DT-INFERIORE          PIC  9(08)  COMP-3 VALUE ZEROES.
          03 WK-DT-SUPERIORE          PIC  9(08)  COMP-3 VALUE ZEROES.
          03 WK-DATA-ULT-PCO          PIC  9(08)  COMP-3 VALUE ZEROES.
          03 WK-COUNT-RATE            PIC  9(04)  COMP   VALUE ZEROES.
          03 WK-MESI                  PIC  9(04)  COMP   VALUE ZEROES.

      *----------------------------------------------------------------*
      *    DEFINIZIONE DI INDICI                                       *
      *----------------------------------------------------------------*
       01 WK-INDICI.
          03 IX-TAB-RATE              PIC 9(04) COMP.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.
           COPY IEAV9903.

      *----------------------------------------------------------------*
      *    FLAG/SWITCH
      *----------------------------------------------------------------*
       01 WK-CICLO                     PIC  X(01) VALUE 'N'.
          88 FINE-CICLO-SI             VALUE 'S'.
          88 FINE-CICLO-NO             VALUE 'N'.

      *----------------------------------------------------------------*
      * MODULI CHIAMATI                                                *
      *----------------------------------------------------------------*

      *----------------------------------------------------------------*
      * AREE MODULI CHIAMATI                                           *
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *    COPY PER SERVIZIO VERIFICA CALCOLI SU DATE
      *----------------------------------------------------------------*

           COPY LCCC0003.

       01 IN-RCODE                       PIC 9(2) VALUE ZEROES.
      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.
      *----------------------------------------------------------------*
      *    COMMAREA SERVIZIO
      *----------------------------------------------------------------*
      *--- AREA COMUNE.
       01  WCOM-AREA-STATI.
            COPY LCCC0001             REPLACING ==(SF)== BY ==WCOM==.

      *--> AREA DI PAGINA
       01 WCOM-AREA-PAGINA.
           COPY LCCC0062.

      *---------------------------------------------------------------*
       PROCEDURE DIVISION USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                WCOM-AREA-PAGINA.
      *---------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           IF IDSV0001-ESITO-OK
              PERFORM S1000-ELABORAZIONE
                 THRU EX-S1000
           END-IF.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      * OPERAZIONI INIZIALI                                            *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           PERFORM S0005-CTRL-DATI-INPUT
              THRU EX-S0005.

       EX-S0000.
           EXIT.
      *----------------------------------------------------------------*
      *    CONTROLLO DATI INPUT
      *----------------------------------------------------------------*
       S0005-CTRL-DATI-INPUT.

           INITIALIZE WK-VARIABILI
                      WK-INDICI.

           IF  LCCC0062-DT-ULT-QTZO IS NUMERIC
           AND LCCC0062-DT-ULT-QTZO > ZEROES
               MOVE LCCC0062-DT-ULT-QTZO      TO WK-DATA-ULT-PCO
           END-IF

           IF  LCCC0062-DT-ULTGZ-TRCH IS NUMERIC
           AND LCCC0062-DT-ULTGZ-TRCH > ZEROES
               MOVE LCCC0062-DT-ULTGZ-TRCH    TO WK-DATA-ULT-PCO
           END-IF.

           IF LCCC0062-FRAZIONAMENTO IS NUMERIC
           AND LCCC0062-FRAZIONAMENTO > ZEROES
              CONTINUE
           ELSE
              MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S0005-CTRL-DATI-INPUT' TO IEAI9901-LABEL-ERR
              MOVE '005018'                TO IEAI9901-COD-ERRORE
              MOVE 'CODICE FRAZIONAMENTO'  TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       EX-S0005.
           EXIT.

      *----------------------------------------------------------------*
      *  ELABORAZIONE                                                  *
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

           IF LCCC0062-DT-DECORRENZA > WK-DATA-ULT-PCO
               MOVE ZEROES   TO LCCC0062-TOT-NUM-RATE
           ELSE
              PERFORM CALCOLA-NUM-RATE
                 THRU CALCOLA-NUM-RATE-EX
           END-IF.

       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES PER IL CALCOLO SULLE DATE
      *----------------------------------------------------------------*
       CALCOLA-NUM-RATE.

           SET  FINE-CICLO-NO            TO TRUE

           PERFORM UNTIL FINE-CICLO-SI
                      OR IDSV0001-ESITO-KO

      *--     Calcola il limite inferiore dell'intervallo di rata
              PERFORM CALCOLA-DT-INF
                 THRU CALCOLA-DT-INF-EX

      *--     Calcola il limite superiore dell'intervallo di rata
              IF IDSV0001-ESITO-OK
                 PERFORM CALCOLA-DT-SUP
                    THRU CALCOLA-DT-SUP-EX
              END-IF

              IF  IDSV0001-ESITO-OK
              AND WK-DT-INFERIORE >  WK-DATA-ULT-PCO
                 SET FINE-CICLO-SI    TO TRUE
              ELSE
                 IF LCCC0062-TOT-NUM-RATE < WK-ELE-MAX-RATE
                    ADD 1               TO IX-TAB-RATE
                    MOVE WK-DT-INFERIORE
                      TO LCCC0062-DT-INF(IX-TAB-RATE)
                    MOVE WK-DT-SUPERIORE
                      TO LCCC0062-DT-SUP(IX-TAB-RATE)
                    ADD 1               TO LCCC0062-TOT-NUM-RATE
                    ADD 1               TO WK-COUNT-RATE
                 ELSE
                    SET FINE-CICLO-SI    TO TRUE
                 END-IF
              END-IF

           END-PERFORM.

       CALCOLA-NUM-RATE-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Calcola la data inferiore dell'intervallo di rata della
      *    polizza
      *----------------------------------------------------------------*
       CALCOLA-DT-INF.

           MOVE ZERO TO WK-DT-INFERIORE.

           COMPUTE WK-MESI = (LCCC0062-FRAZIONAMENTO * WK-COUNT-RATE).

           IF WK-MESI > ZERO
              MOVE 'M'                     TO A2K-TDELTA
              MOVE WK-MESI                 TO A2K-DELTA
              MOVE LCCC0062-DT-DECORRENZA  TO A2K-INDATA

              MOVE '02'                    TO A2K-FUNZ
              MOVE '03'                    TO A2K-INFO
              MOVE '0'                     TO A2K-FISLAV
              MOVE '0'                     TO A2K-INICON

              PERFORM CALL-LCCS0003
                 THRU CALL-LCCS0003-EX

              IF IDSV0001-ESITO-OK
                 MOVE A2K-OUAMG            TO WK-DT-INFERIORE
              END-IF
           ELSE
              MOVE LCCC0062-DT-DECORRENZA  TO WK-DT-INFERIORE
           END-IF.

       CALCOLA-DT-INF-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    Calcola la data inferiore dell'intervallo di rata della
      *    polizza
      *----------------------------------------------------------------*
       CALCOLA-DT-SUP.

           MOVE ZERO                    TO WK-DT-SUPERIORE

           MOVE 'M'                     TO A2K-TDELTA
           MOVE LCCC0062-FRAZIONAMENTO  TO A2K-DELTA
           MOVE WK-DT-INFERIORE         TO A2K-INDATA

           MOVE '02'                    TO A2K-FUNZ.
           MOVE '03'                    TO A2K-INFO.
           MOVE '0'                     TO A2K-FISLAV
                                           A2K-INICON.

           PERFORM CALL-LCCS0003
              THRU CALL-LCCS0003-EX

           IF IDSV0001-ESITO-OK
              MOVE A2K-OUAMG               TO WK-DT-SUPERIORE

              MOVE SPACES                  TO A2K-TDELTA
              MOVE 1                       TO A2K-DELTA
              MOVE WK-DT-SUPERIORE         TO A2K-INDATA

              MOVE '03'                    TO A2K-FUNZ
              MOVE '03'                    TO A2K-INFO
              MOVE '0'                     TO A2K-FISLAV
              MOVE '0'                     TO A2K-INICON

              PERFORM CALL-LCCS0003
                 THRU CALL-LCCS0003-EX

              IF IDSV0001-ESITO-OK
                 MOVE A2K-OUAMG               TO WK-DT-SUPERIORE
              END-IF
           END-IF.

       CALCOLA-DT-SUP-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES PER IL CALCOLO SULLE DATE
      *----------------------------------------------------------------*
       CALL-LCCS0003.

           CALL LCCS0003 USING IO-A2K-LCCC0003 IN-RCODE
           ON EXCEPTION
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SERVIZIO OPERAZIONI SULLE DATE'
                                             TO CALL-DESC
               MOVE 'CALL-LCCS0003'          TO IEAI9901-LABEL-ERR
               PERFORM S0290-ERRORE-DI-SISTEMA
                  THRU EX-S0290
           END-CALL.

           IF IN-RCODE  = '00'
              CONTINUE
           ELSE
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'CALL-LCCS0003'        TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       CALL-LCCS0003-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
           COPY IERP9903.

       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IABS0130 IS INITIAL.
       AUTHOR.        ATS NAPOLI.
       DATE-WRITTEN.  APRILE 2008.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO PER GESTIONE PARALLELISMO            *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2         PIC X(300)  VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.

       77 WS-PROG-PROTOCOL        PIC S9(9) COMP-5 VALUE ZEROES.

       01 WK-FINE-LOOP            PIC X(01).
          88 WK-FINE-LOOP-SI      VALUE 'S'.
          88 WK-FINE-LOOP-NO      VALUE 'N'.

       01 WK-COMODO.
          05 COMODO-COD-COMP-ANIA                PIC 9(9).
          05 COMODO-PROG-PROTOCOL                PIC 9(9).

       01 WK-PROG-PROTOCOL                       PIC S9(9) COMP-5.


       01 WS-BUFFER-WHERE-COND-IABS0130.
          05 WS-FLAG-FASE                  PIC X(02).
             88 WS-FASE-CONTROLLO-INIZIALE VALUE 'CI'.
             88 WS-FASE-CONTROLLO-FINALE   VALUE 'CF'.
             88 WS-FASE-INIZIALE           VALUE 'FI'.
             88 WS-FASE-FINALE             VALUE 'FF'.

          05 FLAG-BATCH-COMPLETE           PIC X(02).
             88 BATCH-COMPLETE             VALUE 'CO'.
             88 BATCH-NOT-COMPLETE         VALUE 'NC'.

          05 FLAG-BATCH-STATE              PIC X(02).
             88 BATCH-STATE-OK             VALUE 'OK'.
             88 BATCH-STATE-KO             VALUE 'KO'.


           EXEC SQL INCLUDE IDSV0014 END-EXEC.

           COPY IABV0004.


      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDBPA0 END-EXEC.
           EXEC SQL INCLUDE IDBVBPA2 END-EXEC.
           EXEC SQL INCLUDE IDBVBPA3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IABV0002 END-EXEC.

           EXEC SQL INCLUDE IDBVBPA1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 IABV0002 BTC-PARALLELISM.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              PERFORM A200-ELABORA                THRU A200-EX
           END-IF.

           MOVE WS-BUFFER-WHERE-COND-IABS0130
                         TO IDSV0003-BUFFER-WHERE-COND.

           GOBACK.

       A000-INIZIO.

           MOVE 'IABS0130'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'BTC_PARALLELISM' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           MOVE BPA-PROG-PROTOCOL        TO   WK-PROG-PROTOCOL.

           MOVE IDSV0003-BUFFER-WHERE-COND
                         TO WS-BUFFER-WHERE-COND-IABS0130.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA.

           EVALUATE TRUE
              WHEN WS-FASE-CONTROLLO-INIZIALE
                 PERFORM A375-CNTL                 THRU A375-EX

              WHEN WS-FASE-INIZIALE
                 PERFORM A212-PREPARA-UPD-INIZIALE THRU A212-EX

                 IF IDSV0003-SUCCESSFUL-RC
                    PERFORM A230-UPDATE            THRU A230-EX
                 END-IF

              WHEN WS-FASE-CONTROLLO-FINALE
                 PERFORM A380-CNTL                 THRU A380-EX

              WHEN WS-FASE-FINALE
                 PERFORM A213-PREPARA-UPD-FINALE   THRU A213-EX
                 PERFORM A230-UPDATE               THRU A230-EX

              WHEN OTHER
                 SET IDSV0003-INVALID-OPER         TO TRUE

           END-EVALUATE.

       A200-EX.
           EXIT.

      *----
      *----  gestione
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                COD_COMP_ANIA
                ,PROTOCOL
                ,PROG_PROTOCOL
                ,COD_BATCH_STATE
                ,DT_INS
                ,USER_INS
                ,DT_START
                ,DT_END
                ,USER_START
                ,DESC_PARALLELISM
                ,ID_OGG_DA
                ,ID_OGG_A
                ,TP_OGG
                ,NUM_ROW_SCHEDULE
             INTO
                :BPA-COD-COMP-ANIA
               ,:BPA-PROTOCOL
               ,:BPA-PROG-PROTOCOL
               ,:BPA-COD-BATCH-STATE
               ,:BPA-DT-INS-DB
               ,:BPA-USER-INS
               ,:BPA-DT-START-DB
                :IND-BPA-DT-START
               ,:BPA-DT-END-DB
                :IND-BPA-DT-END
               ,:BPA-USER-START
                :IND-BPA-USER-START
               ,:BPA-DESC-PARALLELISM-VCHAR
                :IND-BPA-DESC-PARALLELISM
               ,:BPA-ID-OGG-DA
                :IND-BPA-ID-OGG-DA
               ,:BPA-ID-OGG-A
                :IND-BPA-ID-OGG-A
               ,:BPA-TP-OGG
                :IND-BPA-TP-OGG
               ,:BPA-NUM-ROW-SCHEDULE
                :IND-BPA-NUM-ROW-SCHEDULE
             FROM BTC_PARALLELISM
             WHERE     COD_COMP_ANIA = :BPA-COD-COMP-ANIA
                   AND PROTOCOL      = :BPA-PROTOCOL
                   AND PROG_PROTOCOL = :BPA-PROG-PROTOCOL
                   AND COD_BATCH_STATE IN (
                                          :IABV0002-STATE-01,
                                          :IABV0002-STATE-02,
                                          :IABV0002-STATE-03,
                                          :IABV0002-STATE-04,
                                          :IABV0002-STATE-05,
                                          :IABV0002-STATE-06,
                                          :IABV0002-STATE-07,
                                          :IABV0002-STATE-08,
                                          :IABV0002-STATE-09,
                                          :IABV0002-STATE-10
                                          )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A212-PREPARA-UPD-INIZIALE.

           PERFORM A210-SELECT-PK   THRU A210-EX.

           IF NOT IDSV0003-SUCCESSFUL-SQL

              MOVE SPACES            TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-SQL-ERROR TO TRUE

              MOVE BPA-COD-COMP-ANIA TO COMODO-COD-COMP-ANIA
              MOVE BPA-PROG-PROTOCOL TO COMODO-PROG-PROTOCOL
              STRING 'JOB NON TROVATO SU BTC_PARALLELISM : '
                     DELIMITED BY SIZE
                     COMODO-COD-COMP-ANIA
                     ' / '
                     DELIMITED BY SIZE
                     BPA-PROTOCOL
                     DELIMITED BY SPACES
                     ' / '
                     DELIMITED BY SIZE
                     COMODO-PROG-PROTOCOL
                     DELIMITED BY SPACES
                     INTO
                     IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           ELSE
              MOVE BATCH-IN-ESECUZIONE TO BPA-COD-BATCH-STATE
              MOVE IDSV0003-USER-NAME  TO BPA-USER-START

              PERFORM ESTRAI-CURRENT-TIMESTAMP
                      THRU ESTRAI-CURRENT-TIMESTAMP-EX

              IF IDSV0003-SUCCESSFUL-RC
                 MOVE WS-TIMESTAMP-X    TO BPA-DT-START-DB
                 MOVE WS-TIMESTAMP-N    TO BPA-DT-START
              END-IF

           END-IF.

       A212-EX.
           EXIT.

       A213-PREPARA-UPD-FINALE.

           MOVE IDSV0003-USER-NAME    TO BPA-USER-START

           PERFORM ESTRAI-CURRENT-TIMESTAMP
                   THRU ESTRAI-CURRENT-TIMESTAMP-EX

           IF IDSV0003-SUCCESSFUL-RC
              MOVE WS-TIMESTAMP-X    TO BPA-DT-END-DB
              MOVE WS-TIMESTAMP-N    TO BPA-DT-END
           END-IF.

       A213-EX.
           EXIT.

       A230-UPDATE.

           PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE BTC_PARALLELISM SET
                   COD_BATCH_STATE  = :BPA-COD-BATCH-STATE
                  ,DT_START         = :BPA-DT-START-DB
                                      :IND-BPA-DT-START
                  ,DT_END           = :BPA-DT-END-DB
                                      :IND-BPA-DT-END
                  ,USER_START       = :BPA-USER-START
                                      :IND-BPA-USER-START
                WHERE COD_COMP_ANIA = :BPA-COD-COMP-ANIA
                  AND PROTOCOL      = :BPA-PROTOCOL
                  AND PROG_PROTOCOL = :BPA-PROG-PROTOCOL
                  AND COD_BATCH_STATE IN (
                                         :IABV0002-STATE-01,
                                         :IABV0002-STATE-02,
                                         :IABV0002-STATE-03,
                                         :IABV0002-STATE-04,
                                         :IABV0002-STATE-05,
                                         :IABV0002-STATE-06,
                                         :IABV0002-STATE-07,
                                         :IABV0002-STATE-08,
                                         :IABV0002-STATE-09,
                                         :IABV0002-STATE-10
                                         )
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       A230-EX.
           EXIT.
      *----
      *----  gestione FETCH
      *----
       A305-DECLARE-CURSOR.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE CURSOR-BPA CURSOR WITH HOLD FOR
             SELECT
                COD_COMP_ANIA
                ,PROTOCOL
                ,PROG_PROTOCOL
                ,COD_BATCH_STATE
                ,DT_INS
                ,USER_INS
                ,DT_START
                ,DT_END
                ,USER_START
                ,DESC_PARALLELISM
                ,ID_OGG_DA
                ,ID_OGG_A
                ,TP_OGG
                ,NUM_ROW_SCHEDULE
             FROM BTC_PARALLELISM
             WHERE     COD_COMP_ANIA = :BPA-COD-COMP-ANIA
                   AND PROTOCOL      = :BPA-PROTOCOL
           END-EXEC.

       A305-EX.
           EXIT.

       A360-OPEN-CURSOR.

           PERFORM A305-DECLARE-CURSOR       THRU A305-EX.

           EXEC SQL
                OPEN CURSOR-BPA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE    THRU A100-EX.

       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR.
           EXEC SQL
                CLOSE CURSOR-BPA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE       THRU A100-EX.
       A370-EX.
           EXIT.

       A375-CNTL.

           PERFORM A210-SELECT-PK   THRU A210-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              IF BPA-COD-BATCH-STATE = BATCH-IN-ESECUZIONE
                 MOVE SPACES         TO IDSV0003-DESCRIZ-ERR-DB2
                 SET IDSV0003-GENERIC-ERROR TO TRUE
                 MOVE BPA-COD-COMP-ANIA TO COMODO-COD-COMP-ANIA
                 MOVE BPA-PROG-PROTOCOL TO COMODO-PROG-PROTOCOL
                 STRING 'JOB PARALLELO GIA'' IN ESECUZIONE'
                     DELIMITED BY SIZE
                     COMODO-COD-COMP-ANIA
                     ' / '
                     DELIMITED BY SIZE
                     BPA-PROTOCOL
                     DELIMITED BY SPACES
                     ' / '
                     DELIMITED BY SIZE
                     COMODO-PROG-PROTOCOL
                     DELIMITED BY SPACES
                     INTO
                     IDSV0003-DESCRIZ-ERR-DB2
                 END-STRING
              END-IF
           ELSE
              MOVE SPACES            TO IDSV0003-DESCRIZ-ERR-DB2
              SET IDSV0003-SQL-ERROR TO TRUE
              MOVE BPA-COD-COMP-ANIA TO COMODO-COD-COMP-ANIA
              MOVE BPA-PROG-PROTOCOL TO COMODO-PROG-PROTOCOL
              STRING 'JOB PARALLELO NON ESEGUIBILE : '
                     DELIMITED BY SIZE
                     COMODO-COD-COMP-ANIA
                     ' / '
                     DELIMITED BY SIZE
                     BPA-PROTOCOL
                     DELIMITED BY SPACES
                     ' / '
                     DELIMITED BY SIZE
                     COMODO-PROG-PROTOCOL
                     DELIMITED BY SPACES
                     INTO
                     IDSV0003-DESCRIZ-ERR-DB2
              END-STRING
           END-IF.

       A375-EX.
           EXIT.

       A380-CNTL.

           SET BATCH-COMPLETE                   TO TRUE.
           SET BATCH-STATE-OK                   TO TRUE.
           SET IDSV0003-FETCH-FIRST             TO TRUE.

           MOVE BPA-PROG-PROTOCOL               TO WS-PROG-PROTOCOL.

           PERFORM A360-OPEN-CURSOR             THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM UNTIL
                      NOT IDSV0003-SUCCESSFUL-RC  OR
                          WK-FINE-LOOP-SI

                 PERFORM A390-FETCH-NEXT                 THRU A390-EX

                 EVALUATE TRUE
                     WHEN IDSV0003-SUCCESSFUL-SQL
                          PERFORM D000-CTRL-STATI-TOT
                                                THRU D000-EX
                          SET IDSV0003-FETCH-NEXT        TO TRUE

                     WHEN IDSV0003-NOT-FOUND
                          SET WK-FINE-LOOP-SI            TO TRUE
                          IF IDSV0003-FETCH-FIRST
                             CONTINUE
                          ELSE
                             SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                          END-IF

                 END-EVALUATE

              END-PERFORM
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT.
           EXEC SQL
                FETCH CURSOR-BPA
             INTO
                :BPA-COD-COMP-ANIA
               ,:BPA-PROTOCOL
               ,:BPA-PROG-PROTOCOL
               ,:BPA-COD-BATCH-STATE
               ,:BPA-DT-INS-DB
               ,:BPA-USER-INS
               ,:BPA-DT-START-DB
                :IND-BPA-DT-START
               ,:BPA-DT-END-DB
                :IND-BPA-DT-END
               ,:BPA-USER-START
                :IND-BPA-USER-START
               ,:BPA-DESC-PARALLELISM-VCHAR
                :IND-BPA-DESC-PARALLELISM
               ,:BPA-ID-OGG-DA
                :IND-BPA-ID-OGG-DA
               ,:BPA-ID-OGG-A
                :IND-BPA-ID-OGG-A
               ,:BPA-TP-OGG
                :IND-BPA-TP-OGG
               ,:BPA-NUM-ROW-SCHEDULE
                :IND-BPA-NUM-ROW-SCHEDULE
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.

       D000-CTRL-STATI-TOT.
      *
           IF IDSV0003-SUCCESSFUL-RC
              IF  BPA-COD-BATCH-STATE   = BATCH-IN-ESECUZIONE OR
                  BPA-COD-BATCH-STATE   = BATCH-DA-ESEGUIRE
                  SET BATCH-NOT-COMPLETE    TO TRUE
              END-IF

              IF BPA-COD-BATCH-STATE NOT = BATCH-ESEGUITO AND
                 BPA-COD-BATCH-STATE NOT = BATCH-SIMULATO-OK
                 SET BATCH-STATE-KO        TO TRUE
              END-IF
           END-IF.
      *
       D000-EX.

           EXIT.

       Z100-SET-COLONNE-NULL.

           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-BPA-DT-START = -1
              MOVE HIGH-VALUES TO BPA-DT-START-NULL
           END-IF
           IF IND-BPA-DT-END = -1
              MOVE HIGH-VALUES TO BPA-DT-END-NULL
           END-IF
           IF IND-BPA-USER-START = -1
              MOVE HIGH-VALUES TO BPA-USER-START-NULL
           END-IF
           IF IND-BPA-DESC-PARALLELISM = -1
              MOVE HIGH-VALUES TO BPA-DESC-PARALLELISM
           END-IF
           IF IND-BPA-ID-OGG-DA = -1
              MOVE HIGH-VALUES TO BPA-ID-OGG-DA-NULL
           END-IF
           IF IND-BPA-ID-OGG-A = -1
              MOVE HIGH-VALUES TO BPA-ID-OGG-A-NULL
           END-IF
           IF IND-BPA-TP-OGG = -1
              MOVE HIGH-VALUES TO BPA-TP-OGG-NULL
           END-IF
           IF IND-BPA-NUM-ROW-SCHEDULE = -1
              MOVE HIGH-VALUES TO BPA-NUM-ROW-SCHEDULE-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES.

           CONTINUE.
       Z150-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.

           IF BPA-DT-START-NULL = HIGH-VALUES
              MOVE -1 TO IND-BPA-DT-START
           ELSE
              MOVE 0 TO IND-BPA-DT-START
           END-IF
           IF BPA-DT-END-NULL = HIGH-VALUES
              MOVE -1 TO IND-BPA-DT-END
           ELSE
              MOVE 0 TO IND-BPA-DT-END
           END-IF
           IF BPA-USER-START-NULL = HIGH-VALUES
              MOVE -1 TO IND-BPA-USER-START
           ELSE
              MOVE 0 TO IND-BPA-USER-START
           END-IF
           IF BPA-DESC-PARALLELISM = HIGH-VALUES
              MOVE -1 TO IND-BPA-DESC-PARALLELISM
           ELSE
              MOVE 0 TO IND-BPA-DESC-PARALLELISM
           END-IF
           IF BPA-ID-OGG-DA-NULL = HIGH-VALUES
              MOVE -1 TO IND-BPA-ID-OGG-DA
           ELSE
              MOVE 0 TO IND-BPA-ID-OGG-DA
           END-IF
           IF BPA-ID-OGG-A-NULL = HIGH-VALUES
              MOVE -1 TO IND-BPA-ID-OGG-A
           ELSE
              MOVE 0 TO IND-BPA-ID-OGG-A
           END-IF
           IF BPA-TP-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-BPA-TP-OGG
           ELSE
              MOVE 0 TO IND-BPA-TP-OGG
           END-IF
           IF BPA-NUM-ROW-SCHEDULE-NULL = HIGH-VALUES
              MOVE -1 TO IND-BPA-NUM-ROW-SCHEDULE
           ELSE
              MOVE 0 TO IND-BPA-NUM-ROW-SCHEDULE
           END-IF.

       Z200-EX.
           EXIT.

      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE BPA-DT-INS TO WS-TIMESTAMP-N
           PERFORM Z701-TS-N-TO-X   THRU Z701-EX
           MOVE WS-TIMESTAMP-X   TO BPA-DT-INS-DB
           IF IND-BPA-DT-START = 0
               MOVE BPA-DT-START TO WS-TIMESTAMP-N
               PERFORM Z701-TS-N-TO-X   THRU Z701-EX
               MOVE WS-TIMESTAMP-X      TO BPA-DT-START-DB
           END-IF
           IF IND-BPA-DT-END = 0
               MOVE BPA-DT-END TO WS-TIMESTAMP-N
               PERFORM Z701-TS-N-TO-X   THRU Z701-EX
               MOVE WS-TIMESTAMP-X      TO BPA-DT-END-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.

           MOVE BPA-DT-INS-DB TO WS-TIMESTAMP-X
           PERFORM Z801-TS-X-TO-N     THRU Z801-EX
           MOVE WS-TIMESTAMP-N      TO BPA-DT-INS
           IF IND-BPA-DT-START = 0
               MOVE BPA-DT-START-DB TO WS-TIMESTAMP-X
               PERFORM Z801-TS-X-TO-N     THRU Z801-EX
               MOVE WS-TIMESTAMP-N      TO BPA-DT-START
           END-IF
           IF IND-BPA-DT-END = 0
               MOVE BPA-DT-END-DB TO WS-TIMESTAMP-X
               PERFORM Z801-TS-X-TO-N     THRU Z801-EX
               MOVE WS-TIMESTAMP-N      TO BPA-DT-END
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           MOVE LENGTH OF BPA-DESC-PARALLELISM
                       TO BPA-DESC-PARALLELISM-LEN.

       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0014 END-EXEC.
           EXEC SQL INCLUDE IDSP0015 END-EXEC.


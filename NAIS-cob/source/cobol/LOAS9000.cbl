************************************************************************
********                                                        ********
********                                                        ********
********                                                        ********
********                                                        ********
************************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LOAS9000.
       AUTHOR.   ATS NAPOLI.
       DATE-WRITTEN.  AGOSTO 2008.
       DATE-COMPILED.
      *REMARKS.
      *같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같�
      *    PROGRAAMMA .... LOAS9000
      *    FUNZIONE ......
      *같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같�
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
       DATA DIVISION.
      *************************************************
      * W O R K I N G   S T O R A G E   S E C T I O N *
      *************************************************
       WORKING-STORAGE SECTION.
       77 WK-PGM                          PIC X(08) VALUE 'LOAS9000'.
       77 WK-PGM-GUIDA                    PIC X(08) VALUE 'LDBS3540'.
       77 WK-PGM-BUSINESS                 PIC X(08) VALUE 'LOAS0310'.

       77 WK-LABEL                        PIC X(30) VALUE SPACES.

      ******************************************************************
      * COSTANTI
      ******************************************************************
       77 GARANZIA                        PIC X(02) VALUE 'GA'.


      ******************************************************************
      * FLAGS
      ******************************************************************
       01 FLAG-TP-OBL                     PIC X(02).
          88 OBL-POLIZZA                  VALUE 'PO'.
          88 OBL-ADESIONE                 VALUE 'AD'.

       01 FLAG-TP-OGG-MBS                 PIC X(02).
          88 MBS-POLIZZA                  VALUE 'PO'.
          88 MBS-ADESIONE                 VALUE 'AD'.

       01 FLAG-TP-FRM-ASSVA               PIC X(02).
          88 INDIVIDUALE                  VALUE 'IN'.
          88 COLLETTIVA                   VALUE 'CO'.

       01 FLAG-FINE-GUIDE                 PIC X(01).
          88 WK-FINE-GUIDE-YES            VALUE 'Y'.
          88 WK-FINE-GUIDE-NO             VALUE 'N'.



      ******************************************************************
      * INDICI
      ******************************************************************
       01 IX-TAB-PMO                      PIC S9(04) COMP VALUE 0.

      ******************************************************************
      * LIMITI
      ******************************************************************
      *01 WK-LIMITE-MAX-PMO               PIC S9(4) VALUE 100.
AZ     01 WK-LIMITE-MAX-PMO               PIC S9(4) VALUE 10.

      *----------------------------------------------------------------*
      *       COPY VARIBILI PER LA GESTIONE ERRORI
      *----------------------------------------------------------------*
      *
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IEAV9903.
           COPY IDSV0002.

      ******************************************************************
      * COPY INFRASTRUTTURALE PER BATCH
      ******************************************************************

           COPY IDBVPMO1.

           COPY IDSV0003.

           COPY IABV0004.


       01 AREA-MAIN.
      * -- Area parametro movimento
          03 WPMO-AREA-PARAM-MOVI.
             04 WPMO-ELE-PMO-MAX         PIC S9(04) COMP.
      *      04 WPMO-TAB-PMO             OCCURS 100.
                COPY LCCVPMOA REPLACING   ==(SF)==  BY ==WPMO==.
             COPY LCCVPMO1               REPLACING ==(SF)== BY ==WPMO==.
      * -- Area polizza
          03 WPOL-AREA-POLIZZA.
             04 WPOL-ELE-POL-MAX         PIC S9(04) COMP.
             04 WPOL-TAB-POL.
             COPY LCCVPOL1               REPLACING ==(SF)== BY ==WPOL==.
      * -- Area Adesione
          03 WADE-AREA-ADESIONE.
             04 WADE-ELE-ADE-MAX         PIC S9(04) COMP.
             04 WADE-TAB-ADE             OCCURS 1.
             COPY LCCVADE1               REPLACING ==(SF)== BY ==WADE==.
      * --> Area Movimento
          03 WMOV-AREA-MOVIMENTO.
              04 WMOV-ELE-MOVI-MAX        PIC S9(04) COMP.
              04 WMOV-TAB-MOV.
              COPY LCCVMOV1              REPLACING ==(SF)== BY ==WMOV==.

      *----------------------------------------------------------------*
      * -- INTERVALLO DI ELABORAZIONE
      *----------------------------------------------------------------*
       01 WCOM-INTERVALLO-ELAB.
           03 WCOM-PERIODO-ELAB-DA      PIC 9(08).
           03 WCOM-PERIODO-ELAB-A       PIC 9(08).

      *----------------------------------------------------------------*
      * AREA PROFILAZIONE
      *----------------------------------------------------------------*
      *
       01  WCOM-IO-STATI.
           03 WCOM-AREA-STATI.
              COPY LCCC0001              REPLACING ==(SF)== BY ==WCOM==.
           03 WCOM-AREA-LCCC0261.
              COPY LCCC0261              REPLACING ==(SF)== BY ==WCOM==.

      ******************************************************************
      * MAPPATURA DI MBS-D-INPUT-MOVI-SOSP
      ******************************************************************
           COPY IDSV0901.

      * ----------------------------------------------------------------
       LINKAGE SECTION.

       01  IDSV0001.
           COPY IDSV0001.

       01  IABV0006.
           COPY IABV0006.

           COPY IABV0002.

      * ----------------------------------------------------------------
      * AREA DATI    ( BLOB da 1M)
      * ----------------------------------------------------------------
           COPY IABV0003.

           COPY IDBVL111.

           COPY IDBVMBS1.

      ******************************************************************
      * P R O C E D U R E   D I V I S I O N                            *
      ******************************************************************
       PROCEDURE DIVISION USING           IDSV0001
                                          IABV0006
                                          IABV0002
                                          IABV0003
                                          OGG-BLOCCO
                                          MOVI-BATCH-SOSP.

           MOVE 'PROCEDURE DIVISION'       TO WK-LABEL.

           PERFORM A000-OPERAZ-INIZ        THRU A000-EX.

           PERFORM A020-VALORIZZA-IDSV0003 THRU A020-EX.

           PERFORM B000-ELABORA            THRU B000-EX.

           GOBACK.
      *----------------------------------------------------------------*
      * OPERAZIONI INIZIALI                                            *
      *----------------------------------------------------------------*
       A000-OPERAZ-INIZ.
      *
           MOVE 'A000-OPERAZ-INIZ'           TO WK-LABEL.
      *
           PERFORM A050-INITIALIZE           THRU A050-EX.

           PERFORM A030-IDENTIFICA-TIPOLOGIE THRU A030-EX.
      *
       A000-EX.
           EXIT.

      *----------------------------------------------------------------*
      * VALORIZZA AREA IDSV0003
      *----------------------------------------------------------------*
       A020-VALORIZZA-IDSV0003.
      *
           MOVE 'A020-VALORIZZA-IDSV0003'        TO WK-LABEL.
      *
           MOVE IDSV0001-MODALITA-ESECUTIVA
                                     TO IDSV0003-MODALITA-ESECUTIVA

           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                     TO IDSV0003-CODICE-COMPAGNIA-ANIA

           MOVE IDSV0001-COD-MAIN-BATCH
                                     TO IDSV0003-COD-MAIN-BATCH

           MOVE IDSV0001-TIPO-MOVIMENTO
                                     TO IDSV0003-TIPO-MOVIMENTO

           MOVE IDSV0001-USER-NAME   TO IDSV0003-USER-NAME

           MOVE IDSV0001-SESSIONE    TO IDSV0003-SESSIONE

           MOVE IDSV0001-TRATTAMENTO-STORICITA
                                     TO IDSV0003-TRATTAMENTO-STORICITA

           MOVE IDSV0001-FORMATO-DATA-DB
                                     TO IDSV0003-FORMATO-DATA-DB

           MOVE IDSV0001-DATA-EFFETTO
                                     TO IDSV0003-DATA-INIZIO-EFFETTO

           MOVE IDSV0001-DATA-COMPETENZA
                                     TO IDSV0003-DATA-COMPETENZA

           MOVE IDSV0001-DATA-COMP-AGG-STOR
                                      TO IDSV0003-DATA-COMP-AGG-STOR.
      *
       A020-EX.
           EXIT.
      *----------------------------------------------------------------*
      * IDENTIFICA TIPOLOGIE DI OBL ED MBS
      *----------------------------------------------------------------*
       A030-IDENTIFICA-TIPOLOGIE.
      *
           MOVE 'A030-IDENTIFICA-TIPOLOGIE' TO WK-LABEL.

           MOVE L11-TP-OGG                  TO FLAG-TP-OBL.

           MOVE MBS-TP-OGG                  TO FLAG-TP-OGG-MBS.

           MOVE MBS-TP-FRM-ASSVA            TO FLAG-TP-FRM-ASSVA.
      *
       A030-EX.
           EXIT.
      *----------------------------------------------------------------*
      * INIZIALIZZA AREE DI WORKING                                    *
      *----------------------------------------------------------------*
       A050-INITIALIZE.
      *
           MOVE ZEROES                    TO IX-TAB-PMO.
           SET IDSV0001-ESITO-OK          TO TRUE.
           SET WK-FINE-GUIDE-NO           TO TRUE.

           INITIALIZE                     IDSV0901
                                          WCOM-INTERVALLO-ELAB
                                          WCOM-IO-STATI
                                          WK-LABEL.

AZ         SET ALPO-CALL                  TO TRUE.
AZ         MOVE MBS-ID-MOVI               TO WCOM-ID-MOVI-CREAZ.
AZ         MOVE MBS-ID-OGG-BLOCCO         TO WCOM-ID-BLOCCO-CRZ.

           COPY IERP0001.
      *
       A050-EX.
           EXIT.

      *----------------------------------------------------------------*
      * ELABORAZIONE                                                   *
      *----------------------------------------------------------------*
       B000-ELABORA.

           IF IABV0006-GUIDE-AD-HOC
              PERFORM E000-ESTRAI-DATI          THRU E000-EX
           END-IF.

           IF IDSV0001-ESITO-OK
              PERFORM E500-ELABORA-BUSINESS     THRU E500-EX
           END-IF.

       B000-EX.
           EXIT.

      *----------------------------------------------------------------*
      * ELABORAZIONE                                                   *
      *----------------------------------------------------------------*
       E000-ESTRAI-DATI.

           MOVE 'E000-ESTRAI-DATI'        TO WK-LABEL.

           PERFORM E100-PREPARA-GUIDE     THRU E100-EX

           PERFORM E200-ACCEDI-GUIDE      THRU E200-EX.

       E000-EX.
           EXIT.

      *----------------------------------------------------------------*
      * PREPARA WHERE X GUIDE SERVICE                                  *
      *----------------------------------------------------------------*
       E100-PREPARA-GUIDE.
      *
           MOVE 'E100-PREPARA-GUIDE' TO WK-LABEL.

           SET IDSV0003-WHERE-CONDITION-01 TO TRUE

           MOVE HIGH-VALUES          TO PARAM-MOVI

           MOVE MBS-COD-COMP-ANIA    TO PMO-COD-COMP-ANIA
           MOVE MBS-TP-MOVI          TO PMO-TP-MOVI
           MOVE GARANZIA             TO PMO-TP-OGG
           MOVE MBS-TP-FRM-ASSVA     TO PMO-TP-FRM-ASSVA

           MOVE L11-ID-OGG-1RIO      TO PMO-ID-POLI

           MOVE MBS-ID-OGG           TO PMO-ID-ADES
           MOVE MBS-DT-EFF           TO PMO-DT-RICOR-SUCC

           MOVE MBS-DT-EFF           TO PMO-DT-INI-EFF
                                        PMO-DT-END-EFF
           MOVE MBS-DS-TS-CPTZ       TO PMO-DS-TS-INI-CPTZ
                                        PMO-DS-TS-END-CPTZ.
      *
       E100-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESTRAZIONE DATI DA ELABORARE
      *----------------------------------------------------------------*
       E200-ACCEDI-GUIDE.

           MOVE 'E200-ACCEDI-GUIDE'          TO WK-LABEL.

           SET IDSV0003-SUCCESSFUL-RC        TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE

           SET WK-FINE-GUIDE-NO              TO TRUE

           SET IDSV0003-WHERE-CONDITION-01   TO TRUE

           SET IDSV0003-FETCH-FIRST          TO TRUE

           PERFORM UNTIL WK-FINE-GUIDE-YES         OR
                        NOT IDSV0003-SUCCESSFUL-RC OR
                        NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM CALL-PGM-GUIDA         THRU CALL-PGM-GUIDA-EX
      *
                 IF IDSV0003-SUCCESSFUL-RC
                    EVALUATE TRUE
                        WHEN IDSV0003-SUCCESSFUL-SQL
      *-->              OPERAZIONE ESEGUITA CORRETTAMENTE
                           PERFORM E300-CARICA-DATI-ESTRATTI
                                                          THRU E300-EX
                           SET IDSV0003-FETCH-NEXT        TO TRUE

                        WHEN IDSV0003-NOT-FOUND
      *---   >          CAMPO $ NON TROVATO
                           IF IDSV0003-FETCH-FIRST
                              MOVE WK-PGM   TO IEAI9901-COD-SERVIZIO-BE
                              MOVE WK-LABEL TO IEAI9901-LABEL-ERR
                              MOVE '5166'   TO IEAI9901-COD-ERRORE

                              STRING 'DATI NON TROVATI SU '
                                     WK-PGM-GUIDA
                                     DELIMITED BY SIZE
                                     INTO IEAI9901-PARAMETRI-ERR
                              END-STRING

                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                                           THRU EX-S0300
                              SET WK-FINE-GUIDE-YES        TO TRUE
                           END-IF

                        WHEN OTHER
                             MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                             MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                             MOVE '5166'     TO IEAI9901-COD-ERRORE

                             STRING 'ERRORE MODULO '
                                     WK-PGM-GUIDA
                                     ' - '
                                     'SQLCODE '
                                     IDSV0003-SQLCODE
                                     DELIMITED BY SIZE
                                     INTO IEAI9901-PARAMETRI-ERR
                             END-STRING

                             PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                                           THRU EX-S0300

                    END-EVALUATE

                 ELSE
      *-->          GESTIRE ERRORE
                    MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                    MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                    MOVE '5166'     TO IEAI9901-COD-ERRORE

                    STRING 'ERRORE MODULO '
                            WK-PGM-GUIDA
                            ' - '
                            'RC '
                            IDSV0003-RETURN-CODE
                            DELIMITED BY SIZE
                            INTO IEAI9901-PARAMETRI-ERR
                    END-STRING

                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                                  THRU EX-S0300

                 END-IF

           END-PERFORM.

       E200-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CARICA DATI ESTRATTI
      *----------------------------------------------------------------*
       E300-CARICA-DATI-ESTRATTI.
      *
           MOVE 'E300-CARICA-DATI-ESTRATTI' TO WK-LABEL.

           IF IX-TAB-PMO < WK-LIMITE-MAX-PMO
              ADD 1                            TO IX-TAB-PMO
              MOVE IX-TAB-PMO                  TO WPMO-ELE-PMO-MAX
              SET WPMO-ST-INV(IX-TAB-PMO)      TO TRUE
              PERFORM VALORIZZA-OUTPUT-PMO
                      THRU VALORIZZA-OUTPUT-PMO-EX
           ELSE
              MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
              MOVE '5166'     TO IEAI9901-COD-ERRORE

              STRING 'OVERFLOW DATI GUIDA : '
                      WK-PGM-GUIDA
                      DELIMITED BY SIZE
                      INTO IEAI9901-PARAMETRI-ERR
              END-STRING

              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                            THRU EX-S0300
           END-IF.
      *
       E300-EX.
           EXIT.

      *----------------------------------------------------------------*
      * ELABORAZIONE BUSINESS
      *----------------------------------------------------------------*
       E500-ELABORA-BUSINESS.
      *
           MOVE 'E500-ELABORA-BUSINESS'   TO WK-LABEL.

           PERFORM E600-PRE-BUSINESS      THRU E600-EX.

           PERFORM CALL-PGM-BUSINESS      THRU CALL-PGM-BUSINESS-EX.

           PERFORM E700-POST-BUSINESS     THRU E700-EX.
      *
       E500-EX.
           EXIT.

      *----------------------------------------------------------------*
      * prepara lancio del BUSINESS
      *----------------------------------------------------------------*
       E600-PRE-BUSINESS.
      *
           MOVE 'E600-PRE-BUSINESS'         TO WK-LABEL.

           MOVE MBS-D-INPUT-MOVI-SOSP       TO IDSV0901.

           MOVE MBS-DT-EFF                  TO WCOM-PERIODO-ELAB-DA
                                               WCOM-PERIODO-ELAB-A

           MOVE IABV0006-COD-GRU-AUT-PROFIL TO WCOM-COD-GRU-AUT-PROFIL
           MOVE IABV0006-COD-LIV-AUT-PROFIL TO WCOM-COD-LIV-AUT-PROFIL
           MOVE IABV0006-CANALE-VENDITA     TO WCOM-CANALE-VENDITA
           MOVE IABV0006-PUNTO-VENDITA      TO WCOM-PUNTO-VENDITA

           IF MBS-ADESIONE
              MOVE MBS-ID-OGG               TO WCOM-ID-ADES
           END-IF

           MOVE L11-ID-OGG-1RIO             TO WCOM-ID-POLIZZA.
      *
       E600-EX.
           EXIT.

      *----------------------------------------------------------------*
      * POST lancio del BUSINESS
      *----------------------------------------------------------------*
       E700-POST-BUSINESS.
      *
           MOVE 'E700-POST-BUSINESS'         TO WK-LABEL.

           IF IDSV0001-ESITO-OK
              PERFORM E800-AGGIORNA-JOB      THRU E800-EX
           END-IF.
      *
       E700-EX.
           EXIT.

      *----------------------------------------------------------------*
      * ESTRAZIONE DATI DA ELABORARE
      *----------------------------------------------------------------*
       E800-AGGIORNA-JOB.

           MOVE 'E800-AGGIORNA-JOB'          TO WK-LABEL.

           SET IDSV0003-WHERE-CONDITION-01   TO TRUE

           SET IDSV0003-UPDATE               TO TRUE
           MOVE JOB-ESEGUITO-OK              TO IABV0002-STATE-CURRENT

           PERFORM CALL-PGM-GUIDA         THRU CALL-PGM-GUIDA-EX
      *
           IF IDSV0003-SUCCESSFUL-RC
              EVALUATE TRUE
                 WHEN IDSV0003-SUCCESSFUL-SQL
      *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                      CONTINUE

                 WHEN OTHER
                      MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                      MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
                      MOVE '5166'     TO IEAI9901-COD-ERRORE

                      STRING 'ERRORE MODULO '
                              WK-PGM-GUIDA
                              ' - '
                              'SQLCODE '
                              IDSV0003-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                      END-STRING

                      PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                                    THRU EX-S0300

                 END-EVALUATE

           ELSE
      *-->          GESTIRE ERRORE
              MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
              MOVE '5166'     TO IEAI9901-COD-ERRORE

              STRING 'ERRORE MODULO '
                      WK-PGM-GUIDA
                      ' - '
                      'RC '
                      IDSV0003-RETURN-CODE
                      DELIMITED BY SIZE
                      INTO IEAI9901-PARAMETRI-ERR
              END-STRING

              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                            THRU EX-S0300

           END-IF.

       E800-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CALL AL MODULO GUIDA
      *----------------------------------------------------------------*
       CALL-PGM-GUIDA.

           MOVE 'CALL-PGM-GUIDA'        TO WK-LABEL.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET IDSV0003-SUCCESSFUL-RC   TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL  TO TRUE.

           CALL WK-PGM-GUIDA      USING IDSV0003
                                        IABV0002
                                        PARAM-MOVI
           ON EXCEPTION
              MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL   TO IEAI9901-LABEL-ERR
              MOVE '5166'     TO IEAI9901-COD-ERRORE

              STRING 'ERRORE CHIAMATA MODULO : '
                      DELIMITED BY SIZE
                      WK-PGM-GUIDA
                      DELIMITED BY SIZE
                      ' - '
                       DELIMITED BY SIZE INTO
                      IEAI9901-PARAMETRI-ERR
              END-STRING

              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                            THRU EX-S0300
           END-CALL.

       CALL-PGM-GUIDA-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CALL AL MODULO BUSINESS
      *----------------------------------------------------------------*
       CALL-PGM-BUSINESS.

           MOVE 'CALL-PGM-BUSINESS'        TO WK-LABEL.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET IDSV0001-ESITO-OK           TO TRUE.

           CALL WK-PGM-BUSINESS      USING IDSV0001
                                           IABV0006
                                           AREA-MAIN
                                           WCOM-INTERVALLO-ELAB
                                           WCOM-IO-STATI
           ON EXCEPTION
              STRING 'ERRORE CHIAMATA MODULO : '
                      DELIMITED BY SIZE
                      WK-PGM-BUSINESS
                      DELIMITED BY SIZE
                      ' - '
                       DELIMITED BY SIZE INTO
                      IEAI9901-PARAMETRI-ERR
              END-STRING

              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                            THRU EX-S0300
           END-CALL.

       CALL-PGM-BUSINESS-EX.
           EXIT.

      *----------------------------------------------------------------*
      * COPY VALORIZZA OUTPUT TABELLE
      *----------------------------------------------------------------*
      *
      * --> PARAMETRO MOVIMENTO
           COPY LCCVPMO3               REPLACING ==(SF)== BY ==WPMO==.
           COPY LCCVPMO4               REPLACING ==(SF)== BY ==WPMO==.

      *----------------------------------------------------------------*
      *    ROUTINES GESTIONE ERRORE
      *----------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.

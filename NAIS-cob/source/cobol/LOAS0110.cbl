      *****************************************************************
      **                                                              *
      **    PORTAFOGLIO VITA ITALIA VER. 1.0                          *
      **                                                              *
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LOAS0110.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED. 12-Feb-08 22:04.
      * **-----------------------------------------------------------**
      * **-----------------------------------------------------------**
      * **-----------------------------------------------------------**
      * **  PROGRAMMA ..... LOAS0110                                 **
      * **  TIPOLOGIA...... DRIVER EOC                               **
      * **  PROCESSO....... XXX                                      **
      * **  FUNZIONE....... XXX                                      **
      * **  DESCRIZIONE.... EOC COMUNE - OPERAZIONI AUTOMATICHE -    **
      * **-----------------------------------------------------------**
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------
      *     VARIABILI DI WORKING
      *----------------------------------------------------------------
       01 WK-PGM                           PIC X(008) VALUE 'LOAS0110'.
       01 WK-TABELLA                       PIC X(030) VALUE SPACES.
      *  ---------------------------------------------------------------
      *     INDICI
      *  ---------------------------------------------------------------
       01 IX-TAB-PMO                     PIC S9(04) COMP VALUE 0.
      * ----------------------------------------------------------------
      *       COPY VARIBILI PER LA GESTIONE ERRORI
      * ----------------------------------------------------------------
           COPY IEAI9901.
           COPY IEAO9901.
           COPY IDSV0002.

      *----------------------------------------------------------------*
      *    CAMPI X ROUTINES DI GESTIONE DISPLAY IDSP8888
      *----------------------------------------------------------------*
           COPY IDSV8888.

      * ----------------------------------------------------------------
      *   AREA ESTRAZIONE SEQUANCE
      * ----------------------------------------------------------------
       01  AREA-IO-LCCS0090.
           COPY LCCC0090                 REPLACING ==(SF)== BY ==S090==.
      *  ESTRAZIONE SEQUENCE
       01  LCCS0090                         PIC X(008) VALUE 'LCCS0090'.
      *  ESTRAZIONE OGGETTO PTF
       01  LCCS0234                         PIC X(008) VALUE 'LCCS0234'.
      *  INSERIMENTO BLOCCO
       01  LCCS0024                         PIC X(008) VALUE 'LCCS0024'.
      *  MODULO EOC COMUNE
       01  LCCS0005                         PIC X(008) VALUE 'LCCS0005'.
      * ---------------------------------------------------------------
      *    COPY DISPATCHER
      * ---------------------------------------------------------------
       01  IDSV0012.
           COPY IDSV0012.
       01  DISPATCHER-VARIABLES.
           COPY IDSI0011.
      * COPY DI OUTPUT DALLA CHIAMATA DEL MODULO DISPATCHER
           COPY IDSO0011.
      *----------------------------------------------------------------*
      *    COPY DELLE TABELLE DB2
      *----------------------------------------------------------------*
      *--> PARAMETRO MOVIMENTO
           COPY IDBVPMO1.
      *----------------------------------------------------------------*
      *    ALTRE AREE
      *----------------------------------------------------------------*
       01  WK-TP-OGG-MBS                   PIC X(02).
       01  WK-LABEL-ERR                    PIC X(030) VALUE SPACES.
      * --  FLAG VENDITA - POST VENDITA
       01 WCOM-FLAG-CONVERSAZIONE           PIC X(001).
             88 WCOM-VENDITA                  VALUE 'V'.
             88 WCOM-POST-VENDITA             VALUE 'P'.
      *----------------------------------------------------------------*
      * MODULI CHIAMATI
      *----------------------------------------------------------------*
      *  --> WS-MOVIMENTO
           COPY LCCC0006.
           COPY LCCVXOG0.
           COPY LCCVXMV0.
      *-----------------------------------------------------------------
      *    AREA EOC COMUNE
      *-----------------------------------------------------------------
       01  AREA-IO-LCCS0005.
      *--  Interfaccia servizio
           COPY LCCV0005.

      *--  AREA RAPP-RETE
       01  WRRE-AREA-RAP-RETE.
           04 WRRE-ELE-RAP-RETE-MAX   PIC S9(04) COMP.
           04 WRRE-TAB-RRE            OCCURS 20.
           COPY LCCVRRE1              REPLACING ==(SF)== BY ==WRRE==.

      *--  AREA NOTE OGGETTO
       01  WNOT-AREA-NOTE-OGG.
           04 WNOT-ELE-NOTE-OGG-MAX   PIC S9(004) COMP.
           04 WNOT-TAB-NOTE-OGG.
           COPY LCCVNOT1              REPLACING ==(SF)== BY ==WNOT==.

      *----------------------------------------------------------------*
      * AREE MODULO LCCS0234
      *----------------------------------------------------------------*
      *--> AREA I/O LCCS0234
       01 S234-DATI-INPUT.
            04 S234-ID-OGG-EOC         PIC S9(009).
            04 S234-TIPO-OGG-EOC       PIC X(002).
       01 S234-DATI-OUTPUT.
            04 S234-ID-OGG-PTF-EOC     PIC S9(009).
            04 S234-IB-OGG-PTF-EOC     PIC X(040).
            04 S234-ID-POLI-PTF        PIC S9(9).
            04 S234-ID-ADES-PTF        PIC S9(9).
      *----------------------------------------------------------------*
      * AREE MODULO LCCS0024 GESTIONE BLOCCO
      *----------------------------------------------------------------*
       01  S0024-AREA-COMUNE.
           03 S0024-AREA-STATI.
              COPY LCCC0001            REPLACING ==(SF)== BY ==S0024==.
       01  AREA-IO-LCCS0024.
           COPY LCCC0024.
      * ---------------------------------------------------------------*
       LINKAGE SECTION.
      * ---------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.
      *
       01 WADE-AREA-ADESIONE.
          04 WADE-ELE-ADES-MAX       PIC S9(04) COMP.
          04 WADE-TAB-ADES.
          COPY LCCVADE1              REPLACING ==(SF)== BY ==WADE==.
      *
       01 WGRZ-AREA-GARANZIA.
          04 WGRZ-ELE-GAR-MAX        PIC S9(04) COMP.
                COPY LCCVGRZB REPLACING   ==(SF)==  BY ==WGRZ==.
          COPY LCCVGRZ1              REPLACING ==(SF)== BY ==WGRZ==.
      *
       01 WMOV-AREA-MOVIMENTO.
          04 WMOV-ELE-MOVI-MAX       PIC S9(04) COMP.
          04 WMOV-TAB-MOVI.
          COPY LCCVMOV1              REPLACING ==(SF)== BY ==WMOV==.
      *
       01 WPMO-AREA-PARAM-MOVI.
          04 WPMO-ELE-PARAM-MOV-MAX PIC S9(04) COMP.
                COPY LCCVPMOB REPLACING   ==(SF)==  BY ==WPMO==.
          COPY LCCVPMO1              REPLACING ==(SF)== BY ==WPMO==.
      *
       01 WPOL-AREA-POLIZZA.
          04 WPOL-ELE-POLI-MAX       PIC S9(04) COMP.
          04 WPOL-TAB-POLI.
          COPY LCCVPOL1              REPLACING ==(SF)== BY ==WPOL==.
      *
       01 WRIC-AREA-RICHIESTA.
          04 WRIC-ELE-RICH-MAX            PIC S9(004) COMP.
          04 WRIC-TAB-RICH.
             COPY LCCVRIC1           REPLACING ==(SF)== BY ==WRIC==.
      *
       01 WTGA-AREA-TRANCHE.
          04 WTGA-ELE-TRAN-MAX       PIC S9(04) COMP.
                COPY LCCVTGAB REPLACING   ==(SF)==  BY ==WTGA==.
          COPY LCCVTGA1              REPLACING ==(SF)== BY ==WTGA==.
      *
       01  AREA-PASSAGGIO.
           03 WCOM-AREA-STATI.
              COPY LCCC0001          REPLACING ==(SF)== BY ==WCOM==.
           03 WK-AREA-LCCC0261.
              COPY LCCC0261          REPLACING ==(SF)== BY ==WCOM==.
      *
A3227  01 IABV0006.
A3227     COPY IABV0006.
      * ---------------------------------------------------------------*
       PROCEDURE DIVISION           USING AREA-IDSV0001
                                          WADE-AREA-ADESIONE
                                          WGRZ-AREA-GARANZIA
                                          WMOV-AREA-MOVIMENTO
                                          WPMO-AREA-PARAM-MOVI
                                          WPOL-AREA-POLIZZA
                                          WRIC-AREA-RICHIESTA
                                          WTGA-AREA-TRANCHE
                                          AREA-PASSAGGIO
A3227                                     IABV0006.
      * ---------------------------------------------------------------*
      *
           MOVE IDSV0001-TIPO-MOVIMENTO TO WS-MOVIMENTO.
           SET WCOM-POST-VENDITA        TO TRUE.

           PERFORM S0000B-OPERAZIONI-INIZIALI
              THRU EX-S0000B

           IF  IDSV0001-ESITO-OK
A3227      AND IABV0006-SIMULAZIONE-NO
              PERFORM S1000B-ELABORAZIONE
                 THRU EX-S1000B
A3227      END-IF.

           PERFORM S9000B-OPERAZIONI-FINALI
              THRU EX-S9000B
      *
           GOBACK.
      * ---------------------------------------------------------------*
      *                      OPERAZIONI INIZIALI                       *
      * ---------------------------------------------------------------*
      *
       S0000B-OPERAZIONI-INIZIALI.
      *
             MOVE 'S0000B-OPERAZIONI-INIZIALI'
               TO WK-LABEL-ERR.

             PERFORM S0100B-INITIALIZE-AREE
                THRU EX-S0100B.
      *
             MOVE IDSV0001-TIPO-MOVIMENTO    TO WS-MOVIMENTO.
      *
             PERFORM S0200-CTRL-DATI  THRU EX-S0200.

       EX-S0000B.
           EXIT.

      *
      *----------------------------------------------------------------*
      * INIZIALIZZAZIONE AREE DI WORKING DEL PROGRAMMA
      *----------------------------------------------------------------*
       S0100B-INITIALIZE-AREE.

           MOVE 'S0100B-INITIALIZE-AREE'        TO WK-LABEL-ERR.

      *--> PARAGRAFO DI INIZIALIZZAZIONE

           MOVE ZEROES
             TO IX-TAB-PMO.

       EX-S0100B.
           EXIT.
      *
      *----------------------------------------------------------------*
      *  CONTROLLI
      *----------------------------------------------------------------*
       S0200-CTRL-DATI.
             MOVE 'S0200-CTRL-DATI' TO WK-LABEL-ERR.

             IF WCOM-STEP-ELAB NOT NUMERIC
                MOVE WK-PGM
                  TO IEAI9901-COD-SERVIZIO-BE
                MOVE WK-LABEL-ERR
                 TO IEAI9901-LABEL-ERR
                MOVE '001114'
                  TO IEAI9901-COD-ERRORE
                STRING 'STEP DI ELABORAZIONE NON VALORIZZATO'
                   DELIMITED BY SIZE
                   INTO IEAI9901-PARAMETRI-ERR
                END-STRING
                PERFORM S0300-RICERCA-GRAVITA-ERRORE
                   THRU EX-S0300
             ELSE

                IF WCOM-STEP-ELAB NOT = (1 AND 2)
                   MOVE WK-PGM
                     TO IEAI9901-COD-SERVIZIO-BE
                   MOVE WK-LABEL-ERR
                    TO IEAI9901-LABEL-ERR
                   MOVE '001114'
                     TO IEAI9901-COD-ERRORE
                   STRING 'STEP DI ELABORAZIONE VALORE NON AMMESSO'
                      DELIMITED BY SIZE
                      INTO IEAI9901-PARAMETRI-ERR
                   END-STRING
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
                END-IF

             END-IF.

             IF WCOM-WRITE-ERR

                PERFORM S0210-CTRL-DATI-X-ERR
                   THRU EX-S0210

             END-IF.

       EX-S0200.
           EXIT.
      *----------------------------------------------------------------*
      *  CONTROLLI
      *----------------------------------------------------------------*
       S0210-CTRL-DATI-X-ERR.

           IF WPOL-ELE-POLI-MAX = 0

              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
               TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              STRING 'OCORRENZA POLIZZA NON VALORIZZATA'
                 DELIMITED BY SIZE
                 INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300

           END-IF.

           IF WCOM-ID-POLIZZA = ZEROES         OR
              WCOM-ID-POLIZZA   NOT NUMERIC

              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
               TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              STRING 'ID POLIZZA NON VALORIZZATO'
                 DELIMITED BY SIZE
                 INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300

           END-IF.

           IF WCOM-ADESIONE-MBS

              IF WCOM-ID-ADES    = ZEROES         OR
                 WCOM-ID-POLIZZA   NOT NUMERIC

                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR
                  TO IEAI9901-LABEL-ERR
                 MOVE '001114'
                   TO IEAI9901-COD-ERRORE
                 STRING 'ID ADESIONE NON VALORIZZATO'
                    DELIMITED BY SIZE
                    INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300

              END-IF

              IF WADE-ELE-ADES-MAX = 0

                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR
                  TO IEAI9901-LABEL-ERR
                 MOVE '001114'
                   TO IEAI9901-COD-ERRORE
                 STRING 'OCORRENZA ADESIONE NON VALORIZZATA'
                    DELIMITED BY SIZE
                    INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300

              END-IF

           END-IF.

       EX-S0210.
           EXIT.
      * ---------------------------------------------------------------*
      *                         ELABORAZIONE                           *
      * ---------------------------------------------------------------*
      *
       S1000B-ELABORAZIONE.

           IF IDSV0001-ESITO-OK
              PERFORM S1300-PREPARA-LCCS0005
                 THRU S1300-PREPARA-LCCS0005-EX
           END-IF.

           IF IDSV0001-ESITO-OK
              PERFORM S1350-CALL-LCCS0005
                 THRU S1350-CALL-LCCS0005-EX

              SET WMOV-ST-INV
               TO TRUE
           END-IF.
      *
ALPO       IF STANDARD-CALL
               IF IDSV0001-ESITO-OK
                  PERFORM S1900-GESTIONE-PMO
                     THRU EX-S1900
               END-IF
      *
               IF WCOM-WRITE-ERR
                  IF IDSV0001-ESITO-OK
                     MOVE WCOM-TP-OGG-MBS TO WK-TP-OGG-MBS
                     PERFORM S2100B-IMPOSTA-LCCS0024
                        THRU EX-S2100B

                     PERFORM S2000-CALL-LCCS0024
                        THRU EX-S2000
                  END-IF
               END-IF
ALPO       END-IF.

       EX-S1000B.
           EXIT.

      *-----------------------------------------------------------------
      * PREPARA CHIAMATA AL LCCS0005
      *-----------------------------------------------------------------
       S1300-PREPARA-LCCS0005.

            SET LCCV0005-OPER-AUTOM      TO TRUE.
            SET LCCV0005-RICHIESTA-NO    TO TRUE.
            INITIALIZE  WRRE-AREA-RAP-RETE
                        WNOT-AREA-NOTE-OGG.

            PERFORM VALORIZZAZIONE-AREA-STATI
               THRU VALORIZZAZIONE-AREA-STATI-EX.

       S1300-PREPARA-LCCS0005-EX.
            EXIT.
      * ---------------------------------------------------------------
      * CALL MODULO LCCS0005
      * ---------------------------------------------------------------
       S1350-CALL-LCCS0005.
           MOVE 'S1350-CALL-LCCS0005'   TO WK-LABEL-ERR

ALEX       SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
ALEX       SET  IDSV8888-SONDA-S9           TO TRUE
ALEX       MOVE IDSI0011-MODALITA-ESECUTIVA
ALEX            TO IDSV8888-MODALITA-ESECUTIVA
ALEX       MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
ALEX       MOVE 'LCCS0005'                  TO IDSV8888-NOME-PGM
ALEX       MOVE 'LCCS0005       '           TO IDSV8888-DESC-PGM
ALEX       SET  IDSV8888-INIZIO             TO TRUE
ALEX       PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LCCS0005'                     TO IDSV8888-NOME-PGM
           MOVE 'Servizio di EOC'              TO IDSV8888-DESC-PGM
           SET  IDSV8888-INIZIO                TO TRUE
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           CALL LCCS0005  USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                WMOV-AREA-MOVIMENTO
                                WRIC-AREA-RICHIESTA
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TRANCHE
                                AREA-IO-LCCS0005
                                WRRE-AREA-RAP-RETE
                                WNOT-AREA-NOTE-OGG
           ON EXCEPTION
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'PRE EOC COMUNE'
                   TO CALL-DESC
                 MOVE WK-LABEL-ERR
                  TO IEAI9901-LABEL-ERR
                 PERFORM S0290-ERRORE-DI-SISTEMA
                    THRU EX-S0290
           END-CALL.

ALEX       SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
ALEX       SET  IDSV8888-SONDA-S9           TO TRUE
ALEX       MOVE IDSI0011-MODALITA-ESECUTIVA
ALEX            TO IDSV8888-MODALITA-ESECUTIVA
ALEX       MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
ALEX       MOVE 'LCCS0005'                  TO IDSV8888-NOME-PGM
ALEX       MOVE 'LCCS0005       '           TO IDSV8888-DESC-PGM
ALEX       SET  IDSV8888-FINE               TO TRUE
ALEX       PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LCCS0005'                     TO IDSV8888-NOME-PGM
           MOVE 'Servizio di EOC'              TO IDSV8888-DESC-PGM
           SET  IDSV8888-FINE                  TO TRUE
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       S1350-CALL-LCCS0005-EX.
            EXIT.
      * ---------------------------------------------------------------
      * VALORIZZAZIONE AREA STATI X DEFINIRE IL MOVIMENTO E LO STATO
      * DI WORKFLOW
      * ---------------------------------------------------------------
       VALORIZZAZIONE-AREA-STATI.

                MOVE 'CO'   TO  WCOM-STATO-MOVIMENTO.
                MOVE 1      TO  WCOM-MOV-FLAG-ST-FINALE.
                MOVE ZEROES TO  WCOM-NUM-ELE-MOT-DEROGA.

       VALORIZZAZIONE-AREA-STATI-EX.
           EXIT.
      *-----------------------------------------------------------------
      * SCRIVE L''OCCORRENZA FUTURA DI PMO PER ESSERE GESTITA DAL
      * SUCCESSIVO PROCESSO BATCH DI OA
      *-----------------------------------------------------------------
       S1900-GESTIONE-PMO.

              PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
                        UNTIL IX-TAB-PMO > WPMO-ELE-PARAM-MOV-MAX
                           OR IDSV0001-ESITO-KO

                 PERFORM AGGIORNA-PARAM-MOVI
                    THRU AGGIORNA-PARAM-MOVI-EX

              END-PERFORM.

       EX-S1900.
            EXIT.
      *----------------------------------------------------------------*
      *   SCRITTURA MOVIMENTO BATCH SOSPESI
      *----------------------------------------------------------------*
       S2000-CALL-LCCS0024.

ALEX       SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
ALEX       SET  IDSV8888-SONDA-S9           TO TRUE
ALEX       MOVE IDSI0011-MODALITA-ESECUTIVA
ALEX            TO IDSV8888-MODALITA-ESECUTIVA
ALEX       MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
ALEX       MOVE 'LCCS0024'                  TO IDSV8888-NOME-PGM
ALEX       MOVE 'LCCS0024       '           TO IDSV8888-DESC-PGM
ALEX       SET  IDSV8888-INIZIO             TO TRUE
ALEX       PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LCCS0024'                     TO IDSV8888-NOME-PGM
           MOVE 'Scritt. movim. sospesi'       TO IDSV8888-DESC-PGM
           SET  IDSV8888-INIZIO                TO TRUE
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           CALL LCCS0024 USING AREA-IDSV0001
                              S0024-AREA-COMUNE
                              AREA-IO-LCCS0024
               ON EXCEPTION
                  MOVE WK-PGM
                  TO IEAI9901-COD-SERVIZIO-BE
               MOVE 'GESTIONE OGGETTO BLOCCO'  TO CALL-DESC
               MOVE 'S2000-CALL-LCCS0024'      TO IEAI9901-LABEL-ERR
               PERFORM S0290-ERRORE-DI-SISTEMA
                  THRU EX-S0290
           END-CALL.

ALEX       SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
ALEX       SET  IDSV8888-SONDA-S9           TO TRUE
ALEX       MOVE IDSI0011-MODALITA-ESECUTIVA
ALEX            TO IDSV8888-MODALITA-ESECUTIVA
ALEX       MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
ALEX       MOVE 'LCCS0024'                  TO IDSV8888-NOME-PGM
ALEX       MOVE 'LCCS0024       '           TO IDSV8888-DESC-PGM
ALEX       SET  IDSV8888-FINE               TO TRUE
ALEX       PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LCCS0024'                     TO IDSV8888-NOME-PGM
           MOVE 'Scritt. movim. sospesi'       TO IDSV8888-DESC-PGM
           SET  IDSV8888-FINE                  TO TRUE
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF IDSV0001-ESITO-OK

              PERFORM S2200-GESTIONE-OUT
                 THRU EX-S2200
           ELSE

              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S2000-CALL-LCCS0024'
               TO IEAI9901-LABEL-ERR
              MOVE '001114'
                TO IEAI9901-COD-ERRORE
              STRING 'ERRORE CHIAMATA LCCS0024'
                 DELIMITED BY SIZE
                 INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300

           END-IF.

       EX-S2000.
           EXIT.
      *----------------------------------------------------------------*
      * IMPOSTA AREA COMUNE
      *----------------------------------------------------------------*
       S2100B-IMPOSTA-LCCS0024.
      * ANNULLI
      * QUANDO PRONTI PER GLI ANNULLI VALORIZZARE LA PRIMA OCCORRENZA
              INITIALIZE              AREA-IO-LCCS0024.
      *
              MOVE WCOM-AREA-STATI    TO S0024-AREA-STATI.

      *--> DATI OGGETTO BLOCCO

              MOVE WCOM-ID-POLIZZA TO LCCC0024-ID-OGG-PRIM.
      *
              SET POLIZZA             TO TRUE.
              MOVE WS-TP-OGG          TO LCCC0024-TP-OGG-PRIM.
      *
              IF WCOM-POLIZZA-L11
                 MOVE WCOM-ID-POLIZZA TO LCCC0024-ID-OGGETTO
              ELSE
                 MOVE WCOM-ID-ADES    TO LCCC0024-ID-OGGETTO
              END-IF.
      *
              MOVE WCOM-TP-OGG-BLOCCO TO LCCC0024-TP-OGGETTO
      *
              MOVE 'DISPO'            TO LCCC0024-COD-BLOCCO.
      *
           IF WMOV-ID-RICH IS NUMERIC AND
              WMOV-ID-RICH NOT = ZEROES
              MOVE WMOV-ID-RICH       TO LCCC0024-ID-RICH
           END-IF

      * POPOLAMENTO DATI MOVIMENTO BATCH SOSPESI

              MOVE 1                  TO LCCC0024-ELE-MOV-SOS-MAX.

              IF WCOM-POLIZZA-MBS
                 MOVE WCOM-ID-POLIZZA TO LCCC0024-ID-OGG-SOSP(1)
              ELSE
                 MOVE WCOM-ID-ADES    TO LCCC0024-ID-OGG-SOSP(1)
              END-IF.

              MOVE WK-TP-OGG-MBS      TO LCCC0024-TP-OGG-SOSP(1).

              MOVE WCOM-TP-FRM-ASSVA  TO LCCC0024-TP-FRM-ASSVA(1).

              IF WCOM-ID-BATCH-NULL = HIGH-VALUE
                 MOVE WCOM-ID-BATCH-NULL TO LCCC0024-ID-BATCH-NULL(1)
              ELSE
                 MOVE WCOM-ID-BATCH TO LCCC0024-ID-BATCH(1)
              END-IF.
      *
              MOVE IDSV0001-TIPO-MOVIMENTO  TO LCCC0024-TP-MOVI-SOSP(1).
      *
              IF WCOM-ID-JOB-NULL = HIGH-VALUE
                 MOVE WCOM-ID-JOB-NULL TO LCCC0024-ID-JOB-NULL(1)
              ELSE
                 MOVE WCOM-ID-JOB TO LCCC0024-ID-JOB(1)
              END-IF.

              MOVE WCOM-STEP-ELAB TO LCCC0024-STEP-ELAB(1).

              MOVE WCOM-D-INPUT-MOVI-SOSP
                TO LCCC0024-D-INPUT-MOVI-SOSP(1).


              MOVE WMOV-ID-PTF
                TO LCCC0024-ID-MOVI-SOSP(1).

       EX-S2100B.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VALORIZZA OUTPUT LCCS0024
      *----------------------------------------------------------------*
       S2200-GESTIONE-OUT.

           IF LCCC0024-BLC-CENSITO-PTF
              MOVE LCCC0024-ID-OGG-BLOCCO TO WCOM-ID-BLOCCO-CRZ
              SET  WCOM-BLC-CENSITO-PTF   TO TRUE
           END-IF.

       EX-S2200.
           EXIT.

      *----------------------------------------------------------------*
      *    CALL SERVIZIO ESTRAZIONE OGGETTO PTF LCCS0234
      *----------------------------------------------------------------*
       CALL-LCCS0234.

ALEX       SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
ALEX       SET  IDSV8888-SONDA-S9           TO TRUE
ALEX       MOVE IDSI0011-MODALITA-ESECUTIVA
ALEX            TO IDSV8888-MODALITA-ESECUTIVA
ALEX       MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
ALEX       MOVE 'LCCS0234'                  TO IDSV8888-NOME-PGM
ALEX       MOVE 'LCCS0234       '           TO IDSV8888-DESC-PGM
ALEX       SET  IDSV8888-INIZIO             TO TRUE
ALEX       PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LCCS0234'                     TO IDSV8888-NOME-PGM
           MOVE 'Serv estraz ogg. PTF'         TO IDSV8888-DESC-PGM
           SET  IDSV8888-INIZIO                TO TRUE
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           CALL LCCS0234  USING AREA-IDSV0001
                                WPOL-AREA-POLIZZA
                                WADE-AREA-ADESIONE
                                WGRZ-AREA-GARANZIA
                                WTGA-AREA-TRANCHE
                                S234-DATI-INPUT
                                S234-DATI-OUTPUT
           ON EXCEPTION
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'IL REPERIMENTO ID PORTAFOGLIO'
                    TO CALL-DESC
                  MOVE 'S1100-CALL-LCCS0234'
                    TO IEAI9901-LABEL-ERR
                  PERFORM S0290-ERRORE-DI-SISTEMA
                     THRU EX-S0290
           END-CALL.

ALEX       SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
ALEX       SET  IDSV8888-SONDA-S9           TO TRUE
ALEX       MOVE IDSI0011-MODALITA-ESECUTIVA
ALEX            TO IDSV8888-MODALITA-ESECUTIVA
ALEX       MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
ALEX       MOVE 'LCCS0234'                  TO IDSV8888-NOME-PGM
ALEX       MOVE 'LCCS0234       '           TO IDSV8888-DESC-PGM
ALEX       SET  IDSV8888-FINE               TO TRUE
ALEX       PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

           SET  IDSV8888-BUSINESS-DBG          TO TRUE
           MOVE 'LCCS0234'                     TO IDSV8888-NOME-PGM
           MOVE 'Serv estraz ogg. PTF'         TO IDSV8888-DESC-PGM
           SET  IDSV8888-FINE                  TO TRUE
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

       CALL-LCCS0234-EX.
           EXIT.
      * ----------------------------------------------------------------
      *    OPERAZIONI FINALI
      * ----------------------------------------------------------------
       S9000B-OPERAZIONI-FINALI.
      *
           MOVE 'S9000B-OPERAZIONI-FINALI'
             TO WK-LABEL-ERR.

           MOVE SPACES TO WCOM-TP-OGG-BLOCCO
           MOVE SPACES TO WCOM-TP-OGG-MBS.
      *
       EX-S9000B.
           EXIT.
      *
      *  --------------------------------------------------------------*
      *     CONTIENE STATEMENTS PER LA FASE DI EOC                     *
      *  --------------------------------------------------------------*
           COPY LCCVPMO5        REPLACING ==(SF)== BY ==WPMO==.
           COPY LCCVPMO6        REPLACING ==(SF)== BY ==WPMO==.

           COPY LCCP0001.
           COPY LCCP0002.
      * ---------------------------------------------------------------*
      *  ROUTINES GESTIONE ERRORI                                      *
      * ---------------------------------------------------------------*
           COPY IERP9902.
           COPY IERP9901.
      * ---------------------------------------------------------------*
      *    ROUTINES DISPATCHER                                         *
      * ---------------------------------------------------------------*
           COPY IDSP0012.
      *----------------------------------------------------------------*
      *    ROUTINES DI GESTIONE DISPLAY
      *----------------------------------------------------------------*
           COPY IDSP8888.


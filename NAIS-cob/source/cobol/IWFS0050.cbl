************************************************************************
********                                                        ********
********                                                        ********
********              CONVERSIONE STRINGA NUMERICA              ********
************************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IWFS0050 IS INITIAL.
       AUTHOR.
       DATE-WRITTEN.  SETTEMBRE 2007.
       DATE-COMPILED.
      *REMARKS.
      *같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같�
      *    PROGRAAMMA .... IWFS0050
      *    FUNZIONE ......
      *같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같같�
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.

      *************************************************
      * W O R K I N G   S T O R A G E   S E C T I O N *
      *************************************************
       WORKING-STORAGE SECTION.

       77  WK-PGM                         PIC X(008)  VALUE 'IWFS0050'.
       77  WK-NOME-TABELLA                PIC X(015).
       77  WK-LABEL                       PIC X(030).
       77  WK-CONT-NEG                    PIC S9(04) COMP.

      ***************************************************************
      * INDICI
      ***************************************************************
       77 IND-STRINGA                     PIC 9(02).

       01 IND-INTERI                      PIC 9(02).
          88 INTERI                       VALUE 13.
       77 IND-DECIMALI                    PIC 9(02).
          88 DECIMALI                     VALUE 14.

      ***************************************************************
      * LIMITI
      ***************************************************************
       77 WK-LIMITE-STRINGA               PIC 9(02) VALUE 20.

       77 WK-START-RICERCA                PIC 9(02).

      ***************************************************************
      * COMODO
      ***************************************************************
       77 VIRGOLA                         PIC X(01) VALUE ','.
       77 POSIZIONE-VIRGOLA               PIC 9(02) VALUE 0.

       01 WS-CAMPO-OUTPUT                 PIC S9(13)V9(05).

       01 ARRAY-STRINGA-OUTPUT REDEFINES WS-CAMPO-OUTPUT.
          05 ELE-STRINGA-OUTPUT  PIC X OCCURS 18.


       01 WS-STRINGA-OUTPUT              PIC X(18) VALUE SPACES.
       01 WS-ARRAY-STRINGA-INPUT         PIC X(20).

      *----------------------------------------------------------------*
       LINKAGE SECTION.

       01  AREA-CALL.
           COPY IWFI0051.
           COPY IWFO0051.

      ******************************************************************
      * P R O C E D U R E   D I V I S I O N                            *
      ******************************************************************
       PROCEDURE DIVISION USING AREA-CALL.

           PERFORM A000-OPERAZ-INIZ                  THRU A000-EX

           IF IWFO0051-ESITO-OK
              PERFORM B000-ELABORA                   THRU B000-EX
           END-IF.

           GOBACK.
      *----------------------------------------------------------------*
      * OPERAZIONI INIZIALI                                            *
      *----------------------------------------------------------------*
       A000-OPERAZ-INIZ.
      *
           PERFORM A050-INITIALIZE        THRU A050-EX.
           PERFORM A060-CTRL-INPUT        THRU A060-EX.
      *
       A000-EX.
           EXIT.
      *----------------------------------------------------------------*
      * INIZIALIZZA AREE DI WORKING                                    *
      *----------------------------------------------------------------*
       A050-INITIALIZE.
      *
           SET IWFO0051-ESITO-OK       TO TRUE
           SET INTERI                  TO TRUE
           SET DECIMALI                TO TRUE

           MOVE 0                      TO IWFO0051-CAMPO-OUTPUT-DEFI
                                          WS-CAMPO-OUTPUT
                                          POSIZIONE-VIRGOLA
                                          WK-CONT-NEG.

           MOVE SPACES                 TO IWFO0051-LOG-ERRORE.
      *
       A050-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CONTROLLI INPUT
      *----------------------------------------------------------------*
       A060-CTRL-INPUT.
      *
           MOVE 'A060-CTRL-INPUT'    TO WK-LABEL.

           IF IWFI0051-ARRAY-STRINGA-INPUT =
                  SPACES OR HIGH-VALUES OR LOW-VALUE

              SET IWFO0051-ESITO-KO    TO TRUE
              MOVE WK-PGM              TO IWFO0051-COD-SERVIZIO-BE
              MOVE WK-LABEL            TO IWFO0051-LABEL-ERR
              MOVE 'STRINGA DA CONVERTIRE NON VALIDA'
                                       TO IWFO0051-DESC-ERRORE-ESTESA
           END-IF.

           MOVE IWFI0051-ARRAY-STRINGA-INPUT
                                  TO WS-ARRAY-STRINGA-INPUT
           INSPECT WS-ARRAY-STRINGA-INPUT
                                  REPLACING ALL SPACE      BY '0'.
           INSPECT WS-ARRAY-STRINGA-INPUT
                                  REPLACING ALL LOW-VALUE  BY '0'.
           INSPECT WS-ARRAY-STRINGA-INPUT
                                  REPLACING ALL HIGH-VALUE BY '0'.
           INSPECT WS-ARRAY-STRINGA-INPUT
                                  REPLACING ALL ','        BY '0'.

           IF WS-ARRAY-STRINGA-INPUT NOT NUMERIC

              SET IWFO0051-ESITO-KO    TO TRUE
              MOVE WK-PGM              TO IWFO0051-COD-SERVIZIO-BE
              MOVE WK-LABEL            TO IWFO0051-LABEL-ERR

              STRING 'STRINGA DA CONVERTIRE NON VALIDA '
                     ' : '
                     IWFI0051-ARRAY-STRINGA-INPUT
                     DELIMITED BY SIZE INTO
                     IWFO0051-DESC-ERRORE-ESTESA
              END-STRING
           END-IF.
      *
       A060-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ELABORAZIONE                                                   *
      *----------------------------------------------------------------*
       B000-ELABORA.
      *
           PERFORM B050-RICERCA-VIRGOLA THRU B050-EX.

           PERFORM B100-TRATTA-STRINGA  THRU B100-EX.

           INSPECT WS-CAMPO-OUTPUT REPLACING ALL ' ' BY '0'.

           PERFORM B101-CONTROLLA-SEGNO THRU B101-EX.
      *
       B000-EX.
           EXIT.
      *----------------------------------------------------------------*
      * RICERCA VIRGOLA
      *----------------------------------------------------------------*
       B050-RICERCA-VIRGOLA.
      *
           PERFORM VARYING IND-STRINGA   FROM 1 BY 1
                   UNTIL   IND-STRINGA       > WK-LIMITE-STRINGA
                   OR      POSIZIONE-VIRGOLA > 0

                   IF IWFI0051-CAMPO-INPUT(IND-STRINGA) = VIRGOLA
                      MOVE IND-STRINGA    TO POSIZIONE-VIRGOLA
                   END-IF
           END-PERFORM.
      *
       B050-EX.
           EXIT.
      *----------------------------------------------------------------*
      * TRATTA STRINGA
      *----------------------------------------------------------------*
       B100-TRATTA-STRINGA.
      *
           IF POSIZIONE-VIRGOLA = 0
              MOVE WK-LIMITE-STRINGA       TO WK-START-RICERCA
           ELSE
              MOVE POSIZIONE-VIRGOLA       TO WK-START-RICERCA
              PERFORM B300-TRATTA-DECIMALI THRU B300-EX
           END-IF.

           PERFORM B200-TRATTA-INTERI      THRU B200-EX.
      *
       B100-EX.
           EXIT.
      *----------------------------------------------------------------*
      * TRATTA STRINGA
      *----------------------------------------------------------------*
       B101-CONTROLLA-SEGNO.
      *
           INSPECT IWFI0051-ARRAY-STRINGA-INPUT
                    TALLYING WK-CONT-NEG FOR ALL '-'.

           IF WK-CONT-NEG > 0
              COMPUTE IWFO0051-CAMPO-OUTPUT-DEFI = WS-CAMPO-OUTPUT * -1
           ELSE
              MOVE WS-CAMPO-OUTPUT      TO IWFO0051-CAMPO-OUTPUT-DEFI
           END-IF.
      *
       B101-EX.
           EXIT.
      *----------------------------------------------------------------*
      * TRATTA INTERI
      *----------------------------------------------------------------*
       B200-TRATTA-INTERI.
      *
           SUBTRACT 1                 FROM WK-START-RICERCA

           PERFORM VARYING IND-STRINGA FROM WK-START-RICERCA BY -1
                   UNTIL   IND-STRINGA = 0

                   IF IWFI0051-CAMPO-INPUT(IND-STRINGA)
                                             IS NUMERIC

                      MOVE IWFI0051-CAMPO-INPUT(IND-STRINGA)
                           TO ELE-STRINGA-OUTPUT(IND-INTERI)
                      SUBTRACT 1             FROM IND-INTERI
                   END-IF

           END-PERFORM.
      *
       B200-EX.
           EXIT.
      *----------------------------------------------------------------*
      * TRATTA DECIMALI
      *----------------------------------------------------------------*
       B300-TRATTA-DECIMALI.
      *
           ADD  1                      TO   WK-START-RICERCA

           PERFORM VARYING IND-STRINGA FROM WK-START-RICERCA BY 1
                   UNTIL   IND-STRINGA > WK-LIMITE-STRINGA

                   IF IWFI0051-CAMPO-INPUT(IND-STRINGA)
                                             IS NUMERIC

                      MOVE IWFI0051-CAMPO-INPUT(IND-STRINGA)
                           TO ELE-STRINGA-OUTPUT(IND-DECIMALI)
                      ADD 1                  TO IND-DECIMALI

                   END-IF

            END-PERFORM.
      *
       B300-EX.
           EXIT.

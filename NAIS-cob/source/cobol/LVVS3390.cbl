      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS3390.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2018.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      * DESCRIZIONE.... VALORIZZAZIONE VARIABILE PRETOTEMIPOL
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS3390'.
       01  WK-CALL-PGM                      PIC X(008) VALUE  SPACES.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*

      *---------------------------------------------------------------*
      *  COPY PER CHIAMATA IDBS
      *---------------------------------------------------------------*
           COPY IDBVDTC1.
           COPY IDBVTIT1.
      *----------------------------------------------------------------*
      *--> AREA TRANCHE DI GARANZIA
      *----------------------------------------------------------------*
       01 AREA-IO-TGA.
          03 DTGA-AREA-TGA.
             04 DTGA-ELE-TGA-MAX       PIC S9(04) COMP.
                COPY LCCVTGAC REPLACING   ==(SF)==  BY ==DTGA==.
             COPY LCCVTGA1             REPLACING ==(SF)== BY ==DTGA==.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0213.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003
                                INPUT-LVVS0213.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.
      *
           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000
      *
           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.
      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT
                                             AREA-IO-TGA.

           MOVE ZEROES                       TO IVVC0213-VAL-IMP-O
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE

           MOVE IVVC0213-AREA-VARIABILE      TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1200-CONTROLLO-DATI
                  THRU S1200-CONTROLLO-DATI-EX
           END-IF.

      *--> ESTRAE DETT-TIT-CONT PER TRANCHE DI GAR
           IF  IDSV0003-SUCCESSFUL-RC
           AND IDSV0003-SUCCESSFUL-SQL
               PERFORM S1400-CHIAMA-LDBSF220
                  THRU S1400-CHIAMA-LDBSF220-EX
           END-IF.
      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.
      *
           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-TRCH-GAR
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DTGA-AREA-TGA
           END-IF.
      *
       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   CONTROLLO DATI DCLGEN
      *----------------------------------------------------------------*
       S1200-CONTROLLO-DATI.
      *
           IF IVVC0213-ID-LIVELLO IS NUMERIC
              IF IVVC0213-ID-LIVELLO = 0
                SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                MOVE WK-PGM
                  TO IDSV0003-COD-SERVIZIO-BE
                MOVE 'ID-LIVELLO NON VALORIZZATO'
                  TO IDSV0003-DESCRIZ-ERR-DB2
              END-IF
           ELSE
              SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
              MOVE WK-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'ID-LIVELLO NON VALORIZZATO'
                TO IDSV0003-DESCRIZ-ERR-DB2
           END-IF.
      *
       S1200-CONTROLLO-DATI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CHIAMATA LDBSF220 - ESTRAE DETT_TIT_CONT
      *----------------------------------------------------------------*
       S1400-CHIAMA-LDBSF220.

           INITIALIZE                       DETT-TIT-CONT.
      *
           MOVE 'TG'                        TO DTC-TP-OGG.
           MOVE IVVC0213-ID-LIVELLO         TO DTC-ID-OGG.
      *
           SET IDSV0003-TRATT-X-COMPETENZA
             TO TRUE.
      *
           SET IDSV0003-SELECT              TO TRUE.
           SET IDSV0003-WHERE-CONDITION     TO TRUE.
      *
           SET IDSV0003-SUCCESSFUL-RC
             TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL
             TO TRUE.

           MOVE 'LDBSF220'                       TO WK-CALL-PGM.
      *
           CALL WK-CALL-PGM USING IDSV0003 DETT-TIT-CONT
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'LVVS3390 - ERRORE CHIAMATA LDBSF220'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER         TO TRUE
           END-CALL.
      *
           IF IDSV0003-SUCCESSFUL-RC
              EVALUATE TRUE

              WHEN IDSV0003-SUCCESSFUL-SQL
      *--> PREPARA AREA TIT-CONT
                   PERFORM S1410-PREP-TIT-CONT
                      THRU S1410-PREP-TIT-CONT-EX
      *--> SE ESTRAE UN'OCCORRENZA IN DETT_TIT_CONT
      *    ACCEDO ALL'IDBS STANDARD TIT-CONT
                   PERFORM S1450-CALL-TIT-CONT
                      THRU S1450-CALL-TIT-CONT-EX
              WHEN IDSV0003-NOT-FOUND

                   SET IDSV0003-FIELD-NOT-VALUED        TO TRUE

              WHEN OTHER
                   SET    IDSV0003-INVALID-OPER         TO TRUE
                   MOVE   WK-PGM
                   TO     IDSV0003-COD-SERVIZIO-BE
                   STRING 'LVVS3390 - ERRORE CHIAMATA LDBSF220 ;'
                           IDSV0003-RETURN-CODE ';'
                           IDSV0003-SQLCODE
                           DELIMITED BY SIZE
                           INTO IDSV0003-DESCRIZ-ERR-DB2
                   END-STRING
      *
              END-EVALUATE
           END-IF.

       S1400-CHIAMA-LDBSF220-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   SETTAGGIO IDBS STANDARD TIT-CONT
      *----------------------------------------------------------------*
       S1410-PREP-TIT-CONT.

           INITIALIZE                       TIT-CONT
                                            WK-CALL-PGM.
      *
           SET IDSV0003-TRATT-X-COMPETENZA  TO TRUE.
           SET IDSV0003-SELECT              TO TRUE.
           SET IDSV0003-ID                  TO TRUE.
      *
           SET IDSV0003-SUCCESSFUL-RC
             TO TRUE.
           SET IDSV0003-SUCCESSFUL-SQL
             TO TRUE.
      *
           MOVE DTC-ID-TIT-CONT           TO TIT-ID-TIT-CONT.
           MOVE 'IDBSTIT0'                TO WK-CALL-PGM.
      *
       S1410-PREP-TIT-CONT-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   ROUTINE ACCESSO IDBS STANDARD TIT-CONT
      *----------------------------------------------------------------*
       S1450-CALL-TIT-CONT.

      *
           CALL WK-CALL-PGM USING IDSV0003 TIT-CONT
      *
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO IDSV0003-COD-SERVIZIO-BE
              MOVE 'LVVS3390 - ERRORE CHIAMATA IDBSTIT0'
                TO IDSV0003-DESCRIZ-ERR-DB2
               SET IDSV0003-INVALID-OPER         TO TRUE
           END-CALL.
      *
           IF IDSV0003-SUCCESSFUL-RC
              EVALUATE TRUE

              WHEN IDSV0003-SUCCESSFUL-SQL

                   IF TIT-TP-STAT-TIT = 'IN'
                   OR TIT-TP-STAT-TIT = 'EM'
                      IF TIT-TOT-PRE-TOT-NULL = HIGH-VALUES OR LOW-VALUE
                                                            OR SPACES
                         SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                      ELSE
                        MOVE TIT-TOT-PRE-TOT TO IVVC0213-VAL-IMP-O
                      END-IF
                   ELSE
                      MOVE ZEROES            TO IVVC0213-VAL-IMP-O
                   END-IF

              WHEN IDSV0003-NOT-FOUND
                   SET IDSV0003-FIELD-NOT-VALUED TO TRUE

              WHEN OTHER
                   SET    IDSV0003-INVALID-OPER         TO TRUE
                   MOVE   WK-PGM
                   TO     IDSV0003-COD-SERVIZIO-BE
                   STRING 'LVVS3390 - ERRORE CHIAMATA IDBSTIT0 ;'
                           IDSV0003-RETURN-CODE ';'
                           IDSV0003-SQLCODE
                           DELIMITED BY SIZE
                           INTO IDSV0003-DESCRIZ-ERR-DB2
                   END-STRING
      *
              END-EVALUATE
           END-IF.
      *
       S1450-CALL-TIT-CONT-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.


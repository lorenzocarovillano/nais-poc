      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS3510.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2007.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS3510
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... VARIABILE PRMK1TRA
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS3510'.
       01  WK-PGM-CALL                      PIC X(008).
       01  WK-CALL-PGM                      PIC X(008).
       77  LDBSH580                         PIC X(008) VALUE 'LDBSH580'.
MIGCOL 01  WK-APP-DATA-EFFETTO              PIC S9(008) COMP-3.
      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *--> AREA TRCH-DI-GAR
      *----------------------------------------------------------------*
       01 AREA-IO-TGA.
          03 DTGA-AREA-TRCH-GAR.
             04 DTGA-ELE-MAX-TGA          PIC S9(04) COMP.
             04 DTGA-TAB-TGA.
             COPY LCCVTGA1              REPLACING ==(SF)== BY ==DTGA==.

      * COPY TABELLE
           COPY IDBVP611.
      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.
      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                      PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS3510.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.
      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS3510.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.


           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000


           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

           PERFORM LETTURA-D-CRIST
              THRU LETTURA-D-CRIST-EX.

      *
       EX-S1000.
           EXIT.
      *----------------------------------------------------------------*
      *--> LETTURA TABELLA DATI CRISTALLIZZATI PER ID TRANCHE
      *--> PER RECUPERARE IL CAMPO
      *----------------------------------------------------------------*
       LETTURA-D-CRIST.
           SET IDSV0003-SELECT                TO TRUE
           SET IDSV0003-SUCCESSFUL-RC         TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL        TO TRUE

MIGCOL     MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WK-APP-DATA-EFFETTO
MIGCOL     MOVE 99991230  TO IDSV0003-DATA-INIZIO-EFFETTO

           INITIALIZE D-CRIST.

           MOVE IVVC0213-ID-TRANCHE            TO P61-ID-TRCH-DI-GAR
           SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE

      *--> LIVELLO OPERAZIONE
           SET IDSV0003-WHERE-CONDITION       TO TRUE
      *--> INIZIALIZZA CODICE DI RITORNO
           SET IDSV0003-SUCCESSFUL-RC         TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL        TO TRUE

           MOVE 'LDBSH580'            TO WK-PGM-CALL
           CALL WK-PGM-CALL   USING  IDSV0003 D-CRIST

           ON EXCEPTION
              MOVE 'LDBSH580'
                TO IDSV0003-COD-SERVIZIO-BE
             MOVE 'CALL-LDBSH580 ERRORE CHIAMATA '
                TO IDSV0003-DESCRIZ-ERR-DB2
                SET IDSV0003-INVALID-OPER  TO TRUE
           END-CALL

              IF IDSV0003-SUCCESSFUL-RC
                EVALUATE TRUE

                    WHEN IDSV0003-NOT-FOUND
                      MOVE 0
                        TO  IVVC0213-VAL-IMP-O


                    WHEN IDSV0003-SUCCESSFUL-SQL
                      IF P61-PRE-LRD-END2000-NULL NOT = HIGH-VALUE
                                                        AND LOW-VALUE
                                                        AND SPACES
                          MOVE P61-PRE-LRD-END2000
                            TO IVVC0213-VAL-IMP-O
                      END-IF

                    WHEN OTHER
                        SET IDSV0003-INVALID-OPER  TO TRUE
                        MOVE WK-PGM-CALL
                          TO IDSV0003-COD-SERVIZIO-BE
                        STRING 'ERRORE RECUP D CRIST ;'
                               IDSV0003-RETURN-CODE ';'
                               IDSV0003-SQLCODE
                               DELIMITED BY SIZE INTO
                               IDSV0003-DESCRIZ-ERR-DB2
                        END-STRING

                END-EVALUATE
              ELSE
                MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
                STRING 'CHIAMATA LDBSH580 ;'
                       IDSV0003-RETURN-CODE ';'
                       IDSV0003-SQLCODE
                   DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                END-STRING
                IF IDSV0003-NOT-FOUND
                OR IDSV0003-SQLCODE = -305
                   SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                ELSE
                   SET IDSV0003-INVALID-OPER            TO TRUE
                END-IF
              END-IF.

MIGCOL     MOVE WK-APP-DATA-EFFETTO
MIGCOL       TO IDSV0003-DATA-INIZIO-EFFETTO.

       LETTURA-D-CRIST-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-TRCH-GAR
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DTGA-AREA-TRCH-GAR
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.
      *
           GOBACK.
      *
       EX-S9000.
           EXIT.

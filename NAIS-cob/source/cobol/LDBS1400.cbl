       IDENTIFICATION DIVISION.
       PROGRAM-ID.    LDBS1400 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  NOVEMBRE 2007.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDMVV0 END-EXEC.
           EXEC SQL INCLUDE IDBVMVV2 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVMVV1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 MATR-VAL-VAR.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC

              PERFORM A200-ELABORA                THRU A200-EX

           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'LDBS1400'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'MATR_VAL_VAR' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN -811
                                CONTINUE
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA.

           PERFORM A210-SELECT             THRU A210-EX.

       A200-EX.
           EXIT.

      *----
      *----  gestione SELECT
      *----
       A210-SELECT.

           EXEC SQL
             SELECT
                ID_MATR_VAL_VAR
                ,IDP_MATR_VAL_VAR
                ,COD_COMPAGNIA_ANIA
                ,TIPO_MOVIMENTO
                ,COD_DATO_EXT
                ,OBBLIGATORIETA
                ,VALORE_DEFAULT
                ,COD_STR_DATO_PTF
                ,COD_DATO_PTF
                ,COD_PARAMETRO
                ,OPERAZIONE
                ,LIVELLO_OPERAZIONE
                ,TIPO_OGGETTO
                ,WHERE_CONDITION
                ,SERVIZIO_LETTURA
                ,MODULO_CALCOLO
                ,STEP_VALORIZZATORE
                ,STEP_CONVERSAZIONE
                ,OPER_LOG_STATO_BUS
                ,ARRAY_STATO_BUS
                ,OPER_LOG_CAUSALE
                ,ARRAY_CAUSALE
             INTO
                :MVV-ID-MATR-VAL-VAR
               ,:MVV-IDP-MATR-VAL-VAR
                :IND-MVV-IDP-MATR-VAL-VAR
               ,:MVV-COD-COMPAGNIA-ANIA
               ,:MVV-TIPO-MOVIMENTO
                :IND-MVV-TIPO-MOVIMENTO
               ,:MVV-COD-DATO-EXT
                :IND-MVV-COD-DATO-EXT
               ,:MVV-OBBLIGATORIETA
                :IND-MVV-OBBLIGATORIETA
               ,:MVV-VALORE-DEFAULT
                :IND-MVV-VALORE-DEFAULT
               ,:MVV-COD-STR-DATO-PTF
                :IND-MVV-COD-STR-DATO-PTF
               ,:MVV-COD-DATO-PTF
                :IND-MVV-COD-DATO-PTF
               ,:MVV-COD-PARAMETRO
                :IND-MVV-COD-PARAMETRO
               ,:MVV-OPERAZIONE
                :IND-MVV-OPERAZIONE
               ,:MVV-LIVELLO-OPERAZIONE
                :IND-MVV-LIVELLO-OPERAZIONE
               ,:MVV-TIPO-OGGETTO
                :IND-MVV-TIPO-OGGETTO
               ,:MVV-WHERE-CONDITION-VCHAR
                :IND-MVV-WHERE-CONDITION
               ,:MVV-SERVIZIO-LETTURA
                :IND-MVV-SERVIZIO-LETTURA
               ,:MVV-MODULO-CALCOLO
                :IND-MVV-MODULO-CALCOLO
               ,:MVV-STEP-VALORIZZATORE
                :IND-MVV-STEP-VALORIZZATORE
               ,:MVV-STEP-CONVERSAZIONE
                :IND-MVV-STEP-CONVERSAZIONE
               ,:MVV-OPER-LOG-STATO-BUS
                :IND-MVV-OPER-LOG-STATO-BUS
               ,:MVV-ARRAY-STATO-BUS
                :IND-MVV-ARRAY-STATO-BUS
               ,:MVV-OPER-LOG-CAUSALE
                :IND-MVV-OPER-LOG-CAUSALE
               ,:MVV-ARRAY-CAUSALE
                :IND-MVV-ARRAY-CAUSALE
             FROM  MATR_VAL_VAR
             WHERE COD_COMPAGNIA_ANIA = :MVV-COD-COMPAGNIA-ANIA
             AND   TIPO_MOVIMENTO     = :MVV-TIPO-MOVIMENTO
             AND   STEP_VALORIZZATORE = :MVV-STEP-VALORIZZATORE
             AND   STEP_CONVERSAZIONE = :MVV-STEP-CONVERSAZIONE
           END-EXEC.


           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-MVV-IDP-MATR-VAL-VAR = -1
              MOVE HIGH-VALUES TO MVV-IDP-MATR-VAL-VAR-NULL
           END-IF
           IF IND-MVV-TIPO-MOVIMENTO = -1
              MOVE HIGH-VALUES TO MVV-TIPO-MOVIMENTO-NULL
           END-IF
           IF IND-MVV-COD-DATO-EXT = -1
              MOVE HIGH-VALUES TO MVV-COD-DATO-EXT
           END-IF
           IF IND-MVV-OBBLIGATORIETA = -1
              MOVE HIGH-VALUES TO MVV-OBBLIGATORIETA-NULL
           END-IF
           IF IND-MVV-VALORE-DEFAULT = -1
              MOVE HIGH-VALUES TO MVV-VALORE-DEFAULT
           END-IF
           IF IND-MVV-COD-STR-DATO-PTF = -1
              MOVE HIGH-VALUES TO MVV-COD-STR-DATO-PTF
           END-IF
           IF IND-MVV-COD-DATO-PTF = -1
              MOVE HIGH-VALUES TO MVV-COD-DATO-PTF
           END-IF
           IF IND-MVV-COD-PARAMETRO = -1
              MOVE HIGH-VALUES TO MVV-COD-PARAMETRO
           END-IF
           IF IND-MVV-OPERAZIONE = -1
              MOVE HIGH-VALUES TO MVV-OPERAZIONE
           END-IF
           IF IND-MVV-LIVELLO-OPERAZIONE = -1
              MOVE HIGH-VALUES TO MVV-LIVELLO-OPERAZIONE-NULL
           END-IF
           IF IND-MVV-TIPO-OGGETTO = -1
              MOVE HIGH-VALUES TO MVV-TIPO-OGGETTO-NULL
           END-IF
           IF IND-MVV-WHERE-CONDITION = -1
              MOVE HIGH-VALUES TO MVV-WHERE-CONDITION
           END-IF
           IF IND-MVV-SERVIZIO-LETTURA = -1
              MOVE HIGH-VALUES TO MVV-SERVIZIO-LETTURA-NULL
           END-IF
           IF IND-MVV-MODULO-CALCOLO = -1
              MOVE HIGH-VALUES TO MVV-MODULO-CALCOLO-NULL
           END-IF
           IF IND-MVV-STEP-VALORIZZATORE = -1
              MOVE HIGH-VALUES TO MVV-STEP-VALORIZZATORE-NULL
           END-IF
           IF IND-MVV-STEP-CONVERSAZIONE = -1
              MOVE HIGH-VALUES TO MVV-STEP-CONVERSAZIONE-NULL
           END-IF
           IF IND-MVV-OPER-LOG-STATO-BUS = -1
              MOVE HIGH-VALUES TO MVV-OPER-LOG-STATO-BUS-NULL
           END-IF
           IF IND-MVV-ARRAY-STATO-BUS = -1
              MOVE HIGH-VALUES TO MVV-ARRAY-STATO-BUS
           END-IF
           IF IND-MVV-OPER-LOG-CAUSALE = -1
              MOVE HIGH-VALUES TO MVV-OPER-LOG-CAUSALE-NULL
           END-IF
           IF IND-MVV-ARRAY-CAUSALE = -1
              MOVE HIGH-VALUES TO MVV-ARRAY-CAUSALE
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES.

           CONTINUE.
       Z150-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.

           CONTINUE.
       Z950-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.


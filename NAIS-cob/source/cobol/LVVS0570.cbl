      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.         LVVS0570.
       AUTHOR.             ATS.
       DATE-WRITTEN.       2010.
       DATE-COMPILED.
      ***------------------------------------------------------------***
      *  PROGRAMMA...... LVVS0570
      *  TIPOLOGIA...... SERVIZIO
      *  PROCESSO....... XXX
      *  FUNZIONE....... XXX
      *  DESCRIZIONE.... CONVERSIONE CODICE SOGGETTO DA STRINGA A
      *                  NUMERICO
      ***------------------------------------------------------------***
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

      *----------------------------------------------------------------*
      *    COSTANTI
      *----------------------------------------------------------------*
       01  WK-PGM                           PIC X(008) VALUE 'LVVS0570'.
       01  WK-CALL-PGM                      PIC X(008) VALUE SPACES.
      *--> Conversione Stringa a Numerico
       01  IWFS0050                         PIC X(008) VALUE 'IWFS0050'.

      *----------------------------------------------------------------*
      *    VARIABILI
      *----------------------------------------------------------------*

      *--> Area per modulo conversione stringa
       01  AREA-CALL-IWFS0050.
           COPY IWFI0051.
           COPY IWFO0051.

      *----------------------------------------------------------------*
      *--> AREA RAPPORTO ANAGRAFICO
      *----------------------------------------------------------------*
       01 AREA-IO-RAN.
          03 DRAN-AREA-RAPP-ANA.
             04 DRAN-ELE-RAN-MAX        PIC S9(04) COMP.
                COPY LCCVRANA REPLACING   ==(SF)==  BY ==DRAN==.
             COPY LCCVRAN1              REPLACING ==(SF)== BY ==DRAN==.

      *----------------------------------------------------------------*
      *--> AREA ALIAS DCLGEN
      *----------------------------------------------------------------*
      *--  AREA ALIAS.
       01  AREA-ALIAS.
           COPY IVVC0218         REPLACING ==(SF)== BY ==IVVC0218==.

      *----------------------------------------------------------------*
      *      COPY VARIBILI PER LA GESTIONE ERRORI                      *
      *----------------------------------------------------------------*
           COPY IDSV0002.

      *----------------------------------------------------------------*
      *    INDICI
      *----------------------------------------------------------------*
       01  IX-INDICI.
           03 IX-DCLGEN                     PIC S9(04) COMP.

      *----------------------------------------------------------------*
       LINKAGE SECTION.
      *----------------------------------------------------------------*
           COPY IDSV0003.

       01  INPUT-LVVS0570.
           COPY IVVC0213        REPLACING ==(SF)== BY ==IVVC0213==.

      *----------------------------------------------------------------*
       PROCEDURE DIVISION USING IDSV0003 INPUT-LVVS0570.
      *----------------------------------------------------------------*

           PERFORM S0000-OPERAZIONI-INIZIALI
              THRU EX-S0000.

           PERFORM S1000-ELABORAZIONE
              THRU EX-S1000.

           PERFORM S9000-OPERAZIONI-FINALI
              THRU EX-S9000.

      *----------------------------------------------------------------*
      *  OPERAZIONI INIZIALI                                           *
      *----------------------------------------------------------------*
       S0000-OPERAZIONI-INIZIALI.

           INITIALIZE                        IX-INDICI
                                             IVVC0213-TAB-OUTPUT.

           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
           SET IDSV0003-SUCCESSFUL-RC        TO TRUE.

           MOVE IVVC0213-AREA-VARIABILE
             TO IVVC0213-TAB-OUTPUT.

       EX-S0000.
           EXIT.

      *----------------------------------------------------------------*
      *    ELABORAZIONE
      *----------------------------------------------------------------*
       S1000-ELABORAZIONE.

      *--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *--> RISPETTIVE AREE DCLGEN IN WORKING
           PERFORM S1100-VALORIZZA-DCLGEN
              THRU S1100-VALORIZZA-DCLGEN-EX
           VARYING IX-DCLGEN FROM 1 BY 1
             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
                   SPACES OR LOW-VALUE OR HIGH-VALUE.

      * Conversione da stringa a numerico
           PERFORM S1200-CONVERTI-CHAR-NUM
              THRU S1200-CONVERTI-CHAR-NUM-EX.

       EX-S1000.
           EXIT.

      *----------------------------------------------------------------*
      *    ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
      *    RISPETTIVE AREE DCLGEN IN WORKING
      *----------------------------------------------------------------*
       S1100-VALORIZZA-DCLGEN.

           IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
              IVVC0218-ALIAS-RAPP-ANAG
              MOVE IVVC0213-BUFFER-DATI
                  (IVVC0213-POSIZ-INI(IX-DCLGEN) :
                   IVVC0213-LUNGHEZZA(IX-DCLGEN))
                TO DRAN-AREA-RAPP-ANA
           END-IF.

       S1100-VALORIZZA-DCLGEN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *  CONVERTI VALORE DA STRINGA IN NUMERICO
      *----------------------------------------------------------------*
       S1200-CONVERTI-CHAR-NUM.

           MOVE HIGH-VALUE            TO AREA-CALL-IWFS0050.
           MOVE DRAN-COD-SOGG(IVVC0213-IX-TABB)
                                      TO IWFI0051-ARRAY-STRINGA-INPUT.

           CALL IWFS0050              USING AREA-CALL-IWFS0050.

           IF IWFO0051-ESITO-OK
              MOVE IWFO0051-CAMPO-OUTPUT-DEFI
                                      TO IVVC0213-VAL-IMP-O
           END-IF.

       S1200-CONVERTI-CHAR-NUM-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   OPERAZIONI FINALI
      *----------------------------------------------------------------*
       S9000-OPERAZIONI-FINALI.

           GOBACK.

       EX-S9000.
           EXIT.

       IDENTIFICATION DIVISION.
       PROGRAM-ID.    IDBSTGA0 IS INITIAL.
       AUTHOR.        AISS.
       DATE-WRITTEN.  03 GIU 2019.
       DATE-COMPILED.
      *---------------------------------------------------------------*
      * P R O G E T T O : NEWLIFE                                     *
      * F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
      *---------------------------------------------------------------*
      *                                                               *
      *                                                               *
      *---------------------------------------------------------------*
       ENVIRONMENT DIVISION.
       CONFIGURATION SECTION.
          SOURCE-COMPUTER.
          OBJECT-COMPUTER.
       SPECIAL-NAMES.
           DECIMAL-POINT IS COMMA.
       DATA DIVISION.
       WORKING-STORAGE SECTION.

       77 DESCRIZ-ERR-DB2 PIC X(300)      VALUE SPACES.

       77 WS-BUFFER-TABLE         PIC X(2000) VALUE SPACES.
       77 WS-ID-MOVI-CRZ          PIC 9(009)  VALUE ZEROES.


           EXEC SQL INCLUDE IDSV0010 END-EXEC.



      *---------------------------------------------------------------*
      * AREA TABELLE DB2                                              *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE IDBDTGA0 END-EXEC.
           EXEC SQL INCLUDE IDBVTGA2 END-EXEC.
           EXEC SQL INCLUDE IDBVTGA3 END-EXEC.

      *---------------------------------------------------------------*
      * INCLUDE COMMON AREA DB2                                       *
      *---------------------------------------------------------------*

           EXEC SQL INCLUDE SQLCA    END-EXEC.

       LINKAGE SECTION.

           EXEC SQL INCLUDE IDSV0003 END-EXEC.

           EXEC SQL INCLUDE IDBVTGA1 END-EXEC.

       PROCEDURE DIVISION USING IDSV0003 TRCH-DI-GAR.

           PERFORM A000-INIZIO                    THRU A000-EX.

           IF IDSV0003-SUCCESSFUL-RC
              IF IDSV0003-TRATT-X-EFFETTO

                 EVALUATE TRUE
                    WHEN IDSV0003-ID
                       PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                    WHEN IDSV0003-ID-PADRE
                       PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                    WHEN IDSV0003-IB-OGGETTO
                       PERFORM A500-ELABORA-IBO          THRU A500-EX
                    WHEN IDSV0003-IB-SECONDARIO
                       PERFORM A600-ELABORA-IBS          THRU A600-EX
                    WHEN IDSV0003-ID-OGGETTO
                       PERFORM A700-ELABORA-IDO          THRU A700-EX
                    WHEN OTHER
                       SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                 END-EVALUATE

              ELSE

                IF IDSV0003-TRATT-X-COMPETENZA

                   EVALUATE TRUE
                      WHEN IDSV0003-ID
                         PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                      WHEN IDSV0003-ID-PADRE
                         PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                      WHEN IDSV0003-IB-OGGETTO
                         PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                      WHEN IDSV0003-IB-SECONDARIO
                         PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                      WHEN IDSV0003-ID-OGGETTO
                         PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                      WHEN OTHER
                         SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                   END-EVALUATE
                ELSE

                  IF IDSV0003-TRATT-SENZA-STOR

                     EVALUATE TRUE
                        WHEN IDSV0003-PRIMARY-KEY
                           PERFORM A200-ELABORA-PK          THRU A200-EX
                        WHEN IDSV0003-IB-OGGETTO
                           PERFORM A500-ELABORA-IBO         THRU A500-EX
                        WHEN IDSV0003-IB-SECONDARIO
                           PERFORM A600-ELABORA-IBS         THRU A600-EX
                        WHEN IDSV0003-ID-OGGETTO
                           PERFORM A700-ELABORA-IDO         THRU A700-EX
                        WHEN OTHER
                           SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                     END-EVALUATE

                  ELSE

                     SET IDSV0003-INVALID-LEVEL-OPER TO TRUE

                   END-IF
                 END-IF
              END-IF
           END-IF.

           GOBACK.

       A000-INIZIO.
           MOVE 'IDBSTGA0'   TO IDSV0003-COD-SERVIZIO-BE.
           MOVE 'TRCH_DI_GAR' TO IDSV0003-NOME-TABELLA.

           MOVE '00'                     TO   IDSV0003-RETURN-CODE.
           MOVE ZEROES                   TO   IDSV0003-SQLCODE
                                              IDSV0003-NUM-RIGHE-LETTE.
           MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
                                              IDSV0003-KEY-TABELLA.

           PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.

       A000-EX.
           EXIT.

       A100-CHECK-RETURN-CODE.
           IF IDSV0003-SUCCESSFUL-RC
              MOVE SQLCODE               TO   IDSV0003-SQLCODE
              MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2

              EVALUATE IDSV0003-SQLCODE
                  WHEN ZERO
                                CONTINUE
                  WHEN +100
                     IF IDSV0003-AGGIORNAMENTO-STORICO OR
                        IDSV0003-AGG-STORICO-SOLO-INS  OR
                        IDSV0003-DELETE-LOGICA         OR
                        IDSV0003-SELECT                OR
                        IDSV0003-FETCH-FIRST           OR
                        IDSV0003-FETCH-NEXT            OR
                        IDSV0003-FETCH-FIRST-MULTIPLE  OR
                        IDSV0003-FETCH-NEXT-MULTIPLE
                                CONTINUE
                     ELSE
                                SET IDSV0003-SQL-ERROR TO TRUE
                     END-IF
                  WHEN OTHER
                                SET IDSV0003-SQL-ERROR TO TRUE
              END-EVALUATE

           END-IF.
       A100-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A200-ELABORA-PK.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A210-SELECT-PK          THRU A210-EX
              WHEN IDSV0003-INSERT
                 PERFORM A220-INSERT-PK          THRU A220-EX
              WHEN IDSV0003-UPDATE
                 PERFORM A230-UPDATE-PK          THRU A230-EX
              WHEN IDSV0003-DELETE
                 PERFORM A240-DELETE-PK          THRU A240-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A200-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       A300-ELABORA-ID-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A300-EX.
           EXIT.

       A400-ELABORA-IDP-EFF.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A400-EX.
           EXIT.
      *----
      *----  Gestione prevista per tabelle Storiche e non
      *----
       A500-ELABORA-IBO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A510-SELECT-IBO             THRU A510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A500-EX.
           EXIT.

       A600-ELABORA-IBS.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A610-SELECT-IBS             THRU A610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A600-EX.
           EXIT.

       A700-ELABORA-IDO.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM A710-SELECT-IDO                 THRU A710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       A700-EX.
           EXIT.
      *----
      *----  Gestione prevista solo per tabelle Storiche
      *----
       B300-ELABORA-ID-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
              WHEN IDSV0003-AGGIORNAMENTO-STORICO
                   OR IDSV0003-DELETE-LOGICA
                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
              WHEN IDSV0003-AGG-STORICO-SOLO-INS
                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B300-EX.
           EXIT.

       B400-ELABORA-IDP-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B400-EX.
           EXIT.

       B500-ELABORA-IBO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B500-EX.
           EXIT.

       B600-ELABORA-IBS-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B600-EX.
           EXIT.

       B700-ELABORA-IDO-CPZ.
           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
              WHEN OTHER
                 SET IDSV0003-INVALID-OPER TO TRUE
           END-EVALUATE.
       B700-EX.
           EXIT.
      *----
      *----  gestione PK
      *----
       A210-SELECT-PK.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TRCH_DI_GAR
                ,ID_GAR
                ,ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,IB_OGG
                ,TP_RGM_FISC
                ,DT_EMIS
                ,TP_TRCH
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,PRE_CASO_MOR
                ,PC_INTR_RIAT
                ,IMP_BNS_ANTIC
                ,PRE_INI_NET
                ,PRE_PP_INI
                ,PRE_PP_ULT
                ,PRE_TARI_INI
                ,PRE_TARI_ULT
                ,PRE_INVRIO_INI
                ,PRE_INVRIO_ULT
                ,PRE_RIVTO
                ,IMP_SOPR_PROF
                ,IMP_SOPR_SAN
                ,IMP_SOPR_SPO
                ,IMP_SOPR_TEC
                ,IMP_ALT_SOPR
                ,PRE_STAB
                ,DT_EFF_STAB
                ,TS_RIVAL_FIS
                ,TS_RIVAL_INDICIZ
                ,OLD_TS_TEC
                ,RAT_LRD
                ,PRE_LRD
                ,PRSTZ_INI
                ,PRSTZ_ULT
                ,CPT_IN_OPZ_RIVTO
                ,PRSTZ_INI_STAB
                ,CPT_RSH_MOR
                ,PRSTZ_RID_INI
                ,FL_CAR_CONT
                ,BNS_GIA_LIQTO
                ,IMP_BNS
                ,COD_DVS
                ,PRSTZ_INI_NEWFIS
                ,IMP_SCON
                ,ALQ_SCON
                ,IMP_CAR_ACQ
                ,IMP_CAR_INC
                ,IMP_CAR_GEST
                ,ETA_AA_1O_ASSTO
                ,ETA_MM_1O_ASSTO
                ,ETA_AA_2O_ASSTO
                ,ETA_MM_2O_ASSTO
                ,ETA_AA_3O_ASSTO
                ,ETA_MM_3O_ASSTO
                ,RENDTO_LRD
                ,PC_RETR
                ,RENDTO_RETR
                ,MIN_GARTO
                ,MIN_TRNUT
                ,PRE_ATT_DI_TRCH
                ,MATU_END2000
                ,ABB_TOT_INI
                ,ABB_TOT_ULT
                ,ABB_ANNU_ULT
                ,DUR_ABB
                ,TP_ADEG_ABB
                ,MOD_CALC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,VIS_END2000
                ,DT_VLDT_PROD
                ,DT_INI_VAL_TAR
                ,IMPB_VIS_END2000
                ,REN_INI_TS_TEC_0
                ,PC_RIP_PRE
                ,FL_IMPORTI_FORZ
                ,PRSTZ_INI_NFORZ
                ,VIS_END2000_NFORZ
                ,INTR_MORA
                ,MANFEE_ANTIC
                ,MANFEE_RICOR
                ,PRE_UNI_RIVTO
                ,PROV_1AA_ACQ
                ,PROV_2AA_ACQ
                ,PROV_RICOR
                ,PROV_INC
                ,ALQ_PROV_ACQ
                ,ALQ_PROV_INC
                ,ALQ_PROV_RICOR
                ,IMPB_PROV_ACQ
                ,IMPB_PROV_INC
                ,IMPB_PROV_RICOR
                ,FL_PROV_FORZ
                ,PRSTZ_AGG_INI
                ,INCR_PRE
                ,INCR_PRSTZ
                ,DT_ULT_ADEG_PRE_PR
                ,PRSTZ_AGG_ULT
                ,TS_RIVAL_NET
                ,PRE_PATTUITO
                ,TP_RIVAL
                ,RIS_MAT
                ,CPT_MIN_SCAD
                ,COMMIS_GEST
                ,TP_MANFEE_APPL
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,PC_COMMIS_GEST
                ,NUM_GG_RIVAL
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,ACQ_EXP
                ,REMUN_ASS
                ,COMMIS_INTER
                ,ALQ_REMUN_ASS
                ,ALQ_COMMIS_INTER
                ,IMPB_REMUN_ASS
                ,IMPB_COMMIS_INTER
                ,COS_RUN_ASSVA
                ,COS_RUN_ASSVA_IDC
             INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
             FROM TRCH_DI_GAR
             WHERE     DS_RIGA = :TGA-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A210-EX.
           EXIT.

       A220-INSERT-PK.

           PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX

              PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX

              PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX

              PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX

              EXEC SQL
                 INSERT
                 INTO TRCH_DI_GAR
                     (
                        ID_TRCH_DI_GAR
                       ,ID_GAR
                       ,ID_ADES
                       ,ID_POLI
                       ,ID_MOVI_CRZ
                       ,ID_MOVI_CHIU
                       ,DT_INI_EFF
                       ,DT_END_EFF
                       ,COD_COMP_ANIA
                       ,DT_DECOR
                       ,DT_SCAD
                       ,IB_OGG
                       ,TP_RGM_FISC
                       ,DT_EMIS
                       ,TP_TRCH
                       ,DUR_AA
                       ,DUR_MM
                       ,DUR_GG
                       ,PRE_CASO_MOR
                       ,PC_INTR_RIAT
                       ,IMP_BNS_ANTIC
                       ,PRE_INI_NET
                       ,PRE_PP_INI
                       ,PRE_PP_ULT
                       ,PRE_TARI_INI
                       ,PRE_TARI_ULT
                       ,PRE_INVRIO_INI
                       ,PRE_INVRIO_ULT
                       ,PRE_RIVTO
                       ,IMP_SOPR_PROF
                       ,IMP_SOPR_SAN
                       ,IMP_SOPR_SPO
                       ,IMP_SOPR_TEC
                       ,IMP_ALT_SOPR
                       ,PRE_STAB
                       ,DT_EFF_STAB
                       ,TS_RIVAL_FIS
                       ,TS_RIVAL_INDICIZ
                       ,OLD_TS_TEC
                       ,RAT_LRD
                       ,PRE_LRD
                       ,PRSTZ_INI
                       ,PRSTZ_ULT
                       ,CPT_IN_OPZ_RIVTO
                       ,PRSTZ_INI_STAB
                       ,CPT_RSH_MOR
                       ,PRSTZ_RID_INI
                       ,FL_CAR_CONT
                       ,BNS_GIA_LIQTO
                       ,IMP_BNS
                       ,COD_DVS
                       ,PRSTZ_INI_NEWFIS
                       ,IMP_SCON
                       ,ALQ_SCON
                       ,IMP_CAR_ACQ
                       ,IMP_CAR_INC
                       ,IMP_CAR_GEST
                       ,ETA_AA_1O_ASSTO
                       ,ETA_MM_1O_ASSTO
                       ,ETA_AA_2O_ASSTO
                       ,ETA_MM_2O_ASSTO
                       ,ETA_AA_3O_ASSTO
                       ,ETA_MM_3O_ASSTO
                       ,RENDTO_LRD
                       ,PC_RETR
                       ,RENDTO_RETR
                       ,MIN_GARTO
                       ,MIN_TRNUT
                       ,PRE_ATT_DI_TRCH
                       ,MATU_END2000
                       ,ABB_TOT_INI
                       ,ABB_TOT_ULT
                       ,ABB_ANNU_ULT
                       ,DUR_ABB
                       ,TP_ADEG_ABB
                       ,MOD_CALC
                       ,IMP_AZ
                       ,IMP_ADER
                       ,IMP_TFR
                       ,IMP_VOLO
                       ,VIS_END2000
                       ,DT_VLDT_PROD
                       ,DT_INI_VAL_TAR
                       ,IMPB_VIS_END2000
                       ,REN_INI_TS_TEC_0
                       ,PC_RIP_PRE
                       ,FL_IMPORTI_FORZ
                       ,PRSTZ_INI_NFORZ
                       ,VIS_END2000_NFORZ
                       ,INTR_MORA
                       ,MANFEE_ANTIC
                       ,MANFEE_RICOR
                       ,PRE_UNI_RIVTO
                       ,PROV_1AA_ACQ
                       ,PROV_2AA_ACQ
                       ,PROV_RICOR
                       ,PROV_INC
                       ,ALQ_PROV_ACQ
                       ,ALQ_PROV_INC
                       ,ALQ_PROV_RICOR
                       ,IMPB_PROV_ACQ
                       ,IMPB_PROV_INC
                       ,IMPB_PROV_RICOR
                       ,FL_PROV_FORZ
                       ,PRSTZ_AGG_INI
                       ,INCR_PRE
                       ,INCR_PRSTZ
                       ,DT_ULT_ADEG_PRE_PR
                       ,PRSTZ_AGG_ULT
                       ,TS_RIVAL_NET
                       ,PRE_PATTUITO
                       ,TP_RIVAL
                       ,RIS_MAT
                       ,CPT_MIN_SCAD
                       ,COMMIS_GEST
                       ,TP_MANFEE_APPL
                       ,DS_RIGA
                       ,DS_OPER_SQL
                       ,DS_VER
                       ,DS_TS_INI_CPTZ
                       ,DS_TS_END_CPTZ
                       ,DS_UTENTE
                       ,DS_STATO_ELAB
                       ,PC_COMMIS_GEST
                       ,NUM_GG_RIVAL
                       ,IMP_TRASFE
                       ,IMP_TFR_STRC
                       ,ACQ_EXP
                       ,REMUN_ASS
                       ,COMMIS_INTER
                       ,ALQ_REMUN_ASS
                       ,ALQ_COMMIS_INTER
                       ,IMPB_REMUN_ASS
                       ,IMPB_COMMIS_INTER
                       ,COS_RUN_ASSVA
                       ,COS_RUN_ASSVA_IDC
                     )
                 VALUES
                     (
                       :TGA-ID-TRCH-DI-GAR
                       ,:TGA-ID-GAR
                       ,:TGA-ID-ADES
                       ,:TGA-ID-POLI
                       ,:TGA-ID-MOVI-CRZ
                       ,:TGA-ID-MOVI-CHIU
                        :IND-TGA-ID-MOVI-CHIU
                       ,:TGA-DT-INI-EFF-DB
                       ,:TGA-DT-END-EFF-DB
                       ,:TGA-COD-COMP-ANIA
                       ,:TGA-DT-DECOR-DB
                       ,:TGA-DT-SCAD-DB
                        :IND-TGA-DT-SCAD
                       ,:TGA-IB-OGG
                        :IND-TGA-IB-OGG
                       ,:TGA-TP-RGM-FISC
                       ,:TGA-DT-EMIS-DB
                        :IND-TGA-DT-EMIS
                       ,:TGA-TP-TRCH
                       ,:TGA-DUR-AA
                        :IND-TGA-DUR-AA
                       ,:TGA-DUR-MM
                        :IND-TGA-DUR-MM
                       ,:TGA-DUR-GG
                        :IND-TGA-DUR-GG
                       ,:TGA-PRE-CASO-MOR
                        :IND-TGA-PRE-CASO-MOR
                       ,:TGA-PC-INTR-RIAT
                        :IND-TGA-PC-INTR-RIAT
                       ,:TGA-IMP-BNS-ANTIC
                        :IND-TGA-IMP-BNS-ANTIC
                       ,:TGA-PRE-INI-NET
                        :IND-TGA-PRE-INI-NET
                       ,:TGA-PRE-PP-INI
                        :IND-TGA-PRE-PP-INI
                       ,:TGA-PRE-PP-ULT
                        :IND-TGA-PRE-PP-ULT
                       ,:TGA-PRE-TARI-INI
                        :IND-TGA-PRE-TARI-INI
                       ,:TGA-PRE-TARI-ULT
                        :IND-TGA-PRE-TARI-ULT
                       ,:TGA-PRE-INVRIO-INI
                        :IND-TGA-PRE-INVRIO-INI
                       ,:TGA-PRE-INVRIO-ULT
                        :IND-TGA-PRE-INVRIO-ULT
                       ,:TGA-PRE-RIVTO
                        :IND-TGA-PRE-RIVTO
                       ,:TGA-IMP-SOPR-PROF
                        :IND-TGA-IMP-SOPR-PROF
                       ,:TGA-IMP-SOPR-SAN
                        :IND-TGA-IMP-SOPR-SAN
                       ,:TGA-IMP-SOPR-SPO
                        :IND-TGA-IMP-SOPR-SPO
                       ,:TGA-IMP-SOPR-TEC
                        :IND-TGA-IMP-SOPR-TEC
                       ,:TGA-IMP-ALT-SOPR
                        :IND-TGA-IMP-ALT-SOPR
                       ,:TGA-PRE-STAB
                        :IND-TGA-PRE-STAB
                       ,:TGA-DT-EFF-STAB-DB
                        :IND-TGA-DT-EFF-STAB
                       ,:TGA-TS-RIVAL-FIS
                        :IND-TGA-TS-RIVAL-FIS
                       ,:TGA-TS-RIVAL-INDICIZ
                        :IND-TGA-TS-RIVAL-INDICIZ
                       ,:TGA-OLD-TS-TEC
                        :IND-TGA-OLD-TS-TEC
                       ,:TGA-RAT-LRD
                        :IND-TGA-RAT-LRD
                       ,:TGA-PRE-LRD
                        :IND-TGA-PRE-LRD
                       ,:TGA-PRSTZ-INI
                        :IND-TGA-PRSTZ-INI
                       ,:TGA-PRSTZ-ULT
                        :IND-TGA-PRSTZ-ULT
                       ,:TGA-CPT-IN-OPZ-RIVTO
                        :IND-TGA-CPT-IN-OPZ-RIVTO
                       ,:TGA-PRSTZ-INI-STAB
                        :IND-TGA-PRSTZ-INI-STAB
                       ,:TGA-CPT-RSH-MOR
                        :IND-TGA-CPT-RSH-MOR
                       ,:TGA-PRSTZ-RID-INI
                        :IND-TGA-PRSTZ-RID-INI
                       ,:TGA-FL-CAR-CONT
                        :IND-TGA-FL-CAR-CONT
                       ,:TGA-BNS-GIA-LIQTO
                        :IND-TGA-BNS-GIA-LIQTO
                       ,:TGA-IMP-BNS
                        :IND-TGA-IMP-BNS
                       ,:TGA-COD-DVS
                       ,:TGA-PRSTZ-INI-NEWFIS
                        :IND-TGA-PRSTZ-INI-NEWFIS
                       ,:TGA-IMP-SCON
                        :IND-TGA-IMP-SCON
                       ,:TGA-ALQ-SCON
                        :IND-TGA-ALQ-SCON
                       ,:TGA-IMP-CAR-ACQ
                        :IND-TGA-IMP-CAR-ACQ
                       ,:TGA-IMP-CAR-INC
                        :IND-TGA-IMP-CAR-INC
                       ,:TGA-IMP-CAR-GEST
                        :IND-TGA-IMP-CAR-GEST
                       ,:TGA-ETA-AA-1O-ASSTO
                        :IND-TGA-ETA-AA-1O-ASSTO
                       ,:TGA-ETA-MM-1O-ASSTO
                        :IND-TGA-ETA-MM-1O-ASSTO
                       ,:TGA-ETA-AA-2O-ASSTO
                        :IND-TGA-ETA-AA-2O-ASSTO
                       ,:TGA-ETA-MM-2O-ASSTO
                        :IND-TGA-ETA-MM-2O-ASSTO
                       ,:TGA-ETA-AA-3O-ASSTO
                        :IND-TGA-ETA-AA-3O-ASSTO
                       ,:TGA-ETA-MM-3O-ASSTO
                        :IND-TGA-ETA-MM-3O-ASSTO
                       ,:TGA-RENDTO-LRD
                        :IND-TGA-RENDTO-LRD
                       ,:TGA-PC-RETR
                        :IND-TGA-PC-RETR
                       ,:TGA-RENDTO-RETR
                        :IND-TGA-RENDTO-RETR
                       ,:TGA-MIN-GARTO
                        :IND-TGA-MIN-GARTO
                       ,:TGA-MIN-TRNUT
                        :IND-TGA-MIN-TRNUT
                       ,:TGA-PRE-ATT-DI-TRCH
                        :IND-TGA-PRE-ATT-DI-TRCH
                       ,:TGA-MATU-END2000
                        :IND-TGA-MATU-END2000
                       ,:TGA-ABB-TOT-INI
                        :IND-TGA-ABB-TOT-INI
                       ,:TGA-ABB-TOT-ULT
                        :IND-TGA-ABB-TOT-ULT
                       ,:TGA-ABB-ANNU-ULT
                        :IND-TGA-ABB-ANNU-ULT
                       ,:TGA-DUR-ABB
                        :IND-TGA-DUR-ABB
                       ,:TGA-TP-ADEG-ABB
                        :IND-TGA-TP-ADEG-ABB
                       ,:TGA-MOD-CALC
                        :IND-TGA-MOD-CALC
                       ,:TGA-IMP-AZ
                        :IND-TGA-IMP-AZ
                       ,:TGA-IMP-ADER
                        :IND-TGA-IMP-ADER
                       ,:TGA-IMP-TFR
                        :IND-TGA-IMP-TFR
                       ,:TGA-IMP-VOLO
                        :IND-TGA-IMP-VOLO
                       ,:TGA-VIS-END2000
                        :IND-TGA-VIS-END2000
                       ,:TGA-DT-VLDT-PROD-DB
                        :IND-TGA-DT-VLDT-PROD
                       ,:TGA-DT-INI-VAL-TAR-DB
                        :IND-TGA-DT-INI-VAL-TAR
                       ,:TGA-IMPB-VIS-END2000
                        :IND-TGA-IMPB-VIS-END2000
                       ,:TGA-REN-INI-TS-TEC-0
                        :IND-TGA-REN-INI-TS-TEC-0
                       ,:TGA-PC-RIP-PRE
                        :IND-TGA-PC-RIP-PRE
                       ,:TGA-FL-IMPORTI-FORZ
                        :IND-TGA-FL-IMPORTI-FORZ
                       ,:TGA-PRSTZ-INI-NFORZ
                        :IND-TGA-PRSTZ-INI-NFORZ
                       ,:TGA-VIS-END2000-NFORZ
                        :IND-TGA-VIS-END2000-NFORZ
                       ,:TGA-INTR-MORA
                        :IND-TGA-INTR-MORA
                       ,:TGA-MANFEE-ANTIC
                        :IND-TGA-MANFEE-ANTIC
                       ,:TGA-MANFEE-RICOR
                        :IND-TGA-MANFEE-RICOR
                       ,:TGA-PRE-UNI-RIVTO
                        :IND-TGA-PRE-UNI-RIVTO
                       ,:TGA-PROV-1AA-ACQ
                        :IND-TGA-PROV-1AA-ACQ
                       ,:TGA-PROV-2AA-ACQ
                        :IND-TGA-PROV-2AA-ACQ
                       ,:TGA-PROV-RICOR
                        :IND-TGA-PROV-RICOR
                       ,:TGA-PROV-INC
                        :IND-TGA-PROV-INC
                       ,:TGA-ALQ-PROV-ACQ
                        :IND-TGA-ALQ-PROV-ACQ
                       ,:TGA-ALQ-PROV-INC
                        :IND-TGA-ALQ-PROV-INC
                       ,:TGA-ALQ-PROV-RICOR
                        :IND-TGA-ALQ-PROV-RICOR
                       ,:TGA-IMPB-PROV-ACQ
                        :IND-TGA-IMPB-PROV-ACQ
                       ,:TGA-IMPB-PROV-INC
                        :IND-TGA-IMPB-PROV-INC
                       ,:TGA-IMPB-PROV-RICOR
                        :IND-TGA-IMPB-PROV-RICOR
                       ,:TGA-FL-PROV-FORZ
                        :IND-TGA-FL-PROV-FORZ
                       ,:TGA-PRSTZ-AGG-INI
                        :IND-TGA-PRSTZ-AGG-INI
                       ,:TGA-INCR-PRE
                        :IND-TGA-INCR-PRE
                       ,:TGA-INCR-PRSTZ
                        :IND-TGA-INCR-PRSTZ
                       ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                        :IND-TGA-DT-ULT-ADEG-PRE-PR
                       ,:TGA-PRSTZ-AGG-ULT
                        :IND-TGA-PRSTZ-AGG-ULT
                       ,:TGA-TS-RIVAL-NET
                        :IND-TGA-TS-RIVAL-NET
                       ,:TGA-PRE-PATTUITO
                        :IND-TGA-PRE-PATTUITO
                       ,:TGA-TP-RIVAL
                        :IND-TGA-TP-RIVAL
                       ,:TGA-RIS-MAT
                        :IND-TGA-RIS-MAT
                       ,:TGA-CPT-MIN-SCAD
                        :IND-TGA-CPT-MIN-SCAD
                       ,:TGA-COMMIS-GEST
                        :IND-TGA-COMMIS-GEST
                       ,:TGA-TP-MANFEE-APPL
                        :IND-TGA-TP-MANFEE-APPL
                       ,:TGA-DS-RIGA
                       ,:TGA-DS-OPER-SQL
                       ,:TGA-DS-VER
                       ,:TGA-DS-TS-INI-CPTZ
                       ,:TGA-DS-TS-END-CPTZ
                       ,:TGA-DS-UTENTE
                       ,:TGA-DS-STATO-ELAB
                       ,:TGA-PC-COMMIS-GEST
                        :IND-TGA-PC-COMMIS-GEST
                       ,:TGA-NUM-GG-RIVAL
                        :IND-TGA-NUM-GG-RIVAL
                       ,:TGA-IMP-TRASFE
                        :IND-TGA-IMP-TRASFE
                       ,:TGA-IMP-TFR-STRC
                        :IND-TGA-IMP-TFR-STRC
                       ,:TGA-ACQ-EXP
                        :IND-TGA-ACQ-EXP
                       ,:TGA-REMUN-ASS
                        :IND-TGA-REMUN-ASS
                       ,:TGA-COMMIS-INTER
                        :IND-TGA-COMMIS-INTER
                       ,:TGA-ALQ-REMUN-ASS
                        :IND-TGA-ALQ-REMUN-ASS
                       ,:TGA-ALQ-COMMIS-INTER
                        :IND-TGA-ALQ-COMMIS-INTER
                       ,:TGA-IMPB-REMUN-ASS
                        :IND-TGA-IMPB-REMUN-ASS
                       ,:TGA-IMPB-COMMIS-INTER
                        :IND-TGA-IMPB-COMMIS-INTER
                       ,:TGA-COS-RUN-ASSVA
                        :IND-TGA-COS-RUN-ASSVA
                       ,:TGA-COS-RUN-ASSVA-IDC
                        :IND-TGA-COS-RUN-ASSVA-IDC
                     )

              END-EXEC

              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX

           END-IF.
       A220-EX.
           EXIT.

       A230-UPDATE-PK.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.

           EXEC SQL
                UPDATE TRCH_DI_GAR SET

                   ID_TRCH_DI_GAR         =
                :TGA-ID-TRCH-DI-GAR
                  ,ID_GAR                 =
                :TGA-ID-GAR
                  ,ID_ADES                =
                :TGA-ID-ADES
                  ,ID_POLI                =
                :TGA-ID-POLI
                  ,ID_MOVI_CRZ            =
                :TGA-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :TGA-ID-MOVI-CHIU
                                       :IND-TGA-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :TGA-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :TGA-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :TGA-COD-COMP-ANIA
                  ,DT_DECOR               =
           :TGA-DT-DECOR-DB
                  ,DT_SCAD                =
           :TGA-DT-SCAD-DB
                                       :IND-TGA-DT-SCAD
                  ,IB_OGG                 =
                :TGA-IB-OGG
                                       :IND-TGA-IB-OGG
                  ,TP_RGM_FISC            =
                :TGA-TP-RGM-FISC
                  ,DT_EMIS                =
           :TGA-DT-EMIS-DB
                                       :IND-TGA-DT-EMIS
                  ,TP_TRCH                =
                :TGA-TP-TRCH
                  ,DUR_AA                 =
                :TGA-DUR-AA
                                       :IND-TGA-DUR-AA
                  ,DUR_MM                 =
                :TGA-DUR-MM
                                       :IND-TGA-DUR-MM
                  ,DUR_GG                 =
                :TGA-DUR-GG
                                       :IND-TGA-DUR-GG
                  ,PRE_CASO_MOR           =
                :TGA-PRE-CASO-MOR
                                       :IND-TGA-PRE-CASO-MOR
                  ,PC_INTR_RIAT           =
                :TGA-PC-INTR-RIAT
                                       :IND-TGA-PC-INTR-RIAT
                  ,IMP_BNS_ANTIC          =
                :TGA-IMP-BNS-ANTIC
                                       :IND-TGA-IMP-BNS-ANTIC
                  ,PRE_INI_NET            =
                :TGA-PRE-INI-NET
                                       :IND-TGA-PRE-INI-NET
                  ,PRE_PP_INI             =
                :TGA-PRE-PP-INI
                                       :IND-TGA-PRE-PP-INI
                  ,PRE_PP_ULT             =
                :TGA-PRE-PP-ULT
                                       :IND-TGA-PRE-PP-ULT
                  ,PRE_TARI_INI           =
                :TGA-PRE-TARI-INI
                                       :IND-TGA-PRE-TARI-INI
                  ,PRE_TARI_ULT           =
                :TGA-PRE-TARI-ULT
                                       :IND-TGA-PRE-TARI-ULT
                  ,PRE_INVRIO_INI         =
                :TGA-PRE-INVRIO-INI
                                       :IND-TGA-PRE-INVRIO-INI
                  ,PRE_INVRIO_ULT         =
                :TGA-PRE-INVRIO-ULT
                                       :IND-TGA-PRE-INVRIO-ULT
                  ,PRE_RIVTO              =
                :TGA-PRE-RIVTO
                                       :IND-TGA-PRE-RIVTO
                  ,IMP_SOPR_PROF          =
                :TGA-IMP-SOPR-PROF
                                       :IND-TGA-IMP-SOPR-PROF
                  ,IMP_SOPR_SAN           =
                :TGA-IMP-SOPR-SAN
                                       :IND-TGA-IMP-SOPR-SAN
                  ,IMP_SOPR_SPO           =
                :TGA-IMP-SOPR-SPO
                                       :IND-TGA-IMP-SOPR-SPO
                  ,IMP_SOPR_TEC           =
                :TGA-IMP-SOPR-TEC
                                       :IND-TGA-IMP-SOPR-TEC
                  ,IMP_ALT_SOPR           =
                :TGA-IMP-ALT-SOPR
                                       :IND-TGA-IMP-ALT-SOPR
                  ,PRE_STAB               =
                :TGA-PRE-STAB
                                       :IND-TGA-PRE-STAB
                  ,DT_EFF_STAB            =
           :TGA-DT-EFF-STAB-DB
                                       :IND-TGA-DT-EFF-STAB
                  ,TS_RIVAL_FIS           =
                :TGA-TS-RIVAL-FIS
                                       :IND-TGA-TS-RIVAL-FIS
                  ,TS_RIVAL_INDICIZ       =
                :TGA-TS-RIVAL-INDICIZ
                                       :IND-TGA-TS-RIVAL-INDICIZ
                  ,OLD_TS_TEC             =
                :TGA-OLD-TS-TEC
                                       :IND-TGA-OLD-TS-TEC
                  ,RAT_LRD                =
                :TGA-RAT-LRD
                                       :IND-TGA-RAT-LRD
                  ,PRE_LRD                =
                :TGA-PRE-LRD
                                       :IND-TGA-PRE-LRD
                  ,PRSTZ_INI              =
                :TGA-PRSTZ-INI
                                       :IND-TGA-PRSTZ-INI
                  ,PRSTZ_ULT              =
                :TGA-PRSTZ-ULT
                                       :IND-TGA-PRSTZ-ULT
                  ,CPT_IN_OPZ_RIVTO       =
                :TGA-CPT-IN-OPZ-RIVTO
                                       :IND-TGA-CPT-IN-OPZ-RIVTO
                  ,PRSTZ_INI_STAB         =
                :TGA-PRSTZ-INI-STAB
                                       :IND-TGA-PRSTZ-INI-STAB
                  ,CPT_RSH_MOR            =
                :TGA-CPT-RSH-MOR
                                       :IND-TGA-CPT-RSH-MOR
                  ,PRSTZ_RID_INI          =
                :TGA-PRSTZ-RID-INI
                                       :IND-TGA-PRSTZ-RID-INI
                  ,FL_CAR_CONT            =
                :TGA-FL-CAR-CONT
                                       :IND-TGA-FL-CAR-CONT
                  ,BNS_GIA_LIQTO          =
                :TGA-BNS-GIA-LIQTO
                                       :IND-TGA-BNS-GIA-LIQTO
                  ,IMP_BNS                =
                :TGA-IMP-BNS
                                       :IND-TGA-IMP-BNS
                  ,COD_DVS                =
                :TGA-COD-DVS
                  ,PRSTZ_INI_NEWFIS       =
                :TGA-PRSTZ-INI-NEWFIS
                                       :IND-TGA-PRSTZ-INI-NEWFIS
                  ,IMP_SCON               =
                :TGA-IMP-SCON
                                       :IND-TGA-IMP-SCON
                  ,ALQ_SCON               =
                :TGA-ALQ-SCON
                                       :IND-TGA-ALQ-SCON
                  ,IMP_CAR_ACQ            =
                :TGA-IMP-CAR-ACQ
                                       :IND-TGA-IMP-CAR-ACQ
                  ,IMP_CAR_INC            =
                :TGA-IMP-CAR-INC
                                       :IND-TGA-IMP-CAR-INC
                  ,IMP_CAR_GEST           =
                :TGA-IMP-CAR-GEST
                                       :IND-TGA-IMP-CAR-GEST
                  ,ETA_AA_1O_ASSTO        =
                :TGA-ETA-AA-1O-ASSTO
                                       :IND-TGA-ETA-AA-1O-ASSTO
                  ,ETA_MM_1O_ASSTO        =
                :TGA-ETA-MM-1O-ASSTO
                                       :IND-TGA-ETA-MM-1O-ASSTO
                  ,ETA_AA_2O_ASSTO        =
                :TGA-ETA-AA-2O-ASSTO
                                       :IND-TGA-ETA-AA-2O-ASSTO
                  ,ETA_MM_2O_ASSTO        =
                :TGA-ETA-MM-2O-ASSTO
                                       :IND-TGA-ETA-MM-2O-ASSTO
                  ,ETA_AA_3O_ASSTO        =
                :TGA-ETA-AA-3O-ASSTO
                                       :IND-TGA-ETA-AA-3O-ASSTO
                  ,ETA_MM_3O_ASSTO        =
                :TGA-ETA-MM-3O-ASSTO
                                       :IND-TGA-ETA-MM-3O-ASSTO
                  ,RENDTO_LRD             =
                :TGA-RENDTO-LRD
                                       :IND-TGA-RENDTO-LRD
                  ,PC_RETR                =
                :TGA-PC-RETR
                                       :IND-TGA-PC-RETR
                  ,RENDTO_RETR            =
                :TGA-RENDTO-RETR
                                       :IND-TGA-RENDTO-RETR
                  ,MIN_GARTO              =
                :TGA-MIN-GARTO
                                       :IND-TGA-MIN-GARTO
                  ,MIN_TRNUT              =
                :TGA-MIN-TRNUT
                                       :IND-TGA-MIN-TRNUT
                  ,PRE_ATT_DI_TRCH        =
                :TGA-PRE-ATT-DI-TRCH
                                       :IND-TGA-PRE-ATT-DI-TRCH
                  ,MATU_END2000           =
                :TGA-MATU-END2000
                                       :IND-TGA-MATU-END2000
                  ,ABB_TOT_INI            =
                :TGA-ABB-TOT-INI
                                       :IND-TGA-ABB-TOT-INI
                  ,ABB_TOT_ULT            =
                :TGA-ABB-TOT-ULT
                                       :IND-TGA-ABB-TOT-ULT
                  ,ABB_ANNU_ULT           =
                :TGA-ABB-ANNU-ULT
                                       :IND-TGA-ABB-ANNU-ULT
                  ,DUR_ABB                =
                :TGA-DUR-ABB
                                       :IND-TGA-DUR-ABB
                  ,TP_ADEG_ABB            =
                :TGA-TP-ADEG-ABB
                                       :IND-TGA-TP-ADEG-ABB
                  ,MOD_CALC               =
                :TGA-MOD-CALC
                                       :IND-TGA-MOD-CALC
                  ,IMP_AZ                 =
                :TGA-IMP-AZ
                                       :IND-TGA-IMP-AZ
                  ,IMP_ADER               =
                :TGA-IMP-ADER
                                       :IND-TGA-IMP-ADER
                  ,IMP_TFR                =
                :TGA-IMP-TFR
                                       :IND-TGA-IMP-TFR
                  ,IMP_VOLO               =
                :TGA-IMP-VOLO
                                       :IND-TGA-IMP-VOLO
                  ,VIS_END2000            =
                :TGA-VIS-END2000
                                       :IND-TGA-VIS-END2000
                  ,DT_VLDT_PROD           =
           :TGA-DT-VLDT-PROD-DB
                                       :IND-TGA-DT-VLDT-PROD
                  ,DT_INI_VAL_TAR         =
           :TGA-DT-INI-VAL-TAR-DB
                                       :IND-TGA-DT-INI-VAL-TAR
                  ,IMPB_VIS_END2000       =
                :TGA-IMPB-VIS-END2000
                                       :IND-TGA-IMPB-VIS-END2000
                  ,REN_INI_TS_TEC_0       =
                :TGA-REN-INI-TS-TEC-0
                                       :IND-TGA-REN-INI-TS-TEC-0
                  ,PC_RIP_PRE             =
                :TGA-PC-RIP-PRE
                                       :IND-TGA-PC-RIP-PRE
                  ,FL_IMPORTI_FORZ        =
                :TGA-FL-IMPORTI-FORZ
                                       :IND-TGA-FL-IMPORTI-FORZ
                  ,PRSTZ_INI_NFORZ        =
                :TGA-PRSTZ-INI-NFORZ
                                       :IND-TGA-PRSTZ-INI-NFORZ
                  ,VIS_END2000_NFORZ      =
                :TGA-VIS-END2000-NFORZ
                                       :IND-TGA-VIS-END2000-NFORZ
                  ,INTR_MORA              =
                :TGA-INTR-MORA
                                       :IND-TGA-INTR-MORA
                  ,MANFEE_ANTIC           =
                :TGA-MANFEE-ANTIC
                                       :IND-TGA-MANFEE-ANTIC
                  ,MANFEE_RICOR           =
                :TGA-MANFEE-RICOR
                                       :IND-TGA-MANFEE-RICOR
                  ,PRE_UNI_RIVTO          =
                :TGA-PRE-UNI-RIVTO
                                       :IND-TGA-PRE-UNI-RIVTO
                  ,PROV_1AA_ACQ           =
                :TGA-PROV-1AA-ACQ
                                       :IND-TGA-PROV-1AA-ACQ
                  ,PROV_2AA_ACQ           =
                :TGA-PROV-2AA-ACQ
                                       :IND-TGA-PROV-2AA-ACQ
                  ,PROV_RICOR             =
                :TGA-PROV-RICOR
                                       :IND-TGA-PROV-RICOR
                  ,PROV_INC               =
                :TGA-PROV-INC
                                       :IND-TGA-PROV-INC
                  ,ALQ_PROV_ACQ           =
                :TGA-ALQ-PROV-ACQ
                                       :IND-TGA-ALQ-PROV-ACQ
                  ,ALQ_PROV_INC           =
                :TGA-ALQ-PROV-INC
                                       :IND-TGA-ALQ-PROV-INC
                  ,ALQ_PROV_RICOR         =
                :TGA-ALQ-PROV-RICOR
                                       :IND-TGA-ALQ-PROV-RICOR
                  ,IMPB_PROV_ACQ          =
                :TGA-IMPB-PROV-ACQ
                                       :IND-TGA-IMPB-PROV-ACQ
                  ,IMPB_PROV_INC          =
                :TGA-IMPB-PROV-INC
                                       :IND-TGA-IMPB-PROV-INC
                  ,IMPB_PROV_RICOR        =
                :TGA-IMPB-PROV-RICOR
                                       :IND-TGA-IMPB-PROV-RICOR
                  ,FL_PROV_FORZ           =
                :TGA-FL-PROV-FORZ
                                       :IND-TGA-FL-PROV-FORZ
                  ,PRSTZ_AGG_INI          =
                :TGA-PRSTZ-AGG-INI
                                       :IND-TGA-PRSTZ-AGG-INI
                  ,INCR_PRE               =
                :TGA-INCR-PRE
                                       :IND-TGA-INCR-PRE
                  ,INCR_PRSTZ             =
                :TGA-INCR-PRSTZ
                                       :IND-TGA-INCR-PRSTZ
                  ,DT_ULT_ADEG_PRE_PR     =
           :TGA-DT-ULT-ADEG-PRE-PR-DB
                                       :IND-TGA-DT-ULT-ADEG-PRE-PR
                  ,PRSTZ_AGG_ULT          =
                :TGA-PRSTZ-AGG-ULT
                                       :IND-TGA-PRSTZ-AGG-ULT
                  ,TS_RIVAL_NET           =
                :TGA-TS-RIVAL-NET
                                       :IND-TGA-TS-RIVAL-NET
                  ,PRE_PATTUITO           =
                :TGA-PRE-PATTUITO
                                       :IND-TGA-PRE-PATTUITO
                  ,TP_RIVAL               =
                :TGA-TP-RIVAL
                                       :IND-TGA-TP-RIVAL
                  ,RIS_MAT                =
                :TGA-RIS-MAT
                                       :IND-TGA-RIS-MAT
                  ,CPT_MIN_SCAD           =
                :TGA-CPT-MIN-SCAD
                                       :IND-TGA-CPT-MIN-SCAD
                  ,COMMIS_GEST            =
                :TGA-COMMIS-GEST
                                       :IND-TGA-COMMIS-GEST
                  ,TP_MANFEE_APPL         =
                :TGA-TP-MANFEE-APPL
                                       :IND-TGA-TP-MANFEE-APPL
                  ,DS_RIGA                =
                :TGA-DS-RIGA
                  ,DS_OPER_SQL            =
                :TGA-DS-OPER-SQL
                  ,DS_VER                 =
                :TGA-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :TGA-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :TGA-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :TGA-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :TGA-DS-STATO-ELAB
                  ,PC_COMMIS_GEST         =
                :TGA-PC-COMMIS-GEST
                                       :IND-TGA-PC-COMMIS-GEST
                  ,NUM_GG_RIVAL           =
                :TGA-NUM-GG-RIVAL
                                       :IND-TGA-NUM-GG-RIVAL
                  ,IMP_TRASFE             =
                :TGA-IMP-TRASFE
                                       :IND-TGA-IMP-TRASFE
                  ,IMP_TFR_STRC           =
                :TGA-IMP-TFR-STRC
                                       :IND-TGA-IMP-TFR-STRC
                  ,ACQ_EXP                =
                :TGA-ACQ-EXP
                                       :IND-TGA-ACQ-EXP
                  ,REMUN_ASS              =
                :TGA-REMUN-ASS
                                       :IND-TGA-REMUN-ASS
                  ,COMMIS_INTER           =
                :TGA-COMMIS-INTER
                                       :IND-TGA-COMMIS-INTER
                  ,ALQ_REMUN_ASS          =
                :TGA-ALQ-REMUN-ASS
                                       :IND-TGA-ALQ-REMUN-ASS
                  ,ALQ_COMMIS_INTER       =
                :TGA-ALQ-COMMIS-INTER
                                       :IND-TGA-ALQ-COMMIS-INTER
                  ,IMPB_REMUN_ASS         =
                :TGA-IMPB-REMUN-ASS
                                       :IND-TGA-IMPB-REMUN-ASS
                  ,IMPB_COMMIS_INTER      =
                :TGA-IMPB-COMMIS-INTER
                                       :IND-TGA-IMPB-COMMIS-INTER
                  ,COS_RUN_ASSVA          =
                :TGA-COS-RUN-ASSVA
                                       :IND-TGA-COS-RUN-ASSVA
                  ,COS_RUN_ASSVA_IDC      =
                :TGA-COS-RUN-ASSVA-IDC
                                       :IND-TGA-COS-RUN-ASSVA-IDC
                WHERE     DS_RIGA = :TGA-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A230-EX.
           EXIT.

       A240-DELETE-PK.
           EXEC SQL
                DELETE
                FROM TRCH_DI_GAR
                WHERE     DS_RIGA = :TGA-DS-RIGA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A240-EX.
           EXIT.
      *----
      *----  gestione ID Effetto
      *----
       A305-DECLARE-CURSOR-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-ID-UPD-EFF-TGA CURSOR FOR
              SELECT
                     ID_TRCH_DI_GAR
                    ,ID_GAR
                    ,ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_SCAD
                    ,IB_OGG
                    ,TP_RGM_FISC
                    ,DT_EMIS
                    ,TP_TRCH
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,PRE_CASO_MOR
                    ,PC_INTR_RIAT
                    ,IMP_BNS_ANTIC
                    ,PRE_INI_NET
                    ,PRE_PP_INI
                    ,PRE_PP_ULT
                    ,PRE_TARI_INI
                    ,PRE_TARI_ULT
                    ,PRE_INVRIO_INI
                    ,PRE_INVRIO_ULT
                    ,PRE_RIVTO
                    ,IMP_SOPR_PROF
                    ,IMP_SOPR_SAN
                    ,IMP_SOPR_SPO
                    ,IMP_SOPR_TEC
                    ,IMP_ALT_SOPR
                    ,PRE_STAB
                    ,DT_EFF_STAB
                    ,TS_RIVAL_FIS
                    ,TS_RIVAL_INDICIZ
                    ,OLD_TS_TEC
                    ,RAT_LRD
                    ,PRE_LRD
                    ,PRSTZ_INI
                    ,PRSTZ_ULT
                    ,CPT_IN_OPZ_RIVTO
                    ,PRSTZ_INI_STAB
                    ,CPT_RSH_MOR
                    ,PRSTZ_RID_INI
                    ,FL_CAR_CONT
                    ,BNS_GIA_LIQTO
                    ,IMP_BNS
                    ,COD_DVS
                    ,PRSTZ_INI_NEWFIS
                    ,IMP_SCON
                    ,ALQ_SCON
                    ,IMP_CAR_ACQ
                    ,IMP_CAR_INC
                    ,IMP_CAR_GEST
                    ,ETA_AA_1O_ASSTO
                    ,ETA_MM_1O_ASSTO
                    ,ETA_AA_2O_ASSTO
                    ,ETA_MM_2O_ASSTO
                    ,ETA_AA_3O_ASSTO
                    ,ETA_MM_3O_ASSTO
                    ,RENDTO_LRD
                    ,PC_RETR
                    ,RENDTO_RETR
                    ,MIN_GARTO
                    ,MIN_TRNUT
                    ,PRE_ATT_DI_TRCH
                    ,MATU_END2000
                    ,ABB_TOT_INI
                    ,ABB_TOT_ULT
                    ,ABB_ANNU_ULT
                    ,DUR_ABB
                    ,TP_ADEG_ABB
                    ,MOD_CALC
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,VIS_END2000
                    ,DT_VLDT_PROD
                    ,DT_INI_VAL_TAR
                    ,IMPB_VIS_END2000
                    ,REN_INI_TS_TEC_0
                    ,PC_RIP_PRE
                    ,FL_IMPORTI_FORZ
                    ,PRSTZ_INI_NFORZ
                    ,VIS_END2000_NFORZ
                    ,INTR_MORA
                    ,MANFEE_ANTIC
                    ,MANFEE_RICOR
                    ,PRE_UNI_RIVTO
                    ,PROV_1AA_ACQ
                    ,PROV_2AA_ACQ
                    ,PROV_RICOR
                    ,PROV_INC
                    ,ALQ_PROV_ACQ
                    ,ALQ_PROV_INC
                    ,ALQ_PROV_RICOR
                    ,IMPB_PROV_ACQ
                    ,IMPB_PROV_INC
                    ,IMPB_PROV_RICOR
                    ,FL_PROV_FORZ
                    ,PRSTZ_AGG_INI
                    ,INCR_PRE
                    ,INCR_PRSTZ
                    ,DT_ULT_ADEG_PRE_PR
                    ,PRSTZ_AGG_ULT
                    ,TS_RIVAL_NET
                    ,PRE_PATTUITO
                    ,TP_RIVAL
                    ,RIS_MAT
                    ,CPT_MIN_SCAD
                    ,COMMIS_GEST
                    ,TP_MANFEE_APPL
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,PC_COMMIS_GEST
                    ,NUM_GG_RIVAL
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,ACQ_EXP
                    ,REMUN_ASS
                    ,COMMIS_INTER
                    ,ALQ_REMUN_ASS
                    ,ALQ_COMMIS_INTER
                    ,IMPB_REMUN_ASS
                    ,IMPB_COMMIS_INTER
                    ,COS_RUN_ASSVA
                    ,COS_RUN_ASSVA_IDC
              FROM TRCH_DI_GAR
              WHERE     ID_TRCH_DI_GAR = :TGA-ID-TRCH-DI-GAR
                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
              ORDER BY DT_INI_EFF ASC

           END-EXEC.

       A305-EX.
           EXIT.

       A310-SELECT-ID-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TRCH_DI_GAR
                ,ID_GAR
                ,ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,IB_OGG
                ,TP_RGM_FISC
                ,DT_EMIS
                ,TP_TRCH
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,PRE_CASO_MOR
                ,PC_INTR_RIAT
                ,IMP_BNS_ANTIC
                ,PRE_INI_NET
                ,PRE_PP_INI
                ,PRE_PP_ULT
                ,PRE_TARI_INI
                ,PRE_TARI_ULT
                ,PRE_INVRIO_INI
                ,PRE_INVRIO_ULT
                ,PRE_RIVTO
                ,IMP_SOPR_PROF
                ,IMP_SOPR_SAN
                ,IMP_SOPR_SPO
                ,IMP_SOPR_TEC
                ,IMP_ALT_SOPR
                ,PRE_STAB
                ,DT_EFF_STAB
                ,TS_RIVAL_FIS
                ,TS_RIVAL_INDICIZ
                ,OLD_TS_TEC
                ,RAT_LRD
                ,PRE_LRD
                ,PRSTZ_INI
                ,PRSTZ_ULT
                ,CPT_IN_OPZ_RIVTO
                ,PRSTZ_INI_STAB
                ,CPT_RSH_MOR
                ,PRSTZ_RID_INI
                ,FL_CAR_CONT
                ,BNS_GIA_LIQTO
                ,IMP_BNS
                ,COD_DVS
                ,PRSTZ_INI_NEWFIS
                ,IMP_SCON
                ,ALQ_SCON
                ,IMP_CAR_ACQ
                ,IMP_CAR_INC
                ,IMP_CAR_GEST
                ,ETA_AA_1O_ASSTO
                ,ETA_MM_1O_ASSTO
                ,ETA_AA_2O_ASSTO
                ,ETA_MM_2O_ASSTO
                ,ETA_AA_3O_ASSTO
                ,ETA_MM_3O_ASSTO
                ,RENDTO_LRD
                ,PC_RETR
                ,RENDTO_RETR
                ,MIN_GARTO
                ,MIN_TRNUT
                ,PRE_ATT_DI_TRCH
                ,MATU_END2000
                ,ABB_TOT_INI
                ,ABB_TOT_ULT
                ,ABB_ANNU_ULT
                ,DUR_ABB
                ,TP_ADEG_ABB
                ,MOD_CALC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,VIS_END2000
                ,DT_VLDT_PROD
                ,DT_INI_VAL_TAR
                ,IMPB_VIS_END2000
                ,REN_INI_TS_TEC_0
                ,PC_RIP_PRE
                ,FL_IMPORTI_FORZ
                ,PRSTZ_INI_NFORZ
                ,VIS_END2000_NFORZ
                ,INTR_MORA
                ,MANFEE_ANTIC
                ,MANFEE_RICOR
                ,PRE_UNI_RIVTO
                ,PROV_1AA_ACQ
                ,PROV_2AA_ACQ
                ,PROV_RICOR
                ,PROV_INC
                ,ALQ_PROV_ACQ
                ,ALQ_PROV_INC
                ,ALQ_PROV_RICOR
                ,IMPB_PROV_ACQ
                ,IMPB_PROV_INC
                ,IMPB_PROV_RICOR
                ,FL_PROV_FORZ
                ,PRSTZ_AGG_INI
                ,INCR_PRE
                ,INCR_PRSTZ
                ,DT_ULT_ADEG_PRE_PR
                ,PRSTZ_AGG_ULT
                ,TS_RIVAL_NET
                ,PRE_PATTUITO
                ,TP_RIVAL
                ,RIS_MAT
                ,CPT_MIN_SCAD
                ,COMMIS_GEST
                ,TP_MANFEE_APPL
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,PC_COMMIS_GEST
                ,NUM_GG_RIVAL
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,ACQ_EXP
                ,REMUN_ASS
                ,COMMIS_INTER
                ,ALQ_REMUN_ASS
                ,ALQ_COMMIS_INTER
                ,IMPB_REMUN_ASS
                ,IMPB_COMMIS_INTER
                ,COS_RUN_ASSVA
                ,COS_RUN_ASSVA_IDC
             INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
             FROM TRCH_DI_GAR
             WHERE     ID_TRCH_DI_GAR = :TGA-ID-TRCH-DI-GAR
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A310-EX.
           EXIT.

       A330-UPDATE-ID-EFF.
           PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.

           PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.

           PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.

           PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.

           EXEC SQL
                UPDATE TRCH_DI_GAR SET

                   ID_TRCH_DI_GAR         =
                :TGA-ID-TRCH-DI-GAR
                  ,ID_GAR                 =
                :TGA-ID-GAR
                  ,ID_ADES                =
                :TGA-ID-ADES
                  ,ID_POLI                =
                :TGA-ID-POLI
                  ,ID_MOVI_CRZ            =
                :TGA-ID-MOVI-CRZ
                  ,ID_MOVI_CHIU           =
                :TGA-ID-MOVI-CHIU
                                       :IND-TGA-ID-MOVI-CHIU
                  ,DT_INI_EFF             =
           :TGA-DT-INI-EFF-DB
                  ,DT_END_EFF             =
           :TGA-DT-END-EFF-DB
                  ,COD_COMP_ANIA          =
                :TGA-COD-COMP-ANIA
                  ,DT_DECOR               =
           :TGA-DT-DECOR-DB
                  ,DT_SCAD                =
           :TGA-DT-SCAD-DB
                                       :IND-TGA-DT-SCAD
                  ,IB_OGG                 =
                :TGA-IB-OGG
                                       :IND-TGA-IB-OGG
                  ,TP_RGM_FISC            =
                :TGA-TP-RGM-FISC
                  ,DT_EMIS                =
           :TGA-DT-EMIS-DB
                                       :IND-TGA-DT-EMIS
                  ,TP_TRCH                =
                :TGA-TP-TRCH
                  ,DUR_AA                 =
                :TGA-DUR-AA
                                       :IND-TGA-DUR-AA
                  ,DUR_MM                 =
                :TGA-DUR-MM
                                       :IND-TGA-DUR-MM
                  ,DUR_GG                 =
                :TGA-DUR-GG
                                       :IND-TGA-DUR-GG
                  ,PRE_CASO_MOR           =
                :TGA-PRE-CASO-MOR
                                       :IND-TGA-PRE-CASO-MOR
                  ,PC_INTR_RIAT           =
                :TGA-PC-INTR-RIAT
                                       :IND-TGA-PC-INTR-RIAT
                  ,IMP_BNS_ANTIC          =
                :TGA-IMP-BNS-ANTIC
                                       :IND-TGA-IMP-BNS-ANTIC
                  ,PRE_INI_NET            =
                :TGA-PRE-INI-NET
                                       :IND-TGA-PRE-INI-NET
                  ,PRE_PP_INI             =
                :TGA-PRE-PP-INI
                                       :IND-TGA-PRE-PP-INI
                  ,PRE_PP_ULT             =
                :TGA-PRE-PP-ULT
                                       :IND-TGA-PRE-PP-ULT
                  ,PRE_TARI_INI           =
                :TGA-PRE-TARI-INI
                                       :IND-TGA-PRE-TARI-INI
                  ,PRE_TARI_ULT           =
                :TGA-PRE-TARI-ULT
                                       :IND-TGA-PRE-TARI-ULT
                  ,PRE_INVRIO_INI         =
                :TGA-PRE-INVRIO-INI
                                       :IND-TGA-PRE-INVRIO-INI
                  ,PRE_INVRIO_ULT         =
                :TGA-PRE-INVRIO-ULT
                                       :IND-TGA-PRE-INVRIO-ULT
                  ,PRE_RIVTO              =
                :TGA-PRE-RIVTO
                                       :IND-TGA-PRE-RIVTO
                  ,IMP_SOPR_PROF          =
                :TGA-IMP-SOPR-PROF
                                       :IND-TGA-IMP-SOPR-PROF
                  ,IMP_SOPR_SAN           =
                :TGA-IMP-SOPR-SAN
                                       :IND-TGA-IMP-SOPR-SAN
                  ,IMP_SOPR_SPO           =
                :TGA-IMP-SOPR-SPO
                                       :IND-TGA-IMP-SOPR-SPO
                  ,IMP_SOPR_TEC           =
                :TGA-IMP-SOPR-TEC
                                       :IND-TGA-IMP-SOPR-TEC
                  ,IMP_ALT_SOPR           =
                :TGA-IMP-ALT-SOPR
                                       :IND-TGA-IMP-ALT-SOPR
                  ,PRE_STAB               =
                :TGA-PRE-STAB
                                       :IND-TGA-PRE-STAB
                  ,DT_EFF_STAB            =
           :TGA-DT-EFF-STAB-DB
                                       :IND-TGA-DT-EFF-STAB
                  ,TS_RIVAL_FIS           =
                :TGA-TS-RIVAL-FIS
                                       :IND-TGA-TS-RIVAL-FIS
                  ,TS_RIVAL_INDICIZ       =
                :TGA-TS-RIVAL-INDICIZ
                                       :IND-TGA-TS-RIVAL-INDICIZ
                  ,OLD_TS_TEC             =
                :TGA-OLD-TS-TEC
                                       :IND-TGA-OLD-TS-TEC
                  ,RAT_LRD                =
                :TGA-RAT-LRD
                                       :IND-TGA-RAT-LRD
                  ,PRE_LRD                =
                :TGA-PRE-LRD
                                       :IND-TGA-PRE-LRD
                  ,PRSTZ_INI              =
                :TGA-PRSTZ-INI
                                       :IND-TGA-PRSTZ-INI
                  ,PRSTZ_ULT              =
                :TGA-PRSTZ-ULT
                                       :IND-TGA-PRSTZ-ULT
                  ,CPT_IN_OPZ_RIVTO       =
                :TGA-CPT-IN-OPZ-RIVTO
                                       :IND-TGA-CPT-IN-OPZ-RIVTO
                  ,PRSTZ_INI_STAB         =
                :TGA-PRSTZ-INI-STAB
                                       :IND-TGA-PRSTZ-INI-STAB
                  ,CPT_RSH_MOR            =
                :TGA-CPT-RSH-MOR
                                       :IND-TGA-CPT-RSH-MOR
                  ,PRSTZ_RID_INI          =
                :TGA-PRSTZ-RID-INI
                                       :IND-TGA-PRSTZ-RID-INI
                  ,FL_CAR_CONT            =
                :TGA-FL-CAR-CONT
                                       :IND-TGA-FL-CAR-CONT
                  ,BNS_GIA_LIQTO          =
                :TGA-BNS-GIA-LIQTO
                                       :IND-TGA-BNS-GIA-LIQTO
                  ,IMP_BNS                =
                :TGA-IMP-BNS
                                       :IND-TGA-IMP-BNS
                  ,COD_DVS                =
                :TGA-COD-DVS
                  ,PRSTZ_INI_NEWFIS       =
                :TGA-PRSTZ-INI-NEWFIS
                                       :IND-TGA-PRSTZ-INI-NEWFIS
                  ,IMP_SCON               =
                :TGA-IMP-SCON
                                       :IND-TGA-IMP-SCON
                  ,ALQ_SCON               =
                :TGA-ALQ-SCON
                                       :IND-TGA-ALQ-SCON
                  ,IMP_CAR_ACQ            =
                :TGA-IMP-CAR-ACQ
                                       :IND-TGA-IMP-CAR-ACQ
                  ,IMP_CAR_INC            =
                :TGA-IMP-CAR-INC
                                       :IND-TGA-IMP-CAR-INC
                  ,IMP_CAR_GEST           =
                :TGA-IMP-CAR-GEST
                                       :IND-TGA-IMP-CAR-GEST
                  ,ETA_AA_1O_ASSTO        =
                :TGA-ETA-AA-1O-ASSTO
                                       :IND-TGA-ETA-AA-1O-ASSTO
                  ,ETA_MM_1O_ASSTO        =
                :TGA-ETA-MM-1O-ASSTO
                                       :IND-TGA-ETA-MM-1O-ASSTO
                  ,ETA_AA_2O_ASSTO        =
                :TGA-ETA-AA-2O-ASSTO
                                       :IND-TGA-ETA-AA-2O-ASSTO
                  ,ETA_MM_2O_ASSTO        =
                :TGA-ETA-MM-2O-ASSTO
                                       :IND-TGA-ETA-MM-2O-ASSTO
                  ,ETA_AA_3O_ASSTO        =
                :TGA-ETA-AA-3O-ASSTO
                                       :IND-TGA-ETA-AA-3O-ASSTO
                  ,ETA_MM_3O_ASSTO        =
                :TGA-ETA-MM-3O-ASSTO
                                       :IND-TGA-ETA-MM-3O-ASSTO
                  ,RENDTO_LRD             =
                :TGA-RENDTO-LRD
                                       :IND-TGA-RENDTO-LRD
                  ,PC_RETR                =
                :TGA-PC-RETR
                                       :IND-TGA-PC-RETR
                  ,RENDTO_RETR            =
                :TGA-RENDTO-RETR
                                       :IND-TGA-RENDTO-RETR
                  ,MIN_GARTO              =
                :TGA-MIN-GARTO
                                       :IND-TGA-MIN-GARTO
                  ,MIN_TRNUT              =
                :TGA-MIN-TRNUT
                                       :IND-TGA-MIN-TRNUT
                  ,PRE_ATT_DI_TRCH        =
                :TGA-PRE-ATT-DI-TRCH
                                       :IND-TGA-PRE-ATT-DI-TRCH
                  ,MATU_END2000           =
                :TGA-MATU-END2000
                                       :IND-TGA-MATU-END2000
                  ,ABB_TOT_INI            =
                :TGA-ABB-TOT-INI
                                       :IND-TGA-ABB-TOT-INI
                  ,ABB_TOT_ULT            =
                :TGA-ABB-TOT-ULT
                                       :IND-TGA-ABB-TOT-ULT
                  ,ABB_ANNU_ULT           =
                :TGA-ABB-ANNU-ULT
                                       :IND-TGA-ABB-ANNU-ULT
                  ,DUR_ABB                =
                :TGA-DUR-ABB
                                       :IND-TGA-DUR-ABB
                  ,TP_ADEG_ABB            =
                :TGA-TP-ADEG-ABB
                                       :IND-TGA-TP-ADEG-ABB
                  ,MOD_CALC               =
                :TGA-MOD-CALC
                                       :IND-TGA-MOD-CALC
                  ,IMP_AZ                 =
                :TGA-IMP-AZ
                                       :IND-TGA-IMP-AZ
                  ,IMP_ADER               =
                :TGA-IMP-ADER
                                       :IND-TGA-IMP-ADER
                  ,IMP_TFR                =
                :TGA-IMP-TFR
                                       :IND-TGA-IMP-TFR
                  ,IMP_VOLO               =
                :TGA-IMP-VOLO
                                       :IND-TGA-IMP-VOLO
                  ,VIS_END2000            =
                :TGA-VIS-END2000
                                       :IND-TGA-VIS-END2000
                  ,DT_VLDT_PROD           =
           :TGA-DT-VLDT-PROD-DB
                                       :IND-TGA-DT-VLDT-PROD
                  ,DT_INI_VAL_TAR         =
           :TGA-DT-INI-VAL-TAR-DB
                                       :IND-TGA-DT-INI-VAL-TAR
                  ,IMPB_VIS_END2000       =
                :TGA-IMPB-VIS-END2000
                                       :IND-TGA-IMPB-VIS-END2000
                  ,REN_INI_TS_TEC_0       =
                :TGA-REN-INI-TS-TEC-0
                                       :IND-TGA-REN-INI-TS-TEC-0
                  ,PC_RIP_PRE             =
                :TGA-PC-RIP-PRE
                                       :IND-TGA-PC-RIP-PRE
                  ,FL_IMPORTI_FORZ        =
                :TGA-FL-IMPORTI-FORZ
                                       :IND-TGA-FL-IMPORTI-FORZ
                  ,PRSTZ_INI_NFORZ        =
                :TGA-PRSTZ-INI-NFORZ
                                       :IND-TGA-PRSTZ-INI-NFORZ
                  ,VIS_END2000_NFORZ      =
                :TGA-VIS-END2000-NFORZ
                                       :IND-TGA-VIS-END2000-NFORZ
                  ,INTR_MORA              =
                :TGA-INTR-MORA
                                       :IND-TGA-INTR-MORA
                  ,MANFEE_ANTIC           =
                :TGA-MANFEE-ANTIC
                                       :IND-TGA-MANFEE-ANTIC
                  ,MANFEE_RICOR           =
                :TGA-MANFEE-RICOR
                                       :IND-TGA-MANFEE-RICOR
                  ,PRE_UNI_RIVTO          =
                :TGA-PRE-UNI-RIVTO
                                       :IND-TGA-PRE-UNI-RIVTO
                  ,PROV_1AA_ACQ           =
                :TGA-PROV-1AA-ACQ
                                       :IND-TGA-PROV-1AA-ACQ
                  ,PROV_2AA_ACQ           =
                :TGA-PROV-2AA-ACQ
                                       :IND-TGA-PROV-2AA-ACQ
                  ,PROV_RICOR             =
                :TGA-PROV-RICOR
                                       :IND-TGA-PROV-RICOR
                  ,PROV_INC               =
                :TGA-PROV-INC
                                       :IND-TGA-PROV-INC
                  ,ALQ_PROV_ACQ           =
                :TGA-ALQ-PROV-ACQ
                                       :IND-TGA-ALQ-PROV-ACQ
                  ,ALQ_PROV_INC           =
                :TGA-ALQ-PROV-INC
                                       :IND-TGA-ALQ-PROV-INC
                  ,ALQ_PROV_RICOR         =
                :TGA-ALQ-PROV-RICOR
                                       :IND-TGA-ALQ-PROV-RICOR
                  ,IMPB_PROV_ACQ          =
                :TGA-IMPB-PROV-ACQ
                                       :IND-TGA-IMPB-PROV-ACQ
                  ,IMPB_PROV_INC          =
                :TGA-IMPB-PROV-INC
                                       :IND-TGA-IMPB-PROV-INC
                  ,IMPB_PROV_RICOR        =
                :TGA-IMPB-PROV-RICOR
                                       :IND-TGA-IMPB-PROV-RICOR
                  ,FL_PROV_FORZ           =
                :TGA-FL-PROV-FORZ
                                       :IND-TGA-FL-PROV-FORZ
                  ,PRSTZ_AGG_INI          =
                :TGA-PRSTZ-AGG-INI
                                       :IND-TGA-PRSTZ-AGG-INI
                  ,INCR_PRE               =
                :TGA-INCR-PRE
                                       :IND-TGA-INCR-PRE
                  ,INCR_PRSTZ             =
                :TGA-INCR-PRSTZ
                                       :IND-TGA-INCR-PRSTZ
                  ,DT_ULT_ADEG_PRE_PR     =
           :TGA-DT-ULT-ADEG-PRE-PR-DB
                                       :IND-TGA-DT-ULT-ADEG-PRE-PR
                  ,PRSTZ_AGG_ULT          =
                :TGA-PRSTZ-AGG-ULT
                                       :IND-TGA-PRSTZ-AGG-ULT
                  ,TS_RIVAL_NET           =
                :TGA-TS-RIVAL-NET
                                       :IND-TGA-TS-RIVAL-NET
                  ,PRE_PATTUITO           =
                :TGA-PRE-PATTUITO
                                       :IND-TGA-PRE-PATTUITO
                  ,TP_RIVAL               =
                :TGA-TP-RIVAL
                                       :IND-TGA-TP-RIVAL
                  ,RIS_MAT                =
                :TGA-RIS-MAT
                                       :IND-TGA-RIS-MAT
                  ,CPT_MIN_SCAD           =
                :TGA-CPT-MIN-SCAD
                                       :IND-TGA-CPT-MIN-SCAD
                  ,COMMIS_GEST            =
                :TGA-COMMIS-GEST
                                       :IND-TGA-COMMIS-GEST
                  ,TP_MANFEE_APPL         =
                :TGA-TP-MANFEE-APPL
                                       :IND-TGA-TP-MANFEE-APPL
                  ,DS_RIGA                =
                :TGA-DS-RIGA
                  ,DS_OPER_SQL            =
                :TGA-DS-OPER-SQL
                  ,DS_VER                 =
                :TGA-DS-VER
                  ,DS_TS_INI_CPTZ         =
                :TGA-DS-TS-INI-CPTZ
                  ,DS_TS_END_CPTZ         =
                :TGA-DS-TS-END-CPTZ
                  ,DS_UTENTE              =
                :TGA-DS-UTENTE
                  ,DS_STATO_ELAB          =
                :TGA-DS-STATO-ELAB
                  ,PC_COMMIS_GEST         =
                :TGA-PC-COMMIS-GEST
                                       :IND-TGA-PC-COMMIS-GEST
                  ,NUM_GG_RIVAL           =
                :TGA-NUM-GG-RIVAL
                                       :IND-TGA-NUM-GG-RIVAL
                  ,IMP_TRASFE             =
                :TGA-IMP-TRASFE
                                       :IND-TGA-IMP-TRASFE
                  ,IMP_TFR_STRC           =
                :TGA-IMP-TFR-STRC
                                       :IND-TGA-IMP-TFR-STRC
                  ,ACQ_EXP                =
                :TGA-ACQ-EXP
                                       :IND-TGA-ACQ-EXP
                  ,REMUN_ASS              =
                :TGA-REMUN-ASS
                                       :IND-TGA-REMUN-ASS
                  ,COMMIS_INTER           =
                :TGA-COMMIS-INTER
                                       :IND-TGA-COMMIS-INTER
                  ,ALQ_REMUN_ASS          =
                :TGA-ALQ-REMUN-ASS
                                       :IND-TGA-ALQ-REMUN-ASS
                  ,ALQ_COMMIS_INTER       =
                :TGA-ALQ-COMMIS-INTER
                                       :IND-TGA-ALQ-COMMIS-INTER
                  ,IMPB_REMUN_ASS         =
                :TGA-IMPB-REMUN-ASS
                                       :IND-TGA-IMPB-REMUN-ASS
                  ,IMPB_COMMIS_INTER      =
                :TGA-IMPB-COMMIS-INTER
                                       :IND-TGA-IMPB-COMMIS-INTER
                  ,COS_RUN_ASSVA          =
                :TGA-COS-RUN-ASSVA
                                       :IND-TGA-COS-RUN-ASSVA
                  ,COS_RUN_ASSVA_IDC      =
                :TGA-COS-RUN-ASSVA-IDC
                                       :IND-TGA-COS-RUN-ASSVA-IDC
                WHERE     DS_RIGA = :TGA-DS-RIGA
                   AND ID_MOVI_CHIU IS NULL
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A330-EX.
           EXIT.

       A360-OPEN-CURSOR-ID-EFF.

           PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.

           EXEC SQL
                OPEN C-ID-UPD-EFF-TGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A360-EX.
           EXIT.

       A370-CLOSE-CURSOR-ID-EFF.
           EXEC SQL
                CLOSE C-ID-UPD-EFF-TGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A370-EX.
           EXIT.

       A380-FETCH-FIRST-ID-EFF.
           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
           END-IF.
       A380-EX.
           EXIT.

       A390-FETCH-NEXT-ID-EFF.
           EXEC SQL
                FETCH C-ID-UPD-EFF-TGA
           INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A390-EX.
           EXIT.
      *----
      *----  gestione IDP Effetto
      *----
       A405-DECLARE-CURSOR-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-EFF-TGA CURSOR FOR
              SELECT
                     ID_TRCH_DI_GAR
                    ,ID_GAR
                    ,ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_SCAD
                    ,IB_OGG
                    ,TP_RGM_FISC
                    ,DT_EMIS
                    ,TP_TRCH
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,PRE_CASO_MOR
                    ,PC_INTR_RIAT
                    ,IMP_BNS_ANTIC
                    ,PRE_INI_NET
                    ,PRE_PP_INI
                    ,PRE_PP_ULT
                    ,PRE_TARI_INI
                    ,PRE_TARI_ULT
                    ,PRE_INVRIO_INI
                    ,PRE_INVRIO_ULT
                    ,PRE_RIVTO
                    ,IMP_SOPR_PROF
                    ,IMP_SOPR_SAN
                    ,IMP_SOPR_SPO
                    ,IMP_SOPR_TEC
                    ,IMP_ALT_SOPR
                    ,PRE_STAB
                    ,DT_EFF_STAB
                    ,TS_RIVAL_FIS
                    ,TS_RIVAL_INDICIZ
                    ,OLD_TS_TEC
                    ,RAT_LRD
                    ,PRE_LRD
                    ,PRSTZ_INI
                    ,PRSTZ_ULT
                    ,CPT_IN_OPZ_RIVTO
                    ,PRSTZ_INI_STAB
                    ,CPT_RSH_MOR
                    ,PRSTZ_RID_INI
                    ,FL_CAR_CONT
                    ,BNS_GIA_LIQTO
                    ,IMP_BNS
                    ,COD_DVS
                    ,PRSTZ_INI_NEWFIS
                    ,IMP_SCON
                    ,ALQ_SCON
                    ,IMP_CAR_ACQ
                    ,IMP_CAR_INC
                    ,IMP_CAR_GEST
                    ,ETA_AA_1O_ASSTO
                    ,ETA_MM_1O_ASSTO
                    ,ETA_AA_2O_ASSTO
                    ,ETA_MM_2O_ASSTO
                    ,ETA_AA_3O_ASSTO
                    ,ETA_MM_3O_ASSTO
                    ,RENDTO_LRD
                    ,PC_RETR
                    ,RENDTO_RETR
                    ,MIN_GARTO
                    ,MIN_TRNUT
                    ,PRE_ATT_DI_TRCH
                    ,MATU_END2000
                    ,ABB_TOT_INI
                    ,ABB_TOT_ULT
                    ,ABB_ANNU_ULT
                    ,DUR_ABB
                    ,TP_ADEG_ABB
                    ,MOD_CALC
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,VIS_END2000
                    ,DT_VLDT_PROD
                    ,DT_INI_VAL_TAR
                    ,IMPB_VIS_END2000
                    ,REN_INI_TS_TEC_0
                    ,PC_RIP_PRE
                    ,FL_IMPORTI_FORZ
                    ,PRSTZ_INI_NFORZ
                    ,VIS_END2000_NFORZ
                    ,INTR_MORA
                    ,MANFEE_ANTIC
                    ,MANFEE_RICOR
                    ,PRE_UNI_RIVTO
                    ,PROV_1AA_ACQ
                    ,PROV_2AA_ACQ
                    ,PROV_RICOR
                    ,PROV_INC
                    ,ALQ_PROV_ACQ
                    ,ALQ_PROV_INC
                    ,ALQ_PROV_RICOR
                    ,IMPB_PROV_ACQ
                    ,IMPB_PROV_INC
                    ,IMPB_PROV_RICOR
                    ,FL_PROV_FORZ
                    ,PRSTZ_AGG_INI
                    ,INCR_PRE
                    ,INCR_PRSTZ
                    ,DT_ULT_ADEG_PRE_PR
                    ,PRSTZ_AGG_ULT
                    ,TS_RIVAL_NET
                    ,PRE_PATTUITO
                    ,TP_RIVAL
                    ,RIS_MAT
                    ,CPT_MIN_SCAD
                    ,COMMIS_GEST
                    ,TP_MANFEE_APPL
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,PC_COMMIS_GEST
                    ,NUM_GG_RIVAL
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,ACQ_EXP
                    ,REMUN_ASS
                    ,COMMIS_INTER
                    ,ALQ_REMUN_ASS
                    ,ALQ_COMMIS_INTER
                    ,IMPB_REMUN_ASS
                    ,IMPB_COMMIS_INTER
                    ,COS_RUN_ASSVA
                    ,COS_RUN_ASSVA_IDC
              FROM TRCH_DI_GAR
              WHERE     ID_POLI = :TGA-ID-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_TRCH_DI_GAR ASC

           END-EXEC.

       A405-EX.
           EXIT.

       A410-SELECT-IDP-EFF.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TRCH_DI_GAR
                ,ID_GAR
                ,ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,IB_OGG
                ,TP_RGM_FISC
                ,DT_EMIS
                ,TP_TRCH
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,PRE_CASO_MOR
                ,PC_INTR_RIAT
                ,IMP_BNS_ANTIC
                ,PRE_INI_NET
                ,PRE_PP_INI
                ,PRE_PP_ULT
                ,PRE_TARI_INI
                ,PRE_TARI_ULT
                ,PRE_INVRIO_INI
                ,PRE_INVRIO_ULT
                ,PRE_RIVTO
                ,IMP_SOPR_PROF
                ,IMP_SOPR_SAN
                ,IMP_SOPR_SPO
                ,IMP_SOPR_TEC
                ,IMP_ALT_SOPR
                ,PRE_STAB
                ,DT_EFF_STAB
                ,TS_RIVAL_FIS
                ,TS_RIVAL_INDICIZ
                ,OLD_TS_TEC
                ,RAT_LRD
                ,PRE_LRD
                ,PRSTZ_INI
                ,PRSTZ_ULT
                ,CPT_IN_OPZ_RIVTO
                ,PRSTZ_INI_STAB
                ,CPT_RSH_MOR
                ,PRSTZ_RID_INI
                ,FL_CAR_CONT
                ,BNS_GIA_LIQTO
                ,IMP_BNS
                ,COD_DVS
                ,PRSTZ_INI_NEWFIS
                ,IMP_SCON
                ,ALQ_SCON
                ,IMP_CAR_ACQ
                ,IMP_CAR_INC
                ,IMP_CAR_GEST
                ,ETA_AA_1O_ASSTO
                ,ETA_MM_1O_ASSTO
                ,ETA_AA_2O_ASSTO
                ,ETA_MM_2O_ASSTO
                ,ETA_AA_3O_ASSTO
                ,ETA_MM_3O_ASSTO
                ,RENDTO_LRD
                ,PC_RETR
                ,RENDTO_RETR
                ,MIN_GARTO
                ,MIN_TRNUT
                ,PRE_ATT_DI_TRCH
                ,MATU_END2000
                ,ABB_TOT_INI
                ,ABB_TOT_ULT
                ,ABB_ANNU_ULT
                ,DUR_ABB
                ,TP_ADEG_ABB
                ,MOD_CALC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,VIS_END2000
                ,DT_VLDT_PROD
                ,DT_INI_VAL_TAR
                ,IMPB_VIS_END2000
                ,REN_INI_TS_TEC_0
                ,PC_RIP_PRE
                ,FL_IMPORTI_FORZ
                ,PRSTZ_INI_NFORZ
                ,VIS_END2000_NFORZ
                ,INTR_MORA
                ,MANFEE_ANTIC
                ,MANFEE_RICOR
                ,PRE_UNI_RIVTO
                ,PROV_1AA_ACQ
                ,PROV_2AA_ACQ
                ,PROV_RICOR
                ,PROV_INC
                ,ALQ_PROV_ACQ
                ,ALQ_PROV_INC
                ,ALQ_PROV_RICOR
                ,IMPB_PROV_ACQ
                ,IMPB_PROV_INC
                ,IMPB_PROV_RICOR
                ,FL_PROV_FORZ
                ,PRSTZ_AGG_INI
                ,INCR_PRE
                ,INCR_PRSTZ
                ,DT_ULT_ADEG_PRE_PR
                ,PRSTZ_AGG_ULT
                ,TS_RIVAL_NET
                ,PRE_PATTUITO
                ,TP_RIVAL
                ,RIS_MAT
                ,CPT_MIN_SCAD
                ,COMMIS_GEST
                ,TP_MANFEE_APPL
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,PC_COMMIS_GEST
                ,NUM_GG_RIVAL
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,ACQ_EXP
                ,REMUN_ASS
                ,COMMIS_INTER
                ,ALQ_REMUN_ASS
                ,ALQ_COMMIS_INTER
                ,IMPB_REMUN_ASS
                ,IMPB_COMMIS_INTER
                ,COS_RUN_ASSVA
                       ,COS_RUN_ASSVA_IDC
             INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
             FROM TRCH_DI_GAR
             WHERE     ID_POLI = :TGA-ID-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A410-EX.
           EXIT.

       A460-OPEN-CURSOR-IDP-EFF.

           PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.

           EXEC SQL
                OPEN C-IDP-EFF-TGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A460-EX.
           EXIT.

       A470-CLOSE-CURSOR-IDP-EFF.
           EXEC SQL
                CLOSE C-IDP-EFF-TGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A470-EX.
           EXIT.

       A480-FETCH-FIRST-IDP-EFF.
           PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
           END-IF.
       A480-EX.
           EXIT.

       A490-FETCH-NEXT-IDP-EFF.
           EXEC SQL
                FETCH C-IDP-EFF-TGA
           INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A490-EX.
           EXIT.
      *----
      *----  gestione IBO Effetto e non
      *----
       A505-DECLARE-CURSOR-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IBO-EFF-TGA CURSOR FOR
              SELECT
                     ID_TRCH_DI_GAR
                    ,ID_GAR
                    ,ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_SCAD
                    ,IB_OGG
                    ,TP_RGM_FISC
                    ,DT_EMIS
                    ,TP_TRCH
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,PRE_CASO_MOR
                    ,PC_INTR_RIAT
                    ,IMP_BNS_ANTIC
                    ,PRE_INI_NET
                    ,PRE_PP_INI
                    ,PRE_PP_ULT
                    ,PRE_TARI_INI
                    ,PRE_TARI_ULT
                    ,PRE_INVRIO_INI
                    ,PRE_INVRIO_ULT
                    ,PRE_RIVTO
                    ,IMP_SOPR_PROF
                    ,IMP_SOPR_SAN
                    ,IMP_SOPR_SPO
                    ,IMP_SOPR_TEC
                    ,IMP_ALT_SOPR
                    ,PRE_STAB
                    ,DT_EFF_STAB
                    ,TS_RIVAL_FIS
                    ,TS_RIVAL_INDICIZ
                    ,OLD_TS_TEC
                    ,RAT_LRD
                    ,PRE_LRD
                    ,PRSTZ_INI
                    ,PRSTZ_ULT
                    ,CPT_IN_OPZ_RIVTO
                    ,PRSTZ_INI_STAB
                    ,CPT_RSH_MOR
                    ,PRSTZ_RID_INI
                    ,FL_CAR_CONT
                    ,BNS_GIA_LIQTO
                    ,IMP_BNS
                    ,COD_DVS
                    ,PRSTZ_INI_NEWFIS
                    ,IMP_SCON
                    ,ALQ_SCON
                    ,IMP_CAR_ACQ
                    ,IMP_CAR_INC
                    ,IMP_CAR_GEST
                    ,ETA_AA_1O_ASSTO
                    ,ETA_MM_1O_ASSTO
                    ,ETA_AA_2O_ASSTO
                    ,ETA_MM_2O_ASSTO
                    ,ETA_AA_3O_ASSTO
                    ,ETA_MM_3O_ASSTO
                    ,RENDTO_LRD
                    ,PC_RETR
                    ,RENDTO_RETR
                    ,MIN_GARTO
                    ,MIN_TRNUT
                    ,PRE_ATT_DI_TRCH
                    ,MATU_END2000
                    ,ABB_TOT_INI
                    ,ABB_TOT_ULT
                    ,ABB_ANNU_ULT
                    ,DUR_ABB
                    ,TP_ADEG_ABB
                    ,MOD_CALC
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,VIS_END2000
                    ,DT_VLDT_PROD
                    ,DT_INI_VAL_TAR
                    ,IMPB_VIS_END2000
                    ,REN_INI_TS_TEC_0
                    ,PC_RIP_PRE
                    ,FL_IMPORTI_FORZ
                    ,PRSTZ_INI_NFORZ
                    ,VIS_END2000_NFORZ
                    ,INTR_MORA
                    ,MANFEE_ANTIC
                    ,MANFEE_RICOR
                    ,PRE_UNI_RIVTO
                    ,PROV_1AA_ACQ
                    ,PROV_2AA_ACQ
                    ,PROV_RICOR
                    ,PROV_INC
                    ,ALQ_PROV_ACQ
                    ,ALQ_PROV_INC
                    ,ALQ_PROV_RICOR
                    ,IMPB_PROV_ACQ
                    ,IMPB_PROV_INC
                    ,IMPB_PROV_RICOR
                    ,FL_PROV_FORZ
                    ,PRSTZ_AGG_INI
                    ,INCR_PRE
                    ,INCR_PRSTZ
                    ,DT_ULT_ADEG_PRE_PR
                    ,PRSTZ_AGG_ULT
                    ,TS_RIVAL_NET
                    ,PRE_PATTUITO
                    ,TP_RIVAL
                    ,RIS_MAT
                    ,CPT_MIN_SCAD
                    ,COMMIS_GEST
                    ,TP_MANFEE_APPL
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,PC_COMMIS_GEST
                    ,NUM_GG_RIVAL
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,ACQ_EXP
                    ,REMUN_ASS
                    ,COMMIS_INTER
                    ,ALQ_REMUN_ASS
                    ,ALQ_COMMIS_INTER
                    ,IMPB_REMUN_ASS
                    ,IMPB_COMMIS_INTER
                    ,COS_RUN_ASSVA
                    ,COS_RUN_ASSVA_IDC
              FROM TRCH_DI_GAR
              WHERE     IB_OGG = :TGA-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL
              ORDER BY ID_TRCH_DI_GAR ASC

           END-EXEC.

       A505-EX.
           EXIT.

       A510-SELECT-IBO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TRCH_DI_GAR
                ,ID_GAR
                ,ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,IB_OGG
                ,TP_RGM_FISC
                ,DT_EMIS
                ,TP_TRCH
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,PRE_CASO_MOR
                ,PC_INTR_RIAT
                ,IMP_BNS_ANTIC
                ,PRE_INI_NET
                ,PRE_PP_INI
                ,PRE_PP_ULT
                ,PRE_TARI_INI
                ,PRE_TARI_ULT
                ,PRE_INVRIO_INI
                ,PRE_INVRIO_ULT
                ,PRE_RIVTO
                ,IMP_SOPR_PROF
                ,IMP_SOPR_SAN
                ,IMP_SOPR_SPO
                ,IMP_SOPR_TEC
                ,IMP_ALT_SOPR
                ,PRE_STAB
                ,DT_EFF_STAB
                ,TS_RIVAL_FIS
                ,TS_RIVAL_INDICIZ
                ,OLD_TS_TEC
                ,RAT_LRD
                ,PRE_LRD
                ,PRSTZ_INI
                ,PRSTZ_ULT
                ,CPT_IN_OPZ_RIVTO
                ,PRSTZ_INI_STAB
                ,CPT_RSH_MOR
                ,PRSTZ_RID_INI
                ,FL_CAR_CONT
                ,BNS_GIA_LIQTO
                ,IMP_BNS
                ,COD_DVS
                ,PRSTZ_INI_NEWFIS
                ,IMP_SCON
                ,ALQ_SCON
                ,IMP_CAR_ACQ
                ,IMP_CAR_INC
                ,IMP_CAR_GEST
                ,ETA_AA_1O_ASSTO
                ,ETA_MM_1O_ASSTO
                ,ETA_AA_2O_ASSTO
                ,ETA_MM_2O_ASSTO
                ,ETA_AA_3O_ASSTO
                ,ETA_MM_3O_ASSTO
                ,RENDTO_LRD
                ,PC_RETR
                ,RENDTO_RETR
                ,MIN_GARTO
                ,MIN_TRNUT
                ,PRE_ATT_DI_TRCH
                ,MATU_END2000
                ,ABB_TOT_INI
                ,ABB_TOT_ULT
                ,ABB_ANNU_ULT
                ,DUR_ABB
                ,TP_ADEG_ABB
                ,MOD_CALC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,VIS_END2000
                ,DT_VLDT_PROD
                ,DT_INI_VAL_TAR
                ,IMPB_VIS_END2000
                ,REN_INI_TS_TEC_0
                ,PC_RIP_PRE
                ,FL_IMPORTI_FORZ
                ,PRSTZ_INI_NFORZ
                ,VIS_END2000_NFORZ
                ,INTR_MORA
                ,MANFEE_ANTIC
                ,MANFEE_RICOR
                ,PRE_UNI_RIVTO
                ,PROV_1AA_ACQ
                ,PROV_2AA_ACQ
                ,PROV_RICOR
                ,PROV_INC
                ,ALQ_PROV_ACQ
                ,ALQ_PROV_INC
                ,ALQ_PROV_RICOR
                ,IMPB_PROV_ACQ
                ,IMPB_PROV_INC
                ,IMPB_PROV_RICOR
                ,FL_PROV_FORZ
                ,PRSTZ_AGG_INI
                ,INCR_PRE
                ,INCR_PRSTZ
                ,DT_ULT_ADEG_PRE_PR
                ,PRSTZ_AGG_ULT
                ,TS_RIVAL_NET
                ,PRE_PATTUITO
                ,TP_RIVAL
                ,RIS_MAT
                ,CPT_MIN_SCAD
                ,COMMIS_GEST
                ,TP_MANFEE_APPL
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,PC_COMMIS_GEST
                ,NUM_GG_RIVAL
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,ACQ_EXP
                ,REMUN_ASS
                ,COMMIS_INTER
                ,ALQ_REMUN_ASS
                ,ALQ_COMMIS_INTER
                ,IMPB_REMUN_ASS
                ,IMPB_COMMIS_INTER
                ,COS_RUN_ASSVA
                ,COS_RUN_ASSVA_IDC
             INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
             FROM TRCH_DI_GAR
             WHERE     IB_OGG = :TGA-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND ID_MOVI_CHIU IS NULL

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       A510-EX.
           EXIT.

       A560-OPEN-CURSOR-IBO.

           PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.

           EXEC SQL
                OPEN C-IBO-EFF-TGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A560-EX.
           EXIT.

       A570-CLOSE-CURSOR-IBO.
           EXEC SQL
                CLOSE C-IBO-EFF-TGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       A570-EX.
           EXIT.

       A580-FETCH-FIRST-IBO.
           PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
           END-IF.
       A580-EX.
           EXIT.

       A590-FETCH-NEXT-IBO.
           EXEC SQL
                FETCH C-IBO-EFF-TGA
           INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM A570-CLOSE-CURSOR-IBO     THRU A570-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       A590-EX.
           EXIT.
      *----
      *----  gestione IBS Effetto e non
      *----


       A605-DECLARE-CURSOR-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A605-EX.
           EXIT.


       A610-SELECT-IBS.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A610-EX.
           EXIT.

       A660-OPEN-CURSOR-IBS.

           PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A660-EX.
           EXIT.

       A670-CLOSE-CURSOR-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A670-EX.
           EXIT.

       A680-FETCH-FIRST-IBS.
           PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
           END-IF.
       A680-EX.
           EXIT.


       A690-FETCH-NEXT-IBS.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A690-EX.
           EXIT.
      *----
      *----  gestione IDO Effetto e non
      *----
       A705-DECLARE-CURSOR-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A705-EX.
           EXIT.

       A710-SELECT-IDO.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A710-EX.
           EXIT.

       A760-OPEN-CURSOR-IDO.

           PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       A760-EX.
           EXIT.

       A770-CLOSE-CURSOR-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A770-EX.
           EXIT.

       A780-FETCH-FIRST-IDO.
           PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
           END-IF.
       A780-EX.
           EXIT.

       A790-FETCH-NEXT-IDO.
           SET IDSV0003-INVALID-OPER TO TRUE.
       A790-EX.
           EXIT.
      *----
      *----  gestione ID Competenza
      *----
       B310-SELECT-ID-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TRCH_DI_GAR
                ,ID_GAR
                ,ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,IB_OGG
                ,TP_RGM_FISC
                ,DT_EMIS
                ,TP_TRCH
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,PRE_CASO_MOR
                ,PC_INTR_RIAT
                ,IMP_BNS_ANTIC
                ,PRE_INI_NET
                ,PRE_PP_INI
                ,PRE_PP_ULT
                ,PRE_TARI_INI
                ,PRE_TARI_ULT
                ,PRE_INVRIO_INI
                ,PRE_INVRIO_ULT
                ,PRE_RIVTO
                ,IMP_SOPR_PROF
                ,IMP_SOPR_SAN
                ,IMP_SOPR_SPO
                ,IMP_SOPR_TEC
                ,IMP_ALT_SOPR
                ,PRE_STAB
                ,DT_EFF_STAB
                ,TS_RIVAL_FIS
                ,TS_RIVAL_INDICIZ
                ,OLD_TS_TEC
                ,RAT_LRD
                ,PRE_LRD
                ,PRSTZ_INI
                ,PRSTZ_ULT
                ,CPT_IN_OPZ_RIVTO
                ,PRSTZ_INI_STAB
                ,CPT_RSH_MOR
                ,PRSTZ_RID_INI
                ,FL_CAR_CONT
                ,BNS_GIA_LIQTO
                ,IMP_BNS
                ,COD_DVS
                ,PRSTZ_INI_NEWFIS
                ,IMP_SCON
                ,ALQ_SCON
                ,IMP_CAR_ACQ
                ,IMP_CAR_INC
                ,IMP_CAR_GEST
                ,ETA_AA_1O_ASSTO
                ,ETA_MM_1O_ASSTO
                ,ETA_AA_2O_ASSTO
                ,ETA_MM_2O_ASSTO
                ,ETA_AA_3O_ASSTO
                ,ETA_MM_3O_ASSTO
                ,RENDTO_LRD
                ,PC_RETR
                ,RENDTO_RETR
                ,MIN_GARTO
                ,MIN_TRNUT
                ,PRE_ATT_DI_TRCH
                ,MATU_END2000
                ,ABB_TOT_INI
                ,ABB_TOT_ULT
                ,ABB_ANNU_ULT
                ,DUR_ABB
                ,TP_ADEG_ABB
                ,MOD_CALC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,VIS_END2000
                ,DT_VLDT_PROD
                ,DT_INI_VAL_TAR
                ,IMPB_VIS_END2000
                ,REN_INI_TS_TEC_0
                ,PC_RIP_PRE
                ,FL_IMPORTI_FORZ
                ,PRSTZ_INI_NFORZ
                ,VIS_END2000_NFORZ
                ,INTR_MORA
                ,MANFEE_ANTIC
                ,MANFEE_RICOR
                ,PRE_UNI_RIVTO
                ,PROV_1AA_ACQ
                ,PROV_2AA_ACQ
                ,PROV_RICOR
                ,PROV_INC
                ,ALQ_PROV_ACQ
                ,ALQ_PROV_INC
                ,ALQ_PROV_RICOR
                ,IMPB_PROV_ACQ
                ,IMPB_PROV_INC
                ,IMPB_PROV_RICOR
                ,FL_PROV_FORZ
                ,PRSTZ_AGG_INI
                ,INCR_PRE
                ,INCR_PRSTZ
                ,DT_ULT_ADEG_PRE_PR
                ,PRSTZ_AGG_ULT
                ,TS_RIVAL_NET
                ,PRE_PATTUITO
                ,TP_RIVAL
                ,RIS_MAT
                ,CPT_MIN_SCAD
                ,COMMIS_GEST
                ,TP_MANFEE_APPL
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,PC_COMMIS_GEST
                ,NUM_GG_RIVAL
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,ACQ_EXP
                ,REMUN_ASS
                ,COMMIS_INTER
                ,ALQ_REMUN_ASS
                ,ALQ_COMMIS_INTER
                ,IMPB_REMUN_ASS
                ,IMPB_COMMIS_INTER
                ,COS_RUN_ASSVA
                ,COS_RUN_ASSVA_IDC
             INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
             FROM TRCH_DI_GAR
             WHERE     ID_TRCH_DI_GAR = :TGA-ID-TRCH-DI-GAR
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B310-EX.
           EXIT.
      *----
      *----  gestione IDP Competenza
      *----
       B405-DECLARE-CURSOR-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IDP-CPZ-TGA CURSOR FOR
              SELECT
                     ID_TRCH_DI_GAR
                    ,ID_GAR
                    ,ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_SCAD
                    ,IB_OGG
                    ,TP_RGM_FISC
                    ,DT_EMIS
                    ,TP_TRCH
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,PRE_CASO_MOR
                    ,PC_INTR_RIAT
                    ,IMP_BNS_ANTIC
                    ,PRE_INI_NET
                    ,PRE_PP_INI
                    ,PRE_PP_ULT
                    ,PRE_TARI_INI
                    ,PRE_TARI_ULT
                    ,PRE_INVRIO_INI
                    ,PRE_INVRIO_ULT
                    ,PRE_RIVTO
                    ,IMP_SOPR_PROF
                    ,IMP_SOPR_SAN
                    ,IMP_SOPR_SPO
                    ,IMP_SOPR_TEC
                    ,IMP_ALT_SOPR
                    ,PRE_STAB
                    ,DT_EFF_STAB
                    ,TS_RIVAL_FIS
                    ,TS_RIVAL_INDICIZ
                    ,OLD_TS_TEC
                    ,RAT_LRD
                    ,PRE_LRD
                    ,PRSTZ_INI
                    ,PRSTZ_ULT
                    ,CPT_IN_OPZ_RIVTO
                    ,PRSTZ_INI_STAB
                    ,CPT_RSH_MOR
                    ,PRSTZ_RID_INI
                    ,FL_CAR_CONT
                    ,BNS_GIA_LIQTO
                    ,IMP_BNS
                    ,COD_DVS
                    ,PRSTZ_INI_NEWFIS
                    ,IMP_SCON
                    ,ALQ_SCON
                    ,IMP_CAR_ACQ
                    ,IMP_CAR_INC
                    ,IMP_CAR_GEST
                    ,ETA_AA_1O_ASSTO
                    ,ETA_MM_1O_ASSTO
                    ,ETA_AA_2O_ASSTO
                    ,ETA_MM_2O_ASSTO
                    ,ETA_AA_3O_ASSTO
                    ,ETA_MM_3O_ASSTO
                    ,RENDTO_LRD
                    ,PC_RETR
                    ,RENDTO_RETR
                    ,MIN_GARTO
                    ,MIN_TRNUT
                    ,PRE_ATT_DI_TRCH
                    ,MATU_END2000
                    ,ABB_TOT_INI
                    ,ABB_TOT_ULT
                    ,ABB_ANNU_ULT
                    ,DUR_ABB
                    ,TP_ADEG_ABB
                    ,MOD_CALC
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,VIS_END2000
                    ,DT_VLDT_PROD
                    ,DT_INI_VAL_TAR
                    ,IMPB_VIS_END2000
                    ,REN_INI_TS_TEC_0
                    ,PC_RIP_PRE
                    ,FL_IMPORTI_FORZ
                    ,PRSTZ_INI_NFORZ
                    ,VIS_END2000_NFORZ
                    ,INTR_MORA
                    ,MANFEE_ANTIC
                    ,MANFEE_RICOR
                    ,PRE_UNI_RIVTO
                    ,PROV_1AA_ACQ
                    ,PROV_2AA_ACQ
                    ,PROV_RICOR
                    ,PROV_INC
                    ,ALQ_PROV_ACQ
                    ,ALQ_PROV_INC
                    ,ALQ_PROV_RICOR
                    ,IMPB_PROV_ACQ
                    ,IMPB_PROV_INC
                    ,IMPB_PROV_RICOR
                    ,FL_PROV_FORZ
                    ,PRSTZ_AGG_INI
                    ,INCR_PRE
                    ,INCR_PRSTZ
                    ,DT_ULT_ADEG_PRE_PR
                    ,PRSTZ_AGG_ULT
                    ,TS_RIVAL_NET
                    ,PRE_PATTUITO
                    ,TP_RIVAL
                    ,RIS_MAT
                    ,CPT_MIN_SCAD
                    ,COMMIS_GEST
                    ,TP_MANFEE_APPL
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,PC_COMMIS_GEST
                    ,NUM_GG_RIVAL
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,ACQ_EXP
                    ,REMUN_ASS
                    ,COMMIS_INTER
                    ,ALQ_REMUN_ASS
                    ,ALQ_COMMIS_INTER
                    ,IMPB_REMUN_ASS
                    ,IMPB_COMMIS_INTER
                    ,COS_RUN_ASSVA
                       ,COS_RUN_ASSVA_IDC
              FROM TRCH_DI_GAR
              WHERE     ID_POLI = :TGA-ID-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_TRCH_DI_GAR ASC

           END-EXEC.

       B405-EX.
           EXIT.

       B410-SELECT-IDP-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TRCH_DI_GAR
                ,ID_GAR
                ,ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,IB_OGG
                ,TP_RGM_FISC
                ,DT_EMIS
                ,TP_TRCH
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,PRE_CASO_MOR
                ,PC_INTR_RIAT
                ,IMP_BNS_ANTIC
                ,PRE_INI_NET
                ,PRE_PP_INI
                ,PRE_PP_ULT
                ,PRE_TARI_INI
                ,PRE_TARI_ULT
                ,PRE_INVRIO_INI
                ,PRE_INVRIO_ULT
                ,PRE_RIVTO
                ,IMP_SOPR_PROF
                ,IMP_SOPR_SAN
                ,IMP_SOPR_SPO
                ,IMP_SOPR_TEC
                ,IMP_ALT_SOPR
                ,PRE_STAB
                ,DT_EFF_STAB
                ,TS_RIVAL_FIS
                ,TS_RIVAL_INDICIZ
                ,OLD_TS_TEC
                ,RAT_LRD
                ,PRE_LRD
                ,PRSTZ_INI
                ,PRSTZ_ULT
                ,CPT_IN_OPZ_RIVTO
                ,PRSTZ_INI_STAB
                ,CPT_RSH_MOR
                ,PRSTZ_RID_INI
                ,FL_CAR_CONT
                ,BNS_GIA_LIQTO
                ,IMP_BNS
                ,COD_DVS
                ,PRSTZ_INI_NEWFIS
                ,IMP_SCON
                ,ALQ_SCON
                ,IMP_CAR_ACQ
                ,IMP_CAR_INC
                ,IMP_CAR_GEST
                ,ETA_AA_1O_ASSTO
                ,ETA_MM_1O_ASSTO
                ,ETA_AA_2O_ASSTO
                ,ETA_MM_2O_ASSTO
                ,ETA_AA_3O_ASSTO
                ,ETA_MM_3O_ASSTO
                ,RENDTO_LRD
                ,PC_RETR
                ,RENDTO_RETR
                ,MIN_GARTO
                ,MIN_TRNUT
                ,PRE_ATT_DI_TRCH
                ,MATU_END2000
                ,ABB_TOT_INI
                ,ABB_TOT_ULT
                ,ABB_ANNU_ULT
                ,DUR_ABB
                ,TP_ADEG_ABB
                ,MOD_CALC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,VIS_END2000
                ,DT_VLDT_PROD
                ,DT_INI_VAL_TAR
                ,IMPB_VIS_END2000
                ,REN_INI_TS_TEC_0
                ,PC_RIP_PRE
                ,FL_IMPORTI_FORZ
                ,PRSTZ_INI_NFORZ
                ,VIS_END2000_NFORZ
                ,INTR_MORA
                ,MANFEE_ANTIC
                ,MANFEE_RICOR
                ,PRE_UNI_RIVTO
                ,PROV_1AA_ACQ
                ,PROV_2AA_ACQ
                ,PROV_RICOR
                ,PROV_INC
                ,ALQ_PROV_ACQ
                ,ALQ_PROV_INC
                ,ALQ_PROV_RICOR
                ,IMPB_PROV_ACQ
                ,IMPB_PROV_INC
                ,IMPB_PROV_RICOR
                ,FL_PROV_FORZ
                ,PRSTZ_AGG_INI
                ,INCR_PRE
                ,INCR_PRSTZ
                ,DT_ULT_ADEG_PRE_PR
                ,PRSTZ_AGG_ULT
                ,TS_RIVAL_NET
                ,PRE_PATTUITO
                ,TP_RIVAL
                ,RIS_MAT
                ,CPT_MIN_SCAD
                ,COMMIS_GEST
                ,TP_MANFEE_APPL
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,PC_COMMIS_GEST
                ,NUM_GG_RIVAL
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,ACQ_EXP
                ,REMUN_ASS
                ,COMMIS_INTER
                ,ALQ_REMUN_ASS
                ,ALQ_COMMIS_INTER
                ,IMPB_REMUN_ASS
                ,IMPB_COMMIS_INTER
                ,COS_RUN_ASSVA
                       ,COS_RUN_ASSVA_IDC
             INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
             FROM TRCH_DI_GAR
             WHERE     ID_POLI = :TGA-ID-POLI
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B410-EX.
           EXIT.

       B460-OPEN-CURSOR-IDP-CPZ.

           PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.

           EXEC SQL
                OPEN C-IDP-CPZ-TGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B460-EX.
           EXIT.

       B470-CLOSE-CURSOR-IDP-CPZ.
           EXEC SQL
                CLOSE C-IDP-CPZ-TGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B470-EX.
           EXIT.

       B480-FETCH-FIRST-IDP-CPZ.
           PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
           END-IF.
       B480-EX.
           EXIT.

       B490-FETCH-NEXT-IDP-CPZ.
           EXEC SQL
                FETCH C-IDP-CPZ-TGA
           INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B490-EX.
           EXIT.
      *----
      *----  gestione IBO Competenza
      *----
       B505-DECLARE-CURSOR-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
                DECLARE C-IBO-CPZ-TGA CURSOR FOR
              SELECT
                     ID_TRCH_DI_GAR
                    ,ID_GAR
                    ,ID_ADES
                    ,ID_POLI
                    ,ID_MOVI_CRZ
                    ,ID_MOVI_CHIU
                    ,DT_INI_EFF
                    ,DT_END_EFF
                    ,COD_COMP_ANIA
                    ,DT_DECOR
                    ,DT_SCAD
                    ,IB_OGG
                    ,TP_RGM_FISC
                    ,DT_EMIS
                    ,TP_TRCH
                    ,DUR_AA
                    ,DUR_MM
                    ,DUR_GG
                    ,PRE_CASO_MOR
                    ,PC_INTR_RIAT
                    ,IMP_BNS_ANTIC
                    ,PRE_INI_NET
                    ,PRE_PP_INI
                    ,PRE_PP_ULT
                    ,PRE_TARI_INI
                    ,PRE_TARI_ULT
                    ,PRE_INVRIO_INI
                    ,PRE_INVRIO_ULT
                    ,PRE_RIVTO
                    ,IMP_SOPR_PROF
                    ,IMP_SOPR_SAN
                    ,IMP_SOPR_SPO
                    ,IMP_SOPR_TEC
                    ,IMP_ALT_SOPR
                    ,PRE_STAB
                    ,DT_EFF_STAB
                    ,TS_RIVAL_FIS
                    ,TS_RIVAL_INDICIZ
                    ,OLD_TS_TEC
                    ,RAT_LRD
                    ,PRE_LRD
                    ,PRSTZ_INI
                    ,PRSTZ_ULT
                    ,CPT_IN_OPZ_RIVTO
                    ,PRSTZ_INI_STAB
                    ,CPT_RSH_MOR
                    ,PRSTZ_RID_INI
                    ,FL_CAR_CONT
                    ,BNS_GIA_LIQTO
                    ,IMP_BNS
                    ,COD_DVS
                    ,PRSTZ_INI_NEWFIS
                    ,IMP_SCON
                    ,ALQ_SCON
                    ,IMP_CAR_ACQ
                    ,IMP_CAR_INC
                    ,IMP_CAR_GEST
                    ,ETA_AA_1O_ASSTO
                    ,ETA_MM_1O_ASSTO
                    ,ETA_AA_2O_ASSTO
                    ,ETA_MM_2O_ASSTO
                    ,ETA_AA_3O_ASSTO
                    ,ETA_MM_3O_ASSTO
                    ,RENDTO_LRD
                    ,PC_RETR
                    ,RENDTO_RETR
                    ,MIN_GARTO
                    ,MIN_TRNUT
                    ,PRE_ATT_DI_TRCH
                    ,MATU_END2000
                    ,ABB_TOT_INI
                    ,ABB_TOT_ULT
                    ,ABB_ANNU_ULT
                    ,DUR_ABB
                    ,TP_ADEG_ABB
                    ,MOD_CALC
                    ,IMP_AZ
                    ,IMP_ADER
                    ,IMP_TFR
                    ,IMP_VOLO
                    ,VIS_END2000
                    ,DT_VLDT_PROD
                    ,DT_INI_VAL_TAR
                    ,IMPB_VIS_END2000
                    ,REN_INI_TS_TEC_0
                    ,PC_RIP_PRE
                    ,FL_IMPORTI_FORZ
                    ,PRSTZ_INI_NFORZ
                    ,VIS_END2000_NFORZ
                    ,INTR_MORA
                    ,MANFEE_ANTIC
                    ,MANFEE_RICOR
                    ,PRE_UNI_RIVTO
                    ,PROV_1AA_ACQ
                    ,PROV_2AA_ACQ
                    ,PROV_RICOR
                    ,PROV_INC
                    ,ALQ_PROV_ACQ
                    ,ALQ_PROV_INC
                    ,ALQ_PROV_RICOR
                    ,IMPB_PROV_ACQ
                    ,IMPB_PROV_INC
                    ,IMPB_PROV_RICOR
                    ,FL_PROV_FORZ
                    ,PRSTZ_AGG_INI
                    ,INCR_PRE
                    ,INCR_PRSTZ
                    ,DT_ULT_ADEG_PRE_PR
                    ,PRSTZ_AGG_ULT
                    ,TS_RIVAL_NET
                    ,PRE_PATTUITO
                    ,TP_RIVAL
                    ,RIS_MAT
                    ,CPT_MIN_SCAD
                    ,COMMIS_GEST
                    ,TP_MANFEE_APPL
                    ,DS_RIGA
                    ,DS_OPER_SQL
                    ,DS_VER
                    ,DS_TS_INI_CPTZ
                    ,DS_TS_END_CPTZ
                    ,DS_UTENTE
                    ,DS_STATO_ELAB
                    ,PC_COMMIS_GEST
                    ,NUM_GG_RIVAL
                    ,IMP_TRASFE
                    ,IMP_TFR_STRC
                    ,ACQ_EXP
                    ,REMUN_ASS
                    ,COMMIS_INTER
                    ,ALQ_REMUN_ASS
                    ,ALQ_COMMIS_INTER
                    ,IMPB_REMUN_ASS
                    ,IMPB_COMMIS_INTER
                    ,COS_RUN_ASSVA
                    ,COS_RUN_ASSVA_IDC
              FROM TRCH_DI_GAR
              WHERE     IB_OGG = :TGA-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA
              ORDER BY ID_TRCH_DI_GAR ASC

           END-EXEC.

       B505-EX.
           EXIT.

       B510-SELECT-IBO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           EXEC SQL
             SELECT
                ID_TRCH_DI_GAR
                ,ID_GAR
                ,ID_ADES
                ,ID_POLI
                ,ID_MOVI_CRZ
                ,ID_MOVI_CHIU
                ,DT_INI_EFF
                ,DT_END_EFF
                ,COD_COMP_ANIA
                ,DT_DECOR
                ,DT_SCAD
                ,IB_OGG
                ,TP_RGM_FISC
                ,DT_EMIS
                ,TP_TRCH
                ,DUR_AA
                ,DUR_MM
                ,DUR_GG
                ,PRE_CASO_MOR
                ,PC_INTR_RIAT
                ,IMP_BNS_ANTIC
                ,PRE_INI_NET
                ,PRE_PP_INI
                ,PRE_PP_ULT
                ,PRE_TARI_INI
                ,PRE_TARI_ULT
                ,PRE_INVRIO_INI
                ,PRE_INVRIO_ULT
                ,PRE_RIVTO
                ,IMP_SOPR_PROF
                ,IMP_SOPR_SAN
                ,IMP_SOPR_SPO
                ,IMP_SOPR_TEC
                ,IMP_ALT_SOPR
                ,PRE_STAB
                ,DT_EFF_STAB
                ,TS_RIVAL_FIS
                ,TS_RIVAL_INDICIZ
                ,OLD_TS_TEC
                ,RAT_LRD
                ,PRE_LRD
                ,PRSTZ_INI
                ,PRSTZ_ULT
                ,CPT_IN_OPZ_RIVTO
                ,PRSTZ_INI_STAB
                ,CPT_RSH_MOR
                ,PRSTZ_RID_INI
                ,FL_CAR_CONT
                ,BNS_GIA_LIQTO
                ,IMP_BNS
                ,COD_DVS
                ,PRSTZ_INI_NEWFIS
                ,IMP_SCON
                ,ALQ_SCON
                ,IMP_CAR_ACQ
                ,IMP_CAR_INC
                ,IMP_CAR_GEST
                ,ETA_AA_1O_ASSTO
                ,ETA_MM_1O_ASSTO
                ,ETA_AA_2O_ASSTO
                ,ETA_MM_2O_ASSTO
                ,ETA_AA_3O_ASSTO
                ,ETA_MM_3O_ASSTO
                ,RENDTO_LRD
                ,PC_RETR
                ,RENDTO_RETR
                ,MIN_GARTO
                ,MIN_TRNUT
                ,PRE_ATT_DI_TRCH
                ,MATU_END2000
                ,ABB_TOT_INI
                ,ABB_TOT_ULT
                ,ABB_ANNU_ULT
                ,DUR_ABB
                ,TP_ADEG_ABB
                ,MOD_CALC
                ,IMP_AZ
                ,IMP_ADER
                ,IMP_TFR
                ,IMP_VOLO
                ,VIS_END2000
                ,DT_VLDT_PROD
                ,DT_INI_VAL_TAR
                ,IMPB_VIS_END2000
                ,REN_INI_TS_TEC_0
                ,PC_RIP_PRE
                ,FL_IMPORTI_FORZ
                ,PRSTZ_INI_NFORZ
                ,VIS_END2000_NFORZ
                ,INTR_MORA
                ,MANFEE_ANTIC
                ,MANFEE_RICOR
                ,PRE_UNI_RIVTO
                ,PROV_1AA_ACQ
                ,PROV_2AA_ACQ
                ,PROV_RICOR
                ,PROV_INC
                ,ALQ_PROV_ACQ
                ,ALQ_PROV_INC
                ,ALQ_PROV_RICOR
                ,IMPB_PROV_ACQ
                ,IMPB_PROV_INC
                ,IMPB_PROV_RICOR
                ,FL_PROV_FORZ
                ,PRSTZ_AGG_INI
                ,INCR_PRE
                ,INCR_PRSTZ
                ,DT_ULT_ADEG_PRE_PR
                ,PRSTZ_AGG_ULT
                ,TS_RIVAL_NET
                ,PRE_PATTUITO
                ,TP_RIVAL
                ,RIS_MAT
                ,CPT_MIN_SCAD
                ,COMMIS_GEST
                ,TP_MANFEE_APPL
                ,DS_RIGA
                ,DS_OPER_SQL
                ,DS_VER
                ,DS_TS_INI_CPTZ
                ,DS_TS_END_CPTZ
                ,DS_UTENTE
                ,DS_STATO_ELAB
                ,PC_COMMIS_GEST
                ,NUM_GG_RIVAL
                ,IMP_TRASFE
                ,IMP_TFR_STRC
                ,ACQ_EXP
                ,REMUN_ASS
                ,COMMIS_INTER
                ,ALQ_REMUN_ASS
                ,ALQ_COMMIS_INTER
                ,IMPB_REMUN_ASS
                ,IMPB_COMMIS_INTER
                ,COS_RUN_ASSVA
                ,COS_RUN_ASSVA_IDC
             INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
             FROM TRCH_DI_GAR
             WHERE     IB_OGG = :TGA-IB-OGG
                    AND COD_COMP_ANIA =
                        :IDSV0003-CODICE-COMPAGNIA-ANIA
                    AND DT_INI_EFF <=
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DT_END_EFF >
                        :WS-DATA-INIZIO-EFFETTO-DB
                    AND DS_TS_INI_CPTZ <=
                         :WS-TS-COMPETENZA
                    AND DS_TS_END_CPTZ >
                         :WS-TS-COMPETENZA

           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           END-IF.
       B510-EX.
           EXIT.

       B560-OPEN-CURSOR-IBO-CPZ.

           PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.

           EXEC SQL
                OPEN C-IBO-CPZ-TGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B560-EX.
           EXIT.

       B570-CLOSE-CURSOR-IBO-CPZ.
           EXEC SQL
                CLOSE C-IBO-CPZ-TGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
       B570-EX.
           EXIT.

       B580-FETCH-FIRST-IBO-CPZ.
           PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
           END-IF.
       B580-EX.
           EXIT.

       B590-FETCH-NEXT-IBO-CPZ.
           EXEC SQL
                FETCH C-IBO-CPZ-TGA
           INTO
                :TGA-ID-TRCH-DI-GAR
               ,:TGA-ID-GAR
               ,:TGA-ID-ADES
               ,:TGA-ID-POLI
               ,:TGA-ID-MOVI-CRZ
               ,:TGA-ID-MOVI-CHIU
                :IND-TGA-ID-MOVI-CHIU
               ,:TGA-DT-INI-EFF-DB
               ,:TGA-DT-END-EFF-DB
               ,:TGA-COD-COMP-ANIA
               ,:TGA-DT-DECOR-DB
               ,:TGA-DT-SCAD-DB
                :IND-TGA-DT-SCAD
               ,:TGA-IB-OGG
                :IND-TGA-IB-OGG
               ,:TGA-TP-RGM-FISC
               ,:TGA-DT-EMIS-DB
                :IND-TGA-DT-EMIS
               ,:TGA-TP-TRCH
               ,:TGA-DUR-AA
                :IND-TGA-DUR-AA
               ,:TGA-DUR-MM
                :IND-TGA-DUR-MM
               ,:TGA-DUR-GG
                :IND-TGA-DUR-GG
               ,:TGA-PRE-CASO-MOR
                :IND-TGA-PRE-CASO-MOR
               ,:TGA-PC-INTR-RIAT
                :IND-TGA-PC-INTR-RIAT
               ,:TGA-IMP-BNS-ANTIC
                :IND-TGA-IMP-BNS-ANTIC
               ,:TGA-PRE-INI-NET
                :IND-TGA-PRE-INI-NET
               ,:TGA-PRE-PP-INI
                :IND-TGA-PRE-PP-INI
               ,:TGA-PRE-PP-ULT
                :IND-TGA-PRE-PP-ULT
               ,:TGA-PRE-TARI-INI
                :IND-TGA-PRE-TARI-INI
               ,:TGA-PRE-TARI-ULT
                :IND-TGA-PRE-TARI-ULT
               ,:TGA-PRE-INVRIO-INI
                :IND-TGA-PRE-INVRIO-INI
               ,:TGA-PRE-INVRIO-ULT
                :IND-TGA-PRE-INVRIO-ULT
               ,:TGA-PRE-RIVTO
                :IND-TGA-PRE-RIVTO
               ,:TGA-IMP-SOPR-PROF
                :IND-TGA-IMP-SOPR-PROF
               ,:TGA-IMP-SOPR-SAN
                :IND-TGA-IMP-SOPR-SAN
               ,:TGA-IMP-SOPR-SPO
                :IND-TGA-IMP-SOPR-SPO
               ,:TGA-IMP-SOPR-TEC
                :IND-TGA-IMP-SOPR-TEC
               ,:TGA-IMP-ALT-SOPR
                :IND-TGA-IMP-ALT-SOPR
               ,:TGA-PRE-STAB
                :IND-TGA-PRE-STAB
               ,:TGA-DT-EFF-STAB-DB
                :IND-TGA-DT-EFF-STAB
               ,:TGA-TS-RIVAL-FIS
                :IND-TGA-TS-RIVAL-FIS
               ,:TGA-TS-RIVAL-INDICIZ
                :IND-TGA-TS-RIVAL-INDICIZ
               ,:TGA-OLD-TS-TEC
                :IND-TGA-OLD-TS-TEC
               ,:TGA-RAT-LRD
                :IND-TGA-RAT-LRD
               ,:TGA-PRE-LRD
                :IND-TGA-PRE-LRD
               ,:TGA-PRSTZ-INI
                :IND-TGA-PRSTZ-INI
               ,:TGA-PRSTZ-ULT
                :IND-TGA-PRSTZ-ULT
               ,:TGA-CPT-IN-OPZ-RIVTO
                :IND-TGA-CPT-IN-OPZ-RIVTO
               ,:TGA-PRSTZ-INI-STAB
                :IND-TGA-PRSTZ-INI-STAB
               ,:TGA-CPT-RSH-MOR
                :IND-TGA-CPT-RSH-MOR
               ,:TGA-PRSTZ-RID-INI
                :IND-TGA-PRSTZ-RID-INI
               ,:TGA-FL-CAR-CONT
                :IND-TGA-FL-CAR-CONT
               ,:TGA-BNS-GIA-LIQTO
                :IND-TGA-BNS-GIA-LIQTO
               ,:TGA-IMP-BNS
                :IND-TGA-IMP-BNS
               ,:TGA-COD-DVS
               ,:TGA-PRSTZ-INI-NEWFIS
                :IND-TGA-PRSTZ-INI-NEWFIS
               ,:TGA-IMP-SCON
                :IND-TGA-IMP-SCON
               ,:TGA-ALQ-SCON
                :IND-TGA-ALQ-SCON
               ,:TGA-IMP-CAR-ACQ
                :IND-TGA-IMP-CAR-ACQ
               ,:TGA-IMP-CAR-INC
                :IND-TGA-IMP-CAR-INC
               ,:TGA-IMP-CAR-GEST
                :IND-TGA-IMP-CAR-GEST
               ,:TGA-ETA-AA-1O-ASSTO
                :IND-TGA-ETA-AA-1O-ASSTO
               ,:TGA-ETA-MM-1O-ASSTO
                :IND-TGA-ETA-MM-1O-ASSTO
               ,:TGA-ETA-AA-2O-ASSTO
                :IND-TGA-ETA-AA-2O-ASSTO
               ,:TGA-ETA-MM-2O-ASSTO
                :IND-TGA-ETA-MM-2O-ASSTO
               ,:TGA-ETA-AA-3O-ASSTO
                :IND-TGA-ETA-AA-3O-ASSTO
               ,:TGA-ETA-MM-3O-ASSTO
                :IND-TGA-ETA-MM-3O-ASSTO
               ,:TGA-RENDTO-LRD
                :IND-TGA-RENDTO-LRD
               ,:TGA-PC-RETR
                :IND-TGA-PC-RETR
               ,:TGA-RENDTO-RETR
                :IND-TGA-RENDTO-RETR
               ,:TGA-MIN-GARTO
                :IND-TGA-MIN-GARTO
               ,:TGA-MIN-TRNUT
                :IND-TGA-MIN-TRNUT
               ,:TGA-PRE-ATT-DI-TRCH
                :IND-TGA-PRE-ATT-DI-TRCH
               ,:TGA-MATU-END2000
                :IND-TGA-MATU-END2000
               ,:TGA-ABB-TOT-INI
                :IND-TGA-ABB-TOT-INI
               ,:TGA-ABB-TOT-ULT
                :IND-TGA-ABB-TOT-ULT
               ,:TGA-ABB-ANNU-ULT
                :IND-TGA-ABB-ANNU-ULT
               ,:TGA-DUR-ABB
                :IND-TGA-DUR-ABB
               ,:TGA-TP-ADEG-ABB
                :IND-TGA-TP-ADEG-ABB
               ,:TGA-MOD-CALC
                :IND-TGA-MOD-CALC
               ,:TGA-IMP-AZ
                :IND-TGA-IMP-AZ
               ,:TGA-IMP-ADER
                :IND-TGA-IMP-ADER
               ,:TGA-IMP-TFR
                :IND-TGA-IMP-TFR
               ,:TGA-IMP-VOLO
                :IND-TGA-IMP-VOLO
               ,:TGA-VIS-END2000
                :IND-TGA-VIS-END2000
               ,:TGA-DT-VLDT-PROD-DB
                :IND-TGA-DT-VLDT-PROD
               ,:TGA-DT-INI-VAL-TAR-DB
                :IND-TGA-DT-INI-VAL-TAR
               ,:TGA-IMPB-VIS-END2000
                :IND-TGA-IMPB-VIS-END2000
               ,:TGA-REN-INI-TS-TEC-0
                :IND-TGA-REN-INI-TS-TEC-0
               ,:TGA-PC-RIP-PRE
                :IND-TGA-PC-RIP-PRE
               ,:TGA-FL-IMPORTI-FORZ
                :IND-TGA-FL-IMPORTI-FORZ
               ,:TGA-PRSTZ-INI-NFORZ
                :IND-TGA-PRSTZ-INI-NFORZ
               ,:TGA-VIS-END2000-NFORZ
                :IND-TGA-VIS-END2000-NFORZ
               ,:TGA-INTR-MORA
                :IND-TGA-INTR-MORA
               ,:TGA-MANFEE-ANTIC
                :IND-TGA-MANFEE-ANTIC
               ,:TGA-MANFEE-RICOR
                :IND-TGA-MANFEE-RICOR
               ,:TGA-PRE-UNI-RIVTO
                :IND-TGA-PRE-UNI-RIVTO
               ,:TGA-PROV-1AA-ACQ
                :IND-TGA-PROV-1AA-ACQ
               ,:TGA-PROV-2AA-ACQ
                :IND-TGA-PROV-2AA-ACQ
               ,:TGA-PROV-RICOR
                :IND-TGA-PROV-RICOR
               ,:TGA-PROV-INC
                :IND-TGA-PROV-INC
               ,:TGA-ALQ-PROV-ACQ
                :IND-TGA-ALQ-PROV-ACQ
               ,:TGA-ALQ-PROV-INC
                :IND-TGA-ALQ-PROV-INC
               ,:TGA-ALQ-PROV-RICOR
                :IND-TGA-ALQ-PROV-RICOR
               ,:TGA-IMPB-PROV-ACQ
                :IND-TGA-IMPB-PROV-ACQ
               ,:TGA-IMPB-PROV-INC
                :IND-TGA-IMPB-PROV-INC
               ,:TGA-IMPB-PROV-RICOR
                :IND-TGA-IMPB-PROV-RICOR
               ,:TGA-FL-PROV-FORZ
                :IND-TGA-FL-PROV-FORZ
               ,:TGA-PRSTZ-AGG-INI
                :IND-TGA-PRSTZ-AGG-INI
               ,:TGA-INCR-PRE
                :IND-TGA-INCR-PRE
               ,:TGA-INCR-PRSTZ
                :IND-TGA-INCR-PRSTZ
               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
                :IND-TGA-DT-ULT-ADEG-PRE-PR
               ,:TGA-PRSTZ-AGG-ULT
                :IND-TGA-PRSTZ-AGG-ULT
               ,:TGA-TS-RIVAL-NET
                :IND-TGA-TS-RIVAL-NET
               ,:TGA-PRE-PATTUITO
                :IND-TGA-PRE-PATTUITO
               ,:TGA-TP-RIVAL
                :IND-TGA-TP-RIVAL
               ,:TGA-RIS-MAT
                :IND-TGA-RIS-MAT
               ,:TGA-CPT-MIN-SCAD
                :IND-TGA-CPT-MIN-SCAD
               ,:TGA-COMMIS-GEST
                :IND-TGA-COMMIS-GEST
               ,:TGA-TP-MANFEE-APPL
                :IND-TGA-TP-MANFEE-APPL
               ,:TGA-DS-RIGA
               ,:TGA-DS-OPER-SQL
               ,:TGA-DS-VER
               ,:TGA-DS-TS-INI-CPTZ
               ,:TGA-DS-TS-END-CPTZ
               ,:TGA-DS-UTENTE
               ,:TGA-DS-STATO-ELAB
               ,:TGA-PC-COMMIS-GEST
                :IND-TGA-PC-COMMIS-GEST
               ,:TGA-NUM-GG-RIVAL
                :IND-TGA-NUM-GG-RIVAL
               ,:TGA-IMP-TRASFE
                :IND-TGA-IMP-TRASFE
               ,:TGA-IMP-TFR-STRC
                :IND-TGA-IMP-TFR-STRC
               ,:TGA-ACQ-EXP
                :IND-TGA-ACQ-EXP
               ,:TGA-REMUN-ASS
                :IND-TGA-REMUN-ASS
               ,:TGA-COMMIS-INTER
                :IND-TGA-COMMIS-INTER
               ,:TGA-ALQ-REMUN-ASS
                :IND-TGA-ALQ-REMUN-ASS
               ,:TGA-ALQ-COMMIS-INTER
                :IND-TGA-ALQ-COMMIS-INTER
               ,:TGA-IMPB-REMUN-ASS
                :IND-TGA-IMPB-REMUN-ASS
               ,:TGA-IMPB-COMMIS-INTER
                :IND-TGA-IMPB-COMMIS-INTER
               ,:TGA-COS-RUN-ASSVA
                :IND-TGA-COS-RUN-ASSVA
               ,:TGA-COS-RUN-ASSVA-IDC
                :IND-TGA-COS-RUN-ASSVA-IDC
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

           IF IDSV0003-SUCCESSFUL-SQL

              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX

              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX

           ELSE
             IF IDSV0003-NOT-FOUND

                PERFORM B570-CLOSE-CURSOR-IBO-CPZ     THRU B570-EX

                IF IDSV0003-SUCCESSFUL-SQL

                   SET IDSV0003-NOT-FOUND TO TRUE

                END-IF
             END-IF
           END-IF.
       B590-EX.
           EXIT.
      *----
      *----  gestione IBS Competenza
      *----


       B605-DECLARE-CURSOR-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B605-EX.
           EXIT.


       B610-SELECT-IBS-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B610-EX.
           EXIT.

       B660-OPEN-CURSOR-IBS-CPZ.

           PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B660-EX.
           EXIT.

       B670-CLOSE-CURSOR-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B670-EX.
           EXIT.

       B680-FETCH-FIRST-IBS-CPZ.
           PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
           END-IF.
       B680-EX.
           EXIT.


       B690-FETCH-NEXT-IBS-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B690-EX.
           EXIT.
      *----
      *----  gestione IDO Competenza
      *----
       B705-DECLARE-CURSOR-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B705-EX.
           EXIT.

       B710-SELECT-IDO-CPZ.

           PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B710-EX.
           EXIT.

       B760-OPEN-CURSOR-IDO-CPZ.

           PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.

           SET IDSV0003-INVALID-OPER TO TRUE.
       B760-EX.
           EXIT.

       B770-CLOSE-CURSOR-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B770-EX.
           EXIT.

       B780-FETCH-FIRST-IDO-CPZ.
           PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.

           IF IDSV0003-SUCCESSFUL-SQL
              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
           END-IF.
       B780-EX.
           EXIT.

       B790-FETCH-NEXT-IDO-CPZ.
           SET IDSV0003-INVALID-OPER TO TRUE.
       B790-EX.
           EXIT.

       Z100-SET-COLONNE-NULL.
           MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.


           IF IND-TGA-ID-MOVI-CHIU = -1
              MOVE HIGH-VALUES TO TGA-ID-MOVI-CHIU-NULL
           END-IF
           IF IND-TGA-DT-SCAD = -1
              MOVE HIGH-VALUES TO TGA-DT-SCAD-NULL
           END-IF
           IF IND-TGA-IB-OGG = -1
              MOVE HIGH-VALUES TO TGA-IB-OGG-NULL
           END-IF
           IF IND-TGA-DT-EMIS = -1
              MOVE HIGH-VALUES TO TGA-DT-EMIS-NULL
           END-IF
           IF IND-TGA-DUR-AA = -1
              MOVE HIGH-VALUES TO TGA-DUR-AA-NULL
           END-IF
           IF IND-TGA-DUR-MM = -1
              MOVE HIGH-VALUES TO TGA-DUR-MM-NULL
           END-IF
           IF IND-TGA-DUR-GG = -1
              MOVE HIGH-VALUES TO TGA-DUR-GG-NULL
           END-IF
           IF IND-TGA-PRE-CASO-MOR = -1
              MOVE HIGH-VALUES TO TGA-PRE-CASO-MOR-NULL
           END-IF
           IF IND-TGA-PC-INTR-RIAT = -1
              MOVE HIGH-VALUES TO TGA-PC-INTR-RIAT-NULL
           END-IF
           IF IND-TGA-IMP-BNS-ANTIC = -1
              MOVE HIGH-VALUES TO TGA-IMP-BNS-ANTIC-NULL
           END-IF
           IF IND-TGA-PRE-INI-NET = -1
              MOVE HIGH-VALUES TO TGA-PRE-INI-NET-NULL
           END-IF
           IF IND-TGA-PRE-PP-INI = -1
              MOVE HIGH-VALUES TO TGA-PRE-PP-INI-NULL
           END-IF
           IF IND-TGA-PRE-PP-ULT = -1
              MOVE HIGH-VALUES TO TGA-PRE-PP-ULT-NULL
           END-IF
           IF IND-TGA-PRE-TARI-INI = -1
              MOVE HIGH-VALUES TO TGA-PRE-TARI-INI-NULL
           END-IF
           IF IND-TGA-PRE-TARI-ULT = -1
              MOVE HIGH-VALUES TO TGA-PRE-TARI-ULT-NULL
           END-IF
           IF IND-TGA-PRE-INVRIO-INI = -1
              MOVE HIGH-VALUES TO TGA-PRE-INVRIO-INI-NULL
           END-IF
           IF IND-TGA-PRE-INVRIO-ULT = -1
              MOVE HIGH-VALUES TO TGA-PRE-INVRIO-ULT-NULL
           END-IF
           IF IND-TGA-PRE-RIVTO = -1
              MOVE HIGH-VALUES TO TGA-PRE-RIVTO-NULL
           END-IF
           IF IND-TGA-IMP-SOPR-PROF = -1
              MOVE HIGH-VALUES TO TGA-IMP-SOPR-PROF-NULL
           END-IF
           IF IND-TGA-IMP-SOPR-SAN = -1
              MOVE HIGH-VALUES TO TGA-IMP-SOPR-SAN-NULL
           END-IF
           IF IND-TGA-IMP-SOPR-SPO = -1
              MOVE HIGH-VALUES TO TGA-IMP-SOPR-SPO-NULL
           END-IF
           IF IND-TGA-IMP-SOPR-TEC = -1
              MOVE HIGH-VALUES TO TGA-IMP-SOPR-TEC-NULL
           END-IF
           IF IND-TGA-IMP-ALT-SOPR = -1
              MOVE HIGH-VALUES TO TGA-IMP-ALT-SOPR-NULL
           END-IF
           IF IND-TGA-PRE-STAB = -1
              MOVE HIGH-VALUES TO TGA-PRE-STAB-NULL
           END-IF
           IF IND-TGA-DT-EFF-STAB = -1
              MOVE HIGH-VALUES TO TGA-DT-EFF-STAB-NULL
           END-IF
           IF IND-TGA-TS-RIVAL-FIS = -1
              MOVE HIGH-VALUES TO TGA-TS-RIVAL-FIS-NULL
           END-IF
           IF IND-TGA-TS-RIVAL-INDICIZ = -1
              MOVE HIGH-VALUES TO TGA-TS-RIVAL-INDICIZ-NULL
           END-IF
           IF IND-TGA-OLD-TS-TEC = -1
              MOVE HIGH-VALUES TO TGA-OLD-TS-TEC-NULL
           END-IF
           IF IND-TGA-RAT-LRD = -1
              MOVE HIGH-VALUES TO TGA-RAT-LRD-NULL
           END-IF
           IF IND-TGA-PRE-LRD = -1
              MOVE HIGH-VALUES TO TGA-PRE-LRD-NULL
           END-IF
           IF IND-TGA-PRSTZ-INI = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NULL
           END-IF
           IF IND-TGA-PRSTZ-ULT = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-ULT-NULL
           END-IF
           IF IND-TGA-CPT-IN-OPZ-RIVTO = -1
              MOVE HIGH-VALUES TO TGA-CPT-IN-OPZ-RIVTO-NULL
           END-IF
           IF IND-TGA-PRSTZ-INI-STAB = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-STAB-NULL
           END-IF
           IF IND-TGA-CPT-RSH-MOR = -1
              MOVE HIGH-VALUES TO TGA-CPT-RSH-MOR-NULL
           END-IF
           IF IND-TGA-PRSTZ-RID-INI = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-RID-INI-NULL
           END-IF
           IF IND-TGA-FL-CAR-CONT = -1
              MOVE HIGH-VALUES TO TGA-FL-CAR-CONT-NULL
           END-IF
           IF IND-TGA-BNS-GIA-LIQTO = -1
              MOVE HIGH-VALUES TO TGA-BNS-GIA-LIQTO-NULL
           END-IF
           IF IND-TGA-IMP-BNS = -1
              MOVE HIGH-VALUES TO TGA-IMP-BNS-NULL
           END-IF
           IF IND-TGA-PRSTZ-INI-NEWFIS = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NEWFIS-NULL
           END-IF
           IF IND-TGA-IMP-SCON = -1
              MOVE HIGH-VALUES TO TGA-IMP-SCON-NULL
           END-IF
           IF IND-TGA-ALQ-SCON = -1
              MOVE HIGH-VALUES TO TGA-ALQ-SCON-NULL
           END-IF
           IF IND-TGA-IMP-CAR-ACQ = -1
              MOVE HIGH-VALUES TO TGA-IMP-CAR-ACQ-NULL
           END-IF
           IF IND-TGA-IMP-CAR-INC = -1
              MOVE HIGH-VALUES TO TGA-IMP-CAR-INC-NULL
           END-IF
           IF IND-TGA-IMP-CAR-GEST = -1
              MOVE HIGH-VALUES TO TGA-IMP-CAR-GEST-NULL
           END-IF
           IF IND-TGA-ETA-AA-1O-ASSTO = -1
              MOVE HIGH-VALUES TO TGA-ETA-AA-1O-ASSTO-NULL
           END-IF
           IF IND-TGA-ETA-MM-1O-ASSTO = -1
              MOVE HIGH-VALUES TO TGA-ETA-MM-1O-ASSTO-NULL
           END-IF
           IF IND-TGA-ETA-AA-2O-ASSTO = -1
              MOVE HIGH-VALUES TO TGA-ETA-AA-2O-ASSTO-NULL
           END-IF
           IF IND-TGA-ETA-MM-2O-ASSTO = -1
              MOVE HIGH-VALUES TO TGA-ETA-MM-2O-ASSTO-NULL
           END-IF
           IF IND-TGA-ETA-AA-3O-ASSTO = -1
              MOVE HIGH-VALUES TO TGA-ETA-AA-3O-ASSTO-NULL
           END-IF
           IF IND-TGA-ETA-MM-3O-ASSTO = -1
              MOVE HIGH-VALUES TO TGA-ETA-MM-3O-ASSTO-NULL
           END-IF
           IF IND-TGA-RENDTO-LRD = -1
              MOVE HIGH-VALUES TO TGA-RENDTO-LRD-NULL
           END-IF
           IF IND-TGA-PC-RETR = -1
              MOVE HIGH-VALUES TO TGA-PC-RETR-NULL
           END-IF
           IF IND-TGA-RENDTO-RETR = -1
              MOVE HIGH-VALUES TO TGA-RENDTO-RETR-NULL
           END-IF
           IF IND-TGA-MIN-GARTO = -1
              MOVE HIGH-VALUES TO TGA-MIN-GARTO-NULL
           END-IF
           IF IND-TGA-MIN-TRNUT = -1
              MOVE HIGH-VALUES TO TGA-MIN-TRNUT-NULL
           END-IF
           IF IND-TGA-PRE-ATT-DI-TRCH = -1
              MOVE HIGH-VALUES TO TGA-PRE-ATT-DI-TRCH-NULL
           END-IF
           IF IND-TGA-MATU-END2000 = -1
              MOVE HIGH-VALUES TO TGA-MATU-END2000-NULL
           END-IF
           IF IND-TGA-ABB-TOT-INI = -1
              MOVE HIGH-VALUES TO TGA-ABB-TOT-INI-NULL
           END-IF
           IF IND-TGA-ABB-TOT-ULT = -1
              MOVE HIGH-VALUES TO TGA-ABB-TOT-ULT-NULL
           END-IF
           IF IND-TGA-ABB-ANNU-ULT = -1
              MOVE HIGH-VALUES TO TGA-ABB-ANNU-ULT-NULL
           END-IF
           IF IND-TGA-DUR-ABB = -1
              MOVE HIGH-VALUES TO TGA-DUR-ABB-NULL
           END-IF
           IF IND-TGA-TP-ADEG-ABB = -1
              MOVE HIGH-VALUES TO TGA-TP-ADEG-ABB-NULL
           END-IF
           IF IND-TGA-MOD-CALC = -1
              MOVE HIGH-VALUES TO TGA-MOD-CALC-NULL
           END-IF
           IF IND-TGA-IMP-AZ = -1
              MOVE HIGH-VALUES TO TGA-IMP-AZ-NULL
           END-IF
           IF IND-TGA-IMP-ADER = -1
              MOVE HIGH-VALUES TO TGA-IMP-ADER-NULL
           END-IF
           IF IND-TGA-IMP-TFR = -1
              MOVE HIGH-VALUES TO TGA-IMP-TFR-NULL
           END-IF
           IF IND-TGA-IMP-VOLO = -1
              MOVE HIGH-VALUES TO TGA-IMP-VOLO-NULL
           END-IF
           IF IND-TGA-VIS-END2000 = -1
              MOVE HIGH-VALUES TO TGA-VIS-END2000-NULL
           END-IF
           IF IND-TGA-DT-VLDT-PROD = -1
              MOVE HIGH-VALUES TO TGA-DT-VLDT-PROD-NULL
           END-IF
           IF IND-TGA-DT-INI-VAL-TAR = -1
              MOVE HIGH-VALUES TO TGA-DT-INI-VAL-TAR-NULL
           END-IF
           IF IND-TGA-IMPB-VIS-END2000 = -1
              MOVE HIGH-VALUES TO TGA-IMPB-VIS-END2000-NULL
           END-IF
           IF IND-TGA-REN-INI-TS-TEC-0 = -1
              MOVE HIGH-VALUES TO TGA-REN-INI-TS-TEC-0-NULL
           END-IF
           IF IND-TGA-PC-RIP-PRE = -1
              MOVE HIGH-VALUES TO TGA-PC-RIP-PRE-NULL
           END-IF
           IF IND-TGA-FL-IMPORTI-FORZ = -1
              MOVE HIGH-VALUES TO TGA-FL-IMPORTI-FORZ-NULL
           END-IF
           IF IND-TGA-PRSTZ-INI-NFORZ = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NFORZ-NULL
           END-IF
           IF IND-TGA-VIS-END2000-NFORZ = -1
              MOVE HIGH-VALUES TO TGA-VIS-END2000-NFORZ-NULL
           END-IF
           IF IND-TGA-INTR-MORA = -1
              MOVE HIGH-VALUES TO TGA-INTR-MORA-NULL
           END-IF
           IF IND-TGA-MANFEE-ANTIC = -1
              MOVE HIGH-VALUES TO TGA-MANFEE-ANTIC-NULL
           END-IF
           IF IND-TGA-MANFEE-RICOR = -1
              MOVE HIGH-VALUES TO TGA-MANFEE-RICOR-NULL
           END-IF
           IF IND-TGA-PRE-UNI-RIVTO = -1
              MOVE HIGH-VALUES TO TGA-PRE-UNI-RIVTO-NULL
           END-IF
           IF IND-TGA-PROV-1AA-ACQ = -1
              MOVE HIGH-VALUES TO TGA-PROV-1AA-ACQ-NULL
           END-IF
           IF IND-TGA-PROV-2AA-ACQ = -1
              MOVE HIGH-VALUES TO TGA-PROV-2AA-ACQ-NULL
           END-IF
           IF IND-TGA-PROV-RICOR = -1
              MOVE HIGH-VALUES TO TGA-PROV-RICOR-NULL
           END-IF
           IF IND-TGA-PROV-INC = -1
              MOVE HIGH-VALUES TO TGA-PROV-INC-NULL
           END-IF
           IF IND-TGA-ALQ-PROV-ACQ = -1
              MOVE HIGH-VALUES TO TGA-ALQ-PROV-ACQ-NULL
           END-IF
           IF IND-TGA-ALQ-PROV-INC = -1
              MOVE HIGH-VALUES TO TGA-ALQ-PROV-INC-NULL
           END-IF
           IF IND-TGA-ALQ-PROV-RICOR = -1
              MOVE HIGH-VALUES TO TGA-ALQ-PROV-RICOR-NULL
           END-IF
           IF IND-TGA-IMPB-PROV-ACQ = -1
              MOVE HIGH-VALUES TO TGA-IMPB-PROV-ACQ-NULL
           END-IF
           IF IND-TGA-IMPB-PROV-INC = -1
              MOVE HIGH-VALUES TO TGA-IMPB-PROV-INC-NULL
           END-IF
           IF IND-TGA-IMPB-PROV-RICOR = -1
              MOVE HIGH-VALUES TO TGA-IMPB-PROV-RICOR-NULL
           END-IF
           IF IND-TGA-FL-PROV-FORZ = -1
              MOVE HIGH-VALUES TO TGA-FL-PROV-FORZ-NULL
           END-IF
           IF IND-TGA-PRSTZ-AGG-INI = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-INI-NULL
           END-IF
           IF IND-TGA-INCR-PRE = -1
              MOVE HIGH-VALUES TO TGA-INCR-PRE-NULL
           END-IF
           IF IND-TGA-INCR-PRSTZ = -1
              MOVE HIGH-VALUES TO TGA-INCR-PRSTZ-NULL
           END-IF
           IF IND-TGA-DT-ULT-ADEG-PRE-PR = -1
              MOVE HIGH-VALUES TO TGA-DT-ULT-ADEG-PRE-PR-NULL
           END-IF
           IF IND-TGA-PRSTZ-AGG-ULT = -1
              MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-ULT-NULL
           END-IF
           IF IND-TGA-TS-RIVAL-NET = -1
              MOVE HIGH-VALUES TO TGA-TS-RIVAL-NET-NULL
           END-IF
           IF IND-TGA-PRE-PATTUITO = -1
              MOVE HIGH-VALUES TO TGA-PRE-PATTUITO-NULL
           END-IF
           IF IND-TGA-TP-RIVAL = -1
              MOVE HIGH-VALUES TO TGA-TP-RIVAL-NULL
           END-IF
           IF IND-TGA-RIS-MAT = -1
              MOVE HIGH-VALUES TO TGA-RIS-MAT-NULL
           END-IF
           IF IND-TGA-CPT-MIN-SCAD = -1
              MOVE HIGH-VALUES TO TGA-CPT-MIN-SCAD-NULL
           END-IF
           IF IND-TGA-COMMIS-GEST = -1
              MOVE HIGH-VALUES TO TGA-COMMIS-GEST-NULL
           END-IF
           IF IND-TGA-TP-MANFEE-APPL = -1
              MOVE HIGH-VALUES TO TGA-TP-MANFEE-APPL-NULL
           END-IF
           IF IND-TGA-PC-COMMIS-GEST = -1
              MOVE HIGH-VALUES TO TGA-PC-COMMIS-GEST-NULL
           END-IF
           IF IND-TGA-NUM-GG-RIVAL = -1
              MOVE HIGH-VALUES TO TGA-NUM-GG-RIVAL-NULL
           END-IF
           IF IND-TGA-IMP-TRASFE = -1
              MOVE HIGH-VALUES TO TGA-IMP-TRASFE-NULL
           END-IF
           IF IND-TGA-IMP-TFR-STRC = -1
              MOVE HIGH-VALUES TO TGA-IMP-TFR-STRC-NULL
           END-IF
           IF IND-TGA-ACQ-EXP = -1
              MOVE HIGH-VALUES TO TGA-ACQ-EXP-NULL
           END-IF
           IF IND-TGA-REMUN-ASS = -1
              MOVE HIGH-VALUES TO TGA-REMUN-ASS-NULL
           END-IF
           IF IND-TGA-COMMIS-INTER = -1
              MOVE HIGH-VALUES TO TGA-COMMIS-INTER-NULL
           END-IF
           IF IND-TGA-ALQ-REMUN-ASS = -1
              MOVE HIGH-VALUES TO TGA-ALQ-REMUN-ASS-NULL
           END-IF
           IF IND-TGA-ALQ-COMMIS-INTER = -1
              MOVE HIGH-VALUES TO TGA-ALQ-COMMIS-INTER-NULL
           END-IF
           IF IND-TGA-IMPB-REMUN-ASS = -1
              MOVE HIGH-VALUES TO TGA-IMPB-REMUN-ASS-NULL
           END-IF
           IF IND-TGA-IMPB-COMMIS-INTER = -1
              MOVE HIGH-VALUES TO TGA-IMPB-COMMIS-INTER-NULL
           END-IF
           IF IND-TGA-COS-RUN-ASSVA = -1
              MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-NULL
           END-IF
           IF IND-TGA-COS-RUN-ASSVA-IDC = -1
              MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-IDC-NULL
           END-IF.

       Z100-EX.
           EXIT.

       Z150-VALORIZZA-DATA-SERVICES-I.


           MOVE 'I' TO TGA-DS-OPER-SQL
           MOVE 0                   TO TGA-DS-VER
           MOVE IDSV0003-USER-NAME TO TGA-DS-UTENTE
           MOVE '1'                   TO TGA-DS-STATO-ELAB.

       Z150-EX.
           EXIT.

       Z160-VALORIZZA-DATA-SERVICES-U.


           MOVE 'U' TO TGA-DS-OPER-SQL
           MOVE IDSV0003-USER-NAME TO TGA-DS-UTENTE.

       Z160-EX.
           EXIT.

       Z200-SET-INDICATORI-NULL.


           IF TGA-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ID-MOVI-CHIU
           ELSE
              MOVE 0 TO IND-TGA-ID-MOVI-CHIU
           END-IF
           IF TGA-DT-SCAD-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-DT-SCAD
           ELSE
              MOVE 0 TO IND-TGA-DT-SCAD
           END-IF
           IF TGA-IB-OGG-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IB-OGG
           ELSE
              MOVE 0 TO IND-TGA-IB-OGG
           END-IF
           IF TGA-DT-EMIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-DT-EMIS
           ELSE
              MOVE 0 TO IND-TGA-DT-EMIS
           END-IF
           IF TGA-DUR-AA-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-DUR-AA
           ELSE
              MOVE 0 TO IND-TGA-DUR-AA
           END-IF
           IF TGA-DUR-MM-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-DUR-MM
           ELSE
              MOVE 0 TO IND-TGA-DUR-MM
           END-IF
           IF TGA-DUR-GG-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-DUR-GG
           ELSE
              MOVE 0 TO IND-TGA-DUR-GG
           END-IF
           IF TGA-PRE-CASO-MOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRE-CASO-MOR
           ELSE
              MOVE 0 TO IND-TGA-PRE-CASO-MOR
           END-IF
           IF TGA-PC-INTR-RIAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PC-INTR-RIAT
           ELSE
              MOVE 0 TO IND-TGA-PC-INTR-RIAT
           END-IF
           IF TGA-IMP-BNS-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-BNS-ANTIC
           ELSE
              MOVE 0 TO IND-TGA-IMP-BNS-ANTIC
           END-IF
           IF TGA-PRE-INI-NET-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRE-INI-NET
           ELSE
              MOVE 0 TO IND-TGA-PRE-INI-NET
           END-IF
           IF TGA-PRE-PP-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRE-PP-INI
           ELSE
              MOVE 0 TO IND-TGA-PRE-PP-INI
           END-IF
           IF TGA-PRE-PP-ULT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRE-PP-ULT
           ELSE
              MOVE 0 TO IND-TGA-PRE-PP-ULT
           END-IF
           IF TGA-PRE-TARI-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRE-TARI-INI
           ELSE
              MOVE 0 TO IND-TGA-PRE-TARI-INI
           END-IF
           IF TGA-PRE-TARI-ULT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRE-TARI-ULT
           ELSE
              MOVE 0 TO IND-TGA-PRE-TARI-ULT
           END-IF
           IF TGA-PRE-INVRIO-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRE-INVRIO-INI
           ELSE
              MOVE 0 TO IND-TGA-PRE-INVRIO-INI
           END-IF
           IF TGA-PRE-INVRIO-ULT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRE-INVRIO-ULT
           ELSE
              MOVE 0 TO IND-TGA-PRE-INVRIO-ULT
           END-IF
           IF TGA-PRE-RIVTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRE-RIVTO
           ELSE
              MOVE 0 TO IND-TGA-PRE-RIVTO
           END-IF
           IF TGA-IMP-SOPR-PROF-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-SOPR-PROF
           ELSE
              MOVE 0 TO IND-TGA-IMP-SOPR-PROF
           END-IF
           IF TGA-IMP-SOPR-SAN-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-SOPR-SAN
           ELSE
              MOVE 0 TO IND-TGA-IMP-SOPR-SAN
           END-IF
           IF TGA-IMP-SOPR-SPO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-SOPR-SPO
           ELSE
              MOVE 0 TO IND-TGA-IMP-SOPR-SPO
           END-IF
           IF TGA-IMP-SOPR-TEC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-SOPR-TEC
           ELSE
              MOVE 0 TO IND-TGA-IMP-SOPR-TEC
           END-IF
           IF TGA-IMP-ALT-SOPR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-ALT-SOPR
           ELSE
              MOVE 0 TO IND-TGA-IMP-ALT-SOPR
           END-IF
           IF TGA-PRE-STAB-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRE-STAB
           ELSE
              MOVE 0 TO IND-TGA-PRE-STAB
           END-IF
           IF TGA-DT-EFF-STAB-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-DT-EFF-STAB
           ELSE
              MOVE 0 TO IND-TGA-DT-EFF-STAB
           END-IF
           IF TGA-TS-RIVAL-FIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-TS-RIVAL-FIS
           ELSE
              MOVE 0 TO IND-TGA-TS-RIVAL-FIS
           END-IF
           IF TGA-TS-RIVAL-INDICIZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-TS-RIVAL-INDICIZ
           ELSE
              MOVE 0 TO IND-TGA-TS-RIVAL-INDICIZ
           END-IF
           IF TGA-OLD-TS-TEC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-OLD-TS-TEC
           ELSE
              MOVE 0 TO IND-TGA-OLD-TS-TEC
           END-IF
           IF TGA-RAT-LRD-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-RAT-LRD
           ELSE
              MOVE 0 TO IND-TGA-RAT-LRD
           END-IF
           IF TGA-PRE-LRD-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRE-LRD
           ELSE
              MOVE 0 TO IND-TGA-PRE-LRD
           END-IF
           IF TGA-PRSTZ-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRSTZ-INI
           ELSE
              MOVE 0 TO IND-TGA-PRSTZ-INI
           END-IF
           IF TGA-PRSTZ-ULT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRSTZ-ULT
           ELSE
              MOVE 0 TO IND-TGA-PRSTZ-ULT
           END-IF
           IF TGA-CPT-IN-OPZ-RIVTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-CPT-IN-OPZ-RIVTO
           ELSE
              MOVE 0 TO IND-TGA-CPT-IN-OPZ-RIVTO
           END-IF
           IF TGA-PRSTZ-INI-STAB-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRSTZ-INI-STAB
           ELSE
              MOVE 0 TO IND-TGA-PRSTZ-INI-STAB
           END-IF
           IF TGA-CPT-RSH-MOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-CPT-RSH-MOR
           ELSE
              MOVE 0 TO IND-TGA-CPT-RSH-MOR
           END-IF
           IF TGA-PRSTZ-RID-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRSTZ-RID-INI
           ELSE
              MOVE 0 TO IND-TGA-PRSTZ-RID-INI
           END-IF
           IF TGA-FL-CAR-CONT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-FL-CAR-CONT
           ELSE
              MOVE 0 TO IND-TGA-FL-CAR-CONT
           END-IF
           IF TGA-BNS-GIA-LIQTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-BNS-GIA-LIQTO
           ELSE
              MOVE 0 TO IND-TGA-BNS-GIA-LIQTO
           END-IF
           IF TGA-IMP-BNS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-BNS
           ELSE
              MOVE 0 TO IND-TGA-IMP-BNS
           END-IF
           IF TGA-PRSTZ-INI-NEWFIS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRSTZ-INI-NEWFIS
           ELSE
              MOVE 0 TO IND-TGA-PRSTZ-INI-NEWFIS
           END-IF
           IF TGA-IMP-SCON-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-SCON
           ELSE
              MOVE 0 TO IND-TGA-IMP-SCON
           END-IF
           IF TGA-ALQ-SCON-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ALQ-SCON
           ELSE
              MOVE 0 TO IND-TGA-ALQ-SCON
           END-IF
           IF TGA-IMP-CAR-ACQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-CAR-ACQ
           ELSE
              MOVE 0 TO IND-TGA-IMP-CAR-ACQ
           END-IF
           IF TGA-IMP-CAR-INC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-CAR-INC
           ELSE
              MOVE 0 TO IND-TGA-IMP-CAR-INC
           END-IF
           IF TGA-IMP-CAR-GEST-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-CAR-GEST
           ELSE
              MOVE 0 TO IND-TGA-IMP-CAR-GEST
           END-IF
           IF TGA-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ETA-AA-1O-ASSTO
           ELSE
              MOVE 0 TO IND-TGA-ETA-AA-1O-ASSTO
           END-IF
           IF TGA-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ETA-MM-1O-ASSTO
           ELSE
              MOVE 0 TO IND-TGA-ETA-MM-1O-ASSTO
           END-IF
           IF TGA-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ETA-AA-2O-ASSTO
           ELSE
              MOVE 0 TO IND-TGA-ETA-AA-2O-ASSTO
           END-IF
           IF TGA-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ETA-MM-2O-ASSTO
           ELSE
              MOVE 0 TO IND-TGA-ETA-MM-2O-ASSTO
           END-IF
           IF TGA-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ETA-AA-3O-ASSTO
           ELSE
              MOVE 0 TO IND-TGA-ETA-AA-3O-ASSTO
           END-IF
           IF TGA-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ETA-MM-3O-ASSTO
           ELSE
              MOVE 0 TO IND-TGA-ETA-MM-3O-ASSTO
           END-IF
           IF TGA-RENDTO-LRD-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-RENDTO-LRD
           ELSE
              MOVE 0 TO IND-TGA-RENDTO-LRD
           END-IF
           IF TGA-PC-RETR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PC-RETR
           ELSE
              MOVE 0 TO IND-TGA-PC-RETR
           END-IF
           IF TGA-RENDTO-RETR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-RENDTO-RETR
           ELSE
              MOVE 0 TO IND-TGA-RENDTO-RETR
           END-IF
           IF TGA-MIN-GARTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-MIN-GARTO
           ELSE
              MOVE 0 TO IND-TGA-MIN-GARTO
           END-IF
           IF TGA-MIN-TRNUT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-MIN-TRNUT
           ELSE
              MOVE 0 TO IND-TGA-MIN-TRNUT
           END-IF
           IF TGA-PRE-ATT-DI-TRCH-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRE-ATT-DI-TRCH
           ELSE
              MOVE 0 TO IND-TGA-PRE-ATT-DI-TRCH
           END-IF
           IF TGA-MATU-END2000-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-MATU-END2000
           ELSE
              MOVE 0 TO IND-TGA-MATU-END2000
           END-IF
           IF TGA-ABB-TOT-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ABB-TOT-INI
           ELSE
              MOVE 0 TO IND-TGA-ABB-TOT-INI
           END-IF
           IF TGA-ABB-TOT-ULT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ABB-TOT-ULT
           ELSE
              MOVE 0 TO IND-TGA-ABB-TOT-ULT
           END-IF
           IF TGA-ABB-ANNU-ULT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ABB-ANNU-ULT
           ELSE
              MOVE 0 TO IND-TGA-ABB-ANNU-ULT
           END-IF
           IF TGA-DUR-ABB-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-DUR-ABB
           ELSE
              MOVE 0 TO IND-TGA-DUR-ABB
           END-IF
           IF TGA-TP-ADEG-ABB-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-TP-ADEG-ABB
           ELSE
              MOVE 0 TO IND-TGA-TP-ADEG-ABB
           END-IF
           IF TGA-MOD-CALC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-MOD-CALC
           ELSE
              MOVE 0 TO IND-TGA-MOD-CALC
           END-IF
           IF TGA-IMP-AZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-AZ
           ELSE
              MOVE 0 TO IND-TGA-IMP-AZ
           END-IF
           IF TGA-IMP-ADER-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-ADER
           ELSE
              MOVE 0 TO IND-TGA-IMP-ADER
           END-IF
           IF TGA-IMP-TFR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-TFR
           ELSE
              MOVE 0 TO IND-TGA-IMP-TFR
           END-IF
           IF TGA-IMP-VOLO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-VOLO
           ELSE
              MOVE 0 TO IND-TGA-IMP-VOLO
           END-IF
           IF TGA-VIS-END2000-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-VIS-END2000
           ELSE
              MOVE 0 TO IND-TGA-VIS-END2000
           END-IF
           IF TGA-DT-VLDT-PROD-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-DT-VLDT-PROD
           ELSE
              MOVE 0 TO IND-TGA-DT-VLDT-PROD
           END-IF
           IF TGA-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-DT-INI-VAL-TAR
           ELSE
              MOVE 0 TO IND-TGA-DT-INI-VAL-TAR
           END-IF
           IF TGA-IMPB-VIS-END2000-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMPB-VIS-END2000
           ELSE
              MOVE 0 TO IND-TGA-IMPB-VIS-END2000
           END-IF
           IF TGA-REN-INI-TS-TEC-0-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-REN-INI-TS-TEC-0
           ELSE
              MOVE 0 TO IND-TGA-REN-INI-TS-TEC-0
           END-IF
           IF TGA-PC-RIP-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PC-RIP-PRE
           ELSE
              MOVE 0 TO IND-TGA-PC-RIP-PRE
           END-IF
           IF TGA-FL-IMPORTI-FORZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-FL-IMPORTI-FORZ
           ELSE
              MOVE 0 TO IND-TGA-FL-IMPORTI-FORZ
           END-IF
           IF TGA-PRSTZ-INI-NFORZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRSTZ-INI-NFORZ
           ELSE
              MOVE 0 TO IND-TGA-PRSTZ-INI-NFORZ
           END-IF
           IF TGA-VIS-END2000-NFORZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-VIS-END2000-NFORZ
           ELSE
              MOVE 0 TO IND-TGA-VIS-END2000-NFORZ
           END-IF
           IF TGA-INTR-MORA-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-INTR-MORA
           ELSE
              MOVE 0 TO IND-TGA-INTR-MORA
           END-IF
           IF TGA-MANFEE-ANTIC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-MANFEE-ANTIC
           ELSE
              MOVE 0 TO IND-TGA-MANFEE-ANTIC
           END-IF
           IF TGA-MANFEE-RICOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-MANFEE-RICOR
           ELSE
              MOVE 0 TO IND-TGA-MANFEE-RICOR
           END-IF
           IF TGA-PRE-UNI-RIVTO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRE-UNI-RIVTO
           ELSE
              MOVE 0 TO IND-TGA-PRE-UNI-RIVTO
           END-IF
           IF TGA-PROV-1AA-ACQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PROV-1AA-ACQ
           ELSE
              MOVE 0 TO IND-TGA-PROV-1AA-ACQ
           END-IF
           IF TGA-PROV-2AA-ACQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PROV-2AA-ACQ
           ELSE
              MOVE 0 TO IND-TGA-PROV-2AA-ACQ
           END-IF
           IF TGA-PROV-RICOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PROV-RICOR
           ELSE
              MOVE 0 TO IND-TGA-PROV-RICOR
           END-IF
           IF TGA-PROV-INC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PROV-INC
           ELSE
              MOVE 0 TO IND-TGA-PROV-INC
           END-IF
           IF TGA-ALQ-PROV-ACQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ALQ-PROV-ACQ
           ELSE
              MOVE 0 TO IND-TGA-ALQ-PROV-ACQ
           END-IF
           IF TGA-ALQ-PROV-INC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ALQ-PROV-INC
           ELSE
              MOVE 0 TO IND-TGA-ALQ-PROV-INC
           END-IF
           IF TGA-ALQ-PROV-RICOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ALQ-PROV-RICOR
           ELSE
              MOVE 0 TO IND-TGA-ALQ-PROV-RICOR
           END-IF
           IF TGA-IMPB-PROV-ACQ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMPB-PROV-ACQ
           ELSE
              MOVE 0 TO IND-TGA-IMPB-PROV-ACQ
           END-IF
           IF TGA-IMPB-PROV-INC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMPB-PROV-INC
           ELSE
              MOVE 0 TO IND-TGA-IMPB-PROV-INC
           END-IF
           IF TGA-IMPB-PROV-RICOR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMPB-PROV-RICOR
           ELSE
              MOVE 0 TO IND-TGA-IMPB-PROV-RICOR
           END-IF
           IF TGA-FL-PROV-FORZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-FL-PROV-FORZ
           ELSE
              MOVE 0 TO IND-TGA-FL-PROV-FORZ
           END-IF
           IF TGA-PRSTZ-AGG-INI-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRSTZ-AGG-INI
           ELSE
              MOVE 0 TO IND-TGA-PRSTZ-AGG-INI
           END-IF
           IF TGA-INCR-PRE-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-INCR-PRE
           ELSE
              MOVE 0 TO IND-TGA-INCR-PRE
           END-IF
           IF TGA-INCR-PRSTZ-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-INCR-PRSTZ
           ELSE
              MOVE 0 TO IND-TGA-INCR-PRSTZ
           END-IF
           IF TGA-DT-ULT-ADEG-PRE-PR-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-DT-ULT-ADEG-PRE-PR
           ELSE
              MOVE 0 TO IND-TGA-DT-ULT-ADEG-PRE-PR
           END-IF
           IF TGA-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRSTZ-AGG-ULT
           ELSE
              MOVE 0 TO IND-TGA-PRSTZ-AGG-ULT
           END-IF
           IF TGA-TS-RIVAL-NET-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-TS-RIVAL-NET
           ELSE
              MOVE 0 TO IND-TGA-TS-RIVAL-NET
           END-IF
           IF TGA-PRE-PATTUITO-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PRE-PATTUITO
           ELSE
              MOVE 0 TO IND-TGA-PRE-PATTUITO
           END-IF
           IF TGA-TP-RIVAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-TP-RIVAL
           ELSE
              MOVE 0 TO IND-TGA-TP-RIVAL
           END-IF
           IF TGA-RIS-MAT-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-RIS-MAT
           ELSE
              MOVE 0 TO IND-TGA-RIS-MAT
           END-IF
           IF TGA-CPT-MIN-SCAD-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-CPT-MIN-SCAD
           ELSE
              MOVE 0 TO IND-TGA-CPT-MIN-SCAD
           END-IF
           IF TGA-COMMIS-GEST-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-COMMIS-GEST
           ELSE
              MOVE 0 TO IND-TGA-COMMIS-GEST
           END-IF
           IF TGA-TP-MANFEE-APPL-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-TP-MANFEE-APPL
           ELSE
              MOVE 0 TO IND-TGA-TP-MANFEE-APPL
           END-IF
           IF TGA-PC-COMMIS-GEST-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-PC-COMMIS-GEST
           ELSE
              MOVE 0 TO IND-TGA-PC-COMMIS-GEST
           END-IF
           IF TGA-NUM-GG-RIVAL-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-NUM-GG-RIVAL
           ELSE
              MOVE 0 TO IND-TGA-NUM-GG-RIVAL
           END-IF
           IF TGA-IMP-TRASFE-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-TRASFE
           ELSE
              MOVE 0 TO IND-TGA-IMP-TRASFE
           END-IF
           IF TGA-IMP-TFR-STRC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMP-TFR-STRC
           ELSE
              MOVE 0 TO IND-TGA-IMP-TFR-STRC
           END-IF
           IF TGA-ACQ-EXP-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ACQ-EXP
           ELSE
              MOVE 0 TO IND-TGA-ACQ-EXP
           END-IF
           IF TGA-REMUN-ASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-REMUN-ASS
           ELSE
              MOVE 0 TO IND-TGA-REMUN-ASS
           END-IF
           IF TGA-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-COMMIS-INTER
           ELSE
              MOVE 0 TO IND-TGA-COMMIS-INTER
           END-IF
           IF TGA-ALQ-REMUN-ASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ALQ-REMUN-ASS
           ELSE
              MOVE 0 TO IND-TGA-ALQ-REMUN-ASS
           END-IF
           IF TGA-ALQ-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-ALQ-COMMIS-INTER
           ELSE
              MOVE 0 TO IND-TGA-ALQ-COMMIS-INTER
           END-IF
           IF TGA-IMPB-REMUN-ASS-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMPB-REMUN-ASS
           ELSE
              MOVE 0 TO IND-TGA-IMPB-REMUN-ASS
           END-IF
           IF TGA-IMPB-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-IMPB-COMMIS-INTER
           ELSE
              MOVE 0 TO IND-TGA-IMPB-COMMIS-INTER
           END-IF
           IF TGA-COS-RUN-ASSVA-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-COS-RUN-ASSVA
           ELSE
              MOVE 0 TO IND-TGA-COS-RUN-ASSVA
           END-IF
           IF TGA-COS-RUN-ASSVA-IDC-NULL = HIGH-VALUES
              MOVE -1 TO IND-TGA-COS-RUN-ASSVA-IDC
           ELSE
              MOVE 0 TO IND-TGA-COS-RUN-ASSVA-IDC
           END-IF.

       Z200-EX.
           EXIT.

       Z400-SEQ-RIGA.


           EXEC SQL
              VALUES NEXTVAL FOR SEQ_RIGA
              INTO : TGA-DS-RIGA
           END-EXEC.

           PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.

       Z400-EX.
           EXIT.

       Z500-AGGIORNAMENTO-STORICO.

           MOVE TRCH-DI-GAR TO WS-BUFFER-TABLE.

           MOVE TGA-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.

           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL

              PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX

              IF IDSV0003-SUCCESSFUL-SQL

                 MOVE WS-ID-MOVI-CRZ   TO TGA-ID-MOVI-CHIU

                 MOVE WS-TS-COMPETENZA-AGG-STOR
                                       TO TGA-DS-TS-END-CPTZ

                 PERFORM A330-UPDATE-ID-EFF THRU A330-EX

                 IF IDSV0003-SUCCESSFUL-SQL

                       MOVE WS-ID-MOVI-CRZ TO TGA-ID-MOVI-CRZ

                       MOVE HIGH-VALUES    TO TGA-ID-MOVI-CHIU-NULL

                       MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                           TO TGA-DT-END-EFF

                       MOVE WS-TS-COMPETENZA-AGG-STOR
                                           TO TGA-DS-TS-INI-CPTZ
                       MOVE WS-TS-INFINITO
                                           TO TGA-DS-TS-END-CPTZ

                       IF IDSV0003-SUCCESSFUL-SQL

                          PERFORM A220-INSERT-PK THRU A220-EX

                       END-IF

                 END-IF
              END-IF
           END-PERFORM.

           IF IDSV0003-NOT-FOUND

              IF NOT IDSV0003-DELETE-LOGICA

                 PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX

              ELSE

                 SET IDSV0003-SUCCESSFUL-SQL TO TRUE

              END-IF

           END-IF.
       Z500-EX.
           EXIT.

       Z550-AGG-STORICO-SOLO-INS.

           MOVE TRCH-DI-GAR TO WS-BUFFER-TABLE.

           MOVE TGA-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.

           PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.

       Z550-EX.
           EXIT.

       Z600-INSERT-NUOVA-RIGA-STORICA.

           MOVE WS-BUFFER-TABLE TO TRCH-DI-GAR.

           MOVE WS-ID-MOVI-CRZ  TO TGA-ID-MOVI-CRZ.

           MOVE HIGH-VALUES     TO TGA-ID-MOVI-CHIU-NULL.

           MOVE IDSV0003-DATA-INIZIO-EFFETTO
                                TO TGA-DT-INI-EFF.
           MOVE WS-DT-INFINITO
                                TO TGA-DT-END-EFF.

           MOVE WS-TS-COMPETENZA-AGG-STOR
                                TO TGA-DS-TS-INI-CPTZ.
           MOVE WS-TS-INFINITO
                                TO TGA-DS-TS-END-CPTZ.

           MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
                                TO TGA-COD-COMP-ANIA.

           PERFORM A220-INSERT-PK THRU A220-EX.

       Z600-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da 9(8) comp-3 a date
      *----
       Z900-CONVERTI-N-TO-X.


           MOVE TGA-DT-INI-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO TGA-DT-INI-EFF-DB
           MOVE TGA-DT-END-EFF TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO TGA-DT-END-EFF-DB
           MOVE TGA-DT-DECOR TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X THRU Z700-EX
           MOVE WS-DATE-X      TO TGA-DT-DECOR-DB
           IF IND-TGA-DT-SCAD = 0
               MOVE TGA-DT-SCAD TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TGA-DT-SCAD-DB
           END-IF
           IF IND-TGA-DT-EMIS = 0
               MOVE TGA-DT-EMIS TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TGA-DT-EMIS-DB
           END-IF
           IF IND-TGA-DT-EFF-STAB = 0
               MOVE TGA-DT-EFF-STAB TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TGA-DT-EFF-STAB-DB
           END-IF
           IF IND-TGA-DT-VLDT-PROD = 0
               MOVE TGA-DT-VLDT-PROD TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TGA-DT-VLDT-PROD-DB
           END-IF
           IF IND-TGA-DT-INI-VAL-TAR = 0
               MOVE TGA-DT-INI-VAL-TAR TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TGA-DT-INI-VAL-TAR-DB
           END-IF
           IF IND-TGA-DT-ULT-ADEG-PRE-PR = 0
               MOVE TGA-DT-ULT-ADEG-PRE-PR TO WS-DATE-N
               PERFORM Z700-DT-N-TO-X THRU Z700-EX
               MOVE WS-DATE-X      TO TGA-DT-ULT-ADEG-PRE-PR-DB
           END-IF.

       Z900-EX.
           EXIT.
      *----
      *----  Conversione Data e Timestamp da date a 9(8) comp-3
      *----
       Z950-CONVERTI-X-TO-N.


           MOVE TGA-DT-INI-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO TGA-DT-INI-EFF
           MOVE TGA-DT-END-EFF-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO TGA-DT-END-EFF
           MOVE TGA-DT-DECOR-DB TO WS-DATE-X
           PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           MOVE WS-DATE-N      TO TGA-DT-DECOR
           IF IND-TGA-DT-SCAD = 0
               MOVE TGA-DT-SCAD-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TGA-DT-SCAD
           END-IF
           IF IND-TGA-DT-EMIS = 0
               MOVE TGA-DT-EMIS-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TGA-DT-EMIS
           END-IF
           IF IND-TGA-DT-EFF-STAB = 0
               MOVE TGA-DT-EFF-STAB-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TGA-DT-EFF-STAB
           END-IF
           IF IND-TGA-DT-VLDT-PROD = 0
               MOVE TGA-DT-VLDT-PROD-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TGA-DT-VLDT-PROD
           END-IF
           IF IND-TGA-DT-INI-VAL-TAR = 0
               MOVE TGA-DT-INI-VAL-TAR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TGA-DT-INI-VAL-TAR
           END-IF
           IF IND-TGA-DT-ULT-ADEG-PRE-PR = 0
               MOVE TGA-DT-ULT-ADEG-PRE-PR-DB TO WS-DATE-X
               PERFORM Z800-DT-X-TO-N   THRU Z800-EX
               MOVE WS-DATE-N      TO TGA-DT-ULT-ADEG-PRE-PR
           END-IF.

       Z950-EX.
           EXIT.
      *----
      *----  Calcola la lunghezza di tutti i campi VARCHAR
      *----
       Z960-LENGTH-VCHAR.

           CONTINUE.
       Z960-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *        -     GESTIONE COMPETENZA
      *----------------------------------------------------------------*
           EXEC SQL INCLUDE IDSP0003 END-EXEC.

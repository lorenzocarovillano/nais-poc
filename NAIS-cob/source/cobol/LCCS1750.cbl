       IDENTIFICATION DIVISION.
       PROGRAM-ID.        LCCS1750.
       AUTHOR.            SYNTAX VITA.
       DATE-WRITTEN.           31 OTTOBRE 2013.
      *****************************************************************
      *                                                               *
      *            R O U T I N E         B A T C H                    *
      *                                                               *
      *            ( PROGRAMMA COBOL CHIAMATO CON UNA CALL)           *
      *                                                               *
      ******************************************************************
      *
      *               ROUTINE PER IL CALCOLO DELL'ETA'
      *
      *
      *  INPUT :
      *    LCCC1751-DATA-INFERIORE-N    DATA INFERIORE FORMATO AAAAMMGG
      *    LCCC1751-DATA-SUPERIORE-N    DATA SUPERIORE FORMATO AAAAMMGG
      *
      *  OUTPUT :
      *
      *    LCCC1751-ETA-AA-ASSICURATO NUMERO ANNI E NUMERO MESI CHE
      *    LCCC1751-ETA-MM-ASSICURATO INTERCORRE TRA LE DUE DATE
      *
      *    LCCC1751-RETURN-CODE         RETURN CODE
      *    LCCC1751-DESCRIZ-ERR         DESCRIZIONE ERRORE
      ******************************************************************
       EJECT
       ENVIRONMENT             DIVISION.
       CONFIGURATION           SECTION.
       SOURCE-COMPUTER.        IBM-4341.
       OBJECT-COMPUTER.        IBM-4341.
       SKIP3
       DATA DIVISION.
       SKIP3
      *****************************************************************
      *                                                               *
      *      W O R K I N G - S T O R A G E   S E C T I O N            *
      *                                                               *
      *****************************************************************
       SKIP3
       WORKING-STORAGE SECTION.
       SKIP3
       01  FILLER           PIC X(30)         VALUE
           '***INIZIO WORKING-STORAGE***'.
       SKIP3
       01  WK-CAMPI-DI-COMODO.
           03  WK-DATA-INFERIORE.
               05 WK-AAAA-INF                 PIC 9(04).
               05 WK-MM-INF                   PIC 9(02).
               05 WK-GG-INF                   PIC 9(02).
           03  WK-DATA-INFERIORE-N REDEFINES
               WK-DATA-INFERIORE              PIC 9(08).
           03  WK-DATA-SUPERIORE.
               05 WK-AAAA-SUP                 PIC 9(04).
               05 WK-MM-SUP                   PIC 9(02).
               05 WK-GG-SUP                   PIC 9(02).
           03  WK-DATA-SUPERIORE-N REDEFINES
               WK-DATA-SUPERIORE              PIC 9(08).


      *-- AREA ROUTINE PER IL CALCOLO
      *
           03 WK-CALL-PGM                    PIC X(08) VALUE 'LCCS0010'.
           03 WK-COD-SERVIZIO-BE             PIC X(08).
           03 WK-AA-DIFF                     PIC 9(05).
           03 WK-RESTO                       PIC 9(002) VALUE ZERO.
           03 WK-INTERO                      PIC 9(005) VALUE ZERO.

           03  WK-FORMATO                    PIC X(01) VALUE 'M'.
           03  WK-CODICE-RITORNO             PIC X(01).
           03  WK-NUM-GIORNI                 PIC S9(9) COMP-3.
           03  WK-MM-DIFF                    PIC 9(05).
           03  WK-RETURN-CODE                PIC  X(002).
               88 WK-INVALID-OPER            VALUE 'D2'.
               88 WK-FIELD-NOT-VALUED        VALUE 'C1'.

       01  WK-TAB-MESI.
           05  FILLER         PIC X(12) VALUE 'GENNAIO   31'.
           05  FILLER         PIC X(12) VALUE 'FEBBRAIO  28'.
           05  FILLER         PIC X(12) VALUE 'MARZO     31'.
           05  FILLER         PIC X(12) VALUE 'APRILE    30'.
           05  FILLER         PIC X(12) VALUE 'MAGGIO    31'.
           05  FILLER         PIC X(12) VALUE 'GIUGNO    30'.
           05  FILLER         PIC X(12) VALUE 'LUGLIO    31'.
           05  FILLER         PIC X(12) VALUE 'AGOSTO    31'.
           05  FILLER         PIC X(12) VALUE 'SETTEMBRE 30'.
           05  FILLER         PIC X(12) VALUE 'OTTOBRE   31'.
           05  FILLER         PIC X(12) VALUE 'NOVEMBRE  30'.
           05  FILLER         PIC X(12) VALUE 'DICEMBRE  31'.
       01  WK-TAB-MESI-R REDEFINES WK-TAB-MESI.
           05  WK-ELE-TAB-R       OCCURS 12.
               10  WK-MESE                 PIC X(10).
               10  WK-GIORNO-FINE-MESE PIC 9(02).

       EJECT
      *****************************************************************
      **                L I N K A G E    S E C T I O N               **
      *****************************************************************
       SKIP3
       LINKAGE SECTION.
           COPY LCCC1751.
      *****************************************************************
      *                                                               *
      *             P R O C E D U R E   D I V I S I O N               *
      *                                                               *
      *****************************************************************
       SKIP3
       PROCEDURE DIVISION USING LCCC1751.
       INIZIO.
      *
           MOVE ZERO                       TO LCCC1751-ETA-AA-ASSICURATO
                                              LCCC1751-ETA-MM-ASSICURATO
           MOVE SPACES                     TO LCCC1751-RETURN-CODE
                                              WK-RETURN-CODE
                                              LCCC1751-DESCRIZ-ERR

           MOVE LCCC1751-DATA-INFERIORE  TO WK-DATA-INFERIORE-N
           MOVE LCCC1751-DATA-SUPERIORE  TO WK-DATA-SUPERIORE-N

           CALL WK-CALL-PGM      USING   WK-FORMATO,
                                         WK-DATA-INFERIORE,
                                         WK-DATA-SUPERIORE,
                                         WK-MM-DIFF,
                                         WK-CODICE-RITORNO
           ON EXCEPTION
              MOVE WK-CALL-PGM
                TO WK-COD-SERVIZIO-BE
             STRING 'ERRORE CHIAMATA '
                WK-CALL-PGM
                DELIMITED BY SIZE
                INTO LCCC1751-DESCRIZ-ERR
                SET WK-INVALID-OPER      TO TRUE
           END-CALL.
      *
           IF WK-CODICE-RITORNO EQUAL ZERO

              IF  WK-GG-SUP < WK-GG-INF
                  SUBTRACT  1            FROM   WK-MM-DIFF
              END-IF

              DIVIDE WK-MM-DIFF BY 12 GIVING WK-INTERO
                                   REMAINDER WK-RESTO

              MOVE  WK-INTERO            TO LCCC1751-ETA-AA-ASSICURATO
              MOVE  WK-RESTO             TO LCCC1751-ETA-MM-ASSICURATO
           ELSE
              MOVE WK-CALL-PGM           TO WK-COD-SERVIZIO-BE
              STRING 'CHIAMATA LCCS0010 COD-RIT:'
                      WK-CODICE-RITORNO ';'
                 DELIMITED BY SIZE INTO LCCC1751-DESCRIZ-ERR
              END-STRING
              SET WK-FIELD-NOT-VALUED TO TRUE
           END-IF.

           MOVE  WK-RETURN-CODE          TO LCCC1751-RETURN-CODE.

           GOBACK.




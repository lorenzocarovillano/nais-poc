      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA TRCH_DI_GAR
      *   ALIAS TGA
      *   ULTIMO AGG. 03 GIU 2019
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-TRCH-DI-GAR PIC S9(9)     COMP-3.
             07 (SF)-ID-GAR PIC S9(9)     COMP-3.
             07 (SF)-ID-ADES PIC S9(9)     COMP-3.
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-DT-DECOR   PIC S9(8) COMP-3.
             07 (SF)-DT-SCAD   PIC S9(8) COMP-3.
             07 (SF)-DT-SCAD-NULL REDEFINES
                (SF)-DT-SCAD   PIC X(5).
             07 (SF)-IB-OGG PIC X(40).
             07 (SF)-IB-OGG-NULL REDEFINES
                (SF)-IB-OGG   PIC X(40).
             07 (SF)-TP-RGM-FISC PIC X(2).
             07 (SF)-DT-EMIS   PIC S9(8) COMP-3.
             07 (SF)-DT-EMIS-NULL REDEFINES
                (SF)-DT-EMIS   PIC X(5).
             07 (SF)-TP-TRCH PIC X(2).
             07 (SF)-DUR-AA PIC S9(5)     COMP-3.
             07 (SF)-DUR-AA-NULL REDEFINES
                (SF)-DUR-AA   PIC X(3).
             07 (SF)-DUR-MM PIC S9(5)     COMP-3.
             07 (SF)-DUR-MM-NULL REDEFINES
                (SF)-DUR-MM   PIC X(3).
             07 (SF)-DUR-GG PIC S9(5)     COMP-3.
             07 (SF)-DUR-GG-NULL REDEFINES
                (SF)-DUR-GG   PIC X(3).
             07 (SF)-PRE-CASO-MOR PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-CASO-MOR-NULL REDEFINES
                (SF)-PRE-CASO-MOR   PIC X(8).
             07 (SF)-PC-INTR-RIAT PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-INTR-RIAT-NULL REDEFINES
                (SF)-PC-INTR-RIAT   PIC X(4).
             07 (SF)-IMP-BNS-ANTIC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-BNS-ANTIC-NULL REDEFINES
                (SF)-IMP-BNS-ANTIC   PIC X(8).
             07 (SF)-PRE-INI-NET PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-INI-NET-NULL REDEFINES
                (SF)-PRE-INI-NET   PIC X(8).
             07 (SF)-PRE-PP-INI PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-PP-INI-NULL REDEFINES
                (SF)-PRE-PP-INI   PIC X(8).
             07 (SF)-PRE-PP-ULT PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-PP-ULT-NULL REDEFINES
                (SF)-PRE-PP-ULT   PIC X(8).
             07 (SF)-PRE-TARI-INI PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-TARI-INI-NULL REDEFINES
                (SF)-PRE-TARI-INI   PIC X(8).
             07 (SF)-PRE-TARI-ULT PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-TARI-ULT-NULL REDEFINES
                (SF)-PRE-TARI-ULT   PIC X(8).
             07 (SF)-PRE-INVRIO-INI PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-INVRIO-INI-NULL REDEFINES
                (SF)-PRE-INVRIO-INI   PIC X(8).
             07 (SF)-PRE-INVRIO-ULT PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-INVRIO-ULT-NULL REDEFINES
                (SF)-PRE-INVRIO-ULT   PIC X(8).
             07 (SF)-PRE-RIVTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-RIVTO-NULL REDEFINES
                (SF)-PRE-RIVTO   PIC X(8).
             07 (SF)-IMP-SOPR-PROF PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-SOPR-PROF-NULL REDEFINES
                (SF)-IMP-SOPR-PROF   PIC X(8).
             07 (SF)-IMP-SOPR-SAN PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-SOPR-SAN-NULL REDEFINES
                (SF)-IMP-SOPR-SAN   PIC X(8).
             07 (SF)-IMP-SOPR-SPO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-SOPR-SPO-NULL REDEFINES
                (SF)-IMP-SOPR-SPO   PIC X(8).
             07 (SF)-IMP-SOPR-TEC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-SOPR-TEC-NULL REDEFINES
                (SF)-IMP-SOPR-TEC   PIC X(8).
             07 (SF)-IMP-ALT-SOPR PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-ALT-SOPR-NULL REDEFINES
                (SF)-IMP-ALT-SOPR   PIC X(8).
             07 (SF)-PRE-STAB PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-STAB-NULL REDEFINES
                (SF)-PRE-STAB   PIC X(8).
             07 (SF)-DT-EFF-STAB   PIC S9(8) COMP-3.
             07 (SF)-DT-EFF-STAB-NULL REDEFINES
                (SF)-DT-EFF-STAB   PIC X(5).
             07 (SF)-TS-RIVAL-FIS PIC S9(5)V9(9) COMP-3.
             07 (SF)-TS-RIVAL-FIS-NULL REDEFINES
                (SF)-TS-RIVAL-FIS   PIC X(8).
             07 (SF)-TS-RIVAL-INDICIZ PIC S9(5)V9(9) COMP-3.
             07 (SF)-TS-RIVAL-INDICIZ-NULL REDEFINES
                (SF)-TS-RIVAL-INDICIZ   PIC X(8).
             07 (SF)-OLD-TS-TEC PIC S9(5)V9(9) COMP-3.
             07 (SF)-OLD-TS-TEC-NULL REDEFINES
                (SF)-OLD-TS-TEC   PIC X(8).
             07 (SF)-RAT-LRD PIC S9(12)V9(3) COMP-3.
             07 (SF)-RAT-LRD-NULL REDEFINES
                (SF)-RAT-LRD   PIC X(8).
             07 (SF)-PRE-LRD PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-LRD-NULL REDEFINES
                (SF)-PRE-LRD   PIC X(8).
             07 (SF)-PRSTZ-INI PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-INI-NULL REDEFINES
                (SF)-PRSTZ-INI   PIC X(8).
             07 (SF)-PRSTZ-ULT PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-ULT-NULL REDEFINES
                (SF)-PRSTZ-ULT   PIC X(8).
             07 (SF)-CPT-IN-OPZ-RIVTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-CPT-IN-OPZ-RIVTO-NULL REDEFINES
                (SF)-CPT-IN-OPZ-RIVTO   PIC X(8).
             07 (SF)-PRSTZ-INI-STAB PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-INI-STAB-NULL REDEFINES
                (SF)-PRSTZ-INI-STAB   PIC X(8).
             07 (SF)-CPT-RSH-MOR PIC S9(12)V9(3) COMP-3.
             07 (SF)-CPT-RSH-MOR-NULL REDEFINES
                (SF)-CPT-RSH-MOR   PIC X(8).
             07 (SF)-PRSTZ-RID-INI PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-RID-INI-NULL REDEFINES
                (SF)-PRSTZ-RID-INI   PIC X(8).
             07 (SF)-FL-CAR-CONT PIC X(1).
             07 (SF)-FL-CAR-CONT-NULL REDEFINES
                (SF)-FL-CAR-CONT   PIC X(1).
             07 (SF)-BNS-GIA-LIQTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-BNS-GIA-LIQTO-NULL REDEFINES
                (SF)-BNS-GIA-LIQTO   PIC X(8).
             07 (SF)-IMP-BNS PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-BNS-NULL REDEFINES
                (SF)-IMP-BNS   PIC X(8).
             07 (SF)-COD-DVS PIC X(20).
             07 (SF)-PRSTZ-INI-NEWFIS PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-INI-NEWFIS-NULL REDEFINES
                (SF)-PRSTZ-INI-NEWFIS   PIC X(8).
             07 (SF)-IMP-SCON PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-SCON-NULL REDEFINES
                (SF)-IMP-SCON   PIC X(8).
             07 (SF)-ALQ-SCON PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-SCON-NULL REDEFINES
                (SF)-ALQ-SCON   PIC X(4).
             07 (SF)-IMP-CAR-ACQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CAR-ACQ-NULL REDEFINES
                (SF)-IMP-CAR-ACQ   PIC X(8).
             07 (SF)-IMP-CAR-INC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CAR-INC-NULL REDEFINES
                (SF)-IMP-CAR-INC   PIC X(8).
             07 (SF)-IMP-CAR-GEST PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CAR-GEST-NULL REDEFINES
                (SF)-IMP-CAR-GEST   PIC X(8).
             07 (SF)-ETA-AA-1O-ASSTO PIC S9(3)     COMP-3.
             07 (SF)-ETA-AA-1O-ASSTO-NULL REDEFINES
                (SF)-ETA-AA-1O-ASSTO   PIC X(2).
             07 (SF)-ETA-MM-1O-ASSTO PIC S9(3)     COMP-3.
             07 (SF)-ETA-MM-1O-ASSTO-NULL REDEFINES
                (SF)-ETA-MM-1O-ASSTO   PIC X(2).
             07 (SF)-ETA-AA-2O-ASSTO PIC S9(3)     COMP-3.
             07 (SF)-ETA-AA-2O-ASSTO-NULL REDEFINES
                (SF)-ETA-AA-2O-ASSTO   PIC X(2).
             07 (SF)-ETA-MM-2O-ASSTO PIC S9(3)     COMP-3.
             07 (SF)-ETA-MM-2O-ASSTO-NULL REDEFINES
                (SF)-ETA-MM-2O-ASSTO   PIC X(2).
             07 (SF)-ETA-AA-3O-ASSTO PIC S9(3)     COMP-3.
             07 (SF)-ETA-AA-3O-ASSTO-NULL REDEFINES
                (SF)-ETA-AA-3O-ASSTO   PIC X(2).
             07 (SF)-ETA-MM-3O-ASSTO PIC S9(3)     COMP-3.
             07 (SF)-ETA-MM-3O-ASSTO-NULL REDEFINES
                (SF)-ETA-MM-3O-ASSTO   PIC X(2).
             07 (SF)-RENDTO-LRD PIC S9(5)V9(9) COMP-3.
             07 (SF)-RENDTO-LRD-NULL REDEFINES
                (SF)-RENDTO-LRD   PIC X(8).
             07 (SF)-PC-RETR PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-RETR-NULL REDEFINES
                (SF)-PC-RETR   PIC X(4).
             07 (SF)-RENDTO-RETR PIC S9(5)V9(9) COMP-3.
             07 (SF)-RENDTO-RETR-NULL REDEFINES
                (SF)-RENDTO-RETR   PIC X(8).
             07 (SF)-MIN-GARTO PIC S9(5)V9(9) COMP-3.
             07 (SF)-MIN-GARTO-NULL REDEFINES
                (SF)-MIN-GARTO   PIC X(8).
             07 (SF)-MIN-TRNUT PIC S9(5)V9(9) COMP-3.
             07 (SF)-MIN-TRNUT-NULL REDEFINES
                (SF)-MIN-TRNUT   PIC X(8).
             07 (SF)-PRE-ATT-DI-TRCH PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-ATT-DI-TRCH-NULL REDEFINES
                (SF)-PRE-ATT-DI-TRCH   PIC X(8).
             07 (SF)-MATU-END2000 PIC S9(12)V9(3) COMP-3.
             07 (SF)-MATU-END2000-NULL REDEFINES
                (SF)-MATU-END2000   PIC X(8).
             07 (SF)-ABB-TOT-INI PIC S9(12)V9(3) COMP-3.
             07 (SF)-ABB-TOT-INI-NULL REDEFINES
                (SF)-ABB-TOT-INI   PIC X(8).
             07 (SF)-ABB-TOT-ULT PIC S9(12)V9(3) COMP-3.
             07 (SF)-ABB-TOT-ULT-NULL REDEFINES
                (SF)-ABB-TOT-ULT   PIC X(8).
             07 (SF)-ABB-ANNU-ULT PIC S9(12)V9(3) COMP-3.
             07 (SF)-ABB-ANNU-ULT-NULL REDEFINES
                (SF)-ABB-ANNU-ULT   PIC X(8).
             07 (SF)-DUR-ABB PIC S9(6)     COMP-3.
             07 (SF)-DUR-ABB-NULL REDEFINES
                (SF)-DUR-ABB   PIC X(4).
             07 (SF)-TP-ADEG-ABB PIC X(1).
             07 (SF)-TP-ADEG-ABB-NULL REDEFINES
                (SF)-TP-ADEG-ABB   PIC X(1).
             07 (SF)-MOD-CALC PIC X(2).
             07 (SF)-MOD-CALC-NULL REDEFINES
                (SF)-MOD-CALC   PIC X(2).
             07 (SF)-IMP-AZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-AZ-NULL REDEFINES
                (SF)-IMP-AZ   PIC X(8).
             07 (SF)-IMP-ADER PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-ADER-NULL REDEFINES
                (SF)-IMP-ADER   PIC X(8).
             07 (SF)-IMP-TFR PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-TFR-NULL REDEFINES
                (SF)-IMP-TFR   PIC X(8).
             07 (SF)-IMP-VOLO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-VOLO-NULL REDEFINES
                (SF)-IMP-VOLO   PIC X(8).
             07 (SF)-VIS-END2000 PIC S9(12)V9(3) COMP-3.
             07 (SF)-VIS-END2000-NULL REDEFINES
                (SF)-VIS-END2000   PIC X(8).
             07 (SF)-DT-VLDT-PROD   PIC S9(8) COMP-3.
             07 (SF)-DT-VLDT-PROD-NULL REDEFINES
                (SF)-DT-VLDT-PROD   PIC X(5).
             07 (SF)-DT-INI-VAL-TAR   PIC S9(8) COMP-3.
             07 (SF)-DT-INI-VAL-TAR-NULL REDEFINES
                (SF)-DT-INI-VAL-TAR   PIC X(5).
             07 (SF)-IMPB-VIS-END2000 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-END2000-NULL REDEFINES
                (SF)-IMPB-VIS-END2000   PIC X(8).
             07 (SF)-REN-INI-TS-TEC-0 PIC S9(12)V9(3) COMP-3.
             07 (SF)-REN-INI-TS-TEC-0-NULL REDEFINES
                (SF)-REN-INI-TS-TEC-0   PIC X(8).
             07 (SF)-PC-RIP-PRE PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-RIP-PRE-NULL REDEFINES
                (SF)-PC-RIP-PRE   PIC X(4).
             07 (SF)-FL-IMPORTI-FORZ PIC X(1).
             07 (SF)-FL-IMPORTI-FORZ-NULL REDEFINES
                (SF)-FL-IMPORTI-FORZ   PIC X(1).
             07 (SF)-PRSTZ-INI-NFORZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-INI-NFORZ-NULL REDEFINES
                (SF)-PRSTZ-INI-NFORZ   PIC X(8).
             07 (SF)-VIS-END2000-NFORZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-VIS-END2000-NFORZ-NULL REDEFINES
                (SF)-VIS-END2000-NFORZ   PIC X(8).
             07 (SF)-INTR-MORA PIC S9(12)V9(3) COMP-3.
             07 (SF)-INTR-MORA-NULL REDEFINES
                (SF)-INTR-MORA   PIC X(8).
             07 (SF)-MANFEE-ANTIC PIC S9(12)V9(3) COMP-3.
             07 (SF)-MANFEE-ANTIC-NULL REDEFINES
                (SF)-MANFEE-ANTIC   PIC X(8).
             07 (SF)-MANFEE-RICOR PIC S9(12)V9(3) COMP-3.
             07 (SF)-MANFEE-RICOR-NULL REDEFINES
                (SF)-MANFEE-RICOR   PIC X(8).
             07 (SF)-PRE-UNI-RIVTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-UNI-RIVTO-NULL REDEFINES
                (SF)-PRE-UNI-RIVTO   PIC X(8).
             07 (SF)-PROV-1AA-ACQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-1AA-ACQ-NULL REDEFINES
                (SF)-PROV-1AA-ACQ   PIC X(8).
             07 (SF)-PROV-2AA-ACQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-2AA-ACQ-NULL REDEFINES
                (SF)-PROV-2AA-ACQ   PIC X(8).
             07 (SF)-PROV-RICOR PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-RICOR-NULL REDEFINES
                (SF)-PROV-RICOR   PIC X(8).
             07 (SF)-PROV-INC PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-INC-NULL REDEFINES
                (SF)-PROV-INC   PIC X(8).
             07 (SF)-ALQ-PROV-ACQ PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-PROV-ACQ-NULL REDEFINES
                (SF)-ALQ-PROV-ACQ   PIC X(4).
             07 (SF)-ALQ-PROV-INC PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-PROV-INC-NULL REDEFINES
                (SF)-ALQ-PROV-INC   PIC X(4).
             07 (SF)-ALQ-PROV-RICOR PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-PROV-RICOR-NULL REDEFINES
                (SF)-ALQ-PROV-RICOR   PIC X(4).
             07 (SF)-IMPB-PROV-ACQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-PROV-ACQ-NULL REDEFINES
                (SF)-IMPB-PROV-ACQ   PIC X(8).
             07 (SF)-IMPB-PROV-INC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-PROV-INC-NULL REDEFINES
                (SF)-IMPB-PROV-INC   PIC X(8).
             07 (SF)-IMPB-PROV-RICOR PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-PROV-RICOR-NULL REDEFINES
                (SF)-IMPB-PROV-RICOR   PIC X(8).
             07 (SF)-FL-PROV-FORZ PIC X(1).
             07 (SF)-FL-PROV-FORZ-NULL REDEFINES
                (SF)-FL-PROV-FORZ   PIC X(1).
             07 (SF)-PRSTZ-AGG-INI PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-AGG-INI-NULL REDEFINES
                (SF)-PRSTZ-AGG-INI   PIC X(8).
             07 (SF)-INCR-PRE PIC S9(12)V9(3) COMP-3.
             07 (SF)-INCR-PRE-NULL REDEFINES
                (SF)-INCR-PRE   PIC X(8).
             07 (SF)-INCR-PRSTZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-INCR-PRSTZ-NULL REDEFINES
                (SF)-INCR-PRSTZ   PIC X(8).
             07 (SF)-DT-ULT-ADEG-PRE-PR   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ADEG-PRE-PR-NULL REDEFINES
                (SF)-DT-ULT-ADEG-PRE-PR   PIC X(5).
             07 (SF)-PRSTZ-AGG-ULT PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-AGG-ULT-NULL REDEFINES
                (SF)-PRSTZ-AGG-ULT   PIC X(8).
             07 (SF)-TS-RIVAL-NET PIC S9(5)V9(9) COMP-3.
             07 (SF)-TS-RIVAL-NET-NULL REDEFINES
                (SF)-TS-RIVAL-NET   PIC X(8).
             07 (SF)-PRE-PATTUITO PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-PATTUITO-NULL REDEFINES
                (SF)-PRE-PATTUITO   PIC X(8).
             07 (SF)-TP-RIVAL PIC X(2).
             07 (SF)-TP-RIVAL-NULL REDEFINES
                (SF)-TP-RIVAL   PIC X(2).
             07 (SF)-RIS-MAT PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-MAT-NULL REDEFINES
                (SF)-RIS-MAT   PIC X(8).
             07 (SF)-CPT-MIN-SCAD PIC S9(12)V9(3) COMP-3.
             07 (SF)-CPT-MIN-SCAD-NULL REDEFINES
                (SF)-CPT-MIN-SCAD   PIC X(8).
             07 (SF)-COMMIS-GEST PIC S9(12)V9(3) COMP-3.
             07 (SF)-COMMIS-GEST-NULL REDEFINES
                (SF)-COMMIS-GEST   PIC X(8).
             07 (SF)-TP-MANFEE-APPL PIC X(2).
             07 (SF)-TP-MANFEE-APPL-NULL REDEFINES
                (SF)-TP-MANFEE-APPL   PIC X(2).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-PC-COMMIS-GEST PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-COMMIS-GEST-NULL REDEFINES
                (SF)-PC-COMMIS-GEST   PIC X(4).
             07 (SF)-NUM-GG-RIVAL PIC S9(5)     COMP-3.
             07 (SF)-NUM-GG-RIVAL-NULL REDEFINES
                (SF)-NUM-GG-RIVAL   PIC X(3).
             07 (SF)-IMP-TRASFE PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-TRASFE-NULL REDEFINES
                (SF)-IMP-TRASFE   PIC X(8).
             07 (SF)-IMP-TFR-STRC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-TFR-STRC-NULL REDEFINES
                (SF)-IMP-TFR-STRC   PIC X(8).
             07 (SF)-ACQ-EXP PIC S9(12)V9(3) COMP-3.
             07 (SF)-ACQ-EXP-NULL REDEFINES
                (SF)-ACQ-EXP   PIC X(8).
             07 (SF)-REMUN-ASS PIC S9(12)V9(3) COMP-3.
             07 (SF)-REMUN-ASS-NULL REDEFINES
                (SF)-REMUN-ASS   PIC X(8).
             07 (SF)-COMMIS-INTER PIC S9(12)V9(3) COMP-3.
             07 (SF)-COMMIS-INTER-NULL REDEFINES
                (SF)-COMMIS-INTER   PIC X(8).
             07 (SF)-ALQ-REMUN-ASS PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-REMUN-ASS-NULL REDEFINES
                (SF)-ALQ-REMUN-ASS   PIC X(4).
             07 (SF)-ALQ-COMMIS-INTER PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-COMMIS-INTER-NULL REDEFINES
                (SF)-ALQ-COMMIS-INTER   PIC X(4).
             07 (SF)-IMPB-REMUN-ASS PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-REMUN-ASS-NULL REDEFINES
                (SF)-IMPB-REMUN-ASS   PIC X(8).
             07 (SF)-IMPB-COMMIS-INTER PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-COMMIS-INTER-NULL REDEFINES
                (SF)-IMPB-COMMIS-INTER   PIC X(8).
             07 (SF)-COS-RUN-ASSVA PIC S9(12)V9(3) COMP-3.
             07 (SF)-COS-RUN-ASSVA-NULL REDEFINES
                (SF)-COS-RUN-ASSVA   PIC X(8).
             07 (SF)-COS-RUN-ASSVA-IDC PIC S9(12)V9(3) COMP-3.
             07 (SF)-COS-RUN-ASSVA-IDC-NULL REDEFINES
                (SF)-COS-RUN-ASSVA-IDC   PIC X(8).


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVL195
      *   ULTIMO AGG. 24 APR 2015
      *------------------------------------------------------------

       VAL-DCLGEN-L19.
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-L19)
              TO L19-COD-COMP-ANIA
           MOVE (SF)-COD-FND(IX-TAB-L19)
              TO L19-COD-FND
           MOVE (SF)-DT-QTZ(IX-TAB-L19)
              TO L19-DT-QTZ
           IF (SF)-VAL-QUO-NULL(IX-TAB-L19) = HIGH-VALUES
              MOVE (SF)-VAL-QUO-NULL(IX-TAB-L19)
              TO L19-VAL-QUO-NULL
           ELSE
              MOVE (SF)-VAL-QUO(IX-TAB-L19)
              TO L19-VAL-QUO
           END-IF
           IF (SF)-VAL-QUO-MANFEE-NULL(IX-TAB-L19) = HIGH-VALUES
              MOVE (SF)-VAL-QUO-MANFEE-NULL(IX-TAB-L19)
              TO L19-VAL-QUO-MANFEE-NULL
           ELSE
              MOVE (SF)-VAL-QUO-MANFEE(IX-TAB-L19)
              TO L19-VAL-QUO-MANFEE
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-L19)
              TO L19-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-L19) NOT NUMERIC
              MOVE 0 TO L19-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-L19)
              TO L19-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ(IX-TAB-L19) NOT NUMERIC
              MOVE 0 TO L19-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ(IX-TAB-L19)
              TO L19-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-L19)
              TO L19-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-L19)
              TO L19-DS-STATO-ELAB
           MOVE (SF)-TP-FND(IX-TAB-L19)
              TO L19-TP-FND
           IF (SF)-DT-RILEVAZIONE-NAV-NULL(IX-TAB-L19) = HIGH-VALUES
              MOVE (SF)-DT-RILEVAZIONE-NAV-NULL(IX-TAB-L19)
              TO L19-DT-RILEVAZIONE-NAV-NULL
           ELSE
             IF (SF)-DT-RILEVAZIONE-NAV(IX-TAB-L19) = ZERO
                MOVE HIGH-VALUES
                TO L19-DT-RILEVAZIONE-NAV-NULL
             ELSE
              MOVE (SF)-DT-RILEVAZIONE-NAV(IX-TAB-L19)
              TO L19-DT-RILEVAZIONE-NAV
             END-IF
           END-IF
           IF (SF)-VAL-QUO-ACQ-NULL(IX-TAB-L19) = HIGH-VALUES
              MOVE (SF)-VAL-QUO-ACQ-NULL(IX-TAB-L19)
              TO L19-VAL-QUO-ACQ-NULL
           ELSE
              MOVE (SF)-VAL-QUO-ACQ(IX-TAB-L19)
              TO L19-VAL-QUO-ACQ
           END-IF
           IF (SF)-FL-NO-NAV-NULL(IX-TAB-L19) = HIGH-VALUES
              MOVE (SF)-FL-NO-NAV-NULL(IX-TAB-L19)
              TO L19-FL-NO-NAV-NULL
           ELSE
              MOVE (SF)-FL-NO-NAV(IX-TAB-L19)
              TO L19-FL-NO-NAV
           END-IF.
       VAL-DCLGEN-L19-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY      ..... LCCVALL6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO ASSET ALLOCATION
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVALL5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN ASSET ALLOCATION (LCCVALL1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-AST-ALLOC.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE AST-ALLOC

      *--> NOME TABELLA FISICA DB
           MOVE 'AST-ALLOC'                   TO WK-TABELLA

      *--> SE LO STATUS NON E' "INVARIATO"
           IF  NOT WALL-ST-INV(IX-TAB-ALL)
           AND NOT WALL-ST-CON(IX-TAB-ALL)
           AND WALL-ELE-ASSET-ALL-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WALL-ST-ADD(IX-TAB-ALL)

                     MOVE WMOV-ID-PTF  TO ALL-ID-MOVI-CRZ

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK

                        MOVE S090-SEQ-TABELLA
                          TO WALL-ID-PTF(IX-TAB-ALL)
                             ALL-ID-AST-ALLOC

                        MOVE WALL-ID-STRA-DI-INVST(IX-TAB-ALL)
                          TO WS-ID-SDI

      *--->             Ricerca dell'ID-STRATEGIA-INVESTIMENTO
      *--->             sulla Tabella Padre ASSET ALLOCATION
                        PERFORM RICERCA-ID-SDI
                           THRU RICERCA-ID-SDI-EX

                        MOVE WS-ID-PTF
                          TO ALL-ID-STRA-DI-INVST

                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WALL-ST-MOD(IX-TAB-ALL)

                       MOVE WMOV-ID-PTF
                         TO ALL-ID-MOVI-CRZ

                       MOVE WALL-ID-AST-ALLOC(IX-TAB-ALL)
                         TO  ALL-ID-AST-ALLOC

                       MOVE WALL-ID-STRA-DI-INVST(IX-TAB-ALL)
                         TO  ALL-ID-STRA-DI-INVST

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WALL-ST-DEL(IX-TAB-ALL)

                       MOVE WALL-ID-PTF(IX-TAB-ALL)
                         TO ALL-ID-AST-ALLOC
                       MOVE WALL-ID-STRA-DI-INVST(IX-TAB-ALL)
                         TO ALL-ID-STRA-DI-INVST
                       MOVE WMOV-ID-PTF
                         TO ALL-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN ADESIONE
                 PERFORM VAL-DCLGEN-ALL
                    THRU VAL-DCLGEN-ALL-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-ALL
                    THRU VALORIZZA-AREA-DSH-ALL-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-AST-ALLOC-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-ALL.

      *--> DCLGEN TABELLA
           MOVE AST-ALLOC               TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-ALL-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    RICERCA ID-STRATEGIA INVESTIMENTO
      *----------------------------------------------------------------*
       RICERCA-ID-SDI.

           SET NON-TROVATO TO TRUE.

           PERFORM VARYING IX-RICERCA FROM 1 BY 1
                     UNTIL IX-RICERCA > WSDI-ELE-STRA-INV-MAX
                        OR TROVATO

                IF WSDI-ID-STRA-DI-INVST(IX-RICERCA) = WS-ID-SDI

                   MOVE WSDI-ID-PTF(IX-RICERCA) TO WS-ID-PTF

                   SET TROVATO TO TRUE

                END-IF

           END-PERFORM.

           MOVE ZERO TO IX-RICERCA.

       RICERCA-ID-SDI-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVALL5.

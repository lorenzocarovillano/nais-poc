       01 AST-ALLOC.
         05 ALL-ID-AST-ALLOC PIC S9(9)V     COMP-3.
         05 ALL-ID-STRA-DI-INVST PIC S9(9)V     COMP-3.
         05 ALL-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
         05 ALL-ID-MOVI-CHIU PIC S9(9)V     COMP-3.
         05 ALL-ID-MOVI-CHIU-NULL REDEFINES
            ALL-ID-MOVI-CHIU   PIC X(5).
         05 ALL-DT-INI-VLDT   PIC S9(8)V COMP-3.
         05 ALL-DT-END-VLDT   PIC S9(8)V COMP-3.
         05 ALL-DT-END-VLDT-NULL REDEFINES
            ALL-DT-END-VLDT   PIC X(5).
         05 ALL-DT-INI-EFF   PIC S9(8)V COMP-3.
         05 ALL-DT-END-EFF   PIC S9(8)V COMP-3.
         05 ALL-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 ALL-COD-FND PIC X(20).
         05 ALL-COD-FND-NULL REDEFINES
            ALL-COD-FND   PIC X(20).
         05 ALL-COD-TARI PIC X(12).
         05 ALL-COD-TARI-NULL REDEFINES
            ALL-COD-TARI   PIC X(12).
         05 ALL-TP-APPLZ-AST PIC X(2).
         05 ALL-TP-APPLZ-AST-NULL REDEFINES
            ALL-TP-APPLZ-AST   PIC X(2).
         05 ALL-PC-RIP-AST PIC S9(3)V9(3) COMP-3.
         05 ALL-PC-RIP-AST-NULL REDEFINES
            ALL-PC-RIP-AST   PIC X(4).
         05 ALL-TP-FND PIC X(1).
         05 ALL-DS-RIGA PIC S9(10)V     COMP-3.
         05 ALL-DS-OPER-SQL PIC X(1).
         05 ALL-DS-VER PIC S9(9)V     COMP-3.
         05 ALL-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
         05 ALL-DS-TS-END-CPTZ PIC S9(18)V     COMP-3.
         05 ALL-DS-UTENTE PIC X(20).
         05 ALL-DS-STATO-ELAB PIC X(1).
         05 ALL-PERIODO PIC S9(2)V     COMP-3.
         05 ALL-PERIODO-NULL REDEFINES
            ALL-PERIODO   PIC X(2).
         05 ALL-TP-RIBIL-FND PIC X(2).
         05 ALL-TP-RIBIL-FND-NULL REDEFINES
            ALL-TP-RIBIL-FND   PIC X(2).

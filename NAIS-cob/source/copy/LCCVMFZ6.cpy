      *----------------------------------------------------------------*
      *    COPY      ..... LCCVMFZ6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO MOVIMENTO FINANZIARIO
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-MOVI-FINRIO.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE MOVI-FINRIO

      *--> NOME TABELLA FISICA DB
           MOVE 'MOVI-FINRIO'               TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WMFZ-ST-INV(IX-TAB-MFZ)
              AND WMFZ-ELE-MOVI-FINRIO-MAX NOT = 0

      *--->   Impostare ID Tabella MOVIMENTO FINANZIARIO
              MOVE WMOV-ID-PTF              TO MFZ-ID-MOVI-CRZ

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WMFZ-ST-ADD(IX-TAB-MFZ)

                    PERFORM ESTR-SEQUENCE
                       THRU ESTR-SEQUENCE-EX

                    IF IDSV0001-ESITO-OK
                       MOVE S090-SEQ-TABELLA  TO WMFZ-ID-PTF(IX-TAB-MFZ)
                                                 MFZ-ID-MOVI-FINRIO
                       MOVE WADE-ID-PTF       TO MFZ-ID-ADES
                       MOVE WMOV-ID-PTF       TO MFZ-ID-MOVI-CRZ
                    END-IF

      *-->          TIPO OPERAZIONE DISPATCHER
                    SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WMFZ-ST-DEL(IX-TAB-MFZ)

                    MOVE WMFZ-ID-PTF(IX-TAB-MFZ)  TO MFZ-ID-MOVI-FINRIO
                    MOVE WADE-ID-PTF              TO MFZ-ID-ADES
                    MOVE WMOV-ID-PTF              TO MFZ-ID-MOVI-CRZ


      *-->          TIPO OPERAZIONE DISPATCHER
                    SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->        MODIFICA
                  WHEN WMFZ-ST-MOD(IX-TAB-MFZ)

                    MOVE WMFZ-ID-PTF(IX-TAB-MFZ)  TO MFZ-ID-MOVI-FINRIO
                    MOVE WADE-ID-PTF              TO MFZ-ID-ADES
                    MOVE WMOV-ID-PTF              TO MFZ-ID-MOVI-CRZ

      *-->          TIPO OPERAZIONE DISPATCHER
                    SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN MOVIMENTO FINANZIARIO
                 PERFORM VAL-DCLGEN-MFZ
                    THRU VAL-DCLGEN-MFZ-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-MFZ
                    THRU VALORIZZA-AREA-DSH-MFZ-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-MOVI-FINRIO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-MFZ.

      *--> DCLGEN TABELLA
           MOVE MOVI-FINRIO               TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID               TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT  TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-MFZ-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVMFZ5.

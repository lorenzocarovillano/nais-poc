
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVL115
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-L11.
           MOVE (SF)-ID-OGG-BLOCCO
              TO L11-ID-OGG-BLOCCO
           IF (SF)-ID-OGG-1RIO-NULL = HIGH-VALUES
              MOVE (SF)-ID-OGG-1RIO-NULL
              TO L11-ID-OGG-1RIO-NULL
           ELSE
              MOVE (SF)-ID-OGG-1RIO
              TO L11-ID-OGG-1RIO
           END-IF
           IF (SF)-TP-OGG-1RIO-NULL = HIGH-VALUES
              MOVE (SF)-TP-OGG-1RIO-NULL
              TO L11-TP-OGG-1RIO-NULL
           ELSE
              MOVE (SF)-TP-OGG-1RIO
              TO L11-TP-OGG-1RIO
           END-IF
           MOVE (SF)-COD-COMP-ANIA
              TO L11-COD-COMP-ANIA
           MOVE (SF)-TP-MOVI
              TO L11-TP-MOVI
           MOVE (SF)-TP-OGG
              TO L11-TP-OGG
           MOVE (SF)-ID-OGG
              TO L11-ID-OGG
           MOVE (SF)-DT-EFF
              TO L11-DT-EFF
           MOVE (SF)-TP-STAT-BLOCCO
              TO L11-TP-STAT-BLOCCO
           MOVE (SF)-COD-BLOCCO
              TO L11-COD-BLOCCO
           IF (SF)-ID-RICH-NULL = HIGH-VALUES
              MOVE (SF)-ID-RICH-NULL
              TO L11-ID-RICH-NULL
           ELSE
              MOVE (SF)-ID-RICH
              TO L11-ID-RICH
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO L11-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO L11-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO L11-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO L11-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO L11-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO L11-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO L11-DS-STATO-ELAB.
       VAL-DCLGEN-L11-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVP853
      *   ULTIMO AGG. 26 SET 2014
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-P85.
           MOVE P85-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-P85)
           MOVE P85-ID-POLI
             TO (SF)-ID-POLI(IX-TAB-P85)
           MOVE P85-DT-RIVAL
             TO (SF)-DT-RIVAL(IX-TAB-P85)
           MOVE P85-COD-TARI
             TO (SF)-COD-TARI(IX-TAB-P85)
           IF P85-RENDTO-LRD-NULL = HIGH-VALUES
              MOVE P85-RENDTO-LRD-NULL
                TO (SF)-RENDTO-LRD-NULL(IX-TAB-P85)
           ELSE
              MOVE P85-RENDTO-LRD
                TO (SF)-RENDTO-LRD(IX-TAB-P85)
           END-IF
           IF P85-PC-RETR-NULL = HIGH-VALUES
              MOVE P85-PC-RETR-NULL
                TO (SF)-PC-RETR-NULL(IX-TAB-P85)
           ELSE
              MOVE P85-PC-RETR
                TO (SF)-PC-RETR(IX-TAB-P85)
           END-IF
           IF P85-RENDTO-RETR-NULL = HIGH-VALUES
              MOVE P85-RENDTO-RETR-NULL
                TO (SF)-RENDTO-RETR-NULL(IX-TAB-P85)
           ELSE
              MOVE P85-RENDTO-RETR
                TO (SF)-RENDTO-RETR(IX-TAB-P85)
           END-IF
           IF P85-COMMIS-GEST-NULL = HIGH-VALUES
              MOVE P85-COMMIS-GEST-NULL
                TO (SF)-COMMIS-GEST-NULL(IX-TAB-P85)
           ELSE
              MOVE P85-COMMIS-GEST
                TO (SF)-COMMIS-GEST(IX-TAB-P85)
           END-IF
           IF P85-TS-RIVAL-NET-NULL = HIGH-VALUES
              MOVE P85-TS-RIVAL-NET-NULL
                TO (SF)-TS-RIVAL-NET-NULL(IX-TAB-P85)
           ELSE
              MOVE P85-TS-RIVAL-NET
                TO (SF)-TS-RIVAL-NET(IX-TAB-P85)
           END-IF
           IF P85-MIN-GARTO-NULL = HIGH-VALUES
              MOVE P85-MIN-GARTO-NULL
                TO (SF)-MIN-GARTO-NULL(IX-TAB-P85)
           ELSE
              MOVE P85-MIN-GARTO
                TO (SF)-MIN-GARTO(IX-TAB-P85)
           END-IF
           IF P85-MIN-TRNUT-NULL = HIGH-VALUES
              MOVE P85-MIN-TRNUT-NULL
                TO (SF)-MIN-TRNUT-NULL(IX-TAB-P85)
           ELSE
              MOVE P85-MIN-TRNUT
                TO (SF)-MIN-TRNUT(IX-TAB-P85)
           END-IF
           MOVE P85-IB-POLI
             TO (SF)-IB-POLI(IX-TAB-P85)
           MOVE P85-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-P85)
           MOVE P85-DS-VER
             TO (SF)-DS-VER(IX-TAB-P85)
           MOVE P85-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ(IX-TAB-P85)
           MOVE P85-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-P85)
           MOVE P85-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-P85).
       VALORIZZA-OUTPUT-P85-EX.
           EXIT.

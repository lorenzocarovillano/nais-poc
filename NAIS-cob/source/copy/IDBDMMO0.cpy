           EXEC SQL DECLARE MATR_MOVIMENTO TABLE
           (
             ID_MATR_MOVIMENTO   INTEGER NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             TP_MOVI_PTF         DECIMAL(5, 0) NOT NULL,
             TP_OGG              CHAR(2) NOT NULL,
             TP_FRM_ASSVA        CHAR(2),
             TP_MOVI_ACT         CHAR(5) NOT NULL,
             AMMISSIBILITA_MOVI  CHAR(1),
             SERVIZIO_CONTROLLO  CHAR(8),
             COD_PROCESSO_WF     CHAR(3)
          ) END-EXEC.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVBEL5
      *   ULTIMO AGG. 02 LUG 2014
      *------------------------------------------------------------

       VAL-DCLGEN-BEL.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-BEL)
              TO BEL-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-BEL)
              TO BEL-ID-MOVI-CHIU
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-BEL)
              TO BEL-COD-COMP-ANIA
           IF (SF)-DT-INI-EFF(IX-TAB-BEL) NOT NUMERIC
              MOVE 0 TO BEL-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-BEL)
              TO BEL-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-BEL) NOT NUMERIC
              MOVE 0 TO BEL-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-BEL)
              TO BEL-DT-END-EFF
           END-IF
           IF (SF)-ID-RAPP-ANA-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-ID-RAPP-ANA-NULL(IX-TAB-BEL)
              TO BEL-ID-RAPP-ANA-NULL
           ELSE
              MOVE (SF)-ID-RAPP-ANA(IX-TAB-BEL)
              TO BEL-ID-RAPP-ANA
           END-IF
           IF (SF)-COD-BNFICR-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-COD-BNFICR-NULL(IX-TAB-BEL)
              TO BEL-COD-BNFICR-NULL
           ELSE
              MOVE (SF)-COD-BNFICR(IX-TAB-BEL)
              TO BEL-COD-BNFICR
           END-IF
           MOVE (SF)-DESC-BNFICR(IX-TAB-BEL)
              TO BEL-DESC-BNFICR
           IF (SF)-PC-LIQ-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-PC-LIQ-NULL(IX-TAB-BEL)
              TO BEL-PC-LIQ-NULL
           ELSE
              MOVE (SF)-PC-LIQ(IX-TAB-BEL)
              TO BEL-PC-LIQ
           END-IF
           IF (SF)-ESRCN-ATTVT-IMPRS-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-ESRCN-ATTVT-IMPRS-NULL(IX-TAB-BEL)
              TO BEL-ESRCN-ATTVT-IMPRS-NULL
           ELSE
              MOVE (SF)-ESRCN-ATTVT-IMPRS(IX-TAB-BEL)
              TO BEL-ESRCN-ATTVT-IMPRS
           END-IF
           IF (SF)-TP-IND-BNFICR-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-TP-IND-BNFICR-NULL(IX-TAB-BEL)
              TO BEL-TP-IND-BNFICR-NULL
           ELSE
              MOVE (SF)-TP-IND-BNFICR(IX-TAB-BEL)
              TO BEL-TP-IND-BNFICR
           END-IF
           IF (SF)-FL-ESE-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-FL-ESE-NULL(IX-TAB-BEL)
              TO BEL-FL-ESE-NULL
           ELSE
              MOVE (SF)-FL-ESE(IX-TAB-BEL)
              TO BEL-FL-ESE
           END-IF
           IF (SF)-FL-IRREV-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-FL-IRREV-NULL(IX-TAB-BEL)
              TO BEL-FL-IRREV-NULL
           ELSE
              MOVE (SF)-FL-IRREV(IX-TAB-BEL)
              TO BEL-FL-IRREV
           END-IF
           IF (SF)-IMP-LRD-LIQTO-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-IMP-LRD-LIQTO-NULL(IX-TAB-BEL)
              TO BEL-IMP-LRD-LIQTO-NULL
           ELSE
              MOVE (SF)-IMP-LRD-LIQTO(IX-TAB-BEL)
              TO BEL-IMP-LRD-LIQTO
           END-IF
           IF (SF)-IMPST-IPT-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-IMPST-IPT-NULL(IX-TAB-BEL)
              TO BEL-IMPST-IPT-NULL
           ELSE
              MOVE (SF)-IMPST-IPT(IX-TAB-BEL)
              TO BEL-IMPST-IPT
           END-IF
           IF (SF)-IMP-NET-LIQTO-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-IMP-NET-LIQTO-NULL(IX-TAB-BEL)
              TO BEL-IMP-NET-LIQTO-NULL
           ELSE
              MOVE (SF)-IMP-NET-LIQTO(IX-TAB-BEL)
              TO BEL-IMP-NET-LIQTO
           END-IF
           IF (SF)-RIT-ACC-IPT-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-RIT-ACC-IPT-NULL(IX-TAB-BEL)
              TO BEL-RIT-ACC-IPT-NULL
           ELSE
              MOVE (SF)-RIT-ACC-IPT(IX-TAB-BEL)
              TO BEL-RIT-ACC-IPT
           END-IF
           IF (SF)-RIT-VIS-IPT-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-RIT-VIS-IPT-NULL(IX-TAB-BEL)
              TO BEL-RIT-VIS-IPT-NULL
           ELSE
              MOVE (SF)-RIT-VIS-IPT(IX-TAB-BEL)
              TO BEL-RIT-VIS-IPT
           END-IF
           IF (SF)-RIT-TFR-IPT-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-RIT-TFR-IPT-NULL(IX-TAB-BEL)
              TO BEL-RIT-TFR-IPT-NULL
           ELSE
              MOVE (SF)-RIT-TFR-IPT(IX-TAB-BEL)
              TO BEL-RIT-TFR-IPT
           END-IF
           IF (SF)-IMPST-IRPEF-IPT-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-IMPST-IRPEF-IPT-NULL(IX-TAB-BEL)
              TO BEL-IMPST-IRPEF-IPT-NULL
           ELSE
              MOVE (SF)-IMPST-IRPEF-IPT(IX-TAB-BEL)
              TO BEL-IMPST-IRPEF-IPT
           END-IF
           IF (SF)-IMPST-SOST-IPT-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-IMPST-SOST-IPT-NULL(IX-TAB-BEL)
              TO BEL-IMPST-SOST-IPT-NULL
           ELSE
              MOVE (SF)-IMPST-SOST-IPT(IX-TAB-BEL)
              TO BEL-IMPST-SOST-IPT
           END-IF
           IF (SF)-IMPST-PRVR-IPT-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-IMPST-PRVR-IPT-NULL(IX-TAB-BEL)
              TO BEL-IMPST-PRVR-IPT-NULL
           ELSE
              MOVE (SF)-IMPST-PRVR-IPT(IX-TAB-BEL)
              TO BEL-IMPST-PRVR-IPT
           END-IF
           IF (SF)-IMPST-252-IPT-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-IMPST-252-IPT-NULL(IX-TAB-BEL)
              TO BEL-IMPST-252-IPT-NULL
           ELSE
              MOVE (SF)-IMPST-252-IPT(IX-TAB-BEL)
              TO BEL-IMPST-252-IPT
           END-IF
           IF (SF)-ID-ASSTO-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-ID-ASSTO-NULL(IX-TAB-BEL)
              TO BEL-ID-ASSTO-NULL
           ELSE
              MOVE (SF)-ID-ASSTO(IX-TAB-BEL)
              TO BEL-ID-ASSTO
           END-IF
           IF (SF)-TAX-SEP-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-TAX-SEP-NULL(IX-TAB-BEL)
              TO BEL-TAX-SEP-NULL
           ELSE
              MOVE (SF)-TAX-SEP(IX-TAB-BEL)
              TO BEL-TAX-SEP
           END-IF
           IF (SF)-DT-RISERVE-SOM-P-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-DT-RISERVE-SOM-P-NULL(IX-TAB-BEL)
              TO BEL-DT-RISERVE-SOM-P-NULL
           ELSE
             IF (SF)-DT-RISERVE-SOM-P(IX-TAB-BEL) = ZERO
                MOVE HIGH-VALUES
                TO BEL-DT-RISERVE-SOM-P-NULL
             ELSE
              MOVE (SF)-DT-RISERVE-SOM-P(IX-TAB-BEL)
              TO BEL-DT-RISERVE-SOM-P
             END-IF
           END-IF
           IF (SF)-DT-VLT-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-DT-VLT-NULL(IX-TAB-BEL)
              TO BEL-DT-VLT-NULL
           ELSE
             IF (SF)-DT-VLT(IX-TAB-BEL) = ZERO
                MOVE HIGH-VALUES
                TO BEL-DT-VLT-NULL
             ELSE
              MOVE (SF)-DT-VLT(IX-TAB-BEL)
              TO BEL-DT-VLT
             END-IF
           END-IF
           IF (SF)-TP-STAT-LIQ-BNFICR-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-TP-STAT-LIQ-BNFICR-NULL(IX-TAB-BEL)
              TO BEL-TP-STAT-LIQ-BNFICR-NULL
           ELSE
              MOVE (SF)-TP-STAT-LIQ-BNFICR(IX-TAB-BEL)
              TO BEL-TP-STAT-LIQ-BNFICR
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-BEL) NOT NUMERIC
              MOVE 0 TO BEL-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-BEL)
              TO BEL-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-BEL)
              TO BEL-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-BEL) NOT NUMERIC
              MOVE 0 TO BEL-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-BEL)
              TO BEL-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-BEL) NOT NUMERIC
              MOVE 0 TO BEL-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-BEL)
              TO BEL-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-BEL) NOT NUMERIC
              MOVE 0 TO BEL-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-BEL)
              TO BEL-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-BEL)
              TO BEL-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-BEL)
              TO BEL-DS-STATO-ELAB
           IF (SF)-RICH-CALC-CNBT-INP-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-RICH-CALC-CNBT-INP-NULL(IX-TAB-BEL)
              TO BEL-RICH-CALC-CNBT-INP-NULL
           ELSE
              MOVE (SF)-RICH-CALC-CNBT-INP(IX-TAB-BEL)
              TO BEL-RICH-CALC-CNBT-INP
           END-IF
           IF (SF)-IMP-INTR-RIT-PAG-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-IMP-INTR-RIT-PAG-NULL(IX-TAB-BEL)
              TO BEL-IMP-INTR-RIT-PAG-NULL
           ELSE
              MOVE (SF)-IMP-INTR-RIT-PAG(IX-TAB-BEL)
              TO BEL-IMP-INTR-RIT-PAG
           END-IF
           IF (SF)-DT-ULT-DOCTO-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-DT-ULT-DOCTO-NULL(IX-TAB-BEL)
              TO BEL-DT-ULT-DOCTO-NULL
           ELSE
             IF (SF)-DT-ULT-DOCTO(IX-TAB-BEL) = ZERO
                MOVE HIGH-VALUES
                TO BEL-DT-ULT-DOCTO-NULL
             ELSE
              MOVE (SF)-DT-ULT-DOCTO(IX-TAB-BEL)
              TO BEL-DT-ULT-DOCTO
             END-IF
           END-IF
           IF (SF)-DT-DORMIENZA-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-DT-DORMIENZA-NULL(IX-TAB-BEL)
              TO BEL-DT-DORMIENZA-NULL
           ELSE
             IF (SF)-DT-DORMIENZA(IX-TAB-BEL) = ZERO
                MOVE HIGH-VALUES
                TO BEL-DT-DORMIENZA-NULL
             ELSE
              MOVE (SF)-DT-DORMIENZA(IX-TAB-BEL)
              TO BEL-DT-DORMIENZA
             END-IF
           END-IF
           IF (SF)-IMPST-BOLLO-TOT-V-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-IMPST-BOLLO-TOT-V-NULL(IX-TAB-BEL)
              TO BEL-IMPST-BOLLO-TOT-V-NULL
           ELSE
              MOVE (SF)-IMPST-BOLLO-TOT-V(IX-TAB-BEL)
              TO BEL-IMPST-BOLLO-TOT-V
           END-IF
           IF (SF)-IMPST-VIS-1382011-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-1382011-NULL(IX-TAB-BEL)
              TO BEL-IMPST-VIS-1382011-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-1382011(IX-TAB-BEL)
              TO BEL-IMPST-VIS-1382011
           END-IF
           IF (SF)-IMPST-SOST-1382011-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-IMPST-SOST-1382011-NULL(IX-TAB-BEL)
              TO BEL-IMPST-SOST-1382011-NULL
           ELSE
              MOVE (SF)-IMPST-SOST-1382011(IX-TAB-BEL)
              TO BEL-IMPST-SOST-1382011
           END-IF
           IF (SF)-IMPST-VIS-662014-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-662014-NULL(IX-TAB-BEL)
              TO BEL-IMPST-VIS-662014-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-662014(IX-TAB-BEL)
              TO BEL-IMPST-VIS-662014
           END-IF
           IF (SF)-IMPST-SOST-662014-NULL(IX-TAB-BEL) = HIGH-VALUES
              MOVE (SF)-IMPST-SOST-662014-NULL(IX-TAB-BEL)
              TO BEL-IMPST-SOST-662014-NULL
           ELSE
              MOVE (SF)-IMPST-SOST-662014(IX-TAB-BEL)
              TO BEL-IMPST-SOST-662014
           END-IF.
       VAL-DCLGEN-BEL-EX.
           EXIT.

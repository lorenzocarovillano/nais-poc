
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVP045
      *   ULTIMO AGG. 23 FEB 2011
      *------------------------------------------------------------

       VAL-DCLGEN-P04.
           MOVE (SF)-ID-STAT-RICH-EST
              TO P04-ID-STAT-RICH-EST
           MOVE (SF)-ID-RICH-EST
              TO P04-ID-RICH-EST
           MOVE (SF)-TS-INI-VLDT
              TO P04-TS-INI-VLDT
           MOVE (SF)-TS-END-VLDT
              TO P04-TS-END-VLDT
           MOVE (SF)-COD-COMP-ANIA
              TO P04-COD-COMP-ANIA
           MOVE (SF)-COD-PRCS
              TO P04-COD-PRCS
           MOVE (SF)-COD-ATTVT
              TO P04-COD-ATTVT
           MOVE (SF)-STAT-RICH-EST
              TO P04-STAT-RICH-EST
           IF (SF)-FL-STAT-END-NULL = HIGH-VALUES
              MOVE (SF)-FL-STAT-END-NULL
              TO P04-FL-STAT-END-NULL
           ELSE
              MOVE (SF)-FL-STAT-END
              TO P04-FL-STAT-END
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO P04-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO P04-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO P04-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO P04-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO P04-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO P04-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO P04-DS-STATO-ELAB
           IF (SF)-TP-CAUS-SCARTO-NULL = HIGH-VALUES
              MOVE (SF)-TP-CAUS-SCARTO-NULL
              TO P04-TP-CAUS-SCARTO-NULL
           ELSE
              MOVE (SF)-TP-CAUS-SCARTO
              TO P04-TP-CAUS-SCARTO
           END-IF
           MOVE (SF)-DESC-ERR
              TO P04-DESC-ERR
           MOVE (SF)-UTENTE-INS-AGG
              TO P04-UTENTE-INS-AGG
           IF (SF)-COD-ERR-SCARTO-NULL = HIGH-VALUES
              MOVE (SF)-COD-ERR-SCARTO-NULL
              TO P04-COD-ERR-SCARTO-NULL
           ELSE
              MOVE (SF)-COD-ERR-SCARTO
              TO P04-COD-ERR-SCARTO
           END-IF
           IF (SF)-ID-MOVI-CRZ-NULL = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CRZ-NULL
              TO P04-ID-MOVI-CRZ-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CRZ
              TO P04-ID-MOVI-CRZ
           END-IF.
       VAL-DCLGEN-P04-EX.
           EXIT.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA ACC_COMM
      *   ALIAS P63
      *   ULTIMO AGG. 24 GEN 2014
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-ACC-COMM PIC S9(9)     COMP-3.
             07 (SF)-TP-ACC-COMM PIC X(2).
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-PARTNER PIC S9(10)     COMP-3.
             07 (SF)-IB-ACC-COMM PIC X(40).
             07 (SF)-IB-ACC-COMM-MASTER PIC X(40).
             07 (SF)-DESC-ACC-COMM PIC X(100).
             07 (SF)-DT-FIRMA   PIC S9(8) COMP-3.
             07 (SF)-DT-INI-VLDT   PIC S9(8) COMP-3.
             07 (SF)-DT-END-VLDT   PIC S9(8) COMP-3.
             07 (SF)-FL-INVIO-CONFERME PIC X(1).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-DESC-ACC-COMM-MAST PIC X(100).

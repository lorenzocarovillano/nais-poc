
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVB035
      *   ULTIMO AGG. 12 SET 2014
      *------------------------------------------------------------

       VAL-DCLGEN-B03.
           MOVE (SF)-ID-BILA-TRCH-ESTR
              TO B03-ID-BILA-TRCH-ESTR
           MOVE (SF)-COD-COMP-ANIA
              TO B03-COD-COMP-ANIA
           MOVE (SF)-ID-RICH-ESTRAZ-MAS
              TO B03-ID-RICH-ESTRAZ-MAS
           IF (SF)-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
              MOVE (SF)-ID-RICH-ESTRAZ-AGG-NULL
              TO B03-ID-RICH-ESTRAZ-AGG-NULL
           ELSE
              MOVE (SF)-ID-RICH-ESTRAZ-AGG
              TO B03-ID-RICH-ESTRAZ-AGG
           END-IF
           IF (SF)-FL-SIMULAZIONE-NULL = HIGH-VALUES
              MOVE (SF)-FL-SIMULAZIONE-NULL
              TO B03-FL-SIMULAZIONE-NULL
           ELSE
              MOVE (SF)-FL-SIMULAZIONE
              TO B03-FL-SIMULAZIONE
           END-IF
           MOVE (SF)-DT-RIS
              TO B03-DT-RIS
           MOVE (SF)-DT-PRODUZIONE
              TO B03-DT-PRODUZIONE
           MOVE (SF)-ID-POLI
              TO B03-ID-POLI
           MOVE (SF)-ID-ADES
              TO B03-ID-ADES
           MOVE (SF)-ID-GAR
              TO B03-ID-GAR
           MOVE (SF)-ID-TRCH-DI-GAR
              TO B03-ID-TRCH-DI-GAR
           MOVE (SF)-TP-FRM-ASSVA
              TO B03-TP-FRM-ASSVA
           MOVE (SF)-TP-RAMO-BILA
              TO B03-TP-RAMO-BILA
           MOVE (SF)-TP-CALC-RIS
              TO B03-TP-CALC-RIS
           MOVE (SF)-COD-RAMO
              TO B03-COD-RAMO
           MOVE (SF)-COD-TARI
              TO B03-COD-TARI
           IF (SF)-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE (SF)-DT-INI-VAL-TAR-NULL
              TO B03-DT-INI-VAL-TAR-NULL
           ELSE
             IF (SF)-DT-INI-VAL-TAR = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-INI-VAL-TAR-NULL
             ELSE
              MOVE (SF)-DT-INI-VAL-TAR
              TO B03-DT-INI-VAL-TAR
             END-IF
           END-IF
           IF (SF)-COD-PROD-NULL = HIGH-VALUES
              MOVE (SF)-COD-PROD-NULL
              TO B03-COD-PROD-NULL
           ELSE
              MOVE (SF)-COD-PROD
              TO B03-COD-PROD
           END-IF
           MOVE (SF)-DT-INI-VLDT-PROD
              TO B03-DT-INI-VLDT-PROD
           IF (SF)-COD-TARI-ORGN-NULL = HIGH-VALUES
              MOVE (SF)-COD-TARI-ORGN-NULL
              TO B03-COD-TARI-ORGN-NULL
           ELSE
              MOVE (SF)-COD-TARI-ORGN
              TO B03-COD-TARI-ORGN
           END-IF
           IF (SF)-MIN-GARTO-T-NULL = HIGH-VALUES
              MOVE (SF)-MIN-GARTO-T-NULL
              TO B03-MIN-GARTO-T-NULL
           ELSE
              MOVE (SF)-MIN-GARTO-T
              TO B03-MIN-GARTO-T
           END-IF
           IF (SF)-TP-TARI-NULL = HIGH-VALUES
              MOVE (SF)-TP-TARI-NULL
              TO B03-TP-TARI-NULL
           ELSE
              MOVE (SF)-TP-TARI
              TO B03-TP-TARI
           END-IF
           IF (SF)-TP-PRE-NULL = HIGH-VALUES
              MOVE (SF)-TP-PRE-NULL
              TO B03-TP-PRE-NULL
           ELSE
              MOVE (SF)-TP-PRE
              TO B03-TP-PRE
           END-IF
           IF (SF)-TP-ADEG-PRE-NULL = HIGH-VALUES
              MOVE (SF)-TP-ADEG-PRE-NULL
              TO B03-TP-ADEG-PRE-NULL
           ELSE
              MOVE (SF)-TP-ADEG-PRE
              TO B03-TP-ADEG-PRE
           END-IF
           IF (SF)-TP-RIVAL-NULL = HIGH-VALUES
              MOVE (SF)-TP-RIVAL-NULL
              TO B03-TP-RIVAL-NULL
           ELSE
              MOVE (SF)-TP-RIVAL
              TO B03-TP-RIVAL
           END-IF
           IF (SF)-FL-DA-TRASF-NULL = HIGH-VALUES
              MOVE (SF)-FL-DA-TRASF-NULL
              TO B03-FL-DA-TRASF-NULL
           ELSE
              MOVE (SF)-FL-DA-TRASF
              TO B03-FL-DA-TRASF
           END-IF
           IF (SF)-FL-CAR-CONT-NULL = HIGH-VALUES
              MOVE (SF)-FL-CAR-CONT-NULL
              TO B03-FL-CAR-CONT-NULL
           ELSE
              MOVE (SF)-FL-CAR-CONT
              TO B03-FL-CAR-CONT
           END-IF
           IF (SF)-FL-PRE-DA-RIS-NULL = HIGH-VALUES
              MOVE (SF)-FL-PRE-DA-RIS-NULL
              TO B03-FL-PRE-DA-RIS-NULL
           ELSE
              MOVE (SF)-FL-PRE-DA-RIS
              TO B03-FL-PRE-DA-RIS
           END-IF
           IF (SF)-FL-PRE-AGG-NULL = HIGH-VALUES
              MOVE (SF)-FL-PRE-AGG-NULL
              TO B03-FL-PRE-AGG-NULL
           ELSE
              MOVE (SF)-FL-PRE-AGG
              TO B03-FL-PRE-AGG
           END-IF
           IF (SF)-TP-TRCH-NULL = HIGH-VALUES
              MOVE (SF)-TP-TRCH-NULL
              TO B03-TP-TRCH-NULL
           ELSE
              MOVE (SF)-TP-TRCH
              TO B03-TP-TRCH
           END-IF
           IF (SF)-TP-TST-NULL = HIGH-VALUES
              MOVE (SF)-TP-TST-NULL
              TO B03-TP-TST-NULL
           ELSE
              MOVE (SF)-TP-TST
              TO B03-TP-TST
           END-IF
           IF (SF)-COD-CONV-NULL = HIGH-VALUES
              MOVE (SF)-COD-CONV-NULL
              TO B03-COD-CONV-NULL
           ELSE
              MOVE (SF)-COD-CONV
              TO B03-COD-CONV
           END-IF
           MOVE (SF)-DT-DECOR-POLI
              TO B03-DT-DECOR-POLI
           IF (SF)-DT-DECOR-ADES-NULL = HIGH-VALUES
              MOVE (SF)-DT-DECOR-ADES-NULL
              TO B03-DT-DECOR-ADES-NULL
           ELSE
             IF (SF)-DT-DECOR-ADES = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-DECOR-ADES-NULL
             ELSE
              MOVE (SF)-DT-DECOR-ADES
              TO B03-DT-DECOR-ADES
             END-IF
           END-IF
           MOVE (SF)-DT-DECOR-TRCH
              TO B03-DT-DECOR-TRCH
           MOVE (SF)-DT-EMIS-POLI
              TO B03-DT-EMIS-POLI
           IF (SF)-DT-EMIS-TRCH-NULL = HIGH-VALUES
              MOVE (SF)-DT-EMIS-TRCH-NULL
              TO B03-DT-EMIS-TRCH-NULL
           ELSE
             IF (SF)-DT-EMIS-TRCH = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-EMIS-TRCH-NULL
             ELSE
              MOVE (SF)-DT-EMIS-TRCH
              TO B03-DT-EMIS-TRCH
             END-IF
           END-IF
           IF (SF)-DT-SCAD-TRCH-NULL = HIGH-VALUES
              MOVE (SF)-DT-SCAD-TRCH-NULL
              TO B03-DT-SCAD-TRCH-NULL
           ELSE
             IF (SF)-DT-SCAD-TRCH = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-SCAD-TRCH-NULL
             ELSE
              MOVE (SF)-DT-SCAD-TRCH
              TO B03-DT-SCAD-TRCH
             END-IF
           END-IF
           IF (SF)-DT-SCAD-INTMD-NULL = HIGH-VALUES
              MOVE (SF)-DT-SCAD-INTMD-NULL
              TO B03-DT-SCAD-INTMD-NULL
           ELSE
             IF (SF)-DT-SCAD-INTMD = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-SCAD-INTMD-NULL
             ELSE
              MOVE (SF)-DT-SCAD-INTMD
              TO B03-DT-SCAD-INTMD
             END-IF
           END-IF
           IF (SF)-DT-SCAD-PAG-PRE-NULL = HIGH-VALUES
              MOVE (SF)-DT-SCAD-PAG-PRE-NULL
              TO B03-DT-SCAD-PAG-PRE-NULL
           ELSE
             IF (SF)-DT-SCAD-PAG-PRE = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-SCAD-PAG-PRE-NULL
             ELSE
              MOVE (SF)-DT-SCAD-PAG-PRE
              TO B03-DT-SCAD-PAG-PRE
             END-IF
           END-IF
           IF (SF)-DT-ULT-PRE-PAG-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-PRE-PAG-NULL
              TO B03-DT-ULT-PRE-PAG-NULL
           ELSE
             IF (SF)-DT-ULT-PRE-PAG = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-ULT-PRE-PAG-NULL
             ELSE
              MOVE (SF)-DT-ULT-PRE-PAG
              TO B03-DT-ULT-PRE-PAG
             END-IF
           END-IF
           IF (SF)-DT-NASC-1O-ASSTO-NULL = HIGH-VALUES
              MOVE (SF)-DT-NASC-1O-ASSTO-NULL
              TO B03-DT-NASC-1O-ASSTO-NULL
           ELSE
             IF (SF)-DT-NASC-1O-ASSTO = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-NASC-1O-ASSTO-NULL
             ELSE
              MOVE (SF)-DT-NASC-1O-ASSTO
              TO B03-DT-NASC-1O-ASSTO
             END-IF
           END-IF
           IF (SF)-SEX-1O-ASSTO-NULL = HIGH-VALUES
              MOVE (SF)-SEX-1O-ASSTO-NULL
              TO B03-SEX-1O-ASSTO-NULL
           ELSE
              MOVE (SF)-SEX-1O-ASSTO
              TO B03-SEX-1O-ASSTO
           END-IF
           IF (SF)-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE (SF)-ETA-AA-1O-ASSTO-NULL
              TO B03-ETA-AA-1O-ASSTO-NULL
           ELSE
              MOVE (SF)-ETA-AA-1O-ASSTO
              TO B03-ETA-AA-1O-ASSTO
           END-IF
           IF (SF)-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE (SF)-ETA-MM-1O-ASSTO-NULL
              TO B03-ETA-MM-1O-ASSTO-NULL
           ELSE
              MOVE (SF)-ETA-MM-1O-ASSTO
              TO B03-ETA-MM-1O-ASSTO
           END-IF
           IF (SF)-ETA-RAGGN-DT-CALC-NULL = HIGH-VALUES
              MOVE (SF)-ETA-RAGGN-DT-CALC-NULL
              TO B03-ETA-RAGGN-DT-CALC-NULL
           ELSE
              MOVE (SF)-ETA-RAGGN-DT-CALC
              TO B03-ETA-RAGGN-DT-CALC
           END-IF
           IF (SF)-DUR-AA-NULL = HIGH-VALUES
              MOVE (SF)-DUR-AA-NULL
              TO B03-DUR-AA-NULL
           ELSE
              MOVE (SF)-DUR-AA
              TO B03-DUR-AA
           END-IF
           IF (SF)-DUR-MM-NULL = HIGH-VALUES
              MOVE (SF)-DUR-MM-NULL
              TO B03-DUR-MM-NULL
           ELSE
              MOVE (SF)-DUR-MM
              TO B03-DUR-MM
           END-IF
           IF (SF)-DUR-GG-NULL = HIGH-VALUES
              MOVE (SF)-DUR-GG-NULL
              TO B03-DUR-GG-NULL
           ELSE
              MOVE (SF)-DUR-GG
              TO B03-DUR-GG
           END-IF
           IF (SF)-DUR-1O-PER-AA-NULL = HIGH-VALUES
              MOVE (SF)-DUR-1O-PER-AA-NULL
              TO B03-DUR-1O-PER-AA-NULL
           ELSE
              MOVE (SF)-DUR-1O-PER-AA
              TO B03-DUR-1O-PER-AA
           END-IF
           IF (SF)-DUR-1O-PER-MM-NULL = HIGH-VALUES
              MOVE (SF)-DUR-1O-PER-MM-NULL
              TO B03-DUR-1O-PER-MM-NULL
           ELSE
              MOVE (SF)-DUR-1O-PER-MM
              TO B03-DUR-1O-PER-MM
           END-IF
           IF (SF)-DUR-1O-PER-GG-NULL = HIGH-VALUES
              MOVE (SF)-DUR-1O-PER-GG-NULL
              TO B03-DUR-1O-PER-GG-NULL
           ELSE
              MOVE (SF)-DUR-1O-PER-GG
              TO B03-DUR-1O-PER-GG
           END-IF
           IF (SF)-ANTIDUR-RICOR-PREC-NULL = HIGH-VALUES
              MOVE (SF)-ANTIDUR-RICOR-PREC-NULL
              TO B03-ANTIDUR-RICOR-PREC-NULL
           ELSE
              MOVE (SF)-ANTIDUR-RICOR-PREC
              TO B03-ANTIDUR-RICOR-PREC
           END-IF
           IF (SF)-ANTIDUR-DT-CALC-NULL = HIGH-VALUES
              MOVE (SF)-ANTIDUR-DT-CALC-NULL
              TO B03-ANTIDUR-DT-CALC-NULL
           ELSE
              MOVE (SF)-ANTIDUR-DT-CALC
              TO B03-ANTIDUR-DT-CALC
           END-IF
           IF (SF)-DUR-RES-DT-CALC-NULL = HIGH-VALUES
              MOVE (SF)-DUR-RES-DT-CALC-NULL
              TO B03-DUR-RES-DT-CALC-NULL
           ELSE
              MOVE (SF)-DUR-RES-DT-CALC
              TO B03-DUR-RES-DT-CALC
           END-IF
           MOVE (SF)-TP-STAT-BUS-POLI
              TO B03-TP-STAT-BUS-POLI
           MOVE (SF)-TP-CAUS-POLI
              TO B03-TP-CAUS-POLI
           MOVE (SF)-TP-STAT-BUS-ADES
              TO B03-TP-STAT-BUS-ADES
           MOVE (SF)-TP-CAUS-ADES
              TO B03-TP-CAUS-ADES
           MOVE (SF)-TP-STAT-BUS-TRCH
              TO B03-TP-STAT-BUS-TRCH
           MOVE (SF)-TP-CAUS-TRCH
              TO B03-TP-CAUS-TRCH
           IF (SF)-DT-EFF-CAMB-STAT-NULL = HIGH-VALUES
              MOVE (SF)-DT-EFF-CAMB-STAT-NULL
              TO B03-DT-EFF-CAMB-STAT-NULL
           ELSE
             IF (SF)-DT-EFF-CAMB-STAT = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-EFF-CAMB-STAT-NULL
             ELSE
              MOVE (SF)-DT-EFF-CAMB-STAT
              TO B03-DT-EFF-CAMB-STAT
             END-IF
           END-IF
           IF (SF)-DT-EMIS-CAMB-STAT-NULL = HIGH-VALUES
              MOVE (SF)-DT-EMIS-CAMB-STAT-NULL
              TO B03-DT-EMIS-CAMB-STAT-NULL
           ELSE
             IF (SF)-DT-EMIS-CAMB-STAT = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-EMIS-CAMB-STAT-NULL
             ELSE
              MOVE (SF)-DT-EMIS-CAMB-STAT
              TO B03-DT-EMIS-CAMB-STAT
             END-IF
           END-IF
           IF (SF)-DT-EFF-STAB-NULL = HIGH-VALUES
              MOVE (SF)-DT-EFF-STAB-NULL
              TO B03-DT-EFF-STAB-NULL
           ELSE
             IF (SF)-DT-EFF-STAB = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-EFF-STAB-NULL
             ELSE
              MOVE (SF)-DT-EFF-STAB
              TO B03-DT-EFF-STAB
             END-IF
           END-IF
           IF (SF)-CPT-DT-STAB-NULL = HIGH-VALUES
              MOVE (SF)-CPT-DT-STAB-NULL
              TO B03-CPT-DT-STAB-NULL
           ELSE
              MOVE (SF)-CPT-DT-STAB
              TO B03-CPT-DT-STAB
           END-IF
           IF (SF)-DT-EFF-RIDZ-NULL = HIGH-VALUES
              MOVE (SF)-DT-EFF-RIDZ-NULL
              TO B03-DT-EFF-RIDZ-NULL
           ELSE
             IF (SF)-DT-EFF-RIDZ = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-EFF-RIDZ-NULL
             ELSE
              MOVE (SF)-DT-EFF-RIDZ
              TO B03-DT-EFF-RIDZ
             END-IF
           END-IF
           IF (SF)-DT-EMIS-RIDZ-NULL = HIGH-VALUES
              MOVE (SF)-DT-EMIS-RIDZ-NULL
              TO B03-DT-EMIS-RIDZ-NULL
           ELSE
             IF (SF)-DT-EMIS-RIDZ = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-EMIS-RIDZ-NULL
             ELSE
              MOVE (SF)-DT-EMIS-RIDZ
              TO B03-DT-EMIS-RIDZ
             END-IF
           END-IF
           IF (SF)-CPT-DT-RIDZ-NULL = HIGH-VALUES
              MOVE (SF)-CPT-DT-RIDZ-NULL
              TO B03-CPT-DT-RIDZ-NULL
           ELSE
              MOVE (SF)-CPT-DT-RIDZ
              TO B03-CPT-DT-RIDZ
           END-IF
           IF (SF)-FRAZ-NULL = HIGH-VALUES
              MOVE (SF)-FRAZ-NULL
              TO B03-FRAZ-NULL
           ELSE
              MOVE (SF)-FRAZ
              TO B03-FRAZ
           END-IF
           IF (SF)-DUR-PAG-PRE-NULL = HIGH-VALUES
              MOVE (SF)-DUR-PAG-PRE-NULL
              TO B03-DUR-PAG-PRE-NULL
           ELSE
              MOVE (SF)-DUR-PAG-PRE
              TO B03-DUR-PAG-PRE
           END-IF
           IF (SF)-NUM-PRE-PATT-NULL = HIGH-VALUES
              MOVE (SF)-NUM-PRE-PATT-NULL
              TO B03-NUM-PRE-PATT-NULL
           ELSE
              MOVE (SF)-NUM-PRE-PATT
              TO B03-NUM-PRE-PATT
           END-IF
           IF (SF)-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
              MOVE (SF)-FRAZ-INI-EROG-REN-NULL
              TO B03-FRAZ-INI-EROG-REN-NULL
           ELSE
              MOVE (SF)-FRAZ-INI-EROG-REN
              TO B03-FRAZ-INI-EROG-REN
           END-IF
           IF (SF)-AA-REN-CER-NULL = HIGH-VALUES
              MOVE (SF)-AA-REN-CER-NULL
              TO B03-AA-REN-CER-NULL
           ELSE
              MOVE (SF)-AA-REN-CER
              TO B03-AA-REN-CER
           END-IF
           IF (SF)-RAT-REN-NULL = HIGH-VALUES
              MOVE (SF)-RAT-REN-NULL
              TO B03-RAT-REN-NULL
           ELSE
              MOVE (SF)-RAT-REN
              TO B03-RAT-REN
           END-IF
           IF (SF)-COD-DIV-NULL = HIGH-VALUES
              MOVE (SF)-COD-DIV-NULL
              TO B03-COD-DIV-NULL
           ELSE
              MOVE (SF)-COD-DIV
              TO B03-COD-DIV
           END-IF
           IF (SF)-RISCPAR-NULL = HIGH-VALUES
              MOVE (SF)-RISCPAR-NULL
              TO B03-RISCPAR-NULL
           ELSE
              MOVE (SF)-RISCPAR
              TO B03-RISCPAR
           END-IF
           IF (SF)-CUM-RISCPAR-NULL = HIGH-VALUES
              MOVE (SF)-CUM-RISCPAR-NULL
              TO B03-CUM-RISCPAR-NULL
           ELSE
              MOVE (SF)-CUM-RISCPAR
              TO B03-CUM-RISCPAR
           END-IF
           IF (SF)-ULT-RM-NULL = HIGH-VALUES
              MOVE (SF)-ULT-RM-NULL
              TO B03-ULT-RM-NULL
           ELSE
              MOVE (SF)-ULT-RM
              TO B03-ULT-RM
           END-IF
           IF (SF)-TS-RENDTO-T-NULL = HIGH-VALUES
              MOVE (SF)-TS-RENDTO-T-NULL
              TO B03-TS-RENDTO-T-NULL
           ELSE
              MOVE (SF)-TS-RENDTO-T
              TO B03-TS-RENDTO-T
           END-IF
           IF (SF)-ALQ-RETR-T-NULL = HIGH-VALUES
              MOVE (SF)-ALQ-RETR-T-NULL
              TO B03-ALQ-RETR-T-NULL
           ELSE
              MOVE (SF)-ALQ-RETR-T
              TO B03-ALQ-RETR-T
           END-IF
           IF (SF)-MIN-TRNUT-T-NULL = HIGH-VALUES
              MOVE (SF)-MIN-TRNUT-T-NULL
              TO B03-MIN-TRNUT-T-NULL
           ELSE
              MOVE (SF)-MIN-TRNUT-T
              TO B03-MIN-TRNUT-T
           END-IF
           IF (SF)-TS-NET-T-NULL = HIGH-VALUES
              MOVE (SF)-TS-NET-T-NULL
              TO B03-TS-NET-T-NULL
           ELSE
              MOVE (SF)-TS-NET-T
              TO B03-TS-NET-T
           END-IF
           IF (SF)-DT-ULT-RIVAL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-RIVAL-NULL
              TO B03-DT-ULT-RIVAL-NULL
           ELSE
             IF (SF)-DT-ULT-RIVAL = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-ULT-RIVAL-NULL
             ELSE
              MOVE (SF)-DT-ULT-RIVAL
              TO B03-DT-ULT-RIVAL
             END-IF
           END-IF
           IF (SF)-PRSTZ-INI-NULL = HIGH-VALUES
              MOVE (SF)-PRSTZ-INI-NULL
              TO B03-PRSTZ-INI-NULL
           ELSE
              MOVE (SF)-PRSTZ-INI
              TO B03-PRSTZ-INI
           END-IF
           IF (SF)-PRSTZ-AGG-INI-NULL = HIGH-VALUES
              MOVE (SF)-PRSTZ-AGG-INI-NULL
              TO B03-PRSTZ-AGG-INI-NULL
           ELSE
              MOVE (SF)-PRSTZ-AGG-INI
              TO B03-PRSTZ-AGG-INI
           END-IF
           IF (SF)-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
              MOVE (SF)-PRSTZ-AGG-ULT-NULL
              TO B03-PRSTZ-AGG-ULT-NULL
           ELSE
              MOVE (SF)-PRSTZ-AGG-ULT
              TO B03-PRSTZ-AGG-ULT
           END-IF
           IF (SF)-RAPPEL-NULL = HIGH-VALUES
              MOVE (SF)-RAPPEL-NULL
              TO B03-RAPPEL-NULL
           ELSE
              MOVE (SF)-RAPPEL
              TO B03-RAPPEL
           END-IF
           IF (SF)-PRE-PATTUITO-INI-NULL = HIGH-VALUES
              MOVE (SF)-PRE-PATTUITO-INI-NULL
              TO B03-PRE-PATTUITO-INI-NULL
           ELSE
              MOVE (SF)-PRE-PATTUITO-INI
              TO B03-PRE-PATTUITO-INI
           END-IF
           IF (SF)-PRE-DOV-INI-NULL = HIGH-VALUES
              MOVE (SF)-PRE-DOV-INI-NULL
              TO B03-PRE-DOV-INI-NULL
           ELSE
              MOVE (SF)-PRE-DOV-INI
              TO B03-PRE-DOV-INI
           END-IF
           IF (SF)-PRE-DOV-RIVTO-T-NULL = HIGH-VALUES
              MOVE (SF)-PRE-DOV-RIVTO-T-NULL
              TO B03-PRE-DOV-RIVTO-T-NULL
           ELSE
              MOVE (SF)-PRE-DOV-RIVTO-T
              TO B03-PRE-DOV-RIVTO-T
           END-IF
           IF (SF)-PRE-ANNUALIZ-RICOR-NULL = HIGH-VALUES
              MOVE (SF)-PRE-ANNUALIZ-RICOR-NULL
              TO B03-PRE-ANNUALIZ-RICOR-NULL
           ELSE
              MOVE (SF)-PRE-ANNUALIZ-RICOR
              TO B03-PRE-ANNUALIZ-RICOR
           END-IF
           IF (SF)-PRE-CONT-NULL = HIGH-VALUES
              MOVE (SF)-PRE-CONT-NULL
              TO B03-PRE-CONT-NULL
           ELSE
              MOVE (SF)-PRE-CONT
              TO B03-PRE-CONT
           END-IF
           IF (SF)-PRE-PP-INI-NULL = HIGH-VALUES
              MOVE (SF)-PRE-PP-INI-NULL
              TO B03-PRE-PP-INI-NULL
           ELSE
              MOVE (SF)-PRE-PP-INI
              TO B03-PRE-PP-INI
           END-IF
           IF (SF)-RIS-PURA-T-NULL = HIGH-VALUES
              MOVE (SF)-RIS-PURA-T-NULL
              TO B03-RIS-PURA-T-NULL
           ELSE
              MOVE (SF)-RIS-PURA-T
              TO B03-RIS-PURA-T
           END-IF
           IF (SF)-PROV-ACQ-NULL = HIGH-VALUES
              MOVE (SF)-PROV-ACQ-NULL
              TO B03-PROV-ACQ-NULL
           ELSE
              MOVE (SF)-PROV-ACQ
              TO B03-PROV-ACQ
           END-IF
           IF (SF)-PROV-ACQ-RICOR-NULL = HIGH-VALUES
              MOVE (SF)-PROV-ACQ-RICOR-NULL
              TO B03-PROV-ACQ-RICOR-NULL
           ELSE
              MOVE (SF)-PROV-ACQ-RICOR
              TO B03-PROV-ACQ-RICOR
           END-IF
           IF (SF)-PROV-INC-NULL = HIGH-VALUES
              MOVE (SF)-PROV-INC-NULL
              TO B03-PROV-INC-NULL
           ELSE
              MOVE (SF)-PROV-INC
              TO B03-PROV-INC
           END-IF
           IF (SF)-CAR-ACQ-NON-SCON-NULL = HIGH-VALUES
              MOVE (SF)-CAR-ACQ-NON-SCON-NULL
              TO B03-CAR-ACQ-NON-SCON-NULL
           ELSE
              MOVE (SF)-CAR-ACQ-NON-SCON
              TO B03-CAR-ACQ-NON-SCON
           END-IF
           IF (SF)-OVER-COMM-NULL = HIGH-VALUES
              MOVE (SF)-OVER-COMM-NULL
              TO B03-OVER-COMM-NULL
           ELSE
              MOVE (SF)-OVER-COMM
              TO B03-OVER-COMM
           END-IF
           IF (SF)-CAR-ACQ-PRECONTATO-NULL = HIGH-VALUES
              MOVE (SF)-CAR-ACQ-PRECONTATO-NULL
              TO B03-CAR-ACQ-PRECONTATO-NULL
           ELSE
              MOVE (SF)-CAR-ACQ-PRECONTATO
              TO B03-CAR-ACQ-PRECONTATO
           END-IF
           IF (SF)-RIS-ACQ-T-NULL = HIGH-VALUES
              MOVE (SF)-RIS-ACQ-T-NULL
              TO B03-RIS-ACQ-T-NULL
           ELSE
              MOVE (SF)-RIS-ACQ-T
              TO B03-RIS-ACQ-T
           END-IF
           IF (SF)-RIS-ZIL-T-NULL = HIGH-VALUES
              MOVE (SF)-RIS-ZIL-T-NULL
              TO B03-RIS-ZIL-T-NULL
           ELSE
              MOVE (SF)-RIS-ZIL-T
              TO B03-RIS-ZIL-T
           END-IF
           IF (SF)-CAR-GEST-NON-SCON-NULL = HIGH-VALUES
              MOVE (SF)-CAR-GEST-NON-SCON-NULL
              TO B03-CAR-GEST-NON-SCON-NULL
           ELSE
              MOVE (SF)-CAR-GEST-NON-SCON
              TO B03-CAR-GEST-NON-SCON
           END-IF
           IF (SF)-CAR-GEST-NULL = HIGH-VALUES
              MOVE (SF)-CAR-GEST-NULL
              TO B03-CAR-GEST-NULL
           ELSE
              MOVE (SF)-CAR-GEST
              TO B03-CAR-GEST
           END-IF
           IF (SF)-RIS-SPE-T-NULL = HIGH-VALUES
              MOVE (SF)-RIS-SPE-T-NULL
              TO B03-RIS-SPE-T-NULL
           ELSE
              MOVE (SF)-RIS-SPE-T
              TO B03-RIS-SPE-T
           END-IF
           IF (SF)-CAR-INC-NON-SCON-NULL = HIGH-VALUES
              MOVE (SF)-CAR-INC-NON-SCON-NULL
              TO B03-CAR-INC-NON-SCON-NULL
           ELSE
              MOVE (SF)-CAR-INC-NON-SCON
              TO B03-CAR-INC-NON-SCON
           END-IF
           IF (SF)-CAR-INC-NULL = HIGH-VALUES
              MOVE (SF)-CAR-INC-NULL
              TO B03-CAR-INC-NULL
           ELSE
              MOVE (SF)-CAR-INC
              TO B03-CAR-INC
           END-IF
           IF (SF)-RIS-RISTORNI-CAP-NULL = HIGH-VALUES
              MOVE (SF)-RIS-RISTORNI-CAP-NULL
              TO B03-RIS-RISTORNI-CAP-NULL
           ELSE
              MOVE (SF)-RIS-RISTORNI-CAP
              TO B03-RIS-RISTORNI-CAP
           END-IF
           IF (SF)-INTR-TECN-NULL = HIGH-VALUES
              MOVE (SF)-INTR-TECN-NULL
              TO B03-INTR-TECN-NULL
           ELSE
              MOVE (SF)-INTR-TECN
              TO B03-INTR-TECN
           END-IF
           IF (SF)-CPT-RSH-MOR-NULL = HIGH-VALUES
              MOVE (SF)-CPT-RSH-MOR-NULL
              TO B03-CPT-RSH-MOR-NULL
           ELSE
              MOVE (SF)-CPT-RSH-MOR
              TO B03-CPT-RSH-MOR
           END-IF
           IF (SF)-C-SUBRSH-T-NULL = HIGH-VALUES
              MOVE (SF)-C-SUBRSH-T-NULL
              TO B03-C-SUBRSH-T-NULL
           ELSE
              MOVE (SF)-C-SUBRSH-T
              TO B03-C-SUBRSH-T
           END-IF
           IF (SF)-PRE-RSH-T-NULL = HIGH-VALUES
              MOVE (SF)-PRE-RSH-T-NULL
              TO B03-PRE-RSH-T-NULL
           ELSE
              MOVE (SF)-PRE-RSH-T
              TO B03-PRE-RSH-T
           END-IF
           IF (SF)-ALQ-MARG-RIS-NULL = HIGH-VALUES
              MOVE (SF)-ALQ-MARG-RIS-NULL
              TO B03-ALQ-MARG-RIS-NULL
           ELSE
              MOVE (SF)-ALQ-MARG-RIS
              TO B03-ALQ-MARG-RIS
           END-IF
           IF (SF)-ALQ-MARG-C-SUBRSH-NULL = HIGH-VALUES
              MOVE (SF)-ALQ-MARG-C-SUBRSH-NULL
              TO B03-ALQ-MARG-C-SUBRSH-NULL
           ELSE
              MOVE (SF)-ALQ-MARG-C-SUBRSH
              TO B03-ALQ-MARG-C-SUBRSH
           END-IF
           IF (SF)-TS-RENDTO-SPPR-NULL = HIGH-VALUES
              MOVE (SF)-TS-RENDTO-SPPR-NULL
              TO B03-TS-RENDTO-SPPR-NULL
           ELSE
              MOVE (SF)-TS-RENDTO-SPPR
              TO B03-TS-RENDTO-SPPR
           END-IF
           IF (SF)-TP-IAS-NULL = HIGH-VALUES
              MOVE (SF)-TP-IAS-NULL
              TO B03-TP-IAS-NULL
           ELSE
              MOVE (SF)-TP-IAS
              TO B03-TP-IAS
           END-IF
           IF (SF)-NS-QUO-NULL = HIGH-VALUES
              MOVE (SF)-NS-QUO-NULL
              TO B03-NS-QUO-NULL
           ELSE
              MOVE (SF)-NS-QUO
              TO B03-NS-QUO
           END-IF
           IF (SF)-TS-MEDIO-NULL = HIGH-VALUES
              MOVE (SF)-TS-MEDIO-NULL
              TO B03-TS-MEDIO-NULL
           ELSE
              MOVE (SF)-TS-MEDIO
              TO B03-TS-MEDIO
           END-IF
           IF (SF)-CPT-RIASTO-NULL = HIGH-VALUES
              MOVE (SF)-CPT-RIASTO-NULL
              TO B03-CPT-RIASTO-NULL
           ELSE
              MOVE (SF)-CPT-RIASTO
              TO B03-CPT-RIASTO
           END-IF
           IF (SF)-PRE-RIASTO-NULL = HIGH-VALUES
              MOVE (SF)-PRE-RIASTO-NULL
              TO B03-PRE-RIASTO-NULL
           ELSE
              MOVE (SF)-PRE-RIASTO
              TO B03-PRE-RIASTO
           END-IF
           IF (SF)-RIS-RIASTA-NULL = HIGH-VALUES
              MOVE (SF)-RIS-RIASTA-NULL
              TO B03-RIS-RIASTA-NULL
           ELSE
              MOVE (SF)-RIS-RIASTA
              TO B03-RIS-RIASTA
           END-IF
           IF (SF)-CPT-RIASTO-ECC-NULL = HIGH-VALUES
              MOVE (SF)-CPT-RIASTO-ECC-NULL
              TO B03-CPT-RIASTO-ECC-NULL
           ELSE
              MOVE (SF)-CPT-RIASTO-ECC
              TO B03-CPT-RIASTO-ECC
           END-IF
           IF (SF)-PRE-RIASTO-ECC-NULL = HIGH-VALUES
              MOVE (SF)-PRE-RIASTO-ECC-NULL
              TO B03-PRE-RIASTO-ECC-NULL
           ELSE
              MOVE (SF)-PRE-RIASTO-ECC
              TO B03-PRE-RIASTO-ECC
           END-IF
           IF (SF)-RIS-RIASTA-ECC-NULL = HIGH-VALUES
              MOVE (SF)-RIS-RIASTA-ECC-NULL
              TO B03-RIS-RIASTA-ECC-NULL
           ELSE
              MOVE (SF)-RIS-RIASTA-ECC
              TO B03-RIS-RIASTA-ECC
           END-IF
           IF (SF)-COD-AGE-NULL = HIGH-VALUES
              MOVE (SF)-COD-AGE-NULL
              TO B03-COD-AGE-NULL
           ELSE
              MOVE (SF)-COD-AGE
              TO B03-COD-AGE
           END-IF
           IF (SF)-COD-SUBAGE-NULL = HIGH-VALUES
              MOVE (SF)-COD-SUBAGE-NULL
              TO B03-COD-SUBAGE-NULL
           ELSE
              MOVE (SF)-COD-SUBAGE
              TO B03-COD-SUBAGE
           END-IF
           IF (SF)-COD-CAN-NULL = HIGH-VALUES
              MOVE (SF)-COD-CAN-NULL
              TO B03-COD-CAN-NULL
           ELSE
              MOVE (SF)-COD-CAN
              TO B03-COD-CAN
           END-IF
           IF (SF)-IB-POLI-NULL = HIGH-VALUES
              MOVE (SF)-IB-POLI-NULL
              TO B03-IB-POLI-NULL
           ELSE
              MOVE (SF)-IB-POLI
              TO B03-IB-POLI
           END-IF
           IF (SF)-IB-ADES-NULL = HIGH-VALUES
              MOVE (SF)-IB-ADES-NULL
              TO B03-IB-ADES-NULL
           ELSE
              MOVE (SF)-IB-ADES
              TO B03-IB-ADES
           END-IF
           IF (SF)-IB-TRCH-DI-GAR-NULL = HIGH-VALUES
              MOVE (SF)-IB-TRCH-DI-GAR-NULL
              TO B03-IB-TRCH-DI-GAR-NULL
           ELSE
              MOVE (SF)-IB-TRCH-DI-GAR
              TO B03-IB-TRCH-DI-GAR
           END-IF
           IF (SF)-TP-PRSTZ-NULL = HIGH-VALUES
              MOVE (SF)-TP-PRSTZ-NULL
              TO B03-TP-PRSTZ-NULL
           ELSE
              MOVE (SF)-TP-PRSTZ
              TO B03-TP-PRSTZ
           END-IF
           IF (SF)-TP-TRASF-NULL = HIGH-VALUES
              MOVE (SF)-TP-TRASF-NULL
              TO B03-TP-TRASF-NULL
           ELSE
              MOVE (SF)-TP-TRASF
              TO B03-TP-TRASF
           END-IF
           IF (SF)-PP-INVRIO-TARI-NULL = HIGH-VALUES
              MOVE (SF)-PP-INVRIO-TARI-NULL
              TO B03-PP-INVRIO-TARI-NULL
           ELSE
              MOVE (SF)-PP-INVRIO-TARI
              TO B03-PP-INVRIO-TARI
           END-IF
           IF (SF)-COEFF-OPZ-REN-NULL = HIGH-VALUES
              MOVE (SF)-COEFF-OPZ-REN-NULL
              TO B03-COEFF-OPZ-REN-NULL
           ELSE
              MOVE (SF)-COEFF-OPZ-REN
              TO B03-COEFF-OPZ-REN
           END-IF
           IF (SF)-COEFF-OPZ-CPT-NULL = HIGH-VALUES
              MOVE (SF)-COEFF-OPZ-CPT-NULL
              TO B03-COEFF-OPZ-CPT-NULL
           ELSE
              MOVE (SF)-COEFF-OPZ-CPT
              TO B03-COEFF-OPZ-CPT
           END-IF
           IF (SF)-DUR-PAG-REN-NULL = HIGH-VALUES
              MOVE (SF)-DUR-PAG-REN-NULL
              TO B03-DUR-PAG-REN-NULL
           ELSE
              MOVE (SF)-DUR-PAG-REN
              TO B03-DUR-PAG-REN
           END-IF
           IF (SF)-VLT-NULL = HIGH-VALUES
              MOVE (SF)-VLT-NULL
              TO B03-VLT-NULL
           ELSE
              MOVE (SF)-VLT
              TO B03-VLT
           END-IF
           IF (SF)-RIS-MAT-CHIU-PREC-NULL = HIGH-VALUES
              MOVE (SF)-RIS-MAT-CHIU-PREC-NULL
              TO B03-RIS-MAT-CHIU-PREC-NULL
           ELSE
              MOVE (SF)-RIS-MAT-CHIU-PREC
              TO B03-RIS-MAT-CHIU-PREC
           END-IF
           IF (SF)-COD-FND-NULL = HIGH-VALUES
              MOVE (SF)-COD-FND-NULL
              TO B03-COD-FND-NULL
           ELSE
              MOVE (SF)-COD-FND
              TO B03-COD-FND
           END-IF
           IF (SF)-PRSTZ-T-NULL = HIGH-VALUES
              MOVE (SF)-PRSTZ-T-NULL
              TO B03-PRSTZ-T-NULL
           ELSE
              MOVE (SF)-PRSTZ-T
              TO B03-PRSTZ-T
           END-IF
           IF (SF)-TS-TARI-DOV-NULL = HIGH-VALUES
              MOVE (SF)-TS-TARI-DOV-NULL
              TO B03-TS-TARI-DOV-NULL
           ELSE
              MOVE (SF)-TS-TARI-DOV
              TO B03-TS-TARI-DOV
           END-IF
           IF (SF)-TS-TARI-SCON-NULL = HIGH-VALUES
              MOVE (SF)-TS-TARI-SCON-NULL
              TO B03-TS-TARI-SCON-NULL
           ELSE
              MOVE (SF)-TS-TARI-SCON
              TO B03-TS-TARI-SCON
           END-IF
           IF (SF)-TS-PP-NULL = HIGH-VALUES
              MOVE (SF)-TS-PP-NULL
              TO B03-TS-PP-NULL
           ELSE
              MOVE (SF)-TS-PP
              TO B03-TS-PP
           END-IF
           IF (SF)-COEFF-RIS-1-T-NULL = HIGH-VALUES
              MOVE (SF)-COEFF-RIS-1-T-NULL
              TO B03-COEFF-RIS-1-T-NULL
           ELSE
              MOVE (SF)-COEFF-RIS-1-T
              TO B03-COEFF-RIS-1-T
           END-IF
           IF (SF)-COEFF-RIS-2-T-NULL = HIGH-VALUES
              MOVE (SF)-COEFF-RIS-2-T-NULL
              TO B03-COEFF-RIS-2-T-NULL
           ELSE
              MOVE (SF)-COEFF-RIS-2-T
              TO B03-COEFF-RIS-2-T
           END-IF
           IF (SF)-ABB-NULL = HIGH-VALUES
              MOVE (SF)-ABB-NULL
              TO B03-ABB-NULL
           ELSE
              MOVE (SF)-ABB
              TO B03-ABB
           END-IF
           IF (SF)-TP-COASS-NULL = HIGH-VALUES
              MOVE (SF)-TP-COASS-NULL
              TO B03-TP-COASS-NULL
           ELSE
              MOVE (SF)-TP-COASS
              TO B03-TP-COASS
           END-IF
           IF (SF)-TRAT-RIASS-NULL = HIGH-VALUES
              MOVE (SF)-TRAT-RIASS-NULL
              TO B03-TRAT-RIASS-NULL
           ELSE
              MOVE (SF)-TRAT-RIASS
              TO B03-TRAT-RIASS
           END-IF
           IF (SF)-TRAT-RIASS-ECC-NULL = HIGH-VALUES
              MOVE (SF)-TRAT-RIASS-ECC-NULL
              TO B03-TRAT-RIASS-ECC-NULL
           ELSE
              MOVE (SF)-TRAT-RIASS-ECC
              TO B03-TRAT-RIASS-ECC
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO B03-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO B03-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO B03-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO B03-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO B03-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO B03-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO B03-DS-STATO-ELAB
           IF (SF)-TP-RGM-FISC-NULL = HIGH-VALUES
              MOVE (SF)-TP-RGM-FISC-NULL
              TO B03-TP-RGM-FISC-NULL
           ELSE
              MOVE (SF)-TP-RGM-FISC
              TO B03-TP-RGM-FISC
           END-IF
           IF (SF)-DUR-GAR-AA-NULL = HIGH-VALUES
              MOVE (SF)-DUR-GAR-AA-NULL
              TO B03-DUR-GAR-AA-NULL
           ELSE
              MOVE (SF)-DUR-GAR-AA
              TO B03-DUR-GAR-AA
           END-IF
           IF (SF)-DUR-GAR-MM-NULL = HIGH-VALUES
              MOVE (SF)-DUR-GAR-MM-NULL
              TO B03-DUR-GAR-MM-NULL
           ELSE
              MOVE (SF)-DUR-GAR-MM
              TO B03-DUR-GAR-MM
           END-IF
           IF (SF)-DUR-GAR-GG-NULL = HIGH-VALUES
              MOVE (SF)-DUR-GAR-GG-NULL
              TO B03-DUR-GAR-GG-NULL
           ELSE
              MOVE (SF)-DUR-GAR-GG
              TO B03-DUR-GAR-GG
           END-IF
           IF (SF)-ANTIDUR-CALC-365-NULL = HIGH-VALUES
              MOVE (SF)-ANTIDUR-CALC-365-NULL
              TO B03-ANTIDUR-CALC-365-NULL
           ELSE
              MOVE (SF)-ANTIDUR-CALC-365
              TO B03-ANTIDUR-CALC-365
           END-IF
           IF (SF)-COD-FISC-CNTR-NULL = HIGH-VALUES
              MOVE (SF)-COD-FISC-CNTR-NULL
              TO B03-COD-FISC-CNTR-NULL
           ELSE
              MOVE (SF)-COD-FISC-CNTR
              TO B03-COD-FISC-CNTR
           END-IF
           IF (SF)-COD-FISC-ASSTO1-NULL = HIGH-VALUES
              MOVE (SF)-COD-FISC-ASSTO1-NULL
              TO B03-COD-FISC-ASSTO1-NULL
           ELSE
              MOVE (SF)-COD-FISC-ASSTO1
              TO B03-COD-FISC-ASSTO1
           END-IF
           IF (SF)-COD-FISC-ASSTO2-NULL = HIGH-VALUES
              MOVE (SF)-COD-FISC-ASSTO2-NULL
              TO B03-COD-FISC-ASSTO2-NULL
           ELSE
              MOVE (SF)-COD-FISC-ASSTO2
              TO B03-COD-FISC-ASSTO2
           END-IF
           IF (SF)-COD-FISC-ASSTO3-NULL = HIGH-VALUES
              MOVE (SF)-COD-FISC-ASSTO3-NULL
              TO B03-COD-FISC-ASSTO3-NULL
           ELSE
              MOVE (SF)-COD-FISC-ASSTO3
              TO B03-COD-FISC-ASSTO3
           END-IF
           MOVE (SF)-CAUS-SCON
              TO B03-CAUS-SCON
           MOVE (SF)-EMIT-TIT-OPZ
              TO B03-EMIT-TIT-OPZ
           IF (SF)-QTZ-SP-Z-COUP-EMIS-NULL = HIGH-VALUES
              MOVE (SF)-QTZ-SP-Z-COUP-EMIS-NULL
              TO B03-QTZ-SP-Z-COUP-EMIS-NULL
           ELSE
              MOVE (SF)-QTZ-SP-Z-COUP-EMIS
              TO B03-QTZ-SP-Z-COUP-EMIS
           END-IF
           IF (SF)-QTZ-SP-Z-OPZ-EMIS-NULL = HIGH-VALUES
              MOVE (SF)-QTZ-SP-Z-OPZ-EMIS-NULL
              TO B03-QTZ-SP-Z-OPZ-EMIS-NULL
           ELSE
              MOVE (SF)-QTZ-SP-Z-OPZ-EMIS
              TO B03-QTZ-SP-Z-OPZ-EMIS
           END-IF
           IF (SF)-QTZ-SP-Z-COUP-DT-C-NULL = HIGH-VALUES
              MOVE (SF)-QTZ-SP-Z-COUP-DT-C-NULL
              TO B03-QTZ-SP-Z-COUP-DT-C-NULL
           ELSE
              MOVE (SF)-QTZ-SP-Z-COUP-DT-C
              TO B03-QTZ-SP-Z-COUP-DT-C
           END-IF
           IF (SF)-QTZ-SP-Z-OPZ-DT-CA-NULL = HIGH-VALUES
              MOVE (SF)-QTZ-SP-Z-OPZ-DT-CA-NULL
              TO B03-QTZ-SP-Z-OPZ-DT-CA-NULL
           ELSE
              MOVE (SF)-QTZ-SP-Z-OPZ-DT-CA
              TO B03-QTZ-SP-Z-OPZ-DT-CA
           END-IF
           IF (SF)-QTZ-TOT-EMIS-NULL = HIGH-VALUES
              MOVE (SF)-QTZ-TOT-EMIS-NULL
              TO B03-QTZ-TOT-EMIS-NULL
           ELSE
              MOVE (SF)-QTZ-TOT-EMIS
              TO B03-QTZ-TOT-EMIS
           END-IF
           IF (SF)-QTZ-TOT-DT-CALC-NULL = HIGH-VALUES
              MOVE (SF)-QTZ-TOT-DT-CALC-NULL
              TO B03-QTZ-TOT-DT-CALC-NULL
           ELSE
              MOVE (SF)-QTZ-TOT-DT-CALC
              TO B03-QTZ-TOT-DT-CALC
           END-IF
           IF (SF)-QTZ-TOT-DT-ULT-BIL-NULL = HIGH-VALUES
              MOVE (SF)-QTZ-TOT-DT-ULT-BIL-NULL
              TO B03-QTZ-TOT-DT-ULT-BIL-NULL
           ELSE
              MOVE (SF)-QTZ-TOT-DT-ULT-BIL
              TO B03-QTZ-TOT-DT-ULT-BIL
           END-IF
           IF (SF)-DT-QTZ-EMIS-NULL = HIGH-VALUES
              MOVE (SF)-DT-QTZ-EMIS-NULL
              TO B03-DT-QTZ-EMIS-NULL
           ELSE
             IF (SF)-DT-QTZ-EMIS = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-QTZ-EMIS-NULL
             ELSE
              MOVE (SF)-DT-QTZ-EMIS
              TO B03-DT-QTZ-EMIS
             END-IF
           END-IF
           IF (SF)-PC-CAR-GEST-NULL = HIGH-VALUES
              MOVE (SF)-PC-CAR-GEST-NULL
              TO B03-PC-CAR-GEST-NULL
           ELSE
              MOVE (SF)-PC-CAR-GEST
              TO B03-PC-CAR-GEST
           END-IF
           IF (SF)-PC-CAR-ACQ-NULL = HIGH-VALUES
              MOVE (SF)-PC-CAR-ACQ-NULL
              TO B03-PC-CAR-ACQ-NULL
           ELSE
              MOVE (SF)-PC-CAR-ACQ
              TO B03-PC-CAR-ACQ
           END-IF
           IF (SF)-IMP-CAR-CASO-MOR-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CAR-CASO-MOR-NULL
              TO B03-IMP-CAR-CASO-MOR-NULL
           ELSE
              MOVE (SF)-IMP-CAR-CASO-MOR
              TO B03-IMP-CAR-CASO-MOR
           END-IF
           IF (SF)-PC-CAR-MOR-NULL = HIGH-VALUES
              MOVE (SF)-PC-CAR-MOR-NULL
              TO B03-PC-CAR-MOR-NULL
           ELSE
              MOVE (SF)-PC-CAR-MOR
              TO B03-PC-CAR-MOR
           END-IF
           IF (SF)-TP-VERS-NULL = HIGH-VALUES
              MOVE (SF)-TP-VERS-NULL
              TO B03-TP-VERS-NULL
           ELSE
              MOVE (SF)-TP-VERS
              TO B03-TP-VERS
           END-IF
           IF (SF)-FL-SWITCH-NULL = HIGH-VALUES
              MOVE (SF)-FL-SWITCH-NULL
              TO B03-FL-SWITCH-NULL
           ELSE
              MOVE (SF)-FL-SWITCH
              TO B03-FL-SWITCH
           END-IF
           IF (SF)-FL-IAS-NULL = HIGH-VALUES
              MOVE (SF)-FL-IAS-NULL
              TO B03-FL-IAS-NULL
           ELSE
              MOVE (SF)-FL-IAS
              TO B03-FL-IAS
           END-IF
           IF (SF)-DIR-NULL = HIGH-VALUES
              MOVE (SF)-DIR-NULL
              TO B03-DIR-NULL
           ELSE
              MOVE (SF)-DIR
              TO B03-DIR
           END-IF
           IF (SF)-TP-COP-CASO-MOR-NULL = HIGH-VALUES
              MOVE (SF)-TP-COP-CASO-MOR-NULL
              TO B03-TP-COP-CASO-MOR-NULL
           ELSE
              MOVE (SF)-TP-COP-CASO-MOR
              TO B03-TP-COP-CASO-MOR
           END-IF
           IF (SF)-MET-RISC-SPCL-NULL = HIGH-VALUES
              MOVE (SF)-MET-RISC-SPCL-NULL
              TO B03-MET-RISC-SPCL-NULL
           ELSE
              MOVE (SF)-MET-RISC-SPCL
              TO B03-MET-RISC-SPCL
           END-IF
           IF (SF)-TP-STAT-INVST-NULL = HIGH-VALUES
              MOVE (SF)-TP-STAT-INVST-NULL
              TO B03-TP-STAT-INVST-NULL
           ELSE
              MOVE (SF)-TP-STAT-INVST
              TO B03-TP-STAT-INVST
           END-IF
           IF (SF)-COD-PRDT-NULL = HIGH-VALUES
              MOVE (SF)-COD-PRDT-NULL
              TO B03-COD-PRDT-NULL
           ELSE
              MOVE (SF)-COD-PRDT
              TO B03-COD-PRDT
           END-IF
           IF (SF)-STAT-ASSTO-1-NULL = HIGH-VALUES
              MOVE (SF)-STAT-ASSTO-1-NULL
              TO B03-STAT-ASSTO-1-NULL
           ELSE
              MOVE (SF)-STAT-ASSTO-1
              TO B03-STAT-ASSTO-1
           END-IF
           IF (SF)-STAT-ASSTO-2-NULL = HIGH-VALUES
              MOVE (SF)-STAT-ASSTO-2-NULL
              TO B03-STAT-ASSTO-2-NULL
           ELSE
              MOVE (SF)-STAT-ASSTO-2
              TO B03-STAT-ASSTO-2
           END-IF
           IF (SF)-STAT-ASSTO-3-NULL = HIGH-VALUES
              MOVE (SF)-STAT-ASSTO-3-NULL
              TO B03-STAT-ASSTO-3-NULL
           ELSE
              MOVE (SF)-STAT-ASSTO-3
              TO B03-STAT-ASSTO-3
           END-IF
           IF (SF)-CPT-ASSTO-INI-MOR-NULL = HIGH-VALUES
              MOVE (SF)-CPT-ASSTO-INI-MOR-NULL
              TO B03-CPT-ASSTO-INI-MOR-NULL
           ELSE
              MOVE (SF)-CPT-ASSTO-INI-MOR
              TO B03-CPT-ASSTO-INI-MOR
           END-IF
           IF (SF)-TS-STAB-PRE-NULL = HIGH-VALUES
              MOVE (SF)-TS-STAB-PRE-NULL
              TO B03-TS-STAB-PRE-NULL
           ELSE
              MOVE (SF)-TS-STAB-PRE
              TO B03-TS-STAB-PRE
           END-IF
           IF (SF)-DIR-EMIS-NULL = HIGH-VALUES
              MOVE (SF)-DIR-EMIS-NULL
              TO B03-DIR-EMIS-NULL
           ELSE
              MOVE (SF)-DIR-EMIS
              TO B03-DIR-EMIS
           END-IF
           IF (SF)-DT-INC-ULT-PRE-NULL = HIGH-VALUES
              MOVE (SF)-DT-INC-ULT-PRE-NULL
              TO B03-DT-INC-ULT-PRE-NULL
           ELSE
             IF (SF)-DT-INC-ULT-PRE = ZERO
                MOVE HIGH-VALUES
                TO B03-DT-INC-ULT-PRE-NULL
             ELSE
              MOVE (SF)-DT-INC-ULT-PRE
              TO B03-DT-INC-ULT-PRE
             END-IF
           END-IF
           IF (SF)-STAT-TBGC-ASSTO-1-NULL = HIGH-VALUES
              MOVE (SF)-STAT-TBGC-ASSTO-1-NULL
              TO B03-STAT-TBGC-ASSTO-1-NULL
           ELSE
              MOVE (SF)-STAT-TBGC-ASSTO-1
              TO B03-STAT-TBGC-ASSTO-1
           END-IF
           IF (SF)-STAT-TBGC-ASSTO-2-NULL = HIGH-VALUES
              MOVE (SF)-STAT-TBGC-ASSTO-2-NULL
              TO B03-STAT-TBGC-ASSTO-2-NULL
           ELSE
              MOVE (SF)-STAT-TBGC-ASSTO-2
              TO B03-STAT-TBGC-ASSTO-2
           END-IF
           IF (SF)-STAT-TBGC-ASSTO-3-NULL = HIGH-VALUES
              MOVE (SF)-STAT-TBGC-ASSTO-3-NULL
              TO B03-STAT-TBGC-ASSTO-3-NULL
           ELSE
              MOVE (SF)-STAT-TBGC-ASSTO-3
              TO B03-STAT-TBGC-ASSTO-3
           END-IF
           IF (SF)-FRAZ-DECR-CPT-NULL = HIGH-VALUES
              MOVE (SF)-FRAZ-DECR-CPT-NULL
              TO B03-FRAZ-DECR-CPT-NULL
           ELSE
              MOVE (SF)-FRAZ-DECR-CPT
              TO B03-FRAZ-DECR-CPT
           END-IF
           IF (SF)-PRE-PP-ULT-NULL = HIGH-VALUES
              MOVE (SF)-PRE-PP-ULT-NULL
              TO B03-PRE-PP-ULT-NULL
           ELSE
              MOVE (SF)-PRE-PP-ULT
              TO B03-PRE-PP-ULT
           END-IF
           IF (SF)-ACQ-EXP-NULL = HIGH-VALUES
              MOVE (SF)-ACQ-EXP-NULL
              TO B03-ACQ-EXP-NULL
           ELSE
              MOVE (SF)-ACQ-EXP
              TO B03-ACQ-EXP
           END-IF
           IF (SF)-REMUN-ASS-NULL = HIGH-VALUES
              MOVE (SF)-REMUN-ASS-NULL
              TO B03-REMUN-ASS-NULL
           ELSE
              MOVE (SF)-REMUN-ASS
              TO B03-REMUN-ASS
           END-IF
           IF (SF)-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE (SF)-COMMIS-INTER-NULL
              TO B03-COMMIS-INTER-NULL
           ELSE
              MOVE (SF)-COMMIS-INTER
              TO B03-COMMIS-INTER
           END-IF
           IF (SF)-NUM-FINANZ-NULL = HIGH-VALUES
              MOVE (SF)-NUM-FINANZ-NULL
              TO B03-NUM-FINANZ-NULL
           ELSE
              MOVE (SF)-NUM-FINANZ
              TO B03-NUM-FINANZ
           END-IF
           IF (SF)-TP-ACC-COMM-NULL = HIGH-VALUES
              MOVE (SF)-TP-ACC-COMM-NULL
              TO B03-TP-ACC-COMM-NULL
           ELSE
              MOVE (SF)-TP-ACC-COMM
              TO B03-TP-ACC-COMM
           END-IF
           IF (SF)-IB-ACC-COMM-NULL = HIGH-VALUES
              MOVE (SF)-IB-ACC-COMM-NULL
              TO B03-IB-ACC-COMM-NULL
           ELSE
              MOVE (SF)-IB-ACC-COMM
              TO B03-IB-ACC-COMM
           END-IF
           MOVE (SF)-RAMO-BILA
              TO B03-RAMO-BILA
           IF (SF)-CARZ-NULL = HIGH-VALUES
              MOVE (SF)-CARZ-NULL
              TO B03-CARZ-NULL
           ELSE
              MOVE (SF)-CARZ
              TO B03-CARZ
           END-IF.
       VAL-DCLGEN-B03-EX.
           EXIT.

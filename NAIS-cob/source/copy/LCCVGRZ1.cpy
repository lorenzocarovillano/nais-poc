      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA GAR
      *   ALIAS GRZ
      *   ULTIMO AGG. 31 OTT 2013
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-GAR PIC S9(9)     COMP-3.
             07 (SF)-ID-ADES PIC S9(9)     COMP-3.
             07 (SF)-ID-ADES-NULL REDEFINES
                (SF)-ID-ADES   PIC X(5).
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-IB-OGG PIC X(40).
             07 (SF)-IB-OGG-NULL REDEFINES
                (SF)-IB-OGG   PIC X(40).
             07 (SF)-DT-DECOR   PIC S9(8) COMP-3.
             07 (SF)-DT-DECOR-NULL REDEFINES
                (SF)-DT-DECOR   PIC X(5).
             07 (SF)-DT-SCAD   PIC S9(8) COMP-3.
             07 (SF)-DT-SCAD-NULL REDEFINES
                (SF)-DT-SCAD   PIC X(5).
             07 (SF)-COD-SEZ PIC X(12).
             07 (SF)-COD-SEZ-NULL REDEFINES
                (SF)-COD-SEZ   PIC X(12).
             07 (SF)-COD-TARI PIC X(12).
             07 (SF)-RAMO-BILA PIC X(12).
             07 (SF)-RAMO-BILA-NULL REDEFINES
                (SF)-RAMO-BILA   PIC X(12).
             07 (SF)-DT-INI-VAL-TAR   PIC S9(8) COMP-3.
             07 (SF)-DT-INI-VAL-TAR-NULL REDEFINES
                (SF)-DT-INI-VAL-TAR   PIC X(5).
             07 (SF)-ID-1O-ASSTO PIC S9(9)     COMP-3.
             07 (SF)-ID-1O-ASSTO-NULL REDEFINES
                (SF)-ID-1O-ASSTO   PIC X(5).
             07 (SF)-ID-2O-ASSTO PIC S9(9)     COMP-3.
             07 (SF)-ID-2O-ASSTO-NULL REDEFINES
                (SF)-ID-2O-ASSTO   PIC X(5).
             07 (SF)-ID-3O-ASSTO PIC S9(9)     COMP-3.
             07 (SF)-ID-3O-ASSTO-NULL REDEFINES
                (SF)-ID-3O-ASSTO   PIC X(5).
             07 (SF)-TP-GAR PIC S9(2)     COMP-3.
             07 (SF)-TP-GAR-NULL REDEFINES
                (SF)-TP-GAR   PIC X(2).
             07 (SF)-TP-RSH PIC X(2).
             07 (SF)-TP-RSH-NULL REDEFINES
                (SF)-TP-RSH   PIC X(2).
             07 (SF)-TP-INVST PIC S9(2)     COMP-3.
             07 (SF)-TP-INVST-NULL REDEFINES
                (SF)-TP-INVST   PIC X(2).
             07 (SF)-MOD-PAG-GARCOL PIC X(2).
             07 (SF)-MOD-PAG-GARCOL-NULL REDEFINES
                (SF)-MOD-PAG-GARCOL   PIC X(2).
             07 (SF)-TP-PER-PRE PIC X(2).
             07 (SF)-TP-PER-PRE-NULL REDEFINES
                (SF)-TP-PER-PRE   PIC X(2).
             07 (SF)-ETA-AA-1O-ASSTO PIC S9(3)     COMP-3.
             07 (SF)-ETA-AA-1O-ASSTO-NULL REDEFINES
                (SF)-ETA-AA-1O-ASSTO   PIC X(2).
             07 (SF)-ETA-MM-1O-ASSTO PIC S9(3)     COMP-3.
             07 (SF)-ETA-MM-1O-ASSTO-NULL REDEFINES
                (SF)-ETA-MM-1O-ASSTO   PIC X(2).
             07 (SF)-ETA-AA-2O-ASSTO PIC S9(3)     COMP-3.
             07 (SF)-ETA-AA-2O-ASSTO-NULL REDEFINES
                (SF)-ETA-AA-2O-ASSTO   PIC X(2).
             07 (SF)-ETA-MM-2O-ASSTO PIC S9(3)     COMP-3.
             07 (SF)-ETA-MM-2O-ASSTO-NULL REDEFINES
                (SF)-ETA-MM-2O-ASSTO   PIC X(2).
             07 (SF)-ETA-AA-3O-ASSTO PIC S9(3)     COMP-3.
             07 (SF)-ETA-AA-3O-ASSTO-NULL REDEFINES
                (SF)-ETA-AA-3O-ASSTO   PIC X(2).
             07 (SF)-ETA-MM-3O-ASSTO PIC S9(3)     COMP-3.
             07 (SF)-ETA-MM-3O-ASSTO-NULL REDEFINES
                (SF)-ETA-MM-3O-ASSTO   PIC X(2).
             07 (SF)-TP-EMIS-PUR PIC X(1).
             07 (SF)-TP-EMIS-PUR-NULL REDEFINES
                (SF)-TP-EMIS-PUR   PIC X(1).
             07 (SF)-ETA-A-SCAD PIC S9(5)     COMP-3.
             07 (SF)-ETA-A-SCAD-NULL REDEFINES
                (SF)-ETA-A-SCAD   PIC X(3).
             07 (SF)-TP-CALC-PRE-PRSTZ PIC X(2).
             07 (SF)-TP-CALC-PRE-PRSTZ-NULL REDEFINES
                (SF)-TP-CALC-PRE-PRSTZ   PIC X(2).
             07 (SF)-TP-PRE PIC X(1).
             07 (SF)-TP-PRE-NULL REDEFINES
                (SF)-TP-PRE   PIC X(1).
             07 (SF)-TP-DUR PIC X(2).
             07 (SF)-TP-DUR-NULL REDEFINES
                (SF)-TP-DUR   PIC X(2).
             07 (SF)-DUR-AA PIC S9(5)     COMP-3.
             07 (SF)-DUR-AA-NULL REDEFINES
                (SF)-DUR-AA   PIC X(3).
             07 (SF)-DUR-MM PIC S9(5)     COMP-3.
             07 (SF)-DUR-MM-NULL REDEFINES
                (SF)-DUR-MM   PIC X(3).
             07 (SF)-DUR-GG PIC S9(5)     COMP-3.
             07 (SF)-DUR-GG-NULL REDEFINES
                (SF)-DUR-GG   PIC X(3).
             07 (SF)-NUM-AA-PAG-PRE PIC S9(5)     COMP-3.
             07 (SF)-NUM-AA-PAG-PRE-NULL REDEFINES
                (SF)-NUM-AA-PAG-PRE   PIC X(3).
             07 (SF)-AA-PAG-PRE-UNI PIC S9(5)     COMP-3.
             07 (SF)-AA-PAG-PRE-UNI-NULL REDEFINES
                (SF)-AA-PAG-PRE-UNI   PIC X(3).
             07 (SF)-MM-PAG-PRE-UNI PIC S9(5)     COMP-3.
             07 (SF)-MM-PAG-PRE-UNI-NULL REDEFINES
                (SF)-MM-PAG-PRE-UNI   PIC X(3).
             07 (SF)-FRAZ-INI-EROG-REN PIC S9(5)     COMP-3.
             07 (SF)-FRAZ-INI-EROG-REN-NULL REDEFINES
                (SF)-FRAZ-INI-EROG-REN   PIC X(3).
             07 (SF)-MM-1O-RAT PIC S9(2)     COMP-3.
             07 (SF)-MM-1O-RAT-NULL REDEFINES
                (SF)-MM-1O-RAT   PIC X(2).
             07 (SF)-PC-1O-RAT PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-1O-RAT-NULL REDEFINES
                (SF)-PC-1O-RAT   PIC X(4).
             07 (SF)-TP-PRSTZ-ASSTA PIC X(2).
             07 (SF)-TP-PRSTZ-ASSTA-NULL REDEFINES
                (SF)-TP-PRSTZ-ASSTA   PIC X(2).
             07 (SF)-DT-END-CARZ   PIC S9(8) COMP-3.
             07 (SF)-DT-END-CARZ-NULL REDEFINES
                (SF)-DT-END-CARZ   PIC X(5).
             07 (SF)-PC-RIP-PRE PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-RIP-PRE-NULL REDEFINES
                (SF)-PC-RIP-PRE   PIC X(4).
             07 (SF)-COD-FND PIC X(12).
             07 (SF)-COD-FND-NULL REDEFINES
                (SF)-COD-FND   PIC X(12).
             07 (SF)-AA-REN-CER PIC X(18).
             07 (SF)-AA-REN-CER-NULL REDEFINES
                (SF)-AA-REN-CER   PIC X(18).
             07 (SF)-PC-REVRSB PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-REVRSB-NULL REDEFINES
                (SF)-PC-REVRSB   PIC X(4).
             07 (SF)-TP-PC-RIP PIC X(2).
             07 (SF)-TP-PC-RIP-NULL REDEFINES
                (SF)-TP-PC-RIP   PIC X(2).
             07 (SF)-PC-OPZ PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-OPZ-NULL REDEFINES
                (SF)-PC-OPZ   PIC X(4).
             07 (SF)-TP-IAS PIC X(2).
             07 (SF)-TP-IAS-NULL REDEFINES
                (SF)-TP-IAS   PIC X(2).
             07 (SF)-TP-STAB PIC X(2).
             07 (SF)-TP-STAB-NULL REDEFINES
                (SF)-TP-STAB   PIC X(2).
             07 (SF)-TP-ADEG-PRE PIC X(1).
             07 (SF)-TP-ADEG-PRE-NULL REDEFINES
                (SF)-TP-ADEG-PRE   PIC X(1).
             07 (SF)-DT-VARZ-TP-IAS   PIC S9(8) COMP-3.
             07 (SF)-DT-VARZ-TP-IAS-NULL REDEFINES
                (SF)-DT-VARZ-TP-IAS   PIC X(5).
             07 (SF)-FRAZ-DECR-CPT PIC S9(5)     COMP-3.
             07 (SF)-FRAZ-DECR-CPT-NULL REDEFINES
                (SF)-FRAZ-DECR-CPT   PIC X(3).
             07 (SF)-COD-TRAT-RIASS PIC X(12).
             07 (SF)-COD-TRAT-RIASS-NULL REDEFINES
                (SF)-COD-TRAT-RIASS   PIC X(12).
             07 (SF)-TP-DT-EMIS-RIASS PIC X(2).
             07 (SF)-TP-DT-EMIS-RIASS-NULL REDEFINES
                (SF)-TP-DT-EMIS-RIASS   PIC X(2).
             07 (SF)-TP-CESS-RIASS PIC X(2).
             07 (SF)-TP-CESS-RIASS-NULL REDEFINES
                (SF)-TP-CESS-RIASS   PIC X(2).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-AA-STAB PIC S9(5)     COMP-3.
             07 (SF)-AA-STAB-NULL REDEFINES
                (SF)-AA-STAB   PIC X(3).
             07 (SF)-TS-STAB-LIMITATA PIC S9(5)V9(9) COMP-3.
             07 (SF)-TS-STAB-LIMITATA-NULL REDEFINES
                (SF)-TS-STAB-LIMITATA   PIC X(8).
             07 (SF)-DT-PRESC   PIC S9(8) COMP-3.
             07 (SF)-DT-PRESC-NULL REDEFINES
                (SF)-DT-PRESC   PIC X(5).
             07 (SF)-RSH-INVST PIC X(1).
             07 (SF)-RSH-INVST-NULL REDEFINES
                (SF)-RSH-INVST   PIC X(1).
             07 (SF)-TP-RAMO-BILA PIC X(2).

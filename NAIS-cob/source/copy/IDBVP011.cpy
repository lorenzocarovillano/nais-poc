       01 RICH-EST.
         05 P01-ID-RICH-EST PIC S9(9)V     COMP-3.
         05 P01-ID-RICH-EST-COLLG PIC S9(9)V     COMP-3.
         05 P01-ID-RICH-EST-COLLG-NULL REDEFINES
            P01-ID-RICH-EST-COLLG   PIC X(5).
         05 P01-ID-LIQ PIC S9(9)V     COMP-3.
         05 P01-ID-LIQ-NULL REDEFINES
            P01-ID-LIQ   PIC X(5).
         05 P01-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 P01-IB-RICH-EST PIC X(40).
         05 P01-IB-RICH-EST-NULL REDEFINES
            P01-IB-RICH-EST   PIC X(40).
         05 P01-TP-MOVI PIC S9(5)V     COMP-3.
         05 P01-DT-FORM-RICH   PIC S9(8)V COMP-3.
         05 P01-DT-INVIO-RICH   PIC S9(8)V COMP-3.
         05 P01-DT-PERV-RICH   PIC S9(8)V COMP-3.
         05 P01-DT-RGSTRZ-RICH   PIC S9(8)V COMP-3.
         05 P01-DT-EFF   PIC S9(8)V COMP-3.
         05 P01-DT-EFF-NULL REDEFINES
            P01-DT-EFF   PIC X(5).
         05 P01-DT-SIN   PIC S9(8)V COMP-3.
         05 P01-DT-SIN-NULL REDEFINES
            P01-DT-SIN   PIC X(5).
         05 P01-TP-OGG PIC X(2).
         05 P01-ID-OGG PIC S9(9)V     COMP-3.
         05 P01-IB-OGG PIC X(40).
         05 P01-FL-MOD-EXEC PIC X(1).
         05 P01-ID-RICHIEDENTE PIC S9(9)V     COMP-3.
         05 P01-ID-RICHIEDENTE-NULL REDEFINES
            P01-ID-RICHIEDENTE   PIC X(5).
         05 P01-COD-PROD PIC X(12).
         05 P01-DS-OPER-SQL PIC X(1).
         05 P01-DS-VER PIC S9(9)V     COMP-3.
         05 P01-DS-TS-CPTZ PIC S9(18)V     COMP-3.
         05 P01-DS-UTENTE PIC X(20).
         05 P01-DS-STATO-ELAB PIC X(1).
         05 P01-COD-CAN PIC S9(5)V     COMP-3.
         05 P01-IB-OGG-ORIG PIC X(40).
         05 P01-IB-OGG-ORIG-NULL REDEFINES
            P01-IB-OGG-ORIG   PIC X(40).
         05 P01-DT-EST-FINANZ   PIC S9(8)V COMP-3.
         05 P01-DT-EST-FINANZ-NULL REDEFINES
            P01-DT-EST-FINANZ   PIC X(5).
         05 P01-DT-MAN-COP   PIC S9(8)V COMP-3.
         05 P01-DT-MAN-COP-NULL REDEFINES
            P01-DT-MAN-COP   PIC X(5).
         05 P01-FL-GEST-PROTEZIONE PIC X(1).
         05 P01-FL-GEST-PROTEZIONE-NULL REDEFINES
            P01-FL-GEST-PROTEZIONE   PIC X(1).


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA AMMB_FUNZ_FUNZ
      *   ALIAS L05
      *   ULTIMO AGG. 06 GIU 2018
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-DATI.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-TP-MOVI-ESEC PIC S9(5)     COMP-3.
             07 (SF)-TP-MOVI-RIFTO PIC S9(5)     COMP-3.
             07 (SF)-GRAV-FUNZ-FUNZ PIC X(2).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-COD-BLOCCO PIC X(5).
             07 (SF)-COD-BLOCCO-NULL REDEFINES
                (SF)-COD-BLOCCO   PIC X(5).
             07 (SF)-SRVZ-VER-ANN PIC X(8).
             07 (SF)-SRVZ-VER-ANN-NULL REDEFINES
                (SF)-SRVZ-VER-ANN   PIC X(8).
             07 (SF)-WHERE-CONDITION PIC X(300).
             07 (SF)-FL-POLI-IFP PIC X(1).
             07 (SF)-FL-POLI-IFP-NULL REDEFINES
                (SF)-FL-POLI-IFP   PIC X(1).

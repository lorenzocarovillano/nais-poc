           EXEC SQL DECLARE STAT_RICH_EST TABLE
           (
             ID_STAT_RICH_EST    DECIMAL(9, 0) NOT NULL,
             ID_RICH_EST         DECIMAL(9, 0) NOT NULL,
             TS_INI_VLDT         DECIMAL(18, 0) NOT NULL,
             TS_END_VLDT         DECIMAL(18, 0) NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             COD_PRCS            CHAR(3) NOT NULL,
             COD_ATTVT           CHAR(10) NOT NULL,
             STAT_RICH_EST       CHAR(2) NOT NULL,
             FL_STAT_END         CHAR(1),
             DS_OPER_SQL         CHAR(1) NOT NULL,
             DS_VER              DECIMAL(9, 0) NOT NULL,
             DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL,
             DS_UTENTE           CHAR(20) NOT NULL,
             DS_STATO_ELAB       CHAR(1) NOT NULL,
             TP_CAUS_SCARTO      CHAR(2),
             DESC_ERR            VARCHAR(250),
             UTENTE_INS_AGG      CHAR(20) NOT NULL,
             COD_ERR_SCARTO      CHAR(10),
             ID_MOVI_CRZ         DECIMAL(9, 0)
          ) END-EXEC.

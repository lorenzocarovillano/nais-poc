      *----------------------------------------------------------------*
      *    COPY      ..... LCCVRST6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO RISERVA DI TRANCHE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-RIS-DI-TRCH.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE RIS-DI-TRCH

      *--> NOME TABELLA FISICA DB
           MOVE 'RIS-DI-TRCH'
             TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WRST-ST-INV(IX-TAB-RST)
              AND WRST-ELE-RISE-TRA-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WRST-ST-ADD(IX-TAB-RST)

                       MOVE WMOV-ID-PTF
                         TO RST-ID-MOVI-CRZ

      *-->             Estrazione Oggetto
                        PERFORM PREPARA-AREA-LCCS0234-RST
                           THRU PREPARA-AREA-LCCS0234-RST-EX
                        PERFORM CALL-LCCS0234
                           THRU CALL-LCCS0234-EX
                        MOVE S234-ID-OGG-PTF-EOC
                          TO WRST-ID-PTF(IX-TAB-RST)
                             RST-ID-RIS-DI-TRCH

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WRST-ST-MOD(IX-TAB-RST)

                       MOVE WRST-ID-PTF(IX-TAB-RST)
                         TO RST-ID-RIS-DI-TRCH
                       MOVE WMOV-ID-PTF
                         TO RST-ID-MOVI-CRZ
                       MOVE WMOV-DT-EFF
                         TO RST-DT-INI-EFF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WRST-ST-DEL(IX-TAB-RST)

                       MOVE WRST-ID-PTF(IX-TAB-RST)
                         TO RST-ID-RIS-DI-TRCH
                       MOVE WMOV-ID-PTF
                         TO RST-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN RISERVA DI TRANCHE
                 PERFORM VAL-DCLGEN-RST
                    THRU VAL-DCLGEN-RST-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-RST
                    THRU VALORIZZA-AREA-DSH-RST-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-RIS-DI-TRCH-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-RST.

      *--> DCLGEN TABELLA
           MOVE RIS-DI-TRCH             TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-RST-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   PREPARA PER LA CALL ALL'LCCS0234 ESTRATTORE OGGETTO PTF
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234-RST.

           MOVE WRST-ID-RIS-DI-TRCH(IX-TAB-RST)    TO S234-ID-OGG-EOC
           MOVE 'TG'                               TO S234-TIPO-OGG-EOC.

       PREPARA-AREA-LCCS0234-RST-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVRST5.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA ESTR_CNT_DIAGN_RIV
      *   ALIAS P85
      *   ULTIMO AGG. 26 SET 2014
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-DATI.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-DT-RIVAL   PIC S9(8) COMP-3.
             07 (SF)-COD-TARI PIC X(12).
             07 (SF)-RENDTO-LRD PIC S9(5)V9(9) COMP-3.
             07 (SF)-RENDTO-LRD-NULL REDEFINES
                (SF)-RENDTO-LRD   PIC X(8).
             07 (SF)-PC-RETR PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-RETR-NULL REDEFINES
                (SF)-PC-RETR   PIC X(4).
             07 (SF)-RENDTO-RETR PIC S9(5)V9(9) COMP-3.
             07 (SF)-RENDTO-RETR-NULL REDEFINES
                (SF)-RENDTO-RETR   PIC X(8).
             07 (SF)-COMMIS-GEST PIC S9(12)V9(3) COMP-3.
             07 (SF)-COMMIS-GEST-NULL REDEFINES
                (SF)-COMMIS-GEST   PIC X(8).
             07 (SF)-TS-RIVAL-NET PIC S9(5)V9(9) COMP-3.
             07 (SF)-TS-RIVAL-NET-NULL REDEFINES
                (SF)-TS-RIVAL-NET   PIC X(8).
             07 (SF)-MIN-GARTO PIC S9(5)V9(9) COMP-3.
             07 (SF)-MIN-GARTO-NULL REDEFINES
                (SF)-MIN-GARTO   PIC X(8).
             07 (SF)-MIN-TRNUT PIC S9(5)V9(9) COMP-3.
             07 (SF)-MIN-TRNUT-NULL REDEFINES
                (SF)-MIN-TRNUT   PIC X(8).
             07 (SF)-IB-POLI PIC X(40).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).

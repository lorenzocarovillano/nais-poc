      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA BNFIC
      *   ALIAS BEP
      *   ULTIMO AGG. 09 AGO 2018
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-BNFIC PIC S9(9)     COMP-3.
             07 (SF)-ID-RAPP-ANA PIC S9(9)     COMP-3.
             07 (SF)-ID-BNFICR PIC S9(9)     COMP-3.
             07 (SF)-ID-BNFICR-NULL REDEFINES
                (SF)-ID-BNFICR   PIC X(5).
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-BNFIC PIC S9(2)     COMP-3.
             07 (SF)-COD-BNFIC-NULL REDEFINES
                (SF)-COD-BNFIC   PIC X(2).
             07 (SF)-TP-IND-BNFICR PIC X(2).
             07 (SF)-COD-BNFICR PIC X(20).
             07 (SF)-COD-BNFICR-NULL REDEFINES
                (SF)-COD-BNFICR   PIC X(20).
             07 (SF)-DESC-BNFICR PIC X(250).
             07 (SF)-PC-DEL-BNFICR PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-DEL-BNFICR-NULL REDEFINES
                (SF)-PC-DEL-BNFICR   PIC X(4).
             07 (SF)-FL-ESE PIC X(1).
             07 (SF)-FL-ESE-NULL REDEFINES
                (SF)-FL-ESE   PIC X(1).
             07 (SF)-FL-IRREV PIC X(1).
             07 (SF)-FL-IRREV-NULL REDEFINES
                (SF)-FL-IRREV   PIC X(1).
             07 (SF)-FL-DFLT PIC X(1).
             07 (SF)-FL-DFLT-NULL REDEFINES
                (SF)-FL-DFLT   PIC X(1).
             07 (SF)-ESRCN-ATTVT-IMPRS PIC X(1).
             07 (SF)-ESRCN-ATTVT-IMPRS-NULL REDEFINES
                (SF)-ESRCN-ATTVT-IMPRS   PIC X(1).
             07 (SF)-FL-BNFICR-COLL PIC X(1).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-TP-NORMAL-BNFIC PIC X(2).
             07 (SF)-TP-NORMAL-BNFIC-NULL REDEFINES
                (SF)-TP-NORMAL-BNFIC   PIC X(2).

           EXEC SQL DECLARE RAPP_ANA TABLE
           (
             ID_RAPP_ANA         DECIMAL(9, 0) NOT NULL,
             ID_RAPP_ANA_COLLG   DECIMAL(9, 0),
             ID_OGG              DECIMAL(9, 0) NOT NULL,
             TP_OGG              CHAR(2) NOT NULL,
             ID_MOVI_CRZ         DECIMAL(9, 0) NOT NULL,
             ID_MOVI_CHIU        DECIMAL(9, 0),
             DT_INI_EFF          DATE NOT NULL,
             DT_END_EFF          DATE NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             COD_SOGG            CHAR(20),
             TP_RAPP_ANA         CHAR(2) NOT NULL,
             TP_PERS             CHAR(1),
             SEX                 CHAR(1),
             DT_NASC             DATE,
             FL_ESTAS            CHAR(1),
             INDIR_1             CHAR(20),
             INDIR_2             CHAR(20),
             INDIR_3             CHAR(20),
             TP_UTLZ_INDIR_1     CHAR(2),
             TP_UTLZ_INDIR_2     CHAR(2),
             TP_UTLZ_INDIR_3     CHAR(2),
             ESTR_CNT_CORR_ACCR  CHAR(20),
             ESTR_CNT_CORR_ADD   CHAR(20),
             ESTR_DOCTO          CHAR(20),
             PC_NEL_RAPP         DECIMAL(6, 3),
             TP_MEZ_PAG_ADD      CHAR(2),
             TP_MEZ_PAG_ACCR     CHAR(2),
             COD_MATR            CHAR(20),
             TP_ADEGZ            CHAR(2),
             FL_TST_RSH          CHAR(1),
             COD_AZ              CHAR(30),
             IND_PRINC           CHAR(2),
             DT_DELIBERA_CDA     DATE,
             DLG_AL_RISC         CHAR(1),
             LEGALE_RAPPR_PRINC  CHAR(1),
             TP_LEGALE_RAPPR     CHAR(2),
             TP_IND_PRINC        CHAR(2),
             TP_STAT_RID         CHAR(2),
             NOME_INT_RID        VARCHAR(100),
             COGN_INT_RID        VARCHAR(100),
             COGN_INT_TRATT      VARCHAR(100),
             NOME_INT_TRATT      VARCHAR(100),
             CF_INT_RID          CHAR(16),
             FL_COINC_DIP_CNTR   CHAR(1),
             FL_COINC_INT_CNTR   CHAR(1),
             DT_DECES            DATE,
             FL_FUMATORE         CHAR(1),
             DS_RIGA             DECIMAL(10, 0) NOT NULL,
             DS_OPER_SQL         CHAR(1) NOT NULL,
             DS_VER              DECIMAL(9, 0) NOT NULL,
             DS_TS_INI_CPTZ      DECIMAL(18, 0) NOT NULL,
             DS_TS_END_CPTZ      DECIMAL(18, 0) NOT NULL,
             DS_UTENTE           CHAR(20) NOT NULL,
             DS_STATO_ELAB       CHAR(1) NOT NULL,
             FL_LAV_DIP          CHAR(1),
             TP_VARZ_PAGAT       CHAR(2),
             COD_RID             CHAR(11),
             TP_CAUS_RID         CHAR(2),
             IND_MASSA_CORP      CHAR(2),
             CAT_RSH_PROF        CHAR(2)
          ) END-EXEC.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA STRA_DI_INVST
      *   ALIAS SDI
      *   ULTIMO AGG. 07 DIC 2017
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-STRA-DI-INVST PIC S9(9)     COMP-3.
             07 (SF)-ID-OGG PIC S9(9)     COMP-3.
             07 (SF)-TP-OGG PIC X(2).
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-STRA PIC X(30).
             07 (SF)-COD-STRA-NULL REDEFINES
                (SF)-COD-STRA   PIC X(30).
             07 (SF)-MOD-GEST PIC S9(2)     COMP-3.
             07 (SF)-MOD-GEST-NULL REDEFINES
                (SF)-MOD-GEST   PIC X(2).
             07 (SF)-VAR-RIFTO PIC X(12).
             07 (SF)-VAR-RIFTO-NULL REDEFINES
                (SF)-VAR-RIFTO   PIC X(12).
             07 (SF)-VAL-VAR-RIFTO PIC X(12).
             07 (SF)-VAL-VAR-RIFTO-NULL REDEFINES
                (SF)-VAL-VAR-RIFTO   PIC X(12).
             07 (SF)-DT-FIS PIC S9(4)     COMP-3.
             07 (SF)-DT-FIS-NULL REDEFINES
                (SF)-DT-FIS   PIC X(3).
             07 (SF)-FRQ-VALUT PIC S9(5)     COMP-3.
             07 (SF)-FRQ-VALUT-NULL REDEFINES
                (SF)-FRQ-VALUT   PIC X(3).
             07 (SF)-FL-INI-END-PER PIC X(1).
             07 (SF)-FL-INI-END-PER-NULL REDEFINES
                (SF)-FL-INI-END-PER   PIC X(1).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-TP-STRA PIC X(2).
             07 (SF)-TP-STRA-NULL REDEFINES
                (SF)-TP-STRA   PIC X(2).
             07 (SF)-TP-PROVZA-STRA PIC X(2).
             07 (SF)-TP-PROVZA-STRA-NULL REDEFINES
                (SF)-TP-PROVZA-STRA   PIC X(2).
             07 (SF)-PC-PROTEZIONE PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-PROTEZIONE-NULL REDEFINES
                (SF)-PC-PROTEZIONE   PIC X(4).

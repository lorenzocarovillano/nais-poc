       01 TEMPORARY-DATA.
         05 D08-ID-TEMPORARY-DATA PIC S9(10)V     COMP-3.
         05 D08-ALIAS-STR-DATO PIC X(30).
         05 D08-NUM-FRAME PIC S9(5)V     COMP-3.
         05 D08-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 D08-ID-SESSION PIC X(20).
         05 D08-FL-CONTIGUOUS-DATA PIC X(1).
         05 D08-FL-CONTIGUOUS-DATA-NULL REDEFINES
            D08-FL-CONTIGUOUS-DATA   PIC X(1).
         05 D08-TOTAL-RECURRENCE PIC S9(5)V     COMP-3.
         05 D08-TOTAL-RECURRENCE-NULL REDEFINES
            D08-TOTAL-RECURRENCE   PIC X(3).
         05 D08-PARTIAL-RECURRENCE PIC S9(5)V     COMP-3.
         05 D08-PARTIAL-RECURRENCE-NULL REDEFINES
            D08-PARTIAL-RECURRENCE   PIC X(3).
         05 D08-ACTUAL-RECURRENCE PIC S9(5)V     COMP-3.
         05 D08-ACTUAL-RECURRENCE-NULL REDEFINES
            D08-ACTUAL-RECURRENCE   PIC X(3).
         05 D08-DS-OPER-SQL PIC X(1).
         05 D08-DS-VER PIC S9(9)V     COMP-3.
         05 D08-DS-TS-CPTZ PIC S9(18)V     COMP-3.
         05 D08-DS-UTENTE PIC X(20).
         05 D08-DS-STATO-ELAB PIC X(1).
         05 D08-TYPE-RECORD PIC X(3).
         05 D08-TYPE-RECORD-NULL REDEFINES
            D08-TYPE-RECORD   PIC X(3).
         05 D08-BUFFER-DATA-VCHAR.
           49 D08-BUFFER-DATA-LEN PIC S9(4) COMP-5.
           49 D08-BUFFER-DATA PIC X(32000).


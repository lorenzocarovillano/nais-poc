           EXEC SQL DECLARE BTC_JOB_SCHEDULE TABLE
           (
             ID_BATCH            INTEGER NOT NULL,
             ID_JOB              INTEGER NOT NULL,
             COD_ELAB_STATE      CHAR(1) NOT NULL,
             FLAG_WARNINGS       CHAR(1),
             DT_START            TIMESTAMP,
             DT_END              TIMESTAMP,
             USER_START          CHAR(30),
             FLAG_ALWAYS_EXE     CHAR(1),
             EXECUTIONS_COUNT    INTEGER,
             DATA_CONTENT_TYPE   CHAR(30),
             COD_MACROFUNCT      CHAR(2),
             TP_MOVI             DECIMAL(5, 0),
             IB_OGG              CHAR(100),
             DATA                VARCHAR(32000)
          ) END-EXEC.

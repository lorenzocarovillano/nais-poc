      ******************************************************************
      *    TP_COLLGM
      ******************************************************************
       01  WS-TP-COLLGM                     PIC X(002) VALUE SPACES.
           88 TRASFORMAZIONE                           VALUE 'TR'.
           88 ABBINAMENTO                              VALUE 'AB'.
           88 CONTINUAZIONE                            VALUE 'CO'.
           88 TRASFERIMENTO-TESTA-POSIZIONE            VALUE 'TT'.
           88 POLIZZA-DA-REINVESTIMENTO                VALUE 'PR'.
           88 VERSAMENTO-DA-REINVESTIMENTO             VALUE 'VR'.
           88 CONGUAGLIO-MANAGMENT-FEE                 VALUE 'CM'.
           88 RECUP-PROVV-STORNI-ALRE-POL              VALUE 'RS'.
           88 RECUP-PROVV-EMISS-ALRE-POL               VALUE 'RE'.
           88 PIANO-REALTA-ABILITATO                   VALUE 'RA'.
           88 PIANO-REALTA-DISABILITATO                VALUE 'RD'.

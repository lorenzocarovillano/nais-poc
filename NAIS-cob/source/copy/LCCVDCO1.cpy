      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA D_COLL
      *   ALIAS DCO
      *   ULTIMO AGG. 02 SET 2008
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-D-COLL PIC S9(9)     COMP-3.
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-IMP-ARROT-PRE PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-ARROT-PRE-NULL REDEFINES
                (SF)-IMP-ARROT-PRE   PIC X(8).
             07 (SF)-PC-SCON PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-SCON-NULL REDEFINES
                (SF)-PC-SCON   PIC X(4).
             07 (SF)-FL-ADES-SING PIC X(1).
             07 (SF)-FL-ADES-SING-NULL REDEFINES
                (SF)-FL-ADES-SING   PIC X(1).
             07 (SF)-TP-IMP PIC X(2).
             07 (SF)-TP-IMP-NULL REDEFINES
                (SF)-TP-IMP   PIC X(2).
             07 (SF)-FL-RICL-PRE-DA-CPT PIC X(1).
             07 (SF)-FL-RICL-PRE-DA-CPT-NULL REDEFINES
                (SF)-FL-RICL-PRE-DA-CPT   PIC X(1).
             07 (SF)-TP-ADES PIC X(2).
             07 (SF)-TP-ADES-NULL REDEFINES
                (SF)-TP-ADES   PIC X(2).
             07 (SF)-DT-ULT-RINN-TAC   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-RINN-TAC-NULL REDEFINES
                (SF)-DT-ULT-RINN-TAC   PIC X(5).
             07 (SF)-IMP-SCON PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-SCON-NULL REDEFINES
                (SF)-IMP-SCON   PIC X(8).
             07 (SF)-FRAZ-DFLT PIC S9(5)     COMP-3.
             07 (SF)-FRAZ-DFLT-NULL REDEFINES
                (SF)-FRAZ-DFLT   PIC X(3).
             07 (SF)-ETA-SCAD-MASC-DFLT PIC S9(5)     COMP-3.
             07 (SF)-ETA-SCAD-MASC-DFLT-NULL REDEFINES
                (SF)-ETA-SCAD-MASC-DFLT   PIC X(3).
             07 (SF)-ETA-SCAD-FEMM-DFLT PIC S9(5)     COMP-3.
             07 (SF)-ETA-SCAD-FEMM-DFLT-NULL REDEFINES
                (SF)-ETA-SCAD-FEMM-DFLT   PIC X(3).
             07 (SF)-TP-DFLT-DUR PIC X(2).
             07 (SF)-TP-DFLT-DUR-NULL REDEFINES
                (SF)-TP-DFLT-DUR   PIC X(2).
             07 (SF)-DUR-AA-ADES-DFLT PIC S9(5)     COMP-3.
             07 (SF)-DUR-AA-ADES-DFLT-NULL REDEFINES
                (SF)-DUR-AA-ADES-DFLT   PIC X(3).
             07 (SF)-DUR-MM-ADES-DFLT PIC S9(5)     COMP-3.
             07 (SF)-DUR-MM-ADES-DFLT-NULL REDEFINES
                (SF)-DUR-MM-ADES-DFLT   PIC X(3).
             07 (SF)-DUR-GG-ADES-DFLT PIC S9(5)     COMP-3.
             07 (SF)-DUR-GG-ADES-DFLT-NULL REDEFINES
                (SF)-DUR-GG-ADES-DFLT   PIC X(3).
             07 (SF)-DT-SCAD-ADES-DFLT   PIC S9(8) COMP-3.
             07 (SF)-DT-SCAD-ADES-DFLT-NULL REDEFINES
                (SF)-DT-SCAD-ADES-DFLT   PIC X(5).
             07 (SF)-COD-FND-DFLT PIC X(12).
             07 (SF)-COD-FND-DFLT-NULL REDEFINES
                (SF)-COD-FND-DFLT   PIC X(12).
             07 (SF)-TP-DUR PIC X(2).
             07 (SF)-TP-DUR-NULL REDEFINES
                (SF)-TP-DUR   PIC X(2).
             07 (SF)-TP-CALC-DUR PIC X(12).
             07 (SF)-TP-CALC-DUR-NULL REDEFINES
                (SF)-TP-CALC-DUR   PIC X(12).
             07 (SF)-FL-NO-ADERENTI PIC X(1).
             07 (SF)-FL-NO-ADERENTI-NULL REDEFINES
                (SF)-FL-NO-ADERENTI   PIC X(1).
             07 (SF)-FL-DISTINTA-CNBTVA PIC X(1).
             07 (SF)-FL-DISTINTA-CNBTVA-NULL REDEFINES
                (SF)-FL-DISTINTA-CNBTVA   PIC X(1).
             07 (SF)-FL-CNBT-AUTES PIC X(1).
             07 (SF)-FL-CNBT-AUTES-NULL REDEFINES
                (SF)-FL-CNBT-AUTES   PIC X(1).
             07 (SF)-FL-QTZ-POST-EMIS PIC X(1).
             07 (SF)-FL-QTZ-POST-EMIS-NULL REDEFINES
                (SF)-FL-QTZ-POST-EMIS   PIC X(1).
             07 (SF)-FL-COMNZ-FND-IS PIC X(1).
             07 (SF)-FL-COMNZ-FND-IS-NULL REDEFINES
                (SF)-FL-COMNZ-FND-IS   PIC X(1).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).

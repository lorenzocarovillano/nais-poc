      *----------------------------------------------------------------*
      *    COPY      ..... LCCVRAN6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO RAPPORTO ANAGRAFICA
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-RAPP-ANA.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE RAPP-ANA

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WRAN-ST-INV(IX-TAB-RAN)
              AND NOT WRAN-ST-CON(IX-TAB-RAN)
              AND WRAN-ELE-RAPP-ANAG-MAX NOT = 0

      *--->   Impostare ID Tabella RAPPORTO ANAGRAFICO
              MOVE WMOV-ID-PTF              TO RAN-ID-MOVI-CRZ

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WRAN-ST-ADD(IX-TAB-RAN)

                       IF IDSV0001-ESITO-OK
                          MOVE WRAN-ID-PTF(IX-TAB-RAN)
                            TO RAN-ID-RAPP-ANA

                          IF WRAN-TP-OGG(IX-TAB-RAN) = 'LI'
                             MOVE WRAN-ID-OGG(IX-TAB-RAN) TO RAN-ID-OGG
                          ELSE
      *-->                   Estrazione Oggetto
                             PERFORM PREPARA-AREA-LCCS0234-RAN
                                THRU PREPARA-AREA-LCCS0234-RAN-EX

                             PERFORM CALL-LCCS0234
                                THRU CALL-LCCS0234-EX
                             IF IDSV0001-ESITO-OK
                                MOVE S234-ID-OGG-PTF-EOC  TO RAN-ID-OGG
                             END-IF
                          END-IF
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WRAN-ST-DEL(IX-TAB-RAN)

                       MOVE WRAN-ID-PTF(IX-TAB-RAN)   TO RAN-ID-RAPP-ANA
                       MOVE WRAN-ID-OGG(IX-TAB-RAN)   TO RAN-ID-OGG

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->        MODIFICA
                  WHEN WRAN-ST-MOD(IX-TAB-RAN)
                       MOVE WRAN-ID-PTF(IX-TAB-RAN)   TO RAN-ID-RAPP-ANA
                       MOVE WRAN-ID-OGG(IX-TAB-RAN)   TO RAN-ID-OGG

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN RAPPORTO ANAGRAFICO
                 PERFORM VAL-DCLGEN-RAN
                    THRU VAL-DCLGEN-RAN-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-RAN
                    THRU VALORIZZA-AREA-DSH-RAN-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-RAPP-ANA-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-RAN.

      *--> DCLGEN TABELLA
           MOVE RAPP-ANA               TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-RAN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    PREPARA AREA PER CALL LCCS0234
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234-RAN.

           MOVE WRAN-ID-OGG(IX-TAB-RAN)    TO S234-ID-OGG-EOC.
           MOVE WRAN-TP-OGG(IX-TAB-RAN)    TO S234-TIPO-OGG-EOC.

       PREPARA-AREA-LCCS0234-RAN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    CREAZIONE LEGAMI E COLLEGAMENTO TRA LA PERSONA GIURIDICA E
      *    LEGALE RAPPRESENTANTE.
      *    LE AZIONI SOPRA DESCRITTE VERRANNIO ESEGUITE NEI SEGUENTI 2
      *    STEP:
      *    1) ESTRAZIONE SEQUENCE PER I NUOVI RAPP-ANA
      *    2) CREAZIONE DEI COLLEGAMENTI TRA LA PERSONA GIURIDICA E
      *       IL LEGALE RAPPRESENTANTE.
      *----------------------------------------------------------------*
       CREA-COLL-RAPP-ANA.
      *
      *    ESTRAZIONE SEQUENCE PER I NUOVI RAPP-ANA
      *
      *--> NOME TABELLA FISICA DB
           MOVE 'RAPP-ANA'                  TO WK-TABELLA.

           PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
             UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX
                OR IDSV0001-ESITO-KO

                   IF WRAN-ST-ADD(IX-TAB-RAN)
                      PERFORM ESTR-SEQUENCE
                         THRU ESTR-SEQUENCE-EX
                      IF IDSV0001-ESITO-OK
                         MOVE S090-SEQ-TABELLA
                           TO WRAN-ID-PTF(IX-TAB-RAN)
                      END-IF
                   END-IF

           END-PERFORM.
      *
      *    CREAZIONE DEI COLLEGAMENTI TRA LA PERSONA GIURIDICA E
      *    IL LEGALE RAPPRESENTANTE.
      *
           PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
             UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX
                OR IDSV0001-ESITO-KO

                   MOVE WRAN-TP-RAPP-ANA(IX-TAB-RAN)
                     TO WS-TP-RAPP-ANA

                   IF LEGALE-RAPPR
                   OR PERCIP-DISPO
14835              OR PERCIP-LIQUI
AMLF2              OR TITOLARE-EFFETTIVO-BEN
                      PERFORM RICERCA-ID-COLLG
                         THRU RICERCA-ID-COLLG-EX
                   END-IF

           END-PERFORM.
      *
       CREA-COLL-RAPP-ANA-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RICERCA ID_RAPP_ANA COLLEGATO
      *----------------------------------------------------------------*
       RICERCA-ID-COLLG.

           PERFORM VARYING IX-COLL-RAN FROM 1 BY 1
             UNTIL IX-COLL-RAN > WRAN-ELE-RAPP-ANAG-MAX
                OR IDSV0001-ESITO-KO

                   IF WRAN-ID-RAPP-ANA-COLLG-NULL(IX-TAB-RAN) NOT =
                      HIGH-VALUES
                      IF WRAN-ID-RAPP-ANA(IX-COLL-RAN) =
                         WRAN-ID-RAPP-ANA-COLLG(IX-TAB-RAN)
                         MOVE WRAN-ID-PTF(IX-COLL-RAN)
                           TO WRAN-ID-RAPP-ANA-COLLG(IX-TAB-RAN)
                         MOVE WRAN-ELE-RAPP-ANAG-MAX
                           TO IX-COLL-RAN
                      END-IF
                   END-IF
           END-PERFORM.

       RICERCA-ID-COLLG-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVRAN5.

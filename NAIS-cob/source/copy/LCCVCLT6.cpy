      *----------------------------------------------------------------*
      *    COPY      ..... LCCVCLT6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO CLAUSOLA TESTUALE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVCLT5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCVCLT1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-CLAU-TXT.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE CLAU-TXT

      *--> NOME TABELLA FISICA DB
           MOVE 'CLAU-TXT'                  TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WCLT-ST-INV(IX-TAB-CLT)
              AND WCLT-ELE-CLAU-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WCLT-ST-ADD(IX-TAB-CLT)

      *-->             ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX
                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA
                            TO WCLT-ID-PTF(IX-TAB-CLT)
                               CLT-ID-CLAU-TXT
                       END-IF
      *
                       MOVE WCLT-TP-OGG(IX-TAB-CLT)
                         TO WS-TP-OGG
      *
                       IF VINCPEG
                          MOVE WCLT-ID-OGG(IX-TAB-CLT)
                            TO CLT-ID-OGG
                          MOVE WCLT-TP-OGG(IX-TAB-CLT)
                            TO CLT-TP-OGG
                          MOVE WMOV-ID-PTF
                            TO CLT-ID-MOVI-CRZ
                       ELSE
      *-->                PREPARAZIONE ED ESTRAZIONE OGGETTO PTF
                          PERFORM PREPARA-AREA-LCCS0234-CLT
                             THRU PREPARA-AREA-LCCS0234-CLT-EX

                          PERFORM CALL-LCCS0234
                             THRU CALL-LCCS0234-EX

                          IF IDSV0001-ESITO-OK
T                            MOVE S234-ID-OGG-PTF-EOC
T                              TO CLT-ID-OGG
                             MOVE WMOV-ID-PTF
                               TO CLT-ID-MOVI-CRZ
                          END-IF
                       END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WCLT-ST-MOD(IX-TAB-CLT)

                        MOVE WCLT-ID-PTF(IX-TAB-CLT)
                          TO CLT-ID-CLAU-TXT
                        MOVE WCLT-ID-OGG(IX-TAB-CLT)
                          TO CLT-ID-OGG
                        MOVE WMOV-ID-PTF
                          TO CLT-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WCLT-ST-DEL(IX-TAB-CLT)

                        MOVE WCLT-ID-PTF(IX-TAB-CLT)
                          TO CLT-ID-CLAU-TXT
                        MOVE WCLT-ID-OGG(IX-TAB-CLT)
                          TO CLT-ID-OGG
                        MOVE WMOV-ID-PTF
                          TO CLT-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN ADESIONE
                 PERFORM VAL-DCLGEN-CLT
                    THRU VAL-DCLGEN-CLT-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-CLT
                    THRU VALORIZZA-AREA-DSH-CLT-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-CLAU-TXT-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-CLT.

      *--> DCLGEN TABELLA
           MOVE CLAU-TXT               TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-CLT-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   PREPARA PER LA CALL ALL'LCCS0234 ESTRATTORE OGGETTO PTF
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234-CLT.

           MOVE WCLT-ID-OGG(IX-TAB-CLT)      TO S234-ID-OGG-EOC
           MOVE WCLT-TP-OGG(IX-TAB-CLT)      TO S234-TIPO-OGG-EOC.

       PREPARA-AREA-LCCS0234-CLT-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RICERCA DELL'ID-CLAU
      *----------------------------------------------------------------*
       RICERCA-ID-CLAU.

           SET NON-TROVATO        TO TRUE.

           PERFORM VARYING IX-TAB-CLT FROM 1 BY 1
             UNTIL IX-TAB-CLT > WCLT-ELE-CLAU-MAX
                OR TROVATO

                IF WS-ID-CLAU = WCLT-ID-CLAU-TXT(IX-TAB-CLT)
                   MOVE WCLT-ID-PTF(IX-TAB-CLT)
                                  TO WS-ID-PTF-CLT
                   SET  TROVATO   TO TRUE
                END-IF
           END-PERFORM.

       RICERCA-ID-CLAU-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVCLT5.

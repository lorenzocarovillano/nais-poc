      *----------------------------------------------------------------*
      *    ESTRATTO CONTO - FASE DIAGNOSTICO
      *       - CONTROLLI AD HOC MULTIRAMO
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *   CONTROLLO 34                                                 *
      *----------------------------------------------------------------*
       C0060-CNTRL-34.

           MOVE 'C0060-CNTRL-34'          TO WK-LABEL.

           IF CNTRL-34-ERR-NO
               CONTINUE
           ELSE
MOD            IF  CNTRL-34-SI
               MOVE 34 TO WK-CONTROLLO
               SET CNTRL-34        TO TRUE
               MOVE MSG-ERRORE     TO WS-ERRORE
               PERFORM E001-SCARTO THRU EX-E001
MOD            END-IF
           END-IF.

       EX-C0060.
           EXIT.



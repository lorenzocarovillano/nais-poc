
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVBEP4
      *   ULTIMO AGG. 09 AGO 2018
      *------------------------------------------------------------

       INIZIA-TOT-BEP.

           PERFORM INIZIA-ZEROES-BEP THRU INIZIA-ZEROES-BEP-EX

           PERFORM INIZIA-SPACES-BEP THRU INIZIA-SPACES-BEP-EX

           PERFORM INIZIA-NULL-BEP THRU INIZIA-NULL-BEP-EX.

       INIZIA-TOT-BEP-EX.
           EXIT.

       INIZIA-NULL-BEP.
           MOVE HIGH-VALUES TO (SF)-ID-BNFICR-NULL(IX-TAB-BEP)
           MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-BEP)
           MOVE HIGH-VALUES TO (SF)-COD-BNFIC-NULL(IX-TAB-BEP)
           MOVE HIGH-VALUES TO (SF)-COD-BNFICR-NULL(IX-TAB-BEP)
           MOVE HIGH-VALUES TO (SF)-DESC-BNFICR(IX-TAB-BEP)
           MOVE HIGH-VALUES TO (SF)-PC-DEL-BNFICR-NULL(IX-TAB-BEP)
           MOVE HIGH-VALUES TO (SF)-FL-ESE-NULL(IX-TAB-BEP)
           MOVE HIGH-VALUES TO (SF)-FL-IRREV-NULL(IX-TAB-BEP)
           MOVE HIGH-VALUES TO (SF)-FL-DFLT-NULL(IX-TAB-BEP)
           MOVE HIGH-VALUES TO (SF)-ESRCN-ATTVT-IMPRS-NULL(IX-TAB-BEP)
           MOVE HIGH-VALUES TO (SF)-TP-NORMAL-BNFIC-NULL(IX-TAB-BEP).
       INIZIA-NULL-BEP-EX.
           EXIT.

       INIZIA-ZEROES-BEP.
           MOVE 0 TO (SF)-ID-BNFIC(IX-TAB-BEP)
           MOVE 0 TO (SF)-ID-RAPP-ANA(IX-TAB-BEP)
           MOVE 0 TO (SF)-ID-MOVI-CRZ(IX-TAB-BEP)
           MOVE 0 TO (SF)-DT-INI-EFF(IX-TAB-BEP)
           MOVE 0 TO (SF)-DT-END-EFF(IX-TAB-BEP)
           MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-BEP)
           MOVE 0 TO (SF)-DS-RIGA(IX-TAB-BEP)
           MOVE 0 TO (SF)-DS-VER(IX-TAB-BEP)
           MOVE 0 TO (SF)-DS-TS-INI-CPTZ(IX-TAB-BEP)
           MOVE 0 TO (SF)-DS-TS-END-CPTZ(IX-TAB-BEP).
       INIZIA-ZEROES-BEP-EX.
           EXIT.

       INIZIA-SPACES-BEP.
           MOVE SPACES TO (SF)-TP-IND-BNFICR(IX-TAB-BEP)
           MOVE SPACES TO (SF)-FL-BNFICR-COLL(IX-TAB-BEP)
           MOVE SPACES TO (SF)-DS-OPER-SQL(IX-TAB-BEP)
           MOVE SPACES TO (SF)-DS-UTENTE(IX-TAB-BEP)
           MOVE SPACES TO (SF)-DS-STATO-ELAB(IX-TAB-BEP).
       INIZIA-SPACES-BEP-EX.
           EXIT.

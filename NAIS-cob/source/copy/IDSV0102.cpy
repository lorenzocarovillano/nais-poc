      ******************************************************************
      *   area cache x lettura CSV
      ******************************************************************
          05 IDSV0102-LIMITE-MAX                   PIC 9(05) VALUE 4000.
          05 IDSV0102-ELE-MAX-COD-STR-DATO         PIC S9(05) COMP.
      * CONTA-CACHE-CSV : numero di entit� CSV trovate nell'area
      *                   di cache
          05 IDSV0102-CONTA-CACHE-CSV              PIC S9(05) COMP.
          05 IDSV0102-STR-COD-STR-DATO.
             10 IDSV0102-AREA-COD-STR-DATO         OCCURS 4000
                                  INDEXED BY IDSV0102-SEARCH-INDEX.
                15 IDSV0102-COD-STR-DATO           PIC X(030).
                15 IDSV0102-CODICE-DATO            PIC X(030).
                15 IDSV0102-TIPO-DATO              PIC X(002).
                15 IDSV0102-INI-POSI               PIC 9(05) COMP.
                15 IDSV0102-LUN-DATO               PIC 9(05) COMP.
                15 IDSV0102-LUNGHEZZA-DATO-NOM     PIC 9(05) COMP.
                15 IDSV0102-PRECISIONE-DATO        PIC 9(02) COMP.
          05 IDSV0102-STR-COD-STR-DATO-R REDEFINES
             IDSV0102-STR-COD-STR-DATO.
             10 FILLER                             PIC X(76).
             10 IDSV0102-RESTO-TABELLA             PIC X(303924).
      ******************************************************************
      *   area cache x lettura ADA - MVV
      ******************************************************************
          05 IDSV0102A-LIMITE-MAX                  PIC 9(05) VALUE 300.
          05 IDSV0102A-ELE-MAX-ACTU                PIC S9(05) COMP.
      * CONTA-CACHE-ADA-MVV : numero di entit� ADA-MVV trovate nell'area
      *                       di cache
          05 IDSV0102A-CONTA-CACHE-ADA-MVV         PIC S9(05) COMP.
          05 IDSV0102A-ADA-MVV.
             10 IDSV0102A-TAB-PARAM                OCCURS 300
                                  INDEXED BY IDSV0102A-SEARCH-INDEX.
      *- area ADA
                15 IDSV0102A-TIPO-DATO             PIC X(2).
                15 IDSV0102A-TIPO-DATO-NULL REDEFINES
                   IDSV0102A-TIPO-DATO             PIC X(2).
                15 IDSV0102A-LUNGHEZZA-DATO        PIC S9(5) COMP-3.
                15 IDSV0102A-LUNGHEZZA-DATO-NULL REDEFINES
                   IDSV0102A-LUNGHEZZA-DATO        PIC X(3).
                15 IDSV0102A-PRECISIONE-DATO       PIC S9(2) COMP-3.
                15 IDSV0102A-PRECISIONE-DATO-NULL REDEFINES
                   IDSV0102A-PRECISIONE-DATO       PIC X(2).
                15 IDSV0102A-FORMATTAZIONE-DATO    PIC X(20).
      *-> area MVV
                15 IDSV0102A-ID-MATR-VAL-VAR       PIC S9(9) COMP-5.
                15 IDSV0102A-IDP-MATR-VAL-VAR      PIC S9(9) COMP-5.
                15 IDSV0102A-IDP-MAT-VAL-VAR-NULL  REDEFINES
                   IDSV0102A-IDP-MATR-VAL-VAR      PIC X(4).
                15 IDSV0102A-TIPO-MOVIMENTO        PIC S9(5) COMP-3.
                15 IDSV0102A-TIPO-MOVIMENTO-NULL REDEFINES
                   IDSV0102A-TIPO-MOVIMENTO        PIC X(3).
                15 IDSV0102A-COD-DATO-EXT          PIC X(30).
                15 IDSV0102A-OBBLIGATORIETA        PIC X(1).
                15 IDSV0102A-OBBLIGATORIETA-NULL REDEFINES
                   IDSV0102A-OBBLIGATORIETA        PIC X(1).
                15 IDSV0102A-VALORE-DEFAULT        PIC X(50).
                15 IDSV0102A-COD-STR-DATO-PTF      PIC X(30).
                15 IDSV0102A-COD-DATO-PTF          PIC X(30).
                15 IDSV0102A-COD-PARAMETRO         PIC X(20).
                15 IDSV0102A-OPERAZIONE            PIC X(15).
                15 IDSV0102A-LIV-OPERAZIONE        PIC X(3).
                15 IDSV0102A-LIV-OPERAZIONE-NULL REDEFINES
                   IDSV0102A-LIV-OPERAZIONE        PIC X(3).
                15 IDSV0102A-TIPO-OGGETTO          PIC X(2).
                15 IDSV0102A-TIPO-OGGETTO-NULL REDEFINES
                   IDSV0102A-TIPO-OGGETTO          PIC X(2).
                15 IDSV0102A-WHERE-CONDITION       PIC X(300).
                15 IDSV0102A-SERVIZIO-LETTURA      PIC X(8).
                15 IDSV0102A-SERV-LETTURA-NULL     REDEFINES
                   IDSV0102A-SERVIZIO-LETTURA      PIC X(8).
                15 IDSV0102A-MODULO-CALCOLO        PIC X(8).
                15 IDSV0102A-MODULO-CALCOLO-NULL REDEFINES
                   IDSV0102A-MODULO-CALCOLO        PIC X(8).
                15 IDSV0102A-COD-DATO-INTERNO      PIC X(30).
          05 IDSV0102A-ADA-MVV-R REDEFINES
             IDSV0102A-ADA-MVV.
             10 FILLER                             PIC X(565).
             10 IDSV0102A-RESTO-TABELLA            PIC X(168935).



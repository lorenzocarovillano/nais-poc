       01 RAPP-ANA.
         05 RAN-ID-RAPP-ANA PIC S9(9)V     COMP-3.
         05 RAN-ID-RAPP-ANA-COLLG PIC S9(9)V     COMP-3.
         05 RAN-ID-RAPP-ANA-COLLG-NULL REDEFINES
            RAN-ID-RAPP-ANA-COLLG   PIC X(5).
         05 RAN-ID-OGG PIC S9(9)V     COMP-3.
         05 RAN-TP-OGG PIC X(2).
         05 RAN-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
         05 RAN-ID-MOVI-CHIU PIC S9(9)V     COMP-3.
         05 RAN-ID-MOVI-CHIU-NULL REDEFINES
            RAN-ID-MOVI-CHIU   PIC X(5).
         05 RAN-DT-INI-EFF   PIC S9(8)V COMP-3.
         05 RAN-DT-END-EFF   PIC S9(8)V COMP-3.
         05 RAN-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 RAN-COD-SOGG PIC X(20).
         05 RAN-COD-SOGG-NULL REDEFINES
            RAN-COD-SOGG   PIC X(20).
         05 RAN-TP-RAPP-ANA PIC X(2).
         05 RAN-TP-PERS PIC X(1).
         05 RAN-TP-PERS-NULL REDEFINES
            RAN-TP-PERS   PIC X(1).
         05 RAN-SEX PIC X(1).
         05 RAN-SEX-NULL REDEFINES
            RAN-SEX   PIC X(1).
         05 RAN-DT-NASC   PIC S9(8)V COMP-3.
         05 RAN-DT-NASC-NULL REDEFINES
            RAN-DT-NASC   PIC X(5).
         05 RAN-FL-ESTAS PIC X(1).
         05 RAN-FL-ESTAS-NULL REDEFINES
            RAN-FL-ESTAS   PIC X(1).
         05 RAN-INDIR-1 PIC X(20).
         05 RAN-INDIR-1-NULL REDEFINES
            RAN-INDIR-1   PIC X(20).
         05 RAN-INDIR-2 PIC X(20).
         05 RAN-INDIR-2-NULL REDEFINES
            RAN-INDIR-2   PIC X(20).
         05 RAN-INDIR-3 PIC X(20).
         05 RAN-INDIR-3-NULL REDEFINES
            RAN-INDIR-3   PIC X(20).
         05 RAN-TP-UTLZ-INDIR-1 PIC X(2).
         05 RAN-TP-UTLZ-INDIR-1-NULL REDEFINES
            RAN-TP-UTLZ-INDIR-1   PIC X(2).
         05 RAN-TP-UTLZ-INDIR-2 PIC X(2).
         05 RAN-TP-UTLZ-INDIR-2-NULL REDEFINES
            RAN-TP-UTLZ-INDIR-2   PIC X(2).
         05 RAN-TP-UTLZ-INDIR-3 PIC X(2).
         05 RAN-TP-UTLZ-INDIR-3-NULL REDEFINES
            RAN-TP-UTLZ-INDIR-3   PIC X(2).
         05 RAN-ESTR-CNT-CORR-ACCR PIC X(20).
         05 RAN-ESTR-CNT-CORR-ACCR-NULL REDEFINES
            RAN-ESTR-CNT-CORR-ACCR   PIC X(20).
         05 RAN-ESTR-CNT-CORR-ADD PIC X(20).
         05 RAN-ESTR-CNT-CORR-ADD-NULL REDEFINES
            RAN-ESTR-CNT-CORR-ADD   PIC X(20).
         05 RAN-ESTR-DOCTO PIC X(20).
         05 RAN-ESTR-DOCTO-NULL REDEFINES
            RAN-ESTR-DOCTO   PIC X(20).
         05 RAN-PC-NEL-RAPP PIC S9(3)V9(3) COMP-3.
         05 RAN-PC-NEL-RAPP-NULL REDEFINES
            RAN-PC-NEL-RAPP   PIC X(4).
         05 RAN-TP-MEZ-PAG-ADD PIC X(2).
         05 RAN-TP-MEZ-PAG-ADD-NULL REDEFINES
            RAN-TP-MEZ-PAG-ADD   PIC X(2).
         05 RAN-TP-MEZ-PAG-ACCR PIC X(2).
         05 RAN-TP-MEZ-PAG-ACCR-NULL REDEFINES
            RAN-TP-MEZ-PAG-ACCR   PIC X(2).
         05 RAN-COD-MATR PIC X(20).
         05 RAN-COD-MATR-NULL REDEFINES
            RAN-COD-MATR   PIC X(20).
         05 RAN-TP-ADEGZ PIC X(2).
         05 RAN-TP-ADEGZ-NULL REDEFINES
            RAN-TP-ADEGZ   PIC X(2).
         05 RAN-FL-TST-RSH PIC X(1).
         05 RAN-FL-TST-RSH-NULL REDEFINES
            RAN-FL-TST-RSH   PIC X(1).
         05 RAN-COD-AZ PIC X(30).
         05 RAN-COD-AZ-NULL REDEFINES
            RAN-COD-AZ   PIC X(30).
         05 RAN-IND-PRINC PIC X(2).
         05 RAN-IND-PRINC-NULL REDEFINES
            RAN-IND-PRINC   PIC X(2).
         05 RAN-DT-DELIBERA-CDA   PIC S9(8)V COMP-3.
         05 RAN-DT-DELIBERA-CDA-NULL REDEFINES
            RAN-DT-DELIBERA-CDA   PIC X(5).
         05 RAN-DLG-AL-RISC PIC X(1).
         05 RAN-DLG-AL-RISC-NULL REDEFINES
            RAN-DLG-AL-RISC   PIC X(1).
         05 RAN-LEGALE-RAPPR-PRINC PIC X(1).
         05 RAN-LEGALE-RAPPR-PRINC-NULL REDEFINES
            RAN-LEGALE-RAPPR-PRINC   PIC X(1).
         05 RAN-TP-LEGALE-RAPPR PIC X(2).
         05 RAN-TP-LEGALE-RAPPR-NULL REDEFINES
            RAN-TP-LEGALE-RAPPR   PIC X(2).
         05 RAN-TP-IND-PRINC PIC X(2).
         05 RAN-TP-IND-PRINC-NULL REDEFINES
            RAN-TP-IND-PRINC   PIC X(2).
         05 RAN-TP-STAT-RID PIC X(2).
         05 RAN-TP-STAT-RID-NULL REDEFINES
            RAN-TP-STAT-RID   PIC X(2).
         05 RAN-NOME-INT-RID-VCHAR.
           49 RAN-NOME-INT-RID-LEN PIC S9(4) COMP-5.
           49 RAN-NOME-INT-RID PIC X(100).
         05 RAN-COGN-INT-RID-VCHAR.
           49 RAN-COGN-INT-RID-LEN PIC S9(4) COMP-5.
           49 RAN-COGN-INT-RID PIC X(100).
         05 RAN-COGN-INT-TRATT-VCHAR.
           49 RAN-COGN-INT-TRATT-LEN PIC S9(4) COMP-5.
           49 RAN-COGN-INT-TRATT PIC X(100).
         05 RAN-NOME-INT-TRATT-VCHAR.
           49 RAN-NOME-INT-TRATT-LEN PIC S9(4) COMP-5.
           49 RAN-NOME-INT-TRATT PIC X(100).
         05 RAN-CF-INT-RID PIC X(16).
         05 RAN-CF-INT-RID-NULL REDEFINES
            RAN-CF-INT-RID   PIC X(16).
         05 RAN-FL-COINC-DIP-CNTR PIC X(1).
         05 RAN-FL-COINC-DIP-CNTR-NULL REDEFINES
            RAN-FL-COINC-DIP-CNTR   PIC X(1).
         05 RAN-FL-COINC-INT-CNTR PIC X(1).
         05 RAN-FL-COINC-INT-CNTR-NULL REDEFINES
            RAN-FL-COINC-INT-CNTR   PIC X(1).
         05 RAN-DT-DECES   PIC S9(8)V COMP-3.
         05 RAN-DT-DECES-NULL REDEFINES
            RAN-DT-DECES   PIC X(5).
         05 RAN-FL-FUMATORE PIC X(1).
         05 RAN-FL-FUMATORE-NULL REDEFINES
            RAN-FL-FUMATORE   PIC X(1).
         05 RAN-DS-RIGA PIC S9(10)V     COMP-3.
         05 RAN-DS-OPER-SQL PIC X(1).
         05 RAN-DS-VER PIC S9(9)V     COMP-3.
         05 RAN-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
         05 RAN-DS-TS-END-CPTZ PIC S9(18)V     COMP-3.
         05 RAN-DS-UTENTE PIC X(20).
         05 RAN-DS-STATO-ELAB PIC X(1).
         05 RAN-FL-LAV-DIP PIC X(1).
         05 RAN-FL-LAV-DIP-NULL REDEFINES
            RAN-FL-LAV-DIP   PIC X(1).
         05 RAN-TP-VARZ-PAGAT PIC X(2).
         05 RAN-TP-VARZ-PAGAT-NULL REDEFINES
            RAN-TP-VARZ-PAGAT   PIC X(2).
         05 RAN-COD-RID PIC X(11).
         05 RAN-COD-RID-NULL REDEFINES
            RAN-COD-RID   PIC X(11).
         05 RAN-TP-CAUS-RID PIC X(2).
         05 RAN-TP-CAUS-RID-NULL REDEFINES
            RAN-TP-CAUS-RID   PIC X(2).
         05 RAN-IND-MASSA-CORP PIC X(2).
         05 RAN-IND-MASSA-CORP-NULL REDEFINES
            RAN-IND-MASSA-CORP   PIC X(2).
         05 RAN-CAT-RSH-PROF PIC X(2).
         05 RAN-CAT-RSH-PROF-NULL REDEFINES
            RAN-CAT-RSH-PROF   PIC X(2).


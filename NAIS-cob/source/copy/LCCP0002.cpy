      *----------------------------------------------------------------*
      *    COPY      ..... LCCP0002
      *    TIPOLOGIA...... COPY PROCEDURE (COMPONENTI COMUNI)
      *    DESCRIZIONE.... ESTRAZIONE SEQUENCE
      *----------------------------------------------------------------*

      *----------------------------------------------------------------*
      *    ESTRAZIONE SEQUENCE
      *----------------------------------------------------------------*
       ESTR-SEQUENCE.

           MOVE WK-TABELLA TO S090-NOME-TABELLA.

           CALL LCCS0090 USING AREA-IO-LCCS0090
           ON EXCEPTION
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SERVIZIO ESTRAZIONE SEQUENCE'
                TO CALL-DESC
              MOVE 'ESTR-SEQUENCE'
                TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

           IF IDSV0001-ESITO-OK
              EVALUATE S090-RETURN-CODE
                  WHEN '00'
                       CONTINUE
                  WHEN 'S1'
      *-->ERRORE ESTRAZIONE SEQUENCE SULLA TABELLA $ - RC=$ - SQLCODE=$
                     MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1900-CALL-LCCS0090'
                                            TO IEAI9901-LABEL-ERR
                     MOVE '005015'          TO IEAI9901-COD-ERRORE
                     STRING WK-TABELLA        ';'
                            S090-RETURN-CODE  ';'
                            S090-SQLCODE
                     DELIMITED BY SIZE    INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
                  WHEN 'D3'
      *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
                     MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1900-CALL-LCCS0090'
                                            TO IEAI9901-LABEL-ERR
                     MOVE '005016'          TO IEAI9901-COD-ERRORE
                     STRING WK-TABELLA        ';'
                            S090-RETURN-CODE  ';'
                            S090-SQLCODE
                     DELIMITED BY SIZE    INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           END-IF.

       ESTR-SEQUENCE-EX.
           EXIT.

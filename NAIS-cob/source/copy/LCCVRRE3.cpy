
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVRRE3
      *   ULTIMO AGG. 01 OTT 2014
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-RRE.
           MOVE RRE-ID-RAPP-RETE
             TO (SF)-ID-PTF(IX-TAB-RRE)
           MOVE RRE-ID-RAPP-RETE
             TO (SF)-ID-RAPP-RETE(IX-TAB-RRE)
           IF RRE-ID-OGG-NULL = HIGH-VALUES
              MOVE RRE-ID-OGG-NULL
                TO (SF)-ID-OGG-NULL(IX-TAB-RRE)
           ELSE
              MOVE RRE-ID-OGG
                TO (SF)-ID-OGG(IX-TAB-RRE)
           END-IF
           MOVE RRE-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-RRE)
           MOVE RRE-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-RRE)
           IF RRE-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE RRE-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RRE)
           ELSE
              MOVE RRE-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-RRE)
           END-IF
           MOVE RRE-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-RRE)
           MOVE RRE-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-RRE)
           MOVE RRE-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-RRE)
           MOVE RRE-TP-RETE
             TO (SF)-TP-RETE(IX-TAB-RRE)
           IF RRE-TP-ACQS-CNTRT-NULL = HIGH-VALUES
              MOVE RRE-TP-ACQS-CNTRT-NULL
                TO (SF)-TP-ACQS-CNTRT-NULL(IX-TAB-RRE)
           ELSE
              MOVE RRE-TP-ACQS-CNTRT
                TO (SF)-TP-ACQS-CNTRT(IX-TAB-RRE)
           END-IF
           IF RRE-COD-ACQS-CNTRT-NULL = HIGH-VALUES
              MOVE RRE-COD-ACQS-CNTRT-NULL
                TO (SF)-COD-ACQS-CNTRT-NULL(IX-TAB-RRE)
           ELSE
              MOVE RRE-COD-ACQS-CNTRT
                TO (SF)-COD-ACQS-CNTRT(IX-TAB-RRE)
           END-IF
           IF RRE-COD-PNT-RETE-INI-NULL = HIGH-VALUES
              MOVE RRE-COD-PNT-RETE-INI-NULL
                TO (SF)-COD-PNT-RETE-INI-NULL(IX-TAB-RRE)
           ELSE
              MOVE RRE-COD-PNT-RETE-INI
                TO (SF)-COD-PNT-RETE-INI(IX-TAB-RRE)
           END-IF
           IF RRE-COD-PNT-RETE-END-NULL = HIGH-VALUES
              MOVE RRE-COD-PNT-RETE-END-NULL
                TO (SF)-COD-PNT-RETE-END-NULL(IX-TAB-RRE)
           ELSE
              MOVE RRE-COD-PNT-RETE-END
                TO (SF)-COD-PNT-RETE-END(IX-TAB-RRE)
           END-IF
           IF RRE-FL-PNT-RETE-1RIO-NULL = HIGH-VALUES
              MOVE RRE-FL-PNT-RETE-1RIO-NULL
                TO (SF)-FL-PNT-RETE-1RIO-NULL(IX-TAB-RRE)
           ELSE
              MOVE RRE-FL-PNT-RETE-1RIO
                TO (SF)-FL-PNT-RETE-1RIO(IX-TAB-RRE)
           END-IF
           IF RRE-COD-CAN-NULL = HIGH-VALUES
              MOVE RRE-COD-CAN-NULL
                TO (SF)-COD-CAN-NULL(IX-TAB-RRE)
           ELSE
              MOVE RRE-COD-CAN
                TO (SF)-COD-CAN(IX-TAB-RRE)
           END-IF
           MOVE RRE-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-RRE)
           MOVE RRE-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-RRE)
           MOVE RRE-DS-VER
             TO (SF)-DS-VER(IX-TAB-RRE)
           MOVE RRE-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-RRE)
           MOVE RRE-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-RRE)
           MOVE RRE-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-RRE)
           MOVE RRE-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-RRE)
           IF RRE-COD-PNT-RETE-INI-C-NULL = HIGH-VALUES
              MOVE RRE-COD-PNT-RETE-INI-C-NULL
                TO (SF)-COD-PNT-RETE-INI-C-NULL(IX-TAB-RRE)
           ELSE
              MOVE RRE-COD-PNT-RETE-INI-C
                TO (SF)-COD-PNT-RETE-INI-C(IX-TAB-RRE)
           END-IF
           IF RRE-MATR-OPRT-NULL = HIGH-VALUES
              MOVE RRE-MATR-OPRT-NULL
                TO (SF)-MATR-OPRT-NULL(IX-TAB-RRE)
           ELSE
              MOVE RRE-MATR-OPRT
                TO (SF)-MATR-OPRT(IX-TAB-RRE)
           END-IF.
       VALORIZZA-OUTPUT-RRE-EX.
           EXIT.

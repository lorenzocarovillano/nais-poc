       01 IDSV8888-LIVELLO-DEBUG               PIC 9(001) VALUE 0.
          88 IDSV8888-NO-DEBUG                 VALUE 0.
          88 IDSV8888-DEBUG-BASSO              VALUE 1.
          88 IDSV8888-DEBUG-MEDIO              VALUE 2.
          88 IDSV8888-DEBUG-ELEVATO            VALUE 3.
          88 IDSV8888-DEBUG-ESASPERATO         VALUE 4.
          88 IDSV8888-ANY-APPL-DBG             VALUE 1,
                                                     2,
                                                     3,
                                                     4.

          88 IDSV8888-ARCH-BATCH-DBG           VALUE 5.
          88 IDSV8888-COM-COB-JAV-DBG          VALUE 6.
          88 IDSV8888-STRESS-TEST-DBG          VALUE 7.
          88 IDSV8888-BUSINESS-DBG             VALUE 8.
          88 IDSV8888-TOT-TUNING-DBG           VALUE 9.
          88 IDSV8888-ANY-TUNING-DBG           VALUE 5,
                                                     6,
                                                     7,
                                                     8,
                                                     9.

       01 IDSV8888-CALL-TIMESTAMP        PIC X(08)
                                         VALUE 'IJCSTMSP'.
       01 IDSV8888-CALL-DISPLAY          PIC X(08)
                                         VALUE 'IDSS8880'.
       01 IDSV8888-DISPLAY-ADDRESS       POINTER.

      *-- IDENTIFICATIVO/FASE/NOME PGM/TIMESTAMP/INIZIO-FINE
       01 IDSV8888-STR-PERFORMANCE-DBG.
          10 FILLER                      PIC X(06)
                                         VALUE 'TUNING'.
          10 FILLER                      PIC X(01) VALUE '/'.
          10 IDSV8888-FASE               PIC X(04).
             88 IDSV8888-ARCH-BATCH      VALUE 'ARCH'.
             88 IDSV8888-COM-COB-JAV     VALUE 'JAVA'.
             88 IDSV8888-BUSINESS        VALUE 'BUSI'.
             88 IDSV8888-SONDA-S4        VALUE 'S4  '.
             88 IDSV8888-SONDA-S5        VALUE 'S5  '.
             88 IDSV8888-SONDA-S8        VALUE 'S8  '.
             88 IDSV8888-SONDA-S9        VALUE 'S9  '.
          10 FILLER                      PIC X(01) VALUE '/'.
          10 IDSV8888-NOME-PGM           PIC X(30).
          10 FILLER                     PIC X(01) VALUE '/'.
          10 IDSV8888-DESC-PGM           PIC X(23).
          10 FILLER                      PIC X(01) VALUE '/'.

          10 IDSV8888-TIMESTAMP          PIC X(26).

          10 FILLER                      PIC X(01) VALUE '/'.
          10 IDSV8888-STATO              PIC X(06).
             88 IDSV8888-INIZIO          VALUE 'INIZIO'.
             88 IDSV8888-FINE            VALUE 'FINE'.
          10 FILLER                      PIC X(01) VALUE '/'.
          10 IDSV8888-MODALITA-ESECUTIVA PIC X.
                 88 IDSV8888-ON-LINE     VALUE 'O'.
                 88 IDSV8888-BATCH       VALUE 'B'.
                 88 IDSV8888-BATCH-INFR  VALUE 'I'.
          10 FILLER                      PIC X(01) VALUE '/'.
          10 IDSV8888-USER-NAME          PIC X(020).


       01 IDSV8888-AREA-DISPLAY          PIC X(125) VALUE SPACES.

       01 FILLER                         PIC X(050).


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVL305
      *   ULTIMO AGG. 09 LUG 2009
      *------------------------------------------------------------

       VAL-DCLGEN-L30.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-L30) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-L30)
              TO L30-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-L30)
              TO L30-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-L30) NOT NUMERIC
              MOVE 0 TO L30-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-L30)
              TO L30-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-L30) NOT NUMERIC
              MOVE 0 TO L30-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-L30)
              TO L30-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-L30)
              TO L30-COD-COMP-ANIA
           MOVE (SF)-COD-RAMO(IX-TAB-L30)
              TO L30-COD-RAMO
           MOVE (SF)-IB-POLI(IX-TAB-L30)
              TO L30-IB-POLI
           IF (SF)-PR-KEY-SIST-ESTNO-NULL(IX-TAB-L30) = HIGH-VALUES
              MOVE (SF)-PR-KEY-SIST-ESTNO-NULL(IX-TAB-L30)
              TO L30-PR-KEY-SIST-ESTNO-NULL
           ELSE
              MOVE (SF)-PR-KEY-SIST-ESTNO(IX-TAB-L30)
              TO L30-PR-KEY-SIST-ESTNO
           END-IF
           IF (SF)-SEC-KEY-SIST-ESTNO-NULL(IX-TAB-L30) = HIGH-VALUES
              MOVE (SF)-SEC-KEY-SIST-ESTNO-NULL(IX-TAB-L30)
              TO L30-SEC-KEY-SIST-ESTNO-NULL
           ELSE
              MOVE (SF)-SEC-KEY-SIST-ESTNO(IX-TAB-L30)
              TO L30-SEC-KEY-SIST-ESTNO
           END-IF
           MOVE (SF)-COD-CAN(IX-TAB-L30)
              TO L30-COD-CAN
           MOVE (SF)-COD-AGE(IX-TAB-L30)
              TO L30-COD-AGE
           IF (SF)-COD-SUB-AGE-NULL(IX-TAB-L30) = HIGH-VALUES
              MOVE (SF)-COD-SUB-AGE-NULL(IX-TAB-L30)
              TO L30-COD-SUB-AGE-NULL
           ELSE
              MOVE (SF)-COD-SUB-AGE(IX-TAB-L30)
              TO L30-COD-SUB-AGE
           END-IF
           MOVE (SF)-IMP-RES(IX-TAB-L30)
              TO L30-IMP-RES
           MOVE (SF)-TP-LIQ(IX-TAB-L30)
              TO L30-TP-LIQ
           MOVE (SF)-TP-SIST-ESTNO(IX-TAB-L30)
              TO L30-TP-SIST-ESTNO
           MOVE (SF)-COD-FISC-PART-IVA(IX-TAB-L30)
              TO L30-COD-FISC-PART-IVA
           MOVE (SF)-COD-MOVI-LIQ(IX-TAB-L30)
              TO L30-COD-MOVI-LIQ
           IF (SF)-TP-INVST-LIQ-NULL(IX-TAB-L30) = HIGH-VALUES
              MOVE (SF)-TP-INVST-LIQ-NULL(IX-TAB-L30)
              TO L30-TP-INVST-LIQ-NULL
           ELSE
              MOVE (SF)-TP-INVST-LIQ(IX-TAB-L30)
              TO L30-TP-INVST-LIQ
           END-IF
           MOVE (SF)-IMP-TOT-LIQ(IX-TAB-L30)
              TO L30-IMP-TOT-LIQ
           IF (SF)-DS-RIGA(IX-TAB-L30) NOT NUMERIC
              MOVE 0 TO L30-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-L30)
              TO L30-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-L30)
              TO L30-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-L30) NOT NUMERIC
              MOVE 0 TO L30-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-L30)
              TO L30-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-L30) NOT NUMERIC
              MOVE 0 TO L30-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-L30)
              TO L30-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-L30) NOT NUMERIC
              MOVE 0 TO L30-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-L30)
              TO L30-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-L30)
              TO L30-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-L30)
              TO L30-DS-STATO-ELAB
           IF (SF)-DT-DECOR-POLI-LQ-NULL(IX-TAB-L30) = HIGH-VALUES
              MOVE (SF)-DT-DECOR-POLI-LQ-NULL(IX-TAB-L30)
              TO L30-DT-DECOR-POLI-LQ-NULL
           ELSE
             IF (SF)-DT-DECOR-POLI-LQ(IX-TAB-L30) = ZERO
                MOVE HIGH-VALUES
                TO L30-DT-DECOR-POLI-LQ-NULL
             ELSE
              MOVE (SF)-DT-DECOR-POLI-LQ(IX-TAB-L30)
              TO L30-DT-DECOR-POLI-LQ
             END-IF
           END-IF.
       VAL-DCLGEN-L30-EX.
           EXIT.

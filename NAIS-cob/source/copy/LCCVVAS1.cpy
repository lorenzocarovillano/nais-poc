      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA VAL_AST
      *   ALIAS VAS
      *   ULTIMO AGG. 09 LUG 2010
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-VAL-AST PIC S9(9)     COMP-3.
             07 (SF)-ID-TRCH-DI-GAR PIC S9(9)     COMP-3.
             07 (SF)-ID-TRCH-DI-GAR-NULL REDEFINES
                (SF)-ID-TRCH-DI-GAR   PIC X(5).
             07 (SF)-ID-MOVI-FINRIO PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-FND PIC X(20).
             07 (SF)-COD-FND-NULL REDEFINES
                (SF)-COD-FND   PIC X(20).
             07 (SF)-NUM-QUO PIC S9(7)V9(5) COMP-3.
             07 (SF)-NUM-QUO-NULL REDEFINES
                (SF)-NUM-QUO   PIC X(7).
             07 (SF)-VAL-QUO PIC S9(7)V9(5) COMP-3.
             07 (SF)-VAL-QUO-NULL REDEFINES
                (SF)-VAL-QUO   PIC X(7).
             07 (SF)-DT-VALZZ   PIC S9(8) COMP-3.
             07 (SF)-DT-VALZZ-NULL REDEFINES
                (SF)-DT-VALZZ   PIC X(5).
             07 (SF)-TP-VAL-AST PIC X(2).
             07 (SF)-ID-RICH-INVST-FND PIC S9(9)     COMP-3.
             07 (SF)-ID-RICH-INVST-FND-NULL REDEFINES
                (SF)-ID-RICH-INVST-FND   PIC X(5).
             07 (SF)-ID-RICH-DIS-FND PIC S9(9)     COMP-3.
             07 (SF)-ID-RICH-DIS-FND-NULL REDEFINES
                (SF)-ID-RICH-DIS-FND   PIC X(5).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-PRE-MOVTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-MOVTO-NULL REDEFINES
                (SF)-PRE-MOVTO   PIC X(8).
             07 (SF)-IMP-MOVTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-MOVTO-NULL REDEFINES
                (SF)-IMP-MOVTO   PIC X(8).
             07 (SF)-PC-INV-DIS PIC S9(5)V9(9) COMP-3.
             07 (SF)-PC-INV-DIS-NULL REDEFINES
                (SF)-PC-INV-DIS   PIC X(8).
             07 (SF)-NUM-QUO-LORDE PIC S9(7)V9(5) COMP-3.
             07 (SF)-NUM-QUO-LORDE-NULL REDEFINES
                (SF)-NUM-QUO-LORDE   PIC X(7).
             07 (SF)-DT-VALZZ-CALC   PIC S9(8) COMP-3.
             07 (SF)-DT-VALZZ-CALC-NULL REDEFINES
                (SF)-DT-VALZZ-CALC   PIC X(5).
             07 (SF)-MINUS-VALENZA PIC S9(12)V9(3) COMP-3.
             07 (SF)-MINUS-VALENZA-NULL REDEFINES
                (SF)-MINUS-VALENZA   PIC X(8).
             07 (SF)-PLUS-VALENZA PIC S9(12)V9(3) COMP-3.
             07 (SF)-PLUS-VALENZA-NULL REDEFINES
                (SF)-PLUS-VALENZA   PIC X(8).


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVP843
      *   ULTIMO AGG. 26 SET 2014
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-P84.
           MOVE P84-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-P84)
           MOVE P84-ID-POLI
             TO (SF)-ID-POLI(IX-TAB-P84)
           MOVE P84-DT-LIQ-CED
             TO (SF)-DT-LIQ-CED(IX-TAB-P84)
           MOVE P84-COD-TARI
             TO (SF)-COD-TARI(IX-TAB-P84)
           IF P84-IMP-LRD-CEDOLE-LIQ-NULL = HIGH-VALUES
              MOVE P84-IMP-LRD-CEDOLE-LIQ-NULL
                TO (SF)-IMP-LRD-CEDOLE-LIQ-NULL(IX-TAB-P84)
           ELSE
              MOVE P84-IMP-LRD-CEDOLE-LIQ
                TO (SF)-IMP-LRD-CEDOLE-LIQ(IX-TAB-P84)
           END-IF
           MOVE P84-IB-POLI
             TO (SF)-IB-POLI(IX-TAB-P84)
           MOVE P84-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-P84)
           MOVE P84-DS-VER
             TO (SF)-DS-VER(IX-TAB-P84)
           MOVE P84-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ(IX-TAB-P84)
           MOVE P84-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-P84)
           MOVE P84-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-P84).
       VALORIZZA-OUTPUT-P84-EX.
           EXIT.

       01 LDBI0731.
         05 LDBI0731-ID-POLI                 PIC S9(9)     COMP-3.
         05 LDBI0731-ID-ADES                 PIC S9(9)     COMP-3.
         05 LDBI0731-ID-TRCH                 PIC S9(9)     COMP-3.
         05 LDBI0731-TP-CALL                 PIC X(02).
         05 LDBI0731-TP-STAT-BUS             PIC X(02).
         05 LDBI0731-TRASFORMATA             PIC X(01).

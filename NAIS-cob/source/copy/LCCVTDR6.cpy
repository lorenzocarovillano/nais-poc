      *----------------------------------------------------------------*
      *    COPY      ..... LCCVTDR6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO TITOLO DI RATA
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-TIT-RAT.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE TIT-RAT

      *--> NOME TABELLA FISICA DB
           MOVE 'TIT-RAT'                  TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF  NOT WTDR-ST-INV(IX-TAB-TDR)
           AND NOT WTDR-ST-CON(IX-TAB-TDR)
           AND WTDR-ELE-TDR-MAX  NOT  = 0

      *--->   Impostare ID Tabella TITOLO DI RATA
              MOVE WMOV-ID-PTF     TO TDR-ID-MOVI-CRZ

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WTDR-ST-ADD(IX-TAB-TDR)

                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA
                            TO WTDR-ID-PTF(IX-TAB-TDR)
                               TDR-ID-TIT-RAT

      *-->                Estrazione Oggetto
                          PERFORM PREPARA-AREA-LCCS0234-TDR
                             THRU PREPARA-AREA-LCCS0234-TDR-EX

                          PERFORM CALL-LCCS0234
                             THRU CALL-LCCS0234-EX
                          IF IDSV0001-ESITO-OK
                             MOVE S234-ID-OGG-PTF-EOC     TO TDR-ID-OGG
                          END-IF
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WTDR-ST-DEL(IX-TAB-TDR)

                       MOVE WTDR-ID-PTF(IX-TAB-TDR)   TO TDR-ID-TIT-RAT
                       MOVE WTDR-ID-OGG(IX-TAB-TDR)   TO TDR-ID-OGG

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->        MODIFICA
                  WHEN WTDR-ST-MOD(IX-TAB-TDR)
                       MOVE WTDR-ID-PTF(IX-TAB-TDR)   TO TDR-ID-TIT-RAT
                       MOVE WTDR-ID-OGG(IX-TAB-TDR)   TO TDR-ID-OGG

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN TITOLO DI RATA
                 PERFORM VAL-DCLGEN-TDR
                    THRU VAL-DCLGEN-TDR-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-TDR
                    THRU VALORIZZA-AREA-DSH-TDR-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-TIT-RAT-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-TDR.

      *--> DCLGEN TABELLA
           MOVE TIT-RAT                 TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT   TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-TDR-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    PREPARA AREA PER CALL LCCS0234
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234-TDR.

           MOVE WTDR-ID-OGG(IX-TAB-TDR)    TO S234-ID-OGG-EOC.
           MOVE WTDR-TP-OGG(IX-TAB-TDR)    TO S234-TIPO-OGG-EOC.

       PREPARA-AREA-LCCS0234-TDR-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVTDR5.

       01 IEAV9904.
          10 IEAV9904-LIMITE-MAX               PIC S9(4) VALUE 500.
          10 IEAV9904-MAX-ELE-ERRORI           PIC S9(4) COMP.
          10 IEAV9904-TAB-ERRORI-FRONT-END.
              15 IEAV9904-ELE-ERRORI   OCCURS 500.
                 20 IEAV9904-DESC-ERRORE       PIC X(200).
                 20 IEAV9904-COD-ERRORE        PIC 9(006).
                 20 IEAV9904-LIV-GRAVITA-BE    PIC 9(001).
                 20 IEAV9904-TIPO-TRATT-FE     PIC X(001).

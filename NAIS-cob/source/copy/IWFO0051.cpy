       03 IWFO0051-ZONA-DATI.
          10 IWFO0051-CAMPO-OUTPUT-DEFI          PIC S9(13)V9(05).

          10 IWFO0051-AREA-ERRORI.
             15 IWFO0051-ESITO                       PIC X(002).
                 88 IWFO0051-ESITO-OK                VALUE 'OK'.
                 88 IWFO0051-ESITO-KO                VALUE 'KO'.
             15 IWFO0051-LOG-ERRORE.
                 20 IWFO0051-COD-SERVIZIO-BE         PIC X(008).
                 20 IWFO0051-LABEL-ERR               PIC X(030).
                 20 IWFO0051-DESC-ERRORE-ESTESA      PIC X(200).
                 20 IWFO0051-OPER-TABELLA            PIC X(002).
                 20 IWFO0051-NOME-TABELLA            PIC X(018).
                 20 IWFO0051-STATUS-TABELLA          PIC X(010).
                 20 IWFO0051-KEY-TABELLA             PIC X(020).

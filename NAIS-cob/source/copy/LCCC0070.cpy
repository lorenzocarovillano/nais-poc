      *----------------------------------------------------------------*
      *   PROCESSO DI VENDITA - PORTAFOGLIO VITA
      *   AREA INPUT/OUTPUT
      *   SERVIZIO CALCOLO IB OGGETTO ADE/GAR/TGA
      *----------------------------------------------------------------*
           05 LCCC0070-DATI-INPUT.
              10 LCCC0070-FUNZIONE              PIC X(02).
                 88 LCCC0070-CALC-IB-ADE        VALUE 'AD'.
                 88 LCCC0070-CALC-IB-GAR        VALUE 'GA'.
                 88 LCCC0070-CALC-IB-TGA        VALUE 'TG'.
              10 LCCC0070-ID-OGGETTO            PIC S9(09) COMP-3.
              10 LCCC0070-TP-OGGETTO            PIC X(02).
           05 LCCC0070-DATI-OUTPUT.
              10 LCCC0070-IB-OGGETTO             PIC X(40).


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVE125
      *   ULTIMO AGG. 14 MAR 2012
      *------------------------------------------------------------

       VAL-DCLGEN-E12.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-E12) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-E12)
              TO E12-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-E12)
              TO E12-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-E12) NOT NUMERIC
              MOVE 0 TO E12-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-E12)
              TO E12-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-E12) NOT NUMERIC
              MOVE 0 TO E12-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-E12)
              TO E12-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-E12)
              TO E12-COD-COMP-ANIA
           IF (SF)-DT-EMIS-NULL(IX-TAB-E12) = HIGH-VALUES
              MOVE (SF)-DT-EMIS-NULL(IX-TAB-E12)
              TO E12-DT-EMIS-NULL
           ELSE
             IF (SF)-DT-EMIS(IX-TAB-E12) = ZERO
                MOVE HIGH-VALUES
                TO E12-DT-EMIS-NULL
             ELSE
              MOVE (SF)-DT-EMIS(IX-TAB-E12)
              TO E12-DT-EMIS
             END-IF
           END-IF
           IF (SF)-CUM-PRE-ATT-NULL(IX-TAB-E12) = HIGH-VALUES
              MOVE (SF)-CUM-PRE-ATT-NULL(IX-TAB-E12)
              TO E12-CUM-PRE-ATT-NULL
           ELSE
              MOVE (SF)-CUM-PRE-ATT(IX-TAB-E12)
              TO E12-CUM-PRE-ATT
           END-IF
           IF (SF)-ACCPRE-PAG-NULL(IX-TAB-E12) = HIGH-VALUES
              MOVE (SF)-ACCPRE-PAG-NULL(IX-TAB-E12)
              TO E12-ACCPRE-PAG-NULL
           ELSE
              MOVE (SF)-ACCPRE-PAG(IX-TAB-E12)
              TO E12-ACCPRE-PAG
           END-IF
           IF (SF)-CUM-PRSTZ-NULL(IX-TAB-E12) = HIGH-VALUES
              MOVE (SF)-CUM-PRSTZ-NULL(IX-TAB-E12)
              TO E12-CUM-PRSTZ-NULL
           ELSE
              MOVE (SF)-CUM-PRSTZ(IX-TAB-E12)
              TO E12-CUM-PRSTZ
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-E12) NOT NUMERIC
              MOVE 0 TO E12-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-E12)
              TO E12-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-E12)
              TO E12-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-E12) NOT NUMERIC
              MOVE 0 TO E12-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-E12)
              TO E12-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-E12) NOT NUMERIC
              MOVE 0 TO E12-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-E12)
              TO E12-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-E12) NOT NUMERIC
              MOVE 0 TO E12-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-E12)
              TO E12-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-E12)
              TO E12-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-E12)
              TO E12-DS-STATO-ELAB
           MOVE (SF)-TP-TRCH(IX-TAB-E12)
              TO E12-TP-TRCH
           IF (SF)-CAUS-SCON-NULL(IX-TAB-E12) = HIGH-VALUES
              MOVE (SF)-CAUS-SCON-NULL(IX-TAB-E12)
              TO E12-CAUS-SCON-NULL
           ELSE
              MOVE (SF)-CAUS-SCON(IX-TAB-E12)
              TO E12-CAUS-SCON
           END-IF.
       VAL-DCLGEN-E12-EX.
           EXIT.

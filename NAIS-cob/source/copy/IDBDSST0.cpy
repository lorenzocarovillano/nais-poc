           EXEC SQL DECLARE SINONIMO_STR TABLE
           (
             COD_COMPAGNIA_ANIA  DECIMAL(5, 0) NOT NULL,
             COD_STR_DATO        CHAR(30) NOT NULL,
             FLAG_MOD_ESECUTIVA  CHAR(1) NOT NULL,
             OPERAZIONE          CHAR(15) NOT NULL,
             COD_SIN_STR_DATO    CHAR(30) NOT NULL
          ) END-EXEC.

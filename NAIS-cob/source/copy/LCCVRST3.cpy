
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVRST3
      *   ULTIMO AGG. 09 LUG 2010
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-RST.
           MOVE RST-ID-RIS-DI-TRCH
             TO (SF)-ID-PTF(IX-TAB-RST)
           MOVE RST-ID-RIS-DI-TRCH
             TO (SF)-ID-RIS-DI-TRCH(IX-TAB-RST)
           MOVE RST-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-RST)
           IF RST-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE RST-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-RST)
           END-IF
           MOVE RST-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-RST)
           MOVE RST-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-RST)
           MOVE RST-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-RST)
           IF RST-TP-CALC-RIS-NULL = HIGH-VALUES
              MOVE RST-TP-CALC-RIS-NULL
                TO (SF)-TP-CALC-RIS-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-TP-CALC-RIS
                TO (SF)-TP-CALC-RIS(IX-TAB-RST)
           END-IF
           IF RST-ULT-RM-NULL = HIGH-VALUES
              MOVE RST-ULT-RM-NULL
                TO (SF)-ULT-RM-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-ULT-RM
                TO (SF)-ULT-RM(IX-TAB-RST)
           END-IF
           IF RST-DT-CALC-NULL = HIGH-VALUES
              MOVE RST-DT-CALC-NULL
                TO (SF)-DT-CALC-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-DT-CALC
                TO (SF)-DT-CALC(IX-TAB-RST)
           END-IF
           IF RST-DT-ELAB-NULL = HIGH-VALUES
              MOVE RST-DT-ELAB-NULL
                TO (SF)-DT-ELAB-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-DT-ELAB
                TO (SF)-DT-ELAB(IX-TAB-RST)
           END-IF
           IF RST-RIS-BILA-NULL = HIGH-VALUES
              MOVE RST-RIS-BILA-NULL
                TO (SF)-RIS-BILA-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-BILA
                TO (SF)-RIS-BILA(IX-TAB-RST)
           END-IF
           IF RST-RIS-MAT-NULL = HIGH-VALUES
              MOVE RST-RIS-MAT-NULL
                TO (SF)-RIS-MAT-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-MAT
                TO (SF)-RIS-MAT(IX-TAB-RST)
           END-IF
           IF RST-INCR-X-RIVAL-NULL = HIGH-VALUES
              MOVE RST-INCR-X-RIVAL-NULL
                TO (SF)-INCR-X-RIVAL-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-INCR-X-RIVAL
                TO (SF)-INCR-X-RIVAL(IX-TAB-RST)
           END-IF
           IF RST-RPTO-PRE-NULL = HIGH-VALUES
              MOVE RST-RPTO-PRE-NULL
                TO (SF)-RPTO-PRE-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RPTO-PRE
                TO (SF)-RPTO-PRE(IX-TAB-RST)
           END-IF
           IF RST-FRAZ-PRE-PP-NULL = HIGH-VALUES
              MOVE RST-FRAZ-PRE-PP-NULL
                TO (SF)-FRAZ-PRE-PP-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-FRAZ-PRE-PP
                TO (SF)-FRAZ-PRE-PP(IX-TAB-RST)
           END-IF
           IF RST-RIS-TOT-NULL = HIGH-VALUES
              MOVE RST-RIS-TOT-NULL
                TO (SF)-RIS-TOT-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-TOT
                TO (SF)-RIS-TOT(IX-TAB-RST)
           END-IF
           IF RST-RIS-SPE-NULL = HIGH-VALUES
              MOVE RST-RIS-SPE-NULL
                TO (SF)-RIS-SPE-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-SPE
                TO (SF)-RIS-SPE(IX-TAB-RST)
           END-IF
           IF RST-RIS-ABB-NULL = HIGH-VALUES
              MOVE RST-RIS-ABB-NULL
                TO (SF)-RIS-ABB-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-ABB
                TO (SF)-RIS-ABB(IX-TAB-RST)
           END-IF
           IF RST-RIS-BNSFDT-NULL = HIGH-VALUES
              MOVE RST-RIS-BNSFDT-NULL
                TO (SF)-RIS-BNSFDT-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-BNSFDT
                TO (SF)-RIS-BNSFDT(IX-TAB-RST)
           END-IF
           IF RST-RIS-SOPR-NULL = HIGH-VALUES
              MOVE RST-RIS-SOPR-NULL
                TO (SF)-RIS-SOPR-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-SOPR
                TO (SF)-RIS-SOPR(IX-TAB-RST)
           END-IF
           IF RST-RIS-INTEG-BAS-TEC-NULL = HIGH-VALUES
              MOVE RST-RIS-INTEG-BAS-TEC-NULL
                TO (SF)-RIS-INTEG-BAS-TEC-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-INTEG-BAS-TEC
                TO (SF)-RIS-INTEG-BAS-TEC(IX-TAB-RST)
           END-IF
           IF RST-RIS-INTEG-DECR-TS-NULL = HIGH-VALUES
              MOVE RST-RIS-INTEG-DECR-TS-NULL
                TO (SF)-RIS-INTEG-DECR-TS-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-INTEG-DECR-TS
                TO (SF)-RIS-INTEG-DECR-TS(IX-TAB-RST)
           END-IF
           IF RST-RIS-GAR-CASO-MOR-NULL = HIGH-VALUES
              MOVE RST-RIS-GAR-CASO-MOR-NULL
                TO (SF)-RIS-GAR-CASO-MOR-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-GAR-CASO-MOR
                TO (SF)-RIS-GAR-CASO-MOR(IX-TAB-RST)
           END-IF
           IF RST-RIS-ZIL-NULL = HIGH-VALUES
              MOVE RST-RIS-ZIL-NULL
                TO (SF)-RIS-ZIL-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-ZIL
                TO (SF)-RIS-ZIL(IX-TAB-RST)
           END-IF
           IF RST-RIS-FAIVL-NULL = HIGH-VALUES
              MOVE RST-RIS-FAIVL-NULL
                TO (SF)-RIS-FAIVL-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-FAIVL
                TO (SF)-RIS-FAIVL(IX-TAB-RST)
           END-IF
           IF RST-RIS-COS-AMMTZ-NULL = HIGH-VALUES
              MOVE RST-RIS-COS-AMMTZ-NULL
                TO (SF)-RIS-COS-AMMTZ-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-COS-AMMTZ
                TO (SF)-RIS-COS-AMMTZ(IX-TAB-RST)
           END-IF
           IF RST-RIS-SPE-FAIVL-NULL = HIGH-VALUES
              MOVE RST-RIS-SPE-FAIVL-NULL
                TO (SF)-RIS-SPE-FAIVL-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-SPE-FAIVL
                TO (SF)-RIS-SPE-FAIVL(IX-TAB-RST)
           END-IF
           IF RST-RIS-PREST-FAIVL-NULL = HIGH-VALUES
              MOVE RST-RIS-PREST-FAIVL-NULL
                TO (SF)-RIS-PREST-FAIVL-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-PREST-FAIVL
                TO (SF)-RIS-PREST-FAIVL(IX-TAB-RST)
           END-IF
           IF RST-RIS-COMPON-ASSVA-NULL = HIGH-VALUES
              MOVE RST-RIS-COMPON-ASSVA-NULL
                TO (SF)-RIS-COMPON-ASSVA-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-COMPON-ASSVA
                TO (SF)-RIS-COMPON-ASSVA(IX-TAB-RST)
           END-IF
           IF RST-ULT-COEFF-RIS-NULL = HIGH-VALUES
              MOVE RST-ULT-COEFF-RIS-NULL
                TO (SF)-ULT-COEFF-RIS-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-ULT-COEFF-RIS
                TO (SF)-ULT-COEFF-RIS(IX-TAB-RST)
           END-IF
           IF RST-ULT-COEFF-AGG-RIS-NULL = HIGH-VALUES
              MOVE RST-ULT-COEFF-AGG-RIS-NULL
                TO (SF)-ULT-COEFF-AGG-RIS-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-ULT-COEFF-AGG-RIS
                TO (SF)-ULT-COEFF-AGG-RIS(IX-TAB-RST)
           END-IF
           IF RST-RIS-ACQ-NULL = HIGH-VALUES
              MOVE RST-RIS-ACQ-NULL
                TO (SF)-RIS-ACQ-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-ACQ
                TO (SF)-RIS-ACQ(IX-TAB-RST)
           END-IF
           IF RST-RIS-UTI-NULL = HIGH-VALUES
              MOVE RST-RIS-UTI-NULL
                TO (SF)-RIS-UTI-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-UTI
                TO (SF)-RIS-UTI(IX-TAB-RST)
           END-IF
           IF RST-RIS-MAT-EFF-NULL = HIGH-VALUES
              MOVE RST-RIS-MAT-EFF-NULL
                TO (SF)-RIS-MAT-EFF-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-MAT-EFF
                TO (SF)-RIS-MAT-EFF(IX-TAB-RST)
           END-IF
           IF RST-RIS-RISTORNI-CAP-NULL = HIGH-VALUES
              MOVE RST-RIS-RISTORNI-CAP-NULL
                TO (SF)-RIS-RISTORNI-CAP-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-RISTORNI-CAP
                TO (SF)-RIS-RISTORNI-CAP(IX-TAB-RST)
           END-IF
           IF RST-RIS-TRM-BNS-NULL = HIGH-VALUES
              MOVE RST-RIS-TRM-BNS-NULL
                TO (SF)-RIS-TRM-BNS-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-TRM-BNS
                TO (SF)-RIS-TRM-BNS(IX-TAB-RST)
           END-IF
           IF RST-RIS-BNSRIC-NULL = HIGH-VALUES
              MOVE RST-RIS-BNSRIC-NULL
                TO (SF)-RIS-BNSRIC-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-BNSRIC
                TO (SF)-RIS-BNSRIC(IX-TAB-RST)
           END-IF
           MOVE RST-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-RST)
           MOVE RST-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-RST)
           MOVE RST-DS-VER
             TO (SF)-DS-VER(IX-TAB-RST)
           MOVE RST-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-RST)
           MOVE RST-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-RST)
           MOVE RST-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-RST)
           MOVE RST-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-RST)
           MOVE RST-ID-TRCH-DI-GAR
             TO (SF)-ID-TRCH-DI-GAR(IX-TAB-RST)
           IF RST-COD-FND-NULL = HIGH-VALUES
              MOVE RST-COD-FND-NULL
                TO (SF)-COD-FND-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-COD-FND
                TO (SF)-COD-FND(IX-TAB-RST)
           END-IF
           MOVE RST-ID-POLI
             TO (SF)-ID-POLI(IX-TAB-RST)
           MOVE RST-ID-ADES
             TO (SF)-ID-ADES(IX-TAB-RST)
           MOVE RST-ID-GAR
             TO (SF)-ID-GAR(IX-TAB-RST)
           IF RST-RIS-MIN-GARTO-NULL = HIGH-VALUES
              MOVE RST-RIS-MIN-GARTO-NULL
                TO (SF)-RIS-MIN-GARTO-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-MIN-GARTO
                TO (SF)-RIS-MIN-GARTO(IX-TAB-RST)
           END-IF
           IF RST-RIS-RSH-DFLT-NULL = HIGH-VALUES
              MOVE RST-RIS-RSH-DFLT-NULL
                TO (SF)-RIS-RSH-DFLT-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-RSH-DFLT
                TO (SF)-RIS-RSH-DFLT(IX-TAB-RST)
           END-IF
           IF RST-RIS-MOVI-NON-INVES-NULL = HIGH-VALUES
              MOVE RST-RIS-MOVI-NON-INVES-NULL
                TO (SF)-RIS-MOVI-NON-INVES-NULL(IX-TAB-RST)
           ELSE
              MOVE RST-RIS-MOVI-NON-INVES
                TO (SF)-RIS-MOVI-NON-INVES(IX-TAB-RST)
           END-IF.
       VALORIZZA-OUTPUT-RST-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY      ..... LCCVE156
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO EST. RAPPORTO ANAGRAFICA
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-EST-RAPP-ANA.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE EST-RAPP-ANA
           MOVE 'EST-RAPP-ANA'                TO WK-TABELLA

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WE15-ST-INV(IX-TAB-E15)
              AND NOT WE15-ST-CON(IX-TAB-E15)
              AND WE15-EST-RAPP-ANA-MAX NOT = 0

      *--->   Impostare ID Tabella RAPPORTO ANAGRAFICO
              MOVE WMOV-ID-PTF
                TO E15-ID-MOVI-CRZ

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WE15-ST-ADD(IX-TAB-E15)
      *-->
                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA
                            TO WE15-ID-PTF(IX-TAB-E15)
                               E15-ID-EST-RAPP-ANA
      *-->        Estrazione Oggetto
                          IF WE15-TP-OGG(IX-TAB-E15) = 'LI'
                             MOVE WE15-ID-OGG(IX-TAB-E15) TO E15-ID-OGG
                          ELSE
                             PERFORM PREPARA-AREA-LCCS0234-E15
                                THRU PREPARA-AREA-LCCS0234-E15-EX

                             PERFORM CALL-LCCS0234
                                THRU CALL-LCCS0234-EX

                             IF IDSV0001-ESITO-OK
                                MOVE S234-ID-OGG-PTF-EOC
                                  TO WE15-ID-OGG(IX-TAB-E15)
                             END-IF
                          END-IF

                          PERFORM VALORIZZA-ID-RAPP-ANA
                             THRU VALORIZZA-ID-RAPP-ANA-EX

                          SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

                       END-IF

      *-->        CANCELLAZIONE
                  WHEN WE15-ST-DEL(IX-TAB-E15)

                       MOVE WE15-ID-PTF(IX-TAB-E15)
                         TO WE15-ID-EST-RAPP-ANA(IX-TAB-E15)
                            E15-ID-EST-RAPP-ANA

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->        MODIFICA
                  WHEN WE15-ST-MOD(IX-TAB-E15)
                       MOVE WE15-ID-PTF(IX-TAB-E15)
                         TO WE15-ID-EST-RAPP-ANA(IX-TAB-E15)
                            E15-ID-EST-RAPP-ANA

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN RAPPORTO ANAGRAFICO
                 PERFORM VAL-DCLGEN-E15
                    THRU VAL-DCLGEN-E15-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-E15
                    THRU VALORIZZA-AREA-DSH-E15-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO

                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-EST-RAPP-ANA-EX.
           EXIT.
      *
      *
      *----------------------------------------------------------------*
       VALORIZZA-ID-RAPP-ANA.
      *
           PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
             UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX

             IF WE15-ID-RAPP-ANA(IX-TAB-E15) GREATER ZERO
                IF WRAN-ID-RAPP-ANA(IX-TAB-RAN)
                EQUAL WE15-ID-RAPP-ANA(IX-TAB-E15)
                   MOVE WRAN-ID-PTF(IX-TAB-RAN)
                     TO WE15-ID-RAPP-ANA(IX-TAB-E15)
                END-IF
             END-IF

           END-PERFORM.

           IF WE15-ID-RAPP-ANA-COLLG-NULL(IX-TAB-E15) NOT =
              HIGH-VALUE AND LOW-VALUE AND SPACES

              PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
                UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX

                IF WRAN-ID-RAPP-ANA(IX-TAB-RAN)
                EQUAL WE15-ID-RAPP-ANA-COLLG(IX-TAB-E15)
                   MOVE WRAN-ID-PTF(IX-TAB-RAN)
                     TO WE15-ID-RAPP-ANA-COLLG(IX-TAB-E15)
                END-IF

              END-PERFORM

           END-IF.

      *
       VALORIZZA-ID-RAPP-ANA-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-E15.

      *--> DCLGEN TABELLA
           MOVE EST-RAPP-ANA            TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-E15-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    PREPARA AREA PER CALL LCCS0234
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234-E15.

           MOVE WE15-ID-OGG(IX-TAB-E15)    TO S234-ID-OGG-EOC.
           MOVE WE15-TP-OGG(IX-TAB-E15)    TO S234-TIPO-OGG-EOC.

       PREPARA-AREA-LCCS0234-E15-EX.
           EXIT.

           EXEC SQL DECLARE RIS_DI_TRCH TABLE
           (
             ID_RIS_DI_TRCH      DECIMAL(9, 0) NOT NULL,
             ID_MOVI_CRZ         DECIMAL(9, 0) NOT NULL,
             ID_MOVI_CHIU        DECIMAL(9, 0),
             DT_INI_EFF          DATE NOT NULL,
             DT_END_EFF          DATE NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             TP_CALC_RIS         CHAR(2),
             ULT_RM              DECIMAL(15, 3),
             DT_CALC             DATE,
             DT_ELAB             DATE,
             RIS_BILA            DECIMAL(15, 3),
             RIS_MAT             DECIMAL(15, 3),
             INCR_X_RIVAL        DECIMAL(15, 3),
             RPTO_PRE            DECIMAL(15, 3),
             FRAZ_PRE_PP         DECIMAL(15, 3),
             RIS_TOT             DECIMAL(15, 3),
             RIS_SPE             DECIMAL(15, 3),
             RIS_ABB             DECIMAL(15, 3),
             RIS_BNSFDT          DECIMAL(15, 3),
             RIS_SOPR            DECIMAL(15, 3),
             RIS_INTEG_BAS_TEC   DECIMAL(15, 3),
             RIS_INTEG_DECR_TS   DECIMAL(15, 3),
             RIS_GAR_CASO_MOR    DECIMAL(15, 3),
             RIS_ZIL             DECIMAL(15, 3),
             RIS_FAIVL           DECIMAL(15, 3),
             RIS_COS_AMMTZ       DECIMAL(15, 3),
             RIS_SPE_FAIVL       DECIMAL(15, 3),
             RIS_PREST_FAIVL     DECIMAL(15, 3),
             RIS_COMPON_ASSVA    DECIMAL(15, 3),
             ULT_COEFF_RIS       DECIMAL(14, 9),
             ULT_COEFF_AGG_RIS   DECIMAL(14, 9),
             RIS_ACQ             DECIMAL(15, 3),
             RIS_UTI             DECIMAL(15, 3),
             RIS_MAT_EFF         DECIMAL(15, 3),
             RIS_RISTORNI_CAP    DECIMAL(15, 3),
             RIS_TRM_BNS         DECIMAL(15, 3),
             RIS_BNSRIC          DECIMAL(15, 3),
             DS_RIGA             DECIMAL(10, 0) NOT NULL WITH DEFAULT,
             DS_OPER_SQL         CHAR(1) NOT NULL WITH DEFAULT,
             DS_VER              DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             DS_TS_INI_CPTZ      DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_TS_END_CPTZ      DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_UTENTE           CHAR(20) NOT NULL WITH DEFAULT,
             DS_STATO_ELAB       CHAR(1) NOT NULL WITH DEFAULT,
             ID_TRCH_DI_GAR      DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             COD_FND             CHAR(12),
             ID_POLI             DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             ID_ADES             DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             ID_GAR              DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             RIS_MIN_GARTO       DECIMAL(15, 3),
             RIS_RSH_DFLT        DECIMAL(15, 3),
             RIS_MOVI_NON_INVES  DECIMAL(15, 3)
          ) END-EXEC.

      *----------------------------------------------------------------*
      *    ESTRAZIONE SEQUENCE CLIENT bnl
      *----------------------------------------------------------------*
       S1101-ESTRAZIONE-SEQ-CLIENT.

           IF WCOM-NOME-TABELLA = 'EST-POLI'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_EST_POLI
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'EST-RAPP-ANA'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_EST_RAPP_ANA
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'EST-TRCH-DI-GAR'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_EST_TRCH_DI_GAR
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PDM-GAR-UL-ESTR'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PDM_GAR_UL_ESTR
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PDM-TRCH-ESTR'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PDM_TRCH_ESTR
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PDM-VAR-CALC-P'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PDM_VAR_CALC_P
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'PDM-VAR-CALC-T'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_PDM_VAR_CALC_T
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

           IF WCOM-NOME-TABELLA = 'SEGMENTAZ-CLI'
              EXEC SQL
                 VALUES NEXTVAL FOR SEQ_SEGMENTAZ_CLI
                   INTO :WS-ID-TABELLA
              END-EXEC
           END-IF.

       EX-S1101.
           EXIT.



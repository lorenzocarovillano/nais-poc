           EXEC SQL DECLARE ATT_SERV_VAL TABLE
           (
             ID_ATT_SERV_VAL     DECIMAL(9, 0) NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             ID_MOVI_CRZ         DECIMAL(9, 0) NOT NULL,
             ID_MOVI_CHIU        DECIMAL(9, 0),
             DT_INI_EFF          DATE NOT NULL,
             DT_END_EFF          DATE NOT NULL,
             TP_SERV_VAL         CHAR(2) NOT NULL,
             ID_OGG              DECIMAL(9, 0) NOT NULL,
             TP_OGG              CHAR(2) NOT NULL,
             DS_RIGA             DECIMAL(10, 0) NOT NULL,
             DS_OPER_SQL         CHAR(1) NOT NULL,
             DS_VER              DECIMAL(9, 0) NOT NULL,
             DS_TS_INI_CPTZ      DECIMAL(18, 0) NOT NULL,
             DS_TS_END_CPTZ      DECIMAL(18, 0) NOT NULL,
             DS_UTENTE           CHAR(20) NOT NULL,
             DS_STATO_ELAB       CHAR(1) NOT NULL,
             FL_ALL_FND          CHAR(1)
          ) END-EXEC.

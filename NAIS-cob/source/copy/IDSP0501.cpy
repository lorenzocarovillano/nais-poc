
      **************************************************************
       CALL-STRINGATURA.

           PERFORM INITIALIZE-CAMPI THRU INITIALIZE-CAMPI-EX

           PERFORM ELABORA-DATI     THRU ELABORA-DATI-EX.

       CALL-STRINGATURA-EX.
           EXIT.

      **************************************************************
       INITIALIZE-CAMPI.

           SET SENZA-SEGNO                 TO TRUE.
           SET DATO-INPUT-TROVATO-NO       TO TRUE.
           SET PRIMA-VOLTA-SI              TO TRUE.

           INITIALIZE                         IDSV0501-OUTPUT
                                              CAMPO-ALFA
                                              CAMPO-INTERI
                                              CAMPO-DECIMALI.

           MOVE 1                          TO IND-OUT
                                              POSIZIONE
                                              IDSV0501-MAX-TAB-STR.

           MOVE LIMITE-STRINGA             TO SPAZIO-RESTANTE

           MOVE 0                          TO IND-VAR
                                              IND-STRINGA
                                              IND-INT
                                              IND-DEC.

       INITIALIZE-CAMPI-EX.
           EXIT.

      **************************************************************
       ELABORA-DATI.

           PERFORM VARYING IND-VAR FROM 1 BY 1
                     UNTIL IND-VAR > LIMITE-ARRAY-INPUT        OR
                           IDSV0501-TP-DATO(IND-VAR) =
                           SPACES OR LOW-VALUES OR HIGH-VALUES OR
                           NOT IDSV0501-SUCCESSFUL-RC

              SET DATO-INPUT-TROVATO-SI      TO TRUE
              SET SENZA-SEGNO                TO TRUE

              IF PRIMA-VOLTA-SI
                 SET PRIMA-VOLTA-NO          TO TRUE
                 MOVE TP-DATO-PARALLELO(IND-VAR)
                                             TO IDSV0501-TP-STRINGA
                 MOVE TP-DATO-PARALLELO(IND-VAR)
                                             TO WK-TP-DATO-PARALLELO
              ELSE
                 IF TP-DATO-PARALLELO(IND-VAR) NOT =
                    WK-TP-DATO-PARALLELO
                    MOVE MISTA               TO IDSV0501-TP-STRINGA
                 ELSE
      * SIR 20310 GESTIONE LISTE OMOGENEE
                    PERFORM LISTA-OMOGENEA   THRU LISTA-OMOGENEA-EX
                 END-IF
              END-IF


              EVALUATE TP-DATO-PARALLELO(IND-VAR)

                 WHEN IMPORTO
                      PERFORM TRATTA-IMPORTO THRU TRATTA-IMPORTO-EX

                 WHEN NUMERICO
                      PERFORM TRATTA-NUMERICO
                                             THRU TRATTA-NUMERICO-EX

                 WHEN TASSO
                 WHEN PERCENTUALE
                 WHEN MILLESIMI
                      PERFORM TRATTA-PERCENTUALE
                                             THRU TRATTA-PERCENTUALE-EX

                 WHEN DT
                 WHEN STRINGA
                      PERFORM TRATTA-STRINGA
                                             THRU TRATTA-STRINGA-EX

                 WHEN OTHER
                      SET IDSV0501-GENERIC-ERROR  TO TRUE
                      STRING 'TIPO DATO LISTA NON VALIDO  : '''
                             IDSV0501-TP-DATO(IND-VAR)
                             ''' SU OCCORRENZA : '
                             IND-VAR
                             DELIMITED BY SIZE
                             INTO IDSV0501-DESCRIZ-ERR
                      END-STRING

              END-EVALUATE

              IF IDSV0501-SUCCESSFUL-RC
                 MOVE IDSV0501-SEPARATORE    TO IDSV0501-STRINGA-TOT
                                               (IND-OUT)(POSIZIONE:1)
                 ADD 1                       TO POSIZIONE
              END-IF

           END-PERFORM.


           IF IDSV0501-SUCCESSFUL-RC
              IF DATO-INPUT-TROVATO-NO
                 SET IDSV0501-GENERIC-ERROR  TO TRUE
                 STRING 'NESSUN DATO DI INPUT TROVATO'
                        DELIMITED BY SIZE
                        INTO IDSV0501-DESCRIZ-ERR
                 END-STRING
              END-IF
           END-IF.

       ELABORA-DATI-EX.
           EXIT.

      **************************************************************
       TRATTA-IMPORTO.

           MOVE LIM-INT-IMP                 TO LIMITE-INTERI.
           MOVE LIM-DEC-IMP                 TO LIMITE-DECIMALI.

           IF IDSV0501-VAL-IMP (IND-VAR) >= 0
              SET SEGNO-POSITIVO            TO TRUE
           END-IF.

           IF IDSV0501-VAL-IMP (IND-VAR) <  0
              SET SEGNO-NEGATIVO            TO TRUE
           END-IF.

           INITIALIZE                       STRUCT-IMP.
           MOVE IDSV0501-VAL-IMP (IND-VAR)  TO STRUCT-IMP.

           PERFORM ESTRAI-INTERO            THRU ESTRAI-INTERO-EX.

           PERFORM ESTRAI-DECIMALI          THRU ESTRAI-DECIMALI-EX.

           PERFORM STRINGA-CAMPO            THRU STRINGA-CAMPO-EX.

           IF IDSV0501-SEGNO-POSTERIORE
              PERFORM TRATTA-SEGNO          THRU TRATTA-SEGNO-EX
           END-IF.

       TRATTA-IMPORTO-EX.
           EXIT.

      **************************************************************
       TRATTA-NUMERICO.

           MOVE LIMITE-INT-DEC              TO LIMITE-INTERI.

           IF IDSV0501-VAL-NUM (IND-VAR) >= ZEROES
              SET SEGNO-POSITIVO            TO TRUE
           END-IF.

           IF IDSV0501-VAL-NUM (IND-VAR) < ZEROES
              SET SEGNO-NEGATIVO            TO TRUE
           END-IF.

           INITIALIZE                          STRUCT-NUM.
           MOVE IDSV0501-VAL-NUM (IND-VAR)  TO STRUCT-NUM.

           PERFORM ESTRAI-INTERO            THRU ESTRAI-INTERO-EX.

           PERFORM STRINGA-CAMPO            THRU STRINGA-CAMPO-EX.

           IF IDSV0501-SEGNO-POSTERIORE
              PERFORM TRATTA-SEGNO          THRU TRATTA-SEGNO-EX
           END-IF.

       TRATTA-NUMERICO-EX.
           EXIT.

      **************************************************************
       TRATTA-PERCENTUALE.

           MOVE LIM-INT-PERC                TO LIMITE-INTERI.
           MOVE LIM-DEC-PERC                TO LIMITE-DECIMALI.

           INITIALIZE                          STRUCT-IMP.
           MOVE IDSV0501-VAL-PERC (IND-VAR) TO STRUCT-PERC.

           PERFORM ESTRAI-INTERO            THRU ESTRAI-INTERO-EX.

           PERFORM ESTRAI-DECIMALI          THRU ESTRAI-DECIMALI-EX.

           PERFORM STRINGA-CAMPO            THRU STRINGA-CAMPO-EX.

       TRATTA-PERCENTUALE-EX.
           EXIT.

      **************************************************************
       TRATTA-STRINGA.

           INITIALIZE                          STRUCT-ALFA.
           MOVE IDSV0501-VAL-STR (IND-VAR)  TO STRUCT-ALFA.

           PERFORM ESTRAI-ALFANUMERICO      THRU ESTRAI-ALFANUMERICO-EX.

           IF IND-ALFA >= LIMITE-STRINGA
              SET IDSV0501-GENERIC-ERROR    TO TRUE
              STRING 'SUPERATO IL LIMITE DI VALORE PER STRINGA'
                     ' SU OCCORRENZA : '
                     IND-VAR
                     DELIMITED BY SIZE
                     INTO IDSV0501-DESCRIZ-ERR
              END-STRING
           ELSE
              PERFORM STRINGA-CAMPO         THRU STRINGA-CAMPO-EX
           END-IF.

       TRATTA-STRINGA-EX.
           EXIT.

      **************************************************************
       ESTRAI-ALFANUMERICO.

           SET ALFANUMERICO-TROVATO-NO TO TRUE.
           SET FINE-STRINGA-NO         TO TRUE

           INITIALIZE                  CAMPO-ALFA
                                       IND-ALFA.

           PERFORM VARYING IND-STRINGA FROM 1 BY 1
                   UNTIL   IND-STRINGA > LIMITE-STRINGA OR
                           FINE-STRINGA-SI

                      IF ELE-ALFA(IND-STRINGA) =
                         SPACES OR LOW-VALUE OR HIGH-VALUE
                         SET FINE-STRINGA-SI         TO TRUE
                      ELSE
                         SET ALFANUMERICO-TROVATO-SI TO TRUE
                         ADD 1                       TO IND-ALFA
                         MOVE ELE-ALFA(IND-STRINGA)
                           TO ELE-STRINGA-ALFA(IND-ALFA)
                      END-IF

            END-PERFORM.

       ESTRAI-ALFANUMERICO-EX.
           EXIT.

      **************************************************************
       ESTRAI-INTERO.

           SET INT-TROVATO-NO          TO TRUE.

           INITIALIZE                      CAMPO-INTERI
                                           IND-INT.

           PERFORM VARYING IND-STRINGA FROM 1 BY 1
                   UNTIL   IND-STRINGA > LIMITE-INT-DEC    OR
                           IND-STRINGA > LIMITE-INTERI

                      IF ELE-NUM-IMP(IND-STRINGA)
                                                NOT = ZEROES
                         SET INT-TROVATO-SI     TO TRUE
                      END-IF

                      IF INT-TROVATO-SI
                         ADD 1                  TO IND-INT
                         MOVE ELE-NUM-IMP(IND-STRINGA)
                           TO ELE-STRINGA-INTERI(IND-INT)
                      END-IF

            END-PERFORM.

            IF INT-TROVATO-NO
               MOVE 1                 TO  IND-INT
            END-IF.

       ESTRAI-INTERO-EX.
           EXIT.

      **************************************************************
       ESTRAI-DECIMALI.

           INITIALIZE                      CAMPO-DECIMALI
                                           IND-DEC.

           PERFORM VARYING IND-STRINGA FROM LIMITE-DECIMALI BY 1
                   UNTIL   IND-STRINGA > LIMITE-INT-DEC

                         ADD 1                  TO IND-DEC
                         MOVE ELE-NUM-IMP(IND-STRINGA)
                           TO ELE-STRINGA-DECIMALI(IND-DEC)
           END-PERFORM.

       ESTRAI-DECIMALI-EX.
           EXIT.
      **************************************************************
       TRATTA-SEGNO.

           IF SEGNO-NEGATIVO
              MOVE '-'            TO IDSV0501-STRINGA-TOT
                                    (IND-OUT)(POSIZIONE:1)
              ADD 1               TO POSIZIONE
           ELSE
              IF IDSV0501-SEGNO-POSTIV-SI
                 MOVE '+'         TO IDSV0501-STRINGA-TOT
                                    (IND-OUT)(POSIZIONE:1)
                 ADD 1            TO POSIZIONE
              END-IF
           END-IF.

       TRATTA-SEGNO-EX.
           EXIT.

      **************************************************************
       STRINGA-CAMPO.

           PERFORM CALCOLA-SPAZIO       THRU CALCOLA-SPAZIO-EX.

           IF IDSV0501-SUCCESSFUL-RC
              EVALUATE TP-DATO-PARALLELO(IND-VAR)
                 WHEN STRINGA
                 WHEN DT
                     IF ALFANUMERICO-TROVATO-SI
                        MOVE CAMPO-ALFA(1:IND-ALFA)
                                        TO IDSV0501-STRINGA-TOT
                                           (IND-OUT)(POSIZIONE:IND-ALFA)
                        ADD IND-ALFA    TO POSIZIONE
                     ELSE
                        MOVE SPACES     TO IDSV0501-STRINGA-TOT
                                           (IND-OUT)(POSIZIONE:1)
                        ADD 1           TO POSIZIONE
                     END-IF

                 WHEN IMPORTO
                 WHEN PERCENTUALE
                 WHEN TASSO

                     IF IDSV0501-SEGNO-ANTERIORE
                        PERFORM TRATTA-SEGNO  THRU TRATTA-SEGNO-EX
                     END-IF

                     IF INT-TROVATO-SI
                        MOVE CAMPO-INTERI(1:IND-INT)
                                        TO IDSV0501-STRINGA-TOT
                                           (IND-OUT)(POSIZIONE:IND-INT)
                        ADD IND-INT     TO POSIZIONE
                     ELSE
                        MOVE 0          TO IDSV0501-STRINGA-TOT
                                           (IND-OUT)(POSIZIONE:1)
                        ADD 1           TO POSIZIONE
                     END-IF

                     MOVE IDSV0501-SIMBOLO-DECIMALE
                                        TO IDSV0501-STRINGA-TOT
                                          (IND-OUT)(POSIZIONE:1)
                     ADD 1              TO POSIZIONE

                     IF DEC-TROVATO-SI
                        MOVE CAMPO-DECIMALI(1:IND-DEC)
                                        TO IDSV0501-STRINGA-TOT
                                           (IND-OUT)(POSIZIONE:IND-DEC)
                        ADD IND-DEC     TO POSIZIONE
                     ELSE
                        MOVE 0          TO IDSV0501-STRINGA-TOT
                                           (IND-OUT)(POSIZIONE:1)
                        ADD 1           TO POSIZIONE
                     END-IF

                 WHEN NUMERICO

                     IF IDSV0501-SEGNO-ANTERIORE
                        PERFORM TRATTA-SEGNO THRU TRATTA-SEGNO-EX
                     END-IF

                     IF INT-TROVATO-SI
                        MOVE CAMPO-INTERI(1:IND-INT)
                                        TO IDSV0501-STRINGA-TOT
                                           (IND-OUT)(POSIZIONE:IND-INT)
                        ADD IND-INT     TO POSIZIONE
                     ELSE
                        MOVE 0          TO IDSV0501-STRINGA-TOT
                                           (IND-OUT)(POSIZIONE:1)
                        ADD 1           TO POSIZIONE
                     END-IF

              END-EVALUATE
           END-IF.

       STRINGA-CAMPO-EX.
           EXIT.

      **************************************************************
       CALCOLA-SPAZIO.

           IF TP-DATO-PARALLELO(IND-VAR) NOT = STRINGA
              PERFORM INDIVIDUA-IND-DEC    THRU INDIVIDUA-IND-DEC-EX
           END-IF.

           MOVE ZEROES                     TO SPAZIO-RICHIESTO.

           EVALUATE TP-DATO-PARALLELO(IND-VAR)
              WHEN STRINGA
              WHEN DT
                   COMPUTE SPAZIO-RICHIESTO = IND-ALFA + 1

              WHEN NUMERICO
                   COMPUTE SPAZIO-RICHIESTO = IND-INT + 1

              WHEN OTHER
                   COMPUTE SPAZIO-RICHIESTO = IND-INT + IND-DEC + 1 + 1

           END-EVALUATE.

           IF (IDSV0501-SEGNO-ANTERIORE OR
               IDSV0501-SEGNO-POSTERIORE)
               IF SEGNO-NEGATIVO
                  SUBTRACT  1    FROM  SPAZIO-RESTANTE
               ELSE
                  IF IDSV0501-SEGNO-POSTIV-SI
                     SUBTRACT  1    FROM  SPAZIO-RESTANTE
                  END-IF
               END-IF
           END-IF.

           IF SPAZIO-RICHIESTO >= SPAZIO-RESTANTE
              ADD 1                   TO IND-OUT
                                         IDSV0501-MAX-TAB-STR

              IF IND-OUT > LIMITE-ARRAY-OUTPUT
                 SET IDSV0501-GENERIC-ERROR  TO TRUE
                 STRING 'SUPERATO LIMITE STRUTTURE OUTPUT'
                        DELIMITED BY SIZE
                        INTO IDSV0501-DESCRIZ-ERR
                 END-STRING
              ELSE
                 MOVE LIMITE-STRINGA  TO SPAZIO-RESTANTE
                 MOVE 1               TO POSIZIONE
                 COMPUTE SPAZIO-RESTANTE = SPAZIO-RESTANTE -
                                           SPAZIO-RICHIESTO
              END-IF
           ELSE
              COMPUTE SPAZIO-RESTANTE = SPAZIO-RESTANTE -
                                        SPAZIO-RICHIESTO
           END-IF.

       CALCOLA-SPAZIO-EX.
           EXIT.

      **************************************************************
       INDIVIDUA-IND-DEC.

           SET DEC-TROVATO-NO                    TO TRUE.
           MOVE 1                                TO IND-DEC.

           EVALUATE TP-DATO-PARALLELO(IND-VAR)
               WHEN IMPORTO
                    PERFORM VARYING IND-RICERCA
                        FROM IDSV0501-DECIMALI-ESPOSTI-IMP
                        BY -1
                        UNTIL IND-RICERCA <= ZEROES OR
                              DEC-TROVATO-SI

                        IF  ELE-STRINGA-DECIMALI
                                         (IND-RICERCA) IS NUMERIC
                        AND ELE-STRINGA-DECIMALI
                                         (IND-RICERCA) NOT = ZEROES
                            SET DEC-TROVATO-SI TO TRUE
                            MOVE IND-RICERCA   TO IND-DEC
                        END-IF

                    END-PERFORM

               WHEN PERCENTUALE

                    PERFORM VARYING IND-RICERCA
                        FROM IDSV0501-DECIMALI-ESPOSTI-PERC
                        BY -1
                        UNTIL IND-RICERCA <= ZEROES OR
                              DEC-TROVATO-SI

                        IF  ELE-STRINGA-DECIMALI
                                         (IND-RICERCA) IS NUMERIC
                        AND ELE-STRINGA-DECIMALI
                                         (IND-RICERCA) NOT = ZEROES
                            SET DEC-TROVATO-SI TO TRUE
                            MOVE IND-RICERCA   TO IND-DEC
                        END-IF

                    END-PERFORM

               WHEN TASSO

                    PERFORM VARYING IND-RICERCA
                        FROM IDSV0501-DECIMALI-ESPOSTI-TASS
                        BY -1
                        UNTIL IND-RICERCA <= ZEROES OR
                              DEC-TROVATO-SI

                        IF  ELE-STRINGA-DECIMALI
                                         (IND-RICERCA) IS NUMERIC
                        AND ELE-STRINGA-DECIMALI
                                         (IND-RICERCA) NOT = ZEROES
                            SET DEC-TROVATO-SI TO TRUE
                            MOVE IND-RICERCA   TO IND-DEC
                        END-IF

                    END-PERFORM

           END-EVALUATE.

       INDIVIDUA-IND-DEC-EX.
           EXIT.
      **************************************************************
      * SIR 20310 GESTIONE LISTE OMOGENEE
      **************************************************************

       LISTA-OMOGENEA.

           EVALUATE TP-DATO-PARALLELO(IND-VAR)

               WHEN STRINGA
                    MOVE LISTA-STRINGA        TO IDSV0501-TP-STRINGA
               WHEN DT
                    MOVE LISTA-DT             TO IDSV0501-TP-STRINGA
               WHEN IMPORTO
                    MOVE LISTA-IMPORTO        TO IDSV0501-TP-STRINGA
               WHEN PERCENTUALE
                    MOVE LISTA-PERCENTUALE    TO IDSV0501-TP-STRINGA
               WHEN TASSO
                    MOVE LISTA-TASSO          TO IDSV0501-TP-STRINGA
               WHEN NUMERICO
                    MOVE LISTA-NUMERICO       TO IDSV0501-TP-STRINGA
               WHEN MILLESIMI
                    MOVE LISTA-MILLESIMI      TO IDSV0501-TP-STRINGA

           END-EVALUATE.

       LISTA-OMOGENEA-EX.
           EXIT.

      *----------------------------------------------------------------*
      * DIMENSIONAMENTO OCCURS PER LA TAB. LIQ                         *
      * COPY RIPORTANTE IL NUMERO DI OCCURS DA UTILIZZARE              *
      * PER INIZIALIZZA TOT DELLA TABELLA                              *
      *----------------------------------------------------------------*
       01 WK-LQU-MAX.
          03 WK-LQU-MAX-A                 PIC 9(04) VALUE  1.
          03 WK-LQU-MAX-B                 PIC 9(04) VALUE  30.

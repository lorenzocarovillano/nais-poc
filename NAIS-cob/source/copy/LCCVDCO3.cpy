
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVDCO3
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-DCO.
           MOVE DCO-ID-D-COLL
             TO (SF)-ID-PTF
           MOVE DCO-ID-D-COLL
             TO (SF)-ID-D-COLL
           MOVE DCO-ID-POLI
             TO (SF)-ID-POLI
           MOVE DCO-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ
           IF DCO-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE DCO-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL
           ELSE
              MOVE DCO-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU
           END-IF
           MOVE DCO-DT-INI-EFF
             TO (SF)-DT-INI-EFF
           MOVE DCO-DT-END-EFF
             TO (SF)-DT-END-EFF
           MOVE DCO-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           IF DCO-IMP-ARROT-PRE-NULL = HIGH-VALUES
              MOVE DCO-IMP-ARROT-PRE-NULL
                TO (SF)-IMP-ARROT-PRE-NULL
           ELSE
              MOVE DCO-IMP-ARROT-PRE
                TO (SF)-IMP-ARROT-PRE
           END-IF
           IF DCO-PC-SCON-NULL = HIGH-VALUES
              MOVE DCO-PC-SCON-NULL
                TO (SF)-PC-SCON-NULL
           ELSE
              MOVE DCO-PC-SCON
                TO (SF)-PC-SCON
           END-IF
           IF DCO-FL-ADES-SING-NULL = HIGH-VALUES
              MOVE DCO-FL-ADES-SING-NULL
                TO (SF)-FL-ADES-SING-NULL
           ELSE
              MOVE DCO-FL-ADES-SING
                TO (SF)-FL-ADES-SING
           END-IF
           IF DCO-TP-IMP-NULL = HIGH-VALUES
              MOVE DCO-TP-IMP-NULL
                TO (SF)-TP-IMP-NULL
           ELSE
              MOVE DCO-TP-IMP
                TO (SF)-TP-IMP
           END-IF
           IF DCO-FL-RICL-PRE-DA-CPT-NULL = HIGH-VALUES
              MOVE DCO-FL-RICL-PRE-DA-CPT-NULL
                TO (SF)-FL-RICL-PRE-DA-CPT-NULL
           ELSE
              MOVE DCO-FL-RICL-PRE-DA-CPT
                TO (SF)-FL-RICL-PRE-DA-CPT
           END-IF
           IF DCO-TP-ADES-NULL = HIGH-VALUES
              MOVE DCO-TP-ADES-NULL
                TO (SF)-TP-ADES-NULL
           ELSE
              MOVE DCO-TP-ADES
                TO (SF)-TP-ADES
           END-IF
           IF DCO-DT-ULT-RINN-TAC-NULL = HIGH-VALUES
              MOVE DCO-DT-ULT-RINN-TAC-NULL
                TO (SF)-DT-ULT-RINN-TAC-NULL
           ELSE
              MOVE DCO-DT-ULT-RINN-TAC
                TO (SF)-DT-ULT-RINN-TAC
           END-IF
           IF DCO-IMP-SCON-NULL = HIGH-VALUES
              MOVE DCO-IMP-SCON-NULL
                TO (SF)-IMP-SCON-NULL
           ELSE
              MOVE DCO-IMP-SCON
                TO (SF)-IMP-SCON
           END-IF
           IF DCO-FRAZ-DFLT-NULL = HIGH-VALUES
              MOVE DCO-FRAZ-DFLT-NULL
                TO (SF)-FRAZ-DFLT-NULL
           ELSE
              MOVE DCO-FRAZ-DFLT
                TO (SF)-FRAZ-DFLT
           END-IF
           IF DCO-ETA-SCAD-MASC-DFLT-NULL = HIGH-VALUES
              MOVE DCO-ETA-SCAD-MASC-DFLT-NULL
                TO (SF)-ETA-SCAD-MASC-DFLT-NULL
           ELSE
              MOVE DCO-ETA-SCAD-MASC-DFLT
                TO (SF)-ETA-SCAD-MASC-DFLT
           END-IF
           IF DCO-ETA-SCAD-FEMM-DFLT-NULL = HIGH-VALUES
              MOVE DCO-ETA-SCAD-FEMM-DFLT-NULL
                TO (SF)-ETA-SCAD-FEMM-DFLT-NULL
           ELSE
              MOVE DCO-ETA-SCAD-FEMM-DFLT
                TO (SF)-ETA-SCAD-FEMM-DFLT
           END-IF
           IF DCO-TP-DFLT-DUR-NULL = HIGH-VALUES
              MOVE DCO-TP-DFLT-DUR-NULL
                TO (SF)-TP-DFLT-DUR-NULL
           ELSE
              MOVE DCO-TP-DFLT-DUR
                TO (SF)-TP-DFLT-DUR
           END-IF
           IF DCO-DUR-AA-ADES-DFLT-NULL = HIGH-VALUES
              MOVE DCO-DUR-AA-ADES-DFLT-NULL
                TO (SF)-DUR-AA-ADES-DFLT-NULL
           ELSE
              MOVE DCO-DUR-AA-ADES-DFLT
                TO (SF)-DUR-AA-ADES-DFLT
           END-IF
           IF DCO-DUR-MM-ADES-DFLT-NULL = HIGH-VALUES
              MOVE DCO-DUR-MM-ADES-DFLT-NULL
                TO (SF)-DUR-MM-ADES-DFLT-NULL
           ELSE
              MOVE DCO-DUR-MM-ADES-DFLT
                TO (SF)-DUR-MM-ADES-DFLT
           END-IF
           IF DCO-DUR-GG-ADES-DFLT-NULL = HIGH-VALUES
              MOVE DCO-DUR-GG-ADES-DFLT-NULL
                TO (SF)-DUR-GG-ADES-DFLT-NULL
           ELSE
              MOVE DCO-DUR-GG-ADES-DFLT
                TO (SF)-DUR-GG-ADES-DFLT
           END-IF
           IF DCO-DT-SCAD-ADES-DFLT-NULL = HIGH-VALUES
              MOVE DCO-DT-SCAD-ADES-DFLT-NULL
                TO (SF)-DT-SCAD-ADES-DFLT-NULL
           ELSE
              MOVE DCO-DT-SCAD-ADES-DFLT
                TO (SF)-DT-SCAD-ADES-DFLT
           END-IF
           IF DCO-COD-FND-DFLT-NULL = HIGH-VALUES
              MOVE DCO-COD-FND-DFLT-NULL
                TO (SF)-COD-FND-DFLT-NULL
           ELSE
              MOVE DCO-COD-FND-DFLT
                TO (SF)-COD-FND-DFLT
           END-IF
           IF DCO-TP-DUR-NULL = HIGH-VALUES
              MOVE DCO-TP-DUR-NULL
                TO (SF)-TP-DUR-NULL
           ELSE
              MOVE DCO-TP-DUR
                TO (SF)-TP-DUR
           END-IF
           IF DCO-TP-CALC-DUR-NULL = HIGH-VALUES
              MOVE DCO-TP-CALC-DUR-NULL
                TO (SF)-TP-CALC-DUR-NULL
           ELSE
              MOVE DCO-TP-CALC-DUR
                TO (SF)-TP-CALC-DUR
           END-IF
           IF DCO-FL-NO-ADERENTI-NULL = HIGH-VALUES
              MOVE DCO-FL-NO-ADERENTI-NULL
                TO (SF)-FL-NO-ADERENTI-NULL
           ELSE
              MOVE DCO-FL-NO-ADERENTI
                TO (SF)-FL-NO-ADERENTI
           END-IF
           IF DCO-FL-DISTINTA-CNBTVA-NULL = HIGH-VALUES
              MOVE DCO-FL-DISTINTA-CNBTVA-NULL
                TO (SF)-FL-DISTINTA-CNBTVA-NULL
           ELSE
              MOVE DCO-FL-DISTINTA-CNBTVA
                TO (SF)-FL-DISTINTA-CNBTVA
           END-IF
           IF DCO-FL-CNBT-AUTES-NULL = HIGH-VALUES
              MOVE DCO-FL-CNBT-AUTES-NULL
                TO (SF)-FL-CNBT-AUTES-NULL
           ELSE
              MOVE DCO-FL-CNBT-AUTES
                TO (SF)-FL-CNBT-AUTES
           END-IF
           IF DCO-FL-QTZ-POST-EMIS-NULL = HIGH-VALUES
              MOVE DCO-FL-QTZ-POST-EMIS-NULL
                TO (SF)-FL-QTZ-POST-EMIS-NULL
           ELSE
              MOVE DCO-FL-QTZ-POST-EMIS
                TO (SF)-FL-QTZ-POST-EMIS
           END-IF
           IF DCO-FL-COMNZ-FND-IS-NULL = HIGH-VALUES
              MOVE DCO-FL-COMNZ-FND-IS-NULL
                TO (SF)-FL-COMNZ-FND-IS-NULL
           ELSE
              MOVE DCO-FL-COMNZ-FND-IS
                TO (SF)-FL-COMNZ-FND-IS
           END-IF
           MOVE DCO-DS-RIGA
             TO (SF)-DS-RIGA
           MOVE DCO-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE DCO-DS-VER
             TO (SF)-DS-VER
           MOVE DCO-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ
           MOVE DCO-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ
           MOVE DCO-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE DCO-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB.
       VALORIZZA-OUTPUT-DCO-EX.
           EXIT.

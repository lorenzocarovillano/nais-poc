       01 LDBV1661.
          05 LDBV1661-ID-ADES             PIC S9(009) COMP-3.
          05 LDBV1661-ID-OGG              PIC S9(009) COMP-3.
          05 LDBV1661-TP-OGG              PIC  X(002).
          05 LDBV1661-TP-STAT-TIT         PIC  X(002).
          05 LDBV1661-IMP-TOT             PIC S9(12)V9(3) COMP-3.

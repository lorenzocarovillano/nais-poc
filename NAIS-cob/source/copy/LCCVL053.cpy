
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVL053
      *   ULTIMO AGG. 06 GIU 2018
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-L05.
           MOVE L05-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE L05-TP-MOVI-ESEC
             TO (SF)-TP-MOVI-ESEC
           MOVE L05-TP-MOVI-RIFTO
             TO (SF)-TP-MOVI-RIFTO
           MOVE L05-GRAV-FUNZ-FUNZ
             TO (SF)-GRAV-FUNZ-FUNZ
           MOVE L05-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE L05-DS-VER
             TO (SF)-DS-VER
           MOVE L05-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE L05-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE L05-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           IF L05-COD-BLOCCO-NULL = HIGH-VALUES
              MOVE L05-COD-BLOCCO-NULL
                TO (SF)-COD-BLOCCO-NULL
           ELSE
              MOVE L05-COD-BLOCCO
                TO (SF)-COD-BLOCCO
           END-IF
           IF L05-SRVZ-VER-ANN-NULL = HIGH-VALUES
              MOVE L05-SRVZ-VER-ANN-NULL
                TO (SF)-SRVZ-VER-ANN-NULL
           ELSE
              MOVE L05-SRVZ-VER-ANN
                TO (SF)-SRVZ-VER-ANN
           END-IF
           MOVE L05-WHERE-CONDITION
             TO (SF)-WHERE-CONDITION
           IF L05-FL-POLI-IFP-NULL = HIGH-VALUES
              MOVE L05-FL-POLI-IFP-NULL
                TO (SF)-FL-POLI-IFP-NULL
           ELSE
              MOVE L05-FL-POLI-IFP
                TO (SF)-FL-POLI-IFP
           END-IF.
       VALORIZZA-OUTPUT-L05-EX.
           EXIT.

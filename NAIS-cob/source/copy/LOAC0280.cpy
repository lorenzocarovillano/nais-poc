      *----------------------------------------------------------------*
      *   AREA INPUT SERVIZIO VERIFICA PRESENZA BLOCCHI
      *   PORTAFOGLIO VITA - PROCESSO VENDITA
      *   LUNG.
      *   allineamento 10/09/2008
      *----------------------------------------------------------------*
           05 LOAC0280-DATI-INPUT.
              07 LOAC0280-ID-POLI                     PIC S9(9)  COMP-3.
              07 LOAC0280-ID-ADES                     PIC S9(9)  COMP-3.
              07 LOAC0280-TP-OGG                      PIC X(2).

           05 LOAC0280-DATI-OUTPUT.
              07 LOAC0280-FL-PRES-DEROGA              PIC X(1).
                 88 LOAC0280-SI-DEROGA                VALUE 'S'.
                 88 LOAC0280-NO-DEROGA                VALUE 'N'.
      *
              07 LOAC0280-GRAVITA-DEROGA              PIC X(1).
                 88 LOAC0280-DEROGA-BLOCCANTE-SI      VALUE 'S'.
                 88 LOAC0280-DEROGA-BLOCCANTE-NO      VALUE 'N'.
      *
              07 LOAC0280-LIVELLO-DEROGA              PIC X(08).
                 88 LOAC0280-NULL                VALUE '       '.
                 88 LOAC0280-LIV-POLI            VALUE 'POLIZZA'.
                 88 LOAC0280-LIV-ADES            VALUE 'ADESIONE'.


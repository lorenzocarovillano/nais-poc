      *----------------------------------------------------------------*
      * DIMENSIONAMENTO OCCURS PER LA TAB. TIT_CONT                     *
      * COPY RIPORTANTE IL NUMERO DI OCCURS DA UTILIZZARE              *
      * PER INIZIALIZZA TOT DELLA TABELLA                              *
      *----------------------------------------------------------------*
       01 WK-TIT-MAX.
          03 WK-TIT-MAX-A                 PIC 9(04) VALUE 200.
          03 WK-TIT-MAX-B                 PIC 9(04) VALUE 350.

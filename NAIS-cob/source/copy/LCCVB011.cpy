      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA BILA_FND_ESTR
      *   ALIAS B01
      *   ULTIMO AGG. 09 LUG 2010
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-BILA-FND-ESTR PIC S9(9)     COMP-3.
             07 (SF)-ID-BILA-TRCH-ESTR PIC S9(9)     COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-ID-RICH-ESTRAZ-MAS PIC S9(9)     COMP-3.
             07 (SF)-ID-RICH-ESTRAZ-AGG PIC S9(9)     COMP-3.
             07 (SF)-ID-RICH-ESTRAZ-AGG-NULL REDEFINES
                (SF)-ID-RICH-ESTRAZ-AGG   PIC X(5).
             07 (SF)-DT-RIS   PIC S9(8) COMP-3.
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-ID-ADES PIC S9(9)     COMP-3.
             07 (SF)-ID-GAR PIC S9(9)     COMP-3.
             07 (SF)-ID-TRCH-DI-GAR PIC S9(9)     COMP-3.
             07 (SF)-COD-FND PIC X(12).
             07 (SF)-DT-QTZ-INI   PIC S9(8) COMP-3.
             07 (SF)-DT-QTZ-INI-NULL REDEFINES
                (SF)-DT-QTZ-INI   PIC X(5).
             07 (SF)-NUM-QUO-INI PIC S9(7)V9(5) COMP-3.
             07 (SF)-NUM-QUO-INI-NULL REDEFINES
                (SF)-NUM-QUO-INI   PIC X(7).
             07 (SF)-NUM-QUO-DT-CALC PIC S9(7)V9(5) COMP-3.
             07 (SF)-NUM-QUO-DT-CALC-NULL REDEFINES
                (SF)-NUM-QUO-DT-CALC   PIC X(7).
             07 (SF)-VAL-QUO-INI PIC S9(7)V9(5) COMP-3.
             07 (SF)-VAL-QUO-INI-NULL REDEFINES
                (SF)-VAL-QUO-INI   PIC X(7).
             07 (SF)-DT-VALZZ-QUO-DT-CA   PIC S9(8) COMP-3.
             07 (SF)-DT-VALZZ-QUO-DT-CA-NULL REDEFINES
                (SF)-DT-VALZZ-QUO-DT-CA   PIC X(5).
             07 (SF)-VAL-QUO-DT-CALC PIC S9(7)V9(5) COMP-3.
             07 (SF)-VAL-QUO-DT-CALC-NULL REDEFINES
                (SF)-VAL-QUO-DT-CALC   PIC X(7).
             07 (SF)-VAL-QUO-T PIC S9(7)V9(5) COMP-3.
             07 (SF)-VAL-QUO-T-NULL REDEFINES
                (SF)-VAL-QUO-T   PIC X(7).
             07 (SF)-PC-INVST PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-INVST-NULL REDEFINES
                (SF)-PC-INVST   PIC X(4).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).

      *----------------------------------------------------------------*
      *    DISPLAY TABELLA DETTAGLIO TITOLO CONTABILE
      *    N.B Per utilizzare la copy bisogna includere la copy
      *    Infrastrutturale IDSV8888
      *----------------------------------------------------------------*
       DISPLAY-TAB-DTC.

           DISPLAY 'WDTC-STATUS           (' IX-TAB-DTC ') = '
                    WDTC-STATUS             (IX-TAB-DTC)
           DISPLAY 'WDTC-ID-PTF           (' IX-TAB-DTC ') = '
                    WDTC-ID-PTF             (IX-TAB-DTC)
           DISPLAY 'WDTC-ID-DETT-TIT-CONT (' IX-TAB-DTC ') = '
                    WDTC-ID-DETT-TIT-CONT   (IX-TAB-DTC)
           DISPLAY 'WDTC-ID-TIT-CONT      (' IX-TAB-DTC ') = '
                    WDTC-ID-TIT-CONT        (IX-TAB-DTC)
           DISPLAY 'WDTC-ID-OGG           (' IX-TAB-DTC ') = '
                    WDTC-ID-OGG             (IX-TAB-DTC)
           DISPLAY 'WDTC-TP-OGG           (' IX-TAB-DTC ') = '
                    WDTC-TP-OGG             (IX-TAB-DTC)
           DISPLAY 'WDTC-ID-MOVI-CRZ      (' IX-TAB-DTC ') = '
                    WDTC-ID-MOVI-CRZ        (IX-TAB-DTC)
           DISPLAY 'WDTC-ID-MOVI-CHIU     (' IX-TAB-DTC ') = '
                    WDTC-ID-MOVI-CHIU       (IX-TAB-DTC)
           DISPLAY 'WDTC-DT-INI-EFF       (' IX-TAB-DTC ') = '
                    WDTC-DT-INI-EFF         (IX-TAB-DTC)
           DISPLAY 'WDTC-DT-END-EFF       (' IX-TAB-DTC ') = '
                    WDTC-DT-END-EFF         (IX-TAB-DTC)
           DISPLAY 'WDTC-COD-COMP-ANIA    (' IX-TAB-DTC ') = '
                    WDTC-COD-COMP-ANIA      (IX-TAB-DTC)
           DISPLAY 'WDTC-DT-INI-COP       (' IX-TAB-DTC ') = '
                    WDTC-DT-INI-COP         (IX-TAB-DTC)
           DISPLAY 'WDTC-DT-END-COP       (' IX-TAB-DTC ') = '
                    WDTC-DT-END-COP         (IX-TAB-DTC)
           DISPLAY 'WDTC-PRE-NET          (' IX-TAB-DTC ') = '
                    WDTC-PRE-NET            (IX-TAB-DTC)
           DISPLAY 'WDTC-INTR-FRAZ        (' IX-TAB-DTC ') = '
                    WDTC-INTR-FRAZ          (IX-TAB-DTC)
           DISPLAY 'WDTC-INTR-MORA        (' IX-TAB-DTC ') = '
                    WDTC-INTR-MORA          (IX-TAB-DTC)
           DISPLAY 'WDTC-INTR-RETDT       (' IX-TAB-DTC ') = '
                    WDTC-INTR-RETDT         (IX-TAB-DTC)
           DISPLAY 'WDTC-INTR-RIAT        (' IX-TAB-DTC ') = '
                    WDTC-INTR-RIAT          (IX-TAB-DTC)
           DISPLAY 'WDTC-DIR              (' IX-TAB-DTC ') = '
                    WDTC-DIR                (IX-TAB-DTC)
           DISPLAY 'WDTC-SPE-MED          (' IX-TAB-DTC ') = '
                    WDTC-SPE-MED            (IX-TAB-DTC)
           DISPLAY 'WDTC-TAX              (' IX-TAB-DTC ') = '
                    WDTC-TAX                (IX-TAB-DTC)
           DISPLAY 'WDTC-SOPR-SAN         (' IX-TAB-DTC ') = '
                    WDTC-SOPR-SAN           (IX-TAB-DTC)
           DISPLAY 'WDTC-SOPR-SPO         (' IX-TAB-DTC ') = '
                    WDTC-SOPR-SPO           (IX-TAB-DTC)
           DISPLAY 'WDTC-SOPR-TEC         (' IX-TAB-DTC ') = '
                    WDTC-SOPR-TEC           (IX-TAB-DTC)
           DISPLAY 'WDTC-SOPR-PROF        (' IX-TAB-DTC ') = '
                    WDTC-SOPR-PROF          (IX-TAB-DTC)
           DISPLAY 'WDTC-SOPR-ALT         (' IX-TAB-DTC ') = '
                    WDTC-SOPR-ALT           (IX-TAB-DTC)
           DISPLAY 'WDTC-PRE-TOT          (' IX-TAB-DTC ') = '
                    WDTC-PRE-TOT            (IX-TAB-DTC)
           DISPLAY 'WDTC-PRE-PP-IAS       (' IX-TAB-DTC ') = '
                    WDTC-PRE-PP-IAS         (IX-TAB-DTC)
           DISPLAY 'WDTC-PRE-SOLO-RSH     (' IX-TAB-DTC ') = '
                    WDTC-PRE-SOLO-RSH       (IX-TAB-DTC)
           DISPLAY 'WDTC-CAR-ACQ          (' IX-TAB-DTC ') = '
                    WDTC-CAR-ACQ            (IX-TAB-DTC)
           DISPLAY 'WDTC-CAR-GEST         (' IX-TAB-DTC ') = '
                    WDTC-CAR-GEST           (IX-TAB-DTC)
           DISPLAY 'WDTC-CAR-INC          (' IX-TAB-DTC ') = '
                    WDTC-CAR-INC            (IX-TAB-DTC)
           DISPLAY 'WDTC-PROV-ACQ-1AA     (' IX-TAB-DTC ') = '
                    WDTC-PROV-ACQ-1AA       (IX-TAB-DTC)
           DISPLAY 'WDTC-PROV-ACQ-2AA     (' IX-TAB-DTC ') = '
                    WDTC-PROV-ACQ-2AA       (IX-TAB-DTC)
           DISPLAY 'WDTC-PROV-RICOR       (' IX-TAB-DTC ') = '
                    WDTC-PROV-RICOR         (IX-TAB-DTC)
           DISPLAY 'WDTC-PROV-INC         (' IX-TAB-DTC ') = '
                    WDTC-PROV-INC           (IX-TAB-DTC)
           DISPLAY 'WDTC-PROV-DA-REC      (' IX-TAB-DTC ') = '
                    WDTC-PROV-DA-REC        (IX-TAB-DTC)
           DISPLAY 'WDTC-COD-DVS          (' IX-TAB-DTC ') = '
                    WDTC-COD-DVS            (IX-TAB-DTC)
           DISPLAY 'WDTC-FRQ-MOVI         (' IX-TAB-DTC ') = '
                    WDTC-FRQ-MOVI           (IX-TAB-DTC)
           DISPLAY 'WDTC-TP-RGM-FISC      (' IX-TAB-DTC ') = '
                    WDTC-TP-RGM-FISC        (IX-TAB-DTC)
           DISPLAY 'WDTC-COD-TARI         (' IX-TAB-DTC ') = '
                    WDTC-COD-TARI           (IX-TAB-DTC)
           DISPLAY 'WDTC-TP-STAT-TIT      (' IX-TAB-DTC ') = '
                    WDTC-TP-STAT-TIT        (IX-TAB-DTC)
           DISPLAY 'WDTC-IMP-AZ           (' IX-TAB-DTC ') = '
                    WDTC-IMP-AZ             (IX-TAB-DTC)
           DISPLAY 'WDTC-IMP-ADER         (' IX-TAB-DTC ') = '
                    WDTC-IMP-ADER           (IX-TAB-DTC)
           DISPLAY 'WDTC-IMP-TFR          (' IX-TAB-DTC ') = '
                    WDTC-IMP-TFR            (IX-TAB-DTC)
           DISPLAY 'WDTC-IMP-VOLO         (' IX-TAB-DTC ') = '
                    WDTC-IMP-VOLO           (IX-TAB-DTC)
           DISPLAY 'WDTC-MANFEE-ANTIC     (' IX-TAB-DTC ') = '
                    WDTC-MANFEE-ANTIC       (IX-TAB-DTC)
           DISPLAY 'WDTC-MANFEE-RICOR     (' IX-TAB-DTC ') = '
                    WDTC-MANFEE-RICOR       (IX-TAB-DTC)
           DISPLAY 'WDTC-MANFEE-REC       (' IX-TAB-DTC ') = '
                    WDTC-MANFEE-REC         (IX-TAB-DTC)
           DISPLAY 'WDTC-DT-ESI-TIT       (' IX-TAB-DTC ') = '
                    WDTC-DT-ESI-TIT         (IX-TAB-DTC)
           DISPLAY 'WDTC-SPE-AGE          (' IX-TAB-DTC ') = '
                    WDTC-SPE-AGE            (IX-TAB-DTC)
           DISPLAY 'WDTC-CAR-IAS          (' IX-TAB-DTC ') = '
                    WDTC-CAR-IAS            (IX-TAB-DTC)
           DISPLAY 'WDTC-TOT-INTR-PREST   (' IX-TAB-DTC ') = '
                    WDTC-TOT-INTR-PREST     (IX-TAB-DTC)
           DISPLAY 'WDTC-DS-RIGA          (' IX-TAB-DTC ') = '
                    WDTC-DS-RIGA            (IX-TAB-DTC)
           DISPLAY 'WDTC-DS-OPER-SQL      (' IX-TAB-DTC ') = '
                    WDTC-DS-OPER-SQL        (IX-TAB-DTC)
           DISPLAY 'WDTC-DS-VER           (' IX-TAB-DTC ') = '
                    WDTC-DS-VER             (IX-TAB-DTC)
           DISPLAY 'WDTC-DS-TS-INI-CPTZ   (' IX-TAB-DTC ') = '
                    WDTC-DS-TS-INI-CPTZ     (IX-TAB-DTC)
           DISPLAY 'WDTC-DS-TS-END-CPTZ   (' IX-TAB-DTC ') = '
                    WDTC-DS-TS-END-CPTZ     (IX-TAB-DTC)
           DISPLAY 'WDTC-DS-UTENTE        (' IX-TAB-DTC ') = '
                    WDTC-DS-UTENTE          (IX-TAB-DTC)
           DISPLAY 'WDTC-DS-STATO-ELAB    (' IX-TAB-DTC ') = '
                    WDTC-DS-STATO-ELAB      (IX-TAB-DTC).

       DISPLAY-TAB-DTC-EX.

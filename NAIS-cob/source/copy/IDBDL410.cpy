           EXEC SQL DECLARE QUOTZ_AGG_FND TABLE
           (
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             TP_QTZ              CHAR(2) NOT NULL,
             COD_FND             CHAR(12) NOT NULL,
             DT_QTZ              DATE NOT NULL,
             TP_FND              CHAR(1) NOT NULL,
             VAL_QUO             DECIMAL(12, 5) NOT NULL,
             DESC_AGG            VARCHAR(100),
             DS_OPER_SQL         CHAR(1) NOT NULL,
             DS_VER              DECIMAL(9, 0) NOT NULL,
             DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL,
             DS_UTENTE           CHAR(20) NOT NULL,
             DS_STATO_ELAB       CHAR(1) NOT NULL
          ) END-EXEC.

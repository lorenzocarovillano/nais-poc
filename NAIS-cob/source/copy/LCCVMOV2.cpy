      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************

       LETTURA-MOV.

           IF IDSI0011-SELECT
              PERFORM SELECT-MOV
                 THRU SELECT-MOV-EX
           ELSE
              PERFORM FETCH-MOV
                 THRU FETCH-MOV-EX
           END-IF.

       LETTURA-MOV-EX.
           EXIT.

       SELECT-MOV.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

              IF IDSO0011-SUCCESSFUL-RC
                 EVALUATE TRUE
                     WHEN IDSO0011-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                        MOVE IDSO0011-BUFFER-DATI
                          TO MOVI
                        PERFORM VALORIZZA-OUTPUT-MOV
                           THRU VALORIZZA-OUTPUT-MOV-EX
                     WHEN IDSO0011-NOT-FOUND
      *--->          CAMPO $ NON TROVATO
                            MOVE WK-PGM
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'SELECT-MOV'
                             TO IEAI9901-LABEL-ERR
                           MOVE '005019'
                             TO IEAI9901-COD-ERRORE
                           MOVE 'ID-MOVI'
                             TO IEAI9901-PARAMETRI-ERR
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                      WHEN OTHER
      *--->          ERRORE DI ACCESSO AL DB
                        MOVE WK-PGM
                                        TO IEAI9901-COD-SERVIZIO-BE
                        MOVE 'SELECT-MOV'
                                        TO IEAI9901-LABEL-ERR
                        MOVE '005015'   TO IEAI9901-COD-ERRORE
                        MOVE WK-TABELLA TO IEAI9901-PARAMETRI-ERR
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                           THRU EX-S0300
                 END-EVALUATE
              ELSE
      *-->       GESTIRE ERRORE DISPATCHER
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'SELECT-MOV'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005016'
                   TO IEAI9901-COD-ERRORE
                 MOVE SPACES
                   TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF.

       SELECT-MOV-EX.
           EXIT.

       FETCH-MOV.

           CONTINUE.

      *    SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
      *    SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
      *    SET  WCOM-OVERFLOW-NO                  TO TRUE.
      *
      *----------------------------------------------------------------*
      *    PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC  OR
      *                  NOT IDSO0011-SUCCESSFUL-SQL
      *                   OR WCOM-OVERFLOW-YES
      *
      *          PERFORM CALL-DISPATCHER
      *             THRU CALL-DISPATCHER-EX
      *
      *          IF IDSO0011-SUCCESSFUL-RC
      *           EVALUATE TRUE
      *               WHEN IDSO0011-NOT-FOUND
      *-->                 NON TROVATA
      *                    IF IDSI0011-FETCH-FIRST
      *                       MOVE WK-PGM
      *                         TO IEAI9901-COD-SERVIZIO-BE
      *                       MOVE 'S1400-FETCH-DISPATCHER'
      *                         TO IEAI9901-LABEL-ERR
      *                       MOVE '005019'
      *                         TO IEAI9901-COD-ERRORE
      *                       MOVE 'ID-DETT-TIT-CONT'
      *                         TO IEAI9901-PARAMETRI-ERR
      *                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
      *                          THRU EX-S0300
      *                    END-IF
      *               WHEN IDSO0011-SUCCESSFUL-SQL
      *-->                 OPERAZIONE ESEGUITA CORRETTAMENTE
      *                    MOVE IDSO0011-BUFFER-DATI  TO MOVI
      *                    ADD  1                     TO WK-CONT-FETCH
      *
      *-->  Se il contatore di lettura � maggiore dell' elemento MAX
      *-->  previsto per la TAB.PARAM-COMP allora siamo in overflow
      *                    IF WK-CONT-FETCH > (SF)-ELE-MOV-MAX
      *                       SET WCOM-OVERFLOW-YES   TO TRUE
      *                    ELSE
      *                       ADD 1  TO IX-TAB-???
      *                       PERFORM SOU17-VALOUT-MOV
      *                          THRU EX-SOU17
      *                       SET IDSI0011-FETCH-NEXT TO TRUE
      *                    END-IF
      *               WHEN OTHER
      *-->                 ERRORE DI ACCESSO AL DB
      *                    MOVE WK-PGM
      *                      TO IEAI9901-COD-SERVIZIO-BE
      *                    MOVE 'S1400-FETCH-DISPATCHER'
      *                      TO IEAI9901-LABEL-ERR
      *                    MOVE '005015'
      *                      TO IEAI9901-COD-ERRORE
      *                    MOVE WK-TABELLA
      *                      TO IEAI9901-PARAMETRI-ERR
      *                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
      *                       THRU EX-S0300
      *           END-EVALUATE
      *        ELSE
      *-->        ERRORE DISPATCHER
      *           MOVE WK-PGM
      *             TO IEAI9901-COD-SERVIZIO-BE
      *           MOVE 'S1400-FETCH-DISPATCHER'
      *             TO IEAI9901-LABEL-ERR
      *           MOVE '005016'
      *             TO IEAI9901-COD-ERRORE
      *           MOVE SPACES
      *             TO IEAI9901-PARAMETRI-ERR
      *           PERFORM S0300-RICERCA-GRAVITA-ERRORE
      *              THRU EX-S0300
      *        END-IF
      *    END-PERFORM.

       FETCH-MOV-EX.
           EXIT.

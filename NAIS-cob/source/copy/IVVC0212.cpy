      *----------------------------------------------------------------*
      *   AREA INPUT VARIABILI
      *   PORTAFOGLIO VITA - PROCESSO VENDITA
      *   LUNG.
      *----------------------------------------------------------------*
           05 (SF)-AREA-VARIABILI-CONT.
              06 (SF)-ELE-VAR-CONT-MAX         PIC S9(04) COMP-3.
              06 (SF)-TAB-VAR.
                 07 (SF)-TAB-VAR-CONT       OCCURS 500.
                   10 (SF)-AREA-VAR-CONT.
                      13 (SF)-COD-VAR-CONT     PIC  X(12).
                      13 (SF)-TP-DATO-CONT     PIC  X(01).
                      13 (SF)-VAL-IMP-CONT     PIC  S9(11)V9(07).
                      13 (SF)-VAL-PERC-CONT    PIC   9(05)V9(09).
                      13 (SF)-VAL-STR-CONT     PIC  X(12).
                      13 (SF)-TP-LIVELLO       PIC  X(01).
                      13 (SF)-COD-LIVELLO      PIC  X(12).
                      13 (SF)-ID-LIVELLO       PIC   9(09).
              06 (SF)-TAB-VAR-R REDEFINES (SF)-TAB-VAR.
                 07 FILLER                     PIC  X(79).
                 07 (SF)-RESTO-TAB-VAR         PIC  X(39421).


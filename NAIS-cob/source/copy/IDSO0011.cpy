      ***************************************************************
      *   Area Output
      *  - Dispatcher -
      *   LUNGHEZZA :
      ***************************************************************
       02 IDSO0011-AREA.
          10 IDSO0011-MAX                  PIC 9(02).
          10 IDSO0011-COMPOSIZIONE-BUFFER.
            15 IDSO0011-ELEMENTI-BUFFER    OCCURS 30
                               INDEXED BY   IDSO0011-INDEX.
              20 IDSO0011-CODICE-STR-DATO   PIC X(30).
              20 IDSO0011-INIZIO-POSIZIONE  PIC S9(05) COMP-3.
              20 IDSO0011-LUNGHEZZA-DATO    PIC S9(05) COMP-3.

          10 IDSO0011-FLAG-CODA-TS          PIC X(01) VALUE 'N'.
             88 IDSO0011-DELETE-SI              VALUE 'S'.
             88 IDSO0011-DELETE-NO              VALUE 'N'.

      * --  CAMPI ERRORI
      *      COPY IDSV0006 REPLACING ==IDSO0011== BY ==IDSO0011==.
           10 IDSO0011-RETURN-CODE               PIC  X(002).
             88 IDSO0011-SUCCESSFUL-RC           VALUE '00'.
             88 IDSO0011-INVALID-LEVEL-OPER      VALUE 'D1'.
             88 IDSO0011-INVALID-OPER            VALUE 'D2'.
             88 IDSO0011-SQL-ERROR               VALUE 'D3'.
             88 IDSO0011-FIELD-NOT-VALUED        VALUE 'C1'.
             88 IDSO0011-INVALID-CONVERSION      VALUE 'C2'.
             88 IDSO0011-SEQUENCE-NOT-FOUND      VALUE 'S1'.
             88 IDSO0011-COD-COMP-NOT-VALID      VALUE 'ZA'.
             88 IDSO0011-STR-DATO-NOT-VALID      VALUE 'ZB'.
             88 IDSO0011-BUFFER-DATI-NOT-V       VALUE 'ZC'.
             88 IDSO0011-STR-DATO-DB-NOT-F       VALUE 'ZD'.
             88 IDSO0011-COD-SERV-NOT-V          VALUE 'ZE'.
             88 IDSO0011-EXCESS-COPY-I-O         VALUE 'ZF'.
             88 IDSO0011-TIPO-MOVIM-NOT-V        VALUE 'ZG'.
             88 IDSO0011-OPER-NOT-V              VALUE 'ZH'.
             88 IDSO0011-SH-MEMORY-NOT-V         VALUE 'ZI'.
             88 IDSO0011-QUEUE-MANAGEMENT        VALUE 'ZL'.
             88 IDSO0011-SH-MEMORY-FLOOD         VALUE 'ZM'.
             88 IDSO0011-EXCESS-SERV-NEWLIFE     VALUE 'ZO'.
             88 IDSO0011-EXCESS-SERV-VSAM        VALUE 'ZP'.
             88 IDSO0011-KEY-FIELDS-NOT-F        VALUE 'ZQ'.
             88 IDSO0011-WHERE-FIELDS-NOT-F      VALUE 'ZR'.
             88 IDSO0011-SERV-NOT-F-ON-MSS       VALUE 'ZS'.
             88 IDSO0011-STR-RED-NOT-F-ON-RDS    VALUE 'ZT'.
             88 IDSO0011-INSERT-SH-MEMORY        VALUE 'ZU'.
             88 IDSO0011-CONVERTER-NOT-F         VALUE 'ZV'.
             88 IDSO0011-EXCESS-OF-RECURSION     VALUE 'ZW'.
             88 IDSO0011-VALUE-DEFAULT-NOT-F     VALUE 'ZX'.
             88 IDSO0011-COPY-NOT-F-ON-IOS       VALUE 'ZY'.
             88 IDSO0011-VALUE-NOT-F-ON-VSS      VALUE 'ZZ'.
      *   fine COPY IDSV0004


      *
          10 IDSO0011-SQLCODE-SIGNED       PIC S9(009).
             88 IDSO0011-SUCCESSFUL-SQL    VALUE ZERO.
             88 IDSO0011-NOT-FOUND         VALUE +100.
             88 IDSO0011-DUPLICATE-KEY     VALUE -803.
             88 IDSO0011-MORE-THAN-ONE-ROW VALUE -811.
             88 IDSO0011-DEADLOCK-TIMEOUT  VALUE -911
                                             -913.
             88 IDSO0011-CONNECTION-ERROR  VALUE -924.

          10 IDSO0011-SQLCODE                 PIC --------9.

          10 IDSO0011-CAMPI-ESITO.
             15 IDSO0011-DESCRIZ-ERR-DB2      PIC  X(300).
             15 IDSO0011-COD-SERVIZIO-BE      PIC  X(008).
             15 IDSO0011-NOME-TABELLA         PIC  X(018).
             15 IDSO0011-KEY-TABELLA          PIC  X(020).
             15 IDSO0011-NUM-RIGHE-LETTE      PIC  9(002).
      *   fine COPY IDSV0006



      *----------------------------------------------------------------*
      *   AREA INPUT COPY GESTIONE DEROGA
      *----------------------------------------------------------------*
       01  LOAC0002-AREA-GEST-DEROGHE.
           05  LOAC0002-IND-R                         PIC 9(2).
           05  LOAC0002-IND-C                         PIC 9(2).
           05  LOAC0002-TP-MOVI                       PIC 9(5).
           05  LOAC0002-FL-DEROGA                     PIC X.
               88 LOAC0002-DEROGA-NON-TROVATA         VALUE 'N'.
               88 LOAC0002-DEROGA-TROVATA             VALUE 'S'.
           05  LOAC0002-ELE-MAX-FUNZ                  PIC 9(2) VALUE 1.
           05  LOAC0002-TAB-FUNZ-DEROGA.
      *    ADEGUAMENTO PREMIO PRESTAZIONE
                10 FILLER                     PIC 9(05) VALUE 06006.
      *       RISCATTO PARZIALE
                10 FILLER                     PIC 9(05) VALUE 05003.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 05005.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 05006.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 05008.
                10 FILLER                     PIC X     VALUE 'B'.
      *       RISCATTO TOTALE
                10 FILLER                     PIC 9(05) VALUE 03010.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 03012.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 05009.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 05010.
                10 FILLER                     PIC X     VALUE 'B'.
      *       SINISTRO
                10 FILLER                     PIC 9(05) VALUE 03018.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 03019.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 05017.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 05018.
                10 FILLER                     PIC X     VALUE 'B'.
      *       SCADENZA
                10 FILLER                     PIC 9(05) VALUE 03015.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 03017.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 05014.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 05015.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 05019.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 05020.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 05026.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 05027.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 06009.
                10 FILLER                     PIC X     VALUE 'B'.
      *       SWITCH
                10 FILLER                     PIC 9(05) VALUE 02028.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 02063.
                10 FILLER                     PIC X     VALUE 'B'.
      *       SWITCH DI LINEA PER YEL
                10 FILLER                     PIC 9(05) VALUE 02071.
                10 FILLER                     PIC X     VALUE 'B'.
      *       YOUR LIFE TOP
                10 FILLER                     PIC 9(05) VALUE 02316.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 02317.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 02318.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 02319.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 02320.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 02321.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 02322.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 02323.
                10 FILLER                     PIC X     VALUE 'B'.
      *       QUICK PORTFOLIO
                10 FILLER                     PIC 9(05) VALUE 02061.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 02062.
                10 FILLER                     PIC X     VALUE 'B'.
      *       TAKE PROFIT
                10 FILLER                     PIC 9(05) VALUE 02318.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 02319.
                10 FILLER                     PIC X     VALUE 'B'.
10819X*         10 FILLER                     PIC X(24).
10819X*       REDDITO PROGRAMMATO
                10 FILLER                     PIC 9(05) VALUE 02316.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 02317.
                10 FILLER                     PIC X     VALUE 'B'.
10819X*       RISCATTO TOTALE INCAPIENZA
                10 FILLER                     PIC 9(05) VALUE 02324.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 02325.
                10 FILLER                     PIC X     VALUE 'B'.
10819X*       BENEFICIO INCONTROLLATO
                10 FILLER                     PIC 9(05) VALUE 02322.
                10 FILLER                     PIC X     VALUE 'B'.
                10 FILLER                     PIC 9(05) VALUE 02323.
                10 FILLER                     PIC X     VALUE 'B'.
12908           10 FILLER                     PIC 9(05) VALUE 02072.
12908           10 FILLER                     PIC X     VALUE 'B'.
10819X*         10 FILLER                     PIC X(00).
           05  LOAC0002-TAB-FUNZ-DEROGA-R REDEFINES
               LOAC0002-TAB-FUNZ-DEROGA.
             07 LOAC0002-ELE-FUNZ-DEROGA OCCURS 1.
                10 LOAC0002-FUNZ-ESEC                 PIC 9(5).
12908 *         10 LOAC0002-TAB-FUNZ-DEROG-BLOCC OCCURS 40.
12908           10 LOAC0002-TAB-FUNZ-DEROG-BLOCC OCCURS 43.
                   15 LOAC0002-FUNZ-DEROG             PIC 9(5).
      *    TIPO DEROGA - 'N' NON BLOCCANTE - 'B' BLOCCANTE
                   15 LOAC0002-TIPO-DEROGA            PIC X.




      *----------------------------------------------------------------*
      *    COPY      ..... LCCVSDI6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO STRATEGIA DI INVESTIMENTO
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVSDI5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN STRATEGIA DI INVESTIMENTO (LCCVSDI1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-STRA-DI-INVST.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE STRA-DI-INVST

      *--> NOME TABELLA FISICA DB
           MOVE 'STRA-DI-INVST'               TO WK-TABELLA

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WSDI-ST-INV(IX-TAB-SDI)
              AND WSDI-ELE-STRA-INV-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WSDI-ST-ADD(IX-TAB-SDI)

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK
                        MOVE S090-SEQ-TABELLA TO WSDI-ID-PTF(IX-TAB-SDI)
                                                 SDI-ID-STRA-DI-INVST

      *-->              PREPARAZIONE ED ESTRAZIONE OGGETTO PTF
                        PERFORM PREPARA-AREA-LCCS0234-SDI
                           THRU PREPARA-AREA-LCCS0234-SDI-EX

                        PERFORM CALL-LCCS0234
                           THRU CALL-LCCS0234-EX

                        IF IDSV0001-ESITO-OK
                           MOVE S234-ID-OGG-PTF-EOC
                             TO SDI-ID-OGG
                           MOVE WMOV-ID-PTF      TO SDI-ID-MOVI-CRZ
                        END-IF

                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WSDI-ST-MOD(IX-TAB-SDI)

                       MOVE WSDI-ID-PTF(IX-TAB-SDI)
                         TO SDI-ID-STRA-DI-INVST
                       MOVE WSDI-ID-OGG(IX-TAB-SDI)
                         TO SDI-ID-OGG
                       MOVE WMOV-ID-PTF
                         TO SDI-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WSDI-ST-DEL(IX-TAB-SDI)

                       MOVE WSDI-ID-PTF(IX-TAB-SDI)
                         TO SDI-ID-STRA-DI-INVST
                       MOVE WSDI-ID-OGG(IX-TAB-SDI)
                         TO SDI-ID-OGG
                       MOVE WMOV-ID-PTF
                         TO SDI-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN ADESIONE
                 PERFORM VAL-DCLGEN-SDI
                    THRU VAL-DCLGEN-SDI-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-SDI
                    THRU VALORIZZA-AREA-DSH-SDI-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-STRA-DI-INVST-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-SDI.

      *--> DCLGEN TABELLA
           MOVE STRA-DI-INVST           TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           IF WSDI-ST-ADD(IX-TAB-SDI)

              MOVE SDI-DT-INI-EFF      TO IDSI0011-DATA-INIZIO-EFFETTO
                                          IDSI0011-DATA-FINE-EFFETTO

           ELSE

              MOVE WMOV-DT-EFF         TO IDSI0011-DATA-INIZIO-EFFETTO
              MOVE ZERO                TO IDSI0011-DATA-FINE-EFFETTO

           END-IF.

           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-SDI-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    PREPARA AREA PER CALL LCCS0234
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234-SDI.

             MOVE WSDI-ID-OGG(IX-TAB-SDI)      TO S234-ID-OGG-EOC.
             MOVE WSDI-TP-OGG(IX-TAB-SDI)      TO S234-TIPO-OGG-EOC.

       PREPARA-AREA-LCCS0234-SDI-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVSDI5.

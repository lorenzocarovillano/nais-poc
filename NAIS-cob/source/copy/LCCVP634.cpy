
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVP634
      *   ULTIMO AGG. 24 GEN 2014
      *------------------------------------------------------------

       INIZIA-TOT-P63.

           PERFORM INIZIA-ZEROES-P63 THRU INIZIA-ZEROES-P63-EX

           PERFORM INIZIA-SPACES-P63 THRU INIZIA-SPACES-P63-EX

           PERFORM INIZIA-NULL-P63 THRU INIZIA-NULL-P63-EX.

       INIZIA-TOT-P63-EX.
           EXIT.

       INIZIA-NULL-P63.
           MOVE HIGH-VALUES TO (SF)-DESC-ACC-COMM-MAST.
       INIZIA-NULL-P63-EX.
           EXIT.

       INIZIA-ZEROES-P63.
           MOVE 0 TO (SF)-ID-ACC-COMM
           MOVE 0 TO (SF)-COD-COMP-ANIA
           MOVE 0 TO (SF)-COD-PARTNER
           MOVE 0 TO (SF)-DT-FIRMA
           MOVE 0 TO (SF)-DT-INI-VLDT
           MOVE 0 TO (SF)-DT-END-VLDT
           MOVE 0 TO (SF)-DS-VER
           MOVE 0 TO (SF)-DS-TS-CPTZ.
       INIZIA-ZEROES-P63-EX.
           EXIT.

       INIZIA-SPACES-P63.
           MOVE SPACES TO (SF)-TP-ACC-COMM
           MOVE SPACES TO (SF)-IB-ACC-COMM
           MOVE SPACES TO (SF)-IB-ACC-COMM-MASTER
           MOVE SPACES TO (SF)-DESC-ACC-COMM
           MOVE SPACES TO (SF)-FL-INVIO-CONFERME
           MOVE SPACES TO (SF)-DS-OPER-SQL
           MOVE SPACES TO (SF)-DS-UTENTE
           MOVE SPACES TO (SF)-DS-STATO-ELAB.
       INIZIA-SPACES-P63-EX.
           EXIT.

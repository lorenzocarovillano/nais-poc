
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVP675
      *   ULTIMO AGG. 11 MAR 2014
      *------------------------------------------------------------

       VAL-DCLGEN-P67.
      *    MOVE (SF)-ID-EST-POLI-CPI-PR
      *       TO P67-ID-EST-POLI-CPI-PR
      *    MOVE (SF)-ID-MOVI-CRZ
      *       TO P67-ID-MOVI-CRZ
           IF (SF)-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL
              TO P67-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU
              TO P67-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF NOT NUMERIC
              MOVE 0 TO P67-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF
              TO P67-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF NOT NUMERIC
              MOVE 0 TO P67-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF
              TO P67-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA
              TO P67-COD-COMP-ANIA
           MOVE (SF)-IB-OGG
              TO P67-IB-OGG
           IF (SF)-DS-RIGA NOT NUMERIC
              MOVE 0 TO P67-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA
              TO P67-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO P67-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO P67-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO P67-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ NOT NUMERIC
              MOVE 0 TO P67-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ
              TO P67-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ NOT NUMERIC
              MOVE 0 TO P67-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ
              TO P67-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO P67-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO P67-DS-STATO-ELAB
           MOVE (SF)-COD-PROD-ESTNO
              TO P67-COD-PROD-ESTNO
           IF (SF)-CPT-FIN-NULL = HIGH-VALUES
              MOVE (SF)-CPT-FIN-NULL
              TO P67-CPT-FIN-NULL
           ELSE
              MOVE (SF)-CPT-FIN
              TO P67-CPT-FIN
           END-IF
           IF (SF)-NUM-TST-FIN-NULL = HIGH-VALUES
              MOVE (SF)-NUM-TST-FIN-NULL
              TO P67-NUM-TST-FIN-NULL
           ELSE
              MOVE (SF)-NUM-TST-FIN
              TO P67-NUM-TST-FIN
           END-IF
           IF (SF)-TS-FINANZ-NULL = HIGH-VALUES
              MOVE (SF)-TS-FINANZ-NULL
              TO P67-TS-FINANZ-NULL
           ELSE
              MOVE (SF)-TS-FINANZ
              TO P67-TS-FINANZ
           END-IF
           IF (SF)-DUR-MM-FINANZ-NULL = HIGH-VALUES
              MOVE (SF)-DUR-MM-FINANZ-NULL
              TO P67-DUR-MM-FINANZ-NULL
           ELSE
              MOVE (SF)-DUR-MM-FINANZ
              TO P67-DUR-MM-FINANZ
           END-IF
           IF (SF)-DT-END-FINANZ-NULL = HIGH-VALUES
              MOVE (SF)-DT-END-FINANZ-NULL
              TO P67-DT-END-FINANZ-NULL
           ELSE
             IF (SF)-DT-END-FINANZ = ZERO
                MOVE HIGH-VALUES
                TO P67-DT-END-FINANZ-NULL
             ELSE
              MOVE (SF)-DT-END-FINANZ
              TO P67-DT-END-FINANZ
             END-IF
           END-IF
           IF (SF)-AMM-1A-RAT-NULL = HIGH-VALUES
              MOVE (SF)-AMM-1A-RAT-NULL
              TO P67-AMM-1A-RAT-NULL
           ELSE
              MOVE (SF)-AMM-1A-RAT
              TO P67-AMM-1A-RAT
           END-IF
           IF (SF)-VAL-RISC-BENE-NULL = HIGH-VALUES
              MOVE (SF)-VAL-RISC-BENE-NULL
              TO P67-VAL-RISC-BENE-NULL
           ELSE
              MOVE (SF)-VAL-RISC-BENE
              TO P67-VAL-RISC-BENE
           END-IF
           IF (SF)-AMM-RAT-END-NULL = HIGH-VALUES
              MOVE (SF)-AMM-RAT-END-NULL
              TO P67-AMM-RAT-END-NULL
           ELSE
              MOVE (SF)-AMM-RAT-END
              TO P67-AMM-RAT-END
           END-IF
           IF (SF)-TS-CRE-RAT-FINANZ-NULL = HIGH-VALUES
              MOVE (SF)-TS-CRE-RAT-FINANZ-NULL
              TO P67-TS-CRE-RAT-FINANZ-NULL
           ELSE
              MOVE (SF)-TS-CRE-RAT-FINANZ
              TO P67-TS-CRE-RAT-FINANZ
           END-IF
           IF (SF)-IMP-FIN-REVOLVING-NULL = HIGH-VALUES
              MOVE (SF)-IMP-FIN-REVOLVING-NULL
              TO P67-IMP-FIN-REVOLVING-NULL
           ELSE
              MOVE (SF)-IMP-FIN-REVOLVING
              TO P67-IMP-FIN-REVOLVING
           END-IF
           IF (SF)-IMP-UTIL-C-REV-NULL = HIGH-VALUES
              MOVE (SF)-IMP-UTIL-C-REV-NULL
              TO P67-IMP-UTIL-C-REV-NULL
           ELSE
              MOVE (SF)-IMP-UTIL-C-REV
              TO P67-IMP-UTIL-C-REV
           END-IF
           IF (SF)-IMP-RAT-REVOLVING-NULL = HIGH-VALUES
              MOVE (SF)-IMP-RAT-REVOLVING-NULL
              TO P67-IMP-RAT-REVOLVING-NULL
           ELSE
              MOVE (SF)-IMP-RAT-REVOLVING
              TO P67-IMP-RAT-REVOLVING
           END-IF
           IF (SF)-DT-1O-UTLZ-C-REV-NULL = HIGH-VALUES
              MOVE (SF)-DT-1O-UTLZ-C-REV-NULL
              TO P67-DT-1O-UTLZ-C-REV-NULL
           ELSE
             IF (SF)-DT-1O-UTLZ-C-REV = ZERO
                MOVE HIGH-VALUES
                TO P67-DT-1O-UTLZ-C-REV-NULL
             ELSE
              MOVE (SF)-DT-1O-UTLZ-C-REV
              TO P67-DT-1O-UTLZ-C-REV
             END-IF
           END-IF
           IF (SF)-IMP-ASSTO-NULL = HIGH-VALUES
              MOVE (SF)-IMP-ASSTO-NULL
              TO P67-IMP-ASSTO-NULL
           ELSE
              MOVE (SF)-IMP-ASSTO
              TO P67-IMP-ASSTO
           END-IF
           IF (SF)-PRE-VERS-NULL = HIGH-VALUES
              MOVE (SF)-PRE-VERS-NULL
              TO P67-PRE-VERS-NULL
           ELSE
              MOVE (SF)-PRE-VERS
              TO P67-PRE-VERS
           END-IF
           IF (SF)-DT-SCAD-COP-NULL = HIGH-VALUES
              MOVE (SF)-DT-SCAD-COP-NULL
              TO P67-DT-SCAD-COP-NULL
           ELSE
             IF (SF)-DT-SCAD-COP = ZERO
                MOVE HIGH-VALUES
                TO P67-DT-SCAD-COP-NULL
             ELSE
              MOVE (SF)-DT-SCAD-COP
              TO P67-DT-SCAD-COP
             END-IF
           END-IF
           IF (SF)-GG-DEL-MM-SCAD-RAT-NULL = HIGH-VALUES
              MOVE (SF)-GG-DEL-MM-SCAD-RAT-NULL
              TO P67-GG-DEL-MM-SCAD-RAT-NULL
           ELSE
              MOVE (SF)-GG-DEL-MM-SCAD-RAT
              TO P67-GG-DEL-MM-SCAD-RAT
           END-IF
           MOVE (SF)-FL-PRE-FIN
              TO P67-FL-PRE-FIN
           IF (SF)-DT-SCAD-1A-RAT-NULL = HIGH-VALUES
              MOVE (SF)-DT-SCAD-1A-RAT-NULL
              TO P67-DT-SCAD-1A-RAT-NULL
           ELSE
             IF (SF)-DT-SCAD-1A-RAT = ZERO
                MOVE HIGH-VALUES
                TO P67-DT-SCAD-1A-RAT-NULL
             ELSE
              MOVE (SF)-DT-SCAD-1A-RAT
              TO P67-DT-SCAD-1A-RAT
             END-IF
           END-IF
           IF (SF)-DT-EROG-FINANZ-NULL = HIGH-VALUES
              MOVE (SF)-DT-EROG-FINANZ-NULL
              TO P67-DT-EROG-FINANZ-NULL
           ELSE
             IF (SF)-DT-EROG-FINANZ = ZERO
                MOVE HIGH-VALUES
                TO P67-DT-EROG-FINANZ-NULL
             ELSE
              MOVE (SF)-DT-EROG-FINANZ
              TO P67-DT-EROG-FINANZ
             END-IF
           END-IF
           IF (SF)-DT-STIPULA-FINANZ-NULL = HIGH-VALUES
              MOVE (SF)-DT-STIPULA-FINANZ-NULL
              TO P67-DT-STIPULA-FINANZ-NULL
           ELSE
             IF (SF)-DT-STIPULA-FINANZ = ZERO
                MOVE HIGH-VALUES
                TO P67-DT-STIPULA-FINANZ-NULL
             ELSE
              MOVE (SF)-DT-STIPULA-FINANZ
              TO P67-DT-STIPULA-FINANZ
             END-IF
           END-IF
           IF (SF)-MM-PREAMM-NULL = HIGH-VALUES
              MOVE (SF)-MM-PREAMM-NULL
              TO P67-MM-PREAMM-NULL
           ELSE
              MOVE (SF)-MM-PREAMM
              TO P67-MM-PREAMM
           END-IF
           IF (SF)-IMP-DEB-RES-NULL = HIGH-VALUES
              MOVE (SF)-IMP-DEB-RES-NULL
              TO P67-IMP-DEB-RES-NULL
           ELSE
              MOVE (SF)-IMP-DEB-RES
              TO P67-IMP-DEB-RES
           END-IF
           IF (SF)-IMP-RAT-FINANZ-NULL = HIGH-VALUES
              MOVE (SF)-IMP-RAT-FINANZ-NULL
              TO P67-IMP-RAT-FINANZ-NULL
           ELSE
              MOVE (SF)-IMP-RAT-FINANZ
              TO P67-IMP-RAT-FINANZ
           END-IF
           IF (SF)-IMP-CANONE-ANTIC-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CANONE-ANTIC-NULL
              TO P67-IMP-CANONE-ANTIC-NULL
           ELSE
              MOVE (SF)-IMP-CANONE-ANTIC
              TO P67-IMP-CANONE-ANTIC
           END-IF
           IF (SF)-PER-RAT-FINANZ-NULL = HIGH-VALUES
              MOVE (SF)-PER-RAT-FINANZ-NULL
              TO P67-PER-RAT-FINANZ-NULL
           ELSE
              MOVE (SF)-PER-RAT-FINANZ
              TO P67-PER-RAT-FINANZ
           END-IF
           IF (SF)-TP-MOD-PAG-RAT-NULL = HIGH-VALUES
              MOVE (SF)-TP-MOD-PAG-RAT-NULL
              TO P67-TP-MOD-PAG-RAT-NULL
           ELSE
              MOVE (SF)-TP-MOD-PAG-RAT
              TO P67-TP-MOD-PAG-RAT
           END-IF
           IF (SF)-TP-FINANZ-ER-NULL = HIGH-VALUES
              MOVE (SF)-TP-FINANZ-ER-NULL
              TO P67-TP-FINANZ-ER-NULL
           ELSE
              MOVE (SF)-TP-FINANZ-ER
              TO P67-TP-FINANZ-ER
           END-IF
           IF (SF)-CAT-FINANZ-ER-NULL = HIGH-VALUES
              MOVE (SF)-CAT-FINANZ-ER-NULL
              TO P67-CAT-FINANZ-ER-NULL
           ELSE
              MOVE (SF)-CAT-FINANZ-ER
              TO P67-CAT-FINANZ-ER
           END-IF
           IF (SF)-VAL-RISC-END-LEAS-NULL = HIGH-VALUES
              MOVE (SF)-VAL-RISC-END-LEAS-NULL
              TO P67-VAL-RISC-END-LEAS-NULL
           ELSE
              MOVE (SF)-VAL-RISC-END-LEAS
              TO P67-VAL-RISC-END-LEAS
           END-IF
           IF (SF)-DT-EST-FINANZ-NULL = HIGH-VALUES
              MOVE (SF)-DT-EST-FINANZ-NULL
              TO P67-DT-EST-FINANZ-NULL
           ELSE
             IF (SF)-DT-EST-FINANZ = ZERO
                MOVE HIGH-VALUES
                TO P67-DT-EST-FINANZ-NULL
             ELSE
              MOVE (SF)-DT-EST-FINANZ
              TO P67-DT-EST-FINANZ
             END-IF
           END-IF
           IF (SF)-DT-MAN-COP-NULL = HIGH-VALUES
              MOVE (SF)-DT-MAN-COP-NULL
              TO P67-DT-MAN-COP-NULL
           ELSE
             IF (SF)-DT-MAN-COP = ZERO
                MOVE HIGH-VALUES
                TO P67-DT-MAN-COP-NULL
             ELSE
              MOVE (SF)-DT-MAN-COP
              TO P67-DT-MAN-COP
             END-IF
           END-IF
           IF (SF)-NUM-FINANZ-NULL = HIGH-VALUES
              MOVE (SF)-NUM-FINANZ-NULL
              TO P67-NUM-FINANZ-NULL
           ELSE
              MOVE (SF)-NUM-FINANZ
              TO P67-NUM-FINANZ
           END-IF
           IF (SF)-TP-MOD-ACQS-NULL = HIGH-VALUES
              MOVE (SF)-TP-MOD-ACQS-NULL
              TO P67-TP-MOD-ACQS-NULL
           ELSE
              MOVE (SF)-TP-MOD-ACQS
              TO P67-TP-MOD-ACQS
           END-IF.
       VAL-DCLGEN-P67-EX.
           EXIT.

       01 PREV.
         05 L27-ID-PREV PIC S9(9)V     COMP-3.
         05 L27-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 L27-STAT-PREV PIC X(2).
         05 L27-ID-OGG PIC S9(9)V     COMP-3.
         05 L27-ID-OGG-NULL REDEFINES
            L27-ID-OGG   PIC X(5).
         05 L27-TP-OGG PIC X(2).
         05 L27-TP-OGG-NULL REDEFINES
            L27-TP-OGG   PIC X(2).
         05 L27-IB-OGG PIC X(40).
         05 L27-DS-OPER-SQL PIC X(1).
         05 L27-DS-VER PIC S9(9)V     COMP-3.
         05 L27-DS-TS-CPTZ PIC S9(18)V     COMP-3.
         05 L27-DS-UTENTE PIC X(20).
         05 L27-DS-STATO-ELAB PIC X(1).


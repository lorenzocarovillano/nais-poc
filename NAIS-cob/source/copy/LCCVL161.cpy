      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA AMMB_FUNZ_BLOCCO
      *   ALIAS L16
      *   ULTIMO AGG. 07 GEN 2016
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-DATI.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-CAN PIC S9(5)     COMP-3.
             07 (SF)-COD-BLOCCO PIC X(5).
             07 (SF)-TP-MOVI PIC S9(5)     COMP-3.
             07 (SF)-GRAV-FUNZ-BLOCCO PIC X(1).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-TP-MOVI-RIFTO PIC S9(5)     COMP-3.
             07 (SF)-TP-MOVI-RIFTO-NULL REDEFINES
                (SF)-TP-MOVI-RIFTO   PIC X(3).

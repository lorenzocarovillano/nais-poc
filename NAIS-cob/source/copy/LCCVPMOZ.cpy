      *----------------------------------------------------------------*
      * DIMENSIONAMENTO OCCURS PER LA TAB. PARAM_MOVI               *
      * COPY RIPORTANTE IL NUMERO DI OCCURS DA UTILIZZARE              *
      * PER INIZIALIZZA TOT DELLA TABELLA                              *
      *----------------------------------------------------------------*
       01 WK-PMO-MAX.
          03 WK-PMO-MAX-A                 PIC 9(04) VALUE 50.
          03 WK-PMO-MAX-B                 PIC 9(04) VALUE 100.

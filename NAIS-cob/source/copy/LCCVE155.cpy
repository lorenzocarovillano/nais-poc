
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVE155
      *   ULTIMO AGG. 04 MAR 2020
      *------------------------------------------------------------

       VAL-DCLGEN-E15.
           MOVE (SF)-ID-RAPP-ANA(IX-TAB-E15)
              TO E15-ID-RAPP-ANA
           IF (SF)-ID-RAPP-ANA-COLLG-NULL(IX-TAB-E15) = HIGH-VALUES
              MOVE (SF)-ID-RAPP-ANA-COLLG-NULL(IX-TAB-E15)
              TO E15-ID-RAPP-ANA-COLLG-NULL
           ELSE
              MOVE (SF)-ID-RAPP-ANA-COLLG(IX-TAB-E15)
              TO E15-ID-RAPP-ANA-COLLG
           END-IF
           MOVE (SF)-ID-OGG(IX-TAB-E15)
              TO E15-ID-OGG
           MOVE (SF)-TP-OGG(IX-TAB-E15)
              TO E15-TP-OGG
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-E15) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-E15)
              TO E15-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-E15)
              TO E15-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-E15) NOT NUMERIC
              MOVE 0 TO E15-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-E15)
              TO E15-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-E15) NOT NUMERIC
              MOVE 0 TO E15-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-E15)
              TO E15-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-E15)
              TO E15-COD-COMP-ANIA
           IF (SF)-COD-SOGG-NULL(IX-TAB-E15) = HIGH-VALUES
              MOVE (SF)-COD-SOGG-NULL(IX-TAB-E15)
              TO E15-COD-SOGG-NULL
           ELSE
              MOVE (SF)-COD-SOGG(IX-TAB-E15)
              TO E15-COD-SOGG
           END-IF
           MOVE (SF)-TP-RAPP-ANA(IX-TAB-E15)
              TO E15-TP-RAPP-ANA
           IF (SF)-DS-RIGA(IX-TAB-E15) NOT NUMERIC
              MOVE 0 TO E15-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-E15)
              TO E15-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-E15)
              TO E15-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-E15) NOT NUMERIC
              MOVE 0 TO E15-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-E15)
              TO E15-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-E15) NOT NUMERIC
              MOVE 0 TO E15-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-E15)
              TO E15-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-E15) NOT NUMERIC
              MOVE 0 TO E15-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-E15)
              TO E15-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-E15)
              TO E15-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-E15)
              TO E15-DS-STATO-ELAB
           IF (SF)-ID-SEGMENTAZ-CLI-NULL(IX-TAB-E15) = HIGH-VALUES
              MOVE (SF)-ID-SEGMENTAZ-CLI-NULL(IX-TAB-E15)
              TO E15-ID-SEGMENTAZ-CLI-NULL
           ELSE
              MOVE (SF)-ID-SEGMENTAZ-CLI(IX-TAB-E15)
              TO E15-ID-SEGMENTAZ-CLI
           END-IF
           IF (SF)-FL-COINC-TIT-EFF-NULL(IX-TAB-E15) = HIGH-VALUES
              MOVE (SF)-FL-COINC-TIT-EFF-NULL(IX-TAB-E15)
              TO E15-FL-COINC-TIT-EFF-NULL
           ELSE
              MOVE (SF)-FL-COINC-TIT-EFF(IX-TAB-E15)
              TO E15-FL-COINC-TIT-EFF
           END-IF
           IF (SF)-FL-PERS-ESP-POL-NULL(IX-TAB-E15) = HIGH-VALUES
              MOVE (SF)-FL-PERS-ESP-POL-NULL(IX-TAB-E15)
              TO E15-FL-PERS-ESP-POL-NULL
           ELSE
              MOVE (SF)-FL-PERS-ESP-POL(IX-TAB-E15)
              TO E15-FL-PERS-ESP-POL
           END-IF
           MOVE (SF)-DESC-PERS-ESP-POL(IX-TAB-E15)
              TO E15-DESC-PERS-ESP-POL
           IF (SF)-TP-LEG-CNTR-NULL(IX-TAB-E15) = HIGH-VALUES
              MOVE (SF)-TP-LEG-CNTR-NULL(IX-TAB-E15)
              TO E15-TP-LEG-CNTR-NULL
           ELSE
              MOVE (SF)-TP-LEG-CNTR(IX-TAB-E15)
              TO E15-TP-LEG-CNTR
           END-IF
           MOVE (SF)-DESC-LEG-CNTR(IX-TAB-E15)
              TO E15-DESC-LEG-CNTR
           IF (SF)-TP-LEG-PERC-BNFICR-NULL(IX-TAB-E15) = HIGH-VALUES
              MOVE (SF)-TP-LEG-PERC-BNFICR-NULL(IX-TAB-E15)
              TO E15-TP-LEG-PERC-BNFICR-NULL
           ELSE
              MOVE (SF)-TP-LEG-PERC-BNFICR(IX-TAB-E15)
              TO E15-TP-LEG-PERC-BNFICR
           END-IF
           MOVE (SF)-D-LEG-PERC-BNFICR(IX-TAB-E15)
              TO E15-D-LEG-PERC-BNFICR
           IF (SF)-TP-CNT-CORR-NULL(IX-TAB-E15) = HIGH-VALUES
              MOVE (SF)-TP-CNT-CORR-NULL(IX-TAB-E15)
              TO E15-TP-CNT-CORR-NULL
           ELSE
              MOVE (SF)-TP-CNT-CORR(IX-TAB-E15)
              TO E15-TP-CNT-CORR
           END-IF
           IF (SF)-TP-COINC-PIC-PAC-NULL(IX-TAB-E15) = HIGH-VALUES
              MOVE (SF)-TP-COINC-PIC-PAC-NULL(IX-TAB-E15)
              TO E15-TP-COINC-PIC-PAC-NULL
           ELSE
              MOVE (SF)-TP-COINC-PIC-PAC(IX-TAB-E15)
              TO E15-TP-COINC-PIC-PAC
           END-IF.
       VAL-DCLGEN-E15-EX.
           EXIT.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA RAPP_ANA
      *   ALIAS RAN
      *   ULTIMO AGG. 24 GEN 2014
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-RAPP-ANA PIC S9(9)     COMP-3.
             07 (SF)-ID-RAPP-ANA-COLLG PIC S9(9)     COMP-3.
             07 (SF)-ID-RAPP-ANA-COLLG-NULL REDEFINES
                (SF)-ID-RAPP-ANA-COLLG   PIC X(5).
             07 (SF)-ID-OGG PIC S9(9)     COMP-3.
             07 (SF)-TP-OGG PIC X(2).
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-SOGG PIC X(20).
             07 (SF)-COD-SOGG-NULL REDEFINES
                (SF)-COD-SOGG   PIC X(20).
             07 (SF)-TP-RAPP-ANA PIC X(2).
             07 (SF)-TP-PERS PIC X(1).
             07 (SF)-TP-PERS-NULL REDEFINES
                (SF)-TP-PERS   PIC X(1).
             07 (SF)-SEX PIC X(1).
             07 (SF)-SEX-NULL REDEFINES
                (SF)-SEX   PIC X(1).
             07 (SF)-DT-NASC   PIC S9(8) COMP-3.
             07 (SF)-DT-NASC-NULL REDEFINES
                (SF)-DT-NASC   PIC X(5).
             07 (SF)-FL-ESTAS PIC X(1).
             07 (SF)-FL-ESTAS-NULL REDEFINES
                (SF)-FL-ESTAS   PIC X(1).
             07 (SF)-INDIR-1 PIC X(20).
             07 (SF)-INDIR-1-NULL REDEFINES
                (SF)-INDIR-1   PIC X(20).
             07 (SF)-INDIR-2 PIC X(20).
             07 (SF)-INDIR-2-NULL REDEFINES
                (SF)-INDIR-2   PIC X(20).
             07 (SF)-INDIR-3 PIC X(20).
             07 (SF)-INDIR-3-NULL REDEFINES
                (SF)-INDIR-3   PIC X(20).
             07 (SF)-TP-UTLZ-INDIR-1 PIC X(2).
             07 (SF)-TP-UTLZ-INDIR-1-NULL REDEFINES
                (SF)-TP-UTLZ-INDIR-1   PIC X(2).
             07 (SF)-TP-UTLZ-INDIR-2 PIC X(2).
             07 (SF)-TP-UTLZ-INDIR-2-NULL REDEFINES
                (SF)-TP-UTLZ-INDIR-2   PIC X(2).
             07 (SF)-TP-UTLZ-INDIR-3 PIC X(2).
             07 (SF)-TP-UTLZ-INDIR-3-NULL REDEFINES
                (SF)-TP-UTLZ-INDIR-3   PIC X(2).
             07 (SF)-ESTR-CNT-CORR-ACCR PIC X(20).
             07 (SF)-ESTR-CNT-CORR-ACCR-NULL REDEFINES
                (SF)-ESTR-CNT-CORR-ACCR   PIC X(20).
             07 (SF)-ESTR-CNT-CORR-ADD PIC X(20).
             07 (SF)-ESTR-CNT-CORR-ADD-NULL REDEFINES
                (SF)-ESTR-CNT-CORR-ADD   PIC X(20).
             07 (SF)-ESTR-DOCTO PIC X(20).
             07 (SF)-ESTR-DOCTO-NULL REDEFINES
                (SF)-ESTR-DOCTO   PIC X(20).
             07 (SF)-PC-NEL-RAPP PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-NEL-RAPP-NULL REDEFINES
                (SF)-PC-NEL-RAPP   PIC X(4).
             07 (SF)-TP-MEZ-PAG-ADD PIC X(2).
             07 (SF)-TP-MEZ-PAG-ADD-NULL REDEFINES
                (SF)-TP-MEZ-PAG-ADD   PIC X(2).
             07 (SF)-TP-MEZ-PAG-ACCR PIC X(2).
             07 (SF)-TP-MEZ-PAG-ACCR-NULL REDEFINES
                (SF)-TP-MEZ-PAG-ACCR   PIC X(2).
             07 (SF)-COD-MATR PIC X(20).
             07 (SF)-COD-MATR-NULL REDEFINES
                (SF)-COD-MATR   PIC X(20).
             07 (SF)-TP-ADEGZ PIC X(2).
             07 (SF)-TP-ADEGZ-NULL REDEFINES
                (SF)-TP-ADEGZ   PIC X(2).
             07 (SF)-FL-TST-RSH PIC X(1).
             07 (SF)-FL-TST-RSH-NULL REDEFINES
                (SF)-FL-TST-RSH   PIC X(1).
             07 (SF)-COD-AZ PIC X(30).
             07 (SF)-COD-AZ-NULL REDEFINES
                (SF)-COD-AZ   PIC X(30).
             07 (SF)-IND-PRINC PIC X(2).
             07 (SF)-IND-PRINC-NULL REDEFINES
                (SF)-IND-PRINC   PIC X(2).
             07 (SF)-DT-DELIBERA-CDA   PIC S9(8) COMP-3.
             07 (SF)-DT-DELIBERA-CDA-NULL REDEFINES
                (SF)-DT-DELIBERA-CDA   PIC X(5).
             07 (SF)-DLG-AL-RISC PIC X(1).
             07 (SF)-DLG-AL-RISC-NULL REDEFINES
                (SF)-DLG-AL-RISC   PIC X(1).
             07 (SF)-LEGALE-RAPPR-PRINC PIC X(1).
             07 (SF)-LEGALE-RAPPR-PRINC-NULL REDEFINES
                (SF)-LEGALE-RAPPR-PRINC   PIC X(1).
             07 (SF)-TP-LEGALE-RAPPR PIC X(2).
             07 (SF)-TP-LEGALE-RAPPR-NULL REDEFINES
                (SF)-TP-LEGALE-RAPPR   PIC X(2).
             07 (SF)-TP-IND-PRINC PIC X(2).
             07 (SF)-TP-IND-PRINC-NULL REDEFINES
                (SF)-TP-IND-PRINC   PIC X(2).
             07 (SF)-TP-STAT-RID PIC X(2).
             07 (SF)-TP-STAT-RID-NULL REDEFINES
                (SF)-TP-STAT-RID   PIC X(2).
             07 (SF)-NOME-INT-RID PIC X(100).
             07 (SF)-COGN-INT-RID PIC X(100).
             07 (SF)-COGN-INT-TRATT PIC X(100).
             07 (SF)-NOME-INT-TRATT PIC X(100).
             07 (SF)-CF-INT-RID PIC X(16).
             07 (SF)-CF-INT-RID-NULL REDEFINES
                (SF)-CF-INT-RID   PIC X(16).
             07 (SF)-FL-COINC-DIP-CNTR PIC X(1).
             07 (SF)-FL-COINC-DIP-CNTR-NULL REDEFINES
                (SF)-FL-COINC-DIP-CNTR   PIC X(1).
             07 (SF)-FL-COINC-INT-CNTR PIC X(1).
             07 (SF)-FL-COINC-INT-CNTR-NULL REDEFINES
                (SF)-FL-COINC-INT-CNTR   PIC X(1).
             07 (SF)-DT-DECES   PIC S9(8) COMP-3.
             07 (SF)-DT-DECES-NULL REDEFINES
                (SF)-DT-DECES   PIC X(5).
             07 (SF)-FL-FUMATORE PIC X(1).
             07 (SF)-FL-FUMATORE-NULL REDEFINES
                (SF)-FL-FUMATORE   PIC X(1).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-FL-LAV-DIP PIC X(1).
             07 (SF)-FL-LAV-DIP-NULL REDEFINES
                (SF)-FL-LAV-DIP   PIC X(1).
             07 (SF)-TP-VARZ-PAGAT PIC X(2).
             07 (SF)-TP-VARZ-PAGAT-NULL REDEFINES
                (SF)-TP-VARZ-PAGAT   PIC X(2).
             07 (SF)-COD-RID PIC X(11).
             07 (SF)-COD-RID-NULL REDEFINES
                (SF)-COD-RID   PIC X(11).
             07 (SF)-TP-CAUS-RID PIC X(2).
             07 (SF)-TP-CAUS-RID-NULL REDEFINES
                (SF)-TP-CAUS-RID   PIC X(2).
             07 (SF)-IND-MASSA-CORP PIC X(2).
             07 (SF)-IND-MASSA-CORP-NULL REDEFINES
                (SF)-IND-MASSA-CORP   PIC X(2).
             07 (SF)-CAT-RSH-PROF PIC X(2).
             07 (SF)-CAT-RSH-PROF-NULL REDEFINES
                (SF)-CAT-RSH-PROF   PIC X(2).

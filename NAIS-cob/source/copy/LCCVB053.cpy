
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVB053
      *   ULTIMO AGG. 14 MAR 2011
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-B05.
           MOVE B05-ID-BILA-VAR-CALC-T
             TO (SF)-ID-PTF
           MOVE B05-ID-BILA-VAR-CALC-T
             TO (SF)-ID-BILA-VAR-CALC-T
           MOVE B05-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE B05-ID-BILA-TRCH-ESTR
             TO (SF)-ID-BILA-TRCH-ESTR
           MOVE B05-ID-RICH-ESTRAZ-MAS
             TO (SF)-ID-RICH-ESTRAZ-MAS
           IF B05-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
              MOVE B05-ID-RICH-ESTRAZ-AGG-NULL
                TO (SF)-ID-RICH-ESTRAZ-AGG-NULL
           ELSE
              MOVE B05-ID-RICH-ESTRAZ-AGG
                TO (SF)-ID-RICH-ESTRAZ-AGG
           END-IF
           IF B05-DT-RIS-NULL = HIGH-VALUES
              MOVE B05-DT-RIS-NULL
                TO (SF)-DT-RIS-NULL
           ELSE
              MOVE B05-DT-RIS
                TO (SF)-DT-RIS
           END-IF
           MOVE B05-ID-POLI
             TO (SF)-ID-POLI
           MOVE B05-ID-ADES
             TO (SF)-ID-ADES
           MOVE B05-ID-TRCH-DI-GAR
             TO (SF)-ID-TRCH-DI-GAR
           IF B05-PROG-SCHEDA-VALOR-NULL = HIGH-VALUES
              MOVE B05-PROG-SCHEDA-VALOR-NULL
                TO (SF)-PROG-SCHEDA-VALOR-NULL
           ELSE
              MOVE B05-PROG-SCHEDA-VALOR
                TO (SF)-PROG-SCHEDA-VALOR
           END-IF
           MOVE B05-DT-INI-VLDT-TARI
             TO (SF)-DT-INI-VLDT-TARI
           MOVE B05-TP-RGM-FISC
             TO (SF)-TP-RGM-FISC
           MOVE B05-DT-INI-VLDT-PROD
             TO (SF)-DT-INI-VLDT-PROD
           MOVE B05-DT-DECOR-TRCH
             TO (SF)-DT-DECOR-TRCH
           MOVE B05-COD-VAR
             TO (SF)-COD-VAR
           MOVE B05-TP-D
             TO (SF)-TP-D
           IF B05-VAL-IMP-NULL = HIGH-VALUES
              MOVE B05-VAL-IMP-NULL
                TO (SF)-VAL-IMP-NULL
           ELSE
              MOVE B05-VAL-IMP
                TO (SF)-VAL-IMP
           END-IF
           IF B05-VAL-PC-NULL = HIGH-VALUES
              MOVE B05-VAL-PC-NULL
                TO (SF)-VAL-PC-NULL
           ELSE
              MOVE B05-VAL-PC
                TO (SF)-VAL-PC
           END-IF
           IF B05-VAL-STRINGA-NULL = HIGH-VALUES
              MOVE B05-VAL-STRINGA-NULL
                TO (SF)-VAL-STRINGA-NULL
           ELSE
              MOVE B05-VAL-STRINGA
                TO (SF)-VAL-STRINGA
           END-IF
           MOVE B05-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE B05-DS-VER
             TO (SF)-DS-VER
           MOVE B05-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE B05-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE B05-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           MOVE B05-AREA-D-VALOR-VAR-VCHAR
             TO (SF)-AREA-D-VALOR-VAR-VCHAR.
       VALORIZZA-OUTPUT-B05-EX.
           EXIT.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA OGG_BLOCCO
      *   ALIAS L11
      *   ULTIMO AGG. 02 SET 2008
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-OGG-BLOCCO PIC S9(9)     COMP-3.
             07 (SF)-ID-OGG-1RIO PIC S9(9)     COMP-3.
             07 (SF)-ID-OGG-1RIO-NULL REDEFINES
                (SF)-ID-OGG-1RIO   PIC X(5).
             07 (SF)-TP-OGG-1RIO PIC X(2).
             07 (SF)-TP-OGG-1RIO-NULL REDEFINES
                (SF)-TP-OGG-1RIO   PIC X(2).
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-TP-MOVI PIC S9(5)     COMP-3.
             07 (SF)-TP-OGG PIC X(2).
             07 (SF)-ID-OGG PIC S9(9)     COMP-3.
             07 (SF)-DT-EFF   PIC S9(8) COMP-3.
             07 (SF)-TP-STAT-BLOCCO PIC X(2).
             07 (SF)-COD-BLOCCO PIC X(5).
             07 (SF)-ID-RICH PIC S9(9)     COMP-3.
             07 (SF)-ID-RICH-NULL REDEFINES
                (SF)-ID-RICH   PIC X(5).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).

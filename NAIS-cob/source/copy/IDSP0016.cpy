      ******************************************************************
      * INIZIO STATEMENTS DA UTILIZZARE PRETTAMENTE CON IDSV0016
      ******************************************************************
       CNTL-FORMALE-DATA.

           MOVE 'CNTL-CAMPI-DATA'           TO WK-LABEL.

           IF IDSV0016-AAAA NOT NUMERIC OR
              IDSV0016-AAAA = 0

              MOVE HIGH-VALUES  TO WK-LOG-ERRORE
              MOVE 4            TO WK-LOR-ID-GRAVITA-ERRORE
              MOVE WK-LABEL     TO WK-LOR-LABEL-ERR
              MOVE WK-PGM       TO WK-LOR-COD-SERVIZIO-BE

              STRING 'ANNO NON VALIDO'
                      DELIMITED BY SIZE INTO
                      IDSV0016-DESC-ERRORE
              END-STRING

              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
           ELSE
              PERFORM CNTL-ANNO-BISESTILE
                      THRU CNTL-ANNO-BISESTILE-EX
           END-IF

           IF WK-ERRORE-NO
              IF IDSV0016-MM NOT NUMERIC OR
                 IDSV0016-MM < 01           OR
                 IDSV0016-MM > 12

                 STRING 'MESE NON VALIDO'
                         DELIMITED BY SIZE INTO
                         IDSV0016-DESC-ERRORE
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX

              ELSE
                 IF IDSV0016-ANNO-BISESTILE
                    ADD 1          TO IDSV0016-STR-GG-ANNUAL
                                     (IDSV0016-CONST-FEBBRAIO)
                 END-IF
              END-IF

              IF WK-ERRORE-NO
                 IF IDSV0016-GG NOT NUMERIC OR
                    IDSV0016-GG < 0         OR
                    IDSV0016-GG > IDSV0016-STR-GG-ANNUAL(IDSV0016-MM)

                    STRING 'GIORNO NON VALIDO'
                            DELIMITED BY SIZE INTO
                            IDSV0016-DESC-ERRORE
                    END-STRING

                    PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX

                 END-IF
              END-IF
           END-IF.


       CNTL-FORMALE-DATA-EX.
           EXIT.

      ******************************************************************
      *
      ******************************************************************
       CNTL-ANNO-BISESTILE.

           MOVE 'CNTL-ANNO-BISESTILE'      TO WK-LABEL.

           SET IDSV0016-ANNO-STANDARD           TO TRUE

           DIVIDE IDSV0016-AAAA        BY 4
                                       GIVING IDSV0016-RISULTATO
                                       REMAINDER IDSV0016-RESTO

           IF IDSV0016-RESTO = 0

              DIVIDE IDSV0016-AAAA     BY 100
                                       GIVING IDSV0016-RISULTATO
                                       REMAINDER IDSV0016-RESTO

              IF IDSV0016-RESTO NOT = 0

                 SET IDSV0016-ANNO-BISESTILE      TO TRUE

              ELSE

                 DIVIDE IDSV0016-AAAA  BY 400
                                       GIVING IDSV0016-RISULTATO
                                       REMAINDER IDSV0016-RESTO

                 IF IDSV0016-RESTO = 0
                    SET IDSV0016-ANNO-BISESTILE   TO TRUE
                 END-IF

              END-IF

           END-IF.

       CNTL-ANNO-BISESTILE-EX.
           EXIT.

      ******************************************************************
      *  FINE STATEMENTS DA UTILIZZARE PRETTAMENTE CON IDSV0016
      ******************************************************************

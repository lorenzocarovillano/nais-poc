
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVGRL5
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-GRL.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-GRL) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-GRL)
              TO GRL-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-GRL)
              TO GRL-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-GRL) NOT NUMERIC
              MOVE 0 TO GRL-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-GRL)
              TO GRL-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-GRL) NOT NUMERIC
              MOVE 0 TO GRL-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-GRL)
              TO GRL-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-GRL)
              TO GRL-COD-COMP-ANIA
           IF (SF)-DT-SIN-1O-ASSTO-NULL(IX-TAB-GRL) = HIGH-VALUES
              MOVE (SF)-DT-SIN-1O-ASSTO-NULL(IX-TAB-GRL)
              TO GRL-DT-SIN-1O-ASSTO-NULL
           ELSE
             IF (SF)-DT-SIN-1O-ASSTO(IX-TAB-GRL) = ZERO
                MOVE HIGH-VALUES
                TO GRL-DT-SIN-1O-ASSTO-NULL
             ELSE
              MOVE (SF)-DT-SIN-1O-ASSTO(IX-TAB-GRL)
              TO GRL-DT-SIN-1O-ASSTO
             END-IF
           END-IF
           IF (SF)-CAU-SIN-1O-ASSTO-NULL(IX-TAB-GRL) = HIGH-VALUES
              MOVE (SF)-CAU-SIN-1O-ASSTO-NULL(IX-TAB-GRL)
              TO GRL-CAU-SIN-1O-ASSTO-NULL
           ELSE
              MOVE (SF)-CAU-SIN-1O-ASSTO(IX-TAB-GRL)
              TO GRL-CAU-SIN-1O-ASSTO
           END-IF
           IF (SF)-TP-SIN-1O-ASSTO-NULL(IX-TAB-GRL) = HIGH-VALUES
              MOVE (SF)-TP-SIN-1O-ASSTO-NULL(IX-TAB-GRL)
              TO GRL-TP-SIN-1O-ASSTO-NULL
           ELSE
              MOVE (SF)-TP-SIN-1O-ASSTO(IX-TAB-GRL)
              TO GRL-TP-SIN-1O-ASSTO
           END-IF
           IF (SF)-DT-SIN-2O-ASSTO-NULL(IX-TAB-GRL) = HIGH-VALUES
              MOVE (SF)-DT-SIN-2O-ASSTO-NULL(IX-TAB-GRL)
              TO GRL-DT-SIN-2O-ASSTO-NULL
           ELSE
             IF (SF)-DT-SIN-2O-ASSTO(IX-TAB-GRL) = ZERO
                MOVE HIGH-VALUES
                TO GRL-DT-SIN-2O-ASSTO-NULL
             ELSE
              MOVE (SF)-DT-SIN-2O-ASSTO(IX-TAB-GRL)
              TO GRL-DT-SIN-2O-ASSTO
             END-IF
           END-IF
           IF (SF)-CAU-SIN-2O-ASSTO-NULL(IX-TAB-GRL) = HIGH-VALUES
              MOVE (SF)-CAU-SIN-2O-ASSTO-NULL(IX-TAB-GRL)
              TO GRL-CAU-SIN-2O-ASSTO-NULL
           ELSE
              MOVE (SF)-CAU-SIN-2O-ASSTO(IX-TAB-GRL)
              TO GRL-CAU-SIN-2O-ASSTO
           END-IF
           IF (SF)-TP-SIN-2O-ASSTO-NULL(IX-TAB-GRL) = HIGH-VALUES
              MOVE (SF)-TP-SIN-2O-ASSTO-NULL(IX-TAB-GRL)
              TO GRL-TP-SIN-2O-ASSTO-NULL
           ELSE
              MOVE (SF)-TP-SIN-2O-ASSTO(IX-TAB-GRL)
              TO GRL-TP-SIN-2O-ASSTO
           END-IF
           IF (SF)-DT-SIN-3O-ASSTO-NULL(IX-TAB-GRL) = HIGH-VALUES
              MOVE (SF)-DT-SIN-3O-ASSTO-NULL(IX-TAB-GRL)
              TO GRL-DT-SIN-3O-ASSTO-NULL
           ELSE
             IF (SF)-DT-SIN-3O-ASSTO(IX-TAB-GRL) = ZERO
                MOVE HIGH-VALUES
                TO GRL-DT-SIN-3O-ASSTO-NULL
             ELSE
              MOVE (SF)-DT-SIN-3O-ASSTO(IX-TAB-GRL)
              TO GRL-DT-SIN-3O-ASSTO
             END-IF
           END-IF
           IF (SF)-CAU-SIN-3O-ASSTO-NULL(IX-TAB-GRL) = HIGH-VALUES
              MOVE (SF)-CAU-SIN-3O-ASSTO-NULL(IX-TAB-GRL)
              TO GRL-CAU-SIN-3O-ASSTO-NULL
           ELSE
              MOVE (SF)-CAU-SIN-3O-ASSTO(IX-TAB-GRL)
              TO GRL-CAU-SIN-3O-ASSTO
           END-IF
           IF (SF)-TP-SIN-3O-ASSTO-NULL(IX-TAB-GRL) = HIGH-VALUES
              MOVE (SF)-TP-SIN-3O-ASSTO-NULL(IX-TAB-GRL)
              TO GRL-TP-SIN-3O-ASSTO-NULL
           ELSE
              MOVE (SF)-TP-SIN-3O-ASSTO(IX-TAB-GRL)
              TO GRL-TP-SIN-3O-ASSTO
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-GRL) NOT NUMERIC
              MOVE 0 TO GRL-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-GRL)
              TO GRL-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-GRL)
              TO GRL-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-GRL) NOT NUMERIC
              MOVE 0 TO GRL-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-GRL)
              TO GRL-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-GRL) NOT NUMERIC
              MOVE 0 TO GRL-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-GRL)
              TO GRL-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-GRL) NOT NUMERIC
              MOVE 0 TO GRL-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-GRL)
              TO GRL-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-GRL)
              TO GRL-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-GRL)
              TO GRL-DS-STATO-ELAB.
       VAL-DCLGEN-GRL-EX.
           EXIT.

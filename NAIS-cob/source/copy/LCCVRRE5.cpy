
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVRRE5
      *   ULTIMO AGG. 01 OTT 2014
      *------------------------------------------------------------

       VAL-DCLGEN-RRE.
           MOVE (SF)-TP-OGG(IX-TAB-RRE)
              TO RRE-TP-OGG
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RRE) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RRE)
              TO RRE-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-RRE)
              TO RRE-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-RRE) NOT NUMERIC
              MOVE 0 TO RRE-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-RRE)
              TO RRE-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-RRE) NOT NUMERIC
              MOVE 0 TO RRE-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-RRE)
              TO RRE-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-RRE)
              TO RRE-COD-COMP-ANIA
           MOVE (SF)-TP-RETE(IX-TAB-RRE)
              TO RRE-TP-RETE
           IF (SF)-TP-ACQS-CNTRT-NULL(IX-TAB-RRE) = HIGH-VALUES
              MOVE (SF)-TP-ACQS-CNTRT-NULL(IX-TAB-RRE)
              TO RRE-TP-ACQS-CNTRT-NULL
           ELSE
              MOVE (SF)-TP-ACQS-CNTRT(IX-TAB-RRE)
              TO RRE-TP-ACQS-CNTRT
           END-IF
           IF (SF)-COD-ACQS-CNTRT-NULL(IX-TAB-RRE) = HIGH-VALUES
              MOVE (SF)-COD-ACQS-CNTRT-NULL(IX-TAB-RRE)
              TO RRE-COD-ACQS-CNTRT-NULL
           ELSE
              MOVE (SF)-COD-ACQS-CNTRT(IX-TAB-RRE)
              TO RRE-COD-ACQS-CNTRT
           END-IF
           IF (SF)-COD-PNT-RETE-INI-NULL(IX-TAB-RRE) = HIGH-VALUES
              MOVE (SF)-COD-PNT-RETE-INI-NULL(IX-TAB-RRE)
              TO RRE-COD-PNT-RETE-INI-NULL
           ELSE
              MOVE (SF)-COD-PNT-RETE-INI(IX-TAB-RRE)
              TO RRE-COD-PNT-RETE-INI
           END-IF
           IF (SF)-COD-PNT-RETE-END-NULL(IX-TAB-RRE) = HIGH-VALUES
              MOVE (SF)-COD-PNT-RETE-END-NULL(IX-TAB-RRE)
              TO RRE-COD-PNT-RETE-END-NULL
           ELSE
              MOVE (SF)-COD-PNT-RETE-END(IX-TAB-RRE)
              TO RRE-COD-PNT-RETE-END
           END-IF
           IF (SF)-FL-PNT-RETE-1RIO-NULL(IX-TAB-RRE) = HIGH-VALUES
              MOVE (SF)-FL-PNT-RETE-1RIO-NULL(IX-TAB-RRE)
              TO RRE-FL-PNT-RETE-1RIO-NULL
           ELSE
              MOVE (SF)-FL-PNT-RETE-1RIO(IX-TAB-RRE)
              TO RRE-FL-PNT-RETE-1RIO
           END-IF
           IF (SF)-COD-CAN-NULL(IX-TAB-RRE) = HIGH-VALUES
              MOVE (SF)-COD-CAN-NULL(IX-TAB-RRE)
              TO RRE-COD-CAN-NULL
           ELSE
              MOVE (SF)-COD-CAN(IX-TAB-RRE)
              TO RRE-COD-CAN
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-RRE) NOT NUMERIC
              MOVE 0 TO RRE-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-RRE)
              TO RRE-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-RRE)
              TO RRE-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-RRE) NOT NUMERIC
              MOVE 0 TO RRE-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-RRE)
              TO RRE-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-RRE) NOT NUMERIC
              MOVE 0 TO RRE-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-RRE)
              TO RRE-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-RRE) NOT NUMERIC
              MOVE 0 TO RRE-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-RRE)
              TO RRE-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-RRE)
              TO RRE-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-RRE)
              TO RRE-DS-STATO-ELAB
           IF (SF)-COD-PNT-RETE-INI-C-NULL(IX-TAB-RRE) = HIGH-VALUES
              MOVE (SF)-COD-PNT-RETE-INI-C-NULL(IX-TAB-RRE)
              TO RRE-COD-PNT-RETE-INI-C-NULL
           ELSE
              MOVE (SF)-COD-PNT-RETE-INI-C(IX-TAB-RRE)
              TO RRE-COD-PNT-RETE-INI-C
           END-IF
           IF (SF)-MATR-OPRT-NULL(IX-TAB-RRE) = HIGH-VALUES
              MOVE (SF)-MATR-OPRT-NULL(IX-TAB-RRE)
              TO RRE-MATR-OPRT-NULL
           ELSE
              MOVE (SF)-MATR-OPRT(IX-TAB-RRE)
              TO RRE-MATR-OPRT
           END-IF.
       VAL-DCLGEN-RRE-EX.
           EXIT.

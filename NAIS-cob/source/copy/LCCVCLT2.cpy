      *-----------------------------------------------------------------*
      *   PORTAFOGLIO VITA - PROCESSO VENDITA                           *
      *   CHIAMATA  AL DISPATCHER PER OPRAZIONI DI SELECT O FETCH       *
      *         E GESTIONE ERRORE ALL  (TABELLA ASSET ALLOCATION)       *
      *    ULTIMO AGG.  07 FEB 2007
      * --------------------------------------------------------------- *
       LETTURA-CLT.
           IF IDSI0011-SELECT
              PERFORM SELECT-CLT
                THRU SELECT-CLT-EX
           ELSE
              PERFORM FETCH-CLT
                THRU FETCH-CLT-EX
           END-IF
       LETTURA-CLT-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         SELECT                                 *
      *----------------------------------------------------------------*
       SELECT-CLT.

           PERFORM CALL-DISPATCHER
               THRU CALL-DISPATCHER-EX.

              IF IDSO0011-SUCCESSFUL-RC
                 EVALUATE TRUE
                     WHEN IDSO0011-SUCCESSFUL-SQL
      * -->       OPERAZIONE ESEGUITA CORRETTAMENTE
                        MOVE IDSO0011-BUFFER-DATI
                          TO CLAU-TXT
                        PERFORM VALORIZZA-OUTPUT-CLT
                          THRU VALORIZZA-OUTPUT-CLT-EX
                     WHEN IDSO0011-NOT-FOUND
      * --->          CAMPO $ NON TROVATO
                            MOVE WK-PGM
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'SELECT-CLT'
                             TO IEAI9901-LABEL-ERR
                           MOVE '005019'
                             TO IEAI9901-COD-ERRORE
                           MOVE 'ID-OGGETTO'
                             TO IEAI9901-PARAMETRI-ERR
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                      WHEN OTHER
      * --->          ERRORE DI ACCESSO AL DB
                        MOVE WK-PGM
                                        TO IEAI9901-COD-SERVIZIO-BE
                        MOVE 'SELECT-CLT'
                                        TO IEAI9901-LABEL-ERR
                        MOVE '005015'   TO IEAI9901-COD-ERRORE
                        MOVE WK-TABELLA TO IEAI9901-PARAMETRI-ERR
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                           THRU EX-S0300
                 END-EVALUATE
              ELSE
      * -->       GESTIRE ERRORE DISPATCHER
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'SELECT-CLT'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005016'
                   TO IEAI9901-COD-ERRORE
                 MOVE SPACES
                   TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-PERFORM.
       SELECT-CLT-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         FETCH                                  *
      *----------------------------------------------------------------*
       FETCH-CLT.

           SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
           SET  WCOM-OVERFLOW-NO                  TO TRUE.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR WCOM-OVERFLOW-YES

               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      * -->                 NON TROVATA
                           IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM
                                TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'FETCH-CLT'
                                TO IEAI9901-LABEL-ERR
                              MOVE '005019'
                                TO IEAI9901-COD-ERRORE
                              MOVE 'ID-PTF'
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                           END-IF
                      WHEN IDSO0011-SUCCESSFUL-SQL
      * -->                 OPERAZIONE ESEGUITA CORRETTAMENTE
                           MOVE IDSO0011-BUFFER-DATI  TO CLAU-TXT
                           ADD  1                     TO IX-TAB-CLT

      * -->  Se il contatore di lettura � maggiore dell' elemento MAX
      * -->  previsto per la TAB.CLAU_TESTUALE allora siamo in overflow
                           IF IX-TAB-CLT > (SF)-ELE-CLAU-MAX
                              SET WCOM-OVERFLOW-YES   TO TRUE
                           ELSE
                              PERFORM VALORIZZA-OUTPUT-CLT
                                 THRU VALORIZZA-OUTPUT-CLT-EX
                              SET IDSI0011-FETCH-NEXT TO TRUE
                           END-IF
                      WHEN OTHER
      * -->                 ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'FETCH-CLT'
                             TO IEAI9901-LABEL-ERR
                           MOVE '005015'
                             TO IEAI9901-COD-ERRORE
                           MOVE WK-TABELLA
                             TO IEAI9901-PARAMETRI-ERR
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      * -->        ERRORE DISPATCHER
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'FETCH-CLT'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005016'
                    TO IEAI9901-COD-ERRORE
                  MOVE SPACES
                    TO IEAI9901-PARAMETRI-ERR
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

       FETCH-CLT-EX.
           EXIT.



      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVPOL3
      *   ULTIMO AGG. 07 DIC 2017
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-POL.
           MOVE POL-ID-POLI
             TO (SF)-ID-PTF
           MOVE POL-ID-POLI
             TO (SF)-ID-POLI
           MOVE POL-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ
           IF POL-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE POL-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL
           ELSE
              MOVE POL-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU
           END-IF
           IF POL-IB-OGG-NULL = HIGH-VALUES
              MOVE POL-IB-OGG-NULL
                TO (SF)-IB-OGG-NULL
           ELSE
              MOVE POL-IB-OGG
                TO (SF)-IB-OGG
           END-IF
           MOVE POL-IB-PROP
             TO (SF)-IB-PROP
           IF POL-DT-PROP-NULL = HIGH-VALUES
              MOVE POL-DT-PROP-NULL
                TO (SF)-DT-PROP-NULL
           ELSE
              MOVE POL-DT-PROP
                TO (SF)-DT-PROP
           END-IF
           MOVE POL-DT-INI-EFF
             TO (SF)-DT-INI-EFF
           MOVE POL-DT-END-EFF
             TO (SF)-DT-END-EFF
           MOVE POL-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE POL-DT-DECOR
             TO (SF)-DT-DECOR
           MOVE POL-DT-EMIS
             TO (SF)-DT-EMIS
           MOVE POL-TP-POLI
             TO (SF)-TP-POLI
           IF POL-DUR-AA-NULL = HIGH-VALUES
              MOVE POL-DUR-AA-NULL
                TO (SF)-DUR-AA-NULL
           ELSE
              MOVE POL-DUR-AA
                TO (SF)-DUR-AA
           END-IF
           IF POL-DUR-MM-NULL = HIGH-VALUES
              MOVE POL-DUR-MM-NULL
                TO (SF)-DUR-MM-NULL
           ELSE
              MOVE POL-DUR-MM
                TO (SF)-DUR-MM
           END-IF
           IF POL-DT-SCAD-NULL = HIGH-VALUES
              MOVE POL-DT-SCAD-NULL
                TO (SF)-DT-SCAD-NULL
           ELSE
              MOVE POL-DT-SCAD
                TO (SF)-DT-SCAD
           END-IF
           MOVE POL-COD-PROD
             TO (SF)-COD-PROD
           MOVE POL-DT-INI-VLDT-PROD
             TO (SF)-DT-INI-VLDT-PROD
           IF POL-COD-CONV-NULL = HIGH-VALUES
              MOVE POL-COD-CONV-NULL
                TO (SF)-COD-CONV-NULL
           ELSE
              MOVE POL-COD-CONV
                TO (SF)-COD-CONV
           END-IF
           IF POL-COD-RAMO-NULL = HIGH-VALUES
              MOVE POL-COD-RAMO-NULL
                TO (SF)-COD-RAMO-NULL
           ELSE
              MOVE POL-COD-RAMO
                TO (SF)-COD-RAMO
           END-IF
           IF POL-DT-INI-VLDT-CONV-NULL = HIGH-VALUES
              MOVE POL-DT-INI-VLDT-CONV-NULL
                TO (SF)-DT-INI-VLDT-CONV-NULL
           ELSE
              MOVE POL-DT-INI-VLDT-CONV
                TO (SF)-DT-INI-VLDT-CONV
           END-IF
           IF POL-DT-APPLZ-CONV-NULL = HIGH-VALUES
              MOVE POL-DT-APPLZ-CONV-NULL
                TO (SF)-DT-APPLZ-CONV-NULL
           ELSE
              MOVE POL-DT-APPLZ-CONV
                TO (SF)-DT-APPLZ-CONV
           END-IF
           MOVE POL-TP-FRM-ASSVA
             TO (SF)-TP-FRM-ASSVA
           IF POL-TP-RGM-FISC-NULL = HIGH-VALUES
              MOVE POL-TP-RGM-FISC-NULL
                TO (SF)-TP-RGM-FISC-NULL
           ELSE
              MOVE POL-TP-RGM-FISC
                TO (SF)-TP-RGM-FISC
           END-IF
           IF POL-FL-ESTAS-NULL = HIGH-VALUES
              MOVE POL-FL-ESTAS-NULL
                TO (SF)-FL-ESTAS-NULL
           ELSE
              MOVE POL-FL-ESTAS
                TO (SF)-FL-ESTAS
           END-IF
           IF POL-FL-RSH-COMUN-NULL = HIGH-VALUES
              MOVE POL-FL-RSH-COMUN-NULL
                TO (SF)-FL-RSH-COMUN-NULL
           ELSE
              MOVE POL-FL-RSH-COMUN
                TO (SF)-FL-RSH-COMUN
           END-IF
           IF POL-FL-RSH-COMUN-COND-NULL = HIGH-VALUES
              MOVE POL-FL-RSH-COMUN-COND-NULL
                TO (SF)-FL-RSH-COMUN-COND-NULL
           ELSE
              MOVE POL-FL-RSH-COMUN-COND
                TO (SF)-FL-RSH-COMUN-COND
           END-IF
           MOVE POL-TP-LIV-GENZ-TIT
             TO (SF)-TP-LIV-GENZ-TIT
           IF POL-FL-COP-FINANZ-NULL = HIGH-VALUES
              MOVE POL-FL-COP-FINANZ-NULL
                TO (SF)-FL-COP-FINANZ-NULL
           ELSE
              MOVE POL-FL-COP-FINANZ
                TO (SF)-FL-COP-FINANZ
           END-IF
           IF POL-TP-APPLZ-DIR-NULL = HIGH-VALUES
              MOVE POL-TP-APPLZ-DIR-NULL
                TO (SF)-TP-APPLZ-DIR-NULL
           ELSE
              MOVE POL-TP-APPLZ-DIR
                TO (SF)-TP-APPLZ-DIR
           END-IF
           IF POL-SPE-MED-NULL = HIGH-VALUES
              MOVE POL-SPE-MED-NULL
                TO (SF)-SPE-MED-NULL
           ELSE
              MOVE POL-SPE-MED
                TO (SF)-SPE-MED
           END-IF
           IF POL-DIR-EMIS-NULL = HIGH-VALUES
              MOVE POL-DIR-EMIS-NULL
                TO (SF)-DIR-EMIS-NULL
           ELSE
              MOVE POL-DIR-EMIS
                TO (SF)-DIR-EMIS
           END-IF
           IF POL-DIR-1O-VERS-NULL = HIGH-VALUES
              MOVE POL-DIR-1O-VERS-NULL
                TO (SF)-DIR-1O-VERS-NULL
           ELSE
              MOVE POL-DIR-1O-VERS
                TO (SF)-DIR-1O-VERS
           END-IF
           IF POL-DIR-VERS-AGG-NULL = HIGH-VALUES
              MOVE POL-DIR-VERS-AGG-NULL
                TO (SF)-DIR-VERS-AGG-NULL
           ELSE
              MOVE POL-DIR-VERS-AGG
                TO (SF)-DIR-VERS-AGG
           END-IF
           IF POL-COD-DVS-NULL = HIGH-VALUES
              MOVE POL-COD-DVS-NULL
                TO (SF)-COD-DVS-NULL
           ELSE
              MOVE POL-COD-DVS
                TO (SF)-COD-DVS
           END-IF
           IF POL-FL-FNT-AZ-NULL = HIGH-VALUES
              MOVE POL-FL-FNT-AZ-NULL
                TO (SF)-FL-FNT-AZ-NULL
           ELSE
              MOVE POL-FL-FNT-AZ
                TO (SF)-FL-FNT-AZ
           END-IF
           IF POL-FL-FNT-ADER-NULL = HIGH-VALUES
              MOVE POL-FL-FNT-ADER-NULL
                TO (SF)-FL-FNT-ADER-NULL
           ELSE
              MOVE POL-FL-FNT-ADER
                TO (SF)-FL-FNT-ADER
           END-IF
           IF POL-FL-FNT-TFR-NULL = HIGH-VALUES
              MOVE POL-FL-FNT-TFR-NULL
                TO (SF)-FL-FNT-TFR-NULL
           ELSE
              MOVE POL-FL-FNT-TFR
                TO (SF)-FL-FNT-TFR
           END-IF
           IF POL-FL-FNT-VOLO-NULL = HIGH-VALUES
              MOVE POL-FL-FNT-VOLO-NULL
                TO (SF)-FL-FNT-VOLO-NULL
           ELSE
              MOVE POL-FL-FNT-VOLO
                TO (SF)-FL-FNT-VOLO
           END-IF
           IF POL-TP-OPZ-A-SCAD-NULL = HIGH-VALUES
              MOVE POL-TP-OPZ-A-SCAD-NULL
                TO (SF)-TP-OPZ-A-SCAD-NULL
           ELSE
              MOVE POL-TP-OPZ-A-SCAD
                TO (SF)-TP-OPZ-A-SCAD
           END-IF
           IF POL-AA-DIFF-PROR-DFLT-NULL = HIGH-VALUES
              MOVE POL-AA-DIFF-PROR-DFLT-NULL
                TO (SF)-AA-DIFF-PROR-DFLT-NULL
           ELSE
              MOVE POL-AA-DIFF-PROR-DFLT
                TO (SF)-AA-DIFF-PROR-DFLT
           END-IF
           IF POL-FL-VER-PROD-NULL = HIGH-VALUES
              MOVE POL-FL-VER-PROD-NULL
                TO (SF)-FL-VER-PROD-NULL
           ELSE
              MOVE POL-FL-VER-PROD
                TO (SF)-FL-VER-PROD
           END-IF
           IF POL-DUR-GG-NULL = HIGH-VALUES
              MOVE POL-DUR-GG-NULL
                TO (SF)-DUR-GG-NULL
           ELSE
              MOVE POL-DUR-GG
                TO (SF)-DUR-GG
           END-IF
           IF POL-DIR-QUIET-NULL = HIGH-VALUES
              MOVE POL-DIR-QUIET-NULL
                TO (SF)-DIR-QUIET-NULL
           ELSE
              MOVE POL-DIR-QUIET
                TO (SF)-DIR-QUIET
           END-IF
           IF POL-TP-PTF-ESTNO-NULL = HIGH-VALUES
              MOVE POL-TP-PTF-ESTNO-NULL
                TO (SF)-TP-PTF-ESTNO-NULL
           ELSE
              MOVE POL-TP-PTF-ESTNO
                TO (SF)-TP-PTF-ESTNO
           END-IF
           IF POL-FL-CUM-PRE-CNTR-NULL = HIGH-VALUES
              MOVE POL-FL-CUM-PRE-CNTR-NULL
                TO (SF)-FL-CUM-PRE-CNTR-NULL
           ELSE
              MOVE POL-FL-CUM-PRE-CNTR
                TO (SF)-FL-CUM-PRE-CNTR
           END-IF
           IF POL-FL-AMMB-MOVI-NULL = HIGH-VALUES
              MOVE POL-FL-AMMB-MOVI-NULL
                TO (SF)-FL-AMMB-MOVI-NULL
           ELSE
              MOVE POL-FL-AMMB-MOVI
                TO (SF)-FL-AMMB-MOVI
           END-IF
           IF POL-CONV-GECO-NULL = HIGH-VALUES
              MOVE POL-CONV-GECO-NULL
                TO (SF)-CONV-GECO-NULL
           ELSE
              MOVE POL-CONV-GECO
                TO (SF)-CONV-GECO
           END-IF
           MOVE POL-DS-RIGA
             TO (SF)-DS-RIGA
           MOVE POL-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE POL-DS-VER
             TO (SF)-DS-VER
           MOVE POL-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ
           MOVE POL-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ
           MOVE POL-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE POL-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           IF POL-FL-SCUDO-FISC-NULL = HIGH-VALUES
              MOVE POL-FL-SCUDO-FISC-NULL
                TO (SF)-FL-SCUDO-FISC-NULL
           ELSE
              MOVE POL-FL-SCUDO-FISC
                TO (SF)-FL-SCUDO-FISC
           END-IF
           IF POL-FL-TRASFE-NULL = HIGH-VALUES
              MOVE POL-FL-TRASFE-NULL
                TO (SF)-FL-TRASFE-NULL
           ELSE
              MOVE POL-FL-TRASFE
                TO (SF)-FL-TRASFE
           END-IF
           IF POL-FL-TFR-STRC-NULL = HIGH-VALUES
              MOVE POL-FL-TFR-STRC-NULL
                TO (SF)-FL-TFR-STRC-NULL
           ELSE
              MOVE POL-FL-TFR-STRC
                TO (SF)-FL-TFR-STRC
           END-IF
           IF POL-DT-PRESC-NULL = HIGH-VALUES
              MOVE POL-DT-PRESC-NULL
                TO (SF)-DT-PRESC-NULL
           ELSE
              MOVE POL-DT-PRESC
                TO (SF)-DT-PRESC
           END-IF
           IF POL-COD-CONV-AGG-NULL = HIGH-VALUES
              MOVE POL-COD-CONV-AGG-NULL
                TO (SF)-COD-CONV-AGG-NULL
           ELSE
              MOVE POL-COD-CONV-AGG
                TO (SF)-COD-CONV-AGG
           END-IF
           IF POL-SUBCAT-PROD-NULL = HIGH-VALUES
              MOVE POL-SUBCAT-PROD-NULL
                TO (SF)-SUBCAT-PROD-NULL
           ELSE
              MOVE POL-SUBCAT-PROD
                TO (SF)-SUBCAT-PROD
           END-IF
           IF POL-FL-QUEST-ADEGZ-ASS-NULL = HIGH-VALUES
              MOVE POL-FL-QUEST-ADEGZ-ASS-NULL
                TO (SF)-FL-QUEST-ADEGZ-ASS-NULL
           ELSE
              MOVE POL-FL-QUEST-ADEGZ-ASS
                TO (SF)-FL-QUEST-ADEGZ-ASS
           END-IF
           IF POL-COD-TPA-NULL = HIGH-VALUES
              MOVE POL-COD-TPA-NULL
                TO (SF)-COD-TPA-NULL
           ELSE
              MOVE POL-COD-TPA
                TO (SF)-COD-TPA
           END-IF
           IF POL-ID-ACC-COMM-NULL = HIGH-VALUES
              MOVE POL-ID-ACC-COMM-NULL
                TO (SF)-ID-ACC-COMM-NULL
           ELSE
              MOVE POL-ID-ACC-COMM
                TO (SF)-ID-ACC-COMM
           END-IF
           IF POL-FL-POLI-CPI-PR-NULL = HIGH-VALUES
              MOVE POL-FL-POLI-CPI-PR-NULL
                TO (SF)-FL-POLI-CPI-PR-NULL
           ELSE
              MOVE POL-FL-POLI-CPI-PR
                TO (SF)-FL-POLI-CPI-PR
           END-IF
           IF POL-FL-POLI-BUNDLING-NULL = HIGH-VALUES
              MOVE POL-FL-POLI-BUNDLING-NULL
                TO (SF)-FL-POLI-BUNDLING-NULL
           ELSE
              MOVE POL-FL-POLI-BUNDLING
                TO (SF)-FL-POLI-BUNDLING
           END-IF
           IF POL-IND-POLI-PRIN-COLL-NULL = HIGH-VALUES
              MOVE POL-IND-POLI-PRIN-COLL-NULL
                TO (SF)-IND-POLI-PRIN-COLL-NULL
           ELSE
              MOVE POL-IND-POLI-PRIN-COLL
                TO (SF)-IND-POLI-PRIN-COLL
           END-IF
           IF POL-FL-VND-BUNDLE-NULL = HIGH-VALUES
              MOVE POL-FL-VND-BUNDLE-NULL
                TO (SF)-FL-VND-BUNDLE-NULL
           ELSE
              MOVE POL-FL-VND-BUNDLE
                TO (SF)-FL-VND-BUNDLE
           END-IF
           IF POL-IB-BS-NULL = HIGH-VALUES
              MOVE POL-IB-BS-NULL
                TO (SF)-IB-BS-NULL
           ELSE
              MOVE POL-IB-BS
                TO (SF)-IB-BS
           END-IF
           IF POL-FL-POLI-IFP-NULL = HIGH-VALUES
              MOVE POL-FL-POLI-IFP-NULL
                TO (SF)-FL-POLI-IFP-NULL
           ELSE
              MOVE POL-FL-POLI-IFP
                TO (SF)-FL-POLI-IFP
           END-IF.
       VALORIZZA-OUTPUT-POL-EX.
           EXIT.

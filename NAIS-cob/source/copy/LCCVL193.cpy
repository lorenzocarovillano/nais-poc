
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVL193
      *   ULTIMO AGG. 24 APR 2015
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-L19.
           MOVE L19-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-L19)
           MOVE L19-COD-FND
             TO (SF)-COD-FND(IX-TAB-L19)
           MOVE L19-DT-QTZ
             TO (SF)-DT-QTZ(IX-TAB-L19)
           IF L19-VAL-QUO-NULL = HIGH-VALUES
              MOVE L19-VAL-QUO-NULL
                TO (SF)-VAL-QUO-NULL(IX-TAB-L19)
           ELSE
              MOVE L19-VAL-QUO
                TO (SF)-VAL-QUO(IX-TAB-L19)
           END-IF
           IF L19-VAL-QUO-MANFEE-NULL = HIGH-VALUES
              MOVE L19-VAL-QUO-MANFEE-NULL
                TO (SF)-VAL-QUO-MANFEE-NULL(IX-TAB-L19)
           ELSE
              MOVE L19-VAL-QUO-MANFEE
                TO (SF)-VAL-QUO-MANFEE(IX-TAB-L19)
           END-IF
           MOVE L19-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-L19)
           MOVE L19-DS-VER
             TO (SF)-DS-VER(IX-TAB-L19)
           MOVE L19-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ(IX-TAB-L19)
           MOVE L19-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-L19)
           MOVE L19-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-L19)
           MOVE L19-TP-FND
             TO (SF)-TP-FND(IX-TAB-L19)
           IF L19-DT-RILEVAZIONE-NAV-NULL = HIGH-VALUES
              MOVE L19-DT-RILEVAZIONE-NAV-NULL
                TO (SF)-DT-RILEVAZIONE-NAV-NULL(IX-TAB-L19)
           ELSE
              MOVE L19-DT-RILEVAZIONE-NAV
                TO (SF)-DT-RILEVAZIONE-NAV(IX-TAB-L19)
           END-IF
           IF L19-VAL-QUO-ACQ-NULL = HIGH-VALUES
              MOVE L19-VAL-QUO-ACQ-NULL
                TO (SF)-VAL-QUO-ACQ-NULL(IX-TAB-L19)
           ELSE
              MOVE L19-VAL-QUO-ACQ
                TO (SF)-VAL-QUO-ACQ(IX-TAB-L19)
           END-IF
           IF L19-FL-NO-NAV-NULL = HIGH-VALUES
              MOVE L19-FL-NO-NAV-NULL
                TO (SF)-FL-NO-NAV-NULL(IX-TAB-L19)
           ELSE
              MOVE L19-FL-NO-NAV
                TO (SF)-FL-NO-NAV(IX-TAB-L19)
           END-IF.
       VALORIZZA-OUTPUT-L19-EX.
           EXIT.

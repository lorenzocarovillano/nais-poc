      *----------------------------------------------------------------*
      * DIMENSIONAMENTO OCCURS PER LA TAB. RICH_DIS_FND                *
      * COPY RIPORTANTE IL NUMERO DI OCCURS DA UTILIZZARE              *
      * PER INIZIALIZZA TOT DELLA TABELLA                              *
      *----------------------------------------------------------------*
       01 WK-RDF-MAX.
          03 WK-RDF-MAX-A                 PIC 9(04) VALUE 1250.

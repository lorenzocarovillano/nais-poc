
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVQUE5
      *   ULTIMO AGG. 03 GIU 2019
      *------------------------------------------------------------

       VAL-DCLGEN-QUE.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-QUE) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-QUE)
              TO QUE-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-QUE)
              TO QUE-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-QUE) NOT NUMERIC
              MOVE 0 TO QUE-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-QUE)
              TO QUE-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-QUE) NOT NUMERIC
              MOVE 0 TO QUE-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-QUE)
              TO QUE-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-QUE)
              TO QUE-COD-COMP-ANIA
           MOVE (SF)-COD-QUEST(IX-TAB-QUE)
              TO QUE-COD-QUEST
           MOVE (SF)-TP-QUEST(IX-TAB-QUE)
              TO QUE-TP-QUEST
           IF (SF)-FL-VST-MED-NULL(IX-TAB-QUE) = HIGH-VALUES
              MOVE (SF)-FL-VST-MED-NULL(IX-TAB-QUE)
              TO QUE-FL-VST-MED-NULL
           ELSE
              MOVE (SF)-FL-VST-MED(IX-TAB-QUE)
              TO QUE-FL-VST-MED
           END-IF
           IF (SF)-FL-STAT-BUON-SAL-NULL(IX-TAB-QUE) = HIGH-VALUES
              MOVE (SF)-FL-STAT-BUON-SAL-NULL(IX-TAB-QUE)
              TO QUE-FL-STAT-BUON-SAL-NULL
           ELSE
              MOVE (SF)-FL-STAT-BUON-SAL(IX-TAB-QUE)
              TO QUE-FL-STAT-BUON-SAL
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-QUE) NOT NUMERIC
              MOVE 0 TO QUE-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-QUE)
              TO QUE-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-QUE)
              TO QUE-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-QUE) NOT NUMERIC
              MOVE 0 TO QUE-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-QUE)
              TO QUE-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-QUE) NOT NUMERIC
              MOVE 0 TO QUE-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-QUE)
              TO QUE-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-QUE) NOT NUMERIC
              MOVE 0 TO QUE-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-QUE)
              TO QUE-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-QUE)
              TO QUE-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-QUE)
              TO QUE-DS-STATO-ELAB
           IF (SF)-TP-ADEGZ-NULL(IX-TAB-QUE) = HIGH-VALUES
              MOVE (SF)-TP-ADEGZ-NULL(IX-TAB-QUE)
              TO QUE-TP-ADEGZ-NULL
           ELSE
              MOVE (SF)-TP-ADEGZ(IX-TAB-QUE)
              TO QUE-TP-ADEGZ
           END-IF.
       VAL-DCLGEN-QUE-EX.
           EXIT.

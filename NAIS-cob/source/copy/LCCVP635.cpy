
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVP635
      *   ULTIMO AGG. 24 GEN 2014
      *------------------------------------------------------------

       VAL-DCLGEN-P63.
           MOVE (SF)-ID-ACC-COMM
              TO P63-ID-ACC-COMM
           MOVE (SF)-TP-ACC-COMM
              TO P63-TP-ACC-COMM
           MOVE (SF)-COD-COMP-ANIA
              TO P63-COD-COMP-ANIA
           MOVE (SF)-COD-PARTNER
              TO P63-COD-PARTNER
           MOVE (SF)-IB-ACC-COMM
              TO P63-IB-ACC-COMM
           MOVE (SF)-IB-ACC-COMM-MASTER
              TO P63-IB-ACC-COMM-MASTER
           MOVE (SF)-DESC-ACC-COMM
              TO P63-DESC-ACC-COMM
           MOVE (SF)-DT-FIRMA
              TO P63-DT-FIRMA
           MOVE (SF)-DT-INI-VLDT
              TO P63-DT-INI-VLDT
           MOVE (SF)-DT-END-VLDT
              TO P63-DT-END-VLDT
           MOVE (SF)-FL-INVIO-CONFERME
              TO P63-FL-INVIO-CONFERME
           MOVE (SF)-DS-OPER-SQL
              TO P63-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO P63-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO P63-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO P63-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO P63-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO P63-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO P63-DS-STATO-ELAB
           MOVE (SF)-DESC-ACC-COMM-MAST
              TO P63-DESC-ACC-COMM-MAST.
       VAL-DCLGEN-P63-EX.
           EXIT.

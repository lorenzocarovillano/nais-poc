       01 LDBV3411.
           03 LDBV3411-DATI-INPUT.
              05 LDBV3411-ID-POLI               PIC S9(09) COMP-3.
              05 LDBV3411-ID-ADES               PIC S9(09) COMP-3.
              05 LDBV3411-TP-OGG                PIC  X(02).
              05 LDBV3411-TP-STAT-BUS-1         PIC  X(02).
              05 LDBV3411-TP-STAT-BUS-2         PIC  X(02).

           03 LDBV3411-GAR-OUTPUT.
              05 L3411-ID-GAR PIC S9(9)V     COMP-3.
              05 L3411-ID-ADES PIC S9(9)V     COMP-3.
              05 L3411-ID-ADES-NULL REDEFINES
                 L3411-ID-ADES   PIC X(5).
              05 L3411-ID-POLI PIC S9(9)V     COMP-3.
              05 L3411-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
              05 L3411-ID-MOVI-CHIU PIC S9(9)V     COMP-3.
              05 L3411-ID-MOVI-CHIU-NULL REDEFINES
                 L3411-ID-MOVI-CHIU   PIC X(5).
              05 L3411-DT-INI-EFF   PIC S9(8)V COMP-3.
              05 L3411-DT-END-EFF   PIC S9(8)V COMP-3.
              05 L3411-COD-COMP-ANIA PIC S9(5)V     COMP-3.
              05 L3411-IB-OGG PIC X(40).
              05 L3411-IB-OGG-NULL REDEFINES
                 L3411-IB-OGG   PIC X(40).
              05 L3411-DT-DECOR   PIC S9(8)V COMP-3.
              05 L3411-DT-DECOR-NULL REDEFINES
                 L3411-DT-DECOR   PIC X(5).
              05 L3411-DT-SCAD   PIC S9(8)V COMP-3.
              05 L3411-DT-SCAD-NULL REDEFINES
                 L3411-DT-SCAD   PIC X(5).
              05 L3411-COD-SEZ PIC X(12).
              05 L3411-COD-SEZ-NULL REDEFINES
                 L3411-COD-SEZ   PIC X(12).
              05 L3411-COD-TARI PIC X(12).
              05 L3411-RAMO-BILA PIC X(12).
              05 L3411-RAMO-BILA-NULL REDEFINES
                 L3411-RAMO-BILA   PIC X(12).
              05 L3411-DT-INI-VAL-TAR   PIC S9(8)V COMP-3.
              05 L3411-DT-INI-VAL-TAR-NULL REDEFINES
                 L3411-DT-INI-VAL-TAR   PIC X(5).
              05 L3411-ID-1O-ASSTO PIC S9(9)V     COMP-3.
              05 L3411-ID-1O-ASSTO-NULL REDEFINES
                 L3411-ID-1O-ASSTO   PIC X(5).
              05 L3411-ID-2O-ASSTO PIC S9(9)V     COMP-3.
              05 L3411-ID-2O-ASSTO-NULL REDEFINES
                 L3411-ID-2O-ASSTO   PIC X(5).
              05 L3411-ID-3O-ASSTO PIC S9(9)V     COMP-3.
              05 L3411-ID-3O-ASSTO-NULL REDEFINES
                 L3411-ID-3O-ASSTO   PIC X(5).
              05 L3411-TP-GAR PIC S9(2)V     COMP-3.
              05 L3411-TP-GAR-NULL REDEFINES
                 L3411-TP-GAR   PIC X(2).
              05 L3411-TP-RSH PIC X(2).
              05 L3411-TP-RSH-NULL REDEFINES
                 L3411-TP-RSH   PIC X(2).
              05 L3411-TP-INVST PIC S9(2)V     COMP-3.
              05 L3411-TP-INVST-NULL REDEFINES
                 L3411-TP-INVST   PIC X(2).
              05 L3411-MOD-PAG-GARCOL PIC X(2).
              05 L3411-MOD-PAG-GARCOL-NULL REDEFINES
                 L3411-MOD-PAG-GARCOL   PIC X(2).
              05 L3411-TP-PER-PRE PIC X(2).
              05 L3411-TP-PER-PRE-NULL REDEFINES
                 L3411-TP-PER-PRE   PIC X(2).
              05 L3411-ETA-AA-1O-ASSTO PIC S9(3)V     COMP-3.
              05 L3411-ETA-AA-1O-ASSTO-NULL REDEFINES
                 L3411-ETA-AA-1O-ASSTO   PIC X(2).
              05 L3411-ETA-MM-1O-ASSTO PIC S9(3)V     COMP-3.
              05 L3411-ETA-MM-1O-ASSTO-NULL REDEFINES
                 L3411-ETA-MM-1O-ASSTO   PIC X(2).
              05 L3411-ETA-AA-2O-ASSTO PIC S9(3)V     COMP-3.
              05 L3411-ETA-AA-2O-ASSTO-NULL REDEFINES
                 L3411-ETA-AA-2O-ASSTO   PIC X(2).
              05 L3411-ETA-MM-2O-ASSTO PIC S9(3)V     COMP-3.
              05 L3411-ETA-MM-2O-ASSTO-NULL REDEFINES
                 L3411-ETA-MM-2O-ASSTO   PIC X(2).
              05 L3411-ETA-AA-3O-ASSTO PIC S9(3)V     COMP-3.
              05 L3411-ETA-AA-3O-ASSTO-NULL REDEFINES
                 L3411-ETA-AA-3O-ASSTO   PIC X(2).
              05 L3411-ETA-MM-3O-ASSTO PIC S9(3)V     COMP-3.
              05 L3411-ETA-MM-3O-ASSTO-NULL REDEFINES
                 L3411-ETA-MM-3O-ASSTO   PIC X(2).
              05 L3411-TP-EMIS-PUR PIC X(1).
              05 L3411-TP-EMIS-PUR-NULL REDEFINES
                 L3411-TP-EMIS-PUR   PIC X(1).
              05 L3411-ETA-A-SCAD PIC S9(5)V     COMP-3.
              05 L3411-ETA-A-SCAD-NULL REDEFINES
                 L3411-ETA-A-SCAD   PIC X(3).
              05 L3411-TP-CALC-PRE-PRSTZ PIC X(2).
              05 L3411-TP-CALC-PRE-PRSTZ-NULL REDEFINES
                 L3411-TP-CALC-PRE-PRSTZ   PIC X(2).
              05 L3411-TP-PRE PIC X(1).
              05 L3411-TP-PRE-NULL REDEFINES
                 L3411-TP-PRE   PIC X(1).
              05 L3411-TP-DUR PIC X(2).
              05 L3411-TP-DUR-NULL REDEFINES
                 L3411-TP-DUR   PIC X(2).
              05 L3411-DUR-AA PIC S9(5)V     COMP-3.
              05 L3411-DUR-AA-NULL REDEFINES
                 L3411-DUR-AA   PIC X(3).
              05 L3411-DUR-MM PIC S9(5)V     COMP-3.
              05 L3411-DUR-MM-NULL REDEFINES
                 L3411-DUR-MM   PIC X(3).
              05 L3411-DUR-GG PIC S9(5)V     COMP-3.
              05 L3411-DUR-GG-NULL REDEFINES
                 L3411-DUR-GG   PIC X(3).
              05 L3411-NUM-AA-PAG-PRE PIC S9(5)V     COMP-3.
              05 L3411-NUM-AA-PAG-PRE-NULL REDEFINES
                 L3411-NUM-AA-PAG-PRE   PIC X(3).
              05 L3411-AA-PAG-PRE-UNI PIC S9(5)V     COMP-3.
              05 L3411-AA-PAG-PRE-UNI-NULL REDEFINES
                 L3411-AA-PAG-PRE-UNI   PIC X(3).
              05 L3411-MM-PAG-PRE-UNI PIC S9(5)V     COMP-3.
              05 L3411-MM-PAG-PRE-UNI-NULL REDEFINES
                 L3411-MM-PAG-PRE-UNI   PIC X(3).
              05 L3411-FRAZ-INI-EROG-REN PIC S9(5)V     COMP-3.
              05 L3411-FRAZ-INI-EROG-REN-NULL REDEFINES
                 L3411-FRAZ-INI-EROG-REN   PIC X(3).
              05 L3411-MM-1O-RAT PIC S9(2)V     COMP-3.
              05 L3411-MM-1O-RAT-NULL REDEFINES
                 L3411-MM-1O-RAT   PIC X(2).
              05 L3411-PC-1O-RAT PIC S9(3)V9(3) COMP-3.
              05 L3411-PC-1O-RAT-NULL REDEFINES
                 L3411-PC-1O-RAT   PIC X(4).
              05 L3411-TP-PRSTZ-ASSTA PIC X(2).
              05 L3411-TP-PRSTZ-ASSTA-NULL REDEFINES
                 L3411-TP-PRSTZ-ASSTA   PIC X(2).
              05 L3411-DT-END-CARZ   PIC S9(8)V COMP-3.
              05 L3411-DT-END-CARZ-NULL REDEFINES
                 L3411-DT-END-CARZ   PIC X(5).
              05 L3411-PC-RIP-PRE PIC S9(3)V9(3) COMP-3.
              05 L3411-PC-RIP-PRE-NULL REDEFINES
                 L3411-PC-RIP-PRE   PIC X(4).
              05 L3411-COD-FND PIC X(12).
              05 L3411-COD-FND-NULL REDEFINES
                 L3411-COD-FND   PIC X(12).
              05 L3411-AA-REN-CER PIC X(18).
              05 L3411-AA-REN-CER-NULL REDEFINES
                 L3411-AA-REN-CER   PIC X(18).
              05 L3411-PC-REVRSB PIC S9(3)V9(3) COMP-3.
              05 L3411-PC-REVRSB-NULL REDEFINES
                 L3411-PC-REVRSB   PIC X(4).
              05 L3411-TP-PC-RIP PIC X(2).
              05 L3411-TP-PC-RIP-NULL REDEFINES
                 L3411-TP-PC-RIP   PIC X(2).
              05 L3411-PC-OPZ PIC S9(3)V9(3) COMP-3.
              05 L3411-PC-OPZ-NULL REDEFINES
                 L3411-PC-OPZ   PIC X(4).
              05 L3411-TP-IAS PIC X(2).
              05 L3411-TP-IAS-NULL REDEFINES
                 L3411-TP-IAS   PIC X(2).
              05 L3411-TP-STAB PIC X(2).
              05 L3411-TP-STAB-NULL REDEFINES
                 L3411-TP-STAB   PIC X(2).
              05 L3411-TP-ADEG-PRE PIC X(1).
              05 L3411-TP-ADEG-PRE-NULL REDEFINES
                 L3411-TP-ADEG-PRE   PIC X(1).
              05 L3411-DT-VARZ-TP-IAS   PIC S9(8)V COMP-3.
              05 L3411-DT-VARZ-TP-IAS-NULL REDEFINES
                 L3411-DT-VARZ-TP-IAS   PIC X(5).
              05 L3411-FRAZ-DECR-CPT PIC S9(5)V     COMP-3.
              05 L3411-FRAZ-DECR-CPT-NULL REDEFINES
                 L3411-FRAZ-DECR-CPT   PIC X(3).
              05 L3411-COD-TRAT-RIASS PIC X(12).
              05 L3411-COD-TRAT-RIASS-NULL REDEFINES
                 L3411-COD-TRAT-RIASS   PIC X(12).
              05 L3411-TP-DT-EMIS-RIASS PIC X(2).
              05 L3411-TP-DT-EMIS-RIASS-NULL REDEFINES
                 L3411-TP-DT-EMIS-RIASS   PIC X(2).
              05 L3411-TP-CESS-RIASS PIC X(2).
              05 L3411-TP-CESS-RIASS-NULL REDEFINES
                 L3411-TP-CESS-RIASS   PIC X(2).
              05 L3411-DS-RIGA PIC S9(10)V     COMP-3.
              05 L3411-DS-OPER-SQL PIC X(1).
              05 L3411-DS-VER PIC S9(9)V     COMP-3.
              05 L3411-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
              05 L3411-DS-TS-END-CPTZ PIC S9(18)V     COMP-3.
              05 L3411-DS-UTENTE PIC X(20).
              05 L3411-DS-STATO-ELAB PIC X(1).
              05 L3411-AA-STAB PIC S9(5)V     COMP-3.
              05 L3411-AA-STAB-NULL REDEFINES
                 L3411-AA-STAB   PIC X(3).
              05 L3411-TS-STAB-LIMITATA PIC S9(5)V9(9) COMP-3.
              05 L3411-TS-STAB-LIMITATA-NULL REDEFINES
                 L3411-TS-STAB-LIMITATA   PIC X(8).
              05 L3411-DT-PRESC   PIC S9(8)V COMP-3.
              05 L3411-DT-PRESC-NULL REDEFINES
                 L3411-DT-PRESC   PIC X(5).
              05 L3411-RSH-INVST PIC X(1).
              05 L3411-RSH-INVST-NULL REDEFINES
                 L3411-RSH-INVST   PIC X(1).
              05 L3411-TP-RAMO-BILA PIC X(2).

           03 LDBV3411-STB-OUTPUT.
              07 L3411-TP-STAT-BUS PIC X(2).
              07 L3411-TP-CAUS     PIC X(2).

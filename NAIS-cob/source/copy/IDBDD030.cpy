           EXEC SQL DECLARE PROGR_NUM_OGG TABLE
           (
             COD_COMPAGNIA_ANIA  DECIMAL(5, 0) NOT NULL,
             FORMA_ASSICURATIVA  CHAR(2) NOT NULL,
             COD_OGGETTO         CHAR(30) NOT NULL,
             KEY_BUSINESS        CHAR(100) NOT NULL,
             ULT_PROGR           DECIMAL(18, 0) NOT NULL,
             PROGR_INIZIALE      DECIMAL(18, 0),
             PROGR_FINALE        DECIMAL(18, 0)
          ) END-EXEC.

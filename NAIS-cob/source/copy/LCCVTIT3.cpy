
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVTIT3
      *   ULTIMO AGG. 05 DIC 2014
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-TIT.
           MOVE TIT-ID-TIT-CONT
             TO (SF)-ID-PTF(IX-TAB-TIT)
           MOVE TIT-ID-TIT-CONT
             TO (SF)-ID-TIT-CONT(IX-TAB-TIT)
           MOVE TIT-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-TIT)
           MOVE TIT-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-TIT)
           IF TIT-IB-RICH-NULL = HIGH-VALUES
              MOVE TIT-IB-RICH-NULL
                TO (SF)-IB-RICH-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-IB-RICH
                TO (SF)-IB-RICH(IX-TAB-TIT)
           END-IF
           MOVE TIT-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-TIT)
           IF TIT-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE TIT-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-TIT)
           END-IF
           MOVE TIT-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-TIT)
           MOVE TIT-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-TIT)
           MOVE TIT-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-TIT)
           MOVE TIT-TP-TIT
             TO (SF)-TP-TIT(IX-TAB-TIT)
           IF TIT-PROG-TIT-NULL = HIGH-VALUES
              MOVE TIT-PROG-TIT-NULL
                TO (SF)-PROG-TIT-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-PROG-TIT
                TO (SF)-PROG-TIT(IX-TAB-TIT)
           END-IF
           MOVE TIT-TP-PRE-TIT
             TO (SF)-TP-PRE-TIT(IX-TAB-TIT)
           MOVE TIT-TP-STAT-TIT
             TO (SF)-TP-STAT-TIT(IX-TAB-TIT)
           IF TIT-DT-INI-COP-NULL = HIGH-VALUES
              MOVE TIT-DT-INI-COP-NULL
                TO (SF)-DT-INI-COP-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-DT-INI-COP
                TO (SF)-DT-INI-COP(IX-TAB-TIT)
           END-IF
           IF TIT-DT-END-COP-NULL = HIGH-VALUES
              MOVE TIT-DT-END-COP-NULL
                TO (SF)-DT-END-COP-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-DT-END-COP
                TO (SF)-DT-END-COP(IX-TAB-TIT)
           END-IF
           IF TIT-IMP-PAG-NULL = HIGH-VALUES
              MOVE TIT-IMP-PAG-NULL
                TO (SF)-IMP-PAG-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-IMP-PAG
                TO (SF)-IMP-PAG(IX-TAB-TIT)
           END-IF
           IF TIT-FL-SOLL-NULL = HIGH-VALUES
              MOVE TIT-FL-SOLL-NULL
                TO (SF)-FL-SOLL-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-FL-SOLL
                TO (SF)-FL-SOLL(IX-TAB-TIT)
           END-IF
           IF TIT-FRAZ-NULL = HIGH-VALUES
              MOVE TIT-FRAZ-NULL
                TO (SF)-FRAZ-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-FRAZ
                TO (SF)-FRAZ(IX-TAB-TIT)
           END-IF
           IF TIT-DT-APPLZ-MORA-NULL = HIGH-VALUES
              MOVE TIT-DT-APPLZ-MORA-NULL
                TO (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-DT-APPLZ-MORA
                TO (SF)-DT-APPLZ-MORA(IX-TAB-TIT)
           END-IF
           IF TIT-FL-MORA-NULL = HIGH-VALUES
              MOVE TIT-FL-MORA-NULL
                TO (SF)-FL-MORA-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-FL-MORA
                TO (SF)-FL-MORA(IX-TAB-TIT)
           END-IF
           IF TIT-ID-RAPP-RETE-NULL = HIGH-VALUES
              MOVE TIT-ID-RAPP-RETE-NULL
                TO (SF)-ID-RAPP-RETE-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-ID-RAPP-RETE
                TO (SF)-ID-RAPP-RETE(IX-TAB-TIT)
           END-IF
           IF TIT-ID-RAPP-ANA-NULL = HIGH-VALUES
              MOVE TIT-ID-RAPP-ANA-NULL
                TO (SF)-ID-RAPP-ANA-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-ID-RAPP-ANA
                TO (SF)-ID-RAPP-ANA(IX-TAB-TIT)
           END-IF
           IF TIT-COD-DVS-NULL = HIGH-VALUES
              MOVE TIT-COD-DVS-NULL
                TO (SF)-COD-DVS-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-COD-DVS
                TO (SF)-COD-DVS(IX-TAB-TIT)
           END-IF
           IF TIT-DT-EMIS-TIT-NULL = HIGH-VALUES
              MOVE TIT-DT-EMIS-TIT-NULL
                TO (SF)-DT-EMIS-TIT-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-DT-EMIS-TIT
                TO (SF)-DT-EMIS-TIT(IX-TAB-TIT)
           END-IF
           IF TIT-DT-ESI-TIT-NULL = HIGH-VALUES
              MOVE TIT-DT-ESI-TIT-NULL
                TO (SF)-DT-ESI-TIT-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-DT-ESI-TIT
                TO (SF)-DT-ESI-TIT(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-PRE-NET-NULL = HIGH-VALUES
              MOVE TIT-TOT-PRE-NET-NULL
                TO (SF)-TOT-PRE-NET-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-PRE-NET
                TO (SF)-TOT-PRE-NET(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-INTR-FRAZ-NULL = HIGH-VALUES
              MOVE TIT-TOT-INTR-FRAZ-NULL
                TO (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-INTR-FRAZ
                TO (SF)-TOT-INTR-FRAZ(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-INTR-MORA-NULL = HIGH-VALUES
              MOVE TIT-TOT-INTR-MORA-NULL
                TO (SF)-TOT-INTR-MORA-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-INTR-MORA
                TO (SF)-TOT-INTR-MORA(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-INTR-PREST-NULL = HIGH-VALUES
              MOVE TIT-TOT-INTR-PREST-NULL
                TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-INTR-PREST
                TO (SF)-TOT-INTR-PREST(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-INTR-RETDT-NULL = HIGH-VALUES
              MOVE TIT-TOT-INTR-RETDT-NULL
                TO (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-INTR-RETDT
                TO (SF)-TOT-INTR-RETDT(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-INTR-RIAT-NULL = HIGH-VALUES
              MOVE TIT-TOT-INTR-RIAT-NULL
                TO (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-INTR-RIAT
                TO (SF)-TOT-INTR-RIAT(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-DIR-NULL = HIGH-VALUES
              MOVE TIT-TOT-DIR-NULL
                TO (SF)-TOT-DIR-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-DIR
                TO (SF)-TOT-DIR(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-SPE-MED-NULL = HIGH-VALUES
              MOVE TIT-TOT-SPE-MED-NULL
                TO (SF)-TOT-SPE-MED-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-SPE-MED
                TO (SF)-TOT-SPE-MED(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-TAX-NULL = HIGH-VALUES
              MOVE TIT-TOT-TAX-NULL
                TO (SF)-TOT-TAX-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-TAX
                TO (SF)-TOT-TAX(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-SOPR-SAN-NULL = HIGH-VALUES
              MOVE TIT-TOT-SOPR-SAN-NULL
                TO (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-SOPR-SAN
                TO (SF)-TOT-SOPR-SAN(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-SOPR-TEC-NULL = HIGH-VALUES
              MOVE TIT-TOT-SOPR-TEC-NULL
                TO (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-SOPR-TEC
                TO (SF)-TOT-SOPR-TEC(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-SOPR-SPO-NULL = HIGH-VALUES
              MOVE TIT-TOT-SOPR-SPO-NULL
                TO (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-SOPR-SPO
                TO (SF)-TOT-SOPR-SPO(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-SOPR-PROF-NULL = HIGH-VALUES
              MOVE TIT-TOT-SOPR-PROF-NULL
                TO (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-SOPR-PROF
                TO (SF)-TOT-SOPR-PROF(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-SOPR-ALT-NULL = HIGH-VALUES
              MOVE TIT-TOT-SOPR-ALT-NULL
                TO (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-SOPR-ALT
                TO (SF)-TOT-SOPR-ALT(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-PRE-TOT-NULL = HIGH-VALUES
              MOVE TIT-TOT-PRE-TOT-NULL
                TO (SF)-TOT-PRE-TOT-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-PRE-TOT
                TO (SF)-TOT-PRE-TOT(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-PRE-PP-IAS-NULL = HIGH-VALUES
              MOVE TIT-TOT-PRE-PP-IAS-NULL
                TO (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-PRE-PP-IAS
                TO (SF)-TOT-PRE-PP-IAS(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-CAR-ACQ-NULL = HIGH-VALUES
              MOVE TIT-TOT-CAR-ACQ-NULL
                TO (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-CAR-ACQ
                TO (SF)-TOT-CAR-ACQ(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-CAR-GEST-NULL = HIGH-VALUES
              MOVE TIT-TOT-CAR-GEST-NULL
                TO (SF)-TOT-CAR-GEST-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-CAR-GEST
                TO (SF)-TOT-CAR-GEST(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-CAR-INC-NULL = HIGH-VALUES
              MOVE TIT-TOT-CAR-INC-NULL
                TO (SF)-TOT-CAR-INC-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-CAR-INC
                TO (SF)-TOT-CAR-INC(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-PRE-SOLO-RSH-NULL = HIGH-VALUES
              MOVE TIT-TOT-PRE-SOLO-RSH-NULL
                TO (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-PRE-SOLO-RSH
                TO (SF)-TOT-PRE-SOLO-RSH(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-PROV-ACQ-1AA-NULL = HIGH-VALUES
              MOVE TIT-TOT-PROV-ACQ-1AA-NULL
                TO (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-PROV-ACQ-1AA
                TO (SF)-TOT-PROV-ACQ-1AA(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-PROV-ACQ-2AA-NULL = HIGH-VALUES
              MOVE TIT-TOT-PROV-ACQ-2AA-NULL
                TO (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-PROV-ACQ-2AA
                TO (SF)-TOT-PROV-ACQ-2AA(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-PROV-RICOR-NULL = HIGH-VALUES
              MOVE TIT-TOT-PROV-RICOR-NULL
                TO (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-PROV-RICOR
                TO (SF)-TOT-PROV-RICOR(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-PROV-INC-NULL = HIGH-VALUES
              MOVE TIT-TOT-PROV-INC-NULL
                TO (SF)-TOT-PROV-INC-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-PROV-INC
                TO (SF)-TOT-PROV-INC(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-PROV-DA-REC-NULL = HIGH-VALUES
              MOVE TIT-TOT-PROV-DA-REC-NULL
                TO (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-PROV-DA-REC
                TO (SF)-TOT-PROV-DA-REC(IX-TAB-TIT)
           END-IF
           IF TIT-IMP-AZ-NULL = HIGH-VALUES
              MOVE TIT-IMP-AZ-NULL
                TO (SF)-IMP-AZ-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-IMP-AZ
                TO (SF)-IMP-AZ(IX-TAB-TIT)
           END-IF
           IF TIT-IMP-ADER-NULL = HIGH-VALUES
              MOVE TIT-IMP-ADER-NULL
                TO (SF)-IMP-ADER-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-IMP-ADER
                TO (SF)-IMP-ADER(IX-TAB-TIT)
           END-IF
           IF TIT-IMP-TFR-NULL = HIGH-VALUES
              MOVE TIT-IMP-TFR-NULL
                TO (SF)-IMP-TFR-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-IMP-TFR
                TO (SF)-IMP-TFR(IX-TAB-TIT)
           END-IF
           IF TIT-IMP-VOLO-NULL = HIGH-VALUES
              MOVE TIT-IMP-VOLO-NULL
                TO (SF)-IMP-VOLO-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-IMP-VOLO
                TO (SF)-IMP-VOLO(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-MANFEE-ANTIC-NULL = HIGH-VALUES
              MOVE TIT-TOT-MANFEE-ANTIC-NULL
                TO (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-MANFEE-ANTIC
                TO (SF)-TOT-MANFEE-ANTIC(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-MANFEE-RICOR-NULL = HIGH-VALUES
              MOVE TIT-TOT-MANFEE-RICOR-NULL
                TO (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-MANFEE-RICOR
                TO (SF)-TOT-MANFEE-RICOR(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-MANFEE-REC-NULL = HIGH-VALUES
              MOVE TIT-TOT-MANFEE-REC-NULL
                TO (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-MANFEE-REC
                TO (SF)-TOT-MANFEE-REC(IX-TAB-TIT)
           END-IF
           IF TIT-TP-MEZ-PAG-ADD-NULL = HIGH-VALUES
              MOVE TIT-TP-MEZ-PAG-ADD-NULL
                TO (SF)-TP-MEZ-PAG-ADD-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TP-MEZ-PAG-ADD
                TO (SF)-TP-MEZ-PAG-ADD(IX-TAB-TIT)
           END-IF
           IF TIT-ESTR-CNT-CORR-ADD-NULL = HIGH-VALUES
              MOVE TIT-ESTR-CNT-CORR-ADD-NULL
                TO (SF)-ESTR-CNT-CORR-ADD-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-ESTR-CNT-CORR-ADD
                TO (SF)-ESTR-CNT-CORR-ADD(IX-TAB-TIT)
           END-IF
           IF TIT-DT-VLT-NULL = HIGH-VALUES
              MOVE TIT-DT-VLT-NULL
                TO (SF)-DT-VLT-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-DT-VLT
                TO (SF)-DT-VLT(IX-TAB-TIT)
           END-IF
           IF TIT-FL-FORZ-DT-VLT-NULL = HIGH-VALUES
              MOVE TIT-FL-FORZ-DT-VLT-NULL
                TO (SF)-FL-FORZ-DT-VLT-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-FL-FORZ-DT-VLT
                TO (SF)-FL-FORZ-DT-VLT(IX-TAB-TIT)
           END-IF
           IF TIT-DT-CAMBIO-VLT-NULL = HIGH-VALUES
              MOVE TIT-DT-CAMBIO-VLT-NULL
                TO (SF)-DT-CAMBIO-VLT-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-DT-CAMBIO-VLT
                TO (SF)-DT-CAMBIO-VLT(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-SPE-AGE-NULL = HIGH-VALUES
              MOVE TIT-TOT-SPE-AGE-NULL
                TO (SF)-TOT-SPE-AGE-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-SPE-AGE
                TO (SF)-TOT-SPE-AGE(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-CAR-IAS-NULL = HIGH-VALUES
              MOVE TIT-TOT-CAR-IAS-NULL
                TO (SF)-TOT-CAR-IAS-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-CAR-IAS
                TO (SF)-TOT-CAR-IAS(IX-TAB-TIT)
           END-IF
           IF TIT-NUM-RAT-ACCORPATE-NULL = HIGH-VALUES
              MOVE TIT-NUM-RAT-ACCORPATE-NULL
                TO (SF)-NUM-RAT-ACCORPATE-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-NUM-RAT-ACCORPATE
                TO (SF)-NUM-RAT-ACCORPATE(IX-TAB-TIT)
           END-IF
           MOVE TIT-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-TIT)
           MOVE TIT-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-TIT)
           MOVE TIT-DS-VER
             TO (SF)-DS-VER(IX-TAB-TIT)
           MOVE TIT-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TIT)
           MOVE TIT-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-TIT)
           MOVE TIT-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-TIT)
           MOVE TIT-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-TIT)
           IF TIT-FL-TIT-DA-REINVST-NULL = HIGH-VALUES
              MOVE TIT-FL-TIT-DA-REINVST-NULL
                TO (SF)-FL-TIT-DA-REINVST-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-FL-TIT-DA-REINVST
                TO (SF)-FL-TIT-DA-REINVST(IX-TAB-TIT)
           END-IF
           IF TIT-DT-RICH-ADD-RID-NULL = HIGH-VALUES
              MOVE TIT-DT-RICH-ADD-RID-NULL
                TO (SF)-DT-RICH-ADD-RID-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-DT-RICH-ADD-RID
                TO (SF)-DT-RICH-ADD-RID(IX-TAB-TIT)
           END-IF
           IF TIT-TP-ESI-RID-NULL = HIGH-VALUES
              MOVE TIT-TP-ESI-RID-NULL
                TO (SF)-TP-ESI-RID-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TP-ESI-RID
                TO (SF)-TP-ESI-RID(IX-TAB-TIT)
           END-IF
           IF TIT-COD-IBAN-NULL = HIGH-VALUES
              MOVE TIT-COD-IBAN-NULL
                TO (SF)-COD-IBAN-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-COD-IBAN
                TO (SF)-COD-IBAN(IX-TAB-TIT)
           END-IF
           IF TIT-IMP-TRASFE-NULL = HIGH-VALUES
              MOVE TIT-IMP-TRASFE-NULL
                TO (SF)-IMP-TRASFE-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-IMP-TRASFE
                TO (SF)-IMP-TRASFE(IX-TAB-TIT)
           END-IF
           IF TIT-IMP-TFR-STRC-NULL = HIGH-VALUES
              MOVE TIT-IMP-TFR-STRC-NULL
                TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-IMP-TFR-STRC
                TO (SF)-IMP-TFR-STRC(IX-TAB-TIT)
           END-IF
           IF TIT-DT-CERT-FISC-NULL = HIGH-VALUES
              MOVE TIT-DT-CERT-FISC-NULL
                TO (SF)-DT-CERT-FISC-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-DT-CERT-FISC
                TO (SF)-DT-CERT-FISC(IX-TAB-TIT)
           END-IF
           IF TIT-TP-CAUS-STOR-NULL = HIGH-VALUES
              MOVE TIT-TP-CAUS-STOR-NULL
                TO (SF)-TP-CAUS-STOR-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TP-CAUS-STOR
                TO (SF)-TP-CAUS-STOR(IX-TAB-TIT)
           END-IF
           IF TIT-TP-CAUS-DISP-STOR-NULL = HIGH-VALUES
              MOVE TIT-TP-CAUS-DISP-STOR-NULL
                TO (SF)-TP-CAUS-DISP-STOR-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TP-CAUS-DISP-STOR
                TO (SF)-TP-CAUS-DISP-STOR(IX-TAB-TIT)
           END-IF
           IF TIT-TP-TIT-MIGRAZ-NULL = HIGH-VALUES
              MOVE TIT-TP-TIT-MIGRAZ-NULL
                TO (SF)-TP-TIT-MIGRAZ-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TP-TIT-MIGRAZ
                TO (SF)-TP-TIT-MIGRAZ(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-ACQ-EXP-NULL = HIGH-VALUES
              MOVE TIT-TOT-ACQ-EXP-NULL
                TO (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-ACQ-EXP
                TO (SF)-TOT-ACQ-EXP(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-REMUN-ASS-NULL = HIGH-VALUES
              MOVE TIT-TOT-REMUN-ASS-NULL
                TO (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-REMUN-ASS
                TO (SF)-TOT-REMUN-ASS(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE TIT-TOT-COMMIS-INTER-NULL
                TO (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-COMMIS-INTER
                TO (SF)-TOT-COMMIS-INTER(IX-TAB-TIT)
           END-IF
           IF TIT-TP-CAUS-RIMB-NULL = HIGH-VALUES
              MOVE TIT-TP-CAUS-RIMB-NULL
                TO (SF)-TP-CAUS-RIMB-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TP-CAUS-RIMB
                TO (SF)-TP-CAUS-RIMB(IX-TAB-TIT)
           END-IF
           IF TIT-TOT-CNBT-ANTIRAC-NULL = HIGH-VALUES
              MOVE TIT-TOT-CNBT-ANTIRAC-NULL
                TO (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-TOT-CNBT-ANTIRAC
                TO (SF)-TOT-CNBT-ANTIRAC(IX-TAB-TIT)
           END-IF
           IF TIT-FL-INC-AUTOGEN-NULL = HIGH-VALUES
              MOVE TIT-FL-INC-AUTOGEN-NULL
                TO (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TIT)
           ELSE
              MOVE TIT-FL-INC-AUTOGEN
                TO (SF)-FL-INC-AUTOGEN(IX-TAB-TIT)
           END-IF.
       VALORIZZA-OUTPUT-TIT-EX.
           EXIT.

       01 LDBVD511.
         05 LDBVD511-ID-POLI        PIC S9(9) COMP-3.
         05 LDBVD511-ID-ADES        PIC S9(9) COMP-3.
         05 LDBVD511-TP-STAT-BUS    PIC X(02).
         05 LDBVD511-TP-CAUS        PIC X(02).
         05 LDBVD511-TP-OGG         PIC X(02).

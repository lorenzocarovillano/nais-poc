       01 TRCH-DI-GAR-DB.
          05 TGA-DT-INI-EFF-DB     PIC X(10).
          05 TGA-DT-END-EFF-DB     PIC X(10).
          05 TGA-DT-DECOR-DB     PIC X(10).
          05 TGA-DT-SCAD-DB     PIC X(10).
          05 TGA-DT-EMIS-DB     PIC X(10).
          05 TGA-DT-EFF-STAB-DB     PIC X(10).
          05 TGA-DT-VLDT-PROD-DB     PIC X(10).
          05 TGA-DT-INI-VAL-TAR-DB     PIC X(10).
          05 TGA-DT-ULT-ADEG-PRE-PR-DB     PIC X(10).

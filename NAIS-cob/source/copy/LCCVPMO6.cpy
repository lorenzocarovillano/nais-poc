      *----------------------------------------------------------------*
      *    COPY      ..... LCCVPMO6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO PARAMETRO MOVIMENTO
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVPMO5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN STRATEGIA DI INVESTIMENTO (LCCVPMO1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-PARAM-MOVI.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE PARAM-MOVI

      *--> NOME TABELLA FISICA DB
           MOVE 'PARAM-MOVI'                 TO WK-TABELLA

           IF NOT WPMO-ST-INV(IX-TAB-PMO)
              AND NOT WPMO-ST-CON(IX-TAB-PMO)
              AND WPMO-ELE-PARAM-MOV-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WPMO-ST-ADD(IX-TAB-PMO)

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK

                        MOVE S090-SEQ-TABELLA TO WPMO-ID-PTF(IX-TAB-PMO)
                                                 PMO-ID-PARAM-MOVI

      *-->              PREPARAZIONE ED ESTRAZIONE OGGETTO PTF
                        PERFORM PREPARA-AREA-LCCS0234-PMO
                           THRU PREPARA-AREA-LCCS0234-PMO-EX

                        PERFORM CALL-LCCS0234
                           THRU CALL-LCCS0234-EX

                        IF IDSV0001-ESITO-OK
                           MOVE S234-ID-OGG-PTF-EOC   TO PMO-ID-OGG
                           MOVE S234-IB-OGG-PTF-EOC
                             TO WPMO-IB-OGG(IX-TAB-PMO)
                           MOVE WMOV-ID-PTF           TO PMO-ID-MOVI-CRZ
                           EVALUATE WPMO-TP-OGG(IX-TAB-PMO)
                               WHEN 'PO'
                                  MOVE S234-ID-POLI-PTF TO PMO-ID-POLI
                                  MOVE HIGH-VALUE  TO PMO-ID-ADES-NULL
                               WHEN 'AD'
                               WHEN 'GA'
                               WHEN 'TG'
                                  MOVE S234-ID-POLI-PTF TO PMO-ID-POLI
                                  MOVE S234-ID-ADES-PTF TO PMO-ID-ADES
                           END-EVALUATE
                        END-IF
                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WPMO-ST-MOD(IX-TAB-PMO)

                       MOVE WPMO-ID-PTF(IX-TAB-PMO)
                         TO PMO-ID-PARAM-MOVI
                       MOVE WPMO-ID-OGG(IX-TAB-PMO)
                         TO PMO-ID-OGG
                       MOVE WMOV-ID-PTF
                         TO PMO-ID-MOVI-CRZ

                       MOVE WPMO-ID-POLI(IX-TAB-PMO)
                         TO PMO-ID-POLI

                       IF WPMO-ID-ADES-NULL(IX-TAB-PMO) = HIGH-VALUES
                          MOVE HIGH-VALUES
                            TO PMO-ID-ADES-NULL
                       ELSE
                          MOVE WPMO-ID-ADES(IX-TAB-PMO)
                            TO PMO-ID-ADES
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WPMO-ST-DEL(IX-TAB-PMO)

                       MOVE WPMO-ID-PTF(IX-TAB-PMO)
                         TO PMO-ID-PARAM-MOVI
                       MOVE WPMO-ID-OGG(IX-TAB-PMO)
                         TO PMO-ID-OGG
                       MOVE WMOV-ID-PTF
                         TO PMO-ID-MOVI-CRZ
                            PMO-ID-MOVI-CHIU

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN ADESIONE
                 PERFORM VAL-DCLGEN-PMO
                    THRU VAL-DCLGEN-PMO-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-PMO
                    THRU VALORIZZA-AREA-DSH-PMO-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-PARAM-MOVI-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-PMO.

      *--> DCLGEN TABELLA
           MOVE PARAM-MOVI              TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-PMO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    PREPARA AREA PER CALL LCCS0234
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234-PMO.

             MOVE WPMO-ID-OGG(IX-TAB-PMO)      TO S234-ID-OGG-EOC.
             MOVE WPMO-TP-OGG(IX-TAB-PMO)      TO S234-TIPO-OGG-EOC.

       PREPARA-AREA-LCCS0234-PMO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVPMO5.

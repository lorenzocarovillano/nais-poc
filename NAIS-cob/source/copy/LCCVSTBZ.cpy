      *----------------------------------------------------------------*
      * DIMENSIONAMENTO OCCURS PER LA TAB. STAT_OGG_BUS                *
      * COPY RIPORTANTE IL NUMERO DI OCCURS DA UTILIZZARE              *
      * PER INIZIALIZZA TOT DELLA TABELLA                              *
      *----------------------------------------------------------------*
       01 WK-STB-MAX.
          03 WK-STB-MAX-A                 PIC 9(04) VALUE 100.
          03 WK-STB-MAX-B                 PIC 9(04) VALUE 1300.
          03 WK-STB-MAX-C                 PIC 9(04) VALUE 2000.

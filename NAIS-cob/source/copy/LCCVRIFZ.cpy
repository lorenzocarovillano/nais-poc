      *----------------------------------------------------------------*
      * DIMENSIONAMENTO OCCURS PER LA TAB. RICH_INVST_FND              *
      * COPY RIPORTANTE IL NUMERO DI OCCURS DA UTILIZZARE              *
      * PER INIZIALIZZA TOT DELLA TABELLA                              *
      *----------------------------------------------------------------*
       01 WK-RIF-MAX.
          03 WK-RIF-MAX-A                 PIC 9(04) VALUE 1250.

      *
      *----------------------------------------------------------------*
      *  GESTIONE DELLE DEROGHE ATTIVE PER LE FUNZIONALITA' SEGUENTI :
      *
      *  ADEGUAMENTO PREMIO PRESTAZIONE
      *
      *----------------------------------------------------------------*
      *
       LOAP0002-CONTROLLI.
      *
           PERFORM LOAP2-GESTIONE-DEROGA
              THRU LOAP2-GESTIONE-DEROGA-EX.
      *
       LOAP0002-CONTROLLI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * LETTURA E GESTIONE DELL'ULTIMA IMMAGINE DELLA DEROGA
      *----------------------------------------------------------------*
      *
       LOAP2-GESTIONE-DEROGA.

           SET IDSV0001-ESITO-OK  TO TRUE.
           SUBTRACT 1 FROM IDSV0001-MAX-ELE-ERRORI.

           PERFORM LOAP2-LEGGI-ULT-IMMAG-DER
              THRU LOAP2-LEGGI-ULT-IMMAG-DER-EX.

           IF IDSV0001-ESITO-OK

              PERFORM LOAP2-LEGGI-MOVI-DEROGA
                 THRU LOAP2-LEGGI-MOVI-DEROGA-EX
      *
              IF  IDSV0001-ESITO-OK

                  MOVE MOV-TP-MOVI  TO LOAC0002-TP-MOVI

                  SET LOAC0002-DEROGA-NON-TROVATA  TO TRUE

                  PERFORM VARYING LOAC0002-IND-R FROM 1 BY 1
                     UNTIL LOAC0002-IND-R > LOAC0002-ELE-MAX-FUNZ
                        OR LOAC0002-DEROGA-TROVATA
                     IF  LOAC0002-FUNZ-ESEC(LOAC0002-IND-R) =
                         IDSV0001-TIPO-MOVIMENTO
                         PERFORM VARYING LOAC0002-IND-C FROM 1 BY 1
12908 *                    UNTIL LOAC0002-IND-C > 30
12908                      UNTIL LOAC0002-IND-C > 43
                              OR LOAC0002-DEROGA-TROVATA
                           IF LOAC0002-FUNZ-DEROG
                              (LOAC0002-IND-R , LOAC0002-IND-C) =
                               LOAC0002-TP-MOVI
                              SET LOAC0002-DEROGA-TROVATA TO TRUE
                           END-IF
                         END-PERFORM
                     END-IF
                  END-PERFORM

                  IF  LOAC0002-DEROGA-TROVATA
                    SUBTRACT 1 FROM  LOAC0002-IND-R LOAC0002-IND-C
                    IF LOAC0002-TIPO-DEROGA
                    (LOAC0002-IND-R , LOAC0002-IND-C) = 'B'
                     MOVE LOAC0280-LIVELLO-DEROGA
                       TO WK-LIV-DEROGA
                     IF POLIZZA
                        MOVE WPOL-ID-POLI
                          TO WK-OGGETTO-9
                     ELSE
                        MOVE WADE-ID-ADES(1)
                          TO WK-OGGETTO-9
                     END-IF
                     MOVE WK-OGGETTO-9
                       TO WK-OGGETTO-X
                     MOVE WK-PGM
                       TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'LOAP1-S320-CALL-LOAS0280'
                       TO IEAI9901-LABEL-ERR
                     MOVE '005210'
                       TO IEAI9901-COD-ERRORE
                     STRING WK-LIV-DEROGA ';'
                            WK-OGGETTO-X ';'
                           'BLOCCANTE'
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
                    END-IF
                  END-IF
              END-IF
           END-IF.

       LOAP2-GESTIONE-DEROGA-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * LETTURA DELL'ULTIMA IMMAGINE DELLO STATO DELLA DEROGA          *
      *----------------------------------------------------------------*
       LOAP2-LEGGI-ULT-IMMAG-DER.

           MOVE 'LOAP2-LEGGI-ULT-IMMAG-DER' TO WK-LABEL-ERR.

           INITIALIZE IDSI0011-AREA.

           IF INDIVIDUALE

              SET POLIZZA
                TO TRUE
              MOVE WS-TP-OGG
                TO LDBV3361-TP-OGG
              MOVE WPOL-ID-POLI
                TO LDBV3361-ID-OGG

           ELSE

              SET ADESIONE
                TO TRUE
              MOVE WS-TP-OGG
                TO LDBV3361-TP-OGG
             MOVE WADE-ID-ADES(1)
               TO LDBV3361-ID-OGG

           END-IF.

           MOVE WS-DT-INFINITO-1-N TO IDSI0011-DATA-INIZIO-EFFETTO
                                      IDSI0011-DATA-FINE-EFFETTO.
           MOVE WS-TS-INFINITO-1-N TO IDSI0011-DATA-COMPETENZA.

           SET  IDSI0011-TRATT-DEFAULT
             TO TRUE.

           MOVE LDBS3360
             TO IDSI0011-CODICE-STR-DATO.

           MOVE LDBV3361
             TO IDSI0011-BUFFER-WHERE-COND

           SET IDSI0011-WHERE-CONDITION
             TO TRUE

           SET IDSI0011-SELECT
             TO TRUE.

           SET IDSO0011-SUCCESSFUL-RC
             TO TRUE.

           SET IDSO0011-SUCCESSFUL-SQL
             TO TRUE.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.
      *
           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE

                       MOVE IDSO0011-BUFFER-DATI TO OGG-DEROGA

                  WHEN OTHER
      *--> ERRORE DI ACCESSO AL DB
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING LDBS3360  ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *--> ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '005056'
                TO IEAI9901-COD-ERRORE
              STRING LDBS3360  ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                DELIMITED BY SIZE
                INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.
      *
       LOAP2-LEGGI-ULT-IMMAG-DER-EX.
              EXIT.
      *----------------------------------------------------------------*
      * LETTURA DEL MOVIMENTO CHE HA CREATO LA DEROGA                  *
      *----------------------------------------------------------------*
       LOAP2-LEGGI-MOVI-DEROGA.
           MOVE 'LOAP2-LEGGI-MOVI-DEROGA' TO WK-LABEL-ERR.
           PERFORM DISPLAY-LABEL THRU DISPLAY-LABEL-EX.
           MOVE 'MOVI'          TO WK-TABELLA.

           INITIALIZE                       MOVI.
           SET IDSI0011-SELECT           TO TRUE.
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
           SET IDSI0011-PRIMARY-KEY      TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           MOVE ODE-ID-MOVI-CRZ          TO MOV-ID-MOVI.
           MOVE 'MOVI'                   TO IDSI0011-CODICE-STR-DATO.
           MOVE MOVI                     TO IDSI0011-BUFFER-DATI.
           MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

              IF IDSO0011-SUCCESSFUL-RC
                EVALUATE TRUE
                     WHEN IDSO0011-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                        MOVE IDSO0011-BUFFER-DATI
                          TO MOVI
                     WHEN IDSO0011-NOT-FOUND
      *--->          CAMPO $ NON TROVATO
                            MOVE WK-PGM
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE WK-LABEL-ERR
                             TO IEAI9901-LABEL-ERR
                           MOVE '005019'
                             TO IEAI9901-COD-ERRORE
                           MOVE 'ID-MOVI'
                             TO IEAI9901-PARAMETRI-ERR
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                      WHEN OTHER
      *--->          ERRORE DI ACCESSO AL DB
                        MOVE WK-PGM
                                        TO IEAI9901-COD-SERVIZIO-BE
                        MOVE WK-LABEL-ERR
                                        TO IEAI9901-LABEL-ERR
                        MOVE '005015'   TO IEAI9901-COD-ERRORE
                        MOVE WK-TABELLA TO IEAI9901-PARAMETRI-ERR
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                           THRU EX-S0300
                 END-EVALUATE
              ELSE
      *-->       GESTIRE ERRORE DISPATCHER
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR
                   TO IEAI9901-LABEL-ERR
                 MOVE '005016'
                   TO IEAI9901-COD-ERRORE
                 MOVE SPACES
                   TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF.

       LOAP2-LEGGI-MOVI-DEROGA-EX.
              EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVP614
      *   ULTIMO AGG. 24 OTT 2019
      *------------------------------------------------------------

       INIZIA-TOT-P61.

           PERFORM INIZIA-ZEROES-P61 THRU INIZIA-ZEROES-P61-EX

           PERFORM INIZIA-SPACES-P61 THRU INIZIA-SPACES-P61-EX

           PERFORM INIZIA-NULL-P61 THRU INIZIA-NULL-P61-EX.

       INIZIA-TOT-P61-EX.
           EXIT.

       INIZIA-NULL-P61.
           MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL
           MOVE HIGH-VALUES TO (SF)-RIS-MAT-31122011-NULL
           MOVE HIGH-VALUES TO (SF)-PRE-V-31122011-NULL
           MOVE HIGH-VALUES TO (SF)-PRE-RSH-V-31122011-NULL
           MOVE HIGH-VALUES TO (SF)-CPT-RIVTO-31122011-NULL
           MOVE HIGH-VALUES TO (SF)-IMPB-VIS-31122011-NULL
           MOVE HIGH-VALUES TO (SF)-IMPB-IS-31122011-NULL
           MOVE HIGH-VALUES TO (SF)-IMPB-VIS-RP-P2011-NULL
           MOVE HIGH-VALUES TO (SF)-IMPB-IS-RP-P2011-NULL
           MOVE HIGH-VALUES TO (SF)-PRE-V-30062014-NULL
           MOVE HIGH-VALUES TO (SF)-PRE-RSH-V-30062014-NULL
           MOVE HIGH-VALUES TO (SF)-CPT-INI-30062014-NULL
           MOVE HIGH-VALUES TO (SF)-IMPB-VIS-30062014-NULL
           MOVE HIGH-VALUES TO (SF)-IMPB-IS-30062014-NULL
           MOVE HIGH-VALUES TO (SF)-IMPB-VIS-RP-P62014-NULL
           MOVE HIGH-VALUES TO (SF)-IMPB-IS-RP-P62014-NULL
           MOVE HIGH-VALUES TO (SF)-RIS-MAT-30062014-NULL
           MOVE HIGH-VALUES TO (SF)-ID-ADES-NULL
           MOVE HIGH-VALUES TO (SF)-MONT-LRD-END2000-NULL
           MOVE HIGH-VALUES TO (SF)-PRE-LRD-END2000-NULL
           MOVE HIGH-VALUES TO (SF)-RENDTO-LRD-END2000-NULL
           MOVE HIGH-VALUES TO (SF)-MONT-LRD-END2006-NULL
           MOVE HIGH-VALUES TO (SF)-PRE-LRD-END2006-NULL
           MOVE HIGH-VALUES TO (SF)-RENDTO-LRD-END2006-NULL
           MOVE HIGH-VALUES TO (SF)-MONT-LRD-DAL2007-NULL
           MOVE HIGH-VALUES TO (SF)-PRE-LRD-DAL2007-NULL
           MOVE HIGH-VALUES TO (SF)-RENDTO-LRD-DAL2007-NULL
           MOVE HIGH-VALUES TO (SF)-ID-TRCH-DI-GAR-NULL.
       INIZIA-NULL-P61-EX.
           EXIT.

       INIZIA-ZEROES-P61.
           MOVE 0 TO (SF)-ID-D-CRIST
           MOVE 0 TO (SF)-ID-POLI
           MOVE 0 TO (SF)-COD-COMP-ANIA
           MOVE 0 TO (SF)-ID-MOVI-CRZ
           MOVE 0 TO (SF)-DT-INI-EFF
           MOVE 0 TO (SF)-DT-END-EFF
           MOVE 0 TO (SF)-DT-DECOR
           MOVE 0 TO (SF)-DS-RIGA
           MOVE 0 TO (SF)-DS-VER
           MOVE 0 TO (SF)-DS-TS-INI-CPTZ
           MOVE 0 TO (SF)-DS-TS-END-CPTZ.
       INIZIA-ZEROES-P61-EX.
           EXIT.

       INIZIA-SPACES-P61.
           MOVE SPACES TO (SF)-COD-PROD
           MOVE SPACES TO (SF)-DS-OPER-SQL
           MOVE SPACES TO (SF)-DS-UTENTE
           MOVE SPACES TO (SF)-DS-STATO-ELAB.
       INIZIA-SPACES-P61-EX.
           EXIT.

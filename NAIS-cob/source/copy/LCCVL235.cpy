
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVL235
      *   ULTIMO AGG. 31 MAG 2010
      *------------------------------------------------------------

       VAL-DCLGEN-L23.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-L23)
              TO L23-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-L23)
              TO L23-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-L23) NOT NUMERIC
              MOVE 0 TO L23-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-L23)
              TO L23-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-L23) NOT NUMERIC
              MOVE 0 TO L23-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-L23)
              TO L23-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-L23)
              TO L23-COD-COMP-ANIA
           MOVE (SF)-ID-RAPP-ANA(IX-TAB-L23)
              TO L23-ID-RAPP-ANA
           IF (SF)-TP-VINC-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-TP-VINC-NULL(IX-TAB-L23)
              TO L23-TP-VINC-NULL
           ELSE
              MOVE (SF)-TP-VINC(IX-TAB-L23)
              TO L23-TP-VINC
           END-IF
           IF (SF)-FL-DELEGA-AL-RISC-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-FL-DELEGA-AL-RISC-NULL(IX-TAB-L23)
              TO L23-FL-DELEGA-AL-RISC-NULL
           ELSE
              MOVE (SF)-FL-DELEGA-AL-RISC(IX-TAB-L23)
              TO L23-FL-DELEGA-AL-RISC
           END-IF
           MOVE (SF)-DESC(IX-TAB-L23)
              TO L23-DESC
           IF (SF)-DT-ATTIV-VINPG-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-DT-ATTIV-VINPG-NULL(IX-TAB-L23)
              TO L23-DT-ATTIV-VINPG-NULL
           ELSE
             IF (SF)-DT-ATTIV-VINPG(IX-TAB-L23) = ZERO
                MOVE HIGH-VALUES
                TO L23-DT-ATTIV-VINPG-NULL
             ELSE
              MOVE (SF)-DT-ATTIV-VINPG(IX-TAB-L23)
              TO L23-DT-ATTIV-VINPG
             END-IF
           END-IF
           IF (SF)-CPT-VINCTO-PIGN-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-CPT-VINCTO-PIGN-NULL(IX-TAB-L23)
              TO L23-CPT-VINCTO-PIGN-NULL
           ELSE
              MOVE (SF)-CPT-VINCTO-PIGN(IX-TAB-L23)
              TO L23-CPT-VINCTO-PIGN
           END-IF
           IF (SF)-DT-CHIU-VINPG-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-DT-CHIU-VINPG-NULL(IX-TAB-L23)
              TO L23-DT-CHIU-VINPG-NULL
           ELSE
             IF (SF)-DT-CHIU-VINPG(IX-TAB-L23) = ZERO
                MOVE HIGH-VALUES
                TO L23-DT-CHIU-VINPG-NULL
             ELSE
              MOVE (SF)-DT-CHIU-VINPG(IX-TAB-L23)
              TO L23-DT-CHIU-VINPG
             END-IF
           END-IF
           IF (SF)-VAL-RISC-INI-VINPG-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-VAL-RISC-INI-VINPG-NULL(IX-TAB-L23)
              TO L23-VAL-RISC-INI-VINPG-NULL
           ELSE
              MOVE (SF)-VAL-RISC-INI-VINPG(IX-TAB-L23)
              TO L23-VAL-RISC-INI-VINPG
           END-IF
           IF (SF)-VAL-RISC-END-VINPG-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-VAL-RISC-END-VINPG-NULL(IX-TAB-L23)
              TO L23-VAL-RISC-END-VINPG-NULL
           ELSE
              MOVE (SF)-VAL-RISC-END-VINPG(IX-TAB-L23)
              TO L23-VAL-RISC-END-VINPG
           END-IF
           IF (SF)-SOM-PRE-VINPG-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-SOM-PRE-VINPG-NULL(IX-TAB-L23)
              TO L23-SOM-PRE-VINPG-NULL
           ELSE
              MOVE (SF)-SOM-PRE-VINPG(IX-TAB-L23)
              TO L23-SOM-PRE-VINPG
           END-IF
           IF (SF)-FL-VINPG-INT-PRSTZ-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-FL-VINPG-INT-PRSTZ-NULL(IX-TAB-L23)
              TO L23-FL-VINPG-INT-PRSTZ-NULL
           ELSE
              MOVE (SF)-FL-VINPG-INT-PRSTZ(IX-TAB-L23)
              TO L23-FL-VINPG-INT-PRSTZ
           END-IF
           MOVE (SF)-DESC-AGG-VINC(IX-TAB-L23)
              TO L23-DESC-AGG-VINC
           IF (SF)-DS-RIGA(IX-TAB-L23) NOT NUMERIC
              MOVE 0 TO L23-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-L23)
              TO L23-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-L23)
              TO L23-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-L23) NOT NUMERIC
              MOVE 0 TO L23-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-L23)
              TO L23-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-L23) NOT NUMERIC
              MOVE 0 TO L23-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-L23)
              TO L23-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-L23) NOT NUMERIC
              MOVE 0 TO L23-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-L23)
              TO L23-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-L23)
              TO L23-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-L23)
              TO L23-DS-STATO-ELAB
           IF (SF)-TP-AUT-SEQ-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-TP-AUT-SEQ-NULL(IX-TAB-L23)
              TO L23-TP-AUT-SEQ-NULL
           ELSE
              MOVE (SF)-TP-AUT-SEQ(IX-TAB-L23)
              TO L23-TP-AUT-SEQ
           END-IF
           IF (SF)-COD-UFF-SEQ-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-COD-UFF-SEQ-NULL(IX-TAB-L23)
              TO L23-COD-UFF-SEQ-NULL
           ELSE
              MOVE (SF)-COD-UFF-SEQ(IX-TAB-L23)
              TO L23-COD-UFF-SEQ
           END-IF
           IF (SF)-NUM-PROVV-SEQ-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-NUM-PROVV-SEQ-NULL(IX-TAB-L23)
              TO L23-NUM-PROVV-SEQ-NULL
           ELSE
              MOVE (SF)-NUM-PROVV-SEQ(IX-TAB-L23)
              TO L23-NUM-PROVV-SEQ
           END-IF
           IF (SF)-TP-PROVV-SEQ-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-TP-PROVV-SEQ-NULL(IX-TAB-L23)
              TO L23-TP-PROVV-SEQ-NULL
           ELSE
              MOVE (SF)-TP-PROVV-SEQ(IX-TAB-L23)
              TO L23-TP-PROVV-SEQ
           END-IF
           IF (SF)-DT-PROVV-SEQ-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-DT-PROVV-SEQ-NULL(IX-TAB-L23)
              TO L23-DT-PROVV-SEQ-NULL
           ELSE
             IF (SF)-DT-PROVV-SEQ(IX-TAB-L23) = ZERO
                MOVE HIGH-VALUES
                TO L23-DT-PROVV-SEQ-NULL
             ELSE
              MOVE (SF)-DT-PROVV-SEQ(IX-TAB-L23)
              TO L23-DT-PROVV-SEQ
             END-IF
           END-IF
           IF (SF)-DT-NOTIFICA-BLOCCO-NULL(IX-TAB-L23) = HIGH-VALUES
              MOVE (SF)-DT-NOTIFICA-BLOCCO-NULL(IX-TAB-L23)
              TO L23-DT-NOTIFICA-BLOCCO-NULL
           ELSE
             IF (SF)-DT-NOTIFICA-BLOCCO(IX-TAB-L23) = ZERO
                MOVE HIGH-VALUES
                TO L23-DT-NOTIFICA-BLOCCO-NULL
             ELSE
              MOVE (SF)-DT-NOTIFICA-BLOCCO(IX-TAB-L23)
              TO L23-DT-NOTIFICA-BLOCCO
             END-IF
           END-IF
           MOVE (SF)-NOTA-PROVV(IX-TAB-L23)
              TO L23-NOTA-PROVV.
       VAL-DCLGEN-L23-EX.
           EXIT.

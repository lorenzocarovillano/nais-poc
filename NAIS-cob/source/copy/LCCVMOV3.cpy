
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVMOV3
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-MOV.
           MOVE MOV-ID-MOVI
             TO (SF)-ID-PTF
           MOVE MOV-ID-MOVI
             TO (SF)-ID-MOVI
           MOVE MOV-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           IF MOV-ID-OGG-NULL = HIGH-VALUES
              MOVE MOV-ID-OGG-NULL
                TO (SF)-ID-OGG-NULL
           ELSE
              MOVE MOV-ID-OGG
                TO (SF)-ID-OGG
           END-IF
           IF MOV-IB-OGG-NULL = HIGH-VALUES
              MOVE MOV-IB-OGG-NULL
                TO (SF)-IB-OGG-NULL
           ELSE
              MOVE MOV-IB-OGG
                TO (SF)-IB-OGG
           END-IF
           IF MOV-IB-MOVI-NULL = HIGH-VALUES
              MOVE MOV-IB-MOVI-NULL
                TO (SF)-IB-MOVI-NULL
           ELSE
              MOVE MOV-IB-MOVI
                TO (SF)-IB-MOVI
           END-IF
           IF MOV-TP-OGG-NULL = HIGH-VALUES
              MOVE MOV-TP-OGG-NULL
                TO (SF)-TP-OGG-NULL
           ELSE
              MOVE MOV-TP-OGG
                TO (SF)-TP-OGG
           END-IF
           IF MOV-ID-RICH-NULL = HIGH-VALUES
              MOVE MOV-ID-RICH-NULL
                TO (SF)-ID-RICH-NULL
           ELSE
              MOVE MOV-ID-RICH
                TO (SF)-ID-RICH
           END-IF
           IF MOV-TP-MOVI-NULL = HIGH-VALUES
              MOVE MOV-TP-MOVI-NULL
                TO (SF)-TP-MOVI-NULL
           ELSE
              MOVE MOV-TP-MOVI
                TO (SF)-TP-MOVI
           END-IF
           MOVE MOV-DT-EFF
             TO (SF)-DT-EFF
           IF MOV-ID-MOVI-ANN-NULL = HIGH-VALUES
              MOVE MOV-ID-MOVI-ANN-NULL
                TO (SF)-ID-MOVI-ANN-NULL
           ELSE
              MOVE MOV-ID-MOVI-ANN
                TO (SF)-ID-MOVI-ANN
           END-IF
           IF MOV-ID-MOVI-COLLG-NULL = HIGH-VALUES
              MOVE MOV-ID-MOVI-COLLG-NULL
                TO (SF)-ID-MOVI-COLLG-NULL
           ELSE
              MOVE MOV-ID-MOVI-COLLG
                TO (SF)-ID-MOVI-COLLG
           END-IF
           MOVE MOV-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE MOV-DS-VER
             TO (SF)-DS-VER
           MOVE MOV-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE MOV-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE MOV-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB.
       VALORIZZA-OUTPUT-MOV-EX.
           EXIT.

      *--  Macro funzione
           03 LCCV0005-MACRO-FUNZIONE         PIC X(02).
              88 LCCV0005-VENDITA               VALUE 'VE'.
              88 LCCV0005-VARIAZIONE            VALUE 'VG'.
              88 LCCV0005-STORNI                VALUE 'ST'.
              88 LCCV0005-OPER-AUTOM            VALUE 'OA'.
              88 LCCV0005-LEGGE-BILANCIO        VALUE 'LB'.
              88 LCCV0005-CONTABILITA           VALUE 'CT'.
              88 LCCV0005-OPZIONI               VALUE 'OP'.
              88 LCCV0005-PRESTITI              VALUE 'PR'.
              88 LCCV0005-RID-RIATT             VALUE 'RR'.
              88 LCCV0005-COASSICURAZIONE       VALUE 'CS'.
              88 LCCV0005-PAGAMENTI             VALUE 'PA'.
              88 LCCV0005-PREVENTIVAZIONE       VALUE 'PV'.
              88 LCCV0005-ANNULLI               VALUE 'AN'.
              88 LCCV0005-RENDITE               VALUE 'RE'.
      *--  Flag gestione scrittura richiesta
           03 LCCV0005-RICHIESTA              PIC X(01) VALUE 'N'.
              88 LCCV0005-RICHIESTA-SI          VALUE 'S'.
              88 LCCV0005-RICHIESTA-NO          VALUE 'N'.

      **************************************************************
      * COPY X GESTIONE GUIDA DA SEQUENZIALE
      * N.B. - DA UTILIZZARE CON LA COPY IABPSQG1
      **************************************************************

       77  FS-SEQGUIDE             PIC X(002)  VALUE '00'.

       77  NOME-SEQGUIDE           PIC X(008)  VALUE 'SEQGUIDE'.



      ******************************************************************
      * FLAG DI CONTROLLO x FILES SEQUENZIALI
      ******************************************************************
       01 STATO-SEQGUIDE           PIC X(01) VALUE 'C'.
          88 SEQGUIDE-APERTO       VALUE 'A'.
          88 SEQGUIDE-CHIUSO       VALUE 'C'.


      ******************************************************************
      * FLAG X OPEN FILES SEQUENZIALI
      ******************************************************************
       01 FLAG-OPEN-SEQGUIDE            PIC X(01) VALUE 'N'.
          88 OPEN-SEQGUIDE-SI      VALUE 'S'.
          88 OPEN-SEQGUIDE-NO      VALUE 'N'.


      ******************************************************************
      * FLAG X READ FILES SEQUENZIALI
      ******************************************************************
       01 FLAG-READ-SEQGUIDE            PIC X(01) VALUE 'N'.
          88 READ-SEQGUIDE-SI      VALUE 'S'.
          88 READ-SEQGUIDE-NO      VALUE 'N'.


      ******************************************************************
      * FLAG X CLOSE FILES SEQUENZIALI
      ******************************************************************
       01 FLAG-CLOSE-SEQGUIDE            PIC X(01) VALUE 'N'.
          88 CLOSE-SEQGUIDE-SI      VALUE 'S'.
          88 CLOSE-SEQGUIDE-NO      VALUE 'N'.



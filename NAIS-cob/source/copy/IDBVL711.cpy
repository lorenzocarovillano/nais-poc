       01 MATR-ELAB-BATCH.
         05 L71-ID-MATR-ELAB-BATCH PIC S9(9)V     COMP-3.
         05 L71-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 L71-TP-FRM-ASSVA PIC X(2).
         05 L71-TP-MOVI PIC S9(5)V     COMP-3.
         05 L71-COD-RAMO PIC X(12).
         05 L71-COD-RAMO-NULL REDEFINES
            L71-COD-RAMO   PIC X(12).
         05 L71-DT-ULT-ELAB   PIC S9(8)V COMP-3.
         05 L71-DS-OPER-SQL PIC X(1).
         05 L71-DS-VER PIC S9(9)V     COMP-3.
         05 L71-DS-TS-CPTZ PIC S9(18)V     COMP-3.
         05 L71-DS-UTENTE PIC X(20).
         05 L71-DS-STATO-ELAB PIC X(1).


      *----------------------------------------------------------------*
      *    COPY      ..... LCCVB036
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO BIL_ESTRATTI
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVB035 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN VINCOLO PEGNO (LCCVB031)
      *
      *----------------------------------------------------------------*
       SCRIVI-BIL-ESTRATTI.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE BILA-TRCH-ESTR.

      *--> SE LO STATUS NON E' "INVARIATO" O "CONVERSAZIONE"
           IF  NOT WB03-ST-INV
           AND NOT WB03-ST-CON
           AND WB03-ELE-B03-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WB03-ST-ADD

      *-->             ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA
                            TO WB03-ID-BILA-TRCH-ESTR
                       END-IF

      *-->        TIPO OPERAZIONE DISPATCHER
                       SET IDSI0011-INSERT TO TRUE

      *-->        MODIFICA
                  WHEN WB03-ST-MOD


                       MOVE WB03-ID-BILA-TRCH-ESTR
                         TO B03-ID-BILA-TRCH-ESTR

      *-->        TIPO OPERAZIONE DISPATCHER
                       SET IDSI0011-UPDATE TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WB03-ST-DEL

                       MOVE WB03-ID-BILA-TRCH-ESTR
                         TO B03-ID-BILA-TRCH-ESTR

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

      *-->    VALORIZZA DCLGEN ADESIONE
              PERFORM VAL-DCLGEN-B03
                 THRU VAL-DCLGEN-B03-EX

      *-->    VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
              PERFORM VALORIZZA-AREA-DSH-B03
                 THRU VALORIZZA-AREA-DSH-B03-EX

      *-->    CALL DISPATCHER PER AGGIORNAMENTO
              PERFORM AGGIORNA-TABELLA
                 THRU AGGIORNA-TABELLA-EX

           END-IF.

       SCRIVI-BIL-ESTRATTI-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-B03.

      *--> NOME TABELLA FISICA DB
           MOVE 'BILA-TRCH-ESTR'         TO WK-TABELLA.

      *--> DCLGEN TABELLA
           MOVE BILA-TRCH-ESTR           TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-PRIMARY-KEY        TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-SENZA-STOR  TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE IDSV0001-DATA-EFFETTO   TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-B03-EX.
           EXIT.

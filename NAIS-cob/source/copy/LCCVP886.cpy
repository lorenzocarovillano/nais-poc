      *----------------------------------------------------------------*
      *    COPY      ..... LCCVP886
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO ATTIVAZIONE SERVIZI A VALORE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-ATT-SERV-VAL.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE ATT-SERV-VAL
      *--> NOME TABELLA FISICA DB
           MOVE 'ATT-SERV-VAL'                    TO WK-TABELLA
      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WP88-ST-INV(IX-TAB-P88)
              AND NOT WP88-ST-CON(IX-TAB-P88)
              AND WP88-ELE-SERV-VAL-MAX NOT = 0

      *--->   Impostare ID Tabella RAPPORTO ANAGRAFICO
              MOVE WMOV-ID-PTF              TO P88-ID-MOVI-CRZ

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WP88-ST-ADD(IX-TAB-P88)

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK

                        MOVE S090-SEQ-TABELLA
                          TO WP88-ID-PTF(IX-TAB-P88)
                             P88-ID-ATT-SERV-VAL

      *-->               PREPARAZIONE ED ESTRAZIONE OGGETTO PTF
                         PERFORM PREPARA-AREA-LCCS0234-SVAL
                            THRU PREPARA-AREA-LCCS0234-SVAL-EX
                         PERFORM CALL-LCCS0234
                            THRU CALL-LCCS0234-EX

                         IF IDSV0001-ESITO-OK
                            MOVE S234-ID-OGG-PTF-EOC
                              TO P88-ID-OGG
                         END-IF


      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
                     END-IF

      *-->        CANCELLAZIONE
                  WHEN WP88-ST-DEL(IX-TAB-P88)

                       MOVE WP88-ID-PTF(IX-TAB-P88)
                         TO P88-ID-ATT-SERV-VAL
                       MOVE WP88-ID-OGG(IX-TAB-P88)
                         TO P88-ID-OGG

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->        MODIFICA
                  WHEN WP88-ST-MOD(IX-TAB-P88)
                       MOVE WP88-ID-PTF(IX-TAB-P88)
                         TO P88-ID-ATT-SERV-VAL
                       MOVE WP88-ID-OGG(IX-TAB-P88)
                         TO P88-ID-OGG

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN ATTIVAZIONE SERVIZI A VALORE
                 PERFORM VAL-DCLGEN-P88
                    THRU VAL-DCLGEN-P88-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-P88
                    THRU VALORIZZA-AREA-DSH-P88-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-ATT-SERV-VAL-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-P88.

      *--> DCLGEN TABELLA
           MOVE ATT-SERV-VAL           TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-P88-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   PREPARA PER LA CALL ALL'LCCS0234 ESTRATTORE OGGETTO PTF
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234-SVAL.

           MOVE WP88-ID-OGG(IX-TAB-P88)     TO S234-ID-OGG-EOC
           MOVE WP88-TP-OGG(IX-TAB-P88)     TO S234-TIPO-OGG-EOC.

       PREPARA-AREA-LCCS0234-SVAL-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVP885.

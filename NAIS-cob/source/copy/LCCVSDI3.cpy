
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVSDI3
      *   ULTIMO AGG. 07 DIC 2017
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-SDI.
           MOVE SDI-ID-STRA-DI-INVST
             TO (SF)-ID-PTF(IX-TAB-SDI)
           MOVE SDI-ID-STRA-DI-INVST
             TO (SF)-ID-STRA-DI-INVST(IX-TAB-SDI)
           MOVE SDI-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-SDI)
           MOVE SDI-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-SDI)
           MOVE SDI-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-SDI)
           IF SDI-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE SDI-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-SDI)
           ELSE
              MOVE SDI-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-SDI)
           END-IF
           MOVE SDI-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-SDI)
           MOVE SDI-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-SDI)
           MOVE SDI-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-SDI)
           IF SDI-COD-STRA-NULL = HIGH-VALUES
              MOVE SDI-COD-STRA-NULL
                TO (SF)-COD-STRA-NULL(IX-TAB-SDI)
           ELSE
              MOVE SDI-COD-STRA
                TO (SF)-COD-STRA(IX-TAB-SDI)
           END-IF
           IF SDI-MOD-GEST-NULL = HIGH-VALUES
              MOVE SDI-MOD-GEST-NULL
                TO (SF)-MOD-GEST-NULL(IX-TAB-SDI)
           ELSE
              MOVE SDI-MOD-GEST
                TO (SF)-MOD-GEST(IX-TAB-SDI)
           END-IF
           IF SDI-VAR-RIFTO-NULL = HIGH-VALUES
              MOVE SDI-VAR-RIFTO-NULL
                TO (SF)-VAR-RIFTO-NULL(IX-TAB-SDI)
           ELSE
              MOVE SDI-VAR-RIFTO
                TO (SF)-VAR-RIFTO(IX-TAB-SDI)
           END-IF
           IF SDI-VAL-VAR-RIFTO-NULL = HIGH-VALUES
              MOVE SDI-VAL-VAR-RIFTO-NULL
                TO (SF)-VAL-VAR-RIFTO-NULL(IX-TAB-SDI)
           ELSE
              MOVE SDI-VAL-VAR-RIFTO
                TO (SF)-VAL-VAR-RIFTO(IX-TAB-SDI)
           END-IF
           IF SDI-DT-FIS-NULL = HIGH-VALUES
              MOVE SDI-DT-FIS-NULL
                TO (SF)-DT-FIS-NULL(IX-TAB-SDI)
           ELSE
              MOVE SDI-DT-FIS
                TO (SF)-DT-FIS(IX-TAB-SDI)
           END-IF
           IF SDI-FRQ-VALUT-NULL = HIGH-VALUES
              MOVE SDI-FRQ-VALUT-NULL
                TO (SF)-FRQ-VALUT-NULL(IX-TAB-SDI)
           ELSE
              MOVE SDI-FRQ-VALUT
                TO (SF)-FRQ-VALUT(IX-TAB-SDI)
           END-IF
           IF SDI-FL-INI-END-PER-NULL = HIGH-VALUES
              MOVE SDI-FL-INI-END-PER-NULL
                TO (SF)-FL-INI-END-PER-NULL(IX-TAB-SDI)
           ELSE
              MOVE SDI-FL-INI-END-PER
                TO (SF)-FL-INI-END-PER(IX-TAB-SDI)
           END-IF
           MOVE SDI-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-SDI)
           MOVE SDI-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-SDI)
           MOVE SDI-DS-VER
             TO (SF)-DS-VER(IX-TAB-SDI)
           MOVE SDI-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-SDI)
           MOVE SDI-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-SDI)
           MOVE SDI-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-SDI)
           MOVE SDI-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-SDI)
           IF SDI-TP-STRA-NULL = HIGH-VALUES
              MOVE SDI-TP-STRA-NULL
                TO (SF)-TP-STRA-NULL(IX-TAB-SDI)
           ELSE
              MOVE SDI-TP-STRA
                TO (SF)-TP-STRA(IX-TAB-SDI)
           END-IF
           IF SDI-TP-PROVZA-STRA-NULL = HIGH-VALUES
              MOVE SDI-TP-PROVZA-STRA-NULL
                TO (SF)-TP-PROVZA-STRA-NULL(IX-TAB-SDI)
           ELSE
              MOVE SDI-TP-PROVZA-STRA
                TO (SF)-TP-PROVZA-STRA(IX-TAB-SDI)
           END-IF
           IF SDI-PC-PROTEZIONE-NULL = HIGH-VALUES
              MOVE SDI-PC-PROTEZIONE-NULL
                TO (SF)-PC-PROTEZIONE-NULL(IX-TAB-SDI)
           ELSE
              MOVE SDI-PC-PROTEZIONE
                TO (SF)-PC-PROTEZIONE(IX-TAB-SDI)
           END-IF.
       VALORIZZA-OUTPUT-SDI-EX.
           EXIT.

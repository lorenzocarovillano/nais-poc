           03 (SF)-LEN-TOT                       PIC S9(4) COMP-5.
           03 (SF)-REC-FISSO.
             05 (SF)-ID-EST-TRCH-DI-GAR PIC S9(9)V     COMP-3.
             05 (SF)-ID-TRCH-DI-GAR PIC S9(9)V     COMP-3.
             05 (SF)-ID-GAR PIC S9(9)V     COMP-3.
             05 (SF)-ID-ADES PIC S9(9)V     COMP-3.
             05 (SF)-ID-POLI PIC S9(9)V     COMP-3.
             05 (SF)-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
             05 (SF)-ID-MOVI-CHIU-IND  PIC X.
             05 (SF)-ID-MOVI-CHIU PIC S9(9)V     COMP-3.
             05 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             05 (SF)-DT-INI-EFF   PIC X(10).
             05 (SF)-DT-END-EFF   PIC X(10).
             05 (SF)-COD-COMP-ANIA PIC S9(5)V     COMP-3.
             05 (SF)-DT-EMIS-IND  PIC X.
             05 (SF)-DT-EMIS   PIC X(10).
             05 (SF)-DT-EMIS-NULL REDEFINES
                (SF)-DT-EMIS   PIC X(10).
             05 (SF)-CUM-PRE-ATT-IND  PIC X.
             05 (SF)-CUM-PRE-ATT PIC S9(12)V9(3) COMP-3.
             05 (SF)-CUM-PRE-ATT-NULL REDEFINES
                (SF)-CUM-PRE-ATT   PIC X(8).
             05 (SF)-ACCPRE-PAG-IND  PIC X.
             05 (SF)-ACCPRE-PAG PIC S9(12)V9(3) COMP-3.
             05 (SF)-ACCPRE-PAG-NULL REDEFINES
                (SF)-ACCPRE-PAG   PIC X(8).
             05 (SF)-CUM-PRSTZ-IND  PIC X.
             05 (SF)-CUM-PRSTZ PIC S9(12)V9(3) COMP-3.
             05 (SF)-CUM-PRSTZ-NULL REDEFINES
                (SF)-CUM-PRSTZ   PIC X(8).
             05 (SF)-DS-RIGA PIC S9(10)V     COMP-3.
             05 (SF)-DS-OPER-SQL PIC X(1).
             05 (SF)-DS-VER PIC S9(9)V     COMP-3.
             05 (SF)-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
             05 (SF)-DS-TS-END-CPTZ PIC S9(18)V     COMP-3.
             05 (SF)-DS-UTENTE PIC X(20).
             05 (SF)-DS-STATO-ELAB PIC X(1).
             05 (SF)-TP-TRCH PIC X(2).
             05 (SF)-CAUS-SCON-IND  PIC X.
             05 (SF)-CAUS-SCON PIC X(12).
             05 (SF)-CAUS-SCON-NULL REDEFINES
                (SF)-CAUS-SCON   PIC X(12).

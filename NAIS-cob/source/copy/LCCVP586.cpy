      *----------------------------------------------------------------*
      *    COPY      ..... LCCVP586
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO IMPOSTA DI BOLLO
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BP58GNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-P58.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE IMPST-BOLLO

      *--> NOME TABELLA FISICA DB
           MOVE 'IMPST-BOLLO'
             TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WP58-ST-INV(IX-TAB-P58)
              AND WP58-ELE-P58-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WP58-ST-ADD(IX-TAB-P58)

                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK

                          MOVE S090-SEQ-TABELLA
                            TO WP58-ID-PTF(IX-TAB-P58)
                               P58-ID-IMPST-BOLLO

                          MOVE WMOV-ID-PTF
                            TO P58-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                         SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

                       END-IF

      *-->        MODIFICA
                  WHEN WP58-ST-MOD(IX-TAB-P58)

                    MOVE WP58-ID-PTF(IX-TAB-P58)   TO P58-ID-IMPST-BOLLO
                    MOVE WMOV-ID-PTF               TO P58-ID-MOVI-CRZ
                    MOVE WMOV-DT-EFF               TO P58-DT-INI-EFF

      *-->       TIPO OPERAZIONE DISPATCHER
                    SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WP58-ST-DEL(IX-TAB-P58)
                     MOVE WP58-ID-PTF(IX-TAB-P58)  TO P58-ID-IMPST-BOLLO

      *-->       TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->       TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE


              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN IMPOSTA DI BOLLO
                 PERFORM VAL-DCLGEN-P58
                    THRU VAL-DCLGEN-P58-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-P58
                    THRU VALORIZZA-AREA-DSH-P58-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-P58-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-P58.

      *--> DCLGEN TABELLA
           MOVE IMPST-BOLLO             TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT             TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-P58-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVP585.

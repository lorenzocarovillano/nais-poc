      ******************************************************************
      *    TP_TIT     (Tipologia del titolo contabile)
      ******************************************************************
       01  WS-TP-TIT                    PIC X(002) VALUE SPACES.
           88 TT-PREMIO                            VALUE 'PR'.
           88 TT-INTERESSI-PRESTITO                VALUE 'IP'.
           88 TT-RECUPERO-PROVV                    VALUE 'RP'.
           88 TT-PRELIEVO-COSTI                    VALUE 'PC'.
           88 TT-MANAGEMENT-FEE                    VALUE 'ME'.
           88 TT-RIMBORSO-PRESTITO                 VALUE 'RR'.
           88 TT-RIMBORSO-PREMIO                   VALUE 'RO'.
           88 TT-PROVVIGIONALE                     VALUE 'PV'.
           88 TT-ACCR-PROVV                        VALUE 'AP'.

      ******************************************************************
      *    TP_MOVI (DA 3000 A 3999)
      ******************************************************************
           88 MANPE-COLLET                            VALUE 3001.
           88 MANPE-ADESIO                            VALUE 3002.
           88 MANPE-TRANCH                            VALUE 3003.
           88 MANPE-INDIVI                            VALUE 3004.
           88 INSOL-ADESIO                            VALUE 3005.
           88 INSOL-TRANCH                            VALUE 3006.
           88 INSOL-INDIVI                            VALUE 3007.
           88 RECES-ADESIO                            VALUE 3008.
           88 RECES-INDIVI                            VALUE 3009.
           88 RISTO-ADESIO                            VALUE 3010.
           88 REVOC-PERFEZ                            VALUE 3011.
           88 RISTO-INDIVI                            VALUE 3012.
           88 TRASF-COLLET                            VALUE 3013.
           88 TRASF-INDIVI                            VALUE 3014.
           88 SCADE-ADESIO                            VALUE 3015.
           88 SCADE-INDIVI                            VALUE 3017.
           88 SINIS-ADESIO                            VALUE 3018.
           88 SINIS-INDIVI                            VALUE 3019.
           88 RIMBO-TRANCH                            VALUE 3020.
           88 RISCA-ADESIO                            VALUE 3021.
           88 RISCA-POLIND                            VALUE 3022.
           88 LIQUI-REVOC-PERFEZ                      VALUE 3023.
           88 RECES-NOPERF                            VALUE 3024.
           88 COMUN-RECES-POL-COL                     VALUE 3025.
           88 COMUN-RISC-TOT-COL                      VALUE 3026.
           88 COMUN-REV-POL-COL                       VALUE 3027.
           88 COMUN-INSOL-POL-COL                     VALUE 3028.
           88 STO-ALTRE-CAU-POL-COL                   VALUE 3029.
           88 STO-ALTRE-CAU-ADES                      VALUE 3030.
           88 MANPE-TITOLO                            VALUE 3031.
           88 MANPE-STORNO-POLIZZA                    VALUE 3032.
           88 PR-RISCA-TOT-FND                        VALUE 3033.
           88 PR-RISCA-PAR-FND                        VALUE 3034.
           88 PR-SWITCH-OUT-FND                       VALUE 3035.
           88 COM-SWITCH-OUT                          VALUE 3036.
           88 PRENOT-STO-AC-POL-COL                   VALUE 3037.
           88 STO-ALTRE-CAU-TRCH                      VALUE 3038.
           88 COMUN-RISPAR-RPA                        VALUE 3039.
           88 COMUN-RIMB-PRE-ADE                      VALUE 3040.
           88 COMUN-SIN-TERMINE-FISSO-SCAD            VALUE 3050.
           88 CENSIM-BENEF-SIN                        VALUE 3044.
           88 DISDETTA                                VALUE 3055.
           88 COMUN-EST-ANTIC-INDIVI                  VALUE 3056.
           88 ESTINZIONE-FINANZIAMENTO                VALUE 3058.
           88 REVOC-NOPERF                            VALUE 3060.
           88 RIMBORSO-TITOLI                         VALUE 3061.
           88 ESTINZIONE-ANTICIPATA                   VALUE 3066.
           88 MANTEN-IN-COPERTURA                     VALUE 3057.
F3DIS      88 DISDETTA-COMMERCIALE                    VALUE 3059.
MIGCOL     88 RIMBORSO-PREMIO-ADES                    VALUE 3062.

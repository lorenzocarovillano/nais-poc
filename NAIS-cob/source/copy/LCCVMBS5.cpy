
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVMBS5
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-MBS.
           MOVE (SF)-ID-MOVI-BATCH-SOSP(IX-TAB-MBS)
              TO MBS-ID-MOVI-BATCH-SOSP
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-MBS)
              TO MBS-COD-COMP-ANIA
           MOVE (SF)-TP-OGG(IX-TAB-MBS)
              TO MBS-TP-OGG
           MOVE (SF)-ID-MOVI(IX-TAB-MBS)
              TO MBS-ID-MOVI
           MOVE (SF)-TP-MOVI(IX-TAB-MBS)
              TO MBS-TP-MOVI
           MOVE (SF)-DT-EFF(IX-TAB-MBS)
              TO MBS-DT-EFF
           MOVE (SF)-ID-OGG-BLOCCO(IX-TAB-MBS)
              TO MBS-ID-OGG-BLOCCO
           MOVE (SF)-TP-FRM-ASSVA(IX-TAB-MBS)
              TO MBS-TP-FRM-ASSVA
           IF (SF)-ID-BATCH-NULL(IX-TAB-MBS) = HIGH-VALUES
              MOVE (SF)-ID-BATCH-NULL(IX-TAB-MBS)
              TO MBS-ID-BATCH-NULL
           ELSE
              MOVE (SF)-ID-BATCH(IX-TAB-MBS)
              TO MBS-ID-BATCH
           END-IF
           IF (SF)-ID-JOB-NULL(IX-TAB-MBS) = HIGH-VALUES
              MOVE (SF)-ID-JOB-NULL(IX-TAB-MBS)
              TO MBS-ID-JOB-NULL
           ELSE
              MOVE (SF)-ID-JOB(IX-TAB-MBS)
              TO MBS-ID-JOB
           END-IF
           MOVE (SF)-STEP-ELAB(IX-TAB-MBS)
              TO MBS-STEP-ELAB
           MOVE (SF)-FL-MOVI-SOSP(IX-TAB-MBS)
              TO MBS-FL-MOVI-SOSP
           MOVE (SF)-D-INPUT-MOVI-SOSP(IX-TAB-MBS)
              TO MBS-D-INPUT-MOVI-SOSP
           MOVE (SF)-DS-OPER-SQL(IX-TAB-MBS)
              TO MBS-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-MBS) NOT NUMERIC
              MOVE 0 TO MBS-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-MBS)
              TO MBS-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ(IX-TAB-MBS) NOT NUMERIC
              MOVE 0 TO MBS-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ(IX-TAB-MBS)
              TO MBS-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-MBS)
              TO MBS-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-MBS)
              TO MBS-DS-STATO-ELAB.
       VAL-DCLGEN-MBS-EX.
           EXIT.

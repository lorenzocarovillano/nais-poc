      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA D_CRIST
      *   ALIAS P61
      *   ULTIMO AGG. 24 OTT 2019
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-D-CRIST PIC S9(9)     COMP-3.
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-PROD PIC X(12).
             07 (SF)-DT-DECOR   PIC S9(8) COMP-3.
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-RIS-MAT-31122011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-MAT-31122011-NULL REDEFINES
                (SF)-RIS-MAT-31122011   PIC X(8).
             07 (SF)-PRE-V-31122011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-V-31122011-NULL REDEFINES
                (SF)-PRE-V-31122011   PIC X(8).
             07 (SF)-PRE-RSH-V-31122011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-RSH-V-31122011-NULL REDEFINES
                (SF)-PRE-RSH-V-31122011   PIC X(8).
             07 (SF)-CPT-RIVTO-31122011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-CPT-RIVTO-31122011-NULL REDEFINES
                (SF)-CPT-RIVTO-31122011   PIC X(8).
             07 (SF)-IMPB-VIS-31122011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-31122011-NULL REDEFINES
                (SF)-IMPB-VIS-31122011   PIC X(8).
             07 (SF)-IMPB-IS-31122011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-31122011-NULL REDEFINES
                (SF)-IMPB-IS-31122011   PIC X(8).
             07 (SF)-IMPB-VIS-RP-P2011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-RP-P2011-NULL REDEFINES
                (SF)-IMPB-VIS-RP-P2011   PIC X(8).
             07 (SF)-IMPB-IS-RP-P2011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-RP-P2011-NULL REDEFINES
                (SF)-IMPB-IS-RP-P2011   PIC X(8).
             07 (SF)-PRE-V-30062014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-V-30062014-NULL REDEFINES
                (SF)-PRE-V-30062014   PIC X(8).
             07 (SF)-PRE-RSH-V-30062014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-RSH-V-30062014-NULL REDEFINES
                (SF)-PRE-RSH-V-30062014   PIC X(8).
             07 (SF)-CPT-INI-30062014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-CPT-INI-30062014-NULL REDEFINES
                (SF)-CPT-INI-30062014   PIC X(8).
             07 (SF)-IMPB-VIS-30062014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-30062014-NULL REDEFINES
                (SF)-IMPB-VIS-30062014   PIC X(8).
             07 (SF)-IMPB-IS-30062014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-30062014-NULL REDEFINES
                (SF)-IMPB-IS-30062014   PIC X(8).
             07 (SF)-IMPB-VIS-RP-P62014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-RP-P62014-NULL REDEFINES
                (SF)-IMPB-VIS-RP-P62014   PIC X(8).
             07 (SF)-IMPB-IS-RP-P62014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-RP-P62014-NULL REDEFINES
                (SF)-IMPB-IS-RP-P62014   PIC X(8).
             07 (SF)-RIS-MAT-30062014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-MAT-30062014-NULL REDEFINES
                (SF)-RIS-MAT-30062014   PIC X(8).
             07 (SF)-ID-ADES PIC S9(9)     COMP-3.
             07 (SF)-ID-ADES-NULL REDEFINES
                (SF)-ID-ADES   PIC X(5).
             07 (SF)-MONT-LRD-END2000 PIC S9(12)V9(3) COMP-3.
             07 (SF)-MONT-LRD-END2000-NULL REDEFINES
                (SF)-MONT-LRD-END2000   PIC X(8).
             07 (SF)-PRE-LRD-END2000 PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-LRD-END2000-NULL REDEFINES
                (SF)-PRE-LRD-END2000   PIC X(8).
             07 (SF)-RENDTO-LRD-END2000 PIC S9(12)V9(3) COMP-3.
             07 (SF)-RENDTO-LRD-END2000-NULL REDEFINES
                (SF)-RENDTO-LRD-END2000   PIC X(8).
             07 (SF)-MONT-LRD-END2006 PIC S9(12)V9(3) COMP-3.
             07 (SF)-MONT-LRD-END2006-NULL REDEFINES
                (SF)-MONT-LRD-END2006   PIC X(8).
             07 (SF)-PRE-LRD-END2006 PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-LRD-END2006-NULL REDEFINES
                (SF)-PRE-LRD-END2006   PIC X(8).
             07 (SF)-RENDTO-LRD-END2006 PIC S9(12)V9(3) COMP-3.
             07 (SF)-RENDTO-LRD-END2006-NULL REDEFINES
                (SF)-RENDTO-LRD-END2006   PIC X(8).
             07 (SF)-MONT-LRD-DAL2007 PIC S9(12)V9(3) COMP-3.
             07 (SF)-MONT-LRD-DAL2007-NULL REDEFINES
                (SF)-MONT-LRD-DAL2007   PIC X(8).
             07 (SF)-PRE-LRD-DAL2007 PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-LRD-DAL2007-NULL REDEFINES
                (SF)-PRE-LRD-DAL2007   PIC X(8).
             07 (SF)-RENDTO-LRD-DAL2007 PIC S9(12)V9(3) COMP-3.
             07 (SF)-RENDTO-LRD-DAL2007-NULL REDEFINES
                (SF)-RENDTO-LRD-DAL2007   PIC X(8).
             07 (SF)-ID-TRCH-DI-GAR PIC S9(9)     COMP-3.
             07 (SF)-ID-TRCH-DI-GAR-NULL REDEFINES
                (SF)-ID-TRCH-DI-GAR   PIC X(5).

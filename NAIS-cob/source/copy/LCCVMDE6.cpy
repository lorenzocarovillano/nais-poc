      *----------------------------------------------------------------*
      *    PROGRAMMA ..... LCCVMDE6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO MOTIVO DEROGA
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-MOT-DEROGA.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE MOT-DEROGA

      *--> NOME TABELLA FISICA DB
           MOVE 'MOT-DEROGA'                   TO WK-TABELLA

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WMDE-ST-INV(IX-TAB-MDE)
              AND WMDE-ELE-MDE-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WMDE-ST-ADD(IX-TAB-MDE)

                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA
                            TO WMDE-ID-PTF(IX-TAB-MDE)
                          MOVE WMDE-ID-PTF(IX-TAB-MDE)
                            TO MDE-ID-MOT-DEROGA
                          MOVE WMOV-ID-PTF
                            TO MDE-ID-MOVI-CRZ
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WMDE-ST-MOD(IX-TAB-MDE)

                       MOVE WMDE-ID-PTF(IX-TAB-MDE)
                         TO MDE-ID-MOT-DEROGA
                       MOVE WMDE-ID-OGG-DEROGA(IX-TAB-MDE)
                         TO MDE-ID-OGG-DEROGA
                       MOVE WMOV-ID-PTF
                         TO MDE-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WMDE-ST-DEL(IX-TAB-MDE)

                       MOVE WMDE-ID-PTF(IX-TAB-MDE)
                         TO MDE-ID-MOT-DEROGA
                       MOVE WMDE-ID-OGG-DEROGA(IX-TAB-MDE)
                         TO MDE-ID-OGG-DEROGA
                       MOVE WMOV-ID-PTF
                         TO MDE-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN ADESIONE
                 PERFORM VAL-DCLGEN-MDE
                    THRU VAL-DCLGEN-MDE-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-MDE
                    THRU VALORIZZA-AREA-DSH-MDE-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-MOT-DEROGA-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-MDE.

      *--> DCLGEN TABELLA
           MOVE MOT-DEROGA               TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-MDE-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVMDE5.

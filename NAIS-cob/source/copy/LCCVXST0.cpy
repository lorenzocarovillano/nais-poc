      ******************************************************************
      *    TP_STAT_TIT (Stato del titolo contabile)
      ******************************************************************
       01  WS-TP-STAT-TIT                   PIC X(002) VALUE SPACES.
           88 EMESSO                                   VALUE 'EM'.
           88 INCASSATO                                VALUE 'IN'.
           88 DA-PAGARE                                VALUE 'DP'.
           88 STORNATO-EMESSO                          VALUE 'SE'.
           88 STORNATO-INCASSO                         VALUE 'SI'.
           88 ATTESA-EMISSIONE                         VALUE 'AM'.
           88 TIT-SENZA-SEGUITO                        VALUE 'SS'.
           88 SALVO-BUON-FINE                          VALUE 'SB'.
           88 PAGATO                                   VALUE 'PA'.
           88 TP-STAT-STORNATO                         VALUE 'ST'.


       01  LDBVF981.
           03 LDBVF981-ID-GAR             PIC S9(9)V COMP-3.
           03 LDBVF981-TP-STAT-TIT-1      PIC X(2).
           03 LDBVF981-TP-STAT-TIT-2      PIC X(2).
           03 LDBVF981-TP-STAT-TIT-3      PIC X(2).
           03 LDBVF981-TP-STAT-TIT-4      PIC X(2).
           03 LDBVF981-TP-STAT-TIT-5      PIC X(2).
           03 LDBVF981-DT-INI-COP         PIC S9(8)V COMP-3.

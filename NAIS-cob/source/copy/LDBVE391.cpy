       01  LDBVE391.
           03 AREA-LDBVE391-I.
              05 LDBVE391-COD-FISC               PIC X(16).
              05 LDBVE391-TP-CAUS-BOLLO-1        PIC X(02).
              05 LDBVE391-TP-CAUS-BOLLO-2        PIC X(02).
              05 LDBVE391-TP-CAUS-BOLLO-3        PIC X(02).
              05 LDBVE391-TP-CAUS-BOLLO-4        PIC X(02).
              05 LDBVE391-TP-CAUS-BOLLO-5        PIC X(02).
              05 LDBVE391-TP-CAUS-BOLLO-6        PIC X(02).
              05 LDBVE391-TP-CAUS-BOLLO-7        PIC X(02).
              05 LDBVE391-DT-INI-CALC            PIC S9(8)  COMP-3.
              05 LDBVE391-DT-END-CALC            PIC S9(8)  COMP-3.
              05 LDBVE391-DT-INI-CALC-DB         PIC X(10).
              05 LDBVE391-DT-END-CALC-DB         PIC X(10).

           03 AREA-LDBVE391-O.
              05 LDBVE391-IMP-BOLLO-DETT-C       PIC S9(12)V9(3) COMP-3.
              05 LDBVE391-IMP-BOLLO-DETT-V       PIC S9(12)V9(3) COMP-3.
              05 LDBVE391-IMP-BOLLO-TOT-V        PIC S9(12)V9(3) COMP-3.
              05 LDBVE391-IMP-BOLLO-TOT-R        PIC S9(12)V9(3) COMP-3.

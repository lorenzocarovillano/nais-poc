
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVPLI4
      *   ULTIMO AGG. 07 GEN 2016
      *------------------------------------------------------------

       INIZIA-TOT-PLI.

           PERFORM INIZIA-ZEROES-PLI THRU INIZIA-ZEROES-PLI-EX

           PERFORM INIZIA-SPACES-PLI THRU INIZIA-SPACES-PLI-EX

           PERFORM INIZIA-NULL-PLI THRU INIZIA-NULL-PLI-EX.

       INIZIA-TOT-PLI-EX.
           EXIT.

       INIZIA-NULL-PLI.
           MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PLI)
           MOVE HIGH-VALUES TO (SF)-ID-RAPP-ANA-NULL(IX-TAB-PLI)
           MOVE HIGH-VALUES TO (SF)-PC-LIQ-NULL(IX-TAB-PLI)
           MOVE HIGH-VALUES TO (SF)-IMP-LIQ-NULL(IX-TAB-PLI)
           MOVE HIGH-VALUES TO (SF)-TP-MEZ-PAG-NULL(IX-TAB-PLI)
           MOVE HIGH-VALUES TO (SF)-ITER-PAG-AVV-NULL(IX-TAB-PLI)
           MOVE HIGH-VALUES TO (SF)-DT-VLT-NULL(IX-TAB-PLI)
           MOVE HIGH-VALUES TO (SF)-INT-CNT-CORR-ACCR(IX-TAB-PLI)
           MOVE HIGH-VALUES TO (SF)-COD-IBAN-RIT-CON-NULL(IX-TAB-PLI).
       INIZIA-NULL-PLI-EX.
           EXIT.

       INIZIA-ZEROES-PLI.
           MOVE 0 TO (SF)-ID-PERC-LIQ(IX-TAB-PLI)
           MOVE 0 TO (SF)-ID-BNFICR-LIQ(IX-TAB-PLI)
           MOVE 0 TO (SF)-ID-MOVI-CRZ(IX-TAB-PLI)
           MOVE 0 TO (SF)-DT-INI-EFF(IX-TAB-PLI)
           MOVE 0 TO (SF)-DT-END-EFF(IX-TAB-PLI)
           MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-PLI)
           MOVE 0 TO (SF)-DS-RIGA(IX-TAB-PLI)
           MOVE 0 TO (SF)-DS-VER(IX-TAB-PLI)
           MOVE 0 TO (SF)-DS-TS-INI-CPTZ(IX-TAB-PLI)
           MOVE 0 TO (SF)-DS-TS-END-CPTZ(IX-TAB-PLI).
       INIZIA-ZEROES-PLI-EX.
           EXIT.

       INIZIA-SPACES-PLI.
           MOVE SPACES TO (SF)-DS-OPER-SQL(IX-TAB-PLI)
           MOVE SPACES TO (SF)-DS-UTENTE(IX-TAB-PLI)
           MOVE SPACES TO (SF)-DS-STATO-ELAB(IX-TAB-PLI).
       INIZIA-SPACES-PLI-EX.
           EXIT.

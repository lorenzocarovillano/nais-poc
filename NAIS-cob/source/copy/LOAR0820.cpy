      *----------------------------------------------------------------*
      *   PROCESSO DI POST-VENDITA - PORTAFOGLIO VITA
      *   AREA VARIABILI E COSTANTI DI INPUT
      *   SERVIZIO DI CALCOLO MANAGEMENT FEE
      *----------------------------------------------------------------*
      *
          03 (SF)-AREA-TESTATA.
      *
            05 FILLER  PIC X(17)
                           VALUE 'DATA ELABORAZIONE'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(21)
                           VALUE 'CODICE COMPAGNIA ANIA'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(14)
                           VALUE 'CODICE AGENZIA'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(15)
                           VALUE 'RAMO GESTIONALE'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(14)
                           VALUE 'NUMERO POLIZZA'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(14)
                           VALUE 'CODICE TARIFFA'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(14)
                           VALUE 'DATA EMISSIONE'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(12)
                           VALUE 'DATA EFFETTO'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(15)
                           VALUE 'DATA RICORRENZA'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(14)
                           VALUE 'DATA PAGAMENTO'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(12)
                           VALUE 'PREMIO NETTO'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(19)
                           VALUE 'CAPITALE RIVALUTATO'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(17)
                           VALUE 'ULTIMO INCREMENTO'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(08)
                           VALUE 'RISERVA'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(26)
                           VALUE 'FLAG VERSAMENTI AGGIUNTIVI'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(22)
                           VALUE 'FLAG RISCATTI PARZIALI'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(18)
                           VALUE 'IMPORTO RISCATTATO'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(25)
                           VALUE 'PERCENTUALE REMUNERAZIONE'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(21)
                           VALUE 'IMPORTO REMUNERAZIONE'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(15)
                           VALUE 'TP-ELABORAZIONE'.
            05 FILLER  PIC X(01) VALUE ';'.
            05 FILLER  PIC X(59) VALUE SPACES.
      *
          03 (SF)-AREA-DATI.
      *
            05 (SF)-DT-ELAB       PIC X(010) VALUE SPACES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-COD-ANIA      PIC 9(005) VALUE ZEROES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-COD-AGE       PIC 9(005) VALUE ZEROES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-RAMO-GEST     PIC X(012) VALUE SPACES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-NUM-POLI      PIC X(040) VALUE SPACES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-COD-TARI      PIC X(012) VALUE SPACES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-DT-EMIS       PIC X(010) VALUE SPACES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-DT-EFF        PIC X(010) VALUE SPACES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-DT-RICOR      PIC X(007) VALUE SPACES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-DT-PAG        PIC X(010) VALUE SPACES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-PREMIO-NET    PIC ZZZ.ZZZ.ZZZ.ZZ9,999 VALUE ZEROES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-CPT-RIVTO     PIC ZZZ.ZZZ.ZZZ.ZZ9,999 VALUE ZEROES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-ULT-INCR      PIC ZZZ.ZZZ.ZZZ.ZZ9,999 VALUE ZEROES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-RISERVA       PIC ZZZ.ZZZ.ZZZ.ZZ9,999 VALUE ZEROES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-FLAG-VERS-AGG PIC X(001) VALUE SPACES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-FLAG-RIS-PARZ PIC X(001) VALUE SPACES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-IMP-RISC      PIC ZZZ.ZZZ.ZZZ.ZZ9,999 VALUE ZEROES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-PC-REMUNER    PIC             ZZ9,999 VALUE ZEROES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-IMP-REMUNER   PIC ZZZ.ZZZ.ZZZ.ZZ9,999 VALUE ZEROES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 (SF)-TP-ELAB       PIC X(012) VALUE SPACES.
            05 FILLER             PIC X(001) VALUE ';'.
            05 FILLER             PIC X(125) VALUE SPACES.

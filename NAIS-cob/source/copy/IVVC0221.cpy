      *----------------------------------------------------------------*
      * INTERFACCIA WHERE CONDITION DETT_QUEST.
      *----------------------------------------------------------------*
         03 IVVC0221-DETT-QUEST-WC.
            05 IVVC0221-FORMA-TECNICA            PIC X(002).
               88 IVVC0221-INDIVIDUALE           VALUE 'IN'.
               88 IVVC0221-COLLETTIVE            VALUE 'CO'.
            05 IVVC0221-FILLER                   PIC X(001).
            05 IVVC0221-COD-QUEST                PIC X(015).
            05 IVVC0221-FILLER-1                 PIC X(001).
            05 IVVC0221-MAX-DOMANDA              PIC 9(002).
            05 IVVC0221-FILLER-2                 PIC X(001).
            05 IVVC0221-TAB-DOMANDA              OCCURS 5.
               10 IVVC0221-COD-DOMANDA           PIC X(005).
               10 IVVC0221-FILLER-DOM            PIC X(001).
               10 IVVC0221-MAX-RISPOSTA          PIC 9(002).
               10 IVVC0221-FILLER-3              PIC X(001).
               10 IVVC0221-TAB-RISPOSTA          OCCURS 10.
                 15 IVVC0221-AREA-RISP.
                    20 IVVC0221-VAL-RISPOSTA     PIC X(003).
                    20 IVVC0221-FILLER-RIS       PIC X(001).

         03 IVVC0211-DETT-QUEST-VAL.
            05 IVVC0211-COD-DOMANDA              PIC X(005).
            05 IVVC0221-AREA-RISP-APPO.
               10 IVVC0221-VAL-RISP-01           PIC X(005).
               10 IVVC0221-FILLER-RIS-1          PIC X(001).
               10 IVVC0221-VAL-RISP-02           PIC X(005).
               10 IVVC0221-FILLER-RIS-2          PIC X(001).
               10 IVVC0221-VAL-RISP-03           PIC X(005).
               10 IVVC0221-FILLER-RIS-3          PIC X(001).
               10 IVVC0221-VAL-RISP-04           PIC X(005).
               10 IVVC0221-FILLER-RIS-4          PIC X(001).
               10 IVVC0221-VAL-RISP-05           PIC X(005).
               10 IVVC0221-FILLER-RIS-5          PIC X(001).

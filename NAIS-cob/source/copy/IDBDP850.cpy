           EXEC SQL DECLARE ESTR_CNT_DIAGN_RIV TABLE
           (
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             ID_POLI             DECIMAL(9, 0) NOT NULL,
             DT_RIVAL            DATE NOT NULL,
             COD_TARI            CHAR(12) NOT NULL,
             RENDTO_LRD          DECIMAL(14, 9),
             PC_RETR             DECIMAL(6, 3),
             RENDTO_RETR         DECIMAL(14, 9),
             COMMIS_GEST         DECIMAL(15, 3),
             TS_RIVAL_NET        DECIMAL(14, 9),
             MIN_GARTO           DECIMAL(14, 9),
             MIN_TRNUT           DECIMAL(14, 9),
             IB_POLI             CHAR(40) NOT NULL,
             DS_OPER_SQL         CHAR(1) NOT NULL,
             DS_VER              DECIMAL(9, 0) NOT NULL,
             DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL,
             DS_UTENTE           CHAR(20) NOT NULL,
             DS_STATO_ELAB       CHAR(1) NOT NULL
          ) END-EXEC.

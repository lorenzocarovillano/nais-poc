      *----------------------------------------------------------------*
      *    COPY      ..... LCCVDCO6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO DATI COLLETTIVA
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN DATI COLLETTIVA (LCCVDCO1)
      *
      *----------------------------------------------------------------*

       AGGIORNA-D-COLL.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE D-COLL.

      *--> NOME TABELLA FISICA DB
           MOVE 'D-COLL'                   TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WDCO-ST-INV AND WDCO-ELE-COLL-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WDCO-ST-ADD
                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA
                            TO WDCO-ID-PTF
                          MOVE WDCO-ID-PTF         TO DCO-ID-D-COLL
                          MOVE WPOL-ID-PTF         TO DCO-ID-POLI
                          MOVE WMOV-ID-PTF         TO DCO-ID-MOVI-CRZ
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WDCO-ST-MOD

                       MOVE WDCO-ID-D-COLL      TO DCO-ID-D-COLL
                       MOVE WDCO-ID-POLI        TO DCO-ID-POLI
                       MOVE WMOV-ID-PTF         TO DCO-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WDCO-ST-DEL

                       MOVE WDCO-ID-PTF         TO DCO-ID-D-COLL
                       MOVE WPOL-ID-PTF         TO DCO-ID-POLI
                       MOVE WMOV-ID-PTF         TO DCO-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

      *-->    VALORIZZA DCLGEN ADESIONE
              PERFORM VAL-DCLGEN-DCO
                 THRU VAL-DCLGEN-DCO-EX

      *-->    VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
              PERFORM VALORIZZA-AREA-DSH-DCO
                 THRU VALORIZZA-AREA-DSH-DCO-EX

      *-->    CALL DISPATCHER PER AGGIORNAMENTO
              PERFORM AGGIORNA-TABELLA
                 THRU AGGIORNA-TABELLA-EX

           END-IF.

       AGGIORNA-D-COLL-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-DCO.

      *--> NOME TABELLA FISICA DB
           MOVE 'D-COLL'                TO   WK-TABELLA.

      *--> DCLGEN TABELLA
           MOVE D-COLL                  TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-DCO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVDCO5.



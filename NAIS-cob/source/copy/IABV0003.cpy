      ******************************************************************
      * LUNGHEZZA COPY : 1048576 BYTE
      ******************************************************************
       01 IABV0003.
         02 IABV0003-JOB.
            05 IABV0003-IB-OGG                      PIC X(100).
            05 IABV0003-COD-MACROFUNCT              PIC X(002).
            05 IABV0003-TP-MOVI-BUSINESS            PIC S9(5)V COMP-3.

         02 IABV0003-DATA-JOB.
            05 IABV0003-ELE-MAX                     PIC 9(003).
            05 IABV0003-BLOB-DATA.
               10 IABV0003-BLOB-DATA-ARRAY          PIC X(1051050).

               10 IABV0003-BLOB-TABLE
                  REDEFINES IABV0003-BLOB-DATA-ARRAY.
                  15 IABV0003-BLOB-DATA-ELE OCCURS 100 TIMES.
                     20 IABV0003-BLOB-DATA-TYPE-REC    PIC X(003).
                     20 IABV0003-BLOB-DATA-REC         PIC X(10000).
                  15 FILLER                            PIC X(50750).

               10 IABV0003-BLOB-TABLE-3K
                  REDEFINES IABV0003-BLOB-DATA-ARRAY.
                  15 IABV0003-BLOB-DATA-ELE-3K OCCURS 350 TIMES.
                     20 IABV0003-BLOB-DATA-TYPE-REC-3K PIC X(003).
                     20 IABV0003-BLOB-DATA-REC-3K      PIC X(3000).

               10 IABV0003-BLOB-TABLE-2K
                  REDEFINES IABV0003-BLOB-DATA-ARRAY.
                  15 IABV0003-BLOB-DATA-ELE-2K OCCURS 500 TIMES.
                     20 IABV0003-BLOB-DATA-TYPE-REC-2K PIC X(003).
                     20 IABV0003-BLOB-DATA-REC-2K      PIC X(2000).
                  15 FILLER                            PIC X(49550).

               10 IABV0003-BLOB-TABLE-1K
                  REDEFINES IABV0003-BLOB-DATA-ARRAY.
                  15 IABV0003-BLOB-DATA-ELE-1K OCCURS 650 TIMES.
                     20 IABV0003-BLOB-DATA-TYPE-REC-1K PIC X(003).
                     20 IABV0003-BLOB-DATA-REC-1K      PIC X(1503).
                  15 FILLER                            PIC X(72150).

            05 IABV0003-LUNG-EFF-BLOB                  PIC 9(009).

         02 IABV0003-ALIAS-STR-DATO                    PIC X(30).

         02 FILLER                                     PIC X(70).

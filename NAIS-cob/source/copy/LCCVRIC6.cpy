      *----------------------------------------------------------------*
      *    COPY      ..... LCCVRIC6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO RICHIESTA
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVSDI5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN STRATEGIA DI INVESTIMENTO (LCCVSDI1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-RICH.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE RICH

      *--> NOME TABELLA FISICA DB
           MOVE 'RICH'                        TO WK-TABELLA

      *--> IN ATTESA DELLA GESTIONE DINAMICA
FORZA      MOVE 'PG'                          TO WRIC-COD-MACROFUNCT.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WRIC-ST-INV
              AND WRIC-ELE-RICH-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WRIC-ST-ADD

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK

                        MOVE S090-SEQ-TABELLA          TO WRIC-ID-PTF
                        MOVE WRIC-ID-PTF               TO  RIC-ID-RICH
                                                          WRIC-ID-RICH
      *-->              VALORIZZAZIONE  IB RICHIESTA
                        PERFORM VAL-IB-RICHIESTA
                           THRU VAL-IB-RICHIESTA-EX

      *-->              PREPARAZIONE ED ESTRAZIONE OGGETTO PTF
                        PERFORM PREPARA-AREA-LCCS0234-RIC
                           THRU PREPARA-AREA-LCCS0234-RIC-EX

                        PERFORM CALL-LCCS0234
                           THRU CALL-LCCS0234-EX

                        IF IDSV0001-ESITO-OK

                           MOVE S234-ID-OGG-PTF-EOC
                             TO RIC-ID-OGG

                           EVALUATE WRIC-TP-OGG
                              WHEN WS-POLIZZA
                                 MOVE S234-IB-OGG-PTF-EOC
                                   TO WRIC-IB-POLI
                              WHEN WS-ADESIONE
                                 MOVE S234-IB-OGG-PTF-EOC
                                   TO WRIC-IB-ADES
                              WHEN WS-GARANZIA
                                 MOVE S234-IB-OGG-PTF-EOC
                                   TO WRIC-IB-GAR
                              WHEN WS-TRANCHE
                                 MOVE S234-IB-OGG-PTF-EOC
                                   TO WRIC-IB-TRCH-DI-GAR
                           END-EVALUATE

                        END-IF
                     END-IF

                     MOVE WMOV-TP-MOVI              TO  WRIC-TP-MOVI

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-INSERT         TO TRUE

      *-->        MODIFICA
                  WHEN WRIC-ST-MOD

                     MOVE WRIC-ID-RICH       TO RIC-ID-RICH

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-UPDATE         TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WRIC-ST-DEL

                     MOVE WRIC-ID-RICH       TO RIC-ID-RICH

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-DELETE         TO TRUE

           END-EVALUATE

           IF IDSV0001-ESITO-OK

      *-->    VALORIZZA DCLGEN ADESIONE
              PERFORM VAL-DCLGEN-RIC
                 THRU VAL-DCLGEN-RIC-EX

      *-->    VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
              PERFORM VALORIZZA-AREA-DSH-RIC
                 THRU VALORIZZA-AREA-DSH-RIC-EX

      *-->    CALL DISPATCHER PER AGGIORNAMENTO
              PERFORM AGGIORNA-TABELLA
                 THRU AGGIORNA-TABELLA-EX

           END-IF.

       AGGIORNA-RICH-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-RIC.

      *--> DCLGEN TABELLA
           MOVE RICH                    TO IDSI0011-BUFFER-DATI.

           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-PRIMARY-KEY      TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-RIC-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA IB RICHIESTA
      *----------------------------------------------------------------*
       VAL-IB-RICHIESTA.

      *--> L'IB RICHIESTA E' COMPOSTO DAL CODICE COMPAGNIA +
      *--> SEQUENCE DELLA RICHIESTA.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO WS-COD-COMPAGNIA.

           MOVE WRIC-ID-PTF           TO  WS-ID-RICH-PTF.
           MOVE WS-ID-RICH-PTF        TO  WS-SEQUENCE-PTF.
           MOVE WS-OGGETTO            TO  WS-RICHIESTA.

           INSPECT WS-RICHIESTA
                REPLACING ALL ' ' BY '0'.
           MOVE WS-RICHIESTA       TO WRIC-IB-RICH.

       VAL-IB-RICHIESTA-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    PREPARA AREA PER CALL LCCS0234
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234-RIC.

           MOVE WMOV-ID-OGG             TO WRIC-ID-OGG
           MOVE WMOV-TP-OGG             TO WRIC-TP-OGG
           MOVE WRIC-ID-OGG             TO S234-ID-OGG-EOC.
           MOVE WRIC-TP-OGG             TO S234-TIPO-OGG-EOC.

       PREPARA-AREA-LCCS0234-RIC-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVRIC5.

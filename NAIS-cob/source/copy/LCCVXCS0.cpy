      *****************************************************************
      *    TP_CAUS_SCARTO (Tipo Causale scarto)
      *****************************************************************
       01  WS-TP-CAUS-SCARTO                   PIC X(002) VALUE SPACES.
           88 POLIZZA-NON-PRESENTE                     VALUE '01'.
           88 OPERAZIONE-IN-CORSO                      VALUE '02'.
           88 CONTR-FORMALI                            VALUE '03'.
           88 CONTR-INDEROGABILI                       VALUE '04'.
           88 POLIZZA-NON-VIGORE                       VALUE '05'.
           88 ERRORE-GESTIONE                          VALUE '06'.
           88 SCARTO-DEROGHE                           VALUE '07'.
           88 RICHIESTA-IN-ATTESA                      VALUE '08'.
           88 SCADENZA-ANTICIPO                        VALUE '09'.
           88 RICHIESTA-SENZA-QDD                      VALUE '95'.
           88 DEROGA-RIFIUTATA                         VALUE '96'.
           88 RICHIESTA-ANNULLATA                      VALUE '97'.
           88 RICHIESTA-EVASA-ONLINE                   VALUE '98'.
           88 RICHIESTA-DUPLICATA                      VALUE '99'.

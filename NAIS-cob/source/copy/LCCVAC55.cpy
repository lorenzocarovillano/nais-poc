
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVAC55
      *   ULTIMO AGG. 27 OTT 2010
      *------------------------------------------------------------

       VAL-DCLGEN-AC5.
           MOVE (SF)-IDCOMP
              TO AC5-IDCOMP
           MOVE (SF)-CODPROD
              TO AC5-CODPROD
           MOVE (SF)-CODTARI
              TO AC5-CODTARI
           MOVE (SF)-DINIZ
              TO AC5-DINIZ
           MOVE (SF)-NOMEFUNZ
              TO AC5-NOMEFUNZ
           MOVE (SF)-DEND
              TO AC5-DEND
           IF (SF)-ISPRECALC-NULL = HIGH-VALUES
              MOVE (SF)-ISPRECALC-NULL
              TO AC5-ISPRECALC-NULL
           ELSE
              MOVE (SF)-ISPRECALC
              TO AC5-ISPRECALC
           END-IF
           MOVE (SF)-NOMEPROGR
              TO AC5-NOMEPROGR
           MOVE (SF)-MODCALC
              TO AC5-MODCALC
           MOVE (SF)-GLOVARLIST
              TO AC5-GLOVARLIST
           MOVE (SF)-STEP-ELAB
              TO AC5-STEP-ELAB.
       VAL-DCLGEN-AC5-EX.
           EXIT.

      *------------------------------------------------------------*
      * CAMPI PER WHERE CONDITION FETCH SU MATRICE FUNZIONE        *
      *------------------------------------------------------------*
       01 AREA-LDBI0260.
         05 LDBI0260-FLAG-CUR           PIC 9(1).
         05 LDBI0260-COD-MOVI-NAIS      PIC S9(5)V COMP-3.
         05 LDBI0260-OGG-NAIS           PIC X(2).
         05 LDBI0260-FRM-ASSVA          PIC X(2).
         05 LDBI0260-COD-MOVI-ACTUATOR  PIC X(5).
         05 LDBI0260-AMMISSIBILITA-MOVI PIC X(1).

      *----------------------------------------------------------------*
      *    DISPLAY TABELLA RAPPORTO ANAGRAFICO
      *    N.B Per utilizzare la copy bisogna includere la copy
      *    Infrastrutturale IDSV8888
      *----------------------------------------------------------------*
       DISPLAY-TAB-RAN.

           DISPLAY 'WRAN-STATUS              (' IX-DIS-I ') = '
                    WRAN-STATUS(IX-DIS-I)
           DISPLAY 'WRAN-ID-PTF              (' IX-DIS-I ') = '
                    WRAN-ID-PTF(IX-DIS-I)
           DISPLAY 'WRAN-ID-RAPP-ANA         (' IX-DIS-I ') = '
                    WRAN-ID-RAPP-ANA(IX-DIS-I)
           DISPLAY 'WRAN-ID-RAPP-ANA-COLLG   (' IX-DIS-I ') = '
                    WRAN-ID-RAPP-ANA-COLLG(IX-DIS-I)
           DISPLAY 'WRAN-ID-OGG              (' IX-DIS-I ') = '
                    WRAN-ID-OGG(IX-DIS-I)
           DISPLAY 'WRAN-TP-OGG              (' IX-DIS-I ') = '
                    WRAN-TP-OGG(IX-DIS-I)
           DISPLAY 'WRAN-ID-MOVI-CRZ         (' IX-DIS-I ') = '
                    WRAN-ID-MOVI-CRZ(IX-DIS-I)
           DISPLAY 'WRAN-ID-MOVI-CHIU        (' IX-DIS-I ') = '
                    WRAN-ID-MOVI-CHIU(IX-DIS-I)
           DISPLAY 'WRAN-DT-INI-EFF          (' IX-DIS-I ') = '
                    WRAN-DT-INI-EFF(IX-DIS-I)
           DISPLAY 'WRAN-DT-END-EFF          (' IX-DIS-I ') = '
                    WRAN-DT-END-EFF(IX-DIS-I)
           DISPLAY 'WRAN-COD-COMP-ANIA       (' IX-DIS-I ') = '
                    WRAN-COD-COMP-ANIA(IX-DIS-I)
           DISPLAY 'WRAN-COD-SOGG            (' IX-DIS-I ') = '
                    WRAN-COD-SOGG(IX-DIS-I)
           DISPLAY 'WRAN-TP-RAPP-ANA         (' IX-DIS-I ') = '
                    WRAN-TP-RAPP-ANA(IX-DIS-I)
           DISPLAY 'WRAN-TP-PERS             (' IX-DIS-I ') = '
                    WRAN-TP-PERS(IX-DIS-I)
           DISPLAY 'WRAN-SEX                 (' IX-DIS-I ') = '
                    WRAN-SEX(IX-DIS-I)
           DISPLAY 'WRAN-DT-NASC             (' IX-DIS-I ') = '
                    WRAN-DT-NASC(IX-DIS-I)
           DISPLAY 'WRAN-FL-ESTAS            (' IX-DIS-I ') = '
                    WRAN-FL-ESTAS(IX-DIS-I)
           DISPLAY 'WRAN-INDIR-1             (' IX-DIS-I ') = '
                    WRAN-INDIR-1(IX-DIS-I)
           DISPLAY 'WRAN-INDIR-2             (' IX-DIS-I ') = '
                    WRAN-INDIR-2(IX-DIS-I)
           DISPLAY 'WRAN-INDIR-3             (' IX-DIS-I ') = '
                    WRAN-INDIR-3(IX-DIS-I)
           DISPLAY 'WRAN-TP-UTLZ-INDIR-1     (' IX-DIS-I ') = '
                    WRAN-TP-UTLZ-INDIR-1(IX-DIS-I)
           DISPLAY 'WRAN-TP-UTLZ-INDIR-2     (' IX-DIS-I ') = '
                    WRAN-TP-UTLZ-INDIR-2(IX-DIS-I)
           DISPLAY 'WRAN-TP-UTLZ-INDIR-3     (' IX-DIS-I ') = '
                    WRAN-TP-UTLZ-INDIR-3(IX-DIS-I)
           DISPLAY 'WRAN-ESTR-CNT-CORR-ACCR  (' IX-DIS-I ') = '
                    WRAN-ESTR-CNT-CORR-ACCR(IX-DIS-I)
           DISPLAY 'WRAN-ESTR-CNT-CORR-ADD   (' IX-DIS-I ') = '
                    WRAN-ESTR-CNT-CORR-ADD(IX-DIS-I)
           DISPLAY 'WRAN-ESTR-DOCTO          (' IX-DIS-I ') = '
                    WRAN-ESTR-DOCTO(IX-DIS-I)
           DISPLAY 'WRAN-PC-NEL-RAPP         (' IX-DIS-I ') = '
                    WRAN-PC-NEL-RAPP(IX-DIS-I)
           DISPLAY 'WRAN-TP-MEZ-PAG-ADD      (' IX-DIS-I ') = '
                    WRAN-TP-MEZ-PAG-ADD(IX-DIS-I)
           DISPLAY 'WRAN-TP-MEZ-PAG-ACCR     (' IX-DIS-I ') = '
                    WRAN-TP-MEZ-PAG-ACCR(IX-DIS-I)
           DISPLAY 'WRAN-COD-MATR            (' IX-DIS-I ') = '
                    WRAN-COD-MATR(IX-DIS-I)
           DISPLAY 'WRAN-TP-ADEGZ            (' IX-DIS-I ') = '
                    WRAN-TP-ADEGZ(IX-DIS-I)
           DISPLAY 'WRAN-FL-TST-RSH          (' IX-DIS-I ') = '
                    WRAN-FL-TST-RSH(IX-DIS-I)
           DISPLAY 'WRAN-COD-AZ              (' IX-DIS-I ') = '
                    WRAN-COD-AZ(IX-DIS-I)
           DISPLAY 'WRAN-IND-PRINC           (' IX-DIS-I ') = '
                    WRAN-IND-PRINC(IX-DIS-I)
           DISPLAY 'WRAN-DT-DELIBERA-CDA     (' IX-DIS-I ') = '
                    WRAN-DT-DELIBERA-CDA(IX-DIS-I)
           DISPLAY 'WRAN-DLG-AL-RISC         (' IX-DIS-I ') = '
                    WRAN-DLG-AL-RISC(IX-DIS-I)
           DISPLAY 'WRAN-LEGALE-RAPPR-PRINC  (' IX-DIS-I ') = '
                    WRAN-LEGALE-RAPPR-PRINC(IX-DIS-I)
           DISPLAY 'WRAN-TP-LEGALE-RAPPR     (' IX-DIS-I ') = '
                    WRAN-TP-LEGALE-RAPPR(IX-DIS-I)
           DISPLAY 'WRAN-TP-IND-PRINC        (' IX-DIS-I ') = '
                    WRAN-TP-IND-PRINC(IX-DIS-I)
           DISPLAY 'WRAN-TP-STAT-RID         (' IX-DIS-I ') = '
                    WRAN-TP-STAT-RID(IX-DIS-I)
           DISPLAY 'WRAN-NOME-INT-RID        (' IX-DIS-I ') = '
                    WRAN-NOME-INT-RID(IX-DIS-I)
           DISPLAY 'WRAN-COGN-INT-RID        (' IX-DIS-I ') = '
                    WRAN-COGN-INT-RID(IX-DIS-I)
           DISPLAY 'WRAN-COGN-INT-TRATT      (' IX-DIS-I ') = '
                    WRAN-COGN-INT-TRATT(IX-DIS-I)
           DISPLAY 'WRAN-NOME-INT-TRATT      (' IX-DIS-I ') = '
                    WRAN-NOME-INT-TRATT(IX-DIS-I)
           DISPLAY 'WRAN-CF-INT-RID          (' IX-DIS-I ') = '
                    WRAN-CF-INT-RID(IX-DIS-I)
           DISPLAY 'WRAN-FL-COINC-DIP-CNTR   (' IX-DIS-I ') = '
                    WRAN-FL-COINC-DIP-CNTR(IX-DIS-I)
           DISPLAY 'WRAN-FL-COINC-INT-CNTR   (' IX-DIS-I ') = '
                    WRAN-FL-COINC-INT-CNTR(IX-DIS-I)
           DISPLAY 'WRAN-DT-DECES            (' IX-DIS-I ') = '
                    WRAN-DT-DECES(IX-DIS-I)
           DISPLAY 'WRAN-FL-FUMATORE         (' IX-DIS-I ') = '
                    WRAN-FL-FUMATORE(IX-DIS-I)
           DISPLAY 'WRAN-DS-RIGA             (' IX-DIS-I ') = '
                    WRAN-DS-RIGA(IX-DIS-I)
           DISPLAY 'WRAN-DS-OPER-SQL         (' IX-DIS-I ') = '
                    WRAN-DS-OPER-SQL(IX-DIS-I)
           DISPLAY 'WRAN-DS-VER              (' IX-DIS-I ') = '
                    WRAN-DS-VER(IX-DIS-I)
           DISPLAY 'WRAN-DS-TS-INI-CPTZ      (' IX-DIS-I ') = '
                    WRAN-DS-TS-INI-CPTZ(IX-DIS-I)
           DISPLAY 'WRAN-DS-TS-END-CPTZ      (' IX-DIS-I ') = '
                    WRAN-DS-TS-END-CPTZ(IX-DIS-I)
           DISPLAY 'WRAN-DS-UTENTE           (' IX-DIS-I ') = '
                    WRAN-DS-UTENTE(IX-DIS-I)
           DISPLAY 'WRAN-DS-STATO-ELAB       (' IX-DIS-I ') = '
                    WRAN-DS-STATO-ELAB(IX-DIS-I).

       DISPLAY-TAB-RAN-EX.
           EXIT.

          03 LCCC1901-GAR-MAX               PIC 9(004).
          03 LCCC1901-COD-TARI-OCCURS       OCCURS 20.
             05 LCCC1901-COD-TARI           PIC X(012).

          03 LCCC1901-FL-ESEGUIBILE         PIC X(01).
             88 LCCC1901-FL-ESEGUIBILE-SI   VALUE 'S'.
             88 LCCC1901-FL-ESEGUIBILE-NO   VALUE 'N'.


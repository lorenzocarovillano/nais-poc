      *----------------------------------------------------------------*
      *    COPY      ..... LCCVNOT6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO NOTE OGGETTO
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-NOTE-OGG.

      *--> NOME TABELLA FISICA DB
           INITIALIZE NOTE-OGG.
      *--> NOME TABELLA FISICA DB
           MOVE 'NOTE-OGG'                   TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WNOT-ST-INV
              AND WNOT-ELE-NOTE-OGG-MAX NOT = 0

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WNOT-ST-ADD
      *--->          ESTRAZIONE E VALORIZZAZIONE SEQUENCE

                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK
                        MOVE S090-SEQ-TABELLA  TO WNOT-ID-PTF
                                                  NOT-ID-NOTE-OGG
                        MOVE WMOV-ID-PTF       TO NOT-ID-OGG
                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-INSERT   TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WNOT-ST-DEL

                     MOVE WNOT-ID-NOTE-OGG  TO NOT-ID-NOTE-OGG
                     MOVE WNOT-ID-OGG       TO NOT-ID-OGG

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-DELETE   TO TRUE

      *-->        MODIFICA
                  WHEN WNOT-ST-MOD

                     MOVE WNOT-ID-NOTE-OGG  TO NOT-ID-NOTE-OGG
                     MOVE WNOT-ID-OGG       TO NOT-ID-OGG

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-UPDATE   TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN NOTE OGGETTO
                 PERFORM VAL-DCLGEN-NOT
                    THRU VAL-DCLGEN-NOT-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-NOT
                    THRU VALORIZZA-AREA-DSH-NOT-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-NOTE-OGG-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-NOT.

      *--> DCLGEN TABELLA
           MOVE NOTE-OGG                TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-PRIMARY-KEY    TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-SENZA-STOR  TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-NOT-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVNOT5.

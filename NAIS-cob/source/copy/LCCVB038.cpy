         03 (SF)-LEN-TOT                       PIC S9(4) COMP-5.
         03 (SF)-REC-FISSO.
           05 (SF)-ID-BILA-TRCH-ESTR PIC S9(9)V     COMP-3.
           05 (SF)-COD-COMP-ANIA PIC S9(5)V     COMP-3.
           05 (SF)-ID-RICH-ESTRAZ-MAS PIC S9(9)V     COMP-3.
           05 (SF)-ID-RICH-ESTRAZ-AGG-IND PIC X.
           05 (SF)-ID-RICH-ESTRAZ-AGG PIC S9(9)V     COMP-3.
           05 (SF)-ID-RICH-ESTRAZ-AGG-NULL REDEFINES
              (SF)-ID-RICH-ESTRAZ-AGG   PIC X(5).
           05 (SF)-FL-SIMULAZIONE-IND PIC X.
           05 (SF)-FL-SIMULAZIONE PIC X(1).
           05 (SF)-FL-SIMULAZIONE-NULL REDEFINES
              (SF)-FL-SIMULAZIONE   PIC X(1).
           05 (SF)-DT-RIS   PIC X(10).
           05 (SF)-DT-PRODUZIONE   PIC X(10).
           05 (SF)-ID-POLI PIC S9(9)V     COMP-3.
           05 (SF)-ID-ADES PIC S9(9)V     COMP-3.
           05 (SF)-ID-GAR PIC S9(9)V     COMP-3.
           05 (SF)-ID-TRCH-DI-GAR PIC S9(9)V     COMP-3.
           05 (SF)-TP-FRM-ASSVA PIC X(2).
           05 (SF)-TP-RAMO-BILA PIC X(2).
           05 (SF)-TP-CALC-RIS PIC X(2).
           05 (SF)-COD-RAMO PIC X(12).
           05 (SF)-COD-TARI PIC X(12).
           05 (SF)-DT-INI-VAL-TAR-IND   PIC X.
           05 (SF)-DT-INI-VAL-TAR   PIC X(10).
           05 (SF)-DT-INI-VAL-TAR-NULL REDEFINES
              (SF)-DT-INI-VAL-TAR   PIC X(10).
           05 (SF)-COD-PROD-IND PIC X.
           05 (SF)-COD-PROD PIC X(12).
           05 (SF)-COD-PROD-NULL REDEFINES
              (SF)-COD-PROD   PIC X(12).
           05 (SF)-DT-INI-VLDT-PROD   PIC X(10).
           05 (SF)-COD-TARI-ORGN-IND PIC X.
           05 (SF)-COD-TARI-ORGN PIC X(12).
           05 (SF)-COD-TARI-ORGN-NULL REDEFINES
              (SF)-COD-TARI-ORGN   PIC X(12).
           05 (SF)-MIN-GARTO-T-IND PIC X.
           05 (SF)-MIN-GARTO-T PIC S9(12)V9(3) COMP-3.
           05 (SF)-MIN-GARTO-T-NULL REDEFINES
              (SF)-MIN-GARTO-T   PIC X(8).
           05 (SF)-TP-TARI-IND PIC X.
           05 (SF)-TP-TARI PIC X(2).
           05 (SF)-TP-TARI-NULL REDEFINES
              (SF)-TP-TARI   PIC X(2).
           05 (SF)-TP-PRE-IND PIC X.
           05 (SF)-TP-PRE PIC X(1).
           05 (SF)-TP-PRE-NULL REDEFINES
              (SF)-TP-PRE   PIC X(1).
           05 (SF)-TP-ADEG-PRE-IND PIC X.
           05 (SF)-TP-ADEG-PRE PIC X(1).
           05 (SF)-TP-ADEG-PRE-NULL REDEFINES
              (SF)-TP-ADEG-PRE   PIC X(1).
           05 (SF)-TP-RIVAL-IND PIC X.
           05 (SF)-TP-RIVAL PIC X(2).
           05 (SF)-TP-RIVAL-NULL REDEFINES
              (SF)-TP-RIVAL   PIC X(2).
           05 (SF)-FL-DA-TRASF-IND PIC X.
           05 (SF)-FL-DA-TRASF PIC X(1).
           05 (SF)-FL-DA-TRASF-NULL REDEFINES
              (SF)-FL-DA-TRASF   PIC X(1).
           05 (SF)-FL-CAR-CONT-IND PIC X.
           05 (SF)-FL-CAR-CONT PIC X(1).
           05 (SF)-FL-CAR-CONT-NULL REDEFINES
              (SF)-FL-CAR-CONT   PIC X(1).
           05 (SF)-FL-PRE-DA-RIS-IND PIC X.
           05 (SF)-FL-PRE-DA-RIS PIC X(1).
           05 (SF)-FL-PRE-DA-RIS-NULL REDEFINES
              (SF)-FL-PRE-DA-RIS   PIC X(1).
           05 (SF)-FL-PRE-AGG-IND PIC X.
           05 (SF)-FL-PRE-AGG PIC X(1).
           05 (SF)-FL-PRE-AGG-NULL REDEFINES
              (SF)-FL-PRE-AGG   PIC X(1).
           05 (SF)-TP-TRCH-IND PIC X.
           05 (SF)-TP-TRCH PIC X(2).
           05 (SF)-TP-TRCH-NULL REDEFINES
              (SF)-TP-TRCH   PIC X(2).
           05 (SF)-TP-TST-IND PIC X.
           05 (SF)-TP-TST PIC X(2).
           05 (SF)-TP-TST-NULL REDEFINES
              (SF)-TP-TST   PIC X(2).
           05 (SF)-COD-CONV-IND PIC X.
           05 (SF)-COD-CONV PIC X(12).
           05 (SF)-COD-CONV-NULL REDEFINES
              (SF)-COD-CONV   PIC X(12).
           05 (SF)-DT-DECOR-POLI   PIC X(10).
           05 (SF)-DT-DECOR-ADES-IND   PIC X.
           05 (SF)-DT-DECOR-ADES   PIC X(10).
           05 (SF)-DT-DECOR-ADES-NULL REDEFINES
              (SF)-DT-DECOR-ADES   PIC X(10).
           05 (SF)-DT-DECOR-TRCH   PIC X(10).
           05 (SF)-DT-EMIS-POLI   PIC X(10).
           05 (SF)-DT-EMIS-TRCH-IND   PIC X.
           05 (SF)-DT-EMIS-TRCH   PIC X(10).
           05 (SF)-DT-EMIS-TRCH-NULL REDEFINES
              (SF)-DT-EMIS-TRCH   PIC X(10).
           05 (SF)-DT-SCAD-TRCH-IND   PIC X.
           05 (SF)-DT-SCAD-TRCH   PIC X(10).
           05 (SF)-DT-SCAD-TRCH-NULL REDEFINES
              (SF)-DT-SCAD-TRCH   PIC X(10).
           05 (SF)-DT-SCAD-INTMD-IND   PIC X.
           05 (SF)-DT-SCAD-INTMD   PIC X(10).
           05 (SF)-DT-SCAD-INTMD-NULL REDEFINES
              (SF)-DT-SCAD-INTMD   PIC X(10).
           05 (SF)-DT-SCAD-PAG-PRE-IND   PIC X.
           05 (SF)-DT-SCAD-PAG-PRE   PIC X(10).
           05 (SF)-DT-SCAD-PAG-PRE-NULL REDEFINES
              (SF)-DT-SCAD-PAG-PRE   PIC X(10).
           05 (SF)-DT-ULT-PRE-PAG-IND   PIC X.
           05 (SF)-DT-ULT-PRE-PAG   PIC X(10).
           05 (SF)-DT-ULT-PRE-PAG-NULL REDEFINES
              (SF)-DT-ULT-PRE-PAG   PIC X(10).
           05 (SF)-DT-NASC-1O-ASSTO-IND   PIC X.
           05 (SF)-DT-NASC-1O-ASSTO   PIC X(10).
           05 (SF)-DT-NASC-1O-ASSTO-NULL REDEFINES
              (SF)-DT-NASC-1O-ASSTO   PIC X(10).
           05 (SF)-SEX-1O-ASSTO-IND PIC X.
           05 (SF)-SEX-1O-ASSTO PIC X(1).
           05 (SF)-SEX-1O-ASSTO-NULL REDEFINES
              (SF)-SEX-1O-ASSTO   PIC X(1).
           05 (SF)-ETA-AA-1O-ASSTO-IND PIC X.
           05 (SF)-ETA-AA-1O-ASSTO PIC S9(5)V     COMP-3.
           05 (SF)-ETA-AA-1O-ASSTO-NULL REDEFINES
              (SF)-ETA-AA-1O-ASSTO   PIC X(3).
           05 (SF)-ETA-MM-1O-ASSTO-IND PIC X.
           05 (SF)-ETA-MM-1O-ASSTO PIC S9(5)V     COMP-3.
           05 (SF)-ETA-MM-1O-ASSTO-NULL REDEFINES
              (SF)-ETA-MM-1O-ASSTO   PIC X(3).
           05 (SF)-ETA-RAGGN-DT-CALC-IND PIC X.
           05 (SF)-ETA-RAGGN-DT-CALC PIC S9(4)V9(3) COMP-3.
           05 (SF)-ETA-RAGGN-DT-CALC-NULL REDEFINES
              (SF)-ETA-RAGGN-DT-CALC   PIC X(4).
           05 (SF)-DUR-AA-IND PIC X.
           05 (SF)-DUR-AA PIC S9(5)V     COMP-3.
           05 (SF)-DUR-AA-NULL REDEFINES
              (SF)-DUR-AA   PIC X(3).
           05 (SF)-DUR-MM-IND PIC X.
           05 (SF)-DUR-MM PIC S9(5)V     COMP-3.
           05 (SF)-DUR-MM-NULL REDEFINES
              (SF)-DUR-MM   PIC X(3).
           05 (SF)-DUR-GG-IND PIC X.
           05 (SF)-DUR-GG PIC S9(5)V     COMP-3.
           05 (SF)-DUR-GG-NULL REDEFINES
              (SF)-DUR-GG   PIC X(3).
           05 (SF)-DUR-1O-PER-AA-IND PIC X.
           05 (SF)-DUR-1O-PER-AA PIC S9(5)V     COMP-3.
           05 (SF)-DUR-1O-PER-AA-NULL REDEFINES
              (SF)-DUR-1O-PER-AA   PIC X(3).
           05 (SF)-DUR-1O-PER-MM-IND PIC X.
           05 (SF)-DUR-1O-PER-MM PIC S9(5)V     COMP-3.
           05 (SF)-DUR-1O-PER-MM-NULL REDEFINES
              (SF)-DUR-1O-PER-MM   PIC X(3).
           05 (SF)-DUR-1O-PER-GG-IND PIC X.
           05 (SF)-DUR-1O-PER-GG PIC S9(5)V     COMP-3.
           05 (SF)-DUR-1O-PER-GG-NULL REDEFINES
              (SF)-DUR-1O-PER-GG   PIC X(3).
           05 (SF)-ANTIDUR-RICOR-PREC-IND PIC X.
           05 (SF)-ANTIDUR-RICOR-PREC PIC S9(5)V     COMP-3.
           05 (SF)-ANTIDUR-RICOR-PREC-NULL REDEFINES
              (SF)-ANTIDUR-RICOR-PREC   PIC X(3).
           05 (SF)-ANTIDUR-DT-CALC-IND PIC X.
           05 (SF)-ANTIDUR-DT-CALC PIC S9(4)V9(7) COMP-3.
           05 (SF)-ANTIDUR-DT-CALC-NULL REDEFINES
              (SF)-ANTIDUR-DT-CALC   PIC X(6).
           05 (SF)-DUR-RES-DT-CALC-IND PIC X.
           05 (SF)-DUR-RES-DT-CALC PIC S9(4)V9(7) COMP-3.
           05 (SF)-DUR-RES-DT-CALC-NULL REDEFINES
              (SF)-DUR-RES-DT-CALC   PIC X(6).
           05 (SF)-TP-STAT-BUS-POLI PIC X(2).
           05 (SF)-TP-CAUS-POLI PIC X(2).
           05 (SF)-TP-STAT-BUS-ADES PIC X(2).
           05 (SF)-TP-CAUS-ADES PIC X(2).
           05 (SF)-TP-STAT-BUS-TRCH PIC X(2).
           05 (SF)-TP-CAUS-TRCH PIC X(2).
           05 (SF)-DT-EFF-CAMB-STAT-IND   PIC X.
           05 (SF)-DT-EFF-CAMB-STAT   PIC X(10).
           05 (SF)-DT-EFF-CAMB-STAT-NULL REDEFINES
              (SF)-DT-EFF-CAMB-STAT   PIC X(10).
           05 (SF)-DT-EMIS-CAMB-STAT-IND   PIC X.
           05 (SF)-DT-EMIS-CAMB-STAT   PIC X(10).
           05 (SF)-DT-EMIS-CAMB-STAT-NULL REDEFINES
              (SF)-DT-EMIS-CAMB-STAT   PIC X(10).
           05 (SF)-DT-EFF-STAB-IND   PIC X.
           05 (SF)-DT-EFF-STAB   PIC X(10).
           05 (SF)-DT-EFF-STAB-NULL REDEFINES
              (SF)-DT-EFF-STAB   PIC X(10).
           05 (SF)-CPT-DT-STAB-IND PIC X.
           05 (SF)-CPT-DT-STAB PIC S9(12)V9(3) COMP-3.
           05 (SF)-CPT-DT-STAB-NULL REDEFINES
              (SF)-CPT-DT-STAB   PIC X(8).
           05 (SF)-DT-EFF-RIDZ-IND   PIC X.
           05 (SF)-DT-EFF-RIDZ   PIC X(10).
           05 (SF)-DT-EFF-RIDZ-NULL REDEFINES
              (SF)-DT-EFF-RIDZ   PIC X(10).
           05 (SF)-DT-EMIS-RIDZ-IND   PIC X.
           05 (SF)-DT-EMIS-RIDZ   PIC X(10).
           05 (SF)-DT-EMIS-RIDZ-NULL REDEFINES
              (SF)-DT-EMIS-RIDZ   PIC X(10).
           05 (SF)-CPT-DT-RIDZ-IND PIC X.
           05 (SF)-CPT-DT-RIDZ PIC S9(12)V9(3) COMP-3.
           05 (SF)-CPT-DT-RIDZ-NULL REDEFINES
              (SF)-CPT-DT-RIDZ   PIC X(8).
           05 (SF)-FRAZ-IND PIC X.
           05 (SF)-FRAZ PIC S9(5)V     COMP-3.
           05 (SF)-FRAZ-NULL REDEFINES
              (SF)-FRAZ   PIC X(3).
           05 (SF)-DUR-PAG-PRE-IND PIC X.
           05 (SF)-DUR-PAG-PRE PIC S9(5)V     COMP-3.
           05 (SF)-DUR-PAG-PRE-NULL REDEFINES
              (SF)-DUR-PAG-PRE   PIC X(3).
           05 (SF)-NUM-PRE-PATT-IND PIC X.
           05 (SF)-NUM-PRE-PATT PIC S9(5)V     COMP-3.
           05 (SF)-NUM-PRE-PATT-NULL REDEFINES
              (SF)-NUM-PRE-PATT   PIC X(3).
           05 (SF)-FRAZ-INI-EROG-REN-IND PIC X.
           05 (SF)-FRAZ-INI-EROG-REN PIC S9(5)V     COMP-3.
           05 (SF)-FRAZ-INI-EROG-REN-NULL REDEFINES
              (SF)-FRAZ-INI-EROG-REN   PIC X(3).
           05 (SF)-AA-REN-CER-IND PIC X.
           05 (SF)-AA-REN-CER PIC S9(5)V     COMP-3.
           05 (SF)-AA-REN-CER-NULL REDEFINES
              (SF)-AA-REN-CER   PIC X(3).
           05 (SF)-RAT-REN-IND PIC X.
           05 (SF)-RAT-REN PIC S9(12)V9(3) COMP-3.
           05 (SF)-RAT-REN-NULL REDEFINES
              (SF)-RAT-REN   PIC X(8).
           05 (SF)-COD-DIV-IND PIC X.
           05 (SF)-COD-DIV PIC X(20).
           05 (SF)-COD-DIV-NULL REDEFINES
              (SF)-COD-DIV   PIC X(20).
           05 (SF)-RISCPAR-IND PIC X.
           05 (SF)-RISCPAR PIC S9(12)V9(3) COMP-3.
           05 (SF)-RISCPAR-NULL REDEFINES
              (SF)-RISCPAR   PIC X(8).
           05 (SF)-CUM-RISCPAR-IND PIC X.
           05 (SF)-CUM-RISCPAR PIC S9(12)V9(3) COMP-3.
           05 (SF)-CUM-RISCPAR-NULL REDEFINES
              (SF)-CUM-RISCPAR   PIC X(8).
           05 (SF)-ULT-RM-IND PIC X.
           05 (SF)-ULT-RM PIC S9(12)V9(3) COMP-3.
           05 (SF)-ULT-RM-NULL REDEFINES
              (SF)-ULT-RM   PIC X(8).
           05 (SF)-TS-RENDTO-T-IND PIC X.
           05 (SF)-TS-RENDTO-T PIC S9(5)V9(9) COMP-3.
           05 (SF)-TS-RENDTO-T-NULL REDEFINES
              (SF)-TS-RENDTO-T   PIC X(8).
           05 (SF)-ALQ-RETR-T-IND PIC X.
           05 (SF)-ALQ-RETR-T PIC S9(3)V9(3) COMP-3.
           05 (SF)-ALQ-RETR-T-NULL REDEFINES
              (SF)-ALQ-RETR-T   PIC X(4).
           05 (SF)-MIN-TRNUT-T-IND PIC X.
           05 (SF)-MIN-TRNUT-T PIC S9(12)V9(3) COMP-3.
           05 (SF)-MIN-TRNUT-T-NULL REDEFINES
              (SF)-MIN-TRNUT-T   PIC X(8).
           05 (SF)-TS-NET-T-IND PIC X.
           05 (SF)-TS-NET-T PIC S9(5)V9(9) COMP-3.
           05 (SF)-TS-NET-T-NULL REDEFINES
              (SF)-TS-NET-T   PIC X(8).
           05 (SF)-DT-ULT-RIVAL-IND   PIC X.
           05 (SF)-DT-ULT-RIVAL   PIC X(10).
           05 (SF)-DT-ULT-RIVAL-NULL REDEFINES
              (SF)-DT-ULT-RIVAL   PIC X(10).
           05 (SF)-PRSTZ-INI-IND PIC X.
           05 (SF)-PRSTZ-INI PIC S9(12)V9(3) COMP-3.
           05 (SF)-PRSTZ-INI-NULL REDEFINES
              (SF)-PRSTZ-INI   PIC X(8).
           05 (SF)-PRSTZ-AGG-INI-IND PIC X.
           05 (SF)-PRSTZ-AGG-INI PIC S9(12)V9(3) COMP-3.
           05 (SF)-PRSTZ-AGG-INI-NULL REDEFINES
              (SF)-PRSTZ-AGG-INI   PIC X(8).
           05 (SF)-PRSTZ-AGG-ULT-IND PIC X.
           05 (SF)-PRSTZ-AGG-ULT PIC S9(12)V9(3) COMP-3.
           05 (SF)-PRSTZ-AGG-ULT-NULL REDEFINES
              (SF)-PRSTZ-AGG-ULT   PIC X(8).
           05 (SF)-RAPPEL-IND PIC X.
           05 (SF)-RAPPEL PIC S9(12)V9(3) COMP-3.
           05 (SF)-RAPPEL-NULL REDEFINES
              (SF)-RAPPEL   PIC X(8).
           05 (SF)-PRE-PATTUITO-INI-IND PIC X.
           05 (SF)-PRE-PATTUITO-INI PIC S9(12)V9(3) COMP-3.
           05 (SF)-PRE-PATTUITO-INI-NULL REDEFINES
              (SF)-PRE-PATTUITO-INI   PIC X(8).
           05 (SF)-PRE-DOV-INI-IND PIC X.
           05 (SF)-PRE-DOV-INI PIC S9(12)V9(3) COMP-3.
           05 (SF)-PRE-DOV-INI-NULL REDEFINES
              (SF)-PRE-DOV-INI   PIC X(8).
           05 (SF)-PRE-DOV-RIVTO-T-IND PIC X.
           05 (SF)-PRE-DOV-RIVTO-T PIC S9(12)V9(3) COMP-3.
           05 (SF)-PRE-DOV-RIVTO-T-NULL REDEFINES
              (SF)-PRE-DOV-RIVTO-T   PIC X(8).
           05 (SF)-PRE-ANNUALIZ-RICOR-IND PIC X.
           05 (SF)-PRE-ANNUALIZ-RICOR PIC S9(12)V9(3) COMP-3.
           05 (SF)-PRE-ANNUALIZ-RICOR-NULL REDEFINES
              (SF)-PRE-ANNUALIZ-RICOR   PIC X(8).
           05 (SF)-PRE-CONT-IND PIC X.
           05 (SF)-PRE-CONT PIC S9(12)V9(3) COMP-3.
           05 (SF)-PRE-CONT-NULL REDEFINES
              (SF)-PRE-CONT   PIC X(8).
           05 (SF)-PRE-PP-INI-IND PIC X.
           05 (SF)-PRE-PP-INI PIC S9(12)V9(3) COMP-3.
           05 (SF)-PRE-PP-INI-NULL REDEFINES
              (SF)-PRE-PP-INI   PIC X(8).
           05 (SF)-RIS-PURA-T-IND PIC X.
           05 (SF)-RIS-PURA-T PIC S9(12)V9(3) COMP-3.
           05 (SF)-RIS-PURA-T-NULL REDEFINES
              (SF)-RIS-PURA-T   PIC X(8).
           05 (SF)-PROV-ACQ-IND PIC X.
           05 (SF)-PROV-ACQ PIC S9(12)V9(3) COMP-3.
           05 (SF)-PROV-ACQ-NULL REDEFINES
              (SF)-PROV-ACQ   PIC X(8).
           05 (SF)-PROV-ACQ-RICOR-IND PIC X.
           05 (SF)-PROV-ACQ-RICOR PIC S9(12)V9(3) COMP-3.
           05 (SF)-PROV-ACQ-RICOR-NULL REDEFINES
              (SF)-PROV-ACQ-RICOR   PIC X(8).
           05 (SF)-PROV-INC-IND PIC X.
           05 (SF)-PROV-INC PIC S9(12)V9(3) COMP-3.
           05 (SF)-PROV-INC-NULL REDEFINES
              (SF)-PROV-INC   PIC X(8).
           05 (SF)-CAR-ACQ-NON-SCON-IND PIC X.
           05 (SF)-CAR-ACQ-NON-SCON PIC S9(12)V9(3) COMP-3.
           05 (SF)-CAR-ACQ-NON-SCON-NULL REDEFINES
              (SF)-CAR-ACQ-NON-SCON   PIC X(8).
           05 (SF)-OVER-COMM-IND PIC X.
           05 (SF)-OVER-COMM PIC S9(12)V9(3) COMP-3.
           05 (SF)-OVER-COMM-NULL REDEFINES
              (SF)-OVER-COMM   PIC X(8).
           05 (SF)-CAR-ACQ-PRECONTATO-IND PIC X.
           05 (SF)-CAR-ACQ-PRECONTATO PIC S9(12)V9(3) COMP-3.
           05 (SF)-CAR-ACQ-PRECONTATO-NULL REDEFINES
              (SF)-CAR-ACQ-PRECONTATO   PIC X(8).
           05 (SF)-RIS-ACQ-T-IND PIC X.
           05 (SF)-RIS-ACQ-T PIC S9(12)V9(3) COMP-3.
           05 (SF)-RIS-ACQ-T-NULL REDEFINES
              (SF)-RIS-ACQ-T   PIC X(8).
           05 (SF)-RIS-ZIL-T-IND PIC X.
           05 (SF)-RIS-ZIL-T PIC S9(12)V9(3) COMP-3.
           05 (SF)-RIS-ZIL-T-NULL REDEFINES
              (SF)-RIS-ZIL-T   PIC X(8).
           05 (SF)-CAR-GEST-NON-SCON-IND PIC X.
           05 (SF)-CAR-GEST-NON-SCON PIC S9(12)V9(3) COMP-3.
           05 (SF)-CAR-GEST-NON-SCON-NULL REDEFINES
              (SF)-CAR-GEST-NON-SCON   PIC X(8).
           05 (SF)-CAR-GEST-IND PIC X.
           05 (SF)-CAR-GEST PIC S9(12)V9(3) COMP-3.
           05 (SF)-CAR-GEST-NULL REDEFINES
              (SF)-CAR-GEST   PIC X(8).
           05 (SF)-RIS-SPE-T-IND PIC X.
           05 (SF)-RIS-SPE-T PIC S9(12)V9(3) COMP-3.
           05 (SF)-RIS-SPE-T-NULL REDEFINES
              (SF)-RIS-SPE-T   PIC X(8).
           05 (SF)-CAR-INC-NON-SCON-IND PIC X.
           05 (SF)-CAR-INC-NON-SCON PIC S9(12)V9(3) COMP-3.
           05 (SF)-CAR-INC-NON-SCON-NULL REDEFINES
              (SF)-CAR-INC-NON-SCON   PIC X(8).
           05 (SF)-CAR-INC-IND PIC X.
           05 (SF)-CAR-INC PIC S9(12)V9(3) COMP-3.
           05 (SF)-CAR-INC-NULL REDEFINES
              (SF)-CAR-INC   PIC X(8).
           05 (SF)-RIS-RISTORNI-CAP-IND PIC X.
           05 (SF)-RIS-RISTORNI-CAP PIC S9(12)V9(3) COMP-3.
           05 (SF)-RIS-RISTORNI-CAP-NULL REDEFINES
              (SF)-RIS-RISTORNI-CAP   PIC X(8).
           05 (SF)-INTR-TECN-IND PIC X.
           05 (SF)-INTR-TECN PIC S9(3)V9(3) COMP-3.
           05 (SF)-INTR-TECN-NULL REDEFINES
              (SF)-INTR-TECN   PIC X(4).
           05 (SF)-CPT-RSH-MOR-IND PIC X.
           05 (SF)-CPT-RSH-MOR PIC S9(12)V9(3) COMP-3.
           05 (SF)-CPT-RSH-MOR-NULL REDEFINES
              (SF)-CPT-RSH-MOR   PIC X(8).
           05 (SF)-C-SUBRSH-T-IND PIC X.
           05 (SF)-C-SUBRSH-T PIC S9(12)V9(3) COMP-3.
           05 (SF)-C-SUBRSH-T-NULL REDEFINES
              (SF)-C-SUBRSH-T   PIC X(8).
           05 (SF)-PRE-RSH-T-IND PIC X.
           05 (SF)-PRE-RSH-T PIC S9(12)V9(3) COMP-3.
           05 (SF)-PRE-RSH-T-NULL REDEFINES
              (SF)-PRE-RSH-T   PIC X(8).
           05 (SF)-ALQ-MARG-RIS-IND PIC X.
           05 (SF)-ALQ-MARG-RIS PIC S9(3)V9(3) COMP-3.
           05 (SF)-ALQ-MARG-RIS-NULL REDEFINES
              (SF)-ALQ-MARG-RIS   PIC X(4).
           05 (SF)-ALQ-MARG-C-SUBRSH-IND PIC X.
           05 (SF)-ALQ-MARG-C-SUBRSH PIC S9(3)V9(3) COMP-3.
           05 (SF)-ALQ-MARG-C-SUBRSH-NULL REDEFINES
              (SF)-ALQ-MARG-C-SUBRSH   PIC X(4).
           05 (SF)-TS-RENDTO-SPPR-IND PIC X.
           05 (SF)-TS-RENDTO-SPPR PIC S9(5)V9(9) COMP-3.
           05 (SF)-TS-RENDTO-SPPR-NULL REDEFINES
              (SF)-TS-RENDTO-SPPR   PIC X(8).
           05 (SF)-TP-IAS-IND PIC X.
           05 (SF)-TP-IAS PIC X(2).
           05 (SF)-TP-IAS-NULL REDEFINES
              (SF)-TP-IAS   PIC X(2).
           05 (SF)-NS-QUO-IND PIC X.
           05 (SF)-NS-QUO PIC S9(3)V9(3) COMP-3.
           05 (SF)-NS-QUO-NULL REDEFINES
              (SF)-NS-QUO   PIC X(4).
           05 (SF)-TS-MEDIO-IND PIC X.
           05 (SF)-TS-MEDIO PIC S9(5)V9(9) COMP-3.
           05 (SF)-TS-MEDIO-NULL REDEFINES
              (SF)-TS-MEDIO   PIC X(8).
           05 (SF)-CPT-RIASTO-IND PIC X.
           05 (SF)-CPT-RIASTO PIC S9(12)V9(3) COMP-3.
           05 (SF)-CPT-RIASTO-NULL REDEFINES
              (SF)-CPT-RIASTO   PIC X(8).
           05 (SF)-PRE-RIASTO-IND PIC X.
           05 (SF)-PRE-RIASTO PIC S9(12)V9(3) COMP-3.
           05 (SF)-PRE-RIASTO-NULL REDEFINES
              (SF)-PRE-RIASTO   PIC X(8).
           05 (SF)-RIS-RIASTA-IND PIC X.
           05 (SF)-RIS-RIASTA PIC S9(12)V9(3) COMP-3.
           05 (SF)-RIS-RIASTA-NULL REDEFINES
              (SF)-RIS-RIASTA   PIC X(8).
           05 (SF)-CPT-RIASTO-ECC-IND PIC X.
           05 (SF)-CPT-RIASTO-ECC PIC S9(12)V9(3) COMP-3.
           05 (SF)-CPT-RIASTO-ECC-NULL REDEFINES
              (SF)-CPT-RIASTO-ECC   PIC X(8).
           05 (SF)-PRE-RIASTO-ECC-IND PIC X.
           05 (SF)-PRE-RIASTO-ECC PIC S9(12)V9(3) COMP-3.
           05 (SF)-PRE-RIASTO-ECC-NULL REDEFINES
              (SF)-PRE-RIASTO-ECC   PIC X(8).
           05 (SF)-RIS-RIASTA-ECC-IND PIC X.
           05 (SF)-RIS-RIASTA-ECC PIC S9(12)V9(3) COMP-3.
           05 (SF)-RIS-RIASTA-ECC-NULL REDEFINES
              (SF)-RIS-RIASTA-ECC   PIC X(8).
           05 (SF)-COD-AGE-IND PIC X.
           05 (SF)-COD-AGE PIC S9(5)V     COMP-3.
           05 (SF)-COD-AGE-NULL REDEFINES
              (SF)-COD-AGE   PIC X(3).
           05 (SF)-COD-SUBAGE-IND PIC X.
           05 (SF)-COD-SUBAGE PIC S9(5)V     COMP-3.
           05 (SF)-COD-SUBAGE-NULL REDEFINES
              (SF)-COD-SUBAGE   PIC X(3).
           05 (SF)-COD-CAN-IND PIC X.
           05 (SF)-COD-CAN PIC S9(5)V     COMP-3.
           05 (SF)-COD-CAN-NULL REDEFINES
              (SF)-COD-CAN   PIC X(3).
           05 (SF)-IB-POLI-IND PIC X.
           05 (SF)-IB-POLI PIC X(40).
           05 (SF)-IB-POLI-NULL REDEFINES
              (SF)-IB-POLI   PIC X(40).
           05 (SF)-IB-ADES-IND PIC X.
           05 (SF)-IB-ADES PIC X(40).
           05 (SF)-IB-ADES-NULL REDEFINES
              (SF)-IB-ADES   PIC X(40).
           05 (SF)-IB-TRCH-DI-GAR-IND PIC X.
           05 (SF)-IB-TRCH-DI-GAR PIC X(40).
           05 (SF)-IB-TRCH-DI-GAR-NULL REDEFINES
              (SF)-IB-TRCH-DI-GAR   PIC X(40).
           05 (SF)-TP-PRSTZ-IND PIC X.
           05 (SF)-TP-PRSTZ PIC X(2).
           05 (SF)-TP-PRSTZ-NULL REDEFINES
              (SF)-TP-PRSTZ   PIC X(2).
           05 (SF)-TP-TRASF-IND PIC X.
           05 (SF)-TP-TRASF PIC X(2).
           05 (SF)-TP-TRASF-NULL REDEFINES
              (SF)-TP-TRASF   PIC X(2).
           05 (SF)-PP-INVRIO-TARI-IND PIC X.
           05 (SF)-PP-INVRIO-TARI PIC X(1).
           05 (SF)-PP-INVRIO-TARI-NULL REDEFINES
              (SF)-PP-INVRIO-TARI   PIC X(1).
           05 (SF)-COEFF-OPZ-REN-IND PIC X.
           05 (SF)-COEFF-OPZ-REN PIC S9(3)V9(3) COMP-3.
           05 (SF)-COEFF-OPZ-REN-NULL REDEFINES
              (SF)-COEFF-OPZ-REN   PIC X(4).
           05 (SF)-COEFF-OPZ-CPT-IND PIC X.
           05 (SF)-COEFF-OPZ-CPT PIC S9(3)V9(3) COMP-3.
           05 (SF)-COEFF-OPZ-CPT-NULL REDEFINES
              (SF)-COEFF-OPZ-CPT   PIC X(4).
           05 (SF)-DUR-PAG-REN-IND PIC X.
           05 (SF)-DUR-PAG-REN PIC S9(5)V     COMP-3.
           05 (SF)-DUR-PAG-REN-NULL REDEFINES
              (SF)-DUR-PAG-REN   PIC X(3).
           05 (SF)-VLT-IND PIC X.
           05 (SF)-VLT PIC X(3).
           05 (SF)-VLT-NULL REDEFINES
              (SF)-VLT   PIC X(3).
           05 (SF)-RIS-MAT-CHIU-PREC-IND PIC X.
           05 (SF)-RIS-MAT-CHIU-PREC PIC S9(12)V9(3) COMP-3.
           05 (SF)-RIS-MAT-CHIU-PREC-NULL REDEFINES
              (SF)-RIS-MAT-CHIU-PREC   PIC X(8).
           05 (SF)-COD-FND-IND PIC X.
           05 (SF)-COD-FND PIC X(12).
           05 (SF)-COD-FND-NULL REDEFINES
              (SF)-COD-FND   PIC X(12).
           05 (SF)-PRSTZ-T-IND PIC X.
           05 (SF)-PRSTZ-T PIC S9(12)V9(3) COMP-3.
           05 (SF)-PRSTZ-T-NULL REDEFINES
              (SF)-PRSTZ-T   PIC X(8).
           05 (SF)-TS-TARI-DOV-IND PIC X.
           05 (SF)-TS-TARI-DOV PIC S9(5)V9(9) COMP-3.
           05 (SF)-TS-TARI-DOV-NULL REDEFINES
              (SF)-TS-TARI-DOV   PIC X(8).
           05 (SF)-TS-TARI-SCON-IND PIC X.
           05 (SF)-TS-TARI-SCON PIC S9(5)V9(9) COMP-3.
           05 (SF)-TS-TARI-SCON-NULL REDEFINES
              (SF)-TS-TARI-SCON   PIC X(8).
           05 (SF)-TS-PP-IND PIC X.
           05 (SF)-TS-PP PIC S9(5)V9(9) COMP-3.
           05 (SF)-TS-PP-NULL REDEFINES
              (SF)-TS-PP   PIC X(8).
           05 (SF)-COEFF-RIS-1-T-IND PIC X.
           05 (SF)-COEFF-RIS-1-T PIC S9(5)V9(9) COMP-3.
           05 (SF)-COEFF-RIS-1-T-NULL REDEFINES
              (SF)-COEFF-RIS-1-T   PIC X(8).
           05 (SF)-COEFF-RIS-2-T-IND PIC X.
           05 (SF)-COEFF-RIS-2-T PIC S9(5)V9(9) COMP-3.
           05 (SF)-COEFF-RIS-2-T-NULL REDEFINES
              (SF)-COEFF-RIS-2-T   PIC X(8).
           05 (SF)-ABB-IND PIC X.
           05 (SF)-ABB PIC S9(12)V9(3) COMP-3.
           05 (SF)-ABB-NULL REDEFINES
              (SF)-ABB   PIC X(8).
           05 (SF)-TP-COASS-IND PIC X.
           05 (SF)-TP-COASS PIC X(2).
           05 (SF)-TP-COASS-NULL REDEFINES
              (SF)-TP-COASS   PIC X(2).
           05 (SF)-TRAT-RIASS-IND PIC X.
           05 (SF)-TRAT-RIASS PIC X(12).
           05 (SF)-TRAT-RIASS-NULL REDEFINES
              (SF)-TRAT-RIASS   PIC X(12).
           05 (SF)-TRAT-RIASS-ECC-IND PIC X.
           05 (SF)-TRAT-RIASS-ECC PIC X(12).
           05 (SF)-TRAT-RIASS-ECC-NULL REDEFINES
              (SF)-TRAT-RIASS-ECC   PIC X(12).
           05 (SF)-DS-OPER-SQL PIC X(1).
           05 (SF)-DS-VER PIC S9(9)V     COMP-3.
           05 (SF)-DS-TS-CPTZ PIC S9(18)V     COMP-3.
           05 (SF)-DS-UTENTE PIC X(20).
           05 (SF)-DS-STATO-ELAB PIC X(1).
           05 (SF)-TP-RGM-FISC-IND PIC X.
           05 (SF)-TP-RGM-FISC PIC X(2).
           05 (SF)-TP-RGM-FISC-NULL REDEFINES
              (SF)-TP-RGM-FISC   PIC X(2).
           05 (SF)-DUR-GAR-AA-IND PIC X.
           05 (SF)-DUR-GAR-AA PIC S9(5)V     COMP-3.
           05 (SF)-DUR-GAR-AA-NULL REDEFINES
              (SF)-DUR-GAR-AA   PIC X(3).
           05 (SF)-DUR-GAR-MM-IND PIC X.
           05 (SF)-DUR-GAR-MM PIC S9(5)V     COMP-3.
           05 (SF)-DUR-GAR-MM-NULL REDEFINES
              (SF)-DUR-GAR-MM   PIC X(3).
           05 (SF)-DUR-GAR-GG-IND PIC X.
           05 (SF)-DUR-GAR-GG PIC S9(5)V     COMP-3.
           05 (SF)-DUR-GAR-GG-NULL REDEFINES
              (SF)-DUR-GAR-GG   PIC X(3).
           05 (SF)-ANTIDUR-CALC-365-IND PIC X.
           05 (SF)-ANTIDUR-CALC-365 PIC S9(4)V9(7) COMP-3.
           05 (SF)-ANTIDUR-CALC-365-NULL REDEFINES
              (SF)-ANTIDUR-CALC-365   PIC X(6).
           05 (SF)-COD-FISC-CNTR-IND PIC X.
           05 (SF)-COD-FISC-CNTR PIC X(16).
           05 (SF)-COD-FISC-CNTR-NULL REDEFINES
              (SF)-COD-FISC-CNTR   PIC X(16).
           05 (SF)-COD-FISC-ASSTO1-IND PIC X.
           05 (SF)-COD-FISC-ASSTO1 PIC X(16).
           05 (SF)-COD-FISC-ASSTO1-NULL REDEFINES
              (SF)-COD-FISC-ASSTO1   PIC X(16).
           05 (SF)-COD-FISC-ASSTO2-IND PIC X.
           05 (SF)-COD-FISC-ASSTO2 PIC X(16).
           05 (SF)-COD-FISC-ASSTO2-NULL REDEFINES
              (SF)-COD-FISC-ASSTO2   PIC X(16).
           05 (SF)-COD-FISC-ASSTO3-IND PIC X.
           05 (SF)-COD-FISC-ASSTO3 PIC X(16).
           05 (SF)-COD-FISC-ASSTO3-NULL REDEFINES
              (SF)-COD-FISC-ASSTO3   PIC X(16).
  GP       05 (SF)-CAUS-SCON-IND PIC X.
           05 (SF)-CAUS-SCON-VCHAR.
             49 (SF)-CAUS-SCON-LEN PIC S9(4) COMP-5.
             49 (SF)-CAUS-SCON PIC X(100).
  GP       05 (SF)-EMIT-TIT-OPZ-IND PIC X.
           05 (SF)-EMIT-TIT-OPZ-VCHAR.
             49 (SF)-EMIT-TIT-OPZ-LEN PIC S9(4) COMP-5.
             49 (SF)-EMIT-TIT-OPZ PIC X(100).
           05 (SF)-QTZ-SP-Z-COUP-EMIS-IND PIC X.
           05 (SF)-QTZ-SP-Z-COUP-EMIS PIC S9(7)V9(5) COMP-3.
           05 (SF)-QTZ-SP-Z-COUP-EMIS-NULL REDEFINES
              (SF)-QTZ-SP-Z-COUP-EMIS   PIC X(7).
           05 (SF)-QTZ-SP-Z-OPZ-EMIS-IND PIC X.
           05 (SF)-QTZ-SP-Z-OPZ-EMIS PIC S9(7)V9(5) COMP-3.
           05 (SF)-QTZ-SP-Z-OPZ-EMIS-NULL REDEFINES
              (SF)-QTZ-SP-Z-OPZ-EMIS   PIC X(7).
           05 (SF)-QTZ-SP-Z-COUP-DT-C-IND PIC X.
           05 (SF)-QTZ-SP-Z-COUP-DT-C PIC S9(7)V9(5) COMP-3.
           05 (SF)-QTZ-SP-Z-COUP-DT-C-NULL REDEFINES
              (SF)-QTZ-SP-Z-COUP-DT-C   PIC X(7).
           05 (SF)-QTZ-SP-Z-OPZ-DT-CA-IND PIC X.
           05 (SF)-QTZ-SP-Z-OPZ-DT-CA PIC S9(7)V9(5) COMP-3.
           05 (SF)-QTZ-SP-Z-OPZ-DT-CA-NULL REDEFINES
              (SF)-QTZ-SP-Z-OPZ-DT-CA   PIC X(7).
           05 (SF)-QTZ-TOT-EMIS-IND PIC X.
           05 (SF)-QTZ-TOT-EMIS PIC S9(7)V9(5) COMP-3.
           05 (SF)-QTZ-TOT-EMIS-NULL REDEFINES
              (SF)-QTZ-TOT-EMIS   PIC X(7).
           05 (SF)-QTZ-TOT-DT-CALC-IND PIC X.
           05 (SF)-QTZ-TOT-DT-CALC PIC S9(7)V9(5) COMP-3.
           05 (SF)-QTZ-TOT-DT-CALC-NULL REDEFINES
              (SF)-QTZ-TOT-DT-CALC   PIC X(7).
           05 (SF)-QTZ-TOT-DT-ULT-BIL-IND PIC X.
           05 (SF)-QTZ-TOT-DT-ULT-BIL PIC S9(7)V9(5) COMP-3.
           05 (SF)-QTZ-TOT-DT-ULT-BIL-NULL REDEFINES
              (SF)-QTZ-TOT-DT-ULT-BIL   PIC X(7).
           05 (SF)-DT-QTZ-EMIS-IND   PIC X.
           05 (SF)-DT-QTZ-EMIS   PIC X(10).
           05 (SF)-DT-QTZ-EMIS-NULL REDEFINES
              (SF)-DT-QTZ-EMIS   PIC X(10).
           05 (SF)-PC-CAR-GEST-IND PIC X.
           05 (SF)-PC-CAR-GEST PIC S9(3)V9(3) COMP-3.
           05 (SF)-PC-CAR-GEST-NULL REDEFINES
              (SF)-PC-CAR-GEST   PIC X(4).
           05 (SF)-PC-CAR-ACQ-IND PIC X.
           05 (SF)-PC-CAR-ACQ PIC S9(3)V9(3) COMP-3.
           05 (SF)-PC-CAR-ACQ-NULL REDEFINES
              (SF)-PC-CAR-ACQ   PIC X(4).
           05 (SF)-IMP-CAR-CASO-MOR-IND PIC X.
           05 (SF)-IMP-CAR-CASO-MOR PIC S9(12)V9(3) COMP-3.
           05 (SF)-IMP-CAR-CASO-MOR-NULL REDEFINES
              (SF)-IMP-CAR-CASO-MOR   PIC X(8).
           05 (SF)-PC-CAR-MOR-IND PIC X.
           05 (SF)-PC-CAR-MOR PIC S9(3)V9(3) COMP-3.
           05 (SF)-PC-CAR-MOR-NULL REDEFINES
              (SF)-PC-CAR-MOR   PIC X(4).
           05 (SF)-TP-VERS-IND PIC X.
           05 (SF)-TP-VERS PIC X(1).
           05 (SF)-TP-VERS-NULL REDEFINES
              (SF)-TP-VERS   PIC X(1).
           05 (SF)-FL-SWITCH-IND PIC X.
           05 (SF)-FL-SWITCH PIC X(1).
           05 (SF)-FL-SWITCH-NULL REDEFINES
              (SF)-FL-SWITCH   PIC X(1).
           05 (SF)-FL-IAS-IND PIC X.
           05 (SF)-FL-IAS PIC X(1).
           05 (SF)-FL-IAS-NULL REDEFINES
              (SF)-FL-IAS   PIC X(1).
           05 (SF)-DIR-IND PIC X.
           05 (SF)-DIR PIC S9(12)V9(3) COMP-3.
           05 (SF)-DIR-NULL REDEFINES
              (SF)-DIR   PIC X(8).
           05 (SF)-TP-COP-CASO-MOR-IND PIC X.
           05 (SF)-TP-COP-CASO-MOR PIC X(2).
           05 (SF)-TP-COP-CASO-MOR-NULL REDEFINES
              (SF)-TP-COP-CASO-MOR   PIC X(2).
           05 (SF)-MET-RISC-SPCL-IND PIC X.
           05 (SF)-MET-RISC-SPCL PIC S9(1)V     COMP-3.
           05 (SF)-MET-RISC-SPCL-NULL REDEFINES
              (SF)-MET-RISC-SPCL   PIC X(1).
           05 (SF)-TP-STAT-INVST-IND PIC X.
           05 (SF)-TP-STAT-INVST PIC X(2).
           05 (SF)-TP-STAT-INVST-NULL REDEFINES
              (SF)-TP-STAT-INVST   PIC X(2).
           05 (SF)-COD-PRDT-IND PIC X.
           05 (SF)-COD-PRDT PIC S9(5)V     COMP-3.
           05 (SF)-COD-PRDT-NULL REDEFINES
              (SF)-COD-PRDT   PIC X(3).
           05 (SF)-STAT-ASSTO-1-IND PIC X.
           05 (SF)-STAT-ASSTO-1 PIC X(1).
           05 (SF)-STAT-ASSTO-1-NULL REDEFINES
              (SF)-STAT-ASSTO-1   PIC X(1).
           05 (SF)-STAT-ASSTO-2-IND PIC X.
           05 (SF)-STAT-ASSTO-2 PIC X(1).
           05 (SF)-STAT-ASSTO-2-NULL REDEFINES
              (SF)-STAT-ASSTO-2   PIC X(1).
           05 (SF)-STAT-ASSTO-3-IND PIC X.
           05 (SF)-STAT-ASSTO-3 PIC X(1).
           05 (SF)-STAT-ASSTO-3-NULL REDEFINES
              (SF)-STAT-ASSTO-3   PIC X(1).
           05 (SF)-CPT-ASSTO-INI-MOR-IND PIC X.
           05 (SF)-CPT-ASSTO-INI-MOR PIC S9(12)V9(3) COMP-3.
           05 (SF)-CPT-ASSTO-INI-MOR-NULL REDEFINES
              (SF)-CPT-ASSTO-INI-MOR   PIC X(8).
           05 (SF)-TS-STAB-PRE-IND PIC X.
           05 (SF)-TS-STAB-PRE PIC S9(5)V9(9) COMP-3.
           05 (SF)-TS-STAB-PRE-NULL REDEFINES
              (SF)-TS-STAB-PRE   PIC X(8).
           05 (SF)-DIR-EMIS-IND PIC X.
           05 (SF)-DIR-EMIS PIC S9(12)V9(3) COMP-3.
           05 (SF)-DIR-EMIS-NULL REDEFINES
              (SF)-DIR-EMIS   PIC X(8).
           05 (SF)-DT-INC-ULT-PRE-IND  PIC X.
           05 (SF)-DT-INC-ULT-PRE   PIC X(10).
           05 (SF)-DT-INC-ULT-PRE-NULL REDEFINES
              (SF)-DT-INC-ULT-PRE   PIC X(10).
           05 (SF)-STAT-TBGC-ASSTO-1-IND PIC X.
           05 (SF)-STAT-TBGC-ASSTO-1 PIC X(1).
           05 (SF)-STAT-TBGC-ASSTO-1-NULL REDEFINES
              (SF)-STAT-TBGC-ASSTO-1   PIC X(1).
           05 (SF)-STAT-TBGC-ASSTO-2-IND PIC X.
           05 (SF)-STAT-TBGC-ASSTO-2 PIC X(1).
           05 (SF)-STAT-TBGC-ASSTO-2-NULL REDEFINES
              (SF)-STAT-TBGC-ASSTO-2   PIC X(1).
           05 (SF)-STAT-TBGC-ASSTO-3-IND PIC X.
           05 (SF)-STAT-TBGC-ASSTO-3 PIC X(1).
           05 (SF)-STAT-TBGC-ASSTO-3-NULL REDEFINES
              (SF)-STAT-TBGC-ASSTO-3   PIC X(1).
           05 (SF)-FRAZ-DECR-CPT-IND PIC X.
           05 (SF)-FRAZ-DECR-CPT PIC S9(5)V     COMP-3.
           05 (SF)-FRAZ-DECR-CPT-NULL REDEFINES
              (SF)-FRAZ-DECR-CPT   PIC X(3).
           05 (SF)-PRE-PP-ULT-IND PIC X.
           05 (SF)-PRE-PP-ULT PIC S9(12)V9(3) COMP-3.
           05 (SF)-PRE-PP-ULT-NULL REDEFINES
              (SF)-PRE-PP-ULT   PIC X(8).
           05 (SF)-ACQ-EXP-IND PIC X.
           05 (SF)-ACQ-EXP PIC S9(12)V9(3) COMP-3.
           05 (SF)-ACQ-EXP-NULL REDEFINES
              (SF)-ACQ-EXP   PIC X(8).
           05 (SF)-REMUN-ASS-IND PIC X.
           05 (SF)-REMUN-ASS PIC S9(12)V9(3) COMP-3.
           05 (SF)-REMUN-ASS-NULL REDEFINES
              (SF)-REMUN-ASS   PIC X(8).
           05 (SF)-COMMIS-INTER-IND PIC X.
           05 (SF)-COMMIS-INTER PIC S9(12)V9(3) COMP-3.
           05 (SF)-COMMIS-INTER-NULL REDEFINES
              (SF)-COMMIS-INTER   PIC X(8).
           05 (SF)-NUM-FINANZ-IND PIC X.
           05 (SF)-NUM-FINANZ PIC X(40).
           05 (SF)-NUM-FINANZ-NULL REDEFINES
              (SF)-NUM-FINANZ   PIC X(40).
           05 (SF)-TP-ACC-COMM-IND PIC X.
           05 (SF)-TP-ACC-COMM PIC X(2).
           05 (SF)-TP-ACC-COMM-NULL REDEFINES
              (SF)-TP-ACC-COMM   PIC X(2).
           05 (SF)-IB-ACC-COMM-IND PIC X.
           05 (SF)-IB-ACC-COMM PIC X(40).
           05 (SF)-IB-ACC-COMM-NULL REDEFINES
              (SF)-IB-ACC-COMM   PIC X(40).
           05 (SF)-RAMO-BILA PIC X(12).
           05 (SF)-CARZ-IND  PIC X.
           05 (SF)-CARZ PIC S9(5)     COMP-3.
           05 (SF)-CARZ-NULL REDEFINES
              (SF)-CARZ   PIC X(3).

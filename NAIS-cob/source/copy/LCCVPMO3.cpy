
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVPMO3
      *   ULTIMO AGG. 17 FEB 2015
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-PMO.
           MOVE PMO-ID-PARAM-MOVI
             TO (SF)-ID-PTF(IX-TAB-PMO)
           MOVE PMO-ID-PARAM-MOVI
             TO (SF)-ID-PARAM-MOVI(IX-TAB-PMO)
           MOVE PMO-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-PMO)
           MOVE PMO-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-PMO)
           MOVE PMO-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-PMO)
           IF PMO-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE PMO-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-PMO)
           END-IF
           MOVE PMO-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-PMO)
           MOVE PMO-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-PMO)
           MOVE PMO-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-PMO)
           IF PMO-TP-MOVI-NULL = HIGH-VALUES
              MOVE PMO-TP-MOVI-NULL
                TO (SF)-TP-MOVI-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-TP-MOVI
                TO (SF)-TP-MOVI(IX-TAB-PMO)
           END-IF
           IF PMO-FRQ-MOVI-NULL = HIGH-VALUES
              MOVE PMO-FRQ-MOVI-NULL
                TO (SF)-FRQ-MOVI-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-FRQ-MOVI
                TO (SF)-FRQ-MOVI(IX-TAB-PMO)
           END-IF
           IF PMO-DUR-AA-NULL = HIGH-VALUES
              MOVE PMO-DUR-AA-NULL
                TO (SF)-DUR-AA-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-DUR-AA
                TO (SF)-DUR-AA(IX-TAB-PMO)
           END-IF
           IF PMO-DUR-MM-NULL = HIGH-VALUES
              MOVE PMO-DUR-MM-NULL
                TO (SF)-DUR-MM-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-DUR-MM
                TO (SF)-DUR-MM(IX-TAB-PMO)
           END-IF
           IF PMO-DUR-GG-NULL = HIGH-VALUES
              MOVE PMO-DUR-GG-NULL
                TO (SF)-DUR-GG-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-DUR-GG
                TO (SF)-DUR-GG(IX-TAB-PMO)
           END-IF
           IF PMO-DT-RICOR-PREC-NULL = HIGH-VALUES
              MOVE PMO-DT-RICOR-PREC-NULL
                TO (SF)-DT-RICOR-PREC-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-DT-RICOR-PREC
                TO (SF)-DT-RICOR-PREC(IX-TAB-PMO)
           END-IF
           IF PMO-DT-RICOR-SUCC-NULL = HIGH-VALUES
              MOVE PMO-DT-RICOR-SUCC-NULL
                TO (SF)-DT-RICOR-SUCC-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-DT-RICOR-SUCC
                TO (SF)-DT-RICOR-SUCC(IX-TAB-PMO)
           END-IF
           IF PMO-PC-INTR-FRAZ-NULL = HIGH-VALUES
              MOVE PMO-PC-INTR-FRAZ-NULL
                TO (SF)-PC-INTR-FRAZ-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-PC-INTR-FRAZ
                TO (SF)-PC-INTR-FRAZ(IX-TAB-PMO)
           END-IF
           IF PMO-IMP-BNS-DA-SCO-TOT-NULL = HIGH-VALUES
              MOVE PMO-IMP-BNS-DA-SCO-TOT-NULL
                TO (SF)-IMP-BNS-DA-SCO-TOT-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-IMP-BNS-DA-SCO-TOT
                TO (SF)-IMP-BNS-DA-SCO-TOT(IX-TAB-PMO)
           END-IF
           IF PMO-IMP-BNS-DA-SCO-NULL = HIGH-VALUES
              MOVE PMO-IMP-BNS-DA-SCO-NULL
                TO (SF)-IMP-BNS-DA-SCO-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-IMP-BNS-DA-SCO
                TO (SF)-IMP-BNS-DA-SCO(IX-TAB-PMO)
           END-IF
           IF PMO-PC-ANTIC-BNS-NULL = HIGH-VALUES
              MOVE PMO-PC-ANTIC-BNS-NULL
                TO (SF)-PC-ANTIC-BNS-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-PC-ANTIC-BNS
                TO (SF)-PC-ANTIC-BNS(IX-TAB-PMO)
           END-IF
           IF PMO-TP-RINN-COLL-NULL = HIGH-VALUES
              MOVE PMO-TP-RINN-COLL-NULL
                TO (SF)-TP-RINN-COLL-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-TP-RINN-COLL
                TO (SF)-TP-RINN-COLL(IX-TAB-PMO)
           END-IF
           IF PMO-TP-RIVAL-PRE-NULL = HIGH-VALUES
              MOVE PMO-TP-RIVAL-PRE-NULL
                TO (SF)-TP-RIVAL-PRE-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-TP-RIVAL-PRE
                TO (SF)-TP-RIVAL-PRE(IX-TAB-PMO)
           END-IF
           IF PMO-TP-RIVAL-PRSTZ-NULL = HIGH-VALUES
              MOVE PMO-TP-RIVAL-PRSTZ-NULL
                TO (SF)-TP-RIVAL-PRSTZ-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-TP-RIVAL-PRSTZ
                TO (SF)-TP-RIVAL-PRSTZ(IX-TAB-PMO)
           END-IF
           IF PMO-FL-EVID-RIVAL-NULL = HIGH-VALUES
              MOVE PMO-FL-EVID-RIVAL-NULL
                TO (SF)-FL-EVID-RIVAL-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-FL-EVID-RIVAL
                TO (SF)-FL-EVID-RIVAL(IX-TAB-PMO)
           END-IF
           IF PMO-ULT-PC-PERD-NULL = HIGH-VALUES
              MOVE PMO-ULT-PC-PERD-NULL
                TO (SF)-ULT-PC-PERD-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-ULT-PC-PERD
                TO (SF)-ULT-PC-PERD(IX-TAB-PMO)
           END-IF
           IF PMO-TOT-AA-GIA-PROR-NULL = HIGH-VALUES
              MOVE PMO-TOT-AA-GIA-PROR-NULL
                TO (SF)-TOT-AA-GIA-PROR-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-TOT-AA-GIA-PROR
                TO (SF)-TOT-AA-GIA-PROR(IX-TAB-PMO)
           END-IF
           IF PMO-TP-OPZ-NULL = HIGH-VALUES
              MOVE PMO-TP-OPZ-NULL
                TO (SF)-TP-OPZ-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-TP-OPZ
                TO (SF)-TP-OPZ(IX-TAB-PMO)
           END-IF
           IF PMO-AA-REN-CER-NULL = HIGH-VALUES
              MOVE PMO-AA-REN-CER-NULL
                TO (SF)-AA-REN-CER-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-AA-REN-CER
                TO (SF)-AA-REN-CER(IX-TAB-PMO)
           END-IF
           IF PMO-PC-REVRSB-NULL = HIGH-VALUES
              MOVE PMO-PC-REVRSB-NULL
                TO (SF)-PC-REVRSB-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-PC-REVRSB
                TO (SF)-PC-REVRSB(IX-TAB-PMO)
           END-IF
           IF PMO-IMP-RISC-PARZ-PRGT-NULL = HIGH-VALUES
              MOVE PMO-IMP-RISC-PARZ-PRGT-NULL
                TO (SF)-IMP-RISC-PARZ-PRGT-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-IMP-RISC-PARZ-PRGT
                TO (SF)-IMP-RISC-PARZ-PRGT(IX-TAB-PMO)
           END-IF
           IF PMO-IMP-LRD-DI-RAT-NULL = HIGH-VALUES
              MOVE PMO-IMP-LRD-DI-RAT-NULL
                TO (SF)-IMP-LRD-DI-RAT-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-IMP-LRD-DI-RAT
                TO (SF)-IMP-LRD-DI-RAT(IX-TAB-PMO)
           END-IF
           IF PMO-IB-OGG-NULL = HIGH-VALUES
              MOVE PMO-IB-OGG-NULL
                TO (SF)-IB-OGG-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-IB-OGG
                TO (SF)-IB-OGG(IX-TAB-PMO)
           END-IF
           IF PMO-COS-ONER-NULL = HIGH-VALUES
              MOVE PMO-COS-ONER-NULL
                TO (SF)-COS-ONER-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-COS-ONER
                TO (SF)-COS-ONER(IX-TAB-PMO)
           END-IF
           IF PMO-SPE-PC-NULL = HIGH-VALUES
              MOVE PMO-SPE-PC-NULL
                TO (SF)-SPE-PC-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-SPE-PC
                TO (SF)-SPE-PC(IX-TAB-PMO)
           END-IF
           IF PMO-FL-ATTIV-GAR-NULL = HIGH-VALUES
              MOVE PMO-FL-ATTIV-GAR-NULL
                TO (SF)-FL-ATTIV-GAR-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-FL-ATTIV-GAR
                TO (SF)-FL-ATTIV-GAR(IX-TAB-PMO)
           END-IF
           IF PMO-CAMBIO-VER-PROD-NULL = HIGH-VALUES
              MOVE PMO-CAMBIO-VER-PROD-NULL
                TO (SF)-CAMBIO-VER-PROD-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-CAMBIO-VER-PROD
                TO (SF)-CAMBIO-VER-PROD(IX-TAB-PMO)
           END-IF
           IF PMO-MM-DIFF-NULL = HIGH-VALUES
              MOVE PMO-MM-DIFF-NULL
                TO (SF)-MM-DIFF-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-MM-DIFF
                TO (SF)-MM-DIFF(IX-TAB-PMO)
           END-IF
           IF PMO-IMP-RAT-MANFEE-NULL = HIGH-VALUES
              MOVE PMO-IMP-RAT-MANFEE-NULL
                TO (SF)-IMP-RAT-MANFEE-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-IMP-RAT-MANFEE
                TO (SF)-IMP-RAT-MANFEE(IX-TAB-PMO)
           END-IF
           IF PMO-DT-ULT-EROG-MANFEE-NULL = HIGH-VALUES
              MOVE PMO-DT-ULT-EROG-MANFEE-NULL
                TO (SF)-DT-ULT-EROG-MANFEE-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-DT-ULT-EROG-MANFEE
                TO (SF)-DT-ULT-EROG-MANFEE(IX-TAB-PMO)
           END-IF
           IF PMO-TP-OGG-RIVAL-NULL = HIGH-VALUES
              MOVE PMO-TP-OGG-RIVAL-NULL
                TO (SF)-TP-OGG-RIVAL-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-TP-OGG-RIVAL
                TO (SF)-TP-OGG-RIVAL(IX-TAB-PMO)
           END-IF
           IF PMO-SOM-ASSTA-GARAC-NULL = HIGH-VALUES
              MOVE PMO-SOM-ASSTA-GARAC-NULL
                TO (SF)-SOM-ASSTA-GARAC-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-SOM-ASSTA-GARAC
                TO (SF)-SOM-ASSTA-GARAC(IX-TAB-PMO)
           END-IF
           IF PMO-PC-APPLZ-OPZ-NULL = HIGH-VALUES
              MOVE PMO-PC-APPLZ-OPZ-NULL
                TO (SF)-PC-APPLZ-OPZ-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-PC-APPLZ-OPZ
                TO (SF)-PC-APPLZ-OPZ(IX-TAB-PMO)
           END-IF
           IF PMO-ID-ADES-NULL = HIGH-VALUES
              MOVE PMO-ID-ADES-NULL
                TO (SF)-ID-ADES-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-ID-ADES
                TO (SF)-ID-ADES(IX-TAB-PMO)
           END-IF
           MOVE PMO-ID-POLI
             TO (SF)-ID-POLI(IX-TAB-PMO)
           MOVE PMO-TP-FRM-ASSVA
             TO (SF)-TP-FRM-ASSVA(IX-TAB-PMO)
           MOVE PMO-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-PMO)
           MOVE PMO-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-PMO)
           MOVE PMO-DS-VER
             TO (SF)-DS-VER(IX-TAB-PMO)
           MOVE PMO-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-PMO)
           MOVE PMO-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-PMO)
           MOVE PMO-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-PMO)
           MOVE PMO-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-PMO)
           IF PMO-TP-ESTR-CNT-NULL = HIGH-VALUES
              MOVE PMO-TP-ESTR-CNT-NULL
                TO (SF)-TP-ESTR-CNT-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-TP-ESTR-CNT
                TO (SF)-TP-ESTR-CNT(IX-TAB-PMO)
           END-IF
           IF PMO-COD-RAMO-NULL = HIGH-VALUES
              MOVE PMO-COD-RAMO-NULL
                TO (SF)-COD-RAMO-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-COD-RAMO
                TO (SF)-COD-RAMO(IX-TAB-PMO)
           END-IF
           IF PMO-GEN-DA-SIN-NULL = HIGH-VALUES
              MOVE PMO-GEN-DA-SIN-NULL
                TO (SF)-GEN-DA-SIN-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-GEN-DA-SIN
                TO (SF)-GEN-DA-SIN(IX-TAB-PMO)
           END-IF
           IF PMO-COD-TARI-NULL = HIGH-VALUES
              MOVE PMO-COD-TARI-NULL
                TO (SF)-COD-TARI-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-COD-TARI
                TO (SF)-COD-TARI(IX-TAB-PMO)
           END-IF
           IF PMO-NUM-RAT-PAG-PRE-NULL = HIGH-VALUES
              MOVE PMO-NUM-RAT-PAG-PRE-NULL
                TO (SF)-NUM-RAT-PAG-PRE-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-NUM-RAT-PAG-PRE
                TO (SF)-NUM-RAT-PAG-PRE(IX-TAB-PMO)
           END-IF
           IF PMO-PC-SERV-VAL-NULL = HIGH-VALUES
              MOVE PMO-PC-SERV-VAL-NULL
                TO (SF)-PC-SERV-VAL-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-PC-SERV-VAL
                TO (SF)-PC-SERV-VAL(IX-TAB-PMO)
           END-IF
           IF PMO-ETA-AA-SOGL-BNFICR-NULL = HIGH-VALUES
              MOVE PMO-ETA-AA-SOGL-BNFICR-NULL
                TO (SF)-ETA-AA-SOGL-BNFICR-NULL(IX-TAB-PMO)
           ELSE
              MOVE PMO-ETA-AA-SOGL-BNFICR
                TO (SF)-ETA-AA-SOGL-BNFICR(IX-TAB-PMO)
           END-IF.
       VALORIZZA-OUTPUT-PMO-EX.
           EXIT.

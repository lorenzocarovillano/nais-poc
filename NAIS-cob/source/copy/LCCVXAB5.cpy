
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVXAB5
      *   ULTIMO AGG. 09 LUG 2009
      *------------------------------------------------------------

       VAL-DCLGEN-XAB.
           MOVE (SF)-COD-COMP-ANIA
              TO XAB-COD-COMP-ANIA
           MOVE (SF)-COD-BLOCCO
              TO XAB-COD-BLOCCO
           MOVE (SF)-DESC
              TO XAB-DESC
           MOVE (SF)-DS-OPER-SQL
              TO XAB-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO XAB-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO XAB-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO XAB-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO XAB-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO XAB-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO XAB-DS-STATO-ELAB
           IF (SF)-FL-REC-AUT-NULL = HIGH-VALUES
              MOVE (SF)-FL-REC-AUT-NULL
              TO XAB-FL-REC-AUT-NULL
           ELSE
              MOVE (SF)-FL-REC-AUT
              TO XAB-FL-REC-AUT
           END-IF.
       VAL-DCLGEN-XAB-EX.
           EXIT.

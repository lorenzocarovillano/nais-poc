
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVLQU3
      *   ULTIMO AGG. 03 GIU 2019
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-LQU.
           MOVE LQU-ID-LIQ
             TO (SF)-ID-PTF(IX-TAB-LQU)
           MOVE LQU-ID-LIQ
             TO (SF)-ID-LIQ(IX-TAB-LQU)
           MOVE LQU-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-LQU)
           MOVE LQU-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-LQU)
           MOVE LQU-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-LQU)
           IF LQU-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE LQU-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-LQU)
           END-IF
           MOVE LQU-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-LQU)
           MOVE LQU-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-LQU)
           MOVE LQU-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-LQU)
           IF LQU-IB-OGG-NULL = HIGH-VALUES
              MOVE LQU-IB-OGG-NULL
                TO (SF)-IB-OGG-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IB-OGG
                TO (SF)-IB-OGG(IX-TAB-LQU)
           END-IF
           MOVE LQU-TP-LIQ
             TO (SF)-TP-LIQ(IX-TAB-LQU)
           MOVE LQU-DESC-CAU-EVE-SIN
             TO (SF)-DESC-CAU-EVE-SIN(IX-TAB-LQU)
           IF LQU-COD-CAU-SIN-NULL = HIGH-VALUES
              MOVE LQU-COD-CAU-SIN-NULL
                TO (SF)-COD-CAU-SIN-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-COD-CAU-SIN
                TO (SF)-COD-CAU-SIN(IX-TAB-LQU)
           END-IF
           IF LQU-COD-SIN-CATSTRF-NULL = HIGH-VALUES
              MOVE LQU-COD-SIN-CATSTRF-NULL
                TO (SF)-COD-SIN-CATSTRF-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-COD-SIN-CATSTRF
                TO (SF)-COD-SIN-CATSTRF(IX-TAB-LQU)
           END-IF
           IF LQU-DT-MOR-NULL = HIGH-VALUES
              MOVE LQU-DT-MOR-NULL
                TO (SF)-DT-MOR-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-DT-MOR
                TO (SF)-DT-MOR(IX-TAB-LQU)
           END-IF
           IF LQU-DT-DEN-NULL = HIGH-VALUES
              MOVE LQU-DT-DEN-NULL
                TO (SF)-DT-DEN-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-DT-DEN
                TO (SF)-DT-DEN(IX-TAB-LQU)
           END-IF
           IF LQU-DT-PERV-DEN-NULL = HIGH-VALUES
              MOVE LQU-DT-PERV-DEN-NULL
                TO (SF)-DT-PERV-DEN-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-DT-PERV-DEN
                TO (SF)-DT-PERV-DEN(IX-TAB-LQU)
           END-IF
           IF LQU-DT-RICH-NULL = HIGH-VALUES
              MOVE LQU-DT-RICH-NULL
                TO (SF)-DT-RICH-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-DT-RICH
                TO (SF)-DT-RICH(IX-TAB-LQU)
           END-IF
           IF LQU-TP-SIN-NULL = HIGH-VALUES
              MOVE LQU-TP-SIN-NULL
                TO (SF)-TP-SIN-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TP-SIN
                TO (SF)-TP-SIN(IX-TAB-LQU)
           END-IF
           IF LQU-TP-RISC-NULL = HIGH-VALUES
              MOVE LQU-TP-RISC-NULL
                TO (SF)-TP-RISC-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TP-RISC
                TO (SF)-TP-RISC(IX-TAB-LQU)
           END-IF
           IF LQU-TP-MET-RISC-NULL = HIGH-VALUES
              MOVE LQU-TP-MET-RISC-NULL
                TO (SF)-TP-MET-RISC-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TP-MET-RISC
                TO (SF)-TP-MET-RISC(IX-TAB-LQU)
           END-IF
           IF LQU-DT-LIQ-NULL = HIGH-VALUES
              MOVE LQU-DT-LIQ-NULL
                TO (SF)-DT-LIQ-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-DT-LIQ
                TO (SF)-DT-LIQ(IX-TAB-LQU)
           END-IF
           IF LQU-COD-DVS-NULL = HIGH-VALUES
              MOVE LQU-COD-DVS-NULL
                TO (SF)-COD-DVS-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-COD-DVS
                TO (SF)-COD-DVS(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IMP-LRD-LIQTO-NULL = HIGH-VALUES
              MOVE LQU-TOT-IMP-LRD-LIQTO-NULL
                TO (SF)-TOT-IMP-LRD-LIQTO-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IMP-LRD-LIQTO
                TO (SF)-TOT-IMP-LRD-LIQTO(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IMP-PREST-NULL = HIGH-VALUES
              MOVE LQU-TOT-IMP-PREST-NULL
                TO (SF)-TOT-IMP-PREST-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IMP-PREST
                TO (SF)-TOT-IMP-PREST(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IMP-INTR-PREST-NULL = HIGH-VALUES
              MOVE LQU-TOT-IMP-INTR-PREST-NULL
                TO (SF)-TOT-IMP-INTR-PREST-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IMP-INTR-PREST
                TO (SF)-TOT-IMP-INTR-PREST(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IMP-UTI-NULL = HIGH-VALUES
              MOVE LQU-TOT-IMP-UTI-NULL
                TO (SF)-TOT-IMP-UTI-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IMP-UTI
                TO (SF)-TOT-IMP-UTI(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IMP-RIT-TFR-NULL = HIGH-VALUES
              MOVE LQU-TOT-IMP-RIT-TFR-NULL
                TO (SF)-TOT-IMP-RIT-TFR-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IMP-RIT-TFR
                TO (SF)-TOT-IMP-RIT-TFR(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IMP-RIT-ACC-NULL = HIGH-VALUES
              MOVE LQU-TOT-IMP-RIT-ACC-NULL
                TO (SF)-TOT-IMP-RIT-ACC-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IMP-RIT-ACC
                TO (SF)-TOT-IMP-RIT-ACC(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IMP-RIT-VIS-NULL = HIGH-VALUES
              MOVE LQU-TOT-IMP-RIT-VIS-NULL
                TO (SF)-TOT-IMP-RIT-VIS-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IMP-RIT-VIS
                TO (SF)-TOT-IMP-RIT-VIS(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IMPB-TFR-NULL = HIGH-VALUES
              MOVE LQU-TOT-IMPB-TFR-NULL
                TO (SF)-TOT-IMPB-TFR-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IMPB-TFR
                TO (SF)-TOT-IMPB-TFR(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IMPB-ACC-NULL = HIGH-VALUES
              MOVE LQU-TOT-IMPB-ACC-NULL
                TO (SF)-TOT-IMPB-ACC-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IMPB-ACC
                TO (SF)-TOT-IMPB-ACC(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IMPB-VIS-NULL = HIGH-VALUES
              MOVE LQU-TOT-IMPB-VIS-NULL
                TO (SF)-TOT-IMPB-VIS-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IMPB-VIS
                TO (SF)-TOT-IMPB-VIS(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IMP-RIMB-NULL = HIGH-VALUES
              MOVE LQU-TOT-IMP-RIMB-NULL
                TO (SF)-TOT-IMP-RIMB-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IMP-RIMB
                TO (SF)-TOT-IMP-RIMB(IX-TAB-LQU)
           END-IF
           IF LQU-IMPB-IMPST-PRVR-NULL = HIGH-VALUES
              MOVE LQU-IMPB-IMPST-PRVR-NULL
                TO (SF)-IMPB-IMPST-PRVR-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPB-IMPST-PRVR
                TO (SF)-IMPB-IMPST-PRVR(IX-TAB-LQU)
           END-IF
           IF LQU-IMPST-PRVR-NULL = HIGH-VALUES
              MOVE LQU-IMPST-PRVR-NULL
                TO (SF)-IMPST-PRVR-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPST-PRVR
                TO (SF)-IMPST-PRVR(IX-TAB-LQU)
           END-IF
           IF LQU-IMPB-IMPST-252-NULL = HIGH-VALUES
              MOVE LQU-IMPB-IMPST-252-NULL
                TO (SF)-IMPB-IMPST-252-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPB-IMPST-252
                TO (SF)-IMPB-IMPST-252(IX-TAB-LQU)
           END-IF
           IF LQU-IMPST-252-NULL = HIGH-VALUES
              MOVE LQU-IMPST-252-NULL
                TO (SF)-IMPST-252-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPST-252
                TO (SF)-IMPST-252(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IMP-IS-NULL = HIGH-VALUES
              MOVE LQU-TOT-IMP-IS-NULL
                TO (SF)-TOT-IMP-IS-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IMP-IS
                TO (SF)-TOT-IMP-IS(IX-TAB-LQU)
           END-IF
           IF LQU-IMP-DIR-LIQ-NULL = HIGH-VALUES
              MOVE LQU-IMP-DIR-LIQ-NULL
                TO (SF)-IMP-DIR-LIQ-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMP-DIR-LIQ
                TO (SF)-IMP-DIR-LIQ(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IMP-NET-LIQTO-NULL = HIGH-VALUES
              MOVE LQU-TOT-IMP-NET-LIQTO-NULL
                TO (SF)-TOT-IMP-NET-LIQTO-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IMP-NET-LIQTO
                TO (SF)-TOT-IMP-NET-LIQTO(IX-TAB-LQU)
           END-IF
           IF LQU-MONT-END2000-NULL = HIGH-VALUES
              MOVE LQU-MONT-END2000-NULL
                TO (SF)-MONT-END2000-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-MONT-END2000
                TO (SF)-MONT-END2000(IX-TAB-LQU)
           END-IF
           IF LQU-MONT-END2006-NULL = HIGH-VALUES
              MOVE LQU-MONT-END2006-NULL
                TO (SF)-MONT-END2006-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-MONT-END2006
                TO (SF)-MONT-END2006(IX-TAB-LQU)
           END-IF
           IF LQU-PC-REN-NULL = HIGH-VALUES
              MOVE LQU-PC-REN-NULL
                TO (SF)-PC-REN-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-PC-REN
                TO (SF)-PC-REN(IX-TAB-LQU)
           END-IF
           IF LQU-IMP-PNL-NULL = HIGH-VALUES
              MOVE LQU-IMP-PNL-NULL
                TO (SF)-IMP-PNL-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMP-PNL
                TO (SF)-IMP-PNL(IX-TAB-LQU)
           END-IF
           IF LQU-IMPB-IRPEF-NULL = HIGH-VALUES
              MOVE LQU-IMPB-IRPEF-NULL
                TO (SF)-IMPB-IRPEF-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPB-IRPEF
                TO (SF)-IMPB-IRPEF(IX-TAB-LQU)
           END-IF
           IF LQU-IMPST-IRPEF-NULL = HIGH-VALUES
              MOVE LQU-IMPST-IRPEF-NULL
                TO (SF)-IMPST-IRPEF-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPST-IRPEF
                TO (SF)-IMPST-IRPEF(IX-TAB-LQU)
           END-IF
           IF LQU-DT-VLT-NULL = HIGH-VALUES
              MOVE LQU-DT-VLT-NULL
                TO (SF)-DT-VLT-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-DT-VLT
                TO (SF)-DT-VLT(IX-TAB-LQU)
           END-IF
           IF LQU-DT-END-ISTR-NULL = HIGH-VALUES
              MOVE LQU-DT-END-ISTR-NULL
                TO (SF)-DT-END-ISTR-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-DT-END-ISTR
                TO (SF)-DT-END-ISTR(IX-TAB-LQU)
           END-IF
           MOVE LQU-TP-RIMB
             TO (SF)-TP-RIMB(IX-TAB-LQU)
           IF LQU-SPE-RCS-NULL = HIGH-VALUES
              MOVE LQU-SPE-RCS-NULL
                TO (SF)-SPE-RCS-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-SPE-RCS
                TO (SF)-SPE-RCS(IX-TAB-LQU)
           END-IF
           IF LQU-IB-LIQ-NULL = HIGH-VALUES
              MOVE LQU-IB-LIQ-NULL
                TO (SF)-IB-LIQ-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IB-LIQ
                TO (SF)-IB-LIQ(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IAS-ONER-PRVNT-NULL = HIGH-VALUES
              MOVE LQU-TOT-IAS-ONER-PRVNT-NULL
                TO (SF)-TOT-IAS-ONER-PRVNT-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IAS-ONER-PRVNT
                TO (SF)-TOT-IAS-ONER-PRVNT(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IAS-MGG-SIN-NULL = HIGH-VALUES
              MOVE LQU-TOT-IAS-MGG-SIN-NULL
                TO (SF)-TOT-IAS-MGG-SIN-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IAS-MGG-SIN
                TO (SF)-TOT-IAS-MGG-SIN(IX-TAB-LQU)
           END-IF
           IF LQU-TOT-IAS-RST-DPST-NULL = HIGH-VALUES
              MOVE LQU-TOT-IAS-RST-DPST-NULL
                TO (SF)-TOT-IAS-RST-DPST-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IAS-RST-DPST
                TO (SF)-TOT-IAS-RST-DPST(IX-TAB-LQU)
           END-IF
           IF LQU-IMP-ONER-LIQ-NULL = HIGH-VALUES
              MOVE LQU-IMP-ONER-LIQ-NULL
                TO (SF)-IMP-ONER-LIQ-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMP-ONER-LIQ
                TO (SF)-IMP-ONER-LIQ(IX-TAB-LQU)
           END-IF
           IF LQU-COMPON-TAX-RIMB-NULL = HIGH-VALUES
              MOVE LQU-COMPON-TAX-RIMB-NULL
                TO (SF)-COMPON-TAX-RIMB-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-COMPON-TAX-RIMB
                TO (SF)-COMPON-TAX-RIMB(IX-TAB-LQU)
           END-IF
           IF LQU-TP-MEZ-PAG-NULL = HIGH-VALUES
              MOVE LQU-TP-MEZ-PAG-NULL
                TO (SF)-TP-MEZ-PAG-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TP-MEZ-PAG
                TO (SF)-TP-MEZ-PAG(IX-TAB-LQU)
           END-IF
           IF LQU-IMP-EXCONTR-NULL = HIGH-VALUES
              MOVE LQU-IMP-EXCONTR-NULL
                TO (SF)-IMP-EXCONTR-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMP-EXCONTR
                TO (SF)-IMP-EXCONTR(IX-TAB-LQU)
           END-IF
           IF LQU-IMP-INTR-RIT-PAG-NULL = HIGH-VALUES
              MOVE LQU-IMP-INTR-RIT-PAG-NULL
                TO (SF)-IMP-INTR-RIT-PAG-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMP-INTR-RIT-PAG
                TO (SF)-IMP-INTR-RIT-PAG(IX-TAB-LQU)
           END-IF
           IF LQU-BNS-NON-GODUTO-NULL = HIGH-VALUES
              MOVE LQU-BNS-NON-GODUTO-NULL
                TO (SF)-BNS-NON-GODUTO-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-BNS-NON-GODUTO
                TO (SF)-BNS-NON-GODUTO(IX-TAB-LQU)
           END-IF
           IF LQU-CNBT-INPSTFM-NULL = HIGH-VALUES
              MOVE LQU-CNBT-INPSTFM-NULL
                TO (SF)-CNBT-INPSTFM-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-CNBT-INPSTFM
                TO (SF)-CNBT-INPSTFM(IX-TAB-LQU)
           END-IF
           IF LQU-IMPST-DA-RIMB-NULL = HIGH-VALUES
              MOVE LQU-IMPST-DA-RIMB-NULL
                TO (SF)-IMPST-DA-RIMB-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPST-DA-RIMB
                TO (SF)-IMPST-DA-RIMB(IX-TAB-LQU)
           END-IF
           IF LQU-IMPB-IS-NULL = HIGH-VALUES
              MOVE LQU-IMPB-IS-NULL
                TO (SF)-IMPB-IS-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPB-IS
                TO (SF)-IMPB-IS(IX-TAB-LQU)
           END-IF
           IF LQU-TAX-SEP-NULL = HIGH-VALUES
              MOVE LQU-TAX-SEP-NULL
                TO (SF)-TAX-SEP-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TAX-SEP
                TO (SF)-TAX-SEP(IX-TAB-LQU)
           END-IF
           IF LQU-IMPB-TAX-SEP-NULL = HIGH-VALUES
              MOVE LQU-IMPB-TAX-SEP-NULL
                TO (SF)-IMPB-TAX-SEP-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPB-TAX-SEP
                TO (SF)-IMPB-TAX-SEP(IX-TAB-LQU)
           END-IF
           IF LQU-IMPB-INTR-SU-PREST-NULL = HIGH-VALUES
              MOVE LQU-IMPB-INTR-SU-PREST-NULL
                TO (SF)-IMPB-INTR-SU-PREST-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPB-INTR-SU-PREST
                TO (SF)-IMPB-INTR-SU-PREST(IX-TAB-LQU)
           END-IF
           IF LQU-ADDIZ-COMUN-NULL = HIGH-VALUES
              MOVE LQU-ADDIZ-COMUN-NULL
                TO (SF)-ADDIZ-COMUN-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-ADDIZ-COMUN
                TO (SF)-ADDIZ-COMUN(IX-TAB-LQU)
           END-IF
           IF LQU-IMPB-ADDIZ-COMUN-NULL = HIGH-VALUES
              MOVE LQU-IMPB-ADDIZ-COMUN-NULL
                TO (SF)-IMPB-ADDIZ-COMUN-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPB-ADDIZ-COMUN
                TO (SF)-IMPB-ADDIZ-COMUN(IX-TAB-LQU)
           END-IF
           IF LQU-ADDIZ-REGION-NULL = HIGH-VALUES
              MOVE LQU-ADDIZ-REGION-NULL
                TO (SF)-ADDIZ-REGION-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-ADDIZ-REGION
                TO (SF)-ADDIZ-REGION(IX-TAB-LQU)
           END-IF
           IF LQU-IMPB-ADDIZ-REGION-NULL = HIGH-VALUES
              MOVE LQU-IMPB-ADDIZ-REGION-NULL
                TO (SF)-IMPB-ADDIZ-REGION-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPB-ADDIZ-REGION
                TO (SF)-IMPB-ADDIZ-REGION(IX-TAB-LQU)
           END-IF
           IF LQU-MONT-DAL2007-NULL = HIGH-VALUES
              MOVE LQU-MONT-DAL2007-NULL
                TO (SF)-MONT-DAL2007-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-MONT-DAL2007
                TO (SF)-MONT-DAL2007(IX-TAB-LQU)
           END-IF
           IF LQU-IMPB-CNBT-INPSTFM-NULL = HIGH-VALUES
              MOVE LQU-IMPB-CNBT-INPSTFM-NULL
                TO (SF)-IMPB-CNBT-INPSTFM-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPB-CNBT-INPSTFM
                TO (SF)-IMPB-CNBT-INPSTFM(IX-TAB-LQU)
           END-IF
           IF LQU-IMP-LRD-DA-RIMB-NULL = HIGH-VALUES
              MOVE LQU-IMP-LRD-DA-RIMB-NULL
                TO (SF)-IMP-LRD-DA-RIMB-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMP-LRD-DA-RIMB
                TO (SF)-IMP-LRD-DA-RIMB(IX-TAB-LQU)
           END-IF
           IF LQU-IMP-DIR-DA-RIMB-NULL = HIGH-VALUES
              MOVE LQU-IMP-DIR-DA-RIMB-NULL
                TO (SF)-IMP-DIR-DA-RIMB-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMP-DIR-DA-RIMB
                TO (SF)-IMP-DIR-DA-RIMB(IX-TAB-LQU)
           END-IF
           IF LQU-RIS-MAT-NULL = HIGH-VALUES
              MOVE LQU-RIS-MAT-NULL
                TO (SF)-RIS-MAT-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-RIS-MAT
                TO (SF)-RIS-MAT(IX-TAB-LQU)
           END-IF
           IF LQU-RIS-SPE-NULL = HIGH-VALUES
              MOVE LQU-RIS-SPE-NULL
                TO (SF)-RIS-SPE-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-RIS-SPE
                TO (SF)-RIS-SPE(IX-TAB-LQU)
           END-IF
           MOVE LQU-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-LQU)
           MOVE LQU-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-LQU)
           MOVE LQU-DS-VER
             TO (SF)-DS-VER(IX-TAB-LQU)
           MOVE LQU-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-LQU)
           MOVE LQU-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-LQU)
           MOVE LQU-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-LQU)
           MOVE LQU-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-LQU)
           IF LQU-TOT-IAS-PNL-NULL = HIGH-VALUES
              MOVE LQU-TOT-IAS-PNL-NULL
                TO (SF)-TOT-IAS-PNL-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TOT-IAS-PNL
                TO (SF)-TOT-IAS-PNL(IX-TAB-LQU)
           END-IF
           IF LQU-FL-EVE-GARTO-NULL = HIGH-VALUES
              MOVE LQU-FL-EVE-GARTO-NULL
                TO (SF)-FL-EVE-GARTO-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-FL-EVE-GARTO
                TO (SF)-FL-EVE-GARTO(IX-TAB-LQU)
           END-IF
           IF LQU-IMP-REN-K1-NULL = HIGH-VALUES
              MOVE LQU-IMP-REN-K1-NULL
                TO (SF)-IMP-REN-K1-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMP-REN-K1
                TO (SF)-IMP-REN-K1(IX-TAB-LQU)
           END-IF
           IF LQU-IMP-REN-K2-NULL = HIGH-VALUES
              MOVE LQU-IMP-REN-K2-NULL
                TO (SF)-IMP-REN-K2-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMP-REN-K2
                TO (SF)-IMP-REN-K2(IX-TAB-LQU)
           END-IF
           IF LQU-IMP-REN-K3-NULL = HIGH-VALUES
              MOVE LQU-IMP-REN-K3-NULL
                TO (SF)-IMP-REN-K3-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMP-REN-K3
                TO (SF)-IMP-REN-K3(IX-TAB-LQU)
           END-IF
           IF LQU-PC-REN-K1-NULL = HIGH-VALUES
              MOVE LQU-PC-REN-K1-NULL
                TO (SF)-PC-REN-K1-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-PC-REN-K1
                TO (SF)-PC-REN-K1(IX-TAB-LQU)
           END-IF
           IF LQU-PC-REN-K2-NULL = HIGH-VALUES
              MOVE LQU-PC-REN-K2-NULL
                TO (SF)-PC-REN-K2-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-PC-REN-K2
                TO (SF)-PC-REN-K2(IX-TAB-LQU)
           END-IF
           IF LQU-PC-REN-K3-NULL = HIGH-VALUES
              MOVE LQU-PC-REN-K3-NULL
                TO (SF)-PC-REN-K3-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-PC-REN-K3
                TO (SF)-PC-REN-K3(IX-TAB-LQU)
           END-IF
           IF LQU-TP-CAUS-ANTIC-NULL = HIGH-VALUES
              MOVE LQU-TP-CAUS-ANTIC-NULL
                TO (SF)-TP-CAUS-ANTIC-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-TP-CAUS-ANTIC
                TO (SF)-TP-CAUS-ANTIC(IX-TAB-LQU)
           END-IF
           IF LQU-IMP-LRD-LIQTO-RILT-NULL = HIGH-VALUES
              MOVE LQU-IMP-LRD-LIQTO-RILT-NULL
                TO (SF)-IMP-LRD-LIQTO-RILT-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMP-LRD-LIQTO-RILT
                TO (SF)-IMP-LRD-LIQTO-RILT(IX-TAB-LQU)
           END-IF
           IF LQU-IMPST-APPL-RILT-NULL = HIGH-VALUES
              MOVE LQU-IMPST-APPL-RILT-NULL
                TO (SF)-IMPST-APPL-RILT-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPST-APPL-RILT
                TO (SF)-IMPST-APPL-RILT(IX-TAB-LQU)
           END-IF
           IF LQU-PC-RISC-PARZ-NULL = HIGH-VALUES
              MOVE LQU-PC-RISC-PARZ-NULL
                TO (SF)-PC-RISC-PARZ-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-PC-RISC-PARZ
                TO (SF)-PC-RISC-PARZ(IX-TAB-LQU)
           END-IF
           IF LQU-IMPST-BOLLO-TOT-V-NULL = HIGH-VALUES
              MOVE LQU-IMPST-BOLLO-TOT-V-NULL
                TO (SF)-IMPST-BOLLO-TOT-V-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPST-BOLLO-TOT-V
                TO (SF)-IMPST-BOLLO-TOT-V(IX-TAB-LQU)
           END-IF
           IF LQU-IMPST-BOLLO-DETT-C-NULL = HIGH-VALUES
              MOVE LQU-IMPST-BOLLO-DETT-C-NULL
                TO (SF)-IMPST-BOLLO-DETT-C-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPST-BOLLO-DETT-C
                TO (SF)-IMPST-BOLLO-DETT-C(IX-TAB-LQU)
           END-IF
           IF LQU-IMPST-BOLLO-TOT-SW-NULL = HIGH-VALUES
              MOVE LQU-IMPST-BOLLO-TOT-SW-NULL
                TO (SF)-IMPST-BOLLO-TOT-SW-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPST-BOLLO-TOT-SW
                TO (SF)-IMPST-BOLLO-TOT-SW(IX-TAB-LQU)
           END-IF
           IF LQU-IMPST-BOLLO-TOT-AA-NULL = HIGH-VALUES
              MOVE LQU-IMPST-BOLLO-TOT-AA-NULL
                TO (SF)-IMPST-BOLLO-TOT-AA-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPST-BOLLO-TOT-AA
                TO (SF)-IMPST-BOLLO-TOT-AA(IX-TAB-LQU)
           END-IF
           IF LQU-IMPB-VIS-1382011-NULL = HIGH-VALUES
              MOVE LQU-IMPB-VIS-1382011-NULL
                TO (SF)-IMPB-VIS-1382011-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPB-VIS-1382011
                TO (SF)-IMPB-VIS-1382011(IX-TAB-LQU)
           END-IF
           IF LQU-IMPST-VIS-1382011-NULL = HIGH-VALUES
              MOVE LQU-IMPST-VIS-1382011-NULL
                TO (SF)-IMPST-VIS-1382011-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPST-VIS-1382011
                TO (SF)-IMPST-VIS-1382011(IX-TAB-LQU)
           END-IF
           IF LQU-IMPB-IS-1382011-NULL = HIGH-VALUES
              MOVE LQU-IMPB-IS-1382011-NULL
                TO (SF)-IMPB-IS-1382011-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPB-IS-1382011
                TO (SF)-IMPB-IS-1382011(IX-TAB-LQU)
           END-IF
           IF LQU-IMPST-SOST-1382011-NULL = HIGH-VALUES
              MOVE LQU-IMPST-SOST-1382011-NULL
                TO (SF)-IMPST-SOST-1382011-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPST-SOST-1382011
                TO (SF)-IMPST-SOST-1382011(IX-TAB-LQU)
           END-IF
           IF LQU-PC-ABB-TIT-STAT-NULL = HIGH-VALUES
              MOVE LQU-PC-ABB-TIT-STAT-NULL
                TO (SF)-PC-ABB-TIT-STAT-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-PC-ABB-TIT-STAT
                TO (SF)-PC-ABB-TIT-STAT(IX-TAB-LQU)
           END-IF
           IF LQU-IMPB-BOLLO-DETT-C-NULL = HIGH-VALUES
              MOVE LQU-IMPB-BOLLO-DETT-C-NULL
                TO (SF)-IMPB-BOLLO-DETT-C-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPB-BOLLO-DETT-C
                TO (SF)-IMPB-BOLLO-DETT-C(IX-TAB-LQU)
           END-IF
           IF LQU-FL-PRE-COMP-NULL = HIGH-VALUES
              MOVE LQU-FL-PRE-COMP-NULL
                TO (SF)-FL-PRE-COMP-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-FL-PRE-COMP
                TO (SF)-FL-PRE-COMP(IX-TAB-LQU)
           END-IF
           IF LQU-IMPB-VIS-662014-NULL = HIGH-VALUES
              MOVE LQU-IMPB-VIS-662014-NULL
                TO (SF)-IMPB-VIS-662014-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPB-VIS-662014
                TO (SF)-IMPB-VIS-662014(IX-TAB-LQU)
           END-IF
           IF LQU-IMPST-VIS-662014-NULL = HIGH-VALUES
              MOVE LQU-IMPST-VIS-662014-NULL
                TO (SF)-IMPST-VIS-662014-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPST-VIS-662014
                TO (SF)-IMPST-VIS-662014(IX-TAB-LQU)
           END-IF
           IF LQU-IMPB-IS-662014-NULL = HIGH-VALUES
              MOVE LQU-IMPB-IS-662014-NULL
                TO (SF)-IMPB-IS-662014-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPB-IS-662014
                TO (SF)-IMPB-IS-662014(IX-TAB-LQU)
           END-IF
           IF LQU-IMPST-SOST-662014-NULL = HIGH-VALUES
              MOVE LQU-IMPST-SOST-662014-NULL
                TO (SF)-IMPST-SOST-662014-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMPST-SOST-662014
                TO (SF)-IMPST-SOST-662014(IX-TAB-LQU)
           END-IF
           IF LQU-PC-ABB-TS-662014-NULL = HIGH-VALUES
              MOVE LQU-PC-ABB-TS-662014-NULL
                TO (SF)-PC-ABB-TS-662014-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-PC-ABB-TS-662014
                TO (SF)-PC-ABB-TS-662014(IX-TAB-LQU)
           END-IF
           IF LQU-IMP-LRD-CALC-CP-NULL = HIGH-VALUES
              MOVE LQU-IMP-LRD-CALC-CP-NULL
                TO (SF)-IMP-LRD-CALC-CP-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-IMP-LRD-CALC-CP
                TO (SF)-IMP-LRD-CALC-CP(IX-TAB-LQU)
           END-IF
           IF LQU-COS-TUNNEL-USCITA-NULL = HIGH-VALUES
              MOVE LQU-COS-TUNNEL-USCITA-NULL
                TO (SF)-COS-TUNNEL-USCITA-NULL(IX-TAB-LQU)
           ELSE
              MOVE LQU-COS-TUNNEL-USCITA
                TO (SF)-COS-TUNNEL-USCITA(IX-TAB-LQU)
           END-IF.
       VALORIZZA-OUTPUT-LQU-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVP613
      *   ULTIMO AGG. 24 OTT 2019
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-P61.
           MOVE P61-ID-D-CRIST
             TO (SF)-ID-PTF
           MOVE P61-ID-D-CRIST
             TO (SF)-ID-D-CRIST
           MOVE P61-ID-POLI
             TO (SF)-ID-POLI
           MOVE P61-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE P61-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ
           IF P61-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE P61-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL
           ELSE
              MOVE P61-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU
           END-IF
           MOVE P61-DT-INI-EFF
             TO (SF)-DT-INI-EFF
           MOVE P61-DT-END-EFF
             TO (SF)-DT-END-EFF
           MOVE P61-COD-PROD
             TO (SF)-COD-PROD
           MOVE P61-DT-DECOR
             TO (SF)-DT-DECOR
           MOVE P61-DS-RIGA
             TO (SF)-DS-RIGA
           MOVE P61-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE P61-DS-VER
             TO (SF)-DS-VER
           MOVE P61-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ
           MOVE P61-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ
           MOVE P61-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE P61-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           IF P61-RIS-MAT-31122011-NULL = HIGH-VALUES
              MOVE P61-RIS-MAT-31122011-NULL
                TO (SF)-RIS-MAT-31122011-NULL
           ELSE
              MOVE P61-RIS-MAT-31122011
                TO (SF)-RIS-MAT-31122011
           END-IF
           IF P61-PRE-V-31122011-NULL = HIGH-VALUES
              MOVE P61-PRE-V-31122011-NULL
                TO (SF)-PRE-V-31122011-NULL
           ELSE
              MOVE P61-PRE-V-31122011
                TO (SF)-PRE-V-31122011
           END-IF
           IF P61-PRE-RSH-V-31122011-NULL = HIGH-VALUES
              MOVE P61-PRE-RSH-V-31122011-NULL
                TO (SF)-PRE-RSH-V-31122011-NULL
           ELSE
              MOVE P61-PRE-RSH-V-31122011
                TO (SF)-PRE-RSH-V-31122011
           END-IF
           IF P61-CPT-RIVTO-31122011-NULL = HIGH-VALUES
              MOVE P61-CPT-RIVTO-31122011-NULL
                TO (SF)-CPT-RIVTO-31122011-NULL
           ELSE
              MOVE P61-CPT-RIVTO-31122011
                TO (SF)-CPT-RIVTO-31122011
           END-IF
           IF P61-IMPB-VIS-31122011-NULL = HIGH-VALUES
              MOVE P61-IMPB-VIS-31122011-NULL
                TO (SF)-IMPB-VIS-31122011-NULL
           ELSE
              MOVE P61-IMPB-VIS-31122011
                TO (SF)-IMPB-VIS-31122011
           END-IF
           IF P61-IMPB-IS-31122011-NULL = HIGH-VALUES
              MOVE P61-IMPB-IS-31122011-NULL
                TO (SF)-IMPB-IS-31122011-NULL
           ELSE
              MOVE P61-IMPB-IS-31122011
                TO (SF)-IMPB-IS-31122011
           END-IF
           IF P61-IMPB-VIS-RP-P2011-NULL = HIGH-VALUES
              MOVE P61-IMPB-VIS-RP-P2011-NULL
                TO (SF)-IMPB-VIS-RP-P2011-NULL
           ELSE
              MOVE P61-IMPB-VIS-RP-P2011
                TO (SF)-IMPB-VIS-RP-P2011
           END-IF
           IF P61-IMPB-IS-RP-P2011-NULL = HIGH-VALUES
              MOVE P61-IMPB-IS-RP-P2011-NULL
                TO (SF)-IMPB-IS-RP-P2011-NULL
           ELSE
              MOVE P61-IMPB-IS-RP-P2011
                TO (SF)-IMPB-IS-RP-P2011
           END-IF
           IF P61-PRE-V-30062014-NULL = HIGH-VALUES
              MOVE P61-PRE-V-30062014-NULL
                TO (SF)-PRE-V-30062014-NULL
           ELSE
              MOVE P61-PRE-V-30062014
                TO (SF)-PRE-V-30062014
           END-IF
           IF P61-PRE-RSH-V-30062014-NULL = HIGH-VALUES
              MOVE P61-PRE-RSH-V-30062014-NULL
                TO (SF)-PRE-RSH-V-30062014-NULL
           ELSE
              MOVE P61-PRE-RSH-V-30062014
                TO (SF)-PRE-RSH-V-30062014
           END-IF
           IF P61-CPT-INI-30062014-NULL = HIGH-VALUES
              MOVE P61-CPT-INI-30062014-NULL
                TO (SF)-CPT-INI-30062014-NULL
           ELSE
              MOVE P61-CPT-INI-30062014
                TO (SF)-CPT-INI-30062014
           END-IF
           IF P61-IMPB-VIS-30062014-NULL = HIGH-VALUES
              MOVE P61-IMPB-VIS-30062014-NULL
                TO (SF)-IMPB-VIS-30062014-NULL
           ELSE
              MOVE P61-IMPB-VIS-30062014
                TO (SF)-IMPB-VIS-30062014
           END-IF
           IF P61-IMPB-IS-30062014-NULL = HIGH-VALUES
              MOVE P61-IMPB-IS-30062014-NULL
                TO (SF)-IMPB-IS-30062014-NULL
           ELSE
              MOVE P61-IMPB-IS-30062014
                TO (SF)-IMPB-IS-30062014
           END-IF
           IF P61-IMPB-VIS-RP-P62014-NULL = HIGH-VALUES
              MOVE P61-IMPB-VIS-RP-P62014-NULL
                TO (SF)-IMPB-VIS-RP-P62014-NULL
           ELSE
              MOVE P61-IMPB-VIS-RP-P62014
                TO (SF)-IMPB-VIS-RP-P62014
           END-IF
           IF P61-IMPB-IS-RP-P62014-NULL = HIGH-VALUES
              MOVE P61-IMPB-IS-RP-P62014-NULL
                TO (SF)-IMPB-IS-RP-P62014-NULL
           ELSE
              MOVE P61-IMPB-IS-RP-P62014
                TO (SF)-IMPB-IS-RP-P62014
           END-IF
           IF P61-RIS-MAT-30062014-NULL = HIGH-VALUES
              MOVE P61-RIS-MAT-30062014-NULL
                TO (SF)-RIS-MAT-30062014-NULL
           ELSE
              MOVE P61-RIS-MAT-30062014
                TO (SF)-RIS-MAT-30062014
           END-IF
           IF P61-ID-ADES-NULL = HIGH-VALUES
              MOVE P61-ID-ADES-NULL
                TO (SF)-ID-ADES-NULL
           ELSE
              MOVE P61-ID-ADES
                TO (SF)-ID-ADES
           END-IF
           IF P61-MONT-LRD-END2000-NULL = HIGH-VALUES
              MOVE P61-MONT-LRD-END2000-NULL
                TO (SF)-MONT-LRD-END2000-NULL
           ELSE
              MOVE P61-MONT-LRD-END2000
                TO (SF)-MONT-LRD-END2000
           END-IF
           IF P61-PRE-LRD-END2000-NULL = HIGH-VALUES
              MOVE P61-PRE-LRD-END2000-NULL
                TO (SF)-PRE-LRD-END2000-NULL
           ELSE
              MOVE P61-PRE-LRD-END2000
                TO (SF)-PRE-LRD-END2000
           END-IF
           IF P61-RENDTO-LRD-END2000-NULL = HIGH-VALUES
              MOVE P61-RENDTO-LRD-END2000-NULL
                TO (SF)-RENDTO-LRD-END2000-NULL
           ELSE
              MOVE P61-RENDTO-LRD-END2000
                TO (SF)-RENDTO-LRD-END2000
           END-IF
           IF P61-MONT-LRD-END2006-NULL = HIGH-VALUES
              MOVE P61-MONT-LRD-END2006-NULL
                TO (SF)-MONT-LRD-END2006-NULL
           ELSE
              MOVE P61-MONT-LRD-END2006
                TO (SF)-MONT-LRD-END2006
           END-IF
           IF P61-PRE-LRD-END2006-NULL = HIGH-VALUES
              MOVE P61-PRE-LRD-END2006-NULL
                TO (SF)-PRE-LRD-END2006-NULL
           ELSE
              MOVE P61-PRE-LRD-END2006
                TO (SF)-PRE-LRD-END2006
           END-IF
           IF P61-RENDTO-LRD-END2006-NULL = HIGH-VALUES
              MOVE P61-RENDTO-LRD-END2006-NULL
                TO (SF)-RENDTO-LRD-END2006-NULL
           ELSE
              MOVE P61-RENDTO-LRD-END2006
                TO (SF)-RENDTO-LRD-END2006
           END-IF
           IF P61-MONT-LRD-DAL2007-NULL = HIGH-VALUES
              MOVE P61-MONT-LRD-DAL2007-NULL
                TO (SF)-MONT-LRD-DAL2007-NULL
           ELSE
              MOVE P61-MONT-LRD-DAL2007
                TO (SF)-MONT-LRD-DAL2007
           END-IF
           IF P61-PRE-LRD-DAL2007-NULL = HIGH-VALUES
              MOVE P61-PRE-LRD-DAL2007-NULL
                TO (SF)-PRE-LRD-DAL2007-NULL
           ELSE
              MOVE P61-PRE-LRD-DAL2007
                TO (SF)-PRE-LRD-DAL2007
           END-IF
           IF P61-RENDTO-LRD-DAL2007-NULL = HIGH-VALUES
              MOVE P61-RENDTO-LRD-DAL2007-NULL
                TO (SF)-RENDTO-LRD-DAL2007-NULL
           ELSE
              MOVE P61-RENDTO-LRD-DAL2007
                TO (SF)-RENDTO-LRD-DAL2007
           END-IF
           IF P61-ID-TRCH-DI-GAR-NULL = HIGH-VALUES
              MOVE P61-ID-TRCH-DI-GAR-NULL
                TO (SF)-ID-TRCH-DI-GAR-NULL
           ELSE
              MOVE P61-ID-TRCH-DI-GAR
                TO (SF)-ID-TRCH-DI-GAR
           END-IF.
       VALORIZZA-OUTPUT-P61-EX.
           EXIT.

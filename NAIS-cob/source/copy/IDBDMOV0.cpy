           EXEC SQL DECLARE MOVI TABLE
           (
             ID_MOVI             DECIMAL(9, 0) NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             ID_OGG              DECIMAL(9, 0),
             IB_OGG              CHAR(40),
             IB_MOVI             CHAR(40),
             TP_OGG              CHAR(2),
             ID_RICH             DECIMAL(9, 0),
             TP_MOVI             DECIMAL(5, 0),
             DT_EFF              DATE NOT NULL,
             ID_MOVI_ANN         DECIMAL(9, 0),
             ID_MOVI_COLLG       DECIMAL(9, 0),
             DS_OPER_SQL         CHAR(1) NOT NULL WITH DEFAULT,
             DS_VER              DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_UTENTE           CHAR(20) NOT NULL WITH DEFAULT,
             DS_STATO_ELAB       CHAR(1) NOT NULL WITH DEFAULT
          ) END-EXEC.

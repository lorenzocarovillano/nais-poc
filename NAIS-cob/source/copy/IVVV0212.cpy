      *----------------------------------------------------------------*
      *   AREA INPUT VARIABILI
      *   PORTAFOGLIO VITA - PROCESSO VENDITA
      *   LUNG.
      *----------------------------------------------------------------*
      * 03 AREA-INPUT.
           05 (SF)-KEY-AUT-OPER1                PIC X(020).
           05 (SF)-KEY-AUT-OPER2                PIC X(020).
           05 (SF)-KEY-AUT-OPER3                PIC X(020).
           05 (SF)-KEY-AUT-OPER4                PIC X(020).
           05 (SF)-KEY-AUT-OPER5                PIC X(020).

      *    AREA VARIABILI AUTONOMIA OPERATIVA
      * 03 AREA-OUTPUT.
           05 (SF)-ELE-CTRL-AUT-OPER-MAX        PIC S9(04) COMP-3.
           05 (SF)-CTRL-AUT-OPER                OCCURS 20.
             10 (SF)-COD-ERRORE                 PIC S9(09) COMP-5.
             10 (SF)-COD-LIV-AUT                PIC S9(05) COMP-3.
             10 (SF)-TP-MOT-DEROGA              PIC  X(02).
             10 (SF)-MOD-VERIFICA               PIC  X(08).
             10 (SF)-CODICE-CONDIZIONE          PIC X(50).
             10 (SF)-PROGRESS-CONDITION         PIC S9(03)V COMP-3.
             10 (SF)-RISULTATO-CONDIZIONE       PIC  X(01).

             10 (SF)-ELE-PARAM-MAX              PIC S9(04) COMP-3.
             10 (SF)-TAB-PARAM                  OCCURS 20.
                15 (SF)-COD-PARAM               PIC X(030).
                15 (SF)-VAL-PARAM               PIC X(100).


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVXAB4
      *   ULTIMO AGG. 09 LUG 2009
      *------------------------------------------------------------

       INIZIA-TOT-XAB.

           PERFORM INIZIA-ZEROES-XAB THRU INIZIA-ZEROES-XAB-EX

           PERFORM INIZIA-SPACES-XAB THRU INIZIA-SPACES-XAB-EX

           PERFORM INIZIA-NULL-XAB THRU INIZIA-NULL-XAB-EX.

       INIZIA-TOT-XAB-EX.
           EXIT.

       INIZIA-NULL-XAB.
           MOVE HIGH-VALUES TO (SF)-DESC
           MOVE HIGH-VALUES TO (SF)-FL-REC-AUT-NULL.
       INIZIA-NULL-XAB-EX.
           EXIT.

       INIZIA-ZEROES-XAB.
           MOVE 0 TO (SF)-COD-COMP-ANIA
           MOVE 0 TO (SF)-DS-VER
           MOVE 0 TO (SF)-DS-TS-CPTZ.
       INIZIA-ZEROES-XAB-EX.
           EXIT.

       INIZIA-SPACES-XAB.
           MOVE SPACES TO (SF)-COD-BLOCCO
           MOVE SPACES TO (SF)-DS-OPER-SQL
           MOVE SPACES TO (SF)-DS-UTENTE
           MOVE SPACES TO (SF)-DS-STATO-ELAB.
       INIZIA-SPACES-XAB-EX.
           EXIT.

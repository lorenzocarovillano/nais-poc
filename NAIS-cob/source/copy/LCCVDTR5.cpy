
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVDTR5
      *   ULTIMO AGG. 28 NOV 2014
      *------------------------------------------------------------

       VAL-DCLGEN-DTR.
           MOVE (SF)-TP-OGG(IX-TAB-DTR)
              TO DTR-TP-OGG
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-DTR)
              TO DTR-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-DTR)
              TO DTR-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-DTR) NOT NUMERIC
              MOVE 0 TO DTR-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-DTR)
              TO DTR-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-DTR) NOT NUMERIC
              MOVE 0 TO DTR-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-DTR)
              TO DTR-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-DTR)
              TO DTR-COD-COMP-ANIA
           IF (SF)-DT-INI-COP-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-DT-INI-COP-NULL(IX-TAB-DTR)
              TO DTR-DT-INI-COP-NULL
           ELSE
             IF (SF)-DT-INI-COP(IX-TAB-DTR) = ZERO
                MOVE HIGH-VALUES
                TO DTR-DT-INI-COP-NULL
             ELSE
              MOVE (SF)-DT-INI-COP(IX-TAB-DTR)
              TO DTR-DT-INI-COP
             END-IF
           END-IF
           IF (SF)-DT-END-COP-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-DT-END-COP-NULL(IX-TAB-DTR)
              TO DTR-DT-END-COP-NULL
           ELSE
             IF (SF)-DT-END-COP(IX-TAB-DTR) = ZERO
                MOVE HIGH-VALUES
                TO DTR-DT-END-COP-NULL
             ELSE
              MOVE (SF)-DT-END-COP(IX-TAB-DTR)
              TO DTR-DT-END-COP
             END-IF
           END-IF
           IF (SF)-PRE-NET-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-PRE-NET-NULL(IX-TAB-DTR)
              TO DTR-PRE-NET-NULL
           ELSE
              MOVE (SF)-PRE-NET(IX-TAB-DTR)
              TO DTR-PRE-NET
           END-IF
           IF (SF)-INTR-FRAZ-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-INTR-FRAZ-NULL(IX-TAB-DTR)
              TO DTR-INTR-FRAZ-NULL
           ELSE
              MOVE (SF)-INTR-FRAZ(IX-TAB-DTR)
              TO DTR-INTR-FRAZ
           END-IF
           IF (SF)-INTR-MORA-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-INTR-MORA-NULL(IX-TAB-DTR)
              TO DTR-INTR-MORA-NULL
           ELSE
              MOVE (SF)-INTR-MORA(IX-TAB-DTR)
              TO DTR-INTR-MORA
           END-IF
           IF (SF)-INTR-RETDT-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-INTR-RETDT-NULL(IX-TAB-DTR)
              TO DTR-INTR-RETDT-NULL
           ELSE
              MOVE (SF)-INTR-RETDT(IX-TAB-DTR)
              TO DTR-INTR-RETDT
           END-IF
           IF (SF)-INTR-RIAT-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-INTR-RIAT-NULL(IX-TAB-DTR)
              TO DTR-INTR-RIAT-NULL
           ELSE
              MOVE (SF)-INTR-RIAT(IX-TAB-DTR)
              TO DTR-INTR-RIAT
           END-IF
           IF (SF)-DIR-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-DIR-NULL(IX-TAB-DTR)
              TO DTR-DIR-NULL
           ELSE
              MOVE (SF)-DIR(IX-TAB-DTR)
              TO DTR-DIR
           END-IF
           IF (SF)-SPE-MED-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-SPE-MED-NULL(IX-TAB-DTR)
              TO DTR-SPE-MED-NULL
           ELSE
              MOVE (SF)-SPE-MED(IX-TAB-DTR)
              TO DTR-SPE-MED
           END-IF
           IF (SF)-SPE-AGE-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-SPE-AGE-NULL(IX-TAB-DTR)
              TO DTR-SPE-AGE-NULL
           ELSE
              MOVE (SF)-SPE-AGE(IX-TAB-DTR)
              TO DTR-SPE-AGE
           END-IF
           IF (SF)-TAX-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-TAX-NULL(IX-TAB-DTR)
              TO DTR-TAX-NULL
           ELSE
              MOVE (SF)-TAX(IX-TAB-DTR)
              TO DTR-TAX
           END-IF
           IF (SF)-SOPR-SAN-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-SOPR-SAN-NULL(IX-TAB-DTR)
              TO DTR-SOPR-SAN-NULL
           ELSE
              MOVE (SF)-SOPR-SAN(IX-TAB-DTR)
              TO DTR-SOPR-SAN
           END-IF
           IF (SF)-SOPR-SPO-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-SOPR-SPO-NULL(IX-TAB-DTR)
              TO DTR-SOPR-SPO-NULL
           ELSE
              MOVE (SF)-SOPR-SPO(IX-TAB-DTR)
              TO DTR-SOPR-SPO
           END-IF
           IF (SF)-SOPR-TEC-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-SOPR-TEC-NULL(IX-TAB-DTR)
              TO DTR-SOPR-TEC-NULL
           ELSE
              MOVE (SF)-SOPR-TEC(IX-TAB-DTR)
              TO DTR-SOPR-TEC
           END-IF
           IF (SF)-SOPR-PROF-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-SOPR-PROF-NULL(IX-TAB-DTR)
              TO DTR-SOPR-PROF-NULL
           ELSE
              MOVE (SF)-SOPR-PROF(IX-TAB-DTR)
              TO DTR-SOPR-PROF
           END-IF
           IF (SF)-SOPR-ALT-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-SOPR-ALT-NULL(IX-TAB-DTR)
              TO DTR-SOPR-ALT-NULL
           ELSE
              MOVE (SF)-SOPR-ALT(IX-TAB-DTR)
              TO DTR-SOPR-ALT
           END-IF
           IF (SF)-PRE-TOT-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-PRE-TOT-NULL(IX-TAB-DTR)
              TO DTR-PRE-TOT-NULL
           ELSE
              MOVE (SF)-PRE-TOT(IX-TAB-DTR)
              TO DTR-PRE-TOT
           END-IF
           IF (SF)-PRE-PP-IAS-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-PRE-PP-IAS-NULL(IX-TAB-DTR)
              TO DTR-PRE-PP-IAS-NULL
           ELSE
              MOVE (SF)-PRE-PP-IAS(IX-TAB-DTR)
              TO DTR-PRE-PP-IAS
           END-IF
           IF (SF)-PRE-SOLO-RSH-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-PRE-SOLO-RSH-NULL(IX-TAB-DTR)
              TO DTR-PRE-SOLO-RSH-NULL
           ELSE
              MOVE (SF)-PRE-SOLO-RSH(IX-TAB-DTR)
              TO DTR-PRE-SOLO-RSH
           END-IF
           IF (SF)-CAR-IAS-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-CAR-IAS-NULL(IX-TAB-DTR)
              TO DTR-CAR-IAS-NULL
           ELSE
              MOVE (SF)-CAR-IAS(IX-TAB-DTR)
              TO DTR-CAR-IAS
           END-IF
           IF (SF)-PROV-ACQ-1AA-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-PROV-ACQ-1AA-NULL(IX-TAB-DTR)
              TO DTR-PROV-ACQ-1AA-NULL
           ELSE
              MOVE (SF)-PROV-ACQ-1AA(IX-TAB-DTR)
              TO DTR-PROV-ACQ-1AA
           END-IF
           IF (SF)-PROV-ACQ-2AA-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-PROV-ACQ-2AA-NULL(IX-TAB-DTR)
              TO DTR-PROV-ACQ-2AA-NULL
           ELSE
              MOVE (SF)-PROV-ACQ-2AA(IX-TAB-DTR)
              TO DTR-PROV-ACQ-2AA
           END-IF
           IF (SF)-PROV-RICOR-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-PROV-RICOR-NULL(IX-TAB-DTR)
              TO DTR-PROV-RICOR-NULL
           ELSE
              MOVE (SF)-PROV-RICOR(IX-TAB-DTR)
              TO DTR-PROV-RICOR
           END-IF
           IF (SF)-PROV-INC-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-PROV-INC-NULL(IX-TAB-DTR)
              TO DTR-PROV-INC-NULL
           ELSE
              MOVE (SF)-PROV-INC(IX-TAB-DTR)
              TO DTR-PROV-INC
           END-IF
           IF (SF)-PROV-DA-REC-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-PROV-DA-REC-NULL(IX-TAB-DTR)
              TO DTR-PROV-DA-REC-NULL
           ELSE
              MOVE (SF)-PROV-DA-REC(IX-TAB-DTR)
              TO DTR-PROV-DA-REC
           END-IF
           IF (SF)-COD-DVS-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-COD-DVS-NULL(IX-TAB-DTR)
              TO DTR-COD-DVS-NULL
           ELSE
              MOVE (SF)-COD-DVS(IX-TAB-DTR)
              TO DTR-COD-DVS
           END-IF
           IF (SF)-FRQ-MOVI-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-FRQ-MOVI-NULL(IX-TAB-DTR)
              TO DTR-FRQ-MOVI-NULL
           ELSE
              MOVE (SF)-FRQ-MOVI(IX-TAB-DTR)
              TO DTR-FRQ-MOVI
           END-IF
           MOVE (SF)-TP-RGM-FISC(IX-TAB-DTR)
              TO DTR-TP-RGM-FISC
           IF (SF)-COD-TARI-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-COD-TARI-NULL(IX-TAB-DTR)
              TO DTR-COD-TARI-NULL
           ELSE
              MOVE (SF)-COD-TARI(IX-TAB-DTR)
              TO DTR-COD-TARI
           END-IF
           MOVE (SF)-TP-STAT-TIT(IX-TAB-DTR)
              TO DTR-TP-STAT-TIT
           IF (SF)-IMP-AZ-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-IMP-AZ-NULL(IX-TAB-DTR)
              TO DTR-IMP-AZ-NULL
           ELSE
              MOVE (SF)-IMP-AZ(IX-TAB-DTR)
              TO DTR-IMP-AZ
           END-IF
           IF (SF)-IMP-ADER-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-IMP-ADER-NULL(IX-TAB-DTR)
              TO DTR-IMP-ADER-NULL
           ELSE
              MOVE (SF)-IMP-ADER(IX-TAB-DTR)
              TO DTR-IMP-ADER
           END-IF
           IF (SF)-IMP-TFR-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-IMP-TFR-NULL(IX-TAB-DTR)
              TO DTR-IMP-TFR-NULL
           ELSE
              MOVE (SF)-IMP-TFR(IX-TAB-DTR)
              TO DTR-IMP-TFR
           END-IF
           IF (SF)-IMP-VOLO-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-IMP-VOLO-NULL(IX-TAB-DTR)
              TO DTR-IMP-VOLO-NULL
           ELSE
              MOVE (SF)-IMP-VOLO(IX-TAB-DTR)
              TO DTR-IMP-VOLO
           END-IF
           IF (SF)-FL-VLDT-TIT-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-FL-VLDT-TIT-NULL(IX-TAB-DTR)
              TO DTR-FL-VLDT-TIT-NULL
           ELSE
              MOVE (SF)-FL-VLDT-TIT(IX-TAB-DTR)
              TO DTR-FL-VLDT-TIT
           END-IF
           IF (SF)-CAR-ACQ-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-CAR-ACQ-NULL(IX-TAB-DTR)
              TO DTR-CAR-ACQ-NULL
           ELSE
              MOVE (SF)-CAR-ACQ(IX-TAB-DTR)
              TO DTR-CAR-ACQ
           END-IF
           IF (SF)-CAR-GEST-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-CAR-GEST-NULL(IX-TAB-DTR)
              TO DTR-CAR-GEST-NULL
           ELSE
              MOVE (SF)-CAR-GEST(IX-TAB-DTR)
              TO DTR-CAR-GEST
           END-IF
           IF (SF)-CAR-INC-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-CAR-INC-NULL(IX-TAB-DTR)
              TO DTR-CAR-INC-NULL
           ELSE
              MOVE (SF)-CAR-INC(IX-TAB-DTR)
              TO DTR-CAR-INC
           END-IF
           IF (SF)-MANFEE-ANTIC-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-MANFEE-ANTIC-NULL(IX-TAB-DTR)
              TO DTR-MANFEE-ANTIC-NULL
           ELSE
              MOVE (SF)-MANFEE-ANTIC(IX-TAB-DTR)
              TO DTR-MANFEE-ANTIC
           END-IF
           IF (SF)-MANFEE-RICOR-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-MANFEE-RICOR-NULL(IX-TAB-DTR)
              TO DTR-MANFEE-RICOR-NULL
           ELSE
              MOVE (SF)-MANFEE-RICOR(IX-TAB-DTR)
              TO DTR-MANFEE-RICOR
           END-IF
           IF (SF)-MANFEE-REC-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-MANFEE-REC-NULL(IX-TAB-DTR)
              TO DTR-MANFEE-REC-NULL
           ELSE
              MOVE (SF)-MANFEE-REC(IX-TAB-DTR)
              TO DTR-MANFEE-REC
           END-IF
           IF (SF)-TOT-INTR-PREST-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-TOT-INTR-PREST-NULL(IX-TAB-DTR)
              TO DTR-TOT-INTR-PREST-NULL
           ELSE
              MOVE (SF)-TOT-INTR-PREST(IX-TAB-DTR)
              TO DTR-TOT-INTR-PREST
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-DTR) NOT NUMERIC
              MOVE 0 TO DTR-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-DTR)
              TO DTR-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-DTR)
              TO DTR-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-DTR) NOT NUMERIC
              MOVE 0 TO DTR-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-DTR)
              TO DTR-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-DTR) NOT NUMERIC
              MOVE 0 TO DTR-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-DTR)
              TO DTR-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-DTR) NOT NUMERIC
              MOVE 0 TO DTR-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-DTR)
              TO DTR-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-DTR)
              TO DTR-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-DTR)
              TO DTR-DS-STATO-ELAB
           IF (SF)-IMP-TRASFE-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-IMP-TRASFE-NULL(IX-TAB-DTR)
              TO DTR-IMP-TRASFE-NULL
           ELSE
              MOVE (SF)-IMP-TRASFE(IX-TAB-DTR)
              TO DTR-IMP-TRASFE
           END-IF
           IF (SF)-IMP-TFR-STRC-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-IMP-TFR-STRC-NULL(IX-TAB-DTR)
              TO DTR-IMP-TFR-STRC-NULL
           ELSE
              MOVE (SF)-IMP-TFR-STRC(IX-TAB-DTR)
              TO DTR-IMP-TFR-STRC
           END-IF
           IF (SF)-ACQ-EXP-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-ACQ-EXP-NULL(IX-TAB-DTR)
              TO DTR-ACQ-EXP-NULL
           ELSE
              MOVE (SF)-ACQ-EXP(IX-TAB-DTR)
              TO DTR-ACQ-EXP
           END-IF
           IF (SF)-REMUN-ASS-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-REMUN-ASS-NULL(IX-TAB-DTR)
              TO DTR-REMUN-ASS-NULL
           ELSE
              MOVE (SF)-REMUN-ASS(IX-TAB-DTR)
              TO DTR-REMUN-ASS
           END-IF
           IF (SF)-COMMIS-INTER-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-COMMIS-INTER-NULL(IX-TAB-DTR)
              TO DTR-COMMIS-INTER-NULL
           ELSE
              MOVE (SF)-COMMIS-INTER(IX-TAB-DTR)
              TO DTR-COMMIS-INTER
           END-IF
           IF (SF)-CNBT-ANTIRAC-NULL(IX-TAB-DTR) = HIGH-VALUES
              MOVE (SF)-CNBT-ANTIRAC-NULL(IX-TAB-DTR)
              TO DTR-CNBT-ANTIRAC-NULL
           ELSE
              MOVE (SF)-CNBT-ANTIRAC(IX-TAB-DTR)
              TO DTR-CNBT-ANTIRAC
           END-IF.
       VAL-DCLGEN-DTR-EX.
           EXIT.

      ******************************************************************
      *   CALL DISPATCHER
      ******************************************************************

       CALL-DISPATCHER.

           MOVE IDSV0001-MODALITA-ESECUTIVA
                              TO IDSI0011-MODALITA-ESECUTIVA
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
           MOVE IDSV0001-COD-MAIN-BATCH
                              TO IDSI0011-COD-MAIN-BATCH
           MOVE IDSV0001-TIPO-MOVIMENTO
                              TO IDSI0011-TIPO-MOVIMENTO
           MOVE IDSV0001-SESSIONE
                              TO IDSI0011-SESSIONE
           MOVE IDSV0001-USER-NAME
                              TO IDSI0011-USER-NAME

           IF IDSI0011-DATA-INIZIO-EFFETTO = 0
              IF IDSV0001-LETT-ULT-IMMAGINE-SI
                 IF IDSI0011-SELECT
                 OR IDSI0011-FETCH-FIRST
                 OR IDSI0011-FETCH-NEXT
                    MOVE 99991230
                      TO IDSI0011-DATA-INIZIO-EFFETTO
                    MOVE 999912304023595999
                      TO IDSI0011-DATA-COMPETENZA
                 ELSE
                    MOVE IDSV0001-DATA-EFFETTO
                      TO IDSI0011-DATA-INIZIO-EFFETTO
                 END-IF
              ELSE
                 MOVE IDSV0001-DATA-EFFETTO
                   TO IDSI0011-DATA-INIZIO-EFFETTO
              END-IF
           END-IF

           IF IDSI0011-DATA-FINE-EFFETTO = 0
              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
           END-IF

           IF IDSI0011-DATA-COMPETENZA = 0
              MOVE IDSV0001-DATA-COMPETENZA
                              TO IDSI0011-DATA-COMPETENZA
           END-IF

           MOVE IDSV0001-DATA-COMP-AGG-STOR
                              TO IDSI0011-DATA-COMP-AGG-STOR

           IF IDSI0011-TRATT-DEFAULT

              MOVE IDSV0001-TRATTAMENTO-STORICITA
                              TO IDSI0011-TRATTAMENTO-STORICITA
           END-IF


           MOVE IDSV0001-FORMATO-DATA-DB
                              TO IDSI0011-FORMATO-DATA-DB

           MOVE IDSV0001-LIVELLO-DEBUG
                              TO IDSI0011-LIVELLO-DEBUG

           MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO


           SET IDSI0011-PTF-NEWLIFE          TO TRUE

           SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE

           MOVE 'IDSS0010'             TO IDSI0011-PGM

      *     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.

           CALL IDSI0011-PGM           USING IDSI0011-AREA
                                             IDSO0011-AREA.

           MOVE IDSO0011-SQLCODE-SIGNED
                                  TO IDSO0011-SQLCODE.

       CALL-DISPATCHER-EX.
           EXIT.





      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVALL3
      *   ULTIMO AGG. 31 MAR 2020
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-ALL.
           MOVE ALL-ID-AST-ALLOC
             TO (SF)-ID-PTF(IX-TAB-ALL)
           MOVE ALL-ID-AST-ALLOC
             TO (SF)-ID-AST-ALLOC(IX-TAB-ALL)
           MOVE ALL-ID-STRA-DI-INVST
             TO (SF)-ID-STRA-DI-INVST(IX-TAB-ALL)
           MOVE ALL-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-ALL)
           IF ALL-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE ALL-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-ALL)
           ELSE
              MOVE ALL-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-ALL)
           END-IF
           MOVE ALL-DT-INI-VLDT
             TO (SF)-DT-INI-VLDT(IX-TAB-ALL)
           IF ALL-DT-END-VLDT-NULL = HIGH-VALUES
              MOVE ALL-DT-END-VLDT-NULL
                TO (SF)-DT-END-VLDT-NULL(IX-TAB-ALL)
           ELSE
              MOVE ALL-DT-END-VLDT
                TO (SF)-DT-END-VLDT(IX-TAB-ALL)
           END-IF
           MOVE ALL-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-ALL)
           MOVE ALL-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-ALL)
           MOVE ALL-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-ALL)
           IF ALL-COD-FND-NULL = HIGH-VALUES
              MOVE ALL-COD-FND-NULL
                TO (SF)-COD-FND-NULL(IX-TAB-ALL)
           ELSE
              MOVE ALL-COD-FND
                TO (SF)-COD-FND(IX-TAB-ALL)
           END-IF
           IF ALL-COD-TARI-NULL = HIGH-VALUES
              MOVE ALL-COD-TARI-NULL
                TO (SF)-COD-TARI-NULL(IX-TAB-ALL)
           ELSE
              MOVE ALL-COD-TARI
                TO (SF)-COD-TARI(IX-TAB-ALL)
           END-IF
           IF ALL-TP-APPLZ-AST-NULL = HIGH-VALUES
              MOVE ALL-TP-APPLZ-AST-NULL
                TO (SF)-TP-APPLZ-AST-NULL(IX-TAB-ALL)
           ELSE
              MOVE ALL-TP-APPLZ-AST
                TO (SF)-TP-APPLZ-AST(IX-TAB-ALL)
           END-IF
           IF ALL-PC-RIP-AST-NULL = HIGH-VALUES
              MOVE ALL-PC-RIP-AST-NULL
                TO (SF)-PC-RIP-AST-NULL(IX-TAB-ALL)
           ELSE
              MOVE ALL-PC-RIP-AST
                TO (SF)-PC-RIP-AST(IX-TAB-ALL)
           END-IF
           MOVE ALL-TP-FND
             TO (SF)-TP-FND(IX-TAB-ALL)
           MOVE ALL-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-ALL)
           MOVE ALL-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-ALL)
           MOVE ALL-DS-VER
             TO (SF)-DS-VER(IX-TAB-ALL)
           MOVE ALL-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-ALL)
           MOVE ALL-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-ALL)
           MOVE ALL-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-ALL)
           MOVE ALL-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-ALL)
           IF ALL-PERIODO-NULL = HIGH-VALUES
              MOVE ALL-PERIODO-NULL
                TO (SF)-PERIODO-NULL(IX-TAB-ALL)
           ELSE
              MOVE ALL-PERIODO
                TO (SF)-PERIODO(IX-TAB-ALL)
           END-IF
           IF ALL-TP-RIBIL-FND-NULL = HIGH-VALUES
              MOVE ALL-TP-RIBIL-FND-NULL
                TO (SF)-TP-RIBIL-FND-NULL(IX-TAB-ALL)
           ELSE
              MOVE ALL-TP-RIBIL-FND
                TO (SF)-TP-RIBIL-FND(IX-TAB-ALL)
           END-IF.
       VALORIZZA-OUTPUT-ALL-EX.
           EXIT.

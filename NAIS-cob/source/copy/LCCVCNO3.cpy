
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVCNO3
      *   ULTIMO AGG. 16 MAR 2009
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-CNO.
           MOVE CNO-COD-COMPAGNIA-ANIA
             TO (SF)-COD-COMPAGNIA-ANIA
           MOVE CNO-FORMA-ASSICURATIVA
             TO (SF)-FORMA-ASSICURATIVA
           MOVE CNO-COD-OGGETTO
             TO (SF)-COD-OGGETTO
           MOVE CNO-POSIZIONE
             TO (SF)-POSIZIONE
           IF CNO-COD-STR-DATO-NULL = HIGH-VALUES
              MOVE CNO-COD-STR-DATO-NULL
                TO (SF)-COD-STR-DATO-NULL
           ELSE
              MOVE CNO-COD-STR-DATO
                TO (SF)-COD-STR-DATO
           END-IF
           IF CNO-COD-DATO-NULL = HIGH-VALUES
              MOVE CNO-COD-DATO-NULL
                TO (SF)-COD-DATO-NULL
           ELSE
              MOVE CNO-COD-DATO
                TO (SF)-COD-DATO
           END-IF
           IF CNO-VALORE-DEFAULT-NULL = HIGH-VALUES
              MOVE CNO-VALORE-DEFAULT-NULL
                TO (SF)-VALORE-DEFAULT-NULL
           ELSE
              MOVE CNO-VALORE-DEFAULT
                TO (SF)-VALORE-DEFAULT
           END-IF
           IF CNO-LUNGHEZZA-DATO-NULL = HIGH-VALUES
              MOVE CNO-LUNGHEZZA-DATO-NULL
                TO (SF)-LUNGHEZZA-DATO-NULL
           ELSE
              MOVE CNO-LUNGHEZZA-DATO
                TO (SF)-LUNGHEZZA-DATO
           END-IF
           IF CNO-FLAG-KEY-ULT-PROGR-NULL = HIGH-VALUES
              MOVE CNO-FLAG-KEY-ULT-PROGR-NULL
                TO (SF)-FLAG-KEY-ULT-PROGR-NULL
           ELSE
              MOVE CNO-FLAG-KEY-ULT-PROGR
                TO (SF)-FLAG-KEY-ULT-PROGR
           END-IF.
       VALORIZZA-OUTPUT-CNO-EX.
           EXIT.

      ******************************************************************
      *    TP_ANNULLO (Tipo Annullo)
      ******************************************************************
       01  WS-TP-ANNULLO                    PIC X(002) VALUE SPACES.
           88 TA-AMMISSIBILE                  VALUE 'AM'.
           88 TA-BLOCCANTE                    VALUE 'BL'.
           88 TA-ENTRAMBI                     VALUE 'EN'.
           88 TA-ESPLICITO                    VALUE 'ES'.
           88 TA-IMPLICITO                    VALUE 'IM'.

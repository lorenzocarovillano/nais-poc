           EXEC SQL DECLARE RIDEF_DATO_STR TABLE
           (
             COD_COMPAGNIA_ANIA  DECIMAL(5, 0) NOT NULL,
             TIPO_MOVIMENTO      DECIMAL(5, 0) NOT NULL,
             COD_DATO            CHAR(30) NOT NULL,
             COD_STR_DATO        CHAR(30) NOT NULL,
             DS_UTENTE           CHAR(20) NOT NULL WITH DEFAULT
          ) END-EXEC.

       01 IDSV0301.
           10 IDSV0301-ADDRESS                  POINTER.
           10 IDSV0301-COD-COMP-ANIA            PIC S9(5)V COMP-3.
           10 IDSV0301-ID-TEMPORARY-DATA        PIC S9(10)V COMP-3.
           10 IDSV0301-ALIAS-STR-DATO           PIC X(30).
           10 IDSV0301-NUM-FRAME                PIC S9(5)V COMP-3.
           10 IDSV0301-ID-SESSION               PIC X(20).
           10 IDSV0301-FL-CONTIGUOUS            PIC X(01).
              88 IDSV0301-CONTIGUOUS-SI         VALUE 'S'.
              88 IDSV0301-CONTIGUOUS-NO         VALUE 'N'    ,
                                                       X'FF' ,
                                                       X'00' ,
                                                       ' '.

           10 IDSV0301-TOTAL-RECURRENCE         PIC S9(5)V COMP-3.
           10 IDSV0301-PARTIAL-RECURRENCE       PIC S9(5)V COMP-3.
           10 IDSV0301-ACTUAL-RECURRENCE        PIC S9(5)V COMP-3.

           10 IDSV0301-TYPE-RECORD              PIC X(3).

           10 IDSV0301-OPERAZIONE               PIC X(02).
              88 IDSV0301-READ                  VALUE 'RE'.
              88 IDSV0301-READ-NEXT             VALUE 'RN'.
              88 IDSV0301-WRITE                 VALUE 'WR'.
              88 IDSV0301-DECLARE               VALUE 'DC'.

           10 IDSV0301-ACTION-TYPE              PIC X(1) VALUE 'E'.
              88 IDSV0301-DELETE-ACTION         VALUE 'D'.
              88 IDSV0301-APPEND-ACTION         VALUE 'E'.

           10 IDSV0301-TEMPORARY-TABLE          PIC X(001).
              88 IDSV0301-NO-TEMP-TABLE         VALUE '0'.
              88 IDSV0301-STATIC-TEMP-TABLE     VALUE '1'.
              88 IDSV0301-SESSION-TEMP-TABLE    VALUE '2'.

           10 IDSV0301-BUFFER-DATA-LEN          PIC 9(7).

           10 IDSV0301-AREA-ERRORI.
              15 IDSV0301-ESITO                 PIC X(002).
                 88 IDSV0301-ESITO-OK           VALUE 'OK'.
                 88 IDSV0301-ESITO-KO           VALUE 'KO'.
              15 IDSV0301-LOG-ERRORE.
                 20 IDSV0301-COD-SERVIZIO-BE    PIC X(008).
                 20 IDSV0301-DESC-ERRORE-ESTESA PIC X(200).

           10 IDSV0301-SQLCODE-SIGNED           PIC S9(009).
               88 IDSV0301-SUCCESSFUL-SQL       VALUE ZERO.
               88 IDSV0301-NOT-FOUND            VALUE +100.
               88 IDSV0301-DUPLICATE-KEY        VALUE -803.
               88 IDSV0301-MORE-THAN-ONE-ROW    VALUE -811.
               88 IDSV0301-DEADLOCK-TIMEOUT     VALUE -911
                                                      -913.
               88 IDSV0301-CONNECTION-ERROR     VALUE -924.

           10 IDSV0301-SQLCODE                  PIC --------9.




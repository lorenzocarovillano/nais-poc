      ******************************************************************
      *    TP_QTZ (Tipo Quotazione)
      ******************************************************************
       01  WS-TP-QTZ                             PIC X(02) VALUE SPACES.
           88 WS-QTZ-CALC-RIS                              VALUE 'CR'.
           88 WS-QTZ-OPZ-SOT-FND-INDEX                     VALUE 'OP'.
           88 WS-QTZ-ZER-COUP-SOT-FND-INDEX                VALUE 'ZC'.
           88 WS-QTZ-VAL-QUO-LRD                           VALUE 'QL'.
           88 WS-QTZ-QUOZ-BENCHMARK                        VALUE 'BC'.
           88 WS-QTZ-BOND                                  VALUE 'BO'.
           88 WS-QTZ-SWAP                                  VALUE 'SW'.
           88 WS-QTZ-TOTALE                                VALUE 'TT'.
           88 WS-QTZ-OPZIONE-SOT-FND-INDEX                 VALUE 'OZ'.
           88 WS-QTZ-BOND-INIZ                             VALUE 'IB'.
           88 WS-QTZ-SWAP-INIZ                             VALUE 'IS'.
           88 WS-QTZ-OPZIONE-INIZ                          VALUE 'IO'.
           88 WS-QTZ-TOT-INIZ                              VALUE 'IT'.

















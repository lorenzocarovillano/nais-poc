      *----------------------------------------------------------------*
      *   AREA INPUT VARIABILI
      *   PORTAFOGLIO VITA - PROCESSO GARANZIE OPZIONI
      *   LUNG.
      *----------------------------------------------------------------*
      * 03 WOPZ-AREA-OPZIONI.
            04 (SF)-ELE-MAX-OPZIONI             PIC 9(02).
            04 (SF)-OPZIONI                     OCCURS 10.
               05 (SF)-COD-TIPO-OPZIONE         PIC X(002).
               05 (SF)-DESC-TIPO-OPZIONE        PIC X(030).

               05 (SF)-NUM-GAR-POL-MAX-ELE      PIC 9(003).
               05 (SF)-GAR-POLIZZA              OCCURS 20.
                  10 (SF)-GAR-CODICE            PIC X(012).

               05 (SF)-NUM-GAR-OPZ-MAX-ELE      PIC 9(003).
               05 (SF)-GAR-OPZIONE              OCCURS 20.
                  10 (SF)-GAR-OPZ-CODICE        PIC X(012).
                  10 (SF)-GAR-OPZ-DECORRENZA    PIC X(008).
                  10 (SF)-GAR-OPZ-VERSIONE      PIC X(001).

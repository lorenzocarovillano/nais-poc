
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVP865
      *   ULTIMO AGG. 05 DIC 2014
      *------------------------------------------------------------

       VAL-DCLGEN-P86.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P86) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P86)
              TO P86-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-P86)
              TO P86-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-P86) NOT NUMERIC
              MOVE 0 TO P86-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-P86)
              TO P86-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-P86) NOT NUMERIC
              MOVE 0 TO P86-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-P86)
              TO P86-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-P86)
              TO P86-COD-COMP-ANIA
           MOVE (SF)-TP-LIQ(IX-TAB-P86)
              TO P86-TP-LIQ
           MOVE (SF)-TP-MOT-LIQ(IX-TAB-P86)
              TO P86-TP-MOT-LIQ
           IF (SF)-DS-RIGA(IX-TAB-P86) NOT NUMERIC
              MOVE 0 TO P86-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-P86)
              TO P86-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-P86)
              TO P86-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-P86) NOT NUMERIC
              MOVE 0 TO P86-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-P86)
              TO P86-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-P86) NOT NUMERIC
              MOVE 0 TO P86-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-P86)
              TO P86-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-P86) NOT NUMERIC
              MOVE 0 TO P86-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-P86)
              TO P86-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-P86)
              TO P86-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-P86)
              TO P86-DS-STATO-ELAB
           MOVE (SF)-DESC-LIB-MOT-LIQ(IX-TAB-P86)
              TO P86-DESC-LIB-MOT-LIQ.
       VAL-DCLGEN-P86-EX.
           EXIT.

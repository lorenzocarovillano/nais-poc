      *----------------------------------------------------------------*
      *    SALVO ELEMETI DA ELABORATE SU ELE MAX TABELLA APPOGGIO
      *----------------------------------------------------------------*
       GESTIONE-ELE-MAX-TEMP.
      *
           MOVE ZERO                      TO IDSV0303-ELE-MAX-TOT
                                             IDSV0303-ELE-MAX-APPO.

           IF IDSV0301-PARTIAL-RECURRENCE NOT GREATER
              IDSV0301-ACTUAL-RECURRENCE

              ADD IDSV0301-PARTIAL-RECURRENCE
                TO IDSV0303-ELE-MAX-APPO

              IF IDSV0303-ELE-MAX-APPO NOT GREATER
                 IDSV0301-ACTUAL-RECURRENCE

                 MOVE IDSV0301-PARTIAL-RECURRENCE
                   TO IDSV0303-ELE-MAX-TOT

              ELSE

                 COMPUTE IDSV0303-ELE-MAX-TOT =
                         IDSV0303-ELE-MAX-APPO -
                         IDSV0301-ACTUAL-RECURRENCE

              END-IF

           ELSE

              MOVE IDSV0301-ACTUAL-RECURRENCE
                TO IDSV0303-ELE-MAX-TOT

           END-IF.

      *
       GESTIONE-ELE-MAX-TEMP-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA EST_POLI_CPI_PR
      *   ALIAS P67
      *   ULTIMO AGG. 11 MAR 2014
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-EST-POLI-CPI-PR PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-IB-OGG PIC X(40).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-COD-PROD-ESTNO PIC X(20).
             07 (SF)-CPT-FIN PIC S9(12)V9(3) COMP-3.
             07 (SF)-CPT-FIN-NULL REDEFINES
                (SF)-CPT-FIN   PIC X(8).
             07 (SF)-NUM-TST-FIN PIC S9(5)     COMP-3.
             07 (SF)-NUM-TST-FIN-NULL REDEFINES
                (SF)-NUM-TST-FIN   PIC X(3).
             07 (SF)-TS-FINANZ PIC S9(5)V9(9) COMP-3.
             07 (SF)-TS-FINANZ-NULL REDEFINES
                (SF)-TS-FINANZ   PIC X(8).
             07 (SF)-DUR-MM-FINANZ PIC S9(5)     COMP-3.
             07 (SF)-DUR-MM-FINANZ-NULL REDEFINES
                (SF)-DUR-MM-FINANZ   PIC X(3).
             07 (SF)-DT-END-FINANZ   PIC S9(8) COMP-3.
             07 (SF)-DT-END-FINANZ-NULL REDEFINES
                (SF)-DT-END-FINANZ   PIC X(5).
             07 (SF)-AMM-1A-RAT PIC S9(12)V9(3) COMP-3.
             07 (SF)-AMM-1A-RAT-NULL REDEFINES
                (SF)-AMM-1A-RAT   PIC X(8).
             07 (SF)-VAL-RISC-BENE PIC S9(12)V9(3) COMP-3.
             07 (SF)-VAL-RISC-BENE-NULL REDEFINES
                (SF)-VAL-RISC-BENE   PIC X(8).
             07 (SF)-AMM-RAT-END PIC S9(12)V9(3) COMP-3.
             07 (SF)-AMM-RAT-END-NULL REDEFINES
                (SF)-AMM-RAT-END   PIC X(8).
             07 (SF)-TS-CRE-RAT-FINANZ PIC S9(5)V9(9) COMP-3.
             07 (SF)-TS-CRE-RAT-FINANZ-NULL REDEFINES
                (SF)-TS-CRE-RAT-FINANZ   PIC X(8).
             07 (SF)-IMP-FIN-REVOLVING PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-FIN-REVOLVING-NULL REDEFINES
                (SF)-IMP-FIN-REVOLVING   PIC X(8).
             07 (SF)-IMP-UTIL-C-REV PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-UTIL-C-REV-NULL REDEFINES
                (SF)-IMP-UTIL-C-REV   PIC X(8).
             07 (SF)-IMP-RAT-REVOLVING PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-RAT-REVOLVING-NULL REDEFINES
                (SF)-IMP-RAT-REVOLVING   PIC X(8).
             07 (SF)-DT-1O-UTLZ-C-REV   PIC S9(8) COMP-3.
             07 (SF)-DT-1O-UTLZ-C-REV-NULL REDEFINES
                (SF)-DT-1O-UTLZ-C-REV   PIC X(5).
             07 (SF)-IMP-ASSTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-ASSTO-NULL REDEFINES
                (SF)-IMP-ASSTO   PIC X(8).
             07 (SF)-PRE-VERS PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-VERS-NULL REDEFINES
                (SF)-PRE-VERS   PIC X(8).
             07 (SF)-DT-SCAD-COP   PIC S9(8) COMP-3.
             07 (SF)-DT-SCAD-COP-NULL REDEFINES
                (SF)-DT-SCAD-COP   PIC X(5).
             07 (SF)-GG-DEL-MM-SCAD-RAT PIC S9(2)     COMP-3.
             07 (SF)-GG-DEL-MM-SCAD-RAT-NULL REDEFINES
                (SF)-GG-DEL-MM-SCAD-RAT   PIC X(2).
             07 (SF)-FL-PRE-FIN PIC X(1).
             07 (SF)-DT-SCAD-1A-RAT   PIC S9(8) COMP-3.
             07 (SF)-DT-SCAD-1A-RAT-NULL REDEFINES
                (SF)-DT-SCAD-1A-RAT   PIC X(5).
             07 (SF)-DT-EROG-FINANZ   PIC S9(8) COMP-3.
             07 (SF)-DT-EROG-FINANZ-NULL REDEFINES
                (SF)-DT-EROG-FINANZ   PIC X(5).
             07 (SF)-DT-STIPULA-FINANZ   PIC S9(8) COMP-3.
             07 (SF)-DT-STIPULA-FINANZ-NULL REDEFINES
                (SF)-DT-STIPULA-FINANZ   PIC X(5).
             07 (SF)-MM-PREAMM PIC S9(5)     COMP-3.
             07 (SF)-MM-PREAMM-NULL REDEFINES
                (SF)-MM-PREAMM   PIC X(3).
             07 (SF)-IMP-DEB-RES PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-DEB-RES-NULL REDEFINES
                (SF)-IMP-DEB-RES   PIC X(8).
             07 (SF)-IMP-RAT-FINANZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-RAT-FINANZ-NULL REDEFINES
                (SF)-IMP-RAT-FINANZ   PIC X(8).
             07 (SF)-IMP-CANONE-ANTIC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CANONE-ANTIC-NULL REDEFINES
                (SF)-IMP-CANONE-ANTIC   PIC X(8).
             07 (SF)-PER-RAT-FINANZ PIC S9(5)     COMP-3.
             07 (SF)-PER-RAT-FINANZ-NULL REDEFINES
                (SF)-PER-RAT-FINANZ   PIC X(3).
             07 (SF)-TP-MOD-PAG-RAT PIC X(2).
             07 (SF)-TP-MOD-PAG-RAT-NULL REDEFINES
                (SF)-TP-MOD-PAG-RAT   PIC X(2).
             07 (SF)-TP-FINANZ-ER PIC X(2).
             07 (SF)-TP-FINANZ-ER-NULL REDEFINES
                (SF)-TP-FINANZ-ER   PIC X(2).
             07 (SF)-CAT-FINANZ-ER PIC X(20).
             07 (SF)-CAT-FINANZ-ER-NULL REDEFINES
                (SF)-CAT-FINANZ-ER   PIC X(20).
             07 (SF)-VAL-RISC-END-LEAS PIC S9(12)V9(3) COMP-3.
             07 (SF)-VAL-RISC-END-LEAS-NULL REDEFINES
                (SF)-VAL-RISC-END-LEAS   PIC X(8).
             07 (SF)-DT-EST-FINANZ   PIC S9(8) COMP-3.
             07 (SF)-DT-EST-FINANZ-NULL REDEFINES
                (SF)-DT-EST-FINANZ   PIC X(5).
             07 (SF)-DT-MAN-COP   PIC S9(8) COMP-3.
             07 (SF)-DT-MAN-COP-NULL REDEFINES
                (SF)-DT-MAN-COP   PIC X(5).
             07 (SF)-NUM-FINANZ PIC X(40).
             07 (SF)-NUM-FINANZ-NULL REDEFINES
                (SF)-NUM-FINANZ   PIC X(40).
             07 (SF)-TP-MOD-ACQS PIC X(4).
             07 (SF)-TP-MOD-ACQS-NULL REDEFINES
                (SF)-TP-MOD-ACQS   PIC X(4).


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVOCO3
      *   ULTIMO AGG. 09 LUG 2010
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-OCO.
           MOVE OCO-ID-OGG-COLLG
             TO (SF)-ID-PTF(IX-TAB-OCO)
           MOVE OCO-ID-OGG-COLLG
             TO (SF)-ID-OGG-COLLG(IX-TAB-OCO)
           MOVE OCO-ID-OGG-COINV
             TO (SF)-ID-OGG-COINV(IX-TAB-OCO)
           MOVE OCO-TP-OGG-COINV
             TO (SF)-TP-OGG-COINV(IX-TAB-OCO)
           MOVE OCO-ID-OGG-DER
             TO (SF)-ID-OGG-DER(IX-TAB-OCO)
           MOVE OCO-TP-OGG-DER
             TO (SF)-TP-OGG-DER(IX-TAB-OCO)
           IF OCO-IB-RIFTO-ESTNO-NULL = HIGH-VALUES
              MOVE OCO-IB-RIFTO-ESTNO-NULL
                TO (SF)-IB-RIFTO-ESTNO-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-IB-RIFTO-ESTNO
                TO (SF)-IB-RIFTO-ESTNO(IX-TAB-OCO)
           END-IF
           MOVE OCO-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-OCO)
           IF OCO-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE OCO-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-OCO)
           END-IF
           MOVE OCO-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-OCO)
           MOVE OCO-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-OCO)
           MOVE OCO-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-OCO)
           MOVE OCO-COD-PROD
             TO (SF)-COD-PROD(IX-TAB-OCO)
           IF OCO-DT-SCAD-NULL = HIGH-VALUES
              MOVE OCO-DT-SCAD-NULL
                TO (SF)-DT-SCAD-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-DT-SCAD
                TO (SF)-DT-SCAD(IX-TAB-OCO)
           END-IF
           MOVE OCO-TP-COLLGM
             TO (SF)-TP-COLLGM(IX-TAB-OCO)
           IF OCO-TP-TRASF-NULL = HIGH-VALUES
              MOVE OCO-TP-TRASF-NULL
                TO (SF)-TP-TRASF-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-TP-TRASF
                TO (SF)-TP-TRASF(IX-TAB-OCO)
           END-IF
           IF OCO-REC-PROV-NULL = HIGH-VALUES
              MOVE OCO-REC-PROV-NULL
                TO (SF)-REC-PROV-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-REC-PROV
                TO (SF)-REC-PROV(IX-TAB-OCO)
           END-IF
           IF OCO-DT-DECOR-NULL = HIGH-VALUES
              MOVE OCO-DT-DECOR-NULL
                TO (SF)-DT-DECOR-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-DT-DECOR
                TO (SF)-DT-DECOR(IX-TAB-OCO)
           END-IF
           IF OCO-DT-ULT-PRE-PAG-NULL = HIGH-VALUES
              MOVE OCO-DT-ULT-PRE-PAG-NULL
                TO (SF)-DT-ULT-PRE-PAG-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-DT-ULT-PRE-PAG
                TO (SF)-DT-ULT-PRE-PAG(IX-TAB-OCO)
           END-IF
           IF OCO-TOT-PRE-NULL = HIGH-VALUES
              MOVE OCO-TOT-PRE-NULL
                TO (SF)-TOT-PRE-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-TOT-PRE
                TO (SF)-TOT-PRE(IX-TAB-OCO)
           END-IF
           IF OCO-RIS-MAT-NULL = HIGH-VALUES
              MOVE OCO-RIS-MAT-NULL
                TO (SF)-RIS-MAT-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-RIS-MAT
                TO (SF)-RIS-MAT(IX-TAB-OCO)
           END-IF
           IF OCO-RIS-ZIL-NULL = HIGH-VALUES
              MOVE OCO-RIS-ZIL-NULL
                TO (SF)-RIS-ZIL-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-RIS-ZIL
                TO (SF)-RIS-ZIL(IX-TAB-OCO)
           END-IF
           IF OCO-IMP-TRASF-NULL = HIGH-VALUES
              MOVE OCO-IMP-TRASF-NULL
                TO (SF)-IMP-TRASF-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-IMP-TRASF
                TO (SF)-IMP-TRASF(IX-TAB-OCO)
           END-IF
           IF OCO-IMP-REINVST-NULL = HIGH-VALUES
              MOVE OCO-IMP-REINVST-NULL
                TO (SF)-IMP-REINVST-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-IMP-REINVST
                TO (SF)-IMP-REINVST(IX-TAB-OCO)
           END-IF
           IF OCO-PC-REINVST-RILIEVI-NULL = HIGH-VALUES
              MOVE OCO-PC-REINVST-RILIEVI-NULL
                TO (SF)-PC-REINVST-RILIEVI-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-PC-REINVST-RILIEVI
                TO (SF)-PC-REINVST-RILIEVI(IX-TAB-OCO)
           END-IF
           IF OCO-IB-2O-RIFTO-ESTNO-NULL = HIGH-VALUES
              MOVE OCO-IB-2O-RIFTO-ESTNO-NULL
                TO (SF)-IB-2O-RIFTO-ESTNO-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-IB-2O-RIFTO-ESTNO
                TO (SF)-IB-2O-RIFTO-ESTNO(IX-TAB-OCO)
           END-IF
           IF OCO-IND-LIQ-AGG-MAN-NULL = HIGH-VALUES
              MOVE OCO-IND-LIQ-AGG-MAN-NULL
                TO (SF)-IND-LIQ-AGG-MAN-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-IND-LIQ-AGG-MAN
                TO (SF)-IND-LIQ-AGG-MAN(IX-TAB-OCO)
           END-IF
           MOVE OCO-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-OCO)
           MOVE OCO-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-OCO)
           MOVE OCO-DS-VER
             TO (SF)-DS-VER(IX-TAB-OCO)
           MOVE OCO-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-OCO)
           MOVE OCO-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-OCO)
           MOVE OCO-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-OCO)
           MOVE OCO-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-OCO)
           IF OCO-IMP-COLLG-NULL = HIGH-VALUES
              MOVE OCO-IMP-COLLG-NULL
                TO (SF)-IMP-COLLG-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-IMP-COLLG
                TO (SF)-IMP-COLLG(IX-TAB-OCO)
           END-IF
           IF OCO-PRE-PER-TRASF-NULL = HIGH-VALUES
              MOVE OCO-PRE-PER-TRASF-NULL
                TO (SF)-PRE-PER-TRASF-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-PRE-PER-TRASF
                TO (SF)-PRE-PER-TRASF(IX-TAB-OCO)
           END-IF
           IF OCO-CAR-ACQ-NULL = HIGH-VALUES
              MOVE OCO-CAR-ACQ-NULL
                TO (SF)-CAR-ACQ-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-CAR-ACQ
                TO (SF)-CAR-ACQ(IX-TAB-OCO)
           END-IF
           IF OCO-PRE-1A-ANNUALITA-NULL = HIGH-VALUES
              MOVE OCO-PRE-1A-ANNUALITA-NULL
                TO (SF)-PRE-1A-ANNUALITA-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-PRE-1A-ANNUALITA
                TO (SF)-PRE-1A-ANNUALITA(IX-TAB-OCO)
           END-IF
           IF OCO-IMP-TRASFERITO-NULL = HIGH-VALUES
              MOVE OCO-IMP-TRASFERITO-NULL
                TO (SF)-IMP-TRASFERITO-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-IMP-TRASFERITO
                TO (SF)-IMP-TRASFERITO(IX-TAB-OCO)
           END-IF
           IF OCO-TP-MOD-ABBINAMENTO-NULL = HIGH-VALUES
              MOVE OCO-TP-MOD-ABBINAMENTO-NULL
                TO (SF)-TP-MOD-ABBINAMENTO-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-TP-MOD-ABBINAMENTO
                TO (SF)-TP-MOD-ABBINAMENTO(IX-TAB-OCO)
           END-IF
           IF OCO-PC-PRE-TRASFERITO-NULL = HIGH-VALUES
              MOVE OCO-PC-PRE-TRASFERITO-NULL
                TO (SF)-PC-PRE-TRASFERITO-NULL(IX-TAB-OCO)
           ELSE
              MOVE OCO-PC-PRE-TRASFERITO
                TO (SF)-PC-PRE-TRASFERITO(IX-TAB-OCO)
           END-IF.
       VALORIZZA-OUTPUT-OCO-EX.
           EXIT.

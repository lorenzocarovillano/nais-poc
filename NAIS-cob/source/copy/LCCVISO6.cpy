      *----------------------------------------------------------------*
      *    COPY      ..... LCCVISO6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO IMPOSTA SOSTITUTIVA
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-ISO.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE IMPST-SOST

      *--> NOME TABELLA FISICA DB
           MOVE 'IMPST-SOST'
             TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WISO-ST-INV(IX-TAB-ISO)
              AND WISO-ELE-ISO-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WISO-ST-ADD(IX-TAB-ISO)

                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK

                          MOVE S090-SEQ-TABELLA
                            TO WISO-ID-PTF(IX-TAB-ISO)
                               ISO-ID-IMPST-SOST

                          MOVE WMOV-ID-PTF
                            TO ISO-ID-MOVI-CRZ

      *-->             ASSOCIAZIONE OGGETTO
                         PERFORM ASSOCIA-OGGETTO THRU ASSOCIA-OGGETTO-EX

      *-->             TIPO OPERAZIONE DISPATCHER
                         SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

                       END-IF

      *-->        MODIFICA
                  WHEN WISO-ST-MOD(IX-TAB-ISO)

                    MOVE WISO-ID-PTF(IX-TAB-ISO)   TO ISO-ID-IMPST-SOST
ALEX  *             MOVE WISO-ID-OGG(IX-TAB-ISO)   TO ISO-ID-OGG

      *-->       TIPO OPERAZIONE DISPATCHER
                    SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WISO-ST-DEL(IX-TAB-ISO)
                     MOVE WISO-ID-PTF(IX-TAB-ISO)   TO ISO-ID-IMPST-SOST
ALEX  *              MOVE WISO-ID-OGG(IX-TAB-ISO)   TO ISO-ID-OGG

      *-->       TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->       TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

ALEX          MOVE WISO-ID-OGG(IX-TAB-ISO)          TO ISO-ID-OGG

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN IMPOSTA SOSTITUTIVA
                 PERFORM VAL-DCLGEN-ISO
                    THRU VAL-DCLGEN-ISO-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-ISO
                    THRU VALORIZZA-AREA-DSH-ISO-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-ISO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-ISO.

      *--> DCLGEN TABELLA
           MOVE IMPST-SOST             TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT             TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-ISO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   EFFETTUA IL LEGAME TRA L'OGGETTO E TIPO OGGETTO
      *----------------------------------------------------------------*
       ASSOCIA-OGGETTO.

      *--->   Ricerca dell'ID-TRCH-DI-GAR sulla Tabella
      *--->   Padre Tranche Di Garanzia
ALEX        MOVE WISO-TP-OGG(IX-TAB-ISO)  TO WS-TP-OGG


            EVALUATE TRUE
              WHEN WS-TP-OGG = 'AD'
                 MOVE WADE-ID-PTF TO WISO-ID-OGG(IX-TAB-ISO)
              WHEN WS-TP-OGG = 'TG'

                MOVE WISO-ID-OGG(IX-TAB-ISO)
                  TO WS-ID-TRCH-DI-GAR

                PERFORM RICERCA-ID-TRANCHE
                   THRU RICERCA-ID-TRANCHE-EX

ALEX            IF NON-TROVATO
ALEX               SET IDSV0001-ESITO-KO TO TRUE
ALEX               MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
ALEX               MOVE 'ASSOCIA-OGGETTO'
ALEX                                  TO IEAI9901-LABEL-ERR
ALEX               MOVE '001114'      TO IEAI9901-COD-ERRORE
ALEX               MOVE 'NON TROVATA CORRISPONDENZA TRA TRANCE E PTF'
ALEX                                  TO IEAI9901-PARAMETRI-ERR
ALEX               PERFORM S0300-RICERCA-GRAVITA-ERRORE
ALEX                       THRU EX-S0300
ALEX            ELSE
ALEX               MOVE WS-ID-PTF             TO WISO-ID-OGG(IX-TAB-ISO)
ALEX            END-IF
              WHEN OTHER
                SET IDSV0001-ESITO-KO TO TRUE
                MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                MOVE 'ASSOCIA-OGGETTO'
                                   TO IEAI9901-LABEL-ERR
                MOVE '001114'      TO IEAI9901-COD-ERRORE
                MOVE 'TIPO OGGETTO SU TRANCHE NON VALIDO'
                                   TO IEAI9901-PARAMETRI-ERR
                PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
            END-EVALUATE.

       ASSOCIA-OGGETTO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    RICERCA PTF
      *----------------------------------------------------------------*
       RICERCA-ID-TRANCHE.

           SET NON-TROVATO TO TRUE.

           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
                     UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX OR TROVATO

                IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) = WS-ID-TRCH-DI-GAR

                   MOVE WTGA-ID-PTF(IX-TAB-TGA) TO WS-ID-PTF

                   SET TROVATO TO TRUE

                END-IF

           END-PERFORM.

       RICERCA-ID-TRANCHE-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVISO5.

           EXEC SQL DECLARE CLAU_TXT TABLE
           (
             ID_CLAU_TXT         DECIMAL(9, 0) NOT NULL,
             ID_OGG              DECIMAL(9, 0) NOT NULL,
             TP_OGG              CHAR(2) NOT NULL,
             ID_MOVI_CRZ         DECIMAL(9, 0) NOT NULL,
             ID_MOVI_CHIU        DECIMAL(9, 0),
             DT_INI_EFF          DATE NOT NULL,
             DT_END_EFF          DATE NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             TP_CLAU             CHAR(3),
             COD_CLAU            CHAR(20),
             DESC_BREVE          CHAR(30),
             DESC_LNG            VARCHAR(250),
             DS_RIGA             DECIMAL(10, 0) NOT NULL WITH DEFAULT,
             DS_OPER_SQL         CHAR(1) NOT NULL WITH DEFAULT,
             DS_VER              DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             DS_TS_INI_CPTZ      DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_TS_END_CPTZ      DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_UTENTE           CHAR(20) NOT NULL WITH DEFAULT,
             DS_STATO_ELAB       CHAR(1) NOT NULL WITH DEFAULT
          ) END-EXEC.

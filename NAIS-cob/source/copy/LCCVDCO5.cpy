
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVDCO5
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-DCO.
           IF (SF)-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL
              TO DCO-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU
              TO DCO-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF NOT NUMERIC
              MOVE 0 TO DCO-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF
              TO DCO-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF NOT NUMERIC
              MOVE 0 TO DCO-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF
              TO DCO-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA
              TO DCO-COD-COMP-ANIA
           IF (SF)-IMP-ARROT-PRE-NULL = HIGH-VALUES
              MOVE (SF)-IMP-ARROT-PRE-NULL
              TO DCO-IMP-ARROT-PRE-NULL
           ELSE
              MOVE (SF)-IMP-ARROT-PRE
              TO DCO-IMP-ARROT-PRE
           END-IF
           IF (SF)-PC-SCON-NULL = HIGH-VALUES
              MOVE (SF)-PC-SCON-NULL
              TO DCO-PC-SCON-NULL
           ELSE
              MOVE (SF)-PC-SCON
              TO DCO-PC-SCON
           END-IF
           IF (SF)-FL-ADES-SING-NULL = HIGH-VALUES
              MOVE (SF)-FL-ADES-SING-NULL
              TO DCO-FL-ADES-SING-NULL
           ELSE
              MOVE (SF)-FL-ADES-SING
              TO DCO-FL-ADES-SING
           END-IF
           IF (SF)-TP-IMP-NULL = HIGH-VALUES
              MOVE (SF)-TP-IMP-NULL
              TO DCO-TP-IMP-NULL
           ELSE
              MOVE (SF)-TP-IMP
              TO DCO-TP-IMP
           END-IF
           IF (SF)-FL-RICL-PRE-DA-CPT-NULL = HIGH-VALUES
              MOVE (SF)-FL-RICL-PRE-DA-CPT-NULL
              TO DCO-FL-RICL-PRE-DA-CPT-NULL
           ELSE
              MOVE (SF)-FL-RICL-PRE-DA-CPT
              TO DCO-FL-RICL-PRE-DA-CPT
           END-IF
           IF (SF)-TP-ADES-NULL = HIGH-VALUES
              MOVE (SF)-TP-ADES-NULL
              TO DCO-TP-ADES-NULL
           ELSE
              MOVE (SF)-TP-ADES
              TO DCO-TP-ADES
           END-IF
           IF (SF)-DT-ULT-RINN-TAC-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-RINN-TAC-NULL
              TO DCO-DT-ULT-RINN-TAC-NULL
           ELSE
             IF (SF)-DT-ULT-RINN-TAC = ZERO
                MOVE HIGH-VALUES
                TO DCO-DT-ULT-RINN-TAC-NULL
             ELSE
              MOVE (SF)-DT-ULT-RINN-TAC
              TO DCO-DT-ULT-RINN-TAC
             END-IF
           END-IF
           IF (SF)-IMP-SCON-NULL = HIGH-VALUES
              MOVE (SF)-IMP-SCON-NULL
              TO DCO-IMP-SCON-NULL
           ELSE
              MOVE (SF)-IMP-SCON
              TO DCO-IMP-SCON
           END-IF
           IF (SF)-FRAZ-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-FRAZ-DFLT-NULL
              TO DCO-FRAZ-DFLT-NULL
           ELSE
              MOVE (SF)-FRAZ-DFLT
              TO DCO-FRAZ-DFLT
           END-IF
           IF (SF)-ETA-SCAD-MASC-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-ETA-SCAD-MASC-DFLT-NULL
              TO DCO-ETA-SCAD-MASC-DFLT-NULL
           ELSE
              MOVE (SF)-ETA-SCAD-MASC-DFLT
              TO DCO-ETA-SCAD-MASC-DFLT
           END-IF
           IF (SF)-ETA-SCAD-FEMM-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-ETA-SCAD-FEMM-DFLT-NULL
              TO DCO-ETA-SCAD-FEMM-DFLT-NULL
           ELSE
              MOVE (SF)-ETA-SCAD-FEMM-DFLT
              TO DCO-ETA-SCAD-FEMM-DFLT
           END-IF
           IF (SF)-TP-DFLT-DUR-NULL = HIGH-VALUES
              MOVE (SF)-TP-DFLT-DUR-NULL
              TO DCO-TP-DFLT-DUR-NULL
           ELSE
              MOVE (SF)-TP-DFLT-DUR
              TO DCO-TP-DFLT-DUR
           END-IF
           IF (SF)-DUR-AA-ADES-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-DUR-AA-ADES-DFLT-NULL
              TO DCO-DUR-AA-ADES-DFLT-NULL
           ELSE
              MOVE (SF)-DUR-AA-ADES-DFLT
              TO DCO-DUR-AA-ADES-DFLT
           END-IF
           IF (SF)-DUR-MM-ADES-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-DUR-MM-ADES-DFLT-NULL
              TO DCO-DUR-MM-ADES-DFLT-NULL
           ELSE
              MOVE (SF)-DUR-MM-ADES-DFLT
              TO DCO-DUR-MM-ADES-DFLT
           END-IF
           IF (SF)-DUR-GG-ADES-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-DUR-GG-ADES-DFLT-NULL
              TO DCO-DUR-GG-ADES-DFLT-NULL
           ELSE
              MOVE (SF)-DUR-GG-ADES-DFLT
              TO DCO-DUR-GG-ADES-DFLT
           END-IF
           IF (SF)-DT-SCAD-ADES-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-DT-SCAD-ADES-DFLT-NULL
              TO DCO-DT-SCAD-ADES-DFLT-NULL
           ELSE
             IF (SF)-DT-SCAD-ADES-DFLT = ZERO
                MOVE HIGH-VALUES
                TO DCO-DT-SCAD-ADES-DFLT-NULL
             ELSE
              MOVE (SF)-DT-SCAD-ADES-DFLT
              TO DCO-DT-SCAD-ADES-DFLT
             END-IF
           END-IF
           IF (SF)-COD-FND-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-COD-FND-DFLT-NULL
              TO DCO-COD-FND-DFLT-NULL
           ELSE
              MOVE (SF)-COD-FND-DFLT
              TO DCO-COD-FND-DFLT
           END-IF
           IF (SF)-TP-DUR-NULL = HIGH-VALUES
              MOVE (SF)-TP-DUR-NULL
              TO DCO-TP-DUR-NULL
           ELSE
              MOVE (SF)-TP-DUR
              TO DCO-TP-DUR
           END-IF
           IF (SF)-TP-CALC-DUR-NULL = HIGH-VALUES
              MOVE (SF)-TP-CALC-DUR-NULL
              TO DCO-TP-CALC-DUR-NULL
           ELSE
              MOVE (SF)-TP-CALC-DUR
              TO DCO-TP-CALC-DUR
           END-IF
           IF (SF)-FL-NO-ADERENTI-NULL = HIGH-VALUES
              MOVE (SF)-FL-NO-ADERENTI-NULL
              TO DCO-FL-NO-ADERENTI-NULL
           ELSE
              MOVE (SF)-FL-NO-ADERENTI
              TO DCO-FL-NO-ADERENTI
           END-IF
           IF (SF)-FL-DISTINTA-CNBTVA-NULL = HIGH-VALUES
              MOVE (SF)-FL-DISTINTA-CNBTVA-NULL
              TO DCO-FL-DISTINTA-CNBTVA-NULL
           ELSE
              MOVE (SF)-FL-DISTINTA-CNBTVA
              TO DCO-FL-DISTINTA-CNBTVA
           END-IF
           IF (SF)-FL-CNBT-AUTES-NULL = HIGH-VALUES
              MOVE (SF)-FL-CNBT-AUTES-NULL
              TO DCO-FL-CNBT-AUTES-NULL
           ELSE
              MOVE (SF)-FL-CNBT-AUTES
              TO DCO-FL-CNBT-AUTES
           END-IF
           IF (SF)-FL-QTZ-POST-EMIS-NULL = HIGH-VALUES
              MOVE (SF)-FL-QTZ-POST-EMIS-NULL
              TO DCO-FL-QTZ-POST-EMIS-NULL
           ELSE
              MOVE (SF)-FL-QTZ-POST-EMIS
              TO DCO-FL-QTZ-POST-EMIS
           END-IF
           IF (SF)-FL-COMNZ-FND-IS-NULL = HIGH-VALUES
              MOVE (SF)-FL-COMNZ-FND-IS-NULL
              TO DCO-FL-COMNZ-FND-IS-NULL
           ELSE
              MOVE (SF)-FL-COMNZ-FND-IS
              TO DCO-FL-COMNZ-FND-IS
           END-IF
           IF (SF)-DS-RIGA NOT NUMERIC
              MOVE 0 TO DCO-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA
              TO DCO-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO DCO-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO DCO-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO DCO-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ NOT NUMERIC
              MOVE 0 TO DCO-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ
              TO DCO-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ NOT NUMERIC
              MOVE 0 TO DCO-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ
              TO DCO-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO DCO-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO DCO-DS-STATO-ELAB.
       VAL-DCLGEN-DCO-EX.
           EXIT.

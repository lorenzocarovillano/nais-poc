
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVD034
      *   ULTIMO AGG. 16 MAR 2009
      *------------------------------------------------------------

       INIZIA-TOT-D03.

           PERFORM INIZIA-ZEROES-D03 THRU INIZIA-ZEROES-D03-EX

           PERFORM INIZIA-SPACES-D03 THRU INIZIA-SPACES-D03-EX

           PERFORM INIZIA-NULL-D03 THRU INIZIA-NULL-D03-EX.

       INIZIA-TOT-D03-EX.
           EXIT.

       INIZIA-NULL-D03.
           MOVE HIGH-VALUES TO (SF)-PROGR-INIZIALE-NULL
           MOVE HIGH-VALUES TO (SF)-PROGR-FINALE-NULL.
       INIZIA-NULL-D03-EX.
           EXIT.

       INIZIA-ZEROES-D03.
           MOVE 0 TO (SF)-COD-COMPAGNIA-ANIA
           MOVE 0 TO (SF)-ULT-PROGR.
       INIZIA-ZEROES-D03-EX.
           EXIT.

       INIZIA-SPACES-D03.
           MOVE SPACES TO (SF)-FORMA-ASSICURATIVA
           MOVE SPACES TO (SF)-COD-OGGETTO
           MOVE SPACES TO (SF)-KEY-BUSINESS.
       INIZIA-SPACES-D03-EX.
           EXIT.

       01  W-AREA-PER-CHIAMATE-WSMQ.
           02 WSMQ-QM-NAME                 PIC X(48) .
           02 WSMQ-HCONN                   PIC S9(9) BINARY.
           02 WSMQ-HCONN-JCALL             PIC S9(9) BINARY.
           02 WSMQ-Q-HANDLE-JCALL          PIC S9(9) BINARY.
           02 WSMQ-OPTIONS-JCALL           PIC S9(9) BINARY.
           02 WSMQ-COMPLETION-CODE-JCALL   PIC S9(9) BINARY.
           02 WSMQ-OPEN-CODE-JCALL         PIC S9(9) BINARY.
           02 WSMQ-CON-REASON-JCALL        PIC S9(9) BINARY.
           02 WSMQ-REASON-JCALL            PIC S9(9) BINARY.
      *
           02 WSMQ-HCONN-JRET              PIC S9(9) BINARY.
           02 WSMQ-Q-HANDLE-JRET           PIC S9(9) BINARY.
           02 WSMQ-OPTIONS-JRET            PIC S9(9) BINARY.
           02 WSMQ-COMPLETION-CODE-JRET    PIC S9(9) BINARY.
           02 WSMQ-OPEN-CODE-JRET          PIC S9(9) BINARY.
           02 WSMQ-CON-REASON-JRET         PIC S9(9) BINARY.
           02 WSMQ-REASON-JRET             PIC S9(9) BINARY.
      *
           02 WSMQ-BUFFER-LENGTH-JCALL     PIC S9(9) BINARY.
           02 WSMQ-BUFFER-LENGTH-JRET      PIC S9(9) BINARY.
           02 WSMQ-DATA-LENGTH             PIC S9(9) BINARY.
           02 WSMQ-TIPO-CHIAMATA.
              03  WSMQ-TIPO-OPERAZIONE     PIC X(12).
              03  WSMQ-NOME-CODA           PIC X(48).
              03  WSMQ-TP-UTILIZZO-API     PIC X(03).
              03  WSMQ-MESSAGGIO-INTERNO   PIC X(50).

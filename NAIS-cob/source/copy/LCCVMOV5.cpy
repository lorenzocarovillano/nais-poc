
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVMOV5
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-MOV.
           MOVE (SF)-COD-COMP-ANIA
              TO MOV-COD-COMP-ANIA
           IF (SF)-IB-OGG-NULL = HIGH-VALUES
              MOVE (SF)-IB-OGG-NULL
              TO MOV-IB-OGG-NULL
           ELSE
              MOVE (SF)-IB-OGG
              TO MOV-IB-OGG
           END-IF
           IF (SF)-IB-MOVI-NULL = HIGH-VALUES
              MOVE (SF)-IB-MOVI-NULL
              TO MOV-IB-MOVI-NULL
           ELSE
              MOVE (SF)-IB-MOVI
              TO MOV-IB-MOVI
           END-IF
           IF (SF)-TP-OGG-NULL = HIGH-VALUES
              MOVE (SF)-TP-OGG-NULL
              TO MOV-TP-OGG-NULL
           ELSE
              MOVE (SF)-TP-OGG
              TO MOV-TP-OGG
           END-IF
           IF (SF)-ID-RICH-NULL = HIGH-VALUES
              MOVE (SF)-ID-RICH-NULL
              TO MOV-ID-RICH-NULL
           ELSE
              MOVE (SF)-ID-RICH
              TO MOV-ID-RICH
           END-IF
           IF (SF)-TP-MOVI-NULL = HIGH-VALUES
              MOVE (SF)-TP-MOVI-NULL
              TO MOV-TP-MOVI-NULL
           ELSE
              MOVE (SF)-TP-MOVI
              TO MOV-TP-MOVI
           END-IF
           MOVE (SF)-DT-EFF
              TO MOV-DT-EFF
           IF (SF)-ID-MOVI-ANN-NULL = HIGH-VALUES
              MOVE (SF)-ID-MOVI-ANN-NULL
              TO MOV-ID-MOVI-ANN-NULL
           ELSE
              MOVE (SF)-ID-MOVI-ANN
              TO MOV-ID-MOVI-ANN
           END-IF
           IF (SF)-ID-MOVI-COLLG-NULL = HIGH-VALUES
              MOVE (SF)-ID-MOVI-COLLG-NULL
              TO MOV-ID-MOVI-COLLG-NULL
           ELSE
              MOVE (SF)-ID-MOVI-COLLG
              TO MOV-ID-MOVI-COLLG
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO MOV-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO MOV-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO MOV-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO MOV-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO MOV-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO MOV-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO MOV-DS-STATO-ELAB.
       VAL-DCLGEN-MOV-EX.
           EXIT.

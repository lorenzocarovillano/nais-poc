
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVODE4
      *   ULTIMO AGG. 02 SET 2008
      *------------------------------------------------------------

       INIZIA-TOT-ODE.

           PERFORM INIZIA-ZEROES-ODE THRU INIZIA-ZEROES-ODE-EX

           PERFORM INIZIA-SPACES-ODE THRU INIZIA-SPACES-ODE-EX

           PERFORM INIZIA-NULL-ODE THRU INIZIA-NULL-ODE-EX.

       INIZIA-TOT-ODE-EX.
           EXIT.

       INIZIA-NULL-ODE.
           MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL
           MOVE HIGH-VALUES TO (SF)-IB-OGG-NULL
           MOVE HIGH-VALUES TO (SF)-COD-LIV-AUT-APPRT-NULL.
       INIZIA-NULL-ODE-EX.
           EXIT.

       INIZIA-ZEROES-ODE.
           MOVE 0 TO (SF)-ID-OGG-DEROGA
           MOVE 0 TO (SF)-ID-MOVI-CRZ
           MOVE 0 TO (SF)-DT-INI-EFF
           MOVE 0 TO (SF)-DT-END-EFF
           MOVE 0 TO (SF)-COD-COMP-ANIA
           MOVE 0 TO (SF)-ID-OGG
           MOVE 0 TO (SF)-COD-GR-AUT-APPRT
           MOVE 0 TO (SF)-COD-GR-AUT-SUP
           MOVE 0 TO (SF)-COD-LIV-AUT-SUP
           MOVE 0 TO (SF)-DS-RIGA
           MOVE 0 TO (SF)-DS-VER
           MOVE 0 TO (SF)-DS-TS-INI-CPTZ
           MOVE 0 TO (SF)-DS-TS-END-CPTZ.
       INIZIA-ZEROES-ODE-EX.
           EXIT.

       INIZIA-SPACES-ODE.
           MOVE SPACES TO (SF)-TP-OGG
           MOVE SPACES TO (SF)-TP-DEROGA
           MOVE SPACES TO (SF)-DS-OPER-SQL
           MOVE SPACES TO (SF)-DS-UTENTE
           MOVE SPACES TO (SF)-DS-STATO-ELAB.
       INIZIA-SPACES-ODE-EX.
           EXIT.

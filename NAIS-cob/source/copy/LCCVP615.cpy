
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVP615
      *   ULTIMO AGG. 24 OTT 2019
      *------------------------------------------------------------

       VAL-DCLGEN-P61.
           MOVE (SF)-ID-D-CRIST
              TO P61-ID-D-CRIST
           MOVE (SF)-ID-POLI
              TO P61-ID-POLI
           MOVE (SF)-COD-COMP-ANIA
              TO P61-COD-COMP-ANIA
MIGCOL*    MOVE (SF)-ID-MOVI-CRZ
MIGCO *       TO P61-ID-MOVI-CRZ
           IF (SF)-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL
              TO P61-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU
              TO P61-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF NOT NUMERIC
              MOVE 0 TO P61-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF
              TO P61-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF NOT NUMERIC
              MOVE 0 TO P61-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF
              TO P61-DT-END-EFF
           END-IF
           MOVE (SF)-COD-PROD
              TO P61-COD-PROD
           MOVE (SF)-DT-DECOR
              TO P61-DT-DECOR
           IF (SF)-DS-RIGA NOT NUMERIC
              MOVE 0 TO P61-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA
              TO P61-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO P61-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO P61-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO P61-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ NOT NUMERIC
              MOVE 0 TO P61-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ
              TO P61-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ NOT NUMERIC
              MOVE 0 TO P61-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ
              TO P61-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO P61-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO P61-DS-STATO-ELAB
           IF (SF)-RIS-MAT-31122011-NULL = HIGH-VALUES
              MOVE (SF)-RIS-MAT-31122011-NULL
              TO P61-RIS-MAT-31122011-NULL
           ELSE
              MOVE (SF)-RIS-MAT-31122011
              TO P61-RIS-MAT-31122011
           END-IF
           IF (SF)-PRE-V-31122011-NULL = HIGH-VALUES
              MOVE (SF)-PRE-V-31122011-NULL
              TO P61-PRE-V-31122011-NULL
           ELSE
              MOVE (SF)-PRE-V-31122011
              TO P61-PRE-V-31122011
           END-IF
           IF (SF)-PRE-RSH-V-31122011-NULL = HIGH-VALUES
              MOVE (SF)-PRE-RSH-V-31122011-NULL
              TO P61-PRE-RSH-V-31122011-NULL
           ELSE
              MOVE (SF)-PRE-RSH-V-31122011
              TO P61-PRE-RSH-V-31122011
           END-IF
           IF (SF)-CPT-RIVTO-31122011-NULL = HIGH-VALUES
              MOVE (SF)-CPT-RIVTO-31122011-NULL
              TO P61-CPT-RIVTO-31122011-NULL
           ELSE
              MOVE (SF)-CPT-RIVTO-31122011
              TO P61-CPT-RIVTO-31122011
           END-IF
           IF (SF)-IMPB-VIS-31122011-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-31122011-NULL
              TO P61-IMPB-VIS-31122011-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-31122011
              TO P61-IMPB-VIS-31122011
           END-IF
           IF (SF)-IMPB-IS-31122011-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-31122011-NULL
              TO P61-IMPB-IS-31122011-NULL
           ELSE
              MOVE (SF)-IMPB-IS-31122011
              TO P61-IMPB-IS-31122011
           END-IF
           IF (SF)-IMPB-VIS-RP-P2011-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-RP-P2011-NULL
              TO P61-IMPB-VIS-RP-P2011-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-RP-P2011
              TO P61-IMPB-VIS-RP-P2011
           END-IF
           IF (SF)-IMPB-IS-RP-P2011-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-RP-P2011-NULL
              TO P61-IMPB-IS-RP-P2011-NULL
           ELSE
              MOVE (SF)-IMPB-IS-RP-P2011
              TO P61-IMPB-IS-RP-P2011
           END-IF
           IF (SF)-PRE-V-30062014-NULL = HIGH-VALUES
              MOVE (SF)-PRE-V-30062014-NULL
              TO P61-PRE-V-30062014-NULL
           ELSE
              MOVE (SF)-PRE-V-30062014
              TO P61-PRE-V-30062014
           END-IF
           IF (SF)-PRE-RSH-V-30062014-NULL = HIGH-VALUES
              MOVE (SF)-PRE-RSH-V-30062014-NULL
              TO P61-PRE-RSH-V-30062014-NULL
           ELSE
              MOVE (SF)-PRE-RSH-V-30062014
              TO P61-PRE-RSH-V-30062014
           END-IF
           IF (SF)-CPT-INI-30062014-NULL = HIGH-VALUES
              MOVE (SF)-CPT-INI-30062014-NULL
              TO P61-CPT-INI-30062014-NULL
           ELSE
              MOVE (SF)-CPT-INI-30062014
              TO P61-CPT-INI-30062014
           END-IF
           IF (SF)-IMPB-VIS-30062014-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-30062014-NULL
              TO P61-IMPB-VIS-30062014-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-30062014
              TO P61-IMPB-VIS-30062014
           END-IF
           IF (SF)-IMPB-IS-30062014-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-30062014-NULL
              TO P61-IMPB-IS-30062014-NULL
           ELSE
              MOVE (SF)-IMPB-IS-30062014
              TO P61-IMPB-IS-30062014
           END-IF
           IF (SF)-IMPB-VIS-RP-P62014-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-RP-P62014-NULL
              TO P61-IMPB-VIS-RP-P62014-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-RP-P62014
              TO P61-IMPB-VIS-RP-P62014
           END-IF
           IF (SF)-IMPB-IS-RP-P62014-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-RP-P62014-NULL
              TO P61-IMPB-IS-RP-P62014-NULL
           ELSE
              MOVE (SF)-IMPB-IS-RP-P62014
              TO P61-IMPB-IS-RP-P62014
           END-IF
           IF (SF)-RIS-MAT-30062014-NULL = HIGH-VALUES
              MOVE (SF)-RIS-MAT-30062014-NULL
              TO P61-RIS-MAT-30062014-NULL
           ELSE
              MOVE (SF)-RIS-MAT-30062014
              TO P61-RIS-MAT-30062014
           END-IF
           IF (SF)-ID-ADES-NULL = HIGH-VALUES
              MOVE (SF)-ID-ADES-NULL
              TO P61-ID-ADES-NULL
           ELSE
              MOVE (SF)-ID-ADES
              TO P61-ID-ADES
           END-IF
           IF (SF)-MONT-LRD-END2000-NULL = HIGH-VALUES
              MOVE (SF)-MONT-LRD-END2000-NULL
              TO P61-MONT-LRD-END2000-NULL
           ELSE
              MOVE (SF)-MONT-LRD-END2000
              TO P61-MONT-LRD-END2000
           END-IF
           IF (SF)-PRE-LRD-END2000-NULL = HIGH-VALUES
              MOVE (SF)-PRE-LRD-END2000-NULL
              TO P61-PRE-LRD-END2000-NULL
           ELSE
              MOVE (SF)-PRE-LRD-END2000
              TO P61-PRE-LRD-END2000
           END-IF
           IF (SF)-RENDTO-LRD-END2000-NULL = HIGH-VALUES
              MOVE (SF)-RENDTO-LRD-END2000-NULL
              TO P61-RENDTO-LRD-END2000-NULL
           ELSE
              MOVE (SF)-RENDTO-LRD-END2000
              TO P61-RENDTO-LRD-END2000
           END-IF
           IF (SF)-MONT-LRD-END2006-NULL = HIGH-VALUES
              MOVE (SF)-MONT-LRD-END2006-NULL
              TO P61-MONT-LRD-END2006-NULL
           ELSE
              MOVE (SF)-MONT-LRD-END2006
              TO P61-MONT-LRD-END2006
           END-IF
           IF (SF)-PRE-LRD-END2006-NULL = HIGH-VALUES
              MOVE (SF)-PRE-LRD-END2006-NULL
              TO P61-PRE-LRD-END2006-NULL
           ELSE
              MOVE (SF)-PRE-LRD-END2006
              TO P61-PRE-LRD-END2006
           END-IF
           IF (SF)-RENDTO-LRD-END2006-NULL = HIGH-VALUES
              MOVE (SF)-RENDTO-LRD-END2006-NULL
              TO P61-RENDTO-LRD-END2006-NULL
           ELSE
              MOVE (SF)-RENDTO-LRD-END2006
              TO P61-RENDTO-LRD-END2006
           END-IF
           IF (SF)-MONT-LRD-DAL2007-NULL = HIGH-VALUES
              MOVE (SF)-MONT-LRD-DAL2007-NULL
              TO P61-MONT-LRD-DAL2007-NULL
           ELSE
              MOVE (SF)-MONT-LRD-DAL2007
              TO P61-MONT-LRD-DAL2007
           END-IF
           IF (SF)-PRE-LRD-DAL2007-NULL = HIGH-VALUES
              MOVE (SF)-PRE-LRD-DAL2007-NULL
              TO P61-PRE-LRD-DAL2007-NULL
           ELSE
              MOVE (SF)-PRE-LRD-DAL2007
              TO P61-PRE-LRD-DAL2007
           END-IF
           IF (SF)-RENDTO-LRD-DAL2007-NULL = HIGH-VALUES
              MOVE (SF)-RENDTO-LRD-DAL2007-NULL
              TO P61-RENDTO-LRD-DAL2007-NULL
           ELSE
              MOVE (SF)-RENDTO-LRD-DAL2007
              TO P61-RENDTO-LRD-DAL2007
           END-IF
           IF (SF)-ID-TRCH-DI-GAR-NULL = HIGH-VALUES
              MOVE (SF)-ID-TRCH-DI-GAR-NULL
              TO P61-ID-TRCH-DI-GAR-NULL
           ELSE
              MOVE (SF)-ID-TRCH-DI-GAR
              TO P61-ID-TRCH-DI-GAR
           END-IF.
       VAL-DCLGEN-P61-EX.
           EXIT.

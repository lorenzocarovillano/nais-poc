      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA MOVI_BATCH_SOSP
      *   ALIAS MBS
      *   ULTIMO AGG. 02 SET 2008
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-MOVI-BATCH-SOSP PIC S9(9)     COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-ID-OGG PIC S9(9)     COMP-3.
             07 (SF)-TP-OGG PIC X(2).
             07 (SF)-ID-MOVI PIC S9(9)     COMP-3.
             07 (SF)-TP-MOVI PIC S9(5)     COMP-3.
             07 (SF)-DT-EFF   PIC S9(8) COMP-3.
             07 (SF)-ID-OGG-BLOCCO PIC S9(9)     COMP-3.
             07 (SF)-TP-FRM-ASSVA PIC X(2).
             07 (SF)-ID-BATCH PIC S9(9)     COMP-3.
             07 (SF)-ID-BATCH-NULL REDEFINES
                (SF)-ID-BATCH   PIC X(5).
             07 (SF)-ID-JOB PIC S9(9)     COMP-3.
             07 (SF)-ID-JOB-NULL REDEFINES
                (SF)-ID-JOB   PIC X(5).
             07 (SF)-STEP-ELAB PIC X(1).
             07 (SF)-FL-MOVI-SOSP PIC X(1).
             07 (SF)-D-INPUT-MOVI-SOSP PIC X(300).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVRIF5
      *   ULTIMO AGG. 25 NOV 2019
      *------------------------------------------------------------

       VAL-DCLGEN-RIF.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RIF) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RIF)
              TO RIF-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-RIF)
              TO RIF-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-RIF) NOT NUMERIC
              MOVE 0 TO RIF-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-RIF)
              TO RIF-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-RIF) NOT NUMERIC
              MOVE 0 TO RIF-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-RIF)
              TO RIF-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-RIF)
              TO RIF-COD-COMP-ANIA
           IF (SF)-COD-FND-NULL(IX-TAB-RIF) = HIGH-VALUES
              MOVE (SF)-COD-FND-NULL(IX-TAB-RIF)
              TO RIF-COD-FND-NULL
           ELSE
              MOVE (SF)-COD-FND(IX-TAB-RIF)
              TO RIF-COD-FND
           END-IF
           IF (SF)-NUM-QUO-NULL(IX-TAB-RIF) = HIGH-VALUES
              MOVE (SF)-NUM-QUO-NULL(IX-TAB-RIF)
              TO RIF-NUM-QUO-NULL
           ELSE
              MOVE (SF)-NUM-QUO(IX-TAB-RIF)
              TO RIF-NUM-QUO
           END-IF
           IF (SF)-PC-NULL(IX-TAB-RIF) = HIGH-VALUES
              MOVE (SF)-PC-NULL(IX-TAB-RIF)
              TO RIF-PC-NULL
           ELSE
              MOVE (SF)-PC(IX-TAB-RIF)
              TO RIF-PC
           END-IF
           IF (SF)-IMP-MOVTO-NULL(IX-TAB-RIF) = HIGH-VALUES
              MOVE (SF)-IMP-MOVTO-NULL(IX-TAB-RIF)
              TO RIF-IMP-MOVTO-NULL
           ELSE
              MOVE (SF)-IMP-MOVTO(IX-TAB-RIF)
              TO RIF-IMP-MOVTO
           END-IF
           IF (SF)-DT-INVST-NULL(IX-TAB-RIF) = HIGH-VALUES
              MOVE (SF)-DT-INVST-NULL(IX-TAB-RIF)
              TO RIF-DT-INVST-NULL
           ELSE
             IF (SF)-DT-INVST(IX-TAB-RIF) = ZERO
                MOVE HIGH-VALUES
                TO RIF-DT-INVST-NULL
             ELSE
              MOVE (SF)-DT-INVST(IX-TAB-RIF)
              TO RIF-DT-INVST
             END-IF
           END-IF
           IF (SF)-COD-TARI-NULL(IX-TAB-RIF) = HIGH-VALUES
              MOVE (SF)-COD-TARI-NULL(IX-TAB-RIF)
              TO RIF-COD-TARI-NULL
           ELSE
              MOVE (SF)-COD-TARI(IX-TAB-RIF)
              TO RIF-COD-TARI
           END-IF
           IF (SF)-TP-STAT-NULL(IX-TAB-RIF) = HIGH-VALUES
              MOVE (SF)-TP-STAT-NULL(IX-TAB-RIF)
              TO RIF-TP-STAT-NULL
           ELSE
              MOVE (SF)-TP-STAT(IX-TAB-RIF)
              TO RIF-TP-STAT
           END-IF
           IF (SF)-TP-MOD-INVST-NULL(IX-TAB-RIF) = HIGH-VALUES
              MOVE (SF)-TP-MOD-INVST-NULL(IX-TAB-RIF)
              TO RIF-TP-MOD-INVST-NULL
           ELSE
              MOVE (SF)-TP-MOD-INVST(IX-TAB-RIF)
              TO RIF-TP-MOD-INVST
           END-IF
           MOVE (SF)-COD-DIV(IX-TAB-RIF)
              TO RIF-COD-DIV
           IF (SF)-DT-CAMBIO-VLT-NULL(IX-TAB-RIF) = HIGH-VALUES
              MOVE (SF)-DT-CAMBIO-VLT-NULL(IX-TAB-RIF)
              TO RIF-DT-CAMBIO-VLT-NULL
           ELSE
             IF (SF)-DT-CAMBIO-VLT(IX-TAB-RIF) = ZERO
                MOVE HIGH-VALUES
                TO RIF-DT-CAMBIO-VLT-NULL
             ELSE
              MOVE (SF)-DT-CAMBIO-VLT(IX-TAB-RIF)
              TO RIF-DT-CAMBIO-VLT
             END-IF
           END-IF
           MOVE (SF)-TP-FND(IX-TAB-RIF)
              TO RIF-TP-FND
           IF (SF)-DS-RIGA(IX-TAB-RIF) NOT NUMERIC
              MOVE 0 TO RIF-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-RIF)
              TO RIF-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-RIF)
              TO RIF-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-RIF) NOT NUMERIC
              MOVE 0 TO RIF-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-RIF)
              TO RIF-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-RIF) NOT NUMERIC
              MOVE 0 TO RIF-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-RIF)
              TO RIF-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-RIF) NOT NUMERIC
              MOVE 0 TO RIF-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-RIF)
              TO RIF-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-RIF)
              TO RIF-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-RIF)
              TO RIF-DS-STATO-ELAB
           IF (SF)-DT-INVST-CALC-NULL(IX-TAB-RIF) = HIGH-VALUES
              MOVE (SF)-DT-INVST-CALC-NULL(IX-TAB-RIF)
              TO RIF-DT-INVST-CALC-NULL
           ELSE
             IF (SF)-DT-INVST-CALC(IX-TAB-RIF) = ZERO
                MOVE HIGH-VALUES
                TO RIF-DT-INVST-CALC-NULL
             ELSE
              MOVE (SF)-DT-INVST-CALC(IX-TAB-RIF)
              TO RIF-DT-INVST-CALC
             END-IF
           END-IF
           IF (SF)-FL-CALC-INVTO-NULL(IX-TAB-RIF) = HIGH-VALUES
              MOVE (SF)-FL-CALC-INVTO-NULL(IX-TAB-RIF)
              TO RIF-FL-CALC-INVTO-NULL
           ELSE
              MOVE (SF)-FL-CALC-INVTO(IX-TAB-RIF)
              TO RIF-FL-CALC-INVTO
           END-IF
           IF (SF)-IMP-GAP-EVENT-NULL(IX-TAB-RIF) = HIGH-VALUES
              MOVE (SF)-IMP-GAP-EVENT-NULL(IX-TAB-RIF)
              TO RIF-IMP-GAP-EVENT-NULL
           ELSE
              MOVE (SF)-IMP-GAP-EVENT(IX-TAB-RIF)
              TO RIF-IMP-GAP-EVENT
           END-IF
           IF (SF)-FL-SWM-BP2S-NULL(IX-TAB-RIF) = HIGH-VALUES
              MOVE (SF)-FL-SWM-BP2S-NULL(IX-TAB-RIF)
              TO RIF-FL-SWM-BP2S-NULL
           ELSE
              MOVE (SF)-FL-SWM-BP2S(IX-TAB-RIF)
              TO RIF-FL-SWM-BP2S
           END-IF.
       VAL-DCLGEN-RIF-EX.
           EXIT.

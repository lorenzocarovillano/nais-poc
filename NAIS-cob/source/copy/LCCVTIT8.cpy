          03 (SF)-LEN-TOT                       PIC S9(4) COMP-5.
          03 (SF)-REC-FISSO.
             05 (SF)-ID-TIT-CONT PIC S9(9)V     COMP-3.
             05 (SF)-ID-OGG PIC S9(9)V     COMP-3.
             05 (SF)-TP-OGG PIC X(2).
             05 (SF)-IB-RICH-IND PIC X.
             05 (SF)-IB-RICH PIC X(40).
             05 (SF)-IB-RICH-NULL REDEFINES
                (SF)-IB-RICH   PIC X(40).
             05 (SF)-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
             05 (SF)-ID-MOVI-CHIU-IND PIC X.
             05 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             05 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             05 (SF)-DT-INI-EFF   PIC X(10).
             05 (SF)-DT-END-EFF   PIC X(10).
             05 (SF)-COD-COMP-ANIA PIC S9(5)V     COMP-3.
             05 (SF)-TP-TIT PIC X(2).
             05 (SF)-PROG-TIT-IND PIC X.
             05 (SF)-PROG-TIT PIC S9(5)     COMP-3.
             05 (SF)-PROG-TIT-NULL REDEFINES
                (SF)-PROG-TIT   PIC X(3).
             05 (SF)-TP-PRE-TIT PIC X(2).
             05 (SF)-TP-STAT-TIT PIC X(2).
             05 (SF)-DT-INI-COP-IND PIC X.
             05 (SF)-DT-INI-COP   PIC X(10).
             05 (SF)-DT-INI-COP-NULL REDEFINES
                (SF)-DT-INI-COP   PIC X(10).
             05 (SF)-DT-END-COP-IND PIC X.
             05 (SF)-DT-END-COP   PIC X(10).
             05 (SF)-DT-END-COP-NULL REDEFINES
                (SF)-DT-END-COP   PIC X(10).
             05 (SF)-IMP-PAG-IND PIC X.
             05 (SF)-IMP-PAG PIC S9(12)V9(3) COMP-3.
             05 (SF)-IMP-PAG-NULL REDEFINES
                (SF)-IMP-PAG   PIC X(8).
             05 (SF)-FL-SOLL-IND PIC X.
             05 (SF)-FL-SOLL PIC X(1).
             05 (SF)-FL-SOLL-NULL REDEFINES
                (SF)-FL-SOLL   PIC X(1).
             05 (SF)-FRAZ-IND PIC X.
             05 (SF)-FRAZ PIC S9(5)V     COMP-3.
             05 (SF)-FRAZ-NULL REDEFINES
                (SF)-FRAZ   PIC X(3).
             05 (SF)-DT-APPLZ-MORA-IND PIC X.
             05 (SF)-DT-APPLZ-MORA   PIC X(10).
             05 (SF)-DT-APPLZ-MORA-NULL REDEFINES
                (SF)-DT-APPLZ-MORA   PIC X(10).
             05 (SF)-FL-MORA-IND PIC X.
             05 (SF)-FL-MORA PIC X(1).
             05 (SF)-FL-MORA-NULL REDEFINES
                (SF)-FL-MORA   PIC X(1).
             05 (SF)-ID-RAPP-RETE-IND PIC X.
             05 (SF)-ID-RAPP-RETE PIC S9(9)V     COMP-3.
             05 (SF)-ID-RAPP-RETE-NULL REDEFINES
                (SF)-ID-RAPP-RETE   PIC X(5).
             05 (SF)-ID-RAPP-ANA-IND PIC X.
             05 (SF)-ID-RAPP-ANA PIC S9(9)V     COMP-3.
             05 (SF)-ID-RAPP-ANA-NULL REDEFINES
                (SF)-ID-RAPP-ANA   PIC X(5).
             05 (SF)-COD-DVS-IND PIC X.
             05 (SF)-COD-DVS PIC X(20).
             05 (SF)-COD-DVS-NULL REDEFINES
                (SF)-COD-DVS   PIC X(20).
             05 (SF)-DT-EMIS-TIT-IND  PIC X.
             05 (SF)-DT-EMIS-TIT   PIC X(10).
             05 (SF)-DT-EMIS-TIT-NULL REDEFINES
                (SF)-DT-EMIS-TIT   PIC X(10).
             05 (SF)-DT-ESI-TIT-IND PIC X.
             05 (SF)-DT-ESI-TIT   PIC X(10).
             05 (SF)-DT-ESI-TIT-NULL REDEFINES
                (SF)-DT-ESI-TIT   PIC X(10).
             05 (SF)-TOT-PRE-NET-IND PIC X.
             05 (SF)-TOT-PRE-NET PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-PRE-NET-NULL REDEFINES
                (SF)-TOT-PRE-NET   PIC X(8).
             05 (SF)-TOT-INTR-FRAZ-IND PIC X .
             05 (SF)-TOT-INTR-FRAZ PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-INTR-FRAZ-NULL REDEFINES
                (SF)-TOT-INTR-FRAZ   PIC X(8).
             05 (SF)-TOT-INTR-MORA-IND PIC X.
             05 (SF)-TOT-INTR-MORA PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-INTR-MORA-NULL REDEFINES
                (SF)-TOT-INTR-MORA   PIC X(8).
             05 (SF)-TOT-INTR-PREST-IND PIC X.
             05 (SF)-TOT-INTR-PREST PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-INTR-PREST-NULL REDEFINES
                (SF)-TOT-INTR-PREST   PIC X(8).
             05 (SF)-TOT-INTR-RETDT-IND PIC X.
             05 (SF)-TOT-INTR-RETDT PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-INTR-RETDT-NULL REDEFINES
                (SF)-TOT-INTR-RETDT   PIC X(8).
             05 (SF)-TOT-INTR-RIAT-IND PIC X.
             05 (SF)-TOT-INTR-RIAT PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-INTR-RIAT-NULL REDEFINES
                (SF)-TOT-INTR-RIAT   PIC X(8).
             05 (SF)-TOT-DIR-IND PIC X.
             05 (SF)-TOT-DIR PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-DIR-NULL REDEFINES
                (SF)-TOT-DIR   PIC X(8).
             05 (SF)-TOT-SPE-MED-IND PIC X.
             05 (SF)-TOT-SPE-MED PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-SPE-MED-NULL REDEFINES
                (SF)-TOT-SPE-MED   PIC X(8).
             05 (SF)-TOT-TAX-IND PIC X.
             05 (SF)-TOT-TAX PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-TAX-NULL REDEFINES
                (SF)-TOT-TAX   PIC X(8).
             05 (SF)-TOT-SOPR-SAN-IND PIC X.
             05 (SF)-TOT-SOPR-SAN PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-SOPR-SAN-NULL REDEFINES
                (SF)-TOT-SOPR-SAN   PIC X(8).
             05 (SF)-TOT-SOPR-TEC-IND PIC X.
             05 (SF)-TOT-SOPR-TEC PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-SOPR-TEC-NULL REDEFINES
                (SF)-TOT-SOPR-TEC   PIC X(8).
             05 (SF)-TOT-SOPR-SPO-IND PIC X.
             05 (SF)-TOT-SOPR-SPO PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-SOPR-SPO-NULL REDEFINES
                (SF)-TOT-SOPR-SPO   PIC X(8).
             05 (SF)-TOT-SOPR-PROF-IND PIC X.
             05 (SF)-TOT-SOPR-PROF PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-SOPR-PROF-NULL REDEFINES
                (SF)-TOT-SOPR-PROF   PIC X(8).
             05 (SF)-TOT-SOPR-ALT-IND PIC X.
             05 (SF)-TOT-SOPR-ALT PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-SOPR-ALT-NULL REDEFINES
                (SF)-TOT-SOPR-ALT   PIC X(8).
             05 (SF)-TOT-PRE-TOT-IND PIC X.
             05 (SF)-TOT-PRE-TOT PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-PRE-TOT-NULL REDEFINES
                (SF)-TOT-PRE-TOT   PIC X(8).
             05 (SF)-TOT-PRE-PP-IAS-IND PIC X.
             05 (SF)-TOT-PRE-PP-IAS PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-PRE-PP-IAS-NULL REDEFINES
                (SF)-TOT-PRE-PP-IAS   PIC X(8).
             05 (SF)-TOT-CAR-ACQ-IND PIC X.
             05 (SF)-TOT-CAR-ACQ PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-CAR-ACQ-NULL REDEFINES
                (SF)-TOT-CAR-ACQ   PIC X(8).
             05 (SF)-TOT-CAR-GEST-IND PIC X.
             05 (SF)-TOT-CAR-GEST PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-CAR-GEST-NULL REDEFINES
                (SF)-TOT-CAR-GEST   PIC X(8).
             05 (SF)-TOT-CAR-INC-IND PIC X.
             05 (SF)-TOT-CAR-INC PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-CAR-INC-NULL REDEFINES
                (SF)-TOT-CAR-INC   PIC X(8).
             05 (SF)-TOT-PRE-SOLO-RSH-IND PIC X.
             05 (SF)-TOT-PRE-SOLO-RSH PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-PRE-SOLO-RSH-NULL REDEFINES
                (SF)-TOT-PRE-SOLO-RSH   PIC X(8).
             05 (SF)-TOT-PROV-ACQ-1AA-IND PIC X.
             05 (SF)-TOT-PROV-ACQ-1AA PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-PROV-ACQ-1AA-NULL REDEFINES
                (SF)-TOT-PROV-ACQ-1AA   PIC X(8).
             05 (SF)-TOT-PROV-ACQ-2AA-IND PIC X.
             05 (SF)-TOT-PROV-ACQ-2AA PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-PROV-ACQ-2AA-NULL REDEFINES
                (SF)-TOT-PROV-ACQ-2AA   PIC X(8).
             05 (SF)-TOT-PROV-RICOR-IND PIC X.
             05 (SF)-TOT-PROV-RICOR PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-PROV-RICOR-NULL REDEFINES
                (SF)-TOT-PROV-RICOR   PIC X(8).
             05 (SF)-TOT-PROV-INC-IND PIC X.
             05 (SF)-TOT-PROV-INC PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-PROV-INC-NULL REDEFINES
                (SF)-TOT-PROV-INC   PIC X(8).
             05 (SF)-TOT-PROV-DA-REC-IND PIC X.
             05 (SF)-TOT-PROV-DA-REC PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-PROV-DA-REC-NULL REDEFINES
                (SF)-TOT-PROV-DA-REC   PIC X(8).
             05 (SF)-IMP-AZ-IND PIC X.
             05 (SF)-IMP-AZ PIC S9(12)V9(3) COMP-3.
             05 (SF)-IMP-AZ-NULL REDEFINES
                (SF)-IMP-AZ   PIC X(8).
             05 (SF)-IMP-ADER-IND PIC X.
             05 (SF)-IMP-ADER PIC S9(12)V9(3) COMP-3.
             05 (SF)-IMP-ADER-NULL REDEFINES
                (SF)-IMP-ADER   PIC X(8).
             05 (SF)-IMP-TFR-IND PIC X.
             05 (SF)-IMP-TFR PIC S9(12)V9(3) COMP-3.
             05 (SF)-IMP-TFR-NULL REDEFINES
                (SF)-IMP-TFR   PIC X(8).
             05 (SF)-IMP-VOLO-IND PIC X.
             05 (SF)-IMP-VOLO PIC S9(12)V9(3) COMP-3.
             05 (SF)-IMP-VOLO-NULL REDEFINES
                (SF)-IMP-VOLO   PIC X(8).
             05 (SF)-TOT-MANFEE-ANTIC-IND PIC X.
             05 (SF)-TOT-MANFEE-ANTIC PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-MANFEE-ANTIC-NULL REDEFINES
                (SF)-TOT-MANFEE-ANTIC   PIC X(8).
             05 (SF)-TOT-MANFEE-RICOR-IND PIC X.
             05 (SF)-TOT-MANFEE-RICOR PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-MANFEE-RICOR-NULL REDEFINES
                (SF)-TOT-MANFEE-RICOR   PIC X(8).
             05 (SF)-TOT-MANFEE-REC-IND  PIC X.
             05 (SF)-TOT-MANFEE-REC PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-MANFEE-REC-NULL REDEFINES
                (SF)-TOT-MANFEE-REC   PIC X(8).
             05 (SF)-TP-MEZ-PAG-ADD-IND PIC X.
             05 (SF)-TP-MEZ-PAG-ADD PIC X(2).
             05 (SF)-TP-MEZ-PAG-ADD-NULL REDEFINES
                (SF)-TP-MEZ-PAG-ADD   PIC X(2).
             05 (SF)-ESTR-CNT-CORR-ADD-IND PIC X.
             05 (SF)-ESTR-CNT-CORR-ADD PIC X(20).
             05 (SF)-ESTR-CNT-CORR-ADD-NULL REDEFINES
                (SF)-ESTR-CNT-CORR-ADD   PIC X(20).
             05 (SF)-DT-VLT-IND PIC X.
             05 (SF)-DT-VLT   PIC X(10).
             05 (SF)-DT-VLT-NULL REDEFINES
                (SF)-DT-VLT   PIC X(10).
             05 (SF)-FL-FORZ-DT-VLT-IND PIC X.
             05 (SF)-FL-FORZ-DT-VLT PIC X(1).
             05 (SF)-FL-FORZ-DT-VLT-NULL REDEFINES
                (SF)-FL-FORZ-DT-VLT   PIC X(1).
             05 (SF)-DT-CAMBIO-VLT-IND PIC X.
             05 (SF)-DT-CAMBIO-VLT   PIC X(10).
             05 (SF)-DT-CAMBIO-VLT-NULL REDEFINES
                (SF)-DT-CAMBIO-VLT   PIC X(10).
             05 (SF)-TOT-SPE-AGE-IND PIC X.
             05 (SF)-TOT-SPE-AGE PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-SPE-AGE-NULL REDEFINES
                (SF)-TOT-SPE-AGE   PIC X(8).
             05 (SF)-TOT-CAR-IAS-IND PIC X.
             05 (SF)-TOT-CAR-IAS PIC S9(12)V9(3) COMP-3.
             05 (SF)-TOT-CAR-IAS-NULL REDEFINES
                (SF)-TOT-CAR-IAS   PIC X(8).
             05 (SF)-NUM-RAT-ACCORPATE-IND PIC X.
             05 (SF)-NUM-RAT-ACCORPATE PIC S9(5)V     COMP-3.
             05 (SF)-NUM-RAT-ACCORPATE-NULL REDEFINES
                (SF)-NUM-RAT-ACCORPATE   PIC X(3).
             05 (SF)-DS-RIGA PIC S9(10)V     COMP-3.
             05 (SF)-DS-OPER-SQL PIC X(1).
             05 (SF)-DS-VER PIC S9(9)V     COMP-3.
             05 (SF)-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
             05 (SF)-DS-TS-END-CPTZ PIC S9(18)V    COMP-3.
             05 (SF)-DS-UTENTE PIC X(20).
             05 (SF)-DS-STATO-ELAB PIC X(1).
             05 (SF)-FL-TIT-DA-REINVST-IND PIC X.
             05 (SF)-FL-TIT-DA-REINVST PIC X(1).
             05 (SF)-FL-TIT-DA-REINVST-NULL REDEFINES
                (SF)-FL-TIT-DA-REINVST   PIC X(1).
             05 (SF)-DT-RICH-ADD-RID-IND PIC X.
             05 (SF)-DT-RICH-ADD-RID   PIC X(10).
             05 (SF)-DT-RICH-ADD-RID-NULL REDEFINES
                (SF)-DT-RICH-ADD-RID   PIC X(10).
             05 (SF)-TP-ESI-RID-IND PIC X.
             05 (SF)-TP-ESI-RID PIC X(2).
             05 (SF)-TP-ESI-RID-NULL REDEFINES
                (SF)-TP-ESI-RID   PIC X(2).
             05 (SF)-COD-IBAN-IND PIC X.
             05 (SF)-COD-IBAN PIC X(34).
             05 (SF)-COD-IBAN-NULL REDEFINES
                (SF)-COD-IBAN   PIC X(34).
             05 (SF)-IMP-TRASFE-IND PIC X.
             05 (SF)-IMP-TRASFE PIC S9(12)V9(3) COMP-3.
             05 (SF)-IMP-TRASFE-NULL REDEFINES
                (SF)-IMP-TRASFE   PIC X(8).
             05 (SF)-IMP-TFR-STRC-IND PIC X.
             05 (SF)-IMP-TFR-STRC PIC S9(12)V9(3) COMP-3.
             05 (SF)-IMP-TFR-STRC-NULL REDEFINES
                (SF)-IMP-TFR-STRC   PIC X(8).
             05 (SF)-DT-CERT-FISC-IND PIC X.
             05 (SF)-DT-CERT-FISC   PIC X(10).
             05 (SF)-DT-CERT-FISC-NULL REDEFINES
                (SF)-DT-CERT-FISC   PIC X(10).
             05 (SF)-TP-CAUS-STOR-IND PIC X.
             05 (SF)-TP-CAUS-STOR PIC S9(5)V     COMP-3.
             05 (SF)-TP-CAUS-STOR-NULL REDEFINES
                (SF)-TP-CAUS-STOR   PIC X(3).
             05 (SF)-TP-CAUS-DISP-STOR-IND PIC X.
             05 (SF)-TP-CAUS-DISP-STOR PIC X(10).
             05 (SF)-TP-CAUS-DISP-STOR-NULL REDEFINES
                (SF)-TP-CAUS-DISP-STOR   PIC X(10).
             05 (SF)-TP-TIT-MIGRAZ-IND PIC X.
             05 (SF)-TP-TIT-MIGRAZ PIC X(2).
             05 (SF)-TP-TIT-MIGRAZ-NULL REDEFINES
                (SF)-TP-TIT-MIGRAZ   PIC X(2).

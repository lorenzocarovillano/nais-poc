
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVTDR3
      *   ULTIMO AGG. 05 DIC 2014
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-TDR.
           MOVE TDR-ID-TIT-RAT
             TO (SF)-ID-PTF(IX-TAB-TDR)
           MOVE TDR-ID-TIT-RAT
             TO (SF)-ID-TIT-RAT(IX-TAB-TDR)
           MOVE TDR-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-TDR)
           MOVE TDR-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-TDR)
           MOVE TDR-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-TDR)
           IF TDR-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE TDR-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-TDR)
           END-IF
           MOVE TDR-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-TDR)
           MOVE TDR-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-TDR)
           MOVE TDR-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-TDR)
           MOVE TDR-TP-TIT
             TO (SF)-TP-TIT(IX-TAB-TDR)
           IF TDR-PROG-TIT-NULL = HIGH-VALUES
              MOVE TDR-PROG-TIT-NULL
                TO (SF)-PROG-TIT-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-PROG-TIT
                TO (SF)-PROG-TIT(IX-TAB-TDR)
           END-IF
           MOVE TDR-TP-PRE-TIT
             TO (SF)-TP-PRE-TIT(IX-TAB-TDR)
           MOVE TDR-TP-STAT-TIT
             TO (SF)-TP-STAT-TIT(IX-TAB-TDR)
           IF TDR-DT-INI-COP-NULL = HIGH-VALUES
              MOVE TDR-DT-INI-COP-NULL
                TO (SF)-DT-INI-COP-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-DT-INI-COP
                TO (SF)-DT-INI-COP(IX-TAB-TDR)
           END-IF
           IF TDR-DT-END-COP-NULL = HIGH-VALUES
              MOVE TDR-DT-END-COP-NULL
                TO (SF)-DT-END-COP-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-DT-END-COP
                TO (SF)-DT-END-COP(IX-TAB-TDR)
           END-IF
           IF TDR-IMP-PAG-NULL = HIGH-VALUES
              MOVE TDR-IMP-PAG-NULL
                TO (SF)-IMP-PAG-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-IMP-PAG
                TO (SF)-IMP-PAG(IX-TAB-TDR)
           END-IF
           IF TDR-FL-SOLL-NULL = HIGH-VALUES
              MOVE TDR-FL-SOLL-NULL
                TO (SF)-FL-SOLL-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-FL-SOLL
                TO (SF)-FL-SOLL(IX-TAB-TDR)
           END-IF
           IF TDR-FRAZ-NULL = HIGH-VALUES
              MOVE TDR-FRAZ-NULL
                TO (SF)-FRAZ-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-FRAZ
                TO (SF)-FRAZ(IX-TAB-TDR)
           END-IF
           IF TDR-DT-APPLZ-MORA-NULL = HIGH-VALUES
              MOVE TDR-DT-APPLZ-MORA-NULL
                TO (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-DT-APPLZ-MORA
                TO (SF)-DT-APPLZ-MORA(IX-TAB-TDR)
           END-IF
           IF TDR-FL-MORA-NULL = HIGH-VALUES
              MOVE TDR-FL-MORA-NULL
                TO (SF)-FL-MORA-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-FL-MORA
                TO (SF)-FL-MORA(IX-TAB-TDR)
           END-IF
           IF TDR-ID-RAPP-RETE-NULL = HIGH-VALUES
              MOVE TDR-ID-RAPP-RETE-NULL
                TO (SF)-ID-RAPP-RETE-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-ID-RAPP-RETE
                TO (SF)-ID-RAPP-RETE(IX-TAB-TDR)
           END-IF
           IF TDR-ID-RAPP-ANA-NULL = HIGH-VALUES
              MOVE TDR-ID-RAPP-ANA-NULL
                TO (SF)-ID-RAPP-ANA-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-ID-RAPP-ANA
                TO (SF)-ID-RAPP-ANA(IX-TAB-TDR)
           END-IF
           IF TDR-COD-DVS-NULL = HIGH-VALUES
              MOVE TDR-COD-DVS-NULL
                TO (SF)-COD-DVS-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-COD-DVS
                TO (SF)-COD-DVS(IX-TAB-TDR)
           END-IF
           MOVE TDR-DT-EMIS-TIT
             TO (SF)-DT-EMIS-TIT(IX-TAB-TDR)
           IF TDR-DT-ESI-TIT-NULL = HIGH-VALUES
              MOVE TDR-DT-ESI-TIT-NULL
                TO (SF)-DT-ESI-TIT-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-DT-ESI-TIT
                TO (SF)-DT-ESI-TIT(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-PRE-NET-NULL = HIGH-VALUES
              MOVE TDR-TOT-PRE-NET-NULL
                TO (SF)-TOT-PRE-NET-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-PRE-NET
                TO (SF)-TOT-PRE-NET(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-INTR-FRAZ-NULL = HIGH-VALUES
              MOVE TDR-TOT-INTR-FRAZ-NULL
                TO (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-INTR-FRAZ
                TO (SF)-TOT-INTR-FRAZ(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-INTR-MORA-NULL = HIGH-VALUES
              MOVE TDR-TOT-INTR-MORA-NULL
                TO (SF)-TOT-INTR-MORA-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-INTR-MORA
                TO (SF)-TOT-INTR-MORA(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-INTR-PREST-NULL = HIGH-VALUES
              MOVE TDR-TOT-INTR-PREST-NULL
                TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-INTR-PREST
                TO (SF)-TOT-INTR-PREST(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-INTR-RETDT-NULL = HIGH-VALUES
              MOVE TDR-TOT-INTR-RETDT-NULL
                TO (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-INTR-RETDT
                TO (SF)-TOT-INTR-RETDT(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-INTR-RIAT-NULL = HIGH-VALUES
              MOVE TDR-TOT-INTR-RIAT-NULL
                TO (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-INTR-RIAT
                TO (SF)-TOT-INTR-RIAT(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-DIR-NULL = HIGH-VALUES
              MOVE TDR-TOT-DIR-NULL
                TO (SF)-TOT-DIR-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-DIR
                TO (SF)-TOT-DIR(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-SPE-MED-NULL = HIGH-VALUES
              MOVE TDR-TOT-SPE-MED-NULL
                TO (SF)-TOT-SPE-MED-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-SPE-MED
                TO (SF)-TOT-SPE-MED(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-SPE-AGE-NULL = HIGH-VALUES
              MOVE TDR-TOT-SPE-AGE-NULL
                TO (SF)-TOT-SPE-AGE-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-SPE-AGE
                TO (SF)-TOT-SPE-AGE(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-TAX-NULL = HIGH-VALUES
              MOVE TDR-TOT-TAX-NULL
                TO (SF)-TOT-TAX-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-TAX
                TO (SF)-TOT-TAX(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-SOPR-SAN-NULL = HIGH-VALUES
              MOVE TDR-TOT-SOPR-SAN-NULL
                TO (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-SOPR-SAN
                TO (SF)-TOT-SOPR-SAN(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-SOPR-TEC-NULL = HIGH-VALUES
              MOVE TDR-TOT-SOPR-TEC-NULL
                TO (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-SOPR-TEC
                TO (SF)-TOT-SOPR-TEC(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-SOPR-SPO-NULL = HIGH-VALUES
              MOVE TDR-TOT-SOPR-SPO-NULL
                TO (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-SOPR-SPO
                TO (SF)-TOT-SOPR-SPO(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-SOPR-PROF-NULL = HIGH-VALUES
              MOVE TDR-TOT-SOPR-PROF-NULL
                TO (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-SOPR-PROF
                TO (SF)-TOT-SOPR-PROF(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-SOPR-ALT-NULL = HIGH-VALUES
              MOVE TDR-TOT-SOPR-ALT-NULL
                TO (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-SOPR-ALT
                TO (SF)-TOT-SOPR-ALT(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-PRE-TOT-NULL = HIGH-VALUES
              MOVE TDR-TOT-PRE-TOT-NULL
                TO (SF)-TOT-PRE-TOT-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-PRE-TOT
                TO (SF)-TOT-PRE-TOT(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-PRE-PP-IAS-NULL = HIGH-VALUES
              MOVE TDR-TOT-PRE-PP-IAS-NULL
                TO (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-PRE-PP-IAS
                TO (SF)-TOT-PRE-PP-IAS(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-CAR-IAS-NULL = HIGH-VALUES
              MOVE TDR-TOT-CAR-IAS-NULL
                TO (SF)-TOT-CAR-IAS-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-CAR-IAS
                TO (SF)-TOT-CAR-IAS(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-PRE-SOLO-RSH-NULL = HIGH-VALUES
              MOVE TDR-TOT-PRE-SOLO-RSH-NULL
                TO (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-PRE-SOLO-RSH
                TO (SF)-TOT-PRE-SOLO-RSH(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-PROV-ACQ-1AA-NULL = HIGH-VALUES
              MOVE TDR-TOT-PROV-ACQ-1AA-NULL
                TO (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-PROV-ACQ-1AA
                TO (SF)-TOT-PROV-ACQ-1AA(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-PROV-ACQ-2AA-NULL = HIGH-VALUES
              MOVE TDR-TOT-PROV-ACQ-2AA-NULL
                TO (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-PROV-ACQ-2AA
                TO (SF)-TOT-PROV-ACQ-2AA(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-PROV-RICOR-NULL = HIGH-VALUES
              MOVE TDR-TOT-PROV-RICOR-NULL
                TO (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-PROV-RICOR
                TO (SF)-TOT-PROV-RICOR(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-PROV-INC-NULL = HIGH-VALUES
              MOVE TDR-TOT-PROV-INC-NULL
                TO (SF)-TOT-PROV-INC-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-PROV-INC
                TO (SF)-TOT-PROV-INC(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-PROV-DA-REC-NULL = HIGH-VALUES
              MOVE TDR-TOT-PROV-DA-REC-NULL
                TO (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-PROV-DA-REC
                TO (SF)-TOT-PROV-DA-REC(IX-TAB-TDR)
           END-IF
           IF TDR-IMP-AZ-NULL = HIGH-VALUES
              MOVE TDR-IMP-AZ-NULL
                TO (SF)-IMP-AZ-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-IMP-AZ
                TO (SF)-IMP-AZ(IX-TAB-TDR)
           END-IF
           IF TDR-IMP-ADER-NULL = HIGH-VALUES
              MOVE TDR-IMP-ADER-NULL
                TO (SF)-IMP-ADER-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-IMP-ADER
                TO (SF)-IMP-ADER(IX-TAB-TDR)
           END-IF
           IF TDR-IMP-TFR-NULL = HIGH-VALUES
              MOVE TDR-IMP-TFR-NULL
                TO (SF)-IMP-TFR-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-IMP-TFR
                TO (SF)-IMP-TFR(IX-TAB-TDR)
           END-IF
           IF TDR-IMP-VOLO-NULL = HIGH-VALUES
              MOVE TDR-IMP-VOLO-NULL
                TO (SF)-IMP-VOLO-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-IMP-VOLO
                TO (SF)-IMP-VOLO(IX-TAB-TDR)
           END-IF
           IF TDR-FL-VLDT-TIT-NULL = HIGH-VALUES
              MOVE TDR-FL-VLDT-TIT-NULL
                TO (SF)-FL-VLDT-TIT-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-FL-VLDT-TIT
                TO (SF)-FL-VLDT-TIT(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-CAR-ACQ-NULL = HIGH-VALUES
              MOVE TDR-TOT-CAR-ACQ-NULL
                TO (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-CAR-ACQ
                TO (SF)-TOT-CAR-ACQ(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-CAR-GEST-NULL = HIGH-VALUES
              MOVE TDR-TOT-CAR-GEST-NULL
                TO (SF)-TOT-CAR-GEST-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-CAR-GEST
                TO (SF)-TOT-CAR-GEST(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-CAR-INC-NULL = HIGH-VALUES
              MOVE TDR-TOT-CAR-INC-NULL
                TO (SF)-TOT-CAR-INC-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-CAR-INC
                TO (SF)-TOT-CAR-INC(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-MANFEE-ANTIC-NULL = HIGH-VALUES
              MOVE TDR-TOT-MANFEE-ANTIC-NULL
                TO (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-MANFEE-ANTIC
                TO (SF)-TOT-MANFEE-ANTIC(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-MANFEE-RICOR-NULL = HIGH-VALUES
              MOVE TDR-TOT-MANFEE-RICOR-NULL
                TO (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-MANFEE-RICOR
                TO (SF)-TOT-MANFEE-RICOR(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-MANFEE-REC-NULL = HIGH-VALUES
              MOVE TDR-TOT-MANFEE-REC-NULL
                TO (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-MANFEE-REC
                TO (SF)-TOT-MANFEE-REC(IX-TAB-TDR)
           END-IF
           MOVE TDR-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-TDR)
           MOVE TDR-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-TDR)
           MOVE TDR-DS-VER
             TO (SF)-DS-VER(IX-TAB-TDR)
           MOVE TDR-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TDR)
           MOVE TDR-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-TDR)
           MOVE TDR-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-TDR)
           MOVE TDR-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-TDR)
           IF TDR-IMP-TRASFE-NULL = HIGH-VALUES
              MOVE TDR-IMP-TRASFE-NULL
                TO (SF)-IMP-TRASFE-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-IMP-TRASFE
                TO (SF)-IMP-TRASFE(IX-TAB-TDR)
           END-IF
           IF TDR-IMP-TFR-STRC-NULL = HIGH-VALUES
              MOVE TDR-IMP-TFR-STRC-NULL
                TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-IMP-TFR-STRC
                TO (SF)-IMP-TFR-STRC(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-ACQ-EXP-NULL = HIGH-VALUES
              MOVE TDR-TOT-ACQ-EXP-NULL
                TO (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-ACQ-EXP
                TO (SF)-TOT-ACQ-EXP(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-REMUN-ASS-NULL = HIGH-VALUES
              MOVE TDR-TOT-REMUN-ASS-NULL
                TO (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-REMUN-ASS
                TO (SF)-TOT-REMUN-ASS(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE TDR-TOT-COMMIS-INTER-NULL
                TO (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-COMMIS-INTER
                TO (SF)-TOT-COMMIS-INTER(IX-TAB-TDR)
           END-IF
           IF TDR-TOT-CNBT-ANTIRAC-NULL = HIGH-VALUES
              MOVE TDR-TOT-CNBT-ANTIRAC-NULL
                TO (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-TOT-CNBT-ANTIRAC
                TO (SF)-TOT-CNBT-ANTIRAC(IX-TAB-TDR)
           END-IF
           IF TDR-FL-INC-AUTOGEN-NULL = HIGH-VALUES
              MOVE TDR-FL-INC-AUTOGEN-NULL
                TO (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TDR)
           ELSE
              MOVE TDR-FL-INC-AUTOGEN
                TO (SF)-FL-INC-AUTOGEN(IX-TAB-TDR)
           END-IF.
       VALORIZZA-OUTPUT-TDR-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY      ..... LCCVP636
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO ACC_COMM
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-P63.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE ACC-COMM

      *--> NOME TABELLA FISICA DB
           MOVE 'ACC-COMM'
             TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WP63-ST-INV
              AND WP63-ELE-MAX-ACC-COMM NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WP63-ST-ADD

                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK

                          MOVE S090-SEQ-TABELLA
                            TO WP63-ID-PTF
                               P63-ID-ACC-COMM

      *-->             TIPO OPERAZIONE DISPATCHER
                         SET IDSI0011-INSERT       TO TRUE

                       END-IF

      *-->        MODIFICA
                  WHEN WP63-ST-MOD

                    MOVE WP63-ID-PTF               TO P63-ID-ACC-COMM

      *-->       TIPO OPERAZIONE DISPATCHER
                    SET IDSI0011-UPDATE            TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WP63-ST-DEL
                     MOVE WP63-ID-PTF              TO P63-ID-ACC-COMM

      *-->       TIPO OPERAZIONE DISPATCHER
                     SET IDSI0011-DELETE           TO TRUE

              END-EVALUATE


              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN IMPOSTA DI BOLLO
                 PERFORM VAL-DCLGEN-P63
                    THRU VAL-DCLGEN-P63-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-P63
                    THRU VALORIZZA-AREA-DSH-P63-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-P63-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-P63.

      *--> DCLGEN TABELLA
           MOVE ACC-COMM                 TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-PRIMARY-KEY      TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-P63-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVP635.

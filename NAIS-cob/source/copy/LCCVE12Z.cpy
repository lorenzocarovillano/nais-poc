      *----------------------------------------------------------------*
      * DIMENSIONAMENTO OCCURS PER LA TAB. EST_TRCH_DI_GAR              *
      * COPY RIPORTANTE IL NUMERO DI OCCURS DA UTILIZZARE              *
      * PER INIZIALIZZA TOT DELLA TABELLA                              *
      *----------------------------------------------------------------*
       01 WK-E12-MAX.
          03 WK-E12-MAX-A                 PIC 9(04) VALUE     1.
          03 WK-E12-MAX-B                 PIC 9(04) VALUE    20.
          03 WK-E12-MAX-C                 PIC 9(04) VALUE  1250.

      ******************************************************************
      * COPY IO LDBS0370
      ******************************************************************
       01  LDBV0371.

           03 AREA-LDBV0371-IN.
              05 LDBV0371-ID-OGG               PIC S9(09) COMP-3.
              05 LDBV0371-TP-OGG               PIC  X(02).
              05 LDBV0371-TP-PRE-TIT           PIC  X(02).
22017         05 LDBV0371-TP-STAT-TIT1         PIC  X(02).
22017         05 LDBV0371-TP-STAT-TIT2         PIC  X(02).

           03 AREA-LDBV0371-OUT.
              05 LDBV0371-ID-TIT-CONT          PIC S9(09) COMP-3.
              05 LDBV0371-ID-DETT-TIT-CONT     PIC S9(09) COMP-3.
              05 LDBV0371-FRAZ                 PIC S9(05) COMP-3.
              05 LDBV0371-FRAZ-NULL            REDEFINES
                 LDBV0371-FRAZ                 PIC  X(03).
              05 LDBV0371-TIT-PRE-TOT          PIC S9(12)V9(3) COMP-3.
              05 LDBV0371-TIT-PRE-TOT-NULL     REDEFINES
                 LDBV0371-TIT-PRE-TOT          PIC  X(08).
              05 LDBV0371-DTC-PRE-TOT          PIC S9(12)V9(3) COMP-3.
              05 LDBV0371-DTC-PRE-TOT-NULL     REDEFINES
                 LDBV0371-DTC-PRE-TOT          PIC  X(08).
              05 LDBV0371-DTC-DIR              PIC S9(12)V9(3) COMP-3.
              05 LDBV0371-DTC-DIR-NULL         REDEFINES
                 LDBV0371-DTC-DIR              PIC  X(08).

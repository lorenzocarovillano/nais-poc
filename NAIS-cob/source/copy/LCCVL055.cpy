
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVL055
      *   ULTIMO AGG. 06 GIU 2018
      *------------------------------------------------------------

       VAL-DCLGEN-L05.
           MOVE (SF)-COD-COMP-ANIA
              TO L05-COD-COMP-ANIA
           MOVE (SF)-TP-MOVI-ESEC
              TO L05-TP-MOVI-ESEC
           MOVE (SF)-TP-MOVI-RIFTO
              TO L05-TP-MOVI-RIFTO
           MOVE (SF)-GRAV-FUNZ-FUNZ
              TO L05-GRAV-FUNZ-FUNZ
           MOVE (SF)-DS-OPER-SQL
              TO L05-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO L05-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO L05-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO L05-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO L05-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO L05-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO L05-DS-STATO-ELAB
           IF (SF)-COD-BLOCCO-NULL = HIGH-VALUES
              MOVE (SF)-COD-BLOCCO-NULL
              TO L05-COD-BLOCCO-NULL
           ELSE
              MOVE (SF)-COD-BLOCCO
              TO L05-COD-BLOCCO
           END-IF
           IF (SF)-SRVZ-VER-ANN-NULL = HIGH-VALUES
              MOVE (SF)-SRVZ-VER-ANN-NULL
              TO L05-SRVZ-VER-ANN-NULL
           ELSE
              MOVE (SF)-SRVZ-VER-ANN
              TO L05-SRVZ-VER-ANN
           END-IF
           MOVE (SF)-WHERE-CONDITION
              TO L05-WHERE-CONDITION
           IF (SF)-FL-POLI-IFP-NULL = HIGH-VALUES
              MOVE (SF)-FL-POLI-IFP-NULL
              TO L05-FL-POLI-IFP-NULL
           ELSE
              MOVE (SF)-FL-POLI-IFP
              TO L05-FL-POLI-IFP
           END-IF.
       VAL-DCLGEN-L05-EX.
           EXIT.

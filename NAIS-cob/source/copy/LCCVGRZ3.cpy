
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVGRZ3
      *   ULTIMO AGG. 31 OTT 2013
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-GRZ.
           MOVE GRZ-ID-GAR
             TO (SF)-ID-PTF(IX-TAB-GRZ)
           MOVE GRZ-ID-GAR
             TO (SF)-ID-GAR(IX-TAB-GRZ)
           IF GRZ-ID-ADES-NULL = HIGH-VALUES
              MOVE GRZ-ID-ADES-NULL
                TO (SF)-ID-ADES-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-ID-ADES
                TO (SF)-ID-ADES(IX-TAB-GRZ)
           END-IF
           MOVE GRZ-ID-POLI
             TO (SF)-ID-POLI(IX-TAB-GRZ)
           MOVE GRZ-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-GRZ)
           IF GRZ-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE GRZ-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-GRZ)
           END-IF
           MOVE GRZ-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-GRZ)
           MOVE GRZ-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-GRZ)
           MOVE GRZ-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-GRZ)
           IF GRZ-IB-OGG-NULL = HIGH-VALUES
              MOVE GRZ-IB-OGG-NULL
                TO (SF)-IB-OGG-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-IB-OGG
                TO (SF)-IB-OGG(IX-TAB-GRZ)
           END-IF
           IF GRZ-DT-DECOR-NULL = HIGH-VALUES
              MOVE GRZ-DT-DECOR-NULL
                TO (SF)-DT-DECOR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-DT-DECOR
                TO (SF)-DT-DECOR(IX-TAB-GRZ)
           END-IF
           IF GRZ-DT-SCAD-NULL = HIGH-VALUES
              MOVE GRZ-DT-SCAD-NULL
                TO (SF)-DT-SCAD-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-DT-SCAD
                TO (SF)-DT-SCAD(IX-TAB-GRZ)
           END-IF
           IF GRZ-COD-SEZ-NULL = HIGH-VALUES
              MOVE GRZ-COD-SEZ-NULL
                TO (SF)-COD-SEZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-COD-SEZ
                TO (SF)-COD-SEZ(IX-TAB-GRZ)
           END-IF
           MOVE GRZ-COD-TARI
             TO (SF)-COD-TARI(IX-TAB-GRZ)
           IF GRZ-RAMO-BILA-NULL = HIGH-VALUES
              MOVE GRZ-RAMO-BILA-NULL
                TO (SF)-RAMO-BILA-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-RAMO-BILA
                TO (SF)-RAMO-BILA(IX-TAB-GRZ)
           END-IF
           IF GRZ-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE GRZ-DT-INI-VAL-TAR-NULL
                TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-DT-INI-VAL-TAR
                TO (SF)-DT-INI-VAL-TAR(IX-TAB-GRZ)
           END-IF
           IF GRZ-ID-1O-ASSTO-NULL = HIGH-VALUES
              MOVE GRZ-ID-1O-ASSTO-NULL
                TO (SF)-ID-1O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-ID-1O-ASSTO
                TO (SF)-ID-1O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF GRZ-ID-2O-ASSTO-NULL = HIGH-VALUES
              MOVE GRZ-ID-2O-ASSTO-NULL
                TO (SF)-ID-2O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-ID-2O-ASSTO
                TO (SF)-ID-2O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF GRZ-ID-3O-ASSTO-NULL = HIGH-VALUES
              MOVE GRZ-ID-3O-ASSTO-NULL
                TO (SF)-ID-3O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-ID-3O-ASSTO
                TO (SF)-ID-3O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF GRZ-TP-GAR-NULL = HIGH-VALUES
              MOVE GRZ-TP-GAR-NULL
                TO (SF)-TP-GAR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TP-GAR
                TO (SF)-TP-GAR(IX-TAB-GRZ)
           END-IF
           IF GRZ-TP-RSH-NULL = HIGH-VALUES
              MOVE GRZ-TP-RSH-NULL
                TO (SF)-TP-RSH-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TP-RSH
                TO (SF)-TP-RSH(IX-TAB-GRZ)
           END-IF
           IF GRZ-TP-INVST-NULL = HIGH-VALUES
              MOVE GRZ-TP-INVST-NULL
                TO (SF)-TP-INVST-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TP-INVST
                TO (SF)-TP-INVST(IX-TAB-GRZ)
           END-IF
           IF GRZ-MOD-PAG-GARCOL-NULL = HIGH-VALUES
              MOVE GRZ-MOD-PAG-GARCOL-NULL
                TO (SF)-MOD-PAG-GARCOL-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-MOD-PAG-GARCOL
                TO (SF)-MOD-PAG-GARCOL(IX-TAB-GRZ)
           END-IF
           IF GRZ-TP-PER-PRE-NULL = HIGH-VALUES
              MOVE GRZ-TP-PER-PRE-NULL
                TO (SF)-TP-PER-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TP-PER-PRE
                TO (SF)-TP-PER-PRE(IX-TAB-GRZ)
           END-IF
           IF GRZ-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE GRZ-ETA-AA-1O-ASSTO-NULL
                TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-ETA-AA-1O-ASSTO
                TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF GRZ-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE GRZ-ETA-MM-1O-ASSTO-NULL
                TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-ETA-MM-1O-ASSTO
                TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF GRZ-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
              MOVE GRZ-ETA-AA-2O-ASSTO-NULL
                TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-ETA-AA-2O-ASSTO
                TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF GRZ-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
              MOVE GRZ-ETA-MM-2O-ASSTO-NULL
                TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-ETA-MM-2O-ASSTO
                TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF GRZ-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
              MOVE GRZ-ETA-AA-3O-ASSTO-NULL
                TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-ETA-AA-3O-ASSTO
                TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF GRZ-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
              MOVE GRZ-ETA-MM-3O-ASSTO-NULL
                TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-ETA-MM-3O-ASSTO
                TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-GRZ)
           END-IF
           IF GRZ-TP-EMIS-PUR-NULL = HIGH-VALUES
              MOVE GRZ-TP-EMIS-PUR-NULL
                TO (SF)-TP-EMIS-PUR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TP-EMIS-PUR
                TO (SF)-TP-EMIS-PUR(IX-TAB-GRZ)
           END-IF
           IF GRZ-ETA-A-SCAD-NULL = HIGH-VALUES
              MOVE GRZ-ETA-A-SCAD-NULL
                TO (SF)-ETA-A-SCAD-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-ETA-A-SCAD
                TO (SF)-ETA-A-SCAD(IX-TAB-GRZ)
           END-IF
           IF GRZ-TP-CALC-PRE-PRSTZ-NULL = HIGH-VALUES
              MOVE GRZ-TP-CALC-PRE-PRSTZ-NULL
                TO (SF)-TP-CALC-PRE-PRSTZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TP-CALC-PRE-PRSTZ
                TO (SF)-TP-CALC-PRE-PRSTZ(IX-TAB-GRZ)
           END-IF
           IF GRZ-TP-PRE-NULL = HIGH-VALUES
              MOVE GRZ-TP-PRE-NULL
                TO (SF)-TP-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TP-PRE
                TO (SF)-TP-PRE(IX-TAB-GRZ)
           END-IF
           IF GRZ-TP-DUR-NULL = HIGH-VALUES
              MOVE GRZ-TP-DUR-NULL
                TO (SF)-TP-DUR-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TP-DUR
                TO (SF)-TP-DUR(IX-TAB-GRZ)
           END-IF
           IF GRZ-DUR-AA-NULL = HIGH-VALUES
              MOVE GRZ-DUR-AA-NULL
                TO (SF)-DUR-AA-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-DUR-AA
                TO (SF)-DUR-AA(IX-TAB-GRZ)
           END-IF
           IF GRZ-DUR-MM-NULL = HIGH-VALUES
              MOVE GRZ-DUR-MM-NULL
                TO (SF)-DUR-MM-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-DUR-MM
                TO (SF)-DUR-MM(IX-TAB-GRZ)
           END-IF
           IF GRZ-DUR-GG-NULL = HIGH-VALUES
              MOVE GRZ-DUR-GG-NULL
                TO (SF)-DUR-GG-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-DUR-GG
                TO (SF)-DUR-GG(IX-TAB-GRZ)
           END-IF
           IF GRZ-NUM-AA-PAG-PRE-NULL = HIGH-VALUES
              MOVE GRZ-NUM-AA-PAG-PRE-NULL
                TO (SF)-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-NUM-AA-PAG-PRE
                TO (SF)-NUM-AA-PAG-PRE(IX-TAB-GRZ)
           END-IF
           IF GRZ-AA-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE GRZ-AA-PAG-PRE-UNI-NULL
                TO (SF)-AA-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-AA-PAG-PRE-UNI
                TO (SF)-AA-PAG-PRE-UNI(IX-TAB-GRZ)
           END-IF
           IF GRZ-MM-PAG-PRE-UNI-NULL = HIGH-VALUES
              MOVE GRZ-MM-PAG-PRE-UNI-NULL
                TO (SF)-MM-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-MM-PAG-PRE-UNI
                TO (SF)-MM-PAG-PRE-UNI(IX-TAB-GRZ)
           END-IF
           IF GRZ-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
              MOVE GRZ-FRAZ-INI-EROG-REN-NULL
                TO (SF)-FRAZ-INI-EROG-REN-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-FRAZ-INI-EROG-REN
                TO (SF)-FRAZ-INI-EROG-REN(IX-TAB-GRZ)
           END-IF
           IF GRZ-MM-1O-RAT-NULL = HIGH-VALUES
              MOVE GRZ-MM-1O-RAT-NULL
                TO (SF)-MM-1O-RAT-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-MM-1O-RAT
                TO (SF)-MM-1O-RAT(IX-TAB-GRZ)
           END-IF
           IF GRZ-PC-1O-RAT-NULL = HIGH-VALUES
              MOVE GRZ-PC-1O-RAT-NULL
                TO (SF)-PC-1O-RAT-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-PC-1O-RAT
                TO (SF)-PC-1O-RAT(IX-TAB-GRZ)
           END-IF
           IF GRZ-TP-PRSTZ-ASSTA-NULL = HIGH-VALUES
              MOVE GRZ-TP-PRSTZ-ASSTA-NULL
                TO (SF)-TP-PRSTZ-ASSTA-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TP-PRSTZ-ASSTA
                TO (SF)-TP-PRSTZ-ASSTA(IX-TAB-GRZ)
           END-IF
           IF GRZ-DT-END-CARZ-NULL = HIGH-VALUES
              MOVE GRZ-DT-END-CARZ-NULL
                TO (SF)-DT-END-CARZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-DT-END-CARZ
                TO (SF)-DT-END-CARZ(IX-TAB-GRZ)
           END-IF
           IF GRZ-PC-RIP-PRE-NULL = HIGH-VALUES
              MOVE GRZ-PC-RIP-PRE-NULL
                TO (SF)-PC-RIP-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-PC-RIP-PRE
                TO (SF)-PC-RIP-PRE(IX-TAB-GRZ)
           END-IF
           IF GRZ-COD-FND-NULL = HIGH-VALUES
              MOVE GRZ-COD-FND-NULL
                TO (SF)-COD-FND-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-COD-FND
                TO (SF)-COD-FND(IX-TAB-GRZ)
           END-IF
           IF GRZ-AA-REN-CER-NULL = HIGH-VALUES
              MOVE GRZ-AA-REN-CER-NULL
                TO (SF)-AA-REN-CER-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-AA-REN-CER
                TO (SF)-AA-REN-CER(IX-TAB-GRZ)
           END-IF
           IF GRZ-PC-REVRSB-NULL = HIGH-VALUES
              MOVE GRZ-PC-REVRSB-NULL
                TO (SF)-PC-REVRSB-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-PC-REVRSB
                TO (SF)-PC-REVRSB(IX-TAB-GRZ)
           END-IF
           IF GRZ-TP-PC-RIP-NULL = HIGH-VALUES
              MOVE GRZ-TP-PC-RIP-NULL
                TO (SF)-TP-PC-RIP-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TP-PC-RIP
                TO (SF)-TP-PC-RIP(IX-TAB-GRZ)
           END-IF
           IF GRZ-PC-OPZ-NULL = HIGH-VALUES
              MOVE GRZ-PC-OPZ-NULL
                TO (SF)-PC-OPZ-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-PC-OPZ
                TO (SF)-PC-OPZ(IX-TAB-GRZ)
           END-IF
           IF GRZ-TP-IAS-NULL = HIGH-VALUES
              MOVE GRZ-TP-IAS-NULL
                TO (SF)-TP-IAS-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TP-IAS
                TO (SF)-TP-IAS(IX-TAB-GRZ)
           END-IF
           IF GRZ-TP-STAB-NULL = HIGH-VALUES
              MOVE GRZ-TP-STAB-NULL
                TO (SF)-TP-STAB-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TP-STAB
                TO (SF)-TP-STAB(IX-TAB-GRZ)
           END-IF
           IF GRZ-TP-ADEG-PRE-NULL = HIGH-VALUES
              MOVE GRZ-TP-ADEG-PRE-NULL
                TO (SF)-TP-ADEG-PRE-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TP-ADEG-PRE
                TO (SF)-TP-ADEG-PRE(IX-TAB-GRZ)
           END-IF
           IF GRZ-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
              MOVE GRZ-DT-VARZ-TP-IAS-NULL
                TO (SF)-DT-VARZ-TP-IAS-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-DT-VARZ-TP-IAS
                TO (SF)-DT-VARZ-TP-IAS(IX-TAB-GRZ)
           END-IF
           IF GRZ-FRAZ-DECR-CPT-NULL = HIGH-VALUES
              MOVE GRZ-FRAZ-DECR-CPT-NULL
                TO (SF)-FRAZ-DECR-CPT-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-FRAZ-DECR-CPT
                TO (SF)-FRAZ-DECR-CPT(IX-TAB-GRZ)
           END-IF
           IF GRZ-COD-TRAT-RIASS-NULL = HIGH-VALUES
              MOVE GRZ-COD-TRAT-RIASS-NULL
                TO (SF)-COD-TRAT-RIASS-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-COD-TRAT-RIASS
                TO (SF)-COD-TRAT-RIASS(IX-TAB-GRZ)
           END-IF
           IF GRZ-TP-DT-EMIS-RIASS-NULL = HIGH-VALUES
              MOVE GRZ-TP-DT-EMIS-RIASS-NULL
                TO (SF)-TP-DT-EMIS-RIASS-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TP-DT-EMIS-RIASS
                TO (SF)-TP-DT-EMIS-RIASS(IX-TAB-GRZ)
           END-IF
           IF GRZ-TP-CESS-RIASS-NULL = HIGH-VALUES
              MOVE GRZ-TP-CESS-RIASS-NULL
                TO (SF)-TP-CESS-RIASS-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TP-CESS-RIASS
                TO (SF)-TP-CESS-RIASS(IX-TAB-GRZ)
           END-IF
           MOVE GRZ-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-GRZ)
           MOVE GRZ-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-GRZ)
           MOVE GRZ-DS-VER
             TO (SF)-DS-VER(IX-TAB-GRZ)
           MOVE GRZ-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-GRZ)
           MOVE GRZ-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-GRZ)
           MOVE GRZ-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-GRZ)
           MOVE GRZ-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-GRZ)
           IF GRZ-AA-STAB-NULL = HIGH-VALUES
              MOVE GRZ-AA-STAB-NULL
                TO (SF)-AA-STAB-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-AA-STAB
                TO (SF)-AA-STAB(IX-TAB-GRZ)
           END-IF
           IF GRZ-TS-STAB-LIMITATA-NULL = HIGH-VALUES
              MOVE GRZ-TS-STAB-LIMITATA-NULL
                TO (SF)-TS-STAB-LIMITATA-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-TS-STAB-LIMITATA
                TO (SF)-TS-STAB-LIMITATA(IX-TAB-GRZ)
           END-IF
           IF GRZ-DT-PRESC-NULL = HIGH-VALUES
              MOVE GRZ-DT-PRESC-NULL
                TO (SF)-DT-PRESC-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-DT-PRESC
                TO (SF)-DT-PRESC(IX-TAB-GRZ)
           END-IF
           IF GRZ-RSH-INVST-NULL = HIGH-VALUES
              MOVE GRZ-RSH-INVST-NULL
                TO (SF)-RSH-INVST-NULL(IX-TAB-GRZ)
           ELSE
              MOVE GRZ-RSH-INVST
                TO (SF)-RSH-INVST(IX-TAB-GRZ)
           END-IF
           MOVE GRZ-TP-RAMO-BILA
             TO (SF)-TP-RAMO-BILA(IX-TAB-GRZ).
       VALORIZZA-OUTPUT-GRZ-EX.
           EXIT.

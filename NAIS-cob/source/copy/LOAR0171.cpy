      *
      *  --------------------------------------------------------------*
      *     STRUTTURA ARCHIVIO IN OUTPUT - LOAR0171 - LREC=500         *
      *  --------------------------------------------------------------*
      *
          03 WREC-TIPO-REC                      PIC  X(03).
          03 WREC-OUT                           PIC  X(497).
          03 WREC-GEN       REDEFINES   WREC-OUT.
             05 WREC-NUM-POL-IND                PIC  X(40).
             05 WREC-MACROFUNZ                  PIC  X(02).
             05 WREC-FUNZ                       PIC  9(04).
             05 WREC-COD-COMP                   PIC  9(05).
             05 WREC-DT-RICH-DA                 PIC  9(08).
             05 WREC-DT-RICH-A                  PIC  9(08).
             05 WREC-RAMO                       PIC  X(12).
             05 WREC-NUM-POL-COLL               PIC  X(40).
             05 WREC-NUM-ADESIONE               PIC  X(40).
             05 WREC-DT-COMP-RIVA               PIC  9(08).
             05 FILLER                          PIC  X(330).
          03 WREC-DET       REDEFINES   WREC-OUT.
             05 WREC-NUM-POL-IND                PIC  X(40).
             05 WREC-COD-GAR-TRCH               PIC  X(12).
             05 WREC-NUM-TRCH                   PIC  X(40).
             05 WREC-DT-DECOR-TRCH              PIC  9(08).
             05 WREC-DT-ULT-RIVA-TRCH           PIC  9(08).
             05 WREC-PRE-RIVTO                  PIC  9(12)V9(3).
             05 WREC-PRSTZ-PREC                 PIC  9(12)V9(3).
             05 WREC-PRSTZ-ULT                  PIC  9(12)V9(3).
             05 WREC-PRE-PP-ULT                 PIC  9(12)V9(3).
             05 WREC-IMP-SOPR-SAN               PIC  9(12)V9(3).
             05 WREC-IMP-SOPR-PROF              PIC  9(12)V9(3).
             05 WREC-IMP-SOPR-SPO               PIC  9(12)V9(3).
             05 WREC-IMP-SOPR-TEC               PIC  9(12)V9(3).
             05 WREC-IMP-ALT-SOPR               PIC  9(12)V9(3).
             05 WREC-PRE-UNI-RIVTO              PIC  9(12)V9(3).
             05 WREC-PRE-INVRIO-ULT             PIC  9(12)V9(3).
             05 WREC-RIS-MAT                    PIC  9(12)V9(3).
             05 WREC-CPT-IN-OPZ-RIVTO           PIC  9(12)V9(3).
             05 WREC-RENDTO-LRD                 PIC  9(03)V9(2).
             05 WREC-PC-RETR                    PIC  9(03)V9(3).
             05 WREC-MIN-TRNUT                  PIC  9(03)V9(3).
             05 WREC-MIN-GARTO                  PIC  9(03)V9(2).
             05 WREC-RENDTO-RETR                PIC  9(03)V9(2).
             05 WREC-RENDTO-NET                 PIC  9(03)V9(2).
             05 WREC-CPT-GARTO-A-SCAD           PIC  9(12)V9(3).
             05 WREC-PRE-CASO-MOR               PIC  9(12)V9(3).
             05 WREC-ABB-ANNU-ULT               PIC  9(12)V9(3).
             05 WREC-COMM-GEST                  PIC  9(12)V9(3).
             05 WREC-INCR-PRSTZ                 PIC  9(12)V9(3).
             05 WREC-INCR-PRE                   PIC  9(12)V9(3).
             05 WREC-PRSTZ-AGG-ULT              PIC  9(12)V9(3).
             05 WREC-MANFEE-RICOR               PIC  9(12)V9(3).
             05 WREC-DT-VLT-TIT                 PIC  9(08).
             05 WREC-NUM-GG-RIVAL               PIC  9(5).
             05 WREC-INT-MORA                   PIC  9(12)V9(3).
             05 WREC-NUM-GG-RIT-PAG             PIC  9(5).
             05 FILLER                          PIC  X(09).

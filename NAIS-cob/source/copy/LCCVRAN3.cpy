
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVRAN3
      *   ULTIMO AGG. 24 GEN 2014
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-RAN.
           MOVE RAN-ID-RAPP-ANA
             TO (SF)-ID-PTF(IX-TAB-RAN)
           MOVE RAN-ID-RAPP-ANA
             TO (SF)-ID-RAPP-ANA(IX-TAB-RAN)
           IF RAN-ID-RAPP-ANA-COLLG-NULL = HIGH-VALUES
              MOVE RAN-ID-RAPP-ANA-COLLG-NULL
                TO (SF)-ID-RAPP-ANA-COLLG-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-ID-RAPP-ANA-COLLG
                TO (SF)-ID-RAPP-ANA-COLLG(IX-TAB-RAN)
           END-IF
           MOVE RAN-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-RAN)
           MOVE RAN-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-RAN)
           MOVE RAN-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-RAN)
           IF RAN-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE RAN-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-RAN)
           END-IF
           MOVE RAN-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-RAN)
           MOVE RAN-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-RAN)
           MOVE RAN-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-RAN)
           IF RAN-COD-SOGG-NULL = HIGH-VALUES
              MOVE RAN-COD-SOGG-NULL
                TO (SF)-COD-SOGG-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-COD-SOGG
                TO (SF)-COD-SOGG(IX-TAB-RAN)
           END-IF
           MOVE RAN-TP-RAPP-ANA
             TO (SF)-TP-RAPP-ANA(IX-TAB-RAN)
           IF RAN-TP-PERS-NULL = HIGH-VALUES
              MOVE RAN-TP-PERS-NULL
                TO (SF)-TP-PERS-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-TP-PERS
                TO (SF)-TP-PERS(IX-TAB-RAN)
           END-IF
           IF RAN-SEX-NULL = HIGH-VALUES
              MOVE RAN-SEX-NULL
                TO (SF)-SEX-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-SEX
                TO (SF)-SEX(IX-TAB-RAN)
           END-IF
           IF RAN-DT-NASC-NULL = HIGH-VALUES
              MOVE RAN-DT-NASC-NULL
                TO (SF)-DT-NASC-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-DT-NASC
                TO (SF)-DT-NASC(IX-TAB-RAN)
           END-IF
           IF RAN-FL-ESTAS-NULL = HIGH-VALUES
              MOVE RAN-FL-ESTAS-NULL
                TO (SF)-FL-ESTAS-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-FL-ESTAS
                TO (SF)-FL-ESTAS(IX-TAB-RAN)
           END-IF
           IF RAN-INDIR-1-NULL = HIGH-VALUES
              MOVE RAN-INDIR-1-NULL
                TO (SF)-INDIR-1-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-INDIR-1
                TO (SF)-INDIR-1(IX-TAB-RAN)
           END-IF
           IF RAN-INDIR-2-NULL = HIGH-VALUES
              MOVE RAN-INDIR-2-NULL
                TO (SF)-INDIR-2-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-INDIR-2
                TO (SF)-INDIR-2(IX-TAB-RAN)
           END-IF
           IF RAN-INDIR-3-NULL = HIGH-VALUES
              MOVE RAN-INDIR-3-NULL
                TO (SF)-INDIR-3-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-INDIR-3
                TO (SF)-INDIR-3(IX-TAB-RAN)
           END-IF
           IF RAN-TP-UTLZ-INDIR-1-NULL = HIGH-VALUES
              MOVE RAN-TP-UTLZ-INDIR-1-NULL
                TO (SF)-TP-UTLZ-INDIR-1-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-TP-UTLZ-INDIR-1
                TO (SF)-TP-UTLZ-INDIR-1(IX-TAB-RAN)
           END-IF
           IF RAN-TP-UTLZ-INDIR-2-NULL = HIGH-VALUES
              MOVE RAN-TP-UTLZ-INDIR-2-NULL
                TO (SF)-TP-UTLZ-INDIR-2-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-TP-UTLZ-INDIR-2
                TO (SF)-TP-UTLZ-INDIR-2(IX-TAB-RAN)
           END-IF
           IF RAN-TP-UTLZ-INDIR-3-NULL = HIGH-VALUES
              MOVE RAN-TP-UTLZ-INDIR-3-NULL
                TO (SF)-TP-UTLZ-INDIR-3-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-TP-UTLZ-INDIR-3
                TO (SF)-TP-UTLZ-INDIR-3(IX-TAB-RAN)
           END-IF
           IF RAN-ESTR-CNT-CORR-ACCR-NULL = HIGH-VALUES
              MOVE RAN-ESTR-CNT-CORR-ACCR-NULL
                TO (SF)-ESTR-CNT-CORR-ACCR-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-ESTR-CNT-CORR-ACCR
                TO (SF)-ESTR-CNT-CORR-ACCR(IX-TAB-RAN)
           END-IF
           IF RAN-ESTR-CNT-CORR-ADD-NULL = HIGH-VALUES
              MOVE RAN-ESTR-CNT-CORR-ADD-NULL
                TO (SF)-ESTR-CNT-CORR-ADD-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-ESTR-CNT-CORR-ADD
                TO (SF)-ESTR-CNT-CORR-ADD(IX-TAB-RAN)
           END-IF
           IF RAN-ESTR-DOCTO-NULL = HIGH-VALUES
              MOVE RAN-ESTR-DOCTO-NULL
                TO (SF)-ESTR-DOCTO-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-ESTR-DOCTO
                TO (SF)-ESTR-DOCTO(IX-TAB-RAN)
           END-IF
           IF RAN-PC-NEL-RAPP-NULL = HIGH-VALUES
              MOVE RAN-PC-NEL-RAPP-NULL
                TO (SF)-PC-NEL-RAPP-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-PC-NEL-RAPP
                TO (SF)-PC-NEL-RAPP(IX-TAB-RAN)
           END-IF
           IF RAN-TP-MEZ-PAG-ADD-NULL = HIGH-VALUES
              MOVE RAN-TP-MEZ-PAG-ADD-NULL
                TO (SF)-TP-MEZ-PAG-ADD-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-TP-MEZ-PAG-ADD
                TO (SF)-TP-MEZ-PAG-ADD(IX-TAB-RAN)
           END-IF
           IF RAN-TP-MEZ-PAG-ACCR-NULL = HIGH-VALUES
              MOVE RAN-TP-MEZ-PAG-ACCR-NULL
                TO (SF)-TP-MEZ-PAG-ACCR-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-TP-MEZ-PAG-ACCR
                TO (SF)-TP-MEZ-PAG-ACCR(IX-TAB-RAN)
           END-IF
           IF RAN-COD-MATR-NULL = HIGH-VALUES
              MOVE RAN-COD-MATR-NULL
                TO (SF)-COD-MATR-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-COD-MATR
                TO (SF)-COD-MATR(IX-TAB-RAN)
           END-IF
           IF RAN-TP-ADEGZ-NULL = HIGH-VALUES
              MOVE RAN-TP-ADEGZ-NULL
                TO (SF)-TP-ADEGZ-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-TP-ADEGZ
                TO (SF)-TP-ADEGZ(IX-TAB-RAN)
           END-IF
           IF RAN-FL-TST-RSH-NULL = HIGH-VALUES
              MOVE RAN-FL-TST-RSH-NULL
                TO (SF)-FL-TST-RSH-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-FL-TST-RSH
                TO (SF)-FL-TST-RSH(IX-TAB-RAN)
           END-IF
           IF RAN-COD-AZ-NULL = HIGH-VALUES
              MOVE RAN-COD-AZ-NULL
                TO (SF)-COD-AZ-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-COD-AZ
                TO (SF)-COD-AZ(IX-TAB-RAN)
           END-IF
           IF RAN-IND-PRINC-NULL = HIGH-VALUES
              MOVE RAN-IND-PRINC-NULL
                TO (SF)-IND-PRINC-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-IND-PRINC
                TO (SF)-IND-PRINC(IX-TAB-RAN)
           END-IF
           IF RAN-DT-DELIBERA-CDA-NULL = HIGH-VALUES
              MOVE RAN-DT-DELIBERA-CDA-NULL
                TO (SF)-DT-DELIBERA-CDA-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-DT-DELIBERA-CDA
                TO (SF)-DT-DELIBERA-CDA(IX-TAB-RAN)
           END-IF
           IF RAN-DLG-AL-RISC-NULL = HIGH-VALUES
              MOVE RAN-DLG-AL-RISC-NULL
                TO (SF)-DLG-AL-RISC-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-DLG-AL-RISC
                TO (SF)-DLG-AL-RISC(IX-TAB-RAN)
           END-IF
           IF RAN-LEGALE-RAPPR-PRINC-NULL = HIGH-VALUES
              MOVE RAN-LEGALE-RAPPR-PRINC-NULL
                TO (SF)-LEGALE-RAPPR-PRINC-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-LEGALE-RAPPR-PRINC
                TO (SF)-LEGALE-RAPPR-PRINC(IX-TAB-RAN)
           END-IF
           IF RAN-TP-LEGALE-RAPPR-NULL = HIGH-VALUES
              MOVE RAN-TP-LEGALE-RAPPR-NULL
                TO (SF)-TP-LEGALE-RAPPR-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-TP-LEGALE-RAPPR
                TO (SF)-TP-LEGALE-RAPPR(IX-TAB-RAN)
           END-IF
           IF RAN-TP-IND-PRINC-NULL = HIGH-VALUES
              MOVE RAN-TP-IND-PRINC-NULL
                TO (SF)-TP-IND-PRINC-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-TP-IND-PRINC
                TO (SF)-TP-IND-PRINC(IX-TAB-RAN)
           END-IF
           IF RAN-TP-STAT-RID-NULL = HIGH-VALUES
              MOVE RAN-TP-STAT-RID-NULL
                TO (SF)-TP-STAT-RID-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-TP-STAT-RID
                TO (SF)-TP-STAT-RID(IX-TAB-RAN)
           END-IF
           MOVE RAN-NOME-INT-RID
             TO (SF)-NOME-INT-RID(IX-TAB-RAN)
           MOVE RAN-COGN-INT-RID
             TO (SF)-COGN-INT-RID(IX-TAB-RAN)
           MOVE RAN-COGN-INT-TRATT
             TO (SF)-COGN-INT-TRATT(IX-TAB-RAN)
           MOVE RAN-NOME-INT-TRATT
             TO (SF)-NOME-INT-TRATT(IX-TAB-RAN)
           IF RAN-CF-INT-RID-NULL = HIGH-VALUES
              MOVE RAN-CF-INT-RID-NULL
                TO (SF)-CF-INT-RID-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-CF-INT-RID
                TO (SF)-CF-INT-RID(IX-TAB-RAN)
           END-IF
           IF RAN-FL-COINC-DIP-CNTR-NULL = HIGH-VALUES
              MOVE RAN-FL-COINC-DIP-CNTR-NULL
                TO (SF)-FL-COINC-DIP-CNTR-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-FL-COINC-DIP-CNTR
                TO (SF)-FL-COINC-DIP-CNTR(IX-TAB-RAN)
           END-IF
           IF RAN-FL-COINC-INT-CNTR-NULL = HIGH-VALUES
              MOVE RAN-FL-COINC-INT-CNTR-NULL
                TO (SF)-FL-COINC-INT-CNTR-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-FL-COINC-INT-CNTR
                TO (SF)-FL-COINC-INT-CNTR(IX-TAB-RAN)
           END-IF
           IF RAN-DT-DECES-NULL = HIGH-VALUES
              MOVE RAN-DT-DECES-NULL
                TO (SF)-DT-DECES-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-DT-DECES
                TO (SF)-DT-DECES(IX-TAB-RAN)
           END-IF
           IF RAN-FL-FUMATORE-NULL = HIGH-VALUES
              MOVE RAN-FL-FUMATORE-NULL
                TO (SF)-FL-FUMATORE-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-FL-FUMATORE
                TO (SF)-FL-FUMATORE(IX-TAB-RAN)
           END-IF
           MOVE RAN-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-RAN)
           MOVE RAN-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-RAN)
           MOVE RAN-DS-VER
             TO (SF)-DS-VER(IX-TAB-RAN)
           MOVE RAN-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-RAN)
           MOVE RAN-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-RAN)
           MOVE RAN-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-RAN)
           MOVE RAN-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-RAN)
           IF RAN-FL-LAV-DIP-NULL = HIGH-VALUES
              MOVE RAN-FL-LAV-DIP-NULL
                TO (SF)-FL-LAV-DIP-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-FL-LAV-DIP
                TO (SF)-FL-LAV-DIP(IX-TAB-RAN)
           END-IF
           IF RAN-TP-VARZ-PAGAT-NULL = HIGH-VALUES
              MOVE RAN-TP-VARZ-PAGAT-NULL
                TO (SF)-TP-VARZ-PAGAT-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-TP-VARZ-PAGAT
                TO (SF)-TP-VARZ-PAGAT(IX-TAB-RAN)
           END-IF
           IF RAN-COD-RID-NULL = HIGH-VALUES
              MOVE RAN-COD-RID-NULL
                TO (SF)-COD-RID-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-COD-RID
                TO (SF)-COD-RID(IX-TAB-RAN)
           END-IF
           IF RAN-TP-CAUS-RID-NULL = HIGH-VALUES
              MOVE RAN-TP-CAUS-RID-NULL
                TO (SF)-TP-CAUS-RID-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-TP-CAUS-RID
                TO (SF)-TP-CAUS-RID(IX-TAB-RAN)
           END-IF
           IF RAN-IND-MASSA-CORP-NULL = HIGH-VALUES
              MOVE RAN-IND-MASSA-CORP-NULL
                TO (SF)-IND-MASSA-CORP-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-IND-MASSA-CORP
                TO (SF)-IND-MASSA-CORP(IX-TAB-RAN)
           END-IF
           IF RAN-CAT-RSH-PROF-NULL = HIGH-VALUES
              MOVE RAN-CAT-RSH-PROF-NULL
                TO (SF)-CAT-RSH-PROF-NULL(IX-TAB-RAN)
           ELSE
              MOVE RAN-CAT-RSH-PROF
                TO (SF)-CAT-RSH-PROF(IX-TAB-RAN)
           END-IF.
       VALORIZZA-OUTPUT-RAN-EX.
           EXIT.

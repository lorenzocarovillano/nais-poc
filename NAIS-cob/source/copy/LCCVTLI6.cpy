      *----------------------------------------------------------------*
      *    COPY      ..... LCCVTLI6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO TRANCHE DI LIQUIDAZIONE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-TRCH-LIQ.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE TRCH-LIQ

      *--> NOME TABELLA FISICA DB
           MOVE 'TRCH-LIQ'                  TO WK-TABELLA.


      *--> LA PARTE COMMENTATA DEVE ESSERE PORTATA NELL'EOC DEI SINISTRI
      *    IF WTLI-ID-GAR-LIQ-NULL(IX-TAB-TLI) = HIGH-VALUE
      *       CONTINUE
      *    ELSE
      *       MOVE WTLI-ID-GAR-LIQ(IX-TAB-TLI)  TO WS-ID-GAR-DI-TLIQ
      *       SET  NON-TROVATO                  TO TRUE
      *
      *       PERFORM S2000-SEARCH-ID-TLI
      *          THRU S2000-SEARCH-ID-TLI-EX
      *       VARYING IX-TAB-GRL FROM 1 BY 1
      *         UNTIL IX-TAB-GRL > 10 OR TROVATO
      *    END-IF.

      *    IF TROVATO
      *    OR WTLI-ID-GAR-LIQ-NULL(IX-TAB-TLI) = HIGH-VALUE


      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WTLI-ST-INV(IX-TAB-TLI)
              AND WTLI-ELE-TRCH-LIQ-MAX  NOT = 0

              MOVE WMOV-ID-PTF      TO TLI-ID-MOVI-CRZ

              EVALUATE TRUE
      *-->       INSERIMENTO
                  WHEN WTLI-ST-ADD(IX-TAB-TLI)

      *-->           ESTRAZIONE SEQUENCE LIQUIDAZIONE

                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK
                        MOVE S090-SEQ-TABELLA TO WTLI-ID-PTF(IX-TAB-TLI)
                                                 TLI-ID-TRCH-LIQ

                        MOVE WLQU-ID-PTF(IX-TAB-LQU)
                                              TO TLI-ID-LIQ
                        IF WTLI-ID-GAR-LIQ-NULL(IX-TAB-TLI) = HIGH-VALUE
                           MOVE HIGH-VALUE    TO TLI-ID-GAR-LIQ-NULL
                        ELSE
                           MOVE WTLI-ID-GAR-LIQ(IX-TAB-TLI)
                                              TO TLI-ID-GAR-LIQ
                        END-IF
                        MOVE WTLI-ID-TRCH-DI-GAR(IX-TAB-TLI)
                                              TO TLI-ID-TRCH-DI-GAR
                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        MODIFICA
                  WHEN WTLI-ST-MOD(IX-TAB-TLI)

      *-->           VALORIZZAZIONE ID TRANCHE LIQUIDAZIONE
      *               MOVE WMOV-ID-PTF             TO TLI-ID-MOVI-CRZ
                     MOVE WMOV-DT-EFF             TO TLI-DT-INI-EFF
                     MOVE WTLI-ID-TRCH-LIQ(IX-TAB-TLI)
                                                  TO TLI-ID-TRCH-LIQ
                     MOVE WLQU-ID-PTF(IX-TAB-LQU) TO TLI-ID-LIQ
                     IF WTLI-ID-GAR-LIQ-NULL(IX-TAB-TLI) = HIGH-VALUE
                        MOVE HIGH-VALUE           TO TLI-ID-GAR-LIQ-NULL
                     ELSE
                        MOVE WTLI-ID-GAR-LIQ(IX-TAB-TLI)
                                                  TO TLI-ID-GAR-LIQ
                     END-IF
                     MOVE WTLI-ID-TRCH-DI-GAR(IX-TAB-TLI)
                                                  TO TLI-ID-TRCH-DI-GAR

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WTLI-ST-DEL(IX-TAB-TLI)

                     IF WTLI-ID-MOVI-CHIU-NULL(IX-TAB-TLI) = HIGH-VALUES
                        MOVE WTLI-ID-MOVI-CHIU-NULL(IX-TAB-TLI)
                          TO TLI-ID-MOVI-CHIU-NULL
                     ELSE
                        MOVE WTLI-ID-MOVI-CHIU(IX-TAB-TLI)
                          TO TLI-ID-MOVI-CHIU
                     END-IF


                     MOVE WTLI-ID-TRCH-LIQ(IX-TAB-TLI)
                       TO TLI-ID-TRCH-LIQ

                     IF WTLI-ID-GAR-LIQ-NULL(IX-TAB-TLI) = HIGH-VALUE
                        MOVE HIGH-VALUE           TO TLI-ID-GAR-LIQ-NULL
                     ELSE
                        MOVE WTLI-ID-GAR-LIQ(IX-TAB-TLI)
                                                  TO TLI-ID-GAR-LIQ
                     END-IF
                     MOVE WTLI-ID-TRCH-DI-GAR(IX-TAB-TLI)
                       TO TLI-ID-TRCH-DI-GAR

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-DELETE-LOGICA    TO TRUE

                END-EVALUATE

                IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN TRANCHE DI LIQUIDAZIONE
                 PERFORM VAL-DCLGEN-TLI
                    THRU VAL-DCLGEN-TLI-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-TLI
                    THRU VALORIZZA-AREA-DSH-TLI-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.


      *    ELSE
      *         CONTINUE
      *    END-IF.

       AGGIORNA-TRCH-LIQ-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    CICLO DI ELABORAZIONE SU DCLGEN GARANZIE DI LIQUIDAZIONI
      *----------------------------------------------------------------*
      *S2000-SEARCH-ID-TLI.
      *
      *    IF WS-ID-GAR-DI-TLIQ = WGRL-ID-GAR-LIQ(IX-TAB-GRL)
      *       SET TROVATO                  TO TRUE
      *       MOVE WGRL-ID-PTF(IX-TAB-GRL) TO WS-ID-GAR-DI-GLIQ
      *    END-IF.
      *
      *S2000-SEARCH-ID-TLI-EX.
      *    EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-TLI.

      *--> DCLGEN TABELLA
           MOVE TRCH-LIQ                TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT  TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-TLI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVTLI5.

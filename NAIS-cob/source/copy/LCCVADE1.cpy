      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA ADES
      *   ALIAS ADE
      *   ULTIMO AGG. 07 DIC 2010
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-ADES PIC S9(9)     COMP-3.
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-IB-PREV PIC X(40).
             07 (SF)-IB-PREV-NULL REDEFINES
                (SF)-IB-PREV   PIC X(40).
             07 (SF)-IB-OGG PIC X(40).
             07 (SF)-IB-OGG-NULL REDEFINES
                (SF)-IB-OGG   PIC X(40).
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-DT-DECOR   PIC S9(8) COMP-3.
             07 (SF)-DT-DECOR-NULL REDEFINES
                (SF)-DT-DECOR   PIC X(5).
             07 (SF)-DT-SCAD   PIC S9(8) COMP-3.
             07 (SF)-DT-SCAD-NULL REDEFINES
                (SF)-DT-SCAD   PIC X(5).
             07 (SF)-ETA-A-SCAD PIC S9(5)     COMP-3.
             07 (SF)-ETA-A-SCAD-NULL REDEFINES
                (SF)-ETA-A-SCAD   PIC X(3).
             07 (SF)-DUR-AA PIC S9(5)     COMP-3.
             07 (SF)-DUR-AA-NULL REDEFINES
                (SF)-DUR-AA   PIC X(3).
             07 (SF)-DUR-MM PIC S9(5)     COMP-3.
             07 (SF)-DUR-MM-NULL REDEFINES
                (SF)-DUR-MM   PIC X(3).
             07 (SF)-DUR-GG PIC S9(5)     COMP-3.
             07 (SF)-DUR-GG-NULL REDEFINES
                (SF)-DUR-GG   PIC X(3).
             07 (SF)-TP-RGM-FISC PIC X(2).
             07 (SF)-TP-RIAT PIC X(20).
             07 (SF)-TP-RIAT-NULL REDEFINES
                (SF)-TP-RIAT   PIC X(20).
             07 (SF)-TP-MOD-PAG-TIT PIC X(2).
             07 (SF)-TP-IAS PIC X(2).
             07 (SF)-TP-IAS-NULL REDEFINES
                (SF)-TP-IAS   PIC X(2).
             07 (SF)-DT-VARZ-TP-IAS   PIC S9(8) COMP-3.
             07 (SF)-DT-VARZ-TP-IAS-NULL REDEFINES
                (SF)-DT-VARZ-TP-IAS   PIC X(5).
             07 (SF)-PRE-NET-IND PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-NET-IND-NULL REDEFINES
                (SF)-PRE-NET-IND   PIC X(8).
             07 (SF)-PRE-LRD-IND PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-LRD-IND-NULL REDEFINES
                (SF)-PRE-LRD-IND   PIC X(8).
             07 (SF)-RAT-LRD-IND PIC S9(12)V9(3) COMP-3.
             07 (SF)-RAT-LRD-IND-NULL REDEFINES
                (SF)-RAT-LRD-IND   PIC X(8).
             07 (SF)-PRSTZ-INI-IND PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-INI-IND-NULL REDEFINES
                (SF)-PRSTZ-INI-IND   PIC X(8).
             07 (SF)-FL-COINC-ASSTO PIC X(1).
             07 (SF)-FL-COINC-ASSTO-NULL REDEFINES
                (SF)-FL-COINC-ASSTO   PIC X(1).
             07 (SF)-IB-DFLT PIC X(40).
             07 (SF)-IB-DFLT-NULL REDEFINES
                (SF)-IB-DFLT   PIC X(40).
             07 (SF)-MOD-CALC PIC X(2).
             07 (SF)-MOD-CALC-NULL REDEFINES
                (SF)-MOD-CALC   PIC X(2).
             07 (SF)-TP-FNT-CNBTVA PIC X(2).
             07 (SF)-TP-FNT-CNBTVA-NULL REDEFINES
                (SF)-TP-FNT-CNBTVA   PIC X(2).
             07 (SF)-IMP-AZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-AZ-NULL REDEFINES
                (SF)-IMP-AZ   PIC X(8).
             07 (SF)-IMP-ADER PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-ADER-NULL REDEFINES
                (SF)-IMP-ADER   PIC X(8).
             07 (SF)-IMP-TFR PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-TFR-NULL REDEFINES
                (SF)-IMP-TFR   PIC X(8).
             07 (SF)-IMP-VOLO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-VOLO-NULL REDEFINES
                (SF)-IMP-VOLO   PIC X(8).
             07 (SF)-PC-AZ PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-AZ-NULL REDEFINES
                (SF)-PC-AZ   PIC X(4).
             07 (SF)-PC-ADER PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-ADER-NULL REDEFINES
                (SF)-PC-ADER   PIC X(4).
             07 (SF)-PC-TFR PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-TFR-NULL REDEFINES
                (SF)-PC-TFR   PIC X(4).
             07 (SF)-PC-VOLO PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-VOLO-NULL REDEFINES
                (SF)-PC-VOLO   PIC X(4).
             07 (SF)-DT-NOVA-RGM-FISC   PIC S9(8) COMP-3.
             07 (SF)-DT-NOVA-RGM-FISC-NULL REDEFINES
                (SF)-DT-NOVA-RGM-FISC   PIC X(5).
             07 (SF)-FL-ATTIV PIC X(1).
             07 (SF)-FL-ATTIV-NULL REDEFINES
                (SF)-FL-ATTIV   PIC X(1).
             07 (SF)-IMP-REC-RIT-VIS PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-REC-RIT-VIS-NULL REDEFINES
                (SF)-IMP-REC-RIT-VIS   PIC X(8).
             07 (SF)-IMP-REC-RIT-ACC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-REC-RIT-ACC-NULL REDEFINES
                (SF)-IMP-REC-RIT-ACC   PIC X(8).
             07 (SF)-FL-VARZ-STAT-TBGC PIC X(1).
             07 (SF)-FL-VARZ-STAT-TBGC-NULL REDEFINES
                (SF)-FL-VARZ-STAT-TBGC   PIC X(1).
             07 (SF)-FL-PROVZA-MIGRAZ PIC X(1).
             07 (SF)-FL-PROVZA-MIGRAZ-NULL REDEFINES
                (SF)-FL-PROVZA-MIGRAZ   PIC X(1).
             07 (SF)-IMPB-VIS-DA-REC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-DA-REC-NULL REDEFINES
                (SF)-IMPB-VIS-DA-REC   PIC X(8).
             07 (SF)-DT-DECOR-PREST-BAN   PIC S9(8) COMP-3.
             07 (SF)-DT-DECOR-PREST-BAN-NULL REDEFINES
                (SF)-DT-DECOR-PREST-BAN   PIC X(5).
             07 (SF)-DT-EFF-VARZ-STAT-T   PIC S9(8) COMP-3.
             07 (SF)-DT-EFF-VARZ-STAT-T-NULL REDEFINES
                (SF)-DT-EFF-VARZ-STAT-T   PIC X(5).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-CUM-CNBT-CAP PIC S9(12)V9(3) COMP-3.
             07 (SF)-CUM-CNBT-CAP-NULL REDEFINES
                (SF)-CUM-CNBT-CAP   PIC X(8).
             07 (SF)-IMP-GAR-CNBT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-GAR-CNBT-NULL REDEFINES
                (SF)-IMP-GAR-CNBT   PIC X(8).
             07 (SF)-DT-ULT-CONS-CNBT   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-CONS-CNBT-NULL REDEFINES
                (SF)-DT-ULT-CONS-CNBT   PIC X(5).
             07 (SF)-IDEN-ISC-FND PIC X(40).
             07 (SF)-IDEN-ISC-FND-NULL REDEFINES
                (SF)-IDEN-ISC-FND   PIC X(40).
             07 (SF)-NUM-RAT-PIAN PIC S9(7)V9(5) COMP-3.
             07 (SF)-NUM-RAT-PIAN-NULL REDEFINES
                (SF)-NUM-RAT-PIAN   PIC X(7).
             07 (SF)-DT-PRESC   PIC S9(8) COMP-3.
             07 (SF)-DT-PRESC-NULL REDEFINES
                (SF)-DT-PRESC   PIC X(5).
             07 (SF)-CONCS-PREST PIC X(1).
             07 (SF)-CONCS-PREST-NULL REDEFINES
                (SF)-CONCS-PREST   PIC X(1).

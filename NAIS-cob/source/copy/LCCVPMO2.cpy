      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       LETTURA-PMO.

           IF IDSI0011-SELECT
             PERFORM SELECT-PMO
                THRU SELECT-PMO-EX
           ELSE
             PERFORM FETCH-PMO
                THRU FETCH-PMO-EX
           END-IF.

       LETTURA-PMO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    SELECT
      *----------------------------------------------------------------*
       SELECT-PMO.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
      *-->    GESTIRE ERRORE DB
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                     MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'SELECT-PMO'
                                             TO IEAI9901-LABEL-ERR
                     MOVE '005017'           TO IEAI9901-COD-ERRORE
                     MOVE 'PMO-ID-PARAM-MOVI' TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI TO PARAM-MOVI
                     MOVE 1                    TO IX-TAB-PMO
                     PERFORM VALORIZZA-OUTPUT-PMO
                        THRU VALORIZZA-OUTPUT-PMO-EX
                  WHEN OTHER
      *------  ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM
                                        TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'SELECT-PMO'
                                        TO IEAI9901-LABEL-ERR
                     MOVE '005016'          TO IEAI9901-COD-ERRORE
                     STRING 'PARAM-MOVI'       ';'
                          IDSO0011-RETURN-CODE ';'
                          IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SELECT-PMO' TO IEAI9901-LABEL-ERR
              MOVE '005016'                 TO IEAI9901-COD-ERRORE
              STRING 'PARAM-MOVI'         ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       SELECT-PMO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         FETCH                                  *
      *----------------------------------------------------------------*
       FETCH-PMO.

             SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
             SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
             SET  WCOM-OVERFLOW-NO                  TO TRUE.

             PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                        OR NOT IDSO0011-SUCCESSFUL-SQL
                        OR WCOM-OVERFLOW-YES

                   PERFORM CALL-DISPATCHER
                      THRU CALL-DISPATCHER-EX

                   IF IDSO0011-SUCCESSFUL-RC
                     EVALUATE TRUE
      *-->               NON TROVATA
                         WHEN IDSO0011-NOT-FOUND
                            IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM
                                TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'FETCH-DISPATCHER'
                                TO IEAI9901-LABEL-ERR
                              MOVE '005019'
                                TO IEAI9901-COD-ERRORE
                              MOVE 'PMO-ID-PARAM-MOVI'
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                            END-IF

                         WHEN IDSO0011-SUCCESSFUL-SQL
      *-->             OPERAZIONE ESEGUITA CORRETTAMENTE
                            MOVE IDSO0011-BUFFER-DATI TO PARAM-MOVI
                             ADD 1 TO CONT-FETCH

      *-->  Se il contatore di lettura � maggiore dell' elemento MAX
      *-->  previsto per la TABELLA allora siamo in overflow
                            IF CONT-FETCH > (SF)-ELE-PARAM-MOVI-MAX
                              SET WCOM-OVERFLOW-YES TO TRUE
                            ELSE
                              PERFORM VALORIZZA-OUTPUT-PMO
                                 THRU VALORIZZA-OUTPUT-PMO-EX
                              SET  IDSI0011-FETCH-NEXT TO TRUE
                            END-IF
                         WHEN OTHER
      *-------------------ERRORE DI ACCESSO AL DB
                            MOVE WK-PGM
                              TO IEAI9901-COD-SERVIZIO-BE
                            MOVE 'FETCH-PMO'
                              TO IEAI9901-LABEL-ERR
                            MOVE '005016'   TO IEAI9901-COD-ERRORE
                            STRING 'PARAM-MOVI'              ';'
                                 IDSO0011-RETURN-CODE ';'
                                 IDSO0011-SQLCODE
                            DELIMITED BY SIZE
                                             INTO IEAI9901-PARAMETRI-ERR
                            END-STRING
                            PERFORM S0300-RICERCA-GRAVITA-ERRORE
                               THRU EX-S0300
                     END-EVALUATE
                   ELSE
      *-->    GESTIRE ERRORE DISPATCHER
                      MOVE WK-PGM
                        TO IEAI9901-COD-SERVIZIO-BE
                      MOVE 'FETCH-PMO'
                        TO IEAI9901-LABEL-ERR
                      MOVE '005016'
                        TO IEAI9901-COD-ERRORE
                      STRING 'PARAM-MOVI'                ';'
                             IDSO0011-RETURN-CODE ';'
                             IDSO0011-SQLCODE
                      DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                      END-STRING
                      PERFORM S0300-RICERCA-GRAVITA-ERRORE
                         THRU EX-S0300
                   END-IF
             END-PERFORM.

       FETCH-PMO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   PROCESSO DI VENDITA - PORTAFOGLIO VITA
      *   AREA VALORIZZAZIONE AREA VARIABILE DI INPUT
      *   SERVIZIO ESTRAZIONE NUMERAZIONE OGGETTO
      *----------------------------------------------------------------*
       VALORIZZA-AREE-S0088.

           MOVE ZERO                        TO (SF)-IX-CONTA.
           MOVE 1                           TO (SF)-APPO-LUNGHEZZA.

      *--> POLIZZA
           IF WPOL-ELE-POLI-MAX GREATER ZEROES
              ADD  1                      TO (SF)-IX-CONTA
              MOVE (SF)-ALIAS-POLI
                TO (SF)-TAB-ALIAS((SF)-IX-CONTA)
              MOVE WPOL-ELE-POLI-MAX
                TO (SF)-NUM-OCCORRENZE((SF)-IX-CONTA)
              MOVE (SF)-APPO-LUNGHEZZA
                TO (SF)-POSIZ-INI((SF)-IX-CONTA)
              MOVE LENGTH OF WPOL-AREA-POLIZZA
                TO (SF)-LUNGHEZZA((SF)-IX-CONTA)
              COMPUTE (SF)-APPO-LUNGHEZZA = (SF)-APPO-LUNGHEZZA +
                      (SF)-LUNGHEZZA((SF)-IX-CONTA) + 1
      *-->     Valorizzazione del buffer dati

              MOVE WPOL-AREA-POLIZZA
                TO (SF)-BUFFER-DATI((SF)-POSIZ-INI((SF)-IX-CONTA):
                                    (SF)-LUNGHEZZA((SF)-IX-CONTA))
           END-IF.

      *--> ADESIONE
           IF WADE-ELE-ADES-MAX GREATER ZEROES
              ADD 1                         TO (SF)-IX-CONTA
              MOVE (SF)-ALIAS-ADES
                TO (SF)-TAB-ALIAS((SF)-IX-CONTA)
              MOVE WADE-ELE-ADES-MAX
                TO (SF)-NUM-OCCORRENZE((SF)-IX-CONTA)
              MOVE (SF)-APPO-LUNGHEZZA
                TO (SF)-POSIZ-INI((SF)-IX-CONTA)
              MOVE LENGTH OF WADE-AREA-ADESIONE
                TO (SF)-LUNGHEZZA((SF)-IX-CONTA)

              COMPUTE (SF)-APPO-LUNGHEZZA = (SF)-APPO-LUNGHEZZA +
                      (SF)-LUNGHEZZA((SF)-IX-CONTA) + 1
      *-->  Valorizzazione del buffer dati
              MOVE WADE-AREA-ADESIONE
                TO (SF)-BUFFER-DATI((SF)-POSIZ-INI((SF)-IX-CONTA):
                                    (SF)-LUNGHEZZA((SF)-IX-CONTA))
           END-IF.

      *--> RAPPORTO RETE
           IF WRRE-ELE-RAPP-RETE-MAX GREATER ZEROES
              ADD 1                         TO (SF)-IX-CONTA
              MOVE (SF)-ALIAS-RAPP-RETE
                TO (SF)-TAB-ALIAS((SF)-IX-CONTA)
              MOVE WRRE-ELE-RAPP-RETE-MAX
                TO (SF)-NUM-OCCORRENZE((SF)-IX-CONTA)
              MOVE (SF)-APPO-LUNGHEZZA
                TO (SF)-POSIZ-INI((SF)-IX-CONTA)
              MOVE LENGTH OF WRRE-AREA-RAPP-RETE
                TO (SF)-LUNGHEZZA((SF)-IX-CONTA)

              COMPUTE (SF)-APPO-LUNGHEZZA = (SF)-APPO-LUNGHEZZA +
                      (SF)-LUNGHEZZA((SF)-IX-CONTA) + 1
      *-->    Valorizzazione del buffer dati
              MOVE WRRE-AREA-RAPP-RETE
                TO (SF)-BUFFER-DATI((SF)-POSIZ-INI((SF)-IX-CONTA):
                                   (SF)-LUNGHEZZA((SF)-IX-CONTA))
           END-IF.

           MOVE (SF)-IX-CONTA                TO (SF)-ELE-INFO-MAX.

       VALORIZZA-AREE-S0088-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVRIC5
      *   ULTIMO AGG. 21 NOV 2013
      *------------------------------------------------------------

       VAL-DCLGEN-RIC.
           MOVE (SF)-COD-COMP-ANIA
              TO RIC-COD-COMP-ANIA
           MOVE (SF)-TP-RICH
              TO RIC-TP-RICH
           MOVE (SF)-COD-MACROFUNCT
              TO RIC-COD-MACROFUNCT
           MOVE (SF)-TP-MOVI
              TO RIC-TP-MOVI
           IF (SF)-IB-RICH-NULL = HIGH-VALUES
              MOVE (SF)-IB-RICH-NULL
              TO RIC-IB-RICH-NULL
           ELSE
              MOVE (SF)-IB-RICH
              TO RIC-IB-RICH
           END-IF
           MOVE (SF)-DT-EFF
              TO RIC-DT-EFF
           MOVE (SF)-DT-RGSTRZ-RICH
              TO RIC-DT-RGSTRZ-RICH
           MOVE (SF)-DT-PERV-RICH
              TO RIC-DT-PERV-RICH
           MOVE (SF)-DT-ESEC-RICH
              TO RIC-DT-ESEC-RICH
           IF (SF)-TS-EFF-ESEC-RICH-NULL = HIGH-VALUES
              MOVE (SF)-TS-EFF-ESEC-RICH-NULL
              TO RIC-TS-EFF-ESEC-RICH-NULL
           ELSE
              MOVE (SF)-TS-EFF-ESEC-RICH
              TO RIC-TS-EFF-ESEC-RICH
           END-IF
           IF (SF)-TP-OGG-NULL = HIGH-VALUES
              MOVE (SF)-TP-OGG-NULL
              TO RIC-TP-OGG-NULL
           ELSE
              MOVE (SF)-TP-OGG
              TO RIC-TP-OGG
           END-IF
           IF (SF)-IB-POLI-NULL = HIGH-VALUES
              MOVE (SF)-IB-POLI-NULL
              TO RIC-IB-POLI-NULL
           ELSE
              MOVE (SF)-IB-POLI
              TO RIC-IB-POLI
           END-IF
           IF (SF)-IB-ADES-NULL = HIGH-VALUES
              MOVE (SF)-IB-ADES-NULL
              TO RIC-IB-ADES-NULL
           ELSE
              MOVE (SF)-IB-ADES
              TO RIC-IB-ADES
           END-IF
           IF (SF)-IB-GAR-NULL = HIGH-VALUES
              MOVE (SF)-IB-GAR-NULL
              TO RIC-IB-GAR-NULL
           ELSE
              MOVE (SF)-IB-GAR
              TO RIC-IB-GAR
           END-IF
           IF (SF)-IB-TRCH-DI-GAR-NULL = HIGH-VALUES
              MOVE (SF)-IB-TRCH-DI-GAR-NULL
              TO RIC-IB-TRCH-DI-GAR-NULL
           ELSE
              MOVE (SF)-IB-TRCH-DI-GAR
              TO RIC-IB-TRCH-DI-GAR
           END-IF
           IF (SF)-ID-BATCH-NULL = HIGH-VALUES
              MOVE (SF)-ID-BATCH-NULL
              TO RIC-ID-BATCH-NULL
           ELSE
              MOVE (SF)-ID-BATCH
              TO RIC-ID-BATCH
           END-IF
           IF (SF)-ID-JOB-NULL = HIGH-VALUES
              MOVE (SF)-ID-JOB-NULL
              TO RIC-ID-JOB-NULL
           ELSE
              MOVE (SF)-ID-JOB
              TO RIC-ID-JOB
           END-IF
           IF (SF)-FL-SIMULAZIONE-NULL = HIGH-VALUES
              MOVE (SF)-FL-SIMULAZIONE-NULL
              TO RIC-FL-SIMULAZIONE-NULL
           ELSE
              MOVE (SF)-FL-SIMULAZIONE
              TO RIC-FL-SIMULAZIONE
           END-IF
           MOVE (SF)-KEY-ORDINAMENTO
              TO RIC-KEY-ORDINAMENTO
           IF (SF)-ID-RICH-COLLG-NULL = HIGH-VALUES
              MOVE (SF)-ID-RICH-COLLG-NULL
              TO RIC-ID-RICH-COLLG-NULL
           ELSE
              MOVE (SF)-ID-RICH-COLLG
              TO RIC-ID-RICH-COLLG
           END-IF
           IF (SF)-TP-RAMO-BILA-NULL = HIGH-VALUES
              MOVE (SF)-TP-RAMO-BILA-NULL
              TO RIC-TP-RAMO-BILA-NULL
           ELSE
              MOVE (SF)-TP-RAMO-BILA
              TO RIC-TP-RAMO-BILA
           END-IF
           IF (SF)-TP-FRM-ASSVA-NULL = HIGH-VALUES
              MOVE (SF)-TP-FRM-ASSVA-NULL
              TO RIC-TP-FRM-ASSVA-NULL
           ELSE
              MOVE (SF)-TP-FRM-ASSVA
              TO RIC-TP-FRM-ASSVA
           END-IF
           IF (SF)-TP-CALC-RIS-NULL = HIGH-VALUES
              MOVE (SF)-TP-CALC-RIS-NULL
              TO RIC-TP-CALC-RIS-NULL
           ELSE
              MOVE (SF)-TP-CALC-RIS
              TO RIC-TP-CALC-RIS
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO RIC-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO RIC-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO RIC-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO RIC-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO RIC-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO RIC-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO RIC-DS-STATO-ELAB
           IF (SF)-RAMO-BILA-NULL = HIGH-VALUES
              MOVE (SF)-RAMO-BILA-NULL
              TO RIC-RAMO-BILA-NULL
           ELSE
              MOVE (SF)-RAMO-BILA
              TO RIC-RAMO-BILA
           END-IF.
       VAL-DCLGEN-RIC-EX.
           EXIT.

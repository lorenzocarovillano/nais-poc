           EXEC SQL DECLARE TRCH_DI_GAR TABLE
           (
             ID_TRCH_DI_GAR      DECIMAL(9, 0) NOT NULL,
             ID_GAR              DECIMAL(9, 0) NOT NULL,
             ID_ADES             DECIMAL(9, 0) NOT NULL,
             ID_POLI             DECIMAL(9, 0) NOT NULL,
             ID_MOVI_CRZ         DECIMAL(9, 0) NOT NULL,
             ID_MOVI_CHIU        DECIMAL(9, 0),
             DT_INI_EFF          DATE NOT NULL,
             DT_END_EFF          DATE NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             DT_DECOR            DATE NOT NULL,
             DT_SCAD             DATE,
             IB_OGG              CHAR(40),
             TP_RGM_FISC         CHAR(2) NOT NULL,
             DT_EMIS             DATE,
             TP_TRCH             CHAR(2) NOT NULL,
             DUR_AA              DECIMAL(5, 0),
             DUR_MM              DECIMAL(5, 0),
             DUR_GG              DECIMAL(5, 0),
             PRE_CASO_MOR        DECIMAL(15, 3),
             PC_INTR_RIAT        DECIMAL(6, 3),
             IMP_BNS_ANTIC       DECIMAL(15, 3),
             PRE_INI_NET         DECIMAL(15, 3),
             PRE_PP_INI          DECIMAL(15, 3),
             PRE_PP_ULT          DECIMAL(15, 3),
             PRE_TARI_INI        DECIMAL(15, 3),
             PRE_TARI_ULT        DECIMAL(15, 3),
             PRE_INVRIO_INI      DECIMAL(15, 3),
             PRE_INVRIO_ULT      DECIMAL(15, 3),
             PRE_RIVTO           DECIMAL(15, 3),
             IMP_SOPR_PROF       DECIMAL(15, 3),
             IMP_SOPR_SAN        DECIMAL(15, 3),
             IMP_SOPR_SPO        DECIMAL(15, 3),
             IMP_SOPR_TEC        DECIMAL(15, 3),
             IMP_ALT_SOPR        DECIMAL(15, 3),
             PRE_STAB            DECIMAL(15, 3),
             DT_EFF_STAB         DATE,
             TS_RIVAL_FIS        DECIMAL(14, 9),
             TS_RIVAL_INDICIZ    DECIMAL(14, 9),
             OLD_TS_TEC          DECIMAL(14, 9),
             RAT_LRD             DECIMAL(15, 3),
             PRE_LRD             DECIMAL(15, 3),
             PRSTZ_INI           DECIMAL(15, 3),
             PRSTZ_ULT           DECIMAL(15, 3),
             CPT_IN_OPZ_RIVTO    DECIMAL(15, 3),
             PRSTZ_INI_STAB      DECIMAL(15, 3),
             CPT_RSH_MOR         DECIMAL(15, 3),
             PRSTZ_RID_INI       DECIMAL(15, 3),
             FL_CAR_CONT         CHAR(1),
             BNS_GIA_LIQTO       DECIMAL(15, 3),
             IMP_BNS             DECIMAL(15, 3),
             COD_DVS             CHAR(20) NOT NULL,
             PRSTZ_INI_NEWFIS    DECIMAL(15, 3),
             IMP_SCON            DECIMAL(15, 3),
             ALQ_SCON            DECIMAL(6, 3),
             IMP_CAR_ACQ         DECIMAL(15, 3),
             IMP_CAR_INC         DECIMAL(15, 3),
             IMP_CAR_GEST        DECIMAL(15, 3),
             ETA_AA_1O_ASSTO     DECIMAL(3, 0),
             ETA_MM_1O_ASSTO     DECIMAL(3, 0),
             ETA_AA_2O_ASSTO     DECIMAL(3, 0),
             ETA_MM_2O_ASSTO     DECIMAL(3, 0),
             ETA_AA_3O_ASSTO     DECIMAL(3, 0),
             ETA_MM_3O_ASSTO     DECIMAL(3, 0),
             RENDTO_LRD          DECIMAL(14, 9),
             PC_RETR             DECIMAL(6, 3),
             RENDTO_RETR         DECIMAL(14, 9),
             MIN_GARTO           DECIMAL(14, 9),
             MIN_TRNUT           DECIMAL(14, 9),
             PRE_ATT_DI_TRCH     DECIMAL(15, 3),
             MATU_END2000        DECIMAL(15, 3),
             ABB_TOT_INI         DECIMAL(15, 3),
             ABB_TOT_ULT         DECIMAL(15, 3),
             ABB_ANNU_ULT        DECIMAL(15, 3),
             DUR_ABB             DECIMAL(6, 0),
             TP_ADEG_ABB         CHAR(1),
             MOD_CALC            CHAR(2),
             IMP_AZ              DECIMAL(15, 3),
             IMP_ADER            DECIMAL(15, 3),
             IMP_TFR             DECIMAL(15, 3),
             IMP_VOLO            DECIMAL(15, 3),
             VIS_END2000         DECIMAL(15, 3),
             DT_VLDT_PROD        DATE,
             DT_INI_VAL_TAR      DATE,
             IMPB_VIS_END2000    DECIMAL(15, 3),
             REN_INI_TS_TEC_0    DECIMAL(15, 3),
             PC_RIP_PRE          DECIMAL(6, 3),
             FL_IMPORTI_FORZ     CHAR(1),
             PRSTZ_INI_NFORZ     DECIMAL(15, 3),
             VIS_END2000_NFORZ   DECIMAL(15, 3),
             INTR_MORA           DECIMAL(15, 3),
             MANFEE_ANTIC        DECIMAL(15, 3),
             MANFEE_RICOR        DECIMAL(15, 3),
             PRE_UNI_RIVTO       DECIMAL(15, 3),
             PROV_1AA_ACQ        DECIMAL(15, 3),
             PROV_2AA_ACQ        DECIMAL(15, 3),
             PROV_RICOR          DECIMAL(15, 3),
             PROV_INC            DECIMAL(15, 3),
             ALQ_PROV_ACQ        DECIMAL(6, 3),
             ALQ_PROV_INC        DECIMAL(6, 3),
             ALQ_PROV_RICOR      DECIMAL(6, 3),
             IMPB_PROV_ACQ       DECIMAL(15, 3),
             IMPB_PROV_INC       DECIMAL(15, 3),
             IMPB_PROV_RICOR     DECIMAL(15, 3),
             FL_PROV_FORZ        CHAR(1),
             PRSTZ_AGG_INI       DECIMAL(15, 3),
             INCR_PRE            DECIMAL(15, 3),
             INCR_PRSTZ          DECIMAL(15, 3),
             DT_ULT_ADEG_PRE_PR  DATE,
             PRSTZ_AGG_ULT       DECIMAL(15, 3),
             TS_RIVAL_NET        DECIMAL(14, 9),
             PRE_PATTUITO        DECIMAL(15, 3),
             TP_RIVAL            CHAR(2),
             RIS_MAT             DECIMAL(15, 3),
             CPT_MIN_SCAD        DECIMAL(15, 3),
             COMMIS_GEST         DECIMAL(15, 3),
             TP_MANFEE_APPL      CHAR(2),
             DS_RIGA             DECIMAL(10, 0) NOT NULL WITH DEFAULT,
             DS_OPER_SQL         CHAR(1) NOT NULL WITH DEFAULT,
             DS_VER              DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             DS_TS_INI_CPTZ      DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_TS_END_CPTZ      DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_UTENTE           CHAR(20) NOT NULL WITH DEFAULT,
             DS_STATO_ELAB       CHAR(1) NOT NULL WITH DEFAULT,
             PC_COMMIS_GEST      DECIMAL(6, 3),
             NUM_GG_RIVAL        DECIMAL(5, 0),
             IMP_TRASFE          DECIMAL(15, 3),
             IMP_TFR_STRC        DECIMAL(15, 3),
             ACQ_EXP             DECIMAL(15, 3),
             REMUN_ASS           DECIMAL(15, 3),
             COMMIS_INTER        DECIMAL(15, 3),
             ALQ_REMUN_ASS       DECIMAL(6, 3),
             ALQ_COMMIS_INTER    DECIMAL(6, 3),
             IMPB_REMUN_ASS      DECIMAL(15, 3),
             IMPB_COMMIS_INTER   DECIMAL(15, 3),
             COS_RUN_ASSVA       DECIMAL(15, 3),
             COS_RUN_ASSVA_IDC   DECIMAL(15, 3)
          ) END-EXEC.

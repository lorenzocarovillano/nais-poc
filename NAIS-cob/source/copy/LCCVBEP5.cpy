
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVBEP5
      *   ULTIMO AGG. 09 AGO 2018
      *------------------------------------------------------------

       VAL-DCLGEN-BEP.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-BEP) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-BEP)
              TO BEP-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-BEP)
              TO BEP-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-BEP) NOT NUMERIC
              MOVE 0 TO BEP-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-BEP)
              TO BEP-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-BEP) NOT NUMERIC
              MOVE 0 TO BEP-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-BEP)
              TO BEP-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-BEP)
              TO BEP-COD-COMP-ANIA
           IF (SF)-COD-BNFIC-NULL(IX-TAB-BEP) = HIGH-VALUES
              MOVE (SF)-COD-BNFIC-NULL(IX-TAB-BEP)
              TO BEP-COD-BNFIC-NULL
           ELSE
              MOVE (SF)-COD-BNFIC(IX-TAB-BEP)
              TO BEP-COD-BNFIC
           END-IF
           MOVE (SF)-TP-IND-BNFICR(IX-TAB-BEP)
              TO BEP-TP-IND-BNFICR
           IF (SF)-COD-BNFICR-NULL(IX-TAB-BEP) = HIGH-VALUES
              MOVE (SF)-COD-BNFICR-NULL(IX-TAB-BEP)
              TO BEP-COD-BNFICR-NULL
           ELSE
              MOVE (SF)-COD-BNFICR(IX-TAB-BEP)
              TO BEP-COD-BNFICR
           END-IF
           MOVE (SF)-DESC-BNFICR(IX-TAB-BEP)
              TO BEP-DESC-BNFICR
           IF (SF)-PC-DEL-BNFICR-NULL(IX-TAB-BEP) = HIGH-VALUES
              MOVE (SF)-PC-DEL-BNFICR-NULL(IX-TAB-BEP)
              TO BEP-PC-DEL-BNFICR-NULL
           ELSE
              MOVE (SF)-PC-DEL-BNFICR(IX-TAB-BEP)
              TO BEP-PC-DEL-BNFICR
           END-IF
           IF (SF)-FL-ESE-NULL(IX-TAB-BEP) = HIGH-VALUES
              MOVE (SF)-FL-ESE-NULL(IX-TAB-BEP)
              TO BEP-FL-ESE-NULL
           ELSE
              MOVE (SF)-FL-ESE(IX-TAB-BEP)
              TO BEP-FL-ESE
           END-IF
           IF (SF)-FL-IRREV-NULL(IX-TAB-BEP) = HIGH-VALUES
              MOVE (SF)-FL-IRREV-NULL(IX-TAB-BEP)
              TO BEP-FL-IRREV-NULL
           ELSE
              MOVE (SF)-FL-IRREV(IX-TAB-BEP)
              TO BEP-FL-IRREV
           END-IF
           IF (SF)-FL-DFLT-NULL(IX-TAB-BEP) = HIGH-VALUES
              MOVE (SF)-FL-DFLT-NULL(IX-TAB-BEP)
              TO BEP-FL-DFLT-NULL
           ELSE
              MOVE (SF)-FL-DFLT(IX-TAB-BEP)
              TO BEP-FL-DFLT
           END-IF
           IF (SF)-ESRCN-ATTVT-IMPRS-NULL(IX-TAB-BEP) = HIGH-VALUES
              MOVE (SF)-ESRCN-ATTVT-IMPRS-NULL(IX-TAB-BEP)
              TO BEP-ESRCN-ATTVT-IMPRS-NULL
           ELSE
              MOVE (SF)-ESRCN-ATTVT-IMPRS(IX-TAB-BEP)
              TO BEP-ESRCN-ATTVT-IMPRS
           END-IF
           MOVE (SF)-FL-BNFICR-COLL(IX-TAB-BEP)
              TO BEP-FL-BNFICR-COLL
           IF (SF)-DS-RIGA(IX-TAB-BEP) NOT NUMERIC
              MOVE 0 TO BEP-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-BEP)
              TO BEP-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-BEP)
              TO BEP-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-BEP) NOT NUMERIC
              MOVE 0 TO BEP-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-BEP)
              TO BEP-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-BEP) NOT NUMERIC
              MOVE 0 TO BEP-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-BEP)
              TO BEP-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-BEP) NOT NUMERIC
              MOVE 0 TO BEP-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-BEP)
              TO BEP-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-BEP)
              TO BEP-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-BEP)
              TO BEP-DS-STATO-ELAB
           IF (SF)-TP-NORMAL-BNFIC-NULL(IX-TAB-BEP) = HIGH-VALUES
              MOVE (SF)-TP-NORMAL-BNFIC-NULL(IX-TAB-BEP)
              TO BEP-TP-NORMAL-BNFIC-NULL
           ELSE
              MOVE (SF)-TP-NORMAL-BNFIC(IX-TAB-BEP)
              TO BEP-TP-NORMAL-BNFIC
           END-IF.
       VAL-DCLGEN-BEP-EX.
           EXIT.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA MOVI_FINRIO
      *   ALIAS MFZ
      *   ULTIMO AGG. 09 LUG 2010
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-MOVI-FINRIO PIC S9(9)     COMP-3.
             07 (SF)-ID-ADES PIC S9(9)     COMP-3.
             07 (SF)-ID-ADES-NULL REDEFINES
                (SF)-ID-ADES   PIC X(5).
             07 (SF)-ID-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-LIQ-NULL REDEFINES
                (SF)-ID-LIQ   PIC X(5).
             07 (SF)-ID-TIT-CONT PIC S9(9)     COMP-3.
             07 (SF)-ID-TIT-CONT-NULL REDEFINES
                (SF)-ID-TIT-CONT   PIC X(5).
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-TP-MOVI-FINRIO PIC X(2).
             07 (SF)-DT-EFF-MOVI-FINRIO   PIC S9(8) COMP-3.
             07 (SF)-DT-EFF-MOVI-FINRIO-NULL REDEFINES
                (SF)-DT-EFF-MOVI-FINRIO   PIC X(5).
             07 (SF)-DT-ELAB   PIC S9(8) COMP-3.
             07 (SF)-DT-ELAB-NULL REDEFINES
                (SF)-DT-ELAB   PIC X(5).
             07 (SF)-DT-RICH-MOVI   PIC S9(8) COMP-3.
             07 (SF)-DT-RICH-MOVI-NULL REDEFINES
                (SF)-DT-RICH-MOVI   PIC X(5).
             07 (SF)-COS-OPRZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-COS-OPRZ-NULL REDEFINES
                (SF)-COS-OPRZ   PIC X(8).
             07 (SF)-STAT-MOVI PIC X(2).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-TP-CAUS-RETTIFICA PIC X(2).
             07 (SF)-TP-CAUS-RETTIFICA-NULL REDEFINES
                (SF)-TP-CAUS-RETTIFICA   PIC X(2).

      ******************************************************************
      *    CALL VALORIZZATORE VARIABILI
      ******************************************************************
       CALL-VALORIZZATORE.

           MOVE IDSV0001-MODALITA-ESECUTIVA
                TO (SF)-MODALITA-ESECUTIVA

           IF (SF)-COD-COMPAGNIA-ANIA = 0
              MOVE IDSV0001-COD-COMPAGNIA-ANIA
                TO (SF)-COD-COMPAGNIA-ANIA
           END-IF

           IF (SF)-TIPO-MOVIMENTO = 0
              MOVE IDSV0001-TIPO-MOVIMENTO
                TO (SF)-TIPO-MOVIMENTO
           END-IF

           IF (SF)-COD-MAIN-BATCH = SPACES
              MOVE IDSV0001-COD-MAIN-BATCH
                TO (SF)-COD-MAIN-BATCH
           END-IF

           IF (SF)-COD-SERVIZIO-BE = SPACES
              MOVE IDSV0001-COD-SERVIZIO-BE
                TO (SF)-COD-SERVIZIO-BE
           END-IF

           IF (SF)-DATA-EFFETTO = 0
              MOVE IDSV0001-DATA-EFFETTO
                TO (SF)-DATA-EFFETTO
           END-IF

           IF (SF)-DATA-COMPETENZA = 0
              MOVE IDSV0001-DATA-COMPETENZA
                TO (SF)-DATA-COMPETENZA
           END-IF

           MOVE IDSV0001-DATA-COMP-AGG-STOR
                TO (SF)-DATA-COMP-AGG-STOR

           MOVE IDSV0001-TRATTAMENTO-STORICITA
                TO (SF)-TRATTAMENTO-STORICITA

           MOVE IDSV0001-FORMATO-DATA-DB
                TO (SF)-FORMATO-DATA-DB

           MOVE IDSV0001-AREA-ADDRESSES
                TO (SF)-AREA-ADDRESSES

           MOVE 'IVVS0211' TO (SF)-PGM
           CALL (SF)-PGM USING AREA-IO-IVVS0211
           ON EXCEPTION
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'CALL VALORIZZATORE'
                    TO CALL-DESC
                  MOVE 'CALL-VALORIZZATORE'
                    TO IEAI9901-LABEL-ERR
                   PERFORM S0290-ERRORE-DI-SISTEMA
                      THRU EX-S0290
           END-CALL.

           IF NOT (SF)-SUCCESSFUL-RC
              IF (SF)-CALCOLO-NON-COMPLETO
      *
                 IF (SF)-ELE-MAX-NOT-FOUND GREATER ZEROES
                    MOVE (SF)-PGM
                      TO IEAI9901-COD-SERVIZIO-BE
                    MOVE 'CALL-VALORIZZATORE'
                      TO IEAI9901-LABEL-ERR
                    MOVE (SF)-AREA-VAR-NOT-FOUND
                      TO IEAI9901-PARAMETRI-ERR
                    MOVE '005236'
                      TO IEAI9901-COD-ERRORE
                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
                       THRU EX-S0300
                 END-IF
      *
                 IF (SF)-ELE-MAX-CALC-KO GREATER ZEROES
                    MOVE (SF)-PGM
                      TO IEAI9901-COD-SERVIZIO-BE
                    MOVE 'CALL-VALORIZZATORE'
                      TO IEAI9901-LABEL-ERR
                    MOVE (SF)-AREA-CALC-KO
                      TO IEAI9901-PARAMETRI-ERR
                    MOVE '005237'
                      TO IEAI9901-COD-ERRORE
                    PERFORM S0300-RICERCA-GRAVITA-ERRORE
                       THRU EX-S0300
                 END-IF
              ELSE
                IF (SF)-CACHE-PIENA
                 MOVE (SF)-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'CALL-VALORIZZATORE'
                   TO IEAI9901-LABEL-ERR
                 MOVE SPACES
                   TO IEAI9901-PARAMETRI-ERR
                 MOVE '005283'
                   TO IEAI9901-COD-ERRORE
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
                ELSE
                 IF (SF)-GLOVARLIST-VUOTA
NEWP             OR (SF)-SKIP-CALL-ACTUATOR
                    CONTINUE
                 ELSE
                   MOVE (SF)-COD-SERVIZIO-BE
                                             TO IEAI9901-COD-SERVIZIO-BE
                   MOVE 'CALL-VALORIZZATORE' TO IEAI9901-LABEL-ERR
                   MOVE '001114'             TO IEAI9901-COD-ERRORE
                   STRING (SF)-DESCRIZ-ERR  DELIMITED BY '     '
                           ' - '
                          (SF)-NOME-TABELLA DELIMITED BY SIZE
                   INTO IEAI9901-PARAMETRI-ERR
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
                END-IF
              END-IF
           END-IF.

       CALL-VALORIZZATORE-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   PROCESSO DI VENDITA - PORTAFOGLIO VITA
      *   AREA INPUT/OUTPUT
      *   SERVIZIO ESTRAZIONE NUMERAZIONE OGGETTO
      *----------------------------------------------------------------*
      *-- STRUTTURA DATI DI INPUT
           05 (SF)-DATI-INPUT.
      *-- CODICE OGGETTO
              10 (SF)-COD-OGGETTO                   PIC X(30).
                 88 (SF)-NUM-PROP      VALUE 'NUMERO-PROPOSTA'.
                 88 (SF)-NUM-POLI      VALUE 'NUMERO-POLIZZA'.
                 88 (SF)-IDEN-ESTNO    VALUE 'IDENTIFICATIVO-ESTERNO'.
                 88 (SF)-NUM-RICH      VALUE 'NUMERO-RICHIESTA'.
                 88 (SF)-NUM-PREV      VALUE 'NUMERO-PREVENTIVO'.
                 88 (SF)-NUM-LIQ       VALUE 'NUMERO-LIQUIDAZIONE'.
                 88 (SF)-NUM-QUIET     VALUE 'NUMERO-QUIETANZA'.
                 88 (SF)-IDEN-DEROG    VALUE 'IDENTIFICATIVO-DEROGA'.
                 88 (SF)-NUM-REND      VALUE 'NUMERO-RENDITA'.
                 88 (SF)-NUM-RICH-EST  VALUE 'NUMERO-RICHIESTA-EST'.
                 88 (SF)-NUM-RICH-EST-CNL-03
                                       VALUE 'NUM-RICH-EST-CNL-03'.
                 88 (SF)-NUM-RICH-EST-CNL-04
                                       VALUE 'NUM-RICH-EST-CNL-04'.
23150            88 (SF)-NUM-COM       VALUE 'NUMERO-COMUNICAZIONE'.
                 88 (SF)-NUM-POLI-C03  VALUE 'NUMERO-POLIZZA-CNL-03'.
                 88 (SF)-NUM-POLI-C04  VALUE 'NUMERO-POLIZZA-CNL-04'.
ITREG            88 (SF)-NUM-POLI-CPI  VALUE 'NUMERO-POLIZZA-CPI'.
ITREG            88 (SF)-VER-POLI-CPI  VALUE 'VERIFI-POLIZZA-CPI'.
13382            88 (SF)-COD-AUTORIZ   VALUE 'CODICE-AUTORIZZAZIONE'.
FNZF2            88 (SF)-FNZ-EXT-ACCOUNT-ID  VALUE 'FNZ-EXT-ACCOUNT-ID'.
FNZF2            88 (SF)-FNZ-EXT-ORDER-ID  VALUE 'FNZ-EXT-ORDER-ID'.
15592            88 (SF)-NUM-IB-RICH-EST   VALUE 'NUM-IB-RICH-EST'.
15592A           88 (SF)-NUM-RICH-EST-C7A  VALUE 'NUM-RICH-EST-C7A'.
MIGCOL           88 (SF)-NUM-ADES-COLL     VALUE 'NUM-ADES-COLL'.
MIGCOL           88 (SF)-NUM-TRCH-COLL     VALUE 'NUM-TRCH-COLL'.

      *
      *-- TIPO NUMERAZIONE
              10 (SF)-TIPO-NUMERAZIONE              PIC X(01).
                 88 (SF)-MANUALE       VALUE 'M'.
                 88 (SF)-AUTOMATICA    VALUE 'A'.
      *-- IB OGGETTO PASSATO IN INPUT X TIPO NUMERAZIONE MANUALE
              10 (SF)-IB-OGGETTO-I                  PIC X(40).
      *-- STRUTTURA DI MAPPING DELLE AREE PASSATE IN INPUT
              10 (SF)-STRUTTURA-MAPPING-AREE.
                 15 (SF)-IX-CONTA                   PIC 9(04)  COMP.
                 15 (SF)-APPO-LUNGHEZZA             PIC S9(09) COMP-3.
                 15 (SF)-ELE-INFO-MAX               PIC S9(04) COMP-3.
                 15 (SF)-TAB-INFO1.
                  17 (SF)-TAB-INFO                  OCCURS 100
                                            INDEXED BY (SF)-SEARCH-IND.
                     20 (SF)-TAB-ALIAS              PIC X(03).
                     20 (SF)-NUM-OCCORRENZE         PIC S9(05) COMP-3.
                     20 (SF)-POSIZ-INI              PIC S9(09) COMP-3.
                     20 (SF)-LUNGHEZZA              PIC S9(09) COMP-3.
                 15 (SF)-TAB-INFO1-R REDEFINES (SF)-TAB-INFO1.
                     20  FILLER                     PIC X(16).
                     20  (SF)-RESTO-TAB-INFO1       PIC X(1584).
                 15 (SF)-BUFFER-DATI                PIC X(10000).
      *-- STRUTTURA DATI DI OUTPUT
           05 (SF)-DATI-OUTPUT.
      *-- IB OGGETTO RESTITUITO IN OUTPUT
              10 (SF)-IB-OGGETTO                    PIC X(40).

       01  AREA-PRODUCT-SERVICES.
           05  IJCCMQ02-HEADER.
               10  IJCCMQ02-MODALITA-ESECUTIVA           PIC X(001).
                   88  IJCCMQ02-ON-LINE                  VALUE 'O'.
                   88  IJCCMQ02-BATCH                    VALUE 'B'.
                   88  IJCCMQ02-BATCH-INFR               VALUE 'I'.
               10  IJCCMQ02-JAVA-SERVICE-NAME            PIC X(100).
               10  IJCCMQ02-INFO-FRAMES.
                   15  IJCCMQ02-ID-APPLICATION           PIC X(26).
                   15  IJCCMQ02-TOT-NUM-FRAMES           PIC S9(4) COMP.
                   15  IJCCMQ02-NUM-FRAME                PIC S9(4) COMP.
                   15  IJCCMQ02-LENGTH-DATA-FRAME        PIC S9(9) COMP.
                   15  IJCCMQ02-CALL-METHOD              PIC X(01).
                       88 IJCCMQ02-SERVICE-INVOCATION    VALUE 'S'.
                       88 IJCCMQ02-DATA-REQUEST          VALUE 'D'.
               10  IJCCMQ02-ESITO                        PIC X(002).
                   88  IJCCMQ02-ESITO-OK                 VALUE 'OK'.
                   88  IJCCMQ02-ESITO-KO                 VALUE 'KO'.
               10  IJCCMQ02-LENGTH-DATI-SERVIZIO         PIC S9(9) COMP.
               10  IJCCMQ02-MAX-ELE-ERRORI               PIC S9(4) COMP.
               10  IJCCMQ02-TAB-ERRORI-FRONT-END.
                   15  IJCCMQ02-ELE-ERRORI               OCCURS 10.
                       20  IJCCMQ02-DESC-ERRORE          PIC X(200).
                       20  IJCCMQ02-COD-ERRORE           PIC 9(006).
                       20  IJCCMQ02-LIV-GRAVITA-BE       PIC 9(001).
                       20  IJCCMQ02-TIPO-TRATT-FE        PIC X(001).
               10  IJCCMQ02-LENGTH-DATI-OUTPUT           PIC S9(9) COMP.
               10  IJCCMQ02-VAR-AMBIENTE                 PIC X(224).
               10  IJCCMQ02-AREA-ADDRESSES.
                   15 IJCCMQ02-ADDRESSES                OCCURS 5 TIMES.
                       20 IJCCMQ02-ADDRESS-TYPE          PIC X(01).
                       20 IJCCMQ02-ADDRESS               POINTER.
               10 IJCCMQ02-USER-NAME                     PIC X(020).
               10  FILLER                                PIC X(002).
               10 IJCCMQ00-LIVELLO-DEBUG                 PIC 9(001).
                  88 IJCCMQ00-NO-DEBUG                   VALUE 0.
                  88 IJCCMQ00-DEBUG-BASSO                VALUE 1.
                  88 IJCCMQ00-DEBUG-MEDIO                VALUE 2.
                  88 IJCCMQ00-DEBUG-ELEVATO              VALUE 3.
                  88 IJCCMQ00-DEBUG-ESASPERATO           VALUE 4.
                  88 IJCCMQ00-ANY-APPL-DBG               VALUE 1,
                                                               2,
                                                               3,
                                                               4.

                  88 IJCCMQ00-ARCH-BATCH-DBG             VALUE 5.
                  88 IJCCMQ00-COM-COB-JAV-DBG            VALUE 6.
                  88 IJCCMQ00-STRESS-TEST-DBG            VALUE 7.
                  88 IJCCMQ00-BUSINESS-DBG               VALUE 8.
                  88 IJCCMQ00-TOT-TUNING-DBG             VALUE 9.
                  88 IJCCMQ00-ANY-TUNING-DBG             VALUE 5,
                                                               6,
                                                               7,
                                                               8,
                                                               9.
            05  IJCCMQ02-AREA-DATI-SERVIZIO.
               10  IJCCMQ02-CAR  PIC X(001)
                             OCCURS 1 TO 1000000 TIMES
                             DEPENDING ON IJCCMQ02-LENGTH-DATI-SERVIZIO.

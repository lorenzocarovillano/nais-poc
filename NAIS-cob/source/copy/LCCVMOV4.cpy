
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVMOV4
      *   ULTIMO AGG. 02 SET 2008
      *------------------------------------------------------------

       INIZIA-TOT-MOV.

           PERFORM INIZIA-ZEROES-MOV THRU INIZIA-ZEROES-MOV-EX

           PERFORM INIZIA-SPACES-MOV THRU INIZIA-SPACES-MOV-EX

           PERFORM INIZIA-NULL-MOV THRU INIZIA-NULL-MOV-EX.

       INIZIA-TOT-MOV-EX.
           EXIT.

       INIZIA-NULL-MOV.
           MOVE HIGH-VALUES TO (SF)-ID-OGG-NULL
           MOVE HIGH-VALUES TO (SF)-IB-OGG-NULL
           MOVE HIGH-VALUES TO (SF)-IB-MOVI-NULL
           MOVE HIGH-VALUES TO (SF)-TP-OGG-NULL
           MOVE HIGH-VALUES TO (SF)-ID-RICH-NULL
           MOVE HIGH-VALUES TO (SF)-TP-MOVI-NULL
           MOVE HIGH-VALUES TO (SF)-ID-MOVI-ANN-NULL
           MOVE HIGH-VALUES TO (SF)-ID-MOVI-COLLG-NULL.
       INIZIA-NULL-MOV-EX.
           EXIT.

       INIZIA-ZEROES-MOV.
           MOVE 0 TO (SF)-ID-MOVI
           MOVE 0 TO (SF)-COD-COMP-ANIA
           MOVE 0 TO (SF)-DT-EFF
           MOVE 0 TO (SF)-DS-VER
           MOVE 0 TO (SF)-DS-TS-CPTZ.
       INIZIA-ZEROES-MOV-EX.
           EXIT.

       INIZIA-SPACES-MOV.
           MOVE SPACES TO (SF)-DS-OPER-SQL
           MOVE SPACES TO (SF)-DS-UTENTE
           MOVE SPACES TO (SF)-DS-STATO-ELAB.
       INIZIA-SPACES-MOV-EX.
           EXIT.

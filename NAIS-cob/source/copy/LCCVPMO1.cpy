      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA PARAM_MOVI
      *   ALIAS PMO
      *   ULTIMO AGG. 17 FEB 2015
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-PARAM-MOVI PIC S9(9)     COMP-3.
             07 (SF)-ID-OGG PIC S9(9)     COMP-3.
             07 (SF)-TP-OGG PIC X(2).
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-TP-MOVI PIC S9(5)     COMP-3.
             07 (SF)-TP-MOVI-NULL REDEFINES
                (SF)-TP-MOVI   PIC X(3).
             07 (SF)-FRQ-MOVI PIC S9(5)     COMP-3.
             07 (SF)-FRQ-MOVI-NULL REDEFINES
                (SF)-FRQ-MOVI   PIC X(3).
             07 (SF)-DUR-AA PIC S9(5)     COMP-3.
             07 (SF)-DUR-AA-NULL REDEFINES
                (SF)-DUR-AA   PIC X(3).
             07 (SF)-DUR-MM PIC S9(5)     COMP-3.
             07 (SF)-DUR-MM-NULL REDEFINES
                (SF)-DUR-MM   PIC X(3).
             07 (SF)-DUR-GG PIC S9(5)     COMP-3.
             07 (SF)-DUR-GG-NULL REDEFINES
                (SF)-DUR-GG   PIC X(3).
             07 (SF)-DT-RICOR-PREC   PIC S9(8) COMP-3.
             07 (SF)-DT-RICOR-PREC-NULL REDEFINES
                (SF)-DT-RICOR-PREC   PIC X(5).
             07 (SF)-DT-RICOR-SUCC   PIC S9(8) COMP-3.
             07 (SF)-DT-RICOR-SUCC-NULL REDEFINES
                (SF)-DT-RICOR-SUCC   PIC X(5).
             07 (SF)-PC-INTR-FRAZ PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-INTR-FRAZ-NULL REDEFINES
                (SF)-PC-INTR-FRAZ   PIC X(4).
             07 (SF)-IMP-BNS-DA-SCO-TOT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-BNS-DA-SCO-TOT-NULL REDEFINES
                (SF)-IMP-BNS-DA-SCO-TOT   PIC X(8).
             07 (SF)-IMP-BNS-DA-SCO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-BNS-DA-SCO-NULL REDEFINES
                (SF)-IMP-BNS-DA-SCO   PIC X(8).
             07 (SF)-PC-ANTIC-BNS PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-ANTIC-BNS-NULL REDEFINES
                (SF)-PC-ANTIC-BNS   PIC X(4).
             07 (SF)-TP-RINN-COLL PIC X(2).
             07 (SF)-TP-RINN-COLL-NULL REDEFINES
                (SF)-TP-RINN-COLL   PIC X(2).
             07 (SF)-TP-RIVAL-PRE PIC X(2).
             07 (SF)-TP-RIVAL-PRE-NULL REDEFINES
                (SF)-TP-RIVAL-PRE   PIC X(2).
             07 (SF)-TP-RIVAL-PRSTZ PIC X(2).
             07 (SF)-TP-RIVAL-PRSTZ-NULL REDEFINES
                (SF)-TP-RIVAL-PRSTZ   PIC X(2).
             07 (SF)-FL-EVID-RIVAL PIC X(1).
             07 (SF)-FL-EVID-RIVAL-NULL REDEFINES
                (SF)-FL-EVID-RIVAL   PIC X(1).
             07 (SF)-ULT-PC-PERD PIC S9(3)V9(3) COMP-3.
             07 (SF)-ULT-PC-PERD-NULL REDEFINES
                (SF)-ULT-PC-PERD   PIC X(4).
             07 (SF)-TOT-AA-GIA-PROR PIC S9(5)     COMP-3.
             07 (SF)-TOT-AA-GIA-PROR-NULL REDEFINES
                (SF)-TOT-AA-GIA-PROR   PIC X(3).
             07 (SF)-TP-OPZ PIC X(12).
             07 (SF)-TP-OPZ-NULL REDEFINES
                (SF)-TP-OPZ   PIC X(12).
             07 (SF)-AA-REN-CER PIC S9(5)     COMP-3.
             07 (SF)-AA-REN-CER-NULL REDEFINES
                (SF)-AA-REN-CER   PIC X(3).
             07 (SF)-PC-REVRSB PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-REVRSB-NULL REDEFINES
                (SF)-PC-REVRSB   PIC X(4).
             07 (SF)-IMP-RISC-PARZ-PRGT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-RISC-PARZ-PRGT-NULL REDEFINES
                (SF)-IMP-RISC-PARZ-PRGT   PIC X(8).
             07 (SF)-IMP-LRD-DI-RAT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-LRD-DI-RAT-NULL REDEFINES
                (SF)-IMP-LRD-DI-RAT   PIC X(8).
             07 (SF)-IB-OGG PIC X(40).
             07 (SF)-IB-OGG-NULL REDEFINES
                (SF)-IB-OGG   PIC X(40).
             07 (SF)-COS-ONER PIC S9(12)V9(3) COMP-3.
             07 (SF)-COS-ONER-NULL REDEFINES
                (SF)-COS-ONER   PIC X(8).
             07 (SF)-SPE-PC PIC S9(3)V9(3) COMP-3.
             07 (SF)-SPE-PC-NULL REDEFINES
                (SF)-SPE-PC   PIC X(4).
             07 (SF)-FL-ATTIV-GAR PIC X(1).
             07 (SF)-FL-ATTIV-GAR-NULL REDEFINES
                (SF)-FL-ATTIV-GAR   PIC X(1).
             07 (SF)-CAMBIO-VER-PROD PIC X(1).
             07 (SF)-CAMBIO-VER-PROD-NULL REDEFINES
                (SF)-CAMBIO-VER-PROD   PIC X(1).
             07 (SF)-MM-DIFF PIC S9(2)     COMP-3.
             07 (SF)-MM-DIFF-NULL REDEFINES
                (SF)-MM-DIFF   PIC X(2).
             07 (SF)-IMP-RAT-MANFEE PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-RAT-MANFEE-NULL REDEFINES
                (SF)-IMP-RAT-MANFEE   PIC X(8).
             07 (SF)-DT-ULT-EROG-MANFEE   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-EROG-MANFEE-NULL REDEFINES
                (SF)-DT-ULT-EROG-MANFEE   PIC X(5).
             07 (SF)-TP-OGG-RIVAL PIC X(2).
             07 (SF)-TP-OGG-RIVAL-NULL REDEFINES
                (SF)-TP-OGG-RIVAL   PIC X(2).
             07 (SF)-SOM-ASSTA-GARAC PIC S9(12)V9(3) COMP-3.
             07 (SF)-SOM-ASSTA-GARAC-NULL REDEFINES
                (SF)-SOM-ASSTA-GARAC   PIC X(8).
             07 (SF)-PC-APPLZ-OPZ PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-APPLZ-OPZ-NULL REDEFINES
                (SF)-PC-APPLZ-OPZ   PIC X(4).
             07 (SF)-ID-ADES PIC S9(9)     COMP-3.
             07 (SF)-ID-ADES-NULL REDEFINES
                (SF)-ID-ADES   PIC X(5).
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-TP-FRM-ASSVA PIC X(2).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-TP-ESTR-CNT PIC X(2).
             07 (SF)-TP-ESTR-CNT-NULL REDEFINES
                (SF)-TP-ESTR-CNT   PIC X(2).
             07 (SF)-COD-RAMO PIC X(12).
             07 (SF)-COD-RAMO-NULL REDEFINES
                (SF)-COD-RAMO   PIC X(12).
             07 (SF)-GEN-DA-SIN PIC X(1).
             07 (SF)-GEN-DA-SIN-NULL REDEFINES
                (SF)-GEN-DA-SIN   PIC X(1).
             07 (SF)-COD-TARI PIC X(12).
             07 (SF)-COD-TARI-NULL REDEFINES
                (SF)-COD-TARI   PIC X(12).
             07 (SF)-NUM-RAT-PAG-PRE PIC S9(5)     COMP-3.
             07 (SF)-NUM-RAT-PAG-PRE-NULL REDEFINES
                (SF)-NUM-RAT-PAG-PRE   PIC X(3).
             07 (SF)-PC-SERV-VAL PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-SERV-VAL-NULL REDEFINES
                (SF)-PC-SERV-VAL   PIC X(4).
             07 (SF)-ETA-AA-SOGL-BNFICR PIC S9(3)     COMP-3.
             07 (SF)-ETA-AA-SOGL-BNFICR-NULL REDEFINES
                (SF)-ETA-AA-SOGL-BNFICR   PIC X(2).

      ***-------------------------------------------------------------**
      *    PROGRAMMA ..... IERP9902
      *    TIPOLOGIA...... COPY
      *    DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
      ***-------------------------------------------------------------**
       S0290-ERRORE-DI-SISTEMA.
           MOVE '005006'           TO IEAI9901-COD-ERRORE.
           MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
           PERFORM S0300-RICERCA-GRAVITA-ERRORE
              THRU EX-S0300.
       EX-S0290.
           EXIT.

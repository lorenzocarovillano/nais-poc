
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVA255
      *   ULTIMO AGG. 26 GIU 2019
      *------------------------------------------------------------

       VAL-DCLGEN-A25.
           MOVE (SF)-ID-PERS(IX-TAB-A25)
              TO A25-ID-PERS
           MOVE (SF)-TSTAM-INI-VLDT(IX-TAB-A25)
              TO A25-TSTAM-INI-VLDT
           IF (SF)-TSTAM-END-VLDT-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-TSTAM-END-VLDT-NULL(IX-TAB-A25)
              TO A25-TSTAM-END-VLDT-NULL
           ELSE
              MOVE (SF)-TSTAM-END-VLDT(IX-TAB-A25)
              TO A25-TSTAM-END-VLDT
           END-IF
           MOVE (SF)-COD-PERS(IX-TAB-A25)
              TO A25-COD-PERS
           IF (SF)-RIFTO-RETE-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-RIFTO-RETE-NULL(IX-TAB-A25)
              TO A25-RIFTO-RETE-NULL
           ELSE
              MOVE (SF)-RIFTO-RETE(IX-TAB-A25)
              TO A25-RIFTO-RETE
           END-IF
           IF (SF)-COD-PRT-IVA-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-COD-PRT-IVA-NULL(IX-TAB-A25)
              TO A25-COD-PRT-IVA-NULL
           ELSE
              MOVE (SF)-COD-PRT-IVA(IX-TAB-A25)
              TO A25-COD-PRT-IVA
           END-IF
           IF (SF)-IND-PVCY-PRSNL-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-IND-PVCY-PRSNL-NULL(IX-TAB-A25)
              TO A25-IND-PVCY-PRSNL-NULL
           ELSE
              MOVE (SF)-IND-PVCY-PRSNL(IX-TAB-A25)
              TO A25-IND-PVCY-PRSNL
           END-IF
           IF (SF)-IND-PVCY-CMMRC-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-IND-PVCY-CMMRC-NULL(IX-TAB-A25)
              TO A25-IND-PVCY-CMMRC-NULL
           ELSE
              MOVE (SF)-IND-PVCY-CMMRC(IX-TAB-A25)
              TO A25-IND-PVCY-CMMRC
           END-IF
           IF (SF)-IND-PVCY-INDST-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-IND-PVCY-INDST-NULL(IX-TAB-A25)
              TO A25-IND-PVCY-INDST-NULL
           ELSE
              MOVE (SF)-IND-PVCY-INDST(IX-TAB-A25)
              TO A25-IND-PVCY-INDST
           END-IF
           IF (SF)-DT-NASC-CLI-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-DT-NASC-CLI-NULL(IX-TAB-A25)
              TO A25-DT-NASC-CLI-NULL
           ELSE
             IF (SF)-DT-NASC-CLI(IX-TAB-A25) = ZERO
                MOVE HIGH-VALUES
                TO A25-DT-NASC-CLI-NULL
             ELSE
              MOVE (SF)-DT-NASC-CLI(IX-TAB-A25)
              TO A25-DT-NASC-CLI
             END-IF
           END-IF
           IF (SF)-DT-ACQS-PERS-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-DT-ACQS-PERS-NULL(IX-TAB-A25)
              TO A25-DT-ACQS-PERS-NULL
           ELSE
             IF (SF)-DT-ACQS-PERS(IX-TAB-A25) = ZERO
                MOVE HIGH-VALUES
                TO A25-DT-ACQS-PERS-NULL
             ELSE
              MOVE (SF)-DT-ACQS-PERS(IX-TAB-A25)
              TO A25-DT-ACQS-PERS
             END-IF
           END-IF
           IF (SF)-IND-CLI-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-IND-CLI-NULL(IX-TAB-A25)
              TO A25-IND-CLI-NULL
           ELSE
              MOVE (SF)-IND-CLI(IX-TAB-A25)
              TO A25-IND-CLI
           END-IF
           IF (SF)-COD-CMN-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-COD-CMN-NULL(IX-TAB-A25)
              TO A25-COD-CMN-NULL
           ELSE
              MOVE (SF)-COD-CMN(IX-TAB-A25)
              TO A25-COD-CMN
           END-IF
           IF (SF)-COD-FRM-GIURD-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-COD-FRM-GIURD-NULL(IX-TAB-A25)
              TO A25-COD-FRM-GIURD-NULL
           ELSE
              MOVE (SF)-COD-FRM-GIURD(IX-TAB-A25)
              TO A25-COD-FRM-GIURD
           END-IF
           IF (SF)-COD-ENTE-PUBB-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-COD-ENTE-PUBB-NULL(IX-TAB-A25)
              TO A25-COD-ENTE-PUBB-NULL
           ELSE
              MOVE (SF)-COD-ENTE-PUBB(IX-TAB-A25)
              TO A25-COD-ENTE-PUBB
           END-IF
           MOVE (SF)-DEN-RGN-SOC(IX-TAB-A25)
              TO A25-DEN-RGN-SOC
           MOVE (SF)-DEN-SIG-RGN-SOC(IX-TAB-A25)
              TO A25-DEN-SIG-RGN-SOC
           IF (SF)-IND-ESE-FISC-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-IND-ESE-FISC-NULL(IX-TAB-A25)
              TO A25-IND-ESE-FISC-NULL
           ELSE
              MOVE (SF)-IND-ESE-FISC(IX-TAB-A25)
              TO A25-IND-ESE-FISC
           END-IF
           IF (SF)-COD-STAT-CVL-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-COD-STAT-CVL-NULL(IX-TAB-A25)
              TO A25-COD-STAT-CVL-NULL
           ELSE
              MOVE (SF)-COD-STAT-CVL(IX-TAB-A25)
              TO A25-COD-STAT-CVL
           END-IF
           MOVE (SF)-DEN-NOME(IX-TAB-A25)
              TO A25-DEN-NOME
           MOVE (SF)-DEN-COGN(IX-TAB-A25)
              TO A25-DEN-COGN
           IF (SF)-COD-FISC-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-COD-FISC-NULL(IX-TAB-A25)
              TO A25-COD-FISC-NULL
           ELSE
              MOVE (SF)-COD-FISC(IX-TAB-A25)
              TO A25-COD-FISC
           END-IF
           IF (SF)-IND-SEX-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-IND-SEX-NULL(IX-TAB-A25)
              TO A25-IND-SEX-NULL
           ELSE
              MOVE (SF)-IND-SEX(IX-TAB-A25)
              TO A25-IND-SEX
           END-IF
           IF (SF)-IND-CPCT-GIURD-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-IND-CPCT-GIURD-NULL(IX-TAB-A25)
              TO A25-IND-CPCT-GIURD-NULL
           ELSE
              MOVE (SF)-IND-CPCT-GIURD(IX-TAB-A25)
              TO A25-IND-CPCT-GIURD
           END-IF
           IF (SF)-IND-PORT-HDCP-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-IND-PORT-HDCP-NULL(IX-TAB-A25)
              TO A25-IND-PORT-HDCP-NULL
           ELSE
              MOVE (SF)-IND-PORT-HDCP(IX-TAB-A25)
              TO A25-IND-PORT-HDCP
           END-IF
           MOVE (SF)-COD-USER-INS(IX-TAB-A25)
              TO A25-COD-USER-INS
           MOVE (SF)-TSTAM-INS-RIGA(IX-TAB-A25)
              TO A25-TSTAM-INS-RIGA
           IF (SF)-COD-USER-AGGM-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-COD-USER-AGGM-NULL(IX-TAB-A25)
              TO A25-COD-USER-AGGM-NULL
           ELSE
              MOVE (SF)-COD-USER-AGGM(IX-TAB-A25)
              TO A25-COD-USER-AGGM
           END-IF
           IF (SF)-TSTAM-AGGM-RIGA-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-TSTAM-AGGM-RIGA-NULL(IX-TAB-A25)
              TO A25-TSTAM-AGGM-RIGA-NULL
           ELSE
              MOVE (SF)-TSTAM-AGGM-RIGA(IX-TAB-A25)
              TO A25-TSTAM-AGGM-RIGA
           END-IF
           MOVE (SF)-DEN-CMN-NASC-STRN(IX-TAB-A25)
              TO A25-DEN-CMN-NASC-STRN
           IF (SF)-COD-RAMO-STGR-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-COD-RAMO-STGR-NULL(IX-TAB-A25)
              TO A25-COD-RAMO-STGR-NULL
           ELSE
              MOVE (SF)-COD-RAMO-STGR(IX-TAB-A25)
              TO A25-COD-RAMO-STGR
           END-IF
           IF (SF)-COD-STGR-ATVT-UIC-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-COD-STGR-ATVT-UIC-NULL(IX-TAB-A25)
              TO A25-COD-STGR-ATVT-UIC-NULL
           ELSE
              MOVE (SF)-COD-STGR-ATVT-UIC(IX-TAB-A25)
              TO A25-COD-STGR-ATVT-UIC
           END-IF
           IF (SF)-COD-RAMO-ATVT-UIC-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-COD-RAMO-ATVT-UIC-NULL(IX-TAB-A25)
              TO A25-COD-RAMO-ATVT-UIC-NULL
           ELSE
              MOVE (SF)-COD-RAMO-ATVT-UIC(IX-TAB-A25)
              TO A25-COD-RAMO-ATVT-UIC
           END-IF
           IF (SF)-DT-END-VLDT-PERS-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-DT-END-VLDT-PERS-NULL(IX-TAB-A25)
              TO A25-DT-END-VLDT-PERS-NULL
           ELSE
             IF (SF)-DT-END-VLDT-PERS(IX-TAB-A25) = ZERO
                MOVE HIGH-VALUES
                TO A25-DT-END-VLDT-PERS-NULL
             ELSE
              MOVE (SF)-DT-END-VLDT-PERS(IX-TAB-A25)
              TO A25-DT-END-VLDT-PERS
             END-IF
           END-IF
           IF (SF)-DT-DEAD-PERS-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-DT-DEAD-PERS-NULL(IX-TAB-A25)
              TO A25-DT-DEAD-PERS-NULL
           ELSE
             IF (SF)-DT-DEAD-PERS(IX-TAB-A25) = ZERO
                MOVE HIGH-VALUES
                TO A25-DT-DEAD-PERS-NULL
             ELSE
              MOVE (SF)-DT-DEAD-PERS(IX-TAB-A25)
              TO A25-DT-DEAD-PERS
             END-IF
           END-IF
           IF (SF)-TP-STAT-CLI-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-TP-STAT-CLI-NULL(IX-TAB-A25)
              TO A25-TP-STAT-CLI-NULL
           ELSE
              MOVE (SF)-TP-STAT-CLI(IX-TAB-A25)
              TO A25-TP-STAT-CLI
           END-IF
           IF (SF)-DT-BLOC-CLI-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-DT-BLOC-CLI-NULL(IX-TAB-A25)
              TO A25-DT-BLOC-CLI-NULL
           ELSE
             IF (SF)-DT-BLOC-CLI(IX-TAB-A25) = ZERO
                MOVE HIGH-VALUES
                TO A25-DT-BLOC-CLI-NULL
             ELSE
              MOVE (SF)-DT-BLOC-CLI(IX-TAB-A25)
              TO A25-DT-BLOC-CLI
             END-IF
           END-IF
           IF (SF)-COD-PERS-SECOND-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-COD-PERS-SECOND-NULL(IX-TAB-A25)
              TO A25-COD-PERS-SECOND-NULL
           ELSE
              MOVE (SF)-COD-PERS-SECOND(IX-TAB-A25)
              TO A25-COD-PERS-SECOND
           END-IF
           IF (SF)-ID-SEGMENTAZ-CLI-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-ID-SEGMENTAZ-CLI-NULL(IX-TAB-A25)
              TO A25-ID-SEGMENTAZ-CLI-NULL
           ELSE
              MOVE (SF)-ID-SEGMENTAZ-CLI(IX-TAB-A25)
              TO A25-ID-SEGMENTAZ-CLI
           END-IF
           IF (SF)-DT-1A-ATVT-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-DT-1A-ATVT-NULL(IX-TAB-A25)
              TO A25-DT-1A-ATVT-NULL
           ELSE
             IF (SF)-DT-1A-ATVT(IX-TAB-A25) = ZERO
                MOVE HIGH-VALUES
                TO A25-DT-1A-ATVT-NULL
             ELSE
              MOVE (SF)-DT-1A-ATVT(IX-TAB-A25)
              TO A25-DT-1A-ATVT
             END-IF
           END-IF
           IF (SF)-DT-SEGNAL-PARTNER-NULL(IX-TAB-A25) = HIGH-VALUES
              MOVE (SF)-DT-SEGNAL-PARTNER-NULL(IX-TAB-A25)
              TO A25-DT-SEGNAL-PARTNER-NULL
           ELSE
             IF (SF)-DT-SEGNAL-PARTNER(IX-TAB-A25) = ZERO
                MOVE HIGH-VALUES
                TO A25-DT-SEGNAL-PARTNER-NULL
             ELSE
              MOVE (SF)-DT-SEGNAL-PARTNER(IX-TAB-A25)
              TO A25-DT-SEGNAL-PARTNER
             END-IF
           END-IF.
       VAL-DCLGEN-A25-EX.
           EXIT.

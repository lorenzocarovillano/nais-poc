      *----------------------------------------------------------------*
      *    COPY      ..... LCCVGRZ6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO GARANZIA
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVGRZ5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN GARANZIA (LCCVGRZ1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-GAR.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE GAR

      *--> NOME TABELLA FISICA DB
           MOVE 'GAR'                   TO   WK-TABELLA

      *--> SE LO STATUS NON E' "INVARIATO" O "CONVERSAZIONE"
           IF  NOT WGRZ-ST-INV(IX-TAB-GRZ)
           AND NOT WGRZ-ST-CON(IX-TAB-GRZ)
           AND     WGRZ-ELE-GAR-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WGRZ-ST-ADD(IX-TAB-GRZ)

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK
                        MOVE S090-SEQ-TABELLA TO WGRZ-ID-PTF(IX-TAB-GRZ)
                                                 GRZ-ID-GAR
                        MOVE WPOL-ID-PTF         TO GRZ-ID-POLI
                        MOVE WADE-ID-PTF         TO GRZ-ID-ADES
                        MOVE WMOV-ID-PTF         TO GRZ-ID-MOVI-CRZ

      *-->              VALORIZZA GLI ID ASSICURATI DELLA GARANZIA
                        PERFORM VALORIZZA-ID-ASSIC-GAR
                           THRU VALORIZZA-ID-ASSIC-GAR-EX

                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WGRZ-ST-MOD(IX-TAB-GRZ)

                       MOVE WGRZ-ID-PTF(IX-TAB-GRZ)   TO GRZ-ID-GAR
                       MOVE WPOL-ID-PTF               TO GRZ-ID-POLI
                       MOVE WADE-ID-PTF               TO GRZ-ID-ADES
                       MOVE WMOV-ID-PTF               TO GRZ-ID-MOVI-CRZ

      *-->             VALORIZZA GLI ID ASSICURATI DELLA GARANZIA
                       PERFORM VALORIZZA-ID-ASSIC-GAR
                          THRU VALORIZZA-ID-ASSIC-GAR-EX

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WGRZ-ST-DEL(IX-TAB-GRZ)

                       MOVE WGRZ-ID-PTF(IX-TAB-GRZ)   TO GRZ-ID-GAR
                       MOVE WPOL-ID-PTF               TO GRZ-ID-POLI
                       MOVE WADE-ID-PTF               TO GRZ-ID-ADES
                       MOVE WMOV-ID-PTF               TO GRZ-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN ADESIONE
                 PERFORM VAL-DCLGEN-GRZ
                    THRU VAL-DCLGEN-GRZ-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-GRZ
                    THRU VALORIZZA-AREA-DSH-GRZ-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-GAR-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-GRZ.

      *--> DCLGEN TABELLA
           MOVE GAR                     TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-GRZ-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA GLI ID ASSICURATI DELLA GARANZIA
      *----------------------------------------------------------------*
       VALORIZZA-ID-ASSIC-GAR.

           PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
                     UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX

                IF WGRZ-ID-1O-ASSTO-NULL(IX-TAB-GRZ) NOT = HIGH-VALUES
                AND WRAN-ID-RAPP-ANA(IX-TAB-RAN) =
                    WGRZ-ID-1O-ASSTO(IX-TAB-GRZ)

                   MOVE WRAN-ID-PTF(IX-TAB-RAN)
                     TO WGRZ-ID-1O-ASSTO(IX-TAB-GRZ)
                END-IF

                IF WGRZ-ID-2O-ASSTO-NULL(IX-TAB-GRZ) NOT = HIGH-VALUES
                AND WRAN-ID-RAPP-ANA(IX-TAB-RAN) =
                    WGRZ-ID-2O-ASSTO(IX-TAB-GRZ)

                   MOVE WRAN-ID-PTF(IX-TAB-RAN)
                     TO WGRZ-ID-2O-ASSTO(IX-TAB-GRZ)
                END-IF

                IF WGRZ-ID-3O-ASSTO-NULL(IX-TAB-GRZ) NOT = HIGH-VALUES
                AND WRAN-ID-RAPP-ANA(IX-TAB-RAN) =
                    WGRZ-ID-3O-ASSTO(IX-TAB-GRZ)

                   MOVE WRAN-ID-PTF(IX-TAB-RAN)
                     TO WGRZ-ID-3O-ASSTO(IX-TAB-GRZ)
                END-IF

           END-PERFORM.

       VALORIZZA-ID-ASSIC-GAR-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVGRZ5.

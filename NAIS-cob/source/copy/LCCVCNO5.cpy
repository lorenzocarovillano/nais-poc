
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVCNO5
      *   ULTIMO AGG. 16 MAR 2009
      *------------------------------------------------------------

       VAL-DCLGEN-CNO.
           MOVE (SF)-COD-COMPAGNIA-ANIA
              TO CNO-COD-COMPAGNIA-ANIA
           MOVE (SF)-FORMA-ASSICURATIVA
              TO CNO-FORMA-ASSICURATIVA
           MOVE (SF)-COD-OGGETTO
              TO CNO-COD-OGGETTO
           MOVE (SF)-POSIZIONE
              TO CNO-POSIZIONE
           IF (SF)-COD-STR-DATO-NULL = HIGH-VALUES
              MOVE (SF)-COD-STR-DATO-NULL
              TO CNO-COD-STR-DATO-NULL
           ELSE
              MOVE (SF)-COD-STR-DATO
              TO CNO-COD-STR-DATO
           END-IF
           IF (SF)-COD-DATO-NULL = HIGH-VALUES
              MOVE (SF)-COD-DATO-NULL
              TO CNO-COD-DATO-NULL
           ELSE
              MOVE (SF)-COD-DATO
              TO CNO-COD-DATO
           END-IF
           IF (SF)-VALORE-DEFAULT-NULL = HIGH-VALUES
              MOVE (SF)-VALORE-DEFAULT-NULL
              TO CNO-VALORE-DEFAULT-NULL
           ELSE
              MOVE (SF)-VALORE-DEFAULT
              TO CNO-VALORE-DEFAULT
           END-IF
           IF (SF)-LUNGHEZZA-DATO-NULL = HIGH-VALUES
              MOVE (SF)-LUNGHEZZA-DATO-NULL
              TO CNO-LUNGHEZZA-DATO-NULL
           ELSE
              MOVE (SF)-LUNGHEZZA-DATO
              TO CNO-LUNGHEZZA-DATO
           END-IF
           IF (SF)-FLAG-KEY-ULT-PROGR-NULL = HIGH-VALUES
              MOVE (SF)-FLAG-KEY-ULT-PROGR-NULL
              TO CNO-FLAG-KEY-ULT-PROGR-NULL
           ELSE
              MOVE (SF)-FLAG-KEY-ULT-PROGR
              TO CNO-FLAG-KEY-ULT-PROGR
           END-IF.
       VAL-DCLGEN-CNO-EX.
           EXIT.

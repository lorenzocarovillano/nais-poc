      *----------------------------------------------------------------*
      *    SALVO AREA SU TABELLA TEMPORANEA
      *----------------------------------------------------------------*
       GESTIONE-TEMP-TABLE.
      *
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO IDSV0301-COD-COMP-ANIA
           MOVE IDSV0001-ID-TEMPORARY-DATA
             TO IDSV0301-ID-TEMPORARY-DATA
           MOVE IDSV0001-SESSIONE
             TO IDSV0301-ID-SESSION.

           IF IDSV0001-ON-LINE
              SET IDSV0301-STATIC-TEMP-TABLE
               TO TRUE
           ELSE
              MOVE IDSV0001-TEMPORARY-TABLE
                TO IDSV0301-TEMPORARY-TABLE
           END-IF.

           CALL IDSS0300   USING IDSV0301
           ON EXCEPTION
               MOVE IDSS0300               TO IEAI9901-COD-SERVIZIO-BE
               MOVE 'CALL SERVIZIO TEMPORARY TABLE'
                                           TO CALL-DESC
               MOVE 'GESTIONE-TEMP-TABLE'    TO IEAI9901-LABEL-ERR
               PERFORM S0290-ERRORE-DI-SISTEMA
                  THRU EX-S0290
               MOVE IDSV0001-ESITO           TO IDSV0301-ESITO
           END-CALL.

           IF IDSV0301-ESITO-KO AND IDSV0001-ESITO-OK
              IF NOT IDSV0301-NOT-FOUND
                 MOVE IDSS0300              TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'GESTIONE-TEMP-TABLE'   TO IEAI9901-LABEL-ERR
                 MOVE '005016'              TO IEAI9901-COD-ERRORE
                 STRING 'TAB TEMPORANEA SQLCODE : '
                        IDSV0301-SQLCODE  ' ;'
                        IDSV0301-DESC-ERRORE-ESTESA
                 DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

      *
       GESTIONE-TEMP-TABLE-EX.
           EXIT.

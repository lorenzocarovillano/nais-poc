      ******************************************************************
      *  COPY DI PROCEDURE PER GESTIONE NUMERAZIONE OGGETTI (VENDITA)  *
      ******************************************************************
      *  ISTRUZIONI: PER POTER UTILIZZARE QUESTA COPY DI PROCEDURE
      *  BISOGNA DICHIARARE LE SEGUENTI COPY:


      *----------------------------------------------------------------*
      *    ESTRAZIONE IB-OGGETTO PER ADESIONE
      *----------------------------------------------------------------*
       ESTR-IB-OGG-ADE.

           IF  WPOL-ID-PTF IS NUMERIC
           AND WPOL-ID-PTF > ZEROES

               IF   WADE-ST-ADD
               AND (WADE-IB-OGG-NULL = HIGH-VALUE
                OR  WADE-IB-OGG-NULL = SPACES)

                  INITIALIZE AREA-IO-LCCS0070

                  SET LCCC0070-CALC-IB-ADE        TO TRUE

                  MOVE WPOL-ID-PTF                TO LCCC0070-ID-OGGETTO
                  MOVE 'PO'                       TO LCCC0070-TP-OGGETTO

                  PERFORM CALL-LCCS0070
                     THRU CALL-LCCS0070-EX

                  IF IDSV0001-ESITO-OK
                     MOVE LCCC0070-IB-OGGETTO       TO WADE-IB-OGG
                  END-IF

               END-IF

           END-IF.

       ESTR-IB-OGG-ADE-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ESTRAZIONE IB-OGGETTO PER LA GARANZIA
      *----------------------------------------------------------------*
       ESTR-IB-OGG-GAR.

      *--> SE L'IB-OGG DELLA GARANZIA � GIA STATO VALORIZZATO NON DEVE
      *--> ESSERE RICALCOLATO
      *--> SE INVECE IL CALCOLO VIENE EFFETTUATO � NECESSARIO CHE
      *--> L'ID-PTF DELL'ADESIONE SIA VALORIZZATO.
           SET WS-NO-FINE-CICLO   TO TRUE

           IF  WADE-ID-PTF IS NUMERIC
           AND WADE-ID-PTF > ZEROES

               PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
                         UNTIL IX-TAB-GRZ GREATER WGRZ-ELE-GAR-MAX
                            OR IDSV0001-ESITO-KO
                            OR WS-SI-FINE-CICLO

                 IF WGRZ-ST-ADD(IX-TAB-GRZ)

                    IF WGRZ-IB-OGG-NULL(IX-TAB-GRZ) = HIGH-VALUE
                    OR WGRZ-IB-OGG-NULL(IX-TAB-GRZ) = SPACES

                       SET LCCC0070-CALC-IB-GAR        TO TRUE

                       MOVE WADE-ID-PTF           TO LCCC0070-ID-OGGETTO
                       MOVE 'AD'                  TO LCCC0070-TP-OGGETTO

                       PERFORM CALL-LCCS0070
                          THRU CALL-LCCS0070-EX

                       IF IDSV0001-ESITO-OK
                          MOVE LCCC0070-IB-OGGETTO
                            TO WGRZ-IB-OGG(IX-TAB-GRZ)
                       END-IF

                    ELSE

                       SET WS-SI-FINE-CICLO  TO TRUE

                    END-IF

                 END-IF

               END-PERFORM

           END-IF.

       ESTR-IB-OGG-GAR-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ESTRAZIONE IB-OGGETTO PER LA TRANCHE DI GARANZIA
      *----------------------------------------------------------------*
       ESTR-IB-OGG-TGA.

      *--> SE L'IB-OGG DELLA TRANCHE � GIA STATO VALORIZZATO NON DEVE
      *--> ESSERE RICALCOLATO
      *--> SE INVECE IL CALCOLO VIENE EFFETTUATO � NECESSARIO CHE
      *--> L'ID-PTF DELLA GARANZIA SIA VALORIZZATO.
           SET WS-NO-FINE-CICLO   TO TRUE

           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
                     UNTIL IX-TAB-TGA GREATER WTGA-ELE-TRAN-MAX
                        OR IDSV0001-ESITO-KO
                        OR WS-SI-FINE-CICLO

                IF WTGA-ST-ADD(IX-TAB-TGA)

                   IF WTGA-IB-OGG-NULL(IX-TAB-TGA) = HIGH-VALUE
                   OR WTGA-IB-OGG-NULL(IX-TAB-TGA) = SPACES

                      PERFORM CERCA-GAR
                         THRU CERCA-GAR-EX

                      IF TROVATO-SI
                         SET LCCC0070-CALC-IB-TGA        TO TRUE

                         MOVE WS-GAR-ID-PTF
                           TO LCCC0070-ID-OGGETTO
                         MOVE 'GA'
                           TO LCCC0070-TP-OGGETTO

                         PERFORM CALL-LCCS0070
                            THRU CALL-LCCS0070-EX

                         IF IDSV0001-ESITO-OK
                            MOVE LCCC0070-IB-OGGETTO
                              TO WTGA-IB-OGG(IX-TAB-TGA)
                         END-IF

                      END-IF

                   ELSE

                      SET WS-SI-FINE-CICLO  TO TRUE

                   END-IF

                END-IF

           END-PERFORM.

       ESTR-IB-OGG-TGA-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   CERCA ID-GARANZIA
      *----------------------------------------------------------------*
       CERCA-GAR.

           SET TROVATO-NO  TO TRUE.

           PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
                     UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
                        OR TROVATO-SI

             IF WTGA-ID-GAR(IX-TAB-TGA) = WGRZ-ID-GAR(IX-TAB-GRZ)
                IF  WGRZ-ID-PTF(IX-TAB-GRZ) IS NUMERIC
                AND WGRZ-ID-PTF(IX-TAB-GRZ) > ZEROES
                   SET  TROVATO-SI               TO TRUE
                   MOVE WGRZ-ID-PTF(IX-TAB-GRZ)  TO WS-GAR-ID-PTF
                END-IF
             END-IF

           END-PERFORM.

       CERCA-GAR-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    CALL LCCS0070
      *----------------------------------------------------------------*
       CALL-LCCS0070.

           CALL LCCS0070          USING  AREA-IDSV0001
                                         AREA-IO-LCCS0070

           ON EXCEPTION
      *       "ERRORE DI SISTEMA DURANTE..."
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'CALL NUMERAZIONE OGGETTI ADE/GAR/TRA (LCCS0070)'
                TO CALL-DESC
              MOVE 'CALL-LCCS0070'
                TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

       CALL-LCCS0070-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVL165
      *   ULTIMO AGG. 07 GEN 2016
      *------------------------------------------------------------

       VAL-DCLGEN-L16.
           MOVE (SF)-COD-COMP-ANIA
              TO L16-COD-COMP-ANIA
           MOVE (SF)-COD-CAN
              TO L16-COD-CAN
           MOVE (SF)-COD-BLOCCO
              TO L16-COD-BLOCCO
           MOVE (SF)-TP-MOVI
              TO L16-TP-MOVI
           MOVE (SF)-GRAV-FUNZ-BLOCCO
              TO L16-GRAV-FUNZ-BLOCCO
           MOVE (SF)-DS-OPER-SQL
              TO L16-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO L16-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO L16-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO L16-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO L16-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO L16-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO L16-DS-STATO-ELAB
           IF (SF)-TP-MOVI-RIFTO-NULL = HIGH-VALUES
              MOVE (SF)-TP-MOVI-RIFTO-NULL
              TO L16-TP-MOVI-RIFTO-NULL
           ELSE
              MOVE (SF)-TP-MOVI-RIFTO
              TO L16-TP-MOVI-RIFTO
           END-IF.
       VAL-DCLGEN-L16-EX.
           EXIT.

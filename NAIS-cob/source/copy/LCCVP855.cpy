
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVP855
      *   ULTIMO AGG. 26 SET 2014
      *------------------------------------------------------------

       VAL-DCLGEN-P85.
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-P85)
              TO P85-COD-COMP-ANIA
           MOVE (SF)-ID-POLI(IX-TAB-P85)
              TO P85-ID-POLI
           MOVE (SF)-DT-RIVAL(IX-TAB-P85)
              TO P85-DT-RIVAL
           MOVE (SF)-COD-TARI(IX-TAB-P85)
              TO P85-COD-TARI
           IF (SF)-RENDTO-LRD-NULL(IX-TAB-P85) = HIGH-VALUES
              MOVE (SF)-RENDTO-LRD-NULL(IX-TAB-P85)
              TO P85-RENDTO-LRD-NULL
           ELSE
              MOVE (SF)-RENDTO-LRD(IX-TAB-P85)
              TO P85-RENDTO-LRD
           END-IF
           IF (SF)-PC-RETR-NULL(IX-TAB-P85) = HIGH-VALUES
              MOVE (SF)-PC-RETR-NULL(IX-TAB-P85)
              TO P85-PC-RETR-NULL
           ELSE
              MOVE (SF)-PC-RETR(IX-TAB-P85)
              TO P85-PC-RETR
           END-IF
           IF (SF)-RENDTO-RETR-NULL(IX-TAB-P85) = HIGH-VALUES
              MOVE (SF)-RENDTO-RETR-NULL(IX-TAB-P85)
              TO P85-RENDTO-RETR-NULL
           ELSE
              MOVE (SF)-RENDTO-RETR(IX-TAB-P85)
              TO P85-RENDTO-RETR
           END-IF
           IF (SF)-COMMIS-GEST-NULL(IX-TAB-P85) = HIGH-VALUES
              MOVE (SF)-COMMIS-GEST-NULL(IX-TAB-P85)
              TO P85-COMMIS-GEST-NULL
           ELSE
              MOVE (SF)-COMMIS-GEST(IX-TAB-P85)
              TO P85-COMMIS-GEST
           END-IF
           IF (SF)-TS-RIVAL-NET-NULL(IX-TAB-P85) = HIGH-VALUES
              MOVE (SF)-TS-RIVAL-NET-NULL(IX-TAB-P85)
              TO P85-TS-RIVAL-NET-NULL
           ELSE
              MOVE (SF)-TS-RIVAL-NET(IX-TAB-P85)
              TO P85-TS-RIVAL-NET
           END-IF
           IF (SF)-MIN-GARTO-NULL(IX-TAB-P85) = HIGH-VALUES
              MOVE (SF)-MIN-GARTO-NULL(IX-TAB-P85)
              TO P85-MIN-GARTO-NULL
           ELSE
              MOVE (SF)-MIN-GARTO(IX-TAB-P85)
              TO P85-MIN-GARTO
           END-IF
           IF (SF)-MIN-TRNUT-NULL(IX-TAB-P85) = HIGH-VALUES
              MOVE (SF)-MIN-TRNUT-NULL(IX-TAB-P85)
              TO P85-MIN-TRNUT-NULL
           ELSE
              MOVE (SF)-MIN-TRNUT(IX-TAB-P85)
              TO P85-MIN-TRNUT
           END-IF
           MOVE (SF)-IB-POLI(IX-TAB-P85)
              TO P85-IB-POLI
           MOVE (SF)-DS-OPER-SQL(IX-TAB-P85)
              TO P85-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-P85) NOT NUMERIC
              MOVE 0 TO P85-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-P85)
              TO P85-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ(IX-TAB-P85) NOT NUMERIC
              MOVE 0 TO P85-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ(IX-TAB-P85)
              TO P85-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-P85)
              TO P85-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-P85)
              TO P85-DS-STATO-ELAB.
       VAL-DCLGEN-P85-EX.
           EXIT.

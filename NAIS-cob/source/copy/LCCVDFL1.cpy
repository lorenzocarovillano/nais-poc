      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA D_FORZ_LIQ
      *   ALIAS DFL
      *   ULTIMO AGG. 02 LUG 2014
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-D-FORZ-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-IMP-LRD-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-LRD-CALC-NULL REDEFINES
                (SF)-IMP-LRD-CALC   PIC X(8).
             07 (SF)-IMP-LRD-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-LRD-DFZ-NULL REDEFINES
                (SF)-IMP-LRD-DFZ   PIC X(8).
             07 (SF)-IMP-LRD-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-LRD-EFFLQ-NULL REDEFINES
                (SF)-IMP-LRD-EFFLQ   PIC X(8).
             07 (SF)-IMP-NET-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-NET-CALC-NULL REDEFINES
                (SF)-IMP-NET-CALC   PIC X(8).
             07 (SF)-IMP-NET-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-NET-DFZ-NULL REDEFINES
                (SF)-IMP-NET-DFZ   PIC X(8).
             07 (SF)-IMP-NET-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-NET-EFFLQ-NULL REDEFINES
                (SF)-IMP-NET-EFFLQ   PIC X(8).
             07 (SF)-IMPST-PRVR-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-PRVR-CALC-NULL REDEFINES
                (SF)-IMPST-PRVR-CALC   PIC X(8).
             07 (SF)-IMPST-PRVR-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-PRVR-DFZ-NULL REDEFINES
                (SF)-IMPST-PRVR-DFZ   PIC X(8).
             07 (SF)-IMPST-PRVR-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-PRVR-EFFLQ-NULL REDEFINES
                (SF)-IMPST-PRVR-EFFLQ   PIC X(8).
             07 (SF)-IMPST-VIS-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-CALC-NULL REDEFINES
                (SF)-IMPST-VIS-CALC   PIC X(8).
             07 (SF)-IMPST-VIS-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-DFZ-NULL REDEFINES
                (SF)-IMPST-VIS-DFZ   PIC X(8).
             07 (SF)-IMPST-VIS-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-EFFLQ-NULL REDEFINES
                (SF)-IMPST-VIS-EFFLQ   PIC X(8).
             07 (SF)-RIT-ACC-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIT-ACC-CALC-NULL REDEFINES
                (SF)-RIT-ACC-CALC   PIC X(8).
             07 (SF)-RIT-ACC-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIT-ACC-DFZ-NULL REDEFINES
                (SF)-RIT-ACC-DFZ   PIC X(8).
             07 (SF)-RIT-ACC-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIT-ACC-EFFLQ-NULL REDEFINES
                (SF)-RIT-ACC-EFFLQ   PIC X(8).
             07 (SF)-RIT-IRPEF-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIT-IRPEF-CALC-NULL REDEFINES
                (SF)-RIT-IRPEF-CALC   PIC X(8).
             07 (SF)-RIT-IRPEF-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIT-IRPEF-DFZ-NULL REDEFINES
                (SF)-RIT-IRPEF-DFZ   PIC X(8).
             07 (SF)-RIT-IRPEF-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIT-IRPEF-EFFLQ-NULL REDEFINES
                (SF)-RIT-IRPEF-EFFLQ   PIC X(8).
             07 (SF)-IMPST-SOST-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-SOST-CALC-NULL REDEFINES
                (SF)-IMPST-SOST-CALC   PIC X(8).
             07 (SF)-IMPST-SOST-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-SOST-DFZ-NULL REDEFINES
                (SF)-IMPST-SOST-DFZ   PIC X(8).
             07 (SF)-IMPST-SOST-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-SOST-EFFLQ-NULL REDEFINES
                (SF)-IMPST-SOST-EFFLQ   PIC X(8).
             07 (SF)-TAX-SEP-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-TAX-SEP-CALC-NULL REDEFINES
                (SF)-TAX-SEP-CALC   PIC X(8).
             07 (SF)-TAX-SEP-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-TAX-SEP-DFZ-NULL REDEFINES
                (SF)-TAX-SEP-DFZ   PIC X(8).
             07 (SF)-TAX-SEP-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-TAX-SEP-EFFLQ-NULL REDEFINES
                (SF)-TAX-SEP-EFFLQ   PIC X(8).
             07 (SF)-INTR-PREST-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-INTR-PREST-CALC-NULL REDEFINES
                (SF)-INTR-PREST-CALC   PIC X(8).
             07 (SF)-INTR-PREST-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-INTR-PREST-DFZ-NULL REDEFINES
                (SF)-INTR-PREST-DFZ   PIC X(8).
             07 (SF)-INTR-PREST-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-INTR-PREST-EFFLQ-NULL REDEFINES
                (SF)-INTR-PREST-EFFLQ   PIC X(8).
             07 (SF)-ACCPRE-SOST-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-ACCPRE-SOST-CALC-NULL REDEFINES
                (SF)-ACCPRE-SOST-CALC   PIC X(8).
             07 (SF)-ACCPRE-SOST-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-ACCPRE-SOST-DFZ-NULL REDEFINES
                (SF)-ACCPRE-SOST-DFZ   PIC X(8).
             07 (SF)-ACCPRE-SOST-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-ACCPRE-SOST-EFFLQ-NULL REDEFINES
                (SF)-ACCPRE-SOST-EFFLQ   PIC X(8).
             07 (SF)-ACCPRE-VIS-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-ACCPRE-VIS-CALC-NULL REDEFINES
                (SF)-ACCPRE-VIS-CALC   PIC X(8).
             07 (SF)-ACCPRE-VIS-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-ACCPRE-VIS-DFZ-NULL REDEFINES
                (SF)-ACCPRE-VIS-DFZ   PIC X(8).
             07 (SF)-ACCPRE-VIS-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-ACCPRE-VIS-EFFLQ-NULL REDEFINES
                (SF)-ACCPRE-VIS-EFFLQ   PIC X(8).
             07 (SF)-ACCPRE-ACC-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-ACCPRE-ACC-CALC-NULL REDEFINES
                (SF)-ACCPRE-ACC-CALC   PIC X(8).
             07 (SF)-ACCPRE-ACC-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-ACCPRE-ACC-DFZ-NULL REDEFINES
                (SF)-ACCPRE-ACC-DFZ   PIC X(8).
             07 (SF)-ACCPRE-ACC-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-ACCPRE-ACC-EFFLQ-NULL REDEFINES
                (SF)-ACCPRE-ACC-EFFLQ   PIC X(8).
             07 (SF)-RES-PRSTZ-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-RES-PRSTZ-CALC-NULL REDEFINES
                (SF)-RES-PRSTZ-CALC   PIC X(8).
             07 (SF)-RES-PRSTZ-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-RES-PRSTZ-DFZ-NULL REDEFINES
                (SF)-RES-PRSTZ-DFZ   PIC X(8).
             07 (SF)-RES-PRSTZ-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-RES-PRSTZ-EFFLQ-NULL REDEFINES
                (SF)-RES-PRSTZ-EFFLQ   PIC X(8).
             07 (SF)-RES-PRE-ATT-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-RES-PRE-ATT-CALC-NULL REDEFINES
                (SF)-RES-PRE-ATT-CALC   PIC X(8).
             07 (SF)-RES-PRE-ATT-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-RES-PRE-ATT-DFZ-NULL REDEFINES
                (SF)-RES-PRE-ATT-DFZ   PIC X(8).
             07 (SF)-RES-PRE-ATT-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-RES-PRE-ATT-EFFLQ-NULL REDEFINES
                (SF)-RES-PRE-ATT-EFFLQ   PIC X(8).
             07 (SF)-IMP-EXCONTR-EFF PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-EXCONTR-EFF-NULL REDEFINES
                (SF)-IMP-EXCONTR-EFF   PIC X(8).
             07 (SF)-IMPB-VIS-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-CALC-NULL REDEFINES
                (SF)-IMPB-VIS-CALC   PIC X(8).
             07 (SF)-IMPB-VIS-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-EFFLQ-NULL REDEFINES
                (SF)-IMPB-VIS-EFFLQ   PIC X(8).
             07 (SF)-IMPB-VIS-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-DFZ-NULL REDEFINES
                (SF)-IMPB-VIS-DFZ   PIC X(8).
             07 (SF)-IMPB-RIT-ACC-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-RIT-ACC-CALC-NULL REDEFINES
                (SF)-IMPB-RIT-ACC-CALC   PIC X(8).
             07 (SF)-IMPB-RIT-ACC-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-RIT-ACC-EFFLQ-NULL REDEFINES
                (SF)-IMPB-RIT-ACC-EFFLQ   PIC X(8).
             07 (SF)-IMPB-RIT-ACC-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-RIT-ACC-DFZ-NULL REDEFINES
                (SF)-IMPB-RIT-ACC-DFZ   PIC X(8).
             07 (SF)-IMPB-TFR-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-TFR-CALC-NULL REDEFINES
                (SF)-IMPB-TFR-CALC   PIC X(8).
             07 (SF)-IMPB-TFR-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-TFR-EFFLQ-NULL REDEFINES
                (SF)-IMPB-TFR-EFFLQ   PIC X(8).
             07 (SF)-IMPB-TFR-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-TFR-DFZ-NULL REDEFINES
                (SF)-IMPB-TFR-DFZ   PIC X(8).
             07 (SF)-IMPB-IS-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-CALC-NULL REDEFINES
                (SF)-IMPB-IS-CALC   PIC X(8).
             07 (SF)-IMPB-IS-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-EFFLQ-NULL REDEFINES
                (SF)-IMPB-IS-EFFLQ   PIC X(8).
             07 (SF)-IMPB-IS-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-DFZ-NULL REDEFINES
                (SF)-IMPB-IS-DFZ   PIC X(8).
             07 (SF)-IMPB-TAX-SEP-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-TAX-SEP-CALC-NULL REDEFINES
                (SF)-IMPB-TAX-SEP-CALC   PIC X(8).
             07 (SF)-IMPB-TAX-SEP-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-TAX-SEP-EFFLQ-NULL REDEFINES
                (SF)-IMPB-TAX-SEP-EFFLQ   PIC X(8).
             07 (SF)-IMPB-TAX-SEP-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-TAX-SEP-DFZ-NULL REDEFINES
                (SF)-IMPB-TAX-SEP-DFZ   PIC X(8).
             07 (SF)-IINT-PREST-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IINT-PREST-CALC-NULL REDEFINES
                (SF)-IINT-PREST-CALC   PIC X(8).
             07 (SF)-IINT-PREST-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IINT-PREST-EFFLQ-NULL REDEFINES
                (SF)-IINT-PREST-EFFLQ   PIC X(8).
             07 (SF)-IINT-PREST-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IINT-PREST-DFZ-NULL REDEFINES
                (SF)-IINT-PREST-DFZ   PIC X(8).
             07 (SF)-MONT-END2000-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-MONT-END2000-CALC-NULL REDEFINES
                (SF)-MONT-END2000-CALC   PIC X(8).
             07 (SF)-MONT-END2000-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-MONT-END2000-EFFLQ-NULL REDEFINES
                (SF)-MONT-END2000-EFFLQ   PIC X(8).
             07 (SF)-MONT-END2000-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-MONT-END2000-DFZ-NULL REDEFINES
                (SF)-MONT-END2000-DFZ   PIC X(8).
             07 (SF)-MONT-END2006-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-MONT-END2006-CALC-NULL REDEFINES
                (SF)-MONT-END2006-CALC   PIC X(8).
             07 (SF)-MONT-END2006-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-MONT-END2006-EFFLQ-NULL REDEFINES
                (SF)-MONT-END2006-EFFLQ   PIC X(8).
             07 (SF)-MONT-END2006-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-MONT-END2006-DFZ-NULL REDEFINES
                (SF)-MONT-END2006-DFZ   PIC X(8).
             07 (SF)-MONT-DAL2007-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-MONT-DAL2007-CALC-NULL REDEFINES
                (SF)-MONT-DAL2007-CALC   PIC X(8).
             07 (SF)-MONT-DAL2007-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-MONT-DAL2007-EFFLQ-NULL REDEFINES
                (SF)-MONT-DAL2007-EFFLQ   PIC X(8).
             07 (SF)-MONT-DAL2007-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-MONT-DAL2007-DFZ-NULL REDEFINES
                (SF)-MONT-DAL2007-DFZ   PIC X(8).
             07 (SF)-IIMPST-PRVR-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IIMPST-PRVR-CALC-NULL REDEFINES
                (SF)-IIMPST-PRVR-CALC   PIC X(8).
             07 (SF)-IIMPST-PRVR-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IIMPST-PRVR-EFFLQ-NULL REDEFINES
                (SF)-IIMPST-PRVR-EFFLQ   PIC X(8).
             07 (SF)-IIMPST-PRVR-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IIMPST-PRVR-DFZ-NULL REDEFINES
                (SF)-IIMPST-PRVR-DFZ   PIC X(8).
             07 (SF)-IIMPST-252-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IIMPST-252-CALC-NULL REDEFINES
                (SF)-IIMPST-252-CALC   PIC X(8).
             07 (SF)-IIMPST-252-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IIMPST-252-EFFLQ-NULL REDEFINES
                (SF)-IIMPST-252-EFFLQ   PIC X(8).
             07 (SF)-IIMPST-252-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IIMPST-252-DFZ-NULL REDEFINES
                (SF)-IIMPST-252-DFZ   PIC X(8).
             07 (SF)-IMPST-252-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-252-CALC-NULL REDEFINES
                (SF)-IMPST-252-CALC   PIC X(8).
             07 (SF)-IMPST-252-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-252-EFFLQ-NULL REDEFINES
                (SF)-IMPST-252-EFFLQ   PIC X(8).
             07 (SF)-RIT-TFR-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIT-TFR-CALC-NULL REDEFINES
                (SF)-RIT-TFR-CALC   PIC X(8).
             07 (SF)-RIT-TFR-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIT-TFR-EFFLQ-NULL REDEFINES
                (SF)-RIT-TFR-EFFLQ   PIC X(8).
             07 (SF)-RIT-TFR-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIT-TFR-DFZ-NULL REDEFINES
                (SF)-RIT-TFR-DFZ   PIC X(8).
             07 (SF)-CNBT-INPSTFM-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-CNBT-INPSTFM-CALC-NULL REDEFINES
                (SF)-CNBT-INPSTFM-CALC   PIC X(8).
             07 (SF)-CNBT-INPSTFM-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-CNBT-INPSTFM-EFFLQ-NULL REDEFINES
                (SF)-CNBT-INPSTFM-EFFLQ   PIC X(8).
             07 (SF)-CNBT-INPSTFM-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-CNBT-INPSTFM-DFZ-NULL REDEFINES
                (SF)-CNBT-INPSTFM-DFZ   PIC X(8).
             07 (SF)-ICNB-INPSTFM-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-ICNB-INPSTFM-CALC-NULL REDEFINES
                (SF)-ICNB-INPSTFM-CALC   PIC X(8).
             07 (SF)-ICNB-INPSTFM-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-ICNB-INPSTFM-EFFLQ-NULL REDEFINES
                (SF)-ICNB-INPSTFM-EFFLQ   PIC X(8).
             07 (SF)-ICNB-INPSTFM-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-ICNB-INPSTFM-DFZ-NULL REDEFINES
                (SF)-ICNB-INPSTFM-DFZ   PIC X(8).
             07 (SF)-CNDE-END2000-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-CNDE-END2000-CALC-NULL REDEFINES
                (SF)-CNDE-END2000-CALC   PIC X(8).
             07 (SF)-CNDE-END2000-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-CNDE-END2000-EFFLQ-NULL REDEFINES
                (SF)-CNDE-END2000-EFFLQ   PIC X(8).
             07 (SF)-CNDE-END2000-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-CNDE-END2000-DFZ-NULL REDEFINES
                (SF)-CNDE-END2000-DFZ   PIC X(8).
             07 (SF)-CNDE-END2006-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-CNDE-END2006-CALC-NULL REDEFINES
                (SF)-CNDE-END2006-CALC   PIC X(8).
             07 (SF)-CNDE-END2006-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-CNDE-END2006-EFFLQ-NULL REDEFINES
                (SF)-CNDE-END2006-EFFLQ   PIC X(8).
             07 (SF)-CNDE-END2006-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-CNDE-END2006-DFZ-NULL REDEFINES
                (SF)-CNDE-END2006-DFZ   PIC X(8).
             07 (SF)-CNDE-DAL2007-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-CNDE-DAL2007-CALC-NULL REDEFINES
                (SF)-CNDE-DAL2007-CALC   PIC X(8).
             07 (SF)-CNDE-DAL2007-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-CNDE-DAL2007-EFFLQ-NULL REDEFINES
                (SF)-CNDE-DAL2007-EFFLQ   PIC X(8).
             07 (SF)-CNDE-DAL2007-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-CNDE-DAL2007-DFZ-NULL REDEFINES
                (SF)-CNDE-DAL2007-DFZ   PIC X(8).
             07 (SF)-AA-CNBZ-END2000-EF PIC S9(5)     COMP-3.
             07 (SF)-AA-CNBZ-END2000-EF-NULL REDEFINES
                (SF)-AA-CNBZ-END2000-EF   PIC X(3).
             07 (SF)-AA-CNBZ-END2006-EF PIC S9(5)     COMP-3.
             07 (SF)-AA-CNBZ-END2006-EF-NULL REDEFINES
                (SF)-AA-CNBZ-END2006-EF   PIC X(3).
             07 (SF)-AA-CNBZ-DAL2007-EF PIC S9(5)     COMP-3.
             07 (SF)-AA-CNBZ-DAL2007-EF-NULL REDEFINES
                (SF)-AA-CNBZ-DAL2007-EF   PIC X(3).
             07 (SF)-MM-CNBZ-END2000-EF PIC S9(5)     COMP-3.
             07 (SF)-MM-CNBZ-END2000-EF-NULL REDEFINES
                (SF)-MM-CNBZ-END2000-EF   PIC X(3).
             07 (SF)-MM-CNBZ-END2006-EF PIC S9(5)     COMP-3.
             07 (SF)-MM-CNBZ-END2006-EF-NULL REDEFINES
                (SF)-MM-CNBZ-END2006-EF   PIC X(3).
             07 (SF)-MM-CNBZ-DAL2007-EF PIC S9(5)     COMP-3.
             07 (SF)-MM-CNBZ-DAL2007-EF-NULL REDEFINES
                (SF)-MM-CNBZ-DAL2007-EF   PIC X(3).
             07 (SF)-IMPST-DA-RIMB-EFF PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-DA-RIMB-EFF-NULL REDEFINES
                (SF)-IMPST-DA-RIMB-EFF   PIC X(8).
             07 (SF)-ALQ-TAX-SEP-CALC PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-TAX-SEP-CALC-NULL REDEFINES
                (SF)-ALQ-TAX-SEP-CALC   PIC X(4).
             07 (SF)-ALQ-TAX-SEP-EFFLQ PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-TAX-SEP-EFFLQ-NULL REDEFINES
                (SF)-ALQ-TAX-SEP-EFFLQ   PIC X(4).
             07 (SF)-ALQ-TAX-SEP-DFZ PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-TAX-SEP-DFZ-NULL REDEFINES
                (SF)-ALQ-TAX-SEP-DFZ   PIC X(4).
             07 (SF)-ALQ-CNBT-INPSTFM-C PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-CNBT-INPSTFM-C-NULL REDEFINES
                (SF)-ALQ-CNBT-INPSTFM-C   PIC X(4).
             07 (SF)-ALQ-CNBT-INPSTFM-E PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-CNBT-INPSTFM-E-NULL REDEFINES
                (SF)-ALQ-CNBT-INPSTFM-E   PIC X(4).
             07 (SF)-ALQ-CNBT-INPSTFM-D PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-CNBT-INPSTFM-D-NULL REDEFINES
                (SF)-ALQ-CNBT-INPSTFM-D   PIC X(4).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-IMPB-VIS-1382011C PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-1382011C-NULL REDEFINES
                (SF)-IMPB-VIS-1382011C   PIC X(8).
             07 (SF)-IMPB-VIS-1382011D PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-1382011D-NULL REDEFINES
                (SF)-IMPB-VIS-1382011D   PIC X(8).
             07 (SF)-IMPB-VIS-1382011L PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-1382011L-NULL REDEFINES
                (SF)-IMPB-VIS-1382011L   PIC X(8).
             07 (SF)-IMPST-VIS-1382011C PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-1382011C-NULL REDEFINES
                (SF)-IMPST-VIS-1382011C   PIC X(8).
             07 (SF)-IMPST-VIS-1382011D PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-1382011D-NULL REDEFINES
                (SF)-IMPST-VIS-1382011D   PIC X(8).
             07 (SF)-IMPST-VIS-1382011L PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-1382011L-NULL REDEFINES
                (SF)-IMPST-VIS-1382011L   PIC X(8).
             07 (SF)-IMPB-IS-1382011C PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-1382011C-NULL REDEFINES
                (SF)-IMPB-IS-1382011C   PIC X(8).
             07 (SF)-IMPB-IS-1382011D PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-1382011D-NULL REDEFINES
                (SF)-IMPB-IS-1382011D   PIC X(8).
             07 (SF)-IMPB-IS-1382011L PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-1382011L-NULL REDEFINES
                (SF)-IMPB-IS-1382011L   PIC X(8).
             07 (SF)-IS-1382011C PIC S9(12)V9(3) COMP-3.
             07 (SF)-IS-1382011C-NULL REDEFINES
                (SF)-IS-1382011C   PIC X(8).
             07 (SF)-IS-1382011D PIC S9(12)V9(3) COMP-3.
             07 (SF)-IS-1382011D-NULL REDEFINES
                (SF)-IS-1382011D   PIC X(8).
             07 (SF)-IS-1382011L PIC S9(12)V9(3) COMP-3.
             07 (SF)-IS-1382011L-NULL REDEFINES
                (SF)-IS-1382011L   PIC X(8).
             07 (SF)-IMP-INTR-RIT-PAG-C PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-INTR-RIT-PAG-C-NULL REDEFINES
                (SF)-IMP-INTR-RIT-PAG-C   PIC X(8).
             07 (SF)-IMP-INTR-RIT-PAG-D PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-INTR-RIT-PAG-D-NULL REDEFINES
                (SF)-IMP-INTR-RIT-PAG-D   PIC X(8).
             07 (SF)-IMP-INTR-RIT-PAG-L PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-INTR-RIT-PAG-L-NULL REDEFINES
                (SF)-IMP-INTR-RIT-PAG-L   PIC X(8).
             07 (SF)-IMPB-BOLLO-DETT-C PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-BOLLO-DETT-C-NULL REDEFINES
                (SF)-IMPB-BOLLO-DETT-C   PIC X(8).
             07 (SF)-IMPB-BOLLO-DETT-D PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-BOLLO-DETT-D-NULL REDEFINES
                (SF)-IMPB-BOLLO-DETT-D   PIC X(8).
             07 (SF)-IMPB-BOLLO-DETT-L PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-BOLLO-DETT-L-NULL REDEFINES
                (SF)-IMPB-BOLLO-DETT-L   PIC X(8).
             07 (SF)-IMPST-BOLLO-DETT-C PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-BOLLO-DETT-C-NULL REDEFINES
                (SF)-IMPST-BOLLO-DETT-C   PIC X(8).
             07 (SF)-IMPST-BOLLO-DETT-D PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-BOLLO-DETT-D-NULL REDEFINES
                (SF)-IMPST-BOLLO-DETT-D   PIC X(8).
             07 (SF)-IMPST-BOLLO-DETT-L PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-BOLLO-DETT-L-NULL REDEFINES
                (SF)-IMPST-BOLLO-DETT-L   PIC X(8).
             07 (SF)-IMPST-BOLLO-TOT-VC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-BOLLO-TOT-VC-NULL REDEFINES
                (SF)-IMPST-BOLLO-TOT-VC   PIC X(8).
             07 (SF)-IMPST-BOLLO-TOT-VD PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-BOLLO-TOT-VD-NULL REDEFINES
                (SF)-IMPST-BOLLO-TOT-VD   PIC X(8).
             07 (SF)-IMPST-BOLLO-TOT-VL PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-BOLLO-TOT-VL-NULL REDEFINES
                (SF)-IMPST-BOLLO-TOT-VL   PIC X(8).
             07 (SF)-IMPB-VIS-662014C PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-662014C-NULL REDEFINES
                (SF)-IMPB-VIS-662014C   PIC X(8).
             07 (SF)-IMPB-VIS-662014D PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-662014D-NULL REDEFINES
                (SF)-IMPB-VIS-662014D   PIC X(8).
             07 (SF)-IMPB-VIS-662014L PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-662014L-NULL REDEFINES
                (SF)-IMPB-VIS-662014L   PIC X(8).
             07 (SF)-IMPST-VIS-662014C PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-662014C-NULL REDEFINES
                (SF)-IMPST-VIS-662014C   PIC X(8).
             07 (SF)-IMPST-VIS-662014D PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-662014D-NULL REDEFINES
                (SF)-IMPST-VIS-662014D   PIC X(8).
             07 (SF)-IMPST-VIS-662014L PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-662014L-NULL REDEFINES
                (SF)-IMPST-VIS-662014L   PIC X(8).
             07 (SF)-IMPB-IS-662014C PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-662014C-NULL REDEFINES
                (SF)-IMPB-IS-662014C   PIC X(8).
             07 (SF)-IMPB-IS-662014D PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-662014D-NULL REDEFINES
                (SF)-IMPB-IS-662014D   PIC X(8).
             07 (SF)-IMPB-IS-662014L PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-662014L-NULL REDEFINES
                (SF)-IMPB-IS-662014L   PIC X(8).
             07 (SF)-IS-662014C PIC S9(12)V9(3) COMP-3.
             07 (SF)-IS-662014C-NULL REDEFINES
                (SF)-IS-662014C   PIC X(8).
             07 (SF)-IS-662014D PIC S9(12)V9(3) COMP-3.
             07 (SF)-IS-662014D-NULL REDEFINES
                (SF)-IS-662014D   PIC X(8).
             07 (SF)-IS-662014L PIC S9(12)V9(3) COMP-3.
             07 (SF)-IS-662014L-NULL REDEFINES
                (SF)-IS-662014L   PIC X(8).

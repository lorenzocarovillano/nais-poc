      *----------------------------------------------------------------*
      *    COPY      ..... LCCVSTB6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO STATO OGGETTO BUS
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-STAT-OGG-BUS.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE STAT-OGG-BUS.

      *--> NOME TABELLA FISICA DB
           MOVE 'STAT-OGG-BUS'                 TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WSTB-ST-INV(IX-TAB-STB)
              AND WSTB-ELE-STAT-OGG-MAX NOT = 0


              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WSTB-ST-ADD(IX-TAB-STB)

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK
                        MOVE S090-SEQ-TABELLA
                          TO WSTB-ID-STAT-OGG-BUS(IX-TAB-STB)
                        MOVE WMOV-ID-PTF
                          TO WSTB-ID-MOVI-CRZ(IX-TAB-STB)
                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        MODIFICA
                  WHEN WSTB-ST-MOD(IX-TAB-STB)

                     MOVE WMOV-ID-PTF
                       TO WSTB-ID-MOVI-CRZ(IX-TAB-STB)

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WSTB-ST-DEL(IX-TAB-STB)

                     MOVE WMOV-ID-PTF
                       TO WSTB-ID-MOVI-CRZ(IX-TAB-STB)

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-DELETE-LOGICA  TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN STATO OGGETTO BUSINESS
                 PERFORM VAL-DCLGEN-STB
                    THRU VAL-DCLGEN-STB-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-STB
                    THRU VALORIZZA-AREA-DSH-STB-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-STAT-OGG-BUS-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-STB.

      *--> DCLGEN TABELLA
           MOVE STAT-OGG-BUS            TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-STB-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVDFL5
      *   ULTIMO AGG. 02 LUG 2014
      *------------------------------------------------------------

       VAL-DCLGEN-DFL.
           IF (SF)-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL
              TO DFL-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU
              TO DFL-ID-MOVI-CHIU
           END-IF
           MOVE (SF)-COD-COMP-ANIA
              TO DFL-COD-COMP-ANIA
           IF (SF)-DT-INI-EFF NOT NUMERIC
              MOVE 0 TO DFL-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF
              TO DFL-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF NOT NUMERIC
              MOVE 0 TO DFL-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF
              TO DFL-DT-END-EFF
           END-IF
           IF (SF)-IMP-LRD-CALC-NULL = HIGH-VALUES
              MOVE (SF)-IMP-LRD-CALC-NULL
              TO DFL-IMP-LRD-CALC-NULL
           ELSE
              MOVE (SF)-IMP-LRD-CALC
              TO DFL-IMP-LRD-CALC
           END-IF
           IF (SF)-IMP-LRD-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-IMP-LRD-DFZ-NULL
              TO DFL-IMP-LRD-DFZ-NULL
           ELSE
              MOVE (SF)-IMP-LRD-DFZ
              TO DFL-IMP-LRD-DFZ
           END-IF
           IF (SF)-IMP-LRD-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-IMP-LRD-EFFLQ-NULL
              TO DFL-IMP-LRD-EFFLQ-NULL
           ELSE
              MOVE (SF)-IMP-LRD-EFFLQ
              TO DFL-IMP-LRD-EFFLQ
           END-IF
           IF (SF)-IMP-NET-CALC-NULL = HIGH-VALUES
              MOVE (SF)-IMP-NET-CALC-NULL
              TO DFL-IMP-NET-CALC-NULL
           ELSE
              MOVE (SF)-IMP-NET-CALC
              TO DFL-IMP-NET-CALC
           END-IF
           IF (SF)-IMP-NET-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-IMP-NET-DFZ-NULL
              TO DFL-IMP-NET-DFZ-NULL
           ELSE
              MOVE (SF)-IMP-NET-DFZ
              TO DFL-IMP-NET-DFZ
           END-IF
           IF (SF)-IMP-NET-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-IMP-NET-EFFLQ-NULL
              TO DFL-IMP-NET-EFFLQ-NULL
           ELSE
              MOVE (SF)-IMP-NET-EFFLQ
              TO DFL-IMP-NET-EFFLQ
           END-IF
           IF (SF)-IMPST-PRVR-CALC-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-PRVR-CALC-NULL
              TO DFL-IMPST-PRVR-CALC-NULL
           ELSE
              MOVE (SF)-IMPST-PRVR-CALC
              TO DFL-IMPST-PRVR-CALC
           END-IF
           IF (SF)-IMPST-PRVR-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-PRVR-DFZ-NULL
              TO DFL-IMPST-PRVR-DFZ-NULL
           ELSE
              MOVE (SF)-IMPST-PRVR-DFZ
              TO DFL-IMPST-PRVR-DFZ
           END-IF
           IF (SF)-IMPST-PRVR-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-PRVR-EFFLQ-NULL
              TO DFL-IMPST-PRVR-EFFLQ-NULL
           ELSE
              MOVE (SF)-IMPST-PRVR-EFFLQ
              TO DFL-IMPST-PRVR-EFFLQ
           END-IF
           IF (SF)-IMPST-VIS-CALC-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-CALC-NULL
              TO DFL-IMPST-VIS-CALC-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-CALC
              TO DFL-IMPST-VIS-CALC
           END-IF
           IF (SF)-IMPST-VIS-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-DFZ-NULL
              TO DFL-IMPST-VIS-DFZ-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-DFZ
              TO DFL-IMPST-VIS-DFZ
           END-IF
           IF (SF)-IMPST-VIS-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-EFFLQ-NULL
              TO DFL-IMPST-VIS-EFFLQ-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-EFFLQ
              TO DFL-IMPST-VIS-EFFLQ
           END-IF
           IF (SF)-RIT-ACC-CALC-NULL = HIGH-VALUES
              MOVE (SF)-RIT-ACC-CALC-NULL
              TO DFL-RIT-ACC-CALC-NULL
           ELSE
              MOVE (SF)-RIT-ACC-CALC
              TO DFL-RIT-ACC-CALC
           END-IF
           IF (SF)-RIT-ACC-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-RIT-ACC-DFZ-NULL
              TO DFL-RIT-ACC-DFZ-NULL
           ELSE
              MOVE (SF)-RIT-ACC-DFZ
              TO DFL-RIT-ACC-DFZ
           END-IF
           IF (SF)-RIT-ACC-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-RIT-ACC-EFFLQ-NULL
              TO DFL-RIT-ACC-EFFLQ-NULL
           ELSE
              MOVE (SF)-RIT-ACC-EFFLQ
              TO DFL-RIT-ACC-EFFLQ
           END-IF
           IF (SF)-RIT-IRPEF-CALC-NULL = HIGH-VALUES
              MOVE (SF)-RIT-IRPEF-CALC-NULL
              TO DFL-RIT-IRPEF-CALC-NULL
           ELSE
              MOVE (SF)-RIT-IRPEF-CALC
              TO DFL-RIT-IRPEF-CALC
           END-IF
           IF (SF)-RIT-IRPEF-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-RIT-IRPEF-DFZ-NULL
              TO DFL-RIT-IRPEF-DFZ-NULL
           ELSE
              MOVE (SF)-RIT-IRPEF-DFZ
              TO DFL-RIT-IRPEF-DFZ
           END-IF
           IF (SF)-RIT-IRPEF-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-RIT-IRPEF-EFFLQ-NULL
              TO DFL-RIT-IRPEF-EFFLQ-NULL
           ELSE
              MOVE (SF)-RIT-IRPEF-EFFLQ
              TO DFL-RIT-IRPEF-EFFLQ
           END-IF
           IF (SF)-IMPST-SOST-CALC-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-SOST-CALC-NULL
              TO DFL-IMPST-SOST-CALC-NULL
           ELSE
              MOVE (SF)-IMPST-SOST-CALC
              TO DFL-IMPST-SOST-CALC
           END-IF
           IF (SF)-IMPST-SOST-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-SOST-DFZ-NULL
              TO DFL-IMPST-SOST-DFZ-NULL
           ELSE
              MOVE (SF)-IMPST-SOST-DFZ
              TO DFL-IMPST-SOST-DFZ
           END-IF
           IF (SF)-IMPST-SOST-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-SOST-EFFLQ-NULL
              TO DFL-IMPST-SOST-EFFLQ-NULL
           ELSE
              MOVE (SF)-IMPST-SOST-EFFLQ
              TO DFL-IMPST-SOST-EFFLQ
           END-IF
           IF (SF)-TAX-SEP-CALC-NULL = HIGH-VALUES
              MOVE (SF)-TAX-SEP-CALC-NULL
              TO DFL-TAX-SEP-CALC-NULL
           ELSE
              MOVE (SF)-TAX-SEP-CALC
              TO DFL-TAX-SEP-CALC
           END-IF
           IF (SF)-TAX-SEP-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-TAX-SEP-DFZ-NULL
              TO DFL-TAX-SEP-DFZ-NULL
           ELSE
              MOVE (SF)-TAX-SEP-DFZ
              TO DFL-TAX-SEP-DFZ
           END-IF
           IF (SF)-TAX-SEP-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-TAX-SEP-EFFLQ-NULL
              TO DFL-TAX-SEP-EFFLQ-NULL
           ELSE
              MOVE (SF)-TAX-SEP-EFFLQ
              TO DFL-TAX-SEP-EFFLQ
           END-IF
           IF (SF)-INTR-PREST-CALC-NULL = HIGH-VALUES
              MOVE (SF)-INTR-PREST-CALC-NULL
              TO DFL-INTR-PREST-CALC-NULL
           ELSE
              MOVE (SF)-INTR-PREST-CALC
              TO DFL-INTR-PREST-CALC
           END-IF
           IF (SF)-INTR-PREST-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-INTR-PREST-DFZ-NULL
              TO DFL-INTR-PREST-DFZ-NULL
           ELSE
              MOVE (SF)-INTR-PREST-DFZ
              TO DFL-INTR-PREST-DFZ
           END-IF
           IF (SF)-INTR-PREST-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-INTR-PREST-EFFLQ-NULL
              TO DFL-INTR-PREST-EFFLQ-NULL
           ELSE
              MOVE (SF)-INTR-PREST-EFFLQ
              TO DFL-INTR-PREST-EFFLQ
           END-IF
           IF (SF)-ACCPRE-SOST-CALC-NULL = HIGH-VALUES
              MOVE (SF)-ACCPRE-SOST-CALC-NULL
              TO DFL-ACCPRE-SOST-CALC-NULL
           ELSE
              MOVE (SF)-ACCPRE-SOST-CALC
              TO DFL-ACCPRE-SOST-CALC
           END-IF
           IF (SF)-ACCPRE-SOST-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-ACCPRE-SOST-DFZ-NULL
              TO DFL-ACCPRE-SOST-DFZ-NULL
           ELSE
              MOVE (SF)-ACCPRE-SOST-DFZ
              TO DFL-ACCPRE-SOST-DFZ
           END-IF
           IF (SF)-ACCPRE-SOST-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-ACCPRE-SOST-EFFLQ-NULL
              TO DFL-ACCPRE-SOST-EFFLQ-NULL
           ELSE
              MOVE (SF)-ACCPRE-SOST-EFFLQ
              TO DFL-ACCPRE-SOST-EFFLQ
           END-IF
           IF (SF)-ACCPRE-VIS-CALC-NULL = HIGH-VALUES
              MOVE (SF)-ACCPRE-VIS-CALC-NULL
              TO DFL-ACCPRE-VIS-CALC-NULL
           ELSE
              MOVE (SF)-ACCPRE-VIS-CALC
              TO DFL-ACCPRE-VIS-CALC
           END-IF
           IF (SF)-ACCPRE-VIS-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-ACCPRE-VIS-DFZ-NULL
              TO DFL-ACCPRE-VIS-DFZ-NULL
           ELSE
              MOVE (SF)-ACCPRE-VIS-DFZ
              TO DFL-ACCPRE-VIS-DFZ
           END-IF
           IF (SF)-ACCPRE-VIS-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-ACCPRE-VIS-EFFLQ-NULL
              TO DFL-ACCPRE-VIS-EFFLQ-NULL
           ELSE
              MOVE (SF)-ACCPRE-VIS-EFFLQ
              TO DFL-ACCPRE-VIS-EFFLQ
           END-IF
           IF (SF)-ACCPRE-ACC-CALC-NULL = HIGH-VALUES
              MOVE (SF)-ACCPRE-ACC-CALC-NULL
              TO DFL-ACCPRE-ACC-CALC-NULL
           ELSE
              MOVE (SF)-ACCPRE-ACC-CALC
              TO DFL-ACCPRE-ACC-CALC
           END-IF
           IF (SF)-ACCPRE-ACC-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-ACCPRE-ACC-DFZ-NULL
              TO DFL-ACCPRE-ACC-DFZ-NULL
           ELSE
              MOVE (SF)-ACCPRE-ACC-DFZ
              TO DFL-ACCPRE-ACC-DFZ
           END-IF
           IF (SF)-ACCPRE-ACC-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-ACCPRE-ACC-EFFLQ-NULL
              TO DFL-ACCPRE-ACC-EFFLQ-NULL
           ELSE
              MOVE (SF)-ACCPRE-ACC-EFFLQ
              TO DFL-ACCPRE-ACC-EFFLQ
           END-IF
           IF (SF)-RES-PRSTZ-CALC-NULL = HIGH-VALUES
              MOVE (SF)-RES-PRSTZ-CALC-NULL
              TO DFL-RES-PRSTZ-CALC-NULL
           ELSE
              MOVE (SF)-RES-PRSTZ-CALC
              TO DFL-RES-PRSTZ-CALC
           END-IF
           IF (SF)-RES-PRSTZ-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-RES-PRSTZ-DFZ-NULL
              TO DFL-RES-PRSTZ-DFZ-NULL
           ELSE
              MOVE (SF)-RES-PRSTZ-DFZ
              TO DFL-RES-PRSTZ-DFZ
           END-IF
           IF (SF)-RES-PRSTZ-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-RES-PRSTZ-EFFLQ-NULL
              TO DFL-RES-PRSTZ-EFFLQ-NULL
           ELSE
              MOVE (SF)-RES-PRSTZ-EFFLQ
              TO DFL-RES-PRSTZ-EFFLQ
           END-IF
           IF (SF)-RES-PRE-ATT-CALC-NULL = HIGH-VALUES
              MOVE (SF)-RES-PRE-ATT-CALC-NULL
              TO DFL-RES-PRE-ATT-CALC-NULL
           ELSE
              MOVE (SF)-RES-PRE-ATT-CALC
              TO DFL-RES-PRE-ATT-CALC
           END-IF
           IF (SF)-RES-PRE-ATT-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-RES-PRE-ATT-DFZ-NULL
              TO DFL-RES-PRE-ATT-DFZ-NULL
           ELSE
              MOVE (SF)-RES-PRE-ATT-DFZ
              TO DFL-RES-PRE-ATT-DFZ
           END-IF
           IF (SF)-RES-PRE-ATT-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-RES-PRE-ATT-EFFLQ-NULL
              TO DFL-RES-PRE-ATT-EFFLQ-NULL
           ELSE
              MOVE (SF)-RES-PRE-ATT-EFFLQ
              TO DFL-RES-PRE-ATT-EFFLQ
           END-IF
           IF (SF)-IMP-EXCONTR-EFF-NULL = HIGH-VALUES
              MOVE (SF)-IMP-EXCONTR-EFF-NULL
              TO DFL-IMP-EXCONTR-EFF-NULL
           ELSE
              MOVE (SF)-IMP-EXCONTR-EFF
              TO DFL-IMP-EXCONTR-EFF
           END-IF
           IF (SF)-IMPB-VIS-CALC-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-CALC-NULL
              TO DFL-IMPB-VIS-CALC-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-CALC
              TO DFL-IMPB-VIS-CALC
           END-IF
           IF (SF)-IMPB-VIS-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-EFFLQ-NULL
              TO DFL-IMPB-VIS-EFFLQ-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-EFFLQ
              TO DFL-IMPB-VIS-EFFLQ
           END-IF
           IF (SF)-IMPB-VIS-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-DFZ-NULL
              TO DFL-IMPB-VIS-DFZ-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-DFZ
              TO DFL-IMPB-VIS-DFZ
           END-IF
           IF (SF)-IMPB-RIT-ACC-CALC-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-RIT-ACC-CALC-NULL
              TO DFL-IMPB-RIT-ACC-CALC-NULL
           ELSE
              MOVE (SF)-IMPB-RIT-ACC-CALC
              TO DFL-IMPB-RIT-ACC-CALC
           END-IF
           IF (SF)-IMPB-RIT-ACC-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-RIT-ACC-EFFLQ-NULL
              TO DFL-IMPB-RIT-ACC-EFFLQ-NULL
           ELSE
              MOVE (SF)-IMPB-RIT-ACC-EFFLQ
              TO DFL-IMPB-RIT-ACC-EFFLQ
           END-IF
           IF (SF)-IMPB-RIT-ACC-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-RIT-ACC-DFZ-NULL
              TO DFL-IMPB-RIT-ACC-DFZ-NULL
           ELSE
              MOVE (SF)-IMPB-RIT-ACC-DFZ
              TO DFL-IMPB-RIT-ACC-DFZ
           END-IF
           IF (SF)-IMPB-TFR-CALC-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-TFR-CALC-NULL
              TO DFL-IMPB-TFR-CALC-NULL
           ELSE
              MOVE (SF)-IMPB-TFR-CALC
              TO DFL-IMPB-TFR-CALC
           END-IF
           IF (SF)-IMPB-TFR-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-TFR-EFFLQ-NULL
              TO DFL-IMPB-TFR-EFFLQ-NULL
           ELSE
              MOVE (SF)-IMPB-TFR-EFFLQ
              TO DFL-IMPB-TFR-EFFLQ
           END-IF
           IF (SF)-IMPB-TFR-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-TFR-DFZ-NULL
              TO DFL-IMPB-TFR-DFZ-NULL
           ELSE
              MOVE (SF)-IMPB-TFR-DFZ
              TO DFL-IMPB-TFR-DFZ
           END-IF
           IF (SF)-IMPB-IS-CALC-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-CALC-NULL
              TO DFL-IMPB-IS-CALC-NULL
           ELSE
              MOVE (SF)-IMPB-IS-CALC
              TO DFL-IMPB-IS-CALC
           END-IF
           IF (SF)-IMPB-IS-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-EFFLQ-NULL
              TO DFL-IMPB-IS-EFFLQ-NULL
           ELSE
              MOVE (SF)-IMPB-IS-EFFLQ
              TO DFL-IMPB-IS-EFFLQ
           END-IF
           IF (SF)-IMPB-IS-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-DFZ-NULL
              TO DFL-IMPB-IS-DFZ-NULL
           ELSE
              MOVE (SF)-IMPB-IS-DFZ
              TO DFL-IMPB-IS-DFZ
           END-IF
           IF (SF)-IMPB-TAX-SEP-CALC-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-TAX-SEP-CALC-NULL
              TO DFL-IMPB-TAX-SEP-CALC-NULL
           ELSE
              MOVE (SF)-IMPB-TAX-SEP-CALC
              TO DFL-IMPB-TAX-SEP-CALC
           END-IF
           IF (SF)-IMPB-TAX-SEP-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-TAX-SEP-EFFLQ-NULL
              TO DFL-IMPB-TAX-SEP-EFFLQ-NULL
           ELSE
              MOVE (SF)-IMPB-TAX-SEP-EFFLQ
              TO DFL-IMPB-TAX-SEP-EFFLQ
           END-IF
           IF (SF)-IMPB-TAX-SEP-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-TAX-SEP-DFZ-NULL
              TO DFL-IMPB-TAX-SEP-DFZ-NULL
           ELSE
              MOVE (SF)-IMPB-TAX-SEP-DFZ
              TO DFL-IMPB-TAX-SEP-DFZ
           END-IF
           IF (SF)-IINT-PREST-CALC-NULL = HIGH-VALUES
              MOVE (SF)-IINT-PREST-CALC-NULL
              TO DFL-IINT-PREST-CALC-NULL
           ELSE
              MOVE (SF)-IINT-PREST-CALC
              TO DFL-IINT-PREST-CALC
           END-IF
           IF (SF)-IINT-PREST-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-IINT-PREST-EFFLQ-NULL
              TO DFL-IINT-PREST-EFFLQ-NULL
           ELSE
              MOVE (SF)-IINT-PREST-EFFLQ
              TO DFL-IINT-PREST-EFFLQ
           END-IF
           IF (SF)-IINT-PREST-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-IINT-PREST-DFZ-NULL
              TO DFL-IINT-PREST-DFZ-NULL
           ELSE
              MOVE (SF)-IINT-PREST-DFZ
              TO DFL-IINT-PREST-DFZ
           END-IF
           IF (SF)-MONT-END2000-CALC-NULL = HIGH-VALUES
              MOVE (SF)-MONT-END2000-CALC-NULL
              TO DFL-MONT-END2000-CALC-NULL
           ELSE
              MOVE (SF)-MONT-END2000-CALC
              TO DFL-MONT-END2000-CALC
           END-IF
           IF (SF)-MONT-END2000-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-MONT-END2000-EFFLQ-NULL
              TO DFL-MONT-END2000-EFFLQ-NULL
           ELSE
              MOVE (SF)-MONT-END2000-EFFLQ
              TO DFL-MONT-END2000-EFFLQ
           END-IF
           IF (SF)-MONT-END2000-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-MONT-END2000-DFZ-NULL
              TO DFL-MONT-END2000-DFZ-NULL
           ELSE
              MOVE (SF)-MONT-END2000-DFZ
              TO DFL-MONT-END2000-DFZ
           END-IF
           IF (SF)-MONT-END2006-CALC-NULL = HIGH-VALUES
              MOVE (SF)-MONT-END2006-CALC-NULL
              TO DFL-MONT-END2006-CALC-NULL
           ELSE
              MOVE (SF)-MONT-END2006-CALC
              TO DFL-MONT-END2006-CALC
           END-IF
           IF (SF)-MONT-END2006-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-MONT-END2006-EFFLQ-NULL
              TO DFL-MONT-END2006-EFFLQ-NULL
           ELSE
              MOVE (SF)-MONT-END2006-EFFLQ
              TO DFL-MONT-END2006-EFFLQ
           END-IF
           IF (SF)-MONT-END2006-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-MONT-END2006-DFZ-NULL
              TO DFL-MONT-END2006-DFZ-NULL
           ELSE
              MOVE (SF)-MONT-END2006-DFZ
              TO DFL-MONT-END2006-DFZ
           END-IF
           IF (SF)-MONT-DAL2007-CALC-NULL = HIGH-VALUES
              MOVE (SF)-MONT-DAL2007-CALC-NULL
              TO DFL-MONT-DAL2007-CALC-NULL
           ELSE
              MOVE (SF)-MONT-DAL2007-CALC
              TO DFL-MONT-DAL2007-CALC
           END-IF
           IF (SF)-MONT-DAL2007-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-MONT-DAL2007-EFFLQ-NULL
              TO DFL-MONT-DAL2007-EFFLQ-NULL
           ELSE
              MOVE (SF)-MONT-DAL2007-EFFLQ
              TO DFL-MONT-DAL2007-EFFLQ
           END-IF
           IF (SF)-MONT-DAL2007-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-MONT-DAL2007-DFZ-NULL
              TO DFL-MONT-DAL2007-DFZ-NULL
           ELSE
              MOVE (SF)-MONT-DAL2007-DFZ
              TO DFL-MONT-DAL2007-DFZ
           END-IF
           IF (SF)-IIMPST-PRVR-CALC-NULL = HIGH-VALUES
              MOVE (SF)-IIMPST-PRVR-CALC-NULL
              TO DFL-IIMPST-PRVR-CALC-NULL
           ELSE
              MOVE (SF)-IIMPST-PRVR-CALC
              TO DFL-IIMPST-PRVR-CALC
           END-IF
           IF (SF)-IIMPST-PRVR-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-IIMPST-PRVR-EFFLQ-NULL
              TO DFL-IIMPST-PRVR-EFFLQ-NULL
           ELSE
              MOVE (SF)-IIMPST-PRVR-EFFLQ
              TO DFL-IIMPST-PRVR-EFFLQ
           END-IF
           IF (SF)-IIMPST-PRVR-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-IIMPST-PRVR-DFZ-NULL
              TO DFL-IIMPST-PRVR-DFZ-NULL
           ELSE
              MOVE (SF)-IIMPST-PRVR-DFZ
              TO DFL-IIMPST-PRVR-DFZ
           END-IF
           IF (SF)-IIMPST-252-CALC-NULL = HIGH-VALUES
              MOVE (SF)-IIMPST-252-CALC-NULL
              TO DFL-IIMPST-252-CALC-NULL
           ELSE
              MOVE (SF)-IIMPST-252-CALC
              TO DFL-IIMPST-252-CALC
           END-IF
           IF (SF)-IIMPST-252-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-IIMPST-252-EFFLQ-NULL
              TO DFL-IIMPST-252-EFFLQ-NULL
           ELSE
              MOVE (SF)-IIMPST-252-EFFLQ
              TO DFL-IIMPST-252-EFFLQ
           END-IF
           IF (SF)-IIMPST-252-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-IIMPST-252-DFZ-NULL
              TO DFL-IIMPST-252-DFZ-NULL
           ELSE
              MOVE (SF)-IIMPST-252-DFZ
              TO DFL-IIMPST-252-DFZ
           END-IF
           IF (SF)-IMPST-252-CALC-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-252-CALC-NULL
              TO DFL-IMPST-252-CALC-NULL
           ELSE
              MOVE (SF)-IMPST-252-CALC
              TO DFL-IMPST-252-CALC
           END-IF
           IF (SF)-IMPST-252-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-252-EFFLQ-NULL
              TO DFL-IMPST-252-EFFLQ-NULL
           ELSE
              MOVE (SF)-IMPST-252-EFFLQ
              TO DFL-IMPST-252-EFFLQ
           END-IF
           IF (SF)-RIT-TFR-CALC-NULL = HIGH-VALUES
              MOVE (SF)-RIT-TFR-CALC-NULL
              TO DFL-RIT-TFR-CALC-NULL
           ELSE
              MOVE (SF)-RIT-TFR-CALC
              TO DFL-RIT-TFR-CALC
           END-IF
           IF (SF)-RIT-TFR-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-RIT-TFR-EFFLQ-NULL
              TO DFL-RIT-TFR-EFFLQ-NULL
           ELSE
              MOVE (SF)-RIT-TFR-EFFLQ
              TO DFL-RIT-TFR-EFFLQ
           END-IF
           IF (SF)-RIT-TFR-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-RIT-TFR-DFZ-NULL
              TO DFL-RIT-TFR-DFZ-NULL
           ELSE
              MOVE (SF)-RIT-TFR-DFZ
              TO DFL-RIT-TFR-DFZ
           END-IF
           IF (SF)-CNBT-INPSTFM-CALC-NULL = HIGH-VALUES
              MOVE (SF)-CNBT-INPSTFM-CALC-NULL
              TO DFL-CNBT-INPSTFM-CALC-NULL
           ELSE
              MOVE (SF)-CNBT-INPSTFM-CALC
              TO DFL-CNBT-INPSTFM-CALC
           END-IF
           IF (SF)-CNBT-INPSTFM-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-CNBT-INPSTFM-EFFLQ-NULL
              TO DFL-CNBT-INPSTFM-EFFLQ-NULL
           ELSE
              MOVE (SF)-CNBT-INPSTFM-EFFLQ
              TO DFL-CNBT-INPSTFM-EFFLQ
           END-IF
           IF (SF)-CNBT-INPSTFM-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-CNBT-INPSTFM-DFZ-NULL
              TO DFL-CNBT-INPSTFM-DFZ-NULL
           ELSE
              MOVE (SF)-CNBT-INPSTFM-DFZ
              TO DFL-CNBT-INPSTFM-DFZ
           END-IF
           IF (SF)-ICNB-INPSTFM-CALC-NULL = HIGH-VALUES
              MOVE (SF)-ICNB-INPSTFM-CALC-NULL
              TO DFL-ICNB-INPSTFM-CALC-NULL
           ELSE
              MOVE (SF)-ICNB-INPSTFM-CALC
              TO DFL-ICNB-INPSTFM-CALC
           END-IF
           IF (SF)-ICNB-INPSTFM-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-ICNB-INPSTFM-EFFLQ-NULL
              TO DFL-ICNB-INPSTFM-EFFLQ-NULL
           ELSE
              MOVE (SF)-ICNB-INPSTFM-EFFLQ
              TO DFL-ICNB-INPSTFM-EFFLQ
           END-IF
           IF (SF)-ICNB-INPSTFM-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-ICNB-INPSTFM-DFZ-NULL
              TO DFL-ICNB-INPSTFM-DFZ-NULL
           ELSE
              MOVE (SF)-ICNB-INPSTFM-DFZ
              TO DFL-ICNB-INPSTFM-DFZ
           END-IF
           IF (SF)-CNDE-END2000-CALC-NULL = HIGH-VALUES
              MOVE (SF)-CNDE-END2000-CALC-NULL
              TO DFL-CNDE-END2000-CALC-NULL
           ELSE
              MOVE (SF)-CNDE-END2000-CALC
              TO DFL-CNDE-END2000-CALC
           END-IF
           IF (SF)-CNDE-END2000-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-CNDE-END2000-EFFLQ-NULL
              TO DFL-CNDE-END2000-EFFLQ-NULL
           ELSE
              MOVE (SF)-CNDE-END2000-EFFLQ
              TO DFL-CNDE-END2000-EFFLQ
           END-IF
           IF (SF)-CNDE-END2000-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-CNDE-END2000-DFZ-NULL
              TO DFL-CNDE-END2000-DFZ-NULL
           ELSE
              MOVE (SF)-CNDE-END2000-DFZ
              TO DFL-CNDE-END2000-DFZ
           END-IF
           IF (SF)-CNDE-END2006-CALC-NULL = HIGH-VALUES
              MOVE (SF)-CNDE-END2006-CALC-NULL
              TO DFL-CNDE-END2006-CALC-NULL
           ELSE
              MOVE (SF)-CNDE-END2006-CALC
              TO DFL-CNDE-END2006-CALC
           END-IF
           IF (SF)-CNDE-END2006-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-CNDE-END2006-EFFLQ-NULL
              TO DFL-CNDE-END2006-EFFLQ-NULL
           ELSE
              MOVE (SF)-CNDE-END2006-EFFLQ
              TO DFL-CNDE-END2006-EFFLQ
           END-IF
           IF (SF)-CNDE-END2006-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-CNDE-END2006-DFZ-NULL
              TO DFL-CNDE-END2006-DFZ-NULL
           ELSE
              MOVE (SF)-CNDE-END2006-DFZ
              TO DFL-CNDE-END2006-DFZ
           END-IF
           IF (SF)-CNDE-DAL2007-CALC-NULL = HIGH-VALUES
              MOVE (SF)-CNDE-DAL2007-CALC-NULL
              TO DFL-CNDE-DAL2007-CALC-NULL
           ELSE
              MOVE (SF)-CNDE-DAL2007-CALC
              TO DFL-CNDE-DAL2007-CALC
           END-IF
           IF (SF)-CNDE-DAL2007-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-CNDE-DAL2007-EFFLQ-NULL
              TO DFL-CNDE-DAL2007-EFFLQ-NULL
           ELSE
              MOVE (SF)-CNDE-DAL2007-EFFLQ
              TO DFL-CNDE-DAL2007-EFFLQ
           END-IF
           IF (SF)-CNDE-DAL2007-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-CNDE-DAL2007-DFZ-NULL
              TO DFL-CNDE-DAL2007-DFZ-NULL
           ELSE
              MOVE (SF)-CNDE-DAL2007-DFZ
              TO DFL-CNDE-DAL2007-DFZ
           END-IF
           IF (SF)-AA-CNBZ-END2000-EF-NULL = HIGH-VALUES
              MOVE (SF)-AA-CNBZ-END2000-EF-NULL
              TO DFL-AA-CNBZ-END2000-EF-NULL
           ELSE
              MOVE (SF)-AA-CNBZ-END2000-EF
              TO DFL-AA-CNBZ-END2000-EF
           END-IF
           IF (SF)-AA-CNBZ-END2006-EF-NULL = HIGH-VALUES
              MOVE (SF)-AA-CNBZ-END2006-EF-NULL
              TO DFL-AA-CNBZ-END2006-EF-NULL
           ELSE
              MOVE (SF)-AA-CNBZ-END2006-EF
              TO DFL-AA-CNBZ-END2006-EF
           END-IF
           IF (SF)-AA-CNBZ-DAL2007-EF-NULL = HIGH-VALUES
              MOVE (SF)-AA-CNBZ-DAL2007-EF-NULL
              TO DFL-AA-CNBZ-DAL2007-EF-NULL
           ELSE
              MOVE (SF)-AA-CNBZ-DAL2007-EF
              TO DFL-AA-CNBZ-DAL2007-EF
           END-IF
           IF (SF)-MM-CNBZ-END2000-EF-NULL = HIGH-VALUES
              MOVE (SF)-MM-CNBZ-END2000-EF-NULL
              TO DFL-MM-CNBZ-END2000-EF-NULL
           ELSE
              MOVE (SF)-MM-CNBZ-END2000-EF
              TO DFL-MM-CNBZ-END2000-EF
           END-IF
           IF (SF)-MM-CNBZ-END2006-EF-NULL = HIGH-VALUES
              MOVE (SF)-MM-CNBZ-END2006-EF-NULL
              TO DFL-MM-CNBZ-END2006-EF-NULL
           ELSE
              MOVE (SF)-MM-CNBZ-END2006-EF
              TO DFL-MM-CNBZ-END2006-EF
           END-IF
           IF (SF)-MM-CNBZ-DAL2007-EF-NULL = HIGH-VALUES
              MOVE (SF)-MM-CNBZ-DAL2007-EF-NULL
              TO DFL-MM-CNBZ-DAL2007-EF-NULL
           ELSE
              MOVE (SF)-MM-CNBZ-DAL2007-EF
              TO DFL-MM-CNBZ-DAL2007-EF
           END-IF
           IF (SF)-IMPST-DA-RIMB-EFF-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-DA-RIMB-EFF-NULL
              TO DFL-IMPST-DA-RIMB-EFF-NULL
           ELSE
              MOVE (SF)-IMPST-DA-RIMB-EFF
              TO DFL-IMPST-DA-RIMB-EFF
           END-IF
           IF (SF)-ALQ-TAX-SEP-CALC-NULL = HIGH-VALUES
              MOVE (SF)-ALQ-TAX-SEP-CALC-NULL
              TO DFL-ALQ-TAX-SEP-CALC-NULL
           ELSE
              MOVE (SF)-ALQ-TAX-SEP-CALC
              TO DFL-ALQ-TAX-SEP-CALC
           END-IF
           IF (SF)-ALQ-TAX-SEP-EFFLQ-NULL = HIGH-VALUES
              MOVE (SF)-ALQ-TAX-SEP-EFFLQ-NULL
              TO DFL-ALQ-TAX-SEP-EFFLQ-NULL
           ELSE
              MOVE (SF)-ALQ-TAX-SEP-EFFLQ
              TO DFL-ALQ-TAX-SEP-EFFLQ
           END-IF
           IF (SF)-ALQ-TAX-SEP-DFZ-NULL = HIGH-VALUES
              MOVE (SF)-ALQ-TAX-SEP-DFZ-NULL
              TO DFL-ALQ-TAX-SEP-DFZ-NULL
           ELSE
              MOVE (SF)-ALQ-TAX-SEP-DFZ
              TO DFL-ALQ-TAX-SEP-DFZ
           END-IF
           IF (SF)-ALQ-CNBT-INPSTFM-C-NULL = HIGH-VALUES
              MOVE (SF)-ALQ-CNBT-INPSTFM-C-NULL
              TO DFL-ALQ-CNBT-INPSTFM-C-NULL
           ELSE
              MOVE (SF)-ALQ-CNBT-INPSTFM-C
              TO DFL-ALQ-CNBT-INPSTFM-C
           END-IF
           IF (SF)-ALQ-CNBT-INPSTFM-E-NULL = HIGH-VALUES
              MOVE (SF)-ALQ-CNBT-INPSTFM-E-NULL
              TO DFL-ALQ-CNBT-INPSTFM-E-NULL
           ELSE
              MOVE (SF)-ALQ-CNBT-INPSTFM-E
              TO DFL-ALQ-CNBT-INPSTFM-E
           END-IF
           IF (SF)-ALQ-CNBT-INPSTFM-D-NULL = HIGH-VALUES
              MOVE (SF)-ALQ-CNBT-INPSTFM-D-NULL
              TO DFL-ALQ-CNBT-INPSTFM-D-NULL
           ELSE
              MOVE (SF)-ALQ-CNBT-INPSTFM-D
              TO DFL-ALQ-CNBT-INPSTFM-D
           END-IF
           IF (SF)-DS-RIGA NOT NUMERIC
              MOVE 0 TO DFL-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA
              TO DFL-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO DFL-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO DFL-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO DFL-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ NOT NUMERIC
              MOVE 0 TO DFL-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ
              TO DFL-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ NOT NUMERIC
              MOVE 0 TO DFL-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ
              TO DFL-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO DFL-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO DFL-DS-STATO-ELAB
           IF (SF)-IMPB-VIS-1382011C-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-1382011C-NULL
              TO DFL-IMPB-VIS-1382011C-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-1382011C
              TO DFL-IMPB-VIS-1382011C
           END-IF
           IF (SF)-IMPB-VIS-1382011D-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-1382011D-NULL
              TO DFL-IMPB-VIS-1382011D-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-1382011D
              TO DFL-IMPB-VIS-1382011D
           END-IF
           IF (SF)-IMPB-VIS-1382011L-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-1382011L-NULL
              TO DFL-IMPB-VIS-1382011L-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-1382011L
              TO DFL-IMPB-VIS-1382011L
           END-IF
           IF (SF)-IMPST-VIS-1382011C-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-1382011C-NULL
              TO DFL-IMPST-VIS-1382011C-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-1382011C
              TO DFL-IMPST-VIS-1382011C
           END-IF
           IF (SF)-IMPST-VIS-1382011D-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-1382011D-NULL
              TO DFL-IMPST-VIS-1382011D-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-1382011D
              TO DFL-IMPST-VIS-1382011D
           END-IF
           IF (SF)-IMPST-VIS-1382011L-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-1382011L-NULL
              TO DFL-IMPST-VIS-1382011L-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-1382011L
              TO DFL-IMPST-VIS-1382011L
           END-IF
           IF (SF)-IMPB-IS-1382011C-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-1382011C-NULL
              TO DFL-IMPB-IS-1382011C-NULL
           ELSE
              MOVE (SF)-IMPB-IS-1382011C
              TO DFL-IMPB-IS-1382011C
           END-IF
           IF (SF)-IMPB-IS-1382011D-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-1382011D-NULL
              TO DFL-IMPB-IS-1382011D-NULL
           ELSE
              MOVE (SF)-IMPB-IS-1382011D
              TO DFL-IMPB-IS-1382011D
           END-IF
           IF (SF)-IMPB-IS-1382011L-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-1382011L-NULL
              TO DFL-IMPB-IS-1382011L-NULL
           ELSE
              MOVE (SF)-IMPB-IS-1382011L
              TO DFL-IMPB-IS-1382011L
           END-IF
           IF (SF)-IS-1382011C-NULL = HIGH-VALUES
              MOVE (SF)-IS-1382011C-NULL
              TO DFL-IS-1382011C-NULL
           ELSE
              MOVE (SF)-IS-1382011C
              TO DFL-IS-1382011C
           END-IF
           IF (SF)-IS-1382011D-NULL = HIGH-VALUES
              MOVE (SF)-IS-1382011D-NULL
              TO DFL-IS-1382011D-NULL
           ELSE
              MOVE (SF)-IS-1382011D
              TO DFL-IS-1382011D
           END-IF
           IF (SF)-IS-1382011L-NULL = HIGH-VALUES
              MOVE (SF)-IS-1382011L-NULL
              TO DFL-IS-1382011L-NULL
           ELSE
              MOVE (SF)-IS-1382011L
              TO DFL-IS-1382011L
           END-IF
           IF (SF)-IMP-INTR-RIT-PAG-C-NULL = HIGH-VALUES
              MOVE (SF)-IMP-INTR-RIT-PAG-C-NULL
              TO DFL-IMP-INTR-RIT-PAG-C-NULL
           ELSE
              MOVE (SF)-IMP-INTR-RIT-PAG-C
              TO DFL-IMP-INTR-RIT-PAG-C
           END-IF
           IF (SF)-IMP-INTR-RIT-PAG-D-NULL = HIGH-VALUES
              MOVE (SF)-IMP-INTR-RIT-PAG-D-NULL
              TO DFL-IMP-INTR-RIT-PAG-D-NULL
           ELSE
              MOVE (SF)-IMP-INTR-RIT-PAG-D
              TO DFL-IMP-INTR-RIT-PAG-D
           END-IF
           IF (SF)-IMP-INTR-RIT-PAG-L-NULL = HIGH-VALUES
              MOVE (SF)-IMP-INTR-RIT-PAG-L-NULL
              TO DFL-IMP-INTR-RIT-PAG-L-NULL
           ELSE
              MOVE (SF)-IMP-INTR-RIT-PAG-L
              TO DFL-IMP-INTR-RIT-PAG-L
           END-IF
           IF (SF)-IMPB-BOLLO-DETT-C-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-BOLLO-DETT-C-NULL
              TO DFL-IMPB-BOLLO-DETT-C-NULL
           ELSE
              MOVE (SF)-IMPB-BOLLO-DETT-C
              TO DFL-IMPB-BOLLO-DETT-C
           END-IF
           IF (SF)-IMPB-BOLLO-DETT-D-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-BOLLO-DETT-D-NULL
              TO DFL-IMPB-BOLLO-DETT-D-NULL
           ELSE
              MOVE (SF)-IMPB-BOLLO-DETT-D
              TO DFL-IMPB-BOLLO-DETT-D
           END-IF
           IF (SF)-IMPB-BOLLO-DETT-L-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-BOLLO-DETT-L-NULL
              TO DFL-IMPB-BOLLO-DETT-L-NULL
           ELSE
              MOVE (SF)-IMPB-BOLLO-DETT-L
              TO DFL-IMPB-BOLLO-DETT-L
           END-IF
           IF (SF)-IMPST-BOLLO-DETT-C-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-BOLLO-DETT-C-NULL
              TO DFL-IMPST-BOLLO-DETT-C-NULL
           ELSE
              MOVE (SF)-IMPST-BOLLO-DETT-C
              TO DFL-IMPST-BOLLO-DETT-C
           END-IF
           IF (SF)-IMPST-BOLLO-DETT-D-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-BOLLO-DETT-D-NULL
              TO DFL-IMPST-BOLLO-DETT-D-NULL
           ELSE
              MOVE (SF)-IMPST-BOLLO-DETT-D
              TO DFL-IMPST-BOLLO-DETT-D
           END-IF
           IF (SF)-IMPST-BOLLO-DETT-L-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-BOLLO-DETT-L-NULL
              TO DFL-IMPST-BOLLO-DETT-L-NULL
           ELSE
              MOVE (SF)-IMPST-BOLLO-DETT-L
              TO DFL-IMPST-BOLLO-DETT-L
           END-IF
           IF (SF)-IMPST-BOLLO-TOT-VC-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-BOLLO-TOT-VC-NULL
              TO DFL-IMPST-BOLLO-TOT-VC-NULL
           ELSE
              MOVE (SF)-IMPST-BOLLO-TOT-VC
              TO DFL-IMPST-BOLLO-TOT-VC
           END-IF
           IF (SF)-IMPST-BOLLO-TOT-VD-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-BOLLO-TOT-VD-NULL
              TO DFL-IMPST-BOLLO-TOT-VD-NULL
           ELSE
              MOVE (SF)-IMPST-BOLLO-TOT-VD
              TO DFL-IMPST-BOLLO-TOT-VD
           END-IF
           IF (SF)-IMPST-BOLLO-TOT-VL-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-BOLLO-TOT-VL-NULL
              TO DFL-IMPST-BOLLO-TOT-VL-NULL
           ELSE
              MOVE (SF)-IMPST-BOLLO-TOT-VL
              TO DFL-IMPST-BOLLO-TOT-VL
           END-IF
           IF (SF)-IMPB-VIS-662014C-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-662014C-NULL
              TO DFL-IMPB-VIS-662014C-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-662014C
              TO DFL-IMPB-VIS-662014C
           END-IF
           IF (SF)-IMPB-VIS-662014D-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-662014D-NULL
              TO DFL-IMPB-VIS-662014D-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-662014D
              TO DFL-IMPB-VIS-662014D
           END-IF
           IF (SF)-IMPB-VIS-662014L-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-662014L-NULL
              TO DFL-IMPB-VIS-662014L-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-662014L
              TO DFL-IMPB-VIS-662014L
           END-IF
           IF (SF)-IMPST-VIS-662014C-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-662014C-NULL
              TO DFL-IMPST-VIS-662014C-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-662014C
              TO DFL-IMPST-VIS-662014C
           END-IF
           IF (SF)-IMPST-VIS-662014D-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-662014D-NULL
              TO DFL-IMPST-VIS-662014D-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-662014D
              TO DFL-IMPST-VIS-662014D
           END-IF
           IF (SF)-IMPST-VIS-662014L-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-662014L-NULL
              TO DFL-IMPST-VIS-662014L-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-662014L
              TO DFL-IMPST-VIS-662014L
           END-IF
           IF (SF)-IMPB-IS-662014C-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-662014C-NULL
              TO DFL-IMPB-IS-662014C-NULL
           ELSE
              MOVE (SF)-IMPB-IS-662014C
              TO DFL-IMPB-IS-662014C
           END-IF
           IF (SF)-IMPB-IS-662014D-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-662014D-NULL
              TO DFL-IMPB-IS-662014D-NULL
           ELSE
              MOVE (SF)-IMPB-IS-662014D
              TO DFL-IMPB-IS-662014D
           END-IF
           IF (SF)-IMPB-IS-662014L-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-662014L-NULL
              TO DFL-IMPB-IS-662014L-NULL
           ELSE
              MOVE (SF)-IMPB-IS-662014L
              TO DFL-IMPB-IS-662014L
           END-IF
           IF (SF)-IS-662014C-NULL = HIGH-VALUES
              MOVE (SF)-IS-662014C-NULL
              TO DFL-IS-662014C-NULL
           ELSE
              MOVE (SF)-IS-662014C
              TO DFL-IS-662014C
           END-IF
           IF (SF)-IS-662014D-NULL = HIGH-VALUES
              MOVE (SF)-IS-662014D-NULL
              TO DFL-IS-662014D-NULL
           ELSE
              MOVE (SF)-IS-662014D
              TO DFL-IS-662014D
           END-IF
           IF (SF)-IS-662014L-NULL = HIGH-VALUES
              MOVE (SF)-IS-662014L-NULL
              TO DFL-IS-662014L-NULL
           ELSE
              MOVE (SF)-IS-662014L
              TO DFL-IS-662014L
           END-IF.
       VAL-DCLGEN-DFL-EX.
           EXIT.

      ****************************************************************
      * Area Input
      * Dispatcher
      * LUNGHEZZA :
      ****************************************************************
       02 IDSI0011-AREA.

          10 IDSI0011-PGM                         PIC X(08).

      * --   CAMPI SETTAGGI MODULI I-O
      *      COPY IDSV0007 REPLACING ==IDSI0011== BY ==IDSI0011==.

          10 IDSI0011-MODALITA-ESECUTIVA          PIC X.
             88 IDSI0011-ON-LINE                  VALUE 'O'.
             88 IDSI0011-BATCH                    VALUE 'B'.
             88 IDSI0011-BATCH-INFR               VALUE 'I'.


          10 IDSI0011-CODICE-COMPAGNIA-ANIA       PIC S9(005) COMP-3.
          10 IDSI0011-COD-MAIN-BATCH              PIC X(008).
          10 IDSI0011-TIPO-MOVIMENTO              PIC 9(005).
          10 IDSI0011-SESSIONE                    PIC X(020).
          10 IDSI0011-USER-NAME                   PIC X(020).
          10 IDSI0011-DATA-INIZIO-EFFETTO         PIC S9(008) COMP-3.
          10 IDSI0011-DATA-FINE-EFFETTO           PIC S9(008) COMP-3.
          10 IDSI0011-DATA-COMPETENZA             PIC S9(018) COMP-3.
          10 IDSI0011-DATA-COMP-AGG-STOR          PIC S9(018) COMP-3.

          10 IDSI0011-TRATTAMENTO-STORICITA       PIC X(003).
             88 IDSI0011-TRATT-DEFAULT            VALUE 'DEF'.
             88 IDSI0011-TRATT-SENZA-STOR         VALUE 'NST'.
             88 IDSI0011-TRATT-X-EFFETTO          VALUE 'EFF'.
             88 IDSI0011-TRATT-X-COMPETENZA       VALUE 'CPZ'.
             88 IDSI0011-ANNULLO-MOVIMENTO        VALUE 'ANN'.

          10 IDSI0011-FORMATO-DATA-DB             PIC X(003).
             88 IDSI0011-DB-ISO                   VALUE 'ISO'.
             88 IDSI0011-DB-EUR                   VALUE 'EUR'.

          10 IDSI0011-ID-MOVI-ANNULLATO           PIC 9(009).

          10 IDSI0011-IDENTITA-CHIAMANTE    PIC X(003).
             88 IDSI0011-PTF-NEWLIFE              VALUE 'LPF'.
             88 IDSI0011-REFACTORING              VALUE 'REF'.
             88 IDSI0011-CONVERSIONE              VALUE 'CON'.

          10 IDSI0011-LIVELLO-OPERAZIONE          PIC X(003).
             88 IDSI0011-FIRST-ACTION             VALUE 'FAC'.
             88 IDSI0011-NONE-ACTION              VALUE 'NAC'.
             88 IDSI0011-PRIMARY-KEY              VALUE 'PK '.
             88 IDSI0011-ID                       VALUE 'ID '.
             88 IDSI0011-IB-OGGETTO               VALUE 'IBO'.
             88 IDSI0011-ID-PADRE                 VALUE 'IDP'.
             88 IDSI0011-IB-SECONDARIO            VALUE 'IBS'.
             88 IDSI0011-ID-OGGETTO               VALUE 'IDO'.
             88 IDSI0011-WHERE-CONDITION          VALUE 'WC '.


      * -- CAMPI OPERAZIONE
      *    COPY IDSV0008.
           10 IDSI0011-OPERAZIONE                  PIC X(015).
               88 IDSI0011-SELECT               VALUE 'SELECT'
                                                      'SE'.
               88 IDSI0011-AGGIORNAMENTO-STORICO VALUE 'AGG_STORICO'.
               88 IDSI0011-AGG-STORICO-SOLO-INS
                                                VALUE 'AGG_SOLO_INSERT'.
               88 IDSI0011-INSERT               VALUE 'INSERT'
                                                      'IN'.
               88 IDSI0011-UPDATE               VALUE 'UPDATE'
                                                      'UP'.
               88 IDSI0011-DELETE               VALUE 'DELETE'
                                                      'DF'.
               88 IDSI0011-DELETE-LOGICA        VALUE 'DELETE_LOGICA'
                                                      'DL'.
               88 IDSI0011-OPEN-CURSOR          VALUE 'OPEN_CURSOR'
                                                      'OP'.
               88 IDSI0011-CLOSE-CURSOR         VALUE 'CLOSE_CURSOR'
                                                      'CL'.
               88 IDSI0011-FETCH-FIRST          VALUE 'FF'.
               88 IDSI0011-FETCH-NEXT           VALUE 'FETCH'
                                                      'FN'.
               88 IDSI0011-FETCH-FIRST-MULTIPLE VALUE 'FFM'.
               88 IDSI0011-FETCH-NEXT-MULTIPLE  VALUE 'FNM'.
               88 IDSI0011-PRESA-IN-CARICO  VALUE 'PRESA_IN_CARICO'.
      *    fine COPY IDSV0008.

           10 IDSI0011-FLAG-CODA-TS              PIC X(01) VALUE 'N'.
             88 IDSI0011-DELETE-SI               VALUE 'S'.
             88 IDSI0011-DELETE-NO               VALUE 'N'.
      *    fine COPY IDSV0007

           10 IDSI0011-CODICE-STR-DATO             PIC X(030).
           10 IDSI0011-BUFFER-DATI                 PIC X(30000).
           10 IDSO0011-BUFFER-DATI  REDEFINES
              IDSI0011-BUFFER-DATI                 PIC X(30000).
           10 IDSI0011-BUFFER-WHERE-COND           PIC X(300).

           10 IDSI0011-LIVELLO-DEBUG               PIC 9(001).
              88 IDSI0011-NO-DEBUG                 VALUE 0.
              88 IDSI0011-DEBUG-BASSO              VALUE 1.
              88 IDSI0011-DEBUG-MEDIO              VALUE 2.
              88 IDSI0011-DEBUG-ELEVATO            VALUE 3.
              88 IDSI0011-DEBUG-ESASPERATO         VALUE 4.
              88 IDSI0011-ANY-APPL-DBG             VALUE 1,
                                                         2,
                                                         3,
                                                         4.

              88 IDSI0011-ARCH-BATCH-DBG           VALUE 5.
              88 IDSI0011-COM-COB-JAV-DBG          VALUE 6.
              88 IDSI0011-STRESS-TEST-DBG          VALUE 7.
              88 IDSI0011-BUSINESS-DBG             VALUE 8.
              88 IDSI0011-TOT-TUNING-DBG           VALUE 9.
              88 IDSI0011-ANY-TUNING-DBG           VALUE 5,
                                                         6,
                                                         7,
                                                         8,
                                                         9.

           10 FILLER                               PIC X(100).


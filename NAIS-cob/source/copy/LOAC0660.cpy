      *----------------------------------------------------------------*
      *   PROCESSO DI POST-VENDITA - PORTAFOGLIO VITA
      *   AREA VARIABILI E COSTANTI DI INPUT
      *   SERVIZIO DI RIVALUTAZIONE
      *----------------------------------------------------------------*
           05 (SF)-AREA-VAR-INP.
              10 (SF)-DATI-MANFEE-CONTESTO PIC X(002).
                 88 (SF)-MANFEE-CONT-SI         VALUE 'SI'.
                 88 (SF)-MANFEE-CONT-NO         VALUE 'NO'.
              10 (SF)-PERMANFEE            PIC 9(005).
              10 (SF)-MESIDIFFMFEE         PIC 9(005).
              10 (SF)-DECADELMFEE          PIC 9(005).
              10 (SF)-MF-CALCOLATO         PIC X(002).
                 88 (SF)-MF-CALCOLATO-SI        VALUE 'SI'.
                 88 (SF)-MF-CALCOLATO-NO        VALUE 'NO'.

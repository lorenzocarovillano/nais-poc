      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA RICH_EST
      *   ALIAS P01
      *   ULTIMO AGG. 07 DIC 2017
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-RICH-EST PIC S9(9)     COMP-3.
             07 (SF)-ID-RICH-EST-COLLG PIC S9(9)     COMP-3.
             07 (SF)-ID-RICH-EST-COLLG-NULL REDEFINES
                (SF)-ID-RICH-EST-COLLG   PIC X(5).
             07 (SF)-ID-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-LIQ-NULL REDEFINES
                (SF)-ID-LIQ   PIC X(5).
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-IB-RICH-EST PIC X(40).
             07 (SF)-IB-RICH-EST-NULL REDEFINES
                (SF)-IB-RICH-EST   PIC X(40).
             07 (SF)-TP-MOVI PIC S9(5)     COMP-3.
             07 (SF)-DT-FORM-RICH   PIC S9(8) COMP-3.
             07 (SF)-DT-INVIO-RICH   PIC S9(8) COMP-3.
             07 (SF)-DT-PERV-RICH   PIC S9(8) COMP-3.
             07 (SF)-DT-RGSTRZ-RICH   PIC S9(8) COMP-3.
             07 (SF)-DT-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-EFF-NULL REDEFINES
                (SF)-DT-EFF   PIC X(5).
             07 (SF)-DT-SIN   PIC S9(8) COMP-3.
             07 (SF)-DT-SIN-NULL REDEFINES
                (SF)-DT-SIN   PIC X(5).
             07 (SF)-TP-OGG PIC X(2).
             07 (SF)-ID-OGG PIC S9(9)     COMP-3.
             07 (SF)-IB-OGG PIC X(40).
             07 (SF)-FL-MOD-EXEC PIC X(1).
             07 (SF)-ID-RICHIEDENTE PIC S9(9)     COMP-3.
             07 (SF)-ID-RICHIEDENTE-NULL REDEFINES
                (SF)-ID-RICHIEDENTE   PIC X(5).
             07 (SF)-COD-PROD PIC X(12).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-COD-CAN PIC S9(5)     COMP-3.
             07 (SF)-IB-OGG-ORIG PIC X(40).
             07 (SF)-IB-OGG-ORIG-NULL REDEFINES
                (SF)-IB-OGG-ORIG   PIC X(40).
             07 (SF)-DT-EST-FINANZ   PIC S9(8) COMP-3.
             07 (SF)-DT-EST-FINANZ-NULL REDEFINES
                (SF)-DT-EST-FINANZ   PIC X(5).
             07 (SF)-DT-MAN-COP   PIC S9(8) COMP-3.
             07 (SF)-DT-MAN-COP-NULL REDEFINES
                (SF)-DT-MAN-COP   PIC X(5).
             07 (SF)-FL-GEST-PROTEZIONE PIC X(1).
             07 (SF)-FL-GEST-PROTEZIONE-NULL REDEFINES
                (SF)-FL-GEST-PROTEZIONE   PIC X(1).

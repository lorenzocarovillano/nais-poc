      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA MATR_ELAB_BATCH
      *   ALIAS L71
      *   ULTIMO AGG. 22 APR 2010
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-MATR-ELAB-BATCH PIC S9(9)     COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-TP-FRM-ASSVA PIC X(2).
             07 (SF)-TP-MOVI PIC S9(5)     COMP-3.
             07 (SF)-COD-RAMO PIC X(12).
             07 (SF)-COD-RAMO-NULL REDEFINES
                (SF)-COD-RAMO   PIC X(12).
             07 (SF)-DT-ULT-ELAB   PIC S9(8) COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).

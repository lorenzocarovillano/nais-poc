       01  LDBV4021.
           05 LDBV4021-ID-TGA                 PIC S9(09) COMP-3.
           05 LDBV4021-PRE-LRD                PIC S9(12)V9(3) COMP-3.
           05 LDBV4021-DATA-INIZIO            PIC 9(08).
           05 LDBV4021-DATA-FINE              PIC 9(08).
           05 LDBV4021-DATA-INIZIO-DB         PIC X(10).
           05 LDBV4021-DATA-FINE-DB           PIC X(10).



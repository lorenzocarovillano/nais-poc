
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVDTC3
      *   ULTIMO AGG. 28 NOV 2014
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-DTC.
           MOVE DTC-ID-DETT-TIT-CONT
             TO (SF)-ID-PTF(IX-TAB-DTC)
           MOVE DTC-ID-DETT-TIT-CONT
             TO (SF)-ID-DETT-TIT-CONT(IX-TAB-DTC)
           MOVE DTC-ID-TIT-CONT
             TO (SF)-ID-TIT-CONT(IX-TAB-DTC)
           MOVE DTC-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-DTC)
           MOVE DTC-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-DTC)
           MOVE DTC-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-DTC)
           IF DTC-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE DTC-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-DTC)
           END-IF
           MOVE DTC-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-DTC)
           MOVE DTC-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-DTC)
           MOVE DTC-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-DTC)
           IF DTC-DT-INI-COP-NULL = HIGH-VALUES
              MOVE DTC-DT-INI-COP-NULL
                TO (SF)-DT-INI-COP-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-DT-INI-COP
                TO (SF)-DT-INI-COP(IX-TAB-DTC)
           END-IF
           IF DTC-DT-END-COP-NULL = HIGH-VALUES
              MOVE DTC-DT-END-COP-NULL
                TO (SF)-DT-END-COP-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-DT-END-COP
                TO (SF)-DT-END-COP(IX-TAB-DTC)
           END-IF
           IF DTC-PRE-NET-NULL = HIGH-VALUES
              MOVE DTC-PRE-NET-NULL
                TO (SF)-PRE-NET-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-PRE-NET
                TO (SF)-PRE-NET(IX-TAB-DTC)
           END-IF
           IF DTC-INTR-FRAZ-NULL = HIGH-VALUES
              MOVE DTC-INTR-FRAZ-NULL
                TO (SF)-INTR-FRAZ-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-INTR-FRAZ
                TO (SF)-INTR-FRAZ(IX-TAB-DTC)
           END-IF
           IF DTC-INTR-MORA-NULL = HIGH-VALUES
              MOVE DTC-INTR-MORA-NULL
                TO (SF)-INTR-MORA-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-INTR-MORA
                TO (SF)-INTR-MORA(IX-TAB-DTC)
           END-IF
           IF DTC-INTR-RETDT-NULL = HIGH-VALUES
              MOVE DTC-INTR-RETDT-NULL
                TO (SF)-INTR-RETDT-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-INTR-RETDT
                TO (SF)-INTR-RETDT(IX-TAB-DTC)
           END-IF
           IF DTC-INTR-RIAT-NULL = HIGH-VALUES
              MOVE DTC-INTR-RIAT-NULL
                TO (SF)-INTR-RIAT-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-INTR-RIAT
                TO (SF)-INTR-RIAT(IX-TAB-DTC)
           END-IF
           IF DTC-DIR-NULL = HIGH-VALUES
              MOVE DTC-DIR-NULL
                TO (SF)-DIR-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-DIR
                TO (SF)-DIR(IX-TAB-DTC)
           END-IF
           IF DTC-SPE-MED-NULL = HIGH-VALUES
              MOVE DTC-SPE-MED-NULL
                TO (SF)-SPE-MED-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-SPE-MED
                TO (SF)-SPE-MED(IX-TAB-DTC)
           END-IF
           IF DTC-TAX-NULL = HIGH-VALUES
              MOVE DTC-TAX-NULL
                TO (SF)-TAX-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-TAX
                TO (SF)-TAX(IX-TAB-DTC)
           END-IF
           IF DTC-SOPR-SAN-NULL = HIGH-VALUES
              MOVE DTC-SOPR-SAN-NULL
                TO (SF)-SOPR-SAN-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-SOPR-SAN
                TO (SF)-SOPR-SAN(IX-TAB-DTC)
           END-IF
           IF DTC-SOPR-SPO-NULL = HIGH-VALUES
              MOVE DTC-SOPR-SPO-NULL
                TO (SF)-SOPR-SPO-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-SOPR-SPO
                TO (SF)-SOPR-SPO(IX-TAB-DTC)
           END-IF
           IF DTC-SOPR-TEC-NULL = HIGH-VALUES
              MOVE DTC-SOPR-TEC-NULL
                TO (SF)-SOPR-TEC-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-SOPR-TEC
                TO (SF)-SOPR-TEC(IX-TAB-DTC)
           END-IF
           IF DTC-SOPR-PROF-NULL = HIGH-VALUES
              MOVE DTC-SOPR-PROF-NULL
                TO (SF)-SOPR-PROF-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-SOPR-PROF
                TO (SF)-SOPR-PROF(IX-TAB-DTC)
           END-IF
           IF DTC-SOPR-ALT-NULL = HIGH-VALUES
              MOVE DTC-SOPR-ALT-NULL
                TO (SF)-SOPR-ALT-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-SOPR-ALT
                TO (SF)-SOPR-ALT(IX-TAB-DTC)
           END-IF
           IF DTC-PRE-TOT-NULL = HIGH-VALUES
              MOVE DTC-PRE-TOT-NULL
                TO (SF)-PRE-TOT-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-PRE-TOT
                TO (SF)-PRE-TOT(IX-TAB-DTC)
           END-IF
           IF DTC-PRE-PP-IAS-NULL = HIGH-VALUES
              MOVE DTC-PRE-PP-IAS-NULL
                TO (SF)-PRE-PP-IAS-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-PRE-PP-IAS
                TO (SF)-PRE-PP-IAS(IX-TAB-DTC)
           END-IF
           IF DTC-PRE-SOLO-RSH-NULL = HIGH-VALUES
              MOVE DTC-PRE-SOLO-RSH-NULL
                TO (SF)-PRE-SOLO-RSH-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-PRE-SOLO-RSH
                TO (SF)-PRE-SOLO-RSH(IX-TAB-DTC)
           END-IF
           IF DTC-CAR-ACQ-NULL = HIGH-VALUES
              MOVE DTC-CAR-ACQ-NULL
                TO (SF)-CAR-ACQ-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-CAR-ACQ
                TO (SF)-CAR-ACQ(IX-TAB-DTC)
           END-IF
           IF DTC-CAR-GEST-NULL = HIGH-VALUES
              MOVE DTC-CAR-GEST-NULL
                TO (SF)-CAR-GEST-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-CAR-GEST
                TO (SF)-CAR-GEST(IX-TAB-DTC)
           END-IF
           IF DTC-CAR-INC-NULL = HIGH-VALUES
              MOVE DTC-CAR-INC-NULL
                TO (SF)-CAR-INC-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-CAR-INC
                TO (SF)-CAR-INC(IX-TAB-DTC)
           END-IF
           IF DTC-PROV-ACQ-1AA-NULL = HIGH-VALUES
              MOVE DTC-PROV-ACQ-1AA-NULL
                TO (SF)-PROV-ACQ-1AA-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-PROV-ACQ-1AA
                TO (SF)-PROV-ACQ-1AA(IX-TAB-DTC)
           END-IF
           IF DTC-PROV-ACQ-2AA-NULL = HIGH-VALUES
              MOVE DTC-PROV-ACQ-2AA-NULL
                TO (SF)-PROV-ACQ-2AA-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-PROV-ACQ-2AA
                TO (SF)-PROV-ACQ-2AA(IX-TAB-DTC)
           END-IF
           IF DTC-PROV-RICOR-NULL = HIGH-VALUES
              MOVE DTC-PROV-RICOR-NULL
                TO (SF)-PROV-RICOR-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-PROV-RICOR
                TO (SF)-PROV-RICOR(IX-TAB-DTC)
           END-IF
           IF DTC-PROV-INC-NULL = HIGH-VALUES
              MOVE DTC-PROV-INC-NULL
                TO (SF)-PROV-INC-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-PROV-INC
                TO (SF)-PROV-INC(IX-TAB-DTC)
           END-IF
           IF DTC-PROV-DA-REC-NULL = HIGH-VALUES
              MOVE DTC-PROV-DA-REC-NULL
                TO (SF)-PROV-DA-REC-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-PROV-DA-REC
                TO (SF)-PROV-DA-REC(IX-TAB-DTC)
           END-IF
           IF DTC-COD-DVS-NULL = HIGH-VALUES
              MOVE DTC-COD-DVS-NULL
                TO (SF)-COD-DVS-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-COD-DVS
                TO (SF)-COD-DVS(IX-TAB-DTC)
           END-IF
           IF DTC-FRQ-MOVI-NULL = HIGH-VALUES
              MOVE DTC-FRQ-MOVI-NULL
                TO (SF)-FRQ-MOVI-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-FRQ-MOVI
                TO (SF)-FRQ-MOVI(IX-TAB-DTC)
           END-IF
           MOVE DTC-TP-RGM-FISC
             TO (SF)-TP-RGM-FISC(IX-TAB-DTC)
           IF DTC-COD-TARI-NULL = HIGH-VALUES
              MOVE DTC-COD-TARI-NULL
                TO (SF)-COD-TARI-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-COD-TARI
                TO (SF)-COD-TARI(IX-TAB-DTC)
           END-IF
           MOVE DTC-TP-STAT-TIT
             TO (SF)-TP-STAT-TIT(IX-TAB-DTC)
           IF DTC-IMP-AZ-NULL = HIGH-VALUES
              MOVE DTC-IMP-AZ-NULL
                TO (SF)-IMP-AZ-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-IMP-AZ
                TO (SF)-IMP-AZ(IX-TAB-DTC)
           END-IF
           IF DTC-IMP-ADER-NULL = HIGH-VALUES
              MOVE DTC-IMP-ADER-NULL
                TO (SF)-IMP-ADER-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-IMP-ADER
                TO (SF)-IMP-ADER(IX-TAB-DTC)
           END-IF
           IF DTC-IMP-TFR-NULL = HIGH-VALUES
              MOVE DTC-IMP-TFR-NULL
                TO (SF)-IMP-TFR-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-IMP-TFR
                TO (SF)-IMP-TFR(IX-TAB-DTC)
           END-IF
           IF DTC-IMP-VOLO-NULL = HIGH-VALUES
              MOVE DTC-IMP-VOLO-NULL
                TO (SF)-IMP-VOLO-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-IMP-VOLO
                TO (SF)-IMP-VOLO(IX-TAB-DTC)
           END-IF
           IF DTC-MANFEE-ANTIC-NULL = HIGH-VALUES
              MOVE DTC-MANFEE-ANTIC-NULL
                TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-MANFEE-ANTIC
                TO (SF)-MANFEE-ANTIC(IX-TAB-DTC)
           END-IF
           IF DTC-MANFEE-RICOR-NULL = HIGH-VALUES
              MOVE DTC-MANFEE-RICOR-NULL
                TO (SF)-MANFEE-RICOR-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-MANFEE-RICOR
                TO (SF)-MANFEE-RICOR(IX-TAB-DTC)
           END-IF
           IF DTC-MANFEE-REC-NULL = HIGH-VALUES
              MOVE DTC-MANFEE-REC-NULL
                TO (SF)-MANFEE-REC-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-MANFEE-REC
                TO (SF)-MANFEE-REC(IX-TAB-DTC)
           END-IF
           IF DTC-DT-ESI-TIT-NULL = HIGH-VALUES
              MOVE DTC-DT-ESI-TIT-NULL
                TO (SF)-DT-ESI-TIT-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-DT-ESI-TIT
                TO (SF)-DT-ESI-TIT(IX-TAB-DTC)
           END-IF
           IF DTC-SPE-AGE-NULL = HIGH-VALUES
              MOVE DTC-SPE-AGE-NULL
                TO (SF)-SPE-AGE-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-SPE-AGE
                TO (SF)-SPE-AGE(IX-TAB-DTC)
           END-IF
           IF DTC-CAR-IAS-NULL = HIGH-VALUES
              MOVE DTC-CAR-IAS-NULL
                TO (SF)-CAR-IAS-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-CAR-IAS
                TO (SF)-CAR-IAS(IX-TAB-DTC)
           END-IF
           IF DTC-TOT-INTR-PREST-NULL = HIGH-VALUES
              MOVE DTC-TOT-INTR-PREST-NULL
                TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-TOT-INTR-PREST
                TO (SF)-TOT-INTR-PREST(IX-TAB-DTC)
           END-IF
           MOVE DTC-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-DTC)
           MOVE DTC-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-DTC)
           MOVE DTC-DS-VER
             TO (SF)-DS-VER(IX-TAB-DTC)
           MOVE DTC-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-DTC)
           MOVE DTC-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-DTC)
           MOVE DTC-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-DTC)
           MOVE DTC-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-DTC)
           IF DTC-IMP-TRASFE-NULL = HIGH-VALUES
              MOVE DTC-IMP-TRASFE-NULL
                TO (SF)-IMP-TRASFE-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-IMP-TRASFE
                TO (SF)-IMP-TRASFE(IX-TAB-DTC)
           END-IF
           IF DTC-IMP-TFR-STRC-NULL = HIGH-VALUES
              MOVE DTC-IMP-TFR-STRC-NULL
                TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-IMP-TFR-STRC
                TO (SF)-IMP-TFR-STRC(IX-TAB-DTC)
           END-IF
           IF DTC-NUM-GG-RITARDO-PAG-NULL = HIGH-VALUES
              MOVE DTC-NUM-GG-RITARDO-PAG-NULL
                TO (SF)-NUM-GG-RITARDO-PAG-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-NUM-GG-RITARDO-PAG
                TO (SF)-NUM-GG-RITARDO-PAG(IX-TAB-DTC)
           END-IF
           IF DTC-NUM-GG-RIVAL-NULL = HIGH-VALUES
              MOVE DTC-NUM-GG-RIVAL-NULL
                TO (SF)-NUM-GG-RIVAL-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-NUM-GG-RIVAL
                TO (SF)-NUM-GG-RIVAL(IX-TAB-DTC)
           END-IF
           IF DTC-ACQ-EXP-NULL = HIGH-VALUES
              MOVE DTC-ACQ-EXP-NULL
                TO (SF)-ACQ-EXP-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-ACQ-EXP
                TO (SF)-ACQ-EXP(IX-TAB-DTC)
           END-IF
           IF DTC-REMUN-ASS-NULL = HIGH-VALUES
              MOVE DTC-REMUN-ASS-NULL
                TO (SF)-REMUN-ASS-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-REMUN-ASS
                TO (SF)-REMUN-ASS(IX-TAB-DTC)
           END-IF
           IF DTC-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE DTC-COMMIS-INTER-NULL
                TO (SF)-COMMIS-INTER-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-COMMIS-INTER
                TO (SF)-COMMIS-INTER(IX-TAB-DTC)
           END-IF
           IF DTC-CNBT-ANTIRAC-NULL = HIGH-VALUES
              MOVE DTC-CNBT-ANTIRAC-NULL
                TO (SF)-CNBT-ANTIRAC-NULL(IX-TAB-DTC)
           ELSE
              MOVE DTC-CNBT-ANTIRAC
                TO (SF)-CNBT-ANTIRAC(IX-TAB-DTC)
           END-IF.
       VALORIZZA-OUTPUT-DTC-EX.
           EXIT.

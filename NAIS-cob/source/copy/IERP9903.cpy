      *----------------------------------------------------------------*
      *  COPY DI PROCEDURE CONTENETE LA GESTIONE DEGLI ERRORI DERIVANTI
      *  DAL SERVIZIO DI PRODOTTO
      *  SMISTA QUELLI CHE SONO ERRORI BLOCCANTI E LE EVENTUALI DEROGHE
      *----------------------------------------------------------------*
       S0320-OUTPUT-PRODOTTO.

      *--> STEP ELABORAZIONE
           IF ISPC0001-STEP-ELAB EQUAL SPACES OR HIGH-VALUE
              MOVE '*'            TO ISPC0001-STEP-ELAB
           END-IF

           IF WCOM-NUM-ELE-MOT-DEROGA =  SPACES OR HIGH-VALUE
                                                OR LOW-VALUE

              MOVE ZEROES         TO WCOM-NUM-ELE-MOT-DEROGA
           END-IF


      *    PULIZIA AUTOMATICA DELLE DEROGHE NON PIU' VALIDE
      *    PER LO STEP ELABORATIVO CORRENTE
      *    LO STEP ELABORATIVO CI VIENE RESTITUITO DAL VALORIZZATORE
      *    VARIABILI E CANCELLIAMO TUTTE LE DEROGHE RELATIVE ALLO STEP
      *    ELABORATIVO CORRENTE, PRIMA DI MEMORIZZARE QUELLE APPENA
      *    RESTITUITE DAL SERVIZIO DI PRODOTTO
           MOVE ZEROES            TO IX-ERR-GENERIC
           PERFORM VARYING IX-ERR-DEROG FROM 1 BY 1
                     UNTIL IX-ERR-DEROG > WCOM-NUM-ELE-MOT-DEROGA
                IF ISPC0001-STEP-ELAB NOT EQUAL
                   WCOM-MOT-DER-STEP-ELAB(IX-ERR-DEROG)
                      ADD 1         TO IX-ERR-GENERIC
                      MOVE WCOM-TAB-MOT-DEROGA(IX-ERR-DEROG)
                        TO WCOM-TAB-MOT-DEROGA(IX-ERR-GENERIC)
                ELSE
      *             CANCELLA LA DEROGA
                    MOVE SPACES
                      TO WCOM-TAB-MOT-DEROGA(IX-ERR-DEROG)
                END-IF
           END-PERFORM
           MOVE IX-ERR-GENERIC TO WCOM-NUM-ELE-MOT-DEROGA

           EVALUATE ISPC0001-ESITO

      *        RETURN CODE = 00
               WHEN 00
      *          ERRORE DI SISTEMA DURANTE LA CHIAMATA AL SERVIZIO
      *          DI ACTUATOR: $ (COD-ERR= $ - GRAVITA = $)
39135            PERFORM VARYING IX-ERR-GENERIC FROM 1 BY 1
39135              UNTIL IX-ERR-GENERIC > ISPC0001-ERR-NUM-ELE
39135                MOVE '005217'                TO IEAI9901-COD-ERRORE
39135                PERFORM S0322-SALVA-ERRORE
39135                   THRU EX-S0322
39135            END-PERFORM

      *        RETURN CODE = 01
               WHEN 01
                 PERFORM S0321-ERRORI-DEROGA
                    THRU EX-S0321
                 VARYING IX-ERR-GENERIC FROM 1 BY 1
                   UNTIL IX-ERR-GENERIC > ISPC0001-ERR-NUM-ELE

           END-EVALUATE.

       EX-S0320-OUTPUT-PRODOTTO.
           EXIT.

      *----------------------------------------------------------------*
      *   SALVA UN ERRORE NELL'AREA COMUNE
      *----------------------------------------------------------------*
       S0322-SALVA-ERRORE.

           MOVE 'IERP9903'                   TO IEAI9901-COD-SERVIZIO-BE
           MOVE 'S0320-OUTPUT-PRODOTTO'      TO IEAI9901-LABEL-ERR
           STRING ISPC0001-DESCRIZIONE-ERR(IX-ERR-GENERIC)  ';'
                  ISPC0001-COD-ERR(IX-ERR-GENERIC)          ';'
                  ISPC0001-GRAVITA-ERR(IX-ERR-GENERIC)
           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
           END-STRING
           PERFORM S0300-RICERCA-GRAVITA-ERRORE
              THRU EX-S0300.

       EX-S0322.
           EXIT.

      *----------------------------------------------------------------*
      *   GESTIONE ERRORI-DEROGHE
      *----------------------------------------------------------------*
       S0321-ERRORI-DEROGA.

           EVALUATE TRUE

      *        Errore Applicativo Warning
      *        (proveniente dall�applicazione Actuator)
               WHEN ISPC0001-GR-WARNING-ACT(IX-ERR-GENERIC)
      *        Errore Applicativo Warning
      *        (proveniente da controllo caricato dall�utente)
               WHEN ISPC0001-GR-WARNING-UTE(IX-ERR-GENERIC)

      *             ERRORE APPLICATIVO DEL SERVIZIO DI ACTUATOR: $
      *             (COD-ERR= $ - GRAVITA = $)
                    MOVE '005218'                 TO IEAI9901-COD-ERRORE
                    PERFORM S0322-SALVA-ERRORE
                       THRU EX-S0322

      *        Errore Applicativo Bloccante
      *        (proveniente da controllo caricato dall�utente)
               WHEN ISPC0001-GR-BLOCCANTE(IX-ERR-GENERIC)

      *             ERRORE APPLICATIVO DEL SERVIZIO DI ACTUATOR: $
      *             (COD-ERR= $ - GRAVITA = $)
                    MOVE '005220'                 TO IEAI9901-COD-ERRORE
                    PERFORM S0322-SALVA-ERRORE
                       THRU EX-S0322

      *        Errore Applicativo NON Bloccante
      *        (proveniente da controllo caricato dall�utente)
               WHEN ISPC0001-GR-NON-BLOCC(IX-ERR-GENERIC)

      *             ERRORE APPLICATIVO DEL SERVIZIO DI ACTUATOR: $
      *             (COD-ERR= $ - GRAVITA = $)
                    MOVE '005219'                 TO IEAI9901-COD-ERRORE
                    PERFORM S0322-SALVA-ERRORE
                       THRU EX-S0322

      *        Errore Applicativo Bloccante corrispondente
      *        ad un controllo di actuator con esito
      *        "Operazione non consentita"
               WHEN ISPC0001-GR-OPER-NON-CONS(IX-ERR-GENERIC)

                    IF ISPC0001-LIVELLO-DEROGA(IX-ERR-GENERIC) = 0
      *                OPERAZIONE NON CONSENTITA DA ACTUATOR: $
      *                (COD-ERR= $ - GRAVITA = $)
                       MOVE '005221'              TO IEAI9901-COD-ERRORE
                       PERFORM S0322-SALVA-ERRORE
                          THRU EX-S0322
                    END-IF

      *        Si tratta di una anomalia derogabile da utenti
      *        con livello autorizzativo minore di Livello Deroga
               WHEN OTHER

                    PERFORM S0323-SALVA-DEROGA
                       THRU EX-S0323

           END-EVALUATE.

       EX-S0321.
           EXIT.

      *----------------------------------------------------------------*
      *   SALVA UNA DEROGA NELL'AREA COMUNE
      *----------------------------------------------------------------*
       S0323-SALVA-DEROGA.

      *    SE LA DEROGA NON E' GIA' PRESENTE LA INSERISCE
           SET CERCA-DEROGA-ERR                 TO TRUE
           PERFORM VARYING IX-ERR-DEROG FROM 1 BY 1
                     UNTIL IX-ERR-DEROG > WCOM-NUM-ELE-MOT-DEROGA
              IF  WCOM-COD-ERR(IX-ERR-DEROG)   EQUAL
                  ISPC0001-COD-ERR(IX-ERR-GENERIC)
                     SET DEROGA-PRESENTE-ERR       TO TRUE
              END-IF
           END-PERFORM

           IF NOT DEROGA-PRESENTE-ERR
              ADD 1  TO WCOM-NUM-ELE-MOT-DEROGA
              SET WCOM-ST-ADD(WCOM-NUM-ELE-MOT-DEROGA)
               TO TRUE
              MOVE ZERO
                TO WCOM-ID-MOT-DEROGA(WCOM-NUM-ELE-MOT-DEROGA)
              MOVE ISPC0001-COD-ERR(IX-ERR-GENERIC)
                TO WCOM-COD-ERR(WCOM-NUM-ELE-MOT-DEROGA)
              MOVE ISPC0001-DESCRIZIONE-ERR(IX-ERR-GENERIC)
                TO WCOM-DESCRIZIONE-ERR(WCOM-NUM-ELE-MOT-DEROGA)
      *       BS = DEROGA DI BUSINESS (CIOE' RESTITUITA DA ACTUATOR)
              MOVE 'BS'
                TO WCOM-TP-ERR(WCOM-NUM-ELE-MOT-DEROGA)
              MOVE ISPC0001-GRAVITA-ERR(IX-ERR-GENERIC)
                TO WCOM-TP-MOT-DEROGA(WCOM-NUM-ELE-MOT-DEROGA)
              MOVE ISPC0001-LIVELLO-DEROGA(IX-ERR-GENERIC)
               TO WCOM-COD-LIV-AUTORIZZATIVO(WCOM-NUM-ELE-MOT-DEROGA)
              MOVE ISPC0001-STEP-ELAB
                TO WCOM-MOT-DER-STEP-ELAB(WCOM-NUM-ELE-MOT-DEROGA)
           END-IF.

       EX-S0323.
           EXIT.

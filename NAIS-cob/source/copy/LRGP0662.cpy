      *----------------------------------------------------------------*
      *    ESTRATTO CONTO - FASE DIAGNOSTICO
      *       - CONTROLLI AD HOC MULTIRAMO & RIVALUTABILI
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *   CONTROLLO 4 5 39                                             *
      *----------------------------------------------------------------*
       C0020-CNTRL-4-5-39.

           MOVE 'C0020-CNTRL-4-5-39'          TO WK-LABEL.

           IF WRIN-FLAG-PROD-CON-CEDOLE = 'SI'
              PERFORM C0021-CNTRL-4
                 THRU EX-C0021
           END-IF

           IF CONTROLLO-OK
      *--> CONTROLLO 5
           IF WRIN-IMP-LOR-CED-LIQU > 0
              IF  (WRIN-CC-PAESE EQUAL HIGH-VALUES
                   OR LOW-VALUES OR SPACES)
              OR  (WRIN-CC-CHECK-DIGIT EQUAL HIGH-VALUES
                   OR LOW-VALUES OR SPACES)
              OR (WRIN-CC-CIN EQUAL HIGH-VALUES
                  OR LOW-VALUES OR SPACES)
              OR (WRIN-CC-ABI EQUAL HIGH-VALUES
                  OR LOW-VALUES OR SPACES)
              OR (WRIN-CC-CAB EQUAL HIGH-VALUES
                  OR LOW-VALUES OR SPACES)
              OR (WRIN-CC-NUM-CONTO EQUAL HIGH-VALUES
                  OR LOW-VALUES OR SPACES)
                  MOVE 5 TO WK-CONTROLLO
                  SET CNTRL-5     TO TRUE
                  MOVE MSG-ERRORE TO WS-ERRORE
                  PERFORM E001-SCARTO THRU EX-E001
              END-IF
           END-IF
           END-IF

           IF CONTROLLO-OK
           MOVE 0 TO WS-SOMMA

           PERFORM VARYING IX-REC-10 FROM 1 BY 1
             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
               IF REC10-DESC-TP-MOVI(IX-REC-10) =
                  TP-PAG-CEDOLA
                  COMPUTE WS-SOMMA = WS-SOMMA
                        + REC10-IMP-OPER(IX-REC-10)
               END-IF
           END-PERFORM

           COMPUTE WS-DIFF = WRIN-IMP-LOR-CED-LIQU - WS-SOMMA

           IF  WS-DIFF < 0,05 AND WS-DIFF > -0,05
              CONTINUE
           ELSE
              MOVE WS-SOMMA              TO WS-CAMPO-1
              MOVE WRIN-IMP-LOR-CED-LIQU TO WS-CAMPO-2
              MOVE 39 TO WK-CONTROLLO
              SET CNTRL-39     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF
           END-IF.

       EX-C0020.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 4                                                  *
      *----------------------------------------------------------------*
       C0021-CNTRL-4.

           MOVE 'C0021-CNTRL-4'          TO WK-LABEL.

           INITIALIZE ESTR-CNT-DIAGN-CED
                      IDSI0011-BUFFER-DATI.

           MOVE ZEROES        TO IDSI0011-DATA-INIZIO-EFFETTO
                                 IDSI0011-DATA-FINE-EFFETTO
                                 IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-WHERE-CONDITION   TO TRUE.
           SET IDSI0011-SELECT            TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR  TO TRUE.
      *
           MOVE WRIN-ID-POLI     TO LDBVF301-ID-POLI.
test  *    MOVE 20150101         TO LDBVF301-DT-LIQ-CED-DA
test  *    MOVE 20150131         TO LDBVF301-DT-LIQ-CED-A
      *
           MOVE WK-DT-INF        TO LDBVF301-DT-LIQ-CED-DA
      *
           MOVE WK-DT-SUP        TO LDBVF301-DT-LIQ-CED-A
      *
           MOVE 'LDBSF300'       TO IDSI0011-CODICE-STR-DATO
      *
           MOVE LDBVF301         TO IDSI0011-BUFFER-DATI.
           MOVE LDBVF301         TO IDSI0011-BUFFER-WHERE-COND.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI
                         TO ESTR-CNT-DIAGN-CED
                        IF WRIN-IMP-LOR-CED-LIQU =
                           P84-IMP-LRD-CEDOLE-LIQ
                           CONTINUE
                        ELSE
                           MOVE WRIN-IMP-LOR-CED-LIQU
                             TO WS-CAMPO-1
                           MOVE P84-IMP-LRD-CEDOLE-LIQ
                             TO WS-CAMPO-2
                           MOVE 4 TO WK-CONTROLLO
                           SET CNTRL-4     TO TRUE
                           MOVE MSG-ERRORE  TO WS-ERRORE
                           PERFORM E001-SCARTO THRU EX-E001
                        END-IF
      *
                  WHEN IDSO0011-NOT-FOUND
                       IF WRIN-IMP-LOR-CED-LIQU EQUAL ZERO
                          CONTINUE
                       ELSE
                          MOVE 203 TO WK-CONTROLLO
                          STRING '203; POLIZZA NOT '
                                 'FOUND IN TAB CEDOLE'
                          DELIMITED BY SIZE
                          INTO WS-ERRORE
                          PERFORM E001-SCARTO THRU EX-E001
                       END-IF
      *
                  WHEN OTHER
                         MOVE WK-PGM         TO IEAI9901-COD-SERVIZIO-BE
                         MOVE WK-LABEL       TO IEAI9901-LABEL-ERR
                         MOVE '005016'       TO IEAI9901-COD-ERRORE
                         STRING 'LDBSF310'       ';'
                                IDSO0011-RETURN-CODE ';'
                                IDSO0011-SQLCODE
                                DELIMITED BY SIZE
                                INTO IEAI9901-PARAMETRI-ERR
                         END-STRING

                         PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300

              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL              TO IEAI9901-LABEL-ERR
              MOVE '005016'              TO IEAI9901-COD-ERRORE
              STRING 'LDBSF310'        ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY   SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
           END-IF.

       EX-C0021.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO RIVALUTAZ   - 26 27 28 29 30 31 32                 *
      *----------------------------------------------------------------*
       C0022-CNTRL-RIVALUTAZ.
      *
           MOVE 'C0022-CNTRL-RIVALUTAZ'          TO WK-LABEL.
      *

           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
ALFR  *        IF REC3-TP-GAR(IX-REC-3) = 1
ALFR           IF REC3-TP-GAR(IX-REC-3) = ( 1 OR 5 )
                  AND REC3-TP-INVST(IX-REC-3) = 4

                  PERFORM C023-CONTROLLI
                     THRU EX-C023

               END-IF

           END-PERFORM.

       EX-C0022.
           EXIT.

      *----------------------------------------------------------------*
      *   CONTROLLO RIVALUTAZ   - 26 27 28 29 30 31 32                 *
      *----------------------------------------------------------------*
       C023-CONTROLLI.

           MOVE 'C023-CONTROLLI'          TO WK-LABEL.

           INITIALIZE ESTR-CNT-DIAGN-RIV
                      IDSI0011-BUFFER-DATI.

           MOVE ZEROES        TO IDSI0011-DATA-INIZIO-EFFETTO
                                 IDSI0011-DATA-FINE-EFFETTO
                                 IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-WHERE-CONDITION   TO TRUE.
           SET IDSI0011-SELECT            TO TRUE.
           SET IDSI0011-TRATT-SENZA-STOR  TO TRUE.
      *
           MOVE WRIN-ID-POLI     TO LDBVF321-ID-POLI.
test  *    MOVE 20150101         TO LDBVF321-DT-RIV-DA
test  *    MOVE 20150131         TO LDBVF321-DT-RIV-A
      *
           MOVE WK-DT-INF        TO LDBVF321-DT-RIV-DA
           MOVE WK-DT-SUP        TO LDBVF321-DT-RIV-A
           MOVE REC3-COD-GAR(IX-REC-3)
                                 TO LDBVF321-COD-TARI
      *
           MOVE 'LDBSF320'       TO IDSI0011-CODICE-STR-DATO
      *
           MOVE LDBVF321         TO IDSI0011-BUFFER-DATI.
           MOVE LDBVF321         TO IDSI0011-BUFFER-WHERE-COND.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI
                         TO ESTR-CNT-DIAGN-RIV

                         MOVE 1 TO IX-TAB-P85

                         PERFORM VALORIZZA-OUTPUT-P85
                            THRU VALORIZZA-OUTPUT-P85-EX
      *
                         PERFORM CONTROLLI-FLUSSO-RIVA
                            THRU CONTROLLI-FLUSSO-RIVA-EX
      *
                  WHEN IDSO0011-NOT-FOUND
                          MOVE 203 TO WK-CONTROLLO
                          STRING '203; POLIZZA NOT '
                                 'FOUND IN TAB RIVAL'
                          DELIMITED BY SIZE
                          INTO WS-ERRORE
                          PERFORM E001-SCARTO THRU EX-E001
      *
                  WHEN OTHER
                         MOVE WK-PGM         TO IEAI9901-COD-SERVIZIO-BE
                         MOVE WK-LABEL       TO IEAI9901-LABEL-ERR
                         MOVE '005016'       TO IEAI9901-COD-ERRORE
                         STRING 'LDBSF320'       ';'
                                IDSO0011-RETURN-CODE ';'
                                IDSO0011-SQLCODE
                                DELIMITED BY SIZE
                                INTO IEAI9901-PARAMETRI-ERR
                         END-STRING

                         PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300

              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL              TO IEAI9901-LABEL-ERR
              MOVE '005016'              TO IEAI9901-COD-ERRORE
              STRING 'LDBSF320'        ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY   SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
           END-IF.

       EX-C023.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 25                                                 *
      *----------------------------------------------------------------*
       C0255-CNTRL-25.
      *
           MOVE 'C0255-CNTRL-25'                 TO WK-LABEL.
      *
           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
ALFR  *       IF REC3-TP-GAR(IX-REC-3) = 1
ALFR          IF REC3-TP-GAR(IX-REC-3) =  ( 1 OR 5 )
              AND REC3-TP-INVST(IX-REC-3) = 4

              SET LOOP-CNTRL-25-NO TO TRUE

              PERFORM VARYING IX-IND FROM 1 BY 1
                UNTIL IDSV0001-ESITO-KO
                   OR IX-IND > ISPC0590-NUM-MAX-PROD
                   OR LOOP-CNTRL-25-SI

                  IF REC3-COD-GAR(IX-REC-3) =
                     ISPC0590-COD-TARI(IX-IND)
                     IF (ISPC0590-TASSO-TECNICO(IX-IND) * 100) =
                        REC3-TASSO-TECNICO(IX-REC-3)
                        SET LOOP-CNTRL-25-SI TO TRUE
                     ELSE
                        MOVE REC3-TASSO-TECNICO(IX-REC-3)
                          TO WS-CAMPO-1
                        MOVE ISPC0590-TASSO-TECNICO(IX-IND)
                          TO WS-CAMPO-2
                        MOVE 25 TO WK-CONTROLLO
                        SET CNTRL-25     TO TRUE
                        MOVE MSG-ERRORE TO WS-ERRORE
                        PERFORM E001-SCARTO THRU EX-E001
                     END-IF
                  END-IF
              END-PERFORM

              END-IF
           END-PERFORM.
      *
       EX-C0255.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLI SUL FLUSSO RIVALUTAZIONE                           *
      *----------------------------------------------------------------*
       CONTROLLI-FLUSSO-RIVA.

           MOVE 'CONTROLLI-FLUSSO-RIVA'          TO WK-LABEL.

           INITIALIZE AREA-RIVAL

      *--> CONTROLLO 26
           IF WP85-RENDTO-LRD-NULL(IX-TAB-P85) = HIGH-VALUES
              MOVE 0 TO WP85-RENDTO-LRD(IX-TAB-P85)
           END-IF

           COMPUTE WREC-RENDTO-LRD ROUNDED =
                   REC3-REND-LOR-GEST-SEP-GAR(IX-REC-3)

      *    IF REC3-REND-LOR-GEST-SEP-GAR(IX-REC-3) =
           IF WREC-RENDTO-LRD =
              WP85-RENDTO-LRD(IX-TAB-P85)
              CONTINUE
           ELSE
              MOVE 26 TO WK-CONTROLLO
              MOVE REC3-REND-LOR-GEST-SEP-GAR(IX-REC-3)
                TO WS-CAMPO-1
              MOVE WP85-RENDTO-LRD(IX-TAB-P85)
                TO WS-CAMPO-2
              SET CNTRL-26     TO TRUE
              MOVE MSG-ERRORE  TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.
      *
      *--> CONTROLLO 27
           IF WP85-PC-RETR-NULL(IX-TAB-P85) = HIGH-VALUES
              MOVE 0 TO WP85-PC-RETR(IX-TAB-P85)
           END-IF
           COMPUTE WREC-PC-RETR    ROUNDED =
                   REC3-ALIQ-RETROC-RICON-GAR(IX-REC-3)
      *    IF REC3-ALIQ-RETROC-RICON-GAR(IX-REC-3) =
           IF WREC-PC-RETR =
              WP85-PC-RETR(IX-TAB-P85)
              CONTINUE
           ELSE
              MOVE 27 TO WK-CONTROLLO
              MOVE REC3-ALIQ-RETROC-RICON-GAR(IX-REC-3)
                TO WS-CAMPO-1
              MOVE WP85-PC-RETR(IX-TAB-P85)
                TO WS-CAMPO-2
              SET CNTRL-27     TO TRUE
              MOVE MSG-ERRORE  TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.
      *
      *--> CONTROLLO 28 CONFRONTO CON UN CAMPO 9(3)V9(2)
           IF WP85-RENDTO-RETR-NULL(IX-TAB-P85) = HIGH-VALUES
              MOVE 0 TO WP85-RENDTO-RETR(IX-TAB-P85)
           END-IF

           COMPUTE WREC-RENDTO-RETR ROUNDED =
                   REC3-TASSO-ANN-REND-RETROC-GAR(IX-REC-3)

           MOVE REC3-TASSO-ANN-REND-RETROC-GAR(IX-REC-3)
             TO WS-CAMPO-APPOGGIO

      *    IF WS-CAMPO-APPOGGIO =
           IF WREC-RENDTO-RETR  =
              WP85-RENDTO-RETR(IX-TAB-P85)
              CONTINUE
           ELSE
              MOVE 28 TO WK-CONTROLLO
              MOVE REC3-TASSO-ANN-REND-RETROC-GAR(IX-REC-3)
                TO WS-CAMPO-1
              MOVE WP85-RENDTO-RETR(IX-TAB-P85)
                TO WS-CAMPO-2
              SET CNTRL-28     TO TRUE
              MOVE MSG-ERRORE  TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.
      *
      *--> CONTROLLO 29
           IF WP85-COMMIS-GEST-NULL(IX-TAB-P85) = HIGH-VALUES
              MOVE 0 TO WP85-COMMIS-GEST(IX-TAB-P85)
           END-IF
           COMPUTE WREC-COMM-GEST  ROUNDED =
                   REC3-PC-COMM-GEST-GAR(IX-REC-3)
      *    IF REC3-PC-COMM-GEST-GAR(IX-REC-3) =
           IF WREC-COMM-GEST =
              WP85-COMMIS-GEST(IX-TAB-P85)
              CONTINUE
           ELSE
              MOVE 29 TO WK-CONTROLLO
              MOVE REC3-PC-COMM-GEST-GAR(IX-REC-3)
                TO WS-CAMPO-1
              MOVE WP85-COMMIS-GEST(IX-TAB-P85)
                TO WS-CAMPO-2
              SET CNTRL-29     TO TRUE
              MOVE MSG-ERRORE  TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.
      *
      *--> CONTROLLO 30
13087      SET CONTROLLO-30-SI TO TRUE
13087      PERFORM VARYING IX-REC-10 FROM 1 BY 1
13087        UNTIL IX-REC-10 > ELE-MAX-CACHE-10
13087           OR CONTROLLO-30-NO
13087          IF REC10-DESC-TP-MOVI(IX-REC-10) =
13087             TP-IMP-SOST
13087             SET CONTROLLO-30-NO TO TRUE
13087          END-IF
13087      END-PERFORM
13087      IF CONTROLLO-30-SI
13355 *       IF WP85-COD-TARI(IX-TAB-P85) = 'TRVU3'
13355 *       AND WK-ANNO-RIF = 2015
13355 *           MOVE 2,3
13355 *             TO WP85-TS-RIVAL-NET(IX-TAB-P85)
13355 *       END-IF
              IF WP85-TS-RIVAL-NET-NULL(IX-TAB-P85) = HIGH-VALUES
                 MOVE 0 TO WP85-TS-RIVAL-NET(IX-TAB-P85)
              END-IF
              COMPUTE WREC-RENDTO-NET ROUNDED =
                      REC3-TASSO-ANN-RIVAL-PREST-GAR(IX-REC-3)

              IF WREC-RENDTO-NET =
                 WP85-TS-RIVAL-NET(IX-TAB-P85)
                 CONTINUE
              ELSE
                 MOVE 30 TO WK-CONTROLLO
                 MOVE REC3-TASSO-ANN-RIVAL-PREST-GAR(IX-REC-3)
                   TO WS-CAMPO-1
                 MOVE WP85-TS-RIVAL-NET(IX-TAB-P85)
                   TO WS-CAMPO-2
                 SET CNTRL-30     TO TRUE
                 MOVE MSG-ERRORE  TO WS-ERRORE
                 PERFORM E001-SCARTO THRU EX-E001
              END-IF
           END-IF.
      *
      *--> CONTROLLO 31
           IF WP85-MIN-TRNUT-NULL(IX-TAB-P85) = HIGH-VALUES
              MOVE 0 TO WP85-MIN-TRNUT(IX-TAB-P85)
           END-IF
           COMPUTE WREC-MIN-TRNUT  ROUNDED =
                   REC3-REN-MIN-TRNUT(IX-REC-3)
      *    IF REC3-REN-MIN-TRNUT(IX-REC-3) =
           IF WREC-MIN-TRNUT  =
              WP85-MIN-TRNUT(IX-TAB-P85)
              CONTINUE
           ELSE
              MOVE 31 TO WK-CONTROLLO
              MOVE REC3-REN-MIN-TRNUT(IX-REC-3)
                TO WS-CAMPO-1
              MOVE WP85-MIN-TRNUT(IX-TAB-P85)
                TO WS-CAMPO-2
              SET CNTRL-31     TO TRUE
              MOVE MSG-ERRORE  TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.
      *
      *--> CONTROLLO 32
13355 *    IF WP85-COD-TARI(IX-TAB-P85) = 'TRVU3'
13355 *    AND WK-ANNO-RIF = 2015
13355 *       MOVE 0
13355 *         TO WP85-MIN-GARTO(IX-TAB-P85)
13355 *    END-IF
           IF WP85-MIN-GARTO-NULL(IX-TAB-P85) = HIGH-VALUES
              MOVE 0 TO WP85-MIN-GARTO(IX-TAB-P85)
           END-IF
           COMPUTE WREC-MIN-GARTO  ROUNDED =
                   REC3-REN-MIN-GARTO(IX-REC-3)
      *    IF REC3-REN-MIN-GARTO(IX-REC-3) =
           IF WREC-MIN-GARTO =
              WP85-MIN-GARTO(IX-TAB-P85)
              CONTINUE
           ELSE
              MOVE 32 TO WK-CONTROLLO
              MOVE REC3-REN-MIN-GARTO(IX-REC-3)
                TO WS-CAMPO-1
              MOVE WP85-MIN-GARTO(IX-TAB-P85)
                TO WS-CAMPO-2
              SET CNTRL-32     TO TRUE
              MOVE MSG-ERRORE  TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       CONTROLLI-FLUSSO-RIVA-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 43                                                 *
      *----------------------------------------------------------------*
       CONTROLLO-43.

           MOVE 'CONTROLLO-43'                   TO WK-LABEL.
           SET TROVATO-NO           TO TRUE
           IF IDSV0001-ESITO-OK
              PERFORM LETTURA-IMPST-SOST
                 THRU LETTURA-IMPST-SOST-EX
                 IF IDSV0001-ESITO-OK
                 AND TROVATO-SI
      *--> CONTROLLO 43

                    IF ISO-IMPST-SOST-NULL NOT = HIGH-VALUE
                                             AND LOW-VALUE
                                             AND SPACES

                      MOVE ISO-IMPST-SOST TO WS-APPO-43-2

                    ELSE
                        MOVE 43 TO WK-CONTROLLO
                         STRING '203; POLIZZA NOT '
                                'FOUND IN TAB IMPOSTA SOSTITUTIVA'
                         DELIMITED BY SIZE
                         INTO WS-ERRORE
                         PERFORM E001-SCARTO THRU EX-E001
                    END-IF
                 END-IF
           END-IF
      *
           IF IDSV0001-ESITO-OK AND TROVATO-NO
              MOVE 201 TO WK-CONTROLLO
              STRING '201;POLIZZA NON TROVATA NELLA TAB IMPSOST'
              DELIMITED BY SIZE
              INTO WS-ERRORE
           END-IF.


       CONTROLLO-43-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 43                                                 *
      *----------------------------------------------------------------*
       C0024-CNTRL-43.

           MOVE 'C0024-CNTRL-43'          TO WK-LABEL.

           INITIALIZE WS-APPO-43-1
                      WS-APPO-43-2
                      WS-SOMMA

           PERFORM VARYING IX-REC-10 FROM 1 BY 1
             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
               IF REC10-DESC-TP-MOVI(IX-REC-10) =
                  TP-IMP-SOST

                  MOVE 0
                    TO TMP-IMP-OPER

                  PERFORM CONTROLLO-43
                     THRU CONTROLLO-43-EX

                  MOVE REC10-IMP-OPER(IX-REC-10)
                    TO TMP-IMP-OPER

                  MOVE TMP-IMP-OPER
                    TO WS-APPO-43-1

                   ADD WS-APPO-43-1
                    TO WS-SOMMA

               END-IF
           END-PERFORM.

           IF (WS-SOMMA - WS-APPO-43-2) <= 0,05
           AND (WS-SOMMA - WS-APPO-43-2) >= -0,05
               CONTINUE
           ELSE
               MOVE TMP-IMP-OPER
                 TO WS-CAMPO-1
               MOVE ISO-IMPST-SOST
                 TO WS-CAMPO-2
               MOVE 43 TO WK-CONTROLLO
               SET CNTRL-43    TO TRUE
               MOVE MSG-ERRORE  TO WS-ERRORE
               PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0024.
           EXIT.

     ******************************************************************
      *--> LETTURA TABELLA IMPOSTA SOSTITUTIVA
      ******************************************************************
       LETTURA-IMPST-SOST.

           MOVE 'LETTURA-IMPST-SOST'       TO WK-LABEL.
           INITIALIZE LDBV6191
                      IMPST-SOST
                      IDSI0011-BUFFER-DATI
                      IDSI0011-BUFFER-WHERE-COND.

           SET IDSI0011-SELECT           TO TRUE
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE
           SET IDSI0011-TRATT-X-COMPETENZA
                                         TO TRUE
           SET IDSI0011-WHERE-CONDITION  TO TRUE
           SET TROVATO-NO           TO TRUE

           MOVE WK-DT-INF                 TO LDBV6191-DT-INF
           MOVE WK-DT-SUP                 TO LDBV6191-DT-SUP
           MOVE WK-ID-ADES           TO ISO-ID-OGG
           MOVE ZERO
              TO IDSI0011-DATA-INIZIO-EFFETTO
                 IDSI0011-DATA-FINE-EFFETTO
                 IDSI0011-DATA-COMPETENZA
           MOVE 'AD'
              TO ISO-TP-OGG
           MOVE 'LDBS6190'              TO IDSI0011-CODICE-STR-DATO
           MOVE IMPST-SOST              TO IDSI0011-BUFFER-DATI.
           MOVE LDBV6191                TO IDSI0011-BUFFER-WHERE-COND.
           SET IDSV8888-BUSINESS-DBG           TO TRUE
           MOVE 'LDBS6190'                     TO IDSV8888-NOME-PGM.
           SET  IDSV8888-INIZIO                TO TRUE
           MOVE SPACES                         TO IDSV8888-DESC-PGM
           STRING 'Impst_sost'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           SET IDSV8888-BUSINESS-DBG           TO TRUE
           MOVE 'LDBS6190'                     TO IDSV8888-NOME-PGM.
           SET  IDSV8888-FINE                  TO TRUE
           MOVE SPACES                         TO IDSV8888-DESC-PGM
           STRING 'Impst_sost'
                   DELIMITED BY SIZE
                   INTO IDSV8888-DESC-PGM
           END-STRING
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
              WHEN IDSO0011-SUCCESSFUL-SQL
      * --       OPERAZIONE ESEGUITA CON SUCCESSO
                 MOVE IDSO0011-BUFFER-DATI     TO IMPST-SOST
                 SET  TROVATO-SI              TO TRUE
              WHEN IDSO0011-NOT-FOUND
                   CONTINUE
              WHEN OTHER
      * --    ERRORE DB
                     MOVE WK-PGM
                       TO IEAI9901-COD-SERVIZIO-BE
                     MOVE WK-LABEL-ERR
                       TO IEAI9901-LABEL-ERR
                     MOVE '005016'
                       TO IEAI9901-COD-ERRORE
                     STRING 'LDBS6190-ERRORE DB;'
                          IDSO0011-RETURN-CODE ';'
                          IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'LDBS6190-ERRORE DISPATCHER;'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       LETTURA-IMPST-SOST-EX.
           EXIT.


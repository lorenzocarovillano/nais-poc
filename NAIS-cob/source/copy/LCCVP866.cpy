      *----------------------------------------------------------------*
      *    COPY      ..... LCCVP866
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO MOT-LIQ
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVP865 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN MOT-LIQ (LCCVP861)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-MOT-LIQ.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE MOT-LIQ

      *--> NOME TABELLA FISICA DB
           MOVE 'MOT-LIQ'                          TO WK-TABELLA

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WP86-ST-INV(IX-TAB-P86)
              AND NOT WP86-ST-CON(IX-TAB-P86)
              AND WP86-ELE-MOT-LIQ-MAX NOT = 0

              MOVE WMOV-ID-PTF        TO P86-ID-MOVI-CRZ

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WP86-ST-ADD(IX-TAB-P86)

      *-->         ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                   PERFORM ESTR-SEQUENCE
                      THRU ESTR-SEQUENCE-EX

                      MOVE S090-SEQ-TABELLA   TO WP86-ID-PTF(IX-TAB-P86)
                                                 P86-ID-MOT-LIQ
                      MOVE WLQU-ID-PTF(IX-TAB-LQU) TO P86-ID-LIQ


      *-->            TIPO OPERAZIONE DISPATCHER
                      SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WP86-ST-MOD(IX-TAB-P86)

                      MOVE WP86-ID-PTF(IX-TAB-P86) TO P86-ID-MOT-LIQ
                      MOVE WMOV-ID-PTF             TO P86-ID-MOVI-CRZ
                      MOVE WLQU-ID-PTF(IX-TAB-LQU) TO P86-ID-LIQ

      *-->            TIPO OPERAZIONE DISPATCHER
                      SET  IDSI0011-AGGIORNAMENTO-STORICO    TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WP86-ST-DEL(IX-TAB-P86)

                      MOVE WP86-ID-PTF(IX-TAB-P86) TO P86-ID-MOT-LIQ
                      MOVE WMOV-ID-PTF             TO P86-ID-MOVI-CRZ
                      MOVE WLQU-ID-PTF(IX-TAB-LQU) TO P86-ID-LIQ

      *-->            TIPO OPERAZIONE DISPATCHER
                      SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN ADESIONE
                 PERFORM VAL-DCLGEN-P86
                    THRU VAL-DCLGEN-P86-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-P86
                    THRU VALORIZZA-AREA-DSH-P86-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-MOT-LIQ-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-P86.

      *--> DCLGEN TABELLA
           MOVE MOT-LIQ                 TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-P86-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    PORTAFOGLIO VITA ITALIA                                     *
      *    CREATA : 05/02/2007                                         *
      *----------------------------------------------------------------*
      *    GESTIONE INPUT LETTURA PORTAFOGLIO
      *----------------------------------------------------------------*
       01 V1391-AREA-ACTU.
          03 V1391-COD-ACTU             PIC X(12).
          03 V1391-COD-COMPAGNIA-ANIA   PIC 9(05).
          03 V1391-ELE-MAX-ACTU         PIC 9(002).
      *   03 V1391-TAB-PARAM  OCCURS 60.
          03 V1391-TAB-PARAM  OCCURS 100.
      *- area ADA
             05 V1391-TIPO-DATO          PIC X(2).
             05 V1391-TIPO-DATO-NULL REDEFINES
                V1391-TIPO-DATO          PIC X(2).
             05 V1391-LUNGHEZZA-DATO     PIC S9(5)     COMP-3.
             05 V1391-LUNGHEZZA-DATO-NULL REDEFINES
                V1391-LUNGHEZZA-DATO     PIC X(3).
             05 V1391-PRECISIONE-DATO    PIC S9(2)     COMP-3.
             05 V1391-PRECISIONE-DATO-NULL REDEFINES
                V1391-PRECISIONE-DATO    PIC X(2).
             05 V1391-FORMATTAZIONE-DATO PIC X(20).
      *-> area MVV
             05 V1391-ID-MATR-VAL-VAR PIC S9(9) COMP-5.
             05 V1391-IDP-MATR-VAL-VAR PIC S9(9) COMP-5.
             05 V1391-IDP-MATR-VAL-VAR-NULL REDEFINES
                V1391-IDP-MATR-VAL-VAR   PIC X(4).
             05 V1391-TIPO-MOVIMENTO PIC S9(5)     COMP-3.
             05 V1391-TIPO-MOVIMENTO-NULL REDEFINES
                V1391-TIPO-MOVIMENTO   PIC X(3).
             05 V1391-COD-DATO-EXT PIC X(30).
             05 V1391-OBBLIGATORIETA PIC X(1).
             05 V1391-OBBLIGATORIETA-NULL REDEFINES
                V1391-OBBLIGATORIETA   PIC X(1).
             05 V1391-VALORE-DEFAULT PIC X(50).
             05 V1391-COD-STR-DATO-PTF PIC X(30).
             05 V1391-COD-DATO-PTF PIC X(30).
             05 V1391-COD-PARAMETRO PIC X(20).
             05 V1391-OPERAZIONE PIC X(15).
             05 V1391-LIVELLO-OPERAZIONE PIC X(3).
             05 V1391-LIVELLO-OPERAZIONE-NULL REDEFINES
                V1391-LIVELLO-OPERAZIONE   PIC X(3).
             05 V1391-TIPO-OGGETTO PIC X(2).
             05 V1391-TIPO-OGGETTO-NULL REDEFINES
                V1391-TIPO-OGGETTO   PIC X(2).
             05 V1391-WHERE-CONDITION PIC X(300).
             05 V1391-SERVIZIO-LETTURA PIC X(8).
             05 V1391-SERVIZIO-LETTURA-NULL REDEFINES
                V1391-SERVIZIO-LETTURA   PIC X(8).
             05 V1391-MODULO-CALCOLO PIC X(8).
             05 V1391-MODULO-CALCOLO-NULL REDEFINES
                V1391-MODULO-CALCOLO   PIC X(8).
             05 V1391-COD-DATO-INTERNO PIC X(30).


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA PERC_LIQ
      *   ALIAS PLI
      *   ULTIMO AGG. 07 GEN 2016
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-PERC-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-BNFICR-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-ID-RAPP-ANA PIC S9(9)     COMP-3.
             07 (SF)-ID-RAPP-ANA-NULL REDEFINES
                (SF)-ID-RAPP-ANA   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-PC-LIQ PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-LIQ-NULL REDEFINES
                (SF)-PC-LIQ   PIC X(4).
             07 (SF)-IMP-LIQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-LIQ-NULL REDEFINES
                (SF)-IMP-LIQ   PIC X(8).
             07 (SF)-TP-MEZ-PAG PIC X(2).
             07 (SF)-TP-MEZ-PAG-NULL REDEFINES
                (SF)-TP-MEZ-PAG   PIC X(2).
             07 (SF)-ITER-PAG-AVV PIC X(1).
             07 (SF)-ITER-PAG-AVV-NULL REDEFINES
                (SF)-ITER-PAG-AVV   PIC X(1).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-DT-VLT   PIC S9(8) COMP-3.
             07 (SF)-DT-VLT-NULL REDEFINES
                (SF)-DT-VLT   PIC X(5).
             07 (SF)-INT-CNT-CORR-ACCR PIC X(100).
             07 (SF)-COD-IBAN-RIT-CON PIC X(34).
             07 (SF)-COD-IBAN-RIT-CON-NULL REDEFINES
                (SF)-COD-IBAN-RIT-CON   PIC X(34).

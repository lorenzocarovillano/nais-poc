      *****************************************************************
      * N.B. - UTILIZZABILE SOLO INCLUDENDO LE COPY :
      *        IDSV0014
      *        IDSP0014
      *****************************************************************

      *----------------------------------------------------------------*
      * SETTA TIMESTAMP
      *----------------------------------------------------------------*
       ESTRAI-CURRENT-TIMESTAMP.
      *
           EXEC SQL
                 VALUES CURRENT TIMESTAMP
                   INTO :WS-TIMESTAMP-X
           END-EXEC

           IF SQLCODE = 0
              PERFORM Z801-TS-X-TO-N   THRU Z801-EX
           END-IF.
      *
       ESTRAI-CURRENT-TIMESTAMP-EX.
           EXIT.
      *----------------------------------------------------------------*
      * SETTA TIMESTAMP
      *----------------------------------------------------------------*
       ESTRAI-CURRENT-DATE.
      *
           EXEC SQL
                 VALUES CURRENT DATE
                   INTO :WS-DATE-X
           END-EXEC

           IF SQLCODE = 0
              PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           END-IF.
      *
       ESTRAI-CURRENT-DATE-EX.
           EXIT.

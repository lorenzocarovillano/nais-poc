      *-----------------------------------------------------------------*
      *   COMPONENTI COMUNI - PORTAFOGLIO VITA
      *   VERIFICA/ESTRAZIONE POLIZZE PER RECUPERO PROVVIGIONALE
      *   LUNG.
      *-----------------------------------------------------------------*
       05 LCCC0033-DATI-INPUT.
          07 LCCC0033-MACRO-FUNZIONE          PIC X(02).
             88 LCCC0033-VENDITA                VALUE 'VE'.
             88 LCCC0033-STORNI                 VALUE 'ST'.
          07 LCCC0033-MODALITA-SERVIZIO       PIC X(02).
             88 LCCC0033-VERIFICA-REC-PROVV     VALUE '01'.
             88 LCCC0033-ESTR-POL-REC-PROVV     VALUE '02'.
          07 LCCC0033-COD-SOGG                PIC X(20).
          07 LCCC0033-TP-RAPP-ANA             PIC X(02).
          07 LCCC0033-DT-DECOR-POL            PIC S9(8) COMP-3.
          07 LCCC0033-DT-STORNO-POL           PIC S9(8) COMP-3.
       05 LCCC0033-DATI-OUTPUT.
          07 LCCC0033-FLAG-RECUP-PROVV        PIC X(02).
             88 LCCC0033-RECUP-PROVV-SI         VALUE 'SI'.
             88 LCCC0033-RECUP-PROVV-NO         VALUE 'NO'.
      *-- lista polizze stesso contraente per il recupero provv.
          07 LCCC0033-ELE-MAX-RECUP-PROVV     PIC S9(04) COMP.
          07 LCCC0033-RECUPERO-PROVV          OCCURS 15.
             09 LCCC0033-ID-POLI              PIC S9(9)  COMP-3.
             09 LCCC0033-IB-OGG               PIC X(40).
             09 LCCC0033-STATO                PIC X(2).
             09 LCCC0033-CAUSALE              PIC X(2).
             09 LCCC0033-DT-DECORRENZA        PIC S9(8) COMP-3.
             09 LCCC0033-DT-STORNO            PIC S9(8) COMP-3.
             09 LCCC0033-IMP-PREMIO-ANNUO     PIC S9(12)V9(3) COMP-3.

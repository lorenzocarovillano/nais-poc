       01 IDSV0901.
          03 IDSV0901-DATI-GENERICI                    PIC  X(100).
          03 IDSV0901-DATI-FUNZ                        PIC  X(200).
          03 IDSV0901-AREA-BONUS REDEFINES IDSV0901-DATI-FUNZ.
            05 IDSV0901-FLAG-BONUS-FEDELTA             PIC  X(001).
            05 IDSV0901-FLAG-BONUS-RICORR              PIC  X(001).
            05 IDSV0901-FILLER                         PIC  X(198).

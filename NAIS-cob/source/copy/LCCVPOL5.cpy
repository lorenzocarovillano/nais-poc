
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVPOL5
      *   ULTIMO AGG. 07 DIC 2017
      *------------------------------------------------------------

       VAL-DCLGEN-POL.
           IF (SF)-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL
              TO POL-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU
              TO POL-ID-MOVI-CHIU
           END-IF
           IF (SF)-IB-OGG-NULL = HIGH-VALUES
              MOVE (SF)-IB-OGG-NULL
              TO POL-IB-OGG-NULL
           ELSE
              MOVE (SF)-IB-OGG
              TO POL-IB-OGG
           END-IF
           MOVE (SF)-IB-PROP
              TO POL-IB-PROP
           IF (SF)-DT-PROP-NULL = HIGH-VALUES
              MOVE (SF)-DT-PROP-NULL
              TO POL-DT-PROP-NULL
           ELSE
             IF (SF)-DT-PROP = ZERO
                MOVE HIGH-VALUES
                TO POL-DT-PROP-NULL
             ELSE
              MOVE (SF)-DT-PROP
              TO POL-DT-PROP
             END-IF
           END-IF
           IF (SF)-DT-INI-EFF NOT NUMERIC
              MOVE 0 TO POL-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF
              TO POL-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF NOT NUMERIC
              MOVE 0 TO POL-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF
              TO POL-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA
              TO POL-COD-COMP-ANIA
           MOVE (SF)-DT-DECOR
              TO POL-DT-DECOR
           MOVE (SF)-DT-EMIS
              TO POL-DT-EMIS
           MOVE (SF)-TP-POLI
              TO POL-TP-POLI
           IF (SF)-DUR-AA-NULL = HIGH-VALUES
              MOVE (SF)-DUR-AA-NULL
              TO POL-DUR-AA-NULL
           ELSE
              MOVE (SF)-DUR-AA
              TO POL-DUR-AA
           END-IF
           IF (SF)-DUR-MM-NULL = HIGH-VALUES
              MOVE (SF)-DUR-MM-NULL
              TO POL-DUR-MM-NULL
           ELSE
              MOVE (SF)-DUR-MM
              TO POL-DUR-MM
           END-IF
           IF (SF)-DT-SCAD-NULL = HIGH-VALUES
              MOVE (SF)-DT-SCAD-NULL
              TO POL-DT-SCAD-NULL
           ELSE
             IF (SF)-DT-SCAD = ZERO
                MOVE HIGH-VALUES
                TO POL-DT-SCAD-NULL
             ELSE
              MOVE (SF)-DT-SCAD
              TO POL-DT-SCAD
             END-IF
           END-IF
           MOVE (SF)-COD-PROD
              TO POL-COD-PROD
           MOVE (SF)-DT-INI-VLDT-PROD
              TO POL-DT-INI-VLDT-PROD
           IF (SF)-COD-CONV-NULL = HIGH-VALUES
              MOVE (SF)-COD-CONV-NULL
              TO POL-COD-CONV-NULL
           ELSE
              MOVE (SF)-COD-CONV
              TO POL-COD-CONV
           END-IF
           IF (SF)-COD-RAMO-NULL = HIGH-VALUES
              MOVE (SF)-COD-RAMO-NULL
              TO POL-COD-RAMO-NULL
           ELSE
              MOVE (SF)-COD-RAMO
              TO POL-COD-RAMO
           END-IF
           IF (SF)-DT-INI-VLDT-CONV-NULL = HIGH-VALUES
              MOVE (SF)-DT-INI-VLDT-CONV-NULL
              TO POL-DT-INI-VLDT-CONV-NULL
           ELSE
             IF (SF)-DT-INI-VLDT-CONV = ZERO
                MOVE HIGH-VALUES
                TO POL-DT-INI-VLDT-CONV-NULL
             ELSE
              MOVE (SF)-DT-INI-VLDT-CONV
              TO POL-DT-INI-VLDT-CONV
             END-IF
           END-IF
           IF (SF)-DT-APPLZ-CONV-NULL = HIGH-VALUES
              MOVE (SF)-DT-APPLZ-CONV-NULL
              TO POL-DT-APPLZ-CONV-NULL
           ELSE
             IF (SF)-DT-APPLZ-CONV = ZERO
                MOVE HIGH-VALUES
                TO POL-DT-APPLZ-CONV-NULL
             ELSE
              MOVE (SF)-DT-APPLZ-CONV
              TO POL-DT-APPLZ-CONV
             END-IF
           END-IF
           MOVE (SF)-TP-FRM-ASSVA
              TO POL-TP-FRM-ASSVA
           IF (SF)-TP-RGM-FISC-NULL = HIGH-VALUES
              MOVE (SF)-TP-RGM-FISC-NULL
              TO POL-TP-RGM-FISC-NULL
           ELSE
              MOVE (SF)-TP-RGM-FISC
              TO POL-TP-RGM-FISC
           END-IF
           IF (SF)-FL-ESTAS-NULL = HIGH-VALUES
              MOVE (SF)-FL-ESTAS-NULL
              TO POL-FL-ESTAS-NULL
           ELSE
              MOVE (SF)-FL-ESTAS
              TO POL-FL-ESTAS
           END-IF
           IF (SF)-FL-RSH-COMUN-NULL = HIGH-VALUES
              MOVE (SF)-FL-RSH-COMUN-NULL
              TO POL-FL-RSH-COMUN-NULL
           ELSE
              MOVE (SF)-FL-RSH-COMUN
              TO POL-FL-RSH-COMUN
           END-IF
           IF (SF)-FL-RSH-COMUN-COND-NULL = HIGH-VALUES
              MOVE (SF)-FL-RSH-COMUN-COND-NULL
              TO POL-FL-RSH-COMUN-COND-NULL
           ELSE
              MOVE (SF)-FL-RSH-COMUN-COND
              TO POL-FL-RSH-COMUN-COND
           END-IF
           MOVE (SF)-TP-LIV-GENZ-TIT
              TO POL-TP-LIV-GENZ-TIT
           IF (SF)-FL-COP-FINANZ-NULL = HIGH-VALUES
              MOVE (SF)-FL-COP-FINANZ-NULL
              TO POL-FL-COP-FINANZ-NULL
           ELSE
              MOVE (SF)-FL-COP-FINANZ
              TO POL-FL-COP-FINANZ
           END-IF
           IF (SF)-TP-APPLZ-DIR-NULL = HIGH-VALUES
              MOVE (SF)-TP-APPLZ-DIR-NULL
              TO POL-TP-APPLZ-DIR-NULL
           ELSE
              MOVE (SF)-TP-APPLZ-DIR
              TO POL-TP-APPLZ-DIR
           END-IF
           IF (SF)-SPE-MED-NULL = HIGH-VALUES
              MOVE (SF)-SPE-MED-NULL
              TO POL-SPE-MED-NULL
           ELSE
              MOVE (SF)-SPE-MED
              TO POL-SPE-MED
           END-IF
           IF (SF)-DIR-EMIS-NULL = HIGH-VALUES
              MOVE (SF)-DIR-EMIS-NULL
              TO POL-DIR-EMIS-NULL
           ELSE
              MOVE (SF)-DIR-EMIS
              TO POL-DIR-EMIS
           END-IF
           IF (SF)-DIR-1O-VERS-NULL = HIGH-VALUES
              MOVE (SF)-DIR-1O-VERS-NULL
              TO POL-DIR-1O-VERS-NULL
           ELSE
              MOVE (SF)-DIR-1O-VERS
              TO POL-DIR-1O-VERS
           END-IF
           IF (SF)-DIR-VERS-AGG-NULL = HIGH-VALUES
              MOVE (SF)-DIR-VERS-AGG-NULL
              TO POL-DIR-VERS-AGG-NULL
           ELSE
              MOVE (SF)-DIR-VERS-AGG
              TO POL-DIR-VERS-AGG
           END-IF
           IF (SF)-COD-DVS-NULL = HIGH-VALUES
              MOVE (SF)-COD-DVS-NULL
              TO POL-COD-DVS-NULL
           ELSE
              MOVE (SF)-COD-DVS
              TO POL-COD-DVS
           END-IF
           IF (SF)-FL-FNT-AZ-NULL = HIGH-VALUES
              MOVE (SF)-FL-FNT-AZ-NULL
              TO POL-FL-FNT-AZ-NULL
           ELSE
              MOVE (SF)-FL-FNT-AZ
              TO POL-FL-FNT-AZ
           END-IF
           IF (SF)-FL-FNT-ADER-NULL = HIGH-VALUES
              MOVE (SF)-FL-FNT-ADER-NULL
              TO POL-FL-FNT-ADER-NULL
           ELSE
              MOVE (SF)-FL-FNT-ADER
              TO POL-FL-FNT-ADER
           END-IF
           IF (SF)-FL-FNT-TFR-NULL = HIGH-VALUES
              MOVE (SF)-FL-FNT-TFR-NULL
              TO POL-FL-FNT-TFR-NULL
           ELSE
              MOVE (SF)-FL-FNT-TFR
              TO POL-FL-FNT-TFR
           END-IF
           IF (SF)-FL-FNT-VOLO-NULL = HIGH-VALUES
              MOVE (SF)-FL-FNT-VOLO-NULL
              TO POL-FL-FNT-VOLO-NULL
           ELSE
              MOVE (SF)-FL-FNT-VOLO
              TO POL-FL-FNT-VOLO
           END-IF
           IF (SF)-TP-OPZ-A-SCAD-NULL = HIGH-VALUES
              MOVE (SF)-TP-OPZ-A-SCAD-NULL
              TO POL-TP-OPZ-A-SCAD-NULL
           ELSE
              MOVE (SF)-TP-OPZ-A-SCAD
              TO POL-TP-OPZ-A-SCAD
           END-IF
           IF (SF)-AA-DIFF-PROR-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-AA-DIFF-PROR-DFLT-NULL
              TO POL-AA-DIFF-PROR-DFLT-NULL
           ELSE
              MOVE (SF)-AA-DIFF-PROR-DFLT
              TO POL-AA-DIFF-PROR-DFLT
           END-IF
           IF (SF)-FL-VER-PROD-NULL = HIGH-VALUES
              MOVE (SF)-FL-VER-PROD-NULL
              TO POL-FL-VER-PROD-NULL
           ELSE
              MOVE (SF)-FL-VER-PROD
              TO POL-FL-VER-PROD
           END-IF
           IF (SF)-DUR-GG-NULL = HIGH-VALUES
              MOVE (SF)-DUR-GG-NULL
              TO POL-DUR-GG-NULL
           ELSE
              MOVE (SF)-DUR-GG
              TO POL-DUR-GG
           END-IF
           IF (SF)-DIR-QUIET-NULL = HIGH-VALUES
              MOVE (SF)-DIR-QUIET-NULL
              TO POL-DIR-QUIET-NULL
           ELSE
              MOVE (SF)-DIR-QUIET
              TO POL-DIR-QUIET
           END-IF
           IF (SF)-TP-PTF-ESTNO-NULL = HIGH-VALUES
              MOVE (SF)-TP-PTF-ESTNO-NULL
              TO POL-TP-PTF-ESTNO-NULL
           ELSE
              MOVE (SF)-TP-PTF-ESTNO
              TO POL-TP-PTF-ESTNO
           END-IF
           IF (SF)-FL-CUM-PRE-CNTR-NULL = HIGH-VALUES
              MOVE (SF)-FL-CUM-PRE-CNTR-NULL
              TO POL-FL-CUM-PRE-CNTR-NULL
           ELSE
              MOVE (SF)-FL-CUM-PRE-CNTR
              TO POL-FL-CUM-PRE-CNTR
           END-IF
           IF (SF)-FL-AMMB-MOVI-NULL = HIGH-VALUES
              MOVE (SF)-FL-AMMB-MOVI-NULL
              TO POL-FL-AMMB-MOVI-NULL
           ELSE
              MOVE (SF)-FL-AMMB-MOVI
              TO POL-FL-AMMB-MOVI
           END-IF
           IF (SF)-CONV-GECO-NULL = HIGH-VALUES
              MOVE (SF)-CONV-GECO-NULL
              TO POL-CONV-GECO-NULL
           ELSE
              MOVE (SF)-CONV-GECO
              TO POL-CONV-GECO
           END-IF
           IF (SF)-DS-RIGA NOT NUMERIC
              MOVE 0 TO POL-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA
              TO POL-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO POL-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO POL-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO POL-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ NOT NUMERIC
              MOVE 0 TO POL-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ
              TO POL-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ NOT NUMERIC
              MOVE 0 TO POL-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ
              TO POL-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO POL-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO POL-DS-STATO-ELAB
           IF (SF)-FL-SCUDO-FISC-NULL = HIGH-VALUES
              MOVE (SF)-FL-SCUDO-FISC-NULL
              TO POL-FL-SCUDO-FISC-NULL
           ELSE
              MOVE (SF)-FL-SCUDO-FISC
              TO POL-FL-SCUDO-FISC
           END-IF
           IF (SF)-FL-TRASFE-NULL = HIGH-VALUES
              MOVE (SF)-FL-TRASFE-NULL
              TO POL-FL-TRASFE-NULL
           ELSE
              MOVE (SF)-FL-TRASFE
              TO POL-FL-TRASFE
           END-IF
           IF (SF)-FL-TFR-STRC-NULL = HIGH-VALUES
              MOVE (SF)-FL-TFR-STRC-NULL
              TO POL-FL-TFR-STRC-NULL
           ELSE
              MOVE (SF)-FL-TFR-STRC
              TO POL-FL-TFR-STRC
           END-IF
           IF (SF)-DT-PRESC-NULL = HIGH-VALUES
              MOVE (SF)-DT-PRESC-NULL
              TO POL-DT-PRESC-NULL
           ELSE
             IF (SF)-DT-PRESC = ZERO
                MOVE HIGH-VALUES
                TO POL-DT-PRESC-NULL
             ELSE
              MOVE (SF)-DT-PRESC
              TO POL-DT-PRESC
             END-IF
           END-IF
           IF (SF)-COD-CONV-AGG-NULL = HIGH-VALUES
              MOVE (SF)-COD-CONV-AGG-NULL
              TO POL-COD-CONV-AGG-NULL
           ELSE
              MOVE (SF)-COD-CONV-AGG
              TO POL-COD-CONV-AGG
           END-IF
           IF (SF)-SUBCAT-PROD-NULL = HIGH-VALUES
              MOVE (SF)-SUBCAT-PROD-NULL
              TO POL-SUBCAT-PROD-NULL
           ELSE
              MOVE (SF)-SUBCAT-PROD
              TO POL-SUBCAT-PROD
           END-IF
           IF (SF)-FL-QUEST-ADEGZ-ASS-NULL = HIGH-VALUES
              MOVE (SF)-FL-QUEST-ADEGZ-ASS-NULL
              TO POL-FL-QUEST-ADEGZ-ASS-NULL
           ELSE
              MOVE (SF)-FL-QUEST-ADEGZ-ASS
              TO POL-FL-QUEST-ADEGZ-ASS
           END-IF
           IF (SF)-COD-TPA-NULL = HIGH-VALUES
              MOVE (SF)-COD-TPA-NULL
              TO POL-COD-TPA-NULL
           ELSE
              MOVE (SF)-COD-TPA
              TO POL-COD-TPA
           END-IF
           IF (SF)-ID-ACC-COMM-NULL = HIGH-VALUES
              MOVE (SF)-ID-ACC-COMM-NULL
              TO POL-ID-ACC-COMM-NULL
           ELSE
              MOVE (SF)-ID-ACC-COMM
              TO POL-ID-ACC-COMM
           END-IF
           IF (SF)-FL-POLI-CPI-PR-NULL = HIGH-VALUES
              MOVE (SF)-FL-POLI-CPI-PR-NULL
              TO POL-FL-POLI-CPI-PR-NULL
           ELSE
              MOVE (SF)-FL-POLI-CPI-PR
              TO POL-FL-POLI-CPI-PR
           END-IF
           IF (SF)-FL-POLI-BUNDLING-NULL = HIGH-VALUES
              MOVE (SF)-FL-POLI-BUNDLING-NULL
              TO POL-FL-POLI-BUNDLING-NULL
           ELSE
              MOVE (SF)-FL-POLI-BUNDLING
              TO POL-FL-POLI-BUNDLING
           END-IF
           IF (SF)-IND-POLI-PRIN-COLL-NULL = HIGH-VALUES
              MOVE (SF)-IND-POLI-PRIN-COLL-NULL
              TO POL-IND-POLI-PRIN-COLL-NULL
           ELSE
              MOVE (SF)-IND-POLI-PRIN-COLL
              TO POL-IND-POLI-PRIN-COLL
           END-IF
           IF (SF)-FL-VND-BUNDLE-NULL = HIGH-VALUES
              MOVE (SF)-FL-VND-BUNDLE-NULL
              TO POL-FL-VND-BUNDLE-NULL
           ELSE
              MOVE (SF)-FL-VND-BUNDLE
              TO POL-FL-VND-BUNDLE
           END-IF
           IF (SF)-IB-BS-NULL = HIGH-VALUES
              MOVE (SF)-IB-BS-NULL
              TO POL-IB-BS-NULL
           ELSE
              MOVE (SF)-IB-BS
              TO POL-IB-BS
           END-IF
           IF (SF)-FL-POLI-IFP-NULL = HIGH-VALUES
              MOVE (SF)-FL-POLI-IFP-NULL
              TO POL-FL-POLI-IFP-NULL
           ELSE
              MOVE (SF)-FL-POLI-IFP
              TO POL-FL-POLI-IFP
           END-IF.
       VAL-DCLGEN-POL-EX.
           EXIT.

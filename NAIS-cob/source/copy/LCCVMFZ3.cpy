
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVMFZ3
      *   ULTIMO AGG. 09 LUG 2010
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-MFZ.
           MOVE MFZ-ID-MOVI-FINRIO
             TO (SF)-ID-PTF(IX-TAB-MFZ)
           MOVE MFZ-ID-MOVI-FINRIO
             TO (SF)-ID-MOVI-FINRIO(IX-TAB-MFZ)
           IF MFZ-ID-ADES-NULL = HIGH-VALUES
              MOVE MFZ-ID-ADES-NULL
                TO (SF)-ID-ADES-NULL(IX-TAB-MFZ)
           ELSE
              MOVE MFZ-ID-ADES
                TO (SF)-ID-ADES(IX-TAB-MFZ)
           END-IF
           IF MFZ-ID-LIQ-NULL = HIGH-VALUES
              MOVE MFZ-ID-LIQ-NULL
                TO (SF)-ID-LIQ-NULL(IX-TAB-MFZ)
           ELSE
              MOVE MFZ-ID-LIQ
                TO (SF)-ID-LIQ(IX-TAB-MFZ)
           END-IF
           IF MFZ-ID-TIT-CONT-NULL = HIGH-VALUES
              MOVE MFZ-ID-TIT-CONT-NULL
                TO (SF)-ID-TIT-CONT-NULL(IX-TAB-MFZ)
           ELSE
              MOVE MFZ-ID-TIT-CONT
                TO (SF)-ID-TIT-CONT(IX-TAB-MFZ)
           END-IF
           MOVE MFZ-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-MFZ)
           IF MFZ-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE MFZ-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-MFZ)
           ELSE
              MOVE MFZ-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-MFZ)
           END-IF
           MOVE MFZ-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-MFZ)
           MOVE MFZ-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-MFZ)
           MOVE MFZ-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-MFZ)
           MOVE MFZ-TP-MOVI-FINRIO
             TO (SF)-TP-MOVI-FINRIO(IX-TAB-MFZ)
           IF MFZ-DT-EFF-MOVI-FINRIO-NULL = HIGH-VALUES
              MOVE MFZ-DT-EFF-MOVI-FINRIO-NULL
                TO (SF)-DT-EFF-MOVI-FINRIO-NULL(IX-TAB-MFZ)
           ELSE
              MOVE MFZ-DT-EFF-MOVI-FINRIO
                TO (SF)-DT-EFF-MOVI-FINRIO(IX-TAB-MFZ)
           END-IF
           IF MFZ-DT-ELAB-NULL = HIGH-VALUES
              MOVE MFZ-DT-ELAB-NULL
                TO (SF)-DT-ELAB-NULL(IX-TAB-MFZ)
           ELSE
              MOVE MFZ-DT-ELAB
                TO (SF)-DT-ELAB(IX-TAB-MFZ)
           END-IF
           IF MFZ-DT-RICH-MOVI-NULL = HIGH-VALUES
              MOVE MFZ-DT-RICH-MOVI-NULL
                TO (SF)-DT-RICH-MOVI-NULL(IX-TAB-MFZ)
           ELSE
              MOVE MFZ-DT-RICH-MOVI
                TO (SF)-DT-RICH-MOVI(IX-TAB-MFZ)
           END-IF
           IF MFZ-COS-OPRZ-NULL = HIGH-VALUES
              MOVE MFZ-COS-OPRZ-NULL
                TO (SF)-COS-OPRZ-NULL(IX-TAB-MFZ)
           ELSE
              MOVE MFZ-COS-OPRZ
                TO (SF)-COS-OPRZ(IX-TAB-MFZ)
           END-IF
           MOVE MFZ-STAT-MOVI
             TO (SF)-STAT-MOVI(IX-TAB-MFZ)
           MOVE MFZ-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-MFZ)
           MOVE MFZ-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-MFZ)
           MOVE MFZ-DS-VER
             TO (SF)-DS-VER(IX-TAB-MFZ)
           MOVE MFZ-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-MFZ)
           MOVE MFZ-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-MFZ)
           MOVE MFZ-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-MFZ)
           MOVE MFZ-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-MFZ)
           IF MFZ-TP-CAUS-RETTIFICA-NULL = HIGH-VALUES
              MOVE MFZ-TP-CAUS-RETTIFICA-NULL
                TO (SF)-TP-CAUS-RETTIFICA-NULL(IX-TAB-MFZ)
           ELSE
              MOVE MFZ-TP-CAUS-RETTIFICA
                TO (SF)-TP-CAUS-RETTIFICA(IX-TAB-MFZ)
           END-IF.
       VALORIZZA-OUTPUT-MFZ-EX.
           EXIT.

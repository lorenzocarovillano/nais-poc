      *------------------------------------------------------------*
      *    PORTAFOGLIO VITA - PROCESSO VENDITA                     *
      *    CHIAMATA  AL DISPATCHER PER OPRAZIONI DI SELECT O FETCH *
      *          E GESTIONE ERRORE ALL  (TABELLA ASSET ALLOCATION) *
      *     ULTIMO AGG.  07 FEB 2007                               *
      *------------------------------------------------------------*

       LETTURA-ALL.
           IF IDSI0011-SELECT
              PERFORM SELECT-ALL
                THRU SELECT-ALL-EX
           ELSE
              PERFORM FETCH-ALL
                THRU FETCH-ALL-EX
           END-IF
       LETTURA-ALL-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         SELECT                                 *
      *----------------------------------------------------------------*
       SELECT-ALL.

           PERFORM CALL-DISPATCHER
               THRU CALL-DISPATCHER-EX.

              IF IDSO0011-SUCCESSFUL-RC
                 EVALUATE TRUE
                     WHEN IDSO0011-SUCCESSFUL-SQL
      * -->       OPERAZIONE ESEGUITA CORRETTAMENTE
                        MOVE IDSO0011-BUFFER-DATI
                          TO AST-ALLOC
                        PERFORM VALORIZZA-OUTPUT-ALL
                          THRU VALORIZZA-OUTPUT-ALL-EX
                     WHEN IDSO0011-NOT-FOUND
      * --->          CAMPO $ NON TROVATO
                            MOVE WK-PGM
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'SELECT-ALL'
                             TO IEAI9901-LABEL-ERR
                           MOVE '005019'
                             TO IEAI9901-COD-ERRORE
                           MOVE 'ID-OGGETTO'
                             TO IEAI9901-PARAMETRI-ERR
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                      WHEN OTHER
      * --->          ERRORE DI ACCESSO AL DB
                        MOVE WK-PGM
                                        TO IEAI9901-COD-SERVIZIO-BE
                        MOVE 'SELECT-ALL'
                                        TO IEAI9901-LABEL-ERR
                        MOVE '005015'   TO IEAI9901-COD-ERRORE
                        MOVE WK-TABELLA TO IEAI9901-PARAMETRI-ERR
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                           THRU EX-S0300
                 END-EVALUATE
              ELSE
      * -->       GESTIRE ERRORE DISPATCHER
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'SELECT-ALL'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005016'
                   TO IEAI9901-COD-ERRORE
                 MOVE SPACES
                   TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-PERFORM.
       SELECT-ALL-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         FETCH                                  *
      *----------------------------------------------------------------*

       FETCH-ALL.

           SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
           SET  WCOM-OVERFLOW-NO                  TO TRUE.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR WCOM-OVERFLOW-YES

               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      * -->                 NON TROVATA
                           IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM
                                TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'FETCH-ALL'
                                TO IEAI9901-LABEL-ERR
                              MOVE '005019'
                                TO IEAI9901-COD-ERRORE
                              MOVE 'ID-PTF'
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                           END-IF
                      WHEN IDSO0011-SUCCESSFUL-SQL
      * -->                 OPERAZIONE ESEGUITA CORRETTAMENTE
                           MOVE IDSO0011-BUFFER-DATI  TO AST-ALLOC
                           ADD  1                     TO IX-TAB-ALL

      * -->  Se il contatore di lettura � maggiore dell' elemento MAX
      * -->  previsto per la TAB.AST_ALLOCATION allora siamo in overflow
                           IF IX-TAB-BEP > (SF)-ELE-ASSET-MAX
                              SET WCOM-OVERFLOW-YES   TO TRUE
                           ELSE
                              PERFORM VALORIZZA-OUTPUT-ALL
                                 THRU VALORIZZA-OUTPUT-ALL-EX
                              SET IDSI0011-FETCH-NEXT TO TRUE
                           END-IF
                      WHEN OTHER
      * -->                 ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'FETCH-ALL'
                             TO IEAI9901-LABEL-ERR
                           MOVE '005015'
                             TO IEAI9901-COD-ERRORE
                           MOVE WK-TABELLA
                             TO IEAI9901-PARAMETRI-ERR
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      * -->        ERRORE DISPATCHER
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'FETCH-ALL'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005016'
                    TO IEAI9901-COD-ERRORE
                  MOVE SPACES
                    TO IEAI9901-PARAMETRI-ERR
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

       FETCH-ALL-EX.
           EXIT.



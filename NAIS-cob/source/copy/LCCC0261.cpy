      *------------------------------------------------------------*
      *   PORTAFOGLIO VITA - PROCESSO VENDITA                      *
      *   AREA INFRASTRUTTURALE OPERAZIONI AUTOMATICHE             *
      *   ULTIMO AGG.  02 LUG 2007                                 *
      *------------------------------------------------------------*
      * --  PROVENIENZA CHIAMATA
          05 (SF)-BS-CALL-TYPE           PIC X(01) VALUE 'S'.
              88 STANDARD-CALL               VALUE 'S'.
              88 ALPO-CALL                   VALUE 'A'.
      * --  DATI POLIZZA
          05 (SF)-DATI-POLIZZA.
             07 (SF)-ID-POLIZZA          PIC S9(09) COMP-3.
             07 (SF)-POL-IB-OGG          PIC X(40).
             07 (SF)-LIVELLO-GEN-TIT     PIC X(002).
                 88 (SF)-Q-UNICO            VALUE 'PO'.
                 88 (SF)-Q-SEPARATO         VALUE 'AD'.
      * --  DATI ADESIONE
          05 (SF)-DATI-ADESIONE.
             07 (SF)-ID-ADES             PIC S9(09) COMP-3.
             07 (SF)-ADE-IB-OGG          PIC X(40).
      * --  MOVIMENTO CREAZIONE
          05 (SF)-ID-MOVI-CREAZ            PIC S9(09) COMP-3.
      * --  PRIMA VOLTA
          05 (SF)-PRIMA-VOLTA            PIC X(01).
             88 (SF)-PRIMA-VOLTA-SI          VALUE 'Y'.
             88 (SF)-PRIMA-VOLTA-NO          VALUE 'N'.
      * -- INDICATORE DI ESECUZIONE STEP SUCCESSIVI
          05 (SF)-STEP-SUCC              PIC X(01) VALUE 'Y'.
              88 (SF)-STEP-SUCC-SI           VALUE 'Y'.
              88 (SF)-STEP-SUCC-NO           VALUE 'N'.
      * -- GESTIONE ERRORE ELABORATIVO
          05 (SF)-WRITE                  PIC X(02).
             88 (SF)-WRITE-NIENTE            VALUE '  '.
             88 (SF)-WRITE-OK                VALUE 'OK'.
             88 (SF)-WRITE-ERR               VALUE 'ER'.
          05 (SF)-DATI-MBS.
             07 (SF)-TP-OGG-MBS          PIC X(02).
                88 (SF)-POLIZZA-MBS          VALUE 'PO'.
                88 (SF)-ADESIONE-MBS         VALUE 'AD'.
             07 (SF)-ID-BATCH            PIC S9(09)  COMP-3.
             07 (SF)-ID-BATCH-NULL REDEFINES
                (SF)-ID-BATCH            PIC X(05).
             07 (SF)-ID-JOB              PIC S9(09)  COMP-3.
             07 (SF)-ID-JOB-NULL REDEFINES
                (SF)-ID-JOB              PIC X(05).
             07 (SF)-TP-FRM-ASSVA        PIC X(02).
                 88 (SF)-COLLETTIVA      VALUE 'CO'.
                 88 (SF)-INDIVIDUALE     VALUE 'IN'.
             07 (SF)-STEP-ELAB           PIC X(01).
             07 (SF)-D-INPUT-MOVI-SOSP   PIC X(300).
           05 (SF)-DATI-BLOCCO.
      * --  ID BLOCCO
             07 (SF)-VERIFICA-BLOCCO     PIC X(01).
                88 (SF)-BLC-PRESENTE-PTF    VALUE '0'.
                88 (SF)-BLC-CENSITO-PTF     VALUE '1'.
             07 (SF)-ID-BLOCCO-CRZ       PIC S9(09) COMP-3.
             07 (SF)-TP-OGG-BLOCCO       PIC X(02).
                88 (SF)-POLIZZA-L11         VALUE 'PO'.
                88 (SF)-ADESIONE-L11        VALUE 'AD'.



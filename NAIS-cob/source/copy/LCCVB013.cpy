
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVB013
      *   ULTIMO AGG. 09 LUG 2010
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-B01.
           MOVE B01-ID-BILA-FND-ESTR
             TO (SF)-ID-PTF
           MOVE B01-ID-BILA-FND-ESTR
             TO (SF)-ID-BILA-FND-ESTR
           MOVE B01-ID-BILA-TRCH-ESTR
             TO (SF)-ID-BILA-TRCH-ESTR
           MOVE B01-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE B01-ID-RICH-ESTRAZ-MAS
             TO (SF)-ID-RICH-ESTRAZ-MAS
           IF B01-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
              MOVE B01-ID-RICH-ESTRAZ-AGG-NULL
                TO (SF)-ID-RICH-ESTRAZ-AGG-NULL
           ELSE
              MOVE B01-ID-RICH-ESTRAZ-AGG
                TO (SF)-ID-RICH-ESTRAZ-AGG
           END-IF
           MOVE B01-DT-RIS
             TO (SF)-DT-RIS
           MOVE B01-ID-POLI
             TO (SF)-ID-POLI
           MOVE B01-ID-ADES
             TO (SF)-ID-ADES
           MOVE B01-ID-GAR
             TO (SF)-ID-GAR
           MOVE B01-ID-TRCH-DI-GAR
             TO (SF)-ID-TRCH-DI-GAR
           MOVE B01-COD-FND
             TO (SF)-COD-FND
           IF B01-DT-QTZ-INI-NULL = HIGH-VALUES
              MOVE B01-DT-QTZ-INI-NULL
                TO (SF)-DT-QTZ-INI-NULL
           ELSE
              MOVE B01-DT-QTZ-INI
                TO (SF)-DT-QTZ-INI
           END-IF
           IF B01-NUM-QUO-INI-NULL = HIGH-VALUES
              MOVE B01-NUM-QUO-INI-NULL
                TO (SF)-NUM-QUO-INI-NULL
           ELSE
              MOVE B01-NUM-QUO-INI
                TO (SF)-NUM-QUO-INI
           END-IF
           IF B01-NUM-QUO-DT-CALC-NULL = HIGH-VALUES
              MOVE B01-NUM-QUO-DT-CALC-NULL
                TO (SF)-NUM-QUO-DT-CALC-NULL
           ELSE
              MOVE B01-NUM-QUO-DT-CALC
                TO (SF)-NUM-QUO-DT-CALC
           END-IF
           IF B01-VAL-QUO-INI-NULL = HIGH-VALUES
              MOVE B01-VAL-QUO-INI-NULL
                TO (SF)-VAL-QUO-INI-NULL
           ELSE
              MOVE B01-VAL-QUO-INI
                TO (SF)-VAL-QUO-INI
           END-IF
           IF B01-DT-VALZZ-QUO-DT-CA-NULL = HIGH-VALUES
              MOVE B01-DT-VALZZ-QUO-DT-CA-NULL
                TO (SF)-DT-VALZZ-QUO-DT-CA-NULL
           ELSE
              MOVE B01-DT-VALZZ-QUO-DT-CA
                TO (SF)-DT-VALZZ-QUO-DT-CA
           END-IF
           IF B01-VAL-QUO-DT-CALC-NULL = HIGH-VALUES
              MOVE B01-VAL-QUO-DT-CALC-NULL
                TO (SF)-VAL-QUO-DT-CALC-NULL
           ELSE
              MOVE B01-VAL-QUO-DT-CALC
                TO (SF)-VAL-QUO-DT-CALC
           END-IF
           IF B01-VAL-QUO-T-NULL = HIGH-VALUES
              MOVE B01-VAL-QUO-T-NULL
                TO (SF)-VAL-QUO-T-NULL
           ELSE
              MOVE B01-VAL-QUO-T
                TO (SF)-VAL-QUO-T
           END-IF
           IF B01-PC-INVST-NULL = HIGH-VALUES
              MOVE B01-PC-INVST-NULL
                TO (SF)-PC-INVST-NULL
           ELSE
              MOVE B01-PC-INVST
                TO (SF)-PC-INVST
           END-IF
           MOVE B01-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE B01-DS-VER
             TO (SF)-DS-VER
           MOVE B01-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE B01-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE B01-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB.
       VALORIZZA-OUTPUT-B01-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVNUM5
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-NUM.
           MOVE (SF)-COD-COMP-ANIA
              TO NUM-COD-COMP-ANIA
           MOVE (SF)-ID-OGG
              TO NUM-ID-OGG
           MOVE (SF)-TP-OGG
              TO NUM-TP-OGG
           IF (SF)-ULT-NUM-LIN-NULL = HIGH-VALUES
              MOVE (SF)-ULT-NUM-LIN-NULL
              TO NUM-ULT-NUM-LIN-NULL
           ELSE
              MOVE (SF)-ULT-NUM-LIN
              TO NUM-ULT-NUM-LIN
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO NUM-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO NUM-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO NUM-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO NUM-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO NUM-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO NUM-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO NUM-DS-STATO-ELAB.
       VAL-DCLGEN-NUM-EX.
           EXIT.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA LIQ
      *   ALIAS LQU
      *   ULTIMO AGG. 03 GIU 2019
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-OGG PIC S9(9)     COMP-3.
             07 (SF)-TP-OGG PIC X(2).
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-IB-OGG PIC X(40).
             07 (SF)-IB-OGG-NULL REDEFINES
                (SF)-IB-OGG   PIC X(40).
             07 (SF)-TP-LIQ PIC X(2).
             07 (SF)-DESC-CAU-EVE-SIN PIC X(100).
             07 (SF)-COD-CAU-SIN PIC X(20).
             07 (SF)-COD-CAU-SIN-NULL REDEFINES
                (SF)-COD-CAU-SIN   PIC X(20).
             07 (SF)-COD-SIN-CATSTRF PIC X(20).
             07 (SF)-COD-SIN-CATSTRF-NULL REDEFINES
                (SF)-COD-SIN-CATSTRF   PIC X(20).
             07 (SF)-DT-MOR   PIC S9(8) COMP-3.
             07 (SF)-DT-MOR-NULL REDEFINES
                (SF)-DT-MOR   PIC X(5).
             07 (SF)-DT-DEN   PIC S9(8) COMP-3.
             07 (SF)-DT-DEN-NULL REDEFINES
                (SF)-DT-DEN   PIC X(5).
             07 (SF)-DT-PERV-DEN   PIC S9(8) COMP-3.
             07 (SF)-DT-PERV-DEN-NULL REDEFINES
                (SF)-DT-PERV-DEN   PIC X(5).
             07 (SF)-DT-RICH   PIC S9(8) COMP-3.
             07 (SF)-DT-RICH-NULL REDEFINES
                (SF)-DT-RICH   PIC X(5).
             07 (SF)-TP-SIN PIC X(2).
             07 (SF)-TP-SIN-NULL REDEFINES
                (SF)-TP-SIN   PIC X(2).
             07 (SF)-TP-RISC PIC X(2).
             07 (SF)-TP-RISC-NULL REDEFINES
                (SF)-TP-RISC   PIC X(2).
             07 (SF)-TP-MET-RISC PIC S9(2)     COMP-3.
             07 (SF)-TP-MET-RISC-NULL REDEFINES
                (SF)-TP-MET-RISC   PIC X(2).
             07 (SF)-DT-LIQ   PIC S9(8) COMP-3.
             07 (SF)-DT-LIQ-NULL REDEFINES
                (SF)-DT-LIQ   PIC X(5).
             07 (SF)-COD-DVS PIC X(20).
             07 (SF)-COD-DVS-NULL REDEFINES
                (SF)-COD-DVS   PIC X(20).
             07 (SF)-TOT-IMP-LRD-LIQTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IMP-LRD-LIQTO-NULL REDEFINES
                (SF)-TOT-IMP-LRD-LIQTO   PIC X(8).
             07 (SF)-TOT-IMP-PREST PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IMP-PREST-NULL REDEFINES
                (SF)-TOT-IMP-PREST   PIC X(8).
             07 (SF)-TOT-IMP-INTR-PREST PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IMP-INTR-PREST-NULL REDEFINES
                (SF)-TOT-IMP-INTR-PREST   PIC X(8).
             07 (SF)-TOT-IMP-UTI PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IMP-UTI-NULL REDEFINES
                (SF)-TOT-IMP-UTI   PIC X(8).
             07 (SF)-TOT-IMP-RIT-TFR PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IMP-RIT-TFR-NULL REDEFINES
                (SF)-TOT-IMP-RIT-TFR   PIC X(8).
             07 (SF)-TOT-IMP-RIT-ACC PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IMP-RIT-ACC-NULL REDEFINES
                (SF)-TOT-IMP-RIT-ACC   PIC X(8).
             07 (SF)-TOT-IMP-RIT-VIS PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IMP-RIT-VIS-NULL REDEFINES
                (SF)-TOT-IMP-RIT-VIS   PIC X(8).
             07 (SF)-TOT-IMPB-TFR PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IMPB-TFR-NULL REDEFINES
                (SF)-TOT-IMPB-TFR   PIC X(8).
             07 (SF)-TOT-IMPB-ACC PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IMPB-ACC-NULL REDEFINES
                (SF)-TOT-IMPB-ACC   PIC X(8).
             07 (SF)-TOT-IMPB-VIS PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IMPB-VIS-NULL REDEFINES
                (SF)-TOT-IMPB-VIS   PIC X(8).
             07 (SF)-TOT-IMP-RIMB PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IMP-RIMB-NULL REDEFINES
                (SF)-TOT-IMP-RIMB   PIC X(8).
             07 (SF)-IMPB-IMPST-PRVR PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IMPST-PRVR-NULL REDEFINES
                (SF)-IMPB-IMPST-PRVR   PIC X(8).
             07 (SF)-IMPST-PRVR PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-PRVR-NULL REDEFINES
                (SF)-IMPST-PRVR   PIC X(8).
             07 (SF)-IMPB-IMPST-252 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IMPST-252-NULL REDEFINES
                (SF)-IMPB-IMPST-252   PIC X(8).
             07 (SF)-IMPST-252 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-252-NULL REDEFINES
                (SF)-IMPST-252   PIC X(8).
             07 (SF)-TOT-IMP-IS PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IMP-IS-NULL REDEFINES
                (SF)-TOT-IMP-IS   PIC X(8).
             07 (SF)-IMP-DIR-LIQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-DIR-LIQ-NULL REDEFINES
                (SF)-IMP-DIR-LIQ   PIC X(8).
             07 (SF)-TOT-IMP-NET-LIQTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IMP-NET-LIQTO-NULL REDEFINES
                (SF)-TOT-IMP-NET-LIQTO   PIC X(8).
             07 (SF)-MONT-END2000 PIC S9(12)V9(3) COMP-3.
             07 (SF)-MONT-END2000-NULL REDEFINES
                (SF)-MONT-END2000   PIC X(8).
             07 (SF)-MONT-END2006 PIC S9(12)V9(3) COMP-3.
             07 (SF)-MONT-END2006-NULL REDEFINES
                (SF)-MONT-END2006   PIC X(8).
             07 (SF)-PC-REN PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-REN-NULL REDEFINES
                (SF)-PC-REN   PIC X(4).
             07 (SF)-IMP-PNL PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-PNL-NULL REDEFINES
                (SF)-IMP-PNL   PIC X(8).
             07 (SF)-IMPB-IRPEF PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IRPEF-NULL REDEFINES
                (SF)-IMPB-IRPEF   PIC X(8).
             07 (SF)-IMPST-IRPEF PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-IRPEF-NULL REDEFINES
                (SF)-IMPST-IRPEF   PIC X(8).
             07 (SF)-DT-VLT   PIC S9(8) COMP-3.
             07 (SF)-DT-VLT-NULL REDEFINES
                (SF)-DT-VLT   PIC X(5).
             07 (SF)-DT-END-ISTR   PIC S9(8) COMP-3.
             07 (SF)-DT-END-ISTR-NULL REDEFINES
                (SF)-DT-END-ISTR   PIC X(5).
             07 (SF)-TP-RIMB PIC X(2).
             07 (SF)-SPE-RCS PIC S9(12)V9(3) COMP-3.
             07 (SF)-SPE-RCS-NULL REDEFINES
                (SF)-SPE-RCS   PIC X(8).
             07 (SF)-IB-LIQ PIC X(40).
             07 (SF)-IB-LIQ-NULL REDEFINES
                (SF)-IB-LIQ   PIC X(40).
             07 (SF)-TOT-IAS-ONER-PRVNT PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IAS-ONER-PRVNT-NULL REDEFINES
                (SF)-TOT-IAS-ONER-PRVNT   PIC X(8).
             07 (SF)-TOT-IAS-MGG-SIN PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IAS-MGG-SIN-NULL REDEFINES
                (SF)-TOT-IAS-MGG-SIN   PIC X(8).
             07 (SF)-TOT-IAS-RST-DPST PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IAS-RST-DPST-NULL REDEFINES
                (SF)-TOT-IAS-RST-DPST   PIC X(8).
             07 (SF)-IMP-ONER-LIQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-ONER-LIQ-NULL REDEFINES
                (SF)-IMP-ONER-LIQ   PIC X(8).
             07 (SF)-COMPON-TAX-RIMB PIC S9(12)V9(3) COMP-3.
             07 (SF)-COMPON-TAX-RIMB-NULL REDEFINES
                (SF)-COMPON-TAX-RIMB   PIC X(8).
             07 (SF)-TP-MEZ-PAG PIC X(2).
             07 (SF)-TP-MEZ-PAG-NULL REDEFINES
                (SF)-TP-MEZ-PAG   PIC X(2).
             07 (SF)-IMP-EXCONTR PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-EXCONTR-NULL REDEFINES
                (SF)-IMP-EXCONTR   PIC X(8).
             07 (SF)-IMP-INTR-RIT-PAG PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-INTR-RIT-PAG-NULL REDEFINES
                (SF)-IMP-INTR-RIT-PAG   PIC X(8).
             07 (SF)-BNS-NON-GODUTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-BNS-NON-GODUTO-NULL REDEFINES
                (SF)-BNS-NON-GODUTO   PIC X(8).
             07 (SF)-CNBT-INPSTFM PIC S9(12)V9(3) COMP-3.
             07 (SF)-CNBT-INPSTFM-NULL REDEFINES
                (SF)-CNBT-INPSTFM   PIC X(8).
             07 (SF)-IMPST-DA-RIMB PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-DA-RIMB-NULL REDEFINES
                (SF)-IMPST-DA-RIMB   PIC X(8).
             07 (SF)-IMPB-IS PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-NULL REDEFINES
                (SF)-IMPB-IS   PIC X(8).
             07 (SF)-TAX-SEP PIC S9(12)V9(3) COMP-3.
             07 (SF)-TAX-SEP-NULL REDEFINES
                (SF)-TAX-SEP   PIC X(8).
             07 (SF)-IMPB-TAX-SEP PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-TAX-SEP-NULL REDEFINES
                (SF)-IMPB-TAX-SEP   PIC X(8).
             07 (SF)-IMPB-INTR-SU-PREST PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-INTR-SU-PREST-NULL REDEFINES
                (SF)-IMPB-INTR-SU-PREST   PIC X(8).
             07 (SF)-ADDIZ-COMUN PIC S9(12)V9(3) COMP-3.
             07 (SF)-ADDIZ-COMUN-NULL REDEFINES
                (SF)-ADDIZ-COMUN   PIC X(8).
             07 (SF)-IMPB-ADDIZ-COMUN PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-ADDIZ-COMUN-NULL REDEFINES
                (SF)-IMPB-ADDIZ-COMUN   PIC X(8).
             07 (SF)-ADDIZ-REGION PIC S9(12)V9(3) COMP-3.
             07 (SF)-ADDIZ-REGION-NULL REDEFINES
                (SF)-ADDIZ-REGION   PIC X(8).
             07 (SF)-IMPB-ADDIZ-REGION PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-ADDIZ-REGION-NULL REDEFINES
                (SF)-IMPB-ADDIZ-REGION   PIC X(8).
             07 (SF)-MONT-DAL2007 PIC S9(12)V9(3) COMP-3.
             07 (SF)-MONT-DAL2007-NULL REDEFINES
                (SF)-MONT-DAL2007   PIC X(8).
             07 (SF)-IMPB-CNBT-INPSTFM PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-CNBT-INPSTFM-NULL REDEFINES
                (SF)-IMPB-CNBT-INPSTFM   PIC X(8).
             07 (SF)-IMP-LRD-DA-RIMB PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-LRD-DA-RIMB-NULL REDEFINES
                (SF)-IMP-LRD-DA-RIMB   PIC X(8).
             07 (SF)-IMP-DIR-DA-RIMB PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-DIR-DA-RIMB-NULL REDEFINES
                (SF)-IMP-DIR-DA-RIMB   PIC X(8).
             07 (SF)-RIS-MAT PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-MAT-NULL REDEFINES
                (SF)-RIS-MAT   PIC X(8).
             07 (SF)-RIS-SPE PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-SPE-NULL REDEFINES
                (SF)-RIS-SPE   PIC X(8).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-TOT-IAS-PNL PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IAS-PNL-NULL REDEFINES
                (SF)-TOT-IAS-PNL   PIC X(8).
             07 (SF)-FL-EVE-GARTO PIC X(1).
             07 (SF)-FL-EVE-GARTO-NULL REDEFINES
                (SF)-FL-EVE-GARTO   PIC X(1).
             07 (SF)-IMP-REN-K1 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-REN-K1-NULL REDEFINES
                (SF)-IMP-REN-K1   PIC X(8).
             07 (SF)-IMP-REN-K2 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-REN-K2-NULL REDEFINES
                (SF)-IMP-REN-K2   PIC X(8).
             07 (SF)-IMP-REN-K3 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-REN-K3-NULL REDEFINES
                (SF)-IMP-REN-K3   PIC X(8).
             07 (SF)-PC-REN-K1 PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-REN-K1-NULL REDEFINES
                (SF)-PC-REN-K1   PIC X(4).
             07 (SF)-PC-REN-K2 PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-REN-K2-NULL REDEFINES
                (SF)-PC-REN-K2   PIC X(4).
             07 (SF)-PC-REN-K3 PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-REN-K3-NULL REDEFINES
                (SF)-PC-REN-K3   PIC X(4).
             07 (SF)-TP-CAUS-ANTIC PIC X(2).
             07 (SF)-TP-CAUS-ANTIC-NULL REDEFINES
                (SF)-TP-CAUS-ANTIC   PIC X(2).
             07 (SF)-IMP-LRD-LIQTO-RILT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-LRD-LIQTO-RILT-NULL REDEFINES
                (SF)-IMP-LRD-LIQTO-RILT   PIC X(8).
             07 (SF)-IMPST-APPL-RILT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-APPL-RILT-NULL REDEFINES
                (SF)-IMPST-APPL-RILT   PIC X(8).
             07 (SF)-PC-RISC-PARZ PIC S9(7)V9(5) COMP-3.
             07 (SF)-PC-RISC-PARZ-NULL REDEFINES
                (SF)-PC-RISC-PARZ   PIC X(7).
             07 (SF)-IMPST-BOLLO-TOT-V PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-BOLLO-TOT-V-NULL REDEFINES
                (SF)-IMPST-BOLLO-TOT-V   PIC X(8).
             07 (SF)-IMPST-BOLLO-DETT-C PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-BOLLO-DETT-C-NULL REDEFINES
                (SF)-IMPST-BOLLO-DETT-C   PIC X(8).
             07 (SF)-IMPST-BOLLO-TOT-SW PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-BOLLO-TOT-SW-NULL REDEFINES
                (SF)-IMPST-BOLLO-TOT-SW   PIC X(8).
             07 (SF)-IMPST-BOLLO-TOT-AA PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-BOLLO-TOT-AA-NULL REDEFINES
                (SF)-IMPST-BOLLO-TOT-AA   PIC X(8).
             07 (SF)-IMPB-VIS-1382011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-1382011-NULL REDEFINES
                (SF)-IMPB-VIS-1382011   PIC X(8).
             07 (SF)-IMPST-VIS-1382011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-1382011-NULL REDEFINES
                (SF)-IMPST-VIS-1382011   PIC X(8).
             07 (SF)-IMPB-IS-1382011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-1382011-NULL REDEFINES
                (SF)-IMPB-IS-1382011   PIC X(8).
             07 (SF)-IMPST-SOST-1382011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-SOST-1382011-NULL REDEFINES
                (SF)-IMPST-SOST-1382011   PIC X(8).
             07 (SF)-PC-ABB-TIT-STAT PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-ABB-TIT-STAT-NULL REDEFINES
                (SF)-PC-ABB-TIT-STAT   PIC X(4).
             07 (SF)-IMPB-BOLLO-DETT-C PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-BOLLO-DETT-C-NULL REDEFINES
                (SF)-IMPB-BOLLO-DETT-C   PIC X(8).
             07 (SF)-FL-PRE-COMP PIC X(1).
             07 (SF)-FL-PRE-COMP-NULL REDEFINES
                (SF)-FL-PRE-COMP   PIC X(1).
             07 (SF)-IMPB-VIS-662014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-662014-NULL REDEFINES
                (SF)-IMPB-VIS-662014   PIC X(8).
             07 (SF)-IMPST-VIS-662014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-662014-NULL REDEFINES
                (SF)-IMPST-VIS-662014   PIC X(8).
             07 (SF)-IMPB-IS-662014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-662014-NULL REDEFINES
                (SF)-IMPB-IS-662014   PIC X(8).
             07 (SF)-IMPST-SOST-662014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-SOST-662014-NULL REDEFINES
                (SF)-IMPST-SOST-662014   PIC X(8).
             07 (SF)-PC-ABB-TS-662014 PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-ABB-TS-662014-NULL REDEFINES
                (SF)-PC-ABB-TS-662014   PIC X(4).
             07 (SF)-IMP-LRD-CALC-CP PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-LRD-CALC-CP-NULL REDEFINES
                (SF)-IMP-LRD-CALC-CP   PIC X(8).
             07 (SF)-COS-TUNNEL-USCITA PIC S9(12)V9(3) COMP-3.
             07 (SF)-COS-TUNNEL-USCITA-NULL REDEFINES
                (SF)-COS-TUNNEL-USCITA   PIC X(8).

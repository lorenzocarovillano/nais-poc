           EXEC SQL DECLARE NUM_OGG TABLE
           (
             COD_COMPAGNIA_ANIA  DECIMAL(5, 0) NOT NULL,
             FORMA_ASSICURATIVA  CHAR(2) NOT NULL,
             COD_OGGETTO         CHAR(30) NOT NULL,
             TIPO_OGGETTO        CHAR(2) NOT NULL,
             ULT_PROGR           DECIMAL(18, 0),
             DESC_OGGETTO        VARCHAR(250) NOT NULL
          ) END-EXEC.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVAC54
      *   ULTIMO AGG. 27 OTT 2010
      *------------------------------------------------------------

       INIZIA-TOT-AC5.

           PERFORM INIZIA-ZEROES-AC5 THRU INIZIA-ZEROES-AC5-EX

           PERFORM INIZIA-SPACES-AC5 THRU INIZIA-SPACES-AC5-EX

           PERFORM INIZIA-NULL-AC5 THRU INIZIA-NULL-AC5-EX.

       INIZIA-TOT-AC5-EX.
           EXIT.

       INIZIA-NULL-AC5.
           MOVE HIGH-VALUES TO (SF)-ISPRECALC-NULL
           MOVE HIGH-VALUES TO (SF)-NOMEPROGR
           MOVE HIGH-VALUES TO (SF)-MODCALC
           MOVE HIGH-VALUES TO (SF)-GLOVARLIST.
       INIZIA-NULL-AC5-EX.
           EXIT.

       INIZIA-ZEROES-AC5.
           MOVE 0 TO (SF)-IDCOMP
           MOVE 0 TO (SF)-DINIZ
           MOVE 0 TO (SF)-DEND.
       INIZIA-ZEROES-AC5-EX.
           EXIT.

       INIZIA-SPACES-AC5.
           MOVE SPACES TO (SF)-CODPROD
           MOVE SPACES TO (SF)-CODTARI
           MOVE SPACES TO (SF)-NOMEFUNZ
           MOVE SPACES TO (SF)-STEP-ELAB.
       INIZIA-SPACES-AC5-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVTIT5
      *   ULTIMO AGG. 05 DIC 2014
      *------------------------------------------------------------

       VAL-DCLGEN-TIT.
           MOVE (SF)-TP-OGG(IX-TAB-TIT)
              TO TIT-TP-OGG
           IF (SF)-IB-RICH-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-IB-RICH-NULL(IX-TAB-TIT)
              TO TIT-IB-RICH-NULL
           ELSE
              MOVE (SF)-IB-RICH(IX-TAB-TIT)
              TO TIT-IB-RICH
           END-IF
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TIT)
              TO TIT-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-TIT)
              TO TIT-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-TIT) NOT NUMERIC
              MOVE 0 TO TIT-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-TIT)
              TO TIT-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-TIT) NOT NUMERIC
              MOVE 0 TO TIT-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-TIT)
              TO TIT-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-TIT)
              TO TIT-COD-COMP-ANIA
           MOVE (SF)-TP-TIT(IX-TAB-TIT)
              TO TIT-TP-TIT
           IF (SF)-PROG-TIT-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-PROG-TIT-NULL(IX-TAB-TIT)
              TO TIT-PROG-TIT-NULL
           ELSE
              MOVE (SF)-PROG-TIT(IX-TAB-TIT)
              TO TIT-PROG-TIT
           END-IF
           MOVE (SF)-TP-PRE-TIT(IX-TAB-TIT)
              TO TIT-TP-PRE-TIT
           MOVE (SF)-TP-STAT-TIT(IX-TAB-TIT)
              TO TIT-TP-STAT-TIT
           IF (SF)-DT-INI-COP-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-DT-INI-COP-NULL(IX-TAB-TIT)
              TO TIT-DT-INI-COP-NULL
           ELSE
             IF (SF)-DT-INI-COP(IX-TAB-TIT) = ZERO
                MOVE HIGH-VALUES
                TO TIT-DT-INI-COP-NULL
             ELSE
              MOVE (SF)-DT-INI-COP(IX-TAB-TIT)
              TO TIT-DT-INI-COP
             END-IF
           END-IF
           IF (SF)-DT-END-COP-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-DT-END-COP-NULL(IX-TAB-TIT)
              TO TIT-DT-END-COP-NULL
           ELSE
             IF (SF)-DT-END-COP(IX-TAB-TIT) = ZERO
                MOVE HIGH-VALUES
                TO TIT-DT-END-COP-NULL
             ELSE
              MOVE (SF)-DT-END-COP(IX-TAB-TIT)
              TO TIT-DT-END-COP
             END-IF
           END-IF
           IF (SF)-IMP-PAG-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-IMP-PAG-NULL(IX-TAB-TIT)
              TO TIT-IMP-PAG-NULL
           ELSE
              MOVE (SF)-IMP-PAG(IX-TAB-TIT)
              TO TIT-IMP-PAG
           END-IF
           IF (SF)-FL-SOLL-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-FL-SOLL-NULL(IX-TAB-TIT)
              TO TIT-FL-SOLL-NULL
           ELSE
              MOVE (SF)-FL-SOLL(IX-TAB-TIT)
              TO TIT-FL-SOLL
           END-IF
           IF (SF)-FRAZ-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-FRAZ-NULL(IX-TAB-TIT)
              TO TIT-FRAZ-NULL
           ELSE
              MOVE (SF)-FRAZ(IX-TAB-TIT)
              TO TIT-FRAZ
           END-IF
           IF (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TIT)
              TO TIT-DT-APPLZ-MORA-NULL
           ELSE
             IF (SF)-DT-APPLZ-MORA(IX-TAB-TIT) = ZERO
                MOVE HIGH-VALUES
                TO TIT-DT-APPLZ-MORA-NULL
             ELSE
              MOVE (SF)-DT-APPLZ-MORA(IX-TAB-TIT)
              TO TIT-DT-APPLZ-MORA
             END-IF
           END-IF
           IF (SF)-FL-MORA-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-FL-MORA-NULL(IX-TAB-TIT)
              TO TIT-FL-MORA-NULL
           ELSE
              MOVE (SF)-FL-MORA(IX-TAB-TIT)
              TO TIT-FL-MORA
           END-IF
           IF (SF)-ID-RAPP-RETE-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-ID-RAPP-RETE-NULL(IX-TAB-TIT)
              TO TIT-ID-RAPP-RETE-NULL
           ELSE
              MOVE (SF)-ID-RAPP-RETE(IX-TAB-TIT)
              TO TIT-ID-RAPP-RETE
           END-IF
           IF (SF)-ID-RAPP-ANA-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-ID-RAPP-ANA-NULL(IX-TAB-TIT)
              TO TIT-ID-RAPP-ANA-NULL
           ELSE
              MOVE (SF)-ID-RAPP-ANA(IX-TAB-TIT)
              TO TIT-ID-RAPP-ANA
           END-IF
           IF (SF)-COD-DVS-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-COD-DVS-NULL(IX-TAB-TIT)
              TO TIT-COD-DVS-NULL
           ELSE
              MOVE (SF)-COD-DVS(IX-TAB-TIT)
              TO TIT-COD-DVS
           END-IF
           IF (SF)-DT-EMIS-TIT-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-DT-EMIS-TIT-NULL(IX-TAB-TIT)
              TO TIT-DT-EMIS-TIT-NULL
           ELSE
             IF (SF)-DT-EMIS-TIT(IX-TAB-TIT) = ZERO
                MOVE HIGH-VALUES
                TO TIT-DT-EMIS-TIT-NULL
             ELSE
              MOVE (SF)-DT-EMIS-TIT(IX-TAB-TIT)
              TO TIT-DT-EMIS-TIT
             END-IF
           END-IF
           IF (SF)-DT-ESI-TIT-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-DT-ESI-TIT-NULL(IX-TAB-TIT)
              TO TIT-DT-ESI-TIT-NULL
           ELSE
             IF (SF)-DT-ESI-TIT(IX-TAB-TIT) = ZERO
                MOVE HIGH-VALUES
                TO TIT-DT-ESI-TIT-NULL
             ELSE
              MOVE (SF)-DT-ESI-TIT(IX-TAB-TIT)
              TO TIT-DT-ESI-TIT
             END-IF
           END-IF
           IF (SF)-TOT-PRE-NET-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-PRE-NET-NULL(IX-TAB-TIT)
              TO TIT-TOT-PRE-NET-NULL
           ELSE
              MOVE (SF)-TOT-PRE-NET(IX-TAB-TIT)
              TO TIT-TOT-PRE-NET
           END-IF
           IF (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TIT)
              TO TIT-TOT-INTR-FRAZ-NULL
           ELSE
              MOVE (SF)-TOT-INTR-FRAZ(IX-TAB-TIT)
              TO TIT-TOT-INTR-FRAZ
           END-IF
           IF (SF)-TOT-INTR-MORA-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-INTR-MORA-NULL(IX-TAB-TIT)
              TO TIT-TOT-INTR-MORA-NULL
           ELSE
              MOVE (SF)-TOT-INTR-MORA(IX-TAB-TIT)
              TO TIT-TOT-INTR-MORA
           END-IF
           IF (SF)-TOT-INTR-PREST-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-INTR-PREST-NULL(IX-TAB-TIT)
              TO TIT-TOT-INTR-PREST-NULL
           ELSE
              MOVE (SF)-TOT-INTR-PREST(IX-TAB-TIT)
              TO TIT-TOT-INTR-PREST
           END-IF
           IF (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TIT)
              TO TIT-TOT-INTR-RETDT-NULL
           ELSE
              MOVE (SF)-TOT-INTR-RETDT(IX-TAB-TIT)
              TO TIT-TOT-INTR-RETDT
           END-IF
           IF (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TIT)
              TO TIT-TOT-INTR-RIAT-NULL
           ELSE
              MOVE (SF)-TOT-INTR-RIAT(IX-TAB-TIT)
              TO TIT-TOT-INTR-RIAT
           END-IF
           IF (SF)-TOT-DIR-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-DIR-NULL(IX-TAB-TIT)
              TO TIT-TOT-DIR-NULL
           ELSE
              MOVE (SF)-TOT-DIR(IX-TAB-TIT)
              TO TIT-TOT-DIR
           END-IF
           IF (SF)-TOT-SPE-MED-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-SPE-MED-NULL(IX-TAB-TIT)
              TO TIT-TOT-SPE-MED-NULL
           ELSE
              MOVE (SF)-TOT-SPE-MED(IX-TAB-TIT)
              TO TIT-TOT-SPE-MED
           END-IF
           IF (SF)-TOT-TAX-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-TAX-NULL(IX-TAB-TIT)
              TO TIT-TOT-TAX-NULL
           ELSE
              MOVE (SF)-TOT-TAX(IX-TAB-TIT)
              TO TIT-TOT-TAX
           END-IF
           IF (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TIT)
              TO TIT-TOT-SOPR-SAN-NULL
           ELSE
              MOVE (SF)-TOT-SOPR-SAN(IX-TAB-TIT)
              TO TIT-TOT-SOPR-SAN
           END-IF
           IF (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TIT)
              TO TIT-TOT-SOPR-TEC-NULL
           ELSE
              MOVE (SF)-TOT-SOPR-TEC(IX-TAB-TIT)
              TO TIT-TOT-SOPR-TEC
           END-IF
           IF (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TIT)
              TO TIT-TOT-SOPR-SPO-NULL
           ELSE
              MOVE (SF)-TOT-SOPR-SPO(IX-TAB-TIT)
              TO TIT-TOT-SOPR-SPO
           END-IF
           IF (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TIT)
              TO TIT-TOT-SOPR-PROF-NULL
           ELSE
              MOVE (SF)-TOT-SOPR-PROF(IX-TAB-TIT)
              TO TIT-TOT-SOPR-PROF
           END-IF
           IF (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TIT)
              TO TIT-TOT-SOPR-ALT-NULL
           ELSE
              MOVE (SF)-TOT-SOPR-ALT(IX-TAB-TIT)
              TO TIT-TOT-SOPR-ALT
           END-IF
           IF (SF)-TOT-PRE-TOT-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-PRE-TOT-NULL(IX-TAB-TIT)
              TO TIT-TOT-PRE-TOT-NULL
           ELSE
              MOVE (SF)-TOT-PRE-TOT(IX-TAB-TIT)
              TO TIT-TOT-PRE-TOT
           END-IF
           IF (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TIT)
              TO TIT-TOT-PRE-PP-IAS-NULL
           ELSE
              MOVE (SF)-TOT-PRE-PP-IAS(IX-TAB-TIT)
              TO TIT-TOT-PRE-PP-IAS
           END-IF
           IF (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TIT)
              TO TIT-TOT-CAR-ACQ-NULL
           ELSE
              MOVE (SF)-TOT-CAR-ACQ(IX-TAB-TIT)
              TO TIT-TOT-CAR-ACQ
           END-IF
           IF (SF)-TOT-CAR-GEST-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-CAR-GEST-NULL(IX-TAB-TIT)
              TO TIT-TOT-CAR-GEST-NULL
           ELSE
              MOVE (SF)-TOT-CAR-GEST(IX-TAB-TIT)
              TO TIT-TOT-CAR-GEST
           END-IF
           IF (SF)-TOT-CAR-INC-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-CAR-INC-NULL(IX-TAB-TIT)
              TO TIT-TOT-CAR-INC-NULL
           ELSE
              MOVE (SF)-TOT-CAR-INC(IX-TAB-TIT)
              TO TIT-TOT-CAR-INC
           END-IF
           IF (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TIT)
              TO TIT-TOT-PRE-SOLO-RSH-NULL
           ELSE
              MOVE (SF)-TOT-PRE-SOLO-RSH(IX-TAB-TIT)
              TO TIT-TOT-PRE-SOLO-RSH
           END-IF
           IF (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TIT)
              TO TIT-TOT-PROV-ACQ-1AA-NULL
           ELSE
              MOVE (SF)-TOT-PROV-ACQ-1AA(IX-TAB-TIT)
              TO TIT-TOT-PROV-ACQ-1AA
           END-IF
           IF (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TIT)
              TO TIT-TOT-PROV-ACQ-2AA-NULL
           ELSE
              MOVE (SF)-TOT-PROV-ACQ-2AA(IX-TAB-TIT)
              TO TIT-TOT-PROV-ACQ-2AA
           END-IF
           IF (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TIT)
              TO TIT-TOT-PROV-RICOR-NULL
           ELSE
              MOVE (SF)-TOT-PROV-RICOR(IX-TAB-TIT)
              TO TIT-TOT-PROV-RICOR
           END-IF
           IF (SF)-TOT-PROV-INC-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-PROV-INC-NULL(IX-TAB-TIT)
              TO TIT-TOT-PROV-INC-NULL
           ELSE
              MOVE (SF)-TOT-PROV-INC(IX-TAB-TIT)
              TO TIT-TOT-PROV-INC
           END-IF
           IF (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TIT)
              TO TIT-TOT-PROV-DA-REC-NULL
           ELSE
              MOVE (SF)-TOT-PROV-DA-REC(IX-TAB-TIT)
              TO TIT-TOT-PROV-DA-REC
           END-IF
           IF (SF)-IMP-AZ-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-IMP-AZ-NULL(IX-TAB-TIT)
              TO TIT-IMP-AZ-NULL
           ELSE
              MOVE (SF)-IMP-AZ(IX-TAB-TIT)
              TO TIT-IMP-AZ
           END-IF
           IF (SF)-IMP-ADER-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-IMP-ADER-NULL(IX-TAB-TIT)
              TO TIT-IMP-ADER-NULL
           ELSE
              MOVE (SF)-IMP-ADER(IX-TAB-TIT)
              TO TIT-IMP-ADER
           END-IF
           IF (SF)-IMP-TFR-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-IMP-TFR-NULL(IX-TAB-TIT)
              TO TIT-IMP-TFR-NULL
           ELSE
              MOVE (SF)-IMP-TFR(IX-TAB-TIT)
              TO TIT-IMP-TFR
           END-IF
           IF (SF)-IMP-VOLO-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-IMP-VOLO-NULL(IX-TAB-TIT)
              TO TIT-IMP-VOLO-NULL
           ELSE
              MOVE (SF)-IMP-VOLO(IX-TAB-TIT)
              TO TIT-IMP-VOLO
           END-IF
           IF (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TIT)
              TO TIT-TOT-MANFEE-ANTIC-NULL
           ELSE
              MOVE (SF)-TOT-MANFEE-ANTIC(IX-TAB-TIT)
              TO TIT-TOT-MANFEE-ANTIC
           END-IF
           IF (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TIT)
              TO TIT-TOT-MANFEE-RICOR-NULL
           ELSE
              MOVE (SF)-TOT-MANFEE-RICOR(IX-TAB-TIT)
              TO TIT-TOT-MANFEE-RICOR
           END-IF
           IF (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TIT)
              TO TIT-TOT-MANFEE-REC-NULL
           ELSE
              MOVE (SF)-TOT-MANFEE-REC(IX-TAB-TIT)
              TO TIT-TOT-MANFEE-REC
           END-IF
           IF (SF)-TP-MEZ-PAG-ADD-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TP-MEZ-PAG-ADD-NULL(IX-TAB-TIT)
              TO TIT-TP-MEZ-PAG-ADD-NULL
           ELSE
              MOVE (SF)-TP-MEZ-PAG-ADD(IX-TAB-TIT)
              TO TIT-TP-MEZ-PAG-ADD
           END-IF
           IF (SF)-ESTR-CNT-CORR-ADD-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-ESTR-CNT-CORR-ADD-NULL(IX-TAB-TIT)
              TO TIT-ESTR-CNT-CORR-ADD-NULL
           ELSE
              MOVE (SF)-ESTR-CNT-CORR-ADD(IX-TAB-TIT)
              TO TIT-ESTR-CNT-CORR-ADD
           END-IF
           IF (SF)-DT-VLT-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-DT-VLT-NULL(IX-TAB-TIT)
              TO TIT-DT-VLT-NULL
           ELSE
             IF (SF)-DT-VLT(IX-TAB-TIT) = ZERO
                MOVE HIGH-VALUES
                TO TIT-DT-VLT-NULL
             ELSE
              MOVE (SF)-DT-VLT(IX-TAB-TIT)
              TO TIT-DT-VLT
             END-IF
           END-IF
           IF (SF)-FL-FORZ-DT-VLT-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-FL-FORZ-DT-VLT-NULL(IX-TAB-TIT)
              TO TIT-FL-FORZ-DT-VLT-NULL
           ELSE
              MOVE (SF)-FL-FORZ-DT-VLT(IX-TAB-TIT)
              TO TIT-FL-FORZ-DT-VLT
           END-IF
           IF (SF)-DT-CAMBIO-VLT-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-DT-CAMBIO-VLT-NULL(IX-TAB-TIT)
              TO TIT-DT-CAMBIO-VLT-NULL
           ELSE
             IF (SF)-DT-CAMBIO-VLT(IX-TAB-TIT) = ZERO
                MOVE HIGH-VALUES
                TO TIT-DT-CAMBIO-VLT-NULL
             ELSE
              MOVE (SF)-DT-CAMBIO-VLT(IX-TAB-TIT)
              TO TIT-DT-CAMBIO-VLT
             END-IF
           END-IF
           IF (SF)-TOT-SPE-AGE-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-SPE-AGE-NULL(IX-TAB-TIT)
              TO TIT-TOT-SPE-AGE-NULL
           ELSE
              MOVE (SF)-TOT-SPE-AGE(IX-TAB-TIT)
              TO TIT-TOT-SPE-AGE
           END-IF
           IF (SF)-TOT-CAR-IAS-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-CAR-IAS-NULL(IX-TAB-TIT)
              TO TIT-TOT-CAR-IAS-NULL
           ELSE
              MOVE (SF)-TOT-CAR-IAS(IX-TAB-TIT)
              TO TIT-TOT-CAR-IAS
           END-IF
           IF (SF)-NUM-RAT-ACCORPATE-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-NUM-RAT-ACCORPATE-NULL(IX-TAB-TIT)
              TO TIT-NUM-RAT-ACCORPATE-NULL
           ELSE
              MOVE (SF)-NUM-RAT-ACCORPATE(IX-TAB-TIT)
              TO TIT-NUM-RAT-ACCORPATE
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-TIT) NOT NUMERIC
              MOVE 0 TO TIT-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-TIT)
              TO TIT-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-TIT)
              TO TIT-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-TIT) NOT NUMERIC
              MOVE 0 TO TIT-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-TIT)
              TO TIT-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-TIT) NOT NUMERIC
              MOVE 0 TO TIT-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-TIT)
              TO TIT-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-TIT) NOT NUMERIC
              MOVE 0 TO TIT-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-TIT)
              TO TIT-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-TIT)
              TO TIT-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-TIT)
              TO TIT-DS-STATO-ELAB
           IF (SF)-FL-TIT-DA-REINVST-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-FL-TIT-DA-REINVST-NULL(IX-TAB-TIT)
              TO TIT-FL-TIT-DA-REINVST-NULL
           ELSE
              MOVE (SF)-FL-TIT-DA-REINVST(IX-TAB-TIT)
              TO TIT-FL-TIT-DA-REINVST
           END-IF
           IF (SF)-DT-RICH-ADD-RID-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-DT-RICH-ADD-RID-NULL(IX-TAB-TIT)
              TO TIT-DT-RICH-ADD-RID-NULL
           ELSE
             IF (SF)-DT-RICH-ADD-RID(IX-TAB-TIT) = ZERO
                MOVE HIGH-VALUES
                TO TIT-DT-RICH-ADD-RID-NULL
             ELSE
              MOVE (SF)-DT-RICH-ADD-RID(IX-TAB-TIT)
              TO TIT-DT-RICH-ADD-RID
             END-IF
           END-IF
           IF (SF)-TP-ESI-RID-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TP-ESI-RID-NULL(IX-TAB-TIT)
              TO TIT-TP-ESI-RID-NULL
           ELSE
              MOVE (SF)-TP-ESI-RID(IX-TAB-TIT)
              TO TIT-TP-ESI-RID
           END-IF
           IF (SF)-COD-IBAN-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-COD-IBAN-NULL(IX-TAB-TIT)
              TO TIT-COD-IBAN-NULL
           ELSE
              MOVE (SF)-COD-IBAN(IX-TAB-TIT)
              TO TIT-COD-IBAN
           END-IF
           IF (SF)-IMP-TRASFE-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-IMP-TRASFE-NULL(IX-TAB-TIT)
              TO TIT-IMP-TRASFE-NULL
           ELSE
              MOVE (SF)-IMP-TRASFE(IX-TAB-TIT)
              TO TIT-IMP-TRASFE
           END-IF
           IF (SF)-IMP-TFR-STRC-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-IMP-TFR-STRC-NULL(IX-TAB-TIT)
              TO TIT-IMP-TFR-STRC-NULL
           ELSE
              MOVE (SF)-IMP-TFR-STRC(IX-TAB-TIT)
              TO TIT-IMP-TFR-STRC
           END-IF
           IF (SF)-DT-CERT-FISC-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-DT-CERT-FISC-NULL(IX-TAB-TIT)
              TO TIT-DT-CERT-FISC-NULL
           ELSE
             IF (SF)-DT-CERT-FISC(IX-TAB-TIT) = ZERO
                MOVE HIGH-VALUES
                TO TIT-DT-CERT-FISC-NULL
             ELSE
              MOVE (SF)-DT-CERT-FISC(IX-TAB-TIT)
              TO TIT-DT-CERT-FISC
             END-IF
           END-IF
           IF (SF)-TP-CAUS-STOR-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TP-CAUS-STOR-NULL(IX-TAB-TIT)
              TO TIT-TP-CAUS-STOR-NULL
           ELSE
              MOVE (SF)-TP-CAUS-STOR(IX-TAB-TIT)
              TO TIT-TP-CAUS-STOR
           END-IF
           IF (SF)-TP-CAUS-DISP-STOR-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TP-CAUS-DISP-STOR-NULL(IX-TAB-TIT)
              TO TIT-TP-CAUS-DISP-STOR-NULL
           ELSE
              MOVE (SF)-TP-CAUS-DISP-STOR(IX-TAB-TIT)
              TO TIT-TP-CAUS-DISP-STOR
           END-IF
           IF (SF)-TP-TIT-MIGRAZ-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TP-TIT-MIGRAZ-NULL(IX-TAB-TIT)
              TO TIT-TP-TIT-MIGRAZ-NULL
           ELSE
              MOVE (SF)-TP-TIT-MIGRAZ(IX-TAB-TIT)
              TO TIT-TP-TIT-MIGRAZ
           END-IF
           IF (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TIT)
              TO TIT-TOT-ACQ-EXP-NULL
           ELSE
              MOVE (SF)-TOT-ACQ-EXP(IX-TAB-TIT)
              TO TIT-TOT-ACQ-EXP
           END-IF
           IF (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TIT)
              TO TIT-TOT-REMUN-ASS-NULL
           ELSE
              MOVE (SF)-TOT-REMUN-ASS(IX-TAB-TIT)
              TO TIT-TOT-REMUN-ASS
           END-IF
           IF (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TIT)
              TO TIT-TOT-COMMIS-INTER-NULL
           ELSE
              MOVE (SF)-TOT-COMMIS-INTER(IX-TAB-TIT)
              TO TIT-TOT-COMMIS-INTER
           END-IF
           IF (SF)-TP-CAUS-RIMB-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TP-CAUS-RIMB-NULL(IX-TAB-TIT)
              TO TIT-TP-CAUS-RIMB-NULL
           ELSE
              MOVE (SF)-TP-CAUS-RIMB(IX-TAB-TIT)
              TO TIT-TP-CAUS-RIMB
           END-IF
           IF (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TIT)
              TO TIT-TOT-CNBT-ANTIRAC-NULL
           ELSE
              MOVE (SF)-TOT-CNBT-ANTIRAC(IX-TAB-TIT)
              TO TIT-TOT-CNBT-ANTIRAC
           END-IF
           IF (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TIT) = HIGH-VALUES
              MOVE (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TIT)
              TO TIT-FL-INC-AUTOGEN-NULL
           ELSE
              MOVE (SF)-FL-INC-AUTOGEN(IX-TAB-TIT)
              TO TIT-FL-INC-AUTOGEN
           END-IF.
       VAL-DCLGEN-TIT-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVRDF3
      *   ULTIMO AGG. 25 NOV 2019
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-RDF.
           MOVE RDF-ID-RICH-DIS-FND
             TO (SF)-ID-PTF(IX-TAB-RDF)
           MOVE RDF-ID-RICH-DIS-FND
             TO (SF)-ID-RICH-DIS-FND(IX-TAB-RDF)
           MOVE RDF-ID-MOVI-FINRIO
             TO (SF)-ID-MOVI-FINRIO(IX-TAB-RDF)
           MOVE RDF-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-RDF)
           IF RDF-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE RDF-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-RDF)
           END-IF
           MOVE RDF-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-RDF)
           MOVE RDF-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-RDF)
           MOVE RDF-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-RDF)
           IF RDF-COD-FND-NULL = HIGH-VALUES
              MOVE RDF-COD-FND-NULL
                TO (SF)-COD-FND-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-COD-FND
                TO (SF)-COD-FND(IX-TAB-RDF)
           END-IF
           IF RDF-NUM-QUO-NULL = HIGH-VALUES
              MOVE RDF-NUM-QUO-NULL
                TO (SF)-NUM-QUO-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-NUM-QUO
                TO (SF)-NUM-QUO(IX-TAB-RDF)
           END-IF
           IF RDF-PC-NULL = HIGH-VALUES
              MOVE RDF-PC-NULL
                TO (SF)-PC-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-PC
                TO (SF)-PC(IX-TAB-RDF)
           END-IF
           IF RDF-IMP-MOVTO-NULL = HIGH-VALUES
              MOVE RDF-IMP-MOVTO-NULL
                TO (SF)-IMP-MOVTO-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-IMP-MOVTO
                TO (SF)-IMP-MOVTO(IX-TAB-RDF)
           END-IF
           IF RDF-DT-DIS-NULL = HIGH-VALUES
              MOVE RDF-DT-DIS-NULL
                TO (SF)-DT-DIS-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-DT-DIS
                TO (SF)-DT-DIS(IX-TAB-RDF)
           END-IF
           IF RDF-COD-TARI-NULL = HIGH-VALUES
              MOVE RDF-COD-TARI-NULL
                TO (SF)-COD-TARI-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-COD-TARI
                TO (SF)-COD-TARI(IX-TAB-RDF)
           END-IF
           IF RDF-TP-STAT-NULL = HIGH-VALUES
              MOVE RDF-TP-STAT-NULL
                TO (SF)-TP-STAT-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-TP-STAT
                TO (SF)-TP-STAT(IX-TAB-RDF)
           END-IF
           IF RDF-TP-MOD-DIS-NULL = HIGH-VALUES
              MOVE RDF-TP-MOD-DIS-NULL
                TO (SF)-TP-MOD-DIS-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-TP-MOD-DIS
                TO (SF)-TP-MOD-DIS(IX-TAB-RDF)
           END-IF
           MOVE RDF-COD-DIV
             TO (SF)-COD-DIV(IX-TAB-RDF)
           IF RDF-DT-CAMBIO-VLT-NULL = HIGH-VALUES
              MOVE RDF-DT-CAMBIO-VLT-NULL
                TO (SF)-DT-CAMBIO-VLT-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-DT-CAMBIO-VLT
                TO (SF)-DT-CAMBIO-VLT(IX-TAB-RDF)
           END-IF
           MOVE RDF-TP-FND
             TO (SF)-TP-FND(IX-TAB-RDF)
           MOVE RDF-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-RDF)
           MOVE RDF-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-RDF)
           MOVE RDF-DS-VER
             TO (SF)-DS-VER(IX-TAB-RDF)
           MOVE RDF-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-RDF)
           MOVE RDF-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-RDF)
           MOVE RDF-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-RDF)
           MOVE RDF-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-RDF)
           IF RDF-DT-DIS-CALC-NULL = HIGH-VALUES
              MOVE RDF-DT-DIS-CALC-NULL
                TO (SF)-DT-DIS-CALC-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-DT-DIS-CALC
                TO (SF)-DT-DIS-CALC(IX-TAB-RDF)
           END-IF
           IF RDF-FL-CALC-DIS-NULL = HIGH-VALUES
              MOVE RDF-FL-CALC-DIS-NULL
                TO (SF)-FL-CALC-DIS-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-FL-CALC-DIS
                TO (SF)-FL-CALC-DIS(IX-TAB-RDF)
           END-IF
           IF RDF-COMMIS-GEST-NULL = HIGH-VALUES
              MOVE RDF-COMMIS-GEST-NULL
                TO (SF)-COMMIS-GEST-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-COMMIS-GEST
                TO (SF)-COMMIS-GEST(IX-TAB-RDF)
           END-IF
           IF RDF-NUM-QUO-CDG-FNZ-NULL = HIGH-VALUES
              MOVE RDF-NUM-QUO-CDG-FNZ-NULL
                TO (SF)-NUM-QUO-CDG-FNZ-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-NUM-QUO-CDG-FNZ
                TO (SF)-NUM-QUO-CDG-FNZ(IX-TAB-RDF)
           END-IF
           IF RDF-NUM-QUO-CDGTOT-FNZ-NULL = HIGH-VALUES
              MOVE RDF-NUM-QUO-CDGTOT-FNZ-NULL
                TO (SF)-NUM-QUO-CDGTOT-FNZ-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-NUM-QUO-CDGTOT-FNZ
                TO (SF)-NUM-QUO-CDGTOT-FNZ(IX-TAB-RDF)
           END-IF
           IF RDF-COS-RUN-ASSVA-IDC-NULL = HIGH-VALUES
              MOVE RDF-COS-RUN-ASSVA-IDC-NULL
                TO (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-COS-RUN-ASSVA-IDC
                TO (SF)-COS-RUN-ASSVA-IDC(IX-TAB-RDF)
           END-IF
           IF RDF-FL-SWM-BP2S-NULL = HIGH-VALUES
              MOVE RDF-FL-SWM-BP2S-NULL
                TO (SF)-FL-SWM-BP2S-NULL(IX-TAB-RDF)
           ELSE
              MOVE RDF-FL-SWM-BP2S
                TO (SF)-FL-SWM-BP2S(IX-TAB-RDF)
           END-IF.
       VALORIZZA-OUTPUT-RDF-EX.
           EXIT.

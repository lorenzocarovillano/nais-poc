
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVDTR3
      *   ULTIMO AGG. 28 NOV 2014
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-DTR.
           MOVE DTR-ID-DETT-TIT-DI-RAT
             TO (SF)-ID-PTF(IX-TAB-DTR)
           MOVE DTR-ID-DETT-TIT-DI-RAT
             TO (SF)-ID-DETT-TIT-DI-RAT(IX-TAB-DTR)
           MOVE DTR-ID-TIT-RAT
             TO (SF)-ID-TIT-RAT(IX-TAB-DTR)
           MOVE DTR-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-DTR)
           MOVE DTR-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-DTR)
           MOVE DTR-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-DTR)
           IF DTR-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE DTR-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-DTR)
           END-IF
           MOVE DTR-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-DTR)
           MOVE DTR-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-DTR)
           MOVE DTR-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-DTR)
           IF DTR-DT-INI-COP-NULL = HIGH-VALUES
              MOVE DTR-DT-INI-COP-NULL
                TO (SF)-DT-INI-COP-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-DT-INI-COP
                TO (SF)-DT-INI-COP(IX-TAB-DTR)
           END-IF
           IF DTR-DT-END-COP-NULL = HIGH-VALUES
              MOVE DTR-DT-END-COP-NULL
                TO (SF)-DT-END-COP-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-DT-END-COP
                TO (SF)-DT-END-COP(IX-TAB-DTR)
           END-IF
           IF DTR-PRE-NET-NULL = HIGH-VALUES
              MOVE DTR-PRE-NET-NULL
                TO (SF)-PRE-NET-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-PRE-NET
                TO (SF)-PRE-NET(IX-TAB-DTR)
           END-IF
           IF DTR-INTR-FRAZ-NULL = HIGH-VALUES
              MOVE DTR-INTR-FRAZ-NULL
                TO (SF)-INTR-FRAZ-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-INTR-FRAZ
                TO (SF)-INTR-FRAZ(IX-TAB-DTR)
           END-IF
           IF DTR-INTR-MORA-NULL = HIGH-VALUES
              MOVE DTR-INTR-MORA-NULL
                TO (SF)-INTR-MORA-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-INTR-MORA
                TO (SF)-INTR-MORA(IX-TAB-DTR)
           END-IF
           IF DTR-INTR-RETDT-NULL = HIGH-VALUES
              MOVE DTR-INTR-RETDT-NULL
                TO (SF)-INTR-RETDT-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-INTR-RETDT
                TO (SF)-INTR-RETDT(IX-TAB-DTR)
           END-IF
           IF DTR-INTR-RIAT-NULL = HIGH-VALUES
              MOVE DTR-INTR-RIAT-NULL
                TO (SF)-INTR-RIAT-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-INTR-RIAT
                TO (SF)-INTR-RIAT(IX-TAB-DTR)
           END-IF
           IF DTR-DIR-NULL = HIGH-VALUES
              MOVE DTR-DIR-NULL
                TO (SF)-DIR-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-DIR
                TO (SF)-DIR(IX-TAB-DTR)
           END-IF
           IF DTR-SPE-MED-NULL = HIGH-VALUES
              MOVE DTR-SPE-MED-NULL
                TO (SF)-SPE-MED-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-SPE-MED
                TO (SF)-SPE-MED(IX-TAB-DTR)
           END-IF
           IF DTR-SPE-AGE-NULL = HIGH-VALUES
              MOVE DTR-SPE-AGE-NULL
                TO (SF)-SPE-AGE-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-SPE-AGE
                TO (SF)-SPE-AGE(IX-TAB-DTR)
           END-IF
           IF DTR-TAX-NULL = HIGH-VALUES
              MOVE DTR-TAX-NULL
                TO (SF)-TAX-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-TAX
                TO (SF)-TAX(IX-TAB-DTR)
           END-IF
           IF DTR-SOPR-SAN-NULL = HIGH-VALUES
              MOVE DTR-SOPR-SAN-NULL
                TO (SF)-SOPR-SAN-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-SOPR-SAN
                TO (SF)-SOPR-SAN(IX-TAB-DTR)
           END-IF
           IF DTR-SOPR-SPO-NULL = HIGH-VALUES
              MOVE DTR-SOPR-SPO-NULL
                TO (SF)-SOPR-SPO-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-SOPR-SPO
                TO (SF)-SOPR-SPO(IX-TAB-DTR)
           END-IF
           IF DTR-SOPR-TEC-NULL = HIGH-VALUES
              MOVE DTR-SOPR-TEC-NULL
                TO (SF)-SOPR-TEC-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-SOPR-TEC
                TO (SF)-SOPR-TEC(IX-TAB-DTR)
           END-IF
           IF DTR-SOPR-PROF-NULL = HIGH-VALUES
              MOVE DTR-SOPR-PROF-NULL
                TO (SF)-SOPR-PROF-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-SOPR-PROF
                TO (SF)-SOPR-PROF(IX-TAB-DTR)
           END-IF
           IF DTR-SOPR-ALT-NULL = HIGH-VALUES
              MOVE DTR-SOPR-ALT-NULL
                TO (SF)-SOPR-ALT-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-SOPR-ALT
                TO (SF)-SOPR-ALT(IX-TAB-DTR)
           END-IF
           IF DTR-PRE-TOT-NULL = HIGH-VALUES
              MOVE DTR-PRE-TOT-NULL
                TO (SF)-PRE-TOT-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-PRE-TOT
                TO (SF)-PRE-TOT(IX-TAB-DTR)
           END-IF
           IF DTR-PRE-PP-IAS-NULL = HIGH-VALUES
              MOVE DTR-PRE-PP-IAS-NULL
                TO (SF)-PRE-PP-IAS-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-PRE-PP-IAS
                TO (SF)-PRE-PP-IAS(IX-TAB-DTR)
           END-IF
           IF DTR-PRE-SOLO-RSH-NULL = HIGH-VALUES
              MOVE DTR-PRE-SOLO-RSH-NULL
                TO (SF)-PRE-SOLO-RSH-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-PRE-SOLO-RSH
                TO (SF)-PRE-SOLO-RSH(IX-TAB-DTR)
           END-IF
           IF DTR-CAR-IAS-NULL = HIGH-VALUES
              MOVE DTR-CAR-IAS-NULL
                TO (SF)-CAR-IAS-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-CAR-IAS
                TO (SF)-CAR-IAS(IX-TAB-DTR)
           END-IF
           IF DTR-PROV-ACQ-1AA-NULL = HIGH-VALUES
              MOVE DTR-PROV-ACQ-1AA-NULL
                TO (SF)-PROV-ACQ-1AA-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-PROV-ACQ-1AA
                TO (SF)-PROV-ACQ-1AA(IX-TAB-DTR)
           END-IF
           IF DTR-PROV-ACQ-2AA-NULL = HIGH-VALUES
              MOVE DTR-PROV-ACQ-2AA-NULL
                TO (SF)-PROV-ACQ-2AA-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-PROV-ACQ-2AA
                TO (SF)-PROV-ACQ-2AA(IX-TAB-DTR)
           END-IF
           IF DTR-PROV-RICOR-NULL = HIGH-VALUES
              MOVE DTR-PROV-RICOR-NULL
                TO (SF)-PROV-RICOR-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-PROV-RICOR
                TO (SF)-PROV-RICOR(IX-TAB-DTR)
           END-IF
           IF DTR-PROV-INC-NULL = HIGH-VALUES
              MOVE DTR-PROV-INC-NULL
                TO (SF)-PROV-INC-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-PROV-INC
                TO (SF)-PROV-INC(IX-TAB-DTR)
           END-IF
           IF DTR-PROV-DA-REC-NULL = HIGH-VALUES
              MOVE DTR-PROV-DA-REC-NULL
                TO (SF)-PROV-DA-REC-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-PROV-DA-REC
                TO (SF)-PROV-DA-REC(IX-TAB-DTR)
           END-IF
           IF DTR-COD-DVS-NULL = HIGH-VALUES
              MOVE DTR-COD-DVS-NULL
                TO (SF)-COD-DVS-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-COD-DVS
                TO (SF)-COD-DVS(IX-TAB-DTR)
           END-IF
           IF DTR-FRQ-MOVI-NULL = HIGH-VALUES
              MOVE DTR-FRQ-MOVI-NULL
                TO (SF)-FRQ-MOVI-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-FRQ-MOVI
                TO (SF)-FRQ-MOVI(IX-TAB-DTR)
           END-IF
           MOVE DTR-TP-RGM-FISC
             TO (SF)-TP-RGM-FISC(IX-TAB-DTR)
           IF DTR-COD-TARI-NULL = HIGH-VALUES
              MOVE DTR-COD-TARI-NULL
                TO (SF)-COD-TARI-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-COD-TARI
                TO (SF)-COD-TARI(IX-TAB-DTR)
           END-IF
           MOVE DTR-TP-STAT-TIT
             TO (SF)-TP-STAT-TIT(IX-TAB-DTR)
           IF DTR-IMP-AZ-NULL = HIGH-VALUES
              MOVE DTR-IMP-AZ-NULL
                TO (SF)-IMP-AZ-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-IMP-AZ
                TO (SF)-IMP-AZ(IX-TAB-DTR)
           END-IF
           IF DTR-IMP-ADER-NULL = HIGH-VALUES
              MOVE DTR-IMP-ADER-NULL
                TO (SF)-IMP-ADER-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-IMP-ADER
                TO (SF)-IMP-ADER(IX-TAB-DTR)
           END-IF
           IF DTR-IMP-TFR-NULL = HIGH-VALUES
              MOVE DTR-IMP-TFR-NULL
                TO (SF)-IMP-TFR-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-IMP-TFR
                TO (SF)-IMP-TFR(IX-TAB-DTR)
           END-IF
           IF DTR-IMP-VOLO-NULL = HIGH-VALUES
              MOVE DTR-IMP-VOLO-NULL
                TO (SF)-IMP-VOLO-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-IMP-VOLO
                TO (SF)-IMP-VOLO(IX-TAB-DTR)
           END-IF
           IF DTR-FL-VLDT-TIT-NULL = HIGH-VALUES
              MOVE DTR-FL-VLDT-TIT-NULL
                TO (SF)-FL-VLDT-TIT-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-FL-VLDT-TIT
                TO (SF)-FL-VLDT-TIT(IX-TAB-DTR)
           END-IF
           IF DTR-CAR-ACQ-NULL = HIGH-VALUES
              MOVE DTR-CAR-ACQ-NULL
                TO (SF)-CAR-ACQ-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-CAR-ACQ
                TO (SF)-CAR-ACQ(IX-TAB-DTR)
           END-IF
           IF DTR-CAR-GEST-NULL = HIGH-VALUES
              MOVE DTR-CAR-GEST-NULL
                TO (SF)-CAR-GEST-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-CAR-GEST
                TO (SF)-CAR-GEST(IX-TAB-DTR)
           END-IF
           IF DTR-CAR-INC-NULL = HIGH-VALUES
              MOVE DTR-CAR-INC-NULL
                TO (SF)-CAR-INC-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-CAR-INC
                TO (SF)-CAR-INC(IX-TAB-DTR)
           END-IF
           IF DTR-MANFEE-ANTIC-NULL = HIGH-VALUES
              MOVE DTR-MANFEE-ANTIC-NULL
                TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-MANFEE-ANTIC
                TO (SF)-MANFEE-ANTIC(IX-TAB-DTR)
           END-IF
           IF DTR-MANFEE-RICOR-NULL = HIGH-VALUES
              MOVE DTR-MANFEE-RICOR-NULL
                TO (SF)-MANFEE-RICOR-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-MANFEE-RICOR
                TO (SF)-MANFEE-RICOR(IX-TAB-DTR)
           END-IF
           IF DTR-MANFEE-REC-NULL = HIGH-VALUES
              MOVE DTR-MANFEE-REC-NULL
                TO (SF)-MANFEE-REC-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-MANFEE-REC
                TO (SF)-MANFEE-REC(IX-TAB-DTR)
           END-IF
           IF DTR-TOT-INTR-PREST-NULL = HIGH-VALUES
              MOVE DTR-TOT-INTR-PREST-NULL
                TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-TOT-INTR-PREST
                TO (SF)-TOT-INTR-PREST(IX-TAB-DTR)
           END-IF
           MOVE DTR-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-DTR)
           MOVE DTR-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-DTR)
           MOVE DTR-DS-VER
             TO (SF)-DS-VER(IX-TAB-DTR)
           MOVE DTR-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-DTR)
           MOVE DTR-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-DTR)
           MOVE DTR-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-DTR)
           MOVE DTR-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-DTR)
           IF DTR-IMP-TRASFE-NULL = HIGH-VALUES
              MOVE DTR-IMP-TRASFE-NULL
                TO (SF)-IMP-TRASFE-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-IMP-TRASFE
                TO (SF)-IMP-TRASFE(IX-TAB-DTR)
           END-IF
           IF DTR-IMP-TFR-STRC-NULL = HIGH-VALUES
              MOVE DTR-IMP-TFR-STRC-NULL
                TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-IMP-TFR-STRC
                TO (SF)-IMP-TFR-STRC(IX-TAB-DTR)
           END-IF
           IF DTR-ACQ-EXP-NULL = HIGH-VALUES
              MOVE DTR-ACQ-EXP-NULL
                TO (SF)-ACQ-EXP-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-ACQ-EXP
                TO (SF)-ACQ-EXP(IX-TAB-DTR)
           END-IF
           IF DTR-REMUN-ASS-NULL = HIGH-VALUES
              MOVE DTR-REMUN-ASS-NULL
                TO (SF)-REMUN-ASS-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-REMUN-ASS
                TO (SF)-REMUN-ASS(IX-TAB-DTR)
           END-IF
           IF DTR-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE DTR-COMMIS-INTER-NULL
                TO (SF)-COMMIS-INTER-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-COMMIS-INTER
                TO (SF)-COMMIS-INTER(IX-TAB-DTR)
           END-IF
           IF DTR-CNBT-ANTIRAC-NULL = HIGH-VALUES
              MOVE DTR-CNBT-ANTIRAC-NULL
                TO (SF)-CNBT-ANTIRAC-NULL(IX-TAB-DTR)
           ELSE
              MOVE DTR-CNBT-ANTIRAC
                TO (SF)-CNBT-ANTIRAC(IX-TAB-DTR)
           END-IF.
       VALORIZZA-OUTPUT-DTR-EX.
           EXIT.

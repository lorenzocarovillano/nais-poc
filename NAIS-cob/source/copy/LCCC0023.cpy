      *----------------------------------------------------------------*
      *   AREA INPUT SERVIZIO VERIFICA PRESENZA MOVIMENTI FUTURI
      *   PORTAFOGLIO VITA - PROCESSO VENDITA
      *   LUNG.
      *----------------------------------------------------------------*
           05 LCCC0023-DATI-INPUT.
              07 LCCC0023-ID-OGG-PTF                 PIC S9(09) COMP-3.
              07 LCCC0023-TP-OGG-PTF                 PIC  X(02).

           05 LCCC0023-DATI-OUTPUT.
              07 LCCC0023-PRE-GRAV-BLOCCANTE         PIC  X(01).
              07 LCCC0023-PRE-GRAV-ANNULLABILE       PIC  X(01).
              07 LCCC0023-ELE-MOV-FUTURI-MAX         PIC S9(04) COMP.
              07 LCCC0023-TAB-MOV-FUTURI      OCCURS 30.
                 10 LCCC0023-ID-MOVI                 PIC S9(09) COMP-3.
                 10 LCCC0023-ID-OGG                  PIC S9(09) COMP-3.
                 10 LCCC0023-ID-OGG-NULL             REDEFINES
                    LCCC0023-ID-OGG                  PIC  X(05).
                 10 LCCC0023-IB-OGG                  PIC  X(40).
                 10 LCCC0023-IB-OGG-NULL             REDEFINES
                    LCCC0023-IB-OGG                  PIC  X(40).
                 10 LCCC0023-IB-MOVI                 PIC  X(40).
                 10 LCCC0023-IB-MOVI-NULL            REDEFINES
                    LCCC0023-IB-MOVI                 PIC  X(40).
                 10 LCCC0023-TP-OGG                  PIC  X(02).
                 10 LCCC0023-TP-OGG-NULL             REDEFINES
                    LCCC0023-TP-OGG                  PIC  X(02).
                 10 LCCC0023-ID-RICH                 PIC S9(09) COMP-3.
                 10 LCCC0023-ID-RICH-NULL            REDEFINES
                    LCCC0023-ID-RICH                 PIC  X(05).
                 10 LCCC0023-TP-MOVI                 PIC S9(05) COMP-3.
                 10 LCCC0023-TP-MOVI-NULL            REDEFINES
                    LCCC0023-TP-MOVI                 PIC  X(03).
                 10 LCCC0023-DT-EFF                  PIC S9(08) COMP-3.
                 10 LCCC0023-ID-MOVI-ANN             PIC S9(09) COMP-3.
                 10 LCCC0023-ID-MOVI-ANN-NULL        REDEFINES
                    LCCC0023-ID-MOVI-ANN             PIC  X(05).
                 10 LCCC0023-ID-MOVI-PRENOTAZ        PIC S9(09) COMP-3.
                 10 LCCC0023-ID-MOVI-PRENOTAZ-NULL   REDEFINES
                    LCCC0023-ID-MOVI-PRENOTAZ        PIC  X(05).
                 10 LCCC0023-TP-GRAVITA-MOVIMENTO    PIC  X(02).
                 10 LCCC0023-DS-TS-CPTZ              PIC S9(18) COMP-3.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVP633
      *   ULTIMO AGG. 24 GEN 2014
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-P63.
           MOVE P63-ID-ACC-COMM
             TO (SF)-ID-PTF
           MOVE P63-ID-ACC-COMM
             TO (SF)-ID-ACC-COMM
           MOVE P63-TP-ACC-COMM
             TO (SF)-TP-ACC-COMM
           MOVE P63-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE P63-COD-PARTNER
             TO (SF)-COD-PARTNER
           MOVE P63-IB-ACC-COMM
             TO (SF)-IB-ACC-COMM
           MOVE P63-IB-ACC-COMM-MASTER
             TO (SF)-IB-ACC-COMM-MASTER
           MOVE P63-DESC-ACC-COMM
             TO (SF)-DESC-ACC-COMM
           MOVE P63-DT-FIRMA
             TO (SF)-DT-FIRMA
           MOVE P63-DT-INI-VLDT
             TO (SF)-DT-INI-VLDT
           MOVE P63-DT-END-VLDT
             TO (SF)-DT-END-VLDT
           MOVE P63-FL-INVIO-CONFERME
             TO (SF)-FL-INVIO-CONFERME
           MOVE P63-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE P63-DS-VER
             TO (SF)-DS-VER
           MOVE P63-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE P63-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE P63-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           MOVE P63-DESC-ACC-COMM-MAST
             TO (SF)-DESC-ACC-COMM-MAST.
       VALORIZZA-OUTPUT-P63-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    GESTIONE ESTRAZIONE IB PROPOSTA (DISPATCHER)
      *----------------------------------------------------------------*
       ESTRAZIONE-IB-PROPOSTA.

      *    SE STIAMO SCRIVENDO PER LA PRIMA VOLTA LA POLIZZA
           IF WPOL-ST-ADD

      *       SE L'IB PROPOSTA NON E' VALORIZZATO
              IF (WPOL-IB-PROP = HIGH-VALUES OR SPACES OR LOW-VALUES)

      *          NUMERAZIONE AUTOMATICA
                 PERFORM ESTR-IB-PROP-AUT-ASSET
                    THRU ESTR-IB-PROP-AUT-ASSET-EX

              ELSE

      *          NUMERAZIONE MANUALE
                 PERFORM ESTRAZIONE-IB-PROPOSTA-MAN
                    THRU ESTRAZIONE-IB-PROPOSTA-MAN-EX

              END-IF

           END-IF.

       ESTRAZIONE-IB-PROPOSTA-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    GESTIONE ESTRAZIONE IB OGGETTO (DISPATCHER)
      *----------------------------------------------------------------*
       ESTRAZIONE-IB-OGGETTO.

           PERFORM ESTRAZIONE-IB-OGG-BNL
              THRU ESTRAZIONE-IB-OGG-BNL-EX.

       ESTRAZIONE-IB-OGGETTO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ESTRAZIONE IB PROPOSTA (GESTIONE AUTOMATICA)
      *----------------------------------------------------------------*
       ESTR-IB-PROP-AUT-ASSET.

           INITIALIZE AREA-ESTRAZIONE-IB.

           SET C161-NUM-PROP
            TO TRUE.

           SET C161-AUTOMATICA
            TO TRUE.

           MOVE SPACES
             TO C161-IB-OGGETTO-I.

      *    INVOCAZIONE AL SERVIZIO IDSS0160 PER LA NUMERAZIONE OGGETTI
           PERFORM CALL-IDSS0160
              THRU CALL-IDSS0160-EX.

      *    VALORIZZAZIONE IB-PROPOSTA DELLA POLIZZA
           IF IDSV0001-ESITO-OK
              MOVE C161-IB-OGGETTO            TO WPOL-IB-PROP
           END-IF

      *    VERIFICA ESISTENZA POLIZZA
           MOVE WPOL-IB-PROP                  TO LCCS0025-IB
           SET  LCCS0025-FL-IB-PROP           TO TRUE

           PERFORM VERIF-ESIST-IB
              THRU VERIF-ESIST-IB-EX

           IF IDSV0001-ESITO-OK
              IF LCCS0025-FL-PRES-PTF-SI
      *          NUMERO PROPOSTA GIA' PRESENTE IN PORTAFOGLIO
                 MOVE WK-PGM              TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'ESTR-IB-PROP-AUT-ASSET'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005211'              TO IEAI9901-COD-ERRORE
                 MOVE SPACES                TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

       ESTR-IB-PROP-AUT-ASSET-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ESTRAZIONE IB PROPOSTA (GESTIONE MANUALE)
      *----------------------------------------------------------------*
       ESTRAZIONE-IB-PROPOSTA-MAN.

      *    VERIFICA ESISTENZA POLIZZA
           MOVE WPOL-IB-PROP                  TO WK-IB-PROP-APPO

      *    CALCOLA LA LUNGHEZZA DI UN CAMPO (SOLO I BYTE RIEMPITI)
           PERFORM CALCOLA-LUNGHEZZA
              THRU CALCOLA-LUNGHEZZA-EX

      *    INSERISCE ZERI NON SIGNIFICATIVI IN UNA STRINGA
           PERFORM INSERT-ZERO
              THRU INSERT-ZERO-EX

           MOVE WK-IB-PROP-APPO               TO LCCS0025-IB
                                                 WPOL-IB-PROP

           SET  LCCS0025-FL-IB-PROP           TO TRUE

           PERFORM VERIF-ESIST-IB
              THRU VERIF-ESIST-IB-EX

           IF IDSV0001-ESITO-OK
              IF LCCS0025-FL-PRES-PTF-SI
      *          NUMERO PROPOSTA GIA' PRESENTE IN PORTAFOGLIO
                 MOVE WK-PGM              TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'ESTRAZIONE-IB-PROPOSTA-MAN'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005211'              TO IEAI9901-COD-ERRORE
                 MOVE SPACES                TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

      *    CALL SERVIZIO NUMERAZIONE OGGETTI IN MODALITA' MANUALE
           IF IDSV0001-ESITO-OK

              INITIALIZE AREA-ESTRAZIONE-IB

              SET C161-NUM-PROP
               TO TRUE

              SET C161-MANUALE
               TO TRUE

              MOVE WPOL-IB-PROP
                TO C161-IB-OGGETTO-I

      *       INVOCAZIONE AL SERVIZIO IDSS0160 PER LA NUMERAZIONE OGGETTI
              PERFORM CALL-IDSS0160
                 THRU CALL-IDSS0160-EX

           END-IF.

       ESTRAZIONE-IB-PROPOSTA-MAN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ESTRAZIONE IB OGGETTO PER BNL
      *----------------------------------------------------------------*
       ESTRAZIONE-IB-OGG-BNL.

      *    PER BNL L'IB-OGGETTO DEVE COINCIDERE CON L'IB-PROPOSTA
           MOVE WPOL-IB-PROP                 TO WPOL-IB-OGG.

       ESTRAZIONE-IB-OGG-BNL-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    CALL IDSS0160
      *----------------------------------------------------------------*
       CALL-IDSS0160.

           PERFORM VALORIZZA-AREE-S0088
              THRU VALORIZZA-AREE-S0088-EX.

           CALL IDSS0160          USING  AREA-IDSV0001
                                         AREA-ESTRAZIONE-IB
           ON EXCEPTION
      *       "ERRORE DI SISTEMA DURANTE..."
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'CALL NUMERAZIONE OGGETTI (IDSS0160)'
                TO CALL-DESC
              MOVE 'CALL-IDSS0160'
                TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

       CALL-IDSS0160-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   VERIFICA ESISTENZA IB
      *----------------------------------------------------------------*
       VERIF-ESIST-IB.

           CALL LCCS0025 USING AREA-IDSV0001
                               WCOM-AREA-STATI
                               LCCC0025-AREA-COMUNICAZ
           ON EXCEPTION
      *       "ERRORE DI SISTEMA DURANTE..."
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SERVIZIO VERIFICA ESISTENZA POLIZZA (LCCS0025)'
                TO CALL-DESC
              MOVE 'VERIF-ESIST-IB'
                TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

       VERIF-ESIST-IB-EX.
           EXIT.

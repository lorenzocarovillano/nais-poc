      *----------------------------------------------------------------*
      *  Servizio -  Estratto Conto Diagnostico
      *----------------------------------------------------------------*
         02 ISPC0580-DATI-INPUT.
            05 ISPC0580-COD-COMPAGNIA-ANIA           PIC 9(005).
            05 ISPC0580-DATA-INIZ-ELAB               PIC X(008).
            05 ISPC0580-DATA-FINE-ELAB               PIC X(008).
            05 ISPC0580-DATA-RIFERIMENTO             PIC X(08).
            05 ISPC0580-LIVELLO-UTENTE               PIC 9(02).
            05 ISPC0580-FUNZIONALITA                 PIC 9(05).
            05 ISPC0580-SESSION-ID                   PIC X(20).

         02 ISPC0580-DATI-OUTPUT.
            05 ISPC0580-NUM-MAX-PROD                PIC 9(003).
            05 ISPC0580-TAB-PROD                OCCURS 250.
               07 ISPC0580-TIPO-PRD                  PIC X(012).
               07 ISPC0580-TIPO-GAR                  PIC X(012).
               07 ISPC0580-SOTTORAMO                 PIC X(030).
               07 ISPC0580-FORMAPREST                PIC X(070).
               07 ISPC0580-FORMA                     PIC X(070).
               07 ISPC0580-TIPOPREMIO                PIC X(030).

          02 ISPC0580-AREA-ERRORI.
             03 ISPC0580-ESITO                       PIC 9(02).
             03 ISPC0580-ERR-NUM-ELE                 PIC 9(03).
             03 ISPC0580-TAB-ERRORI            OCCURS 10.
                05 ISPC0580-COD-ERR                  PIC X(12).
                05 ISPC0580-DESCRIZIONE-ERR          PIC X(50).
                05 ISPC0580-GRAVITA-ERR              PIC 9(02).
                05 ISPC0580-LIVELLO-DEROGA           PIC 9(02).


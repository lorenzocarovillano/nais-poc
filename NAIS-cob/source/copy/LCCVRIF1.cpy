      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA RICH_INVST_FND
      *   ALIAS RIF
      *   ULTIMO AGG. 25 NOV 2019
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-RICH-INVST-FND PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-FINRIO PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-FND PIC X(20).
             07 (SF)-COD-FND-NULL REDEFINES
                (SF)-COD-FND   PIC X(20).
             07 (SF)-NUM-QUO PIC S9(7)V9(5) COMP-3.
             07 (SF)-NUM-QUO-NULL REDEFINES
                (SF)-NUM-QUO   PIC X(7).
             07 (SF)-PC PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-NULL REDEFINES
                (SF)-PC   PIC X(4).
             07 (SF)-IMP-MOVTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-MOVTO-NULL REDEFINES
                (SF)-IMP-MOVTO   PIC X(8).
             07 (SF)-DT-INVST   PIC S9(8) COMP-3.
             07 (SF)-DT-INVST-NULL REDEFINES
                (SF)-DT-INVST   PIC X(5).
             07 (SF)-COD-TARI PIC X(12).
             07 (SF)-COD-TARI-NULL REDEFINES
                (SF)-COD-TARI   PIC X(12).
             07 (SF)-TP-STAT PIC X(2).
             07 (SF)-TP-STAT-NULL REDEFINES
                (SF)-TP-STAT   PIC X(2).
             07 (SF)-TP-MOD-INVST PIC X(2).
             07 (SF)-TP-MOD-INVST-NULL REDEFINES
                (SF)-TP-MOD-INVST   PIC X(2).
             07 (SF)-COD-DIV PIC X(20).
             07 (SF)-DT-CAMBIO-VLT   PIC S9(8) COMP-3.
             07 (SF)-DT-CAMBIO-VLT-NULL REDEFINES
                (SF)-DT-CAMBIO-VLT   PIC X(5).
             07 (SF)-TP-FND PIC X(1).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-DT-INVST-CALC   PIC S9(8) COMP-3.
             07 (SF)-DT-INVST-CALC-NULL REDEFINES
                (SF)-DT-INVST-CALC   PIC X(5).
             07 (SF)-FL-CALC-INVTO PIC X(1).
             07 (SF)-FL-CALC-INVTO-NULL REDEFINES
                (SF)-FL-CALC-INVTO   PIC X(1).
             07 (SF)-IMP-GAP-EVENT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-GAP-EVENT-NULL REDEFINES
                (SF)-IMP-GAP-EVENT   PIC X(8).
             07 (SF)-FL-SWM-BP2S PIC X(1).
             07 (SF)-FL-SWM-BP2S-NULL REDEFINES
                (SF)-FL-SWM-BP2S   PIC X(1).

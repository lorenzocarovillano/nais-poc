      *-----------------------------------------------------------------
      *   AREA COMUNE INPUT LETTURA DATI PORTAFOGLIO
      *   PORTAFOGLIO VITA
      *   CREATA :  05/02/2007
      *-----------------------------------------------------------------
      *      03 (SF)-DATI-INPUT.
               05 (SF)-ID-OGGETTO-PTF                 PIC  9(009).
               05 (SF)-TIPO-OGGETTO-PTF               PIC  X(002).
               05 (SF)-ID-PTF                         PIC  9(009).
               05 (SF)-IB-OGGETTO-PTF                 PIC  X(040).
               05 (SF)-IB-OGGETTO-SEC-PTF             PIC  X(040).
               05 (SF)-DATA-INIZIO-EFFETTO-PTF        PIC  9(008).
               05 (SF)-DATA-FINE-EFFETTO-PTF          PIC  9(008).
               05 (SF)-NOME-QUERY-PTF                 PIC  X(008).
               05 (SF)-DATI-WHERE-COND-PTF            PIC  X(030).
               05 (SF)-TIPO-LETTURA                   PIC  X(003).
                  88 (SF)-PRIMARY-KEY-DSH             VALUE 'PK '.
                  88 (SF)-ID-DSH                      VALUE 'ID '.
                  88 (SF)-IB-OGGETTO-DSH              VALUE 'IBO'.
                  88 (SF)-ID-PADRE-DSH                VALUE 'IDP'.
                  88 (SF)-IB-SECONDARIO-DSH           VALUE 'IBS'.
                  88 (SF)-ID-OGGETTO-DSH              VALUE 'IDO'.
                  88 (SF)-WHERE-COND-DSH              VALUE 'WC'.

             03 (SF)-DATI-OUTPUT.
               05 (SF)-FLAG-OVERFLOW                   PIC X(002).
                  88 (SF)-OVERFLOW-YES                 VALUE 'SI'.
                  88 (SF)-OVERFLOW-NO                  VALUE 'NO'.










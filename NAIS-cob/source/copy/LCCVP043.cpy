
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVP043
      *   ULTIMO AGG. 23 FEB 2011
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-P04.
           MOVE P04-ID-STAT-RICH-EST
             TO (SF)-ID-PTF
           MOVE P04-ID-STAT-RICH-EST
             TO (SF)-ID-STAT-RICH-EST
           MOVE P04-ID-RICH-EST
             TO (SF)-ID-RICH-EST
           MOVE P04-TS-INI-VLDT
             TO (SF)-TS-INI-VLDT
           MOVE P04-TS-END-VLDT
             TO (SF)-TS-END-VLDT
           MOVE P04-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE P04-COD-PRCS
             TO (SF)-COD-PRCS
           MOVE P04-COD-ATTVT
             TO (SF)-COD-ATTVT
           MOVE P04-STAT-RICH-EST
             TO (SF)-STAT-RICH-EST
           IF P04-FL-STAT-END-NULL = HIGH-VALUES
              MOVE P04-FL-STAT-END-NULL
                TO (SF)-FL-STAT-END-NULL
           ELSE
              MOVE P04-FL-STAT-END
                TO (SF)-FL-STAT-END
           END-IF
           MOVE P04-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE P04-DS-VER
             TO (SF)-DS-VER
           MOVE P04-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE P04-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE P04-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           IF P04-TP-CAUS-SCARTO-NULL = HIGH-VALUES
              MOVE P04-TP-CAUS-SCARTO-NULL
                TO (SF)-TP-CAUS-SCARTO-NULL
           ELSE
              MOVE P04-TP-CAUS-SCARTO
                TO (SF)-TP-CAUS-SCARTO
           END-IF
           MOVE P04-DESC-ERR
             TO (SF)-DESC-ERR
           MOVE P04-UTENTE-INS-AGG
             TO (SF)-UTENTE-INS-AGG
           IF P04-COD-ERR-SCARTO-NULL = HIGH-VALUES
              MOVE P04-COD-ERR-SCARTO-NULL
                TO (SF)-COD-ERR-SCARTO-NULL
           ELSE
              MOVE P04-COD-ERR-SCARTO
                TO (SF)-COD-ERR-SCARTO
           END-IF
           IF P04-ID-MOVI-CRZ-NULL = HIGH-VALUES
              MOVE P04-ID-MOVI-CRZ-NULL
                TO (SF)-ID-MOVI-CRZ-NULL
           ELSE
              MOVE P04-ID-MOVI-CRZ
                TO (SF)-ID-MOVI-CRZ
           END-IF.
       VALORIZZA-OUTPUT-P04-EX.
           EXIT.

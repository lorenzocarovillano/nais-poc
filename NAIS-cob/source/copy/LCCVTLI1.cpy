      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA TRCH_LIQ
      *   ALIAS TLI
      *   ULTIMO AGG. 09 LUG 2009
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-TRCH-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-GAR-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-GAR-LIQ-NULL REDEFINES
                (SF)-ID-GAR-LIQ   PIC X(5).
             07 (SF)-ID-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-TRCH-DI-GAR PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-IMP-LRD-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-LRD-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-LRD-DFZ-NULL REDEFINES
                (SF)-IMP-LRD-DFZ   PIC X(8).
             07 (SF)-IMP-LRD-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-NET-CALC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-NET-DFZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-NET-DFZ-NULL REDEFINES
                (SF)-IMP-NET-DFZ   PIC X(8).
             07 (SF)-IMP-NET-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-UTI PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-UTI-NULL REDEFINES
                (SF)-IMP-UTI   PIC X(8).
             07 (SF)-COD-TARI PIC X(12).
             07 (SF)-COD-TARI-NULL REDEFINES
                (SF)-COD-TARI   PIC X(12).
             07 (SF)-COD-DVS PIC X(20).
             07 (SF)-COD-DVS-NULL REDEFINES
                (SF)-COD-DVS   PIC X(20).
             07 (SF)-IAS-ONER-PRVNT-FIN PIC S9(12)V9(3) COMP-3.
             07 (SF)-IAS-ONER-PRVNT-FIN-NULL REDEFINES
                (SF)-IAS-ONER-PRVNT-FIN   PIC X(8).
             07 (SF)-IAS-MGG-SIN PIC S9(12)V9(3) COMP-3.
             07 (SF)-IAS-MGG-SIN-NULL REDEFINES
                (SF)-IAS-MGG-SIN   PIC X(8).
             07 (SF)-IAS-RST-DPST PIC S9(12)V9(3) COMP-3.
             07 (SF)-IAS-RST-DPST-NULL REDEFINES
                (SF)-IAS-RST-DPST   PIC X(8).
             07 (SF)-IMP-RIMB PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-RIMB-NULL REDEFINES
                (SF)-IMP-RIMB   PIC X(8).
             07 (SF)-COMPON-TAX-RIMB PIC S9(12)V9(3) COMP-3.
             07 (SF)-COMPON-TAX-RIMB-NULL REDEFINES
                (SF)-COMPON-TAX-RIMB   PIC X(8).
             07 (SF)-RIS-MAT PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-MAT-NULL REDEFINES
                (SF)-RIS-MAT   PIC X(8).
             07 (SF)-RIS-SPE PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-SPE-NULL REDEFINES
                (SF)-RIS-SPE   PIC X(8).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-IAS-PNL PIC S9(12)V9(3) COMP-3.
             07 (SF)-IAS-PNL-NULL REDEFINES
                (SF)-IAS-PNL   PIC X(8).

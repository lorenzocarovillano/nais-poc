
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVADE3
      *   ULTIMO AGG. 07 DIC 2010
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-ADE.
           MOVE ADE-ID-ADES
             TO (SF)-ID-PTF(IX-TAB-ADE)
           MOVE ADE-ID-ADES
             TO (SF)-ID-ADES(IX-TAB-ADE)
           MOVE ADE-ID-POLI
             TO (SF)-ID-POLI(IX-TAB-ADE)
           MOVE ADE-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-ADE)
           IF ADE-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE ADE-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-ADE)
           END-IF
           MOVE ADE-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-ADE)
           MOVE ADE-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-ADE)
           IF ADE-IB-PREV-NULL = HIGH-VALUES
              MOVE ADE-IB-PREV-NULL
                TO (SF)-IB-PREV-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-IB-PREV
                TO (SF)-IB-PREV(IX-TAB-ADE)
           END-IF
           IF ADE-IB-OGG-NULL = HIGH-VALUES
              MOVE ADE-IB-OGG-NULL
                TO (SF)-IB-OGG-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-IB-OGG
                TO (SF)-IB-OGG(IX-TAB-ADE)
           END-IF
           MOVE ADE-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-ADE)
           IF ADE-DT-DECOR-NULL = HIGH-VALUES
              MOVE ADE-DT-DECOR-NULL
                TO (SF)-DT-DECOR-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-DT-DECOR
                TO (SF)-DT-DECOR(IX-TAB-ADE)
           END-IF
           IF ADE-DT-SCAD-NULL = HIGH-VALUES
              MOVE ADE-DT-SCAD-NULL
                TO (SF)-DT-SCAD-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-DT-SCAD
                TO (SF)-DT-SCAD(IX-TAB-ADE)
           END-IF
           IF ADE-ETA-A-SCAD-NULL = HIGH-VALUES
              MOVE ADE-ETA-A-SCAD-NULL
                TO (SF)-ETA-A-SCAD-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-ETA-A-SCAD
                TO (SF)-ETA-A-SCAD(IX-TAB-ADE)
           END-IF
           IF ADE-DUR-AA-NULL = HIGH-VALUES
              MOVE ADE-DUR-AA-NULL
                TO (SF)-DUR-AA-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-DUR-AA
                TO (SF)-DUR-AA(IX-TAB-ADE)
           END-IF
           IF ADE-DUR-MM-NULL = HIGH-VALUES
              MOVE ADE-DUR-MM-NULL
                TO (SF)-DUR-MM-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-DUR-MM
                TO (SF)-DUR-MM(IX-TAB-ADE)
           END-IF
           IF ADE-DUR-GG-NULL = HIGH-VALUES
              MOVE ADE-DUR-GG-NULL
                TO (SF)-DUR-GG-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-DUR-GG
                TO (SF)-DUR-GG(IX-TAB-ADE)
           END-IF
           MOVE ADE-TP-RGM-FISC
             TO (SF)-TP-RGM-FISC(IX-TAB-ADE)
           IF ADE-TP-RIAT-NULL = HIGH-VALUES
              MOVE ADE-TP-RIAT-NULL
                TO (SF)-TP-RIAT-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-TP-RIAT
                TO (SF)-TP-RIAT(IX-TAB-ADE)
           END-IF
           MOVE ADE-TP-MOD-PAG-TIT
             TO (SF)-TP-MOD-PAG-TIT(IX-TAB-ADE)
           IF ADE-TP-IAS-NULL = HIGH-VALUES
              MOVE ADE-TP-IAS-NULL
                TO (SF)-TP-IAS-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-TP-IAS
                TO (SF)-TP-IAS(IX-TAB-ADE)
           END-IF
           IF ADE-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
              MOVE ADE-DT-VARZ-TP-IAS-NULL
                TO (SF)-DT-VARZ-TP-IAS-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-DT-VARZ-TP-IAS
                TO (SF)-DT-VARZ-TP-IAS(IX-TAB-ADE)
           END-IF
           IF ADE-PRE-NET-IND-NULL = HIGH-VALUES
              MOVE ADE-PRE-NET-IND-NULL
                TO (SF)-PRE-NET-IND-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-PRE-NET-IND
                TO (SF)-PRE-NET-IND(IX-TAB-ADE)
           END-IF
           IF ADE-PRE-LRD-IND-NULL = HIGH-VALUES
              MOVE ADE-PRE-LRD-IND-NULL
                TO (SF)-PRE-LRD-IND-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-PRE-LRD-IND
                TO (SF)-PRE-LRD-IND(IX-TAB-ADE)
           END-IF
           IF ADE-RAT-LRD-IND-NULL = HIGH-VALUES
              MOVE ADE-RAT-LRD-IND-NULL
                TO (SF)-RAT-LRD-IND-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-RAT-LRD-IND
                TO (SF)-RAT-LRD-IND(IX-TAB-ADE)
           END-IF
           IF ADE-PRSTZ-INI-IND-NULL = HIGH-VALUES
              MOVE ADE-PRSTZ-INI-IND-NULL
                TO (SF)-PRSTZ-INI-IND-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-PRSTZ-INI-IND
                TO (SF)-PRSTZ-INI-IND(IX-TAB-ADE)
           END-IF
           IF ADE-FL-COINC-ASSTO-NULL = HIGH-VALUES
              MOVE ADE-FL-COINC-ASSTO-NULL
                TO (SF)-FL-COINC-ASSTO-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-FL-COINC-ASSTO
                TO (SF)-FL-COINC-ASSTO(IX-TAB-ADE)
           END-IF
           IF ADE-IB-DFLT-NULL = HIGH-VALUES
              MOVE ADE-IB-DFLT-NULL
                TO (SF)-IB-DFLT-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-IB-DFLT
                TO (SF)-IB-DFLT(IX-TAB-ADE)
           END-IF
           IF ADE-MOD-CALC-NULL = HIGH-VALUES
              MOVE ADE-MOD-CALC-NULL
                TO (SF)-MOD-CALC-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-MOD-CALC
                TO (SF)-MOD-CALC(IX-TAB-ADE)
           END-IF
           IF ADE-TP-FNT-CNBTVA-NULL = HIGH-VALUES
              MOVE ADE-TP-FNT-CNBTVA-NULL
                TO (SF)-TP-FNT-CNBTVA-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-TP-FNT-CNBTVA
                TO (SF)-TP-FNT-CNBTVA(IX-TAB-ADE)
           END-IF
           IF ADE-IMP-AZ-NULL = HIGH-VALUES
              MOVE ADE-IMP-AZ-NULL
                TO (SF)-IMP-AZ-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-IMP-AZ
                TO (SF)-IMP-AZ(IX-TAB-ADE)
           END-IF
           IF ADE-IMP-ADER-NULL = HIGH-VALUES
              MOVE ADE-IMP-ADER-NULL
                TO (SF)-IMP-ADER-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-IMP-ADER
                TO (SF)-IMP-ADER(IX-TAB-ADE)
           END-IF
           IF ADE-IMP-TFR-NULL = HIGH-VALUES
              MOVE ADE-IMP-TFR-NULL
                TO (SF)-IMP-TFR-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-IMP-TFR
                TO (SF)-IMP-TFR(IX-TAB-ADE)
           END-IF
           IF ADE-IMP-VOLO-NULL = HIGH-VALUES
              MOVE ADE-IMP-VOLO-NULL
                TO (SF)-IMP-VOLO-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-IMP-VOLO
                TO (SF)-IMP-VOLO(IX-TAB-ADE)
           END-IF
           IF ADE-PC-AZ-NULL = HIGH-VALUES
              MOVE ADE-PC-AZ-NULL
                TO (SF)-PC-AZ-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-PC-AZ
                TO (SF)-PC-AZ(IX-TAB-ADE)
           END-IF
           IF ADE-PC-ADER-NULL = HIGH-VALUES
              MOVE ADE-PC-ADER-NULL
                TO (SF)-PC-ADER-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-PC-ADER
                TO (SF)-PC-ADER(IX-TAB-ADE)
           END-IF
           IF ADE-PC-TFR-NULL = HIGH-VALUES
              MOVE ADE-PC-TFR-NULL
                TO (SF)-PC-TFR-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-PC-TFR
                TO (SF)-PC-TFR(IX-TAB-ADE)
           END-IF
           IF ADE-PC-VOLO-NULL = HIGH-VALUES
              MOVE ADE-PC-VOLO-NULL
                TO (SF)-PC-VOLO-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-PC-VOLO
                TO (SF)-PC-VOLO(IX-TAB-ADE)
           END-IF
           IF ADE-DT-NOVA-RGM-FISC-NULL = HIGH-VALUES
              MOVE ADE-DT-NOVA-RGM-FISC-NULL
                TO (SF)-DT-NOVA-RGM-FISC-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-DT-NOVA-RGM-FISC
                TO (SF)-DT-NOVA-RGM-FISC(IX-TAB-ADE)
           END-IF
           IF ADE-FL-ATTIV-NULL = HIGH-VALUES
              MOVE ADE-FL-ATTIV-NULL
                TO (SF)-FL-ATTIV-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-FL-ATTIV
                TO (SF)-FL-ATTIV(IX-TAB-ADE)
           END-IF
           IF ADE-IMP-REC-RIT-VIS-NULL = HIGH-VALUES
              MOVE ADE-IMP-REC-RIT-VIS-NULL
                TO (SF)-IMP-REC-RIT-VIS-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-IMP-REC-RIT-VIS
                TO (SF)-IMP-REC-RIT-VIS(IX-TAB-ADE)
           END-IF
           IF ADE-IMP-REC-RIT-ACC-NULL = HIGH-VALUES
              MOVE ADE-IMP-REC-RIT-ACC-NULL
                TO (SF)-IMP-REC-RIT-ACC-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-IMP-REC-RIT-ACC
                TO (SF)-IMP-REC-RIT-ACC(IX-TAB-ADE)
           END-IF
           IF ADE-FL-VARZ-STAT-TBGC-NULL = HIGH-VALUES
              MOVE ADE-FL-VARZ-STAT-TBGC-NULL
                TO (SF)-FL-VARZ-STAT-TBGC-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-FL-VARZ-STAT-TBGC
                TO (SF)-FL-VARZ-STAT-TBGC(IX-TAB-ADE)
           END-IF
           IF ADE-FL-PROVZA-MIGRAZ-NULL = HIGH-VALUES
              MOVE ADE-FL-PROVZA-MIGRAZ-NULL
                TO (SF)-FL-PROVZA-MIGRAZ-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-FL-PROVZA-MIGRAZ
                TO (SF)-FL-PROVZA-MIGRAZ(IX-TAB-ADE)
           END-IF
           IF ADE-IMPB-VIS-DA-REC-NULL = HIGH-VALUES
              MOVE ADE-IMPB-VIS-DA-REC-NULL
                TO (SF)-IMPB-VIS-DA-REC-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-IMPB-VIS-DA-REC
                TO (SF)-IMPB-VIS-DA-REC(IX-TAB-ADE)
           END-IF
           IF ADE-DT-DECOR-PREST-BAN-NULL = HIGH-VALUES
              MOVE ADE-DT-DECOR-PREST-BAN-NULL
                TO (SF)-DT-DECOR-PREST-BAN-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-DT-DECOR-PREST-BAN
                TO (SF)-DT-DECOR-PREST-BAN(IX-TAB-ADE)
           END-IF
           IF ADE-DT-EFF-VARZ-STAT-T-NULL = HIGH-VALUES
              MOVE ADE-DT-EFF-VARZ-STAT-T-NULL
                TO (SF)-DT-EFF-VARZ-STAT-T-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-DT-EFF-VARZ-STAT-T
                TO (SF)-DT-EFF-VARZ-STAT-T(IX-TAB-ADE)
           END-IF
           MOVE ADE-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-ADE)
           MOVE ADE-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-ADE)
           MOVE ADE-DS-VER
             TO (SF)-DS-VER(IX-TAB-ADE)
           MOVE ADE-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-ADE)
           MOVE ADE-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-ADE)
           MOVE ADE-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-ADE)
           MOVE ADE-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-ADE)
           IF ADE-CUM-CNBT-CAP-NULL = HIGH-VALUES
              MOVE ADE-CUM-CNBT-CAP-NULL
                TO (SF)-CUM-CNBT-CAP-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-CUM-CNBT-CAP
                TO (SF)-CUM-CNBT-CAP(IX-TAB-ADE)
           END-IF
           IF ADE-IMP-GAR-CNBT-NULL = HIGH-VALUES
              MOVE ADE-IMP-GAR-CNBT-NULL
                TO (SF)-IMP-GAR-CNBT-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-IMP-GAR-CNBT
                TO (SF)-IMP-GAR-CNBT(IX-TAB-ADE)
           END-IF
           IF ADE-DT-ULT-CONS-CNBT-NULL = HIGH-VALUES
              MOVE ADE-DT-ULT-CONS-CNBT-NULL
                TO (SF)-DT-ULT-CONS-CNBT-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-DT-ULT-CONS-CNBT
                TO (SF)-DT-ULT-CONS-CNBT(IX-TAB-ADE)
           END-IF
           IF ADE-IDEN-ISC-FND-NULL = HIGH-VALUES
              MOVE ADE-IDEN-ISC-FND-NULL
                TO (SF)-IDEN-ISC-FND-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-IDEN-ISC-FND
                TO (SF)-IDEN-ISC-FND(IX-TAB-ADE)
           END-IF
           IF ADE-NUM-RAT-PIAN-NULL = HIGH-VALUES
              MOVE ADE-NUM-RAT-PIAN-NULL
                TO (SF)-NUM-RAT-PIAN-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-NUM-RAT-PIAN
                TO (SF)-NUM-RAT-PIAN(IX-TAB-ADE)
           END-IF
           IF ADE-DT-PRESC-NULL = HIGH-VALUES
              MOVE ADE-DT-PRESC-NULL
                TO (SF)-DT-PRESC-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-DT-PRESC
                TO (SF)-DT-PRESC(IX-TAB-ADE)
           END-IF
           IF ADE-CONCS-PREST-NULL = HIGH-VALUES
              MOVE ADE-CONCS-PREST-NULL
                TO (SF)-CONCS-PREST-NULL(IX-TAB-ADE)
           ELSE
              MOVE ADE-CONCS-PREST
                TO (SF)-CONCS-PREST(IX-TAB-ADE)
           END-IF.
       VALORIZZA-OUTPUT-ADE-EX.
           EXIT.

      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **    LETTURA STRAT-DI-INVST                                    **
      **                                                              **
      ******************************************************************

       LETTURA-SDI.

           IF IDSI0011-SELECT
              PERFORM SELECT-SDI
                 THRU SELECT-SDI-EX
           ELSE
              PERFORM FETCH-SDI
                 THRU FETCH-SDI-EX
           END-IF.

       LETTURA-SDI-EX.
           EXIT.

       SELECT-SDI.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

              IF IDSO0011-SUCCESSFUL-RC
                 EVALUATE TRUE
                     WHEN IDSO0011-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                        MOVE IDSO0011-BUFFER-DATI
                          TO STRAT-DI-INVST
                        PERFORM VALORIZZA-OUTPUT-SDI
                           THRU VALORIZZA-OUTPUT-SDI-EX
                     WHEN IDSO0011-NOT-FOUND
      *--->          CAMPO $ NON TROVATO
                            MOVE WK-PGM
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'SELECT-SDI'
                             TO IEAI9901-LABEL-ERR
                           MOVE '005019'
                             TO IEAI9901-COD-ERRORE
                           MOVE 'ID-DETT-RICH'
                             TO IEAI9901-PARAMETRI-ERR
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                      WHEN OTHER
      *--->          ERRORE DI ACCESSO AL DB
                        MOVE WK-PGM
                                        TO IEAI9901-COD-SERVIZIO-BE
                        MOVE 'SELECT-SDI'
                                        TO IEAI9901-LABEL-ERR
                        MOVE '005015'   TO IEAI9901-COD-ERRORE
                        MOVE WK-TABELLA TO IEAI9901-PARAMETRI-ERR
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                           THRU EX-S0300
                 END-EVALUATE
              ELSE
      *-->       GESTIRE ERRORE DISPATCHER
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'SELECT-SDI'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005016'
                   TO IEAI9901-COD-ERRORE
                 MOVE SPACES
                   TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-PERFORM.

       SELECT-SDI-EX.
           EXIT.

       FETCH-SDI.

           SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
           SET  WCOM-OVERFLOW-NO                  TO TRUE.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR WCOM-OVERFLOW-YES

               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *-->                 NON TROVATA
                           IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM
                                TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'FETCH-SDI'
                                TO IEAI9901-LABEL-ERR
                              MOVE '005019'
                                TO IEAI9901-COD-ERRORE
                              MOVE 'ID-PTF'
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                           END-IF
                      WHEN IDSO0011-SUCCESSFUL-SQL
      *-->                 OPERAZIONE ESEGUITA CORRETTAMENTE
                           MOVE IDSO0011-BUFFER-DATI  TO STRAT-DI-INVST
                           ADD  1                     TO IX-TAB-SDI

      *-->  Se il contatore di lettura � maggiore dell' elemento MAX
      *-->  previsto per la TAB.STRAT-DI-INVST allora siamo in overflow
                           IF IX-TAB-SDI > (SF)-ELE-SDI-MAX
                              SET WCOM-OVERFLOW-YES   TO TRUE
                           ELSE
                              PERFORM VALORIZZA-OUTPUT-SDI
                                 THRU VALORIZZA-OUTPUT-SDI-EX
                              SET IDSI0011-FETCH-NEXT TO TRUE
                           END-IF
                      WHEN OTHER
      *-->                 ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'FETCH-SDI'
                             TO IEAI9901-LABEL-ERR
                           MOVE '005015'
                             TO IEAI9901-COD-ERRORE
                           MOVE WK-TABELLA
                             TO IEAI9901-PARAMETRI-ERR
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      *-->        ERRORE DISPATCHER
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'FETCH-SDI'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005016'
                    TO IEAI9901-COD-ERRORE
                  MOVE SPACES
                    TO IEAI9901-PARAMETRI-ERR
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

       FETCH-SDI-EX.
           EXIT.


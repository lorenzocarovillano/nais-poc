
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVDAD5
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-DAD.
           IF (SF)-IB-POLI-NULL = HIGH-VALUES
              MOVE (SF)-IB-POLI-NULL
              TO DAD-IB-POLI-NULL
           ELSE
              MOVE (SF)-IB-POLI
              TO DAD-IB-POLI
           END-IF
           IF (SF)-IB-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-IB-DFLT-NULL
              TO DAD-IB-DFLT-NULL
           ELSE
              MOVE (SF)-IB-DFLT
              TO DAD-IB-DFLT
           END-IF
           IF (SF)-COD-GAR-1-NULL = HIGH-VALUES
              MOVE (SF)-COD-GAR-1-NULL
              TO DAD-COD-GAR-1-NULL
           ELSE
              MOVE (SF)-COD-GAR-1
              TO DAD-COD-GAR-1
           END-IF
           IF (SF)-COD-GAR-2-NULL = HIGH-VALUES
              MOVE (SF)-COD-GAR-2-NULL
              TO DAD-COD-GAR-2-NULL
           ELSE
              MOVE (SF)-COD-GAR-2
              TO DAD-COD-GAR-2
           END-IF
           IF (SF)-COD-GAR-3-NULL = HIGH-VALUES
              MOVE (SF)-COD-GAR-3-NULL
              TO DAD-COD-GAR-3-NULL
           ELSE
              MOVE (SF)-COD-GAR-3
              TO DAD-COD-GAR-3
           END-IF
           IF (SF)-COD-GAR-4-NULL = HIGH-VALUES
              MOVE (SF)-COD-GAR-4-NULL
              TO DAD-COD-GAR-4-NULL
           ELSE
              MOVE (SF)-COD-GAR-4
              TO DAD-COD-GAR-4
           END-IF
           IF (SF)-COD-GAR-5-NULL = HIGH-VALUES
              MOVE (SF)-COD-GAR-5-NULL
              TO DAD-COD-GAR-5-NULL
           ELSE
              MOVE (SF)-COD-GAR-5
              TO DAD-COD-GAR-5
           END-IF
           IF (SF)-COD-GAR-6-NULL = HIGH-VALUES
              MOVE (SF)-COD-GAR-6-NULL
              TO DAD-COD-GAR-6-NULL
           ELSE
              MOVE (SF)-COD-GAR-6
              TO DAD-COD-GAR-6
           END-IF
           IF (SF)-DT-DECOR-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-DT-DECOR-DFLT-NULL
              TO DAD-DT-DECOR-DFLT-NULL
           ELSE
             IF (SF)-DT-DECOR-DFLT = ZERO
                MOVE HIGH-VALUES
                TO DAD-DT-DECOR-DFLT-NULL
             ELSE
              MOVE (SF)-DT-DECOR-DFLT
              TO DAD-DT-DECOR-DFLT
             END-IF
           END-IF
           IF (SF)-ETA-SCAD-MASC-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-ETA-SCAD-MASC-DFLT-NULL
              TO DAD-ETA-SCAD-MASC-DFLT-NULL
           ELSE
              MOVE (SF)-ETA-SCAD-MASC-DFLT
              TO DAD-ETA-SCAD-MASC-DFLT
           END-IF
           IF (SF)-ETA-SCAD-FEMM-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-ETA-SCAD-FEMM-DFLT-NULL
              TO DAD-ETA-SCAD-FEMM-DFLT-NULL
           ELSE
              MOVE (SF)-ETA-SCAD-FEMM-DFLT
              TO DAD-ETA-SCAD-FEMM-DFLT
           END-IF
           IF (SF)-DUR-AA-ADES-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-DUR-AA-ADES-DFLT-NULL
              TO DAD-DUR-AA-ADES-DFLT-NULL
           ELSE
              MOVE (SF)-DUR-AA-ADES-DFLT
              TO DAD-DUR-AA-ADES-DFLT
           END-IF
           IF (SF)-DUR-MM-ADES-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-DUR-MM-ADES-DFLT-NULL
              TO DAD-DUR-MM-ADES-DFLT-NULL
           ELSE
              MOVE (SF)-DUR-MM-ADES-DFLT
              TO DAD-DUR-MM-ADES-DFLT
           END-IF
           IF (SF)-DUR-GG-ADES-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-DUR-GG-ADES-DFLT-NULL
              TO DAD-DUR-GG-ADES-DFLT-NULL
           ELSE
              MOVE (SF)-DUR-GG-ADES-DFLT
              TO DAD-DUR-GG-ADES-DFLT
           END-IF
           IF (SF)-DT-SCAD-ADES-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-DT-SCAD-ADES-DFLT-NULL
              TO DAD-DT-SCAD-ADES-DFLT-NULL
           ELSE
             IF (SF)-DT-SCAD-ADES-DFLT = ZERO
                MOVE HIGH-VALUES
                TO DAD-DT-SCAD-ADES-DFLT-NULL
             ELSE
              MOVE (SF)-DT-SCAD-ADES-DFLT
              TO DAD-DT-SCAD-ADES-DFLT
             END-IF
           END-IF
           IF (SF)-FRAZ-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-FRAZ-DFLT-NULL
              TO DAD-FRAZ-DFLT-NULL
           ELSE
              MOVE (SF)-FRAZ-DFLT
              TO DAD-FRAZ-DFLT
           END-IF
           IF (SF)-PC-PROV-INC-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-PC-PROV-INC-DFLT-NULL
              TO DAD-PC-PROV-INC-DFLT-NULL
           ELSE
              MOVE (SF)-PC-PROV-INC-DFLT
              TO DAD-PC-PROV-INC-DFLT
           END-IF
           IF (SF)-IMP-PROV-INC-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-IMP-PROV-INC-DFLT-NULL
              TO DAD-IMP-PROV-INC-DFLT-NULL
           ELSE
              MOVE (SF)-IMP-PROV-INC-DFLT
              TO DAD-IMP-PROV-INC-DFLT
           END-IF
           IF (SF)-IMP-AZ-NULL = HIGH-VALUES
              MOVE (SF)-IMP-AZ-NULL
              TO DAD-IMP-AZ-NULL
           ELSE
              MOVE (SF)-IMP-AZ
              TO DAD-IMP-AZ
           END-IF
           IF (SF)-IMP-ADER-NULL = HIGH-VALUES
              MOVE (SF)-IMP-ADER-NULL
              TO DAD-IMP-ADER-NULL
           ELSE
              MOVE (SF)-IMP-ADER
              TO DAD-IMP-ADER
           END-IF
           IF (SF)-IMP-TFR-NULL = HIGH-VALUES
              MOVE (SF)-IMP-TFR-NULL
              TO DAD-IMP-TFR-NULL
           ELSE
              MOVE (SF)-IMP-TFR
              TO DAD-IMP-TFR
           END-IF
           IF (SF)-IMP-VOLO-NULL = HIGH-VALUES
              MOVE (SF)-IMP-VOLO-NULL
              TO DAD-IMP-VOLO-NULL
           ELSE
              MOVE (SF)-IMP-VOLO
              TO DAD-IMP-VOLO
           END-IF
           IF (SF)-PC-AZ-NULL = HIGH-VALUES
              MOVE (SF)-PC-AZ-NULL
              TO DAD-PC-AZ-NULL
           ELSE
              MOVE (SF)-PC-AZ
              TO DAD-PC-AZ
           END-IF
           IF (SF)-PC-ADER-NULL = HIGH-VALUES
              MOVE (SF)-PC-ADER-NULL
              TO DAD-PC-ADER-NULL
           ELSE
              MOVE (SF)-PC-ADER
              TO DAD-PC-ADER
           END-IF
           IF (SF)-PC-TFR-NULL = HIGH-VALUES
              MOVE (SF)-PC-TFR-NULL
              TO DAD-PC-TFR-NULL
           ELSE
              MOVE (SF)-PC-TFR
              TO DAD-PC-TFR
           END-IF
           IF (SF)-PC-VOLO-NULL = HIGH-VALUES
              MOVE (SF)-PC-VOLO-NULL
              TO DAD-PC-VOLO-NULL
           ELSE
              MOVE (SF)-PC-VOLO
              TO DAD-PC-VOLO
           END-IF
           IF (SF)-TP-FNT-CNBTVA-NULL = HIGH-VALUES
              MOVE (SF)-TP-FNT-CNBTVA-NULL
              TO DAD-TP-FNT-CNBTVA-NULL
           ELSE
              MOVE (SF)-TP-FNT-CNBTVA
              TO DAD-TP-FNT-CNBTVA
           END-IF
           IF (SF)-IMP-PRE-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-IMP-PRE-DFLT-NULL
              TO DAD-IMP-PRE-DFLT-NULL
           ELSE
              MOVE (SF)-IMP-PRE-DFLT
              TO DAD-IMP-PRE-DFLT
           END-IF
           MOVE (SF)-COD-COMP-ANIA
              TO DAD-COD-COMP-ANIA
           IF (SF)-TP-PRE-NULL = HIGH-VALUES
              MOVE (SF)-TP-PRE-NULL
              TO DAD-TP-PRE-NULL
           ELSE
              MOVE (SF)-TP-PRE
              TO DAD-TP-PRE
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO DAD-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO DAD-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO DAD-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO DAD-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO DAD-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO DAD-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO DAD-DS-STATO-ELAB.
       VAL-DCLGEN-DAD-EX.
           EXIT.

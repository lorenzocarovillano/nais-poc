      *------------------------------------------------------------*
      *    PORTAFOGLIO VITA - PROCESSO VENDITA                     *
      *    CHIAMATA  AL DISPATCHER PER OPRAZIONI DI SELECT O FETCH *
      *          E GESTIONE ERRORE ALL  (TAB.DATO FISCALE ADESIONE)*
      *     ULTIMO AGG.  07 FEB 2007                               *
      *------------------------------------------------------------*
       LETTURA-DFA.

           IF IDSI0011-SELECT
              PERFORM SELECT-DFA
                THRU SELECT-DFA-EX
           ELSE
              PERFORM FETCH-DFA
                THRU FETCH-DFA-EX
           END-IF.

       LETTURA-DFA-EX.
           EXIT.
      *----------------------------------------------------------------*
      *                         SELECT                                 *
      *----------------------------------------------------------------*
       SELECT-DFA.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE

                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI
                         TO D-FISC-ADES
                       PERFORM VALORIZZA-OUTPUT-DFA
                         THRU VALORIZZA-OUTPUT-DFA-EX

                  WHEN IDSO0011-NOT-FOUND
      *--->       CHIAVE NON TROVATA
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'SELECT-DFA'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005019'
                         TO IEAI9901-COD-ERRORE
                       MOVE SPACES
                         TO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                       MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'SELECT-DFA'  TO IEAI9901-LABEL-ERR
                       MOVE '005016'      TO IEAI9901-COD-ERRORE
                       STRING 'D-FISC-ADES'      ';'
                            IDSO0011-RETURN-CODE ';'
                            IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SELECT-DFA'  TO IEAI9901-LABEL-ERR
              MOVE '005016'      TO IEAI9901-COD-ERRORE
              STRING 'D-FISC-ADES'      ';'
                   IDSO0011-RETURN-CODE ';'
                   IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       SELECT-DFA-EX.
           EXIT.
      *----------------------------------------------------------------*
      *                         FETCH                                  *
      *----------------------------------------------------------------*
       FETCH-DFA.

           SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
           SET  WCOM-OVERFLOW-NO                  TO TRUE.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR WCOM-OVERFLOW-YES

               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE

                      WHEN IDSO0011-NOT-FOUND
      *-->                 CHIAVE NON TROVATA
                           IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM
                                TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'FETCH-DFA'
                                TO IEAI9901-LABEL-ERR
                              MOVE '005019'
                                TO IEAI9901-COD-ERRORE
                              MOVE SPACES
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                           END-IF

                      WHEN IDSO0011-SUCCESSFUL-SQL
      *-->                 OPERAZIONE ESEGUITA CORRETTAMENTE
                           MOVE IDSO0011-BUFFER-DATI  TO D-FISC-ADES
                           ADD  1                     TO IX-TAB-DFA

      *-->  Se il contatore di lettura � maggiore dell' elemento MAX
      *-->  previsto per la TAB.DATI FISCALI ADESIONE allora siamo
      *-->  in overflow
                           IF IX-TAB-DFA > (SF)-ELE-DFA-MAX
                              SET WCOM-OVERFLOW-YES   TO TRUE
                           ELSE
                              PERFORM VALORIZZA-OUTPUT-DFA
                                 THRU VALORIZZA-OUTPUT-DFA-EX
                              SET IDSI0011-FETCH-NEXT TO TRUE
                           END-IF

                      WHEN OTHER
      *-->                 ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'SELECT-DFA' TO IEAI9901-LABEL-ERR
                           MOVE '005016'     TO IEAI9901-COD-ERRORE
                           STRING 'D-FISC-ADES'      ';'
                                IDSO0011-RETURN-CODE ';'
                                IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300

                  END-EVALUATE
               ELSE
      *-->        ERRORE DISPATCHER
                  MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'FETCH-DFA'   TO IEAI9901-LABEL-ERR
                  MOVE '005016'      TO IEAI9901-COD-ERRORE
                  STRING 'D-FISC-ADES'      ';'
                       IDSO0011-RETURN-CODE ';'
                       IDSO0011-SQLCODE
                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

       FETCH-DFA-EX.
           EXIT.

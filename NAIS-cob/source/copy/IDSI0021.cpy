      *************************************************************
      *                             Area Input
      *                  Data Mapper - Lettura Struttura Dato
      *************************************************************
        02 IDSI0021-AREA.
          05 IDSI0021-TIPO-MOVIMENTO              PIC S9(005) COMP-3.
          05 IDSI0021-MODALITA-ESECUTIVA          PIC X.
             88 IDSI0021-ON-LINE                 VALUE 'O'.
             88 IDSI0021-BATCH                   VALUE 'B'.

          05 IDSI0021-OPER.
      * -- CAMPI OPERAZIONE
      *    COPY IDSV0008 REPLACING ==(DS)== BY ==IDSI0021==.
           10 IDSI0021-OPERAZIONE                  PIC X(015).
               88 IDSI0021-SELECT               VALUE 'SELECT'
                                                      'SE'.
               88 IDSI0021-AGGIORNAMENTO-STORICO VALUE 'AGG_STORICO'.
               88 IDSI0021-INSERT               VALUE 'INSERT'
                                                      'IN'.
               88 IDSI0021-UPDATE               VALUE 'UPDATE'
                                                      'UP'.
               88 IDSI0021-DELETE               VALUE 'DELETE'
                                                      'DF'.
               88 IDSI0021-DELETE-LOGICA        VALUE 'DELETE_LOGICA'
                                                      'DL'.
               88 IDSI0021-OPEN-CURSOR          VALUE 'OPEN_CURSOR'
                                                      'OP'.
               88 IDSI0021-CLOSE-CURSOR         VALUE 'CLOSE_CURSOR'
                                                      'CL'.
               88 IDSI0021-FETCH-FIRST          VALUE 'FF'.
               88 IDSI0021-FETCH-NEXT           VALUE 'FETCH'
                                                      'FN'.
               88 IDSI0021-FETCH-FIRST-MULTIPLE VALUE 'FFM'.
               88 IDSI0021-FETCH-NEXT-MULTIPLE  VALUE 'FNM'.
               88 IDSI0021-PRESA-IN-CARICO  VALUE 'PRESA_IN_CARICO'.
      *     fine COPY IDSV0008

          05 IDSI0021-IDENTITA-CHIAMANTE          PIC X(003).
             88 IDSI0021-PTF-NEWLIFE              VALUE 'LPF'.
             88 IDSI0021-REFACTORING              VALUE 'REF'.
             88 IDSI0021-CONVERSIONE              VALUE 'CON'.

          05 IDSI0021-CODICE-COMPAGNIA-ANIA       PIC 9(05).

          05 IDSI0021-CODICE-STR-DATO             PIC X(030).

          05 IDSI0021-INITIALIZE               PIC X(01).
             88 IDSI0021-INITIALIZE-SI         VALUE 'S'.
             88 IDSI0021-INITIALIZE-NO         VALUE 'N'.




      ******************************************************************
      *   Copy Servizio Dati Prodotto
      ******************************************************************
          02 ISPC0040-DATI-INPUT.
             05 ISPC0040-COD-COMPAGNIA                  PIC 9(005).
             05 ISPC0040-COD-PRODOTTO                   PIC X(012).
             05 ISPC0040-COD-CONVENZIONE                PIC X(012).
             05 ISPC0040-DATA-INIZ-VALID-CONV           PIC X(008).
             05 ISPC0040-DATA-RIFERIMENTO               PIC X(008).
NEW          05 ISPC0040-DATA-EMISSIONE                 PIC X(008).
NEW          05 ISPC0040-DATA-PROPOSTA                  PIC X(008).
             05 ISPC0040-LIVELLO-UTENTE                 PIC 9(002).
             05 ISPC0040-FUNZIONALITA                   PIC 9(005).
             05 ISPC0040-SESSION-ID                     PIC X(020).
          02 ISPC0040-DATI-OUTPUT.
             03 ISPC0040-DATI-OUTPUT1.
                05 ISPC0040-PRODOTTO                       PIC X(012).
                05 ISPC0040-DATA-VERSIONE-PROD             PIC X(008).
                05 ISPC0040-TIPO-VERSIONE-PROD             PIC X(002).
                05 ISPC0040-CODICE-DIVISA                  PIC X(006).
                05 ISPC0040-DESCR-DIVISA                   PIC X(030).
      *--       Tipo Polizza
                05 ISPC0040-TIPO-POLIZZA                   PIC X(002).
                   88 ISPC0040-COLL-TFR                      VALUE '01'.
                   88 ISPC0040-COLL-PREV                     VALUE '02'.
                   88 ISPC0040-COLL-GRUPPO                   VALUE '03'.
                   88 ISPC0040-INDIV-MORTE                   VALUE '04'.
                   88 ISPC0040-INDIV-VITA                    VALUE '05'.
                   88 ISPC0040-INDIV-MISTA                   VALUE '06'.
                   88 ISPC0040-INDIV-CAPITAL                 VALUE '07'.
                   88 ISPC0040-INDIV-FINANZI                 VALUE '08'.
                   88 ISPC0040-INDIV-MULTI-GAR               VALUE '09'.
                   88 ISPC0040-INDIV-PIP                     VALUE '10'.
                   88 ISPC0040-INDIV-FIP                     VALUE '11'.
                05 ISPC0040-DESC-POLIZZA                   PIC X(030).
      *--       Tipo Trasformazione
                05 ISPC0040-TP-TRASF-NUM-ELE               PIC 9(003).
                05 ISPC0040-TIPO-TRASFORMAZIONE     OCCURS 3.
                   07 ISPC0040-COD-TP-TRASF                PIC X(002).
                      88 ISPC0040-TRASF-ABBUONO              VALUE 'AB'.
                      88 ISPC0040-TRASF-PREM-UNICO           VALUE 'PU'.
                      88 ISPC0040-TRASF-COMMERCIALE          VALUE 'TC'.
                      88 ISPC0040-TRASF-NON-AMMESSA          VALUE 'NA'.
                   07 ISPC0040-DESCR-TP-TRASF              PIC X(030).
      *--       Tipo Adesione
                05 ISPC0040-TP-ADE-NUM-ELE                 PIC 9(003).
                05 ISPC0040-TIPO-ADESIONE           OCCURS 3.
                   07 ISPC0040-CODICE-ADESIONE             PIC X(002).
                      88 ISPC0040-ADES-FACOLTIVA             VALUE 'AF'.
                      88 ISPC0040-ADES-OBBLIGATORIA          VALUE 'AO'.
                      88 ISPC0040-ADES-NON-AMMESSA           VALUE 'NA'.
                   07 ISPC0040-DESCR-ADESIONE              PIC X(030).
      *--       Modalita di calcolo
                05 ISPC0040-NUM-ELE-MOD-CAL-DUR            PIC 9(003).
                05 ISPC0040-MOD-CALC-DUR              OCCURS 3.
                   07 ISPC0040-CODICE-MOD                 PIC X(001).
                      88 ISPC0040-ETA-SCADENZA              VALUE '1'.
                      88 ISPC0040-DATA-FISSA                VALUE '2'.
                      88 ISPC0040-LIBERA                    VALUE '3'.
                   07 ISPC0040-DESCR-MOD                  PIC X(020).
      *--       Rischio Comune
                05 ISPC0040-FLAG-RISCHIO-COMUNE            PIC X(001).
                   88 ISPC0040-RISCHIO-SI                    VALUE '1'.
                   88 ISPC0040-RISCHIO-NO                    VALUE '0'.
      *--       Differimento Proroga
                05 ISPC0040-NUM-ELE-DIFF-PRO               PIC 9(003).
                05 ISPC0040-DIFFERIMENTO-PROROGA OCCURS 2.
                   07 ISPC0040-COD-DIFF-PROG               PIC X(02).
                      88 ISPC0040-DIFFERIMENTO               VALUE '05'.
                      88 ISPC0040-PROROGA                    VALUE '06'.
                   07 ISPC0040-DESC-DIFF-PROG              PIC X(30).
                05 ISPC0040-ANNI-DIFFER-PROR               PIC 9(02).
                05 ISPC0040-TRATT-AA-DIFF-PROR             PIC X(01).
      *--       Modalita durata Adesione
                05 ISPC0040-NUM-ELE-MOD-DUR                PIC 9(003).
                05 ISPC0040-MODALITA-DURATA   OCCURS 3.
                   07 ISPC0040-COD-DURATA                  PIC X(002).
                      88 ISPC0040-ANNI-INTERI                VALUE 'AA'.
                      88 ISPC0040-ANNI-MESI                  VALUE 'AM'.
                      88 ISPC0040-GIORNI                     VALUE 'GG'.
                   07 ISPC0040-DESCR-DURATA                PIC X(030).
      *--       Dati Default Prodotto
                05 ISPC0040-ETA-SCAD-MASCHI-DFLT           PIC 9(005).
                05 ISPC0040-TRATT-ETA-SCAD-MASCHI          PIC X(001).
                05 ISPC0040-ETA-SCAD-FEMMINE-DFLT          PIC 9(005).
                05 ISPC0040-TRATT-ETA-SCAD-FEMM            PIC X(001).
                05 ISPC0040-DATA-SCADENZA-DFLT             PIC X(008).
                05 ISPC0040-TRATT-DATA-SCAD-FEMM           PIC X(001).
                05 ISPC0040-DURATA-ANNI-DEFAULT            PIC 9(005).
                05 ISPC0040-TRATT-DURATA-ANNI              PIC X(001).
                05 ISPC0040-DURATA-MESI-DEFAULT            PIC 9(005).
                05 ISPC0040-TRATT-DURATA-MESI              PIC X(001).
                05 ISPC0040-DURATA-GIORNI-DEFAULT          PIC 9(005).
                05 ISPC0040-TRATT-DURATA-GIORNI            PIC X(001).
                05 ISPC0040-IND-MAX-FRAZ                   PIC 9(003).
      *--       Frazionamento
                05 ISPC0040-FRAZIONAMENTO     OCCURS 7.
                   07 ISPC0040-COD-FRAZ                    PIC X(002).
                   07 ISPC0040-DESC-FRAZ                   PIC X(030).
                05 ISPC0040-TRATTAMENTO-FRAZ               PIC X(001).
      *--       Sconto
                05 ISPC0040-IND-MAX-IMP-SCO                PIC 9(003).
                05 ISPC0040-IMP-SCONTO        OCCURS 2.
                   07 ISPC0040-COD-IMP-SCO                 PIC X(002).
                   07 ISPC0040-DESC-IMP-SCO                PIC X(030).
                05 ISPC0040-TRATTAMENTO-IMP-SCO            PIC X(001).
      *--       Lista Fondi
                05 ISPC0040-IND-MAX-FONDI                  PIC 9(003).
             03 ISPC0040-TAB-FONDI.
                05 ISPC0040-FONDI             OCCURS 200.
                   07 ISPC0040-COD-FONDI                   PIC X(012).
                   07 ISPC0040-DESC-FOND                   PIC X(080).
             03 ISPC0040-TAB-FONDI-R REDEFINES ISPC0040-TAB-FONDI.
                05 FILLER                                  PIC X(92).
                05 ISPC0040-RESTO-TAB-FONDI                PIC X(18308).
             03 ISPC0040-DATI-OUTPUT2.
                05 ISPC0040-TRATTAMENTO-FONDO              PIC X(001).
      *--       Copertura
                05 ISPC0040-FLAG-COPERTURA-FIN             PIC X(001).
                   88 ISPC0040-COP-FIN-SI                    VALUE '1'.
                   88 ISPC0040-COP-FIN-NO                    VALUE '0'.
      *--       Adesione senza testa assicurata
                05 ISPC0040-ADES-SENZA-ASSIC               PIC X(001).
                   88 ISPC0040-SENZA-ASSIC-SI                VALUE '1'.
                   88 ISPC0040-SENZA-ASSIC-NO                VALUE '0'.
      *--       Flag Cumulo Contraente
                05 ISPC0040-FLAG-CUMULO-CONTR              PIC 9(001).
      *--       Diritti Emissione
                05 ISPC0040-DIRITTI-EMISSIONE.
                   07 ISPC0040-FLAG-TRATT-DIR-EMIS         PIC X(001).
                   07 ISPC0040-IMP-DIR-EMIS             PIC S9(15)V9(3).
      *--       Diritti Quietanza
                05 ISPC0040-DIRITTI-QUIETANZA.
                   07 ISPC0040-FLAG-TRATT-DIR-QUIE         PIC X(001).
                   07 ISPC0040-IMP-DIR-QUIE             PIC S9(15)V9(3).
      *--       Diritti Primo Versamneto
                05 ISPC0040-DIRITTI-PRIMO-VERS.
                  07 ISPC0040-FLAG-TRATT-DIR-1-VER         PIC X(001).
                  07 ISPC0040-IMP-DIR-1-VER             PIC S9(15)V9(3).
      *--       Diritti Versamento Aggiuntivo
                05 ISPC0040-DIRITTI-VERS-AGG.
                  07 ISPC0040-FLAG-TRATT-VERS-AGG          PIC X(001).
                  07 ISPC0040-IMP-DIR-VERS-AGG          PIC S9(15)V9(3).
      *--       Sepese Mediche
                05 ISPC0040-SPESE-MEDICHE.
                  07 ISPC0040-FLAG-SPESE-MEDICHE           PIC X(001).
                  07 ISPC0040-IMP-SPESE-MEDICHE         PIC S9(15)V9(3).
      *--       Tipo Premio
                05 ISPC0040-TP-PREMIO-NUM-E                PIC 9(003).
                05 ISPC0040-TIPO-PREMIO             OCCURS 3.
                   07 ISPC0040-COD-TP-PREMIO               PIC X(001).
                      88 ISPC0040-TP-PRE-TARIFFA             VALUE 'T'.
                      88 ISPC0040-TP-PRE-INVENTARIO          VALUE 'I'.
                      88 ISPC0040-TP-PRE-PURO                VALUE 'P'.
                   07 ISPC0040-DESCR-TP-PREMIO             PIC X(030).
                05 ISPC0040-TRATT-TIPO-PREMIO              PIC X(001).
      *--       Distinta contributiva
                05 ISPC0040-DISTINTA-CONTR                 PIC X(001).
                   88 ISPC0040-DISTINTA-CONTR-NO             VALUE '0'.
                   88 ISPC0040-DISTINTA-CONTR-SI             VALUE '1'.
      *--       Data Decorrenza ricalcolata
NEW             05 ISPC0040-DATA-DECOR                     PIC X(008).
NEW             05 ISPC0040-DATA-DECOR-TRATT               PIC X(001).
      *--       Numero giorni da reinvestimento
                05 ISPC0040-NUM-GG-REINVEST                PIC 9(005).
      *--       Tabella Reinvestimento
                05 ISPC0040-MAX-OPER-FORM-INVEST           PIC 9(003).
                05 ISPC0040-OPER-FORMA-REINVEST     OCCURS 40.
                   07 ISPC0040-COD-OPER-REINV              PIC 9(4).
                   07 ISPC0040-COD-FORMA-REINV             PIC 9(4).
                   07 ISPC0040-PERC-REINV                 PIC 9(5)V9(9).
      *--       Sottocategoria
                05 ISPC0040-SOTTOCATEGORIA                 PIC X(012).
ITREG        03 ISPC0040-OUT-CPI-PROTECTION.
ITREG *-- ammissibilit� contraente residente all'estero (dominio 1/0)
ITREG           05 ISPC0040-AMM-CONTR-RES-EST           PIC X(01).
ITREG *-- flag che indica se il prodotto � Cpi&Protection  (dominio 1/0)
ITREG           05 ISPC0040-FL-CPI-PROTECTION           PIC X(01).
ITREG *-- flag che indica se il prodotto � Bundling  (dominio 1/0)
ITREG           05 ISPC0040-FL-BUNDLING                 PIC X(01).
ITREG           05 ISPC0040-FL-BUNDLING-TRATT           PIC X(01).
ITREG *-- flag che indica se il prodotto � un prodotto principale o un
ITREG *-- collegato (dominio P=Principale/C=Collegato)
ITREG           05 ISPC0040-FL-PRINC-COLL               PIC X(01).
CPIF3           05 ISPC0040-TRATT-PACCHETTO             PIC X(001).
CPIF3           05 ISPC0040-ELE-MAX-PACCHETTO           PIC 9(003).
CPIF3           05 ISPC0040-DATI-PACCHETTO          OCCURS 10.
CPIF3              07 ISPC0040-COD-PACCHETTO            PIC X(40).
CPIF3              07 ISPC0040-DESC-PACCHETTO           PIC X(40).
      *-- flag che indica se il prodotto prevede obbligatoriamente
      *-- un assicurato coincidente con il contraente (dominio 1/0)
                05 ISPC0040-FL-CONT-COINC-ASST          PIC X(01).

          02 ISPC0040-AREA-ERRORI.
             03 ISPC0040-ESITO                          PIC 9(02).
             03 ISPC0040-ERR-NUM-ELE                    PIC 9(03).
             03 ISPC0040-TAB-ERRORI OCCURS 10.
                05 ISPC0040-COD-ERR                     PIC X(12).
                05 ISPC0040-DESCRIZIONE-ERR             PIC X(50).
                05 ISPC0040-GRAVITA-ERR                 PIC 9(02).
                05 ISPC0040-LIVELLO-DEROGA              PIC 9(02).


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVD033
      *   ULTIMO AGG. 16 MAR 2009
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-D03.
           MOVE D03-COD-COMPAGNIA-ANIA
             TO (SF)-COD-COMPAGNIA-ANIA
           MOVE D03-FORMA-ASSICURATIVA
             TO (SF)-FORMA-ASSICURATIVA
           MOVE D03-COD-OGGETTO
             TO (SF)-COD-OGGETTO
           MOVE D03-KEY-BUSINESS
             TO (SF)-KEY-BUSINESS
           MOVE D03-ULT-PROGR
             TO (SF)-ULT-PROGR
           IF D03-PROGR-INIZIALE-NULL = HIGH-VALUES
              MOVE D03-PROGR-INIZIALE-NULL
                TO (SF)-PROGR-INIZIALE-NULL
           ELSE
              MOVE D03-PROGR-INIZIALE
                TO (SF)-PROGR-INIZIALE
           END-IF
           IF D03-PROGR-FINALE-NULL = HIGH-VALUES
              MOVE D03-PROGR-FINALE-NULL
                TO (SF)-PROGR-FINALE-NULL
           ELSE
              MOVE D03-PROGR-FINALE
                TO (SF)-PROGR-FINALE
           END-IF.
       VALORIZZA-OUTPUT-D03-EX.
           EXIT.

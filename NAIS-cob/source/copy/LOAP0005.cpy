      *------------------------------------------------------------
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI ERRORE DEROGA
      *   CREAZIONE 31-GEN-2008
      *------------------------------------------------------------

       INIZIA-AREA-ERR-DEROGA.

           PERFORM VARYING (SF)-NUM-ELE-MOT-DEROGA FROM 1 BY 1
                     UNTIL (SF)-NUM-ELE-MOT-DEROGA > 20
              MOVE ZEROES
                TO (SF)-ID-MOT-DEROGA ((SF)-NUM-ELE-MOT-DEROGA)
              MOVE SPACES
                TO (SF)-COD-ERR ((SF)-NUM-ELE-MOT-DEROGA)
              MOVE SPACES
                TO (SF)-DESCRIZIONE-ERR ((SF)-NUM-ELE-MOT-DEROGA)
              MOVE SPACES
                TO (SF)-TP-ERR ((SF)-NUM-ELE-MOT-DEROGA)
              MOVE SPACES
                TO (SF)-TP-MOT-DEROGA ((SF)-NUM-ELE-MOT-DEROGA)
              MOVE ZEROES
                TO (SF)-COD-LIV-AUTORIZZATIVO((SF)-NUM-ELE-MOT-DEROGA)
              MOVE SPACES
                TO (SF)-IDC-FORZABILITA ((SF)-NUM-ELE-MOT-DEROGA)
              MOVE ZEROES
                TO (SF)-MOT-DER-DT-EFFETTO ((SF)-NUM-ELE-MOT-DEROGA)
              MOVE ZEROES
                TO (SF)-MOT-DER-DT-COMPETENZA ((SF)-NUM-ELE-MOT-DEROGA)
              MOVE ZEROES
                TO (SF)-MOT-DER-DS-VER ((SF)-NUM-ELE-MOT-DEROGA)
              MOVE ZEROES
                TO (SF)-MOT-DER-STEP-ELAB((SF)-NUM-ELE-MOT-DEROGA)
           END-PERFORM.

           MOVE ZEROES TO (SF)-NUM-ELE-MOT-DEROGA.

       INIZIA-AREA-ERR-DEROGA-EX.
           EXIT.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA D_FISC_ADES
      *   ALIAS DFA
      *   ULTIMO AGG. 22 APR 2010
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-D-FISC-ADES PIC S9(9)     COMP-3.
             07 (SF)-ID-ADES PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-TP-ISC-FND PIC X(2).
             07 (SF)-TP-ISC-FND-NULL REDEFINES
                (SF)-TP-ISC-FND   PIC X(2).
             07 (SF)-DT-ACCNS-RAPP-FND   PIC S9(8) COMP-3.
             07 (SF)-DT-ACCNS-RAPP-FND-NULL REDEFINES
                (SF)-DT-ACCNS-RAPP-FND   PIC X(5).
             07 (SF)-IMP-CNBT-AZ-K1 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CNBT-AZ-K1-NULL REDEFINES
                (SF)-IMP-CNBT-AZ-K1   PIC X(8).
             07 (SF)-IMP-CNBT-ISC-K1 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CNBT-ISC-K1-NULL REDEFINES
                (SF)-IMP-CNBT-ISC-K1   PIC X(8).
             07 (SF)-IMP-CNBT-TFR-K1 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CNBT-TFR-K1-NULL REDEFINES
                (SF)-IMP-CNBT-TFR-K1   PIC X(8).
             07 (SF)-IMP-CNBT-VOL-K1 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CNBT-VOL-K1-NULL REDEFINES
                (SF)-IMP-CNBT-VOL-K1   PIC X(8).
             07 (SF)-IMP-CNBT-AZ-K2 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CNBT-AZ-K2-NULL REDEFINES
                (SF)-IMP-CNBT-AZ-K2   PIC X(8).
             07 (SF)-IMP-CNBT-ISC-K2 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CNBT-ISC-K2-NULL REDEFINES
                (SF)-IMP-CNBT-ISC-K2   PIC X(8).
             07 (SF)-IMP-CNBT-TFR-K2 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CNBT-TFR-K2-NULL REDEFINES
                (SF)-IMP-CNBT-TFR-K2   PIC X(8).
             07 (SF)-IMP-CNBT-VOL-K2 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CNBT-VOL-K2-NULL REDEFINES
                (SF)-IMP-CNBT-VOL-K2   PIC X(8).
             07 (SF)-MATU-K1 PIC S9(12)V9(3) COMP-3.
             07 (SF)-MATU-K1-NULL REDEFINES
                (SF)-MATU-K1   PIC X(8).
             07 (SF)-MATU-RES-K1 PIC S9(12)V9(3) COMP-3.
             07 (SF)-MATU-RES-K1-NULL REDEFINES
                (SF)-MATU-RES-K1   PIC X(8).
             07 (SF)-MATU-K2 PIC S9(12)V9(3) COMP-3.
             07 (SF)-MATU-K2-NULL REDEFINES
                (SF)-MATU-K2   PIC X(8).
             07 (SF)-IMPB-VIS PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-NULL REDEFINES
                (SF)-IMPB-VIS   PIC X(8).
             07 (SF)-IMPST-VIS PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-NULL REDEFINES
                (SF)-IMPST-VIS   PIC X(8).
             07 (SF)-IMPB-IS-K2 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-K2-NULL REDEFINES
                (SF)-IMPB-IS-K2   PIC X(8).
             07 (SF)-IMPST-SOST-K2 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-SOST-K2-NULL REDEFINES
                (SF)-IMPST-SOST-K2   PIC X(8).
             07 (SF)-RIDZ-TFR PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIDZ-TFR-NULL REDEFINES
                (SF)-RIDZ-TFR   PIC X(8).
             07 (SF)-PC-TFR PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-TFR-NULL REDEFINES
                (SF)-PC-TFR   PIC X(4).
             07 (SF)-ALQ-TFR PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-TFR-NULL REDEFINES
                (SF)-ALQ-TFR   PIC X(4).
             07 (SF)-TOT-ANTIC PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-ANTIC-NULL REDEFINES
                (SF)-TOT-ANTIC   PIC X(8).
             07 (SF)-IMPB-TFR-ANTIC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-TFR-ANTIC-NULL REDEFINES
                (SF)-IMPB-TFR-ANTIC   PIC X(8).
             07 (SF)-IMPST-TFR-ANTIC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-TFR-ANTIC-NULL REDEFINES
                (SF)-IMPST-TFR-ANTIC   PIC X(8).
             07 (SF)-RIDZ-TFR-SU-ANTIC PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIDZ-TFR-SU-ANTIC-NULL REDEFINES
                (SF)-RIDZ-TFR-SU-ANTIC   PIC X(8).
             07 (SF)-IMPB-VIS-ANTIC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-ANTIC-NULL REDEFINES
                (SF)-IMPB-VIS-ANTIC   PIC X(8).
             07 (SF)-IMPST-VIS-ANTIC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-ANTIC-NULL REDEFINES
                (SF)-IMPST-VIS-ANTIC   PIC X(8).
             07 (SF)-IMPB-IS-K2-ANTIC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-K2-ANTIC-NULL REDEFINES
                (SF)-IMPB-IS-K2-ANTIC   PIC X(8).
             07 (SF)-IMPST-SOST-K2-ANTI PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-SOST-K2-ANTI-NULL REDEFINES
                (SF)-IMPST-SOST-K2-ANTI   PIC X(8).
             07 (SF)-ULT-COMMIS-TRASFE PIC S9(12)V9(3) COMP-3.
             07 (SF)-ULT-COMMIS-TRASFE-NULL REDEFINES
                (SF)-ULT-COMMIS-TRASFE   PIC X(8).
             07 (SF)-COD-DVS PIC X(20).
             07 (SF)-COD-DVS-NULL REDEFINES
                (SF)-COD-DVS   PIC X(20).
             07 (SF)-ALQ-PRVR PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-PRVR-NULL REDEFINES
                (SF)-ALQ-PRVR   PIC X(4).
             07 (SF)-PC-ESE-IMPST-TFR PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-ESE-IMPST-TFR-NULL REDEFINES
                (SF)-PC-ESE-IMPST-TFR   PIC X(4).
             07 (SF)-IMP-ESE-IMPST-TFR PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-ESE-IMPST-TFR-NULL REDEFINES
                (SF)-IMP-ESE-IMPST-TFR   PIC X(8).
             07 (SF)-ANZ-CNBTVA-CARASS PIC S9(4)     COMP-3.
             07 (SF)-ANZ-CNBTVA-CARASS-NULL REDEFINES
                (SF)-ANZ-CNBTVA-CARASS   PIC X(3).
             07 (SF)-ANZ-CNBTVA-CARAZI PIC S9(4)     COMP-3.
             07 (SF)-ANZ-CNBTVA-CARAZI-NULL REDEFINES
                (SF)-ANZ-CNBTVA-CARAZI   PIC X(3).
             07 (SF)-ANZ-SRVZ PIC S9(4)     COMP-3.
             07 (SF)-ANZ-SRVZ-NULL REDEFINES
                (SF)-ANZ-SRVZ   PIC X(3).
             07 (SF)-IMP-CNBT-NDED-K1 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CNBT-NDED-K1-NULL REDEFINES
                (SF)-IMP-CNBT-NDED-K1   PIC X(8).
             07 (SF)-IMP-CNBT-NDED-K2 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CNBT-NDED-K2-NULL REDEFINES
                (SF)-IMP-CNBT-NDED-K2   PIC X(8).
             07 (SF)-IMP-CNBT-NDED-K3 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CNBT-NDED-K3-NULL REDEFINES
                (SF)-IMP-CNBT-NDED-K3   PIC X(8).
             07 (SF)-IMP-CNBT-AZ-K3 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CNBT-AZ-K3-NULL REDEFINES
                (SF)-IMP-CNBT-AZ-K3   PIC X(8).
             07 (SF)-IMP-CNBT-ISC-K3 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CNBT-ISC-K3-NULL REDEFINES
                (SF)-IMP-CNBT-ISC-K3   PIC X(8).
             07 (SF)-IMP-CNBT-TFR-K3 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CNBT-TFR-K3-NULL REDEFINES
                (SF)-IMP-CNBT-TFR-K3   PIC X(8).
             07 (SF)-IMP-CNBT-VOL-K3 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CNBT-VOL-K3-NULL REDEFINES
                (SF)-IMP-CNBT-VOL-K3   PIC X(8).
             07 (SF)-MATU-K3 PIC S9(12)V9(3) COMP-3.
             07 (SF)-MATU-K3-NULL REDEFINES
                (SF)-MATU-K3   PIC X(8).
             07 (SF)-IMPB-252-ANTIC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-252-ANTIC-NULL REDEFINES
                (SF)-IMPB-252-ANTIC   PIC X(8).
             07 (SF)-IMPST-252-ANTIC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-252-ANTIC-NULL REDEFINES
                (SF)-IMPST-252-ANTIC   PIC X(8).
             07 (SF)-DT-1A-CNBZ   PIC S9(8) COMP-3.
             07 (SF)-DT-1A-CNBZ-NULL REDEFINES
                (SF)-DT-1A-CNBZ   PIC X(5).
             07 (SF)-COMMIS-DI-TRASFE PIC S9(12)V9(3) COMP-3.
             07 (SF)-COMMIS-DI-TRASFE-NULL REDEFINES
                (SF)-COMMIS-DI-TRASFE   PIC X(8).
             07 (SF)-AA-CNBZ-K1 PIC S9(4)     COMP-3.
             07 (SF)-AA-CNBZ-K1-NULL REDEFINES
                (SF)-AA-CNBZ-K1   PIC X(3).
             07 (SF)-AA-CNBZ-K2 PIC S9(4)     COMP-3.
             07 (SF)-AA-CNBZ-K2-NULL REDEFINES
                (SF)-AA-CNBZ-K2   PIC X(3).
             07 (SF)-AA-CNBZ-K3 PIC S9(4)     COMP-3.
             07 (SF)-AA-CNBZ-K3-NULL REDEFINES
                (SF)-AA-CNBZ-K3   PIC X(3).
             07 (SF)-MM-CNBZ-K1 PIC S9(4)     COMP-3.
             07 (SF)-MM-CNBZ-K1-NULL REDEFINES
                (SF)-MM-CNBZ-K1   PIC X(3).
             07 (SF)-MM-CNBZ-K2 PIC S9(4)     COMP-3.
             07 (SF)-MM-CNBZ-K2-NULL REDEFINES
                (SF)-MM-CNBZ-K2   PIC X(3).
             07 (SF)-MM-CNBZ-K3 PIC S9(4)     COMP-3.
             07 (SF)-MM-CNBZ-K3-NULL REDEFINES
                (SF)-MM-CNBZ-K3   PIC X(3).
             07 (SF)-FL-APPLZ-NEWFIS PIC X(1).
             07 (SF)-FL-APPLZ-NEWFIS-NULL REDEFINES
                (SF)-FL-APPLZ-NEWFIS   PIC X(1).
             07 (SF)-REDT-TASS-ABBAT-K3 PIC S9(12)V9(3) COMP-3.
             07 (SF)-REDT-TASS-ABBAT-K3-NULL REDEFINES
                (SF)-REDT-TASS-ABBAT-K3   PIC X(8).
             07 (SF)-CNBT-ECC-4X100-K1 PIC S9(12)V9(3) COMP-3.
             07 (SF)-CNBT-ECC-4X100-K1-NULL REDEFINES
                (SF)-CNBT-ECC-4X100-K1   PIC X(8).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-CREDITO-IS PIC S9(12)V9(3) COMP-3.
             07 (SF)-CREDITO-IS-NULL REDEFINES
                (SF)-CREDITO-IS   PIC X(8).
             07 (SF)-REDT-TASS-ABBAT-K2 PIC S9(12)V9(3) COMP-3.
             07 (SF)-REDT-TASS-ABBAT-K2-NULL REDEFINES
                (SF)-REDT-TASS-ABBAT-K2   PIC X(8).
             07 (SF)-IMPB-IS-K3 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-K3-NULL REDEFINES
                (SF)-IMPB-IS-K3   PIC X(8).
             07 (SF)-IMPST-SOST-K3 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-SOST-K3-NULL REDEFINES
                (SF)-IMPST-SOST-K3   PIC X(8).
             07 (SF)-IMPB-252-K3 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-252-K3-NULL REDEFINES
                (SF)-IMPB-252-K3   PIC X(8).
             07 (SF)-IMPST-252-K3 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-252-K3-NULL REDEFINES
                (SF)-IMPST-252-K3   PIC X(8).
             07 (SF)-IMPB-IS-K3-ANTIC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-K3-ANTIC-NULL REDEFINES
                (SF)-IMPB-IS-K3-ANTIC   PIC X(8).
             07 (SF)-IMPST-SOST-K3-ANTI PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-SOST-K3-ANTI-NULL REDEFINES
                (SF)-IMPST-SOST-K3-ANTI   PIC X(8).
             07 (SF)-IMPB-IRPEF-K1-ANTI PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IRPEF-K1-ANTI-NULL REDEFINES
                (SF)-IMPB-IRPEF-K1-ANTI   PIC X(8).
             07 (SF)-IMPST-IRPEF-K1-ANT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-IRPEF-K1-ANT-NULL REDEFINES
                (SF)-IMPST-IRPEF-K1-ANT   PIC X(8).
             07 (SF)-IMPB-IRPEF-K2-ANTI PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IRPEF-K2-ANTI-NULL REDEFINES
                (SF)-IMPB-IRPEF-K2-ANTI   PIC X(8).
             07 (SF)-IMPST-IRPEF-K2-ANT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-IRPEF-K2-ANT-NULL REDEFINES
                (SF)-IMPST-IRPEF-K2-ANT   PIC X(8).
             07 (SF)-DT-CESSAZIONE   PIC S9(8) COMP-3.
             07 (SF)-DT-CESSAZIONE-NULL REDEFINES
                (SF)-DT-CESSAZIONE   PIC X(5).
             07 (SF)-TOT-IMPST PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-IMPST-NULL REDEFINES
                (SF)-TOT-IMPST   PIC X(8).
             07 (SF)-ONER-TRASFE PIC S9(12)V9(3) COMP-3.
             07 (SF)-ONER-TRASFE-NULL REDEFINES
                (SF)-ONER-TRASFE   PIC X(8).
             07 (SF)-IMP-NET-TRASFERITO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-NET-TRASFERITO-NULL REDEFINES
                (SF)-IMP-NET-TRASFERITO   PIC X(8).

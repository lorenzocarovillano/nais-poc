
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVTGA3
      *   ULTIMO AGG. 03 GIU 2019
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-TGA.
           MOVE TGA-ID-TRCH-DI-GAR
             TO (SF)-ID-PTF(IX-TAB-TGA)
           MOVE TGA-ID-TRCH-DI-GAR
             TO (SF)-ID-TRCH-DI-GAR(IX-TAB-TGA)
           MOVE TGA-ID-GAR
             TO (SF)-ID-GAR(IX-TAB-TGA)
           MOVE TGA-ID-ADES
             TO (SF)-ID-ADES(IX-TAB-TGA)
           MOVE TGA-ID-POLI
             TO (SF)-ID-POLI(IX-TAB-TGA)
           MOVE TGA-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-TGA)
           IF TGA-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE TGA-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-TGA)
           END-IF
           MOVE TGA-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-TGA)
           MOVE TGA-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-TGA)
           MOVE TGA-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-TGA)
           MOVE TGA-DT-DECOR
             TO (SF)-DT-DECOR(IX-TAB-TGA)
           IF TGA-DT-SCAD-NULL = HIGH-VALUES
              MOVE TGA-DT-SCAD-NULL
                TO (SF)-DT-SCAD-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-DT-SCAD
                TO (SF)-DT-SCAD(IX-TAB-TGA)
           END-IF
           IF TGA-IB-OGG-NULL = HIGH-VALUES
              MOVE TGA-IB-OGG-NULL
                TO (SF)-IB-OGG-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IB-OGG
                TO (SF)-IB-OGG(IX-TAB-TGA)
           END-IF
           MOVE TGA-TP-RGM-FISC
             TO (SF)-TP-RGM-FISC(IX-TAB-TGA)
           IF TGA-DT-EMIS-NULL = HIGH-VALUES
              MOVE TGA-DT-EMIS-NULL
                TO (SF)-DT-EMIS-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-DT-EMIS
                TO (SF)-DT-EMIS(IX-TAB-TGA)
           END-IF
           MOVE TGA-TP-TRCH
             TO (SF)-TP-TRCH(IX-TAB-TGA)
           IF TGA-DUR-AA-NULL = HIGH-VALUES
              MOVE TGA-DUR-AA-NULL
                TO (SF)-DUR-AA-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-DUR-AA
                TO (SF)-DUR-AA(IX-TAB-TGA)
           END-IF
           IF TGA-DUR-MM-NULL = HIGH-VALUES
              MOVE TGA-DUR-MM-NULL
                TO (SF)-DUR-MM-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-DUR-MM
                TO (SF)-DUR-MM(IX-TAB-TGA)
           END-IF
           IF TGA-DUR-GG-NULL = HIGH-VALUES
              MOVE TGA-DUR-GG-NULL
                TO (SF)-DUR-GG-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-DUR-GG
                TO (SF)-DUR-GG(IX-TAB-TGA)
           END-IF
           IF TGA-PRE-CASO-MOR-NULL = HIGH-VALUES
              MOVE TGA-PRE-CASO-MOR-NULL
                TO (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRE-CASO-MOR
                TO (SF)-PRE-CASO-MOR(IX-TAB-TGA)
           END-IF
           IF TGA-PC-INTR-RIAT-NULL = HIGH-VALUES
              MOVE TGA-PC-INTR-RIAT-NULL
                TO (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PC-INTR-RIAT
                TO (SF)-PC-INTR-RIAT(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-BNS-ANTIC-NULL = HIGH-VALUES
              MOVE TGA-IMP-BNS-ANTIC-NULL
                TO (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-BNS-ANTIC
                TO (SF)-IMP-BNS-ANTIC(IX-TAB-TGA)
           END-IF
           IF TGA-PRE-INI-NET-NULL = HIGH-VALUES
              MOVE TGA-PRE-INI-NET-NULL
                TO (SF)-PRE-INI-NET-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRE-INI-NET
                TO (SF)-PRE-INI-NET(IX-TAB-TGA)
           END-IF
           IF TGA-PRE-PP-INI-NULL = HIGH-VALUES
              MOVE TGA-PRE-PP-INI-NULL
                TO (SF)-PRE-PP-INI-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRE-PP-INI
                TO (SF)-PRE-PP-INI(IX-TAB-TGA)
           END-IF
           IF TGA-PRE-PP-ULT-NULL = HIGH-VALUES
              MOVE TGA-PRE-PP-ULT-NULL
                TO (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRE-PP-ULT
                TO (SF)-PRE-PP-ULT(IX-TAB-TGA)
           END-IF
           IF TGA-PRE-TARI-INI-NULL = HIGH-VALUES
              MOVE TGA-PRE-TARI-INI-NULL
                TO (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRE-TARI-INI
                TO (SF)-PRE-TARI-INI(IX-TAB-TGA)
           END-IF
           IF TGA-PRE-TARI-ULT-NULL = HIGH-VALUES
              MOVE TGA-PRE-TARI-ULT-NULL
                TO (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRE-TARI-ULT
                TO (SF)-PRE-TARI-ULT(IX-TAB-TGA)
           END-IF
           IF TGA-PRE-INVRIO-INI-NULL = HIGH-VALUES
              MOVE TGA-PRE-INVRIO-INI-NULL
                TO (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRE-INVRIO-INI
                TO (SF)-PRE-INVRIO-INI(IX-TAB-TGA)
           END-IF
           IF TGA-PRE-INVRIO-ULT-NULL = HIGH-VALUES
              MOVE TGA-PRE-INVRIO-ULT-NULL
                TO (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRE-INVRIO-ULT
                TO (SF)-PRE-INVRIO-ULT(IX-TAB-TGA)
           END-IF
           IF TGA-PRE-RIVTO-NULL = HIGH-VALUES
              MOVE TGA-PRE-RIVTO-NULL
                TO (SF)-PRE-RIVTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRE-RIVTO
                TO (SF)-PRE-RIVTO(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-SOPR-PROF-NULL = HIGH-VALUES
              MOVE TGA-IMP-SOPR-PROF-NULL
                TO (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-SOPR-PROF
                TO (SF)-IMP-SOPR-PROF(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-SOPR-SAN-NULL = HIGH-VALUES
              MOVE TGA-IMP-SOPR-SAN-NULL
                TO (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-SOPR-SAN
                TO (SF)-IMP-SOPR-SAN(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-SOPR-SPO-NULL = HIGH-VALUES
              MOVE TGA-IMP-SOPR-SPO-NULL
                TO (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-SOPR-SPO
                TO (SF)-IMP-SOPR-SPO(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-SOPR-TEC-NULL = HIGH-VALUES
              MOVE TGA-IMP-SOPR-TEC-NULL
                TO (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-SOPR-TEC
                TO (SF)-IMP-SOPR-TEC(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-ALT-SOPR-NULL = HIGH-VALUES
              MOVE TGA-IMP-ALT-SOPR-NULL
                TO (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-ALT-SOPR
                TO (SF)-IMP-ALT-SOPR(IX-TAB-TGA)
           END-IF
           IF TGA-PRE-STAB-NULL = HIGH-VALUES
              MOVE TGA-PRE-STAB-NULL
                TO (SF)-PRE-STAB-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRE-STAB
                TO (SF)-PRE-STAB(IX-TAB-TGA)
           END-IF
           IF TGA-DT-EFF-STAB-NULL = HIGH-VALUES
              MOVE TGA-DT-EFF-STAB-NULL
                TO (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-DT-EFF-STAB
                TO (SF)-DT-EFF-STAB(IX-TAB-TGA)
           END-IF
           IF TGA-TS-RIVAL-FIS-NULL = HIGH-VALUES
              MOVE TGA-TS-RIVAL-FIS-NULL
                TO (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-TS-RIVAL-FIS
                TO (SF)-TS-RIVAL-FIS(IX-TAB-TGA)
           END-IF
           IF TGA-TS-RIVAL-INDICIZ-NULL = HIGH-VALUES
              MOVE TGA-TS-RIVAL-INDICIZ-NULL
                TO (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-TS-RIVAL-INDICIZ
                TO (SF)-TS-RIVAL-INDICIZ(IX-TAB-TGA)
           END-IF
           IF TGA-OLD-TS-TEC-NULL = HIGH-VALUES
              MOVE TGA-OLD-TS-TEC-NULL
                TO (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-OLD-TS-TEC
                TO (SF)-OLD-TS-TEC(IX-TAB-TGA)
           END-IF
           IF TGA-RAT-LRD-NULL = HIGH-VALUES
              MOVE TGA-RAT-LRD-NULL
                TO (SF)-RAT-LRD-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-RAT-LRD
                TO (SF)-RAT-LRD(IX-TAB-TGA)
           END-IF
           IF TGA-PRE-LRD-NULL = HIGH-VALUES
              MOVE TGA-PRE-LRD-NULL
                TO (SF)-PRE-LRD-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRE-LRD
                TO (SF)-PRE-LRD(IX-TAB-TGA)
           END-IF
           IF TGA-PRSTZ-INI-NULL = HIGH-VALUES
              MOVE TGA-PRSTZ-INI-NULL
                TO (SF)-PRSTZ-INI-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRSTZ-INI
                TO (SF)-PRSTZ-INI(IX-TAB-TGA)
           END-IF
           IF TGA-PRSTZ-ULT-NULL = HIGH-VALUES
              MOVE TGA-PRSTZ-ULT-NULL
                TO (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRSTZ-ULT
                TO (SF)-PRSTZ-ULT(IX-TAB-TGA)
           END-IF
           IF TGA-CPT-IN-OPZ-RIVTO-NULL = HIGH-VALUES
              MOVE TGA-CPT-IN-OPZ-RIVTO-NULL
                TO (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-CPT-IN-OPZ-RIVTO
                TO (SF)-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
           END-IF
           IF TGA-PRSTZ-INI-STAB-NULL = HIGH-VALUES
              MOVE TGA-PRSTZ-INI-STAB-NULL
                TO (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRSTZ-INI-STAB
                TO (SF)-PRSTZ-INI-STAB(IX-TAB-TGA)
           END-IF
           IF TGA-CPT-RSH-MOR-NULL = HIGH-VALUES
              MOVE TGA-CPT-RSH-MOR-NULL
                TO (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-CPT-RSH-MOR
                TO (SF)-CPT-RSH-MOR(IX-TAB-TGA)
           END-IF
           IF TGA-PRSTZ-RID-INI-NULL = HIGH-VALUES
              MOVE TGA-PRSTZ-RID-INI-NULL
                TO (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRSTZ-RID-INI
                TO (SF)-PRSTZ-RID-INI(IX-TAB-TGA)
           END-IF
           IF TGA-FL-CAR-CONT-NULL = HIGH-VALUES
              MOVE TGA-FL-CAR-CONT-NULL
                TO (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-FL-CAR-CONT
                TO (SF)-FL-CAR-CONT(IX-TAB-TGA)
           END-IF
           IF TGA-BNS-GIA-LIQTO-NULL = HIGH-VALUES
              MOVE TGA-BNS-GIA-LIQTO-NULL
                TO (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-BNS-GIA-LIQTO
                TO (SF)-BNS-GIA-LIQTO(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-BNS-NULL = HIGH-VALUES
              MOVE TGA-IMP-BNS-NULL
                TO (SF)-IMP-BNS-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-BNS
                TO (SF)-IMP-BNS(IX-TAB-TGA)
           END-IF
           MOVE TGA-COD-DVS
             TO (SF)-COD-DVS(IX-TAB-TGA)
           IF TGA-PRSTZ-INI-NEWFIS-NULL = HIGH-VALUES
              MOVE TGA-PRSTZ-INI-NEWFIS-NULL
                TO (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRSTZ-INI-NEWFIS
                TO (SF)-PRSTZ-INI-NEWFIS(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-SCON-NULL = HIGH-VALUES
              MOVE TGA-IMP-SCON-NULL
                TO (SF)-IMP-SCON-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-SCON
                TO (SF)-IMP-SCON(IX-TAB-TGA)
           END-IF
           IF TGA-ALQ-SCON-NULL = HIGH-VALUES
              MOVE TGA-ALQ-SCON-NULL
                TO (SF)-ALQ-SCON-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ALQ-SCON
                TO (SF)-ALQ-SCON(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-CAR-ACQ-NULL = HIGH-VALUES
              MOVE TGA-IMP-CAR-ACQ-NULL
                TO (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-CAR-ACQ
                TO (SF)-IMP-CAR-ACQ(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-CAR-INC-NULL = HIGH-VALUES
              MOVE TGA-IMP-CAR-INC-NULL
                TO (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-CAR-INC
                TO (SF)-IMP-CAR-INC(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-CAR-GEST-NULL = HIGH-VALUES
              MOVE TGA-IMP-CAR-GEST-NULL
                TO (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-CAR-GEST
                TO (SF)-IMP-CAR-GEST(IX-TAB-TGA)
           END-IF
           IF TGA-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE TGA-ETA-AA-1O-ASSTO-NULL
                TO (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ETA-AA-1O-ASSTO
                TO (SF)-ETA-AA-1O-ASSTO(IX-TAB-TGA)
           END-IF
           IF TGA-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE TGA-ETA-MM-1O-ASSTO-NULL
                TO (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ETA-MM-1O-ASSTO
                TO (SF)-ETA-MM-1O-ASSTO(IX-TAB-TGA)
           END-IF
           IF TGA-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
              MOVE TGA-ETA-AA-2O-ASSTO-NULL
                TO (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ETA-AA-2O-ASSTO
                TO (SF)-ETA-AA-2O-ASSTO(IX-TAB-TGA)
           END-IF
           IF TGA-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
              MOVE TGA-ETA-MM-2O-ASSTO-NULL
                TO (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ETA-MM-2O-ASSTO
                TO (SF)-ETA-MM-2O-ASSTO(IX-TAB-TGA)
           END-IF
           IF TGA-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
              MOVE TGA-ETA-AA-3O-ASSTO-NULL
                TO (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ETA-AA-3O-ASSTO
                TO (SF)-ETA-AA-3O-ASSTO(IX-TAB-TGA)
           END-IF
           IF TGA-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
              MOVE TGA-ETA-MM-3O-ASSTO-NULL
                TO (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ETA-MM-3O-ASSTO
                TO (SF)-ETA-MM-3O-ASSTO(IX-TAB-TGA)
           END-IF
           IF TGA-RENDTO-LRD-NULL = HIGH-VALUES
              MOVE TGA-RENDTO-LRD-NULL
                TO (SF)-RENDTO-LRD-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-RENDTO-LRD
                TO (SF)-RENDTO-LRD(IX-TAB-TGA)
           END-IF
           IF TGA-PC-RETR-NULL = HIGH-VALUES
              MOVE TGA-PC-RETR-NULL
                TO (SF)-PC-RETR-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PC-RETR
                TO (SF)-PC-RETR(IX-TAB-TGA)
           END-IF
           IF TGA-RENDTO-RETR-NULL = HIGH-VALUES
              MOVE TGA-RENDTO-RETR-NULL
                TO (SF)-RENDTO-RETR-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-RENDTO-RETR
                TO (SF)-RENDTO-RETR(IX-TAB-TGA)
           END-IF
           IF TGA-MIN-GARTO-NULL = HIGH-VALUES
              MOVE TGA-MIN-GARTO-NULL
                TO (SF)-MIN-GARTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-MIN-GARTO
                TO (SF)-MIN-GARTO(IX-TAB-TGA)
           END-IF
           IF TGA-MIN-TRNUT-NULL = HIGH-VALUES
              MOVE TGA-MIN-TRNUT-NULL
                TO (SF)-MIN-TRNUT-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-MIN-TRNUT
                TO (SF)-MIN-TRNUT(IX-TAB-TGA)
           END-IF
           IF TGA-PRE-ATT-DI-TRCH-NULL = HIGH-VALUES
              MOVE TGA-PRE-ATT-DI-TRCH-NULL
                TO (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRE-ATT-DI-TRCH
                TO (SF)-PRE-ATT-DI-TRCH(IX-TAB-TGA)
           END-IF
           IF TGA-MATU-END2000-NULL = HIGH-VALUES
              MOVE TGA-MATU-END2000-NULL
                TO (SF)-MATU-END2000-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-MATU-END2000
                TO (SF)-MATU-END2000(IX-TAB-TGA)
           END-IF
           IF TGA-ABB-TOT-INI-NULL = HIGH-VALUES
              MOVE TGA-ABB-TOT-INI-NULL
                TO (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ABB-TOT-INI
                TO (SF)-ABB-TOT-INI(IX-TAB-TGA)
           END-IF
           IF TGA-ABB-TOT-ULT-NULL = HIGH-VALUES
              MOVE TGA-ABB-TOT-ULT-NULL
                TO (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ABB-TOT-ULT
                TO (SF)-ABB-TOT-ULT(IX-TAB-TGA)
           END-IF
           IF TGA-ABB-ANNU-ULT-NULL = HIGH-VALUES
              MOVE TGA-ABB-ANNU-ULT-NULL
                TO (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ABB-ANNU-ULT
                TO (SF)-ABB-ANNU-ULT(IX-TAB-TGA)
           END-IF
           IF TGA-DUR-ABB-NULL = HIGH-VALUES
              MOVE TGA-DUR-ABB-NULL
                TO (SF)-DUR-ABB-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-DUR-ABB
                TO (SF)-DUR-ABB(IX-TAB-TGA)
           END-IF
           IF TGA-TP-ADEG-ABB-NULL = HIGH-VALUES
              MOVE TGA-TP-ADEG-ABB-NULL
                TO (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-TP-ADEG-ABB
                TO (SF)-TP-ADEG-ABB(IX-TAB-TGA)
           END-IF
           IF TGA-MOD-CALC-NULL = HIGH-VALUES
              MOVE TGA-MOD-CALC-NULL
                TO (SF)-MOD-CALC-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-MOD-CALC
                TO (SF)-MOD-CALC(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-AZ-NULL = HIGH-VALUES
              MOVE TGA-IMP-AZ-NULL
                TO (SF)-IMP-AZ-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-AZ
                TO (SF)-IMP-AZ(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-ADER-NULL = HIGH-VALUES
              MOVE TGA-IMP-ADER-NULL
                TO (SF)-IMP-ADER-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-ADER
                TO (SF)-IMP-ADER(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-TFR-NULL = HIGH-VALUES
              MOVE TGA-IMP-TFR-NULL
                TO (SF)-IMP-TFR-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-TFR
                TO (SF)-IMP-TFR(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-VOLO-NULL = HIGH-VALUES
              MOVE TGA-IMP-VOLO-NULL
                TO (SF)-IMP-VOLO-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-VOLO
                TO (SF)-IMP-VOLO(IX-TAB-TGA)
           END-IF
           IF TGA-VIS-END2000-NULL = HIGH-VALUES
              MOVE TGA-VIS-END2000-NULL
                TO (SF)-VIS-END2000-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-VIS-END2000
                TO (SF)-VIS-END2000(IX-TAB-TGA)
           END-IF
           IF TGA-DT-VLDT-PROD-NULL = HIGH-VALUES
              MOVE TGA-DT-VLDT-PROD-NULL
                TO (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-DT-VLDT-PROD
                TO (SF)-DT-VLDT-PROD(IX-TAB-TGA)
           END-IF
           IF TGA-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE TGA-DT-INI-VAL-TAR-NULL
                TO (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-DT-INI-VAL-TAR
                TO (SF)-DT-INI-VAL-TAR(IX-TAB-TGA)
           END-IF
           IF TGA-IMPB-VIS-END2000-NULL = HIGH-VALUES
              MOVE TGA-IMPB-VIS-END2000-NULL
                TO (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMPB-VIS-END2000
                TO (SF)-IMPB-VIS-END2000(IX-TAB-TGA)
           END-IF
           IF TGA-REN-INI-TS-TEC-0-NULL = HIGH-VALUES
              MOVE TGA-REN-INI-TS-TEC-0-NULL
                TO (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-REN-INI-TS-TEC-0
                TO (SF)-REN-INI-TS-TEC-0(IX-TAB-TGA)
           END-IF
           IF TGA-PC-RIP-PRE-NULL = HIGH-VALUES
              MOVE TGA-PC-RIP-PRE-NULL
                TO (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PC-RIP-PRE
                TO (SF)-PC-RIP-PRE(IX-TAB-TGA)
           END-IF
           IF TGA-FL-IMPORTI-FORZ-NULL = HIGH-VALUES
              MOVE TGA-FL-IMPORTI-FORZ-NULL
                TO (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-FL-IMPORTI-FORZ
                TO (SF)-FL-IMPORTI-FORZ(IX-TAB-TGA)
           END-IF
           IF TGA-PRSTZ-INI-NFORZ-NULL = HIGH-VALUES
              MOVE TGA-PRSTZ-INI-NFORZ-NULL
                TO (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRSTZ-INI-NFORZ
                TO (SF)-PRSTZ-INI-NFORZ(IX-TAB-TGA)
           END-IF
           IF TGA-VIS-END2000-NFORZ-NULL = HIGH-VALUES
              MOVE TGA-VIS-END2000-NFORZ-NULL
                TO (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-VIS-END2000-NFORZ
                TO (SF)-VIS-END2000-NFORZ(IX-TAB-TGA)
           END-IF
           IF TGA-INTR-MORA-NULL = HIGH-VALUES
              MOVE TGA-INTR-MORA-NULL
                TO (SF)-INTR-MORA-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-INTR-MORA
                TO (SF)-INTR-MORA(IX-TAB-TGA)
           END-IF
           IF TGA-MANFEE-ANTIC-NULL = HIGH-VALUES
              MOVE TGA-MANFEE-ANTIC-NULL
                TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-MANFEE-ANTIC
                TO (SF)-MANFEE-ANTIC(IX-TAB-TGA)
           END-IF
           IF TGA-MANFEE-RICOR-NULL = HIGH-VALUES
              MOVE TGA-MANFEE-RICOR-NULL
                TO (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-MANFEE-RICOR
                TO (SF)-MANFEE-RICOR(IX-TAB-TGA)
           END-IF
           IF TGA-PRE-UNI-RIVTO-NULL = HIGH-VALUES
              MOVE TGA-PRE-UNI-RIVTO-NULL
                TO (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRE-UNI-RIVTO
                TO (SF)-PRE-UNI-RIVTO(IX-TAB-TGA)
           END-IF
           IF TGA-PROV-1AA-ACQ-NULL = HIGH-VALUES
              MOVE TGA-PROV-1AA-ACQ-NULL
                TO (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PROV-1AA-ACQ
                TO (SF)-PROV-1AA-ACQ(IX-TAB-TGA)
           END-IF
           IF TGA-PROV-2AA-ACQ-NULL = HIGH-VALUES
              MOVE TGA-PROV-2AA-ACQ-NULL
                TO (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PROV-2AA-ACQ
                TO (SF)-PROV-2AA-ACQ(IX-TAB-TGA)
           END-IF
           IF TGA-PROV-RICOR-NULL = HIGH-VALUES
              MOVE TGA-PROV-RICOR-NULL
                TO (SF)-PROV-RICOR-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PROV-RICOR
                TO (SF)-PROV-RICOR(IX-TAB-TGA)
           END-IF
           IF TGA-PROV-INC-NULL = HIGH-VALUES
              MOVE TGA-PROV-INC-NULL
                TO (SF)-PROV-INC-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PROV-INC
                TO (SF)-PROV-INC(IX-TAB-TGA)
           END-IF
           IF TGA-ALQ-PROV-ACQ-NULL = HIGH-VALUES
              MOVE TGA-ALQ-PROV-ACQ-NULL
                TO (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ALQ-PROV-ACQ
                TO (SF)-ALQ-PROV-ACQ(IX-TAB-TGA)
           END-IF
           IF TGA-ALQ-PROV-INC-NULL = HIGH-VALUES
              MOVE TGA-ALQ-PROV-INC-NULL
                TO (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ALQ-PROV-INC
                TO (SF)-ALQ-PROV-INC(IX-TAB-TGA)
           END-IF
           IF TGA-ALQ-PROV-RICOR-NULL = HIGH-VALUES
              MOVE TGA-ALQ-PROV-RICOR-NULL
                TO (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ALQ-PROV-RICOR
                TO (SF)-ALQ-PROV-RICOR(IX-TAB-TGA)
           END-IF
           IF TGA-IMPB-PROV-ACQ-NULL = HIGH-VALUES
              MOVE TGA-IMPB-PROV-ACQ-NULL
                TO (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMPB-PROV-ACQ
                TO (SF)-IMPB-PROV-ACQ(IX-TAB-TGA)
           END-IF
           IF TGA-IMPB-PROV-INC-NULL = HIGH-VALUES
              MOVE TGA-IMPB-PROV-INC-NULL
                TO (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMPB-PROV-INC
                TO (SF)-IMPB-PROV-INC(IX-TAB-TGA)
           END-IF
           IF TGA-IMPB-PROV-RICOR-NULL = HIGH-VALUES
              MOVE TGA-IMPB-PROV-RICOR-NULL
                TO (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMPB-PROV-RICOR
                TO (SF)-IMPB-PROV-RICOR(IX-TAB-TGA)
           END-IF
           IF TGA-FL-PROV-FORZ-NULL = HIGH-VALUES
              MOVE TGA-FL-PROV-FORZ-NULL
                TO (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-FL-PROV-FORZ
                TO (SF)-FL-PROV-FORZ(IX-TAB-TGA)
           END-IF
           IF TGA-PRSTZ-AGG-INI-NULL = HIGH-VALUES
              MOVE TGA-PRSTZ-AGG-INI-NULL
                TO (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRSTZ-AGG-INI
                TO (SF)-PRSTZ-AGG-INI(IX-TAB-TGA)
           END-IF
           IF TGA-INCR-PRE-NULL = HIGH-VALUES
              MOVE TGA-INCR-PRE-NULL
                TO (SF)-INCR-PRE-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-INCR-PRE
                TO (SF)-INCR-PRE(IX-TAB-TGA)
           END-IF
           IF TGA-INCR-PRSTZ-NULL = HIGH-VALUES
              MOVE TGA-INCR-PRSTZ-NULL
                TO (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-INCR-PRSTZ
                TO (SF)-INCR-PRSTZ(IX-TAB-TGA)
           END-IF
           IF TGA-DT-ULT-ADEG-PRE-PR-NULL = HIGH-VALUES
              MOVE TGA-DT-ULT-ADEG-PRE-PR-NULL
                TO (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-DT-ULT-ADEG-PRE-PR
                TO (SF)-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
           END-IF
           IF TGA-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
              MOVE TGA-PRSTZ-AGG-ULT-NULL
                TO (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRSTZ-AGG-ULT
                TO (SF)-PRSTZ-AGG-ULT(IX-TAB-TGA)
           END-IF
           IF TGA-TS-RIVAL-NET-NULL = HIGH-VALUES
              MOVE TGA-TS-RIVAL-NET-NULL
                TO (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-TS-RIVAL-NET
                TO (SF)-TS-RIVAL-NET(IX-TAB-TGA)
           END-IF
           IF TGA-PRE-PATTUITO-NULL = HIGH-VALUES
              MOVE TGA-PRE-PATTUITO-NULL
                TO (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PRE-PATTUITO
                TO (SF)-PRE-PATTUITO(IX-TAB-TGA)
           END-IF
           IF TGA-TP-RIVAL-NULL = HIGH-VALUES
              MOVE TGA-TP-RIVAL-NULL
                TO (SF)-TP-RIVAL-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-TP-RIVAL
                TO (SF)-TP-RIVAL(IX-TAB-TGA)
           END-IF
           IF TGA-RIS-MAT-NULL = HIGH-VALUES
              MOVE TGA-RIS-MAT-NULL
                TO (SF)-RIS-MAT-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-RIS-MAT
                TO (SF)-RIS-MAT(IX-TAB-TGA)
           END-IF
           IF TGA-CPT-MIN-SCAD-NULL = HIGH-VALUES
              MOVE TGA-CPT-MIN-SCAD-NULL
                TO (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-CPT-MIN-SCAD
                TO (SF)-CPT-MIN-SCAD(IX-TAB-TGA)
           END-IF
           IF TGA-COMMIS-GEST-NULL = HIGH-VALUES
              MOVE TGA-COMMIS-GEST-NULL
                TO (SF)-COMMIS-GEST-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-COMMIS-GEST
                TO (SF)-COMMIS-GEST(IX-TAB-TGA)
           END-IF
           IF TGA-TP-MANFEE-APPL-NULL = HIGH-VALUES
              MOVE TGA-TP-MANFEE-APPL-NULL
                TO (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-TP-MANFEE-APPL
                TO (SF)-TP-MANFEE-APPL(IX-TAB-TGA)
           END-IF
           MOVE TGA-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-TGA)
           MOVE TGA-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-TGA)
           MOVE TGA-DS-VER
             TO (SF)-DS-VER(IX-TAB-TGA)
           MOVE TGA-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TGA)
           MOVE TGA-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-TGA)
           MOVE TGA-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-TGA)
           MOVE TGA-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-TGA)
           IF TGA-PC-COMMIS-GEST-NULL = HIGH-VALUES
              MOVE TGA-PC-COMMIS-GEST-NULL
                TO (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-PC-COMMIS-GEST
                TO (SF)-PC-COMMIS-GEST(IX-TAB-TGA)
           END-IF
           IF TGA-NUM-GG-RIVAL-NULL = HIGH-VALUES
              MOVE TGA-NUM-GG-RIVAL-NULL
                TO (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-NUM-GG-RIVAL
                TO (SF)-NUM-GG-RIVAL(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-TRASFE-NULL = HIGH-VALUES
              MOVE TGA-IMP-TRASFE-NULL
                TO (SF)-IMP-TRASFE-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-TRASFE
                TO (SF)-IMP-TRASFE(IX-TAB-TGA)
           END-IF
           IF TGA-IMP-TFR-STRC-NULL = HIGH-VALUES
              MOVE TGA-IMP-TFR-STRC-NULL
                TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMP-TFR-STRC
                TO (SF)-IMP-TFR-STRC(IX-TAB-TGA)
           END-IF
           IF TGA-ACQ-EXP-NULL = HIGH-VALUES
              MOVE TGA-ACQ-EXP-NULL
                TO (SF)-ACQ-EXP-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ACQ-EXP
                TO (SF)-ACQ-EXP(IX-TAB-TGA)
           END-IF
           IF TGA-REMUN-ASS-NULL = HIGH-VALUES
              MOVE TGA-REMUN-ASS-NULL
                TO (SF)-REMUN-ASS-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-REMUN-ASS
                TO (SF)-REMUN-ASS(IX-TAB-TGA)
           END-IF
           IF TGA-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE TGA-COMMIS-INTER-NULL
                TO (SF)-COMMIS-INTER-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-COMMIS-INTER
                TO (SF)-COMMIS-INTER(IX-TAB-TGA)
           END-IF
           IF TGA-ALQ-REMUN-ASS-NULL = HIGH-VALUES
              MOVE TGA-ALQ-REMUN-ASS-NULL
                TO (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ALQ-REMUN-ASS
                TO (SF)-ALQ-REMUN-ASS(IX-TAB-TGA)
           END-IF
           IF TGA-ALQ-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE TGA-ALQ-COMMIS-INTER-NULL
                TO (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-ALQ-COMMIS-INTER
                TO (SF)-ALQ-COMMIS-INTER(IX-TAB-TGA)
           END-IF
           IF TGA-IMPB-REMUN-ASS-NULL = HIGH-VALUES
              MOVE TGA-IMPB-REMUN-ASS-NULL
                TO (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMPB-REMUN-ASS
                TO (SF)-IMPB-REMUN-ASS(IX-TAB-TGA)
           END-IF
           IF TGA-IMPB-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE TGA-IMPB-COMMIS-INTER-NULL
                TO (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-IMPB-COMMIS-INTER
                TO (SF)-IMPB-COMMIS-INTER(IX-TAB-TGA)
           END-IF
           IF TGA-COS-RUN-ASSVA-NULL = HIGH-VALUES
              MOVE TGA-COS-RUN-ASSVA-NULL
                TO (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-COS-RUN-ASSVA
                TO (SF)-COS-RUN-ASSVA(IX-TAB-TGA)
           END-IF
           IF TGA-COS-RUN-ASSVA-IDC-NULL = HIGH-VALUES
              MOVE TGA-COS-RUN-ASSVA-IDC-NULL
                TO (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA)
           ELSE
              MOVE TGA-COS-RUN-ASSVA-IDC
                TO (SF)-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
           END-IF.
       VALORIZZA-OUTPUT-TGA-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVPOG5
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-POG.
           MOVE (SF)-TP-OGG(IX-TAB-POG)
              TO POG-TP-OGG
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-POG) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-POG)
              TO POG-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-POG)
              TO POG-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-POG) NOT NUMERIC
              MOVE 0 TO POG-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-POG)
              TO POG-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-POG) NOT NUMERIC
              MOVE 0 TO POG-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-POG)
              TO POG-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-POG)
              TO POG-COD-COMP-ANIA
           IF (SF)-COD-PARAM-NULL(IX-TAB-POG) = HIGH-VALUES
              MOVE (SF)-COD-PARAM-NULL(IX-TAB-POG)
              TO POG-COD-PARAM-NULL
           ELSE
              MOVE (SF)-COD-PARAM(IX-TAB-POG)
              TO POG-COD-PARAM
           END-IF
           IF (SF)-TP-PARAM-NULL(IX-TAB-POG) = HIGH-VALUES
              MOVE (SF)-TP-PARAM-NULL(IX-TAB-POG)
              TO POG-TP-PARAM-NULL
           ELSE
              MOVE (SF)-TP-PARAM(IX-TAB-POG)
              TO POG-TP-PARAM
           END-IF
           IF (SF)-TP-D-NULL(IX-TAB-POG) = HIGH-VALUES
              MOVE (SF)-TP-D-NULL(IX-TAB-POG)
              TO POG-TP-D-NULL
           ELSE
              MOVE (SF)-TP-D(IX-TAB-POG)
              TO POG-TP-D
           END-IF
           IF (SF)-VAL-IMP-NULL(IX-TAB-POG) = HIGH-VALUES
              MOVE (SF)-VAL-IMP-NULL(IX-TAB-POG)
              TO POG-VAL-IMP-NULL
           ELSE
              MOVE (SF)-VAL-IMP(IX-TAB-POG)
              TO POG-VAL-IMP
           END-IF
           IF (SF)-VAL-DT-NULL(IX-TAB-POG) = HIGH-VALUES
              MOVE (SF)-VAL-DT-NULL(IX-TAB-POG)
              TO POG-VAL-DT-NULL
           ELSE
             IF (SF)-VAL-DT(IX-TAB-POG) = ZERO
                MOVE HIGH-VALUES
                TO POG-VAL-DT-NULL
             ELSE
              MOVE (SF)-VAL-DT(IX-TAB-POG)
              TO POG-VAL-DT
             END-IF
           END-IF
           IF (SF)-VAL-TS-NULL(IX-TAB-POG) = HIGH-VALUES
              MOVE (SF)-VAL-TS-NULL(IX-TAB-POG)
              TO POG-VAL-TS-NULL
           ELSE
              MOVE (SF)-VAL-TS(IX-TAB-POG)
              TO POG-VAL-TS
           END-IF
           MOVE (SF)-VAL-TXT(IX-TAB-POG)
              TO POG-VAL-TXT
           IF (SF)-VAL-FL-NULL(IX-TAB-POG) = HIGH-VALUES
              MOVE (SF)-VAL-FL-NULL(IX-TAB-POG)
              TO POG-VAL-FL-NULL
           ELSE
              MOVE (SF)-VAL-FL(IX-TAB-POG)
              TO POG-VAL-FL
           END-IF
           IF (SF)-VAL-NUM-NULL(IX-TAB-POG) = HIGH-VALUES
              MOVE (SF)-VAL-NUM-NULL(IX-TAB-POG)
              TO POG-VAL-NUM-NULL
           ELSE
              MOVE (SF)-VAL-NUM(IX-TAB-POG)
              TO POG-VAL-NUM
           END-IF
           IF (SF)-VAL-PC-NULL(IX-TAB-POG) = HIGH-VALUES
              MOVE (SF)-VAL-PC-NULL(IX-TAB-POG)
              TO POG-VAL-PC-NULL
           ELSE
              MOVE (SF)-VAL-PC(IX-TAB-POG)
              TO POG-VAL-PC
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-POG) NOT NUMERIC
              MOVE 0 TO POG-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-POG)
              TO POG-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-POG)
              TO POG-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-POG) NOT NUMERIC
              MOVE 0 TO POG-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-POG)
              TO POG-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-POG) NOT NUMERIC
              MOVE 0 TO POG-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-POG)
              TO POG-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-POG) NOT NUMERIC
              MOVE 0 TO POG-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-POG)
              TO POG-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-POG)
              TO POG-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-POG)
              TO POG-DS-STATO-ELAB.
       VAL-DCLGEN-POG-EX.
           EXIT.

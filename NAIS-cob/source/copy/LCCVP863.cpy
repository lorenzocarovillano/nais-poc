
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVP863
      *   ULTIMO AGG. 05 DIC 2014
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-P86.
           MOVE P86-ID-MOT-LIQ
             TO (SF)-ID-PTF(IX-TAB-P86)
           MOVE P86-ID-MOT-LIQ
             TO (SF)-ID-MOT-LIQ(IX-TAB-P86)
           MOVE P86-ID-LIQ
             TO (SF)-ID-LIQ(IX-TAB-P86)
           MOVE P86-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-P86)
           IF P86-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE P86-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P86)
           ELSE
              MOVE P86-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-P86)
           END-IF
           MOVE P86-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-P86)
           MOVE P86-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-P86)
           MOVE P86-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-P86)
           MOVE P86-TP-LIQ
             TO (SF)-TP-LIQ(IX-TAB-P86)
           MOVE P86-TP-MOT-LIQ
             TO (SF)-TP-MOT-LIQ(IX-TAB-P86)
           MOVE P86-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-P86)
           MOVE P86-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-P86)
           MOVE P86-DS-VER
             TO (SF)-DS-VER(IX-TAB-P86)
           MOVE P86-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-P86)
           MOVE P86-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-P86)
           MOVE P86-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-P86)
           MOVE P86-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-P86)
           MOVE P86-DESC-LIB-MOT-LIQ
             TO (SF)-DESC-LIB-MOT-LIQ(IX-TAB-P86).
       VALORIZZA-OUTPUT-P86-EX.
           EXIT.

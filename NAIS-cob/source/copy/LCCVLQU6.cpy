      *----------------------------------------------------------------*
      *    COPY      ..... LCCVLQU6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO LIQUIDAZIONE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-LIQ.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE LIQ.

      *--> NOME TABELLA FISICA DB
           MOVE 'LIQ'                  TO WK-TABELLA

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WLQU-ST-INV(IX-TAB-LQU)
              AND NOT WLQU-ST-CON(IX-TAB-LQU)
              AND WLQU-ELE-LIQ-MAX NOT = 0

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WLQU-ST-ADD(IX-TAB-LQU)

      *-->          ESTRAZIONE  SEQUENCE LIQUIDAZIONE
                    PERFORM ESTR-SEQUENCE
                       THRU ESTR-SEQUENCE-EX

                    IF IDSV0001-ESITO-OK
                       MOVE S090-SEQ-TABELLA  TO WLQU-ID-PTF(IX-TAB-LQU)
                                                 LQU-ID-LIQ
                       MOVE WMOV-ID-PTF       TO LQU-ID-MOVI-CRZ
                       MOVE WLQU-ID-OGG(IX-TAB-LQU) TO LQU-ID-OGG

                    END-IF

      *-->          TIPO OPERAZIONE DISPATCHER
                    SET  IDSI0011-AGGIORNAMENTO-STORICO  TO TRUE

      *-->        MODIFICA
                  WHEN WLQU-ST-MOD(IX-TAB-LQU)

      *-->          VALORIZZAZIONE  ID  LIQUIDAZIONE
                    MOVE WMOV-ID-PTF              TO LQU-ID-MOVI-CRZ
                    MOVE WMOV-DT-EFF              TO LQU-DT-INI-EFF
                    MOVE WLQU-ID-LIQ(IX-TAB-LQU)  TO LQU-ID-LIQ
                    MOVE WLQU-ID-OGG(IX-TAB-LQU)  TO LQU-ID-OGG

      *-->          TIPO OPERAZIONE DISPATCHER
                    SET  IDSI0011-AGGIORNAMENTO-STORICO  TO TRUE

      *--> CANCELLAZIONE LIQUIDAZIONE
               WHEN WLQU-ST-DEL(IX-TAB-LQU)

      *-->          VALORIZZAZIONE  ID  LIQUIDAZIONE
                    MOVE WMOV-ID-PTF               TO LQU-ID-MOVI-CRZ
                                                      LQU-ID-MOVI-CHIU
                    MOVE WLQU-ID-PTF(IX-TAB-LQU)   TO LQU-ID-LIQ
                    MOVE WLQU-ID-OGG(IX-TAB-LQU)   TO LQU-ID-OGG

      *-->          TIPO OPERAZIONE DISPATCHER
                    SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN LIQUIDAZIONE
                 PERFORM VAL-DCLGEN-LQU
                    THRU VAL-DCLGEN-LQU-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-LQU
                    THRU VALORIZZA-AREA-DSH-LQU-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX
              END-IF
           END-IF.

       AGGIORNA-LIQ-EX.
           EXIT.


      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-LQU.

      *--> DCLGEN TABELLA
           MOVE LIQ                       TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID               TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT  TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-LQU-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVLQU5.

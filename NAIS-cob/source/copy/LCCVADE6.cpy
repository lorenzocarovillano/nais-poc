      *----------------------------------------------------------------*
      *    COPY      ..... LCCVADE6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO ADESIONE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVADE5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN ADESIONE (LCCVADE1)
      *
      *----------------------------------------------------------------*

       AGGIORNA-ADES.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE ADES

      *--> SE LO STATUS NON E' "INVARIATO"
           IF  NOT WADE-ST-INV
           AND NOT WADE-ST-CON
           AND WADE-ELE-ADES-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WADE-ST-ADD

                       MOVE WADE-ID-PTF  TO ADE-ID-ADES
                       MOVE WPOL-ID-PTF  TO ADE-ID-POLI
                       MOVE WMOV-ID-PTF  TO ADE-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WADE-ST-MOD

                       MOVE WADE-ID-PTF  TO ADE-ID-ADES
                       MOVE WPOL-ID-PTF  TO ADE-ID-POLI
                       MOVE WMOV-ID-PTF  TO ADE-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WADE-ST-DEL

                       MOVE WADE-ID-PTF  TO ADE-ID-ADES
                       MOVE WPOL-ID-PTF  TO ADE-ID-POLI
                       MOVE WMOV-ID-PTF  TO ADE-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

      *-->    VALORIZZA DCLGEN ADESIONE
              PERFORM VAL-DCLGEN-ADE
                 THRU VAL-DCLGEN-ADE-EX

      *-->    VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
              PERFORM VALORIZZA-AREA-DSH-ADE
                 THRU VALORIZZA-AREA-DSH-ADE-EX

      *-->    CALL DISPATCHER PER AGGIORNAMENTO
              PERFORM AGGIORNA-TABELLA
                 THRU AGGIORNA-TABELLA-EX

           END-IF.

       AGGIORNA-ADES-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-ADE.

      *--> NOME TABELLA FISICA DB
           MOVE 'ADES'                  TO   WK-TABELLA.

      *--> DCLGEN TABELLA
           MOVE ADES                    TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-ADE-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *    COPY LCCVADE5.

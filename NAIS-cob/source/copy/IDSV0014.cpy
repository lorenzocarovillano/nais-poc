
      *---
      *---  CAMPI UTILIZZATI DALLE ROUTINE DI
      *---  CONVERSIONE DATE
      *---
       01 WS-STR-DATE-N.
         05 WS-DATE-N               PIC 9(08).

       77 WS-DATE-X                 PIC X(10).

      *---
      *---  CAMPI UTILIZZATI DALLE ROUTINE DI
      *---  CONVERSIONE TIMESTAMP
      *---
       01 WS-STR-TIMESTAMP-N.
         05 WS-TIMESTAMP-N          PIC 9(18).

       77 WS-TIMESTAMP-X            PIC X(26).


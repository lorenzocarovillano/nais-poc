
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVGRZ5
      *   ULTIMO AGG. 31 OTT 2013
      *------------------------------------------------------------

       VAL-DCLGEN-GRZ.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-GRZ)
              TO GRZ-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-GRZ)
              TO GRZ-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-GRZ) NOT NUMERIC
              MOVE 0 TO GRZ-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-GRZ)
              TO GRZ-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-GRZ) NOT NUMERIC
              MOVE 0 TO GRZ-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-GRZ)
              TO GRZ-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-GRZ)
              TO GRZ-COD-COMP-ANIA
           IF (SF)-IB-OGG-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-IB-OGG-NULL(IX-TAB-GRZ)
              TO GRZ-IB-OGG-NULL
           ELSE
              MOVE (SF)-IB-OGG(IX-TAB-GRZ)
              TO GRZ-IB-OGG
           END-IF
           IF (SF)-DT-DECOR-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-DT-DECOR-NULL(IX-TAB-GRZ)
              TO GRZ-DT-DECOR-NULL
           ELSE
             IF (SF)-DT-DECOR(IX-TAB-GRZ) = ZERO
                MOVE HIGH-VALUES
                TO GRZ-DT-DECOR-NULL
             ELSE
              MOVE (SF)-DT-DECOR(IX-TAB-GRZ)
              TO GRZ-DT-DECOR
             END-IF
           END-IF
           IF (SF)-DT-SCAD-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-DT-SCAD-NULL(IX-TAB-GRZ)
              TO GRZ-DT-SCAD-NULL
           ELSE
             IF (SF)-DT-SCAD(IX-TAB-GRZ) = ZERO
                MOVE HIGH-VALUES
                TO GRZ-DT-SCAD-NULL
             ELSE
              MOVE (SF)-DT-SCAD(IX-TAB-GRZ)
              TO GRZ-DT-SCAD
             END-IF
           END-IF
           IF (SF)-COD-SEZ-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-COD-SEZ-NULL(IX-TAB-GRZ)
              TO GRZ-COD-SEZ-NULL
           ELSE
              MOVE (SF)-COD-SEZ(IX-TAB-GRZ)
              TO GRZ-COD-SEZ
           END-IF
           MOVE (SF)-COD-TARI(IX-TAB-GRZ)
              TO GRZ-COD-TARI
           IF (SF)-RAMO-BILA-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-RAMO-BILA-NULL(IX-TAB-GRZ)
              TO GRZ-RAMO-BILA-NULL
           ELSE
              MOVE (SF)-RAMO-BILA(IX-TAB-GRZ)
              TO GRZ-RAMO-BILA
           END-IF
           IF (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-GRZ)
              TO GRZ-DT-INI-VAL-TAR-NULL
           ELSE
             IF (SF)-DT-INI-VAL-TAR(IX-TAB-GRZ) = ZERO
                MOVE HIGH-VALUES
                TO GRZ-DT-INI-VAL-TAR-NULL
             ELSE
              MOVE (SF)-DT-INI-VAL-TAR(IX-TAB-GRZ)
              TO GRZ-DT-INI-VAL-TAR
             END-IF
           END-IF
           IF (SF)-ID-1O-ASSTO-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-ID-1O-ASSTO-NULL(IX-TAB-GRZ)
              TO GRZ-ID-1O-ASSTO-NULL
           ELSE
              MOVE (SF)-ID-1O-ASSTO(IX-TAB-GRZ)
              TO GRZ-ID-1O-ASSTO
           END-IF
           IF (SF)-ID-2O-ASSTO-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-ID-2O-ASSTO-NULL(IX-TAB-GRZ)
              TO GRZ-ID-2O-ASSTO-NULL
           ELSE
              MOVE (SF)-ID-2O-ASSTO(IX-TAB-GRZ)
              TO GRZ-ID-2O-ASSTO
           END-IF
           IF (SF)-ID-3O-ASSTO-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-ID-3O-ASSTO-NULL(IX-TAB-GRZ)
              TO GRZ-ID-3O-ASSTO-NULL
           ELSE
              MOVE (SF)-ID-3O-ASSTO(IX-TAB-GRZ)
              TO GRZ-ID-3O-ASSTO
           END-IF
           IF (SF)-TP-GAR-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TP-GAR-NULL(IX-TAB-GRZ)
              TO GRZ-TP-GAR-NULL
           ELSE
              MOVE (SF)-TP-GAR(IX-TAB-GRZ)
              TO GRZ-TP-GAR
           END-IF
           IF (SF)-TP-RSH-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TP-RSH-NULL(IX-TAB-GRZ)
              TO GRZ-TP-RSH-NULL
           ELSE
              MOVE (SF)-TP-RSH(IX-TAB-GRZ)
              TO GRZ-TP-RSH
           END-IF
           IF (SF)-TP-INVST-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TP-INVST-NULL(IX-TAB-GRZ)
              TO GRZ-TP-INVST-NULL
           ELSE
              MOVE (SF)-TP-INVST(IX-TAB-GRZ)
              TO GRZ-TP-INVST
           END-IF
           IF (SF)-MOD-PAG-GARCOL-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-MOD-PAG-GARCOL-NULL(IX-TAB-GRZ)
              TO GRZ-MOD-PAG-GARCOL-NULL
           ELSE
              MOVE (SF)-MOD-PAG-GARCOL(IX-TAB-GRZ)
              TO GRZ-MOD-PAG-GARCOL
           END-IF
           IF (SF)-TP-PER-PRE-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TP-PER-PRE-NULL(IX-TAB-GRZ)
              TO GRZ-TP-PER-PRE-NULL
           ELSE
              MOVE (SF)-TP-PER-PRE(IX-TAB-GRZ)
              TO GRZ-TP-PER-PRE
           END-IF
           IF (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-GRZ)
              TO GRZ-ETA-AA-1O-ASSTO-NULL
           ELSE
              MOVE (SF)-ETA-AA-1O-ASSTO(IX-TAB-GRZ)
              TO GRZ-ETA-AA-1O-ASSTO
           END-IF
           IF (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-GRZ)
              TO GRZ-ETA-MM-1O-ASSTO-NULL
           ELSE
              MOVE (SF)-ETA-MM-1O-ASSTO(IX-TAB-GRZ)
              TO GRZ-ETA-MM-1O-ASSTO
           END-IF
           IF (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-GRZ)
              TO GRZ-ETA-AA-2O-ASSTO-NULL
           ELSE
              MOVE (SF)-ETA-AA-2O-ASSTO(IX-TAB-GRZ)
              TO GRZ-ETA-AA-2O-ASSTO
           END-IF
           IF (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-GRZ)
              TO GRZ-ETA-MM-2O-ASSTO-NULL
           ELSE
              MOVE (SF)-ETA-MM-2O-ASSTO(IX-TAB-GRZ)
              TO GRZ-ETA-MM-2O-ASSTO
           END-IF
           IF (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-GRZ)
              TO GRZ-ETA-AA-3O-ASSTO-NULL
           ELSE
              MOVE (SF)-ETA-AA-3O-ASSTO(IX-TAB-GRZ)
              TO GRZ-ETA-AA-3O-ASSTO
           END-IF
           IF (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-GRZ)
              TO GRZ-ETA-MM-3O-ASSTO-NULL
           ELSE
              MOVE (SF)-ETA-MM-3O-ASSTO(IX-TAB-GRZ)
              TO GRZ-ETA-MM-3O-ASSTO
           END-IF
           IF (SF)-TP-EMIS-PUR-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TP-EMIS-PUR-NULL(IX-TAB-GRZ)
              TO GRZ-TP-EMIS-PUR-NULL
           ELSE
              MOVE (SF)-TP-EMIS-PUR(IX-TAB-GRZ)
              TO GRZ-TP-EMIS-PUR
           END-IF
           IF (SF)-ETA-A-SCAD-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-ETA-A-SCAD-NULL(IX-TAB-GRZ)
              TO GRZ-ETA-A-SCAD-NULL
           ELSE
              MOVE (SF)-ETA-A-SCAD(IX-TAB-GRZ)
              TO GRZ-ETA-A-SCAD
           END-IF
           IF (SF)-TP-CALC-PRE-PRSTZ-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TP-CALC-PRE-PRSTZ-NULL(IX-TAB-GRZ)
              TO GRZ-TP-CALC-PRE-PRSTZ-NULL
           ELSE
              MOVE (SF)-TP-CALC-PRE-PRSTZ(IX-TAB-GRZ)
              TO GRZ-TP-CALC-PRE-PRSTZ
           END-IF
           IF (SF)-TP-PRE-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TP-PRE-NULL(IX-TAB-GRZ)
              TO GRZ-TP-PRE-NULL
           ELSE
              MOVE (SF)-TP-PRE(IX-TAB-GRZ)
              TO GRZ-TP-PRE
           END-IF
           IF (SF)-TP-DUR-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TP-DUR-NULL(IX-TAB-GRZ)
              TO GRZ-TP-DUR-NULL
           ELSE
              MOVE (SF)-TP-DUR(IX-TAB-GRZ)
              TO GRZ-TP-DUR
           END-IF
           IF (SF)-DUR-AA-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-DUR-AA-NULL(IX-TAB-GRZ)
              TO GRZ-DUR-AA-NULL
           ELSE
              MOVE (SF)-DUR-AA(IX-TAB-GRZ)
              TO GRZ-DUR-AA
           END-IF
           IF (SF)-DUR-MM-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-DUR-MM-NULL(IX-TAB-GRZ)
              TO GRZ-DUR-MM-NULL
           ELSE
              MOVE (SF)-DUR-MM(IX-TAB-GRZ)
              TO GRZ-DUR-MM
           END-IF
           IF (SF)-DUR-GG-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-DUR-GG-NULL(IX-TAB-GRZ)
              TO GRZ-DUR-GG-NULL
           ELSE
              MOVE (SF)-DUR-GG(IX-TAB-GRZ)
              TO GRZ-DUR-GG
           END-IF
           IF (SF)-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ)
              TO GRZ-NUM-AA-PAG-PRE-NULL
           ELSE
              MOVE (SF)-NUM-AA-PAG-PRE(IX-TAB-GRZ)
              TO GRZ-NUM-AA-PAG-PRE
           END-IF
           IF (SF)-AA-PAG-PRE-UNI-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-AA-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
              TO GRZ-AA-PAG-PRE-UNI-NULL
           ELSE
              MOVE (SF)-AA-PAG-PRE-UNI(IX-TAB-GRZ)
              TO GRZ-AA-PAG-PRE-UNI
           END-IF
           IF (SF)-MM-PAG-PRE-UNI-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-MM-PAG-PRE-UNI-NULL(IX-TAB-GRZ)
              TO GRZ-MM-PAG-PRE-UNI-NULL
           ELSE
              MOVE (SF)-MM-PAG-PRE-UNI(IX-TAB-GRZ)
              TO GRZ-MM-PAG-PRE-UNI
           END-IF
           IF (SF)-FRAZ-INI-EROG-REN-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-FRAZ-INI-EROG-REN-NULL(IX-TAB-GRZ)
              TO GRZ-FRAZ-INI-EROG-REN-NULL
           ELSE
              MOVE (SF)-FRAZ-INI-EROG-REN(IX-TAB-GRZ)
              TO GRZ-FRAZ-INI-EROG-REN
           END-IF
           IF (SF)-MM-1O-RAT-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-MM-1O-RAT-NULL(IX-TAB-GRZ)
              TO GRZ-MM-1O-RAT-NULL
           ELSE
              MOVE (SF)-MM-1O-RAT(IX-TAB-GRZ)
              TO GRZ-MM-1O-RAT
           END-IF
           IF (SF)-PC-1O-RAT-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-PC-1O-RAT-NULL(IX-TAB-GRZ)
              TO GRZ-PC-1O-RAT-NULL
           ELSE
              MOVE (SF)-PC-1O-RAT(IX-TAB-GRZ)
              TO GRZ-PC-1O-RAT
           END-IF
           IF (SF)-TP-PRSTZ-ASSTA-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TP-PRSTZ-ASSTA-NULL(IX-TAB-GRZ)
              TO GRZ-TP-PRSTZ-ASSTA-NULL
           ELSE
              MOVE (SF)-TP-PRSTZ-ASSTA(IX-TAB-GRZ)
              TO GRZ-TP-PRSTZ-ASSTA
           END-IF
           IF (SF)-DT-END-CARZ-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-DT-END-CARZ-NULL(IX-TAB-GRZ)
              TO GRZ-DT-END-CARZ-NULL
           ELSE
             IF (SF)-DT-END-CARZ(IX-TAB-GRZ) = ZERO
                MOVE HIGH-VALUES
                TO GRZ-DT-END-CARZ-NULL
             ELSE
              MOVE (SF)-DT-END-CARZ(IX-TAB-GRZ)
              TO GRZ-DT-END-CARZ
             END-IF
           END-IF
           IF (SF)-PC-RIP-PRE-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-PC-RIP-PRE-NULL(IX-TAB-GRZ)
              TO GRZ-PC-RIP-PRE-NULL
           ELSE
              MOVE (SF)-PC-RIP-PRE(IX-TAB-GRZ)
              TO GRZ-PC-RIP-PRE
           END-IF
           IF (SF)-COD-FND-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-COD-FND-NULL(IX-TAB-GRZ)
              TO GRZ-COD-FND-NULL
           ELSE
              MOVE (SF)-COD-FND(IX-TAB-GRZ)
              TO GRZ-COD-FND
           END-IF
           IF (SF)-AA-REN-CER-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-AA-REN-CER-NULL(IX-TAB-GRZ)
              TO GRZ-AA-REN-CER-NULL
           ELSE
              MOVE (SF)-AA-REN-CER(IX-TAB-GRZ)
              TO GRZ-AA-REN-CER
           END-IF
           IF (SF)-PC-REVRSB-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-PC-REVRSB-NULL(IX-TAB-GRZ)
              TO GRZ-PC-REVRSB-NULL
           ELSE
              MOVE (SF)-PC-REVRSB(IX-TAB-GRZ)
              TO GRZ-PC-REVRSB
           END-IF
           IF (SF)-TP-PC-RIP-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TP-PC-RIP-NULL(IX-TAB-GRZ)
              TO GRZ-TP-PC-RIP-NULL
           ELSE
              MOVE (SF)-TP-PC-RIP(IX-TAB-GRZ)
              TO GRZ-TP-PC-RIP
           END-IF
           IF (SF)-PC-OPZ-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-PC-OPZ-NULL(IX-TAB-GRZ)
              TO GRZ-PC-OPZ-NULL
           ELSE
              MOVE (SF)-PC-OPZ(IX-TAB-GRZ)
              TO GRZ-PC-OPZ
           END-IF
           IF (SF)-TP-IAS-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TP-IAS-NULL(IX-TAB-GRZ)
              TO GRZ-TP-IAS-NULL
           ELSE
              MOVE (SF)-TP-IAS(IX-TAB-GRZ)
              TO GRZ-TP-IAS
           END-IF
           IF (SF)-TP-STAB-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TP-STAB-NULL(IX-TAB-GRZ)
              TO GRZ-TP-STAB-NULL
           ELSE
              MOVE (SF)-TP-STAB(IX-TAB-GRZ)
              TO GRZ-TP-STAB
           END-IF
           IF (SF)-TP-ADEG-PRE-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TP-ADEG-PRE-NULL(IX-TAB-GRZ)
              TO GRZ-TP-ADEG-PRE-NULL
           ELSE
              MOVE (SF)-TP-ADEG-PRE(IX-TAB-GRZ)
              TO GRZ-TP-ADEG-PRE
           END-IF
           IF (SF)-DT-VARZ-TP-IAS-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-DT-VARZ-TP-IAS-NULL(IX-TAB-GRZ)
              TO GRZ-DT-VARZ-TP-IAS-NULL
           ELSE
             IF (SF)-DT-VARZ-TP-IAS(IX-TAB-GRZ) = ZERO
                MOVE HIGH-VALUES
                TO GRZ-DT-VARZ-TP-IAS-NULL
             ELSE
              MOVE (SF)-DT-VARZ-TP-IAS(IX-TAB-GRZ)
              TO GRZ-DT-VARZ-TP-IAS
             END-IF
           END-IF
           IF (SF)-FRAZ-DECR-CPT-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-FRAZ-DECR-CPT-NULL(IX-TAB-GRZ)
              TO GRZ-FRAZ-DECR-CPT-NULL
           ELSE
              MOVE (SF)-FRAZ-DECR-CPT(IX-TAB-GRZ)
              TO GRZ-FRAZ-DECR-CPT
           END-IF
           IF (SF)-COD-TRAT-RIASS-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-COD-TRAT-RIASS-NULL(IX-TAB-GRZ)
              TO GRZ-COD-TRAT-RIASS-NULL
           ELSE
              MOVE (SF)-COD-TRAT-RIASS(IX-TAB-GRZ)
              TO GRZ-COD-TRAT-RIASS
           END-IF
           IF (SF)-TP-DT-EMIS-RIASS-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TP-DT-EMIS-RIASS-NULL(IX-TAB-GRZ)
              TO GRZ-TP-DT-EMIS-RIASS-NULL
           ELSE
              MOVE (SF)-TP-DT-EMIS-RIASS(IX-TAB-GRZ)
              TO GRZ-TP-DT-EMIS-RIASS
           END-IF
           IF (SF)-TP-CESS-RIASS-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TP-CESS-RIASS-NULL(IX-TAB-GRZ)
              TO GRZ-TP-CESS-RIASS-NULL
           ELSE
              MOVE (SF)-TP-CESS-RIASS(IX-TAB-GRZ)
              TO GRZ-TP-CESS-RIASS
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-GRZ) NOT NUMERIC
              MOVE 0 TO GRZ-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-GRZ)
              TO GRZ-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-GRZ)
              TO GRZ-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-GRZ) NOT NUMERIC
              MOVE 0 TO GRZ-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-GRZ)
              TO GRZ-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-GRZ) NOT NUMERIC
              MOVE 0 TO GRZ-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-GRZ)
              TO GRZ-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-GRZ) NOT NUMERIC
              MOVE 0 TO GRZ-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-GRZ)
              TO GRZ-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-GRZ)
              TO GRZ-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-GRZ)
              TO GRZ-DS-STATO-ELAB
           IF (SF)-AA-STAB-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-AA-STAB-NULL(IX-TAB-GRZ)
              TO GRZ-AA-STAB-NULL
           ELSE
              MOVE (SF)-AA-STAB(IX-TAB-GRZ)
              TO GRZ-AA-STAB
           END-IF
           IF (SF)-TS-STAB-LIMITATA-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-TS-STAB-LIMITATA-NULL(IX-TAB-GRZ)
              TO GRZ-TS-STAB-LIMITATA-NULL
           ELSE
              MOVE (SF)-TS-STAB-LIMITATA(IX-TAB-GRZ)
              TO GRZ-TS-STAB-LIMITATA
           END-IF
           IF (SF)-DT-PRESC-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-DT-PRESC-NULL(IX-TAB-GRZ)
              TO GRZ-DT-PRESC-NULL
           ELSE
             IF (SF)-DT-PRESC(IX-TAB-GRZ) = ZERO
                MOVE HIGH-VALUES
                TO GRZ-DT-PRESC-NULL
             ELSE
              MOVE (SF)-DT-PRESC(IX-TAB-GRZ)
              TO GRZ-DT-PRESC
             END-IF
           END-IF
           IF (SF)-RSH-INVST-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE (SF)-RSH-INVST-NULL(IX-TAB-GRZ)
              TO GRZ-RSH-INVST-NULL
           ELSE
              MOVE (SF)-RSH-INVST(IX-TAB-GRZ)
              TO GRZ-RSH-INVST
           END-IF
           MOVE (SF)-TP-RAMO-BILA(IX-TAB-GRZ)
              TO GRZ-TP-RAMO-BILA.
       VAL-DCLGEN-GRZ-EX.
           EXIT.

       01 IND-EST-POLI.
          05 IND-E06-IB-OGG PIC S9(4) COMP-5.
          05 IND-E06-ID-MOVI-CHIU PIC S9(4) COMP-5.
          05 IND-E06-DT-ULT-PERD PIC S9(4) COMP-5.
          05 IND-E06-PC-ULT-PERD PIC S9(4) COMP-5.
          05 IND-E06-FL-ESCL-SWITCH-MAX PIC S9(4) COMP-5.
          05 IND-E06-ESI-ADEGZ-ISVAP PIC S9(4) COMP-5.
          05 IND-E06-NUM-INA PIC S9(4) COMP-5.
          05 IND-E06-TP-MOD-PROV PIC S9(4) COMP-5.
          05 IND-E06-CPT-PROTETTO PIC S9(4) COMP-5.
          05 IND-E06-CUM-PRE-ATT-TAKE-P PIC S9(4) COMP-5.
          05 IND-E06-DT-EMIS-PARTNER PIC S9(4) COMP-5.

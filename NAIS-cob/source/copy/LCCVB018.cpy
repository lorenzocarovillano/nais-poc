         03 (SF)-LEN-TOT                       PIC S9(4) COMP-5.
         03 (SF)-REC-FISSO.
           05 (SF)-ID-BILA-FND-ESTR       PIC S9(9)V     COMP-3.
           05 (SF)-ID-BILA-TRCH-ESTR      PIC S9(9)V     COMP-3.
           05 (SF)-COD-COMP-ANIA          PIC S9(5)V     COMP-3.
           05 (SF)-ID-RICH-ESTRAZ-MAS     PIC S9(9)V     COMP-3.
           05 (SF)-ID-RICH-ESTRAZ-AGG-IND PIC X.
           05 (SF)-ID-RICH-ESTRAZ-AGG     PIC S9(9)V     COMP-3.
           05 (SF)-ID-RICH-ESTRAZ-AGG-NULL REDEFINES
              (SF)-ID-RICH-ESTRAZ-AGG     PIC X(5).
           05 (SF)-DT-RIS                 PIC X(10).
           05 (SF)-ID-POLI                PIC S9(9)V     COMP-3.
           05 (SF)-ID-ADES                PIC S9(9)V     COMP-3.
           05 (SF)-ID-GAR                 PIC S9(9)V     COMP-3.
           05 (SF)-ID-TRCH-DI-GAR         PIC S9(9)V     COMP-3.
           05 (SF)-COD-FND                PIC X(12).
           05 (SF)-DT-QTZ-INI-IND         PIC X.
           05 (SF)-DT-QTZ-INI             PIC X(10).
           05 (SF)-DT-QTZ-INI-NULL REDEFINES
              (SF)-DT-QTZ-INI             PIC X(10).
           05 (SF)-NUM-QUO-INI-IND        PIC X.
           05 (SF)-NUM-QUO-INI            PIC S9(7)V9(5) COMP-3.
           05 (SF)-NUM-QUO-INI-NULL REDEFINES
              (SF)-NUM-QUO-INI            PIC X(7).
           05 (SF)-NUM-QUO-DT-CALC-IND    PIC X.
           05 (SF)-NUM-QUO-DT-CALC        PIC S9(7)V9(5) COMP-3.
           05 (SF)-NUM-QUO-DT-CALC-NULL REDEFINES
              (SF)-NUM-QUO-DT-CALC        PIC X(7).
           05 (SF)-VAL-QUO-INI-IND        PIC X.
           05 (SF)-VAL-QUO-INI            PIC S9(7)V9(5) COMP-3.
           05 (SF)-VAL-QUO-INI-NULL REDEFINES
              (SF)-VAL-QUO-INI            PIC X(7).
           05 (SF)-DT-VALZZ-QUO-DT-CA-IND PIC X.
           05 (SF)-DT-VALZZ-QUO-DT-CA     PIC X(10).
           05 (SF)-DT-VALZZ-QUO-DT-CA-NULL REDEFINES
              (SF)-DT-VALZZ-QUO-DT-CA     PIC X(10).
           05 (SF)-VAL-QUO-DT-CALC-IND    PIC X.
           05 (SF)-VAL-QUO-DT-CALC        PIC S9(7)V9(5) COMP-3.
           05 (SF)-VAL-QUO-DT-CALC-NULL REDEFINES
              (SF)-VAL-QUO-DT-CALC        PIC X(7).
           05 (SF)-VAL-QUO-T-IND          PIC X.
           05 (SF)-VAL-QUO-T              PIC S9(7)V9(5) COMP-3.
           05 (SF)-VAL-QUO-T-NULL REDEFINES
              (SF)-VAL-QUO-T              PIC X(7).
           05 (SF)-PC-INVST-IND           PIC X.
           05 (SF)-PC-INVST               PIC S9(3)V9(3) COMP-3.
           05 (SF)-PC-INVST-NULL REDEFINES
              (SF)-PC-INVST               PIC X(4).
           05 (SF)-DS-OPER-SQL            PIC X(1).
           05 (SF)-DS-VER                 PIC S9(9)V     COMP-3.
           05 (SF)-DS-TS-CPTZ             PIC S9(18)V     COMP-3.
           05 (SF)-DS-UTENTE              PIC X(20).
           05 (SF)-DS-STATO-ELAB          PIC X(1).


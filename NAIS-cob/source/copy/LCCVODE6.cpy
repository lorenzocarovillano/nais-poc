      *----------------------------------------------------------------*
      *    PROGRAMMA ..... LCCVODE6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO OGGETTO DEROGA
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVALL5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN ASSET ALLOCATION (LCCVALL1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-OGG-DEROGA.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE OGG-DEROGA

      *--> NOME TABELLA FISICA DB
           MOVE 'OGG-DEROGA'                   TO WK-TABELLA

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WODE-ST-INV
              AND WODE-ELE-ODE-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WODE-ST-ADD

                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA  TO WODE-ID-PTF
                          MOVE WODE-ID-PTF       TO ODE-ID-OGG-DEROGA
                                                    WODE-ID-OGG-DEROGA
                          MOVE WODE-ID-OGG       TO ODE-ID-OGG
      *                   MOVE WMOV-ID-OGG       TO ODE-ID-OGG
      *                                             WODE-ID-OGG
      *                   MOVE WMOV-TP-OGG       TO ODE-TP-OGG
      *                                             WODE-TP-OGG
                          MOVE WMOV-ID-PTF       TO ODE-ID-MOVI-CRZ
                                                    WODE-ID-MOVI-CRZ
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WODE-ST-MOD

                       MOVE WODE-ID-OGG-DEROGA TO ODE-ID-OGG-DEROGA
                       MOVE WODE-ID-OGG        TO ODE-ID-OGG
                       MOVE WODE-TP-OGG        TO ODE-TP-OGG
                       MOVE WODE-ID-MOVI-CRZ   TO ODE-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WODE-ST-DEL

                       MOVE WODE-ID-PTF
                         TO ODE-ID-OGG-DEROGA
                       MOVE WODE-ID-OGG
                         TO ODE-ID-OGG
                       MOVE WMOV-ID-PTF
                         TO ODE-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN
                 PERFORM VAL-DCLGEN-ODE
                    THRU VAL-DCLGEN-ODE-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-ODE
                    THRU VALORIZZA-AREA-DSH-ODE-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-OGG-DEROGA-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    PREPARA AREA PER CALL LCCS0234
      *----------------------------------------------------------------*
      *PREPARA-AREA-LCCS0234-ODE.
      *
      *    MOVE WODE-ID-OGG              TO S234-ID-OGG-EOC.
      *    MOVE WODE-TP-OGG              TO S234-TIPO-OGG-EOC.
      *
      *PREPARA-AREA-LCCS0234-ODE-EX.
      *    EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-ODE.

      *--> DCLGEN TABELLA
           MOVE OGG-DEROGA              TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-ODE-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVODE5.

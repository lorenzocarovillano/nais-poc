      *----------------------------------------------------------------*
      *    COPY      ..... LCCVB056
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO BIL_ESTRATTI
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVB055 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN VINCOLO PEGNO (LCCVB051)
      *
      *----------------------------------------------------------------*
       SCRIVI-BIL-VAR-DI-CALC-T.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE BILA-VAR-CALC-T.

      *--> SE LO STATUS NON E' "INVARIATO" O "CONVERSAZIONE"
           IF  NOT WB05-ST-INV
           AND NOT WB05-ST-CON
           AND WB05-ELE-B05-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WB05-ST-ADD

      *-->    ESTRAZIONE SEQUENCE E VALORIZZAZIONE CONSEGUENTE DEL CAMPO
      *-->    WB05-ID-BILA-VAR-CALC-T INIBITE X MOTIVI DI PERFORMANCE
      *
      *-->             ESTRAZIONE E VALORIZZAZIONE SEQUENCE
      *                 PERFORM ESTR-SEQUENCE
      *                    THRU ESTR-SEQUENCE-EX

      *                 IF IDSV0001-ESITO-OK
      *                    MOVE S090-SEQ-TABELLA
                           MOVE ZERO
                            TO WB05-ID-BILA-VAR-CALC-T
      *                 END-IF

      *-->        TIPO OPERAZIONE DISPATCHER
                       SET IDSI0011-INSERT TO TRUE

      *-->        MODIFICA
      *           WHEN WB05-ST-MOD
      *
      *
      *                MOVE WB05-ID-TRCH-DI-GAR
      *                  TO B05-ID-TRCH-DI-GAR
      *                MOVE WB05-ID-RICH-ESTRAZ-MAS
      *                  TO B05-ID-RICH-ESTRAZ-MAS
      *
      *-->        TIPO OPERAZIONE DISPATCHER
      *                SET IDSI0011-UPDATE TO TRUE
      *
      *-->        CANCELLAZIONE
      *           WHEN WB05-ST-DEL
      *
      *                MOVE WB05-ID-TRCH-DI-GAR
      *                  TO B05-ID-TRCH-DI-GAR
      *                MOVE WB05-ID-RICH-ESTRAZ-MAS
      *                  TO B05-ID-RICH-ESTRAZ-MAS
      *
      *-->             TIPO OPERAZIONE DISPATCHER
      *                SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

      *-->    VALORIZZA DCLGEN ADESIONE
              PERFORM VAL-DCLGEN-B05
                 THRU VAL-DCLGEN-B05-EX

      *-->    VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
              IF WB05-ST-ADD
                 PERFORM VALORIZZA-AREA-DSH-B05
                    THRU VALORIZZA-AREA-DSH-B05-EX
      *-->    CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX
              END-IF

           END-IF.

       SCRIVI-BIL-VAR-DI-CALC-T-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-B05.

      *--> NOME TABELLA FISICA DB
           MOVE 'BILA-VAR-CALC-T'         TO WK-TABELLA.

      *--> DCLGEN TABELLA
           MOVE BILA-VAR-CALC-T             TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-PRIMARY-KEY        TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-SENZA-STOR  TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE IDSV0001-DATA-EFFETTO   TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-B05-EX.
           EXIT.

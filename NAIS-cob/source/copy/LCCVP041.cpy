      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA STAT_RICH_EST
      *   ALIAS P04
      *   ULTIMO AGG. 23 FEB 2011
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-STAT-RICH-EST PIC S9(9)     COMP-3.
             07 (SF)-ID-RICH-EST PIC S9(9)     COMP-3.
             07 (SF)-TS-INI-VLDT PIC S9(18)     COMP-3.
             07 (SF)-TS-END-VLDT PIC S9(18)     COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-PRCS PIC X(3).
             07 (SF)-COD-ATTVT PIC X(10).
             07 (SF)-STAT-RICH-EST PIC X(2).
             07 (SF)-FL-STAT-END PIC X(1).
             07 (SF)-FL-STAT-END-NULL REDEFINES
                (SF)-FL-STAT-END   PIC X(1).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-TP-CAUS-SCARTO PIC X(2).
             07 (SF)-TP-CAUS-SCARTO-NULL REDEFINES
                (SF)-TP-CAUS-SCARTO   PIC X(2).
             07 (SF)-DESC-ERR PIC X(250).
             07 (SF)-UTENTE-INS-AGG PIC X(20).
             07 (SF)-COD-ERR-SCARTO PIC X(10).
             07 (SF)-COD-ERR-SCARTO-NULL REDEFINES
                (SF)-COD-ERR-SCARTO   PIC X(10).
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ-NULL REDEFINES
                (SF)-ID-MOVI-CRZ   PIC X(5).

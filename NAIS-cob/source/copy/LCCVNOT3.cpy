
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVNOT3
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-NOT.
           MOVE NOT-ID-NOTE-OGG
             TO (SF)-ID-PTF
           MOVE NOT-ID-NOTE-OGG
             TO (SF)-ID-NOTE-OGG
           MOVE NOT-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE NOT-ID-OGG
             TO (SF)-ID-OGG
           MOVE NOT-TP-OGG
             TO (SF)-TP-OGG
           MOVE NOT-NOTA-OGG
             TO (SF)-NOTA-OGG
           MOVE NOT-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE NOT-DS-VER
             TO (SF)-DS-VER
           MOVE NOT-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE NOT-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE NOT-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB.
       VALORIZZA-OUTPUT-NOT-EX.
           EXIT.

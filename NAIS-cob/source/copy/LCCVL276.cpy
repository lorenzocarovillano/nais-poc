      *----------------------------------------------------------------*
      *    COPY      ..... LCCVRIC6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO PREVENTIVO
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVL275 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN PREVENTIVO (LCCVL271)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-PREV.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE PREV

      *--> NOME TABELLA FISICA DB
           MOVE 'PREV'                        TO WK-TABELLA


      *--> SE LO STATUS NON E' "INVARIATO"
           IF  NOT WL27-ST-INV
           AND WL27-ELE-PREVENTIVO-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WL27-ST-ADD

                       MOVE WL27-ID-PTF         TO  L27-ID-PREV
                                                    WL27-ID-PREV

                       IF WL27-ID-OGG-NULL = HIGH-VALUES
                          MOVE WL27-ID-OGG-NULL  TO L27-ID-OGG-NULL
                       ELSE
      *-->                PREPARAZIONE ED ESTRAZIONE OGGETTO PTF
                          PERFORM PREPARA-AREA-LCCS0234
                              THRU PREPARA-AREA-LCCS0234-EX

                          PERFORM CALL-LCCS0234
                             THRU CALL-LCCS0234-EX

                          IF IDSV0001-ESITO-OK
                             MOVE S234-ID-OGG-PTF-EOC  TO L27-ID-OGG
                          END-IF
                       END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-INSERT       TO TRUE

      *-->        MODIFICA
                  WHEN WL27-ST-MOD

                     MOVE WL27-ID-PREV          TO L27-ID-PREV
                     IF WL27-ID-OGG-NULL = HIGH-VALUES
                        MOVE WL27-ID-OGG-NULL   TO L27-ID-OGG-NULL
                     ELSE
                        MOVE WL27-ID-OGG        TO L27-ID-OGG
                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-UPDATE       TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WL27-ST-DEL

                     MOVE WL27-ID-PREV          TO L27-ID-PREV
                     IF WL27-ID-OGG-NULL = HIGH-VALUES
                        MOVE WL27-ID-OGG-NULL   TO L27-ID-OGG-NULL
                     ELSE
                        MOVE WL27-ID-OGG        TO L27-ID-OGG
                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-DELETE       TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK
      *-->       VALORIZZA DCLGEN PREVENTIVO
                 PERFORM VAL-DCLGEN-L27
                    THRU VAL-DCLGEN-L27-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-L27
                    THRU VALORIZZA-AREA-DSH-L27-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX
              END-IF

           END-IF.

       AGGIORNA-PREV-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-L27.

      *--> DCLGEN TABELLA
           MOVE PREV                    TO IDSI0011-BUFFER-DATI.

           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-PRIMARY-KEY      TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-L27-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    PREPARA AREA PER CALL LCCS0234
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234.

           MOVE WL27-ID-OGG             TO S234-ID-OGG-EOC.
           MOVE WL27-TP-OGG             TO S234-TIPO-OGG-EOC.

       PREPARA-AREA-LCCS0234-EX.
           EXIT.

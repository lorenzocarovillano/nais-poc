      *----------------------------------------------------------------*
      * DIMENSIONAMENTO OCCURS PER LA TAB. GAR                         *
      * COPY RIPORTANTE IL NUMERO DI OCCURS DA UTILIZZARE              *
      * PER INIZIALIZZA TOT DELLA TABELLA                              *
      *----------------------------------------------------------------*
       01 WK-GRZ-MAX.
          03 WK-GRZ-MAX-A                 PIC 9(04) VALUE 1.
          03 WK-GRZ-MAX-B                 PIC 9(04) VALUE 20.

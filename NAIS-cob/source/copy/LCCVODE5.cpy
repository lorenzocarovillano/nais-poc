
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVODE5
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-ODE.
           IF (SF)-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL
              TO ODE-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU
              TO ODE-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF NOT NUMERIC
              MOVE 0 TO ODE-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF
              TO ODE-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF NOT NUMERIC
              MOVE 0 TO ODE-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF
              TO ODE-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA
              TO ODE-COD-COMP-ANIA
           MOVE (SF)-TP-OGG
              TO ODE-TP-OGG
           IF (SF)-IB-OGG-NULL = HIGH-VALUES
              MOVE (SF)-IB-OGG-NULL
              TO ODE-IB-OGG-NULL
           ELSE
              MOVE (SF)-IB-OGG
              TO ODE-IB-OGG
           END-IF
           MOVE (SF)-TP-DEROGA
              TO ODE-TP-DEROGA
           MOVE (SF)-COD-GR-AUT-APPRT
              TO ODE-COD-GR-AUT-APPRT
           IF (SF)-COD-LIV-AUT-APPRT-NULL = HIGH-VALUES
              MOVE (SF)-COD-LIV-AUT-APPRT-NULL
              TO ODE-COD-LIV-AUT-APPRT-NULL
           ELSE
              MOVE (SF)-COD-LIV-AUT-APPRT
              TO ODE-COD-LIV-AUT-APPRT
           END-IF
           MOVE (SF)-COD-GR-AUT-SUP
              TO ODE-COD-GR-AUT-SUP
           MOVE (SF)-COD-LIV-AUT-SUP
              TO ODE-COD-LIV-AUT-SUP
           IF (SF)-DS-RIGA NOT NUMERIC
              MOVE 0 TO ODE-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA
              TO ODE-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO ODE-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO ODE-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO ODE-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ NOT NUMERIC
              MOVE 0 TO ODE-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ
              TO ODE-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ NOT NUMERIC
              MOVE 0 TO ODE-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ
              TO ODE-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO ODE-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO ODE-DS-STATO-ELAB.
       VAL-DCLGEN-ODE-EX.
           EXIT.

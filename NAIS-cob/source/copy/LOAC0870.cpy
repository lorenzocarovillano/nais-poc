      *----------------------------------------------------------------*
      *   PROCESSO DI POST-VENDITA - PORTAFOGLIO VITA
      *   AREA VARIABILI E COSTANTI DI INPUT
      *   SERVIZIO DI RIVALUTAZIONE
      *----------------------------------------------------------------*
           05 (SF)-AREA-XX0-VAR-INP.
      *
              07 (SF)-DATI-GGINC.
                 09 (SF)-DATI-GGINCASSO-CONTESTO   PIC X(02).
                    88 (SF)-GGCONTESTO-CONT-SI       VALUE 'SI'.
                    88 (SF)-GGCONTESTO-CONT-NO       VALUE 'NO'.
                 09 (SF)-GGINCASSO                 PIC  9(5).
      *
      *--> NON E STATO REWORKATO PERCHE LA RIVALUTAZIONE UTILIZZA SOLO
      *--> LE GARANZIE DI RAMO I
              07 (SF)-AREA-TGA-TIT.
                 08 (SF)-TGA-NUM-MAX-ELE           PIC S9(4).
                 08 (SF)-TAB-TIT.
                    09 (SF)-DATI-TGA           OCCURS 300.
                       10 (SF)-ID-TRCH-DI-GAR      PIC S9(9) COMP-3.
                       10 (SF)-TIT-NUM-MAX-ELE     PIC S9(4).
                       10 (SF)-DATI-TIT        OCCURS 12.
                          15 (SF)-ID-TIT-CONT      PIC S9(9) COMP-3.
                          15 (SF)-DT-VLT           PIC S9(8) COMP-3.
                          15 (SF)-DT-VLT-NULL      REDEFINES
                             (SF)-DT-VLT           PIC  X(5).
                          15 (SF)-DT-INI-EFF       PIC S9(8) COMP-3.
                          15 (SF)-TP-STAT-TIT      PIC  X(2).
                 08 (SF)-TAB-TIT-R REDEFINES (SF)-TAB-TIT.
                    09  FILLER                     PIC X(213).
                    09  (SF)-RESTO-TAB-TIT         PIC X(63687).


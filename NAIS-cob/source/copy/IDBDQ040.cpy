           EXEC SQL DECLARE COMP_QUEST TABLE
           (
             ID_COMP_QUEST       DECIMAL(9, 0) NOT NULL,
             DT_INI_EFF          DATE NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             COD_CAN             DECIMAL(5, 0),
             DT_END_EFF          DATE NOT NULL,
             COD_QUEST           CHAR(20) NOT NULL,
             COD_DOMANDA         CHAR(20) NOT NULL,
             ORDINE_DOMANDA      DECIMAL(5, 0) NOT NULL,
             COD_RISP            CHAR(20) NOT NULL,
             OBBL_RISP           CHAR(1)
          ) END-EXEC.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA VAR_FUNZ_DI_CALC
      *   ALIAS VFC
      *   ULTIMO AGG. 18 SET 2007
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-DATI.
             07 (SF)-IDCOMP PIC S9(4) COMP-5.
             07 (SF)-CODPROD PIC X(12).
             07 (SF)-CODTARI PIC X(12).
             07 (SF)-DINIZ PIC S9(9) COMP-5.
             07 (SF)-NOMEFUNZ PIC X(12).
             07 (SF)-DEND PIC S9(9) COMP-5.
             07 (SF)-ISPRECALC PIC S9(4) COMP-5.
             07 (SF)-ISPRECALC-NULL REDEFINES
                (SF)-ISPRECALC   PIC X(2).
             07 (SF)-NOMEPROGR PIC X(12).
             07 (SF)-MODCALC PIC X(12).
             07 (SF)-GLOVARLIST PIC X(4000).
             07 (SF)-STEP-ELAB PIC X(20).

      ******************************************************************
      *    TP_MOVI (DA 8000 A 8999)
      ******************************************************************
           88 BOLL-EMES-COL                           VALUE 8000.
           88 BOLL-PERFEZ-COL                         VALUE 8001.
           88 BOLL-RISC-SCAD-PAG-COL                  VALUE 8002.
           88 BOLL-SINIS-DENUN-COL                    VALUE 8003.
           88 BOLL-SINIS-NO-LIQ-COL                   VALUE 8004.
           88 BOLL-STORNI-COL                         VALUE 8005.
           88 BOLL-QUIETANZE-COL                      VALUE 8006.
           88 BOLL-RISCATTATI-COL                     VALUE 8007.
           88 BOLL-TRASFORMATI-COL                    VALUE 8008.
           88 BOLL-EMES-IND                           VALUE 8010.
           88 BOLL-QUIETANZE-IND                      VALUE 8011.
           88 BOLL-STORNI-SCAD-IND                    VALUE 8012.
           88 BOLL-SINIS-DENUN-IND                    VALUE 8013.
           88 BOLL-RISCATTATI-IND                     VALUE 8014.
           88 BOLL-TRASFORMATI-IND                    VALUE 8020.
           88 ANAG-TRIBUT-05-92-IND                   VALUE 8030.
           88 ANAG-TRIBUT-05-93-IND                   VALUE 8031.
           88 ANAG-TRIBUT-05-92-COL                   VALUE 8032.
           88 ANAG-TRIBUT-05-93-COL                   VALUE 8033.
           88 PRE-INC-MENSILE-COL                     VALUE 8034.
           88 PRE-INC-ANNUALE-COL                     VALUE 8035.
           88 PRE-INC-MENSILE-IND                     VALUE 8036.
           88 PRE-INC-TRIMESTR-IND                    VALUE 8037.
           88 PRE-INC-ANNUALE-IND                     VALUE 8038.
           88 COMUNIC-INGRESSO-FUG                    VALUE 8050.
           88 COMUNIC-VARIAZIONE-FUG                  VALUE 8051.

           88 COASS-DELEGA-PROPRIA                    VALUE 8100.
      * CENSITO UN NUOVO TIPO MOVIMENTO
           88 RINNOVO-DIFFERIM                        VALUE 8402.
           88 STO-ALTRE-CAU-POL-IND                   VALUE 8452.

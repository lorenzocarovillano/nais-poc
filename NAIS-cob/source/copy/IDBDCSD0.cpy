           EXEC SQL DECLARE COMP_STR_DATO TABLE
           (
             COD_COMPAGNIA_ANIA  DECIMAL(5, 0) NOT NULL,
             COD_STR_DATO        CHAR(30) NOT NULL,
             POSIZIONE           DECIMAL(5, 0) NOT NULL,
             COD_STR_DATO2       CHAR(30),
             COD_DATO            CHAR(30),
             FLAG_KEY            CHAR(1),
             FLAG_RETURN_CODE    CHAR(1),
             FLAG_CALL_USING     CHAR(1),
             FLAG_WHERE_COND     CHAR(1),
             FLAG_REDEFINES      CHAR(1),
             TIPO_DATO           CHAR(2),
             LUNGHEZZA_DATO      DECIMAL(5, 0),
             PRECISIONE_DATO     DECIMAL(2, 0),
             COD_DOMINIO         CHAR(30),
             FORMATTAZIONE_DATO  CHAR(20),
             SERVIZIO_CONVERS    CHAR(8),
             AREA_CONV_STANDARD  CHAR(1),
             RICORRENZA          DECIMAL(5, 0),
             OBBLIGATORIETA      CHAR(1),
             VALORE_DEFAULT      CHAR(10),
             DS_UTENTE           CHAR(20) NOT NULL WITH DEFAULT
          ) END-EXEC.

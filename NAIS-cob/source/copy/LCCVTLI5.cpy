
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVTLI5
      *   ULTIMO AGG. 09 LUG 2009
      *------------------------------------------------------------

       VAL-DCLGEN-TLI.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TLI) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TLI)
              TO TLI-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-TLI)
              TO TLI-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-TLI) NOT NUMERIC
              MOVE 0 TO TLI-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-TLI)
              TO TLI-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-TLI) NOT NUMERIC
              MOVE 0 TO TLI-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-TLI)
              TO TLI-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-TLI)
              TO TLI-COD-COMP-ANIA
           MOVE (SF)-IMP-LRD-CALC(IX-TAB-TLI)
              TO TLI-IMP-LRD-CALC
           IF (SF)-IMP-LRD-DFZ-NULL(IX-TAB-TLI) = HIGH-VALUES
              MOVE (SF)-IMP-LRD-DFZ-NULL(IX-TAB-TLI)
              TO TLI-IMP-LRD-DFZ-NULL
           ELSE
              MOVE (SF)-IMP-LRD-DFZ(IX-TAB-TLI)
              TO TLI-IMP-LRD-DFZ
           END-IF
           MOVE (SF)-IMP-LRD-EFFLQ(IX-TAB-TLI)
              TO TLI-IMP-LRD-EFFLQ
           MOVE (SF)-IMP-NET-CALC(IX-TAB-TLI)
              TO TLI-IMP-NET-CALC
           IF (SF)-IMP-NET-DFZ-NULL(IX-TAB-TLI) = HIGH-VALUES
              MOVE (SF)-IMP-NET-DFZ-NULL(IX-TAB-TLI)
              TO TLI-IMP-NET-DFZ-NULL
           ELSE
              MOVE (SF)-IMP-NET-DFZ(IX-TAB-TLI)
              TO TLI-IMP-NET-DFZ
           END-IF
           MOVE (SF)-IMP-NET-EFFLQ(IX-TAB-TLI)
              TO TLI-IMP-NET-EFFLQ
           IF (SF)-IMP-UTI-NULL(IX-TAB-TLI) = HIGH-VALUES
              MOVE (SF)-IMP-UTI-NULL(IX-TAB-TLI)
              TO TLI-IMP-UTI-NULL
           ELSE
              MOVE (SF)-IMP-UTI(IX-TAB-TLI)
              TO TLI-IMP-UTI
           END-IF
           IF (SF)-COD-TARI-NULL(IX-TAB-TLI) = HIGH-VALUES
              MOVE (SF)-COD-TARI-NULL(IX-TAB-TLI)
              TO TLI-COD-TARI-NULL
           ELSE
              MOVE (SF)-COD-TARI(IX-TAB-TLI)
              TO TLI-COD-TARI
           END-IF
           IF (SF)-COD-DVS-NULL(IX-TAB-TLI) = HIGH-VALUES
              MOVE (SF)-COD-DVS-NULL(IX-TAB-TLI)
              TO TLI-COD-DVS-NULL
           ELSE
              MOVE (SF)-COD-DVS(IX-TAB-TLI)
              TO TLI-COD-DVS
           END-IF
           IF (SF)-IAS-ONER-PRVNT-FIN-NULL(IX-TAB-TLI) = HIGH-VALUES
              MOVE (SF)-IAS-ONER-PRVNT-FIN-NULL(IX-TAB-TLI)
              TO TLI-IAS-ONER-PRVNT-FIN-NULL
           ELSE
              MOVE (SF)-IAS-ONER-PRVNT-FIN(IX-TAB-TLI)
              TO TLI-IAS-ONER-PRVNT-FIN
           END-IF
           IF (SF)-IAS-MGG-SIN-NULL(IX-TAB-TLI) = HIGH-VALUES
              MOVE (SF)-IAS-MGG-SIN-NULL(IX-TAB-TLI)
              TO TLI-IAS-MGG-SIN-NULL
           ELSE
              MOVE (SF)-IAS-MGG-SIN(IX-TAB-TLI)
              TO TLI-IAS-MGG-SIN
           END-IF
           IF (SF)-IAS-RST-DPST-NULL(IX-TAB-TLI) = HIGH-VALUES
              MOVE (SF)-IAS-RST-DPST-NULL(IX-TAB-TLI)
              TO TLI-IAS-RST-DPST-NULL
           ELSE
              MOVE (SF)-IAS-RST-DPST(IX-TAB-TLI)
              TO TLI-IAS-RST-DPST
           END-IF
           IF (SF)-IMP-RIMB-NULL(IX-TAB-TLI) = HIGH-VALUES
              MOVE (SF)-IMP-RIMB-NULL(IX-TAB-TLI)
              TO TLI-IMP-RIMB-NULL
           ELSE
              MOVE (SF)-IMP-RIMB(IX-TAB-TLI)
              TO TLI-IMP-RIMB
           END-IF
           IF (SF)-COMPON-TAX-RIMB-NULL(IX-TAB-TLI) = HIGH-VALUES
              MOVE (SF)-COMPON-TAX-RIMB-NULL(IX-TAB-TLI)
              TO TLI-COMPON-TAX-RIMB-NULL
           ELSE
              MOVE (SF)-COMPON-TAX-RIMB(IX-TAB-TLI)
              TO TLI-COMPON-TAX-RIMB
           END-IF
           IF (SF)-RIS-MAT-NULL(IX-TAB-TLI) = HIGH-VALUES
              MOVE (SF)-RIS-MAT-NULL(IX-TAB-TLI)
              TO TLI-RIS-MAT-NULL
           ELSE
              MOVE (SF)-RIS-MAT(IX-TAB-TLI)
              TO TLI-RIS-MAT
           END-IF
           IF (SF)-RIS-SPE-NULL(IX-TAB-TLI) = HIGH-VALUES
              MOVE (SF)-RIS-SPE-NULL(IX-TAB-TLI)
              TO TLI-RIS-SPE-NULL
           ELSE
              MOVE (SF)-RIS-SPE(IX-TAB-TLI)
              TO TLI-RIS-SPE
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-TLI) NOT NUMERIC
              MOVE 0 TO TLI-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-TLI)
              TO TLI-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-TLI)
              TO TLI-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-TLI) NOT NUMERIC
              MOVE 0 TO TLI-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-TLI)
              TO TLI-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-TLI) NOT NUMERIC
              MOVE 0 TO TLI-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-TLI)
              TO TLI-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-TLI) NOT NUMERIC
              MOVE 0 TO TLI-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-TLI)
              TO TLI-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-TLI)
              TO TLI-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-TLI)
              TO TLI-DS-STATO-ELAB
           IF (SF)-IAS-PNL-NULL(IX-TAB-TLI) = HIGH-VALUES
              MOVE (SF)-IAS-PNL-NULL(IX-TAB-TLI)
              TO TLI-IAS-PNL-NULL
           ELSE
              MOVE (SF)-IAS-PNL(IX-TAB-TLI)
              TO TLI-IAS-PNL
           END-IF.
       VAL-DCLGEN-TLI-EX.
           EXIT.

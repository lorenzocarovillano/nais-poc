           EXEC SQL DECLARE OGG_IN_COASS TABLE
           (
             ID_OGG_IN_COASS     DECIMAL(9, 0) NOT NULL,
             ID_POLI             DECIMAL(9, 0),
             ID_MOVI_CRZ         DECIMAL(9, 0) NOT NULL,
             ID_MOVI_CHIU        DECIMAL(9, 0),
             DT_INI_EFF          DATE NOT NULL,
             DT_END_EFF          DATE NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             COD_COASS           CHAR(5),
             TP_DLG              CHAR(2) NOT NULL,
             COD_COMP_ANIA_ACQS  DECIMAL(5, 0),
             DS_RIGA             DECIMAL(10, 0) NOT NULL,
             DS_OPER_SQL         CHAR(1) NOT NULL,
             DS_VER              DECIMAL(9, 0) NOT NULL,
             DS_TS_INI_CPTZ      DECIMAL(18, 0) NOT NULL,
             DS_TS_END_CPTZ      DECIMAL(18, 0) NOT NULL,
             DS_UTENTE           CHAR(20) NOT NULL,
             DS_STATO_ELAB       CHAR(1) NOT NULL
          ) END-EXEC.

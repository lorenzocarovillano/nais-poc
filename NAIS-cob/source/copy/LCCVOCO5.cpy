
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVOCO5
      *   ULTIMO AGG. 09 LUG 2010
      *------------------------------------------------------------

       VAL-DCLGEN-OCO.
           MOVE (SF)-TP-OGG-COINV(IX-TAB-OCO)
              TO OCO-TP-OGG-COINV
           MOVE (SF)-ID-OGG-DER(IX-TAB-OCO)
              TO OCO-ID-OGG-DER
           MOVE (SF)-TP-OGG-DER(IX-TAB-OCO)
              TO OCO-TP-OGG-DER
           IF (SF)-IB-RIFTO-ESTNO-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-IB-RIFTO-ESTNO-NULL(IX-TAB-OCO)
              TO OCO-IB-RIFTO-ESTNO-NULL
           ELSE
              MOVE (SF)-IB-RIFTO-ESTNO(IX-TAB-OCO)
              TO OCO-IB-RIFTO-ESTNO
           END-IF
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-OCO)
              TO OCO-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-OCO)
              TO OCO-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-OCO) NOT NUMERIC
              MOVE 0 TO OCO-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-OCO)
              TO OCO-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-OCO) NOT NUMERIC
              MOVE 0 TO OCO-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-OCO)
              TO OCO-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-OCO)
              TO OCO-COD-COMP-ANIA
           MOVE (SF)-COD-PROD(IX-TAB-OCO)
              TO OCO-COD-PROD
           IF (SF)-DT-SCAD-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-DT-SCAD-NULL(IX-TAB-OCO)
              TO OCO-DT-SCAD-NULL
           ELSE
             IF (SF)-DT-SCAD(IX-TAB-OCO) = ZERO
                MOVE HIGH-VALUES
                TO OCO-DT-SCAD-NULL
             ELSE
              MOVE (SF)-DT-SCAD(IX-TAB-OCO)
              TO OCO-DT-SCAD
             END-IF
           END-IF
           MOVE (SF)-TP-COLLGM(IX-TAB-OCO)
              TO OCO-TP-COLLGM
           IF (SF)-TP-TRASF-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-TP-TRASF-NULL(IX-TAB-OCO)
              TO OCO-TP-TRASF-NULL
           ELSE
              MOVE (SF)-TP-TRASF(IX-TAB-OCO)
              TO OCO-TP-TRASF
           END-IF
           IF (SF)-REC-PROV-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-REC-PROV-NULL(IX-TAB-OCO)
              TO OCO-REC-PROV-NULL
           ELSE
              MOVE (SF)-REC-PROV(IX-TAB-OCO)
              TO OCO-REC-PROV
           END-IF
           IF (SF)-DT-DECOR-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-DT-DECOR-NULL(IX-TAB-OCO)
              TO OCO-DT-DECOR-NULL
           ELSE
             IF (SF)-DT-DECOR(IX-TAB-OCO) = ZERO
                MOVE HIGH-VALUES
                TO OCO-DT-DECOR-NULL
             ELSE
              MOVE (SF)-DT-DECOR(IX-TAB-OCO)
              TO OCO-DT-DECOR
             END-IF
           END-IF
           IF (SF)-DT-ULT-PRE-PAG-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-DT-ULT-PRE-PAG-NULL(IX-TAB-OCO)
              TO OCO-DT-ULT-PRE-PAG-NULL
           ELSE
             IF (SF)-DT-ULT-PRE-PAG(IX-TAB-OCO) = ZERO
                MOVE HIGH-VALUES
                TO OCO-DT-ULT-PRE-PAG-NULL
             ELSE
              MOVE (SF)-DT-ULT-PRE-PAG(IX-TAB-OCO)
              TO OCO-DT-ULT-PRE-PAG
             END-IF
           END-IF
           IF (SF)-TOT-PRE-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-TOT-PRE-NULL(IX-TAB-OCO)
              TO OCO-TOT-PRE-NULL
           ELSE
              MOVE (SF)-TOT-PRE(IX-TAB-OCO)
              TO OCO-TOT-PRE
           END-IF
           IF (SF)-RIS-MAT-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-RIS-MAT-NULL(IX-TAB-OCO)
              TO OCO-RIS-MAT-NULL
           ELSE
              MOVE (SF)-RIS-MAT(IX-TAB-OCO)
              TO OCO-RIS-MAT
           END-IF
           IF (SF)-RIS-ZIL-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-RIS-ZIL-NULL(IX-TAB-OCO)
              TO OCO-RIS-ZIL-NULL
           ELSE
              MOVE (SF)-RIS-ZIL(IX-TAB-OCO)
              TO OCO-RIS-ZIL
           END-IF
           IF (SF)-IMP-TRASF-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-IMP-TRASF-NULL(IX-TAB-OCO)
              TO OCO-IMP-TRASF-NULL
           ELSE
              MOVE (SF)-IMP-TRASF(IX-TAB-OCO)
              TO OCO-IMP-TRASF
           END-IF
           IF (SF)-IMP-REINVST-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-IMP-REINVST-NULL(IX-TAB-OCO)
              TO OCO-IMP-REINVST-NULL
           ELSE
              MOVE (SF)-IMP-REINVST(IX-TAB-OCO)
              TO OCO-IMP-REINVST
           END-IF
           IF (SF)-PC-REINVST-RILIEVI-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-PC-REINVST-RILIEVI-NULL(IX-TAB-OCO)
              TO OCO-PC-REINVST-RILIEVI-NULL
           ELSE
              MOVE (SF)-PC-REINVST-RILIEVI(IX-TAB-OCO)
              TO OCO-PC-REINVST-RILIEVI
           END-IF
           IF (SF)-IB-2O-RIFTO-ESTNO-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-IB-2O-RIFTO-ESTNO-NULL(IX-TAB-OCO)
              TO OCO-IB-2O-RIFTO-ESTNO-NULL
           ELSE
              MOVE (SF)-IB-2O-RIFTO-ESTNO(IX-TAB-OCO)
              TO OCO-IB-2O-RIFTO-ESTNO
           END-IF
           IF (SF)-IND-LIQ-AGG-MAN-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-IND-LIQ-AGG-MAN-NULL(IX-TAB-OCO)
              TO OCO-IND-LIQ-AGG-MAN-NULL
           ELSE
              MOVE (SF)-IND-LIQ-AGG-MAN(IX-TAB-OCO)
              TO OCO-IND-LIQ-AGG-MAN
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-OCO) NOT NUMERIC
              MOVE 0 TO OCO-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-OCO)
              TO OCO-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-OCO)
              TO OCO-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-OCO) NOT NUMERIC
              MOVE 0 TO OCO-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-OCO)
              TO OCO-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-OCO) NOT NUMERIC
              MOVE 0 TO OCO-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-OCO)
              TO OCO-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-OCO) NOT NUMERIC
              MOVE 0 TO OCO-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-OCO)
              TO OCO-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-OCO)
              TO OCO-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-OCO)
              TO OCO-DS-STATO-ELAB
           IF (SF)-IMP-COLLG-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-IMP-COLLG-NULL(IX-TAB-OCO)
              TO OCO-IMP-COLLG-NULL
           ELSE
              MOVE (SF)-IMP-COLLG(IX-TAB-OCO)
              TO OCO-IMP-COLLG
           END-IF
           IF (SF)-PRE-PER-TRASF-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-PRE-PER-TRASF-NULL(IX-TAB-OCO)
              TO OCO-PRE-PER-TRASF-NULL
           ELSE
              MOVE (SF)-PRE-PER-TRASF(IX-TAB-OCO)
              TO OCO-PRE-PER-TRASF
           END-IF
           IF (SF)-CAR-ACQ-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-CAR-ACQ-NULL(IX-TAB-OCO)
              TO OCO-CAR-ACQ-NULL
           ELSE
              MOVE (SF)-CAR-ACQ(IX-TAB-OCO)
              TO OCO-CAR-ACQ
           END-IF
           IF (SF)-PRE-1A-ANNUALITA-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-PRE-1A-ANNUALITA-NULL(IX-TAB-OCO)
              TO OCO-PRE-1A-ANNUALITA-NULL
           ELSE
              MOVE (SF)-PRE-1A-ANNUALITA(IX-TAB-OCO)
              TO OCO-PRE-1A-ANNUALITA
           END-IF
           IF (SF)-IMP-TRASFERITO-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-IMP-TRASFERITO-NULL(IX-TAB-OCO)
              TO OCO-IMP-TRASFERITO-NULL
           ELSE
              MOVE (SF)-IMP-TRASFERITO(IX-TAB-OCO)
              TO OCO-IMP-TRASFERITO
           END-IF
           IF (SF)-TP-MOD-ABBINAMENTO-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-TP-MOD-ABBINAMENTO-NULL(IX-TAB-OCO)
              TO OCO-TP-MOD-ABBINAMENTO-NULL
           ELSE
              MOVE (SF)-TP-MOD-ABBINAMENTO(IX-TAB-OCO)
              TO OCO-TP-MOD-ABBINAMENTO
           END-IF
           IF (SF)-PC-PRE-TRASFERITO-NULL(IX-TAB-OCO) = HIGH-VALUES
              MOVE (SF)-PC-PRE-TRASFERITO-NULL(IX-TAB-OCO)
              TO OCO-PC-PRE-TRASFERITO-NULL
           ELSE
              MOVE (SF)-PC-PRE-TRASFERITO(IX-TAB-OCO)
              TO OCO-PC-PRE-TRASFERITO
           END-IF.
       VAL-DCLGEN-OCO-EX.
           EXIT.

      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       LETTURA-STW.

           IF IDSI0011-SELECT
            PERFORM SELECT-STW
                THRU SELECT-STW-EX
         ELSE
            PERFORM FETCH-STW
                THRU FETCH-STW-EX
           END-IF.

       LETTURA-STW-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         SELECT                                 *
      *----------------------------------------------------------------*
       SELECT-STW.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
      *-->    GESTIRE ERRORE DB
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                     MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'SELECT-DISPATCHER'
                                               TO IEAI9901-LABEL-ERR
                     MOVE '005017'             TO IEAI9901-COD-ERRORE
                     MOVE 'STW-ID-STAT-OGG-WF' TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI TO STAT-OGG-WF
                     MOVE 1                    TO IX-TAB-STW
                     PERFORM VALORIZZA-OUTPUT-STW
                        THRU VALORIZZA-OUTPUT-STW-EX
                  WHEN OTHER
      *------  ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM
                                        TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'SELECT-DISPATCHER'
                                        TO IEAI9901-LABEL-ERR
                     MOVE '005015'      TO IEAI9901-COD-ERRORE
                     MOVE WK-TABELLA    TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SELECT-DISPATCHER'       TO IEAI9901-LABEL-ERR
              MOVE '005016'                  TO IEAI9901-COD-ERRORE
              MOVE SPACES        TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.
       SELECT-STW-EX.
           EXIT.
      *----------------------------------------------------------------*
      *                         FETCH                                  *
      *----------------------------------------------------------------*
       FETCH-STW.

             SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
             SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
             SET  WCOM-OVERFLOW-NO                  TO TRUE.

             PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                        OR NOT IDSO0011-SUCCESSFUL-SQL
                        OR WCOM-OVERFLOW-YES

                   PERFORM CALL-DISPATCHER
                      THRU CALL-DISPATCHER-EX

                   IF IDSO0011-SUCCESSFUL-RC
                     EVALUATE TRUE
      *-->               NON TROVATA
                         WHEN IDSO0011-NOT-FOUND
                            IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM
                                TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'FETCH-DISPATCHER'
                                TO IEAI9901-LABEL-ERR
                              MOVE '005019'
                                TO IEAI9901-COD-ERRORE
                              MOVE 'ID-STAT-OGG-WF'
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                            END-IF

                         WHEN IDSO0011-SUCCESSFUL-SQL
      *-->             OPERAZIONE ESEGUITA CORRETTAMENTE
                            MOVE IDSO0011-BUFFER-DATI TO STAT-OGG-WF
                             ADD 1 TO IX-TAB-STW

      *-->  Se il contatore di lettura � maggiore dell' elemento MAX
      *-->  previsto per la TABELLA allora siamo in overflow
                            IF IX-TAB-STW > WSTW-ELE-STAT-OGG-WF-MAX
                              SET WCOM-OVERFLOW-YES TO TRUE
                            ELSE
                              PERFORM VALORIZZA-OUTPUT-STW
                                 THRU VALORIZZA-OUTPUT-STW-EX
                              SET  IDSI0011-FETCH-NEXT TO TRUE
                            END-IF
                         WHEN OTHER
      *-------------------ERRORE DI ACCESSO AL DB
                            MOVE WK-PGM
                              TO IEAI9901-COD-SERVIZIO-BE
                            MOVE 'FETCH-DISPATCHER'
                              TO IEAI9901-LABEL-ERR
                            MOVE '005015'
                              TO IEAI9901-COD-ERRORE
                            MOVE WK-TABELLA
                              TO IEAI9901-PARAMETRI-ERR
                            PERFORM S0300-RICERCA-GRAVITA-ERRORE
                               THRU EX-S0300
                     END-EVALUATE
                   ELSE
      *-->    GESTIRE ERRORE DISPATCHER
                      MOVE WK-PGM
                        TO IEAI9901-COD-SERVIZIO-BE
                      MOVE 'FETCH-DISPATCHER'
                        TO IEAI9901-LABEL-ERR
                      MOVE '005016'
                        TO IEAI9901-COD-ERRORE
                      MOVE SPACES
                        TO IEAI9901-PARAMETRI-ERR
                      PERFORM S0300-RICERCA-GRAVITA-ERRORE
                         THRU EX-S0300
                   END-IF
             END-PERFORM.

       FETCH-STW-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVL274
      *   ULTIMO AGG. 17 NOV 2008
      *------------------------------------------------------------

       INIZIA-TOT-L27.

           PERFORM INIZIA-ZEROES-L27 THRU INIZIA-ZEROES-L27-EX

           PERFORM INIZIA-SPACES-L27 THRU INIZIA-SPACES-L27-EX

           PERFORM INIZIA-NULL-L27 THRU INIZIA-NULL-L27-EX.

       INIZIA-TOT-L27-EX.
           EXIT.

       INIZIA-NULL-L27.
           MOVE HIGH-VALUES TO (SF)-ID-OGG-NULL
           MOVE HIGH-VALUES TO (SF)-TP-OGG-NULL.
       INIZIA-NULL-L27-EX.
           EXIT.

       INIZIA-ZEROES-L27.
           MOVE 0 TO (SF)-ID-PREV
           MOVE 0 TO (SF)-COD-COMP-ANIA
           MOVE 0 TO (SF)-DS-VER
           MOVE 0 TO (SF)-DS-TS-CPTZ.
       INIZIA-ZEROES-L27-EX.
           EXIT.

       INIZIA-SPACES-L27.
           MOVE SPACES TO (SF)-STAT-PREV
           MOVE SPACES TO (SF)-IB-OGG
           MOVE SPACES TO (SF)-DS-OPER-SQL
           MOVE SPACES TO (SF)-DS-UTENTE
           MOVE SPACES TO (SF)-DS-STATO-ELAB.
       INIZIA-SPACES-L27-EX.
           EXIT.

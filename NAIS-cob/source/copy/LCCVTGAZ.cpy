      *----------------------------------------------------------------*
      * DIMENSIONAMENTO OCCURS PER LA TAB. TRCH_DI_GAR                 *
      * COPY RIPORTANTE IL NUMERO DI OCCURS DA UTILIZZARE              *
      * PER INIZIALIZZA TOT DELLA TABELLA                              *
      *----------------------------------------------------------------*
       01 WK-TGA-MAX.
          03 WK-TGA-MAX-A                 PIC 9(04) VALUE 1.
          03 WK-TGA-MAX-B                 PIC 9(04) VALUE 20.
          03 WK-TGA-MAX-C                 PIC 9(04) VALUE 1250.
          03 WK-TGA-MAX-D                 PIC 9(04) VALUE 2000.

       ESEGUI-DISPLAY.

           IF IDSI0011-ANY-TUNING-DBG AND
              IDSV8888-ANY-TUNING-DBG

              IF IDSI0011-TOT-TUNING-DBG
                 IF IDSV8888-STRESS-TEST-DBG
                    CONTINUE
                 ELSE
                    IF IDSV8888-COM-COB-JAV-DBG
                       SET IDSV8888-COM-COB-JAV   TO TRUE
                    ELSE
                       IF IDSV8888-BUSINESS-DBG
                          SET IDSV8888-BUSINESS   TO TRUE
                       ELSE
                          IF IDSV8888-ARCH-BATCH-DBG
                             SET IDSV8888-ARCH-BATCH TO TRUE
                          ELSE
                             MOVE 'ERRO'   TO IDSV8888-FASE
                          END-IF
                       END-IF
                    END-IF
                 END-IF

                 CALL IDSV8888-CALL-TIMESTAMP
                      USING IDSV8888-TIMESTAMP

                 SET IDSV8888-DISPLAY-ADDRESS
                     TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                 CALL IDSV8888-CALL-DISPLAY
                      USING IDSV8888-DISPLAY-ADDRESS
                 INITIALIZE IDSV8888-STR-PERFORMANCE-DBG

              ELSE

                 IF IDSV8888-LIVELLO-DEBUG =
                    IDSI0011-LIVELLO-DEBUG

                    IF IDSV8888-STRESS-TEST-DBG
                       CONTINUE
                    ELSE
                       IF IDSV8888-COM-COB-JAV-DBG
                          SET IDSV8888-COM-COB-JAV   TO TRUE
                       ELSE
                          IF IDSV8888-BUSINESS-DBG
                             SET IDSV8888-BUSINESS   TO TRUE
                          ELSE
                             SET IDSV8888-ARCH-BATCH TO TRUE
                          END-IF
                       END-IF
                    END-IF

                    CALL IDSV8888-CALL-TIMESTAMP
                         USING IDSV8888-TIMESTAMP

                    SET IDSV8888-DISPLAY-ADDRESS
                        TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                    CALL IDSV8888-CALL-DISPLAY
                         USING IDSV8888-DISPLAY-ADDRESS
                    INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                 END-IF

              END-IF

           ELSE

              IF IDSI0011-ANY-APPL-DBG AND
                 IDSV8888-ANY-APPL-DBG

                  IF IDSV8888-LIVELLO-DEBUG <=
                     IDSI0011-LIVELLO-DEBUG

                     SET IDSV8888-DISPLAY-ADDRESS
                         TO ADDRESS OF IDSV8888-AREA-DISPLAY
                     CALL IDSV8888-CALL-DISPLAY
                         USING IDSV8888-DISPLAY-ADDRESS

                     MOVE SPACES TO IDSV8888-AREA-DISPLAY

                  END-IF

              END-IF
           END-IF.

           SET IDSV8888-NO-DEBUG              TO TRUE.

       ESEGUI-DISPLAY-EX.
           EXIT.

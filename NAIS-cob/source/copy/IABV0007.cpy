       77 WK-SQLCODE                      PIC --------9.

       01  WK-ERRORE                      PIC X(001)  VALUE 'N'.
          88 WK-ERRORE-YES                VALUE 'Y'.
          88 WK-ERRORE-NO                 VALUE 'N'.

       01 WK-TIPO-ERRORE                  PIC X(02).
          88 WK-ERRORE-BLOCCANTE          VALUE 'BL'.
          88 WK-ERRORE-NON-BLOCCANTE      VALUE 'NB'.
          88 WK-ERRORE-FATALE             VALUE 'FT'.

       77  WK-PGM-ERRORE                  PIC X(008)  VALUE SPACES.
       77  WK-PGM-GUIDA                   PIC X(008)  VALUE SPACES.
       77  WK-PGM-BUSINESS                PIC X(008)  VALUE SPACES.


       77  WK-NOME-TABELLA                PIC X(015).
       77  WK-LABEL                       PIC X(030).

       77 YES                             PIC X(001)  VALUE 'Y'.
       77 ESSE                            PIC X(001)  VALUE 'S'.
       77 ENNE                            PIC X(001)  VALUE 'N'.

       77 MAX-VALUE                       PIC S9(9)V  COMP-3
                                          VALUE 999999999.

       77 WK-VERSIONING                   PIC S9(9)V  COMP-3.

       01 FLAG-BATCH-EXECUTOR-STD         PIC X(01).
          88 BATCH-EXECUTOR-STD-SI        VALUE 'S'.
          88 BATCH-EXECUTOR-STD-NO        VALUE 'N'.

       01 WK-LIVELLO-GRAVITA.
          05 MESSAGGIO-INFORMATIVO        PIC 9(01) VALUE 1.
          05 WARNING                      PIC 9(01) VALUE 2.
          05 ERRORE-BLOCCANTE             PIC 9(01) VALUE 3.
          05 ERRORE-FATALE                PIC 9(01) VALUE 4.

       01 WK-PROTOCOL                     PIC X(0001) VALUE 'N'.
          88 WK-PROTOCOL-YES              VALUE 'Y'.
          88 WK-PROTOCOL-NO               VALUE 'N'.

       01 WK-SIMULAZIONE                  PIC X(0001) VALUE 'N'.
          88 WK-SIMULAZIONE-INFR          VALUE 'I'.
          88 WK-SIMULAZIONE-FITTIZIA      VALUE 'S'.
          88 WK-SIMULAZIONE-APPL          VALUE 'A'.
          88 WK-SIMULAZIONE-NO            VALUE 'N'.

       01 WK-PRIMA-BUFFER-SK-TROVATO      PIC X(0001) VALUE 'N'.
          88 PRIMA-BUFFER-SK-TROVATO-YES  VALUE 'Y'.
          88 PRIMA-BUFFER-SK-TROVATO-NO   VALUE 'N'.

       01 WK-PRIMA-VOLTA                  PIC X(0001) VALUE 'Y'.
          88 PRIMA-VOLTA-YES              VALUE 'Y'.
          88 PRIMA-VOLTA-NO               VALUE 'N'.

       01 WK-PRIMA-VOLTA-SUSPEND          PIC X(0001) VALUE 'Y'.
          88 PRIMA-VOLTA-SUSP-YES         VALUE 'Y'.
          88 PRIMA-VOLTA-SUSP-NO          VALUE 'N'.

       01 WK-POST-GUIDE-X-JOB-NOT-FOUND   PIC X(0001) VALUE 'Y'.
          88 POST-GUIDE-X-JOB-NOT-FOUND-YES VALUE 'Y'.
          88 POST-GUIDE-X-JOB-NOT-FOUND-NO  VALUE 'N'.

       01 WK-FLAG-PROCESS-TYPE            PIC X(0003).
          88 WK-STD-TYPE-REC-NO           VALUE 'TRN'.
          88 WK-STD-TYPE-REC-YES          VALUE 'TRY'.
          88 WK-GUIDE-AD-HOC              VALUE 'HOC'.

       01 WK-FLAG-GUIDE-TYPE              PIC X(002).
          88 SEQUENTIAL-GUIDE             VALUE 'SQ'.
          88 DB-GUIDE                     VALUE 'DB'.

       01 WK-BAT-COLLECTION-KEY           PIC X(001) VALUE 'N'.
          88 WK-BAT-COLLECTION-KEY-OK     VALUE 'Y'.
          88 WK-BAT-COLLECTION-KEY-KO     VALUE 'N'.

       01 WK-JOB-COLLECTION-KEY           PIC X(001) VALUE 'N'.
          88 WK-JOB-COLLECTION-KEY-OK     VALUE 'Y'.
          88 WK-JOB-COLLECTION-KEY-KO     VALUE 'N'.

       01 WK-OPEN-PARAM                   PIC X(001) VALUE 'N'.
          88 FILE-PARAM-APERTO            VALUE 'Y'.
          88 FILE-PARAM-CHIUSO            VALUE 'N'.

       01 WK-OPEN-REPORT                   PIC X(001) VALUE 'N'.
          88 FILE-REPORT01-APERTO          VALUE 'Y'.
          88 FILE-REPORT01-CHIUSO          VALUE 'N'.

       01 WK-EOF-PARAM                    PIC X(001)  VALUE 'N'.
          88 FILE-PARAM-FINITO            VALUE 'Y'.
          88 FILE-PARAM-NON-FINITO        VALUE 'N'.

       01 FLAG-INIZIO-LETTURA             PIC X(01) VALUE 'N'.
          88 INIZIO-LETT-LUNG-YES         VALUE 'Y'.
          88 INIZIO-LETT-LUNG-NO          VALUE 'N'.

       01 FLAG-FINE-LETTURA               PIC X(01) VALUE 'N'.
          88 FINE-LETT-LUNG-YES           VALUE 'Y'.
          88 FINE-LETT-LUNG-NO            VALUE 'N'.

       01 FLAG-POSIZ-PARAM                PIC X(01) VALUE 'N'.
          88 POSIZ-PARAM-YES              VALUE 'Y'.
          88 POSIZ-PARAM-NO               VALUE 'N'.

       01 FLAG-VERIFICA-BATCH             PIC X(01) VALUE 'N'.
          88 WK-VERIFICA-BATCH-OK         VALUE 'Y'.
          88 WK-VERIFICA-BATCH-KO         VALUE 'N'.

       01 FLAG-RISULT-VERIF-BATCH-STATE   PIC X(01) VALUE 'N'.
          88 WK-RISULT-VERIF-BATCH-STATE-OK VALUE 'Y'.
          88 WK-RISULT-VERIF-BATCH-STATE-KO VALUE 'N'.

       01 FLAG-RISULT-VERIF-ELAB-STATE   PIC X(01) VALUE 'N'.
          88 WK-RISULT-VERIF-ELAB-STATE-OK VALUE 'Y'.
          88 WK-RISULT-VERIF-ELAB-STATE-KO VALUE 'N'.

       01 FLAG-WK-BATCH-STATE-FOUND       PIC X(01) VALUE 'N'.
          88 WK-BATCH-STATE-FOUND-YES     VALUE 'Y'.
          88 WK-BATCH-STATE-FOUND-NO      VALUE 'N'.

       01 FLAG-VERIFICA-PRENOTAZIONE      PIC X(01) VALUE 'N'.
          88 WK-VERIFICA-PRENOTAZIONE-OK  VALUE 'Y'.
          88 WK-VERIFICA-PRENOTAZIONE-KO  VALUE 'N'.

       01 FLAG-ELABORA-BATCH              PIC X(01) VALUE 'N'.
          88 WK-ELABORA-BATCH-YES         VALUE 'Y'.
          88 WK-ELABORA-BATCH-NO          VALUE 'N'.

       01 FLAG-AGGIORNA-PRENOTAZ          PIC X(01) VALUE 'N'.
          88 WK-AGGIORNA-PRENOTAZ-YES     VALUE 'Y'.
          88 WK-AGGIORNA-PRENOTAZ-NO      VALUE 'N'.

       01 FLAG-CONFERMA-BATCH-ESEGUITO     PIC X(01) VALUE 'N'.
          88 WK-CONFERMA-BATCH-ESEGUITO-OK VALUE 'Y'.
          88 WK-CONFERMA-BATCH-ESEGUITO-KO VALUE 'N'.

       01 FLAG-SKIP-BATCH                 PIC X(01) VALUE 'N'.
          88 WK-SKIP-BATCH-YES            VALUE 'Y'.
          88 WK-SKIP-BATCH-NO             VALUE 'N'.

       01 FLAG-ESTRAI-JOB                 PIC X(01) VALUE 'N'.
          88 WK-ESTRAI-JOB-YES            VALUE 'Y'.
          88 WK-ESTRAI-JOB-NO             VALUE 'N'.

       01 FLAG-ESTRAI-REC                 PIC X(01) VALUE 'N'.
          88 WK-ESTRAI-REC-YES            VALUE 'Y'.
          88 WK-ESTRAI-REC-NO             VALUE 'N'.

       01 FLAG-TROVATO-NOK                PIC X(01) VALUE 'N'.
          88 WK-TROVATO-NOK-YES           VALUE 'Y'.
          88 WK-TROVATO-NOK-NO            VALUE 'N'.

       01 FLAG-FOUND-NOT-FOUND            PIC X(01) VALUE 'N'.
          88 WK-NOT-FOUND-ALLOWED         VALUE 'Y'.
          88 WK-NOT-FOUND-NOT-ALLOWED     VALUE 'N'.

       01 FLAG-FINE-BTC-BATCH             PIC X(01) VALUE 'N'.
          88 WK-FINE-BTC-BATCH-YES        VALUE 'Y'.
          88 WK-FINE-BTC-BATCH-NO         VALUE 'N'.

       01 FLAG-LANCIA-BUSINESS            PIC X(01) VALUE 'N'.
          88 WK-LANCIA-BUSINESS-YES       VALUE 'Y'.
          88 WK-LANCIA-BUSINESS-NO        VALUE 'N'.

       01 FLAG-LANCIA-ROTTURA             PIC X(01) VALUE 'N'.
          88 WK-LANCIA-ROTTURA-YES        VALUE 'Y'.
          88 WK-LANCIA-ROTTURA-NO         VALUE 'N'.

       01 FLAG-FINE-ALIMENTAZIONE         PIC X(01) VALUE 'N'.
          88 WK-FINE-ALIMENTAZIONE-YES    VALUE 'Y'.
          88 WK-FINE-ALIMENTAZIONE-NO     VALUE 'N'.

       01 FLAG-ALLINEA-STATI              PIC X(02).
          88 WK-ALLINEA-STATI-OK          VALUE 'OK'.
          88 WK-ALLINEA-STATI-KO          VALUE 'KO'.

       01 FLAG-GESTIONE-LANCI-BUS         PIC X(02) VALUE 'RT'.
          88 GESTIONE-REAL-TIME           VALUE 'RT'.
          88 GESTIONE-SUSPEND             VALUE 'SU'.

       01 FLAG-VERIFICA-FINALE-JOBS       PIC X(01).
          88 VERIFICA-FINALE-JOBS-SI      VALUE 'S'.
          88 VERIFICA-FINALE-JOBS-NO      VALUE 'N'.

       01 WK-COMMIT-FREQUENCY             PIC S9(9) COMP-5.
          88 INIT-COMMIT-FREQUENCY        VALUE 1.

       77 WK-PRIMO                        PIC 9(02) VALUE 1.

       77 WK-DES-BATCH-STATE              PIC X(50) VALUE SPACES.


       01 FLAG-TIPO-PGM                   PIC X(01) VALUE 'A'.
          88 WK-PGM-ARCHITECTURE          VALUE 'A'.
          88 WK-PGM-BUS-SERVICE           VALUE 'B'.


       01 FLAG-UPDATE-FOR-RESTORE         PIC X(01) VALUE 'N'.
          88 WK-UPDATE-FOR-RESTORE-YES    VALUE 'Y'.
          88 WK-UPDATE-FOR-RESTORE-NO     VALUE 'N'.


       01 FLAG-RETURN-CODE                PIC 9(02) VALUE 00.
          88 WK-RC-00                     VALUE 00.
          88 WK-RC-04                     VALUE 04.
          88 WK-RC-08                     VALUE 08.
          88 WK-RC-12                     VALUE 12.

       01 FLAG-RC-04                      PIC X(01) VALUE '0'.
          88 WK-RC-04-NON-TROVATO         VALUE '0'.
          88 WK-RC-04-NON-FORZATO         VALUE '1'.
          88 WK-RC-04-FORZATO             VALUE '2'.
          88 WK-RC-08-TROVATO             VALUE '3'.


       01 WK-ID-BATCH                     PIC 9(9).
       01 WK-ID-JOB                       PIC 9(9).
       01 WK-ID-JOB-ULT-COMMIT            PIC 9(9).
       01 WK-COD-BATCH-STATE              PIC X(1).
       01 WK-COD-ELAB-STATE               PIC X(1).
       01 WK-ID-RICH                      PIC 9(09).
       01 WK-ID-RICH-X REDEFINES WK-ID-RICH
                                          PIC X(09).

       01 ULTIMO-LANCIO-X-CLOSE-FILE      PIC X(27)
                    VALUE 'ULTIMO LANCIO X CLOSE FILE'.

       01 WS-BUFFER-WHERE-COND.
          05 WS-LUNG-EFF-BLOB    PIC 9(009) VALUE 0.

          05 FLAG-GESTIONE-PARALLELISMO   PIC X(01).
             88 GESTIONE-PARALLELISMO-YES VALUE 'Y'.
             88 GESTIONE-PARALLELISMO-NO  VALUE 'N'.

       01 WS-BUFFER-WHERE-COND-IABS0130.
          05 WS-FLAG-FASE                  PIC X(02).
             88 WS-FASE-CONTROLLO-INIZIALE VALUE 'CI'.
             88 WS-FASE-CONTROLLO-FINALE   VALUE 'CF'.
             88 WS-FASE-INIZIALE           VALUE 'FI'.
             88 WS-FASE-FINALE             VALUE 'FF'.

          05 FLAG-BATCH-COMPLETE      PIC X(02).
             88 BATCH-COMPLETE        VALUE 'CO'.
             88 BATCH-NOT-COMPLETE    VALUE 'NC'.


          05 FLAG-BATCH-STATE         PIC X(02).
             88 BATCH-STATE-OK        VALUE 'OK'.
             88 BATCH-STATE-KO        VALUE 'KO'.

      *---------------------------------------------------------------*
      * AREE per CONNECT / DISCONNECT (IABS0140)
      *---------------------------------------------------------------*
       01 AREA-X-IABS0140.
         10 AREA0140-DATABASE-NAME     PIC X(010).
         10 AREA0140-USER              PIC X(008).
         10 AREA0140-PASSWORD          PIC X(008).

      * -- CAMPI OPERAZIONE
         10 AREA0140-OPERAZIONE        PIC X(015).
            88 AREA0140-CONNECT        VALUE 'CONNECT'.
            88 AREA0140-DISCONNECT     VALUE 'DISCONNECT'.
            88 AREA0140-CONNECT-RESET  VALUE 'CONNECT RESET'.

      ******************************************************************
      * CAMPI COMODO PER GESTIONE ROTTURA CHIAVE
      ******************************************************************
       77 WK-MAX-DATE                    PIC X(10) VALUE '9999-12-31'.
       77 WK-MIN-DATE                    PIC X(10) VALUE '1900-01-01'.

       01 WK-DATABASE-NAME               PIC X(010).
       01 WK-USER                        PIC X(008).
       01 WK-PASSWORD                    PIC X(008).


      ******************************************************************
      * CAMPI COMODO PER GESTIONE ROTTURA CHIAVE
      ******************************************************************
       01 WK-CHIAVE-ROTTURA-ESTER.
          05 WK-IB-OGG-OLD                PIC X(100).


       77 WK-ATTIVO                       PIC X(01) VALUE 'Y'.


           COPY IABV0004.


      *----------------------------------------------------------------*
      *    CAMPI X ROUTINES DI GESTIONE DISPLAY IDSP8888
      *----------------------------------------------------------------*
           COPY IDSV8888.


      ******************************************************************
      *---  CAMPI UTILIZZATI DALLE ROUTINE DI
      *---  CONVERSIONE DATE
      *---  COPY IDSV0010
      ******************************************************************
       77 QUARANTA          PIC 9(02) VALUE 40.
       77 WS-TS-INFINITO    PIC S9(18) COMP-3 VALUE 999912314023595999.
       77 WS-TS-INFINITO-1  PIC S9(18) COMP-3 VALUE 999912304023595999.
       77 WS-HH-INFINITO    PIC 9(08) VALUE 23595999.
       77 WS-DT-INFINITO    PIC 9(08) VALUE 99991231.
       77 WS-DT-INFINITO-1  PIC X(10) VALUE '9999-12-30'.

       01 WS-STR-RISULTATO.
          05 WS-RISULTATO           PIC 9(02).

       77 WS-DIFFERENZA             PIC S9(09) COMP-5.

       77 WS-DATE-SYSTEM-DB         PIC X(10).
       77 WS-TS-SYSTEM-DB           PIC X(26).
       77 WS-TS-SYSTEM              PIC 9(18).

       77 WS-DATA-INIZIO-EFFETTO-DB PIC X(10).
       77 WS-DATA-FINE-EFFETTO-DB   PIC X(10).

       77 WS-TS-COMPETENZA          PIC S9(18) COMP-3.

       77 WS-TS-COMPETENZA-AGG-STOR PIC S9(18) COMP-3.

       77 WS-TS-DISPLAY             PIC 9(18).

      *
       77 WS-TIMESTAMP-ADD          PIC 9(02) VALUE 0.


      *---
      *---  CAMPI UTILIZZATI DALLE ROUTINE DI
      *---  CONVERSIONE DATE
      *---
       01 WS-STR-DATE-N.
         05 WS-DATE-N               PIC 9(08).

       77 WS-DATE-X                 PIC X(10).

      *---
      *---  CAMPI UTILIZZATI DALLE ROUTINE DI
      *---  CONVERSIONE TIMESTAMP
      *---
       01 WS-STR-TIMESTAMP-N.
         05 WS-TIMESTAMP-N          PIC 9(18).

       77 WS-TIMESTAMP-X            PIC X(26).



      ***************************************************************
      *   STRUTTURA STATI BATCH
      ***************************************************************
       01 TABELLA-STATI-BATCH.
         05 TABELLA-STATI-BATCH-ELEMENTS OCCURS 10 TIMES.
            10 BAT-COD-BATCH-STATE       PIC  X(01).
            10 BAT-DES                   PIC  X(50).
            10 BAT-FLAG-TO-EXECUTE       PIC  X(01).

      ***************************************************************
      *   STRUTTURA STATI JOB
      ***************************************************************
       01 TABELLA-STATI-JOB.
         05 TABELLA-STATI-JOB-ELEMENTS OCCURS 10 TIMES.
            10 JOB-COD-ELAB-STATE        PIC  X(01).
            10 JOB-DES                   PIC  X(50).
            10 JOB-FLAG-TO-EXECUTE       PIC  X(01).

       01 WK-CURRENT-PK                  PIC 9(10) COMP-3.


       01 WK-STATI-SOSPESI-STR.
          05 WK-STATI-SOSPESI-ELE OCCURS 50 TIMES.
             10 WK-STATI-SOSP-PK         PIC 9(10) COMP-3.


ULF    01 TIPO-CONV-N-TO-X               PIC X(02) VALUE 'NX'.
ULF       88 CONV-N-TO-X                 VALUE 'NX'.
ULF       88 CONV-X-TO-N                 VALUE 'XN'.

ULF    01 APPO-STATE-X                   PIC X(001).
ULF    01 APPO-STATE-9
ULF        REDEFINES APPO-STATE-X         PIC 9(001).
ULF    01 APPO-STATE-N                   PIC S9(009)  COMP-5.

      ***************************************************************
      * STRUTTURA COMODO PER GESTIONE DI ACCODAMENTO LOG_ERRORE
      ***************************************************************
       01 WK-LOG-ERRORE.
          10 WK-LOR-ID-LOG-ERRORE         PIC S9(9)V     COMP-3.
          10 WK-LOR-ID-GRAVITA-ERRORE     PIC S9(9)V     COMP-3.
          10 WK-LOR-DESC-ERRORE-ESTESA    PIC X(200).
          10 WK-LOR-COD-MAIN-BATCH        PIC X(008).
          10 WK-LOR-COD-SERVIZIO-BE       PIC X(008).
          10 WK-LOR-LABEL-ERR             PIC X(030).
          10 WK-LOR-OPER-TABELLA          PIC X(002).
          10 WK-LOR-NOME-TABELLA          PIC X(018).
          10 WK-LOR-STATUS-TABELLA        PIC X(010).
          10 WK-LOR-KEY-TABELLA           PIC X(100).

      ***************************************************************
      * CAMPI LIMITE
      ***************************************************************
       77 WK-LIMITE-SKEDA-PARAMETRO      PIC 9(02) VALUE 79.
       77 WK-LIMITE-STATI-BATCH          PIC 9(02) VALUE 10.
       77 WK-LIMITE-STATI-JOB            PIC 9(02) VALUE 10.
       77 WK-LIMITE-LOG-ERRORE           PIC 9(03) VALUE 100.
       77 WK-LIMITE-STATI-SOSP           PIC 9(02) VALUE 50.
       01 WK-LIMITE-ARRAY-BLOB           PIC 9(03).
          88 WK-LIMITE-ARRAY-BLOB-1K     VALUE 650.
          88 WK-LIMITE-ARRAY-BLOB-2K     VALUE 500.
          88 WK-LIMITE-ARRAY-BLOB-3K     VALUE 350.
          88 WK-LIMITE-ARRAY-BLOB-10K    VALUE 100.

      ***************************************************************
      * INDICI
      ***************************************************************
       01 IND-MAX-ELE-ERRORI-WK          PIC S9(4) COMP VALUE 0.
       01 IND-MAX-ELE-ERRORI             PIC S9(4) COMP VALUE 0.
       01 IND-INS-ELE-ERRORI             PIC S9(4) COMP VALUE 0.
       01 IND-SEARCH                     PIC 9(02) VALUE 0.
       01 IND-SEARCH-N                   PIC 9(02) VALUE 0.
       01 IND-LUNG                       PIC 9(01) VALUE 0.
       01 IND-POSIZ-PARAM                PIC 9(03) VALUE 1.
       01 IND-STATI-BATCH                PIC 9(02) VALUE 0.
       01 IND-STATI-JOB                  PIC 9(02) VALUE 0.
       01 IND-STATI-V0002                PIC 9(02) VALUE 0.
       01 IND-STATI-SOSP                 PIC 9(02) VALUE 0.



       01 WK-LUNG-PARAM-X                 PIC X(02).
       01 WK-LUNG-PARAM-9 REDEFINES  WK-LUNG-PARAM-X PIC 9(02).

      * Area di contesto del Back End
       01 LCCC0090.
      *     COPY LCCC0090                 REPLACING ==(SF)== BY ==LINK==.
      *-- inizio COPY LCCC0090
           05 LINK-DATI-INPUT.
              10 LINK-CONNECT                   PIC X(01) VALUE 'S'.
              10 LINK-NOME-TABELLA              PIC X(020).

           05 LINK-DATI-OUTPUT.
              10 LINK-SEQ-TABELLA               PIC S9(09) COMP-3.
              10 LINK-RETURN-CODE               PIC X(002).
                 88 LINK-SUCCESSFUL-RC          VALUE '00'.
                 88 LINK-SEQUENCE-NOT-FOUND     VALUE 'S1'.
                 88 LINK-SQL-ERROR              VALUE 'D3'.

              10 LINK-SQLCODE                   PIC S9(09).
                 88 LINK-SUCCESSFUL-SQL    VALUE ZERO.
                 88 LINK-NOT-FOUND         VALUE +100.
                 88 LINK-DUPLICATE-KEY     VALUE -803.
                 88 LINK-MORE-THAN-ONE-ROW VALUE -811.
                 88 LINK-DEADLOCK-TIMEOUT  VALUE -911
                                                 -913.
                 88 LINK-CONNECTION-ERROR  VALUE -924.

              10 LINK-DESCRIZ-ERR-DB2           PIC X(300).
      *-- fine COPY LCCC0090

       01 WK-SEQ-SESSIONE                PIC 9(9)  VALUE ZERO.


      *---> FILE STATUS FILES UTILIZZATI
       77  FS-PARA                        PIC X(002)  VALUE '00'.
       77  FS-REPORT01                    PIC X(002)  VALUE '00'.

      *----------------------------------------------------------------*
      *---> CONTATORI DI ELABORAZIONE PER STATISTICHE FINALI
      *----------------------------------------------------------------*
      *--- NUMERO CONTRATTI LETTI FILE INPUT MOVIMENTO
       77  CNT-NUM-CONTR-LETT             PIC 9(09) VALUE ZEROES.
      *--- NUMERO CONTRATTI ELABORATI CON ESITO OK
       77  CNT-NUM-CONTR-ELAB             PIC 9(09) VALUE ZEROES.
      *--- NUMERO MOVIMENTI LETTI DA INPUT MOVIMENTO
       77  CNT-NUM-OPERAZ-LETTE           PIC 9(09) VALUE ZEROES.

      *----------------------------------------------------------------*
      * DEFINIZIONE AREA DI APPOGGIO PER FORMATTAZIONE DATE:
      *----------------------------------------------------------------*

       01 WK-TIMESTAMP-26                PIC X(26).
       01 WK-TIMESTAMP-26-TS REDEFINES WK-TIMESTAMP-26.
          02 WK-DATA-SISTEMA.
             05 GG-SYS                    PIC X(002).
             05 FILLER                    PIC X(001).
             05 MM-SYS                    PIC X(002).
             05 FILLER                    PIC X(001).
             05 AAAA-SYS                  PIC X(004).
          02 WK-ORA-SISTEMA.
             05 HH-SYS                    PIC X(002).
             05 FILLER                    PIC X(001).
             05 MI-SYS                    PIC X(002).
             05 FILLER                    PIC X(001).
             05 SS-SYS                    PIC X(002).


       77 DATAIN                         PIC S9(007) VALUE ZERO COMP-3.



       01 WK-TIMESTAMP.
          05 WK-TIMES-14                  PIC 9(014).
          05 WK-TIMES-04                  PIC 9(004) VALUE ZEROES.

       01 WK-SESSIONE.
          05 WK-SESSIONE-N09                     PIC 9(009).
          05 WK-SESSIONE-X11                     PIC X(011).


      *----------------------------------------------------------------*
      * CAMPI COMODO GENERICI
      *----------------------------------------------------------------*
       01 WK-COMODO.
          05 COMODO-COD-COMP-ANIA                PIC 9(9).
          05 COMODO-PROG-PROTOCOL                PIC 9(9).

      *----------------------------------------------------------------*
      *----*    G E S T I O N E   E R R O R I                   -------*
      *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - *
                 COPY IEAI9901.
                 COPY IEAO9901.
                 COPY IDSV0002.
      *----------------------------------------------------------------*
       01  AREA-IDSV0001.
           COPY IDSV0001.

           COPY IDSV0003.

           COPY IABV0003.

           COPY IABV0002.

       01  IABI0011.
           COPY IABI0011.

      *----------------------------------------------------------------*
      * GESTIONE TEMPORARY TABLE
      *----------------------------------------------------------------*
           COPY IDSV0301.

      *----------------------------------------------------------------*
      * GESTIONE REPORT BATCH
      *----------------------------------------------------------------*
           COPY IABV0010.

       01 FLAG-DESC-ERR-MORE-100              PIC X(01) VALUE 'N'.
          88 DESC-ERR-MORE-100-YES            VALUE 'Y'.
          88 DESC-ERR-MORE-100-NO             VALUE 'N'.

       01 FLAG-STAMPA-TESTATA-JCL             PIC X(01) VALUE 'N'.
          88 STAMPA-TESTATA-JCL-YES           VALUE 'Y'.
          88 STAMPA-TESTATA-JCL-NO            VALUE 'N'.

       01 FLAG-STAMPA-LOGO-ERRORE             PIC X(01) VALUE 'N'.
          88 STAMPA-LOGO-ERRORE-YES           VALUE 'Y'.
          88 STAMPA-LOGO-ERRORE-NO            VALUE 'N'.

       01 FLAG-WARNING-TROVATO                PIC X(01) VALUE 'N'.
          88 WARNING-TROVATO-YES              VALUE 'Y'.
          88 WARNING-TROVATO-NO               VALUE 'N'.


       01 CONT-TAB-GUIDE-LETTE                PIC 9(09).
       01 CONT-BUS-SERV-ESEG                  PIC 9(09).
       01 CONT-BUS-SERV-ESITO-OK              PIC 9(09).
       01 CONT-BUS-SERV-WARNING               PIC 9(09).
       01 CONT-BUS-SERV-ESITO-KO              PIC 9(09).


       01 GUIDE-TYPE-CONST.
          05 CONST-SEQUENTIAL-GUIDE PIC X(50) VALUE 'SEQUENTIAL GUIDE'.


       01 PGM-IJCS0060                       PIC X(08) VALUE 'IJCS0060'.

      ******************************************************************
      * CAMPI PER GESTIONE CACHE
      ******************************************************************
           COPY IABC0010.

       01 IX-ADDRESS                        PIC 9(2) COMP.
       01 IX-ADDRESS1                       PIC 9(2) COMP.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONTROLLO DATA FORMALE
      *----------------------------------------------------------------*
           COPY IDSV0016.

      *----------------------------------------------------------------*
      *    AREE UTILIZZATE X IL REPERIMENTO DELLE VARIABILI DI AMBIENTE
      *----------------------------------------------------------------*

           COPY IDBVD091.

           COPY IJCCMQ04.

      *----------------------------------------------------------------*
      *--- AREE PER CHIAMATE A WEBSPHERE MQ
      *----------------------------------------------------------------*
       01  W-EDIT-REASONS                  PIC ZZZZZZZZ9-.

           COPY IJCC0060.








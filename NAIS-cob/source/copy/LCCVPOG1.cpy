      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA PARAM_OGG
      *   ALIAS POG
      *   ULTIMO AGG. 02 SET 2008
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-PARAM-OGG PIC S9(9)     COMP-3.
             07 (SF)-ID-OGG PIC S9(9)     COMP-3.
             07 (SF)-TP-OGG PIC X(2).
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-PARAM PIC X(20).
             07 (SF)-COD-PARAM-NULL REDEFINES
                (SF)-COD-PARAM   PIC X(20).
             07 (SF)-TP-PARAM PIC X(1).
             07 (SF)-TP-PARAM-NULL REDEFINES
                (SF)-TP-PARAM   PIC X(1).
             07 (SF)-TP-D PIC X(2).
             07 (SF)-TP-D-NULL REDEFINES
                (SF)-TP-D   PIC X(2).
             07 (SF)-VAL-IMP PIC S9(12)V9(3) COMP-3.
             07 (SF)-VAL-IMP-NULL REDEFINES
                (SF)-VAL-IMP   PIC X(8).
             07 (SF)-VAL-DT   PIC S9(8) COMP-3.
             07 (SF)-VAL-DT-NULL REDEFINES
                (SF)-VAL-DT   PIC X(5).
             07 (SF)-VAL-TS PIC S9(5)V9(9) COMP-3.
             07 (SF)-VAL-TS-NULL REDEFINES
                (SF)-VAL-TS   PIC X(8).
             07 (SF)-VAL-TXT PIC X(100).
             07 (SF)-VAL-FL PIC X(1).
             07 (SF)-VAL-FL-NULL REDEFINES
                (SF)-VAL-FL   PIC X(1).
             07 (SF)-VAL-NUM PIC S9(14)     COMP-3.
             07 (SF)-VAL-NUM-NULL REDEFINES
                (SF)-VAL-NUM   PIC X(8).
             07 (SF)-VAL-PC PIC S9(5)V9(9) COMP-3.
             07 (SF)-VAL-PC-NULL REDEFINES
                (SF)-VAL-PC   PIC X(8).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).

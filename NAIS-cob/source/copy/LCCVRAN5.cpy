
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVRAN5
      *   ULTIMO AGG. 24 GEN 2014
      *------------------------------------------------------------

       VAL-DCLGEN-RAN.
           IF (SF)-ID-RAPP-ANA-COLLG-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-ID-RAPP-ANA-COLLG-NULL(IX-TAB-RAN)
              TO RAN-ID-RAPP-ANA-COLLG-NULL
           ELSE
              MOVE (SF)-ID-RAPP-ANA-COLLG(IX-TAB-RAN)
              TO RAN-ID-RAPP-ANA-COLLG
           END-IF
           MOVE (SF)-TP-OGG(IX-TAB-RAN)
              TO RAN-TP-OGG
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RAN)
              TO RAN-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-RAN)
              TO RAN-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-RAN) NOT NUMERIC
              MOVE 0 TO RAN-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-RAN)
              TO RAN-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-RAN) NOT NUMERIC
              MOVE 0 TO RAN-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-RAN)
              TO RAN-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-RAN)
              TO RAN-COD-COMP-ANIA
           IF (SF)-COD-SOGG-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-COD-SOGG-NULL(IX-TAB-RAN)
              TO RAN-COD-SOGG-NULL
           ELSE
              MOVE (SF)-COD-SOGG(IX-TAB-RAN)
              TO RAN-COD-SOGG
           END-IF
           MOVE (SF)-TP-RAPP-ANA(IX-TAB-RAN)
              TO RAN-TP-RAPP-ANA
           IF (SF)-TP-PERS-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-TP-PERS-NULL(IX-TAB-RAN)
              TO RAN-TP-PERS-NULL
           ELSE
              MOVE (SF)-TP-PERS(IX-TAB-RAN)
              TO RAN-TP-PERS
           END-IF
           IF (SF)-SEX-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-SEX-NULL(IX-TAB-RAN)
              TO RAN-SEX-NULL
           ELSE
              MOVE (SF)-SEX(IX-TAB-RAN)
              TO RAN-SEX
           END-IF
           IF (SF)-DT-NASC-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-DT-NASC-NULL(IX-TAB-RAN)
              TO RAN-DT-NASC-NULL
           ELSE
             IF (SF)-DT-NASC(IX-TAB-RAN) = ZERO
                MOVE HIGH-VALUES
                TO RAN-DT-NASC-NULL
             ELSE
              MOVE (SF)-DT-NASC(IX-TAB-RAN)
              TO RAN-DT-NASC
             END-IF
           END-IF
           IF (SF)-FL-ESTAS-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-FL-ESTAS-NULL(IX-TAB-RAN)
              TO RAN-FL-ESTAS-NULL
           ELSE
              MOVE (SF)-FL-ESTAS(IX-TAB-RAN)
              TO RAN-FL-ESTAS
           END-IF
           IF (SF)-INDIR-1-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-INDIR-1-NULL(IX-TAB-RAN)
              TO RAN-INDIR-1-NULL
           ELSE
              MOVE (SF)-INDIR-1(IX-TAB-RAN)
              TO RAN-INDIR-1
           END-IF
           IF (SF)-INDIR-2-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-INDIR-2-NULL(IX-TAB-RAN)
              TO RAN-INDIR-2-NULL
           ELSE
              MOVE (SF)-INDIR-2(IX-TAB-RAN)
              TO RAN-INDIR-2
           END-IF
           IF (SF)-INDIR-3-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-INDIR-3-NULL(IX-TAB-RAN)
              TO RAN-INDIR-3-NULL
           ELSE
              MOVE (SF)-INDIR-3(IX-TAB-RAN)
              TO RAN-INDIR-3
           END-IF
           IF (SF)-TP-UTLZ-INDIR-1-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-TP-UTLZ-INDIR-1-NULL(IX-TAB-RAN)
              TO RAN-TP-UTLZ-INDIR-1-NULL
           ELSE
              MOVE (SF)-TP-UTLZ-INDIR-1(IX-TAB-RAN)
              TO RAN-TP-UTLZ-INDIR-1
           END-IF
           IF (SF)-TP-UTLZ-INDIR-2-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-TP-UTLZ-INDIR-2-NULL(IX-TAB-RAN)
              TO RAN-TP-UTLZ-INDIR-2-NULL
           ELSE
              MOVE (SF)-TP-UTLZ-INDIR-2(IX-TAB-RAN)
              TO RAN-TP-UTLZ-INDIR-2
           END-IF
           IF (SF)-TP-UTLZ-INDIR-3-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-TP-UTLZ-INDIR-3-NULL(IX-TAB-RAN)
              TO RAN-TP-UTLZ-INDIR-3-NULL
           ELSE
              MOVE (SF)-TP-UTLZ-INDIR-3(IX-TAB-RAN)
              TO RAN-TP-UTLZ-INDIR-3
           END-IF
           IF (SF)-ESTR-CNT-CORR-ACCR-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-ESTR-CNT-CORR-ACCR-NULL(IX-TAB-RAN)
              TO RAN-ESTR-CNT-CORR-ACCR-NULL
           ELSE
              MOVE (SF)-ESTR-CNT-CORR-ACCR(IX-TAB-RAN)
              TO RAN-ESTR-CNT-CORR-ACCR
           END-IF
           IF (SF)-ESTR-CNT-CORR-ADD-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-ESTR-CNT-CORR-ADD-NULL(IX-TAB-RAN)
              TO RAN-ESTR-CNT-CORR-ADD-NULL
           ELSE
              MOVE (SF)-ESTR-CNT-CORR-ADD(IX-TAB-RAN)
              TO RAN-ESTR-CNT-CORR-ADD
           END-IF
           IF (SF)-ESTR-DOCTO-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-ESTR-DOCTO-NULL(IX-TAB-RAN)
              TO RAN-ESTR-DOCTO-NULL
           ELSE
              MOVE (SF)-ESTR-DOCTO(IX-TAB-RAN)
              TO RAN-ESTR-DOCTO
           END-IF
           IF (SF)-PC-NEL-RAPP-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-PC-NEL-RAPP-NULL(IX-TAB-RAN)
              TO RAN-PC-NEL-RAPP-NULL
           ELSE
              MOVE (SF)-PC-NEL-RAPP(IX-TAB-RAN)
              TO RAN-PC-NEL-RAPP
           END-IF
           IF (SF)-TP-MEZ-PAG-ADD-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-TP-MEZ-PAG-ADD-NULL(IX-TAB-RAN)
              TO RAN-TP-MEZ-PAG-ADD-NULL
           ELSE
              MOVE (SF)-TP-MEZ-PAG-ADD(IX-TAB-RAN)
              TO RAN-TP-MEZ-PAG-ADD
           END-IF
           IF (SF)-TP-MEZ-PAG-ACCR-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-TP-MEZ-PAG-ACCR-NULL(IX-TAB-RAN)
              TO RAN-TP-MEZ-PAG-ACCR-NULL
           ELSE
              MOVE (SF)-TP-MEZ-PAG-ACCR(IX-TAB-RAN)
              TO RAN-TP-MEZ-PAG-ACCR
           END-IF
           IF (SF)-COD-MATR-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-COD-MATR-NULL(IX-TAB-RAN)
              TO RAN-COD-MATR-NULL
           ELSE
              MOVE (SF)-COD-MATR(IX-TAB-RAN)
              TO RAN-COD-MATR
           END-IF
           IF (SF)-TP-ADEGZ-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-TP-ADEGZ-NULL(IX-TAB-RAN)
              TO RAN-TP-ADEGZ-NULL
           ELSE
              MOVE (SF)-TP-ADEGZ(IX-TAB-RAN)
              TO RAN-TP-ADEGZ
           END-IF
           IF (SF)-FL-TST-RSH-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-FL-TST-RSH-NULL(IX-TAB-RAN)
              TO RAN-FL-TST-RSH-NULL
           ELSE
              MOVE (SF)-FL-TST-RSH(IX-TAB-RAN)
              TO RAN-FL-TST-RSH
           END-IF
           IF (SF)-COD-AZ-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-COD-AZ-NULL(IX-TAB-RAN)
              TO RAN-COD-AZ-NULL
           ELSE
              MOVE (SF)-COD-AZ(IX-TAB-RAN)
              TO RAN-COD-AZ
           END-IF
           IF (SF)-IND-PRINC-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-IND-PRINC-NULL(IX-TAB-RAN)
              TO RAN-IND-PRINC-NULL
           ELSE
              MOVE (SF)-IND-PRINC(IX-TAB-RAN)
              TO RAN-IND-PRINC
           END-IF
           IF (SF)-DT-DELIBERA-CDA-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-DT-DELIBERA-CDA-NULL(IX-TAB-RAN)
              TO RAN-DT-DELIBERA-CDA-NULL
           ELSE
             IF (SF)-DT-DELIBERA-CDA(IX-TAB-RAN) = ZERO
                MOVE HIGH-VALUES
                TO RAN-DT-DELIBERA-CDA-NULL
             ELSE
              MOVE (SF)-DT-DELIBERA-CDA(IX-TAB-RAN)
              TO RAN-DT-DELIBERA-CDA
             END-IF
           END-IF
           IF (SF)-DLG-AL-RISC-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-DLG-AL-RISC-NULL(IX-TAB-RAN)
              TO RAN-DLG-AL-RISC-NULL
           ELSE
              MOVE (SF)-DLG-AL-RISC(IX-TAB-RAN)
              TO RAN-DLG-AL-RISC
           END-IF
           IF (SF)-LEGALE-RAPPR-PRINC-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-LEGALE-RAPPR-PRINC-NULL(IX-TAB-RAN)
              TO RAN-LEGALE-RAPPR-PRINC-NULL
           ELSE
              MOVE (SF)-LEGALE-RAPPR-PRINC(IX-TAB-RAN)
              TO RAN-LEGALE-RAPPR-PRINC
           END-IF
           IF (SF)-TP-LEGALE-RAPPR-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-TP-LEGALE-RAPPR-NULL(IX-TAB-RAN)
              TO RAN-TP-LEGALE-RAPPR-NULL
           ELSE
              MOVE (SF)-TP-LEGALE-RAPPR(IX-TAB-RAN)
              TO RAN-TP-LEGALE-RAPPR
           END-IF
           IF (SF)-TP-IND-PRINC-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-TP-IND-PRINC-NULL(IX-TAB-RAN)
              TO RAN-TP-IND-PRINC-NULL
           ELSE
              MOVE (SF)-TP-IND-PRINC(IX-TAB-RAN)
              TO RAN-TP-IND-PRINC
           END-IF
           IF (SF)-TP-STAT-RID-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-TP-STAT-RID-NULL(IX-TAB-RAN)
              TO RAN-TP-STAT-RID-NULL
           ELSE
              MOVE (SF)-TP-STAT-RID(IX-TAB-RAN)
              TO RAN-TP-STAT-RID
           END-IF
           MOVE (SF)-NOME-INT-RID(IX-TAB-RAN)
              TO RAN-NOME-INT-RID
           MOVE (SF)-COGN-INT-RID(IX-TAB-RAN)
              TO RAN-COGN-INT-RID
           MOVE (SF)-COGN-INT-TRATT(IX-TAB-RAN)
              TO RAN-COGN-INT-TRATT
           MOVE (SF)-NOME-INT-TRATT(IX-TAB-RAN)
              TO RAN-NOME-INT-TRATT
           IF (SF)-CF-INT-RID-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-CF-INT-RID-NULL(IX-TAB-RAN)
              TO RAN-CF-INT-RID-NULL
           ELSE
              MOVE (SF)-CF-INT-RID(IX-TAB-RAN)
              TO RAN-CF-INT-RID
           END-IF
           IF (SF)-FL-COINC-DIP-CNTR-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-FL-COINC-DIP-CNTR-NULL(IX-TAB-RAN)
              TO RAN-FL-COINC-DIP-CNTR-NULL
           ELSE
              MOVE (SF)-FL-COINC-DIP-CNTR(IX-TAB-RAN)
              TO RAN-FL-COINC-DIP-CNTR
           END-IF
           IF (SF)-FL-COINC-INT-CNTR-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-FL-COINC-INT-CNTR-NULL(IX-TAB-RAN)
              TO RAN-FL-COINC-INT-CNTR-NULL
           ELSE
              MOVE (SF)-FL-COINC-INT-CNTR(IX-TAB-RAN)
              TO RAN-FL-COINC-INT-CNTR
           END-IF
           IF (SF)-DT-DECES-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-DT-DECES-NULL(IX-TAB-RAN)
              TO RAN-DT-DECES-NULL
           ELSE
             IF (SF)-DT-DECES(IX-TAB-RAN) = ZERO
                MOVE HIGH-VALUES
                TO RAN-DT-DECES-NULL
             ELSE
              MOVE (SF)-DT-DECES(IX-TAB-RAN)
              TO RAN-DT-DECES
             END-IF
           END-IF
           IF (SF)-FL-FUMATORE-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-FL-FUMATORE-NULL(IX-TAB-RAN)
              TO RAN-FL-FUMATORE-NULL
           ELSE
              MOVE (SF)-FL-FUMATORE(IX-TAB-RAN)
              TO RAN-FL-FUMATORE
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-RAN) NOT NUMERIC
              MOVE 0 TO RAN-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-RAN)
              TO RAN-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-RAN)
              TO RAN-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-RAN) NOT NUMERIC
              MOVE 0 TO RAN-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-RAN)
              TO RAN-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-RAN) NOT NUMERIC
              MOVE 0 TO RAN-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-RAN)
              TO RAN-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-RAN) NOT NUMERIC
              MOVE 0 TO RAN-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-RAN)
              TO RAN-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-RAN)
              TO RAN-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-RAN)
              TO RAN-DS-STATO-ELAB
           IF (SF)-FL-LAV-DIP-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-FL-LAV-DIP-NULL(IX-TAB-RAN)
              TO RAN-FL-LAV-DIP-NULL
           ELSE
              MOVE (SF)-FL-LAV-DIP(IX-TAB-RAN)
              TO RAN-FL-LAV-DIP
           END-IF
           IF (SF)-TP-VARZ-PAGAT-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-TP-VARZ-PAGAT-NULL(IX-TAB-RAN)
              TO RAN-TP-VARZ-PAGAT-NULL
           ELSE
              MOVE (SF)-TP-VARZ-PAGAT(IX-TAB-RAN)
              TO RAN-TP-VARZ-PAGAT
           END-IF
           IF (SF)-COD-RID-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-COD-RID-NULL(IX-TAB-RAN)
              TO RAN-COD-RID-NULL
           ELSE
              MOVE (SF)-COD-RID(IX-TAB-RAN)
              TO RAN-COD-RID
           END-IF
           IF (SF)-TP-CAUS-RID-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-TP-CAUS-RID-NULL(IX-TAB-RAN)
              TO RAN-TP-CAUS-RID-NULL
           ELSE
              MOVE (SF)-TP-CAUS-RID(IX-TAB-RAN)
              TO RAN-TP-CAUS-RID
           END-IF
           IF (SF)-IND-MASSA-CORP-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-IND-MASSA-CORP-NULL(IX-TAB-RAN)
              TO RAN-IND-MASSA-CORP-NULL
           ELSE
              MOVE (SF)-IND-MASSA-CORP(IX-TAB-RAN)
              TO RAN-IND-MASSA-CORP
           END-IF
           IF (SF)-CAT-RSH-PROF-NULL(IX-TAB-RAN) = HIGH-VALUES
              MOVE (SF)-CAT-RSH-PROF-NULL(IX-TAB-RAN)
              TO RAN-CAT-RSH-PROF-NULL
           ELSE
              MOVE (SF)-CAT-RSH-PROF(IX-TAB-RAN)
              TO RAN-CAT-RSH-PROF
           END-IF.
       VAL-DCLGEN-RAN-EX.
           EXIT.

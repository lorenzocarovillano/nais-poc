      *----------------------------------------------------------------*
      * DIMENSIONAMENTO OCCURS PER LA TAB. DETT_TIT_DI_RAT             *
      * COPY RIPORTANTE IL NUMERO DI OCCURS DA UTILIZZARE              *
      * PER INIZIALIZZA TOT DELLA TABELLA                              *
      *----------------------------------------------------------------*
       01 WK-DTR-MAX.
          03 WK-DTR-MAX-A                 PIC 9(04) VALUE 60.

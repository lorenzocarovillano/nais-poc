
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVRIF3
      *   ULTIMO AGG. 25 NOV 2019
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-RIF.
           MOVE RIF-ID-RICH-INVST-FND
             TO (SF)-ID-PTF(IX-TAB-RIF)
           MOVE RIF-ID-RICH-INVST-FND
             TO (SF)-ID-RICH-INVST-FND(IX-TAB-RIF)
           MOVE RIF-ID-MOVI-FINRIO
             TO (SF)-ID-MOVI-FINRIO(IX-TAB-RIF)
           MOVE RIF-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-RIF)
           IF RIF-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE RIF-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RIF)
           ELSE
              MOVE RIF-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-RIF)
           END-IF
           MOVE RIF-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-RIF)
           MOVE RIF-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-RIF)
           MOVE RIF-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-RIF)
           IF RIF-COD-FND-NULL = HIGH-VALUES
              MOVE RIF-COD-FND-NULL
                TO (SF)-COD-FND-NULL(IX-TAB-RIF)
           ELSE
              MOVE RIF-COD-FND
                TO (SF)-COD-FND(IX-TAB-RIF)
           END-IF
           IF RIF-NUM-QUO-NULL = HIGH-VALUES
              MOVE RIF-NUM-QUO-NULL
                TO (SF)-NUM-QUO-NULL(IX-TAB-RIF)
           ELSE
              MOVE RIF-NUM-QUO
                TO (SF)-NUM-QUO(IX-TAB-RIF)
           END-IF
           IF RIF-PC-NULL = HIGH-VALUES
              MOVE RIF-PC-NULL
                TO (SF)-PC-NULL(IX-TAB-RIF)
           ELSE
              MOVE RIF-PC
                TO (SF)-PC(IX-TAB-RIF)
           END-IF
           IF RIF-IMP-MOVTO-NULL = HIGH-VALUES
              MOVE RIF-IMP-MOVTO-NULL
                TO (SF)-IMP-MOVTO-NULL(IX-TAB-RIF)
           ELSE
              MOVE RIF-IMP-MOVTO
                TO (SF)-IMP-MOVTO(IX-TAB-RIF)
           END-IF
           IF RIF-DT-INVST-NULL = HIGH-VALUES
              MOVE RIF-DT-INVST-NULL
                TO (SF)-DT-INVST-NULL(IX-TAB-RIF)
           ELSE
              MOVE RIF-DT-INVST
                TO (SF)-DT-INVST(IX-TAB-RIF)
           END-IF
           IF RIF-COD-TARI-NULL = HIGH-VALUES
              MOVE RIF-COD-TARI-NULL
                TO (SF)-COD-TARI-NULL(IX-TAB-RIF)
           ELSE
              MOVE RIF-COD-TARI
                TO (SF)-COD-TARI(IX-TAB-RIF)
           END-IF
           IF RIF-TP-STAT-NULL = HIGH-VALUES
              MOVE RIF-TP-STAT-NULL
                TO (SF)-TP-STAT-NULL(IX-TAB-RIF)
           ELSE
              MOVE RIF-TP-STAT
                TO (SF)-TP-STAT(IX-TAB-RIF)
           END-IF
           IF RIF-TP-MOD-INVST-NULL = HIGH-VALUES
              MOVE RIF-TP-MOD-INVST-NULL
                TO (SF)-TP-MOD-INVST-NULL(IX-TAB-RIF)
           ELSE
              MOVE RIF-TP-MOD-INVST
                TO (SF)-TP-MOD-INVST(IX-TAB-RIF)
           END-IF
           MOVE RIF-COD-DIV
             TO (SF)-COD-DIV(IX-TAB-RIF)
           IF RIF-DT-CAMBIO-VLT-NULL = HIGH-VALUES
              MOVE RIF-DT-CAMBIO-VLT-NULL
                TO (SF)-DT-CAMBIO-VLT-NULL(IX-TAB-RIF)
           ELSE
              MOVE RIF-DT-CAMBIO-VLT
                TO (SF)-DT-CAMBIO-VLT(IX-TAB-RIF)
           END-IF
           MOVE RIF-TP-FND
             TO (SF)-TP-FND(IX-TAB-RIF)
           MOVE RIF-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-RIF)
           MOVE RIF-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-RIF)
           MOVE RIF-DS-VER
             TO (SF)-DS-VER(IX-TAB-RIF)
           MOVE RIF-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-RIF)
           MOVE RIF-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-RIF)
           MOVE RIF-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-RIF)
           MOVE RIF-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-RIF)
           IF RIF-DT-INVST-CALC-NULL = HIGH-VALUES
              MOVE RIF-DT-INVST-CALC-NULL
                TO (SF)-DT-INVST-CALC-NULL(IX-TAB-RIF)
           ELSE
              MOVE RIF-DT-INVST-CALC
                TO (SF)-DT-INVST-CALC(IX-TAB-RIF)
           END-IF
           IF RIF-FL-CALC-INVTO-NULL = HIGH-VALUES
              MOVE RIF-FL-CALC-INVTO-NULL
                TO (SF)-FL-CALC-INVTO-NULL(IX-TAB-RIF)
           ELSE
              MOVE RIF-FL-CALC-INVTO
                TO (SF)-FL-CALC-INVTO(IX-TAB-RIF)
           END-IF
           IF RIF-IMP-GAP-EVENT-NULL = HIGH-VALUES
              MOVE RIF-IMP-GAP-EVENT-NULL
                TO (SF)-IMP-GAP-EVENT-NULL(IX-TAB-RIF)
           ELSE
              MOVE RIF-IMP-GAP-EVENT
                TO (SF)-IMP-GAP-EVENT(IX-TAB-RIF)
           END-IF
           IF RIF-FL-SWM-BP2S-NULL = HIGH-VALUES
              MOVE RIF-FL-SWM-BP2S-NULL
                TO (SF)-FL-SWM-BP2S-NULL(IX-TAB-RIF)
           ELSE
              MOVE RIF-FL-SWM-BP2S
                TO (SF)-FL-SWM-BP2S(IX-TAB-RIF)
           END-IF.
       VALORIZZA-OUTPUT-RIF-EX.
           EXIT.

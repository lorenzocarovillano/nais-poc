
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVDEQ5
      *   ULTIMO AGG. 03 GIU 2019
      *------------------------------------------------------------

       VAL-DCLGEN-DEQ.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-DEQ) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-DEQ)
              TO DEQ-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-DEQ)
              TO DEQ-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-DEQ) NOT NUMERIC
              MOVE 0 TO DEQ-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-DEQ)
              TO DEQ-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-DEQ) NOT NUMERIC
              MOVE 0 TO DEQ-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-DEQ)
              TO DEQ-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-DEQ)
              TO DEQ-COD-COMP-ANIA
           IF (SF)-COD-QUEST-NULL(IX-TAB-DEQ) = HIGH-VALUES
              MOVE (SF)-COD-QUEST-NULL(IX-TAB-DEQ)
              TO DEQ-COD-QUEST-NULL
           ELSE
              MOVE (SF)-COD-QUEST(IX-TAB-DEQ)
              TO DEQ-COD-QUEST
           END-IF
           IF (SF)-COD-DOM-NULL(IX-TAB-DEQ) = HIGH-VALUES
              MOVE (SF)-COD-DOM-NULL(IX-TAB-DEQ)
              TO DEQ-COD-DOM-NULL
           ELSE
              MOVE (SF)-COD-DOM(IX-TAB-DEQ)
              TO DEQ-COD-DOM
           END-IF
           IF (SF)-COD-DOM-COLLG-NULL(IX-TAB-DEQ) = HIGH-VALUES
              MOVE (SF)-COD-DOM-COLLG-NULL(IX-TAB-DEQ)
              TO DEQ-COD-DOM-COLLG-NULL
           ELSE
              MOVE (SF)-COD-DOM-COLLG(IX-TAB-DEQ)
              TO DEQ-COD-DOM-COLLG
           END-IF
           IF (SF)-RISP-NUM-NULL(IX-TAB-DEQ) = HIGH-VALUES
              MOVE (SF)-RISP-NUM-NULL(IX-TAB-DEQ)
              TO DEQ-RISP-NUM-NULL
           ELSE
              MOVE (SF)-RISP-NUM(IX-TAB-DEQ)
              TO DEQ-RISP-NUM
           END-IF
           IF (SF)-RISP-FL-NULL(IX-TAB-DEQ) = HIGH-VALUES
              MOVE (SF)-RISP-FL-NULL(IX-TAB-DEQ)
              TO DEQ-RISP-FL-NULL
           ELSE
              MOVE (SF)-RISP-FL(IX-TAB-DEQ)
              TO DEQ-RISP-FL
           END-IF
           MOVE (SF)-RISP-TXT(IX-TAB-DEQ)
              TO DEQ-RISP-TXT
           IF (SF)-RISP-TS-NULL(IX-TAB-DEQ) = HIGH-VALUES
              MOVE (SF)-RISP-TS-NULL(IX-TAB-DEQ)
              TO DEQ-RISP-TS-NULL
           ELSE
              MOVE (SF)-RISP-TS(IX-TAB-DEQ)
              TO DEQ-RISP-TS
           END-IF
           IF (SF)-RISP-IMP-NULL(IX-TAB-DEQ) = HIGH-VALUES
              MOVE (SF)-RISP-IMP-NULL(IX-TAB-DEQ)
              TO DEQ-RISP-IMP-NULL
           ELSE
              MOVE (SF)-RISP-IMP(IX-TAB-DEQ)
              TO DEQ-RISP-IMP
           END-IF
           IF (SF)-RISP-PC-NULL(IX-TAB-DEQ) = HIGH-VALUES
              MOVE (SF)-RISP-PC-NULL(IX-TAB-DEQ)
              TO DEQ-RISP-PC-NULL
           ELSE
              MOVE (SF)-RISP-PC(IX-TAB-DEQ)
              TO DEQ-RISP-PC
           END-IF
           IF (SF)-RISP-DT-NULL(IX-TAB-DEQ) = HIGH-VALUES
              MOVE (SF)-RISP-DT-NULL(IX-TAB-DEQ)
              TO DEQ-RISP-DT-NULL
           ELSE
             IF (SF)-RISP-DT(IX-TAB-DEQ) = ZERO
                MOVE HIGH-VALUES
                TO DEQ-RISP-DT-NULL
             ELSE
              MOVE (SF)-RISP-DT(IX-TAB-DEQ)
              TO DEQ-RISP-DT
             END-IF
           END-IF
           IF (SF)-RISP-KEY-NULL(IX-TAB-DEQ) = HIGH-VALUES
              MOVE (SF)-RISP-KEY-NULL(IX-TAB-DEQ)
              TO DEQ-RISP-KEY-NULL
           ELSE
              MOVE (SF)-RISP-KEY(IX-TAB-DEQ)
              TO DEQ-RISP-KEY
           END-IF
           IF (SF)-TP-RISP-NULL(IX-TAB-DEQ) = HIGH-VALUES
              MOVE (SF)-TP-RISP-NULL(IX-TAB-DEQ)
              TO DEQ-TP-RISP-NULL
           ELSE
              MOVE (SF)-TP-RISP(IX-TAB-DEQ)
              TO DEQ-TP-RISP
           END-IF
           IF (SF)-ID-COMP-QUEST-NULL(IX-TAB-DEQ) = HIGH-VALUES
              MOVE (SF)-ID-COMP-QUEST-NULL(IX-TAB-DEQ)
              TO DEQ-ID-COMP-QUEST-NULL
           ELSE
              MOVE (SF)-ID-COMP-QUEST(IX-TAB-DEQ)
              TO DEQ-ID-COMP-QUEST
           END-IF
           MOVE (SF)-VAL-RISP(IX-TAB-DEQ)
              TO DEQ-VAL-RISP
           IF (SF)-DS-RIGA(IX-TAB-DEQ) NOT NUMERIC
              MOVE 0 TO DEQ-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-DEQ)
              TO DEQ-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-DEQ)
              TO DEQ-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-DEQ) NOT NUMERIC
              MOVE 0 TO DEQ-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-DEQ)
              TO DEQ-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-DEQ) NOT NUMERIC
              MOVE 0 TO DEQ-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-DEQ)
              TO DEQ-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-DEQ) NOT NUMERIC
              MOVE 0 TO DEQ-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-DEQ)
              TO DEQ-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-DEQ)
              TO DEQ-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-DEQ)
              TO DEQ-DS-STATO-ELAB.
       VAL-DCLGEN-DEQ-EX.
           EXIT.

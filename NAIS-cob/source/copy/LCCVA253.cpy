
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVA253
      *   ULTIMO AGG. 26 GIU 2019
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-A25.
           MOVE A25-ID-PERS
             TO (SF)-ID-PTF(IX-TAB-A25)
           MOVE A25-ID-PERS
             TO (SF)-ID-PERS(IX-TAB-A25)
           MOVE A25-TSTAM-INI-VLDT
             TO (SF)-TSTAM-INI-VLDT(IX-TAB-A25)
           IF A25-TSTAM-END-VLDT-NULL = HIGH-VALUES
              MOVE A25-TSTAM-END-VLDT-NULL
                TO (SF)-TSTAM-END-VLDT-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-TSTAM-END-VLDT
                TO (SF)-TSTAM-END-VLDT(IX-TAB-A25)
           END-IF
           MOVE A25-COD-PERS
             TO (SF)-COD-PERS(IX-TAB-A25)
           IF A25-RIFTO-RETE-NULL = HIGH-VALUES
              MOVE A25-RIFTO-RETE-NULL
                TO (SF)-RIFTO-RETE-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-RIFTO-RETE
                TO (SF)-RIFTO-RETE(IX-TAB-A25)
           END-IF
           IF A25-COD-PRT-IVA-NULL = HIGH-VALUES
              MOVE A25-COD-PRT-IVA-NULL
                TO (SF)-COD-PRT-IVA-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-COD-PRT-IVA
                TO (SF)-COD-PRT-IVA(IX-TAB-A25)
           END-IF
           IF A25-IND-PVCY-PRSNL-NULL = HIGH-VALUES
              MOVE A25-IND-PVCY-PRSNL-NULL
                TO (SF)-IND-PVCY-PRSNL-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-IND-PVCY-PRSNL
                TO (SF)-IND-PVCY-PRSNL(IX-TAB-A25)
           END-IF
           IF A25-IND-PVCY-CMMRC-NULL = HIGH-VALUES
              MOVE A25-IND-PVCY-CMMRC-NULL
                TO (SF)-IND-PVCY-CMMRC-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-IND-PVCY-CMMRC
                TO (SF)-IND-PVCY-CMMRC(IX-TAB-A25)
           END-IF
           IF A25-IND-PVCY-INDST-NULL = HIGH-VALUES
              MOVE A25-IND-PVCY-INDST-NULL
                TO (SF)-IND-PVCY-INDST-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-IND-PVCY-INDST
                TO (SF)-IND-PVCY-INDST(IX-TAB-A25)
           END-IF
           IF A25-DT-NASC-CLI-NULL = HIGH-VALUES
              MOVE A25-DT-NASC-CLI-NULL
                TO (SF)-DT-NASC-CLI-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-DT-NASC-CLI
                TO (SF)-DT-NASC-CLI(IX-TAB-A25)
           END-IF
           IF A25-DT-ACQS-PERS-NULL = HIGH-VALUES
              MOVE A25-DT-ACQS-PERS-NULL
                TO (SF)-DT-ACQS-PERS-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-DT-ACQS-PERS
                TO (SF)-DT-ACQS-PERS(IX-TAB-A25)
           END-IF
           IF A25-IND-CLI-NULL = HIGH-VALUES
              MOVE A25-IND-CLI-NULL
                TO (SF)-IND-CLI-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-IND-CLI
                TO (SF)-IND-CLI(IX-TAB-A25)
           END-IF
           IF A25-COD-CMN-NULL = HIGH-VALUES
              MOVE A25-COD-CMN-NULL
                TO (SF)-COD-CMN-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-COD-CMN
                TO (SF)-COD-CMN(IX-TAB-A25)
           END-IF
           IF A25-COD-FRM-GIURD-NULL = HIGH-VALUES
              MOVE A25-COD-FRM-GIURD-NULL
                TO (SF)-COD-FRM-GIURD-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-COD-FRM-GIURD
                TO (SF)-COD-FRM-GIURD(IX-TAB-A25)
           END-IF
           IF A25-COD-ENTE-PUBB-NULL = HIGH-VALUES
              MOVE A25-COD-ENTE-PUBB-NULL
                TO (SF)-COD-ENTE-PUBB-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-COD-ENTE-PUBB
                TO (SF)-COD-ENTE-PUBB(IX-TAB-A25)
           END-IF
           MOVE A25-DEN-RGN-SOC
             TO (SF)-DEN-RGN-SOC(IX-TAB-A25)
           MOVE A25-DEN-SIG-RGN-SOC
             TO (SF)-DEN-SIG-RGN-SOC(IX-TAB-A25)
           IF A25-IND-ESE-FISC-NULL = HIGH-VALUES
              MOVE A25-IND-ESE-FISC-NULL
                TO (SF)-IND-ESE-FISC-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-IND-ESE-FISC
                TO (SF)-IND-ESE-FISC(IX-TAB-A25)
           END-IF
           IF A25-COD-STAT-CVL-NULL = HIGH-VALUES
              MOVE A25-COD-STAT-CVL-NULL
                TO (SF)-COD-STAT-CVL-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-COD-STAT-CVL
                TO (SF)-COD-STAT-CVL(IX-TAB-A25)
           END-IF
           MOVE A25-DEN-NOME
             TO (SF)-DEN-NOME(IX-TAB-A25)
           MOVE A25-DEN-COGN
             TO (SF)-DEN-COGN(IX-TAB-A25)
           IF A25-COD-FISC-NULL = HIGH-VALUES
              MOVE A25-COD-FISC-NULL
                TO (SF)-COD-FISC-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-COD-FISC
                TO (SF)-COD-FISC(IX-TAB-A25)
           END-IF
           IF A25-IND-SEX-NULL = HIGH-VALUES
              MOVE A25-IND-SEX-NULL
                TO (SF)-IND-SEX-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-IND-SEX
                TO (SF)-IND-SEX(IX-TAB-A25)
           END-IF
           IF A25-IND-CPCT-GIURD-NULL = HIGH-VALUES
              MOVE A25-IND-CPCT-GIURD-NULL
                TO (SF)-IND-CPCT-GIURD-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-IND-CPCT-GIURD
                TO (SF)-IND-CPCT-GIURD(IX-TAB-A25)
           END-IF
           IF A25-IND-PORT-HDCP-NULL = HIGH-VALUES
              MOVE A25-IND-PORT-HDCP-NULL
                TO (SF)-IND-PORT-HDCP-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-IND-PORT-HDCP
                TO (SF)-IND-PORT-HDCP(IX-TAB-A25)
           END-IF
           MOVE A25-COD-USER-INS
             TO (SF)-COD-USER-INS(IX-TAB-A25)
           MOVE A25-TSTAM-INS-RIGA
             TO (SF)-TSTAM-INS-RIGA(IX-TAB-A25)
           IF A25-COD-USER-AGGM-NULL = HIGH-VALUES
              MOVE A25-COD-USER-AGGM-NULL
                TO (SF)-COD-USER-AGGM-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-COD-USER-AGGM
                TO (SF)-COD-USER-AGGM(IX-TAB-A25)
           END-IF
           IF A25-TSTAM-AGGM-RIGA-NULL = HIGH-VALUES
              MOVE A25-TSTAM-AGGM-RIGA-NULL
                TO (SF)-TSTAM-AGGM-RIGA-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-TSTAM-AGGM-RIGA
                TO (SF)-TSTAM-AGGM-RIGA(IX-TAB-A25)
           END-IF
           MOVE A25-DEN-CMN-NASC-STRN
             TO (SF)-DEN-CMN-NASC-STRN(IX-TAB-A25)
           IF A25-COD-RAMO-STGR-NULL = HIGH-VALUES
              MOVE A25-COD-RAMO-STGR-NULL
                TO (SF)-COD-RAMO-STGR-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-COD-RAMO-STGR
                TO (SF)-COD-RAMO-STGR(IX-TAB-A25)
           END-IF
           IF A25-COD-STGR-ATVT-UIC-NULL = HIGH-VALUES
              MOVE A25-COD-STGR-ATVT-UIC-NULL
                TO (SF)-COD-STGR-ATVT-UIC-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-COD-STGR-ATVT-UIC
                TO (SF)-COD-STGR-ATVT-UIC(IX-TAB-A25)
           END-IF
           IF A25-COD-RAMO-ATVT-UIC-NULL = HIGH-VALUES
              MOVE A25-COD-RAMO-ATVT-UIC-NULL
                TO (SF)-COD-RAMO-ATVT-UIC-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-COD-RAMO-ATVT-UIC
                TO (SF)-COD-RAMO-ATVT-UIC(IX-TAB-A25)
           END-IF
           IF A25-DT-END-VLDT-PERS-NULL = HIGH-VALUES
              MOVE A25-DT-END-VLDT-PERS-NULL
                TO (SF)-DT-END-VLDT-PERS-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-DT-END-VLDT-PERS
                TO (SF)-DT-END-VLDT-PERS(IX-TAB-A25)
           END-IF
           IF A25-DT-DEAD-PERS-NULL = HIGH-VALUES
              MOVE A25-DT-DEAD-PERS-NULL
                TO (SF)-DT-DEAD-PERS-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-DT-DEAD-PERS
                TO (SF)-DT-DEAD-PERS(IX-TAB-A25)
           END-IF
           IF A25-TP-STAT-CLI-NULL = HIGH-VALUES
              MOVE A25-TP-STAT-CLI-NULL
                TO (SF)-TP-STAT-CLI-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-TP-STAT-CLI
                TO (SF)-TP-STAT-CLI(IX-TAB-A25)
           END-IF
           IF A25-DT-BLOC-CLI-NULL = HIGH-VALUES
              MOVE A25-DT-BLOC-CLI-NULL
                TO (SF)-DT-BLOC-CLI-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-DT-BLOC-CLI
                TO (SF)-DT-BLOC-CLI(IX-TAB-A25)
           END-IF
           IF A25-COD-PERS-SECOND-NULL = HIGH-VALUES
              MOVE A25-COD-PERS-SECOND-NULL
                TO (SF)-COD-PERS-SECOND-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-COD-PERS-SECOND
                TO (SF)-COD-PERS-SECOND(IX-TAB-A25)
           END-IF
           IF A25-ID-SEGMENTAZ-CLI-NULL = HIGH-VALUES
              MOVE A25-ID-SEGMENTAZ-CLI-NULL
                TO (SF)-ID-SEGMENTAZ-CLI-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-ID-SEGMENTAZ-CLI
                TO (SF)-ID-SEGMENTAZ-CLI(IX-TAB-A25)
           END-IF
           IF A25-DT-1A-ATVT-NULL = HIGH-VALUES
              MOVE A25-DT-1A-ATVT-NULL
                TO (SF)-DT-1A-ATVT-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-DT-1A-ATVT
                TO (SF)-DT-1A-ATVT(IX-TAB-A25)
           END-IF
           IF A25-DT-SEGNAL-PARTNER-NULL = HIGH-VALUES
              MOVE A25-DT-SEGNAL-PARTNER-NULL
                TO (SF)-DT-SEGNAL-PARTNER-NULL(IX-TAB-A25)
           ELSE
              MOVE A25-DT-SEGNAL-PARTNER
                TO (SF)-DT-SEGNAL-PARTNER(IX-TAB-A25)
           END-IF.
       VALORIZZA-OUTPUT-A25-EX.
           EXIT.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA RAPP_RETE
      *   ALIAS RRE
      *   ULTIMO AGG. 01 OTT 2014
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-RAPP-RETE PIC S9(9)     COMP-3.
             07 (SF)-ID-OGG PIC S9(9)     COMP-3.
             07 (SF)-ID-OGG-NULL REDEFINES
                (SF)-ID-OGG   PIC X(5).
             07 (SF)-TP-OGG PIC X(2).
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-TP-RETE PIC X(2).
             07 (SF)-TP-ACQS-CNTRT PIC S9(5)     COMP-3.
             07 (SF)-TP-ACQS-CNTRT-NULL REDEFINES
                (SF)-TP-ACQS-CNTRT   PIC X(3).
             07 (SF)-COD-ACQS-CNTRT PIC S9(5)     COMP-3.
             07 (SF)-COD-ACQS-CNTRT-NULL REDEFINES
                (SF)-COD-ACQS-CNTRT   PIC X(3).
             07 (SF)-COD-PNT-RETE-INI PIC S9(10)     COMP-3.
             07 (SF)-COD-PNT-RETE-INI-NULL REDEFINES
                (SF)-COD-PNT-RETE-INI   PIC X(6).
             07 (SF)-COD-PNT-RETE-END PIC S9(10)     COMP-3.
             07 (SF)-COD-PNT-RETE-END-NULL REDEFINES
                (SF)-COD-PNT-RETE-END   PIC X(6).
             07 (SF)-FL-PNT-RETE-1RIO PIC X(1).
             07 (SF)-FL-PNT-RETE-1RIO-NULL REDEFINES
                (SF)-FL-PNT-RETE-1RIO   PIC X(1).
             07 (SF)-COD-CAN PIC S9(5)     COMP-3.
             07 (SF)-COD-CAN-NULL REDEFINES
                (SF)-COD-CAN   PIC X(3).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-COD-PNT-RETE-INI-C PIC S9(10)     COMP-3.
             07 (SF)-COD-PNT-RETE-INI-C-NULL REDEFINES
                (SF)-COD-PNT-RETE-INI-C   PIC X(6).
             07 (SF)-MATR-OPRT PIC X(20).
             07 (SF)-MATR-OPRT-NULL REDEFINES
                (SF)-MATR-OPRT   PIC X(20).

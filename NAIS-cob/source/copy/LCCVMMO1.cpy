      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA MATR_MOVIMENTO
      *   ALIAS MMO
      *   ULTIMO AGG. 28 FEB 2008
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                    PIC S9(9) COMP-5.
           05 (SF)-DATI.
             07 (SF)-ID-MATR-MOVIMENTO PIC S9(9) COMP-5.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-TP-MOVI-PTF PIC S9(5)     COMP-3.
             07 (SF)-TP-OGG PIC X(2).
             07 (SF)-TP-FRM-ASSVA PIC X(2).
             07 (SF)-TP-FRM-ASSVA-NULL REDEFINES
                (SF)-TP-FRM-ASSVA   PIC X(2).
             07 (SF)-TP-MOVI-ACT PIC X(5).
             07 (SF)-AMMISSIBILITA-MOVI PIC X(1).
             07 (SF)-AMMISSIBILITA-MOVI-NULL REDEFINES
                (SF)-AMMISSIBILITA-MOVI   PIC X(1).
             07 (SF)-SERVIZIO-CONTROLLO PIC X(8).
             07 (SF)-SERVIZIO-CONTROLLO-NULL REDEFINES
                (SF)-SERVIZIO-CONTROLLO   PIC X(8).
             07 (SF)-COD-PROCESSO-WF PIC X(3).
             07 (SF)-COD-PROCESSO-WF-NULL REDEFINES
                (SF)-COD-PROCESSO-WF   PIC X(3).


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVL113
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-L11.
           MOVE L11-ID-OGG-BLOCCO
             TO (SF)-ID-PTF
           MOVE L11-ID-OGG-BLOCCO
             TO (SF)-ID-OGG-BLOCCO
           IF L11-ID-OGG-1RIO-NULL = HIGH-VALUES
              MOVE L11-ID-OGG-1RIO-NULL
                TO (SF)-ID-OGG-1RIO-NULL
           ELSE
              MOVE L11-ID-OGG-1RIO
                TO (SF)-ID-OGG-1RIO
           END-IF
           IF L11-TP-OGG-1RIO-NULL = HIGH-VALUES
              MOVE L11-TP-OGG-1RIO-NULL
                TO (SF)-TP-OGG-1RIO-NULL
           ELSE
              MOVE L11-TP-OGG-1RIO
                TO (SF)-TP-OGG-1RIO
           END-IF
           MOVE L11-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE L11-TP-MOVI
             TO (SF)-TP-MOVI
           MOVE L11-TP-OGG
             TO (SF)-TP-OGG
           MOVE L11-ID-OGG
             TO (SF)-ID-OGG
           MOVE L11-DT-EFF
             TO (SF)-DT-EFF
           MOVE L11-TP-STAT-BLOCCO
             TO (SF)-TP-STAT-BLOCCO
           MOVE L11-COD-BLOCCO
             TO (SF)-COD-BLOCCO
           IF L11-ID-RICH-NULL = HIGH-VALUES
              MOVE L11-ID-RICH-NULL
                TO (SF)-ID-RICH-NULL
           ELSE
              MOVE L11-ID-RICH
                TO (SF)-ID-RICH
           END-IF
           MOVE L11-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE L11-DS-VER
             TO (SF)-DS-VER
           MOVE L11-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE L11-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE L11-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB.
       VALORIZZA-OUTPUT-L11-EX.
           EXIT.

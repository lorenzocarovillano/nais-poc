      *----------------------------------------------------------------*
      *    ESTRATTO CONTO - FASE DIAGNOSTICO
      *       - CONTROLLI COMUNI A TUTTI GLI E/C
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *   CONTROLLO 1                                                  *
      *----------------------------------------------------------------*
       C0001-CNTRL-1.
      *
           MOVE 'C0001-CNTRL-1'          TO WK-LABEL.
      *
           IF ELE-MAX-CACHE-3 > 0
              CONTINUE
           ELSE
              MOVE 1          TO WK-CONTROLLO
              SET CNTRL-1     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.
      *
       EX-C0001.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 2                                                  *
      *----------------------------------------------------------------*
       C0002-CNTRL-2.

           MOVE 'C0002-CNTRL-2'          TO WK-LABEL.

      *--> LA DT_RIF_A DEVE ESSERE COMPRESA NEL
      *--> PERIODO DI PRENOTAZIONE
           IF  WRIN-DT-RIF-A      >= WK-DT-RICOR-DA
           AND WRIN-DT-RIF-A      <= WK-DT-RICOR-A
      *--> LA DT_RIF_DA DEVE ESSERE LA MAX TRA
      *--> (DT_DECOR E DT_RIF_A - 1 ANNO)
               MOVE WRIN-DT-RIF-A TO WK-DATA-IN
      *
               PERFORM A700-CALCOLO-DATA
                  THRU A700-EX
      *
               IF  WRIN-DT-RIF-DA = WK-DATA-OUT
               OR  WRIN-DT-RIF-DA = WRIN-DT-DECOR-POLI
                   CONTINUE
               ELSE
CASH               IF WRIN-DT-RIF-A(1:4) =  WRIN-DT-RIF-DA(1:4)
CASH               AND WRIN-COD-PROD-ACT = 'PTCASH'
                       CONTINUE
                   ELSE
                       MOVE 2                  TO WK-CONTROLLO
                       MOVE WRIN-DT-RIF-DA     TO WS-DATA-1
                       MOVE WRIN-DT-RIF-A      TO WS-DATA-2
                       MOVE WRIN-DT-DECOR-POLI TO WS-DATA-3
                       SET CNTRL-2         TO TRUE
                       MOVE MSG-ERRORE     TO WS-ERRORE
                       PERFORM E001-SCARTO THRU EX-E001
                   END-IF
               END-IF
           ELSE
              MOVE 2                  TO WK-CONTROLLO
              MOVE WRIN-DT-RIF-DA     TO WS-DATA-1
              MOVE WRIN-DT-RIF-A      TO WS-DATA-2
              MOVE WRIN-DT-DECOR-POLI TO WS-DATA-3
              MOVE 2          TO WK-CONTROLLO
              SET CNTRL-2         TO TRUE
              MOVE MSG-ERRORE     TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.
      *
       EX-C0002.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 6 7 14                                             *
      *----------------------------------------------------------------*
       C0003-CNTRL-6-7-14.

           MOVE 'C0003-CNTRL-6-7-14'      TO WK-LABEL.

           INITIALIZE WORK-CALCOLI

           COMPUTE WS-DIFF = WRIN-TOT-CUM-PREMI-VERS
                           - WRIN-TOT-CUM-PRE-INVST-AA-RIF

           IF WRIN-TOT-CUM-PREMI-VERS > 0
              COMPUTE WS-DIV = WS-DIFF / WRIN-TOT-CUM-PREMI-VERS
              COMPUTE WS-PERC = WS-DIV * 100
           END-IF

           IF TP-ESTR-MU
           OR TP-ESTR-UL
              IF WS-PERC > 6
                 MOVE 6          TO WK-CONTROLLO
                 MOVE WRIN-TOT-CUM-PREMI-VERS       TO WS-CAMPO-1
                 MOVE WRIN-TOT-CUM-PRE-INVST-AA-RIF TO WS-CAMPO-2
                 MOVE WS-PERC                       TO WS-CAMPO-3
                 SET CNTRL-6     TO TRUE
                 MOVE MSG-ERRORE TO WS-ERRORE
                 PERFORM E001-SCARTO THRU EX-E001
              END-IF
           ELSE
              IF TP-ESTR-RV
                 MOVE WK-DT-DECOR(1:4) TO WK-DT-DECOR-AA
                 IF WK-DT-DECOR-AA = WK-ANNO-RIF
                 AND ((WK-FRAZ = 12) OR (WK-FRAZ = 4))
                    IF WS-PERC > 50
                       MOVE 6          TO WK-CONTROLLO
                       MOVE WRIN-TOT-CUM-PREMI-VERS       TO WS-CAMPO-1
                       MOVE WRIN-TOT-CUM-PRE-INVST-AA-RIF TO WS-CAMPO-2
                       MOVE WS-PERC                       TO WS-CAMPO-3
                       MOVE WK-DT-DECOR                   TO WS-DATA-1
                       SET CNTRL-6     TO TRUE
                       MOVE MSG-ERRORE TO WS-ERRORE
                       PERFORM E001-SCARTO THRU EX-E001
                    END-IF
                 ELSE
                    IF WS-PERC > 25
                       MOVE 6          TO WK-CONTROLLO
                       MOVE WRIN-TOT-CUM-PREMI-VERS       TO WS-CAMPO-1
                       MOVE WRIN-TOT-CUM-PRE-INVST-AA-RIF TO WS-CAMPO-2
                       MOVE WS-PERC                       TO WS-CAMPO-3
                       MOVE WK-DT-DECOR                   TO WS-DATA-1
                       SET CNTRL-6     TO TRUE
                       MOVE MSG-ERRORE TO WS-ERRORE
                       PERFORM E001-SCARTO THRU EX-E001
                    END-IF
                 END-IF
              END-IF
           END-IF

           IF WRIN-TOT-CUM-PREMI-VERS = 0
              IF WRIN-TOT-CUM-PRE-INVST-AA-RIF = 0
                 CONTINUE
              ELSE
                 MOVE 7          TO WK-CONTROLLO
                 MOVE WRIN-TOT-CUM-PREMI-VERS       TO WS-CAMPO-1
                 MOVE WRIN-TOT-CUM-PRE-INVST-AA-RIF TO WS-CAMPO-2
                 SET CNTRL-7     TO TRUE
                 MOVE MSG-ERRORE TO WS-ERRORE
                 PERFORM E001-SCARTO THRU EX-E001
              END-IF
           END-IF

           MOVE 0 TO WS-SOMMA

           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
               COMPUTE WS-SOMMA = WS-SOMMA
                   + REC3-CUM-PREM-VERS-AA-RIF(IX-REC-3)
           END-PERFORM.

           IF  WRIN-TOT-CUM-PREMI-VERS
               = WS-SOMMA
              CONTINUE
           ELSE
              MOVE 14                      TO WK-CONTROLLO
              MOVE WRIN-TOT-CUM-PREMI-VERS TO WS-CAMPO-1
              MOVE WS-SOMMA                TO WS-CAMPO-2
              SET CNTRL-14     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0003.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 8 9 18                                             *
      *----------------------------------------------------------------*
       C0004-CNTRL-8-9-18.

           MOVE 'C0004-CNTRL-8-9-18'      TO WK-LABEL.

           IF WRIN-DT-DECOR-POLI < WRIN-DT-RIF-DA
              IF WRIN-TOT-CUM-PREMI-VERS-AP > 0
                 CONTINUE
              ELSE
                 MOVE 8          TO WK-CONTROLLO
                 MOVE WRIN-TOT-CUM-PREMI-VERS-AP
                   TO WS-CAMPO-1
                 MOVE WRIN-DT-DECOR-POLI TO WS-DATA-1
                 MOVE WRIN-DT-RIF-DA     TO WS-DATA-2
                 SET CNTRL-8     TO TRUE
                 MOVE MSG-ERRORE TO WS-ERRORE
                 PERFORM E001-SCARTO THRU EX-E001
              END-IF
           END-IF.

           IF WRIN-DT-RIF-DA = WRIN-DT-RIF-A
               IF  WRIN-TOT-CUM-PREMI-VERS-AP IS NUMERIC
                   CONTINUE
               ELSE
                   MOVE 0 TO WRIN-TOT-CUM-PREMI-VERS-AP
               END-IF
              IF  WRIN-TOT-CUM-PREMI-VERS-AP = 0
              AND WRIN-PRESTAZIONE-AP-POL = 0
              AND WRIN-IMP-LRD-RISC-PARZ-AP-POL = 0
              AND WRIN-IMP-LRD-RISC-PARZ-AC-POL = 0
                  CONTINUE
              ELSE
                  MOVE 9          TO WK-CONTROLLO
                  MOVE WRIN-TOT-CUM-PREMI-VERS-AP TO WS-CAMPO-1
                  MOVE WRIN-PRESTAZIONE-AP-POL     TO WS-CAMPO-2
                  SET CNTRL-9     TO TRUE
                  MOVE MSG-ERRORE TO WS-ERRORE
                  PERFORM E001-SCARTO THRU EX-E001
             END-IF
           END-IF.

           MOVE 0 TO WS-SOMMA

           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
               COMPUTE WS-SOMMA = WS-SOMMA
                   + REC3-CUM-PREM-VERS-EC-PREC(IX-REC-3)
           END-PERFORM.

           IF  WRIN-TOT-CUM-PREMI-VERS-AP
               = WS-SOMMA
              CONTINUE
           ELSE
              MOVE 18         TO WK-CONTROLLO
              MOVE WRIN-TOT-CUM-PREMI-VERS-AP TO WS-CAMPO-1
              MOVE WS-SOMMA                   TO WS-CAMPO-2
              SET CNTRL-18     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0004.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 10 19                                              *
      *----------------------------------------------------------------*
       C0005-CNTRL-10-19.

           MOVE 'C0005-CNTRL-10-19'      TO WK-LABEL.

           IF  WRIN-DT-DECOR-POLI < WRIN-DT-RIF-DA
               IF WRIN-PRESTAZIONE-AP-POL > 0
                  CONTINUE
               ELSE
                  MOVE WRIN-DT-DECOR-POLI TO WS-DATA-1
                  MOVE WRIN-DT-RIF-DA     TO WS-DATA-2
                  MOVE WRIN-PRESTAZIONE-AP-POL TO WS-CAMPO-1
                  MOVE 10         TO WK-CONTROLLO
                  SET CNTRL-10     TO TRUE
                  MOVE MSG-ERRORE TO WS-ERRORE
                  PERFORM E001-SCARTO THRU EX-E001
               END-IF
           END-IF.

           MOVE 0 TO WS-SOMMA

           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
ALFR           IF REC3-TP-GAR(IX-REC-3) = 1 OR 5
ALFR  *        IF REC3-TP-GAR(IX-REC-3) = 1
                  COMPUTE WS-SOMMA = WS-SOMMA
                         + REC3-PREST-MATUR-EC-PREC(IX-REC-3)
               END-IF
           END-PERFORM.

           IF  WRIN-PRESTAZIONE-AP-POL
               = WS-SOMMA
              CONTINUE
           ELSE
              MOVE 19         TO WK-CONTROLLO
              MOVE WRIN-PRESTAZIONE-AP-POL    TO WS-CAMPO-1
              MOVE WS-SOMMA                   TO WS-CAMPO-2
              SET CNTRL-19     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0005.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 11 15                                              *
      *----------------------------------------------------------------*
       C0006-CNTRL-11-15.

           MOVE 'C0006-CNTRL-11-15'      TO WK-LABEL.

           IF  WRIN-PRESTAZIONE-AC-POL > 0
               CONTINUE
           ELSE
              MOVE 11         TO WK-CONTROLLO
              MOVE WRIN-PRESTAZIONE-AC-POL    TO WS-CAMPO-1
              MOVE ZERO                       TO WS-CAMPO-2
              SET CNTRL-11     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

           MOVE 0 TO WS-SOMMA

           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
ALFR           IF REC3-TP-GAR(IX-REC-3) = 1 OR 5
ALFR  *        IF REC3-TP-GAR(IX-REC-3) = 1
                  COMPUTE WS-SOMMA = WS-SOMMA
                      + REC3-PREST-MATUR-AA-RIF(IX-REC-3)
               END-IF
           END-PERFORM.

           IF  WRIN-PRESTAZIONE-AC-POL
               = WS-SOMMA
              CONTINUE
           ELSE
              MOVE 15         TO WK-CONTROLLO
              MOVE WRIN-PRESTAZIONE-AC-POL    TO WS-CAMPO-1
              MOVE WS-SOMMA                   TO WS-CAMPO-2
              SET CNTRL-15     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0006.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 13 42                                              *
      *----------------------------------------------------------------*
       C0007-CNTRL-13-42.


      * --> CONTROLLO 13
ALFR  * --> FILTRARE LE POLIZZE TCM DAI CONTROLLI 13 E 42
ALFR       IF TP-ESTR-TCM
ALFR          CONTINUE
ALFR       ELSE
              MOVE 'C0007-CNTRL-13-42'      TO WK-LABEL

              MOVE 0 TO WS-SOMMA

              PERFORM VARYING IX-REC-3 FROM 1 BY 1
                UNTIL IX-REC-3 > ELE-MAX-CACHE-3
ALFR  *           IF REC3-TP-GAR(IX-REC-3) = 1
ALFR              IF REC3-TP-GAR(IX-REC-3) = 1 OR 5
                     COMPUTE WS-SOMMA = WS-SOMMA
                         + REC3-CUM-PREM-INVST-AA-RIF(IX-REC-3)
                  END-IF
              END-PERFORM

              IF  WRIN-TOT-CUM-PRE-INVST-AA-RIF
                  = WS-SOMMA
                 CONTINUE
              ELSE
                 MOVE 13         TO WK-CONTROLLO
                 MOVE WRIN-TOT-CUM-PRE-INVST-AA-RIF TO WS-CAMPO-1
                 MOVE WS-SOMMA                      TO WS-CAMPO-2
                 SET CNTRL-13     TO TRUE
                 MOVE MSG-ERRORE TO WS-ERRORE
                 PERFORM E001-SCARTO THRU EX-E001
              END-IF
ALFR       END-IF.

      * --> CONTROLLO 42
           MOVE 0 TO WS-SOMMA-123
                     WS-DIFF-123

           PERFORM VARYING IX-REC-10 FROM 1 BY 1
             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
               IF REC10-DESC-TP-MOVI(IX-REC-10) =
                  TP-VERS-PREMI
                   COMPUTE WS-SOMMA-123 = WS-SOMMA-123
                       + REC10-IMP-OPER(IX-REC-10)
               END-IF
           END-PERFORM.

           COMPUTE WS-DIFF-123 = WRIN-TOT-CUM-PRE-INVST-AA-RIF -
                             WS-SOMMA-123

           IF  WS-DIFF-123 < 0,05 AND WS-DIFF-123 > -0,05
              CONTINUE
           ELSE
              MOVE 42         TO WK-CONTROLLO
              MOVE WS-SOMMA-123               TO WS-CAMPO-1
              MOVE WRIN-TOT-CUM-PRE-INVST-AA-RIF TO WS-CAMPO-2
              SET CNTRL-42     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0007.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 16                                                 *
      *----------------------------------------------------------------*
       C0008-CNTRL-16.

           MOVE 'C0008-CNTRL-16'      TO WK-LABEL.

           MOVE 0 TO WS-SOMMA

           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
ALFR  *        IF REC3-TP-GAR(IX-REC-3) = 1
ALFR           IF REC3-TP-GAR(IX-REC-3) = 1 OR 5
                  COMPUTE WS-SOMMA = WS-SOMMA
                        + REC3-CAP-LIQ-GA(IX-REC-3)
               END-IF
           END-PERFORM.

           IF  WRIN-CAP-LIQ-DT-RIF-EST-CC
               = WS-SOMMA
              CONTINUE
           ELSE
              MOVE 16         TO WK-CONTROLLO
              MOVE WRIN-CAP-LIQ-DT-RIF-EST-CC TO WS-CAMPO-1
              MOVE WS-SOMMA                   TO WS-CAMPO-2
              SET CNTRL-16     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0008.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 17                                                 *
      *----------------------------------------------------------------*
       C0009-CNTRL-17.

           MOVE 'C0009-CNTRL-17'      TO WK-LABEL.

           MOVE 0 TO WS-SOMMA

           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
               COMPUTE WS-SOMMA = WS-SOMMA
                   + REC3-RISC-TOT-GA(IX-REC-3)
           END-PERFORM.

           IF  WRIN-RISC-TOT-DT-RIF-EST-CC
               = WS-SOMMA
              CONTINUE
           ELSE
              MOVE 17         TO WK-CONTROLLO
              MOVE WRIN-RISC-TOT-DT-RIF-EST-CC TO WS-CAMPO-1
              MOVE WS-SOMMA                   TO WS-CAMPO-2
              SET CNTRL-17     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0009.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 20                                                 *
      *----------------------------------------------------------------*
       C0010-CNTRL-20.

           MOVE 'C0010-CNTRL-20'      TO WK-LABEL.

           MOVE 0 TO WS-SOMMA

           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
               COMPUTE WS-SOMMA = WS-SOMMA
                   + REC3-IMP-LRD-RISC-PARZ-AP-GAR(IX-REC-3)
           END-PERFORM.

           IF  WRIN-IMP-LRD-RISC-PARZ-AP-POL
               = WS-SOMMA
              CONTINUE
           ELSE
              MOVE 20         TO WK-CONTROLLO
              MOVE WRIN-IMP-LRD-RISC-PARZ-AP-POL TO WS-CAMPO-1
              MOVE WS-SOMMA                      TO WS-CAMPO-2
              SET CNTRL-20     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0010.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 21                                                 *
      *----------------------------------------------------------------*
       C0011-CNTRL-21.

           MOVE 'C0011-CNTRL-21'      TO WK-LABEL.

           MOVE 0 TO WS-SOMMA

           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
               COMPUTE WS-SOMMA = WS-SOMMA
                   + REC3-IMP-LRD-RISC-PARZ-AC-GAR(IX-REC-3)
           END-PERFORM.

           IF  WRIN-IMP-LRD-RISC-PARZ-AC-POL
               = WS-SOMMA
              CONTINUE
           ELSE
              MOVE 21         TO WK-CONTROLLO
              MOVE WRIN-IMP-LRD-RISC-PARZ-AC-POL TO WS-CAMPO-1
              MOVE WS-SOMMA                      TO WS-CAMPO-2
              SET CNTRL-21     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0011.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 23                                                 *
      *----------------------------------------------------------------*
       C0012-CNTRL-23.

           MOVE 'C0012-CNTRL-23'      TO WK-LABEL.

           IF REC2-COGNOME-ASS EQUAL SPACES
              OR HIGH-VALUES OR LOW-VALUES
              SET CNTRL-23     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           ELSE
              IF REC2-NOME-ASS EQUAL SPACES
              OR HIGH-VALUES OR LOW-VALUES
                 SET CNTRL-23     TO TRUE
                 MOVE MSG-ERRORE TO WS-ERRORE
                 PERFORM E001-SCARTO THRU EX-E001
              END-IF
           END-IF.

       EX-C0012.
           EXIT.
ALFR  *-  DA RIPRISTINARE DOPO AVER FATTO I TEST           ------------*
      *----------------------------------------------------------------*
      *   CONTROLLO 33                                                 *
      *----------------------------------------------------------------*
      *
       C0013-CNTRL-33.

           MOVE 'C0013-CNTRL-33'      TO WK-LABEL.

           INITIALIZE WORK-CALCOLI.

           SET ABBINATA-NO TO TRUE

           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
               IF TP-ESTR-RV
ALFR              IF REC3-TP-GAR(IX-REC-3) = ( 1 OR 5 )
                  AND REC3-TP-INVST(IX-REC-3) = 4
                  COMPUTE WS-APPO-1 = REC3-PREST-MATUR-AA-RIF(IX-REC-3)
                                    + WS-APPO-1
                  COMPUTE WS-APPO-2 = REC3-CAP-LIQ-GA(IX-REC-3)
                                    + WS-APPO-2
                  END-IF
               ELSE
                  IF REC3-TP-GAR(IX-REC-3) = 1
                  AND REC3-TP-INVST(IX-REC-3) = 7
                  COMPUTE WS-APPO-1 = REC3-PREST-MATUR-AA-RIF(IX-REC-3)
                                    + WS-APPO-1
                  COMPUTE WS-APPO-2 = REC3-CAP-LIQ-GA(IX-REC-3)
                                    + WS-APPO-2
                  END-IF

               END-IF
           END-PERFORM

           MOVE WS-APPO-1 TO WS-DIFF


ALFR       SET GAR-RAMO3-NO            TO TRUE
ALFR       SET GAR-RAMO3-STOR-NO       TO TRUE
           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3

ALFR           IF  REC3-TP-GAR(IX-REC-3) = 1
 "             AND REC3-TP-INVST(IX-REC-3) = 7 OR 8
 "                 SET GAR-RAMO3-SI       TO TRUE
 "    *    CONTROLLIAMO SE LA GARANZIA DI RAMO 3 SIA STORNATA
 "                 IF REC3-FL-STORNATE-ANN(IX-REC-3) = '1'
 "                    SET GAR-RAMO3-STOR-SI  TO TRUE
 "                 END-IF
ALFR           END-IF


               IF REC3-TP-GAR(IX-REC-3) = 4
                  SET ABBINATA-SI TO TRUE
                  COMPUTE WS-APPO-3 = WS-APPO-3     +
                                    REC3-CAP-LIQ-GA(IX-REC-3)


               END-IF
           END-PERFORM

ALFR       IF (TP-ESTR-MU AND GAR-RAMO3-NO)
 "                        OR
 "            (TP-ESTR-MU AND (GAR-RAMO3-SI AND GAR-RAMO3-STOR-SI))
 "            SET ABBINATA-NO TO TRUE
 "         ELSE
 "            COMPUTE WS-DIFF = (WS-APPO-1 + WS-APPO-3) - WS-APPO-2
ALFR       END-IF

           IF ABBINATA-SI
              IF WS-DIFF < -1 OR
                 WS-DIFF > 1
                 MOVE 33         TO WK-CONTROLLO
                 MOVE WS-APPO-1  TO WS-CAMPO-1
                 MOVE WS-APPO-2  TO WS-CAMPO-2
                 MOVE WS-APPO-3  TO WS-CAMPO-3
                 SET CNTRL-33        TO TRUE
                 MOVE MSG-ERRORE     TO WS-ERRORE
                 PERFORM E001-SCARTO THRU EX-E001
              END-IF
           END-IF.

      *
       EX-C0013.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 38                                                 *
      *----------------------------------------------------------------*
       C0014-CNTRL-38.

           MOVE 'C0014-CNTRL-38'      TO WK-LABEL.

           MOVE 0 TO WS-SOMMA

           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
               COMPUTE WS-SOMMA = WS-SOMMA
                   + REC3-IMP-LRD-RISC-PARZ-AC-GAR(IX-REC-3)
           END-PERFORM.
           IF  REC9-IMP-LORDO-RISC-PARZ =
               WS-SOMMA
               IF REC9-IMP-LORDO-RISC-PARZ =
                  WRIN-IMP-LRD-RISC-PARZ-AC-POL
                  CONTINUE
               ELSE
                  MOVE REC9-IMP-LORDO-RISC-PARZ TO WS-CAMPO-1
                  MOVE WRIN-IMP-LRD-RISC-PARZ-AC-POL
                    TO WS-CAMPO-2
                  MOVE WS-SOMMA
                    TO WS-CAMPO-3
                  MOVE 38         TO WK-CONTROLLO
                  SET CNTRL-38     TO TRUE
                  MOVE MSG-ERRORE TO WS-ERRORE
                  PERFORM E001-SCARTO THRU EX-E001
               END-IF
           ELSE
               MOVE REC9-IMP-LORDO-RISC-PARZ TO WS-CAMPO-1
               MOVE WRIN-IMP-LRD-RISC-PARZ-AC-POL
                 TO WS-CAMPO-2
               MOVE WS-SOMMA
                 TO WS-CAMPO-3
               MOVE 38         TO WK-CONTROLLO
               SET CNTRL-38     TO TRUE
               MOVE MSG-ERRORE TO WS-ERRORE
               PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0014.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 40                                                 *
      *----------------------------------------------------------------*
       C0015-CNTRL-40.

           MOVE 'C0015-CNTRL-40'      TO WK-LABEL.

           MOVE 0 TO WS-SOMMA
                     WS-DIFF
12193      SET REC-9-TP-MOVI-TROVATO-NO TO TRUE
12193      SET ERRORE-NO TO TRUE
12193      PERFORM VARYING IX-REC-9 FROM 1 BY 1
12193        UNTIL IX-REC-9 > ELE-MAX-CACHE-9
12193           OR ERRORE-SI
12193           OR REC-9-TP-MOVI-TROVATO-SI
12193           SET REC-9-TP-MOVI-TROVATO-NO TO TRUE
                MOVE 0 TO WS-SOMMA
                          WS-DIFF
12193         PERFORM VARYING IX-REC-10 FROM 1 BY 1
12193           UNTIL IX-REC-10 > ELE-MAX-CACHE-10
12193                OR ERRORE-SI
12193           IF REC10-TP-MOVI(IX-REC-10) =
12193              REC9-RP-TP-MOVI(IX-REC-9)

12193                 COMPUTE WS-SOMMA =
12193                     WS-SOMMA +
12193                     REC10-IMP-OPER(IX-REC-10)

12193                  MOVE IX-REC-9 TO IX-REC9-TP-MOVI
12193                  SET REC-9-TP-MOVI-TROVATO-SI TO TRUE
12193              END-IF
12193         END-PERFORM
14235      END-PERFORM.
12193      IF REC-9-TP-MOVI-TROVATO-SI
12193         COMPUTE WS-DIFF = REC9-IMP-LRD-RISC-PARZ(IX-REC9-TP-MOVI)--
12193                           WS-SOMMA
12193         IF WS-DIFF < 0,05 AND WS-DIFF > -0,05
12193             CONTINUE
12193         ELSE
12193             MOVE 40      TO WK-CONTROLLO
12193             MOVE REC9-IMP-LRD-RISC-PARZ(IX-REC9-TP-MOVI)
12193                                             TO WS-CAMPO-1
12193             MOVE WS-SOMMA                TO WS-CAMPO-2
12193             SET CNTRL-40  TO TRUE
12193             MOVE MSG-ERRORE TO WS-ERRORE
12193             PERFORM E001-SCARTO THRU EX-E001
12193              SET ERRORE-SI TO TRUE
12193           END-IF
12193      END-IF.
14235 *

       EX-C0015.
           EXIT.
      *----------------------------------------------------------------*
      * CALCOLO LA DATA + DELTA GIORNI
      *----------------------------------------------------------------*
       A700-CALCOLO-DATA.

           MOVE 'A700-CALCOLO-DATA'          TO WK-LABEL.

      * SOTTRAE UN ANNO ALLA DATA RIF A
           INITIALIZE IO-A2K-LCCC0003.
           MOVE '03'                           TO A2K-FUNZ.
           MOVE '03'                           TO A2K-INFO.
           MOVE 1                              TO A2K-DELTA.
           MOVE 'A'                            TO A2K-TDELTA.
           MOVE WK-DATA-IN                     TO A2K-INDATA.
           MOVE '0'                            TO A2K-FISLAV
                                                  A2K-INICON.

           PERFORM A710-CALL-LCCS0003
              THRU A710-EX.

           IF IDSV0001-ESITO-OK
              MOVE A2K-OUAMG                   TO WK-DATA-OUT
           END-IF.

       A700-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINE PER IL CALCOLO SULLE DATE
      *----------------------------------------------------------------*
       A710-CALL-LCCS0003.

           MOVE 'A710-CALL-LCCS0003'     TO WK-LABEL.

           CALL PGM-LCCS0003 USING IO-A2K-LCCC0003 WK-RCODE
           ON EXCEPTION
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SERVIZIO OPERAZIONI SULLE DATE'
                                          TO CALL-DESC
              MOVE WK-LABEL               TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

           IF WK-RCODE NOT = '00'
              MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL               TO IEAI9901-LABEL-ERR
              MOVE '005016'               TO IEAI9901-COD-ERRORE
              MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       A710-EX.
           EXIT.






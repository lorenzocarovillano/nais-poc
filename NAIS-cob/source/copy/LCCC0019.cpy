      *----------------------------------------------------------------*
      *    PORTAFOGLIO VITA ITALIA                                     *
      *    CREATA : 05/02/2007                                         *
      *----------------------------------------------------------------*
      *    GESTIONE INPUT/OUTPUT VERIFICA PLATFOND
      *    VERSIONE AL 24/03/2009
      *----------------------------------------------------------------*
      *
          05 (SF)-TP-OPERAZ                PIC X(002).
             88 (SF)-LETTURA-VPL           VALUE '00'.
             88 (SF)-AGGIORNA-VPL          VALUE '01'.
      *
          05 (SF)-TARIFFA                  PIC X(002).
             88 (SF)-TARIFFA-OK            VALUE 'OK'.
             88 (SF)-TARIFFA-KO            VALUE 'KO'.
      *
          05 (SF)-CONTROLLO               PIC X(002).
             88 (SF)-CONTROLLO-SI          VALUE 'SI'.
             88 (SF)-CONTROLLO-NO          VALUE 'NO'.
      *
          05 (SF)-IMPORTO-PLAT             PIC X(002).
             88 (SF)-PATTUITO-SI           VALUE 'SI'.
             88 (SF)-PATTUITO-NO           VALUE 'NO'.
      *
          05 (SF)-TIPO-AGG                 PIC X(002).
             88 (SF)-DEROGA-EV             VALUE 'EV'.
             88 (SF)-DEROGA-SI             VALUE 'SI'.
             88 (SF)-DEROGA-NO             VALUE 'NO'.
             88 (SF)-DEROGA-ST             VALUE 'SD'.
             88 (SF)-STORNO-SI             VALUE 'ST'.
      *
      *-- CAMPI PER LA GESTIONE DELL'IMPORTO DI PLAFOND PRENOTATO
      *
FO           88 (SF)-PRENOT-SI             VALUE 'PR'.
FO           88 (SF)-STOPRE-SI             VALUE 'SP'.
      *
          05 (SF)-AREA-FONDO.
             10 (SF)-ELE-MAX-FONDI         PIC S9(04) COMP.
             10 (SF)-TAB-FONDI             OCCURS 3.
                15 (SF)-IMP-PLATFOND       PIC S9(15)V9(003) COMP-3.
                15 (SF)-COD-FONDO          PIC X(012).
                15 (SF)-PERC-FONDO         PIC S9(3)V9(3) COMP-3.
                15 (SF)-PERC-FONDO-OLD     PIC S9(3)V9(3) COMP-3.

          05 (SF)-IMP-DA-SCALARE           PIC S9(12)V9(3) COMP-3.
          05 (SF)-IMP-DA-SCALARE-OLD       PIC S9(12)V9(3) COMP-3.
      *
      *-- IMPORTO DISPONIBILE DI PLAFOND
      *
FO        05 (SF)-IMP-DISP-PLAFOND         PIC S9(12)V9(3) COMP-3.
      *
      *-- return code

          05 (SF)-RETURN-CODE             PIC  X(002).
             88 (SF)-SUCCESSFUL-RC        VALUE '00' 'IN'.
             88 (SF)-INFORMATION          VALUE 'IN'.
             88 (SF)-SQL-ERROR            VALUE 'D3'.
             88 (SF)-GENERIC-ERROR        VALUE 'KO'.


          05 (SF)-SQLCODE-SIGNED          PIC S9(009).
             88 (SF)-SUCCESSFUL-SQL       VALUE ZERO.
             88 (SF)-NOT-FOUND            VALUE +100.
             88 (SF)-DUPLICATE-KEY        VALUE -803.
             88 (SF)-MORE-THAN-ONE-ROW    VALUE -811.
             88 (SF)-DEADLOCK-TIMEOUT     VALUE -911
                                                -913.
             88 (SF)-CONNECTION-ERROR     VALUE -924.

          05 (SF)-SQLCODE                 PIC --------9.

      *-- campi esito

          05 (SF)-CAMPI-ESITO.
             10 (SF)-DESCRIZ-ERR-DB2      PIC  X(300).
             10 (SF)-COD-SERVIZIO-BE      PIC  X(008).
             10 (SF)-LABEL-ERR            PIC  X(030).
             10 (SF)-COD-ERRORE           PIC  9(006).
             10 (SF)-PARAMETRI-ERR        PIC  X(100).
             10 (SF)-NOME-TABELLA         PIC  X(018).
             10 (SF)-KEY-TABELLA          PIC  X(020).



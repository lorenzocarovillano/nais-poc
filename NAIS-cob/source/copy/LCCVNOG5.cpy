
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVNOG5
      *   ULTIMO AGG. 02 FEB 2009
      *------------------------------------------------------------

       VAL-DCLGEN-NOG.
           MOVE (SF)-COD-COMPAGNIA-ANIA
              TO NOG-COD-COMPAGNIA-ANIA
           MOVE (SF)-FORMA-ASSICURATIVA
              TO NOG-FORMA-ASSICURATIVA
           MOVE (SF)-COD-OGGETTO
              TO NOG-COD-OGGETTO
           MOVE (SF)-TIPO-OGGETTO
              TO NOG-TIPO-OGGETTO
           IF (SF)-ULT-PROGR-NULL = HIGH-VALUES
              MOVE (SF)-ULT-PROGR-NULL
              TO NOG-ULT-PROGR-NULL
           ELSE
              MOVE (SF)-ULT-PROGR
              TO NOG-ULT-PROGR
           END-IF
           MOVE (SF)-DESC-OGGETTO
              TO NOG-DESC-OGGETTO.
       VAL-DCLGEN-NOG-EX.
           EXIT.

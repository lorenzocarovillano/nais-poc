
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVXAB3
      *   ULTIMO AGG. 09 LUG 2009
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-XAB.
           MOVE XAB-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE XAB-COD-BLOCCO
             TO (SF)-COD-BLOCCO
           MOVE XAB-DESC
             TO (SF)-DESC
           MOVE XAB-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE XAB-DS-VER
             TO (SF)-DS-VER
           MOVE XAB-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE XAB-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE XAB-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           IF XAB-FL-REC-AUT-NULL = HIGH-VALUES
              MOVE XAB-FL-REC-AUT-NULL
                TO (SF)-FL-REC-AUT-NULL
           ELSE
              MOVE XAB-FL-REC-AUT
                TO (SF)-FL-REC-AUT
           END-IF.
       VALORIZZA-OUTPUT-XAB-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVSTB3
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-STB.
           MOVE STB-ID-STAT-OGG-BUS
             TO (SF)-ID-PTF(IX-TAB-STB)
           MOVE STB-ID-STAT-OGG-BUS
             TO (SF)-ID-STAT-OGG-BUS(IX-TAB-STB)
           MOVE STB-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-STB)
           MOVE STB-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-STB)
           MOVE STB-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-STB)
           IF STB-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE STB-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-STB)
           ELSE
              MOVE STB-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-STB)
           END-IF
           MOVE STB-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-STB)
           MOVE STB-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-STB)
           MOVE STB-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-STB)
           MOVE STB-TP-STAT-BUS
             TO (SF)-TP-STAT-BUS(IX-TAB-STB)
           MOVE STB-TP-CAUS
             TO (SF)-TP-CAUS(IX-TAB-STB)
           MOVE STB-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-STB)
           MOVE STB-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-STB)
           MOVE STB-DS-VER
             TO (SF)-DS-VER(IX-TAB-STB)
           MOVE STB-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-STB)
           MOVE STB-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-STB)
           MOVE STB-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-STB)
           MOVE STB-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-STB).
       VALORIZZA-OUTPUT-STB-EX.
           EXIT.

      ******************************************************************
      *    TP_FRM_ASSVA (Forma Assicurativa)
      ******************************************************************
       01  WS-TP-FRM-ASSVA                  PIC X(002) VALUE SPACES.
           88 INDIVIDUALE                              VALUE 'IN'.
           88 COLLETTIVA                               VALUE 'CO'.
           88 ENTRAMBI                                 VALUE 'EN'.

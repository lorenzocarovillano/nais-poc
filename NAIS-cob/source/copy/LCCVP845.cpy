
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVP845
      *   ULTIMO AGG. 26 SET 2014
      *------------------------------------------------------------

       VAL-DCLGEN-P84.
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-P84)
              TO P84-COD-COMP-ANIA
           MOVE (SF)-ID-POLI(IX-TAB-P84)
              TO P84-ID-POLI
           MOVE (SF)-DT-LIQ-CED(IX-TAB-P84)
              TO P84-DT-LIQ-CED
           MOVE (SF)-COD-TARI(IX-TAB-P84)
              TO P84-COD-TARI
           IF (SF)-IMP-LRD-CEDOLE-LIQ-NULL(IX-TAB-P84) = HIGH-VALUES
              MOVE (SF)-IMP-LRD-CEDOLE-LIQ-NULL(IX-TAB-P84)
              TO P84-IMP-LRD-CEDOLE-LIQ-NULL
           ELSE
              MOVE (SF)-IMP-LRD-CEDOLE-LIQ(IX-TAB-P84)
              TO P84-IMP-LRD-CEDOLE-LIQ
           END-IF
           MOVE (SF)-IB-POLI(IX-TAB-P84)
              TO P84-IB-POLI
           MOVE (SF)-DS-OPER-SQL(IX-TAB-P84)
              TO P84-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-P84) NOT NUMERIC
              MOVE 0 TO P84-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-P84)
              TO P84-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ(IX-TAB-P84) NOT NUMERIC
              MOVE 0 TO P84-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ(IX-TAB-P84)
              TO P84-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-P84)
              TO P84-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-P84)
              TO P84-DS-STATO-ELAB.
       VAL-DCLGEN-P84-EX.
           EXIT.

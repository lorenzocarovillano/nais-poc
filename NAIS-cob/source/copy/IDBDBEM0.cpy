           EXEC SQL DECLARE BTC_EXE_MESSAGE TABLE
           (
             ID_BATCH            INTEGER NOT NULL,
             ID_JOB              INTEGER NOT NULL,
             ID_EXECUTION        INTEGER NOT NULL,
             ID_MESSAGE          INTEGER NOT NULL,
             COD_MESSAGE         INTEGER NOT NULL,
             MESSAGE             VARCHAR(1024) NOT NULL
          ) END-EXEC.

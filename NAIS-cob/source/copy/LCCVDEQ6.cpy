      *----------------------------------------------------------------*
      *    COPY      ..... LCCVDEQ6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO DETTAGLIO QUESTIONARIO
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVDEQ5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCVDEQ1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-DETT-QUEST.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE DETT-QUEST

      *--> NOME TABELLA FISICA DB
           MOVE 'DETT-QUEST'                   TO WK-TABELLA

      *--> SE LO STATUS NON E' "INVARIATO"
           IF  NOT WDEQ-ST-INV(IX-TAB-DEQ)
           AND NOT WDEQ-ST-CON(IX-TAB-DEQ)
           AND     WDEQ-ELE-DETT-QUEST-MAX NOT = 0

              MOVE WDEQ-ID-QUEST(IX-TAB-DEQ)
                TO WS-ID-QUEST

              PERFORM RICERCA-QUEST-PTF
                 THRU RICERCA-QUEST-PTF-EX

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WDEQ-ST-ADD(IX-TAB-DEQ)

      *-->             ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA
                            TO WDEQ-ID-PTF(IX-TAB-DEQ)
                               DEQ-ID-DETT-QUEST
                          MOVE WS-ID-PTF
                            TO DEQ-ID-QUEST
                          MOVE WMOV-ID-PTF
                            TO DEQ-ID-MOVI-CRZ
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        UPDATE
                  WHEN WDEQ-ST-MOD(IX-TAB-DEQ)

                       MOVE WDEQ-ID-PTF(IX-TAB-DEQ)
                         TO DEQ-ID-DETT-QUEST
                       MOVE WS-ID-PTF
                         TO DEQ-ID-QUEST
                       MOVE WMOV-ID-PTF
                         TO DEQ-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        DELETE
                  WHEN WDEQ-ST-DEL(IX-TAB-DEQ)

                       MOVE WDEQ-ID-PTF(IX-TAB-DEQ)
                         TO DEQ-ID-DETT-QUEST
                       MOVE WS-ID-PTF
                         TO DEQ-ID-QUEST
                       MOVE WMOV-ID-PTF
                         TO DEQ-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN
                 PERFORM VAL-DCLGEN-DEQ
                    THRU VAL-DCLGEN-DEQ-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-DEQ
                    THRU VALORIZZA-AREA-DSH-DEQ-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-DETT-QUEST-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    RICERCA DELL' ID-QUEST-PTF NELLA TABELLA QUESTIONARIO
      *----------------------------------------------------------------*
       RICERCA-QUEST-PTF.

           SET NON-TROVATO TO TRUE.

           PERFORM VARYING IX-TAB-QUE FROM 1 BY 1
                     UNTIL IX-TAB-QUE > WQUE-ELE-QUEST-MAX
                        OR TROVATO

              IF WS-ID-QUEST = WQUE-ID-QUEST(IX-TAB-QUE)

                 MOVE WQUE-ID-PTF(IX-TAB-QUE)
                   TO WS-ID-PTF
                 SET  TROVATO TO TRUE

              END-IF

           END-PERFORM.

       RICERCA-QUEST-PTF-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-DEQ.

      *--> DCLGEN TABELLA
           MOVE DETT-QUEST              TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-DEQ-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVDEQ5.

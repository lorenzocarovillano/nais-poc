
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVL164
      *   ULTIMO AGG. 07 GEN 2016
      *------------------------------------------------------------

       INIZIA-TOT-L16.

           PERFORM INIZIA-ZEROES-L16 THRU INIZIA-ZEROES-L16-EX

           PERFORM INIZIA-SPACES-L16 THRU INIZIA-SPACES-L16-EX

           PERFORM INIZIA-NULL-L16 THRU INIZIA-NULL-L16-EX.

       INIZIA-TOT-L16-EX.
           EXIT.

       INIZIA-NULL-L16.
           MOVE HIGH-VALUES TO (SF)-TP-MOVI-RIFTO-NULL.
       INIZIA-NULL-L16-EX.
           EXIT.

       INIZIA-ZEROES-L16.
           MOVE 0 TO (SF)-COD-COMP-ANIA
           MOVE 0 TO (SF)-COD-CAN
           MOVE 0 TO (SF)-TP-MOVI
           MOVE 0 TO (SF)-DS-VER
           MOVE 0 TO (SF)-DS-TS-CPTZ.
       INIZIA-ZEROES-L16-EX.
           EXIT.

       INIZIA-SPACES-L16.
           MOVE SPACES TO (SF)-COD-BLOCCO
           MOVE SPACES TO (SF)-GRAV-FUNZ-BLOCCO
           MOVE SPACES TO (SF)-DS-OPER-SQL
           MOVE SPACES TO (SF)-DS-UTENTE
           MOVE SPACES TO (SF)-DS-STATO-ELAB.
       INIZIA-SPACES-L16-EX.
           EXIT.

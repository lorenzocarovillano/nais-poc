
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVADE5
      *   ULTIMO AGG. 07 DIC 2010
      *------------------------------------------------------------

       VAL-DCLGEN-ADE.
           IF (SF)-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL
              TO ADE-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU
              TO ADE-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF NOT NUMERIC
              MOVE 0 TO ADE-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF
              TO ADE-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF NOT NUMERIC
              MOVE 0 TO ADE-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF
              TO ADE-DT-END-EFF
           END-IF
           IF (SF)-IB-PREV-NULL = HIGH-VALUES
              MOVE (SF)-IB-PREV-NULL
              TO ADE-IB-PREV-NULL
           ELSE
              MOVE (SF)-IB-PREV
              TO ADE-IB-PREV
           END-IF
           IF (SF)-IB-OGG-NULL = HIGH-VALUES
              MOVE (SF)-IB-OGG-NULL
              TO ADE-IB-OGG-NULL
           ELSE
              MOVE (SF)-IB-OGG
              TO ADE-IB-OGG
           END-IF
           MOVE (SF)-COD-COMP-ANIA
              TO ADE-COD-COMP-ANIA
           IF (SF)-DT-DECOR-NULL = HIGH-VALUES
              MOVE (SF)-DT-DECOR-NULL
              TO ADE-DT-DECOR-NULL
           ELSE
             IF (SF)-DT-DECOR = ZERO
                MOVE HIGH-VALUES
                TO ADE-DT-DECOR-NULL
             ELSE
              MOVE (SF)-DT-DECOR
              TO ADE-DT-DECOR
             END-IF
           END-IF
           IF (SF)-DT-SCAD-NULL = HIGH-VALUES
              MOVE (SF)-DT-SCAD-NULL
              TO ADE-DT-SCAD-NULL
           ELSE
             IF (SF)-DT-SCAD = ZERO
                MOVE HIGH-VALUES
                TO ADE-DT-SCAD-NULL
             ELSE
              MOVE (SF)-DT-SCAD
              TO ADE-DT-SCAD
             END-IF
           END-IF
           IF (SF)-ETA-A-SCAD-NULL = HIGH-VALUES
              MOVE (SF)-ETA-A-SCAD-NULL
              TO ADE-ETA-A-SCAD-NULL
           ELSE
              MOVE (SF)-ETA-A-SCAD
              TO ADE-ETA-A-SCAD
           END-IF
           IF (SF)-DUR-AA-NULL = HIGH-VALUES
              MOVE (SF)-DUR-AA-NULL
              TO ADE-DUR-AA-NULL
           ELSE
              MOVE (SF)-DUR-AA
              TO ADE-DUR-AA
           END-IF
           IF (SF)-DUR-MM-NULL = HIGH-VALUES
              MOVE (SF)-DUR-MM-NULL
              TO ADE-DUR-MM-NULL
           ELSE
              MOVE (SF)-DUR-MM
              TO ADE-DUR-MM
           END-IF
           IF (SF)-DUR-GG-NULL = HIGH-VALUES
              MOVE (SF)-DUR-GG-NULL
              TO ADE-DUR-GG-NULL
           ELSE
              MOVE (SF)-DUR-GG
              TO ADE-DUR-GG
           END-IF
           MOVE (SF)-TP-RGM-FISC
              TO ADE-TP-RGM-FISC
           IF (SF)-TP-RIAT-NULL = HIGH-VALUES
              MOVE (SF)-TP-RIAT-NULL
              TO ADE-TP-RIAT-NULL
           ELSE
              MOVE (SF)-TP-RIAT
              TO ADE-TP-RIAT
           END-IF
           MOVE (SF)-TP-MOD-PAG-TIT
              TO ADE-TP-MOD-PAG-TIT
           IF (SF)-TP-IAS-NULL = HIGH-VALUES
              MOVE (SF)-TP-IAS-NULL
              TO ADE-TP-IAS-NULL
           ELSE
              MOVE (SF)-TP-IAS
              TO ADE-TP-IAS
           END-IF
           IF (SF)-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
              MOVE (SF)-DT-VARZ-TP-IAS-NULL
              TO ADE-DT-VARZ-TP-IAS-NULL
           ELSE
             IF (SF)-DT-VARZ-TP-IAS = ZERO
                MOVE HIGH-VALUES
                TO ADE-DT-VARZ-TP-IAS-NULL
             ELSE
              MOVE (SF)-DT-VARZ-TP-IAS
              TO ADE-DT-VARZ-TP-IAS
             END-IF
           END-IF
           IF (SF)-PRE-NET-IND-NULL = HIGH-VALUES
              MOVE (SF)-PRE-NET-IND-NULL
              TO ADE-PRE-NET-IND-NULL
           ELSE
              MOVE (SF)-PRE-NET-IND
              TO ADE-PRE-NET-IND
           END-IF
           IF (SF)-PRE-LRD-IND-NULL = HIGH-VALUES
              MOVE (SF)-PRE-LRD-IND-NULL
              TO ADE-PRE-LRD-IND-NULL
           ELSE
              MOVE (SF)-PRE-LRD-IND
              TO ADE-PRE-LRD-IND
           END-IF
           IF (SF)-RAT-LRD-IND-NULL = HIGH-VALUES
              MOVE (SF)-RAT-LRD-IND-NULL
              TO ADE-RAT-LRD-IND-NULL
           ELSE
              MOVE (SF)-RAT-LRD-IND
              TO ADE-RAT-LRD-IND
           END-IF
           IF (SF)-PRSTZ-INI-IND-NULL = HIGH-VALUES
              MOVE (SF)-PRSTZ-INI-IND-NULL
              TO ADE-PRSTZ-INI-IND-NULL
           ELSE
              MOVE (SF)-PRSTZ-INI-IND
              TO ADE-PRSTZ-INI-IND
           END-IF
           IF (SF)-FL-COINC-ASSTO-NULL = HIGH-VALUES
              MOVE (SF)-FL-COINC-ASSTO-NULL
              TO ADE-FL-COINC-ASSTO-NULL
           ELSE
              MOVE (SF)-FL-COINC-ASSTO
              TO ADE-FL-COINC-ASSTO
           END-IF
           IF (SF)-IB-DFLT-NULL = HIGH-VALUES
              MOVE (SF)-IB-DFLT-NULL
              TO ADE-IB-DFLT-NULL
           ELSE
              MOVE (SF)-IB-DFLT
              TO ADE-IB-DFLT
           END-IF
           IF (SF)-MOD-CALC-NULL = HIGH-VALUES
              MOVE (SF)-MOD-CALC-NULL
              TO ADE-MOD-CALC-NULL
           ELSE
              MOVE (SF)-MOD-CALC
              TO ADE-MOD-CALC
           END-IF
           IF (SF)-TP-FNT-CNBTVA-NULL = HIGH-VALUES
              MOVE (SF)-TP-FNT-CNBTVA-NULL
              TO ADE-TP-FNT-CNBTVA-NULL
           ELSE
              MOVE (SF)-TP-FNT-CNBTVA
              TO ADE-TP-FNT-CNBTVA
           END-IF
           IF (SF)-IMP-AZ-NULL = HIGH-VALUES
              MOVE (SF)-IMP-AZ-NULL
              TO ADE-IMP-AZ-NULL
           ELSE
              MOVE (SF)-IMP-AZ
              TO ADE-IMP-AZ
           END-IF
           IF (SF)-IMP-ADER-NULL = HIGH-VALUES
              MOVE (SF)-IMP-ADER-NULL
              TO ADE-IMP-ADER-NULL
           ELSE
              MOVE (SF)-IMP-ADER
              TO ADE-IMP-ADER
           END-IF
           IF (SF)-IMP-TFR-NULL = HIGH-VALUES
              MOVE (SF)-IMP-TFR-NULL
              TO ADE-IMP-TFR-NULL
           ELSE
              MOVE (SF)-IMP-TFR
              TO ADE-IMP-TFR
           END-IF
           IF (SF)-IMP-VOLO-NULL = HIGH-VALUES
              MOVE (SF)-IMP-VOLO-NULL
              TO ADE-IMP-VOLO-NULL
           ELSE
              MOVE (SF)-IMP-VOLO
              TO ADE-IMP-VOLO
           END-IF
           IF (SF)-PC-AZ-NULL = HIGH-VALUES
              MOVE (SF)-PC-AZ-NULL
              TO ADE-PC-AZ-NULL
           ELSE
              MOVE (SF)-PC-AZ
              TO ADE-PC-AZ
           END-IF
           IF (SF)-PC-ADER-NULL = HIGH-VALUES
              MOVE (SF)-PC-ADER-NULL
              TO ADE-PC-ADER-NULL
           ELSE
              MOVE (SF)-PC-ADER
              TO ADE-PC-ADER
           END-IF
           IF (SF)-PC-TFR-NULL = HIGH-VALUES
              MOVE (SF)-PC-TFR-NULL
              TO ADE-PC-TFR-NULL
           ELSE
              MOVE (SF)-PC-TFR
              TO ADE-PC-TFR
           END-IF
           IF (SF)-PC-VOLO-NULL = HIGH-VALUES
              MOVE (SF)-PC-VOLO-NULL
              TO ADE-PC-VOLO-NULL
           ELSE
              MOVE (SF)-PC-VOLO
              TO ADE-PC-VOLO
           END-IF
           IF (SF)-DT-NOVA-RGM-FISC-NULL = HIGH-VALUES
              MOVE (SF)-DT-NOVA-RGM-FISC-NULL
              TO ADE-DT-NOVA-RGM-FISC-NULL
           ELSE
             IF (SF)-DT-NOVA-RGM-FISC = ZERO
                MOVE HIGH-VALUES
                TO ADE-DT-NOVA-RGM-FISC-NULL
             ELSE
              MOVE (SF)-DT-NOVA-RGM-FISC
              TO ADE-DT-NOVA-RGM-FISC
             END-IF
           END-IF
           IF (SF)-FL-ATTIV-NULL = HIGH-VALUES
              MOVE (SF)-FL-ATTIV-NULL
              TO ADE-FL-ATTIV-NULL
           ELSE
              MOVE (SF)-FL-ATTIV
              TO ADE-FL-ATTIV
           END-IF
           IF (SF)-IMP-REC-RIT-VIS-NULL = HIGH-VALUES
              MOVE (SF)-IMP-REC-RIT-VIS-NULL
              TO ADE-IMP-REC-RIT-VIS-NULL
           ELSE
              MOVE (SF)-IMP-REC-RIT-VIS
              TO ADE-IMP-REC-RIT-VIS
           END-IF
           IF (SF)-IMP-REC-RIT-ACC-NULL = HIGH-VALUES
              MOVE (SF)-IMP-REC-RIT-ACC-NULL
              TO ADE-IMP-REC-RIT-ACC-NULL
           ELSE
              MOVE (SF)-IMP-REC-RIT-ACC
              TO ADE-IMP-REC-RIT-ACC
           END-IF
           IF (SF)-FL-VARZ-STAT-TBGC-NULL = HIGH-VALUES
              MOVE (SF)-FL-VARZ-STAT-TBGC-NULL
              TO ADE-FL-VARZ-STAT-TBGC-NULL
           ELSE
              MOVE (SF)-FL-VARZ-STAT-TBGC
              TO ADE-FL-VARZ-STAT-TBGC
           END-IF
           IF (SF)-FL-PROVZA-MIGRAZ-NULL = HIGH-VALUES
              MOVE (SF)-FL-PROVZA-MIGRAZ-NULL
              TO ADE-FL-PROVZA-MIGRAZ-NULL
           ELSE
              MOVE (SF)-FL-PROVZA-MIGRAZ
              TO ADE-FL-PROVZA-MIGRAZ
           END-IF
           IF (SF)-IMPB-VIS-DA-REC-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-DA-REC-NULL
              TO ADE-IMPB-VIS-DA-REC-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-DA-REC
              TO ADE-IMPB-VIS-DA-REC
           END-IF
           IF (SF)-DT-DECOR-PREST-BAN-NULL = HIGH-VALUES
              MOVE (SF)-DT-DECOR-PREST-BAN-NULL
              TO ADE-DT-DECOR-PREST-BAN-NULL
           ELSE
             IF (SF)-DT-DECOR-PREST-BAN = ZERO
                MOVE HIGH-VALUES
                TO ADE-DT-DECOR-PREST-BAN-NULL
             ELSE
              MOVE (SF)-DT-DECOR-PREST-BAN
              TO ADE-DT-DECOR-PREST-BAN
             END-IF
           END-IF
           IF (SF)-DT-EFF-VARZ-STAT-T-NULL = HIGH-VALUES
              MOVE (SF)-DT-EFF-VARZ-STAT-T-NULL
              TO ADE-DT-EFF-VARZ-STAT-T-NULL
           ELSE
             IF (SF)-DT-EFF-VARZ-STAT-T = ZERO
                MOVE HIGH-VALUES
                TO ADE-DT-EFF-VARZ-STAT-T-NULL
             ELSE
              MOVE (SF)-DT-EFF-VARZ-STAT-T
              TO ADE-DT-EFF-VARZ-STAT-T
             END-IF
           END-IF
           IF (SF)-DS-RIGA NOT NUMERIC
              MOVE 0 TO ADE-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA
              TO ADE-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO ADE-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO ADE-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO ADE-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ NOT NUMERIC
              MOVE 0 TO ADE-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ
              TO ADE-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ NOT NUMERIC
              MOVE 0 TO ADE-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ
              TO ADE-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO ADE-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO ADE-DS-STATO-ELAB
           IF (SF)-CUM-CNBT-CAP-NULL = HIGH-VALUES
              MOVE (SF)-CUM-CNBT-CAP-NULL
              TO ADE-CUM-CNBT-CAP-NULL
           ELSE
              MOVE (SF)-CUM-CNBT-CAP
              TO ADE-CUM-CNBT-CAP
           END-IF
           IF (SF)-IMP-GAR-CNBT-NULL = HIGH-VALUES
              MOVE (SF)-IMP-GAR-CNBT-NULL
              TO ADE-IMP-GAR-CNBT-NULL
           ELSE
              MOVE (SF)-IMP-GAR-CNBT
              TO ADE-IMP-GAR-CNBT
           END-IF
           IF (SF)-DT-ULT-CONS-CNBT-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-CONS-CNBT-NULL
              TO ADE-DT-ULT-CONS-CNBT-NULL
           ELSE
             IF (SF)-DT-ULT-CONS-CNBT = ZERO
                MOVE HIGH-VALUES
                TO ADE-DT-ULT-CONS-CNBT-NULL
             ELSE
              MOVE (SF)-DT-ULT-CONS-CNBT
              TO ADE-DT-ULT-CONS-CNBT
             END-IF
           END-IF
           IF (SF)-IDEN-ISC-FND-NULL = HIGH-VALUES
              MOVE (SF)-IDEN-ISC-FND-NULL
              TO ADE-IDEN-ISC-FND-NULL
           ELSE
              MOVE (SF)-IDEN-ISC-FND
              TO ADE-IDEN-ISC-FND
           END-IF
           IF (SF)-NUM-RAT-PIAN-NULL = HIGH-VALUES
              MOVE (SF)-NUM-RAT-PIAN-NULL
              TO ADE-NUM-RAT-PIAN-NULL
           ELSE
              MOVE (SF)-NUM-RAT-PIAN
              TO ADE-NUM-RAT-PIAN
           END-IF
           IF (SF)-DT-PRESC-NULL = HIGH-VALUES
              MOVE (SF)-DT-PRESC-NULL
              TO ADE-DT-PRESC-NULL
           ELSE
             IF (SF)-DT-PRESC = ZERO
                MOVE HIGH-VALUES
                TO ADE-DT-PRESC-NULL
             ELSE
              MOVE (SF)-DT-PRESC
              TO ADE-DT-PRESC
             END-IF
           END-IF
           IF (SF)-CONCS-PREST-NULL = HIGH-VALUES
              MOVE (SF)-CONCS-PREST-NULL
              TO ADE-CONCS-PREST-NULL
           ELSE
              MOVE (SF)-CONCS-PREST
              TO ADE-CONCS-PREST
           END-IF.
       VAL-DCLGEN-ADE-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ESTRATTO CONTO - FASE DIAGNOSTICO
      *       - AREA ERRORI DI CONTROLLO FLUSSO
      *----------------------------------------------------------------*
       01 MSG-ERRORE           PIC X(185).
          88 CNTRL-1           VALUE
             '1; Il numero delle polizze non concide con il numero delle
      -      ' polizze del tipo Record 3 - Garanzie'.
          88 CNTRL-2           VALUE
             '2; DT-RIF-DA E DT-RIF-A non sono coerenti con il periodo d
      -      'i riferimento richiesto e con la DT-DECOR-POLI'.
          88 CNTRL-3           VALUE
             '3; CAP-LIQ-DT-RIF-EST-CC - PRESTAZIONE-AC-POL <
      -      ' -0,01 euro'.
          88 CNTRL-4           VALUE
             '4; IMP-LOR-CED-LIQU - Flusso generazione cedole batch
      -      '  <> 0'.
          88 CNTRL-5           VALUE
             '5; IMP-LOR-CED-LIQU > - Campi CC-PAESE - CC-CHECK-DIGIT -
      -      ' CC-CIN - CC-ABI - CC-CAB - CC-NUM-CONTO non valorizzati'.
          88 CNTRL-6           VALUE
             '6; TOT-CUM-PREMI-VERS-TOT-CUM-PRE-INVST-AA-RIF)/TOT-CUM-PR
      -      'EMI-VERS  > 6,00% (MR-UL) - > 25% (RV)        '.
          88 CNTRL-7           VALUE
             '7; TOT-CUM-PREMI-VERS = 0,  TOT-CUM-PRE-INVST-AA-RIF
      -      ' diverso da 0'.
          88 CNTRL-8           VALUE
             '8; DT-DECOR-POLI < DT-RIF-DA, allora TOT-CUM-PREMI-VERS-AP
      -      ' non > 0 '.
          88 CNTRL-9           VALUE
             '9; DT-RIF-DA=DT-RIF-A-valorizzati TOT-CUM-PREMI-VERS-AP-
      -      ' PRESTAZIONE-AP-POL - IMP-LRD-RISC-PARZ-AP-POL -
      -      'IMP-LRD-RISC-PARZ-AC-POL '.
          88 CNTRL-10          VALUE
             '10; DT-DECOR-POLI < DT-RIF-DA,PRESTAZIONE-AP-POL non > 0'.
          88 CNTRL-11          VALUE
             '11; PRESTAZIONE-AC-POL <=0'.
          88 CNTRL-12          VALUE
             '12; PRESTAZIONE-AC-POL - RISC-TOT-DT-RIF-EST-CC < -0,01'.
          88 CNTRL-13          VALUE
             '13; La somma del campo TOT-CUM-PRE-INVST-AA-RIF non corris
      -      'ponde alla somma del campo CUM-PREM-INVST-AA-RIF(Rec. 3)'.
          88 CNTRL-14          VALUE
             '14; La somma del campo TOT-CUM-PREMI-VERS non corrisponde
      -      ' alla somma del campo CUM-PREM-VERS-AA-RIF (Rec. 3)'.
          88 CNTRL-15          VALUE
             '15; La somma del campo PRESTAZIONE-AC-POL non corrisponde
      -      ' alla somma del campo PREST-MATUR-AA-RIF  (Rec. 3)'.
          88 CNTRL-16          VALUE
             '16; La somma del campo CAP-LIQ-DT-RIF-EST-CC non
      -      'corrisponde alla somma del campo CAP-LIQ-GA (Rec. 3),
      -      ' filtrato per TP-GAR = 1 '.
          88 CNTRL-17          VALUE
             '17; La somma del campo RISC-TOT-DT-RIF-EST-CC non
      -      'corrisponde alla somma del campo RISC-TOT-GA (Rec. 3)'.
          88 CNTRL-18          VALUE
             '18; La somma del campo TOT-CUM-PREMI-VERS-AP non corrispon
      -      'de alla somma del campo CUM-PREM-VERS-EC-PREC  (Rec. 3)'.
          88 CNTRL-19          VALUE
             '19; La somma del campo PRESTAZIONE-AP-POL  non
      -      'corrisponde alla somma del campo  PREST-MATUR-EC-PREC
      -      ' (Rec. 3), filtrato per TP-GAR = 1'.
          88 CNTRL-20          VALUE
             '20; La somma del campo IMP-LRD-RISC-PARZ-AP-POL  non
      -      'corrisponde alla somma del campo IMP-LRD-RISC-PARZ-AP-POL
      -      ' (Rec. 3)'.
          88 CNTRL-21          VALUE
             '21; La somma del campo IMP-LRD-RISC-PARZ-AC-POL  non
      -      'corrisponde alla somma del campo IMP-LRD-RISC-PARZ-AC-POL
      -      ' (Rec. 3)'.
          88 CNTRL-22          VALUE
             '22; BOLLO - Output flusso calcolo bollo 31/12
      -      'Esclusione casi <> 0'.
          88 CNTRL-23          VALUE
             '23; COGNOME-ASSICU/NOME-ASSICU non valorizzato'.
          88 CNTRL-24          VALUE
             '24; CAP-LIQ-GA - PREST-MATUR-AA-RIF filtrato per TP-GAR =
      -      '1 < -0,01 euro'.
          88 CNTRL-25          VALUE
             '25; TASSO-TECNICO - Flusso rivalutazione batch filtrato pe
      -      'r TP-GAR=1 and TP-INVST-GAR=4 <> 0 '.
          88 CNTRL-26          VALUE
             '26; REND-LOR-GEST-SEP-GAR - Flusso rivalutazione batch
      -      'filtrato per TP-GAR=1 and TP-INVST-GAR=4 <> 0 '.
          88 CNTRL-27          VALUE
             '27; ALIQ-RETROC-RICON-GAR - Flusso rivalutazione batch
      -      'filtrato per TP-GAR=1 and TP-INVST-GAR=4 <> 0 '.
          88 CNTRL-28          VALUE
             '28; TASSO-ANNO-REND-RETROC-GAR - Flusso rivalutazione batc
      -      'h filtrato per TP-GAR=1 and TP-INVST-GAR=4 <> 0 '.
          88 CNTRL-29          VALUE
             '29; PC-COMM-GEST-GAR - Flusso rivalutazione batch
      -      'filtrato per TP-GAR=1 and TP-INVST-GAR=4 <> 0 '.
          88 CNTRL-30          VALUE
             '30; TASSO-ANN-RIVAL-PREST-GAR - Flusso rivalutazione batch
      -      'filtrato per TP-GAR=1 and TP-INVST-GAR=4 <> 0 '.
          88 CNTRL-31          VALUE
             '31; REN-MIN-TRNUT - Flusso rivalutazione batch filtrato pe
      -      'r TP-GAR=1 and TP-INVST-GAR=4 <> 0 '.
          88 CNTRL-32          VALUE
             '32; REN-MIN-GARTO - Flusso rivalutazione batch filtrato pe
      -      'r TP-GAR=1 and TP-INVST-GAR=4 <> 0 '.
          88 CNTRL-33          VALUE
             '33; (PREST-MATUR-AA-RIF - g. base - CAP-LIQ-GA- g. acc. -)
      -      '- CAP-LIQ-GA - g. base.  < - 1,00 o  > 1,00'.
          88 CNTRL-34          VALUE
             '34; NON coerenza campi DT-INI-PER-INV - DT-FIN-PER-INV con
      -      ' periodo di riferimento elaborato'.
          88 CNTRL-35          VALUE
             '35; Incoerenza campi PREST-MATUR-EC-PREC, per TP-INVST-GAR
      -      '=7 con campo CNTRVAL-PREC   (Rec.8). Esclusione casi in cu
      -      'i la differenza  < - 0,01 o > 0,01 euro'.
          88 CNTRL-36          VALUE
             '36; Incoerenza campi PREST-MATUR-AA-RIF filtrato per TP-IN
      -      'VST-GAR=7 con campo CNTRVAL-ATTUALE (Rec 8) , esclusione c
      -      'asi  < - 0,01 > 0,01 euro'.
          88 CNTRL-37          VALUE
             '37;CONTROLLO SUL RECORD 8'.
      *      '37;(NUM-QUO-PREC - NUM-QUO-DISINV-RISC-PARZ -
      *      'NUM-QUO-DIS-RISC-PARZ-PRG - NUM-QUO-DISINV-IMPOS-SOST -
      *      'NUM-QUO-DISINV-COMMGES - NUM-QUO-DISINV-PREL-COSTI -
      *      'NUM-QUO-DISINV-SWITCH - NUM-QUO-DISINV-NAV-NEG +
      *      'NUM-QUO-INVST-SWITCH + NUM-QUO-INVST-VERS +
      *      'NUM-QUO-INVST-REBATE  + NUM-QUO-INVST-PURO-RISC +
      *      'NUM-QUO-INVST-NAV-POS) - NUM-QUO-ATTUALE.
      *      '< - 0,01 o  > 0,01 '.
          88 CNTRL-38          VALUE
             '38;Incoerenza campo IMP-LORDO-RISC-PARZ (Rec. 9) con i cor
      -      'rispondenti campi: IMP-LRD-RISC-PARZ-AC-POL (Rec. 1) IMP-L
      -      'RD-RISC-PARZ-AC-GAR (Rec. 3) '.
          88 CNTRL-39          VALUE
             '39; DESC-TP-MOVI=PAGAMENTO CEDOLA AND IMP-OPER con il corr
      -      'ispondente campo IMP-LOR-CED-LIQU (Rec. 1)'.
          88 CNTRL-40          VALUE
             '40; DESC-TP-MOVI=RISCATTO PARZIALE AND IMP-OPER con il cor
      -      'rispondente campo IMP-LORDO-RISC-PARZ (Rec.9)'.
          88 CNTRL-41          VALUE
             '41; DESC-TP-MOVI=VERSAMENTO PREMI AND IMP-IMP-VERS con il
      -      'corrispondente campo TOT-CUM-PREMI-VERS (Rec.1)'.
          88 CNTRL-42          VALUE
             '42; DESC-TP-MOVI=VERSAMENTO PREMI AND IMP-IMP-OPER con il
      -      'corrispondente campo TOT-CUM-PRE-INVST-AA-RIF (Rec. 1)'.
          88 CNTRL-43          VALUE
             '43; DESC-TP-MOVI=CALCOLO IMPOSTA SOSTITUTIVA AND IMP-OPER)
      -      ' Flusso imposta'.
          88 CNTRL-44          VALUE
             '44; DESC-TP-MOVI=COMMISSIONI DI GESTIONE AND NUMERO QUO
      -      ' con il corrispondente campo NUM-QUO-DISINV-COMMGES
      -      ' (Rec. 8)'.
          88 CNTRL-45          VALUE
             '45; DESC-TP-MOVI=COSTO CASO MORTE AND NUMERO QUO con il
      -      ' corrispondente campo NUM-QUO-DISINV-PREL-COSTI (Rec. 8)'.
          88 CNTRL-46          VALUE
             '46; DESC-TP-MOVI=REBATE AND NUMERO QUO con il corrisponden
      -      'te campo NUM-QUO-INVST-REBATE  (Rec. 8)'.
          88 CNTRL-47          VALUE
             '47; DESC-TP-MOVI=VERSAMENTO PREMI AND NUMERO QUO con il co
      -      'rrispondente campo NUM-QUO-INVST-VERS  (Rec. 8)'.
          88 CNTRL-48          VALUE
             '48; DESC-TP-MOVI=sum SWITCH INGRESSO (stand alone/massivo/
      -      'quick) AND NUMERO QUO con il corrispondente campo NUM-QUO-
      -      'DISINV-SWITCH (Rec. 8)'.
          88 CNTRL-49          VALUE
             '49; DESC-TP-MOVI= SWITCH USCITA AND NUMERO QUO con il corr
      -      'ispondente campo  NUM-QUO-INV-SWITCH (Rec. 8)'.
13815     88 CNTRL-50          VALUE
13815        '50; DESC-TP-MOVI=COMPENSAZIONE NAV NEGATIVA AND NUMERO QUO
13815 -      ' con il corrispondente campo NUM-QUO-DISINV-COMP-NAV
13815 -      ' (Rec. 8)'.
13815     88 CNTRL-51          VALUE
13815        '51; DESC-TP-MOVI=COMPENSAZIONE NAV POSITIVA AND NUMERO QUO
13815 -      ' con il corrispondente campo NUM-QUO-INVST-COMP-NAV
13815 -      ' (Rec. 8)'.
      *----------------------------------------------------------------*
      *    ESTRATTO CONTO - FASE DIAGNOSTICO
      *       - AREA TIPO MOVIMENTO
      *----------------------------------------------------------------*
       01  WS-DESC-TP-MOVI.
           03 TP-VERS-PREMI                 PIC X(050) VALUE
              'Versamento premi                                  '.
           03 TP-RISC-PARZ                  PIC X(050) VALUE
              'Riscatto parziale                                 '.
           03 TP-SW-IN-1                    PIC X(050) VALUE
              'Switch in ingresso                                '.
           03 TP-SW-OUT-1                   PIC X(050) VALUE
              'Switch in uscita                                  '.
           03 TP-SW-IN-2                    PIC X(050) VALUE
              'Switch in ingresso quick                          '.
           03 TP-SW-OUT-2                   PIC X(050) VALUE
              'Switch in uscita quick                            '.
           03 TP-SW-IN-3                    PIC X(050) VALUE
              'Switch in ingresso quick                          '.
           03 TP-SW-OUT-3                   PIC X(050) VALUE
              'Switch in uscita quick                            '.
           03 TP-SW-IN-4                    PIC X(050) VALUE
              'Switch in ingresso massivo                        '.
           03 TP-SW-OUT-4                   PIC X(050) VALUE
              'Switch in uscita massivo                          '.
           03 TP-SW-IN-5                    PIC X(050) VALUE
              'Switch in ingresso da ribilanciamento             '.
           03 TP-SW-OUT-5                   PIC X(050) VALUE
              'Switch in uscita da ribilanciamento               '.
           03 TP-SW-IN-6                    PIC X(050) VALUE
              'Switch in ingresso da chiusura fondi              '.
           03 TP-SW-OUT-6                   PIC X(050) VALUE
              'Switch in uscita da chiusura fondi                '.
10818      03 TP-SW-IN-7                    PIC X(050) VALUE
10818         'Ribilanciamento automatico in ingresso            '.
10818      03 TP-SW-OUT-7                   PIC X(050) VALUE
10818         'Ribilanciamento automatico in uscita              '.
10818      03 TP-SW-IN-8                    PIC X(050) VALUE
10818         'Ribilanciamento periodico in ingresso             '.
10818      03 TP-SW-OUT-8                   PIC X(050) VALUE
10818         'Ribilanciamento periodico in uscita               '.
      *    03 TP-SW-IN                      PIC X(050) VALUE
      *       'Switch in ingresso                                '.
      *    03 TP-SW-OUT                     PIC X(050) VALUE
      *       'Switch in uscita                                  '.
           03 TP-CASO-MORTE                 PIC X(050) VALUE
              'Costo caso morte                                  '.
           03 TP-PAG-CEDOLA                 PIC X(050) VALUE
              'Pagamento cedola                                  '.
           03 TP-COMM-GEST                  PIC X(050) VALUE
              'Commissioni di gestione                           '.
           03 TP-REBATE                     PIC X(050) VALUE
              'Rebate                                            '.
           03 TP-IMP-SOST                   PIC X(050) VALUE
              'Calcolo imposta sostitutiva                       '.
           03 TP-IN-SW-PASSO-PASSO          PIC X(050) VALUE
              'Switch da Passo dopo Passo in ingresso            '.
           03 TP-OUT-SW-PASSO-PASSO         PIC X(050) VALUE
              'Switch da Passo dopo Passo in uscita              '.
           03 TP-IN-SW-STOP-LOSS            PIC X(050) VALUE
              'Switch da Stop Loss in ingresso                   '.
           03 TP-IN-SW-LINEA                PIC X(050) VALUE
              'Switch di Linea in ingresso                       '.
           03 TP-OUT-SW-STOP-LOSS           PIC X(050) VALUE
              'Switch da Stop Loss in uscita                     '.
           03 TP-OUT-SW-LINEA               PIC X(050) VALUE
              'Switch di Linea in uscita                         '.
12908      03 TP-IN-SW-BATCH-PARZ-MASS           PIC X(050) VALUE
12908 *        'Switch batch massivo parziale  in ingresso       '.
14151         'Switch batch massivo automatico in ingresso       '.
14151      03 TP-OUT-SW-BATCH-PARZ-MASS           PIC X(050) VALUE
14151         'Switch batch massivo automatico in uscita         '.
13815      03 TP-COMP-NAV-NEG               PIC X(050) VALUE
13815         'Disinvestimento per operazione straordinaria      '.
13815      03 TP-COMP-NAV-POS               PIC X(050) VALUE
13815         'Investimento per operazione straordinaria         '.
FNZ        03 TP-IN-SW-RIBIL                PIC X(050) VALUE
FNZ           'Switch Ribilanciamento fnz in ingresso            '.
FNZ        03 TP-IN-SW-GAP-EVENT            PIC X(050) VALUE
FNZ           'Switch Gap Event FNZ   in ingresso                '.
FNZ        03 TP-IN-SW-SUP-SOGLIA           PIC X(050) VALUE
FNZ           'Switch Superamento Soglia FNZ in ingresso         '.
FNZ        03 TP-IN-SW-MASSIVO-FNZ          PIC X(050) VALUE
FNZ           'Switch Massivo FNZ in ingresso                    '.
FNZ        03 TP-OUT-SW-RIBIL                PIC X(050) VALUE
FNZ           'Switch Ribilanciamento fnz in uscita              '.
FNZ        03 TP-OUT-SW-GAP-EVENT            PIC X(050) VALUE
FNZ           'Switch Gap Event FNZ   in uscita                  '.
FNZ        03 TP-OUT-SW-SUP-SOGLIA           PIC X(050) VALUE
FNZ           'Switch Superamento Soglia FNZ in uscita           '.
FNZ        03 TP-OUT-SW-MASSIVO-FNZ          PIC X(050) VALUE
FNZ           'Switch Massivo FNZ in uscita                      '.

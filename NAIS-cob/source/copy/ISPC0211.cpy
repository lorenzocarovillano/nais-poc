      *----------------------------------------------------------------*
      *   SERVIZIO CALCOLI NOTEVOLI E LIQUIDAZIONE
      *----------------------------------------------------------------*
         02 ISPC0211-DATI-INPUT.
            05 ISPC0211-COD-COMPAGNIA                PIC 9(005).
      *----------------------------------------------------------------*
      *   DATI CONTRATTO
      *----------------------------------------------------------------*
            05 ISPC0211-NUM-POLIZZA                  PIC X(011).
            05 ISPC0211-COD-PRODOTTO                 PIC X(012).
            05 ISPC0211-DATA-DECORR-POLIZZA          PIC X(008).

      *----------------------------------------------------------------*
      *   DATI CONVENZIONE
      *----------------------------------------------------------------*
            05 ISPC0211-COD-CONVENZIONE              PIC X(012).
            05 ISPC0211-DATA-INIZ-VALID-CONV         PIC X(008).

      *----------------------------------------------------------------*
      *   DATI OPERAZIONE
      *----------------------------------------------------------------*
            05 ISPC0211-DEE                          PIC X(008).
            05 ISPC0211-DATA-RIFERIMENTO             PIC X(008).
            05 ISPC0211-LIVELLO-UTENTE               PIC 9(002).
            05 ISPC0211-OPZ-NUM-MAX-ELE              PIC 9(003).
            05 ISPC0211-OPZIONI                      OCCURS 10.
               07 ISPC0211-OPZ-ESERCITATA            PIC X(002).
            05 ISPC0211-SESSION-ID                   PIC X(020).
            05 ISPC0211-FLG-REC-PROV                 PIC X(001).
            05 ISPC0211-FUNZIONALITA                 PIC 9(005).
            05 ISPC0211-COD-INIZIATIVA               PIC X(012).

      *----------------------------------------------------------------*
      *   AREA ERRORI
      *----------------------------------------------------------------*
         02 ISPC0211-AREA-ERRORI.
            05 ISPC0211-ESITO                       PIC 9(02).
            05 ISPC0211-ERR-NUM-ELE                 PIC 9(03).
            05 ISPC0211-TAB-ERRORI            OCCURS 10.
               10 ISPC0211-COD-ERR                  PIC X(12).
               10 ISPC0211-DESCRIZIONE-ERR          PIC X(50).
               10 ISPC0211-GRAVITA-ERR              PIC 9(02).
               10 ISPC0211-LIVELLO-DEROGA           PIC 9(02).

      *----------------------------------------------------------------*
      *   AREA DATI INPUT/OUTPUT
      *----------------------------------------------------------------*
         02 ISPC0211-DATI-IO.
          03 ISPC0211-TAB-SCHEDE.
      *----------------------------------------------------------------*
      *   SCHEDE PRODOTTO 'P' 'Q' 'O'
      *----------------------------------------------------------------*
            05 ISPC0211-ELE-MAX-SCHEDA-P            PIC 9(04).
            05 ISPC0211-TAB-VAL-P.
               07 ISPC0211-TAB-SCHEDA-P          OCCURS 3.
                  09 ISPC0211-TIPO-LIVELLO-P            PIC X(001).
                  09 ISPC0211-CODICE-LIVELLO-P          PIC X(012).
                  09 ISPC0211-IDENT-LIVELLO-P           PIC 9(009).
                  09 ISPC0211-DT-INIZ-PROD              PIC X(008).
                  09 ISPC0211-COD-RGM-FISC              PIC 9(002).
                  09 ISPC0211-FLG-LIQ-P                 PIC X(002).
                  09 ISPC0211-CODICE-OPZIONE-P          PIC X(002).
                  09 ISPC0211-NUM-COMPON-MAX-ELE-P      PIC 9(003).
                  09 ISPC0211-AREA-VARIABILI-P   OCCURS 100.
                  10 ISPC0211-CODICE-VARIABILE-P     PIC X(012).
                  10 ISPC0211-TIPO-DATO-P            PIC X(001).
                  10 ISPC0211-VAL-GENERICO-P         PIC  X(60).
      *----------------------------------------------------------------*
      *   REDEFINES VALORE GENERICO
      *----------------------------------------------------------------*
                  10 ISPC0211-VAL-IMP-GEN-P          REDEFINES
                     ISPC0211-VAL-GENERICO-P.
                     15 ISPC0211-VALORE-IMP-P        PIC S9(11)V9(7).
                     15 ISPC0211-VAL-IMP-FILLER-P    PIC X(42).

                  10 ISPC0211-VAL-PERC-GEN-P         REDEFINES
                     ISPC0211-VAL-GENERICO-P.
                     15 ISPC0211-VALORE-PERC-P       PIC  9(5)V9(9).
                     15 ISPC0211-VAL-PERC-FILLER-P   PIC X(46).

                  10 ISPC0211-VAL-STR-GEN-P          REDEFINES
                     ISPC0211-VAL-GENERICO-P.
                     15 ISPC0211-VALORE-STR-P        PIC X(60).

            05 ISPC0211-TAB-SCHEDA-R-P REDEFINES ISPC0211-TAB-VAL-P.
              09 FILLER                                PIC X(7339).
              09 ISPC0211-RESTO-TAB-SCHEDA-P           PIC X(14678).


      *----------------------------------------------------------------*
      *   SCHEDE TRANCHE 'G' 'H'
      *----------------------------------------------------------------*
            05 ISPC0211-ELE-MAX-SCHEDA-T             PIC 9(04).
            05 ISPC0211-TAB-VAL-T.
               07 ISPC0211-TAB-SCHEDA-T                 OCCURS 750.
                  09 ISPC0211-TIPO-LIVELLO-T            PIC X(001).
                  09 ISPC0211-CODICE-LIVELLO-T          PIC X(012).
                  09 ISPC0211-IDENT-LIVELLO-T           PIC 9(009).
                  09 ISPC0211-DT-INIZ-TARI              PIC X(008).
                  09 ISPC0211-COD-RGM-FISC-T            PIC 9(002).
                  09 ISPC0211-DT-DECOR-TRCH             PIC X(008).

                  09 ISPC0211-FLG-LIQ-T                 PIC X(002).
                  09 ISPC0211-CODICE-OPZIONE-T          PIC X(002).

                  09 ISPC0211-TIPO-TRCH                 PIC 9(002).
                  09 ISPC0211-FLG-ITN                   PIC 9(001).
                  88 ISPC0211-FLG-ITN-POS            VALUE 0.
                  88 ISPC0211-FLG-ITN-NEG            VALUE 1.


                  09 ISPC0211-NUM-COMPON-MAX-ELE-T      PIC 9(004).
                  09 ISPC0211-AREA-VARIABILI-T   OCCURS 75.
                  10 ISPC0211-CODICE-VARIABILE-T     PIC X(012).
                  10 ISPC0211-TIPO-DATO-T            PIC X(001).
                  10 ISPC0211-VAL-GENERICO-T         PIC  X(60).
      *----------------------------------------------------------------*
      *   REDEFINES VALORE GENERICO
      *----------------------------------------------------------------*
                  10 ISPC0211-VAL-IMP-GEN-T          REDEFINES
                     ISPC0211-VAL-GENERICO-T.
                     15 ISPC0211-VALORE-IMP-T        PIC S9(11)V9(7).
                     15 ISPC0211-VAL-IMP-FILLER-T    PIC X(42).

                  10 ISPC0211-VAL-PERC-GEN-T         REDEFINES
                     ISPC0211-VAL-GENERICO-T.
                     15 ISPC0211-VALORE-PERC-T       PIC  9(5)V9(9).
                     15 ISPC0211-VAL-PERC-FILLER-T   PIC X(46).

                  10 ISPC0211-VAL-STR-GEN-T          REDEFINES
                     ISPC0211-VAL-GENERICO-T.
                     15 ISPC0211-VALORE-STR-T        PIC X(60).


            05 ISPC0211-TAB-SCHEDA-R-T REDEFINES ISPC0211-TAB-VAL-T.
              09 FILLER                                PIC X(5526).
              09 ISPC0211-RESTO-TAB-SCHEDA-T           PIC X(4138974).

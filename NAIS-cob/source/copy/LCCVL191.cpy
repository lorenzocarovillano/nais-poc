      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA QUOTZ_FND_UNIT
      *   ALIAS L19
      *   ULTIMO AGG. 24 APR 2015
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-DATI.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-FND PIC X(12).
             07 (SF)-DT-QTZ   PIC S9(8) COMP-3.
             07 (SF)-VAL-QUO PIC S9(7)V9(5) COMP-3.
             07 (SF)-VAL-QUO-NULL REDEFINES
                (SF)-VAL-QUO   PIC X(7).
             07 (SF)-VAL-QUO-MANFEE PIC S9(7)V9(5) COMP-3.
             07 (SF)-VAL-QUO-MANFEE-NULL REDEFINES
                (SF)-VAL-QUO-MANFEE   PIC X(7).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-TP-FND PIC X(1).
             07 (SF)-DT-RILEVAZIONE-NAV   PIC S9(8) COMP-3.
             07 (SF)-DT-RILEVAZIONE-NAV-NULL REDEFINES
                (SF)-DT-RILEVAZIONE-NAV   PIC X(5).
             07 (SF)-VAL-QUO-ACQ PIC S9(7)V9(5) COMP-3.
             07 (SF)-VAL-QUO-ACQ-NULL REDEFINES
                (SF)-VAL-QUO-ACQ   PIC X(7).
             07 (SF)-FL-NO-NAV PIC X(1).
             07 (SF)-FL-NO-NAV-NULL REDEFINES
                (SF)-FL-NO-NAV   PIC X(1).

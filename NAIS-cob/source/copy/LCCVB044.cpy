
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVB044
      *   ULTIMO AGG. 14 MAR 2011
      *------------------------------------------------------------

       INIZIA-TOT-B04.

           PERFORM INIZIA-ZEROES-B04 THRU INIZIA-ZEROES-B04-EX

           PERFORM INIZIA-SPACES-B04 THRU INIZIA-SPACES-B04-EX

           PERFORM INIZIA-NULL-B04 THRU INIZIA-NULL-B04-EX.

       INIZIA-TOT-B04-EX.
           EXIT.

       INIZIA-NULL-B04.
           MOVE HIGH-VALUES TO (SF)-ID-RICH-ESTRAZ-AGG-NULL
           MOVE HIGH-VALUES TO (SF)-VAL-IMP-NULL
           MOVE HIGH-VALUES TO (SF)-VAL-PC-NULL
           MOVE HIGH-VALUES TO (SF)-VAL-STRINGA-NULL
           MOVE HIGH-VALUES TO (SF)-AREA-D-VALOR-VAR.
       INIZIA-NULL-B04-EX.
           EXIT.

       INIZIA-ZEROES-B04.
           MOVE 0 TO (SF)-ID-BILA-VAR-CALC-P
           MOVE 0 TO (SF)-COD-COMP-ANIA
           MOVE 0 TO (SF)-ID-RICH-ESTRAZ-MAS
           MOVE 0 TO (SF)-DT-RIS
           MOVE 0 TO (SF)-ID-POLI
           MOVE 0 TO (SF)-ID-ADES
           MOVE 0 TO (SF)-PROG-SCHEDA-VALOR
           MOVE 0 TO (SF)-DT-INI-VLDT-PROD
           MOVE 0 TO (SF)-DS-VER
           MOVE 0 TO (SF)-DS-TS-CPTZ.
       INIZIA-ZEROES-B04-EX.
           EXIT.

       INIZIA-SPACES-B04.
           MOVE SPACES TO (SF)-TP-RGM-FISC
           MOVE SPACES TO (SF)-COD-VAR
           MOVE SPACES TO (SF)-TP-D
           MOVE SPACES TO (SF)-DS-OPER-SQL
           MOVE SPACES TO (SF)-DS-UTENTE
           MOVE SPACES TO (SF)-DS-STATO-ELAB.
       INIZIA-SPACES-B04-EX.
           EXIT.

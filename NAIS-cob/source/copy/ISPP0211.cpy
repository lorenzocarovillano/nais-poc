      *----------------------------------------------------------------*
      *    VALORIZZAZIONE SCHEDE ISPC0211
      *----------------------------------------------------------------*
       VAL-SCHEDE-ISPC0211.

      *--> VALORIZZAZIONE OCCURS AREA INPUT SERVIZIO CALCOLI E CONTROLLI
      *--> CON L'OUTPUT DEL VALORIZZATORE VARIABILI

           MOVE WSKD-DEE
             TO ISPC0211-DEE

           PERFORM AREA-SCHEDA-P-ISPC0211
              THRU AREA-SCHEDA-P-ISPC0211-EX
           VARYING IX-AREA-SCHEDA-P FROM 1 BY 1
             UNTIL IX-AREA-SCHEDA-P > WSKD-ELE-LIVELLO-MAX-P

           MOVE WSKD-ELE-LIVELLO-MAX-P   TO ISPC0211-ELE-MAX-SCHEDA-P

           PERFORM AREA-SCHEDA-T-ISPC0211
              THRU AREA-SCHEDA-T-ISPC0211-EX
           VARYING IX-AREA-SCHEDA-T FROM 1 BY 1
             UNTIL IX-AREA-SCHEDA-T > WSKD-ELE-LIVELLO-MAX-T

           MOVE WSKD-ELE-LIVELLO-MAX-T   TO ISPC0211-ELE-MAX-SCHEDA-T.

       VAL-SCHEDE-ISPC0211-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZAZIONE AREA SCHEDA P
      *----------------------------------------------------------------*
       AREA-SCHEDA-P-ISPC0211.

           MOVE WSKD-TP-LIVELLO-P(IX-AREA-SCHEDA-P)
             TO ISPC0211-TIPO-LIVELLO-P(IX-AREA-SCHEDA-P)
           MOVE WSKD-COD-LIVELLO-P(IX-AREA-SCHEDA-P)
             TO ISPC0211-CODICE-LIVELLO-P(IX-AREA-SCHEDA-P)
           MOVE WSKD-ID-LIVELLO-P(IX-AREA-SCHEDA-P)
             TO ISPC0211-IDENT-LIVELLO-P(IX-AREA-SCHEDA-P)

           MOVE WSKD-DT-INIZ-PROD-P(IX-AREA-SCHEDA-P)
             TO ISPC0211-DT-INIZ-PROD(IX-AREA-SCHEDA-P)

           MOVE WSKD-COD-TIPO-OPZIONE-P(IX-AREA-SCHEDA-P)
             TO ISPC0211-CODICE-OPZIONE-P(IX-AREA-SCHEDA-P)

           IF WSKD-COD-RGM-FISC-P(IX-AREA-SCHEDA-P) NOT EQUAL SPACES
                                                    AND LOW-VALUE
                                                    AND HIGH-VALUE
      *
             MOVE WSKD-COD-RGM-FISC-P(IX-AREA-SCHEDA-P)
               TO ISPC0211-COD-RGM-FISC(IX-AREA-SCHEDA-P)
      *
           END-IF
      *
      *--> VALORIZZAZIONE DELL'AREA VARIABILI DI INPUT DEL SERVIZIO
      *--> CALCOLI E CONTROLLI
      *
           MOVE ZERO
             TO ISPC0211-NUM-COMPON-MAX-ELE-P(IX-AREA-SCHEDA-P)

           PERFORM AREA-VAR-P-ISPC0211
              THRU AREA-VAR-P-ISPC0211-EX
           VARYING IX-TAB-VAR-P FROM 1 BY 1
             UNTIL IX-TAB-VAR-P >
                   WSKD-ELE-VARIABILI-MAX-P(IX-AREA-SCHEDA-P)
                OR IX-TAB-VAR-P > WK-ISPC0211-NUM-COMPON-MAX-P

      *
      *--> SEGNALO CHE LE VARIABILI FORNITEMI SONO IN NUMERO
      *--> MAGGIORE DEL NUMERO CONSENTITO DALL'AREA ISPC0211
      *
           IF WSKD-ELE-VARIABILI-MAX-P(IX-AREA-SCHEDA-P) >
                             WK-ISPC0211-NUM-COMPON-MAX-P
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S12100-AREA-SCHEDA-P'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005247'
                    TO IEAI9901-COD-ERRORE
                  STRING 'NUMERO VARIABILI DI PRODOTTO '
                         'SUPERA LIMITE PREVISTO'
                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
           END-IF.

       AREA-SCHEDA-P-ISPC0211-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE DELL'AREA VARIABILI DI PRODOTTO
      *    DI INPUT DEL SERVIZIO CALCOLI E CONTROLLI
      *----------------------------------------------------------------*
       AREA-VAR-P-ISPC0211.

           ADD 1 TO ISPC0211-NUM-COMPON-MAX-ELE-P(IX-AREA-SCHEDA-P)

           MOVE WSKD-COD-VARIABILE-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
             TO ISPC0211-CODICE-VARIABILE-P
               (IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
           MOVE WSKD-TP-DATO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
             TO ISPC0211-TIPO-DATO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
           MOVE WSKD-VAL-GENERICO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
             TO ISPC0211-VAL-GENERICO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P).

       AREA-VAR-P-ISPC0211-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZAZIONE AREA SCHEDA T
      *----------------------------------------------------------------*
       AREA-SCHEDA-T-ISPC0211.

           MOVE WSKD-TP-LIVELLO-T(IX-AREA-SCHEDA-T)
             TO ISPC0211-TIPO-LIVELLO-T(IX-AREA-SCHEDA-T)
           MOVE WSKD-COD-LIVELLO-T(IX-AREA-SCHEDA-T)
             TO ISPC0211-CODICE-LIVELLO-T(IX-AREA-SCHEDA-T)
           MOVE WSKD-ID-LIVELLO-T(IX-AREA-SCHEDA-T)
             TO ISPC0211-IDENT-LIVELLO-T(IX-AREA-SCHEDA-T)

           MOVE WSKD-COD-TIPO-OPZIONE-T(IX-AREA-SCHEDA-T)
             TO ISPC0211-CODICE-OPZIONE-T(IX-AREA-SCHEDA-T)
      *
           IF WSKD-COD-RGM-FISC-T(IX-AREA-SCHEDA-T) NOT EQUAL SPACES
                                                    AND LOW-VALUE
                                                    AND HIGH-VALUE
      *
             MOVE WSKD-COD-RGM-FISC-T(IX-AREA-SCHEDA-T)
               TO ISPC0211-COD-RGM-FISC-T(IX-AREA-SCHEDA-T)
      *
           END-IF
      *
           MOVE WSKD-DT-INIZ-TARI-T(IX-AREA-SCHEDA-T)
             TO ISPC0211-DT-INIZ-TARI(IX-AREA-SCHEDA-T)
      *
           MOVE WSKD-DT-DECOR-TRCH-T(IX-AREA-SCHEDA-T)
             TO ISPC0211-DT-DECOR-TRCH(IX-AREA-SCHEDA-T)

           MOVE WSKD-TIPO-TRCH(IX-AREA-SCHEDA-T)
             TO ISPC0211-TIPO-TRCH(IX-AREA-SCHEDA-T)

           MOVE WSKD-FLG-ITN(IX-AREA-SCHEDA-T)
             TO ISPC0211-FLG-ITN(IX-AREA-SCHEDA-T)

      *--> VALORIZZAZIONE DELL'AREA VARIABILI DI INPUT DEL SERVIZIO
      *--> CALCOLI E CONTROLLI
      *
           MOVE ZERO
             TO ISPC0211-NUM-COMPON-MAX-ELE-T(IX-AREA-SCHEDA-T)

           PERFORM AREA-VAR-T-ISPC0211
              THRU AREA-VAR-T-ISPC0211-EX
           VARYING IX-TAB-VAR-T FROM 1 BY 1
             UNTIL IX-TAB-VAR-T >
                   WSKD-ELE-VARIABILI-MAX-T(IX-AREA-SCHEDA-T)
                OR IX-TAB-VAR-T > WK-ISPC0211-NUM-COMPON-MAX-T
      *
      *--> SEGNALO CHE LE VARIABILI FORNITEMI SONO IN NUMERO
      *--> MAGGIORE DEL NUMERO CONSENTITO DALL'AREA ISPC0211
      *
           IF WSKD-ELE-VARIABILI-MAX-T(IX-AREA-SCHEDA-T) >
                             WK-ISPC0211-NUM-COMPON-MAX-T
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'S12110-AREA-SCHEDA-T'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005247'
                    TO IEAI9901-COD-ERRORE
                  STRING 'NUMERO VARIABILI DI TARIFFA '
                         'SUPERA LIMITE PREVISTO'
                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
           END-IF.

       AREA-SCHEDA-T-ISPC0211-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE DELL'AREA VARIABILI DI TARIFFA
      *    DI INPUT DEL SERVIZIO CALCOLI E CONTROLLI
      *----------------------------------------------------------------*
       AREA-VAR-T-ISPC0211.

           ADD 1 TO ISPC0211-NUM-COMPON-MAX-ELE-T(IX-AREA-SCHEDA-T)

           MOVE WSKD-COD-VARIABILE-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
             TO ISPC0211-CODICE-VARIABILE-T
               (IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
           MOVE WSKD-TP-DATO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
             TO ISPC0211-TIPO-DATO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
           MOVE WSKD-VAL-GENERICO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
             TO ISPC0211-VAL-GENERICO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T).

       AREA-VAR-T-ISPC0211-EX.
           EXIT.

      *-----------------------------------------------------------------
      *   PROCESSO DI VENDITA - PORTAFOGLIO VITA
      *   AREA INPUT/OUTPUT
      *   SERVIZIO CALCOLO RATE DI POLIZZA
      *   LUNG.
      *-----------------------------------------------------------------
       03 LCCC0062-AREA-INPUT.
          05 LCCC0062-DT-COMPETENZA            PIC  9(08)  COMP-3.
          05 LCCC0062-DT-DECORRENZA            PIC  9(08)  COMP-3.
          05 LCCC0062-DT-ULT-QTZO              PIC  9(08)  COMP-3.
          05 LCCC0062-DT-ULTGZ-TRCH            PIC  9(08)  COMP-3.
          05 LCCC0062-FRAZIONAMENTO            PIC  9(05).
       03 LCCC0062-AREA-OUTPUT.
          05 LCCC0062-TOT-NUM-RATE             PIC S9(14) COMP-3.
          05 LCCC0062-TAB-RATE OCCURS 12.
             07 LCCC0062-RATA.
                09 LCCC0062-DT-INF                PIC 9(08)  COMP-3.
                09 LCCC0062-DT-SUP                PIC 9(08)  COMP-3.


           EXEC SQL DECLARE BTC_PARALLELISM TABLE
           (
             COD_COMP_ANIA       INTEGER NOT NULL,
             PROTOCOL            CHAR(50) NOT NULL,
             PROG_PROTOCOL       INTEGER NOT NULL,
             COD_BATCH_STATE     CHAR(1) NOT NULL,
             DT_INS              TIMESTAMP NOT NULL,
             USER_INS            CHAR(30) NOT NULL,
             DT_START            TIMESTAMP,
             DT_END              TIMESTAMP,
             USER_START          CHAR(30),
             DESC_PARALLELISM    VARCHAR(250),
             ID_OGG_DA           DECIMAL(9, 0),
             ID_OGG_A            DECIMAL(9, 0),
             TP_OGG              CHAR(2),
             NUM_ROW_SCHEDULE    DECIMAL(9, 0)
          ) END-EXEC.

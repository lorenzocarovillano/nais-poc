       01 IDSV0003.
      * --   CAMPI SETTAGGI MODULI I-O
      *      COPY IDSV0007 REPLACING ==IDSV0003== BY ==IDSV0003==.
      *    inizio COPY IDSV0007.
      * --   CAMPI SETTAGGI MODULI I-O

          10 IDSV0003-MODALITA-ESECUTIVA          PIC X.
             88 IDSV0003-ON-LINE                  VALUE 'O'.
             88 IDSV0003-BATCH                    VALUE 'B'.
             88 IDSV0003-BATCH-INFR               VALUE 'I'.

          10 IDSV0003-CODICE-COMPAGNIA-ANIA       PIC S9(005) COMP-3.
          10 IDSV0003-COD-MAIN-BATCH              PIC X(008).
          10 IDSV0003-TIPO-MOVIMENTO              PIC 9(005).
          10 IDSV0003-SESSIONE                    PIC X(020).
          10 IDSV0003-USER-NAME                   PIC X(020).
          10 IDSV0003-DATA-INIZIO-EFFETTO         PIC S9(008) COMP-3.
          10 IDSV0003-DATA-FINE-EFFETTO           PIC S9(008) COMP-3.
          10 IDSV0003-DATA-COMPETENZA             PIC S9(018) COMP-3.
          10 IDSV0003-DATA-COMP-AGG-STOR          PIC S9(018) COMP-3.

          10 IDSV0003-TRATTAMENTO-STORICITA       PIC X(003).
             88 IDSV0003-TRATT-DEFAULT            VALUE 'DEF'.
             88 IDSV0003-TRATT-SENZA-STOR         VALUE 'NST'.
             88 IDSV0003-TRATT-X-EFFETTO          VALUE 'EFF'.
             88 IDSV0003-TRATT-X-COMPETENZA       VALUE 'CPZ'.
             88 IDSV0003-ANNULLO-MOVIMENTO        VALUE 'ANN'.

          10 IDSV0003-FORMATO-DATA-DB             PIC X(003).
             88 IDSV0003-DB-ISO                   VALUE 'ISO'.
             88 IDSV0003-DB-EUR                   VALUE 'EUR'.

          10 IDSV0003-ID-MOVI-ANNULLATO           PIC 9(009).

          10 IDSV0003-IDENTITA-CHIAMANTE          PIC X(003).
             88 IDSV0003-PTF-NEWLIFE              VALUE 'LPF'.
             88 IDSV0003-REFACTORING              VALUE 'REF'.
             88 IDSV0003-CONVERSIONE              VALUE 'CON'.

          10 IDSV0003-TIPOLOGIA-OPERAZIONE        PIC X(003).
             88 IDSV0003-WHERE-CONDITION-01       VALUE 'WC1'.
             88 IDSV0003-WHERE-CONDITION-02       VALUE 'WC2'.
             88 IDSV0003-WHERE-CONDITION-03       VALUE 'WC3'.
             88 IDSV0003-WHERE-CONDITION-04       VALUE 'WC4'.
             88 IDSV0003-WHERE-CONDITION-05       VALUE 'WC5'.
             88 IDSV0003-WHERE-CONDITION-06       VALUE 'WC6'.
             88 IDSV0003-WHERE-CONDITION-07       VALUE 'WC7'.
             88 IDSV0003-WHERE-CONDITION-08       VALUE 'WC8'.
             88 IDSV0003-WHERE-CONDITION-09       VALUE 'WC9'.
             88 IDSV0003-WHERE-CONDITION-10       VALUE 'WC0'.

          10 IDSV0003-LIVELLO-OPERAZIONE          PIC X(003).
             88 IDSV0003-FIRST-ACTION             VALUE 'FAC'.
             88 IDSV0003-NONE-ACTION              VALUE 'NAC'.
             88 IDSV0003-PRIMARY-KEY              VALUE 'PK '.
             88 IDSV0003-ID                       VALUE 'ID '.
             88 IDSV0003-IB-OGGETTO               VALUE 'IBO'.
             88 IDSV0003-ID-PADRE                 VALUE 'IDP'.
             88 IDSV0003-IB-SECONDARIO            VALUE 'IBS'.
             88 IDSV0003-ID-OGGETTO               VALUE 'IDO'.
             88 IDSV0003-WHERE-CONDITION          VALUE 'WC '.


      * -- CAMPI OPERAZIONE
      *    COPY IDSV0008.
      *    inizio COPY IDSV0008.
           10 IDSV0003-OPERAZIONE               PIC X(015).
               88 IDSV0003-SELECT               VALUE 'SELECT'
                                                      'SE'.
               88 IDSV0003-AGGIORNAMENTO-STORICO
                                                VALUE 'AGG_STORICO'.
               88 IDSV0003-AGG-STORICO-SOLO-INS
                                                VALUE 'AGG_SOLO_INSERT'.
               88 IDSV0003-INSERT               VALUE 'INSERT'
                                                      'IN'.
               88 IDSV0003-UPDATE               VALUE 'UPDATE'
                                                      'UP'.
               88 IDSV0003-DELETE               VALUE 'DELETE'
                                                      'DF'.
               88 IDSV0003-DELETE-LOGICA        VALUE 'DELETE_LOGICA'
                                                      'DL'.
               88 IDSV0003-OPEN-CURSOR          VALUE 'OPEN_CURSOR'
                                                      'OP'.
               88 IDSV0003-CLOSE-CURSOR         VALUE 'CLOSE_CURSOR'
                                                      'CL'.
               88 IDSV0003-FETCH-FIRST          VALUE 'FF'.
               88 IDSV0003-FETCH-NEXT           VALUE 'FETCH'
                                                      'FN'.
               88 IDSV0003-FETCH-FIRST-MULTIPLE VALUE 'FFM'.
               88 IDSV0003-FETCH-NEXT-MULTIPLE  VALUE 'FNM'.
               88 IDSV0003-PRESA-IN-CARICO  VALUE 'PRESA_IN_CARICO'.
      *    fine COPY IDSV0008.

           10 IDSV0003-FLAG-CODA-TS              PIC X(01) VALUE 'N'.
             88 IDSV0003-DELETE-SI               VALUE 'S'.
             88 IDSV0003-DELETE-NO               VALUE 'N'.
      *    fine COPY IDSV0007

           10 IDSV0003-BUFFER-WHERE-COND         PIC X(300).

      * --   CAMPI ERRORI
      *       COPY IDSV0006 REPLACING ==IDSV0003== BY ==IDSV0003==.
      *     inizio COPY IDSV0006.

      * -- RETURN-CODE
      *     COPY IDSV0004.
      *     inizio COPY IDSV0004.
           10 IDSV0003-RETURN-CODE               PIC  X(002).
             88 IDSV0003-SUCCESSFUL-RC           VALUE '00'.
             88 IDSV0003-GENERIC-ERROR           VALUE 'KO'.
             88 IDSV0003-INVALID-LEVEL-OPER      VALUE 'D1'.
             88 IDSV0003-INVALID-OPER            VALUE 'D2'.
             88 IDSV0003-SQL-ERROR               VALUE 'D3'.
             88 IDSV0003-CONCURRENT-UPDATE       VALUE 'D4'.
             88 IDSV0003-FIELD-NOT-VALUED        VALUE 'C1'.
             88 IDSV0003-INVALID-CONVERSION      VALUE 'C2'.
             88 IDSV0003-SEQUENCE-NOT-FOUND      VALUE 'S1'.
             88 IDSV0003-COD-COMP-NOT-VALID      VALUE 'ZA'.
             88 IDSV0003-STR-DATO-NOT-VALID      VALUE 'ZB'.
             88 IDSV0003-BUFFER-DATI-NOT-V       VALUE 'ZC'.
             88 IDSV0003-STR-DATO-DB-NOT-F       VALUE 'ZD'.
             88 IDSV0003-COD-SERV-NOT-V          VALUE 'ZE'.
             88 IDSV0003-EXCESS-COPY-I-O         VALUE 'ZF'.
             88 IDSV0003-TIPO-MOVIM-NOT-V        VALUE 'ZG'.
             88 IDSV0003-OPER-NOT-V              VALUE 'ZH'.
             88 IDSV0003-SH-MEMORY-NOT-V         VALUE 'ZI'.
             88 IDSV0003-QUEUE-MANAGEMENT        VALUE 'ZL'.
             88 IDSV0003-SH-MEMORY-FLOOD         VALUE 'ZM'.
             88 IDSV0003-EXCESS-SERV-NEWLIFE     VALUE 'ZO'.
             88 IDSV0003-EXCESS-SERV-VSAM        VALUE 'ZP'.
             88 IDSV0003-KEY-FIELDS-NOT-F        VALUE 'ZQ'.
             88 IDSV0003-WHERE-FIELDS-NOT-F      VALUE 'ZR'.
             88 IDSV0003-SERV-NOT-F-ON-MSS       VALUE 'ZS'.
             88 IDSV0003-STR-RED-NOT-F-ON-RDS    VALUE 'ZT'.
             88 IDSV0003-INSERT-SH-MEMORY        VALUE 'ZU'.
             88 IDSV0003-CONVERTER-NOT-F         VALUE 'ZV'.
             88 IDSV0003-EXCESS-OF-RECURSION     VALUE 'ZW'.
             88 IDSV0003-VALUE-DEFAULT-NOT-F     VALUE 'ZX'.
             88 IDSV0003-COPY-NOT-F-ON-IOS       VALUE 'ZY'.
             88 IDSV0003-VALUE-NOT-F-ON-VSS      VALUE 'ZZ'.
             88 IDSV0003-CACHE-PIENA             VALUE 'CP'.

      *   fine COPY IDSV0004

          10 IDSV0003-SQLCODE                PIC S9(009).
             88 IDSV0003-SUCCESSFUL-SQL      VALUE ZERO.
             88 IDSV0003-NOT-FOUND           VALUE +100.
             88 IDSV0003-OPER-NF-X-UNDONE    VALUE +883.
             88 IDSV0003-DUPLICATE-KEY       VALUE -803.
             88 IDSV0003-MORE-THAN-ONE-ROW   VALUE -811.
             88 IDSV0003-SAVEPOINT-NOT-FOUND VALUE -880.
             88 IDSV0003-DEADLOCK-TIMEOUT    VALUE -911
                                                   -913.
             88 IDSV0003-CONNECTION-ERROR    VALUE -924.
             88 IDSV0003-NEGATIVI            VALUE -999999999 THROUGH
                                                   -000000001.
             88 IDSV0003-POSITIVI            VALUE +000000001 THROUGH
                                                   +999999999.

          10 IDSV0003-CAMPI-ESITO.
             15 IDSV0003-DESCRIZ-ERR-DB2     PIC  X(300).
             15 IDSV0003-COD-SERVIZIO-BE     PIC  X(008).
             15 IDSV0003-NOME-TABELLA        PIC  X(018).
             15 IDSV0003-KEY-TABELLA         PIC  X(020).
             15 IDSV0003-NUM-RIGHE-LETTE     PIC  9(002).
      *   fine COPY IDSV0006

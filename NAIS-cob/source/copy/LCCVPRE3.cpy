
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVPRE3
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-PRE.
           MOVE PRE-ID-PREST
             TO (SF)-ID-PTF(IX-TAB-PRE)
           MOVE PRE-ID-PREST
             TO (SF)-ID-PREST(IX-TAB-PRE)
           MOVE PRE-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-PRE)
           MOVE PRE-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-PRE)
           MOVE PRE-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-PRE)
           IF PRE-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE PRE-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-PRE)
           END-IF
           MOVE PRE-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-PRE)
           MOVE PRE-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-PRE)
           MOVE PRE-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-PRE)
           IF PRE-DT-CONCS-PREST-NULL = HIGH-VALUES
              MOVE PRE-DT-CONCS-PREST-NULL
                TO (SF)-DT-CONCS-PREST-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-DT-CONCS-PREST
                TO (SF)-DT-CONCS-PREST(IX-TAB-PRE)
           END-IF
           IF PRE-DT-DECOR-PREST-NULL = HIGH-VALUES
              MOVE PRE-DT-DECOR-PREST-NULL
                TO (SF)-DT-DECOR-PREST-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-DT-DECOR-PREST
                TO (SF)-DT-DECOR-PREST(IX-TAB-PRE)
           END-IF
           IF PRE-IMP-PREST-NULL = HIGH-VALUES
              MOVE PRE-IMP-PREST-NULL
                TO (SF)-IMP-PREST-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-IMP-PREST
                TO (SF)-IMP-PREST(IX-TAB-PRE)
           END-IF
           IF PRE-INTR-PREST-NULL = HIGH-VALUES
              MOVE PRE-INTR-PREST-NULL
                TO (SF)-INTR-PREST-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-INTR-PREST
                TO (SF)-INTR-PREST(IX-TAB-PRE)
           END-IF
           IF PRE-TP-PREST-NULL = HIGH-VALUES
              MOVE PRE-TP-PREST-NULL
                TO (SF)-TP-PREST-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-TP-PREST
                TO (SF)-TP-PREST(IX-TAB-PRE)
           END-IF
           IF PRE-FRAZ-PAG-INTR-NULL = HIGH-VALUES
              MOVE PRE-FRAZ-PAG-INTR-NULL
                TO (SF)-FRAZ-PAG-INTR-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-FRAZ-PAG-INTR
                TO (SF)-FRAZ-PAG-INTR(IX-TAB-PRE)
           END-IF
           IF PRE-DT-RIMB-NULL = HIGH-VALUES
              MOVE PRE-DT-RIMB-NULL
                TO (SF)-DT-RIMB-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-DT-RIMB
                TO (SF)-DT-RIMB(IX-TAB-PRE)
           END-IF
           IF PRE-IMP-RIMB-NULL = HIGH-VALUES
              MOVE PRE-IMP-RIMB-NULL
                TO (SF)-IMP-RIMB-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-IMP-RIMB
                TO (SF)-IMP-RIMB(IX-TAB-PRE)
           END-IF
           IF PRE-COD-DVS-NULL = HIGH-VALUES
              MOVE PRE-COD-DVS-NULL
                TO (SF)-COD-DVS-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-COD-DVS
                TO (SF)-COD-DVS(IX-TAB-PRE)
           END-IF
           MOVE PRE-DT-RICH-PREST
             TO (SF)-DT-RICH-PREST(IX-TAB-PRE)
           IF PRE-MOD-INTR-PREST-NULL = HIGH-VALUES
              MOVE PRE-MOD-INTR-PREST-NULL
                TO (SF)-MOD-INTR-PREST-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-MOD-INTR-PREST
                TO (SF)-MOD-INTR-PREST(IX-TAB-PRE)
           END-IF
           IF PRE-SPE-PREST-NULL = HIGH-VALUES
              MOVE PRE-SPE-PREST-NULL
                TO (SF)-SPE-PREST-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-SPE-PREST
                TO (SF)-SPE-PREST(IX-TAB-PRE)
           END-IF
           IF PRE-IMP-PREST-LIQTO-NULL = HIGH-VALUES
              MOVE PRE-IMP-PREST-LIQTO-NULL
                TO (SF)-IMP-PREST-LIQTO-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-IMP-PREST-LIQTO
                TO (SF)-IMP-PREST-LIQTO(IX-TAB-PRE)
           END-IF
           IF PRE-SDO-INTR-NULL = HIGH-VALUES
              MOVE PRE-SDO-INTR-NULL
                TO (SF)-SDO-INTR-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-SDO-INTR
                TO (SF)-SDO-INTR(IX-TAB-PRE)
           END-IF
           IF PRE-RIMB-EFF-NULL = HIGH-VALUES
              MOVE PRE-RIMB-EFF-NULL
                TO (SF)-RIMB-EFF-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-RIMB-EFF
                TO (SF)-RIMB-EFF(IX-TAB-PRE)
           END-IF
           IF PRE-PREST-RES-EFF-NULL = HIGH-VALUES
              MOVE PRE-PREST-RES-EFF-NULL
                TO (SF)-PREST-RES-EFF-NULL(IX-TAB-PRE)
           ELSE
              MOVE PRE-PREST-RES-EFF
                TO (SF)-PREST-RES-EFF(IX-TAB-PRE)
           END-IF
           MOVE PRE-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-PRE)
           MOVE PRE-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-PRE)
           MOVE PRE-DS-VER
             TO (SF)-DS-VER(IX-TAB-PRE)
           MOVE PRE-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-PRE)
           MOVE PRE-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-PRE)
           MOVE PRE-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-PRE)
           MOVE PRE-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-PRE).
       VALORIZZA-OUTPUT-PRE-EX.
           EXIT.

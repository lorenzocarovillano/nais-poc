      *----------------------------------------------------------------*
      *    COPY      ..... LCCVTGA6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO TRANCHE DI GARANZIA
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-TRCH-DI-GAR.

      *--  TABELLA STORICA
           INITIALIZE TRCH-DI-GAR.

      *--> NOME TABELLA FISICA DB
           MOVE 'TRCH-DI-GAR'               TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO" O "CONVERSAZIONE"
           IF  NOT WTGA-ST-INV(IX-TAB-TGA)
           AND NOT WTGA-ST-CON(IX-TAB-TGA)
           AND WTGA-ELE-TRAN-MAX NOT = 0

              MOVE WMOV-ID-PTF              TO TGA-ID-MOVI-CRZ

              EVALUATE TRUE
      *-->    INSERT
                WHEN WTGA-ST-ADD(IX-TAB-TGA)

      *-->         RICERCA DELL'ID-GAR NELLA TABELLA PADRE(GARANZIA)
                   MOVE WTGA-ID-GAR(IX-TAB-TGA)  TO WS-ID-GARANZIA
                   PERFORM RICERCA-ID-GAR
                      THRU RICERCA-ID-GAR-EX

      *-->         ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                   PERFORM ESTR-SEQUENCE
                      THRU ESTR-SEQUENCE-EX

                   IF IDSV0001-ESITO-OK
                      MOVE S090-SEQ-TABELLA   TO WTGA-ID-PTF(IX-TAB-TGA)
                                                 TGA-ID-TRCH-DI-GAR
                      MOVE WPOL-ID-PTF        TO TGA-ID-POLI
                      MOVE WADE-ID-PTF        TO TGA-ID-ADES
                      MOVE WS-ID-PTF-GAR      TO TGA-ID-GAR

      *-->            TIPO OPERAZIONE DISPATCHER
                      SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

                   END-IF

      *-->    DELETE
                WHEN WTGA-ST-DEL(IX-TAB-TGA)
                   MOVE WTGA-ID-PTF(IX-TAB-TGA)
                                         TO TGA-ID-TRCH-DI-GAR
                   MOVE WPOL-ID-PTF      TO TGA-ID-POLI
                   MOVE WADE-ID-PTF      TO TGA-ID-ADES
                   MOVE WTGA-ID-GAR(IX-TAB-TGA)
                     TO TGA-ID-GAR

      *-->         TIPO OPERAZIONE DISPATCHER
                   SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->    UPDATE
                WHEN WTGA-ST-MOD(IX-TAB-TGA)
                   MOVE WTGA-ID-PTF(IX-TAB-TGA)
                                         TO TGA-ID-TRCH-DI-GAR
                   MOVE WPOL-ID-PTF      TO TGA-ID-POLI
                   MOVE WADE-ID-PTF      TO TGA-ID-ADES
                   MOVE WTGA-ID-GAR(IX-TAB-TGA)
                     TO TGA-ID-GAR

      *-->         TIPO OPERAZIONE DISPATCHER
                   SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN TRANCHE DI GARANZIA
                 PERFORM VAL-DCLGEN-TGA
                    THRU VAL-DCLGEN-TGA-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-TGA
                    THRU VALORIZZA-AREA-DSH-TGA-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-TRCH-DI-GAR-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RICERCA DELL'ID-GAR DELLA COPY LCCVTGA1 NELLA TAB. GARANZIA
      *----------------------------------------------------------------*
       RICERCA-ID-GAR.

           SET NON-TROVATO        TO TRUE.

           PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
             UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
                OR TROVATO

                IF WS-ID-GARANZIA = WGRZ-ID-GAR(IX-TAB-GRZ)
                   MOVE WGRZ-ID-PTF(IX-TAB-GRZ)
                                  TO WS-ID-PTF-GAR
                   SET  TROVATO   TO TRUE
                END-IF
           END-PERFORM.

       RICERCA-ID-GAR-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-TGA.

      *--> DCLGEN TABELLA
           MOVE TRCH-DI-GAR               TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID               TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT  TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-TGA-EX.
           EXIT.

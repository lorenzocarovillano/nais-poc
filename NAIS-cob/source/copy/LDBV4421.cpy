       01  LDBV4421.
           05 LDBV4421-ID-OGG                 PIC S9(9)     COMP-3.
           05 LDBV4421-TP-OGG                 PIC X(002).
           05 LDBV4421-TP-MOVI PIC S9(5)V     COMP-3.
           05 LDBV4421-TP-MOVI-NULL REDEFINES
              LDBV4421-TP-MOVI   PIC X(3).
           05 LDBV4421-DT-INI-DB              PIC X(010).
           05 LDBV4421-DT-INI                 PIC 9(008).
           05 LDBV4421-TOT-MOVI               PIC S9(05) COMP-3.

      ******************************************************************
      *    TP_STAT_BUS (Stato Oggetto di Business)
      ******************************************************************
       01  WS-TP-STAT-BUS                   PIC X(002) VALUE SPACES.
           88 IN-TRATTATIVA                            VALUE 'TR'.
           88 PAGATA                                   VALUE 'PA'.
           88 IN-VIGORE                                VALUE 'VI'.
           88 LIQUIDARE                                VALUE 'DL'.
           88 STORNATO                                 VALUE 'ST'.
           88 INIZIALE                                 VALUE 'IN'.
           88 ATTESA-ATTIVAZIONE                       VALUE 'AA'.
           88 COMPLETATA                               VALUE 'LI'.
           88 PAGAMENTO-BLOCCATO                       VALUE 'PB'.
           88 PARZIALMENTE-PAGATA                      VALUE 'PP'.
           88 CONTABILIZZATO                           VALUE 'CB'.
           88 TP-STAT-DA-AUTORIZZARE                   VALUE 'DA'.
           88 TP-STAT-SOSPESO                          VALUE 'SO'.

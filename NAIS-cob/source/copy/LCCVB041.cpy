      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA BILA_VAR_CALC_P
      *   ALIAS B04
      *   ULTIMO AGG. 14 MAR 2011
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-BILA-VAR-CALC-P PIC S9(9)     COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-ID-RICH-ESTRAZ-MAS PIC S9(9)     COMP-3.
             07 (SF)-ID-RICH-ESTRAZ-AGG PIC S9(9)     COMP-3.
             07 (SF)-ID-RICH-ESTRAZ-AGG-NULL REDEFINES
                (SF)-ID-RICH-ESTRAZ-AGG   PIC X(5).
             07 (SF)-DT-RIS   PIC S9(8) COMP-3.
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-ID-ADES PIC S9(9)     COMP-3.
             07 (SF)-PROG-SCHEDA-VALOR PIC S9(9)     COMP-3.
             07 (SF)-TP-RGM-FISC PIC X(2).
             07 (SF)-DT-INI-VLDT-PROD   PIC S9(8) COMP-3.
             07 (SF)-COD-VAR PIC X(30).
             07 (SF)-TP-D PIC X(1).
             07 (SF)-VAL-IMP PIC S9(11)V9(7) COMP-3.
             07 (SF)-VAL-IMP-NULL REDEFINES
                (SF)-VAL-IMP   PIC X(10).
             07 (SF)-VAL-PC PIC S9(5)V9(9) COMP-3.
             07 (SF)-VAL-PC-NULL REDEFINES
                (SF)-VAL-PC   PIC X(8).
             07 (SF)-VAL-STRINGA PIC X(60).
             07 (SF)-VAL-STRINGA-NULL REDEFINES
                (SF)-VAL-STRINGA   PIC X(60).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-AREA-D-VALOR-VAR-VCHAR.
                49 (SF)-AREA-D-VALOR-VAR-LEN PIC S9(4) COMP-5.
                49 (SF)-AREA-D-VALOR-VAR     PIC X(4000).

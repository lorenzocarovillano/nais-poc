      ******************************************************************
      *    TP_CAUS (Tipo Causale)
      ******************************************************************
       01  WS-TP-CAUS                       PIC X(002) VALUE SPACES.
           88 RIDOTTA                                  VALUE 'RI'.
           88 ATTESA-COMUNIC-PAG-REN                   VALUE 'AE'.
           88 RIATTIVATA                               VALUE 'RA'.
           88 RISCATTO-TOTALE-LIMITATO                 VALUE 'RL'.
           88 RISCATTO-TOTALE-LIMITATO-RIII            VALUE 'LM'.
           88 ALTRE-CAUSE                              VALUE 'AC'.
           88 SINISTRO-PRIMO-ASSICURATO                VALUE 'SP'.
           88 SINISTRO-TERMINE-FISSO                   VALUE 'ST'.
           88 PAG-TRASF-POSIZ-PREV                     VALUE 'PV'.
           88 ATTESA-LIQ-RENDITA                       VALUE 'AR'.
           88 DIFFERITA                                VALUE 'DF'.
           88 PROROGATA                                VALUE 'PR'.
           88 CAUS-ATTESA-ATTIVAZIONE                  VALUE 'AA'.
           88 SENZA-SEGUITO                            VALUE 'SS'.
           88 ATTESA-PERFEZIONAMENTO                   VALUE 'AP'.
           88 REVOCATO                                 VALUE 'RE'.
           88 REVOCATO-NOPERF                          VALUE 'RV'.
           88 INSOLVENZA                               VALUE 'IN'.
           88 MANCATO-PERFEZIONAMENTO                  VALUE 'MP'.
           88 MANCATO-RINNOVO                          VALUE 'MR'.
           88 STORNATO-TRASFORMAZIONE                  VALUE 'TR'.
           88 RECESSO                                  VALUE 'RO'.
           88 RECESSO-NOPERF                           VALUE 'RC'.
           88 SINISTRO                                 VALUE 'SI'.
           88 LIQUID-RECESSO                           VALUE 'LR'.
           88 LIQUID-RIMBORSO-PREMI                    VALUE 'LP'.
           88 LIQUID-RISCATTO-TOTALE                   VALUE 'LT'.
           88 LIQUID-RISCATTO-TOTALE-ORD               VALUE 'LD'.
           88 LIQUID-RISCATTO-TOTALE-SPE               VALUE 'LC'.
           88 LIQUID-RISCATTO-TOTALE-VOL               VALUE 'LN'.
           88 LIQUID-RISCATTO-TOTALE-DIF               VALUE 'LF'.
           88 LIQUID-RISCATTO-TOTALE-ANT               VALUE 'LU'.
           88 LIQUID-ANTICIPAZIONE                     VALUE 'LG'.
           88 LIQUID-RISCATTO-PARZIALE                 VALUE 'LL'.
           88 LIQUID-RISCATTO-PARZIALE-DIF             VALUE 'LK'.
           88 LIQUID-RISCATTO-UFFICIO                  VALUE 'RU'.
           88 LIQUID-SCADENZA-ANTICIPATA               VALUE 'LA'.
           88 LIQUID-SCADENZA                          VALUE 'LZ'.
           88 LIQUID-SCADENZA-ECC-CONTR                VALUE 'LH'.
           88 LIQUID-SCADENZA-ECC-RIII                 VALUE 'LE'.
           88 LIQUID-SINISTRO                          VALUE 'LS'.
           88 LIQUID-SINISTRO-MORTE                    VALUE 'LX'.
           88 LIQUID-REVOCA                            VALUE 'LV'.
           88 LIQUID-TRASF-POSIZ-PREV                  VALUE 'LO'.
           88 LIQUID-TRASF-POSIZ-PREV-RIS              VALUE 'LI'.
           88 RENDITA-EROGAZIONE                       VALUE 'RZ'.
           88 RIMBORSO-PREMI                           VALUE 'RP'.
           88 RISCATTO-PARZIALE                        VALUE 'RT'.
           88 RISCATTO-TOTALE                          VALUE 'TT'.
           88 RISCATTO-UFFICIO                         VALUE 'RU'.
           88 SCADENZA                                 VALUE 'SC'.
           88 SCADENZA-ANTICIPATA                      VALUE 'SA'.
           88 STABILIZZATA                             VALUE 'SZ'.
           88 TRASF-POSIZ-PREV                         VALUE 'TP'.
           88 PAGAMENTO-RECESSO                        VALUE 'PO'.
           88 PAGAMENTO-REVOCA                         VALUE 'PH'.
           88 PAGAMENT-RISCATTO-PARZIALE               VALUE 'PX'.
           88 PAGAMENT-RISCATTO-TOTALE                 VALUE 'PT'.
           88 PAGAMENT-RISCATTO-UFFICIO                VALUE 'LY'.
           88 PAGAMENTO-SINISTRO                       VALUE 'PS'.
           88 PAGAMENTO-SCAD-ANTICIPATA                VALUE 'PA'.
           88 PAGAMENTO-SCADENZA                       VALUE 'PM'.
           88 PAGAMENTO-RIMBORSO-TRANCHE               VALUE 'PP'.
           88 PERFEZIONATA                             VALUE 'PZ'.
           88 COMUNICAZIONE-ISTRUTTORIA                VALUE 'CI'.
           88 COMUNICAZIONE-SENZA-ISTRUT               VALUE 'SO'.
           88 ISTRUTTORIA-COMPETATA                    VALUE 'IT'.
           88 LIQUIDAZIONE-COMPLETATA                  VALUE 'LQ'.
           88 PAGAMENTO-EFFETTUATO                     VALUE 'PE'.
           88 SINISTRO-RIFIUTATO                       VALUE 'SR'.
           88 PRENOTATO-SCADENZA                       VALUE 'PN'.
           88 IN-DEROGA                                VALUE 'DE'.
           88 PAGAMENTO-IN-ATTESA-CONFERMA             VALUE 'PI'.
           88 PAGAMENTO-PARZIALE                       VALUE 'P1'.
           88 STORNO-CEDOLA                            VALUE 'CE'.
           88 STORNO-SWITCH-FND                        VALUE 'SW'.
           88 CONCLUSIONE-EROG-RENDITA                 VALUE 'CR'.
           88 RENDITA-EROG-REVISIONARIO                VALUE 'RR'.
           88 RENDITA-SOSP-ACCERTAMENTI                VALUE 'RS'.
           88 DORMIENTE-NON-LIQUIDABILE                VALUE 'DL'.
           88 ATTESA-PAG-MEF                           VALUE 'AM'.
           88 CORRETTA-IN-ATTESA-DI-PREMIO             VALUE 'CP'.
           88 CORRETTA-EMETTIBILE                      VALUE 'EM'.
           88 ATTESA-LIQ-REN-PRIMO-ASSIC               VALUE 'AS'.
           88 ATTESA-COM-PAG-REN-PRIMO-ASS             VALUE 'AT'.
           88 SINISTRO-TERM-FISS-PRIMO-ASS             VALUE 'SF'.
           88 ATTESA-LIQ-POL-COL                       VALUE 'AL'.
           88 SINISTRO-SU-RENDITA-IN-EROGAZ            VALUE 'SE'.
           88 SCAD-RISCATTO-TOT-LIMITATO               VALUE 'SL'.
           88 LIQ-SCAD-RISC-TOT-LIMITATO               VALUE 'LB'.
           88 SINISTRO-RISC-TOT-LIMITATO               VALUE 'SG'.
           88 SINISTRO-SU-REVERSIONARIO                VALUE 'SY'.
           88 PRESCRITTO-COMPAGNIA                     VALUE 'PC'.
           88 DA-PAGARE                                VALUE 'DP'.
           88 STORNO-PER-REVISIONE                     VALUE 'SV'.
           88 PAGAMENTO-DISPOSTO                       VALUE 'PD'.
           88 STORNO-PER-DISDETTA                      VALUE 'DS'.
           88 STORNO-PER-EST-ANTICIPATA                VALUE 'EA'.
           88 LIQUID-PER-DISDETTA                      VALUE 'L1'.
           88 LIQUID-PER-EST-ANTICIPATA                VALUE 'L2'.
           88 PAGAMENTO-PER-DISDETTA                   VALUE 'PK'.
           88 PAGAMENTO-PER-EST-ANT                    VALUE 'PL'.
           88 PAGAMENTO-PER-RIMBORSO                   VALUE 'PJ'.
F3DIS      88 STORNO-PER-DISDETTA-COMM                 VALUE 'DC'.
11257      88 STORNO-MANCATO-RINNOVO-TACITO            VALUE 'SM'.
12193      88 SINISTRO-RENDITA-DA-EROGARE              VALUE 'ER'.


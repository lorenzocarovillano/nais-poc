      *----------------------------------------------------------------*
      *   AREA INPUT VARIABILI
      *   PORTAFOGLIO VITA - PROCESSO VENDITA
      *   LUNG.
      *----------------------------------------------------------------*
      * 03 AREA-TAB-VARIABILI.
           05 (SF)-ELE-LIVELLO-MAX               PIC S9(04) COMP-3.
           05 (SF)-TAB-VAR.
            06 (SF)-TAB-LIVELLO            OCCURS 400.
              07 (SF)-COD-COMP-ANIA              PIC  9(05).
              07 (SF)-TP-LIVELLO                 PIC  X(01).
              07 (SF)-COD-LIVELLO                PIC  X(12).
              07 (SF)-ID-LIVELLO                 PIC  9(09).
              07 (SF)-AREA-VARIABILI.
                 10 (SF)-ELE-VARIABILI-MAX       PIC S9(04) COMP-3.
                 10 (SF)-TAB-VARIABILI        OCCURS 70.
                    12 (SF)-COD-VARIABILE        PIC  X(12).
                    12 (SF)-TP-DATO              PIC  X(01).
                    12 (SF)-VAL-IMP              PIC  9(11)V9(07).
                    12 (SF)-VAL-PERC             PIC  9(05)V9(09).
                    12 (SF)-VAL-STR              PIC  X(12).
              07 (SF)-NOME-SERVIZIO              PIC  X(08).
           05  (SF)-TAB-VAR-R REDEFINES (SF)-TAB-VAR.
            06 FILLER                            PIC  X(4028).
            06 (SF)-RESTO-TAB-VAR                PIC  X(1607172).

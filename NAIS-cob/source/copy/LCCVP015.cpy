
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVP015
      *   ULTIMO AGG. 07 DIC 2017
      *------------------------------------------------------------

       VAL-DCLGEN-P01.
           MOVE (SF)-ID-RICH-EST
              TO P01-ID-RICH-EST
           IF (SF)-ID-RICH-EST-COLLG-NULL = HIGH-VALUES
              MOVE (SF)-ID-RICH-EST-COLLG-NULL
              TO P01-ID-RICH-EST-COLLG-NULL
           ELSE
              MOVE (SF)-ID-RICH-EST-COLLG
              TO P01-ID-RICH-EST-COLLG
           END-IF
           IF (SF)-ID-LIQ-NULL = HIGH-VALUES
              MOVE (SF)-ID-LIQ-NULL
              TO P01-ID-LIQ-NULL
           ELSE
              MOVE (SF)-ID-LIQ
              TO P01-ID-LIQ
           END-IF
           MOVE (SF)-COD-COMP-ANIA
              TO P01-COD-COMP-ANIA
           IF (SF)-IB-RICH-EST-NULL = HIGH-VALUES
              MOVE (SF)-IB-RICH-EST-NULL
              TO P01-IB-RICH-EST-NULL
           ELSE
              MOVE (SF)-IB-RICH-EST
              TO P01-IB-RICH-EST
           END-IF
           MOVE (SF)-TP-MOVI
              TO P01-TP-MOVI
           MOVE (SF)-DT-FORM-RICH
              TO P01-DT-FORM-RICH
           MOVE (SF)-DT-INVIO-RICH
              TO P01-DT-INVIO-RICH
           MOVE (SF)-DT-PERV-RICH
              TO P01-DT-PERV-RICH
           MOVE (SF)-DT-RGSTRZ-RICH
              TO P01-DT-RGSTRZ-RICH
           IF (SF)-DT-EFF-NULL = HIGH-VALUES
              MOVE (SF)-DT-EFF-NULL
              TO P01-DT-EFF-NULL
           ELSE
             IF (SF)-DT-EFF = ZERO
                MOVE HIGH-VALUES
                TO P01-DT-EFF-NULL
             ELSE
              MOVE (SF)-DT-EFF
              TO P01-DT-EFF
             END-IF
           END-IF
           IF (SF)-DT-SIN-NULL = HIGH-VALUES
              MOVE (SF)-DT-SIN-NULL
              TO P01-DT-SIN-NULL
           ELSE
             IF (SF)-DT-SIN = ZERO
                MOVE HIGH-VALUES
                TO P01-DT-SIN-NULL
             ELSE
              MOVE (SF)-DT-SIN
              TO P01-DT-SIN
             END-IF
           END-IF
           MOVE (SF)-TP-OGG
              TO P01-TP-OGG
           MOVE (SF)-ID-OGG
              TO P01-ID-OGG
           MOVE (SF)-IB-OGG
              TO P01-IB-OGG
           MOVE (SF)-FL-MOD-EXEC
              TO P01-FL-MOD-EXEC
           IF (SF)-ID-RICHIEDENTE-NULL = HIGH-VALUES
              MOVE (SF)-ID-RICHIEDENTE-NULL
              TO P01-ID-RICHIEDENTE-NULL
           ELSE
              MOVE (SF)-ID-RICHIEDENTE
              TO P01-ID-RICHIEDENTE
           END-IF
           MOVE (SF)-COD-PROD
              TO P01-COD-PROD
           MOVE (SF)-DS-OPER-SQL
              TO P01-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO P01-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO P01-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO P01-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO P01-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO P01-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO P01-DS-STATO-ELAB
           MOVE (SF)-COD-CAN
              TO P01-COD-CAN
           IF (SF)-IB-OGG-ORIG-NULL = HIGH-VALUES
              MOVE (SF)-IB-OGG-ORIG-NULL
              TO P01-IB-OGG-ORIG-NULL
           ELSE
              MOVE (SF)-IB-OGG-ORIG
              TO P01-IB-OGG-ORIG
           END-IF
           IF (SF)-DT-EST-FINANZ-NULL = HIGH-VALUES
              MOVE (SF)-DT-EST-FINANZ-NULL
              TO P01-DT-EST-FINANZ-NULL
           ELSE
             IF (SF)-DT-EST-FINANZ = ZERO
                MOVE HIGH-VALUES
                TO P01-DT-EST-FINANZ-NULL
             ELSE
              MOVE (SF)-DT-EST-FINANZ
              TO P01-DT-EST-FINANZ
             END-IF
           END-IF
           IF (SF)-DT-MAN-COP-NULL = HIGH-VALUES
              MOVE (SF)-DT-MAN-COP-NULL
              TO P01-DT-MAN-COP-NULL
           ELSE
             IF (SF)-DT-MAN-COP = ZERO
                MOVE HIGH-VALUES
                TO P01-DT-MAN-COP-NULL
             ELSE
              MOVE (SF)-DT-MAN-COP
              TO P01-DT-MAN-COP
             END-IF
           END-IF
           IF (SF)-FL-GEST-PROTEZIONE-NULL = HIGH-VALUES
              MOVE (SF)-FL-GEST-PROTEZIONE-NULL
              TO P01-FL-GEST-PROTEZIONE-NULL
           ELSE
              MOVE (SF)-FL-GEST-PROTEZIONE
              TO P01-FL-GEST-PROTEZIONE
           END-IF.
       VAL-DCLGEN-P01-EX.
           EXIT.

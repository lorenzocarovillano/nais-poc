
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVL413
      *   ULTIMO AGG. 22 APR 2010
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-L41.
           MOVE L41-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-L41)
           MOVE L41-TP-QTZ
             TO (SF)-TP-QTZ(IX-TAB-L41)
           MOVE L41-COD-FND
             TO (SF)-COD-FND(IX-TAB-L41)
           MOVE L41-DT-QTZ
             TO (SF)-DT-QTZ(IX-TAB-L41)
           MOVE L41-TP-FND
             TO (SF)-TP-FND(IX-TAB-L41)
           MOVE L41-VAL-QUO
             TO (SF)-VAL-QUO(IX-TAB-L41)
           MOVE L41-DESC-AGG
             TO (SF)-DESC-AGG(IX-TAB-L41)
           MOVE L41-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-L41)
           MOVE L41-DS-VER
             TO (SF)-DS-VER(IX-TAB-L41)
           MOVE L41-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ(IX-TAB-L41)
           MOVE L41-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-L41)
           MOVE L41-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-L41).
       VALORIZZA-OUTPUT-L41-EX.
           EXIT.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA POLI
      *   ALIAS POL
      *   ULTIMO AGG. 07 DIC 2017
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-IB-OGG PIC X(40).
             07 (SF)-IB-OGG-NULL REDEFINES
                (SF)-IB-OGG   PIC X(40).
             07 (SF)-IB-PROP PIC X(40).
             07 (SF)-DT-PROP   PIC S9(8) COMP-3.
             07 (SF)-DT-PROP-NULL REDEFINES
                (SF)-DT-PROP   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-DT-DECOR   PIC S9(8) COMP-3.
             07 (SF)-DT-EMIS   PIC S9(8) COMP-3.
             07 (SF)-TP-POLI PIC X(2).
             07 (SF)-DUR-AA PIC S9(5)     COMP-3.
             07 (SF)-DUR-AA-NULL REDEFINES
                (SF)-DUR-AA   PIC X(3).
             07 (SF)-DUR-MM PIC S9(5)     COMP-3.
             07 (SF)-DUR-MM-NULL REDEFINES
                (SF)-DUR-MM   PIC X(3).
             07 (SF)-DT-SCAD   PIC S9(8) COMP-3.
             07 (SF)-DT-SCAD-NULL REDEFINES
                (SF)-DT-SCAD   PIC X(5).
             07 (SF)-COD-PROD PIC X(12).
             07 (SF)-DT-INI-VLDT-PROD   PIC S9(8) COMP-3.
             07 (SF)-COD-CONV PIC X(12).
             07 (SF)-COD-CONV-NULL REDEFINES
                (SF)-COD-CONV   PIC X(12).
             07 (SF)-COD-RAMO PIC X(12).
             07 (SF)-COD-RAMO-NULL REDEFINES
                (SF)-COD-RAMO   PIC X(12).
             07 (SF)-DT-INI-VLDT-CONV   PIC S9(8) COMP-3.
             07 (SF)-DT-INI-VLDT-CONV-NULL REDEFINES
                (SF)-DT-INI-VLDT-CONV   PIC X(5).
             07 (SF)-DT-APPLZ-CONV   PIC S9(8) COMP-3.
             07 (SF)-DT-APPLZ-CONV-NULL REDEFINES
                (SF)-DT-APPLZ-CONV   PIC X(5).
             07 (SF)-TP-FRM-ASSVA PIC X(2).
             07 (SF)-TP-RGM-FISC PIC X(2).
             07 (SF)-TP-RGM-FISC-NULL REDEFINES
                (SF)-TP-RGM-FISC   PIC X(2).
             07 (SF)-FL-ESTAS PIC X(1).
             07 (SF)-FL-ESTAS-NULL REDEFINES
                (SF)-FL-ESTAS   PIC X(1).
             07 (SF)-FL-RSH-COMUN PIC X(1).
             07 (SF)-FL-RSH-COMUN-NULL REDEFINES
                (SF)-FL-RSH-COMUN   PIC X(1).
             07 (SF)-FL-RSH-COMUN-COND PIC X(1).
             07 (SF)-FL-RSH-COMUN-COND-NULL REDEFINES
                (SF)-FL-RSH-COMUN-COND   PIC X(1).
             07 (SF)-TP-LIV-GENZ-TIT PIC X(2).
             07 (SF)-FL-COP-FINANZ PIC X(1).
             07 (SF)-FL-COP-FINANZ-NULL REDEFINES
                (SF)-FL-COP-FINANZ   PIC X(1).
             07 (SF)-TP-APPLZ-DIR PIC X(2).
             07 (SF)-TP-APPLZ-DIR-NULL REDEFINES
                (SF)-TP-APPLZ-DIR   PIC X(2).
             07 (SF)-SPE-MED PIC S9(12)V9(3) COMP-3.
             07 (SF)-SPE-MED-NULL REDEFINES
                (SF)-SPE-MED   PIC X(8).
             07 (SF)-DIR-EMIS PIC S9(12)V9(3) COMP-3.
             07 (SF)-DIR-EMIS-NULL REDEFINES
                (SF)-DIR-EMIS   PIC X(8).
             07 (SF)-DIR-1O-VERS PIC S9(12)V9(3) COMP-3.
             07 (SF)-DIR-1O-VERS-NULL REDEFINES
                (SF)-DIR-1O-VERS   PIC X(8).
             07 (SF)-DIR-VERS-AGG PIC S9(12)V9(3) COMP-3.
             07 (SF)-DIR-VERS-AGG-NULL REDEFINES
                (SF)-DIR-VERS-AGG   PIC X(8).
             07 (SF)-COD-DVS PIC X(20).
             07 (SF)-COD-DVS-NULL REDEFINES
                (SF)-COD-DVS   PIC X(20).
             07 (SF)-FL-FNT-AZ PIC X(1).
             07 (SF)-FL-FNT-AZ-NULL REDEFINES
                (SF)-FL-FNT-AZ   PIC X(1).
             07 (SF)-FL-FNT-ADER PIC X(1).
             07 (SF)-FL-FNT-ADER-NULL REDEFINES
                (SF)-FL-FNT-ADER   PIC X(1).
             07 (SF)-FL-FNT-TFR PIC X(1).
             07 (SF)-FL-FNT-TFR-NULL REDEFINES
                (SF)-FL-FNT-TFR   PIC X(1).
             07 (SF)-FL-FNT-VOLO PIC X(1).
             07 (SF)-FL-FNT-VOLO-NULL REDEFINES
                (SF)-FL-FNT-VOLO   PIC X(1).
             07 (SF)-TP-OPZ-A-SCAD PIC X(2).
             07 (SF)-TP-OPZ-A-SCAD-NULL REDEFINES
                (SF)-TP-OPZ-A-SCAD   PIC X(2).
             07 (SF)-AA-DIFF-PROR-DFLT PIC S9(5)     COMP-3.
             07 (SF)-AA-DIFF-PROR-DFLT-NULL REDEFINES
                (SF)-AA-DIFF-PROR-DFLT   PIC X(3).
             07 (SF)-FL-VER-PROD PIC X(2).
             07 (SF)-FL-VER-PROD-NULL REDEFINES
                (SF)-FL-VER-PROD   PIC X(2).
             07 (SF)-DUR-GG PIC S9(5)     COMP-3.
             07 (SF)-DUR-GG-NULL REDEFINES
                (SF)-DUR-GG   PIC X(3).
             07 (SF)-DIR-QUIET PIC S9(12)V9(3) COMP-3.
             07 (SF)-DIR-QUIET-NULL REDEFINES
                (SF)-DIR-QUIET   PIC X(8).
             07 (SF)-TP-PTF-ESTNO PIC X(2).
             07 (SF)-TP-PTF-ESTNO-NULL REDEFINES
                (SF)-TP-PTF-ESTNO   PIC X(2).
             07 (SF)-FL-CUM-PRE-CNTR PIC X(1).
             07 (SF)-FL-CUM-PRE-CNTR-NULL REDEFINES
                (SF)-FL-CUM-PRE-CNTR   PIC X(1).
             07 (SF)-FL-AMMB-MOVI PIC X(1).
             07 (SF)-FL-AMMB-MOVI-NULL REDEFINES
                (SF)-FL-AMMB-MOVI   PIC X(1).
             07 (SF)-CONV-GECO PIC X(5).
             07 (SF)-CONV-GECO-NULL REDEFINES
                (SF)-CONV-GECO   PIC X(5).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-FL-SCUDO-FISC PIC X(1).
             07 (SF)-FL-SCUDO-FISC-NULL REDEFINES
                (SF)-FL-SCUDO-FISC   PIC X(1).
             07 (SF)-FL-TRASFE PIC X(1).
             07 (SF)-FL-TRASFE-NULL REDEFINES
                (SF)-FL-TRASFE   PIC X(1).
             07 (SF)-FL-TFR-STRC PIC X(1).
             07 (SF)-FL-TFR-STRC-NULL REDEFINES
                (SF)-FL-TFR-STRC   PIC X(1).
             07 (SF)-DT-PRESC   PIC S9(8) COMP-3.
             07 (SF)-DT-PRESC-NULL REDEFINES
                (SF)-DT-PRESC   PIC X(5).
             07 (SF)-COD-CONV-AGG PIC X(12).
             07 (SF)-COD-CONV-AGG-NULL REDEFINES
                (SF)-COD-CONV-AGG   PIC X(12).
             07 (SF)-SUBCAT-PROD PIC X(12).
             07 (SF)-SUBCAT-PROD-NULL REDEFINES
                (SF)-SUBCAT-PROD   PIC X(12).
             07 (SF)-FL-QUEST-ADEGZ-ASS PIC X(1).
             07 (SF)-FL-QUEST-ADEGZ-ASS-NULL REDEFINES
                (SF)-FL-QUEST-ADEGZ-ASS   PIC X(1).
             07 (SF)-COD-TPA PIC X(4).
             07 (SF)-COD-TPA-NULL REDEFINES
                (SF)-COD-TPA   PIC X(4).
             07 (SF)-ID-ACC-COMM PIC S9(9)     COMP-3.
             07 (SF)-ID-ACC-COMM-NULL REDEFINES
                (SF)-ID-ACC-COMM   PIC X(5).
             07 (SF)-FL-POLI-CPI-PR PIC X(1).
             07 (SF)-FL-POLI-CPI-PR-NULL REDEFINES
                (SF)-FL-POLI-CPI-PR   PIC X(1).
             07 (SF)-FL-POLI-BUNDLING PIC X(1).
             07 (SF)-FL-POLI-BUNDLING-NULL REDEFINES
                (SF)-FL-POLI-BUNDLING   PIC X(1).
             07 (SF)-IND-POLI-PRIN-COLL PIC X(1).
             07 (SF)-IND-POLI-PRIN-COLL-NULL REDEFINES
                (SF)-IND-POLI-PRIN-COLL   PIC X(1).
             07 (SF)-FL-VND-BUNDLE PIC X(1).
             07 (SF)-FL-VND-BUNDLE-NULL REDEFINES
                (SF)-FL-VND-BUNDLE   PIC X(1).
             07 (SF)-IB-BS PIC X(40).
             07 (SF)-IB-BS-NULL REDEFINES
                (SF)-IB-BS   PIC X(40).
             07 (SF)-FL-POLI-IFP PIC X(1).
             07 (SF)-FL-POLI-IFP-NULL REDEFINES
                (SF)-FL-POLI-IFP   PIC X(1).

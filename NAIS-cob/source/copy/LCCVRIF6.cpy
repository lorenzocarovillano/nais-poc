      *----------------------------------------------------------------*
      *    COPY      ..... LCCVRIF6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO RICHIESTA INVESTIMENTO FONDO
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-RICH-INVST-FND.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE RICH-INVST-FND

      *--> NOME TABELLA FISICA DB
           MOVE 'RICH-INVST-FND'                  TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WRIF-ST-INV(IX-TAB-RIF)
              AND WRIF-ELE-RIC-INV-MAX NOT = 0

      *--->   Impostare ID Tabella RICHIESTA INVESTIMENTO FONDO
              MOVE WMOV-ID-PTF              TO RIF-ID-MOVI-CRZ

      *--->   Impostare ID Tabella Titolo Contabile
              MOVE WRIF-ID-MOVI-FINRIO(IX-TAB-RIF)
                TO WS-ID-MOVI-FINRIO

      *--->   Ricerca dell'ID-TIT-CONT sulla Tabella
      *--->   Padre Titolo Contabile
              PERFORM RICERCA-MOVI-FINRIO-RIF
                 THRU RICERCA-MOVI-FINRIO-RIF-EX

              MOVE WS-ID-PTF
                TO RIF-ID-MOVI-FINRIO

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WRIF-ST-ADD(IX-TAB-RIF)

                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA
                            TO WRIF-ID-PTF(IX-TAB-RIF)
                               RIF-ID-RICH-INVST-FND
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WRIF-ST-DEL(IX-TAB-RIF)

                       MOVE WRIF-ID-PTF(IX-TAB-RIF)
                         TO RIF-ID-RICH-INVST-FND
                       MOVE WRIF-ID-MOVI-FINRIO(IX-TAB-RIF)
                         TO RIF-ID-MOVI-FINRIO

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->        MODIFICA
                  WHEN WRIF-ST-MOD(IX-TAB-RIF)

                       MOVE WRIF-ID-PTF(IX-TAB-RIF)
                         TO RIF-ID-RICH-INVST-FND
                       MOVE WRIF-ID-MOVI-FINRIO(IX-TAB-RIF)
                         TO RIF-ID-MOVI-FINRIO

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN RICHIESTA INVESTIMENTO FONDO
                 PERFORM VAL-DCLGEN-RIF
                    THRU VAL-DCLGEN-RIF-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-RIF
                    THRU VALORIZZA-AREA-DSH-RIF-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-RICH-INVST-FND-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-RIF.

      *--> DCLGEN TABELLA
           MOVE RICH-INVST-FND          TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-RIF-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RICERCA PTF
      *----------------------------------------------------------------*
       RICERCA-MOVI-FINRIO-RIF.

           SET NON-TROVATO TO TRUE.

           PERFORM VARYING IX-MFZ FROM 1 BY 1
                     UNTIL IX-MFZ > WMFZ-ELE-MOVI-FINRIO-MAX
                        OR TROVATO

                IF WMFZ-ID-MOVI-FINRIO(IX-MFZ) = WS-ID-MOVI-FINRIO

                   MOVE WMFZ-ID-PTF(IX-MFZ) TO WS-ID-PTF

                   SET TROVATO TO TRUE

                END-IF

           END-PERFORM.

       RICERCA-MOVI-FINRIO-RIF-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVRIF5.

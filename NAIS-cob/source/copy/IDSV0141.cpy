      * MODULI DI CONVERSIONE GENERICI
             05 IDSV0141-TIPO-DATO-MITT               PIC X(002).
                88 IDSV0141-MITT-ALFANUM              VALUE 'AL'.
                88 IDSV0141-MITT-COMP                 VALUE 'C0'.
                88 IDSV0141-MITT-COMP-1               VALUE 'C1'.
                88 IDSV0141-MITT-COMP-3               VALUE 'C3'.
                88 IDSV0141-MITT-COMP-4               VALUE 'C4'.
                88 IDSV0141-MITT-NUM-SEGN             VALUE 'NS'.
                88 IDSV0141-MITT-NUM-NON-SEGN         VALUE 'NN'.
      *
             05 IDSV0141-LUNGHEZZA-DATO-STR           PIC S9(05) COMP-3.
             05 IDSV0141-LUNGHEZZA-DATO-MITT          PIC S9(05) COMP-3.
             05 IDSV0141-PRECISIONE-DATO-MITT         PIC S9(02) COMP-3.
             05 IDSV0141-CAMPO-MITT                   PIC X(040).
      *
             05 IDSV0141-CAMPO-DEST                   PIC S9(11)V9(07).
      *-- return code
             05 IDSV0141-RETURN-CODE                   PIC  X(002).
                88 IDSV0141-SUCCESSFUL-RC               VALUE 'OK'.
                88 IDSV0141-UNSUCCESSFUL-RC             VALUE 'KO'.
      *--campi esito
             05 IDSV0141-CAMPI-ESITO.
               15 IDSV0141-DESCRIZ-ERR-DB2             PIC  X(200).
               15 IDSV0141-COD-SERVIZIO-BE             PIC  X(008).

      *----------------------------------------------------------------*
      *    COPY      ..... LCCVBEP6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO BENEFICIARIO
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVBEP5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN ADESIONE (LCCVBEP1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-BNFIC.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE BNFIC

      *--> NOME TABELLA FISICA DB
           MOVE 'BNFIC'                     TO   WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO" O "CONVERSAZIONE"
           IF  NOT WBEP-ST-INV(IX-TAB-BEP)
           AND NOT WBEP-ST-CON(IX-TAB-BEP)
           AND     WBEP-ELE-BENEFICIARI-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WBEP-ST-ADD(IX-TAB-BEP)

      *-->           RICERCA DELL'ID-RAN NELLA TABELLA PADRE(RAPP-ANA)
                     MOVE WBEP-ID-RAPP-ANA(IX-TAB-BEP)
                       TO WS-ID-RAN
                     PERFORM RIC-RAPP-ANAG
                        THRU RIC-RAPP-ANAG-EX

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK
                        MOVE S090-SEQ-TABELLA TO WBEP-ID-PTF(IX-TAB-BEP)
                                                 BEP-ID-BNFIC

                        MOVE WS-ID-PTF-RAN
                          TO BEP-ID-RAPP-ANA
                        MOVE WMOV-ID-PTF
                          TO BEP-ID-MOVI-CRZ

                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WBEP-ST-MOD(IX-TAB-BEP)

                       MOVE WBEP-ID-PTF(IX-TAB-BEP)
                         TO BEP-ID-BNFIC
                       MOVE WBEP-ID-RAPP-ANA(IX-TAB-BEP)
                         TO BEP-ID-RAPP-ANA
                       MOVE WMOV-ID-PTF
                         TO BEP-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WBEP-ST-DEL(IX-TAB-BEP)

                       MOVE WBEP-ID-PTF(IX-TAB-BEP)
                         TO BEP-ID-BNFIC
                       MOVE WBEP-ID-RAPP-ANA(IX-TAB-BEP)
                         TO BEP-ID-RAPP-ANA
                       MOVE WMOV-ID-PTF
                         TO BEP-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       TIPO BENEFICIARIO CENSITO
                 IF (WBEP-ST-ADD(IX-TAB-BEP) OR WBEP-ST-MOD(IX-TAB-BEP))
                     AND WBEP-TP-IND-BNFICR(IX-TAB-BEP) = 'CE'
                    MOVE WBEP-ID-BNFICR(IX-TAB-BEP)
                      TO WS-ID-RAN
                    PERFORM RIC-RAPP-ANAG
                       THRU RIC-RAPP-ANAG-EX
                    MOVE WS-ID-PTF-RAN
                      TO BEP-ID-BNFICR
                 END-IF

      *-->       VALORIZZA DCLGEN ADESIONE
                 PERFORM VAL-DCLGEN-BEP
                    THRU VAL-DCLGEN-BEP-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-BEP
                    THRU VALORIZZA-AREA-DSH-BEP-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-BNFIC-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-BEP.

      *--> DCLGEN TABELLA
           MOVE BNFIC                   TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-BEP-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    RICERCA RAPPORTO ANAGRAFICO
      *----------------------------------------------------------------*
       RIC-RAPP-ANAG.

           SET NON-TROVATO TO TRUE.

           PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
                     UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX
                        OR TROVATO

                IF WS-ID-RAN = WRAN-ID-RAPP-ANA(IX-TAB-RAN)
                   MOVE WRAN-ID-PTF(IX-TAB-RAN)
                     TO WS-ID-PTF-RAN
                   SET  TROVATO TO TRUE
                END-IF

           END-PERFORM.

       RIC-RAPP-ANAG-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVL114
      *   ULTIMO AGG. 02 SET 2008
      *------------------------------------------------------------

       INIZIA-TOT-L11.

           PERFORM INIZIA-ZEROES-L11 THRU INIZIA-ZEROES-L11-EX

           PERFORM INIZIA-SPACES-L11 THRU INIZIA-SPACES-L11-EX

           PERFORM INIZIA-NULL-L11 THRU INIZIA-NULL-L11-EX.

       INIZIA-TOT-L11-EX.
           EXIT.

       INIZIA-NULL-L11.
           MOVE HIGH-VALUES TO (SF)-ID-OGG-1RIO-NULL
           MOVE HIGH-VALUES TO (SF)-TP-OGG-1RIO-NULL
           MOVE HIGH-VALUES TO (SF)-ID-RICH-NULL.
       INIZIA-NULL-L11-EX.
           EXIT.

       INIZIA-ZEROES-L11.
           MOVE 0 TO (SF)-ID-OGG-BLOCCO
           MOVE 0 TO (SF)-COD-COMP-ANIA
           MOVE 0 TO (SF)-TP-MOVI
           MOVE 0 TO (SF)-ID-OGG
           MOVE 0 TO (SF)-DT-EFF
           MOVE 0 TO (SF)-DS-VER
           MOVE 0 TO (SF)-DS-TS-CPTZ.
       INIZIA-ZEROES-L11-EX.
           EXIT.

       INIZIA-SPACES-L11.
           MOVE SPACES TO (SF)-TP-OGG
           MOVE SPACES TO (SF)-TP-STAT-BLOCCO
           MOVE SPACES TO (SF)-COD-BLOCCO
           MOVE SPACES TO (SF)-DS-OPER-SQL
           MOVE SPACES TO (SF)-DS-UTENTE
           MOVE SPACES TO (SF)-DS-STATO-ELAB.
       INIZIA-SPACES-L11-EX.
           EXIT.

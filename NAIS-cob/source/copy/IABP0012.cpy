      *----------------------------------------------------------------*
      * ELABORAZIONE                                                   *
      *----------------------------------------------------------------*
       B500-ELABORA-MICRO.

           MOVE 'B500-ELABORA-MICRO'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF IABI0011-RIP-CON-MONITORING
              PERFORM M600-UPD-STARTING            THRU M600-EX
           END-IF

           IF WK-ERRORE-NO
              PERFORM N500-ESTRAI-JOB              THRU N500-EX

              IF WK-ERRORE-NO
                 PERFORM E601-VERIFICA-LETTURA     THRU E601-EX

                 IF WK-ERRORE-NO
                    IF IABI0011-RIP-CON-MONITORING
                       PERFORM M601-UPD-RIESEGUIRE THRU M601-EX
                    END-IF
                 END-IF

              END-IF
           END-IF.
      *
       B500-EX.
           EXIT.
      *----------------------------------------------------------------*
      * UPDATE JOB ESEGUITO OK
      *----------------------------------------------------------------*
       B700-UPD-ESEGUITO-OK.

           MOVE 'B700-UPD-ESEGUITO-OK'      TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           SET IDSV0003-PRIMARY-KEY           TO TRUE
           SET WK-NOT-FOUND-NOT-ALLOWED       TO TRUE

           IF IABI0011-RIP-CON-VERSIONAMENTO AND
              (IABV0006-GUIDE-AD-HOC          OR
               IABV0006-BY-BOOKING)

              IF IABI0011-RIP-CON-VERSIONAMENTO
                 PERFORM V000-VALORIZZAZIONE-VERSIONE
                                              THRU V000-EX
              END-IF

              MOVE JOB-DA-ESEGUIRE            TO IABV0002-STATE-CURRENT

           ELSE
              MOVE JOB-ESEGUITO-OK            TO IABV0002-STATE-CURRENT
           END-IF

           PERFORM A150-SET-CURRENT-TIMESTAMP THRU A150-EX
           MOVE WS-TIMESTAMP-N                TO   BJS-DT-END

           PERFORM U000-UPD-JOB               THRU U000-EX

           IF WK-ERRORE-NO
              PERFORM E550-UPD-JOB-EXECUTION  THRU E550-EX
           END-IF.
      *
       B700-EX.
           EXIT.

      *----------------------------------------------------------------*
      * UPDATE JOB ESEGUITO KO
      *----------------------------------------------------------------*
       B800-UPD-ESEGUITO-KO.

           MOVE 'B800-UPD-ESEGUITO-KO'      TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           SET IDSV0003-PRIMARY-KEY            TO TRUE
           SET WK-NOT-FOUND-NOT-ALLOWED        TO TRUE

           MOVE JOB-ESEGUITO-KO                TO IABV0002-STATE-CURRENT
           PERFORM A150-SET-CURRENT-TIMESTAMP  THRU A150-EX
           MOVE WS-TIMESTAMP-N                 TO BJS-DT-END

           IF IABI0011-RIP-CON-VERSIONAMENTO
              MOVE ZEROES                      TO IABV0009-VERSIONING
           END-IF

           PERFORM U000-UPD-JOB                THRU U000-EX

           IF WK-ERRORE-NO
              PERFORM E550-UPD-JOB-EXECUTION   THRU E550-EX
           END-IF.
      *
       B800-EX.
           EXIT.

      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       B900-CTRL-STATI-SOSP.

           MOVE 'B900-CTRL-STATI-SOSP'      TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF NOT SEQUENTIAL-GUIDE

              PERFORM VARYING IND-STATI-SOSP FROM 1 BY 1
                  UNTIL IND-STATI-SOSP > WK-LIMITE-STATI-SOSP     OR
                        WK-STATI-SOSP-PK(IND-STATI-SOSP) = ZEROES OR
                        NOT WK-ERRORE-NO

                  IF IABV0006-GUIDE-AD-HOC OR
                     IABV0006-BY-BOOKING

                     PERFORM L802-CARICA-STATI-SOSPESI     THRU L802-EX
                  ELSE
                     MOVE WK-STATI-SOSP-PK(IND-STATI-SOSP) TO BJS-ID-JOB
                  END-IF

                  IF NOT WK-SIMULAZIONE-INFR       AND
                     NOT WK-SIMULAZIONE-APPL       AND
                     NOT IABI0011-SENZA-RIPARTENZA

                     IF WK-ALLINEA-STATI-OK
                        PERFORM B700-UPD-ESEGUITO-OK     THRU B700-EX
                     ELSE
                        PERFORM B800-UPD-ESEGUITO-KO     THRU B800-EX
                     END-IF

                  END-IF

              END-PERFORM

              IF WK-ERRORE-NO
                 MOVE 1             TO IND-STATI-SOSP
                 MOVE WK-CURRENT-PK TO WK-STATI-SOSP-PK(IND-STATI-SOSP)

                 PERFORM L802-CARICA-STATI-SOSPESI THRU L802-EX

                 INITIALIZE WK-STATI-SOSPESI-STR
                                  IND-STATI-SOSP
              END-IF

           END-IF.
      *
       B900-EX.
           EXIT.
      *----------------------------------------------------------------*
      * UPDATE JOB IN "STARTING"
      *----------------------------------------------------------------*
       M600-UPD-STARTING.

           MOVE 'M600-UPD-STARTING'         TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *
           MOVE JOB-STARTING                TO IABV0002-STATE-CURRENT

           PERFORM M650-UPD-CON-MONITORING  THRU M650-EX.
      *
       M600-EX.
           EXIT.
      *----------------------------------------------------------------*
      * UPDATE JOB IN "DA RIESEGUIRE" DEI JOB IN "STARTING"
      *----------------------------------------------------------------*
       M601-UPD-RIESEGUIRE.

           MOVE 'M601-UPD-RIESEGUIRE'       TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           INITIALIZE IABV0002-STATE

           SET WK-UPDATE-FOR-RESTORE-YES    TO TRUE
           MOVE JOB-DA-RIESEGUIRE           TO IABV0002-STATE-CURRENT
           MOVE JOB-STARTING                TO IABV0002-STATE-01.

           PERFORM M650-UPD-CON-MONITORING  THRU M650-EX.
      *
       M601-EX.
           EXIT.
      *----------------------------------------------------------------*
      * UPDATE DELLO STATE CON GESTIONE "MONITORING"
      *----------------------------------------------------------------*
       M650-UPD-CON-MONITORING.

           MOVE 'M650-UPD-CON-MONITORING'   TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           SET IDSV0003-WHERE-CONDITION     TO TRUE
           SET WK-NOT-FOUND-ALLOWED         TO TRUE

           PERFORM U000-UPD-JOB             THRU U000-EX.

           IF WK-ERRORE-NO
              IF NOT WK-SIMULAZIONE-INFR
                 PERFORM Y000-COMMIT        THRU Y000-EX
              END-IF
           END-IF.
      *
       M650-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESTRAI JOB
      *----------------------------------------------------------------*
       N500-ESTRAI-JOB.

           MOVE 'N500-ESTRAI-JOB'           TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST           TO TRUE.
           SET WK-ESTRAI-JOB-YES              TO TRUE.
           SET WK-SKIP-BATCH-NO               TO TRUE.

           MOVE IABI0011-TIPO-ORDER-BY        TO IABV0002-TIPO-ORDER-BY.

           IF IABI0011-RIP-CON-MONITORING
              INITIALIZE IABV0002-STATE
              MOVE JOB-STARTING               TO IABV0002-STATE-01
           END-IF

           PERFORM UNTIL WK-ESTRAI-JOB-NO  OR
                         WK-ERRORE-YES     OR
                         WK-SKIP-BATCH-YES


      *--> INIZIALIZZA CODICE DI RITORNO
               SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
               SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE

               IF IABV0006-GUIDE-AD-HOC OR
                  IABV0006-BY-BOOKING

                  IF IABI0011-RIP-CON-VERSIONAMENTO
                     PERFORM V000-VALORIZZAZIONE-VERSIONE
                                       THRU V000-EX
                  END-IF

                  PERFORM N501-PRE-GUIDE-AD-HOC  THRU N501-EX

                  IF WK-ERRORE-NO
                     PERFORM N509-SWITCH-GUIDE   THRU N509-EX
                  END-IF

               ELSE
                  MOVE BTC-ID-BATCH           TO BJS-ID-BATCH

                  IF IABI0011-MACROFUNZIONALITA NOT =
                     SPACES AND LOW-VALUE AND HIGH-VALUE
                     MOVE IABI0011-MACROFUNZIONALITA
                                               TO BJS-COD-MACROFUNCT
                     SET VERIFICA-FINALE-JOBS-SI TO TRUE
                  END-IF

                  IF IABI0011-TIPO-MOVIMENTO IS NUMERIC AND
                     IABI0011-TIPO-MOVIMENTO NOT = ZEROES
                     MOVE IABI0011-TIPO-MOVIMENTO TO BJS-TP-MOVI
                     SET VERIFICA-FINALE-JOBS-SI   TO TRUE
                  END-IF

                  MOVE PGM-IABS0060           TO WK-PGM-ERRORE
                  CALL PGM-IABS0060           USING IDSV0003
                                                    IABV0002
                                                    BTC-JOB-SCHEDULE
                  ON EXCEPTION
                     INITIALIZE WK-LOG-ERRORE

                     STRING 'ERRORE CHIAMATA MODULO : '
                             DELIMITED BY SIZE
                             PGM-IABS0060
                             DELIMITED BY SIZE
                             ' - '
                              DELIMITED BY SIZE INTO
                             WK-LOR-DESC-ERRORE-ESTESA
                     END-STRING

                     PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                  END-CALL

                  IF WK-ERRORE-NO
                     IF BJS-TP-MOVI-NULL NOT = HIGH-VALUE AND
                        BJS-TP-MOVI      NOT = ZEROES
                        MOVE BJS-TP-MOVI    TO IABV0003-TP-MOVI-BUSINESS
                     ELSE
                        MOVE IDSV0001-TIPO-MOVIMENTO
                                            TO IABV0003-TP-MOVI-BUSINESS
                     END-IF

                     MOVE BJS-IB-OGG         TO IABV0003-IB-OGG
                     MOVE BJS-COD-MACROFUNCT TO IABV0003-COD-MACROFUNCT

                  END-IF

               END-IF

               IF WK-ERRORE-NO
                  PERFORM N503-CNTL-GUIDE        THRU N503-EX
               END-IF

           END-PERFORM.
      *
       N500-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CONTROLLA GUIDE
      *----------------------------------------------------------------*
       N503-CNTL-GUIDE.
      *
           MOVE 'N503-CNTL-GUIDE'           TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF IDSV0003-SUCCESSFUL-RC

              EVALUATE TRUE
              WHEN IDSV0003-SUCCESSFUL-SQL
      *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                 ADD 1                    TO CONT-TAB-GUIDE-LETTE
                 PERFORM Z000-DISP-OCCORR-GUIDE THRU Z000-EX

                 PERFORM E601-VERIFICA-LETTURA    THRU E601-EX

                 IF WK-ERRORE-NO
                    PERFORM E600-ELABORA-BUSINESS THRU E600-EX
                    SET IDSV0003-FETCH-NEXT       TO TRUE
                 END-IF


              WHEN IDSV0003-NOT-FOUND
      *--->   CAMPO $ NON TROVATO

                 IF IDSV0003-FETCH-FIRST
                    INITIALIZE WK-LOG-ERRORE

                    MOVE 'OCCORRENZE GUIDA NON TROVATE'
                                     TO WK-LOR-DESC-ERRORE-ESTESA

                    MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                                       THRU W070-EX
                 ELSE
                    SET WK-FINE-ALIMENTAZIONE-YES   TO TRUE
                    SET WK-ESTRAI-JOB-NO            TO TRUE
                    PERFORM Z000-DISP-OCCORR-GUIDE THRU Z000-EX
                 END-IF

              WHEN IDSV0003-NEGATIVI
      *--->   ERRORE DI ACCESSO AL DB
                 SET WK-ESTRAI-JOB-NO          TO TRUE

                 INITIALIZE WK-LOG-ERRORE

                 MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                 STRING WK-PGM-ERRORE
                        ' - '
                        'SQLCODE : '
                        WK-SQLCODE
                        DELIMITED BY SIZE INTO
                        WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
             SET WK-ESTRAI-JOB-NO TO TRUE

             INITIALIZE WK-LOG-ERRORE

             STRING 'ERRORE MODULO '
                     WK-PGM-ERRORE
                     ' - '
                     'RC : '
                     IDSV0003-RETURN-CODE
                     DELIMITED BY SIZE INTO
                     WK-LOR-DESC-ERRORE-ESTESA
             END-STRING

             MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
             PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
           END-IF.
      *
       N503-EX.
           EXIT.

      *----------------------------------------------------------------*
      * SWITCH GUIDE
      *----------------------------------------------------------------*
       N509-SWITCH-GUIDE.

           MOVE 'N509-SWITCH-GUIDE'         TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           EVALUATE TRUE

              WHEN SEQUENTIAL-GUIDE
                 PERFORM CALL-SEQGUIDE THRU CALL-SEQGUIDE-EX

              WHEN DB-GUIDE
                 PERFORM N502-CALL-GUIDE-AD-HOC THRU N502-EX
                 MOVE WK-PGM-GUIDA              TO WK-PGM-ERRORE

              WHEN OTHER
                 INITIALIZE WK-LOG-ERRORE

                 STRING 'TIPO GUIDA NON VALIDO '
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX

           END-EVALUATE.
      *
       N509-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CALL ATTIVATORE
      *----------------------------------------------------------------*
       Q503-CNTL-SERV-ROTTURA.

           MOVE 'Q503-CNTL-SERV-ROTTURA'    TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF IDSV0001-ESITO-OK
              PERFORM Y400-ESITO-OK-OTHER-SERV  THRU Y400-EX
           ELSE
              PERFORM Y500-ESITO-KO-OTHER-SERV  THRU Y500-EX

              IF WK-ERRORE-NO
                 SET WK-PGM-ARCHITECTURE          TO TRUE

                 IF WK-PGM-SERV-SECON-ROTT NOT =
                    SPACES AND LOW-VALUE AND HIGH-VALUE
                    PERFORM L450-TRATTA-SERV-SECOND-ROTT THRU L450-EX
                 END-IF
              END-IF
           END-IF.
      *
       Q503-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ELABORA SERVIZIO BUSINESS
      *----------------------------------------------------------------*
       E600-ELABORA-BUSINESS.

           MOVE 'E600-ELABORA-BUSINESS'     TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF IABI0011-RIP-CON-MONITORING
              PERFORM E150-UPD-IN-ESECUZIONE      THRU E150-EX
           END-IF

           IF WK-ERRORE-NO
              PERFORM E500-INS-JOB-EXECUTION   THRU E500-EX

              IF WK-ERRORE-NO
                 PERFORM R000-ESTRAI-DATI-ELABORAZIONE  THRU R000-EX

                 IF WK-ERRORE-NO
                    PERFORM L000-LANCIA-BUSINESS        THRU L000-EX
                 ELSE
                    SET WK-BAT-COLLECTION-KEY-KO        TO TRUE
                    SET WK-JOB-COLLECTION-KEY-KO        TO TRUE

                    IF NOT WK-SIMULAZIONE-INFR       AND
                       NOT WK-SIMULAZIONE-APPL       AND
                       NOT IABI0011-SENZA-RIPARTENZA AND
                       NOT SEQUENTIAL-GUIDE

                       PERFORM B800-UPD-ESEGUITO-KO     THRU B800-EX

                    END-IF

                 END-IF
              END-IF
           END-IF.
      *
       E600-EX.
           EXIT.
      *----------------------------------------------------------------*
      * VERIFICA GUIDE
      *----------------------------------------------------------------*
       E601-VERIFICA-LETTURA.

           MOVE 'E601-VERIFICA-LETTURA'     TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF WK-SKIP-BATCH-NO
              IF WK-FINE-ALIMENTAZIONE-NO
                 IF IABV0006-GUIDE-AD-HOC OR
                    IABV0006-BY-BOOKING

                    PERFORM N504-POST-GUIDE-AD-HOC          THRU N504-EX
                 ELSE
                    IF WK-PGM-SERV-ROTTURA NOT =
                       SPACES AND LOW-VALUE AND HIGH-VALUE
                       PERFORM E602-CNTL-ROTTURA-EST        THRU E602-EX
                    END-IF
                 END-IF
              ELSE
                  IF WK-PGM-SERV-ROTTURA NOT =
                     SPACES AND LOW-VALUE AND HIGH-VALUE
                     PERFORM Q001-ELABORA-ROTTURA           THRU Q001-EX
                  ELSE
                     IF GESTIONE-SUSPEND
                        PERFORM Y300-SAVEPOINT              THRU Y300-EX

                        IF WK-ERRORE-NO
                           IF BBT-DEF-CONTENT-TYPE(4:) =
                              ULTIMO-LANCIO-X-CLOSE-FILE
                              PERFORM L000-LANCIA-BUSINESS  THRU L000-EX

                              IF WK-ERRORE-NO
                                 PERFORM Y300-SAVEPOINT     THRU Y300-EX

                                 IF WK-ERRORE-NO
                                    SET IABV0006-ULTIMO-LANCIO TO TRUE
                                    PERFORM L602-CALL-BUS-EOF
                                                            THRU L602-EX
                                 END-IF
                              END-IF

                           ELSE
                              SET IABV0006-ULTIMO-LANCIO    TO TRUE
                              PERFORM L000-LANCIA-BUSINESS  THRU L000-EX
                           END-IF
                        END-IF
                     ELSE
                        SET IABV0006-ULTIMO-LANCIO          TO TRUE
                        IF BBT-DEF-CONTENT-TYPE(4:) =
                           ULTIMO-LANCIO-X-CLOSE-FILE
                           PERFORM L602-CALL-BUS-EOF        THRU L602-EX
                        END-IF
                     END-IF
                  END-IF
              END-IF
           END-IF.
      *
       E601-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESTRAI RECORD
      *----------------------------------------------------------------*
       E620-ESTRAI-RECORD.
      *
           MOVE 'E620-ESTRAI-RECORD'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE.
           SET WK-ESTRAI-REC-YES          TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
           INITIALIZE IABV0003-DATA-JOB.
           MOVE 0                         TO IABV0003-ELE-MAX

           IF IABI0011-APPEND-DATA-GATES-YES
              PERFORM E625-CONFIG-APPEND-GATES THRU E625-EX
           END-IF

           PERFORM UNTIL  WK-ESTRAI-REC-NO OR
                          NOT WK-ERRORE-NO

              MOVE BJS-ID-BATCH           TO BRS-ID-BATCH
              MOVE BJS-ID-JOB             TO BRS-ID-JOB
                                             WK-ID-JOB

              MOVE PGM-IABS0110           TO WK-PGM-ERRORE
              CALL PGM-IABS0110           USING IDSV0003
                                                IABV0002
                                                BTC-REC-SCHEDULE
              ON EXCEPTION

                 INITIALIZE WK-LOG-ERRORE

                 STRING 'ERRORE CHIAMATA MODULO '
                         DELIMITED BY SIZE
                         PGM-IABS0110
                         DELIMITED BY SIZE
                         ' - '
                          DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              END-CALL

              IF WK-ERRORE-NO
                 IF IDSV0003-SUCCESSFUL-RC
                    EVALUATE TRUE
                    WHEN IDSV0003-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE

                       PERFORM E630-VALOR-DA-GATES THRU E630-EX

                       SET IDSV0003-FETCH-NEXT     TO TRUE

                    WHEN IDSV0003-NOT-FOUND
      *--> CAMPO $ NON TROVATO
                       SET WK-ESTRAI-REC-NO            TO TRUE

                       IF IDSV0003-FETCH-FIRST
                          SET WK-BAT-COLLECTION-KEY-KO TO TRUE
                          SET WK-JOB-COLLECTION-KEY-KO TO TRUE

                          INITIALIZE WK-LOG-ERRORE

                          STRING
                              'ID-BATCH : '
                              WK-ID-BATCH ' - '
                              'ID-JOB : '
                              WK-ID-JOB ' - '
                              'REC_SCHEDULE - OCCORR. NON TROVATE'
                              DELIMITED BY SIZE INTO
                              WK-LOR-DESC-ERRORE-ESTESA
                          END-STRING

                          MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                          PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                         THRU W070-EX

                          IF WK-ERRORE-NO
                             PERFORM E800-INS-EXE-MESSAGE THRU E800-EX
                          END-IF
                       END-IF

                      WHEN IDSV0003-NEGATIVI
      *--> ERRORE DI ACCESSO AL DB
                         SET WK-ESTRAI-REC-NO           TO TRUE
                         SET WK-BAT-COLLECTION-KEY-KO   TO TRUE
                         SET WK-JOB-COLLECTION-KEY-KO   TO TRUE

                         INITIALIZE WK-LOG-ERRORE

                         MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                         STRING 'ERRORE MODULO '
                                WK-PGM-ERRORE
                                ' SQLCODE : '
                                WK-SQLCODE
                                DELIMITED BY SIZE INTO
                                WK-LOR-DESC-ERRORE-ESTESA
                         END-STRING

                         MOVE ERRORE-FATALE  TO WK-LOR-ID-GRAVITA-ERRORE
                         PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                           THRU W070-EX

                         IF WK-ERRORE-NO
                            PERFORM E800-INS-EXE-MESSAGE   THRU E800-EX
                         END-IF

                   END-EVALUATE
                 ELSE
      *--> GESTIRE ERRORE DISPATCHER
                    SET WK-ESTRAI-REC-NO           TO TRUE
                    SET WK-BAT-COLLECTION-KEY-KO   TO TRUE
                    SET WK-JOB-COLLECTION-KEY-KO   TO TRUE

                    INITIALIZE WK-LOG-ERRORE

                    STRING 'ERRORE MODULO '
                            PGM-IABS0110
                            ' - '
                            'RC : '
                            IDSV0003-RETURN-CODE
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                        THRU W070-EX
                 END-IF
              END-IF
           END-PERFORM.
      *
       E620-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CONFIGURA CALL APPEND
      *----------------------------------------------------------------*
       E625-CONFIG-APPEND-GATES.

           MOVE 'E625-CONFIG-APPEND-GATES'  TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                    TO IDSV0301-COD-COMP-ANIA
           MOVE IDSV0001-ID-TEMPORARY-DATA
                                    TO IDSV0301-ID-TEMPORARY-DATA
           MOVE 'BTC_REC_SCHEDULE'  TO IDSV0301-ALIAS-STR-DATO
           MOVE IDSV0001-SESSIONE   TO IDSV0301-ID-SESSION

           SET IDSV0301-CONTIGUOUS-NO TO TRUE

           MOVE 0                   TO IDSV0301-TOTAL-RECURRENCE
                                       IDSV0301-PARTIAL-RECURRENCE
                                       IDSV0301-ACTUAL-RECURRENCE

           SET IDSV0301-WRITE                 TO TRUE
           SET IDSV0301-DELETE-ACTION         TO TRUE.

       E625-EX.
           EXIT.

      *----------------------------------------------------------------*
      * VALORIZZA DA GATES
      *----------------------------------------------------------------*
       E630-VALOR-DA-GATES.

           MOVE 'E630-VALOR-DA-GATES'       TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF IABV0003-ELE-MAX < WK-LIMITE-ARRAY-BLOB
              PERFORM E635-VALORIZZA-BLOB THRU E635-EX
           ELSE
              IF IABI0011-APPEND-DATA-GATES-YES
                 MOVE BRS-TYPE-RECORD TO IDSV0301-TYPE-RECORD
                 MOVE BRS-DATA-RECORD-LEN
                                      TO IDSV0301-BUFFER-DATA-LEN
                 SET IDSV0301-ADDRESS TO ADDRESS OF BRS-DATA-RECORD
                 PERFORM CALL-IDSS0300 THRU CALL-IDSS0300-EX
              ELSE
                 INITIALIZE WK-LOG-ERRORE
                 MOVE 'OVERFLOW CANALE DATA GATES'
                                          TO WK-LOR-DESC-ERRORE-ESTESA
                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF

           END-IF.
      *
       E630-EX.
           EXIT.

      *----------------------------------------------------------------*
      * VALORIZZA BLOB
      *----------------------------------------------------------------*
       E635-VALORIZZA-BLOB.

           MOVE 'E635-VALORIZZA-BLOB'       TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           ADD 1                      TO IABV0003-ELE-MAX
           INITIALIZE                    IABV0003-BLOB-DATA-ELE
                                        (IABV0003-ELE-MAX)

           EVALUATE TRUE

              WHEN IABI0011-DATA-1K
                 MOVE BRS-TYPE-RECORD TO IABV0003-BLOB-DATA-TYPE-REC-1K
                                        (IABV0003-ELE-MAX)
                 MOVE BRS-DATA-RECORD TO IABV0003-BLOB-DATA-REC-1K
                                        (IABV0003-ELE-MAX)
                 ADD  LENGTH OF IABV0003-BLOB-DATA-REC-1K
                                      TO WS-LUNG-EFF-BLOB

              WHEN IABI0011-DATA-2K
                 MOVE BRS-TYPE-RECORD TO IABV0003-BLOB-DATA-TYPE-REC-2K
                                        (IABV0003-ELE-MAX)
                 MOVE BRS-DATA-RECORD TO IABV0003-BLOB-DATA-REC-2K
                                        (IABV0003-ELE-MAX)
                 ADD  LENGTH OF IABV0003-BLOB-DATA-REC-2K
                                      TO WS-LUNG-EFF-BLOB

              WHEN IABI0011-DATA-3K
                 MOVE BRS-TYPE-RECORD TO IABV0003-BLOB-DATA-TYPE-REC-3K
                                        (IABV0003-ELE-MAX)
                 MOVE BRS-DATA-RECORD TO IABV0003-BLOB-DATA-REC-3K
                                        (IABV0003-ELE-MAX)
                 ADD  LENGTH OF IABV0003-BLOB-DATA-REC-3K
                                      TO WS-LUNG-EFF-BLOB

              WHEN IABI0011-DATA-10K
                 MOVE BRS-TYPE-RECORD TO IABV0003-BLOB-DATA-TYPE-REC
                                        (IABV0003-ELE-MAX)
                 MOVE BRS-DATA-RECORD TO IABV0003-BLOB-DATA-REC
                                        (IABV0003-ELE-MAX)
                 ADD  LENGTH OF IABV0003-BLOB-DATA-REC
                                      TO WS-LUNG-EFF-BLOB

           END-EVALUATE.
      *
       E635-EX.
           EXIT.

      *----------------------------------------------------------------*
      * ELABORA SERVIZIO BUSINESS
      *----------------------------------------------------------------*
       E150-UPD-IN-ESECUZIONE.

           MOVE 'E150-UPD-IN-ESECUZIONE'    TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           MOVE JOB-IN-ESECUZIONE           TO IABV0002-STATE-CURRENT

           ADD 1                            TO BJS-EXECUTIONS-COUNT

           SET IDSV0003-PRIMARY-KEY         TO TRUE
           SET WK-NOT-FOUND-NOT-ALLOWED     TO TRUE

           MOVE IDSV0001-USER-NAME          TO BJS-USER-START
           PERFORM A150-SET-CURRENT-TIMESTAMP       THRU A150-EX
           MOVE WS-TIMESTAMP-N              TO BJS-DT-START

           MOVE HIGH-VALUE                  TO BJS-DT-END-NULL
           MOVE HIGH-VALUE                  TO BJS-FLAG-WARNINGS

           PERFORM U000-UPD-JOB             THRU U000-EX.
      *
       E150-EX.
           EXIT.

      *----------------------------------------------------------------*
      * INSERT IN JOB EXECUTION
      *----------------------------------------------------------------*
       E500-INS-JOB-EXECUTION.
      *
           MOVE 'E500-INS-JOB-EXECUTION'    TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF IABI0011-FLAG-STOR-ESECUZ-YES AND
              NOT IABV0006-GUIDE-AD-HOC     AND
              NOT IABV0006-BY-BOOKING

      *--> TIPO OPERAZIONE
              SET IDSV0003-INSERT            TO TRUE
              SET IDSV0003-PRIMARY-KEY       TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE

              PERFORM E700-VALORIZZA-STD    THRU E700-EX

              MOVE PGM-IABS0080       TO WK-PGM-ERRORE
              CALL PGM-IABS0080       USING IDSV0003
                                            BTC-JOB-EXECUTION
              ON EXCEPTION
                 INITIALIZE WK-LOG-ERRORE

                 STRING 'ERRORE CHIAMATA MODULO '
                         DELIMITED BY SIZE
                         PGM-IABS0080
                         DELIMITED BY SIZE
                         ' - '
                          DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              END-CALL

              IF WK-ERRORE-NO
                 IF IDSV0003-SUCCESSFUL-RC
                    EVALUATE TRUE
                       WHEN IDSV0003-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                         CONTINUE

                    WHEN IDSV0003-NEGATIVI
      *--> ERRORE DI ACCESSO AL DB
                       INITIALIZE WK-LOG-ERRORE

                       MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                       STRING 'ERRORE MODULO '
                              DELIMITED BY SIZE
                              PGM-IABS0080
                              DELIMITED BY SIZE
                              ' SQLCODE : '
                              DELIMITED BY SIZE
                              WK-SQLCODE ' - '
                              DELIMITED BY SIZE
                              'ID-BATCH : '
                              WK-ID-BATCH ' - '
                              DELIMITED BY SIZE
                              'ID-JOB : '
                              WK-ID-JOB ' - '
                              DELIMITED BY SIZE INTO
                              WK-LOR-DESC-ERRORE-ESTESA
                       END-STRING

                       MOVE ERRORE-FATALE  TO WK-LOR-ID-GRAVITA-ERRORE
                       PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                           THRU W070-EX
                    END-EVALUATE
                 ELSE
      *--> GESTIRE ERRORE DISPATCHER
                    INITIALIZE WK-LOG-ERRORE

                    STRING 'ERRORE MODULO '
                            PGM-IABS0080
                            ' - '
                            'RC : '
                            IDSV0003-RETURN-CODE
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                        THRU W070-EX
                 END-IF
              END-IF
           END-IF.
      *
       E500-EX.
           EXIT.
      *----------------------------------------------------------------*
      * UPDATE IN JOB EXECUTION
      *----------------------------------------------------------------*
       E550-UPD-JOB-EXECUTION.
      *
           MOVE 'E550-UPD-JOB-EXECUTION'    TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF NOT IABV0006-GUIDE-AD-HOC     AND
              NOT IABV0006-BY-BOOKING       AND
              NOT WK-SIMULAZIONE-INFR       AND
              IABI0011-FLAG-STOR-ESECUZ-YES

      *-->    TIPO OPERAZIONE
              SET IDSV0003-UPDATE            TO TRUE
              SET IDSV0003-PRIMARY-KEY       TO TRUE

      *-->    INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE

              PERFORM E700-VALORIZZA-STD     THRU E700-EX

              MOVE PGM-IABS0080              TO WK-PGM-ERRORE
              CALL PGM-IABS0080              USING IDSV0003
                                                   BTC-JOB-EXECUTION
              ON EXCEPTION
                 INITIALIZE WK-LOG-ERRORE

                 STRING 'ERRORE CHIAMATA MODULO '
                         DELIMITED BY SIZE
                         PGM-IABS0080
                         DELIMITED BY SIZE
                         ' - '
                          DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              END-CALL

              IF WK-ERRORE-NO
                 IF IDSV0003-SUCCESSFUL-RC
                    EVALUATE TRUE
                       WHEN IDSV0003-SUCCESSFUL-SQL
      *-->             OPERAZIONE ESEGUITA CORRETTAMENTE
                            PERFORM E800-INS-EXE-MESSAGE THRU E800-EX

                       WHEN IDSV0003-NEGATIVI
      *--->            ERRORE DI ACCESSO AL DB
                         INITIALIZE WK-LOG-ERRORE

                         MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                         STRING 'ERRORE MODULO '
                                DELIMITED BY SIZE
                                PGM-IABS0080
                                DELIMITED BY SIZE
                                ' SQLCODE : '
                                WK-SQLCODE ' - '
                                'ID-BATCH : '
                                WK-ID-BATCH ' - '
                                'ID-JOB : '
                                WK-ID-JOB ' - '
                                DELIMITED BY SIZE INTO
                                WK-LOR-DESC-ERRORE-ESTESA
                         END-STRING

                         MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                         PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                             THRU W070-EX
                    END-EVALUATE
                 ELSE
      *--> GESTIRE ERRORE DISPATCHER
                    INITIALIZE WK-LOG-ERRORE

                    STRING 'ERRORE MODULO '
                            PGM-IABS0080
                            ' - '
                            'RC : '
                            IDSV0003-RETURN-CODE
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                        THRU W070-EX
                 END-IF
              END-IF
           END-IF.
      *
       E550-EX.
           EXIT.
      *----------------------------------------------------------------*
      * VALORIZZA JOB EXECUTION DA STD
      *----------------------------------------------------------------*
       E700-VALORIZZA-STD.

           MOVE 'E700-VALORIZZA-STD'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           MOVE WS-LUNG-EFF-BLOB         TO IDSV0003-BUFFER-WHERE-COND.

           MOVE BJS-ID-BATCH             TO BJE-ID-BATCH
           MOVE BJS-ID-JOB               TO BJE-ID-JOB
                                            WK-ID-JOB

           MOVE BJS-DT-START             TO BJE-DT-START

           IF BJS-DT-END-NULL NOT = HIGH-VALUE
              MOVE BJS-DT-END            TO BJE-DT-END
           ELSE
              MOVE BJS-DT-END-NULL       TO BJE-DT-END-NULL
           END-IF

           MOVE BJS-USER-START           TO BJE-USER-START
           MOVE IABV0003-BLOB-DATA       TO BJE-DATA

           MOVE IABV0002-STATE-CURRENT   TO BJE-COD-ELAB-STATE

           MOVE BJS-FLAG-WARNINGS        TO BJE-FLAG-WARNINGS.
      *
       E700-EX.
           EXIT.
      *----------------------------------------------------------------*
      * INSERT IN EXE MESSAGE
      *----------------------------------------------------------------*
       E800-INS-EXE-MESSAGE.
      *
           MOVE 'E800-INS-EXE-MESSAGE'      TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF NOT IABV0006-GUIDE-AD-HOC     AND
              NOT IABV0006-BY-BOOKING       AND
              NOT WK-SIMULAZIONE-INFR       AND
              IABI0011-FLAG-STOR-ESECUZ-YES

      *--> TIPO OPERAZIONE
              SET IDSV0003-INSERT            TO TRUE
              SET IDSV0003-PRIMARY-KEY       TO TRUE

      *--> INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE

              PERFORM E850-VALORIZZA-EXE-MESSAGE THRU E850-EX

              MOVE PGM-IABS0090           TO WK-PGM-ERRORE
              CALL PGM-IABS0090           USING IDSV0003
                                                BTC-EXE-MESSAGE
              ON EXCEPTION
                 INITIALIZE WK-LOG-ERRORE

                 STRING 'ERRORE CHIAMATA MODULO '
                         DELIMITED BY SIZE
                         PGM-IABS0090
                         DELIMITED BY SIZE
                         ' - '
                          DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              END-CALL

              IF WK-ERRORE-NO
                 IF IDSV0003-SUCCESSFUL-RC
                    EVALUATE TRUE
                    WHEN IDSV0003-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                       CONTINUE

                    WHEN IDSV0003-NEGATIVI
      *--> ERRORE DI ACCESSO AL DB
                       INITIALIZE WK-LOG-ERRORE

                       MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                       STRING 'ERRORE MODULO '
                              DELIMITED BY SIZE
                              PGM-IABS0090
                              DELIMITED BY SIZE
                              ' SQLCODE : '
                              WK-SQLCODE
                              DELIMITED BY SIZE INTO
                              WK-LOR-DESC-ERRORE-ESTESA
                       END-STRING

                       MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                       PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    END-EVALUATE
                 ELSE
      *-->       GESTIRE ERRORE DISPATCHER
                    INITIALIZE WK-LOG-ERRORE

                    STRING 'ERRORE MODULO '
                            PGM-IABS0090
                            ' - '
                            'RC : '
                            IDSV0003-RETURN-CODE
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                 END-IF
              END-IF
           END-IF.
      *
       E800-EX.
           EXIT.
      *----------------------------------------------------------------*
      * VALORIZZA EXE MESSAGE
      *----------------------------------------------------------------*
       E850-VALORIZZA-EXE-MESSAGE.

           MOVE 'E850-VALORIZZA-EXE-MESSAGE' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO   TO TRUE
           MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           INITIALIZE BTC-EXE-MESSAGE.

           MOVE BJE-ID-BATCH           TO BEM-ID-BATCH
           MOVE BJE-ID-JOB             TO BEM-ID-JOB
           MOVE BJE-ID-EXECUTION       TO BEM-ID-EXECUTION

           IF WK-PGM-BUS-SERVICE
              STRING IDSV0001-COD-SERVIZIO-BE ' - '
                  IDSV0001-DESC-ERRORE-ESTESA
                  DELIMITED BY SIZE INTO
                  BEM-MESSAGE
              END-STRING
           ELSE
              MOVE IDSV0003-SQLCODE TO WK-SQLCODE
              STRING WK-PGM-ERRORE        ' - '
                     'RC : '  IDSV0003-RETURN-CODE
                     ' - SQL : ' WK-SQLCODE ' - '
                  IDSV0001-DESC-ERRORE-ESTESA
                  DELIMITED BY SIZE INTO
                  BEM-MESSAGE
              END-STRING
           END-IF.
      *
       E850-EX.
           EXIT.
      *----------------------------------------------------------------*
      * LANCIA BUSINESS
      *----------------------------------------------------------------*
       L000-LANCIA-BUSINESS.

           MOVE 'L000-LANCIA-BUSINESS'      TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           SET WK-LANCIA-BUSINESS-YES         TO TRUE

           PERFORM L500-PRE-BUSINESS          THRU L500-EX

           INITIALIZE IABV0006-REPORT.

           EVALUATE TRUE

           WHEN GESTIONE-REAL-TIME
              PERFORM L600-CALL-BUS-REAL-TIME THRU L600-EX

           WHEN GESTIONE-SUSPEND
              IF WK-FINE-ALIMENTAZIONE-YES
                 SET WK-LANCIA-BUSINESS-YES   TO TRUE
              END-IF

              PERFORM L601-CALL-BUS-SUSPEND   THRU L601-EX

           END-EVALUATE.

           PERFORM L700-POST-BUSINESS         THRU L700-EX.

      *
       L000-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CALL BUSINESS GESTIONE REAL TIME
      *----------------------------------------------------------------*
       L600-CALL-BUS-REAL-TIME.

           MOVE 'L600-CALL-BUS-REAL-TIME'   TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF WK-LANCIA-BUSINESS-YES
              IF IDSV0001-ESITO-OK
                 ADD 1                        TO CONT-BUS-SERV-ESEG

                 SET  IDSV8888-BUSINESS-DBG   TO TRUE
                 MOVE WK-PGM-BUSINESS         TO IDSV8888-NOME-PGM
                 MOVE 'Business Service'      TO IDSV8888-DESC-PGM
                 SET  IDSV8888-INIZIO         TO TRUE
                 PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

                 SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
                 SET  IDSV8888-SONDA-S8           TO TRUE
                 MOVE IDSV0001-MODALITA-ESECUTIVA
                      TO IDSV8888-MODALITA-ESECUTIVA
                 MOVE IDSV0001-USER-NAME          TO IDSV8888-USER-NAME
                 MOVE WK-PGM-BUSINESS             TO IDSV8888-NOME-PGM
                 MOVE 'Business Service LIV-1'    TO IDSV8888-DESC-PGM
                 SET  IDSV8888-INIZIO             TO TRUE
                 PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

                 PERFORM L800-ESEGUI-CALL-BUS THRU L800-EX

                 SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
                 SET  IDSV8888-SONDA-S8           TO TRUE
                 MOVE IDSV0001-MODALITA-ESECUTIVA
                      TO IDSV8888-MODALITA-ESECUTIVA
                 MOVE IDSV0001-USER-NAME          TO IDSV8888-USER-NAME
                 MOVE WK-PGM-BUSINESS             TO IDSV8888-NOME-PGM
                 MOVE 'Business Service LIV-1'    TO IDSV8888-DESC-PGM
                 SET  IDSV8888-FINE             TO TRUE
                 PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX


                 SET  IDSV8888-BUSINESS-DBG   TO TRUE
                 MOVE WK-PGM-BUSINESS         TO IDSV8888-NOME-PGM
                 MOVE 'Business Service'      TO IDSV8888-DESC-PGM
                 SET  IDSV8888-FINE           TO TRUE
                 PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

                 SET IABV0006-CORRENTE-LANCIO TO TRUE
              END-IF

              IF WK-ERRORE-NO
                 IF IDSV0001-ESITO-OK
                    ADD 1               TO CONT-BUS-SERV-ESITO-OK

                    IF WK-SIMULAZIONE-INFR
                       PERFORM Y100-ROLLBACK-SAVEPOINT THRU Y100-EX
                    END-IF

                    IF WK-ERRORE-NO
                       PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX

                       IF WARNING-TROVATO-YES
                          ADD 1              TO CONT-BUS-SERV-WARNING
                          SET WARNING-TROVATO-NO         TO TRUE
                       END-IF

                       IF WK-ERRORE-NO

                          IF NOT WK-SIMULAZIONE-INFR       AND
                             NOT WK-SIMULAZIONE-APPL       AND
                             NOT IABI0011-SENZA-RIPARTENZA AND
                             NOT SEQUENTIAL-GUIDE

                             PERFORM B700-UPD-ESEGUITO-OK
                                                        THRU B700-EX
                          END-IF

                          IF WK-ERRORE-NO
                             IF ((BBT-COMMIT-FREQUENCY > 0) AND
                                (WK-COMMIT-FREQUENCY >=
                                BBT-COMMIT-FREQUENCY)) OR
                                IABV0006-COMMIT-YES

                                SET IABV0006-COMMIT-NO   TO TRUE

                                IF NOT WK-SIMULAZIONE-INFR
                                   PERFORM Y000-COMMIT  THRU Y000-EX
                                   IF (IABV0006-STD-TYPE-REC-NO   OR
                                       IABV0006-STD-TYPE-REC-YES) AND
                                       IDSV0001-ARCH-BATCH-DBG
                                       MOVE WK-ID-JOB
                                         TO WK-ID-JOB-ULT-COMMIT
                                   END-IF
                                   IF WK-ERRORE-NO
                                      PERFORM Y050-CONTROLLO-COMMIT
                                         THRU Y050-EX
                                   END-IF
                                END-IF
                             ELSE
                                ADD 1 TO WK-COMMIT-FREQUENCY
                                PERFORM Y300-SAVEPOINT  THRU Y300-EX
                             END-IF

                          END-IF
                       END-IF
                    END-IF

                 ELSE
                    IF IDSV0001-FORZ-RC-04-YES
                        ADD 1               TO CONT-BUS-SERV-ESITO-OK
                    ELSE
                        ADD 1               TO CONT-BUS-SERV-ESITO-KO
                    END-IF

                    SET  WK-PGM-BUS-SERVICE         TO TRUE
                    MOVE WK-PGM-BUSINESS            TO WK-PGM-ERRORE

                    SET WK-BAT-COLLECTION-KEY-KO         TO TRUE
                    SET WK-JOB-COLLECTION-KEY-KO         TO TRUE

                    IF IABI0011-TRATTAMENTO-FULL
                       PERFORM Y200-ROLLBACK             THRU Y200-EX
                       SET WK-SKIP-BATCH-YES             TO TRUE
                       IF WK-FINE-ALIMENTAZIONE-NO
                          PERFORM N501-PRE-GUIDE-AD-HOC     THRU N501-EX
                          IF WK-ERRORE-NO
                             SET  IDSV0003-CLOSE-CURSOR     TO TRUE
                             SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
                             SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE
                             PERFORM N509-SWITCH-GUIDE      THRU N509-EX
                          END-IF
                       END-IF
                    ELSE
                       PERFORM Y100-ROLLBACK-SAVEPOINT   THRU Y100-EX
                    END-IF

                    IF WK-ERRORE-NO
                       PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX

                       IF IDSV0001-FORZ-RC-04-YES AND
                          WARNING-TROVATO-YES
                          ADD 1              TO CONT-BUS-SERV-WARNING
                          SET WARNING-TROVATO-NO         TO TRUE
                       END-IF

                      IF WK-ERRORE-NO

                         IF NOT WK-SIMULAZIONE-INFR       AND
                            NOT WK-SIMULAZIONE-APPL       AND
                            NOT IABI0011-SENZA-RIPARTENZA AND
                            NOT SEQUENTIAL-GUIDE
                                PERFORM B800-UPD-ESEGUITO-KO
                                                      THRU B800-EX
                         END-IF

                         IF WK-ERRORE-NO
                            PERFORM Y000-COMMIT        THRU Y000-EX
                            IF WK-ERRORE-NO
                               PERFORM Y050-CONTROLLO-COMMIT
                                  THRU Y050-EX
                            END-IF

                            IF WK-ERRORE-NO
                               SET WK-PGM-ARCHITECTURE    TO TRUE

                               IF WK-PGM-SERV-SECON-BUS NOT =
                                  SPACES AND LOW-VALUE AND HIGH-VALUE
                                  PERFORM L400-TRATTA-SERV-SECOND-BUS
                                                        THRU L400-EX
                               END-IF
                            END-IF
                         END-IF
                      END-IF
                    END-IF
                 END-IF
              END-IF
           END-IF.
      *
       L600-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CALL BUSINESS GESTIONE SUSPEND
      *----------------------------------------------------------------*
       L601-CALL-BUS-SUSPEND.

           MOVE 'L601-CALL-BUS-SUSPEND'     TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF WK-LANCIA-BUSINESS-YES
              IF IDSV0001-ESITO-OK
                 ADD 1                        TO CONT-BUS-SERV-ESEG

                 SET  IDSV8888-BUSINESS-DBG   TO TRUE
                 MOVE WK-PGM-BUSINESS         TO IDSV8888-NOME-PGM
                 MOVE 'Business Service'      TO IDSV8888-DESC-PGM
                 SET  IDSV8888-INIZIO         TO TRUE
                 PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

                 SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
                 SET  IDSV8888-SONDA-S8           TO TRUE
                 MOVE IDSV0001-MODALITA-ESECUTIVA
                      TO IDSV8888-MODALITA-ESECUTIVA
                 MOVE IDSV0001-USER-NAME          TO IDSV8888-USER-NAME
                 MOVE WK-PGM-BUSINESS             TO IDSV8888-NOME-PGM
                 MOVE 'Business Service LIV-1'    TO IDSV8888-DESC-PGM
                 SET  IDSV8888-INIZIO             TO TRUE
                 PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

                 PERFORM L800-ESEGUI-CALL-BUS THRU L800-EX

                 SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
                 SET  IDSV8888-SONDA-S8           TO TRUE
                 MOVE IDSV0001-MODALITA-ESECUTIVA
                      TO IDSV8888-MODALITA-ESECUTIVA
                 MOVE IDSV0001-USER-NAME          TO IDSV8888-USER-NAME
                 MOVE WK-PGM-BUSINESS             TO IDSV8888-NOME-PGM
                 MOVE 'Business Service LIV-1'    TO IDSV8888-DESC-PGM
                 SET  IDSV8888-FINE               TO TRUE
                 PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

                 SET  IDSV8888-BUSINESS-DBG   TO TRUE
                 MOVE WK-PGM-BUSINESS         TO IDSV8888-NOME-PGM
                 MOVE 'Business Service'      TO IDSV8888-DESC-PGM
                 SET  IDSV8888-FINE           TO TRUE
                 PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

                 SET IABV0006-CORRENTE-LANCIO TO TRUE
              END-IF

              IF WK-ERRORE-NO
                 IF IDSV0001-ESITO-OK
                    ADD 1               TO CONT-BUS-SERV-ESITO-OK

                    IF WK-SIMULAZIONE-INFR
                       PERFORM Y100-ROLLBACK-SAVEPOINT   THRU Y100-EX
                    END-IF

                    IF WK-ERRORE-NO
                       PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX

                       IF WARNING-TROVATO-YES
                          ADD 1              TO CONT-BUS-SERV-WARNING
                          SET WARNING-TROVATO-NO         TO TRUE
                       END-IF

                       IF WK-ERRORE-NO
                          SET WK-ALLINEA-STATI-OK        TO TRUE
                          PERFORM B900-CTRL-STATI-SOSP THRU B900-EX

                          IF WK-ERRORE-NO
                             IF ((BBT-COMMIT-FREQUENCY > 0) AND
                                (WK-COMMIT-FREQUENCY >=
                                BBT-COMMIT-FREQUENCY)) OR
                                IABV0006-COMMIT-YES

                                SET IABV0006-COMMIT-NO   TO TRUE

                                IF NOT WK-SIMULAZIONE-INFR
                                   PERFORM Y000-COMMIT THRU Y000-EX
                                   IF (IABV0006-STD-TYPE-REC-NO   OR
                                       IABV0006-STD-TYPE-REC-YES) AND
                                       IDSV0001-ARCH-BATCH-DBG
                                       MOVE WK-ID-JOB
                                         TO WK-ID-JOB-ULT-COMMIT
                                   END-IF
                                   IF WK-ERRORE-NO
                                      PERFORM Y050-CONTROLLO-COMMIT
                                         THRU Y050-EX
                                   END-IF
                                END-IF
                             ELSE
                                ADD 1 TO WK-COMMIT-FREQUENCY
                                PERFORM Y300-SAVEPOINT THRU Y300-EX
                             END-IF
                          END-IF
                       END-IF
                    END-IF

                 ELSE
                    IF IDSV0001-FORZ-RC-04-YES
                        ADD 1               TO CONT-BUS-SERV-ESITO-OK
                    ELSE
                        ADD 1               TO CONT-BUS-SERV-ESITO-KO
                    END-IF
                    SET  WK-PGM-BUS-SERVICE          TO TRUE
                    MOVE WK-PGM-BUSINESS             TO WK-PGM-ERRORE

                    SET WK-BAT-COLLECTION-KEY-KO        TO TRUE
                    SET WK-JOB-COLLECTION-KEY-KO        TO TRUE

                    IF IABI0011-TRATTAMENTO-FULL
                       PERFORM Y200-ROLLBACK            THRU Y200-EX
                       SET WK-SKIP-BATCH-YES            TO TRUE
                       IF WK-FINE-ALIMENTAZIONE-NO
                          PERFORM N501-PRE-GUIDE-AD-HOC     THRU N501-EX
                          IF WK-ERRORE-NO
                             SET  IDSV0003-CLOSE-CURSOR     TO TRUE
                             SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
                             SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE
                             PERFORM N509-SWITCH-GUIDE      THRU N509-EX
                          END-IF
                       END-IF
                    ELSE
                       PERFORM Y100-ROLLBACK-SAVEPOINT THRU Y100-EX
                    END-IF

                    IF WK-ERRORE-NO
                       PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX

                       IF IDSV0001-FORZ-RC-04-YES AND
                          WARNING-TROVATO-YES
                          ADD 1              TO CONT-BUS-SERV-WARNING
                          SET WARNING-TROVATO-NO         TO TRUE
                       END-IF

                       IF WK-ERRORE-NO

                          SET WK-ALLINEA-STATI-KO       TO TRUE
                          PERFORM B900-CTRL-STATI-SOSP  THRU B900-EX

                          IF WK-ERRORE-NO
                             PERFORM Y000-COMMIT        THRU Y000-EX
                             IF WK-ERRORE-NO
                                PERFORM Y050-CONTROLLO-COMMIT
                                   THRU Y050-EX
                             END-IF

                             IF WK-ERRORE-NO
                                SET WK-PGM-ARCHITECTURE    TO TRUE

                                IF WK-PGM-SERV-SECON-BUS NOT =
                                   SPACES AND LOW-VALUE AND HIGH-VALUE
                                   PERFORM L400-TRATTA-SERV-SECOND-BUS
                                                          THRU L400-EX
                                END-IF
                             END-IF
                          END-IF
                       END-IF
                    END-IF
                 END-IF
              END-IF
           END-IF.

           IF WK-ERRORE-NO
              IF WK-LANCIA-ROTTURA-NO
                 ADD 1                         TO IND-STATI-SOSP
                 PERFORM L801-SOSPENDI-STATI   THRU L801-EX
              END-IF
           END-IF.
      *
       L601-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CALL BUSINESS EOF
      *----------------------------------------------------------------*
       L602-CALL-BUS-EOF.

           MOVE 'L602-CALL-BUS-EOF'          TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF WK-ERRORE-NO
              SET  IDSV8888-BUSINESS-DBG TO TRUE
              MOVE WK-PGM-BUSINESS       TO IDSV8888-NOME-PGM
              MOVE 'Business Service'    TO IDSV8888-DESC-PGM
              SET  IDSV8888-INIZIO       TO TRUE
              PERFORM ESEGUI-DISPLAY     THRU ESEGUI-DISPLAY-EX

              SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
              SET  IDSV8888-SONDA-S8           TO TRUE
              MOVE IDSV0001-MODALITA-ESECUTIVA
                   TO IDSV8888-MODALITA-ESECUTIVA
              MOVE IDSV0001-USER-NAME          TO IDSV8888-USER-NAME
              MOVE WK-PGM-BUSINESS             TO IDSV8888-NOME-PGM
              MOVE 'Business Service LIV-1'    TO IDSV8888-DESC-PGM
              SET  IDSV8888-INIZIO             TO TRUE
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX


              PERFORM L800-ESEGUI-CALL-BUS THRU L800-EX

              SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
              SET  IDSV8888-SONDA-S8           TO TRUE
              MOVE IDSV0001-MODALITA-ESECUTIVA
                   TO IDSV8888-MODALITA-ESECUTIVA
              MOVE IDSV0001-USER-NAME          TO IDSV8888-USER-NAME
              MOVE WK-PGM-BUSINESS             TO IDSV8888-NOME-PGM
              MOVE 'Business Service LIV-1'    TO IDSV8888-DESC-PGM
              SET  IDSV8888-FINE               TO TRUE
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

              SET  IDSV8888-BUSINESS-DBG TO TRUE
              MOVE WK-PGM-BUSINESS       TO IDSV8888-NOME-PGM
              MOVE 'Business Service'    TO IDSV8888-DESC-PGM
              SET  IDSV8888-FINE         TO TRUE
              PERFORM ESEGUI-DISPLAY     THRU ESEGUI-DISPLAY-EX

              IF WK-ERRORE-NO
                 IF IDSV0001-ESITO-OK
                    PERFORM Y400-ESITO-OK-OTHER-SERV THRU Y400-EX
                 ELSE
                    PERFORM Y500-ESITO-KO-OTHER-SERV THRU Y500-EX
                 END-IF
              END-IF
           END-IF.
      *
       L602-EX.
           EXIT.
      *----------------------------------------------------------------*
      * TRATTA SERVIZIO SECONDARIO DI BUSINESS
      *----------------------------------------------------------------*
       L400-TRATTA-SERV-SECOND-BUS.

           MOVE 'L400-TRATTA-SERV-SECOND-BUS' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO    TO TRUE
           MOVE WK-LABEL                      TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           PERFORM L901-PRE-SERV-SECOND-BUS             THRU L901-EX

           IF WK-ERRORE-NO

              SET  IDSV8888-BUSINESS-DBG         TO TRUE
              MOVE WK-PGM-SERV-SECON-BUS         TO IDSV8888-NOME-PGM
              MOVE 'Serv second di business'     TO IDSV8888-DESC-PGM
              SET  IDSV8888-INIZIO               TO TRUE
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

              PERFORM L900-CALL-SERV-SECOND-BUS         THRU L900-EX

              SET  IDSV8888-BUSINESS-DBG         TO TRUE
              MOVE WK-PGM-SERV-SECON-BUS         TO IDSV8888-NOME-PGM
              MOVE 'Serv second di business'     TO IDSV8888-DESC-PGM
              SET  IDSV8888-FINE                 TO TRUE
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

              IF WK-ERRORE-NO
                 PERFORM L902-POST-SERV-SECOND-BUS      THRU L902-EX

                 IF WK-ERRORE-NO

                    IF IDSV0001-ESITO-OK
                       PERFORM Y400-ESITO-OK-OTHER-SERV THRU Y400-EX
                    ELSE
                       PERFORM Y500-ESITO-KO-OTHER-SERV THRU Y500-EX
                    END-IF
                 END-IF

              END-IF
           END-IF.
      *
       L400-EX.
           EXIT.
      *----------------------------------------------------------------*
      * TRATTA SERVIZIO SECONDARIO DI ROTTURA
      *----------------------------------------------------------------*
       L450-TRATTA-SERV-SECOND-ROTT.

           MOVE 'L450-TRATTA-SERV-SECOND-ROTT' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO     TO TRUE
           MOVE WK-LABEL                       TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           PERFORM L951-PRE-SERV-SECOND-ROTT   THRU L951-EX

           IF WK-ERRORE-NO

              SET  IDSV8888-BUSINESS-DBG          TO TRUE
              MOVE WK-PGM-SERV-SECON-ROTT         TO IDSV8888-NOME-PGM
              MOVE 'Serv second. di rottura'      TO IDSV8888-DESC-PGM
              SET  IDSV8888-INIZIO                TO TRUE
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

              PERFORM L950-CALL-SERV-SECOND-ROTT  THRU L950-EX

              SET  IDSV8888-BUSINESS-DBG          TO TRUE
              MOVE WK-PGM-SERV-SECON-ROTT         TO IDSV8888-NOME-PGM
              MOVE 'Serv second. di rottura'      TO IDSV8888-DESC-PGM
              SET  IDSV8888-FINE                  TO TRUE
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

              IF WK-ERRORE-NO
                 PERFORM L952-POST-SERV-SECOND-ROTT  THRU L952-EX

                 IF WK-ERRORE-NO
                    IF IDSV0001-ESITO-OK
                       PERFORM Y400-ESITO-OK-OTHER-SERV   THRU Y400-EX
                    ELSE
                       PERFORM Y500-ESITO-KO-OTHER-SERV   THRU Y500-EX
                    END-IF
                 END-IF

              END-IF

           END-IF.
      *
       L450-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ELABORAZIONE IN FASE DI ROTTURA
      *----------------------------------------------------------------*
       Q001-ELABORA-ROTTURA.

           MOVE 'Q001-ELABORA-ROTTURA'      TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           SET WK-LANCIA-ROTTURA-YES         TO TRUE

           IF IABI0011-TRATTAMENTO-PARTIAL
              IF NOT WK-SIMULAZIONE-INFR
                 PERFORM Y000-COMMIT      THRU Y000-EX
              END-IF
           ELSE
              IF WK-JOB-COLLECTION-KEY-KO
                 PERFORM Y200-ROLLBACK    THRU Y200-EX
              END-IF
           END-IF

           IF WK-ERRORE-NO
              PERFORM Q002-LANCIA-ROTTURA      THRU Q002-EX

              IF WK-ERRORE-NO
                 IF WK-LANCIA-ROTTURA-YES
                    PERFORM Q503-CNTL-SERV-ROTTURA THRU Q503-EX

                    SET WK-JOB-COLLECTION-KEY-OK   TO TRUE

                    SET WK-LANCIA-ROTTURA-NO       TO TRUE
                 END-IF
              END-IF
           END-IF.
      *
       Q001-EX.
           EXIT.
      *----------------------------------------------------------------*
      * LANCIO DEL MODULO DI ROTTURA
      *----------------------------------------------------------------*
       Q002-LANCIA-ROTTURA.

           MOVE 'Q002-LANCIA-ROTTURA'       TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF WK-ERRORE-NO
              IF GESTIONE-SUSPEND
                 PERFORM Y300-SAVEPOINT                    THRU Y300-EX

                 IF WK-ERRORE-NO
                    IF WK-FINE-ALIMENTAZIONE-NO
                       PERFORM L000-LANCIA-BUSINESS        THRU L000-EX
                    ELSE
                       IF BBT-DEF-CONTENT-TYPE(4:) =
                          ULTIMO-LANCIO-X-CLOSE-FILE
                          PERFORM L000-LANCIA-BUSINESS     THRU L000-EX

                          IF WK-ERRORE-NO
                             SET IABV0006-ULTIMO-LANCIO    TO TRUE
                             PERFORM L602-CALL-BUS-EOF     THRU L602-EX
                          END-IF
                       ELSE
                          SET IABV0006-ULTIMO-LANCIO       TO TRUE
                          PERFORM L000-LANCIA-BUSINESS     THRU L000-EX
                       END-IF
                    END-IF
                 END-IF
              END-IF

              IF WK-ERRORE-NO
                 PERFORM Q501-PRE-SERV-ROTTURA             THRU Q501-EX

                 IF WK-ERRORE-NO
                    IF WK-LANCIA-ROTTURA-YES
                       SET  IDSV8888-BUSINESS-DBG   TO TRUE
                       MOVE WK-PGM-SERV-ROTTURA     TO IDSV8888-NOME-PGM
                       MOVE 'Servizio di rottura'   TO IDSV8888-DESC-PGM
                       SET  IDSV8888-INIZIO         TO TRUE
                       PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

                       PERFORM Q500-CALL-SERV-ROTTURA    THRU Q500-EX

                       SET  IDSV8888-BUSINESS-DBG   TO TRUE
                       MOVE WK-PGM-SERV-ROTTURA     TO IDSV8888-NOME-PGM
                       MOVE 'Servizio di rottura'   TO IDSV8888-DESC-PGM
                       SET  IDSV8888-FINE           TO TRUE
                       PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

                       IF WK-ERRORE-NO
                          SET WK-LANCIA-ROTTURA-YES      TO TRUE
                          PERFORM Q502-POST-SERV-ROTTURA THRU Q502-EX
                       END-IF
                    END-IF
                 END-IF
              END-IF
           END-IF.
      *
       Q002-EX.
           EXIT.
      *****************************************************************
       R000-ESTRAI-DATI-ELABORAZIONE.

           MOVE 'R000-ESTRAI-DATI-ELABORAZIONE' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO      TO TRUE
           MOVE WK-LABEL                        TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           PERFORM Y300-SAVEPOINT           THRU Y300-EX

           IF WK-ERRORE-NO
              MOVE 0                           TO WS-LUNG-EFF-BLOB
              IF NOT IABV0006-GUIDE-AD-HOC AND
                 NOT IABV0006-BY-BOOKING

                 IF BJS-DATA =  SPACES OR LOW-VALUES OR HIGH-VALUES
                    SET IABV0006-STD-TYPE-REC-YES TO TRUE
                    PERFORM E620-ESTRAI-RECORD    THRU E620-EX
                 ELSE
                    SET IABV0006-STD-TYPE-REC-NO TO TRUE
                    INITIALIZE IABV0003
                    MOVE BJS-DATA-LEN            TO WS-LUNG-EFF-BLOB
                    MOVE BJS-DATA                TO IABV0003-BLOB-DATA
                 END-IF
              END-IF
           END-IF.
      *
       R000-EX.
           EXIT.
      *----------------------------------------------------------------*
      * UPDATE DEI JOB
      *----------------------------------------------------------------*
       U000-UPD-JOB.

           MOVE 'U000-UPD-JOB'              TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           MOVE 'U000-UPD-JOB'            TO WK-LABEL.

      *--> TIPO OPERAZIONE
           SET IDSV0003-UPDATE            TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

           IF IABV0006-GUIDE-AD-HOC OR
              IABV0006-BY-BOOKING

              PERFORM N501-PRE-GUIDE-AD-HOC  THRU N501-EX

              IF WK-ERRORE-NO
                 PERFORM N509-SWITCH-GUIDE   THRU N509-EX
              END-IF

           ELSE
              MOVE BTC-ID-BATCH           TO BJS-ID-BATCH
              MOVE PGM-IABS0060           TO WK-PGM-ERRORE
              CALL PGM-IABS0060           USING IDSV0003
                                                IABV0002
                                                BTC-JOB-SCHEDULE
              ON EXCEPTION
                 INITIALIZE WK-LOG-ERRORE
                 STRING 'ERRORE CHIAMATA MODULO '
                         DELIMITED BY SIZE
                         PGM-IABS0060
                         DELIMITED BY SIZE
                         ' - '
                          DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              END-CALL

           END-IF

           IF WK-ERRORE-NO
              IF IDSV0003-SUCCESSFUL-RC
                 EVALUATE TRUE

                    WHEN IDSV0003-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                       CONTINUE

                    WHEN IDSV0003-NOT-FOUND
      *--->         CAMPO $ NON TROVATO
                       IF WK-NOT-FOUND-NOT-ALLOWED
                          INITIALIZE WK-LOG-ERRORE

                          MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                          STRING WK-PGM-ERRORE
                                 ' - '
                                 'SQLCODE : '
                                 WK-SQLCODE ' - '
                                 'ID-BATCH : '
                                 WK-ID-BATCH ' - '
                                 DELIMITED BY SIZE INTO
                                 WK-LOR-DESC-ERRORE-ESTESA
                          END-STRING

                          MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                          PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                              THRU W070-EX
                       END-IF

                     WHEN IDSV0003-NEGATIVI
      *--->          ERRORE DI ACCESSO AL DB
                        INITIALIZE WK-LOG-ERRORE

                        MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                        STRING WK-PGM-ERRORE
                               ' - '
                               'SQLCODE : '
                               WK-SQLCODE
                               DELIMITED BY SIZE INTO
                               WK-LOR-DESC-ERRORE-ESTESA
                        END-STRING

                        MOVE ERRORE-FATALE  TO WK-LOR-ID-GRAVITA-ERRORE
                        PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                            THRU W070-EX
                 END-EVALUATE

              ELSE

                 IF WK-UPDATE-FOR-RESTORE-NO
      *-->       GESTIRE ERRORE DISPATCHER
                    INITIALIZE WK-LOG-ERRORE

                    STRING 'ERRORE MODULO '
                            WK-PGM-ERRORE
                            ' - '
                            'RC : '
                            IDSV0003-RETURN-CODE
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                        THRU W070-EX
                 END-IF
              END-IF
           END-IF.
      *
       U000-EX.
           EXIT.

      *****************************************************************
       Z000-DISP-OCCORR-GUIDE.

           MOVE 'Z000-CONTA-OCCORRENZE-GUIDE' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO      TO TRUE
           MOVE WK-LABEL                        TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF IABI0011-CONTATORE IS NUMERIC AND
                    IABI0011-CONTATORE GREATER ZERO
                   DIVIDE CONT-TAB-GUIDE-LETTE
                        BY IABI0011-CONTATORE
                          GIVING RESULT REMAINDER RESTO
               IF RESTO = 0 OR IDSV0003-FETCH-FIRST  OR
                             WK-FINE-ALIMENTAZIONE-YES
                 SET  IDSV8888-ARCH-BATCH-DBG TO TRUE
                 MOVE 'Architettura Batch'
                              TO IDSV8888-DESC-PGM
                 STRING WK-PGM '/'
                       'Rec. Letti: '
                       CONT-TAB-GUIDE-LETTE
                 DELIMITED BY SIZE
                     INTO IDSV8888-NOME-PGM
                 END-STRING
                 PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
               END-IF
            END-IF.
      *
       Z000-EX.
           EXIT.



      *------------------------------------------------------------*
      *   PORTAFOGLIO VITA                                         *
      *   AREA DI PAGINA                                           *
      *------------------------------------------------------------*
      *03  WPAG-AREA-PAGINA

      *--  AREA DETTAGLIO FONDI
           05 (SF)-AREA-DETT-FONDI.
              07 (SF)-TRATT-COD-FND            PIC  X(01).
              07 (SF)-TRATT-NUM-QUO            PIC  X(01).
              07 (SF)-TRATT-VAL-QUO            PIC  X(01).
              07 (SF)-TRATT-IMP-DISINV         PIC  X(01).
              07 (SF)-ELE-DETT-FONDI-MAX       PIC S9(04) COMP.
              07 (SF)-DATI-TRANCHE             OCCURS 1250.
                 09 (SF)-COD-FND               PIC  X(20).
                 09 (SF)-TP-FND                PIC  X(01).
                 09 (SF)-NUM-QUO               PIC S9(07)V9(5) COMP-3.
                 09 (SF)-NUM-QUO-NULL          REDEFINES
                    (SF)-NUM-QUO               PIC  X(07).
                 09 (SF)-VAL-QUO               PIC S9(12)V9(3) COMP-3.
                 09 (SF)-VAL-QUO-NULL          REDEFINES
                    (SF)-VAL-QUO               PIC  X(08).
                 09 (SF)-IMP-DISINV            PIC S9(12)V9(3) COMP-3.
                 09 (SF)-IMP-DISINV-NULL       REDEFINES
                    (SF)-IMP-DISINV            PIC  X(08).
                 09 (SF)-ID-TRCH-DI-GAR        PIC S9(09) COMP-3.

       01 LCCC0450.
      *
      *   Area Input
      *
          05 LCCC0450-AREA-INPUT.
             10 LCCC0450-DATA-EFFETTO           PIC  9(08).
             10 LCCC0450-DATA-COMPETENZA        PIC  9(18).
             10 LCCC0450-DATA-RICORR            PIC  9(08).
             10 LCCC0450-FLAG-OPER              PIC  X(02).
             10 LCCC0450-DATA-RISERVA           PIC  9(08).
      *
      *   Area Output
      *
          05 LCCC0450-AREA-OUTPUT.
             10 LCCC0450-ELE-TAB-MAX            PIC S9(04) COMP.
             10 LCCC0450-TAB-MATRICE.
45229 *         12 LCCC0450-MATRICE             OCCURS 1000.
45229 *         12 LCCC0450-MATRICE             OCCURS 1500.
51365           12 LCCC0450-MATRICE             OCCURS 2000.
                   15 LCCC0450-ID-TRCH-DI-GAR   PIC S9(09)V     COMP-3.
                   15 LCCC0450-COD-FONDO        PIC  X(20).
                   15 LCCC0450-NUM-QUOTE        PIC S9(07)V9(5) COMP-3.
                   15 LCCC0450-VAL-QUOTA        PIC S9(12)V9(3) COMP-3.
                   15 LCCC0450-NUM-QUOTE-INI    PIC S9(07)V9(5) COMP-3.
                   15 LCCC0450-VAL-QUOTA-INI    PIC S9(12)V9(3) COMP-3.
                   15 LCCC0450-VAL-QUOTA-T      PIC S9(12)V9(3) COMP-3.
                   15 LCCC0450-PERCENT-INV      PIC S9(03)V9(3) COMP-3.
                   15 LCCC0450-CONTROVALORE     PIC S9(12)V9(3) COMP-3.
                   15 LCCC0450-DT-VAL-QUOTE     PIC  9(08).
                   15 LCCC0450-IMP-INVES        PIC S9(12)V9(3) COMP-3.
                   15 LCCC0450-IMP-DISINV       PIC S9(12)V9(3) COMP-3.
             10 LCCC0450-TAB-MATRICE-R REDEFINES LCCC0450-TAB-MATRICE.
                12 FILLER                       PIC  X(99).
45229 *         12 LCCC0450-RESTO-TAB-MATRICE   PIC  X(98901).
45229 *         12 LCCC0450-RESTO-TAB-MATRICE   PIC  X(148401).
51365           12 LCCC0450-RESTO-TAB-MATRICE   PIC  X(197901).
             10 LCCC0450-TOT-CONTRATTO          PIC S9(12)V9(3) COMP-3.
             10 LCCC0450-TOT-IMP-INVES          PIC S9(12)V9(3) COMP-3.
             10 LCCC0450-TOT-IMP-DISINV         PIC S9(12)V9(3) COMP-3.

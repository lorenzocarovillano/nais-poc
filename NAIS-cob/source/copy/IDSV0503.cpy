      *-- INPUT
       01 IDSV0503-INPUT.
          05 IDSV0503-ELE-VARIABILI-MAX      PIC S9(04) COMP-3.
          05 IDSV0503-TAB-VARIABILI          OCCURS 100.
             10 IDSV0503-AREA-VARIABILE.
                15 IDSV0503-COD-VARIABILE    PIC  X(12).
                15 IDSV0503-TP-DATO          PIC  X(01).
                15 IDSV0503-VAL-GENERICO     PIC  X(60).

                15 IDSV0503-VAL-IMP-GEN      REDEFINES
                   IDSV0503-VAL-GENERICO.
                   17 IDSV0503-VAL-IMP       PIC  S9(11)V9(07).
                   17 IDSV0503-VAL-IMP-FILLER
                                             PIC  X(42).

                15 IDSV0503-VAL-PERC-GEN     REDEFINES
                   IDSV0503-VAL-GENERICO.
                   17 IDSV0503-VAL-PERC      PIC  9(05)V9(09).
                   17 IDSV0503-VAL-PERC-FILLER
                                             PIC X(46).

                15 IDSV0503-VAL-STR-GEN     REDEFINES
                   IDSV0503-VAL-GENERICO.
                   17 IDSV0503-VAL-STR      PIC  X(60).

      ***************************************************************
      * FLAGS
      ***************************************************************
       01 IDSV0503-TIPO-FORMATO.
          05 IDSV0503-IMPORTO                      PIC X(01) VALUE 'I'.
          05 IDSV0503-NUMERICO                     PIC X(01) VALUE 'N'.
          05 IDSV0503-MILLESIMI                    PIC X(01) VALUE 'M'.
          05 IDSV0503-PERCENTUALE                  PIC X(01) VALUE 'P'.
          05 IDSV0503-DT                           PIC X(01) VALUE 'D'.
          05 IDSV0503-STRINGA                      PIC X(01) VALUE 'S'.
          05 IDSV0503-TASSO                        PIC X(01) VALUE 'A'.
          05 IDSV0503-LISTA-IMPORTO                PIC X(01) VALUE 'R'.
          05 IDSV0503-LISTA-NUMERICO               PIC X(01) VALUE 'H'.
          05 IDSV0503-LISTA-MILLESIMI              PIC X(01) VALUE 'Z'.
          05 IDSV0503-LISTA-PERCENTUALE            PIC X(01) VALUE 'O'.
          05 IDSV0503-LISTA-DT                     PIC X(01) VALUE 'C'.
          05 IDSV0503-LISTA-STRINGA                PIC X(01) VALUE 'L'.
          05 IDSV0503-LISTA-TASSO                  PIC X(01) VALUE 'T'.
          05 IDSV0503-LISTA-MISTA                  PIC X(01) VALUE 'X'.

       01 IDSV0503-TIPO-SEGNO                      PIC X(01).
          88 IDSV0503-SENZA-SEGNO                  VALUE ' '.
          88 IDSV0503-SEGNO-POSITIVO               VALUE '+'.
          88 IDSV0503-SEGNO-NEGATIVO               VALUE '-'.

       01 IDSV0503-DATO-INPUT-TROVATO              PIC X(01).
          88 IDSV0503-DATO-INPUT-TROVATO-SI        VALUE 'S'.
          88 IDSV0503-DATO-INPUT-TROVATO-NO        VALUE 'N'.

       01 IDSV0503-FINE-STRINGA                    PIC X(01).
          88 IDSV0503-FINE-STRINGA-SI              VALUE 'S'.
          88 IDSV0503-FINE-STRINGA-NO              VALUE 'N'.

       01 IDSV0503-ALFA-TROVATO                    PIC X(01).
          88 IDSV0503-ALFA-TROVATO-SI              VALUE 'S'.
          88 IDSV0503-ALFA-TROVATO-NO              VALUE 'N'.

       01 IDSV0503-DEC-TROVATO                     PIC X(01).
          88 IDSV0503-DEC-TROVATO-SI               VALUE 'S'.
          88 IDSV0503-DEC-TROVATO-NO               VALUE 'N'.

       01 IDSV0503-INT-TROVATO                     PIC X(01).
          88 IDSV0503-INT-TROVATO-SI               VALUE 'S'.
          88 IDSV0503-INT-TROVATO-NO               VALUE 'N'.

       01 IDSV0503-SEGNO-POSTIV                    PIC X(01).
          88 IDSV0503-SEGNO-POSTIV-SI              VALUE 'S'.
          88 IDSV0503-SEGNO-POSTIV-NO              VALUE 'N'.

       01 IDSV0503-POS-SEGNO                       PIC X(01).
          88 IDSV0503-SEGNO-ANTERIORE              VALUE 'A'.
          88 IDSV0503-SEGNO-POSTERIORE             VALUE 'P'.

      ***************************************************************
      * INDICI
      ***************************************************************
       77 IDSV0503-IND-VAR                         PIC 9(04).
       77 IDSV0503-IND-STRINGA                     PIC 9(04).
       77 IDSV0503-IND-ALFA                        PIC 9(04).
       77 IDSV0503-IND-INT                         PIC 9(04).
       77 IDSV0503-IND-DEC                         PIC 9(04).
       77 IDSV0503-IND-OUT                         PIC 9(04).
       77 IDSV0503-IND-RICERCA                     PIC 9(04).
       77 IDSV0503-POSIZ-MEDIA                     PIC 9(04).
       77 IDSV0503-POSIZ-SUCC                      PIC 9(04).
       77 IDSV0503-PARTE-INTERA                    PIC 9(04).
       77 IDSV0503-PARTE-DECIMALE                  PIC 9(04).
       77 IDSV0503-INTERVALLO                      PIC 9(04).

      ***************************************************************
      * LIMITI
      ***************************************************************
       77 IDSV0503-LIMITE-INT-DEC                  PIC 9(02) VALUE 18.
       77 IDSV0503-LIMITE-STRINGA                  PIC 9(04) VALUE 60.
       77 IDSV0503-LIMITE-STRINGA-OUTPUT           PIC 9(04) VALUE 4000.
       77 IDSV0503-LIMITE-ARRAY-INPUT              PIC 9(03) VALUE 60.
       77 IDSV0503-LIMITE-ARRAY-OUTPUT             PIC 9(02) VALUE 05.

       77 IDSV0503-LIMITE-DECIMALI                 PIC 9(02) VALUE 0.
       77 IDSV0503-LIMITE-INTERI                   PIC 9(02) VALUE 0.
       77 IDSV0503-LIM-INT-IMP                     PIC 9(02) VALUE 11.
       77 IDSV0503-LIM-DEC-IMP                     PIC 9(02) VALUE 12.
       77 IDSV0503-LIM-INT-PERC                    PIC 9(02) VALUE 9.
       77 IDSV0503-LIM-DEC-PERC                    PIC 9(02) VALUE 10.

       77 IDSV0503-DECIMALI-ESPOSTI-IMP            PIC 9(1).
       77 IDSV0503-DECIMALI-ESPOSTI-PERC           PIC 9(1).
       77 IDSV0503-DECIMALI-ESPOSTI-TASS           PIC 9(1).

       77 IDSV0503-SEPARATORE-VALORE               PIC X(01).
       77 IDSV0503-SEPARATORE-VARIABILE            PIC X(01).
       77 IDSV0503-SIMBOLO-DECIMALE                PIC X(01).

       77 IDSV0503-SPAZIO-RICHIESTO                PIC 9(04).
       77 IDSV0503-SPAZIO-RESTANTE                 PIC 9(04).
       77 IDSV0503-POSIZIONE                       PIC 9(04).
       77 IDSV0503-POSIZIONE-TOT                   PIC 9(04).
       77 IDSV0503-POSIZ-FINALE                    PIC 9(04).

       77 IDSV0503-CHAR-FINE-STRINGA             PIC X(02) VALUE SPACES.
       77 IDSV0503-LUNG-CHAR-FINE-STR              PIC 9     VALUE 2.

      ***************************************************************
      * COMODO
      ***************************************************************

       01  IDSV0503-TP-DATO-APP                     PIC  X(01).

       01  IDSV0503-STRUCT-NUM                      PIC 9(18).
       01  IDSV0503-STRUCT-IMP                      REDEFINES
           IDSV0503-STRUCT-NUM                      PIC 9(11)V9(07).
       01  IDSV0503-STRUCT-PERC                     REDEFINES
           IDSV0503-STRUCT-NUM                      PIC 9(09)V9(09).
       01  IDSV0503-ELEMENT-NUM-IMP      REDEFINES  IDSV0503-STRUCT-NUM.
           05 IDSV0503-ELE-NUM-IMP                  PIC X OCCURS 18.

       01  IDSV0503-CAMPO-INTERI                    PIC X(18).
       01  IDSV0503-ARRAY-STRINGA-INTERI REDEFINES
           IDSV0503-CAMPO-INTERI.
           05 IDSV0503-ELE-STRINGA-INTERI           PIC X OCCURS 18.

       01  IDSV0503-CAMPO-DECIMALI                  PIC X(18).
       01  IDSV0503-ARRAY-STR-DECIMALI REDEFINES
           IDSV0503-CAMPO-DECIMALI.
           05 IDSV0503-ELE-STRINGA-DECIMALI         PIC X OCCURS 18.

       01  IDSV0503-CAMPO-ALFA                      PIC X(65).
       01  IDSV0503-ARRAY-STRINGA-ALFA REDEFINES
           IDSV0503-CAMPO-ALFA.
           05 IDSV0503-ELE-STRINGA-ALFA             PIC X OCCURS 65.

       01  IDSV0503-CAMPO-APPOGGIO                  PIC X(150).

      ***************************************************************
      *  OUTPUT
      ***************************************************************
       01 IDSV0503-LEN-STRINGA-TOT        PIC  S9(4) COMP-5.
       01 IDSV0503-STRINGA-TOT            PIC  X(4000).

       01 IDSV0503-CAMPI-ESITO.
          05 IDSV0503-RETURN-CODE         PIC  X(002).
             88 IDSV0503-SUCCESSFUL-RC    VALUE '00'.
             88 IDSV0503-GENERIC-ERROR    VALUE 'KO'.

          05 IDSV0503-DESCRIZ-ERR         PIC  X(300).

       01 RIS-DI-TRCH.
         05 RST-ID-RIS-DI-TRCH PIC S9(9)V     COMP-3.
         05 RST-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
         05 RST-ID-MOVI-CHIU PIC S9(9)V     COMP-3.
         05 RST-ID-MOVI-CHIU-NULL REDEFINES
            RST-ID-MOVI-CHIU   PIC X(5).
         05 RST-DT-INI-EFF   PIC S9(8)V COMP-3.
         05 RST-DT-END-EFF   PIC S9(8)V COMP-3.
         05 RST-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 RST-TP-CALC-RIS PIC X(2).
         05 RST-TP-CALC-RIS-NULL REDEFINES
            RST-TP-CALC-RIS   PIC X(2).
         05 RST-ULT-RM PIC S9(12)V9(3) COMP-3.
         05 RST-ULT-RM-NULL REDEFINES
            RST-ULT-RM   PIC X(8).
         05 RST-DT-CALC   PIC S9(8)V COMP-3.
         05 RST-DT-CALC-NULL REDEFINES
            RST-DT-CALC   PIC X(5).
         05 RST-DT-ELAB   PIC S9(8)V COMP-3.
         05 RST-DT-ELAB-NULL REDEFINES
            RST-DT-ELAB   PIC X(5).
         05 RST-RIS-BILA PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-BILA-NULL REDEFINES
            RST-RIS-BILA   PIC X(8).
         05 RST-RIS-MAT PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-MAT-NULL REDEFINES
            RST-RIS-MAT   PIC X(8).
         05 RST-INCR-X-RIVAL PIC S9(12)V9(3) COMP-3.
         05 RST-INCR-X-RIVAL-NULL REDEFINES
            RST-INCR-X-RIVAL   PIC X(8).
         05 RST-RPTO-PRE PIC S9(12)V9(3) COMP-3.
         05 RST-RPTO-PRE-NULL REDEFINES
            RST-RPTO-PRE   PIC X(8).
         05 RST-FRAZ-PRE-PP PIC S9(12)V9(3) COMP-3.
         05 RST-FRAZ-PRE-PP-NULL REDEFINES
            RST-FRAZ-PRE-PP   PIC X(8).
         05 RST-RIS-TOT PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-TOT-NULL REDEFINES
            RST-RIS-TOT   PIC X(8).
         05 RST-RIS-SPE PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-SPE-NULL REDEFINES
            RST-RIS-SPE   PIC X(8).
         05 RST-RIS-ABB PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-ABB-NULL REDEFINES
            RST-RIS-ABB   PIC X(8).
         05 RST-RIS-BNSFDT PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-BNSFDT-NULL REDEFINES
            RST-RIS-BNSFDT   PIC X(8).
         05 RST-RIS-SOPR PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-SOPR-NULL REDEFINES
            RST-RIS-SOPR   PIC X(8).
         05 RST-RIS-INTEG-BAS-TEC PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-INTEG-BAS-TEC-NULL REDEFINES
            RST-RIS-INTEG-BAS-TEC   PIC X(8).
         05 RST-RIS-INTEG-DECR-TS PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-INTEG-DECR-TS-NULL REDEFINES
            RST-RIS-INTEG-DECR-TS   PIC X(8).
         05 RST-RIS-GAR-CASO-MOR PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-GAR-CASO-MOR-NULL REDEFINES
            RST-RIS-GAR-CASO-MOR   PIC X(8).
         05 RST-RIS-ZIL PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-ZIL-NULL REDEFINES
            RST-RIS-ZIL   PIC X(8).
         05 RST-RIS-FAIVL PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-FAIVL-NULL REDEFINES
            RST-RIS-FAIVL   PIC X(8).
         05 RST-RIS-COS-AMMTZ PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-COS-AMMTZ-NULL REDEFINES
            RST-RIS-COS-AMMTZ   PIC X(8).
         05 RST-RIS-SPE-FAIVL PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-SPE-FAIVL-NULL REDEFINES
            RST-RIS-SPE-FAIVL   PIC X(8).
         05 RST-RIS-PREST-FAIVL PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-PREST-FAIVL-NULL REDEFINES
            RST-RIS-PREST-FAIVL   PIC X(8).
         05 RST-RIS-COMPON-ASSVA PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-COMPON-ASSVA-NULL REDEFINES
            RST-RIS-COMPON-ASSVA   PIC X(8).
         05 RST-ULT-COEFF-RIS PIC S9(5)V9(9) COMP-3.
         05 RST-ULT-COEFF-RIS-NULL REDEFINES
            RST-ULT-COEFF-RIS   PIC X(8).
         05 RST-ULT-COEFF-AGG-RIS PIC S9(5)V9(9) COMP-3.
         05 RST-ULT-COEFF-AGG-RIS-NULL REDEFINES
            RST-ULT-COEFF-AGG-RIS   PIC X(8).
         05 RST-RIS-ACQ PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-ACQ-NULL REDEFINES
            RST-RIS-ACQ   PIC X(8).
         05 RST-RIS-UTI PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-UTI-NULL REDEFINES
            RST-RIS-UTI   PIC X(8).
         05 RST-RIS-MAT-EFF PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-MAT-EFF-NULL REDEFINES
            RST-RIS-MAT-EFF   PIC X(8).
         05 RST-RIS-RISTORNI-CAP PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-RISTORNI-CAP-NULL REDEFINES
            RST-RIS-RISTORNI-CAP   PIC X(8).
         05 RST-RIS-TRM-BNS PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-TRM-BNS-NULL REDEFINES
            RST-RIS-TRM-BNS   PIC X(8).
         05 RST-RIS-BNSRIC PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-BNSRIC-NULL REDEFINES
            RST-RIS-BNSRIC   PIC X(8).
         05 RST-DS-RIGA PIC S9(10)V     COMP-3.
         05 RST-DS-OPER-SQL PIC X(1).
         05 RST-DS-VER PIC S9(9)V     COMP-3.
         05 RST-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
         05 RST-DS-TS-END-CPTZ PIC S9(18)V     COMP-3.
         05 RST-DS-UTENTE PIC X(20).
         05 RST-DS-STATO-ELAB PIC X(1).
         05 RST-ID-TRCH-DI-GAR PIC S9(9)V     COMP-3.
         05 RST-COD-FND PIC X(12).
         05 RST-COD-FND-NULL REDEFINES
            RST-COD-FND   PIC X(12).
         05 RST-ID-POLI PIC S9(9)V     COMP-3.
         05 RST-ID-ADES PIC S9(9)V     COMP-3.
         05 RST-ID-GAR PIC S9(9)V     COMP-3.
         05 RST-RIS-MIN-GARTO PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-MIN-GARTO-NULL REDEFINES
            RST-RIS-MIN-GARTO   PIC X(8).
         05 RST-RIS-RSH-DFLT PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-RSH-DFLT-NULL REDEFINES
            RST-RIS-RSH-DFLT   PIC X(8).
         05 RST-RIS-MOVI-NON-INVES PIC S9(12)V9(3) COMP-3.
         05 RST-RIS-MOVI-NON-INVES-NULL REDEFINES
            RST-RIS-MOVI-NON-INVES   PIC X(8).


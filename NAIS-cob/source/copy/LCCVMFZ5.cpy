
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVMFZ5
      *   ULTIMO AGG. 09 LUG 2010
      *------------------------------------------------------------

       VAL-DCLGEN-MFZ.
           IF (SF)-ID-LIQ-NULL(IX-TAB-MFZ) = HIGH-VALUES
              MOVE (SF)-ID-LIQ-NULL(IX-TAB-MFZ)
              TO MFZ-ID-LIQ-NULL
           ELSE
              MOVE (SF)-ID-LIQ(IX-TAB-MFZ)
              TO MFZ-ID-LIQ
           END-IF
           IF (SF)-ID-TIT-CONT-NULL(IX-TAB-MFZ) = HIGH-VALUES
              MOVE (SF)-ID-TIT-CONT-NULL(IX-TAB-MFZ)
              TO MFZ-ID-TIT-CONT-NULL
           ELSE
              MOVE (SF)-ID-TIT-CONT(IX-TAB-MFZ)
              TO MFZ-ID-TIT-CONT
           END-IF
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-MFZ) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-MFZ)
              TO MFZ-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-MFZ)
              TO MFZ-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-MFZ) NOT NUMERIC
              MOVE 0 TO MFZ-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-MFZ)
              TO MFZ-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-MFZ) NOT NUMERIC
              MOVE 0 TO MFZ-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-MFZ)
              TO MFZ-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-MFZ)
              TO MFZ-COD-COMP-ANIA
           MOVE (SF)-TP-MOVI-FINRIO(IX-TAB-MFZ)
              TO MFZ-TP-MOVI-FINRIO
           IF (SF)-DT-EFF-MOVI-FINRIO-NULL(IX-TAB-MFZ) = HIGH-VALUES
              MOVE (SF)-DT-EFF-MOVI-FINRIO-NULL(IX-TAB-MFZ)
              TO MFZ-DT-EFF-MOVI-FINRIO-NULL
           ELSE
             IF (SF)-DT-EFF-MOVI-FINRIO(IX-TAB-MFZ) = ZERO
                MOVE HIGH-VALUES
                TO MFZ-DT-EFF-MOVI-FINRIO-NULL
             ELSE
              MOVE (SF)-DT-EFF-MOVI-FINRIO(IX-TAB-MFZ)
              TO MFZ-DT-EFF-MOVI-FINRIO
             END-IF
           END-IF
           IF (SF)-DT-ELAB-NULL(IX-TAB-MFZ) = HIGH-VALUES
              MOVE (SF)-DT-ELAB-NULL(IX-TAB-MFZ)
              TO MFZ-DT-ELAB-NULL
           ELSE
             IF (SF)-DT-ELAB(IX-TAB-MFZ) = ZERO
                MOVE HIGH-VALUES
                TO MFZ-DT-ELAB-NULL
             ELSE
              MOVE (SF)-DT-ELAB(IX-TAB-MFZ)
              TO MFZ-DT-ELAB
             END-IF
           END-IF
           IF (SF)-DT-RICH-MOVI-NULL(IX-TAB-MFZ) = HIGH-VALUES
              MOVE (SF)-DT-RICH-MOVI-NULL(IX-TAB-MFZ)
              TO MFZ-DT-RICH-MOVI-NULL
           ELSE
             IF (SF)-DT-RICH-MOVI(IX-TAB-MFZ) = ZERO
                MOVE HIGH-VALUES
                TO MFZ-DT-RICH-MOVI-NULL
             ELSE
              MOVE (SF)-DT-RICH-MOVI(IX-TAB-MFZ)
              TO MFZ-DT-RICH-MOVI
             END-IF
           END-IF
           IF (SF)-COS-OPRZ-NULL(IX-TAB-MFZ) = HIGH-VALUES
              MOVE (SF)-COS-OPRZ-NULL(IX-TAB-MFZ)
              TO MFZ-COS-OPRZ-NULL
           ELSE
              MOVE (SF)-COS-OPRZ(IX-TAB-MFZ)
              TO MFZ-COS-OPRZ
           END-IF
           MOVE (SF)-STAT-MOVI(IX-TAB-MFZ)
              TO MFZ-STAT-MOVI
           IF (SF)-DS-RIGA(IX-TAB-MFZ) NOT NUMERIC
              MOVE 0 TO MFZ-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-MFZ)
              TO MFZ-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-MFZ)
              TO MFZ-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-MFZ) NOT NUMERIC
              MOVE 0 TO MFZ-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-MFZ)
              TO MFZ-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-MFZ) NOT NUMERIC
              MOVE 0 TO MFZ-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-MFZ)
              TO MFZ-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-MFZ) NOT NUMERIC
              MOVE 0 TO MFZ-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-MFZ)
              TO MFZ-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-MFZ)
              TO MFZ-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-MFZ)
              TO MFZ-DS-STATO-ELAB
           IF (SF)-TP-CAUS-RETTIFICA-NULL(IX-TAB-MFZ) = HIGH-VALUES
              MOVE (SF)-TP-CAUS-RETTIFICA-NULL(IX-TAB-MFZ)
              TO MFZ-TP-CAUS-RETTIFICA-NULL
           ELSE
              MOVE (SF)-TP-CAUS-RETTIFICA(IX-TAB-MFZ)
              TO MFZ-TP-CAUS-RETTIFICA
           END-IF.
       VAL-DCLGEN-MFZ-EX.
           EXIT.

       01 LDBV4151.
          05 LDBV4151-DATI-INPUT.
             07 LDBV4151-TP-OGG                  PIC X(2).
             07 LDBV4151-ID-OGG                  PIC S9(9)       COMP-3.
             07 LDBV4151-TP-STAT-TIT             PIC X(2).
             07 LDBV4151-DATA-INIZIO-PERIODO     PIC S9(8)       COMP-3.
             07 LDBV4151-DATA-FINE-PERIODO       PIC S9(8)       COMP-3.
             07 LDBV4151-DATA-INIZ-PERIODO-DB    PIC X(10).
             07 LDBV4151-DATA-FINE-PERIODO-DB    PIC X(10).
          05 LDBV4151-DATI-OUTPUT.
             07 LDBV4151-PRE-TOT                 PIC S9(12)V9(3) COMP-3.

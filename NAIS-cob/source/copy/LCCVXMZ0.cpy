      ******************************************************************
      *    TP_MEZ_PAG (Tipo Mezzo Pagamento) versione BNL
      ******************************************************************
       01  WS-TP-MEZ-PAG                    PIC X(002) VALUE SPACES.
           88 MEZ-PAG-RID                              VALUE 'RI'.
           88 MEZ-PAG-RID-EX26                         VALUE 'NR'.
           88 MEZ-PAG-SWITCH                           VALUE 'SW'.
           88 MEZ-PAG-TRAS-FON                         VALUE 'TF'.
           88 MEZ-PAG-NON-AUT                          VALUE 'XE'.
           88 MEZ-PAG-CASH                             VALUE 'CH'.
           88 MEZ-PAG-ACC-CC                           VALUE 'CC'.
           88 MEZ-PAG-ACC-CASH                         VALUE 'C2'.
           88 MEZ-PAG-TRAT-STIP                        VALUE 'TS'.
           88 MEZ-PAG-BONIFICO                         VALUE 'BO'.
           88 MEZ-PAG-BONIF-EST                        VALUE 'BT'.
           88 MEZ-PAG-BONIF-MAN                        VALUE 'BM'.
           88 MEZ-PAG-BONIF-EDW                        VALUE 'BE'.
           88 MEZ-PAG-BONIF-BNL                        VALUE 'BP'.
           88 MEZ-PAG-ASSEGNO                          VALUE 'AS'.
           88 MEZ-PAG-ASS-CIR-BNL                      VALUE 'AC'.
           88 MEZ-PAG-ASS-CIR-ALTRI                    VALUE 'AI'.
           88 MEZ-PAG-ASS-BAN-BNL                      VALUE 'AB'.
           88 MEZ-PAG-ASS-BAN-ALTRI                    VALUE 'AA'.
           88 MEZ-PAG-RID-ESTERNO                      VALUE 'RD'.
           88 MEZ-PAG-DA-COMPLETARE                    VALUE 'DC'.
           88 MEZ-PAG-COMPENSAZIONE                    VALUE 'CP'.
           88 MEZ-PAG-ACCANTONAMENTO                   VALUE 'AT'.
CPICDC     88 MEZ-PAG-CARTA-DI-CREDITO                 VALUE 'CD'.

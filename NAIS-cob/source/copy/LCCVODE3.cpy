
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVODE3
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-ODE.
           MOVE ODE-ID-OGG-DEROGA
             TO (SF)-ID-PTF
           MOVE ODE-ID-OGG-DEROGA
             TO (SF)-ID-OGG-DEROGA
           MOVE ODE-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ
           IF ODE-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE ODE-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL
           ELSE
              MOVE ODE-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU
           END-IF
           MOVE ODE-DT-INI-EFF
             TO (SF)-DT-INI-EFF
           MOVE ODE-DT-END-EFF
             TO (SF)-DT-END-EFF
           MOVE ODE-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE ODE-ID-OGG
             TO (SF)-ID-OGG
           MOVE ODE-TP-OGG
             TO (SF)-TP-OGG
           IF ODE-IB-OGG-NULL = HIGH-VALUES
              MOVE ODE-IB-OGG-NULL
                TO (SF)-IB-OGG-NULL
           ELSE
              MOVE ODE-IB-OGG
                TO (SF)-IB-OGG
           END-IF
           MOVE ODE-TP-DEROGA
             TO (SF)-TP-DEROGA
           MOVE ODE-COD-GR-AUT-APPRT
             TO (SF)-COD-GR-AUT-APPRT
           IF ODE-COD-LIV-AUT-APPRT-NULL = HIGH-VALUES
              MOVE ODE-COD-LIV-AUT-APPRT-NULL
                TO (SF)-COD-LIV-AUT-APPRT-NULL
           ELSE
              MOVE ODE-COD-LIV-AUT-APPRT
                TO (SF)-COD-LIV-AUT-APPRT
           END-IF
           MOVE ODE-COD-GR-AUT-SUP
             TO (SF)-COD-GR-AUT-SUP
           MOVE ODE-COD-LIV-AUT-SUP
             TO (SF)-COD-LIV-AUT-SUP
           MOVE ODE-DS-RIGA
             TO (SF)-DS-RIGA
           MOVE ODE-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE ODE-DS-VER
             TO (SF)-DS-VER
           MOVE ODE-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ
           MOVE ODE-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ
           MOVE ODE-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE ODE-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB.
       VALORIZZA-OUTPUT-ODE-EX.
           EXIT.

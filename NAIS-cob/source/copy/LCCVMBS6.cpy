      *----------------------------------------------------------------*
      *    COPY      ..... LCCVMBS6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO MOVIMENTI BATCH SOSPESI
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-MOVI-BATCH-SOSP.

      *--  TABELLA STORICA
           INITIALIZE MOVI-BATCH-SOSP.

      *--> NOME TABELLA FISICA DB
           MOVE 'MOVI-BATCH-SOSP'               TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WMBS-ST-INV(IX-TAB-MBS)
              AND WMBS-ELE-MOV-SOSP-MAX = 0

              MOVE WMOV-ID-PTF              TO MBS-ID-MOVI

              EVALUATE TRUE
      *-->    INSERT
                WHEN WMBS-ST-ADD(IX-TAB-MBS)
      *-->         ESTRAZIONE E VALORIZZAZIONE SEQUENCE

                   PERFORM ESTR-SEQUENCE
                      THRU ESTR-SEQUENCE-EX

                   IF IDSV0001-ESITO-OK
                      MOVE S090-SEQ-TABELLA
                        TO WMBS-ID-PTF(IX-TAB-MBS)
                           MBS-ID-MOVI-BATCH-SOSP
                           WMBS-ID-MOVI-BATCH-SOSP(IX-TAB-MBS)
                      MOVE WMBS-ID-OGG(IX-TAB-MBS)
                        TO MBS-ID-OGG
      *-->            TIPO OPERAZIONE DISPATCHER
                      SET  IDSI0011-TRATT-SENZA-STOR   TO TRUE
                      SET  IDSI0011-INSERT             TO TRUE
                      SET  IDSI0011-PRIMARY-KEY        TO TRUE

                   END-IF
      *-->    DELETE
                WHEN WMBS-ST-DEL(IX-TAB-MBS)
                   MOVE WMBS-ID-PTF(IX-TAB-MBS)
                     TO  MBS-ID-MOVI-BATCH-SOSP


      *-->         TIPO OPERAZIONE DISPATCHER
                   SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->    UPDATE
                WHEN WMBS-ST-MOD(IX-TAB-MBS)
                   MOVE WMBS-ID-PTF(IX-TAB-MBS)
                     TO  MBS-ID-MOVI-BATCH-SOSP

      *-->         TIPO OPERAZIONE DISPATCHER
                   SET  IDSI0011-TRATT-SENZA-STOR   TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN MOVIMENTI BATCH SOSPESI
                 PERFORM VAL-DCLGEN-MBS
                    THRU VAL-DCLGEN-MBS-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-MBS
                    THRU VALORIZZA-AREA-DSH-MBS-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-MOVI-BATCH-SOSP-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-MBS.

      *--> DCLGEN TABELLA
           MOVE MOVI-BATCH-SOSP            TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
      *    SET  IDSI0011-ID               TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
      *    SET  IDSI0011-TRATT-X-EFFETTO  TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-MBS-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVMBS5.

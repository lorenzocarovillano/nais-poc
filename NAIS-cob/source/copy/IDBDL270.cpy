           EXEC SQL DECLARE PREV TABLE
           (
             ID_PREV             DECIMAL(9, 0) NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             STAT_PREV           CHAR(2) NOT NULL,
             ID_OGG              DECIMAL(9, 0),
             TP_OGG              CHAR(2),
             IB_OGG              CHAR(40) NOT NULL,
             DS_OPER_SQL         CHAR(1) NOT NULL WITH DEFAULT,
             DS_VER              DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_UTENTE           CHAR(20) NOT NULL WITH DEFAULT,
             DS_STATO_ELAB       CHAR(1) NOT NULL WITH DEFAULT
          ) END-EXEC.

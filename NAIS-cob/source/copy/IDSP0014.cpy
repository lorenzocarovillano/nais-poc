
       Z700-DT-N-TO-X.

           MOVE WS-STR-DATE-N(1:4)
                TO WS-DATE-X(1:4)
           MOVE WS-STR-DATE-N(5:2)
                TO WS-DATE-X(6:2)
           MOVE WS-STR-DATE-N(7:2)
                TO WS-DATE-X(9:2)

           MOVE '-'
                TO WS-DATE-X(5:1)
                   WS-DATE-X(8:1).

       Z700-EX.
           EXIT.

       Z701-TS-N-TO-X.

           MOVE WS-STR-TIMESTAMP-N(1:4)
                TO WS-TIMESTAMP-X(1:4)
           MOVE WS-STR-TIMESTAMP-N(5:2)
                TO WS-TIMESTAMP-X(6:2)
           MOVE WS-STR-TIMESTAMP-N(7:2)
                TO WS-TIMESTAMP-X(9:2)
           MOVE WS-STR-TIMESTAMP-N(9:2)
                TO WS-TIMESTAMP-X(12:2)
           MOVE WS-STR-TIMESTAMP-N(11:2)
                TO WS-TIMESTAMP-X(15:2)
           MOVE WS-STR-TIMESTAMP-N(13:2)
                TO WS-TIMESTAMP-X(18:2)
           MOVE WS-STR-TIMESTAMP-N(15:4)
                TO WS-TIMESTAMP-X(21:4)
           MOVE '00'
                TO WS-TIMESTAMP-X(25:2)

           MOVE '-'
                TO WS-TIMESTAMP-X(5:1)
                   WS-TIMESTAMP-X(8:1)
           MOVE SPACE
                TO WS-TIMESTAMP-X(11:1)

           MOVE ':'
                TO WS-TIMESTAMP-X(14:1)
                   WS-TIMESTAMP-X(17:1)
           MOVE '.'
                TO WS-TIMESTAMP-X(20:1).
       Z701-EX.
           EXIT.


       Z800-DT-X-TO-N.

           IF IDSV0003-DB-ISO
              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
           ELSE
              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
           END-IF.

       Z800-EX.
           EXIT.

       Z810-DT-X-TO-N-ISO.

           MOVE WS-DATE-X(1:4)
                   TO WS-STR-DATE-N(1:4)
           MOVE WS-DATE-X(6:2)
                   TO WS-STR-DATE-N(5:2)
           MOVE WS-DATE-X(9:2)
                   TO WS-STR-DATE-N(7:2).

       Z810-EX.
           EXIT.

       Z820-DT-X-TO-N-EUR.

           MOVE WS-DATE-X(1:2)
                   TO WS-STR-DATE-N(7:2)
           MOVE WS-DATE-X(4:2)
                   TO WS-STR-DATE-N(5:2)
           MOVE WS-DATE-X(7:4)
                   TO WS-STR-DATE-N(1:4).

       Z820-EX.
           EXIT.

       Z801-TS-X-TO-N.

           MOVE WS-TIMESTAMP-X(1:4)
                TO WS-STR-TIMESTAMP-N(1:4)
           MOVE WS-TIMESTAMP-X(6:2)
                TO WS-STR-TIMESTAMP-N(5:2)
           MOVE WS-TIMESTAMP-X(9:2)
                TO WS-STR-TIMESTAMP-N(7:2)
           MOVE WS-TIMESTAMP-X(12:2)
                TO WS-STR-TIMESTAMP-N(9:2)
           MOVE WS-TIMESTAMP-X(15:2)
                TO WS-STR-TIMESTAMP-N(11:2)
           MOVE WS-TIMESTAMP-X(18:2)
                TO WS-STR-TIMESTAMP-N(13:2)
           MOVE WS-TIMESTAMP-X(21:4)
                TO WS-STR-TIMESTAMP-N(15:4).

       Z801-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVDTC4
      *   ULTIMO AGG. 28 NOV 2014
      *------------------------------------------------------------

       INIZIA-TOT-DTC.

           PERFORM INIZIA-ZEROES-DTC THRU INIZIA-ZEROES-DTC-EX

           PERFORM INIZIA-SPACES-DTC THRU INIZIA-SPACES-DTC-EX

           PERFORM INIZIA-NULL-DTC THRU INIZIA-NULL-DTC-EX.

       INIZIA-TOT-DTC-EX.
           EXIT.

       INIZIA-NULL-DTC.
           MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-DT-INI-COP-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-DT-END-COP-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-PRE-NET-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-INTR-FRAZ-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-INTR-MORA-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-INTR-RETDT-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-INTR-RIAT-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-DIR-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-SPE-MED-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-TAX-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-SOPR-SAN-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-SOPR-SPO-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-SOPR-TEC-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-SOPR-PROF-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-SOPR-ALT-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-PRE-TOT-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-PRE-PP-IAS-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-PRE-SOLO-RSH-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-CAR-ACQ-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-CAR-GEST-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-CAR-INC-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-PROV-ACQ-1AA-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-PROV-ACQ-2AA-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-PROV-RICOR-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-PROV-INC-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-PROV-DA-REC-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-COD-DVS-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-FRQ-MOVI-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-COD-TARI-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-IMP-AZ-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-IMP-ADER-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-IMP-TFR-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-IMP-VOLO-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-MANFEE-ANTIC-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-MANFEE-RICOR-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-MANFEE-REC-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-DT-ESI-TIT-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-SPE-AGE-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-CAR-IAS-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-IMP-TRASFE-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-NUM-GG-RITARDO-PAG-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-NUM-GG-RIVAL-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-ACQ-EXP-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-REMUN-ASS-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-COMMIS-INTER-NULL(IX-TAB-DTC)
           MOVE HIGH-VALUES TO (SF)-CNBT-ANTIRAC-NULL(IX-TAB-DTC).
       INIZIA-NULL-DTC-EX.
           EXIT.

       INIZIA-ZEROES-DTC.
           MOVE 0 TO (SF)-ID-DETT-TIT-CONT(IX-TAB-DTC)
           MOVE 0 TO (SF)-ID-TIT-CONT(IX-TAB-DTC)
           MOVE 0 TO (SF)-ID-OGG(IX-TAB-DTC)
           MOVE 0 TO (SF)-ID-MOVI-CRZ(IX-TAB-DTC)
           MOVE 0 TO (SF)-DT-INI-EFF(IX-TAB-DTC)
           MOVE 0 TO (SF)-DT-END-EFF(IX-TAB-DTC)
           MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-DTC)
           MOVE 0 TO (SF)-DS-RIGA(IX-TAB-DTC)
           MOVE 0 TO (SF)-DS-VER(IX-TAB-DTC)
           MOVE 0 TO (SF)-DS-TS-INI-CPTZ(IX-TAB-DTC)
           MOVE 0 TO (SF)-DS-TS-END-CPTZ(IX-TAB-DTC).
       INIZIA-ZEROES-DTC-EX.
           EXIT.

       INIZIA-SPACES-DTC.
           MOVE SPACES TO (SF)-TP-OGG(IX-TAB-DTC)
           MOVE SPACES TO (SF)-TP-RGM-FISC(IX-TAB-DTC)
           MOVE SPACES TO (SF)-TP-STAT-TIT(IX-TAB-DTC)
           MOVE SPACES TO (SF)-DS-OPER-SQL(IX-TAB-DTC)
           MOVE SPACES TO (SF)-DS-UTENTE(IX-TAB-DTC)
           MOVE SPACES TO (SF)-DS-STATO-ELAB(IX-TAB-DTC).
       INIZIA-SPACES-DTC-EX.
           EXIT.

       01 LDBV6001.
         05 LDBV6001-ANN-DS-TS-CPTZ       PIC S9(18)V     COMP-3.
         05 LDBV6001-TP-VAL-AST-1         PIC X(02).
         05 LDBV6001-TP-VAL-AST-2         PIC X(02).
         05 LDBV6001-TP-VAL-AST-3         PIC X(02).
         05 LDBV6001-TP-VAL-AST-4         PIC X(02).
         05 LDBV6001-TP-VAL-AST-5         PIC X(02).
         05 LDBV6001-TP-VAL-AST-6         PIC X(02).
         05 LDBV6001-TP-VAL-AST-7         PIC X(02).
         05 LDBV6001-TP-VAL-AST-8         PIC X(02).
         05 LDBV6001-TP-VAL-AST-9         PIC X(02).
         05 LDBV6001-TP-VAL-AST-10        PIC X(02).
         05 LDBV6001-ID-TRCH-DI-GAR       PIC S9(9)V     COMP-3.

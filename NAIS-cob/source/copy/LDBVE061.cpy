       01 LDBVE061.
          03 LDBVE061-ID-OGG              PIC S9(09)  COMP-3.
          03 LDBVE061-TP-OGG              PIC  X(02).
          03 LDBVE061-TP-MOVI             PIC S9(05)  COMP-3.
          03 LDBVE061-DT-EFF-I            PIC S9(08)V COMP-3.
          03 LDBVE061-DT-EFF-O            PIC S9(08)V COMP-3.
          03 LDBVE061-DS-CPTZ-O           PIC S9(18)V COMP-3.

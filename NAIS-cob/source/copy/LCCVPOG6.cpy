      *----------------------------------------------------------------*
      *    COPY      ..... LCCVPOG6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO PARAMETRO OGGETTO
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVPOG5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCVPOG1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-PARAM-OGG.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE PARAM-OGG

      *--> NOME TABELLA FISICA DB
           MOVE 'PARAM-OGG'                  TO   WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF  NOT WPOG-ST-INV(IX-TAB-POG)
           AND NOT WPOG-ST-CON(IX-TAB-POG)
           AND     WPOG-ELE-PARAM-OGG-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WPOG-ST-ADD(IX-TAB-POG)

                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK

                          MOVE S090-SEQ-TABELLA
                            TO WPOG-ID-PTF(IX-TAB-POG)
                               POG-ID-PARAM-OGG

                          PERFORM PREPARA-AREA-LCCS0234-POG
                             THRU PREPARA-AREA-LCCS0234-POG-EX
                          PERFORM CALL-LCCS0234
                             THRU CALL-LCCS0234-EX

                          IF IDSV0001-ESITO-OK
                             MOVE S234-ID-OGG-PTF-EOC
                               TO POG-ID-OGG
                             MOVE WMOV-ID-PTF
                               TO POG-ID-MOVI-CRZ
                          END-IF
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WPOG-ST-MOD(IX-TAB-POG)
                       MOVE WPOG-ID-PTF(IX-TAB-POG)
                         TO POG-ID-PARAM-OGG
                       MOVE WPOG-ID-OGG(IX-TAB-POG)
                         TO POG-ID-OGG
                       MOVE WMOV-ID-PTF
                         TO POG-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WPOG-ST-DEL(IX-TAB-POG)

                       MOVE WPOG-ID-PTF(IX-TAB-POG)
                         TO POG-ID-PARAM-OGG
                       MOVE WPOG-ID-OGG(IX-TAB-POG)
                         TO POG-ID-OGG
                       MOVE WMOV-ID-PTF
                         TO POG-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

                  END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN ADESIONE
                 PERFORM VAL-DCLGEN-POG
                    THRU VAL-DCLGEN-POG-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-POG
                    THRU VALORIZZA-AREA-DSH-POG-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-PARAM-OGG-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    PREPARA AREA PER CALL LCCS0234
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234-POG.

           MOVE WPOG-ID-OGG(IX-TAB-POG)     TO S234-ID-OGG-EOC.
           MOVE WPOG-TP-OGG(IX-TAB-POG)     TO S234-TIPO-OGG-EOC.

       PREPARA-AREA-LCCS0234-POG-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-POG.

      *--> DCLGEN TABELLA
           MOVE PARAM-OGG               TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-POG-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVPOG5.

      *----------------------------------------------------------------*
      *    COPY      ..... LCCVP016
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO RICHIESTA ESTERNA
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVP015 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN RICHIESTA ESTERNA (LCCVP011)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-RICH-EST.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE RICH-EST.

      *--> NOME TABELLA FISICA DB
           MOVE 'RICH-EST'                    TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WP01-ST-INV
              AND WP01-ELE-RICH-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WP01-ST-ADD

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK

                        MOVE S090-SEQ-TABELLA      TO WP01-ID-PTF
                        MOVE WP01-ID-PTF           TO P01-ID-RICH-EST
                                                      WP01-ID-RICH-EST

                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-INSERT          TO TRUE

      *-->        MODIFICA
                  WHEN WP01-ST-MOD

                     MOVE WP01-ID-RICH-EST         TO P01-ID-RICH-EST

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-UPDATE          TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WP01-ST-DEL

                     MOVE WP01-ID-RICH-EST         TO P01-ID-RICH-EST

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-DELETE          TO TRUE

           END-EVALUATE

           IF IDSV0001-ESITO-OK

      *-->    VALORIZZA DCLGEN RICHIESTA ESTERNA
              PERFORM VAL-DCLGEN-P01
                 THRU VAL-DCLGEN-P01-EX

      *-->    VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
              PERFORM VALORIZZA-AREA-DSH-P01
                 THRU VALORIZZA-AREA-DSH-P01-EX

      *-->    CALL DISPATCHER PER AGGIORNAMENTO
              PERFORM AGGIORNA-TABELLA
                 THRU AGGIORNA-TABELLA-EX

           END-IF.

       AGGIORNA-RICH-EST-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-P01.

      *--> DCLGEN TABELLA
           MOVE RICH-EST                 TO IDSI0011-BUFFER-DATI.

           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-PRIMARY-KEY      TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-P01-EX.
           EXIT.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA DFLT_ADES
      *   ALIAS DAD
      *   ULTIMO AGG. 02 SET 2008
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-DFLT-ADES PIC S9(9)     COMP-3.
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-IB-POLI PIC X(40).
             07 (SF)-IB-POLI-NULL REDEFINES
                (SF)-IB-POLI   PIC X(40).
             07 (SF)-IB-DFLT PIC X(40).
             07 (SF)-IB-DFLT-NULL REDEFINES
                (SF)-IB-DFLT   PIC X(40).
             07 (SF)-COD-GAR-1 PIC X(12).
             07 (SF)-COD-GAR-1-NULL REDEFINES
                (SF)-COD-GAR-1   PIC X(12).
             07 (SF)-COD-GAR-2 PIC X(12).
             07 (SF)-COD-GAR-2-NULL REDEFINES
                (SF)-COD-GAR-2   PIC X(12).
             07 (SF)-COD-GAR-3 PIC X(12).
             07 (SF)-COD-GAR-3-NULL REDEFINES
                (SF)-COD-GAR-3   PIC X(12).
             07 (SF)-COD-GAR-4 PIC X(12).
             07 (SF)-COD-GAR-4-NULL REDEFINES
                (SF)-COD-GAR-4   PIC X(12).
             07 (SF)-COD-GAR-5 PIC X(12).
             07 (SF)-COD-GAR-5-NULL REDEFINES
                (SF)-COD-GAR-5   PIC X(12).
             07 (SF)-COD-GAR-6 PIC X(12).
             07 (SF)-COD-GAR-6-NULL REDEFINES
                (SF)-COD-GAR-6   PIC X(12).
             07 (SF)-DT-DECOR-DFLT   PIC S9(8) COMP-3.
             07 (SF)-DT-DECOR-DFLT-NULL REDEFINES
                (SF)-DT-DECOR-DFLT   PIC X(5).
             07 (SF)-ETA-SCAD-MASC-DFLT PIC S9(5)     COMP-3.
             07 (SF)-ETA-SCAD-MASC-DFLT-NULL REDEFINES
                (SF)-ETA-SCAD-MASC-DFLT   PIC X(3).
             07 (SF)-ETA-SCAD-FEMM-DFLT PIC S9(5)     COMP-3.
             07 (SF)-ETA-SCAD-FEMM-DFLT-NULL REDEFINES
                (SF)-ETA-SCAD-FEMM-DFLT   PIC X(3).
             07 (SF)-DUR-AA-ADES-DFLT PIC S9(5)     COMP-3.
             07 (SF)-DUR-AA-ADES-DFLT-NULL REDEFINES
                (SF)-DUR-AA-ADES-DFLT   PIC X(3).
             07 (SF)-DUR-MM-ADES-DFLT PIC S9(5)     COMP-3.
             07 (SF)-DUR-MM-ADES-DFLT-NULL REDEFINES
                (SF)-DUR-MM-ADES-DFLT   PIC X(3).
             07 (SF)-DUR-GG-ADES-DFLT PIC S9(5)     COMP-3.
             07 (SF)-DUR-GG-ADES-DFLT-NULL REDEFINES
                (SF)-DUR-GG-ADES-DFLT   PIC X(3).
             07 (SF)-DT-SCAD-ADES-DFLT   PIC S9(8) COMP-3.
             07 (SF)-DT-SCAD-ADES-DFLT-NULL REDEFINES
                (SF)-DT-SCAD-ADES-DFLT   PIC X(5).
             07 (SF)-FRAZ-DFLT PIC S9(5)     COMP-3.
             07 (SF)-FRAZ-DFLT-NULL REDEFINES
                (SF)-FRAZ-DFLT   PIC X(3).
             07 (SF)-PC-PROV-INC-DFLT PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-PROV-INC-DFLT-NULL REDEFINES
                (SF)-PC-PROV-INC-DFLT   PIC X(4).
             07 (SF)-IMP-PROV-INC-DFLT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-PROV-INC-DFLT-NULL REDEFINES
                (SF)-IMP-PROV-INC-DFLT   PIC X(8).
             07 (SF)-IMP-AZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-AZ-NULL REDEFINES
                (SF)-IMP-AZ   PIC X(8).
             07 (SF)-IMP-ADER PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-ADER-NULL REDEFINES
                (SF)-IMP-ADER   PIC X(8).
             07 (SF)-IMP-TFR PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-TFR-NULL REDEFINES
                (SF)-IMP-TFR   PIC X(8).
             07 (SF)-IMP-VOLO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-VOLO-NULL REDEFINES
                (SF)-IMP-VOLO   PIC X(8).
             07 (SF)-PC-AZ PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-AZ-NULL REDEFINES
                (SF)-PC-AZ   PIC X(4).
             07 (SF)-PC-ADER PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-ADER-NULL REDEFINES
                (SF)-PC-ADER   PIC X(4).
             07 (SF)-PC-TFR PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-TFR-NULL REDEFINES
                (SF)-PC-TFR   PIC X(4).
             07 (SF)-PC-VOLO PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-VOLO-NULL REDEFINES
                (SF)-PC-VOLO   PIC X(4).
             07 (SF)-TP-FNT-CNBTVA PIC X(2).
             07 (SF)-TP-FNT-CNBTVA-NULL REDEFINES
                (SF)-TP-FNT-CNBTVA   PIC X(2).
             07 (SF)-IMP-PRE-DFLT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-PRE-DFLT-NULL REDEFINES
                (SF)-IMP-PRE-DFLT   PIC X(8).
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-TP-PRE PIC X(1).
             07 (SF)-TP-PRE-NULL REDEFINES
                (SF)-TP-PRE   PIC X(1).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).

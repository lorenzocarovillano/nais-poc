
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVPOG3
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-POG.
           MOVE POG-ID-PARAM-OGG
             TO (SF)-ID-PTF(IX-TAB-POG)
           MOVE POG-ID-PARAM-OGG
             TO (SF)-ID-PARAM-OGG(IX-TAB-POG)
           MOVE POG-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-POG)
           MOVE POG-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-POG)
           MOVE POG-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-POG)
           IF POG-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE POG-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-POG)
           ELSE
              MOVE POG-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-POG)
           END-IF
           MOVE POG-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-POG)
           MOVE POG-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-POG)
           MOVE POG-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-POG)
           IF POG-COD-PARAM-NULL = HIGH-VALUES
              MOVE POG-COD-PARAM-NULL
                TO (SF)-COD-PARAM-NULL(IX-TAB-POG)
           ELSE
              MOVE POG-COD-PARAM
                TO (SF)-COD-PARAM(IX-TAB-POG)
           END-IF
           IF POG-TP-PARAM-NULL = HIGH-VALUES
              MOVE POG-TP-PARAM-NULL
                TO (SF)-TP-PARAM-NULL(IX-TAB-POG)
           ELSE
              MOVE POG-TP-PARAM
                TO (SF)-TP-PARAM(IX-TAB-POG)
           END-IF
           IF POG-TP-D-NULL = HIGH-VALUES
              MOVE POG-TP-D-NULL
                TO (SF)-TP-D-NULL(IX-TAB-POG)
           ELSE
              MOVE POG-TP-D
                TO (SF)-TP-D(IX-TAB-POG)
           END-IF
           IF POG-VAL-IMP-NULL = HIGH-VALUES
              MOVE POG-VAL-IMP-NULL
                TO (SF)-VAL-IMP-NULL(IX-TAB-POG)
           ELSE
              MOVE POG-VAL-IMP
                TO (SF)-VAL-IMP(IX-TAB-POG)
           END-IF
           IF POG-VAL-DT-NULL = HIGH-VALUES
              MOVE POG-VAL-DT-NULL
                TO (SF)-VAL-DT-NULL(IX-TAB-POG)
           ELSE
              MOVE POG-VAL-DT
                TO (SF)-VAL-DT(IX-TAB-POG)
           END-IF
           IF POG-VAL-TS-NULL = HIGH-VALUES
              MOVE POG-VAL-TS-NULL
                TO (SF)-VAL-TS-NULL(IX-TAB-POG)
           ELSE
              MOVE POG-VAL-TS
                TO (SF)-VAL-TS(IX-TAB-POG)
           END-IF
           MOVE POG-VAL-TXT
             TO (SF)-VAL-TXT(IX-TAB-POG)
           IF POG-VAL-FL-NULL = HIGH-VALUES
              MOVE POG-VAL-FL-NULL
                TO (SF)-VAL-FL-NULL(IX-TAB-POG)
           ELSE
              MOVE POG-VAL-FL
                TO (SF)-VAL-FL(IX-TAB-POG)
           END-IF
           IF POG-VAL-NUM-NULL = HIGH-VALUES
              MOVE POG-VAL-NUM-NULL
                TO (SF)-VAL-NUM-NULL(IX-TAB-POG)
           ELSE
              MOVE POG-VAL-NUM
                TO (SF)-VAL-NUM(IX-TAB-POG)
           END-IF
           IF POG-VAL-PC-NULL = HIGH-VALUES
              MOVE POG-VAL-PC-NULL
                TO (SF)-VAL-PC-NULL(IX-TAB-POG)
           ELSE
              MOVE POG-VAL-PC
                TO (SF)-VAL-PC(IX-TAB-POG)
           END-IF
           MOVE POG-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-POG)
           MOVE POG-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-POG)
           MOVE POG-DS-VER
             TO (SF)-DS-VER(IX-TAB-POG)
           MOVE POG-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-POG)
           MOVE POG-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-POG)
           MOVE POG-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-POG)
           MOVE POG-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-POG).
       VALORIZZA-OUTPUT-POG-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY      ..... LCCP0001
      *    TIPOLOGIA...... COPY PROCEDURE (COMPONENTI COMUNI)
      *    DESCRIZIONE.... AGGIORNAMENTO TABELLA
      *----------------------------------------------------------------*

      *----------------------------------------------------------------*
      *    AGGIORNAMENTO TABELLA
      *----------------------------------------------------------------*
       AGGIORNA-TABELLA.

      *--> NOME TABELLA FISICA DB
           MOVE WK-TABELLA               TO IDSI0011-CODICE-STR-DATO.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
      *-->      OPERAZIONE ESEGUITA CORRETTAMENTE
                WHEN IDSO0011-SUCCESSFUL-SQL
                   CONTINUE

      *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
                WHEN OTHER
                   MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
                   MOVE 'AGGIORNA-TABELLA'
                                         TO IEAI9901-LABEL-ERR
                   MOVE '005016'         TO IEAI9901-COD-ERRORE
                   STRING WK-TABELLA            ';'
                          IDSO0011-RETURN-CODE  ';'
                          IDSO0011-SQLCODE
                   DELIMITED BY SIZE   INTO IEAI9901-PARAMETRI-ERR
                   END-STRING
                   PERFORM S0300-RICERCA-GRAVITA-ERRORE
                      THRU EX-S0300
              END-EVALUATE

           ELSE

      *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
              MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'AGGIORNA-TABELLA'
                                         TO IEAI9901-LABEL-ERR
              MOVE '005016'              TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA            ';'
                     IDSO0011-RETURN-CODE  ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE        INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300

           END-IF.

       AGGIORNA-TABELLA-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVCLT5
      *   ULTIMO AGG. 28 GEN 2009
      *------------------------------------------------------------

       VAL-DCLGEN-CLT.
           MOVE (SF)-TP-OGG(IX-TAB-CLT)
              TO CLT-TP-OGG
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-CLT) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-CLT)
              TO CLT-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-CLT)
              TO CLT-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-CLT) NOT NUMERIC
              MOVE 0 TO CLT-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-CLT)
              TO CLT-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-CLT) NOT NUMERIC
              MOVE 0 TO CLT-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-CLT)
              TO CLT-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-CLT)
              TO CLT-COD-COMP-ANIA
           IF (SF)-TP-CLAU-NULL(IX-TAB-CLT) = HIGH-VALUES
              MOVE (SF)-TP-CLAU-NULL(IX-TAB-CLT)
              TO CLT-TP-CLAU-NULL
           ELSE
              MOVE (SF)-TP-CLAU(IX-TAB-CLT)
              TO CLT-TP-CLAU
           END-IF
           IF (SF)-COD-CLAU-NULL(IX-TAB-CLT) = HIGH-VALUES
              MOVE (SF)-COD-CLAU-NULL(IX-TAB-CLT)
              TO CLT-COD-CLAU-NULL
           ELSE
              MOVE (SF)-COD-CLAU(IX-TAB-CLT)
              TO CLT-COD-CLAU
           END-IF
           IF (SF)-DESC-BREVE-NULL(IX-TAB-CLT) = HIGH-VALUES
              MOVE (SF)-DESC-BREVE-NULL(IX-TAB-CLT)
              TO CLT-DESC-BREVE-NULL
           ELSE
              MOVE (SF)-DESC-BREVE(IX-TAB-CLT)
              TO CLT-DESC-BREVE
           END-IF
           MOVE (SF)-DESC-LNG(IX-TAB-CLT)
              TO CLT-DESC-LNG
           IF (SF)-DS-RIGA(IX-TAB-CLT) NOT NUMERIC
              MOVE 0 TO CLT-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-CLT)
              TO CLT-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-CLT)
              TO CLT-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-CLT) NOT NUMERIC
              MOVE 0 TO CLT-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-CLT)
              TO CLT-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-CLT) NOT NUMERIC
              MOVE 0 TO CLT-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-CLT)
              TO CLT-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-CLT) NOT NUMERIC
              MOVE 0 TO CLT-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-CLT)
              TO CLT-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-CLT)
              TO CLT-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-CLT)
              TO CLT-DS-STATO-ELAB.
       VAL-DCLGEN-CLT-EX.
           EXIT.

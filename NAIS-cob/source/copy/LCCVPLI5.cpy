
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVPLI5
      *   ULTIMO AGG. 07 GEN 2016
      *------------------------------------------------------------

       VAL-DCLGEN-PLI.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PLI) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PLI)
              TO PLI-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-PLI)
              TO PLI-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-PLI) NOT NUMERIC
              MOVE 0 TO PLI-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-PLI)
              TO PLI-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-PLI) NOT NUMERIC
              MOVE 0 TO PLI-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-PLI)
              TO PLI-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-PLI)
              TO PLI-COD-COMP-ANIA
           IF (SF)-PC-LIQ-NULL(IX-TAB-PLI) = HIGH-VALUES
              MOVE (SF)-PC-LIQ-NULL(IX-TAB-PLI)
              TO PLI-PC-LIQ-NULL
           ELSE
              MOVE (SF)-PC-LIQ(IX-TAB-PLI)
              TO PLI-PC-LIQ
           END-IF
           IF (SF)-IMP-LIQ-NULL(IX-TAB-PLI) = HIGH-VALUES
              MOVE (SF)-IMP-LIQ-NULL(IX-TAB-PLI)
              TO PLI-IMP-LIQ-NULL
           ELSE
              MOVE (SF)-IMP-LIQ(IX-TAB-PLI)
              TO PLI-IMP-LIQ
           END-IF
           IF (SF)-TP-MEZ-PAG-NULL(IX-TAB-PLI) = HIGH-VALUES
              MOVE (SF)-TP-MEZ-PAG-NULL(IX-TAB-PLI)
              TO PLI-TP-MEZ-PAG-NULL
           ELSE
              MOVE (SF)-TP-MEZ-PAG(IX-TAB-PLI)
              TO PLI-TP-MEZ-PAG
           END-IF
           IF (SF)-ITER-PAG-AVV-NULL(IX-TAB-PLI) = HIGH-VALUES
              MOVE (SF)-ITER-PAG-AVV-NULL(IX-TAB-PLI)
              TO PLI-ITER-PAG-AVV-NULL
           ELSE
              MOVE (SF)-ITER-PAG-AVV(IX-TAB-PLI)
              TO PLI-ITER-PAG-AVV
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-PLI) NOT NUMERIC
              MOVE 0 TO PLI-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-PLI)
              TO PLI-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-PLI)
              TO PLI-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-PLI) NOT NUMERIC
              MOVE 0 TO PLI-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-PLI)
              TO PLI-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-PLI) NOT NUMERIC
              MOVE 0 TO PLI-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-PLI)
              TO PLI-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-PLI) NOT NUMERIC
              MOVE 0 TO PLI-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-PLI)
              TO PLI-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-PLI)
              TO PLI-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-PLI)
              TO PLI-DS-STATO-ELAB
           IF (SF)-DT-VLT-NULL(IX-TAB-PLI) = HIGH-VALUES
              MOVE (SF)-DT-VLT-NULL(IX-TAB-PLI)
              TO PLI-DT-VLT-NULL
           ELSE
             IF (SF)-DT-VLT(IX-TAB-PLI) = ZERO
                MOVE HIGH-VALUES
                TO PLI-DT-VLT-NULL
             ELSE
              MOVE (SF)-DT-VLT(IX-TAB-PLI)
              TO PLI-DT-VLT
             END-IF
           END-IF
           MOVE (SF)-INT-CNT-CORR-ACCR(IX-TAB-PLI)
              TO PLI-INT-CNT-CORR-ACCR
           IF (SF)-COD-IBAN-RIT-CON-NULL(IX-TAB-PLI) = HIGH-VALUES
              MOVE (SF)-COD-IBAN-RIT-CON-NULL(IX-TAB-PLI)
              TO PLI-COD-IBAN-RIT-CON-NULL
           ELSE
              MOVE (SF)-COD-IBAN-RIT-CON(IX-TAB-PLI)
              TO PLI-COD-IBAN-RIT-CON
           END-IF.
       VAL-DCLGEN-PLI-EX.
           EXIT.

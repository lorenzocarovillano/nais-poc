
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVDAD3
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-DAD.
           MOVE DAD-ID-DFLT-ADES
             TO (SF)-ID-PTF
           MOVE DAD-ID-DFLT-ADES
             TO (SF)-ID-DFLT-ADES
           MOVE DAD-ID-POLI
             TO (SF)-ID-POLI
           IF DAD-IB-POLI-NULL = HIGH-VALUES
              MOVE DAD-IB-POLI-NULL
                TO (SF)-IB-POLI-NULL
           ELSE
              MOVE DAD-IB-POLI
                TO (SF)-IB-POLI
           END-IF
           IF DAD-IB-DFLT-NULL = HIGH-VALUES
              MOVE DAD-IB-DFLT-NULL
                TO (SF)-IB-DFLT-NULL
           ELSE
              MOVE DAD-IB-DFLT
                TO (SF)-IB-DFLT
           END-IF
           IF DAD-COD-GAR-1-NULL = HIGH-VALUES
              MOVE DAD-COD-GAR-1-NULL
                TO (SF)-COD-GAR-1-NULL
           ELSE
              MOVE DAD-COD-GAR-1
                TO (SF)-COD-GAR-1
           END-IF
           IF DAD-COD-GAR-2-NULL = HIGH-VALUES
              MOVE DAD-COD-GAR-2-NULL
                TO (SF)-COD-GAR-2-NULL
           ELSE
              MOVE DAD-COD-GAR-2
                TO (SF)-COD-GAR-2
           END-IF
           IF DAD-COD-GAR-3-NULL = HIGH-VALUES
              MOVE DAD-COD-GAR-3-NULL
                TO (SF)-COD-GAR-3-NULL
           ELSE
              MOVE DAD-COD-GAR-3
                TO (SF)-COD-GAR-3
           END-IF
           IF DAD-COD-GAR-4-NULL = HIGH-VALUES
              MOVE DAD-COD-GAR-4-NULL
                TO (SF)-COD-GAR-4-NULL
           ELSE
              MOVE DAD-COD-GAR-4
                TO (SF)-COD-GAR-4
           END-IF
           IF DAD-COD-GAR-5-NULL = HIGH-VALUES
              MOVE DAD-COD-GAR-5-NULL
                TO (SF)-COD-GAR-5-NULL
           ELSE
              MOVE DAD-COD-GAR-5
                TO (SF)-COD-GAR-5
           END-IF
           IF DAD-COD-GAR-6-NULL = HIGH-VALUES
              MOVE DAD-COD-GAR-6-NULL
                TO (SF)-COD-GAR-6-NULL
           ELSE
              MOVE DAD-COD-GAR-6
                TO (SF)-COD-GAR-6
           END-IF
           IF DAD-DT-DECOR-DFLT-NULL = HIGH-VALUES
              MOVE DAD-DT-DECOR-DFLT-NULL
                TO (SF)-DT-DECOR-DFLT-NULL
           ELSE
              MOVE DAD-DT-DECOR-DFLT
                TO (SF)-DT-DECOR-DFLT
           END-IF
           IF DAD-ETA-SCAD-MASC-DFLT-NULL = HIGH-VALUES
              MOVE DAD-ETA-SCAD-MASC-DFLT-NULL
                TO (SF)-ETA-SCAD-MASC-DFLT-NULL
           ELSE
              MOVE DAD-ETA-SCAD-MASC-DFLT
                TO (SF)-ETA-SCAD-MASC-DFLT
           END-IF
           IF DAD-ETA-SCAD-FEMM-DFLT-NULL = HIGH-VALUES
              MOVE DAD-ETA-SCAD-FEMM-DFLT-NULL
                TO (SF)-ETA-SCAD-FEMM-DFLT-NULL
           ELSE
              MOVE DAD-ETA-SCAD-FEMM-DFLT
                TO (SF)-ETA-SCAD-FEMM-DFLT
           END-IF
           IF DAD-DUR-AA-ADES-DFLT-NULL = HIGH-VALUES
              MOVE DAD-DUR-AA-ADES-DFLT-NULL
                TO (SF)-DUR-AA-ADES-DFLT-NULL
           ELSE
              MOVE DAD-DUR-AA-ADES-DFLT
                TO (SF)-DUR-AA-ADES-DFLT
           END-IF
           IF DAD-DUR-MM-ADES-DFLT-NULL = HIGH-VALUES
              MOVE DAD-DUR-MM-ADES-DFLT-NULL
                TO (SF)-DUR-MM-ADES-DFLT-NULL
           ELSE
              MOVE DAD-DUR-MM-ADES-DFLT
                TO (SF)-DUR-MM-ADES-DFLT
           END-IF
           IF DAD-DUR-GG-ADES-DFLT-NULL = HIGH-VALUES
              MOVE DAD-DUR-GG-ADES-DFLT-NULL
                TO (SF)-DUR-GG-ADES-DFLT-NULL
           ELSE
              MOVE DAD-DUR-GG-ADES-DFLT
                TO (SF)-DUR-GG-ADES-DFLT
           END-IF
           IF DAD-DT-SCAD-ADES-DFLT-NULL = HIGH-VALUES
              MOVE DAD-DT-SCAD-ADES-DFLT-NULL
                TO (SF)-DT-SCAD-ADES-DFLT-NULL
           ELSE
              MOVE DAD-DT-SCAD-ADES-DFLT
                TO (SF)-DT-SCAD-ADES-DFLT
           END-IF
           IF DAD-FRAZ-DFLT-NULL = HIGH-VALUES
              MOVE DAD-FRAZ-DFLT-NULL
                TO (SF)-FRAZ-DFLT-NULL
           ELSE
              MOVE DAD-FRAZ-DFLT
                TO (SF)-FRAZ-DFLT
           END-IF
           IF DAD-PC-PROV-INC-DFLT-NULL = HIGH-VALUES
              MOVE DAD-PC-PROV-INC-DFLT-NULL
                TO (SF)-PC-PROV-INC-DFLT-NULL
           ELSE
              MOVE DAD-PC-PROV-INC-DFLT
                TO (SF)-PC-PROV-INC-DFLT
           END-IF
           IF DAD-IMP-PROV-INC-DFLT-NULL = HIGH-VALUES
              MOVE DAD-IMP-PROV-INC-DFLT-NULL
                TO (SF)-IMP-PROV-INC-DFLT-NULL
           ELSE
              MOVE DAD-IMP-PROV-INC-DFLT
                TO (SF)-IMP-PROV-INC-DFLT
           END-IF
           IF DAD-IMP-AZ-NULL = HIGH-VALUES
              MOVE DAD-IMP-AZ-NULL
                TO (SF)-IMP-AZ-NULL
           ELSE
              MOVE DAD-IMP-AZ
                TO (SF)-IMP-AZ
           END-IF
           IF DAD-IMP-ADER-NULL = HIGH-VALUES
              MOVE DAD-IMP-ADER-NULL
                TO (SF)-IMP-ADER-NULL
           ELSE
              MOVE DAD-IMP-ADER
                TO (SF)-IMP-ADER
           END-IF
           IF DAD-IMP-TFR-NULL = HIGH-VALUES
              MOVE DAD-IMP-TFR-NULL
                TO (SF)-IMP-TFR-NULL
           ELSE
              MOVE DAD-IMP-TFR
                TO (SF)-IMP-TFR
           END-IF
           IF DAD-IMP-VOLO-NULL = HIGH-VALUES
              MOVE DAD-IMP-VOLO-NULL
                TO (SF)-IMP-VOLO-NULL
           ELSE
              MOVE DAD-IMP-VOLO
                TO (SF)-IMP-VOLO
           END-IF
           IF DAD-PC-AZ-NULL = HIGH-VALUES
              MOVE DAD-PC-AZ-NULL
                TO (SF)-PC-AZ-NULL
           ELSE
              MOVE DAD-PC-AZ
                TO (SF)-PC-AZ
           END-IF
           IF DAD-PC-ADER-NULL = HIGH-VALUES
              MOVE DAD-PC-ADER-NULL
                TO (SF)-PC-ADER-NULL
           ELSE
              MOVE DAD-PC-ADER
                TO (SF)-PC-ADER
           END-IF
           IF DAD-PC-TFR-NULL = HIGH-VALUES
              MOVE DAD-PC-TFR-NULL
                TO (SF)-PC-TFR-NULL
           ELSE
              MOVE DAD-PC-TFR
                TO (SF)-PC-TFR
           END-IF
           IF DAD-PC-VOLO-NULL = HIGH-VALUES
              MOVE DAD-PC-VOLO-NULL
                TO (SF)-PC-VOLO-NULL
           ELSE
              MOVE DAD-PC-VOLO
                TO (SF)-PC-VOLO
           END-IF
           IF DAD-TP-FNT-CNBTVA-NULL = HIGH-VALUES
              MOVE DAD-TP-FNT-CNBTVA-NULL
                TO (SF)-TP-FNT-CNBTVA-NULL
           ELSE
              MOVE DAD-TP-FNT-CNBTVA
                TO (SF)-TP-FNT-CNBTVA
           END-IF
           IF DAD-IMP-PRE-DFLT-NULL = HIGH-VALUES
              MOVE DAD-IMP-PRE-DFLT-NULL
                TO (SF)-IMP-PRE-DFLT-NULL
           ELSE
              MOVE DAD-IMP-PRE-DFLT
                TO (SF)-IMP-PRE-DFLT
           END-IF
           MOVE DAD-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           IF DAD-TP-PRE-NULL = HIGH-VALUES
              MOVE DAD-TP-PRE-NULL
                TO (SF)-TP-PRE-NULL
           ELSE
              MOVE DAD-TP-PRE
                TO (SF)-TP-PRE
           END-IF
           MOVE DAD-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE DAD-DS-VER
             TO (SF)-DS-VER
           MOVE DAD-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE DAD-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE DAD-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB.
       VALORIZZA-OUTPUT-DAD-EX.
           EXIT.

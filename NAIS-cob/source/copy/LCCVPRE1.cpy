      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA PREST
      *   ALIAS PRE
      *   ULTIMO AGG. 02 SET 2008
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-PREST PIC S9(9)     COMP-3.
             07 (SF)-ID-OGG PIC S9(9)     COMP-3.
             07 (SF)-TP-OGG PIC X(2).
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-DT-CONCS-PREST   PIC S9(8) COMP-3.
             07 (SF)-DT-CONCS-PREST-NULL REDEFINES
                (SF)-DT-CONCS-PREST   PIC X(5).
             07 (SF)-DT-DECOR-PREST   PIC S9(8) COMP-3.
             07 (SF)-DT-DECOR-PREST-NULL REDEFINES
                (SF)-DT-DECOR-PREST   PIC X(5).
             07 (SF)-IMP-PREST PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-PREST-NULL REDEFINES
                (SF)-IMP-PREST   PIC X(8).
             07 (SF)-INTR-PREST PIC S9(3)V9(3) COMP-3.
             07 (SF)-INTR-PREST-NULL REDEFINES
                (SF)-INTR-PREST   PIC X(4).
             07 (SF)-TP-PREST PIC X(2).
             07 (SF)-TP-PREST-NULL REDEFINES
                (SF)-TP-PREST   PIC X(2).
             07 (SF)-FRAZ-PAG-INTR PIC S9(5)     COMP-3.
             07 (SF)-FRAZ-PAG-INTR-NULL REDEFINES
                (SF)-FRAZ-PAG-INTR   PIC X(3).
             07 (SF)-DT-RIMB   PIC S9(8) COMP-3.
             07 (SF)-DT-RIMB-NULL REDEFINES
                (SF)-DT-RIMB   PIC X(5).
             07 (SF)-IMP-RIMB PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-RIMB-NULL REDEFINES
                (SF)-IMP-RIMB   PIC X(8).
             07 (SF)-COD-DVS PIC X(20).
             07 (SF)-COD-DVS-NULL REDEFINES
                (SF)-COD-DVS   PIC X(20).
             07 (SF)-DT-RICH-PREST   PIC S9(8) COMP-3.
             07 (SF)-MOD-INTR-PREST PIC X(2).
             07 (SF)-MOD-INTR-PREST-NULL REDEFINES
                (SF)-MOD-INTR-PREST   PIC X(2).
             07 (SF)-SPE-PREST PIC S9(12)V9(3) COMP-3.
             07 (SF)-SPE-PREST-NULL REDEFINES
                (SF)-SPE-PREST   PIC X(8).
             07 (SF)-IMP-PREST-LIQTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-PREST-LIQTO-NULL REDEFINES
                (SF)-IMP-PREST-LIQTO   PIC X(8).
             07 (SF)-SDO-INTR PIC S9(12)V9(3) COMP-3.
             07 (SF)-SDO-INTR-NULL REDEFINES
                (SF)-SDO-INTR   PIC X(8).
             07 (SF)-RIMB-EFF PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIMB-EFF-NULL REDEFINES
                (SF)-RIMB-EFF   PIC X(8).
             07 (SF)-PREST-RES-EFF PIC S9(12)V9(3) COMP-3.
             07 (SF)-PREST-RES-EFF-NULL REDEFINES
                (SF)-PREST-RES-EFF   PIC X(8).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).

      *----------------------------------------------------------------*
      *   PORTAFOGLIO VITA
      *   La copy contiene i valori di dominio di actuator
      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
      *    Dominio tipo polizza
      *----------------------------------------------------------------*
       01  ISPV0000-TP-POLI                     PIC X(002).
           88 ISPV0000-COLL-TFR                   VALUE '01'.
           88 ISPV0000-COLL-PREV                  VALUE '02'.
           88 ISPV0000-COLL-GRUPPO                VALUE '03'.
           88 ISPV0000-COLL-TFM                   VALUE '04'.
           88 ISPV0000-COLL-FP-APERTO             VALUE '05'.
           88 ISPV0000-COLL-FP-CHIUSO             VALUE '06'.
           88 ISPV0000-COLL-FP-COMP               VALUE '07'.
           88 ISPV0000-IND-FIP-PIP                VALUE '08'.
           88 ISPV0000-IND-NO-FIP-PIP             VALUE '09'.
      *----------------------------------------------------------------*
      *    Dominio modalita di calcolo premio
      *----------------------------------------------------------------*
       01  ISPV0000-MOD-CALC-PREMIO             PIC X(001).
           88 ISPV0000-MCALC-PREST-INI                     VALUE '1'.
           88 ISPV0000-MCALC-PREMIO-LRD                    VALUE '2'.
           88 ISPV0000-MCALC-PREMIO-RATA                   VALUE '3'.
           88 ISPV0000-MCALC-PREMIO-NETTO                  VALUE '4'.
      *----------------------------------------------------------------*
      *    Dominio tipo premio calcoli
      *----------------------------------------------------------------*
       01  ISPV0000-TP-PREMIO-CALCOLI           PIC X(010).
           88 ISPV0000-TPPRM-ANNUO                 VALUE 'ANNUO'.
           88 ISPV0000-TPPRM-RATA                  VALUE 'RATA'.
           88 ISPV0000-TPPRM-FIRMA                 VALUE 'FIRMA'.

      *----------------------------------------------------------------*
      *    Dominio tipo garanzia
      *----------------------------------------------------------------*
       01  ISPV0000-TP-GARANZIA                  PIC 9(001).
           88 ISPV0000-GAR-BASE                             VALUE 1.
           88 ISPV0000-GAR-COMPLEM                          VALUE 2.
           88 ISPV0000-GAR-OPZIONE                          VALUE 3.
           88 ISPV0000-GAR-ACCESSORIA                       VALUE 4.
           88 ISPV0000-GAR-DIFFERIMENTO                     VALUE 5.
           88 ISPV0000-GAR-CEDOLA                           VALUE 6.
      *----------------------------------------------------------------*
      *    Dominio tipo investimento
      *----------------------------------------------------------------*
       01  ISPV0000-TP-INV                      PIC 9(002).
           88 ISPV0000-INV-COSTANTI                        VALUE 01.
           88 ISPV0000-INV-DECRESCENTI                     VALUE 02.
           88 ISPV0000-INV-LEGGE-FISSA                     VALUE 03.
           88 ISPV0000-INV-RIVALUTAZIONE                   VALUE 04.
           88 ISPV0000-INV-INDICIZZAZIONE                  VALUE 05.
           88 ISPV0000-INV-ESPANSIONE-RIVAL                VALUE 06.
           88 ISPV0000-INV-UNIT-LINKED                     VALUE 07.
           88 ISPV0000-INV-INDEX-LINKED                    VALUE 08.
           88 ISPV0000-INV-SPECIFICA-PROVV                 VALUE 09.
           88 ISPV0000-INV-PARTECIPAZ-UTILI                VALUE 10.
      *----------------------------------------------------------------*
      *    Dominio durate
      *----------------------------------------------------------------*
       01  ISPV0000-CODICE-DURATA               PIC X(002).
           88 ISPV0000-ANNI-INTERI                         VALUE 'AA'.
           88 ISPV0000-ANNI-MESI                           VALUE 'AM'.
           88 ISPV0000-GIORNI                              VALUE 'GG'.
      *----------------------------------------------------------------*
      *    Dominio tipo periodo premio
      *----------------------------------------------------------------*
       01  ISPV0000-PERIODO-PREM                PIC X(001).
           88 ISPV0000-UNICO                               VALUE 'U'.
           88 ISPV0000-UNICO-RICORRENTE                    VALUE 'R'.
           88 ISPV0000-ANNUALE                             VALUE 'A'.
      *----------------------------------------------------------------*
      *    Dominio indicatore rischio
      *----------------------------------------------------------------*
       01  ISPV0000-INDICATORE-RISCHIO          PIC X(002).
           88 ISPV0000-RISCHIO-INVALIDITA                  VALUE 'IV'.
           88 ISPV0000-RISCHIO-INABILITA                   VALUE 'IB'.
           88 ISPV0000-RISCHIO-MALATTIA-GRA                VALUE 'MG'.
           88 ISPV0000-RISCHIO-MORTE                       VALUE 'MO'.
           88 ISPV0000-RISCHIO-VITA                        VALUE 'VI'.
           88 ISPV0000-RISCHIO-VITA-MORTE                  VALUE 'MI'.
           88 ISPV0000-RISCHIO-NESSUNO                     VALUE 'NN'.
      *----------------------------------------------------------------*
      *    Dominio modalita gestione strategia
      *----------------------------------------------------------------*
       01 ISPV0000-MODALITA-GESTIONE            PIC 9(002).
      *-- strategie tipo investimento
          88 ISPV0000-MODGEST-FREE-MIX                     VALUE 00.
          88 ISPV0000-MODGEST-CONSTANT-MIX                 VALUE 01.
          88 ISPV0000-MODGEST-LIFE-CICLE                   VALUE 02.
          88 ISPV0000-MODGEST-LOCK-IN                      VALUE 03.
          88 ISPV0000-MODGEST-BANCHMARK                    VALUE 04.
          88 ISPV0000-MODGEST-FIXED-SWITCH                 VALUE 05.
          88 ISPV0000-MODGEST-LIBERA                       VALUE 06.
      *-- strategie tipo servizio
          88 ISPV0000-BIGC-LIFE-FUND                       VALUE 07.
          88 ISPV0000-BIGC-TAX-BENEFIT-NEW                 VALUE 08.
          88 ISPV0000-BIGC-DEDICATO                        VALUE 09.
          88 ISPV0000-CONSOLIDA-RENDIMENTI                 VALUE 09.

      *----------------------------------------------------------------*
      *    Dominio tipo strategia
      *----------------------------------------------------------------*
       01 ISPV0000-TIPO-STRATEGIA               PIC 9(002).
          88 ISPV0000-TPSTR-PIANO-INVEST                   VALUE 00.
          88 ISPV0000-TPSTR-SERVIZIO                       VALUE 01.
      *----------------------------------------------------------------*
      *    Dominio tipo fondo
      *----------------------------------------------------------------*
       01 ISPV0000-TIPOLOGIA-FONDO              PIC X(001).
          88 ISPV0000-TPFND-UNIT-OICR                      VALUE '0'.
          88 ISPV0000-TPFND-UNIT-ASSIC                     VALUE '1'.
          88 ISPV0000-TPFND-INDEX                          VALUE '2'.
          88 ISPV0000-TPFND-GEST-SPECIALE                  VALUE '3'.
          88 ISPV0000-TPFND-SPEC-PROVVISTA                 VALUE '4'.
          88 ISPV0000-TPFND-INDICE                         VALUE '5'.
      *----------------------------------------------------------------*
      *    Dominio classe fondo
      *----------------------------------------------------------------*
       01 ISPV0000-CLASSE-FONDO                 PIC X(001).
          88 ISPV0000-CLFND-DESTINAZIONE                   VALUE 'D'.
          88 ISPV0000-CLFND-PARTENZA                       VALUE 'P'.
      *----------------------------------------------------------------*
      *    Dominio forma assicurativa
      *----------------------------------------------------------------*
       01 ISPV0000-FORMA-ASSICURATIVA           PIC X(002).
      * Annualitā residue
          88 ISPV0000-FRMAS-ANNUAL-RESIDUE                 VALUE 'AN'.
      * Complementare Copertura Famiglia
          88 ISPV0000-FRMAS-COM-COP-FAM                    VALUE 'CB'.
      * Capitale Differito
          88 ISPV0000-FRMAS-CAPITALE-DIFFER                VALUE 'CD'.
      * Complementare Esonero PagPremi Invaliditā
          88 ISPV0000-FRMAS-COM-ESO-PAG-INV                VALUE 'CE'.
      * Complementare,Infortuni
          88 ISPV0000-FRMAS-COM-INFORTUNI                  VALUE 'CF'.
      * Capitale di Invaliditā
          88 ISPV0000-FRMAS-CAPITALE-INVAL                 VALUE 'CI'.
      * Capitalizzazione Limitata
          88 ISPV0000-FRMAS-CAPITAL-LIM                    VALUE 'CL'.
      * Capitalizzazione Illimitata
          88 ISPV0000-FRMAS-CAPITAL-ILL                    VALUE 'CN'.
      * Complementare Infortuni da Incidente Stradale
          88 ISPV0000-FRMAS-COM-INF-INC-STR                VALUE 'CS'.
      * Complementare Rischio Volo
          88 ISPV0000-FRMAS-COM-RISKIO-VOLO                VALUE 'CV'.
      * Dread Desease Accessoria Anticipativa
          88 ISPV0000-FRMAS-DR-DESE-ACC-ANT                VALUE 'DA'.
      * Dread Desease Base
          88 ISPV0000-FRMAS-DR-DESE-BASE                   VALUE 'DB'.
      * Dread Disease,Aggiuntiva
          88 ISPV0000-FRMAS-DR-DISE-AGG                    VALUE 'DG'.
      * Dotale
          88 ISPV0000-FRMAS-DOTALE                         VALUE 'DO'.
      * Gruppo Morte e Invaliditā
          88 ISPV0000-FRMAS-GPO-MORTE-INVAL                VALUE 'GI'.
      * Gruppo Morte
          88 ISPV0000-FRMAS-GPO-MORTE                      VALUE 'GM'.
      * Rendita di Invaliditā,Illimitata
          88 ISPV0000-FRMAS-REN-INV-ILL                    VALUE 'II'.
      * Rendita di Invaliditā,Temporanea
          88 ISPV0000-FRMAS-REN-INV-TEM                    VALUE 'IT'.
      * Long Term Care
          88 ISPV0000-FRMAS-LONG-TERM-CARE                 VALUE 'LT'.
      * Mista
          88 ISPV0000-FRMAS-MISTA                          VALUE 'MI'.
      * Rendita,Posticipata,Differita,Illimitata,Vitalizia
          88 ISPV0000-FRMAS-REN-POS-DIF-ILL                VALUE 'RA'.
      * Rendita,Anticipata,Differita,Illimitata,Vitalizia
          88 ISPV0000-FRMAS-REN-ANT-DIF-ILL                VALUE 'RB'.
      * Rendita,Anticipata,Immediata,Illimitata,Vitalizia
          88 ISPV0000-FRMAS-REN-ANT-IMM-ILL                VALUE 'RC'.
      * Rendita,Posticipata,Immediata,Illimitata,Vitalizia
          88 ISPV0000-FRMAS-REN-POS-IMM-ILL                VALUE 'RD'.
      * Rendita,Posticipata,Differita,Temporanea,Vitalizia
          88 ISPV0000-FRMAS-REN-POS-DIF-TEM                VALUE 'RE'.
      * Rendita,Anticipata,Differita,Temporanea,Vitalizia
          88 ISPV0000-FRMAS-REN-ANT-DIF-TEM                VALUE 'RF'.
      * Rendita,Anticipata,Immediata,Temporanea,Vitalizia
          88 ISPV0000-FRMAS-REN-ANT-IMM-TEM                VALUE 'RG'.
      * Rendita,Posticipata,Immediata,Temporanea,Vitalizia
          88 ISPV0000-FRMAS-REN-POS-IMM-TEM                VALUE 'RH'.
      * Temporanea Caso Morte
          88 ISPV0000-FRMAS-TEMP-CASO-MORTE                VALUE 'TC'.
      * Termine Fisso
          88 ISPV0000-FRMAS-TERMINE-FISSO                  VALUE 'TF'.
      * Vita Intera
          88 ISPV0000-FRMAS-VITA-INTERA                    VALUE 'VI'.
      * Temporanea Caso Morte, Capitale Morte o Invaliditā
          88 ISPV0000-FRMAS-TEMP-MORT-INVAL                VALUE 'TI'.
      * Temporanea Caso Morte, Rendita di Premorienza
          88 ISPV0000-FRMAS-TEMP-REND-PREMO                VALUE 'TR'.
      *----------------------------------------------------------------*
      *    Ramo bilancio
      *----------------------------------------------------------------*
       01 ISPV0000-RAMO-BILANCIO                  PIC X(012).
          88 ISPV0000-RAMO1                                VALUE '1'.
          88 ISPV0000-RAMO2                                VALUE '2'.
          88 ISPV0000-RAMO3                                VALUE '3'.
          88 ISPV0000-RAMO4                                VALUE '4'.
          88 ISPV0000-RAMO5                                VALUE '5'.
          88 ISPV0000-RAMO6                                VALUE '6'.
      *----------------------------------------------------------------*
      *    Codice Istat
      *----------------------------------------------------------------*
       01 ISPV0000-COD-ISTAT                      PIC X(001).
          88 ISPV0000-ISTAT-NO                             VALUE '0'.
          88 ISPV0000-ISTAT-SI                             VALUE '1'.
          88 ISPV0000-ISTAT-3PERC                          VALUE '2'.
          88 ISPV0000-ISTAT-LIBERA                         VALUE '3'.
      *----------------------------------------------------------------*
      *    Ramo gestionale (ACT_VITA.LINEE)
      *----------------------------------------------------------------*
       01 ISPV0000-COD-RAMO                       PIC X(012).
          88 ISPV0000-CO-VITA-PI                           VALUE '054'.
          88 ISPV0000-CO-GRUPPO                            VALUE '057'.
          88 ISPV0000-CO-BASE-AURORA                       VALUE '058'.
          88 ISPV0000-CO-VITA-AIL                          VALUE '064'.
          88 ISPV0000-IN-BASE                              VALUE '089'.
          88 ISPV0000-IN-UNIT                              VALUE '110'.
          88 ISPV0000-IN-INDEX                             VALUE '111'.
          88 ISPV0000-CO-FONDI-CHIUSI                      VALUE '120'.
          88 ISPV0000-CO-FONDI-PENSIONE                    VALUE '154'.
          88 ISPV0000-CO-TFR                               VALUE '155'.
          88 ISPV0000-CO-CAPITALIZ                         VALUE '156'.
          88 ISPV0000-IN-UNIT-MULTIRAMO                    VALUE '311'.
          88 ISPV0000-IN-CC-ASSICURATIVO                   VALUE '312'.
          88 ISPV0000-CO-BASE-UNIPOL                       VALUE 'C10'.

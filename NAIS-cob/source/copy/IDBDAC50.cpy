           EXEC SQL DECLARE VAR_FUNZ_DI_CALC_R TABLE
           (
             IDCOMP              SMALLINT NOT NULL,
             CODPROD             VARCHAR(12) NOT NULL,
             CODTARI             VARCHAR(12) NOT NULL,
             DINIZ               INTEGER NOT NULL,
             NOMEFUNZ            VARCHAR(12) NOT NULL,
             DEND                INTEGER NOT NULL,
             ISPRECALC           SMALLINT,
             NOMEPROGR           VARCHAR(12),
             MODCALC             VARCHAR(12),
             GLOVARLIST          VARCHAR(4000),
             STEP_ELAB           VARCHAR(20) NOT NULL
          ) END-EXEC.

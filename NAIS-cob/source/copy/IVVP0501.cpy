      **************************************************************
       STRINGA-X-VALORIZZATORE.

           SET IDSV0501-SUCCESSFUL-RC TO TRUE

           MOVE SPACES                TO IDSV0501-DESCRIZ-ERR

           SET IDSV0501-SEGNO-POSTIV-NO TO TRUE
           SET IDSV0501-SEGNO-ANTERIORE TO TRUE

           MOVE 3               TO IDSV0501-DECIMALI-ESPOSTI-IMP

           MOVE 3               TO IDSV0501-DECIMALI-ESPOSTI-PERC
           MOVE 9               TO IDSV0501-DECIMALI-ESPOSTI-TASS

           MOVE ';'             TO IDSV0501-SEPARATORE
           MOVE '.'             TO IDSV0501-SIMBOLO-DECIMALE

           PERFORM CONVERTI-FORMATI THRU CONVERTI-FORMATI-EX

           IF IDSV0501-SUCCESSFUL-RC
              PERFORM CALL-STRINGATURA THRU CALL-STRINGATURA-EX
           END-IF.

       STRINGA-X-VALORIZZATORE-EX.
           EXIT.

      **************************************************************
       CONVERTI-FORMATI.

           PERFORM VARYING IND-VAR FROM 1 BY 1
                     UNTIL IND-VAR > LIMITE-ARRAY-INPUT OR
                           IDSV0501-TP-DATO(IND-VAR) =
                           SPACES OR LOW-VALUES OR HIGH-VALUES

                    EVALUATE IDSV0501-TP-DATO(IND-VAR)
                       WHEN 'R'
                             MOVE IMPORTO  TO TP-DATO-PARALLELO(IND-VAR)

                       WHEN 'Z'
                            MOVE MILLESIMI TO TP-DATO-PARALLELO(IND-VAR)

                       WHEN 'O'
                            MOVE PERCENTUALE
                                           TO TP-DATO-PARALLELO(IND-VAR)

                       WHEN 'T'
                            MOVE TASSO     TO TP-DATO-PARALLELO(IND-VAR)

                       WHEN 'L'
                            MOVE STRINGA   TO TP-DATO-PARALLELO(IND-VAR)

                       WHEN 'C'
                            MOVE DT        TO TP-DATO-PARALLELO(IND-VAR)

                       WHEN 'H'
                            MOVE NUMERICO  TO TP-DATO-PARALLELO(IND-VAR)

                       WHEN OTHER
                            SET IDSV0501-GENERIC-ERROR  TO TRUE
                            STRING 'TIPO DATO LISTA NON VALIDO  : '''
                                   IDSV0501-TP-DATO(IND-VAR)
                                   ''' SU OCCORRENZA : '
                                   IND-VAR
                                   DELIMITED BY SIZE
                                   INTO IDSV0501-DESCRIZ-ERR
                            END-STRING

                    END-EVALUATE
           END-PERFORM.

       CONVERTI-FORMATI-EX.
           EXIT.


           COPY IDSP0501.
      **************************************************************

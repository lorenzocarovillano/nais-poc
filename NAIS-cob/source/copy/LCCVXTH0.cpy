      ******************************************************************
      *    TP_TRCH
      ******************************************************************
       01  WS-TP-TRCH                       PIC X(002) VALUE SPACES.
           88 TP-TRCH-ORDINARIO                        VALUE '1 '.
           88 TP-TRCH-AGGIUNTIV                        VALUE '2 '.
           88 TP-TRCH-BON-RICOR                        VALUE '3 '.
           88 TP-TRCH-BON-FEDEL                        VALUE '4 '.
           88 TP-TRCH-DA-TRASFO                        VALUE '5 '.
           88 TP-TRCH-CED-REINV                        VALUE '6 '.
           88 TP-TRCH-DIFFERIME                        VALUE '7 '.
           88 TP-TRCH-CONV-REND                        VALUE '8 '.
           88 TP-TRCH-NEG-PRCOS                        VALUE '9 '.
           88 TP-TRCH-NEG-RIS-PAR                      VALUE '10'.
           88 TP-TRCH-NEG-RIS-PRO                      VALUE '11'.
           88 TP-TRCH-RIEP-RIS-PRO                     VALUE '12'.
           88 TP-TRCH-NEG-IMPST-SOST                   VALUE '13'.
           88 TP-TRCH-NEG-DA-DIS                       VALUE '14'.
           88 TP-TRCH-NEG-INV-DA-SWIT                  VALUE '15'.
           88 TP-TRCH-INV-DA-SWIT                      VALUE '16'.
           88 TP-TRCH-INV-BONUS-DA-REBATE              VALUE '17'.
           88 TP-TRCH-BONUS-SCADENZA                   VALUE '18'.
           88 TP-TRCH-BIG-CHANCE-POS                   VALUE '19'.
           88 TP-TRCH-BIG-CHANCE-NEG                   VALUE '20'.
           88 TP-TRCH-CONSOLIDA-POS                    VALUE '21'.
           88 TP-TRCH-CONSOLIDA-NEG                    VALUE '22'.
           88 TP-TRCH-COSTI-SWITCH                     VALUE '23'.
           88 TP-TRCH-COSTI-VAR-RIP-PREMI              VALUE '24'.
           88 TP-TRCH-COSTI-INSOLUTO                   VALUE '25'.

      *----------------------------------------------------------------*
      *    COPY      ..... LCCVL716
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO MATRICE ELABORAZIONE BATCH
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVL715 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN MATRICE ELABORAZIONE BATCH (LCCVL711)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-MATR-ELAB-BATCH.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE MATR-ELAB-BATCH.

      *--> NOME TABELLA FISICA DB
           MOVE 'MATR-ELAB-BATCH'                  TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WL71-ST-INV
              AND NOT WL71-ST-CON
              AND WL71-ELE-MATR-ELAB-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WL71-ST-ADD

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK
                        MOVE S090-SEQ-TABELLA TO WL71-ID-PTF
                                                 WL71-ID-MATR-ELAB-BATCH
                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET IDSI0011-INSERT              TO TRUE

      *-->        MODIFICA
                  WHEN WL71-ST-MOD

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET IDSI0011-UPDATE              TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WL71-ST-DEL

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET IDSI0011-DELETE              TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN ADESIONE
                 PERFORM VAL-DCLGEN-L71
                    THRU VAL-DCLGEN-L71-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-L71
                    THRU VALORIZZA-AREA-DSH-L71-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-MATR-ELAB-BATCH-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-L71.

      *--> DCLGEN TABELLA
           MOVE MATR-ELAB-BATCH          TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-PRIMARY-KEY      TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-L71-EX.
           EXIT.

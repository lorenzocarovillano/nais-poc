
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVPLI3
      *   ULTIMO AGG. 07 GEN 2016
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-PLI.
           MOVE PLI-ID-PERC-LIQ
             TO (SF)-ID-PTF(IX-TAB-PLI)
           MOVE PLI-ID-PERC-LIQ
             TO (SF)-ID-PERC-LIQ(IX-TAB-PLI)
           MOVE PLI-ID-BNFICR-LIQ
             TO (SF)-ID-BNFICR-LIQ(IX-TAB-PLI)
           MOVE PLI-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-PLI)
           IF PLI-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE PLI-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PLI)
           ELSE
              MOVE PLI-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-PLI)
           END-IF
           IF PLI-ID-RAPP-ANA-NULL = HIGH-VALUES
              MOVE PLI-ID-RAPP-ANA-NULL
                TO (SF)-ID-RAPP-ANA-NULL(IX-TAB-PLI)
           ELSE
              MOVE PLI-ID-RAPP-ANA
                TO (SF)-ID-RAPP-ANA(IX-TAB-PLI)
           END-IF
           MOVE PLI-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-PLI)
           MOVE PLI-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-PLI)
           MOVE PLI-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-PLI)
           IF PLI-PC-LIQ-NULL = HIGH-VALUES
              MOVE PLI-PC-LIQ-NULL
                TO (SF)-PC-LIQ-NULL(IX-TAB-PLI)
           ELSE
              MOVE PLI-PC-LIQ
                TO (SF)-PC-LIQ(IX-TAB-PLI)
           END-IF
           IF PLI-IMP-LIQ-NULL = HIGH-VALUES
              MOVE PLI-IMP-LIQ-NULL
                TO (SF)-IMP-LIQ-NULL(IX-TAB-PLI)
           ELSE
              MOVE PLI-IMP-LIQ
                TO (SF)-IMP-LIQ(IX-TAB-PLI)
           END-IF
           IF PLI-TP-MEZ-PAG-NULL = HIGH-VALUES
              MOVE PLI-TP-MEZ-PAG-NULL
                TO (SF)-TP-MEZ-PAG-NULL(IX-TAB-PLI)
           ELSE
              MOVE PLI-TP-MEZ-PAG
                TO (SF)-TP-MEZ-PAG(IX-TAB-PLI)
           END-IF
           IF PLI-ITER-PAG-AVV-NULL = HIGH-VALUES
              MOVE PLI-ITER-PAG-AVV-NULL
                TO (SF)-ITER-PAG-AVV-NULL(IX-TAB-PLI)
           ELSE
              MOVE PLI-ITER-PAG-AVV
                TO (SF)-ITER-PAG-AVV(IX-TAB-PLI)
           END-IF
           MOVE PLI-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-PLI)
           MOVE PLI-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-PLI)
           MOVE PLI-DS-VER
             TO (SF)-DS-VER(IX-TAB-PLI)
           MOVE PLI-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-PLI)
           MOVE PLI-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-PLI)
           MOVE PLI-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-PLI)
           MOVE PLI-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-PLI)
           IF PLI-DT-VLT-NULL = HIGH-VALUES
              MOVE PLI-DT-VLT-NULL
                TO (SF)-DT-VLT-NULL(IX-TAB-PLI)
           ELSE
              MOVE PLI-DT-VLT
                TO (SF)-DT-VLT(IX-TAB-PLI)
           END-IF
           MOVE PLI-INT-CNT-CORR-ACCR
             TO (SF)-INT-CNT-CORR-ACCR(IX-TAB-PLI)
           IF PLI-COD-IBAN-RIT-CON-NULL = HIGH-VALUES
              MOVE PLI-COD-IBAN-RIT-CON-NULL
                TO (SF)-COD-IBAN-RIT-CON-NULL(IX-TAB-PLI)
           ELSE
              MOVE PLI-COD-IBAN-RIT-CON
                TO (SF)-COD-IBAN-RIT-CON(IX-TAB-PLI)
           END-IF.
       VALORIZZA-OUTPUT-PLI-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY      ..... LCCVP676
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO EST-POLI-CPI-PR
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BP58GNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-P67.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE EST-POLI-CPI-PR

      *--> NOME TABELLA FISICA DB
           MOVE 'EST-POLI-CPI-PR'
             TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF  NOT WP67-ST-INV
           AND NOT WP67-ST-CON
           AND     WP67-EST-POLI-CPI-PR-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WP67-ST-ADD

                       MOVE WPOL-ID-PTF
                         TO WP67-ID-PTF
                             P67-ID-EST-POLI-CPI-PR

                       MOVE WMOV-ID-PTF
                         TO P67-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                        SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE


      *-->        MODIFICA
                  WHEN WP67-ST-MOD

                    MOVE WP67-ID-PTF           TO P67-ID-EST-POLI-CPI-PR
                    MOVE WMOV-ID-PTF           TO P67-ID-MOVI-CRZ

      *-->       TIPO OPERAZIONE DISPATCHER
                    SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WP67-ST-DEL
                     MOVE WP67-ID-PTF          TO P67-ID-EST-POLI-CPI-PR
                     MOVE WMOV-ID-PTF          TO P67-ID-MOVI-CRZ

      *-->       TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE


              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN IMPOSTA DI BOLLO
                 PERFORM VAL-DCLGEN-P67
                    THRU VAL-DCLGEN-P67-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-P67
                    THRU VALORIZZA-AREA-DSH-P67-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-P67-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-P67.

      *--> DCLGEN TABELLA
           MOVE EST-POLI-CPI-PR         TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT             TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-P67-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVP675.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA ESTR_CNT_DIAGN_CED
      *   ALIAS P84
      *   ULTIMO AGG. 26 SET 2014
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-DATI.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-DT-LIQ-CED   PIC S9(8) COMP-3.
             07 (SF)-COD-TARI PIC X(12).
             07 (SF)-IMP-LRD-CEDOLE-LIQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-LRD-CEDOLE-LIQ-NULL REDEFINES
                (SF)-IMP-LRD-CEDOLE-LIQ   PIC X(8).
             07 (SF)-IB-POLI PIC X(40).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).

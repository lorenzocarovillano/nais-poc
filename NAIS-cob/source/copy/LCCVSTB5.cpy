
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVSTB5
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-STB.
           MOVE (SF)-ID-STAT-OGG-BUS(IX-TAB-STB)
              TO STB-ID-STAT-OGG-BUS
           MOVE (SF)-ID-OGG(IX-TAB-STB)
              TO STB-ID-OGG
           MOVE (SF)-TP-OGG(IX-TAB-STB)
              TO STB-TP-OGG
           MOVE (SF)-ID-MOVI-CRZ(IX-TAB-STB)
              TO STB-ID-MOVI-CRZ
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-STB) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-STB)
              TO STB-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-STB)
              TO STB-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-STB) NOT NUMERIC
              MOVE 0 TO STB-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-STB)
              TO STB-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-STB) NOT NUMERIC
              MOVE 0 TO STB-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-STB)
              TO STB-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-STB)
              TO STB-COD-COMP-ANIA
           MOVE (SF)-TP-STAT-BUS(IX-TAB-STB)
              TO STB-TP-STAT-BUS
           MOVE (SF)-TP-CAUS(IX-TAB-STB)
              TO STB-TP-CAUS
           IF (SF)-DS-RIGA(IX-TAB-STB) NOT NUMERIC
              MOVE 0 TO STB-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-STB)
              TO STB-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-STB)
              TO STB-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-STB) NOT NUMERIC
              MOVE 0 TO STB-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-STB)
              TO STB-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-STB) NOT NUMERIC
              MOVE 0 TO STB-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-STB)
              TO STB-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-STB) NOT NUMERIC
              MOVE 0 TO STB-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-STB)
              TO STB-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-STB)
              TO STB-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-STB)
              TO STB-DS-STATO-ELAB.
       VAL-DCLGEN-STB-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVL054
      *   ULTIMO AGG. 06 GIU 2018
      *------------------------------------------------------------

       INIZIA-TOT-L05.

           PERFORM INIZIA-ZEROES-L05 THRU INIZIA-ZEROES-L05-EX

           PERFORM INIZIA-SPACES-L05 THRU INIZIA-SPACES-L05-EX

           PERFORM INIZIA-NULL-L05 THRU INIZIA-NULL-L05-EX.

       INIZIA-TOT-L05-EX.
           EXIT.

       INIZIA-NULL-L05.
           MOVE HIGH-VALUES TO (SF)-COD-BLOCCO-NULL
           MOVE HIGH-VALUES TO (SF)-SRVZ-VER-ANN-NULL
           MOVE HIGH-VALUES TO (SF)-WHERE-CONDITION
           MOVE HIGH-VALUES TO (SF)-FL-POLI-IFP-NULL.
       INIZIA-NULL-L05-EX.
           EXIT.

       INIZIA-ZEROES-L05.
           MOVE 0 TO (SF)-COD-COMP-ANIA
           MOVE 0 TO (SF)-TP-MOVI-ESEC
           MOVE 0 TO (SF)-TP-MOVI-RIFTO
           MOVE 0 TO (SF)-DS-VER
           MOVE 0 TO (SF)-DS-TS-CPTZ.
       INIZIA-ZEROES-L05-EX.
           EXIT.

       INIZIA-SPACES-L05.
           MOVE SPACES TO (SF)-GRAV-FUNZ-FUNZ
           MOVE SPACES TO (SF)-DS-OPER-SQL
           MOVE SPACES TO (SF)-DS-UTENTE
           MOVE SPACES TO (SF)-DS-STATO-ELAB.
       INIZIA-SPACES-L05-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    ESTRATTO CONTO - FASE DIAGNOSTICO
      *       - CONTROLLI AD HOC MULTIRAMO & UNIT LINKED
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *   CONTROLLO 3 12                                               *
      *----------------------------------------------------------------*
       C0030-CNTRL-3-12.

           MOVE 'C0030-CNTRL-3-12'          TO WK-LABEL.

           SET CONTR-3-RV-SI  TO TRUE
           SET CONTR-12-RV-SI TO TRUE

           IF TP-ESTR-RV
              SET POLI-580-NO TO TRUE
              PERFORM RECUPERA-PROD
                 THRU RECUPERA-PROD-EX

              PERFORM VARYING IX-IND FROM 1 BY 1
                UNTIL IDSV0001-ESITO-KO
                   OR IX-IND > ISPC0580-NUM-MAX-PROD
                   OR CONTR-3-RV-NO
                   IF POL-COD-PROD = ISPC0580-TIPO-PRD(IX-IND)
                      SET POLI-580-SI TO TRUE
                      IF ISPC0580-SOTTORAMO(IX-IND) = 'Ramo V'
                      OR (ISPC0580-FORMA(IX-IND) = 'Capitale Differito'
                         AND ISPC0580-TIPOPREMIO(IX-IND) = 'Annuo')
                      OR (ISPC0580-FORMA(IX-IND) = 'Capitale Differito'
                         AND ISPC0580-TIPOPREMIO(IX-IND) = 'Unico')
                      OR (ISPC0580-FORMA(IX-IND) = 'Capitale Differito'
                          AND ISPC0580-TIPOPREMIO(IX-IND) =
                          'Unico Ricorrente'
NEW                       AND WK-TASSO-TEC-RV > 0 )
                          SET CONTR-3-RV-NO TO TRUE
                      END-IF
                      MOVE IX-IND TO IX-580
                   END-IF
              END-PERFORM
           END-IF

           IF TP-ESTR-RV
              IF POLI-580-SI
13090            IF ISPC0580-FORMAPREST(IX-580) = 'Rendita'
                    SET CONTR-12-RV-NO TO TRUE
                 END-IF
              ELSE
                 IF WRIN-FLAG-PROD-CON-CEDOLE = 'SI'
                    SET CONTR-12-RV-NO TO TRUE
                 END-IF
              END-IF
           END-IF
      *
           IF CONTR-3-RV-SI
ALFR  * --> filtrare le polizze TCM dal controllo 3
alfr          IF TP-ESTR-TCM
alfr             CONTINUE
alfr          ELSE

NEW              MOVE 0 TO WS-DIFF-2
NEW              COMPUTE WS-DIFF-2 = WRIN-CAP-LIQ-DT-RIF-EST-CC
                                  -  WRIN-PRESTAZIONE-AC-POL

alfr  * --> ripristinata la tolleranza a -0,01 per il controllo 3
alfr  *         IF WS-DIFF-2 < -1
NEW              IF WS-DIFF-2 < -0,01
                    MOVE 3 TO WK-CONTROLLO
                    MOVE WRIN-CAP-LIQ-DT-RIF-EST-CC TO WS-CAMPO-1
                    MOVE WRIN-PRESTAZIONE-AC-POL    TO WS-CAMPO-2
                    SET CNTRL-3      TO TRUE
                    MOVE MSG-ERRORE TO WS-ERRORE
                    PERFORM E001-SCARTO THRU EX-E001
                END-IF
ALFR          END-IF
           END-IF

           IF CONTR-12-RV-SI
NEW          MOVE 0 TO WS-DIFF-2

NEW          COMPUTE WS-DIFF-2 = WRIN-PRESTAZIONE-AC-POL
                             - WRIN-RISC-TOT-DT-RIF-EST-CC

alfr  * --> ripristinata la tolleranza a -0,01 per il controllo 12
alfr  *      IF WS-DIFF-2 < -10
NEW          IF WS-DIFF-2 < -0,01
                MOVE 12 TO WK-CONTROLLO
                MOVE WRIN-PRESTAZIONE-AC-POL     TO WS-CAMPO-1
                MOVE WRIN-RISC-TOT-DT-RIF-EST-CC TO WS-CAMPO-2
                SET CNTRL-12      TO TRUE
                MOVE MSG-ERRORE TO WS-ERRORE
                PERFORM E001-SCARTO THRU EX-E001
             END-IF
           END-IF.

       EX-C0030.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 24                                                 *
      *----------------------------------------------------------------*
       C0032-CNTRL-24.

           MOVE 'C0032-CNTRL-24'          TO WK-LABEL.

           MOVE 0 TO WS-SOMMA WS-SOMMA-2
NEW        MOVE 0 TO WS-DIFF-2

           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
ALFR  *        IF REC3-TP-GAR(IX-REC-3) = 1
alfr           IF REC3-TP-GAR(IX-REC-3) = 1 OR 5

                  COMPUTE WS-SOMMA = WS-SOMMA
                        + REC3-CAP-LIQ-GA(IX-REC-3)

                  COMPUTE WS-SOMMA-2 = WS-SOMMA-2
                        + REC3-PREST-MATUR-AA-RIF(IX-REC-3)

               END-IF

           END-PERFORM.

NEW        COMPUTE WS-DIFF-2  = WS-SOMMA - WS-SOMMA-2

alfr  * --> ripristinata la tolleranza a -0,01 per il controllo 24
alfr  *    IF WS-DIFF-2 < -1
NEW        IF WS-DIFF-2 < -0,01
              MOVE WS-SOMMA   TO WS-CAMPO-1
              MOVE WS-SOMMA-2 TO WS-CAMPO-2
              MOVE 24 TO WK-CONTROLLO
              SET CNTRL-24      TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0032.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 35                                                 *
      *----------------------------------------------------------------*
       C0034-CNTRL-35.

           MOVE ' C0034-CNTRL-35'          TO WK-LABEL.

           MOVE 0 TO WS-SOMMA.
           MOVE 0 TO WS-CNTRVAL-PREC.
           SET 35-ERRORE-NO TO TRUE

           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
                OR 35-ERRORE-SI
      *
                MOVE 0 TO WS-SOMMA
                MOVE 0 TO WS-CNTRVAL-PREC
      *
                IF REC3-TP-INVST(IX-REC-3) = 7
      *
                   PERFORM VARYING IX-C35-TAB FROM 1 BY 1
                      UNTIL IX-C35-TAB > IX-MAX-C35
                      IF REC3-ID-GAR(IX-REC-3) = C35-ID-GAR(IX-C35-TAB)
                         COMPUTE WS-CNTRVAL-PREC =
                                 C35-CNTRVAL-PREC(IX-C35-TAB) +
                                 WS-CNTRVAL-PREC
                      END-IF
                   END-PERFORM
      *
                   COMPUTE WS-SOMMA = WS-SOMMA
                         + REC3-PREST-MATUR-EC-PREC(IX-REC-3)
                END-IF


alfr  * --> ripristinata la tolleranza a -0,01/+0,01 per il controllo 35
 "    *        IF (WS-SOMMA - WS-CNTRVAL-PREC) > +10
 "    *        OR (WS-SOMMA - WS-CNTRVAL-PREC) < -10
 "             IF (WS-SOMMA - WS-CNTRVAL-PREC) > +0,01
alfr           OR (WS-SOMMA - WS-CNTRVAL-PREC) < -0,01

                   SET 35-ERRORE-SI TO TRUE
                   MOVE 35 TO WK-CONTROLLO
                   MOVE WS-SOMMA          TO WS-CAMPO-1
                   MOVE WS-CNTRVAL-PREC   TO WS-CAMPO-2
                   SET CNTRL-35      TO TRUE
                   MOVE MSG-ERRORE TO WS-ERRORE
                   PERFORM E001-SCARTO THRU EX-E001
               END-IF
      *
           END-PERFORM.
      *
       EX-C0034.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 36                                                 *
      *----------------------------------------------------------------*
       C0036-CNTRL-36.

           MOVE ' C0036-CNTRL-36'          TO WK-LABEL.

           MOVE 0 TO WS-SOMMA.

           PERFORM VARYING IX-REC-3 FROM 1 BY 1
             UNTIL IX-REC-3 > ELE-MAX-CACHE-3
               IF REC3-TP-INVST(IX-REC-3) = 7

                  COMPUTE WS-SOMMA = WS-SOMMA
                        + REC3-PREST-MATUR-AA-RIF(IX-REC-3)

               END-IF

           END-PERFORM.

           IF (WS-SOMMA - REC8-CNTRVAL-ATTUALE) > 0,01
           OR (WS-SOMMA - REC8-CNTRVAL-ATTUALE) < -0,01
              MOVE 36 TO WK-CONTROLLO
              MOVE WS-SOMMA             TO WS-CAMPO-1
              MOVE REC8-CNTRVAL-ATTUALE TO WS-CAMPO-2
              SET CNTRL-36      TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0036.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 37                                                 *
      *----------------------------------------------------------------*
       C0038-CNTRL-37.
      *
           MOVE 'C0038-CNTRL-37'          TO WK-LABEL.
      *

           SET CNTRL-37-ERR-NO TO TRUE
           PERFORM VARYING IX-C37-TAB FROM 1 BY 1
                   UNTIL IX-C37-TAB > IX-MAX-C37
                      OR CNTRL-37-ERR-SI

               COMPUTE WK-CNTRL-37 =
                 (C37-NUM-QUO-PREC(IX-C37-TAB) -
              C37-NUM-QUO-DISINV-RISC-PARZ(IX-C37-TAB) -
              C37-NUM-QUO-DIS-RISC-PARZ-PRG(IX-C37-TAB) -
              C37-NUM-QUO-DISINV-IMPOS-SOST(IX-C37-TAB) -
              C37-NUM-QUO-DISINV-COMMGES(IX-C37-TAB) -
              C37-NUM-QUO-DISINV-PREL-COSTI(IX-C37-TAB) -
              C37-NUM-QUO-DISINV-SWITCH(IX-C37-TAB) -
13815         C37-NUM-QUO-DISINV-COMP-NAV(IX-C37-TAB) +
              C37-NUM-QUO-INVST-SWITCH(IX-C37-TAB) +
              C37-NUM-QUO-INVST-VERS(IX-C37-TAB) +
              C37-NUM-QUO-INVST-REBATE(IX-C37-TAB)  +
              C37-NUM-QUO-INVST-PURO-RISC(IX-C37-TAB) +
13815         C37-NUM-QUO-INVST-COMP-NAV(IX-C37-TAB)) -
              C37-NUM-QUO-ATTUALE(IX-C37-TAB)

              IF WK-CNTRL-37 > 0,01
              OR WK-CNTRL-37 < -0,01
                 MOVE WK-CNTRL-37
                    TO WS-CAMPO-1
                 MOVE C37-NUM-QUO-PREC(IX-C37-TAB)
                   TO WS-CAMPO-2
                 MOVE C37-NUM-QUO-ATTUALE(IX-C37-TAB)
                   TO WS-CAMPO-3
                 MOVE 37 TO WK-CONTROLLO
                 SET CNTRL-37        TO TRUE
                 MOVE MSG-ERRORE     TO WS-ERRORE
                 PERFORM E001-SCARTO THRU EX-E001
                 SET CNTRL-37-ERR-SI TO TRUE
              END-IF

           END-PERFORM.

      *    IF CNTRL-37-ERR-NO
      *       CONTINUE
      *    ELSE
      *       MOVE 37 TO WK-CONTROLLO
      *       SET CNTRL-37        TO TRUE
      *       MOVE MSG-ERRORE     TO WS-ERRORE
      *       PERFORM E001-SCARTO THRU EX-E001
      *    END-IF.
      *
       EX-C0038.
           EXIT.

      *----------------------------------------------------------------*
      *   CONTROLLO 44                                                 *
      *----------------------------------------------------------------*
       C0040-CNTRL-44.

           MOVE 'C0040-CNTRL-44'          TO WK-LABEL.

           MOVE 0 TO WS-SOMMA-3
           MOVE 0 TO WS-DIFF-3

           PERFORM VARYING IX-REC-10 FROM 1 BY 1
             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
               IF REC10-DESC-TP-MOVI(IX-REC-10) =
                  TP-COMM-GEST
                  COMPUTE WS-SOMMA-3 = WS-SOMMA-3
                        + REC10-NUMERO-QUO(IX-REC-10)
               END-IF
           END-PERFORM.

           COMPUTE WS-DIFF-3 = REC8-NUM-QUO-DISINV-COMMGES
                           - WS-SOMMA-3

           IF  WS-DIFF-3 > -0,05 AND WS-DIFF-3 < 0,05
              CONTINUE
           ELSE
              MOVE WS-SOMMA-3                  TO WS-CAMPO-1
              MOVE REC8-NUM-QUO-DISINV-COMMGES TO WS-CAMPO-2
              MOVE 44 TO WK-CONTROLLO
              SET CNTRL-44     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0040.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 45                                                 *
      *----------------------------------------------------------------*
       C0042-CNTRL-45.

           MOVE 'C0042-CNTRL-45'          TO WK-LABEL.

           MOVE 0 TO WS-SOMMA-3

           PERFORM VARYING IX-REC-10 FROM 1 BY 1
             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
               IF REC10-DESC-TP-MOVI(IX-REC-10) =
                  TP-CASO-MORTE
                  COMPUTE WS-SOMMA-3 = WS-SOMMA-3
                        + REC10-NUMERO-QUO(IX-REC-10)
               END-IF
           END-PERFORM.

           COMPUTE WS-DIFF-3 = REC8-NUM-QUO-DISINV-PREL-COSTI
                           - WS-SOMMA-3

           IF  WS-DIFF-3 > -0,05 AND WS-DIFF-3 < 0,05
              CONTINUE
           ELSE
              MOVE WS-SOMMA-3                     TO WS-CAMPO-1
              MOVE REC8-NUM-QUO-DISINV-PREL-COSTI TO WS-CAMPO-2
              MOVE 45 TO WK-CONTROLLO
              SET CNTRL-45     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0042.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 46                                                 *
      *----------------------------------------------------------------*
       C0044-CNTRL-46.

           MOVE 'C0044-CNTRL-46'          TO WK-LABEL.

           MOVE 0 TO WS-SOMMA-3

           PERFORM VARYING IX-REC-10 FROM 1 BY 1
             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
               IF REC10-DESC-TP-MOVI(IX-REC-10) =
                  TP-REBATE
                  COMPUTE WS-SOMMA-3 = WS-SOMMA-3
                        + REC10-NUMERO-QUO(IX-REC-10)
               END-IF
           END-PERFORM.

           IF  REC8-NUM-QUO-INVST-REBATE
               = WS-SOMMA-3
              CONTINUE
           ELSE
              MOVE WS-SOMMA-3                     TO WS-CAMPO-1
              MOVE REC8-NUM-QUO-INVST-REBATE      TO WS-CAMPO-2
              MOVE 46 TO WK-CONTROLLO
              SET CNTRL-46     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0044.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 47                                                 *
      *----------------------------------------------------------------*
       C0046-CNTRL-47.

           MOVE 'C0046-CNTRL-47'          TO WK-LABEL.

           MOVE 0 TO WS-SOMMA-3

           PERFORM VARYING IX-REC-10 FROM 1 BY 1
             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
               IF REC10-DESC-TP-MOVI(IX-REC-10) =
                  TP-VERS-PREMI
                  COMPUTE WS-SOMMA-3 = WS-SOMMA-3
                        + REC10-NUMERO-QUO(IX-REC-10)
               END-IF
           END-PERFORM.


           IF  REC8-NUM-QUO-INVST-VERS
               = WS-SOMMA-3
              CONTINUE
           ELSE
              MOVE WS-SOMMA-3                     TO WS-CAMPO-1
              MOVE REC8-NUM-QUO-INVST-VERS        TO WS-CAMPO-2
              MOVE 47 TO WK-CONTROLLO
              SET CNTRL-47     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0046.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 48                                                 *
      *----------------------------------------------------------------*
       C0048-CNTRL-48.

           MOVE 'C0048-CNTRL-48'          TO WK-LABEL.

           MOVE 0 TO WS-SOMMA-3

           PERFORM VARYING IX-REC-10 FROM 1 BY 1
             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
               IF REC10-DESC-TP-MOVI(IX-REC-10) =
                  TP-SW-OUT-1 OR TP-SW-OUT-2 OR
                  TP-SW-OUT-3 OR TP-SW-OUT-4 OR
                  TP-SW-OUT-5 OR TP-SW-OUT-6 OR
10818             TP-SW-OUT-7 OR TP-SW-OUT-8 OR
12193             TP-OUT-SW-PASSO-PASSO OR
12193             TP-OUT-SW-STOP-LOSS   OR
12193             TP-OUT-SW-LINEA OR
12908             TP-OUT-SW-BATCH-PARZ-MASS OR
FNZ               TP-OUT-SW-RIBIL           OR
FNZ               TP-OUT-SW-GAP-EVENT OR
FNZ               TP-OUT-SW-SUP-SOGLIA OR
FNZ               TP-OUT-SW-MASSIVO-FNZ
                  COMPUTE WS-SOMMA-3 = WS-SOMMA-3
                      + REC10-NUMERO-QUO(IX-REC-10)
               END-IF
           END-PERFORM.

           IF  REC8-NUM-QUO-DISINV-SWITCH
               = WS-SOMMA-3
              CONTINUE
           ELSE
              MOVE WS-SOMMA-3                     TO WS-CAMPO-1
              MOVE REC8-NUM-QUO-DISINV-SWITCH     TO WS-CAMPO-2
              MOVE 48 TO WK-CONTROLLO
              SET CNTRL-48     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.


       EX-C0048.
           EXIT.
      *----------------------------------------------------------------*
      *   CONTROLLO 49                                                 *
      *----------------------------------------------------------------*
       C0050-CNTRL-49.

           MOVE 'C0050-CNTRL-49'          TO WK-LABEL.

           MOVE 0 TO WS-SOMMA-3

           PERFORM VARYING IX-REC-10 FROM 1 BY 1
             UNTIL IX-REC-10 > ELE-MAX-CACHE-10
               IF REC10-DESC-TP-MOVI(IX-REC-10) =
                  TP-SW-IN-1 OR TP-SW-IN-2 OR
                  TP-SW-IN-3 OR TP-SW-IN-4 OR
                  TP-SW-IN-5 OR TP-SW-IN-6 OR
10818             TP-SW-IN-7 OR TP-SW-IN-8 OR
12193             TP-IN-SW-PASSO-PASSO     OR
12193             TP-IN-SW-STOP-LOSS       OR
12193             TP-IN-SW-LINEA OR
12908             TP-IN-SW-BATCH-PARZ-MASS OR
FNZ               TP-IN-SW-RIBIL  OR
FNZ               TP-IN-SW-GAP-EVENT OR
FNZ               TP-IN-SW-SUP-SOGLIA OR
FNZ               TP-IN-SW-MASSIVO-FNZ
                  COMPUTE WS-SOMMA-3 = WS-SOMMA-3
                      + REC10-NUMERO-QUO(IX-REC-10)
               END-IF
           END-PERFORM.
           IF  REC8-NUM-QUO-INV-SWITCH
               = WS-SOMMA-3
              CONTINUE
           ELSE
              MOVE WS-SOMMA-3                     TO WS-CAMPO-1
              MOVE REC8-NUM-QUO-INV-SWITCH        TO WS-CAMPO-2
              MOVE 49 TO WK-CONTROLLO
              SET CNTRL-49     TO TRUE
              MOVE MSG-ERRORE TO WS-ERRORE
              PERFORM E001-SCARTO THRU EX-E001
           END-IF.

       EX-C0050.
           EXIT.
13815 *----------------------------------------------------------------*
13815 *   CONTROLLO 50                                                 *
13815 *----------------------------------------------------------------*
13815  C0052-CNTRL-50.

13815      MOVE 'C0052-CNTRL-50'          TO WK-LABEL.

13815      MOVE 0 TO WS-SOMMA-3
13815      MOVE 0 TO WS-DIFF-3

13815      PERFORM VARYING IX-REC-10 FROM 1 BY 1
13815        UNTIL IX-REC-10 > ELE-MAX-CACHE-10
13815          IF REC10-DESC-TP-MOVI(IX-REC-10) =
13815             TP-COMP-NAV-NEG
13815             COMPUTE WS-SOMMA-3 = WS-SOMMA-3
13815                   + REC10-NUMERO-QUO(IX-REC-10)
13815          END-IF
13815      END-PERFORM.

13815      COMPUTE WS-DIFF-3 = REC8-NUM-QUO-DISINV-COMP-NAV
13815                      - WS-SOMMA-3

13815      IF  WS-DIFF-3 > -0,05 AND WS-DIFF-3 < 0,05
13815         CONTINUE
13815      ELSE
13815         MOVE WS-SOMMA-3                   TO WS-CAMPO-1
13815         MOVE REC8-NUM-QUO-DISINV-COMP-NAV TO WS-CAMPO-2
13815         MOVE 50 TO WK-CONTROLLO
13815         SET CNTRL-50     TO TRUE
13815         MOVE MSG-ERRORE TO WS-ERRORE
13815         PERFORM E001-SCARTO THRU EX-E001
13815      END-IF.

13815  EX-C0052.
13815      EXIT.
13815 *----------------------------------------------------------------*
13815 *   CONTROLLO 51                                                 *
13815 *----------------------------------------------------------------*
13815  C0054-CNTRL-51.

13815      MOVE 'C0054-CNTRL-51'          TO WK-LABEL.

13815      MOVE 0 TO WS-SOMMA-3

13815      PERFORM VARYING IX-REC-10 FROM 1 BY 1
13815        UNTIL IX-REC-10 > ELE-MAX-CACHE-10
13815          IF REC10-DESC-TP-MOVI(IX-REC-10) =
13815             TP-COMP-NAV-POS
13815             COMPUTE WS-SOMMA-3 = WS-SOMMA-3
13815                   + REC10-NUMERO-QUO(IX-REC-10)
13815          END-IF
13815      END-PERFORM.

13815      IF  REC8-NUM-QUO-INVST-COMP-NAV
13815          = WS-SOMMA-3
13815         CONTINUE
13815      ELSE
13815         MOVE WS-SOMMA-3                     TO WS-CAMPO-1
13815         MOVE REC8-NUM-QUO-INVST-COMP-NAV    TO WS-CAMPO-2
13815         MOVE 51 TO WK-CONTROLLO
13815         SET CNTRL-51     TO TRUE
13815         MOVE MSG-ERRORE TO WS-ERRORE
13815         PERFORM E001-SCARTO THRU EX-E001
13815      END-IF.

13815  EX-C0054.
13815      EXIT.
      *----------------------------------------------------------------*
      *   LETTURA POLIZZA PER RECUPERARE IL CODICE PRODOTTO            *
      *----------------------------------------------------------------*
       RECUPERA-PROD.
      *
           MOVE 'RECUPERA-PROD'          TO WK-LABEL.
      *
           INITIALIZE POLI
                      IDSI0011-BUFFER-DATI.

           MOVE ZEROES        TO IDSI0011-DATA-INIZIO-EFFETTO
                                 IDSI0011-DATA-FINE-EFFETTO
                                 IDSI0011-DATA-COMPETENZA.

           SET IDSI0011-ID                TO TRUE.
           SET IDSI0011-SELECT            TO TRUE.
           SET IDSI0011-TRATT-DEFAULT     TO TRUE.
      *
           MOVE WRIN-ID-POLI               TO POL-ID-POLI.
      *
           MOVE 'POLI'                    TO IDSI0011-CODICE-STR-DATO
      *
           MOVE POLI                      TO IDSI0011-BUFFER-DATI.
      *
           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI TO POLI
      *
                  WHEN OTHER
                         MOVE WK-PGM         TO IEAI9901-COD-SERVIZIO-BE
                         MOVE WK-LABEL       TO IEAI9901-LABEL-ERR
                         MOVE '005016'       TO IEAI9901-COD-ERRORE
                         STRING 'POLI'       ';'
                                IDSO0011-RETURN-CODE ';'
                                IDSO0011-SQLCODE
                                DELIMITED BY SIZE
                                INTO IEAI9901-PARAMETRI-ERR
                         END-STRING

                         PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300

              END-EVALUATE
           ELSE
      *-->    ERRORE DISPATCHER
              MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL              TO IEAI9901-LABEL-ERR
              MOVE '005016'              TO IEAI9901-COD-ERRORE
              STRING 'POLI       '        ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY   SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE THRU EX-S0300
           END-IF.
      *
       RECUPERA-PROD-EX.
           EXIT.


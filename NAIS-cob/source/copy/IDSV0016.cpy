      ******************************************************************
      * INIZIO STATEMENTS DA UTILIZZARE PRETTAMENTE CON IDSP0016
      ******************************************************************
       77 IDSV0016-RISULTATO                 PIC S9(04).
       77 IDSV0016-RESTO                     PIC S9(04).

       77 IDSV0016-CONST-FEBBRAIO            PIC 9(01) VALUE 2.

       77 IDSV0016-DESC-ERRORE               PIC X(30).

       01 IDSV0016-DATA.
          05 IDSV0016-AAAA                   PIC 9(04).
          05 IDSV0016-MM                     PIC 9(02).
          05 IDSV0016-GG                     PIC 9(02).

       01 IDSV0016-TIPO-ANNO                 PIC X(01).
          88 IDSV0016-ANNO-BISESTILE         VALUE 'B'.
          88 IDSV0016-ANNO-STANDARD          VALUE 'S'.

       01 IDSV0016-CALENDARIO.
          05 IDSV0016-STR-GIORNI-ANNUAL.
             10 IDSV0016-STR-GENNAIO         PIC 9(02) VALUE 31.
             10 IDSV0016-STR-FEBBRAIO        PIC 9(02) VALUE 28.
             10 IDSV0016-STR-MARZO           PIC 9(02) VALUE 31.
             10 IDSV0016-STR-APRILE          PIC 9(02) VALUE 30.
             10 IDSV0016-STR-MAGGIO          PIC 9(02) VALUE 31.
             10 IDSV0016-STR-GIUGNO          PIC 9(02) VALUE 30.
             10 IDSV0016-STR-LUGLIO          PIC 9(02) VALUE 31.
             10 IDSV0016-STR-AGOSTO          PIC 9(02) VALUE 31.
             10 IDSV0016-STR-SETTEMBRE       PIC 9(02) VALUE 30.
             10 IDSV0016-STR-OTTOBRE         PIC 9(02) VALUE 31.
             10 IDSV0016-STR-NOVEMBRE        PIC 9(02) VALUE 30.
             10 IDSV0016-STR-DICEMBRE        PIC 9(02) VALUE 31.

          05 IDSV0016-STR-GIORNI-ANNUAL-ELE REDEFINES
             IDSV0016-STR-GIORNI-ANNUAL  OCCURS  12 TIMES.
             10 IDSV0016-STR-GG-ANNUAL          PIC 9(02).



      ******************************************************************
      * FINE STATEMENTS DA UTILIZZARE PRETTAMENTE CON IDSP0016
      ******************************************************************

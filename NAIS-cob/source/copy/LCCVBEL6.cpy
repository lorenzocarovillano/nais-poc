      *----------------------------------------------------------------*
      *    COPY      ..... LCCVBEL6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO BENEFICIARIO LIQUIDAZIONE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVBEP5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN ADESIONE (LCCVBEP1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-BNFICR-LIQ.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE BNFICR-LIQ

      *--> NOME TABELLA FISICA DB
           MOVE 'BNFICR-LIQ'             TO WK-TABELLA

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WBEL-ST-INV(IX-TAB-BEL)
              AND NOT WBEL-ST-CON(IX-TAB-BEL)
              AND WBEL-ELE-BEN-LIQ-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO TRACHE LIQUIDAZIONE
                  WHEN WBEL-ST-ADD(IX-TAB-BEL)

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK
                        MOVE S090-SEQ-TABELLA
                          TO WBEL-ID-PTF(IX-TAB-BEL)
                     END-IF

                     MOVE WBEL-ID-PTF(IX-TAB-BEL)
                       TO BEL-ID-BNFICR-LIQ
                     MOVE WLQU-ID-PTF(IX-TAB-LQU)
                       TO WBEL-ID-LIQ(IX-TAB-BEL)
                          BEL-ID-LIQ
                     MOVE WMOV-ID-PTF
                       TO WBEL-ID-MOVI-CRZ(IX-TAB-BEL)
                          BEL-ID-MOVI-CRZ
                     MOVE WMOV-DT-EFF
                       TO WBEL-DT-INI-EFF(IX-TAB-BEL)

                     IF WBEL-ID-RAPP-ANA-NULL(IX-TAB-BEL) = HIGH-VALUE
                        MOVE ZERO
                          TO WS-ID-RAPP-ANA-BEN
                     ELSE
                        MOVE WBEL-ID-RAPP-ANA(IX-TAB-BEL)
                          TO WS-ID-RAPP-ANA-BEN
                     END-IF

                     SET  NO-TROVATO                         TO TRUE

                     PERFORM SEARCH-ID-RAP-ANA-BEL
                        THRU SEARCH-ID-RAP-ANA-BEL-EX
                     VARYING IX-TAB-RAN FROM 1 BY 1
                       UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX
                          OR SI-TROVATO

                     IF SI-TROVATO
                        MOVE WS-ID-RAPP-ANA
                          TO WBEL-ID-RAPP-ANA(IX-TAB-BEL)
                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA LIQUIDAZIONE
                  WHEN WBEL-ST-MOD(IX-TAB-BEL)

                     MOVE WBEL-ID-BNFICR-LIQ(IX-TAB-BEL)
                       TO BEL-ID-BNFICR-LIQ
                     MOVE WBEL-ID-LIQ(IX-TAB-BEL)
                       TO BEL-ID-LIQ
                     MOVE WMOV-ID-PTF
                       TO WBEL-ID-MOVI-CRZ(IX-TAB-BEL)
                          BEL-ID-MOVI-CRZ
                     MOVE WMOV-DT-EFF
                       TO WBEL-DT-INI-EFF(IX-TAB-BEL)

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE LIQUIDAZIONE
                  WHEN WBEL-ST-DEL(IX-TAB-BEL)

                       MOVE WBEL-ID-BNFICR-LIQ(IX-TAB-BEL)
                         TO BEL-ID-BNFICR-LIQ
                       MOVE WBEL-ID-LIQ(IX-TAB-BEL)
                         TO BEL-ID-LIQ

                       MOVE WMOV-ID-PTF
                         TO WBEL-ID-MOVI-CRZ(IX-TAB-BEL)
                            BEL-ID-MOVI-CRZ
                       MOVE WMOV-DT-EFF
                         TO WBEL-DT-END-EFF(IX-TAB-BEL)

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA        TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN ADESIONE
                 PERFORM VAL-DCLGEN-BEL
                    THRU VAL-DCLGEN-BEL-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-BEL
                    THRU VALORIZZA-AREA-DSH-BEL-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-BNFICR-LIQ-EX.
           EXIT.
      *---------------------------------------------------------------*
      *    CICLO DI ELABORAZIONE SU DCLGEN RAPP-ANA
      *--------------------------------------------------------------*
       SEARCH-ID-RAP-ANA-BEL.

           IF WS-ID-RAPP-ANA-BEN = WRAN-ID-RAPP-ANA(IX-TAB-RAN)
              SET SI-TROVATO                 TO TRUE
              MOVE WRAN-ID-PTF(IX-TAB-RAN)   TO WS-ID-RAPP-ANA
           END-IF.

       SEARCH-ID-RAP-ANA-BEL-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-BEL.

      *--> DCLGEN TABELLA
           MOVE BNFICR-LIQ              TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-BEL-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVBEL5.

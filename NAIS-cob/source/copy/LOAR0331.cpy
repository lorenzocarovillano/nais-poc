      *  allineamento 10/09/2008
      *  --------------------------------------------------------------*
      *  STRUTTURA ARCHIVIO IN OUTPUT  - CALCOLO IMPOSTA SOSTITUTIVA   *
      *       - FLUSSO DI OUTPUT ELABORAZIONE -         *LREC=500      *
      *  --------------------------------------------------------------*
      *
          03 OUT331-TP-REC                  PIC  X(003).
          03 OUT331-REC-OUT                 PIC  X(450).
      *
          03 OUT331-REC-GEN                 REDEFINES  OUT331-REC-OUT.
      *
             05 OUT331-G-MACRO              PIC  X(002).
             05 OUT331-G-FUNZ               PIC  9(005).
             05 OUT331-G-COD-COMP-ANIA      PIC  9(005).
             05 OUT331-G-NUM-POL-I          PIC  X(040).
             05 OUT331-G-NUM-POL-C          PIC  X(040).
             05 OUT331-G-NUM-ADE            PIC  X(040).
             05 OUT331-G-NUM-GAR            PIC  X(040).
             05 OUT331-G-COGN-ASSTO         PIC  X(100).
             05 OUT331-G-NOME-ASSTO         PIC  X(100).
             05 OUT331-G-CODFISC-PIVA       PIC  X(016).
             05 OUT331-G-DT-ELAB            PIC  9(008).
             05 OUT331-G-DT-INI-PERIODO     PIC  9(008).
             05 OUT331-G-DT-END-PERIODO     PIC  9(008).
             05 FILLER                      PIC  X(038).
      *
          03 OUT331-REC-DET                 REDEFINES  OUT331-REC-OUT.
      *
             05 OUT331-D-COD-TARI           PIC  X(012).
             05 OUT331-D-NUM-TGA            PIC  X(040).
             05 OUT331-D-IMPST-SOST         PIC -9(012)V9(003).
             05 OUT331-D-IMPB-IS-CALC       PIC -9(012)V9(003).
             05 OUT331-D-IMPB-IS-RID        PIC -9(012)V9(003).
             05 OUT331-D-PRSTZ-LRD-ANTE-IS  PIC -9(012)V9(003).
             05 OUT331-D-RIS-MAT-NET-PREC   PIC -9(012)V9(003).
             05 OUT331-D-RIS-MAT-ANTE-TAX   PIC -9(012)V9(003).
             05 OUT331-D-PRSTZ-NET          PIC -9(012)V9(003).
             05 OUT331-D-PRSTZ-PREC         PIC -9(012)V9(003).
             05 OUT331-D-RIS-MAT-POST-TAX   PIC -9(012)V9(003).
             05 OUT331-D-CUM-PRE-VERS       PIC -9(012)V9(003).
             05 OUT331-D-DERIV-DA-RIS       PIC X(1).
             05 FILLER                      PIC X(237).
      *      05 FILLER                      PIC X(247).
      *
          03 OUT331-REC-TOT                 REDEFINES  OUT331-REC-OUT.
      *
             05 OUT331-T-CUM-PRE-VERS       PIC -9(012)V9(003).
             05 OUT331-T-IMPST-SOST         PIC -9(012)V9(003).
             05 OUT331-T-IMPB-IS-CALC       PIC -9(012)V9(003).
             05 OUT331-T-IMPB-IS-RID        PIC -9(012)V9(003).
             05 OUT331-T-RIS-MAT-NET-PREC   PIC -9(012)V9(003).
             05 OUT331-T-RIS-MAT-ANTE-TAX   PIC -9(012)V9(003).
             05 OUT331-T-CREDITO-IS-PREC    PIC -9(012)V9(003).
             05 OUT331-T-CUM-IMPB           PIC -9(012)V9(003).
             05 OUT331-T-CUM-IMPST          PIC -9(012)V9(003).
             05 OUT331-T-CUM-IMPB-GIA-TASS  PIC -9(012)V9(003).
             05 OUT331-T-CUM-DERIV-DA-RIS   PIC -9(012)V9(003).
             05 FILLER                      PIC  X(274).
      *      05 FILLER                      PIC  X(285).
      *
          03 FILLER                         PIC  X(047).

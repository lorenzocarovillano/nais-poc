      *----------------------------------------------------------------*
      *    COPY      ..... LCCVGRL6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO GARANZIA DI LIQUIDAZIONE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-GAR-LIQ.

      *--> TABELLA STORICA
           INITIALIZE GAR-LIQ.

      *--> NOME TABELLA FISICA DB
           MOVE 'GAR-LIQ'               TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WGRL-ST-INV(IX-TAB-GRL)
              AND WGRL-ELE-GRL-MAX NOT = 0

              EVALUATE TRUE
      *-->      INSERIMENTO
                WHEN WGRL-ST-ADD(IX-TAB-GRL)

                  PERFORM ESTR-SEQUENCE
                     THRU ESTR-SEQUENCE-EX

      *-->        VALORIZZAZIONE TABELLA LIQ
                  IF IDSV0001-ESITO-OK
                     MOVE S090-SEQ-TABELLA    TO WGRL-ID-PTF(IX-TAB-GRL)
                                                 GRL-ID-GAR-LIQ

      *-->           VALORIZZAZIONE  ID TAB.GARANZIA DI LIQUIDAZIONE
                     MOVE WLQU-ID-PTF(IX-TAB-LQU)  TO GRL-ID-LIQ
                     MOVE WGRL-ID-GAR(IX-TAB-GRL)  TO GRL-ID-GAR
                     MOVE WMOV-ID-PTF              TO GRL-ID-MOVI-CRZ
                     MOVE WMOV-DT-EFF              TO GRL-DT-INI-EFF
                  END-IF

      *-->        TIPO OPERAZIONE DISPATCHER
                  SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->      CANCELLAZIONE
                WHEN WGRL-ST-DEL(IX-TAB-GRL)

                  MOVE WGRL-ID-PTF(IX-TAB-GRL)  TO GRL-ID-GAR-LIQ
                  MOVE WLQU-ID-PTF(IX-TAB-LQU)  TO GRL-ID-LIQ
                  MOVE WGRL-ID-GAR(IX-TAB-GRL)  TO GRL-ID-GAR
                  MOVE WMOV-ID-MOVI             TO GRL-ID-MOVI-CHIU
                  MOVE WMOV-DT-EFF              TO GRL-DT-END-EFF

      *-->        TIPO OPERAZIONE DISPATCHER
                  SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->      MODIFICA
                WHEN WGRL-ST-MOD(IX-TAB-GRL)

                  MOVE WGRL-ID-PTF(IX-TAB-GRL)  TO GRL-ID-GAR-LIQ
                  MOVE WLQU-ID-PTF(IX-TAB-LQU)  TO GRL-ID-LIQ
                  MOVE WGRL-ID-GAR(IX-TAB-GRL)  TO GRL-ID-GAR
                  MOVE WMOV-ID-PTF              TO GRL-ID-MOVI-CRZ
                  MOVE WMOV-DT-EFF              TO GRL-DT-INI-EFF

      *-->        TIPO OPERAZIONE DISPATCHER
                  SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN GARANZIA DI LIQUIDAZIONE
                 PERFORM VAL-DCLGEN-GRL
                    THRU VAL-DCLGEN-GRL-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-GRL
                    THRU VALORIZZA-AREA-DSH-GRL-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-GAR-LIQ-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-GRL.

      *--> DCLGEN TABELLA
           MOVE GAR-LIQ                   TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID               TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT  TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-GRL-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVGRL5.

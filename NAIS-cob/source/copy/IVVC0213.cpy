      *----------------------------------------------------------------*
      * AREA CHIAMATA MODULI DI CALCOLO VALORIZZATORE
      *----------------------------------------------------------------*
      *
           05 (SF)-COD-PARAMETRO                 PIC  X(020).
           05 (SF)-ID-POLIZZA                    PIC  9(009).
           05 (SF)-ID-ADESIONE                   PIC  9(009).
           05 (SF)-ID-GARANZIA                   PIC  9(009).
           05 (SF)-ID-TRANCHE                    PIC  9(009).
           05 (SF)-ID-TIT-CONT                   PIC  9(009).
           05 (SF)-TIPO-MOVIMENTO                PIC  9(005).
           05 (SF)-TIPO-MOVI-ORIG                PIC  9(005).
           05 (SF)-IX-TABB                       PIC  9(004).
           05 (SF)-DATA-EFFETTO                  PIC  9(008).
           05 (SF)-DATA-COMPETENZA               PIC S9(18) COMP-3.
           05 (SF)-DATA-COMP-AGG-STOR            PIC S9(18) COMP-3.

           05 (SF)-COD-COMPAGNIA-ANIA            PIC  9(005).
      *
           05 (SF)-TAB-LIVELLO.
              07 (SF)-ID-POL-GAR                 PIC  9(09).
              07 (SF)-DATI-LIVELLO.
                 09 (SF)-TP-LIVELLO              PIC  X(01).
                 09 (SF)-COD-LIVELLO             PIC  X(12).
      * INFORMAZIONI DI TRANCHE
                 09 (SF)-ID-LIVELLO              PIC 9(09).
                 09 (SF)-DT-INIZ-TARI            PIC X(008).
                 09 (SF)-DT-INIZ-PROD            PIC X(008).
                 09 (SF)-COD-RGM-FISC            PIC X(002).
                 09 (SF)-DT-DECOR                PIC X(008).
      *AREA VARIABILI PRODOTTO
                 09 (SF)-AREA-VARIABILI.
                    11 (SF)-TAB-VARIABILI.
                     12 (SF)-AREA-VARIABILE.
                       13 (SF)-COD-VARIABILE     PIC  X(12).
                       13 (SF)-TP-DATO           PIC  X(01).
                       13 (SF)-VAL-IMP           PIC  S9(11)V9(07).
                       13 (SF)-VAL-PERC          PIC   9(05)V9(09).
                       13 (SF)-VAL-STR           PIC  X(12).
                 09 (SF)-NOME-SERVIZIO           PIC  X(08).
      *
           05 (SF)-WHERE-COND                PIC X(300).
           05 (SF)-LUNGHEZZA-TOT             PIC S9(09) COMP-3.
           05 (SF)-ELE-INFO-MAX              PIC S9(04) COMP-3.
           05 (SF)-TAB-INFO                  OCCURS 10.
              07 (SF)-TAB-ALIAS              PIC  X(03).
              07 (SF)-NUM-OCCORRENZE         PIC S9(05) COMP-3.
              07 (SF)-NUM-ELE-TAB-ALIAS      PIC S9(05) COMP-3.
              07 (SF)-POSIZ-INI              PIC S9(09) COMP-3.
              07 (SF)-LUNGHEZZA              PIC S9(09) COMP-3.

           05 (SF)-BUFFER-DATI               PIC  X(110000).

           05 (SF)-MODALITA-ID-POL           PIC  X(1).
              88 (SF)-ID-POL-FITTZIO         VALUE 'F'.
              88 (SF)-ID-POL-AUTENTICO       VALUE 'A'.
           05 (SF)-MODALITA-ID-ADE           PIC  X(1).
              88 (SF)-ID-ADE-FITTZIO         VALUE 'F'.
              88 (SF)-ID-ADE-AUTENTICO       VALUE 'A'.
           05 (SF)-MODALITA-ID-GRZ           PIC  X(1).
              88 (SF)-ID-GRZ-FITTZIO         VALUE 'F'.
              88 (SF)-ID-GRZ-AUTENTICO       VALUE 'A'.
           05 (SF)-MODALITA-ID-TGA           PIC  X(1).
              88 (SF)-ID-TGA-FITTZIO         VALUE 'F'.
              88 (SF)-ID-TGA-AUTENTICO       VALUE 'A'.
           05 (SF)-MODALITA-ID-TIT           PIC  X(1).
              88 (SF)-ID-TIT-FITTZIO         VALUE 'F'.
              88 (SF)-ID-TIT-AUTENTICO       VALUE 'A'.


           05 (SF)-SALTA-VALORIZZAZIONE      PIC  X(1).
              88 (SF)-SALTA-SI               VALUE 'S'.
              88 (SF)-SALTA-NO               VALUE 'N'.
      *
           05 (SF)-TAB-OUTPUT.
              07 (SF)-COD-VARIABILE-O        PIC  X(12).
              07 (SF)-TP-DATO-O              PIC  X(01).
              07 (SF)-VAL-IMP-O              PIC  S9(11)V9(07).
              07 (SF)-VAL-PERC-O             PIC   9(05)V9(09).
              07 (SF)-VAL-STR-O              PIC  X(12).
      *
           05 (SF)-FLG-AREA                      PIC  X(02).
              88 (SF)-AREA-VE                    VALUE 'VE'.
              88 (SF)-AREA-PV                    VALUE 'PV'.

           05 FILLER                         PIC X(203).

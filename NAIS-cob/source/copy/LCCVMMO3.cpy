
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVMMO3
      *   ULTIMO AGG. 23 GEN 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-MMO.
           MOVE MMO-ID-MATR-MOVIMENTO
             TO (SF)-ID-PTF(IX-TAB-MMO)
           MOVE MMO-ID-MATR-MOVIMENTO
             TO (SF)-ID-MATR-MOVIMENTO(IX-TAB-MMO)
           MOVE MMO-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-MMO)
           MOVE MMO-TP-MOVI-PTF
             TO (SF)-TP-MOVI-PTF(IX-TAB-MMO)
           MOVE MMO-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-MMO)
           IF MMO-TP-FRM-ASSVA-NULL = HIGH-VALUES
              MOVE MMO-TP-FRM-ASSVA-NULL
                TO (SF)-TP-FRM-ASSVA-NULL(IX-TAB-MMO)
           ELSE
              MOVE MMO-TP-FRM-ASSVA
                TO (SF)-TP-FRM-ASSVA(IX-TAB-MMO)
           END-IF.
           MOVE MMO-TP-MOVI-ACT
             TO (SF)-TP-MOVI-ACT(IX-TAB-MMO)
           IF MMO-AMMISSIBILITA-MOVI-NULL = HIGH-VALUES
              MOVE MMO-AMMISSIBILITA-MOVI-NULL
                TO (SF)-AMMISSIBILITA-MOVI-NULL(IX-TAB-MMO)
           ELSE
              MOVE MMO-AMMISSIBILITA-MOVI
                TO (SF)-AMMISSIBILITA-MOVI(IX-TAB-MMO)
           END-IF.
           IF MMO-SERVIZIO-CONTROLLO-NULL = HIGH-VALUES
              MOVE MMO-SERVIZIO-CONTROLLO-NULL
                TO (SF)-SERVIZIO-CONTROLLO-NULL(IX-TAB-MMO)
           ELSE
              MOVE MMO-SERVIZIO-CONTROLLO
                TO (SF)-SERVIZIO-CONTROLLO(IX-TAB-MMO)
           END-IF.
           IF MMO-COD-PROCESSO-WF-NULL = HIGH-VALUES
              MOVE MMO-COD-PROCESSO-WF-NULL
                TO (SF)-COD-PROCESSO-WF-NULL(IX-TAB-MMO)
           ELSE
              MOVE MMO-COD-PROCESSO-WF
                TO (SF)-COD-PROCESSO-WF(IX-TAB-MMO)
           END-IF.
       VALORIZZA-OUTPUT-MMO-EX.
           EXIT.

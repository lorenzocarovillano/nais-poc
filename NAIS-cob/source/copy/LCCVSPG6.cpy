      *----------------------------------------------------------------*
      *    COPY      ..... LCCVSPG6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO SOPRAPREMIO DI GARANZIA
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-SOPR-DI-GAR.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE SOPR-DI-GAR

      *--> NOME TABELLA FISICA DB
           MOVE 'SOPR-DI-GAR'                 TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WSPG-ST-INV(IX-TAB-SPG)
              AND WSPG-ELE-SOPRE-MAX NOT = 0

      *--->   Impostare ID Tabella SOPRAPREMIO DI GARANZIA
              MOVE WMOV-ID-PTF                TO SPG-ID-MOVI-CRZ

      *--->   RICERCA ID-GARANZIA NELLA TABELLA PADRE(GARANZIA)
              MOVE WSPG-ID-GAR(IX-TAB-SPG)  TO WS-ID-GARANZIA
              PERFORM RICERCA-ID-GARANZIA
                 THRU RICERCA-ID-GARANZIA-EX

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WSPG-ST-ADD(IX-TAB-SPG)

                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA
                            TO WSPG-ID-PTF(IX-TAB-SPG)
                               SPG-ID-SOPR-DI-GAR
                          MOVE WS-ID-PTF-GRZ
                            TO SPG-ID-GAR
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WSPG-ST-DEL(IX-TAB-SPG)

                       MOVE WSPG-ID-PTF(IX-TAB-SPG)
                         TO SPG-ID-SOPR-DI-GAR
                       MOVE WS-ID-PTF-GRZ  TO SPG-ID-GAR

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->        MODIFICA
                  WHEN WSPG-ST-MOD(IX-TAB-SPG)
                       MOVE WSPG-ID-PTF(IX-TAB-SPG)
                         TO SPG-ID-SOPR-DI-GAR
                       MOVE WS-ID-PTF-GRZ  TO SPG-ID-GAR

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN SOPRAPREMIO DI GARANZIA
                 PERFORM VAL-DCLGEN-SPG
                    THRU VAL-DCLGEN-SPG-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-SPG
                    THRU VALORIZZA-AREA-DSH-SPG-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-SOPR-DI-GAR-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    RICERCA ID-GAR NELLA TABELLA PADRE(GARANZIA)
      *----------------------------------------------------------------*
       RICERCA-ID-GARANZIA.

           SET NON-TROVATO TO TRUE

           PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
             UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
                OR TROVATO

                IF WS-ID-GARANZIA = WGRZ-ID-GAR(IX-TAB-GRZ)
                   MOVE WGRZ-ID-PTF(IX-TAB-GRZ) TO WS-ID-PTF-GRZ
                   SET TROVATO                  TO TRUE
                END-IF

           END-PERFORM.

       RICERCA-ID-GARANZIA-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-SPG.

      *--> DCLGEN TABELLA
           MOVE SOPR-DI-GAR             TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-SPG-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVSPG5.

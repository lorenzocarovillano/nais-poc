      *----------------------------------------------------------------*
      *    COPY      ..... LCCVDAD6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO DATI DEFAULT ADESIONE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVDAD5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCVDAD1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-DFLT-ADES.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE DFLT-ADES

      *--> NOME TABELLA FISICA DB
           MOVE 'DFLT-ADES'                   TO WK-TABELLA

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WDAD-ST-INV
              AND WDAD-ELE-DAD-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WDAD-ST-ADD

      *-->             ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA     TO WDAD-ID-PTF
                          MOVE WDAD-ID-PTF          TO DAD-ID-DFLT-ADES
                          MOVE WPOL-ID-PTF          TO DAD-ID-POLI
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-INSERT               TO TRUE

      *-->        UPDATE
                  WHEN WDAD-ST-MOD

                       MOVE WDAD-ID-PTF   TO DAD-ID-DFLT-ADES
                       MOVE WPOL-ID-PTF   TO DAD-ID-POLI

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-UPDATE               TO TRUE

      *-->        DELETE
                  WHEN WDAD-ST-DEL

                       MOVE WDAD-ID-PTF   TO DAD-ID-DFLT-ADES
                       MOVE WPOL-ID-PTF   TO DAD-ID-POLI

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE               TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN
                 PERFORM VAL-DCLGEN-DAD
                    THRU VAL-DCLGEN-DAD-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-DAD
                    THRU VALORIZZA-AREA-DSH-DAD-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-DFLT-ADES-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-DAD.

      *--> DCLGEN TABELLA
           MOVE DFLT-ADES               TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-PRIMARY-KEY          TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-SENZA-STOR     TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-DAD-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVDAD5.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA COMP_NUM_OGG
      *   ALIAS CNO
      *   ULTIMO AGG. 16 MAR 2009
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-DATI.
             07 (SF)-COD-COMPAGNIA-ANIA PIC S9(5)     COMP-3.
             07 (SF)-FORMA-ASSICURATIVA PIC X(2).
             07 (SF)-COD-OGGETTO PIC X(30).
             07 (SF)-POSIZIONE PIC S9(5)     COMP-3.
             07 (SF)-COD-STR-DATO PIC X(30).
             07 (SF)-COD-STR-DATO-NULL REDEFINES
                (SF)-COD-STR-DATO   PIC X(30).
             07 (SF)-COD-DATO PIC X(30).
             07 (SF)-COD-DATO-NULL REDEFINES
                (SF)-COD-DATO   PIC X(30).
             07 (SF)-VALORE-DEFAULT PIC X(50).
             07 (SF)-VALORE-DEFAULT-NULL REDEFINES
                (SF)-VALORE-DEFAULT   PIC X(50).
             07 (SF)-LUNGHEZZA-DATO PIC S9(5)     COMP-3.
             07 (SF)-LUNGHEZZA-DATO-NULL REDEFINES
                (SF)-LUNGHEZZA-DATO   PIC X(3).
             07 (SF)-FLAG-KEY-ULT-PROGR PIC X(1).
             07 (SF)-FLAG-KEY-ULT-PROGR-NULL REDEFINES
                (SF)-FLAG-KEY-ULT-PROGR   PIC X(1).

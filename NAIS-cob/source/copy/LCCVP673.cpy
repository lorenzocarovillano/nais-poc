
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVP673
      *   ULTIMO AGG. 11 MAR 2014
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-P67.
           MOVE P67-ID-EST-POLI-CPI-PR
             TO (SF)-ID-PTF
           MOVE P67-ID-EST-POLI-CPI-PR
             TO (SF)-ID-EST-POLI-CPI-PR
           MOVE P67-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ
           IF P67-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE P67-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL
           ELSE
              MOVE P67-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU
           END-IF
           MOVE P67-DT-INI-EFF
             TO (SF)-DT-INI-EFF
           MOVE P67-DT-END-EFF
             TO (SF)-DT-END-EFF
           MOVE P67-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE P67-IB-OGG
             TO (SF)-IB-OGG
           MOVE P67-DS-RIGA
             TO (SF)-DS-RIGA
           MOVE P67-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE P67-DS-VER
             TO (SF)-DS-VER
           MOVE P67-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ
           MOVE P67-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ
           MOVE P67-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE P67-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           MOVE P67-COD-PROD-ESTNO
             TO (SF)-COD-PROD-ESTNO
           IF P67-CPT-FIN-NULL = HIGH-VALUES
              MOVE P67-CPT-FIN-NULL
                TO (SF)-CPT-FIN-NULL
           ELSE
              MOVE P67-CPT-FIN
                TO (SF)-CPT-FIN
           END-IF
           IF P67-NUM-TST-FIN-NULL = HIGH-VALUES
              MOVE P67-NUM-TST-FIN-NULL
                TO (SF)-NUM-TST-FIN-NULL
           ELSE
              MOVE P67-NUM-TST-FIN
                TO (SF)-NUM-TST-FIN
           END-IF
           IF P67-TS-FINANZ-NULL = HIGH-VALUES
              MOVE P67-TS-FINANZ-NULL
                TO (SF)-TS-FINANZ-NULL
           ELSE
              MOVE P67-TS-FINANZ
                TO (SF)-TS-FINANZ
           END-IF
           IF P67-DUR-MM-FINANZ-NULL = HIGH-VALUES
              MOVE P67-DUR-MM-FINANZ-NULL
                TO (SF)-DUR-MM-FINANZ-NULL
           ELSE
              MOVE P67-DUR-MM-FINANZ
                TO (SF)-DUR-MM-FINANZ
           END-IF
           IF P67-DT-END-FINANZ-NULL = HIGH-VALUES
              MOVE P67-DT-END-FINANZ-NULL
                TO (SF)-DT-END-FINANZ-NULL
           ELSE
              MOVE P67-DT-END-FINANZ
                TO (SF)-DT-END-FINANZ
           END-IF
           IF P67-AMM-1A-RAT-NULL = HIGH-VALUES
              MOVE P67-AMM-1A-RAT-NULL
                TO (SF)-AMM-1A-RAT-NULL
           ELSE
              MOVE P67-AMM-1A-RAT
                TO (SF)-AMM-1A-RAT
           END-IF
           IF P67-VAL-RISC-BENE-NULL = HIGH-VALUES
              MOVE P67-VAL-RISC-BENE-NULL
                TO (SF)-VAL-RISC-BENE-NULL
           ELSE
              MOVE P67-VAL-RISC-BENE
                TO (SF)-VAL-RISC-BENE
           END-IF
           IF P67-AMM-RAT-END-NULL = HIGH-VALUES
              MOVE P67-AMM-RAT-END-NULL
                TO (SF)-AMM-RAT-END-NULL
           ELSE
              MOVE P67-AMM-RAT-END
                TO (SF)-AMM-RAT-END
           END-IF
           IF P67-TS-CRE-RAT-FINANZ-NULL = HIGH-VALUES
              MOVE P67-TS-CRE-RAT-FINANZ-NULL
                TO (SF)-TS-CRE-RAT-FINANZ-NULL
           ELSE
              MOVE P67-TS-CRE-RAT-FINANZ
                TO (SF)-TS-CRE-RAT-FINANZ
           END-IF
           IF P67-IMP-FIN-REVOLVING-NULL = HIGH-VALUES
              MOVE P67-IMP-FIN-REVOLVING-NULL
                TO (SF)-IMP-FIN-REVOLVING-NULL
           ELSE
              MOVE P67-IMP-FIN-REVOLVING
                TO (SF)-IMP-FIN-REVOLVING
           END-IF
           IF P67-IMP-UTIL-C-REV-NULL = HIGH-VALUES
              MOVE P67-IMP-UTIL-C-REV-NULL
                TO (SF)-IMP-UTIL-C-REV-NULL
           ELSE
              MOVE P67-IMP-UTIL-C-REV
                TO (SF)-IMP-UTIL-C-REV
           END-IF
           IF P67-IMP-RAT-REVOLVING-NULL = HIGH-VALUES
              MOVE P67-IMP-RAT-REVOLVING-NULL
                TO (SF)-IMP-RAT-REVOLVING-NULL
           ELSE
              MOVE P67-IMP-RAT-REVOLVING
                TO (SF)-IMP-RAT-REVOLVING
           END-IF
           IF P67-DT-1O-UTLZ-C-REV-NULL = HIGH-VALUES
              MOVE P67-DT-1O-UTLZ-C-REV-NULL
                TO (SF)-DT-1O-UTLZ-C-REV-NULL
           ELSE
              MOVE P67-DT-1O-UTLZ-C-REV
                TO (SF)-DT-1O-UTLZ-C-REV
           END-IF
           IF P67-IMP-ASSTO-NULL = HIGH-VALUES
              MOVE P67-IMP-ASSTO-NULL
                TO (SF)-IMP-ASSTO-NULL
           ELSE
              MOVE P67-IMP-ASSTO
                TO (SF)-IMP-ASSTO
           END-IF
           IF P67-PRE-VERS-NULL = HIGH-VALUES
              MOVE P67-PRE-VERS-NULL
                TO (SF)-PRE-VERS-NULL
           ELSE
              MOVE P67-PRE-VERS
                TO (SF)-PRE-VERS
           END-IF
           IF P67-DT-SCAD-COP-NULL = HIGH-VALUES
              MOVE P67-DT-SCAD-COP-NULL
                TO (SF)-DT-SCAD-COP-NULL
           ELSE
              MOVE P67-DT-SCAD-COP
                TO (SF)-DT-SCAD-COP
           END-IF
           IF P67-GG-DEL-MM-SCAD-RAT-NULL = HIGH-VALUES
              MOVE P67-GG-DEL-MM-SCAD-RAT-NULL
                TO (SF)-GG-DEL-MM-SCAD-RAT-NULL
           ELSE
              MOVE P67-GG-DEL-MM-SCAD-RAT
                TO (SF)-GG-DEL-MM-SCAD-RAT
           END-IF
           MOVE P67-FL-PRE-FIN
             TO (SF)-FL-PRE-FIN
           IF P67-DT-SCAD-1A-RAT-NULL = HIGH-VALUES
              MOVE P67-DT-SCAD-1A-RAT-NULL
                TO (SF)-DT-SCAD-1A-RAT-NULL
           ELSE
              MOVE P67-DT-SCAD-1A-RAT
                TO (SF)-DT-SCAD-1A-RAT
           END-IF
           IF P67-DT-EROG-FINANZ-NULL = HIGH-VALUES
              MOVE P67-DT-EROG-FINANZ-NULL
                TO (SF)-DT-EROG-FINANZ-NULL
           ELSE
              MOVE P67-DT-EROG-FINANZ
                TO (SF)-DT-EROG-FINANZ
           END-IF
           IF P67-DT-STIPULA-FINANZ-NULL = HIGH-VALUES
              MOVE P67-DT-STIPULA-FINANZ-NULL
                TO (SF)-DT-STIPULA-FINANZ-NULL
           ELSE
              MOVE P67-DT-STIPULA-FINANZ
                TO (SF)-DT-STIPULA-FINANZ
           END-IF
           IF P67-MM-PREAMM-NULL = HIGH-VALUES
              MOVE P67-MM-PREAMM-NULL
                TO (SF)-MM-PREAMM-NULL
           ELSE
              MOVE P67-MM-PREAMM
                TO (SF)-MM-PREAMM
           END-IF
           IF P67-IMP-DEB-RES-NULL = HIGH-VALUES
              MOVE P67-IMP-DEB-RES-NULL
                TO (SF)-IMP-DEB-RES-NULL
           ELSE
              MOVE P67-IMP-DEB-RES
                TO (SF)-IMP-DEB-RES
           END-IF
           IF P67-IMP-RAT-FINANZ-NULL = HIGH-VALUES
              MOVE P67-IMP-RAT-FINANZ-NULL
                TO (SF)-IMP-RAT-FINANZ-NULL
           ELSE
              MOVE P67-IMP-RAT-FINANZ
                TO (SF)-IMP-RAT-FINANZ
           END-IF
           IF P67-IMP-CANONE-ANTIC-NULL = HIGH-VALUES
              MOVE P67-IMP-CANONE-ANTIC-NULL
                TO (SF)-IMP-CANONE-ANTIC-NULL
           ELSE
              MOVE P67-IMP-CANONE-ANTIC
                TO (SF)-IMP-CANONE-ANTIC
           END-IF
           IF P67-PER-RAT-FINANZ-NULL = HIGH-VALUES
              MOVE P67-PER-RAT-FINANZ-NULL
                TO (SF)-PER-RAT-FINANZ-NULL
           ELSE
              MOVE P67-PER-RAT-FINANZ
                TO (SF)-PER-RAT-FINANZ
           END-IF
           IF P67-TP-MOD-PAG-RAT-NULL = HIGH-VALUES
              MOVE P67-TP-MOD-PAG-RAT-NULL
                TO (SF)-TP-MOD-PAG-RAT-NULL
           ELSE
              MOVE P67-TP-MOD-PAG-RAT
                TO (SF)-TP-MOD-PAG-RAT
           END-IF
           IF P67-TP-FINANZ-ER-NULL = HIGH-VALUES
              MOVE P67-TP-FINANZ-ER-NULL
                TO (SF)-TP-FINANZ-ER-NULL
           ELSE
              MOVE P67-TP-FINANZ-ER
                TO (SF)-TP-FINANZ-ER
           END-IF
           IF P67-CAT-FINANZ-ER-NULL = HIGH-VALUES
              MOVE P67-CAT-FINANZ-ER-NULL
                TO (SF)-CAT-FINANZ-ER-NULL
           ELSE
              MOVE P67-CAT-FINANZ-ER
                TO (SF)-CAT-FINANZ-ER
           END-IF
           IF P67-VAL-RISC-END-LEAS-NULL = HIGH-VALUES
              MOVE P67-VAL-RISC-END-LEAS-NULL
                TO (SF)-VAL-RISC-END-LEAS-NULL
           ELSE
              MOVE P67-VAL-RISC-END-LEAS
                TO (SF)-VAL-RISC-END-LEAS
           END-IF
           IF P67-DT-EST-FINANZ-NULL = HIGH-VALUES
              MOVE P67-DT-EST-FINANZ-NULL
                TO (SF)-DT-EST-FINANZ-NULL
           ELSE
              MOVE P67-DT-EST-FINANZ
                TO (SF)-DT-EST-FINANZ
           END-IF
           IF P67-DT-MAN-COP-NULL = HIGH-VALUES
              MOVE P67-DT-MAN-COP-NULL
                TO (SF)-DT-MAN-COP-NULL
           ELSE
              MOVE P67-DT-MAN-COP
                TO (SF)-DT-MAN-COP
           END-IF
           IF P67-NUM-FINANZ-NULL = HIGH-VALUES
              MOVE P67-NUM-FINANZ-NULL
                TO (SF)-NUM-FINANZ-NULL
           ELSE
              MOVE P67-NUM-FINANZ
                TO (SF)-NUM-FINANZ
           END-IF
           IF P67-TP-MOD-ACQS-NULL = HIGH-VALUES
              MOVE P67-TP-MOD-ACQS-NULL
                TO (SF)-TP-MOD-ACQS-NULL
           ELSE
              MOVE P67-TP-MOD-ACQS
                TO (SF)-TP-MOD-ACQS
           END-IF.
       VALORIZZA-OUTPUT-P67-EX.
           EXIT.

           EXEC SQL DECLARE ACC_COMM TABLE
           (
             ID_ACC_COMM         DECIMAL(9, 0) NOT NULL,
             TP_ACC_COMM         CHAR(2) NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             COD_PARTNER         DECIMAL(10, 0) NOT NULL,
             IB_ACC_COMM         CHAR(40) NOT NULL,
             IB_ACC_COMM_MASTER  CHAR(40) NOT NULL,
             DESC_ACC_COMM       VARCHAR(100) NOT NULL,
             DT_FIRMA            DATE NOT NULL,
             DT_INI_VLDT         DATE NOT NULL,
             DT_END_VLDT         DATE NOT NULL,
             FL_INVIO_CONFERME   CHAR(1) NOT NULL,
             DS_OPER_SQL         CHAR(1) NOT NULL,
             DS_VER              DECIMAL(9, 0) NOT NULL,
             DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL,
             DS_UTENTE           CHAR(20) NOT NULL,
             DS_STATO_ELAB       CHAR(1) NOT NULL,
             DESC_ACC_COMM_MAST  VARCHAR(100)
          ) END-EXEC.

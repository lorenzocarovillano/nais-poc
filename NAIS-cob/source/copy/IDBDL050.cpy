           EXEC SQL DECLARE AMMB_FUNZ_FUNZ TABLE
           (
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             TP_MOVI_ESEC        DECIMAL(5, 0) NOT NULL,
             TP_MOVI_RIFTO       DECIMAL(5, 0) NOT NULL,
             GRAV_FUNZ_FUNZ      CHAR(2) NOT NULL,
             DS_OPER_SQL         CHAR(1) NOT NULL,
             DS_VER              DECIMAL(9, 0) NOT NULL,
             DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL,
             DS_UTENTE           CHAR(20) NOT NULL,
             DS_STATO_ELAB       CHAR(1) NOT NULL,
             COD_BLOCCO          CHAR(5),
             SRVZ_VER_ANN        CHAR(8),
             WHERE_CONDITION     VARCHAR(300),
             FL_POLI_IFP         CHAR(1)
          ) END-EXEC.

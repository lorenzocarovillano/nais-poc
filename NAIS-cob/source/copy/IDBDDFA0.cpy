           EXEC SQL DECLARE D_FISC_ADES TABLE
           (
             ID_D_FISC_ADES      DECIMAL(9, 0) NOT NULL,
             ID_ADES             DECIMAL(9, 0) NOT NULL,
             ID_MOVI_CRZ         DECIMAL(9, 0) NOT NULL,
             ID_MOVI_CHIU        DECIMAL(9, 0),
             DT_INI_EFF          DATE NOT NULL,
             DT_END_EFF          DATE NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             TP_ISC_FND          CHAR(2),
             DT_ACCNS_RAPP_FND   DATE,
             IMP_CNBT_AZ_K1      DECIMAL(15, 3),
             IMP_CNBT_ISC_K1     DECIMAL(15, 3),
             IMP_CNBT_TFR_K1     DECIMAL(15, 3),
             IMP_CNBT_VOL_K1     DECIMAL(15, 3),
             IMP_CNBT_AZ_K2      DECIMAL(15, 3),
             IMP_CNBT_ISC_K2     DECIMAL(15, 3),
             IMP_CNBT_TFR_K2     DECIMAL(15, 3),
             IMP_CNBT_VOL_K2     DECIMAL(15, 3),
             MATU_K1             DECIMAL(15, 3),
             MATU_RES_K1         DECIMAL(15, 3),
             MATU_K2             DECIMAL(15, 3),
             IMPB_VIS            DECIMAL(15, 3),
             IMPST_VIS           DECIMAL(15, 3),
             IMPB_IS_K2          DECIMAL(15, 3),
             IMPST_SOST_K2       DECIMAL(15, 3),
             RIDZ_TFR            DECIMAL(15, 3),
             PC_TFR              DECIMAL(6, 3),
             ALQ_TFR             DECIMAL(6, 3),
             TOT_ANTIC           DECIMAL(15, 3),
             IMPB_TFR_ANTIC      DECIMAL(15, 3),
             IMPST_TFR_ANTIC     DECIMAL(15, 3),
             RIDZ_TFR_SU_ANTIC   DECIMAL(15, 3),
             IMPB_VIS_ANTIC      DECIMAL(15, 3),
             IMPST_VIS_ANTIC     DECIMAL(15, 3),
             IMPB_IS_K2_ANTIC    DECIMAL(15, 3),
             IMPST_SOST_K2_ANTI  DECIMAL(15, 3),
             ULT_COMMIS_TRASFE   DECIMAL(15, 3),
             COD_DVS             CHAR(20),
             ALQ_PRVR            DECIMAL(6, 3),
             PC_ESE_IMPST_TFR    DECIMAL(6, 3),
             IMP_ESE_IMPST_TFR   DECIMAL(15, 3),
             ANZ_CNBTVA_CARASS   DECIMAL(4, 0),
             ANZ_CNBTVA_CARAZI   DECIMAL(4, 0),
             ANZ_SRVZ            DECIMAL(4, 0),
             IMP_CNBT_NDED_K1    DECIMAL(15, 3),
             IMP_CNBT_NDED_K2    DECIMAL(15, 3),
             IMP_CNBT_NDED_K3    DECIMAL(15, 3),
             IMP_CNBT_AZ_K3      DECIMAL(15, 3),
             IMP_CNBT_ISC_K3     DECIMAL(15, 3),
             IMP_CNBT_TFR_K3     DECIMAL(15, 3),
             IMP_CNBT_VOL_K3     DECIMAL(15, 3),
             MATU_K3             DECIMAL(15, 3),
             IMPB_252_ANTIC      DECIMAL(15, 3),
             IMPST_252_ANTIC     DECIMAL(15, 3),
             DT_1A_CNBZ          DATE,
             COMMIS_DI_TRASFE    DECIMAL(15, 3),
             AA_CNBZ_K1          DECIMAL(4, 0),
             AA_CNBZ_K2          DECIMAL(4, 0),
             AA_CNBZ_K3          DECIMAL(4, 0),
             MM_CNBZ_K1          DECIMAL(4, 0),
             MM_CNBZ_K2          DECIMAL(4, 0),
             MM_CNBZ_K3          DECIMAL(4, 0),
             FL_APPLZ_NEWFIS     CHAR(1),
             REDT_TASS_ABBAT_K3  DECIMAL(15, 3),
             CNBT_ECC_4X100_K1   DECIMAL(15, 3),
             DS_RIGA             DECIMAL(10, 0) NOT NULL,
             DS_OPER_SQL         CHAR(1) NOT NULL,
             DS_VER              DECIMAL(9, 0) NOT NULL,
             DS_TS_INI_CPTZ      DECIMAL(18, 0) NOT NULL,
             DS_TS_END_CPTZ      DECIMAL(18, 0) NOT NULL,
             DS_UTENTE           CHAR(20) NOT NULL,
             DS_STATO_ELAB       CHAR(1) NOT NULL,
             CREDITO_IS          DECIMAL(15, 3),
             REDT_TASS_ABBAT_K2  DECIMAL(15, 3),
             IMPB_IS_K3          DECIMAL(15, 3),
             IMPST_SOST_K3       DECIMAL(15, 3),
             IMPB_252_K3         DECIMAL(15, 3),
             IMPST_252_K3        DECIMAL(15, 3),
             IMPB_IS_K3_ANTIC    DECIMAL(15, 3),
             IMPST_SOST_K3_ANTI  DECIMAL(15, 3),
             IMPB_IRPEF_K1_ANTI  DECIMAL(15, 3),
             IMPST_IRPEF_K1_ANT  DECIMAL(15, 3),
             IMPB_IRPEF_K2_ANTI  DECIMAL(15, 3),
             IMPST_IRPEF_K2_ANT  DECIMAL(15, 3),
             DT_CESSAZIONE       DATE,
             TOT_IMPST           DECIMAL(15, 3),
             ONER_TRASFE         DECIMAL(15, 3),
             IMP_NET_TRASFERITO  DECIMAL(15, 3)
          ) END-EXEC.

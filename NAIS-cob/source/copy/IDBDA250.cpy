           EXEC SQL DECLARE PERS TABLE
           (
             ID_PERS             DECIMAL(9, 0) NOT NULL,
             TSTAM_INI_VLDT      DECIMAL(18, 0) NOT NULL,
             TSTAM_END_VLDT      DECIMAL(18, 0),
             COD_PERS            DECIMAL(11, 0) NOT NULL,
             RIFTO_RETE          CHAR(20),
             COD_PRT_IVA         CHAR(11),
             IND_PVCY_PRSNL      CHAR(1),
             IND_PVCY_CMMRC      CHAR(1),
             IND_PVCY_INDST      CHAR(1),
             DT_NASC_CLI         DATE,
             DT_ACQS_PERS        DATE,
             IND_CLI             CHAR(1),
             COD_CMN             CHAR(4),
             COD_FRM_GIURD       CHAR(4),
             COD_ENTE_PUBB       CHAR(4),
             DEN_RGN_SOC         VARCHAR(100),
             DEN_SIG_RGN_SOC     VARCHAR(100),
             IND_ESE_FISC        CHAR(1),
             COD_STAT_CVL        CHAR(4),
             DEN_NOME            VARCHAR(100),
             DEN_COGN            VARCHAR(100),
             COD_FISC            CHAR(16),
             IND_SEX             CHAR(1),
             IND_CPCT_GIURD      CHAR(1),
             IND_PORT_HDCP       CHAR(1),
             COD_USER_INS        CHAR(20) NOT NULL,
             TSTAM_INS_RIGA      DECIMAL(18, 0) NOT NULL,
             COD_USER_AGGM       CHAR(20),
             TSTAM_AGGM_RIGA     DECIMAL(18, 0),
             DEN_CMN_NASC_STRN   VARCHAR(100),
             COD_RAMO_STGR       CHAR(4),
             COD_STGR_ATVT_UIC   CHAR(4),
             COD_RAMO_ATVT_UIC   CHAR(4),
             DT_END_VLDT_PERS    DATE,
             DT_DEAD_PERS        DATE,
             TP_STAT_CLI         CHAR(2),
             DT_BLOC_CLI         DATE,
             COD_PERS_SECOND     DECIMAL(11, 0),
             ID_SEGMENTAZ_CLI    DECIMAL(9, 0),
             DT_1A_ATVT          DATE,
             DT_SEGNAL_PARTNER   DATE
          ) END-EXEC.

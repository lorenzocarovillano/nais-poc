
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVL275
      *   ULTIMO AGG. 21 NOV 2008
      *------------------------------------------------------------

       VAL-DCLGEN-L27.
           MOVE (SF)-COD-COMP-ANIA
              TO L27-COD-COMP-ANIA
           MOVE (SF)-STAT-PREV
              TO L27-STAT-PREV
           IF (SF)-TP-OGG-NULL = HIGH-VALUES
              MOVE (SF)-TP-OGG-NULL
              TO L27-TP-OGG-NULL
           ELSE
              MOVE (SF)-TP-OGG
              TO L27-TP-OGG
           END-IF
           IF (SF)-ID-OGG-NULL = HIGH-VALUES
              MOVE (SF)-ID-OGG-NULL
              TO L27-ID-OGG-NULL
           ELSE
              MOVE (SF)-ID-OGG
              TO L27-ID-OGG
           END-IF
           MOVE (SF)-IB-OGG
              TO L27-IB-OGG
           MOVE (SF)-DS-OPER-SQL
              TO L27-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO L27-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO L27-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO L27-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO L27-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO L27-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO L27-DS-STATO-ELAB.
       VAL-DCLGEN-L27-EX.
           EXIT.

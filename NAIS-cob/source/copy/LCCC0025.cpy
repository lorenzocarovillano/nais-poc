      *----------------------------------------------------------------*
      *   AREA INPUT SERVIZIO VERIFICA ESISTENZA POLIZZA
      *   PORTAFOGLIO VITA - PROCESSO VENDITA
      *   LUNG.
      *----------------------------------------------------------------*
           05 LCCS0025-DATI-INPUT.
              07 LCCS0025-FL-TIPO-IB                  PIC X(2).
                 88 LCCS0025-FL-IB-PROP               VALUE 'PR'.
                 88 LCCS0025-FL-IB-POLI               VALUE 'PO'.
              07 LCCS0025-IB                          PIC X(40).

           05 LCCS0025-DATI-OUTPUT.
              07 LCCS0025-FL-PRES-IN-PTF              PIC X(1).
                 88 LCCS0025-FL-PRES-PTF-SI           VALUE 'S'.
                 88 LCCS0025-FL-PRES-PTF-NO           VALUE 'N'.
              07 LCCS0025-ID-POLI                     PIC S9(9) COMP-3.

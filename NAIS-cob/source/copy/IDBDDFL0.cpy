           EXEC SQL DECLARE D_FORZ_LIQ TABLE
           (
             ID_D_FORZ_LIQ       DECIMAL(9, 0) NOT NULL,
             ID_LIQ              DECIMAL(9, 0) NOT NULL,
             ID_MOVI_CRZ         DECIMAL(9, 0) NOT NULL,
             ID_MOVI_CHIU        DECIMAL(9, 0),
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             DT_INI_EFF          DATE NOT NULL,
             DT_END_EFF          DATE NOT NULL,
             IMP_LRD_CALC        DECIMAL(15, 3),
             IMP_LRD_DFZ         DECIMAL(15, 3),
             IMP_LRD_EFFLQ       DECIMAL(15, 3),
             IMP_NET_CALC        DECIMAL(15, 3),
             IMP_NET_DFZ         DECIMAL(15, 3),
             IMP_NET_EFFLQ       DECIMAL(15, 3),
             IMPST_PRVR_CALC     DECIMAL(15, 3),
             IMPST_PRVR_DFZ      DECIMAL(15, 3),
             IMPST_PRVR_EFFLQ    DECIMAL(15, 3),
             IMPST_VIS_CALC      DECIMAL(15, 3),
             IMPST_VIS_DFZ       DECIMAL(15, 3),
             IMPST_VIS_EFFLQ     DECIMAL(15, 3),
             RIT_ACC_CALC        DECIMAL(15, 3),
             RIT_ACC_DFZ         DECIMAL(15, 3),
             RIT_ACC_EFFLQ       DECIMAL(15, 3),
             RIT_IRPEF_CALC      DECIMAL(15, 3),
             RIT_IRPEF_DFZ       DECIMAL(15, 3),
             RIT_IRPEF_EFFLQ     DECIMAL(15, 3),
             IMPST_SOST_CALC     DECIMAL(15, 3),
             IMPST_SOST_DFZ      DECIMAL(15, 3),
             IMPST_SOST_EFFLQ    DECIMAL(15, 3),
             TAX_SEP_CALC        DECIMAL(15, 3),
             TAX_SEP_DFZ         DECIMAL(15, 3),
             TAX_SEP_EFFLQ       DECIMAL(15, 3),
             INTR_PREST_CALC     DECIMAL(15, 3),
             INTR_PREST_DFZ      DECIMAL(15, 3),
             INTR_PREST_EFFLQ    DECIMAL(15, 3),
             ACCPRE_SOST_CALC    DECIMAL(15, 3),
             ACCPRE_SOST_DFZ     DECIMAL(15, 3),
             ACCPRE_SOST_EFFLQ   DECIMAL(15, 3),
             ACCPRE_VIS_CALC     DECIMAL(15, 3),
             ACCPRE_VIS_DFZ      DECIMAL(15, 3),
             ACCPRE_VIS_EFFLQ    DECIMAL(15, 3),
             ACCPRE_ACC_CALC     DECIMAL(15, 3),
             ACCPRE_ACC_DFZ      DECIMAL(15, 3),
             ACCPRE_ACC_EFFLQ    DECIMAL(15, 3),
             RES_PRSTZ_CALC      DECIMAL(15, 3),
             RES_PRSTZ_DFZ       DECIMAL(15, 3),
             RES_PRSTZ_EFFLQ     DECIMAL(15, 3),
             RES_PRE_ATT_CALC    DECIMAL(15, 3),
             RES_PRE_ATT_DFZ     DECIMAL(15, 3),
             RES_PRE_ATT_EFFLQ   DECIMAL(15, 3),
             IMP_EXCONTR_EFF     DECIMAL(15, 3),
             IMPB_VIS_CALC       DECIMAL(15, 3),
             IMPB_VIS_EFFLQ      DECIMAL(15, 3),
             IMPB_VIS_DFZ        DECIMAL(15, 3),
             IMPB_RIT_ACC_CALC   DECIMAL(15, 3),
             IMPB_RIT_ACC_EFFLQ  DECIMAL(15, 3),
             IMPB_RIT_ACC_DFZ    DECIMAL(15, 3),
             IMPB_TFR_CALC       DECIMAL(15, 3),
             IMPB_TFR_EFFLQ      DECIMAL(15, 3),
             IMPB_TFR_DFZ        DECIMAL(15, 3),
             IMPB_IS_CALC        DECIMAL(15, 3),
             IMPB_IS_EFFLQ       DECIMAL(15, 3),
             IMPB_IS_DFZ         DECIMAL(15, 3),
             IMPB_TAX_SEP_CALC   DECIMAL(15, 3),
             IMPB_TAX_SEP_EFFLQ  DECIMAL(15, 3),
             IMPB_TAX_SEP_DFZ    DECIMAL(15, 3),
             IINT_PREST_CALC     DECIMAL(15, 3),
             IINT_PREST_EFFLQ    DECIMAL(15, 3),
             IINT_PREST_DFZ      DECIMAL(15, 3),
             MONT_END2000_CALC   DECIMAL(15, 3),
             MONT_END2000_EFFLQ  DECIMAL(15, 3),
             MONT_END2000_DFZ    DECIMAL(15, 3),
             MONT_END2006_CALC   DECIMAL(15, 3),
             MONT_END2006_EFFLQ  DECIMAL(15, 3),
             MONT_END2006_DFZ    DECIMAL(15, 3),
             MONT_DAL2007_CALC   DECIMAL(15, 3),
             MONT_DAL2007_EFFLQ  DECIMAL(15, 3),
             MONT_DAL2007_DFZ    DECIMAL(15, 3),
             IIMPST_PRVR_CALC    DECIMAL(15, 3),
             IIMPST_PRVR_EFFLQ   DECIMAL(15, 3),
             IIMPST_PRVR_DFZ     DECIMAL(15, 3),
             IIMPST_252_CALC     DECIMAL(15, 3),
             IIMPST_252_EFFLQ    DECIMAL(15, 3),
             IIMPST_252_DFZ      DECIMAL(15, 3),
             IMPST_252_CALC      DECIMAL(15, 3),
             IMPST_252_EFFLQ     DECIMAL(15, 3),
             RIT_TFR_CALC        DECIMAL(15, 3),
             RIT_TFR_EFFLQ       DECIMAL(15, 3),
             RIT_TFR_DFZ         DECIMAL(15, 3),
             CNBT_INPSTFM_CALC   DECIMAL(15, 3),
             CNBT_INPSTFM_EFFLQ  DECIMAL(15, 3),
             CNBT_INPSTFM_DFZ    DECIMAL(15, 3),
             ICNB_INPSTFM_CALC   DECIMAL(15, 3),
             ICNB_INPSTFM_EFFLQ  DECIMAL(15, 3),
             ICNB_INPSTFM_DFZ    DECIMAL(15, 3),
             CNDE_END2000_CALC   DECIMAL(15, 3),
             CNDE_END2000_EFFLQ  DECIMAL(15, 3),
             CNDE_END2000_DFZ    DECIMAL(15, 3),
             CNDE_END2006_CALC   DECIMAL(15, 3),
             CNDE_END2006_EFFLQ  DECIMAL(15, 3),
             CNDE_END2006_DFZ    DECIMAL(15, 3),
             CNDE_DAL2007_CALC   DECIMAL(15, 3),
             CNDE_DAL2007_EFFLQ  DECIMAL(15, 3),
             CNDE_DAL2007_DFZ    DECIMAL(15, 3),
             AA_CNBZ_END2000_EF  DECIMAL(5, 0),
             AA_CNBZ_END2006_EF  DECIMAL(5, 0),
             AA_CNBZ_DAL2007_EF  DECIMAL(5, 0),
             MM_CNBZ_END2000_EF  DECIMAL(5, 0),
             MM_CNBZ_END2006_EF  DECIMAL(5, 0),
             MM_CNBZ_DAL2007_EF  DECIMAL(5, 0),
             IMPST_DA_RIMB_EFF   DECIMAL(15, 3),
             ALQ_TAX_SEP_CALC    DECIMAL(6, 3),
             ALQ_TAX_SEP_EFFLQ   DECIMAL(6, 3),
             ALQ_TAX_SEP_DFZ     DECIMAL(6, 3),
             ALQ_CNBT_INPSTFM_C  DECIMAL(6, 3),
             ALQ_CNBT_INPSTFM_E  DECIMAL(6, 3),
             ALQ_CNBT_INPSTFM_D  DECIMAL(6, 3),
             DS_RIGA             DECIMAL(10, 0) NOT NULL WITH DEFAULT,
             DS_OPER_SQL         CHAR(1) NOT NULL WITH DEFAULT,
             DS_VER              DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             DS_TS_INI_CPTZ      DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_TS_END_CPTZ      DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_UTENTE           CHAR(20) NOT NULL WITH DEFAULT,
             DS_STATO_ELAB       CHAR(1) NOT NULL WITH DEFAULT,
             IMPB_VIS_1382011C   DECIMAL(15, 3),
             IMPB_VIS_1382011D   DECIMAL(15, 3),
             IMPB_VIS_1382011L   DECIMAL(15, 3),
             IMPST_VIS_1382011C  DECIMAL(15, 3),
             IMPST_VIS_1382011D  DECIMAL(15, 3),
             IMPST_VIS_1382011L  DECIMAL(15, 3),
             IMPB_IS_1382011C    DECIMAL(15, 3),
             IMPB_IS_1382011D    DECIMAL(15, 3),
             IMPB_IS_1382011L    DECIMAL(15, 3),
             IS_1382011C         DECIMAL(15, 3),
             IS_1382011D         DECIMAL(15, 3),
             IS_1382011L         DECIMAL(15, 3),
             IMP_INTR_RIT_PAG_C  DECIMAL(15, 3),
             IMP_INTR_RIT_PAG_D  DECIMAL(15, 3),
             IMP_INTR_RIT_PAG_L  DECIMAL(15, 3),
             IMPB_BOLLO_DETT_C   DECIMAL(15, 3),
             IMPB_BOLLO_DETT_D   DECIMAL(15, 3),
             IMPB_BOLLO_DETT_L   DECIMAL(15, 3),
             IMPST_BOLLO_DETT_C  DECIMAL(15, 3),
             IMPST_BOLLO_DETT_D  DECIMAL(15, 3),
             IMPST_BOLLO_DETT_L  DECIMAL(15, 3),
             IMPST_BOLLO_TOT_VC  DECIMAL(15, 3),
             IMPST_BOLLO_TOT_VD  DECIMAL(15, 3),
             IMPST_BOLLO_TOT_VL  DECIMAL(15, 3),
             IMPB_VIS_662014C    DECIMAL(15, 3),
             IMPB_VIS_662014D    DECIMAL(15, 3),
             IMPB_VIS_662014L    DECIMAL(15, 3),
             IMPST_VIS_662014C   DECIMAL(15, 3),
             IMPST_VIS_662014D   DECIMAL(15, 3),
             IMPST_VIS_662014L   DECIMAL(15, 3),
             IMPB_IS_662014C     DECIMAL(15, 3),
             IMPB_IS_662014D     DECIMAL(15, 3),
             IMPB_IS_662014L     DECIMAL(15, 3),
             IS_662014C          DECIMAL(15, 3),
             IS_662014D          DECIMAL(15, 3),
             IS_662014L          DECIMAL(15, 3)
          ) END-EXEC.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVVAS3
      *   ULTIMO AGG. 09 LUG 2010
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-VAS.
           MOVE VAS-ID-VAL-AST
             TO (SF)-ID-PTF(IX-TAB-VAS)
           MOVE VAS-ID-VAL-AST
             TO (SF)-ID-VAL-AST(IX-TAB-VAS)
           IF VAS-ID-TRCH-DI-GAR-NULL = HIGH-VALUES
              MOVE VAS-ID-TRCH-DI-GAR-NULL
                TO (SF)-ID-TRCH-DI-GAR-NULL(IX-TAB-VAS)
           ELSE
              MOVE VAS-ID-TRCH-DI-GAR
                TO (SF)-ID-TRCH-DI-GAR(IX-TAB-VAS)
           END-IF
           MOVE VAS-ID-MOVI-FINRIO
             TO (SF)-ID-MOVI-FINRIO(IX-TAB-VAS)
           MOVE VAS-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-VAS)
           IF VAS-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE VAS-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-VAS)
           ELSE
              MOVE VAS-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-VAS)
           END-IF
           MOVE VAS-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-VAS)
           MOVE VAS-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-VAS)
           MOVE VAS-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-VAS)
           IF VAS-COD-FND-NULL = HIGH-VALUES
              MOVE VAS-COD-FND-NULL
                TO (SF)-COD-FND-NULL(IX-TAB-VAS)
           ELSE
              MOVE VAS-COD-FND
                TO (SF)-COD-FND(IX-TAB-VAS)
           END-IF
           IF VAS-NUM-QUO-NULL = HIGH-VALUES
              MOVE VAS-NUM-QUO-NULL
                TO (SF)-NUM-QUO-NULL(IX-TAB-VAS)
           ELSE
              MOVE VAS-NUM-QUO
                TO (SF)-NUM-QUO(IX-TAB-VAS)
           END-IF
           IF VAS-VAL-QUO-NULL = HIGH-VALUES
              MOVE VAS-VAL-QUO-NULL
                TO (SF)-VAL-QUO-NULL(IX-TAB-VAS)
           ELSE
              MOVE VAS-VAL-QUO
                TO (SF)-VAL-QUO(IX-TAB-VAS)
           END-IF
           IF VAS-DT-VALZZ-NULL = HIGH-VALUES
              MOVE VAS-DT-VALZZ-NULL
                TO (SF)-DT-VALZZ-NULL(IX-TAB-VAS)
           ELSE
              MOVE VAS-DT-VALZZ
                TO (SF)-DT-VALZZ(IX-TAB-VAS)
           END-IF
           MOVE VAS-TP-VAL-AST
             TO (SF)-TP-VAL-AST(IX-TAB-VAS)
           IF VAS-ID-RICH-INVST-FND-NULL = HIGH-VALUES
              MOVE VAS-ID-RICH-INVST-FND-NULL
                TO (SF)-ID-RICH-INVST-FND-NULL(IX-TAB-VAS)
           ELSE
              MOVE VAS-ID-RICH-INVST-FND
                TO (SF)-ID-RICH-INVST-FND(IX-TAB-VAS)
           END-IF
           IF VAS-ID-RICH-DIS-FND-NULL = HIGH-VALUES
              MOVE VAS-ID-RICH-DIS-FND-NULL
                TO (SF)-ID-RICH-DIS-FND-NULL(IX-TAB-VAS)
           ELSE
              MOVE VAS-ID-RICH-DIS-FND
                TO (SF)-ID-RICH-DIS-FND(IX-TAB-VAS)
           END-IF
           MOVE VAS-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-VAS)
           MOVE VAS-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-VAS)
           MOVE VAS-DS-VER
             TO (SF)-DS-VER(IX-TAB-VAS)
           MOVE VAS-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-VAS)
           MOVE VAS-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-VAS)
           MOVE VAS-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-VAS)
           MOVE VAS-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-VAS)
           IF VAS-PRE-MOVTO-NULL = HIGH-VALUES
              MOVE VAS-PRE-MOVTO-NULL
                TO (SF)-PRE-MOVTO-NULL(IX-TAB-VAS)
           ELSE
              MOVE VAS-PRE-MOVTO
                TO (SF)-PRE-MOVTO(IX-TAB-VAS)
           END-IF
           IF VAS-IMP-MOVTO-NULL = HIGH-VALUES
              MOVE VAS-IMP-MOVTO-NULL
                TO (SF)-IMP-MOVTO-NULL(IX-TAB-VAS)
           ELSE
              MOVE VAS-IMP-MOVTO
                TO (SF)-IMP-MOVTO(IX-TAB-VAS)
           END-IF
           IF VAS-PC-INV-DIS-NULL = HIGH-VALUES
              MOVE VAS-PC-INV-DIS-NULL
                TO (SF)-PC-INV-DIS-NULL(IX-TAB-VAS)
           ELSE
              MOVE VAS-PC-INV-DIS
                TO (SF)-PC-INV-DIS(IX-TAB-VAS)
           END-IF
           IF VAS-NUM-QUO-LORDE-NULL = HIGH-VALUES
              MOVE VAS-NUM-QUO-LORDE-NULL
                TO (SF)-NUM-QUO-LORDE-NULL(IX-TAB-VAS)
           ELSE
              MOVE VAS-NUM-QUO-LORDE
                TO (SF)-NUM-QUO-LORDE(IX-TAB-VAS)
           END-IF
           IF VAS-DT-VALZZ-CALC-NULL = HIGH-VALUES
              MOVE VAS-DT-VALZZ-CALC-NULL
                TO (SF)-DT-VALZZ-CALC-NULL(IX-TAB-VAS)
           ELSE
              MOVE VAS-DT-VALZZ-CALC
                TO (SF)-DT-VALZZ-CALC(IX-TAB-VAS)
           END-IF
           IF VAS-MINUS-VALENZA-NULL = HIGH-VALUES
              MOVE VAS-MINUS-VALENZA-NULL
                TO (SF)-MINUS-VALENZA-NULL(IX-TAB-VAS)
           ELSE
              MOVE VAS-MINUS-VALENZA
                TO (SF)-MINUS-VALENZA(IX-TAB-VAS)
           END-IF
           IF VAS-PLUS-VALENZA-NULL = HIGH-VALUES
              MOVE VAS-PLUS-VALENZA-NULL
                TO (SF)-PLUS-VALENZA-NULL(IX-TAB-VAS)
           ELSE
              MOVE VAS-PLUS-VALENZA
                TO (SF)-PLUS-VALENZA(IX-TAB-VAS)
           END-IF.
       VALORIZZA-OUTPUT-VAS-EX.
           EXIT.

       01 LDBV1591.
          05 LDBV1591-ID-ADES             PIC S9(009) COMP-3.
          05 LDBV1591-ID-OGG              PIC S9(009) COMP-3.
          05 LDBV1591-TP-OGG              PIC  X(002).
          05 LDBV1591-DT-DECOR            PIC S9(008) COMP-3.
          05 LDBV1591-TP-STAT-TIT         PIC  X(002).
          05 LDBV1591-IMP-TOT             PIC S9(12)V9(3) COMP-3.

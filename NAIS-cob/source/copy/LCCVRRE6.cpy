      *----------------------------------------------------------------*
      *    COPY      ..... LCCVRRE6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO RAPPORTO RETE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-RAPP-RETE.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE RAPP-RETE

      *--> NOME TABELLA FISICA DB
           MOVE 'RAPP-RETE'                  TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WRRE-ST-INV(IX-TAB-RRE)
              AND WRRE-ELE-RAPP-RETE-MAX NOT = 0

      *--->   Impostare ID Tabella RAPPORTO RETE
              MOVE WMOV-ID-PTF            TO RRE-ID-MOVI-CRZ

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WRRE-ST-ADD(IX-TAB-RRE)

                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA
                            TO WRRE-ID-PTF(IX-TAB-RRE)
                          MOVE WRRE-ID-PTF(IX-TAB-RRE)
                                                    TO RRE-ID-RAPP-RETE
                          MOVE WPOL-ID-PTF          TO RRE-ID-OGG
                          MOVE WMOV-ID-PTF          TO RRE-ID-MOVI-CRZ
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WRRE-ST-DEL(IX-TAB-RRE)

                       MOVE WRRE-ID-PTF(IX-TAB-RRE)
                         TO RRE-ID-RAPP-RETE
                       IF WRRE-ID-OGG-NULL(IX-TAB-RRE) = HIGH-VALUES
                          MOVE WRRE-ID-OGG-NULL(IX-TAB-RRE)
                            TO RRE-ID-OGG-NULL
                       ELSE
                          MOVE WRRE-ID-OGG(IX-TAB-RRE)
                            TO RRE-ID-OGG
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->        MODIFICA
                  WHEN WRRE-ST-MOD(IX-TAB-RRE)

                       MOVE WRRE-ID-PTF(IX-TAB-RRE)
                         TO RRE-ID-RAPP-RETE
                       IF WRRE-ID-OGG-NULL(IX-TAB-RRE) = HIGH-VALUES
                          MOVE WRRE-ID-OGG-NULL(IX-TAB-RRE)
                            TO RRE-ID-OGG-NULL
                       ELSE
                          MOVE WRRE-ID-OGG(IX-TAB-RRE)
                            TO RRE-ID-OGG
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN RAPPORTO RETE
                 PERFORM VAL-DCLGEN-RRE
                    THRU VAL-DCLGEN-RRE-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-RRE
                    THRU VALORIZZA-AREA-DSH-RRE-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-RAPP-RETE-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-RRE.

      *--> DCLGEN TABELLA
           MOVE RAPP-RETE               TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-RRE-EX.
           EXIT.


      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVRRE5.

       01 LDBV5571.
          03 LDBV5571-INPUT.
             05 LDBV5571-ID-OGG-DER           PIC S9(9)       COMP-3.
             05 LDBV5571-TP-OGG-DER           PIC X(02).
             05 LDBV5571-TP-COLL-1            PIC X(02).
             05 LDBV5571-TP-COLL-2            PIC X(02).
             05 LDBV5571-TP-COLL-3            PIC X(02).
          03 LDBV5571-OUTPUT.
             05 LDBV5571-TOT-IMP-COLL         PIC S9(12)V9(3) COMP-3.

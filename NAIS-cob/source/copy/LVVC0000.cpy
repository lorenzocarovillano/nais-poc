      *----------------------------------------------------------------*
      * AREA CHIAMATA MODULI CALCOLO DATA
      *----------------------------------------------------------------*
      *
       03 (SF)-FORMAT-DATE              PIC X(01).
          88 (SF)-AAAA-V-9999999        VALUE '1'.
          88 (SF)-AAA-V-GGG             VALUE '2'.
          88 (SF)-AAA-V-MM              VALUE '3'.

       03 (SF)-DATI-INPUT.
              05 (SF)-DATA-INPUT        PIC  9(08).

       03 (SF)-DATI-INPUT-1.
              05 (SF)-DATA-INPUT-1      PIC  9(08).

       03 (SF)-DATI-INPUT-2.
              05 (SF)-ANNI-INPUT-2      PIC  9(05).
              05 (SF)-MESI-INPUT-2      PIC  9(05).
              05 (SF)-GIORNI-INPUT-2    PIC  9(05).

       03 (SF)-DATI-INPUT-3.
              05 (SF)-ANNI-INPUT-3      PIC  9(05).
              05 (SF)-MESI-INPUT-3      PIC  9(05).

      *
       03 (SF)-DATI-OUTPUT.
              05 (SF)-DATA-OUTPUT       PIC  9(04)V9(07).
      *

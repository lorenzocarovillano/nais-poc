      *----------------------------------------------------------------*
      *    COPY      ..... LCCVP616
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO DATI CRISTALLIZZATI
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BP58GNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-P61.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE D-CRIST

      *--> NOME TABELLA FISICA DB
           MOVE 'D-CRIST'
             TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WP61-ST-INV
              AND WP61-ELE-MAX-D-CRIST NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WP61-ST-ADD

                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK

                          MOVE S090-SEQ-TABELLA
                            TO WP61-ID-PTF
                               P61-ID-D-CRIST

                          MOVE WMOV-ID-PTF
                            TO P61-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                         SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

                       END-IF

      *-->        MODIFICA
                  WHEN WP61-ST-MOD

                    MOVE WP61-ID-PTF               TO P61-ID-D-CRIST
                    MOVE WMOV-ID-PTF               TO P61-ID-MOVI-CRZ
                    MOVE WMOV-DT-EFF               TO P61-DT-INI-EFF

      *-->       TIPO OPERAZIONE DISPATCHER
                    SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WP61-ST-DEL
                     MOVE WP61-ID-PTF              TO P61-ID-D-CRIST

      *-->       TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->       TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE


              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN IMPOSTA DI BOLLO
                 PERFORM VAL-DCLGEN-P61
                    THRU VAL-DCLGEN-P61-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-P61
                    THRU VALORIZZA-AREA-DSH-P61-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-P61-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-P61.

      *--> DCLGEN TABELLA
           MOVE D-CRIST                 TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT             TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-P61-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVP615.

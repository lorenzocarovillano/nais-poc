
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVDFA5
      *   ULTIMO AGG. 22 APR 2010
      *------------------------------------------------------------

       VAL-DCLGEN-DFA.
           IF (SF)-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL
              TO DFA-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU
              TO DFA-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF NOT NUMERIC
              MOVE 0 TO DFA-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF
              TO DFA-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF NOT NUMERIC
              MOVE 0 TO DFA-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF
              TO DFA-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA
              TO DFA-COD-COMP-ANIA
           IF (SF)-TP-ISC-FND-NULL = HIGH-VALUES
              MOVE (SF)-TP-ISC-FND-NULL
              TO DFA-TP-ISC-FND-NULL
           ELSE
              MOVE (SF)-TP-ISC-FND
              TO DFA-TP-ISC-FND
           END-IF
           IF (SF)-DT-ACCNS-RAPP-FND-NULL = HIGH-VALUES
              MOVE (SF)-DT-ACCNS-RAPP-FND-NULL
              TO DFA-DT-ACCNS-RAPP-FND-NULL
           ELSE
             IF (SF)-DT-ACCNS-RAPP-FND = ZERO
                MOVE HIGH-VALUES
                TO DFA-DT-ACCNS-RAPP-FND-NULL
             ELSE
              MOVE (SF)-DT-ACCNS-RAPP-FND
              TO DFA-DT-ACCNS-RAPP-FND
             END-IF
           END-IF
           IF (SF)-IMP-CNBT-AZ-K1-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CNBT-AZ-K1-NULL
              TO DFA-IMP-CNBT-AZ-K1-NULL
           ELSE
              MOVE (SF)-IMP-CNBT-AZ-K1
              TO DFA-IMP-CNBT-AZ-K1
           END-IF
           IF (SF)-IMP-CNBT-ISC-K1-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CNBT-ISC-K1-NULL
              TO DFA-IMP-CNBT-ISC-K1-NULL
           ELSE
              MOVE (SF)-IMP-CNBT-ISC-K1
              TO DFA-IMP-CNBT-ISC-K1
           END-IF
           IF (SF)-IMP-CNBT-TFR-K1-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CNBT-TFR-K1-NULL
              TO DFA-IMP-CNBT-TFR-K1-NULL
           ELSE
              MOVE (SF)-IMP-CNBT-TFR-K1
              TO DFA-IMP-CNBT-TFR-K1
           END-IF
           IF (SF)-IMP-CNBT-VOL-K1-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CNBT-VOL-K1-NULL
              TO DFA-IMP-CNBT-VOL-K1-NULL
           ELSE
              MOVE (SF)-IMP-CNBT-VOL-K1
              TO DFA-IMP-CNBT-VOL-K1
           END-IF
           IF (SF)-IMP-CNBT-AZ-K2-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CNBT-AZ-K2-NULL
              TO DFA-IMP-CNBT-AZ-K2-NULL
           ELSE
              MOVE (SF)-IMP-CNBT-AZ-K2
              TO DFA-IMP-CNBT-AZ-K2
           END-IF
           IF (SF)-IMP-CNBT-ISC-K2-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CNBT-ISC-K2-NULL
              TO DFA-IMP-CNBT-ISC-K2-NULL
           ELSE
              MOVE (SF)-IMP-CNBT-ISC-K2
              TO DFA-IMP-CNBT-ISC-K2
           END-IF
           IF (SF)-IMP-CNBT-TFR-K2-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CNBT-TFR-K2-NULL
              TO DFA-IMP-CNBT-TFR-K2-NULL
           ELSE
              MOVE (SF)-IMP-CNBT-TFR-K2
              TO DFA-IMP-CNBT-TFR-K2
           END-IF
           IF (SF)-IMP-CNBT-VOL-K2-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CNBT-VOL-K2-NULL
              TO DFA-IMP-CNBT-VOL-K2-NULL
           ELSE
              MOVE (SF)-IMP-CNBT-VOL-K2
              TO DFA-IMP-CNBT-VOL-K2
           END-IF
           IF (SF)-MATU-K1-NULL = HIGH-VALUES
              MOVE (SF)-MATU-K1-NULL
              TO DFA-MATU-K1-NULL
           ELSE
              MOVE (SF)-MATU-K1
              TO DFA-MATU-K1
           END-IF
           IF (SF)-MATU-RES-K1-NULL = HIGH-VALUES
              MOVE (SF)-MATU-RES-K1-NULL
              TO DFA-MATU-RES-K1-NULL
           ELSE
              MOVE (SF)-MATU-RES-K1
              TO DFA-MATU-RES-K1
           END-IF
           IF (SF)-MATU-K2-NULL = HIGH-VALUES
              MOVE (SF)-MATU-K2-NULL
              TO DFA-MATU-K2-NULL
           ELSE
              MOVE (SF)-MATU-K2
              TO DFA-MATU-K2
           END-IF
           IF (SF)-IMPB-VIS-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-NULL
              TO DFA-IMPB-VIS-NULL
           ELSE
              MOVE (SF)-IMPB-VIS
              TO DFA-IMPB-VIS
           END-IF
           IF (SF)-IMPST-VIS-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-NULL
              TO DFA-IMPST-VIS-NULL
           ELSE
              MOVE (SF)-IMPST-VIS
              TO DFA-IMPST-VIS
           END-IF
           IF (SF)-IMPB-IS-K2-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-K2-NULL
              TO DFA-IMPB-IS-K2-NULL
           ELSE
              MOVE (SF)-IMPB-IS-K2
              TO DFA-IMPB-IS-K2
           END-IF
           IF (SF)-IMPST-SOST-K2-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-SOST-K2-NULL
              TO DFA-IMPST-SOST-K2-NULL
           ELSE
              MOVE (SF)-IMPST-SOST-K2
              TO DFA-IMPST-SOST-K2
           END-IF
           IF (SF)-RIDZ-TFR-NULL = HIGH-VALUES
              MOVE (SF)-RIDZ-TFR-NULL
              TO DFA-RIDZ-TFR-NULL
           ELSE
              MOVE (SF)-RIDZ-TFR
              TO DFA-RIDZ-TFR
           END-IF
           IF (SF)-PC-TFR-NULL = HIGH-VALUES
              MOVE (SF)-PC-TFR-NULL
              TO DFA-PC-TFR-NULL
           ELSE
              MOVE (SF)-PC-TFR
              TO DFA-PC-TFR
           END-IF
           IF (SF)-ALQ-TFR-NULL = HIGH-VALUES
              MOVE (SF)-ALQ-TFR-NULL
              TO DFA-ALQ-TFR-NULL
           ELSE
              MOVE (SF)-ALQ-TFR
              TO DFA-ALQ-TFR
           END-IF
           IF (SF)-TOT-ANTIC-NULL = HIGH-VALUES
              MOVE (SF)-TOT-ANTIC-NULL
              TO DFA-TOT-ANTIC-NULL
           ELSE
              MOVE (SF)-TOT-ANTIC
              TO DFA-TOT-ANTIC
           END-IF
           IF (SF)-IMPB-TFR-ANTIC-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-TFR-ANTIC-NULL
              TO DFA-IMPB-TFR-ANTIC-NULL
           ELSE
              MOVE (SF)-IMPB-TFR-ANTIC
              TO DFA-IMPB-TFR-ANTIC
           END-IF
           IF (SF)-IMPST-TFR-ANTIC-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-TFR-ANTIC-NULL
              TO DFA-IMPST-TFR-ANTIC-NULL
           ELSE
              MOVE (SF)-IMPST-TFR-ANTIC
              TO DFA-IMPST-TFR-ANTIC
           END-IF
           IF (SF)-RIDZ-TFR-SU-ANTIC-NULL = HIGH-VALUES
              MOVE (SF)-RIDZ-TFR-SU-ANTIC-NULL
              TO DFA-RIDZ-TFR-SU-ANTIC-NULL
           ELSE
              MOVE (SF)-RIDZ-TFR-SU-ANTIC
              TO DFA-RIDZ-TFR-SU-ANTIC
           END-IF
           IF (SF)-IMPB-VIS-ANTIC-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-ANTIC-NULL
              TO DFA-IMPB-VIS-ANTIC-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-ANTIC
              TO DFA-IMPB-VIS-ANTIC
           END-IF
           IF (SF)-IMPST-VIS-ANTIC-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-ANTIC-NULL
              TO DFA-IMPST-VIS-ANTIC-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-ANTIC
              TO DFA-IMPST-VIS-ANTIC
           END-IF
           IF (SF)-IMPB-IS-K2-ANTIC-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-K2-ANTIC-NULL
              TO DFA-IMPB-IS-K2-ANTIC-NULL
           ELSE
              MOVE (SF)-IMPB-IS-K2-ANTIC
              TO DFA-IMPB-IS-K2-ANTIC
           END-IF
           IF (SF)-IMPST-SOST-K2-ANTI-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-SOST-K2-ANTI-NULL
              TO DFA-IMPST-SOST-K2-ANTI-NULL
           ELSE
              MOVE (SF)-IMPST-SOST-K2-ANTI
              TO DFA-IMPST-SOST-K2-ANTI
           END-IF
           IF (SF)-ULT-COMMIS-TRASFE-NULL = HIGH-VALUES
              MOVE (SF)-ULT-COMMIS-TRASFE-NULL
              TO DFA-ULT-COMMIS-TRASFE-NULL
           ELSE
              MOVE (SF)-ULT-COMMIS-TRASFE
              TO DFA-ULT-COMMIS-TRASFE
           END-IF
           IF (SF)-COD-DVS-NULL = HIGH-VALUES
              MOVE (SF)-COD-DVS-NULL
              TO DFA-COD-DVS-NULL
           ELSE
              MOVE (SF)-COD-DVS
              TO DFA-COD-DVS
           END-IF
           IF (SF)-ALQ-PRVR-NULL = HIGH-VALUES
              MOVE (SF)-ALQ-PRVR-NULL
              TO DFA-ALQ-PRVR-NULL
           ELSE
              MOVE (SF)-ALQ-PRVR
              TO DFA-ALQ-PRVR
           END-IF
           IF (SF)-PC-ESE-IMPST-TFR-NULL = HIGH-VALUES
              MOVE (SF)-PC-ESE-IMPST-TFR-NULL
              TO DFA-PC-ESE-IMPST-TFR-NULL
           ELSE
              MOVE (SF)-PC-ESE-IMPST-TFR
              TO DFA-PC-ESE-IMPST-TFR
           END-IF
           IF (SF)-IMP-ESE-IMPST-TFR-NULL = HIGH-VALUES
              MOVE (SF)-IMP-ESE-IMPST-TFR-NULL
              TO DFA-IMP-ESE-IMPST-TFR-NULL
           ELSE
              MOVE (SF)-IMP-ESE-IMPST-TFR
              TO DFA-IMP-ESE-IMPST-TFR
           END-IF
           IF (SF)-ANZ-CNBTVA-CARASS-NULL = HIGH-VALUES
              MOVE (SF)-ANZ-CNBTVA-CARASS-NULL
              TO DFA-ANZ-CNBTVA-CARASS-NULL
           ELSE
              MOVE (SF)-ANZ-CNBTVA-CARASS
              TO DFA-ANZ-CNBTVA-CARASS
           END-IF
           IF (SF)-ANZ-CNBTVA-CARAZI-NULL = HIGH-VALUES
              MOVE (SF)-ANZ-CNBTVA-CARAZI-NULL
              TO DFA-ANZ-CNBTVA-CARAZI-NULL
           ELSE
              MOVE (SF)-ANZ-CNBTVA-CARAZI
              TO DFA-ANZ-CNBTVA-CARAZI
           END-IF
           IF (SF)-ANZ-SRVZ-NULL = HIGH-VALUES
              MOVE (SF)-ANZ-SRVZ-NULL
              TO DFA-ANZ-SRVZ-NULL
           ELSE
              MOVE (SF)-ANZ-SRVZ
              TO DFA-ANZ-SRVZ
           END-IF
           IF (SF)-IMP-CNBT-NDED-K1-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CNBT-NDED-K1-NULL
              TO DFA-IMP-CNBT-NDED-K1-NULL
           ELSE
              MOVE (SF)-IMP-CNBT-NDED-K1
              TO DFA-IMP-CNBT-NDED-K1
           END-IF
           IF (SF)-IMP-CNBT-NDED-K2-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CNBT-NDED-K2-NULL
              TO DFA-IMP-CNBT-NDED-K2-NULL
           ELSE
              MOVE (SF)-IMP-CNBT-NDED-K2
              TO DFA-IMP-CNBT-NDED-K2
           END-IF
           IF (SF)-IMP-CNBT-NDED-K3-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CNBT-NDED-K3-NULL
              TO DFA-IMP-CNBT-NDED-K3-NULL
           ELSE
              MOVE (SF)-IMP-CNBT-NDED-K3
              TO DFA-IMP-CNBT-NDED-K3
           END-IF
           IF (SF)-IMP-CNBT-AZ-K3-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CNBT-AZ-K3-NULL
              TO DFA-IMP-CNBT-AZ-K3-NULL
           ELSE
              MOVE (SF)-IMP-CNBT-AZ-K3
              TO DFA-IMP-CNBT-AZ-K3
           END-IF
           IF (SF)-IMP-CNBT-ISC-K3-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CNBT-ISC-K3-NULL
              TO DFA-IMP-CNBT-ISC-K3-NULL
           ELSE
              MOVE (SF)-IMP-CNBT-ISC-K3
              TO DFA-IMP-CNBT-ISC-K3
           END-IF
           IF (SF)-IMP-CNBT-TFR-K3-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CNBT-TFR-K3-NULL
              TO DFA-IMP-CNBT-TFR-K3-NULL
           ELSE
              MOVE (SF)-IMP-CNBT-TFR-K3
              TO DFA-IMP-CNBT-TFR-K3
           END-IF
           IF (SF)-IMP-CNBT-VOL-K3-NULL = HIGH-VALUES
              MOVE (SF)-IMP-CNBT-VOL-K3-NULL
              TO DFA-IMP-CNBT-VOL-K3-NULL
           ELSE
              MOVE (SF)-IMP-CNBT-VOL-K3
              TO DFA-IMP-CNBT-VOL-K3
           END-IF
           IF (SF)-MATU-K3-NULL = HIGH-VALUES
              MOVE (SF)-MATU-K3-NULL
              TO DFA-MATU-K3-NULL
           ELSE
              MOVE (SF)-MATU-K3
              TO DFA-MATU-K3
           END-IF
           IF (SF)-IMPB-252-ANTIC-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-252-ANTIC-NULL
              TO DFA-IMPB-252-ANTIC-NULL
           ELSE
              MOVE (SF)-IMPB-252-ANTIC
              TO DFA-IMPB-252-ANTIC
           END-IF
           IF (SF)-IMPST-252-ANTIC-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-252-ANTIC-NULL
              TO DFA-IMPST-252-ANTIC-NULL
           ELSE
              MOVE (SF)-IMPST-252-ANTIC
              TO DFA-IMPST-252-ANTIC
           END-IF
           IF (SF)-DT-1A-CNBZ-NULL = HIGH-VALUES
              MOVE (SF)-DT-1A-CNBZ-NULL
              TO DFA-DT-1A-CNBZ-NULL
           ELSE
             IF (SF)-DT-1A-CNBZ = ZERO
                MOVE HIGH-VALUES
                TO DFA-DT-1A-CNBZ-NULL
             ELSE
              MOVE (SF)-DT-1A-CNBZ
              TO DFA-DT-1A-CNBZ
             END-IF
           END-IF
           IF (SF)-COMMIS-DI-TRASFE-NULL = HIGH-VALUES
              MOVE (SF)-COMMIS-DI-TRASFE-NULL
              TO DFA-COMMIS-DI-TRASFE-NULL
           ELSE
              MOVE (SF)-COMMIS-DI-TRASFE
              TO DFA-COMMIS-DI-TRASFE
           END-IF
           IF (SF)-AA-CNBZ-K1-NULL = HIGH-VALUES
              MOVE (SF)-AA-CNBZ-K1-NULL
              TO DFA-AA-CNBZ-K1-NULL
           ELSE
              MOVE (SF)-AA-CNBZ-K1
              TO DFA-AA-CNBZ-K1
           END-IF
           IF (SF)-AA-CNBZ-K2-NULL = HIGH-VALUES
              MOVE (SF)-AA-CNBZ-K2-NULL
              TO DFA-AA-CNBZ-K2-NULL
           ELSE
              MOVE (SF)-AA-CNBZ-K2
              TO DFA-AA-CNBZ-K2
           END-IF
           IF (SF)-AA-CNBZ-K3-NULL = HIGH-VALUES
              MOVE (SF)-AA-CNBZ-K3-NULL
              TO DFA-AA-CNBZ-K3-NULL
           ELSE
              MOVE (SF)-AA-CNBZ-K3
              TO DFA-AA-CNBZ-K3
           END-IF
           IF (SF)-MM-CNBZ-K1-NULL = HIGH-VALUES
              MOVE (SF)-MM-CNBZ-K1-NULL
              TO DFA-MM-CNBZ-K1-NULL
           ELSE
              MOVE (SF)-MM-CNBZ-K1
              TO DFA-MM-CNBZ-K1
           END-IF
           IF (SF)-MM-CNBZ-K2-NULL = HIGH-VALUES
              MOVE (SF)-MM-CNBZ-K2-NULL
              TO DFA-MM-CNBZ-K2-NULL
           ELSE
              MOVE (SF)-MM-CNBZ-K2
              TO DFA-MM-CNBZ-K2
           END-IF
           IF (SF)-MM-CNBZ-K3-NULL = HIGH-VALUES
              MOVE (SF)-MM-CNBZ-K3-NULL
              TO DFA-MM-CNBZ-K3-NULL
           ELSE
              MOVE (SF)-MM-CNBZ-K3
              TO DFA-MM-CNBZ-K3
           END-IF
           IF (SF)-FL-APPLZ-NEWFIS-NULL = HIGH-VALUES
              MOVE (SF)-FL-APPLZ-NEWFIS-NULL
              TO DFA-FL-APPLZ-NEWFIS-NULL
           ELSE
              MOVE (SF)-FL-APPLZ-NEWFIS
              TO DFA-FL-APPLZ-NEWFIS
           END-IF
           IF (SF)-REDT-TASS-ABBAT-K3-NULL = HIGH-VALUES
              MOVE (SF)-REDT-TASS-ABBAT-K3-NULL
              TO DFA-REDT-TASS-ABBAT-K3-NULL
           ELSE
              MOVE (SF)-REDT-TASS-ABBAT-K3
              TO DFA-REDT-TASS-ABBAT-K3
           END-IF
           IF (SF)-CNBT-ECC-4X100-K1-NULL = HIGH-VALUES
              MOVE (SF)-CNBT-ECC-4X100-K1-NULL
              TO DFA-CNBT-ECC-4X100-K1-NULL
           ELSE
              MOVE (SF)-CNBT-ECC-4X100-K1
              TO DFA-CNBT-ECC-4X100-K1
           END-IF
           IF (SF)-DS-RIGA NOT NUMERIC
              MOVE 0 TO DFA-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA
              TO DFA-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO DFA-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO DFA-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO DFA-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ NOT NUMERIC
              MOVE 0 TO DFA-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ
              TO DFA-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ NOT NUMERIC
              MOVE 0 TO DFA-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ
              TO DFA-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO DFA-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO DFA-DS-STATO-ELAB
           IF (SF)-CREDITO-IS-NULL = HIGH-VALUES
              MOVE (SF)-CREDITO-IS-NULL
              TO DFA-CREDITO-IS-NULL
           ELSE
              MOVE (SF)-CREDITO-IS
              TO DFA-CREDITO-IS
           END-IF
           IF (SF)-REDT-TASS-ABBAT-K2-NULL = HIGH-VALUES
              MOVE (SF)-REDT-TASS-ABBAT-K2-NULL
              TO DFA-REDT-TASS-ABBAT-K2-NULL
           ELSE
              MOVE (SF)-REDT-TASS-ABBAT-K2
              TO DFA-REDT-TASS-ABBAT-K2
           END-IF
           IF (SF)-IMPB-IS-K3-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-K3-NULL
              TO DFA-IMPB-IS-K3-NULL
           ELSE
              MOVE (SF)-IMPB-IS-K3
              TO DFA-IMPB-IS-K3
           END-IF
           IF (SF)-IMPST-SOST-K3-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-SOST-K3-NULL
              TO DFA-IMPST-SOST-K3-NULL
           ELSE
              MOVE (SF)-IMPST-SOST-K3
              TO DFA-IMPST-SOST-K3
           END-IF
           IF (SF)-IMPB-252-K3-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-252-K3-NULL
              TO DFA-IMPB-252-K3-NULL
           ELSE
              MOVE (SF)-IMPB-252-K3
              TO DFA-IMPB-252-K3
           END-IF
           IF (SF)-IMPST-252-K3-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-252-K3-NULL
              TO DFA-IMPST-252-K3-NULL
           ELSE
              MOVE (SF)-IMPST-252-K3
              TO DFA-IMPST-252-K3
           END-IF
           IF (SF)-IMPB-IS-K3-ANTIC-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IS-K3-ANTIC-NULL
              TO DFA-IMPB-IS-K3-ANTIC-NULL
           ELSE
              MOVE (SF)-IMPB-IS-K3-ANTIC
              TO DFA-IMPB-IS-K3-ANTIC
           END-IF
           IF (SF)-IMPST-SOST-K3-ANTI-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-SOST-K3-ANTI-NULL
              TO DFA-IMPST-SOST-K3-ANTI-NULL
           ELSE
              MOVE (SF)-IMPST-SOST-K3-ANTI
              TO DFA-IMPST-SOST-K3-ANTI
           END-IF
           IF (SF)-IMPB-IRPEF-K1-ANTI-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IRPEF-K1-ANTI-NULL
              TO DFA-IMPB-IRPEF-K1-ANTI-NULL
           ELSE
              MOVE (SF)-IMPB-IRPEF-K1-ANTI
              TO DFA-IMPB-IRPEF-K1-ANTI
           END-IF
           IF (SF)-IMPST-IRPEF-K1-ANT-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-IRPEF-K1-ANT-NULL
              TO DFA-IMPST-IRPEF-K1-ANT-NULL
           ELSE
              MOVE (SF)-IMPST-IRPEF-K1-ANT
              TO DFA-IMPST-IRPEF-K1-ANT
           END-IF
           IF (SF)-IMPB-IRPEF-K2-ANTI-NULL = HIGH-VALUES
              MOVE (SF)-IMPB-IRPEF-K2-ANTI-NULL
              TO DFA-IMPB-IRPEF-K2-ANTI-NULL
           ELSE
              MOVE (SF)-IMPB-IRPEF-K2-ANTI
              TO DFA-IMPB-IRPEF-K2-ANTI
           END-IF
           IF (SF)-IMPST-IRPEF-K2-ANT-NULL = HIGH-VALUES
              MOVE (SF)-IMPST-IRPEF-K2-ANT-NULL
              TO DFA-IMPST-IRPEF-K2-ANT-NULL
           ELSE
              MOVE (SF)-IMPST-IRPEF-K2-ANT
              TO DFA-IMPST-IRPEF-K2-ANT
           END-IF
           IF (SF)-DT-CESSAZIONE-NULL = HIGH-VALUES
              MOVE (SF)-DT-CESSAZIONE-NULL
              TO DFA-DT-CESSAZIONE-NULL
           ELSE
             IF (SF)-DT-CESSAZIONE = ZERO
                MOVE HIGH-VALUES
                TO DFA-DT-CESSAZIONE-NULL
             ELSE
              MOVE (SF)-DT-CESSAZIONE
              TO DFA-DT-CESSAZIONE
             END-IF
           END-IF
           IF (SF)-TOT-IMPST-NULL = HIGH-VALUES
              MOVE (SF)-TOT-IMPST-NULL
              TO DFA-TOT-IMPST-NULL
           ELSE
              MOVE (SF)-TOT-IMPST
              TO DFA-TOT-IMPST
           END-IF
           IF (SF)-ONER-TRASFE-NULL = HIGH-VALUES
              MOVE (SF)-ONER-TRASFE-NULL
              TO DFA-ONER-TRASFE-NULL
           ELSE
              MOVE (SF)-ONER-TRASFE
              TO DFA-ONER-TRASFE
           END-IF
           IF (SF)-IMP-NET-TRASFERITO-NULL = HIGH-VALUES
              MOVE (SF)-IMP-NET-TRASFERITO-NULL
              TO DFA-IMP-NET-TRASFERITO-NULL
           ELSE
              MOVE (SF)-IMP-NET-TRASFERITO
              TO DFA-IMP-NET-TRASFERITO
           END-IF.
       VAL-DCLGEN-DFA-EX.
           EXIT.

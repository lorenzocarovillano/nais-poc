
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVP895
      *   ULTIMO AGG. 17 FEB 2015
      *------------------------------------------------------------

       VAL-DCLGEN-P89.
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-P89)
              TO P89-COD-COMP-ANIA
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P89) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P89)
              TO P89-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-P89)
              TO P89-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-P89) NOT NUMERIC
              MOVE 0 TO P89-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-P89)
              TO P89-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-P89) NOT NUMERIC
              MOVE 0 TO P89-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-P89)
              TO P89-DT-END-EFF
           END-IF

           MOVE (SF)-TP-SERV-VAL(IX-TAB-P89)
              TO P89-TP-SERV-VAL
           MOVE (SF)-COD-FND(IX-TAB-P89)
              TO P89-COD-FND
           IF (SF)-DT-INI-CNTRL-FND-NULL(IX-TAB-P89) = HIGH-VALUES
              MOVE (SF)-DT-INI-CNTRL-FND-NULL(IX-TAB-P89)
              TO P89-DT-INI-CNTRL-FND-NULL
           ELSE
             IF (SF)-DT-INI-CNTRL-FND(IX-TAB-P89) = ZERO
                MOVE HIGH-VALUES
                TO P89-DT-INI-CNTRL-FND-NULL
             ELSE
              MOVE (SF)-DT-INI-CNTRL-FND(IX-TAB-P89)
              TO P89-DT-INI-CNTRL-FND
             END-IF
           END-IF

           MOVE (SF)-TP-OGG(IX-TAB-P89)
              TO P89-TP-OGG
           IF (SF)-DS-RIGA(IX-TAB-P89) NOT NUMERIC
              MOVE 0 TO P89-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-P89)
              TO P89-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-P89)
              TO P89-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-P89) NOT NUMERIC
              MOVE 0 TO P89-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-P89)
              TO P89-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-P89) NOT NUMERIC
              MOVE 0 TO P89-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-P89)
              TO P89-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-P89) NOT NUMERIC
              MOVE 0 TO P89-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-P89)
              TO P89-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-P89)
              TO P89-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-P89)
              TO P89-DS-STATO-ELAB.
       VAL-DCLGEN-P89-EX.
           EXIT.

      *---------------------------------------------------------------*
      * AREA DATI EXTRA IN AMBITO DI BATCH
      *---------------------------------------------------------------*

         02 IABV0006-TIPO-LANCIO-BUS             PIC X(0001).
            88 IABV0006-PRIMO-LANCIO             VALUE 'P'.
            88 IABV0006-CORRENTE-LANCIO          VALUE 'C'.
            88 IABV0006-ULTIMO-LANCIO            VALUE 'U'.

         02 IABV0006-DATA-BATCH                  PIC X(32000).

         02 IABV0006-IBO.
            05 IABV0006-IBO-OLD                  PIC X(100).
            05 IABV0006-IBO-NEW                  PIC X(100).

         02 IABV0006-FLAG-SIMULAZIONE            PIC X(001).
              88 IABV0006-SIMULAZIONE-INFR       VALUE 'I'.
              88 IABV0006-SIMULAZIONE-APPL       VALUE 'A'.
              88 IABV0006-SIMULAZIONE-NO         VALUE 'N'.

         02 IABV0006-FLAG-COMMIT                 PIC X(0001).
            88 IABV0006-COMMIT-YES               VALUE 'Y'.
            88 IABV0006-COMMIT-NO                VALUE 'N'.

         02 IABV0006-FLAG-PROCESS-TYPE           PIC X(0003).
            88 IABV0006-STD-TYPE-REC-NO          VALUE 'TRN'.
            88 IABV0006-STD-TYPE-REC-YES         VALUE 'TRY'.
            88 IABV0006-GUIDE-AD-HOC             VALUE 'HOC'.
            88 IABV0006-BY-BOOKING               VALUE 'BKG'.

         02 IABV0006-COD-LIV-AUT-PROFIL          PIC S9(05) COMP-3.
         02 IABV0006-COD-GRU-AUT-PROFIL          PIC S9(10) COMP-3.
         02 IABV0006-CANALE-VENDITA              PIC S9(05) COMP-3.
         02 IABV0006-PUNTO-VENDITA               PIC X(006).

         02 IABV0006-ID-BATCH                    PIC S9(9)V COMP-3.
         02 IABV0006-ID-RICH                     PIC S9(9)V COMP-3.
         02 IABV0006-ID-RICH-EST                 PIC S9(9)V COMP-3.

         02 IABV0006-REPORT.
            05 IABV0006-OGG-BUSINESS             PIC X(40).
            05 IABV0006-TP-OGG-BUSINESS          PIC X(02).
            05 IABV0006-IB-OGG-POLI              PIC X(40).
            05 IABV0006-IB-OGG-ADES              PIC X(40).
            05 IABV0006-ID-POLI                  PIC X(09).
            05 IABV0006-ID-ADES                  PIC X(09).
            05 IABV0006-DT-EFF-BUSINESS          PIC X(10).

         02 IABV0006-CUSTOM-COUNTERS.
            05 IABV0006-COUNTER-STD              PIC X(01) VALUE 'Y'.
               88 IABV0006-COUNTER-STD-YES       VALUE 'Y'.
               88 IABV0006-COUNTER-STD-NO        VALUE 'N'.
            05 IABV0006-GUIDE-ROWS-READ          PIC X(01) VALUE 'Y'.
               88 IABV0006-GUIDE-ROWS-READ-YES   VALUE 'Y'.
               88 IABV0006-GUIDE-ROWS-READ-NO    VALUE 'N'.
            05 IABV0006-LIM-CUSTOM-COUNT-MAX     PIC S9(4) VALUE 10.
            05 IABV0006-MAX-ELE-CUSTOM-COUNT     PIC S9(4) COMP.
            05 IABV0006-ELE-ERRORI               OCCURS 10.
               15 IABV0006-DESC-TYPE             PIC X(01).
                  88 IABV0006-ONLY-DESC          VALUE 'D'.
                  88 IABV0006-COUNTER-DESC       VALUE 'C'.
               15 IABV0006-CUSTOM-COUNT-DESC     PIC X(50).
               15 IABV0006-CUSTOM-COUNT          PIC 9(009).

         02 IABV0006-GEST-ROTTURA-IBO            PIC X(001).
            88 IABV0006-GEST-ARCH-ROTTURA-IBO    VALUE 'A'.
            88 IABV0006-GEST-APPL-ROTTURA-IBO    VALUE 'B'.

         02 IABV0006-NUM-ELE-VAR-COND            PIC 9(003).
         02 IABV0006-VARIABILI-COND-WF           OCCURS 20.
            05 IABV0006-COD-OPERANDO-WF          PIC X(030).
            05 IABV0006-VAL-OPERANDO-WF          PIC X(100).

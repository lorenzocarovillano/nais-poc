      *----------------------------------------------------------------*
      * COSTANTI PER GESTIONE FILES SEQUENTIALI PER MAIN
      *----------------------------------------------------------------*
       77  FS-FILESQM1             PIC X(002)  VALUE '00'.
       77  FS-FILESQM2             PIC X(002)  VALUE '00'.
       77  FS-FILESQM3             PIC X(002)  VALUE '00'.
       77  FS-FILESQM4             PIC X(002)  VALUE '00'.
       77  FS-FILESQM5             PIC X(002)  VALUE '00'.
       77  FS-FILESQM6             PIC X(002)  VALUE '00'.
       77  FS-FILESQM7             PIC X(002)  VALUE '00'.
       77  FS-FILESQM8             PIC X(002)  VALUE '00'.
       77  FS-FILESQM9             PIC X(002)  VALUE '00'.
       77  FS-FILESQM0             PIC X(002)  VALUE '00'.

       77  NOME-FILESQM1           PIC X(008)  VALUE 'FILESQM1'.
       77  NOME-FILESQM2           PIC X(008)  VALUE 'FILESQM2'.
       77  NOME-FILESQM3           PIC X(008)  VALUE 'FILESQM3'.
       77  NOME-FILESQM4           PIC X(008)  VALUE 'FILESQM4'.
       77  NOME-FILESQM5           PIC X(008)  VALUE 'FILESQM5'.
       77  NOME-FILESQM6           PIC X(008)  VALUE 'FILESQM6'.
       77  NOME-FILESQM7           PIC X(008)  VALUE 'FILESQM7'.
       77  NOME-FILESQM8           PIC X(008)  VALUE 'FILESQM8'.
       77  NOME-FILESQM9           PIC X(008)  VALUE 'FILESQM9'.
       77  NOME-FILESQM0           PIC X(008)  VALUE 'FILESQM0'.

       77  FS-FILESQS1             PIC X(002)  VALUE '00'.
       77  FS-FILESQS2             PIC X(002)  VALUE '00'.
       77  FS-FILESQS3             PIC X(002)  VALUE '00'.
       77  FS-FILESQS4             PIC X(002)  VALUE '00'.
       77  FS-FILESQS5             PIC X(002)  VALUE '00'.
       77  FS-FILESQS6             PIC X(002)  VALUE '00'.
       77  FS-FILESQS7             PIC X(002)  VALUE '00'.
       77  FS-FILESQS8             PIC X(002)  VALUE '00'.
       77  FS-FILESQS9             PIC X(002)  VALUE '00'.
       77  FS-FILESQS0             PIC X(002)  VALUE '00'.

       77  NOME-FILESQS1           PIC X(008)  VALUE 'FILESQS1'.
       77  NOME-FILESQS2           PIC X(008)  VALUE 'FILESQS2'.
       77  NOME-FILESQS3           PIC X(008)  VALUE 'FILESQS3'.
       77  NOME-FILESQS4           PIC X(008)  VALUE 'FILESQS4'.
       77  NOME-FILESQS5           PIC X(008)  VALUE 'FILESQS5'.
       77  NOME-FILESQS6           PIC X(008)  VALUE 'FILESQS6'.
       77  NOME-FILESQS7           PIC X(008)  VALUE 'FILESQS7'.
       77  NOME-FILESQS8           PIC X(008)  VALUE 'FILESQS8'.
       77  NOME-FILESQS9           PIC X(008)  VALUE 'FILESQS9'.
       77  NOME-FILESQS0           PIC X(008)  VALUE 'FILESQS0'.

      ******************************************************************
      * COMODO X FILES SEQUENZIALI
      ******************************************************************



      ******************************************************************
      * STRUTTURA MESSAGGIO X FILES SEQUENZIALI
      ******************************************************************

       01 SEQUENTIAL-ESITO                       PIC X(002).
          88 SEQUENTIAL-ESITO-OK                 VALUE 'OK'.
          88 SEQUENTIAL-ESITO-KO                 VALUE 'KO'.

       01 SEQUENTIAL-DESC-ERRORE-ESTESA          PIC X(200).

       01 MSG-ERR-FILE.
          05 FILLER                PIC X(09) VALUE 'ERRORE : '.
          05 MSG-OPERAZIONI        PIC X(07).
             88 MSG-OPEN           VALUE 'OPEN'.
             88 MSG-READ           VALUE 'READ'.
             88 MSG-WRITE          VALUE 'WRITE'.
             88 MSG-REWRITE        VALUE 'REWRITE'.
             88 MSG-CLOSE          VALUE 'CLOSE'.
          05 FILLER                PIC X(15) VALUE ' - FILE NAME : '.
          05 MSG-NOME-FILE         PIC X(08) VALUE SPACES.
          05 FILLER                PIC X(17) VALUE ' - FILE STATUS : '.
          05 MSG-RC                PIC X(02) VALUE SPACES.


       01 FILE-STATUS                     PIC X(002).
          88 FILE-STATUS-OK                  VALUE '00'.
          88 FILE-STATUS-END-OF-FILE         VALUE '10'.
          88 FILE-STATUS-DUPLICATE-KEY       VALUE '22'.
          88 FILE-STATUS-FILE-NOT-PRESENT    VALUE '35'.
          88 FILE-STATUS-DEF-FD-NOT-VALID    VALUE '39'.
          88 FILE-STATUS-FILE-NOT-OPEN       VALUE '49'.

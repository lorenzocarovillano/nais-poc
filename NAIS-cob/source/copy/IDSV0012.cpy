          02 IDSV0012-AREA-I-O                       PIC X(32000).
          02 IDSV0012-STATUS-SHARED-MEMORY           PIC X(02).
             88 IDSV0012-STATUS-SHARED-MEM-OK        VALUE 'OK'.
             88 IDSV0012-STATUS-SHARED-MEM-KO        VALUE 'KO'.

       02 IDSV0012-RECYCLE-STR.
          05 IDSV0012-RECYCLE-LIMITE-MAX     PIC S9(04) VALUE 100.
          05 IDSV0012-RECYCLE-ELE-MAX        PIC S9(04) COMP
                                                       VALUE 0.
          05 IDSV0012-RECYCLE-TOT                OCCURS 100
                      INDEXED BY IDSV0012-SEARCH-RECYCLE-INDEX.
          10 IDSV0012-RECYCLE-COD-STR-DATO        PIC X(30).
          10 IDSV0012-AREA-ELEMENTS.
             12 IDSV0012-CODICE-SERVIZIO-ELEM OCCURS 01.
                15 IDSV0012-CODICE-SERVIZIO       PIC X(08).
                15 IDSV0012-OBBLIGATORIETA        PIC X(01).
                15 IDSV0012-FLAG-SHARED-MEMORY    PIC X(01).
                15 IDSV0012-TIPO-SERVIZIO         PIC X(03).
                15 IDSV0012-STRUTT-SERV-ELEMENTS.
                   20 IDSV0012-STRUTT-SERVIZIO-ELEM OCCURS 2 TIMES.
                      25 IDSV0012-FLAG-INPUT-OUTPUT   PIC X(001).
                      25 IDSV0012-FLAG-CALL-USING     PIC X(001).
                      25 IDSV0012-CODICE-STR-DATO-R   PIC X(030).




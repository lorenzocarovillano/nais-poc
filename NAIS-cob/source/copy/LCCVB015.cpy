
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVB015
      *   ULTIMO AGG. 09 LUG 2010
      *------------------------------------------------------------

       VAL-DCLGEN-B01.
           MOVE (SF)-ID-BILA-FND-ESTR
              TO B01-ID-BILA-FND-ESTR
           MOVE (SF)-ID-BILA-TRCH-ESTR
              TO B01-ID-BILA-TRCH-ESTR
           MOVE (SF)-COD-COMP-ANIA
              TO B01-COD-COMP-ANIA
           MOVE (SF)-ID-RICH-ESTRAZ-MAS
              TO B01-ID-RICH-ESTRAZ-MAS
           IF (SF)-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
              MOVE (SF)-ID-RICH-ESTRAZ-AGG-NULL
              TO B01-ID-RICH-ESTRAZ-AGG-NULL
           ELSE
              MOVE (SF)-ID-RICH-ESTRAZ-AGG
              TO B01-ID-RICH-ESTRAZ-AGG
           END-IF
           MOVE (SF)-DT-RIS
              TO B01-DT-RIS
           MOVE (SF)-ID-POLI
              TO B01-ID-POLI
           MOVE (SF)-ID-ADES
              TO B01-ID-ADES
           MOVE (SF)-ID-GAR
              TO B01-ID-GAR
           MOVE (SF)-ID-TRCH-DI-GAR
              TO B01-ID-TRCH-DI-GAR
           MOVE (SF)-COD-FND
              TO B01-COD-FND
           IF (SF)-DT-QTZ-INI-NULL = HIGH-VALUES
              MOVE (SF)-DT-QTZ-INI-NULL
              TO B01-DT-QTZ-INI-NULL
           ELSE
             IF (SF)-DT-QTZ-INI = ZERO
                MOVE HIGH-VALUES
                TO B01-DT-QTZ-INI-NULL
             ELSE
              MOVE (SF)-DT-QTZ-INI
              TO B01-DT-QTZ-INI
             END-IF
           END-IF
           IF (SF)-NUM-QUO-INI-NULL = HIGH-VALUES
              MOVE (SF)-NUM-QUO-INI-NULL
              TO B01-NUM-QUO-INI-NULL
           ELSE
              MOVE (SF)-NUM-QUO-INI
              TO B01-NUM-QUO-INI
           END-IF
           IF (SF)-NUM-QUO-DT-CALC-NULL = HIGH-VALUES
              MOVE (SF)-NUM-QUO-DT-CALC-NULL
              TO B01-NUM-QUO-DT-CALC-NULL
           ELSE
              MOVE (SF)-NUM-QUO-DT-CALC
              TO B01-NUM-QUO-DT-CALC
           END-IF
           IF (SF)-VAL-QUO-INI-NULL = HIGH-VALUES
              MOVE (SF)-VAL-QUO-INI-NULL
              TO B01-VAL-QUO-INI-NULL
           ELSE
              MOVE (SF)-VAL-QUO-INI
              TO B01-VAL-QUO-INI
           END-IF
           IF (SF)-DT-VALZZ-QUO-DT-CA-NULL = HIGH-VALUES
              MOVE (SF)-DT-VALZZ-QUO-DT-CA-NULL
              TO B01-DT-VALZZ-QUO-DT-CA-NULL
           ELSE
             IF (SF)-DT-VALZZ-QUO-DT-CA = ZERO
                MOVE HIGH-VALUES
                TO B01-DT-VALZZ-QUO-DT-CA-NULL
             ELSE
              MOVE (SF)-DT-VALZZ-QUO-DT-CA
              TO B01-DT-VALZZ-QUO-DT-CA
             END-IF
           END-IF
           IF (SF)-VAL-QUO-DT-CALC-NULL = HIGH-VALUES
              MOVE (SF)-VAL-QUO-DT-CALC-NULL
              TO B01-VAL-QUO-DT-CALC-NULL
           ELSE
              MOVE (SF)-VAL-QUO-DT-CALC
              TO B01-VAL-QUO-DT-CALC
           END-IF
           IF (SF)-VAL-QUO-T-NULL = HIGH-VALUES
              MOVE (SF)-VAL-QUO-T-NULL
              TO B01-VAL-QUO-T-NULL
           ELSE
              MOVE (SF)-VAL-QUO-T
              TO B01-VAL-QUO-T
           END-IF
           IF (SF)-PC-INVST-NULL = HIGH-VALUES
              MOVE (SF)-PC-INVST-NULL
              TO B01-PC-INVST-NULL
           ELSE
              MOVE (SF)-PC-INVST
              TO B01-PC-INVST
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO B01-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO B01-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO B01-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO B01-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO B01-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO B01-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO B01-DS-STATO-ELAB.
       VAL-DCLGEN-B01-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   PORTAFOGLIO VITA - PROCESSO VENDITA                          *
      *   CHIAMATA AL DISPATCHER                                       *
      *   ULTIMO AGG.  21 FEB 2007                                     *
      *----------------------------------------------------------------*

      *----------------------------------------------------------------*
      *               GESTIONE LETTURA (SELECT/FETCH)                  *
      *----------------------------------------------------------------*
       LETTURA-POG.

           IF IDSI0011-SELECT
              PERFORM SELECT-POG
                 THRU SELECT-POG-EX
           ELSE
              PERFORM FETCH-POG
                 THRU FETCH-POG-EX
           END-IF

           MOVE IX-TAB-POG
             TO (SF)-ELE-PARAM-OGG-MAX.

       LETTURA-POG-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         SELECT                                 *
      *----------------------------------------------------------------*
       SELECT-POG.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
      *-->    GESTIRE ERRORE DB
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                     MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'SELECT-POG'
                                             TO IEAI9901-LABEL-ERR
                     MOVE '005017'           TO IEAI9901-COD-ERRORE
                     MOVE 'POG-ID-PARAM-OGG' TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI TO PARAM-OGG
                     MOVE 1                    TO IX-TAB-POG
                     PERFORM VALORIZZA-OUTPUT-POG
                        THRU VALORIZZA-OUTPUT-POG-EX
                  WHEN OTHER
      *------  ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM
                       TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'SELECT-POG'
                       TO IEAI9901-LABEL-ERR
                     MOVE '005016'
                       TO IEAI9901-COD-ERRORE
                     STRING 'PARAM-OGG'          ';'
                            IDSO0011-RETURN-CODE ';'
                            IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SELECT-POG'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              STRING 'PARAM-OGG'          ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       SELECT-POG-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         FETCH                                  *
      *----------------------------------------------------------------*
       FETCH-POG.

             SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
             SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
             SET  WCOM-OVERFLOW-NO                  TO TRUE.

             PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                        OR NOT IDSO0011-SUCCESSFUL-SQL
                        OR WCOM-OVERFLOW-YES

                   PERFORM CALL-DISPATCHER
                      THRU CALL-DISPATCHER-EX

                   IF IDSO0011-SUCCESSFUL-RC
                     EVALUATE TRUE
      *-->               NON TROVATA
                         WHEN IDSO0011-NOT-FOUND
                            IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM
                                TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'FETCH-POG'
                                TO IEAI9901-LABEL-ERR
                              MOVE '005019'
                                TO IEAI9901-COD-ERRORE
                              MOVE 'POG-ID-PARAM-OGG'
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                            END-IF

                         WHEN IDSO0011-SUCCESSFUL-SQL
      *-->             OPERAZIONE ESEGUITA CORRETTAMENTE
                            MOVE IDSO0011-BUFFER-DATI TO PARAM-OGG
                            ADD 1                   TO IX-TAB-POG

      *-->  Se il contatore di lettura � maggiore dell' elemento MAX
      *-->  previsto per la TABELLA allora siamo in overflow
                            IF IX-TAB-POG > (SF)-ELE-PARAM-OGG-MAX
                              SET WCOM-OVERFLOW-YES TO TRUE
                            ELSE
                              PERFORM VALORIZZA-OUTPUT-POG
                                 THRU VALORIZZA-OUTPUT-POG-EX
                              SET  IDSI0011-FETCH-NEXT TO TRUE
                            END-IF
                         WHEN OTHER
      *-------------------ERRORE DI ACCESSO AL DB
                            MOVE WK-PGM
                              TO IEAI9901-COD-SERVIZIO-BE
                            MOVE 'FETCH-POG'
                              TO IEAI9901-LABEL-ERR
                            MOVE '005016'
                              TO IEAI9901-COD-ERRORE
                            STRING 'PARAM-OGG'        ';'
                                 IDSO0011-RETURN-CODE ';'
                                 IDSO0011-SQLCODE
                            DELIMITED BY SIZE
                                             INTO IEAI9901-PARAMETRI-ERR
                            END-STRING
                            PERFORM S0300-RICERCA-GRAVITA-ERRORE
                               THRU EX-S0300
                     END-EVALUATE
                   ELSE
      *-->    GESTIRE ERRORE DISPATCHER
                      MOVE WK-PGM
                        TO IEAI9901-COD-SERVIZIO-BE
                      MOVE 'FETCH-POG'
                        TO IEAI9901-LABEL-ERR
                      MOVE '005016'
                        TO IEAI9901-COD-ERRORE
                      STRING 'PARAM-OGG'          ';'
                             IDSO0011-RETURN-CODE ';'
                             IDSO0011-SQLCODE
                      DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                      END-STRING
                      PERFORM S0300-RICERCA-GRAVITA-ERRORE
                         THRU EX-S0300
                   END-IF
             END-PERFORM.

       FETCH-POG-EX.
           EXIT.

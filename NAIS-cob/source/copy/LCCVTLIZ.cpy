      *----------------------------------------------------------------*
      * DIMENSIONAMENTO OCCURS PER LA TAB. TRCH_DI_LIQ                 *
      * COPY RIPORTANTE IL NUMERO DI OCCURS DA UTILIZZARE              *
      * PER INIZIALIZZA TOT DELLA TABELLA                              *
      *----------------------------------------------------------------*
       01 WK-TLI-MAX.
          03 WK-TLI-MAX-A                 PIC 9(04) VALUE 20.
          03 WK-TLI-MAX-B                 PIC 9(04) VALUE 1250.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVBEL3
      *   ULTIMO AGG. 02 LUG 2014
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-BEL.
           MOVE BEL-ID-BNFICR-LIQ
             TO (SF)-ID-PTF(IX-TAB-BEL)
           MOVE BEL-ID-BNFICR-LIQ
             TO (SF)-ID-BNFICR-LIQ(IX-TAB-BEL)
           MOVE BEL-ID-LIQ
             TO (SF)-ID-LIQ(IX-TAB-BEL)
           MOVE BEL-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-BEL)
           IF BEL-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE BEL-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-BEL)
           END-IF
           MOVE BEL-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-BEL)
           MOVE BEL-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-BEL)
           MOVE BEL-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-BEL)
           IF BEL-ID-RAPP-ANA-NULL = HIGH-VALUES
              MOVE BEL-ID-RAPP-ANA-NULL
                TO (SF)-ID-RAPP-ANA-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-ID-RAPP-ANA
                TO (SF)-ID-RAPP-ANA(IX-TAB-BEL)
           END-IF
           IF BEL-COD-BNFICR-NULL = HIGH-VALUES
              MOVE BEL-COD-BNFICR-NULL
                TO (SF)-COD-BNFICR-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-COD-BNFICR
                TO (SF)-COD-BNFICR(IX-TAB-BEL)
           END-IF
           MOVE BEL-DESC-BNFICR
             TO (SF)-DESC-BNFICR(IX-TAB-BEL)
           IF BEL-PC-LIQ-NULL = HIGH-VALUES
              MOVE BEL-PC-LIQ-NULL
                TO (SF)-PC-LIQ-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-PC-LIQ
                TO (SF)-PC-LIQ(IX-TAB-BEL)
           END-IF
           IF BEL-ESRCN-ATTVT-IMPRS-NULL = HIGH-VALUES
              MOVE BEL-ESRCN-ATTVT-IMPRS-NULL
                TO (SF)-ESRCN-ATTVT-IMPRS-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-ESRCN-ATTVT-IMPRS
                TO (SF)-ESRCN-ATTVT-IMPRS(IX-TAB-BEL)
           END-IF
           IF BEL-TP-IND-BNFICR-NULL = HIGH-VALUES
              MOVE BEL-TP-IND-BNFICR-NULL
                TO (SF)-TP-IND-BNFICR-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-TP-IND-BNFICR
                TO (SF)-TP-IND-BNFICR(IX-TAB-BEL)
           END-IF
           IF BEL-FL-ESE-NULL = HIGH-VALUES
              MOVE BEL-FL-ESE-NULL
                TO (SF)-FL-ESE-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-FL-ESE
                TO (SF)-FL-ESE(IX-TAB-BEL)
           END-IF
           IF BEL-FL-IRREV-NULL = HIGH-VALUES
              MOVE BEL-FL-IRREV-NULL
                TO (SF)-FL-IRREV-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-FL-IRREV
                TO (SF)-FL-IRREV(IX-TAB-BEL)
           END-IF
           IF BEL-IMP-LRD-LIQTO-NULL = HIGH-VALUES
              MOVE BEL-IMP-LRD-LIQTO-NULL
                TO (SF)-IMP-LRD-LIQTO-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-IMP-LRD-LIQTO
                TO (SF)-IMP-LRD-LIQTO(IX-TAB-BEL)
           END-IF
           IF BEL-IMPST-IPT-NULL = HIGH-VALUES
              MOVE BEL-IMPST-IPT-NULL
                TO (SF)-IMPST-IPT-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-IMPST-IPT
                TO (SF)-IMPST-IPT(IX-TAB-BEL)
           END-IF
           IF BEL-IMP-NET-LIQTO-NULL = HIGH-VALUES
              MOVE BEL-IMP-NET-LIQTO-NULL
                TO (SF)-IMP-NET-LIQTO-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-IMP-NET-LIQTO
                TO (SF)-IMP-NET-LIQTO(IX-TAB-BEL)
           END-IF
           IF BEL-RIT-ACC-IPT-NULL = HIGH-VALUES
              MOVE BEL-RIT-ACC-IPT-NULL
                TO (SF)-RIT-ACC-IPT-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-RIT-ACC-IPT
                TO (SF)-RIT-ACC-IPT(IX-TAB-BEL)
           END-IF
           IF BEL-RIT-VIS-IPT-NULL = HIGH-VALUES
              MOVE BEL-RIT-VIS-IPT-NULL
                TO (SF)-RIT-VIS-IPT-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-RIT-VIS-IPT
                TO (SF)-RIT-VIS-IPT(IX-TAB-BEL)
           END-IF
           IF BEL-RIT-TFR-IPT-NULL = HIGH-VALUES
              MOVE BEL-RIT-TFR-IPT-NULL
                TO (SF)-RIT-TFR-IPT-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-RIT-TFR-IPT
                TO (SF)-RIT-TFR-IPT(IX-TAB-BEL)
           END-IF
           IF BEL-IMPST-IRPEF-IPT-NULL = HIGH-VALUES
              MOVE BEL-IMPST-IRPEF-IPT-NULL
                TO (SF)-IMPST-IRPEF-IPT-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-IMPST-IRPEF-IPT
                TO (SF)-IMPST-IRPEF-IPT(IX-TAB-BEL)
           END-IF
           IF BEL-IMPST-SOST-IPT-NULL = HIGH-VALUES
              MOVE BEL-IMPST-SOST-IPT-NULL
                TO (SF)-IMPST-SOST-IPT-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-IMPST-SOST-IPT
                TO (SF)-IMPST-SOST-IPT(IX-TAB-BEL)
           END-IF
           IF BEL-IMPST-PRVR-IPT-NULL = HIGH-VALUES
              MOVE BEL-IMPST-PRVR-IPT-NULL
                TO (SF)-IMPST-PRVR-IPT-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-IMPST-PRVR-IPT
                TO (SF)-IMPST-PRVR-IPT(IX-TAB-BEL)
           END-IF
           IF BEL-IMPST-252-IPT-NULL = HIGH-VALUES
              MOVE BEL-IMPST-252-IPT-NULL
                TO (SF)-IMPST-252-IPT-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-IMPST-252-IPT
                TO (SF)-IMPST-252-IPT(IX-TAB-BEL)
           END-IF
           IF BEL-ID-ASSTO-NULL = HIGH-VALUES
              MOVE BEL-ID-ASSTO-NULL
                TO (SF)-ID-ASSTO-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-ID-ASSTO
                TO (SF)-ID-ASSTO(IX-TAB-BEL)
           END-IF
           IF BEL-TAX-SEP-NULL = HIGH-VALUES
              MOVE BEL-TAX-SEP-NULL
                TO (SF)-TAX-SEP-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-TAX-SEP
                TO (SF)-TAX-SEP(IX-TAB-BEL)
           END-IF
           IF BEL-DT-RISERVE-SOM-P-NULL = HIGH-VALUES
              MOVE BEL-DT-RISERVE-SOM-P-NULL
                TO (SF)-DT-RISERVE-SOM-P-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-DT-RISERVE-SOM-P
                TO (SF)-DT-RISERVE-SOM-P(IX-TAB-BEL)
           END-IF
           IF BEL-DT-VLT-NULL = HIGH-VALUES
              MOVE BEL-DT-VLT-NULL
                TO (SF)-DT-VLT-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-DT-VLT
                TO (SF)-DT-VLT(IX-TAB-BEL)
           END-IF
           IF BEL-TP-STAT-LIQ-BNFICR-NULL = HIGH-VALUES
              MOVE BEL-TP-STAT-LIQ-BNFICR-NULL
                TO (SF)-TP-STAT-LIQ-BNFICR-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-TP-STAT-LIQ-BNFICR
                TO (SF)-TP-STAT-LIQ-BNFICR(IX-TAB-BEL)
           END-IF
           MOVE BEL-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-BEL)
           MOVE BEL-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-BEL)
           MOVE BEL-DS-VER
             TO (SF)-DS-VER(IX-TAB-BEL)
           MOVE BEL-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-BEL)
           MOVE BEL-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-BEL)
           MOVE BEL-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-BEL)
           MOVE BEL-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-BEL)
           IF BEL-RICH-CALC-CNBT-INP-NULL = HIGH-VALUES
              MOVE BEL-RICH-CALC-CNBT-INP-NULL
                TO (SF)-RICH-CALC-CNBT-INP-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-RICH-CALC-CNBT-INP
                TO (SF)-RICH-CALC-CNBT-INP(IX-TAB-BEL)
           END-IF
           IF BEL-IMP-INTR-RIT-PAG-NULL = HIGH-VALUES
              MOVE BEL-IMP-INTR-RIT-PAG-NULL
                TO (SF)-IMP-INTR-RIT-PAG-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-IMP-INTR-RIT-PAG
                TO (SF)-IMP-INTR-RIT-PAG(IX-TAB-BEL)
           END-IF
           IF BEL-DT-ULT-DOCTO-NULL = HIGH-VALUES
              MOVE BEL-DT-ULT-DOCTO-NULL
                TO (SF)-DT-ULT-DOCTO-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-DT-ULT-DOCTO
                TO (SF)-DT-ULT-DOCTO(IX-TAB-BEL)
           END-IF
           IF BEL-DT-DORMIENZA-NULL = HIGH-VALUES
              MOVE BEL-DT-DORMIENZA-NULL
                TO (SF)-DT-DORMIENZA-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-DT-DORMIENZA
                TO (SF)-DT-DORMIENZA(IX-TAB-BEL)
           END-IF
           IF BEL-IMPST-BOLLO-TOT-V-NULL = HIGH-VALUES
              MOVE BEL-IMPST-BOLLO-TOT-V-NULL
                TO (SF)-IMPST-BOLLO-TOT-V-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-IMPST-BOLLO-TOT-V
                TO (SF)-IMPST-BOLLO-TOT-V(IX-TAB-BEL)
           END-IF
           IF BEL-IMPST-VIS-1382011-NULL = HIGH-VALUES
              MOVE BEL-IMPST-VIS-1382011-NULL
                TO (SF)-IMPST-VIS-1382011-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-IMPST-VIS-1382011
                TO (SF)-IMPST-VIS-1382011(IX-TAB-BEL)
           END-IF
           IF BEL-IMPST-SOST-1382011-NULL = HIGH-VALUES
              MOVE BEL-IMPST-SOST-1382011-NULL
                TO (SF)-IMPST-SOST-1382011-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-IMPST-SOST-1382011
                TO (SF)-IMPST-SOST-1382011(IX-TAB-BEL)
           END-IF
           IF BEL-IMPST-VIS-662014-NULL = HIGH-VALUES
              MOVE BEL-IMPST-VIS-662014-NULL
                TO (SF)-IMPST-VIS-662014-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-IMPST-VIS-662014
                TO (SF)-IMPST-VIS-662014(IX-TAB-BEL)
           END-IF
           IF BEL-IMPST-SOST-662014-NULL = HIGH-VALUES
              MOVE BEL-IMPST-SOST-662014-NULL
                TO (SF)-IMPST-SOST-662014-NULL(IX-TAB-BEL)
           ELSE
              MOVE BEL-IMPST-SOST-662014
                TO (SF)-IMPST-SOST-662014(IX-TAB-BEL)
           END-IF.
       VALORIZZA-OUTPUT-BEL-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   AREA I/O SERVIZIO  CREAZIONE BLOCCO
      *   PORTAFOGLIO VITA
      *   LUNG.
      *----------------------------------------------------------------*
           03 LCCC0024-DATI-INPUT.
              05 DATI-BLOCCO.
                 07 LCCC0024-ID-OGG-PRIM         PIC S9(09)  COMP-3.
                 07 LCCC0024-ID-OGG-PRIM-NULL REDEFINES
                    LCCC0024-ID-OGG-PRIM         PIC X(05).
                 07 LCCC0024-TP-OGG-PRIM         PIC X(02).
                 07 LCCC0024-TP-OGG-PRIM-NULL REDEFINES
                    LCCC0024-TP-OGG-PRIM         PIC X(02).
                 07 LCCC0024-ID-OGGETTO          PIC S9(09)  COMP-3.
                 07 LCCC0024-TP-OGGETTO          PIC X(02).
                 07 LCCC0024-COD-BLOCCO          PIC X(05).
                 07 LCCC0024-ID-RICH             PIC S9(9)   COMP-3.

              05 LCCC0024-ELE-MOV-SOS-MAX        PIC S9(04)  COMP.
              05 LCCC0024-DATI-MOV-SOSPESI    OCCURS 10.
                 07 LCCC0024-ID-OGG-SOSP         PIC S9(09)  COMP-3.
                 07 LCCC0024-TP-OGG-SOSP         PIC X(02).
                 07 LCCC0024-ID-MOVI-SOSP        PIC S9(09)  COMP-3.
                 07 LCCC0024-TP-MOVI-SOSP        PIC S9(5)   COMP-3.
                 07 LCCC0024-TP-MOVI-SOSP-NULL   REDEFINES
                    LCCC0024-TP-MOVI-SOSP        PIC X(3).
                 07 LCCC0024-TP-FRM-ASSVA        PIC X(2).
      *          07 LCCC0024-PER-ELAB-DA         PIC S9(08)V COMP-3.
      *          07 LCCC0024-PER-ELAB-A          PIC S9(08)V COMP-3.
                 07 LCCC0024-ID-BATCH            PIC S9(09)  COMP-3.
                 07 LCCC0024-ID-BATCH-NULL    REDEFINES
                    LCCC0024-ID-BATCH            PIC X(05).
                 07 LCCC0024-ID-JOB              PIC S9(09)  COMP-3.
                 07 LCCC0024-ID-JOB-NULL      REDEFINES
                    LCCC0024-ID-JOB              PIC X(05).
                 07 LCCC0024-STEP-ELAB           PIC X(01).
                 07 LCCC0024-D-INPUT-MOVI-SOSP   PIC X(300).

           03 LCCC0024-DATI-OUTPUT.
                 07 LCCC0024-ID-OGG-BLOCCO       PIC S9(09)  COMP-3.
                 07 LCCC0024-VERIFICA-BLOCCO     PIC X(01).
                    88 LCCC0024-BLC-PRESENTE-PTF   VALUE '0'.
                    88 LCCC0024-BLC-CENSITO-PTF    VALUE '1'.

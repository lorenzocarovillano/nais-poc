
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVADE7
      *   CREATA. 04 APR 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-ADE.
           MOVE ADE-ID-ADES
             TO (SF)-ID-PTF
           MOVE ADE-ID-ADES
             TO (SF)-ID-ADES
           MOVE ADE-ID-POLI
             TO (SF)-ID-POLI
           MOVE ADE-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ
           IF ADE-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE ADE-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL
           ELSE
              MOVE ADE-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU
           END-IF.
           MOVE ADE-DT-INI-EFF
             TO (SF)-DT-INI-EFF
           MOVE ADE-DT-END-EFF
             TO (SF)-DT-END-EFF
           IF ADE-IB-PREV-NULL = HIGH-VALUES
              MOVE ADE-IB-PREV-NULL
                TO (SF)-IB-PREV-NULL
           ELSE
              MOVE ADE-IB-PREV
                TO (SF)-IB-PREV
           END-IF.
           IF ADE-IB-OGG-NULL = HIGH-VALUES
              MOVE ADE-IB-OGG-NULL
                TO (SF)-IB-OGG-NULL
           ELSE
              MOVE ADE-IB-OGG
                TO (SF)-IB-OGG
           END-IF.
           MOVE ADE-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           IF ADE-DT-DECOR-NULL = HIGH-VALUES
              MOVE ADE-DT-DECOR-NULL
                TO (SF)-DT-DECOR-NULL
           ELSE
              MOVE ADE-DT-DECOR
                TO (SF)-DT-DECOR
           END-IF.
           IF ADE-DT-SCAD-NULL = HIGH-VALUES
              MOVE ADE-DT-SCAD-NULL
                TO (SF)-DT-SCAD-NULL
           ELSE
              MOVE ADE-DT-SCAD
                TO (SF)-DT-SCAD
           END-IF.
           IF ADE-ETA-A-SCAD-NULL = HIGH-VALUES
              MOVE ADE-ETA-A-SCAD-NULL
                TO (SF)-ETA-A-SCAD-NULL
           ELSE
              MOVE ADE-ETA-A-SCAD
                TO (SF)-ETA-A-SCAD
           END-IF.
           IF ADE-DUR-AA-NULL = HIGH-VALUES
              MOVE ADE-DUR-AA-NULL
                TO (SF)-DUR-AA-NULL
           ELSE
              MOVE ADE-DUR-AA
                TO (SF)-DUR-AA
           END-IF.
           IF ADE-DUR-MM-NULL = HIGH-VALUES
              MOVE ADE-DUR-MM-NULL
                TO (SF)-DUR-MM-NULL
           ELSE
              MOVE ADE-DUR-MM
                TO (SF)-DUR-MM
           END-IF.
           IF ADE-DUR-GG-NULL = HIGH-VALUES
              MOVE ADE-DUR-GG-NULL
                TO (SF)-DUR-GG-NULL
           ELSE
              MOVE ADE-DUR-GG
                TO (SF)-DUR-GG
           END-IF.
           MOVE ADE-TP-RGM-FISC
             TO (SF)-TP-RGM-FISC
           IF ADE-TP-RIAT-NULL = HIGH-VALUES
              MOVE ADE-TP-RIAT-NULL
                TO (SF)-TP-RIAT-NULL
           ELSE
              MOVE ADE-TP-RIAT
                TO (SF)-TP-RIAT
           END-IF.
           MOVE ADE-TP-MOD-PAG-TIT
             TO (SF)-TP-MOD-PAG-TIT
           IF ADE-TP-IAS-NULL = HIGH-VALUES
              MOVE ADE-TP-IAS-NULL
                TO (SF)-TP-IAS-NULL
           ELSE
              MOVE ADE-TP-IAS
                TO (SF)-TP-IAS
           END-IF.
           IF ADE-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
              MOVE ADE-DT-VARZ-TP-IAS-NULL
                TO (SF)-DT-VARZ-TP-IAS-NULL
           ELSE
              MOVE ADE-DT-VARZ-TP-IAS
                TO (SF)-DT-VARZ-TP-IAS
           END-IF.
           IF ADE-PRE-NET-IND-NULL = HIGH-VALUES
              MOVE ADE-PRE-NET-IND-NULL
                TO (SF)-PRE-NET-IND-NULL
           ELSE
              MOVE ADE-PRE-NET-IND
                TO (SF)-PRE-NET-IND
           END-IF.
           IF ADE-PRE-LRD-IND-NULL = HIGH-VALUES
              MOVE ADE-PRE-LRD-IND-NULL
                TO (SF)-PRE-LRD-IND-NULL
           ELSE
              MOVE ADE-PRE-LRD-IND
                TO (SF)-PRE-LRD-IND
           END-IF.
           IF ADE-RAT-LRD-IND-NULL = HIGH-VALUES
              MOVE ADE-RAT-LRD-IND-NULL
                TO (SF)-RAT-LRD-IND-NULL
           ELSE
              MOVE ADE-RAT-LRD-IND
                TO (SF)-RAT-LRD-IND
           END-IF.
           IF ADE-PRSTZ-INI-IND-NULL = HIGH-VALUES
              MOVE ADE-PRSTZ-INI-IND-NULL
                TO (SF)-PRSTZ-INI-IND-NULL
           ELSE
              MOVE ADE-PRSTZ-INI-IND
                TO (SF)-PRSTZ-INI-IND
           END-IF.
           IF ADE-FL-COINC-ASSTO-NULL = HIGH-VALUES
              MOVE ADE-FL-COINC-ASSTO-NULL
                TO (SF)-FL-COINC-ASSTO-NULL
           ELSE
              MOVE ADE-FL-COINC-ASSTO
                TO (SF)-FL-COINC-ASSTO
           END-IF.
           IF ADE-IB-DFLT-NULL = HIGH-VALUES
              MOVE ADE-IB-DFLT-NULL
                TO (SF)-IB-DFLT-NULL
           ELSE
              MOVE ADE-IB-DFLT
                TO (SF)-IB-DFLT
           END-IF.
           IF ADE-MOD-CALC-NULL = HIGH-VALUES
              MOVE ADE-MOD-CALC-NULL
                TO (SF)-MOD-CALC-NULL
           ELSE
              MOVE ADE-MOD-CALC
                TO (SF)-MOD-CALC
           END-IF.
           IF ADE-TP-FNT-CNBTVA-NULL = HIGH-VALUES
              MOVE ADE-TP-FNT-CNBTVA-NULL
                TO (SF)-TP-FNT-CNBTVA-NULL
           ELSE
              MOVE ADE-TP-FNT-CNBTVA
                TO (SF)-TP-FNT-CNBTVA
           END-IF.
           IF ADE-IMP-AZ-NULL = HIGH-VALUES
              MOVE ADE-IMP-AZ-NULL
                TO (SF)-IMP-AZ-NULL
           ELSE
              MOVE ADE-IMP-AZ
                TO (SF)-IMP-AZ
           END-IF.
           IF ADE-IMP-ADER-NULL = HIGH-VALUES
              MOVE ADE-IMP-ADER-NULL
                TO (SF)-IMP-ADER-NULL
           ELSE
              MOVE ADE-IMP-ADER
                TO (SF)-IMP-ADER
           END-IF.
           IF ADE-IMP-TFR-NULL = HIGH-VALUES
              MOVE ADE-IMP-TFR-NULL
                TO (SF)-IMP-TFR-NULL
           ELSE
              MOVE ADE-IMP-TFR
                TO (SF)-IMP-TFR
           END-IF.
           IF ADE-IMP-VOLO-NULL = HIGH-VALUES
              MOVE ADE-IMP-VOLO-NULL
                TO (SF)-IMP-VOLO-NULL
           ELSE
              MOVE ADE-IMP-VOLO
                TO (SF)-IMP-VOLO
           END-IF.
           IF ADE-PC-AZ-NULL = HIGH-VALUES
              MOVE ADE-PC-AZ-NULL
                TO (SF)-PC-AZ-NULL
           ELSE
              MOVE ADE-PC-AZ
                TO (SF)-PC-AZ
           END-IF.
           IF ADE-PC-ADER-NULL = HIGH-VALUES
              MOVE ADE-PC-ADER-NULL
                TO (SF)-PC-ADER-NULL
           ELSE
              MOVE ADE-PC-ADER
                TO (SF)-PC-ADER
           END-IF.
           IF ADE-PC-TFR-NULL = HIGH-VALUES
              MOVE ADE-PC-TFR-NULL
                TO (SF)-PC-TFR-NULL
           ELSE
              MOVE ADE-PC-TFR
                TO (SF)-PC-TFR
           END-IF.
           IF ADE-PC-VOLO-NULL = HIGH-VALUES
              MOVE ADE-PC-VOLO-NULL
                TO (SF)-PC-VOLO-NULL
           ELSE
              MOVE ADE-PC-VOLO
                TO (SF)-PC-VOLO
           END-IF.
           IF ADE-DT-NOVA-RGM-FISC-NULL = HIGH-VALUES
              MOVE ADE-DT-NOVA-RGM-FISC-NULL
                TO (SF)-DT-NOVA-RGM-FISC-NULL
           ELSE
              MOVE ADE-DT-NOVA-RGM-FISC
                TO (SF)-DT-NOVA-RGM-FISC
           END-IF.
           IF ADE-FL-ATTIV-NULL = HIGH-VALUES
              MOVE ADE-FL-ATTIV-NULL
                TO (SF)-FL-ATTIV-NULL
           ELSE
              MOVE ADE-FL-ATTIV
                TO (SF)-FL-ATTIV
           END-IF.
           IF ADE-IMP-REC-RIT-VIS-NULL = HIGH-VALUES
              MOVE ADE-IMP-REC-RIT-VIS-NULL
                TO (SF)-IMP-REC-RIT-VIS-NULL
           ELSE
              MOVE ADE-IMP-REC-RIT-VIS
                TO (SF)-IMP-REC-RIT-VIS
           END-IF.
           IF ADE-IMP-REC-RIT-ACC-NULL = HIGH-VALUES
              MOVE ADE-IMP-REC-RIT-ACC-NULL
                TO (SF)-IMP-REC-RIT-ACC-NULL
           ELSE
              MOVE ADE-IMP-REC-RIT-ACC
                TO (SF)-IMP-REC-RIT-ACC
           END-IF.
           IF ADE-FL-VARZ-STAT-TBGC-NULL = HIGH-VALUES
              MOVE ADE-FL-VARZ-STAT-TBGC-NULL
                TO (SF)-FL-VARZ-STAT-TBGC-NULL
           ELSE
              MOVE ADE-FL-VARZ-STAT-TBGC
                TO (SF)-FL-VARZ-STAT-TBGC
           END-IF.
           IF ADE-FL-PROVZA-MIGRAZ-NULL = HIGH-VALUES
              MOVE ADE-FL-PROVZA-MIGRAZ-NULL
                TO (SF)-FL-PROVZA-MIGRAZ-NULL
           ELSE
              MOVE ADE-FL-PROVZA-MIGRAZ
                TO (SF)-FL-PROVZA-MIGRAZ
           END-IF.
           MOVE ADE-DS-RIGA
             TO (SF)-DS-RIGA
           MOVE ADE-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE ADE-DS-VER
             TO (SF)-DS-VER
           MOVE ADE-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ
           MOVE ADE-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ
           MOVE ADE-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE ADE-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB.
           MOVE ADE-CONCS-PREST
             TO (SF)-CONCS-PREST.
       VALORIZZA-OUTPUT-ADE-EX.
           EXIT.

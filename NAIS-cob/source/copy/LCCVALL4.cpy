
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVALL4
      *   ULTIMO AGG. 31 MAR 2020
      *------------------------------------------------------------

       INIZIA-TOT-ALL.

           PERFORM INIZIA-ZEROES-ALL THRU INIZIA-ZEROES-ALL-EX

           PERFORM INIZIA-SPACES-ALL THRU INIZIA-SPACES-ALL-EX

           PERFORM INIZIA-NULL-ALL THRU INIZIA-NULL-ALL-EX.

       INIZIA-TOT-ALL-EX.
           EXIT.

       INIZIA-NULL-ALL.
           MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-ALL)
           MOVE HIGH-VALUES TO (SF)-DT-END-VLDT-NULL(IX-TAB-ALL)
           MOVE HIGH-VALUES TO (SF)-COD-FND-NULL(IX-TAB-ALL)
           MOVE HIGH-VALUES TO (SF)-COD-TARI-NULL(IX-TAB-ALL)
           MOVE HIGH-VALUES TO (SF)-TP-APPLZ-AST-NULL(IX-TAB-ALL)
           MOVE HIGH-VALUES TO (SF)-PC-RIP-AST-NULL(IX-TAB-ALL)
           MOVE HIGH-VALUES TO (SF)-PERIODO-NULL(IX-TAB-ALL)
           MOVE HIGH-VALUES TO (SF)-TP-RIBIL-FND-NULL(IX-TAB-ALL).
       INIZIA-NULL-ALL-EX.
           EXIT.

       INIZIA-ZEROES-ALL.
           MOVE 0 TO (SF)-ID-AST-ALLOC(IX-TAB-ALL)
           MOVE 0 TO (SF)-ID-STRA-DI-INVST(IX-TAB-ALL)
           MOVE 0 TO (SF)-ID-MOVI-CRZ(IX-TAB-ALL)
           MOVE 0 TO (SF)-DT-INI-VLDT(IX-TAB-ALL)
           MOVE 0 TO (SF)-DT-INI-EFF(IX-TAB-ALL)
           MOVE 0 TO (SF)-DT-END-EFF(IX-TAB-ALL)
           MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-ALL)
           MOVE 0 TO (SF)-DS-RIGA(IX-TAB-ALL)
           MOVE 0 TO (SF)-DS-VER(IX-TAB-ALL)
           MOVE 0 TO (SF)-DS-TS-INI-CPTZ(IX-TAB-ALL)
           MOVE 0 TO (SF)-DS-TS-END-CPTZ(IX-TAB-ALL).
       INIZIA-ZEROES-ALL-EX.
           EXIT.

       INIZIA-SPACES-ALL.
           MOVE SPACES TO (SF)-TP-FND(IX-TAB-ALL)
           MOVE SPACES TO (SF)-DS-OPER-SQL(IX-TAB-ALL)
           MOVE SPACES TO (SF)-DS-UTENTE(IX-TAB-ALL)
           MOVE SPACES TO (SF)-DS-STATO-ELAB(IX-TAB-ALL).
       INIZIA-SPACES-ALL-EX.
           EXIT.

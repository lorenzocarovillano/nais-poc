
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVGRL3
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-GRL.
           MOVE GRL-ID-GAR-LIQ
             TO (SF)-ID-PTF(IX-TAB-GRL)
           MOVE GRL-ID-GAR-LIQ
             TO (SF)-ID-GAR-LIQ(IX-TAB-GRL)
           MOVE GRL-ID-LIQ
             TO (SF)-ID-LIQ(IX-TAB-GRL)
           MOVE GRL-ID-GAR
             TO (SF)-ID-GAR(IX-TAB-GRL)
           MOVE GRL-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-GRL)
           IF GRL-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE GRL-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-GRL)
           ELSE
              MOVE GRL-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-GRL)
           END-IF
           MOVE GRL-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-GRL)
           MOVE GRL-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-GRL)
           MOVE GRL-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-GRL)
           IF GRL-DT-SIN-1O-ASSTO-NULL = HIGH-VALUES
              MOVE GRL-DT-SIN-1O-ASSTO-NULL
                TO (SF)-DT-SIN-1O-ASSTO-NULL(IX-TAB-GRL)
           ELSE
              MOVE GRL-DT-SIN-1O-ASSTO
                TO (SF)-DT-SIN-1O-ASSTO(IX-TAB-GRL)
           END-IF
           IF GRL-CAU-SIN-1O-ASSTO-NULL = HIGH-VALUES
              MOVE GRL-CAU-SIN-1O-ASSTO-NULL
                TO (SF)-CAU-SIN-1O-ASSTO-NULL(IX-TAB-GRL)
           ELSE
              MOVE GRL-CAU-SIN-1O-ASSTO
                TO (SF)-CAU-SIN-1O-ASSTO(IX-TAB-GRL)
           END-IF
           IF GRL-TP-SIN-1O-ASSTO-NULL = HIGH-VALUES
              MOVE GRL-TP-SIN-1O-ASSTO-NULL
                TO (SF)-TP-SIN-1O-ASSTO-NULL(IX-TAB-GRL)
           ELSE
              MOVE GRL-TP-SIN-1O-ASSTO
                TO (SF)-TP-SIN-1O-ASSTO(IX-TAB-GRL)
           END-IF
           IF GRL-DT-SIN-2O-ASSTO-NULL = HIGH-VALUES
              MOVE GRL-DT-SIN-2O-ASSTO-NULL
                TO (SF)-DT-SIN-2O-ASSTO-NULL(IX-TAB-GRL)
           ELSE
              MOVE GRL-DT-SIN-2O-ASSTO
                TO (SF)-DT-SIN-2O-ASSTO(IX-TAB-GRL)
           END-IF
           IF GRL-CAU-SIN-2O-ASSTO-NULL = HIGH-VALUES
              MOVE GRL-CAU-SIN-2O-ASSTO-NULL
                TO (SF)-CAU-SIN-2O-ASSTO-NULL(IX-TAB-GRL)
           ELSE
              MOVE GRL-CAU-SIN-2O-ASSTO
                TO (SF)-CAU-SIN-2O-ASSTO(IX-TAB-GRL)
           END-IF
           IF GRL-TP-SIN-2O-ASSTO-NULL = HIGH-VALUES
              MOVE GRL-TP-SIN-2O-ASSTO-NULL
                TO (SF)-TP-SIN-2O-ASSTO-NULL(IX-TAB-GRL)
           ELSE
              MOVE GRL-TP-SIN-2O-ASSTO
                TO (SF)-TP-SIN-2O-ASSTO(IX-TAB-GRL)
           END-IF
           IF GRL-DT-SIN-3O-ASSTO-NULL = HIGH-VALUES
              MOVE GRL-DT-SIN-3O-ASSTO-NULL
                TO (SF)-DT-SIN-3O-ASSTO-NULL(IX-TAB-GRL)
           ELSE
              MOVE GRL-DT-SIN-3O-ASSTO
                TO (SF)-DT-SIN-3O-ASSTO(IX-TAB-GRL)
           END-IF
           IF GRL-CAU-SIN-3O-ASSTO-NULL = HIGH-VALUES
              MOVE GRL-CAU-SIN-3O-ASSTO-NULL
                TO (SF)-CAU-SIN-3O-ASSTO-NULL(IX-TAB-GRL)
           ELSE
              MOVE GRL-CAU-SIN-3O-ASSTO
                TO (SF)-CAU-SIN-3O-ASSTO(IX-TAB-GRL)
           END-IF
           IF GRL-TP-SIN-3O-ASSTO-NULL = HIGH-VALUES
              MOVE GRL-TP-SIN-3O-ASSTO-NULL
                TO (SF)-TP-SIN-3O-ASSTO-NULL(IX-TAB-GRL)
           ELSE
              MOVE GRL-TP-SIN-3O-ASSTO
                TO (SF)-TP-SIN-3O-ASSTO(IX-TAB-GRL)
           END-IF
           MOVE GRL-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-GRL)
           MOVE GRL-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-GRL)
           MOVE GRL-DS-VER
             TO (SF)-DS-VER(IX-TAB-GRL)
           MOVE GRL-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-GRL)
           MOVE GRL-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-GRL)
           MOVE GRL-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-GRL)
           MOVE GRL-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-GRL).
       VALORIZZA-OUTPUT-GRL-EX.
           EXIT.

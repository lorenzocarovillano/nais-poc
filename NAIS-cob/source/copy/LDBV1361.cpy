

       01  LDBV1361.
           05 LDBV1361-LIVELLO-OPERAZIONI   PIC X(01).
              88 LDBV1361-PRODOTTO          VALUE 'P'.
              88 LDBV1361-GARANZIE          VALUE 'G'.
              88 LDBV1361-OPZIONE           VALUE 'O'.

           05 LDBV1361-OPER-LOG-STAT-BUS    PIC X(03).
           05 LDBV1361-STAT-BUS-TAB.
              10 LDBV1361-STAT-BUS-ELE      OCCURS 07.
                15 LDBV1361-TP-STAT-BUS     PIC X(02).

           05 LDBV1361-STAT-BUS-ELE-EST
              REDEFINES LDBV1361-STAT-BUS-TAB.
              10 LDBV1361-TP-STAT-BUS-01 PIC X(02).
              10 LDBV1361-TP-STAT-BUS-02 PIC X(02).
              10 LDBV1361-TP-STAT-BUS-03 PIC X(02).
              10 LDBV1361-TP-STAT-BUS-04 PIC X(02).
              10 LDBV1361-TP-STAT-BUS-05 PIC X(02).
              10 LDBV1361-TP-STAT-BUS-06 PIC X(02).
              10 LDBV1361-TP-STAT-BUS-07 PIC X(02).

           05 LDBV1361-OPER-LOG-CAUS     PIC X(03).
           05 LDBV1361-TP-CAUS-TAB.
              10 LDBV1361-TP-CAUS-ELE    OCCURS 07.
                 15 LDBV1361-TP-CAUS     PIC X(02).

           05 LDBV1361-TP-CAUS-ELE-EST
              REDEFINES LDBV1361-TP-CAUS-TAB.
              10 LDBV1361-TP-CAUS-01       PIC X(02).
              10 LDBV1361-TP-CAUS-02       PIC X(02).
              10 LDBV1361-TP-CAUS-03       PIC X(02).
              10 LDBV1361-TP-CAUS-04       PIC X(02).
              10 LDBV1361-TP-CAUS-05       PIC X(02).
              10 LDBV1361-TP-CAUS-06       PIC X(02).
              10 LDBV1361-TP-CAUS-07       PIC X(02).

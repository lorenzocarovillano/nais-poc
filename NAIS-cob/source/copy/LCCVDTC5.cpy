
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVDTC5
      *   ULTIMO AGG. 28 NOV 2014
      *------------------------------------------------------------

       VAL-DCLGEN-DTC.
           MOVE (SF)-TP-OGG(IX-TAB-DTC)
              TO DTC-TP-OGG
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-DTC)
              TO DTC-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-DTC)
              TO DTC-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-DTC) NOT NUMERIC
              MOVE 0 TO DTC-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-DTC)
              TO DTC-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-DTC) NOT NUMERIC
              MOVE 0 TO DTC-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-DTC)
              TO DTC-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-DTC)
              TO DTC-COD-COMP-ANIA
           IF (SF)-DT-INI-COP-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-DT-INI-COP-NULL(IX-TAB-DTC)
              TO DTC-DT-INI-COP-NULL
           ELSE
             IF (SF)-DT-INI-COP(IX-TAB-DTC) = ZERO
                MOVE HIGH-VALUES
                TO DTC-DT-INI-COP-NULL
             ELSE
              MOVE (SF)-DT-INI-COP(IX-TAB-DTC)
              TO DTC-DT-INI-COP
             END-IF
           END-IF
           IF (SF)-DT-END-COP-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-DT-END-COP-NULL(IX-TAB-DTC)
              TO DTC-DT-END-COP-NULL
           ELSE
             IF (SF)-DT-END-COP(IX-TAB-DTC) = ZERO
                MOVE HIGH-VALUES
                TO DTC-DT-END-COP-NULL
             ELSE
              MOVE (SF)-DT-END-COP(IX-TAB-DTC)
              TO DTC-DT-END-COP
             END-IF
           END-IF
           IF (SF)-PRE-NET-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-PRE-NET-NULL(IX-TAB-DTC)
              TO DTC-PRE-NET-NULL
           ELSE
              MOVE (SF)-PRE-NET(IX-TAB-DTC)
              TO DTC-PRE-NET
           END-IF
           IF (SF)-INTR-FRAZ-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-INTR-FRAZ-NULL(IX-TAB-DTC)
              TO DTC-INTR-FRAZ-NULL
           ELSE
              MOVE (SF)-INTR-FRAZ(IX-TAB-DTC)
              TO DTC-INTR-FRAZ
           END-IF
           IF (SF)-INTR-MORA-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-INTR-MORA-NULL(IX-TAB-DTC)
              TO DTC-INTR-MORA-NULL
           ELSE
              MOVE (SF)-INTR-MORA(IX-TAB-DTC)
              TO DTC-INTR-MORA
           END-IF
           IF (SF)-INTR-RETDT-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-INTR-RETDT-NULL(IX-TAB-DTC)
              TO DTC-INTR-RETDT-NULL
           ELSE
              MOVE (SF)-INTR-RETDT(IX-TAB-DTC)
              TO DTC-INTR-RETDT
           END-IF
           IF (SF)-INTR-RIAT-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-INTR-RIAT-NULL(IX-TAB-DTC)
              TO DTC-INTR-RIAT-NULL
           ELSE
              MOVE (SF)-INTR-RIAT(IX-TAB-DTC)
              TO DTC-INTR-RIAT
           END-IF
           IF (SF)-DIR-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-DIR-NULL(IX-TAB-DTC)
              TO DTC-DIR-NULL
           ELSE
              MOVE (SF)-DIR(IX-TAB-DTC)
              TO DTC-DIR
           END-IF
           IF (SF)-SPE-MED-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-SPE-MED-NULL(IX-TAB-DTC)
              TO DTC-SPE-MED-NULL
           ELSE
              MOVE (SF)-SPE-MED(IX-TAB-DTC)
              TO DTC-SPE-MED
           END-IF
           IF (SF)-TAX-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-TAX-NULL(IX-TAB-DTC)
              TO DTC-TAX-NULL
           ELSE
              MOVE (SF)-TAX(IX-TAB-DTC)
              TO DTC-TAX
           END-IF
           IF (SF)-SOPR-SAN-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-SOPR-SAN-NULL(IX-TAB-DTC)
              TO DTC-SOPR-SAN-NULL
           ELSE
              MOVE (SF)-SOPR-SAN(IX-TAB-DTC)
              TO DTC-SOPR-SAN
           END-IF
           IF (SF)-SOPR-SPO-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-SOPR-SPO-NULL(IX-TAB-DTC)
              TO DTC-SOPR-SPO-NULL
           ELSE
              MOVE (SF)-SOPR-SPO(IX-TAB-DTC)
              TO DTC-SOPR-SPO
           END-IF
           IF (SF)-SOPR-TEC-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-SOPR-TEC-NULL(IX-TAB-DTC)
              TO DTC-SOPR-TEC-NULL
           ELSE
              MOVE (SF)-SOPR-TEC(IX-TAB-DTC)
              TO DTC-SOPR-TEC
           END-IF
           IF (SF)-SOPR-PROF-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-SOPR-PROF-NULL(IX-TAB-DTC)
              TO DTC-SOPR-PROF-NULL
           ELSE
              MOVE (SF)-SOPR-PROF(IX-TAB-DTC)
              TO DTC-SOPR-PROF
           END-IF
           IF (SF)-SOPR-ALT-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-SOPR-ALT-NULL(IX-TAB-DTC)
              TO DTC-SOPR-ALT-NULL
           ELSE
              MOVE (SF)-SOPR-ALT(IX-TAB-DTC)
              TO DTC-SOPR-ALT
           END-IF
           IF (SF)-PRE-TOT-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-PRE-TOT-NULL(IX-TAB-DTC)
              TO DTC-PRE-TOT-NULL
           ELSE
              MOVE (SF)-PRE-TOT(IX-TAB-DTC)
              TO DTC-PRE-TOT
           END-IF
           IF (SF)-PRE-PP-IAS-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-PRE-PP-IAS-NULL(IX-TAB-DTC)
              TO DTC-PRE-PP-IAS-NULL
           ELSE
              MOVE (SF)-PRE-PP-IAS(IX-TAB-DTC)
              TO DTC-PRE-PP-IAS
           END-IF
           IF (SF)-PRE-SOLO-RSH-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-PRE-SOLO-RSH-NULL(IX-TAB-DTC)
              TO DTC-PRE-SOLO-RSH-NULL
           ELSE
              MOVE (SF)-PRE-SOLO-RSH(IX-TAB-DTC)
              TO DTC-PRE-SOLO-RSH
           END-IF
           IF (SF)-CAR-ACQ-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-CAR-ACQ-NULL(IX-TAB-DTC)
              TO DTC-CAR-ACQ-NULL
           ELSE
              MOVE (SF)-CAR-ACQ(IX-TAB-DTC)
              TO DTC-CAR-ACQ
           END-IF
           IF (SF)-CAR-GEST-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-CAR-GEST-NULL(IX-TAB-DTC)
              TO DTC-CAR-GEST-NULL
           ELSE
              MOVE (SF)-CAR-GEST(IX-TAB-DTC)
              TO DTC-CAR-GEST
           END-IF
           IF (SF)-CAR-INC-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-CAR-INC-NULL(IX-TAB-DTC)
              TO DTC-CAR-INC-NULL
           ELSE
              MOVE (SF)-CAR-INC(IX-TAB-DTC)
              TO DTC-CAR-INC
           END-IF
           IF (SF)-PROV-ACQ-1AA-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-PROV-ACQ-1AA-NULL(IX-TAB-DTC)
              TO DTC-PROV-ACQ-1AA-NULL
           ELSE
              MOVE (SF)-PROV-ACQ-1AA(IX-TAB-DTC)
              TO DTC-PROV-ACQ-1AA
           END-IF
           IF (SF)-PROV-ACQ-2AA-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-PROV-ACQ-2AA-NULL(IX-TAB-DTC)
              TO DTC-PROV-ACQ-2AA-NULL
           ELSE
              MOVE (SF)-PROV-ACQ-2AA(IX-TAB-DTC)
              TO DTC-PROV-ACQ-2AA
           END-IF
           IF (SF)-PROV-RICOR-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-PROV-RICOR-NULL(IX-TAB-DTC)
              TO DTC-PROV-RICOR-NULL
           ELSE
              MOVE (SF)-PROV-RICOR(IX-TAB-DTC)
              TO DTC-PROV-RICOR
           END-IF
           IF (SF)-PROV-INC-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-PROV-INC-NULL(IX-TAB-DTC)
              TO DTC-PROV-INC-NULL
           ELSE
              MOVE (SF)-PROV-INC(IX-TAB-DTC)
              TO DTC-PROV-INC
           END-IF
           IF (SF)-PROV-DA-REC-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-PROV-DA-REC-NULL(IX-TAB-DTC)
              TO DTC-PROV-DA-REC-NULL
           ELSE
              MOVE (SF)-PROV-DA-REC(IX-TAB-DTC)
              TO DTC-PROV-DA-REC
           END-IF
           IF (SF)-COD-DVS-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-COD-DVS-NULL(IX-TAB-DTC)
              TO DTC-COD-DVS-NULL
           ELSE
              MOVE (SF)-COD-DVS(IX-TAB-DTC)
              TO DTC-COD-DVS
           END-IF
           IF (SF)-FRQ-MOVI-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-FRQ-MOVI-NULL(IX-TAB-DTC)
              TO DTC-FRQ-MOVI-NULL
           ELSE
              MOVE (SF)-FRQ-MOVI(IX-TAB-DTC)
              TO DTC-FRQ-MOVI
           END-IF
           MOVE (SF)-TP-RGM-FISC(IX-TAB-DTC)
              TO DTC-TP-RGM-FISC
           IF (SF)-COD-TARI-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-COD-TARI-NULL(IX-TAB-DTC)
              TO DTC-COD-TARI-NULL
           ELSE
              MOVE (SF)-COD-TARI(IX-TAB-DTC)
              TO DTC-COD-TARI
           END-IF
           MOVE (SF)-TP-STAT-TIT(IX-TAB-DTC)
              TO DTC-TP-STAT-TIT
           IF (SF)-IMP-AZ-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-IMP-AZ-NULL(IX-TAB-DTC)
              TO DTC-IMP-AZ-NULL
           ELSE
              MOVE (SF)-IMP-AZ(IX-TAB-DTC)
              TO DTC-IMP-AZ
           END-IF
           IF (SF)-IMP-ADER-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-IMP-ADER-NULL(IX-TAB-DTC)
              TO DTC-IMP-ADER-NULL
           ELSE
              MOVE (SF)-IMP-ADER(IX-TAB-DTC)
              TO DTC-IMP-ADER
           END-IF
           IF (SF)-IMP-TFR-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-IMP-TFR-NULL(IX-TAB-DTC)
              TO DTC-IMP-TFR-NULL
           ELSE
              MOVE (SF)-IMP-TFR(IX-TAB-DTC)
              TO DTC-IMP-TFR
           END-IF
           IF (SF)-IMP-VOLO-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-IMP-VOLO-NULL(IX-TAB-DTC)
              TO DTC-IMP-VOLO-NULL
           ELSE
              MOVE (SF)-IMP-VOLO(IX-TAB-DTC)
              TO DTC-IMP-VOLO
           END-IF
           IF (SF)-MANFEE-ANTIC-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-MANFEE-ANTIC-NULL(IX-TAB-DTC)
              TO DTC-MANFEE-ANTIC-NULL
           ELSE
              MOVE (SF)-MANFEE-ANTIC(IX-TAB-DTC)
              TO DTC-MANFEE-ANTIC
           END-IF
           IF (SF)-MANFEE-RICOR-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-MANFEE-RICOR-NULL(IX-TAB-DTC)
              TO DTC-MANFEE-RICOR-NULL
           ELSE
              MOVE (SF)-MANFEE-RICOR(IX-TAB-DTC)
              TO DTC-MANFEE-RICOR
           END-IF
           IF (SF)-MANFEE-REC-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-MANFEE-REC-NULL(IX-TAB-DTC)
              TO DTC-MANFEE-REC-NULL
           ELSE
              MOVE (SF)-MANFEE-REC(IX-TAB-DTC)
              TO DTC-MANFEE-REC
           END-IF
           IF (SF)-DT-ESI-TIT-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-DT-ESI-TIT-NULL(IX-TAB-DTC)
              TO DTC-DT-ESI-TIT-NULL
           ELSE
             IF (SF)-DT-ESI-TIT(IX-TAB-DTC) = ZERO
                MOVE HIGH-VALUES
                TO DTC-DT-ESI-TIT-NULL
             ELSE
              MOVE (SF)-DT-ESI-TIT(IX-TAB-DTC)
              TO DTC-DT-ESI-TIT
             END-IF
           END-IF
           IF (SF)-SPE-AGE-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-SPE-AGE-NULL(IX-TAB-DTC)
              TO DTC-SPE-AGE-NULL
           ELSE
              MOVE (SF)-SPE-AGE(IX-TAB-DTC)
              TO DTC-SPE-AGE
           END-IF
           IF (SF)-CAR-IAS-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-CAR-IAS-NULL(IX-TAB-DTC)
              TO DTC-CAR-IAS-NULL
           ELSE
              MOVE (SF)-CAR-IAS(IX-TAB-DTC)
              TO DTC-CAR-IAS
           END-IF
           IF (SF)-TOT-INTR-PREST-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-TOT-INTR-PREST-NULL(IX-TAB-DTC)
              TO DTC-TOT-INTR-PREST-NULL
           ELSE
              MOVE (SF)-TOT-INTR-PREST(IX-TAB-DTC)
              TO DTC-TOT-INTR-PREST
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-DTC) NOT NUMERIC
              MOVE 0 TO DTC-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-DTC)
              TO DTC-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-DTC)
              TO DTC-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-DTC) NOT NUMERIC
              MOVE 0 TO DTC-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-DTC)
              TO DTC-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-DTC) NOT NUMERIC
              MOVE 0 TO DTC-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-DTC)
              TO DTC-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-DTC) NOT NUMERIC
              MOVE 0 TO DTC-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-DTC)
              TO DTC-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-DTC)
              TO DTC-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-DTC)
              TO DTC-DS-STATO-ELAB
           IF (SF)-IMP-TRASFE-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-IMP-TRASFE-NULL(IX-TAB-DTC)
              TO DTC-IMP-TRASFE-NULL
           ELSE
              MOVE (SF)-IMP-TRASFE(IX-TAB-DTC)
              TO DTC-IMP-TRASFE
           END-IF
           IF (SF)-IMP-TFR-STRC-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-IMP-TFR-STRC-NULL(IX-TAB-DTC)
              TO DTC-IMP-TFR-STRC-NULL
           ELSE
              MOVE (SF)-IMP-TFR-STRC(IX-TAB-DTC)
              TO DTC-IMP-TFR-STRC
           END-IF
           IF (SF)-NUM-GG-RITARDO-PAG-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-NUM-GG-RITARDO-PAG-NULL(IX-TAB-DTC)
              TO DTC-NUM-GG-RITARDO-PAG-NULL
           ELSE
              MOVE (SF)-NUM-GG-RITARDO-PAG(IX-TAB-DTC)
              TO DTC-NUM-GG-RITARDO-PAG
           END-IF
           IF (SF)-NUM-GG-RIVAL-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-NUM-GG-RIVAL-NULL(IX-TAB-DTC)
              TO DTC-NUM-GG-RIVAL-NULL
           ELSE
              MOVE (SF)-NUM-GG-RIVAL(IX-TAB-DTC)
              TO DTC-NUM-GG-RIVAL
           END-IF
           IF (SF)-ACQ-EXP-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-ACQ-EXP-NULL(IX-TAB-DTC)
              TO DTC-ACQ-EXP-NULL
           ELSE
              MOVE (SF)-ACQ-EXP(IX-TAB-DTC)
              TO DTC-ACQ-EXP
           END-IF
           IF (SF)-REMUN-ASS-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-REMUN-ASS-NULL(IX-TAB-DTC)
              TO DTC-REMUN-ASS-NULL
           ELSE
              MOVE (SF)-REMUN-ASS(IX-TAB-DTC)
              TO DTC-REMUN-ASS
           END-IF
           IF (SF)-COMMIS-INTER-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-COMMIS-INTER-NULL(IX-TAB-DTC)
              TO DTC-COMMIS-INTER-NULL
           ELSE
              MOVE (SF)-COMMIS-INTER(IX-TAB-DTC)
              TO DTC-COMMIS-INTER
           END-IF
           IF (SF)-CNBT-ANTIRAC-NULL(IX-TAB-DTC) = HIGH-VALUES
              MOVE (SF)-CNBT-ANTIRAC-NULL(IX-TAB-DTC)
              TO DTC-CNBT-ANTIRAC-NULL
           ELSE
              MOVE (SF)-CNBT-ANTIRAC(IX-TAB-DTC)
              TO DTC-CNBT-ANTIRAC
           END-IF.
       VAL-DCLGEN-DTC-EX.
           EXIT.

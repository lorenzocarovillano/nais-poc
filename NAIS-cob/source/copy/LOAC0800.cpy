      *----------------------------------------------------------------*
      *   PROCESSO DI POST-VENDITA - PORTAFOGLIO VITA
      *   AREA VARIABILI E COSTANTI DI INPUT
      *   SERVIZIO DI CALCOLO MANAGEMENT FEE
      *----------------------------------------------------------------*
           05 (SF)-AREA-VAR-INP.
              10 (SF)-MOD-CHIAMATA-SERVIZIO PIC X(02).
                 88 (SF)-CON-CALCOLO          VALUE 'SI'.
                 88 (SF)-SENZA-CALCOLO        VALUE 'NO'.

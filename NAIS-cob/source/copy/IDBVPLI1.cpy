       01 PERC-LIQ.
         05 PLI-ID-PERC-LIQ PIC S9(9)V     COMP-3.
         05 PLI-ID-BNFICR-LIQ PIC S9(9)V     COMP-3.
         05 PLI-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
         05 PLI-ID-MOVI-CHIU PIC S9(9)V     COMP-3.
         05 PLI-ID-MOVI-CHIU-NULL REDEFINES
            PLI-ID-MOVI-CHIU   PIC X(5).
         05 PLI-ID-RAPP-ANA PIC S9(9)V     COMP-3.
         05 PLI-ID-RAPP-ANA-NULL REDEFINES
            PLI-ID-RAPP-ANA   PIC X(5).
         05 PLI-DT-INI-EFF   PIC S9(8)V COMP-3.
         05 PLI-DT-END-EFF   PIC S9(8)V COMP-3.
         05 PLI-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 PLI-PC-LIQ PIC S9(3)V9(3) COMP-3.
         05 PLI-PC-LIQ-NULL REDEFINES
            PLI-PC-LIQ   PIC X(4).
         05 PLI-IMP-LIQ PIC S9(12)V9(3) COMP-3.
         05 PLI-IMP-LIQ-NULL REDEFINES
            PLI-IMP-LIQ   PIC X(8).
         05 PLI-TP-MEZ-PAG PIC X(2).
         05 PLI-TP-MEZ-PAG-NULL REDEFINES
            PLI-TP-MEZ-PAG   PIC X(2).
         05 PLI-ITER-PAG-AVV PIC X(1).
         05 PLI-ITER-PAG-AVV-NULL REDEFINES
            PLI-ITER-PAG-AVV   PIC X(1).
         05 PLI-DS-RIGA PIC S9(10)V     COMP-3.
         05 PLI-DS-OPER-SQL PIC X(1).
         05 PLI-DS-VER PIC S9(9)V     COMP-3.
         05 PLI-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
         05 PLI-DS-TS-END-CPTZ PIC S9(18)V     COMP-3.
         05 PLI-DS-UTENTE PIC X(20).
         05 PLI-DS-STATO-ELAB PIC X(1).
         05 PLI-DT-VLT   PIC S9(8)V COMP-3.
         05 PLI-DT-VLT-NULL REDEFINES
            PLI-DT-VLT   PIC X(5).
         05 PLI-INT-CNT-CORR-ACCR-VCHAR.
           49 PLI-INT-CNT-CORR-ACCR-LEN PIC S9(4) COMP-5.
           49 PLI-INT-CNT-CORR-ACCR PIC X(100).
         05 PLI-COD-IBAN-RIT-CON PIC X(34).
         05 PLI-COD-IBAN-RIT-CON-NULL REDEFINES
            PLI-COD-IBAN-RIT-CON   PIC X(34).


      ******************************************************************
      *    TP_RAPP_ANA (Tipo Rapporto Anagrafico)
      ******************************************************************
       01  WS-TP-RAPP-ANA                   PIC X(002) VALUE SPACES.
           88 CONTRAENTE                               VALUE 'CO'.
           88 ASSICURATO                               VALUE 'AS'.
           88 ADERENTE                                 VALUE 'AD'.
           88 LEGALE-RAPPR                             VALUE 'LR'.
           88 BENEFI-DISPO                             VALUE 'BD'.
           88 BENEFI-LIQUI                             VALUE 'BL'.
           88 VINCOLATARIO                             VALUE 'VI'.
           88 PERCIP-LIQUI                             VALUE 'PL'.
           88 CREDIT-PIGNO                             VALUE 'CP'.
           88 REVERS-ASSIC                             VALUE 'RA'.
           88 DELEGATARIO                              VALUE 'DE'.
           88 PERCIP-DISPO                             VALUE 'PD'.
           88 PERCIP-REND                              VALUE 'PR'.
           88 CONTRAENTE-CE                            VALUE 'CE'.
           88 PERCIP-CEDOLE                            VALUE 'PC'.
           88 PERC-RISC-PARZ-PROG                      VALUE 'PP'.
           88 PAGATORE                                 VALUE 'PG'.
           88 PAGATORE-TFR                             VALUE 'PT'.
           88 PAGATORE-FND-CED                         VALUE 'PF'.
           88 PAGAT-VERS-AGG                           VALUE 'PV'.
           88 TUTORE                                   VALUE 'TU'.
           88 PAGATORE-PERFEZ                          VALUE 'PZ'.
           88 PAGAT-RATE-SUCC                          VALUE 'PS'.
           88 RAPP-LEG-PAGAT                           VALUE 'RP'.
           88 PAGAT-PREMI-SUCC                         VALUE 'PA'.
           88 TITOLARE-EFFETTIVO                       VALUE 'TE'.
           88 TERZO-ESECUTORE                          VALUE 'TS'.
CR7779     88 ALTRO-TERZO-ESEC                         VALUE 'TA'.
10819      88 BENEFI-REDDITO-PROG                      VALUE 'B1'.
10819      88 BENEFI-TAKE-PROFIT                       VALUE 'B2'.
11204      88 ESECUTORE                                VALUE 'ES'.
AMLF2      88 TITOLARE-EFFETTIVO-BEN                   VALUE 'TB'.

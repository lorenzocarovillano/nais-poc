       01 IMPST-SOST.
         05 ISO-ID-IMPST-SOST PIC S9(9)V     COMP-3.
         05 ISO-ID-OGG PIC S9(9)V     COMP-3.
         05 ISO-ID-OGG-NULL REDEFINES
            ISO-ID-OGG   PIC X(5).
         05 ISO-TP-OGG PIC X(2).
         05 ISO-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
         05 ISO-ID-MOVI-CHIU PIC S9(9)V     COMP-3.
         05 ISO-ID-MOVI-CHIU-NULL REDEFINES
            ISO-ID-MOVI-CHIU   PIC X(5).
         05 ISO-DT-INI-EFF   PIC S9(8)V COMP-3.
         05 ISO-DT-END-EFF   PIC S9(8)V COMP-3.
         05 ISO-DT-INI-PER   PIC S9(8)V COMP-3.
         05 ISO-DT-INI-PER-NULL REDEFINES
            ISO-DT-INI-PER   PIC X(5).
         05 ISO-DT-END-PER   PIC S9(8)V COMP-3.
         05 ISO-DT-END-PER-NULL REDEFINES
            ISO-DT-END-PER   PIC X(5).
         05 ISO-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 ISO-IMPST-SOST PIC S9(12)V9(3) COMP-3.
         05 ISO-IMPST-SOST-NULL REDEFINES
            ISO-IMPST-SOST   PIC X(8).
         05 ISO-IMPB-IS PIC S9(12)V9(3) COMP-3.
         05 ISO-IMPB-IS-NULL REDEFINES
            ISO-IMPB-IS   PIC X(8).
         05 ISO-ALQ-IS PIC S9(3)V9(3) COMP-3.
         05 ISO-ALQ-IS-NULL REDEFINES
            ISO-ALQ-IS   PIC X(4).
         05 ISO-COD-TRB PIC X(20).
         05 ISO-COD-TRB-NULL REDEFINES
            ISO-COD-TRB   PIC X(20).
         05 ISO-PRSTZ-LRD-ANTE-IS PIC S9(12)V9(3) COMP-3.
         05 ISO-PRSTZ-LRD-ANTE-IS-NULL REDEFINES
            ISO-PRSTZ-LRD-ANTE-IS   PIC X(8).
         05 ISO-RIS-MAT-NET-PREC PIC S9(12)V9(3) COMP-3.
         05 ISO-RIS-MAT-NET-PREC-NULL REDEFINES
            ISO-RIS-MAT-NET-PREC   PIC X(8).
         05 ISO-RIS-MAT-ANTE-TAX PIC S9(12)V9(3) COMP-3.
         05 ISO-RIS-MAT-ANTE-TAX-NULL REDEFINES
            ISO-RIS-MAT-ANTE-TAX   PIC X(8).
         05 ISO-RIS-MAT-POST-TAX PIC S9(12)V9(3) COMP-3.
         05 ISO-RIS-MAT-POST-TAX-NULL REDEFINES
            ISO-RIS-MAT-POST-TAX   PIC X(8).
         05 ISO-PRSTZ-NET PIC S9(12)V9(3) COMP-3.
         05 ISO-PRSTZ-NET-NULL REDEFINES
            ISO-PRSTZ-NET   PIC X(8).
         05 ISO-PRSTZ-PREC PIC S9(12)V9(3) COMP-3.
         05 ISO-PRSTZ-PREC-NULL REDEFINES
            ISO-PRSTZ-PREC   PIC X(8).
         05 ISO-CUM-PRE-VERS PIC S9(12)V9(3) COMP-3.
         05 ISO-CUM-PRE-VERS-NULL REDEFINES
            ISO-CUM-PRE-VERS   PIC X(8).
         05 ISO-TP-CALC-IMPST PIC X(2).
         05 ISO-IMP-GIA-TASSATO PIC S9(12)V9(3) COMP-3.
         05 ISO-DS-RIGA PIC S9(10)V     COMP-3.
         05 ISO-DS-OPER-SQL PIC X(1).
         05 ISO-DS-VER PIC S9(9)V     COMP-3.
         05 ISO-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
         05 ISO-DS-TS-END-CPTZ PIC S9(18)V     COMP-3.
         05 ISO-DS-UTENTE PIC X(20).
         05 ISO-DS-STATO-ELAB PIC X(1).


           EXEC SQL DECLARE DOMANDA_COLLG TABLE
           (
             ID_COMP_QUEST_PAD   DECIMAL(9, 0) NOT NULL,
             ID_COMP_QUEST       DECIMAL(9, 0) NOT NULL,
             DT_INI_EFF          DATE NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             DT_END_EFF          DATE NOT NULL,
             VAL_RISP            VARCHAR(250) NOT NULL,
             TP_GERARCHIA        CHAR(2) NOT NULL,
             DFLT_VISUAL         CHAR(1)
          ) END-EXEC.

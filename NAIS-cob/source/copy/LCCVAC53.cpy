
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVAC53
      *   ULTIMO AGG. 27 OTT 2010
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-AC5.
           MOVE AC5-IDCOMP
             TO (SF)-IDCOMP
           MOVE AC5-CODPROD
             TO (SF)-CODPROD
           MOVE AC5-CODTARI
             TO (SF)-CODTARI
           MOVE AC5-DINIZ
             TO (SF)-DINIZ
           MOVE AC5-NOMEFUNZ
             TO (SF)-NOMEFUNZ
           MOVE AC5-DEND
             TO (SF)-DEND
           IF AC5-ISPRECALC-NULL = HIGH-VALUES
              MOVE AC5-ISPRECALC-NULL
                TO (SF)-ISPRECALC-NULL
           ELSE
              MOVE AC5-ISPRECALC
                TO (SF)-ISPRECALC
           END-IF
           MOVE AC5-NOMEPROGR
             TO (SF)-NOMEPROGR
           MOVE AC5-MODCALC
             TO (SF)-MODCALC
           MOVE AC5-GLOVARLIST
             TO (SF)-GLOVARLIST
           MOVE AC5-STEP-ELAB
             TO (SF)-STEP-ELAB.
       VALORIZZA-OUTPUT-AC5-EX.
           EXIT.

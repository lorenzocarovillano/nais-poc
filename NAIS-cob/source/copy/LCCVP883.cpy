
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVP883
      *   ULTIMO AGG. 17 FEB 2015
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-P88.
           MOVE P88-ID-ATT-SERV-VAL
             TO (SF)-ID-PTF(IX-TAB-P88)
           MOVE P88-ID-ATT-SERV-VAL
             TO (SF)-ID-ATT-SERV-VAL(IX-TAB-P88)
           MOVE P88-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-P88)
           MOVE P88-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-P88)
           IF P88-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE P88-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P88)
           ELSE
              MOVE P88-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-P88)
           END-IF
           MOVE P88-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-P88)
           MOVE P88-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-P88)
           MOVE P88-TP-SERV-VAL
             TO (SF)-TP-SERV-VAL(IX-TAB-P88)
           MOVE P88-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-P88)
           MOVE P88-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-P88)
           MOVE P88-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-P88)
           MOVE P88-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-P88)
           MOVE P88-DS-VER
             TO (SF)-DS-VER(IX-TAB-P88)
           MOVE P88-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-P88)
           MOVE P88-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-P88)
           MOVE P88-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-P88)
           MOVE P88-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-P88)
           IF P88-FL-ALL-FND-NULL = HIGH-VALUES
              MOVE P88-FL-ALL-FND-NULL
                TO (SF)-FL-ALL-FND-NULL(IX-TAB-P88)
           ELSE
              MOVE P88-FL-ALL-FND
                TO (SF)-FL-ALL-FND(IX-TAB-P88)
           END-IF.
       VALORIZZA-OUTPUT-P88-EX.
           EXIT.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA EST_RAPP_ANA
      *   ALIAS E15
      *   ULTIMO AGG. 04 MAR 2020
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-EST-RAPP-ANA PIC S9(9)     COMP-3.
             07 (SF)-ID-RAPP-ANA PIC S9(9)     COMP-3.
             07 (SF)-ID-RAPP-ANA-COLLG PIC S9(9)     COMP-3.
             07 (SF)-ID-RAPP-ANA-COLLG-NULL REDEFINES
                (SF)-ID-RAPP-ANA-COLLG   PIC X(5).
             07 (SF)-ID-OGG PIC S9(9)     COMP-3.
             07 (SF)-TP-OGG PIC X(2).
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-SOGG PIC X(20).
             07 (SF)-COD-SOGG-NULL REDEFINES
                (SF)-COD-SOGG   PIC X(20).
             07 (SF)-TP-RAPP-ANA PIC X(2).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-ID-SEGMENTAZ-CLI PIC S9(9)     COMP-3.
             07 (SF)-ID-SEGMENTAZ-CLI-NULL REDEFINES
                (SF)-ID-SEGMENTAZ-CLI   PIC X(5).
             07 (SF)-FL-COINC-TIT-EFF PIC X(1).
             07 (SF)-FL-COINC-TIT-EFF-NULL REDEFINES
                (SF)-FL-COINC-TIT-EFF   PIC X(1).
             07 (SF)-FL-PERS-ESP-POL PIC X(1).
             07 (SF)-FL-PERS-ESP-POL-NULL REDEFINES
                (SF)-FL-PERS-ESP-POL   PIC X(1).
             07 (SF)-DESC-PERS-ESP-POL PIC X(250).
             07 (SF)-TP-LEG-CNTR PIC X(2).
             07 (SF)-TP-LEG-CNTR-NULL REDEFINES
                (SF)-TP-LEG-CNTR   PIC X(2).
             07 (SF)-DESC-LEG-CNTR PIC X(250).
             07 (SF)-TP-LEG-PERC-BNFICR PIC X(2).
             07 (SF)-TP-LEG-PERC-BNFICR-NULL REDEFINES
                (SF)-TP-LEG-PERC-BNFICR   PIC X(2).
             07 (SF)-D-LEG-PERC-BNFICR PIC X(250).
             07 (SF)-TP-CNT-CORR PIC X(2).
             07 (SF)-TP-CNT-CORR-NULL REDEFINES
                (SF)-TP-CNT-CORR   PIC X(2).
             07 (SF)-TP-COINC-PIC-PAC PIC X(2).
             07 (SF)-TP-COINC-PIC-PAC-NULL REDEFINES
                (SF)-TP-COINC-PIC-PAC   PIC X(2).

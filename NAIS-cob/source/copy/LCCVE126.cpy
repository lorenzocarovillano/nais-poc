      *----------------------------------------------------------------*
      *    COPY      ..... LCCVE126
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO ESTENSIONE DI TRANCHE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-EST-TRCH-DI-GAR.

      *--  TABELLA STORICA
           INITIALIZE EST-TRCH-DI-GAR.

      *--> NOME TABELLA FISICA DB
           MOVE 'EST-TRCH-DI-GAR'               TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO" O "CONVERSAZIONE"
           IF  NOT WE12-ST-INV(IX-TAB-E12)
           AND NOT WE12-ST-CON(IX-TAB-E12)
           AND WE12-ELE-ESTE-TRAN-MAX NOT = 0

              MOVE WMOV-ID-PTF              TO E12-ID-MOVI-CRZ

              EVALUATE TRUE
      *-->    INSERT
                WHEN WE12-ST-ADD(IX-TAB-E12)

      *-->       RICERCA DELL'ID-TRCH-DI-GAR NELLA TABELLA TRANCHE DI
      *-->       GARANZIA
                   MOVE WE12-ID-TRCH-DI-GAR(IX-TAB-E12)
                     TO WS-ID-TRANCHE
                   PERFORM RICERCA-ID-TGA
                      THRU RICERCA-ID-TGA-EX

      *-->       RICERCA DELL'ID-GAR NELLA TABELLA GARANZIA
                   MOVE WE12-ID-GAR(IX-TAB-E12)
                     TO WS-ID-GARANZIA
                   PERFORM RICERCA-ID-GAR
                      THRU RICERCA-ID-GAR-EX

      *-->         ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                   PERFORM ESTR-SEQUENCE
                      THRU ESTR-SEQUENCE-EX

                   IF IDSV0001-ESITO-OK
                      MOVE S090-SEQ-TABELLA   TO WE12-ID-PTF(IX-TAB-E12)
                                                 E12-ID-EST-TRCH-DI-GAR
                      MOVE WPOL-ID-PTF        TO E12-ID-POLI
                      MOVE WADE-ID-PTF        TO E12-ID-ADES
                      MOVE WS-ID-PTF-GAR      TO E12-ID-GAR
                      MOVE WS-ID-PTF-TGA      TO E12-ID-TRCH-DI-GAR

      *-->            TIPO OPERAZIONE DISPATCHER
                      SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
                   END-IF

      *-->    DELETE
                WHEN WE12-ST-DEL(IX-TAB-E12)
                   MOVE WE12-ID-PTF(IX-TAB-E12)
                                         TO E12-ID-EST-TRCH-DI-GAR
                   MOVE WPOL-ID-PTF      TO E12-ID-POLI
                   MOVE WADE-ID-PTF      TO E12-ID-ADES
                   MOVE WE12-ID-GAR(IX-TAB-E12)
                     TO E12-ID-GAR
                   MOVE WE12-ID-TRCH-DI-GAR(IX-TAB-E12)
                     TO E12-ID-TRCH-DI-GAR

      *-->         TIPO OPERAZIONE DISPATCHER
                   SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->    UPDATE
                WHEN WE12-ST-MOD(IX-TAB-E12)
                   MOVE WE12-ID-PTF(IX-TAB-E12)
                                         TO E12-ID-EST-TRCH-DI-GAR
                   MOVE WPOL-ID-PTF      TO E12-ID-POLI
                   MOVE WADE-ID-PTF      TO E12-ID-ADES
                   MOVE WE12-ID-GAR(IX-TAB-E12)
                     TO E12-ID-GAR
                   MOVE WE12-ID-TRCH-DI-GAR(IX-TAB-E12)
                     TO E12-ID-TRCH-DI-GAR

      *-->         TIPO OPERAZIONE DISPATCHER
                   SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN TRANCHE DI GARANZIA
                 PERFORM VAL-DCLGEN-E12
                    THRU VAL-DCLGEN-E12-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-E12
                    THRU VALORIZZA-AREA-DSH-E12-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-EST-TRCH-DI-GAR-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RICERCA DELL'ID-TRCH-DI-GAR DELLA COPY LCCVTGA1 NELLA TAB.
      *    TRANCHE
      *----------------------------------------------------------------*
       RICERCA-ID-TGA.

           SET NON-TROVATO        TO TRUE.

           PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
             UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
                OR TROVATO

                IF WS-ID-TRANCHE = WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
                   MOVE WTGA-ID-PTF(IX-TAB-TGA)
                                  TO WS-ID-PTF-TGA
                   SET  TROVATO   TO TRUE
                END-IF
           END-PERFORM.

       RICERCA-ID-TGA-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-E12.

      *--> DCLGEN TABELLA
           MOVE EST-TRCH-DI-GAR               TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID               TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT  TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-E12-EX.
           EXIT.

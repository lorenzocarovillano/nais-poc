      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA OGG_COLLG
      *   ALIAS OCO
      *   ULTIMO AGG. 09 LUG 2010
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-OGG-COLLG PIC S9(9)     COMP-3.
             07 (SF)-ID-OGG-COINV PIC S9(9)     COMP-3.
             07 (SF)-TP-OGG-COINV PIC X(2).
             07 (SF)-ID-OGG-DER PIC S9(9)     COMP-3.
             07 (SF)-TP-OGG-DER PIC X(2).
             07 (SF)-IB-RIFTO-ESTNO PIC X(40).
             07 (SF)-IB-RIFTO-ESTNO-NULL REDEFINES
                (SF)-IB-RIFTO-ESTNO   PIC X(40).
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-PROD PIC X(12).
             07 (SF)-DT-SCAD   PIC S9(8) COMP-3.
             07 (SF)-DT-SCAD-NULL REDEFINES
                (SF)-DT-SCAD   PIC X(5).
             07 (SF)-TP-COLLGM PIC X(2).
             07 (SF)-TP-TRASF PIC X(2).
             07 (SF)-TP-TRASF-NULL REDEFINES
                (SF)-TP-TRASF   PIC X(2).
             07 (SF)-REC-PROV PIC S9(12)V9(3) COMP-3.
             07 (SF)-REC-PROV-NULL REDEFINES
                (SF)-REC-PROV   PIC X(8).
             07 (SF)-DT-DECOR   PIC S9(8) COMP-3.
             07 (SF)-DT-DECOR-NULL REDEFINES
                (SF)-DT-DECOR   PIC X(5).
             07 (SF)-DT-ULT-PRE-PAG   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-PRE-PAG-NULL REDEFINES
                (SF)-DT-ULT-PRE-PAG   PIC X(5).
             07 (SF)-TOT-PRE PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-PRE-NULL REDEFINES
                (SF)-TOT-PRE   PIC X(8).
             07 (SF)-RIS-MAT PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-MAT-NULL REDEFINES
                (SF)-RIS-MAT   PIC X(8).
             07 (SF)-RIS-ZIL PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-ZIL-NULL REDEFINES
                (SF)-RIS-ZIL   PIC X(8).
             07 (SF)-IMP-TRASF PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-TRASF-NULL REDEFINES
                (SF)-IMP-TRASF   PIC X(8).
             07 (SF)-IMP-REINVST PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-REINVST-NULL REDEFINES
                (SF)-IMP-REINVST   PIC X(8).
             07 (SF)-PC-REINVST-RILIEVI PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-REINVST-RILIEVI-NULL REDEFINES
                (SF)-PC-REINVST-RILIEVI   PIC X(4).
             07 (SF)-IB-2O-RIFTO-ESTNO PIC X(40).
             07 (SF)-IB-2O-RIFTO-ESTNO-NULL REDEFINES
                (SF)-IB-2O-RIFTO-ESTNO   PIC X(40).
             07 (SF)-IND-LIQ-AGG-MAN PIC X(1).
             07 (SF)-IND-LIQ-AGG-MAN-NULL REDEFINES
                (SF)-IND-LIQ-AGG-MAN   PIC X(1).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-IMP-COLLG PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-COLLG-NULL REDEFINES
                (SF)-IMP-COLLG   PIC X(8).
             07 (SF)-PRE-PER-TRASF PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-PER-TRASF-NULL REDEFINES
                (SF)-PRE-PER-TRASF   PIC X(8).
             07 (SF)-CAR-ACQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-CAR-ACQ-NULL REDEFINES
                (SF)-CAR-ACQ   PIC X(8).
             07 (SF)-PRE-1A-ANNUALITA PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-1A-ANNUALITA-NULL REDEFINES
                (SF)-PRE-1A-ANNUALITA   PIC X(8).
             07 (SF)-IMP-TRASFERITO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-TRASFERITO-NULL REDEFINES
                (SF)-IMP-TRASFERITO   PIC X(8).
             07 (SF)-TP-MOD-ABBINAMENTO PIC X(2).
             07 (SF)-TP-MOD-ABBINAMENTO-NULL REDEFINES
                (SF)-TP-MOD-ABBINAMENTO   PIC X(2).
             07 (SF)-PC-PRE-TRASFERITO PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-PRE-TRASFERITO-NULL REDEFINES
                (SF)-PC-PRE-TRASFERITO   PIC X(4).

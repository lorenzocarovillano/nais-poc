      *----------------------------------------------------------------*
      *   ROUTINES OPERAZIONI SULLE STRINGHE
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *   CALCOLA LA LUNGHEZZA DI UN CAMPO (SOLO I BYTE RIEMPITI)
      *----------------------------------------------------------------*
       CALCOLA-LUNGHEZZA.

      *    RECUPERO LA LUNGHEZZA TOTALE DEL CAMPO DI INPUT
           MOVE LENGTH OF WK-IB-PROP-APPO
             TO WK-LUNG-TOT-CAMPO

           IF WK-IB-PROP-APPO = SPACES OR HIGH-VALUE
              MOVE ZEROES
                TO WK-LUNG-CAMPO
           ELSE
              SET FINE-CICLO-NO
               TO TRUE
              PERFORM VARYING IX-TAB FROM 2 BY 1
                        UNTIL IX-TAB > WK-LUNG-TOT-CAMPO
                           OR FINE-CICLO

      *          CARATTERI RIMANENTI
                 COMPUTE WK-NUM-CAR-RIMAN = WK-LUNG-TOT-CAMPO - IX-TAB
      *          SE LA PARTE RIMANENTE DEL CAMPO NON E' VALORIZZATO
      *          VUOL DIRE CHE LA LUNGHEZZA DEL CAMPO E' UGUALE A
      *          IX-TAB - 1
                 IF WK-IB-PROP-APPO(IX-TAB:WK-NUM-CAR-RIMAN)
                                                  = SPACES OR HIGH-VALUE
                    COMPUTE WK-LUNG-CAMPO = IX-TAB - 1
                    SET FINE-CICLO
                     TO TRUE
                 END-IF
              END-PERFORM

      *       SE IL CICLO E' TERMINATO PERCHE' SONO STATI CONTROLLATI
      *       TUTTI I BYTE VUOL DIRE CHE IL CAMPO E' VALORIZZATO
      *       COMPLETAMENTE
              IF NOT FINE-CICLO
                 MOVE WK-LUNG-TOT-CAMPO
                   TO WK-LUNG-CAMPO
              END-IF

           END-IF.

       CALCOLA-LUNGHEZZA-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   INSERISCE ZERI NON SIGNIFICATIVI IN UNA STRINGA
      *----------------------------------------------------------------*
       INSERT-ZERO.

      *    POICHE' LA LUNGHEZZA DELL'IB-PROPOSTA, ESCLUSI I 3 BYTE DEL
      *    CODICE RAMO E' DI 9 BYTE, ESEGUO QUESTA FUNZIONE SOLO SE
      *    LA LUNGHEZZA EFFETTIVA DEL CAMPO E' MINORE DI 9
           IF WK-LUNG-CAMPO < 9
              COMPUTE WK-NUM-CAR-ZERO = 9 - WK-LUNG-CAMPO

              MOVE SPACES
                TO WK-IB-PROP-APPO2

              MOVE WK-IB-PROP-APPO(1:WK-LUNG-CAMPO)
              TO WK-IB-PROP-APPO2(9 - WK-LUNG-CAMPO + 1:WK-LUNG-CAMPO)

              MOVE ZEROES
                TO WK-IB-PROP-APPO2(1:WK-NUM-CAR-ZERO)

              MOVE SPACES
                TO WK-IB-PROP-APPO

              MOVE WK-IB-PROP-APPO2
                TO WK-IB-PROP-APPO

           END-IF.

       INSERT-ZERO-EX.
           EXIT.

       VERIFICA-PROP.

           MOVE WK-IB-PROP-APPO              TO LCCS0025-IB.
           SET  LCCS0025-FL-IB-PROP          TO TRUE.

           CALL LCCS0025 USING AREA-IDSV0001
                               WCOM-AREA-STATI
                               LCCC0025-AREA-COMUNICAZ
           ON EXCEPTION
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SERVIZIO VERIFICA ESISTENZA PROPOSTA'
                                             TO CALL-DESC
              MOVE 'VERIFICA-PROP       '    TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

           IF IDSV0001-ESITO-OK
              IF LCCS0025-FL-PRES-PTF-SI
      *          NUMERO PROPOSTA GIA' PRESENTE IN PORTAFOGLIO
                 MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'VERIFICA-PROP'        TO IEAI9901-LABEL-ERR
                 MOVE '005211'               TO IEAI9901-COD-ERRORE
                 MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

           IF IDSV0001-ESITO-OK
              IF LCCS0025-FL-PRES-PTF-NO
      *          VERIFICO LA PRESENZA DELLA POLIZZA
                 PERFORM VERIFICA-POLI
                    THRU VERIFICA-POLI-EX
              END-IF
           END-IF.

       VERIFICA-PROP-EX.
           EXIT.

       VERIFICA-POLI.

           MOVE WK-IB-PROP-APPO              TO LCCS0025-IB.
           SET  LCCS0025-FL-IB-POLI          TO TRUE.

           CALL LCCS0025 USING AREA-IDSV0001
                               WCOM-AREA-STATI
                               LCCC0025-AREA-COMUNICAZ
           ON EXCEPTION
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SERVIZIO VERIFICA ESISTENZA POLIZZA '
                                             TO CALL-DESC
              MOVE 'VERIFICA-POLI       '    TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.

           IF IDSV0001-ESITO-OK
              IF LCCS0025-FL-PRES-PTF-SI
      *          NUMERO POLIZZA  GIA' PRESENTE IN PORTAFOGLIO
                 MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'VERIFICA-POLI'        TO IEAI9901-LABEL-ERR
                 MOVE '005211'               TO IEAI9901-COD-ERRORE
                 MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           END-IF.

       VERIFICA-POLI-EX.
           EXIT.




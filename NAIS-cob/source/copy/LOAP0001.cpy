      *
      *----------------------------------------------------------------*
      * GESTIONE DEI CONTROLLI SU PRESENZA DI :
      *
      *      BLOCCHI (TABELLA OGGETTO BLOCCO) ->  LCCS0022
      *      MOVIMENTI FUTURI BLOCCANTI       ->  LCCS0023
      *      DEROGHE BLOCCANTI                ->  LOAS0280
      *----------------------------------------------------------------*
      *
       LOAP0001-CONTROLLI.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM LOAP1-S100-CTRL-BLOCCHI
                 THRU LOAP1-S100-CTRL-BLOCCHI-EX
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM LOAP1-S200-CTRL-MOV-FUT
                 THRU LOAP1-S200-CTRL-MOV-FUT-EX
      *
           END-IF.
      *
           IF IDSV0001-ESITO-OK
      *
              PERFORM LOAP1-S300-CTRL-DEROGHE
                 THRU LOAP1-S300-CTRL-DEROGHE-EX
      *
           END-IF.
      *
       LOAP0001-CONTROLLI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VERIFICA PRESENZA DI BLOCCHI (TABELLA OGGETTO BLOCCO)
      *                         LCCS0022
      *----------------------------------------------------------------*
      *
       LOAP1-S100-CTRL-BLOCCHI.
      *
           PERFORM LOAP1-S110-INPUT-LCCS0022
              THRU LOAP1-S110-INPUT-LCCS0022-EX.
      *
           PERFORM LOAP1-S120-CALL-LCCS0022
              THRU LOAP1-S120-CALL-LCCS0022-EX.
      *
           IF IDSV0001-ESITO-OK
      *
              IF ALPO-CALL
      *
                 PERFORM LOAP1-S130-OUT-LCCS0022-A
                    THRU LOAP1-S130-OUT-LCCS0022-A-EX
      *
              ELSE
      *
                 PERFORM LOAP1-S140-OUT-LCCS0022-S
                    THRU LOAP1-S140-OUT-LCCS0022-S-EX
      *
              END-IF
      *
           END-IF.
      *
       LOAP1-S100-CTRL-BLOCCHI-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VERIFICA PRESENZA DI BLOCCHI (TABELLA OGGETTO BLOCCO)
      * VALORIZZAZIONE INPUT SERVIZIO LCCS0022
      *----------------------------------------------------------------*
      *
       LOAP1-S110-INPUT-LCCS0022.
      *
           INITIALIZE AREA-IO-LCCS0022.
      *
           SET LCCC0022-BLOCCO-ATTIVO
             TO TRUE.
      *
           IF INDIVIDUALE
      *
              SET POLIZZA
                TO TRUE
              MOVE WS-TP-OGG
                TO LCCC0022-TP-OGG
              MOVE LOAP1-ID-POLI
                TO WK-OGGETTO-9
      *
           ELSE
      *
              SET ADESIONE
                TO TRUE
              MOVE WS-TP-OGG
                TO LCCC0022-TP-OGG
              MOVE LOAP1-ID-ADES
                TO WK-OGGETTO-9
      *
           END-IF.
      *
           MOVE LOAP1-ID-POLI
             TO LCCC0022-ID-POLI.
           MOVE LOAP1-ID-ADES
             TO LCCC0022-ID-ADES.
           MOVE WK-OGGETTO-9
             TO WK-OGGETTO-X.
      *
       LOAP1-S110-INPUT-LCCS0022-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VERIFICA PRESENZA DI BLOCCHI (TABELLA OGGETTO BLOCCO)
      * CHIAMATA AL SERVIZIO LCCS0022
      *----------------------------------------------------------------*
      *
       LOAP1-S120-CALL-LCCS0022.
      *
           CALL LCCS0022  USING IDSV0001-AREA-CONTESTO
                                WCOM-AREA-STATI
                                AREA-IO-LCCS0022
      *
           ON EXCEPTION
      *
               MOVE WK-PGM
                 TO IEAI9901-COD-SERVIZIO-BE
               MOVE 'SERVIZIO VERIFICA BLOCCHI - LCCS0022'
                 TO CALL-DESC
               MOVE 'LOAP1-S120-CALL-LCCS0022'
                 TO IEAI9901-LABEL-ERR
               PERFORM S0290-ERRORE-DI-SISTEMA
                  THRU EX-S0290
      *
           END-CALL.
      *
       LOAP1-S120-CALL-LCCS0022-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VERIFICA PRESENZA DI BLOCCHI (TABELLA OGGETTO BLOCCO)
      * VERIFICA OUTPUT SERVIZIO LCCS0022 - CHIAMATA DA ALPO -
      *----------------------------------------------------------------*
      *
       LOAP1-S130-OUT-LCCS0022-A.
      *
           IF LCCC0022-FL-PRES-GRAV-BLOC = 'S'
      *
              PERFORM VARYING IX-TAB-BLC FROM 1 BY 1
                UNTIL IX-TAB-BLC > LCCC0022-ELE-MAX-OGG-BLOC
      *
                  IF LCCC0022-OGB-DT-EFFETTO(IX-TAB-BLC)  <
                     WK-DT-EFF-BLC
      *
                     IF LCCC0022-OGB-GRAVITA (IX-TAB-BLC) = 'B'
      *
                        SET WK-BLC-TROVATO-SI
                          TO TRUE
                        MOVE LCCC0022-OGB-DT-EFFETTO (IX-TAB-BLC)
                          TO WK-DT-EFF-BLC
                        MOVE LCCC0022-OGB-ID-OGG-BLOCCO (IX-TAB-BLC)
                          TO WK-APPO-OGB-ID-OGG-BLOCCO
      *
                     END-IF
      *
                  END-IF
      *
              END-PERFORM
      *
           END-IF.
      *
           IF WK-BLC-TROVATO-SI
      *
              IF WK-APPO-OGB-ID-OGG-BLOCCO NOT = WCOM-ID-BLOCCO-CRZ
      *
                 SET IDSV0001-ESITO-KO
                  TO TRUE
      *
              END-IF
      *
           END-IF.
      *
       LOAP1-S130-OUT-LCCS0022-A-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VERIFICA PRESENZA DI BLOCCHI (TABELLA OGGETTO BLOCCO)
      * VERIFICA OUTPUT SERVIZIO LCCS0022 - CHIAMATA STANDARD -
      *----------------------------------------------------------------*
      *
       LOAP1-S140-OUT-LCCS0022-S.
      *
           IF IDSV0001-ESITO-OK
      *
              IF LCCC0022-FL-PRES-GRAV-BLOC = 'S'
      *
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'LOAP1-S140-LCCS0022-STD'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005204'
                   TO IEAI9901-COD-ERRORE
                 STRING WK-OGGETTO-X   ';'
                        LCCC0022-OGB-COD-BLOCCO(1)
                 DELIMITED BY SIZE
                 INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
      *
              END-IF
      *
           END-IF.
      *
       LOAP1-S140-OUT-LCCS0022-S-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VERIFICA PRESENZA DI MOVIMENTI FUTURI BLOCCANTI
      *                         LCCS0023
      *----------------------------------------------------------------*
      *
       LOAP1-S200-CTRL-MOV-FUT.
      *
           PERFORM LOAP1-S210-INPUT-LCCS0023
              THRU LOAP1-S210-INPUT-LCCS0023-EX.
      *
           PERFORM LOAP1-S220-CALL-LCCS0023
              THRU LOAP1-S220-CALL-LCCS0023-EX.
      *
           PERFORM LOAP1-S230-OUT-LCCS0023
              THRU LOAP1-S230-OUT-LCCS0023-EX.
      *
       LOAP1-S200-CTRL-MOV-FUT-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VERIFICA PRESENZA DI MOVIMENTI FUTURI BLOCCANTI
      * VALORIZZAZIONE INPUT SERVIZIO LCCS0023
      *----------------------------------------------------------------*
      *
       LOAP1-S210-INPUT-LCCS0023.
      *
           INITIALIZE AREA-IO-LCCS0023.
      *
           IF INDIVIDUALE
      *
              SET POLIZZA
                TO TRUE
              MOVE WS-TP-OGG
                TO LCCC0023-TP-OGG-PTF
              MOVE LOAP1-ID-POLI
                TO LCCC0023-ID-OGG-PTF
                   WK-OGGETTO-9
      *
           ELSE
      *
              SET ADESIONE
                TO TRUE
              MOVE WS-TP-OGG
                TO LCCC0023-TP-OGG-PTF
              MOVE LOAP1-ID-ADES
                TO LCCC0023-ID-OGG-PTF
                   WK-OGGETTO-9
      *
           END-IF.
      *
           MOVE WK-OGGETTO-9
             TO WK-OGGETTO-X.
      *
       LOAP1-S210-INPUT-LCCS0023-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VERIFICA PRESENZA DI MOVIMENTI FUTURI BLOCCANTI
      * CHIAMATA AL SERVIZIO LCCS0023
      *----------------------------------------------------------------*
      *
       LOAP1-S220-CALL-LCCS0023.
      *
           CALL LCCS0023  USING IDSV0001-AREA-CONTESTO
                                WCOM-AREA-STATI
                                AREA-IO-LCCS0023
      *
           ON EXCEPTION
      *
               MOVE WK-PGM
                 TO IEAI9901-COD-SERVIZIO-BE
               MOVE 'SERVIZIO VERIFICA MOV. FUTURI - LCCS0023'
                 TO CALL-DESC
               MOVE 'LOAP1-S220-CALL-LCCS0023'
                 TO IEAI9901-LABEL-ERR
               PERFORM S0290-ERRORE-DI-SISTEMA
                  THRU EX-S0290
      *
           END-CALL.
      *
       LOAP1-S220-CALL-LCCS0023-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VERIFICA PRESENZA DI MOVIMENTI FUTURI BLOCCANTI
      * VERIFICA OUTPUT SERVIZIO LCCS0023
      *----------------------------------------------------------------*
      *
       LOAP1-S230-OUT-LCCS0023.
      *
           IF IDSV0001-ESITO-OK
      *
              IF LCCC0023-PRE-GRAV-BLOCCANTE = 'S'
      *
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'LOAP1-S230-OUT-LCCS0023'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005205'
                   TO IEAI9901-COD-ERRORE
                 STRING WK-OGGETTO-X   ';'
                        'MOVIMENTO FUTURO BLOCCANTE'
                 DELIMITED BY SIZE
                 INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
      *
              END-IF
      *
           END-IF.
      *
       LOAP1-S230-OUT-LCCS0023-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VERIFICA PRESENZA DI DEROGHE BLOCCANTI
      *                         LOAS0280
      *----------------------------------------------------------------*
      *
       LOAP1-S300-CTRL-DEROGHE.
      *
           PERFORM LOAP1-S310-INPUT-LOAS0280
              THRU LOAP1-S310-INPUT-LOAS0280-EX.
      *
           PERFORM LOAP1-S320-CALL-LOAS0280
              THRU LOAP1-S320-CALL-LOAS0280-EX.
      *
           PERFORM LOAP1-S330-OUT-LOAS0280
              THRU LOAP1-S330-OUT-LOAS0280-EX.
      *
       LOAP1-S300-CTRL-DEROGHE-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VERIFICA PRESENZA DI DEROGHE BLOCCANTI
      * VALORIZZAZIONE INPUT SERVIZIO LOAS0280
      *----------------------------------------------------------------*
      *
       LOAP1-S310-INPUT-LOAS0280.
      *
           INITIALIZE AREA-IO-LOAS0280.
      *
           MOVE LOAP1-ID-POLI
             TO LOAC0280-ID-POLI
                WK-OGGETTO-9.
      *
           IF COLLETTIVA
      *
              SET ADESIONE
                TO TRUE
              MOVE WS-TP-OGG
                TO LOAC0280-TP-OGG
              MOVE LOAP1-ID-ADES
                TO LOAC0280-ID-ADES
                   WK-OGGETTO-9
      *
           ELSE
      *
              SET POLIZZA
                TO TRUE
              MOVE WS-TP-OGG
                TO LOAC0280-TP-OGG
      *
           END-IF.
      *
           MOVE WK-OGGETTO-9
             TO WK-OGGETTO-X.
      *
       LOAP1-S310-INPUT-LOAS0280-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VERIFICA PRESENZA DI DEROGHE BLOCCANTI
      * CHIAMATA AL SERVIZIO LOAS0280
      *----------------------------------------------------------------*
      *
       LOAP1-S320-CALL-LOAS0280.
      *
           CALL LOAS0280  USING AREA-IDSV0001
                                WCOM-AREA-STATI
                                AREA-IO-LOAS0280
      *
           ON EXCEPTION
      *
               MOVE WK-PGM
                 TO IEAI9901-COD-SERVIZIO-BE
               MOVE 'SERVIZIO VERIFICA PRES. DEROGA - LOAS0280'
                 TO CALL-DESC
               MOVE 'LOAP1-S320-CALL-LOAS0280'
                 TO IEAI9901-LABEL-ERR
               PERFORM S0290-ERRORE-DI-SISTEMA
                  THRU EX-S0290
      *
           END-CALL.
      *
       LOAP1-S320-CALL-LOAS0280-EX.
           EXIT.
      *
      *----------------------------------------------------------------*
      * VERIFICA PRESENZA DI DEROGHE BLOCCANTI
      * VERIFICA OUTPUT SERVIZIO LOAS0280
      *----------------------------------------------------------------*
      *
       LOAP1-S330-OUT-LOAS0280.
      *
           IF IDSV0001-ESITO-OK
      *
              IF LOAC0280-DEROGA-BLOCCANTE-SI
      *
                 MOVE LOAC0280-LIVELLO-DEROGA
                   TO WK-LIV-DEROGA
      *
                 IF LOAC0280-LIV-POLI
      *
                    MOVE LOAP1-ID-POLI
                      TO WK-OGGETTO-9
      *
                 ELSE
      *
                    MOVE LOAP1-ID-ADES
                      TO WK-OGGETTO-9
      *
                 END-IF
      *
                 MOVE WK-OGGETTO-9
                   TO WK-OGGETTO-X
      *
                 MOVE WK-PGM
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'LOAP1-S320-CALL-LOAS0280'
                   TO IEAI9901-LABEL-ERR
                 MOVE '005210'
                   TO IEAI9901-COD-ERRORE
                 STRING WK-LIV-DEROGA ';'
                        WK-OGGETTO-X ';'
                       'BLOCCANTE'
                 DELIMITED BY SIZE
                 INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
      *
              END-IF
      *
           END-IF.
      *
       LOAP1-S330-OUT-LOAS0280-EX.
           EXIT.

      ******************************************************************
      *    TP_OGG (TIPO OGGETTO)
      ******************************************************************
       01  WS-TP-OGG                        PIC X(002) VALUE SPACES.
           88 POLIZZA                                  VALUE 'PO'.
           88 ADESIONE                                 VALUE 'AD'.
           88 GARANZIA                                 VALUE 'GA'.
           88 TRANCHE                                  VALUE 'TG'.
           88 RIPARTO-FONDI                            VALUE 'RF'.
           88 LIQUIDAZIONE                             VALUE 'LI'.
           88 PRESTITO                                 VALUE 'PR'.
           88 TITOLO-CONTABILE                         VALUE 'TC'.
           88 GARANZIA-LIQUID                          VALUE 'GL'.
           88 DEROGA                                   VALUE 'DE'.
           88 MOVIMENTO                                VALUE 'MO'.
           88 PREVENTIVO                               VALUE 'PV'.
           88 VINCPEG                                  VALUE 'VP'.
           88 PERCIP-LIQUID                            VALUE 'PL'.

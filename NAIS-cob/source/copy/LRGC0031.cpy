      *----------------------------------------------------------------*
      *    ESTRATTO CONTO - TRACCIATI RECORD OUTPUT (TIPI RECORD)
      *       0000 - INTESTAZIONE
      *       0001 - GENERALE
      *       0002 - SOGGETTO
      *       0003 - DATI GARANZIA
      *       0004 - DATI DETTAGLIO TRANCHE DI GARANZIA
      *       0005 - PREMI INCASSATI E NON INCASSATI NELL'ANNUALITA'
      *       0006 - DETTAGLIO FONDI PREMI EMESSI
      *       0007 - PREMI NON INCASSATI
      *       0008 - DATI FONDO (SOLO RAMO III)
      *       0009 - MOVIMENTI DI LIQUIDAZ. DI RISCATTI PARZIALI
      *       0010 - DETTAGLIO FONDI PER MOVIMENTO/GARANZIA
      *
      *    LEN: 2.500 BYTE
      *----------------------------------------------------------------*
      *01  AREA-LRGC0031.
           03 (SF)-REC-OUTPUT.
      *----------------------------------------------------------------*
      * --    DATI ELABORAZIONE ESTRATTO CONTO (FISSA PER TUTTI I TP)
      *----------------------------------------------------------------*
              05 (SF)-MACROFUNZIONALITA            PIC X(0002).
              05 (SF)-FUNZIONALITA                 PIC X(0004).
              05 (SF)-COD-COMP-ANIA                PIC 9(0005).
              05 (SF)-IB-POLI-IND                  PIC X(0040).
              05 (SF)-IB-POLI-COL                  PIC X(0040).
              05 (SF)-IB-ADES                      PIC X(0040).
              05 (SF)-IB-GAR                       PIC X(0040).
              05 (SF)-IB-TRCH-DI-GAR               PIC X(0040).
              05 (SF)-TP-INVST                     PIC X(0003).
              05 (SF)-TP-RECORD                    PIC X(0004).
              05 (SF)-TP-RECORD-DATI               PIC X(2282).
      *----------------------------------------------------------------*
      * --    TIPO RECORD 0000 - INTESTAZIONE
      *----------------------------------------------------------------*
              05 FILLER REDEFINES (SF)-TP-RECORD-DATI.
                 07 (SF)-ANNO-RIF                  PIC X(0004).
                 07 (SF)-DATA-ELAB                 PIC X(0008).
                 07 (SF)-DATA-RICH-DA              PIC X(0008).
                 07 (SF)-DATA-RICH-A               PIC X(0008).
                 07 FILLER                         PIC X(2254).
      *----------------------------------------------------------------*
      * --    TIPO RECORD 0001 - GENERALE
      *----------------------------------------------------------------*
              05 FILLER REDEFINES (SF)-TP-RECORD-DATI.
      * --       DATI RETE
                 07 (SF)-COD-AG-GEST               PIC 9(0005).
                 07 (SF)-TP-ACQ-CONTR-GEST         PIC 9(0005).
                 07 (SF)-COD-ACQ-CONTR-GEST        PIC 9(0005).
                 07 (SF)-COD-PUNTO-RETE-FIN-GEST   PIC 9(0005).
                 07 (SF)-COD-CAN-GEST              PIC 9(0005).
                 07 (SF)-COD-AG-ACQ                PIC 9(0005).
                 07 (SF)-TP-ACQ-CONTR-ACQ          PIC 9(0005).
                 07 (SF)-COD-ACQ-CONTR-ACQ         PIC 9(0005).
                 07 (SF)-COD-PUNTO-RETE-FIN-ACQ    PIC 9(0005).
                 07 (SF)-COD-CAN-ACQ               PIC 9(0005).
      * --       DATI POLIZZA
                 07 (SF)-TP-POLI                   PIC X(0002).
                 07 (SF)-DT-EMIS-POLI              PIC X(0008).
                 07 (SF)-DT-DECOR-POLI             PIC X(0008).
                 07 (SF)-DT-SCAD-POLI              PIC X(0008).
                 07 (SF)-TP-STAT-POLI              PIC X(0002).
                 07 (SF)-TP-CAUS-POLI              PIC X(0002).
                 07 (SF)-DUR-ANNI-POLI             PIC 9(0005).
                 07 (SF)-DUR-MESI-POLI             PIC 9(0005).
                 07 (SF)-DUR-GIORNI-POLI           PIC 9(0005).
                 07 (SF)-COD-CONVENZ-POLI          PIC X(0012).
                 07 (SF)-DENOMINAZ-PRODOTTO        PIC X(0050).
                 07 (SF)-DT-RIF-DA                 PIC X(0008).
                 07 (SF)-DT-RIF-A                  PIC X(0008).
      * --       DATI ADESIONE
                 07 (SF)-DT-EMIS-ADES              PIC X(0008).
                 07 (SF)-DT-DECOR-ADES             PIC X(0008).
                 07 (SF)-TP-STAT-ADES              PIC X(0002).
                 07 (SF)-TP-CAUS-ADES              PIC X(0002).
                 07 (SF)-DUR-ANNI-ADES             PIC 9(0005).
                 07 (SF)-DUR-MESI-ADES             PIC 9(0005).
                 07 (SF)-DUR-GIORNI-ADES           PIC 9(0005).
      * --       DATI VINCOLO
                 07 (SF)-CAPITALE-VINCOL           PIC 9(0012)V9(03).
                 07 (SF)-DT-VINCOL                 PIC X(0008).
      * --       DATI CALCOLATI
                 07 (SF)-RISC-TOT-DT-RIF-EST-CC    PIC 9(0012)V9(03).
                 07 (SF)-CAP-LIQ-DT-RIF-EST-CC     PIC 9(0012)V9(03).
                 07 (SF)-RISCAT-PER-QUIESC         PIC 9(0012)V9(03).
                 07 (SF)-VAL-RIDUZ                 PIC 9(0012)V9(03).
      *--        DATI LETTI DA PTF
                 07 (SF)-VALORE-IMPOSTA-AA-RIF-EC  PIC 9(0012)V9(003).
      *          07 (SF)-RISCAT-PARZ-RIMB          PIC 9(0012)V9(03).
      * --       DATI CALCOLATI PER PRODOTTI CEDOLA
                 07 (SF)-FLAG-PROD-CON-CEDOLE      PIC X(0002).
                 07 (SF)-IMP-LOR-CED-LIQU-AP       PIC 9(0012)V9(03).
                 07 (SF)-IMP-LOR-CED-LIQU          PIC 9(0012)V9(03).
                 07 (SF)-SPESE-CED-LIQU            PIC 9(0012)V9(03).
                 07 (SF)-IMPOST-SOST-CED-LIQU      PIC 9(0012)V9(03).
                 07 (SF)-IMP-NET-CED-LIQU          PIC 9(0012)V9(03).
                 07 (SF)-DT-ULT-LIQU-CED           PIC X(0008).
                 07 (SF)-ESTREMI-CONTO-CORR.
                    09 (SF)-CC-PAESE               PIC X(0002).
                    09 (SF)-CC-CHECK-DIGIT         PIC X(0002).
                    09 (SF)-CC-CIN                 PIC X(0001).
                    09 (SF)-CC-ABI                 PIC X(0005).
                    09 (SF)-CC-CAB                 PIC X(0005).
                    09 (SF)-CC-NUM-CONTO           PIC X(0020).
                 07 (SF)-IMP-CED-CAPIT             PIC 9(0012)V9(03).
                 07 (SF)-DT-ULT-CAPIT-CED          PIC X(0008).
      * --       DATI GENERICI
                 07 (SF)-DT-ULT-TIT-CONT-INC       PIC X(0008).
                 07 (SF)-TOT-CUM-PREMI-VERS        PIC 9(0012)V9(03).
                 07 (SF)-TOT-CUM-PREMI-VERS-AP     PIC 9(0012)V9(03).
                 07 (SF)-TOT-CUM-PRE-INVST-AA-RIF  PIC 9(0012)V9(03).
                 07 (SF)-IMPOST-SOSTIT-TOTALE      PIC 9(0012)V9(03).
                 07 (SF)-PRESTAZIONE-AP-POL        PIC 9(0012)V9(03).
                 07 (SF)-PRESTAZIONE-AC-POL        PIC 9(0012)V9(03).
                 07 (SF)-IMP-LRD-RISC-PARZ-AP-POL  PIC 9(0012)V9(03).
                 07 (SF)-IMP-LRD-RISC-PARZ-AC-POL  PIC 9(0012)V9(03).
      * --       ID PER LETTURA TABELLE ESTRATTO CONTO
                 07 (SF)-ID-POLI                   PIC 9(0009).
                 07 (SF)-ID-ADES                   PIC 9(0009).
CR8796           07 (SF)-COD-PROD-PARTNER          PIC X(0004).
CR8796           07 (SF)-COD-PROD-ACT              PIC X(0012).
FNZ              07 (SF)-CAPITALE-PROTETTO         PIC 9(0012)V9(03).
IDD              07 (SF)-COS-RUN-ASSVA             PIC 9(12)V9(3).
IDD              07 (SF)-COS-RUN-ASSVA-IDC         PIC 9(12)V9(3).
IDD              07 (SF)-COS-RUN-AMS-FNDINT        PIC 9(12)V9(3).
CR8796*          07 FILLER                         PIC X(1656).
FNZ   *          07 FILLER                         PIC X(1641).
IDD              07 FILLER                         PIC X(1596).
      * --       DATI IMPOSTA DI BOLLO
NEWFIS           07 (SF)-TOT-IMP-BOLLO-RESIDUO     PIC 9(0012)V9(03).
      *          07 FILLER                         PIC X(1687).
      *----------------------------------------------------------------*
      * --    TIPO RECORD 0002 - SOGGETTO
      *----------------------------------------------------------------*
              05 FILLER REDEFINES (SF)-TP-RECORD-DATI.
      * --       DATI CONTRAENTE
                 07 (SF)-COD-SOGG-CONTRA           PIC X(0020).
                 07 (SF)-TP-PERS-CONTRA            PIC X(0001).
                 07 (SF)-NOME-CONTRA               PIC X(0050).
                 07 (SF)-COGNOME-CONTRA            PIC X(0050).
                 07 (SF)-RAG-SOC-CONTRA            PIC X(0100).
                 07 (SF)-INDIRIZZO-CONTRA          PIC X(0100).
                 07 (SF)-LOCALITA-CONTRA           PIC X(0050).
                 07 (SF)-CAP-CONTRA                PIC X(0010).
                 07 (SF)-PROVINCIA-CONTRA          PIC X(0004).
                 07 (SF)-PAESE-CONTRA              PIC X(0050).
                 07 (SF)-SESSO-CONTRA              PIC X(0001).
      * --       DATI ADERENTE
                 07 (SF)-COD-SOGG-ADEREN           PIC X(0020).
                 07 (SF)-TP-PERS-ADEREN            PIC X(0001).
                 07 (SF)-NOME-ADEREN               PIC X(0050).
                 07 (SF)-COGNOME-ADEREN            PIC X(0050).
                 07 (SF)-RAG-SOC-ADEREN            PIC X(0100).
                 07 (SF)-INDIRIZZO-ADEREN          PIC X(0100).
                 07 (SF)-LOCALITA-ADEREN           PIC X(0050).
                 07 (SF)-CAP-ADEREN                PIC X(0010).
                 07 (SF)-PROVINCIA-ADEREN          PIC X(0004).
                 07 (SF)-PAESE-ADEREN              PIC X(0050).
                 07 (SF)-SESSO-ADEREN              PIC X(0001).
      * --       DATI ASSICURATO
                 07 (SF)-COD-SOGG-ASSICU           PIC X(0020).
                 07 (SF)-TP-PERS-ASSICU            PIC X(0001).
                 07 (SF)-NOME-ASSICU               PIC X(0050).
                 07 (SF)-COGNOME-ASSICU            PIC X(0050).
                 07 (SF)-RAG-SOC-ASSICU            PIC X(0100).
                 07 (SF)-FLAG-COINCID-ASSICU       PIC X(0001).
                    88 (SF)-COINCID-SI                 VALUE 'S'.
                    88 (SF)-COINCID-NO                 VALUE 'N'.
                 07 (SF)-SESSO-ASSICU              PIC X(0001).
      * --       DATI VINCOLATARIO
                 07 (SF)-COD-SOGG-VINCOL           PIC X(0020).
                 07 (SF)-TP-PERS-VINCOL            PIC X(0001).
                 07 (SF)-NOME-VINCOL               PIC X(0050).
                 07 (SF)-COGNOME-VINCOL            PIC X(0050).
                 07 (SF)-RAG-SOC-VINCOL            PIC X(0100).
                 07 (SF)-SESSO-VINCOL              PIC X(0001).
                 07 (SF)-BENEFIC-MORTE             PIC X(0400).
                 07 (SF)-BENEFIC-VITA              PIC X(0400).
16339            07 (SF)-FL-BEN-NORMAL             PIC X(0002).
                 07 FILLER                         PIC X(0163).
      *----------------------------------------------------------------*
      * --    TIPO RECORD 0003 - DATI GARANZIA
      *----------------------------------------------------------------*
              05 FILLER REDEFINES (SF)-TP-RECORD-DATI.
      * --       DATI GARANZIA
                 07 (SF)-ID-GAR                    PIC 9(0009).
                 07 (SF)-COD-GAR                   PIC X(0012).
                 07 (SF)-DENOMINAZ-GAR             PIC X(0050).
                 07 (SF)-TP-GAR                    PIC 9(0002).
                 07 (SF)-TP-INVST-GAR              PIC 9(0002).
                 07 (SF)-TP-PERIOD-PREMIO          PIC X(0002).
                 07 (SF)-FRAZIONAMENTO             PIC 9(0005).
                 07 (SF)-DUR-PAGAM-PREMI           PIC 9(0005).
                 07 (SF)-TASSO-TECNICO             PIC 9(0005)V9(09).
      * --       PREMI ALLA DATA DI RIFERIMENTO ESTRATTO CONTO
                 07 (SF)-CUM-PREM-INVST-AA-RIF     PIC 9(0012)V9(03).
                 07 (SF)-CUM-PREM-VERS-AA-RIF      PIC 9(0012)V9(03).
                 07 (SF)-TASSE-PREM-VERS-AA-RIF    PIC 9(0012)V9(03).
                 07 (SF)-PREST-MATUR-AA-RIF        PIC 9(0012)V9(03).
                 07 (SF)-CAP-LIQ-GA                PIC 9(0012)V9(03).
                 07 (SF)-CAP-LIQ-GA-ACC            PIC 9(0012)V9(03).
                 07 (SF)-RISC-TOT-GA               PIC 9(0012)V9(03).
                 07 (SF)-PRE-ATT-GAR               PIC 9(0012)V9(03).
                 07 (SF)-PREST-INIZ-GAR            PIC 9(0012)V9(03).
      * --       PREMI ALLA DATA DELL'ESTRATTO CONTO PRECEDENTE
                 07 (SF)-CUM-PREM-VERS-EC-PREC     PIC 9(0012)V9(03).
                 07 (SF)-TASSE-PREM-VERS-EC-PREC   PIC 9(0012)V9(03).
                 07 (SF)-PREST-MATUR-EC-PREC       PIC 9(0012)V9(03).
      * --       TASSI RENDIMENTO ALLA DATA DI RIFERIM.ESTRATTO CONTO
                 07 (SF)-REND-LOR-GEST-SEP-GAR     PIC 9(0005)V9(09).
                 07 (SF)-ALIQ-RETROC-RICON-GAR     PIC 9(0003)V9(03).
                 07 (SF)-TASSO-ANN-REND-RETROC-GAR PIC 9(0005)V9(09).
                 07 (SF)-PC-COMM-GEST-GAR          PIC 9(0003)V9(03).
                 07 (SF)-TASSO-ANN-RIVAL-PREST-GAR PIC 9(0005)V9(09).
      * --       DATI RISCATTO PARZIALE DI GARANZIA
                 07 (SF)-IMP-LRD-RISC-PARZ-AP-GA   PIC 9(0012)V9(03).
                 07 (SF)-IMP-LRD-RISC-PARZ-AC-GA   PIC 9(0012)V9(03).
                 07 (SF)-TAB-TASSI-MIN-GAR         OCCURS 12.
                     09 (SF)-DT-INI-PER-INV        PIC X(0008).
                     09 (SF)-DT-FIN-PER-INV        PIC X(0008).
                     09 (SF)-TASSO-MIN-GAR         PIC 9(0005)V9(09).
                     09 (SF)-DT-ATTRIB             PIC X(0008).
NEW              07 (SF)-CAP-MIN-LIQU-SCAD-GAR     PIC 9(0012)V9(03).
NEW              07 (SF)-CAP-AGG-COMPL-INF         PIC 9(0012)V9(03).
NEW              07 (SF)-CAP-AGG-GAR-FAM           PIC 9(0012)V9(03).
NEW              07 (SF)-REN-MIN-TRNUT             PIC 9(0005)V9(09).
NEW              07 (SF)-REN-MIN-GARTO             PIC 9(0005)V9(09).
NEW              07 (SF)-COD-FND-GAR               PIC X(0020).
NEW              07 (SF)-DESC-FND-GAR              PIC X(0050).
NEW              07 (SF)-VAL-RIDUZ-PRE-PAG         PIC 9(0012)V9(03).
NEW              07 (SF)-VAL-RISC-PRE-PAG          PIC 9(0012)V9(03).
MIKE             07 (SF)-FL-STORNATE-ANN           PIC X(1).
16721            07 (SF)-FL-CUM-GAR-STORNATE       PIC X(1).
      *          07 FILLER                         PIC X(1461).
NEW   *          07 FILLER                         PIC X(1288).
MIKE  *          07 FILLER                         PIC X(1287).
16721            07 FILLER                         PIC X(1286).

      *----------------------------------------------------------------*
      * --    TIPO RECORD 0004 - DATI DETTAGLIO TRANCHE DI GARANZIA
      *----------------------------------------------------------------*
              05 FILLER REDEFINES (SF)-TP-RECORD-DATI.
      * --       DATI TRANCHE DI GARANZIA
                 07 (SF)-COD-TARI                  PIC X(0012).
                 07 (SF)-TRCH-ID-GAR               PIC 9(0009).
                 07 (SF)-TRCH-DI-GAR               PIC 9(0009).
                 07 (SF)-DT-DECOR-TRCH-GAR         PIC X(0008).
                 07 (SF)-DT-SCAD-TRCH-GAR          PIC X(0008).
                 07 (SF)-DT-RIVALUT                PIC X(0008).
                 07 (SF)-IMP-CONTRIB-AZ            PIC 9(0012)V9(03).
                 07 (SF)-IMP-CONTRIB-ADER          PIC 9(0012)V9(03).
                 07 (SF)-IMP-CONTRIB-TFR           PIC 9(0012)V9(03).
                 07 (SF)-IMP-CONTRIB-VOL           PIC 9(0012)V9(03).
      * --       DATI TECNICI
                 07 (SF)-REND-LOR-GEST-SEP         PIC 9(0005)V9(09).
                 07 (SF)-ALIQ-RETROC-RICON         PIC 9(0003)V9(03).
                 07 (SF)-TASSO-ANN-REND-RETROC     PIC 9(0005)V9(09).
                 07 (SF)-REND-MIN-TRATTEN          PIC 9(0005)V9(09).
                 07 (SF)-TASSO-ANN-RIVAL-PREST     PIC 9(0005)V9(09).
                 07 (SF)-PC-COMM-GEST              PIC 9(0003)V9(03).
                 07 (SF)-CAP-MIN-LIQU-SCAD         PIC 9(0012)V9(03).
                 07 (SF)-COMM-GEST                 PIC 9(0012)V9(03).
                 07 (SF)-DT-ULT-ADEG-PRE           PIC X(0008).
                 07 (SF)-PRSTZ-ULT                 PIC 9(0012)V9(03).
                 07 (SF)-PRSTZ-INI                 PIC 9(0012)V9(03).
                 07 (SF)-TRCH-STATO-BUS            PIC X(0002).
                 07 (SF)-TRCH-CAUS-BUS             PIC X(0002).
NEW              07 (SF)-REN-MIN-GARTO-TG          PIC 9(0005)V9(09).
      *          07 FILLER                         PIC X(2028).
NEW              07 FILLER                         PIC X(2014).
      *----------------------------------------------------------------*
      * --    TIPO RECORD 0005 - PREMI INC. E NON INC. NELL'ANNUALITA'
      *----------------------------------------------------------------*
              05 FILLER REDEFINES (SF)-TP-RECORD-DATI.
      * --       PREMI INCASSATI E NON INCASSATI NELL'ANNUALITA'
                 07 (SF)-COD-GAR-PREMI             PIC X(0012).
                 07 (SF)-ID-TRCH-GAR-PREMI         PIC 9(0009).
                 07 (SF)-ID-DETT-TIT-CONT-PREMI    PIC 9(0009).
                 07 (SF)-ID-TIT-CONT-PREMI         PIC 9(0009).
                 07 (SF)-TP-PREMI                  PIC X(0001).
                 07 (SF)-DT-RIVAL-PREMI            PIC X(0008).
                 07 (SF)-IMP-TOT-PREMI             PIC 9(0012)V9(03).
                 07 (SF)-IMP-NET-PREMI             PIC 9(0012)V9(03).
                 07 (SF)-TASSE-PREMI               PIC 9(0012)V9(03).
                 07 (SF)-DIRITTI-PREMI             PIC 9(0012)V9(03).
                 07 (SF)-SPESE-MEDICHE-PREMI       PIC 9(0012)V9(03).
                 07 (SF)-IMP-PAGATO-PREMI          PIC 9(0012)V9(03).
                 07 (SF)-PRE-INVST-PREMI           PIC 9(0012)V9(03).
                 07 (SF)-DT-EMIS-PREMI             PIC X(0008).
                 07 (SF)-DT-INC-PREMI              PIC X(0008).
                 07 (SF)-DT-STORNO-PREMI           PIC X(0008).
                 07 (SF)-FLAG-INCAS-PREMI          PIC X(0002).
                 07 (SF)-FLAG-STORN-PREMI          PIC X(0002).
                 07 (SF)-FRAZIONAM-PREMI           PIC 9(0005).
                 07 FILLER                         PIC X(2096).
      *----------------------------------------------------------------*
      * --    TIPO RECORD 0006 - DETTAGLIO FONDI PREMI EMESSI
      *----------------------------------------------------------------*
              05 FILLER REDEFINES (SF)-TP-RECORD-DATI.
      * --       DETTAGLIO FONDI PREMI EMESSI
                 07 (SF)-COD-GAR-FND-PRE           PIC X(0012).
                 07 (SF)-ID-TRCH-GAR-FND-PRE       PIC 9(0009).
                 07 (SF)-ID-TIT-CONT-FND-PRE       PIC 9(0009).
                 07 (SF)-ID-DETT-TIT-CONT-FND-PRE  PIC 9(0009).
                 07 (SF)-COD-FND-PRE               PIC X(0020).
                 07 (SF)-NUM-QUO-FND-PRE           PIC 9(0007)V9(05).
                 07 (SF)-VAL-UNIT-QUO-FND-PRE      PIC 9(0012)V9(03).
                 07 (SF)-DT-VAL-QUO-FND-PRE        PIC X(0008).
                 07 (SF)-DT-NAV                    PIC X(0008).
                 07 FILLER                         PIC X(2180).
      *----------------------------------------------------------------*
      * --    TIPO RECORD 0007 - PREMI NON INCASSATI
      *----------------------------------------------------------------*
              05 FILLER REDEFINES (SF)-TP-RECORD-DATI.
      * --       PREMI NON INCASSATI
                 07 (SF)-COD-GAR-NINC              PIC X(0012).
                 07 (SF)-TP-PRE-NINC               PIC X(0001).
                 07 (SF)-ID-TRCH-GAR-NINC          PIC 9(0009).
                 07 (SF)-DT-ULT-RIVAL-NINC         PIC X(0008).
                 07 (SF)-ID-DETT-TIT-CONT-NINC     PIC 9(0009).
                 07 (SF)-ID-TIT-CONT-NINC          PIC 9(0009).
                 07 (SF)-IMP-TOT-NINC              PIC 9(0012)V9(03).
                 07 (SF)-IMP-NET-NINC              PIC 9(0012)V9(03).
                 07 (SF)-TASSE-NINC                PIC 9(0012)V9(03).
                 07 (SF)-DIRITTI-NINC              PIC 9(0012)V9(03).
                 07 (SF)-SPESE-MEDICHE-NINC        PIC 9(0012)V9(03).
                 07 (SF)-IMP-PAGATO-NINC           PIC 9(0012)V9(03).
                 07 (SF)-PRE-INVST-NINC            PIC 9(0012)V9(03).
                 07 (SF)-DT-EMIS-NINC              PIC X(0008).
                 07 (SF)-DT-INC-NINC               PIC X(0008).
                 07 (SF)-DT-STORNO-NINC            PIC X(0008).
                 07 (SF)-FLAG-INCAS-NINC           PIC X(0002).
                 07 (SF)-FLAG-STORN-NINC           PIC X(0002).
                 07 (SF)-FRAZIONAM-NINC            PIC 9(0005).
                 07 FILLER                         PIC X(2096).
      *----------------------------------------------------------------*
      * --    TIPO RECORD 0008 - DATI FONDI
      *----------------------------------------------------------------*
              05 FILLER REDEFINES (SF)-TP-RECORD-DATI.
      * --       DATI FONDI
                 07 (SF)-COD-FND                   PIC X(0020).
                 07 (SF)-DENOMINAZ-FONDO           PIC X(0050).
                 07 (SF)-COD-GAR-FND               PIC X(0012).
                 07 (SF)-NUM-QUO-DISINV-RISC-PARZ  PIC 9(0007)V9(05).
                 07 (SF)-VAL-QUO-DISINV-RISC-PARZ  PIC 9(0012)V9(03).
                 07 (SF)-NUM-QUO-DIS-RISC-PARZ-PRG PIC 9(0007)V9(05).
                 07 (SF)-VAL-QUO-DIS-RISC-PARZ-PRG PIC 9(0012)V9(03).
                 07 (SF)-NUM-QUO-DISINV-IMPOS-SOST PIC 9(0007)V9(05).
                 07 (SF)-VAL-QUO-DISINV-IMPOS-SOST PIC 9(0012)V9(03).
                 07 (SF)-NUM-QUO-DISINV-COMMGES    PIC 9(0007)V9(05).
                 07 (SF)-VAL-QUO-DISINV-COMMGES    PIC 9(0012)V9(03).
                 07 (SF)-NUM-QUO-DISINV-PREL-COSTI PIC 9(0007)V9(05).
                 07 (SF)-VAL-QUO-DISINV-PREL-COSTI PIC 9(0012)V9(03).
                 07 (SF)-NUM-QUO-DISINV-SWITCH     PIC 9(0007)V9(05).
                 07 (SF)-VAL-QUO-DISINV-SWITCH     PIC 9(0012)V9(03).
                 07 (SF)-NUM-QUO-INVST-SWITCH      PIC 9(0007)V9(05).
                 07 (SF)-VAL-QUO-INVST-SWITCH      PIC 9(0012)V9(03).
                 07 (SF)-NUM-QUO-INVST-VERS        PIC 9(0007)V9(05).
                 07 (SF)-VAL-QUO-INVST-VERS        PIC 9(0012)V9(03).
                 07 (SF)-NUM-QUO-INVST-REBATE      PIC 9(0007)V9(05).
                 07 (SF)-VAL-QUO-INVST-REBATE      PIC 9(0012)V9(03).
                 07 (SF)-NUM-QUO-INVST-PURO-RISC   PIC 9(0007)V9(05).
                 07 (SF)-VAL-QUO-INVST-PURO-RISC   PIC 9(0012)V9(03).
                 07 (SF)-NUM-QUO-ATTUALE           PIC 9(0007)V9(05).
                 07 (SF)-CNTRVAL-ATTUALE           PIC 9(0012)V9(03).
                 07 (SF)-DT-VALOR-QUO-ATTUALE      PIC X(0008).
                 07 (SF)-VAL-QUO-ATTUALE           PIC 9(0007)V9(05).
                 07 (SF)-NUM-QUO-PREC              PIC 9(0007)V9(05).
                 07 (SF)-CNTRVAL-PREC              PIC 9(0012)V9(03).
                 07 (SF)-DT-VALOR-QUO-PREC         PIC X(0008).
                 07 (SF)-VAL-QUO-PREC              PIC 9(0007)V9(05).
                 07 (SF)-ID-GAR-FND                PIC 9(0009).
13815            07 (SF)-NUM-QUO-DISINV-COMP-NAV   PIC 9(0007)V9(05).
13815            07 (SF)-VAL-QUO-DISINV-COMP-NAV   PIC 9(0012)V9(03).
13815            07 (SF)-NUM-QUO-INVST-COMP-NAV    PIC 9(0007)V9(05).
13815            07 (SF)-VAL-QUO-INVST-COMP-NAV    PIC 9(0012)V9(03).
13815 *          07 FILLER                         PIC X(1827).
13815            07 FILLER                         PIC X(1773).
      *----------------------------------------------------------------*
      * --    TIPO RECORD 0009 - DATI LIQUIDAZIONE PER RISCATTO PARZ.
      *----------------------------------------------------------------*
              05 FILLER REDEFINES (SF)-TP-RECORD-DATI.
      * --       DATI RISCATTI PARZIALI
                 07 (SF)-IMP-LORDO-RISC-PARZ       PIC 9(0012)V9(03).
                 07 (SF)-PREMI-ATTIVI-PRE-RISC     PIC 9(0012)V9(03).
                 07 (SF)-PREMI-ATTIVI-POST-RISC    PIC 9(0012)V9(03).
                 07 (SF)-DELTA-PREMI-ATTIVI        PIC 9(0012)V9(03).
                 07 (SF)-DT-EFFETTO-LIQ            PIC X(0008).
12193            07 (SF)-RP-TP-MOVI                PIC 9(0005).
                 07 FILLER                         PIC X(2209).
      *----------------------------------------------------------------*
      * --    TIPO RECORD 0010 - DETT. FONDI PER MOVIMENTI E GARANZIE
      *----------------------------------------------------------------*
              05 FILLER REDEFINES (SF)-TP-RECORD-DATI.
      * --       DATI DETTAGLIO MOVIMENTI FINANZIARI
                 07 (SF)-ID-GARANZIA               PIC 9(0009).
                 07 (SF)-CODICE-GARANZIA           PIC X(0012).
                 07 (SF)-DESC-GARANZIA             PIC X(0050).
                 07 (SF)-CODICE-FND                PIC X(0020).
                 07 (SF)-DESCR-FND                 PIC X(0050).
                 07 (SF)-COMPETENZA-MOVI           PIC X(0008).
                 07 (SF)-ID-MOVI                   PIC 9(0009).
                 07 (SF)-TP-MOVI                   PIC 9(0005).
                 07 (SF)-DESC-TP-MOVI              PIC X(0050).
                 07 (SF)-VERSO-MOVI                PIC X(0002).
                 07 (SF)-DT-EFF-MOVI               PIC X(0008).
                 07 (SF)-IMP-VERS                  PIC 9(0012)V9(03).
                 07 (SF)-IMP-OPER                  PIC 9(0012)V9(03).
                 07 (SF)-DATA-NAV                  PIC X(0008).
                 07 (SF)-VAL-QUO-NAV               PIC 9(0012)V9(03).
                 07 (SF)-NUMERO-QUO                PIC 9(0007)V9(05).
                 07 FILLER                         PIC X(1994).
      *----------------------------------------------------------------*

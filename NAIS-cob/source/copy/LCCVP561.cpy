      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA QUEST_ADEG_VER
      *   ALIAS P56
      *   ULTIMO AGG. 26 MAR 2019
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-QUEST-ADEG-VER PIC S9(9)     COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-RAPP-ANA PIC S9(9)     COMP-3.
             07 (SF)-ID-RAPP-ANA-NULL REDEFINES
                (SF)-ID-RAPP-ANA   PIC X(5).
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-NATURA-OPRZ PIC X(2).
             07 (SF)-NATURA-OPRZ-NULL REDEFINES
                (SF)-NATURA-OPRZ   PIC X(2).
             07 (SF)-ORGN-FND PIC X(2).
             07 (SF)-ORGN-FND-NULL REDEFINES
                (SF)-ORGN-FND   PIC X(2).
             07 (SF)-COD-QLFC-PROF PIC X(20).
             07 (SF)-COD-QLFC-PROF-NULL REDEFINES
                (SF)-COD-QLFC-PROF   PIC X(20).
             07 (SF)-COD-NAZ-QLFC-PROF PIC X(4).
             07 (SF)-COD-NAZ-QLFC-PROF-NULL REDEFINES
                (SF)-COD-NAZ-QLFC-PROF   PIC X(4).
             07 (SF)-COD-PRV-QLFC-PROF PIC X(4).
             07 (SF)-COD-PRV-QLFC-PROF-NULL REDEFINES
                (SF)-COD-PRV-QLFC-PROF   PIC X(4).
             07 (SF)-FNT-REDD PIC X(2).
             07 (SF)-FNT-REDD-NULL REDEFINES
                (SF)-FNT-REDD   PIC X(2).
             07 (SF)-REDD-FATT-ANNU PIC S9(12)V9(3) COMP-3.
             07 (SF)-REDD-FATT-ANNU-NULL REDEFINES
                (SF)-REDD-FATT-ANNU   PIC X(8).
             07 (SF)-COD-ATECO PIC X(20).
             07 (SF)-COD-ATECO-NULL REDEFINES
                (SF)-COD-ATECO   PIC X(20).
             07 (SF)-VALUT-COLL PIC X(2).
             07 (SF)-VALUT-COLL-NULL REDEFINES
                (SF)-VALUT-COLL   PIC X(2).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-LUOGO-COSTITUZIONE PIC X(250).
             07 (SF)-TP-MOVI PIC S9(5)     COMP-3.
             07 (SF)-TP-MOVI-NULL REDEFINES
                (SF)-TP-MOVI   PIC X(3).
             07 (SF)-FL-RAG-RAPP PIC X(1).
             07 (SF)-FL-RAG-RAPP-NULL REDEFINES
                (SF)-FL-RAG-RAPP   PIC X(1).
             07 (SF)-FL-PRSZ-TIT-EFF PIC X(1).
             07 (SF)-FL-PRSZ-TIT-EFF-NULL REDEFINES
                (SF)-FL-PRSZ-TIT-EFF   PIC X(1).
             07 (SF)-TP-MOT-RISC PIC X(2).
             07 (SF)-TP-MOT-RISC-NULL REDEFINES
                (SF)-TP-MOT-RISC   PIC X(2).
             07 (SF)-TP-PNT-VND PIC X(2).
             07 (SF)-TP-PNT-VND-NULL REDEFINES
                (SF)-TP-PNT-VND   PIC X(2).
             07 (SF)-TP-ADEG-VER PIC X(2).
             07 (SF)-TP-ADEG-VER-NULL REDEFINES
                (SF)-TP-ADEG-VER   PIC X(2).
             07 (SF)-TP-RELA-ESEC PIC X(2).
             07 (SF)-TP-RELA-ESEC-NULL REDEFINES
                (SF)-TP-RELA-ESEC   PIC X(2).
             07 (SF)-TP-SCO-FIN-RAPP PIC X(2).
             07 (SF)-TP-SCO-FIN-RAPP-NULL REDEFINES
                (SF)-TP-SCO-FIN-RAPP   PIC X(2).
             07 (SF)-FL-PRSZ-3O-PAGAT PIC X(1).
             07 (SF)-FL-PRSZ-3O-PAGAT-NULL REDEFINES
                (SF)-FL-PRSZ-3O-PAGAT   PIC X(1).
             07 (SF)-AREA-GEO-PROV-FND PIC X(4).
             07 (SF)-AREA-GEO-PROV-FND-NULL REDEFINES
                (SF)-AREA-GEO-PROV-FND   PIC X(4).
             07 (SF)-TP-DEST-FND PIC X(2).
             07 (SF)-TP-DEST-FND-NULL REDEFINES
                (SF)-TP-DEST-FND   PIC X(2).
             07 (SF)-FL-PAESE-RESID-AUT PIC X(1).
             07 (SF)-FL-PAESE-RESID-AUT-NULL REDEFINES
                (SF)-FL-PAESE-RESID-AUT   PIC X(1).
             07 (SF)-FL-PAESE-CIT-AUT PIC X(1).
             07 (SF)-FL-PAESE-CIT-AUT-NULL REDEFINES
                (SF)-FL-PAESE-CIT-AUT   PIC X(1).
             07 (SF)-FL-PAESE-NAZ-AUT PIC X(1).
             07 (SF)-FL-PAESE-NAZ-AUT-NULL REDEFINES
                (SF)-FL-PAESE-NAZ-AUT   PIC X(1).
             07 (SF)-COD-PROF-PREC PIC X(20).
             07 (SF)-COD-PROF-PREC-NULL REDEFINES
                (SF)-COD-PROF-PREC   PIC X(20).
             07 (SF)-FL-AUT-PEP PIC X(1).
             07 (SF)-FL-AUT-PEP-NULL REDEFINES
                (SF)-FL-AUT-PEP   PIC X(1).
             07 (SF)-FL-IMP-CAR-PUB PIC X(1).
             07 (SF)-FL-IMP-CAR-PUB-NULL REDEFINES
                (SF)-FL-IMP-CAR-PUB   PIC X(1).
             07 (SF)-FL-LIS-TERR-SORV PIC X(1).
             07 (SF)-FL-LIS-TERR-SORV-NULL REDEFINES
                (SF)-FL-LIS-TERR-SORV   PIC X(1).
             07 (SF)-TP-SIT-FIN-PAT PIC X(2).
             07 (SF)-TP-SIT-FIN-PAT-NULL REDEFINES
                (SF)-TP-SIT-FIN-PAT   PIC X(2).
             07 (SF)-TP-SIT-FIN-PAT-CON PIC X(2).
             07 (SF)-TP-SIT-FIN-PAT-CON-NULL REDEFINES
                (SF)-TP-SIT-FIN-PAT-CON   PIC X(2).
             07 (SF)-IMP-TOT-AFF-UTIL PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-TOT-AFF-UTIL-NULL REDEFINES
                (SF)-IMP-TOT-AFF-UTIL   PIC X(8).
             07 (SF)-IMP-TOT-FIN-UTIL PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-TOT-FIN-UTIL-NULL REDEFINES
                (SF)-IMP-TOT-FIN-UTIL   PIC X(8).
             07 (SF)-IMP-TOT-AFF-ACC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-TOT-AFF-ACC-NULL REDEFINES
                (SF)-IMP-TOT-AFF-ACC   PIC X(8).
             07 (SF)-IMP-TOT-FIN-ACC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-TOT-FIN-ACC-NULL REDEFINES
                (SF)-IMP-TOT-FIN-ACC   PIC X(8).
             07 (SF)-TP-FRM-GIUR-SAV PIC X(2).
             07 (SF)-TP-FRM-GIUR-SAV-NULL REDEFINES
                (SF)-TP-FRM-GIUR-SAV   PIC X(2).
             07 (SF)-REG-COLL-POLI PIC X(4).
             07 (SF)-REG-COLL-POLI-NULL REDEFINES
                (SF)-REG-COLL-POLI   PIC X(4).
             07 (SF)-NUM-TEL PIC X(20).
             07 (SF)-NUM-TEL-NULL REDEFINES
                (SF)-NUM-TEL   PIC X(20).
             07 (SF)-NUM-DIP PIC S9(5)     COMP-3.
             07 (SF)-NUM-DIP-NULL REDEFINES
                (SF)-NUM-DIP   PIC X(3).
             07 (SF)-TP-SIT-FAM-CONV PIC X(2).
             07 (SF)-TP-SIT-FAM-CONV-NULL REDEFINES
                (SF)-TP-SIT-FAM-CONV   PIC X(2).
             07 (SF)-COD-PROF-CON PIC X(20).
             07 (SF)-COD-PROF-CON-NULL REDEFINES
                (SF)-COD-PROF-CON   PIC X(20).
             07 (SF)-FL-ES-PROC-PEN PIC X(1).
             07 (SF)-FL-ES-PROC-PEN-NULL REDEFINES
                (SF)-FL-ES-PROC-PEN   PIC X(1).
             07 (SF)-TP-COND-CLIENTE PIC X(2).
             07 (SF)-TP-COND-CLIENTE-NULL REDEFINES
                (SF)-TP-COND-CLIENTE   PIC X(2).
             07 (SF)-COD-SAE PIC X(4).
             07 (SF)-COD-SAE-NULL REDEFINES
                (SF)-COD-SAE   PIC X(4).
             07 (SF)-TP-OPER-ESTERO PIC X(2).
             07 (SF)-TP-OPER-ESTERO-NULL REDEFINES
                (SF)-TP-OPER-ESTERO   PIC X(2).
             07 (SF)-STAT-OPER-ESTERO PIC X(4).
             07 (SF)-STAT-OPER-ESTERO-NULL REDEFINES
                (SF)-STAT-OPER-ESTERO   PIC X(4).
             07 (SF)-COD-PRV-SVOL-ATT PIC X(4).
             07 (SF)-COD-PRV-SVOL-ATT-NULL REDEFINES
                (SF)-COD-PRV-SVOL-ATT   PIC X(4).
             07 (SF)-COD-STAT-SVOL-ATT PIC X(4).
             07 (SF)-COD-STAT-SVOL-ATT-NULL REDEFINES
                (SF)-COD-STAT-SVOL-ATT   PIC X(4).
             07 (SF)-TP-SOC PIC X(2).
             07 (SF)-TP-SOC-NULL REDEFINES
                (SF)-TP-SOC   PIC X(2).
             07 (SF)-FL-IND-SOC-QUOT PIC X(1).
             07 (SF)-FL-IND-SOC-QUOT-NULL REDEFINES
                (SF)-FL-IND-SOC-QUOT   PIC X(1).
             07 (SF)-TP-SIT-GIUR PIC X(2).
             07 (SF)-TP-SIT-GIUR-NULL REDEFINES
                (SF)-TP-SIT-GIUR   PIC X(2).
             07 (SF)-PC-QUO-DET-TIT-EFF PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-QUO-DET-TIT-EFF-NULL REDEFINES
                (SF)-PC-QUO-DET-TIT-EFF   PIC X(4).
             07 (SF)-TP-PRFL-RSH-PEP PIC X(2).
             07 (SF)-TP-PRFL-RSH-PEP-NULL REDEFINES
                (SF)-TP-PRFL-RSH-PEP   PIC X(2).
             07 (SF)-TP-PEP PIC X(2).
             07 (SF)-TP-PEP-NULL REDEFINES
                (SF)-TP-PEP   PIC X(2).
             07 (SF)-FL-NOT-PREG PIC X(1).
             07 (SF)-FL-NOT-PREG-NULL REDEFINES
                (SF)-FL-NOT-PREG   PIC X(1).
             07 (SF)-DT-INI-FNT-REDD   PIC S9(8) COMP-3.
             07 (SF)-DT-INI-FNT-REDD-NULL REDEFINES
                (SF)-DT-INI-FNT-REDD   PIC X(5).
             07 (SF)-FNT-REDD-2 PIC X(2).
             07 (SF)-FNT-REDD-2-NULL REDEFINES
                (SF)-FNT-REDD-2   PIC X(2).
             07 (SF)-DT-INI-FNT-REDD-2   PIC S9(8) COMP-3.
             07 (SF)-DT-INI-FNT-REDD-2-NULL REDEFINES
                (SF)-DT-INI-FNT-REDD-2   PIC X(5).
             07 (SF)-FNT-REDD-3 PIC X(2).
             07 (SF)-FNT-REDD-3-NULL REDEFINES
                (SF)-FNT-REDD-3   PIC X(2).
             07 (SF)-DT-INI-FNT-REDD-3   PIC S9(8) COMP-3.
             07 (SF)-DT-INI-FNT-REDD-3-NULL REDEFINES
                (SF)-DT-INI-FNT-REDD-3   PIC X(5).
             07 (SF)-MOT-ASS-TIT-EFF PIC X(250).
             07 (SF)-FIN-COSTITUZIONE PIC X(250).
             07 (SF)-DESC-IMP-CAR-PUB PIC X(250).
             07 (SF)-DESC-SCO-FIN-RAPP PIC X(250).
             07 (SF)-DESC-PROC-PNL PIC X(250).
             07 (SF)-DESC-NOT-PREG PIC X(250).
             07 (SF)-ID-ASSICURATI PIC S9(9)     COMP-3.
             07 (SF)-ID-ASSICURATI-NULL REDEFINES
                (SF)-ID-ASSICURATI   PIC X(5).
             07 (SF)-REDD-CON PIC S9(12)V9(3) COMP-3.
             07 (SF)-REDD-CON-NULL REDEFINES
                (SF)-REDD-CON   PIC X(8).
             07 (SF)-DESC-LIB-MOT-RISC PIC X(250).
             07 (SF)-TP-MOT-ASS-TIT-EFF PIC X(2).
             07 (SF)-TP-MOT-ASS-TIT-EFF-NULL REDEFINES
                (SF)-TP-MOT-ASS-TIT-EFF   PIC X(2).
             07 (SF)-TP-RAG-RAPP PIC X(2).
             07 (SF)-TP-RAG-RAPP-NULL REDEFINES
                (SF)-TP-RAG-RAPP   PIC X(2).
             07 (SF)-COD-CAN PIC S9(5)     COMP-3.
             07 (SF)-COD-CAN-NULL REDEFINES
                (SF)-COD-CAN   PIC X(3).
             07 (SF)-TP-FIN-COST PIC X(2).
             07 (SF)-TP-FIN-COST-NULL REDEFINES
                (SF)-TP-FIN-COST   PIC X(2).
             07 (SF)-NAZ-DEST-FND PIC X(4).
             07 (SF)-NAZ-DEST-FND-NULL REDEFINES
                (SF)-NAZ-DEST-FND   PIC X(4).
             07 (SF)-FL-AU-FATCA-AEOI PIC X(1).
             07 (SF)-FL-AU-FATCA-AEOI-NULL REDEFINES
                (SF)-FL-AU-FATCA-AEOI   PIC X(1).
             07 (SF)-TP-CAR-FIN-GIUR PIC X(2).
             07 (SF)-TP-CAR-FIN-GIUR-NULL REDEFINES
                (SF)-TP-CAR-FIN-GIUR   PIC X(2).
             07 (SF)-TP-CAR-FIN-GIUR-AT PIC X(2).
             07 (SF)-TP-CAR-FIN-GIUR-AT-NULL REDEFINES
                (SF)-TP-CAR-FIN-GIUR-AT   PIC X(2).
             07 (SF)-TP-CAR-FIN-GIUR-PA PIC X(2).
             07 (SF)-TP-CAR-FIN-GIUR-PA-NULL REDEFINES
                (SF)-TP-CAR-FIN-GIUR-PA   PIC X(2).
             07 (SF)-FL-ISTITUZ-FIN PIC X(1).
             07 (SF)-FL-ISTITUZ-FIN-NULL REDEFINES
                (SF)-FL-ISTITUZ-FIN   PIC X(1).
             07 (SF)-TP-ORI-FND-TIT-EFF PIC X(2).
             07 (SF)-TP-ORI-FND-TIT-EFF-NULL REDEFINES
                (SF)-TP-ORI-FND-TIT-EFF   PIC X(2).
             07 (SF)-PC-ESP-AG-PA-MSC PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-ESP-AG-PA-MSC-NULL REDEFINES
                (SF)-PC-ESP-AG-PA-MSC   PIC X(4).
             07 (SF)-FL-PR-TR-USA PIC X(1).
             07 (SF)-FL-PR-TR-USA-NULL REDEFINES
                (SF)-FL-PR-TR-USA   PIC X(1).
             07 (SF)-FL-PR-TR-NO-USA PIC X(1).
             07 (SF)-FL-PR-TR-NO-USA-NULL REDEFINES
                (SF)-FL-PR-TR-NO-USA   PIC X(1).
             07 (SF)-PC-RIP-PAT-AS-VITA PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-RIP-PAT-AS-VITA-NULL REDEFINES
                (SF)-PC-RIP-PAT-AS-VITA   PIC X(4).
             07 (SF)-PC-RIP-PAT-IM PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-RIP-PAT-IM-NULL REDEFINES
                (SF)-PC-RIP-PAT-IM   PIC X(4).
             07 (SF)-PC-RIP-PAT-SET-IM PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-RIP-PAT-SET-IM-NULL REDEFINES
                (SF)-PC-RIP-PAT-SET-IM   PIC X(4).
             07 (SF)-TP-STATUS-AEOI PIC X(2).
             07 (SF)-TP-STATUS-AEOI-NULL REDEFINES
                (SF)-TP-STATUS-AEOI   PIC X(2).
             07 (SF)-TP-STATUS-FATCA PIC X(2).
             07 (SF)-TP-STATUS-FATCA-NULL REDEFINES
                (SF)-TP-STATUS-FATCA   PIC X(2).
             07 (SF)-FL-RAPP-PA-MSC PIC X(1).
             07 (SF)-FL-RAPP-PA-MSC-NULL REDEFINES
                (SF)-FL-RAPP-PA-MSC   PIC X(1).
             07 (SF)-COD-COMUN-SVOL-ATT PIC X(4).
             07 (SF)-COD-COMUN-SVOL-ATT-NULL REDEFINES
                (SF)-COD-COMUN-SVOL-ATT   PIC X(4).
             07 (SF)-TP-DT-1O-CON-CLI PIC X(2).
             07 (SF)-TP-DT-1O-CON-CLI-NULL REDEFINES
                (SF)-TP-DT-1O-CON-CLI   PIC X(2).
             07 (SF)-TP-MOD-EN-RELA-INT PIC X(2).
             07 (SF)-TP-MOD-EN-RELA-INT-NULL REDEFINES
                (SF)-TP-MOD-EN-RELA-INT   PIC X(2).
             07 (SF)-TP-REDD-ANNU-LRD PIC X(2).
             07 (SF)-TP-REDD-ANNU-LRD-NULL REDEFINES
                (SF)-TP-REDD-ANNU-LRD   PIC X(2).
             07 (SF)-TP-REDD-CON PIC X(2).
             07 (SF)-TP-REDD-CON-NULL REDEFINES
                (SF)-TP-REDD-CON   PIC X(2).
             07 (SF)-TP-OPER-SOC-FID PIC X(2).
             07 (SF)-TP-OPER-SOC-FID-NULL REDEFINES
                (SF)-TP-OPER-SOC-FID   PIC X(2).
             07 (SF)-COD-PA-ESP-MSC-1 PIC X(4).
             07 (SF)-COD-PA-ESP-MSC-1-NULL REDEFINES
                (SF)-COD-PA-ESP-MSC-1   PIC X(4).
             07 (SF)-IMP-PA-ESP-MSC-1 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-PA-ESP-MSC-1-NULL REDEFINES
                (SF)-IMP-PA-ESP-MSC-1   PIC X(8).
             07 (SF)-COD-PA-ESP-MSC-2 PIC X(4).
             07 (SF)-COD-PA-ESP-MSC-2-NULL REDEFINES
                (SF)-COD-PA-ESP-MSC-2   PIC X(4).
             07 (SF)-IMP-PA-ESP-MSC-2 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-PA-ESP-MSC-2-NULL REDEFINES
                (SF)-IMP-PA-ESP-MSC-2   PIC X(8).
             07 (SF)-COD-PA-ESP-MSC-3 PIC X(4).
             07 (SF)-COD-PA-ESP-MSC-3-NULL REDEFINES
                (SF)-COD-PA-ESP-MSC-3   PIC X(4).
             07 (SF)-IMP-PA-ESP-MSC-3 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-PA-ESP-MSC-3-NULL REDEFINES
                (SF)-IMP-PA-ESP-MSC-3   PIC X(8).
             07 (SF)-COD-PA-ESP-MSC-4 PIC X(4).
             07 (SF)-COD-PA-ESP-MSC-4-NULL REDEFINES
                (SF)-COD-PA-ESP-MSC-4   PIC X(4).
             07 (SF)-IMP-PA-ESP-MSC-4 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-PA-ESP-MSC-4-NULL REDEFINES
                (SF)-IMP-PA-ESP-MSC-4   PIC X(8).
             07 (SF)-COD-PA-ESP-MSC-5 PIC X(4).
             07 (SF)-COD-PA-ESP-MSC-5-NULL REDEFINES
                (SF)-COD-PA-ESP-MSC-5   PIC X(4).
             07 (SF)-IMP-PA-ESP-MSC-5 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-PA-ESP-MSC-5-NULL REDEFINES
                (SF)-IMP-PA-ESP-MSC-5   PIC X(8).
             07 (SF)-DESC-ORGN-FND PIC X(250).
             07 (SF)-COD-AUT-DUE-DIL PIC X(10).
             07 (SF)-COD-AUT-DUE-DIL-NULL REDEFINES
                (SF)-COD-AUT-DUE-DIL   PIC X(10).
             07 (SF)-FL-PR-QUEST-FATCA PIC X(1).
             07 (SF)-FL-PR-QUEST-FATCA-NULL REDEFINES
                (SF)-FL-PR-QUEST-FATCA   PIC X(1).
             07 (SF)-FL-PR-QUEST-AEOI PIC X(1).
             07 (SF)-FL-PR-QUEST-AEOI-NULL REDEFINES
                (SF)-FL-PR-QUEST-AEOI   PIC X(1).
             07 (SF)-FL-PR-QUEST-OFAC PIC X(1).
             07 (SF)-FL-PR-QUEST-OFAC-NULL REDEFINES
                (SF)-FL-PR-QUEST-OFAC   PIC X(1).
             07 (SF)-FL-PR-QUEST-KYC PIC X(1).
             07 (SF)-FL-PR-QUEST-KYC-NULL REDEFINES
                (SF)-FL-PR-QUEST-KYC   PIC X(1).
             07 (SF)-FL-PR-QUEST-MSCQ PIC X(1).
             07 (SF)-FL-PR-QUEST-MSCQ-NULL REDEFINES
                (SF)-FL-PR-QUEST-MSCQ   PIC X(1).
             07 (SF)-TP-NOT-PREG PIC X(2).
             07 (SF)-TP-NOT-PREG-NULL REDEFINES
                (SF)-TP-NOT-PREG   PIC X(2).
             07 (SF)-TP-PROC-PNL PIC X(2).
             07 (SF)-TP-PROC-PNL-NULL REDEFINES
                (SF)-TP-PROC-PNL   PIC X(2).
             07 (SF)-COD-IMP-CAR-PUB PIC X(2).
             07 (SF)-COD-IMP-CAR-PUB-NULL REDEFINES
                (SF)-COD-IMP-CAR-PUB   PIC X(2).
             07 (SF)-OPRZ-SOSPETTE PIC X(1).
             07 (SF)-OPRZ-SOSPETTE-NULL REDEFINES
                (SF)-OPRZ-SOSPETTE   PIC X(1).
             07 (SF)-ULT-FATT-ANNU PIC S9(12)V9(3) COMP-3.
             07 (SF)-ULT-FATT-ANNU-NULL REDEFINES
                (SF)-ULT-FATT-ANNU   PIC X(8).
             07 (SF)-DESC-PEP PIC X(100).
             07 (SF)-NUM-TEL-2 PIC X(20).
             07 (SF)-NUM-TEL-2-NULL REDEFINES
                (SF)-NUM-TEL-2   PIC X(20).
             07 (SF)-IMP-AFI PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-AFI-NULL REDEFINES
                (SF)-IMP-AFI   PIC X(8).
             07 (SF)-FL-NEW-PRO PIC X(1).
             07 (SF)-FL-NEW-PRO-NULL REDEFINES
                (SF)-FL-NEW-PRO   PIC X(1).
             07 (SF)-TP-MOT-CAMBIO-CNTR PIC X(2).
             07 (SF)-TP-MOT-CAMBIO-CNTR-NULL REDEFINES
                (SF)-TP-MOT-CAMBIO-CNTR   PIC X(2).
             07 (SF)-DESC-MOT-CAMBIO-CN PIC X(250).
             07 (SF)-COD-SOGG PIC X(20).
             07 (SF)-COD-SOGG-NULL REDEFINES
                (SF)-COD-SOGG   PIC X(20).

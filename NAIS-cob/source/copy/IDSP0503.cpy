
      **************************************************************
       S503-CALL-STRINGATURA.

           PERFORM S503-INITIALIZE-CAMPI THRU S503-INITIALIZE-CAMPI-EX

           PERFORM S503-ELABORA-DATI     THRU S503-ELABORA-DATI-EX.

       S503-CALL-STRINGATURA-EX.
           EXIT.

      **************************************************************
       S503-INITIALIZE-CAMPI.

           SET IDSV0503-SUCCESSFUL-RC TO TRUE

           MOVE SPACES                TO IDSV0503-DESCRIZ-ERR

           SET IDSV0503-SEGNO-POSTIV-NO        TO TRUE
           SET IDSV0503-SEGNO-ANTERIORE        TO TRUE

           MOVE 7                     TO IDSV0503-DECIMALI-ESPOSTI-IMP

           MOVE 3                     TO IDSV0503-DECIMALI-ESPOSTI-PERC
           MOVE 9                     TO IDSV0503-DECIMALI-ESPOSTI-TASS

           MOVE '!'                   TO IDSV0503-SEPARATORE-VALORE
           MOVE ':'                   TO IDSV0503-SEPARATORE-VARIABILE
           MOVE ','                   TO IDSV0503-SIMBOLO-DECIMALE

           SET IDSV0503-SENZA-SEGNO                 TO TRUE.
           SET IDSV0503-DATO-INPUT-TROVATO-NO       TO TRUE.

      *     INITIALIZE                         IDSV0503-OUTPUT
           INITIALIZE                         IDSV0503-STRINGA-TOT
                                              IDSV0503-CAMPO-ALFA
                                              IDSV0503-CAMPO-APPOGGIO
                                              IDSV0503-CAMPO-INTERI
                                              IDSV0503-CAMPO-DECIMALI.

           MOVE 1                          TO IDSV0503-IND-OUT
                                              IDSV0503-POSIZIONE
                                              IDSV0503-POSIZIONE-TOT.
      *                                        IDSV0503-MAX-TAB-STR.

           MOVE IDSV0503-LIMITE-STRINGA-OUTPUT
                                           TO IDSV0503-SPAZIO-RESTANTE

           MOVE 0                          TO IDSV0503-IND-VAR
                                              IDSV0503-IND-STRINGA
                                              IDSV0503-IND-INT
                                              IDSV0503-IND-DEC.

       S503-INITIALIZE-CAMPI-EX.
           EXIT.

      **************************************************************
       S503-ELABORA-DATI.

           PERFORM VARYING IDSV0503-IND-VAR FROM 1 BY 1
                    UNTIL IDSV0503-IND-VAR > IDSV0503-LIMITE-ARRAY-INPUT
                        OR IDSV0503-TP-DATO(IDSV0503-IND-VAR) =
                           SPACES OR LOW-VALUES OR HIGH-VALUES OR
                           NOT IDSV0503-SUCCESSFUL-RC

              SET IDSV0503-DATO-INPUT-TROVATO-SI      TO TRUE
              SET IDSV0503-SENZA-SEGNO                TO TRUE

              SET IDSV0503-FINE-STRINGA-NO            TO TRUE
              PERFORM VARYING IDSV0503-IND-STRINGA FROM 12 BY -1
                      UNTIL   IDSV0503-IND-STRINGA = 0 OR
                              IDSV0503-FINE-STRINGA-SI

                      IF IDSV0503-COD-VARIABILE(IDSV0503-IND-VAR)
                         (IDSV0503-IND-STRINGA:1)
                         NOT = (SPACES AND LOW-VALUE AND HIGH-VALUE)
                            SET IDSV0503-FINE-STRINGA-SI         TO TRUE
                      END-IF

              END-PERFORM

              ADD  1                      TO IDSV0503-IND-STRINGA
              MOVE IDSV0503-COD-VARIABILE(IDSV0503-IND-VAR)
                   (1:IDSV0503-IND-STRINGA)
                                           TO IDSV0503-CAMPO-APPOGGIO
                               (IDSV0503-POSIZIONE:IDSV0503-IND-STRINGA)
              ADD IDSV0503-IND-STRINGA     TO
                  IDSV0503-POSIZIONE IDSV0503-IND-ALFA

              MOVE IDSV0503-SEPARATORE-VALORE   TO
                                             IDSV0503-CAMPO-APPOGGIO
                                        (IDSV0503-POSIZIONE:1)
              ADD 1                       TO IDSV0503-POSIZIONE
                                             IDSV0503-IND-ALFA

              IF IDSV0503-TP-DATO(IDSV0503-IND-VAR) =
                                       ( IDSV0503-LISTA-IMPORTO     OR
                                         IDSV0503-LISTA-NUMERICO    OR
                                         IDSV0503-LISTA-MILLESIMI   OR
                                         IDSV0503-LISTA-PERCENTUALE OR
                                         IDSV0503-LISTA-DT          OR
                                         IDSV0503-LISTA-STRINGA     OR
                                         IDSV0503-LISTA-TASSO       OR
                                         IDSV0503-LISTA-MISTA )
                 MOVE  IDSV0503-STRINGA          TO IDSV0503-TP-DATO-APP
              ELSE
                 MOVE IDSV0503-TP-DATO(IDSV0503-IND-VAR)
                   TO IDSV0503-TP-DATO-APP
              END-IF

              MOVE IDSV0503-TP-DATO(IDSV0503-IND-VAR)
                                           TO IDSV0503-CAMPO-APPOGGIO
                                              (IDSV0503-POSIZIONE:1)
              ADD 1                        TO IDSV0503-POSIZIONE
                                              IDSV0503-IND-ALFA

              MOVE IDSV0503-SEPARATORE-VALORE TO IDSV0503-CAMPO-APPOGGIO
                                             (IDSV0503-POSIZIONE:1)

              ADD 1                        TO IDSV0503-POSIZIONE
                                              IDSV0503-IND-ALFA

              IF  IDSV0503-TP-DATO-APP = 'N'
                  PERFORM S503-TRATTA-NUMERICO
                     THRU S503-TRATTA-NUMERICO-EX
              ELSE
                  IF IDSV0503-TP-DATO-APP = 'I'
                     PERFORM S503-TRATTA-IMPORTO
                        THRU S503-TRATTA-IMPORTO-EX
                  ELSE
                     PERFORM S503-INIZ-CAMPO-APP-IMP
                        THRU S503-INIZ-CAMPO-APP-IMP-EX
                  END-IF
              END-IF

              IF  IDSV0503-TP-DATO-APP = 'A' OR 'P' OR 'M'
                  PERFORM S503-TRATTA-PERCENTUALE
                     THRU S503-TRATTA-PERCENTUALE-EX
              ELSE
                  PERFORM S503-INIZ-CAMPO-APP-PERC THRU
                          S503-INIZ-CAMPO-APP-PERC-EX
              END-IF

              IF  IDSV0503-TP-DATO-APP = ( 'D' OR 'S' ) AND
                 IDSV0503-VAL-STR (IDSV0503-IND-VAR)
                                  (1:IDSV0503-LUNG-CHAR-FINE-STR)
                                        NOT = IDSV0503-CHAR-FINE-STRINGA
                  PERFORM S503-TRATTA-STRINGA
                     THRU S503-TRATTA-STRINGA-EX
              ELSE
                  MOVE  SPACE                 TO IDSV0503-CAMPO-APPOGGIO
                                                 (IDSV0503-POSIZIONE:1)
                  ADD 1                       TO IDSV0503-POSIZIONE
                                                 IDSV0503-IND-ALFA
              END-IF

              MOVE IDSV0503-SEPARATORE-VALORE TO IDSV0503-CAMPO-APPOGGIO
                                                 (IDSV0503-POSIZIONE:1)
              ADD 1                           TO IDSV0503-POSIZIONE
                                                 IDSV0503-IND-ALFA

              MOVE IDSV0503-SEPARATORE-VARIABILE TO
                   IDSV0503-CAMPO-APPOGGIO(IDSV0503-POSIZIONE:1)
              ADD 1                           TO IDSV0503-POSIZIONE
                                                 IDSV0503-IND-ALFA

              PERFORM S503-CALCOLA-SPAZIO    THRU S503-CALCOLA-SPAZIO-EX

           END-PERFORM.

           IF IDSV0503-SUCCESSFUL-RC
              IF IDSV0503-DATO-INPUT-TROVATO-NO
                 SET IDSV0503-GENERIC-ERROR  TO TRUE
                 STRING 'NESSUN DATO DI INPUT TROVATO'
                        DELIMITED BY SIZE
                        INTO IDSV0503-DESCRIZ-ERR
                 END-STRING
              ELSE
                 SUBTRACT 1 FROM IDSV0503-POSIZIONE-TOT
                   GIVING IDSV0503-LEN-STRINGA-TOT
              END-IF
           END-IF.

       S503-ELABORA-DATI-EX.
           EXIT.

       S503-INIZ-CAMPO-APP-PERC.

           MOVE  '0'                   TO IDSV0503-CAMPO-APPOGGIO
                                          (IDSV0503-POSIZIONE:1)
           ADD 1                       TO IDSV0503-POSIZIONE
                                          IDSV0503-IND-ALFA
           MOVE IDSV0503-SIMBOLO-DECIMALE
                                       TO IDSV0503-CAMPO-APPOGGIO
                                          (IDSV0503-POSIZIONE:1)
           ADD 1                       TO IDSV0503-POSIZIONE
                                          IDSV0503-IND-ALFA
           MOVE  '0'                   TO IDSV0503-CAMPO-APPOGGIO
                                          (IDSV0503-POSIZIONE:1)
           ADD 1                       TO IDSV0503-POSIZIONE
                                          IDSV0503-IND-ALFA
           MOVE IDSV0503-SEPARATORE-VALORE
                                       TO IDSV0503-CAMPO-APPOGGIO
                                          (IDSV0503-POSIZIONE:1)
           ADD 1                       TO IDSV0503-POSIZIONE
                                          IDSV0503-IND-ALFA.

       S503-INIZ-CAMPO-APP-PERC-EX.
           EXIT.

       S503-INIZ-CAMPO-APP-IMP.

           IF IDSV0503-SEGNO-ANTERIORE
              PERFORM S503-TRATTA-SEGNO     THRU S503-TRATTA-SEGNO-EX
           END-IF
           MOVE  '0'                   TO IDSV0503-CAMPO-APPOGGIO
                                          (IDSV0503-POSIZIONE:1)
           ADD 1                       TO IDSV0503-POSIZIONE
                                          IDSV0503-IND-ALFA
           MOVE IDSV0503-SIMBOLO-DECIMALE
                                       TO  IDSV0503-CAMPO-APPOGGIO
                                          (IDSV0503-POSIZIONE:1)
           ADD 1                       TO IDSV0503-POSIZIONE
                                          IDSV0503-IND-ALFA
           MOVE  '0'                   TO IDSV0503-CAMPO-APPOGGIO
                                          (IDSV0503-POSIZIONE:1)
           ADD 1                       TO IDSV0503-POSIZIONE
                                          IDSV0503-IND-ALFA
           IF IDSV0503-SEGNO-POSTERIORE
              PERFORM S503-TRATTA-SEGNO     THRU S503-TRATTA-SEGNO-EX
           END-IF
           MOVE IDSV0503-SEPARATORE-VALORE  TO IDSV0503-CAMPO-APPOGGIO
                                          (IDSV0503-POSIZIONE:1)
           ADD 1                       TO IDSV0503-POSIZIONE
                                          IDSV0503-IND-ALFA.

       S503-INIZ-CAMPO-APP-IMP-EX.
           EXIT.
      **************************************************************
       S503-TRATTA-IMPORTO.

           MOVE IDSV0503-LIM-INT-IMP     TO IDSV0503-LIMITE-INTERI.
           MOVE IDSV0503-LIM-DEC-IMP     TO IDSV0503-LIMITE-DECIMALI.

           IF IDSV0503-VAL-IMP (IDSV0503-IND-VAR) >= 0
              SET IDSV0503-SEGNO-POSITIVO            TO TRUE
           END-IF.

           IF IDSV0503-VAL-IMP (IDSV0503-IND-VAR) <  0
              SET IDSV0503-SEGNO-NEGATIVO            TO TRUE
           END-IF.

           INITIALIZE                       IDSV0503-STRUCT-IMP.
           MOVE IDSV0503-VAL-IMP (IDSV0503-IND-VAR)  TO
                IDSV0503-STRUCT-IMP.

           PERFORM S503-ESTRAI-INTERO      THRU S503-ESTRAI-INTERO-EX.

           PERFORM S503-ESTRAI-DECIMALI    THRU S503-ESTRAI-DECIMALI-EX.

           PERFORM S503-INDIVIDUA-IND-DEC
              THRU S503-INDIVIDUA-IND-DEC-EX.

           PERFORM S503-STRINGA-CAMPO       THRU S503-STRINGA-CAMPO-EX.

           IF IDSV0503-SEGNO-POSTERIORE
              PERFORM S503-TRATTA-SEGNO     THRU S503-TRATTA-SEGNO-EX
           END-IF.

           MOVE IDSV0503-SEPARATORE-VALORE  TO IDSV0503-CAMPO-APPOGGIO
                                               (IDSV0503-POSIZIONE:1)
           ADD 1                            TO IDSV0503-POSIZIONE
                                               IDSV0503-IND-ALFA.

       S503-TRATTA-IMPORTO-EX.
           EXIT.

      **************************************************************
       S503-TRATTA-NUMERICO.

           MOVE IDSV0503-LIMITE-INT-DEC      TO IDSV0503-LIMITE-INTERI.

            IF IDSV0503-VAL-IMP (IDSV0503-IND-VAR) >= ZEROES
              SET IDSV0503-SEGNO-POSITIVO    TO TRUE
           END-IF.

            IF IDSV0503-VAL-IMP (IDSV0503-IND-VAR) < ZEROES
              SET IDSV0503-SEGNO-NEGATIVO            TO TRUE
           END-IF.

           INITIALIZE                          IDSV0503-STRUCT-NUM.
           MOVE IDSV0503-VAL-IMP (IDSV0503-IND-VAR) TO
                IDSV0503-STRUCT-NUM.

           PERFORM S503-ESTRAI-INTERO       THRU S503-ESTRAI-INTERO-EX.

           PERFORM S503-STRINGA-CAMPO       THRU S503-STRINGA-CAMPO-EX.

           IF IDSV0503-SEGNO-POSTERIORE
              PERFORM S503-TRATTA-SEGNO     THRU S503-TRATTA-SEGNO-EX
           END-IF.

           MOVE IDSV0503-SEPARATORE-VALORE  TO IDSV0503-CAMPO-APPOGGIO
                                          (IDSV0503-POSIZIONE:1)
           ADD 1                       TO IDSV0503-POSIZIONE
                                          IDSV0503-IND-ALFA.

       S503-TRATTA-NUMERICO-EX.
           EXIT.

      **************************************************************
       S503-TRATTA-PERCENTUALE.

           MOVE IDSV0503-LIM-INT-PERC    TO IDSV0503-LIMITE-INTERI.
           MOVE IDSV0503-LIM-DEC-PERC    TO IDSV0503-LIMITE-DECIMALI.

           INITIALIZE                          IDSV0503-STRUCT-IMP.
           MOVE IDSV0503-VAL-PERC (IDSV0503-IND-VAR) TO
                IDSV0503-STRUCT-PERC.

           PERFORM S503-ESTRAI-INTERO       THRU S503-ESTRAI-INTERO-EX.

           PERFORM S503-ESTRAI-DECIMALI    THRU S503-ESTRAI-DECIMALI-EX.

           PERFORM S503-INDIVIDUA-IND-DEC
              THRU S503-INDIVIDUA-IND-DEC-EX.

           PERFORM S503-STRINGA-CAMPO      THRU S503-STRINGA-CAMPO-EX.

           MOVE IDSV0503-SEPARATORE-VALORE  TO IDSV0503-CAMPO-APPOGGIO
                                               (IDSV0503-POSIZIONE:1)
           ADD 1                            TO IDSV0503-POSIZIONE
                                               IDSV0503-IND-ALFA.

       S503-TRATTA-PERCENTUALE-EX.
           EXIT.

      **************************************************************
       S503-TRATTA-STRINGA.

           INITIALIZE                          IDSV0503-CAMPO-ALFA
           MOVE IDSV0503-LIMITE-STRINGA     TO IDSV0503-POSIZ-MEDIA
                                               IDSV0503-INTERVALLO.
           MOVE IDSV0503-VAL-STR (IDSV0503-IND-VAR) TO
                IDSV0503-CAMPO-ALFA.

           SET IDSV0503-ALFA-TROVATO-NO      TO TRUE
      *
      *    ASTERISCARE SE SI UTILIZZA LA RICERCA DICOTOMICA
      *
           PERFORM VARYING IDSV0503-IND-STRINGA
                   FROM IDSV0503-LIMITE-STRINGA BY -1
                   UNTIL   IDSV0503-IND-STRINGA = 0 OR
                           IDSV0503-ALFA-TROVATO-SI

                   IF IDSV0503-CAMPO-ALFA(IDSV0503-IND-STRINGA:1)
                      NOT = (SPACES AND LOW-VALUE AND HIGH-VALUE)
                      SET IDSV0503-ALFA-TROVATO-SI    TO TRUE
                      ADD  1                     TO IDSV0503-IND-STRINGA
                   END-IF

           END-PERFORM.

      *
      *    DISASTERISCARE SE SI UTILIZZA LA RICERCA DICOTOMICA
      *
      *    COMPUTE POSIZ-FINALE = IDSV0503-LIMITE-STRINGA -
      *                            IDSV0503-LUNG-CHAR-FINE-STR + 1
      *    IF IDSV0503-VAL-STR (IDSV0503-IND-VAR)
      *       (IDSV0503-POSIZ-FINALE:IDSV0503-LUNG-CHAR-FINE-STR)
      *         NOT = IDSV0503-CHAR-FINE-STRINGA AND SPACES
      *       SET IDSV0503-ALFA-TROVATO-SI    TO TRUE
      *    ELSE
      *        PERFORM S503-ESTRAI-ALFANUMERICO
      *           THRU S503-ESTRAI-ALFANUMERICO-EX
      *               UNTIL IDSV0503-ALFA-TROVATO-SI
      *    END-IF.

           PERFORM S503-STRINGA-CAMPO        THRU S503-STRINGA-CAMPO-EX.

       S503-TRATTA-STRINGA-EX.
           EXIT.

      **************************************************************
      *S503-ESTRAI-ALFANUMERICO.
      *
      *      IF IDSV0503-CAMPO-ALFA
      *         (IDSV0503-POSIZ-MEDIA:IDSV0503-LUNG-CHAR-FINE-STR) =
      *                     IDSV0503-CHAR-FINE-STRINGA
      *         DIVIDE    IDSV0503-INTERVALLO
      *         BY        2
      *         GIVING    IDSV0503-PARTE-INTERA
      *         REMAINDER IDSV0503-PARTE-DECIMALE
      *         IF IDSV0503-PARTE-DECIMALE > ZEROES
      *            ADD 1               TO IDSV0503-PARTE-INTERA
      *         END-IF
      *         MOVE IDSV0503-PARTE-INTERA      TO IDSV0503-INTERVALLO
      *         SUBTRACT IDSV0503-INTERVALLO   FROM IDSV0503-POSIZ-MEDIA
      *      ELSE
      *         MOVE  IDSV0503-POSIZ-MEDIA      TO IDSV0503-POSIZ-SUCC
      *         ADD   1                TO IDSV0503-POSIZ-SUCC
      *         IF IDSV0503-CAMPO-ALFA
      *          (IDSV0503-POSIZ-SUCC:IDSV0503-LUNG-CHAR-FINE-STR) =
      *                               IDSV0503-CHAR-FINE-STRINGA
      *            SET IDSV0503-ALFA-TROVATO-SI     TO TRUE
      *         ELSE
      *            DIVIDE    IDSV0503-INTERVALLO
      *            BY        2
      *            GIVING    IDSV0503-PARTE-INTERA
      *            REMAINDER IDSV0503-PARTE-DECIMALE
      *            IF IDSV0503-PARTE-DECIMALE > ZEROES
      *               ADD 1               TO IDSV0503-PARTE-INTERA
      *            END-IF
      *            MOVE IDSV0503-PARTE-INTERA   TO IDSV0503-INTERVALLO
      *            ADD  IDSV0503-PARTE-INTERA   TO IDSV0503-POSIZ-MEDIA
      *         END-IF
      *      END-IF.
      *
      *S503-ESTRAI-ALFANUMERICO-EX.
      *    EXIT.

      **************************************************************
       S503-ESTRAI-INTERO.

           SET IDSV0503-INT-TROVATO-NO          TO TRUE.

           INITIALIZE                      IDSV0503-CAMPO-INTERI
                                           IDSV0503-IND-INT.

           PERFORM VARYING IDSV0503-IND-STRINGA FROM 1 BY 1
                   UNTIL  IDSV0503-IND-STRINGA > IDSV0503-LIMITE-INT-DEC
                       OR IDSV0503-IND-STRINGA > IDSV0503-LIMITE-INTERI

                      IF IDSV0503-ELE-NUM-IMP(IDSV0503-IND-STRINGA)
                                                NOT = ZEROES
                         SET IDSV0503-INT-TROVATO-SI     TO TRUE
                      END-IF

                      IF IDSV0503-INT-TROVATO-SI
                         ADD 1                  TO IDSV0503-IND-INT
                        MOVE IDSV0503-ELE-NUM-IMP(IDSV0503-IND-STRINGA)
                        TO IDSV0503-ELE-STRINGA-INTERI(IDSV0503-IND-INT)
                      END-IF

            END-PERFORM.

            IF IDSV0503-INT-TROVATO-NO
               MOVE 1                 TO  IDSV0503-IND-INT
            END-IF.

       S503-ESTRAI-INTERO-EX.
           EXIT.

      **************************************************************
       S503-ESTRAI-DECIMALI.

           INITIALIZE                      IDSV0503-CAMPO-DECIMALI
                                           IDSV0503-IND-DEC.

           PERFORM VARYING IDSV0503-IND-STRINGA
                   FROM IDSV0503-LIMITE-DECIMALI BY 1
                   UNTIL IDSV0503-IND-STRINGA > IDSV0503-LIMITE-INT-DEC

                      ADD 1                  TO IDSV0503-IND-DEC
                      MOVE IDSV0503-ELE-NUM-IMP(IDSV0503-IND-STRINGA)
                      TO IDSV0503-ELE-STRINGA-DECIMALI(IDSV0503-IND-DEC)
           END-PERFORM.

       S503-ESTRAI-DECIMALI-EX.
           EXIT.
      **************************************************************
       S503-TRATTA-SEGNO.

           IF IDSV0503-SEGNO-NEGATIVO
              MOVE '-'           TO IDSV0503-CAMPO-APPOGGIO
                                     (IDSV0503-POSIZIONE:1)
              ADD 1              TO IDSV0503-POSIZIONE IDSV0503-IND-ALFA
           ELSE
              IF IDSV0503-SEGNO-POSTIV-SI
                 MOVE '+'        TO IDSV0503-CAMPO-APPOGGIO
                                     (IDSV0503-POSIZIONE:1)
                 ADD 1           TO IDSV0503-POSIZIONE IDSV0503-IND-ALFA
              END-IF
           END-IF.

       S503-TRATTA-SEGNO-EX.
           EXIT.

      **************************************************************
       S503-STRINGA-CAMPO.

           IF IDSV0503-SUCCESSFUL-RC
              EVALUATE IDSV0503-TP-DATO-APP
                 WHEN IDSV0503-STRINGA
                 WHEN IDSV0503-DT
                     IF IDSV0503-ALFA-TROVATO-SI
                        MOVE IDSV0503-CAMPO-ALFA(1:IDSV0503-IND-STRINGA)
                                         TO IDSV0503-CAMPO-APPOGGIO
                               (IDSV0503-POSIZIONE:IDSV0503-IND-STRINGA)
                        ADD IDSV0503-IND-STRINGA  TO IDSV0503-POSIZIONE
                                                     IDSV0503-IND-ALFA
      *                 MOVE IDSV0503-CAMPO-ALFA(1:IDSV0503-POSIZ-MEDIA)
      *                                  TO IDSV0503-CAMPO-APPOGGIO
      *                        (IDSV0503-POSIZIONE:IDSV0503-POSIZ-MEDIA)
      *                 ADD IDSV0503-POSIZ-MEDIA   TO IDSV0503-POSIZIONE
      *                                                IDSV0503-IND-ALFA
                     ELSE
                        MOVE SPACE      TO IDSV0503-CAMPO-APPOGGIO
                                            (IDSV0503-POSIZIONE:1)
                        ADD 1           TO IDSV0503-POSIZIONE
      *                  ADD 1           TO IDSV0503-POSIZIONE
      *                                     IDSV0503-IND-ALFA
                     END-IF

                 WHEN IDSV0503-IMPORTO
                 WHEN IDSV0503-PERCENTUALE
                 WHEN IDSV0503-TASSO
                 WHEN IDSV0503-MILLESIMI

                     IF IDSV0503-SEGNO-ANTERIORE
                        PERFORM S503-TRATTA-SEGNO
                           THRU S503-TRATTA-SEGNO-EX
                     END-IF

                     IF IDSV0503-INT-TROVATO-SI
                        MOVE IDSV0503-CAMPO-INTERI(1:IDSV0503-IND-INT)
                                        TO IDSV0503-CAMPO-APPOGGIO
                                   (IDSV0503-POSIZIONE:IDSV0503-IND-INT)
                        ADD IDSV0503-IND-INT     TO IDSV0503-POSIZIONE
                                                    IDSV0503-IND-ALFA
                     ELSE
                        MOVE  0         TO  IDSV0503-CAMPO-APPOGGIO
                                            (IDSV0503-POSIZIONE:1)
                        ADD 1           TO IDSV0503-POSIZIONE
                                           IDSV0503-IND-ALFA
                     END-IF

                     MOVE IDSV0503-SIMBOLO-DECIMALE
                                        TO  IDSV0503-CAMPO-APPOGGIO
                                            (IDSV0503-POSIZIONE:1)
                     ADD 1              TO IDSV0503-POSIZIONE
                                           IDSV0503-IND-ALFA

                     IF IDSV0503-DEC-TROVATO-SI
                        MOVE IDSV0503-CAMPO-DECIMALI(1:IDSV0503-IND-DEC)
                                        TO  IDSV0503-CAMPO-APPOGGIO
                                   (IDSV0503-POSIZIONE:IDSV0503-IND-DEC)
                        ADD IDSV0503-IND-DEC     TO IDSV0503-POSIZIONE
                     ELSE
                        MOVE 0          TO  IDSV0503-CAMPO-APPOGGIO
                                            (IDSV0503-POSIZIONE:1)
                        ADD 1           TO IDSV0503-POSIZIONE
                     END-IF

                 WHEN IDSV0503-NUMERICO

                     IF IDSV0503-SEGNO-ANTERIORE
                        PERFORM S503-TRATTA-SEGNO
                           THRU S503-TRATTA-SEGNO-EX
                     END-IF

                     IF IDSV0503-INT-TROVATO-SI
                        MOVE IDSV0503-CAMPO-INTERI(1:IDSV0503-IND-INT)
                                        TO  IDSV0503-CAMPO-APPOGGIO
                                   (IDSV0503-POSIZIONE:IDSV0503-IND-INT)
                        ADD IDSV0503-IND-INT     TO IDSV0503-POSIZIONE
                                                    IDSV0503-IND-ALFA
                     ELSE
                        MOVE 0          TO  IDSV0503-CAMPO-APPOGGIO
                                            (IDSV0503-POSIZIONE:1)
                        ADD 1           TO IDSV0503-POSIZIONE
                                           IDSV0503-IND-ALFA
                     END-IF

              END-EVALUATE
           END-IF.

       S503-STRINGA-CAMPO-EX.
           EXIT.

      **************************************************************
       S503-CALCOLA-SPAZIO.

           MOVE IDSV0503-IND-ALFA      TO  IDSV0503-SPAZIO-RICHIESTO.

           IF IDSV0503-SPAZIO-RICHIESTO > IDSV0503-SPAZIO-RESTANTE
                SET IDSV0503-GENERIC-ERROR  TO TRUE
                STRING 'SUPERATO LIMITE STRUTTURE OUTPUT'
                       DELIMITED BY SIZE
                       INTO IDSV0503-DESCRIZ-ERR
                END-STRING
           ELSE
              COMPUTE IDSV0503-SPAZIO-RESTANTE =
                      IDSV0503-SPAZIO-RESTANTE -
                      IDSV0503-SPAZIO-RICHIESTO
              MOVE IDSV0503-CAMPO-APPOGGIO(1:IDSV0503-IND-ALFA)
                TO IDSV0503-STRINGA-TOT
                   (IDSV0503-POSIZIONE-TOT:IDSV0503-IND-ALFA)
      *     (IDSV0503-IND-OUT)(IDSV0503-POSIZIONE-TOT:IDSV0503-IND-ALFA)
           END-IF.

           ADD  IDSV0503-IND-ALFA      TO IDSV0503-POSIZIONE-TOT.
           MOVE  ZERO                  TO IDSV0503-IND-ALFA.
           MOVE  1                     TO IDSV0503-POSIZIONE.
           INITIALIZE                     IDSV0503-CAMPO-APPOGGIO.

       S503-CALCOLA-SPAZIO-EX.
           EXIT.

      **************************************************************
       S503-INDIVIDUA-IND-DEC.

           SET IDSV0503-DEC-TROVATO-NO           TO TRUE.
           MOVE 1                                TO IDSV0503-IND-DEC.

           EVALUATE IDSV0503-TP-DATO-APP
               WHEN IDSV0503-IMPORTO
                    PERFORM VARYING IDSV0503-IND-RICERCA
                        FROM IDSV0503-DECIMALI-ESPOSTI-IMP
                        BY -1
                        UNTIL IDSV0503-IND-RICERCA <= ZEROES OR
                              IDSV0503-DEC-TROVATO-SI

                        IF  IDSV0503-ELE-STRINGA-DECIMALI
                                       (IDSV0503-IND-RICERCA) IS NUMERIC
                        AND IDSV0503-ELE-STRINGA-DECIMALI
                                     (IDSV0503-IND-RICERCA) NOT = ZEROES
                            SET IDSV0503-DEC-TROVATO-SI TO TRUE
                            MOVE IDSV0503-IND-RICERCA
                              TO IDSV0503-IND-DEC
                        END-IF

                    END-PERFORM

               WHEN IDSV0503-PERCENTUALE
               WHEN IDSV0503-MILLESIMI

                    PERFORM VARYING IDSV0503-IND-RICERCA
                        FROM IDSV0503-DECIMALI-ESPOSTI-PERC
                        BY -1
                        UNTIL IDSV0503-IND-RICERCA <= ZEROES OR
                              IDSV0503-DEC-TROVATO-SI

                        IF  IDSV0503-ELE-STRINGA-DECIMALI
                                      (IDSV0503-IND-RICERCA) IS NUMERIC
                        AND IDSV0503-ELE-STRINGA-DECIMALI
                                    (IDSV0503-IND-RICERCA) NOT = ZEROES
                            SET IDSV0503-DEC-TROVATO-SI TO TRUE
                           MOVE IDSV0503-IND-RICERCA TO IDSV0503-IND-DEC
                        END-IF

                    END-PERFORM

               WHEN IDSV0503-TASSO

                    PERFORM VARYING IDSV0503-IND-RICERCA
                        FROM IDSV0503-DECIMALI-ESPOSTI-TASS
                        BY -1
                        UNTIL IDSV0503-IND-RICERCA <= ZEROES OR
                              IDSV0503-DEC-TROVATO-SI

                        IF  IDSV0503-ELE-STRINGA-DECIMALI
                                     (IDSV0503-IND-RICERCA) IS NUMERIC
                        AND IDSV0503-ELE-STRINGA-DECIMALI
                                     (IDSV0503-IND-RICERCA) NOT = ZEROES
                            SET IDSV0503-DEC-TROVATO-SI TO TRUE
                            MOVE IDSV0503-IND-RICERCA
                              TO IDSV0503-IND-DEC
                        END-IF

                    END-PERFORM

           END-EVALUATE.

           ADD IDSV0503-IND-DEC                TO IDSV0503-IND-ALFA.

       S503-INDIVIDUA-IND-DEC-EX.
           EXIT.



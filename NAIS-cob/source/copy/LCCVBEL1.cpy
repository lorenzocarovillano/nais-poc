      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA BNFICR_LIQ
      *   ALIAS BEL
      *   ULTIMO AGG. 02 LUG 2014
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-BNFICR-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-ID-RAPP-ANA PIC S9(9)     COMP-3.
             07 (SF)-ID-RAPP-ANA-NULL REDEFINES
                (SF)-ID-RAPP-ANA   PIC X(5).
             07 (SF)-COD-BNFICR PIC X(20).
             07 (SF)-COD-BNFICR-NULL REDEFINES
                (SF)-COD-BNFICR   PIC X(20).
             07 (SF)-DESC-BNFICR PIC X(100).
             07 (SF)-PC-LIQ PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-LIQ-NULL REDEFINES
                (SF)-PC-LIQ   PIC X(4).
             07 (SF)-ESRCN-ATTVT-IMPRS PIC X(1).
             07 (SF)-ESRCN-ATTVT-IMPRS-NULL REDEFINES
                (SF)-ESRCN-ATTVT-IMPRS   PIC X(1).
             07 (SF)-TP-IND-BNFICR PIC X(2).
             07 (SF)-TP-IND-BNFICR-NULL REDEFINES
                (SF)-TP-IND-BNFICR   PIC X(2).
             07 (SF)-FL-ESE PIC X(1).
             07 (SF)-FL-ESE-NULL REDEFINES
                (SF)-FL-ESE   PIC X(1).
             07 (SF)-FL-IRREV PIC X(1).
             07 (SF)-FL-IRREV-NULL REDEFINES
                (SF)-FL-IRREV   PIC X(1).
             07 (SF)-IMP-LRD-LIQTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-LRD-LIQTO-NULL REDEFINES
                (SF)-IMP-LRD-LIQTO   PIC X(8).
             07 (SF)-IMPST-IPT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-IPT-NULL REDEFINES
                (SF)-IMPST-IPT   PIC X(8).
             07 (SF)-IMP-NET-LIQTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-NET-LIQTO-NULL REDEFINES
                (SF)-IMP-NET-LIQTO   PIC X(8).
             07 (SF)-RIT-ACC-IPT PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIT-ACC-IPT-NULL REDEFINES
                (SF)-RIT-ACC-IPT   PIC X(8).
             07 (SF)-RIT-VIS-IPT PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIT-VIS-IPT-NULL REDEFINES
                (SF)-RIT-VIS-IPT   PIC X(8).
             07 (SF)-RIT-TFR-IPT PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIT-TFR-IPT-NULL REDEFINES
                (SF)-RIT-TFR-IPT   PIC X(8).
             07 (SF)-IMPST-IRPEF-IPT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-IRPEF-IPT-NULL REDEFINES
                (SF)-IMPST-IRPEF-IPT   PIC X(8).
             07 (SF)-IMPST-SOST-IPT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-SOST-IPT-NULL REDEFINES
                (SF)-IMPST-SOST-IPT   PIC X(8).
             07 (SF)-IMPST-PRVR-IPT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-PRVR-IPT-NULL REDEFINES
                (SF)-IMPST-PRVR-IPT   PIC X(8).
             07 (SF)-IMPST-252-IPT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-252-IPT-NULL REDEFINES
                (SF)-IMPST-252-IPT   PIC X(8).
             07 (SF)-ID-ASSTO PIC S9(9)     COMP-3.
             07 (SF)-ID-ASSTO-NULL REDEFINES
                (SF)-ID-ASSTO   PIC X(5).
             07 (SF)-TAX-SEP PIC S9(12)V9(3) COMP-3.
             07 (SF)-TAX-SEP-NULL REDEFINES
                (SF)-TAX-SEP   PIC X(8).
             07 (SF)-DT-RISERVE-SOM-P   PIC S9(8) COMP-3.
             07 (SF)-DT-RISERVE-SOM-P-NULL REDEFINES
                (SF)-DT-RISERVE-SOM-P   PIC X(5).
             07 (SF)-DT-VLT   PIC S9(8) COMP-3.
             07 (SF)-DT-VLT-NULL REDEFINES
                (SF)-DT-VLT   PIC X(5).
             07 (SF)-TP-STAT-LIQ-BNFICR PIC X(2).
             07 (SF)-TP-STAT-LIQ-BNFICR-NULL REDEFINES
                (SF)-TP-STAT-LIQ-BNFICR   PIC X(2).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-RICH-CALC-CNBT-INP PIC X(1).
             07 (SF)-RICH-CALC-CNBT-INP-NULL REDEFINES
                (SF)-RICH-CALC-CNBT-INP   PIC X(1).
             07 (SF)-IMP-INTR-RIT-PAG PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-INTR-RIT-PAG-NULL REDEFINES
                (SF)-IMP-INTR-RIT-PAG   PIC X(8).
             07 (SF)-DT-ULT-DOCTO   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-DOCTO-NULL REDEFINES
                (SF)-DT-ULT-DOCTO   PIC X(5).
             07 (SF)-DT-DORMIENZA   PIC S9(8) COMP-3.
             07 (SF)-DT-DORMIENZA-NULL REDEFINES
                (SF)-DT-DORMIENZA   PIC X(5).
             07 (SF)-IMPST-BOLLO-TOT-V PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-BOLLO-TOT-V-NULL REDEFINES
                (SF)-IMPST-BOLLO-TOT-V   PIC X(8).
             07 (SF)-IMPST-VIS-1382011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-1382011-NULL REDEFINES
                (SF)-IMPST-VIS-1382011   PIC X(8).
             07 (SF)-IMPST-SOST-1382011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-SOST-1382011-NULL REDEFINES
                (SF)-IMPST-SOST-1382011   PIC X(8).
             07 (SF)-IMPST-VIS-662014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-662014-NULL REDEFINES
                (SF)-IMPST-VIS-662014   PIC X(8).
             07 (SF)-IMPST-SOST-662014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-SOST-662014-NULL REDEFINES
                (SF)-IMPST-SOST-662014   PIC X(8).

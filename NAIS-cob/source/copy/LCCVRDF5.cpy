
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVRDF5
      *   ULTIMO AGG. 25 NOV 2019
      *------------------------------------------------------------

       VAL-DCLGEN-RDF.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RDF)
              TO RDF-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-RDF)
              TO RDF-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-RDF) NOT NUMERIC
              MOVE 0 TO RDF-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-RDF)
              TO RDF-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-RDF) NOT NUMERIC
              MOVE 0 TO RDF-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-RDF)
              TO RDF-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-RDF)
              TO RDF-COD-COMP-ANIA
           IF (SF)-COD-FND-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-COD-FND-NULL(IX-TAB-RDF)
              TO RDF-COD-FND-NULL
           ELSE
              MOVE (SF)-COD-FND(IX-TAB-RDF)
              TO RDF-COD-FND
           END-IF
           IF (SF)-NUM-QUO-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-NUM-QUO-NULL(IX-TAB-RDF)
              TO RDF-NUM-QUO-NULL
           ELSE
              MOVE (SF)-NUM-QUO(IX-TAB-RDF)
              TO RDF-NUM-QUO
           END-IF
           IF (SF)-PC-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-PC-NULL(IX-TAB-RDF)
              TO RDF-PC-NULL
           ELSE
              MOVE (SF)-PC(IX-TAB-RDF)
              TO RDF-PC
           END-IF
           IF (SF)-IMP-MOVTO-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-IMP-MOVTO-NULL(IX-TAB-RDF)
              TO RDF-IMP-MOVTO-NULL
           ELSE
              MOVE (SF)-IMP-MOVTO(IX-TAB-RDF)
              TO RDF-IMP-MOVTO
           END-IF
           IF (SF)-DT-DIS-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-DT-DIS-NULL(IX-TAB-RDF)
              TO RDF-DT-DIS-NULL
           ELSE
             IF (SF)-DT-DIS(IX-TAB-RDF) = ZERO
                MOVE HIGH-VALUES
                TO RDF-DT-DIS-NULL
             ELSE
              MOVE (SF)-DT-DIS(IX-TAB-RDF)
              TO RDF-DT-DIS
             END-IF
           END-IF
           IF (SF)-COD-TARI-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-COD-TARI-NULL(IX-TAB-RDF)
              TO RDF-COD-TARI-NULL
           ELSE
              MOVE (SF)-COD-TARI(IX-TAB-RDF)
              TO RDF-COD-TARI
           END-IF
           IF (SF)-TP-STAT-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-TP-STAT-NULL(IX-TAB-RDF)
              TO RDF-TP-STAT-NULL
           ELSE
              MOVE (SF)-TP-STAT(IX-TAB-RDF)
              TO RDF-TP-STAT
           END-IF
           IF (SF)-TP-MOD-DIS-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-TP-MOD-DIS-NULL(IX-TAB-RDF)
              TO RDF-TP-MOD-DIS-NULL
           ELSE
              MOVE (SF)-TP-MOD-DIS(IX-TAB-RDF)
              TO RDF-TP-MOD-DIS
           END-IF
           MOVE (SF)-COD-DIV(IX-TAB-RDF)
              TO RDF-COD-DIV
           IF (SF)-DT-CAMBIO-VLT-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-DT-CAMBIO-VLT-NULL(IX-TAB-RDF)
              TO RDF-DT-CAMBIO-VLT-NULL
           ELSE
             IF (SF)-DT-CAMBIO-VLT(IX-TAB-RDF) = ZERO
                MOVE HIGH-VALUES
                TO RDF-DT-CAMBIO-VLT-NULL
             ELSE
              MOVE (SF)-DT-CAMBIO-VLT(IX-TAB-RDF)
              TO RDF-DT-CAMBIO-VLT
             END-IF
           END-IF
           MOVE (SF)-TP-FND(IX-TAB-RDF)
              TO RDF-TP-FND
           IF (SF)-DS-RIGA(IX-TAB-RDF) NOT NUMERIC
              MOVE 0 TO RDF-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-RDF)
              TO RDF-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-RDF)
              TO RDF-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-RDF) NOT NUMERIC
              MOVE 0 TO RDF-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-RDF)
              TO RDF-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-RDF) NOT NUMERIC
              MOVE 0 TO RDF-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-RDF)
              TO RDF-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-RDF) NOT NUMERIC
              MOVE 0 TO RDF-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-RDF)
              TO RDF-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-RDF)
              TO RDF-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-RDF)
              TO RDF-DS-STATO-ELAB
           IF (SF)-DT-DIS-CALC-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-DT-DIS-CALC-NULL(IX-TAB-RDF)
              TO RDF-DT-DIS-CALC-NULL
           ELSE
             IF (SF)-DT-DIS-CALC(IX-TAB-RDF) = ZERO
                MOVE HIGH-VALUES
                TO RDF-DT-DIS-CALC-NULL
             ELSE
              MOVE (SF)-DT-DIS-CALC(IX-TAB-RDF)
              TO RDF-DT-DIS-CALC
             END-IF
           END-IF
           IF (SF)-FL-CALC-DIS-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-FL-CALC-DIS-NULL(IX-TAB-RDF)
              TO RDF-FL-CALC-DIS-NULL
           ELSE
              MOVE (SF)-FL-CALC-DIS(IX-TAB-RDF)
              TO RDF-FL-CALC-DIS
           END-IF
           IF (SF)-COMMIS-GEST-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-COMMIS-GEST-NULL(IX-TAB-RDF)
              TO RDF-COMMIS-GEST-NULL
           ELSE
              MOVE (SF)-COMMIS-GEST(IX-TAB-RDF)
              TO RDF-COMMIS-GEST
           END-IF
           IF (SF)-NUM-QUO-CDG-FNZ-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-NUM-QUO-CDG-FNZ-NULL(IX-TAB-RDF)
              TO RDF-NUM-QUO-CDG-FNZ-NULL
           ELSE
              MOVE (SF)-NUM-QUO-CDG-FNZ(IX-TAB-RDF)
              TO RDF-NUM-QUO-CDG-FNZ
           END-IF
           IF (SF)-NUM-QUO-CDGTOT-FNZ-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-NUM-QUO-CDGTOT-FNZ-NULL(IX-TAB-RDF)
              TO RDF-NUM-QUO-CDGTOT-FNZ-NULL
           ELSE
              MOVE (SF)-NUM-QUO-CDGTOT-FNZ(IX-TAB-RDF)
              TO RDF-NUM-QUO-CDGTOT-FNZ
           END-IF
           IF (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-RDF)
              TO RDF-COS-RUN-ASSVA-IDC-NULL
           ELSE
              MOVE (SF)-COS-RUN-ASSVA-IDC(IX-TAB-RDF)
              TO RDF-COS-RUN-ASSVA-IDC
           END-IF
           IF (SF)-FL-SWM-BP2S-NULL(IX-TAB-RDF) = HIGH-VALUES
              MOVE (SF)-FL-SWM-BP2S-NULL(IX-TAB-RDF)
              TO RDF-FL-SWM-BP2S-NULL
           ELSE
              MOVE (SF)-FL-SWM-BP2S(IX-TAB-RDF)
              TO RDF-FL-SWM-BP2S
           END-IF.
       VAL-DCLGEN-RDF-EX.
           EXIT.

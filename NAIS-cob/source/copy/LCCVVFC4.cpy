
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVVFC4
      *   ULTIMO AGG. 18 SET 2007
      *------------------------------------------------------------

       INIZIA-TOT-VFC.

           PERFORM INIZIA-ZEROES-VFC THRU INIZIA-ZEROES-VFC-EX

           PERFORM INIZIA-SPACES-VFC THRU INIZIA-SPACES-VFC-EX

           PERFORM INIZIA-NULL-VFC THRU INIZIA-NULL-VFC-EX.

       INIZIA-TOT-VFC-EX.
           EXIT.

       INIZIA-NULL-VFC.
           MOVE HIGH-VALUES TO (SF)-ISPRECALC-NULL
           MOVE HIGH-VALUES TO (SF)-NOMEPROGR
           MOVE HIGH-VALUES TO (SF)-MODCALC
           MOVE HIGH-VALUES TO (SF)-GLOVARLIST
           MOVE HIGH-VALUES TO (SF)-STEP-ELAB.
       INIZIA-NULL-VFC-EX.
           EXIT.

       INIZIA-ZEROES-VFC.
           MOVE 0 TO (SF)-IDCOMP
           MOVE 0 TO (SF)-DINIZ
           MOVE 0 TO (SF)-DEND.
       INIZIA-ZEROES-VFC-EX.
           EXIT.

       INIZIA-SPACES-VFC.
           MOVE SPACES TO (SF)-CODPROD
           MOVE SPACES TO (SF)-CODTARI
           MOVE SPACES TO (SF)-NOMEFUNZ.
       INIZIA-SPACES-VFC-EX.
           EXIT.

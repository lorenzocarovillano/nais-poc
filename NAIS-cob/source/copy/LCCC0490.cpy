       01 LCCC0490.
      *
      *   Area Input
      *
          05 LCCC0490-AREA-INPUT.
      *      Identificativo polizza
             10 LCCC0490-ID-POLI                PIC 9(09).
      *      Data decorrenza della polizza
             10 LCCC0490-DT-DECOR-POLI          PIC 9(08).
      *      Garanzie
             10 LCCC0490-TAB-GRZ                OCCURS 20.
      *      Codice tariffa
                15 LCCC0490-COD-TARI            PIC X(12).
      *      Tipo garanzia
                15 LCCC0490-TP-GAR              PIC 9(01).
      *      Frazionamento
                15 LCCC0490-FRAZIONAMENTO       PIC 9(05).
      *      Tipo periodo premio
                15 LCCC0490-TP-PER-PRE          PIC X(01).
      *      Data decorrenza
                15 LCCC0490-DT-DECOR            PIC 9(08).
      *      Tipologia ricorrenza
      *         "Q" --> Quietanzamento
      *         "G" --> Generazione tranche
                15 LCCC0490-TIPO-RICORRENZA     PIC X(01).
      *
      *   Area Output
      *
          05 LCCC0490-AREA-OUTPUT.
      *      Rateo
             10 LCCC0490-RATEO                  PIC 9(09).
      *      Data fine copertura
             10 LCCC0490-DT-FINE-COP            PIC 9(08).
      *      Flag presenza rateo
             10 LCCC0490-PRESENZA-RATEO         PIC X(01).
                88 LCCC0490-SI-PRESENZA-RATEO       VALUE 'S'.
                88 LCCC0490-NO-PRESENZA-RATEO       VALUE 'N'.
      *      Flag presenza titoli emessi da stornare
             10 LCCC0490-TITOLI-EMESSI          PIC X(01).
                88 LCCC0490-SI-TIT-EMESSI           VALUE 'S'.
                88 LCCC0490-NO-TIT-EMESSI           VALUE 'N'.


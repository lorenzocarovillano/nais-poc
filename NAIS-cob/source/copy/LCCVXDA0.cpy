      ******************************************************************
      *    TP_D (Tipo Dato)
      ******************************************************************
       01  WS-TP-DATO                       PIC X(001) VALUE SPACES.
           88 TD-IMPORTO                               VALUE 'N'.
           88 TD-PERC-MILLESIMI                        VALUE 'M'.
           88 TD-TASSO                                 VALUE 'A'.
           88 TD-STRINGA                               VALUE 'S'.
           88 TD-FLAG                                  VALUE 'F'.
           88 TD-NUMERICO                              VALUE 'I'.
           88 TD-DATA                                  VALUE 'D'.
           88 TD-PERC-CENTESIMI                        VALUE 'P'.
12807      88 TD-VAR-LISTA                             VALUE 'X'.

       01 LDBV3101.
         05 LDBV3101-ID-OGG                 PIC S9(9)   COMP-3.
         05 LDBV3101-TP-OGG                 PIC  X(2).
         05 LDBV3101-TP-STA-TIT             PIC  X(2).
         05 LDBV3101-TP-TIT                 PIC  X(2).
         05 LDBV3101-DT-DA                  PIC S9(8)   COMP-3.
         05 LDBV3101-DT-DA-DB               PIC X(10)   VALUE SPACE.
         05 LDBV3101-DT-A                   PIC S9(8)   COMP-3.
         05 LDBV3101-DT-A-DB                PIC X(10)   VALUE SPACE.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVMDE5
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-MDE.
           MOVE (SF)-ID-OGG-DEROGA(IX-TAB-MDE)
              TO MDE-ID-OGG-DEROGA
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-MDE) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-MDE)
              TO MDE-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-MDE)
              TO MDE-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-MDE) NOT NUMERIC
              MOVE 0 TO MDE-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-MDE)
              TO MDE-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-MDE) NOT NUMERIC
              MOVE 0 TO MDE-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-MDE)
              TO MDE-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-MDE)
              TO MDE-COD-COMP-ANIA
           MOVE (SF)-TP-MOT-DEROGA(IX-TAB-MDE)
              TO MDE-TP-MOT-DEROGA
           MOVE (SF)-COD-LIV-AUT(IX-TAB-MDE)
              TO MDE-COD-LIV-AUT
           MOVE (SF)-COD-ERR(IX-TAB-MDE)
              TO MDE-COD-ERR
           MOVE (SF)-TP-ERR(IX-TAB-MDE)
              TO MDE-TP-ERR
           MOVE (SF)-DESC-ERR-BREVE(IX-TAB-MDE)
              TO MDE-DESC-ERR-BREVE
           MOVE (SF)-DESC-ERR-EST(IX-TAB-MDE)
              TO MDE-DESC-ERR-EST
           IF (SF)-DS-RIGA(IX-TAB-MDE) NOT NUMERIC
              MOVE 0 TO MDE-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-MDE)
              TO MDE-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-MDE)
              TO MDE-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-MDE) NOT NUMERIC
              MOVE 0 TO MDE-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-MDE)
              TO MDE-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-MDE) NOT NUMERIC
              MOVE 0 TO MDE-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-MDE)
              TO MDE-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-MDE) NOT NUMERIC
              MOVE 0 TO MDE-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-MDE)
              TO MDE-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-MDE)
              TO MDE-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-MDE)
              TO MDE-DS-STATO-ELAB.
       VAL-DCLGEN-MDE-EX.
           EXIT.

       01 EST-TRCH-DI-GAR.
         05 E12-ID-EST-TRCH-DI-GAR PIC S9(9)V     COMP-3.
         05 E12-ID-TRCH-DI-GAR PIC S9(9)V     COMP-3.
         05 E12-ID-GAR PIC S9(9)V     COMP-3.
         05 E12-ID-ADES PIC S9(9)V     COMP-3.
         05 E12-ID-POLI PIC S9(9)V     COMP-3.
         05 E12-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
         05 E12-ID-MOVI-CHIU PIC S9(9)V     COMP-3.
         05 E12-ID-MOVI-CHIU-NULL REDEFINES
            E12-ID-MOVI-CHIU   PIC X(5).
         05 E12-DT-INI-EFF   PIC S9(8)V COMP-3.
         05 E12-DT-END-EFF   PIC S9(8)V COMP-3.
         05 E12-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 E12-DT-EMIS   PIC S9(8)V COMP-3.
         05 E12-DT-EMIS-NULL REDEFINES
            E12-DT-EMIS   PIC X(5).
         05 E12-CUM-PRE-ATT PIC S9(12)V9(3) COMP-3.
         05 E12-CUM-PRE-ATT-NULL REDEFINES
            E12-CUM-PRE-ATT   PIC X(8).
         05 E12-ACCPRE-PAG PIC S9(12)V9(3) COMP-3.
         05 E12-ACCPRE-PAG-NULL REDEFINES
            E12-ACCPRE-PAG   PIC X(8).
         05 E12-CUM-PRSTZ PIC S9(12)V9(3) COMP-3.
         05 E12-CUM-PRSTZ-NULL REDEFINES
            E12-CUM-PRSTZ   PIC X(8).
         05 E12-DS-RIGA PIC S9(10)V     COMP-3.
         05 E12-DS-OPER-SQL PIC X(1).
         05 E12-DS-VER PIC S9(9)V     COMP-3.
         05 E12-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
         05 E12-DS-TS-END-CPTZ PIC S9(18)V     COMP-3.
         05 E12-DS-UTENTE PIC X(20).
         05 E12-DS-STATO-ELAB PIC X(1).
         05 E12-TP-TRCH PIC X(2).
         05 E12-CAUS-SCON PIC X(12).
         05 E12-CAUS-SCON-NULL REDEFINES
            E12-CAUS-SCON   PIC X(12).


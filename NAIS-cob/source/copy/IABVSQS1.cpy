      ******************************************************************
      * STATEMETS X GESTIONE CON FILES SEQUENZIALI
      * N.B. - DA UTILIZZARE CON LA COPY IABPSQSn
      ******************************************************************
       01 IABVSQS1.

      ******************************************************************
      * OPERAZIONE SU FILE SEQUENZIALE
      ******************************************************************
          05 OPERAZIONE-FILESQS               PIC X(02).
             88 OPEN-FILESQS-OPER             VALUE 'OP'.
             88 CLOSE-FILESQS-OPER            VALUE 'CL'.
             88 READ-FILESQS-OPER             VALUE 'RE'.
             88 WRITE-FILESQS-OPER            VALUE 'WR'.
             88 REWRITE-FILESQS-OPER          VALUE 'RW'.


      ******************************************************************
      * FLAG DI CONTROLLO x FILES SEQUENZIALI
      ******************************************************************
          05 STATO-FILESQS1           PIC X(01).
             88 FILESQS1-APERTO       VALUE 'A'.
             88 FILESQS1-CHIUSO       VALUE 'C'.

          05 STATO-FILESQS2           PIC X(01).
             88 FILESQS2-APERTO       VALUE 'A'.
             88 FILESQS2-CHIUSO       VALUE 'C'.

          05 STATO-FILESQS3           PIC X(01).
             88 FILESQS3-APERTO       VALUE 'A'.
             88 FILESQS3-CHIUSO       VALUE 'C'.

          05 STATO-FILESQS4           PIC X(01).
             88 FILESQS4-APERTO       VALUE 'A'.
             88 FILESQS4-CHIUSO       VALUE 'C'.

          05 STATO-FILESQS5           PIC X(01).
             88 FILESQS5-APERTO       VALUE 'A'.
             88 FILESQS5-CHIUSO       VALUE 'C'.

          05 STATO-FILESQS6           PIC X(01).
             88 FILESQS6-APERTO       VALUE 'A'.
             88 FILESQS6-CHIUSO       VALUE 'C'.

          05 STATO-FILESQS7           PIC X(01).
             88 FILESQS7-APERTO       VALUE 'A'.
             88 FILESQS7-CHIUSO       VALUE 'C'.

          05 STATO-FILESQS8           PIC X(01).
             88 FILESQS8-APERTO       VALUE 'A'.
             88 FILESQS8-CHIUSO       VALUE 'C'.

          05 STATO-FILESQS9           PIC X(01).
             88 FILESQS9-APERTO       VALUE 'A'.
             88 FILESQS9-CHIUSO       VALUE 'C'.

          05 STATO-FILESQS0           PIC X(01).
             88 FILESQS0-APERTO       VALUE 'A'.
             88 FILESQS0-CHIUSO       VALUE 'C'.

      ******************************************************************
      * FLAG DI CONTROLLO x END-OF-FILE
      ******************************************************************
          05 EOF-FILESQS1             PIC X(01).
             88 FILESQS1-EOF-SI       VALUE 'S'.
             88 FILESQS1-EOF-NO       VALUE 'N'.

          05 EOF-FILESQS2             PIC X(01).
             88 FILESQS2-EOF-SI       VALUE 'S'.
             88 FILESQS2-EOF-NO       VALUE 'N'.

          05 EOF-FILESQS3             PIC X(01).
             88 FILESQS3-EOF-SI       VALUE 'S'.
             88 FILESQS3-EOF-NO       VALUE 'N'.

          05 EOF-FILESQS4             PIC X(01).
             88 FILESQS4-EOF-SI       VALUE 'S'.
             88 FILESQS4-EOF-NO       VALUE 'N'.

          05 EOF-FILESQS5             PIC X(01).
             88 FILESQS5-EOF-SI       VALUE 'S'.
             88 FILESQS5-EOF-NO       VALUE 'N'.

         05 EOF-FILESQS6             PIC X(01).
             88 FILESQS6-EOF-SI       VALUE 'S'.
             88 FILESQS6-EOF-NO       VALUE 'N'.

          05 EOF-FILESQS7             PIC X(01).
             88 FILESQS7-EOF-SI       VALUE 'S'.
             88 FILESQS7-EOF-NO       VALUE 'N'.

          05 EOF-FILESQS8             PIC X(01).
             88 FILESQS8-EOF-SI       VALUE 'S'.
             88 FILESQS8-EOF-NO       VALUE 'N'.

          05 EOF-FILESQS9             PIC X(01).
             88 FILESQS9-EOF-SI       VALUE 'S'.
             88 FILESQS9-EOF-NO       VALUE 'N'.

          05 EOF-FILESQS0             PIC X(01).
             88 FILESQS0-EOF-SI       VALUE 'S'.
             88 FILESQS0-EOF-NO       VALUE 'N'.


      ******************************************************************
      * FLAG X OPEN FILES SEQUENZIALI
      ******************************************************************
          05 FLAG-OPEN-FILESQS1       PIC X(01).
             88 OPEN-FILESQS1-SI      VALUE 'S'.
             88 OPEN-FILESQS1-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQS2       PIC X(01).
             88 OPEN-FILESQS2-SI      VALUE 'S'.
             88 OPEN-FILESQS2-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQS3       PIC X(01).
             88 OPEN-FILESQS3-SI      VALUE 'S'.
             88 OPEN-FILESQS3-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQS4       PIC X(01).
             88 OPEN-FILESQS4-SI      VALUE 'S'.
             88 OPEN-FILESQS4-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQS5       PIC X(01).
             88 OPEN-FILESQS5-SI      VALUE 'S'.
             88 OPEN-FILESQS5-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQS6       PIC X(01).
             88 OPEN-FILESQS6-SI      VALUE 'S'.
             88 OPEN-FILESQS6-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQS7       PIC X(01).
             88 OPEN-FILESQS7-SI      VALUE 'S'.
             88 OPEN-FILESQS7-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQS8       PIC X(01).
             88 OPEN-FILESQS8-SI      VALUE 'S'.
             88 OPEN-FILESQS8-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQS9       PIC X(01).
             88 OPEN-FILESQS9-SI      VALUE 'S'.
             88 OPEN-FILESQS9-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQS0       PIC X(01).
             88 OPEN-FILESQS0-SI      VALUE 'S'.
             88 OPEN-FILESQS0-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQS-ALL    PIC X(01).
             88 OPEN-FILESQS-ALL-SI   VALUE 'S'.
             88 OPEN-FILESQS-ALL-NO   VALUE 'N'.


      ******************************************************************
      * FLAG X READ FILES SEQUENZIALI
      ******************************************************************
          05 FLAG-READ-FILESQS1       PIC X(01).
             88 READ-FILESQS1-SI      VALUE 'S'.
             88 READ-FILESQS1-NO      VALUE 'N'.

          05 FLAG-READ-FILESQS2       PIC X(01).
             88 READ-FILESQS2-SI      VALUE 'S'.
             88 READ-FILESQS2-NO      VALUE 'N'.

          05 FLAG-READ-FILESQS3       PIC X(01).
             88 READ-FILESQS3-SI      VALUE 'S'.
             88 READ-FILESQS3-NO      VALUE 'N'.

          05 FLAG-READ-FILESQS4       PIC X(01).
             88 READ-FILESQS4-SI      VALUE 'S'.
             88 READ-FILESQS4-NO      VALUE 'N'.

          05 FLAG-READ-FILESQS5       PIC X(01).
             88 READ-FILESQS5-SI      VALUE 'S'.
             88 READ-FILESQS5-NO      VALUE 'N'.

          05 FLAG-READ-FILESQS6       PIC X(01).
             88 READ-FILESQS6-SI      VALUE 'S'.
             88 READ-FILESQS6-NO      VALUE 'N'.

          05 FLAG-READ-FILESQS7       PIC X(01).
             88 READ-FILESQS7-SI      VALUE 'S'.
             88 READ-FILESQS7-NO      VALUE 'N'.

          05 FLAG-READ-FILESQS8       PIC X(01).
             88 READ-FILESQS8-SI      VALUE 'S'.
             88 READ-FILESQS8-NO      VALUE 'N'.

          05 FLAG-READ-FILESQS9       PIC X(01).
             88 READ-FILESQS9-SI      VALUE 'S'.
             88 READ-FILESQS9-NO      VALUE 'N'.

          05 FLAG-READ-FILESQS0       PIC X(01).
             88 READ-FILESQS0-SI      VALUE 'S'.
             88 READ-FILESQS0-NO      VALUE 'N'.

          05 FLAG-READ-FILESQS-ALL    PIC X(01).
             88 READ-FILESQS-ALL-SI   VALUE 'S'.
             88 READ-FILESQS-ALL-NO   VALUE 'N'.


      ******************************************************************
      * FLAG X WRITE FILES SEQUENZIALI
      ******************************************************************
          05 FLAG-WRITE-FILESQS1       PIC X(01).
             88 WRITE-FILESQS1-SI      VALUE 'S'.
             88 WRITE-FILESQS1-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQS2       PIC X(01).
             88 WRITE-FILESQS2-SI      VALUE 'S'.
             88 WRITE-FILESQS2-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQS3       PIC X(01).
             88 WRITE-FILESQS3-SI      VALUE 'S'.
             88 WRITE-FILESQS3-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQS4       PIC X(01).
             88 WRITE-FILESQS4-SI      VALUE 'S'.
             88 WRITE-FILESQS4-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQS5       PIC X(01).
             88 WRITE-FILESQS5-SI      VALUE 'S'.
             88 WRITE-FILESQS5-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQS6       PIC X(01).
             88 WRITE-FILESQS6-SI      VALUE 'S'.
             88 WRITE-FILESQS6-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQS7       PIC X(01).
             88 WRITE-FILESQS7-SI      VALUE 'S'.
             88 WRITE-FILESQS7-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQS8       PIC X(01).
             88 WRITE-FILESQS8-SI      VALUE 'S'.
             88 WRITE-FILESQS8-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQS9       PIC X(01).
             88 WRITE-FILESQS9-SI      VALUE 'S'.
             88 WRITE-FILESQS9-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQS0       PIC X(01).
             88 WRITE-FILESQS0-SI      VALUE 'S'.
             88 WRITE-FILESQS0-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQS-ALL    PIC X(01).
             88 WRITE-FILESQS-ALL-SI   VALUE 'S'.
             88 WRITE-FILESQS-ALL-NO   VALUE 'N'.

      ******************************************************************
      * FLAG X WRITE FILES SEQUENZIALI
      ******************************************************************
          05 FLAG-REWRITE-FILESQS1       PIC X(01).
             88 REWRITE-FILESQS1-SI      VALUE 'S'.
             88 REWRITE-FILESQS1-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQS2       PIC X(01).
             88 REWRITE-FILESQS2-SI      VALUE 'S'.
             88 REWRITE-FILESQS2-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQS3       PIC X(01).
             88 REWRITE-FILESQS3-SI      VALUE 'S'.
             88 REWRITE-FILESQS3-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQS4       PIC X(01).
             88 REWRITE-FILESQS4-SI      VALUE 'S'.
             88 REWRITE-FILESQS4-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQS5       PIC X(01).
             88 REWRITE-FILESQS5-SI      VALUE 'S'.
             88 REWRITE-FILESQS5-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQS6       PIC X(01).
             88 REWRITE-FILESQS6-SI      VALUE 'S'.
             88 REWRITE-FILESQS6-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQS7       PIC X(01).
             88 REWRITE-FILESQS7-SI      VALUE 'S'.
             88 REWRITE-FILESQS7-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQS8       PIC X(01).
             88 REWRITE-FILESQS8-SI      VALUE 'S'.
             88 REWRITE-FILESQS8-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQS9       PIC X(01).
             88 REWRITE-FILESQS9-SI      VALUE 'S'.
             88 REWRITE-FILESQS9-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQS0       PIC X(01).
             88 REWRITE-FILESQS0-SI      VALUE 'S'.
             88 REWRITE-FILESQS0-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQS-ALL    PIC X(01).
             88 REWRITE-FILESQS-ALL-SI   VALUE 'S'.
             88 REWRITE-FILESQS-ALL-NO   VALUE 'N'.

      ******************************************************************
      * FLAG X CLOSE FILES SEQUENZIALI
      ******************************************************************
          05 FLAG-CLOSE-FILESQS1       PIC X(01).
             88 CLOSE-FILESQS1-SI      VALUE 'S'.
             88 CLOSE-FILESQS1-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQS2       PIC X(01).
             88 CLOSE-FILESQS2-SI      VALUE 'S'.
             88 CLOSE-FILESQS2-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQS3       PIC X(01).
             88 CLOSE-FILESQS3-SI      VALUE 'S'.
             88 CLOSE-FILESQS3-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQS4       PIC X(01).
             88 CLOSE-FILESQS4-SI      VALUE 'S'.
             88 CLOSE-FILESQS4-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQS5       PIC X(01).
             88 CLOSE-FILESQS5-SI      VALUE 'S'.
             88 CLOSE-FILESQS5-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQS6       PIC X(01).
             88 CLOSE-FILESQS6-SI      VALUE 'S'.
             88 CLOSE-FILESQS6-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQS7       PIC X(01).
             88 CLOSE-FILESQS7-SI      VALUE 'S'.
             88 CLOSE-FILESQS7-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQS8       PIC X(01).
             88 CLOSE-FILESQS8-SI      VALUE 'S'.
             88 CLOSE-FILESQS8-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQS9       PIC X(01).
             88 CLOSE-FILESQS9-SI      VALUE 'S'.
             88 CLOSE-FILESQS9-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQS0       PIC X(01).
             88 CLOSE-FILESQS0-SI      VALUE 'S'.
             88 CLOSE-FILESQS0-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQS-ALL    PIC X(01).
             88 CLOSE-FILESQS-ALL-SI   VALUE 'S'.
             88 CLOSE-FILESQS-ALL-NO   VALUE 'N'.


      ******************************************************************
      * FLAG X TIPO OPEN FILES SEQUENZIALI
      ******************************************************************

          05 OPEN-TYPE-FILESQS1     PIC X(03).
             88 INPUT-FILESQS1      VALUE 'INP'.
             88 OUTPUT-FILESQS1     VALUE 'OUT'.
             88 I-O-FILESQS1        VALUE 'I-O'.
             88 EXTEND-FILESQS1     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQS2     PIC X(03).
             88 INPUT-FILESQS2      VALUE 'INP'.
             88 OUTPUT-FILESQS2     VALUE 'OUT'.
             88 I-O-FILESQS2        VALUE 'I-O'.
             88 EXTEND-FILESQS2     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQS3     PIC X(03).
             88 INPUT-FILESQS3      VALUE 'INP'.
             88 OUTPUT-FILESQS3     VALUE 'OUT'.
             88 I-O-FILESQS3        VALUE 'I-O'.
             88 EXTEND-FILESQS3     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQS4     PIC X(03).
             88 INPUT-FILESQS4      VALUE 'INP'.
             88 OUTPUT-FILESQS4     VALUE 'OUT'.
             88 I-O-FILESQS4        VALUE 'I-O'.
             88 EXTEND-FILESQS4     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQS5     PIC X(03).
             88 INPUT-FILESQS5      VALUE 'INP'.
             88 OUTPUT-FILESQS5     VALUE 'OUT'.
             88 I-O-FILESQS5        VALUE 'I-O'.
             88 EXTEND-FILESQS5     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQS6     PIC X(03).
             88 INPUT-FILESQS6      VALUE 'INP'.
             88 OUTPUT-FILESQS6     VALUE 'OUT'.
             88 I-O-FILESQS6        VALUE 'I-O'.
             88 EXTEND-FILESQS6     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQS7     PIC X(03).
             88 INPUT-FILESQS7      VALUE 'INP'.
             88 OUTPUT-FILESQS7     VALUE 'OUT'.
             88 I-O-FILESQS7        VALUE 'I-O'.
             88 EXTEND-FILESQS7     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQS8     PIC X(03).
             88 INPUT-FILESQS8      VALUE 'INP'.
             88 OUTPUT-FILESQS8     VALUE 'OUT'.
             88 I-O-FILESQS8        VALUE 'I-O'.
             88 EXTEND-FILESQS8     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQS9     PIC X(03).
             88 INPUT-FILESQS9      VALUE 'INP'.
             88 OUTPUT-FILESQS9     VALUE 'OUT'.
             88 I-O-FILESQS9        VALUE 'I-O'.
             88 EXTEND-FILESQS9     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQS0     PIC X(03).
             88 INPUT-FILESQS0      VALUE 'INP'.
             88 OUTPUT-FILESQS0     VALUE 'OUT'.
             88 I-O-FILESQS0        VALUE 'I-O'.
             88 EXTEND-FILESQS0     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQS-ALL  PIC X(03).
             88 INPUT-FILESQS-ALL   VALUE 'INP'.
             88 OUTPUT-FILESQS-ALL  VALUE 'OUT'.
             88 I-O-FILESQS-ALL     VALUE 'I-O'.
             88 EXTEND-FILESQS-ALL  VALUE 'EXT'.

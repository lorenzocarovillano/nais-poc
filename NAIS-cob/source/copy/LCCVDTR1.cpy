      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA DETT_TIT_DI_RAT
      *   ALIAS DTR
      *   ULTIMO AGG. 28 NOV 2014
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-DETT-TIT-DI-RAT PIC S9(9)     COMP-3.
             07 (SF)-ID-TIT-RAT PIC S9(9)     COMP-3.
             07 (SF)-ID-OGG PIC S9(9)     COMP-3.
             07 (SF)-TP-OGG PIC X(2).
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-DT-INI-COP   PIC S9(8) COMP-3.
             07 (SF)-DT-INI-COP-NULL REDEFINES
                (SF)-DT-INI-COP   PIC X(5).
             07 (SF)-DT-END-COP   PIC S9(8) COMP-3.
             07 (SF)-DT-END-COP-NULL REDEFINES
                (SF)-DT-END-COP   PIC X(5).
             07 (SF)-PRE-NET PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-NET-NULL REDEFINES
                (SF)-PRE-NET   PIC X(8).
             07 (SF)-INTR-FRAZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-INTR-FRAZ-NULL REDEFINES
                (SF)-INTR-FRAZ   PIC X(8).
             07 (SF)-INTR-MORA PIC S9(12)V9(3) COMP-3.
             07 (SF)-INTR-MORA-NULL REDEFINES
                (SF)-INTR-MORA   PIC X(8).
             07 (SF)-INTR-RETDT PIC S9(12)V9(3) COMP-3.
             07 (SF)-INTR-RETDT-NULL REDEFINES
                (SF)-INTR-RETDT   PIC X(8).
             07 (SF)-INTR-RIAT PIC S9(12)V9(3) COMP-3.
             07 (SF)-INTR-RIAT-NULL REDEFINES
                (SF)-INTR-RIAT   PIC X(8).
             07 (SF)-DIR PIC S9(12)V9(3) COMP-3.
             07 (SF)-DIR-NULL REDEFINES
                (SF)-DIR   PIC X(8).
             07 (SF)-SPE-MED PIC S9(12)V9(3) COMP-3.
             07 (SF)-SPE-MED-NULL REDEFINES
                (SF)-SPE-MED   PIC X(8).
             07 (SF)-SPE-AGE PIC S9(12)V9(3) COMP-3.
             07 (SF)-SPE-AGE-NULL REDEFINES
                (SF)-SPE-AGE   PIC X(8).
             07 (SF)-TAX PIC S9(12)V9(3) COMP-3.
             07 (SF)-TAX-NULL REDEFINES
                (SF)-TAX   PIC X(8).
             07 (SF)-SOPR-SAN PIC S9(12)V9(3) COMP-3.
             07 (SF)-SOPR-SAN-NULL REDEFINES
                (SF)-SOPR-SAN   PIC X(8).
             07 (SF)-SOPR-SPO PIC S9(12)V9(3) COMP-3.
             07 (SF)-SOPR-SPO-NULL REDEFINES
                (SF)-SOPR-SPO   PIC X(8).
             07 (SF)-SOPR-TEC PIC S9(12)V9(3) COMP-3.
             07 (SF)-SOPR-TEC-NULL REDEFINES
                (SF)-SOPR-TEC   PIC X(8).
             07 (SF)-SOPR-PROF PIC S9(12)V9(3) COMP-3.
             07 (SF)-SOPR-PROF-NULL REDEFINES
                (SF)-SOPR-PROF   PIC X(8).
             07 (SF)-SOPR-ALT PIC S9(12)V9(3) COMP-3.
             07 (SF)-SOPR-ALT-NULL REDEFINES
                (SF)-SOPR-ALT   PIC X(8).
             07 (SF)-PRE-TOT PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-TOT-NULL REDEFINES
                (SF)-PRE-TOT   PIC X(8).
             07 (SF)-PRE-PP-IAS PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-PP-IAS-NULL REDEFINES
                (SF)-PRE-PP-IAS   PIC X(8).
             07 (SF)-PRE-SOLO-RSH PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-SOLO-RSH-NULL REDEFINES
                (SF)-PRE-SOLO-RSH   PIC X(8).
             07 (SF)-CAR-IAS PIC S9(12)V9(3) COMP-3.
             07 (SF)-CAR-IAS-NULL REDEFINES
                (SF)-CAR-IAS   PIC X(8).
             07 (SF)-PROV-ACQ-1AA PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-ACQ-1AA-NULL REDEFINES
                (SF)-PROV-ACQ-1AA   PIC X(8).
             07 (SF)-PROV-ACQ-2AA PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-ACQ-2AA-NULL REDEFINES
                (SF)-PROV-ACQ-2AA   PIC X(8).
             07 (SF)-PROV-RICOR PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-RICOR-NULL REDEFINES
                (SF)-PROV-RICOR   PIC X(8).
             07 (SF)-PROV-INC PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-INC-NULL REDEFINES
                (SF)-PROV-INC   PIC X(8).
             07 (SF)-PROV-DA-REC PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-DA-REC-NULL REDEFINES
                (SF)-PROV-DA-REC   PIC X(8).
             07 (SF)-COD-DVS PIC X(20).
             07 (SF)-COD-DVS-NULL REDEFINES
                (SF)-COD-DVS   PIC X(20).
             07 (SF)-FRQ-MOVI PIC S9(5)     COMP-3.
             07 (SF)-FRQ-MOVI-NULL REDEFINES
                (SF)-FRQ-MOVI   PIC X(3).
             07 (SF)-TP-RGM-FISC PIC X(2).
             07 (SF)-COD-TARI PIC X(12).
             07 (SF)-COD-TARI-NULL REDEFINES
                (SF)-COD-TARI   PIC X(12).
             07 (SF)-TP-STAT-TIT PIC X(2).
             07 (SF)-IMP-AZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-AZ-NULL REDEFINES
                (SF)-IMP-AZ   PIC X(8).
             07 (SF)-IMP-ADER PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-ADER-NULL REDEFINES
                (SF)-IMP-ADER   PIC X(8).
             07 (SF)-IMP-TFR PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-TFR-NULL REDEFINES
                (SF)-IMP-TFR   PIC X(8).
             07 (SF)-IMP-VOLO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-VOLO-NULL REDEFINES
                (SF)-IMP-VOLO   PIC X(8).
             07 (SF)-FL-VLDT-TIT PIC X(1).
             07 (SF)-FL-VLDT-TIT-NULL REDEFINES
                (SF)-FL-VLDT-TIT   PIC X(1).
             07 (SF)-CAR-ACQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-CAR-ACQ-NULL REDEFINES
                (SF)-CAR-ACQ   PIC X(8).
             07 (SF)-CAR-GEST PIC S9(12)V9(3) COMP-3.
             07 (SF)-CAR-GEST-NULL REDEFINES
                (SF)-CAR-GEST   PIC X(8).
             07 (SF)-CAR-INC PIC S9(12)V9(3) COMP-3.
             07 (SF)-CAR-INC-NULL REDEFINES
                (SF)-CAR-INC   PIC X(8).
             07 (SF)-MANFEE-ANTIC PIC S9(12)V9(3) COMP-3.
             07 (SF)-MANFEE-ANTIC-NULL REDEFINES
                (SF)-MANFEE-ANTIC   PIC X(8).
             07 (SF)-MANFEE-RICOR PIC S9(12)V9(3) COMP-3.
             07 (SF)-MANFEE-RICOR-NULL REDEFINES
                (SF)-MANFEE-RICOR   PIC X(8).
             07 (SF)-MANFEE-REC PIC X(18).
             07 (SF)-MANFEE-REC-NULL REDEFINES
                (SF)-MANFEE-REC   PIC X(18).
             07 (SF)-TOT-INTR-PREST PIC S9(12)V9(3) COMP-3.
             07 (SF)-TOT-INTR-PREST-NULL REDEFINES
                (SF)-TOT-INTR-PREST   PIC X(8).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-IMP-TRASFE PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-TRASFE-NULL REDEFINES
                (SF)-IMP-TRASFE   PIC X(8).
             07 (SF)-IMP-TFR-STRC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-TFR-STRC-NULL REDEFINES
                (SF)-IMP-TFR-STRC   PIC X(8).
             07 (SF)-ACQ-EXP PIC S9(12)V9(3) COMP-3.
             07 (SF)-ACQ-EXP-NULL REDEFINES
                (SF)-ACQ-EXP   PIC X(8).
             07 (SF)-REMUN-ASS PIC S9(12)V9(3) COMP-3.
             07 (SF)-REMUN-ASS-NULL REDEFINES
                (SF)-REMUN-ASS   PIC X(8).
             07 (SF)-COMMIS-INTER PIC S9(12)V9(3) COMP-3.
             07 (SF)-COMMIS-INTER-NULL REDEFINES
                (SF)-COMMIS-INTER   PIC X(8).
             07 (SF)-CNBT-ANTIRAC PIC S9(12)V9(3) COMP-3.
             07 (SF)-CNBT-ANTIRAC-NULL REDEFINES
                (SF)-CNBT-ANTIRAC   PIC X(8).

      *---------------------------------------------------------------*
      *                                                               *
      *             I/O AREA ROUTINE NUOVA LCCC0003                   *
      *                                                               *
      *---------------------------------------------------------------*
       01  IO-A2K-LCCC0003.
           05  A2K-LCCC0003.
      *
      *                                       funzione richiesta
      *                                       01 : conversione data
      *                                       02 : somma DELTA e
      *                                            conversione data
      *                                       03 : sottrae DELTA e
      *                                            conversione data
      *                                       04 : controllo festivita'
      *                                            della data
      *                                       05 : controllo se decadale
      *                                            quindicinale, num.
      *                                            trimestre, fine mese
      *                                       06 : calcolo pasqua per
      *                                            la data di input
      *                                       07 : come funz 01 tranne
      *                                            che per OUGG07 e
      *                                            OUFA07 num gg lav del
      *                                            mese e ultimo gg lav
      *                                            del mese
             10 A2K-INPUT.
                15 A2K-FUNZ             PIC X(02).
      *
      *                                       formato data input
      *                                       01 : GGMMAAAA   in INGMA
      *                                       02 : GG/MM/AAAA in INGBMBA
      *                                       03 : AAAAMMGG   in INAMG
      *                                       04 : AAAAMMGGs  in INAMGP
      *                                       05 : AAAAMMGG0s in INAMG0P
      *                                       06 : GGGGGs     in INPROG9
      *                                       07 : GGGGGGGs   in INPROG
      *                                       08 : AAAAGGG    in INJUL
      *                                       09 : GGGGGs     in INPROGC
      *                                       10 : GGGGGGGs   in INPROGO
      *                                       11 : GG.MM.AAAA in INGBMBA
                15 A2K-INFO             PIC X(02).
      *
      *                                       valore da sommare
                15 A2K-DELTA-X.
                   20 A2K-DELTA         PIC 9(03).
      *
      *                                       tipo delta : G = giorni
      *                                                    M = mesi
      *                                                    A = anni
                15 A2K-TDELTA           PIC X.
      *
      *                                       tipo giorni da sommare
      *                                       0 = fissi
      *                                       1 = lavorativi
                15 A2K-FISLAV           PIC X.
      *
      *                                       giorno inizio conteggio
      *                                       0 = stesso giorno
      *                                       1 = primo giorno lavorat.
      *                                       2 = se fest.,lavorat. prec
                15 A2K-INICON           PIC X.
      *
      *                                       data di input
                15 A2K-INDATA           PIC X(10).
      *
      *                                       formato 01 GGMMAAAA
                15 FILLER           REDEFINES A2K-INDATA.
                   20 A2K-INGMA         PIC 9(08).
                   20 A2K-INGMA-X       REDEFINES A2K-INGMA.
                      25 A2K-INGG       PIC 9(02).
                      25 A2K-INMM       PIC 9(02).
                      25 A2K-INSS       PIC 9(02).
                      25 A2K-INAA       PIC 9(02).
                   20 FILLER            PIC 9(02).
      *
      *                                       formato 02 GG/MM/AAAA
                15 FILLER           REDEFINES A2K-INDATA.
                   20 A2K-INGBMBA.
                      25 A2K-INGG02     PIC 9(02).
                      25 A2K-INS102     PIC X.
                      25 A2K-INMM02     PIC 9(02).
                      25 A2K-INS202     PIC X.
                      25 A2K-INSS02     PIC 9(02).
                      25 A2K-INAA02     PIC 9(02).
      *
      *                                       formato 03 AAAAMMGG
                15 FILLER           REDEFINES A2K-INDATA.
                   20 A2K-INAMG         PIC 9(08).
                   20 A2K-INAMG-X       REDEFINES A2K-INAMG.
                      25 A2K-INSS03     PIC 9(02).
                      25 A2K-INAA03     PIC 9(02).
                      25 A2K-INMM03     PIC 9(02).
                      25 A2K-INGG03     PIC 9(02).
                   20 FILLER            PIC 9(02).
      *
      *                                       formato 04 0AAAAMMGGs
      *                                                      packed
                15 FILLER           REDEFINES A2K-INDATA.
                   20 A2K-INAMGP        PIC S9(09) COMP-3.
                   20 FILLER            PIC  X(05).
      *
      *                                       formato 05 AAAAMMGG0s
      *                                                      packed
                15 FILLER           REDEFINES A2K-INDATA.
                   20 A2K-INAMG0P       PIC S9(09) COMP-3.
                   20 FILLER            PIC  X(05).
      *
      *                                       formato 06 GGGGGs packed
      *                                           progressivo dal 1900
                15 FILLER           REDEFINES A2K-INDATA.
                   20 A2K-INPROG9       PIC S9(05) COMP-3.
                   20 FILLER            PIC  X(07).
      *
      *                                       formato 07 GGGGGGGs packed
      *                                                      progressivo
                15 FILLER           REDEFINES A2K-INDATA.
                   20 A2K-INPROG        PIC S9(07) COMP-3.
                   20 FILLER            PIC  X(06).
      *
      *                                       formato 08 AAAAGGG
      *                                            data giuliana
                15 FILLER           REDEFINES A2K-INDATA.
                   20 A2K-INJUL         PIC 9(07).
                   20 A2K-INJUL-X       REDEFINES A2K-INJUL.
                      25 A2K-INJSS      PIC 9(02).
                      25 A2K-INJAA      PIC 9(02).
                      25 A2K-INJGGG     PIC 9(03).
                   20 FILLER            PIC XXX.
      *
      *                                       formato 09 GGGGGs packed
      *                                          progr. comm. dal 1900
                15 FILLER           REDEFINES A2K-INDATA.
                   20 A2K-INPROGC       PIC S9(05) COMP-3.
                   20 FILLER            PIC  X(07).
      *
      *                                       formato 10 GGGGGGGs packed
      *                                          progressivo commerciale
                15 FILLER           REDEFINES A2K-INDATA.
                   20 A2K-INPROCO       PIC S9(07) COMP-3.
                   20 FILLER            PIC  X(06).
      *
      *                                       formati di output
      *
      *                                       formato GGMMAAAA
             10 A2K-OUTPUT.
                15 A2K-OUGMA            PIC 9(08).
                15 A2K-OUGMA-X          REDEFINES A2K-OUGMA.
                   20 A2K-OUGG          PIC 9(02).
                   20 A2K-OUMM          PIC 9(02).
                   20 A2K-OUSS          PIC 9(02).
                   20 A2K-OUAA          PIC 9(02).
      *
      *                                       formato GG/MM/AAAA
                15 A2K-OUGBMBA.
                   20 A2K-OUGG02        PIC 9(02).
                   20 A2K-OUS102        PIC X.
                   20 A2K-OUMM02        PIC 9(02).
                   20 A2K-OUS202        PIC X.
                   20 A2K-OUSS02        PIC 9(02).
                   20 A2K-OUAA02        PIC 9(02).
      *
      *                                       formato AAAAMMGG
                15 A2K-OUAMG            PIC 9(08).
                15 A2K-OUAMG-X          REDEFINES A2K-OUAMG.
                   20 A2K-OUSS03        PIC 9(02).
                   20 A2K-OUAA03        PIC 9(02).
                   20 A2K-OUMM03        PIC 9(02).
                   20 A2K-OUGG03        PIC 9(02).
      *
      *                                            formato 0AAAAMMGGs
      *                                                        packed
                15 A2K-OUAMGP           PIC 9(9) COMP-3.
      *
      *                                            formato AAAAMMGG0s
      *                                                        packed
                15 A2K-OUAMG0P          PIC 9(9) COMP-3.
      *
      *                                            formato GGGGGs packed
      *                                             progressivo dal 1900
                15 A2K-OUPROG9          PIC 9(5) COMP-3.
      *
      *                                            formato GGGGGs packed
      *                                            progr. comm. dal 1900
                15 A2K-OUPROGC          PIC 9(5) COMP-3.
      *
      *                                            formato GGGGGGGs pack
      *                                                        progressi
                15 A2K-OUPROG           PIC 9(7) COMP-3.
      *
      *                                            formato GGGGGGGs pack
      *                                            progressivo commercia
                15 A2K-OUPROCO          PIC 9(7) COMP-3.
      *
      *                                            formato AAAAGGG giuli
                15 A2K-OUJUL            PIC 9(7).
                15 A2K-OUJUL-X          REDEFINES A2K-OUJUL.
                   20 A2K-OUJSS         PIC 9(02).
                   20 A2K-OUJAA         PIC 9(02).
                   20 A2K-OUJGGG        PIC 9(03).
      *
      *                                            formato esteso per st
                15 A2K-OUSTA.
                   20 A2K-OUGG04        PIC 9(02).
                   20 FILLER            PIC X.
                   20 A2K-OUMM04        PIC X(09).
                   20 FILLER            PIC X.
                   20 A2K-OUSS04        PIC 9(02).
                   20 A2K-OUAA04        PIC 9(02).
      *
      *                                            formato ridotto per s
                15 A2K-OURID.
                   20 A2K-OUGG05        PIC 9(02).
                   20 FILLER            PIC X.
                   20 A2K-OUMM05        PIC X(03).
                   20 FILLER            PIC X.
                   20 A2K-OUAA05        PIC 9(02).
      *
      *                                            descrizione giorno
                15 A2K-OUGG06           PIC X(09).
      *
      *                                            Nr. giorno settimana
      *                                            0 = domenica
      *                                            1 = lunedi
      *                                            2 = martedi
      *                                            3 = mercoledi
      *                                            4 = giovedi
      *                                            5 = venerdi
      *                                            6 = sabato
                15 A2K-OUNG06           PIC 9.
      *
      *                                            festa
      *                                            F = festivo (anche sa
      *                                            N = non festivo
                15 A2K-OUTG06           PIC X.
      *
      *                                            Nr. giorni del mese
      *                                            giorni lav. del mese
                15 A2K-OUGG07           PIC 99.
      *
      *                                            giorni a fine anno
                15 A2K-OUFA07           PIC 999.
      *
      *                                            ult. gg lav. del mese
                15 A2K-OUGL07-X REDEFINES A2K-OUFA07.
                   20 A2K-OUGL07        PIC 99.
                   20 FILLER            PIC X.
      *
      *                                            Nr. trimestre
      *
                15 A2K-OUGG08           PIC 9.
      *
      *                                            tipo giorno
      *                                            spaces oppure
      *                                            D = fine decade
      *                                            Q = fine quindicina
      *                                            M = fine mese
                15 A2K-OUGG09           PIC X.
      *
      *                                            spaces oppure
      *                                            P = pasqua o pasquett
      *                                            F = festivita' fissa
                15 A2K-OUGG10           PIC X.
      *
      *                                       return code
      *
           05  A2K-RCODE                PIC X(2).

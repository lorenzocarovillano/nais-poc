
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVNOT5
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-NOT.
           MOVE (SF)-COD-COMP-ANIA
              TO NOT-COD-COMP-ANIA
           MOVE (SF)-TP-OGG
              TO NOT-TP-OGG
           MOVE (SF)-NOTA-OGG
              TO NOT-NOTA-OGG
           MOVE (SF)-DS-OPER-SQL
              TO NOT-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO NOT-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO NOT-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO NOT-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO NOT-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO NOT-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO NOT-DS-STATO-ELAB.
       VAL-DCLGEN-NOT-EX.
           EXIT.

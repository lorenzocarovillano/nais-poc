       01 PREST.
         05 PRE-ID-PREST PIC S9(9)V     COMP-3.
         05 PRE-ID-OGG PIC S9(9)V     COMP-3.
         05 PRE-TP-OGG PIC X(2).
         05 PRE-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
         05 PRE-ID-MOVI-CHIU PIC S9(9)V     COMP-3.
         05 PRE-ID-MOVI-CHIU-NULL REDEFINES
            PRE-ID-MOVI-CHIU   PIC X(5).
         05 PRE-DT-INI-EFF   PIC S9(8)V COMP-3.
         05 PRE-DT-END-EFF   PIC S9(8)V COMP-3.
         05 PRE-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 PRE-DT-CONCS-PREST   PIC S9(8)V COMP-3.
         05 PRE-DT-CONCS-PREST-NULL REDEFINES
            PRE-DT-CONCS-PREST   PIC X(5).
         05 PRE-DT-DECOR-PREST   PIC S9(8)V COMP-3.
         05 PRE-DT-DECOR-PREST-NULL REDEFINES
            PRE-DT-DECOR-PREST   PIC X(5).
         05 PRE-IMP-PREST PIC S9(12)V9(3) COMP-3.
         05 PRE-IMP-PREST-NULL REDEFINES
            PRE-IMP-PREST   PIC X(8).
         05 PRE-INTR-PREST PIC S9(3)V9(3) COMP-3.
         05 PRE-INTR-PREST-NULL REDEFINES
            PRE-INTR-PREST   PIC X(4).
         05 PRE-TP-PREST PIC X(2).
         05 PRE-TP-PREST-NULL REDEFINES
            PRE-TP-PREST   PIC X(2).
         05 PRE-FRAZ-PAG-INTR PIC S9(5)V     COMP-3.
         05 PRE-FRAZ-PAG-INTR-NULL REDEFINES
            PRE-FRAZ-PAG-INTR   PIC X(3).
         05 PRE-DT-RIMB   PIC S9(8)V COMP-3.
         05 PRE-DT-RIMB-NULL REDEFINES
            PRE-DT-RIMB   PIC X(5).
         05 PRE-IMP-RIMB PIC S9(12)V9(3) COMP-3.
         05 PRE-IMP-RIMB-NULL REDEFINES
            PRE-IMP-RIMB   PIC X(8).
         05 PRE-COD-DVS PIC X(20).
         05 PRE-COD-DVS-NULL REDEFINES
            PRE-COD-DVS   PIC X(20).
         05 PRE-DT-RICH-PREST   PIC S9(8)V COMP-3.
         05 PRE-MOD-INTR-PREST PIC X(2).
         05 PRE-MOD-INTR-PREST-NULL REDEFINES
            PRE-MOD-INTR-PREST   PIC X(2).
         05 PRE-SPE-PREST PIC S9(12)V9(3) COMP-3.
         05 PRE-SPE-PREST-NULL REDEFINES
            PRE-SPE-PREST   PIC X(8).
         05 PRE-IMP-PREST-LIQTO PIC S9(12)V9(3) COMP-3.
         05 PRE-IMP-PREST-LIQTO-NULL REDEFINES
            PRE-IMP-PREST-LIQTO   PIC X(8).
         05 PRE-SDO-INTR PIC S9(12)V9(3) COMP-3.
         05 PRE-SDO-INTR-NULL REDEFINES
            PRE-SDO-INTR   PIC X(8).
         05 PRE-RIMB-EFF PIC S9(12)V9(3) COMP-3.
         05 PRE-RIMB-EFF-NULL REDEFINES
            PRE-RIMB-EFF   PIC X(8).
         05 PRE-PREST-RES-EFF PIC S9(12)V9(3) COMP-3.
         05 PRE-PREST-RES-EFF-NULL REDEFINES
            PRE-PREST-RES-EFF   PIC X(8).
         05 PRE-DS-RIGA PIC S9(10)V     COMP-3.
         05 PRE-DS-OPER-SQL PIC X(1).
         05 PRE-DS-VER PIC S9(9)V     COMP-3.
         05 PRE-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
         05 PRE-DS-TS-END-CPTZ PIC S9(18)V     COMP-3.
         05 PRE-DS-UTENTE PIC X(20).
         05 PRE-DS-STATO-ELAB PIC X(1).


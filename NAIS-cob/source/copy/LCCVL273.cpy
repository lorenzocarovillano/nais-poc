
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVL273
      *   ULTIMO AGG. 21 NOV 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-L27.
           MOVE L27-ID-PREV
             TO (SF)-ID-PTF
           MOVE L27-ID-PREV
             TO (SF)-ID-PREV
           MOVE L27-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE L27-STAT-PREV
             TO (SF)-STAT-PREV
           IF L27-ID-OGG-NULL = HIGH-VALUES
              MOVE L27-ID-OGG-NULL
                TO (SF)-ID-OGG-NULL
           ELSE
              MOVE L27-ID-OGG
                TO (SF)-ID-OGG
           END-IF
           IF L27-TP-OGG-NULL = HIGH-VALUES
              MOVE L27-TP-OGG-NULL
                TO (SF)-TP-OGG-NULL
           ELSE
              MOVE L27-TP-OGG
                TO (SF)-TP-OGG
           END-IF
           MOVE L27-IB-OGG
             TO (SF)-IB-OGG
           MOVE L27-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE L27-DS-VER
             TO (SF)-DS-VER
           MOVE L27-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE L27-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE L27-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB.
       VALORIZZA-OUTPUT-L27-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY      ..... LCCVL306
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO REINVESTIMENTO POLIZZE LIQ.
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVL305 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN REINVST-POLI-LQ (LCCVL301)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-REINVST-POLI.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE REINVST-POLI-LQ.

      *--> NOME TABELLA FISICA DB
           MOVE 'REINVST-POLI-LQ'         TO   WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO" O "CONVERSAZIONE"
           IF  NOT WL30-ST-INV(IX-TAB-L30)
           AND NOT WL30-ST-CON(IX-TAB-L30)
           AND     WL30-ELE-REINVST-POLI-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WL30-ST-ADD(IX-TAB-L30)

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK
                        MOVE S090-SEQ-TABELLA TO WL30-ID-PTF(IX-TAB-L30)
                                                 L30-ID-REINVST-POLI-LQ
                        MOVE WMOV-ID-PTF      TO L30-ID-MOVI-CRZ
                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WL30-ST-MOD(IX-TAB-L30)

                       MOVE WL30-ID-PTF(IX-TAB-L30)
                         TO L30-ID-REINVST-POLI-LQ
                       MOVE WMOV-ID-PTF
                         TO L30-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WL30-ST-DEL(IX-TAB-L30)

                       MOVE WL30-ID-PTF(IX-TAB-L30)
                         TO L30-ID-REINVST-POLI-LQ
                       MOVE WMOV-ID-PTF
                         TO L30-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN REINVESTIMENTO
                 PERFORM VAL-DCLGEN-L30
                    THRU VAL-DCLGEN-L30-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-L30
                    THRU VALORIZZA-AREA-DSH-L30-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-REINVST-POLI-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-L30.

      *--> DCLGEN TABELLA
           MOVE REINVST-POLI-LQ         TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT  TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-L30-EX.
           EXIT.

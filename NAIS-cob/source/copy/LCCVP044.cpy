
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVP044
      *   ULTIMO AGG. 23 FEB 2011
      *------------------------------------------------------------

       INIZIA-TOT-P04.

           PERFORM INIZIA-ZEROES-P04 THRU INIZIA-ZEROES-P04-EX

           PERFORM INIZIA-SPACES-P04 THRU INIZIA-SPACES-P04-EX

           PERFORM INIZIA-NULL-P04 THRU INIZIA-NULL-P04-EX.

       INIZIA-TOT-P04-EX.
           EXIT.

       INIZIA-NULL-P04.
           MOVE HIGH-VALUES TO (SF)-FL-STAT-END-NULL
           MOVE HIGH-VALUES TO (SF)-TP-CAUS-SCARTO-NULL
           MOVE HIGH-VALUES TO (SF)-DESC-ERR
           MOVE HIGH-VALUES TO (SF)-COD-ERR-SCARTO-NULL
           MOVE HIGH-VALUES TO (SF)-ID-MOVI-CRZ-NULL.
       INIZIA-NULL-P04-EX.
           EXIT.

       INIZIA-ZEROES-P04.
           MOVE 0 TO (SF)-ID-STAT-RICH-EST
           MOVE 0 TO (SF)-ID-RICH-EST
           MOVE 0 TO (SF)-TS-INI-VLDT
           MOVE 0 TO (SF)-TS-END-VLDT
           MOVE 0 TO (SF)-COD-COMP-ANIA
           MOVE 0 TO (SF)-DS-VER
           MOVE 0 TO (SF)-DS-TS-CPTZ.
       INIZIA-ZEROES-P04-EX.
           EXIT.

       INIZIA-SPACES-P04.
           MOVE SPACES TO (SF)-COD-PRCS
           MOVE SPACES TO (SF)-COD-ATTVT
           MOVE SPACES TO (SF)-STAT-RICH-EST
           MOVE SPACES TO (SF)-DS-OPER-SQL
           MOVE SPACES TO (SF)-DS-UTENTE
           MOVE SPACES TO (SF)-DS-STATO-ELAB
           MOVE SPACES TO (SF)-UTENTE-INS-AGG.
       INIZIA-SPACES-P04-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY      ..... LCCVP846
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO ESTRATTO CONTO DIAGNOSTICO CED
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVP845 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN ESTRATTO CONTO DIAGN CED (LCCVP841)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-ESTR-CNT-DIAGN-CED.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE ESTR-CNT-DIAGN-CED.

      *--> NOME TABELLA FISICA DB
           MOVE 'ESTR-CNT-DIAGN-CED'            TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WP84-ST-INV(IX-TAB-P84)
              AND WP84-ELE-P84-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->       INSERIMENTO
                 WHEN WP84-ST-ADD(IX-TAB-P84)

      *-->          TIPO OPERAZIONE DISPATCHER
                    SET IDSI0011-INSERT   TO TRUE

      *-->       MODIFICA
                 WHEN WP84-ST-MOD(IX-TAB-P84)

      *-->          TIPO OPERAZIONE DISPATCHER
                    SET IDSI0011-UPDATE   TO TRUE

      *-->       CANCELLAZIONE
                 WHEN WP84-ST-DEL(IX-TAB-P84)

      *-->          TIPO OPERAZIONE DISPATCHER
                    SET IDSI0011-DELETE   TO TRUE

           END-EVALUATE

           IF IDSV0001-ESITO-OK

      *-->    VALORIZZA DCLGEN ESTRATTO CONTO DIAGN CE
              PERFORM VAL-DCLGEN-P84
                 THRU VAL-DCLGEN-P84-EX

      *-->    VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
              PERFORM VALORIZZA-AREA-DSH-P84
                 THRU VALORIZZA-AREA-DSH-P84-EX

      *-->    CALL DISPATCHER PER AGGIORNAMENTO
              PERFORM AGGIORNA-TABELLA
                 THRU AGGIORNA-TABELLA-EX

           END-IF.

       AGGIORNA-ESTR-CNT-DIAGN-CED-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-P84.

      *--> DCLGEN TABELLA
           MOVE ESTR-CNT-DIAGN-CED       TO IDSI0011-BUFFER-DATI.

           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-PRIMARY-KEY      TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZERO                     TO IDSI0011-DATA-FINE-EFFETTO
           MOVE ZERO                     TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-P84-EX.
           EXIT.

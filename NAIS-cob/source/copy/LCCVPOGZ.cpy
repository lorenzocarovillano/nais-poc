      *----------------------------------------------------------------*
      * DIMENSIONAMENTO OCCURS PER LA TAB. PARAM_OGG                   *
      * COPY RIPORTANTE IL NUMERO DI OCCURS DA UTILIZZARE              *
      * PER INIZIALIZZA TOT DELLA TABELLA                              *
      *----------------------------------------------------------------*
       01 WK-POG-MAX.
          03 WK-POG-MAX-A                 PIC 9(04) VALUE 100.
          03 WK-POG-MAX-B                 PIC 9(04) VALUE 200.

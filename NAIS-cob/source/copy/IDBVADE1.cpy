       01 ADES.
         05 ADE-ID-ADES PIC S9(9)V     COMP-3.
         05 ADE-ID-POLI PIC S9(9)V     COMP-3.
         05 ADE-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
         05 ADE-ID-MOVI-CHIU PIC S9(9)V     COMP-3.
         05 ADE-ID-MOVI-CHIU-NULL REDEFINES
            ADE-ID-MOVI-CHIU   PIC X(5).
         05 ADE-DT-INI-EFF   PIC S9(8)V COMP-3.
         05 ADE-DT-END-EFF   PIC S9(8)V COMP-3.
         05 ADE-IB-PREV PIC X(40).
         05 ADE-IB-PREV-NULL REDEFINES
            ADE-IB-PREV   PIC X(40).
         05 ADE-IB-OGG PIC X(40).
         05 ADE-IB-OGG-NULL REDEFINES
            ADE-IB-OGG   PIC X(40).
         05 ADE-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 ADE-DT-DECOR   PIC S9(8)V COMP-3.
         05 ADE-DT-DECOR-NULL REDEFINES
            ADE-DT-DECOR   PIC X(5).
         05 ADE-DT-SCAD   PIC S9(8)V COMP-3.
         05 ADE-DT-SCAD-NULL REDEFINES
            ADE-DT-SCAD   PIC X(5).
         05 ADE-ETA-A-SCAD PIC S9(5)V     COMP-3.
         05 ADE-ETA-A-SCAD-NULL REDEFINES
            ADE-ETA-A-SCAD   PIC X(3).
         05 ADE-DUR-AA PIC S9(5)V     COMP-3.
         05 ADE-DUR-AA-NULL REDEFINES
            ADE-DUR-AA   PIC X(3).
         05 ADE-DUR-MM PIC S9(5)V     COMP-3.
         05 ADE-DUR-MM-NULL REDEFINES
            ADE-DUR-MM   PIC X(3).
         05 ADE-DUR-GG PIC S9(5)V     COMP-3.
         05 ADE-DUR-GG-NULL REDEFINES
            ADE-DUR-GG   PIC X(3).
         05 ADE-TP-RGM-FISC PIC X(2).
         05 ADE-TP-RIAT PIC X(20).
         05 ADE-TP-RIAT-NULL REDEFINES
            ADE-TP-RIAT   PIC X(20).
         05 ADE-TP-MOD-PAG-TIT PIC X(2).
         05 ADE-TP-IAS PIC X(2).
         05 ADE-TP-IAS-NULL REDEFINES
            ADE-TP-IAS   PIC X(2).
         05 ADE-DT-VARZ-TP-IAS   PIC S9(8)V COMP-3.
         05 ADE-DT-VARZ-TP-IAS-NULL REDEFINES
            ADE-DT-VARZ-TP-IAS   PIC X(5).
         05 ADE-PRE-NET-IND PIC S9(12)V9(3) COMP-3.
         05 ADE-PRE-NET-IND-NULL REDEFINES
            ADE-PRE-NET-IND   PIC X(8).
         05 ADE-PRE-LRD-IND PIC S9(12)V9(3) COMP-3.
         05 ADE-PRE-LRD-IND-NULL REDEFINES
            ADE-PRE-LRD-IND   PIC X(8).
         05 ADE-RAT-LRD-IND PIC S9(12)V9(3) COMP-3.
         05 ADE-RAT-LRD-IND-NULL REDEFINES
            ADE-RAT-LRD-IND   PIC X(8).
         05 ADE-PRSTZ-INI-IND PIC S9(12)V9(3) COMP-3.
         05 ADE-PRSTZ-INI-IND-NULL REDEFINES
            ADE-PRSTZ-INI-IND   PIC X(8).
         05 ADE-FL-COINC-ASSTO PIC X(1).
         05 ADE-FL-COINC-ASSTO-NULL REDEFINES
            ADE-FL-COINC-ASSTO   PIC X(1).
         05 ADE-IB-DFLT PIC X(40).
         05 ADE-IB-DFLT-NULL REDEFINES
            ADE-IB-DFLT   PIC X(40).
         05 ADE-MOD-CALC PIC X(2).
         05 ADE-MOD-CALC-NULL REDEFINES
            ADE-MOD-CALC   PIC X(2).
         05 ADE-TP-FNT-CNBTVA PIC X(2).
         05 ADE-TP-FNT-CNBTVA-NULL REDEFINES
            ADE-TP-FNT-CNBTVA   PIC X(2).
         05 ADE-IMP-AZ PIC S9(12)V9(3) COMP-3.
         05 ADE-IMP-AZ-NULL REDEFINES
            ADE-IMP-AZ   PIC X(8).
         05 ADE-IMP-ADER PIC S9(12)V9(3) COMP-3.
         05 ADE-IMP-ADER-NULL REDEFINES
            ADE-IMP-ADER   PIC X(8).
         05 ADE-IMP-TFR PIC S9(12)V9(3) COMP-3.
         05 ADE-IMP-TFR-NULL REDEFINES
            ADE-IMP-TFR   PIC X(8).
         05 ADE-IMP-VOLO PIC S9(12)V9(3) COMP-3.
         05 ADE-IMP-VOLO-NULL REDEFINES
            ADE-IMP-VOLO   PIC X(8).
         05 ADE-PC-AZ PIC S9(3)V9(3) COMP-3.
         05 ADE-PC-AZ-NULL REDEFINES
            ADE-PC-AZ   PIC X(4).
         05 ADE-PC-ADER PIC S9(3)V9(3) COMP-3.
         05 ADE-PC-ADER-NULL REDEFINES
            ADE-PC-ADER   PIC X(4).
         05 ADE-PC-TFR PIC S9(3)V9(3) COMP-3.
         05 ADE-PC-TFR-NULL REDEFINES
            ADE-PC-TFR   PIC X(4).
         05 ADE-PC-VOLO PIC S9(3)V9(3) COMP-3.
         05 ADE-PC-VOLO-NULL REDEFINES
            ADE-PC-VOLO   PIC X(4).
         05 ADE-DT-NOVA-RGM-FISC   PIC S9(8)V COMP-3.
         05 ADE-DT-NOVA-RGM-FISC-NULL REDEFINES
            ADE-DT-NOVA-RGM-FISC   PIC X(5).
         05 ADE-FL-ATTIV PIC X(1).
         05 ADE-FL-ATTIV-NULL REDEFINES
            ADE-FL-ATTIV   PIC X(1).
         05 ADE-IMP-REC-RIT-VIS PIC S9(12)V9(3) COMP-3.
         05 ADE-IMP-REC-RIT-VIS-NULL REDEFINES
            ADE-IMP-REC-RIT-VIS   PIC X(8).
         05 ADE-IMP-REC-RIT-ACC PIC S9(12)V9(3) COMP-3.
         05 ADE-IMP-REC-RIT-ACC-NULL REDEFINES
            ADE-IMP-REC-RIT-ACC   PIC X(8).
         05 ADE-FL-VARZ-STAT-TBGC PIC X(1).
         05 ADE-FL-VARZ-STAT-TBGC-NULL REDEFINES
            ADE-FL-VARZ-STAT-TBGC   PIC X(1).
         05 ADE-FL-PROVZA-MIGRAZ PIC X(1).
         05 ADE-FL-PROVZA-MIGRAZ-NULL REDEFINES
            ADE-FL-PROVZA-MIGRAZ   PIC X(1).
         05 ADE-IMPB-VIS-DA-REC PIC S9(12)V9(3) COMP-3.
         05 ADE-IMPB-VIS-DA-REC-NULL REDEFINES
            ADE-IMPB-VIS-DA-REC   PIC X(8).
         05 ADE-DT-DECOR-PREST-BAN   PIC S9(8)V COMP-3.
         05 ADE-DT-DECOR-PREST-BAN-NULL REDEFINES
            ADE-DT-DECOR-PREST-BAN   PIC X(5).
         05 ADE-DT-EFF-VARZ-STAT-T   PIC S9(8)V COMP-3.
         05 ADE-DT-EFF-VARZ-STAT-T-NULL REDEFINES
            ADE-DT-EFF-VARZ-STAT-T   PIC X(5).
         05 ADE-DS-RIGA PIC S9(10)V     COMP-3.
         05 ADE-DS-OPER-SQL PIC X(1).
         05 ADE-DS-VER PIC S9(9)V     COMP-3.
         05 ADE-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
         05 ADE-DS-TS-END-CPTZ PIC S9(18)V     COMP-3.
         05 ADE-DS-UTENTE PIC X(20).
         05 ADE-DS-STATO-ELAB PIC X(1).
         05 ADE-CUM-CNBT-CAP PIC S9(12)V9(3) COMP-3.
         05 ADE-CUM-CNBT-CAP-NULL REDEFINES
            ADE-CUM-CNBT-CAP   PIC X(8).
         05 ADE-IMP-GAR-CNBT PIC S9(12)V9(3) COMP-3.
         05 ADE-IMP-GAR-CNBT-NULL REDEFINES
            ADE-IMP-GAR-CNBT   PIC X(8).
         05 ADE-DT-ULT-CONS-CNBT   PIC S9(8)V COMP-3.
         05 ADE-DT-ULT-CONS-CNBT-NULL REDEFINES
            ADE-DT-ULT-CONS-CNBT   PIC X(5).
         05 ADE-IDEN-ISC-FND PIC X(40).
         05 ADE-IDEN-ISC-FND-NULL REDEFINES
            ADE-IDEN-ISC-FND   PIC X(40).
         05 ADE-NUM-RAT-PIAN PIC S9(7)V9(5) COMP-3.
         05 ADE-NUM-RAT-PIAN-NULL REDEFINES
            ADE-NUM-RAT-PIAN   PIC X(7).
         05 ADE-DT-PRESC   PIC S9(8)V COMP-3.
         05 ADE-DT-PRESC-NULL REDEFINES
            ADE-DT-PRESC   PIC X(5).
         05 ADE-CONCS-PREST PIC X(1).
         05 ADE-CONCS-PREST-NULL REDEFINES
            ADE-CONCS-PREST   PIC X(1).


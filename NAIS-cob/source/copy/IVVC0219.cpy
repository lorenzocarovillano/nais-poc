      *----------------------------------------------------------------*
      *   VALORIZZA  TAB. OUTPUT
      *   LA SEGUENTE COPY VIENE UTLIZZARE PER GESTIRE LA VALORIZZAZIONE
      *   PARAMETRICA DI TUTTE LE AREE DA VALORIZZARE IN OUTPUT
      *----------------------------------------------------------------*
       S1600-VALORIZZA-OUTPUT.
      *
      *--> GESIONE ADESIONE
           IF WKS-NOME-TABELLA = 'ADES'
              ADD 1                         TO IX-TAB-ADE
              PERFORM VALORIZZA-OUTPUT-ADE  THRU VALORIZZA-OUTPUT-ADE-EX
           END-IF.
      *
      *--> GESIONE BENEFICIARIO
           IF WKS-NOME-TABELLA = 'BNFIC'
              ADD 1                         TO IX-TAB-BEP
              PERFORM VALORIZZA-OUTPUT-BEP  THRU VALORIZZA-OUTPUT-BEP-EX
           END-IF.
      *
      *--> GESIONE DATI-COLLETTIVA
           IF WKS-NOME-TABELLA = 'D-COLL'
              ADD 1                         TO IX-TAB-DCO
              PERFORM VALORIZZA-OUTPUT-DCO  THRU VALORIZZA-OUTPUT-DCO-EX
           END-IF.
      *
      *--> GESIONE DATO-FISCALE-ADESIONE
           IF WKS-NOME-TABELLA = 'D-FISC-ADES'
              ADD 1                         TO IX-TAB-DFA
              PERFORM VALORIZZA-OUTPUT-DFA  THRU VALORIZZA-OUTPUT-DFA-EX
           END-IF.
      *
      *--> GESIONE DETTAGLIO-QUESTIONARIO
           IF WKS-NOME-TABELLA = 'DETT-QUEST'
              ADD 1                         TO IX-TAB-DEQ
              PERFORM VALORIZZA-OUTPUT-DEQ  THRU VALORIZZA-OUTPUT-DEQ-EX
           END-IF.
      *
      *--> GESIONE DETTAGLIO-TITOLO-CONTABIL
           IF WKS-NOME-TABELLA = 'DETT-TIT-CONT'
              ADD 1                         TO IX-TAB-DTC
              PERFORM VALORIZZA-OUTPUT-DTC  THRU VALORIZZA-OUTPUT-DTC-EX
           END-IF.
      *
      *--> GESIONE GARANZIA
           IF WKS-NOME-TABELLA = 'GAR'
              ADD 1                         TO IX-TAB-GRZ
              PERFORM VALORIZZA-OUTPUT-GRZ  THRU VALORIZZA-OUTPUT-GRZ-EX
           END-IF.
      *
      *--> GESIONE MOVIMENTO
           IF WKS-NOME-TABELLA = 'MOVI'
              ADD 1                         TO IX-TAB-MOV
              PERFORM VALORIZZA-OUTPUT-MOV  THRU VALORIZZA-OUTPUT-MOV-EX
           END-IF.
      *
      *--> GESIONE PARAMETRO-MOVIMENTO
           IF WKS-NOME-TABELLA = 'PARAM-MOVI'
              ADD 1                         TO IX-TAB-PMO
              PERFORM VALORIZZA-OUTPUT-PMO  THRU VALORIZZA-OUTPUT-PMO-EX
           END-IF.
      *
      *--> GESIONE PARAMETRO-OGGETTO
           IF WKS-NOME-TABELLA = 'PARAM-OGG'
              ADD 1                         TO IX-TAB-POG
              PERFORM VALORIZZA-OUTPUT-POG  THRU VALORIZZA-OUTPUT-POG-EX
           END-IF.
      *
      *--> GESIONE POLIZZA
           IF WKS-NOME-TABELLA = 'POLI'
              ADD 1                         TO IX-TAB-POL
              PERFORM VALORIZZA-OUTPUT-POL  THRU VALORIZZA-OUTPUT-POL-EX
           END-IF.
      *
      *--> GESIONE PROVVIGIONE-DI-TRANCHE
           IF WKS-NOME-TABELLA = 'PROV-DI-GAR'
              ADD 1                         TO IX-TAB-PVT
              PERFORM VALORIZZA-OUTPUT-PVT  THRU VALORIZZA-OUTPUT-PVT-EX
           END-IF.
      *
      *--> GESIONE QUESTIONARIO
           IF WKS-NOME-TABELLA = 'QUEST'
              ADD 1                         TO IX-TAB-QUE
              PERFORM VALORIZZA-OUTPUT-QUE  THRU VALORIZZA-OUTPUT-QUE-EX
           END-IF.
      *
      *--> GESIONE RAPPORTO-ANAGRAFICO
           IF WKS-NOME-TABELLA = 'RAPP-ANA'
              ADD 1                         TO IX-TAB-RAN
              PERFORM VALORIZZA-OUTPUT-RAN  THRU VALORIZZA-OUTPUT-RAN-EX
           END-IF.

      *--> GESIONE EST RAPPORTO-ANAGRAFICO
           IF WKS-NOME-TABELLA = 'EST-RAPP-ANA'
              ADD 1                         TO IX-TAB-E15
              PERFORM VALORIZZA-OUTPUT-E15  THRU VALORIZZA-OUTPUT-E15-EX
           END-IF.

      *
      *--> GESIONE RAPPORTO-RETE
           IF WKS-NOME-TABELLA = 'RAPP-RETE'
              ADD 1                         TO IX-TAB-RRE
              PERFORM VALORIZZA-OUTPUT-RRE  THRU VALORIZZA-OUTPUT-RRE-EX
           END-IF.
      *
      *--> GESIONE RICHIESTA
           IF WKS-NOME-TABELLA = 'RICH'
              ADD 1                         TO IX-TAB-RIC
              PERFORM VALORIZZA-OUTPUT-RIC  THRU VALORIZZA-OUTPUT-RIC-EX
           END-IF.
      *
      *--> GESIONE SOPRAPREMIO-DI-GARANZIA
           IF WKS-NOME-TABELLA = 'SOPR-DI-GAR'
              ADD 1                         TO IX-TAB-SPG
              PERFORM VALORIZZA-OUTPUT-SPG THRU VALORIZZA-OUTPUT-SPG-EX
           END-IF.

      *--> GESIONE STRATEGIA-DI-INVESTIMENTO
           IF WKS-NOME-TABELLA = 'STRA-DI-INVST'
              ADD 1                         TO IX-TAB-SDI
              PERFORM VALORIZZA-OUTPUT-SDI  THRU VALORIZZA-OUTPUT-SDI-EX
           END-IF.
      *
      *--> GESIONE TITOLO-CONTABILE
           IF WKS-NOME-TABELLA = 'TIT-CONT'
              ADD 1                         TO IX-TAB-TIT
              PERFORM VALORIZZA-OUTPUT-TIT  THRU VALORIZZA-OUTPUT-TIT-EX
           END-IF.
      *
      *--> GESIONE TRANCHE-DI-GARANZIA
           IF WKS-NOME-TABELLA = 'TRCH-DI-GAR'
              ADD 1                         TO IX-TAB-TGA
              PERFORM VALORIZZA-OUTPUT-TGA  THRU VALORIZZA-OUTPUT-TGA-EX
           END-IF.
      *
      *--> GESIONE TRANCHE-LIQUIDAZIONE
           IF WKS-NOME-TABELLA = 'TRCH-LIQ'
              ADD 1              TO IX-TAB-TLI
              PERFORM VALORIZZA-OUTPUT-TLI  THRU VALORIZZA-OUTPUT-TLI-EX
           END-IF.
      *
      *--> GESIONE LIQUIDAZIONE
           IF WKS-NOME-TABELLA = 'LIQ'
              ADD 1              TO IX-TAB-LQU
              PERFORM VALORIZZA-OUTPUT-LQU  THRU VALORIZZA-OUTPUT-LQU-EX
           END-IF.
      *
      *--> GESIONE PRESTITO
           IF WKS-NOME-TABELLA = 'PREST'
              ADD 1              TO IX-TAB-PRE
              PERFORM VALORIZZA-OUTPUT-PRE  THRU VALORIZZA-OUTPUT-PRE-EX
           END-IF.
      *
      *--> GESIONE BENEFICIARIO-LIQUIDAZIONE
           IF WKS-NOME-TABELLA = 'BNFICR-LIQ'
              ADD 1              TO IX-TAB-BEL
              PERFORM VALORIZZA-OUTPUT-BEL  THRU VALORIZZA-OUTPUT-BEL-EX
           END-IF.
      *
      *--> GESIONE OGGETTO-COLLEGATO
           IF WKS-NOME-TABELLA = 'OGG-COLLG'
              ADD 1              TO IX-TAB-OCO
              PERFORM VALORIZZA-OUTPUT-OCO  THRU VALORIZZA-OUTPUT-OCO-EX
           END-IF.
      *
      *--> GESIONE DEFAULT_DESIONE
           IF WKS-NOME-TABELLA = 'DFLT-ADES'
              ADD 1              TO IX-TAB-DAD
              PERFORM VALORIZZA-OUTPUT-DAD  THRU VALORIZZA-OUTPUT-DAD-EX
           END-IF.
      *
      *--> GESIONE MOVIMENTO FINANZIARIO
           IF WKS-NOME-TABELLA = 'MOVI-FINRIO'
              ADD 1              TO IX-TAB-MFZ
              PERFORM VALORIZZA-OUTPUT-MFZ  THRU VALORIZZA-OUTPUT-MFZ-EX
           END-IF.

      *--> GESIONE RICHIESTA INVESTIMENTO FONDI
           IF WKS-NOME-TABELLA = 'RICH-INVST-FND'
              ADD 1              TO IX-TAB-RIF
              PERFORM VALORIZZA-OUTPUT-RIF  THRU VALORIZZA-OUTPUT-RIF-EX
           END-IF.

      *--> GESIONE PERCIPIENTE LIQUIDAZIONE
           IF WKS-NOME-TABELLA = 'PERC-LIQ'
              ADD 1              TO IX-TAB-PLI
              PERFORM VALORIZZA-OUTPUT-PLI  THRU VALORIZZA-OUTPUT-PLI-EX
           END-IF.

      *--> GESIONE GARANZIA LIQUIDAZIONE
           IF WKS-NOME-TABELLA = 'GAR-LIQ'
              ADD 1              TO IX-TAB-GRL
              PERFORM VALORIZZA-OUTPUT-GRL  THRU VALORIZZA-OUTPUT-GRL-EX
           END-IF.

      *--> GESIONE DATI RIPRESA LIQUIDAZIONE
            IF WKS-NOME-TABELLA = 'D-FORZ-LIQ'
               ADD 1              TO IX-TAB-DFL
               PERFORM VALORIZZA-OUTPUT-DFL THRU VALORIZZA-OUTPUT-DFL-EX
            END-IF.

      *--> GESIONE DATI RIPRESA LIQUIDAZIONE
            IF WKS-NOME-TABELLA = 'IMPST-SOST'
               ADD 1              TO IX-TAB-ISO
               PERFORM VALORIZZA-OUTPUT-ISO THRU VALORIZZA-OUTPUT-ISO-EX
            END-IF.
      *
      *--> GESIONE DATI RIPRESA LIQUIDAZIONE
            IF WKS-NOME-TABELLA = 'PARAM-COMP'
               ADD 1              TO IX-TAB-PCO
               PERFORM VALORIZZA-OUTPUT-PCO THRU VALORIZZA-OUTPUT-PCO-EX
            END-IF.
      *
      *--> GESIONE DATI RIPRESA LIQUIDAZIONE
            IF WKS-NOME-TABELLA = 'RIS-DI-TRCH'
               ADD 1              TO IX-TAB-RST
               PERFORM VALORIZZA-OUTPUT-RST THRU VALORIZZA-OUTPUT-RST-EX
            END-IF.
      *
      *--> GESIONE ESTENSIONE DI TRCH
            IF WKS-NOME-TABELLA = 'EST-TRCH-DI-GAR'
               ADD 1              TO IX-TAB-E12
               PERFORM VALORIZZA-OUTPUT-E12 THRU VALORIZZA-OUTPUT-E12-EX
            END-IF.
      *
      *--> GESIONE REINVESTIMENTO POLIZZE LIQUIDATE
            IF WKS-NOME-TABELLA = 'REINVST-POLI-LQ'
               ADD 1              TO IX-TAB-L30
               PERFORM VALORIZZA-OUTPUT-L30 THRU VALORIZZA-OUTPUT-L30-EX
            END-IF.

      *--> GESIONE VINCOLO E PEGNO
            IF WKS-NOME-TABELLA = 'VINC-PEG'
               ADD 1              TO IX-TAB-L23
               PERFORM VALORIZZA-OUTPUT-L23 THRU VALORIZZA-OUTPUT-L23-EX
            END-IF.
      *--> GESIONE DATI CRISTALLIZZATI
            IF WKS-NOME-TABELLA = 'D-CRIST'
               ADD 1              TO IX-TAB-P61
               PERFORM VALORIZZA-OUTPUT-P61 THRU VALORIZZA-OUTPUT-P61-EX
            END-IF.
      *--> DATI AGGIUNTIVI PER POLIZZE CPI
            IF WKS-NOME-TABELLA = 'EST-POLI-CPI-PR'
               ADD 1              TO IX-TAB-P67
               PERFORM VALORIZZA-OUTPUT-P67 THRU VALORIZZA-OUTPUT-P67-EX
            END-IF.
      *--> RICHIESTA ESTERNA
            IF WKS-NOME-TABELLA = 'RICH-EST'
               ADD 1              TO IX-TAB-P01
               PERFORM VALORIZZA-OUTPUT-P01 THRU VALORIZZA-OUTPUT-P01-EX
            END-IF.
      *
       S1600-EX.
           EXIT.

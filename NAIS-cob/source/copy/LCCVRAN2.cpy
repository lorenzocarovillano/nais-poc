      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **    LETTURA RAPP_ANA                                          **
      **                                                              **
      ******************************************************************

      *----------------------------------------------------------------*
      *               GESTIONE LETTURA (SELECT/FETCH)                  *
      *----------------------------------------------------------------*
       LETTURA-RAN.

           IF IDSI0011-SELECT
              PERFORM SELECT-RAN
                 THRU SELECT-RAN-EX
           ELSE
              PERFORM FETCH-RAN
                 THRU FETCH-RAN-EX
           END-IF.

           MOVE IX-TAB-RAN
             TO (SF)-ELE-RAPP-ANAG-MAX.

       LETTURA-RAN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         SELECT                                 *
      *----------------------------------------------------------------*
       SELECT-RAN.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--->       OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI
                         TO RAPP-ANA
                       MOVE 1              TO IX-TAB-RAN
                       PERFORM VALORIZZA-OUTPUT-RAN
                          THRU VALORIZZA-OUTPUT-RAN-EX

                  WHEN IDSO0011-NOT-FOUND
      *--->       CHIAVE NON TROVATA
                       MOVE WK-PGM         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'SELECT-RAN'   TO IEAI9901-LABEL-ERR
                       MOVE '005017'       TO IEAI9901-COD-ERRORE
                       MOVE SPACES         TO IEAI9901-PARAMETRI-ERR
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                       MOVE WK-PGM     TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'SELECT-RAN'
                                       TO IEAI9901-LABEL-ERR
                       MOVE '005016'   TO IEAI9901-COD-ERRORE
                       STRING 'RAPP-ANA'           ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                       DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM          TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SELECT-RAN'    TO IEAI9901-LABEL-ERR
              MOVE '005016'        TO IEAI9901-COD-ERRORE
              STRING 'RAPP-ANA'           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       SELECT-RAN-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         FETCH                                  *
      *----------------------------------------------------------------*
       FETCH-RAN.

           SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
           SET  WCOM-OVERFLOW-NO                  TO TRUE.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR WCOM-OVERFLOW-YES

               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *-->        CHIAVE NON TROVATA
                           IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM
                                TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'FETCH-RAN'
                                TO IEAI9901-LABEL-ERR
                              MOVE '005017'
                                TO IEAI9901-COD-ERRORE
                              MOVE SPACES
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                           END-IF
                      WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                           MOVE IDSO0011-BUFFER-DATI  TO RAPP-ANA
                           ADD  1                     TO IX-TAB-RAN

      *-->  Se il contatore di lettura � maggiore dell' elemento MAX
      *-->  previsto per la TAB.RAPP-ANAG allora siamo in overflow
                           IF IX-TAB-RAN > (SF)-ELE-RAPP-ANAG-MAX
                              SET WCOM-OVERFLOW-YES   TO TRUE
                           ELSE
                              PERFORM VALORIZZA-OUTPUT-RAN
                                 THRU VALORIZZA-OUTPUT-RAN-EX
                              SET IDSI0011-FETCH-NEXT TO TRUE
                           END-IF
                      WHEN OTHER
      *-->        ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'FETCH-RAN'  TO IEAI9901-LABEL-ERR
                           MOVE '005016'     TO IEAI9901-COD-ERRORE
                           STRING 'CUM-OGG'            ';'
                                  IDSO0011-RETURN-CODE ';'
                                  IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      *-->        ERRORE DISPATCHER
                  MOVE WK-PGM          TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'FETCH-RAN'     TO IEAI9901-LABEL-ERR
                  MOVE '005016'        TO IEAI9901-COD-ERRORE
                  STRING 'CUM-OGG'            ';'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

       FETCH-RAN-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVPVT3
      *   ULTIMO AGG. 21 NOV 2013
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-PVT.
           MOVE PVT-ID-PROV-DI-GAR
             TO (SF)-ID-PTF(IX-TAB-PVT)
           MOVE PVT-ID-PROV-DI-GAR
             TO (SF)-ID-PROV-DI-GAR(IX-TAB-PVT)
           IF PVT-ID-GAR-NULL = HIGH-VALUES
              MOVE PVT-ID-GAR-NULL
                TO (SF)-ID-GAR-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-ID-GAR
                TO (SF)-ID-GAR(IX-TAB-PVT)
           END-IF
           MOVE PVT-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-PVT)
           IF PVT-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE PVT-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-PVT)
           END-IF
           MOVE PVT-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-PVT)
           MOVE PVT-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-PVT)
           MOVE PVT-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-PVT)
           IF PVT-TP-PROV-NULL = HIGH-VALUES
              MOVE PVT-TP-PROV-NULL
                TO (SF)-TP-PROV-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-TP-PROV
                TO (SF)-TP-PROV(IX-TAB-PVT)
           END-IF
           IF PVT-PROV-1AA-ACQ-NULL = HIGH-VALUES
              MOVE PVT-PROV-1AA-ACQ-NULL
                TO (SF)-PROV-1AA-ACQ-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-PROV-1AA-ACQ
                TO (SF)-PROV-1AA-ACQ(IX-TAB-PVT)
           END-IF
           IF PVT-PROV-2AA-ACQ-NULL = HIGH-VALUES
              MOVE PVT-PROV-2AA-ACQ-NULL
                TO (SF)-PROV-2AA-ACQ-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-PROV-2AA-ACQ
                TO (SF)-PROV-2AA-ACQ(IX-TAB-PVT)
           END-IF
           IF PVT-PROV-RICOR-NULL = HIGH-VALUES
              MOVE PVT-PROV-RICOR-NULL
                TO (SF)-PROV-RICOR-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-PROV-RICOR
                TO (SF)-PROV-RICOR(IX-TAB-PVT)
           END-IF
           IF PVT-PROV-INC-NULL = HIGH-VALUES
              MOVE PVT-PROV-INC-NULL
                TO (SF)-PROV-INC-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-PROV-INC
                TO (SF)-PROV-INC(IX-TAB-PVT)
           END-IF
           IF PVT-ALQ-PROV-ACQ-NULL = HIGH-VALUES
              MOVE PVT-ALQ-PROV-ACQ-NULL
                TO (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-ALQ-PROV-ACQ
                TO (SF)-ALQ-PROV-ACQ(IX-TAB-PVT)
           END-IF
           IF PVT-ALQ-PROV-INC-NULL = HIGH-VALUES
              MOVE PVT-ALQ-PROV-INC-NULL
                TO (SF)-ALQ-PROV-INC-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-ALQ-PROV-INC
                TO (SF)-ALQ-PROV-INC(IX-TAB-PVT)
           END-IF
           IF PVT-ALQ-PROV-RICOR-NULL = HIGH-VALUES
              MOVE PVT-ALQ-PROV-RICOR-NULL
                TO (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-ALQ-PROV-RICOR
                TO (SF)-ALQ-PROV-RICOR(IX-TAB-PVT)
           END-IF
           IF PVT-FL-STOR-PROV-ACQ-NULL = HIGH-VALUES
              MOVE PVT-FL-STOR-PROV-ACQ-NULL
                TO (SF)-FL-STOR-PROV-ACQ-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-FL-STOR-PROV-ACQ
                TO (SF)-FL-STOR-PROV-ACQ(IX-TAB-PVT)
           END-IF
           IF PVT-FL-REC-PROV-STORN-NULL = HIGH-VALUES
              MOVE PVT-FL-REC-PROV-STORN-NULL
                TO (SF)-FL-REC-PROV-STORN-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-FL-REC-PROV-STORN
                TO (SF)-FL-REC-PROV-STORN(IX-TAB-PVT)
           END-IF
           IF PVT-FL-PROV-FORZ-NULL = HIGH-VALUES
              MOVE PVT-FL-PROV-FORZ-NULL
                TO (SF)-FL-PROV-FORZ-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-FL-PROV-FORZ
                TO (SF)-FL-PROV-FORZ(IX-TAB-PVT)
           END-IF
           IF PVT-TP-CALC-PROV-NULL = HIGH-VALUES
              MOVE PVT-TP-CALC-PROV-NULL
                TO (SF)-TP-CALC-PROV-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-TP-CALC-PROV
                TO (SF)-TP-CALC-PROV(IX-TAB-PVT)
           END-IF
           MOVE PVT-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-PVT)
           MOVE PVT-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-PVT)
           MOVE PVT-DS-VER
             TO (SF)-DS-VER(IX-TAB-PVT)
           MOVE PVT-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-PVT)
           MOVE PVT-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-PVT)
           MOVE PVT-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-PVT)
           MOVE PVT-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-PVT)
           IF PVT-REMUN-ASS-NULL = HIGH-VALUES
              MOVE PVT-REMUN-ASS-NULL
                TO (SF)-REMUN-ASS-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-REMUN-ASS
                TO (SF)-REMUN-ASS(IX-TAB-PVT)
           END-IF
           IF PVT-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE PVT-COMMIS-INTER-NULL
                TO (SF)-COMMIS-INTER-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-COMMIS-INTER
                TO (SF)-COMMIS-INTER(IX-TAB-PVT)
           END-IF
           IF PVT-ALQ-REMUN-ASS-NULL = HIGH-VALUES
              MOVE PVT-ALQ-REMUN-ASS-NULL
                TO (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-ALQ-REMUN-ASS
                TO (SF)-ALQ-REMUN-ASS(IX-TAB-PVT)
           END-IF
           IF PVT-ALQ-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE PVT-ALQ-COMMIS-INTER-NULL
                TO (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-PVT)
           ELSE
              MOVE PVT-ALQ-COMMIS-INTER
                TO (SF)-ALQ-COMMIS-INTER(IX-TAB-PVT)
           END-IF.
       VALORIZZA-OUTPUT-PVT-EX.
           EXIT.

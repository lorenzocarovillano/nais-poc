
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVRIC3
      *   ULTIMO AGG. 21 NOV 2013
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-RIC.
           MOVE RIC-ID-RICH
             TO (SF)-ID-PTF
           MOVE RIC-ID-RICH
             TO (SF)-ID-RICH
           MOVE RIC-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE RIC-TP-RICH
             TO (SF)-TP-RICH
           MOVE RIC-COD-MACROFUNCT
             TO (SF)-COD-MACROFUNCT
           MOVE RIC-TP-MOVI
             TO (SF)-TP-MOVI
           IF RIC-IB-RICH-NULL = HIGH-VALUES
              MOVE RIC-IB-RICH-NULL
                TO (SF)-IB-RICH-NULL
           ELSE
              MOVE RIC-IB-RICH
                TO (SF)-IB-RICH
           END-IF
           MOVE RIC-DT-EFF
             TO (SF)-DT-EFF
           MOVE RIC-DT-RGSTRZ-RICH
             TO (SF)-DT-RGSTRZ-RICH
           MOVE RIC-DT-PERV-RICH
             TO (SF)-DT-PERV-RICH
           MOVE RIC-DT-ESEC-RICH
             TO (SF)-DT-ESEC-RICH
           IF RIC-TS-EFF-ESEC-RICH-NULL = HIGH-VALUES
              MOVE RIC-TS-EFF-ESEC-RICH-NULL
                TO (SF)-TS-EFF-ESEC-RICH-NULL
           ELSE
              MOVE RIC-TS-EFF-ESEC-RICH
                TO (SF)-TS-EFF-ESEC-RICH
           END-IF
           IF RIC-ID-OGG-NULL = HIGH-VALUES
              MOVE RIC-ID-OGG-NULL
                TO (SF)-ID-OGG-NULL
           ELSE
              MOVE RIC-ID-OGG
                TO (SF)-ID-OGG
           END-IF
           IF RIC-TP-OGG-NULL = HIGH-VALUES
              MOVE RIC-TP-OGG-NULL
                TO (SF)-TP-OGG-NULL
           ELSE
              MOVE RIC-TP-OGG
                TO (SF)-TP-OGG
           END-IF
           IF RIC-IB-POLI-NULL = HIGH-VALUES
              MOVE RIC-IB-POLI-NULL
                TO (SF)-IB-POLI-NULL
           ELSE
              MOVE RIC-IB-POLI
                TO (SF)-IB-POLI
           END-IF
           IF RIC-IB-ADES-NULL = HIGH-VALUES
              MOVE RIC-IB-ADES-NULL
                TO (SF)-IB-ADES-NULL
           ELSE
              MOVE RIC-IB-ADES
                TO (SF)-IB-ADES
           END-IF
           IF RIC-IB-GAR-NULL = HIGH-VALUES
              MOVE RIC-IB-GAR-NULL
                TO (SF)-IB-GAR-NULL
           ELSE
              MOVE RIC-IB-GAR
                TO (SF)-IB-GAR
           END-IF
           IF RIC-IB-TRCH-DI-GAR-NULL = HIGH-VALUES
              MOVE RIC-IB-TRCH-DI-GAR-NULL
                TO (SF)-IB-TRCH-DI-GAR-NULL
           ELSE
              MOVE RIC-IB-TRCH-DI-GAR
                TO (SF)-IB-TRCH-DI-GAR
           END-IF
           IF RIC-ID-BATCH-NULL = HIGH-VALUES
              MOVE RIC-ID-BATCH-NULL
                TO (SF)-ID-BATCH-NULL
           ELSE
              MOVE RIC-ID-BATCH
                TO (SF)-ID-BATCH
           END-IF
           IF RIC-ID-JOB-NULL = HIGH-VALUES
              MOVE RIC-ID-JOB-NULL
                TO (SF)-ID-JOB-NULL
           ELSE
              MOVE RIC-ID-JOB
                TO (SF)-ID-JOB
           END-IF
           IF RIC-FL-SIMULAZIONE-NULL = HIGH-VALUES
              MOVE RIC-FL-SIMULAZIONE-NULL
                TO (SF)-FL-SIMULAZIONE-NULL
           ELSE
              MOVE RIC-FL-SIMULAZIONE
                TO (SF)-FL-SIMULAZIONE
           END-IF
           MOVE RIC-KEY-ORDINAMENTO
             TO (SF)-KEY-ORDINAMENTO
           IF RIC-ID-RICH-COLLG-NULL = HIGH-VALUES
              MOVE RIC-ID-RICH-COLLG-NULL
                TO (SF)-ID-RICH-COLLG-NULL
           ELSE
              MOVE RIC-ID-RICH-COLLG
                TO (SF)-ID-RICH-COLLG
           END-IF
           IF RIC-TP-RAMO-BILA-NULL = HIGH-VALUES
              MOVE RIC-TP-RAMO-BILA-NULL
                TO (SF)-TP-RAMO-BILA-NULL
           ELSE
              MOVE RIC-TP-RAMO-BILA
                TO (SF)-TP-RAMO-BILA
           END-IF
           IF RIC-TP-FRM-ASSVA-NULL = HIGH-VALUES
              MOVE RIC-TP-FRM-ASSVA-NULL
                TO (SF)-TP-FRM-ASSVA-NULL
           ELSE
              MOVE RIC-TP-FRM-ASSVA
                TO (SF)-TP-FRM-ASSVA
           END-IF
           IF RIC-TP-CALC-RIS-NULL = HIGH-VALUES
              MOVE RIC-TP-CALC-RIS-NULL
                TO (SF)-TP-CALC-RIS-NULL
           ELSE
              MOVE RIC-TP-CALC-RIS
                TO (SF)-TP-CALC-RIS
           END-IF
           MOVE RIC-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE RIC-DS-VER
             TO (SF)-DS-VER
           MOVE RIC-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE RIC-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE RIC-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           IF RIC-RAMO-BILA-NULL = HIGH-VALUES
              MOVE RIC-RAMO-BILA-NULL
                TO (SF)-RAMO-BILA-NULL
           ELSE
              MOVE RIC-RAMO-BILA
                TO (SF)-RAMO-BILA
           END-IF.
       VALORIZZA-OUTPUT-RIC-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVL233
      *   ULTIMO AGG. 31 MAG 2010
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-L23.
           MOVE L23-ID-VINC-PEG
             TO (SF)-ID-PTF(IX-TAB-L23)
           MOVE L23-ID-VINC-PEG
             TO (SF)-ID-VINC-PEG(IX-TAB-L23)
           MOVE L23-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-L23)
           IF L23-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE L23-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-L23)
           END-IF
           MOVE L23-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-L23)
           MOVE L23-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-L23)
           MOVE L23-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-L23)
           MOVE L23-ID-RAPP-ANA
             TO (SF)-ID-RAPP-ANA(IX-TAB-L23)
           IF L23-TP-VINC-NULL = HIGH-VALUES
              MOVE L23-TP-VINC-NULL
                TO (SF)-TP-VINC-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-TP-VINC
                TO (SF)-TP-VINC(IX-TAB-L23)
           END-IF
           IF L23-FL-DELEGA-AL-RISC-NULL = HIGH-VALUES
              MOVE L23-FL-DELEGA-AL-RISC-NULL
                TO (SF)-FL-DELEGA-AL-RISC-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-FL-DELEGA-AL-RISC
                TO (SF)-FL-DELEGA-AL-RISC(IX-TAB-L23)
           END-IF
           MOVE L23-DESC
             TO (SF)-DESC(IX-TAB-L23)
           IF L23-DT-ATTIV-VINPG-NULL = HIGH-VALUES
              MOVE L23-DT-ATTIV-VINPG-NULL
                TO (SF)-DT-ATTIV-VINPG-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-DT-ATTIV-VINPG
                TO (SF)-DT-ATTIV-VINPG(IX-TAB-L23)
           END-IF
           IF L23-CPT-VINCTO-PIGN-NULL = HIGH-VALUES
              MOVE L23-CPT-VINCTO-PIGN-NULL
                TO (SF)-CPT-VINCTO-PIGN-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-CPT-VINCTO-PIGN
                TO (SF)-CPT-VINCTO-PIGN(IX-TAB-L23)
           END-IF
           IF L23-DT-CHIU-VINPG-NULL = HIGH-VALUES
              MOVE L23-DT-CHIU-VINPG-NULL
                TO (SF)-DT-CHIU-VINPG-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-DT-CHIU-VINPG
                TO (SF)-DT-CHIU-VINPG(IX-TAB-L23)
           END-IF
           IF L23-VAL-RISC-INI-VINPG-NULL = HIGH-VALUES
              MOVE L23-VAL-RISC-INI-VINPG-NULL
                TO (SF)-VAL-RISC-INI-VINPG-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-VAL-RISC-INI-VINPG
                TO (SF)-VAL-RISC-INI-VINPG(IX-TAB-L23)
           END-IF
           IF L23-VAL-RISC-END-VINPG-NULL = HIGH-VALUES
              MOVE L23-VAL-RISC-END-VINPG-NULL
                TO (SF)-VAL-RISC-END-VINPG-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-VAL-RISC-END-VINPG
                TO (SF)-VAL-RISC-END-VINPG(IX-TAB-L23)
           END-IF
           IF L23-SOM-PRE-VINPG-NULL = HIGH-VALUES
              MOVE L23-SOM-PRE-VINPG-NULL
                TO (SF)-SOM-PRE-VINPG-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-SOM-PRE-VINPG
                TO (SF)-SOM-PRE-VINPG(IX-TAB-L23)
           END-IF
           IF L23-FL-VINPG-INT-PRSTZ-NULL = HIGH-VALUES
              MOVE L23-FL-VINPG-INT-PRSTZ-NULL
                TO (SF)-FL-VINPG-INT-PRSTZ-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-FL-VINPG-INT-PRSTZ
                TO (SF)-FL-VINPG-INT-PRSTZ(IX-TAB-L23)
           END-IF
           MOVE L23-DESC-AGG-VINC
             TO (SF)-DESC-AGG-VINC(IX-TAB-L23)
           MOVE L23-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-L23)
           MOVE L23-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-L23)
           MOVE L23-DS-VER
             TO (SF)-DS-VER(IX-TAB-L23)
           MOVE L23-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-L23)
           MOVE L23-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-L23)
           MOVE L23-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-L23)
           MOVE L23-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-L23)
           IF L23-TP-AUT-SEQ-NULL = HIGH-VALUES
              MOVE L23-TP-AUT-SEQ-NULL
                TO (SF)-TP-AUT-SEQ-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-TP-AUT-SEQ
                TO (SF)-TP-AUT-SEQ(IX-TAB-L23)
           END-IF
           IF L23-COD-UFF-SEQ-NULL = HIGH-VALUES
              MOVE L23-COD-UFF-SEQ-NULL
                TO (SF)-COD-UFF-SEQ-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-COD-UFF-SEQ
                TO (SF)-COD-UFF-SEQ(IX-TAB-L23)
           END-IF
           IF L23-NUM-PROVV-SEQ-NULL = HIGH-VALUES
              MOVE L23-NUM-PROVV-SEQ-NULL
                TO (SF)-NUM-PROVV-SEQ-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-NUM-PROVV-SEQ
                TO (SF)-NUM-PROVV-SEQ(IX-TAB-L23)
           END-IF
           IF L23-TP-PROVV-SEQ-NULL = HIGH-VALUES
              MOVE L23-TP-PROVV-SEQ-NULL
                TO (SF)-TP-PROVV-SEQ-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-TP-PROVV-SEQ
                TO (SF)-TP-PROVV-SEQ(IX-TAB-L23)
           END-IF
           IF L23-DT-PROVV-SEQ-NULL = HIGH-VALUES
              MOVE L23-DT-PROVV-SEQ-NULL
                TO (SF)-DT-PROVV-SEQ-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-DT-PROVV-SEQ
                TO (SF)-DT-PROVV-SEQ(IX-TAB-L23)
           END-IF
           IF L23-DT-NOTIFICA-BLOCCO-NULL = HIGH-VALUES
              MOVE L23-DT-NOTIFICA-BLOCCO-NULL
                TO (SF)-DT-NOTIFICA-BLOCCO-NULL(IX-TAB-L23)
           ELSE
              MOVE L23-DT-NOTIFICA-BLOCCO
                TO (SF)-DT-NOTIFICA-BLOCCO(IX-TAB-L23)
           END-IF
           MOVE L23-NOTA-PROVV
             TO (SF)-NOTA-PROVV(IX-TAB-L23).
       VALORIZZA-OUTPUT-L23-EX.
           EXIT.

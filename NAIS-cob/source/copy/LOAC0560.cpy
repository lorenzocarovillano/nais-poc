      *----------------------------------------------------------------*
      *   COPY AREA DI PAGINA SCHEDULAZIONE OPERAZIONI AUTOMATICHE
      * allineamento 10/09/2008
      *----------------------------------------------------------------*
      *01 AREA-LOAC0560.
          10 (SF)-MOVI-BATCH-SEL             PIC  9(005).
          10 (SF)-TP-ELA-EST-CONTO-SEL       PIC  X(002).
             88 (SF)-RIVALUTAB-SEL           VALUE 'RV'.
             88 (SF)-INDEX-LINKED-SEL        VALUE 'IL'.
             88 (SF)-UNIT-LINKED-SEL         VALUE 'UL'.
CR7672       88 (SF)-MULTIRAMO-SEL           VALUE 'MU'.
CR7672       88 (SF)-TCM-SEL                 VALUE 'TC'.
          10 (SF)-DT-ELAB-DA-DEF             PIC  9(008).
          10 (SF)-DT-ELAB-A-DEF              PIC  9(008).
          10 (SF)-DT-COMPETENZA              PIC  9(008).
          10 (SF)-TP-FRM-ASSVA-SEL           PIC  X(002).
             88 (SF)-COLLETTIVA-SEL          VALUE 'CO'.
             88 (SF)-INDIVIDUALI-SEL         VALUE 'IN'.
             88 (SF)-ENTRAMBE-SEL            VALUE 'EN'.
          10 (SF)-CANALE-VENDITA-SEL         PIC  9(005).
      *--> ELENCO PRENOTAZIONI
          10 (SF)-ELE-MAX-BATCH-PRES         PIC S9(004) COMP.
          10 (SF)-TAB-BATCH-PRES             OCCURS 10.
            12 (SF)-ID-RICH                  PIC S9(009) COMP-3.
            12 (SF)-ID-RICH-COLLG            PIC S9(009) COMP-3.
            12 (SF)-STATI-PRENOTAZIONE       PIC X(001).
              88 (SF)-PREN-ST-PRENOTATA      VALUE 'P'.
              88 (SF)-PREN-ST-ESEGUITA       VALUE 'E'.
              88 (SF)-PREN-ST-ESEGUITA-KO    VALUE 'K'.
              88 (SF)-PREN-ST-ANNULLATA      VALUE 'A'.
            12 (SF)-STATUS                   PIC X(001).
              88 (SF)-ST-ADD                 VALUE 'A'.
              88 (SF)-ST-MOD                 VALUE 'M'.
              88 (SF)-ST-INV                 VALUE 'I'.
              88 (SF)-ST-DEL                 VALUE 'D'.
              88 (SF)-ST-CON                 VALUE 'C'.
      *--> AREA DA SALVARE SULLA DETT-RICH.
            12 (SF)-AREA-TAB-BATCH-PRES.
             15 (SF)-MOVI-BATCH              PIC  9(005).
             15 (SF)-TP-ELA-EST-CONTO        PIC  X(002).
                88 (SF)-RIVALUTAB            VALUE 'RV'.
                88 (SF)-INDEX-LINKED         VALUE 'IL'.
                88 (SF)-UNIT-LINKED          VALUE 'UL'.
CR7672          88 (SF)-MULTIRAMO            VALUE 'MU'.
CR7672          88 (SF)-TCM                  VALUE 'TC'.
             15 (SF)-BATCH-PROVA             PIC  X(001).
                88 (SF)-BATCH-PROVA-SI       VALUE 'S'.
                88 (SF)-BATCH-PROVA-NO       VALUE 'N'.
             15 (SF)-TP-FRM-ASSVA            PIC  X(002).
                88 (SF)-COLLETTIVA           VALUE 'CO'.
                88 (SF)-INDIVIDUALI          VALUE 'IN'.
                88 (SF)-FT-ENTRAMBE          VALUE 'EN'.
             15 (SF)-FL-BONUS                PIC  X(001).
                88 (SF)-FL-BONUS-RICORR      VALUE 'R'.
                88 (SF)-FL-BONUS-FEDELTA     VALUE 'F'.
                88 (SF)-FL-BONUS-ENTRAMBE    VALUE 'E'.
             15 (SF)-TP-RIASS                PIC X(002).
             15 (SF)-ELE-MAX-RIASS           PIC S9(004) COMP.
             15 (SF)-TAB-AREA-RIASS            OCCURS 10.
                20 (SF)-COD-TRATTATI-ELA     PIC  X(012).
                20 (SF)-DES-TRATTATI-ELA     PIC  X(030).
             15 (SF)-DT-COMPETENZA-DET       PIC  9(008).
             15 (SF)-IB-OGGETTO              PIC  X(040).
             15 (SF)-DT-ELAB-DA              PIC  9(008).
             15 (SF)-DT-ELAB-A               PIC  9(008).
             15 (SF)-FL-ONL-UPD-PARAM-COMP   PIC  X(001).
                88 (SF)-ONL-UPD-PARAM-COMP-SI     VALUE 'S'.
                88 (SF)-ONL-UPD-PARAM-COMP-NO     VALUE 'N'.
ID67         15 (SF)-RAMO                    PIC  X(012).
             15 (SF)-CANALE-VENDITA          PIC  9(005).
MIGCOL       15 (SF)-IND-REC-PROVV           PIC  X(001).
MIGCOL          88 (SF)-IND-REC-PROVV-SI          VALUE 'S'.
MIGCOL          88 (SF)-IND-REC-PROVV-NO          VALUE 'N'.
FAZIO        15 (SF)-DATA-ATTIVAZIONE        PIC  9(008).
MIGCOL*      INDICATORE MODALITA RECUPERO. (COLLETTIVA, POSIZIONE).
MIGCOL       15 (SF)-IND-MOD-REC             PIC  X(001).
MIGCOL          88 (SF)-IND-MOD-REC-C          VALUE 'C'.
MIGCOL          88 (SF)-IND-MOD-REC-P          VALUE 'P'.
PDLST        15 (SF)-DATA-COMPETENZA         PIC S9(18)V     COMP-3.

           EXEC SQL DECLARE NOTE_OGG TABLE
           (
             ID_NOTE_OGG         DECIMAL(9, 0) NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             ID_OGG              DECIMAL(9, 0) NOT NULL,
             TP_OGG              CHAR(2) NOT NULL,
             NOTA_OGG            VARCHAR(250),
             DS_OPER_SQL         CHAR(1) NOT NULL WITH DEFAULT,
             DS_VER              DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_UTENTE           CHAR(20) NOT NULL WITH DEFAULT,
             DS_STATO_ELAB       CHAR(1) NOT NULL WITH DEFAULT
          ) END-EXEC.

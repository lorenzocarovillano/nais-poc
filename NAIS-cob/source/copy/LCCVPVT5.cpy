
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVPVT5
      *   ULTIMO AGG. 21 NOV 2013
      *------------------------------------------------------------

       VAL-DCLGEN-PVT.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PVT)
              TO PVT-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-PVT)
              TO PVT-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-PVT) NOT NUMERIC
              MOVE 0 TO PVT-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-PVT)
              TO PVT-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-PVT) NOT NUMERIC
              MOVE 0 TO PVT-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-PVT)
              TO PVT-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-PVT)
              TO PVT-COD-COMP-ANIA
           IF (SF)-TP-PROV-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-TP-PROV-NULL(IX-TAB-PVT)
              TO PVT-TP-PROV-NULL
           ELSE
              MOVE (SF)-TP-PROV(IX-TAB-PVT)
              TO PVT-TP-PROV
           END-IF
           IF (SF)-PROV-1AA-ACQ-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-PROV-1AA-ACQ-NULL(IX-TAB-PVT)
              TO PVT-PROV-1AA-ACQ-NULL
           ELSE
              MOVE (SF)-PROV-1AA-ACQ(IX-TAB-PVT)
              TO PVT-PROV-1AA-ACQ
           END-IF
           IF (SF)-PROV-2AA-ACQ-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-PROV-2AA-ACQ-NULL(IX-TAB-PVT)
              TO PVT-PROV-2AA-ACQ-NULL
           ELSE
              MOVE (SF)-PROV-2AA-ACQ(IX-TAB-PVT)
              TO PVT-PROV-2AA-ACQ
           END-IF
           IF (SF)-PROV-RICOR-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-PROV-RICOR-NULL(IX-TAB-PVT)
              TO PVT-PROV-RICOR-NULL
           ELSE
              MOVE (SF)-PROV-RICOR(IX-TAB-PVT)
              TO PVT-PROV-RICOR
           END-IF
           IF (SF)-PROV-INC-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-PROV-INC-NULL(IX-TAB-PVT)
              TO PVT-PROV-INC-NULL
           ELSE
              MOVE (SF)-PROV-INC(IX-TAB-PVT)
              TO PVT-PROV-INC
           END-IF
           IF (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-PVT)
              TO PVT-ALQ-PROV-ACQ-NULL
           ELSE
              MOVE (SF)-ALQ-PROV-ACQ(IX-TAB-PVT)
              TO PVT-ALQ-PROV-ACQ
           END-IF
           IF (SF)-ALQ-PROV-INC-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-ALQ-PROV-INC-NULL(IX-TAB-PVT)
              TO PVT-ALQ-PROV-INC-NULL
           ELSE
              MOVE (SF)-ALQ-PROV-INC(IX-TAB-PVT)
              TO PVT-ALQ-PROV-INC
           END-IF
           IF (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-PVT)
              TO PVT-ALQ-PROV-RICOR-NULL
           ELSE
              MOVE (SF)-ALQ-PROV-RICOR(IX-TAB-PVT)
              TO PVT-ALQ-PROV-RICOR
           END-IF
           IF (SF)-FL-STOR-PROV-ACQ-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-FL-STOR-PROV-ACQ-NULL(IX-TAB-PVT)
              TO PVT-FL-STOR-PROV-ACQ-NULL
           ELSE
              MOVE (SF)-FL-STOR-PROV-ACQ(IX-TAB-PVT)
              TO PVT-FL-STOR-PROV-ACQ
           END-IF
           IF (SF)-FL-REC-PROV-STORN-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-FL-REC-PROV-STORN-NULL(IX-TAB-PVT)
              TO PVT-FL-REC-PROV-STORN-NULL
           ELSE
              MOVE (SF)-FL-REC-PROV-STORN(IX-TAB-PVT)
              TO PVT-FL-REC-PROV-STORN
           END-IF
           IF (SF)-FL-PROV-FORZ-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-FL-PROV-FORZ-NULL(IX-TAB-PVT)
              TO PVT-FL-PROV-FORZ-NULL
           ELSE
              MOVE (SF)-FL-PROV-FORZ(IX-TAB-PVT)
              TO PVT-FL-PROV-FORZ
           END-IF
           IF (SF)-TP-CALC-PROV-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-TP-CALC-PROV-NULL(IX-TAB-PVT)
              TO PVT-TP-CALC-PROV-NULL
           ELSE
              MOVE (SF)-TP-CALC-PROV(IX-TAB-PVT)
              TO PVT-TP-CALC-PROV
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-PVT) NOT NUMERIC
              MOVE 0 TO PVT-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-PVT)
              TO PVT-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-PVT)
              TO PVT-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-PVT) NOT NUMERIC
              MOVE 0 TO PVT-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-PVT)
              TO PVT-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-PVT) NOT NUMERIC
              MOVE 0 TO PVT-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-PVT)
              TO PVT-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-PVT) NOT NUMERIC
              MOVE 0 TO PVT-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-PVT)
              TO PVT-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-PVT)
              TO PVT-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-PVT)
              TO PVT-DS-STATO-ELAB
           IF (SF)-REMUN-ASS-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-REMUN-ASS-NULL(IX-TAB-PVT)
              TO PVT-REMUN-ASS-NULL
           ELSE
              MOVE (SF)-REMUN-ASS(IX-TAB-PVT)
              TO PVT-REMUN-ASS
           END-IF
           IF (SF)-COMMIS-INTER-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-COMMIS-INTER-NULL(IX-TAB-PVT)
              TO PVT-COMMIS-INTER-NULL
           ELSE
              MOVE (SF)-COMMIS-INTER(IX-TAB-PVT)
              TO PVT-COMMIS-INTER
           END-IF
           IF (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-PVT)
              TO PVT-ALQ-REMUN-ASS-NULL
           ELSE
              MOVE (SF)-ALQ-REMUN-ASS(IX-TAB-PVT)
              TO PVT-ALQ-REMUN-ASS
           END-IF
           IF (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-PVT) = HIGH-VALUES
              MOVE (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-PVT)
              TO PVT-ALQ-COMMIS-INTER-NULL
           ELSE
              MOVE (SF)-ALQ-COMMIS-INTER(IX-TAB-PVT)
              TO PVT-ALQ-COMMIS-INTER
           END-IF.
       VAL-DCLGEN-PVT-EX.
           EXIT.

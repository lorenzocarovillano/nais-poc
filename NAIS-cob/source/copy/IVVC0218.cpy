      *----------------------------------------------------------------*
      *   ALIAS INPUT VARIABILI
      *   PORTAFOGLIO VITA - PROCESSO VENDITA
      *   LUNG.
      *----------------------------------------------------------------*
      ******************************************************************
      *ASSET
      ******************************************************************
      *--  ALIAS ADESIONE
           03 (SF)-ALIAS-ADES                   PIC X(3) VALUE 'ADE'.
      *--  ALIAS ASSET ALLOCATION
           03 (SF)-ALIAS-ASSET                  PIC X(3) VALUE 'ALL'.
      *--  ALIAS BENEFICIARI
           03 (SF)-ALIAS-BENEF                  PIC X(3) VALUE 'BEP'.
      *--  ALIAS BENEFICIARIO DI LIQUIDAZIONE
           03 (SF)-ALIAS-BENEF-LIQ              PIC X(3) VALUE 'BEL'.
      *--  ALIAS DATI COLLETTIVA
           03 (SF)-ALIAS-DT-COLL                PIC X(3) VALUE 'DCO'.
      *--  ALIAS DATI FISCALE ADESIONE
           03 (SF)-ALIAS-DT-FISC-ADES           PIC X(3) VALUE 'DFA'.
      *--  ALIAS DETTAGLIO QUESTIONARIO
           03 (SF)-ALIAS-DETT-QUEST             PIC X(3) VALUE 'DEQ'.
      *--  ALIAS DETTAGLIO TITOLO CONTABILE
           03 (SF)-ALIAS-DETT-TIT-CONT          PIC X(3) VALUE 'DTC'.
      *--  ALIAS GARANZIA
           03 (SF)-ALIAS-GARANZIA               PIC X(3) VALUE 'GRZ'.
      *--  ALIAS GARANZIA OPZIONE
           03 (SF)-ALIAS-GARANZIA-OPZ           PIC X(3) VALUE 'GOP'.
      *--  ALIAS GARANZIA DI LIQUIDAZIONE
           03 (SF)-ALIAS-GAR-LIQ                PIC X(3) VALUE 'GRL'.
      *--  ALIAS IMPOSTA DI BOLLO
           03 (SF)-ALIAS-IMPOSTA-BOLLO          PIC X(3) VALUE 'P58'.
      *--  ALIAS DATI CRISTALLIZZATI
           03 (SF)-ALIAS-DATI-CRIST             PIC X(3) VALUE 'P61'.
      *--  ALIAS IMPOSTA SOSTITUTIVA
           03 (SF)-ALIAS-IMPOSTA-SOST           PIC X(3) VALUE 'ISO'.
      *--  ALIAS LIQUIDAZIONE
           03 (SF)-ALIAS-LIQUIDAZ               PIC X(3) VALUE 'LQU'.
      *--  ALIAS MOVIMENTO
           03 (SF)-ALIAS-MOVIMENTO              PIC X(3) VALUE 'MOV'.
      *--  ALIAS MOVIMENTO FINANZIARIO
           03 (SF)-ALIAS-MOVI-FINRIO            PIC X(3) VALUE 'MFZ'.
      *--  ALIAS PARAMETRO OGGETTO
           03 (SF)-ALIAS-PARAM-OGG              PIC X(3) VALUE 'POG'.
      *--  ALIAS PARAMETRO MOVIMENTO
           03 (SF)-ALIAS-PARAM-MOV              PIC X(3) VALUE 'PMO'.
      *--  ALIAS PERCIPIENTE LIQUIDAZIONE
           03 (SF)-ALIAS-PERC-LIQ               PIC X(3) VALUE 'PLI'.
      *--  ALIAS POLIZZA
           03 (SF)-ALIAS-POLI                   PIC X(3) VALUE 'POL'.
      *--  ALIAS PRESTITI
           03 (SF)-ALIAS-PRESTITI               PIC X(3) VALUE 'PRE'.
      *--  ALIAS PROVVIGIONE DI TRANCHE
           03 (SF)-ALIAS-PROVV-TRAN             PIC X(3) VALUE 'PVT'.
      *--  ALIAS QUESTIONARIO
           03 (SF)-ALIAS-QUEST                  PIC X(3) VALUE 'QUE'.
      *--  ALIAS RICHIESTA
           03 (SF)-ALIAS-RICH                   PIC X(3) VALUE 'RIC'.
      *--  ALIAS RICHIESTA DISINVESTIMENTO FONDO
           03 (SF)-ALIAS-RICH-DISINV-FND        PIC X(3) VALUE 'RDF'.
      *--  ALIAS RICHIESTA INVESTIMENTO FONDO
           03 (SF)-ALIAS-RICH-INV-FND           PIC X(3) VALUE 'RIF'.
      *--  ALIAS RAPPORTO ANAGRAFICO
           03 (SF)-ALIAS-RAPP-ANAG              PIC X(3) VALUE 'RAN'.
      *--  ALIAS EST RAPPORTO ANAGRAFICO
           03 (SF)-ALIAS-EST-RAPP-ANAG          PIC X(3) VALUE 'E15'.
      *--  ALIAS RAPPORTO RETE
           03 (SF)-ALIAS-RAPP-RETE              PIC X(3) VALUE 'RRE'.
      *--  ALIAS SOPRAPREMIO DI GARANZIA
           03 (SF)-ALIAS-SOPRAP-GAR             PIC X(3) VALUE 'SPG'.
      *--  ALIAS STRATEGIA DI INVESTIMENTO
           03 (SF)-ALIAS-STRA-INV               PIC X(3) VALUE 'SDI'.
      *--  ALIAS TITOLO CONTABILE
           03 (SF)-ALIAS-TIT-CONT               PIC X(3) VALUE 'TIT'.
      *--  ALIAS TITOLO DI LIQUIDAZIONE
           03 (SF)-ALIAS-TIT-LIQ                PIC X(3) VALUE 'TCL'.
      *--  ALIAS TRANCHE DI GARANZIA
           03 (SF)-ALIAS-TRCH-GAR               PIC X(3) VALUE 'TGA'.
      *-- ALIAS TRANCHE GARANZIA DI LIQUIDAZIONE
           03 (SF)-ALIAS-TRCH-LIQ               PIC X(3) VALUE 'TLI'.
      *--  ALIAS TRANCHE DI GARANZIA OPZIONE
           03 (SF)-ALIAS-TRCH-GAR-OPZ           PIC X(3) VALUE 'TOP'.
      *-- ALIAS DEFAULT ADESIONE
           03 (SF)-ALIAS-DFLT-ADES              PIC X(3) VALUE 'DAD'.
      *-- ALIAS OGGETTO COLLEGATO
           03 (SF)-ALIAS-OGG-COLL               PIC X(3) VALUE 'OCO'.
      *-- ALIAS DATI FORZATI DI LIQUIDAZIONE
           03 (SF)-ALIAS-DT-FORZ-LIQ            PIC X(3) VALUE 'DFL'.
      *--  ALIAS DATI PARAM DI CALC
           03 (SF)-ALIAS-PARAM-CALC             PIC X(3) VALUE 'PCA'.
      *--  ALIAS DATI TIT RATA
           03 (SF)-ALIAS-TIT-DI-RAT             PIC X(3) VALUE 'TDR'.
      *--  ALIAS DATI DETT TIT RAT
           03 (SF)-ALIAS-DETT-TIT-RAT           PIC X(3) VALUE 'DTR'.
      *--  ALIAS DATI CLAUSOLA TESTUAEL
           03 (SF)-ALIAS-CLAU-TEST              PIC X(3) VALUE 'CLT'.
      *--  ALIAS DATI CLAUSOLA TESTUAEL
           03 (SF)-ALIAS-NOTE-OGG               PIC X(3) VALUE 'NOT'.
      *--  ALIAS DATI CLAUSOLA TESTUAEL
           03 (SF)-ALIAS-CONTR-NON-DED          PIC X(3) VALUE 'CND'.
      *--  ALIAS DATI OGGETTO COAS
           03 (SF)-ALIAS-OGGETTO-COAS           PIC X(3) VALUE 'OCS'.
      *--  ALIAS parametro compagnia
           03 (SF)-ALIAS-PARAM-COMP             PIC X(3) VALUE 'PCO'.
      *-- ALIAS SCHEDA PER SERVIZI DI PRODOTTO
           03 (SF)-ALIAS-SCHEDA                 PIC X(3) VALUE 'SKD'.
      *-- ALIAS OPZIONI
           03 (SF)-ALIAS-OPZIONI                PIC X(3) VALUE 'OPZ'.
      *--  ALIAS DATI CONTESTUALI
           03 (SF)-ALIAS-DATI-CONTEST           PIC X(3) VALUE 'CNT'.
      *--  ALIAS DATI DETT RICHIESTA
           03 (SF)-ALIAS-DETT-RICH              PIC X(3) VALUE 'DER'.
      *--  ALIAS RIPARTO IN COASSICURAZIONE
           03 (SF)-ALIAS-RIP-COASS              PIC X(3) VALUE 'RCA'.
      *--  ALIAS DOC IN LIQUIDAZIONE
           03 (SF)-ALIAS-DOC-LIQ                PIC X(3) VALUE 'DLQ'.
      *--  ALIAS DETTAGLIO ANTICIPAZIONE
           03 (SF)-ALIAS-DETT-ANT               PIC X(3) VALUE 'L24'.
      *--  ALIAS VINCOLI E PEGNI
           03 (SF)-ALIAS-VINC-PEGN              PIC X(3) VALUE 'L23'.
      *--  ALIAS VALORE ASSET
           03 (SF)-ALIAS-VAL-ASS                PIC X(3) VALUE 'VAS'.
      *--  ALIAS REINVESTIMENTO POLIZZA
           03 (SF)-ALIAS-REINVST-POLI           PIC X(3) VALUE 'L30'.
      *--  ALIAS REINVESTIMENTO POLIZZA
           03 (SF)-ALIAS-QOTAZ-FON              PIC X(3) VALUE 'L19'.
      *--  ALIAR RIS DI TRANCHE
           03 (SF)-ALIAS-RIS-DI-TRANCHE         PIC X(3) VALUE 'RST'.
      *--  ALIAS STAT OGG BUS
           03 (SF)-ALIAS-STAT-OGG-BUS           PIC X(3) VALUE 'STB'.
      *--  ALIAS DETTAGLIO TRANSF
           03 (SF)-ALIAS-DETT-TRASFE            PIC X(3) VALUE 'L25'.
      *--  ALIAS AMMB FUNZ BLOCCO
           03 (SF)-ALIAS-AMMB-FUNZ-BLOC         PIC X(3) VALUE 'L16'.
      *--  ALIAS AMMB FUNZ FUNZ
           03 (SF)-ALIAS-AMMB-FUNZ-FUNZ         PIC X(3) VALUE 'L05'.
      *--  ALIAS ANA AZ
           03 (SF)-ALIAS-ANA-AZ                 PIC X(3) VALUE 'AAZ'.
      *--  ALIAS ANA BLOCCO
           03 (SF)-ALIAS-ANA-BLOCCO             PIC X(3) VALUE 'XAB'.
      *--  ALIAS ANA COMP COAS RIAS
           03 (SF)-ALIAS-ANA-COM-CO-RIA         PIC X(3) VALUE 'L09'.
      *--  ALIAS ANA CNTRL ADEGZ
           03 (SF)-ALIAS-ANA-CNTRL-ADEGZ        PIC X(3) VALUE 'L03'.
      *--  ALIAS ANA DOCTI ISTR
           03 (SF)-ALIAS-ANA-DOCTI-ISTR         PIC X(3) VALUE 'ADI'.
      *--  ALIAS ANA MATR
           03 (SF)-ALIAS-ANA-MATR               PIC X(3) VALUE 'AMT'.
      *--  ALIAS ANA SOPR AUT
           03 (SF)-ALIAS-ANA-SOPR-AUT           PIC X(3) VALUE 'L17'.
      *--  ALIAS ANTIRICIC
           03 (SF)-ALIAS-ANTIRICIC              PIC X(3) VALUE 'L04'.
      *--  ALIAS BILA FND CALC
           03 (SF)-ALIAS-BILA-FND-CALC          PIC X(3) VALUE 'B07'.
      *--  ALIAS BILA FND ESTR
           03 (SF)-ALIAS-BILA-FND-ESTR          PIC X(3) VALUE 'B01'.
      *--  ALIAS BILA TRCH CALC
           03 (SF)-ALIAS-BILA-TRCH-CALC         PIC X(3) VALUE 'B02'.
      *--  ALIAS BILA TRCH ESTR
           03 (SF)-ALIAS-BILA-TRCH-ESTR         PIC X(3) VALUE 'B03'.
      *--  ALIAS BILA TRCH SOM P
           03 (SF)-ALIAS-BILA-TRCH-SOM-P        PIC X(3) VALUE 'B06'.
      *--  ALIAS BILA VAR CALC P
           03 (SF)-ALIAS-BILA-VAR-CALC-P        PIC X(3) VALUE 'B04'.
      *--  ALIAS BILA VAR CALC T
           03 (SF)-ALIAS-BILA-VAR-CALC-T        PIC X(3) VALUE 'B05'.
      *--  ALIAS CONV AZ
           03 (SF)-ALIAS-CONV-AZ                PIC X(3) VALUE 'L34'.
      *--  ALIAS CONV RSH
           03 (SF)-ALIAS-CONV-RSH               PIC X(3) VALUE 'L01'.
      *--  ALIAS D COASS
           03 (SF)-ALIAS-D-COASS                PIC X(3) VALUE 'L12'.
      *--  ALIAS DFLT RINN TRCH
           03 (SF)-ALIAS-DFLT-RINN-TRCH         PIC X(3) VALUE 'DRT'.
      *--  ALIAS DETT D COASS
           03 (SF)-ALIAS-DETT-D-COASS           PIC X(3) VALUE 'L14'.
      *--  ALIAS DETT PREV
           03 (SF)-ALIAS-DETT-PREV              PIC X(3) VALUE 'L28'.
      *--  ALIAS DETT TIT COA AG
           03 (SF)-ALIAS-DETT-TIT-COA-AG        PIC X(3) VALUE 'DTA'.
      *--  ALIAS DETT TCONT LIQ
           03 (SF)-ALIAS-DETT-TCONT-LIQ         PIC X(3) VALUE 'DTL'.
      *--  ALIAS DIVISA
           03 (SF)-ALIAS-DIVISA                 PIC X(3) VALUE 'L65'.
      *--  ALIAS ESTENZ CLAU TXT
           03 (SF)-ALIAS-ESTENZ-CLAU-TXT        PIC X(3) VALUE 'L31'.
      *--  ALIAS ESTR CNT COASS
           03 (SF)-ALIAS-ESTR-CNT-COASS         PIC X(3) VALUE 'L18'.
      *--  ALIAS IDEN ESTNI
           03 (SF)-ALIAS-IDEN-ESTNI             PIC X(3) VALUE 'IDE'.
      *--  ALIAS MATR ANNULLO
           03 (SF)-ALIAS-MATR-ANNULLO           PIC X(3) VALUE 'L52'.
      *--  ALIAS MATR ELAB BATCH
           03 (SF)-ALIAS-MATR-ELAB-BATCH        PIC X(3) VALUE 'L71'.
      *--  ALIAS MOT DEROGA
           03 (SF)-ALIAS-MOT-DEROGA             PIC X(3) VALUE 'MDE'.
      *--  ALIAS MOVI BATCH SOSP
           03 (SF)-ALIAS-MOVI-BATCH-SOSP        PIC X(3) VALUE 'MBS'.
      *--  ALIAS NUM ADE GAR TRA
           03 (SF)-ALIAS-NUM-ADE-GAR-TRA        PIC X(3) VALUE 'NUM'.
      *--  ALIAS OGG BLOCCO
           03 (SF)-ALIAS-OGG-BLOCCO             PIC X(3) VALUE 'L11'.
      *--  ALIAS OGG DEROGA
           03 (SF)-ALIAS-OGG-DEROGA             PIC X(3) VALUE 'ODE'.
      *--  ALIAS ORDINE ESEC FUNZ
           03 (SF)-ALIAS-ORDI-ESEC-FUNZ         PIC X(3) VALUE 'L06'.
      *--  ALIAS ORDINE PARAM GEN
           03 (SF)-ALIAS-PARAM-GEN              PIC X(3) VALUE 'PGE'.
      *--  ALIAS ORDINE PARAM CLAU TXT
           03 (SF)-ALIAS-PARAM-CLAU-TXT         PIC X(3) VALUE 'L32'.
      *--  ALIAS PREV
           03 (SF)-ALIAS-PREV                   PIC X(3) VALUE 'L27'.
      *--  ALIAS PROD RSH
           03 (SF)-ALIAS-PROD-RSH               PIC X(3) VALUE 'L02'.
      *--  ALIAS QUOTZ AGG FND
           03 (SF)-ALIAS-QUOTZ-AGG-FND          PIC X(3) VALUE 'L41'.
      *--  ALIAS RIP DI RIASS
           03 (SF)-ALIAS-RIP-DI-RIASS           PIC X(3) VALUE 'L15'.
      *--  ALIAS SIN PEND CPI PR
           03 (SF)-ALIAS-SIN-PEND-CPI-PR        PIC X(3) VALUE 'P74'.
      *--  ALIAS SOGG ANTIRICIC
           03 (SF)-ALIAS-SOGG-ANTIRICIC         PIC X(3) VALUE 'L33'.
      *--  ALIAS SOPR DETT QUEST
           03 (SF)-ALIAS-SOPR-DETT-QUEST        PIC X(3) VALUE 'L20'.
      *--  ALIAS STAT OGG WF
           03 (SF)-ALIAS-STAT-OGG-WF            PIC X(3) VALUE 'STW'.
      *--  ALIAS TS BNS SIN PRE
           03 (SF)-ALIAS-TS-BNS-SIN-PRE         PIC X(3) VALUE 'L22'.
      *--  ALIAS TASSO DI CAMBIO
           03 (SF)-ALIAS-TASSO-DI-CAMBIO        PIC X(3) VALUE 'L67'.
      *--  ALIAS TP ADEGZ
           03 (SF)-ALIAS-TP-ADEGZ               PIC X(3) VALUE 'XAZ'.
      *--  ALIAS TP ACC COMM
           03 (SF)-ALIAS-TP-ACC-COMM            PIC X(3) VALUE 'P64'.
      *--  ALIAS TP ANNULLO
           03 (SF)-ALIAS-TP-ANNULLO             PIC X(3) VALUE 'L51'.
      *--  ALIAS TP APPLZ AST
           03 (SF)-ALIAS-TP-APPLZ-AST           PIC X(3) VALUE 'XAS'.
      *--  ALIAS TP APPLZ DIR
           03 (SF)-ALIAS-TP-APPLZ-DIR           PIC X(3) VALUE 'XAD'.
      *--  ALIAS TP AUT SEQ
           03 (SF)-ALIAS-TP-AUT-SEQ             PIC X(3) VALUE 'L82'.
      *--  ALIAS TP CALC IMPST
           03 (SF)-ALIAS-TP-CALC-IMPST          PIC X(3) VALUE 'X15'.
      *--  ALIAS TP CALC RIS
           03 (SF)-ALIAS-TP-CALC-RIS            PIC X(3) VALUE 'XCR'.
      *--  ALIAS TP CAU RIFRMTO
           03 (SF)-ALIAS-TP-CAU-RIFRMTO         PIC X(3) VALUE 'X01'.
      *--  ALIAS TP CAUS
           03 (SF)-ALIAS-TP-CAUS                PIC X(3) VALUE 'XCA'.
      *--  ALIAS TP CAUS ANTIC
           03 (SF)-ALIAS-TP-CAUS-ANTIC          PIC X(3) VALUE 'L70'.
      *--  ALIAS TP CAUS RETTIFICA
           03 (SF)-ALIAS-TP-CAUS-RETT           PIC X(3) VALUE 'L86'.
      *--  ALIAS TP CAUS RID
           03 (SF)-ALIAS-TP-CAUS-RID            PIC X(3) VALUE 'L97'.
      *--  ALIAS TP CAUS SIN
           03 (SF)-ALIAS-TP-CAUS-SIN            PIC X(3) VALUE 'XCS'.
      *--  ALIAS TP CESS
           03 (SF)-ALIAS-TP-CESS                PIC X(3) VALUE 'XCE'.
      *--  ALIAS TP COASS
           03 (SF)-ALIAS-TP-COASS               PIC X(3) VALUE 'XTC'.
      *--  ALIAS TP COLLGM
           03 (SF)-ALIAS-TP-COLLGM              PIC X(3) VALUE 'XCO'.
      *--  ALIAS TP COMP ACQS
           03 (SF)-ALIAS-TP-COMP-ACQS           PIC X(3) VALUE 'XCQ'.
      *--  ALIAS TP CONS ALCOL
           03 (SF)-ALIAS-TP-CONS-ALCOL          PIC X(3) VALUE 'X12'.
      *--  ALIAS TP DT FLTR LIS
           03 (SF)-ALIAS-TP-DT-FLTR-LIS         PIC X(3) VALUE 'XDT'.
      *--  ALIAS TP D
           03 (SF)-ALIAS-TP-D                   PIC X(3) VALUE 'XDA'.
      *--  ALIAS TP DFLT DUR
           03 (SF)-ALIAS-TP-DFLT-DUR            PIC X(3) VALUE 'XDD'.
      *--  ALIAS TP DLG
           03 (SF)-ALIAS-TP-DLG                 PIC X(3) VALUE 'XDE'.
      *--  ALIAS TP DEROGA
           03 (SF)-ALIAS-TP-DEROGA              PIC X(3) VALUE 'XDG'.
      *--  ALIAS TP DISTINTA
           03 (SF)-ALIAS-TP-DISTINTA            PIC X(3) VALUE 'X03'.
      *--  ALIAS TP DOCTO
           03 (SF)-ALIAS-TP-DOCTO               PIC X(3) VALUE 'XDR'.
      *--  ALIAS TP ERR
           03 (SF)-ALIAS-TP-ERR                 PIC X(3) VALUE 'XER'.
      *--  ALIAS TP ESI ESAME
           03 (SF)-ALIAS-TP-ESI-ESAME           PIC X(3) VALUE 'X11'.
      *--  ALIAS TP ESI RID
           03 (SF)-ALIAS-TP-ESI-RID             PIC X(3) VALUE 'L40'.
      *--  ALIAS TP ESTR CNT
           03 (SF)-ALIAS-TP-ESTR-CNT            PIC X(3) VALUE 'L58'.
      *--  ALIAS TP END PATLG
           03 (SF)-ALIAS-TP-END-PATLG           PIC X(3) VALUE 'X04'.
      *--  ALIAS TP FRM ASSVA
           03 (SF)-ALIAS-TP-FRM-ASSVA           PIC X(3) VALUE 'XFA'.
      *--  ALIAS TP GEN
           03 (SF)-ALIAS-TP-GEN                 PIC X(3) VALUE 'XGE'.
      *--  ALIAS TP IMPFZ FISIC
           03 (SF)-ALIAS-TP-IMPFZ-FISIC         PIC X(3) VALUE 'X05'.
      *--  ALIAS TP IMP
           03 (SF)-ALIAS-TP-IMP                 PIC X(3) VALUE 'XIM'.
      *--  ALIAS TP IND PRINC
           03 (SF)-ALIAS-TP-IND-PRINC           PIC X(3) VALUE 'XIP'.
      *--  ALIAS TP IND BNFICR
           03 (SF)-ALIAS-TP-IND-BNFICR          PIC X(3) VALUE 'XIN'.
      *--  ALIAS TP ISC
           03 (SF)-ALIAS-TP-ISC                 PIC X(3) VALUE 'XIS'.
      *--  ALIAS TP LEGALE RAPPR
           03 (SF)-ALIAS-TP-LEGALE-RAPPR        PIC X(3) VALUE 'XLP'.
      *--  ALIAS TP LIQ
           03 (SF)-ALIAS-TP-LIQ                 PIC X(3) VALUE 'XLI'.
      *--  ALIAS TP LIV GENZ TIT
           03 (SF)-ALIAS-TP-LIV-GENZ-TIT        PIC X(3) VALUE 'XLT'.
      *--  ALIAS TP LIV RICH
           03 (SF)-ALIAS-TP-LIV-RICH            PIC X(3) VALUE 'XLR'.
      *--  ALIAS TP MET RIVAL
           03 (SF)-ALIAS-TP-MET-RIVAL           PIC X(3) VALUE 'XMR'.
      *--  ALIAS TP MEZ PAG
           03 (SF)-ALIAS-TP-MEZ-PAG             PIC X(3) VALUE 'XMZ'.
      *--  ALIAS TP MOD ABBINAMENTO
           03 (SF)-ALIAS-TP-MOD-ABBIN           PIC X(3) VALUE 'L45'.
      *--  ALIAS TP MOD ACQS
           03 (SF)-ALIAS-TP-MOD-ACQS            PIC X(3) VALUE 'P73'.
      *--  ALIAS TP MOD EXEC
           03 (SF)-ALIAS-TP-MOD-EXEC            PIC X(3) VALUE 'XME'.
      *--  ALIAS TP MOD INTR PREST
           03 (SF)-ALIAS-TP-MOD-IN-PR           PIC X(3) VALUE 'L26'.
      *--  ALIAS TP MOD INV DIS
           03 (SF)-ALIAS-TP-MOD-INV-DIS         PIC X(3) VALUE 'XTM'.
      *--  ALIAS TP MOD PAG TIT
           03 (SF)-ALIAS-TP-MOD-PAG-TIT         PIC X(3) VALUE 'XMP'.
      *--  ALIAS TP MOD RIVAL
           03 (SF)-ALIAS-TP-MOD-RIVAL           PIC X(3) VALUE 'X14'.
      *--  ALIAS TP MOT ESAME
           03 (SF)-ALIAS-TP-MOT-ESAME           PIC X(3) VALUE 'X06'.
      *--  ALIAS TP MOT PERD PESO
           03 (SF)-ALIAS-TP-MOT-PER-PES         PIC X(3) VALUE 'X07'.
      *--  ALIAS TP MOVI
           03 (SF)-ALIAS-TP-MOVI                PIC X(3) VALUE 'XMV'.
      *--  ALIAS TP MOVI FINRIO
           03 (SF)-ALIAS-TP-MOVI-FINRIO         PIC X(3) VALUE 'XMF'.
      *--  ALIAS TP OGG
           03 (SF)-ALIAS-TP-OGG                 PIC X(3) VALUE 'XOG'.
      *--  ALIAS TP OGG RIVAL
           03 (SF)-ALIAS-TP-OGG-RIVAL           PIC X(3) VALUE 'XOR'.
      *--  ALIAS TP OPRZ ANTIRICIC
           03 (SF)-ALIAS-TP-OPRZ-ANTIRIC        PIC X(3) VALUE 'XTA'.
      *--  ALIAS TP PC RIP
           03 (SF)-ALIAS-TP-PC-RIP              PIC X(3) VALUE 'XPR'.
      *--  ALIAS TP PERS
           03 (SF)-ALIAS-TP-PERS                PIC X(3) VALUE 'XPE'.
      *--  ALIAS TP PTF
           03 (SF)-ALIAS-TP-PTF                 PIC X(3) VALUE 'XPF'.
      *--  ALIAS TP PRE TIT
           03 (SF)-ALIAS-TP-PRE-TIT             PIC X(3) VALUE 'XPT'.
      *--  ALIAS TP PREST
           03 (SF)-ALIAS-TP-PREST               PIC X(3) VALUE 'X16'.
      *--  ALIAS TP PROVZA STRA
           03 (SF)-ALIAS-TP-PROVZA-STRA         PIC X(3) VALUE 'L94'.
      *--  ALIAS TP PROVV SEQ
           03 (SF)-ALIAS-TP-PROVV-SEQ           PIC X(3) VALUE 'L83'.
      *--  ALIAS TP QUEST
           03 (SF)-ALIAS-TP-QUEST               PIC X(3) VALUE 'XQU'.
      *--  ALIAS TP QTZ
           03 (SF)-ALIAS-TP-QTZ                 PIC X(3) VALUE 'L42'.
      *--  ALIAS TP RAMO BILA
           03 (SF)-ALIAS-TP-RAMO-BILA           PIC X(3) VALUE 'X17'.
      *--  ALIAS TP RAPP ANA
           03 (SF)-ALIAS-TP-RAPP-ANA            PIC X(3) VALUE 'XRA'.
      *--  ALIAS TP RGM FISC
           03 (SF)-ALIAS-TP-RGM-FISC            PIC X(3) VALUE 'X13'.
      *--  ALIAS TP RETE
           03 (SF)-ALIAS-TP-RETE                PIC X(3) VALUE 'XRE'.
      *--  ALIAS TP RICH
           03 (SF)-ALIAS-TP-RICH                PIC X(3) VALUE 'X18'.
      *--  ALIAS TP RICH DIST
           03 (SF)-ALIAS-TP-RICH-DIST           PIC X(3) VALUE 'X08'.
      *--  ALIAS TP RIMB
           03 (SF)-ALIAS-TP-RIMB                PIC X(3) VALUE 'XRM'.
      *--  ALIAS TP RINN
           03 (SF)-ALIAS-TP-RINN                PIC X(3) VALUE 'XRN'.
      *--  ALIAS TP RISC
           03 (SF)-ALIAS-TP-RISC                PIC X(3) VALUE 'XRS'.
      *--  ALIAS TP RSH TRAT
           03 (SF)-ALIAS-TP-RSH-TRAT            PIC X(3) VALUE 'XRT'.
      *--  ALIAS TP RISP
           03 (SF)-ALIAS-TP-RISP                PIC X(3) VALUE 'XRP'.
      *--  ALIAS TP RIVAL
           03 (SF)-ALIAS-TP-RIVAL               PIC X(3) VALUE 'XRV'.
      *--  ALIAS TP SIN
           03 (SF)-ALIAS-TP-SIN                 PIC X(3) VALUE 'XSN'.
      *--  ALIAS TP SIST ESTNO
           03 (SF)-ALIAS-TP-SIST-ESTNO          PIC X(3) VALUE 'L29'.
      *--  ALIAS TP STAB
           03 (SF)-ALIAS-TP-STAB                PIC X(3) VALUE 'XSZ'.
      *--  ALIAS TP STAT BLOCCO
           03 (SF)-ALIAS-TP-STAT-BLOCCO         PIC X(3) VALUE 'XBL'.
      *--  ALIAS TP STAT BUS
           03 (SF)-ALIAS-TP-STAT-BUS            PIC X(3) VALUE 'XSB'.
      *--  ALIAS TP STAT INVST
           03 (SF)-ALIAS-TP-STAT-INVST          PIC X(3) VALUE 'XSI'.
      *--  ALIAS TP STAT MOVI FINRI
           03 (SF)-ALIAS-TP-ST-MOV-FIN          PIC X(3) VALUE 'XSF'.
      *--  ALIAS TP STAT RID
           03 (SF)-ALIAS-TP-STAT-RID            PIC X(3) VALUE 'X09'.
      *--  ALIAS TP STAT TIT
           03 (SF)-ALIAS-TP-STAT-TIT            PIC X(3) VALUE 'XST'.
      *--  ALIAS TP TS CAMBIO
           03 (SF)-ALIAS-TP-TS-CAMBIO           PIC X(3) VALUE 'L84'.
      *--  ALIAS TP TIT
           03 (SF)-ALIAS-TP-TIT                 PIC X(3) VALUE 'XTT'.
      *--  ALIAS TP TRCH
           03 (SF)-ALIAS-TP-TRCH                PIC X(3) VALUE 'XTH'.
      *--  ALIAS TP TRATM COMMIS
           03 (SF)-ALIAS-TP-TRATM-COMMIS        PIC X(3) VALUE 'XTR'.
      *--  ALIAS TP TRAT
           03 (SF)-ALIAS-TP-TRAT                PIC X(3) VALUE 'XTO'.
      *--  ALIAS TP UTLZ INDIR
           03 (SF)-ALIAS-TP-UTLZ-INDIR          PIC X(3) VALUE 'XUI'.
      *--  ALIAS TP VAL
           03 (SF)-ALIAS-TP-VAL                 PIC X(3) VALUE 'XVA'.
      *--  ALIAS TP VAL AST
           03 (SF)-ALIAS-TP-VAL-AST             PIC X(3) VALUE 'XVS'.
      *--  ALIAS TP VARZ PAGAT
           03 (SF)-ALIAS-TP-VARZ-PAGAT          PIC X(3) VALUE 'L96'.
      *--  ALIAS TP VINC
           03 (SF)-ALIAS-TP-VINC                PIC X(3) VALUE 'X10'.
      *--  ALIAS TRCH RIASTA
           03 (SF)-ALIAS-TRCH-RIASTA            PIC X(3) VALUE 'L07'.
      *--  ALIAS TRANS POLI
           03 (SF)-ALIAS-TRANS-POLI             PIC X(3) VALUE 'L61'.
      *--  ALIAS TRANS RAPP ANA
           03 (SF)-ALIAS-TRANS-RAPP-ANA         PIC X(3) VALUE 'L62'.
      *--  ALIAS TRANS REN
           03 (SF)-ALIAS-TRANS-REN              PIC X(3) VALUE 'L60'.
      *--  ALIAS VAL PLFD
           03 (SF)-ALIAS-VAL-PLFD               PIC X(3) VALUE 'VPL'.
      *--  ALIAS D SECOND LIQ
           03 (SF)-ALIAS-D-SECOND-LIQ           PIC X(3) VALUE 'P31'.
      *--  ALIAS TP CAUS DISP STOR
           03 (SF)-ALIAS-TP-CAU-DIS-STOR        PIC X(3) VALUE 'P32'.
      *--  ALIAS TP CAUS STOR
           03 (SF)-ALIAS-TP-CAUS-STOR           PIC X(3) VALUE 'P33'.
      *--  ALIAS AREA FONDI X TRANCHE
           03 (SF)-ALIAS-AREA-FND-X-TRCH        PIC X(3) VALUE 'FXT'.
      *--  ALIAS RICH_EST
           03 (SF)-ALIAS-RICH-EST               PIC X(3) VALUE 'P01'.
      *--  ALIAS QUEST ADEG VER
           03 (SF)-ALIAS-QUEST-ADEG-VER         PIC X(3) VALUE 'P56'.
      *--  ALIAS ACC COMM
           03 (SF)-ALIAS-ACC-COMM               PIC X(3) VALUE 'P63'.
      *--  ALIAS TRASC PROD
           03 (SF)-ALIAS-TRASC-PROD             PIC X(3) VALUE 'P65'.
      *--  ALIAS POLI COLLG
           03 (SF)-ALIAS-POLI-COLLG             PIC X(3) VALUE 'P66'.
      *--  ALIAS EST POLI CPI PR
           03 (SF)-ALIAS-EST-POLI-CPI-PR        PIC X(3) VALUE 'P67'.
      *--  ALIAS PROG POLI PARTNER
           03 (SF)-ALIAS-PROG-POLI-PTN          PIC X(3) VALUE 'P68'.
      *--  ALIAS PROD ACC COMM
           03 (SF)-ALIAS-PROD-ACC-COMM          PIC X(3) VALUE 'P69'.
      *--  ALIAS CAT RSH PROF
           03 (SF)-ALIAS-CAT-RSH-PROF           PIC X(3) VALUE 'P70'.
      *--  ALIAS IND MASSA CORP
           03 (SF)-ALIAS-IND-MASSA-CORP         PIC X(3) VALUE 'P71'.
      *--  ALIAS TP CAUS RIMB
           03 (SF)-ALIAS-TP-CAUS-RIMB           PIC X(3) VALUE 'P72'.
      *--  ALIAS IND SINT RSH
           03 (SF)-ALIAS-IND-SINT-RSH           PIC X(3) VALUE 'P75'.
      *--  ALIAS SERV INV MIFID
           03 (SF)-ALIAS-SERV-INV-MIFID         PIC X(3) VALUE 'P76'.
      *-- ALIAS CALC IND SINT RSH
           03 (SF)-ALIAS-CALC-I-SINT-RSH        PIC X(3) VALUE 'P77'.
      *-- ALIAS TP RIBIL
           03 (SF)-ALIAS-TP-RIBIL               PIC X(3) VALUE 'P78'.
      *-- ALIAS RIBIL
           03 (SF)-ALIAS-RIBIL                  PIC X(3) VALUE 'P79'.
      *-- ALIAS ASSICURATI
           03 (SF)-ALIAS-ASSICURATI             PIC X(3) VALUE 'P80'.
      *-- ALIAS PRESTAZ-PRE-ASS
           03 (SF)-ALIAS-PRESTAZ-PRE-ASS        PIC X(3) VALUE 'P82'.
      *-- ALIAS PROD-PTF
           03 (SF)-ALIAS-PROD-PTF               PIC X(3) VALUE 'P83'.
      *-- ALIAS ESTR-CNT-DIAGN-CED
           03 (SF)-ALIAS-ESTR-CNT-DI-CED        PIC X(3) VALUE 'P84'.
      *-- ALIAS ESTR-CNT-DIAGN-RIV
           03 (SF)-ALIAS-ESTR-CNT-DI-RIV        PIC X(3) VALUE 'P85'.
      *-- ALIAS NDG-PARTNER
           03 (SF)-ALIAS-NDG-PARTNER            PIC X(3) VALUE 'A63'.
      *-- ALIAS MOT-LIQ
           03 (SF)-ALIAS-MOT-LIQ                PIC X(3) VALUE 'P86'.
      *-- ALIAS TP-MOT-RISC
           03 (SF)-ALIAS-TP-MOT-RISC            PIC X(3) VALUE 'P87'.
      *-- ALIAS ATT-SERV-VAL
           03 (SF)-ALIAS-ATT-SERV-VAL           PIC X(3) VALUE 'P88'.
      *-- ALIAS D-ATT-SERV-VAL
           03 (SF)-ALIAS-D-ATT-SERV-VAL         PIC X(3) VALUE 'P89'.
      *-- ALIAS TP-SERV-VAL
           03 (SF)-ALIAS-TP-SERV-VAL            PIC X(3) VALUE 'P90'.
      *-- ALIAS CALC-STOP-LOSS
           03 (SF)-ALIAS-CALC-STOP-LOSS         PIC X(3) VALUE 'P91'.
      *-- ALIAS EVE-SERV-VAL
           03 (SF)-ALIAS-EVE-SERV-VAL           PIC X(3) VALUE 'P92'.
      *-- ALIAS PROD-FND-STOP-LOSS
           03 (SF)-ALIAS-PROD-FND-ST-LO         PIC X(3) VALUE 'P93'.
      *-- ALIAS TP-STAT-E-SERV-VAL
           03 (SF)-ALIAS-TP-ST-E-SER-VAL        PIC X(3) VALUE 'P94'.
      *-- ALIAS PDM-CONF-ELAB
           03 (SF)-ALIAS-PDM-CONF-ELAB          PIC X(3) VALUE 'P95'.
      *-- ALIAS PDM-CONF-GAP-IM
           03 (SF)-ALIAS-PDM-CONF-GAP-IM        PIC X(3) VALUE 'P96'.
      *-- ALIAS PDM-CONF-SCARTO
           03 (SF)-ALIAS-PDM-CONF-SCARTO        PIC X(3) VALUE 'P97'.
      *-- ALIAS PDM-POLI-FORZ
           03 (SF)-ALIAS-PDM-POLI-FORZ          PIC X(3) VALUE 'P98'.
      *-- ALIAS PDM-CONF-ELAB-B
           03 (SF)-ALIAS-PDM-CONF-ELAB-B        PIC X(3) VALUE 'M75'.
      *-- ALIAS PDM-STRC-ELAB
           03 (SF)-ALIAS-PDM-STRC-ELAB          PIC X(3) VALUE 'M76'.
      *-- ALIAS PDM-STAT-ELAB
           03 (SF)-ALIAS-PDM-STAT-ELAB          PIC X(3) VALUE 'M78'.
      *-- ALIAS MATR_OBBL_ATTB
           03 (SF)-ALIAS-MATR-OBBL-ATTB         PIC X(3) VALUE 'V01'.
      *-- ALIAS LIV-RSH-IVASS-NAZ
           03 (SF)-ALIAS-LV-RSH-IVAS-NAZ        PIC X(3) VALUE 'V02'.
      *-- ALIAS TP-ADEG-VER
           03 (SF)-ALIAS-TP-ADEG-VER            PIC X(3) VALUE 'V03'.
      *-- ALIAS TP-AZNAZ-RSH-IVASS
           03 (SF)-ALIAS-TP-AZN-RSH-IVAS        PIC X(3) VALUE 'V04'.
      *-- ALIAS TP-COND-CLIENTE
           03 (SF)-ALIAS-TP-COND-CLIENTE        PIC X(3) VALUE 'V05'.
      *-- ALIAS TP-DEST-FND
           03 (SF)-ALIAS-TP-DEST-FND            PIC X(3) VALUE 'V06'.
      *-- ALIAS TP-FRM-GIUR-SAV
           03 (SF)-ALIAS-TP-FRM-GIUR-SAV        PIC X(3) VALUE 'V07'.
      *-- ALIAS TP-NATURA-OPRZ
           03 (SF)-ALIAS-TP-NATURA-OPRZ         PIC X(3) VALUE 'V08'.
      *-- ALIAS TP-LEG-CNTR
           03 (SF)-ALIAS-TP-LEG-CNTR            PIC X(3) VALUE 'V09'.
      *-- ALIAS TP-OPER-ESTERO
           03 (SF)-ALIAS-TP-OPER-ESTERO         PIC X(3) VALUE 'V10'.
      *-- ALIAS TP-ORGN-FND
           03 (SF)-ALIAS-TP-ORGN-FND            PIC X(3) VALUE 'V11'.
      *-- ALIAS TP-PEP
           03 (SF)-ALIAS-TP-PEP                 PIC X(3) VALUE 'V12'.
      *-- ALIAS TP-PRFL-RSH-PEP
           03 (SF)-ALIAS-TP-PRFL-RSH-PEP        PIC X(3) VALUE 'V13'.
      *-- ALIAS TP-PRINC-FNT-REDD
           03 (SF)-ALIAS-TP-PRIN-FNT-RED        PIC X(3) VALUE 'V14'.
      *-- ALIAS TP-RELA-ESEC
           03 (SF)-ALIAS-TP-RELA-ESEC           PIC X(3) VALUE 'V16'.
      *-- ALIAS TP-SCO-FIN-RAPP
           03 (SF)-ALIAS-TP-SCO-FIN-RAPP        PIC X(3) VALUE 'V17'.
      *-- ALIAS TP-SIT-FIN-PAT
           03 (SF)-ALIAS-TP-SIT-FIN-PAT         PIC X(3) VALUE 'V18'.
      *-- ALIAS TP-SIT-FAM-CONV
           03 (SF)-ALIAS-TP-SIT-FAM-CONV        PIC X(3) VALUE 'V19'.
      *-- ALIAS TP-SIT-GIUR
           03 (SF)-ALIAS-TP-SIT-GIUR            PIC X(3) VALUE 'V20'.
      *-- ALIAS TP-SOC
           03 (SF)-ALIAS-TP-SOC                 PIC X(3) VALUE 'V21'.
      *-- ALIAS LIV-RSH-IVASS-PEP
           03 (SF)-ALIAS-LV-RSH-IVAS-PEP        PIC X(3) VALUE 'V22'.
      *-- ALIAS TP-VALUT-COLL
           03 (SF)-ALIAS-TP-VALUT-COLL          PIC X(3) VALUE 'V23'.
      *-- ALIAS VAL-PMC
           03 (SF)-ALIAS-VAL-PMC                PIC X(3) VALUE 'V24'.
      *-- ALIAS TP-MOT-ASS-TIT-EFF
           03 (SF)-ALIAS-TP-MOT-ASS-TIT     PIC X(3) VALUE 'V25'.
      *-- ALIAS TP-RAG-RAPP
           03 (SF)-ALIAS-TP-RAG-RAPP            PIC X(3) VALUE 'V26'.

      ******************************************************************
      *MEDIOLANUM ADEGUAMENTO
      ******************************************************************

      *--  ALIAS EST ADES
           03 (SF)-ALIAS-EST-ADES               PIC X(3) VALUE 'E01'.
      *--  ALIAS EST DETT TCONT
           03 (SF)-ALIAS-EST-DETT-TCONT         PIC X(3) VALUE 'E02'.
      *--  ALIAS EST GAR
           03 (SF)-ALIAS-EST-GAR                PIC X(3) VALUE 'E04'.
      *--  ALIAS EST PARAM COMP
           03 (SF)-ALIAS-EST-PARAM-COMP         PIC X(3) VALUE 'E05'.
      *--  ALIAS EST POLI
           03 (SF)-ALIAS-EST-POLI               PIC X(3) VALUE 'E06'.
      *--  ALIAS EST TIT CONT
           03 (SF)-ALIAS-EST-TIT-CONT           PIC X(3) VALUE 'E10'.
      *--  ALIAS EST TRCH DI GAR
           03 (SF)-ALIAS-EST-TRCH-DI-GAR        PIC X(3) VALUE 'E12'.
      *--  ALIAS EST VINPG
           03 (SF)-ALIAS-EST-VIN-PG             PIC X(3) VALUE 'E14'.
      *--  ALIAS EST PARAM MOVI
           03 (SF)-ALIAS-EST-PARAM-MOVI         PIC X(3) VALUE 'E16'.
      *--  ALIAS EST RAPP ANA
           03 (SF)-ALIAS-EST-RAPP-ANA           PIC X(3) VALUE 'E15'.
      *--  ALIAS EST RIS DI TRCH
           03 (SF)-ALIAS-EST-RIS-DI-TRCH        PIC X(3) VALUE 'E18'.
      *--  ALIAS EST BL FND CALC
           03 (SF)-ALIAS-EST-BL-FND-CALC        PIC X(3) VALUE 'E19'.
      *--  ALIAS EST BL TRC CALC
           03 (SF)-ALIAS-EST-BL-TRC-CALC        PIC X(3) VALUE 'E20'.
      *--  ALIAS EST LIQ
           03 (SF)-ALIAS-EST-LIQ                PIC X(3) VALUE 'E21'.
      *--  ALIAS EST BNFICR LIQ
           03 (SF)-ALIAS-EST-BNFICR-LIQ         PIC X(3) VALUE 'E22'.
      *--  ALIAS EST D FORZ LIQ
           03 (SF)-ALIAS-EST-D-FORZ-LIQ         PIC X(3) VALUE 'E23'.
      *--  ALIAS EST IPRODFND
           03 (SF)-ALIAS-EST-IPRODFND           PIC X(3) VALUE 'E24'.
      *--  ALIAS EST IPROFILI
           03 (SF)-ALIAS-EST-IPROFILI           PIC X(3) VALUE 'E25'.
      *--  ALIAS COEFF INDEX
           03 (SF)-ALIAS-COEFF-INDEX            PIC X(3) VALUE 'L08'.
      *--  ALIAS INTF DETT FND
           03 (SF)-ALIAS-INTF-DETT-FND          PIC X(3) VALUE 'L10'.
      *--  ALIAS INTF MOVI FINRI
           03 (SF)-ALIAS-INTF-MOVI-FINRI        PIC X(3) VALUE 'L13'.
      *--  ALIAS TP STAT AMPLIAM
           03 (SF)-ALIAS-TP-STAT-AMPLIAM        PIC X(3) VALUE 'L21'.
      *--  ALIAS OPRZ FREEDOM
           03 (SF)-ALIAS-OPRZ-FREEDOM           PIC X(3) VALUE 'L35'.
      *--  ALIAS TP COD OPRZ
           03 (SF)-ALIAS-TP-COD-OPRZ            PIC X(3) VALUE 'L43'.
      *--  ALIAS TP COD SCARTO
           03 (SF)-ALIAS-TP-COD-SCARTO          PIC X(3) VALUE 'L44'.
      *--  ALIAS TP MSTAT CNTRT ELE
           03 (SF)-ALIAS-TP-MST-CNT-ELE         PIC X(3) VALUE 'L46'.
      *--  ALIAS AMPLIAM
           03 (SF)-ALIAS-AMPLIAM                PIC X(3) VALUE 'L63'.
      *--  ALIAS COSTI
           03 (SF)-ALIAS-COSTI                  PIC X(3) VALUE 'L64'.
      *--  ALIAS PREMI
           03 (SF)-ALIAS-PREMI                  PIC X(3) VALUE 'L66'.
      *--  ALIAS CNTRT ELE
           03 (SF)-ALIAS-CNTRT-ELE              PIC X(3) VALUE 'L68'.
      *--  ALIAS COMPL-CNTRT-ELE
           03 (SF)-ALIAS-COMPL-CNTRT-ELE        PIC X(3) VALUE 'L69'.
      *--  ALIAS TP STAT CNTRT ELE
           03 (SF)-ALIAS-TP-ST-CNT-ELE          PIC X(3) VALUE 'L72'.
      *--  ALIAS DETT PROV TBN
           03 (SF)-ALIAS-DETT-PROV-TBN          PIC X(3) VALUE 'L73'.
      *--  ALIAS COD INTERFACCE
           03 (SF)-ALIAS-COD-INTERFACCE         PIC X(3) VALUE 'L74'.
      *--  ALIAS TRASC COD
           03 (SF)-ALIAS-TRASC-COD              PIC X(3) VALUE 'L75'.
      *--  ALIAS TRASC PARAM
           03 (SF)-ALIAS-TRASC-PARAM            PIC X(3) VALUE 'L76'.
      *--  ALIAS PTF DIN
           03 (SF)-ALIAS-PTF-DIN                PIC X(3) VALUE 'L77'.
      *--  ALIAS PRE VIRT
           03 (SF)-ALIAS-PRE-VIRT               PIC X(3) VALUE 'L78'.
      *--  ALIAS PRSTZ
           03 (SF)-ALIAS-PRSTZ                  PIC X(3) VALUE 'L79'.
      *--  ALIAS SIN
           03 (SF)-ALIAS-SIN                    PIC X(3) VALUE 'L80'.
      *--  ALIAS TP FOCAL POINT
           03 (SF)-ALIAS-TP-FOCAL-POINT         PIC X(3) VALUE 'L81'.
      *--  ALIAS RICH FREEDOM
           03 (SF)-ALIAS-RICH-FREEDOM           PIC X(3) VALUE 'L59'.
      *--  ALIAS TP STAT VIRT
           03 (SF)-ALIAS-TP-STAT-VIRT           PIC X(3) VALUE 'L85'.
      *--  ALIAS TP PROVZA AMPLIAM
           03 (SF)-ALIAS-TP-PROV-AMPL           PIC X(3) VALUE 'L87'.
      *--  ALIAS TP PRE ANNU PRGT
           03 (SF)-ALIAS-TP-PR-AN-PRGT          PIC X(3) VALUE 'L88'.
      *--  ALIAS TP STAT PRE ANNU P
           03 (SF)-ALIAS-TP-STA-PRE-AN-P        PIC X(3) VALUE 'L89'.
      *--  ALIAS TP STAT SIN
           03 (SF)-ALIAS-TP-STAT-SIN            PIC X(3) VALUE 'L90'.
      *--  ALIAS INTF POLI FIP
           03 (SF)-ALIAS-INTF-POLI-FIP          PIC X(3) VALUE 'L92'.
      *--  ALIAS TP AMPLIAM
           03 (SF)-ALIAS-TP-AMPLIAM             PIC X(3) VALUE 'L93'.
      *--  ALIAS TP MOTIV SCARTO
           03 (SF)-ALIAS-TP-MOTIV-SCARTO        PIC X(3) VALUE 'L95'.
      *--  ALIAS SIN
           03 (SF)-ALIAS-SIN                    PIC X(3) VALUE 'SIN'.
      *--  ALIAS DATI MDG
           03 (SF)-ALIAS-DATI-MDG               PIC X(3) VALUE 'P06'.
      *--  ALIAS EST DETT TRASFE
           03 (SF)-ALIAS-EST-DETT-TRASFE        PIC X(3) VALUE 'P17'.
      *--  ALIAS RICH COMNZ
           03 (SF)-ALIAS-RICH-COMNZ             PIC X(3) VALUE 'P20'.
      *--  ALIAS TAB MODELLI ID MDG
           03 (SF)-ALIAS-TAB-MOD-ID-MDG         PIC X(3) VALUE 'P09'.

      ******************************************************************
      *BNL ADEGUAMENTO
      ******************************************************************
      *--  ALIAS PDM GAR UL ESTR
           03 (SF)-ALIAS-PDM-GAR-UL-ESTR        PIC X(3) VALUE 'L36'.
      *--  ALIAS PDM TRCH ESTR
           03 (SF)-ALIAS-PDM-TRCH-ESTR          PIC X(3) VALUE 'L37'.
      *--  ALIAS PDM VAR CALC P
           03 (SF)-ALIAS-PDM-VAR-CALC-P         PIC X(3) VALUE 'L38'.
      *--  ALIAS PDM VAR CALC T
           03 (SF)-ALIAS-PDM-VAR-CALC-T         PIC X(3) VALUE 'L39'.
      *--  ALIAS SEGMENTAZ CLI
           03 (SF)-ALIAS-SEGMENTAZ-CLI          PIC X(3) VALUE 'L54'.
      *--  ALIAS TP MACROSEGMENTO
           03 (SF)-ALIAS-TP-MACROSEG            PIC X(3) VALUE 'L55'.
      *--  ALIAS TP MERCATO
           03 (SF)-ALIAS-TP-MERCATO             PIC X(3) VALUE 'L56'.
      *--  ALIAS TP SEGMENTO
           03 (SF)-ALIAS-TP-SEGMENTO            PIC X(3) VALUE 'L57'.
      *--  ALIAS BILA PROV AMM D
           03 (SF)-ALIAS-BILA-PROV-AMM-D        PIC X(3) VALUE 'P25'.
      *--  ALIAS BILA PROV AMM T
           03 (SF)-ALIAS-BILA-PROV-AMM-T        PIC X(3) VALUE 'P26'.
12969 *--  ALIAS BILA PROV AMM T
12969      03 (SF)-ALIAS-AREA-VAR-X-GAR         PIC X(3) VALUE 'VXG'.
12807 *--  ALIAS DETT DEROGA
12807      03 (SF)-ALIAS-DETT-DEROGA            PIC X(3) VALUE 'P99'.
      *--  ALIAS TP FORZ
           03 (SF)-ALIAS-TP-FORZ                PIC X(3) VALUE 'P05'.
13382 *--  ALIAS AUT DUE DIL
13382      03 (SF)-ALIAS-AUT-DUE-DIL            PIC X(3) VALUE 'V33'.
13382 *--  ALIAS TP CAR FIN GIUR AT
13382      03 (SF)-ALIAS-TP-CAR-FIN-GIUR        PIC X(3) VALUE 'V28'.
13382 *--  ALIAS TP DT 1O CON CLI
13382      03 (SF)-ALIAS-TP-DT-1O-CLI           PIC X(3) VALUE 'V32'.
13382 *--  ALIAS TP FIN COST
13382 *--  03 (SF)-ALIAS-TP-FIN-COST            PIC X(3) VALUE 'V27'.
13382 *--  ALIAS TP LEG PERC BNFICR
13382      03 (SF)-ALIAS-TPLEG-PC-BNFICR        PIC X(3) VALUE 'V34'.
13382 *--  ALIAS TP MOD EN RELA INT
13382      03 (SF)-TP-MOD-EN-RELA-INT           PIC X(3) VALUE 'V35'.
13382 *--  ALIAS TP NOT PREG PROC P
13382      03 (SF)-ALIAS-TP-NOT-PREG            PIC X(3) VALUE 'V36'.
13382 *--  ALIAS TP OPER SOC FID
13382      03 (SF)-ALIAS-TP-SOC-FID             PIC X(3) VALUE 'V37'.
13382 *--  ALIAS TP PAT AZ
13382      03 (SF)-ALIAS-TP-PAT-AZ              PIC X(3) VALUE 'V38'.
13382 *--  ALIAS TP REDD ANNU
13382      03 (SF)-ALIAS-TP-REDD-ANNU           PIC X(3) VALUE 'V31'.
13382 *--  ALIAS TP STATUS AEOI
13382      03 (SF)-ALIAS-TP-STATUS-AEOI         PIC X(3) VALUE 'V29'.
13382 *--  ALIAS TP STATUS FATCA
13382      03 (SF)-ALIAS-TP-STATUS-FATCA        PIC X(3) VALUE 'V30'.
13382 *--  ALIAS TP UTILE ESERCIZIO
13382      03 (SF)-ALIAS-TP-UTILE-ESER          PIC X(3) VALUE 'V39'.
13382 *--  ALIAS AZIONISTA-MAG
13382      03 (SF)-ALIAS-AZIONISTA-MAG          PIC X(3) VALUE 'A67'.
13382 *--  ALIAS RESIDENZA-FISC
13382      03 (SF)-ALIAS-RESIDENZA-FISC         PIC X(3) VALUE 'A68'.
13382 *--  ALIAS LIV-RSH-DUE-DIL-NA
13382      03 (SF)-ALIAS-LIVRSH-DUE-NA          PIC X(3) VALUE 'V40'.
      *--  ALIAS ANA-INTERF-MON
           03 (SF)-ALIAS-ANA-INTERF-MON         PIC X(3) VALUE 'M01'.
      *--  ALIAS RICH-ESTRAZ-MON
           03 (SF)-ALIAS-RICH-ESTRAZ-MON        PIC X(3) VALUE 'M02'.
FNZ   *--  ALIAS AREA FNZ
FNZ        03 (SF)-ALIAS-VAR-CONTEST            PIC X(3) VALUE 'VCO'.
15591 *-- ALIAS RIV-TRCH-ESTR
15591      03 (SF)-ALIAS-RIV-TRCH-ESTR          PIC X(3) VALUE 'M36'.
15591 *-- ALIAS RIV-VAR-CALC-P
15591      03 (SF)-ALIAS-RIV-VAR-CALC-P         PIC X(3) VALUE 'M38'.
15591 *-- ALIAS RIV-VAR-CALC-T
15591      03 (SF)-ALIAS-RIV-VAR-CALC-T         PIC X(3) VALUE 'M39'.
FNZF2 *-- ALIAS FNZ-EXT-ACC-ID
FNZF2      03 (SF)-ALIAS-FNZ-EXT-ACC-ID         PIC X(3) VALUE 'M42'.
FNZF2 *-- ALIAS FNZ-STANDBY
FNZF2      03 (SF)-ALIAS-FNZ-STANDBY            PIC X(3) VALUE 'M43'.
FNZF2 *-- ALIAS FNZ-MATR-CONC
FNZF2      03 (SF)-ALIAS-FNZ-MATR-CONC          PIC X(3) VALUE 'M44'.
15592 *-- ALIS TP-NORMAL-BNFIC
15592      03 (SF)-ALIAS-TP-NORMAL-BNFIC        PIC X(3) VALUE 'V41'.
15592 *-- ALIAS TP-MOT-CAMBIO-CNTR
15592      03 (SF)-ALIAS-TP-MOT-CAMB-CC         PIC X(3) VALUE 'V42'.
      *-- ALIAS COSTI-IDD
           03 (SF)-ALIAS-COSTI-IDD              PIC X(3) VALUE 'M46'.
      *-- ALIAS DETT-COSTI-IDD
           03 (SF)-ALIAS-DETT-COSTI-IDD         PIC X(3) VALUE 'M47'.
16324 *-- ALIAS FONDI PER COMMIS GEST
16324      03 (SF)-ALIAS-AREA-FND-X-CDG         PIC X(3) VALUE 'FXC'.
AMLF3 *-- ALIAS FONDI PER COMMIS GEST
AMLF3      03 (SF)-ALIAS-TERZO-REFERENTE        PIC X(3) VALUE 'M50'.
      *-- ALIAS COINTESTATARI CONTO CORRENTE
           03 (SF)-ALIAS-COINTESTATARI          PIC X(3) VALUE 'M79'.
      *-- ALIAS COINTESTATARI CONTO CORRENTE
           03 (SF)-ALIAS-STRA-ACTV-PTF          PIC X(3) VALUE 'M83'.
      *-- ALIAS COINTESTATARI CONTO CORRENTE
           03 (SF)-ALIAS-D-STRA-ACTV-PTF        PIC X(3) VALUE 'M84'.
      *-- ALIAS COINTESTATARI CONTO CORRENTE
           03 (SF)-ALIAS-STAT-STRA-AP           PIC X(3) VALUE 'M87'.

       01 LDBV2091.
          05 LDBV2091-ID-OGG                  PIC S9(9) COMP-3.
          05 LDBV2091-TP-OGG                  PIC X(002).
          05 LDBV2091-TP-STAT-TIT-01          PIC X(002).
          05 LDBV2091-TP-STAT-TIT-02          PIC X(002).
          05 LDBV2091-TP-STAT-TIT-03          PIC X(002).
          05 LDBV2091-TP-STAT-TIT-04          PIC X(002).
          05 LDBV2091-TP-STAT-TIT-05          PIC X(002).
          05 LDBV2091-TOT-PREMI               PIC S9(15)V9(003) COMP-3.

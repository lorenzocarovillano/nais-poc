
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVVFC5
      *   ULTIMO AGG. 18 SET 2007
      *------------------------------------------------------------

       VAL-DCLGEN-VFC.
           MOVE (SF)-IDCOMP
              TO VFC-IDCOMP
           MOVE (SF)-CODPROD
              TO VFC-CODPROD
           MOVE (SF)-CODTARI
              TO VFC-CODTARI
           MOVE (SF)-DINIZ
              TO VFC-DINIZ
           MOVE (SF)-NOMEFUNZ
              TO VFC-NOMEFUNZ
           MOVE (SF)-DEND
              TO VFC-DEND
           IF (SF)-ISPRECALC-NULL = HIGH-VALUES
              MOVE (SF)-ISPRECALC-NULL
              TO VFC-ISPRECALC-NULL
           ELSE
              MOVE (SF)-ISPRECALC
              TO VFC-ISPRECALC
           END-IF
           MOVE (SF)-NOMEPROGR
              TO VFC-NOMEPROGR
           MOVE (SF)-MODCALC
              TO VFC-MODCALC
           MOVE (SF)-GLOVARLIST
              TO VFC-GLOVARLIST
           MOVE (SF)-STEP-ELAB
              TO VFC-STEP-ELAB.
       VAL-DCLGEN-VFC-EX.
           EXIT.

      ******************************************************************
      * COPY IO LDBSG780
      ******************************************************************
       01  LDBVG781.
           05 LDBVG781-GRAV-FUNZ-FUNZ-1         PIC  X(02).
           05 LDBVG781-GRAV-FUNZ-FUNZ-2         PIC  X(02).
           05 LDBVG781-GRAV-FUNZ-FUNZ-3         PIC  X(02).
           05 LDBVG781-GRAV-FUNZ-FUNZ-4         PIC  X(02).
           05 LDBVG781-GRAV-FUNZ-FUNZ-5         PIC  X(02).
           05 LDBVG781-FL-POLI-IFP              PIC  X(01).


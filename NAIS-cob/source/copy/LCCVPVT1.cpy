      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA PROV_DI_GAR
      *   ALIAS PVT
      *   ULTIMO AGG. 21 NOV 2013
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-PROV-DI-GAR PIC S9(9)     COMP-3.
             07 (SF)-ID-GAR PIC S9(9)     COMP-3.
             07 (SF)-ID-GAR-NULL REDEFINES
                (SF)-ID-GAR   PIC X(5).
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-TP-PROV PIC X(2).
             07 (SF)-TP-PROV-NULL REDEFINES
                (SF)-TP-PROV   PIC X(2).
             07 (SF)-PROV-1AA-ACQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-1AA-ACQ-NULL REDEFINES
                (SF)-PROV-1AA-ACQ   PIC X(8).
             07 (SF)-PROV-2AA-ACQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-2AA-ACQ-NULL REDEFINES
                (SF)-PROV-2AA-ACQ   PIC X(8).
             07 (SF)-PROV-RICOR PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-RICOR-NULL REDEFINES
                (SF)-PROV-RICOR   PIC X(8).
             07 (SF)-PROV-INC PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-INC-NULL REDEFINES
                (SF)-PROV-INC   PIC X(8).
             07 (SF)-ALQ-PROV-ACQ PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-PROV-ACQ-NULL REDEFINES
                (SF)-ALQ-PROV-ACQ   PIC X(4).
             07 (SF)-ALQ-PROV-INC PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-PROV-INC-NULL REDEFINES
                (SF)-ALQ-PROV-INC   PIC X(4).
             07 (SF)-ALQ-PROV-RICOR PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-PROV-RICOR-NULL REDEFINES
                (SF)-ALQ-PROV-RICOR   PIC X(4).
             07 (SF)-FL-STOR-PROV-ACQ PIC X(1).
             07 (SF)-FL-STOR-PROV-ACQ-NULL REDEFINES
                (SF)-FL-STOR-PROV-ACQ   PIC X(1).
             07 (SF)-FL-REC-PROV-STORN PIC X(1).
             07 (SF)-FL-REC-PROV-STORN-NULL REDEFINES
                (SF)-FL-REC-PROV-STORN   PIC X(1).
             07 (SF)-FL-PROV-FORZ PIC X(1).
             07 (SF)-FL-PROV-FORZ-NULL REDEFINES
                (SF)-FL-PROV-FORZ   PIC X(1).
             07 (SF)-TP-CALC-PROV PIC X(1).
             07 (SF)-TP-CALC-PROV-NULL REDEFINES
                (SF)-TP-CALC-PROV   PIC X(1).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-REMUN-ASS PIC S9(12)V9(3) COMP-3.
             07 (SF)-REMUN-ASS-NULL REDEFINES
                (SF)-REMUN-ASS   PIC X(8).
             07 (SF)-COMMIS-INTER PIC S9(12)V9(3) COMP-3.
             07 (SF)-COMMIS-INTER-NULL REDEFINES
                (SF)-COMMIS-INTER   PIC X(8).
             07 (SF)-ALQ-REMUN-ASS PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-REMUN-ASS-NULL REDEFINES
                (SF)-ALQ-REMUN-ASS   PIC X(4).
             07 (SF)-ALQ-COMMIS-INTER PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-COMMIS-INTER-NULL REDEFINES
                (SF)-ALQ-COMMIS-INTER   PIC X(4).

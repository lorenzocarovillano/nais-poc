       01 LCCV0021.
          05 LCCV0021-PGM                          PIC X(08).
          05 LCCV0021-AREA-INPUT.
             10 LCCV0021-TP-MOV-PTF                PIC 9(005).

          05 LCCV0021-AREA-OUTPUT.
             10 LCCV0021-TP-MOV-ACT                PIC 9(005).
             10 LCCV0021-COD-PROCESSO-WF           PIC X(003).
      *-- return code
             10 LCCV0021-RETURN-CODE               PIC  X(002).
                88 LCCV0021-SUCCESSFUL-RC          VALUE '00'.
                88 LCCV0021-SQL-ERROR              VALUE 'D3'.
      *-- sqlcode
             10 LCCV0021-SQLCODE                   PIC S9(009).
                88 LCCV0021-SUCCESSFUL-SQL         VALUE ZERO.
                88 LCCV0021-NOT-FOUND              VALUE +100.
                88 LCCV0021-DUPLICATE-KEY          VALUE -803.
                88 LCCV0021-MORE-THAN-ONE-ROW     VALUE -811.
                88 LCCV0021-DEADLOCK-TIMEOUT       VALUE -911
                                                     -913.
                88 LCCV0021-CONNECTION-ERROR       VALUE -924.

      *--campi esito
             10 LCCV0021-CAMPI-ESITO.
                15 LCCV0021-DESCRIZ-ERR            PIC  X(300).
                15 LCCV0021-COD-SERVIZIO-BE        PIC  X(008).
                15 LCCV0021-NOME-TABELLA           PIC  X(018).
                15 LCCV0021-KEY-TABELLA            PIC  X(020).


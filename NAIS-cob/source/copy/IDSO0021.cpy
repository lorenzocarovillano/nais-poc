      ***************************************************************
      * Area Output
      * Data Mapper - Lettura Struttura Dato
      * LUNGHEZZA : 691
      ***************************************************************


       02 IDSO0021-AREA.
          03 IDSO0021-LIMITE-STR-MAX              PIC 9(05) VALUE 250.
          03 IDSO0021-NUM-ELEM                    PIC 9(05) VALUE 0.
          03 IDSO0021-OUTPUT.
             05 IDSO0021-STRUTTURA-DATO.
      * -- STRUTTURA DATI
      *          COPY IDSV0042 REPLACING ==IDSO0021== BY ==IDSO0021==.
      *   inizio COPY IDSV0042
                10 IDSO0021-ELEMENTS-STR-DATO OCCURS 250 TIMES
                                          INDEXED BY IDSO0021-IND.
                   15 IDSO0021-CODICE-DATO            PIC X(030).
                   15 IDSO0021-FLAG-KEY               PIC X(001).
                   15 IDSO0021-FLAG-RETURN-CODE       PIC X(001).
                   15 IDSO0021-FLAG-CALL-USING        PIC X(001).
                   15 IDSO0021-FLAG-WHERE-COND        PIC X(001).
                   15 IDSO0021-FLAG-REDEFINES         PIC X(001).
                   15 IDSO0021-TIPO-DATO              PIC X(002).
                   15 IDSO0021-INIZIO-POSIZIONE   PIC S9(05) COMP-3.
                   15 IDSO0021-LUNGHEZZA-DATO     PIC S9(05) COMP-3.
                   15 IDSO0021-LUNGHEZZA-DATO-NOM PIC S9(05) COMP-3.
                   15 IDSO0021-PRECISIONE-DATO    PIC S9(02) COMP-3.
                   15 IDSO0021-FORMATTAZIONE-DATO     PIC X(020).
                   15 IDSO0021-SERV-CONVERSIONE       PIC X(008).
                   15 IDSO0021-AREA-CONV-STANDARD     PIC X(001).
                   15 IDSO0021-CODICE-DOMINIO         PIC X(030).
                   15 IDSO0021-SERVIZIO-VALIDAZIONE   PIC X(008).
                   15 IDSO0021-RICORRENZA             PIC S9(05) COMP-3.
                   15 IDSO0021-CLONE                  PIC X(01).
                   15 IDSO0021-OBBLIGATORIETA         PIC X(001).
                   15 IDSO0021-VALORE-DEFAULT         PIC X(010).
      *   fine COPY IDSV0042
             05 IDSO0021-STRUTTURA-DATO-R REDEFINES
                IDSO0021-STRUTTURA-DATO.
                10 FILLER                             PIC X(130).
                10 IDSO0021-RESTO-TAB                 PIC X(32370).

             05 IDSO0021-AREA-ERRORI.
      * --  CAMPI ERRORI
      *          COPY IDSV0006 REPLACING ==IDSO0021== BY ==IDSO0021==.
      *   inizio COPY IDSV0006

      *   inizio COPY IDSV0004
                10 IDSO0021-RETURN-CODE               PIC  X(002).
                   88 IDSO0021-SUCCESSFUL-RC           VALUE '00'.
                   88 IDSO0021-INVALID-LEVEL-OPER      VALUE 'D1'.
                   88 IDSO0021-INVALID-OPER            VALUE 'D2'.
                   88 IDSO0021-SQL-ERROR               VALUE 'D3'.
                   88 IDSO0021-FIELD-NOT-VALUED        VALUE 'C1'.
                   88 IDSO0021-INVALID-CONVERSION      VALUE 'C2'.
                   88 IDSO0021-SEQUENCE-NOT-FOUND      VALUE 'S1'.
                   88 IDSO0021-COD-COMP-NOT-VALID      VALUE 'ZA'.
                   88 IDSO0021-STR-DATO-NOT-VALID      VALUE 'ZB'.
                   88 IDSO0021-BUFFER-DATI-NOT-V       VALUE 'ZC'.
                   88 IDSO0021-STR-DATO-DB-NOT-F       VALUE 'ZD'.
                   88 IDSO0021-COD-SERV-NOT-V          VALUE 'ZE'.
                   88 IDSO0021-EXCESS-COPY-I-O         VALUE 'ZF'.
                   88 IDSO0021-TIPO-MOVIM-NOT-V        VALUE 'ZG'.
                   88 IDSO0021-OPER-NOT-V              VALUE 'ZH'.
                   88 IDSO0021-SH-MEMORY-NOT-V         VALUE 'ZI'.
                   88 IDSO0021-QUEUE-MANAGEMENT        VALUE 'ZL'.
                   88 IDSO0021-SH-MEMORY-FLOOD         VALUE 'ZM'.
                   88 IDSO0021-EXCESS-SERV-NEWLIFE     VALUE 'ZO'.
                   88 IDSO0021-EXCESS-SERV-VSAM        VALUE 'ZP'.
                   88 IDSO0021-KEY-FIELDS-NOT-F        VALUE 'ZQ'.
                   88 IDSO0021-WHERE-FIELDS-NOT-F      VALUE 'ZR'.
                   88 IDSO0021-SERV-NOT-F-ON-MSS       VALUE 'ZS'.
                   88 IDSO0021-STR-RED-NOT-F-ON-RDS    VALUE 'ZT'.
                   88 IDSO0021-INSERT-SH-MEMORY        VALUE 'ZU'.
                   88 IDSO0021-CONVERTER-NOT-F         VALUE 'ZV'.
                   88 IDSO0021-EXCESS-OF-RECURSION     VALUE 'ZW'.
                   88 IDSO0021-VALUE-DEFAULT-NOT-F     VALUE 'ZX'.
                   88 IDSO0021-COPY-NOT-F-ON-IOS       VALUE 'ZY'.
                   88 IDSO0021-VALUE-NOT-F-ON-VSS      VALUE 'ZZ'.
      *   fine COPY IDSV0004

                10 IDSO0021-SQLCODE              PIC S9(009).
                   88 IDSO0021-SUCCESSFUL-SQL    VALUE ZERO.
                   88 IDSO0021-NOT-FOUND         VALUE +100.
                   88 IDSO0021-DUPLICATE-KEY     VALUE -803.
                   88 IDSO0021-MORE-THAN-ONE-ROW VALUE -811.
                   88 IDSO0021-DEADLOCK-TIMEOUT  VALUE -911
                                                   -913.
                   88 IDSO0021-CONNECTION-ERROR  VALUE -924.

                10 IDSO0021-CAMPI-ESITO.
                   15 IDSO0021-DESCRIZ-ERR-DB2      PIC  X(300).
                   15 IDSO0021-COD-SERVIZIO-BE      PIC  X(008).
                   15 IDSO0021-NOME-TABELLA         PIC  X(018).
                   15 IDSO0021-KEY-TABELLA          PIC  X(020).
                   15 IDSO0021-NUM-RIGHE-LETTE      PIC  9(002).
      *   fine COPY IDSV0006





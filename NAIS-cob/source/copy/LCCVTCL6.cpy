      *----------------------------------------------------------------*
      *    COPY      ..... LCCVTCL6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO TITOLO CONTABILE LIQUIDAZIONE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-TCONT-LIQ.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE TCONT-LIQ

      *--> NOME TABELLA FISICA DB
           MOVE 'TCONT-LIQ'                  TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WTCL-ST-INV(IX-TAB-TCL)
              AND WTCL-ELE-TIT-CONT-MAX  NOT = 0

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WTCL-ST-ADD(IX-TAB-TCL)

                    PERFORM ESTR-SEQUENCE
                       THRU ESTR-SEQUENCE-EX

                    IF IDSV0001-ESITO-OK
                       MOVE S090-SEQ-TABELLA  TO WTCL-ID-PTF(IX-TAB-TCL)
                                                 TCL-ID-TCONT-LIQ
                       MOVE WMOV-ID-PTF       TO TCL-ID-MOVI-CRZ
                       MOVE WTCL-ID-BNFICR-LIQ  (IX-TAB-TCL)
                                              TO TCL-ID-BNFICR-LIQ
                       MOVE WTCL-ID-PERC-LIQ    (IX-TAB-TCL)
                                              TO TCL-ID-PERC-LIQ



                    END-IF

      *-->          TIPO OPERAZIONE DISPATCHER
                    SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WTCL-ST-DEL(IX-TAB-TCL)

                    MOVE WTCL-ID-PTF(IX-TAB-TCL)   TO TCL-ID-TCONT-LIQ
                    MOVE WMOV-ID-PTF               TO TCL-ID-MOVI-CHIU

      *-->          TIPO OPERAZIONE DISPATCHER
                    SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->        MODIFICA
                  WHEN WTCL-ST-MOD(IX-TAB-TCL)

                    MOVE WTCL-ID-PTF(IX-TAB-TCL)   TO TCL-ID-TCONT-LIQ
                    MOVE WMOV-ID-PTF               TO TCL-ID-MOVI-CRZ

      *-->          TIPO OPERAZIONE DISPATCHER
                    SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN TITOLO CONTABILE LIQUIDAZIONE
                 PERFORM VAL-DCLGEN-TCL
                    THRU VAL-DCLGEN-TCL-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-TCL
                    THRU VALORIZZA-AREA-DSH-TCL-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-TCONT-LIQ-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-TCL.

      *--> DCLGEN TABELLA
           MOVE TCONT-LIQ               TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT  TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-TCL-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVTCL5.

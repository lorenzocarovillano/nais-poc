      *----------------------------------------------------------------*
      *    COPY      ..... LCCVRDF6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO RICHIESTA DISIVESTIMENTO FONDO
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-RICH-DISINVST-FND.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE RICH-DIS-FND

      *--> NOME TABELLA FISICA DB
           MOVE 'RICH-DIS-FND'                  TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WRDF-ST-INV(IX-TAB-RDF)
              AND WRDF-ELE-RIC-INV-MAX NOT = 0

      *--->   Impostare ID Tabella RICHIESTA INVESTIMENTO FONDO
              MOVE WMOV-ID-PTF              TO RDF-ID-MOVI-CRZ

      *--->   Impostare ID Tabella Titolo Contabile
              MOVE WRDF-ID-MOVI-FINRIO(IX-TAB-RDF)
                TO WS-ID-MOVI-FINRIO

      *--->   Ricerca dell'ID-TIT-CONT sulla Tabella
      *--->   Padre Titolo Contabile
              PERFORM RICERCA-MOVI-FINRIO
                 THRU RICERCA-MOVI-FINRIO-EX

              MOVE WS-ID-PTF
                TO RDF-ID-MOVI-FINRIO

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WRDF-ST-ADD(IX-TAB-RDF)

                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA
                            TO WRDF-ID-PTF(IX-TAB-RDF)
                               RDF-ID-RICH-DIS-FND

                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WRDF-ST-DEL(IX-TAB-RDF)

                       MOVE WRDF-ID-PTF(IX-TAB-RDF)
                         TO RDF-ID-RICH-DIS-FND

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->        MODIFICA
                  WHEN WRDF-ST-MOD(IX-TAB-RDF)

                       MOVE WRDF-ID-PTF(IX-TAB-RDF)
                         TO RDF-ID-RICH-DIS-FND

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN RICHIESTA INVESTIMENTO FONDO
                 PERFORM VAL-DCLGEN-RDF
                    THRU VAL-DCLGEN-RDF-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-RDF
                    THRU VALORIZZA-AREA-DSH-RDF-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-RICH-DISINVST-FND-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-RDF.

      *--> DCLGEN TABELLA
           MOVE RICH-DIS-FND          TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-RDF-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    RICERCA PTF
      *----------------------------------------------------------------*
       RICERCA-MOVI-FINRIO.

           SET NON-TROVATO TO TRUE.

           PERFORM VARYING IX-MFZ FROM 1 BY 1
                     UNTIL IX-MFZ > WMFZ-ELE-MOVI-FINRIO-MAX
                        OR TROVATO

                IF WMFZ-ID-MOVI-FINRIO(IX-MFZ) = WS-ID-MOVI-FINRIO

                   MOVE WMFZ-ID-PTF(IX-MFZ) TO WS-ID-PTF

                   SET TROVATO TO TRUE

                END-IF

           END-PERFORM.

       RICERCA-MOVI-FINRIO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVRDF5.

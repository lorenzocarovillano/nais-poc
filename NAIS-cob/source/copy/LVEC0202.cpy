       01  WK-VAR-LVEC0202.
           03 WK-IB-PROP-APPO               PIC X(40)  VALUE SPACES.
           03 WK-IB-PROP-APPO2              PIC X(40)  VALUE SPACES.
           03 WK-LUNG-CAMPO                 PIC 9(02)  VALUE ZERO.
           03 WK-LUNG-TOT-CAMPO             PIC 9(02)  VALUE ZERO.
           03 WK-NUM-CAR-RIMAN              PIC 9(02)  VALUE ZERO.
           03 WK-NUM-CAR-ZERO               PIC 9(02)  VALUE ZERO.
           03 IX-TAB                        PIC S9(04) COMP.
           03 FG-FINE-CICLO                 PIC X(01)  VALUE SPACES.
              88 FINE-CICLO                 VALUE 'S'.
              88 FINE-CICLO-NO              VALUE 'N'.

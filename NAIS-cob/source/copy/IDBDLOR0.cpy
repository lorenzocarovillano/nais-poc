           EXEC SQL DECLARE LOG_ERRORE TABLE
           (
             ID_LOG_ERRORE       DECIMAL(9, 0) NOT NULL,
             PROG_LOG_ERRORE     DECIMAL(5, 0) NOT NULL,
             ID_GRAVITA_ERRORE   DECIMAL(9, 0) NOT NULL,
             DESC_ERRORE_ESTESA  VARCHAR(200) NOT NULL,
             COD_MAIN_BATCH      CHAR(8) NOT NULL,
             COD_SERVIZIO_BE     CHAR(8) NOT NULL,
             LABEL_ERR           CHAR(30),
             OPER_TABELLA        CHAR(2),
             NOME_TABELLA        CHAR(18),
             STATUS_TABELLA      CHAR(10),
             KEY_TABELLA         VARCHAR(100),
             TIMESTAMP_REG       DECIMAL(18, 0) NOT NULL,
             TIPO_OGGETTO        DECIMAL(2, 0),
             IB_OGGETTO          CHAR(20)
          ) END-EXEC.

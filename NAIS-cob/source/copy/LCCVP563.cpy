
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVP563
      *   ULTIMO AGG. 26 MAR 2019
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-P56.
           MOVE P56-ID-QUEST-ADEG-VER
             TO (SF)-ID-PTF(IX-TAB-P56)
           MOVE P56-ID-QUEST-ADEG-VER
             TO (SF)-ID-QUEST-ADEG-VER(IX-TAB-P56)
           MOVE P56-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-P56)
           MOVE P56-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-P56)
           IF P56-ID-RAPP-ANA-NULL = HIGH-VALUES
              MOVE P56-ID-RAPP-ANA-NULL
                TO (SF)-ID-RAPP-ANA-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-ID-RAPP-ANA
                TO (SF)-ID-RAPP-ANA(IX-TAB-P56)
           END-IF
           MOVE P56-ID-POLI
             TO (SF)-ID-POLI(IX-TAB-P56)
           IF P56-NATURA-OPRZ-NULL = HIGH-VALUES
              MOVE P56-NATURA-OPRZ-NULL
                TO (SF)-NATURA-OPRZ-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-NATURA-OPRZ
                TO (SF)-NATURA-OPRZ(IX-TAB-P56)
           END-IF
           IF P56-ORGN-FND-NULL = HIGH-VALUES
              MOVE P56-ORGN-FND-NULL
                TO (SF)-ORGN-FND-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-ORGN-FND
                TO (SF)-ORGN-FND(IX-TAB-P56)
           END-IF
           IF P56-COD-QLFC-PROF-NULL = HIGH-VALUES
              MOVE P56-COD-QLFC-PROF-NULL
                TO (SF)-COD-QLFC-PROF-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-QLFC-PROF
                TO (SF)-COD-QLFC-PROF(IX-TAB-P56)
           END-IF
           IF P56-COD-NAZ-QLFC-PROF-NULL = HIGH-VALUES
              MOVE P56-COD-NAZ-QLFC-PROF-NULL
                TO (SF)-COD-NAZ-QLFC-PROF-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-NAZ-QLFC-PROF
                TO (SF)-COD-NAZ-QLFC-PROF(IX-TAB-P56)
           END-IF
           IF P56-COD-PRV-QLFC-PROF-NULL = HIGH-VALUES
              MOVE P56-COD-PRV-QLFC-PROF-NULL
                TO (SF)-COD-PRV-QLFC-PROF-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-PRV-QLFC-PROF
                TO (SF)-COD-PRV-QLFC-PROF(IX-TAB-P56)
           END-IF
           IF P56-FNT-REDD-NULL = HIGH-VALUES
              MOVE P56-FNT-REDD-NULL
                TO (SF)-FNT-REDD-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FNT-REDD
                TO (SF)-FNT-REDD(IX-TAB-P56)
           END-IF
           IF P56-REDD-FATT-ANNU-NULL = HIGH-VALUES
              MOVE P56-REDD-FATT-ANNU-NULL
                TO (SF)-REDD-FATT-ANNU-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-REDD-FATT-ANNU
                TO (SF)-REDD-FATT-ANNU(IX-TAB-P56)
           END-IF
           IF P56-COD-ATECO-NULL = HIGH-VALUES
              MOVE P56-COD-ATECO-NULL
                TO (SF)-COD-ATECO-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-ATECO
                TO (SF)-COD-ATECO(IX-TAB-P56)
           END-IF
           IF P56-VALUT-COLL-NULL = HIGH-VALUES
              MOVE P56-VALUT-COLL-NULL
                TO (SF)-VALUT-COLL-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-VALUT-COLL
                TO (SF)-VALUT-COLL(IX-TAB-P56)
           END-IF
           MOVE P56-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-P56)
           MOVE P56-DS-VER
             TO (SF)-DS-VER(IX-TAB-P56)
           MOVE P56-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ(IX-TAB-P56)
           MOVE P56-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-P56)
           MOVE P56-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-P56)
           MOVE P56-LUOGO-COSTITUZIONE
             TO (SF)-LUOGO-COSTITUZIONE(IX-TAB-P56)
           IF P56-TP-MOVI-NULL = HIGH-VALUES
              MOVE P56-TP-MOVI-NULL
                TO (SF)-TP-MOVI-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-MOVI
                TO (SF)-TP-MOVI(IX-TAB-P56)
           END-IF
           IF P56-FL-RAG-RAPP-NULL = HIGH-VALUES
              MOVE P56-FL-RAG-RAPP-NULL
                TO (SF)-FL-RAG-RAPP-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-RAG-RAPP
                TO (SF)-FL-RAG-RAPP(IX-TAB-P56)
           END-IF
           IF P56-FL-PRSZ-TIT-EFF-NULL = HIGH-VALUES
              MOVE P56-FL-PRSZ-TIT-EFF-NULL
                TO (SF)-FL-PRSZ-TIT-EFF-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-PRSZ-TIT-EFF
                TO (SF)-FL-PRSZ-TIT-EFF(IX-TAB-P56)
           END-IF
           IF P56-TP-MOT-RISC-NULL = HIGH-VALUES
              MOVE P56-TP-MOT-RISC-NULL
                TO (SF)-TP-MOT-RISC-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-MOT-RISC
                TO (SF)-TP-MOT-RISC(IX-TAB-P56)
           END-IF
           IF P56-TP-PNT-VND-NULL = HIGH-VALUES
              MOVE P56-TP-PNT-VND-NULL
                TO (SF)-TP-PNT-VND-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-PNT-VND
                TO (SF)-TP-PNT-VND(IX-TAB-P56)
           END-IF
           IF P56-TP-ADEG-VER-NULL = HIGH-VALUES
              MOVE P56-TP-ADEG-VER-NULL
                TO (SF)-TP-ADEG-VER-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-ADEG-VER
                TO (SF)-TP-ADEG-VER(IX-TAB-P56)
           END-IF
           IF P56-TP-RELA-ESEC-NULL = HIGH-VALUES
              MOVE P56-TP-RELA-ESEC-NULL
                TO (SF)-TP-RELA-ESEC-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-RELA-ESEC
                TO (SF)-TP-RELA-ESEC(IX-TAB-P56)
           END-IF
           IF P56-TP-SCO-FIN-RAPP-NULL = HIGH-VALUES
              MOVE P56-TP-SCO-FIN-RAPP-NULL
                TO (SF)-TP-SCO-FIN-RAPP-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-SCO-FIN-RAPP
                TO (SF)-TP-SCO-FIN-RAPP(IX-TAB-P56)
           END-IF
           IF P56-FL-PRSZ-3O-PAGAT-NULL = HIGH-VALUES
              MOVE P56-FL-PRSZ-3O-PAGAT-NULL
                TO (SF)-FL-PRSZ-3O-PAGAT-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-PRSZ-3O-PAGAT
                TO (SF)-FL-PRSZ-3O-PAGAT(IX-TAB-P56)
           END-IF
           IF P56-AREA-GEO-PROV-FND-NULL = HIGH-VALUES
              MOVE P56-AREA-GEO-PROV-FND-NULL
                TO (SF)-AREA-GEO-PROV-FND-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-AREA-GEO-PROV-FND
                TO (SF)-AREA-GEO-PROV-FND(IX-TAB-P56)
           END-IF
           IF P56-TP-DEST-FND-NULL = HIGH-VALUES
              MOVE P56-TP-DEST-FND-NULL
                TO (SF)-TP-DEST-FND-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-DEST-FND
                TO (SF)-TP-DEST-FND(IX-TAB-P56)
           END-IF
           IF P56-FL-PAESE-RESID-AUT-NULL = HIGH-VALUES
              MOVE P56-FL-PAESE-RESID-AUT-NULL
                TO (SF)-FL-PAESE-RESID-AUT-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-PAESE-RESID-AUT
                TO (SF)-FL-PAESE-RESID-AUT(IX-TAB-P56)
           END-IF
           IF P56-FL-PAESE-CIT-AUT-NULL = HIGH-VALUES
              MOVE P56-FL-PAESE-CIT-AUT-NULL
                TO (SF)-FL-PAESE-CIT-AUT-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-PAESE-CIT-AUT
                TO (SF)-FL-PAESE-CIT-AUT(IX-TAB-P56)
           END-IF
           IF P56-FL-PAESE-NAZ-AUT-NULL = HIGH-VALUES
              MOVE P56-FL-PAESE-NAZ-AUT-NULL
                TO (SF)-FL-PAESE-NAZ-AUT-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-PAESE-NAZ-AUT
                TO (SF)-FL-PAESE-NAZ-AUT(IX-TAB-P56)
           END-IF
           IF P56-COD-PROF-PREC-NULL = HIGH-VALUES
              MOVE P56-COD-PROF-PREC-NULL
                TO (SF)-COD-PROF-PREC-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-PROF-PREC
                TO (SF)-COD-PROF-PREC(IX-TAB-P56)
           END-IF
           IF P56-FL-AUT-PEP-NULL = HIGH-VALUES
              MOVE P56-FL-AUT-PEP-NULL
                TO (SF)-FL-AUT-PEP-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-AUT-PEP
                TO (SF)-FL-AUT-PEP(IX-TAB-P56)
           END-IF
           IF P56-FL-IMP-CAR-PUB-NULL = HIGH-VALUES
              MOVE P56-FL-IMP-CAR-PUB-NULL
                TO (SF)-FL-IMP-CAR-PUB-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-IMP-CAR-PUB
                TO (SF)-FL-IMP-CAR-PUB(IX-TAB-P56)
           END-IF
           IF P56-FL-LIS-TERR-SORV-NULL = HIGH-VALUES
              MOVE P56-FL-LIS-TERR-SORV-NULL
                TO (SF)-FL-LIS-TERR-SORV-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-LIS-TERR-SORV
                TO (SF)-FL-LIS-TERR-SORV(IX-TAB-P56)
           END-IF
           IF P56-TP-SIT-FIN-PAT-NULL = HIGH-VALUES
              MOVE P56-TP-SIT-FIN-PAT-NULL
                TO (SF)-TP-SIT-FIN-PAT-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-SIT-FIN-PAT
                TO (SF)-TP-SIT-FIN-PAT(IX-TAB-P56)
           END-IF
           IF P56-TP-SIT-FIN-PAT-CON-NULL = HIGH-VALUES
              MOVE P56-TP-SIT-FIN-PAT-CON-NULL
                TO (SF)-TP-SIT-FIN-PAT-CON-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-SIT-FIN-PAT-CON
                TO (SF)-TP-SIT-FIN-PAT-CON(IX-TAB-P56)
           END-IF
           IF P56-IMP-TOT-AFF-UTIL-NULL = HIGH-VALUES
              MOVE P56-IMP-TOT-AFF-UTIL-NULL
                TO (SF)-IMP-TOT-AFF-UTIL-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-IMP-TOT-AFF-UTIL
                TO (SF)-IMP-TOT-AFF-UTIL(IX-TAB-P56)
           END-IF
           IF P56-IMP-TOT-FIN-UTIL-NULL = HIGH-VALUES
              MOVE P56-IMP-TOT-FIN-UTIL-NULL
                TO (SF)-IMP-TOT-FIN-UTIL-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-IMP-TOT-FIN-UTIL
                TO (SF)-IMP-TOT-FIN-UTIL(IX-TAB-P56)
           END-IF
           IF P56-IMP-TOT-AFF-ACC-NULL = HIGH-VALUES
              MOVE P56-IMP-TOT-AFF-ACC-NULL
                TO (SF)-IMP-TOT-AFF-ACC-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-IMP-TOT-AFF-ACC
                TO (SF)-IMP-TOT-AFF-ACC(IX-TAB-P56)
           END-IF
           IF P56-IMP-TOT-FIN-ACC-NULL = HIGH-VALUES
              MOVE P56-IMP-TOT-FIN-ACC-NULL
                TO (SF)-IMP-TOT-FIN-ACC-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-IMP-TOT-FIN-ACC
                TO (SF)-IMP-TOT-FIN-ACC(IX-TAB-P56)
           END-IF
           IF P56-TP-FRM-GIUR-SAV-NULL = HIGH-VALUES
              MOVE P56-TP-FRM-GIUR-SAV-NULL
                TO (SF)-TP-FRM-GIUR-SAV-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-FRM-GIUR-SAV
                TO (SF)-TP-FRM-GIUR-SAV(IX-TAB-P56)
           END-IF
           IF P56-REG-COLL-POLI-NULL = HIGH-VALUES
              MOVE P56-REG-COLL-POLI-NULL
                TO (SF)-REG-COLL-POLI-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-REG-COLL-POLI
                TO (SF)-REG-COLL-POLI(IX-TAB-P56)
           END-IF
           IF P56-NUM-TEL-NULL = HIGH-VALUES
              MOVE P56-NUM-TEL-NULL
                TO (SF)-NUM-TEL-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-NUM-TEL
                TO (SF)-NUM-TEL(IX-TAB-P56)
           END-IF
           IF P56-NUM-DIP-NULL = HIGH-VALUES
              MOVE P56-NUM-DIP-NULL
                TO (SF)-NUM-DIP-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-NUM-DIP
                TO (SF)-NUM-DIP(IX-TAB-P56)
           END-IF
           IF P56-TP-SIT-FAM-CONV-NULL = HIGH-VALUES
              MOVE P56-TP-SIT-FAM-CONV-NULL
                TO (SF)-TP-SIT-FAM-CONV-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-SIT-FAM-CONV
                TO (SF)-TP-SIT-FAM-CONV(IX-TAB-P56)
           END-IF
           IF P56-COD-PROF-CON-NULL = HIGH-VALUES
              MOVE P56-COD-PROF-CON-NULL
                TO (SF)-COD-PROF-CON-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-PROF-CON
                TO (SF)-COD-PROF-CON(IX-TAB-P56)
           END-IF
           IF P56-FL-ES-PROC-PEN-NULL = HIGH-VALUES
              MOVE P56-FL-ES-PROC-PEN-NULL
                TO (SF)-FL-ES-PROC-PEN-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-ES-PROC-PEN
                TO (SF)-FL-ES-PROC-PEN(IX-TAB-P56)
           END-IF
           IF P56-TP-COND-CLIENTE-NULL = HIGH-VALUES
              MOVE P56-TP-COND-CLIENTE-NULL
                TO (SF)-TP-COND-CLIENTE-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-COND-CLIENTE
                TO (SF)-TP-COND-CLIENTE(IX-TAB-P56)
           END-IF
           IF P56-COD-SAE-NULL = HIGH-VALUES
              MOVE P56-COD-SAE-NULL
                TO (SF)-COD-SAE-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-SAE
                TO (SF)-COD-SAE(IX-TAB-P56)
           END-IF
           IF P56-TP-OPER-ESTERO-NULL = HIGH-VALUES
              MOVE P56-TP-OPER-ESTERO-NULL
                TO (SF)-TP-OPER-ESTERO-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-OPER-ESTERO
                TO (SF)-TP-OPER-ESTERO(IX-TAB-P56)
           END-IF
           IF P56-STAT-OPER-ESTERO-NULL = HIGH-VALUES
              MOVE P56-STAT-OPER-ESTERO-NULL
                TO (SF)-STAT-OPER-ESTERO-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-STAT-OPER-ESTERO
                TO (SF)-STAT-OPER-ESTERO(IX-TAB-P56)
           END-IF
           IF P56-COD-PRV-SVOL-ATT-NULL = HIGH-VALUES
              MOVE P56-COD-PRV-SVOL-ATT-NULL
                TO (SF)-COD-PRV-SVOL-ATT-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-PRV-SVOL-ATT
                TO (SF)-COD-PRV-SVOL-ATT(IX-TAB-P56)
           END-IF
           IF P56-COD-STAT-SVOL-ATT-NULL = HIGH-VALUES
              MOVE P56-COD-STAT-SVOL-ATT-NULL
                TO (SF)-COD-STAT-SVOL-ATT-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-STAT-SVOL-ATT
                TO (SF)-COD-STAT-SVOL-ATT(IX-TAB-P56)
           END-IF
           IF P56-TP-SOC-NULL = HIGH-VALUES
              MOVE P56-TP-SOC-NULL
                TO (SF)-TP-SOC-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-SOC
                TO (SF)-TP-SOC(IX-TAB-P56)
           END-IF
           IF P56-FL-IND-SOC-QUOT-NULL = HIGH-VALUES
              MOVE P56-FL-IND-SOC-QUOT-NULL
                TO (SF)-FL-IND-SOC-QUOT-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-IND-SOC-QUOT
                TO (SF)-FL-IND-SOC-QUOT(IX-TAB-P56)
           END-IF
           IF P56-TP-SIT-GIUR-NULL = HIGH-VALUES
              MOVE P56-TP-SIT-GIUR-NULL
                TO (SF)-TP-SIT-GIUR-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-SIT-GIUR
                TO (SF)-TP-SIT-GIUR(IX-TAB-P56)
           END-IF
           IF P56-PC-QUO-DET-TIT-EFF-NULL = HIGH-VALUES
              MOVE P56-PC-QUO-DET-TIT-EFF-NULL
                TO (SF)-PC-QUO-DET-TIT-EFF-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-PC-QUO-DET-TIT-EFF
                TO (SF)-PC-QUO-DET-TIT-EFF(IX-TAB-P56)
           END-IF
           IF P56-TP-PRFL-RSH-PEP-NULL = HIGH-VALUES
              MOVE P56-TP-PRFL-RSH-PEP-NULL
                TO (SF)-TP-PRFL-RSH-PEP-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-PRFL-RSH-PEP
                TO (SF)-TP-PRFL-RSH-PEP(IX-TAB-P56)
           END-IF
           IF P56-TP-PEP-NULL = HIGH-VALUES
              MOVE P56-TP-PEP-NULL
                TO (SF)-TP-PEP-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-PEP
                TO (SF)-TP-PEP(IX-TAB-P56)
           END-IF
           IF P56-FL-NOT-PREG-NULL = HIGH-VALUES
              MOVE P56-FL-NOT-PREG-NULL
                TO (SF)-FL-NOT-PREG-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-NOT-PREG
                TO (SF)-FL-NOT-PREG(IX-TAB-P56)
           END-IF
           IF P56-DT-INI-FNT-REDD-NULL = HIGH-VALUES
              MOVE P56-DT-INI-FNT-REDD-NULL
                TO (SF)-DT-INI-FNT-REDD-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-DT-INI-FNT-REDD
                TO (SF)-DT-INI-FNT-REDD(IX-TAB-P56)
           END-IF
           IF P56-FNT-REDD-2-NULL = HIGH-VALUES
              MOVE P56-FNT-REDD-2-NULL
                TO (SF)-FNT-REDD-2-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FNT-REDD-2
                TO (SF)-FNT-REDD-2(IX-TAB-P56)
           END-IF
           IF P56-DT-INI-FNT-REDD-2-NULL = HIGH-VALUES
              MOVE P56-DT-INI-FNT-REDD-2-NULL
                TO (SF)-DT-INI-FNT-REDD-2-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-DT-INI-FNT-REDD-2
                TO (SF)-DT-INI-FNT-REDD-2(IX-TAB-P56)
           END-IF
           IF P56-FNT-REDD-3-NULL = HIGH-VALUES
              MOVE P56-FNT-REDD-3-NULL
                TO (SF)-FNT-REDD-3-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FNT-REDD-3
                TO (SF)-FNT-REDD-3(IX-TAB-P56)
           END-IF
           IF P56-DT-INI-FNT-REDD-3-NULL = HIGH-VALUES
              MOVE P56-DT-INI-FNT-REDD-3-NULL
                TO (SF)-DT-INI-FNT-REDD-3-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-DT-INI-FNT-REDD-3
                TO (SF)-DT-INI-FNT-REDD-3(IX-TAB-P56)
           END-IF
           MOVE P56-MOT-ASS-TIT-EFF
             TO (SF)-MOT-ASS-TIT-EFF(IX-TAB-P56)
           MOVE P56-FIN-COSTITUZIONE
             TO (SF)-FIN-COSTITUZIONE(IX-TAB-P56)
           MOVE P56-DESC-IMP-CAR-PUB
             TO (SF)-DESC-IMP-CAR-PUB(IX-TAB-P56)
           MOVE P56-DESC-SCO-FIN-RAPP
             TO (SF)-DESC-SCO-FIN-RAPP(IX-TAB-P56)
           MOVE P56-DESC-PROC-PNL
             TO (SF)-DESC-PROC-PNL(IX-TAB-P56)
           MOVE P56-DESC-NOT-PREG
             TO (SF)-DESC-NOT-PREG(IX-TAB-P56)
           IF P56-ID-ASSICURATI-NULL = HIGH-VALUES
              MOVE P56-ID-ASSICURATI-NULL
                TO (SF)-ID-ASSICURATI-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-ID-ASSICURATI
                TO (SF)-ID-ASSICURATI(IX-TAB-P56)
           END-IF
           IF P56-REDD-CON-NULL = HIGH-VALUES
              MOVE P56-REDD-CON-NULL
                TO (SF)-REDD-CON-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-REDD-CON
                TO (SF)-REDD-CON(IX-TAB-P56)
           END-IF
           MOVE P56-DESC-LIB-MOT-RISC
             TO (SF)-DESC-LIB-MOT-RISC(IX-TAB-P56)
           IF P56-TP-MOT-ASS-TIT-EFF-NULL = HIGH-VALUES
              MOVE P56-TP-MOT-ASS-TIT-EFF-NULL
                TO (SF)-TP-MOT-ASS-TIT-EFF-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-MOT-ASS-TIT-EFF
                TO (SF)-TP-MOT-ASS-TIT-EFF(IX-TAB-P56)
           END-IF
           IF P56-TP-RAG-RAPP-NULL = HIGH-VALUES
              MOVE P56-TP-RAG-RAPP-NULL
                TO (SF)-TP-RAG-RAPP-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-RAG-RAPP
                TO (SF)-TP-RAG-RAPP(IX-TAB-P56)
           END-IF
           IF P56-COD-CAN-NULL = HIGH-VALUES
              MOVE P56-COD-CAN-NULL
                TO (SF)-COD-CAN-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-CAN
                TO (SF)-COD-CAN(IX-TAB-P56)
           END-IF
           IF P56-TP-FIN-COST-NULL = HIGH-VALUES
              MOVE P56-TP-FIN-COST-NULL
                TO (SF)-TP-FIN-COST-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-FIN-COST
                TO (SF)-TP-FIN-COST(IX-TAB-P56)
           END-IF
           IF P56-NAZ-DEST-FND-NULL = HIGH-VALUES
              MOVE P56-NAZ-DEST-FND-NULL
                TO (SF)-NAZ-DEST-FND-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-NAZ-DEST-FND
                TO (SF)-NAZ-DEST-FND(IX-TAB-P56)
           END-IF
           IF P56-FL-AU-FATCA-AEOI-NULL = HIGH-VALUES
              MOVE P56-FL-AU-FATCA-AEOI-NULL
                TO (SF)-FL-AU-FATCA-AEOI-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-AU-FATCA-AEOI
                TO (SF)-FL-AU-FATCA-AEOI(IX-TAB-P56)
           END-IF
           IF P56-TP-CAR-FIN-GIUR-NULL = HIGH-VALUES
              MOVE P56-TP-CAR-FIN-GIUR-NULL
                TO (SF)-TP-CAR-FIN-GIUR-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-CAR-FIN-GIUR
                TO (SF)-TP-CAR-FIN-GIUR(IX-TAB-P56)
           END-IF
           IF P56-TP-CAR-FIN-GIUR-AT-NULL = HIGH-VALUES
              MOVE P56-TP-CAR-FIN-GIUR-AT-NULL
                TO (SF)-TP-CAR-FIN-GIUR-AT-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-CAR-FIN-GIUR-AT
                TO (SF)-TP-CAR-FIN-GIUR-AT(IX-TAB-P56)
           END-IF
           IF P56-TP-CAR-FIN-GIUR-PA-NULL = HIGH-VALUES
              MOVE P56-TP-CAR-FIN-GIUR-PA-NULL
                TO (SF)-TP-CAR-FIN-GIUR-PA-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-CAR-FIN-GIUR-PA
                TO (SF)-TP-CAR-FIN-GIUR-PA(IX-TAB-P56)
           END-IF
           IF P56-FL-ISTITUZ-FIN-NULL = HIGH-VALUES
              MOVE P56-FL-ISTITUZ-FIN-NULL
                TO (SF)-FL-ISTITUZ-FIN-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-ISTITUZ-FIN
                TO (SF)-FL-ISTITUZ-FIN(IX-TAB-P56)
           END-IF
           IF P56-TP-ORI-FND-TIT-EFF-NULL = HIGH-VALUES
              MOVE P56-TP-ORI-FND-TIT-EFF-NULL
                TO (SF)-TP-ORI-FND-TIT-EFF-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-ORI-FND-TIT-EFF
                TO (SF)-TP-ORI-FND-TIT-EFF(IX-TAB-P56)
           END-IF
           IF P56-PC-ESP-AG-PA-MSC-NULL = HIGH-VALUES
              MOVE P56-PC-ESP-AG-PA-MSC-NULL
                TO (SF)-PC-ESP-AG-PA-MSC-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-PC-ESP-AG-PA-MSC
                TO (SF)-PC-ESP-AG-PA-MSC(IX-TAB-P56)
           END-IF
           IF P56-FL-PR-TR-USA-NULL = HIGH-VALUES
              MOVE P56-FL-PR-TR-USA-NULL
                TO (SF)-FL-PR-TR-USA-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-PR-TR-USA
                TO (SF)-FL-PR-TR-USA(IX-TAB-P56)
           END-IF
           IF P56-FL-PR-TR-NO-USA-NULL = HIGH-VALUES
              MOVE P56-FL-PR-TR-NO-USA-NULL
                TO (SF)-FL-PR-TR-NO-USA-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-PR-TR-NO-USA
                TO (SF)-FL-PR-TR-NO-USA(IX-TAB-P56)
           END-IF
           IF P56-PC-RIP-PAT-AS-VITA-NULL = HIGH-VALUES
              MOVE P56-PC-RIP-PAT-AS-VITA-NULL
                TO (SF)-PC-RIP-PAT-AS-VITA-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-PC-RIP-PAT-AS-VITA
                TO (SF)-PC-RIP-PAT-AS-VITA(IX-TAB-P56)
           END-IF
           IF P56-PC-RIP-PAT-IM-NULL = HIGH-VALUES
              MOVE P56-PC-RIP-PAT-IM-NULL
                TO (SF)-PC-RIP-PAT-IM-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-PC-RIP-PAT-IM
                TO (SF)-PC-RIP-PAT-IM(IX-TAB-P56)
           END-IF
           IF P56-PC-RIP-PAT-SET-IM-NULL = HIGH-VALUES
              MOVE P56-PC-RIP-PAT-SET-IM-NULL
                TO (SF)-PC-RIP-PAT-SET-IM-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-PC-RIP-PAT-SET-IM
                TO (SF)-PC-RIP-PAT-SET-IM(IX-TAB-P56)
           END-IF
           IF P56-TP-STATUS-AEOI-NULL = HIGH-VALUES
              MOVE P56-TP-STATUS-AEOI-NULL
                TO (SF)-TP-STATUS-AEOI-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-STATUS-AEOI
                TO (SF)-TP-STATUS-AEOI(IX-TAB-P56)
           END-IF
           IF P56-TP-STATUS-FATCA-NULL = HIGH-VALUES
              MOVE P56-TP-STATUS-FATCA-NULL
                TO (SF)-TP-STATUS-FATCA-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-STATUS-FATCA
                TO (SF)-TP-STATUS-FATCA(IX-TAB-P56)
           END-IF
           IF P56-FL-RAPP-PA-MSC-NULL = HIGH-VALUES
              MOVE P56-FL-RAPP-PA-MSC-NULL
                TO (SF)-FL-RAPP-PA-MSC-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-RAPP-PA-MSC
                TO (SF)-FL-RAPP-PA-MSC(IX-TAB-P56)
           END-IF
           IF P56-COD-COMUN-SVOL-ATT-NULL = HIGH-VALUES
              MOVE P56-COD-COMUN-SVOL-ATT-NULL
                TO (SF)-COD-COMUN-SVOL-ATT-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-COMUN-SVOL-ATT
                TO (SF)-COD-COMUN-SVOL-ATT(IX-TAB-P56)
           END-IF
           IF P56-TP-DT-1O-CON-CLI-NULL = HIGH-VALUES
              MOVE P56-TP-DT-1O-CON-CLI-NULL
                TO (SF)-TP-DT-1O-CON-CLI-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-DT-1O-CON-CLI
                TO (SF)-TP-DT-1O-CON-CLI(IX-TAB-P56)
           END-IF
           IF P56-TP-MOD-EN-RELA-INT-NULL = HIGH-VALUES
              MOVE P56-TP-MOD-EN-RELA-INT-NULL
                TO (SF)-TP-MOD-EN-RELA-INT-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-MOD-EN-RELA-INT
                TO (SF)-TP-MOD-EN-RELA-INT(IX-TAB-P56)
           END-IF
           IF P56-TP-REDD-ANNU-LRD-NULL = HIGH-VALUES
              MOVE P56-TP-REDD-ANNU-LRD-NULL
                TO (SF)-TP-REDD-ANNU-LRD-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-REDD-ANNU-LRD
                TO (SF)-TP-REDD-ANNU-LRD(IX-TAB-P56)
           END-IF
           IF P56-TP-REDD-CON-NULL = HIGH-VALUES
              MOVE P56-TP-REDD-CON-NULL
                TO (SF)-TP-REDD-CON-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-REDD-CON
                TO (SF)-TP-REDD-CON(IX-TAB-P56)
           END-IF
           IF P56-TP-OPER-SOC-FID-NULL = HIGH-VALUES
              MOVE P56-TP-OPER-SOC-FID-NULL
                TO (SF)-TP-OPER-SOC-FID-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-OPER-SOC-FID
                TO (SF)-TP-OPER-SOC-FID(IX-TAB-P56)
           END-IF
           IF P56-COD-PA-ESP-MSC-1-NULL = HIGH-VALUES
              MOVE P56-COD-PA-ESP-MSC-1-NULL
                TO (SF)-COD-PA-ESP-MSC-1-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-PA-ESP-MSC-1
                TO (SF)-COD-PA-ESP-MSC-1(IX-TAB-P56)
           END-IF
           IF P56-IMP-PA-ESP-MSC-1-NULL = HIGH-VALUES
              MOVE P56-IMP-PA-ESP-MSC-1-NULL
                TO (SF)-IMP-PA-ESP-MSC-1-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-IMP-PA-ESP-MSC-1
                TO (SF)-IMP-PA-ESP-MSC-1(IX-TAB-P56)
           END-IF
           IF P56-COD-PA-ESP-MSC-2-NULL = HIGH-VALUES
              MOVE P56-COD-PA-ESP-MSC-2-NULL
                TO (SF)-COD-PA-ESP-MSC-2-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-PA-ESP-MSC-2
                TO (SF)-COD-PA-ESP-MSC-2(IX-TAB-P56)
           END-IF
           IF P56-IMP-PA-ESP-MSC-2-NULL = HIGH-VALUES
              MOVE P56-IMP-PA-ESP-MSC-2-NULL
                TO (SF)-IMP-PA-ESP-MSC-2-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-IMP-PA-ESP-MSC-2
                TO (SF)-IMP-PA-ESP-MSC-2(IX-TAB-P56)
           END-IF
           IF P56-COD-PA-ESP-MSC-3-NULL = HIGH-VALUES
              MOVE P56-COD-PA-ESP-MSC-3-NULL
                TO (SF)-COD-PA-ESP-MSC-3-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-PA-ESP-MSC-3
                TO (SF)-COD-PA-ESP-MSC-3(IX-TAB-P56)
           END-IF
           IF P56-IMP-PA-ESP-MSC-3-NULL = HIGH-VALUES
              MOVE P56-IMP-PA-ESP-MSC-3-NULL
                TO (SF)-IMP-PA-ESP-MSC-3-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-IMP-PA-ESP-MSC-3
                TO (SF)-IMP-PA-ESP-MSC-3(IX-TAB-P56)
           END-IF
           IF P56-COD-PA-ESP-MSC-4-NULL = HIGH-VALUES
              MOVE P56-COD-PA-ESP-MSC-4-NULL
                TO (SF)-COD-PA-ESP-MSC-4-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-PA-ESP-MSC-4
                TO (SF)-COD-PA-ESP-MSC-4(IX-TAB-P56)
           END-IF
           IF P56-IMP-PA-ESP-MSC-4-NULL = HIGH-VALUES
              MOVE P56-IMP-PA-ESP-MSC-4-NULL
                TO (SF)-IMP-PA-ESP-MSC-4-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-IMP-PA-ESP-MSC-4
                TO (SF)-IMP-PA-ESP-MSC-4(IX-TAB-P56)
           END-IF
           IF P56-COD-PA-ESP-MSC-5-NULL = HIGH-VALUES
              MOVE P56-COD-PA-ESP-MSC-5-NULL
                TO (SF)-COD-PA-ESP-MSC-5-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-PA-ESP-MSC-5
                TO (SF)-COD-PA-ESP-MSC-5(IX-TAB-P56)
           END-IF
           IF P56-IMP-PA-ESP-MSC-5-NULL = HIGH-VALUES
              MOVE P56-IMP-PA-ESP-MSC-5-NULL
                TO (SF)-IMP-PA-ESP-MSC-5-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-IMP-PA-ESP-MSC-5
                TO (SF)-IMP-PA-ESP-MSC-5(IX-TAB-P56)
           END-IF
           MOVE P56-DESC-ORGN-FND
             TO (SF)-DESC-ORGN-FND(IX-TAB-P56)
           IF P56-COD-AUT-DUE-DIL-NULL = HIGH-VALUES
              MOVE P56-COD-AUT-DUE-DIL-NULL
                TO (SF)-COD-AUT-DUE-DIL-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-AUT-DUE-DIL
                TO (SF)-COD-AUT-DUE-DIL(IX-TAB-P56)
           END-IF
           IF P56-FL-PR-QUEST-FATCA-NULL = HIGH-VALUES
              MOVE P56-FL-PR-QUEST-FATCA-NULL
                TO (SF)-FL-PR-QUEST-FATCA-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-PR-QUEST-FATCA
                TO (SF)-FL-PR-QUEST-FATCA(IX-TAB-P56)
           END-IF
           IF P56-FL-PR-QUEST-AEOI-NULL = HIGH-VALUES
              MOVE P56-FL-PR-QUEST-AEOI-NULL
                TO (SF)-FL-PR-QUEST-AEOI-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-PR-QUEST-AEOI
                TO (SF)-FL-PR-QUEST-AEOI(IX-TAB-P56)
           END-IF
           IF P56-FL-PR-QUEST-OFAC-NULL = HIGH-VALUES
              MOVE P56-FL-PR-QUEST-OFAC-NULL
                TO (SF)-FL-PR-QUEST-OFAC-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-PR-QUEST-OFAC
                TO (SF)-FL-PR-QUEST-OFAC(IX-TAB-P56)
           END-IF
           IF P56-FL-PR-QUEST-KYC-NULL = HIGH-VALUES
              MOVE P56-FL-PR-QUEST-KYC-NULL
                TO (SF)-FL-PR-QUEST-KYC-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-PR-QUEST-KYC
                TO (SF)-FL-PR-QUEST-KYC(IX-TAB-P56)
           END-IF
           IF P56-FL-PR-QUEST-MSCQ-NULL = HIGH-VALUES
              MOVE P56-FL-PR-QUEST-MSCQ-NULL
                TO (SF)-FL-PR-QUEST-MSCQ-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-PR-QUEST-MSCQ
                TO (SF)-FL-PR-QUEST-MSCQ(IX-TAB-P56)
           END-IF
           IF P56-TP-NOT-PREG-NULL = HIGH-VALUES
              MOVE P56-TP-NOT-PREG-NULL
                TO (SF)-TP-NOT-PREG-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-NOT-PREG
                TO (SF)-TP-NOT-PREG(IX-TAB-P56)
           END-IF
           IF P56-TP-PROC-PNL-NULL = HIGH-VALUES
              MOVE P56-TP-PROC-PNL-NULL
                TO (SF)-TP-PROC-PNL-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-PROC-PNL
                TO (SF)-TP-PROC-PNL(IX-TAB-P56)
           END-IF
           IF P56-COD-IMP-CAR-PUB-NULL = HIGH-VALUES
              MOVE P56-COD-IMP-CAR-PUB-NULL
                TO (SF)-COD-IMP-CAR-PUB-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-IMP-CAR-PUB
                TO (SF)-COD-IMP-CAR-PUB(IX-TAB-P56)
           END-IF
           IF P56-OPRZ-SOSPETTE-NULL = HIGH-VALUES
              MOVE P56-OPRZ-SOSPETTE-NULL
                TO (SF)-OPRZ-SOSPETTE-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-OPRZ-SOSPETTE
                TO (SF)-OPRZ-SOSPETTE(IX-TAB-P56)
           END-IF
           IF P56-ULT-FATT-ANNU-NULL = HIGH-VALUES
              MOVE P56-ULT-FATT-ANNU-NULL
                TO (SF)-ULT-FATT-ANNU-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-ULT-FATT-ANNU
                TO (SF)-ULT-FATT-ANNU(IX-TAB-P56)
           END-IF
           MOVE P56-DESC-PEP
             TO (SF)-DESC-PEP(IX-TAB-P56)
           IF P56-NUM-TEL-2-NULL = HIGH-VALUES
              MOVE P56-NUM-TEL-2-NULL
                TO (SF)-NUM-TEL-2-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-NUM-TEL-2
                TO (SF)-NUM-TEL-2(IX-TAB-P56)
           END-IF
           IF P56-IMP-AFI-NULL = HIGH-VALUES
              MOVE P56-IMP-AFI-NULL
                TO (SF)-IMP-AFI-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-IMP-AFI
                TO (SF)-IMP-AFI(IX-TAB-P56)
           END-IF
           IF P56-FL-NEW-PRO-NULL = HIGH-VALUES
              MOVE P56-FL-NEW-PRO-NULL
                TO (SF)-FL-NEW-PRO-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-FL-NEW-PRO
                TO (SF)-FL-NEW-PRO(IX-TAB-P56)
           END-IF
           IF P56-TP-MOT-CAMBIO-CNTR-NULL = HIGH-VALUES
              MOVE P56-TP-MOT-CAMBIO-CNTR-NULL
                TO (SF)-TP-MOT-CAMBIO-CNTR-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-TP-MOT-CAMBIO-CNTR
                TO (SF)-TP-MOT-CAMBIO-CNTR(IX-TAB-P56)
           END-IF
           MOVE P56-DESC-MOT-CAMBIO-CN
             TO (SF)-DESC-MOT-CAMBIO-CN(IX-TAB-P56)
           IF P56-COD-SOGG-NULL = HIGH-VALUES
              MOVE P56-COD-SOGG-NULL
                TO (SF)-COD-SOGG-NULL(IX-TAB-P56)
           ELSE
              MOVE P56-COD-SOGG
                TO (SF)-COD-SOGG(IX-TAB-P56)
           END-IF.
       VALORIZZA-OUTPUT-P56-EX.
           EXIT.

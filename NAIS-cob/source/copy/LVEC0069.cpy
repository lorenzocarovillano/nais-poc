      *-----------------------------------------------------------------*
      *   AREA INCLUSIONE ADESIONE - CALCOLI
      *                              AREA DI PAGINA IAS
      *   PORTAFOGLIO VITA - PROCESSO VENDITA
      *   LUNG.
      *-----------------------------------------------------------------*
      * 03 AREA-IAS.
           05 (SF)-ELE-MAX-IAS                PIC S9(04)      COMP.
           05 (SF)-TAB-IAS                    OCCURS 10.
               07 (SF)-ID-OGG                 PIC S9(9)     COMP-3.
               07 (SF)-TP-OGG                 PIC X(02).
               07 (SF)-TP-IAS-OLD             PIC X(02).
               07 (SF)-TP-IAS-NEW             PIC X(02).
               07 (SF)-MOD-IAS                PIC X(01).
                  88 (SF)-SI-MOD-IAS          VALUE 'S'.
                  88 (SF)-NO-MOD-IAS          VALUE 'N'.

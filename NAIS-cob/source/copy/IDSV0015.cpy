      *-- TIME
       77 WS-HH-PRINCIPIO-N       PIC 9(08) VALUE 01010000.
       77 WS-HH-PRINCIPIO-X       PIC X(10) VALUE '01.01.0000'.

       77 WS-HH-INFINITO-N        PIC 9(08) VALUE 23595999.
       77 WS-HH-INFINITO-X        PIC X(10) VALUE '23.59.5999'.

      *-- DATE
       77 WS-DT-PRINCIPIO-N       PIC 9(08) VALUE 00010101.
       77 WS-DT-PRINCIPIO-X       PIC X(10) VALUE '0001-01-01'.

       77 WS-DT-PRINCIPIO-PIU-1-N PIC 9(08) VALUE 00010102.
       77 WS-DT-PRINCIPIO-PIU-1-X PIC X(10) VALUE '0001-01-02'.

       77 WS-DT-INFINITO-N        PIC 9(08) VALUE 99991231.
       77 WS-DT-INFINITO-X        PIC X(10) VALUE '9999-12-31'.

       77 WS-DT-INFINITO-1-N      PIC 9(08) VALUE 99991230.
       77 WS-DT-INFINITO-1-X      PIC X(10) VALUE '9999-12-30'.

      *-- TIMESTAMP
       77 WS-TS-PRINCIPIO-N       PIC S9(18) COMP-3
                                     VALUE 000101014000000000.
       77 WS-TS-PRINCIPIO-X       PIC X(26)
                                     VALUE '0001-01-01-40.00.00.000000'.

       77 WS-TS-PRINCIPIO-PIU-1-N PIC S9(18) COMP-3
                                     VALUE 000101024000000000.
       77 WS-TS-PRINCIPIO-PIU-1-X PIC X(26)
                                     VALUE '0001-01-02-40.00.00.000000'.

       77 WS-TS-INFINITO-N        PIC S9(18) COMP-3
                                     VALUE 999912314023595999.
       77 WS-TS-INFINITO-X        PIC X(26)
                                     VALUE '9999-12-31-40.23.59.599999'.

       77 WS-TS-INFINITO-1-N      PIC S9(18) COMP-3
                                     VALUE 999912304023595999.
       77 WS-TS-INFINITO-1-X      PIC X(26)
                                     VALUE '9999-12-30-40.23.59.599999'.






      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVLQU5
      *   ULTIMO AGG. 03 GIU 2019
      *------------------------------------------------------------

       VAL-DCLGEN-LQU.
           MOVE (SF)-TP-OGG(IX-TAB-LQU)
              TO LQU-TP-OGG
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-LQU)
              TO LQU-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-LQU)
              TO LQU-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-LQU) NOT NUMERIC
              MOVE 0 TO LQU-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-LQU)
              TO LQU-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-LQU) NOT NUMERIC
              MOVE 0 TO LQU-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-LQU)
              TO LQU-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-LQU)
              TO LQU-COD-COMP-ANIA
           IF (SF)-IB-OGG-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IB-OGG-NULL(IX-TAB-LQU)
              TO LQU-IB-OGG-NULL
           ELSE
              MOVE (SF)-IB-OGG(IX-TAB-LQU)
              TO LQU-IB-OGG
           END-IF
           MOVE (SF)-TP-LIQ(IX-TAB-LQU)
              TO LQU-TP-LIQ
           MOVE (SF)-DESC-CAU-EVE-SIN(IX-TAB-LQU)
              TO LQU-DESC-CAU-EVE-SIN
           IF (SF)-COD-CAU-SIN-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-COD-CAU-SIN-NULL(IX-TAB-LQU)
              TO LQU-COD-CAU-SIN-NULL
           ELSE
              MOVE (SF)-COD-CAU-SIN(IX-TAB-LQU)
              TO LQU-COD-CAU-SIN
           END-IF
           IF (SF)-COD-SIN-CATSTRF-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-COD-SIN-CATSTRF-NULL(IX-TAB-LQU)
              TO LQU-COD-SIN-CATSTRF-NULL
           ELSE
              MOVE (SF)-COD-SIN-CATSTRF(IX-TAB-LQU)
              TO LQU-COD-SIN-CATSTRF
           END-IF
           IF (SF)-DT-MOR-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-DT-MOR-NULL(IX-TAB-LQU)
              TO LQU-DT-MOR-NULL
           ELSE
             IF (SF)-DT-MOR(IX-TAB-LQU) = ZERO
                MOVE HIGH-VALUES
                TO LQU-DT-MOR-NULL
             ELSE
              MOVE (SF)-DT-MOR(IX-TAB-LQU)
              TO LQU-DT-MOR
             END-IF
           END-IF
           IF (SF)-DT-DEN-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-DT-DEN-NULL(IX-TAB-LQU)
              TO LQU-DT-DEN-NULL
           ELSE
             IF (SF)-DT-DEN(IX-TAB-LQU) = ZERO
                MOVE HIGH-VALUES
                TO LQU-DT-DEN-NULL
             ELSE
              MOVE (SF)-DT-DEN(IX-TAB-LQU)
              TO LQU-DT-DEN
             END-IF
           END-IF
           IF (SF)-DT-PERV-DEN-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-DT-PERV-DEN-NULL(IX-TAB-LQU)
              TO LQU-DT-PERV-DEN-NULL
           ELSE
             IF (SF)-DT-PERV-DEN(IX-TAB-LQU) = ZERO
                MOVE HIGH-VALUES
                TO LQU-DT-PERV-DEN-NULL
             ELSE
              MOVE (SF)-DT-PERV-DEN(IX-TAB-LQU)
              TO LQU-DT-PERV-DEN
             END-IF
           END-IF
           IF (SF)-DT-RICH-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-DT-RICH-NULL(IX-TAB-LQU)
              TO LQU-DT-RICH-NULL
           ELSE
             IF (SF)-DT-RICH(IX-TAB-LQU) = ZERO
                MOVE HIGH-VALUES
                TO LQU-DT-RICH-NULL
             ELSE
              MOVE (SF)-DT-RICH(IX-TAB-LQU)
              TO LQU-DT-RICH
             END-IF
           END-IF
           IF (SF)-TP-SIN-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TP-SIN-NULL(IX-TAB-LQU)
              TO LQU-TP-SIN-NULL
           ELSE
              MOVE (SF)-TP-SIN(IX-TAB-LQU)
              TO LQU-TP-SIN
           END-IF
           IF (SF)-TP-RISC-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TP-RISC-NULL(IX-TAB-LQU)
              TO LQU-TP-RISC-NULL
           ELSE
              MOVE (SF)-TP-RISC(IX-TAB-LQU)
              TO LQU-TP-RISC
           END-IF
           IF (SF)-TP-MET-RISC-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TP-MET-RISC-NULL(IX-TAB-LQU)
              TO LQU-TP-MET-RISC-NULL
           ELSE
              MOVE (SF)-TP-MET-RISC(IX-TAB-LQU)
              TO LQU-TP-MET-RISC
           END-IF
           IF (SF)-DT-LIQ-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-DT-LIQ-NULL(IX-TAB-LQU)
              TO LQU-DT-LIQ-NULL
           ELSE
             IF (SF)-DT-LIQ(IX-TAB-LQU) = ZERO
                MOVE HIGH-VALUES
                TO LQU-DT-LIQ-NULL
             ELSE
              MOVE (SF)-DT-LIQ(IX-TAB-LQU)
              TO LQU-DT-LIQ
             END-IF
           END-IF
           IF (SF)-COD-DVS-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-COD-DVS-NULL(IX-TAB-LQU)
              TO LQU-COD-DVS-NULL
           ELSE
              MOVE (SF)-COD-DVS(IX-TAB-LQU)
              TO LQU-COD-DVS
           END-IF
           IF (SF)-TOT-IMP-LRD-LIQTO-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IMP-LRD-LIQTO-NULL(IX-TAB-LQU)
              TO LQU-TOT-IMP-LRD-LIQTO-NULL
           ELSE
              MOVE (SF)-TOT-IMP-LRD-LIQTO(IX-TAB-LQU)
              TO LQU-TOT-IMP-LRD-LIQTO
           END-IF
           IF (SF)-TOT-IMP-PREST-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IMP-PREST-NULL(IX-TAB-LQU)
              TO LQU-TOT-IMP-PREST-NULL
           ELSE
              MOVE (SF)-TOT-IMP-PREST(IX-TAB-LQU)
              TO LQU-TOT-IMP-PREST
           END-IF
           IF (SF)-TOT-IMP-INTR-PREST-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IMP-INTR-PREST-NULL(IX-TAB-LQU)
              TO LQU-TOT-IMP-INTR-PREST-NULL
           ELSE
              MOVE (SF)-TOT-IMP-INTR-PREST(IX-TAB-LQU)
              TO LQU-TOT-IMP-INTR-PREST
           END-IF
           IF (SF)-TOT-IMP-UTI-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IMP-UTI-NULL(IX-TAB-LQU)
              TO LQU-TOT-IMP-UTI-NULL
           ELSE
              MOVE (SF)-TOT-IMP-UTI(IX-TAB-LQU)
              TO LQU-TOT-IMP-UTI
           END-IF
           IF (SF)-TOT-IMP-RIT-TFR-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IMP-RIT-TFR-NULL(IX-TAB-LQU)
              TO LQU-TOT-IMP-RIT-TFR-NULL
           ELSE
              MOVE (SF)-TOT-IMP-RIT-TFR(IX-TAB-LQU)
              TO LQU-TOT-IMP-RIT-TFR
           END-IF
           IF (SF)-TOT-IMP-RIT-ACC-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IMP-RIT-ACC-NULL(IX-TAB-LQU)
              TO LQU-TOT-IMP-RIT-ACC-NULL
           ELSE
              MOVE (SF)-TOT-IMP-RIT-ACC(IX-TAB-LQU)
              TO LQU-TOT-IMP-RIT-ACC
           END-IF
           IF (SF)-TOT-IMP-RIT-VIS-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IMP-RIT-VIS-NULL(IX-TAB-LQU)
              TO LQU-TOT-IMP-RIT-VIS-NULL
           ELSE
              MOVE (SF)-TOT-IMP-RIT-VIS(IX-TAB-LQU)
              TO LQU-TOT-IMP-RIT-VIS
           END-IF
           IF (SF)-TOT-IMPB-TFR-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IMPB-TFR-NULL(IX-TAB-LQU)
              TO LQU-TOT-IMPB-TFR-NULL
           ELSE
              MOVE (SF)-TOT-IMPB-TFR(IX-TAB-LQU)
              TO LQU-TOT-IMPB-TFR
           END-IF
           IF (SF)-TOT-IMPB-ACC-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IMPB-ACC-NULL(IX-TAB-LQU)
              TO LQU-TOT-IMPB-ACC-NULL
           ELSE
              MOVE (SF)-TOT-IMPB-ACC(IX-TAB-LQU)
              TO LQU-TOT-IMPB-ACC
           END-IF
           IF (SF)-TOT-IMPB-VIS-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IMPB-VIS-NULL(IX-TAB-LQU)
              TO LQU-TOT-IMPB-VIS-NULL
           ELSE
              MOVE (SF)-TOT-IMPB-VIS(IX-TAB-LQU)
              TO LQU-TOT-IMPB-VIS
           END-IF
           IF (SF)-TOT-IMP-RIMB-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IMP-RIMB-NULL(IX-TAB-LQU)
              TO LQU-TOT-IMP-RIMB-NULL
           ELSE
              MOVE (SF)-TOT-IMP-RIMB(IX-TAB-LQU)
              TO LQU-TOT-IMP-RIMB
           END-IF
           IF (SF)-IMPB-IMPST-PRVR-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPB-IMPST-PRVR-NULL(IX-TAB-LQU)
              TO LQU-IMPB-IMPST-PRVR-NULL
           ELSE
              MOVE (SF)-IMPB-IMPST-PRVR(IX-TAB-LQU)
              TO LQU-IMPB-IMPST-PRVR
           END-IF
           IF (SF)-IMPST-PRVR-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPST-PRVR-NULL(IX-TAB-LQU)
              TO LQU-IMPST-PRVR-NULL
           ELSE
              MOVE (SF)-IMPST-PRVR(IX-TAB-LQU)
              TO LQU-IMPST-PRVR
           END-IF
           IF (SF)-IMPB-IMPST-252-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPB-IMPST-252-NULL(IX-TAB-LQU)
              TO LQU-IMPB-IMPST-252-NULL
           ELSE
              MOVE (SF)-IMPB-IMPST-252(IX-TAB-LQU)
              TO LQU-IMPB-IMPST-252
           END-IF
           IF (SF)-IMPST-252-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPST-252-NULL(IX-TAB-LQU)
              TO LQU-IMPST-252-NULL
           ELSE
              MOVE (SF)-IMPST-252(IX-TAB-LQU)
              TO LQU-IMPST-252
           END-IF
           IF (SF)-TOT-IMP-IS-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IMP-IS-NULL(IX-TAB-LQU)
              TO LQU-TOT-IMP-IS-NULL
           ELSE
              MOVE (SF)-TOT-IMP-IS(IX-TAB-LQU)
              TO LQU-TOT-IMP-IS
           END-IF
           IF (SF)-IMP-DIR-LIQ-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMP-DIR-LIQ-NULL(IX-TAB-LQU)
              TO LQU-IMP-DIR-LIQ-NULL
           ELSE
              MOVE (SF)-IMP-DIR-LIQ(IX-TAB-LQU)
              TO LQU-IMP-DIR-LIQ
           END-IF
           IF (SF)-TOT-IMP-NET-LIQTO-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IMP-NET-LIQTO-NULL(IX-TAB-LQU)
              TO LQU-TOT-IMP-NET-LIQTO-NULL
           ELSE
              MOVE (SF)-TOT-IMP-NET-LIQTO(IX-TAB-LQU)
              TO LQU-TOT-IMP-NET-LIQTO
           END-IF
           IF (SF)-MONT-END2000-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-MONT-END2000-NULL(IX-TAB-LQU)
              TO LQU-MONT-END2000-NULL
           ELSE
              MOVE (SF)-MONT-END2000(IX-TAB-LQU)
              TO LQU-MONT-END2000
           END-IF
           IF (SF)-MONT-END2006-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-MONT-END2006-NULL(IX-TAB-LQU)
              TO LQU-MONT-END2006-NULL
           ELSE
              MOVE (SF)-MONT-END2006(IX-TAB-LQU)
              TO LQU-MONT-END2006
           END-IF
           IF (SF)-PC-REN-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-PC-REN-NULL(IX-TAB-LQU)
              TO LQU-PC-REN-NULL
           ELSE
              MOVE (SF)-PC-REN(IX-TAB-LQU)
              TO LQU-PC-REN
           END-IF
           IF (SF)-IMP-PNL-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMP-PNL-NULL(IX-TAB-LQU)
              TO LQU-IMP-PNL-NULL
           ELSE
              MOVE (SF)-IMP-PNL(IX-TAB-LQU)
              TO LQU-IMP-PNL
           END-IF
           IF (SF)-IMPB-IRPEF-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPB-IRPEF-NULL(IX-TAB-LQU)
              TO LQU-IMPB-IRPEF-NULL
           ELSE
              MOVE (SF)-IMPB-IRPEF(IX-TAB-LQU)
              TO LQU-IMPB-IRPEF
           END-IF
           IF (SF)-IMPST-IRPEF-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPST-IRPEF-NULL(IX-TAB-LQU)
              TO LQU-IMPST-IRPEF-NULL
           ELSE
              MOVE (SF)-IMPST-IRPEF(IX-TAB-LQU)
              TO LQU-IMPST-IRPEF
           END-IF
           IF (SF)-DT-VLT-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-DT-VLT-NULL(IX-TAB-LQU)
              TO LQU-DT-VLT-NULL
           ELSE
             IF (SF)-DT-VLT(IX-TAB-LQU) = ZERO
                MOVE HIGH-VALUES
                TO LQU-DT-VLT-NULL
             ELSE
              MOVE (SF)-DT-VLT(IX-TAB-LQU)
              TO LQU-DT-VLT
             END-IF
           END-IF
           IF (SF)-DT-END-ISTR-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-DT-END-ISTR-NULL(IX-TAB-LQU)
              TO LQU-DT-END-ISTR-NULL
           ELSE
             IF (SF)-DT-END-ISTR(IX-TAB-LQU) = ZERO
                MOVE HIGH-VALUES
                TO LQU-DT-END-ISTR-NULL
             ELSE
              MOVE (SF)-DT-END-ISTR(IX-TAB-LQU)
              TO LQU-DT-END-ISTR
             END-IF
           END-IF
           MOVE (SF)-TP-RIMB(IX-TAB-LQU)
              TO LQU-TP-RIMB
           IF (SF)-SPE-RCS-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-SPE-RCS-NULL(IX-TAB-LQU)
              TO LQU-SPE-RCS-NULL
           ELSE
              MOVE (SF)-SPE-RCS(IX-TAB-LQU)
              TO LQU-SPE-RCS
           END-IF
           IF (SF)-IB-LIQ-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IB-LIQ-NULL(IX-TAB-LQU)
              TO LQU-IB-LIQ-NULL
           ELSE
              MOVE (SF)-IB-LIQ(IX-TAB-LQU)
              TO LQU-IB-LIQ
           END-IF
           IF (SF)-TOT-IAS-ONER-PRVNT-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IAS-ONER-PRVNT-NULL(IX-TAB-LQU)
              TO LQU-TOT-IAS-ONER-PRVNT-NULL
           ELSE
              MOVE (SF)-TOT-IAS-ONER-PRVNT(IX-TAB-LQU)
              TO LQU-TOT-IAS-ONER-PRVNT
           END-IF
           IF (SF)-TOT-IAS-MGG-SIN-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IAS-MGG-SIN-NULL(IX-TAB-LQU)
              TO LQU-TOT-IAS-MGG-SIN-NULL
           ELSE
              MOVE (SF)-TOT-IAS-MGG-SIN(IX-TAB-LQU)
              TO LQU-TOT-IAS-MGG-SIN
           END-IF
           IF (SF)-TOT-IAS-RST-DPST-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IAS-RST-DPST-NULL(IX-TAB-LQU)
              TO LQU-TOT-IAS-RST-DPST-NULL
           ELSE
              MOVE (SF)-TOT-IAS-RST-DPST(IX-TAB-LQU)
              TO LQU-TOT-IAS-RST-DPST
           END-IF
           IF (SF)-IMP-ONER-LIQ-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMP-ONER-LIQ-NULL(IX-TAB-LQU)
              TO LQU-IMP-ONER-LIQ-NULL
           ELSE
              MOVE (SF)-IMP-ONER-LIQ(IX-TAB-LQU)
              TO LQU-IMP-ONER-LIQ
           END-IF
           IF (SF)-COMPON-TAX-RIMB-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-COMPON-TAX-RIMB-NULL(IX-TAB-LQU)
              TO LQU-COMPON-TAX-RIMB-NULL
           ELSE
              MOVE (SF)-COMPON-TAX-RIMB(IX-TAB-LQU)
              TO LQU-COMPON-TAX-RIMB
           END-IF
           IF (SF)-TP-MEZ-PAG-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TP-MEZ-PAG-NULL(IX-TAB-LQU)
              TO LQU-TP-MEZ-PAG-NULL
           ELSE
              MOVE (SF)-TP-MEZ-PAG(IX-TAB-LQU)
              TO LQU-TP-MEZ-PAG
           END-IF
           IF (SF)-IMP-EXCONTR-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMP-EXCONTR-NULL(IX-TAB-LQU)
              TO LQU-IMP-EXCONTR-NULL
           ELSE
              MOVE (SF)-IMP-EXCONTR(IX-TAB-LQU)
              TO LQU-IMP-EXCONTR
           END-IF
           IF (SF)-IMP-INTR-RIT-PAG-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMP-INTR-RIT-PAG-NULL(IX-TAB-LQU)
              TO LQU-IMP-INTR-RIT-PAG-NULL
           ELSE
              MOVE (SF)-IMP-INTR-RIT-PAG(IX-TAB-LQU)
              TO LQU-IMP-INTR-RIT-PAG
           END-IF
           IF (SF)-BNS-NON-GODUTO-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-BNS-NON-GODUTO-NULL(IX-TAB-LQU)
              TO LQU-BNS-NON-GODUTO-NULL
           ELSE
              MOVE (SF)-BNS-NON-GODUTO(IX-TAB-LQU)
              TO LQU-BNS-NON-GODUTO
           END-IF
           IF (SF)-CNBT-INPSTFM-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-CNBT-INPSTFM-NULL(IX-TAB-LQU)
              TO LQU-CNBT-INPSTFM-NULL
           ELSE
              MOVE (SF)-CNBT-INPSTFM(IX-TAB-LQU)
              TO LQU-CNBT-INPSTFM
           END-IF
           IF (SF)-IMPST-DA-RIMB-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPST-DA-RIMB-NULL(IX-TAB-LQU)
              TO LQU-IMPST-DA-RIMB-NULL
           ELSE
              MOVE (SF)-IMPST-DA-RIMB(IX-TAB-LQU)
              TO LQU-IMPST-DA-RIMB
           END-IF
           IF (SF)-IMPB-IS-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPB-IS-NULL(IX-TAB-LQU)
              TO LQU-IMPB-IS-NULL
           ELSE
              MOVE (SF)-IMPB-IS(IX-TAB-LQU)
              TO LQU-IMPB-IS
           END-IF
           IF (SF)-TAX-SEP-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TAX-SEP-NULL(IX-TAB-LQU)
              TO LQU-TAX-SEP-NULL
           ELSE
              MOVE (SF)-TAX-SEP(IX-TAB-LQU)
              TO LQU-TAX-SEP
           END-IF
           IF (SF)-IMPB-TAX-SEP-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPB-TAX-SEP-NULL(IX-TAB-LQU)
              TO LQU-IMPB-TAX-SEP-NULL
           ELSE
              MOVE (SF)-IMPB-TAX-SEP(IX-TAB-LQU)
              TO LQU-IMPB-TAX-SEP
           END-IF
           IF (SF)-IMPB-INTR-SU-PREST-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPB-INTR-SU-PREST-NULL(IX-TAB-LQU)
              TO LQU-IMPB-INTR-SU-PREST-NULL
           ELSE
              MOVE (SF)-IMPB-INTR-SU-PREST(IX-TAB-LQU)
              TO LQU-IMPB-INTR-SU-PREST
           END-IF
           IF (SF)-ADDIZ-COMUN-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-ADDIZ-COMUN-NULL(IX-TAB-LQU)
              TO LQU-ADDIZ-COMUN-NULL
           ELSE
              MOVE (SF)-ADDIZ-COMUN(IX-TAB-LQU)
              TO LQU-ADDIZ-COMUN
           END-IF
           IF (SF)-IMPB-ADDIZ-COMUN-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPB-ADDIZ-COMUN-NULL(IX-TAB-LQU)
              TO LQU-IMPB-ADDIZ-COMUN-NULL
           ELSE
              MOVE (SF)-IMPB-ADDIZ-COMUN(IX-TAB-LQU)
              TO LQU-IMPB-ADDIZ-COMUN
           END-IF
           IF (SF)-ADDIZ-REGION-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-ADDIZ-REGION-NULL(IX-TAB-LQU)
              TO LQU-ADDIZ-REGION-NULL
           ELSE
              MOVE (SF)-ADDIZ-REGION(IX-TAB-LQU)
              TO LQU-ADDIZ-REGION
           END-IF
           IF (SF)-IMPB-ADDIZ-REGION-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPB-ADDIZ-REGION-NULL(IX-TAB-LQU)
              TO LQU-IMPB-ADDIZ-REGION-NULL
           ELSE
              MOVE (SF)-IMPB-ADDIZ-REGION(IX-TAB-LQU)
              TO LQU-IMPB-ADDIZ-REGION
           END-IF
           IF (SF)-MONT-DAL2007-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-MONT-DAL2007-NULL(IX-TAB-LQU)
              TO LQU-MONT-DAL2007-NULL
           ELSE
              MOVE (SF)-MONT-DAL2007(IX-TAB-LQU)
              TO LQU-MONT-DAL2007
           END-IF
           IF (SF)-IMPB-CNBT-INPSTFM-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPB-CNBT-INPSTFM-NULL(IX-TAB-LQU)
              TO LQU-IMPB-CNBT-INPSTFM-NULL
           ELSE
              MOVE (SF)-IMPB-CNBT-INPSTFM(IX-TAB-LQU)
              TO LQU-IMPB-CNBT-INPSTFM
           END-IF
           IF (SF)-IMP-LRD-DA-RIMB-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMP-LRD-DA-RIMB-NULL(IX-TAB-LQU)
              TO LQU-IMP-LRD-DA-RIMB-NULL
           ELSE
              MOVE (SF)-IMP-LRD-DA-RIMB(IX-TAB-LQU)
              TO LQU-IMP-LRD-DA-RIMB
           END-IF
           IF (SF)-IMP-DIR-DA-RIMB-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMP-DIR-DA-RIMB-NULL(IX-TAB-LQU)
              TO LQU-IMP-DIR-DA-RIMB-NULL
           ELSE
              MOVE (SF)-IMP-DIR-DA-RIMB(IX-TAB-LQU)
              TO LQU-IMP-DIR-DA-RIMB
           END-IF
           IF (SF)-RIS-MAT-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-RIS-MAT-NULL(IX-TAB-LQU)
              TO LQU-RIS-MAT-NULL
           ELSE
              MOVE (SF)-RIS-MAT(IX-TAB-LQU)
              TO LQU-RIS-MAT
           END-IF
           IF (SF)-RIS-SPE-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-RIS-SPE-NULL(IX-TAB-LQU)
              TO LQU-RIS-SPE-NULL
           ELSE
              MOVE (SF)-RIS-SPE(IX-TAB-LQU)
              TO LQU-RIS-SPE
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-LQU) NOT NUMERIC
              MOVE 0 TO LQU-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-LQU)
              TO LQU-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-LQU)
              TO LQU-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-LQU) NOT NUMERIC
              MOVE 0 TO LQU-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-LQU)
              TO LQU-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-LQU) NOT NUMERIC
              MOVE 0 TO LQU-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-LQU)
              TO LQU-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-LQU) NOT NUMERIC
              MOVE 0 TO LQU-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-LQU)
              TO LQU-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-LQU)
              TO LQU-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-LQU)
              TO LQU-DS-STATO-ELAB
           IF (SF)-TOT-IAS-PNL-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TOT-IAS-PNL-NULL(IX-TAB-LQU)
              TO LQU-TOT-IAS-PNL-NULL
           ELSE
              MOVE (SF)-TOT-IAS-PNL(IX-TAB-LQU)
              TO LQU-TOT-IAS-PNL
           END-IF
           IF (SF)-FL-EVE-GARTO-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-FL-EVE-GARTO-NULL(IX-TAB-LQU)
              TO LQU-FL-EVE-GARTO-NULL
           ELSE
              MOVE (SF)-FL-EVE-GARTO(IX-TAB-LQU)
              TO LQU-FL-EVE-GARTO
           END-IF
           IF (SF)-IMP-REN-K1-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMP-REN-K1-NULL(IX-TAB-LQU)
              TO LQU-IMP-REN-K1-NULL
           ELSE
              MOVE (SF)-IMP-REN-K1(IX-TAB-LQU)
              TO LQU-IMP-REN-K1
           END-IF
           IF (SF)-IMP-REN-K2-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMP-REN-K2-NULL(IX-TAB-LQU)
              TO LQU-IMP-REN-K2-NULL
           ELSE
              MOVE (SF)-IMP-REN-K2(IX-TAB-LQU)
              TO LQU-IMP-REN-K2
           END-IF
           IF (SF)-IMP-REN-K3-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMP-REN-K3-NULL(IX-TAB-LQU)
              TO LQU-IMP-REN-K3-NULL
           ELSE
              MOVE (SF)-IMP-REN-K3(IX-TAB-LQU)
              TO LQU-IMP-REN-K3
           END-IF
           IF (SF)-PC-REN-K1-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-PC-REN-K1-NULL(IX-TAB-LQU)
              TO LQU-PC-REN-K1-NULL
           ELSE
              MOVE (SF)-PC-REN-K1(IX-TAB-LQU)
              TO LQU-PC-REN-K1
           END-IF
           IF (SF)-PC-REN-K2-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-PC-REN-K2-NULL(IX-TAB-LQU)
              TO LQU-PC-REN-K2-NULL
           ELSE
              MOVE (SF)-PC-REN-K2(IX-TAB-LQU)
              TO LQU-PC-REN-K2
           END-IF
           IF (SF)-PC-REN-K3-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-PC-REN-K3-NULL(IX-TAB-LQU)
              TO LQU-PC-REN-K3-NULL
           ELSE
              MOVE (SF)-PC-REN-K3(IX-TAB-LQU)
              TO LQU-PC-REN-K3
           END-IF
           IF (SF)-TP-CAUS-ANTIC-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-TP-CAUS-ANTIC-NULL(IX-TAB-LQU)
              TO LQU-TP-CAUS-ANTIC-NULL
           ELSE
              MOVE (SF)-TP-CAUS-ANTIC(IX-TAB-LQU)
              TO LQU-TP-CAUS-ANTIC
           END-IF
           IF (SF)-IMP-LRD-LIQTO-RILT-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMP-LRD-LIQTO-RILT-NULL(IX-TAB-LQU)
              TO LQU-IMP-LRD-LIQTO-RILT-NULL
           ELSE
              MOVE (SF)-IMP-LRD-LIQTO-RILT(IX-TAB-LQU)
              TO LQU-IMP-LRD-LIQTO-RILT
           END-IF
           IF (SF)-IMPST-APPL-RILT-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPST-APPL-RILT-NULL(IX-TAB-LQU)
              TO LQU-IMPST-APPL-RILT-NULL
           ELSE
              MOVE (SF)-IMPST-APPL-RILT(IX-TAB-LQU)
              TO LQU-IMPST-APPL-RILT
           END-IF
           IF (SF)-PC-RISC-PARZ-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-PC-RISC-PARZ-NULL(IX-TAB-LQU)
              TO LQU-PC-RISC-PARZ-NULL
           ELSE
              MOVE (SF)-PC-RISC-PARZ(IX-TAB-LQU)
              TO LQU-PC-RISC-PARZ
           END-IF
           IF (SF)-IMPST-BOLLO-TOT-V-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPST-BOLLO-TOT-V-NULL(IX-TAB-LQU)
              TO LQU-IMPST-BOLLO-TOT-V-NULL
           ELSE
              MOVE (SF)-IMPST-BOLLO-TOT-V(IX-TAB-LQU)
              TO LQU-IMPST-BOLLO-TOT-V
           END-IF
           IF (SF)-IMPST-BOLLO-DETT-C-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPST-BOLLO-DETT-C-NULL(IX-TAB-LQU)
              TO LQU-IMPST-BOLLO-DETT-C-NULL
           ELSE
              MOVE (SF)-IMPST-BOLLO-DETT-C(IX-TAB-LQU)
              TO LQU-IMPST-BOLLO-DETT-C
           END-IF
           IF (SF)-IMPST-BOLLO-TOT-SW-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPST-BOLLO-TOT-SW-NULL(IX-TAB-LQU)
              TO LQU-IMPST-BOLLO-TOT-SW-NULL
           ELSE
              MOVE (SF)-IMPST-BOLLO-TOT-SW(IX-TAB-LQU)
              TO LQU-IMPST-BOLLO-TOT-SW
           END-IF
           IF (SF)-IMPST-BOLLO-TOT-AA-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPST-BOLLO-TOT-AA-NULL(IX-TAB-LQU)
              TO LQU-IMPST-BOLLO-TOT-AA-NULL
           ELSE
              MOVE (SF)-IMPST-BOLLO-TOT-AA(IX-TAB-LQU)
              TO LQU-IMPST-BOLLO-TOT-AA
           END-IF
           IF (SF)-IMPB-VIS-1382011-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-1382011-NULL(IX-TAB-LQU)
              TO LQU-IMPB-VIS-1382011-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-1382011(IX-TAB-LQU)
              TO LQU-IMPB-VIS-1382011
           END-IF
           IF (SF)-IMPST-VIS-1382011-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-1382011-NULL(IX-TAB-LQU)
              TO LQU-IMPST-VIS-1382011-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-1382011(IX-TAB-LQU)
              TO LQU-IMPST-VIS-1382011
           END-IF
           IF (SF)-IMPB-IS-1382011-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPB-IS-1382011-NULL(IX-TAB-LQU)
              TO LQU-IMPB-IS-1382011-NULL
           ELSE
              MOVE (SF)-IMPB-IS-1382011(IX-TAB-LQU)
              TO LQU-IMPB-IS-1382011
           END-IF
           IF (SF)-IMPST-SOST-1382011-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPST-SOST-1382011-NULL(IX-TAB-LQU)
              TO LQU-IMPST-SOST-1382011-NULL
           ELSE
              MOVE (SF)-IMPST-SOST-1382011(IX-TAB-LQU)
              TO LQU-IMPST-SOST-1382011
           END-IF
           IF (SF)-PC-ABB-TIT-STAT-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-PC-ABB-TIT-STAT-NULL(IX-TAB-LQU)
              TO LQU-PC-ABB-TIT-STAT-NULL
           ELSE
              MOVE (SF)-PC-ABB-TIT-STAT(IX-TAB-LQU)
              TO LQU-PC-ABB-TIT-STAT
           END-IF
           IF (SF)-IMPB-BOLLO-DETT-C-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPB-BOLLO-DETT-C-NULL(IX-TAB-LQU)
              TO LQU-IMPB-BOLLO-DETT-C-NULL
           ELSE
              MOVE (SF)-IMPB-BOLLO-DETT-C(IX-TAB-LQU)
              TO LQU-IMPB-BOLLO-DETT-C
           END-IF
           IF (SF)-FL-PRE-COMP-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-FL-PRE-COMP-NULL(IX-TAB-LQU)
              TO LQU-FL-PRE-COMP-NULL
           ELSE
              MOVE (SF)-FL-PRE-COMP(IX-TAB-LQU)
              TO LQU-FL-PRE-COMP
           END-IF
           IF (SF)-IMPB-VIS-662014-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-662014-NULL(IX-TAB-LQU)
              TO LQU-IMPB-VIS-662014-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-662014(IX-TAB-LQU)
              TO LQU-IMPB-VIS-662014
           END-IF
           IF (SF)-IMPST-VIS-662014-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-662014-NULL(IX-TAB-LQU)
              TO LQU-IMPST-VIS-662014-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-662014(IX-TAB-LQU)
              TO LQU-IMPST-VIS-662014
           END-IF
           IF (SF)-IMPB-IS-662014-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPB-IS-662014-NULL(IX-TAB-LQU)
              TO LQU-IMPB-IS-662014-NULL
           ELSE
              MOVE (SF)-IMPB-IS-662014(IX-TAB-LQU)
              TO LQU-IMPB-IS-662014
           END-IF
           IF (SF)-IMPST-SOST-662014-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMPST-SOST-662014-NULL(IX-TAB-LQU)
              TO LQU-IMPST-SOST-662014-NULL
           ELSE
              MOVE (SF)-IMPST-SOST-662014(IX-TAB-LQU)
              TO LQU-IMPST-SOST-662014
           END-IF
           IF (SF)-PC-ABB-TS-662014-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-PC-ABB-TS-662014-NULL(IX-TAB-LQU)
              TO LQU-PC-ABB-TS-662014-NULL
           ELSE
              MOVE (SF)-PC-ABB-TS-662014(IX-TAB-LQU)
              TO LQU-PC-ABB-TS-662014
           END-IF
           IF (SF)-IMP-LRD-CALC-CP-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-IMP-LRD-CALC-CP-NULL(IX-TAB-LQU)
              TO LQU-IMP-LRD-CALC-CP-NULL
           ELSE
              MOVE (SF)-IMP-LRD-CALC-CP(IX-TAB-LQU)
              TO LQU-IMP-LRD-CALC-CP
           END-IF
           IF (SF)-COS-TUNNEL-USCITA-NULL(IX-TAB-LQU) = HIGH-VALUES
              MOVE (SF)-COS-TUNNEL-USCITA-NULL(IX-TAB-LQU)
              TO LQU-COS-TUNNEL-USCITA-NULL
           ELSE
              MOVE (SF)-COS-TUNNEL-USCITA(IX-TAB-LQU)
              TO LQU-COS-TUNNEL-USCITA
           END-IF.
       VAL-DCLGEN-LQU-EX.
           EXIT.

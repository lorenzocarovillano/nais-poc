      *------------------------------------------------------------*
      *    PORTAFOGLIO VITA - PROCESSO VENDITA                     *
      *    CHIAMATA  AL DISPATCHER PER OPRAZIONI DI SELECT O FETCH *
      *          E GESTIONE ERRORE BEP  (TABELLA BENEFICIARI P.)   *
      *     ULTIMO AGG.  07 FEB 2007                               *
      *------------------------------------------------------------*

       LETTURA-BEP.

           IF IDSI0011-SELECT
              PERFORM SELECT-BEP
                THRU SELECT-BEP-EX
           ELSE
              PERFORM FETCH-BEP
                THRU FETCH-BEP-EX
           END-IF.

       LETTURA-BEP-EX.
           EXIT.
      *----------------------------------------------------------------*
      *                         SELECT                                 *
      *----------------------------------------------------------------*
       SELECT-BEP.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI
                       TO BNFIC
                     PERFORM VALORIZZA-OUTPUT-BEP
                       THRU VALORIZZA-OUTPUT-BEP-EX
                  WHEN IDSO0011-NOT-FOUND
      *--->       CAMPO $ NON TROVATO
                         MOVE WK-PGM
                          TO IEAI9901-COD-SERVIZIO-BE
                        MOVE 'SELECT-BEP'
                          TO IEAI9901-LABEL-ERR
                        MOVE '005019'
                          TO IEAI9901-COD-ERRORE
                        MOVE 'ID-OGGETTO'
                          TO IEAI9901-PARAMETRI-ERR
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                           THRU EX-S0300
                   WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM
                                     TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'SELECT-BEP'
                                     TO IEAI9901-LABEL-ERR
                     MOVE '005015'   TO IEAI9901-COD-ERRORE
                     MOVE WK-TABELLA TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SELECT-BEP'
                TO IEAI9901-LABEL-ERR
              MOVE '005016'
                TO IEAI9901-COD-ERRORE
              MOVE SPACES
                TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       SELECT-BEP-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         FETCH                                  *
      *----------------------------------------------------------------*
       FETCH-BEP.

           SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
           SET  WCOM-OVERFLOW-NO                  TO TRUE.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR WCOM-OVERFLOW-YES

               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *-->                 NON TROVATA
                           IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM
                                TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'FETCH-BEP'
                                TO IEAI9901-LABEL-ERR
                              MOVE '005019'
                                TO IEAI9901-COD-ERRORE
                              MOVE 'ID-PTF'
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                           END-IF
                      WHEN IDSO0011-SUCCESSFUL-SQL
      *-->                 OPERAZIONE ESEGUITA CORRETTAMENTE
                           MOVE IDSO0011-BUFFER-DATI  TO BNFIC
                           ADD  1                     TO IX-TAB-BEP

      *-->  Se il contatore di lettura � maggiore dell' elemento MAX
      *-->  previsto per la TAB.BNFIC allora siamo in overflow
                           IF IX-TAB-BEP > (SF)-ELE-BENEFICIARI-MAX
                              SET WCOM-OVERFLOW-YES   TO TRUE
                           ELSE
                              PERFORM VALORIZZA-OUTPUT-BEP
                                 THRU VALORIZZA-OUTPUT-BEP-EX
                              SET IDSI0011-FETCH-NEXT TO TRUE
                           END-IF
                      WHEN OTHER
      *-->                 ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'FETCH-BEP'
                             TO IEAI9901-LABEL-ERR
                           MOVE '005015'
                             TO IEAI9901-COD-ERRORE
                           MOVE WK-TABELLA
                             TO IEAI9901-PARAMETRI-ERR
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      *-->        ERRORE DISPATCHER
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'FETCH-BEP'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005016'
                    TO IEAI9901-COD-ERRORE
                  MOVE SPACES
                    TO IEAI9901-PARAMETRI-ERR
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

       FETCH-BEP-EX.
           EXIT.

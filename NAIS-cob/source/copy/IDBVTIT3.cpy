       01 TIT-CONT-DB.
          05 TIT-DT-INI-EFF-DB     PIC X(10).
          05 TIT-DT-END-EFF-DB     PIC X(10).
          05 TIT-DT-INI-COP-DB     PIC X(10).
          05 TIT-DT-END-COP-DB     PIC X(10).
          05 TIT-DT-APPLZ-MORA-DB     PIC X(10).
          05 TIT-DT-EMIS-TIT-DB     PIC X(10).
          05 TIT-DT-ESI-TIT-DB     PIC X(10).
          05 TIT-DT-VLT-DB     PIC X(10).
          05 TIT-DT-CAMBIO-VLT-DB     PIC X(10).
          05 TIT-DT-RICH-ADD-RID-DB     PIC X(10).
          05 TIT-DT-CERT-FISC-DB     PIC X(10).

           EXEC SQL DECLARE CTRL_AUT_OPER TABLE
           (
             ID_CTRL_AUT_OPER    DECIMAL(9, 0) NOT NULL,
             COD_COMPAGNIA_ANIA  DECIMAL(5, 0) NOT NULL,
             TP_MOVI             DECIMAL(5, 0),
             COD_ERRORE          INTEGER NOT NULL,
             KEY_AUT_OPER1       CHAR(20),
             KEY_AUT_OPER2       CHAR(20),
             KEY_AUT_OPER3       CHAR(20),
             KEY_AUT_OPER4       CHAR(20),
             KEY_AUT_OPER5       CHAR(20),
             COD_LIV_AUT         DECIMAL(5, 0) NOT NULL,
             TP_MOT_DEROGA       CHAR(2) NOT NULL,
             MODULO_VERIFICA     CHAR(8),
             COD_COND            CHAR(50),
             PROG_COND           DECIMAL(3, 0),
             RISULT_COND         CHAR(1),
             PARAM_AUT_OPER      VARCHAR(2000),
             STATO_ATTIVAZIONE   CHAR(1)
          ) END-EXEC.

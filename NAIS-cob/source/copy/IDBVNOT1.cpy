       01 NOTE-OGG.
         05 NOT-ID-NOTE-OGG PIC S9(9)V     COMP-3.
         05 NOT-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 NOT-ID-OGG PIC S9(9)V     COMP-3.
         05 NOT-TP-OGG PIC X(2).
         05 NOT-NOTA-OGG-VCHAR.
           49 NOT-NOTA-OGG-LEN PIC S9(4) COMP-5.
           49 NOT-NOTA-OGG PIC X(250).
         05 NOT-DS-OPER-SQL PIC X(1).
         05 NOT-DS-VER PIC S9(9)V     COMP-3.
         05 NOT-DS-TS-CPTZ PIC S9(18)V     COMP-3.
         05 NOT-DS-UTENTE PIC X(20).
         05 NOT-DS-STATO-ELAB PIC X(1).


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA PERS
      *   ALIAS A25
      *   ULTIMO AGG. 13 NOV 2018
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-PERS PIC S9(9)     COMP-3.
             07 (SF)-TSTAM-INI-VLDT PIC S9(18)     COMP-3.
             07 (SF)-TSTAM-END-VLDT PIC S9(18)     COMP-3.
             07 (SF)-TSTAM-END-VLDT-NULL REDEFINES
                (SF)-TSTAM-END-VLDT   PIC X(10).
             07 (SF)-COD-PERS PIC S9(11)     COMP-3.
             07 (SF)-RIFTO-RETE PIC X(20).
             07 (SF)-RIFTO-RETE-NULL REDEFINES
                (SF)-RIFTO-RETE   PIC X(20).
             07 (SF)-COD-PRT-IVA PIC X(11).
             07 (SF)-COD-PRT-IVA-NULL REDEFINES
                (SF)-COD-PRT-IVA   PIC X(11).
             07 (SF)-IND-PVCY-PRSNL PIC X(1).
             07 (SF)-IND-PVCY-PRSNL-NULL REDEFINES
                (SF)-IND-PVCY-PRSNL   PIC X(1).
             07 (SF)-IND-PVCY-CMMRC PIC X(1).
             07 (SF)-IND-PVCY-CMMRC-NULL REDEFINES
                (SF)-IND-PVCY-CMMRC   PIC X(1).
             07 (SF)-IND-PVCY-INDST PIC X(1).
             07 (SF)-IND-PVCY-INDST-NULL REDEFINES
                (SF)-IND-PVCY-INDST   PIC X(1).
             07 (SF)-DT-NASC-CLI   PIC S9(8) COMP-3.
             07 (SF)-DT-NASC-CLI-NULL REDEFINES
                (SF)-DT-NASC-CLI   PIC X(5).
             07 (SF)-DT-ACQS-PERS   PIC S9(8) COMP-3.
             07 (SF)-DT-ACQS-PERS-NULL REDEFINES
                (SF)-DT-ACQS-PERS   PIC X(5).
             07 (SF)-IND-CLI PIC X(1).
             07 (SF)-IND-CLI-NULL REDEFINES
                (SF)-IND-CLI   PIC X(1).
             07 (SF)-COD-CMN PIC X(4).
             07 (SF)-COD-CMN-NULL REDEFINES
                (SF)-COD-CMN   PIC X(4).
             07 (SF)-COD-FRM-GIURD PIC X(4).
             07 (SF)-COD-FRM-GIURD-NULL REDEFINES
                (SF)-COD-FRM-GIURD   PIC X(4).
             07 (SF)-COD-ENTE-PUBB PIC X(4).
             07 (SF)-COD-ENTE-PUBB-NULL REDEFINES
                (SF)-COD-ENTE-PUBB   PIC X(4).
             07 (SF)-DEN-RGN-SOC PIC X(100).
             07 (SF)-DEN-SIG-RGN-SOC PIC X(100).
             07 (SF)-IND-ESE-FISC PIC X(1).
             07 (SF)-IND-ESE-FISC-NULL REDEFINES
                (SF)-IND-ESE-FISC   PIC X(1).
             07 (SF)-COD-STAT-CVL PIC X(4).
             07 (SF)-COD-STAT-CVL-NULL REDEFINES
                (SF)-COD-STAT-CVL   PIC X(4).
             07 (SF)-DEN-NOME PIC X(100).
             07 (SF)-DEN-COGN PIC X(100).
             07 (SF)-COD-FISC PIC X(16).
             07 (SF)-COD-FISC-NULL REDEFINES
                (SF)-COD-FISC   PIC X(16).
             07 (SF)-IND-SEX PIC X(1).
             07 (SF)-IND-SEX-NULL REDEFINES
                (SF)-IND-SEX   PIC X(1).
             07 (SF)-IND-CPCT-GIURD PIC X(1).
             07 (SF)-IND-CPCT-GIURD-NULL REDEFINES
                (SF)-IND-CPCT-GIURD   PIC X(1).
             07 (SF)-IND-PORT-HDCP PIC X(1).
             07 (SF)-IND-PORT-HDCP-NULL REDEFINES
                (SF)-IND-PORT-HDCP   PIC X(1).
             07 (SF)-COD-USER-INS PIC X(20).
             07 (SF)-TSTAM-INS-RIGA PIC S9(18)     COMP-3.
             07 (SF)-COD-USER-AGGM PIC X(20).
             07 (SF)-COD-USER-AGGM-NULL REDEFINES
                (SF)-COD-USER-AGGM   PIC X(20).
             07 (SF)-TSTAM-AGGM-RIGA PIC S9(18)     COMP-3.
             07 (SF)-TSTAM-AGGM-RIGA-NULL REDEFINES
                (SF)-TSTAM-AGGM-RIGA   PIC X(10).
             07 (SF)-DEN-CMN-NASC-STRN PIC X(100).
             07 (SF)-COD-RAMO-STGR PIC X(4).
             07 (SF)-COD-RAMO-STGR-NULL REDEFINES
                (SF)-COD-RAMO-STGR   PIC X(4).
             07 (SF)-COD-STGR-ATVT-UIC PIC X(4).
             07 (SF)-COD-STGR-ATVT-UIC-NULL REDEFINES
                (SF)-COD-STGR-ATVT-UIC   PIC X(4).
             07 (SF)-COD-RAMO-ATVT-UIC PIC X(4).
             07 (SF)-COD-RAMO-ATVT-UIC-NULL REDEFINES
                (SF)-COD-RAMO-ATVT-UIC   PIC X(4).
             07 (SF)-DT-END-VLDT-PERS   PIC S9(8) COMP-3.
             07 (SF)-DT-END-VLDT-PERS-NULL REDEFINES
                (SF)-DT-END-VLDT-PERS   PIC X(5).
             07 (SF)-DT-DEAD-PERS   PIC S9(8) COMP-3.
             07 (SF)-DT-DEAD-PERS-NULL REDEFINES
                (SF)-DT-DEAD-PERS   PIC X(5).
             07 (SF)-TP-STAT-CLI PIC X(2).
             07 (SF)-TP-STAT-CLI-NULL REDEFINES
                (SF)-TP-STAT-CLI   PIC X(2).
             07 (SF)-DT-BLOC-CLI   PIC S9(8) COMP-3.
             07 (SF)-DT-BLOC-CLI-NULL REDEFINES
                (SF)-DT-BLOC-CLI   PIC X(5).
             07 (SF)-COD-PERS-SECOND PIC S9(11)     COMP-3.
             07 (SF)-COD-PERS-SECOND-NULL REDEFINES
                (SF)-COD-PERS-SECOND   PIC X(6).
             07 (SF)-ID-SEGMENTAZ-CLI PIC S9(9)     COMP-3.
             07 (SF)-ID-SEGMENTAZ-CLI-NULL REDEFINES
                (SF)-ID-SEGMENTAZ-CLI   PIC X(5).
             07 (SF)-DT-1A-ATVT   PIC S9(8) COMP-3.
             07 (SF)-DT-1A-ATVT-NULL REDEFINES
                (SF)-DT-1A-ATVT   PIC X(5).
             07 (SF)-DT-SEGNAL-PARTNER   PIC S9(8) COMP-3.
             07 (SF)-DT-SEGNAL-PARTNER-NULL REDEFINES
                (SF)-DT-SEGNAL-PARTNER   PIC X(5).

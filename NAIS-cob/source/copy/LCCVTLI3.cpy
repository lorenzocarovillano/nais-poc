
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVTLI3
      *   ULTIMO AGG. 09 LUG 2009
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-TLI.
           MOVE TLI-ID-TRCH-LIQ
             TO (SF)-ID-PTF(IX-TAB-TLI)
           MOVE TLI-ID-TRCH-LIQ
             TO (SF)-ID-TRCH-LIQ(IX-TAB-TLI)
           IF TLI-ID-GAR-LIQ-NULL = HIGH-VALUES
              MOVE TLI-ID-GAR-LIQ-NULL
                TO (SF)-ID-GAR-LIQ-NULL(IX-TAB-TLI)
           ELSE
              MOVE TLI-ID-GAR-LIQ
                TO (SF)-ID-GAR-LIQ(IX-TAB-TLI)
           END-IF
           MOVE TLI-ID-LIQ
             TO (SF)-ID-LIQ(IX-TAB-TLI)
           MOVE TLI-ID-TRCH-DI-GAR
             TO (SF)-ID-TRCH-DI-GAR(IX-TAB-TLI)
           MOVE TLI-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-TLI)
           IF TLI-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE TLI-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TLI)
           ELSE
              MOVE TLI-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-TLI)
           END-IF
           MOVE TLI-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-TLI)
           MOVE TLI-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-TLI)
           MOVE TLI-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-TLI)
           MOVE TLI-IMP-LRD-CALC
             TO (SF)-IMP-LRD-CALC(IX-TAB-TLI)
           IF TLI-IMP-LRD-DFZ-NULL = HIGH-VALUES
              MOVE TLI-IMP-LRD-DFZ-NULL
                TO (SF)-IMP-LRD-DFZ-NULL(IX-TAB-TLI)
           ELSE
              MOVE TLI-IMP-LRD-DFZ
                TO (SF)-IMP-LRD-DFZ(IX-TAB-TLI)
           END-IF
           MOVE TLI-IMP-LRD-EFFLQ
             TO (SF)-IMP-LRD-EFFLQ(IX-TAB-TLI)
           MOVE TLI-IMP-NET-CALC
             TO (SF)-IMP-NET-CALC(IX-TAB-TLI)
           IF TLI-IMP-NET-DFZ-NULL = HIGH-VALUES
              MOVE TLI-IMP-NET-DFZ-NULL
                TO (SF)-IMP-NET-DFZ-NULL(IX-TAB-TLI)
           ELSE
              MOVE TLI-IMP-NET-DFZ
                TO (SF)-IMP-NET-DFZ(IX-TAB-TLI)
           END-IF
           MOVE TLI-IMP-NET-EFFLQ
             TO (SF)-IMP-NET-EFFLQ(IX-TAB-TLI)
           IF TLI-IMP-UTI-NULL = HIGH-VALUES
              MOVE TLI-IMP-UTI-NULL
                TO (SF)-IMP-UTI-NULL(IX-TAB-TLI)
           ELSE
              MOVE TLI-IMP-UTI
                TO (SF)-IMP-UTI(IX-TAB-TLI)
           END-IF
           IF TLI-COD-TARI-NULL = HIGH-VALUES
              MOVE TLI-COD-TARI-NULL
                TO (SF)-COD-TARI-NULL(IX-TAB-TLI)
           ELSE
              MOVE TLI-COD-TARI
                TO (SF)-COD-TARI(IX-TAB-TLI)
           END-IF
           IF TLI-COD-DVS-NULL = HIGH-VALUES
              MOVE TLI-COD-DVS-NULL
                TO (SF)-COD-DVS-NULL(IX-TAB-TLI)
           ELSE
              MOVE TLI-COD-DVS
                TO (SF)-COD-DVS(IX-TAB-TLI)
           END-IF
           IF TLI-IAS-ONER-PRVNT-FIN-NULL = HIGH-VALUES
              MOVE TLI-IAS-ONER-PRVNT-FIN-NULL
                TO (SF)-IAS-ONER-PRVNT-FIN-NULL(IX-TAB-TLI)
           ELSE
              MOVE TLI-IAS-ONER-PRVNT-FIN
                TO (SF)-IAS-ONER-PRVNT-FIN(IX-TAB-TLI)
           END-IF
           IF TLI-IAS-MGG-SIN-NULL = HIGH-VALUES
              MOVE TLI-IAS-MGG-SIN-NULL
                TO (SF)-IAS-MGG-SIN-NULL(IX-TAB-TLI)
           ELSE
              MOVE TLI-IAS-MGG-SIN
                TO (SF)-IAS-MGG-SIN(IX-TAB-TLI)
           END-IF
           IF TLI-IAS-RST-DPST-NULL = HIGH-VALUES
              MOVE TLI-IAS-RST-DPST-NULL
                TO (SF)-IAS-RST-DPST-NULL(IX-TAB-TLI)
           ELSE
              MOVE TLI-IAS-RST-DPST
                TO (SF)-IAS-RST-DPST(IX-TAB-TLI)
           END-IF
           IF TLI-IMP-RIMB-NULL = HIGH-VALUES
              MOVE TLI-IMP-RIMB-NULL
                TO (SF)-IMP-RIMB-NULL(IX-TAB-TLI)
           ELSE
              MOVE TLI-IMP-RIMB
                TO (SF)-IMP-RIMB(IX-TAB-TLI)
           END-IF
           IF TLI-COMPON-TAX-RIMB-NULL = HIGH-VALUES
              MOVE TLI-COMPON-TAX-RIMB-NULL
                TO (SF)-COMPON-TAX-RIMB-NULL(IX-TAB-TLI)
           ELSE
              MOVE TLI-COMPON-TAX-RIMB
                TO (SF)-COMPON-TAX-RIMB(IX-TAB-TLI)
           END-IF
           IF TLI-RIS-MAT-NULL = HIGH-VALUES
              MOVE TLI-RIS-MAT-NULL
                TO (SF)-RIS-MAT-NULL(IX-TAB-TLI)
           ELSE
              MOVE TLI-RIS-MAT
                TO (SF)-RIS-MAT(IX-TAB-TLI)
           END-IF
           IF TLI-RIS-SPE-NULL = HIGH-VALUES
              MOVE TLI-RIS-SPE-NULL
                TO (SF)-RIS-SPE-NULL(IX-TAB-TLI)
           ELSE
              MOVE TLI-RIS-SPE
                TO (SF)-RIS-SPE(IX-TAB-TLI)
           END-IF
           MOVE TLI-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-TLI)
           MOVE TLI-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-TLI)
           MOVE TLI-DS-VER
             TO (SF)-DS-VER(IX-TAB-TLI)
           MOVE TLI-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TLI)
           MOVE TLI-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-TLI)
           MOVE TLI-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-TLI)
           MOVE TLI-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-TLI)
           IF TLI-IAS-PNL-NULL = HIGH-VALUES
              MOVE TLI-IAS-PNL-NULL
                TO (SF)-IAS-PNL-NULL(IX-TAB-TLI)
           ELSE
              MOVE TLI-IAS-PNL
                TO (SF)-IAS-PNL(IX-TAB-TLI)
           END-IF.
       VALORIZZA-OUTPUT-TLI-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVP565
      *   ULTIMO AGG. 26 MAR 2019
      *------------------------------------------------------------

       VAL-DCLGEN-P56.
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-P56)
              TO P56-COD-COMP-ANIA
           IF (SF)-NATURA-OPRZ-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-NATURA-OPRZ-NULL(IX-TAB-P56)
              TO P56-NATURA-OPRZ-NULL
           ELSE
              MOVE (SF)-NATURA-OPRZ(IX-TAB-P56)
              TO P56-NATURA-OPRZ
           END-IF
           IF (SF)-ORGN-FND-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-ORGN-FND-NULL(IX-TAB-P56)
              TO P56-ORGN-FND-NULL
           ELSE
              MOVE (SF)-ORGN-FND(IX-TAB-P56)
              TO P56-ORGN-FND
           END-IF
           IF (SF)-COD-QLFC-PROF-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-QLFC-PROF-NULL(IX-TAB-P56)
              TO P56-COD-QLFC-PROF-NULL
           ELSE
              MOVE (SF)-COD-QLFC-PROF(IX-TAB-P56)
              TO P56-COD-QLFC-PROF
           END-IF
           IF (SF)-COD-NAZ-QLFC-PROF-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-NAZ-QLFC-PROF-NULL(IX-TAB-P56)
              TO P56-COD-NAZ-QLFC-PROF-NULL
           ELSE
              MOVE (SF)-COD-NAZ-QLFC-PROF(IX-TAB-P56)
              TO P56-COD-NAZ-QLFC-PROF
           END-IF
           IF (SF)-COD-PRV-QLFC-PROF-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-PRV-QLFC-PROF-NULL(IX-TAB-P56)
              TO P56-COD-PRV-QLFC-PROF-NULL
           ELSE
              MOVE (SF)-COD-PRV-QLFC-PROF(IX-TAB-P56)
              TO P56-COD-PRV-QLFC-PROF
           END-IF
           IF (SF)-FNT-REDD-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FNT-REDD-NULL(IX-TAB-P56)
              TO P56-FNT-REDD-NULL
           ELSE
              MOVE (SF)-FNT-REDD(IX-TAB-P56)
              TO P56-FNT-REDD
           END-IF
           IF (SF)-REDD-FATT-ANNU-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-REDD-FATT-ANNU-NULL(IX-TAB-P56)
              TO P56-REDD-FATT-ANNU-NULL
           ELSE
              MOVE (SF)-REDD-FATT-ANNU(IX-TAB-P56)
              TO P56-REDD-FATT-ANNU
           END-IF
           IF (SF)-COD-ATECO-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-ATECO-NULL(IX-TAB-P56)
              TO P56-COD-ATECO-NULL
           ELSE
              MOVE (SF)-COD-ATECO(IX-TAB-P56)
              TO P56-COD-ATECO
           END-IF
           IF (SF)-VALUT-COLL-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-VALUT-COLL-NULL(IX-TAB-P56)
              TO P56-VALUT-COLL-NULL
           ELSE
              MOVE (SF)-VALUT-COLL(IX-TAB-P56)
              TO P56-VALUT-COLL
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-P56)
              TO P56-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-P56) NOT NUMERIC
              MOVE 0 TO P56-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-P56)
              TO P56-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ(IX-TAB-P56) NOT NUMERIC
              MOVE 0 TO P56-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ(IX-TAB-P56)
              TO P56-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-P56)
              TO P56-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-P56)
              TO P56-DS-STATO-ELAB
           MOVE (SF)-LUOGO-COSTITUZIONE(IX-TAB-P56)
              TO P56-LUOGO-COSTITUZIONE
              MOVE (SF)-TP-MOVI(IX-TAB-P56)
              TO P56-TP-MOVI
           IF (SF)-FL-RAG-RAPP-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-RAG-RAPP-NULL(IX-TAB-P56)
              TO P56-FL-RAG-RAPP-NULL
           ELSE
              MOVE (SF)-FL-RAG-RAPP(IX-TAB-P56)
              TO P56-FL-RAG-RAPP
           END-IF
           IF (SF)-FL-PRSZ-TIT-EFF-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-PRSZ-TIT-EFF-NULL(IX-TAB-P56)
              TO P56-FL-PRSZ-TIT-EFF-NULL
           ELSE
              MOVE (SF)-FL-PRSZ-TIT-EFF(IX-TAB-P56)
              TO P56-FL-PRSZ-TIT-EFF
           END-IF
           IF (SF)-TP-MOT-RISC-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-MOT-RISC-NULL(IX-TAB-P56)
              TO P56-TP-MOT-RISC-NULL
           ELSE
              MOVE (SF)-TP-MOT-RISC(IX-TAB-P56)
              TO P56-TP-MOT-RISC
           END-IF
           IF (SF)-TP-PNT-VND-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-PNT-VND-NULL(IX-TAB-P56)
              TO P56-TP-PNT-VND-NULL
           ELSE
              MOVE (SF)-TP-PNT-VND(IX-TAB-P56)
              TO P56-TP-PNT-VND
           END-IF
              MOVE (SF)-TP-ADEG-VER(IX-TAB-P56)
              TO P56-TP-ADEG-VER
           IF (SF)-TP-RELA-ESEC-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-RELA-ESEC-NULL(IX-TAB-P56)
              TO P56-TP-RELA-ESEC-NULL
           ELSE
              MOVE (SF)-TP-RELA-ESEC(IX-TAB-P56)
              TO P56-TP-RELA-ESEC
           END-IF
           IF (SF)-TP-SCO-FIN-RAPP-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-SCO-FIN-RAPP-NULL(IX-TAB-P56)
              TO P56-TP-SCO-FIN-RAPP-NULL
           ELSE
              MOVE (SF)-TP-SCO-FIN-RAPP(IX-TAB-P56)
              TO P56-TP-SCO-FIN-RAPP
           END-IF
           IF (SF)-FL-PRSZ-3O-PAGAT-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-PRSZ-3O-PAGAT-NULL(IX-TAB-P56)
              TO P56-FL-PRSZ-3O-PAGAT-NULL
           ELSE
              MOVE (SF)-FL-PRSZ-3O-PAGAT(IX-TAB-P56)
              TO P56-FL-PRSZ-3O-PAGAT
           END-IF
           IF (SF)-AREA-GEO-PROV-FND-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-AREA-GEO-PROV-FND-NULL(IX-TAB-P56)
              TO P56-AREA-GEO-PROV-FND-NULL
           ELSE
              MOVE (SF)-AREA-GEO-PROV-FND(IX-TAB-P56)
              TO P56-AREA-GEO-PROV-FND
           END-IF
           IF (SF)-TP-DEST-FND-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-DEST-FND-NULL(IX-TAB-P56)
              TO P56-TP-DEST-FND-NULL
           ELSE
              MOVE (SF)-TP-DEST-FND(IX-TAB-P56)
              TO P56-TP-DEST-FND
           END-IF
           IF (SF)-FL-PAESE-RESID-AUT-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-PAESE-RESID-AUT-NULL(IX-TAB-P56)
              TO P56-FL-PAESE-RESID-AUT-NULL
           ELSE
              MOVE (SF)-FL-PAESE-RESID-AUT(IX-TAB-P56)
              TO P56-FL-PAESE-RESID-AUT
           END-IF
           IF (SF)-FL-PAESE-CIT-AUT-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-PAESE-CIT-AUT-NULL(IX-TAB-P56)
              TO P56-FL-PAESE-CIT-AUT-NULL
           ELSE
              MOVE (SF)-FL-PAESE-CIT-AUT(IX-TAB-P56)
              TO P56-FL-PAESE-CIT-AUT
           END-IF
           IF (SF)-FL-PAESE-NAZ-AUT-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-PAESE-NAZ-AUT-NULL(IX-TAB-P56)
              TO P56-FL-PAESE-NAZ-AUT-NULL
           ELSE
              MOVE (SF)-FL-PAESE-NAZ-AUT(IX-TAB-P56)
              TO P56-FL-PAESE-NAZ-AUT
           END-IF
           IF (SF)-COD-PROF-PREC-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-PROF-PREC-NULL(IX-TAB-P56)
              TO P56-COD-PROF-PREC-NULL
           ELSE
              MOVE (SF)-COD-PROF-PREC(IX-TAB-P56)
              TO P56-COD-PROF-PREC
           END-IF
           IF (SF)-FL-AUT-PEP-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-AUT-PEP-NULL(IX-TAB-P56)
              TO P56-FL-AUT-PEP-NULL
           ELSE
              MOVE (SF)-FL-AUT-PEP(IX-TAB-P56)
              TO P56-FL-AUT-PEP
           END-IF
           IF (SF)-FL-IMP-CAR-PUB-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-IMP-CAR-PUB-NULL(IX-TAB-P56)
              TO P56-FL-IMP-CAR-PUB-NULL
           ELSE
              MOVE (SF)-FL-IMP-CAR-PUB(IX-TAB-P56)
              TO P56-FL-IMP-CAR-PUB
           END-IF
           IF (SF)-FL-LIS-TERR-SORV-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-LIS-TERR-SORV-NULL(IX-TAB-P56)
              TO P56-FL-LIS-TERR-SORV-NULL
           ELSE
              MOVE (SF)-FL-LIS-TERR-SORV(IX-TAB-P56)
              TO P56-FL-LIS-TERR-SORV
           END-IF
           IF (SF)-TP-SIT-FIN-PAT-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-SIT-FIN-PAT-NULL(IX-TAB-P56)
              TO P56-TP-SIT-FIN-PAT-NULL
           ELSE
              MOVE (SF)-TP-SIT-FIN-PAT(IX-TAB-P56)
              TO P56-TP-SIT-FIN-PAT
           END-IF
           IF (SF)-TP-SIT-FIN-PAT-CON-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-SIT-FIN-PAT-CON-NULL(IX-TAB-P56)
              TO P56-TP-SIT-FIN-PAT-CON-NULL
           ELSE
              MOVE (SF)-TP-SIT-FIN-PAT-CON(IX-TAB-P56)
              TO P56-TP-SIT-FIN-PAT-CON
           END-IF
           IF (SF)-IMP-TOT-AFF-UTIL-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-IMP-TOT-AFF-UTIL-NULL(IX-TAB-P56)
              TO P56-IMP-TOT-AFF-UTIL-NULL
           ELSE
              MOVE (SF)-IMP-TOT-AFF-UTIL(IX-TAB-P56)
              TO P56-IMP-TOT-AFF-UTIL
           END-IF
           IF (SF)-IMP-TOT-FIN-UTIL-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-IMP-TOT-FIN-UTIL-NULL(IX-TAB-P56)
              TO P56-IMP-TOT-FIN-UTIL-NULL
           ELSE
              MOVE (SF)-IMP-TOT-FIN-UTIL(IX-TAB-P56)
              TO P56-IMP-TOT-FIN-UTIL
           END-IF
           IF (SF)-IMP-TOT-AFF-ACC-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-IMP-TOT-AFF-ACC-NULL(IX-TAB-P56)
              TO P56-IMP-TOT-AFF-ACC-NULL
           ELSE
              MOVE (SF)-IMP-TOT-AFF-ACC(IX-TAB-P56)
              TO P56-IMP-TOT-AFF-ACC
           END-IF
           IF (SF)-IMP-TOT-FIN-ACC-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-IMP-TOT-FIN-ACC-NULL(IX-TAB-P56)
              TO P56-IMP-TOT-FIN-ACC-NULL
           ELSE
              MOVE (SF)-IMP-TOT-FIN-ACC(IX-TAB-P56)
              TO P56-IMP-TOT-FIN-ACC
           END-IF
           IF (SF)-TP-FRM-GIUR-SAV-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-FRM-GIUR-SAV-NULL(IX-TAB-P56)
              TO P56-TP-FRM-GIUR-SAV-NULL
           ELSE
              MOVE (SF)-TP-FRM-GIUR-SAV(IX-TAB-P56)
              TO P56-TP-FRM-GIUR-SAV
           END-IF
           IF (SF)-REG-COLL-POLI-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-REG-COLL-POLI-NULL(IX-TAB-P56)
              TO P56-REG-COLL-POLI-NULL
           ELSE
              MOVE (SF)-REG-COLL-POLI(IX-TAB-P56)
              TO P56-REG-COLL-POLI
           END-IF
           IF (SF)-NUM-TEL-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-NUM-TEL-NULL(IX-TAB-P56)
              TO P56-NUM-TEL-NULL
           ELSE
              MOVE (SF)-NUM-TEL(IX-TAB-P56)
              TO P56-NUM-TEL
           END-IF
           IF (SF)-NUM-DIP-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-NUM-DIP-NULL(IX-TAB-P56)
              TO P56-NUM-DIP-NULL
           ELSE
              MOVE (SF)-NUM-DIP(IX-TAB-P56)
              TO P56-NUM-DIP
           END-IF
           IF (SF)-TP-SIT-FAM-CONV-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-SIT-FAM-CONV-NULL(IX-TAB-P56)
              TO P56-TP-SIT-FAM-CONV-NULL
           ELSE
              MOVE (SF)-TP-SIT-FAM-CONV(IX-TAB-P56)
              TO P56-TP-SIT-FAM-CONV
           END-IF
           IF (SF)-COD-PROF-CON-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-PROF-CON-NULL(IX-TAB-P56)
              TO P56-COD-PROF-CON-NULL
           ELSE
              MOVE (SF)-COD-PROF-CON(IX-TAB-P56)
              TO P56-COD-PROF-CON
           END-IF
           IF (SF)-FL-ES-PROC-PEN-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-ES-PROC-PEN-NULL(IX-TAB-P56)
              TO P56-FL-ES-PROC-PEN-NULL
           ELSE
              MOVE (SF)-FL-ES-PROC-PEN(IX-TAB-P56)
              TO P56-FL-ES-PROC-PEN
           END-IF
           IF (SF)-TP-COND-CLIENTE-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-COND-CLIENTE-NULL(IX-TAB-P56)
              TO P56-TP-COND-CLIENTE-NULL
           ELSE
              MOVE (SF)-TP-COND-CLIENTE(IX-TAB-P56)
              TO P56-TP-COND-CLIENTE
           END-IF
           IF (SF)-COD-SAE-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-SAE-NULL(IX-TAB-P56)
              TO P56-COD-SAE-NULL
           ELSE
              MOVE (SF)-COD-SAE(IX-TAB-P56)
              TO P56-COD-SAE
           END-IF
           IF (SF)-TP-OPER-ESTERO-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-OPER-ESTERO-NULL(IX-TAB-P56)
              TO P56-TP-OPER-ESTERO-NULL
           ELSE
              MOVE (SF)-TP-OPER-ESTERO(IX-TAB-P56)
              TO P56-TP-OPER-ESTERO
           END-IF
           IF (SF)-STAT-OPER-ESTERO-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-STAT-OPER-ESTERO-NULL(IX-TAB-P56)
              TO P56-STAT-OPER-ESTERO-NULL
           ELSE
              MOVE (SF)-STAT-OPER-ESTERO(IX-TAB-P56)
              TO P56-STAT-OPER-ESTERO
           END-IF
           IF (SF)-COD-PRV-SVOL-ATT-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-PRV-SVOL-ATT-NULL(IX-TAB-P56)
              TO P56-COD-PRV-SVOL-ATT-NULL
           ELSE
              MOVE (SF)-COD-PRV-SVOL-ATT(IX-TAB-P56)
              TO P56-COD-PRV-SVOL-ATT
           END-IF
           IF (SF)-COD-STAT-SVOL-ATT-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-STAT-SVOL-ATT-NULL(IX-TAB-P56)
              TO P56-COD-STAT-SVOL-ATT-NULL
           ELSE
              MOVE (SF)-COD-STAT-SVOL-ATT(IX-TAB-P56)
              TO P56-COD-STAT-SVOL-ATT
           END-IF
           IF (SF)-TP-SOC-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-SOC-NULL(IX-TAB-P56)
              TO P56-TP-SOC-NULL
           ELSE
              MOVE (SF)-TP-SOC(IX-TAB-P56)
              TO P56-TP-SOC
           END-IF
           IF (SF)-FL-IND-SOC-QUOT-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-IND-SOC-QUOT-NULL(IX-TAB-P56)
              TO P56-FL-IND-SOC-QUOT-NULL
           ELSE
              MOVE (SF)-FL-IND-SOC-QUOT(IX-TAB-P56)
              TO P56-FL-IND-SOC-QUOT
           END-IF
           IF (SF)-TP-SIT-GIUR-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-SIT-GIUR-NULL(IX-TAB-P56)
              TO P56-TP-SIT-GIUR-NULL
           ELSE
              MOVE (SF)-TP-SIT-GIUR(IX-TAB-P56)
              TO P56-TP-SIT-GIUR
           END-IF
           IF (SF)-PC-QUO-DET-TIT-EFF-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-PC-QUO-DET-TIT-EFF-NULL(IX-TAB-P56)
              TO P56-PC-QUO-DET-TIT-EFF-NULL
           ELSE
              MOVE (SF)-PC-QUO-DET-TIT-EFF(IX-TAB-P56)
              TO P56-PC-QUO-DET-TIT-EFF
           END-IF
           IF (SF)-TP-PRFL-RSH-PEP-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-PRFL-RSH-PEP-NULL(IX-TAB-P56)
              TO P56-TP-PRFL-RSH-PEP-NULL
           ELSE
              MOVE (SF)-TP-PRFL-RSH-PEP(IX-TAB-P56)
              TO P56-TP-PRFL-RSH-PEP
           END-IF
           IF (SF)-TP-PEP-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-PEP-NULL(IX-TAB-P56)
              TO P56-TP-PEP-NULL
           ELSE
              MOVE (SF)-TP-PEP(IX-TAB-P56)
              TO P56-TP-PEP
           END-IF
           IF (SF)-FL-NOT-PREG-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-NOT-PREG-NULL(IX-TAB-P56)
              TO P56-FL-NOT-PREG-NULL
           ELSE
              MOVE (SF)-FL-NOT-PREG(IX-TAB-P56)
              TO P56-FL-NOT-PREG
           END-IF
           IF (SF)-DT-INI-FNT-REDD-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-DT-INI-FNT-REDD-NULL(IX-TAB-P56)
              TO P56-DT-INI-FNT-REDD-NULL
           ELSE
             IF (SF)-DT-INI-FNT-REDD(IX-TAB-P56) = ZERO
                MOVE HIGH-VALUES
                TO P56-DT-INI-FNT-REDD-NULL
             ELSE
              MOVE (SF)-DT-INI-FNT-REDD(IX-TAB-P56)
              TO P56-DT-INI-FNT-REDD
             END-IF
           END-IF
           IF (SF)-FNT-REDD-2-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FNT-REDD-2-NULL(IX-TAB-P56)
              TO P56-FNT-REDD-2-NULL
           ELSE
              MOVE (SF)-FNT-REDD-2(IX-TAB-P56)
              TO P56-FNT-REDD-2
           END-IF
           IF (SF)-DT-INI-FNT-REDD-2-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-DT-INI-FNT-REDD-2-NULL(IX-TAB-P56)
              TO P56-DT-INI-FNT-REDD-2-NULL
           ELSE
             IF (SF)-DT-INI-FNT-REDD-2(IX-TAB-P56) = ZERO
                MOVE HIGH-VALUES
                TO P56-DT-INI-FNT-REDD-2-NULL
             ELSE
              MOVE (SF)-DT-INI-FNT-REDD-2(IX-TAB-P56)
              TO P56-DT-INI-FNT-REDD-2
             END-IF
           END-IF
           IF (SF)-FNT-REDD-3-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FNT-REDD-3-NULL(IX-TAB-P56)
              TO P56-FNT-REDD-3-NULL
           ELSE
              MOVE (SF)-FNT-REDD-3(IX-TAB-P56)
              TO P56-FNT-REDD-3
           END-IF
           IF (SF)-DT-INI-FNT-REDD-3-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-DT-INI-FNT-REDD-3-NULL(IX-TAB-P56)
              TO P56-DT-INI-FNT-REDD-3-NULL
           ELSE
             IF (SF)-DT-INI-FNT-REDD-3(IX-TAB-P56) = ZERO
                MOVE HIGH-VALUES
                TO P56-DT-INI-FNT-REDD-3-NULL
             ELSE
              MOVE (SF)-DT-INI-FNT-REDD-3(IX-TAB-P56)
              TO P56-DT-INI-FNT-REDD-3
             END-IF
           END-IF
           MOVE (SF)-MOT-ASS-TIT-EFF(IX-TAB-P56)
              TO P56-MOT-ASS-TIT-EFF
           MOVE (SF)-FIN-COSTITUZIONE(IX-TAB-P56)
              TO P56-FIN-COSTITUZIONE
           MOVE (SF)-DESC-IMP-CAR-PUB(IX-TAB-P56)
              TO P56-DESC-IMP-CAR-PUB
           MOVE (SF)-DESC-SCO-FIN-RAPP(IX-TAB-P56)
              TO P56-DESC-SCO-FIN-RAPP
           MOVE (SF)-DESC-PROC-PNL(IX-TAB-P56)
              TO P56-DESC-PROC-PNL
           MOVE (SF)-DESC-NOT-PREG(IX-TAB-P56)
              TO P56-DESC-NOT-PREG
           IF (SF)-ID-ASSICURATI-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-ID-ASSICURATI-NULL(IX-TAB-P56)
              TO P56-ID-ASSICURATI-NULL
           ELSE
              MOVE (SF)-ID-ASSICURATI(IX-TAB-P56)
              TO P56-ID-ASSICURATI
           END-IF
           IF (SF)-REDD-CON-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-REDD-CON-NULL(IX-TAB-P56)
              TO P56-REDD-CON-NULL
           ELSE
              MOVE (SF)-REDD-CON(IX-TAB-P56)
              TO P56-REDD-CON
           END-IF
           MOVE (SF)-DESC-LIB-MOT-RISC(IX-TAB-P56)
              TO P56-DESC-LIB-MOT-RISC
           IF (SF)-TP-MOT-ASS-TIT-EFF-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-MOT-ASS-TIT-EFF-NULL(IX-TAB-P56)
              TO P56-TP-MOT-ASS-TIT-EFF-NULL
           ELSE
              MOVE (SF)-TP-MOT-ASS-TIT-EFF(IX-TAB-P56)
              TO P56-TP-MOT-ASS-TIT-EFF
           END-IF
           IF (SF)-TP-RAG-RAPP-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-RAG-RAPP-NULL(IX-TAB-P56)
              TO P56-TP-RAG-RAPP-NULL
           ELSE
              MOVE (SF)-TP-RAG-RAPP(IX-TAB-P56)
              TO P56-TP-RAG-RAPP
           END-IF
           IF (SF)-COD-CAN-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-CAN-NULL(IX-TAB-P56)
              TO P56-COD-CAN-NULL
           ELSE
              MOVE (SF)-COD-CAN(IX-TAB-P56)
              TO P56-COD-CAN
           END-IF
           IF (SF)-TP-FIN-COST-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-FIN-COST-NULL(IX-TAB-P56)
              TO P56-TP-FIN-COST-NULL
           ELSE
              MOVE (SF)-TP-FIN-COST(IX-TAB-P56)
              TO P56-TP-FIN-COST
           END-IF
           IF (SF)-NAZ-DEST-FND-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-NAZ-DEST-FND-NULL(IX-TAB-P56)
              TO P56-NAZ-DEST-FND-NULL
           ELSE
              MOVE (SF)-NAZ-DEST-FND(IX-TAB-P56)
              TO P56-NAZ-DEST-FND
           END-IF
           IF (SF)-FL-AU-FATCA-AEOI-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-AU-FATCA-AEOI-NULL(IX-TAB-P56)
              TO P56-FL-AU-FATCA-AEOI-NULL
           ELSE
              MOVE (SF)-FL-AU-FATCA-AEOI(IX-TAB-P56)
              TO P56-FL-AU-FATCA-AEOI
           END-IF
           IF (SF)-TP-CAR-FIN-GIUR-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-CAR-FIN-GIUR-NULL(IX-TAB-P56)
              TO P56-TP-CAR-FIN-GIUR-NULL
           ELSE
              MOVE (SF)-TP-CAR-FIN-GIUR(IX-TAB-P56)
              TO P56-TP-CAR-FIN-GIUR
           END-IF
           IF (SF)-TP-CAR-FIN-GIUR-AT-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-CAR-FIN-GIUR-AT-NULL(IX-TAB-P56)
              TO P56-TP-CAR-FIN-GIUR-AT-NULL
           ELSE
              MOVE (SF)-TP-CAR-FIN-GIUR-AT(IX-TAB-P56)
              TO P56-TP-CAR-FIN-GIUR-AT
           END-IF
           IF (SF)-TP-CAR-FIN-GIUR-PA-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-CAR-FIN-GIUR-PA-NULL(IX-TAB-P56)
              TO P56-TP-CAR-FIN-GIUR-PA-NULL
           ELSE
              MOVE (SF)-TP-CAR-FIN-GIUR-PA(IX-TAB-P56)
              TO P56-TP-CAR-FIN-GIUR-PA
           END-IF
           IF (SF)-FL-ISTITUZ-FIN-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-ISTITUZ-FIN-NULL(IX-TAB-P56)
              TO P56-FL-ISTITUZ-FIN-NULL
           ELSE
              MOVE (SF)-FL-ISTITUZ-FIN(IX-TAB-P56)
              TO P56-FL-ISTITUZ-FIN
           END-IF
           IF (SF)-TP-ORI-FND-TIT-EFF-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-ORI-FND-TIT-EFF-NULL(IX-TAB-P56)
              TO P56-TP-ORI-FND-TIT-EFF-NULL
           ELSE
              MOVE (SF)-TP-ORI-FND-TIT-EFF(IX-TAB-P56)
              TO P56-TP-ORI-FND-TIT-EFF
           END-IF
           IF (SF)-PC-ESP-AG-PA-MSC-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-PC-ESP-AG-PA-MSC-NULL(IX-TAB-P56)
              TO P56-PC-ESP-AG-PA-MSC-NULL
           ELSE
              MOVE (SF)-PC-ESP-AG-PA-MSC(IX-TAB-P56)
              TO P56-PC-ESP-AG-PA-MSC
           END-IF
           IF (SF)-FL-PR-TR-USA-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-PR-TR-USA-NULL(IX-TAB-P56)
              TO P56-FL-PR-TR-USA-NULL
           ELSE
              MOVE (SF)-FL-PR-TR-USA(IX-TAB-P56)
              TO P56-FL-PR-TR-USA
           END-IF
           IF (SF)-FL-PR-TR-NO-USA-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-PR-TR-NO-USA-NULL(IX-TAB-P56)
              TO P56-FL-PR-TR-NO-USA-NULL
           ELSE
              MOVE (SF)-FL-PR-TR-NO-USA(IX-TAB-P56)
              TO P56-FL-PR-TR-NO-USA
           END-IF
           IF (SF)-PC-RIP-PAT-AS-VITA-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-PC-RIP-PAT-AS-VITA-NULL(IX-TAB-P56)
              TO P56-PC-RIP-PAT-AS-VITA-NULL
           ELSE
              MOVE (SF)-PC-RIP-PAT-AS-VITA(IX-TAB-P56)
              TO P56-PC-RIP-PAT-AS-VITA
           END-IF
           IF (SF)-PC-RIP-PAT-IM-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-PC-RIP-PAT-IM-NULL(IX-TAB-P56)
              TO P56-PC-RIP-PAT-IM-NULL
           ELSE
              MOVE (SF)-PC-RIP-PAT-IM(IX-TAB-P56)
              TO P56-PC-RIP-PAT-IM
           END-IF
           IF (SF)-PC-RIP-PAT-SET-IM-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-PC-RIP-PAT-SET-IM-NULL(IX-TAB-P56)
              TO P56-PC-RIP-PAT-SET-IM-NULL
           ELSE
              MOVE (SF)-PC-RIP-PAT-SET-IM(IX-TAB-P56)
              TO P56-PC-RIP-PAT-SET-IM
           END-IF
           IF (SF)-TP-STATUS-AEOI-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-STATUS-AEOI-NULL(IX-TAB-P56)
              TO P56-TP-STATUS-AEOI-NULL
           ELSE
              MOVE (SF)-TP-STATUS-AEOI(IX-TAB-P56)
              TO P56-TP-STATUS-AEOI
           END-IF
           IF (SF)-TP-STATUS-FATCA-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-STATUS-FATCA-NULL(IX-TAB-P56)
              TO P56-TP-STATUS-FATCA-NULL
           ELSE
              MOVE (SF)-TP-STATUS-FATCA(IX-TAB-P56)
              TO P56-TP-STATUS-FATCA
           END-IF
           IF (SF)-FL-RAPP-PA-MSC-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-RAPP-PA-MSC-NULL(IX-TAB-P56)
              TO P56-FL-RAPP-PA-MSC-NULL
           ELSE
              MOVE (SF)-FL-RAPP-PA-MSC(IX-TAB-P56)
              TO P56-FL-RAPP-PA-MSC
           END-IF
           IF (SF)-COD-COMUN-SVOL-ATT-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-COMUN-SVOL-ATT-NULL(IX-TAB-P56)
              TO P56-COD-COMUN-SVOL-ATT-NULL
           ELSE
              MOVE (SF)-COD-COMUN-SVOL-ATT(IX-TAB-P56)
              TO P56-COD-COMUN-SVOL-ATT
           END-IF
           IF (SF)-TP-DT-1O-CON-CLI-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-DT-1O-CON-CLI-NULL(IX-TAB-P56)
              TO P56-TP-DT-1O-CON-CLI-NULL
           ELSE
              MOVE (SF)-TP-DT-1O-CON-CLI(IX-TAB-P56)
              TO P56-TP-DT-1O-CON-CLI
           END-IF
           IF (SF)-TP-MOD-EN-RELA-INT-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-MOD-EN-RELA-INT-NULL(IX-TAB-P56)
              TO P56-TP-MOD-EN-RELA-INT-NULL
           ELSE
              MOVE (SF)-TP-MOD-EN-RELA-INT(IX-TAB-P56)
              TO P56-TP-MOD-EN-RELA-INT
           END-IF
           IF (SF)-TP-REDD-ANNU-LRD-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-REDD-ANNU-LRD-NULL(IX-TAB-P56)
              TO P56-TP-REDD-ANNU-LRD-NULL
           ELSE
              MOVE (SF)-TP-REDD-ANNU-LRD(IX-TAB-P56)
              TO P56-TP-REDD-ANNU-LRD
           END-IF
           IF (SF)-TP-REDD-CON-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-REDD-CON-NULL(IX-TAB-P56)
              TO P56-TP-REDD-CON-NULL
           ELSE
              MOVE (SF)-TP-REDD-CON(IX-TAB-P56)
              TO P56-TP-REDD-CON
           END-IF
           IF (SF)-TP-OPER-SOC-FID-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-OPER-SOC-FID-NULL(IX-TAB-P56)
              TO P56-TP-OPER-SOC-FID-NULL
           ELSE
              MOVE (SF)-TP-OPER-SOC-FID(IX-TAB-P56)
              TO P56-TP-OPER-SOC-FID
           END-IF
           IF (SF)-COD-PA-ESP-MSC-1-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-PA-ESP-MSC-1-NULL(IX-TAB-P56)
              TO P56-COD-PA-ESP-MSC-1-NULL
           ELSE
              MOVE (SF)-COD-PA-ESP-MSC-1(IX-TAB-P56)
              TO P56-COD-PA-ESP-MSC-1
           END-IF
           IF (SF)-IMP-PA-ESP-MSC-1-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-IMP-PA-ESP-MSC-1-NULL(IX-TAB-P56)
              TO P56-IMP-PA-ESP-MSC-1-NULL
           ELSE
              MOVE (SF)-IMP-PA-ESP-MSC-1(IX-TAB-P56)
              TO P56-IMP-PA-ESP-MSC-1
           END-IF
           IF (SF)-COD-PA-ESP-MSC-2-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-PA-ESP-MSC-2-NULL(IX-TAB-P56)
              TO P56-COD-PA-ESP-MSC-2-NULL
           ELSE
              MOVE (SF)-COD-PA-ESP-MSC-2(IX-TAB-P56)
              TO P56-COD-PA-ESP-MSC-2
           END-IF
           IF (SF)-IMP-PA-ESP-MSC-2-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-IMP-PA-ESP-MSC-2-NULL(IX-TAB-P56)
              TO P56-IMP-PA-ESP-MSC-2-NULL
           ELSE
              MOVE (SF)-IMP-PA-ESP-MSC-2(IX-TAB-P56)
              TO P56-IMP-PA-ESP-MSC-2
           END-IF
           IF (SF)-COD-PA-ESP-MSC-3-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-PA-ESP-MSC-3-NULL(IX-TAB-P56)
              TO P56-COD-PA-ESP-MSC-3-NULL
           ELSE
              MOVE (SF)-COD-PA-ESP-MSC-3(IX-TAB-P56)
              TO P56-COD-PA-ESP-MSC-3
           END-IF
           IF (SF)-IMP-PA-ESP-MSC-3-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-IMP-PA-ESP-MSC-3-NULL(IX-TAB-P56)
              TO P56-IMP-PA-ESP-MSC-3-NULL
           ELSE
              MOVE (SF)-IMP-PA-ESP-MSC-3(IX-TAB-P56)
              TO P56-IMP-PA-ESP-MSC-3
           END-IF
           IF (SF)-COD-PA-ESP-MSC-4-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-PA-ESP-MSC-4-NULL(IX-TAB-P56)
              TO P56-COD-PA-ESP-MSC-4-NULL
           ELSE
              MOVE (SF)-COD-PA-ESP-MSC-4(IX-TAB-P56)
              TO P56-COD-PA-ESP-MSC-4
           END-IF
           IF (SF)-IMP-PA-ESP-MSC-4-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-IMP-PA-ESP-MSC-4-NULL(IX-TAB-P56)
              TO P56-IMP-PA-ESP-MSC-4-NULL
           ELSE
              MOVE (SF)-IMP-PA-ESP-MSC-4(IX-TAB-P56)
              TO P56-IMP-PA-ESP-MSC-4
           END-IF
           IF (SF)-COD-PA-ESP-MSC-5-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-PA-ESP-MSC-5-NULL(IX-TAB-P56)
              TO P56-COD-PA-ESP-MSC-5-NULL
           ELSE
              MOVE (SF)-COD-PA-ESP-MSC-5(IX-TAB-P56)
              TO P56-COD-PA-ESP-MSC-5
           END-IF
           IF (SF)-IMP-PA-ESP-MSC-5-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-IMP-PA-ESP-MSC-5-NULL(IX-TAB-P56)
              TO P56-IMP-PA-ESP-MSC-5-NULL
           ELSE
              MOVE (SF)-IMP-PA-ESP-MSC-5(IX-TAB-P56)
              TO P56-IMP-PA-ESP-MSC-5
           END-IF
           MOVE (SF)-DESC-ORGN-FND(IX-TAB-P56)
              TO P56-DESC-ORGN-FND
           IF (SF)-COD-AUT-DUE-DIL-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-AUT-DUE-DIL-NULL(IX-TAB-P56)
              TO P56-COD-AUT-DUE-DIL-NULL
           ELSE
              MOVE (SF)-COD-AUT-DUE-DIL(IX-TAB-P56)
              TO P56-COD-AUT-DUE-DIL
           END-IF
           IF (SF)-FL-PR-QUEST-FATCA-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-PR-QUEST-FATCA-NULL(IX-TAB-P56)
              TO P56-FL-PR-QUEST-FATCA-NULL
           ELSE
              MOVE (SF)-FL-PR-QUEST-FATCA(IX-TAB-P56)
              TO P56-FL-PR-QUEST-FATCA
           END-IF
           IF (SF)-FL-PR-QUEST-AEOI-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-PR-QUEST-AEOI-NULL(IX-TAB-P56)
              TO P56-FL-PR-QUEST-AEOI-NULL
           ELSE
              MOVE (SF)-FL-PR-QUEST-AEOI(IX-TAB-P56)
              TO P56-FL-PR-QUEST-AEOI
           END-IF
           IF (SF)-FL-PR-QUEST-OFAC-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-PR-QUEST-OFAC-NULL(IX-TAB-P56)
              TO P56-FL-PR-QUEST-OFAC-NULL
           ELSE
              MOVE (SF)-FL-PR-QUEST-OFAC(IX-TAB-P56)
              TO P56-FL-PR-QUEST-OFAC
           END-IF
           IF (SF)-FL-PR-QUEST-KYC-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-PR-QUEST-KYC-NULL(IX-TAB-P56)
              TO P56-FL-PR-QUEST-KYC-NULL
           ELSE
              MOVE (SF)-FL-PR-QUEST-KYC(IX-TAB-P56)
              TO P56-FL-PR-QUEST-KYC
           END-IF
           IF (SF)-FL-PR-QUEST-MSCQ-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-PR-QUEST-MSCQ-NULL(IX-TAB-P56)
              TO P56-FL-PR-QUEST-MSCQ-NULL
           ELSE
              MOVE (SF)-FL-PR-QUEST-MSCQ(IX-TAB-P56)
              TO P56-FL-PR-QUEST-MSCQ
           END-IF
           IF (SF)-TP-NOT-PREG-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-NOT-PREG-NULL(IX-TAB-P56)
              TO P56-TP-NOT-PREG-NULL
           ELSE
              MOVE (SF)-TP-NOT-PREG(IX-TAB-P56)
              TO P56-TP-NOT-PREG
           END-IF
           IF (SF)-TP-PROC-PNL-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-PROC-PNL-NULL(IX-TAB-P56)
              TO P56-TP-PROC-PNL-NULL
           ELSE
              MOVE (SF)-TP-PROC-PNL(IX-TAB-P56)
              TO P56-TP-PROC-PNL
           END-IF
           IF (SF)-COD-IMP-CAR-PUB-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-IMP-CAR-PUB-NULL(IX-TAB-P56)
              TO P56-COD-IMP-CAR-PUB-NULL
           ELSE
              MOVE (SF)-COD-IMP-CAR-PUB(IX-TAB-P56)
              TO P56-COD-IMP-CAR-PUB
           END-IF
           IF (SF)-OPRZ-SOSPETTE-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-OPRZ-SOSPETTE-NULL(IX-TAB-P56)
              TO P56-OPRZ-SOSPETTE-NULL
           ELSE
              MOVE (SF)-OPRZ-SOSPETTE(IX-TAB-P56)
              TO P56-OPRZ-SOSPETTE
           END-IF
           IF (SF)-ULT-FATT-ANNU-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-ULT-FATT-ANNU-NULL(IX-TAB-P56)
              TO P56-ULT-FATT-ANNU-NULL
           ELSE
              MOVE (SF)-ULT-FATT-ANNU(IX-TAB-P56)
              TO P56-ULT-FATT-ANNU
           END-IF
           MOVE (SF)-DESC-PEP(IX-TAB-P56)
              TO P56-DESC-PEP
           IF (SF)-NUM-TEL-2-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-NUM-TEL-2-NULL(IX-TAB-P56)
              TO P56-NUM-TEL-2-NULL
           ELSE
              MOVE (SF)-NUM-TEL-2(IX-TAB-P56)
              TO P56-NUM-TEL-2
           END-IF
           IF (SF)-IMP-AFI-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-IMP-AFI-NULL(IX-TAB-P56)
              TO P56-IMP-AFI-NULL
           ELSE
              MOVE (SF)-IMP-AFI(IX-TAB-P56)
              TO P56-IMP-AFI
           END-IF
           IF (SF)-FL-NEW-PRO-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-FL-NEW-PRO-NULL(IX-TAB-P56)
              TO P56-FL-NEW-PRO-NULL
           ELSE
              MOVE (SF)-FL-NEW-PRO(IX-TAB-P56)
              TO P56-FL-NEW-PRO
           END-IF
           IF (SF)-TP-MOT-CAMBIO-CNTR-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-TP-MOT-CAMBIO-CNTR-NULL(IX-TAB-P56)
              TO P56-TP-MOT-CAMBIO-CNTR-NULL
           ELSE
              MOVE (SF)-TP-MOT-CAMBIO-CNTR(IX-TAB-P56)
              TO P56-TP-MOT-CAMBIO-CNTR
           END-IF
           MOVE (SF)-DESC-MOT-CAMBIO-CN(IX-TAB-P56)
              TO P56-DESC-MOT-CAMBIO-CN.
           IF (SF)-COD-SOGG-NULL(IX-TAB-P56) = HIGH-VALUES
              MOVE (SF)-COD-SOGG-NULL(IX-TAB-P56)
              TO P56-COD-SOGG-NULL
           ELSE
              MOVE (SF)-COD-SOGG(IX-TAB-P56)
              TO P56-COD-SOGG
           END-IF.
       VAL-DCLGEN-P56-EX.
           EXIT.

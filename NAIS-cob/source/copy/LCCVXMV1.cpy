      ******************************************************************
      *    TP_MOVI (DA 1000 A 1999)
      ******************************************************************
           88 CREAZ-COLLET                            VALUE 1001.
           88 REVIS-COLLET                            VALUE 1002.
           88 STORN-COLLET                            VALUE 1003.
           88 ATTIV-COLLET                            VALUE 1004.
           88 DATI-DEFAULT                            VALUE 1005.
           88 EMISS-POLIZZA                           VALUE 1006.
           88 PRENOT-RICALC                           VALUE 1007.

           88 CREAZ-INDIVI                            VALUE 1011.
           88 REVIS-INDIVI                            VALUE 1012.
           88 STORN-INDIVI                            VALUE 1013.
           88 REVOC-INDIVI                            VALUE 1014.
           88 ATTIV-INDIVI                            VALUE 1015.
           88 CREAZ-INDIVI-REINV                      VALUE 1016.
           88 CREAZ-INDIVI-REINV-LIQ                  VALUE 1017.

           88 STAMPA-PROP-POLI                        VALUE 1018.

           88 TRFOR-INDIVI                            VALUE 1021.
           88 REVIS-TRFOR-INDIVI                      VALUE 1022.
           88 STORN-TRFOR-INDIVI                      VALUE 1023.
           88 REVOC-TRFOR-INDIVI                      VALUE 1024.
           88 ATTIV-TRFOR-INDIVI                      VALUE 1025.

           88 CREAZ-ADESIO                            VALUE 1031.
           88 REVIS-ADESIO                            VALUE 1032.
           88 STORN-ADESIO                            VALUE 1033.
           88 REVOC-ADESIO                            VALUE 1034.
           88 ATTIV-ADESIO                            VALUE 1035.
MIGCOL     88 PREN-CREAZ-ADES                         VALUE 1036.

           88 TRASF-ADESIO                            VALUE 1041.
           88 TRASF-REVADE                            VALUE 1042.
           88 STORN-TRADES                            VALUE 1043.
           88 REVOC-TRADES                            VALUE 1044.
           88 ATTIV-TRADES                            VALUE 1045.

           88 INCLU-GARANZ                            VALUE 1051.
           88 REVIS-GARANZ                            VALUE 1052.
           88 STORN-GARANZ                            VALUE 1053.
           88 ATTIV-GARANZ                            VALUE 1054.
           88 INCLU-GARANZ-COMPLEM                    VALUE 1055.
           88 REVIS-GARANZ-COMPLEM                    VALUE 1056.

           88 INCLU-TRANCH                            VALUE 1061.
           88 REVIS-TRANCH                            VALUE 1062.
           88 STORN-TRANCH                            VALUE 1063.
           88 VERSAM-AGGIUNTIVO                       VALUE 1065.
           88 VERSAM-AGGIUNTIVO-REINV                 VALUE 1066.
           88 RINNOVO-TRANCHE                         VALUE 1075.

           88 CREAZ-PREVEN                            VALUE 1081.
           88 MODIF-PREVEN                            VALUE 1082.
           88 ANNUL-PREVEN                            VALUE 1083.

      *-- INPUT
       01 IDSV0501-INPUT.
          05 IDSV0501-TAB-VARIABILI          OCCURS 200.
             10 IDSV0501-AREA-VARIABILE.
                15 IDSV0501-TP-DATO          PIC  X(01).
                15 IDSV0501-VAL-NUM          PIC  S9(18).
                15 IDSV0501-VAL-IMP          PIC  S9(11)V9(07).
                15 IDSV0501-VAL-PERC         PIC  9(09)V9(09).
                15 IDSV0501-VAL-STR          PIC X(60).

      ***************************************************************
      * FLAGS
      ***************************************************************
       01 TIPO-FORMATO.
          05 IMPORTO                      PIC X(01) VALUE 'I'.
          05 NUMERICO                     PIC X(01) VALUE 'N'.
          05 MILLESIMI                    PIC X(01) VALUE 'M'.
          05 PERCENTUALE                  PIC X(01) VALUE 'P'.
          05 DT                           PIC X(01) VALUE 'D'.
          05 STRINGA                      PIC X(01) VALUE 'S'.
          05 TASSO                        PIC X(01) VALUE 'T'.
      * SIR 20310 GESTIONE LISTE OMOGENEE
          05 LISTA-IMPORTO                PIC X(01) VALUE 'R'.
          05 LISTA-NUMERICO               PIC X(01) VALUE 'H'.
          05 LISTA-MILLESIMI              PIC X(01) VALUE 'Z'.
          05 LISTA-PERCENTUALE            PIC X(01) VALUE 'O'.
          05 LISTA-DT                     PIC X(01) VALUE 'C'.
          05 LISTA-STRINGA                PIC X(01) VALUE 'L'.
          05 LISTA-TASSO                  PIC X(01) VALUE 'T'.

       01 WK-TP-DATO-PARALLELO            PIC X(01).

       77 MISTA                           PIC X(01) VALUE 'X'.

       01 PRIMA-VOLTA                     PIC X(01).
          88 PRIMA-VOLTA-SI               VALUE 'S'.
          88 PRIMA-VOLTA-NO               VALUE 'N'.

       01 TIPO-SEGNO                      PIC X(01).
          88 SENZA-SEGNO                  VALUE ' '.
          88 SEGNO-POSITIVO               VALUE '+'.
          88 SEGNO-NEGATIVO               VALUE '-'.

       01 DATO-INPUT-TROVATO              PIC X(01).
          88 DATO-INPUT-TROVATO-SI        VALUE 'S'.
          88 DATO-INPUT-TROVATO-NO        VALUE 'N'.

       01 FINE-STRINGA                    PIC X(01).
          88 FINE-STRINGA-SI              VALUE 'S'.
          88 FINE-STRINGA-NO              VALUE 'N'.

       01 ALFANUMERICO-TROVATO            PIC X(01).
          88 ALFANUMERICO-TROVATO-SI      VALUE 'S'.
          88 ALFANUMERICO-TROVATO-NO      VALUE 'N'.

       01 DEC-TROVATO                     PIC X(01).
          88 DEC-TROVATO-SI               VALUE 'S'.
          88 DEC-TROVATO-NO               VALUE 'N'.

       01 INT-TROVATO                     PIC X(01).
          88 INT-TROVATO-SI               VALUE 'S'.
          88 INT-TROVATO-NO               VALUE 'N'.

       01 IDSV0501-SEGNO-POSTIV           PIC X(01).
          88 IDSV0501-SEGNO-POSTIV-SI     VALUE 'S'.
          88 IDSV0501-SEGNO-POSTIV-NO     VALUE 'N'.

       01 IDSV0501-POS-SEGNO              PIC X(01).
          88 IDSV0501-SEGNO-ANTERIORE     VALUE 'A'.
          88 IDSV0501-SEGNO-POSTERIORE    VALUE 'P'.

      ***************************************************************
      * INDICI
      ***************************************************************
       77 IND-VAR                         PIC 9(02).
       77 IND-STRINGA                     PIC 9(02).
       77 IND-ALFA                        PIC 9(02).
       77 IND-INT                         PIC 9(02).
       77 IND-DEC                         PIC 9(02).
       77 IND-OUT                         PIC 9(02).
       77 IND-RICERCA                     PIC 9(02).
       77 IND-TP-DATO                     PIC 9(02).

      ***************************************************************
      * LIMITI
      ***************************************************************
       77 LIMITE-INT-DEC                  PIC 9(02) VALUE 18.
       77 LIMITE-STRINGA                  PIC 9(02) VALUE 60.
       77 LIMITE-ARRAY-INPUT              PIC 9(03) VALUE 200.
       77 LIMITE-ARRAY-OUTPUT             PIC 9(02) VALUE 20.

       77 START-RICERCA                   PIC 9(02).
       77 START-INTERO                    PIC 9(02).
       77 START-DECIMALE                  PIC 9(02).

       77 LIMITE-DECIMALI                 PIC 9(02) VALUE 0.
       77 LIMITE-INTERI                   PIC 9(02) VALUE 0.
       77 LIM-INT-IMP                     PIC 9(02) VALUE 11.
       77 LIM-DEC-IMP                     PIC 9(02) VALUE 12.
       77 LIM-INT-PERC                    PIC 9(02) VALUE 9.
       77 LIM-DEC-PERC                    PIC 9(02) VALUE 10.
       77 LIM-TP-DATO                     PIC 9(02) VALUE 10.

       77 IDSV0501-DECIMALI-ESPOSTI-IMP   PIC 9(1).
       77 IDSV0501-DECIMALI-ESPOSTI-PERC  PIC 9(1).
       77 IDSV0501-DECIMALI-ESPOSTI-TASS  PIC 9(1).

       77 IDSV0501-SEPARATORE             PIC X(01).
       77 IDSV0501-SIMBOLO-DECIMALE       PIC X(01).

       77 SPAZIO-RICHIESTO                PIC 9(02).
       77 SPAZIO-RESTANTE                 PIC 9(02).
       77 POSIZIONE                       PIC 9(02).

       77 LUNGHEZZA-STRINGZA              PIC 9(02) VALUE 0.
      ***************************************************************
      * COMODO
      ***************************************************************

       01 INPUT-PARALLELO.
          05 TABELLA-TP-DATO              OCCURS 200.
             15 TP-DATO-PARALLELO         PIC  X(01).

       01 STRUCT-NUM                      PIC 9(18).
       01 STRUCT-IMP                      REDEFINES
           STRUCT-NUM                     PIC 9(11)V9(07).
       01 STRUCT-PERC                     REDEFINES
           STRUCT-NUM                     PIC 9(09)V9(09).
       01 ELEMENT-NUM-IMP                 REDEFINES  STRUCT-NUM.
          05 ELE-NUM-IMP                  PIC X OCCURS 18.

       01 STRUCT-ALF                      PIC X(18).
       01 STRUCT-IMP2                     REDEFINES
           STRUCT-ALF                     PIC 9(11)V9(07).
       01 STRUCT-PERC2                    REDEFINES
           STRUCT-ALF                     PIC 9(09)V9(09).
       01 STRUCT-NUM2                     REDEFINES
           STRUCT-ALF                     PIC 9(18).

       01 STRUCT-ALFA                     PIC X(60).
       01 ELEMENT-ALFA                    REDEFINES  STRUCT-ALFA.
          05 ELE-ALFA                     PIC X OCCURS 60.

       01 CAMPO-INTERI                    PIC X(18).
       01 ARRAY-STRINGA-INTERI REDEFINES  CAMPO-INTERI.
          05 ELE-STRINGA-INTERI           PIC X OCCURS 18.

       01 CAMPO-DECIMALI                  PIC X(18).
       01 ARRAY-STRINGA-DECIMALI REDEFINES CAMPO-DECIMALI.
          05 ELE-STRINGA-DECIMALI         PIC X OCCURS 18.

       01 CAMPO-ALFA                      PIC X(60).
       01 ARRAY-STRINGA-ALFA REDEFINES    CAMPO-ALFA.
          05 ELE-STRINGA-ALFA             PIC X OCCURS 60.

       01 TIPO-DATO-UNSTR.
          05 TP-DATO-UNSTR-ELE-MAX        PIC S9(02) COMP.
          05 TAB-TP-DATO-UNSTR            OCCURS 10.
             15 TP-DATO-UNSTR             PIC  X(01).

       01 TIPO-DATO-UNSTRING              PIC  X(01).
      ***************************************************************
      *
      ***************************************************************
       01 IDSV0501-OUTPUT.
          05 IDSV0501-TP-STRINGA          PIC  X(01).
          05 IDSV0501-MAX-TAB-STR         PIC S9(02) COMP.
          05 IDSV0501-TAB-STRINGHE-TOT    OCCURS 20.
             10 IDSV0501-STRINGA-TOT      PIC  X(60).

       01 IDSV0501-CAMPI-ESITO.
          05 IDSV0501-RETURN-CODE         PIC  X(002).
             88 IDSV0501-SUCCESSFUL-RC    VALUE '00'.
             88 IDSV0501-GENERIC-ERROR    VALUE 'KO'.

          05 IDSV0501-DESCRIZ-ERR         PIC  X(300).

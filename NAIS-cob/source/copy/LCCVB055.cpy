
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVB055
      *   ULTIMO AGG. 14 MAR 2011
      *------------------------------------------------------------

       VAL-DCLGEN-B05.
           MOVE (SF)-ID-BILA-VAR-CALC-T
              TO B05-ID-BILA-VAR-CALC-T
           MOVE (SF)-COD-COMP-ANIA
              TO B05-COD-COMP-ANIA
           MOVE (SF)-ID-BILA-TRCH-ESTR
              TO B05-ID-BILA-TRCH-ESTR
           MOVE (SF)-ID-RICH-ESTRAZ-MAS
              TO B05-ID-RICH-ESTRAZ-MAS
           IF (SF)-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
              MOVE (SF)-ID-RICH-ESTRAZ-AGG-NULL
              TO B05-ID-RICH-ESTRAZ-AGG-NULL
           ELSE
              MOVE (SF)-ID-RICH-ESTRAZ-AGG
              TO B05-ID-RICH-ESTRAZ-AGG
           END-IF
           IF (SF)-DT-RIS-NULL = HIGH-VALUES
              MOVE (SF)-DT-RIS-NULL
              TO B05-DT-RIS-NULL
           ELSE
             IF (SF)-DT-RIS = ZERO
                MOVE HIGH-VALUES
                TO B05-DT-RIS-NULL
             ELSE
              MOVE (SF)-DT-RIS
              TO B05-DT-RIS
             END-IF
           END-IF
           MOVE (SF)-ID-POLI
              TO B05-ID-POLI
           MOVE (SF)-ID-ADES
              TO B05-ID-ADES
           MOVE (SF)-ID-TRCH-DI-GAR
              TO B05-ID-TRCH-DI-GAR
           IF (SF)-PROG-SCHEDA-VALOR-NULL = HIGH-VALUES
              MOVE (SF)-PROG-SCHEDA-VALOR-NULL
              TO B05-PROG-SCHEDA-VALOR-NULL
           ELSE
              MOVE (SF)-PROG-SCHEDA-VALOR
              TO B05-PROG-SCHEDA-VALOR
           END-IF
           MOVE (SF)-DT-INI-VLDT-TARI
              TO B05-DT-INI-VLDT-TARI
           MOVE (SF)-TP-RGM-FISC
              TO B05-TP-RGM-FISC
           MOVE (SF)-DT-INI-VLDT-PROD
              TO B05-DT-INI-VLDT-PROD
           MOVE (SF)-DT-DECOR-TRCH
              TO B05-DT-DECOR-TRCH
           MOVE (SF)-COD-VAR
              TO B05-COD-VAR
           MOVE (SF)-TP-D
              TO B05-TP-D
           IF (SF)-VAL-IMP-NULL = HIGH-VALUES
              MOVE (SF)-VAL-IMP-NULL
              TO B05-VAL-IMP-NULL
           ELSE
              MOVE (SF)-VAL-IMP
              TO B05-VAL-IMP
           END-IF
           IF (SF)-VAL-PC-NULL = HIGH-VALUES
              MOVE (SF)-VAL-PC-NULL
              TO B05-VAL-PC-NULL
           ELSE
              MOVE (SF)-VAL-PC
              TO B05-VAL-PC
           END-IF
           IF (SF)-VAL-STRINGA-NULL = HIGH-VALUES
              MOVE (SF)-VAL-STRINGA-NULL
              TO B05-VAL-STRINGA-NULL
           ELSE
              MOVE (SF)-VAL-STRINGA
              TO B05-VAL-STRINGA
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO B05-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO B05-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO B05-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO B05-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO B05-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO B05-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO B05-DS-STATO-ELAB
           MOVE (SF)-AREA-D-VALOR-VAR-VCHAR
              TO B05-AREA-D-VALOR-VAR-VCHAR.
       VAL-DCLGEN-B05-EX.
           EXIT.

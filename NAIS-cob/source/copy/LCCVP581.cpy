      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA IMPST_BOLLO
      *   ALIAS P58
      *   ULTIMO AGG. 09 AGO 2013
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-IMPST-BOLLO PIC S9(9)     COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-IB-POLI PIC X(40).
             07 (SF)-COD-FISC PIC X(16).
             07 (SF)-COD-FISC-NULL REDEFINES
                (SF)-COD-FISC   PIC X(16).
             07 (SF)-COD-PART-IVA PIC X(11).
             07 (SF)-COD-PART-IVA-NULL REDEFINES
                (SF)-COD-PART-IVA   PIC X(11).
             07 (SF)-ID-RAPP-ANA PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-INI-CALC   PIC S9(8) COMP-3.
             07 (SF)-DT-END-CALC   PIC S9(8) COMP-3.
             07 (SF)-TP-CAUS-BOLLO PIC X(2).
             07 (SF)-IMPST-BOLLO-DETT-C PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-BOLLO-DETT-V PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-BOLLO-DETT-V-NULL REDEFINES
                (SF)-IMPST-BOLLO-DETT-V   PIC X(8).
             07 (SF)-IMPST-BOLLO-TOT-V PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-BOLLO-TOT-V-NULL REDEFINES
                (SF)-IMPST-BOLLO-TOT-V   PIC X(8).
             07 (SF)-IMPST-BOLLO-TOT-R PIC S9(12)V9(3) COMP-3.
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).

           EXEC SQL DECLARE QUOTZ_FND_UNIT TABLE
           (
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             COD_FND             CHAR(12) NOT NULL,
             DT_QTZ              DATE NOT NULL,
             VAL_QUO             DECIMAL(12, 5),
             VAL_QUO_MANFEE      DECIMAL(12, 5),
             DS_OPER_SQL         CHAR(1) NOT NULL,
             DS_VER              DECIMAL(9, 0) NOT NULL,
             DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL,
             DS_UTENTE           CHAR(20) NOT NULL,
             DS_STATO_ELAB       CHAR(1) NOT NULL,
             TP_FND              CHAR(1) NOT NULL,
             DT_RILEVAZIONE_NAV  DATE,
             VAL_QUO_ACQ         DECIMAL(12, 5),
             FL_NO_NAV           CHAR(1)
          ) END-EXEC.

       01  IJCCMQ04-VAR-AMBIENTE.
           05 IJCCMQ04-COD-COMP-ANIA              PIC 9(05).
           05 IJCCMQ04-AMBIENTE                   PIC X(10).
           05 IJCCMQ04-PIATTAFORMA                PIC X(10).
               88 IJCCMQ04-UNIX               VALUE 'UNIX      '.
               88 IJCCMQ04-HOST               VALUE 'HOST      '.
           05 IJCCMQ04-COB-TO-JAVA                PIC X(10).
              88 IJCCMQ04-MQ                  VALUE 'MQ        '.
              88 IJCCMQ04-CSOCKET             VALUE 'C-SOCKET  '.
              88 IJCCMQ04-JCICS-COO           VALUE 'JCICSCOO  '.
           05 IJCCMQ04-TP-UTILIZZO-API            PIC X(03).
              88 IJCCMQ04-CALL-COBOL              VALUE 'COB'.
              88 IJCCMQ04-CALL-C                  VALUE 'C  '.
            05 IJCCMQ04-QUEUE-MANAGER              PIC X(48).
            05 IJCCMQ04-CODA-PUT                   PIC X(48).
              88 IJCCMQ04-PUT-A               VALUE 'JCALL08   '.
              88 IJCCMQ04-PUT-B               VALUE 'JCALL05   '.
              88 IJCCMQ04-PUT-C               VALUE 'TST2IN    '.
              88 IJCCMQ04-PUT-D               VALUE 'JCALL06   '.
           05 IJCCMQ04-CODA-GET                   PIC X(48).
              88 IJCCMQ04-GET-A               VALUE 'JRET08    '.
              88 IJCCMQ04-GET-B               VALUE 'JRET05    '.
              88 IJCCMQ04-GET-C               VALUE 'TST2OU    '.
              88 IJCCMQ04-GET-D               VALUE 'JRET06    '.
           05 IJCCMQ04-OPZ-PERSISTENZA            PIC X(01).
              88  IJCCMQ04-OP-PERSISTENZA         VALUE 'S'.
              88  IJCCMQ04-OP-NO-PERSISTENZA      VALUE 'N'.
              88  IJCCMQ04-OP-VALIDA              VALUE 'S' 'N'.
           05 IJCCMQ04-OPZ-WAIT                   PIC X(01).
              88  IJCCMQ04-OW-SI-WAIT             VALUE 'S'.
              88  IJCCMQ04-OW-NO-WAIT             VALUE 'N'.
              88  IJCCMQ04-OW-VALIDA              VALUE 'S' 'N'.
           05 IJCCMQ04-OPZ-SYNCPOINT              PIC X(01).
              88  IJCCMQ04-OS-SYNCPOINT           VALUE 'S'.
              88  IJCCMQ04-OS-NO-SYNCPOINT        VALUE 'N'.
              88  IJCCMQ04-OS-VALIDA              VALUE 'S' 'N'.
           05 IJCCMQ04-ATTESA-RISPOSTA            PIC X(01).
              88  IJCCMQ04-AR-ILLIMITATA          VALUE 'I'.
              88  IJCCMQ04-AR-LIMITATA            VALUE 'L'.
              88  IJCCMQ04-AR-VALIDA              VALUE 'I' 'L'.
           05 IJCCMQ04-TEMPO-ATTESA-1             PIC S9(9) COMP.
           05 IJCCMQ04-TEMPO-ATTESA-2             PIC S9(9) COMP.
           05 IJCCMQ04-TEMPO-EXPIRY               PIC S9(9) COMP.
           05 IJCCMQ04-CSOCKET-IP-ADDRESS         PIC X(20).
           05 IJCCMQ04-CSOCKET-PORT-NUM           PIC 9(05).
           05 IJCCMQ04-FL-COMPRESSORE-C           PIC X(01).
              88 IJCCMQ04-FL-COMPRESSORE-C-SI     VALUE 'S'.
              88 IJCCMQ04-FL-COMPRESSORE-C-NO     VALUE 'N'.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVL713
      *   ULTIMO AGG. 22 APR 2010
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-L71.
           MOVE L71-ID-MATR-ELAB-BATCH
             TO (SF)-ID-PTF
           MOVE L71-ID-MATR-ELAB-BATCH
             TO (SF)-ID-MATR-ELAB-BATCH
           MOVE L71-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE L71-TP-FRM-ASSVA
             TO (SF)-TP-FRM-ASSVA
           MOVE L71-TP-MOVI
             TO (SF)-TP-MOVI
           IF L71-COD-RAMO-NULL = HIGH-VALUES
              MOVE L71-COD-RAMO-NULL
                TO (SF)-COD-RAMO-NULL
           ELSE
              MOVE L71-COD-RAMO
                TO (SF)-COD-RAMO
           END-IF
           MOVE L71-DT-ULT-ELAB
             TO (SF)-DT-ULT-ELAB
           MOVE L71-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE L71-DS-VER
             TO (SF)-DS-VER
           MOVE L71-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE L71-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE L71-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB.
       VALORIZZA-OUTPUT-L71-EX.
           EXIT.

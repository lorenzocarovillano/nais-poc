      *----------------------------------------------------------------*
      *     COPY COMUNE DI INTERFACCIA  BACK-END --> FRONT-END
      *     LUNGHEZZA COMPLESSIVA : 5000 BYTES
      *----------------------------------------------------------------*

            05 (SF)-TIPO-OPERAZIONE                 PIC X(002).
                88 PREPARA-MAPPA                    VALUE '00'.
                88 CONVALIDA                        VALUE '01'.
                88 CALCOLI                          VALUE '02'.
                88 CTR-RICALCOLO                    VALUE '03'.
                88 EOC-CON-PREVENTIVO               VALUE '04'.
                88 ACQUIS-RICH-EST                  VALUE '08'.

            05 (SF)-NAVIGABILITA                    PIC X(001).
                88 AMMESSA                          VALUE 'A'.
                88 NON-AMMESSA                      VALUE 'N'.
                88 EMBEDDED-MOD                     VALUE 'E'.
                88 EMBEDDED-READ-ONLY               VALUE 'R'.


            05 (SF)-TASTI-DA-ABILITARE.
      *--> IL TASTO "ANNULLA" SI RIFERISCE AL TASTO "ESCI"
                10 (SF)-TASTO-ANNULLA               PIC X(001).
                10 (SF)-TASTO-DA-AUTORIZZARE        PIC X(001).
                10 (SF)-TASTO-AUTORIZZA             PIC X(001).
                10 (SF)-TASTO-AGGIORNA-PTF          PIC X(001).
                10 (SF)-TASTO-RESPINGI              PIC X(001).
                10 (SF)-TASTO-ELIMINA               PIC X(001).
                10 (SF)-TASTO-AVANTI                PIC X(001).
                10 (SF)-TASTO-INDIETRO              PIC X(001).
                10 (SF)-TASTO-NOTE                  PIC X(001).

            05 (SF)-DATI-ACTUATOR.
                10 (SF)-DT-ULT-VERS-PROD            PIC 9(008).

            05 (SF)-STATI.
                10 (SF)-STATO-MOVIMENTO             PIC X(002).
                10 (SF)-MOV-FLAG-ST-FINALE          PIC X(001).
                    88 (SF)-MOV-ST-FINALE-CON-EFF   VALUE '1'.
                    88 (SF)-MOV-ST-FINALE-SENZA-EFF VALUE '2'.
                    88 (SF)-MOV-ST-NON-FINALE       VALUE '3'.

                10 (SF)-STATO-OGG-DEROGA            PIC X(002).
                10 (SF)-DER-FLAG-ST-FINALE          PIC X(001).
                    88 (SF)-DER-ST-FINALE-CON-EFF   VALUE '1'.
                    88 (SF)-DER-ST-FINALE-SENZA-EFF VALUE '2'.
                    88 (SF)-DER-ST-NON-FINALE       VALUE '3'.

                10 (SF)-STATO-RICHIESTA-EST         PIC X(002).
                10 (SF)-RIC-FLAG-ST-FINALE          PIC X(001).
                    88 (SF)-RIC-ST-FINALE-CON-EFF   VALUE '1'.
                    88 (SF)-RIC-ST-FINALE-SENZA-EFF VALUE '2'.
                    88 (SF)-RIC-ST-NON-FINALE       VALUE '3'.

            05 (SF)-DATI-DEROGHE.

                10 (SF)-KEY-AUT-OPER1               PIC X(020).
                10 (SF)-KEY-AUT-OPER2               PIC X(020).
                10 (SF)-KEY-AUT-OPER3               PIC X(020).
                10 (SF)-KEY-AUT-OPER4               PIC X(020).
                10 (SF)-KEY-AUT-OPER5               PIC X(020).

                10 (SF)-COD-PROCESSO-WF             PIC X(003).
                10 (SF)-COD-ATTIVITA-WF             PIC X(010).

                10 (SF)-NUM-ELE-VARIABILI-COND      PIC 9(003).
                10 (SF)-VARIABILI-COND-WF           OCCURS 20.
                   15 (SF)-COD-OPERANDO-WF          PIC X(030).
                   15 (SF)-VAL-OPERANDO-WF          PIC X(100).

                10 (SF)-COD-LIV-AUT-PROFIL          PIC S9(05) COMP-3.
                10 (SF)-COD-GRU-AUT-PROFIL          PIC S9(10) COMP-3.
                10 (SF)-CANALE-VENDITA              PIC S9(05) COMP-3.
                10 (SF)-PUNTO-VENDITA               PIC X(006).

                10 (SF)-OGGETTO-DEROGA.
                   15 (SF)-ID-OGG-DEROGA            PIC S9(09) COMP-3.
                   15 (SF)-COD-LIV-AUT-APPARTZA     PIC S9(05) COMP-3.
                   15 (SF)-COD-GRU-AUT-APPARTZA     PIC S9(10) COMP-3.
                   15 (SF)-COD-LIV-AUT-SUPER        PIC S9(05) COMP-3.
                   15 (SF)-COD-GRU-AUT-SUPER        PIC S9(10) COMP-3.
                   15 (SF)-TIPO-DEROGA              PIC X(002).
                   15 (SF)-IB-OGG                   PIC X(040).
                   15 (SF)-ID-OGG                   PIC S9(09) COMP-3.
                   15 (SF)-TP-OGG                   PIC X(002).
                   15 (SF)-DS-VER                   PIC S9(09) COMP-3.

                10 (SF)-MOTIVI-DEROGA.
                   15 (SF)-NUM-ELE-MOT-DEROGA       PIC 9(003).
                   15 (SF)-TAB-MOT-DEROGA           OCCURS 20.
                      20 (SF)-STATUS                PIC X(001).
                          88 (SF)-ST-ADD            VALUE 'A'.
                          88 (SF)-ST-MOD            VALUE 'M'.
                          88 (SF)-ST-INV            VALUE 'I'.
                          88 (SF)-ST-DEL            VALUE 'D'.
                          88 (SF)-ST-CON            VALUE 'C'.
                      20 (SF)-ID-MOT-DEROGA         PIC S9(09) COMP-3.
                      20 (SF)-COD-ERR               PIC X(012).
                      20 (SF)-TP-ERR                PIC X(002).
                      20 (SF)-DESCRIZIONE-ERR       PIC X(050).
                      20 (SF)-TP-MOT-DEROGA         PIC X(002).
                      20 (SF)-COD-LIV-AUTORIZZATIVO PIC S9(05) COMP-3.
                      20 (SF)-IDC-FORZABILITA       PIC X(001).
                      20 (SF)-MOT-DER-DT-EFFETTO    PIC S9(08) COMP-3.
                      20 (SF)-MOT-DER-DT-COMPETENZA PIC S9(08) COMP-3.
                      20 (SF)-MOT-DER-DS-VER        PIC S9(09) COMP-3.
                      20 (SF)-MOT-DER-STEP-ELAB     PIC X(1).

            05 (SF)-MOVIMENTI-ANNULL.
               10 (SF)-ELE-MOV-ANNULL-MAX           PIC S9(04) COMP.
               10 (SF)-TAB-MOV-ANNULL      OCCURS 10.
                 15 (SF)-ID-MOVI-ANN                PIC S9(9)  COMP-3.


            05 (SF)-ID-MOVI-CRZ                     PIC S9(9)  COMP-3.

            05 (SF)-FLAG-TARIFFA-RISCHIO            PIC X(001).

            05 (SF)-AREA-PLATFOND.
               10 (SF)-ELE-MAX-PLATFOND     PIC S9(04) COMP.
               10 (SF)-TAB-PLATFOND         OCCURS 3.
                  15 (SF)-IMP-PLATFOND      PIC S9(15)V9(003)  COMP-3.
                  15 (SF)-COD-FONDO         PIC X(012).
                  15 (SF)-PERC-FOND-OLD     PIC S9(3)V9(3)     COMP-3.
                  15 (SF)-IMPORTO-OLD       PIC S9(12)V9(3)    COMP-3.

            05 (SF)-MODIFICA-DTDECOR                PIC X(001).
               88 (SF)-MODIFICA-DTDECOR-SI          VALUE 'S'.
               88 (SF)-MODIFICA-DTDECOR-NO          VALUE 'N'.

            05 (SF)-STATUS-DER              PIC X(01).
               88 (SF)-ST-DE-ADD                VALUE 'A'.
               88 (SF)-ST-DE-MOD                VALUE 'M'.
               88 (SF)-ST-DE-INV                VALUE 'I'.
               88 (SF)-ST-DE-DEL                VALUE 'D'.
               88 (SF)-ST-DE-CON                VALUE 'C'.

            05 (SF)-ID-RICH-EST                     PIC S9(9)  COMP-3.
            05 (SF)-CODICE-INIZIATIVA               PIC X(12).
            05 (SF)-TP-VISUALIZ-PAG         PIC X(01).
               88 (SF)-MODIFICA                VALUE 'M'.
               88 (SF)-CONSULTA                VALUE 'C'.


            05 FILLER                               PIC X(135).

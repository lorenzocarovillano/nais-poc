           EXEC SQL DECLARE STAT_OGG_WF TABLE
           (
             ID_STAT_OGG_WF      DECIMAL(9, 0) NOT NULL,
             ID_OGG              DECIMAL(9, 0) NOT NULL,
             TP_OGG              CHAR(2) NOT NULL,
             ID_MOVI_CRZ         DECIMAL(9, 0) NOT NULL,
             ID_MOVI_CHIU        DECIMAL(9, 0),
             DT_INI_EFF          DATE NOT NULL,
             DT_END_EFF          DATE NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             COD_PRCS            CHAR(3) NOT NULL,
             COD_ATTVT           CHAR(10) NOT NULL,
             STAT_OGG_WF         CHAR(2) NOT NULL,
             FL_STAT_END         CHAR(1),
             DS_RIGA             DECIMAL(10, 0) NOT NULL WITH DEFAULT,
             DS_OPER_SQL         CHAR(1) NOT NULL WITH DEFAULT,
             DS_VER              DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             DS_TS_INI_CPTZ      DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_TS_END_CPTZ      DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_UTENTE           CHAR(20) NOT NULL WITH DEFAULT,
             DS_STATO_ELAB       CHAR(1) NOT NULL WITH DEFAULT
          ) END-EXEC.

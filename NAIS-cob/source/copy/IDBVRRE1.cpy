       01 RAPP-RETE.
         05 RRE-ID-RAPP-RETE PIC S9(9)V     COMP-3.
         05 RRE-ID-OGG PIC S9(9)V     COMP-3.
         05 RRE-ID-OGG-NULL REDEFINES
            RRE-ID-OGG   PIC X(5).
         05 RRE-TP-OGG PIC X(2).
         05 RRE-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
         05 RRE-ID-MOVI-CHIU PIC S9(9)V     COMP-3.
         05 RRE-ID-MOVI-CHIU-NULL REDEFINES
            RRE-ID-MOVI-CHIU   PIC X(5).
         05 RRE-DT-INI-EFF   PIC S9(8)V COMP-3.
         05 RRE-DT-END-EFF   PIC S9(8)V COMP-3.
         05 RRE-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 RRE-TP-RETE PIC X(2).
         05 RRE-TP-ACQS-CNTRT PIC S9(5)V     COMP-3.
         05 RRE-TP-ACQS-CNTRT-NULL REDEFINES
            RRE-TP-ACQS-CNTRT   PIC X(3).
         05 RRE-COD-ACQS-CNTRT PIC S9(5)V     COMP-3.
         05 RRE-COD-ACQS-CNTRT-NULL REDEFINES
            RRE-COD-ACQS-CNTRT   PIC X(3).
         05 RRE-COD-PNT-RETE-INI PIC S9(10)V     COMP-3.
         05 RRE-COD-PNT-RETE-INI-NULL REDEFINES
            RRE-COD-PNT-RETE-INI   PIC X(6).
         05 RRE-COD-PNT-RETE-END PIC S9(10)V     COMP-3.
         05 RRE-COD-PNT-RETE-END-NULL REDEFINES
            RRE-COD-PNT-RETE-END   PIC X(6).
         05 RRE-FL-PNT-RETE-1RIO PIC X(1).
         05 RRE-FL-PNT-RETE-1RIO-NULL REDEFINES
            RRE-FL-PNT-RETE-1RIO   PIC X(1).
         05 RRE-COD-CAN PIC S9(5)V     COMP-3.
         05 RRE-COD-CAN-NULL REDEFINES
            RRE-COD-CAN   PIC X(3).
         05 RRE-DS-RIGA PIC S9(10)V     COMP-3.
         05 RRE-DS-OPER-SQL PIC X(1).
         05 RRE-DS-VER PIC S9(9)V     COMP-3.
         05 RRE-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
         05 RRE-DS-TS-END-CPTZ PIC S9(18)V     COMP-3.
         05 RRE-DS-UTENTE PIC X(20).
         05 RRE-DS-STATO-ELAB PIC X(1).
         05 RRE-COD-PNT-RETE-INI-C PIC S9(10)V     COMP-3.
         05 RRE-COD-PNT-RETE-INI-C-NULL REDEFINES
            RRE-COD-PNT-RETE-INI-C   PIC X(6).
         05 RRE-MATR-OPRT PIC X(20).
         05 RRE-MATR-OPRT-NULL REDEFINES
            RRE-MATR-OPRT   PIC X(20).


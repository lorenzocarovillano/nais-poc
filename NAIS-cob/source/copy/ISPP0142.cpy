      *----------------------------------------------------------------*
      *    VALORIZZAZIONE SCHEDE ISPC0140
      *----------------------------------------------------------------*
       VAL-SCHEDE-ISPC0140.

      *--> VALORIZZAZIONE OCCURS AREA INPUT SERVIZIO CALCOLI E CONTROLLI
      *--> CON L'OUTPUT DEL VALORIZZATORE VARIABILI

           MOVE WSKD-DEE
             TO ISPC0140-DEE

           PERFORM AREA-SCHEDA-P-ISPC0140
              THRU AREA-SCHEDA-P-ISPC0140-EX
           VARYING IX-AREA-SCHEDA-P FROM 1 BY 1
             UNTIL IX-AREA-SCHEDA-P > WSKD-ELE-LIVELLO-MAX-P.

           MOVE WSKD-ELE-LIVELLO-MAX-P   TO ISPC0140-ELE-MAX-SCHEDA-P

           PERFORM AREA-SCHEDA-T-ISPC0140
              THRU AREA-SCHEDA-T-ISPC0140-EX
           VARYING IX-AREA-SCHEDA-T FROM 1 BY 1
             UNTIL IX-AREA-SCHEDA-T > WSKD-ELE-LIVELLO-MAX-T.

           MOVE WSKD-ELE-LIVELLO-MAX-T   TO ISPC0140-ELE-MAX-SCHEDA-T.

       VAL-SCHEDE-ISPC0140-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE AREA SCHEDA P
      *----------------------------------------------------------------*
       AREA-SCHEDA-P-ISPC0140.

           MOVE WSKD-TP-LIVELLO-P(IX-AREA-SCHEDA-P)
                     TO ISPC0140-TIPO-LIVELLO-P(IX-AREA-SCHEDA-P)
           MOVE WSKD-COD-LIVELLO-P(IX-AREA-SCHEDA-P)
                     TO ISPC0140-CODICE-LIVELLO-P(IX-AREA-SCHEDA-P)

           IF WSKD-TP-LIVELLO-P(IX-AREA-SCHEDA-P) = 'P'
              MOVE WADE-MOD-CALC
                TO ISPC0140-FLAG-MOD-CALC-PRE-P(IX-AREA-SCHEDA-P)
           END-IF.

           PERFORM AREA-VAR-P-ISPC0140
              THRU AREA-VAR-P-ISPC0140-EX
               VARYING IX-TAB-VAR-P FROM 1 BY 1
                 UNTIL IX-TAB-VAR-P >
                       WSKD-ELE-VARIABILI-MAX-P(IX-AREA-SCHEDA-P)
                    OR IX-TAB-VAR-P > WK-ISPC0140-NUM-COMPON-MAX-P.

           IF WSKD-ELE-VARIABILI-MAX-P(IX-AREA-SCHEDA-P) >
                             WK-ISPC0140-NUM-COMPON-MAX-P
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S12100-AREA-SCHEDA'
                TO IEAI9901-LABEL-ERR
              MOVE '005247'
                TO IEAI9901-COD-ERRORE
              STRING 'NUMERO VARIABILI SUPERA LIMITE PREVISTO'
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       AREA-SCHEDA-P-ISPC0140-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE DELL'AREA VARIABILI DI PRODOTTO
      *    DI INPUT DEL SERVIZIO CALCOLI E CONTROLLI
      *----------------------------------------------------------------*
       AREA-VAR-P-ISPC0140.

49032      ADD 1
49032       TO ISPC0140-NUM-COMPON-MAX-ELE-P(IX-AREA-SCHEDA-P)

           MOVE WSKD-COD-VARIABILE-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
             TO ISPC0140-CODICE-VARIABILE-P
                                    (IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
           MOVE WSKD-TP-DATO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
             TO ISPC0140-TIPO-DATO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)

           MOVE WSKD-VAL-GENERICO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P)
             TO ISPC0140-VAL-GENERICO-P(IX-AREA-SCHEDA-P, IX-TAB-VAR-P).

       AREA-VAR-P-ISPC0140-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE AREA SCHEDA T
      *----------------------------------------------------------------*
       AREA-SCHEDA-T-ISPC0140.

           MOVE WSKD-TP-LIVELLO-T(IX-AREA-SCHEDA-T)
                     TO ISPC0140-TIPO-LIVELLO-T(IX-AREA-SCHEDA-T)
           MOVE WSKD-COD-LIVELLO-T(IX-AREA-SCHEDA-T)
                     TO ISPC0140-CODICE-LIVELLO-T(IX-AREA-SCHEDA-T)

           MOVE WSKD-TIPO-TRCH(IX-AREA-SCHEDA-T)
                     TO ISPC0140-TIPO-TRCH(IX-AREA-SCHEDA-T)

           MOVE WSKD-FLG-ITN(IX-AREA-SCHEDA-T)
                     TO ISPC0140-FLG-ITN(IX-AREA-SCHEDA-T)

           IF WSKD-TP-LIVELLO-T(IX-AREA-SCHEDA-T) NOT = 'P'

              SET WK-GRZ-NON-TROVATA    TO TRUE

      *-->    RICERCA DELL'OCCORRENZA ASSOCIATA AL CODICE LIVELLO DEL
      *-->    VALORIZZATORE VARIABILI SULLA DCLGEN GARANZIA
              PERFORM S12110-AREA-GARANZIA
                 THRU EX-S12110
              VARYING IX-TAB-GRZ FROM 1 BY 1
                UNTIL IX-TAB-GRZ > VGRZ-ELE-GARANZIA-MAX
                   OR WK-GRZ-TROVATA
           END-IF.

           PERFORM AREA-VAR-T-ISPC0140
              THRU AREA-VAR-T-ISPC0140-EX
               VARYING IX-TAB-VAR-T FROM 1 BY 1
                 UNTIL IX-TAB-VAR-T >
                  WSKD-ELE-VARIABILI-MAX-T(IX-AREA-SCHEDA-T)
                    OR IDSV0001-ESITO-KO.

           IF WSKD-ELE-VARIABILI-MAX-T(IX-AREA-SCHEDA-T) >
                             WK-ISPC0140-NUM-COMPON-MAX-T
              MOVE WK-PGM
                TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S12100-AREA-SCHEDA'
                TO IEAI9901-LABEL-ERR
              MOVE '005247'
                TO IEAI9901-COD-ERRORE
              STRING 'NUMERO VARIABILI SUPERA LIMITE PREVISTO'
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       AREA-SCHEDA-T-ISPC0140-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RICERCA DELL'OCCORRENZA ASSOCIATA AL CODICE LIVELLO DEL
      *    VALORIZZATORE VARIABILI SULLA DCLGEN GARANZIA
      *----------------------------------------------------------------*
       S12110-AREA-GARANZIA.

           IF  WSKD-COD-LIVELLO-T(IX-AREA-SCHEDA-T) =
               VGRZ-COD-TARI(IX-TAB-GRZ)
           AND NOT VGRZ-ST-DEL(IX-TAB-GRZ)

               SET WK-GRZ-TROVATA    TO TRUE

               IF VGRZ-TP-PRE-NULL(IX-TAB-GRZ) = HIGH-VALUES
                  MOVE SPACES
                    TO ISPC0140-TIPO-PREMIO-T(IX-AREA-SCHEDA-T)
               ELSE
                  MOVE VGRZ-TP-PRE(IX-TAB-GRZ)
                    TO ISPC0140-TIPO-PREMIO-T(IX-AREA-SCHEDA-T)
               END-IF

               MOVE VGRZ-COD-TARI(IX-TAB-GRZ) TO WS-COD-TARIFFA

               PERFORM CTRL-GAR-INCLUSA
                  THRU CTRL-GAR-INCLUSA-EX
               IF TROVATO
                  MOVE WPAG-FL-GAR-INCLUSA-I(IX-RIC-GRZ)
                    TO ISPC0140-GARANZIA-INCLUSA-T(IX-AREA-SCHEDA-T)
               END-IF

               SET WK-TGA-NON-TROVATA    TO TRUE

               PERFORM S12111-AREA-TRANCHE
                  THRU EX-S12111
               VARYING IX-TAB-TGA FROM 1 BY 1
                 UNTIL IX-TAB-TGA > VTGA-ELE-TRAN-MAX
                    OR WK-TGA-TROVATA

               IF VGRZ-PC-RIP-PRE-NULL(IX-TAB-GRZ) = HIGH-VALUE
                  MOVE ZERO
                    TO ISPC0140-PERC-RIP-PREMIO-T(IX-AREA-SCHEDA-T)
               ELSE
                  MOVE VGRZ-PC-RIP-PRE(IX-TAB-GRZ)
                    TO ISPC0140-PERC-RIP-PREMIO-T(IX-AREA-SCHEDA-T)
               END-IF

           END-IF.

       EX-S12110.
           EXIT.
      *----------------------------------------------------------------*
      *    Controllo delle garanzie da includere
      *----------------------------------------------------------------*
       CTRL-GAR-INCLUSA.

           SET NON-TROVATO TO TRUE
           MOVE ZERO TO IX-RIC-GRZ

           PERFORM UNTIL IX-RIC-GRZ >= WPAG-ELE-MAX-GARNZIE
                      OR TROVATO

              ADD 1 TO IX-RIC-GRZ

              IF WPAG-COD-GARANZIA-I(IX-RIC-GRZ) = WS-COD-TARIFFA
                 SET TROVATO TO TRUE
              END-IF

           END-PERFORM.

       CTRL-GAR-INCLUSA-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RICERCA DELLA TRANCHE ASSOCIATA ALLA GARANZIA
      *----------------------------------------------------------------*
       S12111-AREA-TRANCHE.

           IF  VGRZ-ID-GAR(IX-TAB-GRZ) = VTGA-ID-GAR(IX-TAB-TGA)
           AND NOT VTGA-ST-DEL(IX-TAB-TGA)
           AND NOT VTGA-TP-TRCH(IX-TAB-TGA) = '9'
               SET WK-TGA-TROVATA      TO TRUE
               MOVE VTGA-MOD-CALC(IX-TAB-TGA)
                 TO ISPC0140-FLAG-MOD-CALC-PRE-T(IX-AREA-SCHEDA-T)
            END-IF.

       EX-S12111.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZAZIONE DELL'AREA VARIABILI DI TARIFFA
      *    DI INPUT DEL SERVIZIO CALCOLI E CONTROLLI
      *----------------------------------------------------------------*
       AREA-VAR-T-ISPC0140.

49032      ADD 1
49032       TO ISPC0140-NUM-COMPON-MAX-ELE-T(IX-AREA-SCHEDA-T)

           MOVE WSKD-COD-VARIABILE-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
             TO ISPC0140-CODICE-VARIABILE-T
                                    (IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
           MOVE WSKD-TP-DATO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
             TO ISPC0140-TIPO-DATO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)

           MOVE WSKD-VAL-GENERICO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T)
             TO ISPC0140-VAL-GENERICO-T(IX-AREA-SCHEDA-T, IX-TAB-VAR-T).

       AREA-VAR-T-ISPC0140-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVNUM4
      *   ULTIMO AGG. 02 SET 2008
      *------------------------------------------------------------

       INIZIA-TOT-NUM.

           PERFORM INIZIA-ZEROES-NUM THRU INIZIA-ZEROES-NUM-EX

           PERFORM INIZIA-SPACES-NUM THRU INIZIA-SPACES-NUM-EX

           PERFORM INIZIA-NULL-NUM THRU INIZIA-NULL-NUM-EX.

       INIZIA-TOT-NUM-EX.
           EXIT.

       INIZIA-NULL-NUM.
           MOVE HIGH-VALUES TO (SF)-ULT-NUM-LIN-NULL.
       INIZIA-NULL-NUM-EX.
           EXIT.

       INIZIA-ZEROES-NUM.
           MOVE 0 TO (SF)-COD-COMP-ANIA
           MOVE 0 TO (SF)-ID-OGG
           MOVE 0 TO (SF)-DS-VER
           MOVE 0 TO (SF)-DS-TS-CPTZ.
       INIZIA-ZEROES-NUM-EX.
           EXIT.

       INIZIA-SPACES-NUM.
           MOVE SPACES TO (SF)-TP-OGG
           MOVE SPACES TO (SF)-DS-OPER-SQL
           MOVE SPACES TO (SF)-DS-UTENTE
           MOVE SPACES TO (SF)-DS-STATO-ELAB.
       INIZIA-SPACES-NUM-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY      ..... LCCVP896
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO DETTAGLIO ATTIVAZIONE
      *                                  SERVIZI A VALORE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *  - 01 FL-RICERCA                    PIC X(002) VALUE SPACES.
      *       88 TROVATO                        VALUE 'SI'.
      *       88 NON-TROVATO                    VALUE 'NO'.
      *  - 01 WS-ID-ATT-SERV-VAL            PIC S9(9)  COMP-3.
      *  - 01 WS-ID-PTF                     PIC S9(9)  COMP-3.
      *
      *----------------------------------------------------------------*
       AGGIORNA-D-ATT-SERV-VAL.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE D-ATT-SERV-VAL
      *--> NOME TABELLA FISICA DB
           MOVE 'D-ATT-SERV-VAL'                TO WK-TABELLA
      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WP89-ST-INV(IX-TAB-P89)
              AND NOT WP89-ST-CON(IX-TAB-P89)
              AND WP89-ELE-DSERV-VAL-MAX NOT = 0

      *--->   Impostare ID Tabella RAPPORTO ANAGRAFICO
              MOVE WMOV-ID-PTF              TO P89-ID-MOVI-CRZ
              MOVE WP89-ID-ATT-SERV-VAL(IX-TAB-P89)
                                            TO WS-ID-ATT-SERV-VAL
      *--->   Ricerca dell'ID-ATT-SERV-VAL sulla Tabella
      *--->   Padre Attivazione Servizi a Valore
              PERFORM RICERCA-ID-ATT-SERV-VAL
                 THRU RICERCA-ID-ATT-SERV-VAL-EX

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WP89-ST-ADD(IX-TAB-P89)

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK

                        MOVE S090-SEQ-TABELLA
                          TO WP89-ID-PTF(IX-TAB-P89)
                             P89-ID-D-ATT-SERV-VAL
                        MOVE WS-ID-PTF
                          TO P89-ID-ATT-SERV-VAL


      *-->               PREPARAZIONE ED ESTRAZIONE OGGETTO PTF
                         PERFORM PREPARA-AREA-LCCS0234-DSVAL
                            THRU PREPARA-AREA-LCCS0234-DSVAL-EX
                         PERFORM CALL-LCCS0234
                            THRU CALL-LCCS0234-EX

                         IF IDSV0001-ESITO-OK
                            MOVE S234-ID-OGG-PTF-EOC
                              TO P89-ID-OGG
                         END-IF


      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
                     END-IF

      *-->        CANCELLAZIONE
                  WHEN WP89-ST-DEL(IX-TAB-P89)

                       MOVE WP89-ID-PTF(IX-TAB-P89)
                         TO P89-ID-D-ATT-SERV-VAL
                       MOVE WP89-ID-OGG(IX-TAB-P89)
                         TO P89-ID-OGG
                       MOVE WP89-ID-ATT-SERV-VAL(IX-TAB-P89)
                         TO P89-ID-ATT-SERV-VAL

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->        MODIFICA
                  WHEN WP89-ST-MOD(IX-TAB-P89)
                       MOVE WP89-ID-PTF(IX-TAB-P89)
                         TO P89-ID-D-ATT-SERV-VAL
                       MOVE WP89-ID-OGG(IX-TAB-P89)
                         TO P89-ID-OGG
                       MOVE WP89-ID-ATT-SERV-VAL(IX-TAB-P89)
                         TO P89-ID-ATT-SERV-VAL

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN DETTAGLIO ATTIVAZIONE SERVIZI A VALORE
                 PERFORM VAL-DCLGEN-P89
                    THRU VAL-DCLGEN-P89-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-P89
                    THRU VALORIZZA-AREA-DSH-P89-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-D-ATT-SERV-VAL-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-P89.

      *--> DCLGEN TABELLA
           MOVE D-ATT-SERV-VAL          TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-P89-EX.
           EXIT.
      *----------------------------------------------------------------*
      *   PREPARA PER LA CALL ALL'LCCS0234 ESTRATTORE OGGETTO PTF
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234-DSVAL.

           MOVE WP89-ID-OGG(IX-TAB-P89)     TO S234-ID-OGG-EOC
           MOVE WP89-TP-OGG(IX-TAB-P89)     TO S234-TIPO-OGG-EOC.

       PREPARA-AREA-LCCS0234-DSVAL-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RICERCA PTF
      *----------------------------------------------------------------*
       RICERCA-ID-ATT-SERV-VAL.

           SET NON-TROVATO TO TRUE.

           PERFORM VARYING IX-P88 FROM 1 BY 1
                     UNTIL IX-P88 > WP88-ELE-SERV-VAL-MAX OR TROVATO

                IF WP88-ID-ATT-SERV-VAL(IX-P88) = WS-ID-ATT-SERV-VAL

                   MOVE WP88-ID-PTF(IX-P88) TO WS-ID-PTF

                   SET TROVATO TO TRUE

                END-IF

           END-PERFORM.

       RICERCA-ID-ATT-SERV-VAL-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVP895.

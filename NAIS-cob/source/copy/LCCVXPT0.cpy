      ******************************************************************
      *    TP_PRE_TIT (Tipologia del premio del titolo contabile)
      ******************************************************************
       01  WS-TP-PRE-TIT                    PIC X(002) VALUE SPACES.
           88 TP-PERFEZ-EMIS                           VALUE 'PE'.
           88 TP-QUIETANZAMENTO                        VALUE 'QU'.
           88 TP-SUCCESSIVA-EMIS                       VALUE 'SE'.
           88 TP-VERSAM-AGGIUNTIVO                     VALUE 'VA'.
           88 TP-RECUPERO-PROVV                        VALUE 'RP'.
           88 TP-CONGUAGLIO-PROVV                      VALUE 'CP'.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVL415
      *   ULTIMO AGG. 22 APR 2010
      *------------------------------------------------------------

       VAL-DCLGEN-L41.
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-L41)
              TO L41-COD-COMP-ANIA
           MOVE (SF)-TP-QTZ(IX-TAB-L41)
              TO L41-TP-QTZ
           MOVE (SF)-COD-FND(IX-TAB-L41)
              TO L41-COD-FND
           MOVE (SF)-DT-QTZ(IX-TAB-L41)
              TO L41-DT-QTZ
           MOVE (SF)-TP-FND(IX-TAB-L41)
              TO L41-TP-FND
           MOVE (SF)-VAL-QUO(IX-TAB-L41)
              TO L41-VAL-QUO
           MOVE (SF)-DESC-AGG(IX-TAB-L41)
              TO L41-DESC-AGG
           MOVE (SF)-DS-OPER-SQL(IX-TAB-L41)
              TO L41-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-L41) NOT NUMERIC
              MOVE 0 TO L41-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-L41)
              TO L41-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ(IX-TAB-L41) NOT NUMERIC
              MOVE 0 TO L41-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ(IX-TAB-L41)
              TO L41-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-L41)
              TO L41-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-L41)
              TO L41-DS-STATO-ELAB.
       VAL-DCLGEN-L41-EX.
           EXIT.

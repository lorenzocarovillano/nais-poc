      *----------------------------------------------------------------*
      *    COPY      ..... LCCVL236
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO VINCOLO PEGNO
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVL235 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN VINCOLO PEGNO (LCCVL231)
      *
      *----------------------------------------------------------------*
       AGGIORNA-VINC-PEG.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE VINC-PEG

      *--> SE LO STATUS NON E' "INVARIATO" O "CONVERSAZIONE"
           IF  NOT WL23-ST-INV(IX-TAB-L23)
           AND NOT WL23-ST-CON(IX-TAB-L23)
           AND WL23-ELE-VINC-PEG-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WL23-ST-ADD(IX-TAB-L23)

      *-->             ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA
                            TO WL23-ID-PTF(IX-TAB-L23)
                               L23-ID-VINC-PEG
                       END-IF

                       MOVE WMOV-ID-PTF  TO L23-ID-MOVI-CRZ

                       MOVE WL23-ID-RAPP-ANA(IX-TAB-L23)
                         TO WS-ID-RAPP-ANA-L23

                       SET  NON-TROVATO              TO TRUE

                       PERFORM SEARCH-ID-RAP-ANA-L23
                          THRU SEARCH-ID-RAP-ANA-L23-EX
                       VARYING IX-TAB-RAN FROM 1 BY 1
                         UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX
                            OR TROVATO

                       IF TROVATO
                          MOVE WS-ID-RAPP-ANA
                            TO WL23-ID-RAPP-ANA(IX-TAB-L23)
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WL23-ST-MOD(IX-TAB-L23)

                       MOVE WL23-ID-VINC-PEG(IX-TAB-L23)
                         TO L23-ID-VINC-PEG

                       MOVE WMOV-ID-PTF  TO L23-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WL23-ST-DEL(IX-TAB-L23)

                       MOVE WL23-ID-VINC-PEG(IX-TAB-L23)
                         TO L23-ID-VINC-PEG

                       MOVE WMOV-ID-PTF  TO L23-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

      *-->    VALORIZZA DCLGEN ADESIONE
              PERFORM VAL-DCLGEN-L23
                 THRU VAL-DCLGEN-L23-EX

      *-->    VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
              PERFORM VALORIZZA-AREA-DSH-L23
                 THRU VALORIZZA-AREA-DSH-L23-EX

      *-->    CALL DISPATCHER PER AGGIORNAMENTO
              PERFORM AGGIORNA-TABELLA
                 THRU AGGIORNA-TABELLA-EX

           END-IF.

       AGGIORNA-VINC-PEG-EX.
           EXIT.

      *---------------------------------------------------------------*
      *    CICLO DI ELABORAZIONE SU DCLGEN RAPP-ANA
      *--------------------------------------------------------------*
       SEARCH-ID-RAP-ANA-L23.

           IF WS-ID-RAPP-ANA-L23 = WRAN-ID-RAPP-ANA(IX-TAB-RAN)

              SET TROVATO                    TO TRUE
              MOVE WRAN-ID-PTF(IX-TAB-RAN)   TO WS-ID-RAPP-ANA

           END-IF.

       SEARCH-ID-RAP-ANA-L23-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-L23.

      *--> NOME TABELLA FISICA DB
           MOVE 'VINC-PEG'              TO   WK-TABELLA.

      *--> DCLGEN TABELLA
           MOVE VINC-PEG                TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-L23-EX.
           EXIT.

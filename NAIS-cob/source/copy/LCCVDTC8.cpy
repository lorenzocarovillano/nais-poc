          02 (SF)-DTC.
             03 (SF)-LEN-TOT                       PIC S9(4) COMP-5.
             03 (SF)-REC-FISSO.
                05 (SF)-ID-DETT-TIT-CONT PIC S9(9)V     COMP-3.
                05 (SF)-ID-TIT-CONT PIC S9(9)V          COMP-3.
                05 (SF)-ID-OGG PIC S9(9)V               COMP-3.
                05 (SF)-TP-OGG PIC X(2).
                05 (SF)-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
                05 (SF)-ID-MOVI-CHIU-IND  PIC X.
                05 (SF)-ID-MOVI-CHIU PIC S9(9)V     COMP-3.
                05 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                   (SF)-ID-MOVI-CHIU   PIC X(5).
                05 (SF)-DT-INI-EFF   PIC X(10).
                05 (SF)-DT-END-EFF   PIC X(10).
                05 (SF)-COD-COMP-ANIA PIC S9(5)V         COMP-3.
                05 (SF)-DT-INI-COP-IND   PIC X.
                05 (SF)-DT-INI-COP   PIC X(10).
                05 (SF)-DT-INI-COP-NULL REDEFINES
                   (SF)-DT-INI-COP   PIC X(10).
                05 (SF)-DT-END-COP-IND   PIC X.
                05 (SF)-DT-END-COP   PIC X(10).
                05 (SF)-DT-END-COP-NULL REDEFINES
                   (SF)-DT-END-COP   PIC X(10).
                05 (SF)-PRE-NET-IND PIC X.
                05 (SF)-PRE-NET PIC S9(12)V9(3) COMP-3.
                05 (SF)-PRE-NET-NULL REDEFINES
                   (SF)-PRE-NET   PIC X(8).
                05 (SF)-INTR-FRAZ-IND PIC X.
                05 (SF)-INTR-FRAZ PIC S9(12)V9(3) COMP-3.
                05 (SF)-INTR-FRAZ-NULL REDEFINES
                   (SF)-INTR-FRAZ   PIC X(8).
                05 (SF)-INTR-MORA-IND PIC X.
                05 (SF)-INTR-MORA PIC S9(12)V9(3) COMP-3.
                05 (SF)-INTR-MORA-NULL REDEFINES
                   (SF)-INTR-MORA   PIC X(8).
                05 (SF)-INTR-RETDT-IND PIC X.
                05 (SF)-INTR-RETDT PIC S9(12)V9(3) COMP-3.
                05 (SF)-INTR-RETDT-NULL REDEFINES
                   (SF)-INTR-RETDT   PIC X(8).
                05 (SF)-INTR-RIAT-IND PIC X.
                05 (SF)-INTR-RIAT PIC S9(12)V9(3) COMP-3.
                05 (SF)-INTR-RIAT-NULL REDEFINES
                   (SF)-INTR-RIAT   PIC X(8).
                05 (SF)-DIR-IND PIC X.
                05 (SF)-DIR PIC S9(12)V9(3) COMP-3.
                05 (SF)-DIR-NULL REDEFINES
                   (SF)-DIR   PIC X(8).
                05 (SF)-SPE-MED-IND PIC X.
                05 (SF)-SPE-MED PIC S9(12)V9(3) COMP-3.
                05 (SF)-SPE-MED-NULL REDEFINES
                   (SF)-SPE-MED   PIC X(8).
                05 (SF)-TAX-IND PIC X.
                05 (SF)-TAX PIC S9(12)V9(3) COMP-3.
                05 (SF)-TAX-NULL REDEFINES
                   (SF)-TAX   PIC X(8).
                05 (SF)-SOPR-SAN-IND PIC X.
                05 (SF)-SOPR-SAN PIC S9(12)V9(3) COMP-3.
                05 (SF)-SOPR-SAN-NULL REDEFINES
                   (SF)-SOPR-SAN   PIC X(8).
                05 (SF)-SOPR-SPO-IND PIC X.
                05 (SF)-SOPR-SPO PIC S9(12)V9(3) COMP-3.
                05 (SF)-SOPR-SPO-NULL REDEFINES
                   (SF)-SOPR-SPO   PIC X(8).
                05 (SF)-SOPR-TEC-IND PIC X.
                05 (SF)-SOPR-TEC PIC S9(12)V9(3) COMP-3.
                05 (SF)-SOPR-TEC-NULL REDEFINES
                   (SF)-SOPR-TEC   PIC X(8).
                05 (SF)-SOPR-PROF-IND PIC X.
                05 (SF)-SOPR-PROF PIC S9(12)V9(3) COMP-3.
                05 (SF)-SOPR-PROF-NULL REDEFINES
                   (SF)-SOPR-PROF   PIC X(8).
                05 (SF)-SOPR-ALT-IND PIC X.
                05 (SF)-SOPR-ALT PIC S9(12)V9(3) COMP-3.
                05 (SF)-SOPR-ALT-NULL REDEFINES
                   (SF)-SOPR-ALT   PIC X(8).
                05 (SF)-PRE-TOT-IND PIC X.
                05 (SF)-PRE-TOT PIC S9(12)V9(3) COMP-3.
                05 (SF)-PRE-TOT-NULL REDEFINES
                   (SF)-PRE-TOT   PIC X(8).
                05 (SF)-PRE-PP-IAS-IND PIC X.
                05 (SF)-PRE-PP-IAS PIC S9(12)V9(3) COMP-3.
                05 (SF)-PRE-PP-IAS-NULL REDEFINES
                   (SF)-PRE-PP-IAS   PIC X(8).
                05 (SF)-PRE-SOLO-RSH-IND PIC X.
                05 (SF)-PRE-SOLO-RSH PIC S9(12)V9(3) COMP-3.
                05 (SF)-PRE-SOLO-RSH-NULL REDEFINES
                   (SF)-PRE-SOLO-RSH   PIC X(8).
                05 (SF)-CAR-ACQ-IND PIC X.
                05 (SF)-CAR-ACQ PIC S9(12)V9(3) COMP-3.
                05 (SF)-CAR-ACQ-NULL REDEFINES
                   (SF)-CAR-ACQ   PIC X(8).
                05 (SF)-CAR-GEST-IND PIC X.
                05 (SF)-CAR-GEST PIC S9(12)V9(3) COMP-3.
                05 (SF)-CAR-GEST-NULL REDEFINES
                   (SF)-CAR-GEST   PIC X(8).
                05 (SF)-CAR-INC-IND PIC X.
                05 (SF)-CAR-INC PIC S9(12)V9(3) COMP-3.
                05 (SF)-CAR-INC-NULL REDEFINES
                   (SF)-CAR-INC   PIC X(8).
                05 (SF)-PROV-ACQ-1AA-IND PIC X.
                05 (SF)-PROV-ACQ-1AA PIC S9(12)V9(3) COMP-3.
                05 (SF)-PROV-ACQ-1AA-NULL REDEFINES
                   (SF)-PROV-ACQ-1AA   PIC X(8).
                05 (SF)-PROV-ACQ-2AA-IND PIC X.
                05 (SF)-PROV-ACQ-2AA PIC S9(12)V9(3) COMP-3.
                05 (SF)-PROV-ACQ-2AA-NULL REDEFINES
                   (SF)-PROV-ACQ-2AA   PIC X(8).
                05 (SF)-PROV-RICOR-IND PIC X.
                05 (SF)-PROV-RICOR PIC S9(12)V9(3) COMP-3.
                05 (SF)-PROV-RICOR-NULL REDEFINES
                   (SF)-PROV-RICOR   PIC X(8).
                05 (SF)-PROV-INC-IND PIC X.
                05 (SF)-PROV-INC PIC S9(12)V9(3) COMP-3.
                05 (SF)-PROV-INC-NULL REDEFINES
                   (SF)-PROV-INC   PIC X(8).
                05 (SF)-PROV-DA-REC-IND PIC X.
                05 (SF)-PROV-DA-REC PIC S9(12)V9(3) COMP-3.
                05 (SF)-PROV-DA-REC-NULL REDEFINES
                   (SF)-PROV-DA-REC   PIC X(8).
                05 (SF)-COD-DVS-IND PIC X.
                05 (SF)-COD-DVS PIC X(20).
                05 (SF)-COD-DVS-NULL REDEFINES
                   (SF)-COD-DVS   PIC X(20).
                05 (SF)-FRQ-MOVI-IND PIC X.
                05 (SF)-FRQ-MOVI PIC S9(5)V     COMP-3.
                05 (SF)-FRQ-MOVI-NULL REDEFINES
                   (SF)-FRQ-MOVI   PIC X(3).
                05 (SF)-TP-RGM-FISC PIC X(2).
                05 (SF)-COD-TARI-IND PIC X.
                05 (SF)-COD-TARI PIC X(12).
                05 (SF)-COD-TARI-NULL REDEFINES
                   (SF)-COD-TARI   PIC X(12).
                05 (SF)-TP-STAT-TIT PIC X(2).
                05 (SF)-IMP-AZ-IND PIC X.
                05 (SF)-IMP-AZ PIC S9(12)V9(3) COMP-3.
                05 (SF)-IMP-AZ-NULL REDEFINES
                   (SF)-IMP-AZ   PIC X(8).
                05 (SF)-IMP-ADER-IND PIC X.
                05 (SF)-IMP-ADER PIC S9(12)V9(3) COMP-3.
                05 (SF)-IMP-ADER-NULL REDEFINES
                   (SF)-IMP-ADER   PIC X(8).
                05 (SF)-IMP-TFR-IND PIC X.
                05 (SF)-IMP-TFR PIC S9(12)V9(3) COMP-3.
                05 (SF)-IMP-TFR-NULL REDEFINES
                   (SF)-IMP-TFR   PIC X(8).
                05 (SF)-IMP-VOLO-IND PIC X.
                05 (SF)-IMP-VOLO PIC S9(12)V9(3) COMP-3.
                05 (SF)-IMP-VOLO-NULL REDEFINES
                   (SF)-IMP-VOLO   PIC X(8).
                05 (SF)-MANFEE-ANTIC-IND PIC X.
                05 (SF)-MANFEE-ANTIC PIC S9(12)V9(3) COMP-3.
                05 (SF)-MANFEE-ANTIC-NULL REDEFINES
                   (SF)-MANFEE-ANTIC   PIC X(8).
                05 (SF)-MANFEE-RICOR-IND PIC X.
                05 (SF)-MANFEE-RICOR PIC S9(12)V9(3) COMP-3.
                05 (SF)-MANFEE-RICOR-NULL REDEFINES
                   (SF)-MANFEE-RICOR   PIC X(8).
                05 (SF)-MANFEE-REC-IND PIC X.
                05 (SF)-MANFEE-REC PIC S9(12)V9(3) COMP-3.
                05 (SF)-MANFEE-REC-NULL REDEFINES
                   (SF)-MANFEE-REC   PIC X(8).
                05 (SF)-DT-ESI-TIT-IND PIC X.
                05 (SF)-DT-ESI-TIT   PIC X(10).
                05 (SF)-DT-ESI-TIT-NULL REDEFINES
                   (SF)-DT-ESI-TIT   PIC X(10).
                05 (SF)-SPE-AGE-IND PIC X.
                05 (SF)-SPE-AGE PIC S9(12)V9(3) COMP-3.
                05 (SF)-SPE-AGE-NULL REDEFINES
                   (SF)-SPE-AGE   PIC X(8).
                05 (SF)-CAR-IAS-IND PIC X.
                05 (SF)-CAR-IAS PIC S9(12)V9(3) COMP-3.
                05 (SF)-CAR-IAS-NULL REDEFINES
                   (SF)-CAR-IAS   PIC X(8).
                05 (SF)-TOT-INTR-PREST-IND PIC X.
                05 (SF)-TOT-INTR-PREST PIC S9(12)V9(3) COMP-3.
                05 (SF)-TOT-INTR-PREST-NULL REDEFINES
                   (SF)-TOT-INTR-PREST   PIC X(8).
                05 (SF)-DS-RIGA PIC S9(10)V     COMP-3.
                05 (SF)-DS-OPER-SQL PIC X(1).
                05 (SF)-DS-VER PIC S9(9)V     COMP-3.
                05 (SF)-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
                05 (SF)-DS-TS-END-CPTZ PIC S9(18)V     COMP-3.
                05 (SF)-DS-UTENTE PIC X(20).
                05 (SF)-DS-STATO-ELAB PIC X(1).
                05 (SF)-IMP-TRASFE-IND PIC X.
                05 (SF)-IMP-TRASFE PIC S9(12)V9(3) COMP-3.
                05 (SF)-IMP-TRASFE-NULL REDEFINES
                   (SF)-IMP-TRASFE   PIC X(8).
                05 (SF)-IMP-TFR-STRC-IND PIC X.
                05 (SF)-IMP-TFR-STRC PIC S9(12)V9(3) COMP-3.
                05 (SF)-IMP-TFR-STRC-NULL REDEFINES
                   (SF)-IMP-TFR-STRC   PIC X(8).
                05 (SF)-NUM-GG-RITARDO-PAG-IND PIC X.
                05 (SF)-NUM-GG-RITARDO-PAG PIC S9(5)     COMP-3.
                05 (SF)-NUM-GG-RITARDO-PAG-NULL REDEFINES
                   (SF)-NUM-GG-RITARDO-PAG   PIC X(3).
                05 (SF)-NUM-GG-RIVAL-IND PIC X.
                05 (SF)-NUM-GG-RIVAL PIC S9(5)     COMP-3.
                05 (SF)-NUM-GG-RIVAL-NULL REDEFINES
                   (SF)-NUM-GG-RIVAL   PIC X(3).
          02 (SF)-POL.
             03 (SF)-ID-POLI        PIC S9(9)V     COMP-3.

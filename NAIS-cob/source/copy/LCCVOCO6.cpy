      *----------------------------------------------------------------*
      *    COPY      ..... LCCVOCO6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO OGGETTO COLLEGATO
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVOCO5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCVOCO1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-OGG-COLLG.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE OGG-COLLG

      *--> NOME TABELLA FISICA DB
           MOVE 'OGG-COLLG'                 TO   WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WOCO-ST-INV(IX-TAB-OCO)
              AND WOCO-ELE-OCO-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WOCO-ST-ADD(IX-TAB-OCO)

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK
                        MOVE S090-SEQ-TABELLA
                          TO WOCO-ID-PTF(IX-TAB-OCO)
                             OCO-ID-OGG-COLLG
                        IF WOCO-TP-OGG-COINV(IX-TAB-OCO) = 'PO'
                           MOVE WPOL-ID-PTF       TO OCO-ID-OGG-COINV
                        ELSE
                           MOVE WADE-ID-PTF       TO OCO-ID-OGG-COINV
                        END-IF
                        MOVE WMOV-ID-PTF          TO OCO-ID-MOVI-CRZ
                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WOCO-ST-MOD(IX-TAB-OCO)

                       MOVE WOCO-ID-PTF(IX-TAB-OCO) TO OCO-ID-OGG-COLLG
                       IF WOCO-TP-OGG-COINV(IX-TAB-OCO) = 'PO'
                          MOVE WPOL-ID-PTF          TO OCO-ID-OGG-COINV
                       ELSE
                          MOVE WADE-ID-PTF          TO OCO-ID-OGG-COINV
                       END-IF
                       MOVE WMOV-ID-PTF             TO OCO-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WOCO-ST-DEL(IX-TAB-OCO)

                       MOVE WOCO-ID-PTF(IX-TAB-OCO) TO OCO-ID-OGG-COLLG
                       IF WOCO-TP-OGG-COINV(IX-TAB-OCO) = 'PO'
                          MOVE WPOL-ID-PTF          TO OCO-ID-OGG-COINV
                       ELSE
                          MOVE WADE-ID-PTF          TO OCO-ID-OGG-COINV
                       END-IF
                       MOVE WMOV-ID-PTF             TO OCO-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN ADESIONE
                 PERFORM VAL-DCLGEN-OCO
                    THRU VAL-DCLGEN-OCO-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-OCO
                    THRU VALORIZZA-AREA-DSH-OCO-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-OGG-COLLG-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-OCO.

      *--> DCLGEN TABELLA
           MOVE OGG-COLLG               TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-OCO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVOCO5.

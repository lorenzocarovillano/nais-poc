      *------------------------------------------------------------*
      *   PORTAFOGLIO VITA - PROCESSO VENDITA                      *
      *   CHIAMATA AL DISPATCHER DETT_QUEST                        *
      *   ULTIMO AGG.                                              *
      *------------------------------------------------------------*
       LETTURA-DEQ.

           IF IDSI0011-SELECT
              PERFORM SELECT-DEQ
                 THRU SELECT-DEQ-EX
           ELSE
              PERFORM FETCH-DEQ
                 THRU FETCH-DEQ-EX
           END-IF.

       LETTURA-DEQ-EX.
           EXIT.
      *----------------------------------------------------------------*
      *                         SELECT                                 *
      *----------------------------------------------------------------*
       SELECT-DEQ.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
      *-->    GESTIRE ERRORE DB
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI
                       TO DETT-QUEST
                     MOVE 1       TO IX-TAB-DEQ
                     PERFORM VALORIZZA-OUTPUT-DEQ
                        THRU VALORIZZA-OUTPUT-DEQ-EX

                  WHEN IDSO0011-NOT-FOUND
      *----- CHIAVE NON TROVATA
                     MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'SELECT-DEQ'      TO IEAI9901-LABEL-ERR
                     MOVE '005017'          TO IEAI9901-COD-ERRORE
                     MOVE 'DEQ-ID-PTF'      TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300

                  WHEN OTHER
      *------  ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'SELECT-DEQ'      TO IEAI9901-LABEL-ERR
                     MOVE '005016'          TO IEAI9901-COD-ERRORE
                     STRING WK-TABELLA         ';'
                          IDSO0011-RETURN-CODE ';'
                          IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SELECT-DEQ'             TO IEAI9901-LABEL-ERR
              MOVE '005016'                 TO IEAI9901-COD-ERRORE
              STRING WK-TABELLA           ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       SELECT-DEQ-EX.
           EXIT.
      *----------------------------------------------------------------*
      *                         FETCH                                  *
      *----------------------------------------------------------------*
       FETCH-DEQ.

           SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
           SET  WCOM-OVERFLOW-NO                  TO TRUE.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR WCOM-OVERFLOW-YES

               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *-->                 CAMPO $ NON TROVATO
                           IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM
                                TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'FETCH-DEQ'
                                TO IEAI9901-LABEL-ERR
                              MOVE '005019'
                                TO IEAI9901-COD-ERRORE
                              MOVE 'ID-PTF'
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                           END-IF
                      WHEN IDSO0011-SUCCESSFUL-SQL
      *-->                 OPERAZIONE ESEGUITA CORRETTAMENTE
                           MOVE IDSO0011-BUFFER-DATI  TO DETT-QUEST
                           ADD  1                     TO IX-TAB-DEQ

      *-->  Se il contatore di lettura � maggiore dell' elemento MAX
      *-->  previsto per la TAB.DETT-QUEST allora siamo in overflow
                           IF IX-TAB-DEQ > (SF)-ELE-DEQ-MAX
                              SET WCOM-OVERFLOW-YES   TO TRUE
                           ELSE
                              PERFORM VALORIZZA-OUTPUT-DEQ
                                 THRU VALORIZZA-OUTPUT-DEQ-EX
                              SET IDSI0011-FETCH-NEXT TO TRUE
                           END-IF
                      WHEN OTHER
      *-->                 ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'FETCH-DEQ'
                             TO IEAI9901-LABEL-ERR
                           MOVE '005015'
                             TO IEAI9901-COD-ERRORE
                           MOVE WK-TABELLA
                             TO IEAI9901-PARAMETRI-ERR
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      *-->        ERRORE DISPATCHER
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'FETCH-DEQ'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005016'
                    TO IEAI9901-COD-ERRORE
                  MOVE SPACES
                    TO IEAI9901-PARAMETRI-ERR
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

       FETCH-DEQ-EX.
           EXIT.

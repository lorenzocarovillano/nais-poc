           EXEC SQL DECLARE ANAG_DATO TABLE
           (
             COD_COMPAGNIA_ANIA  DECIMAL(5, 0) NOT NULL,
             COD_DATO            CHAR(30) NOT NULL,
             DESC_DATO           VARCHAR(250),
             TIPO_DATO           CHAR(2),
             LUNGHEZZA_DATO      DECIMAL(5, 0),
             PRECISIONE_DATO     DECIMAL(2, 0),
             COD_DOMINIO         CHAR(30),
             FORMATTAZIONE_DATO  CHAR(20),
             DS_UTENTE           CHAR(20) NOT NULL WITH DEFAULT
          ) END-EXEC.

      *----------------------------------------------------------------*
      *   AREA INPUT SERVIZIO VERIFICA PRESENZA BLOCCHI
      *   PORTAFOGLIO VITA - PROCESSO VENDITA
      *   LUNG.
      *----------------------------------------------------------------*
           05 LCCC0022-DATI-INPUT.
              07 LCCC0022-ID-POLI                     PIC S9(9)  COMP-3.
              07 LCCC0022-ID-ADES                     PIC S9(9)  COMP-3.
              07 LCCC0022-TP-OGG                      PIC X(2).
              07 LCCC0022-TP-STATO-BLOCCO             PIC X(2).
                 88 LCCC0022-BLOCCO-ATTIVO            VALUE 'AT'.
                 88 LCCC0022-BLOCCO-NON-ATTIVO        VALUE 'NA'.

           05 LCCC0022-DATI-OUTPUT.
              07 LCCC0022-FL-PRES-GRAV-BLOC           PIC X(1).
              07 LCCC0022-ELE-MAX-OGG-BLOC            PIC S9(04) COMP.
              07 LCCC0022-TAB-OGG-BLOCCO         OCCURS 30.
                 10 LCCC0022-OGB-ID-OGG-BLOCCO        PIC S9(9)  COMP-3.
                 10 LCCC0022-OGB-ID-OGG-1RIO          PIC S9(9)  COMP-3.
                 10 LCCC0022-OGB-TP-OGG-1RIO          PIC X(2).
                 10 LCCC0022-OGB-COD-COMP-ANIA        PIC S9(5)  COMP-3.
                 10 LCCC0022-OGB-TP-MOVI              PIC S9(5)  COMP-3.
                 10 LCCC0022-OGB-TP-OGG               PIC X(2).
                 10 LCCC0022-OGB-ID-OGG               PIC S9(9)  COMP-3.
                 10 LCCC0022-OGB-DT-EFFETTO           PIC S9(8)  COMP-3.
                 10 LCCC0022-OGB-TP-STAT-BLOCCO       PIC X(2).
                 10 LCCC0022-OGB-COD-BLOCCO           PIC X(5).
                 10 LCCC0022-OGB-DESCRIZIONE          PIC X(100).
                 10 LCCC0022-OGB-GRAVITA              PIC X(1).


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVSTW3
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-STW.
           MOVE STW-ID-STAT-OGG-WF
             TO (SF)-ID-PTF(IX-TAB-STW)
           MOVE STW-ID-STAT-OGG-WF
             TO (SF)-ID-STAT-OGG-WF(IX-TAB-STW)
           MOVE STW-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-STW)
           MOVE STW-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-STW)
           MOVE STW-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-STW)
           IF STW-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE STW-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-STW)
           ELSE
              MOVE STW-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-STW)
           END-IF
           MOVE STW-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-STW)
           MOVE STW-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-STW)
           MOVE STW-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-STW)
           MOVE STW-COD-PRCS
             TO (SF)-COD-PRCS(IX-TAB-STW)
           MOVE STW-COD-ATTVT
             TO (SF)-COD-ATTVT(IX-TAB-STW)
           MOVE STW-STAT-OGG-WF
             TO (SF)-STAT-OGG-WF(IX-TAB-STW)
           IF STW-FL-STAT-END-NULL = HIGH-VALUES
              MOVE STW-FL-STAT-END-NULL
                TO (SF)-FL-STAT-END-NULL(IX-TAB-STW)
           ELSE
              MOVE STW-FL-STAT-END
                TO (SF)-FL-STAT-END(IX-TAB-STW)
           END-IF
           MOVE STW-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-STW)
           MOVE STW-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-STW)
           MOVE STW-DS-VER
             TO (SF)-DS-VER(IX-TAB-STW)
           MOVE STW-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-STW)
           MOVE STW-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-STW)
           MOVE STW-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-STW)
           MOVE STW-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-STW).
       VALORIZZA-OUTPUT-STW-EX.
           EXIT.

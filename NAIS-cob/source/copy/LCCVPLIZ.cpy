      *----------------------------------------------------------------*
      * DIMENSIONAMENTO OCCURS PER LA TAB. PERC_LIQ                    *
      * COPY RIPORTANTE IL NUMERO DI OCCURS DA UTILIZZARE              *
      * PER INIZIALIZZA TOT DELLA TABELLA                              *
      *----------------------------------------------------------------*
       01 WK-PLI-MAX.
          03 WK-PLI-MAX-A                 PIC 9(04) VALUE 30.

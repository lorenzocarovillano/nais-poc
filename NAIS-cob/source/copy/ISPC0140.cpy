      *----------------------------------------------------------------*
      *   SERVIZIO CALCOLI E CONTROLLI
      *----------------------------------------------------------------*
      *   DATI INPUT
      *----------------------------------------------------------------*
         02 ISPC0140-DATI-INPUT.
            05 ISPC0140-COD-COMPAGNIA                PIC 9(005).
            05 ISPC0140-COD-PRODOTTO                 PIC X(012).
            05 ISPC0140-COD-CONVENZIONE              PIC X(012).
            05 ISPC0140-DATA-INIZ-VALID-CONV         PIC X(008).
            05 ISPC0140-FUNZIONALITA                 PIC 9(005).
            05 ISPC0140-DATA-RIFERIMENTO             PIC X(008).
            05 ISPC0140-LIVELLO-UTENTE               PIC 9(002).
            05 ISPC0140-SESSION-ID                   PIC X(020).
            05 ISPC0140-DATA-DECORR-POLIZZA          PIC X(008).
            05 ISPC0140-MODALITA-CALCOLO             PIC X(001).
            05 ISPC0140-RICALC-QUIET                 PIC X(001).
               88 ISPC0140-RICALC-QUIET-SI             VALUE 'S'.
               88 ISPC0140-RICALC-QUIET-NO             VALUE 'N'.

            05 ISPC0140-DEE                          PIC X(008).
      *----------------------------------------------------------------*
      *   SCHEDE PRODOTTO
      *----------------------------------------------------------------*
            05 ISPC0140-ELE-MAX-SCHEDA-P            PIC 9(04).
            05 ISPC0140-SCHEDA-P                OCCURS  1.
               07 ISPC0140-TIPO-LIVELLO-P            PIC X(001).
               07 ISPC0140-CODICE-LIVELLO-P          PIC X(012).
               07 ISPC0140-FLAG-MOD-CALC-PRE-P       PIC X(001).
                  88 ISPC0140-PREST-INIZ-P             VALUE '1'.
                  88 ISPC0140-PREMIO-LORDO-P           VALUE '2'.
                  88 ISPC0140-PREMIO-RATA-P            VALUE '3'.
                  88 ISPC0140-PREMIO-NETTO-P           VALUE '4'.
               07 ISPC0140-NUM-COMPON-MAX-ELE-P      PIC 9(003).
               07 ISPC0140-AREA-VARIABILI-P     OCCURS 100.
                  10 ISPC0140-CODICE-VARIABILE-P     PIC X(012).
                  10 ISPC0140-TIPO-DATO-P            PIC X(001).
                  10 ISPC0140-VAL-GENERICO-P         PIC  X(60).
      *----------------------------------------------------------------*
      *   REDEFINES VALORE GENERICO
      *----------------------------------------------------------------*
                  10 ISPC0140-VAL-IMP-GEN-P          REDEFINES
                     ISPC0140-VAL-GENERICO-P.
                     15 ISPC0140-VALORE-IMP-P        PIC S9(11)V9(7).
                     15 ISPC0140-VAL-IMP-FILLER-P    PIC X(42).

                  10 ISPC0140-VAL-PERC-GEN-P         REDEFINES
                     ISPC0140-VAL-GENERICO-P.
                     15 ISPC0140-VALORE-PERC-P       PIC  9(5)V9(9).
                     15 ISPC0140-VAL-PERC-FILLER-P   PIC X(46).

                  10 ISPC0140-VAL-STR-GEN-P          REDEFINES
                     ISPC0140-VAL-GENERICO-P.
                     15 ISPC0140-VALORE-STR-P        PIC X(60).

      *----------------------------------------------------------------*
      *   SCHEDE TRANCHE
      *----------------------------------------------------------------*
            05 ISPC0140-ELE-MAX-SCHEDA-T            PIC 9(04).
CPI         05 ISPC0140-SCHEDA-T                OCCURS  20.
               07 ISPC0140-TIPO-LIVELLO-T            PIC X(001).
               07 ISPC0140-CODICE-LIVELLO-T          PIC X(012).
               07 ISPC0140-GARANZIA-INCLUSA-T        PIC X(001).
               07 ISPC0140-TIPO-PREMIO-T             PIC X(001).
               07 ISPC0140-TIPO-TRCH                 PIC 9(002).
               07 ISPC0140-FLG-ITN                   PIC 9(001).
                  88 ISPC0211-FLG-ITN-POS            VALUE 0.
                  88 ISPC0211-FLG-ITN-NEG            VALUE 1.

               07 ISPC0140-FLAG-MOD-CALC-PRE-T       PIC X(001).
                  88 ISPC0140-PREST-INIZ-T             VALUE '1'.
                  88 ISPC0140-PREMIO-LORDO-T           VALUE '2'.
                  88 ISPC0140-PREMIO-RATA-T            VALUE '3'.
                  88 ISPC0140-PREMIO-NETTO-T           VALUE '4'.
               07 ISPC0140-PERC-RIP-PREMIO-T         PIC S9(3)V9(003).
               07 ISPC0140-NUM-COMPON-MAX-ELE-T      PIC 9(003).
               07 ISPC0140-AREA-VARIABILI-T     OCCURS 75.
                  10 ISPC0140-CODICE-VARIABILE-T     PIC X(012).
                  10 ISPC0140-TIPO-DATO-T            PIC X(001).
                  10 ISPC0140-VAL-GENERICO-T         PIC  X(60).
      *----------------------------------------------------------------*
      *   REDEFINES VALORE GENERICO
      *----------------------------------------------------------------*
                  10 ISPC0140-VAL-IMP-GEN-T          REDEFINES
                     ISPC0140-VAL-GENERICO-T.
                     15 ISPC0140-VALORE-IMP-T        PIC S9(11)V9(7).
                     15 ISPC0140-VAL-IMP-FILLER-T    PIC X(42).

                  10 ISPC0140-VAL-PERC-GEN-T         REDEFINES
                     ISPC0140-VAL-GENERICO-T.
                     15 ISPC0140-VALORE-PERC-T       PIC  9(5)V9(9).
                     15 ISPC0140-VAL-PERC-FILLER-T   PIC X(46).

                  10 ISPC0140-VAL-STR-GEN-T          REDEFINES
                     ISPC0140-VAL-GENERICO-T.
                     15 ISPC0140-VALORE-STR-T        PIC X(60).

      *----------------------------------------------------------------*
      *   DATI OUTPUT
      *----------------------------------------------------------------*
      *----------------------------------------------------------------*
      *   SCHEDE PRODOTTO
      *----------------------------------------------------------------*
         02 ISPC0140-DATI-OUTPUT.
            05 ISPC0140-DATI-NUM-MAX-ELE-P           PIC 9(003).
            05 ISPC0140-CALCOLI-P               OCCURS 1.
               07 ISPC0140-O-TIPO-LIVELLO-P          PIC X(001).
               07 ISPC0140-O-CODICE-LIVELLO-P        PIC X(012).
               07 ISPC0140-PRE-NUM-MAX-ELE-P         PIC 9(003).
               07 ISPC0140-AREA-TP-PREMIO-P    OCCURS 3.
                  10 ISPC0140-CODICE-TP-PREMIO-P     PIC X(010).
                     88 ISPC0140-ANNUO-P               VALUE 'ANNUO'.
                     88 ISPC0140-RATA-P                VALUE 'RATA'.
                     88 ISPC0140-FIRMA-P               VALUE 'FIRMA'.
                  10 ISPC0140-O-NUM-COMP-MAX-ELE-P   PIC 9(003).
                  10 ISPC0140-COMPONENTE-P      OCCURS 100.
                     15 ISPC0140-CODICE-COMPONENTE-P PIC X(012).
                     15 ISPC0140-DESCR-COMPONENTE-P  PIC X(030).
                     15 ISPC0140-COMP-TIPO-DATO-P    PIC X(001).
                     15 ISPC0140-COMP-VAL-GENERICO-P    PIC  X(60).
      *----------------------------------------------------------------*
      *   REDEFINES VALORE GENERICO
      *----------------------------------------------------------------*
                     15 ISPC0140-COMP-VAL-IMP-GEN-P     REDEFINES
                        ISPC0140-COMP-VAL-GENERICO-P.
                        17 ISPC0140-COMP-VALORE-IMP-P   PIC S9(11)V9(7).
                        17 ISPC0140-VAL-IMP-FILLER-P    PIC X(42).

                     15 ISPC0140-COMP-VAL-PERC-GEN-P    REDEFINES
                        ISPC0140-COMP-VAL-GENERICO-P.
                        17 ISPC0140-COMP-VALORE-PERC-P  PIC  9(5)V9(9).
                        17 ISPC0140-VAL-PERC-FILLER-P   PIC X(46).

                     15 ISPC0140-COMP-VAL-STR-GEN-P     REDEFINES
                        ISPC0140-COMP-VAL-GENERICO-P.
                        17 ISPC0140-COMP-VALORE-STR-P   PIC X(60).

                     15 ISPC0140-TRATTAMENTO-PROVV-P PIC X(001).
                        88 ISPC0140-TR-PROVV-AMM-MODIF-P  VALUE '1'.
                        88 ISPC0140-TR-PROVV-AMM-PROTE-P  VALUE '2'.
                        88 ISPC0140-TR-PROVV-NON-AMM-P    VALUE '3'.
                        88 ISPC0140-TR-PROVV-OBBLIG-P     VALUE '4'.

      *----------------------------------------------------------------*
      *   SCHEDE TRANCHE
      *----------------------------------------------------------------*
            05 ISPC0140-DATI-NUM-MAX-ELE-T           PIC 9(003).
            05 ISPC0140-CALCOLI-T               OCCURS 20.
               07 ISPC0140-O-TIPO-LIVELLO-T          PIC X(001).
               07 ISPC0140-O-CODICE-LIVELLO-T        PIC X(012).
               07 ISPC0140-PRE-NUM-MAX-ELE-T         PIC 9(003).
               07 ISPC0140-AREA-TP-PREMIO-T    OCCURS 3.
                  10 ISPC0140-CODICE-TP-PREMIO-T     PIC X(010).
                     88 ISPC0140-ANNUO-T               VALUE 'ANNUO'.
                     88 ISPC0140-RATA-T                VALUE 'RATA'.
                     88 ISPC0140-FIRMA-T               VALUE 'FIRMA'.
                  10 ISPC0140-O-NUM-COMP-MAX-ELE-T   PIC 9(003).
                  10 ISPC0140-COMPONENTE-T      OCCURS 75.
                     15 ISPC0140-CODICE-COMPONENTE-T PIC X(012).
                     15 ISPC0140-DESCR-COMPONENTE-T  PIC X(030).
                     15 ISPC0140-COMP-TIPO-DATO-T    PIC X(001).
                     15 ISPC0140-COMP-VAL-GENERICO-T    PIC  X(60).
      *----------------------------------------------------------------*
      *   REDEFINES VALORE GENERICO
      *----------------------------------------------------------------*
                     15 ISPC0140-COMP-VAL-IMP-GEN-T     REDEFINES
                        ISPC0140-COMP-VAL-GENERICO-T.
                        17 ISPC0140-COMP-VALORE-IMP-T   PIC S9(11)V9(7).
                        17 ISPC0140-VAL-IMP-FILLER-T    PIC X(42).

                     15 ISPC0140-COMP-VAL-PERC-GEN-T    REDEFINES
                        ISPC0140-COMP-VAL-GENERICO-T.
                        17 ISPC0140-COMP-VALORE-PERC-T  PIC  9(5)V9(9).
                        17 ISPC0140-VAL-PERC-FILLER-T   PIC X(46).

                     15 ISPC0140-COMP-VAL-STR-GEN-T     REDEFINES
                        ISPC0140-COMP-VAL-GENERICO-T.
                        17 ISPC0140-COMP-VALORE-STR-T   PIC X(60).

                     15 ISPC0140-TRATTAMENTO-PROVV-T PIC X(001).
                        88 ISPC0140-TR-PROVV-AMM-MODIF-T  VALUE '1'.
                        88 ISPC0140-TR-PROVV-AMM-PROTE-T  VALUE '2'.
                        88 ISPC0140-TR-PROVV-NON-AMM-T    VALUE '3'.
                        88 ISPC0140-TR-PROVV-OBBLIG-T     VALUE '4'.

      *----------------------------------------------------------------*
      *   AREA ERRORI
      *----------------------------------------------------------------*
          02 ISPC0140-AREA-ERRORI.
             03 ISPC0140-ESITO                       PIC 9(02).
             03 ISPC0140-ERR-NUM-ELE                 PIC 9(03).
             03 ISPC0140-TAB-ERRORI             OCCURS 10.
                05 ISPC0140-COD-ERR                  PIC X(12).
                05 ISPC0140-DESCRIZIONE-ERR          PIC X(50).
                05 ISPC0140-GRAVITA-ERR              PIC 9(02).
                05 ISPC0140-LIVELLO-DEROGA           PIC 9(02).

      **************************************************************
      * STATEMETS X GESTIONE GUIDA DA SEQUENZIALE
      * N.B. - DA UTILIZZARE CON LA COPY IABVSQG1
      **************************************************************

       CALL-SEQGUIDE.

           PERFORM INIZIO-SEQGUIDE     THRU INIZIO-SEQGUIDE-EX.

           PERFORM ELABORA-SEQGUIDE    THRU ELABORA-SEQGUIDE-EX

           PERFORM FINE-SEQGUIDE       THRU FINE-SEQGUIDE-EX.

       CALL-SEQGUIDE-EX.
           EXIT.


      ******************************************************************
       INIZIO-SEQGUIDE.

           MOVE NOME-SEQGUIDE           TO   IDSV0003-NOME-TABELLA.

           MOVE ZEROES                  TO   IDSV0003-NUM-RIGHE-LETTE.

           SET IDSV0003-SUCCESSFUL-RC   TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL  TO TRUE.

       INIZIO-SEQGUIDE-EX.
           EXIT.

      ******************************************************************
       CHECK-RETURN-CODE-SEQGUIDE.

           MOVE FS-SEQGUIDE          TO FILE-STATUS.

           EVALUATE TRUE
               WHEN FILE-STATUS-OK
                    CONTINUE

               WHEN FILE-STATUS-END-OF-FILE
                  IF IDSV0003-SELECT                OR
                     IDSV0003-FETCH-FIRST           OR
                     IDSV0003-FETCH-NEXT
                     SET IDSV0003-NOT-FOUND     TO TRUE
                  ELSE
                     SET IDSV0003-GENERIC-ERROR TO TRUE

                     PERFORM COMPONI-MSG-SEQGUIDE
                                            THRU COMPONI-MSG-SEQGUIDE-EX
                  END-IF

               WHEN OTHER
                  SET IDSV0003-GENERIC-ERROR    TO TRUE
                  PERFORM COMPONI-MSG-SEQGUIDE
                                            THRU COMPONI-MSG-SEQGUIDE-EX

           END-EVALUATE.

       CHECK-RETURN-CODE-SEQGUIDE-EX.
           EXIT.

      ******************************************************************
       COMPONI-MSG-SEQGUIDE.

           MOVE IDSV0003-NOME-TABELLA     TO MSG-NOME-FILE.
           MOVE FILE-STATUS               TO MSG-RC
           MOVE MSG-ERR-FILE              TO IDSV0003-DESCRIZ-ERR-DB2.

       COMPONI-MSG-SEQGUIDE-EX.
           EXIT.

      ******************************************************************
       ELABORA-SEQGUIDE.

           EVALUATE TRUE
              WHEN IDSV0003-SELECT
                 PERFORM UNIQUE-READ-SEQGUIDE
                                        THRU UNIQUE-READ-SEQGUIDE-EX
              WHEN IDSV0003-OPEN-CURSOR
                 PERFORM OPEN-FILE-SEQGUIDE
                                             THRU OPEN-FILE-SEQGUIDE-EX
              WHEN IDSV0003-CLOSE-CURSOR
                 PERFORM CLOSE-FILE-SEQGUIDE
                                             THRU CLOSE-FILE-SEQGUIDE-EX
              WHEN IDSV0003-FETCH-FIRST
                 PERFORM READ-FIRST-SEQGUIDE
                                             THRU READ-FIRST-SEQGUIDE-EX
              WHEN IDSV0003-FETCH-NEXT
                 PERFORM READ-NEXT-SEQGUIDE
                                             THRU READ-NEXT-SEQGUIDE-EX
              WHEN IDSV0003-UPDATE
                 CONTINUE

              WHEN OTHER
                 SET IDSV0003-INVALID-OPER  TO TRUE
           END-EVALUATE.

       ELABORA-SEQGUIDE-EX.
           EXIT.

      ******************************************************************
       UNIQUE-READ-SEQGUIDE.

           PERFORM OPEN-FILE-SEQGUIDE        THRU OPEN-FILE-SEQGUIDE-EX.

           IF IDSV0003-SUCCESSFUL-RC
              PERFORM READ-NEXT-SEQGUIDE     THRU READ-NEXT-SEQGUIDE-EX

              IF IDSV0003-SUCCESSFUL-RC
                 PERFORM CLOSE-FILE-SEQGUIDE THRU CLOSE-FILE-SEQGUIDE-EX
              END-IF

           END-IF.

       UNIQUE-READ-SEQGUIDE-EX.
           EXIT.

      ******************************************************************
       OPEN-FILE-SEQGUIDE.

           SET MSG-OPEN              TO TRUE.

           OPEN INPUT SEQGUIDE.

           PERFORM CHECK-RETURN-CODE-SEQGUIDE
                                     THRU CHECK-RETURN-CODE-SEQGUIDE-EX.

           IF IDSV0003-SUCCESSFUL-RC
              SET SEQGUIDE-APERTO    TO TRUE
           END-IF.

       OPEN-FILE-SEQGUIDE-EX.
           EXIT.

      ******************************************************************
       CLOSE-FILE-SEQGUIDE.

           IF SEQGUIDE-APERTO

              SET MSG-CLOSE                    TO TRUE

              CLOSE SEQGUIDE

              PERFORM CHECK-RETURN-CODE-SEQGUIDE
                                      THRU CHECK-RETURN-CODE-SEQGUIDE-EX

              IF IDSV0003-SUCCESSFUL-RC
                 SET SEQGUIDE-CHIUSO           TO TRUE
              END-IF

           END-IF.

       CLOSE-FILE-SEQGUIDE-EX.
           EXIT.

      ******************************************************************
       READ-FIRST-SEQGUIDE.

           PERFORM OPEN-FILE-SEQGUIDE      THRU OPEN-FILE-SEQGUIDE-EX.

           IF IDSV0003-SUCCESSFUL-RC
              PERFORM READ-NEXT-SEQGUIDE   THRU READ-NEXT-SEQGUIDE-EX
           END-IF.

       READ-FIRST-SEQGUIDE-EX.
           EXIT.

      ******************************************************************
       READ-NEXT-SEQGUIDE.

           SET MSG-READ                    TO TRUE.

           READ SEQGUIDE.

           PERFORM CHECK-RETURN-CODE-SEQGUIDE
                                     THRU CHECK-RETURN-CODE-SEQGUIDE-EX.

           IF IDSV0003-SUCCESSFUL-RC

              IF IDSV0003-NOT-FOUND

                 PERFORM CLOSE-FILE-SEQGUIDE THRU CLOSE-FILE-SEQGUIDE-EX

                 IF IDSV0003-SUCCESSFUL-RC
                    SET IDSV0003-NOT-FOUND   TO TRUE
                 END-IF

              END-IF

           END-IF.

       READ-NEXT-SEQGUIDE-EX.
           EXIT.

      ******************************************************************
       FINE-SEQGUIDE.

           CONTINUE.

       FINE-SEQGUIDE-EX.
           EXIT.

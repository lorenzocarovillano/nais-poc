      *----------------------------------------------------------------*
      *  preparazione aree per chiamata modulo
      *----------------------------------------------------------------*
       VALORIZZA-AREA-VAR.
      *-->  Valorizzazione della struttura di mapping
           MOVE ZERO                        TO IX-CONTA
           MOVE 1                           TO WK-APPO-LUNGHEZZA

      *--> OCCORRENZA 1
           EVALUATE WKS-NOME-TABB(IX-AP-TABB)
             WHEN 'MOVI'
              IF WMOV-ELE-MOVI-MAX GREATER ZEROES
                 ADD  1                      TO IX-CONTA
                 MOVE (SF)-ALIAS-MOVIMENTO   TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WMOV-ELE-MOVI-MAX TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WMOV-AREA-MOVIMENTO
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WMOV-AREA-MOVIMENTO
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                       (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'POLI'
      *-->    OCCORRENZA 2
              IF WPOL-ELE-POLI-MAX GREATER ZEROES
                 ADD  1                      TO IX-CONTA
                 MOVE (SF)-ALIAS-POLI        TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WPOL-ELE-POLI-MAX TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WPOL-AREA-POLIZZA
                                             TO (SF)-LUNGHEZZA(IX-CONTA)
                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati

                 MOVE WPOL-AREA-POLIZZA
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                       (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
              IF V1391-WHERE-CONDITION(IX-COD-MVV)(1:7) = 'GRZ-L19'
                 IF WGRZ-ELE-GARANZIA-MAX GREATER ZEROES
                    ADD 1                    TO IX-CONTA
                    MOVE (SF)-ALIAS-GARANZIA TO (SF)-TAB-ALIAS(IX-CONTA)
                    MOVE WGRZ-ELE-GARANZIA-MAX
                                       TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                    MOVE WK-APPO-LUNGHEZZA   TO (SF)-POSIZ-INI(IX-CONTA)
                    MOVE LENGTH OF WGRZ-AREA-GARANZIA
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                    COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->           Valorizzazione del buffer dati
                    MOVE WGRZ-AREA-GARANZIA
                      TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                              (SF)-LUNGHEZZA(IX-CONTA))
                 END-IF
                 IF WL19-ELE-FND-MAX GREATER ZEROES
                    ADD 1                    TO IX-CONTA
                    MOVE (SF)-ALIAS-QOTAZ-FON
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                    MOVE WL19-ELE-FND-MAX
                                       TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                    MOVE WK-APPO-LUNGHEZZA   TO (SF)-POSIZ-INI(IX-CONTA)
                    MOVE LENGTH OF  WL19-AREA-QUOTE
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                    COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->           Valorizzazione del buffer dati
                    MOVE WL19-AREA-QUOTE
                      TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                              (SF)-LUNGHEZZA(IX-CONTA))
                 END-IF
                 IF WMOV-ELE-MOVI-MAX GREATER ZEROES
                    ADD  1                    TO IX-CONTA
                    MOVE (SF)-ALIAS-MOVIMENTO
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                    MOVE WMOV-ELE-MOVI-MAX
                                        TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                    MOVE WK-APPO-LUNGHEZZA   TO (SF)-POSIZ-INI(IX-CONTA)
                    MOVE LENGTH OF WMOV-AREA-MOVIMENTO
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                    COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->      Valorizzazione del buffer dati
                    MOVE WMOV-AREA-MOVIMENTO
                      TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                          (SF)-LUNGHEZZA(IX-CONTA))
                 END-IF
              END-IF
              IF V1391-WHERE-CONDITION(IX-COD-MVV)(1:3)  = 'GRZ'
                 IF WGRZ-ELE-GARANZIA-MAX GREATER ZEROES
                    ADD 1                    TO IX-CONTA
                    MOVE (SF)-ALIAS-GARANZIA TO (SF)-TAB-ALIAS(IX-CONTA)
                    MOVE WGRZ-ELE-GARANZIA-MAX
                                       TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                    MOVE WK-APPO-LUNGHEZZA   TO (SF)-POSIZ-INI(IX-CONTA)
                    MOVE LENGTH OF WGRZ-AREA-GARANZIA
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                    COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1

                    MOVE WGRZ-AREA-GARANZIA
                      TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                              (SF)-LUNGHEZZA(IX-CONTA))
                 END-IF
              END-IF

              IF V1391-WHERE-CONDITION(IX-COD-MVV)(1:3)  = 'TGA'
                 IF IVVC0216-AREA-VE
                    IF W1TGA-ELE-TRAN-MAX GREATER ZEROES

                       INITIALIZE WTGA-AREA-TRANCHE

                       PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
                         UNTIL IX-TAB-TGA > W1TGA-ELE-TRAN-MAX
                            OR NOT IDSV0003-SUCCESSFUL-RC

                            IF IX-TAB-TGA > WK-GRZ-MAX-B
                               MOVE WK-PGM
                                 TO IDSV0003-COD-SERVIZIO-BE
                               STRING 'OVERFLOW AREA TGA '
                                      'CALL MODULO CALCOLO'
                               DELIMITED BY SIZE INTO
                                   IDSV0003-DESCRIZ-ERR-DB2
                               END-STRING
                               SET IDSV0003-INVALID-OPER  TO TRUE
                            ELSE
                               ADD  1 TO WTGA-ELE-TRAN-MAX
                               MOVE W1TGA-TAB-TRAN(IX-TAB-TGA)
                                 TO WTGA-TAB-TRAN(WTGA-ELE-TRAN-MAX)
                            END-IF

                       END-PERFORM

                       IF IDSV0003-SUCCESSFUL-RC
                          ADD 1                    TO IX-CONTA
                          MOVE (SF)-ALIAS-TRCH-GAR
                              TO (SF)-TAB-ALIAS(IX-CONTA)
                          MOVE WTGA-ELE-TRAN-MAX
                                       TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                          MOVE WK-APPO-LUNGHEZZA
                            TO (SF)-POSIZ-INI(IX-CONTA)
                          MOVE LENGTH OF WTGA-AREA-TRANCHE
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                          COMPUTE WK-APPO-LUNGHEZZA =
                                           WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1

                          MOVE WTGA-AREA-TRANCHE
                           TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                              (SF)-LUNGHEZZA(IX-CONTA))
                      END-IF
                    END-IF
                 END-IF
              END-IF
              IF V1391-WHERE-CONDITION(IX-COD-MVV)(1:3) = 'RAN'
                 IF WRAN-ELE-RAPP-ANAG-MAX GREATER ZEROES
                   ADD 1                     TO IX-CONTA
                   MOVE (SF)-ALIAS-RAPP-ANAG TO (SF)-TAB-ALIAS(IX-CONTA)
                   MOVE WRAN-ELE-RAPP-ANAG-MAX
                                       TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                   MOVE WK-APPO-LUNGHEZZA    TO (SF)-POSIZ-INI(IX-CONTA)
                   MOVE LENGTH OF WRAN-AREA-RAPP-ANAG
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                   COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                          (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->           Valorizzazione del buffer dati
                   MOVE WRAN-AREA-RAPP-ANAG
                     TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                             (SF)-LUNGHEZZA(IX-CONTA))
                 END-IF
              END-IF

12969         IF V1391-WHERE-CONDITION(IX-COD-MVV)(1:9) = 'VAR-X-GAR'
12969 *          IF IVVC0223-ELE-MAX-AREA-GAR GREATER ZEROES
12969              SET VXG-ADDRESS   TO ADDRESS
12969               OF AREA-IVVC0223
12969              ADD 1                     TO IX-CONTA
12969              MOVE (SF)-ALIAS-AREA-VAR-X-GAR
12969                TO (SF)-TAB-ALIAS(IX-CONTA)
12969              MOVE IVVC0223-ELE-MAX-AREA-GAR
12969                                  TO (SF)-NUM-OCCORRENZE(IX-CONTA)
12969              MOVE WK-APPO-LUNGHEZZA    TO (SF)-POSIZ-INI(IX-CONTA)
12969 *            MOVE LENGTH OF IVVC0223-AREA-VARIABILI-GAR
12969 *                                      TO (SF)-LUNGHEZZA(IX-CONTA)
12969              MOVE LENGTH OF AREA-POINTER
12969                          TO (SF)-LUNGHEZZA(IX-CONTA)

12969              COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
12969                                     (SF)-LUNGHEZZA(IX-CONTA) + 1
12969 *-->           Valorizzazione del buffer dati
12969 *            MOVE IVVC0223-AREA-VARIABILI-GAR
12969 *              TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
12969 *                                      (SF)-LUNGHEZZA(IX-CONTA))
12969            MOVE AREA-POINTER
12969              TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
12969                                  (SF)-LUNGHEZZA(IX-CONTA))


12969 *          END-IF
12969         END-IF


12121         IF V1391-WHERE-CONDITION(IX-COD-MVV)(1:11)  =
12121                                                     'GRZ-TGA-RAN'

12121            IF WGRZ-ELE-GARANZIA-MAX GREATER ZEROES
12121               ADD 1                    TO IX-CONTA
12121               MOVE (SF)-ALIAS-GARANZIA TO (SF)-TAB-ALIAS(IX-CONTA)
12121               MOVE WGRZ-ELE-GARANZIA-MAX
12121                                  TO (SF)-NUM-OCCORRENZE(IX-CONTA)
12121               MOVE WK-APPO-LUNGHEZZA   TO (SF)-POSIZ-INI(IX-CONTA)
12121               MOVE LENGTH OF WGRZ-AREA-GARANZIA
12121                                        TO (SF)-LUNGHEZZA(IX-CONTA)

12121               COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
12121                                      (SF)-LUNGHEZZA(IX-CONTA) + 1

12121               MOVE WGRZ-AREA-GARANZIA
12121                 TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
12121                                         (SF)-LUNGHEZZA(IX-CONTA))
12121            END-IF

12121            IF IVVC0216-AREA-VE
12121               IF W1TGA-ELE-TRAN-MAX GREATER ZEROES

12121                  INITIALIZE WTGA-AREA-TRANCHE

12121                  PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
12121                    UNTIL IX-TAB-TGA > W1TGA-ELE-TRAN-MAX
12121                       OR NOT IDSV0003-SUCCESSFUL-RC

12121                       IF IX-TAB-TGA > WK-GRZ-MAX-B
12121                          MOVE WK-PGM
12121                            TO IDSV0003-COD-SERVIZIO-BE
12121                          STRING 'OVERFLOW AREA TGA '
12121                                 'CALL MODULO CALCOLO'
12121                          DELIMITED BY SIZE INTO
12121                              IDSV0003-DESCRIZ-ERR-DB2
12121                          END-STRING
12121                          SET IDSV0003-INVALID-OPER  TO TRUE
12121                       ELSE
12121                          ADD  1 TO WTGA-ELE-TRAN-MAX
12121                          MOVE W1TGA-TAB-TRAN(IX-TAB-TGA)
12121                            TO WTGA-TAB-TRAN(WTGA-ELE-TRAN-MAX)
12121                       END-IF

12121                  END-PERFORM

12121                  IF IDSV0003-SUCCESSFUL-RC
12121                     ADD 1                    TO IX-CONTA
12121                     MOVE (SF)-ALIAS-TRCH-GAR
12121                         TO (SF)-TAB-ALIAS(IX-CONTA)
12121                     MOVE WTGA-ELE-TRAN-MAX
12121                                  TO (SF)-NUM-OCCORRENZE(IX-CONTA)
12121                     MOVE WK-APPO-LUNGHEZZA
12121                       TO (SF)-POSIZ-INI(IX-CONTA)
12121                     MOVE LENGTH OF WTGA-AREA-TRANCHE
12121                                        TO (SF)-LUNGHEZZA(IX-CONTA)

12121                     COMPUTE WK-APPO-LUNGHEZZA =
12121                                      WK-APPO-LUNGHEZZA +
12121                                      (SF)-LUNGHEZZA(IX-CONTA) + 1

12121                     MOVE WTGA-AREA-TRANCHE
12121                      TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
12121                                         (SF)-LUNGHEZZA(IX-CONTA))
12121                 END-IF
12121               END-IF
12121            END-IF

12121            IF WRAN-ELE-RAPP-ANAG-MAX GREATER ZEROES
12121              ADD 1                     TO IX-CONTA
12121              MOVE (SF)-ALIAS-RAPP-ANAG TO (SF)-TAB-ALIAS(IX-CONTA)
12121              MOVE WRAN-ELE-RAPP-ANAG-MAX
12121                                  TO (SF)-NUM-OCCORRENZE(IX-CONTA)
12121              MOVE WK-APPO-LUNGHEZZA    TO (SF)-POSIZ-INI(IX-CONTA)
12121              MOVE LENGTH OF WRAN-AREA-RAPP-ANAG
12121                                        TO (SF)-LUNGHEZZA(IX-CONTA)

12121              COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
12121                                     (SF)-LUNGHEZZA(IX-CONTA) + 1
12121 *-->           Valorizzazione del buffer dati
12121              MOVE WRAN-AREA-RAPP-ANAG
12121                TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
12121                                        (SF)-LUNGHEZZA(IX-CONTA))
12121            END-IF

12121         END-IF

             WHEN 'ADES'
      *-->    OCCORRENZA 3
              IF WADE-ELE-ADES-MAX GREATER ZEROES
               ADD 1                         TO IX-CONTA
               MOVE (SF)-ALIAS-ADES          TO (SF)-TAB-ALIAS(IX-CONTA)
               MOVE WADE-ELE-ADES-MAX   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
               MOVE WK-APPO-LUNGHEZZA        TO (SF)-POSIZ-INI(IX-CONTA)
               MOVE LENGTH OF WADE-AREA-ADESIONE
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

               COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->     lorizzazione del buffer dati
               MOVE WADE-AREA-ADESIONE
                 TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                             (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'D-FISC-ADES'
      *-->    OCCORRENZA 4
              IF WDFA-ELE-FISC-ADES-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-DT-FISC-ADES
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WDFA-ELE-FISC-ADES-MAX
                                        TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WDFA-AREA-DT-FISC-ADES
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WDFA-AREA-DT-FISC-ADES
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                              (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'RAPP-ANA'
      *-->    OCCORRENZA 5
              IF WRAN-ELE-RAPP-ANAG-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-RAPP-ANAG   TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WRAN-ELE-RAPP-ANAG-MAX
                                        TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WRAN-AREA-RAPP-ANAG
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WRAN-AREA-RAPP-ANAG
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                              (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
              IF V1391-WHERE-CONDITION(IX-COD-MVV)(1:3) = 'GRZ'
                 IF WGRZ-ELE-GARANZIA-MAX GREATER ZEROES
                    ADD 1                    TO IX-CONTA
                    MOVE (SF)-ALIAS-GARANZIA TO (SF)-TAB-ALIAS(IX-CONTA)
                    MOVE WGRZ-ELE-GARANZIA-MAX
                                       TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                    MOVE WK-APPO-LUNGHEZZA   TO (SF)-POSIZ-INI(IX-CONTA)
                    MOVE LENGTH OF WGRZ-AREA-GARANZIA
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                    COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->           Valorizzazione del buffer dati
                    MOVE WGRZ-AREA-GARANZIA
                      TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                              (SF)-LUNGHEZZA(IX-CONTA))
                 END-IF
              END-IF
13385         IF V1391-WHERE-CONDITION(IX-COD-MVV)(1:3) = 'P56'
13385            IF WP56-QUEST-ADEG-VER-MAX GREATER ZEROES
13385               ADD 1                    TO IX-CONTA
13385               MOVE (SF)-ALIAS-QUEST-ADEG-VER
13385                                        TO (SF)-TAB-ALIAS(IX-CONTA)
13385               MOVE WP56-QUEST-ADEG-VER-MAX
13385                                  TO (SF)-NUM-OCCORRENZE(IX-CONTA)
13385               MOVE WK-APPO-LUNGHEZZA   TO (SF)-POSIZ-INI(IX-CONTA)
13385               MOVE LENGTH OF WP56-AREA-QUEST-ADEG-VER
13385                                        TO (SF)-LUNGHEZZA(IX-CONTA)

13385               COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
13385                                      (SF)-LUNGHEZZA(IX-CONTA) + 1
13385 *-->           Valorizzazione del buffer dati
13385               MOVE WP56-AREA-QUEST-ADEG-VER
13385                 TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
13385                                         (SF)-LUNGHEZZA(IX-CONTA))
13385            END-IF
13385         END-IF
             WHEN 'D-FISC-ADES'
      *-->    OCCORRENZA 4
              IF WDFA-ELE-FISC-ADES-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-DT-FISC-ADES
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WDFA-ELE-FISC-ADES-MAX
                                        TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WDFA-AREA-DT-FISC-ADES
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WDFA-AREA-DT-FISC-ADES
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                              (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'IMPST-SOST'
      *-->    OCCORRENZA 5
              IF WISO-ELE-IMP-SOST-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-IMPOSTA-SOST
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WISO-ELE-IMP-SOST-MAX
                                        TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WISO-AREA-IMPOSTA-SOST
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WISO-AREA-IMPOSTA-SOST
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                              (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'GAR'
      *-->       OCCORRENZA 6
              IF WGRZ-ELE-GARANZIA-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-GARANZIA    TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WGRZ-ELE-GARANZIA-MAX
                                        TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WGRZ-AREA-GARANZIA
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WGRZ-AREA-GARANZIA
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                              (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'STRA-DI-INVST'
      *-->       OCCORRENZA 7
              IF WSDI-ELE-STRA-INV-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-STRA-INV    TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WSDI-ELE-STRA-INV-MAX
                                        TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA     TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WSDI-AREA-STRA-INV
                                            TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WSDI-AREA-STRA-INV
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                             (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'TRCH-DI-GAR'
      *-->       OCCORRENZA 9
              IF WTGA-ELE-TRAN-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-TRCH-GAR    TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WTGA-ELE-TRAN-MAX TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WTGA-AREA-TRANCHE
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WTGA-AREA-TRANCHE
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                            (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
              IF V1391-WHERE-CONDITION(IX-COD-MVV)(1:7) = 'GRZ-L19'
                 IF WGRZ-ELE-GARANZIA-MAX GREATER ZEROES
                    ADD 1                    TO IX-CONTA
                    MOVE (SF)-ALIAS-GARANZIA TO (SF)-TAB-ALIAS(IX-CONTA)
                    MOVE WGRZ-ELE-GARANZIA-MAX
                                       TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                    MOVE WK-APPO-LUNGHEZZA   TO (SF)-POSIZ-INI(IX-CONTA)
                    MOVE LENGTH OF WGRZ-AREA-GARANZIA
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                    COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->           Valorizzazione del buffer dati
                    MOVE WGRZ-AREA-GARANZIA
                      TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                              (SF)-LUNGHEZZA(IX-CONTA))
                 END-IF
                 IF WL19-ELE-FND-MAX GREATER ZEROES
                    ADD 1                    TO IX-CONTA
                    MOVE (SF)-ALIAS-QOTAZ-FON
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                    MOVE WL19-ELE-FND-MAX
                                       TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                    MOVE WK-APPO-LUNGHEZZA   TO (SF)-POSIZ-INI(IX-CONTA)
                    MOVE LENGTH OF  WL19-AREA-QUOTE
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                    COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->           Valorizzazione del buffer dati
                    MOVE WL19-AREA-QUOTE
                      TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                              (SF)-LUNGHEZZA(IX-CONTA))
                 END-IF
              END-IF
              IF V1391-WHERE-CONDITION(IX-COD-MVV)(1:3) = 'GRZ'
                 IF WGRZ-ELE-GARANZIA-MAX GREATER ZEROES
                    ADD 1                    TO IX-CONTA
                    MOVE (SF)-ALIAS-GARANZIA TO (SF)-TAB-ALIAS(IX-CONTA)
                    MOVE WGRZ-ELE-GARANZIA-MAX
                                       TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                    MOVE WK-APPO-LUNGHEZZA   TO (SF)-POSIZ-INI(IX-CONTA)
                    MOVE LENGTH OF WGRZ-AREA-GARANZIA
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                    COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->           Valorizzazione del buffer dati
                    MOVE WGRZ-AREA-GARANZIA
                      TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                              (SF)-LUNGHEZZA(IX-CONTA))
                 END-IF
              END-IF
             WHEN 'PARAM-MOVI'
      *-->       OCCORRENZA 10
              IF WPMO-ELE-PARAM-MOV-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-PARAM-MOV   TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WPMO-ELE-PARAM-MOV-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WPMO-AREA-PARAM-MOV
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WPMO-AREA-PARAM-MOV
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                       (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'BNFIC'
      *-->       OCCORRENZA 11
              IF WBEP-ELE-BENEF-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-BENEF       TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WBEP-ELE-BENEF-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WBEP-AREA-BENEF
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WBEP-AREA-BENEF
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                             (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'PARAM-OGG'
      *-->       OCCORRENZA 12
              IF WPOG-ELE-PARAM-OGG-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-PARAM-OGG   TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WPOG-ELE-PARAM-OGG-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WPOG-AREA-PARAM-OGG
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WPOG-AREA-PARAM-OGG
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                       (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'QUEST'
      *-->       OCCORRENZA 14
              IF WQUE-ELE-QUEST-MAX GREATER ZEROES
                 ADD 1                      TO IX-CONTA
                 MOVE (SF)-ALIAS-QUEST      TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WQUE-ELE-QUEST-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA     TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WQUE-AREA-QUEST
                                            TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WQUE-AREA-QUEST
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                              (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'DETT-QUEST'
      *-->       OCCORRENZA 15
              IF WDEQ-ELE-DETT-QUEST-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-DETT-QUEST  TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WDEQ-ELE-DETT-QUEST-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WDEQ-AREA-DETT-QUEST
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WDEQ-AREA-DETT-QUEST
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'TIT-CONT'
      *-->       OCCORRENZA 16
              IF WTIT-ELE-TIT-CONT-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-TIT-CONT    TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WTIT-ELE-TIT-CONT-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WTIT-AREA-TIT-CONT
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WTIT-AREA-TIT-CONT
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                             (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'DETT-TIT-CONT'
      *-->       OCCORRENZA 17
              IF WDTC-ELE-DETT-TIT-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-DETT-TIT-CONT
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WDTC-ELE-DETT-TIT-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WDTC-AREA-DETT-TIT-CONT
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WDTC-AREA-DETT-TIT-CONT
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                             (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'RICH-INVST-FND'
      *-->       OCCORRENZA 20
              IF WRIF-ELE-RIC-INV-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-RICH-INV-FND
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WRIF-ELE-RIC-INV-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WRIF-AREA-RICH-INV-FND
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WRIF-AREA-RICH-INV-FND
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                             (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'SOPR-DI-GAR'
      *-->       OCCORRENZA 21
              IF WSPG-ELE-SOPRAP-GAR-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-SOPRAP-GAR  TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WSPG-ELE-SOPRAP-GAR-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WSPG-AREA-SOPRAP-GAR
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WSPG-AREA-SOPRAP-GAR
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'OGG-COLLG'
      *-->       OCCORRENZA 22
              IF WOCO-ELE-OGG-COLL-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-OGG-COLL    TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WOCO-ELE-OGG-COLL-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WOCO-AREA-OGG-COLL
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WOCO-AREA-OGG-COLL
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                            (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'GAR-LIQ'
      *-->       OCCORRENZA 25
              IF WGRL-ELE-GAR-LIQ-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-GAR-LIQ     TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WGRL-ELE-GAR-LIQ-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WGRL-AREA-GARANZIA-LIQ
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WGRL-AREA-GARANZIA-LIQ
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                             (SF)-LUNGHEZZA(IX-CONTA))
              END-IF

             WHEN 'LIQ'
      *-->       OCCORRENZA 25
              IF WLQU-ELE-LIQ-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-LIQUIDAZ    TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WLQU-ELE-LIQ-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WLQU-AREA-LIQUIDAZIONE
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WLQU-AREA-LIQUIDAZIONE
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                             (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'PERC-LIQ'
      *-->       OCCORRENZA 25
              IF WPLI-ELE-PERC-LIQ-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-PERC-LIQ    TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WPLI-ELE-PERC-LIQ-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WPLI-AREA-PERC-LIQ
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WPLI-AREA-PERC-LIQ
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                            (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'D-COLL'
      *-->       OCCORRENZA 25
              IF WDCO-ELE-COLL-MAX GREATER ZEROES
                 ADD 1                      TO IX-CONTA
                 MOVE (SF)-ALIAS-DT-COLL    TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WDCO-ELE-COLL-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WDCO-AREA-DT-COLLETTIVA
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WDCO-AREA-DT-COLLETTIVA
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                             (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
             WHEN 'RAPP-RETE'
      *-->       OCCORRENZA 25
              IF WRRE-ELE-RAPP-RETE-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-RAPP-RETE   TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WRRE-ELE-RAPP-RETE-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WRRE-AREA-RAPP-RETE
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WRRE-AREA-RAPP-RETE
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
      *
             WHEN 'BNFICR-LIQ'
              IF WBEL-ELE-BENEF-LIQ-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-BENEF-LIQ   TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WBEL-ELE-BENEF-LIQ-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WBEL-AREA-BENEF-LIQ
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WBEL-AREA-BENEF-LIQ
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                              (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
      *
             WHEN 'DFLT-ADES'
              IF WDAD-ELE-DFLT-ADES-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-DFLT-ADES   TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WDAD-ELE-DFLT-ADES-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WDAD-AREA-DEFAULT-ADES
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WDAD-AREA-DEFAULT-ADES
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                             (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
      *
             WHEN 'TRCH-LIQ'
              IF WTLI-ELE-TRCH-LIQ-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-TRCH-LIQ    TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WTLI-ELE-TRCH-LIQ-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WTLI-AREA-TRCH-LIQ
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WTLI-AREA-TRCH-LIQ
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                             (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
      *
             WHEN 'D-FORZ-LIQ'
              IF WDFL-ELE-DFL-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-DOC-LIQ     TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WDFL-ELE-DFL-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WDFL-AREA-DFL
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WDFL-AREA-DFL
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
      *
             WHEN 'PARAM-COMP'
              IF WPCO-ELE-PCO-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-PARAM-COMP  TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WPCO-ELE-PCO-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WPCO-AREA-PARA-COM
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WPCO-AREA-PARA-COM
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
              END-IF
      *
             WHEN 'RIS-DI-TRCH'
              IF WRST-ELE-RST-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-RIS-DI-TRANCHE
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WRST-ELE-RST-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WRST-AREA-RST
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WRST-AREA-RST
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
              END-IF

             WHEN 'EST-TRCH-DI-GAR'
              IF WE12-ELE-ESTRA-MAX GREATER ZEROES
                 ADD 1                       TO IX-CONTA
                 MOVE (SF)-ALIAS-EST-TRCH-DI-GAR
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE WE12-ELE-ESTRA-MAX
                   TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                 MOVE LENGTH OF WE12-AREA-ESTRA
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati
                 MOVE WE12-AREA-ESTRA
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
               END-IF

              WHEN 'REINVST-POLI-LQ'
               IF WL30-ELE-REINVST-POLI-MAX GREATER ZEROES
                  ADD 1                      TO IX-CONTA
                  MOVE (SF)-ALIAS-REINVST-POLI
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                  MOVE WL30-ELE-REINVST-POLI-MAX
                    TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                  MOVE WK-APPO-LUNGHEZZA     TO (SF)-POSIZ-INI(IX-CONTA)
                  MOVE LENGTH OF WL30-AREA-REINVST-POLI
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                  COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->         Valorizzazione del buffer dati
                  MOVE WL30-AREA-REINVST-POLI
                    TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
               END-IF

              WHEN 'VINC-PEG'
               IF WL23-ELE-VINC-PEG-MAX GREATER ZEROES
                  ADD 1                      TO IX-CONTA
                  MOVE (SF)-ALIAS-VINC-PEGN
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                  MOVE WL23-ELE-VINC-PEG-MAX
                    TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                  MOVE WK-APPO-LUNGHEZZA     TO (SF)-POSIZ-INI(IX-CONTA)
                  MOVE LENGTH OF WL23-AREA-VINC-PEG
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                  COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->         Valorizzazione del buffer dati
                  MOVE WL23-AREA-VINC-PEG
                    TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
               END-IF

              WHEN 'PROV-DI-GAR'
               IF WPVT-ELE-PROV-MAX GREATER ZEROES
                  ADD 1                      TO IX-CONTA
                  MOVE (SF)-ALIAS-PROVV-TRAN
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                  MOVE WPVT-ELE-PROV-MAX
                    TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                  MOVE WK-APPO-LUNGHEZZA     TO (SF)-POSIZ-INI(IX-CONTA)
                  MOVE LENGTH OF WPVT-AREA-PROV
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                  COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->         Valorizzazione del buffer dati
                  MOVE WPVT-AREA-PROV
                    TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
               END-IF

              WHEN 'AREA-FND-X-TRANCHE'
12805 *       IF IVVC0222-ELE-TRCH-MAX GREATER ZEROES
                 ADD  1                      TO IX-CONTA
                 MOVE (SF)-ALIAS-AREA-FND-X-TRCH
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                 MOVE IVVC0222-ELE-TRCH-MAX
                                        TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                 MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
      *          MOVE LENGTH OF IVVC0222-AREA-FND-X-TRANCHE
      *                                      TO (SF)-LUNGHEZZA(IX-CONTA)
                 MOVE LENGTH OF AREA-POINTER TO (SF)-LUNGHEZZA(IX-CONTA)
                 COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->        Valorizzazione del buffer dati

      *          MOVE IVVC0222-AREA-FND-X-TRANCHE
      *            TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
      *                                (SF)-LUNGHEZZA(IX-CONTA))
                 MOVE AREA-POINTER
                   TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                       (SF)-LUNGHEZZA(IX-CONTA))
12805 *       END-IF
FNZF2           IF V1391-WHERE-CONDITION(IX-COD-MVV)(1:3) = 'RDF'
FNZF2             IF WRDF-ELE-RIC-INV-MAX GREATER ZEROES
FNZF2               ADD 1                    TO IX-CONTA
FNZF2               MOVE (SF)-ALIAS-RICH-DISINV-FND
FNZF2                                        TO (SF)-TAB-ALIAS(IX-CONTA)
FNZF2               MOVE WRDF-ELE-RIC-INV-MAX
FNZF2                                  TO (SF)-NUM-OCCORRENZE(IX-CONTA)
FNZF2               MOVE WK-APPO-LUNGHEZZA   TO (SF)-POSIZ-INI(IX-CONTA)
FNZF2               COMPUTE (SF)-LUNGHEZZA(IX-CONTA) =
FNZF2                       ((LENGTH OF WRDF-TAB-RIC-DISINV(1))
FNZF2                       * WRDF-ELE-RIC-INV-MAX)
FNZF2                       + (LENGTH OF WRDF-ELE-RIC-INV-MAX)

FNZF2               COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
FNZF2                                      (SF)-LUNGHEZZA(IX-CONTA) + 1
FNZF2 *-->           Valorizzazione del buffer dati
FNZF2               MOVE WRDF-AREA-RICH-DISINV-FND
FNZF2                        (1:(SF)-LUNGHEZZA(IX-CONTA))
FNZF2                 TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
FNZF2                                         (SF)-LUNGHEZZA(IX-CONTA))
FNZF2             END-IF
FNZF2           END-IF

FNZF2            IF WL19-ELE-FND-MAX GREATER ZEROES
FNZF2               ADD 1                    TO IX-CONTA
FNZF2               MOVE (SF)-ALIAS-QOTAZ-FON
FNZF2                                        TO (SF)-TAB-ALIAS(IX-CONTA)
FNZF2               MOVE WL19-ELE-FND-MAX
FNZF2                                  TO (SF)-NUM-OCCORRENZE(IX-CONTA)
FNZF2               MOVE WK-APPO-LUNGHEZZA   TO (SF)-POSIZ-INI(IX-CONTA)
FNZF2               MOVE LENGTH OF  WL19-AREA-QUOTE
FNZF2                                        TO (SF)-LUNGHEZZA(IX-CONTA)

FNZF2               COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
FNZF2                                      (SF)-LUNGHEZZA(IX-CONTA) + 1
FNZF2 *-->           Valorizzazione del buffer dati
FNZF2               MOVE WL19-AREA-QUOTE
FNZF2                 TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
FNZF2                                         (SF)-LUNGHEZZA(IX-CONTA))
FNZF2            END-IF
              WHEN 'IMPST-BOLLO'
               IF WP58-ELE-MAX-IMPST-BOL GREATER ZEROES
                  ADD 1                      TO IX-CONTA
                  MOVE (SF)-ALIAS-IMPOSTA-BOLLO
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                  MOVE WP58-ELE-MAX-IMPST-BOL
                    TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                  MOVE WK-APPO-LUNGHEZZA     TO (SF)-POSIZ-INI(IX-CONTA)
                  MOVE LENGTH OF WP58-AREA-IMPOSTA-BOLLO
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                  COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->         Valorizzazione del buffer dati
                  MOVE WP58-AREA-IMPOSTA-BOLLO
                    TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
               END-IF
              WHEN 'D-CRIST'
               IF WP61-ELE-MAX-D-CRIST GREATER ZEROES
                  ADD 1                      TO IX-CONTA
                  MOVE (SF)-ALIAS-DATI-CRIST
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                  MOVE WP61-ELE-MAX-D-CRIST
                    TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                  MOVE WK-APPO-LUNGHEZZA     TO (SF)-POSIZ-INI(IX-CONTA)
                  MOVE LENGTH OF WP61-AREA-D-CRIST
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                  COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->         Valorizzazione del buffer dati
                  MOVE WP61-AREA-D-CRIST
                    TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
               END-IF
               IF V1391-WHERE-CONDITION(IX-COD-MVV)(1:7) = 'POLI'
               IF WPOL-ELE-POLI-MAX GREATER ZEROES
                ADD  1                      TO IX-CONTA
                MOVE (SF)-ALIAS-POLI        TO (SF)-TAB-ALIAS(IX-CONTA)
                MOVE WPOL-ELE-POLI-MAX TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                MOVE WK-APPO-LUNGHEZZA      TO (SF)-POSIZ-INI(IX-CONTA)
                MOVE LENGTH OF WPOL-AREA-POLIZZA
                                            TO (SF)-LUNGHEZZA(IX-CONTA)
                COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                           (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->       Valorizzazione del buffer dati

                MOVE WPOL-AREA-POLIZZA
                  TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                      (SF)-LUNGHEZZA(IX-CONTA))
               END-IF
               END-IF

              WHEN 'EST-POLI-CPI-PR'
               IF WP67-ELE-MAX-EST-POLI-CPI-PR GREATER ZEROES
                  ADD 1                      TO IX-CONTA
                  MOVE (SF)-ALIAS-EST-POLI-CPI-PR
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                  MOVE WP67-ELE-MAX-EST-POLI-CPI-PR
                    TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                  MOVE WK-APPO-LUNGHEZZA     TO (SF)-POSIZ-INI(IX-CONTA)
                  MOVE LENGTH OF WP67-AREA-EST-POLI-CPI-PR
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                  COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->         Valorizzazione del buffer dati
                  MOVE WP67-AREA-EST-POLI-CPI-PR
                    TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
               END-IF
              WHEN 'RICH-EST'
               IF WP01-ELE-MAX-RICH-EST GREATER ZEROES
                  ADD 1                      TO IX-CONTA
                  MOVE (SF)-ALIAS-RICH-EST
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                  MOVE WP01-ELE-MAX-RICH-EST
                    TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                  MOVE WK-APPO-LUNGHEZZA     TO (SF)-POSIZ-INI(IX-CONTA)
                  MOVE LENGTH OF WP01-AREA-RICH-EST
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                  COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->         Valorizzazione del buffer dati
                  MOVE WP01-AREA-RICH-EST
                    TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
               END-IF
              WHEN 'MOT-LIQ'
               IF WP86-ELE-MOT-LIQ-MAX GREATER ZEROES
                  ADD 1                      TO IX-CONTA
                  MOVE (SF)-ALIAS-MOT-LIQ
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                  MOVE WP86-ELE-MOT-LIQ-MAX
                    TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                  MOVE WK-APPO-LUNGHEZZA     TO (SF)-POSIZ-INI(IX-CONTA)
                  MOVE LENGTH OF WP86-AREA-MOT-LIQ
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                  COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->         Valorizzazione del buffer dati
                  MOVE WP86-AREA-MOT-LIQ
                    TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
               END-IF

            WHEN 'ATT-SERV-VAL'
               IF WP88-ELE-SER-VAL-MAX GREATER ZEROES
                  ADD 1                      TO IX-CONTA
                  MOVE (SF)-ALIAS-ATT-SERV-VAL
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                  MOVE WP88-ELE-SER-VAL-MAX
                    TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                  MOVE WK-APPO-LUNGHEZZA     TO (SF)-POSIZ-INI(IX-CONTA)
                  MOVE LENGTH OF WP88-AREA-SERV-VAL
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                  COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->         Valorizzazione del buffer dati
                  MOVE WP88-AREA-SERV-VAL
                    TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
               END-IF


            WHEN 'D-ATT-SERV-VAL'
               IF WP89-ELE-DSERV-VAL-MAX GREATER ZEROES
                  ADD 1                      TO IX-CONTA
                  MOVE (SF)-ALIAS-D-ATT-SERV-VAL
                                             TO (SF)-TAB-ALIAS(IX-CONTA)
                  MOVE WP89-ELE-DSERV-VAL-MAX
                    TO (SF)-NUM-OCCORRENZE(IX-CONTA)
                  MOVE WK-APPO-LUNGHEZZA     TO (SF)-POSIZ-INI(IX-CONTA)
                  MOVE LENGTH OF WP89-AREA-DSERV-VAL
                                             TO (SF)-LUNGHEZZA(IX-CONTA)

                  COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
                                            (SF)-LUNGHEZZA(IX-CONTA) + 1
      *-->         Valorizzazione del buffer dati
                  MOVE WP89-AREA-DSERV-VAL
                    TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
                                               (SF)-LUNGHEZZA(IX-CONTA))
               END-IF

16324       WHEN 'AREA-FND-X-CDG'
16324          IF WCDG-ELE-MAX-CDG-VV GREATER ZEROES
16324             ADD  1
16324               TO IX-CONTA
16324             MOVE (SF)-ALIAS-AREA-FND-X-CDG
16324               TO (SF)-TAB-ALIAS(IX-CONTA)
16324             MOVE WCDG-ELE-MAX-CDG-VV
16324               TO (SF)-NUM-OCCORRENZE(IX-CONTA)
16324             MOVE WK-APPO-LUNGHEZZA
16324               TO (SF)-POSIZ-INI(IX-CONTA)
16324             MOVE LENGTH OF WCDG-AREA-COMMIS-GEST-VV
16324               TO (SF)-LUNGHEZZA(IX-CONTA)

16324             COMPUTE WK-APPO-LUNGHEZZA = WK-APPO-LUNGHEZZA +
16324                                       (SF)-LUNGHEZZA(IX-CONTA) + 1
16324 *-->         Valorizzazione del buffer dati
16324             MOVE WCDG-AREA-COMMIS-GEST-VV
16324               TO (SF)-BUFFER-DATI((SF)-POSIZ-INI(IX-CONTA):
16324                                   (SF)-LUNGHEZZA(IX-CONTA))
16324          END-IF

16324      END-EVALUATE.
      *
           MOVE IX-CONTA                    TO (SF)-ELE-INFO-MAX.

      *
       VALORIZZA-AREA-VAR-EX.
           EXIT.

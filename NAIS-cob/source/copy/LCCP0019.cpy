      *----------------------------------------------------------------*
      *    VERIFICA PLATFOND
      *----------------------------------------------------------------*
       VERIFICA-PLATFOND.
      *
           PERFORM VERIFICA-POG-PLTFOND
              THRU VERIFICA-POG-PLTFOND-EX.
      *
           MOVE WCOM-ELE-MAX-PLATFOND
             TO LCCC0019-ELE-MAX-FONDI.
      *
           PERFORM VARYING IX-FONDI FROM 1 BY 1
             UNTIL IX-FONDI GREATER WCOM-ELE-MAX-PLATFOND
      *
               MOVE WCOM-IMP-PLATFOND(IX-FONDI)
                 TO LCCC0019-IMP-PLATFOND(IX-FONDI)
               MOVE WCOM-COD-FONDO(IX-FONDI)
                 TO LCCC0019-COD-FONDO(IX-FONDI)
               IF WCOM-PERC-FOND-OLD(IX-FONDI) IS NUMERIC
                  MOVE WCOM-PERC-FOND-OLD(IX-FONDI)
                    TO LCCC0019-PERC-FONDO-OLD(IX-FONDI)
               ELSE
                  MOVE ZEROES
                    TO LCCC0019-PERC-FONDO-OLD(IX-FONDI)
               END-IF

               PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
                 UNTIL IX-TAB-GRZ GREATER WGRZ-ELE-GAR-MAX

                 SET LCCC0019-TARIFFA-KO        TO TRUE

                 PERFORM VARYING IX-ALL FROM 1 BY 1
                   UNTIL IX-ALL > WALL-ELE-ASSET-ALL-MAX

                   IF NOT WGRZ-ST-DEL(IX-TAB-GRZ)
                   AND WALL-COD-TARI(IX-ALL) =
                      WGRZ-COD-TARI(IX-TAB-GRZ)
                   AND WALL-DT-INI-VLDT(IX-ALL) =
                      WGRZ-DT-DECOR(IX-TAB-GRZ)
                      PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
                        UNTIL IX-TAB-TGA GREATER (SF)-ELE-TRAN-MAX
                           OR LCCC0019-TARIFFA-OK
                           IF WGRZ-ID-GAR(IX-TAB-GRZ) =
                              WTGA-ID-GAR(IX-TAB-TGA)
                              IF (SF)-TP-TRCH(IX-TAB-TGA) NOT = '9'
                              AND (SF)-TP-TRCH(IX-TAB-TGA) NOT = '10'
                              AND (SF)-TP-TRCH(IX-TAB-TGA) NOT = '11'

                                 IF LCCC0019-PATTUITO-NO
                                    IF (SF)-PRSTZ-INI(IX-TAB-TGA)
                                      IS NUMERIC
                                      COMPUTE LCCC0019-IMP-DA-SCALARE =
                                              LCCC0019-IMP-DA-SCALARE +
                                             (SF)-PRSTZ-INI(IX-TAB-TGA)
                                    END-IF
                                 ELSE
                                    IF (SF)-PRE-PATTUITO(IX-TAB-TGA)
                                      IS NUMERIC
                                      COMPUTE LCCC0019-IMP-DA-SCALARE =
                                              LCCC0019-IMP-DA-SCALARE +
                                          (SF)-PRE-PATTUITO(IX-TAB-TGA)
                                    END-IF
                                 END-IF

                                 SET LCCC0019-TARIFFA-OK      TO TRUE
                              END-IF
                           END-IF
                      END-PERFORM
                   END-IF
                 END-PERFORM
               END-PERFORM
      *
               PERFORM VARYING IX-ALL FROM 1 BY 1
                UNTIL IX-ALL > WALL-ELE-ASSET-ALL-MAX

                IF WCOM-COD-FONDO(IX-FONDI) EQUAL
                   WALL-COD-FND(IX-ALL)
                   MOVE WALL-PC-RIP-AST(IX-ALL)
                     TO LCCC0019-PERC-FONDO(IX-FONDI)
                   IF WCOM-IMPORTO-OLD(IX-FONDI) IS NUMERIC
                      MOVE WCOM-IMPORTO-OLD(IX-FONDI)
                        TO LCCC0019-IMP-DA-SCALARE-OLD
                   ELSE
                      MOVE ZERO
                        TO LCCC0019-IMP-DA-SCALARE-OLD
                   END-IF
                END-IF

               END-PERFORM
           END-PERFORM.

           CALL LCCS0019 USING AREA-IDSV0001
                               AREA-LCCC0019
           ON EXCEPTION
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'VERIFICA PLATFOND'       TO CALL-DESC
              MOVE 'LCCP0019'    TO IEAI9901-LABEL-ERR
              PERFORM S0290-ERRORE-DI-SISTEMA
                 THRU EX-S0290
           END-CALL.
      *
       VERIFICA-PLATFOND-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  verifica tipo importo da utilizzare                           *
      *----------------------------------------------------------------*
       VERIFICA-POG-PLTFOND.
      *
           SET LCCC0019-PATTUITO-NO          TO TRUE.
      *
           PERFORM VARYING IX-TAB-POG FROM 1 BY 1
             UNTIL IX-TAB-POG GREATER WPOG-ELE-PARAM-OGG-MAX
                OR LCCC0019-PATTUITO-SI

                IF WPOG-COD-PARAM(IX-TAB-POG) = 'TIPOABBPLAF'
                   IF WPOG-VAL-NUM(IX-TAB-POG) = 1
                      SET LCCC0019-PATTUITO-SI          TO TRUE
                   END-IF
                END-IF

           END-PERFORM.
      *
       VERIFICA-POG-PLTFOND-EX.
           EXIT.

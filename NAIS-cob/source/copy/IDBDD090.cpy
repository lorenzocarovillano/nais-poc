           EXEC SQL DECLARE PARAM_INFR_APPL TABLE
           (
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             AMBIENTE            CHAR(10) NOT NULL,
             PIATTAFORMA         CHAR(10) NOT NULL,
             TP_COM_COBOL_JAVA   CHAR(10) NOT NULL,
             MQ_TP_UTILIZZO_API  CHAR(3),
             MQ_QUEUE_MANAGER    CHAR(10),
             MQ_CODA_PUT         CHAR(10),
             MQ_CODA_GET         CHAR(10),
             MQ_OPZ_PERSISTENZA  CHAR(1),
             MQ_OPZ_WAIT         CHAR(1),
             MQ_OPZ_SYNCPOINT    CHAR(1),
             MQ_ATTESA_RISPOSTA  CHAR(1),
             MQ_TEMPO_ATTESA_1   DECIMAL(10, 0),
             MQ_TEMPO_ATTESA_2   DECIMAL(10, 0),
             MQ_TEMPO_EXPIRY     DECIMAL(10, 0),
             CSOCKET_IP_ADDRESS  CHAR(20),
             CSOCKET_PORT_NUM    DECIMAL(5, 0),
             FL_COMPRESSORE_C    CHAR(1)
          ) END-EXEC.

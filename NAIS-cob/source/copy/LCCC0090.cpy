      *----------------------------------------------------------------*
      *   PROCESSO DI VENDITA - PORTAFOGLIO VITA
      *   AREA INPUT/OUTPUT
      *   SERVIZIO ESTRAZIONE SEQUENCE
      *   LUNG.
      *----------------------------------------------------------------*
           05 (SF)-DATI-INPUT.
              10 (SF)-CONNECT                   PIC X(01) VALUE 'S'.
              10 (SF)-NOME-TABELLA              PIC X(020).

           05 (SF)-DATI-OUTPUT.
              10 (SF)-SEQ-TABELLA               PIC S9(09) COMP-3.
              10 (SF)-RETURN-CODE               PIC X(002).
                 88 (SF)-SUCCESSFUL-RC          VALUE '00'.
                 88 (SF)-SEQUENCE-NOT-FOUND     VALUE 'S1'.
                 88 (SF)-SQL-ERROR              VALUE 'D3'.

              10 (SF)-SQLCODE                   PIC S9(09).
                 88 (SF)-SUCCESSFUL-SQL    VALUE ZERO.
                 88 (SF)-NOT-FOUND         VALUE +100.
                 88 (SF)-DUPLICATE-KEY     VALUE -803.
                 88 (SF)-MORE-THAN-ONE-ROW VALUE -811.
                 88 (SF)-DEADLOCK-TIMEOUT  VALUE -911
                                                 -913.
                 88 (SF)-CONNECTION-ERROR  VALUE -924.

              10 (SF)-DESCRIZ-ERR-DB2           PIC X(300).

      *----------------------------------------------------------------*
      * DIMENSIONAMENTO OCCURS PER LA TAB. DETT_TIT_CONT               *
      * COPY RIPORTANTE IL NUMERO DI OCCURS DA UTILIZZARE              *
      * PER INIZIALIZZA TOT DELLA TABELLA                              *
      *----------------------------------------------------------------*
       01 WK-DTC-MAX.
          03 WK-DTC-MAX-A                 PIC 9(04) VALUE 100.
          03 WK-DTC-MAX-B                 PIC 9(04) VALUE 400.
          03 WK-DTC-MAX-C                 PIC 9(04) VALUE 1400.

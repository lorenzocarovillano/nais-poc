           EXEC SQL DECLARE BTC_BATCH_TYPE TABLE
           (
             COD_BATCH_TYPE      INTEGER NOT NULL,
             DES                 VARCHAR(100) NOT NULL,
             DEF_CONTENT_TYPE    CHAR(30),
             COMMIT_FREQUENCY    INTEGER,
             SERVICE_NAME        CHAR(50),
             SERVICE_GUIDE_NAME  CHAR(50),
             SERVICE_ALPO_NAME   CHAR(50),
             COD_MACROFUNCT      CHAR(2),
             TP_MOVI             DECIMAL(5, 0)
          ) END-EXEC.

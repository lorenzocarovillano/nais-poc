       01 COMP-NUM-OGG.
         05 CNO-COD-COMPAGNIA-ANIA PIC S9(5)V     COMP-3.
         05 CNO-FORMA-ASSICURATIVA PIC X(2).
         05 CNO-COD-OGGETTO PIC X(30).
         05 CNO-POSIZIONE PIC S9(5)V     COMP-3.
         05 CNO-COD-STR-DATO PIC X(30).
         05 CNO-COD-STR-DATO-NULL REDEFINES
            CNO-COD-STR-DATO   PIC X(30).
         05 CNO-COD-DATO PIC X(30).
         05 CNO-COD-DATO-NULL REDEFINES
            CNO-COD-DATO   PIC X(30).
         05 CNO-VALORE-DEFAULT PIC X(50).
         05 CNO-VALORE-DEFAULT-NULL REDEFINES
            CNO-VALORE-DEFAULT   PIC X(50).
         05 CNO-LUNGHEZZA-DATO PIC S9(5)V     COMP-3.
         05 CNO-LUNGHEZZA-DATO-NULL REDEFINES
            CNO-LUNGHEZZA-DATO   PIC X(3).
         05 CNO-FLAG-KEY-ULT-PROGR PIC X(1).
         05 CNO-FLAG-KEY-ULT-PROGR-NULL REDEFINES
            CNO-FLAG-KEY-ULT-PROGR   PIC X(1).


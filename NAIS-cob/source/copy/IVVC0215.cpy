      *--> Gestione output variabili variabili
           05 (SF)-AREA-VAR-KO.

             10 (SF)-NOT-FOUND-MVV.
                15 (SF)-ELE-MAX-NOT-FOUND      PIC S9(04) COMP-3.
                15 (SF)-AREA-VAR-NOT-FOUND.
                   17 (SF)-TAB-VAR-NOT-FOUND      OCCURS 5.
                      20 (SF)-VAR-NOT-FOUND       PIC X(019).
                      20 (SF)-VAR-NOT-FOUND-SP    PIC X(01).

             10 (SF)-CALCOLO-KO.
                15 (SF)-ELE-MAX-CALC-KO        PIC S9(04) COMP-3.
                15 (SF)-AREA-CALC-KO.
                   17 (SF)-TAB-CALCOLO-KO         OCCURS 5.
                      20 (SF)-VAR-CALCOLO-KO      PIC X(019).
                      20 (SF)-VAR-CALC-SP         PIC X(01).

           05 (SF)-ERRORE-VALORIZZA            PIC  X(002).
             88 (SF)-SUCCESSFUL-RC               VALUE '00'.
             88 (SF)-VARIABILE-KO                VALUE 'CA'.

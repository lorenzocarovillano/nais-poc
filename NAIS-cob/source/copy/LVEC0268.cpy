      *-----------------------------------------------------------------*
      *   AREA EMISSIONE CONTRATTO INDIVIDUALE - CALCOLI
      *                              AREA DI PAGINA
      *   PORTAFOGLIO VITA - PROCESSO VENDITA
      *   LUNG.
      *-----------------------------------------------------------------*
      * 03 20/01/2009 10.18.
       04  (SF)-DATI-INPUT.
           05 (SF)-ELE-MAX-GARNZIE                         PIC 9(02).
           05 (SF)-TAB-GARANZIE OCCURS 20.
              07 (SF)-COD-GARANZIA-I                       PIC X(012).
              07 (SF)-FL-GAR-INCLUSA-I                     PIC X(001).
                 88 (SF)-GAR-INCLUSA-SI                      VALUE 'S'.
                 88 (SF)-GAR-INCLUSA-NO                      VALUE 'N'.
       04  (SF)-DATI-OUPUT.
      *--  Tabella Asset per fondi
           05 (SF)-ELE-ASSET-MAX                PIC S9(04)      COMP.
           05 (SF)-TAB-ASSET            OCCURS 250.
               07 (SF)-COD-FONDO                PIC X(20).
               07 (SF)-TP-FND                   PIC X(1).
               07 (SF)-PERC-FONDO               PIC S9(3)V9(3)  COMP-3.
               07 (SF)-PERC-FONDO-NULL REDEFINES
                  (SF)-PERC-FONDO               PIC X(04).
               07 (SF)-COD-TARI                 PIC X(12).
               07 (SF)-COD-TARI-NULL   REDEFINES
                  (SF)-COD-TARI                 PIC X(12).
      *--  Parametro questionario
           05 (SF)-PARAM-QUESTSAN               PIC X(001).
              88 (SF)-FL-PARAM-QUESTSAN-SI        VALUE 'S'.
              88 (SF)-FL-PARAM-QUESTSAN-NO        VALUE 'N'.
              88 (SF)-FL-VISITA-MEDICA-OBB        VALUE 'P'.
      *--  Parametro questionario SPECIALE
           05 (SF)-PARAM-QUESTSAN-SPECIALE      PIC X(001).
              88 (SF)-FL-QUESTSAN-SPECIALE-SI      VALUE 'S'.
              88 (SF)-FL-QUESTSAN-SPECIALE-NO      VALUE 'N'.
      *--  Numero di rate accorpate
           05 (SF)-NUM-RATE-ACC          PIC S9(04)      COMP-3.
           05 (SF)-NUM-RATE-ACC-NULL        REDEFINES
              (SF)-NUM-RATE-ACC          PIC  X(03).
      *--  Numero di rate retrodatate
           05 (SF)-RATE-RETRODATATE      PIC S9(04)      COMP-3.
           05 (SF)-RATE-RETRODATATE-NULL    REDEFINES
              (SF)-RATE-RETRODATATE      PIC  X(03).
      *--  Tabella mappatura componente --> Prodotto
           05 (SF)-DATI-PRODOTTO.
      *--     Dati Adesione
              07 (SF)-DATI-ADESIONE.
                 09 (SF)-TP-IAS-ADE   PIC X(2).
                 09 (SF)-TP-IAS-ADE-NULL REDEFINES
                    (SF)-TP-IAS-ADE   PIC X(2).
11141         07 (SF)-TP-PROV.
11141            09 (SF)-TP-MOD-PROV   PIC X(1).
11141            09 (SF)-TP-MOD-PROV-NULL REDEFINES
11141               (SF)-TP-MOD-PROV   PIC X(1).
      *--  Tabella mappatura componente --> Garanzia
           05 (SF)-ELE-CALCOLI-MAX              PIC S9(04)      COMP.
           05 (SF)-TAB-CALCOLI          OCCURS 20.
               07 (SF)-COD-GARANZIA             PIC  X(12).
               07 (SF)-IMP-MOVI                 PIC S9(12)V9(3) COMP-3.
               07 (SF)-IMP-MOVI-NULL REDEFINES
                  (SF)-IMP-MOVI                 PIC X(08).
               07 (SF)-IMP-MOVI-NEG             PIC S9(12)V9(3) COMP-3.
               07 (SF)-IMP-MOVI-NEG-NULL REDEFINES
                  (SF)-IMP-MOVI-NEG             PIC X(08).
               07 (SF)-AA-DIFF-PROR             PIC 9(2).
               07 (SF)-AA-DIFF-PROR-NULL        REDEFINES
                  (SF)-AA-DIFF-PROR             PIC X(2).
               07 (SF)-MM-DIFF-PROR             PIC 9(2).
               07 (SF)-MM-DIFF-PROR-NULL        REDEFINES
                  (SF)-MM-DIFF-PROR             PIC X(2).
      *--      Dati di Tranche
               07 (SF)-ELE-TRANCHE-MAX          PIC S9(04)      COMP.
               07 (SF)-DATI-TRANCHE     OCCURS 2.
                   09 (SF)-TP-RGM-FISC          PIC X(2).
                   09 (SF)-TP-TRCH              PIC X(2).
                   09 (SF)-TP-TRCH-NULL            REDEFINES
                      (SF)-TP-TRCH              PIC X(2).
                   09 (SF)-PRE-CASO-MOR         PIC S9(12)V9(3) COMP-3.
                   09 (SF)-PRE-CASO-MOR-NULL       REDEFINES
                      (SF)-PRE-CASO-MOR         PIC  X(08).
                   09 (SF)-IMP-BNS-ANTIC        PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-BNS-ANTIC-NULL      REDEFINES
                      (SF)-IMP-BNS-ANTIC        PIC  X(08).
                   09 (SF)-PRE-INI-NET          PIC S9(12)V9(3) COMP-3.
                   09 (SF)-PRE-INI-NET-NULL        REDEFINES
                      (SF)-PRE-INI-NET          PIC  X(08).
                   09 (SF)-PRE-PP-INI           PIC S9(12)V9(3) COMP-3.
                   09 (SF)-PRE-PP-INI-NULL         REDEFINES
                      (SF)-PRE-PP-INI           PIC  X(08).
                   09 (SF)-PRE-PP-ULT           PIC S9(12)V9(3) COMP-3.
                   09 (SF)-PRE-PP-ULT-NULL         REDEFINES
                      (SF)-PRE-PP-ULT           PIC  X(08).
                   09 (SF)-PRE-TARI-INI         PIC S9(12)V9(3) COMP-3.
                   09 (SF)-PRE-TARI-INI-NULL       REDEFINES
                      (SF)-PRE-TARI-INI         PIC  X(08).
                   09 (SF)-PRE-TARI-ULT         PIC S9(12)V9(3) COMP-3.
                   09 (SF)-PRE-TARI-ULT-NULL       REDEFINES
                      (SF)-PRE-TARI-ULT         PIC  X(08).
                   09 (SF)-PRE-INVRIO-INI       PIC S9(12)V9(3) COMP-3.
                   09 (SF)-PRE-INVRIO-INI-NULL     REDEFINES
                      (SF)-PRE-INVRIO-INI       PIC  X(08).
                   09 (SF)-PRE-INVRIO-ULT       PIC S9(12)V9(3) COMP-3.
                   09 (SF)-PRE-INVRIO-ULT-NULL     REDEFINES
                      (SF)-PRE-INVRIO-ULT       PIC  X(08).
                   09 (SF)-IMP-SOPR-PROF        PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-SOPR-PROF-NULL      REDEFINES
                      (SF)-IMP-SOPR-PROF        PIC  X(08).
                   09 (SF)-IMP-SOPR-SAN         PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-SOPR-SAN-NULL       REDEFINES
                      (SF)-IMP-SOPR-SAN         PIC  X(08).
                   09 (SF)-IMP-SOPR-SPO         PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-SOPR-SPO-NULL       REDEFINES
                      (SF)-IMP-SOPR-SPO         PIC  X(08).
                   09 (SF)-IMP-SOPR-TEC         PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-SOPR-TEC-NULL       REDEFINES
                      (SF)-IMP-SOPR-TEC         PIC  X(08).
                   09 (SF)-IMP-ALT-SOPR         PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-ALT-SOPR-NULL       REDEFINES
                      (SF)-IMP-ALT-SOPR         PIC  X(08).
                   09 (SF)-TS-RIVAL-FIS         PIC S9(3)V9(3) COMP-3.
                   09 (SF)-TS-RIVAL-FIS-NULL       REDEFINES
                      (SF)-TS-RIVAL-FIS         PIC X(4).
                   09 (SF)-RAT-LRD              PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RAT-LRD-NULL            REDEFINES
                      (SF)-RAT-LRD              PIC  X(08).
                   09 (SF)-PRE-LRD              PIC S9(12)V9(3) COMP-3.
                   09 (SF)-PRE-LRD-NULL            REDEFINES
                      (SF)-PRE-LRD              PIC  X(08).
                   09 (SF)-PRSTZ-INI            PIC S9(12)V9(3) COMP-3.
                   09 (SF)-PRSTZ-INI-NULL          REDEFINES
                      (SF)-PRSTZ-INI            PIC  X(08).
                   09 (SF)-PRSTZ-ULT            PIC S9(12)V9(3) COMP-3.
                   09 (SF)-PRSTZ-ULT-NULL          REDEFINES
                      (SF)-PRSTZ-ULT            PIC  X(08).
                   09 (SF)-CPT-IN-OPZ-RIVTO     PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CPT-IN-OPZ-RIVTO-NULL   REDEFINES
                      (SF)-CPT-IN-OPZ-RIVTO     PIC  X(08).
                   09 (SF)-CPT-RSH-MOR          PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CPT-RSH-MOR-NULL        REDEFINES
                      (SF)-CPT-RSH-MOR          PIC  X(08).
                   09 (SF)-IMP-SCON             PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-SCON-NULL           REDEFINES
                      (SF)-IMP-SCON             PIC  X(08).
                   09 (SF)-ALQ-SCON             PIC S9(3)V9(3) COMP-3.
                   09 (SF)-ALQ-SCON-NULL           REDEFINES
                      (SF)-ALQ-SCON             PIC  X(4).
                   09 (SF)-IMP-CAR-ACQ-TGA      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-CAR-ACQ-TGA-NULL    REDEFINES
                      (SF)-IMP-CAR-ACQ-TGA      PIC  X(08).
                   09 (SF)-IMP-CAR-INC-TGA      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-CAR-INC-TGA-NULL    REDEFINES
                      (SF)-IMP-CAR-INC-TGA      PIC  X(08).
                   09 (SF)-IMP-CAR-GEST-TGA     PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-CAR-GEST-TGA-NULL   REDEFINES
                      (SF)-IMP-CAR-GEST-TGA     PIC  X(08).
                   09 (SF)-IMP-AZ-TGA           PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-AZ-TGA-NULL         REDEFINES
                      (SF)-IMP-AZ-TGA           PIC X(8).
                   09 (SF)-IMP-ADER-TGA         PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-ADER-TGA-NULL       REDEFINES
                      (SF)-IMP-ADER-TGA         PIC X(8).
                   09 (SF)-IMP-TFR-TGA          PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-TFR-TGA-NULL        REDEFINES
                      (SF)-IMP-TFR-TGA          PIC X(8).
                   09 (SF)-IMP-VOLO-TGA         PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-VOLO-TGA-NULL       REDEFINES
                      (SF)-IMP-VOLO-TGA         PIC X(8).
                   09 (SF)-IMP-ABB-ANNO-ULT     PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-ABB-ANNO-ULT-NULL REDEFINES
                      (SF)-IMP-ABB-ANNO-ULT     PIC X(8).
                   09 (SF)-IMP-ABB-TOT-INI      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-ABB-TOT-INI-NULL  REDEFINES
                      (SF)-IMP-ABB-TOT-INI      PIC X(8).
                   09 (SF)-DUR-ABB-ANNI         PIC S9(06) COMP-3.
                   09 (SF)-DUR-ABB-ANNI-NULL     REDEFINES
                      (SF)-DUR-ABB-ANNI         PIC X(4).
                   09 (SF)-IMP-PRE-PATTUITO     PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-PRE-PATTUITO-NULL  REDEFINES
                      (SF)-IMP-PRE-PATTUITO     PIC X(8).
                   09 (SF)-IMP-PREST-ULT        PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-PREST-ULT-NULL    REDEFINES
                      (SF)-IMP-PREST-ULT        PIC X(8).
                   09 (SF)-IMP-PRE-RIVAL        PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-PRE-RIVAL-NULL    REDEFINES
                      (SF)-IMP-PRE-RIVAL        PIC X(8).
                   09 (SF)-TGA-ALIQ-INCAS       PIC S9(05)V9(9) COMP-3.
                   09 (SF)-TGA-ALIQ-INCAS-NULL   REDEFINES
                      (SF)-TGA-ALIQ-INCAS       PIC  X(08).
                   09 (SF)-TGA-ALIQ-ACQ         PIC S9(05)V9(9) COMP-3.
                   09 (SF)-TGA-ALIQ-ACQ-NULL     REDEFINES
                      (SF)-TGA-ALIQ-ACQ         PIC  X(08).
                   09 (SF)-TGA-ALIQ-RICOR       PIC S9(05)V9(9) COMP-3.
                   09 (SF)-TGA-ALIQ-RICOR-NULL   REDEFINES
                      (SF)-TGA-ALIQ-RICOR       PIC  X(08).
                   09 (SF)-TGA-IMP-INCAS        PIC S9(12)V9(3) COMP-3.
                   09 (SF)-TGA-IMP-INCAS-NULL    REDEFINES
                      (SF)-TGA-IMP-INCAS        PIC  X(08).
                   09 (SF)-TGA-IMP-ACQ          PIC S9(12)V9(3) COMP-3.
                   09 (SF)-TGA-IMP-ACQ-NULL      REDEFINES
                      (SF)-TGA-IMP-ACQ          PIC  X(08).
                   09 (SF)-TGA-IMP-RICOR        PIC S9(12)V9(3) COMP-3.
                   09 (SF)-TGA-IMP-RICOR-NULL    REDEFINES
                      (SF)-TGA-IMP-RICOR        PIC  X(08).
                   09 (SF)-IMP-PRSTZ-AGG-INI    PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-PRSTZ-AGG-INI-NULL REDEFINES
                      (SF)-IMP-PRSTZ-AGG-INI    PIC  X(08).
                   09 (SF)-FL-PREL-RIS          PIC X(02).
                   09 (SF)-FL-RICALCOLO         PIC X(02).
                   09 (SF)-MANFEE-ANTIC         PIC S9(12)V9(3) COMP-3.
                   09 (SF)-MANFEE-ANTIC-NULL     REDEFINES
                      (SF)-MANFEE-ANTIC         PIC X(8).
                   09 (SF)-TGA-INT-FRAZ         PIC S9(12)V9(3) COMP-3.
                   09 (SF)-TGA-INT-FRAZ-NULL     REDEFINES
                      (SF)-TGA-INT-FRAZ         PIC  X(08).
                   09 (SF)-REN-INI-TS-TEC-0     PIC S9(12)V9(3) COMP-3.
                   09 (SF)-REN-INI-TS-TEC-0-NULL REDEFINES
                      (SF)-REN-INI-TS-TEC-0     PIC X(8).
ITRAT              09 (SF)-TGA-IMP-ACQ-EXP      PIC S9(12)V9(3) COMP-3.
ITRAT              09 (SF)-TGA-IMP-ACQ-EXP-NULL  REDEFINES
ITRAT                 (SF)-TGA-IMP-ACQ-EXP      PIC  X(08).
ITRAT              09 (SF)-TGA-IMP-REN-ASS      PIC S9(12)V9(3) COMP-3.
ITRAT              09 (SF)-TGA-IMP-REN-ASS-NULL  REDEFINES
ITRAT                 (SF)-TGA-IMP-REN-ASS      PIC  X(08).
ITRAT              09 (SF)-TGA-IMP-COMMIS-INT   PIC S9(12)V9(3) COMP-3.
ITRAT              09 (SF)-TGA-IMP-COMMIS-INT-NULL   REDEFINES
ITRAT                 (SF)-TGA-IMP-COMMIS-INT   PIC  X(08).
ITRAT              09 (SF)-TGA-ALIQ-REN-ASS     PIC S9(05)V9(9) COMP-3.
ITRAT              09 (SF)-TGA-ALIQ-REN-ASS-NULL REDEFINES
ITRAT                 (SF)-TGA-ALIQ-REN-ASS     PIC  X(08).
ITRAT              09 (SF)-TGA-ALIQ-COMMIS-INT  PIC S9(05)V9(9) COMP-3.
ITRAT              09 (SF)-TGA-ALIQ-COMMIS-INT-NULL REDEFINES
ITRAT                 (SF)-TGA-ALIQ-COMMIS-INT  PIC  X(08).
ITRAT              09 (SF)-TGA-IMPB-REN-ASS     PIC S9(12)V9(3) COMP-3.
ITRAT              09 (SF)-TGA-IMPB-REN-ASS-NULL REDEFINES
ITRAT                 (SF)-TGA-IMPB-REN-ASS     PIC  X(08).
ITRAT              09 (SF)-TGA-IMPB-COMMIS-INT  PIC S9(12)V9(3) COMP-3.
ITRAT              09 (SF)-TGA-IMPB-COMMIS-INT-NULL REDEFINES
ITRAT                 (SF)-TGA-IMPB-COMMIS-INT  PIC  X(08).
               07 (SF)-ELE-SOPR-GAR-MAX         PIC S9(04)      COMP.
               07 (SF)-DATI-SOPR-GAR         OCCURS 20.
                   09 (SF)-COD-SOVRAP           PIC X(02).
                   09 (SF)-PERC-SOVRAP          PIC S9(3)V9(3)  COMP-3.
                   09 (SF)-PERC-SOVRAP-NULL        REDEFINES
                      (SF)-PERC-SOVRAP          PIC X(4).
                   09 (SF)-IMP-SOVRAP           PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-SOVRAP-NULL        REDEFINES
                      (SF)-IMP-SOVRAP           PIC X(8).
                   09 (SF)-SOVRAM               PIC S9(03)V9(03) COMP-3.
                   09 (SF)-SOVRAM-NULL             REDEFINES
                      (SF)-SOVRAM               PIC X(4).
               07 (SF)-ELE-PAR-CALC-MAX         PIC S9(04)      COMP.
      *---     Parametri di calcolo
               07 (SF)-DATI-PAR-CALC         OCCURS 20.
                   09 (SF)-COD-PARAM            PIC  X(20).
                   09 (SF)-TP-D                 PIC  X(02).
                   09 (SF)-VAL-DT               PIC S9(08)      COMP-3.
                   09 (SF)-VAL-DT-NULL             REDEFINES
                      (SF)-VAL-DT               PIC  X(05).
                   09 (SF)-VAL-IMP              PIC S9(12)V9(3) COMP-3.
                   09 (SF)-VAL-IMP-NULL            REDEFINES
                      (SF)-VAL-IMP              PIC  X(08).
                   09 (SF)-VAL-TS               PIC S9(05)V9(9) COMP-3.
                   09 (SF)-VAL-TS-NULL             REDEFINES
                      (SF)-VAL-TS               PIC  X(08).
                   09 (SF)-VAL-NUM              PIC S9(05)      COMP-3.
                   09 (SF)-VAL-NUM-NULL            REDEFINES
                      (SF)-VAL-NUM              PIC  X(03).
                   09 (SF)-VAL-PC               PIC S9(03)V9(3) COMP-3.
                   09 (SF)-VAL-PC-NULL             REDEFINES
                      (SF)-VAL-PC               PIC  X(04).
                   09 (SF)-VAL-STR              PIC  X(12).
                   09 (SF)-VAL-FL               PIC  X(01).
               07 (SF)-DATI-PROVVIG.
                   09 (SF)-PRV-ACQ-1O-ANNO      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-PRV-ACQ-1O-ANNO-NULL    REDEFINES
                      (SF)-PRV-ACQ-1O-ANNO      PIC  X(08).
                   09 (SF)-PRV-ACQ-2O-ANNO      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-PRV-ACQ-2O-ANNO-NULL    REDEFINES
                      (SF)-PRV-ACQ-2O-ANNO      PIC  X(08).
                   09 (SF)-PRV-RICOR            PIC S9(12)V9(3) COMP-3.
                   09 (SF)-PRV-RICOR-NULL          REDEFINES
                      (SF)-PRV-RICOR            PIC  X(08).
                   09 (SF)-PRV-INCAS            PIC S9(12)V9(3) COMP-3.
                   09 (SF)-PRV-INCAS-NULL          REDEFINES
                      (SF)-PRV-INCAS            PIC  X(08).
ITRAT              09 (SF)-PRV-REN-ASS          PIC S9(12)V9(3) COMP-3.
ITRAT              09 (SF)-PRV-REN-ASS-NULL        REDEFINES
ITRAT                 (SF)-PRV-REN-ASS          PIC  X(08).
ITRAT              09 (SF)-PRV-COMMIS-INT       PIC S9(12)V9(3) COMP-3.
ITRAT              09 (SF)-PRV-COMMIS-INT-NULL     REDEFINES
ITRAT                 (SF)-PRV-COMMIS-INT       PIC  X(08).
                   09 (SF)-ALIQ-INCAS           PIC S9(05)V9(9) COMP-3.
                   09 (SF)-ALIQ-INCAS-NULL         REDEFINES
                      (SF)-ALIQ-INCAS           PIC  X(08).
                   09 (SF)-ALIQ-ACQ             PIC S9(05)V9(9) COMP-3.
                   09 (SF)-ALIQ-ACQ-NULL           REDEFINES
                      (SF)-ALIQ-ACQ             PIC  X(08).
                   09 (SF)-ALIQ-RICOR           PIC S9(05)V9(9) COMP-3.
                   09 (SF)-ALIQ-RICOR-NULL         REDEFINES
                      (SF)-ALIQ-RICOR           PIC  X(08).
ITRAT              09 (SF)-ALIQ-REN-ASS         PIC S9(05)V9(9) COMP-3.
ITRAT              09 (SF)-ALIQ-REN-ASS-NULL       REDEFINES
ITRAT                 (SF)-ALIQ-REN-ASS         PIC  X(08).
ITRAT              09 (SF)-ALIQ-COMMIS-INT      PIC S9(05)V9(9) COMP-3.
ITRAT              09 (SF)-ALIQ-COMMIS-INT-NULL    REDEFINES
ITRAT                 (SF)-ALIQ-COMMIS-INT      PIC  X(08).
                   09 (SF)-IMP-INCAS            PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-INCAS-NULL          REDEFINES
                      (SF)-IMP-INCAS            PIC  X(08).
                   09 (SF)-IMP-ACQ              PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-ACQ-NULL            REDEFINES
                      (SF)-IMP-ACQ              PIC  X(08).
                   09 (SF)-IMP-RICOR            PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-RICOR-NULL          REDEFINES
                      (SF)-IMP-RICOR            PIC  X(08).
ITRAT              09 (SF)-IMP-REN-ASS          PIC S9(12)V9(3) COMP-3.
ITRAT              09 (SF)-IMP-REN-ASS-NULL        REDEFINES
ITRAT                 (SF)-IMP-REN-ASS          PIC  X(08).
ITRAT              09 (SF)-IMP-COMMIS-INT       PIC S9(12)V9(3) COMP-3.
ITRAT              09 (SF)-IMP-COMMIS-INT-NULL     REDEFINES
ITRAT                 (SF)-IMP-COMMIS-INT       PIC  X(08).
               07 (SF)-ELE-TIT-CONT-MAX         PIC S9(04)      COMP.
               07 (SF)-DATI-TIT-CONT         OCCURS 12.
                   09 (SF)-CONT-PREMIO-NETTO     PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-PREMIO-NETTO-NULL   REDEFINES
                      (SF)-CONT-PREMIO-NETTO     PIC  X(08).
                   09 (SF)-CONT-INT-FRAZ         PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-INT-FRAZ-NULL       REDEFINES
                      (SF)-CONT-INT-FRAZ         PIC  X(08).
                   09 (SF)-CONT-INT-RETRODT      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-INT-RETRODT-NULL    REDEFINES
                      (SF)-CONT-INT-RETRODT      PIC  X(08).
                   09 (SF)-CONT-DIRITTI          PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-DIRITTI-NULL        REDEFINES
                      (SF)-CONT-DIRITTI          PIC  X(08).
                   09 (SF)-CONT-SPESE-MEDICHE    PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-SPESE-MEDICHE-NULL  REDEFINES
                      (SF)-CONT-SPESE-MEDICHE    PIC  X(08).
                   09 (SF)-CONT-TASSE            PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-TASSE-NULL          REDEFINES
                      (SF)-CONT-TASSE            PIC  X(08).
                   09 (SF)-CONT-SOPR-SANIT       PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-SOPR-SANIT-NULL     REDEFINES
                      (SF)-CONT-SOPR-SANIT       PIC  X(08).
                   09 (SF)-CONT-SOPR-PROFES      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-SOPR-PROFES-NULL    REDEFINES
                      (SF)-CONT-SOPR-PROFES      PIC  X(08).
                   09 (SF)-CONT-SOPR-SPORT       PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-SOPR-SPORT-NULL     REDEFINES
                      (SF)-CONT-SOPR-SPORT       PIC  X(08).
                   09 (SF)-CONT-SOPR-TECN        PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-SOPR-TECN-NULL      REDEFINES
                      (SF)-CONT-SOPR-TECN        PIC  X(08).
                   09 (SF)-CONT-ALTRI-SOPR       PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-ALTRI-SOPR-NULL     REDEFINES
                      (SF)-CONT-ALTRI-SOPR       PIC  X(08).
                   09 (SF)-CONT-PREMIO-TOT       PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-PREMIO-TOT-NULL     REDEFINES
                      (SF)-CONT-PREMIO-TOT       PIC  X(08).
                   09 (SF)-CONT-PREMIO-PURO-IAS  PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-PREMIO-PURO-IAS-NULL  REDEFINES
                      (SF)-CONT-PREMIO-PURO-IAS  PIC  X(08).
                   09 (SF)-CONT-PREMIO-RISC      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-PREMIO-RISC-NULL    REDEFINES
                      (SF)-CONT-PREMIO-RISC      PIC  X(08).
                   09 (SF)-IMP-CAR-ACQ-TIT      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-CAR-ACQ-TIT-NULL     REDEFINES
                      (SF)-IMP-CAR-ACQ-TIT      PIC  X(08).
                   09 (SF)-IMP-CAR-INC-TIT      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-CAR-INC-TIT-NULL     REDEFINES
                      (SF)-IMP-CAR-INC-TIT      PIC  X(08).
                   09 (SF)-IMP-CAR-GEST-TIT     PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-CAR-GEST-TIT-NULL    REDEFINES
                      (SF)-IMP-CAR-GEST-TIT     PIC  X(08).
                   09 (SF)-CONT-ACQ-1O-ANNO      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-ACQ-1O-ANNO-NULL    REDEFINES
                      (SF)-CONT-ACQ-1O-ANNO      PIC  X(08).
                   09 (SF)-CONT-ACQ-2O-ANNO      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-ACQ-2O-ANNO-NULL    REDEFINES
                      (SF)-CONT-ACQ-2O-ANNO      PIC  X(08).
                   09 (SF)-CONT-RICOR            PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-RICOR-NULL          REDEFINES
                      (SF)-CONT-RICOR            PIC  X(08).
                   09 (SF)-CONT-INCAS            PIC S9(12)V9(3) COMP-3.
                   09 (SF)-CONT-INCAS-NULL          REDEFINES
                      (SF)-CONT-INCAS            PIC  X(08).
                   09 (SF)-IMP-AZ-TIT            PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-AZ-TIT-NULL          REDEFINES
                      (SF)-IMP-AZ-TIT            PIC X(8).
                   09 (SF)-IMP-ADER-TIT          PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-ADER-TIT-NULL        REDEFINES
                      (SF)-IMP-ADER-TIT          PIC X(8).
                   09 (SF)-IMP-TFR-TIT           PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-TFR-TIT-NULL         REDEFINES
                      (SF)-IMP-TFR-TIT           PIC X(8).
                   09 (SF)-IMP-VOLO-TIT          PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-VOLO-TIT-NULL        REDEFINES
                      (SF)-IMP-VOLO-TIT          PIC X(8).
ITRAT              09 (SF)-CONT-IMP-ACQ-EXP      PIC S9(12)V9(3) COMP-3.
ITRAT              09 (SF)-CONT-IMP-ACQ-EXP-NULL  REDEFINES
ITRAT                 (SF)-CONT-IMP-ACQ-EXP      PIC X(8).
ITRAT              09 (SF)-CONT-IMP-REN-ASS      PIC S9(12)V9(3) COMP-3.
ITRAT              09 (SF)-CONT-IMP-REN-ASS-NULL  REDEFINES
ITRAT                 (SF)-CONT-IMP-REN-ASS      PIC X(8).
ITRAT              09 (SF)-CONT-IMP-COMMIS-INT   PIC S9(12)V9(3) COMP-3.
ITRAT              09 (SF)-CONT-IMP-COMMIS-INT-NULL    REDEFINES
ITRAT                 (SF)-CONT-IMP-COMMIS-INT   PIC X(8).
11140              09 (SF)-CONT-ANTIRACKET       PIC S9(12)V9(3) COMP-3.
11140              09 (SF)-CONT-ANTIRACKET-NULL  REDEFINES
11140                 (SF)-CONT-ANTIRACKET       PIC X(8).
11174              09 (SF)-CONT-AUTOGEN-INC      PIC X(01).
11174              09 (SF)-CONT-AUTOGEN-INC-NULL REDEFINES
11174                 (SF)-CONT-AUTOGEN-INC      PIC X(1).
               07 (SF)-ELE-TIT-RATA-MAX          PIC S9(04)      COMP.
               07 (SF)-DATI-TIT-RATA          OCCURS 12.
                   09 (SF)-RATA-PREMIO-NETTO     PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-PREMIO-NETTO-NULL   REDEFINES
                      (SF)-RATA-PREMIO-NETTO     PIC  X(08).
                   09 (SF)-RATA-INT-FRAZ         PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-INT-FRAZ-NULL       REDEFINES
                      (SF)-RATA-INT-FRAZ         PIC  X(08).
                   09 (SF)-RATA-INT-RETRODT      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-INT-RETRODT-NULL    REDEFINES
                      (SF)-RATA-INT-RETRODT      PIC  X(08).
                   09 (SF)-RATA-DIRITTI          PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-DIRITTI-NULL        REDEFINES
                      (SF)-RATA-DIRITTI          PIC  X(08).
                   09 (SF)-RATA-SPESE-MEDICHE    PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-SPESE-MEDICHE-NULL  REDEFINES
                      (SF)-RATA-SPESE-MEDICHE    PIC  X(08).
                   09 (SF)-RATA-TASSE            PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-TASSE-NULL          REDEFINES
                      (SF)-RATA-TASSE            PIC  X(08).
                   09 (SF)-RATA-SOPR-SANIT       PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-SOPR-SANIT-NULL     REDEFINES
                      (SF)-RATA-SOPR-SANIT       PIC  X(08).
                   09 (SF)-RATA-SOPR-PROFES      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-SOPR-PROFES-NULL    REDEFINES
                      (SF)-RATA-SOPR-PROFES      PIC  X(08).
                   09 (SF)-RATA-SOPR-SPORT       PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-SOPR-SPORT-NULL     REDEFINES
                      (SF)-RATA-SOPR-SPORT       PIC  X(08).
                   09 (SF)-RATA-SOPR-TECN        PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-SOPR-TECN-NULL      REDEFINES
                      (SF)-RATA-SOPR-TECN        PIC  X(08).
                   09 (SF)-RATA-ALTRI-SOPR       PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-ALTRI-SOPR-NULL     REDEFINES
                      (SF)-RATA-ALTRI-SOPR       PIC  X(08).
                   09 (SF)-RATA-PREMIO-TOT       PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-PREMIO-TOT-NULL     REDEFINES
                      (SF)-RATA-PREMIO-TOT       PIC  X(08).
                   09 (SF)-RATA-PREMIO-PURO-IAS  PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-PREMIO-PURO-IAS-NULL  REDEFINES
                      (SF)-RATA-PREMIO-PURO-IAS  PIC  X(08).
                   09 (SF)-RATA-PREMIO-RISC      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-PREMIO-RISC-NULL    REDEFINES
                      (SF)-RATA-PREMIO-RISC      PIC  X(08).
                   09 (SF)-IMP-CAR-ACQ-TDR      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-CAR-ACQ-TDR-NULL REDEFINES
                      (SF)-IMP-CAR-ACQ-TDR      PIC  X(08).
                   09 (SF)-IMP-CAR-INC-TDR      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-CAR-INC-TDR-NULL REDEFINES
                      (SF)-IMP-CAR-INC-TDR      PIC  X(08).
                   09 (SF)-IMP-CAR-GEST-TDR     PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-CAR-GEST-TDR-NULL REDEFINES
                      (SF)-IMP-CAR-GEST-TDR     PIC  X(08).
                   09 (SF)-RATA-ACQ-1O-ANNO      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-ACQ-1O-ANNO-NULL    REDEFINES
                      (SF)-RATA-ACQ-1O-ANNO      PIC  X(08).
                   09 (SF)-RATA-ACQ-2O-ANNO      PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-ACQ-2O-ANNO-NULL    REDEFINES
                      (SF)-RATA-ACQ-2O-ANNO      PIC  X(08).
                   09 (SF)-RATA-RICOR            PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-RICOR-NULL          REDEFINES
                      (SF)-RATA-RICOR            PIC  X(08).
                   09 (SF)-RATA-INCAS            PIC S9(12)V9(3) COMP-3.
                   09 (SF)-RATA-INCAS-NULL          REDEFINES
                      (SF)-RATA-INCAS            PIC  X(08).
                   09 (SF)-IMP-AZ-TDR            PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-AZ-TDR-NULL       REDEFINES
                      (SF)-IMP-AZ-TDR            PIC X(8).
                   09 (SF)-IMP-ADER-TDR          PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-ADER-TDR-NULL     REDEFINES
                      (SF)-IMP-ADER-TDR          PIC X(8).
                   09 (SF)-IMP-TFR-TDR           PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-TFR-TDR-NULL      REDEFINES
                      (SF)-IMP-TFR-TDR           PIC X(8).
                   09 (SF)-IMP-VOLO-TDR          PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-VOLO-TDR-NULL     REDEFINES
                      (SF)-IMP-VOLO-TDR          PIC X(8).
                   09 (SF)-IMP-INT-RIATT         PIC S9(12)V9(3) COMP-3.
                   09 (SF)-IMP-INT-RIATT-NULL     REDEFINES
                      (SF)-IMP-INT-RIATT         PIC X(8).
ITRAT              09 (SF)-RATA-IMP-ACQ-EXP      PIC S9(12)V9(3) COMP-3.
ITRAT              09 (SF)-RATA-IMP-ACQ-EXP-NULL  REDEFINES
ITRAT                 (SF)-RATA-IMP-ACQ-EXP      PIC X(8).
ITRAT              09 (SF)-RATA-IMP-REN-ASS      PIC S9(12)V9(3) COMP-3.
ITRAT              09 (SF)-RATA-IMP-REN-ASS-NULL  REDEFINES
ITRAT                 (SF)-RATA-IMP-REN-ASS      PIC X(8).
ITRAT              09 (SF)-RATA-IMP-COMMIS-INT   PIC S9(12)V9(3) COMP-3.
ITRAT              09 (SF)-RATA-IMP-COMMIS-INT-NULL    REDEFINES
ITRAT                 (SF)-RATA-IMP-COMMIS-INT   PIC X(8).
11140              09 (SF)-RATA-ANTIRACKET       PIC S9(12)V9(3) COMP-3.
11140              09 (SF)-RATA-ANTIRACKET-NULL  REDEFINES
11140                 (SF)-RATA-ANTIRACKET       PIC X(8).
11174              09 (SF)-RATA-AUTOGEN-INC      PIC X(01).
11174              09 (SF)-RATA-AUTOGEN-INC-NULL REDEFINES
11174                 (SF)-RATA-AUTOGEN-INC      PIC X(1).
               07 (SF)-DATI-ALTRI.
                   09 (SF)-TP-IAS                PIC  X(02).
                   09 (SF)-DT-VAL-QUOTE          PIC S9(08)      COMP-3.
                   09 (SF)-DT-VAL-QUOTE-NULL        REDEFINES
                      (SF)-DT-VAL-QUOTE          PIC  X(05).
                   09 (SF)-FL-FRAZ-PROVV         PIC  X(01).
               07 (SF)-DATI-PAR-MOVIM.
                   09 (SF)-DT-PROS-BNS-FED       PIC S9(08)      COMP-3.
                   09 (SF)-DT-PROS-BNS-FED-NULL   REDEFINES
                      (SF)-DT-PROS-BNS-FED       PIC  X(05).
                   09 (SF)-DT-PROS-BNS-RIC       PIC S9(08)      COMP-3.
                   09 (SF)-DT-PROS-BNS-RIC-NULL   REDEFINES
                      (SF)-DT-PROS-BNS-RIC       PIC  X(05).
                   09 (SF)-DT-PROS-CEDOLA        PIC S9(08)      COMP-3.
                   09 (SF)-DT-PROS-CEDOLA-NULL    REDEFINES
                      (SF)-DT-PROS-CEDOLA        PIC  X(05).
      *--- Data fine copertura titolo contabile
           05 (SF)-DT-END-COP-TIT                PIC S9(08)      COMP-3.
           05 (SF)-DT-END-COP-TIT-NULL            REDEFINES
              (SF)-DT-END-COP-TIT                PIC X(05).
      *--- Data effetto calcolo
           05 (SF)-DT-EFF-CALCOLO-I              PIC 9(0008).
           05 (SF)-DT-EFF-CALCOLO-I-NULL          REDEFINES
              (SF)-DT-EFF-CALCOLO-I              PIC X(0008).

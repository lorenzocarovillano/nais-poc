      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************

       LETTURA-TIT.

           IF IDSI0011-SELECT
              PERFORM SELECT-TIT
                 THRU SELECT-TIT-EX
           ELSE
              PERFORM FETCH-TIT
                 THRU FETCH-TIT-EX
           END-IF.

       LETTURA-TIT-EX.
           EXIT.

       SELECT-TIT.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
      *-->    GESTIRE ERRORE DB
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI
                       TO TIT-CONT
                     PERFORM VALORIZZA-OUTPUT-TIT
                        THRU VALORIZZA-OUTPUT-TIT-EX

                  WHEN IDSO0011-NOT-FOUND
      *----- CHIAVE NON TROVATA
                     MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'SELECT-TIT'      TO IEAI9901-LABEL-ERR
                     MOVE '005017'          TO IEAI9901-COD-ERRORE
                     MOVE SPACES            TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300

                  WHEN OTHER
      *------  ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'SELECT-TIT'      TO IEAI9901-LABEL-ERR
                     MOVE '005016'          TO IEAI9901-COD-ERRORE
                     STRING WK-TIT-CONT        ';'
                          IDSO0011-RETURN-CODE ';'
                          IDSO0011-SQLCODE
                     DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                     END-STRING
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SELECT-TIT'             TO IEAI9901-LABEL-ERR
              MOVE '005016'                 TO IEAI9901-COD-ERRORE
              STRING WK-TIT-CONT          ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       SELECT-TIT-EX.
           EXIT.

       FETCH-TIT.

           SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
           SET  WCOM-OVERFLOW-NO                  TO TRUE.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR WCOM-OVERFLOW-YES

               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *-->        CHIAVE NON TROVATA
                           IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM
                                TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'FETCH-TIT'
                                TO IEAI9901-LABEL-ERR
                              MOVE '005017'
                                TO IEAI9901-COD-ERRORE
                              MOVE SPACES
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                           END-IF
                      WHEN IDSO0011-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                           MOVE IDSO0011-BUFFER-DATI  TO TIT-CONT
                           ADD  1                     TO IX-TAB-TIT

      *-->  Se il contatore di lettura � maggiore dell' elemento MAX
      *-->  previsto per la TAB.STAT-OGG-BUS allora siamo in overflow
                           IF IX-TAB-TIT > (SF)-ELE-TIT-MAX
                              SET WCOM-OVERFLOW-YES   TO TRUE
                           ELSE
                              PERFORM VALORIZZA-OUTPUT-TIT
                                 THRU VALORIZZA-OUTPUT-TIT-EX
                              SET IDSI0011-FETCH-NEXT TO TRUE
                           END-IF
                      WHEN OTHER
      *-->        ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'FETCH-TIT'  TO IEAI9901-LABEL-ERR
                           MOVE '005016'     TO IEAI9901-COD-ERRORE
                           STRING WK-TIT-CONT    ';'
                                  IDSO0011-RETURN-CODE ';'
                                  IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      *-->        ERRORE DISPATCHER
                  MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'FETCH-TIT'   TO IEAI9901-LABEL-ERR
                  MOVE '005016'      TO IEAI9901-COD-ERRORE
                  STRING WK-TIT-CONT     ';'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

       FETCH-TIT-EX.
           EXIT.

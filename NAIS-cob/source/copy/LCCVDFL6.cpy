      *----------------------------------------------------------------*
      *    COPY      ..... LCCVDFL6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO DATI_FORZATI_LIQUIDAZIONE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-DATI-FORZ-LIQU.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE D-FORZ-LIQ

      *--> NOME TABELLA FISICA DB
           MOVE 'D-FORZ-LIQ'                  TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WDFL-ST-INV
              AND WDFL-ELE-DFL-MAX  NOT = 0

              EVALUATE TRUE
      *-->       INSERIMENTO
                  WHEN WDFL-ST-ADD

      *-->           ESTRAZIONE SEQUENCE LIQUIDAZIONE

                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK
                        MOVE S090-SEQ-TABELLA TO WDFL-ID-PTF
                                                 DFL-ID-D-FORZ-LIQ
                        MOVE WMOV-ID-PTF     TO DFL-ID-MOVI-CRZ
                        MOVE WLQU-ID-PTF(IX-TAB-LQU)
                                              TO DFL-ID-LIQ
                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        MODIFICA
                  WHEN WDFL-ST-MOD

      *-->           VALORIZZAZIONE ID TRANCHE LIQUIDAZIONE
                     MOVE WMOV-ID-PTF            TO DFL-ID-MOVI-CRZ
                     MOVE WMOV-DT-EFF             TO DFL-DT-INI-EFF
                     MOVE WLQU-ID-PTF(IX-TAB-LQU) TO DFL-ID-LIQ

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WDFL-ST-DEL

                     IF WDFL-ID-MOVI-CHIU-NULL = HIGH-VALUES
                        MOVE WDFL-ID-MOVI-CHIU-NULL
                          TO DFL-ID-MOVI-CHIU-NULL
                     ELSE
                        MOVE WDFL-ID-MOVI-CHIU
                          TO DFL-ID-MOVI-CHIU
                     END-IF
                     MOVE WDFL-DT-END-EFF
                       TO DFL-DT-END-EFF
                     MOVE WLQU-ID-PTF(IX-TAB-LQU)
                       TO DFL-ID-LIQ

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-DELETE-LOGICA    TO TRUE

                END-EVALUATE

                IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN TRANCHE DI LIQUIDAZIONE
                 PERFORM VAL-DCLGEN-DFL
                    THRU VAL-DCLGEN-DFL-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-DFL
                    THRU VALORIZZA-AREA-DSH-DFL-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-DATI-FORZ-LIQU-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-DFL.

      *--> DCLGEN TABELLA
           MOVE D-FORZ-LIQ              TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT  TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-DFL-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVDFL5.

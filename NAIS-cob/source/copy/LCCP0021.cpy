      ******************************************************************
      *    CALL MATRICE MOVIMENTO
      ******************************************************************
       CALL-MATR-MOVIMENTO.

           MOVE 'LCCS0020' TO LCCV0021-PGM
           CALL LCCV0021-PGM USING AREA-IDSV0001 LCCV0021
           ON EXCEPTION
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'CALL-MATR-MOVIMENTO'
                    TO CALL-DESC
                  MOVE 'CALL-MATR-MOVIMENTO'
                    TO IEAI9901-LABEL-ERR
                  PERFORM S0290-ERRORE-DI-SISTEMA
                     THRU EX-S0290
           END-CALL.

           IF IDSV0001-ESITO-OK
              IF NOT LCCV0021-SUCCESSFUL-RC
              OR NOT LCCV0021-SUCCESSFUL-SQL

                 SET IDSV0001-ESITO-KO
                  TO TRUE
                 MOVE LCCV0021-COD-SERVIZIO-BE
                   TO IEAI9901-COD-SERVIZIO-BE
                 MOVE LCCV0021-DESCRIZ-ERR
                   TO CALL-DESC
                 MOVE 'CALL-MATR-MOVIMENTO'
                    TO IEAI9901-LABEL-ERR
                 PERFORM S0290-ERRORE-DI-SISTEMA
                    THRU EX-S0290
              END-IF
           END-IF.

       CALL-MATR-MOVIMENTO-EX.
           EXIT.

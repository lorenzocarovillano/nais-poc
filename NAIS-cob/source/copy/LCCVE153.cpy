
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVE153
      *   ULTIMO AGG. 04 MAR 2020
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-E15.
           MOVE E15-ID-EST-RAPP-ANA
             TO (SF)-ID-PTF(IX-TAB-E15)
           MOVE E15-ID-EST-RAPP-ANA
             TO (SF)-ID-EST-RAPP-ANA(IX-TAB-E15)
           MOVE E15-ID-RAPP-ANA
             TO (SF)-ID-RAPP-ANA(IX-TAB-E15)
           IF E15-ID-RAPP-ANA-COLLG-NULL = HIGH-VALUES
              MOVE E15-ID-RAPP-ANA-COLLG-NULL
                TO (SF)-ID-RAPP-ANA-COLLG-NULL(IX-TAB-E15)
           ELSE
              MOVE E15-ID-RAPP-ANA-COLLG
                TO (SF)-ID-RAPP-ANA-COLLG(IX-TAB-E15)
           END-IF
           MOVE E15-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-E15)
           MOVE E15-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-E15)
           MOVE E15-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-E15)
           IF E15-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE E15-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-E15)
           ELSE
              MOVE E15-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-E15)
           END-IF
           MOVE E15-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-E15)
           MOVE E15-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-E15)
           MOVE E15-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-E15)
           IF E15-COD-SOGG-NULL = HIGH-VALUES
              MOVE E15-COD-SOGG-NULL
                TO (SF)-COD-SOGG-NULL(IX-TAB-E15)
           ELSE
              MOVE E15-COD-SOGG
                TO (SF)-COD-SOGG(IX-TAB-E15)
           END-IF
           MOVE E15-TP-RAPP-ANA
             TO (SF)-TP-RAPP-ANA(IX-TAB-E15)
           MOVE E15-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-E15)
           MOVE E15-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-E15)
           MOVE E15-DS-VER
             TO (SF)-DS-VER(IX-TAB-E15)
           MOVE E15-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-E15)
           MOVE E15-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-E15)
           MOVE E15-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-E15)
           MOVE E15-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-E15)
           IF E15-ID-SEGMENTAZ-CLI-NULL = HIGH-VALUES
              MOVE E15-ID-SEGMENTAZ-CLI-NULL
                TO (SF)-ID-SEGMENTAZ-CLI-NULL(IX-TAB-E15)
           ELSE
              MOVE E15-ID-SEGMENTAZ-CLI
                TO (SF)-ID-SEGMENTAZ-CLI(IX-TAB-E15)
           END-IF
           IF E15-FL-COINC-TIT-EFF-NULL = HIGH-VALUES
              MOVE E15-FL-COINC-TIT-EFF-NULL
                TO (SF)-FL-COINC-TIT-EFF-NULL(IX-TAB-E15)
           ELSE
              MOVE E15-FL-COINC-TIT-EFF
                TO (SF)-FL-COINC-TIT-EFF(IX-TAB-E15)
           END-IF
           IF E15-FL-PERS-ESP-POL-NULL = HIGH-VALUES
              MOVE E15-FL-PERS-ESP-POL-NULL
                TO (SF)-FL-PERS-ESP-POL-NULL(IX-TAB-E15)
           ELSE
              MOVE E15-FL-PERS-ESP-POL
                TO (SF)-FL-PERS-ESP-POL(IX-TAB-E15)
           END-IF
           MOVE E15-DESC-PERS-ESP-POL
             TO (SF)-DESC-PERS-ESP-POL(IX-TAB-E15)
           IF E15-TP-LEG-CNTR-NULL = HIGH-VALUES
              MOVE E15-TP-LEG-CNTR-NULL
                TO (SF)-TP-LEG-CNTR-NULL(IX-TAB-E15)
           ELSE
              MOVE E15-TP-LEG-CNTR
                TO (SF)-TP-LEG-CNTR(IX-TAB-E15)
           END-IF
           MOVE E15-DESC-LEG-CNTR
             TO (SF)-DESC-LEG-CNTR(IX-TAB-E15)
           IF E15-TP-LEG-PERC-BNFICR-NULL = HIGH-VALUES
              MOVE E15-TP-LEG-PERC-BNFICR-NULL
                TO (SF)-TP-LEG-PERC-BNFICR-NULL(IX-TAB-E15)
           ELSE
              MOVE E15-TP-LEG-PERC-BNFICR
                TO (SF)-TP-LEG-PERC-BNFICR(IX-TAB-E15)
           END-IF
           MOVE E15-D-LEG-PERC-BNFICR
             TO (SF)-D-LEG-PERC-BNFICR(IX-TAB-E15)
           IF E15-TP-CNT-CORR-NULL = HIGH-VALUES
              MOVE E15-TP-CNT-CORR-NULL
                TO (SF)-TP-CNT-CORR-NULL(IX-TAB-E15)
           ELSE
              MOVE E15-TP-CNT-CORR
                TO (SF)-TP-CNT-CORR(IX-TAB-E15)
           END-IF
           IF E15-TP-COINC-PIC-PAC-NULL = HIGH-VALUES
              MOVE E15-TP-COINC-PIC-PAC-NULL
                TO (SF)-TP-COINC-PIC-PAC-NULL(IX-TAB-E15)
           ELSE
              MOVE E15-TP-COINC-PIC-PAC
                TO (SF)-TP-COINC-PIC-PAC(IX-TAB-E15)
           END-IF.
       VALORIZZA-OUTPUT-E15-EX.
           EXIT.

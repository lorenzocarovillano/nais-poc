      **************************************************************
      * STATEMETS X GESTIONE CON FILES SEQUENZIALI
      * N.B. - DA UTILIZZARE CON LA COPY IABPSQMn
      **************************************************************
       01 IABVSQM1.

      ******************************************************************
      * OPERAZIONE SU FILE SEQUENZIALE
      ******************************************************************
          05 OPERAZIONE-FILESQM               PIC X(02).
             88 OPEN-FILESQM-OPER             VALUE 'OP'.
             88 CLOSE-FILESQM-OPER            VALUE 'CL'.
             88 READ-FILESQM-OPER             VALUE 'RE'.
             88 WRITE-FILESQM-OPER            VALUE 'WR'.
             88 REWRITE-FILESQM-OPER          VALUE 'RW'.


      ******************************************************************
      * FLAG DI CONTROLLO x FILES SEQUENZIALI
      ******************************************************************
          05 STATO-FILESQM1           PIC X(01).
             88 FILESQM1-APERTO       VALUE 'A'.
             88 FILESQM1-CHIUSO       VALUE 'C'.

          05 STATO-FILESQM2           PIC X(01).
             88 FILESQM2-APERTO       VALUE 'A'.
             88 FILESQM2-CHIUSO       VALUE 'C'.

          05 STATO-FILESQM3           PIC X(01).
             88 FILESQM3-APERTO       VALUE 'A'.
             88 FILESQM3-CHIUSO       VALUE 'C'.

          05 STATO-FILESQM4           PIC X(01).
             88 FILESQM4-APERTO       VALUE 'A'.
             88 FILESQM4-CHIUSO       VALUE 'C'.

          05 STATO-FILESQM5           PIC X(01).
             88 FILESQM5-APERTO       VALUE 'A'.
             88 FILESQM5-CHIUSO       VALUE 'C'.

          05 STATO-FILESQM6           PIC X(01).
             88 FILESQM6-APERTO       VALUE 'A'.
             88 FILESQM6-CHIUSO       VALUE 'C'.

          05 STATO-FILESQM7           PIC X(01).
             88 FILESQM7-APERTO       VALUE 'A'.
             88 FILESQM7-CHIUSO       VALUE 'C'.

          05 STATO-FILESQM8           PIC X(01).
             88 FILESQM8-APERTO       VALUE 'A'.
             88 FILESQM8-CHIUSO       VALUE 'C'.

          05 STATO-FILESQM9           PIC X(01).
             88 FILESQM9-APERTO       VALUE 'A'.
             88 FILESQM9-CHIUSO       VALUE 'C'.

          05 STATO-FILESQM0           PIC X(01).
             88 FILESQM0-APERTO       VALUE 'A'.
             88 FILESQM0-CHIUSO       VALUE 'C'.

      ******************************************************************
      * FLAG DI CONTROLLO x END-OF-FILE
      ******************************************************************
          05 EOF-FILESQM1             PIC X(01).
             88 FILESQM1-EOF-SI       VALUE 'S'.
             88 FILESQM1-EOF-NO       VALUE 'N'.

          05 EOF-FILESQM2             PIC X(01).
             88 FILESQM2-EOF-SI       VALUE 'S'.
             88 FILESQM2-EOF-NO       VALUE 'N'.

          05 EOF-FILESQM3             PIC X(01).
             88 FILESQM3-EOF-SI       VALUE 'S'.
             88 FILESQM3-EOF-NO       VALUE 'N'.

          05 EOF-FILESQM4             PIC X(01).
             88 FILESQM4-EOF-SI       VALUE 'S'.
             88 FILESQM4-EOF-NO       VALUE 'N'.

          05 EOF-FILESQM5             PIC X(01).
             88 FILESQM5-EOF-SI       VALUE 'S'.
             88 FILESQM5-EOF-NO       VALUE 'N'.

          05 EOF-FILESQM6             PIC X(01).
             88 FILESQM6-EOF-SI       VALUE 'S'.
             88 FILESQM6-EOF-NO       VALUE 'N'.

          05 EOF-FILESQM7             PIC X(01).
             88 FILESQM7-EOF-SI       VALUE 'S'.
             88 FILESQM7-EOF-NO       VALUE 'N'.

          05 EOF-FILESQM8             PIC X(01).
             88 FILESQM8-EOF-SI       VALUE 'S'.
             88 FILESQM8-EOF-NO       VALUE 'N'.

          05 EOF-FILESQM9             PIC X(01).
             88 FILESQM9-EOF-SI       VALUE 'S'.
             88 FILESQM9-EOF-NO       VALUE 'N'.

          05 EOF-FILESQM0             PIC X(01).
             88 FILESQM0-EOF-SI       VALUE 'S'.
             88 FILESQM0-EOF-NO       VALUE 'N'.

      ******************************************************************
      * FLAG X OPEN FILES SEQUENZIALI
      ******************************************************************
          05 FLAG-OPEN-FILESQM1       PIC X(01).
             88 OPEN-FILESQM1-SI      VALUE 'S'.
             88 OPEN-FILESQM1-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQM2       PIC X(01).
             88 OPEN-FILESQM2-SI      VALUE 'S'.
             88 OPEN-FILESQM2-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQM3       PIC X(01).
             88 OPEN-FILESQM3-SI      VALUE 'S'.
             88 OPEN-FILESQM3-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQM4       PIC X(01).
             88 OPEN-FILESQM4-SI      VALUE 'S'.
             88 OPEN-FILESQM4-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQM5       PIC X(01).
             88 OPEN-FILESQM5-SI      VALUE 'S'.
             88 OPEN-FILESQM5-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQM6       PIC X(01).
             88 OPEN-FILESQM6-SI      VALUE 'S'.
             88 OPEN-FILESQM6-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQM7       PIC X(01).
             88 OPEN-FILESQM7-SI      VALUE 'S'.
             88 OPEN-FILESQM7-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQM8       PIC X(01).
             88 OPEN-FILESQM8-SI      VALUE 'S'.
             88 OPEN-FILESQM8-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQM9       PIC X(01).
             88 OPEN-FILESQM9-SI      VALUE 'S'.
             88 OPEN-FILESQM9-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQM0       PIC X(01).
             88 OPEN-FILESQM0-SI      VALUE 'S'.
             88 OPEN-FILESQM0-NO      VALUE 'N'.

          05 FLAG-OPEN-FILESQM-ALL     PIC X(01).
             88 OPEN-FILESQM-ALL-SI    VALUE 'S'.
             88 OPEN-FILESQM-ALL-NO    VALUE 'N'.


      ******************************************************************
      * FLAG X READ FILES SEQUENZIALI
      ******************************************************************
          05 FLAG-READ-FILESQM1       PIC X(01).
             88 READ-FILESQM1-SI      VALUE 'S'.
             88 READ-FILESQM1-NO      VALUE 'N'.

          05 FLAG-READ-FILESQM2       PIC X(01).
             88 READ-FILESQM2-SI      VALUE 'S'.
             88 READ-FILESQM2-NO      VALUE 'N'.

          05 FLAG-READ-FILESQM3       PIC X(01).
             88 READ-FILESQM3-SI      VALUE 'S'.
             88 READ-FILESQM3-NO      VALUE 'N'.

          05 FLAG-READ-FILESQM4       PIC X(01).
             88 READ-FILESQM4-SI      VALUE 'S'.
             88 READ-FILESQM4-NO      VALUE 'N'.

          05 FLAG-READ-FILESQM5       PIC X(01).
             88 READ-FILESQM5-SI      VALUE 'S'.
             88 READ-FILESQM5-NO      VALUE 'N'.

          05 FLAG-READ-FILESQM6       PIC X(01).
             88 READ-FILESQM6-SI      VALUE 'S'.
             88 READ-FILESQM6-NO      VALUE 'N'.

          05 FLAG-READ-FILESQM7       PIC X(01).
             88 READ-FILESQM7-SI      VALUE 'S'.
             88 READ-FILESQM7-NO      VALUE 'N'.

          05 FLAG-READ-FILESQM8       PIC X(01).
             88 READ-FILESQM8-SI      VALUE 'S'.
             88 READ-FILESQM8-NO      VALUE 'N'.

          05 FLAG-READ-FILESQM9       PIC X(01).
             88 READ-FILESQM9-SI      VALUE 'S'.
             88 READ-FILESQM9-NO      VALUE 'N'.

          05 FLAG-READ-FILESQM0       PIC X(01).
             88 READ-FILESQM0-SI      VALUE 'S'.
             88 READ-FILESQM0-NO      VALUE 'N'.

          05 FLAG-READ-FILESQM-ALL     PIC X(01).
             88 READ-FILESQM-ALL-SI    VALUE 'S'.
             88 READ-FILESQM-ALL-NO    VALUE 'N'.


      ******************************************************************
      * FLAG X WRITE FILES SEQUENZIALI
      ******************************************************************
          05 FLAG-WRITE-FILESQM1       PIC X(01).
             88 WRITE-FILESQM1-SI      VALUE 'S'.
             88 WRITE-FILESQM1-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQM2       PIC X(01).
             88 WRITE-FILESQM2-SI      VALUE 'S'.
             88 WRITE-FILESQM2-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQM3       PIC X(01).
             88 WRITE-FILESQM3-SI      VALUE 'S'.
             88 WRITE-FILESQM3-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQM4       PIC X(01).
             88 WRITE-FILESQM4-SI      VALUE 'S'.
             88 WRITE-FILESQM4-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQM5       PIC X(01).
             88 WRITE-FILESQM5-SI      VALUE 'S'.
             88 WRITE-FILESQM5-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQM6       PIC X(01).
             88 WRITE-FILESQM6-SI      VALUE 'S'.
             88 WRITE-FILESQM6-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQM7       PIC X(01).
             88 WRITE-FILESQM7-SI      VALUE 'S'.
             88 WRITE-FILESQM7-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQM8       PIC X(01).
             88 WRITE-FILESQM8-SI      VALUE 'S'.
             88 WRITE-FILESQM8-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQM9       PIC X(01).
             88 WRITE-FILESQM9-SI      VALUE 'S'.
             88 WRITE-FILESQM9-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQM0       PIC X(01).
             88 WRITE-FILESQM0-SI      VALUE 'S'.
             88 WRITE-FILESQM0-NO      VALUE 'N'.

          05 FLAG-WRITE-FILESQM-ALL     PIC X(01).
             88 WRITE-FILESQM-ALL-SI    VALUE 'S'.
             88 WRITE-FILESQM-ALL-NO    VALUE 'N'.

      ******************************************************************
      * FLAG X WRITE FILES SEQUENZIALI
      ******************************************************************
          05 FLAG-REWRITE-FILESQM1       PIC X(01).
             88 REWRITE-FILESQM1-SI      VALUE 'S'.
             88 REWRITE-FILESQM1-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQM2       PIC X(01).
             88 REWRITE-FILESQM2-SI      VALUE 'S'.
             88 REWRITE-FILESQM2-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQM3       PIC X(01).
             88 REWRITE-FILESQM3-SI      VALUE 'S'.
             88 REWRITE-FILESQM3-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQM4       PIC X(01).
             88 REWRITE-FILESQM4-SI      VALUE 'S'.
             88 REWRITE-FILESQM4-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQM5       PIC X(01).
             88 REWRITE-FILESQM5-SI      VALUE 'S'.
             88 REWRITE-FILESQM5-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQM6       PIC X(01).
             88 REWRITE-FILESQM6-SI      VALUE 'S'.
             88 REWRITE-FILESQM6-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQM7       PIC X(01).
             88 REWRITE-FILESQM7-SI      VALUE 'S'.
             88 REWRITE-FILESQM7-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQM8       PIC X(01).
             88 REWRITE-FILESQM8-SI      VALUE 'S'.
             88 REWRITE-FILESQM8-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQM9       PIC X(01).
             88 REWRITE-FILESQM9-SI      VALUE 'S'.
             88 REWRITE-FILESQM9-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQM0       PIC X(01).
             88 REWRITE-FILESQM0-SI      VALUE 'S'.
             88 REWRITE-FILESQM0-NO      VALUE 'N'.

          05 FLAG-REWRITE-FILESQM-ALL     PIC X(01).
             88 REWRITE-FILESQM-ALL-SI    VALUE 'S'.
             88 REWRITE-FILESQM-ALL-NO    VALUE 'N'.

      ******************************************************************
      * FLAG X CLOSE FILES SEQUENZIALI
      ******************************************************************
          05 FLAG-CLOSE-FILESQM1       PIC X(01).
             88 CLOSE-FILESQM1-SI      VALUE 'S'.
             88 CLOSE-FILESQM1-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQM2       PIC X(01).
             88 CLOSE-FILESQM2-SI      VALUE 'S'.
             88 CLOSE-FILESQM2-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQM3       PIC X(01).
             88 CLOSE-FILESQM3-SI      VALUE 'S'.
             88 CLOSE-FILESQM3-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQM4       PIC X(01).
             88 CLOSE-FILESQM4-SI      VALUE 'S'.
             88 CLOSE-FILESQM4-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQM5       PIC X(01).
             88 CLOSE-FILESQM5-SI      VALUE 'S'.
             88 CLOSE-FILESQM5-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQM6       PIC X(01).
             88 CLOSE-FILESQM6-SI      VALUE 'S'.
             88 CLOSE-FILESQM6-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQM7       PIC X(01).
             88 CLOSE-FILESQM7-SI      VALUE 'S'.
             88 CLOSE-FILESQM7-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQM8       PIC X(01).
             88 CLOSE-FILESQM8-SI      VALUE 'S'.
             88 CLOSE-FILESQM8-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQM9       PIC X(01).
             88 CLOSE-FILESQM9-SI      VALUE 'S'.
             88 CLOSE-FILESQM9-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQM0       PIC X(01).
             88 CLOSE-FILESQM0-SI      VALUE 'S'.
             88 CLOSE-FILESQM0-NO      VALUE 'N'.

          05 FLAG-CLOSE-FILESQM-ALL     PIC X(01).
             88 CLOSE-FILESQM-ALL-SI    VALUE 'S'.
             88 CLOSE-FILESQM-ALL-NO    VALUE 'N'.


      ******************************************************************
      * FLAG X TIPO OPEN FILES SEQUENZIALI
      ******************************************************************

          05 OPEN-TYPE-FILESQM1     PIC X(03).
             88 INPUT-FILESQM1      VALUE 'INP'.
             88 OUTPUT-FILESQM1     VALUE 'OUT'.
             88 I-O-FILESQM1        VALUE 'I-O'.
             88 EXTEND-FILESQM1     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQM2     PIC X(03).
             88 INPUT-FILESQM2      VALUE 'INP'.
             88 OUTPUT-FILESQM2     VALUE 'OUT'.
             88 I-O-FILESQM2        VALUE 'I-O'.
             88 EXTEND-FILESQM2     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQM3     PIC X(03).
             88 INPUT-FILESQM3      VALUE 'INP'.
             88 OUTPUT-FILESQM3     VALUE 'OUT'.
             88 I-O-FILESQM3        VALUE 'I-O'.
             88 EXTEND-FILESQM3     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQM4     PIC X(03).
             88 INPUT-FILESQM4      VALUE 'INP'.
             88 OUTPUT-FILESQM4     VALUE 'OUT'.
             88 I-O-FILESQM4        VALUE 'I-O'.
             88 EXTEND-FILESQM4     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQM5     PIC X(03).
             88 INPUT-FILESQM5      VALUE 'INP'.
             88 OUTPUT-FILESQM5     VALUE 'OUT'.
             88 I-O-FILESQM5        VALUE 'I-O'.
             88 EXTEND-FILESQM5     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQM6     PIC X(03).
             88 INPUT-FILESQM6      VALUE 'INP'.
             88 OUTPUT-FILESQM6     VALUE 'OUT'.
             88 I-O-FILESQM6        VALUE 'I-O'.
             88 EXTEND-FILESQM6     VALUE 'EXT'.

          05 OPEN-TYPE-FILESQM7     PIC X(03).
             88 INPUT-FILESQM7      VALUE 'INP'.
             88 OUTPUT-FILESQM7     VALUE 'OUT'.
             88 I-O-FILESQM7        VALUE 'I-O'.
             88 EXTEND-FILESQM7    VALUE 'EXT'.

          05 OPEN-TYPE-FILESQM8     PIC X(03).
             88 INPUT-FILESQM8      VALUE 'INP'.
             88 OUTPUT-FILESQM8     VALUE 'OUT'.
             88 I-O-FILESQM8        VALUE 'I-O'.
             88 EXTEND-FILESQM8    VALUE 'EXT'.

          05 OPEN-TYPE-FILESQM9     PIC X(03).
             88 INPUT-FILESQM9      VALUE 'INP'.
             88 OUTPUT-FILESQM9     VALUE 'OUT'.
             88 I-O-FILESQM9        VALUE 'I-O'.
             88 EXTEND-FILESQM9    VALUE 'EXT'.

          05 OPEN-TYPE-FILESQM0     PIC X(03).
             88 INPUT-FILESQM0      VALUE 'INP'.
             88 OUTPUT-FILESQM0     VALUE 'OUT'.
             88 I-O-FILESQM0        VALUE 'I-O'.
             88 EXTEND-FILESQM0    VALUE 'EXT'.

          05 OPEN-TYPE-FILESQM-ALL  PIC X(03).
             88 INPUT-FILESQM-ALL   VALUE 'INP'.
             88 OUTPUT-FILESQM-ALL  VALUE 'OUT'.
             88 I-O-FILESQM-ALL     VALUE 'I-O'.
             88 EXTEND-FILESQM-ALL  VALUE 'EXT'.




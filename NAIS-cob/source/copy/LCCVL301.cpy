      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA REINVST_POLI_LQ
      *   ALIAS L30
      *   ULTIMO AGG. 09 LUG 2009
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-REINVST-POLI-LQ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-RAMO PIC X(12).
             07 (SF)-IB-POLI PIC X(40).
             07 (SF)-PR-KEY-SIST-ESTNO PIC X(40).
             07 (SF)-PR-KEY-SIST-ESTNO-NULL REDEFINES
                (SF)-PR-KEY-SIST-ESTNO   PIC X(40).
             07 (SF)-SEC-KEY-SIST-ESTNO PIC X(40).
             07 (SF)-SEC-KEY-SIST-ESTNO-NULL REDEFINES
                (SF)-SEC-KEY-SIST-ESTNO   PIC X(40).
             07 (SF)-COD-CAN PIC S9(5)     COMP-3.
             07 (SF)-COD-AGE PIC S9(5)     COMP-3.
             07 (SF)-COD-SUB-AGE PIC S9(5)     COMP-3.
             07 (SF)-COD-SUB-AGE-NULL REDEFINES
                (SF)-COD-SUB-AGE   PIC X(3).
             07 (SF)-IMP-RES PIC S9(12)V9(3) COMP-3.
             07 (SF)-TP-LIQ PIC X(2).
             07 (SF)-TP-SIST-ESTNO PIC X(2).
             07 (SF)-COD-FISC-PART-IVA PIC X(16).
             07 (SF)-COD-MOVI-LIQ PIC S9(5)     COMP-3.
             07 (SF)-TP-INVST-LIQ PIC S9(2)     COMP-3.
             07 (SF)-TP-INVST-LIQ-NULL REDEFINES
                (SF)-TP-INVST-LIQ   PIC X(2).
             07 (SF)-IMP-TOT-LIQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-DT-DECOR-POLI-LQ   PIC S9(8) COMP-3.
             07 (SF)-DT-DECOR-POLI-LQ-NULL REDEFINES
                (SF)-DT-DECOR-POLI-LQ   PIC X(5).

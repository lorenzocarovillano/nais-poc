
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVPRE5
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-PRE.
           MOVE (SF)-TP-OGG(IX-TAB-PRE)
              TO PRE-TP-OGG
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PRE)
              TO PRE-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-PRE)
              TO PRE-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-PRE) NOT NUMERIC
              MOVE 0 TO PRE-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-PRE)
              TO PRE-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-PRE) NOT NUMERIC
              MOVE 0 TO PRE-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-PRE)
              TO PRE-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-PRE)
              TO PRE-COD-COMP-ANIA
           IF (SF)-DT-CONCS-PREST-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-DT-CONCS-PREST-NULL(IX-TAB-PRE)
              TO PRE-DT-CONCS-PREST-NULL
           ELSE
             IF (SF)-DT-CONCS-PREST(IX-TAB-PRE) = ZERO
                MOVE HIGH-VALUES
                TO PRE-DT-CONCS-PREST-NULL
             ELSE
              MOVE (SF)-DT-CONCS-PREST(IX-TAB-PRE)
              TO PRE-DT-CONCS-PREST
             END-IF
           END-IF
           IF (SF)-DT-DECOR-PREST-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-DT-DECOR-PREST-NULL(IX-TAB-PRE)
              TO PRE-DT-DECOR-PREST-NULL
           ELSE
             IF (SF)-DT-DECOR-PREST(IX-TAB-PRE) = ZERO
                MOVE HIGH-VALUES
                TO PRE-DT-DECOR-PREST-NULL
             ELSE
              MOVE (SF)-DT-DECOR-PREST(IX-TAB-PRE)
              TO PRE-DT-DECOR-PREST
             END-IF
           END-IF
           IF (SF)-IMP-PREST-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-IMP-PREST-NULL(IX-TAB-PRE)
              TO PRE-IMP-PREST-NULL
           ELSE
              MOVE (SF)-IMP-PREST(IX-TAB-PRE)
              TO PRE-IMP-PREST
           END-IF
           IF (SF)-INTR-PREST-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-INTR-PREST-NULL(IX-TAB-PRE)
              TO PRE-INTR-PREST-NULL
           ELSE
              MOVE (SF)-INTR-PREST(IX-TAB-PRE)
              TO PRE-INTR-PREST
           END-IF
           IF (SF)-TP-PREST-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-TP-PREST-NULL(IX-TAB-PRE)
              TO PRE-TP-PREST-NULL
           ELSE
              MOVE (SF)-TP-PREST(IX-TAB-PRE)
              TO PRE-TP-PREST
           END-IF
           IF (SF)-FRAZ-PAG-INTR-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-FRAZ-PAG-INTR-NULL(IX-TAB-PRE)
              TO PRE-FRAZ-PAG-INTR-NULL
           ELSE
              MOVE (SF)-FRAZ-PAG-INTR(IX-TAB-PRE)
              TO PRE-FRAZ-PAG-INTR
           END-IF
           IF (SF)-DT-RIMB-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-DT-RIMB-NULL(IX-TAB-PRE)
              TO PRE-DT-RIMB-NULL
           ELSE
             IF (SF)-DT-RIMB(IX-TAB-PRE) = ZERO
                MOVE HIGH-VALUES
                TO PRE-DT-RIMB-NULL
             ELSE
              MOVE (SF)-DT-RIMB(IX-TAB-PRE)
              TO PRE-DT-RIMB
             END-IF
           END-IF
           IF (SF)-IMP-RIMB-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-IMP-RIMB-NULL(IX-TAB-PRE)
              TO PRE-IMP-RIMB-NULL
           ELSE
              MOVE (SF)-IMP-RIMB(IX-TAB-PRE)
              TO PRE-IMP-RIMB
           END-IF
           IF (SF)-COD-DVS-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-COD-DVS-NULL(IX-TAB-PRE)
              TO PRE-COD-DVS-NULL
           ELSE
              MOVE (SF)-COD-DVS(IX-TAB-PRE)
              TO PRE-COD-DVS
           END-IF
           MOVE (SF)-DT-RICH-PREST(IX-TAB-PRE)
              TO PRE-DT-RICH-PREST
           IF (SF)-MOD-INTR-PREST-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-MOD-INTR-PREST-NULL(IX-TAB-PRE)
              TO PRE-MOD-INTR-PREST-NULL
           ELSE
              MOVE (SF)-MOD-INTR-PREST(IX-TAB-PRE)
              TO PRE-MOD-INTR-PREST
           END-IF
           IF (SF)-SPE-PREST-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-SPE-PREST-NULL(IX-TAB-PRE)
              TO PRE-SPE-PREST-NULL
           ELSE
              MOVE (SF)-SPE-PREST(IX-TAB-PRE)
              TO PRE-SPE-PREST
           END-IF
           IF (SF)-IMP-PREST-LIQTO-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-IMP-PREST-LIQTO-NULL(IX-TAB-PRE)
              TO PRE-IMP-PREST-LIQTO-NULL
           ELSE
              MOVE (SF)-IMP-PREST-LIQTO(IX-TAB-PRE)
              TO PRE-IMP-PREST-LIQTO
           END-IF
           IF (SF)-SDO-INTR-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-SDO-INTR-NULL(IX-TAB-PRE)
              TO PRE-SDO-INTR-NULL
           ELSE
              MOVE (SF)-SDO-INTR(IX-TAB-PRE)
              TO PRE-SDO-INTR
           END-IF
           IF (SF)-RIMB-EFF-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-RIMB-EFF-NULL(IX-TAB-PRE)
              TO PRE-RIMB-EFF-NULL
           ELSE
              MOVE (SF)-RIMB-EFF(IX-TAB-PRE)
              TO PRE-RIMB-EFF
           END-IF
           IF (SF)-PREST-RES-EFF-NULL(IX-TAB-PRE) = HIGH-VALUES
              MOVE (SF)-PREST-RES-EFF-NULL(IX-TAB-PRE)
              TO PRE-PREST-RES-EFF-NULL
           ELSE
              MOVE (SF)-PREST-RES-EFF(IX-TAB-PRE)
              TO PRE-PREST-RES-EFF
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-PRE) NOT NUMERIC
              MOVE 0 TO PRE-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-PRE)
              TO PRE-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-PRE)
              TO PRE-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-PRE) NOT NUMERIC
              MOVE 0 TO PRE-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-PRE)
              TO PRE-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-PRE) NOT NUMERIC
              MOVE 0 TO PRE-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-PRE)
              TO PRE-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-PRE) NOT NUMERIC
              MOVE 0 TO PRE-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-PRE)
              TO PRE-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-PRE)
              TO PRE-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-PRE)
              TO PRE-DS-STATO-ELAB.
       VAL-DCLGEN-PRE-EX.
           EXIT.

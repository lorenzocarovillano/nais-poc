
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVPMO5
      *   ULTIMO AGG. 17 FEB 2015
      *------------------------------------------------------------

       VAL-DCLGEN-PMO.
           MOVE (SF)-TP-OGG(IX-TAB-PMO)
              TO PMO-TP-OGG
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-PMO)
              TO PMO-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-PMO)
              TO PMO-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-PMO) NOT NUMERIC
              MOVE 0 TO PMO-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-PMO)
              TO PMO-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-PMO) NOT NUMERIC
              MOVE 0 TO PMO-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-PMO)
              TO PMO-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-PMO)
              TO PMO-COD-COMP-ANIA
           IF (SF)-TP-MOVI-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-TP-MOVI-NULL(IX-TAB-PMO)
              TO PMO-TP-MOVI-NULL
           ELSE
              MOVE (SF)-TP-MOVI(IX-TAB-PMO)
              TO PMO-TP-MOVI
           END-IF
           IF (SF)-FRQ-MOVI-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-FRQ-MOVI-NULL(IX-TAB-PMO)
              TO PMO-FRQ-MOVI-NULL
           ELSE
              MOVE (SF)-FRQ-MOVI(IX-TAB-PMO)
              TO PMO-FRQ-MOVI
           END-IF
           IF (SF)-DUR-AA-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-DUR-AA-NULL(IX-TAB-PMO)
              TO PMO-DUR-AA-NULL
           ELSE
              MOVE (SF)-DUR-AA(IX-TAB-PMO)
              TO PMO-DUR-AA
           END-IF
           IF (SF)-DUR-MM-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-DUR-MM-NULL(IX-TAB-PMO)
              TO PMO-DUR-MM-NULL
           ELSE
              MOVE (SF)-DUR-MM(IX-TAB-PMO)
              TO PMO-DUR-MM
           END-IF
           IF (SF)-DUR-GG-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-DUR-GG-NULL(IX-TAB-PMO)
              TO PMO-DUR-GG-NULL
           ELSE
              MOVE (SF)-DUR-GG(IX-TAB-PMO)
              TO PMO-DUR-GG
           END-IF
           IF (SF)-DT-RICOR-PREC-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-DT-RICOR-PREC-NULL(IX-TAB-PMO)
              TO PMO-DT-RICOR-PREC-NULL
           ELSE
             IF (SF)-DT-RICOR-PREC(IX-TAB-PMO) = ZERO
                MOVE HIGH-VALUES
                TO PMO-DT-RICOR-PREC-NULL
             ELSE
              MOVE (SF)-DT-RICOR-PREC(IX-TAB-PMO)
              TO PMO-DT-RICOR-PREC
             END-IF
           END-IF
           IF (SF)-DT-RICOR-SUCC-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-DT-RICOR-SUCC-NULL(IX-TAB-PMO)
              TO PMO-DT-RICOR-SUCC-NULL
           ELSE
             IF (SF)-DT-RICOR-SUCC(IX-TAB-PMO) = ZERO
                MOVE HIGH-VALUES
                TO PMO-DT-RICOR-SUCC-NULL
             ELSE
              MOVE (SF)-DT-RICOR-SUCC(IX-TAB-PMO)
              TO PMO-DT-RICOR-SUCC
             END-IF
           END-IF
           IF (SF)-PC-INTR-FRAZ-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-PC-INTR-FRAZ-NULL(IX-TAB-PMO)
              TO PMO-PC-INTR-FRAZ-NULL
           ELSE
              MOVE (SF)-PC-INTR-FRAZ(IX-TAB-PMO)
              TO PMO-PC-INTR-FRAZ
           END-IF
           IF (SF)-IMP-BNS-DA-SCO-TOT-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-IMP-BNS-DA-SCO-TOT-NULL(IX-TAB-PMO)
              TO PMO-IMP-BNS-DA-SCO-TOT-NULL
           ELSE
              MOVE (SF)-IMP-BNS-DA-SCO-TOT(IX-TAB-PMO)
              TO PMO-IMP-BNS-DA-SCO-TOT
           END-IF
           IF (SF)-IMP-BNS-DA-SCO-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-IMP-BNS-DA-SCO-NULL(IX-TAB-PMO)
              TO PMO-IMP-BNS-DA-SCO-NULL
           ELSE
              MOVE (SF)-IMP-BNS-DA-SCO(IX-TAB-PMO)
              TO PMO-IMP-BNS-DA-SCO
           END-IF
           IF (SF)-PC-ANTIC-BNS-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-PC-ANTIC-BNS-NULL(IX-TAB-PMO)
              TO PMO-PC-ANTIC-BNS-NULL
           ELSE
              MOVE (SF)-PC-ANTIC-BNS(IX-TAB-PMO)
              TO PMO-PC-ANTIC-BNS
           END-IF
           IF (SF)-TP-RINN-COLL-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-TP-RINN-COLL-NULL(IX-TAB-PMO)
              TO PMO-TP-RINN-COLL-NULL
           ELSE
              MOVE (SF)-TP-RINN-COLL(IX-TAB-PMO)
              TO PMO-TP-RINN-COLL
           END-IF
           IF (SF)-TP-RIVAL-PRE-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-TP-RIVAL-PRE-NULL(IX-TAB-PMO)
              TO PMO-TP-RIVAL-PRE-NULL
           ELSE
              MOVE (SF)-TP-RIVAL-PRE(IX-TAB-PMO)
              TO PMO-TP-RIVAL-PRE
           END-IF
           IF (SF)-TP-RIVAL-PRSTZ-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-TP-RIVAL-PRSTZ-NULL(IX-TAB-PMO)
              TO PMO-TP-RIVAL-PRSTZ-NULL
           ELSE
              MOVE (SF)-TP-RIVAL-PRSTZ(IX-TAB-PMO)
              TO PMO-TP-RIVAL-PRSTZ
           END-IF
           IF (SF)-FL-EVID-RIVAL-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-FL-EVID-RIVAL-NULL(IX-TAB-PMO)
              TO PMO-FL-EVID-RIVAL-NULL
           ELSE
              MOVE (SF)-FL-EVID-RIVAL(IX-TAB-PMO)
              TO PMO-FL-EVID-RIVAL
           END-IF
           IF (SF)-ULT-PC-PERD-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-ULT-PC-PERD-NULL(IX-TAB-PMO)
              TO PMO-ULT-PC-PERD-NULL
           ELSE
              MOVE (SF)-ULT-PC-PERD(IX-TAB-PMO)
              TO PMO-ULT-PC-PERD
           END-IF
           IF (SF)-TOT-AA-GIA-PROR-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-TOT-AA-GIA-PROR-NULL(IX-TAB-PMO)
              TO PMO-TOT-AA-GIA-PROR-NULL
           ELSE
              MOVE (SF)-TOT-AA-GIA-PROR(IX-TAB-PMO)
              TO PMO-TOT-AA-GIA-PROR
           END-IF
           IF (SF)-TP-OPZ-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-TP-OPZ-NULL(IX-TAB-PMO)
              TO PMO-TP-OPZ-NULL
           ELSE
              MOVE (SF)-TP-OPZ(IX-TAB-PMO)
              TO PMO-TP-OPZ
           END-IF
           IF (SF)-AA-REN-CER-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-AA-REN-CER-NULL(IX-TAB-PMO)
              TO PMO-AA-REN-CER-NULL
           ELSE
              MOVE (SF)-AA-REN-CER(IX-TAB-PMO)
              TO PMO-AA-REN-CER
           END-IF
           IF (SF)-PC-REVRSB-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-PC-REVRSB-NULL(IX-TAB-PMO)
              TO PMO-PC-REVRSB-NULL
           ELSE
              MOVE (SF)-PC-REVRSB(IX-TAB-PMO)
              TO PMO-PC-REVRSB
           END-IF
           IF (SF)-IMP-RISC-PARZ-PRGT-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-IMP-RISC-PARZ-PRGT-NULL(IX-TAB-PMO)
              TO PMO-IMP-RISC-PARZ-PRGT-NULL
           ELSE
              MOVE (SF)-IMP-RISC-PARZ-PRGT(IX-TAB-PMO)
              TO PMO-IMP-RISC-PARZ-PRGT
           END-IF
           IF (SF)-IMP-LRD-DI-RAT-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-IMP-LRD-DI-RAT-NULL(IX-TAB-PMO)
              TO PMO-IMP-LRD-DI-RAT-NULL
           ELSE
              MOVE (SF)-IMP-LRD-DI-RAT(IX-TAB-PMO)
              TO PMO-IMP-LRD-DI-RAT
           END-IF
           IF (SF)-IB-OGG-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-IB-OGG-NULL(IX-TAB-PMO)
              TO PMO-IB-OGG-NULL
           ELSE
              MOVE (SF)-IB-OGG(IX-TAB-PMO)
              TO PMO-IB-OGG
           END-IF
           IF (SF)-COS-ONER-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-COS-ONER-NULL(IX-TAB-PMO)
              TO PMO-COS-ONER-NULL
           ELSE
              MOVE (SF)-COS-ONER(IX-TAB-PMO)
              TO PMO-COS-ONER
           END-IF
           IF (SF)-SPE-PC-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-SPE-PC-NULL(IX-TAB-PMO)
              TO PMO-SPE-PC-NULL
           ELSE
              MOVE (SF)-SPE-PC(IX-TAB-PMO)
              TO PMO-SPE-PC
           END-IF
           IF (SF)-FL-ATTIV-GAR-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-FL-ATTIV-GAR-NULL(IX-TAB-PMO)
              TO PMO-FL-ATTIV-GAR-NULL
           ELSE
              MOVE (SF)-FL-ATTIV-GAR(IX-TAB-PMO)
              TO PMO-FL-ATTIV-GAR
           END-IF
           IF (SF)-CAMBIO-VER-PROD-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-CAMBIO-VER-PROD-NULL(IX-TAB-PMO)
              TO PMO-CAMBIO-VER-PROD-NULL
           ELSE
              MOVE (SF)-CAMBIO-VER-PROD(IX-TAB-PMO)
              TO PMO-CAMBIO-VER-PROD
           END-IF
           IF (SF)-MM-DIFF-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-MM-DIFF-NULL(IX-TAB-PMO)
              TO PMO-MM-DIFF-NULL
           ELSE
              MOVE (SF)-MM-DIFF(IX-TAB-PMO)
              TO PMO-MM-DIFF
           END-IF
           IF (SF)-IMP-RAT-MANFEE-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-IMP-RAT-MANFEE-NULL(IX-TAB-PMO)
              TO PMO-IMP-RAT-MANFEE-NULL
           ELSE
              MOVE (SF)-IMP-RAT-MANFEE(IX-TAB-PMO)
              TO PMO-IMP-RAT-MANFEE
           END-IF
           IF (SF)-DT-ULT-EROG-MANFEE-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-DT-ULT-EROG-MANFEE-NULL(IX-TAB-PMO)
              TO PMO-DT-ULT-EROG-MANFEE-NULL
           ELSE
             IF (SF)-DT-ULT-EROG-MANFEE(IX-TAB-PMO) = ZERO
                MOVE HIGH-VALUES
                TO PMO-DT-ULT-EROG-MANFEE-NULL
             ELSE
              MOVE (SF)-DT-ULT-EROG-MANFEE(IX-TAB-PMO)
              TO PMO-DT-ULT-EROG-MANFEE
             END-IF
           END-IF
           IF (SF)-TP-OGG-RIVAL-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-TP-OGG-RIVAL-NULL(IX-TAB-PMO)
              TO PMO-TP-OGG-RIVAL-NULL
           ELSE
              MOVE (SF)-TP-OGG-RIVAL(IX-TAB-PMO)
              TO PMO-TP-OGG-RIVAL
           END-IF
           IF (SF)-SOM-ASSTA-GARAC-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-SOM-ASSTA-GARAC-NULL(IX-TAB-PMO)
              TO PMO-SOM-ASSTA-GARAC-NULL
           ELSE
              MOVE (SF)-SOM-ASSTA-GARAC(IX-TAB-PMO)
              TO PMO-SOM-ASSTA-GARAC
           END-IF
           IF (SF)-PC-APPLZ-OPZ-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-PC-APPLZ-OPZ-NULL(IX-TAB-PMO)
              TO PMO-PC-APPLZ-OPZ-NULL
           ELSE
              MOVE (SF)-PC-APPLZ-OPZ(IX-TAB-PMO)
              TO PMO-PC-APPLZ-OPZ
           END-IF
           MOVE (SF)-TP-FRM-ASSVA(IX-TAB-PMO)
              TO PMO-TP-FRM-ASSVA
           IF (SF)-DS-RIGA(IX-TAB-PMO) NOT NUMERIC
              MOVE 0 TO PMO-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-PMO)
              TO PMO-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-PMO)
              TO PMO-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-PMO) NOT NUMERIC
              MOVE 0 TO PMO-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-PMO)
              TO PMO-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-PMO) NOT NUMERIC
              MOVE 0 TO PMO-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-PMO)
              TO PMO-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-PMO) NOT NUMERIC
              MOVE 0 TO PMO-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-PMO)
              TO PMO-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-PMO)
              TO PMO-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-PMO)
              TO PMO-DS-STATO-ELAB
           IF (SF)-TP-ESTR-CNT-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-TP-ESTR-CNT-NULL(IX-TAB-PMO)
              TO PMO-TP-ESTR-CNT-NULL
           ELSE
              MOVE (SF)-TP-ESTR-CNT(IX-TAB-PMO)
              TO PMO-TP-ESTR-CNT
           END-IF
           IF (SF)-COD-RAMO-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-COD-RAMO-NULL(IX-TAB-PMO)
              TO PMO-COD-RAMO-NULL
           ELSE
              MOVE (SF)-COD-RAMO(IX-TAB-PMO)
              TO PMO-COD-RAMO
           END-IF
           IF (SF)-GEN-DA-SIN-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-GEN-DA-SIN-NULL(IX-TAB-PMO)
              TO PMO-GEN-DA-SIN-NULL
           ELSE
              MOVE (SF)-GEN-DA-SIN(IX-TAB-PMO)
              TO PMO-GEN-DA-SIN
           END-IF
           IF (SF)-COD-TARI-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-COD-TARI-NULL(IX-TAB-PMO)
              TO PMO-COD-TARI-NULL
           ELSE
              MOVE (SF)-COD-TARI(IX-TAB-PMO)
              TO PMO-COD-TARI
           END-IF
           IF (SF)-NUM-RAT-PAG-PRE-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-NUM-RAT-PAG-PRE-NULL(IX-TAB-PMO)
              TO PMO-NUM-RAT-PAG-PRE-NULL
           ELSE
              MOVE (SF)-NUM-RAT-PAG-PRE(IX-TAB-PMO)
              TO PMO-NUM-RAT-PAG-PRE
           END-IF
           IF (SF)-PC-SERV-VAL-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-PC-SERV-VAL-NULL(IX-TAB-PMO)
              TO PMO-PC-SERV-VAL-NULL
           ELSE
              MOVE (SF)-PC-SERV-VAL(IX-TAB-PMO)
              TO PMO-PC-SERV-VAL
           END-IF
           IF (SF)-ETA-AA-SOGL-BNFICR-NULL(IX-TAB-PMO) = HIGH-VALUES
              MOVE (SF)-ETA-AA-SOGL-BNFICR-NULL(IX-TAB-PMO)
              TO PMO-ETA-AA-SOGL-BNFICR-NULL
           ELSE
              MOVE (SF)-ETA-AA-SOGL-BNFICR(IX-TAB-PMO)
              TO PMO-ETA-AA-SOGL-BNFICR
           END-IF.
       VAL-DCLGEN-PMO-EX.
           EXIT.

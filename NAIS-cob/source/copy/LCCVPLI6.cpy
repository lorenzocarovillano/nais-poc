      *----------------------------------------------------------------*
      *    COPY      ..... LCCVPLI6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO PERCIPIENTE LIQUIDAZIONE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVPLI5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCVPLI1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-PERC-LIQ.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE PERC-LIQ

      *--> NOME TABELLA FISICA DB
           MOVE 'PERC-LIQ'                   TO WK-TABELLA

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WPLI-ST-INV(IX-TAB-PLI)
              AND NOT WPLI-ST-CON(IX-TAB-PLI)
              AND WPLI-ELE-PLI-MAX NOT = 0

              EVALUATE TRUE
      *-->        INSERIMENTO LIQ
                  WHEN WPLI-ST-ADD(IX-TAB-PLI)

      *-->             ESTRAZIONE  SEQUENCE LIQ
                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

      *-->             VALORIZZAZIONE TABELLA LIQ
                       IF IDSV0001-ESITO-OK

                          MOVE WPLI-ID-BNFICR-LIQ(IX-TAB-PLI)
                            TO WS-ID-BEN-LIQU

                          SET  NON-TROVATO    TO TRUE

                          PERFORM SEARCH-ID-PLI
                             THRU SEARCH-ID-PLI-EX
                          VARYING IX-TAB-BEL FROM 1 BY 1
                            UNTIL IX-TAB-BEL > WBEL-ELE-BEN-LIQ-MAX
                               OR TROVATO

                          MOVE WS-PLI-BEN-LIQ
                            TO PLI-ID-BNFICR-LIQ

                          MOVE WPLI-ID-RAPP-ANA(IX-TAB-PLI)
                            TO WS-ID-RAPP-ANA

                          SET  NO-TROVATO          TO TRUE

                          PERFORM SEARCH-ID-RAP-ANA-PLI
                             THRU SEARCH-ID-RAP-ANA-PLI-EX
                          VARYING IX-TAB-RAN FROM 1 BY 1
                            UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX
                               OR SI-TROVATO

                          IF SI-TROVATO
                             MOVE WS-ID-RAPP-ANA
                               TO PLI-ID-RAPP-ANA
                          END-IF

      *-->                VALORIZZAZIONE  SEQUENCE LIQ
                          MOVE WMOV-ID-PTF          TO PLI-ID-MOVI-CRZ
                          MOVE S090-SEQ-TABELLA
                            TO WPLI-ID-PTF(IX-TAB-PLI)
                               PLI-ID-PERC-LIQ
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->    MODIFICA LIQ
                  WHEN WPLI-ST-MOD(IX-TAB-PLI)

                       MOVE WMOV-ID-PTF          TO PLI-ID-MOVI-CRZ
                       MOVE WPLI-ID-PERC-LIQ(IX-TAB-PLI)
                         TO PLI-ID-PERC-LIQ
                       MOVE WPLI-ID-RAPP-ANA(IX-TAB-PLI)
                         TO PLI-ID-RAPP-ANA
                       MOVE WPLI-ID-BNFICR-LIQ(IX-TAB-PLI)
                         TO PLI-ID-BNFICR-LIQ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->    CANCELLAZIONE LIQ
                  WHEN WPLI-ST-DEL(IX-TAB-PLI)

                       MOVE WMOV-ID-PTF          TO PLI-ID-MOVI-CRZ
                       MOVE WPLI-ID-PERC-LIQ(IX-TAB-PLI)
                         TO PLI-ID-PERC-LIQ
                       MOVE WPLI-ID-RAPP-ANA(IX-TAB-PLI)
                         TO PLI-ID-RAPP-ANA
                       MOVE WPLI-ID-BNFICR-LIQ(IX-TAB-PLI)
                         TO PLI-ID-BNFICR-LIQ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN
                 PERFORM VAL-DCLGEN-PLI
                    THRU VAL-DCLGEN-PLI-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-PLI
                    THRU VALORIZZA-AREA-DSH-PLI-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-PERC-LIQ-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-PLI.

      *--> DCLGEN TABELLA
           MOVE PERC-LIQ               TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-ID              TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET IDSI0011-TRATT-DEFAULT TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-PLI-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    CICLO DI ELABORAZIONE SU DCLGEN GARANZIE DI LIQUIDAZIONI
      *----------------------------------------------------------------*
       SEARCH-ID-PLI.

           IF WS-ID-BEN-LIQU = WBEL-ID-BNFICR-LIQ(IX-TAB-BEL)
              SET TROVATO                  TO TRUE
              MOVE WBEL-ID-PTF(IX-TAB-BEL) TO WS-PLI-BEN-LIQ
           END-IF.

       SEARCH-ID-PLI-EX.
           EXIT.
      *---------------------------------------------------------------*
      *    CICLO DI ELABORAZIONE SU DCLGEN RAPP-ANA
      *--------------------------------------------------------------*
       SEARCH-ID-RAP-ANA-PLI.

           IF WS-ID-RAPP-ANA = WRAN-ID-RAPP-ANA(IX-TAB-RAN)
              SET SI-TROVATO                 TO TRUE
              MOVE WRAN-ID-PTF(IX-TAB-RAN)   TO WS-ID-RAPP-ANA
           END-IF.

       SEARCH-ID-RAP-ANA-PLI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVPLI5.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVTCL5
      *   ULTIMO AGG. 02 LUG 2014
      *------------------------------------------------------------

       VAL-DCLGEN-TCL.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TCL)
              TO TCL-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-TCL)
              TO TCL-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-TCL) NOT NUMERIC
              MOVE 0 TO TCL-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-TCL)
              TO TCL-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-TCL) NOT NUMERIC
              MOVE 0 TO TCL-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-TCL)
              TO TCL-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-TCL)
              TO TCL-COD-COMP-ANIA
           IF (SF)-DT-VLT-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-DT-VLT-NULL(IX-TAB-TCL)
              TO TCL-DT-VLT-NULL
           ELSE
             IF (SF)-DT-VLT(IX-TAB-TCL) = ZERO
                MOVE HIGH-VALUES
                TO TCL-DT-VLT-NULL
             ELSE
              MOVE (SF)-DT-VLT(IX-TAB-TCL)
              TO TCL-DT-VLT
             END-IF
           END-IF
           IF (SF)-IMP-LRD-LIQTO-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMP-LRD-LIQTO-NULL(IX-TAB-TCL)
              TO TCL-IMP-LRD-LIQTO-NULL
           ELSE
              MOVE (SF)-IMP-LRD-LIQTO(IX-TAB-TCL)
              TO TCL-IMP-LRD-LIQTO
           END-IF
           IF (SF)-IMP-PREST-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMP-PREST-NULL(IX-TAB-TCL)
              TO TCL-IMP-PREST-NULL
           ELSE
              MOVE (SF)-IMP-PREST(IX-TAB-TCL)
              TO TCL-IMP-PREST
           END-IF
           IF (SF)-IMP-INTR-PREST-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMP-INTR-PREST-NULL(IX-TAB-TCL)
              TO TCL-IMP-INTR-PREST-NULL
           ELSE
              MOVE (SF)-IMP-INTR-PREST(IX-TAB-TCL)
              TO TCL-IMP-INTR-PREST
           END-IF
           IF (SF)-IMP-RAT-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMP-RAT-NULL(IX-TAB-TCL)
              TO TCL-IMP-RAT-NULL
           ELSE
              MOVE (SF)-IMP-RAT(IX-TAB-TCL)
              TO TCL-IMP-RAT
           END-IF
           IF (SF)-IMP-UTI-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMP-UTI-NULL(IX-TAB-TCL)
              TO TCL-IMP-UTI-NULL
           ELSE
              MOVE (SF)-IMP-UTI(IX-TAB-TCL)
              TO TCL-IMP-UTI
           END-IF
           IF (SF)-IMP-RIT-TFR-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMP-RIT-TFR-NULL(IX-TAB-TCL)
              TO TCL-IMP-RIT-TFR-NULL
           ELSE
              MOVE (SF)-IMP-RIT-TFR(IX-TAB-TCL)
              TO TCL-IMP-RIT-TFR
           END-IF
           IF (SF)-IMP-RIT-ACC-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMP-RIT-ACC-NULL(IX-TAB-TCL)
              TO TCL-IMP-RIT-ACC-NULL
           ELSE
              MOVE (SF)-IMP-RIT-ACC(IX-TAB-TCL)
              TO TCL-IMP-RIT-ACC
           END-IF
           IF (SF)-IMP-RIT-VIS-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMP-RIT-VIS-NULL(IX-TAB-TCL)
              TO TCL-IMP-RIT-VIS-NULL
           ELSE
              MOVE (SF)-IMP-RIT-VIS(IX-TAB-TCL)
              TO TCL-IMP-RIT-VIS
           END-IF
           IF (SF)-IMPB-TFR-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPB-TFR-NULL(IX-TAB-TCL)
              TO TCL-IMPB-TFR-NULL
           ELSE
              MOVE (SF)-IMPB-TFR(IX-TAB-TCL)
              TO TCL-IMPB-TFR
           END-IF
           IF (SF)-IMPB-ACC-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPB-ACC-NULL(IX-TAB-TCL)
              TO TCL-IMPB-ACC-NULL
           ELSE
              MOVE (SF)-IMPB-ACC(IX-TAB-TCL)
              TO TCL-IMPB-ACC
           END-IF
           IF (SF)-IMPB-VIS-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-NULL(IX-TAB-TCL)
              TO TCL-IMPB-VIS-NULL
           ELSE
              MOVE (SF)-IMPB-VIS(IX-TAB-TCL)
              TO TCL-IMPB-VIS
           END-IF
           IF (SF)-IMP-RIMB-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMP-RIMB-NULL(IX-TAB-TCL)
              TO TCL-IMP-RIMB-NULL
           ELSE
              MOVE (SF)-IMP-RIMB(IX-TAB-TCL)
              TO TCL-IMP-RIMB
           END-IF
           IF (SF)-IMP-CORTVO-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMP-CORTVO-NULL(IX-TAB-TCL)
              TO TCL-IMP-CORTVO-NULL
           ELSE
              MOVE (SF)-IMP-CORTVO(IX-TAB-TCL)
              TO TCL-IMP-CORTVO
           END-IF
           IF (SF)-IMPB-IMPST-PRVR-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPB-IMPST-PRVR-NULL(IX-TAB-TCL)
              TO TCL-IMPB-IMPST-PRVR-NULL
           ELSE
              MOVE (SF)-IMPB-IMPST-PRVR(IX-TAB-TCL)
              TO TCL-IMPB-IMPST-PRVR
           END-IF
           IF (SF)-IMPST-PRVR-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPST-PRVR-NULL(IX-TAB-TCL)
              TO TCL-IMPST-PRVR-NULL
           ELSE
              MOVE (SF)-IMPST-PRVR(IX-TAB-TCL)
              TO TCL-IMPST-PRVR
           END-IF
           IF (SF)-IMPB-IMPST-252-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPB-IMPST-252-NULL(IX-TAB-TCL)
              TO TCL-IMPB-IMPST-252-NULL
           ELSE
              MOVE (SF)-IMPB-IMPST-252(IX-TAB-TCL)
              TO TCL-IMPB-IMPST-252
           END-IF
           IF (SF)-IMPST-252-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPST-252-NULL(IX-TAB-TCL)
              TO TCL-IMPST-252-NULL
           ELSE
              MOVE (SF)-IMPST-252(IX-TAB-TCL)
              TO TCL-IMPST-252
           END-IF
           IF (SF)-IMP-IS-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMP-IS-NULL(IX-TAB-TCL)
              TO TCL-IMP-IS-NULL
           ELSE
              MOVE (SF)-IMP-IS(IX-TAB-TCL)
              TO TCL-IMP-IS
           END-IF
           IF (SF)-IMP-DIR-LIQ-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMP-DIR-LIQ-NULL(IX-TAB-TCL)
              TO TCL-IMP-DIR-LIQ-NULL
           ELSE
              MOVE (SF)-IMP-DIR-LIQ(IX-TAB-TCL)
              TO TCL-IMP-DIR-LIQ
           END-IF
           IF (SF)-IMP-NET-LIQTO-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMP-NET-LIQTO-NULL(IX-TAB-TCL)
              TO TCL-IMP-NET-LIQTO-NULL
           ELSE
              MOVE (SF)-IMP-NET-LIQTO(IX-TAB-TCL)
              TO TCL-IMP-NET-LIQTO
           END-IF
           IF (SF)-IMP-EFFLQ-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMP-EFFLQ-NULL(IX-TAB-TCL)
              TO TCL-IMP-EFFLQ-NULL
           ELSE
              MOVE (SF)-IMP-EFFLQ(IX-TAB-TCL)
              TO TCL-IMP-EFFLQ
           END-IF
           IF (SF)-COD-DVS-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-COD-DVS-NULL(IX-TAB-TCL)
              TO TCL-COD-DVS-NULL
           ELSE
              MOVE (SF)-COD-DVS(IX-TAB-TCL)
              TO TCL-COD-DVS
           END-IF
           IF (SF)-TP-MEZ-PAG-ACCR-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-TP-MEZ-PAG-ACCR-NULL(IX-TAB-TCL)
              TO TCL-TP-MEZ-PAG-ACCR-NULL
           ELSE
              MOVE (SF)-TP-MEZ-PAG-ACCR(IX-TAB-TCL)
              TO TCL-TP-MEZ-PAG-ACCR
           END-IF
           IF (SF)-ESTR-CNT-CORR-ACCR-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-ESTR-CNT-CORR-ACCR-NULL(IX-TAB-TCL)
              TO TCL-ESTR-CNT-CORR-ACCR-NULL
           ELSE
              MOVE (SF)-ESTR-CNT-CORR-ACCR(IX-TAB-TCL)
              TO TCL-ESTR-CNT-CORR-ACCR
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-TCL) NOT NUMERIC
              MOVE 0 TO TCL-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-TCL)
              TO TCL-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-TCL)
              TO TCL-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-TCL) NOT NUMERIC
              MOVE 0 TO TCL-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-TCL)
              TO TCL-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-TCL) NOT NUMERIC
              MOVE 0 TO TCL-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-TCL)
              TO TCL-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-TCL) NOT NUMERIC
              MOVE 0 TO TCL-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-TCL)
              TO TCL-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-TCL)
              TO TCL-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-TCL)
              TO TCL-DS-STATO-ELAB
           IF (SF)-TP-STAT-TIT-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-TP-STAT-TIT-NULL(IX-TAB-TCL)
              TO TCL-TP-STAT-TIT-NULL
           ELSE
              MOVE (SF)-TP-STAT-TIT(IX-TAB-TCL)
              TO TCL-TP-STAT-TIT
           END-IF
           IF (SF)-IMPB-VIS-1382011-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-1382011-NULL(IX-TAB-TCL)
              TO TCL-IMPB-VIS-1382011-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-1382011(IX-TAB-TCL)
              TO TCL-IMPB-VIS-1382011
           END-IF
           IF (SF)-IMPST-VIS-1382011-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-1382011-NULL(IX-TAB-TCL)
              TO TCL-IMPST-VIS-1382011-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-1382011(IX-TAB-TCL)
              TO TCL-IMPST-VIS-1382011
           END-IF
           IF (SF)-IMPST-SOST-1382011-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPST-SOST-1382011-NULL(IX-TAB-TCL)
              TO TCL-IMPST-SOST-1382011-NULL
           ELSE
              MOVE (SF)-IMPST-SOST-1382011(IX-TAB-TCL)
              TO TCL-IMPST-SOST-1382011
           END-IF
           IF (SF)-IMP-INTR-RIT-PAG-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMP-INTR-RIT-PAG-NULL(IX-TAB-TCL)
              TO TCL-IMP-INTR-RIT-PAG-NULL
           ELSE
              MOVE (SF)-IMP-INTR-RIT-PAG(IX-TAB-TCL)
              TO TCL-IMP-INTR-RIT-PAG
           END-IF
           IF (SF)-IMPB-IS-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPB-IS-NULL(IX-TAB-TCL)
              TO TCL-IMPB-IS-NULL
           ELSE
              MOVE (SF)-IMPB-IS(IX-TAB-TCL)
              TO TCL-IMPB-IS
           END-IF
           IF (SF)-IMPB-IS-1382011-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPB-IS-1382011-NULL(IX-TAB-TCL)
              TO TCL-IMPB-IS-1382011-NULL
           ELSE
              MOVE (SF)-IMPB-IS-1382011(IX-TAB-TCL)
              TO TCL-IMPB-IS-1382011
           END-IF
           IF (SF)-IMPB-VIS-662014-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-662014-NULL(IX-TAB-TCL)
              TO TCL-IMPB-VIS-662014-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-662014(IX-TAB-TCL)
              TO TCL-IMPB-VIS-662014
           END-IF
           IF (SF)-IMPST-VIS-662014-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPST-VIS-662014-NULL(IX-TAB-TCL)
              TO TCL-IMPST-VIS-662014-NULL
           ELSE
              MOVE (SF)-IMPST-VIS-662014(IX-TAB-TCL)
              TO TCL-IMPST-VIS-662014
           END-IF
           IF (SF)-IMPB-IS-662014-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPB-IS-662014-NULL(IX-TAB-TCL)
              TO TCL-IMPB-IS-662014-NULL
           ELSE
              MOVE (SF)-IMPB-IS-662014(IX-TAB-TCL)
              TO TCL-IMPB-IS-662014
           END-IF
           IF (SF)-IMPST-SOST-662014-NULL(IX-TAB-TCL) = HIGH-VALUES
              MOVE (SF)-IMPST-SOST-662014-NULL(IX-TAB-TCL)
              TO TCL-IMPST-SOST-662014-NULL
           ELSE
              MOVE (SF)-IMPST-SOST-662014(IX-TAB-TCL)
              TO TCL-IMPST-SOST-662014
           END-IF.
       VAL-DCLGEN-TCL-EX.
           EXIT.

           EXEC SQL DECLARE AMMB_FUNZ_BLOCCO TABLE
           (
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             COD_CAN             DECIMAL(5, 0) NOT NULL,
             COD_BLOCCO          CHAR(5) NOT NULL,
             TP_MOVI             DECIMAL(5, 0) NOT NULL,
             GRAV_FUNZ_BLOCCO    CHAR(1) NOT NULL,
             DS_OPER_SQL         CHAR(1) NOT NULL,
             DS_VER              DECIMAL(9, 0) NOT NULL,
             DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL,
             DS_UTENTE           CHAR(20) NOT NULL,
             DS_STATO_ELAB       CHAR(1) NOT NULL,
             TP_MOVI_RIFTO       DECIMAL(5, 0)
          ) END-EXEC.

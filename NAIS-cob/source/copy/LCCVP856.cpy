      *----------------------------------------------------------------*
      *    COPY      ..... LCCVP856
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO ESTRATTO CONTO DIAGNOSTICO CED
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVP855 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN ESTRATTO CONTO DIAGN CED (LCCVP851)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-ESTR-CNT-DIAGN-RIV.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE ESTR-CNT-DIAGN-RIV.

      *--> NOME TABELLA FISICA DB
           MOVE 'ESTR-CNT-DIAGN-RIV'            TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WP85-ST-INV(IX-TAB-P85)
              AND WP85-ELE-P85-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->       INSERIMENTO
                 WHEN WP85-ST-ADD(IX-TAB-P85)

      *-->          TIPO OPERAZIONE DISPATCHER
                    SET IDSI0011-INSERT   TO TRUE

      *-->       MODIFICA
                 WHEN WP85-ST-MOD(IX-TAB-P85)

      *-->          TIPO OPERAZIONE DISPATCHER
                    SET IDSI0011-UPDATE   TO TRUE

      *-->       CANCELLAZIONE
                 WHEN WP85-ST-DEL(IX-TAB-P85)

      *-->          TIPO OPERAZIONE DISPATCHER
                    SET IDSI0011-DELETE   TO TRUE

           END-EVALUATE

           IF IDSV0001-ESITO-OK

      *-->    VALORIZZA DCLGEN ESTRATTO CONTO DIAGN CE
              PERFORM VAL-DCLGEN-P85
                 THRU VAL-DCLGEN-P85-EX

      *-->    VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
              PERFORM VALORIZZA-AREA-DSH-P85
                 THRU VALORIZZA-AREA-DSH-P85-EX

      *-->    CALL DISPATCHER PER AGGIORNAMENTO
              PERFORM AGGIORNA-TABELLA
                 THRU AGGIORNA-TABELLA-EX

           END-IF.

       AGGIORNA-ESTR-CNT-DIAGN-RIV-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-P85.

      *--> DCLGEN TABELLA
           MOVE ESTR-CNT-DIAGN-RIV       TO IDSI0011-BUFFER-DATI.

           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-PRIMARY-KEY      TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZERO                     TO IDSI0011-DATA-FINE-EFFETTO
           MOVE ZERO                     TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-P85-EX.
           EXIT.

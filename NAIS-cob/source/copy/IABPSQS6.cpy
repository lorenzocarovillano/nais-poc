      **************************************************************
      * STATEMETS X GESTIONE CON 6 FILES SEQUENZIALI
      * N.B. - DA UTILIZZARE CON LA COPY IABVSQS1
      **************************************************************

       CALL-FILESEQ.

           PERFORM INIZIO-FILESEQ      THRU INIZIO-FILESEQ-EX.

           PERFORM ELABORA-FILESEQ    THRU ELABORA-FILESEQ-EX

           IF SEQUENTIAL-ESITO-OK
              PERFORM FINE-FILESEQ       THRU FINE-FILESEQ-EX
           END-IF.

       CALL-FILESEQ-EX.
           EXIT.


      ******************************************************************
       INIZIO-FILESEQ.

           SET SEQUENTIAL-ESITO-OK   TO TRUE.

       INIZIO-FILESEQ-EX.
           EXIT.

      ******************************************************************
       CHECK-RETURN-CODE-FILESEQ.

           EVALUATE TRUE
               WHEN FILE-STATUS-OK
                    CONTINUE

               WHEN FILE-STATUS-END-OF-FILE
                  IF NOT READ-FILESQS-OPER
                     SET SEQUENTIAL-ESITO-KO  TO TRUE

                     PERFORM COMPONI-MSG-FILESEQ
                                            THRU COMPONI-MSG-FILESEQ-EX
                  END-IF

               WHEN OTHER
                  SET SEQUENTIAL-ESITO-KO  TO TRUE
                  PERFORM COMPONI-MSG-FILESEQ
                                            THRU COMPONI-MSG-FILESEQ-EX

           END-EVALUATE.

       CHECK-RETURN-CODE-FILESEQ-EX.
           EXIT.

      ******************************************************************
       COMPONI-MSG-FILESEQ.

           MOVE FILE-STATUS           TO MSG-RC
           MOVE MSG-ERR-FILE          TO SEQUENTIAL-DESC-ERRORE-ESTESA.

       COMPONI-MSG-FILESEQ-EX.
           EXIT.

      ******************************************************************
       ELABORA-FILESEQ.

           EVALUATE TRUE

              WHEN OPEN-FILESQS-OPER
                 PERFORM OPEN-FILESEQ        THRU OPEN-FILESEQ-EX

              WHEN CLOSE-FILESQS-OPER
                 PERFORM CLOSE-FILESEQ       THRU CLOSE-FILESEQ-EX

              WHEN READ-FILESQS-OPER
                   PERFORM READ-FILESEQ      THRU READ-FILESEQ-EX

              WHEN WRITE-FILESQS-OPER
                 PERFORM WRITE-FILESEQ       THRU WRITE-FILESEQ-EX

              WHEN REWRITE-FILESQS-OPER
                 PERFORM REWRITE-FILESEQ     THRU REWRITE-FILESEQ-EX

              WHEN OTHER
                 SET SEQUENTIAL-ESITO-KO  TO TRUE
                 MOVE 'TIPO OPERAZIONE NON VALIDA'
                                        TO SEQUENTIAL-DESC-ERRORE-ESTESA

           END-EVALUATE.

       ELABORA-FILESEQ-EX.
           EXIT.

      ******************************************************************
       OPEN-FILESEQ.

           SET MSG-OPEN                         TO TRUE.

           IF OPEN-FILESQS1-SI OR
              OPEN-FILESQS-ALL-SI
              PERFORM OPEN-FILESQS1             THRU OPEN-FILESQS1-EX
           END-IF

           IF SEQUENTIAL-ESITO-OK
              IF OPEN-FILESQS2-SI OR
                 OPEN-FILESQS-ALL-SI
                 PERFORM OPEN-FILESQS2          THRU OPEN-FILESQS2-EX
              END-IF

              IF SEQUENTIAL-ESITO-OK
                 IF OPEN-FILESQS3-SI OR
                    OPEN-FILESQS-ALL-SI
                    PERFORM OPEN-FILESQS3       THRU OPEN-FILESQS3-EX
                 END-IF

                 IF SEQUENTIAL-ESITO-OK
                    IF OPEN-FILESQS4-SI OR
                       OPEN-FILESQS-ALL-SI
                       PERFORM OPEN-FILESQS4    THRU OPEN-FILESQS4-EX
                    END-IF

                    IF SEQUENTIAL-ESITO-OK
                       IF OPEN-FILESQS5-SI OR
                          OPEN-FILESQS-ALL-SI
                          PERFORM OPEN-FILESQS5 THRU OPEN-FILESQS5-EX
                       END-IF

                       IF SEQUENTIAL-ESITO-OK
                          IF OPEN-FILESQS6-SI OR
                             OPEN-FILESQS-ALL-SI
                             PERFORM OPEN-FILESQS6
                                                THRU OPEN-FILESQS6-EX
                          END-IF
                       END-IF
                    END-IF
                 END-IF
              END-IF
           END-IF.

       OPEN-FILESEQ-EX.
           EXIT.

      ******************************************************************
       OPEN-FILESQS1.

           MOVE NOME-FILESQS1            TO MSG-NOME-FILE.

           EVALUATE TRUE
              WHEN INPUT-FILESQS1
              WHEN INPUT-FILESQS-ALL
                   OPEN INPUT FILESQS1

              WHEN OUTPUT-FILESQS1
              WHEN OUTPUT-FILESQS-ALL
                   OPEN OUTPUT FILESQS1

              WHEN I-O-FILESQS1
              WHEN I-O-FILESQS-ALL
                   OPEN I-O FILESQS1

              WHEN EXTEND-FILESQS1
              WHEN EXTEND-FILESQS-ALL
                   OPEN EXTEND FILESQS1

              WHEN OTHER
                   SET SEQUENTIAL-ESITO-KO  TO TRUE
                   MOVE 'TIPO OPEN NON VALIDO'
                                        TO SEQUENTIAL-DESC-ERRORE-ESTESA

           END-EVALUATE

           MOVE FS-FILESQS1          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

           IF SEQUENTIAL-ESITO-OK
              SET FILESQS1-APERTO    TO TRUE
              SET FILESQS1-EOF-NO    TO TRUE
           END-IF.

       OPEN-FILESQS1-EX.
           EXIT.

      ******************************************************************
       OPEN-FILESQS2.

           MOVE NOME-FILESQS2        TO MSG-NOME-FILE.

           EVALUATE TRUE
              WHEN INPUT-FILESQS2
              WHEN INPUT-FILESQS-ALL
                   OPEN INPUT FILESQS2

              WHEN OUTPUT-FILESQS2
              WHEN OUTPUT-FILESQS-ALL
                   OPEN OUTPUT FILESQS2

              WHEN I-O-FILESQS2
              WHEN I-O-FILESQS-ALL
                   OPEN I-O FILESQS2

              WHEN EXTEND-FILESQS2
              WHEN EXTEND-FILESQS-ALL
                   OPEN EXTEND FILESQS2

              WHEN OTHER
                   SET SEQUENTIAL-ESITO-KO  TO TRUE
                   MOVE 'TIPO OPEN NON VALIDO'
                                        TO SEQUENTIAL-DESC-ERRORE-ESTESA

           END-EVALUATE

           MOVE FS-FILESQS2          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

           IF SEQUENTIAL-ESITO-OK
              SET FILESQS2-APERTO    TO TRUE
              SET FILESQS2-EOF-NO    TO TRUE
           END-IF.

       OPEN-FILESQS2-EX.
           EXIT.

      ******************************************************************
       OPEN-FILESQS3.

           MOVE NOME-FILESQS3          TO MSG-NOME-FILE.

           EVALUATE TRUE
              WHEN INPUT-FILESQS3
              WHEN INPUT-FILESQS-ALL
                   OPEN INPUT FILESQS3

              WHEN OUTPUT-FILESQS3
              WHEN OUTPUT-FILESQS-ALL
                   OPEN OUTPUT FILESQS3

              WHEN I-O-FILESQS3
              WHEN I-O-FILESQS-ALL
                   OPEN I-O FILESQS3

              WHEN EXTEND-FILESQS3
              WHEN EXTEND-FILESQS-ALL
                   OPEN EXTEND FILESQS3

              WHEN OTHER
                   SET SEQUENTIAL-ESITO-KO  TO TRUE
                   MOVE 'TIPO OPEN NON VALIDO'
                                        TO SEQUENTIAL-DESC-ERRORE-ESTESA

           END-EVALUATE

           MOVE FS-FILESQS3          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

           IF SEQUENTIAL-ESITO-OK
              SET FILESQS3-APERTO    TO TRUE
              SET FILESQS3-EOF-NO    TO TRUE
           END-IF.

       OPEN-FILESQS3-EX.
           EXIT.

      ******************************************************************
       OPEN-FILESQS4.

           MOVE NOME-FILESQS4          TO MSG-NOME-FILE.

           EVALUATE TRUE
              WHEN INPUT-FILESQS4
              WHEN INPUT-FILESQS-ALL
                   OPEN INPUT FILESQS4

              WHEN OUTPUT-FILESQS4
              WHEN OUTPUT-FILESQS-ALL
                   OPEN OUTPUT FILESQS4

              WHEN I-O-FILESQS4
              WHEN I-O-FILESQS-ALL
                   OPEN I-O FILESQS4

              WHEN EXTEND-FILESQS4
              WHEN EXTEND-FILESQS-ALL
                   OPEN EXTEND FILESQS4

              WHEN OTHER
                   SET SEQUENTIAL-ESITO-KO  TO TRUE
                   MOVE 'TIPO OPEN NON VALIDO'
                                        TO SEQUENTIAL-DESC-ERRORE-ESTESA

           END-EVALUATE

           MOVE FS-FILESQS4          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

           IF SEQUENTIAL-ESITO-OK
              SET FILESQS4-APERTO    TO TRUE
              SET FILESQS4-EOF-NO    TO TRUE
           END-IF.

       OPEN-FILESQS4-EX.
           EXIT.

      ******************************************************************
       OPEN-FILESQS5.

           MOVE NOME-FILESQS5          TO MSG-NOME-FILE.

           EVALUATE TRUE
              WHEN INPUT-FILESQS5
              WHEN INPUT-FILESQS-ALL
                   OPEN INPUT FILESQS5

              WHEN OUTPUT-FILESQS5
              WHEN OUTPUT-FILESQS-ALL
                   OPEN OUTPUT FILESQS5

              WHEN I-O-FILESQS5
              WHEN I-O-FILESQS-ALL
                   OPEN I-O FILESQS5

              WHEN EXTEND-FILESQS5
              WHEN EXTEND-FILESQS-ALL
                   OPEN EXTEND FILESQS5

              WHEN OTHER
                   SET SEQUENTIAL-ESITO-KO  TO TRUE
                   MOVE 'TIPO OPEN NON VALIDO'
                                        TO SEQUENTIAL-DESC-ERRORE-ESTESA

           END-EVALUATE

           MOVE FS-FILESQS5          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

           IF SEQUENTIAL-ESITO-OK
              SET FILESQS5-APERTO    TO TRUE
              SET FILESQS5-EOF-NO    TO TRUE
           END-IF.

       OPEN-FILESQS5-EX.
           EXIT.

      ******************************************************************
       OPEN-FILESQS6.

           MOVE NOME-FILESQS6          TO MSG-NOME-FILE.

           EVALUATE TRUE
              WHEN INPUT-FILESQS6
              WHEN INPUT-FILESQS-ALL
                   OPEN INPUT FILESQS6

              WHEN OUTPUT-FILESQS6
              WHEN OUTPUT-FILESQS-ALL
                   OPEN OUTPUT FILESQS6

              WHEN I-O-FILESQS6
              WHEN I-O-FILESQS-ALL
                   OPEN I-O FILESQS6

              WHEN EXTEND-FILESQS6
              WHEN EXTEND-FILESQS-ALL
                   OPEN EXTEND FILESQS6

              WHEN OTHER
                   SET SEQUENTIAL-ESITO-KO  TO TRUE
                   MOVE 'TIPO OPEN NON VALIDO'
                                        TO SEQUENTIAL-DESC-ERRORE-ESTESA

           END-EVALUATE

           MOVE FS-FILESQS6          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

           IF SEQUENTIAL-ESITO-OK
              SET FILESQS6-APERTO    TO TRUE
              SET FILESQS6-EOF-NO    TO TRUE
           END-IF.

       OPEN-FILESQS6-EX.
           EXIT.

      ******************************************************************
       CLOSE-FILESEQ.

           SET MSG-CLOSE                      TO TRUE.

           IF CLOSE-FILESQS1-SI OR
              CLOSE-FILESQS-ALL-SI
              PERFORM CLOSE-FILESQS1          THRU CLOSE-FILESQS1-EX
           END-IF

           IF SEQUENTIAL-ESITO-OK
              IF CLOSE-FILESQS2-SI OR
                 CLOSE-FILESQS-ALL-SI
                 PERFORM CLOSE-FILESQS2       THRU CLOSE-FILESQS2-EX
              END-IF

              IF SEQUENTIAL-ESITO-OK
                 IF CLOSE-FILESQS3-SI OR
                    CLOSE-FILESQS-ALL-SI
                    PERFORM CLOSE-FILESQS3    THRU CLOSE-FILESQS3-EX
                 END-IF

                 IF SEQUENTIAL-ESITO-OK
                    IF CLOSE-FILESQS4-SI OR
                       CLOSE-FILESQS-ALL-SI
                       PERFORM CLOSE-FILESQS4 THRU CLOSE-FILESQS4-EX
                    END-IF

                    IF SEQUENTIAL-ESITO-OK
                       IF CLOSE-FILESQS5-SI OR
                          CLOSE-FILESQS-ALL-SI
                          PERFORM CLOSE-FILESQS5
                                              THRU CLOSE-FILESQS5-EX
                       END-IF

                       IF SEQUENTIAL-ESITO-OK
                          IF CLOSE-FILESQS6-SI OR
                             CLOSE-FILESQS-ALL-SI
                             PERFORM CLOSE-FILESQS6
                                              THRU CLOSE-FILESQS6-EX
                          END-IF
                       END-IF
                    END-IF
                 END-IF
              END-IF
           END-IF.

       CLOSE-FILESEQ-EX.
           EXIT.

      ******************************************************************
       CLOSE-FILESQS1.

           IF FILESQS1-APERTO

              MOVE NOME-FILESQS1        TO MSG-NOME-FILE

              CLOSE FILESQS1

              MOVE FS-FILESQS1          TO FILE-STATUS

              PERFORM CHECK-RETURN-CODE-FILESEQ
                      THRU CHECK-RETURN-CODE-FILESEQ-EX

              IF SEQUENTIAL-ESITO-OK
                 SET FILESQS1-CHIUSO    TO TRUE
              END-IF

           END-IF.

       CLOSE-FILESQS1-EX.
           EXIT.

      ******************************************************************
       CLOSE-FILESQS2.

           IF FILESQS2-APERTO

              MOVE NOME-FILESQS2       TO MSG-NOME-FILE

              CLOSE FILESQS2

              MOVE FS-FILESQS2         TO FILE-STATUS

              PERFORM CHECK-RETURN-CODE-FILESEQ
                      THRU CHECK-RETURN-CODE-FILESEQ-EX

              IF SEQUENTIAL-ESITO-OK
                 SET FILESQS2-CHIUSO    TO TRUE
              END-IF

           END-IF.

       CLOSE-FILESQS2-EX.
           EXIT.

      ******************************************************************
       CLOSE-FILESQS3.

           IF FILESQS3-APERTO

              MOVE NOME-FILESQS3        TO MSG-NOME-FILE

              CLOSE FILESQS3

              MOVE FS-FILESQS3          TO FILE-STATUS

              PERFORM CHECK-RETURN-CODE-FILESEQ
                      THRU CHECK-RETURN-CODE-FILESEQ-EX

              IF SEQUENTIAL-ESITO-OK
                 SET FILESQS3-CHIUSO    TO TRUE
              END-IF

           END-IF.

       CLOSE-FILESQS3-EX.
           EXIT.

      ******************************************************************
       CLOSE-FILESQS4.

           IF FILESQS4-APERTO

              MOVE NOME-FILESQS4        TO MSG-NOME-FILE

              CLOSE FILESQS4

              MOVE FS-FILESQS4          TO FILE-STATUS

              PERFORM CHECK-RETURN-CODE-FILESEQ
                      THRU CHECK-RETURN-CODE-FILESEQ-EX

              IF SEQUENTIAL-ESITO-OK
                 SET FILESQS4-CHIUSO    TO TRUE
              END-IF

           END-IF.

       CLOSE-FILESQS4-EX.
           EXIT.

      ******************************************************************
       CLOSE-FILESQS5.

           IF FILESQS5-APERTO

              MOVE NOME-FILESQS5        TO MSG-NOME-FILE

              CLOSE FILESQS5

              MOVE FS-FILESQS5          TO FILE-STATUS

              PERFORM CHECK-RETURN-CODE-FILESEQ
                      THRU CHECK-RETURN-CODE-FILESEQ-EX

              IF SEQUENTIAL-ESITO-OK
                 SET FILESQS5-CHIUSO    TO TRUE
              END-IF

           END-IF.

       CLOSE-FILESQS5-EX.
           EXIT.

      ******************************************************************
       CLOSE-FILESQS6.

           IF FILESQS6-APERTO

              MOVE NOME-FILESQS6        TO MSG-NOME-FILE

              CLOSE FILESQS6

              MOVE FS-FILESQS6          TO FILE-STATUS

              PERFORM CHECK-RETURN-CODE-FILESEQ
                      THRU CHECK-RETURN-CODE-FILESEQ-EX

              IF SEQUENTIAL-ESITO-OK
                 SET FILESQS6-CHIUSO    TO TRUE
              END-IF

           END-IF.

       CLOSE-FILESQS6-EX.
           EXIT.

      ******************************************************************
       READ-FILESEQ.

           SET MSG-READ                      TO TRUE.

           IF READ-FILESQS1-SI OR
              READ-FILESQS-ALL-SI
              PERFORM READ-FILESQS1          THRU READ-FILESQS1-EX
           END-IF

           IF SEQUENTIAL-ESITO-OK
              IF READ-FILESQS2-SI OR
                 READ-FILESQS-ALL-SI
                 PERFORM READ-FILESQS2       THRU READ-FILESQS2-EX
              END-IF

              IF SEQUENTIAL-ESITO-OK
                 IF READ-FILESQS3-SI OR
                    READ-FILESQS-ALL-SI
                    PERFORM READ-FILESQS3    THRU READ-FILESQS3-EX
                 END-IF

                 IF SEQUENTIAL-ESITO-OK
                    IF READ-FILESQS4-SI OR
                       READ-FILESQS-ALL-SI
                       PERFORM READ-FILESQS4 THRU READ-FILESQS4-EX
                    END-IF

                    IF SEQUENTIAL-ESITO-OK
                       IF READ-FILESQS5-SI OR
                          READ-FILESQS-ALL-SI
                          PERFORM READ-FILESQS5
                                             THRU READ-FILESQS5-EX
                       END-IF

                       IF SEQUENTIAL-ESITO-OK
                          IF READ-FILESQS6-SI OR
                             READ-FILESQS-ALL-SI
                             PERFORM READ-FILESQS6
                                             THRU READ-FILESQS6-EX
                          END-IF
                       END-IF
                    END-IF
                 END-IF
              END-IF
           END-IF.

       READ-FILESEQ-EX.
           EXIT.

      ******************************************************************
       READ-FILESQS1.

           MOVE NOME-FILESQS1        TO MSG-NOME-FILE.

           READ FILESQS1.

           MOVE FS-FILESQS1          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

           IF SEQUENTIAL-ESITO-OK

              IF FILE-STATUS-END-OF-FILE

                 SET FILESQS1-EOF-SI              TO TRUE
                 SET CLOSE-FILESQS1-SI            TO TRUE

                 PERFORM CLOSE-FILESEQ            THRU CLOSE-FILESEQ-EX

                 IF SEQUENTIAL-ESITO-OK
                    SET FILE-STATUS-END-OF-FILE   TO TRUE
                 END-IF

              END-IF

           END-IF.

       READ-FILESQS1-EX.
           EXIT.

      ******************************************************************
       READ-FILESQS2.

           MOVE NOME-FILESQS2        TO MSG-NOME-FILE.

           READ FILESQS2.

           MOVE FS-FILESQS2          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

           IF SEQUENTIAL-ESITO-OK

              IF FILE-STATUS-END-OF-FILE

                 SET FILESQS2-EOF-SI              TO TRUE
                 SET CLOSE-FILESQS2-SI            TO TRUE

                 PERFORM CLOSE-FILESEQ            THRU CLOSE-FILESEQ-EX

                 IF SEQUENTIAL-ESITO-OK
                    SET FILE-STATUS-END-OF-FILE   TO TRUE
                 END-IF

              END-IF

           END-IF.

       READ-FILESQS2-EX.
           EXIT.

      ******************************************************************
       READ-FILESQS3.

           MOVE NOME-FILESQS3        TO MSG-NOME-FILE.

           READ FILESQS3.

           MOVE FS-FILESQS3          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.
           IF SEQUENTIAL-ESITO-OK

              IF FILE-STATUS-END-OF-FILE

                 SET FILESQS3-EOF-SI              TO TRUE
                 SET CLOSE-FILESQS3-SI            TO TRUE

                 PERFORM CLOSE-FILESEQ            THRU CLOSE-FILESEQ-EX

                 IF SEQUENTIAL-ESITO-OK
                    SET FILE-STATUS-END-OF-FILE   TO TRUE
                 END-IF

              END-IF

           END-IF.

       READ-FILESQS3-EX.
           EXIT.

      ******************************************************************
       READ-FILESQS4.

           MOVE NOME-FILESQS4        TO MSG-NOME-FILE.

           READ FILESQS4.

           MOVE FS-FILESQS4          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

           IF SEQUENTIAL-ESITO-OK

              IF FILE-STATUS-END-OF-FILE

                 SET FILESQS4-EOF-SI              TO TRUE
                 SET CLOSE-FILESQS4-SI            TO TRUE

                 PERFORM CLOSE-FILESEQ            THRU CLOSE-FILESEQ-EX

                 IF SEQUENTIAL-ESITO-OK
                    SET FILE-STATUS-END-OF-FILE   TO TRUE
                 END-IF

              END-IF

           END-IF.

        READ-FILESQS4-EX.
           EXIT.

      ******************************************************************
       READ-FILESQS5.

           MOVE NOME-FILESQS5        TO MSG-NOME-FILE.

           READ FILESQS5.

           MOVE FS-FILESQS5          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

           IF SEQUENTIAL-ESITO-OK

              IF FILE-STATUS-END-OF-FILE

                 SET FILESQS5-EOF-SI              TO TRUE
                 SET CLOSE-FILESQS5-SI            TO TRUE

                 PERFORM CLOSE-FILESEQ            THRU CLOSE-FILESEQ-EX

                 IF SEQUENTIAL-ESITO-OK
                    SET FILE-STATUS-END-OF-FILE   TO TRUE
                 END-IF

              END-IF

           END-IF.

       READ-FILESQS5-EX.
           EXIT.

      ******************************************************************
       READ-FILESQS6.

           MOVE NOME-FILESQS6        TO MSG-NOME-FILE.

           READ FILESQS6.

           MOVE FS-FILESQS6          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

           IF SEQUENTIAL-ESITO-OK

              IF FILE-STATUS-END-OF-FILE

                 SET FILESQS6-EOF-SI              TO TRUE
                 SET CLOSE-FILESQS6-SI            TO TRUE

                 PERFORM CLOSE-FILESEQ            THRU CLOSE-FILESEQ-EX

                 IF SEQUENTIAL-ESITO-OK
                    SET FILE-STATUS-END-OF-FILE   TO TRUE
                 END-IF

              END-IF

           END-IF.

       READ-FILESQS6-EX.
           EXIT.

      ******************************************************************
       WRITE-FILESEQ.

           SET MSG-WRITE                      TO TRUE.

           IF WRITE-FILESQS1-SI OR
              WRITE-FILESQS-ALL-SI
              PERFORM WRITE-FILESQS1          THRU WRITE-FILESQS1-EX
           END-IF

           IF SEQUENTIAL-ESITO-OK
              IF WRITE-FILESQS2-SI OR
                 WRITE-FILESQS-ALL-SI
                 PERFORM WRITE-FILESQS2       THRU WRITE-FILESQS2-EX
              END-IF

              IF SEQUENTIAL-ESITO-OK
                 IF WRITE-FILESQS3-SI OR
                    WRITE-FILESQS-ALL-SI
                    PERFORM WRITE-FILESQS3    THRU WRITE-FILESQS3-EX
                 END-IF

                 IF SEQUENTIAL-ESITO-OK
                    IF WRITE-FILESQS4-SI OR
                       WRITE-FILESQS-ALL-SI
                       PERFORM WRITE-FILESQS4 THRU WRITE-FILESQS4-EX
                    END-IF

                    IF SEQUENTIAL-ESITO-OK
                       IF WRITE-FILESQS5-SI OR
                          WRITE-FILESQS-ALL-SI
                          PERFORM WRITE-FILESQS5
                                              THRU WRITE-FILESQS5-EX
                       END-IF

                       IF SEQUENTIAL-ESITO-OK
                          IF WRITE-FILESQS6-SI OR
                             WRITE-FILESQS-ALL-SI
                             PERFORM WRITE-FILESQS6
                                              THRU WRITE-FILESQS6-EX
                          END-IF
                       END-IF
                    END-IF
                 END-IF
              END-IF
           END-IF.

       WRITE-FILESEQ-EX.
           EXIT.

      ******************************************************************
       WRITE-FILESQS1.

           MOVE NOME-FILESQS1        TO MSG-NOME-FILE.

           WRITE FILESQS1-REC.

           MOVE FS-FILESQS1          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

       WRITE-FILESQS1-EX.
           EXIT.

      ******************************************************************
       WRITE-FILESQS2.

           MOVE NOME-FILESQS2        TO MSG-NOME-FILE.

           WRITE FILESQS2-REC.

           MOVE FS-FILESQS2          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

       WRITE-FILESQS2-EX.
           EXIT.

      ******************************************************************
       WRITE-FILESQS3.

           MOVE NOME-FILESQS3        TO MSG-NOME-FILE.

           WRITE FILESQS3-REC.

           MOVE FS-FILESQS3          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

       WRITE-FILESQS3-EX.
           EXIT.

      ******************************************************************
       WRITE-FILESQS4.

           MOVE NOME-FILESQS4        TO MSG-NOME-FILE.

           WRITE FILESQS4-REC.

           MOVE FS-FILESQS4          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

        WRITE-FILESQS4-EX.
           EXIT.

      ******************************************************************
       WRITE-FILESQS5.

           MOVE NOME-FILESQS5        TO MSG-NOME-FILE.

           WRITE FILESQS5-REC.

           MOVE FS-FILESQS5          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

       WRITE-FILESQS5-EX.
           EXIT.

      ******************************************************************
       WRITE-FILESQS6.

           MOVE NOME-FILESQS6        TO MSG-NOME-FILE.

           WRITE FILESQS6-REC.

           MOVE FS-FILESQS6          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

       WRITE-FILESQS6-EX.
           EXIT.

      ******************************************************************
       REWRITE-FILESEQ.

           SET MSG-REWRITE                   TO TRUE.

           IF REWRITE-FILESQS1-SI OR
              REWRITE-FILESQS-ALL-SI
              PERFORM REWRITE-FILESQS1       THRU REWRITE-FILESQS1-EX
           END-IF

           IF SEQUENTIAL-ESITO-OK
              IF REWRITE-FILESQS2-SI OR
                 REWRITE-FILESQS-ALL-SI
                 PERFORM REWRITE-FILESQS2    THRU REWRITE-FILESQS2-EX
              END-IF

              IF SEQUENTIAL-ESITO-OK
                 IF REWRITE-FILESQS3-SI OR
                    REWRITE-FILESQS-ALL-SI
                    PERFORM REWRITE-FILESQS3 THRU REWRITE-FILESQS3-EX
                 END-IF

                 IF SEQUENTIAL-ESITO-OK
                    IF REWRITE-FILESQS4-SI OR
                       REWRITE-FILESQS-ALL-SI
                       PERFORM REWRITE-FILESQS4
                                             THRU REWRITE-FILESQS4-EX
                    END-IF

                    IF SEQUENTIAL-ESITO-OK
                       IF REWRITE-FILESQS5-SI OR
                          REWRITE-FILESQS-ALL-SI
                          PERFORM REWRITE-FILESQS5
                                             THRU REWRITE-FILESQS5-EX
                       END-IF

                       IF SEQUENTIAL-ESITO-OK
                          IF REWRITE-FILESQS6-SI OR
                             REWRITE-FILESQS-ALL-SI
                             PERFORM REWRITE-FILESQS6
                                             THRU REWRITE-FILESQS6-EX
                          END-IF
                       END-IF
                    END-IF
                 END-IF
              END-IF
           END-IF.

       REWRITE-FILESEQ-EX.
           EXIT.

      ******************************************************************
       REWRITE-FILESQS1.

           MOVE NOME-FILESQS1        TO MSG-NOME-FILE.

           REWRITE FILESQS1-REC.

           MOVE FS-FILESQS1          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

       REWRITE-FILESQS1-EX.
           EXIT.

      ******************************************************************
       REWRITE-FILESQS2.

           MOVE NOME-FILESQS2        TO MSG-NOME-FILE.

           REWRITE FILESQS2-REC.

           MOVE FS-FILESQS2          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

       REWRITE-FILESQS2-EX.
           EXIT.

      ******************************************************************
       REWRITE-FILESQS3.

           MOVE NOME-FILESQS3        TO MSG-NOME-FILE.

           REWRITE FILESQS3-REC.

           MOVE FS-FILESQS3          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

       REWRITE-FILESQS3-EX.
           EXIT.

      ******************************************************************
       REWRITE-FILESQS4.

           MOVE NOME-FILESQS4        TO MSG-NOME-FILE.

           REWRITE FILESQS4-REC.

           MOVE FS-FILESQS4          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

        REWRITE-FILESQS4-EX.
           EXIT.

      ******************************************************************
       REWRITE-FILESQS5.

           MOVE NOME-FILESQS5        TO MSG-NOME-FILE.

           REWRITE FILESQS5-REC.

           MOVE FS-FILESQS5          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

       REWRITE-FILESQS5-EX.
           EXIT.

      ******************************************************************
       REWRITE-FILESQS6.

           MOVE NOME-FILESQS6        TO MSG-NOME-FILE.

           REWRITE FILESQS6-REC.

           MOVE FS-FILESQS6          TO FILE-STATUS.

           PERFORM CHECK-RETURN-CODE-FILESEQ
                                     THRU CHECK-RETURN-CODE-FILESEQ-EX.

       REWRITE-FILESQS6-EX.
           EXIT.

      ******************************************************************
       FINE-FILESEQ.

           SET READ-FILESQS1-NO       TO TRUE.
           SET READ-FILESQS2-NO       TO TRUE.
           SET READ-FILESQS3-NO       TO TRUE.
           SET READ-FILESQS4-NO       TO TRUE.
           SET READ-FILESQS5-NO       TO TRUE.
           SET READ-FILESQS6-NO       TO TRUE.
           SET READ-FILESQS-ALL-NO    TO TRUE.

           SET WRITE-FILESQS1-NO      TO TRUE.
           SET WRITE-FILESQS2-NO      TO TRUE.
           SET WRITE-FILESQS3-NO      TO TRUE.
           SET WRITE-FILESQS4-NO      TO TRUE.
           SET WRITE-FILESQS5-NO      TO TRUE.
           SET WRITE-FILESQS6-NO      TO TRUE.
           SET WRITE-FILESQS-ALL-NO   TO TRUE.

           SET REWRITE-FILESQS1-NO    TO TRUE.
           SET REWRITE-FILESQS2-NO    TO TRUE.
           SET REWRITE-FILESQS3-NO    TO TRUE.
           SET REWRITE-FILESQS4-NO    TO TRUE.
           SET REWRITE-FILESQS5-NO    TO TRUE.
           SET REWRITE-FILESQS6-NO    TO TRUE.
           SET REWRITE-FILESQS-ALL-NO TO TRUE.

       FINE-FILESEQ-EX.
           EXIT.

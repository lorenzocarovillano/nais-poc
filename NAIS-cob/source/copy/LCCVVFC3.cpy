
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVVFC3
      *   ULTIMO AGG. 18 SET 2007
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-VFC.
           MOVE VFC-IDCOMP
             TO (SF)-IDCOMP
           MOVE VFC-CODPROD
             TO (SF)-CODPROD
           MOVE VFC-CODTARI
             TO (SF)-CODTARI
           MOVE VFC-DINIZ
             TO (SF)-DINIZ
           MOVE VFC-NOMEFUNZ
             TO (SF)-NOMEFUNZ
           MOVE VFC-DEND
             TO (SF)-DEND
           IF VFC-ISPRECALC-NULL = HIGH-VALUES
              MOVE VFC-ISPRECALC-NULL
                TO (SF)-ISPRECALC-NULL
           ELSE
              MOVE VFC-ISPRECALC
                TO (SF)-ISPRECALC
           END-IF
           MOVE VFC-NOMEPROGR
             TO (SF)-NOMEPROGR
           MOVE VFC-MODCALC
             TO (SF)-MODCALC
           MOVE VFC-GLOVARLIST
             TO (SF)-GLOVARLIST
           MOVE VFC-STEP-ELAB
             TO (SF)-STEP-ELAB.
       VALORIZZA-OUTPUT-VFC-EX.
           EXIT.

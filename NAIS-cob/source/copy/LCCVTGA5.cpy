
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVTGA5
      *   ULTIMO AGG. 03 GIU 2019
      *------------------------------------------------------------

       VAL-DCLGEN-TGA.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TGA)
              TO TGA-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-TGA)
              TO TGA-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-TGA) NOT NUMERIC
              MOVE 0 TO TGA-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-TGA)
              TO TGA-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-TGA) NOT NUMERIC
              MOVE 0 TO TGA-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-TGA)
              TO TGA-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-TGA)
              TO TGA-COD-COMP-ANIA
           MOVE (SF)-DT-DECOR(IX-TAB-TGA)
              TO TGA-DT-DECOR
           IF (SF)-DT-SCAD-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-DT-SCAD-NULL(IX-TAB-TGA)
              TO TGA-DT-SCAD-NULL
           ELSE
             IF (SF)-DT-SCAD(IX-TAB-TGA) = ZERO
                MOVE HIGH-VALUES
                TO TGA-DT-SCAD-NULL
             ELSE
              MOVE (SF)-DT-SCAD(IX-TAB-TGA)
              TO TGA-DT-SCAD
             END-IF
           END-IF
           IF (SF)-IB-OGG-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IB-OGG-NULL(IX-TAB-TGA)
              TO TGA-IB-OGG-NULL
           ELSE
              MOVE (SF)-IB-OGG(IX-TAB-TGA)
              TO TGA-IB-OGG
           END-IF
           MOVE (SF)-TP-RGM-FISC(IX-TAB-TGA)
              TO TGA-TP-RGM-FISC
           IF (SF)-DT-EMIS-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-DT-EMIS-NULL(IX-TAB-TGA)
              TO TGA-DT-EMIS-NULL
           ELSE
             IF (SF)-DT-EMIS(IX-TAB-TGA) = ZERO
                MOVE HIGH-VALUES
                TO TGA-DT-EMIS-NULL
             ELSE
              MOVE (SF)-DT-EMIS(IX-TAB-TGA)
              TO TGA-DT-EMIS
             END-IF
           END-IF
           MOVE (SF)-TP-TRCH(IX-TAB-TGA)
              TO TGA-TP-TRCH
           IF (SF)-DUR-AA-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-DUR-AA-NULL(IX-TAB-TGA)
              TO TGA-DUR-AA-NULL
           ELSE
              MOVE (SF)-DUR-AA(IX-TAB-TGA)
              TO TGA-DUR-AA
           END-IF
           IF (SF)-DUR-MM-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-DUR-MM-NULL(IX-TAB-TGA)
              TO TGA-DUR-MM-NULL
           ELSE
              MOVE (SF)-DUR-MM(IX-TAB-TGA)
              TO TGA-DUR-MM
           END-IF
           IF (SF)-DUR-GG-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-DUR-GG-NULL(IX-TAB-TGA)
              TO TGA-DUR-GG-NULL
           ELSE
              MOVE (SF)-DUR-GG(IX-TAB-TGA)
              TO TGA-DUR-GG
           END-IF
           IF (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRE-CASO-MOR-NULL(IX-TAB-TGA)
              TO TGA-PRE-CASO-MOR-NULL
           ELSE
              MOVE (SF)-PRE-CASO-MOR(IX-TAB-TGA)
              TO TGA-PRE-CASO-MOR
           END-IF
           IF (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PC-INTR-RIAT-NULL(IX-TAB-TGA)
              TO TGA-PC-INTR-RIAT-NULL
           ELSE
              MOVE (SF)-PC-INTR-RIAT(IX-TAB-TGA)
              TO TGA-PC-INTR-RIAT
           END-IF
           IF (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-BNS-ANTIC-NULL(IX-TAB-TGA)
              TO TGA-IMP-BNS-ANTIC-NULL
           ELSE
              MOVE (SF)-IMP-BNS-ANTIC(IX-TAB-TGA)
              TO TGA-IMP-BNS-ANTIC
           END-IF
           IF (SF)-PRE-INI-NET-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRE-INI-NET-NULL(IX-TAB-TGA)
              TO TGA-PRE-INI-NET-NULL
           ELSE
              MOVE (SF)-PRE-INI-NET(IX-TAB-TGA)
              TO TGA-PRE-INI-NET
           END-IF
           IF (SF)-PRE-PP-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRE-PP-INI-NULL(IX-TAB-TGA)
              TO TGA-PRE-PP-INI-NULL
           ELSE
              MOVE (SF)-PRE-PP-INI(IX-TAB-TGA)
              TO TGA-PRE-PP-INI
           END-IF
           IF (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRE-PP-ULT-NULL(IX-TAB-TGA)
              TO TGA-PRE-PP-ULT-NULL
           ELSE
              MOVE (SF)-PRE-PP-ULT(IX-TAB-TGA)
              TO TGA-PRE-PP-ULT
           END-IF
           IF (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRE-TARI-INI-NULL(IX-TAB-TGA)
              TO TGA-PRE-TARI-INI-NULL
           ELSE
              MOVE (SF)-PRE-TARI-INI(IX-TAB-TGA)
              TO TGA-PRE-TARI-INI
           END-IF
           IF (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRE-TARI-ULT-NULL(IX-TAB-TGA)
              TO TGA-PRE-TARI-ULT-NULL
           ELSE
              MOVE (SF)-PRE-TARI-ULT(IX-TAB-TGA)
              TO TGA-PRE-TARI-ULT
           END-IF
           IF (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRE-INVRIO-INI-NULL(IX-TAB-TGA)
              TO TGA-PRE-INVRIO-INI-NULL
           ELSE
              MOVE (SF)-PRE-INVRIO-INI(IX-TAB-TGA)
              TO TGA-PRE-INVRIO-INI
           END-IF
           IF (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRE-INVRIO-ULT-NULL(IX-TAB-TGA)
              TO TGA-PRE-INVRIO-ULT-NULL
           ELSE
              MOVE (SF)-PRE-INVRIO-ULT(IX-TAB-TGA)
              TO TGA-PRE-INVRIO-ULT
           END-IF
           IF (SF)-PRE-RIVTO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRE-RIVTO-NULL(IX-TAB-TGA)
              TO TGA-PRE-RIVTO-NULL
           ELSE
              MOVE (SF)-PRE-RIVTO(IX-TAB-TGA)
              TO TGA-PRE-RIVTO
           END-IF
           IF (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-SOPR-PROF-NULL(IX-TAB-TGA)
              TO TGA-IMP-SOPR-PROF-NULL
           ELSE
              MOVE (SF)-IMP-SOPR-PROF(IX-TAB-TGA)
              TO TGA-IMP-SOPR-PROF
           END-IF
           IF (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-SOPR-SAN-NULL(IX-TAB-TGA)
              TO TGA-IMP-SOPR-SAN-NULL
           ELSE
              MOVE (SF)-IMP-SOPR-SAN(IX-TAB-TGA)
              TO TGA-IMP-SOPR-SAN
           END-IF
           IF (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-SOPR-SPO-NULL(IX-TAB-TGA)
              TO TGA-IMP-SOPR-SPO-NULL
           ELSE
              MOVE (SF)-IMP-SOPR-SPO(IX-TAB-TGA)
              TO TGA-IMP-SOPR-SPO
           END-IF
           IF (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-SOPR-TEC-NULL(IX-TAB-TGA)
              TO TGA-IMP-SOPR-TEC-NULL
           ELSE
              MOVE (SF)-IMP-SOPR-TEC(IX-TAB-TGA)
              TO TGA-IMP-SOPR-TEC
           END-IF
           IF (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-ALT-SOPR-NULL(IX-TAB-TGA)
              TO TGA-IMP-ALT-SOPR-NULL
           ELSE
              MOVE (SF)-IMP-ALT-SOPR(IX-TAB-TGA)
              TO TGA-IMP-ALT-SOPR
           END-IF
           IF (SF)-PRE-STAB-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRE-STAB-NULL(IX-TAB-TGA)
              TO TGA-PRE-STAB-NULL
           ELSE
              MOVE (SF)-PRE-STAB(IX-TAB-TGA)
              TO TGA-PRE-STAB
           END-IF
           IF (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-DT-EFF-STAB-NULL(IX-TAB-TGA)
              TO TGA-DT-EFF-STAB-NULL
           ELSE
             IF (SF)-DT-EFF-STAB(IX-TAB-TGA) = ZERO
                MOVE HIGH-VALUES
                TO TGA-DT-EFF-STAB-NULL
             ELSE
              MOVE (SF)-DT-EFF-STAB(IX-TAB-TGA)
              TO TGA-DT-EFF-STAB
             END-IF
           END-IF
           IF (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-TS-RIVAL-FIS-NULL(IX-TAB-TGA)
              TO TGA-TS-RIVAL-FIS-NULL
           ELSE
              MOVE (SF)-TS-RIVAL-FIS(IX-TAB-TGA)
              TO TGA-TS-RIVAL-FIS
           END-IF
           IF (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-TS-RIVAL-INDICIZ-NULL(IX-TAB-TGA)
              TO TGA-TS-RIVAL-INDICIZ-NULL
           ELSE
              MOVE (SF)-TS-RIVAL-INDICIZ(IX-TAB-TGA)
              TO TGA-TS-RIVAL-INDICIZ
           END-IF
           IF (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-OLD-TS-TEC-NULL(IX-TAB-TGA)
              TO TGA-OLD-TS-TEC-NULL
           ELSE
              MOVE (SF)-OLD-TS-TEC(IX-TAB-TGA)
              TO TGA-OLD-TS-TEC
           END-IF
           IF (SF)-RAT-LRD-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-RAT-LRD-NULL(IX-TAB-TGA)
              TO TGA-RAT-LRD-NULL
           ELSE
              MOVE (SF)-RAT-LRD(IX-TAB-TGA)
              TO TGA-RAT-LRD
           END-IF
           IF (SF)-PRE-LRD-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRE-LRD-NULL(IX-TAB-TGA)
              TO TGA-PRE-LRD-NULL
           ELSE
              MOVE (SF)-PRE-LRD(IX-TAB-TGA)
              TO TGA-PRE-LRD
           END-IF
           IF (SF)-PRSTZ-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRSTZ-INI-NULL(IX-TAB-TGA)
              TO TGA-PRSTZ-INI-NULL
           ELSE
              MOVE (SF)-PRSTZ-INI(IX-TAB-TGA)
              TO TGA-PRSTZ-INI
           END-IF
           IF (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRSTZ-ULT-NULL(IX-TAB-TGA)
              TO TGA-PRSTZ-ULT-NULL
           ELSE
              MOVE (SF)-PRSTZ-ULT(IX-TAB-TGA)
              TO TGA-PRSTZ-ULT
           END-IF
           IF (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-CPT-IN-OPZ-RIVTO-NULL(IX-TAB-TGA)
              TO TGA-CPT-IN-OPZ-RIVTO-NULL
           ELSE
              MOVE (SF)-CPT-IN-OPZ-RIVTO(IX-TAB-TGA)
              TO TGA-CPT-IN-OPZ-RIVTO
           END-IF
           IF (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRSTZ-INI-STAB-NULL(IX-TAB-TGA)
              TO TGA-PRSTZ-INI-STAB-NULL
           ELSE
              MOVE (SF)-PRSTZ-INI-STAB(IX-TAB-TGA)
              TO TGA-PRSTZ-INI-STAB
           END-IF
           IF (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-CPT-RSH-MOR-NULL(IX-TAB-TGA)
              TO TGA-CPT-RSH-MOR-NULL
           ELSE
              MOVE (SF)-CPT-RSH-MOR(IX-TAB-TGA)
              TO TGA-CPT-RSH-MOR
           END-IF
           IF (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRSTZ-RID-INI-NULL(IX-TAB-TGA)
              TO TGA-PRSTZ-RID-INI-NULL
           ELSE
              MOVE (SF)-PRSTZ-RID-INI(IX-TAB-TGA)
              TO TGA-PRSTZ-RID-INI
           END-IF
           IF (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-FL-CAR-CONT-NULL(IX-TAB-TGA)
              TO TGA-FL-CAR-CONT-NULL
           ELSE
              MOVE (SF)-FL-CAR-CONT(IX-TAB-TGA)
              TO TGA-FL-CAR-CONT
           END-IF
           IF (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-BNS-GIA-LIQTO-NULL(IX-TAB-TGA)
              TO TGA-BNS-GIA-LIQTO-NULL
           ELSE
              MOVE (SF)-BNS-GIA-LIQTO(IX-TAB-TGA)
              TO TGA-BNS-GIA-LIQTO
           END-IF
           IF (SF)-IMP-BNS-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-BNS-NULL(IX-TAB-TGA)
              TO TGA-IMP-BNS-NULL
           ELSE
              MOVE (SF)-IMP-BNS(IX-TAB-TGA)
              TO TGA-IMP-BNS
           END-IF
           MOVE (SF)-COD-DVS(IX-TAB-TGA)
              TO TGA-COD-DVS
           IF (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRSTZ-INI-NEWFIS-NULL(IX-TAB-TGA)
              TO TGA-PRSTZ-INI-NEWFIS-NULL
           ELSE
              MOVE (SF)-PRSTZ-INI-NEWFIS(IX-TAB-TGA)
              TO TGA-PRSTZ-INI-NEWFIS
           END-IF
           IF (SF)-IMP-SCON-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-SCON-NULL(IX-TAB-TGA)
              TO TGA-IMP-SCON-NULL
           ELSE
              MOVE (SF)-IMP-SCON(IX-TAB-TGA)
              TO TGA-IMP-SCON
           END-IF
           IF (SF)-ALQ-SCON-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ALQ-SCON-NULL(IX-TAB-TGA)
              TO TGA-ALQ-SCON-NULL
           ELSE
              MOVE (SF)-ALQ-SCON(IX-TAB-TGA)
              TO TGA-ALQ-SCON
           END-IF
           IF (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-CAR-ACQ-NULL(IX-TAB-TGA)
              TO TGA-IMP-CAR-ACQ-NULL
           ELSE
              MOVE (SF)-IMP-CAR-ACQ(IX-TAB-TGA)
              TO TGA-IMP-CAR-ACQ
           END-IF
           IF (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-CAR-INC-NULL(IX-TAB-TGA)
              TO TGA-IMP-CAR-INC-NULL
           ELSE
              MOVE (SF)-IMP-CAR-INC(IX-TAB-TGA)
              TO TGA-IMP-CAR-INC
           END-IF
           IF (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-CAR-GEST-NULL(IX-TAB-TGA)
              TO TGA-IMP-CAR-GEST-NULL
           ELSE
              MOVE (SF)-IMP-CAR-GEST(IX-TAB-TGA)
              TO TGA-IMP-CAR-GEST
           END-IF
           IF (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA)
              TO TGA-ETA-AA-1O-ASSTO-NULL
           ELSE
              MOVE (SF)-ETA-AA-1O-ASSTO(IX-TAB-TGA)
              TO TGA-ETA-AA-1O-ASSTO
           END-IF
           IF (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA)
              TO TGA-ETA-MM-1O-ASSTO-NULL
           ELSE
              MOVE (SF)-ETA-MM-1O-ASSTO(IX-TAB-TGA)
              TO TGA-ETA-MM-1O-ASSTO
           END-IF
           IF (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ETA-AA-2O-ASSTO-NULL(IX-TAB-TGA)
              TO TGA-ETA-AA-2O-ASSTO-NULL
           ELSE
              MOVE (SF)-ETA-AA-2O-ASSTO(IX-TAB-TGA)
              TO TGA-ETA-AA-2O-ASSTO
           END-IF
           IF (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ETA-MM-2O-ASSTO-NULL(IX-TAB-TGA)
              TO TGA-ETA-MM-2O-ASSTO-NULL
           ELSE
              MOVE (SF)-ETA-MM-2O-ASSTO(IX-TAB-TGA)
              TO TGA-ETA-MM-2O-ASSTO
           END-IF
           IF (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ETA-AA-3O-ASSTO-NULL(IX-TAB-TGA)
              TO TGA-ETA-AA-3O-ASSTO-NULL
           ELSE
              MOVE (SF)-ETA-AA-3O-ASSTO(IX-TAB-TGA)
              TO TGA-ETA-AA-3O-ASSTO
           END-IF
           IF (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ETA-MM-3O-ASSTO-NULL(IX-TAB-TGA)
              TO TGA-ETA-MM-3O-ASSTO-NULL
           ELSE
              MOVE (SF)-ETA-MM-3O-ASSTO(IX-TAB-TGA)
              TO TGA-ETA-MM-3O-ASSTO
           END-IF
           IF (SF)-RENDTO-LRD-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-RENDTO-LRD-NULL(IX-TAB-TGA)
              TO TGA-RENDTO-LRD-NULL
           ELSE
              MOVE (SF)-RENDTO-LRD(IX-TAB-TGA)
              TO TGA-RENDTO-LRD
           END-IF
           IF (SF)-PC-RETR-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PC-RETR-NULL(IX-TAB-TGA)
              TO TGA-PC-RETR-NULL
           ELSE
              MOVE (SF)-PC-RETR(IX-TAB-TGA)
              TO TGA-PC-RETR
           END-IF
           IF (SF)-RENDTO-RETR-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-RENDTO-RETR-NULL(IX-TAB-TGA)
              TO TGA-RENDTO-RETR-NULL
           ELSE
              MOVE (SF)-RENDTO-RETR(IX-TAB-TGA)
              TO TGA-RENDTO-RETR
           END-IF
           IF (SF)-MIN-GARTO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-MIN-GARTO-NULL(IX-TAB-TGA)
              TO TGA-MIN-GARTO-NULL
           ELSE
              MOVE (SF)-MIN-GARTO(IX-TAB-TGA)
              TO TGA-MIN-GARTO
           END-IF
           IF (SF)-MIN-TRNUT-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-MIN-TRNUT-NULL(IX-TAB-TGA)
              TO TGA-MIN-TRNUT-NULL
           ELSE
              MOVE (SF)-MIN-TRNUT(IX-TAB-TGA)
              TO TGA-MIN-TRNUT
           END-IF
           IF (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRE-ATT-DI-TRCH-NULL(IX-TAB-TGA)
              TO TGA-PRE-ATT-DI-TRCH-NULL
           ELSE
              MOVE (SF)-PRE-ATT-DI-TRCH(IX-TAB-TGA)
              TO TGA-PRE-ATT-DI-TRCH
           END-IF
           IF (SF)-MATU-END2000-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-MATU-END2000-NULL(IX-TAB-TGA)
              TO TGA-MATU-END2000-NULL
           ELSE
              MOVE (SF)-MATU-END2000(IX-TAB-TGA)
              TO TGA-MATU-END2000
           END-IF
           IF (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ABB-TOT-INI-NULL(IX-TAB-TGA)
              TO TGA-ABB-TOT-INI-NULL
           ELSE
              MOVE (SF)-ABB-TOT-INI(IX-TAB-TGA)
              TO TGA-ABB-TOT-INI
           END-IF
           IF (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ABB-TOT-ULT-NULL(IX-TAB-TGA)
              TO TGA-ABB-TOT-ULT-NULL
           ELSE
              MOVE (SF)-ABB-TOT-ULT(IX-TAB-TGA)
              TO TGA-ABB-TOT-ULT
           END-IF
           IF (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ABB-ANNU-ULT-NULL(IX-TAB-TGA)
              TO TGA-ABB-ANNU-ULT-NULL
           ELSE
              MOVE (SF)-ABB-ANNU-ULT(IX-TAB-TGA)
              TO TGA-ABB-ANNU-ULT
           END-IF
           IF (SF)-DUR-ABB-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-DUR-ABB-NULL(IX-TAB-TGA)
              TO TGA-DUR-ABB-NULL
           ELSE
              MOVE (SF)-DUR-ABB(IX-TAB-TGA)
              TO TGA-DUR-ABB
           END-IF
           IF (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-TP-ADEG-ABB-NULL(IX-TAB-TGA)
              TO TGA-TP-ADEG-ABB-NULL
           ELSE
              MOVE (SF)-TP-ADEG-ABB(IX-TAB-TGA)
              TO TGA-TP-ADEG-ABB
           END-IF
           IF (SF)-MOD-CALC-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-MOD-CALC-NULL(IX-TAB-TGA)
              TO TGA-MOD-CALC-NULL
           ELSE
              MOVE (SF)-MOD-CALC(IX-TAB-TGA)
              TO TGA-MOD-CALC
           END-IF
           IF (SF)-IMP-AZ-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-AZ-NULL(IX-TAB-TGA)
              TO TGA-IMP-AZ-NULL
           ELSE
              MOVE (SF)-IMP-AZ(IX-TAB-TGA)
              TO TGA-IMP-AZ
           END-IF
           IF (SF)-IMP-ADER-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-ADER-NULL(IX-TAB-TGA)
              TO TGA-IMP-ADER-NULL
           ELSE
              MOVE (SF)-IMP-ADER(IX-TAB-TGA)
              TO TGA-IMP-ADER
           END-IF
           IF (SF)-IMP-TFR-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-TFR-NULL(IX-TAB-TGA)
              TO TGA-IMP-TFR-NULL
           ELSE
              MOVE (SF)-IMP-TFR(IX-TAB-TGA)
              TO TGA-IMP-TFR
           END-IF
           IF (SF)-IMP-VOLO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-VOLO-NULL(IX-TAB-TGA)
              TO TGA-IMP-VOLO-NULL
           ELSE
              MOVE (SF)-IMP-VOLO(IX-TAB-TGA)
              TO TGA-IMP-VOLO
           END-IF
           IF (SF)-VIS-END2000-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-VIS-END2000-NULL(IX-TAB-TGA)
              TO TGA-VIS-END2000-NULL
           ELSE
              MOVE (SF)-VIS-END2000(IX-TAB-TGA)
              TO TGA-VIS-END2000
           END-IF
           IF (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-DT-VLDT-PROD-NULL(IX-TAB-TGA)
              TO TGA-DT-VLDT-PROD-NULL
           ELSE
             IF (SF)-DT-VLDT-PROD(IX-TAB-TGA) = ZERO
                MOVE HIGH-VALUES
                TO TGA-DT-VLDT-PROD-NULL
             ELSE
              MOVE (SF)-DT-VLDT-PROD(IX-TAB-TGA)
              TO TGA-DT-VLDT-PROD
             END-IF
           END-IF
           IF (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-DT-INI-VAL-TAR-NULL(IX-TAB-TGA)
              TO TGA-DT-INI-VAL-TAR-NULL
           ELSE
             IF (SF)-DT-INI-VAL-TAR(IX-TAB-TGA) = ZERO
                MOVE HIGH-VALUES
                TO TGA-DT-INI-VAL-TAR-NULL
             ELSE
              MOVE (SF)-DT-INI-VAL-TAR(IX-TAB-TGA)
              TO TGA-DT-INI-VAL-TAR
             END-IF
           END-IF
           IF (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMPB-VIS-END2000-NULL(IX-TAB-TGA)
              TO TGA-IMPB-VIS-END2000-NULL
           ELSE
              MOVE (SF)-IMPB-VIS-END2000(IX-TAB-TGA)
              TO TGA-IMPB-VIS-END2000
           END-IF
           IF (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-REN-INI-TS-TEC-0-NULL(IX-TAB-TGA)
              TO TGA-REN-INI-TS-TEC-0-NULL
           ELSE
              MOVE (SF)-REN-INI-TS-TEC-0(IX-TAB-TGA)
              TO TGA-REN-INI-TS-TEC-0
           END-IF
           IF (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PC-RIP-PRE-NULL(IX-TAB-TGA)
              TO TGA-PC-RIP-PRE-NULL
           ELSE
              MOVE (SF)-PC-RIP-PRE(IX-TAB-TGA)
              TO TGA-PC-RIP-PRE
           END-IF
           IF (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-FL-IMPORTI-FORZ-NULL(IX-TAB-TGA)
              TO TGA-FL-IMPORTI-FORZ-NULL
           ELSE
              MOVE (SF)-FL-IMPORTI-FORZ(IX-TAB-TGA)
              TO TGA-FL-IMPORTI-FORZ
           END-IF
           IF (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRSTZ-INI-NFORZ-NULL(IX-TAB-TGA)
              TO TGA-PRSTZ-INI-NFORZ-NULL
           ELSE
              MOVE (SF)-PRSTZ-INI-NFORZ(IX-TAB-TGA)
              TO TGA-PRSTZ-INI-NFORZ
           END-IF
           IF (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-VIS-END2000-NFORZ-NULL(IX-TAB-TGA)
              TO TGA-VIS-END2000-NFORZ-NULL
           ELSE
              MOVE (SF)-VIS-END2000-NFORZ(IX-TAB-TGA)
              TO TGA-VIS-END2000-NFORZ
           END-IF
           IF (SF)-INTR-MORA-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-INTR-MORA-NULL(IX-TAB-TGA)
              TO TGA-INTR-MORA-NULL
           ELSE
              MOVE (SF)-INTR-MORA(IX-TAB-TGA)
              TO TGA-INTR-MORA
           END-IF
           IF (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-MANFEE-ANTIC-NULL(IX-TAB-TGA)
              TO TGA-MANFEE-ANTIC-NULL
           ELSE
              MOVE (SF)-MANFEE-ANTIC(IX-TAB-TGA)
              TO TGA-MANFEE-ANTIC
           END-IF
           IF (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-MANFEE-RICOR-NULL(IX-TAB-TGA)
              TO TGA-MANFEE-RICOR-NULL
           ELSE
              MOVE (SF)-MANFEE-RICOR(IX-TAB-TGA)
              TO TGA-MANFEE-RICOR
           END-IF
           IF (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRE-UNI-RIVTO-NULL(IX-TAB-TGA)
              TO TGA-PRE-UNI-RIVTO-NULL
           ELSE
              MOVE (SF)-PRE-UNI-RIVTO(IX-TAB-TGA)
              TO TGA-PRE-UNI-RIVTO
           END-IF
           IF (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PROV-1AA-ACQ-NULL(IX-TAB-TGA)
              TO TGA-PROV-1AA-ACQ-NULL
           ELSE
              MOVE (SF)-PROV-1AA-ACQ(IX-TAB-TGA)
              TO TGA-PROV-1AA-ACQ
           END-IF
           IF (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PROV-2AA-ACQ-NULL(IX-TAB-TGA)
              TO TGA-PROV-2AA-ACQ-NULL
           ELSE
              MOVE (SF)-PROV-2AA-ACQ(IX-TAB-TGA)
              TO TGA-PROV-2AA-ACQ
           END-IF
           IF (SF)-PROV-RICOR-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PROV-RICOR-NULL(IX-TAB-TGA)
              TO TGA-PROV-RICOR-NULL
           ELSE
              MOVE (SF)-PROV-RICOR(IX-TAB-TGA)
              TO TGA-PROV-RICOR
           END-IF
           IF (SF)-PROV-INC-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PROV-INC-NULL(IX-TAB-TGA)
              TO TGA-PROV-INC-NULL
           ELSE
              MOVE (SF)-PROV-INC(IX-TAB-TGA)
              TO TGA-PROV-INC
           END-IF
           IF (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ALQ-PROV-ACQ-NULL(IX-TAB-TGA)
              TO TGA-ALQ-PROV-ACQ-NULL
           ELSE
              MOVE (SF)-ALQ-PROV-ACQ(IX-TAB-TGA)
              TO TGA-ALQ-PROV-ACQ
           END-IF
           IF (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ALQ-PROV-INC-NULL(IX-TAB-TGA)
              TO TGA-ALQ-PROV-INC-NULL
           ELSE
              MOVE (SF)-ALQ-PROV-INC(IX-TAB-TGA)
              TO TGA-ALQ-PROV-INC
           END-IF
           IF (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ALQ-PROV-RICOR-NULL(IX-TAB-TGA)
              TO TGA-ALQ-PROV-RICOR-NULL
           ELSE
              MOVE (SF)-ALQ-PROV-RICOR(IX-TAB-TGA)
              TO TGA-ALQ-PROV-RICOR
           END-IF
           IF (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMPB-PROV-ACQ-NULL(IX-TAB-TGA)
              TO TGA-IMPB-PROV-ACQ-NULL
           ELSE
              MOVE (SF)-IMPB-PROV-ACQ(IX-TAB-TGA)
              TO TGA-IMPB-PROV-ACQ
           END-IF
           IF (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMPB-PROV-INC-NULL(IX-TAB-TGA)
              TO TGA-IMPB-PROV-INC-NULL
           ELSE
              MOVE (SF)-IMPB-PROV-INC(IX-TAB-TGA)
              TO TGA-IMPB-PROV-INC
           END-IF
           IF (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMPB-PROV-RICOR-NULL(IX-TAB-TGA)
              TO TGA-IMPB-PROV-RICOR-NULL
           ELSE
              MOVE (SF)-IMPB-PROV-RICOR(IX-TAB-TGA)
              TO TGA-IMPB-PROV-RICOR
           END-IF
           IF (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-FL-PROV-FORZ-NULL(IX-TAB-TGA)
              TO TGA-FL-PROV-FORZ-NULL
           ELSE
              MOVE (SF)-FL-PROV-FORZ(IX-TAB-TGA)
              TO TGA-FL-PROV-FORZ
           END-IF
           IF (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRSTZ-AGG-INI-NULL(IX-TAB-TGA)
              TO TGA-PRSTZ-AGG-INI-NULL
           ELSE
              MOVE (SF)-PRSTZ-AGG-INI(IX-TAB-TGA)
              TO TGA-PRSTZ-AGG-INI
           END-IF
           IF (SF)-INCR-PRE-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-INCR-PRE-NULL(IX-TAB-TGA)
              TO TGA-INCR-PRE-NULL
           ELSE
              MOVE (SF)-INCR-PRE(IX-TAB-TGA)
              TO TGA-INCR-PRE
           END-IF
           IF (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-INCR-PRSTZ-NULL(IX-TAB-TGA)
              TO TGA-INCR-PRSTZ-NULL
           ELSE
              MOVE (SF)-INCR-PRSTZ(IX-TAB-TGA)
              TO TGA-INCR-PRSTZ
           END-IF
           IF (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA)
              TO TGA-DT-ULT-ADEG-PRE-PR-NULL
           ELSE
             IF (SF)-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA) = ZERO
                MOVE HIGH-VALUES
                TO TGA-DT-ULT-ADEG-PRE-PR-NULL
             ELSE
              MOVE (SF)-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
              TO TGA-DT-ULT-ADEG-PRE-PR
             END-IF
           END-IF
           IF (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRSTZ-AGG-ULT-NULL(IX-TAB-TGA)
              TO TGA-PRSTZ-AGG-ULT-NULL
           ELSE
              MOVE (SF)-PRSTZ-AGG-ULT(IX-TAB-TGA)
              TO TGA-PRSTZ-AGG-ULT
           END-IF
           IF (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-TS-RIVAL-NET-NULL(IX-TAB-TGA)
              TO TGA-TS-RIVAL-NET-NULL
           ELSE
              MOVE (SF)-TS-RIVAL-NET(IX-TAB-TGA)
              TO TGA-TS-RIVAL-NET
           END-IF
           IF (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PRE-PATTUITO-NULL(IX-TAB-TGA)
              TO TGA-PRE-PATTUITO-NULL
           ELSE
              MOVE (SF)-PRE-PATTUITO(IX-TAB-TGA)
              TO TGA-PRE-PATTUITO
           END-IF
           IF (SF)-TP-RIVAL-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-TP-RIVAL-NULL(IX-TAB-TGA)
              TO TGA-TP-RIVAL-NULL
           ELSE
              MOVE (SF)-TP-RIVAL(IX-TAB-TGA)
              TO TGA-TP-RIVAL
           END-IF
           IF (SF)-RIS-MAT-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-RIS-MAT-NULL(IX-TAB-TGA)
              TO TGA-RIS-MAT-NULL
           ELSE
              MOVE (SF)-RIS-MAT(IX-TAB-TGA)
              TO TGA-RIS-MAT
           END-IF
           IF (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-CPT-MIN-SCAD-NULL(IX-TAB-TGA)
              TO TGA-CPT-MIN-SCAD-NULL
           ELSE
              MOVE (SF)-CPT-MIN-SCAD(IX-TAB-TGA)
              TO TGA-CPT-MIN-SCAD
           END-IF
           IF (SF)-COMMIS-GEST-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-COMMIS-GEST-NULL(IX-TAB-TGA)
              TO TGA-COMMIS-GEST-NULL
           ELSE
              MOVE (SF)-COMMIS-GEST(IX-TAB-TGA)
              TO TGA-COMMIS-GEST
           END-IF
           IF (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-TP-MANFEE-APPL-NULL(IX-TAB-TGA)
              TO TGA-TP-MANFEE-APPL-NULL
           ELSE
              MOVE (SF)-TP-MANFEE-APPL(IX-TAB-TGA)
              TO TGA-TP-MANFEE-APPL
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-TGA) NOT NUMERIC
              MOVE 0 TO TGA-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-TGA)
              TO TGA-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-TGA)
              TO TGA-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-TGA) NOT NUMERIC
              MOVE 0 TO TGA-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-TGA)
              TO TGA-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-TGA) NOT NUMERIC
              MOVE 0 TO TGA-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-TGA)
              TO TGA-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-TGA) NOT NUMERIC
              MOVE 0 TO TGA-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-TGA)
              TO TGA-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-TGA)
              TO TGA-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-TGA)
              TO TGA-DS-STATO-ELAB
           IF (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-PC-COMMIS-GEST-NULL(IX-TAB-TGA)
              TO TGA-PC-COMMIS-GEST-NULL
           ELSE
              MOVE (SF)-PC-COMMIS-GEST(IX-TAB-TGA)
              TO TGA-PC-COMMIS-GEST
           END-IF
           IF (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-NUM-GG-RIVAL-NULL(IX-TAB-TGA)
              TO TGA-NUM-GG-RIVAL-NULL
           ELSE
              MOVE (SF)-NUM-GG-RIVAL(IX-TAB-TGA)
              TO TGA-NUM-GG-RIVAL
           END-IF
           IF (SF)-IMP-TRASFE-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-TRASFE-NULL(IX-TAB-TGA)
              TO TGA-IMP-TRASFE-NULL
           ELSE
              MOVE (SF)-IMP-TRASFE(IX-TAB-TGA)
              TO TGA-IMP-TRASFE
           END-IF
           IF (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMP-TFR-STRC-NULL(IX-TAB-TGA)
              TO TGA-IMP-TFR-STRC-NULL
           ELSE
              MOVE (SF)-IMP-TFR-STRC(IX-TAB-TGA)
              TO TGA-IMP-TFR-STRC
           END-IF
           IF (SF)-ACQ-EXP-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ACQ-EXP-NULL(IX-TAB-TGA)
              TO TGA-ACQ-EXP-NULL
           ELSE
              MOVE (SF)-ACQ-EXP(IX-TAB-TGA)
              TO TGA-ACQ-EXP
           END-IF
           IF (SF)-REMUN-ASS-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-REMUN-ASS-NULL(IX-TAB-TGA)
              TO TGA-REMUN-ASS-NULL
           ELSE
              MOVE (SF)-REMUN-ASS(IX-TAB-TGA)
              TO TGA-REMUN-ASS
           END-IF
           IF (SF)-COMMIS-INTER-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-COMMIS-INTER-NULL(IX-TAB-TGA)
              TO TGA-COMMIS-INTER-NULL
           ELSE
              MOVE (SF)-COMMIS-INTER(IX-TAB-TGA)
              TO TGA-COMMIS-INTER
           END-IF
           IF (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ALQ-REMUN-ASS-NULL(IX-TAB-TGA)
              TO TGA-ALQ-REMUN-ASS-NULL
           ELSE
              MOVE (SF)-ALQ-REMUN-ASS(IX-TAB-TGA)
              TO TGA-ALQ-REMUN-ASS
           END-IF
           IF (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-ALQ-COMMIS-INTER-NULL(IX-TAB-TGA)
              TO TGA-ALQ-COMMIS-INTER-NULL
           ELSE
              MOVE (SF)-ALQ-COMMIS-INTER(IX-TAB-TGA)
              TO TGA-ALQ-COMMIS-INTER
           END-IF
           IF (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMPB-REMUN-ASS-NULL(IX-TAB-TGA)
              TO TGA-IMPB-REMUN-ASS-NULL
           ELSE
              MOVE (SF)-IMPB-REMUN-ASS(IX-TAB-TGA)
              TO TGA-IMPB-REMUN-ASS
           END-IF
           IF (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-IMPB-COMMIS-INTER-NULL(IX-TAB-TGA)
              TO TGA-IMPB-COMMIS-INTER-NULL
           ELSE
              MOVE (SF)-IMPB-COMMIS-INTER(IX-TAB-TGA)
              TO TGA-IMPB-COMMIS-INTER
           END-IF
           IF (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-COS-RUN-ASSVA-NULL(IX-TAB-TGA)
              TO TGA-COS-RUN-ASSVA-NULL
           ELSE
              MOVE (SF)-COS-RUN-ASSVA(IX-TAB-TGA)
              TO TGA-COS-RUN-ASSVA
           END-IF
           IF (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE (SF)-COS-RUN-ASSVA-IDC-NULL(IX-TAB-TGA)
              TO TGA-COS-RUN-ASSVA-IDC-NULL
           ELSE
              MOVE (SF)-COS-RUN-ASSVA-IDC(IX-TAB-TGA)
              TO TGA-COS-RUN-ASSVA-IDC
           END-IF.
       VAL-DCLGEN-TGA-EX.
           EXIT.

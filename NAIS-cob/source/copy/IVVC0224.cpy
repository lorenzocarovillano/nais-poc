      *----------------------------------------------------------------*
      *   AREA INPUT VARIABILI
      *   PORTAFOGLIO VITA - AREA FONDI COMMISSIONI GESTIONE
      *   LUNG.
      *----------------------------------------------------------------*
      * -- AREA NUOVO ALGORITMO CALCOLO COMMISSIONI GESTIONE
      * -- FONDI IMPATTATI DAL RISCATTO
      * -- PERCENTUALE DEL FONDO SUL RISCATTO
      * -- CDG DA RESTITUIRE SUL FONDO
       03 WCDG-AREA-COMMIS-GEST-CALC.
            04 (SF)-ELE-MAX-CDG-CALC           PIC 9(04).
            04 (SF)-FND-X-CDG-CALC       OCCURS 200.
               05 (SF)-COD-FND-CALC             PIC X(0020).
               05 (SF)-PERC-FND-CALC            PIC S9(03)V9(7).
               05 (SF)-CDG-REST-CALC            PIC S9(11)V9(07).

      * -- AREA I/O VALORIZZATORE VARIABILI
       03 WCDG-AREA-COMMIS-GEST-VV.
            04 (SF)-ELE-MAX-CDG-VV              PIC 9(04).
            04 (SF)-FND-X-CDG-VV         OCCURS 200.
               05 (SF)-COD-FND-VV               PIC X(0020).
               05 (SF)-CDG-REST-VV              PIC S9(11)V9(07).

      * -- AREA OUTPUT ACTUATOR
      * -- AREA CONTENTENTE: (RESTITUITA DA ACTUATOR)
      * -- FONDI CON COMMISSIONI DA RECUPERARE
      * -- COMMISSIONI UTILIZZATE TRA QUELLE DA RECUPERARE
       03 WCDG-AREA-COMMIS-GEST-OUT.
            04 (SF)-ELE-MAX-RECCDG              PIC 9(04).
            04 (SF)-FND-X-RECCDG         OCCURS 200.
               05 (SF)-COD-FND-RECCDG           PIC X(60).
               05 (SF)-CONTROVAL-FND-RECCDG     PIC S9(11)V9(07).


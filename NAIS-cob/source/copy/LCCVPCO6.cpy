      *----------------------------------------------------------------*
      *    COPY      ..... LCCVPCO6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO PARAMETRO COMPAGNIA
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *
      *----------------------------------------------------------------*
       AGGIORNA-PARAM-COMP.

      *--> NOME TABELLA FISICA DB
           INITIALIZE PARAM-COMP.
      *--> NOME TABELLA FISICA DB
           MOVE 'PARAM-COMP'                   TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WPCO-ST-INV
              AND WPCO-ELE-PAR-COMP-MAX NOT = 0

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WPCO-ST-ADD
      *-->        CANCELLAZIONE
                  WHEN WPCO-ST-DEL
      *-->        MODIFICA
                     SET IDSV0001-ESITO-KO TO TRUE
                     MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'AGGIORNA-PARAM-COMP'
                                        TO IEAI9901-LABEL-ERR
                     MOVE '005017'      TO IEAI9901-COD-ERRORE
                     MOVE SPACES        TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                             THRU EX-S0300
                  WHEN WPCO-ST-MOD

                     MOVE WPCO-COD-COMP-ANIA  TO PCO-COD-COMP-ANIA

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-UPDATE   TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN NOTE OGGETTO
                 PERFORM VAL-DCLGEN-PCO
                    THRU VAL-DCLGEN-PCO-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-NOT
                    THRU VALORIZZA-AREA-DSH-NOT-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-PARAM-COMP-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-NOT.

      *--> DCLGEN TABELLA
           MOVE PARAM-COMP                     TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-PRIMARY-KEY           TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-SENZA-STOR      TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-NOT-EX.
           EXIT.


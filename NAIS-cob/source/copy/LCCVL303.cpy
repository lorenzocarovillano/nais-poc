
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVL303
      *   ULTIMO AGG. 09 LUG 2009
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-L30.
           MOVE L30-ID-REINVST-POLI-LQ
             TO (SF)-ID-PTF(IX-TAB-L30)
           MOVE L30-ID-REINVST-POLI-LQ
             TO (SF)-ID-REINVST-POLI-LQ(IX-TAB-L30)
           MOVE L30-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-L30)
           IF L30-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE L30-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-L30)
           ELSE
              MOVE L30-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-L30)
           END-IF
           MOVE L30-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-L30)
           MOVE L30-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-L30)
           MOVE L30-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-L30)
           MOVE L30-COD-RAMO
             TO (SF)-COD-RAMO(IX-TAB-L30)
           MOVE L30-IB-POLI
             TO (SF)-IB-POLI(IX-TAB-L30)
           IF L30-PR-KEY-SIST-ESTNO-NULL = HIGH-VALUES
              MOVE L30-PR-KEY-SIST-ESTNO-NULL
                TO (SF)-PR-KEY-SIST-ESTNO-NULL(IX-TAB-L30)
           ELSE
              MOVE L30-PR-KEY-SIST-ESTNO
                TO (SF)-PR-KEY-SIST-ESTNO(IX-TAB-L30)
           END-IF
           IF L30-SEC-KEY-SIST-ESTNO-NULL = HIGH-VALUES
              MOVE L30-SEC-KEY-SIST-ESTNO-NULL
                TO (SF)-SEC-KEY-SIST-ESTNO-NULL(IX-TAB-L30)
           ELSE
              MOVE L30-SEC-KEY-SIST-ESTNO
                TO (SF)-SEC-KEY-SIST-ESTNO(IX-TAB-L30)
           END-IF
           MOVE L30-COD-CAN
             TO (SF)-COD-CAN(IX-TAB-L30)
           MOVE L30-COD-AGE
             TO (SF)-COD-AGE(IX-TAB-L30)
           IF L30-COD-SUB-AGE-NULL = HIGH-VALUES
              MOVE L30-COD-SUB-AGE-NULL
                TO (SF)-COD-SUB-AGE-NULL(IX-TAB-L30)
           ELSE
              MOVE L30-COD-SUB-AGE
                TO (SF)-COD-SUB-AGE(IX-TAB-L30)
           END-IF
           MOVE L30-IMP-RES
             TO (SF)-IMP-RES(IX-TAB-L30)
           MOVE L30-TP-LIQ
             TO (SF)-TP-LIQ(IX-TAB-L30)
           MOVE L30-TP-SIST-ESTNO
             TO (SF)-TP-SIST-ESTNO(IX-TAB-L30)
           MOVE L30-COD-FISC-PART-IVA
             TO (SF)-COD-FISC-PART-IVA(IX-TAB-L30)
           MOVE L30-COD-MOVI-LIQ
             TO (SF)-COD-MOVI-LIQ(IX-TAB-L30)
           IF L30-TP-INVST-LIQ-NULL = HIGH-VALUES
              MOVE L30-TP-INVST-LIQ-NULL
                TO (SF)-TP-INVST-LIQ-NULL(IX-TAB-L30)
           ELSE
              MOVE L30-TP-INVST-LIQ
                TO (SF)-TP-INVST-LIQ(IX-TAB-L30)
           END-IF
           MOVE L30-IMP-TOT-LIQ
             TO (SF)-IMP-TOT-LIQ(IX-TAB-L30)
           MOVE L30-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-L30)
           MOVE L30-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-L30)
           MOVE L30-DS-VER
             TO (SF)-DS-VER(IX-TAB-L30)
           MOVE L30-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-L30)
           MOVE L30-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-L30)
           MOVE L30-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-L30)
           MOVE L30-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-L30)
           IF L30-DT-DECOR-POLI-LQ-NULL = HIGH-VALUES
              MOVE L30-DT-DECOR-POLI-LQ-NULL
                TO (SF)-DT-DECOR-POLI-LQ-NULL(IX-TAB-L30)
           ELSE
              MOVE L30-DT-DECOR-POLI-LQ
                TO (SF)-DT-DECOR-POLI-LQ(IX-TAB-L30)
           END-IF.
       VALORIZZA-OUTPUT-L30-EX.
           EXIT.

       01 IND-RICH-INVST-FND.
          05 IND-RIF-ID-MOVI-CHIU PIC S9(4) COMP-5.
          05 IND-RIF-COD-FND PIC S9(4) COMP-5.
          05 IND-RIF-NUM-QUO PIC S9(4) COMP-5.
          05 IND-RIF-PC PIC S9(4) COMP-5.
          05 IND-RIF-IMP-MOVTO PIC S9(4) COMP-5.
          05 IND-RIF-DT-INVST PIC S9(4) COMP-5.
          05 IND-RIF-COD-TARI PIC S9(4) COMP-5.
          05 IND-RIF-TP-STAT PIC S9(4) COMP-5.
          05 IND-RIF-TP-MOD-INVST PIC S9(4) COMP-5.
          05 IND-RIF-DT-CAMBIO-VLT PIC S9(4) COMP-5.
          05 IND-RIF-DT-INVST-CALC PIC S9(4) COMP-5.
          05 IND-RIF-FL-CALC-INVTO PIC S9(4) COMP-5.
          05 IND-RIF-IMP-GAP-EVENT PIC S9(4) COMP-5.
          05 IND-RIF-FL-SWM-BP2S PIC S9(4) COMP-5.

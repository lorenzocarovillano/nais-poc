           EXEC SQL DECLARE MATR_ELAB_BATCH TABLE
           (
             ID_MATR_ELAB_BATCH  DECIMAL(9, 0) NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             TP_FRM_ASSVA        CHAR(2) NOT NULL,
             TP_MOVI             DECIMAL(5, 0) NOT NULL,
             COD_RAMO            CHAR(12),
             DT_ULT_ELAB         DATE NOT NULL,
             DS_OPER_SQL         CHAR(1) NOT NULL,
             DS_VER              DECIMAL(9, 0) NOT NULL,
             DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL,
             DS_UTENTE           CHAR(20) NOT NULL,
             DS_STATO_ELAB       CHAR(1) NOT NULL
          ) END-EXEC.

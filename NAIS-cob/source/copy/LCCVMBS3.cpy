
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVMBS3
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-MBS.
           MOVE MBS-ID-MOVI-BATCH-SOSP
             TO (SF)-ID-PTF(IX-TAB-MBS)
           MOVE MBS-ID-MOVI-BATCH-SOSP
             TO (SF)-ID-MOVI-BATCH-SOSP(IX-TAB-MBS)
           MOVE MBS-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-MBS)
           MOVE MBS-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-MBS)
           MOVE MBS-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-MBS)
           MOVE MBS-ID-MOVI
             TO (SF)-ID-MOVI(IX-TAB-MBS)
           MOVE MBS-TP-MOVI
             TO (SF)-TP-MOVI(IX-TAB-MBS)
           MOVE MBS-DT-EFF
             TO (SF)-DT-EFF(IX-TAB-MBS)
           MOVE MBS-ID-OGG-BLOCCO
             TO (SF)-ID-OGG-BLOCCO(IX-TAB-MBS)
           MOVE MBS-TP-FRM-ASSVA
             TO (SF)-TP-FRM-ASSVA(IX-TAB-MBS)
           IF MBS-ID-BATCH-NULL = HIGH-VALUES
              MOVE MBS-ID-BATCH-NULL
                TO (SF)-ID-BATCH-NULL(IX-TAB-MBS)
           ELSE
              MOVE MBS-ID-BATCH
                TO (SF)-ID-BATCH(IX-TAB-MBS)
           END-IF
           IF MBS-ID-JOB-NULL = HIGH-VALUES
              MOVE MBS-ID-JOB-NULL
                TO (SF)-ID-JOB-NULL(IX-TAB-MBS)
           ELSE
              MOVE MBS-ID-JOB
                TO (SF)-ID-JOB(IX-TAB-MBS)
           END-IF
           MOVE MBS-STEP-ELAB
             TO (SF)-STEP-ELAB(IX-TAB-MBS)
           MOVE MBS-FL-MOVI-SOSP
             TO (SF)-FL-MOVI-SOSP(IX-TAB-MBS)
           MOVE MBS-D-INPUT-MOVI-SOSP
             TO (SF)-D-INPUT-MOVI-SOSP(IX-TAB-MBS)
           MOVE MBS-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-MBS)
           MOVE MBS-DS-VER
             TO (SF)-DS-VER(IX-TAB-MBS)
           MOVE MBS-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ(IX-TAB-MBS)
           MOVE MBS-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-MBS)
           MOVE MBS-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-MBS).
       VALORIZZA-OUTPUT-MBS-EX.
           EXIT.

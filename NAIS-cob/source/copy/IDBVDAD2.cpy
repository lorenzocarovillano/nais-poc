       01 IND-DFLT-ADES.
          05 IND-DAD-IB-POLI PIC S9(4) COMP-5.
          05 IND-DAD-IB-DFLT PIC S9(4) COMP-5.
          05 IND-DAD-COD-GAR-1 PIC S9(4) COMP-5.
          05 IND-DAD-COD-GAR-2 PIC S9(4) COMP-5.
          05 IND-DAD-COD-GAR-3 PIC S9(4) COMP-5.
          05 IND-DAD-COD-GAR-4 PIC S9(4) COMP-5.
          05 IND-DAD-COD-GAR-5 PIC S9(4) COMP-5.
          05 IND-DAD-COD-GAR-6 PIC S9(4) COMP-5.
          05 IND-DAD-DT-DECOR-DFLT PIC S9(4) COMP-5.
          05 IND-DAD-ETA-SCAD-MASC-DFLT PIC S9(4) COMP-5.
          05 IND-DAD-ETA-SCAD-FEMM-DFLT PIC S9(4) COMP-5.
          05 IND-DAD-DUR-AA-ADES-DFLT PIC S9(4) COMP-5.
          05 IND-DAD-DUR-MM-ADES-DFLT PIC S9(4) COMP-5.
          05 IND-DAD-DUR-GG-ADES-DFLT PIC S9(4) COMP-5.
          05 IND-DAD-DT-SCAD-ADES-DFLT PIC S9(4) COMP-5.
          05 IND-DAD-FRAZ-DFLT PIC S9(4) COMP-5.
          05 IND-DAD-PC-PROV-INC-DFLT PIC S9(4) COMP-5.
          05 IND-DAD-IMP-PROV-INC-DFLT PIC S9(4) COMP-5.
          05 IND-DAD-IMP-AZ PIC S9(4) COMP-5.
          05 IND-DAD-IMP-ADER PIC S9(4) COMP-5.
          05 IND-DAD-IMP-TFR PIC S9(4) COMP-5.
          05 IND-DAD-IMP-VOLO PIC S9(4) COMP-5.
          05 IND-DAD-PC-AZ PIC S9(4) COMP-5.
          05 IND-DAD-PC-ADER PIC S9(4) COMP-5.
          05 IND-DAD-PC-TFR PIC S9(4) COMP-5.
          05 IND-DAD-PC-VOLO PIC S9(4) COMP-5.
          05 IND-DAD-TP-FNT-CNBTVA PIC S9(4) COMP-5.
          05 IND-DAD-IMP-PRE-DFLT PIC S9(4) COMP-5.
          05 IND-DAD-TP-PRE PIC S9(4) COMP-5.

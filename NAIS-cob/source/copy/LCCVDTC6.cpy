      *----------------------------------------------------------------*
      *    COPY      ..... LCCVDTC6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO DETTAGLIO TITOLO CONTABILE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-DETT-TIT-CONT.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE DETT-TIT-CONT

      *--> NOME TABELLA FISICA DB
           MOVE 'DETT-TIT-CONT'                  TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF  NOT WDTC-ST-INV(IX-TAB-DTC)
           AND NOT WDTC-ST-CON(IX-TAB-DTC)
           AND     WDTC-ELE-DETT-TIT-MAX NOT = 0

      *--->   Impostare ID Tabella Titolo Contabile
              MOVE WDTC-ID-TIT-CONT(IX-TAB-DTC)  TO WS-ID-TIT-CONT

      *--->   Ricerca dell'ID-TIT-CONT sulla Tabella
      *--->   Padre Titolo Contabile
              PERFORM RICERCA-ID-TIT-CONT
                 THRU RICERCA-ID-TIT-CONT-EX

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WDTC-ST-ADD(IX-TAB-DTC)

                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK

                          MOVE S090-SEQ-TABELLA
                            TO WDTC-ID-PTF(IX-TAB-DTC)
                               DTC-ID-DETT-TIT-CONT
                          MOVE WS-ID-PTF
                            TO DTC-ID-TIT-CONT
                          MOVE WMOV-ID-PTF
                            TO DTC-ID-MOVI-CRZ

      *-->                Estrazione Oggetto
                          IF INSOL-ADESIO        OR
                             INSOL-INDIVI        OR
                             RECES-ADESIO        OR
                             RECES-INDIVI        OR
                             RISTO-ADESIO        OR
                             RISTO-INDIVI        OR
10819X                       COMUN-RISTOT-INCAPIENZA OR
FNZS2                        COMUN-RISTOT-INCAP      OR
                             TRASF-COLLET        OR
                             TRASF-INDIVI        OR
                             SCADE-ADESIO        OR
                             SCADE-INDIVI        OR
                             SINIS-ADESIO        OR
                             SINIS-INDIVI        OR
10819                        RPP-BENEFICIO-CONTR OR
                             RIMBO-TRANCH        OR
                             RISCA-ADESIO        OR
                             LIQUI-RISTOT-ADE    OR
                             LIQUI-RISTOT-IND    OR
10819X                       LIQUI-RISTOT-INCAPIENZA OR
FNZS2                        COMUN-RISTOT-INCAP      OR
                             RISCA-POLIND        OR
                             REVOC-PERFEZ        OR
                             COMUN-INSOL-POL-COL OR
                             COMUN-RECES-POL-COL OR
                             COMUN-REV-POL-COL   OR
                             VARIA-RIATTI        OR
      * INIZIO-Gestione Garanzie complementari
                             VARIA-CONTAR        OR
      * FINE-Gestione Garanzie complementari
                             STO-ALTRE-CAU-ADES  OR
35037                        TRASFER-AGENZIA     OR
35037                        VARIA-PREN-TRASF-AGE OR
MIGCOL                        RINNOVO-TRANCHE     OR
                             STO-ALTRE-CAU-POL-COL

                             MOVE WDTC-ID-OGG(IX-TAB-DTC)
                               TO DTC-ID-OGG
                          ELSE
                             PERFORM PREPARA-AREA-LCCS0234-DTC
                                THRU PREPARA-AREA-LCCS0234-DTC-EX
                             PERFORM CALL-LCCS0234
                                THRU CALL-LCCS0234-EX
                             IF IDSV0001-ESITO-OK
                                MOVE S234-ID-OGG-PTF-EOC
                                  TO DTC-ID-OGG
                             END-IF
                          END-IF
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WDTC-ST-MOD(IX-TAB-DTC)

                       MOVE WDTC-ID-PTF(IX-TAB-DTC)
                         TO DTC-ID-DETT-TIT-CONT
                       MOVE WDTC-ID-TIT-CONT(IX-TAB-DTC)
                         TO DTC-ID-TIT-CONT
                       MOVE WMOV-ID-PTF
                         TO DTC-ID-MOVI-CRZ
                       MOVE WMOV-DT-EFF
                         TO DTC-DT-INI-EFF
                       MOVE WDTC-ID-OGG(IX-TAB-DTC)
                         TO DTC-ID-OGG

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WDTC-ST-DEL(IX-TAB-DTC)

                       MOVE WDTC-ID-PTF(IX-TAB-DTC)
                         TO DTC-ID-DETT-TIT-CONT
                       MOVE WDTC-ID-TIT-CONT(IX-TAB-DTC)
                         TO DTC-ID-TIT-CONT
                       MOVE WMOV-ID-PTF
                         TO DTC-ID-MOVI-CRZ
                       MOVE WDTC-ID-OGG(IX-TAB-DTC)
                         TO DTC-ID-OGG

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN ADESIONE
                 PERFORM VAL-DCLGEN-DTC
                    THRU VAL-DCLGEN-DTC-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-DTC
                    THRU VALORIZZA-AREA-DSH-DTC-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-DETT-TIT-CONT-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-DTC.

      *--> DCLGEN TABELLA
           MOVE DETT-TIT-CONT             TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-DTC-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   PREPARA PER LA CALL ALL'LCCS0234 ESTRATTORE OGGETTO PTF
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234-DTC.

           MOVE WDTC-ID-OGG(IX-TAB-DTC)     TO S234-ID-OGG-EOC
           MOVE WDTC-TP-OGG(IX-TAB-DTC)     TO S234-TIPO-OGG-EOC.

       PREPARA-AREA-LCCS0234-DTC-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    RICERCA PTF
      *----------------------------------------------------------------*
       RICERCA-ID-TIT-CONT.

           SET NON-TROVATO TO TRUE.

           PERFORM VARYING IX-TIT FROM 1 BY 1
                     UNTIL IX-TIT > WTIT-ELE-TIT-MAX OR TROVATO

                IF WTIT-ID-TIT-CONT(IX-TIT) = WS-ID-TIT-CONT

                   MOVE WTIT-ID-PTF(IX-TIT) TO WS-ID-PTF

                   SET TROVATO TO TRUE

                END-IF

           END-PERFORM.

       RICERCA-ID-TIT-CONT-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVDTC5.

           EXEC SQL DECLARE BTC_REC_SCHEDULE TABLE
           (
             ID_BATCH            INTEGER NOT NULL,
             ID_JOB              INTEGER NOT NULL,
             ID_RECORD           INTEGER NOT NULL,
             TYPE_RECORD         CHAR(3),
             DATA_RECORD         VARCHAR(10000) NOT NULL
          ) END-EXEC.

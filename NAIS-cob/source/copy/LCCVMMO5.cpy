
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVMMO5
      *   ULTIMO AGG. 23 GEN 2008
      *------------------------------------------------------------

       VAL-DCLGEN-MMO.
           MOVE (SF)-ID-MATR-MOVIMENTO(IX-TAB-MMO)
              TO MMO-ID-MATR-MOVIMENTO
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-MMO)
              TO MMO-COD-COMP-ANIA
           MOVE (SF)-TP-MOVI-PTF(IX-TAB-MMO)
              TO MMO-TP-MOVI-PTF
           MOVE (SF)-TP-OGG(IX-TAB-MMO)
              TO MMO-TP-OGG
           IF (SF)-TP-FRM-ASSVA-NULL(IX-TAB-MMO) = HIGH-VALUES
              MOVE (SF)-TP-FRM-ASSVA-NULL(IX-TAB-MMO)
              TO MMO-TP-FRM-ASSVA-NULL
           ELSE
              MOVE (SF)-TP-FRM-ASSVA(IX-TAB-MMO)
              TO MMO-TP-FRM-ASSVA
           END-IF.
           MOVE (SF)-TP-MOVI-ACT(IX-TAB-MMO)
              TO MMO-TP-MOVI-ACT
           IF (SF)-AMMISSIBILITA-MOVI-NULL(IX-TAB-MMO) = HIGH-VALUES
              MOVE (SF)-AMMISSIBILITA-MOVI-NULL(IX-TAB-MMO)
              TO MMO-AMMISSIBILITA-MOVI-NULL
           ELSE
              MOVE (SF)-AMMISSIBILITA-MOVI(IX-TAB-MMO)
              TO MMO-AMMISSIBILITA-MOVI
           END-IF.
           IF (SF)-SERVIZIO-CONTROLLO-NULL(IX-TAB-MMO) = HIGH-VALUES
              MOVE (SF)-SERVIZIO-CONTROLLO-NULL(IX-TAB-MMO)
              TO MMO-SERVIZIO-CONTROLLO-NULL
           ELSE
              MOVE (SF)-SERVIZIO-CONTROLLO(IX-TAB-MMO)
              TO MMO-SERVIZIO-CONTROLLO
           END-IF.
           IF (SF)-COD-PROCESSO-WF-NULL(IX-TAB-MMO) = HIGH-VALUES
              MOVE (SF)-COD-PROCESSO-WF-NULL(IX-TAB-MMO)
              TO MMO-COD-PROCESSO-WF-NULL
           ELSE
              MOVE (SF)-COD-PROCESSO-WF(IX-TAB-MMO)
              TO MMO-COD-PROCESSO-WF
           END-IF..
       VAL-DCLGEN-MMO-EX.
           EXIT.

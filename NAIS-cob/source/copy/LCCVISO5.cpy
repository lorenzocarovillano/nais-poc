
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVISO5
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-ISO.
           MOVE (SF)-TP-OGG(IX-TAB-ISO)
              TO ISO-TP-OGG
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-ISO) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-ISO)
              TO ISO-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-ISO)
              TO ISO-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-ISO) NOT NUMERIC
              MOVE 0 TO ISO-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-ISO)
              TO ISO-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-ISO) NOT NUMERIC
              MOVE 0 TO ISO-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-ISO)
              TO ISO-DT-END-EFF
           END-IF
           IF (SF)-DT-INI-PER-NULL(IX-TAB-ISO) = HIGH-VALUES
              MOVE (SF)-DT-INI-PER-NULL(IX-TAB-ISO)
              TO ISO-DT-INI-PER-NULL
           ELSE
             IF (SF)-DT-INI-PER(IX-TAB-ISO) = ZERO
                MOVE HIGH-VALUES
                TO ISO-DT-INI-PER-NULL
             ELSE
              MOVE (SF)-DT-INI-PER(IX-TAB-ISO)
              TO ISO-DT-INI-PER
             END-IF
           END-IF
           IF (SF)-DT-END-PER-NULL(IX-TAB-ISO) = HIGH-VALUES
              MOVE (SF)-DT-END-PER-NULL(IX-TAB-ISO)
              TO ISO-DT-END-PER-NULL
           ELSE
             IF (SF)-DT-END-PER(IX-TAB-ISO) = ZERO
                MOVE HIGH-VALUES
                TO ISO-DT-END-PER-NULL
             ELSE
              MOVE (SF)-DT-END-PER(IX-TAB-ISO)
              TO ISO-DT-END-PER
             END-IF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-ISO)
              TO ISO-COD-COMP-ANIA
           IF (SF)-IMPST-SOST-NULL(IX-TAB-ISO) = HIGH-VALUES
              MOVE (SF)-IMPST-SOST-NULL(IX-TAB-ISO)
              TO ISO-IMPST-SOST-NULL
           ELSE
              MOVE (SF)-IMPST-SOST(IX-TAB-ISO)
              TO ISO-IMPST-SOST
           END-IF
           IF (SF)-IMPB-IS-NULL(IX-TAB-ISO) = HIGH-VALUES
              MOVE (SF)-IMPB-IS-NULL(IX-TAB-ISO)
              TO ISO-IMPB-IS-NULL
           ELSE
              MOVE (SF)-IMPB-IS(IX-TAB-ISO)
              TO ISO-IMPB-IS
           END-IF
           IF (SF)-ALQ-IS-NULL(IX-TAB-ISO) = HIGH-VALUES
              MOVE (SF)-ALQ-IS-NULL(IX-TAB-ISO)
              TO ISO-ALQ-IS-NULL
           ELSE
              MOVE (SF)-ALQ-IS(IX-TAB-ISO)
              TO ISO-ALQ-IS
           END-IF
           IF (SF)-COD-TRB-NULL(IX-TAB-ISO) = HIGH-VALUES
              MOVE (SF)-COD-TRB-NULL(IX-TAB-ISO)
              TO ISO-COD-TRB-NULL
           ELSE
              MOVE (SF)-COD-TRB(IX-TAB-ISO)
              TO ISO-COD-TRB
           END-IF
           IF (SF)-PRSTZ-LRD-ANTE-IS-NULL(IX-TAB-ISO) = HIGH-VALUES
              MOVE (SF)-PRSTZ-LRD-ANTE-IS-NULL(IX-TAB-ISO)
              TO ISO-PRSTZ-LRD-ANTE-IS-NULL
           ELSE
              MOVE (SF)-PRSTZ-LRD-ANTE-IS(IX-TAB-ISO)
              TO ISO-PRSTZ-LRD-ANTE-IS
           END-IF
           IF (SF)-RIS-MAT-NET-PREC-NULL(IX-TAB-ISO) = HIGH-VALUES
              MOVE (SF)-RIS-MAT-NET-PREC-NULL(IX-TAB-ISO)
              TO ISO-RIS-MAT-NET-PREC-NULL
           ELSE
              MOVE (SF)-RIS-MAT-NET-PREC(IX-TAB-ISO)
              TO ISO-RIS-MAT-NET-PREC
           END-IF
           IF (SF)-RIS-MAT-ANTE-TAX-NULL(IX-TAB-ISO) = HIGH-VALUES
              MOVE (SF)-RIS-MAT-ANTE-TAX-NULL(IX-TAB-ISO)
              TO ISO-RIS-MAT-ANTE-TAX-NULL
           ELSE
              MOVE (SF)-RIS-MAT-ANTE-TAX(IX-TAB-ISO)
              TO ISO-RIS-MAT-ANTE-TAX
           END-IF
           IF (SF)-RIS-MAT-POST-TAX-NULL(IX-TAB-ISO) = HIGH-VALUES
              MOVE (SF)-RIS-MAT-POST-TAX-NULL(IX-TAB-ISO)
              TO ISO-RIS-MAT-POST-TAX-NULL
           ELSE
              MOVE (SF)-RIS-MAT-POST-TAX(IX-TAB-ISO)
              TO ISO-RIS-MAT-POST-TAX
           END-IF
           IF (SF)-PRSTZ-NET-NULL(IX-TAB-ISO) = HIGH-VALUES
              MOVE (SF)-PRSTZ-NET-NULL(IX-TAB-ISO)
              TO ISO-PRSTZ-NET-NULL
           ELSE
              MOVE (SF)-PRSTZ-NET(IX-TAB-ISO)
              TO ISO-PRSTZ-NET
           END-IF
           IF (SF)-PRSTZ-PREC-NULL(IX-TAB-ISO) = HIGH-VALUES
              MOVE (SF)-PRSTZ-PREC-NULL(IX-TAB-ISO)
              TO ISO-PRSTZ-PREC-NULL
           ELSE
              MOVE (SF)-PRSTZ-PREC(IX-TAB-ISO)
              TO ISO-PRSTZ-PREC
           END-IF
           IF (SF)-CUM-PRE-VERS-NULL(IX-TAB-ISO) = HIGH-VALUES
              MOVE (SF)-CUM-PRE-VERS-NULL(IX-TAB-ISO)
              TO ISO-CUM-PRE-VERS-NULL
           ELSE
              MOVE (SF)-CUM-PRE-VERS(IX-TAB-ISO)
              TO ISO-CUM-PRE-VERS
           END-IF
           MOVE (SF)-TP-CALC-IMPST(IX-TAB-ISO)
              TO ISO-TP-CALC-IMPST
           MOVE (SF)-IMP-GIA-TASSATO(IX-TAB-ISO)
              TO ISO-IMP-GIA-TASSATO
           IF (SF)-DS-RIGA(IX-TAB-ISO) NOT NUMERIC
              MOVE 0 TO ISO-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-ISO)
              TO ISO-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-ISO)
              TO ISO-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-ISO) NOT NUMERIC
              MOVE 0 TO ISO-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-ISO)
              TO ISO-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-ISO) NOT NUMERIC
              MOVE 0 TO ISO-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-ISO)
              TO ISO-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-ISO) NOT NUMERIC
              MOVE 0 TO ISO-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-ISO)
              TO ISO-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-ISO)
              TO ISO-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-ISO)
              TO ISO-DS-STATO-ELAB.
       VAL-DCLGEN-ISO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY      ..... LCCVB046
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO BIL_ESTRATTI
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVB045 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN VINCOLO PEGNO (LCCVB041)
      *
      *----------------------------------------------------------------*
       SCRIVI-BIL-VAR-DI-CALC-P.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE BILA-VAR-CALC-P.

      *--> SE LO STATUS NON E' "INVARIATO" O "CONVERSAZIONE"
           IF  NOT WB04-ST-INV
           AND NOT WB04-ST-CON
           AND WB04-ELE-B04-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WB04-ST-ADD

      *-->    ESTRAZIONE SEQUENCE E VALORIZZAZIONE CONSEGUENTE DEL CAMPO
      *-->    WB04-ID-BILA-VAR-CALC-P INIBITE X MOTIVI DI PERFORMANCE
      *
      *-->             ESTRAZIONE E VALORIZZAZIONE SEQUENCE
      *                 PERFORM ESTR-SEQUENCE
      *                    THRU ESTR-SEQUENCE-EX

      *                 IF IDSV0001-ESITO-OK
      *                    MOVE S090-SEQ-TABELLA
                           MOVE ZERO
                            TO WB04-ID-BILA-VAR-CALC-P
      *                 END-IF

      *-->        TIPO OPERAZIONE DISPATCHER
                       SET IDSI0011-INSERT TO TRUE

      *-->        MODIFICA
      *           WHEN WB04-ST-MOD
      *
      *
      *                MOVE WB04-ID-BILA-VAR-CALC-P
      *                  TO B04-ID-BILA-VAR-CALC-P
      *
      *-->        TIPO OPERAZIONE DISPATCHER
      *                SET IDSI0011-UPDATE TO TRUE
      *
      *-->        CANCELLAZIONE
      *           WHEN WB04-ST-DEL
      *
      *                MOVE WB04-ID-BILA-VAR-CALC-P
      *                  TO B04-ID-BILA-VAR-CALC-P
      *
      *-->             TIPO OPERAZIONE DISPATCHER
      *                SET  IDSI0011-DELETE-LOGICA    TO TRUE
      *
              END-EVALUATE

      *-->    VALORIZZA DCLGEN ADESIONE
              PERFORM VAL-DCLGEN-B04
                 THRU VAL-DCLGEN-B04-EX

      *-->    VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
              IF WB04-ST-ADD
                 PERFORM VALORIZZA-AREA-DSH-B04
                    THRU VALORIZZA-AREA-DSH-B04-EX
      *-->    CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX
              END-IF

           END-IF.

       SCRIVI-BIL-VAR-DI-CALC-P-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-B04.

      *--> NOME TABELLA FISICA DB
           MOVE 'BILA-VAR-CALC-P'           TO WK-TABELLA.

      *--> DCLGEN TABELLA
           MOVE BILA-VAR-CALC-P             TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET IDSI0011-PRIMARY-KEY        TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-SENZA-STOR  TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE IDSV0001-DATA-EFFETTO   TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-B04-EX.
           EXIT.

      ******************************************************************
      *    TP_MOVI (DA 6000 A 6999)
      ******************************************************************
           88 SCHEDUL-OPERAZ-AUTOM                    VALUE 6000.
           88 RITAR-ACCES                             VALUE 6001.
           88 GENER-TRANCH                            VALUE 6002.
           88 GENER-TRAINC                            VALUE 6003.
           88 GENER-TRAPRE                            VALUE 6004.
           88 GENER-RISPAR                            VALUE 6005.
           88 ADPRE-PRESTA                            VALUE 6006.
           88 GENER-CEDOLA                            VALUE 6007.
           88 RICON-COLLET                            VALUE 6008.
           88 VARIA-OPZION                            VALUE 6009.
           88 CALC-IMPOSTA-SOSTIT                     VALUE 6010.
      *    88 PREN-ADDEBITO-RID                       VALUE 6011.
           88 CALC-IMPST-BOLLO                        VALUE 6012.
           88 BONUS-FEDE                              VALUE 6013.
           88 SIGNI-RISCH                             VALUE 6014.
           88 ATTUA-STINV                             VALUE 6015.
           88 BONUS-RICOR                             VALUE 6016.
           88 DETERM-BONUS                            VALUE 6017.
           88 LIQUI-CEDOLE                            VALUE 6018.
           88 APPL-CALC-IMPST-SOST                    VALUE 6020.
           88 STORNO-AUTOMATICO-PUR                   VALUE 6021.
           88 LIQUI-MANFEE                            VALUE 6023.
           88 CALC-MANFEE-RICH                        VALUE 6024.
           88 LIQ-MANFEE-ANTIC                        VALUE 6027.
           88 LIQ-MANFEE-RICH                         VALUE 6028.
           88 LIQ-MANFEE-RISC-SECON-AA                VALUE 6029.
           88 LIQ-MANFEE-RISC-ALTRE-PO                VALUE 6030.
           88 COMMISSIONI-UNIT                        VALUE 6031.
           88 CALC-BONUS-REBATE                       VALUE 6032.
           88 DIFFERIMENTO-PROROGA                    VALUE 6033.
           88 LIQ-MAS-IS-X-ANTIC                      VALUE 6034.
           88 CONS-CNBT-CPTZ                          VALUE 6035.
           88 RISPAR-PROGRAM-REINV                    VALUE 6036.
           88 MOVIM-STORNO-CEDOLA                     VALUE 6037.
           88 GENER-PREVIRT                           VALUE 6038.
           88 ADEG-PRESGIORN                          VALUE 6039.
           88 PRESCRZ-AUTOM                           VALUE 6040.
           88 COMUN-DORM-MEF                          VALUE 6041.
           88 LIQUI-DORM-MEF                          VALUE 6042.
           88 ATT-SERV-CONS-RENDIM                    VALUE 6043.
           88 ATT-SERV-REINVST-CALI                   VALUE 6044.
           88 ATT-SERV-BIG-CHANCE-DE                  VALUE 6045.
           88 CALCOLI-PREVISIONALI                    VALUE 6049.
           88 RINN-TAC-KM2009                         VALUE 6050.
           88 VERIFICA-RINNOVO-TACITO                 VALUE 6051.
           88 STATIS-RICH-EST                         VALUE 6052.
           88 ATT-SERV-BIG-CHANCE                     VALUE 6053.
           88 ATTRIB-COSTI-AMM                        VALUE 6054.
           88 CALC-BONUS-GG-KM2009                    VALUE 6056.
           88 APPLICA-BONUS-KM2009                    VALUE 6057.
           88 CRIST-AL-20140630                       VALUE 6058.
13709      88 REPORT-COMMIS-GEST                      VALUE 6059.
15784      88 ESTRAZIONE-DECESSI-COMUNICATI           VALUE 6063.
15410      88 ESTRAZIONE-CALCOLO-COSTI                VALUE 6064.
15012      88 GESTIONE-TRANCHE-CT0                    VALUE 6065.
13815F     88 INVEST-OPER-STRAORD                     VALUE 6070.
13815F     88 DISINV-OPER-STRAORD                     VALUE 6071.
AMLF3      88 MOVI-ANAG                               VALUE 6080.
AMLF3      88 MOVI-ANAG-EVE                           VALUE 6081.
           88 MOVIM-QUIETA                            VALUE 6101.
12807      88 COMMISSIONI-UNIT-SWITCH                 VALUE 6131.
           88 MOVIM-QUIETA-INT-PREST                  VALUE 6102.
           88 INVESTIMENTO                            VALUE 6201.
           88 DISINVESTIMENTO                         VALUE 6202.
           88 REINVESTIMENTO                          VALUE 6203.
           88 CALCOLO-RISERVE                         VALUE 6204.
           88 COMUNIC-INVESTIMENTO                    VALUE 6205.
           88 COMUNIC-DISINVESTIMENTO                 VALUE 6206.
           88 PORTAFOGLIO-DINAMICO                    VALUE 6207.
           88 ESTRATTO-CONTO                          VALUE 6208.
           88 ATT-STRAT-INVST                         VALUE 6210.
           88 AGG-SALDO-QUOTE                         VALUE 6212.
           88 AGG-SALDO-PREMI                         VALUE 6213.
           88 CERTIF-FISCALI                          VALUE 6214.
           88 ELAB-FLS-BATCH-FND                      VALUE 6301.
           88 CONS-ELAB-FLS-BATCH-FND                 VALUE 6302.
           88 CAMBIO-STATO                            VALUE 6305.
           88 QUADR-CPI-PROT-BUND                     VALUE 6306.

           88 INQUIRY-COLL                            VALUE 6500.
           88 INQUIRY-ADES                            VALUE 6501.
           88 INQUIRY-INDIV                           VALUE 6502.
           88 INQUIRY-DAT-DEF                         VALUE 6503.
           88 ANALISI-PERDITE                         VALUE 6601.
           88 ESPLO-TRATTATI                          VALUE 6700.
           88 RICALC-RINNOVO                          VALUE 6701.
           88 DIST-PRE-RISC-CESSIONI                  VALUE 6710.
           88 DIST-PRE-RISC-RINNOVI                   VALUE 6711.
           88 DIST-PRE-RISC-STORNI                    VALUE 6712.
           88 DIST-PRE-RISC-LIQUIDAZ                  VALUE 6713.
           88 DIST-PRE-RISC-RIATTIVAZ                 VALUE 6714.
           88 DIST-PRE-COMMERC-CESSIONI               VALUE 6715.
           88 DIST-PRE-COMMERC-RINNOVI                VALUE 6716.
           88 DIST-PRE-COMMERC-STORNI                 VALUE 6717.
           88 DIST-PRE-COMMERC-LIQUIDAZ               VALUE 6718.
           88 DIST-PRE-COMMERC-RIATTIVAZ              VALUE 6719.

           88 RICAGG-GEST-RENDITE                     VALUE 6800.
           88 GEST-DATI-PAGAM-RID                     VALUE 6801.
           88 CALC-BONUS-SCADENZA                     VALUE 6802.

           88 SOSPENSIONE-RENDITA                     VALUE 6901.
           88 RIATTIVAZIONE-RENDITA                   VALUE 6902.
           88 QUIETANZAM-RATA-DI-RENDITA              VALUE 6903.
           88 STORNO-CONCLUSIONE-RENDITA              VALUE 6904.
PDLST      88 PRENOTA-RECUP-CONFERME                  VALUE 6905.
PDLST      88 PRENOTA-CARICA-CONF-GIORNO              VALUE 6906.
PDLST      88 PRENOTA-RECUP-GAP-IM                    VALUE 6907.
PDLST      88 PRENOTA-ESTRAT-PDL                      VALUE 6908.
PDLST      88 PRENOTA-ESTRAT-STOR-SCAD                VALUE 6909.
PDLST      88 PRENOTA-RECUP-FILE-PREC                 VALUE 6910.
PDLST      88 PRENOTA-RECUP-CALCOLO-PDL               VALUE 6911.
PDLST      88 PRENOTA-BONFICA-FLUSSO                  VALUE 6912.
PDLST      88 PRENOTA-RECUP-BACKUP                    VALUE 6913.

      *----------------------------------------------------------------*
      *   AREA INPUT VARIABILI
      *   PORTAFOGLIO VITA - PROCESSO VENDITA
      *   LUNG.
      *----------------------------------------------------------------*
      *--  DATI DI INPUT

           03 (SF)-DATI-INPUT.
              05 (SF)-PGM                           PIC X(08).

      *--     FLAG IDENTIFICATIVO DELLA MODALITA DI CHIAMATA
              05 (SF)-FLG-AREA                      PIC  X(02).
                 88 (SF)-AREA-VE                    VALUE 'VE'.
                 88 (SF)-AREA-PV                    VALUE 'PV'.

              05 (SF)-MODALITA-ESECUTIVA            PIC X.
                 88 (SF)-ON-LINE                    VALUE 'O'.
                 88 (SF)-BATCH                      VALUE 'B'.
                 88 (SF)-BATCH-INFR                 VALUE 'I'.

              05 (SF)-COD-COMPAGNIA-ANIA            PIC  9(05).
              05 (SF)-TIPO-MOVIMENTO                PIC  9(05).
              05 (SF)-TIPO-MOVI-ORIG                PIC  9(05).
              05 (SF)-COD-MAIN-BATCH                PIC  X(12).
      *        05 (SF)-COD-SERVIZIO-BE               PIC  X(12).
              05 (SF)-DATA-EFFETTO                  PIC S9(08) COMP-3.
              05 (SF)-DATA-COMPETENZA               PIC S9(18) COMP-3.
              05 (SF)-DATA-COMP-AGG-STOR            PIC S9(18) COMP-3.
NEW           05 (SF)-DATA-ULT-VERS-PROD            PIC S9(08) COMP-3.
              05 (SF)-DATA-ULT-TIT-INC              PIC S9(08) COMP-3.

              05 (SF)-TRATTAMENTO-STORICITA         PIC  X(03).

              05 (SF)-FORMATO-DATA-DB               PIC X(003).
                88 (SF)-DB-ISO                      VALUE 'ISO'.
                88 (SF)-DB-EUR                      VALUE 'EUR'.

      *--     FLAG IDENTIFICATIVO DEL PASSO DI ELABORAZIONE
              05 (SF)-STEP-ELAB                     PIC  X(01).
                 88 (SF)-PRE-CONV                   VALUE '0'.
                 88 (SF)-POST-CONV                  VALUE '1'.
                 88 (SF)-IN-CONV                    VALUE '2'.
                 88 (SF)-PROCESSO-ADESIONE          VALUE '3'.
                 88 (SF)-CONTROLLO-INIZIATIVA       VALUE '4'.
                 88 (SF)-ATTIVAZ-INIZIATIVA         VALUE '5'.
                 88 (SF)-PRE-CALC-INIZIATIVA        VALUE '6'.
                 88 (SF)-POST-CALC-INIZIATIVA       VALUE '7'.

              05 (SF)-KEY-AUT-OPER1                 PIC X(020).
              05 (SF)-KEY-AUT-OPER2                 PIC X(020).
              05 (SF)-KEY-AUT-OPER3                 PIC X(020).
              05 (SF)-KEY-AUT-OPER4                 PIC X(020).
              05 (SF)-KEY-AUT-OPER5                 PIC X(020).

      *
              05 (SF)-FLAG-GAR-OPZIONE              PIC X(01).
                 88 (SF)-FLAG-GAR-OPZIONE-SI        VALUE 'S'.
                 88 (SF)-FLAG-GAR-OPZIONE-NO        VALUE 'N'.

      *--     STRUTTURA DI MAPPING DELLE AREE PASSATE IN INPUT
              05 (SF)-ELE-INFO-MAX                  PIC S9(04) COMP-3.
           03 (SF)-TAB-INFO1.
              06 (SF)-TAB-INFO             OCCURS 100
                                         INDEXED BY (SF)-SEARCH-IND.
                 07 (SF)-TAB-ALIAS               PIC  X(03).
                 07 (SF)-NUM-ELE-TAB-ALIAS       PIC S9(05) COMP-3.
                 07 (SF)-POSIZ-INI               PIC S9(09) COMP-3.
                 07 (SF)-LUNGHEZZA               PIC S9(09) COMP-3.
           03 (SF)-TAB-INFO1-R  REDEFINES (SF)-TAB-INFO1.
              06 FILLER                          PIC X(16).
              06 (SF)-RESTO-TAB-INFO1            PIC X(1584).

           03 (SF)-RESTO-DATI.
      *       05 (SF)-BUFFER-DATI                   PIC  X(2000000).
      *       05 (SF)-BUFFER-DATI                   PIC  X(1700000).
              05 (SF)-BUFFER-DATI                   PIC  X(4500000).
      *--     ID TABELLA TEMPORANEA ALTERNATIVA AL BUFFER DATI
              05 (SF)-ID-TAB-TEMP                   PIC S9(09) COMP-3.


      *--    return code
              05 (SF)-RETURN-CODE                   PIC  X(002).
                88 (SF)-SUCCESSFUL-RC               VALUE '00'.
                88 (SF)-GENERIC-ERROR               VALUE 'KO'.
                88 (SF)-SQL-ERROR                   VALUE 'D3'.
                88 (SF)-DT-VLDT-PROD-NOT-V          VALUE 'DT'.
                88 (SF)-BUFFER-NOT-VALID            VALUE 'BU'.
                88 (SF)-AREA-NOT-FOUND              VALUE 'AF'.
                88 (SF)-STEP-ELAB-NOT-VALID         VALUE 'SE'.
                88 (SF)-FLAG-AREA-NOT-VALID         VALUE 'FA'.
                88 (SF)-TIPO-MOVIM-NOT-VALID        VALUE 'TM'.
                88 (SF)-VERS-PROD-NOT-VALID         VALUE 'VP'.
                88 (SF)-VERS-OPZ-NOT-VALID          VALUE 'VO'.
                88 (SF)-CALCOLO-NON-COMPLETO        VALUE 'CA'.
                88 (SF)-CACHE-PIENA                 VALUE 'CP'.
                88 (SF)-TIPO-SCHEDA-NOT-VALID       VALUE 'TS'.
                88 (SF)-FUNZ-NON-DEFINITA           VALUE 'FU'.
                88 (SF)-GLOVARLIST-VUOTA            VALUE 'GV'.
                88 (SF)-TRATTATO-NOT-VALID          VALUE 'TV'.
NEWP            88 (SF)-SKIP-CALL-ACTUATOR          VALUE 'SA'.

      *--    campi esito
             05 (SF)-CAMPI-ESITO.
                15 (SF)-DESCRIZ-ERR                 PIC  X(300).
                15 (SF)-COD-SERVIZIO-BE             PIC  X(008).
                15 (SF)-NOME-TABELLA                PIC  X(018).
                15 (SF)-KEY-TABELLA                 PIC  X(020).

      *-->    Gestione output variabili variabili
             05 (SF)-AREA-VAR-KO.

                10 (SF)-NOT-FOUND-MVV.
                   15 (SF)-ELE-MAX-NOT-FOUND      PIC S9(04) COMP-3.
                   15 (SF)-AREA-VAR-NOT-FOUND.
                      17 (SF)-TAB-VAR-NOT-FOUND      OCCURS 5.
                         20 (SF)-VAR-NOT-FOUND       PIC X(019).
                         20 (SF)-VAR-NOT-FOUND-SP    PIC X(01).

                10 (SF)-CALCOLO-KO.
                   15 (SF)-ELE-MAX-CALC-KO        PIC S9(04) COMP-3.
                   15 (SF)-AREA-CALC-KO.
                      17 (SF)-TAB-CALCOLO-KO         OCCURS 5.
                         20 (SF)-VAR-CALCOLO-KO      PIC X(019).
                         20 (SF)-VAR-NOT-FOUND-SP    PIC X(01).

             05 (SF)-AREA-ADDRESSES.
                15 (SF)-ADDRESSES                   OCCURS 5 TIMES.
                   20 (SF)-ADDRESS-TYPE             PIC X(01).
                   20 (SF)-ADDRESS                  POINTER.

             05 (SF)-CODICE-INIZIATIVA              PIC X(012).

             05 (SF)-CODICE-TRATTATO                PIC X(012).

13382        05 (SF)-FL-AREA-VAR-EXTRA              PIC X(001).
13382           88 (SF)-FL-AREA-VAR-EXTRA-SI        VALUE 'S'.
13382           88 (SF)-FL-AREA-VAR-EXTRA-NO        VALUE 'N'.

13382 *      05 FILLER                              PIC X(071).
13382        05 FILLER                              PIC X(070).

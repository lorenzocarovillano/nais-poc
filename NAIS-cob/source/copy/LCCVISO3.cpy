
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVISO3
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-ISO.
           MOVE ISO-ID-IMPST-SOST
             TO (SF)-ID-PTF(IX-TAB-ISO)
           MOVE ISO-ID-IMPST-SOST
             TO (SF)-ID-IMPST-SOST(IX-TAB-ISO)
           IF ISO-ID-OGG-NULL = HIGH-VALUES
              MOVE ISO-ID-OGG-NULL
                TO (SF)-ID-OGG-NULL(IX-TAB-ISO)
           ELSE
              MOVE ISO-ID-OGG
                TO (SF)-ID-OGG(IX-TAB-ISO)
           END-IF
           MOVE ISO-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-ISO)
           MOVE ISO-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-ISO)
           IF ISO-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE ISO-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-ISO)
           ELSE
              MOVE ISO-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-ISO)
           END-IF
           MOVE ISO-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-ISO)
           MOVE ISO-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-ISO)
           IF ISO-DT-INI-PER-NULL = HIGH-VALUES
              MOVE ISO-DT-INI-PER-NULL
                TO (SF)-DT-INI-PER-NULL(IX-TAB-ISO)
           ELSE
              MOVE ISO-DT-INI-PER
                TO (SF)-DT-INI-PER(IX-TAB-ISO)
           END-IF
           IF ISO-DT-END-PER-NULL = HIGH-VALUES
              MOVE ISO-DT-END-PER-NULL
                TO (SF)-DT-END-PER-NULL(IX-TAB-ISO)
           ELSE
              MOVE ISO-DT-END-PER
                TO (SF)-DT-END-PER(IX-TAB-ISO)
           END-IF
           MOVE ISO-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-ISO)
           IF ISO-IMPST-SOST-NULL = HIGH-VALUES
              MOVE ISO-IMPST-SOST-NULL
                TO (SF)-IMPST-SOST-NULL(IX-TAB-ISO)
           ELSE
              MOVE ISO-IMPST-SOST
                TO (SF)-IMPST-SOST(IX-TAB-ISO)
           END-IF
           IF ISO-IMPB-IS-NULL = HIGH-VALUES
              MOVE ISO-IMPB-IS-NULL
                TO (SF)-IMPB-IS-NULL(IX-TAB-ISO)
           ELSE
              MOVE ISO-IMPB-IS
                TO (SF)-IMPB-IS(IX-TAB-ISO)
           END-IF
           IF ISO-ALQ-IS-NULL = HIGH-VALUES
              MOVE ISO-ALQ-IS-NULL
                TO (SF)-ALQ-IS-NULL(IX-TAB-ISO)
           ELSE
              MOVE ISO-ALQ-IS
                TO (SF)-ALQ-IS(IX-TAB-ISO)
           END-IF
           IF ISO-COD-TRB-NULL = HIGH-VALUES
              MOVE ISO-COD-TRB-NULL
                TO (SF)-COD-TRB-NULL(IX-TAB-ISO)
           ELSE
              MOVE ISO-COD-TRB
                TO (SF)-COD-TRB(IX-TAB-ISO)
           END-IF
           IF ISO-PRSTZ-LRD-ANTE-IS-NULL = HIGH-VALUES
              MOVE ISO-PRSTZ-LRD-ANTE-IS-NULL
                TO (SF)-PRSTZ-LRD-ANTE-IS-NULL(IX-TAB-ISO)
           ELSE
              MOVE ISO-PRSTZ-LRD-ANTE-IS
                TO (SF)-PRSTZ-LRD-ANTE-IS(IX-TAB-ISO)
           END-IF
           IF ISO-RIS-MAT-NET-PREC-NULL = HIGH-VALUES
              MOVE ISO-RIS-MAT-NET-PREC-NULL
                TO (SF)-RIS-MAT-NET-PREC-NULL(IX-TAB-ISO)
           ELSE
              MOVE ISO-RIS-MAT-NET-PREC
                TO (SF)-RIS-MAT-NET-PREC(IX-TAB-ISO)
           END-IF
           IF ISO-RIS-MAT-ANTE-TAX-NULL = HIGH-VALUES
              MOVE ISO-RIS-MAT-ANTE-TAX-NULL
                TO (SF)-RIS-MAT-ANTE-TAX-NULL(IX-TAB-ISO)
           ELSE
              MOVE ISO-RIS-MAT-ANTE-TAX
                TO (SF)-RIS-MAT-ANTE-TAX(IX-TAB-ISO)
           END-IF
           IF ISO-RIS-MAT-POST-TAX-NULL = HIGH-VALUES
              MOVE ISO-RIS-MAT-POST-TAX-NULL
                TO (SF)-RIS-MAT-POST-TAX-NULL(IX-TAB-ISO)
           ELSE
              MOVE ISO-RIS-MAT-POST-TAX
                TO (SF)-RIS-MAT-POST-TAX(IX-TAB-ISO)
           END-IF
           IF ISO-PRSTZ-NET-NULL = HIGH-VALUES
              MOVE ISO-PRSTZ-NET-NULL
                TO (SF)-PRSTZ-NET-NULL(IX-TAB-ISO)
           ELSE
              MOVE ISO-PRSTZ-NET
                TO (SF)-PRSTZ-NET(IX-TAB-ISO)
           END-IF
           IF ISO-PRSTZ-PREC-NULL = HIGH-VALUES
              MOVE ISO-PRSTZ-PREC-NULL
                TO (SF)-PRSTZ-PREC-NULL(IX-TAB-ISO)
           ELSE
              MOVE ISO-PRSTZ-PREC
                TO (SF)-PRSTZ-PREC(IX-TAB-ISO)
           END-IF
           IF ISO-CUM-PRE-VERS-NULL = HIGH-VALUES
              MOVE ISO-CUM-PRE-VERS-NULL
                TO (SF)-CUM-PRE-VERS-NULL(IX-TAB-ISO)
           ELSE
              MOVE ISO-CUM-PRE-VERS
                TO (SF)-CUM-PRE-VERS(IX-TAB-ISO)
           END-IF
           MOVE ISO-TP-CALC-IMPST
             TO (SF)-TP-CALC-IMPST(IX-TAB-ISO)
           MOVE ISO-IMP-GIA-TASSATO
             TO (SF)-IMP-GIA-TASSATO(IX-TAB-ISO)
           MOVE ISO-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-ISO)
           MOVE ISO-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-ISO)
           MOVE ISO-DS-VER
             TO (SF)-DS-VER(IX-TAB-ISO)
           MOVE ISO-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-ISO)
           MOVE ISO-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-ISO)
           MOVE ISO-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-ISO)
           MOVE ISO-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-ISO).
       VALORIZZA-OUTPUT-ISO-EX.
           EXIT.

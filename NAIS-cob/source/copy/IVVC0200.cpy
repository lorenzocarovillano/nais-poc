      *----------------------------------------------------------------*
      *   AREA OUTPUT VALORIZZATORE VARIABILI
      *   - area skeda per servizi di prodotto
      *   - area variabili autonomia operativa
      *----------------------------------------------------------------*

      *AREA SKEDA PER SERVIZI DI PRODOTTO
      *    05 (SF)-NUM-MAX-LIVELLI              PIC 9(04) VALUE 300.
           05 (SF)-NUM-MAX-LIVELLI-P            PIC 9(04) VALUE 3.
           05 (SF)-NUM-MAX-LIVELLI-T            PIC 9(04) VALUE 750.
           05 (SF)-NUM-MAX-VARIABILI-P          PIC 9(03) VALUE 100.
           05 (SF)-NUM-MAX-VARIABILI-T          PIC 9(03) VALUE 75.

           05 (SF)-DEE                          PIC X(008).

           05 (SF)-ELE-LIVELLO-MAX-P            PIC S9(04) COMP-3.
           05 (SF)-TAB-VAL-P.
            06 (SF)-TAB-LIVELLO-P                OCCURS 3.
               07 (SF)-ID-POL-P                  PIC  9(09).
               07 (SF)-DATI-LIVELLO-P.
                  09 (SF)-COD-TIPO-OPZIONE-P     PIC X(002).
                  09 (SF)-TP-LIVELLO-P           PIC  X(01).
                  09 (SF)-COD-LIVELLO-P          PIC  X(12).
                  09 (SF)-ID-LIVELLO-P           PIC 9(09).
                  09 (SF)-DT-INIZ-PROD-P         PIC X(008).
                  09 (SF)-COD-RGM-FISC-P         PIC X(002).
                  09 (SF)-NOME-SERVIZIO-P        PIC  X(08).
                  09 (SF)-AREA-VARIABILI-P.
                     11 (SF)-ELE-VARIABILI-MAX-P PIC S9(04) COMP-3.
                     11 (SF)-TAB-VARIABILI-P     OCCURS 100.
                       12 (SF)-AREA-VARIABILE-P.
                        13 (SF)-COD-VARIABILE-P  PIC  X(12).
                        13 (SF)-TP-DATO-P        PIC  X(01).
                        13 (SF)-VAL-GENERICO-P   PIC  X(60).
      *----------------------------------------------------------------*
      *   AREA VALORE RIDEFINITO
      *----------------------------------------------------------------*
                        13 (SF)-VAL-IMP-GEN-P      REDEFINES
                           (SF)-VAL-GENERICO-P.
                           15 (SF)-VAL-IMP-P       PIC  S9(11)V9(07).
                           15 (SF)-VAL-IMP-FILLER-P
                                                   PIC X(42).

                        13 (SF)-VAL-PERC-GEN-P     REDEFINES
                           (SF)-VAL-GENERICO-P.
                           15 (SF)-VAL-PERC-P      PIC   9(05)V9(09).
                           15 (SF)-VAL-PERC-FILLER-P
                                                   PIC X(46).

                        13 (SF)-VAL-STR-GEN-P      REDEFINES
                           (SF)-VAL-GENERICO-P.
                           15 (SF)-VAL-STR-P       PIC X(60).

           05 (SF)-TAB-VAL-R-P REDEFINES  (SF)-TAB-VAL-P.
            06 FILLER                          PIC  X(7354).
            06 (SF)-RESTO-TAB-VAL-P            PIC  X(14708).


      *----------------------------------------------------------------*
      *   AREA OUTPUT TRANCHE SCHEDE 'G' 'H'
      *----------------------------------------------------------------*
           05 (SF)-ELE-LIVELLO-MAX-T            PIC S9(04) COMP-3.
           05 (SF)-TAB-VAL-T.
            06 (SF)-TAB-LIVELLO-T                OCCURS 750.
               07 (SF)-ID-GAR-T                  PIC  9(09).
               07 (SF)-DATI-LIVELLO-T.
                  09 (SF)-COD-TIPO-OPZIONE-T     PIC X(002).
                  09 (SF)-TP-LIVELLO-T           PIC  X(01).
                  09 (SF)-COD-LIVELLO-T          PIC  X(12).
                  09 (SF)-ID-LIVELLO-T           PIC 9(09).
                  09 (SF)-DT-DECOR-TRCH-T        PIC X(008).
                  09 (SF)-DT-INIZ-TARI-T         PIC X(008).
                  09 (SF)-COD-RGM-FISC-T         PIC X(002).
                  09 (SF)-NOME-SERVIZIO-T        PIC  X(08).

                  09 (SF)-TIPO-TRCH              PIC 9(002).
                  09 (SF)-FLG-ITN                PIC 9(001).
                     88 (SF)-FLG-ITN-POS         VALUE 0.
                     88 (SF)-FLG-ITN-NEG         VALUE 1.

                  09 (SF)-AREA-VARIABILI-T.
                     11 (SF)-ELE-VARIABILI-MAX-T PIC S9(04) COMP-3.
                     11 (SF)-TAB-VARIABILI-T     OCCURS 75.
                       12 (SF)-AREA-VARIABILE-T.
                        13 (SF)-COD-VARIABILE-T  PIC  X(12).
                        13 (SF)-TP-DATO-T        PIC  X(01).
                        13 (SF)-VAL-GENERICO-T   PIC  X(60).

      *----------------------------------------------------------------*
      *   AREA VALORE RIDEFINITO
      *----------------------------------------------------------------*
                        13 (SF)-VAL-IMP-GEN-T      REDEFINES
                           (SF)-VAL-GENERICO-T.
                           15 (SF)-VAL-IMP-T       PIC  S9(11)V9(07).
                           15 (SF)-VAL-IMP-FILLER-T
                                                   PIC X(42).

                        13 (SF)-VAL-PERC-GEN-T     REDEFINES
                           (SF)-VAL-GENERICO-T.
                           15 (SF)-VAL-PERC-T      PIC   9(05)V9(09).
                           15 (SF)-VAL-PERC-FILLER-T
                                                   PIC X(46).

                        13 (SF)-VAL-STR-GEN-T      REDEFINES
                           (SF)-VAL-GENERICO-T.
                           15 (SF)-VAL-STR-T       PIC X(60).


           05 (SF)-TAB-VAL-R-T REDEFINES  (SF)-TAB-VAL-T.
            06 FILLER                          PIC  X(5540).
            06 (SF)-RESTO-TAB-VAL-T            PIC  X(4149460).


      *AREA VARIABILI AUTONOMIA OPERATIVA
           05 (SF)-VAR-AUT-OPER.
            06 (SF)-ELE-CTRL-AUT-OPER-MAX        PIC S9(04) COMP-3.
            06 (SF)-CTRL-AUT-OPER                OCCURS 20.
             10 (SF)-COD-ERRORE                 PIC S9(09) COMP-5.
             10 (SF)-COD-LIV-AUT                PIC S9(05) COMP-3.
             10 (SF)-TP-MOT-DEROGA              PIC  X(02).
             10 (SF)-MOD-VERIFICA               PIC  X(08).
             10 (SF)-CODICE-CONDIZIONE          PIC X(50).
             10 (SF)-PROGRESS-CONDITION         PIC S9(03)V COMP-3.
             10 (SF)-RISULTATO-CONDIZIONE       PIC  X(01).

             10 (SF)-ELE-PARAM-MAX              PIC S9(04) COMP-3.
             10 (SF)-TAB-PARAM                  OCCURS 20.
                15 (SF)-COD-PARAM               PIC X(030).
                15 (SF)-VAL-PARAM               PIC X(100).

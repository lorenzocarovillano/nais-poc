      * -----------------------------------------------------------
      *  CAMPI PER WHERE CONDITION JOIN STATO OGG WRK E GARANZIA
      * -----------------------------------------------------------
       01 LDBV3021.
          05 LDBV3021-ID-OGG             PIC S9(9)    COMP-3.
          05 LDBV3021-TP-OGG             PIC  X(2).
          05 LDBV3021-TP-STAT-BUS        PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS1       PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS2       PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS3       PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS4       PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS5       PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS6       PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS7       PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS8       PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS9       PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS10      PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS11      PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS12      PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS13      PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS14      PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS15      PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS16      PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS17      PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS18      PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS19      PIC  X(2).
          05 LDBV3021-TP-CAUS-BUS20      PIC  X(2).


       01  LDBVB441.
           05 LDBVB441-TP-TIT-01              PIC X(02).
           05 LDBVB441-TP-TIT-02              PIC X(02).
           05 LDBVB441-TP-STAT-TIT-1          PIC X(02).
           05 LDBVB441-TP-STAT-TIT-2          PIC X(02).
           05 LDBVB441-TP-STAT-TIT-3          PIC X(02).
           05 LDBVB441-ID-OGG                 PIC S9(9) COMP-3.
           05 LDBVB441-TP-OGG                 PIC X(02).
           05 LDBVB441-DT-MAX                 PIC 9(08).
           05 LDBVB441-DT-MAX-DB              PIC X(10).
           05 LDBVB441-DT-MAX-IND             PIC S9(4) COMP-5.

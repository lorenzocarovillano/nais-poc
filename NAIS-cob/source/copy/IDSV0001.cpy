       03 IDSV0001-AREA-CONTESTO.
          05 IDSV0001-AREA-COMUNE.
             10 IDSV0001-MODALITA-ESECUTIVA          PIC X.
                 88 IDSV0001-ON-LINE                 VALUE 'O'.
                 88 IDSV0001-BATCH                   VALUE 'B'.
                 88 IDSV0001-BATCH-INFR              VALUE 'I'.
             10 IDSV0001-COD-COMPAGNIA-ANIA          PIC 9(005).
             10 IDSV0001-LINGUA                      PIC X(002).
             10 IDSV0001-PAESE                       PIC X(003).
             10 IDSV0001-USER-NAME                   PIC X(020).
             10 IDSV0001-PROFILO-UTENTE              PIC X(020).
             10 IDSV0001-KEY-BUSINESS1               PIC X(020).
             10 IDSV0001-KEY-BUSINESS2               PIC X(020).
             10 IDSV0001-KEY-BUSINESS3               PIC X(020).
             10 IDSV0001-KEY-BUSINESS4               PIC X(020).
             10 IDSV0001-KEY-BUSINESS5               PIC X(020).
             10 IDSV0001-SESSIONE                    PIC X(020).
             10 IDSV0001-COD-MAIN-BATCH              PIC X(008).
             10 IDSV0001-TIPO-MOVIMENTO              PIC 9(005).
             10 IDSV0001-DATA-EFFETTO                PIC 9(008).
             10 IDSV0001-DATA-COMPETENZA             PIC 9(018).
             10 IDSV0001-DATA-COMP-AGG-STOR          PIC 9(018).

             10 IDSV0001-TRATTAMENTO-STORICITA       PIC X(003).
                88 IDSV0001-TRATT-X-EFFETTO          VALUE 'EFF'.
                88 IDSV0001-TRATT-X-COMPETENZA       VALUE 'CPZ'.

             10 IDSV0001-FORMATO-DATA-DB             PIC X(003).
                88 IDSV0001-DB-ISO                   VALUE 'ISO'.
                88 IDSV0001-DB-EUR                   VALUE 'EUR'.

             10 IDSV0001-TIPO-ENTE-CHIAMANTE         PIC 9(001).
                88 IDSV0001-AGENZIA                  VALUE 1.
                88 IDSV0001-SUB-AGENZIA              VALUE 2.
                88 IDSV0001-DIREZIONE                VALUE 3.
                88 IDSV0001-COLLOCATORE              VALUE 4.

             10 IDSV0001-LIVELLO-DEBUG               PIC 9(001).
                88 IDSV0001-NO-DEBUG                 VALUE 0.
                88 IDSV0001-DEBUG-BASSO              VALUE 1.
                88 IDSV0001-DEBUG-MEDIO              VALUE 2.
                88 IDSV0001-DEBUG-ELEVATO            VALUE 3.
                88 IDSV0001-DEBUG-ESASPERATO         VALUE 4.
                88 IDSV0001-ANY-APPL-DBG             VALUE 1,
                                                           2,
                                                           3,
                                                           4.

                88 IDSV0001-ARCH-BATCH-DBG           VALUE 5.
                88 IDSV0001-COM-COB-JAV-DBG          VALUE 6.
                88 IDSV0001-STRESS-TEST-DBG          VALUE 7.
                88 IDSV0001-BUSINESS-DBG             VALUE 8.
                88 IDSV0001-TOT-TUNING-DBG           VALUE 9.
                88 IDSV0001-ANY-TUNING-DBG           VALUE 5,
                                                           6,
                                                           7,
                                                           8,
                                                           9.

             10 IDSV0001-DT-PERV-RICH                PIC 9(008).
             10 IDSV0001-LETT-ULT-IMMAGINE           PIC X(001).
                88 IDSV0001-LETT-ULT-IMMAGINE-SI     VALUE 'S'.
                88 IDSV0001-LETT-ULT-IMMAGINE-NO     VALUE 'N'.

             10 IDSV0001-FLAG-ID-TMPRY-DATA          PIC X(001).
                88 IDSV0001-ID-TMPRY-DATA-SI         VALUE 'S'.
                88 IDSV0001-ID-TMPRY-DATA-NO         VALUE 'N'.
             10 IDSV0001-ID-TEMPORARY-DATA  PIC S9(10) COMP-3.

             10 IDSV0001-TEMPORARY-TABLE             PIC X(001).
                88 IDSV0001-NO-TEMP-TABLE            VALUE '0'.
                88 IDSV0001-STATIC-TEMP-TABLE        VALUE '1'.
                88 IDSV0001-SESSION-TEMP-TABLE       VALUE '2'.

             10 IDSV0001-AREA-ADDRESSES.
                15 IDSV0001-ADDRESSES             OCCURS 5 TIMES.
                   20 IDSV0001-ADDRESS-TYPE       PIC X(01).
                   20 IDSV0001-ADDRESS            POINTER.

             10 IDSV0001-FORZ-RC-04               PIC X(001).
                88 IDSV0001-FORZ-RC-04-YES        VALUE 'Y'.
                88 IDSV0001-FORZ-RC-04-NO         VALUE 'N'.

             10 FILLER                               PIC X(921).


          05 IDSV0001-AREA-ERRORI.
             10 IDSV0001-ESITO                       PIC X(002).
                 88 IDSV0001-ESITO-OK                VALUE 'OK'.
                 88 IDSV0001-ESITO-KO                VALUE 'KO'.
             10 IDSV0001-LOG-ERRORE.
                 15 IDSV0001-COD-SERVIZIO-BE         PIC X(008).
                 15 IDSV0001-LABEL-ERR               PIC X(030).
                 15 IDSV0001-DESC-ERRORE-ESTESA      PIC X(200).
                 15 IDSV0001-OPER-TABELLA            PIC X(002).
                 15 IDSV0001-NOME-TABELLA            PIC X(018).
                 15 IDSV0001-STATUS-TABELLA          PIC X(010).
                 15 IDSV0001-KEY-TABELLA             PIC X(020).

             10 IDSV0001-MAX-ELE-ERRORI              PIC S9(4) COMP.
             10 IDSV0001-TAB-ERRORI-FRONT-END.
                 15 IDSV0001-ELE-ERRORI   OCCURS 10.
                    20 IDSV0001-DESC-ERRORE          PIC X(200).
                    20 IDSV0001-COD-ERRORE           PIC 9(006).
                    20 IDSV0001-LIV-GRAVITA-BE       PIC 9(001).
                    20 IDSV0001-TIPO-TRATT-FE        PIC X(001).

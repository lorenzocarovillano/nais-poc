
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVTDR5
      *   ULTIMO AGG. 05 DIC 2014
      *------------------------------------------------------------

       VAL-DCLGEN-TDR.
           MOVE (SF)-TP-OGG(IX-TAB-TDR)
              TO TDR-TP-OGG
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TDR)
              TO TDR-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-TDR)
              TO TDR-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-TDR) NOT NUMERIC
              MOVE 0 TO TDR-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-TDR)
              TO TDR-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-TDR) NOT NUMERIC
              MOVE 0 TO TDR-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-TDR)
              TO TDR-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-TDR)
              TO TDR-COD-COMP-ANIA
           MOVE (SF)-TP-TIT(IX-TAB-TDR)
              TO TDR-TP-TIT
           IF (SF)-PROG-TIT-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-PROG-TIT-NULL(IX-TAB-TDR)
              TO TDR-PROG-TIT-NULL
           ELSE
              MOVE (SF)-PROG-TIT(IX-TAB-TDR)
              TO TDR-PROG-TIT
           END-IF
           MOVE (SF)-TP-PRE-TIT(IX-TAB-TDR)
              TO TDR-TP-PRE-TIT
           MOVE (SF)-TP-STAT-TIT(IX-TAB-TDR)
              TO TDR-TP-STAT-TIT
           IF (SF)-DT-INI-COP-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-DT-INI-COP-NULL(IX-TAB-TDR)
              TO TDR-DT-INI-COP-NULL
           ELSE
             IF (SF)-DT-INI-COP(IX-TAB-TDR) = ZERO
                MOVE HIGH-VALUES
                TO TDR-DT-INI-COP-NULL
             ELSE
              MOVE (SF)-DT-INI-COP(IX-TAB-TDR)
              TO TDR-DT-INI-COP
             END-IF
           END-IF
           IF (SF)-DT-END-COP-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-DT-END-COP-NULL(IX-TAB-TDR)
              TO TDR-DT-END-COP-NULL
           ELSE
             IF (SF)-DT-END-COP(IX-TAB-TDR) = ZERO
                MOVE HIGH-VALUES
                TO TDR-DT-END-COP-NULL
             ELSE
              MOVE (SF)-DT-END-COP(IX-TAB-TDR)
              TO TDR-DT-END-COP
             END-IF
           END-IF
           IF (SF)-IMP-PAG-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-IMP-PAG-NULL(IX-TAB-TDR)
              TO TDR-IMP-PAG-NULL
           ELSE
              MOVE (SF)-IMP-PAG(IX-TAB-TDR)
              TO TDR-IMP-PAG
           END-IF
           IF (SF)-FL-SOLL-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-FL-SOLL-NULL(IX-TAB-TDR)
              TO TDR-FL-SOLL-NULL
           ELSE
              MOVE (SF)-FL-SOLL(IX-TAB-TDR)
              TO TDR-FL-SOLL
           END-IF
           IF (SF)-FRAZ-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-FRAZ-NULL(IX-TAB-TDR)
              TO TDR-FRAZ-NULL
           ELSE
              MOVE (SF)-FRAZ(IX-TAB-TDR)
              TO TDR-FRAZ
           END-IF
           IF (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TDR)
              TO TDR-DT-APPLZ-MORA-NULL
           ELSE
             IF (SF)-DT-APPLZ-MORA(IX-TAB-TDR) = ZERO
                MOVE HIGH-VALUES
                TO TDR-DT-APPLZ-MORA-NULL
             ELSE
              MOVE (SF)-DT-APPLZ-MORA(IX-TAB-TDR)
              TO TDR-DT-APPLZ-MORA
             END-IF
           END-IF
           IF (SF)-FL-MORA-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-FL-MORA-NULL(IX-TAB-TDR)
              TO TDR-FL-MORA-NULL
           ELSE
              MOVE (SF)-FL-MORA(IX-TAB-TDR)
              TO TDR-FL-MORA
           END-IF
           IF (SF)-ID-RAPP-RETE-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-ID-RAPP-RETE-NULL(IX-TAB-TDR)
              TO TDR-ID-RAPP-RETE-NULL
           ELSE
              MOVE (SF)-ID-RAPP-RETE(IX-TAB-TDR)
              TO TDR-ID-RAPP-RETE
           END-IF
           IF (SF)-ID-RAPP-ANA-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-ID-RAPP-ANA-NULL(IX-TAB-TDR)
              TO TDR-ID-RAPP-ANA-NULL
           ELSE
              MOVE (SF)-ID-RAPP-ANA(IX-TAB-TDR)
              TO TDR-ID-RAPP-ANA
           END-IF
           IF (SF)-COD-DVS-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-COD-DVS-NULL(IX-TAB-TDR)
              TO TDR-COD-DVS-NULL
           ELSE
              MOVE (SF)-COD-DVS(IX-TAB-TDR)
              TO TDR-COD-DVS
           END-IF
           MOVE (SF)-DT-EMIS-TIT(IX-TAB-TDR)
              TO TDR-DT-EMIS-TIT
           IF (SF)-DT-ESI-TIT-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-DT-ESI-TIT-NULL(IX-TAB-TDR)
              TO TDR-DT-ESI-TIT-NULL
           ELSE
             IF (SF)-DT-ESI-TIT(IX-TAB-TDR) = ZERO
                MOVE HIGH-VALUES
                TO TDR-DT-ESI-TIT-NULL
             ELSE
              MOVE (SF)-DT-ESI-TIT(IX-TAB-TDR)
              TO TDR-DT-ESI-TIT
             END-IF
           END-IF
           IF (SF)-TOT-PRE-NET-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-PRE-NET-NULL(IX-TAB-TDR)
              TO TDR-TOT-PRE-NET-NULL
           ELSE
              MOVE (SF)-TOT-PRE-NET(IX-TAB-TDR)
              TO TDR-TOT-PRE-NET
           END-IF
           IF (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TDR)
              TO TDR-TOT-INTR-FRAZ-NULL
           ELSE
              MOVE (SF)-TOT-INTR-FRAZ(IX-TAB-TDR)
              TO TDR-TOT-INTR-FRAZ
           END-IF
           IF (SF)-TOT-INTR-MORA-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-INTR-MORA-NULL(IX-TAB-TDR)
              TO TDR-TOT-INTR-MORA-NULL
           ELSE
              MOVE (SF)-TOT-INTR-MORA(IX-TAB-TDR)
              TO TDR-TOT-INTR-MORA
           END-IF
           IF (SF)-TOT-INTR-PREST-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-INTR-PREST-NULL(IX-TAB-TDR)
              TO TDR-TOT-INTR-PREST-NULL
           ELSE
              MOVE (SF)-TOT-INTR-PREST(IX-TAB-TDR)
              TO TDR-TOT-INTR-PREST
           END-IF
           IF (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TDR)
              TO TDR-TOT-INTR-RETDT-NULL
           ELSE
              MOVE (SF)-TOT-INTR-RETDT(IX-TAB-TDR)
              TO TDR-TOT-INTR-RETDT
           END-IF
           IF (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TDR)
              TO TDR-TOT-INTR-RIAT-NULL
           ELSE
              MOVE (SF)-TOT-INTR-RIAT(IX-TAB-TDR)
              TO TDR-TOT-INTR-RIAT
           END-IF
           IF (SF)-TOT-DIR-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-DIR-NULL(IX-TAB-TDR)
              TO TDR-TOT-DIR-NULL
           ELSE
              MOVE (SF)-TOT-DIR(IX-TAB-TDR)
              TO TDR-TOT-DIR
           END-IF
           IF (SF)-TOT-SPE-MED-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-SPE-MED-NULL(IX-TAB-TDR)
              TO TDR-TOT-SPE-MED-NULL
           ELSE
              MOVE (SF)-TOT-SPE-MED(IX-TAB-TDR)
              TO TDR-TOT-SPE-MED
           END-IF
           IF (SF)-TOT-SPE-AGE-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-SPE-AGE-NULL(IX-TAB-TDR)
              TO TDR-TOT-SPE-AGE-NULL
           ELSE
              MOVE (SF)-TOT-SPE-AGE(IX-TAB-TDR)
              TO TDR-TOT-SPE-AGE
           END-IF
           IF (SF)-TOT-TAX-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-TAX-NULL(IX-TAB-TDR)
              TO TDR-TOT-TAX-NULL
           ELSE
              MOVE (SF)-TOT-TAX(IX-TAB-TDR)
              TO TDR-TOT-TAX
           END-IF
           IF (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TDR)
              TO TDR-TOT-SOPR-SAN-NULL
           ELSE
              MOVE (SF)-TOT-SOPR-SAN(IX-TAB-TDR)
              TO TDR-TOT-SOPR-SAN
           END-IF
           IF (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TDR)
              TO TDR-TOT-SOPR-TEC-NULL
           ELSE
              MOVE (SF)-TOT-SOPR-TEC(IX-TAB-TDR)
              TO TDR-TOT-SOPR-TEC
           END-IF
           IF (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TDR)
              TO TDR-TOT-SOPR-SPO-NULL
           ELSE
              MOVE (SF)-TOT-SOPR-SPO(IX-TAB-TDR)
              TO TDR-TOT-SOPR-SPO
           END-IF
           IF (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TDR)
              TO TDR-TOT-SOPR-PROF-NULL
           ELSE
              MOVE (SF)-TOT-SOPR-PROF(IX-TAB-TDR)
              TO TDR-TOT-SOPR-PROF
           END-IF
           IF (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TDR)
              TO TDR-TOT-SOPR-ALT-NULL
           ELSE
              MOVE (SF)-TOT-SOPR-ALT(IX-TAB-TDR)
              TO TDR-TOT-SOPR-ALT
           END-IF
           IF (SF)-TOT-PRE-TOT-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-PRE-TOT-NULL(IX-TAB-TDR)
              TO TDR-TOT-PRE-TOT-NULL
           ELSE
              MOVE (SF)-TOT-PRE-TOT(IX-TAB-TDR)
              TO TDR-TOT-PRE-TOT
           END-IF
           IF (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TDR)
              TO TDR-TOT-PRE-PP-IAS-NULL
           ELSE
              MOVE (SF)-TOT-PRE-PP-IAS(IX-TAB-TDR)
              TO TDR-TOT-PRE-PP-IAS
           END-IF
           IF (SF)-TOT-CAR-IAS-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-CAR-IAS-NULL(IX-TAB-TDR)
              TO TDR-TOT-CAR-IAS-NULL
           ELSE
              MOVE (SF)-TOT-CAR-IAS(IX-TAB-TDR)
              TO TDR-TOT-CAR-IAS
           END-IF
           IF (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TDR)
              TO TDR-TOT-PRE-SOLO-RSH-NULL
           ELSE
              MOVE (SF)-TOT-PRE-SOLO-RSH(IX-TAB-TDR)
              TO TDR-TOT-PRE-SOLO-RSH
           END-IF
           IF (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TDR)
              TO TDR-TOT-PROV-ACQ-1AA-NULL
           ELSE
              MOVE (SF)-TOT-PROV-ACQ-1AA(IX-TAB-TDR)
              TO TDR-TOT-PROV-ACQ-1AA
           END-IF
           IF (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TDR)
              TO TDR-TOT-PROV-ACQ-2AA-NULL
           ELSE
              MOVE (SF)-TOT-PROV-ACQ-2AA(IX-TAB-TDR)
              TO TDR-TOT-PROV-ACQ-2AA
           END-IF
           IF (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TDR)
              TO TDR-TOT-PROV-RICOR-NULL
           ELSE
              MOVE (SF)-TOT-PROV-RICOR(IX-TAB-TDR)
              TO TDR-TOT-PROV-RICOR
           END-IF
           IF (SF)-TOT-PROV-INC-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-PROV-INC-NULL(IX-TAB-TDR)
              TO TDR-TOT-PROV-INC-NULL
           ELSE
              MOVE (SF)-TOT-PROV-INC(IX-TAB-TDR)
              TO TDR-TOT-PROV-INC
           END-IF
           IF (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TDR)
              TO TDR-TOT-PROV-DA-REC-NULL
           ELSE
              MOVE (SF)-TOT-PROV-DA-REC(IX-TAB-TDR)
              TO TDR-TOT-PROV-DA-REC
           END-IF
           IF (SF)-IMP-AZ-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-IMP-AZ-NULL(IX-TAB-TDR)
              TO TDR-IMP-AZ-NULL
           ELSE
              MOVE (SF)-IMP-AZ(IX-TAB-TDR)
              TO TDR-IMP-AZ
           END-IF
           IF (SF)-IMP-ADER-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-IMP-ADER-NULL(IX-TAB-TDR)
              TO TDR-IMP-ADER-NULL
           ELSE
              MOVE (SF)-IMP-ADER(IX-TAB-TDR)
              TO TDR-IMP-ADER
           END-IF
           IF (SF)-IMP-TFR-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-IMP-TFR-NULL(IX-TAB-TDR)
              TO TDR-IMP-TFR-NULL
           ELSE
              MOVE (SF)-IMP-TFR(IX-TAB-TDR)
              TO TDR-IMP-TFR
           END-IF
           IF (SF)-IMP-VOLO-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-IMP-VOLO-NULL(IX-TAB-TDR)
              TO TDR-IMP-VOLO-NULL
           ELSE
              MOVE (SF)-IMP-VOLO(IX-TAB-TDR)
              TO TDR-IMP-VOLO
           END-IF
           IF (SF)-FL-VLDT-TIT-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-FL-VLDT-TIT-NULL(IX-TAB-TDR)
              TO TDR-FL-VLDT-TIT-NULL
           ELSE
              MOVE (SF)-FL-VLDT-TIT(IX-TAB-TDR)
              TO TDR-FL-VLDT-TIT
           END-IF
           IF (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TDR)
              TO TDR-TOT-CAR-ACQ-NULL
           ELSE
              MOVE (SF)-TOT-CAR-ACQ(IX-TAB-TDR)
              TO TDR-TOT-CAR-ACQ
           END-IF
           IF (SF)-TOT-CAR-GEST-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-CAR-GEST-NULL(IX-TAB-TDR)
              TO TDR-TOT-CAR-GEST-NULL
           ELSE
              MOVE (SF)-TOT-CAR-GEST(IX-TAB-TDR)
              TO TDR-TOT-CAR-GEST
           END-IF
           IF (SF)-TOT-CAR-INC-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-CAR-INC-NULL(IX-TAB-TDR)
              TO TDR-TOT-CAR-INC-NULL
           ELSE
              MOVE (SF)-TOT-CAR-INC(IX-TAB-TDR)
              TO TDR-TOT-CAR-INC
           END-IF
           IF (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TDR)
              TO TDR-TOT-MANFEE-ANTIC-NULL
           ELSE
              MOVE (SF)-TOT-MANFEE-ANTIC(IX-TAB-TDR)
              TO TDR-TOT-MANFEE-ANTIC
           END-IF
           IF (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TDR)
              TO TDR-TOT-MANFEE-RICOR-NULL
           ELSE
              MOVE (SF)-TOT-MANFEE-RICOR(IX-TAB-TDR)
              TO TDR-TOT-MANFEE-RICOR
           END-IF
           IF (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TDR)
              TO TDR-TOT-MANFEE-REC-NULL
           ELSE
              MOVE (SF)-TOT-MANFEE-REC(IX-TAB-TDR)
              TO TDR-TOT-MANFEE-REC
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-TDR) NOT NUMERIC
              MOVE 0 TO TDR-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-TDR)
              TO TDR-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-TDR)
              TO TDR-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-TDR) NOT NUMERIC
              MOVE 0 TO TDR-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-TDR)
              TO TDR-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-TDR) NOT NUMERIC
              MOVE 0 TO TDR-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-TDR)
              TO TDR-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-TDR) NOT NUMERIC
              MOVE 0 TO TDR-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-TDR)
              TO TDR-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-TDR)
              TO TDR-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-TDR)
              TO TDR-DS-STATO-ELAB
           IF (SF)-IMP-TRASFE-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-IMP-TRASFE-NULL(IX-TAB-TDR)
              TO TDR-IMP-TRASFE-NULL
           ELSE
              MOVE (SF)-IMP-TRASFE(IX-TAB-TDR)
              TO TDR-IMP-TRASFE
           END-IF
           IF (SF)-IMP-TFR-STRC-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-IMP-TFR-STRC-NULL(IX-TAB-TDR)
              TO TDR-IMP-TFR-STRC-NULL
           ELSE
              MOVE (SF)-IMP-TFR-STRC(IX-TAB-TDR)
              TO TDR-IMP-TFR-STRC
           END-IF
           IF (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TDR)
              TO TDR-TOT-ACQ-EXP-NULL
           ELSE
              MOVE (SF)-TOT-ACQ-EXP(IX-TAB-TDR)
              TO TDR-TOT-ACQ-EXP
           END-IF
           IF (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TDR)
              TO TDR-TOT-REMUN-ASS-NULL
           ELSE
              MOVE (SF)-TOT-REMUN-ASS(IX-TAB-TDR)
              TO TDR-TOT-REMUN-ASS
           END-IF
           IF (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TDR)
              TO TDR-TOT-COMMIS-INTER-NULL
           ELSE
              MOVE (SF)-TOT-COMMIS-INTER(IX-TAB-TDR)
              TO TDR-TOT-COMMIS-INTER
           END-IF
           IF (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TDR)
              TO TDR-TOT-CNBT-ANTIRAC-NULL
           ELSE
              MOVE (SF)-TOT-CNBT-ANTIRAC(IX-TAB-TDR)
              TO TDR-TOT-CNBT-ANTIRAC
           END-IF
           IF (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TDR) = HIGH-VALUES
              MOVE (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TDR)
              TO TDR-FL-INC-AUTOGEN-NULL
           ELSE
              MOVE (SF)-FL-INC-AUTOGEN(IX-TAB-TDR)
              TO TDR-FL-INC-AUTOGEN
           END-IF.
       VAL-DCLGEN-TDR-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY      ..... LCCVL116
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO OGGETTO BLOCCO
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-OGG-BLOCCO.

      *--  TABELLA NON STORICA
           INITIALIZE OGG-BLOCCO.

      *--> NOME TABELLA FISICA DB
           MOVE 'OGG-BLOCCO'               TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WL11-ST-INV

              EVALUATE TRUE
      *-->    INSERT
                WHEN WL11-ST-ADD
      *-->         ESTRAZIONE E VALORIZZAZIONE SEQUENCE

                   PERFORM ESTR-SEQUENCE
                      THRU ESTR-SEQUENCE-EX

                   IF IDSV0001-ESITO-OK
                      MOVE S090-SEQ-TABELLA   TO WL11-ID-PTF
                                                 L11-ID-OGG-BLOCCO
                                                 WL11-ID-OGG-BLOCCO

      *-->            TIPO OPERAZIONE DISPATCHER
                      SET  IDSI0011-TRATT-SENZA-STOR   TO TRUE
                      SET  IDSI0011-INSERT             TO TRUE
                      SET  IDSI0011-PRIMARY-KEY        TO TRUE


                   END-IF
      *-->    DELETE
                WHEN WL11-ST-DEL
                   MOVE WL11-ID-PTF
                     TO  L11-ID-OGG-BLOCCO


      *-->         TIPO OPERAZIONE DISPATCHER
                   SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->    UPDATE
                WHEN WL11-ST-MOD
                   MOVE WL11-ID-PTF
                     TO  L11-ID-OGG-BLOCCO


      *-->         TIPO OPERAZIONE DISPATCHER
                   SET  IDSI0011-UPDATE TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN MOVIMENTI BATCH SOSPESI
                 PERFORM VAL-DCLGEN-L11
                    THRU VAL-DCLGEN-L11-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-L11
                    THRU VALORIZZA-AREA-DSH-L11-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-OGG-BLOCCO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-L11.

      *--> DCLGEN TABELLA
           MOVE OGG-BLOCCO            TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
      *    SET  IDSI0011-PRIMARY-KEY    TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
      *    SET  IDSI0011-TRATT-SENZA-STOR           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-L11-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVL115.

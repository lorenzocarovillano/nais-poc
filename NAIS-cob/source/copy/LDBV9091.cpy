       01 LDBV9091.
           03 LDBV9091-DATI-INPUT.
              05 LDBV9091-ID-POLI               PIC S9(09) COMP-3.
              05 LDBV9091-ID-ADES               PIC S9(09) COMP-3.
              05 LDBV9091-TP-OGG                PIC  X(02).
              05 LDBV9091-TP-STAT-BUS-1         PIC  X(02).
              05 LDBV9091-TP-STAT-BUS-2         PIC  X(02).
              05 LDBV9091-RAMO1                 PIC  X(02).
              05 LDBV9091-RAMO2                 PIC  X(02).
              05 LDBV9091-RAMO3                 PIC  X(02).
              05 LDBV9091-RAMO4                 PIC  X(02).
              05 LDBV9091-RAMO5                 PIC  X(02).
              05 LDBV9091-RAMO6                 PIC  X(02).
              05 LDBV9091-TP-INVST1             PIC S9(02) COMP-3.
              05 LDBV9091-TP-INVST2             PIC S9(02) COMP-3.
              05 LDBV9091-TP-INVST3             PIC S9(02) COMP-3.
              05 LDBV9091-TP-INVST4             PIC S9(02) COMP-3.
              05 LDBV9091-TP-INVST5             PIC S9(02) COMP-3.
              05 LDBV9091-TP-INVST6             PIC S9(02) COMP-3.


           03 LDBV9091-GAR-OUTPUT.
              05 L9091-ID-GAR PIC S9(9)V     COMP-3.
              05 L9091-ID-ADES PIC S9(9)V     COMP-3.
              05 L9091-ID-ADES-NULL REDEFINES
                 L9091-ID-ADES   PIC X(5).
              05 L9091-ID-POLI PIC S9(9)V     COMP-3.
              05 L9091-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
              05 L9091-ID-MOVI-CHIU PIC S9(9)V     COMP-3.
              05 L9091-ID-MOVI-CHIU-NULL REDEFINES
                 L9091-ID-MOVI-CHIU   PIC X(5).
              05 L9091-DT-INI-EFF   PIC S9(8)V COMP-3.
              05 L9091-DT-END-EFF   PIC S9(8)V COMP-3.
              05 L9091-COD-COMP-ANIA PIC S9(5)V     COMP-3.
              05 L9091-IB-OGG PIC X(40).
              05 L9091-IB-OGG-NULL REDEFINES
                 L9091-IB-OGG   PIC X(40).
              05 L9091-DT-DECOR   PIC S9(8)V COMP-3.
              05 L9091-DT-DECOR-NULL REDEFINES
                 L9091-DT-DECOR   PIC X(5).
              05 L9091-DT-SCAD   PIC S9(8)V COMP-3.
              05 L9091-DT-SCAD-NULL REDEFINES
                 L9091-DT-SCAD   PIC X(5).
              05 L9091-COD-SEZ PIC X(12).
              05 L9091-COD-SEZ-NULL REDEFINES
                 L9091-COD-SEZ   PIC X(12).
              05 L9091-COD-TARI PIC X(12).
              05 L9091-RAMO-BILA PIC X(12).
              05 L9091-RAMO-BILA-NULL REDEFINES
                 L9091-RAMO-BILA   PIC X(12).
              05 L9091-DT-INI-VAL-TAR   PIC S9(8)V COMP-3.
              05 L9091-DT-INI-VAL-TAR-NULL REDEFINES
                 L9091-DT-INI-VAL-TAR   PIC X(5).
              05 L9091-ID-1O-ASSTO PIC S9(9)V     COMP-3.
              05 L9091-ID-1O-ASSTO-NULL REDEFINES
                 L9091-ID-1O-ASSTO   PIC X(5).
              05 L9091-ID-2O-ASSTO PIC S9(9)V     COMP-3.
              05 L9091-ID-2O-ASSTO-NULL REDEFINES
                 L9091-ID-2O-ASSTO   PIC X(5).
              05 L9091-ID-3O-ASSTO PIC S9(9)V     COMP-3.
              05 L9091-ID-3O-ASSTO-NULL REDEFINES
                 L9091-ID-3O-ASSTO   PIC X(5).
              05 L9091-TP-GAR PIC S9(2)V     COMP-3.
              05 L9091-TP-GAR-NULL REDEFINES
                 L9091-TP-GAR   PIC X(2).
              05 L9091-TP-RSH PIC X(2).
              05 L9091-TP-RSH-NULL REDEFINES
                 L9091-TP-RSH   PIC X(2).
              05 L9091-TP-INVST PIC S9(2)V     COMP-3.
              05 L9091-TP-INVST-NULL REDEFINES
                 L9091-TP-INVST   PIC X(2).
              05 L9091-MOD-PAG-GARCOL PIC X(2).
              05 L9091-MOD-PAG-GARCOL-NULL REDEFINES
                 L9091-MOD-PAG-GARCOL   PIC X(2).
              05 L9091-TP-PER-PRE PIC X(2).
              05 L9091-TP-PER-PRE-NULL REDEFINES
                 L9091-TP-PER-PRE   PIC X(2).
              05 L9091-ETA-AA-1O-ASSTO PIC S9(3)V     COMP-3.
              05 L9091-ETA-AA-1O-ASSTO-NULL REDEFINES
                 L9091-ETA-AA-1O-ASSTO   PIC X(2).
              05 L9091-ETA-MM-1O-ASSTO PIC S9(3)V     COMP-3.
              05 L9091-ETA-MM-1O-ASSTO-NULL REDEFINES
                 L9091-ETA-MM-1O-ASSTO   PIC X(2).
              05 L9091-ETA-AA-2O-ASSTO PIC S9(3)V     COMP-3.
              05 L9091-ETA-AA-2O-ASSTO-NULL REDEFINES
                 L9091-ETA-AA-2O-ASSTO   PIC X(2).
              05 L9091-ETA-MM-2O-ASSTO PIC S9(3)V     COMP-3.
              05 L9091-ETA-MM-2O-ASSTO-NULL REDEFINES
                 L9091-ETA-MM-2O-ASSTO   PIC X(2).
              05 L9091-ETA-AA-3O-ASSTO PIC S9(3)V     COMP-3.
              05 L9091-ETA-AA-3O-ASSTO-NULL REDEFINES
                 L9091-ETA-AA-3O-ASSTO   PIC X(2).
              05 L9091-ETA-MM-3O-ASSTO PIC S9(3)V     COMP-3.
              05 L9091-ETA-MM-3O-ASSTO-NULL REDEFINES
                 L9091-ETA-MM-3O-ASSTO   PIC X(2).
              05 L9091-TP-EMIS-PUR PIC X(1).
              05 L9091-TP-EMIS-PUR-NULL REDEFINES
                 L9091-TP-EMIS-PUR   PIC X(1).
              05 L9091-ETA-A-SCAD PIC S9(5)V     COMP-3.
              05 L9091-ETA-A-SCAD-NULL REDEFINES
                 L9091-ETA-A-SCAD   PIC X(3).
              05 L9091-TP-CALC-PRE-PRSTZ PIC X(2).
              05 L9091-TP-CALC-PRE-PRSTZ-NULL REDEFINES
                 L9091-TP-CALC-PRE-PRSTZ   PIC X(2).
              05 L9091-TP-PRE PIC X(1).
              05 L9091-TP-PRE-NULL REDEFINES
                 L9091-TP-PRE   PIC X(1).
              05 L9091-TP-DUR PIC X(2).
              05 L9091-TP-DUR-NULL REDEFINES
                 L9091-TP-DUR   PIC X(2).
              05 L9091-DUR-AA PIC S9(5)V     COMP-3.
              05 L9091-DUR-AA-NULL REDEFINES
                 L9091-DUR-AA   PIC X(3).
              05 L9091-DUR-MM PIC S9(5)V     COMP-3.
              05 L9091-DUR-MM-NULL REDEFINES
                 L9091-DUR-MM   PIC X(3).
              05 L9091-DUR-GG PIC S9(5)V     COMP-3.
              05 L9091-DUR-GG-NULL REDEFINES
                 L9091-DUR-GG   PIC X(3).
              05 L9091-NUM-AA-PAG-PRE PIC S9(5)V     COMP-3.
              05 L9091-NUM-AA-PAG-PRE-NULL REDEFINES
                 L9091-NUM-AA-PAG-PRE   PIC X(3).
              05 L9091-AA-PAG-PRE-UNI PIC S9(5)V     COMP-3.
              05 L9091-AA-PAG-PRE-UNI-NULL REDEFINES
                 L9091-AA-PAG-PRE-UNI   PIC X(3).
              05 L9091-MM-PAG-PRE-UNI PIC S9(5)V     COMP-3.
              05 L9091-MM-PAG-PRE-UNI-NULL REDEFINES
                 L9091-MM-PAG-PRE-UNI   PIC X(3).
              05 L9091-FRAZ-INI-EROG-REN PIC S9(5)V     COMP-3.
              05 L9091-FRAZ-INI-EROG-REN-NULL REDEFINES
                 L9091-FRAZ-INI-EROG-REN   PIC X(3).
              05 L9091-MM-1O-RAT PIC S9(2)V     COMP-3.
              05 L9091-MM-1O-RAT-NULL REDEFINES
                 L9091-MM-1O-RAT   PIC X(2).
              05 L9091-PC-1O-RAT PIC S9(3)V9(3) COMP-3.
              05 L9091-PC-1O-RAT-NULL REDEFINES
                 L9091-PC-1O-RAT   PIC X(4).
              05 L9091-TP-PRSTZ-ASSTA PIC X(2).
              05 L9091-TP-PRSTZ-ASSTA-NULL REDEFINES
                 L9091-TP-PRSTZ-ASSTA   PIC X(2).
              05 L9091-DT-END-CARZ   PIC S9(8)V COMP-3.
              05 L9091-DT-END-CARZ-NULL REDEFINES
                 L9091-DT-END-CARZ   PIC X(5).
              05 L9091-PC-RIP-PRE PIC S9(3)V9(3) COMP-3.
              05 L9091-PC-RIP-PRE-NULL REDEFINES
                 L9091-PC-RIP-PRE   PIC X(4).
              05 L9091-COD-FND PIC X(12).
              05 L9091-COD-FND-NULL REDEFINES
                 L9091-COD-FND   PIC X(12).
              05 L9091-AA-REN-CER PIC X(18).
              05 L9091-AA-REN-CER-NULL REDEFINES
                 L9091-AA-REN-CER   PIC X(18).
              05 L9091-PC-REVRSB PIC S9(3)V9(3) COMP-3.
              05 L9091-PC-REVRSB-NULL REDEFINES
                 L9091-PC-REVRSB   PIC X(4).
              05 L9091-TP-PC-RIP PIC X(2).
              05 L9091-TP-PC-RIP-NULL REDEFINES
                 L9091-TP-PC-RIP   PIC X(2).
              05 L9091-PC-OPZ PIC S9(3)V9(3) COMP-3.
              05 L9091-PC-OPZ-NULL REDEFINES
                 L9091-PC-OPZ   PIC X(4).
              05 L9091-TP-IAS PIC X(2).
              05 L9091-TP-IAS-NULL REDEFINES
                 L9091-TP-IAS   PIC X(2).
              05 L9091-TP-STAB PIC X(2).
              05 L9091-TP-STAB-NULL REDEFINES
                 L9091-TP-STAB   PIC X(2).
              05 L9091-TP-ADEG-PRE PIC X(1).
              05 L9091-TP-ADEG-PRE-NULL REDEFINES
                 L9091-TP-ADEG-PRE   PIC X(1).
              05 L9091-DT-VARZ-TP-IAS   PIC S9(8)V COMP-3.
              05 L9091-DT-VARZ-TP-IAS-NULL REDEFINES
                 L9091-DT-VARZ-TP-IAS   PIC X(5).
              05 L9091-FRAZ-DECR-CPT PIC S9(5)V     COMP-3.
              05 L9091-FRAZ-DECR-CPT-NULL REDEFINES
                 L9091-FRAZ-DECR-CPT   PIC X(3).
              05 L9091-COD-TRAT-RIASS PIC X(12).
              05 L9091-COD-TRAT-RIASS-NULL REDEFINES
                 L9091-COD-TRAT-RIASS   PIC X(12).
              05 L9091-TP-DT-EMIS-RIASS PIC X(2).
              05 L9091-TP-DT-EMIS-RIASS-NULL REDEFINES
                 L9091-TP-DT-EMIS-RIASS   PIC X(2).
              05 L9091-TP-CESS-RIASS PIC X(2).
              05 L9091-TP-CESS-RIASS-NULL REDEFINES
                 L9091-TP-CESS-RIASS   PIC X(2).
              05 L9091-DS-RIGA PIC S9(10)V     COMP-3.
              05 L9091-DS-OPER-SQL PIC X(1).
              05 L9091-DS-VER PIC S9(9)V     COMP-3.
              05 L9091-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
              05 L9091-DS-TS-END-CPTZ PIC S9(18)V     COMP-3.
              05 L9091-DS-UTENTE PIC X(20).
              05 L9091-DS-STATO-ELAB PIC X(1).
              05 L9091-AA-STAB PIC S9(5)V     COMP-3.
              05 L9091-AA-STAB-NULL REDEFINES
                 L9091-AA-STAB   PIC X(3).
              05 L9091-TS-STAB-LIMITATA PIC S9(5)V9(9) COMP-3.
              05 L9091-TS-STAB-LIMITATA-NULL REDEFINES
                 L9091-TS-STAB-LIMITATA   PIC X(8).
              05 L9091-DT-PRESC   PIC S9(8)V COMP-3.
              05 L9091-DT-PRESC-NULL REDEFINES
                 L9091-DT-PRESC   PIC X(5).
              05 L9091-RSH-INVST PIC X(1).
              05 L9091-RSH-INVST-NULL REDEFINES
                 L9091-RSH-INVST   PIC X(1).
              05 L9091-TP-RAMO-BILA PIC X(2).

           03 LDBV9091-STB-OUTPUT.
              05 L9091-TP-STAT-BUS PIC X(2).
              05 L9091-TP-CAUS     PIC X(2).

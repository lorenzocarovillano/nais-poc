       01  LDBVB471.
           05 LDBVB471-TP-TIT-01              PIC X(02).
           05 LDBVB471-TP-TIT-02              PIC X(02).
           05 LDBVB471-TP-STAT-TIT-1          PIC X(02).
           05 LDBVB471-TP-STAT-TIT-2          PIC X(02).
           05 LDBVB471-TP-STAT-TIT-3          PIC X(02).
           05 LDBVB471-DT-MAX-DB              PIC X(10).
           05 LDBVB471-DT-MAX                 PIC S9(08) COMP-3.
           05 LDBVB471-TOT-INT-PRE            PIC S9(12)V9(3) COMP-3.
           05 LDBVB471-TOT-INT-PRE-NULL REDEFINES
              LDBVB471-TOT-INT-PRE            PIC X(8).
           05 LDBVB471-ID-OGG                 PIC S9(9) COMP-3.
           05 LDBVB471-TP-OGG                 PIC X(02).

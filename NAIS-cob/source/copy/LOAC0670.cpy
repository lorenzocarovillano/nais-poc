      *----------------------------------------------------------------*
      *   PROCESSO DI POST-VENDITA - PORTAFOGLIO VITA
      *   AREA VARIABILI E COSTANTI DI INPUT
      *   SERVIZIO DI RIVALUTAZIONE
      *----------------------------------------------------------------*
           05 (SF)-AREA-670-VAR-INP.
              07 (SF)-DATI-GGINC.
                 10 (SF)-DATI-GGINCASSO-CONTESTO PIC X(02).
                    88 (SF)-GGCONTESTO-CONT-SI         VALUE 'SI'.
                    88 (SF)-GGCONTESTO-CONT-NO         VALUE 'NO'.
                 10 (SF)-GGINCASSO            PIC 9(005).
              07 (SF)-AREA-TITOLO.
                 10 (SF)-NUM-MAX-ELE               PIC S9(4).
                 10 (SF)-DATI-TIT   OCCURS 1250.
                    15 (SF)-ID-TRCH-DI-GAR         PIC S9(9) COMP-3.
                    15 (SF)-ID-TIT-CONT            PIC S9(9) COMP-3.
                    15 (SF)-DT-VLT                 PIC S9(8) COMP-3.
                    15 (SF)-DT-VLT-NULL REDEFINES
                                     (SF)-DT-VLT   PIC X(5).
                    15 (SF)-DT-INI-EFF             PIC S9(8) COMP-3.
                    15 (SF)-TP-STAT-TIT            PIC X(2).


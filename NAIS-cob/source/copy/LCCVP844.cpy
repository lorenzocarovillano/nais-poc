
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVP844
      *   ULTIMO AGG. 26 SET 2014
      *------------------------------------------------------------

       INIZIA-TOT-P84.

           PERFORM INIZIA-ZEROES-P84 THRU INIZIA-ZEROES-P84-EX

           PERFORM INIZIA-SPACES-P84 THRU INIZIA-SPACES-P84-EX

           PERFORM INIZIA-NULL-P84 THRU INIZIA-NULL-P84-EX.

       INIZIA-TOT-P84-EX.
           EXIT.

       INIZIA-NULL-P84.
           MOVE HIGH-VALUES TO (SF)-IMP-LRD-CEDOLE-LIQ-NULL(IX-TAB-P84).
       INIZIA-NULL-P84-EX.
           EXIT.

       INIZIA-ZEROES-P84.
           MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-P84)
           MOVE 0 TO (SF)-ID-POLI(IX-TAB-P84)
           MOVE 0 TO (SF)-DT-LIQ-CED(IX-TAB-P84)
           MOVE 0 TO (SF)-DS-VER(IX-TAB-P84)
           MOVE 0 TO (SF)-DS-TS-CPTZ(IX-TAB-P84).
       INIZIA-ZEROES-P84-EX.
           EXIT.

       INIZIA-SPACES-P84.
           MOVE SPACES TO (SF)-COD-TARI(IX-TAB-P84)
           MOVE SPACES TO (SF)-IB-POLI(IX-TAB-P84)
           MOVE SPACES TO (SF)-DS-OPER-SQL(IX-TAB-P84)
           MOVE SPACES TO (SF)-DS-UTENTE(IX-TAB-P84)
           MOVE SPACES TO (SF)-DS-STATO-ELAB(IX-TAB-P84).
       INIZIA-SPACES-P84-EX.
           EXIT.

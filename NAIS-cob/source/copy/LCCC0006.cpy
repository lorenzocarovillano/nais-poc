      *----------------------------------------------------------------*
      *   PORTAFOGLIO VITA - PROCESSO VENDITA
      *   ULTIMO AGG.  12/11/2009
      *----------------------------------------------------------------*

      *----------------------------------------------------------------*
      *    TP_POLI
      *----------------------------------------------------------------*
       01  WS-TP-POLI                       PIC X(002) VALUE SPACES.
           88 TP-COLL-TFR                              VALUE '01'.
           88 TP-COLL-PREV                             VALUE '02'.
           88 TP-COLL-GRUPPO                           VALUE '03'.
           88 TP-COLL-TFM                              VALUE '04'.
           88 TP-COLL-FP-APERTO                        VALUE '05'.
           88 TP-COLL-FP-CHIUSO                        VALUE '06'.
           88 TP-COLL-FP-COMP                          VALUE '07'.
           88 TP-IND-FIP-PIP                           VALUE '08'.
           88 TP-IND-NO-FIP-PIP                        VALUE '09'.
      *----------------------------------------------------------------*
      *    TP_GAR (ACTUATOR)
      *----------------------------------------------------------------*
       01  ACT-TP-GARANZIA                  PIC 9(001) VALUE ZERO.
           88 TP-GAR-BASE                              VALUE 1.
           88 TP-GAR-COMPLEM                           VALUE 2.
           88 TP-GAR-OPZIONE                           VALUE 3.
           88 TP-GAR-ACCESSORIA                        VALUE 4.
           88 TP-GAR-DIFFERIMENTO                      VALUE 5.
           88 TP-GAR-CEDOLA                            VALUE 6.
      *----------------------------------------------------------------*
      *    ADE-FL-ATTIV
      *----------------------------------------------------------------*
      *    N => Attivazione Non Contestuale - Numerazione Automatica
      *    O => Attivazione Non Contestuale - Numerazione Manuale
      *    S => Attivazione Contestuale - Numerazione Automatica
      *    T => Attivazione Contestuale - Numerazione Manuale
      *----------------------------------------------------------------*
       01  WS-ADE-FL-ATTIV                  PIC X(001) VALUE SPACES.
           88 WS-ATTIV-NON-CONT-AUT                    VALUE 'N'.
           88 WS-ATTIV-NON-CONT-MAN                    VALUE 'O'.
           88 WS-ATTIV-CONT-AUT                        VALUE 'S'.
           88 WS-ATTIV-CONT-MAN                        VALUE 'T'.

      *----------------------------------------------------------------*
      *    FLAG-ATTIVAZIONE
      *----------------------------------------------------------------*
       01  WS-FLAG-ATTIVAZIONE              PIC X(001) VALUE SPACES.
           88 WS-ATTIV-CONT                            VALUE 'S'.
           88 WS-ATTIV-NON-CONT                        VALUE 'N'.

      *----------------------------------------------------------------*
      *    FLAG-NUMERAZIONE
      *----------------------------------------------------------------*
       01  WS-FLAG-NUMERAZIONE              PIC X(001) VALUE SPACES.
           88 WS-NUM-AUT                               VALUE 'A'.
           88 WS-NUM-MAN                               VALUE 'M'.

      *----------------------------------------------------------------*
      *    TP_INVST (ACTUATOR)
      *----------------------------------------------------------------*
       01  WS-TP-INVST                      PIC 9(002) VALUE ZERO.
           88 WS-COSTANTI                              VALUE 1.
           88 WS-DECRESCENTI                           VALUE 2.
           88 WS-LEGGE-FISSA                           VALUE 3.
           88 WS-RIVALUTAZIONE                         VALUE 4.
           88 WS-INDICIZZAZIONE                        VALUE 5.
           88 WS-ESPANSIONE-RIVAL                      VALUE 6.
           88 WS-UNIT-LINKED                           VALUE 7.
           88 WS-INDEX-LINKED                          VALUE 8.
           88 WS-SPECIFICA-PROVVISTA                   VALUE 9.
           88 WS-PARTECIPAZ-UTILI                      VALUE 10.

      *----------------------------------------------------------------*
      *    Dominio dei trattamenti utilizzati dal PTF e ACT
      *----------------------------------------------------------------*
       01  LCCC006-TRATT-DATI               PIC X(001).
           88 LCCC006-DATO-AMMESSO-MOD                 VALUE '1'.
           88 LCCC006-DATO-AMMESSO-NMOD                VALUE '2'.
           88 LCCC006-DATO-NON-AMMESSO                 VALUE '3'.
           88 LCCC006-DATO-OBBLIGATORIO                VALUE '4'.

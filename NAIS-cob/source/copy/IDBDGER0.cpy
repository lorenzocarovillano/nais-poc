           EXEC SQL DECLARE GRAVITA_ERRORE TABLE
           (
             ID_GRAVITA_ERRORE   DECIMAL(9, 0) NOT NULL,
             COD_COMPAGNIA_ANIA  DECIMAL(5, 0),
             COD_CONV_MAIN_BTC   CHAR(12),
             COD_PAGINA_SERV_BE  CHAR(12),
             KEY_FILTRO2         CHAR(20),
             KEY_FILTRO1         CHAR(20),
             KEY_FILTRO3         CHAR(20),
             KEY_FILTRO4         CHAR(20),
             KEY_FILTRO5         CHAR(20),
             LIVELLO_GRAVITA     DECIMAL(1, 0) NOT NULL,
             TIPO_TRATT_FE       CHAR(1) NOT NULL,
             COD_ERRORE          INTEGER NOT NULL
          ) END-EXEC.

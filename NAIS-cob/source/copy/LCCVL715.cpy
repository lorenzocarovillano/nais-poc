
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVL715
      *   ULTIMO AGG. 22 APR 2010
      *------------------------------------------------------------

       VAL-DCLGEN-L71.
           MOVE (SF)-ID-MATR-ELAB-BATCH
              TO L71-ID-MATR-ELAB-BATCH
           MOVE (SF)-COD-COMP-ANIA
              TO L71-COD-COMP-ANIA
           MOVE (SF)-TP-FRM-ASSVA
              TO L71-TP-FRM-ASSVA
           MOVE (SF)-TP-MOVI
              TO L71-TP-MOVI
           IF (SF)-COD-RAMO-NULL = HIGH-VALUES
              MOVE (SF)-COD-RAMO-NULL
              TO L71-COD-RAMO-NULL
           ELSE
              MOVE (SF)-COD-RAMO
              TO L71-COD-RAMO
           END-IF
           MOVE (SF)-DT-ULT-ELAB
              TO L71-DT-ULT-ELAB
           MOVE (SF)-DS-OPER-SQL
              TO L71-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO L71-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO L71-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO L71-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO L71-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO L71-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO L71-DS-STATO-ELAB.
       VAL-DCLGEN-L71-EX.
           EXIT.

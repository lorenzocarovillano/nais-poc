      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA PARAM_COMP
      *   ALIAS PCO
      *   ULTIMO AGG. 13 NOV 2018
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-DATI.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-TRAT-CIRT PIC X(20).
             07 (SF)-COD-TRAT-CIRT-NULL REDEFINES
                (SF)-COD-TRAT-CIRT   PIC X(20).
             07 (SF)-LIM-VLTR PIC S9(12)V9(3) COMP-3.
             07 (SF)-LIM-VLTR-NULL REDEFINES
                (SF)-LIM-VLTR   PIC X(8).
             07 (SF)-TP-RAT-PERF PIC X(1).
             07 (SF)-TP-RAT-PERF-NULL REDEFINES
                (SF)-TP-RAT-PERF   PIC X(1).
             07 (SF)-TP-LIV-GENZ-TIT PIC X(2).
             07 (SF)-ARROT-PRE PIC S9(12)V9(3) COMP-3.
             07 (SF)-ARROT-PRE-NULL REDEFINES
                (SF)-ARROT-PRE   PIC X(8).
             07 (SF)-DT-CONT   PIC S9(8) COMP-3.
             07 (SF)-DT-CONT-NULL REDEFINES
                (SF)-DT-CONT   PIC X(5).
             07 (SF)-DT-ULT-RIVAL-IN   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-RIVAL-IN-NULL REDEFINES
                (SF)-DT-ULT-RIVAL-IN   PIC X(5).
             07 (SF)-DT-ULT-QTZO-IN   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-QTZO-IN-NULL REDEFINES
                (SF)-DT-ULT-QTZO-IN   PIC X(5).
             07 (SF)-DT-ULT-RICL-RIASS   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-RICL-RIASS-NULL REDEFINES
                (SF)-DT-ULT-RICL-RIASS   PIC X(5).
             07 (SF)-DT-ULT-TABUL-RIASS   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-TABUL-RIASS-NULL REDEFINES
                (SF)-DT-ULT-TABUL-RIASS   PIC X(5).
             07 (SF)-DT-ULT-BOLL-EMES   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-EMES-NULL REDEFINES
                (SF)-DT-ULT-BOLL-EMES   PIC X(5).
             07 (SF)-DT-ULT-BOLL-STOR   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-STOR-NULL REDEFINES
                (SF)-DT-ULT-BOLL-STOR   PIC X(5).
             07 (SF)-DT-ULT-BOLL-LIQ   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-LIQ-NULL REDEFINES
                (SF)-DT-ULT-BOLL-LIQ   PIC X(5).
             07 (SF)-DT-ULT-BOLL-RIAT   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-RIAT-NULL REDEFINES
                (SF)-DT-ULT-BOLL-RIAT   PIC X(5).
             07 (SF)-DT-ULTELRISCPAR-PR   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTELRISCPAR-PR-NULL REDEFINES
                (SF)-DT-ULTELRISCPAR-PR   PIC X(5).
             07 (SF)-DT-ULTC-IS-IN   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTC-IS-IN-NULL REDEFINES
                (SF)-DT-ULTC-IS-IN   PIC X(5).
             07 (SF)-DT-ULT-RICL-PRE   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-RICL-PRE-NULL REDEFINES
                (SF)-DT-ULT-RICL-PRE   PIC X(5).
             07 (SF)-DT-ULTC-MARSOL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTC-MARSOL-NULL REDEFINES
                (SF)-DT-ULTC-MARSOL   PIC X(5).
             07 (SF)-DT-ULTC-RB-IN   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTC-RB-IN-NULL REDEFINES
                (SF)-DT-ULTC-RB-IN   PIC X(5).
             07 (SF)-PC-PROV-1AA-ACQ PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-PROV-1AA-ACQ-NULL REDEFINES
                (SF)-PC-PROV-1AA-ACQ   PIC X(4).
             07 (SF)-MOD-INTR-PREST PIC X(2).
             07 (SF)-MOD-INTR-PREST-NULL REDEFINES
                (SF)-MOD-INTR-PREST   PIC X(2).
             07 (SF)-GG-MAX-REC-PROV PIC S9(4)     COMP-3.
             07 (SF)-GG-MAX-REC-PROV-NULL REDEFINES
                (SF)-GG-MAX-REC-PROV   PIC X(3).
             07 (SF)-DT-ULTGZ-TRCH-E-IN   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTGZ-TRCH-E-IN-NULL REDEFINES
                (SF)-DT-ULTGZ-TRCH-E-IN   PIC X(5).
             07 (SF)-DT-ULT-BOLL-SNDEN   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-SNDEN-NULL REDEFINES
                (SF)-DT-ULT-BOLL-SNDEN   PIC X(5).
             07 (SF)-DT-ULT-BOLL-SNDNLQ   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-SNDNLQ-NULL REDEFINES
                (SF)-DT-ULT-BOLL-SNDNLQ   PIC X(5).
             07 (SF)-DT-ULTSC-ELAB-IN   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTSC-ELAB-IN-NULL REDEFINES
                (SF)-DT-ULTSC-ELAB-IN   PIC X(5).
             07 (SF)-DT-ULTSC-OPZ-IN   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTSC-OPZ-IN-NULL REDEFINES
                (SF)-DT-ULTSC-OPZ-IN   PIC X(5).
             07 (SF)-DT-ULTC-BNSRIC-IN   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTC-BNSRIC-IN-NULL REDEFINES
                (SF)-DT-ULTC-BNSRIC-IN   PIC X(5).
             07 (SF)-DT-ULTC-BNSFDT-IN   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTC-BNSFDT-IN-NULL REDEFINES
                (SF)-DT-ULTC-BNSFDT-IN   PIC X(5).
             07 (SF)-DT-ULT-RINN-GARAC   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-RINN-GARAC-NULL REDEFINES
                (SF)-DT-ULT-RINN-GARAC   PIC X(5).
             07 (SF)-DT-ULTGZ-CED   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTGZ-CED-NULL REDEFINES
                (SF)-DT-ULTGZ-CED   PIC X(5).
             07 (SF)-DT-ULT-ELAB-PRLCOS   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ELAB-PRLCOS-NULL REDEFINES
                (SF)-DT-ULT-ELAB-PRLCOS   PIC X(5).
             07 (SF)-DT-ULT-RINN-COLL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-RINN-COLL-NULL REDEFINES
                (SF)-DT-ULT-RINN-COLL   PIC X(5).
             07 (SF)-FL-RVC-PERF PIC X(1).
             07 (SF)-FL-RVC-PERF-NULL REDEFINES
                (SF)-FL-RVC-PERF   PIC X(1).
             07 (SF)-FL-RCS-POLI-NOPERF PIC X(1).
             07 (SF)-FL-RCS-POLI-NOPERF-NULL REDEFINES
                (SF)-FL-RCS-POLI-NOPERF   PIC X(1).
             07 (SF)-FL-GEST-PLUSV PIC X(1).
             07 (SF)-FL-GEST-PLUSV-NULL REDEFINES
                (SF)-FL-GEST-PLUSV   PIC X(1).
             07 (SF)-DT-ULT-RIVAL-CL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-RIVAL-CL-NULL REDEFINES
                (SF)-DT-ULT-RIVAL-CL   PIC X(5).
             07 (SF)-DT-ULT-QTZO-CL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-QTZO-CL-NULL REDEFINES
                (SF)-DT-ULT-QTZO-CL   PIC X(5).
             07 (SF)-DT-ULTC-BNSRIC-CL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTC-BNSRIC-CL-NULL REDEFINES
                (SF)-DT-ULTC-BNSRIC-CL   PIC X(5).
             07 (SF)-DT-ULTC-BNSFDT-CL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTC-BNSFDT-CL-NULL REDEFINES
                (SF)-DT-ULTC-BNSFDT-CL   PIC X(5).
             07 (SF)-DT-ULTC-IS-CL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTC-IS-CL-NULL REDEFINES
                (SF)-DT-ULTC-IS-CL   PIC X(5).
             07 (SF)-DT-ULTC-RB-CL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTC-RB-CL-NULL REDEFINES
                (SF)-DT-ULTC-RB-CL   PIC X(5).
             07 (SF)-DT-ULTGZ-TRCH-E-CL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTGZ-TRCH-E-CL-NULL REDEFINES
                (SF)-DT-ULTGZ-TRCH-E-CL   PIC X(5).
             07 (SF)-DT-ULTSC-ELAB-CL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTSC-ELAB-CL-NULL REDEFINES
                (SF)-DT-ULTSC-ELAB-CL   PIC X(5).
             07 (SF)-DT-ULTSC-OPZ-CL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTSC-OPZ-CL-NULL REDEFINES
                (SF)-DT-ULTSC-OPZ-CL   PIC X(5).
             07 (SF)-STST-X-REGIONE   PIC S9(8) COMP-3.
             07 (SF)-STST-X-REGIONE-NULL REDEFINES
                (SF)-STST-X-REGIONE   PIC X(5).
             07 (SF)-DT-ULTGZ-CED-COLL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTGZ-CED-COLL-NULL REDEFINES
                (SF)-DT-ULTGZ-CED-COLL   PIC X(5).
             07 (SF)-TP-MOD-RIVAL PIC X(2).
             07 (SF)-NUM-MM-CALC-MORA PIC S9(5)     COMP-3.
             07 (SF)-NUM-MM-CALC-MORA-NULL REDEFINES
                (SF)-NUM-MM-CALC-MORA   PIC X(3).
             07 (SF)-DT-ULT-EC-RIV-COLL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-EC-RIV-COLL-NULL REDEFINES
                (SF)-DT-ULT-EC-RIV-COLL   PIC X(5).
             07 (SF)-DT-ULT-EC-RIV-IND   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-EC-RIV-IND-NULL REDEFINES
                (SF)-DT-ULT-EC-RIV-IND   PIC X(5).
             07 (SF)-DT-ULT-EC-IL-COLL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-EC-IL-COLL-NULL REDEFINES
                (SF)-DT-ULT-EC-IL-COLL   PIC X(5).
             07 (SF)-DT-ULT-EC-IL-IND   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-EC-IL-IND-NULL REDEFINES
                (SF)-DT-ULT-EC-IL-IND   PIC X(5).
             07 (SF)-DT-ULT-EC-UL-COLL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-EC-UL-COLL-NULL REDEFINES
                (SF)-DT-ULT-EC-UL-COLL   PIC X(5).
             07 (SF)-DT-ULT-EC-UL-IND   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-EC-UL-IND-NULL REDEFINES
                (SF)-DT-ULT-EC-UL-IND   PIC X(5).
             07 (SF)-AA-UTI PIC S9(4)     COMP-3.
             07 (SF)-AA-UTI-NULL REDEFINES
                (SF)-AA-UTI   PIC X(3).
             07 (SF)-CALC-RSH-COMUN PIC X(1).
             07 (SF)-FL-LIV-DEBUG PIC S9(1)     COMP-3.
             07 (SF)-DT-ULT-BOLL-PERF-C   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-PERF-C-NULL REDEFINES
                (SF)-DT-ULT-BOLL-PERF-C   PIC X(5).
             07 (SF)-DT-ULT-BOLL-RSP-IN   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-RSP-IN-NULL REDEFINES
                (SF)-DT-ULT-BOLL-RSP-IN   PIC X(5).
             07 (SF)-DT-ULT-BOLL-RSP-CL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-RSP-CL-NULL REDEFINES
                (SF)-DT-ULT-BOLL-RSP-CL   PIC X(5).
             07 (SF)-DT-ULT-BOLL-EMES-I   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-EMES-I-NULL REDEFINES
                (SF)-DT-ULT-BOLL-EMES-I   PIC X(5).
             07 (SF)-DT-ULT-BOLL-STOR-I   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-STOR-I-NULL REDEFINES
                (SF)-DT-ULT-BOLL-STOR-I   PIC X(5).
             07 (SF)-DT-ULT-BOLL-RIAT-I   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-RIAT-I-NULL REDEFINES
                (SF)-DT-ULT-BOLL-RIAT-I   PIC X(5).
             07 (SF)-DT-ULT-BOLL-SD-I   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-SD-I-NULL REDEFINES
                (SF)-DT-ULT-BOLL-SD-I   PIC X(5).
             07 (SF)-DT-ULT-BOLL-SDNL-I   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-SDNL-I-NULL REDEFINES
                (SF)-DT-ULT-BOLL-SDNL-I   PIC X(5).
             07 (SF)-DT-ULT-BOLL-PERF-I   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-PERF-I-NULL REDEFINES
                (SF)-DT-ULT-BOLL-PERF-I   PIC X(5).
             07 (SF)-DT-RICL-RIRIAS-COM   PIC S9(8) COMP-3.
             07 (SF)-DT-RICL-RIRIAS-COM-NULL REDEFINES
                (SF)-DT-RICL-RIRIAS-COM   PIC X(5).
             07 (SF)-DT-ULT-ELAB-AT92-C   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ELAB-AT92-C-NULL REDEFINES
                (SF)-DT-ULT-ELAB-AT92-C   PIC X(5).
             07 (SF)-DT-ULT-ELAB-AT92-I   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ELAB-AT92-I-NULL REDEFINES
                (SF)-DT-ULT-ELAB-AT92-I   PIC X(5).
             07 (SF)-DT-ULT-ELAB-AT93-C   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ELAB-AT93-C-NULL REDEFINES
                (SF)-DT-ULT-ELAB-AT93-C   PIC X(5).
             07 (SF)-DT-ULT-ELAB-AT93-I   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ELAB-AT93-I-NULL REDEFINES
                (SF)-DT-ULT-ELAB-AT93-I   PIC X(5).
             07 (SF)-DT-ULT-ELAB-SPE-IN   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ELAB-SPE-IN-NULL REDEFINES
                (SF)-DT-ULT-ELAB-SPE-IN   PIC X(5).
             07 (SF)-DT-ULT-ELAB-PR-CON   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ELAB-PR-CON-NULL REDEFINES
                (SF)-DT-ULT-ELAB-PR-CON   PIC X(5).
             07 (SF)-DT-ULT-BOLL-RP-CL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-RP-CL-NULL REDEFINES
                (SF)-DT-ULT-BOLL-RP-CL   PIC X(5).
             07 (SF)-DT-ULT-BOLL-RP-IN   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-RP-IN-NULL REDEFINES
                (SF)-DT-ULT-BOLL-RP-IN   PIC X(5).
             07 (SF)-DT-ULT-BOLL-PRE-I   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-PRE-I-NULL REDEFINES
                (SF)-DT-ULT-BOLL-PRE-I   PIC X(5).
             07 (SF)-DT-ULT-BOLL-PRE-C   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-PRE-C-NULL REDEFINES
                (SF)-DT-ULT-BOLL-PRE-C   PIC X(5).
             07 (SF)-DT-ULTC-PILDI-MM-C   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTC-PILDI-MM-C-NULL REDEFINES
                (SF)-DT-ULTC-PILDI-MM-C   PIC X(5).
             07 (SF)-DT-ULTC-PILDI-AA-C   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTC-PILDI-AA-C-NULL REDEFINES
                (SF)-DT-ULTC-PILDI-AA-C   PIC X(5).
             07 (SF)-DT-ULTC-PILDI-MM-I   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTC-PILDI-MM-I-NULL REDEFINES
                (SF)-DT-ULTC-PILDI-MM-I   PIC X(5).
             07 (SF)-DT-ULTC-PILDI-TR-I   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTC-PILDI-TR-I-NULL REDEFINES
                (SF)-DT-ULTC-PILDI-TR-I   PIC X(5).
             07 (SF)-DT-ULTC-PILDI-AA-I   PIC S9(8) COMP-3.
             07 (SF)-DT-ULTC-PILDI-AA-I-NULL REDEFINES
                (SF)-DT-ULTC-PILDI-AA-I   PIC X(5).
             07 (SF)-DT-ULT-BOLL-QUIE-C   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-QUIE-C-NULL REDEFINES
                (SF)-DT-ULT-BOLL-QUIE-C   PIC X(5).
             07 (SF)-DT-ULT-BOLL-QUIE-I   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-QUIE-I-NULL REDEFINES
                (SF)-DT-ULT-BOLL-QUIE-I   PIC X(5).
             07 (SF)-DT-ULT-BOLL-COTR-I   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-COTR-I-NULL REDEFINES
                (SF)-DT-ULT-BOLL-COTR-I   PIC X(5).
             07 (SF)-DT-ULT-BOLL-COTR-C   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-COTR-C-NULL REDEFINES
                (SF)-DT-ULT-BOLL-COTR-C   PIC X(5).
             07 (SF)-DT-ULT-BOLL-CORI-C   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-CORI-C-NULL REDEFINES
                (SF)-DT-ULT-BOLL-CORI-C   PIC X(5).
             07 (SF)-DT-ULT-BOLL-CORI-I   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-BOLL-CORI-I-NULL REDEFINES
                (SF)-DT-ULT-BOLL-CORI-I   PIC X(5).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-TP-VALZZ-DT-VLT PIC X(2).
             07 (SF)-TP-VALZZ-DT-VLT-NULL REDEFINES
                (SF)-TP-VALZZ-DT-VLT   PIC X(2).
             07 (SF)-FL-FRAZ-PROV-ACQ PIC X(1).
             07 (SF)-FL-FRAZ-PROV-ACQ-NULL REDEFINES
                (SF)-FL-FRAZ-PROV-ACQ   PIC X(1).
             07 (SF)-DT-ULT-AGG-EROG-RE   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-AGG-EROG-RE-NULL REDEFINES
                (SF)-DT-ULT-AGG-EROG-RE   PIC X(5).
             07 (SF)-PC-RM-MARSOL PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-RM-MARSOL-NULL REDEFINES
                (SF)-PC-RM-MARSOL   PIC X(4).
             07 (SF)-PC-C-SUBRSH-MARSOL PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-C-SUBRSH-MARSOL-NULL REDEFINES
                (SF)-PC-C-SUBRSH-MARSOL   PIC X(4).
             07 (SF)-COD-COMP-ISVAP PIC X(5).
             07 (SF)-COD-COMP-ISVAP-NULL REDEFINES
                (SF)-COD-COMP-ISVAP   PIC X(5).
             07 (SF)-LM-RIS-CON-INT PIC S9(3)V9(3) COMP-3.
             07 (SF)-LM-RIS-CON-INT-NULL REDEFINES
                (SF)-LM-RIS-CON-INT   PIC X(4).
             07 (SF)-LM-C-SUBRSH-CON-IN PIC S9(3)V9(3) COMP-3.
             07 (SF)-LM-C-SUBRSH-CON-IN-NULL REDEFINES
                (SF)-LM-C-SUBRSH-CON-IN   PIC X(4).
             07 (SF)-PC-GAR-NORISK-MARS PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-GAR-NORISK-MARS-NULL REDEFINES
                (SF)-PC-GAR-NORISK-MARS   PIC X(4).
             07 (SF)-CRZ-1A-RAT-INTR-PR PIC X(1).
             07 (SF)-CRZ-1A-RAT-INTR-PR-NULL REDEFINES
                (SF)-CRZ-1A-RAT-INTR-PR   PIC X(1).
             07 (SF)-NUM-GG-ARR-INTR-PR PIC S9(5)     COMP-3.
             07 (SF)-NUM-GG-ARR-INTR-PR-NULL REDEFINES
                (SF)-NUM-GG-ARR-INTR-PR   PIC X(3).
             07 (SF)-FL-VISUAL-VINPG PIC X(1).
             07 (SF)-FL-VISUAL-VINPG-NULL REDEFINES
                (SF)-FL-VISUAL-VINPG   PIC X(1).
             07 (SF)-DT-ULT-ESTRAZ-FUG   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ESTRAZ-FUG-NULL REDEFINES
                (SF)-DT-ULT-ESTRAZ-FUG   PIC X(5).
             07 (SF)-DT-ULT-ELAB-PR-AUT   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ELAB-PR-AUT-NULL REDEFINES
                (SF)-DT-ULT-ELAB-PR-AUT   PIC X(5).
             07 (SF)-DT-ULT-ELAB-COMMEF   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ELAB-COMMEF-NULL REDEFINES
                (SF)-DT-ULT-ELAB-COMMEF   PIC X(5).
             07 (SF)-DT-ULT-ELAB-LIQMEF   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ELAB-LIQMEF-NULL REDEFINES
                (SF)-DT-ULT-ELAB-LIQMEF   PIC X(5).
             07 (SF)-COD-FISC-MEF PIC X(16).
             07 (SF)-COD-FISC-MEF-NULL REDEFINES
                (SF)-COD-FISC-MEF   PIC X(16).
             07 (SF)-IMP-ASS-SOCIALE PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-ASS-SOCIALE-NULL REDEFINES
                (SF)-IMP-ASS-SOCIALE   PIC X(8).
             07 (SF)-MOD-COMNZ-INVST-SW PIC X(1).
             07 (SF)-MOD-COMNZ-INVST-SW-NULL REDEFINES
                (SF)-MOD-COMNZ-INVST-SW   PIC X(1).
             07 (SF)-DT-RIAT-RIASS-RSH   PIC S9(8) COMP-3.
             07 (SF)-DT-RIAT-RIASS-RSH-NULL REDEFINES
                (SF)-DT-RIAT-RIASS-RSH   PIC X(5).
             07 (SF)-DT-RIAT-RIASS-COMM   PIC S9(8) COMP-3.
             07 (SF)-DT-RIAT-RIASS-COMM-NULL REDEFINES
                (SF)-DT-RIAT-RIASS-COMM   PIC X(5).
             07 (SF)-GG-INTR-RIT-PAG PIC S9(5)     COMP-3.
             07 (SF)-GG-INTR-RIT-PAG-NULL REDEFINES
                (SF)-GG-INTR-RIT-PAG   PIC X(3).
             07 (SF)-DT-ULT-RINN-TAC   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-RINN-TAC-NULL REDEFINES
                (SF)-DT-ULT-RINN-TAC   PIC X(5).
ITRAT        07 (SF)-DESC-COMP-VCHAR.
ITRAT           49 (SF)-DESC-COMP-LEN PIC S9(4) COMP-5.
ITRAT           49 (SF)-DESC-COMP PIC X(250).
             07 (SF)-DT-ULT-EC-TCM-IND   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-EC-TCM-IND-NULL REDEFINES
                (SF)-DT-ULT-EC-TCM-IND   PIC X(5).
             07 (SF)-DT-ULT-EC-TCM-COLL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-EC-TCM-COLL-NULL REDEFINES
                (SF)-DT-ULT-EC-TCM-COLL   PIC X(5).
             07 (SF)-DT-ULT-EC-MRM-IND   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-EC-MRM-IND-NULL REDEFINES
                (SF)-DT-ULT-EC-MRM-IND   PIC X(5).
             07 (SF)-DT-ULT-EC-MRM-COLL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-EC-MRM-COLL-NULL REDEFINES
                (SF)-DT-ULT-EC-MRM-COLL   PIC X(5).
             07 (SF)-COD-COMP-LDAP PIC X(20).
             07 (SF)-COD-COMP-LDAP-NULL REDEFINES
                (SF)-COD-COMP-LDAP   PIC X(20).
             07 (SF)-PC-RID-IMP-1382011 PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-RID-IMP-1382011-NULL REDEFINES
                (SF)-PC-RID-IMP-1382011   PIC X(4).
             07 (SF)-PC-RID-IMP-662014 PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-RID-IMP-662014-NULL REDEFINES
                (SF)-PC-RID-IMP-662014   PIC X(4).
             07 (SF)-SOGL-AML-PRE-UNI PIC S9(12)V9(3) COMP-3.
             07 (SF)-SOGL-AML-PRE-UNI-NULL REDEFINES
                (SF)-SOGL-AML-PRE-UNI   PIC X(8).
             07 (SF)-SOGL-AML-PRE-PER PIC S9(12)V9(3) COMP-3.
             07 (SF)-SOGL-AML-PRE-PER-NULL REDEFINES
                (SF)-SOGL-AML-PRE-PER   PIC X(8).
             07 (SF)-COD-SOGG-FTZ-ASSTO PIC X(20).
             07 (SF)-COD-SOGG-FTZ-ASSTO-NULL REDEFINES
                (SF)-COD-SOGG-FTZ-ASSTO   PIC X(20).
             07 (SF)-DT-ULT-ELAB-REDPRO   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ELAB-REDPRO-NULL REDEFINES
                (SF)-DT-ULT-ELAB-REDPRO   PIC X(5).
             07 (SF)-DT-ULT-ELAB-TAKE-P   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ELAB-TAKE-P-NULL REDEFINES
                (SF)-DT-ULT-ELAB-TAKE-P   PIC X(5).
             07 (SF)-DT-ULT-ELAB-PASPAS   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ELAB-PASPAS-NULL REDEFINES
                (SF)-DT-ULT-ELAB-PASPAS   PIC X(5).
             07 (SF)-SOGL-AML-PRE-SAV-R PIC S9(12)V9(3) COMP-3.
             07 (SF)-SOGL-AML-PRE-SAV-R-NULL REDEFINES
                (SF)-SOGL-AML-PRE-SAV-R   PIC X(8).
             07 (SF)-DT-ULT-ESTR-DEC-CO   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ESTR-DEC-CO-NULL REDEFINES
                (SF)-DT-ULT-ESTR-DEC-CO   PIC X(5).
             07 (SF)-DT-ULT-ELAB-COS-AT   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ELAB-COS-AT-NULL REDEFINES
                (SF)-DT-ULT-ELAB-COS-AT   PIC X(5).
             07 (SF)-FRQ-COSTI-ATT PIC S9(5)     COMP-3.
             07 (SF)-FRQ-COSTI-ATT-NULL REDEFINES
                (SF)-FRQ-COSTI-ATT   PIC X(3).
             07 (SF)-DT-ULT-ELAB-COS-ST   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-ELAB-COS-ST-NULL REDEFINES
                (SF)-DT-ULT-ELAB-COS-ST   PIC X(5).
             07 (SF)-FRQ-COSTI-STORNATI PIC S9(5)     COMP-3.
             07 (SF)-FRQ-COSTI-STORNATI-NULL REDEFINES
                (SF)-FRQ-COSTI-STORNATI   PIC X(3).
             07 (SF)-DT-ESTR-ASS-MIN70A   PIC S9(8) COMP-3.
             07 (SF)-DT-ESTR-ASS-MIN70A-NULL REDEFINES
                (SF)-DT-ESTR-ASS-MIN70A   PIC X(5).
             07 (SF)-DT-ESTR-ASS-MAG70A   PIC S9(8) COMP-3.
             07 (SF)-DT-ESTR-ASS-MAG70A-NULL REDEFINES
                (SF)-DT-ESTR-ASS-MAG70A   PIC X(5).

      *----------------------------------------------------------------*
      *   CHIAMATA AL DISPATCHER
      *   POPOLAMENTO AREA INPUT DISPATCHER
      *   ULTIMO AGG.  03 LUG 2007                                     *
      *----------------------------------------------------------------*

       A0004-AREA-COMUNE-DSH.

           INITIALIZE IDSI0011-BUFFER-DATI.

           SET  IDSO0011-SUCCESSFUL-RC   TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL  TO TRUE.
           SET  IDSV0001-ESITO-OK        TO TRUE.


           MOVE WK-TABELLA               TO IDSI0011-CODICE-STR-DATO.

      *-------------- POPOLAMENTO AREA INPUT DISPATCHER ---------------*
      *--> DATA EFFETTO
           MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
                                            IDSI0011-DATA-COMPETENZA.

      *--> TABELLA NON STORICA
           SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
           SET IDSI0011-SELECT           TO TRUE.
           SET IDSI0011-PRIMARY-KEY      TO TRUE.

       A0004-EX.
           EXIT.


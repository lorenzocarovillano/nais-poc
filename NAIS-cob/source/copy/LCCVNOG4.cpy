
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVNOG4
      *   ULTIMO AGG. 02 FEB 2009
      *------------------------------------------------------------

       INIZIA-TOT-NOG.

           PERFORM INIZIA-ZEROES-NOG THRU INIZIA-ZEROES-NOG-EX

           PERFORM INIZIA-SPACES-NOG THRU INIZIA-SPACES-NOG-EX

           PERFORM INIZIA-NULL-NOG THRU INIZIA-NULL-NOG-EX.

       INIZIA-TOT-NOG-EX.
           EXIT.

       INIZIA-NULL-NOG.
           MOVE HIGH-VALUES TO (SF)-ULT-PROGR-NULL.
       INIZIA-NULL-NOG-EX.
           EXIT.

       INIZIA-ZEROES-NOG.
           MOVE 0 TO (SF)-COD-COMPAGNIA-ANIA.
       INIZIA-ZEROES-NOG-EX.
           EXIT.

       INIZIA-SPACES-NOG.
           MOVE SPACES TO (SF)-FORMA-ASSICURATIVA
           MOVE SPACES TO (SF)-COD-OGGETTO
           MOVE SPACES TO (SF)-TIPO-OGGETTO
           MOVE SPACES TO (SF)-DESC-OGGETTO.
       INIZIA-SPACES-NOG-EX.
           EXIT.

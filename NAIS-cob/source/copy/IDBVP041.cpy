       01 STAT-RICH-EST.
         05 P04-ID-STAT-RICH-EST PIC S9(9)V     COMP-3.
         05 P04-ID-RICH-EST PIC S9(9)V     COMP-3.
         05 P04-TS-INI-VLDT PIC S9(18)V     COMP-3.
         05 P04-TS-END-VLDT PIC S9(18)V     COMP-3.
         05 P04-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 P04-COD-PRCS PIC X(3).
         05 P04-COD-ATTVT PIC X(10).
         05 P04-STAT-RICH-EST PIC X(2).
         05 P04-FL-STAT-END PIC X(1).
         05 P04-FL-STAT-END-NULL REDEFINES
            P04-FL-STAT-END   PIC X(1).
         05 P04-DS-OPER-SQL PIC X(1).
         05 P04-DS-VER PIC S9(9)V     COMP-3.
         05 P04-DS-TS-CPTZ PIC S9(18)V     COMP-3.
         05 P04-DS-UTENTE PIC X(20).
         05 P04-DS-STATO-ELAB PIC X(1).
         05 P04-TP-CAUS-SCARTO PIC X(2).
         05 P04-TP-CAUS-SCARTO-NULL REDEFINES
            P04-TP-CAUS-SCARTO   PIC X(2).
         05 P04-DESC-ERR-VCHAR.
           49 P04-DESC-ERR-LEN PIC S9(4) COMP-5.
           49 P04-DESC-ERR PIC X(250).
         05 P04-UTENTE-INS-AGG PIC X(20).
         05 P04-COD-ERR-SCARTO PIC X(10).
         05 P04-COD-ERR-SCARTO-NULL REDEFINES
            P04-COD-ERR-SCARTO   PIC X(10).
         05 P04-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
         05 P04-ID-MOVI-CRZ-NULL REDEFINES
            P04-ID-MOVI-CRZ   PIC X(5).


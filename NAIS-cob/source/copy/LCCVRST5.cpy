
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVRST5
      *   ULTIMO AGG. 09 LUG 2010
      *------------------------------------------------------------

       VAL-DCLGEN-RST.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RST)
              TO RST-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-RST)
              TO RST-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-RST) NOT NUMERIC
              MOVE 0 TO RST-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-RST)
              TO RST-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-RST) NOT NUMERIC
              MOVE 0 TO RST-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-RST)
              TO RST-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-RST)
              TO RST-COD-COMP-ANIA
           IF (SF)-TP-CALC-RIS-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-TP-CALC-RIS-NULL(IX-TAB-RST)
              TO RST-TP-CALC-RIS-NULL
           ELSE
              MOVE (SF)-TP-CALC-RIS(IX-TAB-RST)
              TO RST-TP-CALC-RIS
           END-IF
           IF (SF)-ULT-RM-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-ULT-RM-NULL(IX-TAB-RST)
              TO RST-ULT-RM-NULL
           ELSE
              MOVE (SF)-ULT-RM(IX-TAB-RST)
              TO RST-ULT-RM
           END-IF
           IF (SF)-DT-CALC-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-DT-CALC-NULL(IX-TAB-RST)
              TO RST-DT-CALC-NULL
           ELSE
             IF (SF)-DT-CALC(IX-TAB-RST) = ZERO
                MOVE HIGH-VALUES
                TO RST-DT-CALC-NULL
             ELSE
              MOVE (SF)-DT-CALC(IX-TAB-RST)
              TO RST-DT-CALC
             END-IF
           END-IF
           IF (SF)-DT-ELAB-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-DT-ELAB-NULL(IX-TAB-RST)
              TO RST-DT-ELAB-NULL
           ELSE
             IF (SF)-DT-ELAB(IX-TAB-RST) = ZERO
                MOVE HIGH-VALUES
                TO RST-DT-ELAB-NULL
             ELSE
              MOVE (SF)-DT-ELAB(IX-TAB-RST)
              TO RST-DT-ELAB
             END-IF
           END-IF
           IF (SF)-RIS-BILA-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-BILA-NULL(IX-TAB-RST)
              TO RST-RIS-BILA-NULL
           ELSE
              MOVE (SF)-RIS-BILA(IX-TAB-RST)
              TO RST-RIS-BILA
           END-IF
           IF (SF)-RIS-MAT-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-MAT-NULL(IX-TAB-RST)
              TO RST-RIS-MAT-NULL
           ELSE
              MOVE (SF)-RIS-MAT(IX-TAB-RST)
              TO RST-RIS-MAT
           END-IF
           IF (SF)-INCR-X-RIVAL-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-INCR-X-RIVAL-NULL(IX-TAB-RST)
              TO RST-INCR-X-RIVAL-NULL
           ELSE
              MOVE (SF)-INCR-X-RIVAL(IX-TAB-RST)
              TO RST-INCR-X-RIVAL
           END-IF
           IF (SF)-RPTO-PRE-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RPTO-PRE-NULL(IX-TAB-RST)
              TO RST-RPTO-PRE-NULL
           ELSE
              MOVE (SF)-RPTO-PRE(IX-TAB-RST)
              TO RST-RPTO-PRE
           END-IF
           IF (SF)-FRAZ-PRE-PP-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-FRAZ-PRE-PP-NULL(IX-TAB-RST)
              TO RST-FRAZ-PRE-PP-NULL
           ELSE
              MOVE (SF)-FRAZ-PRE-PP(IX-TAB-RST)
              TO RST-FRAZ-PRE-PP
           END-IF
           IF (SF)-RIS-TOT-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-TOT-NULL(IX-TAB-RST)
              TO RST-RIS-TOT-NULL
           ELSE
              MOVE (SF)-RIS-TOT(IX-TAB-RST)
              TO RST-RIS-TOT
           END-IF
           IF (SF)-RIS-SPE-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-SPE-NULL(IX-TAB-RST)
              TO RST-RIS-SPE-NULL
           ELSE
              MOVE (SF)-RIS-SPE(IX-TAB-RST)
              TO RST-RIS-SPE
           END-IF
           IF (SF)-RIS-ABB-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-ABB-NULL(IX-TAB-RST)
              TO RST-RIS-ABB-NULL
           ELSE
              MOVE (SF)-RIS-ABB(IX-TAB-RST)
              TO RST-RIS-ABB
           END-IF
           IF (SF)-RIS-BNSFDT-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-BNSFDT-NULL(IX-TAB-RST)
              TO RST-RIS-BNSFDT-NULL
           ELSE
              MOVE (SF)-RIS-BNSFDT(IX-TAB-RST)
              TO RST-RIS-BNSFDT
           END-IF
           IF (SF)-RIS-SOPR-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-SOPR-NULL(IX-TAB-RST)
              TO RST-RIS-SOPR-NULL
           ELSE
              MOVE (SF)-RIS-SOPR(IX-TAB-RST)
              TO RST-RIS-SOPR
           END-IF
           IF (SF)-RIS-INTEG-BAS-TEC-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-INTEG-BAS-TEC-NULL(IX-TAB-RST)
              TO RST-RIS-INTEG-BAS-TEC-NULL
           ELSE
              MOVE (SF)-RIS-INTEG-BAS-TEC(IX-TAB-RST)
              TO RST-RIS-INTEG-BAS-TEC
           END-IF
           IF (SF)-RIS-INTEG-DECR-TS-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-INTEG-DECR-TS-NULL(IX-TAB-RST)
              TO RST-RIS-INTEG-DECR-TS-NULL
           ELSE
              MOVE (SF)-RIS-INTEG-DECR-TS(IX-TAB-RST)
              TO RST-RIS-INTEG-DECR-TS
           END-IF
           IF (SF)-RIS-GAR-CASO-MOR-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-GAR-CASO-MOR-NULL(IX-TAB-RST)
              TO RST-RIS-GAR-CASO-MOR-NULL
           ELSE
              MOVE (SF)-RIS-GAR-CASO-MOR(IX-TAB-RST)
              TO RST-RIS-GAR-CASO-MOR
           END-IF
           IF (SF)-RIS-ZIL-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-ZIL-NULL(IX-TAB-RST)
              TO RST-RIS-ZIL-NULL
           ELSE
              MOVE (SF)-RIS-ZIL(IX-TAB-RST)
              TO RST-RIS-ZIL
           END-IF
           IF (SF)-RIS-FAIVL-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-FAIVL-NULL(IX-TAB-RST)
              TO RST-RIS-FAIVL-NULL
           ELSE
              MOVE (SF)-RIS-FAIVL(IX-TAB-RST)
              TO RST-RIS-FAIVL
           END-IF
           IF (SF)-RIS-COS-AMMTZ-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-COS-AMMTZ-NULL(IX-TAB-RST)
              TO RST-RIS-COS-AMMTZ-NULL
           ELSE
              MOVE (SF)-RIS-COS-AMMTZ(IX-TAB-RST)
              TO RST-RIS-COS-AMMTZ
           END-IF
           IF (SF)-RIS-SPE-FAIVL-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-SPE-FAIVL-NULL(IX-TAB-RST)
              TO RST-RIS-SPE-FAIVL-NULL
           ELSE
              MOVE (SF)-RIS-SPE-FAIVL(IX-TAB-RST)
              TO RST-RIS-SPE-FAIVL
           END-IF
           IF (SF)-RIS-PREST-FAIVL-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-PREST-FAIVL-NULL(IX-TAB-RST)
              TO RST-RIS-PREST-FAIVL-NULL
           ELSE
              MOVE (SF)-RIS-PREST-FAIVL(IX-TAB-RST)
              TO RST-RIS-PREST-FAIVL
           END-IF
           IF (SF)-RIS-COMPON-ASSVA-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-COMPON-ASSVA-NULL(IX-TAB-RST)
              TO RST-RIS-COMPON-ASSVA-NULL
           ELSE
              MOVE (SF)-RIS-COMPON-ASSVA(IX-TAB-RST)
              TO RST-RIS-COMPON-ASSVA
           END-IF
           IF (SF)-ULT-COEFF-RIS-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-ULT-COEFF-RIS-NULL(IX-TAB-RST)
              TO RST-ULT-COEFF-RIS-NULL
           ELSE
              MOVE (SF)-ULT-COEFF-RIS(IX-TAB-RST)
              TO RST-ULT-COEFF-RIS
           END-IF
           IF (SF)-ULT-COEFF-AGG-RIS-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-ULT-COEFF-AGG-RIS-NULL(IX-TAB-RST)
              TO RST-ULT-COEFF-AGG-RIS-NULL
           ELSE
              MOVE (SF)-ULT-COEFF-AGG-RIS(IX-TAB-RST)
              TO RST-ULT-COEFF-AGG-RIS
           END-IF
           IF (SF)-RIS-ACQ-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-ACQ-NULL(IX-TAB-RST)
              TO RST-RIS-ACQ-NULL
           ELSE
              MOVE (SF)-RIS-ACQ(IX-TAB-RST)
              TO RST-RIS-ACQ
           END-IF
           IF (SF)-RIS-UTI-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-UTI-NULL(IX-TAB-RST)
              TO RST-RIS-UTI-NULL
           ELSE
              MOVE (SF)-RIS-UTI(IX-TAB-RST)
              TO RST-RIS-UTI
           END-IF
           IF (SF)-RIS-MAT-EFF-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-MAT-EFF-NULL(IX-TAB-RST)
              TO RST-RIS-MAT-EFF-NULL
           ELSE
              MOVE (SF)-RIS-MAT-EFF(IX-TAB-RST)
              TO RST-RIS-MAT-EFF
           END-IF
           IF (SF)-RIS-RISTORNI-CAP-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-RISTORNI-CAP-NULL(IX-TAB-RST)
              TO RST-RIS-RISTORNI-CAP-NULL
           ELSE
              MOVE (SF)-RIS-RISTORNI-CAP(IX-TAB-RST)
              TO RST-RIS-RISTORNI-CAP
           END-IF
           IF (SF)-RIS-TRM-BNS-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-TRM-BNS-NULL(IX-TAB-RST)
              TO RST-RIS-TRM-BNS-NULL
           ELSE
              MOVE (SF)-RIS-TRM-BNS(IX-TAB-RST)
              TO RST-RIS-TRM-BNS
           END-IF
           IF (SF)-RIS-BNSRIC-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-BNSRIC-NULL(IX-TAB-RST)
              TO RST-RIS-BNSRIC-NULL
           ELSE
              MOVE (SF)-RIS-BNSRIC(IX-TAB-RST)
              TO RST-RIS-BNSRIC
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-RST) NOT NUMERIC
              MOVE 0 TO RST-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-RST)
              TO RST-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-RST)
              TO RST-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-RST) NOT NUMERIC
              MOVE 0 TO RST-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-RST)
              TO RST-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-RST) NOT NUMERIC
              MOVE 0 TO RST-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-RST)
              TO RST-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-RST) NOT NUMERIC
              MOVE 0 TO RST-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-RST)
              TO RST-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-RST)
              TO RST-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-RST)
              TO RST-DS-STATO-ELAB
           IF (SF)-COD-FND-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-COD-FND-NULL(IX-TAB-RST)
              TO RST-COD-FND-NULL
           ELSE
              MOVE (SF)-COD-FND(IX-TAB-RST)
              TO RST-COD-FND
           END-IF
           MOVE (SF)-ID-POLI(IX-TAB-RST)
              TO RST-ID-POLI
           MOVE (SF)-ID-ADES(IX-TAB-RST)
              TO RST-ID-ADES
           MOVE (SF)-ID-GAR(IX-TAB-RST)
              TO RST-ID-GAR
           IF (SF)-RIS-MIN-GARTO-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-MIN-GARTO-NULL(IX-TAB-RST)
              TO RST-RIS-MIN-GARTO-NULL
           ELSE
              MOVE (SF)-RIS-MIN-GARTO(IX-TAB-RST)
              TO RST-RIS-MIN-GARTO
           END-IF
           IF (SF)-RIS-RSH-DFLT-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-RSH-DFLT-NULL(IX-TAB-RST)
              TO RST-RIS-RSH-DFLT-NULL
           ELSE
              MOVE (SF)-RIS-RSH-DFLT(IX-TAB-RST)
              TO RST-RIS-RSH-DFLT
           END-IF
           IF (SF)-RIS-MOVI-NON-INVES-NULL(IX-TAB-RST) = HIGH-VALUES
              MOVE (SF)-RIS-MOVI-NON-INVES-NULL(IX-TAB-RST)
              TO RST-RIS-MOVI-NON-INVES-NULL
           ELSE
              MOVE (SF)-RIS-MOVI-NON-INVES(IX-TAB-RST)
              TO RST-RIS-MOVI-NON-INVES
           END-IF.
       VAL-DCLGEN-RST-EX.
           EXIT.

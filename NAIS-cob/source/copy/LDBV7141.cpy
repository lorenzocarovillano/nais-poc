       01 LDBV7141.
         05 LDBV7141-ID-OGG                 PIC S9(9)   COMP-3.
         05 LDBV7141-TP-OGG                 PIC  X(2).
         05 LDBV7141-TP-STA-TIT1            PIC  X(2).
         05 LDBV7141-TP-STA-TIT2            PIC  X(2).
         05 LDBV7141-TP-TIT                 PIC  X(2).
         05 LDBV7141-DT-DA                  PIC S9(8)   COMP-3.
         05 LDBV7141-DT-DA-DB               PIC X(10)   VALUE SPACE.
         05 LDBV7141-DT-A                   PIC S9(8)   COMP-3.
         05 LDBV7141-DT-A-DB                PIC X(10)   VALUE SPACE.

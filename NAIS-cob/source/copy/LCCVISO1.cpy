      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA IMPST_SOST
      *   ALIAS ISO
      *   ULTIMO AGG. 02 SET 2008
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-IMPST-SOST PIC S9(9)     COMP-3.
             07 (SF)-ID-OGG PIC S9(9)     COMP-3.
             07 (SF)-ID-OGG-NULL REDEFINES
                (SF)-ID-OGG   PIC X(5).
             07 (SF)-TP-OGG PIC X(2).
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-INI-PER   PIC S9(8) COMP-3.
             07 (SF)-DT-INI-PER-NULL REDEFINES
                (SF)-DT-INI-PER   PIC X(5).
             07 (SF)-DT-END-PER   PIC S9(8) COMP-3.
             07 (SF)-DT-END-PER-NULL REDEFINES
                (SF)-DT-END-PER   PIC X(5).
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-IMPST-SOST PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-SOST-NULL REDEFINES
                (SF)-IMPST-SOST   PIC X(8).
             07 (SF)-IMPB-IS PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-NULL REDEFINES
                (SF)-IMPB-IS   PIC X(8).
             07 (SF)-ALQ-IS PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-IS-NULL REDEFINES
                (SF)-ALQ-IS   PIC X(4).
             07 (SF)-COD-TRB PIC X(20).
             07 (SF)-COD-TRB-NULL REDEFINES
                (SF)-COD-TRB   PIC X(20).
             07 (SF)-PRSTZ-LRD-ANTE-IS PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-LRD-ANTE-IS-NULL REDEFINES
                (SF)-PRSTZ-LRD-ANTE-IS   PIC X(8).
             07 (SF)-RIS-MAT-NET-PREC PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-MAT-NET-PREC-NULL REDEFINES
                (SF)-RIS-MAT-NET-PREC   PIC X(8).
             07 (SF)-RIS-MAT-ANTE-TAX PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-MAT-ANTE-TAX-NULL REDEFINES
                (SF)-RIS-MAT-ANTE-TAX   PIC X(8).
             07 (SF)-RIS-MAT-POST-TAX PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-MAT-POST-TAX-NULL REDEFINES
                (SF)-RIS-MAT-POST-TAX   PIC X(8).
             07 (SF)-PRSTZ-NET PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-NET-NULL REDEFINES
                (SF)-PRSTZ-NET   PIC X(8).
             07 (SF)-PRSTZ-PREC PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-PREC-NULL REDEFINES
                (SF)-PRSTZ-PREC   PIC X(8).
             07 (SF)-CUM-PRE-VERS PIC S9(12)V9(3) COMP-3.
             07 (SF)-CUM-PRE-VERS-NULL REDEFINES
                (SF)-CUM-PRE-VERS   PIC X(8).
             07 (SF)-TP-CALC-IMPST PIC X(2).
             07 (SF)-IMP-GIA-TASSATO PIC S9(12)V9(3) COMP-3.
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).

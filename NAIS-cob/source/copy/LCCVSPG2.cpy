      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       LETTURA-SPG.

           IF IDSI0011-SELECT
            PERFORM SELECT-DISPATCHER
                THRU SELECT-DISPATCHER-EX
         ELSE
            PERFORM FETCH-DISPATCHER
                THRU FETCH-DISPATCHER-EX
           END-IF.

       LETTURA-SPG-EX.
           EXIT.
      *----------------------------------------------------------------*
      *                         SELECT                                 *
      *----------------------------------------------------------------*
       SELECT-DISPATCHER.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
      *-->    GESTIRE ERRORE DB
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                     MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'SELECT-DISPATCHER'
                                             TO IEAI9901-LABEL-ERR
                     MOVE '005017'           TO IEAI9901-COD-ERRORE
                     MOVE 'SPG-ID-SOPR-DI-GAR' TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI TO SOPR-DI-GAR
                  WHEN OTHER
      *------  ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM
                                        TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'SELECT-DISPATCHER'
                                        TO IEAI9901-LABEL-ERR
                     MOVE '005015'      TO IEAI9901-COD-ERRORE
                     MOVE WK-TABELLA    TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SELECT-DISPATCHER'       TO IEAI9901-LABEL-ERR
              MOVE '005016'                  TO IEAI9901-COD-ERRORE
              MOVE SPACES        TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       SELECT-DISPATCHER-EX.
           EXIT.
      *----------------------------------------------------------------*
      *                         FETCH                                  *
      *----------------------------------------------------------------*
       FETCH-DISPATCHER.


             SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
             SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
             SET  WCOM-OVERFLOW-NO                  TO TRUE.

             PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                        OR NOT IDSO0011-SUCCESSFUL-SQL
                        OR WCOM-OVERFLOW-YES

                PERFORM CALL-DISPATCHER
                   THRU CALL-DISPATCHER-EX

                IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
      *-->            NON TROVATA
                      WHEN IDSO0011-NOT-FOUND
                         CONTINUE

                      WHEN IDSO0011-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                         MOVE IDSO0011-BUFFER-DATI TO SOPR-DI-GAR
                          ADD 1 TO IX-TAB-SPG

      *-->  Se il contatore di lettura � maggiore dell' elemento MAX
      *-->  previsto per la TABELLA allora siamo in overflow
                         IF IX-TAB-SPG > (SF)-ELE-SOPRE-MAX
                           SET WCOM-OVERFLOW-YES TO TRUE
                         ELSE
                           PERFORM VALORIZZA-OUTPUT-SPG
                              THRU VALORIZZA-OUTPUT-SPG-EX
                           SET  IDSI0011-FETCH-NEXT TO TRUE
                         END-IF
                      WHEN OTHER
      *-------------------ERRORE DI ACCESSO AL DB
                         MOVE WK-PGM
                           TO IEAI9901-COD-SERVIZIO-BE
                         MOVE 'FETCH-DISPATCHER'
                           TO IEAI9901-LABEL-ERR
                         MOVE '005015'
                           TO IEAI9901-COD-ERRORE
                         MOVE WK-TABELLA
                           TO IEAI9901-PARAMETRI-ERR
                         PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            THRU EX-S0300
                     END-EVALUATE
                   ELSE
      *-->    GESTIRE ERRORE DISPATCHER
                      MOVE WK-PGM
                        TO IEAI9901-COD-SERVIZIO-BE
                      MOVE 'FETCH-DISPATCHER'
                        TO IEAI9901-LABEL-ERR
                      MOVE '005016'
                        TO IEAI9901-COD-ERRORE
                      MOVE SPACES
                        TO IEAI9901-PARAMETRI-ERR
                      PERFORM S0300-RICERCA-GRAVITA-ERRORE
                         THRU EX-S0300
                   END-IF

            END-PERFORM.

       FETCH-DISPATCHER-EX.
           EXIT.

       01 LRGC0041.
          03 DATI-EC.
             05 LRGC0041-ID-POLI           PIC S9(09).
             05 LRGC0041-ID-ADES           PIC S9(09).
             05 LRGC0041-DT-ESTR-CNT-X     PIC X(08).
             05 LRGC0041-DT-ESTR-CNT-N REDEFINES LRGC0041-DT-ESTR-CNT-X
                                           PIC 9(08).
             05 LRGC0041-DT-ESTR-CNT-ATT   PIC 9(08).
             05 LRGC0041-DT-DECOR-POLI     PIC X(08).
      *-----------------------------------------------------------------*
      *    AREA APPOGGIO PER UNIFORMARE IMPORTI TRA REC03 E REC08
      *-----------------------------------------------------------------*
          03  WK-AP-GAR-FND.
              05 WK-AP-ID-GAR               PIC 9(09).
              05 WK-AP-PREST-MATUR          PIC 9(12)V9(03).
              05 WK-AP-CNTRVAL-TOT          PIC 9(12)V9(03).
              05 WK-AP-FND-ELE-MAX          PIC 9(03).
              05 WK-AP-FND-TAB              OCCURS 100 TIMES.
                 07 WK-AP-COD-FND           PIC X(20).
                 07 WK-AP-CNTRVAL           PIC 9(12)V9(03).

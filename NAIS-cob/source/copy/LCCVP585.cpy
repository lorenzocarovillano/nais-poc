
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVP585
      *   ULTIMO AGG. 30 AGO 2013
      *------------------------------------------------------------

       VAL-DCLGEN-P58.
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-P58)
              TO P58-COD-COMP-ANIA
           MOVE (SF)-ID-POLI(IX-TAB-P58)
              TO P58-ID-POLI
           MOVE (SF)-IB-POLI(IX-TAB-P58)
              TO P58-IB-POLI
           IF (SF)-COD-FISC-NULL(IX-TAB-P58) = HIGH-VALUES
              MOVE (SF)-COD-FISC-NULL(IX-TAB-P58)
              TO P58-COD-FISC-NULL
           ELSE
              MOVE (SF)-COD-FISC(IX-TAB-P58)
              TO P58-COD-FISC
           END-IF
           IF (SF)-COD-PART-IVA-NULL(IX-TAB-P58) = HIGH-VALUES
              MOVE (SF)-COD-PART-IVA-NULL(IX-TAB-P58)
              TO P58-COD-PART-IVA-NULL
           ELSE
              MOVE (SF)-COD-PART-IVA(IX-TAB-P58)
              TO P58-COD-PART-IVA
           END-IF
           MOVE (SF)-ID-RAPP-ANA(IX-TAB-P58)
              TO P58-ID-RAPP-ANA
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P58) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P58)
              TO P58-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-P58)
              TO P58-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-P58) NOT NUMERIC
              MOVE 0 TO P58-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-P58)
              TO P58-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-P58) NOT NUMERIC
              MOVE 0 TO P58-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-P58)
              TO P58-DT-END-EFF
           END-IF
           MOVE (SF)-DT-INI-CALC(IX-TAB-P58)
              TO P58-DT-INI-CALC
           MOVE (SF)-DT-END-CALC(IX-TAB-P58)
              TO P58-DT-END-CALC
           MOVE (SF)-TP-CAUS-BOLLO(IX-TAB-P58)
              TO P58-TP-CAUS-BOLLO
           MOVE (SF)-IMPST-BOLLO-DETT-C(IX-TAB-P58)
              TO P58-IMPST-BOLLO-DETT-C
           IF (SF)-IMPST-BOLLO-DETT-V-NULL(IX-TAB-P58) = HIGH-VALUES
              MOVE (SF)-IMPST-BOLLO-DETT-V-NULL(IX-TAB-P58)
              TO P58-IMPST-BOLLO-DETT-V-NULL
           ELSE
              MOVE (SF)-IMPST-BOLLO-DETT-V(IX-TAB-P58)
              TO P58-IMPST-BOLLO-DETT-V
           END-IF
           IF (SF)-IMPST-BOLLO-TOT-V-NULL(IX-TAB-P58) = HIGH-VALUES
              MOVE (SF)-IMPST-BOLLO-TOT-V-NULL(IX-TAB-P58)
              TO P58-IMPST-BOLLO-TOT-V-NULL
           ELSE
              MOVE (SF)-IMPST-BOLLO-TOT-V(IX-TAB-P58)
              TO P58-IMPST-BOLLO-TOT-V
           END-IF
           MOVE (SF)-IMPST-BOLLO-TOT-R(IX-TAB-P58)
              TO P58-IMPST-BOLLO-TOT-R
           IF (SF)-DS-RIGA(IX-TAB-P58) NOT NUMERIC
              MOVE 0 TO P58-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-P58)
              TO P58-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-P58)
              TO P58-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-P58) NOT NUMERIC
              MOVE 0 TO P58-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-P58)
              TO P58-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-P58) NOT NUMERIC
              MOVE 0 TO P58-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-P58)
              TO P58-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-P58) NOT NUMERIC
              MOVE 0 TO P58-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-P58)
              TO P58-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-P58)
              TO P58-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-P58)
              TO P58-DS-STATO-ELAB.
       VAL-DCLGEN-P58-EX.
           EXIT.

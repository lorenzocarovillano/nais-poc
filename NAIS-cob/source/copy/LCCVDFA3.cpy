
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVDFA3
      *   ULTIMO AGG. 22 APR 2010
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-DFA.
           MOVE DFA-ID-D-FISC-ADES
             TO (SF)-ID-PTF
           MOVE DFA-ID-D-FISC-ADES
             TO (SF)-ID-D-FISC-ADES
           MOVE DFA-ID-ADES
             TO (SF)-ID-ADES
           MOVE DFA-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ
           IF DFA-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE DFA-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL
           ELSE
              MOVE DFA-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU
           END-IF
           MOVE DFA-DT-INI-EFF
             TO (SF)-DT-INI-EFF
           MOVE DFA-DT-END-EFF
             TO (SF)-DT-END-EFF
           MOVE DFA-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           IF DFA-TP-ISC-FND-NULL = HIGH-VALUES
              MOVE DFA-TP-ISC-FND-NULL
                TO (SF)-TP-ISC-FND-NULL
           ELSE
              MOVE DFA-TP-ISC-FND
                TO (SF)-TP-ISC-FND
           END-IF
           IF DFA-DT-ACCNS-RAPP-FND-NULL = HIGH-VALUES
              MOVE DFA-DT-ACCNS-RAPP-FND-NULL
                TO (SF)-DT-ACCNS-RAPP-FND-NULL
           ELSE
              MOVE DFA-DT-ACCNS-RAPP-FND
                TO (SF)-DT-ACCNS-RAPP-FND
           END-IF
           IF DFA-IMP-CNBT-AZ-K1-NULL = HIGH-VALUES
              MOVE DFA-IMP-CNBT-AZ-K1-NULL
                TO (SF)-IMP-CNBT-AZ-K1-NULL
           ELSE
              MOVE DFA-IMP-CNBT-AZ-K1
                TO (SF)-IMP-CNBT-AZ-K1
           END-IF
           IF DFA-IMP-CNBT-ISC-K1-NULL = HIGH-VALUES
              MOVE DFA-IMP-CNBT-ISC-K1-NULL
                TO (SF)-IMP-CNBT-ISC-K1-NULL
           ELSE
              MOVE DFA-IMP-CNBT-ISC-K1
                TO (SF)-IMP-CNBT-ISC-K1
           END-IF
           IF DFA-IMP-CNBT-TFR-K1-NULL = HIGH-VALUES
              MOVE DFA-IMP-CNBT-TFR-K1-NULL
                TO (SF)-IMP-CNBT-TFR-K1-NULL
           ELSE
              MOVE DFA-IMP-CNBT-TFR-K1
                TO (SF)-IMP-CNBT-TFR-K1
           END-IF
           IF DFA-IMP-CNBT-VOL-K1-NULL = HIGH-VALUES
              MOVE DFA-IMP-CNBT-VOL-K1-NULL
                TO (SF)-IMP-CNBT-VOL-K1-NULL
           ELSE
              MOVE DFA-IMP-CNBT-VOL-K1
                TO (SF)-IMP-CNBT-VOL-K1
           END-IF
           IF DFA-IMP-CNBT-AZ-K2-NULL = HIGH-VALUES
              MOVE DFA-IMP-CNBT-AZ-K2-NULL
                TO (SF)-IMP-CNBT-AZ-K2-NULL
           ELSE
              MOVE DFA-IMP-CNBT-AZ-K2
                TO (SF)-IMP-CNBT-AZ-K2
           END-IF
           IF DFA-IMP-CNBT-ISC-K2-NULL = HIGH-VALUES
              MOVE DFA-IMP-CNBT-ISC-K2-NULL
                TO (SF)-IMP-CNBT-ISC-K2-NULL
           ELSE
              MOVE DFA-IMP-CNBT-ISC-K2
                TO (SF)-IMP-CNBT-ISC-K2
           END-IF
           IF DFA-IMP-CNBT-TFR-K2-NULL = HIGH-VALUES
              MOVE DFA-IMP-CNBT-TFR-K2-NULL
                TO (SF)-IMP-CNBT-TFR-K2-NULL
           ELSE
              MOVE DFA-IMP-CNBT-TFR-K2
                TO (SF)-IMP-CNBT-TFR-K2
           END-IF
           IF DFA-IMP-CNBT-VOL-K2-NULL = HIGH-VALUES
              MOVE DFA-IMP-CNBT-VOL-K2-NULL
                TO (SF)-IMP-CNBT-VOL-K2-NULL
           ELSE
              MOVE DFA-IMP-CNBT-VOL-K2
                TO (SF)-IMP-CNBT-VOL-K2
           END-IF
           IF DFA-MATU-K1-NULL = HIGH-VALUES
              MOVE DFA-MATU-K1-NULL
                TO (SF)-MATU-K1-NULL
           ELSE
              MOVE DFA-MATU-K1
                TO (SF)-MATU-K1
           END-IF
           IF DFA-MATU-RES-K1-NULL = HIGH-VALUES
              MOVE DFA-MATU-RES-K1-NULL
                TO (SF)-MATU-RES-K1-NULL
           ELSE
              MOVE DFA-MATU-RES-K1
                TO (SF)-MATU-RES-K1
           END-IF
           IF DFA-MATU-K2-NULL = HIGH-VALUES
              MOVE DFA-MATU-K2-NULL
                TO (SF)-MATU-K2-NULL
           ELSE
              MOVE DFA-MATU-K2
                TO (SF)-MATU-K2
           END-IF
           IF DFA-IMPB-VIS-NULL = HIGH-VALUES
              MOVE DFA-IMPB-VIS-NULL
                TO (SF)-IMPB-VIS-NULL
           ELSE
              MOVE DFA-IMPB-VIS
                TO (SF)-IMPB-VIS
           END-IF
           IF DFA-IMPST-VIS-NULL = HIGH-VALUES
              MOVE DFA-IMPST-VIS-NULL
                TO (SF)-IMPST-VIS-NULL
           ELSE
              MOVE DFA-IMPST-VIS
                TO (SF)-IMPST-VIS
           END-IF
           IF DFA-IMPB-IS-K2-NULL = HIGH-VALUES
              MOVE DFA-IMPB-IS-K2-NULL
                TO (SF)-IMPB-IS-K2-NULL
           ELSE
              MOVE DFA-IMPB-IS-K2
                TO (SF)-IMPB-IS-K2
           END-IF
           IF DFA-IMPST-SOST-K2-NULL = HIGH-VALUES
              MOVE DFA-IMPST-SOST-K2-NULL
                TO (SF)-IMPST-SOST-K2-NULL
           ELSE
              MOVE DFA-IMPST-SOST-K2
                TO (SF)-IMPST-SOST-K2
           END-IF
           IF DFA-RIDZ-TFR-NULL = HIGH-VALUES
              MOVE DFA-RIDZ-TFR-NULL
                TO (SF)-RIDZ-TFR-NULL
           ELSE
              MOVE DFA-RIDZ-TFR
                TO (SF)-RIDZ-TFR
           END-IF
           IF DFA-PC-TFR-NULL = HIGH-VALUES
              MOVE DFA-PC-TFR-NULL
                TO (SF)-PC-TFR-NULL
           ELSE
              MOVE DFA-PC-TFR
                TO (SF)-PC-TFR
           END-IF
           IF DFA-ALQ-TFR-NULL = HIGH-VALUES
              MOVE DFA-ALQ-TFR-NULL
                TO (SF)-ALQ-TFR-NULL
           ELSE
              MOVE DFA-ALQ-TFR
                TO (SF)-ALQ-TFR
           END-IF
           IF DFA-TOT-ANTIC-NULL = HIGH-VALUES
              MOVE DFA-TOT-ANTIC-NULL
                TO (SF)-TOT-ANTIC-NULL
           ELSE
              MOVE DFA-TOT-ANTIC
                TO (SF)-TOT-ANTIC
           END-IF
           IF DFA-IMPB-TFR-ANTIC-NULL = HIGH-VALUES
              MOVE DFA-IMPB-TFR-ANTIC-NULL
                TO (SF)-IMPB-TFR-ANTIC-NULL
           ELSE
              MOVE DFA-IMPB-TFR-ANTIC
                TO (SF)-IMPB-TFR-ANTIC
           END-IF
           IF DFA-IMPST-TFR-ANTIC-NULL = HIGH-VALUES
              MOVE DFA-IMPST-TFR-ANTIC-NULL
                TO (SF)-IMPST-TFR-ANTIC-NULL
           ELSE
              MOVE DFA-IMPST-TFR-ANTIC
                TO (SF)-IMPST-TFR-ANTIC
           END-IF
           IF DFA-RIDZ-TFR-SU-ANTIC-NULL = HIGH-VALUES
              MOVE DFA-RIDZ-TFR-SU-ANTIC-NULL
                TO (SF)-RIDZ-TFR-SU-ANTIC-NULL
           ELSE
              MOVE DFA-RIDZ-TFR-SU-ANTIC
                TO (SF)-RIDZ-TFR-SU-ANTIC
           END-IF
           IF DFA-IMPB-VIS-ANTIC-NULL = HIGH-VALUES
              MOVE DFA-IMPB-VIS-ANTIC-NULL
                TO (SF)-IMPB-VIS-ANTIC-NULL
           ELSE
              MOVE DFA-IMPB-VIS-ANTIC
                TO (SF)-IMPB-VIS-ANTIC
           END-IF
           IF DFA-IMPST-VIS-ANTIC-NULL = HIGH-VALUES
              MOVE DFA-IMPST-VIS-ANTIC-NULL
                TO (SF)-IMPST-VIS-ANTIC-NULL
           ELSE
              MOVE DFA-IMPST-VIS-ANTIC
                TO (SF)-IMPST-VIS-ANTIC
           END-IF
           IF DFA-IMPB-IS-K2-ANTIC-NULL = HIGH-VALUES
              MOVE DFA-IMPB-IS-K2-ANTIC-NULL
                TO (SF)-IMPB-IS-K2-ANTIC-NULL
           ELSE
              MOVE DFA-IMPB-IS-K2-ANTIC
                TO (SF)-IMPB-IS-K2-ANTIC
           END-IF
           IF DFA-IMPST-SOST-K2-ANTI-NULL = HIGH-VALUES
              MOVE DFA-IMPST-SOST-K2-ANTI-NULL
                TO (SF)-IMPST-SOST-K2-ANTI-NULL
           ELSE
              MOVE DFA-IMPST-SOST-K2-ANTI
                TO (SF)-IMPST-SOST-K2-ANTI
           END-IF
           IF DFA-ULT-COMMIS-TRASFE-NULL = HIGH-VALUES
              MOVE DFA-ULT-COMMIS-TRASFE-NULL
                TO (SF)-ULT-COMMIS-TRASFE-NULL
           ELSE
              MOVE DFA-ULT-COMMIS-TRASFE
                TO (SF)-ULT-COMMIS-TRASFE
           END-IF
           IF DFA-COD-DVS-NULL = HIGH-VALUES
              MOVE DFA-COD-DVS-NULL
                TO (SF)-COD-DVS-NULL
           ELSE
              MOVE DFA-COD-DVS
                TO (SF)-COD-DVS
           END-IF
           IF DFA-ALQ-PRVR-NULL = HIGH-VALUES
              MOVE DFA-ALQ-PRVR-NULL
                TO (SF)-ALQ-PRVR-NULL
           ELSE
              MOVE DFA-ALQ-PRVR
                TO (SF)-ALQ-PRVR
           END-IF
           IF DFA-PC-ESE-IMPST-TFR-NULL = HIGH-VALUES
              MOVE DFA-PC-ESE-IMPST-TFR-NULL
                TO (SF)-PC-ESE-IMPST-TFR-NULL
           ELSE
              MOVE DFA-PC-ESE-IMPST-TFR
                TO (SF)-PC-ESE-IMPST-TFR
           END-IF
           IF DFA-IMP-ESE-IMPST-TFR-NULL = HIGH-VALUES
              MOVE DFA-IMP-ESE-IMPST-TFR-NULL
                TO (SF)-IMP-ESE-IMPST-TFR-NULL
           ELSE
              MOVE DFA-IMP-ESE-IMPST-TFR
                TO (SF)-IMP-ESE-IMPST-TFR
           END-IF
           IF DFA-ANZ-CNBTVA-CARASS-NULL = HIGH-VALUES
              MOVE DFA-ANZ-CNBTVA-CARASS-NULL
                TO (SF)-ANZ-CNBTVA-CARASS-NULL
           ELSE
              MOVE DFA-ANZ-CNBTVA-CARASS
                TO (SF)-ANZ-CNBTVA-CARASS
           END-IF
           IF DFA-ANZ-CNBTVA-CARAZI-NULL = HIGH-VALUES
              MOVE DFA-ANZ-CNBTVA-CARAZI-NULL
                TO (SF)-ANZ-CNBTVA-CARAZI-NULL
           ELSE
              MOVE DFA-ANZ-CNBTVA-CARAZI
                TO (SF)-ANZ-CNBTVA-CARAZI
           END-IF
           IF DFA-ANZ-SRVZ-NULL = HIGH-VALUES
              MOVE DFA-ANZ-SRVZ-NULL
                TO (SF)-ANZ-SRVZ-NULL
           ELSE
              MOVE DFA-ANZ-SRVZ
                TO (SF)-ANZ-SRVZ
           END-IF
           IF DFA-IMP-CNBT-NDED-K1-NULL = HIGH-VALUES
              MOVE DFA-IMP-CNBT-NDED-K1-NULL
                TO (SF)-IMP-CNBT-NDED-K1-NULL
           ELSE
              MOVE DFA-IMP-CNBT-NDED-K1
                TO (SF)-IMP-CNBT-NDED-K1
           END-IF
           IF DFA-IMP-CNBT-NDED-K2-NULL = HIGH-VALUES
              MOVE DFA-IMP-CNBT-NDED-K2-NULL
                TO (SF)-IMP-CNBT-NDED-K2-NULL
           ELSE
              MOVE DFA-IMP-CNBT-NDED-K2
                TO (SF)-IMP-CNBT-NDED-K2
           END-IF
           IF DFA-IMP-CNBT-NDED-K3-NULL = HIGH-VALUES
              MOVE DFA-IMP-CNBT-NDED-K3-NULL
                TO (SF)-IMP-CNBT-NDED-K3-NULL
           ELSE
              MOVE DFA-IMP-CNBT-NDED-K3
                TO (SF)-IMP-CNBT-NDED-K3
           END-IF
           IF DFA-IMP-CNBT-AZ-K3-NULL = HIGH-VALUES
              MOVE DFA-IMP-CNBT-AZ-K3-NULL
                TO (SF)-IMP-CNBT-AZ-K3-NULL
           ELSE
              MOVE DFA-IMP-CNBT-AZ-K3
                TO (SF)-IMP-CNBT-AZ-K3
           END-IF
           IF DFA-IMP-CNBT-ISC-K3-NULL = HIGH-VALUES
              MOVE DFA-IMP-CNBT-ISC-K3-NULL
                TO (SF)-IMP-CNBT-ISC-K3-NULL
           ELSE
              MOVE DFA-IMP-CNBT-ISC-K3
                TO (SF)-IMP-CNBT-ISC-K3
           END-IF
           IF DFA-IMP-CNBT-TFR-K3-NULL = HIGH-VALUES
              MOVE DFA-IMP-CNBT-TFR-K3-NULL
                TO (SF)-IMP-CNBT-TFR-K3-NULL
           ELSE
              MOVE DFA-IMP-CNBT-TFR-K3
                TO (SF)-IMP-CNBT-TFR-K3
           END-IF
           IF DFA-IMP-CNBT-VOL-K3-NULL = HIGH-VALUES
              MOVE DFA-IMP-CNBT-VOL-K3-NULL
                TO (SF)-IMP-CNBT-VOL-K3-NULL
           ELSE
              MOVE DFA-IMP-CNBT-VOL-K3
                TO (SF)-IMP-CNBT-VOL-K3
           END-IF
           IF DFA-MATU-K3-NULL = HIGH-VALUES
              MOVE DFA-MATU-K3-NULL
                TO (SF)-MATU-K3-NULL
           ELSE
              MOVE DFA-MATU-K3
                TO (SF)-MATU-K3
           END-IF
           IF DFA-IMPB-252-ANTIC-NULL = HIGH-VALUES
              MOVE DFA-IMPB-252-ANTIC-NULL
                TO (SF)-IMPB-252-ANTIC-NULL
           ELSE
              MOVE DFA-IMPB-252-ANTIC
                TO (SF)-IMPB-252-ANTIC
           END-IF
           IF DFA-IMPST-252-ANTIC-NULL = HIGH-VALUES
              MOVE DFA-IMPST-252-ANTIC-NULL
                TO (SF)-IMPST-252-ANTIC-NULL
           ELSE
              MOVE DFA-IMPST-252-ANTIC
                TO (SF)-IMPST-252-ANTIC
           END-IF
           IF DFA-DT-1A-CNBZ-NULL = HIGH-VALUES
              MOVE DFA-DT-1A-CNBZ-NULL
                TO (SF)-DT-1A-CNBZ-NULL
           ELSE
              MOVE DFA-DT-1A-CNBZ
                TO (SF)-DT-1A-CNBZ
           END-IF
           IF DFA-COMMIS-DI-TRASFE-NULL = HIGH-VALUES
              MOVE DFA-COMMIS-DI-TRASFE-NULL
                TO (SF)-COMMIS-DI-TRASFE-NULL
           ELSE
              MOVE DFA-COMMIS-DI-TRASFE
                TO (SF)-COMMIS-DI-TRASFE
           END-IF
           IF DFA-AA-CNBZ-K1-NULL = HIGH-VALUES
              MOVE DFA-AA-CNBZ-K1-NULL
                TO (SF)-AA-CNBZ-K1-NULL
           ELSE
              MOVE DFA-AA-CNBZ-K1
                TO (SF)-AA-CNBZ-K1
           END-IF
           IF DFA-AA-CNBZ-K2-NULL = HIGH-VALUES
              MOVE DFA-AA-CNBZ-K2-NULL
                TO (SF)-AA-CNBZ-K2-NULL
           ELSE
              MOVE DFA-AA-CNBZ-K2
                TO (SF)-AA-CNBZ-K2
           END-IF
           IF DFA-AA-CNBZ-K3-NULL = HIGH-VALUES
              MOVE DFA-AA-CNBZ-K3-NULL
                TO (SF)-AA-CNBZ-K3-NULL
           ELSE
              MOVE DFA-AA-CNBZ-K3
                TO (SF)-AA-CNBZ-K3
           END-IF
           IF DFA-MM-CNBZ-K1-NULL = HIGH-VALUES
              MOVE DFA-MM-CNBZ-K1-NULL
                TO (SF)-MM-CNBZ-K1-NULL
           ELSE
              MOVE DFA-MM-CNBZ-K1
                TO (SF)-MM-CNBZ-K1
           END-IF
           IF DFA-MM-CNBZ-K2-NULL = HIGH-VALUES
              MOVE DFA-MM-CNBZ-K2-NULL
                TO (SF)-MM-CNBZ-K2-NULL
           ELSE
              MOVE DFA-MM-CNBZ-K2
                TO (SF)-MM-CNBZ-K2
           END-IF
           IF DFA-MM-CNBZ-K3-NULL = HIGH-VALUES
              MOVE DFA-MM-CNBZ-K3-NULL
                TO (SF)-MM-CNBZ-K3-NULL
           ELSE
              MOVE DFA-MM-CNBZ-K3
                TO (SF)-MM-CNBZ-K3
           END-IF
           IF DFA-FL-APPLZ-NEWFIS-NULL = HIGH-VALUES
              MOVE DFA-FL-APPLZ-NEWFIS-NULL
                TO (SF)-FL-APPLZ-NEWFIS-NULL
           ELSE
              MOVE DFA-FL-APPLZ-NEWFIS
                TO (SF)-FL-APPLZ-NEWFIS
           END-IF
           IF DFA-REDT-TASS-ABBAT-K3-NULL = HIGH-VALUES
              MOVE DFA-REDT-TASS-ABBAT-K3-NULL
                TO (SF)-REDT-TASS-ABBAT-K3-NULL
           ELSE
              MOVE DFA-REDT-TASS-ABBAT-K3
                TO (SF)-REDT-TASS-ABBAT-K3
           END-IF
           IF DFA-CNBT-ECC-4X100-K1-NULL = HIGH-VALUES
              MOVE DFA-CNBT-ECC-4X100-K1-NULL
                TO (SF)-CNBT-ECC-4X100-K1-NULL
           ELSE
              MOVE DFA-CNBT-ECC-4X100-K1
                TO (SF)-CNBT-ECC-4X100-K1
           END-IF
           MOVE DFA-DS-RIGA
             TO (SF)-DS-RIGA
           MOVE DFA-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE DFA-DS-VER
             TO (SF)-DS-VER
           MOVE DFA-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ
           MOVE DFA-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ
           MOVE DFA-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE DFA-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           IF DFA-CREDITO-IS-NULL = HIGH-VALUES
              MOVE DFA-CREDITO-IS-NULL
                TO (SF)-CREDITO-IS-NULL
           ELSE
              MOVE DFA-CREDITO-IS
                TO (SF)-CREDITO-IS
           END-IF
           IF DFA-REDT-TASS-ABBAT-K2-NULL = HIGH-VALUES
              MOVE DFA-REDT-TASS-ABBAT-K2-NULL
                TO (SF)-REDT-TASS-ABBAT-K2-NULL
           ELSE
              MOVE DFA-REDT-TASS-ABBAT-K2
                TO (SF)-REDT-TASS-ABBAT-K2
           END-IF
           IF DFA-IMPB-IS-K3-NULL = HIGH-VALUES
              MOVE DFA-IMPB-IS-K3-NULL
                TO (SF)-IMPB-IS-K3-NULL
           ELSE
              MOVE DFA-IMPB-IS-K3
                TO (SF)-IMPB-IS-K3
           END-IF
           IF DFA-IMPST-SOST-K3-NULL = HIGH-VALUES
              MOVE DFA-IMPST-SOST-K3-NULL
                TO (SF)-IMPST-SOST-K3-NULL
           ELSE
              MOVE DFA-IMPST-SOST-K3
                TO (SF)-IMPST-SOST-K3
           END-IF
           IF DFA-IMPB-252-K3-NULL = HIGH-VALUES
              MOVE DFA-IMPB-252-K3-NULL
                TO (SF)-IMPB-252-K3-NULL
           ELSE
              MOVE DFA-IMPB-252-K3
                TO (SF)-IMPB-252-K3
           END-IF
           IF DFA-IMPST-252-K3-NULL = HIGH-VALUES
              MOVE DFA-IMPST-252-K3-NULL
                TO (SF)-IMPST-252-K3-NULL
           ELSE
              MOVE DFA-IMPST-252-K3
                TO (SF)-IMPST-252-K3
           END-IF
           IF DFA-IMPB-IS-K3-ANTIC-NULL = HIGH-VALUES
              MOVE DFA-IMPB-IS-K3-ANTIC-NULL
                TO (SF)-IMPB-IS-K3-ANTIC-NULL
           ELSE
              MOVE DFA-IMPB-IS-K3-ANTIC
                TO (SF)-IMPB-IS-K3-ANTIC
           END-IF
           IF DFA-IMPST-SOST-K3-ANTI-NULL = HIGH-VALUES
              MOVE DFA-IMPST-SOST-K3-ANTI-NULL
                TO (SF)-IMPST-SOST-K3-ANTI-NULL
           ELSE
              MOVE DFA-IMPST-SOST-K3-ANTI
                TO (SF)-IMPST-SOST-K3-ANTI
           END-IF
           IF DFA-IMPB-IRPEF-K1-ANTI-NULL = HIGH-VALUES
              MOVE DFA-IMPB-IRPEF-K1-ANTI-NULL
                TO (SF)-IMPB-IRPEF-K1-ANTI-NULL
           ELSE
              MOVE DFA-IMPB-IRPEF-K1-ANTI
                TO (SF)-IMPB-IRPEF-K1-ANTI
           END-IF
           IF DFA-IMPST-IRPEF-K1-ANT-NULL = HIGH-VALUES
              MOVE DFA-IMPST-IRPEF-K1-ANT-NULL
                TO (SF)-IMPST-IRPEF-K1-ANT-NULL
           ELSE
              MOVE DFA-IMPST-IRPEF-K1-ANT
                TO (SF)-IMPST-IRPEF-K1-ANT
           END-IF
           IF DFA-IMPB-IRPEF-K2-ANTI-NULL = HIGH-VALUES
              MOVE DFA-IMPB-IRPEF-K2-ANTI-NULL
                TO (SF)-IMPB-IRPEF-K2-ANTI-NULL
           ELSE
              MOVE DFA-IMPB-IRPEF-K2-ANTI
                TO (SF)-IMPB-IRPEF-K2-ANTI
           END-IF
           IF DFA-IMPST-IRPEF-K2-ANT-NULL = HIGH-VALUES
              MOVE DFA-IMPST-IRPEF-K2-ANT-NULL
                TO (SF)-IMPST-IRPEF-K2-ANT-NULL
           ELSE
              MOVE DFA-IMPST-IRPEF-K2-ANT
                TO (SF)-IMPST-IRPEF-K2-ANT
           END-IF
           IF DFA-DT-CESSAZIONE-NULL = HIGH-VALUES
              MOVE DFA-DT-CESSAZIONE-NULL
                TO (SF)-DT-CESSAZIONE-NULL
           ELSE
              MOVE DFA-DT-CESSAZIONE
                TO (SF)-DT-CESSAZIONE
           END-IF
           IF DFA-TOT-IMPST-NULL = HIGH-VALUES
              MOVE DFA-TOT-IMPST-NULL
                TO (SF)-TOT-IMPST-NULL
           ELSE
              MOVE DFA-TOT-IMPST
                TO (SF)-TOT-IMPST
           END-IF
           IF DFA-ONER-TRASFE-NULL = HIGH-VALUES
              MOVE DFA-ONER-TRASFE-NULL
                TO (SF)-ONER-TRASFE-NULL
           ELSE
              MOVE DFA-ONER-TRASFE
                TO (SF)-ONER-TRASFE
           END-IF
           IF DFA-IMP-NET-TRASFERITO-NULL = HIGH-VALUES
              MOVE DFA-IMP-NET-TRASFERITO-NULL
                TO (SF)-IMP-NET-TRASFERITO-NULL
           ELSE
              MOVE DFA-IMP-NET-TRASFERITO
                TO (SF)-IMP-NET-TRASFERITO
           END-IF.
       VALORIZZA-OUTPUT-DFA-EX.
           EXIT.

      ******************************************************************
      *    TP_MOVI (DA 2000 A 2999)
      ******************************************************************
           88 VARIA-CONTRA                            VALUE 2001.
           88 VARIA-PIANOV                            VALUE 2002.
           88 SOSPE-PIANOV                            VALUE 2003.
           88 RIPRE-PIANOV                            VALUE 2004.
           88 VARIA-RECAPI                            VALUE 2006.
           88 VARIA-STRAIN                            VALUE 2005.
           88 ASSOC-CONVEN                            VALUE 2007.
           88 ESCLU-CONVEN                            VALUE 2008.
           88 VARIA-CONVEN                            VALUE 2009.
           88 VARIA-BENEFI                            VALUE 2010.
           88 VARIA-MODALI                            VALUE 2011.
           88 VARIA-COORDI                            VALUE 2013.
           88 ATTIV-VINCOL                            VALUE 2015.
           88 CESSA-VINCOL                            VALUE 2016.
           88 CESSA-IPEGNO                            VALUE 2017.
           88 CESSA-CPEGNO                            VALUE 2018.
           88 VARIA-PIARIS                            VALUE 2019.
           88 SOSPE-PIARIS                            VALUE 2020.
           88 RIPRE-PIARIS                            VALUE 2021.
           88 INCLU-SOVRAP                            VALUE 2022.
           88 VARIA-SOVRAP                            VALUE 2023.
           88 ESCLU-SOVRAP                            VALUE 2024.
           88 VARIA-TRIACC                            VALUE 2025.
           88 VARIA-CONTAR                            VALUE 2026.
           88 VARIA-CEDOLA                            VALUE 2027.
           88 COMUN-SWITCH-FND                        VALUE 2028.
           88 VARIA-ADEGUA                            VALUE 2029.
           88 VARIA-PARFAT                            VALUE 2030.
           88 VARIA-DATGEN                            VALUE 2031.
           88 VARIA-CLAUSO                            VALUE 2032.
           88 VARIA-BENEF-DFLT                        VALUE 2033.
           88 VARIA-CONTRA-DECES                      VALUE 2034.
           88 VARIA-ETA-COMPU                         VALUE 2035.
           88 FORZA-DT-VALUTA                         VALUE 2036.
           88 VARIA-PREN-TRASF-AGE                    VALUE 2038.
      * CENSITO UN NUOVO TIPO MOVIMENTO
           88 VARIA-BENEF-DFLT-ADES                   VALUE 2039.
      *
           88 RETTIFICA-ANAGRAFICA                    VALUE 2042.
           88 VARIA-PERCIPENTI-DISPOSTI               VALUE 2043.
           88 RETT-MOV-IN-QUOTE                       VALUE 2044.
           88 INTERESSI-DI-MORA                       VALUE 2045.
           88 ATT-SERVIZIO-BIG-CHANCE                 VALUE 2048.
           88 VAR-SERVIZIO-BIG-CHANCE                 VALUE 2049.
           88 DIS-SERVIZIO-BIG-CHANCE                 VALUE 2050.
           88 ATT-SERVIZIO-CONS-RENDI                 VALUE 2051.
           88 DIS-SERVIZIO-CONS-RENDI                 VALUE 2052.
           88 VAR-RIPART-PREMI                        VALUE 2053.
           88 AMPLIA-POLI                             VALUE 2055.
           88 VAR-DT-CRTF-FSC                         VALUE 2056.
           88 GEST-COORDINATE-BENEF-SCAD              VALUE 2057.
           88 ATTIVAZ-RISC-PARZ-PROGRAMMATI           VALUE 2058.
           88 QUICK-PORT-PARZ                         VALUE 2061.
           88 QUICK-PORT-TOT                          VALUE 2062.
           88 BATCH-MASSIVO                           VALUE 2063.
           88 ESCLUS-BATCH-MASSIVO                    VALUE 2064.
           88 ANNULL-ESCLUS-BATCH-MASSIVO             VALUE 2065.
           88 PRENOTAZ-BATCH-SW-MASSIVO               VALUE 2066.
CR9960     88 RIBILANCIAMENTO-AUTOMATICO              VALUE 2067.
CR9960     88 RIBILANCIAMENTO-PERIODICO               VALUE 2068.
CR9960     88 AGG-AST-ALLOC-RIBIL                     VALUE 2069.
10818      88 SWITCH-LINEE-INVST                      VALUE 2071.
12908      88 SWITCH-MASSIVO-PARZIALE                 VALUE 2072.
13382      88 VARIA-TIT-EFF                           VALUE 2074.
FNZ        88 SWITCH-RIBILANCIAMENTO-IFP              VALUE 2075.
FNZ        88 SWITCH-GAP-EVENT                        VALUE 2076.
FNZ        88 SWITCH-SUP-SOGLIA                       VALUE 2077.
FNZ        88 SWITCH-MASSIVO-FNZ                      VALUE 2078.
FNZ        88 BLOCCO-DISP-GAP-EVENT                   VALUE 2079.
FNZ        88 BLOCCO-DISP-GEST-SCARTI-FNZ             VALUE 2080.
FNZ        88 RECUP-STRATEGIE-NON-PROT                VALUE 2081.
           88 GESTI-PRESTI                            VALUE 2101.
           88 RIMBO-PRESTI                            VALUE 2102.
11236      88 CALC-COSTO-OPER                         VALUE 2128.
           88 VARIA-ASCADE                            VALUE 2201.
           88 VARIA-DIFFER                            VALUE 2202.
           88 VARIA-PROROG                            VALUE 2203.
           88 VARIA-CONVCA                            VALUE 2204.
           88 VARIA-TABAGI                            VALUE 2205.
           88 VARIA-SUBAGE-PROD                       VALUE 2206.
           88 VARIA-SUBAGE-PROD-MASS                  VALUE 2207.
           88 VARIA-FONDO-SPEC                        VALUE 2208.
           88 TRASFER-AGENZIA                         VALUE 2209.
           88 VARIA-ETA-SCAD                          VALUE 2210.
           88 PREN-VARIA-ETA-SCAD                     VALUE 2214.
           88 TRASFER-TESTA-POS                       VALUE 2211.
           88 VARIA-CONVRE                            VALUE 2212.
           88 VARIAZIONE-RID                          VALUE 2213.
           88 VAR-DATI-TRASF                          VALUE 2215.
           88 INS-CNBT-NDED                           VALUE 2216.
           88 VAR-CNBT-NDED                           VALUE 2217.
           88 CAN-CNBT-NDED                           VALUE 2218.
           88 PRENOT-CNBT-NDED                        VALUE 2219.
           88 VARIA-DT-PRESCR                         VALUE 2220.
           88 VARIA-PAGATORE                          VALUE 2221.
           88 ATTIV-FONTI-CONTRB                      VALUE 2222.
           88 ASSOC-MATR-AGENTE                       VALUE 2223.
           88 VARIA-MATR-AGENTE                       VALUE 2224.
           88 ESCLU-MATR-AGENTE                       VALUE 2225.
           88 VARIA-LEG-RAPPR                         VALUE 2226.
           88 POL-FREDM-SLG-CC                        VALUE 2227.
           88 RIATTIVAZ-SERVIZIO-FREEDOM              VALUE 2229.
           88 VARIA-PAGRID                            VALUE 2232.
           88 VARIA-TITOLARE-EFFETTIVO                VALUE 2236.
           88 PROVV-3-IVASS                           VALUE 2237.
           88 AGG-ANAG-NAIS-TP-DATI-DOC               VALUE 2240.
F3ASS      88 INCL-ASSICURATO                         VALUE 2241.
F3ASS      88 ESCL-ASSICURATO                         VALUE 2242.
TOP        88 ATTIVA-REDDITO-PROGRAMMATO              VALUE 2301.
TOP        88 ATTIVA-TAKE-PROFIT                      VALUE 2302.
TOP        88 ATTIVA-STOP-LOSS                        VALUE 2303.
TOP        88 ATTIVA-PASSO-PASSO                      VALUE 2304.
TOP        88 ATTIVA-BENEFICIO-CONTR                  VALUE 2305.
TOP        88 VARIAZ-REDDITO-PROGRAMMATO              VALUE 2306.
TOP        88 VARIAZ-TAKE-PROFIT                      VALUE 2307.
TOP        88 VARIAZ-STOP-LOSS                        VALUE 2308.
TOP        88 VARIAZ-PASSO-PASSO                      VALUE 2309.
TOP        88 VARIAZ-BENEFICIO-CONTR                  VALUE 2310.
TOP        88 CESSAZ-REDDITO-PROGRAMMATO              VALUE 2311.
TOP        88 CESSAZ-TAKE-PROFIT                      VALUE 2312.
TOP        88 CESSAZ-STOP-LOSS                        VALUE 2313.
TOP        88 CESSAZ-PASSO-PASSO                      VALUE 2314.
TOP        88 CESSAZ-BENEFICIO-CONTR                  VALUE 2315.
TOP        88 RPP-REDDITO-PROGRAMMATO                 VALUE 2316.
TOP        88 LIQUI-RPP-REDDITO-PROGR                 VALUE 2317.
TOP        88 RPP-TAKE-PROFIT                         VALUE 2318.
TOP        88 LIQUI-RPP-TAKE-PROFIT                   VALUE 2319.
TOP        88 SW-STOP-LOSS                            VALUE 2320.
TOP        88 SW-PASSO-PASSO                          VALUE 2321.
TOP        88 RPP-BENEFICIO-CONTR                     VALUE 2322.
TOP        88 LIQUI-RPP-BENEFICIO-CONTR               VALUE 2323.
FNZ   * -- TOTALE PER INCAPIENZA DA REDDITO PROGRAMMATO
TOP        88 COMUN-RISTOT-INCAPIENZA                 VALUE 2324.
TOP        88 LIQUI-RISTOT-INCAPIENZA                 VALUE 2325.
FNZ   * --
TOP        88 SW-PASSO-PASSO-NON-EROG                 VALUE 2326.
TOP        88 RPP-REDDITO-PROG-NON-EROG               VALUE 2327.
TOP        88 CALC-VAL-RISC-RPP                       VALUE 2328.
TOP        88 CALC-VAL-RISC-TAKE-PROFIT               VALUE 2329.
TOP        88 RPP-TAKE-PROFIT-NON-EROG                VALUE 2330.
13382      88 VARIAZ-PERC-CEDOLA                      VALUE 2331.
FNZ   * -- TOTALE PER INCAPIENZA DA RICATTO PARZIALE
FNZ        88 COMUN-RISTOT-INCAP                      VALUE 2332.
FNZ        88 LIQUI-RISTOT-INCAP                      VALUE 2333.
MIGOL *    88 VARIA-MODALI-COLL                       VALUE 2334.
MIGOL *    88 VARIA-COORDI-COLL                       VALUE 2335.
MIGOL      88 PRENOT-APPENDICE-PRESTAZ                VALUE 2336.
MIGOL      88 PRENOT-REPORT-NOMINATIVO                VALUE 2337.
FNZ   * --
FNZRIB     88 COMUN-PERIOD-SWITCH                     VALUE 2338.
MIGOL      88 VARIA-MODALI-COLL                       VALUE 2339.
MIGOL      88 VARIA-COORDI-COLL                       VALUE 2340.


       01  LDBVE261.
          04 LDBVE261-TP-TRCH.
             05 LDBVE261-TP-TRCH-1         PIC X(02).
             05 LDBVE261-TP-TRCH-2         PIC X(02).
             05 LDBVE261-TP-TRCH-3         PIC X(02).
             05 LDBVE261-TP-TRCH-4         PIC X(02).
             05 LDBVE261-TP-TRCH-5         PIC X(02).
             05 LDBVE261-TP-TRCH-6         PIC X(02).
             05 LDBVE261-TP-TRCH-7         PIC X(02).
             05 LDBVE261-TP-TRCH-8         PIC X(02).
             05 LDBVE261-TP-TRCH-9         PIC X(02).
             05 LDBVE261-TP-TRCH-10        PIC X(02).
             05 LDBVE261-TP-TRCH-11        PIC X(02).
             05 LDBVE261-TP-TRCH-12        PIC X(02).
             05 LDBVE261-TP-TRCH-13        PIC X(02).
             05 LDBVE261-TP-TRCH-14        PIC X(02).
             05 LDBVE261-TP-TRCH-15        PIC X(02).
             05 LDBVE261-TP-TRCH-16        PIC X(02).
          04 LDBVE261-ID-ADES              PIC S9(09) COMP-3.
          04 LDBVE261-ID-MOVI              PIC S9(09) COMP-3.
          04 LDBVE261-IMPB-VIS-END2000     PIC S9(12)V9(3) COMP-3.


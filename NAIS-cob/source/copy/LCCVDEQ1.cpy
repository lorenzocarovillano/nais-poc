      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA DETT_QUEST
      *   ALIAS DEQ
      *   ULTIMO AGG. 03 GIU 2019
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-DETT-QUEST PIC S9(9)     COMP-3.
             07 (SF)-ID-QUEST PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-QUEST PIC X(20).
             07 (SF)-COD-QUEST-NULL REDEFINES
                (SF)-COD-QUEST   PIC X(20).
             07 (SF)-COD-DOM PIC X(20).
             07 (SF)-COD-DOM-NULL REDEFINES
                (SF)-COD-DOM   PIC X(20).
             07 (SF)-COD-DOM-COLLG PIC X(20).
             07 (SF)-COD-DOM-COLLG-NULL REDEFINES
                (SF)-COD-DOM-COLLG   PIC X(20).
             07 (SF)-RISP-NUM PIC S9(5)     COMP-3.
             07 (SF)-RISP-NUM-NULL REDEFINES
                (SF)-RISP-NUM   PIC X(3).
             07 (SF)-RISP-FL PIC X(1).
             07 (SF)-RISP-FL-NULL REDEFINES
                (SF)-RISP-FL   PIC X(1).
             07 (SF)-RISP-TXT PIC X(100).
             07 (SF)-RISP-TS PIC S9(5)V9(9) COMP-3.
             07 (SF)-RISP-TS-NULL REDEFINES
                (SF)-RISP-TS   PIC X(8).
             07 (SF)-RISP-IMP PIC S9(12)V9(3) COMP-3.
             07 (SF)-RISP-IMP-NULL REDEFINES
                (SF)-RISP-IMP   PIC X(8).
             07 (SF)-RISP-PC PIC S9(3)V9(3) COMP-3.
             07 (SF)-RISP-PC-NULL REDEFINES
                (SF)-RISP-PC   PIC X(4).
             07 (SF)-RISP-DT   PIC S9(8) COMP-3.
             07 (SF)-RISP-DT-NULL REDEFINES
                (SF)-RISP-DT   PIC X(5).
             07 (SF)-RISP-KEY PIC X(20).
             07 (SF)-RISP-KEY-NULL REDEFINES
                (SF)-RISP-KEY   PIC X(20).
             07 (SF)-TP-RISP PIC X(2).
             07 (SF)-TP-RISP-NULL REDEFINES
                (SF)-TP-RISP   PIC X(2).
             07 (SF)-ID-COMP-QUEST PIC S9(9)     COMP-3.
             07 (SF)-ID-COMP-QUEST-NULL REDEFINES
                (SF)-ID-COMP-QUEST   PIC X(5).
             07 (SF)-VAL-RISP PIC X(250).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).

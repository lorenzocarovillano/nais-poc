      *----------------------------------------------------------------*
      * OPERAZIONI INIZIALI                                            *
      *----------------------------------------------------------------*
       A000-OPERAZ-INIZ.
      *
           MOVE 'A000-OPERAZ-INIZ'          TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM A050-INITIALIZE          THRU A050-EX.

           PERFORM T000-ACCEPT-TIMESTAMP    THRU T000-EX.

           PERFORM A001-OPERAZIONI-INIZIALI THRU A001-EX.

       A000-EX.
           EXIT.
      *----------------------------------------------------------------*
      * INIZIALIZZA AREE DI WORKING                                    *
      *----------------------------------------------------------------*
       A050-INITIALIZE.
      *
           MOVE 'A050-INITIALIZE'           TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           INITIALIZE FS-PARA
                      TABELLA-STATI-BATCH
                      TABELLA-STATI-JOB
                      IABV0002
                      CNT-NUM-CONTR-LETT
                      CNT-NUM-CONTR-ELAB
                      CNT-NUM-OPERAZ-LETTE
                      WK-STATI-SOSPESI-STR
                      WK-COMODO
                      VALORI-CAMPI-LOG-ERRORE-REPORT
                      IABI0011
                      W-AREA-PER-CHIAMATE-WSMQ
                      IDSV0003.

           MOVE HIGH-VALUE                   TO BTC-PARALLELISM

           SET IDSV0001-BATCH-INFR           TO TRUE
           SET WK-BAT-COLLECTION-KEY-OK      TO TRUE
           SET WK-JOB-COLLECTION-KEY-OK      TO TRUE
           SET WK-ERRORE-NO                  TO TRUE
           SET WK-ERRORE-BLOCCANTE           TO TRUE

           SET WK-FINE-BTC-BATCH-NO          TO TRUE

           SET INIT-COMMIT-FREQUENCY         TO TRUE

           SET IABV0006-COMMIT-NO            TO TRUE
           SET WK-LANCIA-BUSINESS-YES        TO TRUE
           SET WK-LANCIA-ROTTURA-NO          TO TRUE
           SET WK-FINE-ALIMENTAZIONE-NO      TO TRUE
           SET GESTIONE-PARALLELISMO-NO      TO TRUE
           SET BATCH-COMPLETE                TO TRUE
           SET BATCH-STATE-OK                TO TRUE

           SET WK-ALLINEA-STATI-OK           TO TRUE
           SET GESTIONE-REAL-TIME            TO TRUE

           SET POST-GUIDE-X-JOB-NOT-FOUND-NO TO TRUE

           SET PRIMA-VOLTA-YES               TO TRUE
           SET IABV0006-PRIMO-LANCIO         TO TRUE

           SET STAMPA-TESTATA-JCL-YES        TO TRUE
           SET STAMPA-LOGO-ERRORE-YES        TO TRUE

           SET IDSV0001-NO-DEBUG             TO TRUE

           SET WK-VERIFICA-PRENOTAZIONE-OK   TO TRUE
           SET WK-VERIFICA-BATCH-KO          TO TRUE
           SET WK-ELABORA-BATCH-NO           TO TRUE
           SET WK-AGGIORNA-PRENOTAZ-YES      TO TRUE

           SET BATCH-EXECUTOR-STD-NO         TO TRUE

           SET IDSV0003-SUCCESSFUL-RC        TO TRUE
           SET IDSV0003-SUCCESSFUL-SQL       TO TRUE


           MOVE SPACES               TO IDSV0003-NOME-TABELLA
                                        WK-COD-BATCH-STATE
                                        WK-COD-ELAB-STATE
                                        WK-DES-BATCH-STATE
                                        WK-STATE-CURRENT.

           MOVE ZEROES               TO WK-SQLCODE
                                        IDSV0003-NUM-RIGHE-LETTE
                                        IND-STATI-SOSP
                                        WK-VERSIONING
                                        IABV0009-VERSIONING
                                        CONT-TAB-GUIDE-LETTE
                                        CONT-BUS-SERV-ESEG
                                        CONT-BUS-SERV-ESITO-OK
                                        CONT-BUS-SERV-WARNING
                                        CONT-BUS-SERV-ESITO-KO.

           MOVE SPACES               TO IDSV0003-DESCRIZ-ERR-DB2
                                        IDSV0003-KEY-TABELLA.

           SET  IABV0006-GEST-APPL-ROTTURA-IBO  TO TRUE.

           PERFORM I000-INIT-CUSTOM-COUNT THRU I000-EX.


      *----------------------------------------------------------------*
      *    initializzazione routine degli errori
      *----------------------------------------------------------------*

           COPY IERP0001.
           COPY IERP0004.
      *
       A050-EX.
           EXIT.
      *----------------------------------------------------------------*
      * INIZIO ELABORAZIONE                                            *
      *----------------------------------------------------------------*
       A100-INIZIO-ELABORA.
      *
           MOVE 'A100-INIZIO-ELABORA'       TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE WK-PGM             TO IDSV0001-COD-MAIN-BATCH

           PERFORM A101-DISPLAY-INIZIO-ELABORA THRU A101-EX.

      *
       A100-EX.
           EXIT.
      *----------------------------------------------------------------*
      * LETTURA DELLA TABELLA PARAM_INFR_APPL
      *----------------------------------------------------------------*
       A103-LEGGI-PARAM-INFR-APPL.
      *
           MOVE 'A103-LEGGI-PARAM-INFR-APPL' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO   TO TRUE
           MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM A104-CALL-LDBS6730  THRU A104-EX.

       A103-EX.
           EXIT.

       A104-CALL-LDBS6730.

           INITIALIZE PARAM-INFR-APPL
      *               IJCCMQ04-VAR-AMBIENTE
                      IDSV0003.

           SET  IDSV0003-SELECT           TO TRUE.
           SET  IDSV0003-WHERE-CONDITION  TO TRUE.
           SET  IDSV0003-TRATT-SENZA-STOR TO TRUE.

           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

           MOVE 'LDBS6730'                TO IDSV0003-COD-MAIN-BATCH.

           CALL IDSV0003-COD-MAIN-BATCH   USING  IDSV0003
                                                 PARAM-INFR-APPL.

           IF IDSV0003-SUCCESSFUL-RC
              EVALUATE TRUE
                WHEN IDSV0003-SUCCESSFUL-SQL

                   PERFORM A106-VALORIZZA-OUTPUT-PIA
                      THRU A106-EX

                WHEN IDSV0003-NOT-FOUND
                     INITIALIZE PARAM-INFR-APPL
      *               INITIALIZE WK-LOG-ERRORE
      *               MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
      *               STRING 'TABELLA PARAM_INFR_APPL VUOTA'
      *                       ' - '
      *                       'SQLCODE : '
      *                       WK-SQLCODE
      *                       DELIMITED BY SIZE INTO
      *                       WK-LOR-DESC-ERRORE-ESTESA
      *               END-STRING
      *               MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
      *               PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                WHEN OTHER
                     INITIALIZE WK-LOG-ERRORE
                     MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
                     STRING 'ERR.LETT. PARAM_INFR_APPL '
                            ' RC ' IDSV0003-RETURN-CODE
                             'SQLCODE : '
                             WK-SQLCODE
                             DELIMITED BY SIZE INTO
                             WK-LOR-DESC-ERRORE-ESTESA
                     END-STRING
                     MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                     PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-EVALUATE
           ELSE
              INITIALIZE WK-LOG-ERRORE
              MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
              STRING 'ERRORE CHIAMATA LDBS6730 '
                     ' RC ' IDSV0003-RETURN-CODE
                      'SQLCODE : '
                      WK-SQLCODE
                      DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
              END-STRING
              MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
           END-IF.

       A104-EX.
           EXIT.
      *---------------------------------------------------------------*
      *      VALORIZZAZIONE TRACCIATO DI OUTPUT
      *---------------------------------------------------------------*
       A106-VALORIZZA-OUTPUT-PIA.

           MOVE D09-COD-COMP-ANIA       TO IJCCMQ04-COD-COMP-ANIA.

           MOVE D09-AMBIENTE            TO IJCCMQ04-AMBIENTE.
           MOVE D09-PIATTAFORMA         TO IJCCMQ04-PIATTAFORMA.

           MOVE D09-TP-COM-COBOL-JAVA   TO IJCCMQ04-COB-TO-JAVA.
           IF NOT ( IJCCMQ04-MQ        OR
                    IJCCMQ04-CSOCKET   OR
                    IJCCMQ04-JCICS-COO )
              INITIALIZE WK-LOG-ERRORE
              MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
              STRING 'PARAM_INFR_APPL - COMUNIC. NON VALIDA : '
                      IJCCMQ04-COB-TO-JAVA
              DELIMITED BY SIZE   INTO WK-LOR-DESC-ERRORE-ESTESA
              END-STRING
              MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
           END-IF.

           IF D09-MQ-TP-UTILIZZO-API-NULL = HIGH-VALUES
              MOVE SPACES                  TO IJCCMQ04-TP-UTILIZZO-API
           ELSE
              MOVE D09-MQ-TP-UTILIZZO-API   TO IJCCMQ04-TP-UTILIZZO-API
              IF  NOT ( IJCCMQ04-CALL-COBOL OR
                        IJCCMQ04-CALL-C )
                INITIALIZE WK-LOG-ERRORE
                MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
                STRING 'PARAM_INFR_APPL - TP UTILIZZO API NON VALIDO : '
                         IJCCMQ04-TP-UTILIZZO-API
                DELIMITED BY SIZE   INTO WK-LOR-DESC-ERRORE-ESTESA
                END-STRING
                MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                END-IF
           END-IF

           IF D09-MQ-QUEUE-MANAGER-NULL = HIGH-VALUES
              MOVE SPACES                  TO IJCCMQ04-QUEUE-MANAGER
           ELSE
              MOVE D09-MQ-QUEUE-MANAGER    TO IJCCMQ04-QUEUE-MANAGER
           END-IF

           IF D09-MQ-CODA-PUT-NULL = HIGH-VALUES
              MOVE SPACES                  TO IJCCMQ04-CODA-PUT
           ELSE
              MOVE D09-MQ-CODA-PUT         TO IJCCMQ04-CODA-PUT
           END-IF

           IF D09-MQ-CODA-GET-NULL = HIGH-VALUES
              MOVE SPACES                  TO IJCCMQ04-CODA-GET
           ELSE
              MOVE D09-MQ-CODA-GET         TO IJCCMQ04-CODA-GET
           END-IF

           IF D09-MQ-OPZ-PERSISTENZA-NULL = HIGH-VALUES
              MOVE SPACE                   TO IJCCMQ04-OPZ-PERSISTENZA
           ELSE
              MOVE D09-MQ-OPZ-PERSISTENZA  TO IJCCMQ04-OPZ-PERSISTENZA
              IF NOT IJCCMQ04-OP-VALIDA
                INITIALIZE WK-LOG-ERRORE
                MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
                STRING 'PARAM_INFR_APPL - OPZ PERSISTENZA NON VALIDA : '
                       IJCCMQ04-OPZ-PERSISTENZA
                DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
                END-STRING
                MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF

           IF D09-MQ-ATTESA-RISPOSTA-NULL = HIGH-VALUES
              MOVE SPACE                   TO IJCCMQ04-ATTESA-RISPOSTA
           ELSE
              MOVE D09-MQ-ATTESA-RISPOSTA  TO IJCCMQ04-ATTESA-RISPOSTA
              IF  NOT IJCCMQ04-AR-VALIDA
                INITIALIZE WK-LOG-ERRORE
                MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
                STRING 'PARAM_INFR_APPL - ATTESA RISPOSTA NON VALIDA : '
                         IJCCMQ04-ATTESA-RISPOSTA
                  DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
                END-STRING
                MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF

           IF D09-MQ-OPZ-WAIT-NULL = HIGH-VALUES
              MOVE SPACE                   TO IJCCMQ04-OPZ-WAIT
           ELSE
              MOVE D09-MQ-OPZ-WAIT         TO IJCCMQ04-OPZ-WAIT
              IF  NOT IJCCMQ04-OW-VALIDA
                  INITIALIZE WK-LOG-ERRORE
                  MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
                  STRING 'PARAM_INFR_APPL - OPZ WAIT NON VALIDA : '
                           IJCCMQ04-OPZ-WAIT
                  DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
                  END-STRING
                  MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                  PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF

           IF D09-MQ-OPZ-SYNCPOINT-NULL = HIGH-VALUES
              MOVE SPACE                   TO IJCCMQ04-OPZ-SYNCPOINT
           ELSE
              MOVE D09-MQ-OPZ-SYNCPOINT    TO IJCCMQ04-OPZ-SYNCPOINT
              IF NOT IJCCMQ04-OS-VALIDA
                 INITIALIZE WK-LOG-ERRORE
                 MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
                 STRING 'PARAM_INFR_APPL - OPZ SYNCPOINT NON VALIDA : '
                         IJCCMQ04-OPZ-SYNCPOINT
                 DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING
                 MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF

           IF D09-MQ-TEMPO-ATTESA-1-NULL = HIGH-VALUES
              MOVE ZERO                    TO IJCCMQ04-TEMPO-ATTESA-1
           ELSE
              MOVE D09-MQ-TEMPO-ATTESA-1   TO IJCCMQ04-TEMPO-ATTESA-1
           END-IF

           IF D09-MQ-TEMPO-ATTESA-2-NULL = HIGH-VALUES
              MOVE ZERO                    TO IJCCMQ04-TEMPO-ATTESA-2
           ELSE
              MOVE D09-MQ-TEMPO-ATTESA-2   TO IJCCMQ04-TEMPO-ATTESA-2
           END-IF

           IF D09-MQ-TEMPO-EXPIRY-NULL = HIGH-VALUES
              MOVE ZERO                 TO IJCCMQ04-TEMPO-EXPIRY
           ELSE
              MOVE D09-MQ-TEMPO-EXPIRY  TO IJCCMQ04-TEMPO-EXPIRY
           END-IF

           IF D09-CSOCKET-IP-ADDRESS-NULL = HIGH-VALUES
              MOVE SPACES                 TO IJCCMQ04-CSOCKET-IP-ADDRESS
           ELSE
              MOVE D09-CSOCKET-IP-ADDRESS TO IJCCMQ04-CSOCKET-IP-ADDRESS
           END-IF

           IF D09-CSOCKET-PORT-NUM-NULL = HIGH-VALUES
              MOVE ZERO                  TO IJCCMQ04-CSOCKET-PORT-NUM
           ELSE
              MOVE D09-CSOCKET-PORT-NUM  TO IJCCMQ04-CSOCKET-PORT-NUM
           END-IF

           IF D09-FL-COMPRESSORE-C-NULL = HIGH-VALUES
              MOVE SPACES                TO IJCCMQ04-FL-COMPRESSORE-C
           ELSE
              MOVE D09-FL-COMPRESSORE-C  TO IJCCMQ04-FL-COMPRESSORE-C
              IF  NOT ( IJCCMQ04-FL-COMPRESSORE-C-SI OR
                        IJCCMQ04-FL-COMPRESSORE-C-NO )
                 INITIALIZE WK-LOG-ERRORE
                 MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
                 STRING 'PARAM_INFR_APPL - FL COMPRESSORE NON VALIDO : '
                         IJCCMQ04-FL-COMPRESSORE-C
                 DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING
                 MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.

           IF IJCCMQ04-MQ
              AND (IJCCMQ04-QUEUE-MANAGER   NOT > SPACES
              OR   IJCCMQ04-CODA-PUT        NOT > SPACES
              OR   IJCCMQ04-CODA-GET        NOT > SPACES
              OR   IJCCMQ04-OPZ-PERSISTENZA NOT > SPACES
              OR   IJCCMQ04-OPZ-WAIT        NOT > SPACES
              OR   IJCCMQ04-OPZ-SYNCPOINT   NOT > SPACES
              OR   IJCCMQ04-ATTESA-RISPOSTA NOT > SPACES)
              INITIALIZE WK-LOG-ERRORE
              MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
              MOVE 'PARAM_INFR_APPL - PARAMETRI INCOMPLETI X MQ'
                   TO  WK-LOR-DESC-ERRORE-ESTESA
              MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
           END-IF.

       A106-EX.
           EXIT.
      *----------------------------------------------------------------*
      * attiva la connessione alle code mq
      *----------------------------------------------------------------*
       A105-ATTIVA-CONNESSIONE.
      *
           MOVE IJCCMQ04-QUEUE-MANAGER          TO WSMQ-QM-NAME.
           MOVE 'MQCONN'                        TO WSMQ-TIPO-OPERAZIONE.
           MOVE IJCCMQ04-TP-UTILIZZO-API        TO WSMQ-TP-UTILIZZO-API.

           CALL PGM-IJCS0060  USING W-AREA-PER-CHIAMATE-WSMQ.

           IF WSMQ-REASON-JCALL IS NOT EQUAL 0
              INITIALIZE WK-LOG-ERRORE
              MOVE WSMQ-REASON-JCALL    TO W-EDIT-REASONS
              STRING 'MQCONN REASON-CODE:' W-EDIT-REASONS
              DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
              END-STRING
              MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
           ELSE
              IF WSMQ-MESSAGGIO-INTERNO = SPACES
                 PERFORM  A109-APRI-CODA-JCALL
                    THRU  A109-EX
                 IF WSMQ-REASON-JCALL IS EQUAL 0
                    PERFORM A110-APRI-CODA-JRET
                       THRU A110-EX
                 END-IF
              ELSE
                 INITIALIZE WK-LOG-ERRORE
                 MOVE WSMQ-MESSAGGIO-INTERNO
                                          TO  WK-LOR-DESC-ERRORE-ESTESA
                 MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.

       A105-EX.
           EXIT.
      *----------------------------------------------------------------*
      * Chiude la connessione alle code mq
      *----------------------------------------------------------------*
       A107-CHIUDI-CONNESSIONE.
      *
            PERFORM  A108-CHIUDI-CODA-JRET
               THRU  A108-EX.

            IF WSMQ-REASON-JRET IS EQUAL 0
               PERFORM A111-CHIUDI-CODA-JCALL
                  THRU A111-EX
               IF WSMQ-REASON-JCALL IS EQUAL 0
                  MOVE 'MQDISC'             TO WSMQ-TIPO-OPERAZIONE
                  MOVE IJCCMQ04-TP-UTILIZZO-API
                    TO WSMQ-TP-UTILIZZO-API
                  CALL PGM-IJCS0060  USING W-AREA-PER-CHIAMATE-WSMQ

                 IF WSMQ-REASON-JCALL IS NOT EQUAL 0
                    INITIALIZE WK-LOG-ERRORE
                    MOVE WSMQ-REASON-JCALL    TO W-EDIT-REASONS
                    STRING 'MQDISC REASON-CODE:' W-EDIT-REASONS
                    DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING
                    MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                    PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                 ELSE
                    IF WSMQ-MESSAGGIO-INTERNO NOT = SPACES
                       INITIALIZE WK-LOG-ERRORE
                       MOVE WSMQ-MESSAGGIO-INTERNO
                                          TO  WK-LOR-DESC-ERRORE-ESTESA
                       MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                       PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    END-IF
                 END-IF
              END-IF
           END-IF.
      *
       A107-EX.
           EXIT.
      *----------------------------------------------------------------*
      * Apertura coda per operazioni di SCRITTURA
      *----------------------------------------------------------------*
       A109-APRI-CODA-JCALL.
      *
           MOVE IJCCMQ04-CODA-PUT        TO WSMQ-NOME-CODA.
           MOVE IJCCMQ04-TP-UTILIZZO-API TO WSMQ-TP-UTILIZZO-API.
           MOVE 'MQOPENJCALL'            TO WSMQ-TIPO-OPERAZIONE.

           CALL PGM-IJCS0060  USING W-AREA-PER-CHIAMATE-WSMQ.

           IF WSMQ-REASON-JCALL NOT = 0
              INITIALIZE WK-LOG-ERRORE
              MOVE WSMQ-REASON-JCALL    TO W-EDIT-REASONS
              STRING 'MQOPEN REASON-CODE:' W-EDIT-REASONS ' O.C.:'
              DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
              END-STRING
              MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
           ELSE
              IF WSMQ-MESSAGGIO-INTERNO NOT = SPACES
                 INITIALIZE WK-LOG-ERRORE
                 MOVE WSMQ-MESSAGGIO-INTERNO
                                    TO  WK-LOR-DESC-ERRORE-ESTESA
                 MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.
      *
       A109-EX.
           EXIT.
      *----------------------------------------------------------------*
      * Apertura coda per operazioni di lettura
      *----------------------------------------------------------------*
       A110-APRI-CODA-JRET.
      *
           MOVE IJCCMQ04-CODA-GET        TO WSMQ-NOME-CODA.
           MOVE IJCCMQ04-TP-UTILIZZO-API TO WSMQ-TP-UTILIZZO-API.
           MOVE 'MQOPENJRET'             TO WSMQ-TIPO-OPERAZIONE.

           CALL PGM-IJCS0060  USING W-AREA-PER-CHIAMATE-WSMQ.

           IF WSMQ-REASON-JRET NOT = 0
              INITIALIZE WK-LOG-ERRORE
              MOVE WSMQ-REASON-JRET TO W-EDIT-REASONS
              STRING 'MQOPEN REASON-CODE:' W-EDIT-REASONS ' O.C.:'
              DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
              END-STRING
              MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
           ELSE
              IF WSMQ-MESSAGGIO-INTERNO NOT = SPACES
                 INITIALIZE WK-LOG-ERRORE
                 MOVE WSMQ-MESSAGGIO-INTERNO
                                    TO  WK-LOR-DESC-ERRORE-ESTESA
                 MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.
      *
       A110-EX.
           EXIT.
      *----------------------------------------------------------------*
      * Chiudi coda JRET
      *----------------------------------------------------------------*
       A108-CHIUDI-CODA-JRET.
      *
           MOVE IJCCMQ04-TP-UTILIZZO-API TO WSMQ-TP-UTILIZZO-API.
           MOVE 'MQCLOSEJRET'            TO WSMQ-TIPO-OPERAZIONE.

           CALL PGM-IJCS0060  USING W-AREA-PER-CHIAMATE-WSMQ.
      *
           IF WSMQ-REASON-JRET NOT = 0      AND
              WSMQ-REASON-JRET NOT = 2019
              INITIALIZE WK-LOG-ERRORE
              MOVE WSMQ-REASON-JRET TO W-EDIT-REASONS
              STRING 'MQCLOSE REASON-CODE:' W-EDIT-REASONS
              DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
              END-STRING
              MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
           ELSE
              IF WSMQ-MESSAGGIO-INTERNO NOT = SPACES
                 INITIALIZE WK-LOG-ERRORE
                 MOVE WSMQ-MESSAGGIO-INTERNO
                                    TO  WK-LOR-DESC-ERRORE-ESTESA
                 MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.

       A108-EX.
           EXIT.
      *----------------------------------------------------------------*
      * Chiudi coda JCALL
      *----------------------------------------------------------------*
       A111-CHIUDI-CODA-JCALL.
      *
           MOVE IJCCMQ04-TP-UTILIZZO-API TO WSMQ-TP-UTILIZZO-API.
           MOVE 'MQCLOSEJCALL'           TO WSMQ-TIPO-OPERAZIONE.

           CALL PGM-IJCS0060  USING W-AREA-PER-CHIAMATE-WSMQ.

           IF WSMQ-REASON-JCALL NOT = 0      AND
              WSMQ-REASON-JCALL NOT = 2019
              INITIALIZE WK-LOG-ERRORE
              MOVE WSMQ-REASON-JRET TO W-EDIT-REASONS
              STRING 'MQCLOSE REASON-CODE:' W-EDIT-REASONS
              DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
              END-STRING
              MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
           ELSE
              IF WSMQ-MESSAGGIO-INTERNO NOT = SPACES
                 INITIALIZE WK-LOG-ERRORE
                 MOVE WSMQ-MESSAGGIO-INTERNO
                                    TO  WK-LOR-DESC-ERRORE-ESTESA
                 MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.

       A111-EX.
           EXIT.

      *----------------------------------------------------------------*
      * SETTA TIMESTAMP
      *----------------------------------------------------------------*
       A150-SET-CURRENT-TIMESTAMP.
      *
           MOVE 'A150-SET-CURRENT-TIMESTAMP' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO   TO TRUE
           MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM ESTRAI-CURRENT-TIMESTAMP
                   THRU ESTRAI-CURRENT-TIMESTAMP-EX

           MOVE SQLCODE       TO IDSV0003-SQLCODE

           IF NOT IDSV0003-SUCCESSFUL-SQL
              INITIALIZE WK-LOG-ERRORE

              MOVE SQLCODE             TO WK-SQLCODE
              STRING 'ERRORE ESTRAZIONE CURRENT TIMESTAMP'
                      ' - '
                      'SQLCODE : '
                      WK-SQLCODE
                      DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX

           END-IF.
      *
       A150-EX.
           EXIT.
      *----------------------------------------------------------------*
      * SETTA TIMESTAMP
      *----------------------------------------------------------------*
       A160-SET-CURRENT-DATE.
      *
           MOVE 'A160-SET-DATE'             TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           PERFORM ESTRAI-CURRENT-DATE
                   THRU ESTRAI-CURRENT-DATE-EX

           MOVE SQLCODE       TO IDSV0003-SQLCODE

           IF NOT IDSV0003-SUCCESSFUL-SQL
              INITIALIZE                  WK-LOG-ERRORE

              MOVE SQLCODE             TO WK-SQLCODE
              STRING 'ERRORE ESTRAZIONE CURRENT DATE'
                      ' - '
                      'SQLCODE : '
                      WK-SQLCODE
                      DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX

           END-IF.
      *
       A160-EX.
           EXIT.
      *----------------------------------------------------------------*
      * APRI ARCHIVI                                                   *
      *----------------------------------------------------------------*
       A200-APRI-FILE.
      *
           MOVE 'A200-APRI-FILE'            TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF WK-ERRORE-NO
              OPEN INPUT PBTCEXEC

              IF FS-PARA    NOT = '00'
                 INITIALIZE     WK-LOG-ERRORE

                 STRING 'OPEN ERRATA DEL FILE '
                         DELIMITED BY SIZE
                        'PBTCEXEC'
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         'RC : '
                         DELIMITED BY SIZE
                         FS-PARA
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              ELSE
                 SET FILE-PARAM-APERTO TO TRUE
              END-IF
           END-IF.
      *
       A200-EX.
           EXIT.
      *----------------------------------------------------------------*
      *  CALL IABS0140
      *----------------------------------------------------------------*
       A888-CALL-IABS0140.

           MOVE 'A888-CALL-IABS0140'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

           CALL PGM-IABS0140                 USING IDSV0003
           ON EXCEPTION
              INITIALIZE WK-LOG-ERRORE
              STRING 'ERRORE CHIAMATA MODULO '
                      DELIMITED BY SIZE
                      PGM-IABS0140
                      DELIMITED BY SIZE
                      ' - '
                       DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
           END-CALL

           IF WK-ERRORE-NO
               IF NOT IDSV0003-SUCCESSFUL-RC
                 INITIALIZE WK-LOG-ERRORE
                 STRING 'ERRORE MODULO '
                         DELIMITED BY SIZE
                         PGM-IABS0140
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         IDSV0003-DESCRIZ-ERR-DB2
                         DELIMITED BY '   '
                         ' - '
                         DELIMITED BY SIZE
                         'RC : '
                         DELIMITED BY SIZE
                         IDSV0003-RETURN-CODE
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              END-IF
           END-IF.
      *
       A888-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESEGUI CONNECT
      *----------------------------------------------------------------*
       A999-ESEGUI-CONNECT.
      *
           MOVE 'A999-ESEGUI-CONNECT'       TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE IABV0008-DATABASE-NAME       TO AREA0140-DATABASE-NAME
           MOVE IABV0008-USER                TO AREA0140-USER
           MOVE IABV0008-PASSWORD            TO AREA0140-PASSWORD

      *--> TIPO OPERAZIONE
           SET AREA0140-CONNECT              TO TRUE.

           MOVE AREA-X-IABS0140     TO IDSV0003-BUFFER-WHERE-COND
           PERFORM A888-CALL-IABS0140 THRU A888-EX

           IF WK-ERRORE-NO
              EVALUATE TRUE
                 WHEN IDSV0003-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                      CONTINUE

                  WHEN IDSV0003-NEGATIVI
      *--->      ERRORE DI ACCESSO AL DB
                      INITIALIZE WK-LOG-ERRORE
                      MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                      STRING 'ERRORE CONNECT AL DB'
                             ' - '
                             'SQLCODE : '
                             WK-SQLCODE
                             DELIMITED BY SIZE INTO
                             WK-LOR-DESC-ERRORE-ESTESA
                      END-STRING

                      PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX

                 END-EVALUATE
           END-IF.
      *
       A999-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESTRAZIONE PARAMETRI INPUT
      *----------------------------------------------------------------*
       B050-ESTRAI-PARAMETRI.
      *
           MOVE 'B050-ESTRAI-PARAMETRI'     TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM A200-APRI-FILE        THRU A200-EX.

           PERFORM B100-READ-PARAM       THRU B100-EX.

           PERFORM B200-CONTROL-PARAM    THRU B200-EX
             UNTIL FILE-PARAM-FINITO
                OR WK-ERRORE-YES.
      *
       B050-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESTRAZIONE DATA COMPETENZA
      *----------------------------------------------------------------*
       B020-ESTRAI-DT-COMPETENZA.
      *
           MOVE 'B020-ESTRAI-DT-COMPETENZA' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           CALL PGM-IDSS0150               USING AREA-IDSV0001
           ON EXCEPTION
              STRING 'ERRORE CHIAMATA MODULO '
                      DELIMITED BY SIZE
                      PGM-IDSS0150
                      DELIMITED BY SIZE
                      ' - '
                       DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
           END-CALL

           IF WK-ERRORE-NO
              IF IDSV0001-ESITO-OK
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                 IF IABI0011-LIVELLO-DEBUG IS NUMERIC AND
                    IABI0011-LIVELLO-DEBUG NOT = ZEROES
                    MOVE IABI0011-LIVELLO-DEBUG
                                   TO IDSV0001-LIVELLO-DEBUG
                 END-IF
              ELSE
      *-->       GESTIRE ERRORE DISPATCHER
                 INITIALIZE WK-LOG-ERRORE

                 STRING 'ERRORE MODULO '
                         DELIMITED BY SIZE
                         PGM-IDSS0150
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         IDSV0001-DESC-ERRORE-ESTESA
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 SET  WK-ERRORE-FATALE    TO TRUE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.
      *
       B020-EX.
           EXIT.
      *----------------------------------------------------------------*
      * VALORIZZA AREA CONTESTO
      *----------------------------------------------------------------*
       B060-VALORIZZA-AREA-CONTESTO.
      *
           MOVE 'B060-VALORIZZA-AREA-CONTESTO' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO     TO TRUE
           MOVE WK-LABEL                       TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM VARYING IX-ADDRESS FROM 1 BY 1
                   UNTIL   IX-ADDRESS > 5
                   MOVE SPACE      TO IDSV0001-ADDRESS-TYPE(IX-ADDRESS)
           END-PERFORM

           MOVE ZERO                          TO IX-ADDRESS1.
           INITIALIZE IJCCMQ04-VAR-AMBIENTE.
           PERFORM VARYING IX-ADDRESS FROM 1 BY 1
                UNTIL   IX-ADDRESS  > 5
                MOVE IABI0011-ADDRESS-TYPE(IX-ADDRESS) TO
                     TIPO-ADDRESS
                IF IABI0011-ADDRESS-TYPE(IX-ADDRESS) NOT = SPACE
                                                 AND NOT = HIGH-VALUE
                                                 AND NOT = LOW-VALUE
                                                 AND NOT ADDRESS-AREA-MQ
                   ADD  1                      TO IX-ADDRESS1
                   MOVE IABI0011-ADDRESS-TYPE(IX-ADDRESS) TO
                        IDSV0001-ADDRESS-TYPE(IX-ADDRESS1)
                   IF   ADDRESS-VAR-AMBIENTE
                        SET IDSV0001-ADDRESS        (IX-ADDRESS1)
                         TO ADDRESS OF IJCCMQ04-VAR-AMBIENTE
                        PERFORM A103-LEGGI-PARAM-INFR-APPL
                           THRU A103-EX
                   END-IF
                END-IF
           END-PERFORM.

           PERFORM VARYING IX-ADDRESS FROM 1 BY 1
                UNTIL   IX-ADDRESS  > 5
                MOVE IABI0011-ADDRESS-TYPE(IX-ADDRESS) TO
                     TIPO-ADDRESS
                IF ADDRESS-AREA-MQ AND
                   IJCCMQ04-MQ
                   ADD  1                      TO IX-ADDRESS1
                   MOVE TIPO-ADDRESS           TO
                        IDSV0001-ADDRESS-TYPE(IX-ADDRESS1)
                   SET IDSV0001-ADDRESS        (IX-ADDRESS1)
                               TO ADDRESS OF W-AREA-PER-CHIAMATE-WSMQ
                   PERFORM A105-ATTIVA-CONNESSIONE
                      THRU A105-EX
                END-IF
           END-PERFORM.

           MOVE IDSV0001-MODALITA-ESECUTIVA
                                     TO IDSV0003-MODALITA-ESECUTIVA

           MOVE IABI0011-CODICE-COMPAGNIA-ANIA
                                     TO IDSV0001-COD-COMPAGNIA-ANIA
                                        IDSV0003-CODICE-COMPAGNIA-ANIA

           MOVE IDSV0001-COD-MAIN-BATCH
                                     TO IDSV0003-COD-MAIN-BATCH

           IF IABI0011-TIPO-MOVIMENTO IS NUMERIC AND
              IABI0011-TIPO-MOVIMENTO NOT = ZEROES
              MOVE IABI0011-TIPO-MOVIMENTO
                                        TO IDSV0001-TIPO-MOVIMENTO
                                           IDSV0003-TIPO-MOVIMENTO
           END-IF

           MOVE IABI0011-LINGUA         TO IDSV0001-LINGUA
           MOVE IABI0011-PAESE          TO IDSV0001-PAESE
           MOVE IABI0011-USER-NAME      TO IDSV0001-USER-NAME
                                           IDSV0003-USER-NAME

           MOVE IABI0011-KEY-BUSINESS1  TO IDSV0001-KEY-BUSINESS1.
           MOVE IABI0011-KEY-BUSINESS2  TO IDSV0001-KEY-BUSINESS2.
           MOVE IABI0011-KEY-BUSINESS3  TO IDSV0001-KEY-BUSINESS3.
           MOVE IABI0011-KEY-BUSINESS4  TO IDSV0001-KEY-BUSINESS4.
           MOVE IABI0011-KEY-BUSINESS5  TO IDSV0001-KEY-BUSINESS5.

           PERFORM B070-ESTRAI-SESSIONE THRU B070-EX

           IF WK-ERRORE-NO
              MOVE IDSV0001-SESSIONE    TO IDSV0003-SESSIONE
              MOVE IABV0008-TRATTAMENTO-STORICITA
                               TO IDSV0001-TRATTAMENTO-STORICITA
                                  IDSV0003-TRATTAMENTO-STORICITA

              MOVE IABV0008-FORMATO-DATA-DB
                               TO IDSV0001-FORMATO-DATA-DB
                                  IDSV0003-FORMATO-DATA-DB


              IF IABI0011-DATA-EFFETTO IS NUMERIC   AND
                 IABI0011-DATA-EFFETTO NOT = ZEROES
                 MOVE IABI0011-DATA-EFFETTO
                                       TO IDSV0001-DATA-EFFETTO
                                          IDSV0003-DATA-INIZIO-EFFETTO
              ELSE
                 PERFORM A160-SET-CURRENT-DATE THRU A160-EX
                 MOVE    WS-DATE-N     TO IDSV0001-DATA-EFFETTO
                                       IDSV0003-DATA-INIZIO-EFFETTO
              END-IF

              IF WK-ERRORE-NO
                 IF NOT IABI0011-NO-TEMP-TABLE
                    SET IDSV0001-ID-TMPRY-DATA-SI  TO TRUE
                 END-IF

                 PERFORM B020-ESTRAI-DT-COMPETENZA THRU B020-EX

                 IF WK-ERRORE-NO
                    IF IABI0011-TS-COMPETENZA IS NUMERIC   AND
                       IABI0011-TS-COMPETENZA NOT = ZEROES
                       MOVE IABI0011-TS-COMPETENZA
                                     TO IDSV0001-DATA-COMPETENZA
                                        IDSV0003-DATA-COMPETENZA
                    ELSE
                       MOVE IDSV0001-DATA-COMPETENZA
                                     TO IDSV0003-DATA-COMPETENZA
                    END-IF

                    MOVE IDSV0001-DATA-COMP-AGG-STOR
                                      TO IDSV0003-DATA-COMP-AGG-STOR
                 END-IF
              END-IF
           END-IF.

           IF WK-ERRORE-NO
              EVALUATE TRUE
                 WHEN IABI0011-NO-TEMP-TABLE
                      SET IDSV0001-NO-TEMP-TABLE      TO TRUE

                 WHEN IABI0011-STATIC-TEMP-TABLE
                      SET IDSV0001-STATIC-TEMP-TABLE  TO TRUE

                 WHEN IABI0011-SESSION-TEMP-TABLE
                      SET IDSV0001-SESSION-TEMP-TABLE TO TRUE

              END-EVALUATE
           END-IF.
      *
       B060-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESTRAI SESSIONE
      *----------------------------------------------------------------*
       B070-ESTRAI-SESSIONE.

           MOVE 'B070-ESTRAI-SESSIONE'      TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           MOVE SPACES                   TO LCCC0090.
           MOVE 'SEQ_SESSIONE'           TO LINK-NOME-TABELLA.
           CALL PGM-LCCS0090             USING LCCC0090
           ON EXCEPTION
              STRING 'ERRORE CHIAMATA MODULO '
                      DELIMITED BY SIZE
                      PGM-LCCS0090
                      DELIMITED BY SIZE
                      ' - '
                       DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
           END-CALL

           IF WK-ERRORE-NO
              IF LINK-RETURN-CODE NOT = ZERO
                 INITIALIZE WK-LOG-ERRORE

                 MOVE IDSV0003-SQLCODE  TO WK-SQLCODE
                 STRING 'ERRORE MODULO '
                         DELIMITED BY SIZE
                         PGM-LCCS0090
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         ' RC : '
                         DELIMITED BY SIZE
                         LINK-RETURN-CODE
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         ' SQLCODE : '
                         DELIMITED BY SIZE
                         LINK-SQLCODE
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         LINK-DESCRIZ-ERR-DB2
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              ELSE
                 MOVE LINK-SEQ-TABELLA   TO WK-SEQ-SESSIONE
                 MOVE WK-SEQ-SESSIONE    TO IDSV0001-SESSIONE
              END-IF
           END-IF.
      *
       B070-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ELABORAZIONE                                                   *
      *----------------------------------------------------------------*
       B000-ELABORA-MACRO.
      *
           MOVE 'B000-ELABORA-MACRO'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM B050-ESTRAI-PARAMETRI      THRU B050-EX.

           IF WK-ERRORE-NO

            PERFORM B300-CTRL-INPUT-SKEDA   THRU B300-EX

            IF WK-ERRORE-NO

               IF IABV0008-CONNESSIONE-YES
                  PERFORM A999-ESEGUI-CONNECT  THRU A999-EX
               END-IF

               IF WK-ERRORE-NO
                  PERFORM A100-INIZIO-ELABORA    THRU A100-EX

                 IF WK-ERRORE-NO
                  PERFORM B060-VALORIZZA-AREA-CONTESTO THRU B060-EX

                  SET  IDSV8888-ARCH-BATCH-DBG    TO TRUE
                  MOVE WK-PGM                     TO IDSV8888-NOME-PGM
                  MOVE 'Architettura Batch'       TO IDSV8888-DESC-PGM
                  SET  IDSV8888-INIZIO            TO TRUE
                  PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX

                  IF WK-ERRORE-NO
                    PERFORM C000-ESTRAI-STATI    THRU C000-EX

                    IF WK-ERRORE-NO
                      PERFORM X001-CARICA-STATI-BATCH   THRU X001-EX

                      IF GESTIONE-PARALLELISMO-YES
                         MOVE BATCH-IN-ESECUZIONE
                           TO IABV0002-STATE-ELE(IND-STATI-V0002 + 1)
                      END-IF

                      IF IABI0011-SESSION-TEMP-TABLE
                         MOVE IDSV0001-TEMPORARY-TABLE
                                            TO IDSV0301-TEMPORARY-TABLE
                         PERFORM D999-SESSION-TABLE        THRU D999-EX
                      END-IF

                      IF WK-ERRORE-NO
                         IF WK-PROTOCOL-YES
                            PERFORM D001-ESTRAI-BATCH-PROT THRU D001-EX
                         ELSE
                            PERFORM D000-ESTRAI-BATCH      THRU D000-EX
                         END-IF

                         IF WK-ERRORE-NO
                            IF WK-ELABORA-BATCH-NO
                              INITIALIZE WK-LOG-ERRORE

                              MOVE 'NON ESISTONO BATCH ESEGUIBILI'
                                           TO WK-LOR-DESC-ERRORE-ESTESA

                              SET WK-RC-04                    TO TRUE

                              SET WK-ERRORE-NON-BLOCCANTE     TO TRUE
                              MOVE WARNING TO WK-LOR-ID-GRAVITA-ERRORE

                              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                                         THRU W070-EX
                            END-IF
                         END-IF
                      END-IF
                    END-IF
                  END-IF
                 END-IF
               END-IF
            END-IF
           END-IF

           IF IDSV0001-MAX-ELE-ERRORI > 0
              PERFORM W000-CARICA-LOG-ERRORE          THRU W000-EX
           END-IF.
      *
       B000-EX.
           EXIT.
      *----------------------------------------------------------------*
      * LEGGI FILE PARAM
      *----------------------------------------------------------------*
       B100-READ-PARAM.
      *
           MOVE 'B100-READ-PARAM'           TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           READ PBTCEXEC.

           EVALUATE FS-PARA
              WHEN '00'
                 ADD  1                      TO CNT-NUM-OPERAZ-LETTE

              WHEN '10'
                 SET FILE-PARAM-FINITO       TO TRUE

              WHEN OTHER
                 INITIALIZE               WK-LOG-ERRORE
                 MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                 MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
                 MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE

                 STRING 'READ ERRATA DEL FILE '
                         DELIMITED BY SIZE
                         'PBTCEXEC'
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         'FS : '
                         DELIMITED BY SIZE
                         FS-PARA
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX

           END-EVALUATE.
      *
       B100-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CONTROLLA PARAMETRI
      *----------------------------------------------------------------*
       B200-CONTROL-PARAM.

           MOVE 'B200-CONTROL-PARAM'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF PARAM-REC(1:1) = '*' OR '$'
              IF PARAM-REC(1:1) = '$'
                 SET PRIMA-BUFFER-SK-TROVATO-YES TO TRUE
                 MOVE 1                          TO IND-POSIZ-PARAM
              END-IF
           ELSE
              PERFORM B210-ESTRAI-LUNG-PARAM THRU B210-EX
           END-IF

           IF WK-ERRORE-NO
              PERFORM B100-READ-PARAM        THRU B100-EX
           END-IF.
      *
       B200-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESTRAI LUNGHEZZA PARAMETRI
      *----------------------------------------------------------------*
       B210-ESTRAI-LUNG-PARAM.

           MOVE 'B210-ESTRAI-LUNG-PARAM'    TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           INITIALIZE WK-LUNG-PARAM-X
                      IND-LUNG

           SET INIZIO-LETT-LUNG-NO   TO TRUE
           SET FINE-LETT-LUNG-NO     TO TRUE
           SET POSIZ-PARAM-NO        TO TRUE

           PERFORM VARYING IND-SEARCH FROM 1 BY 1
               UNTIL IND-SEARCH = WK-LIMITE-SKEDA-PARAMETRO
               OR    POSIZ-PARAM-YES

                     IF PARAM-REC(IND-SEARCH:1) = '('
                        SET INIZIO-LETT-LUNG-YES TO TRUE
                     ELSE
                        IF PARAM-REC(IND-SEARCH:1) = ')'
                           SET FINE-LETT-LUNG-YES   TO TRUE
                        ELSE
                           IF PARAM-REC(IND-SEARCH:1) = ':'

                              SET POSIZ-PARAM-YES   TO TRUE
                              IF PARAM-REC(IND-SEARCH:1) = 'N'
                                 PERFORM B211-CNTL-NUMERICO
                                              THRU B211-EX
                              ELSE
                                IF PRIMA-BUFFER-SK-TROVATO-YES
                                  MOVE PARAM-REC(IND-SEARCH + 1 :
                                             WK-LUNG-PARAM-9)
                                  TO IABI0011-BUFFER-WHERE-COND
                                              (IND-POSIZ-PARAM :
                                                   WK-LUNG-PARAM-9)
                                ELSE
                                   MOVE PARAM-REC(IND-SEARCH + 1 :
                                              WK-LUNG-PARAM-9)
                                   TO IABI0011-AREA(IND-POSIZ-PARAM :
                                                    WK-LUNG-PARAM-9)
                                END-IF
                              END-IF
                           ELSE

                              IF INIZIO-LETT-LUNG-YES AND
                                 FINE-LETT-LUNG-NO

                                 IF PARAM-REC(IND-SEARCH:1)
                                                         IS NUMERIC
                                    ADD 1  TO IND-LUNG
                                    MOVE PARAM-REC(IND-SEARCH:1)
                                         TO WK-LUNG-PARAM-X(IND-LUNG:1)
                                 END-IF

                              END-IF
                           END-IF
                        END-IF
                     END-IF

           END-PERFORM.

           IF INIZIO-LETT-LUNG-YES      AND
              FINE-LETT-LUNG-YES        AND
              POSIZ-PARAM-YES
              COMPUTE IND-POSIZ-PARAM = IND-POSIZ-PARAM +
                                           WK-LUNG-PARAM-9
           ELSE
              PERFORM Z300-SKEDA-PAR-ERRATA   THRU Z300-EX
           END-IF.
      *
       B210-EX.
           EXIT.
      *----------------------------------------------------------------*
      * NORMALIZZA CAMPO NUMERICO
      *----------------------------------------------------------------*
       B211-CNTL-NUMERICO.
      *
           MOVE 'B211-CNTL-NUMERICO'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF PARAM-REC(IND-SEARCH + 1 : WK-LUNG-PARAM-9)
                                     IS NOT NUMERIC
                 INITIALIZE               WK-LOG-ERRORE

                 STRING 'INPUT SK. PARAM. NON VALIDO : '
                         DELIMITED BY SIZE
                         'CAMPO NUMERICO'
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
           END-IF.
      *
       B211-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CONTROLLO INPUT SKEDA PARAMTERI
      *----------------------------------------------------------------*
       B300-CTRL-INPUT-SKEDA.
      *
           MOVE 'B300-CTRL-INPUT-SKEDA'     TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF IABI0011-PROTOCOL = LOW-VALUE      OR
                                  HIGH-VALUES    OR
                                  SPACES
              IF IABI0011-TIPO-BATCH NOT NUMERIC OR
                 IABI0011-TIPO-BATCH = ZEROES

                 INITIALIZE           WK-LOG-ERRORE
                 MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                 MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                 MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE

                 STRING 'INPUT SK. PARAM. NON VALIDO : '
                         DELIMITED BY SIZE
                         'PROTOCOL/TIPO '
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              ELSE
                 SET WK-PROTOCOL-NO         TO TRUE
                 MOVE IABI0011-TIPO-BATCH
                                 TO BTC-COD-BATCH-TYPE
                 MOVE IABI0011-CODICE-COMPAGNIA-ANIA
                                 TO BTC-COD-COMP-ANIA
              END-IF

           ELSE
             SET WK-PROTOCOL-YES        TO TRUE
             MOVE IABI0011-PROTOCOL     TO BTC-PROTOCOL

           END-IF

           IF WK-ERRORE-NO
              IF IABI0011-PROG-PROTOCOL IS NUMERIC AND
                 IABI0011-PROG-PROTOCOL NOT = ZEROES
                 SET GESTIONE-PARALLELISMO-YES TO TRUE
              END-IF
           END-IF

           IF WK-ERRORE-NO
              IF IABI0011-CODICE-COMPAGNIA-ANIA NOT NUMERIC OR
                 IABI0011-CODICE-COMPAGNIA-ANIA = ZEROES

                 INITIALIZE           WK-LOG-ERRORE
                 MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                 MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                 MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE

                 STRING 'INPUT SK. PARAM. NON VALIDO : '
                         DELIMITED BY SIZE
                         'CODICE COMPAGNIA '
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              ELSE
                 MOVE IABI0011-CODICE-COMPAGNIA-ANIA
                                       TO BTC-COD-COMP-ANIA
              END-IF
           END-IF

           IF WK-ERRORE-NO
              IF IABI0011-LINGUA = HIGH-VALUE OR
                                   LOW-VALUE  OR
                                   SPACES

                 INITIALIZE           WK-LOG-ERRORE
                 MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                 MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                 MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE

                 STRING 'INPUT SK. PARAM. NON VALIDO : '
                         DELIMITED BY SIZE
                         'LINGUA '
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              END-IF
           END-IF

           IF WK-ERRORE-NO
              IF IABI0011-PAESE = HIGH-VALUE OR
                                  LOW-VALUE  OR
                                  SPACES

                 INITIALIZE           WK-LOG-ERRORE
                 MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                 MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                 MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE

                 STRING 'INPUT SK. PARAM. NON VALIDO : '
                         DELIMITED BY SIZE
                         'PAESE '
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              END-IF
           END-IF

           IF WK-ERRORE-NO
              IF IABI0011-USER-NAME = HIGH-VALUE OR
                                      LOW-VALUE  OR
                                      SPACES

                 INITIALIZE           WK-LOG-ERRORE
                 MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                 MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                 MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE

                 STRING 'INPUT SK. PARAM. NON VALIDO : '
                         DELIMITED BY SIZE
                         'USER NAME'
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              END-IF
           END-IF

           IF WK-ERRORE-NO
              IF IABI0011-GRUPPO-AUTORIZZANTE NOT NUMERIC OR
                 IABI0011-GRUPPO-AUTORIZZANTE = ZEROES

                 STRING 'INPUT SK. PARAM. NON VALIDO : '
                         DELIMITED BY SIZE
                         'GRUPPO AUTORIZZANTE'
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
               END-IF
           END-IF

           IF WK-ERRORE-NO
              IF IABI0011-CANALE-VENDITA NOT NUMERIC

                 STRING 'INPUT SK. PARAM. NON VALIDO : '
                         DELIMITED BY SIZE
                         'CANALE VENDITA'
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
               END-IF
           END-IF

           IF WK-ERRORE-NO
              IF IABI0011-PUNTO-VENDITA = HIGH-VALUE OR
                                          LOW-VALUE OR
                                          SPACES

                 INITIALIZE           WK-LOG-ERRORE
                 MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                 MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                 MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE

                 STRING 'INPUT SK. PARAM. NON VALIDO : '
                         DELIMITED BY SIZE
                         'PUNTO VENDITA'
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              END-IF
           END-IF

           IF WK-ERRORE-NO
              IF BATCH-EXECUTOR-STD-NO
                 IF IABI0011-MACROFUNZIONALITA NOT = HIGH-VALUE AND
                    IABI0011-MACROFUNZIONALITA NOT = LOW-VALUE  AND
                    IABI0011-MACROFUNZIONALITA NOT = SPACES

                    INITIALIZE           WK-LOG-ERRORE
                    MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                    MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                    MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE

                    STRING 'INPUT SK. PARAM. NON VALIDO : '
                            DELIMITED BY SIZE
                            'MACROFUNZIONALITA NON AMMESSA'
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                 END-IF


                 IF WK-ERRORE-NO
                    IF IABI0011-TIPO-MOVIMENTO IS NUMERIC AND
                       IABI0011-TIPO-MOVIMENTO NOT = ZEROES

                       INITIALIZE           WK-LOG-ERRORE
                       MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                       MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                       MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE

                       STRING 'INPUT SK. PARAM. NON VALIDO : '
                               DELIMITED BY SIZE
                               'TIPO MOVIMENTO NON AMMESSO'
                               DELIMITED BY SIZE INTO
                               WK-LOR-DESC-ERRORE-ESTESA
                       END-STRING

                       PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX

                    END-IF
                 END-IF
              END-IF
           END-IF

           IF WK-ERRORE-NO
              IF IABI0011-DATA-EFFETTO IS NUMERIC   AND
                 IABI0011-DATA-EFFETTO NOT = ZEROES

                    MOVE IABI0011-DATA-EFFETTO(1:4)
                         TO IDSV0016-AAAA
                    MOVE IABI0011-DATA-EFFETTO(5:2)
                         TO IDSV0016-MM
                    MOVE IABI0011-DATA-EFFETTO(7:2)
                         TO IDSV0016-GG

                    PERFORM CNTL-FORMALE-DATA
                            THRU CNTL-FORMALE-DATA-EX

                    IF WK-ERRORE-YES

                       STRING 'INPUT SK. PARAM. NON VALIDO : '
                               DELIMITED BY SIZE
                               'DATA EFFETTO'
                               DELIMITED BY SIZE INTO
                               WK-LOR-DESC-ERRORE-ESTESA
                       END-STRING

                       PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    END-IF

              END-IF
           END-IF

           IF WK-ERRORE-NO
              IF NOT IABI0011-FLAG-STOR-ESECUZ-YES AND
                 NOT IABI0011-FLAG-STOR-ESECUZ-NO
                 IF IABI0011-FLAG-STOR-ESECUZ =
                    SPACES OR LOW-VALUE OR HIGH-VALUE

                    MOVE IABV0008-FLAG-STOR-ESECUZ
                         TO IABI0011-FLAG-STOR-ESECUZ
                 ELSE

                    STRING 'INPUT SK. PARAM. NON VALIDO : '
                            DELIMITED BY SIZE
                            'FLAG STOR. ESECUZ.'
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                 END-IF
              END-IF
           END-IF

           IF WK-ERRORE-NO
              IF NOT IABI0011-SIMULAZIONE-INFR     AND
                 NOT IABI0011-SIMULAZIONE-FITTIZIA AND
                 NOT IABI0011-SIMULAZIONE-APPL     AND
                 NOT IABI0011-SIMULAZIONE-NO

                 IF IABI0011-FLAG-SIMULAZIONE =
                    SPACES OR LOW-VALUE OR HIGH-VALUE

                    MOVE IABV0008-FLAG-SIMULAZIONE
                         TO IABI0011-FLAG-SIMULAZIONE
                 ELSE

                    STRING 'INPUT SK. PARAM. NON VALIDO : '
                            DELIMITED BY SIZE
                            'FLAG SIMULAZIONE'
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                 END-IF

              END-IF

           END-IF

           IF WK-ERRORE-NO
              IF NOT IABI0011-TRATTAMENTO-PARTIAL AND
                 NOT IABI0011-TRATTAMENTO-FULL
                 IF IABI0011-TRATTAMENTO-COMMIT =
                    SPACES OR LOW-VALUE OR HIGH-VALUE

                    MOVE IABV0008-TRATTAMENTO-COMMIT
                         TO IABI0011-TRATTAMENTO-COMMIT
                 ELSE

                    STRING 'INPUT SK. PARAM. NON VALIDO : '
                            DELIMITED BY SIZE
                            'TRATT. COMMIT'
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                 END-IF
              END-IF
           END-IF

           IF WK-ERRORE-NO
              IF NOT IABI0011-SENZA-RIPARTENZA     AND
                 NOT IABI0011-RIP-CON-MONITORING   AND
                 NOT IABI0011-RIP-SENZA-MONITORING AND
                 NOT IABI0011-RIP-CON-VERSIONAMENTO
                 IF IABI0011-GESTIONE-RIPARTENZA =
                    SPACES OR LOW-VALUE OR HIGH-VALUE

                    MOVE IABV0008-GESTIONE-RIPARTENZA
                         TO IABI0011-GESTIONE-RIPARTENZA
                 ELSE

                    STRING 'INPUT SK. PARAM. NON VALIDO : '
                            DELIMITED BY SIZE
                            'GESTIONE RIPARTENZA'
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                 END-IF
              END-IF
           END-IF

           IF WK-ERRORE-NO
              IF NOT IABI0011-ORDER-BY-IB-OGG AND
                 NOT IABI0011-ORDER-BY-ID-JOB
                 IF IABI0011-TIPO-ORDER-BY =
                    SPACES OR LOW-VALUE OR HIGH-VALUE

                    MOVE IABV0008-TIPO-ORDER-BY
                         TO IABI0011-TIPO-ORDER-BY
                 ELSE

                    STRING 'INPUT SK. PARAM. NON VALIDO : '
                            DELIMITED BY SIZE
                            'TIPO ORDER BY'
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                 END-IF
              END-IF
           END-IF

           IF WK-ERRORE-NO
              IF NOT IABI0011-GRAVITA-UNO     AND
                 NOT IABI0011-GRAVITA-DUE     AND
                 NOT IABI0011-GRAVITA-TRE     AND
                 NOT IABI0011-GRAVITA-QUATTRO
                 IF IABI0011-LIVELLO-GRAVITA-LOG =
                    SPACES OR LOW-VALUE OR HIGH-VALUE

                    MOVE IABV0008-LIVELLO-GRAVITA-LOG
                         TO IABI0011-LIVELLO-GRAVITA-LOG
                 ELSE

                    STRING 'INPUT SK. PARAM. NON VALIDO : '
                            DELIMITED BY SIZE
                            'LIVELLO GRAVITA'
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                 END-IF
              END-IF
           END-IF

           IF WK-ERRORE-NO
              IF NOT IABI0011-NO-TEMP-TABLE      AND
                 NOT IABI0011-STATIC-TEMP-TABLE  AND
                 NOT IABI0011-SESSION-TEMP-TABLE
                 IF IABI0011-TEMPORARY-TABLE =
                    SPACES OR LOW-VALUE OR HIGH-VALUE

                    MOVE IABV0008-TEMPORARY-TABLE
                         TO IABI0011-TEMPORARY-TABLE
                 ELSE

                    STRING 'INPUT SK. PARAM. NON VALIDO : '
                            DELIMITED BY SIZE
                            'TEMPORARY TABLE'
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                 END-IF
              END-IF
           END-IF.

           IF WK-ERRORE-NO
              IF NOT IABI0011-DATA-1K  AND
                 NOT IABI0011-DATA-2K  AND
                 NOT IABI0011-DATA-3K  AND
                 NOT IABI0011-DATA-10K
                 IF IABI0011-LENGTH-DATA-GATES =
                    SPACES OR LOW-VALUE OR HIGH-VALUE

                    MOVE IABV0008-LENGTH-DATA-GATES
                         TO IABI0011-LENGTH-DATA-GATES

                 ELSE

                    STRING 'INPUT SK. PARAM. NON VALIDO : '
                            DELIMITED BY SIZE
                            'LENGTH DATA GATES'
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                 END-IF
              END-IF
           END-IF.

           IF WK-ERRORE-NO
              EVALUATE TRUE
                 WHEN IABI0011-DATA-1K
                      SET WK-LIMITE-ARRAY-BLOB-1K     TO TRUE

                 WHEN IABI0011-DATA-2K
                      SET WK-LIMITE-ARRAY-BLOB-2K     TO TRUE

                 WHEN IABI0011-DATA-3K
                      SET WK-LIMITE-ARRAY-BLOB-3K     TO TRUE

                 WHEN IABI0011-DATA-10K
                      SET WK-LIMITE-ARRAY-BLOB-10K    TO TRUE

              END-EVALUATE
           END-IF.

           IF WK-ERRORE-NO
              IF NOT IABI0011-APPEND-DATA-GATES-YES AND
                 NOT IABI0011-APPEND-DATA-GATES-NO
                 IF IABI0011-APPEND-DATA-GATES =
                    SPACES OR LOW-VALUE OR HIGH-VALUE

                    MOVE IABV0008-APPEND-DATA-GATES
                         TO IABI0011-APPEND-DATA-GATES
                 ELSE

                    STRING 'INPUT SK. PARAM. NON VALIDO : '
                            DELIMITED BY SIZE
                            'APPEND DATA GATES'
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                 END-IF
              END-IF
           END-IF.

           IF WK-ERRORE-NO
              IF IABI0011-APPEND-DATA-GATES-YES AND
                 IABI0011-NO-TEMP-TABLE
                 STRING 'INPUT SK. PARAM. NON VALIDO : '
                         DELIMITED BY SIZE
                         'APPEND DATA GATES NON CONGRUENTE'
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              END-IF
           END-IF.
      *
           IF WK-ERRORE-NO
              PERFORM VARYING IX-ADDRESS FROM 1 BY 1
                  UNTIL   IX-ADDRESS > 5
                  MOVE IABI0011-ADDRESS-TYPE(IX-ADDRESS) TO
                       TIPO-ADDRESS
                  IF NOT ADDRESS-CACHE-VALIDO
                     STRING 'INPUT SK. PARAM. : '
                       DELIMITED BY SIZE
                       'ADDRESS TYPE NON VALIDO'
                       DELIMITED BY SIZE INTO
                       WK-LOR-DESC-ERRORE-ESTESA
                     END-STRING
                     PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                  END-IF
              END-PERFORM
           END-IF.

           IF WK-ERRORE-NO
              PERFORM VARYING IX-ADDRESS FROM 1 BY 1
                  UNTIL   IX-ADDRESS > 4
                  MOVE IX-ADDRESS    TO IX-ADDRESS1
                  ADD  1             TO IX-ADDRESS1
                  PERFORM VARYING IX-ADDRESS1 FROM IX-ADDRESS1 BY 1
                  UNTIL   IX-ADDRESS1 > 5
                        IF IABI0011-ADDRESS-TYPE(IX-ADDRESS) =
                           IABI0011-ADDRESS-TYPE(IX-ADDRESS1) AND
                          (IABI0011-ADDRESS-TYPE(IX-ADDRESS) NOT =
                           SPACE AND LOW-VALUE AND HIGH-VALUE)
                           STRING 'INPUT SK. PARAM. NON VALIDO : '
                             DELIMITED BY SIZE
                             'ADDRESS TYPE UGUALI'
                             DELIMITED BY SIZE INTO
                             WK-LOR-DESC-ERRORE-ESTESA
                           END-STRING
                           PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                           MOVE 6     TO IX-ADDRESS IX-ADDRESS1
                        END-IF
                  END-PERFORM
              END-PERFORM
           END-IF.

           IF WK-ERRORE-NO
              IF NOT IABI0011-FORZ-RC-04-YES AND
                 NOT IABI0011-FORZ-RC-04-NO
                 IF IABI0011-FORZ-RC-04 =
                    SPACES OR LOW-VALUE OR HIGH-VALUE

                    CONTINUE

                 ELSE

                    STRING 'INPUT SK. PARAM. NON VALIDO : '
                            DELIMITED BY SIZE
                            'FORZATURA RC'
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                 END-IF
              END-IF
           END-IF.

           IF WK-ERRORE-NO
              IF NOT IABI0011-ACCESSO-X-DT-EFF-MAX AND
                 NOT IABI0011-ACCESSO-X-DT-EFF-MIN AND
                 NOT IABI0011-ACCESSO-X-DT-INS-MAX AND
                 NOT IABI0011-ACCESSO-X-DT-INS-MIN AND
                 NOT IABI0011-ACCESSO-ALL-BATCH

                 IF IABI0011-ACCESSO-BATCH =
                    SPACES OR LOW-VALUE OR HIGH-VALUE

                    MOVE IABV0008-ACCESSO-BATCH
                      TO IABI0011-ACCESSO-BATCH
                 ELSE
                    STRING 'INPUT SK. PARAM. NON VALIDO : '
                           DELIMITED BY SIZE
                           'ACCESSO BATCH'
                           DELIMITED BY SIZE INTO
                           WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                 END-IF
              END-IF
           END-IF.

       B300-EX.
           EXIT.

      *----------------------------------------------------------------*
      * ESTRAI STATI
      *----------------------------------------------------------------*
       C000-ESTRAI-STATI.

           MOVE 'C000-ESTRAI-STATI'         TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           PERFORM C100-ESTRAI-STATI-BATCH  THRU C100-EX

           IF WK-ERRORE-NO
              PERFORM C200-ESTRAI-STATI-JOB THRU C200-EX
           END-IF.
      *
       C000-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESTRAI STATI BATCH
      *----------------------------------------------------------------*
       C100-ESTRAI-STATI-BATCH.
      *
           MOVE 'C100-ESTRAI-STATI-BATCH'   TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

           MOVE ZEROES                    TO IND-STATI-BATCH

      *----------------------------------------------------------------*
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC  OR
                         NOT IDSV0003-SUCCESSFUL-SQL OR
                         NOT WK-ERRORE-NO

            PERFORM CALL-IABS0070        THRU CALL-IABS0070-EX

            IF WK-ERRORE-NO
               IF IDSV0003-SUCCESSFUL-RC
                 EVALUATE TRUE
                    WHEN IDSV0003-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                       ADD 1    TO IND-STATI-BATCH
                       IF IND-STATI-BATCH > WK-LIMITE-STATI-BATCH
                          INITIALIZE        WK-LOG-ERRORE

                          STRING 'OVERFLOW STATI '
                                 DELIMITED BY SIZE
                                 'BATCH_STATE'
                                 DELIMITED BY SIZE
                                 INTO
                                 WK-LOR-DESC-ERRORE-ESTESA
                          END-STRING

                          MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                          PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                             THRU W070-EX
                       ELSE
                          MOVE BBS-COD-BATCH-STATE
                                       TO BAT-COD-BATCH-STATE
                                                      (IND-STATI-BATCH)
                          MOVE BBS-DES TO BAT-DES
                                                      (IND-STATI-BATCH)
                          MOVE BBS-FLAG-TO-EXECUTE
                                       TO BAT-FLAG-TO-EXECUTE
                                                      (IND-STATI-BATCH)

                          SET IDSV0003-FETCH-NEXT   TO TRUE
                       END-IF

                    WHEN IDSV0003-NOT-FOUND
      *--->         CAMPO $ NON TROVATO
                      IF IDSV0003-FETCH-FIRST
                         INITIALIZE           WK-LOG-ERRORE

                         MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                         STRING 'ERRORE MODULO '
                                DELIMITED BY SIZE
                                PGM-IABS0070
                                DELIMITED BY SIZE
                                ' SQLCODE : '
                                DELIMITED BY SIZE
                                WK-SQLCODE
                                DELIMITED BY SIZE INTO
                                WK-LOR-DESC-ERRORE-ESTESA
                         END-STRING

                         MOVE ERRORE-FATALE  TO WK-LOR-ID-GRAVITA-ERRORE
                         PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                           THRU W070-EX
                       END-IF

                    WHEN IDSV0003-NEGATIVI
      *--->         ERRORE DI ACCESSO AL DB
                       INITIALIZE               WK-LOG-ERRORE

                       MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                       STRING 'ERRORE MODULO '
                              DELIMITED BY SIZE
                              PGM-IABS0070
                              DELIMITED BY SIZE
                              ' SQLCODE : '
                              DELIMITED BY SIZE
                              WK-SQLCODE
                              DELIMITED BY SIZE INTO
                              WK-LOR-DESC-ERRORE-ESTESA
                       END-STRING

                       MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                       PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                 END-EVALUATE
               ELSE
      *-->        GESTIRE ERRORE DISPATCHER
                    INITIALIZE  WK-LOG-ERRORE

                    STRING 'ERRORE MODULO '
                            DELIMITED BY SIZE
                            PGM-IABS0070
                            DELIMITED BY SIZE
                            ' - '
                            DELIMITED BY SIZE
                            IDSV0003-DESCRIZ-ERR-DB2
                            DELIMITED BY '   '
                            ' - '
                            DELIMITED BY SIZE
                            'RC : '
                            DELIMITED BY SIZE
                            IDSV0003-RETURN-CODE
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
               END-IF
            END-IF
           END-PERFORM.
      *
       C100-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESTRAI STATI JOB
      *----------------------------------------------------------------*
       C200-ESTRAI-STATI-JOB.
      *
           MOVE 'C200-ESTRAI-STATI-JOB'     TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> TIPO OPERAZIONE
           SET IDSV0003-FETCH-FIRST       TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

           MOVE ZEROES                    TO IND-STATI-JOB

      *----------------------------------------------------------------*
           PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC  OR
                         NOT IDSV0003-SUCCESSFUL-SQL OR
                         NOT WK-ERRORE-NO

             PERFORM CALL-IABS0030         THRU CALL-IABS0030-EX

           IF WK-ERRORE-NO
             IF IDSV0003-SUCCESSFUL-RC
                EVALUATE TRUE
                    WHEN IDSV0003-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                       ADD 1    TO IND-STATI-JOB
                       IF IND-STATI-JOB > WK-LIMITE-STATI-JOB
                          INITIALIZE WK-LOG-ERRORE

                          STRING 'OVERFLOW STATI '
                                 DELIMITED BY SIZE
                                 'ELAB_STATE'
                                 DELIMITED BY SIZE
                                 INTO
                                 WK-LOR-DESC-ERRORE-ESTESA
                          END-STRING

                          MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                          PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                                 THRU W070-EX
                       ELSE

                          MOVE BES-COD-ELAB-STATE
                                                 TO JOB-COD-ELAB-STATE
                                                      (IND-STATI-JOB)

                          MOVE BES-DES           TO JOB-DES
                                                      (IND-STATI-JOB)
                          MOVE BES-FLAG-TO-EXECUTE
                                                 TO JOB-FLAG-TO-EXECUTE
                                                      (IND-STATI-JOB)
                          SET IDSV0003-FETCH-NEXT   TO TRUE
                       END-IF

                    WHEN IDSV0003-NOT-FOUND
      *--->         CAMPO $ NON TROVATO
                       IF IDSV0003-FETCH-FIRST
                          INITIALIZE WK-LOG-ERRORE

                          MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                          STRING 'ERRORE MODULO '
                                 DELIMITED BY SIZE
                                 PGM-IABS0030
                                 DELIMITED BY SIZE
                                 ' SQLCODE : '
                                 DELIMITED BY SIZE
                                 WK-SQLCODE
                                 DELIMITED BY SIZE INTO
                                 WK-LOR-DESC-ERRORE-ESTESA
                          END-STRING

                          MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                          PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                             THRU W070-EX
                       END-IF

                    WHEN IDSV0003-NEGATIVI
      *--->         ERRORE DI ACCESSO AL DB
                       INITIALIZE WK-LOG-ERRORE

                       MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                       STRING 'ERRORE MODULO '
                              DELIMITED BY SIZE
                              PGM-IABS0030
                              DELIMITED BY SIZE
                              ' SQLCODE : '
                              DELIMITED BY SIZE
                              WK-SQLCODE
                              DELIMITED BY SIZE INTO
                              WK-LOR-DESC-ERRORE-ESTESA
                       END-STRING

                       MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                       PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                END-EVALUATE

             ELSE
      *-->      GESTIRE ERRORE DISPATCHER
                INITIALIZE WK-LOG-ERRORE

                STRING 'ERRORE MODULO  '
                        DELIMITED BY SIZE
                        PGM-IABS0030
                        DELIMITED BY SIZE
                        ' - '
                        DELIMITED BY SIZE
                        IDSV0003-DESCRIZ-ERR-DB2
                        DELIMITED BY '   '
                        ' - '
                        DELIMITED BY SIZE
                        'RC : '
                        DELIMITED BY SIZE
                        IDSV0003-RETURN-CODE
                        DELIMITED BY SIZE INTO
                        WK-LOR-DESC-ERRORE-ESTESA
                END-STRING

                MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
             END-IF
           END-IF
           END-PERFORM.
      *
       C200-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESTRAZIONE BATCH X COD-OMP-ANIA AND BATCH-TYPE
      *----------------------------------------------------------------*
       D000-ESTRAI-BATCH.
      *
           MOVE 'D000-ESTRAI-BATCH'         TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF  IABI0011-ACCESSO-BATCH NOT = SPACES AND
           NOT IABI0011-ACCESSO-ALL-BATCH
              PERFORM D006-CALL-IABS0040  THRU D006-EX
           ELSE
      *--> TIPO OPERAZIONE
              SET IDSV0003-FETCH-FIRST       TO TRUE

              PERFORM UNTIL WK-FINE-BTC-BATCH-YES OR
                            NOT WK-ERRORE-NO

                  PERFORM D005-CALL-IABS0040  THRU D005-EX

              END-PERFORM
           END-IF.
      *
       D000-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESTRAZIONE BATCH PER PROTOCOLLO
      *----------------------------------------------------------------*
       D001-ESTRAI-BATCH-PROT.
      *
           MOVE 'D001-ESTRAI-BATCH-PROT'    TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> TIPO OPERAZIONE
           SET IDSV0003-SELECT            TO TRUE.

           PERFORM D005-CALL-IABS0040     THRU D005-EX.
      *
       D001-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CALL AL MODULO IABS0040
      *----------------------------------------------------------------*
       D005-CALL-IABS0040.

           MOVE 'D005-CALL-IABS0040'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> LIVELLO OPERAZIONE
           SET IDSV0003-PRIMARY-KEY                   TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC                TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL               TO TRUE.

           SET WK-BAT-COLLECTION-KEY-OK               TO TRUE.
           SET WK-JOB-COLLECTION-KEY-OK               TO TRUE.

           SET VERIFICA-FINALE-JOBS-NO                TO TRUE

           PERFORM CALL-IABS0040         THRU CALL-IABS0040-EX

           IF WK-ERRORE-NO
              IF IDSV0003-SUCCESSFUL-RC
                EVALUATE TRUE
                    WHEN IDSV0003-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE

                       PERFORM D100-ELABORA-BATCH        THRU D100-EX
                       SET IDSV0003-FETCH-NEXT           TO TRUE

                    WHEN IDSV0003-NOT-FOUND
      *--->         CAMPO $ NON TROVATO
                         SET WK-FINE-BTC-BATCH-YES       TO TRUE

                    WHEN IDSV0003-NEGATIVI
      *--->         ERRORE DI ACCESSO AL DB
                         INITIALIZE WK-LOG-ERRORE

                         MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                         STRING 'ERRORE MODULO '
                                DELIMITED BY SIZE
                                PGM-IABS0040
                                DELIMITED BY SIZE
                                ' SQLCODE : '
                                DELIMITED BY SIZE
                                WK-SQLCODE
                                DELIMITED BY SIZE INTO
                                WK-LOR-DESC-ERRORE-ESTESA
                         END-STRING

                        MOVE ERRORE-FATALE   TO WK-LOR-ID-GRAVITA-ERRORE
                        PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                THRU W070-EX
                END-EVALUATE

              ELSE
      *-->      GESTIRE ERRORE DISPATCHER
                INITIALIZE WK-LOG-ERRORE

                STRING 'ERRORE MODULO '
                        DELIMITED BY SIZE
                        PGM-IABS0040
                        DELIMITED BY SIZE
                        ' - '
                        DELIMITED BY SIZE
                        IDSV0003-DESCRIZ-ERR-DB2
                        DELIMITED BY '   '
                        ' - '
                        DELIMITED BY SIZE
                        'RC : '
                        DELIMITED BY SIZE
                        IDSV0003-RETURN-CODE
                        DELIMITED BY SIZE INTO
                        WK-LOR-DESC-ERRORE-ESTESA
                END-STRING

                MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.
      *
       D005-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CALL AL MODULO IABS0040 X MIN/MAX DELLA DT-EFF/DT-INS
      *----------------------------------------------------------------*
       D006-CALL-IABS0040.

           MOVE 'D006-CALL-IABS0040'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> LIVELLO OPERAZIONE
           SET IDSV0003-WHERE-CONDITION-01 TO TRUE
           SET IDSV0003-WHERE-CONDITION    TO TRUE

           MOVE IABI0011-ACCESSO-BATCH     TO IDSV0003-BUFFER-WHERE-COND

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC                TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL               TO TRUE.

           SET WK-BAT-COLLECTION-KEY-OK               TO TRUE.
           SET WK-JOB-COLLECTION-KEY-OK               TO TRUE.

           SET VERIFICA-FINALE-JOBS-NO                TO TRUE

           PERFORM CALL-IABS0040         THRU CALL-IABS0040-EX

           IF WK-ERRORE-NO
              IF IDSV0003-SUCCESSFUL-RC
                EVALUATE TRUE
                    WHEN IDSV0003-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE

                       PERFORM D100-ELABORA-BATCH        THRU D100-EX

                    WHEN IDSV0003-NOT-FOUND
      *--->         CAMPO $ NON TROVATO
                         SET WK-FINE-BTC-BATCH-YES       TO TRUE

                    WHEN IDSV0003-NEGATIVI
      *--->         ERRORE DI ACCESSO AL DB
                         INITIALIZE WK-LOG-ERRORE

                         MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                         STRING 'ERRORE MODULO '
                                DELIMITED BY SIZE
                                PGM-IABS0040
                                DELIMITED BY SIZE
                                ' SQLCODE : '
                                DELIMITED BY SIZE
                                WK-SQLCODE
                                DELIMITED BY SIZE INTO
                                WK-LOR-DESC-ERRORE-ESTESA
                         END-STRING

                        MOVE ERRORE-FATALE   TO WK-LOR-ID-GRAVITA-ERRORE
                        PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                THRU W070-EX
                END-EVALUATE

              ELSE
      *-->      GESTIRE ERRORE DISPATCHER
                INITIALIZE WK-LOG-ERRORE

                STRING 'ERRORE MODULO '
                        DELIMITED BY SIZE
                        PGM-IABS0040
                        DELIMITED BY SIZE
                        ' - '
                        DELIMITED BY SIZE
                        IDSV0003-DESCRIZ-ERR-DB2
                        DELIMITED BY '   '
                        ' - '
                        DELIMITED BY SIZE
                        'RC : '
                        DELIMITED BY SIZE
                        IDSV0003-RETURN-CODE
                        DELIMITED BY SIZE INTO
                        WK-LOR-DESC-ERRORE-ESTESA
                END-STRING

                MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.
      *
       D006-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ELABORAZIONE BATCH
      *----------------------------------------------------------------*
       D100-ELABORA-BATCH.

           MOVE 'D100-ELABORA-BATCH'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           SET IABV0006-PRIMO-LANCIO        TO TRUE
           SET WK-FINE-ALIMENTAZIONE-NO     TO TRUE
      *
           MOVE BTC-DATA-BATCH            TO IABV0006-DATA-BATCH

           PERFORM D200-VERIFICA-BATCH THRU D200-EX

           IF WK-ERRORE-NO
            IF WK-VERIFICA-BATCH-OK

             MOVE BTC-ID-BATCH            TO REP-ID-BATCH
                                             IABV0006-ID-BATCH

             IF BTC-ID-RICH-NULL NOT = HIGH-VALUE AND
                BTC-ID-RICH      NOT = ZEROES

                SET IDSV0003-WHERE-CONDITION-02    TO TRUE
                PERFORM V100-VERIFICA-PRENOTAZIONE THRU V100-EX

             END-IF

             IF WK-VERIFICA-PRENOTAZIONE-OK

                SET WK-ELABORA-BATCH-YES TO TRUE
                MOVE RIC-FL-SIMULAZIONE  TO WK-SIMULAZIONE
                IF NOT WK-SIMULAZIONE-FITTIZIA AND
                   NOT WK-SIMULAZIONE-APPL     AND
                   NOT WK-SIMULAZIONE-NO
                   MOVE IABI0011-FLAG-SIMULAZIONE
                                        TO WK-SIMULAZIONE
                END-IF
                MOVE WK-SIMULAZIONE     TO IABV0006-FLAG-SIMULAZIONE

                IF IABI0011-RIP-CON-VERSIONAMENTO
                   MOVE BTC-ID-BATCH     TO WK-VERSIONING
                END-IF

                MOVE BTC-ID-BATCH        TO IABV0009-ID-BATCH

                PERFORM D300-ACCEDI-SU-BATCH-TYPE THRU D300-EX

                IF WK-ERRORE-NO
                   IF STAMPA-TESTATA-JCL-YES
                      PERFORM S100-TESTATA-JCL    THRU S100-EX
                      SET STAMPA-TESTATA-JCL-NO   TO TRUE
                   END-IF

                   IF WK-ERRORE-NO
                     PERFORM S170-SCRIVI-TESTATA-BATCH THRU S170-EX

                     IF WK-ERRORE-NO
                      IF GESTIONE-PARALLELISMO-YES
                         PERFORM P000-PARALLELISMO-INIZIALE
                                                     THRU P000-EX
                      END-IF

                      IF WK-ERRORE-NO
                        MOVE BATCH-IN-ESECUZIONE
                                      TO IABV0002-STATE-CURRENT
                        PERFORM A150-SET-CURRENT-TIMESTAMP
                                                THRU A150-EX
                        MOVE WS-TIMESTAMP-N     TO BTC-DT-START
                        MOVE IDSV0001-USER-NAME TO BTC-USER-START
                        PERFORM D400-UPD-BATCH  THRU D400-EX

                        IF WK-ERRORE-NO

                          IF BTC-ID-RICH-NULL NOT = HIGH-VALUE AND
                             BTC-ID-RICH      NOT = ZEROES
                             SET IDSV0003-WHERE-CONDITION-02 TO TRUE
                             SET IDSV0003-FIRST-ACTION       TO TRUE
                             MOVE IDSV0001-DATA-COMPETENZA
                                             TO RIC-TS-EFF-ESEC-RICH
                             PERFORM D950-AGGIORNA-PRENOTAZIONE
                                                         THRU D950-EX
                          END-IF

                          IF WK-ERRORE-NO
                            PERFORM Y000-COMMIT  THRU Y000-EX
                            MOVE BTC-ID-BATCH    TO WK-ID-BATCH

                            IF WK-ERRORE-NO
                               PERFORM E000-VERIFICA-JOB
                                                 THRU E000-EX
                            END-IF
                          END-IF
                        END-IF
                      END-IF
                     END-IF
                   END-IF
                END-IF

                IF WK-ERRORE-NO
                   PERFORM S900-REPORT01O-BATCH  THRU S900-EX
                END-IF

             END-IF
            END-IF
           END-IF.
      *
       D100-EX.
           EXIT.
      *----------------------------------------------------------------*
      * VERIFICA BATCH
      *----------------------------------------------------------------*
       D200-VERIFICA-BATCH.

           MOVE 'D200-VERIFICA-BATCH'       TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           SET WK-VERIFICA-BATCH-KO               TO TRUE
           IF BTC-COD-BATCH-STATE = BATCH-IN-ESECUZIONE
               IF GESTIONE-PARALLELISMO-YES
                  PERFORM P800-CNTL-INIZIALE      THRU P800-EX
               END-IF
           ELSE
               IF GESTIONE-PARALLELISMO-YES
                  PERFORM P800-CNTL-INIZIALE      THRU P800-EX
               ELSE
                  SET WK-VERIFICA-BATCH-OK           TO TRUE
               END-IF
           END-IF.
      *
       D200-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ACCEDI SU BATCH TYPE
      *----------------------------------------------------------------*
       D300-ACCEDI-SU-BATCH-TYPE.
      *
           MOVE 'D300-ACCEDI-SU-BATCH-TYPE' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> LIVELLO OPERAZIONE
           SET IDSV0003-PRIMARY-KEY           TO TRUE.

      *--> TIPO OPERAZIONE
           SET IDSV0003-SELECT                TO TRUE.

           MOVE BTC-COD-BATCH-TYPE            TO BBT-COD-BATCH-TYPE

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC        TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL       TO TRUE.

           PERFORM CALL-IABS0050              THRU CALL-IABS0050-EX

           IF WK-ERRORE-NO
              IF IDSV0003-SUCCESSFUL-RC

                 EVALUATE TRUE
                  WHEN IDSV0003-SUCCESSFUL-SQL
      *-->        OPERAZIONE ESEGUITA CORRETTAMENTE

                     IF WK-PGM-BUSINESS =
                        SPACES OR HIGH-VALUE OR LOW-VALUE
                        MOVE BBT-SERVICE-NAME
                                         TO WK-PGM-BUSINESS
                     END-IF

                     IF WK-PGM-GUIDA =
                        SPACES OR HIGH-VALUE OR LOW-VALUE
                        IF BBT-SERVICE-GUIDE-NAME NOT =
                           SPACES AND LOW-VALUES AND HIGH-VALUES

                           SET IABV0006-GUIDE-AD-HOC   TO TRUE

                           PERFORM S555-SWITCH-GUIDE-TYPE THRU S555-EX

                        END-IF
                     END-IF

                     IF NOT IABV0006-GUIDE-AD-HOC AND
                        NOT IABV0006-BY-BOOKING
                        MOVE IABI0011-BUFFER-WHERE-COND(01:08)
                                               TO WK-PGM-SERV-ROTTURA
                        MOVE IABI0011-BUFFER-WHERE-COND(09:08)
                                               TO WK-PGM-SERV-SECON-BUS
                        MOVE IABI0011-BUFFER-WHERE-COND(17:08)
                                               TO WK-PGM-SERV-SECON-ROTT
                     END-IF

                     IF BBT-TP-MOVI-NULL NOT = HIGH-VALUE AND
                        BBT-TP-MOVI NOT = ZEROES
                        MOVE BBT-TP-MOVI TO IDSV0001-TIPO-MOVIMENTO
                                            IDSV0003-TIPO-MOVIMENTO
                     END-IF

                  WHEN IDSV0003-NOT-FOUND
      *--->       CAMPO $ NON TROVATO
                       INITIALIZE WK-LOG-ERRORE

                       MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                       STRING 'ERRORE MODULO'
                              DELIMITED BY SIZE
                              PGM-IABS0050
                              DELIMITED BY SIZE
                              ' SQLCODE : '
                              DELIMITED BY SIZE
                              WK-SQLCODE
                              DELIMITED BY SIZE INTO
                              WK-LOR-DESC-ERRORE-ESTESA
                       END-STRING

                       MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                       PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX

                  WHEN IDSV0003-NEGATIVI
      *--->       ERRORE DI ACCESSO AL DB
                       INITIALIZE WK-LOG-ERRORE

                       MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                       STRING 'ERRORE MODULO'
                              DELIMITED BY SIZE
                              PGM-IABS0050
                              DELIMITED BY SIZE
                              ' SQLCODE : '
                              DELIMITED BY SIZE
                              WK-SQLCODE
                              DELIMITED BY SIZE INTO
                              WK-LOR-DESC-ERRORE-ESTESA
                       END-STRING

                       MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                       PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                END-EVALUATE

              ELSE
      *-->    GESTIRE ERRORE DISPATCHER
                 INITIALIZE WK-LOG-ERRORE

                 STRING 'ERRORE MODULO '
                         DELIMITED BY SIZE
                         PGM-IABS0050
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         IDSV0003-DESCRIZ-ERR-DB2
                         DELIMITED BY '   '
                         ' - '
                         DELIMITED BY SIZE
                         'RC : '
                         IDSV0003-RETURN-CODE
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.
      *
       D300-EX.
           EXIT.
      *----------------------------------------------------------------*
      * MESSA IN ESECUZIONE DEL BATCH
      *----------------------------------------------------------------*
       D400-UPD-BATCH.
      *
           MOVE 'D400-UPD-BATCH'            TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> LIVELLO OPERAZIONE
           SET IDSV0003-PRIMARY-KEY       TO TRUE.

      *--> TIPO OPERAZIONE
           SET IDSV0003-UPDATE            TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

           PERFORM CALL-IABS0040         THRU CALL-IABS0040-EX

           IF WK-ERRORE-NO
              IF IDSV0003-SUCCESSFUL-RC
                 EVALUATE TRUE
                     WHEN IDSV0003-SUCCESSFUL-SQL
      *-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                        CONTINUE

                     WHEN IDSV0003-NEGATIVI
      *--->          ERRORE DI ACCESSO AL DB
                          INITIALIZE WK-LOG-ERRORE

                          MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                          STRING 'ERRORE MODULO '
                                 DELIMITED BY SIZE
                                 PGM-IABS0040
                                 DELIMITED BY SIZE
                                 ' SQLCODE : '
                                 DELIMITED BY SIZE
                                 WK-SQLCODE
                                 DELIMITED BY SIZE INTO
                                 WK-LOR-DESC-ERRORE-ESTESA
                          END-STRING

                          MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                          PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                                     THRU W070-EX
                 END-EVALUATE
              ELSE
      *-->       GESTIRE ERRORE DISPATCHER
                 INITIALIZE WK-LOG-ERRORE

                 STRING 'ERRORE MODULO '
                         DELIMITED BY SIZE
                         PGM-IABS0040
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         IDSV0003-DESCRIZ-ERR-DB2
                         DELIMITED BY '   '
                         ' - '
                         DELIMITED BY SIZE
                         'RC : '
                         DELIMITED BY SIZE
                         IDSV0003-RETURN-CODE
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.
      *
       D400-EX.
           EXIT.

      *----------------------------------------------------------------*
      * VERIFICA ELABT STATE
      *----------------------------------------------------------------*
       D901-VERIFICA-ELAB-STATE.
      *
           MOVE 'D901-VERIFICA-ELAB-STATE'  TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           SET WK-RISULT-VERIF-ELAB-STATE-KO      TO TRUE

           PERFORM VARYING IND-STATI-JOB FROM 1 BY 1
              UNTIL IND-STATI-JOB > WK-LIMITE-STATI-JOB OR
                    JOB-COD-ELAB-STATE(IND-STATI-JOB) =
                   ZEROES OR HIGH-VALUE OR SPACES OR LOW-VALUE

                   IF JOB-COD-ELAB-STATE(IND-STATI-JOB) =
                                            WK-COD-ELAB-STATE

                     IF JOB-FLAG-TO-EXECUTE(IND-STATI-JOB) =
                                                     WK-ATTIVO
                       SET WK-RISULT-VERIF-ELAB-STATE-OK TO TRUE

                     END-IF

                   END-IF
           END-PERFORM.
      *
       D901-EX.
           EXIT.
      *----------------------------------------------------------------*
      * REPERISCI DESCRIZIONE BATCH STATE
      *----------------------------------------------------------------*
       D902-REPERISCI-DES-BATCH-STATE.
      *
           MOVE 'D902-REPERISCI-DES-BATCH-STATE' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO      TO TRUE
           MOVE WK-LABEL                        TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           SET WK-BATCH-STATE-FOUND-NO               TO TRUE
           PERFORM VARYING IND-STATI-BATCH FROM 1 BY 1
              UNTIL IND-STATI-BATCH > WK-LIMITE-STATI-BATCH OR
                    WK-BATCH-STATE-FOUND-YES                OR
                    BAT-COD-BATCH-STATE(IND-STATI-BATCH) =
                   ZEROES OR HIGH-VALUE OR SPACES OR LOW-VALUE

                   IF BAT-COD-BATCH-STATE(IND-STATI-BATCH) =
                                            WK-COD-BATCH-STATE
                      SET WK-BATCH-STATE-FOUND-YES     TO TRUE
                      MOVE BAT-DES(IND-STATI-BATCH)
                                          TO  WK-DES-BATCH-STATE
                   END-IF
           END-PERFORM.
      *
       D902-EX.
           EXIT.
      *----------------------------------------------------------------*
      * AGGIORNA PRENOTAZIONE
      *----------------------------------------------------------------*
       D950-AGGIORNA-PRENOTAZIONE.
      *
           MOVE 'D950-AGGIORNA-PRENOTAZIONE' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO   TO TRUE
           MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF WK-ERRORE-NO
      *--> TIPO OPERAZIONE
              SET IDSV0003-UPDATE            TO TRUE

      *-->    INIZIALIZZA CODICE DI RITORNO
              SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
              SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE
      *
              CALL PGM-IABS0900 USING IDSV0003
                                      IABV0002
                                      RICH
                                      IABV0003
              ON EXCEPTION
                 STRING 'ERRORE CHIAMATA MODULO '
                         DELIMITED BY SIZE
                         PGM-IABS0900
                         DELIMITED BY SIZE
                         ' - '
                          DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              END-CALL

              IF WK-ERRORE-NO
                 IF IDSV0003-SUCCESSFUL-RC
                    EVALUATE TRUE
                       WHEN IDSV0003-SUCCESSFUL-SQL
      *-->             OPERAZIONE ESEGUITA CORRETTAMENTE
                          CONTINUE

                       WHEN IDSV0003-NEGATIVI
      *--->            ERRORE DI ACCESSO AL DB
                          INITIALIZE WK-LOG-ERRORE

                          MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                          STRING 'ERRORE MODULO '
                                 DELIMITED BY SIZE
                                 PGM-IABS0900
                                 DELIMITED BY SIZE
                                 ' SQLCODE : '
                                 DELIMITED BY SIZE
                                 WK-SQLCODE
                                 DELIMITED BY SIZE INTO
                                 WK-LOR-DESC-ERRORE-ESTESA
                          END-STRING

                          MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                          PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                                    THRU W070-EX
                    END-EVALUATE

                 ELSE
      *-->          GESTIRE ERRORE DISPATCHER
                    INITIALIZE WK-LOG-ERRORE

                    STRING 'ERRORE MODULO '
                            DELIMITED BY SIZE
                            PGM-IABS0900
                            DELIMITED BY SIZE
                            ' - '
                            DELIMITED BY SIZE
                            IDSV0003-DESCRIZ-ERR-DB2
                            DELIMITED BY '   '
                            ' - '
                            DELIMITED BY SIZE
                            'RC : '
                            DELIMITED BY SIZE
                            IDSV0003-RETURN-CODE
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                 END-IF
              END-IF
           END-IF.
      *
       D950-EX.
           EXIT.

      *---------------------------------------------------------
      * TRASFORMA DA BATCH-STATE A PRENOTAZ-STATE
      *---------------------------------------------------------
       D951-TRASF-BATCH-TO-PRENOTAZ.
      *
           MOVE 'D951-TRASF-BATCH-TO-PRENOTAZ' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           SET WK-AGGIORNA-PRENOTAZ-YES       TO TRUE

           MOVE IABV0002-STATE-CURRENT        TO WK-STATE-CURRENT

           IF IABV0002-STATE-CURRENT = BATCH-ESEGUITO    OR
                                       BATCH-SIMULATO-OK

              MOVE ESEGUITA           TO IABV0002-STATE-CURRENT

           ELSE

              IF IABV0002-STATE-CURRENT = BATCH-DA-RIESEGUIRE OR
                                          BATCH-SIMULATO-KO

                 MOVE ESEGUITA-KO     TO  IABV0002-STATE-CURRENT

              ELSE

                 INITIALIZE WK-LOG-ERRORE

                 MOVE 'COD-BATCH-STATE NON VALIDO'
                                    TO WK-LOR-DESC-ERRORE-ESTESA

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX

              END-IF

           END-IF.
      *
       D951-EX.
           EXIT.
      *----------------------------------------------------------------
      * TRASFORMA PRENOTAZ-STATE A BATCH-STATE
      *----------------------------------------------------------------
       D952-TRASF-PRENOTAZ-TO-BATCH.
      *
           MOVE 'D952-TRASF-PRENOTAZ-TO-BATCH' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           MOVE WK-STATE-CURRENT TO IABV0002-STATE-CURRENT.
      *
       D952-EX.
           EXIT.
      *----------------------------------------------------------------
      * TRASFORMA PRENOTAZ-STATE A BATCH-STATE
      *----------------------------------------------------------------
       D953-INITIALIZE-X-BATCH.
      *
           MOVE 'D953-INITIALIZE-X-BATCH' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           SET PRIMA-VOLTA-YES               TO TRUE.
      *
       D953-EX.
           EXIT.
      *----------------------------------------------------------------
      * SESSION TABLE
      *----------------------------------------------------------------
       D999-SESSION-TABLE.
      *
           MOVE 'D999-SESSION-TABLE'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           SET IDSV0301-DECLARE              TO TRUE.

           PERFORM CALL-IDSS0300             THRU CALL-IDSS0300-EX.
      *
       D999-EX.
           EXIT.

      *----------------------------------------------------------------*
      * VERIFICA JOB
      *----------------------------------------------------------------*
       E000-VERIFICA-JOB.
      *
           MOVE 'E000-VERIFICA-JOB'         TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> TIPO OPERAZIONE
           SET IDSV0003-SELECT             TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC     TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL    TO TRUE.

           MOVE IABI0011-BUFFER-WHERE-COND
                                 TO IDSV0003-BUFFER-WHERE-COND

           PERFORM X000-CARICA-STATI-JOB     THRU X000-EX

           IF IABV0006-GUIDE-AD-HOC OR
              IABV0006-BY-BOOKING

              IF IABI0011-RIP-CON-VERSIONAMENTO
                 PERFORM V000-VALORIZZAZIONE-VERSIONE
                                             THRU V000-EX
              END-IF

              PERFORM N501-PRE-GUIDE-AD-HOC  THRU N501-EX

              IF WK-ERRORE-NO
                 PERFORM N509-SWITCH-GUIDE   THRU N509-EX
              END-IF

           ELSE
              MOVE BTC-ID-BATCH              TO BJS-ID-BATCH

              IF IABI0011-MACROFUNZIONALITA NOT =
                 SPACES AND LOW-VALUE AND HIGH-VALUE

                 MOVE IABI0011-MACROFUNZIONALITA
                                            TO BJS-COD-MACROFUNCT
                 SET VERIFICA-FINALE-JOBS-SI TO TRUE

              END-IF

              IF IABI0011-TIPO-MOVIMENTO IS NUMERIC AND
                 IABI0011-TIPO-MOVIMENTO NOT = ZEROES

                 MOVE IABI0011-TIPO-MOVIMENTO TO BJS-TP-MOVI
                 SET VERIFICA-FINALE-JOBS-SI   TO TRUE

              END-IF


              MOVE PGM-IABS0060              TO WK-PGM-ERRORE
              PERFORM CALL-IABS0060          THRU CALL-IABS0060-EX

           END-IF

           IF WK-ERRORE-NO
              IF IDSV0003-SUCCESSFUL-RC
                 EVALUATE TRUE
                 WHEN IDSV0003-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE

                    PERFORM F000-FASE-MICRO THRU F000-EX

                 WHEN IDSV0003-NOT-FOUND
      *--> CAMPO $ NON TROVATO
                    IF POST-GUIDE-X-JOB-NOT-FOUND-YES
                       PERFORM N504-POST-GUIDE-AD-HOC THRU N504-EX

                       PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX
                    END-IF

                    IF WK-ERRORE-NO
                       IF GESTIONE-PARALLELISMO-YES
                          PERFORM P100-PARALLELISMO-FINALE
                                                            THRU P100-EX

                          IF WK-ERRORE-NO
                             PERFORM P900-CNTL-FINALE    THRU P900-EX
                          END-IF
                       END-IF
                       IF BATCH-COMPLETE
                          IF WK-BAT-COLLECTION-KEY-OK AND
                             BATCH-STATE-OK

                             PERFORM I100-INIT-ESEGUITO THRU I100-EX

                          ELSE

                             PERFORM I200-INIT-DA-ESEGUIRE
                                THRU I200-EX

                          END-IF

                          PERFORM A150-SET-CURRENT-TIMESTAMP
                                                   THRU A150-EX
                          MOVE WS-TIMESTAMP-N      TO BTC-DT-END
                          PERFORM D400-UPD-BATCH   THRU D400-EX

                          IF WK-ERRORE-NO
                             IF BTC-ID-RICH-NULL NOT = HIGH-VALUE AND
                                BTC-ID-RICH      NOT = ZEROES
                                SET IDSV0003-WHERE-CONDITION-02 TO TRUE
                                PERFORM D951-TRASF-BATCH-TO-PRENOTAZ
                                                            THRU D951-EX
                                IF WK-AGGIORNA-PRENOTAZ-YES
                                   SET IDSV0003-NONE-ACTION    TO TRUE
                                   PERFORM D950-AGGIORNA-PRENOTAZIONE
                                                            THRU D950-EX
                                   IF WK-ERRORE-NO
                                    PERFORM D952-TRASF-PRENOTAZ-TO-BATCH
                                                            THRU D952-EX
                                   END-IF
                                END-IF
                             END-IF
                          END-IF
                       END-IF

                       IF WK-ERRORE-NO
                          PERFORM Z002-OPERAZ-FINALI-X-BATCH
                                                THRU Z002-EX
                          PERFORM D953-INITIALIZE-X-BATCH
                                                THRU D953-EX
                       END-IF

                       MOVE BTC-ID-BATCH TO WK-ID-BATCH

                       INITIALIZE WK-LOG-ERRORE

                       STRING 'NON ESISTONO DATI DA ELABORARE'
                              DELIMITED BY SIZE
                              ' X '
                              DELIMITED BY SIZE
                              'ID_BATCH : '
                              DELIMITED BY SIZE
                              WK-ID-BATCH
                              DELIMITED BY SIZE
                              INTO
                              WK-LOR-DESC-ERRORE-ESTESA
                       END-STRING

                       IF  WK-RC-08
                           CONTINUE
                       ELSE
                           SET WK-RC-04                          TO TRUE
                       END-IF

                       SET WK-ERRORE-NON-BLOCCANTE           TO TRUE
                       MOVE WARNING       TO WK-LOR-ID-GRAVITA-ERRORE
                       PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX

                       PERFORM Y000-COMMIT                  THRU Y000-EX
                    END-IF

                 WHEN IDSV0003-NEGATIVI
      *--> ERRORE DI ACCESSO AL DB
                    INITIALIZE WK-LOG-ERRORE

                    MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    STRING 'ERRORE MODULO '
                           DELIMITED BY SIZE
                           PGM-IABS0060
                           DELIMITED BY SIZE
                           ' SQLCODE : '
                           DELIMITED BY SIZE
                           WK-SQLCODE
                           DELIMITED BY SIZE INTO
                           WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX

                 END-EVALUATE
              ELSE
      *--> GESTIRE ERRORE DISPATCHER
                 INITIALIZE WK-LOG-ERRORE

                 STRING 'ERRORE MODULO '
                         DELIMITED BY SIZE
                         WK-PGM-ERRORE
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         IDSV0003-DESCRIZ-ERR-DB2
                         DELIMITED BY '   '
                         ' - '
                         DELIMITED BY SIZE
                         'RC : '
                         DELIMITED BY SIZE
                         IDSV0003-RETURN-CODE
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
ALEX       ELSE
              PERFORM  Z500-GESTIONE-ERR-MAIN  THRU Z500-EX
           END-IF.
      *
       E000-EX.
           EXIT.

      *----------------------------------------------------------------*
      * INIZIALIZZA CUSTOM COUNT
      *----------------------------------------------------------------*
       I000-INIT-CUSTOM-COUNT.

           MOVE 'I000-INIT-CUSTOM-COUNT'    TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM VARYING IABV0006-MAX-ELE-CUSTOM-COUNT FROM 1 BY 1
                     UNTIL IABV0006-MAX-ELE-CUSTOM-COUNT >
                           IABV0006-LIM-CUSTOM-COUNT-MAX

              MOVE SPACES TO IABV0006-CUSTOM-COUNT-DESC
                            (IABV0006-MAX-ELE-CUSTOM-COUNT)
              MOVE ZEROES TO IABV0006-CUSTOM-COUNT
                            (IABV0006-MAX-ELE-CUSTOM-COUNT)

           END-PERFORM

           MOVE ZEROES    TO IABV0006-MAX-ELE-CUSTOM-COUNT.

       I000-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CARICA STATI JOB
      *----------------------------------------------------------------*
       X000-CARICA-STATI-JOB.

           MOVE 'X000-CARICA-STATI-JOB'     TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           INITIALIZE IABV0002-STATE-GLOBAL.

           MOVE ZEROES                  TO IND-STATI-V0002
           PERFORM VARYING IND-STATI-JOB FROM 1 BY 1
                   UNTIL IND-STATI-JOB > WK-LIMITE-STATI-JOB OR
                   JOB-COD-ELAB-STATE(IND-STATI-JOB) =
                   SPACES OR HIGH-VALUE OR LOW-VALUE OR ZERO

                    ADD 1               TO IND-STATI-V0002
                    MOVE JOB-COD-ELAB-STATE(IND-STATI-JOB)
                         TO IABV0002-STATE-ELE(IND-STATI-V0002)
           END-PERFORM.
      *
       X000-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CARICA STATI BATCH
      *----------------------------------------------------------------*
       X001-CARICA-STATI-BATCH.

           MOVE 'X001-CARICA-STATI-BATCH'   TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           INITIALIZE IABV0002-STATE-GLOBAL.

           MOVE ZEROES                  TO IND-STATI-V0002
           PERFORM VARYING IND-STATI-BATCH FROM 1 BY 1
                   UNTIL (IND-STATI-BATCH > WK-LIMITE-STATI-BATCH - 1)
                   OR     BAT-COD-BATCH-STATE(IND-STATI-BATCH) =
                          SPACES OR HIGH-VALUE OR LOW-VALUE OR ZERO

                   IF BAT-FLAG-TO-EXECUTE(IND-STATI-BATCH) =
                                                     WK-ATTIVO
                      ADD 1               TO IND-STATI-V0002
                      MOVE BAT-COD-BATCH-STATE(IND-STATI-BATCH)
                           TO IABV0002-STATE-ELE(IND-STATI-V0002)
                   END-IF
           END-PERFORM.
      *
       X001-EX.
           EXIT.

      *----------------------------------------------------------------*
      * GESTIONE T1
      *----------------------------------------------------------------*
       F000-FASE-MICRO.

           MOVE 'F000-FASE-MICRO'           TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           PERFORM F100-ESTRAI-LIV-ORG               THRU F100-EX.

           IF WK-ERRORE-NO
              PERFORM B500-ELABORA-MICRO             THRU B500-EX

              IF WK-ERRORE-NO
                 IF GESTIONE-PARALLELISMO-YES
                    PERFORM P100-PARALLELISMO-FINALE THRU P100-EX

                    IF WK-ERRORE-NO
                       PERFORM P900-CNTL-FINALE      THRU P900-EX
                    END-IF
                 END-IF

                 IF WK-ERRORE-NO
                    IF BATCH-COMPLETE

                       IF WK-BAT-COLLECTION-KEY-OK AND
                          BATCH-STATE-OK
                          IF WK-RC-04-NON-FORZATO
                             IF  WK-RC-08
                                 CONTINUE
                             ELSE
                                 SET WK-RC-04           TO TRUE
                             END-IF
                          ELSE
                             IF  WK-RC-04 OR
                                 WK-RC-08
                                 CONTINUE
                             ELSE
                                 SET WK-RC-00           TO TRUE
                             END-IF
                          END-IF

                          PERFORM I100-INIT-ESEGUITO THRU I100-EX
                       ELSE
                          IF WK-ERRORE-FATALE
                             SET WK-RC-12         TO TRUE
                          ELSE
                             IF  IABI0011-FORZ-RC-04-YES OR
                                 WK-RC-04-FORZATO
                                 SET WK-RC-04         TO TRUE
                             ELSE
                                 SET WK-RC-08         TO TRUE
                             END-IF
                          END-IF
                          IF WK-ERRORE-FATALE
                             PERFORM I200-INIT-DA-ESEGUIRE
                                THRU I200-EX
                          ELSE
                             IF  IABI0011-FORZ-RC-04-YES OR
                                 WK-RC-04-FORZATO
                                PERFORM I100-INIT-ESEGUITO THRU I100-EX
                             ELSE
                                PERFORM I200-INIT-DA-ESEGUIRE
                                   THRU I200-EX
                             END-IF
                          END-IF
                       END-IF

                       PERFORM A150-SET-CURRENT-TIMESTAMP THRU A150-EX
                       MOVE WS-TIMESTAMP-N        TO   BTC-DT-END

                       IF WK-SIMULAZIONE-INFR
                          PERFORM Y200-ROLLBACK   THRU Y200-EX
                       END-IF

                       PERFORM D400-UPD-BATCH     THRU D400-EX

                       IF WK-ERRORE-NO
                          IF BTC-ID-RICH-NULL NOT = HIGH-VALUE AND
                             BTC-ID-RICH      NOT = ZEROES
                             SET IDSV0003-WHERE-CONDITION-02 TO TRUE
                             PERFORM D951-TRASF-BATCH-TO-PRENOTAZ
                                                            THRU D951-EX
                             IF WK-AGGIORNA-PRENOTAZ-YES
                                SET IDSV0003-NONE-ACTION       TO TRUE
                                PERFORM D950-AGGIORNA-PRENOTAZIONE
                                                            THRU D950-EX
                                IF WK-ERRORE-NO
                                   PERFORM D952-TRASF-PRENOTAZ-TO-BATCH
                                                            THRU D952-EX
                                END-IF
                             END-IF
                          END-IF
                          IF WK-ERRORE-NO
                             PERFORM Z002-OPERAZ-FINALI-X-BATCH
                                                   THRU Z002-EX
                             PERFORM D953-INITIALIZE-X-BATCH
                                                   THRU D953-EX
                          END-IF
                       END-IF

                    END-IF

                    IF WK-ERRORE-NO
                       PERFORM Y000-COMMIT        THRU Y000-EX
                       IF (IABV0006-STD-TYPE-REC-NO   OR
                           IABV0006-STD-TYPE-REC-YES) AND
                           IDSV0001-ARCH-BATCH-DBG
                           MOVE WK-ID-JOB      TO WK-ID-JOB-ULT-COMMIT
                       END-IF
                       IF WK-ERRORE-NO
                          PERFORM Y050-CONTROLLO-COMMIT THRU Y050-EX
                       END-IF
                    END-IF
                 END-IF
              END-IF
           END-IF.
      *
       F000-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ESTRAI JOB
      *----------------------------------------------------------------*
       F100-ESTRAI-LIV-ORG.
      *
           MOVE 'F100-ESTRAI-LIV-ORG'       TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> TIPO OPERAZIONE
           SET IDSV0003-SELECT            TO TRUE.

           MOVE IABI0011-GRUPPO-AUTORIZZANTE TO GRU-COD-GRU-ARZ.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

      *
           MOVE PGM-IDES0020              TO WK-PGM-ERRORE
           CALL PGM-IDES0020              USING IDSV0003
                                          GRU-ARZ
           ON EXCEPTION
              STRING 'ERRORE CHIAMATA MODULO '
                   DELIMITED BY SIZE
                   PGM-IDES0020
                   DELIMITED BY SIZE
                   ' - '
                    DELIMITED BY SIZE INTO
                   WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
           END-CALL

           IF WK-ERRORE-NO
              IF IDSV0003-SUCCESSFUL-RC
                 EVALUATE TRUE
                 WHEN IDSV0003-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE

                    MOVE GRU-COD-LIV-OGZ TO IABV0006-COD-LIV-AUT-PROFIL
                    MOVE IABI0011-GRUPPO-AUTORIZZANTE
                                         TO IABV0006-COD-GRU-AUT-PROFIL
                    MOVE IABI0011-CANALE-VENDITA
                                  TO IABV0006-CANALE-VENDITA
                    MOVE IABI0011-PUNTO-VENDITA
                                  TO IABV0006-PUNTO-VENDITA

                 WHEN IDSV0003-NEGATIVI
      *--->      ERRORE DI ACCESSO AL DB
                    INITIALIZE WK-LOG-ERRORE

                    MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    STRING 'ERRORE MODULO '
                           DELIMITED BY SIZE
                           PGM-IDES0020
                           DELIMITED BY SIZE
                           ' SQLCODE : '
                           DELIMITED BY SIZE
                           WK-SQLCODE
                           DELIMITED BY SIZE INTO
                           WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX

                 END-EVALUATE
              ELSE
      *--> GESTIRE ERRORE CALL

                 INITIALIZE WK-LOG-ERRORE

                 STRING 'ERRORE MODULO '
                         DELIMITED BY SIZE
                         WK-PGM-ERRORE
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         IDSV0003-DESCRIZ-ERR-DB2
                         DELIMITED BY '   '
                         ' - '
                         DELIMITED BY SIZE
                         'RC : '
                         DELIMITED BY SIZE
                         IDSV0003-RETURN-CODE
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.
      *
       F100-EX.
           EXIT.
      *****************************************************************
       I100-INIT-ESEGUITO.

           MOVE 'I100-INIT-ESEGUITO'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           SET WK-CONFERMA-BATCH-ESEGUITO-OK TO TRUE

           IF WK-SIMULAZIONE-INFR OR
               WK-SIMULAZIONE-APPL
              MOVE BATCH-SIMULATO-OK   TO IABV0002-STATE-CURRENT
           ELSE
              IF NOT IABI0011-SENZA-RIPARTENZA

                 IF VERIFICA-FINALE-JOBS-SI
                     PERFORM I300-VERIFICA-FINALE-JOBS THRU I300-EX
                 END-IF
              END-IF

              IF WK-ERRORE-NO
                 IF WK-CONFERMA-BATCH-ESEGUITO-OK
                    MOVE BATCH-ESEGUITO TO IABV0002-STATE-CURRENT
                 END-IF
              END-IF
           END-IF.
      *
       I100-EX.
           EXIT.
      *****************************************************************
       I200-INIT-DA-ESEGUIRE.

           MOVE 'I200-INIT-DA-ESEGUIRE'     TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF WK-SIMULAZIONE-INFR OR
              WK-SIMULAZIONE-APPL
              MOVE BATCH-SIMULATO-KO   TO IABV0002-STATE-CURRENT
           ELSE
              MOVE BATCH-DA-RIESEGUIRE TO IABV0002-STATE-CURRENT
           END-IF.
      *
       I200-EX.
           EXIT.
      *****************************************************************
       I300-VERIFICA-FINALE-JOBS.
      *
           MOVE 'I300-VERIFICA-FINALE-JOB'  TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> TIPO OPERAZIONE
           SET IDSV0003-SELECT             TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC     TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL    TO TRUE.

           MOVE IABI0011-BUFFER-WHERE-COND
                                 TO IDSV0003-BUFFER-WHERE-COND

           IF IABV0006-GUIDE-AD-HOC OR
              IABV0006-BY-BOOKING

              IF IABI0011-RIP-CON-VERSIONAMENTO
                 PERFORM V000-VALORIZZAZIONE-VERSIONE
                                                THRU V000-EX
              END-IF

              PERFORM N501-PRE-GUIDE-AD-HOC     THRU N501-EX

              MOVE HIGH-VALUE            TO RIC-COD-MACROFUNCT
              MOVE ZEROES                TO RIC-TP-MOVI

              IF WK-ERRORE-NO
                 PERFORM N509-SWITCH-GUIDE      THRU N509-EX
              END-IF

           ELSE
              MOVE BTC-ID-BATCH          TO BJS-ID-BATCH

              MOVE HIGH-VALUE            TO BJS-COD-MACROFUNCT-NULL
                                            BJS-TP-MOVI-NULL

              MOVE PGM-IABS0060          TO WK-PGM-ERRORE
              PERFORM CALL-IABS0060      THRU CALL-IABS0060-EX

           END-IF

           IF WK-ERRORE-NO
              IF IDSV0003-SUCCESSFUL-RC
                 EVALUATE TRUE

                 WHEN IDSV0003-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    SET WK-CONFERMA-BATCH-ESEGUITO-KO TO TRUE

                 WHEN IDSV0003-NOT-FOUND
      *--->      CAMPO $ NON TROVATO
                    SET WK-CONFERMA-BATCH-ESEGUITO-OK TO TRUE

                 WHEN IDSV0003-NEGATIVI
      *--->      ERRORE DI ACCESSO AL DB
                    INITIALIZE WK-LOG-ERRORE

                    MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    STRING 'ERRORE MODULO '
                           DELIMITED BY SIZE
                           PGM-IABS0060
                           DELIMITED BY SIZE
                           'SQLCODE : '
                           DELIMITED BY SIZE
                           WK-SQLCODE
                           DELIMITED BY SIZE INTO
                           WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX

                 END-EVALUATE
              ELSE
      *-->       GESTIRE ERRORE DISPATCHER
                 INITIALIZE WK-LOG-ERRORE

                 STRING 'ERRORE MODULO '
                         DELIMITED BY SIZE
                         WK-PGM-ERRORE
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         IDSV0003-DESCRIZ-ERR-DB2
                         DELIMITED BY '   '
                         ' - '
                         DELIMITED BY SIZE
                         'RC : '
                         DELIMITED BY SIZE
                         IDSV0003-RETURN-CODE
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.
      *
       I300-EX.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE PARALLELISMO INIZIALE
      *----------------------------------------------------------------*
       P000-PARALLELISMO-INIZIALE.
      *
           MOVE 'P000-PARALLELISMO-INIZIALE' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO   TO TRUE
           MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE IABI0011-CODICE-COMPAGNIA-ANIA TO BPA-COD-COMP-ANIA
           MOVE BTC-PROTOCOL                   TO BPA-PROTOCOL
           MOVE IABI0011-PROG-PROTOCOL         TO BPA-PROG-PROTOCOL

           PERFORM P050-PREPARA-FASE-INIZIALE  THRU P050-EX.

           PERFORM P500-CALL-IABS0130          THRU P500-EX.

           PERFORM P051-ESITO-PARALLELO        THRU P051-EX.
      *
       P000-EX.
           EXIT.
      *----------------------------------------------------------------*
      * GESTIONE PARALLELISMO FINALE
      *----------------------------------------------------------------*
       P100-PARALLELISMO-FINALE.
      *
           MOVE 'P100-PARALLELISMO-FINALE'  TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE IABI0011-CODICE-COMPAGNIA-ANIA TO BPA-COD-COMP-ANIA
           MOVE BTC-PROTOCOL                   TO BPA-PROTOCOL

           PERFORM P150-PREPARA-FASE-FINALE    THRU P150-EX.

           IF WK-ERRORE-NO
              PERFORM P500-CALL-IABS0130          THRU P500-EX
              PERFORM P051-ESITO-PARALLELO        THRU P051-EX
           END-IF.
      *
       P100-EX.
           EXIT.
      *----------------------------------------------------------------*
      * PREPARA FASE INIZIALE
      *----------------------------------------------------------------*
       P050-PREPARA-FASE-INIZIALE.
      *
           MOVE 'P050-PREPARA-FASE-INIZIALE'  TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO    TO TRUE
           MOVE WK-LABEL                      TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> TIPO OPERAZIONE
           SET WS-FASE-INIZIALE               TO TRUE.

           PERFORM X000-CARICA-STATI-JOB     THRU X000-EX

           MOVE WS-BUFFER-WHERE-COND-IABS0130
                                      TO IDSV0003-BUFFER-WHERE-COND.
      *
       P050-EX.
           EXIT.
      *----------------------------------------------------------------*
      * PREPARA FASE FINALE
      *----------------------------------------------------------------*
       P150-PREPARA-FASE-FINALE.
      *
           MOVE 'P150-PREPARA-FASE-FINALE'  TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> TIPO OPERAZIONE
           SET WS-FASE-FINALE               TO TRUE.

           INITIALIZE IABV0002-STATE-GLOBAL
           MOVE JOB-IN-ESECUZIONE           TO IABV0002-STATE-01

           IF WK-BAT-COLLECTION-KEY-OK
              PERFORM I100-INIT-ESEGUITO    THRU I100-EX
           ELSE
              PERFORM I200-INIT-DA-ESEGUIRE THRU I200-EX
           END-IF

           IF WK-ERRORE-NO
              MOVE IABV0002-STATE-CURRENT   TO BPA-COD-BATCH-STATE
              MOVE WS-BUFFER-WHERE-COND-IABS0130
                                     TO IDSV0003-BUFFER-WHERE-COND
           END-IF.
      *
       P150-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CALL IABS0130
      *----------------------------------------------------------------*
       P500-CALL-IABS0130.
      *
           MOVE 'P500-CALL-IABS0130'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

           CALL PGM-IABS0130 USING IDSV0003
                                   IABV0002
                                   BTC-PARALLELISM
           ON EXCEPTION
              STRING 'ERRORE CHIAMATA MODULO '
                   DELIMITED BY SIZE
                   PGM-IABS0130
                   DELIMITED BY SIZE
                   ' - '
                    DELIMITED BY SIZE INTO
                   WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
           END-CALL.
      *
       P500-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CONTROLLA ESITO PARALLELISMO
      *----------------------------------------------------------------*
       P051-ESITO-PARALLELO.

           MOVE 'P051-ESITO-PARALLELO'      TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF WK-ERRORE-NO
              IF IDSV0003-SUCCESSFUL-RC
                 EVALUATE TRUE
                 WHEN IDSV0003-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                    CONTINUE

                 WHEN IDSV0003-NOT-FOUND
      *--> CAMPO $ NON TROVATO

                    INITIALIZE WK-LOG-ERRORE

                    MOVE BPA-COD-COMP-ANIA TO COMODO-COD-COMP-ANIA
                    MOVE BPA-PROG-PROTOCOL TO COMODO-PROG-PROTOCOL

                    MOVE IDSV0003-SQLCODE TO WK-SQLCODE

                    STRING 'ERRORE MODULO '
                           DELIMITED BY SIZE
                           PGM-IABS0130
                           DELIMITED BY SIZE
                           ' SQLCODE : '
                           WK-SQLCODE
                           DELIMITED BY SIZE
                           ' '
                           DELIMITED BY SIZE
                           COMODO-COD-COMP-ANIA
                           ' / '
                           DELIMITED BY SIZE
                           BPA-PROTOCOL
                           DELIMITED BY SPACES
                           ' / '
                           DELIMITED BY SIZE
                           COMODO-PROG-PROTOCOL
                           DELIMITED BY SPACES
                           INTO
                           WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX

                 WHEN IDSV0003-NEGATIVI
      *--->      ERRORE DI ACCESSO AL DB
                    INITIALIZE WK-LOG-ERRORE

                    MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    STRING 'ERRORE MODULO '
                           DELIMITED BY SIZE
                           PGM-IABS0130
                           DELIMITED BY SIZE
                           ' SQLCODE : '
                           WK-SQLCODE
                           DELIMITED BY SIZE INTO
                           WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                   MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                   PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                 END-EVALUATE

              ELSE
      *-->       GESTIRE ERRORE DISPATCHER
                 INITIALIZE WK-LOG-ERRORE

                 STRING 'ERRORE MODULO '
                         DELIMITED BY SIZE
                         PGM-IABS0130
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         IDSV0003-DESCRIZ-ERR-DB2
                         DELIMITED BY '   '
                         ' - '
                         DELIMITED BY SIZE
                         'RC : '
                         IDSV0003-RETURN-CODE
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.
      *
       P051-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CONTROLLA ESITO PARALLELISMO SU CONTROLLO INIZIALE
      *----------------------------------------------------------------*
       P899-ESITO-CNTL-INIZIALE.

           MOVE 'P899-ESITO-CNTL-INIZIALE'  TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF WK-ERRORE-NO
              IF IDSV0003-SUCCESSFUL-RC
                 EVALUATE TRUE
                 WHEN IDSV0003-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                    SET WK-VERIFICA-BATCH-OK     TO TRUE
                    MOVE IDSV0003-BUFFER-WHERE-COND
                                  TO WS-BUFFER-WHERE-COND-IABS0130

                    IF BPA-ID-OGG-DA IS NUMERIC AND
                       BPA-ID-OGG-DA NOT = ZEROES
                       MOVE BPA-ID-OGG-DA TO IABV0009-ID-OGG-DA
                    END-IF

                    IF BPA-ID-OGG-A  IS NUMERIC AND
                       BPA-ID-OGG-A  NOT = ZEROES
                       MOVE BPA-ID-OGG-A  TO IABV0009-ID-OGG-A
                    END-IF

                    MOVE BPA-TP-OGG       TO IABV0009-TP-OGG

      *              IF BATCH-COMPLETE
      *                 INITIALIZE WK-LOG-ERRORE

      *                 MOVE IDSV0003-SQLCODE TO WK-SQLCODE
      *                 STRING 'INCONG. BTC_BATCH - BTC_PARALLELISM'
      *                        ' - '
      *                        'RC : '
      *                        IDSV0003-RETURN-CODE
      *                        DELIMITED BY SIZE INTO
      *                        WK-LOR-DESC-ERRORE-ESTESA
      *                 END-STRING

      *                 MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERROR
      *                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
      *                          THRU W070-EX
      *              END-IF

                 WHEN IDSV0003-NOT-FOUND
      *--> CAMPO $ NON TROVATO

      *--> ERRORE DI ACCESSO AL DB
                    INITIALIZE WK-LOG-ERRORE

                    MOVE BPA-COD-COMP-ANIA TO COMODO-COD-COMP-ANIA
                    MOVE BPA-PROG-PROTOCOL TO COMODO-PROG-PROTOCOL

                    MOVE IDSV0003-SQLCODE  TO WK-SQLCODE

                    STRING 'ERRORE MODULO '
                           DELIMITED BY SIZE
                           PGM-IABS0130
                           DELIMITED BY SIZE
                           ' SQLCODE : '
                           WK-SQLCODE
                           DELIMITED BY SIZE
                           ' '
                           DELIMITED BY SIZE
                           COMODO-COD-COMP-ANIA
                           ' / '
                           DELIMITED BY SIZE
                           BPA-PROTOCOL
                           DELIMITED BY SPACES
                           INTO
                           WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX

                 WHEN IDSV0003-NEGATIVI
      *--> ERRORE DI ACCESSO AL DB
                    INITIALIZE WK-LOG-ERRORE

                    MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    STRING 'ERRORE MODULO '
                           DELIMITED BY SIZE
                           PGM-IABS0130
                           DELIMITED BY SIZE
                           ' SQLCODE : '
                           DELIMITED BY SIZE
                           WK-SQLCODE
                           DELIMITED BY SIZE INTO
                           WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                   MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                   PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                 END-EVALUATE
              ELSE
      *--> GESTIRE ERRORE DISPATCHER
                 INITIALIZE WK-LOG-ERRORE

                 STRING 'ERRORE MODULO '
                         DELIMITED BY SIZE
                         PGM-IABS0130
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         IDSV0003-DESCRIZ-ERR-DB2
                         DELIMITED BY '   '
                         ' - '
                         DELIMITED BY SIZE
                         'RC : '
                         DELIMITED BY SIZE
                         IDSV0003-RETURN-CODE
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.
      *
       P899-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CONTROLLA ESITO PARALLELISMO SU CONTROLLO FINALE
      *----------------------------------------------------------------*
       P951-ESITO-CNTL-FINALE.

           MOVE 'P951-ESITO-CNTL-FINALE'    TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF WK-ERRORE-NO
              IF IDSV0003-SUCCESSFUL-RC
                 EVALUATE TRUE
                 WHEN IDSV0003-SUCCESSFUL-SQL
      *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                    MOVE IDSV0003-BUFFER-WHERE-COND
                                    TO WS-BUFFER-WHERE-COND-IABS0130

                 WHEN IDSV0003-NOT-FOUND
      *--> CAMPO $ NON TROVATO

      *--> ERRORE DI ACCESSO AL DB
                    INITIALIZE WK-LOG-ERRORE

                    MOVE BPA-COD-COMP-ANIA TO COMODO-COD-COMP-ANIA
                    MOVE BPA-PROG-PROTOCOL TO COMODO-PROG-PROTOCOL

                    MOVE IDSV0003-SQLCODE TO WK-SQLCODE

                    STRING 'ERRORE MODULO '
                           DELIMITED BY SIZE
                           PGM-IABS0130
                           DELIMITED BY SIZE
                           ' '
                           DELIMITED BY SIZE
                           COMODO-COD-COMP-ANIA
                           DELIMITED BY SIZE
                           ' / '
                           DELIMITED BY SIZE
                           BPA-PROTOCOL
                           DELIMITED BY SPACES
                           ' / '
                           DELIMITED BY SIZE
                           COMODO-PROG-PROTOCOL
                           DELIMITED BY SPACES
                           INTO
                           WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX

                 WHEN IDSV0003-NEGATIVI
      *--->      ERRORE DI ACCESSO AL DB
                    INITIALIZE WK-LOG-ERRORE

                    MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    STRING 'ERRORE MODULO '
                           DELIMITED BY SIZE
                           PGM-IABS0130
                           DELIMITED BY SIZE
                           ' SQLCODE : '
                           DELIMITED BY SIZE
                           WK-SQLCODE
                           DELIMITED BY SIZE INTO
                           WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                   MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                   PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                 END-EVALUATE
              ELSE
      *-->       GESTIRE ERRORE DISPATCHER
                 INITIALIZE WK-LOG-ERRORE

                 STRING 'ERRORE MODULO '
                         DELIMITED BY SIZE
                         PGM-IABS0130
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         IDSV0003-DESCRIZ-ERR-DB2
                         DELIMITED BY '   '
                         ' - '
                         DELIMITED BY SIZE
                         'RC : '
                         DELIMITED BY SIZE
                         IDSV0003-RETURN-CODE
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.
      *
       P951-EX.
           EXIT.
      *----------------------------------------------------------------*
      * FASE CONTROLLO INIZIALE
      *----------------------------------------------------------------*
       P800-CNTL-INIZIALE.
      *
           MOVE 'P800-CNTL-INIZIALE'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE IABI0011-CODICE-COMPAGNIA-ANIA TO BPA-COD-COMP-ANIA
           MOVE BTC-PROTOCOL                   TO BPA-PROTOCOL
           MOVE IABI0011-PROG-PROTOCOL         TO BPA-PROG-PROTOCOL

           PERFORM P850-PREPARA-CNTL-INIZIALE  THRU P850-EX.

           PERFORM P500-CALL-IABS0130          THRU P500-EX.

           PERFORM P899-ESITO-CNTL-INIZIALE    THRU P899-EX.
      *
       P800-EX.
           EXIT.
      *----------------------------------------------------------------*
      * PREPARA FASE CONTROLLO
      *----------------------------------------------------------------*
       P850-PREPARA-CNTL-INIZIALE.
      *
           MOVE 'P850-PREPARA-CNTL-INIZIALE'  TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO    TO TRUE
           MOVE WK-LABEL                      TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> TIPO OPERAZIONE
           SET IDSV0003-WHERE-CONDITION       TO TRUE.
           SET WS-FASE-CONTROLLO-INIZIALE     TO TRUE.

           MOVE WS-BUFFER-WHERE-COND-IABS0130
                                      TO IDSV0003-BUFFER-WHERE-COND.
      *
       P850-EX.
           EXIT.
      *----------------------------------------------------------------*
      * TRATTA TESTATA JCL
      *----------------------------------------------------------------*
       S100-TESTATA-JCL.
      *
           MOVE 'S100-TESTATA-JCL'          TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF WK-ERRORE-NO
              PERFORM OPEN-REPORT01          THRU OPEN-REPORT01-EX

              IF WK-ERRORE-NO
                 PERFORM S149-SCRIVI-TESTATA-JCL THRU S149-EX
              END-IF

           END-IF.
      *
       S100-EX.
           EXIT.
      *----------------------------------------------------------------*
      * SCRIVI TESTATA JCL
      *----------------------------------------------------------------*
       S149-SCRIVI-TESTATA-JCL.
      *
           MOVE 'S149-SCRIVI-TESTATA-JCL'   TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM S151-PREPARA-TESTATA-JCL  THRU S151-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE ALL '*'                  TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX


           MOVE SPACES                   TO REPORT01-REC
           MOVE '* '                     TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE LOGO-JCL-REPORT          TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE '* '                     TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE DATA-ELABORAZIONE-REPORT TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX


           MOVE SPACES                   TO REPORT01-REC
           MOVE ORA-ELABORAZIONE-REPORT  TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX


           MOVE SPACES                   TO REPORT01-REC
           MOVE COD-COMPAGNIA-REPORT     TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX


           MOVE SPACES                   TO REPORT01-REC
           MOVE DESC-BATCH-REPORT        TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX


           MOVE SPACES                   TO REPORT01-REC
           MOVE USER-NAME-REPORT         TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX


           MOVE SPACES                   TO REPORT01-REC
           MOVE GRUPPO-AUT-REPORT        TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX


           IF REP-MACROFUNZIONALITA NOT =
              SPACES AND LOW-VALUE AND HIGH-VALUE
              MOVE SPACES                   TO REPORT01-REC
              MOVE MACROFUNZIONALITA-REPORT TO REPORT01-REC
              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
           END-IF

           IF REP-TIPO-MOVIMENTO IS NUMERIC AND
              REP-TIPO-MOVIMENTO NOT = ZEROES
              MOVE SPACES                   TO REPORT01-REC
              MOVE TIPO-MOVIMENTO-REPORT    TO REPORT01-REC
              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
           END-IF

           MOVE SPACES                   TO REPORT01-REC
           MOVE DATA-EFFETTO-REPORT      TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE TIMESTAMP-COMPETENZA-REPORT
                                         TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE SIMULAZIONE-REPORT       TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE CANALE-VENDITA-REPORT    TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX


           MOVE SPACES                   TO REPORT01-REC
           MOVE PUNTO-VENDITA-REPORT     TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX


           MOVE SPACES                   TO REPORT01-REC
           MOVE '* '                     TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX


           MOVE SPACES                   TO REPORT01-REC
           MOVE ALL '*'                  TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX


           MOVE SPACES                   TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX


           MOVE SPACES                   TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX.
      *
       S149-EX.
           EXIT.
      *----------------------------------------------------------------*
      * PREPARA TESTATA JCL
      *----------------------------------------------------------------*
       S151-PREPARA-TESTATA-JCL.
      *
           MOVE 'S151-PREPARA-TESTATA-JCL'  TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE GG-SYS                   TO REP-GIORNO
           MOVE MM-SYS                   TO REP-MESE
           MOVE AAAA-SYS                 TO REP-ANNO

           MOVE HH-SYS                   TO REP-ORA
           MOVE MI-SYS                   TO REP-MINUTI
           MOVE SS-SYS                   TO REP-SECONDI

           MOVE BBT-DES                  TO REP-DESC-BATCH
           MOVE IDSV0001-COD-COMPAGNIA-ANIA
                                         TO REP-COD-COMPAGNIA
           MOVE IABI0011-USER-NAME       TO REP-USER-NAME
           MOVE IABI0011-GRUPPO-AUTORIZZANTE TO REP-GRUPPO-AUT


           IF IABI0011-MACROFUNZIONALITA NOT =
              SPACES AND LOW-VALUE AND HIGH-VALUE
              MOVE IABI0011-MACROFUNZIONALITA
                                           TO REP-MACROFUNZIONALITA
           END-IF

           IF IABI0011-TIPO-MOVIMENTO IS NUMERIC AND
              IABI0011-TIPO-MOVIMENTO NOT = ZEROES
              MOVE IABI0011-TIPO-MOVIMENTO TO REP-TIPO-MOVIMENTO
           END-IF

           MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
           PERFORM Z700-DT-N-TO-X            THRU Z700-EX
           MOVE WS-DATE-X                    TO REP-DATA-EFFETTO

           MOVE IDSV0003-DATA-COMPETENZA     TO WS-TIMESTAMP-N
           PERFORM Z701-TS-N-TO-X            THRU Z701-EX
           MOVE WS-TIMESTAMP-X               TO REP-TIMESTAMP-COMPETENZA

           IF WK-SIMULAZIONE-INFR OR
              WK-SIMULAZIONE-APPL
              MOVE ESSE                      TO REP-SIMULAZIONE
           ELSE
              MOVE ENNE                      TO REP-SIMULAZIONE
           END-IF

           MOVE IABI0011-CANALE-VENDITA  TO REP-CANALE-VENDITA
           MOVE IABI0011-PUNTO-VENDITA   TO REP-PUNTO-VENDITA.
      *
       S151-EX.
           EXIT.
      *----------------------------------------------------------------*
      * SCRIVI TESTATA BATCH
      *----------------------------------------------------------------*
       S170-SCRIVI-TESTATA-BATCH.
      *
           MOVE 'S170-SCRIVI-TESTATA-BATCH' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM S171-PREPARA-TESTATA-BATCH  THRU S171-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE ALL '*'                  TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE LOGO-BATCH-REPORT        TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE ALL '*'                  TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE ID-BATCH-REPORT          TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE PROTOCOL-REPORT          TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           IF GESTIONE-PARALLELISMO-YES
              MOVE SPACES                TO REPORT01-REC
              MOVE PROGR-PROTOCOL-REPORT TO REPORT01-REC
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
           END-IF.
      *
       S170-EX.
           EXIT.
      *----------------------------------------------------------------*
      * PREPARA TESTATA BATCH
      *----------------------------------------------------------------*
       S171-PREPARA-TESTATA-BATCH.
      *
           MOVE 'S171-PREPARA-TESTATA-BATCH' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO   TO TRUE
           MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE BTC-ID-BATCH                   TO REP-ID-BATCH

           MOVE BTC-PROTOCOL                   TO REP-PROTOCOL

           IF GESTIONE-PARALLELISMO-YES
               MOVE IABI0011-PROG-PROTOCOL     TO REP-PROGR-PROTOCOL
           END-IF.
      *
       S171-EX.
           EXIT.
      *----------------------------------------------------------------*
      * SCRIVI DETTAGLIO ERRORE
      *----------------------------------------------------------------*
       S200-DETTAGLIO-ERRORE.
      *
           MOVE 'S200-DETTAGLIO-ERRORE'     TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE SPACES                   TO REP-DESC-ERRORE

           PERFORM S210-PREPARA-DETT-ERR THRU S210-EX

           IF STAMPA-LOGO-ERRORE-YES
              PERFORM S202-LOGO-DETTAGLIO-ERRORE THRU S202-EX
              SET STAMPA-LOGO-ERRORE-NO          TO TRUE

              MOVE SPACES                   TO REPORT01-REC
              MOVE CAMPI-LOG-ERRORE-REPORT-RIGA-1
                                            TO REPORT01-REC
              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX


              MOVE SPACES                   TO REPORT01-REC
              MOVE CAMPI-LOG-ERRORE-REPORT-RIGA-2
                                            TO REPORT01-REC
              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

              MOVE SPACES                   TO REPORT01-REC
              MOVE SL-CAMPI-LOG-ERRORE-REPORT
                                         TO REPORT01-REC
              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
           END-IF


           MOVE SPACES                   TO REPORT01-REC
           MOVE VALORI-CAMPI-LOG-ERRORE-REPORT
                                         TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           IF DESC-ERR-MORE-100-YES
              MOVE SPACES       TO VALORI-CAMPI-LOG-ERRORE-REPORT
              MOVE IDSV0001-DESC-ERRORE(IND-MAX-ELE-ERRORI)(101:100)
                                            TO REP-DESC-ERRORE
              MOVE SPACES                   TO REPORT01-REC
              MOVE VALORI-CAMPI-LOG-ERRORE-REPORT
                                            TO REPORT01-REC
              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
           END-IF.

           INITIALIZE                    VALORI-CAMPI-LOG-ERRORE-REPORT.

      *
       S200-EX.
           EXIT.

      *----------------------------------------------------------------*
      * SCRIVI DETTAGLIO ERRORE EXTRA
      *----------------------------------------------------------------*
       S205-DETTAGLIO-ERRORE-EXTRA.
      *
           MOVE 'S205-DETTAGLIO-ERRORE-EXTRA' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO    TO TRUE
           MOVE WK-LABEL                      TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE SPACES                         TO REP-DESC-ERRORE

           PERFORM S215-PREPARA-DETT-ERR-EXTRA THRU S215-EX

           IF STAMPA-LOGO-ERRORE-YES
              PERFORM S202-LOGO-DETTAGLIO-ERRORE THRU S202-EX
              SET STAMPA-LOGO-ERRORE-NO          TO TRUE

              MOVE SPACES                   TO REPORT01-REC
              MOVE CAMPI-LOG-ERRORE-REPORT-RIGA-1
                                            TO REPORT01-REC
              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX


              MOVE SPACES                   TO REPORT01-REC
              MOVE CAMPI-LOG-ERRORE-REPORT-RIGA-2
                                            TO REPORT01-REC
              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

              MOVE SPACES                   TO REPORT01-REC
              MOVE SL-CAMPI-LOG-ERRORE-REPORT
                                         TO REPORT01-REC
              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
           END-IF


           MOVE SPACES                   TO REPORT01-REC
           MOVE VALORI-CAMPI-LOG-ERRORE-REPORT
                                         TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           IF DESC-ERR-MORE-100-YES
              MOVE SPACES              TO VALORI-CAMPI-LOG-ERRORE-REPORT
              MOVE WK-LOR-DESC-ERRORE-ESTESA (101:100)
                                            TO REP-DESC-ERRORE
              MOVE SPACES                   TO REPORT01-REC
              MOVE VALORI-CAMPI-LOG-ERRORE-REPORT
                                            TO REPORT01-REC
              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
           END-IF.

           INITIALIZE                    VALORI-CAMPI-LOG-ERRORE-REPORT.

      *
       S205-EX.
           EXIT.

      *----------------------------------------------------------------*
      * PREPARA DETTAGLIO ERRORE
      *----------------------------------------------------------------*
       S210-PREPARA-DETT-ERR.
      *
           MOVE 'S210-PREPARA-DETT-ERR'     TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE IABV0006-OGG-BUSINESS       TO REP-OGG-BUSINESS
           MOVE IABV0006-TP-OGG-BUSINESS    TO REP-TP-OGG-BUSINESS
           MOVE IABV0006-IB-OGG-POLI        TO REP-IB-OGG-POLI
           MOVE IABV0006-IB-OGG-ADES        TO REP-IB-OGG-ADES
           MOVE IABV0006-ID-POLI            TO REP-ID-POLI
           MOVE IABV0006-ID-ADES            TO REP-ID-ADES
           MOVE IABV0006-DT-EFF-BUSINESS    TO REP-DT-EFF-BUSINESS

           INSPECT IDSV0001-DESC-ERRORE (IND-MAX-ELE-ERRORI)
                                            REPLACING ALL X'09' BY ' '
           INSPECT IDSV0001-DESC-ERRORE (IND-MAX-ELE-ERRORI)
                                            REPLACING ALL X'0A' BY ' '

           SET DESC-ERR-MORE-100-NO         TO TRUE
           IF IDSV0001-DESC-ERRORE(IND-MAX-ELE-ERRORI)(101:100) NOT =
              HIGH-VALUE AND LOW-VALUE AND SPACES
              SET DESC-ERR-MORE-100-YES     TO TRUE
           END-IF.

           MOVE IDSV0001-DESC-ERRORE(IND-MAX-ELE-ERRORI)(1:100)
                                            TO REP-DESC-ERRORE.

           MOVE WK-LOR-ID-GRAVITA-ERRORE    TO REP-LIV-GRAVITA

           MOVE WK-LOR-COD-MAIN-BATCH       TO REP-COD-MAIN-BATCH

           MOVE WK-LOR-COD-SERVIZIO-BE      TO REP-SERVIZIO-BE

           MOVE WK-LOR-LABEL-ERR            TO REP-LABEL-ERR

           MOVE WK-LOR-OPER-TABELLA         TO REP-OPER-TABELLA

           MOVE WK-LOR-NOME-TABELLA         TO REP-NOME-TABELLA

           MOVE WK-LOR-STATUS-TABELLA       TO REP-STATUS-TABELLA

           MOVE WS-TIMESTAMP-X              TO REP-TMST-ERRORE

           MOVE WK-LOR-KEY-TABELLA          TO REP-KEY-TABELLA.
      *
       S210-EX.
           EXIT.

      *----------------------------------------------------------------*
      * PREPARA DETTAGLIO ERRORE
      *----------------------------------------------------------------*
       S215-PREPARA-DETT-ERR-EXTRA.
      *
           MOVE 'S215-PREPARA-DETT-ERR-EXTRA' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO    TO TRUE
           MOVE WK-LABEL                      TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE IABV0006-OGG-BUSINESS       TO REP-OGG-BUSINESS
           MOVE IABV0006-TP-OGG-BUSINESS    TO REP-TP-OGG-BUSINESS
           MOVE IABV0006-IB-OGG-POLI        TO REP-IB-OGG-POLI
           MOVE IABV0006-IB-OGG-ADES        TO REP-IB-OGG-ADES
           MOVE IABV0006-ID-POLI            TO REP-ID-POLI
           MOVE IABV0006-ID-ADES            TO REP-ID-ADES
           MOVE IABV0006-DT-EFF-BUSINESS    TO REP-DT-EFF-BUSINESS

           INSPECT WK-LOR-DESC-ERRORE-ESTESA
                                            REPLACING ALL X'09' BY ' '
           INSPECT WK-LOR-DESC-ERRORE-ESTESA
                                            REPLACING ALL X'0A' BY ' '

           SET DESC-ERR-MORE-100-NO         TO TRUE
           IF WK-LOR-DESC-ERRORE-ESTESA (101:100) NOT =
              HIGH-VALUE AND LOW-VALUE AND SPACES
              SET DESC-ERR-MORE-100-YES     TO TRUE
           END-IF.

           MOVE WK-LOR-DESC-ERRORE-ESTESA(1:100)
                                            TO REP-DESC-ERRORE.

           MOVE WK-LOR-ID-GRAVITA-ERRORE    TO REP-LIV-GRAVITA

           MOVE WK-LOR-COD-MAIN-BATCH       TO REP-COD-MAIN-BATCH

           MOVE WK-LOR-COD-SERVIZIO-BE      TO REP-SERVIZIO-BE

           MOVE WK-LOR-LABEL-ERR            TO REP-LABEL-ERR

           MOVE WK-LOR-OPER-TABELLA         TO REP-OPER-TABELLA

           MOVE WK-LOR-NOME-TABELLA         TO REP-NOME-TABELLA

           MOVE WK-LOR-STATUS-TABELLA       TO REP-STATUS-TABELLA

           MOVE WS-TIMESTAMP-X              TO REP-TMST-ERRORE

           MOVE WK-LOR-KEY-TABELLA          TO REP-KEY-TABELLA.
      *
       S215-EX.
           EXIT.

      *----------------------------------------------------------------*
      * logo dettaglio errori
      *----------------------------------------------------------------*
       S202-LOGO-DETTAGLIO-ERRORE.
      *
           MOVE 'S202-LOGO-DETTAGLIO-ERRORE' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO   TO TRUE
           MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE SPACES                   TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE ALL '*'                  TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE DETT-LOG-ERRORE-REPORT   TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE ALL '*'                  TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX.

      *
       S202-EX.
           EXIT.

      *----------------------------------------------------------------*
      *
      *----------------------------------------------------------------*
       S555-SWITCH-GUIDE-TYPE.
      *
           MOVE 'S555-SWITCH-GUIDE-TYPE'    TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           EVALUATE BBT-SERVICE-GUIDE-NAME

              WHEN CONST-SEQUENTIAL-GUIDE
                   SET SEQUENTIAL-GUIDE       TO TRUE

              WHEN OTHER
                   SET DB-GUIDE               TO TRUE
                   MOVE BBT-SERVICE-GUIDE-NAME
                                         TO WK-PGM-GUIDA
           END-EVALUATE.
      *
       S555-EX.
           EXIT.

      *----------------------------------------------------------------*
      * SCRIVI REPORT01O BATCH
      *----------------------------------------------------------------*
       S900-REPORT01O-BATCH.
      *
           MOVE 'S900-REPORT01O-BATCH'      TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM S901-PREPARA-RIEP-BATCH THRU S901-EX

           MOVE SPACES                   TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE ALL '*'                  TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE '* '                     TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE LOGO-ESITI-BATCH-REPORT  TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE '* '                     TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE ALL '*'                  TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           IF IABV0006-COUNTER-STD-YES
              MOVE SPACES                TO REPORT01-REC
              MOVE SL-COUNT              TO REPORT01-REC
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX

              MOVE SPACES                TO REPORT01-REC
              MOVE LOGO-STANDARD-COUNT   TO REPORT01-REC
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX

              MOVE SPACES                TO REPORT01-REC
              MOVE SL-COUNT              TO REPORT01-REC
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX

              MOVE SPACES                TO REPORT01-REC
              MOVE BUS-SERV-ESEG-REPORT  TO REPORT01-REC
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX

              MOVE SPACES                TO REPORT01-REC
              MOVE BUS-SERV-OK-REPORT    TO REPORT01-REC
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX

              MOVE SPACES                TO REPORT01-REC
              MOVE BUS-SERV-OK-WARN-REPORT
                                         TO REPORT01-REC
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX

              MOVE SPACES                TO REPORT01-REC
              MOVE BUS-SERV-KO-REPORT    TO REPORT01-REC
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
           END-IF

           IF IABV0006-GUIDE-ROWS-READ-YES
              MOVE SPACES                TO REPORT01-REC
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX

              MOVE SPACES                TO REPORT01-REC
              MOVE SL-COUNT              TO REPORT01-REC
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX

              MOVE SPACES                TO REPORT01-REC
              MOVE LOGO-CUSTOM-COUNT     TO REPORT01-REC
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX

              MOVE SPACES                TO REPORT01-REC
              MOVE SL-COUNT              TO REPORT01-REC
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX

              MOVE SPACES                 TO REPORT01-REC
              MOVE TAB-GUIDE-LETTE-REPORT TO REPORT01-REC
              PERFORM WRITE-REPORT01      THRU WRITE-REPORT01-EX
           END-IF

           IF IABV0006-MAX-ELE-CUSTOM-COUNT > 0
              PERFORM S950-TRATTA-CUSTOM-COUNT THRU S950-EX
           END-IF

           MOVE SPACES                   TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE ALL '*'                  TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE ALL '*'                  TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           IF GESTIONE-PARALLELISMO-YES
              MOVE STATO-FINALE-PARALLELO-REPORT
                                         TO REPORT01-REC
           ELSE
              MOVE STATO-FINALE-BATCH-REPORT
                                         TO REPORT01-REC
           END-IF.
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE ALL '*'                  TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           MOVE ALL '*'                  TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX

           MOVE SPACES                   TO REPORT01-REC
           PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX.
      *
       S900-EX.
           EXIT.

      *----------------------------------------------------------------*
      * PREPARA REPORT01O BATCH
      *----------------------------------------------------------------*
       S901-PREPARA-RIEP-BATCH.
      *
           MOVE 'S901-PREPARA-RIEP-BATCH'   TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE CONT-TAB-GUIDE-LETTE      TO REP-TAB-GUIDE-LETTE

           MOVE CONT-BUS-SERV-ESEG        TO REP-BUS-SERV-ESEGUITI
           MOVE CONT-BUS-SERV-ESITO-OK    TO REP-BUS-SERV-ESITO-OK
           MOVE CONT-BUS-SERV-WARNING     TO REP-BUS-SERV-WARNING
           MOVE CONT-BUS-SERV-ESITO-KO    TO REP-BUS-SERV-ESITO-KO.

           MOVE ZEROES                    TO CONT-TAB-GUIDE-LETTE
                                             CONT-BUS-SERV-ESEG
                                             CONT-BUS-SERV-ESITO-OK
                                             CONT-BUS-SERV-WARNING
                                             CONT-BUS-SERV-ESITO-KO.

           IF GESTIONE-PARALLELISMO-YES
              MOVE IABV0002-STATE-CURRENT TO WK-COD-BATCH-STATE
           ELSE
              MOVE BTC-COD-BATCH-STATE    TO WK-COD-BATCH-STATE
           END-IF.
           PERFORM D902-REPERISCI-DES-BATCH-STATE
                                          THRU D902-EX
           IF GESTIONE-PARALLELISMO-YES
              MOVE WK-DES-BATCH-STATE     TO REP-STATO-FINALE-P
           ELSE
              MOVE WK-DES-BATCH-STATE     TO REP-STATO-FINALE
           END-IF.
      *
       S901-EX.
           EXIT.

      *----------------------------------------------------------------*
      * TRATTA CUSTOM COUNT
      *----------------------------------------------------------------*
       S950-TRATTA-CUSTOM-COUNT.
      *
           MOVE 'S950-TRATTA-CUSTOM-COUNT'  TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF IABV0006-GUIDE-ROWS-READ-NO
              MOVE SPACES                TO REPORT01-REC
              MOVE SL-COUNT              TO REPORT01-REC
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX

              MOVE SPACES                TO REPORT01-REC
              MOVE LOGO-CUSTOM-COUNT     TO REPORT01-REC
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX

              MOVE SPACES                TO REPORT01-REC
              MOVE SL-COUNT              TO REPORT01-REC
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
           END-IF

           PERFORM VARYING IABV0006-MAX-ELE-CUSTOM-COUNT FROM 1 BY 1
                     UNTIL IABV0006-MAX-ELE-CUSTOM-COUNT >
                           IABV0006-LIM-CUSTOM-COUNT-MAX OR
                           IABV0006-CUSTOM-COUNT-DESC
                           (IABV0006-MAX-ELE-CUSTOM-COUNT) =
                           SPACES OR HIGH-VALUES OR LOW-VALUES

              MOVE SPACES                TO REPORT01-REC
              IF IABV0006-ONLY-DESC
                            (IABV0006-MAX-ELE-CUSTOM-COUNT)
                 MOVE IABV0006-CUSTOM-COUNT-DESC
                            (IABV0006-MAX-ELE-CUSTOM-COUNT)
                                            TO REPORT01-REC

              ELSE
                 MOVE IABV0006-CUSTOM-COUNT-DESC
                            (IABV0006-MAX-ELE-CUSTOM-COUNT)
                          TO REP-CUSTOM-COUNT-DESC
                 MOVE IABV0006-CUSTOM-COUNT
                            (IABV0006-MAX-ELE-CUSTOM-COUNT)
                                            TO REP-CUSTOM-COUNT
                 MOVE CUSTOM-REPORT         TO REPORT01-REC
              END-IF
              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX

           END-PERFORM.

           PERFORM I000-INIT-CUSTOM-COUNT THRU I000-EX.
      *
       S950-EX.
           EXIT.

      *----------------------------------------------------------------*
      * OPEN FILE SEQUENZIALE
      *----------------------------------------------------------------*
       OPEN-REPORT01.

           MOVE 'OPEN-REPORT01'             TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           OPEN OUTPUT REPORT01.

           IF FS-REPORT01 NOT = '00'
                 INITIALIZE WK-LOG-ERRORE
                 MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                 MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
                 MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE

                 STRING 'OPEN ERRATA DEL FILE '
                         DELIMITED BY SIZE
                         'REPORT01'
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         'FS : '
                         DELIMITED BY SIZE
                         FS-REPORT01
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
           ELSE
              SET FILE-REPORT01-APERTO      TO TRUE
           END-IF.

      *
       OPEN-REPORT01-EX.
           EXIT.
      *----------------------------------------------------------------*
      * WRITE FILE SEQUENZIALE
      *----------------------------------------------------------------*
       WRITE-REPORT01.

           MOVE 'WRITE-REPORT01'            TO WK-LABEL

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           WRITE REPORT01-REC.

           IF FS-REPORT01 NOT = '00'
                 INITIALIZE WK-LOG-ERRORE
                 MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                 MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
                 MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE

                 STRING 'WRITE ERRATA DEL FILE '
                         DELIMITED BY SIZE
                         'REPORT01'
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         'FS : '
                         DELIMITED BY SIZE
                         FS-REPORT01
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
           END-IF.

       WRITE-REPORT01-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CONTROLLO FINALE
      *----------------------------------------------------------------*
       P900-CNTL-FINALE.
      *
           MOVE 'P900-CNTL-FINALE'          TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE IABI0011-CODICE-COMPAGNIA-ANIA TO BPA-COD-COMP-ANIA
           MOVE BTC-PROTOCOL                   TO BPA-PROTOCOL
           MOVE IABI0011-PROG-PROTOCOL         TO BPA-PROG-PROTOCOL

           PERFORM P950-PREPARA-CNTL-FINALE    THRU P950-EX.

           PERFORM P500-CALL-IABS0130          THRU P500-EX.

           PERFORM P951-ESITO-CNTL-FINALE      THRU P951-EX.
      *
       P900-EX.
           EXIT.
      *----------------------------------------------------------------*
      * PREPARA CONTROLLO FINALE
      *----------------------------------------------------------------*
       P950-PREPARA-CNTL-FINALE.
      *
           MOVE 'P950-PREPARA-CNTL-FINALE'  TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--> TIPO OPERAZIONE
           SET WS-FASE-CONTROLLO-FINALE    TO TRUE.

           MOVE WS-BUFFER-WHERE-COND-IABS0130
                                      TO IDSV0003-BUFFER-WHERE-COND.
      *
       P950-EX.
           EXIT.
      *----------------------------------------------------------------*
      * ACCEPT TIMESTAMP
      *----------------------------------------------------------------*
       T000-ACCEPT-TIMESTAMP.
      *
           ACCEPT WK-TIMESTAMP(1:8)         FROM DATE YYYYMMDD.
           ACCEPT WK-TIMESTAMP(9:6)         FROM TIME.

           MOVE   WK-TIMESTAMP(1:4)         TO AAAA-SYS.
           MOVE   WK-TIMESTAMP(5:2)         TO MM-SYS.
           MOVE   WK-TIMESTAMP(7:2)         TO GG-SYS.

           MOVE   WK-TIMESTAMP(09:2)        TO HH-SYS.
           MOVE   WK-TIMESTAMP(11:2)        TO MI-SYS.
           MOVE   WK-TIMESTAMP(13:2)        TO SS-SYS.
      *
       T000-EX.
           EXIT.
      *----------------------------------------------------------------*
      * valorizzazione versione
      *----------------------------------------------------------------*
       V000-VALORIZZAZIONE-VERSIONE.
      *
           MOVE 'V000-VALORIZZAZIONE-VERSIONE' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO     TO TRUE
           MOVE WK-LABEL                       TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

      *--    eventuale valorizzazione
      *--    del campo IABV0009-VERSIONING PER TABELLA GUIDA
      *--    con il campo WK-VERSIONING
      *--    modalit "RIPARTENZA CON VERSIONAMENTO"

           MOVE WK-VERSIONING       TO IABV0009-VERSIONING.
      *
       V000-EX.
           EXIT.
      *----------------------------------------------------------------*
      * VERIFICA ESEGUIBILITA' PRENOTAZIONE
      *----------------------------------------------------------------*
       V100-VERIFICA-PRENOTAZIONE.
      *
           MOVE 'V100-VERIFICA-PRENOTAZIONE'  TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO    TO TRUE
           MOVE WK-LABEL                      TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE BTC-ID-RICH                   TO RIC-ID-RICH
                                                 WK-ID-RICH
                                                 IABV0006-ID-RICH

           SET WK-VERIFICA-PRENOTAZIONE-KO    TO TRUE

      *--> TIPO OPERAZIONE
           SET IDSV0003-SELECT                TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC        TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL       TO TRUE.
      *
           CALL PGM-IABS0900 USING IDSV0003
                                   IABV0002
                                   RICH
                                   IABV0003
           ON EXCEPTION
              STRING 'ERRORE CHIAMATA MODULO : '
                   DELIMITED BY SIZE
                   PGM-IABS0900
                   DELIMITED BY SIZE
                   ' - '
                    DELIMITED BY SIZE INTO
                   WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
           END-CALL

           IF WK-ERRORE-NO
              IF IDSV0003-SUCCESSFUL-RC
                 EVALUATE TRUE
                 WHEN IDSV0003-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE

                    MOVE RIC-DS-STATO-ELAB TO WK-COD-ELAB-STATE
                    PERFORM D901-VERIFICA-ELAB-STATE THRU D901-EX

                    IF WK-RISULT-VERIF-ELAB-STATE-OK
                       SET WK-VERIFICA-PRENOTAZIONE-OK  TO TRUE
                    END-IF

                 WHEN IDSV0003-NEGATIVI
      *--->      ERRORE DI ACCESSO AL DB
                    INITIALIZE WK-LOG-ERRORE

                    MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    STRING 'ERRORE MODULO '
                           DELIMITED BY SIZE
                           PGM-IABS0900
                           DELIMITED BY SIZE
                           IDSV0003-DESCRIZ-ERR-DB2
                           DELIMITED BY SIZE INTO
                           WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                   MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                   PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX

                 END-EVALUATE
              ELSE
      *-->       GESTIRE ERRORE DISPATCHER
                 INITIALIZE WK-LOG-ERRORE

                 STRING 'ERRORE MODULO '
                         DELIMITED BY SIZE
                         PGM-IABS0900
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         IDSV0003-DESCRIZ-ERR-DB2
                         DELIMITED BY '   '
                         ' - '
                         DELIMITED BY SIZE
                         'RC : '
                         DELIMITED BY SIZE
                         IDSV0003-RETURN-CODE
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.
      *
       V100-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CARICA LOG_ERRORE IN WORKING
      *----------------------------------------------------------------*
       W000-CARICA-LOG-ERRORE.

           MOVE 'W000-CARICA-LOG-ERRORE'    TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           PERFORM VARYING IND-MAX-ELE-ERRORI FROM 1 BY 1
               UNTIL IND-MAX-ELE-ERRORI > IDSV0001-MAX-ELE-ERRORI
                  OR IND-MAX-ELE-ERRORI > 10
                  OR WK-ERRORE-YES

                  IF IDSV0001-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) =
                     ERRORE-FATALE
                     SET WK-SKIP-BATCH-YES            TO TRUE
                     SET WK-ERRORE-FATALE          TO TRUE
                  END-IF

                  IF IDSV0001-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) =
                     ERRORE-BLOCCANTE
                     SET WK-RC-08-TROVATO           TO TRUE
                  END-IF

                  IF IDSV0001-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) =
                     WARNING
                     SET WARNING-TROVATO-YES TO TRUE
                     IF  NOT WK-RC-08-TROVATO
                         IF  IDSV0001-FORZ-RC-04-YES
                             SET WK-RC-04-FORZATO        TO TRUE
                         END-IF
                         IF  NOT WK-RC-04-FORZATO
                             SET WK-RC-04-NON-FORZATO    TO TRUE
                         END-IF
                     END-IF
                  END-IF

                  IF IDSV0001-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) >=
                     IABI0011-LIVELLO-GRAVITA-LOG

                     INITIALIZE                WK-LOG-ERRORE

                     MOVE WK-SESSIONE-N09   TO WK-LOR-ID-LOG-ERRORE

                     MOVE IDSV0001-COD-MAIN-BATCH
                                            TO WK-LOR-COD-MAIN-BATCH

                     MOVE IDSV0001-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI)
                                            TO WK-LOR-ID-GRAVITA-ERRORE


                     MOVE IDSV0001-DESC-ERRORE
                                            (IND-MAX-ELE-ERRORI)
                                            TO WK-LOR-DESC-ERRORE-ESTESA

                     IF IABV0008-SCRIVI-LOG-ERRORE-SI
                        MOVE IDSV0001-SESSIONE       TO WK-SESSIONE
                        SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
                        SET  IDSV0003-SUCCESSFUL-SQL TO TRUE
                        PERFORM W100-INS-LOG-ERRORE  THRU W100-EX
                     END-IF

                     IF WK-ERRORE-NO
                        IF IND-MAX-ELE-ERRORI =
                           IDSV0001-MAX-ELE-ERRORI

                           IF IDSV0001-DESC-ERRORE-ESTESA =
                                WK-LOR-DESC-ERRORE-ESTESA

                                PERFORM W005-VALORIZZA-DA-ULTIMO
                                                      THRU W005-EX

                           END-IF

                        END-IF

                        IF WK-ERRORE-NO
                           PERFORM S200-DETTAGLIO-ERRORE THRU S200-EX
                        END-IF

                     END-IF

                  END-IF
           END-PERFORM.

           IF WK-ERRORE-NO
              PERFORM W001-CARICA-LOG-ERRORE-EXTRA THRU W001-EX
           END-IF.

           COPY IERP0001.
      *
       W000-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CARICA LOG_ERRORE IN WORKING
      *----------------------------------------------------------------*
       W001-CARICA-LOG-ERRORE-EXTRA.

           MOVE 'W001-CARICA-LOG-ERRORE-EXTRA' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO     TO TRUE
           MOVE WK-LABEL                       TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           PERFORM VARYING IND-MAX-ELE-ERRORI FROM 1 BY 1
               UNTIL IND-MAX-ELE-ERRORI > IEAV9904-MAX-ELE-ERRORI
                  OR IND-MAX-ELE-ERRORI > 10
                  OR WK-ERRORE-YES

                  IF IEAV9904-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) =
                     ERRORE-FATALE
                     SET WK-SKIP-BATCH-YES            TO TRUE
                     SET WK-ERRORE-FATALE          TO TRUE
                  END-IF

                  IF IEAV9904-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) =
                     ERRORE-BLOCCANTE
                     SET WK-RC-08-TROVATO           TO TRUE
                  END-IF

                  IF IEAV9904-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) =
                     WARNING
                     SET WARNING-TROVATO-YES TO TRUE
                     IF  NOT WK-RC-08-TROVATO
                         IF  IDSV0001-FORZ-RC-04-YES
                             SET WK-RC-04-FORZATO        TO TRUE
                         END-IF
                         IF  NOT WK-RC-04-FORZATO
                             SET WK-RC-04-NON-FORZATO    TO TRUE
                         END-IF
                     END-IF
                  END-IF

                  IF IEAV9904-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) >=
                     IABI0011-LIVELLO-GRAVITA-LOG

                     INITIALIZE                WK-LOG-ERRORE

                     MOVE WK-SESSIONE-N09   TO WK-LOR-ID-LOG-ERRORE

                     MOVE IDSV0001-COD-MAIN-BATCH
                                            TO WK-LOR-COD-MAIN-BATCH

                     MOVE IEAV9904-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI)
                                            TO WK-LOR-ID-GRAVITA-ERRORE


                     MOVE IEAV9904-DESC-ERRORE
                                            (IND-MAX-ELE-ERRORI)
                                            TO WK-LOR-DESC-ERRORE-ESTESA


                     IF IABV0008-SCRIVI-LOG-ERRORE-SI
                        MOVE IDSV0001-SESSIONE       TO WK-SESSIONE
                        SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
                        SET  IDSV0003-SUCCESSFUL-SQL TO TRUE
                        PERFORM W100-INS-LOG-ERRORE  THRU W100-EX
                     END-IF

                     IF WK-ERRORE-NO
                        IF IND-MAX-ELE-ERRORI = IEAV9904-MAX-ELE-ERRORI
                           IF IDSV0001-DESC-ERRORE-ESTESA =
                                WK-LOR-DESC-ERRORE-ESTESA

                              PERFORM W005-VALORIZZA-DA-ULTIMO
                                                     THRU W005-EX

                           END-IF

                        END-IF

                        IF WK-ERRORE-NO
                           PERFORM S205-DETTAGLIO-ERRORE-EXTRA
                                                     THRU S205-EX
                        END-IF
                     END-IF

                  END-IF
           END-PERFORM.

           IF IEAV9904-MAX-ELE-ERRORI > 0
              IF IDSV0001-DESC-ERRORE-ESTESA NOT =
                   WK-LOR-DESC-ERRORE-ESTESA

                 MOVE IDSV0001-DESC-ERRORE-ESTESA
                             TO WK-LOR-DESC-ERRORE-ESTESA

                 IF WK-ERRORE-NO
                    PERFORM S205-DETTAGLIO-ERRORE-EXTRA
                                                     THRU S205-EX
                 END-IF
              END-IF
           END-IF.

           COPY IERP0004.
      *
       W001-EX.
           EXIT.

      *----------------------------------------------------------------*
       W005-VALORIZZA-DA-ULTIMO.

           MOVE IDSV0001-COD-SERVIZIO-BE TO WK-LOR-COD-SERVIZIO-BE
           MOVE IDSV0001-LABEL-ERR       TO WK-LOR-LABEL-ERR
           MOVE IDSV0001-OPER-TABELLA    TO WK-LOR-OPER-TABELLA
           MOVE IDSV0001-NOME-TABELLA    TO WK-LOR-NOME-TABELLA
           MOVE IDSV0001-STATUS-TABELLA  TO WK-LOR-STATUS-TABELLA
           MOVE IDSV0001-KEY-TABELLA     TO WK-LOR-KEY-TABELLA.

       W005-EX.
           EXIT.
      *----------------------------------------------------------------*
      * CARICA LOG_ERRORE IN WORKING DA BATCH EXECUTOR
      *----------------------------------------------------------------*
       W070-CARICA-LOG-ERR-BAT-EXEC.

           MOVE 'W070-CARICA-LOG-ERR-BAT-EXEC' TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO     TO TRUE
           MOVE WK-LABEL                       TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           MOVE HIGH-VALUES                 TO LOG-ERRORE

           MOVE IDSV0001-SESSIONE           TO WK-SESSIONE

           MOVE 1                           TO IND-MAX-ELE-ERRORI

           MOVE WK-SESSIONE-N09             TO WK-LOR-ID-LOG-ERRORE
           MOVE IDSV0001-COD-MAIN-BATCH     TO WK-LOR-COD-MAIN-BATCH
           MOVE WK-LOR-DESC-ERRORE-ESTESA   TO IDSV0001-DESC-ERRORE
                                               (IND-MAX-ELE-ERRORI)
           MOVE WK-PGM                      TO WK-LOR-COD-SERVIZIO-BE
           MOVE WK-LABEL                    TO WK-LOR-LABEL-ERR

           IF IABV0008-SCRIVI-LOG-ERRORE-SI
              PERFORM W100-INS-LOG-ERRORE   THRU W100-EX
           END-IF

           IF STAMPA-TESTATA-JCL-YES
              PERFORM S100-TESTATA-JCL      THRU S100-EX
              SET STAMPA-TESTATA-JCL-NO     TO TRUE
           END-IF

           IF WK-ERRORE-NO
              PERFORM S200-DETTAGLIO-ERRORE THRU S200-EX
           END-IF.

           IF WK-ERRORE-BLOCCANTE
              SET WK-ERRORE-YES                TO TRUE
              SET WK-RC-08                     TO TRUE
           END-IF

ALEX       IF WK-ERRORE-FATALE
              SET WK-ERRORE-YES                TO TRUE
              SET WK-RC-12                     TO TRUE
           END-IF

           SET WK-ERRORE-BLOCCANTE          TO TRUE.

           MOVE 0                           TO IND-MAX-ELE-ERRORI.
      *
       W070-EX.
           EXIT.
      *----------------------------------------------------------------*
      * INSERIMENTO LOG ERRORE
      *----------------------------------------------------------------*
       W100-INS-LOG-ERRORE.
      *
           MOVE 'W100-INS-LOG-ERRORE'       TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           MOVE HIGH-VALUES               TO LOG-ERRORE.

           MOVE WK-LOR-ID-LOG-ERRORE      TO LOR-ID-LOG-ERRORE
           MOVE WK-LOR-ID-GRAVITA-ERRORE  TO LOR-ID-GRAVITA-ERRORE
           MOVE WK-LOR-DESC-ERRORE-ESTESA TO LOR-DESC-ERRORE-ESTESA
           MOVE WK-LOR-COD-MAIN-BATCH     TO LOR-COD-MAIN-BATCH
           MOVE WK-LOR-COD-SERVIZIO-BE    TO LOR-COD-SERVIZIO-BE
           MOVE WK-LOR-LABEL-ERR          TO LOR-LABEL-ERR
           MOVE WK-LOR-OPER-TABELLA       TO LOR-OPER-TABELLA
           MOVE WK-LOR-NOME-TABELLA       TO LOR-NOME-TABELLA
           MOVE WK-LOR-STATUS-TABELLA     TO LOR-STATUS-TABELLA
           MOVE WK-LOR-KEY-TABELLA        TO LOR-KEY-TABELLA

      *--> TIPO OPERAZIONE
           SET IDSV0003-INSERT            TO TRUE.
           SET IDSV0003-PRIMARY-KEY       TO TRUE.
           SET IDSV0003-TRATT-SENZA-STOR  TO TRUE.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
           SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.

           CALL PGM-IABS0120              USING IDSV0003
                                                LOG-ERRORE
           ON EXCEPTION
              STRING 'ERRORE CHIAMATA MODULO : '
                      DELIMITED BY SIZE
                      PGM-IABS0120
                      DELIMITED BY SIZE
                      ' - '
                       DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
           END-CALL

           IF WK-ERRORE-NO
                 IF IDSV0003-SUCCESSFUL-RC
                    EVALUATE TRUE
                        WHEN IDSV0003-SUCCESSFUL-SQL
      *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                             CONTINUE

                        WHEN IDSV0003-NEGATIVI
      *--->   ERRORE DI ACCESSO AL DB
                       INITIALIZE WK-LOG-ERRORE

                       MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                       STRING 'ERRORE MODULO '
                              DELIMITED BY SIZE
                              PGM-IABS0120
                              DELIMITED BY SIZE
                              ' SQLCODE : '
                              WK-SQLCODE
                              DELIMITED BY SIZE INTO
                              WK-LOR-DESC-ERRORE-ESTESA
                       END-STRING

                       PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX

                    END-EVALUATE
                 ELSE
      *-->          GESTIRE ERRORE DISPATCHER
                    INITIALIZE WK-LOG-ERRORE

                    STRING 'ERRORE MODULO '
                            DELIMITED BY SIZE
                            PGM-IABS0120
                            DELIMITED BY SIZE
                            ' - '
                            DELIMITED BY SIZE
                            IDSV0003-DESCRIZ-ERR-DB2
                            DELIMITED BY '   '
                            ' - '
                            DELIMITED BY SIZE
                            'RC : '
                            DELIMITED BY SIZE
                            IDSV0003-RETURN-CODE
                            DELIMITED BY SIZE INTO
                            WK-LOR-DESC-ERRORE-ESTESA
                    END-STRING

                    PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX

                 END-IF
           END-IF.
      *
       W100-EX.
           EXIT.
      *****************************************************************
       Y000-COMMIT.
      *
           MOVE 'Y000-COMMIT'               TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           EXEC SQL
                COMMIT WORK
           END-EXEC.

           MOVE SQLCODE             TO IDSV0003-SQLCODE

           IF IDSV0003-SUCCESSFUL-SQL
              SET INIT-COMMIT-FREQUENCY TO TRUE
              PERFORM Y350-SAVEPOINT-TOT THRU Y350-EX
           ELSE
              INITIALIZE WK-LOG-ERRORE
              MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
              MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
              MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE

              MOVE SQLCODE          TO WK-SQLCODE
              STRING 'ERRORE COMMIT WORK'
                      ' - '
                      'SQLCODE : '
                      WK-SQLCODE
                      DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX

           END-IF.
      *
       Y000-EX.
           EXIT.
      *****************************************************************
       Y050-CONTROLLO-COMMIT.

           IF IDSV0001-ARCH-BATCH-DBG AND
              CONT-TAB-GUIDE-LETTE > ZERO
              SET      IDSV8888-ARCH-BATCH-DBG TO TRUE
              MOVE 'Architettura Batch'        TO IDSV8888-DESC-PGM
              STRING WK-PGM '/'
                     'Rec. Commit:'
                      CONT-TAB-GUIDE-LETTE
                      DELIMITED BY SIZE
                      INTO IDSV8888-NOME-PGM
              END-STRING
              PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
              IF  IABV0006-STD-TYPE-REC-NO       OR
                  IABV0006-STD-TYPE-REC-YES
                  SET      IDSV8888-ARCH-BATCH-DBG TO TRUE
                  MOVE 'Architettura Batch'        TO IDSV8888-DESC-PGM
                  STRING WK-PGM '/'
                         'Ult. Id Job:'
                          WK-ID-JOB-ULT-COMMIT
                          DELIMITED BY SIZE
                          INTO IDSV8888-NOME-PGM
                  END-STRING
                  PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
              END-IF
           END-IF.
      *
       Y050-EX.
           EXIT.
      *****************************************************************
       Y100-ROLLBACK-SAVEPOINT.
      *
           MOVE 'Y100-ROLLBACK-SAVEPOINT'   TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

ALEX       IF WK-GESTIONE-SAVEPOINT-SI
              EXEC SQL
                   ROLLBACK WORK TO SAVEPOINT A
              END-EXEC

           MOVE SQLCODE       TO IDSV0003-SQLCODE

              EVALUATE TRUE
                 WHEN IDSV0003-SUCCESSFUL-SQL
                 WHEN IDSV0003-OPER-NF-X-UNDONE
              CONTINUE
                 WHEN OTHER
              INITIALIZE WK-LOG-ERRORE
              MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
              MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
              MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE

              MOVE SQLCODE          TO WK-SQLCODE
                      STRING 'ERRORE ROLLBACK WORK TO SAVEPOINT A'
                      ' - '
                      'SQLCODE : '
                      WK-SQLCODE
                      DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX

              SET WK-ERRORE-NO                  TO TRUE
              PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX
              SET WK-ERRORE-YES                 TO TRUE

              END-EVALUATE
           ELSE
              PERFORM Y600-ROLLBACK-NO-SAVEPOINT
                 THRU Y600-EX
           END-IF.
      *
       Y100-EX.
           EXIT.
      *****************************************************************
       Y200-ROLLBACK.
      *
           MOVE 'Y200-ROLLBACK'             TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

ALEX       IF WK-GESTIONE-SAVEPOINT-SI
              EXEC SQL
                   ROLLBACK WORK TO SAVEPOINT TOT
              END-EXEC

           MOVE SQLCODE       TO IDSV0003-SQLCODE

              EVALUATE TRUE
                 WHEN IDSV0003-SUCCESSFUL-SQL
                 WHEN IDSV0003-OPER-NF-X-UNDONE
              CONTINUE
                 WHEN OTHER
              INITIALIZE WK-LOG-ERRORE
              MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
              MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
              MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE

              MOVE SQLCODE          TO WK-SQLCODE
                      STRING 'ERRORE ROLLBACK TO SAVEPOINT TOT'
                      ' - '
                      'SQLCODE : '
                      WK-SQLCODE
                      DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX

              SET WK-ERRORE-NO                  TO TRUE
              PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX
              SET WK-ERRORE-YES                 TO TRUE

              END-EVALUATE
           ELSE
              PERFORM Y600-ROLLBACK-NO-SAVEPOINT
                 THRU Y600-EX
           END-IF.
      *
       Y200-EX.
           EXIT.
      *****************************************************************
       Y300-SAVEPOINT.
      *
           MOVE 'Y300-SAVEPOINT'            TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

ALEX       IF WK-GESTIONE-SAVEPOINT-SI
              EXEC SQL
                   SAVEPOINT A ON ROLLBACK RETAIN CURSORS
              END-EXEC

           MOVE SQLCODE       TO IDSV0003-SQLCODE

           IF IDSV0003-SUCCESSFUL-SQL
              IF (IABV0006-STD-TYPE-REC-NO   OR
                  IABV0006-STD-TYPE-REC-YES) AND
                  IDSV0001-ARCH-BATCH-DBG
                  MOVE WK-ID-JOB      TO WK-ID-JOB-ULT-COMMIT
              END-IF
           ELSE
              INITIALIZE WK-LOG-ERRORE
              MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
              MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
              MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE

              MOVE SQLCODE          TO WK-SQLCODE
              STRING 'ERRORE SAVEPOINT'
                      ' - '
                      'SQLCODE : '
                      WK-SQLCODE
                      DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX

              END-IF
           END-IF.
      *
       Y300-EX.
           EXIT.
      *****************************************************************
       Y350-SAVEPOINT-TOT.
      *
           MOVE 'Y350-SAVEPOINT-TOT'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

ALEX       IF WK-GESTIONE-SAVEPOINT-SI
              EXEC SQL
                  SAVEPOINT TOT ON ROLLBACK RETAIN CURSORS
              END-EXEC

           MOVE SQLCODE       TO IDSV0003-SQLCODE

           IF IDSV0003-SUCCESSFUL-SQL
              CONTINUE
           ELSE
              INITIALIZE WK-LOG-ERRORE
              MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
              MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
              MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE

              MOVE SQLCODE          TO WK-SQLCODE
              STRING 'ERRORE SAVEPOINT TOT'
                      ' - '
                      'SQLCODE : '
                      WK-SQLCODE
                      DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX

              END-IF
           END-IF.
      *
       Y350-EX.
           EXIT.
      *****************************************************************
       Y400-ESITO-OK-OTHER-SERV.

           MOVE 'Y400-ESITO-OK-OTHER-SERV'  TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           IF WK-SIMULAZIONE-INFR
              PERFORM Y200-ROLLBACK            THRU Y200-EX
           ELSE
              PERFORM Y000-COMMIT              THRU Y000-EX
           END-IF.
      *
       Y400-EX.
           EXIT.
      *****************************************************************
       Y500-ESITO-KO-OTHER-SERV.

           MOVE 'Y500-ESITO-KO-OTHER-SERV'  TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           SET WK-ERRORE-NO                    TO TRUE
           PERFORM Y200-ROLLBACK               THRU Y200-EX

           IF WK-ERRORE-NO
              PERFORM W000-CARICA-LOG-ERRORE   THRU W000-EX

              IF WK-ERRORE-NO
                 PERFORM Y000-COMMIT           THRU Y000-EX
              END-IF
           END-IF.
      *
       Y500-EX.
           EXIT.
      *----------------------------------------------------------------*
      * OPERAZIONI FINALI
      *----------------------------------------------------------------*
       Z000-OPERAZ-FINALI.

           MOVE 'Z000-OPERAZ-FINALI'        TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
      *
           PERFORM Z001-OPERAZIONI-FINALI THRU Z001-EX

           IF IJCCMQ04-MQ AND
              WSMQ-HCONN NOT = ZERO
              PERFORM A107-CHIUDI-CONNESSIONE THRU A107-EX
           END-IF.

           PERFORM Z100-CHIUDI-FILE       THRU Z100-EX.

           PERFORM Z200-STATISTICHE       THRU Z200-STATISTICHE-FINE.

           SET  IDSV8888-ARCH-BATCH-DBG    TO TRUE.
           MOVE WK-PGM                     TO IDSV8888-NOME-PGM.
           MOVE 'Architettura Batch'       TO IDSV8888-DESC-PGM.
           SET  IDSV8888-FINE              TO TRUE.
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           PERFORM Z999-SETTAGGIO-RC      THRU Z999-EX.
      *
       Z000-OPERAZ-FINALI-FINE.
           EXIT.

      *----------------------------------------------------------------*
      * CHIUDI FILE PARAMETRI
      *----------------------------------------------------------------*
       Z100-CHIUDI-FILE.
      *
           MOVE 'Z100-CHIUDI-FILE'          TO WK-LABEL.

           SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
           MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
           PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.

           IF FILE-PARAM-APERTO
              CLOSE PBTCEXEC

              IF FS-PARA NOT = '00'
                 INITIALIZE WK-LOG-ERRORE

                 STRING 'CLOSE ERRATA DEL FILE '
                         DELIMITED BY SIZE
                        'PBTCEXEC'
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         'RC : '
                         DELIMITED BY SIZE
                         FS-PARA
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.

           IF FILE-REPORT01-APERTO
              CLOSE REPORT01

              IF FS-REPORT01 NOT = '00'
                 INITIALIZE WK-LOG-ERRORE

                 STRING 'CLOSE ERRATA DEL FILE '
                         DELIMITED BY SIZE
                        'REPORT01'
                         DELIMITED BY SIZE
                         ' - '
                         DELIMITED BY SIZE
                         'RC : '
                         DELIMITED BY SIZE
                         FS-REPORT01
                         DELIMITED BY SIZE INTO
                         WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
              END-IF
           END-IF.

      *
       Z100-EX.
           EXIT.

      *----------------------------------------------------------------*
      * STATISTICHE FINALI                                             *
      *----------------------------------------------------------------*
       Z200-STATISTICHE.
      *
           PERFORM T000-ACCEPT-TIMESTAMP     THRU T000-EX.

           PERFORM A102-DISPLAY-FINE-ELABORA THRU A102-EX.
      *
       Z200-STATISTICHE-FINE.
           EXIT.
      *----------------------------------------------------------------*
      * DISPLAY SKEDA PARAMETRI
      *----------------------------------------------------------------*
       Z300-SKEDA-PAR-ERRATA.
      *
           SET WK-RC-12                      TO TRUE
           SET WK-ERRORE-YES                 TO TRUE.

           DISPLAY '***********************************************'.
           DISPLAY '* NOME PROGRAMMA    = ' WK-PGM.
           DISPLAY '* DESCRIZIONE       = Fine anomala di '
           DISPLAY '* BATCH EXECUTOR '.
           DISPLAY '* SKEDA PARAMETRI ERRATA '.
           DISPLAY '* SU RIGA : ' CNT-NUM-OPERAZ-LETTE
           DISPLAY '* DATA ELABORAZIONE = ' WK-DATA-SISTEMA.
           DISPLAY '* ORA  ELABORAZIONE = ' WK-ORA-SISTEMA.
           DISPLAY '************************************************'.
           DISPLAY ALL SPACES.
      *
       Z300-EX.
           EXIT.
      *----------------------------------------------------------------*
      * DISPLAY ERRORE PRE CONNESIONE DB
      *----------------------------------------------------------------*
       Z400-ERRORE-PRE-DB.
      *
           SET WK-RC-12                      TO TRUE
           SET WK-ERRORE-YES                 TO TRUE.

           DISPLAY '***********************************************'.
           DISPLAY '* NOME PROGRAMMA    = ' WK-PGM.
           DISPLAY '* DESCRIZIONE       = Fine anomala di '
           DISPLAY '* BATCH EXECUTOR '.
           DISPLAY WK-LOR-DESC-ERRORE-ESTESA.
           DISPLAY '* DATA ELABORAZIONE = ' WK-DATA-SISTEMA.
           DISPLAY '* ORA  ELABORAZIONE = ' WK-ORA-SISTEMA.
           DISPLAY '************************************************'.
           DISPLAY ALL SPACES.
      *
       Z400-EX.
           EXIT.

      *----------------------------------------------------------------*
      * GESTIONE DELL'ERRORE CHE SI VERIFICA NEL PGM MAIN
      *----------------------------------------------------------------*
ALEX   Z500-GESTIONE-ERR-MAIN.

           SET WK-ERRORE-NO TO TRUE.
           SET WK-BAT-COLLECTION-KEY-KO  TO TRUE.

           IF GESTIONE-PARALLELISMO-YES
              PERFORM P100-PARALLELISMO-FINALE THRU P100-EX

              IF WK-ERRORE-NO
                 PERFORM P900-CNTL-FINALE      THRU P900-EX
              END-IF
           END-IF

           IF WK-ERRORE-NO
              IF BATCH-COMPLETE
                 PERFORM I200-INIT-DA-ESEGUIRE
                    THRU I200-EX

                 PERFORM A150-SET-CURRENT-TIMESTAMP THRU A150-EX
                 MOVE WS-TIMESTAMP-N        TO   BTC-DT-END

                 IF WK-SIMULAZIONE-INFR
                    PERFORM Y200-ROLLBACK   THRU Y200-EX
                 END-IF

                 PERFORM D400-UPD-BATCH     THRU D400-EX

                 IF WK-ERRORE-NO
                    IF BTC-ID-RICH-NULL NOT = HIGH-VALUE AND
                       BTC-ID-RICH      NOT = ZEROES
                       SET IDSV0003-WHERE-CONDITION-02 TO TRUE
                       PERFORM D951-TRASF-BATCH-TO-PRENOTAZ
                                                      THRU D951-EX
                       IF WK-AGGIORNA-PRENOTAZ-YES
                          SET IDSV0003-NONE-ACTION       TO TRUE
                          PERFORM D950-AGGIORNA-PRENOTAZIONE
                                                      THRU D950-EX
                          IF WK-ERRORE-NO
                             PERFORM D952-TRASF-PRENOTAZ-TO-BATCH
                                                      THRU D952-EX
                          END-IF
                       END-IF
                    END-IF
                 END-IF

              END-IF

              IF WK-ERRORE-NO
                 PERFORM Y000-COMMIT        THRU Y000-EX
              END-IF

           END-IF.

       Z500-EX.
           EXIT.
      *----------------------------------------------------------------*
      * SETTAGGIO RETURN CODE
      *----------------------------------------------------------------*

       Z999-SETTAGGIO-RC.

           MOVE FLAG-RETURN-CODE          TO RETURN-CODE.

       Z999-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONVERSIONE DATE AND TIMESTAMP
      *----------------------------------------------------------------*
           COPY IDSP0014.

      *----------------------------------------------------------------*
      *    ROUTINES DI :
      *        -     CONTROLLO DATA FORMALE
      *----------------------------------------------------------------*
           COPY IDSP0016.

      *****************************************************************
      * N.B. - UTILIZZABILE SOLO INCLUDENDO LE COPY :
      *        IDSV0014
      *        IDSP0014
      *****************************************************************

      *----------------------------------------------------------------*
      * SETTA TIMESTAMP
      *----------------------------------------------------------------*
       ESTRAI-CURRENT-TIMESTAMP.
      *
           EXEC SQL
                 VALUES CURRENT TIMESTAMP
                   INTO :WS-TIMESTAMP-X
           END-EXEC

           IF SQLCODE = 0
              PERFORM Z801-TS-X-TO-N   THRU Z801-EX
           END-IF.
      *
       ESTRAI-CURRENT-TIMESTAMP-EX.
           EXIT.
      *----------------------------------------------------------------*
      * SETTA TIMESTAMP
      *----------------------------------------------------------------*
       ESTRAI-CURRENT-DATE.
      *
           EXEC SQL
                 VALUES CURRENT DATE
                   INTO :WS-DATE-X
           END-EXEC

           IF SQLCODE = 0
              PERFORM Z800-DT-X-TO-N   THRU Z800-EX
           END-IF.
      *
       ESTRAI-CURRENT-DATE-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CALL IABS0030
      *----------------------------------------------------------------*
       CALL-IABS0030.

             CALL PGM-IABS0030 USING IDSV0003
                                     IABV0002
                                     BTC-ELAB-STATE
             ON EXCEPTION
                STRING 'ERRORE CHIAMATA MODULO : '
                      DELIMITED BY SIZE
                      PGM-IABS0030
                      DELIMITED BY SIZE
                      ' - '
                       DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
                END-STRING

                PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
           END-CALL.

       CALL-IABS0030-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CALL IABS0040
      *----------------------------------------------------------------*
       CALL-IABS0040.

           CALL PGM-IABS0040   USING IDSV0003
                                   IABV0002
                                   BTC-BATCH
           ON EXCEPTION
              STRING 'ERRORE CHIAMATA MODULO : '
                      DELIMITED BY SIZE
                      PGM-IABS0040
                      DELIMITED BY SIZE
                      ' - '
                       DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
           END-CALL.

       CALL-IABS0040-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CALL IABS0050
      *----------------------------------------------------------------*
       CALL-IABS0050.

           CALL PGM-IABS0050 USING IDSV0003
                                   BTC-BATCH-TYPE
           ON EXCEPTION
              STRING 'ERRORE CHIAMATA MODULO : '
                      DELIMITED BY SIZE
                      PGM-IABS0050
                      DELIMITED BY SIZE
                      ' - '
                       DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
              END-STRING

              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
           END-CALL.

       CALL-IABS0050-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CALL IABS0060
      *----------------------------------------------------------------*
       CALL-IABS0060.

              CALL PGM-IABS0060              USING IDSV0003
                                                   IABV0002
                                                   BTC-JOB-SCHEDULE
              ON EXCEPTION
                 STRING 'ERRORE CHIAMATA MODULO : '
                      DELIMITED BY SIZE
                      PGM-IABS0060
                      DELIMITED BY SIZE
                      ' - '
                       DELIMITED BY SIZE INTO
                      WK-LOR-DESC-ERRORE-ESTESA
                 END-STRING

                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
              END-CALL.

       CALL-IABS0060-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CALL IABS0070
      *----------------------------------------------------------------*
       CALL-IABS0070.

            CALL PGM-IABS0070 USING      IDSV0003
                                         IABV0002
                                         BTC-BATCH-STATE
            ON EXCEPTION
               STRING 'ERRORE CHIAMATA MODULO : '
                       DELIMITED BY SIZE
                       PGM-IABS0070
                       DELIMITED BY SIZE
                       ' - '
                        DELIMITED BY SIZE INTO
                       WK-LOR-DESC-ERRORE-ESTESA
               END-STRING

               PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            END-CALL.

       CALL-IABS0070-EX.
           EXIT.

      *----------------------------------------------------------------*
      * CALL IDSS0300
      *----------------------------------------------------------------*
       CALL-IDSS0300.

            CALL PGM-IDSS0300 USING      IDSV0301
            ON EXCEPTION
               STRING 'ERRORE CHIAMATA MODULO : '
                       DELIMITED BY SIZE
                       PGM-IDSS0300
                       DELIMITED BY SIZE
                       ' - '
                        DELIMITED BY SIZE INTO
                       WK-LOR-DESC-ERRORE-ESTESA
               END-STRING

               PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            END-CALL.

       CALL-IDSS0300-EX.
           EXIT.

       Y600-ROLLBACK-NO-SAVEPOINT.

           MOVE 'Y100-ROLLBACK-SAVEPOINT'   TO WK-LABEL.

           PERFORM VARYING IND-MAX-ELE-ERRORI FROM 1 BY 1
             UNTIL IND-MAX-ELE-ERRORI > 10
                OR IND-MAX-ELE-ERRORI > IDSV0001-MAX-ELE-ERRORI

             IF IDSV0001-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) =
                ERRORE-FATALE
                EXEC SQL
                     ROLLBACK
                END-EXEC

                MOVE SQLCODE       TO IDSV0003-SQLCODE

                EVALUATE TRUE
                   WHEN IDSV0003-SUCCESSFUL-SQL
                   WHEN IDSV0003-OPER-NF-X-UNDONE
                        INITIALIZE WK-LOG-ERRORE
                        MOVE ERRORE-FATALE   TO WK-LOR-ID-GRAVITA-ERRORE
                        MOVE WK-LABEL        TO WK-LOR-LABEL-ERR
                        MOVE WK-PGM          TO WK-LOR-COD-SERVIZIO-BE

                        MOVE 'ROLLBACK EFFETTUATA'
                          TO WK-LOR-DESC-ERRORE-ESTESA

                        PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX

                        SET WK-ERRORE-NO                  TO TRUE
                        PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX
                        SET WK-ERRORE-YES                 TO TRUE

                   WHEN OTHER
                        INITIALIZE WK-LOG-ERRORE
                        MOVE ERRORE-FATALE   TO WK-LOR-ID-GRAVITA-ERRORE
                        MOVE WK-LABEL        TO WK-LOR-LABEL-ERR
                        MOVE WK-PGM          TO WK-LOR-COD-SERVIZIO-BE

                        MOVE SQLCODE          TO WK-SQLCODE
                        STRING 'ERRORE ROLLBACK '
                                ' - '
                                'SQLCODE : '
                                WK-SQLCODE
                                DELIMITED BY SIZE INTO
                                WK-LOR-DESC-ERRORE-ESTESA
                        END-STRING

                        PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX

                        SET WK-ERRORE-NO                  TO TRUE
                        PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX
                        SET WK-ERRORE-YES                 TO TRUE

                END-EVALUATE
                MOVE 11 TO IND-MAX-ELE-ERRORI
              END-IF
           END-PERFORM.

       Y600-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ROUTINES DI GESTIONE DISPLAY
      *----------------------------------------------------------------*
           COPY IDSP8888.





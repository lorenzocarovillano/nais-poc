      ******************************************************************
      * STRUTTURA PER GESTIONE CACHE
      ******************************************************************
       01 IX-CACHE                          PIC 9(5) COMP.

       01 TIPO-ADDRESS                      PIC X(01).
          88 ADDRESS-CACHE-VAL-VAR          VALUE 'A'.
          88 ADDRESS-CACHE-ACTUATOR         VALUE 'B' 'E'.
          88 ADDRESS-CACHE-ANAG             VALUE 'C'.
13382     88 ADDRESS-ANA-STR-DATO           VALUE 'D'.
          88 ADDRESS-VAR-AMBIENTE           VALUE 'P'.
          88 ADDRESS-AREA-MQ                VALUE 'Q'.
          88 ADDRESS-CACHE-VALIDO           VALUE ' '
                                                  'A'
                                                  'B'
                                                  'E'
                                                  'C'
13382                                             'D'
                                                  'P'
                                                  'Q'.

       01 ADDRESS-TROVATO                   PIC X(01).
          88 ADDRESS-CACHE-VAL-VAR-SI       VALUE 'S'.
          88 ADDRESS-CACHE-VAL-VAR-NO       VALUE 'N'.
          88 ADDRESS-CACHE-SI               VALUE 'S'.
          88 ADDRESS-CACHE-NO               VALUE 'N'.

       01 ADDRESS-AREA-MQ1                  PIC X(01).
          88 ADDRESS-AREA-MQ-SI             VALUE 'S'.
          88 ADDRESS-AREA-MQ-NO             VALUE 'N'.

       01 ADDRESS-VAR-AMB                   PIC X(01).
          88 ADDRESS-VAR-AMB-SI             VALUE 'S'.
          88 ADDRESS-VAR-AMB-NO             VALUE 'N'.

       01 TROVATO-DATO-IN-CACHE             PIC X(001).
          88 TROVATO-DATO-IN-CACHE-SI       VALUE 'S'.
          88 TROVATO-DATO-IN-CACHE-NO       VALUE 'N'.
          88 TROVATO-IN-CACHE-SI            VALUE 'S'.
          88 TROVATO-IN-CACHE-NO            VALUE 'N'.

13382  01 ADDRESS-STR-DATO                  PIC X(001).
13382     88 ADDRESS-STR-DATO-SI            VALUE 'S'.
13382     88 ADDRESS-STR-DATO-NO            VALUE 'N'.


       01 CACHE-PIENA                       PIC X(01).
          88 CACHE-PIENA-SI                 VALUE 'S'.
          88 CACHE-PIENA-NO                 VALUE 'N'.



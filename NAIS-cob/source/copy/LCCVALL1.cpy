      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA AST_ALLOC
      *   ALIAS ALL
      *   ULTIMO AGG. 31 MAR 2020
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-AST-ALLOC PIC S9(9)     COMP-3.
             07 (SF)-ID-STRA-DI-INVST PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-VLDT   PIC S9(8) COMP-3.
             07 (SF)-DT-END-VLDT   PIC S9(8) COMP-3.
             07 (SF)-DT-END-VLDT-NULL REDEFINES
                (SF)-DT-END-VLDT   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-COD-FND PIC X(20).
             07 (SF)-COD-FND-NULL REDEFINES
                (SF)-COD-FND   PIC X(20).
             07 (SF)-COD-TARI PIC X(12).
             07 (SF)-COD-TARI-NULL REDEFINES
                (SF)-COD-TARI   PIC X(12).
             07 (SF)-TP-APPLZ-AST PIC X(2).
             07 (SF)-TP-APPLZ-AST-NULL REDEFINES
                (SF)-TP-APPLZ-AST   PIC X(2).
             07 (SF)-PC-RIP-AST PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-RIP-AST-NULL REDEFINES
                (SF)-PC-RIP-AST   PIC X(4).
             07 (SF)-TP-FND PIC X(1).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-PERIODO PIC S9(2)     COMP-3.
             07 (SF)-PERIODO-NULL REDEFINES
                (SF)-PERIODO   PIC X(2).
             07 (SF)-TP-RIBIL-FND PIC X(2).
             07 (SF)-TP-RIBIL-FND-NULL REDEFINES
                (SF)-TP-RIBIL-FND   PIC X(2).

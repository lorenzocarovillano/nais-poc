
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVL163
      *   ULTIMO AGG. 07 GEN 2016
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-L16.
           MOVE L16-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE L16-COD-CAN
             TO (SF)-COD-CAN
           MOVE L16-COD-BLOCCO
             TO (SF)-COD-BLOCCO
           MOVE L16-TP-MOVI
             TO (SF)-TP-MOVI
           MOVE L16-GRAV-FUNZ-BLOCCO
             TO (SF)-GRAV-FUNZ-BLOCCO
           MOVE L16-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE L16-DS-VER
             TO (SF)-DS-VER
           MOVE L16-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE L16-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE L16-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           IF L16-TP-MOVI-RIFTO-NULL = HIGH-VALUES
              MOVE L16-TP-MOVI-RIFTO-NULL
                TO (SF)-TP-MOVI-RIFTO-NULL
           ELSE
              MOVE L16-TP-MOVI-RIFTO
                TO (SF)-TP-MOVI-RIFTO
           END-IF.
       VALORIZZA-OUTPUT-L16-EX.
           EXIT.

           EXEC SQL DECLARE ESTR_CNT_DIAGN_CED TABLE
           (
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             ID_POLI             DECIMAL(9, 0) NOT NULL,
             DT_LIQ_CED          DATE NOT NULL,
             COD_TARI            CHAR(12) NOT NULL,
             IMP_LRD_CEDOLE_LIQ  DECIMAL(15, 3),
             IB_POLI             CHAR(40) NOT NULL,
             DS_OPER_SQL         CHAR(1) NOT NULL,
             DS_VER              DECIMAL(9, 0) NOT NULL,
             DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL,
             DS_UTENTE           CHAR(20) NOT NULL,
             DS_STATO_ELAB       CHAR(1) NOT NULL
          ) END-EXEC.

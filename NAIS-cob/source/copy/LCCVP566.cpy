      *----------------------------------------------------------------*
      *    COPY      ..... LCCVP566
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO QUESTIONARIO ADEGUATA VERIFICA
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    POL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVP565 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN ASSET POLOCATION (LCCVP561)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-QUEST-ADEG-VER.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE QUEST-ADEG-VER

      *--> NOME TABELLA FISICA DB
           MOVE 'QUEST-ADEG-VER'                  TO WK-TABELLA

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WP56-ST-INV(IX-TAB-P56)
              AND NOT WP56-ST-CON(IX-TAB-P56)
              AND WP56-QUEST-ADEG-VER-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WP56-ST-ADD(IX-TAB-P56)

                       MOVE WPOL-ID-PTF            TO P56-ID-POLI
                       MOVE WMOV-ID-PTF            TO P56-ID-MOVI-CRZ

      *-->        ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                       PERFORM S1050-ESTR-SEQUENCE
                          THRU S1050-ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          PERFORM S1055-VALORIZZA-ID-RAPP-ANA
                             THRU S1055-VALORIZZA-ID-RAPP-ANA-EX
                       END-IF

      *-->        TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-INSERT  TO TRUE

      *-->        MODIFICA
                  WHEN WP56-ST-MOD(IX-TAB-P56)

                       MOVE WMOV-ID-PTF            TO P56-ID-MOVI-CRZ
      *-->        TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-UPDATE  TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WP56-ST-DEL(IX-TAB-P56)

17142 *                MOVE WMOV-ID-PTF            TO P56-ID-MOVI-CRZ
17142                  MOVE WP56-ID-QUEST-ADEG-VER(IX-TAB-P56)
17142                    TO P56-ID-QUEST-ADEG-VER

      *-->             TIPO OPERAZIONE DISPATCHER
17142 *                SET  IDSI0011-DELETE-LOGICA    TO TRUE
17142                  SET  IDSI0011-DELETE           TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN P56
                 PERFORM VAL-DCLGEN-P56
                    THRU VAL-DCLGEN-P56-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-P56
                    THRU VALORIZZA-AREA-DSH-P56-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-QUEST-ADEG-VER-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ESTRAZIONE SEQUENCE
      *----------------------------------------------------------------*
       S1050-ESTR-SEQUENCE.

           MOVE 'QUEST-ADEG-VER'             TO WK-TABELLA.

           PERFORM ESTR-SEQUENCE
              THRU ESTR-SEQUENCE-EX.

           IF IDSV0001-ESITO-OK
              MOVE S090-SEQ-TABELLA     TO P56-ID-QUEST-ADEG-VER
                                           WP56-ID-PTF(IX-TAB-P56)
           END-IF.

       S1050-ESTR-SEQUENCE-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA ID_RAPP_ANA
      *----------------------------------------------------------------*
       S1055-VALORIZZA-ID-RAPP-ANA.

           PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
                     UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX
AMLF3           IF WP56-ID-RAPP-ANA-NULL(IX-TAB-P56)  NOT = HIGH-VALUE
                                                      AND LOW-VALUE
                                                      AND SPACES
                IF WRAN-ID-RAPP-ANA(IX-TAB-RAN) =
                   WP56-ID-RAPP-ANA(IX-TAB-P56)
                   MOVE WRAN-ID-PTF(IX-TAB-RAN)
                     TO P56-ID-RAPP-ANA
                END-IF
                ELSE
AMLF3              MOVE HIGH-VALUE
AMLF3                TO P56-ID-RAPP-ANA-NULL
AMLF3           END-IF

      *         IF WRAN-TP-OGG(IX-TAB-RAN) = 'CO'
      *            MOVE WRAN-ID-PTF(IX-TAB-RAN)
      *              TO P56-ID-RAPP-ANA
      *         END-IF

           END-PERFORM.

       S1055-VALORIZZA-ID-RAPP-ANA-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-P56.

      *--> DCLGEN TABELLA
           MOVE QUEST-ADEG-VER                  TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-PRIMARY-KEY            TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-SENZA-STOR       TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-P56-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *      COPY LCCVP565.



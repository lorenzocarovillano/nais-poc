      *------------------------------------------------------------*
      *   PORTAFOGLIO VITA - PROCESSO VENDITA                      *
      *   CHIAMATA  AL DISPATCHER PER OPRAZIONI DI SELECT O FETCH  *
      *         E GESTIONE ERRORE (TABELLA DETT. TITOLO DI RATA)
      *   ULTIMO AGG.  19/06/2007
      *------------------------------------------------------------*
       LETTURA-DTR.

           IF IDSI0011-SELECT
              PERFORM SELECT-DTR
                 THRU SELECT-DTR-EX
           ELSE
              PERFORM FETCH-DTR
                 THRU FETCH-DTR-EX
           END-IF.

       LETTURA-DTR-EX.
           EXIT.
      *----------------------------------------------------------------*
      *                         SELECT                                 *
      *----------------------------------------------------------------*
       SELECT-DTR.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--->       OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI   TO DETT-TIT-DI-RAT
                       MOVE 1                      TO IX-TAB-DTR
                       PERFORM VALORIZZA-OUTPUT-DTR
                          THRU VALORIZZA-OUTPUT-DTR-EX

                  WHEN IDSO0011-NOT-FOUND
      *--->       CHIAVE NON TROVATA
                        MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                        MOVE 'SELECT-DTR' TO IEAI9901-LABEL-ERR
                        MOVE '005017'     TO IEAI9901-COD-ERRORE
                        MOVE SPACES       TO IEAI9901-PARAMETRI-ERR
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                           THRU EX-S0300
                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                        MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                        MOVE 'SELECT-DTR' TO IEAI9901-LABEL-ERR
                        MOVE '005016'     TO IEAI9901-COD-ERRORE
                        STRING 'DETT-TIT-DI-RAT'    ';'
                               IDSO0011-RETURN-CODE ';'
                               IDSO0011-SQLCODE
                        DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                        END-STRING
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                           THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM          TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SELECT-DTR'    TO IEAI9901-LABEL-ERR
              MOVE '005016'        TO IEAI9901-COD-ERRORE
              STRING 'DETT-TIT-DI-RAT'    ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       SELECT-DTR-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         FETCH                                  *
      *----------------------------------------------------------------*
       FETCH-DTR.

           SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
           SET  WCOM-OVERFLOW-NO                  TO TRUE.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR WCOM-OVERFLOW-YES

               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *-->                 CHIAVE NON TROVATA
                           IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM
                                TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'FETCH-DTR'
                                TO IEAI9901-LABEL-ERR
                              MOVE '005017'
                                TO IEAI9901-COD-ERRORE
                              MOVE SPACES
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                           END-IF
                      WHEN IDSO0011-SUCCESSFUL-SQL
      *-->                 OPERAZIONE ESEGUITA CORRETTAMENTE
                           MOVE IDSO0011-BUFFER-DATI  TO DETT-TIT-DI-RAT
                           ADD  1                     TO IX-TAB-DTR

      *-->  Se il contatore di lettura � maggiore dell' elemento MAX
      *-->  previsto per la TAB.DETT-TIT-DI-RAT
      *-->  allora siamo in overflow
                           IF IX-TAB-DTR > (SF)-ELE-DTRS-MAX
                              SET WCOM-OVERFLOW-YES   TO TRUE
                           ELSE
                              PERFORM VALORIZZA-OUTPUT-DTR
                                 THRU VALORIZZA-OUTPUT-DTR-EX
                              SET IDSI0011-FETCH-NEXT TO TRUE
                           END-IF
                      WHEN OTHER
      *-->                 ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'FETCH-DTR'
                             TO IEAI9901-LABEL-ERR
                           MOVE '005016'
                             TO IEAI9901-COD-ERRORE
                           STRING 'DETT-TIT-DI-RAT'            ';'
                                IDSO0011-RETURN-CODE ';'
                                IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      *-->        ERRORE DISPATCHER
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'FETCH-DTR'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005016'
                    TO IEAI9901-COD-ERRORE
                  STRING 'DETT-TIT-DI-RAT'   ';'
                        IDSO0011-RETURN-CODE ';'
                        IDSO0011-SQLCODE
                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

       FETCH-DTR-EX.
           EXIT.


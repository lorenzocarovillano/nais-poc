      *   inizio COPY IDSV0042
      * -- STRUTTURA DATI
                10 (DS)-ELEMENTS-STR-DATO OCCURS 1000 TIMES
                                          INDEXED BY (DS)-IND.
                   15 (DS)-CODICE-DATO            PIC X(030).
                   15 (DS)-FLAG-KEY               PIC X(001).
                   15 (DS)-FLAG-RETURN-CODE       PIC X(001).
                   15 (DS)-FLAG-CALL-USING        PIC X(001).
                   15 (DS)-FLAG-WHERE-COND        PIC X(001).
                   15 (DS)-FLAG-REDEFINES         PIC X(001).
                   15 (DS)-TIPO-DATO              PIC X(002).
                   15 (DS)-INIZIO-POSIZIONE   PIC S9(05) COMP-3.
                   15 (DS)-LUNGHEZZA-DATO     PIC S9(05) COMP-3.
                   15 (DS)-LUNGHEZZA-DATO-NOM PIC S9(05) COMP-3.
                   15 (DS)-PRECISIONE-DATO    PIC S9(02) COMP-3.
                   15 (DS)-FORMATTAZIONE-DATO     PIC X(020).
                   15 (DS)-SERV-CONVERSIONE       PIC X(008).
                   15 (DS)-AREA-CONV-STANDARD     PIC X(001).
                   15 (DS)-CODICE-DOMINIO         PIC X(030).
                   15 (DS)-SERVIZIO-VALIDAZIONE   PIC X(008).
                   15 (DS)-RICORRENZA             PIC S9(05) COMP-3.
                   15 (DS)-CLONE                  PIC X(01).
                   15 (DS)-OBBLIGATORIETA         PIC X(001).
                   15 (DS)-VALORE-DEFAULT         PIC X(010).
      *   fine COPY IDSV0042





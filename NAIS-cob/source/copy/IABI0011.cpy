      ****************************************************************
      * Area Input
      * BATCH EXECUTOR
      * LUNGHEZZA :
      ****************************************************************
       02 IABI0011-AREA.
           10 IABI0011-PROTOCOL                 PIC X(050).
           10 IABI0011-PROG-PROTOCOL            PIC 9(009).
           10 IABI0011-TIPO-BATCH               PIC 9(009).
           10 IABI0011-CODICE-COMPAGNIA-ANIA    PIC 9(005).
           10 IABI0011-LINGUA                   PIC X(002).
           10 IABI0011-PAESE                    PIC X(003).
           10 IABI0011-USER-NAME                PIC X(020).
           10 IABI0011-GRUPPO-AUTORIZZANTE      PIC 9(010).
           10 IABI0011-CANALE-VENDITA           PIC 9(005).
           10 IABI0011-PUNTO-VENDITA            PIC X(006).
           10 IABI0011-KEY-BUSINESS1            PIC X(020).
           10 IABI0011-KEY-BUSINESS2            PIC X(020).
           10 IABI0011-KEY-BUSINESS3            PIC X(020).
           10 IABI0011-KEY-BUSINESS4            PIC X(020).
           10 IABI0011-KEY-BUSINESS5            PIC X(020).
           10 IABI0011-MACROFUNZIONALITA        PIC X(002).
           10 IABI0011-TIPO-MOVIMENTO           PIC 9(005).
           10 IABI0011-DATA-EFFETTO             PIC 9(008).
           10 IABI0011-TS-COMPETENZA            PIC 9(018).
           10 IABI0011-FLAG-STOR-ESECUZ         PIC X(001).
              88 IABI0011-FLAG-STOR-ESECUZ-YES  VALUE 'Y'.
              88 IABI0011-FLAG-STOR-ESECUZ-NO   VALUE 'N'.
           10 IABI0011-FLAG-SIMULAZIONE         PIC X(001).
              88 IABI0011-SIMULAZIONE-INFR      VALUE 'I'.
              88 IABI0011-SIMULAZIONE-FITTIZIA  VALUE 'S'.
              88 IABI0011-SIMULAZIONE-APPL      VALUE 'A'.
              88 IABI0011-SIMULAZIONE-NO        VALUE 'N'.
           10 IABI0011-TRATTAMENTO-COMMIT       PIC X(008).
              88 IABI0011-TRATTAMENTO-PARTIAL   VALUE 'PARTIAL'.
              88 IABI0011-TRATTAMENTO-FULL      VALUE 'FULL'.
           10 IABI0011-GESTIONE-RIPARTENZA      PIC X(002).
              88 IABI0011-SENZA-RIPARTENZA      VALUE 'SR'.
              88 IABI0011-RIP-CON-MONITORING    VALUE 'CM'.
              88 IABI0011-RIP-SENZA-MONITORING  VALUE 'SM'.
              88 IABI0011-RIP-CON-VERSIONAMENTO VALUE 'CV'.
           10 IABI0011-TIPO-ORDER-BY            PIC X(003).
              88 IABI0011-ORDER-BY-IB-OGG       VALUE 'IBO'.
              88 IABI0011-ORDER-BY-ID-JOB       VALUE 'JOB'.
           10 IABI0011-LIVELLO-GRAVITA-LOG      PIC X(001).
              88 IABI0011-GRAVITA-UNO           VALUE '1'.
              88 IABI0011-GRAVITA-DUE           VALUE '2'.
              88 IABI0011-GRAVITA-TRE           VALUE '3'.
              88 IABI0011-GRAVITA-QUATTRO       VALUE '4'.
           10 IABI0011-LIVELLO-DEBUG               PIC 9(001).
              88 IABI0011-NO-DEBUG                 VALUE 0.
              88 IABI0011-DEBUG-BASSO              VALUE 1.
              88 IABI0011-DEBUG-MEDIO              VALUE 2.
              88 IABI0011-DEBUG-ELEVATO            VALUE 3.
              88 IABI0011-DEBUG-ESASPERATO         VALUE 4.
              88 IABI0011-ANY-APPL-DBG             VALUE 1,
                                                         2,
                                                         3,
                                                         4.

              88 IABI0011-ARCH-BATCH-DBG           VALUE 5.
              88 IABI0011-COM-COB-JAV-DBG          VALUE 6.
              88 IABI0011-STRESS-TEST-DBG          VALUE 7.
              88 IABI0011-BUSINESS-DBG             VALUE 8.
              88 IABI0011-TOT-TUNING-DBG           VALUE 9.
              88 IABI0011-ANY-TUNING-DBG           VALUE 5,
                                                         6,
                                                         7,
                                                         8,
                                                         9.

           10 IABI0011-TEMPORARY-TABLE       PIC X(007).
              88 IABI0011-NO-TEMP-TABLE      VALUE 'NOTABLE'.
              88 IABI0011-STATIC-TEMP-TABLE  VALUE 'STATIC'.
              88 IABI0011-SESSION-TEMP-TABLE VALUE 'SESSION'.

           10 IABI0011-LENGTH-DATA-GATES        PIC X(003).
              88 IABI0011-DATA-1K               VALUE '1K '.
              88 IABI0011-DATA-2K               VALUE '2K '.
              88 IABI0011-DATA-3K               VALUE '3K '.
              88 IABI0011-DATA-10K              VALUE '10K'.

           10 IABI0011-APPEND-DATA-GATES        PIC X(001).
              88 IABI0011-APPEND-DATA-GATES-YES VALUE 'Y'.
              88 IABI0011-APPEND-DATA-GATES-NO  VALUE 'N'.

           10 IABI0011-TAB-ADDRESS.
              15 IABI0011-ADDRESS-TYPE OCCURS 5 PIC X(001).
           10 IABI0011-TAB-ADDRESS-R REDEFINES IABI0011-TAB-ADDRESS.
              15 IABI0011-ADDRESS-TYPE1            PIC X(001).
              15 IABI0011-ADDRESS-TYPE2            PIC X(001).
              15 IABI0011-ADDRESS-TYPE3            PIC X(001).
              15 IABI0011-ADDRESS-TYPE4            PIC X(001).
              15 IABI0011-ADDRESS-TYPE5            PIC X(001).

           10 IABI0011-FORZ-RC-04               PIC X(001).
              88 IABI0011-FORZ-RC-04-YES        VALUE 'Y'.
              88 IABI0011-FORZ-RC-04-NO         VALUE 'N'.

           10 IABI0011-CONTATORE                PIC 9(005).

           10 IABI0011-ACCESSO-BATCH            PIC X(06).
              88 IABI0011-ACCESSO-ALL-BATCH     VALUE 'ALLBTC'.
              88 IABI0011-ACCESSO-X-DT-EFF-MAX  VALUE 'MAXEFF'.
              88 IABI0011-ACCESSO-X-DT-EFF-MIN  VALUE 'MINEFF'.
              88 IABI0011-ACCESSO-X-DT-INS-MAX  VALUE 'MAXINS'.
              88 IABI0011-ACCESSO-X-DT-INS-MIN  VALUE 'MININS'.

           10 IABI0011-BUFFER-WHERE-COND        PIC X(300).

           EXEC SQL DECLARE LINGUA_ERRORE TABLE
           (
             ID_LINGUA_ERRORE    DECIMAL(9, 0) NOT NULL,
             COD_ERRORE          INTEGER NOT NULL,
             COD_COMPAGNIA_ANIA  DECIMAL(5, 0),
             LINGUA              CHAR(2) NOT NULL,
             PAESE               CHAR(3) NOT NULL,
             DESC_ERRORE_BREVE   VARCHAR(100) NOT NULL,
             DESC_ERRORE_ESTESA  VARCHAR(200) NOT NULL
          ) END-EXEC.

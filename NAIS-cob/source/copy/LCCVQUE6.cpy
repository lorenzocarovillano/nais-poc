      *----------------------------------------------------------------*
      *    COPY      ..... LCCVQUE6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO QUESTIONARIO
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-QUEST.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE QUEST.

      *--> NOME TABELLA FISICA DB
           MOVE 'QUEST'                  TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF  NOT WQUE-ST-INV(IX-TAB-QUE)
           AND NOT WQUE-ST-CON(IX-TAB-QUE)
           AND     WQUE-ELE-QUEST-MAX NOT = 0

      *-->    Ricerca dell'ID-RAPP-ANAG nella tab. Rapp-Anag.
              MOVE WQUE-ID-RAPP-ANA(IX-TAB-QUE)  TO WS-ID-RAPP-ANA
              PERFORM RICERCA-ID-RAN             THRU RICERCA-ID-RAN-EX

      *--->   Impostare ID Tabella QUESTIONARIO
              MOVE WMOV-ID-PTF                   TO QUE-ID-MOVI-CRZ

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WQUE-ST-ADD(IX-TAB-QUE)

                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK
                        MOVE S090-SEQ-TABELLA TO WQUE-ID-PTF(IX-TAB-QUE)
                                                 QUE-ID-QUEST
                        MOVE WS-ID-PTF        TO QUE-ID-RAPP-ANA
                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WQUE-ST-DEL(IX-TAB-QUE)

                     MOVE WQUE-ID-PTF(IX-TAB-QUE) TO QUE-ID-QUEST
                     MOVE WS-ID-PTF               TO QUE-ID-RAPP-ANA

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-DELETE-LOGICA  TO TRUE

      *-->        MODIFICA
                  WHEN WQUE-ST-MOD(IX-TAB-QUE)

                     MOVE WQUE-ID-PTF(IX-TAB-QUE) TO QUE-ID-QUEST
                     MOVE WS-ID-PTF               TO QUE-ID-RAPP-ANA

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN QUESTIONARIO
                 PERFORM VAL-DCLGEN-QUE
                    THRU VAL-DCLGEN-QUE-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-QUE
                    THRU VALORIZZA-AREA-DSH-QUE-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-QUEST-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    RICERCA ID RAPPORTO ANAGRAFICO
      *----------------------------------------------------------------*
       RICERCA-ID-RAN.

           SET NON-TROVATO TO TRUE.

           PERFORM VARYING IX-TAB-RAN FROM 1 BY 1
             UNTIL IX-TAB-RAN > WRAN-ELE-RAPP-ANAG-MAX OR TROVATO

                IF WRAN-ID-RAPP-ANA(IX-TAB-RAN) = WS-ID-RAPP-ANA

                   MOVE WRAN-ID-PTF(IX-TAB-RAN) TO WS-ID-PTF
                   SET TROVATO                  TO TRUE

                END-IF

           END-PERFORM.

       RICERCA-ID-RAN-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-QUE.

      *--> DCLGEN TABELLA
           MOVE QUEST                   TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-QUE-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVQUE5.

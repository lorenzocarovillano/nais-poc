
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVPCO3
      *   ULTIMO AGG. 13 NOV 2018
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-PCO.
           MOVE PCO-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           IF PCO-COD-TRAT-CIRT-NULL = HIGH-VALUES
              MOVE PCO-COD-TRAT-CIRT-NULL
                TO (SF)-COD-TRAT-CIRT-NULL
           ELSE
              MOVE PCO-COD-TRAT-CIRT
                TO (SF)-COD-TRAT-CIRT
           END-IF
           IF PCO-LIM-VLTR-NULL = HIGH-VALUES
              MOVE PCO-LIM-VLTR-NULL
                TO (SF)-LIM-VLTR-NULL
           ELSE
              MOVE PCO-LIM-VLTR
                TO (SF)-LIM-VLTR
           END-IF
           IF PCO-TP-RAT-PERF-NULL = HIGH-VALUES
              MOVE PCO-TP-RAT-PERF-NULL
                TO (SF)-TP-RAT-PERF-NULL
           ELSE
              MOVE PCO-TP-RAT-PERF
                TO (SF)-TP-RAT-PERF
           END-IF
           MOVE PCO-TP-LIV-GENZ-TIT
             TO (SF)-TP-LIV-GENZ-TIT
           IF PCO-ARROT-PRE-NULL = HIGH-VALUES
              MOVE PCO-ARROT-PRE-NULL
                TO (SF)-ARROT-PRE-NULL
           ELSE
              MOVE PCO-ARROT-PRE
                TO (SF)-ARROT-PRE
           END-IF
           IF PCO-DT-CONT-NULL = HIGH-VALUES
              MOVE PCO-DT-CONT-NULL
                TO (SF)-DT-CONT-NULL
           ELSE
              MOVE PCO-DT-CONT
                TO (SF)-DT-CONT
           END-IF
           IF PCO-DT-ULT-RIVAL-IN-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-RIVAL-IN-NULL
                TO (SF)-DT-ULT-RIVAL-IN-NULL
           ELSE
              MOVE PCO-DT-ULT-RIVAL-IN
                TO (SF)-DT-ULT-RIVAL-IN
           END-IF
           IF PCO-DT-ULT-QTZO-IN-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-QTZO-IN-NULL
                TO (SF)-DT-ULT-QTZO-IN-NULL
           ELSE
              MOVE PCO-DT-ULT-QTZO-IN
                TO (SF)-DT-ULT-QTZO-IN
           END-IF
           IF PCO-DT-ULT-RICL-RIASS-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-RICL-RIASS-NULL
                TO (SF)-DT-ULT-RICL-RIASS-NULL
           ELSE
              MOVE PCO-DT-ULT-RICL-RIASS
                TO (SF)-DT-ULT-RICL-RIASS
           END-IF
           IF PCO-DT-ULT-TABUL-RIASS-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-TABUL-RIASS-NULL
                TO (SF)-DT-ULT-TABUL-RIASS-NULL
           ELSE
              MOVE PCO-DT-ULT-TABUL-RIASS
                TO (SF)-DT-ULT-TABUL-RIASS
           END-IF
           IF PCO-DT-ULT-BOLL-EMES-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-EMES-NULL
                TO (SF)-DT-ULT-BOLL-EMES-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-EMES
                TO (SF)-DT-ULT-BOLL-EMES
           END-IF
           IF PCO-DT-ULT-BOLL-STOR-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-STOR-NULL
                TO (SF)-DT-ULT-BOLL-STOR-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-STOR
                TO (SF)-DT-ULT-BOLL-STOR
           END-IF
           IF PCO-DT-ULT-BOLL-LIQ-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-LIQ-NULL
                TO (SF)-DT-ULT-BOLL-LIQ-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-LIQ
                TO (SF)-DT-ULT-BOLL-LIQ
           END-IF
           IF PCO-DT-ULT-BOLL-RIAT-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-RIAT-NULL
                TO (SF)-DT-ULT-BOLL-RIAT-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-RIAT
                TO (SF)-DT-ULT-BOLL-RIAT
           END-IF
           IF PCO-DT-ULTELRISCPAR-PR-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTELRISCPAR-PR-NULL
                TO (SF)-DT-ULTELRISCPAR-PR-NULL
           ELSE
              MOVE PCO-DT-ULTELRISCPAR-PR
                TO (SF)-DT-ULTELRISCPAR-PR
           END-IF
           IF PCO-DT-ULTC-IS-IN-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTC-IS-IN-NULL
                TO (SF)-DT-ULTC-IS-IN-NULL
           ELSE
              MOVE PCO-DT-ULTC-IS-IN
                TO (SF)-DT-ULTC-IS-IN
           END-IF
           IF PCO-DT-ULT-RICL-PRE-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-RICL-PRE-NULL
                TO (SF)-DT-ULT-RICL-PRE-NULL
           ELSE
              MOVE PCO-DT-ULT-RICL-PRE
                TO (SF)-DT-ULT-RICL-PRE
           END-IF
           IF PCO-DT-ULTC-MARSOL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTC-MARSOL-NULL
                TO (SF)-DT-ULTC-MARSOL-NULL
           ELSE
              MOVE PCO-DT-ULTC-MARSOL
                TO (SF)-DT-ULTC-MARSOL
           END-IF
           IF PCO-DT-ULTC-RB-IN-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTC-RB-IN-NULL
                TO (SF)-DT-ULTC-RB-IN-NULL
           ELSE
              MOVE PCO-DT-ULTC-RB-IN
                TO (SF)-DT-ULTC-RB-IN
           END-IF
           IF PCO-PC-PROV-1AA-ACQ-NULL = HIGH-VALUES
              MOVE PCO-PC-PROV-1AA-ACQ-NULL
                TO (SF)-PC-PROV-1AA-ACQ-NULL
           ELSE
              MOVE PCO-PC-PROV-1AA-ACQ
                TO (SF)-PC-PROV-1AA-ACQ
           END-IF
           IF PCO-MOD-INTR-PREST-NULL = HIGH-VALUES
              MOVE PCO-MOD-INTR-PREST-NULL
                TO (SF)-MOD-INTR-PREST-NULL
           ELSE
              MOVE PCO-MOD-INTR-PREST
                TO (SF)-MOD-INTR-PREST
           END-IF
           IF PCO-GG-MAX-REC-PROV-NULL = HIGH-VALUES
              MOVE PCO-GG-MAX-REC-PROV-NULL
                TO (SF)-GG-MAX-REC-PROV-NULL
           ELSE
              MOVE PCO-GG-MAX-REC-PROV
                TO (SF)-GG-MAX-REC-PROV
           END-IF
           IF PCO-DT-ULTGZ-TRCH-E-IN-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTGZ-TRCH-E-IN-NULL
                TO (SF)-DT-ULTGZ-TRCH-E-IN-NULL
           ELSE
              MOVE PCO-DT-ULTGZ-TRCH-E-IN
                TO (SF)-DT-ULTGZ-TRCH-E-IN
           END-IF
           IF PCO-DT-ULT-BOLL-SNDEN-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-SNDEN-NULL
                TO (SF)-DT-ULT-BOLL-SNDEN-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-SNDEN
                TO (SF)-DT-ULT-BOLL-SNDEN
           END-IF
           IF PCO-DT-ULT-BOLL-SNDNLQ-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-SNDNLQ-NULL
                TO (SF)-DT-ULT-BOLL-SNDNLQ-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-SNDNLQ
                TO (SF)-DT-ULT-BOLL-SNDNLQ
           END-IF
           IF PCO-DT-ULTSC-ELAB-IN-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTSC-ELAB-IN-NULL
                TO (SF)-DT-ULTSC-ELAB-IN-NULL
           ELSE
              MOVE PCO-DT-ULTSC-ELAB-IN
                TO (SF)-DT-ULTSC-ELAB-IN
           END-IF
           IF PCO-DT-ULTSC-OPZ-IN-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTSC-OPZ-IN-NULL
                TO (SF)-DT-ULTSC-OPZ-IN-NULL
           ELSE
              MOVE PCO-DT-ULTSC-OPZ-IN
                TO (SF)-DT-ULTSC-OPZ-IN
           END-IF
           IF PCO-DT-ULTC-BNSRIC-IN-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTC-BNSRIC-IN-NULL
                TO (SF)-DT-ULTC-BNSRIC-IN-NULL
           ELSE
              MOVE PCO-DT-ULTC-BNSRIC-IN
                TO (SF)-DT-ULTC-BNSRIC-IN
           END-IF
           IF PCO-DT-ULTC-BNSFDT-IN-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTC-BNSFDT-IN-NULL
                TO (SF)-DT-ULTC-BNSFDT-IN-NULL
           ELSE
              MOVE PCO-DT-ULTC-BNSFDT-IN
                TO (SF)-DT-ULTC-BNSFDT-IN
           END-IF
           IF PCO-DT-ULT-RINN-GARAC-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-RINN-GARAC-NULL
                TO (SF)-DT-ULT-RINN-GARAC-NULL
           ELSE
              MOVE PCO-DT-ULT-RINN-GARAC
                TO (SF)-DT-ULT-RINN-GARAC
           END-IF
           IF PCO-DT-ULTGZ-CED-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTGZ-CED-NULL
                TO (SF)-DT-ULTGZ-CED-NULL
           ELSE
              MOVE PCO-DT-ULTGZ-CED
                TO (SF)-DT-ULTGZ-CED
           END-IF
           IF PCO-DT-ULT-ELAB-PRLCOS-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ELAB-PRLCOS-NULL
                TO (SF)-DT-ULT-ELAB-PRLCOS-NULL
           ELSE
              MOVE PCO-DT-ULT-ELAB-PRLCOS
                TO (SF)-DT-ULT-ELAB-PRLCOS
           END-IF
           IF PCO-DT-ULT-RINN-COLL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-RINN-COLL-NULL
                TO (SF)-DT-ULT-RINN-COLL-NULL
           ELSE
              MOVE PCO-DT-ULT-RINN-COLL
                TO (SF)-DT-ULT-RINN-COLL
           END-IF
           IF PCO-FL-RVC-PERF-NULL = HIGH-VALUES
              MOVE PCO-FL-RVC-PERF-NULL
                TO (SF)-FL-RVC-PERF-NULL
           ELSE
              MOVE PCO-FL-RVC-PERF
                TO (SF)-FL-RVC-PERF
           END-IF
           IF PCO-FL-RCS-POLI-NOPERF-NULL = HIGH-VALUES
              MOVE PCO-FL-RCS-POLI-NOPERF-NULL
                TO (SF)-FL-RCS-POLI-NOPERF-NULL
           ELSE
              MOVE PCO-FL-RCS-POLI-NOPERF
                TO (SF)-FL-RCS-POLI-NOPERF
           END-IF
           IF PCO-FL-GEST-PLUSV-NULL = HIGH-VALUES
              MOVE PCO-FL-GEST-PLUSV-NULL
                TO (SF)-FL-GEST-PLUSV-NULL
           ELSE
              MOVE PCO-FL-GEST-PLUSV
                TO (SF)-FL-GEST-PLUSV
           END-IF
           IF PCO-DT-ULT-RIVAL-CL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-RIVAL-CL-NULL
                TO (SF)-DT-ULT-RIVAL-CL-NULL
           ELSE
              MOVE PCO-DT-ULT-RIVAL-CL
                TO (SF)-DT-ULT-RIVAL-CL
           END-IF
           IF PCO-DT-ULT-QTZO-CL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-QTZO-CL-NULL
                TO (SF)-DT-ULT-QTZO-CL-NULL
           ELSE
              MOVE PCO-DT-ULT-QTZO-CL
                TO (SF)-DT-ULT-QTZO-CL
           END-IF
           IF PCO-DT-ULTC-BNSRIC-CL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTC-BNSRIC-CL-NULL
                TO (SF)-DT-ULTC-BNSRIC-CL-NULL
           ELSE
              MOVE PCO-DT-ULTC-BNSRIC-CL
                TO (SF)-DT-ULTC-BNSRIC-CL
           END-IF
           IF PCO-DT-ULTC-BNSFDT-CL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTC-BNSFDT-CL-NULL
                TO (SF)-DT-ULTC-BNSFDT-CL-NULL
           ELSE
              MOVE PCO-DT-ULTC-BNSFDT-CL
                TO (SF)-DT-ULTC-BNSFDT-CL
           END-IF
           IF PCO-DT-ULTC-IS-CL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTC-IS-CL-NULL
                TO (SF)-DT-ULTC-IS-CL-NULL
           ELSE
              MOVE PCO-DT-ULTC-IS-CL
                TO (SF)-DT-ULTC-IS-CL
           END-IF
           IF PCO-DT-ULTC-RB-CL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTC-RB-CL-NULL
                TO (SF)-DT-ULTC-RB-CL-NULL
           ELSE
              MOVE PCO-DT-ULTC-RB-CL
                TO (SF)-DT-ULTC-RB-CL
           END-IF
           IF PCO-DT-ULTGZ-TRCH-E-CL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTGZ-TRCH-E-CL-NULL
                TO (SF)-DT-ULTGZ-TRCH-E-CL-NULL
           ELSE
              MOVE PCO-DT-ULTGZ-TRCH-E-CL
                TO (SF)-DT-ULTGZ-TRCH-E-CL
           END-IF
           IF PCO-DT-ULTSC-ELAB-CL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTSC-ELAB-CL-NULL
                TO (SF)-DT-ULTSC-ELAB-CL-NULL
           ELSE
              MOVE PCO-DT-ULTSC-ELAB-CL
                TO (SF)-DT-ULTSC-ELAB-CL
           END-IF
           IF PCO-DT-ULTSC-OPZ-CL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTSC-OPZ-CL-NULL
                TO (SF)-DT-ULTSC-OPZ-CL-NULL
           ELSE
              MOVE PCO-DT-ULTSC-OPZ-CL
                TO (SF)-DT-ULTSC-OPZ-CL
           END-IF
           IF PCO-STST-X-REGIONE-NULL = HIGH-VALUES
              MOVE PCO-STST-X-REGIONE-NULL
                TO (SF)-STST-X-REGIONE-NULL
           ELSE
              MOVE PCO-STST-X-REGIONE
                TO (SF)-STST-X-REGIONE
           END-IF
           IF PCO-DT-ULTGZ-CED-COLL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTGZ-CED-COLL-NULL
                TO (SF)-DT-ULTGZ-CED-COLL-NULL
           ELSE
              MOVE PCO-DT-ULTGZ-CED-COLL
                TO (SF)-DT-ULTGZ-CED-COLL
           END-IF
           MOVE PCO-TP-MOD-RIVAL
             TO (SF)-TP-MOD-RIVAL
           IF PCO-NUM-MM-CALC-MORA-NULL = HIGH-VALUES
              MOVE PCO-NUM-MM-CALC-MORA-NULL
                TO (SF)-NUM-MM-CALC-MORA-NULL
           ELSE
              MOVE PCO-NUM-MM-CALC-MORA
                TO (SF)-NUM-MM-CALC-MORA
           END-IF
           IF PCO-DT-ULT-EC-RIV-COLL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-EC-RIV-COLL-NULL
                TO (SF)-DT-ULT-EC-RIV-COLL-NULL
           ELSE
              MOVE PCO-DT-ULT-EC-RIV-COLL
                TO (SF)-DT-ULT-EC-RIV-COLL
           END-IF
           IF PCO-DT-ULT-EC-RIV-IND-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-EC-RIV-IND-NULL
                TO (SF)-DT-ULT-EC-RIV-IND-NULL
           ELSE
              MOVE PCO-DT-ULT-EC-RIV-IND
                TO (SF)-DT-ULT-EC-RIV-IND
           END-IF
           IF PCO-DT-ULT-EC-IL-COLL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-EC-IL-COLL-NULL
                TO (SF)-DT-ULT-EC-IL-COLL-NULL
           ELSE
              MOVE PCO-DT-ULT-EC-IL-COLL
                TO (SF)-DT-ULT-EC-IL-COLL
           END-IF
           IF PCO-DT-ULT-EC-IL-IND-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-EC-IL-IND-NULL
                TO (SF)-DT-ULT-EC-IL-IND-NULL
           ELSE
              MOVE PCO-DT-ULT-EC-IL-IND
                TO (SF)-DT-ULT-EC-IL-IND
           END-IF
           IF PCO-DT-ULT-EC-UL-COLL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-EC-UL-COLL-NULL
                TO (SF)-DT-ULT-EC-UL-COLL-NULL
           ELSE
              MOVE PCO-DT-ULT-EC-UL-COLL
                TO (SF)-DT-ULT-EC-UL-COLL
           END-IF
           IF PCO-DT-ULT-EC-UL-IND-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-EC-UL-IND-NULL
                TO (SF)-DT-ULT-EC-UL-IND-NULL
           ELSE
              MOVE PCO-DT-ULT-EC-UL-IND
                TO (SF)-DT-ULT-EC-UL-IND
           END-IF
           IF PCO-AA-UTI-NULL = HIGH-VALUES
              MOVE PCO-AA-UTI-NULL
                TO (SF)-AA-UTI-NULL
           ELSE
              MOVE PCO-AA-UTI
                TO (SF)-AA-UTI
           END-IF
           MOVE PCO-CALC-RSH-COMUN
             TO (SF)-CALC-RSH-COMUN
           MOVE PCO-FL-LIV-DEBUG
             TO (SF)-FL-LIV-DEBUG
           IF PCO-DT-ULT-BOLL-PERF-C-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-PERF-C-NULL
                TO (SF)-DT-ULT-BOLL-PERF-C-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-PERF-C
                TO (SF)-DT-ULT-BOLL-PERF-C
           END-IF
           IF PCO-DT-ULT-BOLL-RSP-IN-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-RSP-IN-NULL
                TO (SF)-DT-ULT-BOLL-RSP-IN-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-RSP-IN
                TO (SF)-DT-ULT-BOLL-RSP-IN
           END-IF
           IF PCO-DT-ULT-BOLL-RSP-CL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-RSP-CL-NULL
                TO (SF)-DT-ULT-BOLL-RSP-CL-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-RSP-CL
                TO (SF)-DT-ULT-BOLL-RSP-CL
           END-IF
           IF PCO-DT-ULT-BOLL-EMES-I-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-EMES-I-NULL
                TO (SF)-DT-ULT-BOLL-EMES-I-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-EMES-I
                TO (SF)-DT-ULT-BOLL-EMES-I
           END-IF
           IF PCO-DT-ULT-BOLL-STOR-I-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-STOR-I-NULL
                TO (SF)-DT-ULT-BOLL-STOR-I-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-STOR-I
                TO (SF)-DT-ULT-BOLL-STOR-I
           END-IF
           IF PCO-DT-ULT-BOLL-RIAT-I-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-RIAT-I-NULL
                TO (SF)-DT-ULT-BOLL-RIAT-I-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-RIAT-I
                TO (SF)-DT-ULT-BOLL-RIAT-I
           END-IF
           IF PCO-DT-ULT-BOLL-SD-I-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-SD-I-NULL
                TO (SF)-DT-ULT-BOLL-SD-I-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-SD-I
                TO (SF)-DT-ULT-BOLL-SD-I
           END-IF
           IF PCO-DT-ULT-BOLL-SDNL-I-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-SDNL-I-NULL
                TO (SF)-DT-ULT-BOLL-SDNL-I-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-SDNL-I
                TO (SF)-DT-ULT-BOLL-SDNL-I
           END-IF
           IF PCO-DT-ULT-BOLL-PERF-I-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-PERF-I-NULL
                TO (SF)-DT-ULT-BOLL-PERF-I-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-PERF-I
                TO (SF)-DT-ULT-BOLL-PERF-I
           END-IF
           IF PCO-DT-RICL-RIRIAS-COM-NULL = HIGH-VALUES
              MOVE PCO-DT-RICL-RIRIAS-COM-NULL
                TO (SF)-DT-RICL-RIRIAS-COM-NULL
           ELSE
              MOVE PCO-DT-RICL-RIRIAS-COM
                TO (SF)-DT-RICL-RIRIAS-COM
           END-IF
           IF PCO-DT-ULT-ELAB-AT92-C-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ELAB-AT92-C-NULL
                TO (SF)-DT-ULT-ELAB-AT92-C-NULL
           ELSE
              MOVE PCO-DT-ULT-ELAB-AT92-C
                TO (SF)-DT-ULT-ELAB-AT92-C
           END-IF
           IF PCO-DT-ULT-ELAB-AT92-I-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ELAB-AT92-I-NULL
                TO (SF)-DT-ULT-ELAB-AT92-I-NULL
           ELSE
              MOVE PCO-DT-ULT-ELAB-AT92-I
                TO (SF)-DT-ULT-ELAB-AT92-I
           END-IF
           IF PCO-DT-ULT-ELAB-AT93-C-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ELAB-AT93-C-NULL
                TO (SF)-DT-ULT-ELAB-AT93-C-NULL
           ELSE
              MOVE PCO-DT-ULT-ELAB-AT93-C
                TO (SF)-DT-ULT-ELAB-AT93-C
           END-IF
           IF PCO-DT-ULT-ELAB-AT93-I-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ELAB-AT93-I-NULL
                TO (SF)-DT-ULT-ELAB-AT93-I-NULL
           ELSE
              MOVE PCO-DT-ULT-ELAB-AT93-I
                TO (SF)-DT-ULT-ELAB-AT93-I
           END-IF
           IF PCO-DT-ULT-ELAB-SPE-IN-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ELAB-SPE-IN-NULL
                TO (SF)-DT-ULT-ELAB-SPE-IN-NULL
           ELSE
              MOVE PCO-DT-ULT-ELAB-SPE-IN
                TO (SF)-DT-ULT-ELAB-SPE-IN
           END-IF
           IF PCO-DT-ULT-ELAB-PR-CON-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ELAB-PR-CON-NULL
                TO (SF)-DT-ULT-ELAB-PR-CON-NULL
           ELSE
              MOVE PCO-DT-ULT-ELAB-PR-CON
                TO (SF)-DT-ULT-ELAB-PR-CON
           END-IF
           IF PCO-DT-ULT-BOLL-RP-CL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-RP-CL-NULL
                TO (SF)-DT-ULT-BOLL-RP-CL-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-RP-CL
                TO (SF)-DT-ULT-BOLL-RP-CL
           END-IF
           IF PCO-DT-ULT-BOLL-RP-IN-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-RP-IN-NULL
                TO (SF)-DT-ULT-BOLL-RP-IN-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-RP-IN
                TO (SF)-DT-ULT-BOLL-RP-IN
           END-IF
           IF PCO-DT-ULT-BOLL-PRE-I-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-PRE-I-NULL
                TO (SF)-DT-ULT-BOLL-PRE-I-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-PRE-I
                TO (SF)-DT-ULT-BOLL-PRE-I
           END-IF
           IF PCO-DT-ULT-BOLL-PRE-C-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-PRE-C-NULL
                TO (SF)-DT-ULT-BOLL-PRE-C-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-PRE-C
                TO (SF)-DT-ULT-BOLL-PRE-C
           END-IF
           IF PCO-DT-ULTC-PILDI-MM-C-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTC-PILDI-MM-C-NULL
                TO (SF)-DT-ULTC-PILDI-MM-C-NULL
           ELSE
              MOVE PCO-DT-ULTC-PILDI-MM-C
                TO (SF)-DT-ULTC-PILDI-MM-C
           END-IF
           IF PCO-DT-ULTC-PILDI-AA-C-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTC-PILDI-AA-C-NULL
                TO (SF)-DT-ULTC-PILDI-AA-C-NULL
           ELSE
              MOVE PCO-DT-ULTC-PILDI-AA-C
                TO (SF)-DT-ULTC-PILDI-AA-C
           END-IF
           IF PCO-DT-ULTC-PILDI-MM-I-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTC-PILDI-MM-I-NULL
                TO (SF)-DT-ULTC-PILDI-MM-I-NULL
           ELSE
              MOVE PCO-DT-ULTC-PILDI-MM-I
                TO (SF)-DT-ULTC-PILDI-MM-I
           END-IF
           IF PCO-DT-ULTC-PILDI-TR-I-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTC-PILDI-TR-I-NULL
                TO (SF)-DT-ULTC-PILDI-TR-I-NULL
           ELSE
              MOVE PCO-DT-ULTC-PILDI-TR-I
                TO (SF)-DT-ULTC-PILDI-TR-I
           END-IF
           IF PCO-DT-ULTC-PILDI-AA-I-NULL = HIGH-VALUES
              MOVE PCO-DT-ULTC-PILDI-AA-I-NULL
                TO (SF)-DT-ULTC-PILDI-AA-I-NULL
           ELSE
              MOVE PCO-DT-ULTC-PILDI-AA-I
                TO (SF)-DT-ULTC-PILDI-AA-I
           END-IF
           IF PCO-DT-ULT-BOLL-QUIE-C-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-QUIE-C-NULL
                TO (SF)-DT-ULT-BOLL-QUIE-C-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-QUIE-C
                TO (SF)-DT-ULT-BOLL-QUIE-C
           END-IF
           IF PCO-DT-ULT-BOLL-QUIE-I-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-QUIE-I-NULL
                TO (SF)-DT-ULT-BOLL-QUIE-I-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-QUIE-I
                TO (SF)-DT-ULT-BOLL-QUIE-I
           END-IF
           IF PCO-DT-ULT-BOLL-COTR-I-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-COTR-I-NULL
                TO (SF)-DT-ULT-BOLL-COTR-I-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-COTR-I
                TO (SF)-DT-ULT-BOLL-COTR-I
           END-IF
           IF PCO-DT-ULT-BOLL-COTR-C-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-COTR-C-NULL
                TO (SF)-DT-ULT-BOLL-COTR-C-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-COTR-C
                TO (SF)-DT-ULT-BOLL-COTR-C
           END-IF
           IF PCO-DT-ULT-BOLL-CORI-C-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-CORI-C-NULL
                TO (SF)-DT-ULT-BOLL-CORI-C-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-CORI-C
                TO (SF)-DT-ULT-BOLL-CORI-C
           END-IF
           IF PCO-DT-ULT-BOLL-CORI-I-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-BOLL-CORI-I-NULL
                TO (SF)-DT-ULT-BOLL-CORI-I-NULL
           ELSE
              MOVE PCO-DT-ULT-BOLL-CORI-I
                TO (SF)-DT-ULT-BOLL-CORI-I
           END-IF
           MOVE PCO-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE PCO-DS-VER
             TO (SF)-DS-VER
           MOVE PCO-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE PCO-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE PCO-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           IF PCO-TP-VALZZ-DT-VLT-NULL = HIGH-VALUES
              MOVE PCO-TP-VALZZ-DT-VLT-NULL
                TO (SF)-TP-VALZZ-DT-VLT-NULL
           ELSE
              MOVE PCO-TP-VALZZ-DT-VLT
                TO (SF)-TP-VALZZ-DT-VLT
           END-IF
           IF PCO-FL-FRAZ-PROV-ACQ-NULL = HIGH-VALUES
              MOVE PCO-FL-FRAZ-PROV-ACQ-NULL
                TO (SF)-FL-FRAZ-PROV-ACQ-NULL
           ELSE
              MOVE PCO-FL-FRAZ-PROV-ACQ
                TO (SF)-FL-FRAZ-PROV-ACQ
           END-IF
           IF PCO-DT-ULT-AGG-EROG-RE-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-AGG-EROG-RE-NULL
                TO (SF)-DT-ULT-AGG-EROG-RE-NULL
           ELSE
              MOVE PCO-DT-ULT-AGG-EROG-RE
                TO (SF)-DT-ULT-AGG-EROG-RE
           END-IF
           IF PCO-PC-RM-MARSOL-NULL = HIGH-VALUES
              MOVE PCO-PC-RM-MARSOL-NULL
                TO (SF)-PC-RM-MARSOL-NULL
           ELSE
              MOVE PCO-PC-RM-MARSOL
                TO (SF)-PC-RM-MARSOL
           END-IF
           IF PCO-PC-C-SUBRSH-MARSOL-NULL = HIGH-VALUES
              MOVE PCO-PC-C-SUBRSH-MARSOL-NULL
                TO (SF)-PC-C-SUBRSH-MARSOL-NULL
           ELSE
              MOVE PCO-PC-C-SUBRSH-MARSOL
                TO (SF)-PC-C-SUBRSH-MARSOL
           END-IF
           IF PCO-COD-COMP-ISVAP-NULL = HIGH-VALUES
              MOVE PCO-COD-COMP-ISVAP-NULL
                TO (SF)-COD-COMP-ISVAP-NULL
           ELSE
              MOVE PCO-COD-COMP-ISVAP
                TO (SF)-COD-COMP-ISVAP
           END-IF
           IF PCO-LM-RIS-CON-INT-NULL = HIGH-VALUES
              MOVE PCO-LM-RIS-CON-INT-NULL
                TO (SF)-LM-RIS-CON-INT-NULL
           ELSE
              MOVE PCO-LM-RIS-CON-INT
                TO (SF)-LM-RIS-CON-INT
           END-IF
           IF PCO-LM-C-SUBRSH-CON-IN-NULL = HIGH-VALUES
              MOVE PCO-LM-C-SUBRSH-CON-IN-NULL
                TO (SF)-LM-C-SUBRSH-CON-IN-NULL
           ELSE
              MOVE PCO-LM-C-SUBRSH-CON-IN
                TO (SF)-LM-C-SUBRSH-CON-IN
           END-IF
           IF PCO-PC-GAR-NORISK-MARS-NULL = HIGH-VALUES
              MOVE PCO-PC-GAR-NORISK-MARS-NULL
                TO (SF)-PC-GAR-NORISK-MARS-NULL
           ELSE
              MOVE PCO-PC-GAR-NORISK-MARS
                TO (SF)-PC-GAR-NORISK-MARS
           END-IF
           IF PCO-CRZ-1A-RAT-INTR-PR-NULL = HIGH-VALUES
              MOVE PCO-CRZ-1A-RAT-INTR-PR-NULL
                TO (SF)-CRZ-1A-RAT-INTR-PR-NULL
           ELSE
              MOVE PCO-CRZ-1A-RAT-INTR-PR
                TO (SF)-CRZ-1A-RAT-INTR-PR
           END-IF
           IF PCO-NUM-GG-ARR-INTR-PR-NULL = HIGH-VALUES
              MOVE PCO-NUM-GG-ARR-INTR-PR-NULL
                TO (SF)-NUM-GG-ARR-INTR-PR-NULL
           ELSE
              MOVE PCO-NUM-GG-ARR-INTR-PR
                TO (SF)-NUM-GG-ARR-INTR-PR
           END-IF
           IF PCO-FL-VISUAL-VINPG-NULL = HIGH-VALUES
              MOVE PCO-FL-VISUAL-VINPG-NULL
                TO (SF)-FL-VISUAL-VINPG-NULL
           ELSE
              MOVE PCO-FL-VISUAL-VINPG
                TO (SF)-FL-VISUAL-VINPG
           END-IF
           IF PCO-DT-ULT-ESTRAZ-FUG-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ESTRAZ-FUG-NULL
                TO (SF)-DT-ULT-ESTRAZ-FUG-NULL
           ELSE
              MOVE PCO-DT-ULT-ESTRAZ-FUG
                TO (SF)-DT-ULT-ESTRAZ-FUG
           END-IF
           IF PCO-DT-ULT-ELAB-PR-AUT-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ELAB-PR-AUT-NULL
                TO (SF)-DT-ULT-ELAB-PR-AUT-NULL
           ELSE
              MOVE PCO-DT-ULT-ELAB-PR-AUT
                TO (SF)-DT-ULT-ELAB-PR-AUT
           END-IF
           IF PCO-DT-ULT-ELAB-COMMEF-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ELAB-COMMEF-NULL
                TO (SF)-DT-ULT-ELAB-COMMEF-NULL
           ELSE
              MOVE PCO-DT-ULT-ELAB-COMMEF
                TO (SF)-DT-ULT-ELAB-COMMEF
           END-IF
           IF PCO-DT-ULT-ELAB-LIQMEF-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ELAB-LIQMEF-NULL
                TO (SF)-DT-ULT-ELAB-LIQMEF-NULL
           ELSE
              MOVE PCO-DT-ULT-ELAB-LIQMEF
                TO (SF)-DT-ULT-ELAB-LIQMEF
           END-IF
           IF PCO-COD-FISC-MEF-NULL = HIGH-VALUES
              MOVE PCO-COD-FISC-MEF-NULL
                TO (SF)-COD-FISC-MEF-NULL
           ELSE
              MOVE PCO-COD-FISC-MEF
                TO (SF)-COD-FISC-MEF
           END-IF
           IF PCO-IMP-ASS-SOCIALE-NULL = HIGH-VALUES
              MOVE PCO-IMP-ASS-SOCIALE-NULL
                TO (SF)-IMP-ASS-SOCIALE-NULL
           ELSE
              MOVE PCO-IMP-ASS-SOCIALE
                TO (SF)-IMP-ASS-SOCIALE
           END-IF
           IF PCO-MOD-COMNZ-INVST-SW-NULL = HIGH-VALUES
              MOVE PCO-MOD-COMNZ-INVST-SW-NULL
                TO (SF)-MOD-COMNZ-INVST-SW-NULL
           ELSE
              MOVE PCO-MOD-COMNZ-INVST-SW
                TO (SF)-MOD-COMNZ-INVST-SW
           END-IF
           IF PCO-DT-RIAT-RIASS-RSH-NULL = HIGH-VALUES
              MOVE PCO-DT-RIAT-RIASS-RSH-NULL
                TO (SF)-DT-RIAT-RIASS-RSH-NULL
           ELSE
              MOVE PCO-DT-RIAT-RIASS-RSH
                TO (SF)-DT-RIAT-RIASS-RSH
           END-IF
           IF PCO-DT-RIAT-RIASS-COMM-NULL = HIGH-VALUES
              MOVE PCO-DT-RIAT-RIASS-COMM-NULL
                TO (SF)-DT-RIAT-RIASS-COMM-NULL
           ELSE
              MOVE PCO-DT-RIAT-RIASS-COMM
                TO (SF)-DT-RIAT-RIASS-COMM
           END-IF
           IF PCO-GG-INTR-RIT-PAG-NULL = HIGH-VALUES
              MOVE PCO-GG-INTR-RIT-PAG-NULL
                TO (SF)-GG-INTR-RIT-PAG-NULL
           ELSE
              MOVE PCO-GG-INTR-RIT-PAG
                TO (SF)-GG-INTR-RIT-PAG
           END-IF
           IF PCO-DT-ULT-RINN-TAC-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-RINN-TAC-NULL
                TO (SF)-DT-ULT-RINN-TAC-NULL
           ELSE
              MOVE PCO-DT-ULT-RINN-TAC
                TO (SF)-DT-ULT-RINN-TAC
           END-IF
ITRAT      MOVE PCO-DESC-COMP-VCHAR
ITRAT        TO (SF)-DESC-COMP-VCHAR
           IF PCO-DT-ULT-EC-TCM-IND-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-EC-TCM-IND-NULL
                TO (SF)-DT-ULT-EC-TCM-IND-NULL
           ELSE
              MOVE PCO-DT-ULT-EC-TCM-IND
                TO (SF)-DT-ULT-EC-TCM-IND
           END-IF
           IF PCO-DT-ULT-EC-TCM-COLL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-EC-TCM-COLL-NULL
                TO (SF)-DT-ULT-EC-TCM-COLL-NULL
           ELSE
              MOVE PCO-DT-ULT-EC-TCM-COLL
                TO (SF)-DT-ULT-EC-TCM-COLL
           END-IF
           IF PCO-DT-ULT-EC-MRM-IND-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-EC-MRM-IND-NULL
                TO (SF)-DT-ULT-EC-MRM-IND-NULL
           ELSE
              MOVE PCO-DT-ULT-EC-MRM-IND
                TO (SF)-DT-ULT-EC-MRM-IND
           END-IF
           IF PCO-DT-ULT-EC-MRM-COLL-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-EC-MRM-COLL-NULL
                TO (SF)-DT-ULT-EC-MRM-COLL-NULL
           ELSE
              MOVE PCO-DT-ULT-EC-MRM-COLL
                TO (SF)-DT-ULT-EC-MRM-COLL
           END-IF
           IF PCO-COD-COMP-LDAP-NULL = HIGH-VALUES
              MOVE PCO-COD-COMP-LDAP-NULL
                TO (SF)-COD-COMP-LDAP-NULL
           ELSE
              MOVE PCO-COD-COMP-LDAP
                TO (SF)-COD-COMP-LDAP
           END-IF
           IF PCO-PC-RID-IMP-1382011-NULL = HIGH-VALUES
              MOVE PCO-PC-RID-IMP-1382011-NULL
                TO (SF)-PC-RID-IMP-1382011-NULL
           ELSE
              MOVE PCO-PC-RID-IMP-1382011
                TO (SF)-PC-RID-IMP-1382011
           END-IF
           IF PCO-PC-RID-IMP-662014-NULL = HIGH-VALUES
              MOVE PCO-PC-RID-IMP-662014-NULL
                TO (SF)-PC-RID-IMP-662014-NULL
           ELSE
              MOVE PCO-PC-RID-IMP-662014
                TO (SF)-PC-RID-IMP-662014
           END-IF
           IF PCO-SOGL-AML-PRE-UNI-NULL = HIGH-VALUES
              MOVE PCO-SOGL-AML-PRE-UNI-NULL
                TO (SF)-SOGL-AML-PRE-UNI-NULL
           ELSE
              MOVE PCO-SOGL-AML-PRE-UNI
                TO (SF)-SOGL-AML-PRE-UNI
           END-IF
           IF PCO-SOGL-AML-PRE-PER-NULL = HIGH-VALUES
              MOVE PCO-SOGL-AML-PRE-PER-NULL
                TO (SF)-SOGL-AML-PRE-PER-NULL
           ELSE
              MOVE PCO-SOGL-AML-PRE-PER
                TO (SF)-SOGL-AML-PRE-PER
           END-IF
           IF PCO-COD-SOGG-FTZ-ASSTO-NULL = HIGH-VALUES
              MOVE PCO-COD-SOGG-FTZ-ASSTO-NULL
                TO (SF)-COD-SOGG-FTZ-ASSTO-NULL
           ELSE
              MOVE PCO-COD-SOGG-FTZ-ASSTO
                TO (SF)-COD-SOGG-FTZ-ASSTO
           END-IF
           IF PCO-DT-ULT-ELAB-REDPRO-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ELAB-REDPRO-NULL
                TO (SF)-DT-ULT-ELAB-REDPRO-NULL
           ELSE
              MOVE PCO-DT-ULT-ELAB-REDPRO
                TO (SF)-DT-ULT-ELAB-REDPRO
           END-IF
           IF PCO-DT-ULT-ELAB-TAKE-P-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ELAB-TAKE-P-NULL
                TO (SF)-DT-ULT-ELAB-TAKE-P-NULL
           ELSE
              MOVE PCO-DT-ULT-ELAB-TAKE-P
                TO (SF)-DT-ULT-ELAB-TAKE-P
           END-IF
           IF PCO-DT-ULT-ELAB-PASPAS-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ELAB-PASPAS-NULL
                TO (SF)-DT-ULT-ELAB-PASPAS-NULL
           ELSE
              MOVE PCO-DT-ULT-ELAB-PASPAS
                TO (SF)-DT-ULT-ELAB-PASPAS
           END-IF
           IF PCO-SOGL-AML-PRE-SAV-R-NULL = HIGH-VALUES
              MOVE PCO-SOGL-AML-PRE-SAV-R-NULL
                TO (SF)-SOGL-AML-PRE-SAV-R-NULL
           ELSE
              MOVE PCO-SOGL-AML-PRE-SAV-R
                TO (SF)-SOGL-AML-PRE-SAV-R
           END-IF
           IF PCO-DT-ULT-ESTR-DEC-CO-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ESTR-DEC-CO-NULL
                TO (SF)-DT-ULT-ESTR-DEC-CO-NULL
           ELSE
              MOVE PCO-DT-ULT-ESTR-DEC-CO
                TO (SF)-DT-ULT-ESTR-DEC-CO
           END-IF
           IF PCO-DT-ULT-ELAB-COS-AT-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ELAB-COS-AT-NULL
                TO (SF)-DT-ULT-ELAB-COS-AT-NULL
           ELSE
              MOVE PCO-DT-ULT-ELAB-COS-AT
                TO (SF)-DT-ULT-ELAB-COS-AT
           END-IF
           IF PCO-FRQ-COSTI-ATT-NULL = HIGH-VALUES
              MOVE PCO-FRQ-COSTI-ATT-NULL
                TO (SF)-FRQ-COSTI-ATT-NULL
           ELSE
              MOVE PCO-FRQ-COSTI-ATT
                TO (SF)-FRQ-COSTI-ATT
           END-IF
           IF PCO-DT-ULT-ELAB-COS-ST-NULL = HIGH-VALUES
              MOVE PCO-DT-ULT-ELAB-COS-ST-NULL
                TO (SF)-DT-ULT-ELAB-COS-ST-NULL
           ELSE
              MOVE PCO-DT-ULT-ELAB-COS-ST
                TO (SF)-DT-ULT-ELAB-COS-ST
           END-IF
           IF PCO-FRQ-COSTI-STORNATI-NULL = HIGH-VALUES
              MOVE PCO-FRQ-COSTI-STORNATI-NULL
                TO (SF)-FRQ-COSTI-STORNATI-NULL
           ELSE
              MOVE PCO-FRQ-COSTI-STORNATI
                TO (SF)-FRQ-COSTI-STORNATI
           END-IF
           IF PCO-DT-ESTR-ASS-MIN70A-NULL = HIGH-VALUES
              MOVE PCO-DT-ESTR-ASS-MIN70A-NULL
                TO (SF)-DT-ESTR-ASS-MIN70A-NULL
           ELSE
              MOVE PCO-DT-ESTR-ASS-MIN70A
                TO (SF)-DT-ESTR-ASS-MIN70A
           END-IF
           IF PCO-DT-ESTR-ASS-MAG70A-NULL = HIGH-VALUES
              MOVE PCO-DT-ESTR-ASS-MAG70A-NULL
                TO (SF)-DT-ESTR-ASS-MAG70A-NULL
           ELSE
              MOVE PCO-DT-ESTR-ASS-MAG70A
                TO (SF)-DT-ESTR-ASS-MAG70A
           END-IF.
       VALORIZZA-OUTPUT-PCO-EX.
           EXIT.

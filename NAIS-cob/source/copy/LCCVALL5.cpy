
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVALL5
      *   ULTIMO AGG. 31 MAR 2020
      *------------------------------------------------------------

       VAL-DCLGEN-ALL.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-ALL) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-ALL)
              TO ALL-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-ALL)
              TO ALL-ID-MOVI-CHIU
           END-IF
           MOVE (SF)-DT-INI-VLDT(IX-TAB-ALL)
              TO ALL-DT-INI-VLDT
           IF (SF)-DT-END-VLDT-NULL(IX-TAB-ALL) = HIGH-VALUES
              MOVE (SF)-DT-END-VLDT-NULL(IX-TAB-ALL)
              TO ALL-DT-END-VLDT-NULL
           ELSE
             IF (SF)-DT-END-VLDT(IX-TAB-ALL) = ZERO
                MOVE HIGH-VALUES
                TO ALL-DT-END-VLDT-NULL
             ELSE
              MOVE (SF)-DT-END-VLDT(IX-TAB-ALL)
              TO ALL-DT-END-VLDT
             END-IF
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-ALL) NOT NUMERIC
              MOVE 0 TO ALL-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-ALL)
              TO ALL-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-ALL) NOT NUMERIC
              MOVE 0 TO ALL-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-ALL)
              TO ALL-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-ALL)
              TO ALL-COD-COMP-ANIA
           IF (SF)-COD-FND-NULL(IX-TAB-ALL) = HIGH-VALUES
              MOVE (SF)-COD-FND-NULL(IX-TAB-ALL)
              TO ALL-COD-FND-NULL
           ELSE
              MOVE (SF)-COD-FND(IX-TAB-ALL)
              TO ALL-COD-FND
           END-IF
           IF (SF)-COD-TARI-NULL(IX-TAB-ALL) = HIGH-VALUES
              MOVE (SF)-COD-TARI-NULL(IX-TAB-ALL)
              TO ALL-COD-TARI-NULL
           ELSE
              MOVE (SF)-COD-TARI(IX-TAB-ALL)
              TO ALL-COD-TARI
           END-IF
           IF (SF)-TP-APPLZ-AST-NULL(IX-TAB-ALL) = HIGH-VALUES
              MOVE (SF)-TP-APPLZ-AST-NULL(IX-TAB-ALL)
              TO ALL-TP-APPLZ-AST-NULL
           ELSE
              MOVE (SF)-TP-APPLZ-AST(IX-TAB-ALL)
              TO ALL-TP-APPLZ-AST
           END-IF
           IF (SF)-PC-RIP-AST-NULL(IX-TAB-ALL) = HIGH-VALUES
              MOVE (SF)-PC-RIP-AST-NULL(IX-TAB-ALL)
              TO ALL-PC-RIP-AST-NULL
           ELSE
              MOVE (SF)-PC-RIP-AST(IX-TAB-ALL)
              TO ALL-PC-RIP-AST
           END-IF
           MOVE (SF)-TP-FND(IX-TAB-ALL)
              TO ALL-TP-FND
           IF (SF)-DS-RIGA(IX-TAB-ALL) NOT NUMERIC
              MOVE 0 TO ALL-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-ALL)
              TO ALL-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-ALL)
              TO ALL-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-ALL) NOT NUMERIC
              MOVE 0 TO ALL-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-ALL)
              TO ALL-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-ALL) NOT NUMERIC
              MOVE 0 TO ALL-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-ALL)
              TO ALL-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-ALL) NOT NUMERIC
              MOVE 0 TO ALL-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-ALL)
              TO ALL-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-ALL)
              TO ALL-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-ALL)
              TO ALL-DS-STATO-ELAB
           IF (SF)-PERIODO-NULL(IX-TAB-ALL) = HIGH-VALUES
              MOVE (SF)-PERIODO-NULL(IX-TAB-ALL)
              TO ALL-PERIODO-NULL
           ELSE
              MOVE (SF)-PERIODO(IX-TAB-ALL)
              TO ALL-PERIODO
           END-IF
           IF (SF)-TP-RIBIL-FND-NULL(IX-TAB-ALL) = HIGH-VALUES
              MOVE (SF)-TP-RIBIL-FND-NULL(IX-TAB-ALL)
              TO ALL-TP-RIBIL-FND-NULL
           ELSE
              MOVE (SF)-TP-RIBIL-FND(IX-TAB-ALL)
              TO ALL-TP-RIBIL-FND
           END-IF.
       VAL-DCLGEN-ALL-EX.
           EXIT.

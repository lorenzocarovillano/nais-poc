
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVB033
      *   ULTIMO AGG. 12 SET 2014
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-B03.
           MOVE B03-ID-BILA-TRCH-ESTR
             TO (SF)-ID-PTF
           MOVE B03-ID-BILA-TRCH-ESTR
             TO (SF)-ID-BILA-TRCH-ESTR
           MOVE B03-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE B03-ID-RICH-ESTRAZ-MAS
             TO (SF)-ID-RICH-ESTRAZ-MAS
           IF B03-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
              MOVE B03-ID-RICH-ESTRAZ-AGG-NULL
                TO (SF)-ID-RICH-ESTRAZ-AGG-NULL
           ELSE
              MOVE B03-ID-RICH-ESTRAZ-AGG
                TO (SF)-ID-RICH-ESTRAZ-AGG
           END-IF
           IF B03-FL-SIMULAZIONE-NULL = HIGH-VALUES
              MOVE B03-FL-SIMULAZIONE-NULL
                TO (SF)-FL-SIMULAZIONE-NULL
           ELSE
              MOVE B03-FL-SIMULAZIONE
                TO (SF)-FL-SIMULAZIONE
           END-IF
           MOVE B03-DT-RIS
             TO (SF)-DT-RIS
           MOVE B03-DT-PRODUZIONE
             TO (SF)-DT-PRODUZIONE
           MOVE B03-ID-POLI
             TO (SF)-ID-POLI
           MOVE B03-ID-ADES
             TO (SF)-ID-ADES
           MOVE B03-ID-GAR
             TO (SF)-ID-GAR
           MOVE B03-ID-TRCH-DI-GAR
             TO (SF)-ID-TRCH-DI-GAR
           MOVE B03-TP-FRM-ASSVA
             TO (SF)-TP-FRM-ASSVA
           MOVE B03-TP-RAMO-BILA
             TO (SF)-TP-RAMO-BILA
           MOVE B03-TP-CALC-RIS
             TO (SF)-TP-CALC-RIS
           MOVE B03-COD-RAMO
             TO (SF)-COD-RAMO
           MOVE B03-COD-TARI
             TO (SF)-COD-TARI
           IF B03-DT-INI-VAL-TAR-NULL = HIGH-VALUES
              MOVE B03-DT-INI-VAL-TAR-NULL
                TO (SF)-DT-INI-VAL-TAR-NULL
           ELSE
              MOVE B03-DT-INI-VAL-TAR
                TO (SF)-DT-INI-VAL-TAR
           END-IF
           IF B03-COD-PROD-NULL = HIGH-VALUES
              MOVE B03-COD-PROD-NULL
                TO (SF)-COD-PROD-NULL
           ELSE
              MOVE B03-COD-PROD
                TO (SF)-COD-PROD
           END-IF
           MOVE B03-DT-INI-VLDT-PROD
             TO (SF)-DT-INI-VLDT-PROD
           IF B03-COD-TARI-ORGN-NULL = HIGH-VALUES
              MOVE B03-COD-TARI-ORGN-NULL
                TO (SF)-COD-TARI-ORGN-NULL
           ELSE
              MOVE B03-COD-TARI-ORGN
                TO (SF)-COD-TARI-ORGN
           END-IF
           IF B03-MIN-GARTO-T-NULL = HIGH-VALUES
              MOVE B03-MIN-GARTO-T-NULL
                TO (SF)-MIN-GARTO-T-NULL
           ELSE
              MOVE B03-MIN-GARTO-T
                TO (SF)-MIN-GARTO-T
           END-IF
           IF B03-TP-TARI-NULL = HIGH-VALUES
              MOVE B03-TP-TARI-NULL
                TO (SF)-TP-TARI-NULL
           ELSE
              MOVE B03-TP-TARI
                TO (SF)-TP-TARI
           END-IF
           IF B03-TP-PRE-NULL = HIGH-VALUES
              MOVE B03-TP-PRE-NULL
                TO (SF)-TP-PRE-NULL
           ELSE
              MOVE B03-TP-PRE
                TO (SF)-TP-PRE
           END-IF
           IF B03-TP-ADEG-PRE-NULL = HIGH-VALUES
              MOVE B03-TP-ADEG-PRE-NULL
                TO (SF)-TP-ADEG-PRE-NULL
           ELSE
              MOVE B03-TP-ADEG-PRE
                TO (SF)-TP-ADEG-PRE
           END-IF
           IF B03-TP-RIVAL-NULL = HIGH-VALUES
              MOVE B03-TP-RIVAL-NULL
                TO (SF)-TP-RIVAL-NULL
           ELSE
              MOVE B03-TP-RIVAL
                TO (SF)-TP-RIVAL
           END-IF
           IF B03-FL-DA-TRASF-NULL = HIGH-VALUES
              MOVE B03-FL-DA-TRASF-NULL
                TO (SF)-FL-DA-TRASF-NULL
           ELSE
              MOVE B03-FL-DA-TRASF
                TO (SF)-FL-DA-TRASF
           END-IF
           IF B03-FL-CAR-CONT-NULL = HIGH-VALUES
              MOVE B03-FL-CAR-CONT-NULL
                TO (SF)-FL-CAR-CONT-NULL
           ELSE
              MOVE B03-FL-CAR-CONT
                TO (SF)-FL-CAR-CONT
           END-IF
           IF B03-FL-PRE-DA-RIS-NULL = HIGH-VALUES
              MOVE B03-FL-PRE-DA-RIS-NULL
                TO (SF)-FL-PRE-DA-RIS-NULL
           ELSE
              MOVE B03-FL-PRE-DA-RIS
                TO (SF)-FL-PRE-DA-RIS
           END-IF
           IF B03-FL-PRE-AGG-NULL = HIGH-VALUES
              MOVE B03-FL-PRE-AGG-NULL
                TO (SF)-FL-PRE-AGG-NULL
           ELSE
              MOVE B03-FL-PRE-AGG
                TO (SF)-FL-PRE-AGG
           END-IF
           IF B03-TP-TRCH-NULL = HIGH-VALUES
              MOVE B03-TP-TRCH-NULL
                TO (SF)-TP-TRCH-NULL
           ELSE
              MOVE B03-TP-TRCH
                TO (SF)-TP-TRCH
           END-IF
           IF B03-TP-TST-NULL = HIGH-VALUES
              MOVE B03-TP-TST-NULL
                TO (SF)-TP-TST-NULL
           ELSE
              MOVE B03-TP-TST
                TO (SF)-TP-TST
           END-IF
           IF B03-COD-CONV-NULL = HIGH-VALUES
              MOVE B03-COD-CONV-NULL
                TO (SF)-COD-CONV-NULL
           ELSE
              MOVE B03-COD-CONV
                TO (SF)-COD-CONV
           END-IF
           MOVE B03-DT-DECOR-POLI
             TO (SF)-DT-DECOR-POLI
           IF B03-DT-DECOR-ADES-NULL = HIGH-VALUES
              MOVE B03-DT-DECOR-ADES-NULL
                TO (SF)-DT-DECOR-ADES-NULL
           ELSE
              MOVE B03-DT-DECOR-ADES
                TO (SF)-DT-DECOR-ADES
           END-IF
           MOVE B03-DT-DECOR-TRCH
             TO (SF)-DT-DECOR-TRCH
           MOVE B03-DT-EMIS-POLI
             TO (SF)-DT-EMIS-POLI
           IF B03-DT-EMIS-TRCH-NULL = HIGH-VALUES
              MOVE B03-DT-EMIS-TRCH-NULL
                TO (SF)-DT-EMIS-TRCH-NULL
           ELSE
              MOVE B03-DT-EMIS-TRCH
                TO (SF)-DT-EMIS-TRCH
           END-IF
           IF B03-DT-SCAD-TRCH-NULL = HIGH-VALUES
              MOVE B03-DT-SCAD-TRCH-NULL
                TO (SF)-DT-SCAD-TRCH-NULL
           ELSE
              MOVE B03-DT-SCAD-TRCH
                TO (SF)-DT-SCAD-TRCH
           END-IF
           IF B03-DT-SCAD-INTMD-NULL = HIGH-VALUES
              MOVE B03-DT-SCAD-INTMD-NULL
                TO (SF)-DT-SCAD-INTMD-NULL
           ELSE
              MOVE B03-DT-SCAD-INTMD
                TO (SF)-DT-SCAD-INTMD
           END-IF
           IF B03-DT-SCAD-PAG-PRE-NULL = HIGH-VALUES
              MOVE B03-DT-SCAD-PAG-PRE-NULL
                TO (SF)-DT-SCAD-PAG-PRE-NULL
           ELSE
              MOVE B03-DT-SCAD-PAG-PRE
                TO (SF)-DT-SCAD-PAG-PRE
           END-IF
           IF B03-DT-ULT-PRE-PAG-NULL = HIGH-VALUES
              MOVE B03-DT-ULT-PRE-PAG-NULL
                TO (SF)-DT-ULT-PRE-PAG-NULL
           ELSE
              MOVE B03-DT-ULT-PRE-PAG
                TO (SF)-DT-ULT-PRE-PAG
           END-IF
           IF B03-DT-NASC-1O-ASSTO-NULL = HIGH-VALUES
              MOVE B03-DT-NASC-1O-ASSTO-NULL
                TO (SF)-DT-NASC-1O-ASSTO-NULL
           ELSE
              MOVE B03-DT-NASC-1O-ASSTO
                TO (SF)-DT-NASC-1O-ASSTO
           END-IF
           IF B03-SEX-1O-ASSTO-NULL = HIGH-VALUES
              MOVE B03-SEX-1O-ASSTO-NULL
                TO (SF)-SEX-1O-ASSTO-NULL
           ELSE
              MOVE B03-SEX-1O-ASSTO
                TO (SF)-SEX-1O-ASSTO
           END-IF
           IF B03-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
              MOVE B03-ETA-AA-1O-ASSTO-NULL
                TO (SF)-ETA-AA-1O-ASSTO-NULL
           ELSE
              MOVE B03-ETA-AA-1O-ASSTO
                TO (SF)-ETA-AA-1O-ASSTO
           END-IF
           IF B03-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
              MOVE B03-ETA-MM-1O-ASSTO-NULL
                TO (SF)-ETA-MM-1O-ASSTO-NULL
           ELSE
              MOVE B03-ETA-MM-1O-ASSTO
                TO (SF)-ETA-MM-1O-ASSTO
           END-IF
           IF B03-ETA-RAGGN-DT-CALC-NULL = HIGH-VALUES
              MOVE B03-ETA-RAGGN-DT-CALC-NULL
                TO (SF)-ETA-RAGGN-DT-CALC-NULL
           ELSE
              MOVE B03-ETA-RAGGN-DT-CALC
                TO (SF)-ETA-RAGGN-DT-CALC
           END-IF
           IF B03-DUR-AA-NULL = HIGH-VALUES
              MOVE B03-DUR-AA-NULL
                TO (SF)-DUR-AA-NULL
           ELSE
              MOVE B03-DUR-AA
                TO (SF)-DUR-AA
           END-IF
           IF B03-DUR-MM-NULL = HIGH-VALUES
              MOVE B03-DUR-MM-NULL
                TO (SF)-DUR-MM-NULL
           ELSE
              MOVE B03-DUR-MM
                TO (SF)-DUR-MM
           END-IF
           IF B03-DUR-GG-NULL = HIGH-VALUES
              MOVE B03-DUR-GG-NULL
                TO (SF)-DUR-GG-NULL
           ELSE
              MOVE B03-DUR-GG
                TO (SF)-DUR-GG
           END-IF
           IF B03-DUR-1O-PER-AA-NULL = HIGH-VALUES
              MOVE B03-DUR-1O-PER-AA-NULL
                TO (SF)-DUR-1O-PER-AA-NULL
           ELSE
              MOVE B03-DUR-1O-PER-AA
                TO (SF)-DUR-1O-PER-AA
           END-IF
           IF B03-DUR-1O-PER-MM-NULL = HIGH-VALUES
              MOVE B03-DUR-1O-PER-MM-NULL
                TO (SF)-DUR-1O-PER-MM-NULL
           ELSE
              MOVE B03-DUR-1O-PER-MM
                TO (SF)-DUR-1O-PER-MM
           END-IF
           IF B03-DUR-1O-PER-GG-NULL = HIGH-VALUES
              MOVE B03-DUR-1O-PER-GG-NULL
                TO (SF)-DUR-1O-PER-GG-NULL
           ELSE
              MOVE B03-DUR-1O-PER-GG
                TO (SF)-DUR-1O-PER-GG
           END-IF
           IF B03-ANTIDUR-RICOR-PREC-NULL = HIGH-VALUES
              MOVE B03-ANTIDUR-RICOR-PREC-NULL
                TO (SF)-ANTIDUR-RICOR-PREC-NULL
           ELSE
              MOVE B03-ANTIDUR-RICOR-PREC
                TO (SF)-ANTIDUR-RICOR-PREC
           END-IF
           IF B03-ANTIDUR-DT-CALC-NULL = HIGH-VALUES
              MOVE B03-ANTIDUR-DT-CALC-NULL
                TO (SF)-ANTIDUR-DT-CALC-NULL
           ELSE
              MOVE B03-ANTIDUR-DT-CALC
                TO (SF)-ANTIDUR-DT-CALC
           END-IF
           IF B03-DUR-RES-DT-CALC-NULL = HIGH-VALUES
              MOVE B03-DUR-RES-DT-CALC-NULL
                TO (SF)-DUR-RES-DT-CALC-NULL
           ELSE
              MOVE B03-DUR-RES-DT-CALC
                TO (SF)-DUR-RES-DT-CALC
           END-IF
           MOVE B03-TP-STAT-BUS-POLI
             TO (SF)-TP-STAT-BUS-POLI
           MOVE B03-TP-CAUS-POLI
             TO (SF)-TP-CAUS-POLI
           MOVE B03-TP-STAT-BUS-ADES
             TO (SF)-TP-STAT-BUS-ADES
           MOVE B03-TP-CAUS-ADES
             TO (SF)-TP-CAUS-ADES
           MOVE B03-TP-STAT-BUS-TRCH
             TO (SF)-TP-STAT-BUS-TRCH
           MOVE B03-TP-CAUS-TRCH
             TO (SF)-TP-CAUS-TRCH
           IF B03-DT-EFF-CAMB-STAT-NULL = HIGH-VALUES
              MOVE B03-DT-EFF-CAMB-STAT-NULL
                TO (SF)-DT-EFF-CAMB-STAT-NULL
           ELSE
              MOVE B03-DT-EFF-CAMB-STAT
                TO (SF)-DT-EFF-CAMB-STAT
           END-IF
           IF B03-DT-EMIS-CAMB-STAT-NULL = HIGH-VALUES
              MOVE B03-DT-EMIS-CAMB-STAT-NULL
                TO (SF)-DT-EMIS-CAMB-STAT-NULL
           ELSE
              MOVE B03-DT-EMIS-CAMB-STAT
                TO (SF)-DT-EMIS-CAMB-STAT
           END-IF
           IF B03-DT-EFF-STAB-NULL = HIGH-VALUES
              MOVE B03-DT-EFF-STAB-NULL
                TO (SF)-DT-EFF-STAB-NULL
           ELSE
              MOVE B03-DT-EFF-STAB
                TO (SF)-DT-EFF-STAB
           END-IF
           IF B03-CPT-DT-STAB-NULL = HIGH-VALUES
              MOVE B03-CPT-DT-STAB-NULL
                TO (SF)-CPT-DT-STAB-NULL
           ELSE
              MOVE B03-CPT-DT-STAB
                TO (SF)-CPT-DT-STAB
           END-IF
           IF B03-DT-EFF-RIDZ-NULL = HIGH-VALUES
              MOVE B03-DT-EFF-RIDZ-NULL
                TO (SF)-DT-EFF-RIDZ-NULL
           ELSE
              MOVE B03-DT-EFF-RIDZ
                TO (SF)-DT-EFF-RIDZ
           END-IF
           IF B03-DT-EMIS-RIDZ-NULL = HIGH-VALUES
              MOVE B03-DT-EMIS-RIDZ-NULL
                TO (SF)-DT-EMIS-RIDZ-NULL
           ELSE
              MOVE B03-DT-EMIS-RIDZ
                TO (SF)-DT-EMIS-RIDZ
           END-IF
           IF B03-CPT-DT-RIDZ-NULL = HIGH-VALUES
              MOVE B03-CPT-DT-RIDZ-NULL
                TO (SF)-CPT-DT-RIDZ-NULL
           ELSE
              MOVE B03-CPT-DT-RIDZ
                TO (SF)-CPT-DT-RIDZ
           END-IF
           IF B03-FRAZ-NULL = HIGH-VALUES
              MOVE B03-FRAZ-NULL
                TO (SF)-FRAZ-NULL
           ELSE
              MOVE B03-FRAZ
                TO (SF)-FRAZ
           END-IF
           IF B03-DUR-PAG-PRE-NULL = HIGH-VALUES
              MOVE B03-DUR-PAG-PRE-NULL
                TO (SF)-DUR-PAG-PRE-NULL
           ELSE
              MOVE B03-DUR-PAG-PRE
                TO (SF)-DUR-PAG-PRE
           END-IF
           IF B03-NUM-PRE-PATT-NULL = HIGH-VALUES
              MOVE B03-NUM-PRE-PATT-NULL
                TO (SF)-NUM-PRE-PATT-NULL
           ELSE
              MOVE B03-NUM-PRE-PATT
                TO (SF)-NUM-PRE-PATT
           END-IF
           IF B03-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
              MOVE B03-FRAZ-INI-EROG-REN-NULL
                TO (SF)-FRAZ-INI-EROG-REN-NULL
           ELSE
              MOVE B03-FRAZ-INI-EROG-REN
                TO (SF)-FRAZ-INI-EROG-REN
           END-IF
           IF B03-AA-REN-CER-NULL = HIGH-VALUES
              MOVE B03-AA-REN-CER-NULL
                TO (SF)-AA-REN-CER-NULL
           ELSE
              MOVE B03-AA-REN-CER
                TO (SF)-AA-REN-CER
           END-IF
           IF B03-RAT-REN-NULL = HIGH-VALUES
              MOVE B03-RAT-REN-NULL
                TO (SF)-RAT-REN-NULL
           ELSE
              MOVE B03-RAT-REN
                TO (SF)-RAT-REN
           END-IF
           IF B03-COD-DIV-NULL = HIGH-VALUES
              MOVE B03-COD-DIV-NULL
                TO (SF)-COD-DIV-NULL
           ELSE
              MOVE B03-COD-DIV
                TO (SF)-COD-DIV
           END-IF
           IF B03-RISCPAR-NULL = HIGH-VALUES
              MOVE B03-RISCPAR-NULL
                TO (SF)-RISCPAR-NULL
           ELSE
              MOVE B03-RISCPAR
                TO (SF)-RISCPAR
           END-IF
           IF B03-CUM-RISCPAR-NULL = HIGH-VALUES
              MOVE B03-CUM-RISCPAR-NULL
                TO (SF)-CUM-RISCPAR-NULL
           ELSE
              MOVE B03-CUM-RISCPAR
                TO (SF)-CUM-RISCPAR
           END-IF
           IF B03-ULT-RM-NULL = HIGH-VALUES
              MOVE B03-ULT-RM-NULL
                TO (SF)-ULT-RM-NULL
           ELSE
              MOVE B03-ULT-RM
                TO (SF)-ULT-RM
           END-IF
           IF B03-TS-RENDTO-T-NULL = HIGH-VALUES
              MOVE B03-TS-RENDTO-T-NULL
                TO (SF)-TS-RENDTO-T-NULL
           ELSE
              MOVE B03-TS-RENDTO-T
                TO (SF)-TS-RENDTO-T
           END-IF
           IF B03-ALQ-RETR-T-NULL = HIGH-VALUES
              MOVE B03-ALQ-RETR-T-NULL
                TO (SF)-ALQ-RETR-T-NULL
           ELSE
              MOVE B03-ALQ-RETR-T
                TO (SF)-ALQ-RETR-T
           END-IF
           IF B03-MIN-TRNUT-T-NULL = HIGH-VALUES
              MOVE B03-MIN-TRNUT-T-NULL
                TO (SF)-MIN-TRNUT-T-NULL
           ELSE
              MOVE B03-MIN-TRNUT-T
                TO (SF)-MIN-TRNUT-T
           END-IF
           IF B03-TS-NET-T-NULL = HIGH-VALUES
              MOVE B03-TS-NET-T-NULL
                TO (SF)-TS-NET-T-NULL
           ELSE
              MOVE B03-TS-NET-T
                TO (SF)-TS-NET-T
           END-IF
           IF B03-DT-ULT-RIVAL-NULL = HIGH-VALUES
              MOVE B03-DT-ULT-RIVAL-NULL
                TO (SF)-DT-ULT-RIVAL-NULL
           ELSE
              MOVE B03-DT-ULT-RIVAL
                TO (SF)-DT-ULT-RIVAL
           END-IF
           IF B03-PRSTZ-INI-NULL = HIGH-VALUES
              MOVE B03-PRSTZ-INI-NULL
                TO (SF)-PRSTZ-INI-NULL
           ELSE
              MOVE B03-PRSTZ-INI
                TO (SF)-PRSTZ-INI
           END-IF
           IF B03-PRSTZ-AGG-INI-NULL = HIGH-VALUES
              MOVE B03-PRSTZ-AGG-INI-NULL
                TO (SF)-PRSTZ-AGG-INI-NULL
           ELSE
              MOVE B03-PRSTZ-AGG-INI
                TO (SF)-PRSTZ-AGG-INI
           END-IF
           IF B03-PRSTZ-AGG-ULT-NULL = HIGH-VALUES
              MOVE B03-PRSTZ-AGG-ULT-NULL
                TO (SF)-PRSTZ-AGG-ULT-NULL
           ELSE
              MOVE B03-PRSTZ-AGG-ULT
                TO (SF)-PRSTZ-AGG-ULT
           END-IF
           IF B03-RAPPEL-NULL = HIGH-VALUES
              MOVE B03-RAPPEL-NULL
                TO (SF)-RAPPEL-NULL
           ELSE
              MOVE B03-RAPPEL
                TO (SF)-RAPPEL
           END-IF
           IF B03-PRE-PATTUITO-INI-NULL = HIGH-VALUES
              MOVE B03-PRE-PATTUITO-INI-NULL
                TO (SF)-PRE-PATTUITO-INI-NULL
           ELSE
              MOVE B03-PRE-PATTUITO-INI
                TO (SF)-PRE-PATTUITO-INI
           END-IF
           IF B03-PRE-DOV-INI-NULL = HIGH-VALUES
              MOVE B03-PRE-DOV-INI-NULL
                TO (SF)-PRE-DOV-INI-NULL
           ELSE
              MOVE B03-PRE-DOV-INI
                TO (SF)-PRE-DOV-INI
           END-IF
           IF B03-PRE-DOV-RIVTO-T-NULL = HIGH-VALUES
              MOVE B03-PRE-DOV-RIVTO-T-NULL
                TO (SF)-PRE-DOV-RIVTO-T-NULL
           ELSE
              MOVE B03-PRE-DOV-RIVTO-T
                TO (SF)-PRE-DOV-RIVTO-T
           END-IF
           IF B03-PRE-ANNUALIZ-RICOR-NULL = HIGH-VALUES
              MOVE B03-PRE-ANNUALIZ-RICOR-NULL
                TO (SF)-PRE-ANNUALIZ-RICOR-NULL
           ELSE
              MOVE B03-PRE-ANNUALIZ-RICOR
                TO (SF)-PRE-ANNUALIZ-RICOR
           END-IF
           IF B03-PRE-CONT-NULL = HIGH-VALUES
              MOVE B03-PRE-CONT-NULL
                TO (SF)-PRE-CONT-NULL
           ELSE
              MOVE B03-PRE-CONT
                TO (SF)-PRE-CONT
           END-IF
           IF B03-PRE-PP-INI-NULL = HIGH-VALUES
              MOVE B03-PRE-PP-INI-NULL
                TO (SF)-PRE-PP-INI-NULL
           ELSE
              MOVE B03-PRE-PP-INI
                TO (SF)-PRE-PP-INI
           END-IF
           IF B03-RIS-PURA-T-NULL = HIGH-VALUES
              MOVE B03-RIS-PURA-T-NULL
                TO (SF)-RIS-PURA-T-NULL
           ELSE
              MOVE B03-RIS-PURA-T
                TO (SF)-RIS-PURA-T
           END-IF
           IF B03-PROV-ACQ-NULL = HIGH-VALUES
              MOVE B03-PROV-ACQ-NULL
                TO (SF)-PROV-ACQ-NULL
           ELSE
              MOVE B03-PROV-ACQ
                TO (SF)-PROV-ACQ
           END-IF
           IF B03-PROV-ACQ-RICOR-NULL = HIGH-VALUES
              MOVE B03-PROV-ACQ-RICOR-NULL
                TO (SF)-PROV-ACQ-RICOR-NULL
           ELSE
              MOVE B03-PROV-ACQ-RICOR
                TO (SF)-PROV-ACQ-RICOR
           END-IF
           IF B03-PROV-INC-NULL = HIGH-VALUES
              MOVE B03-PROV-INC-NULL
                TO (SF)-PROV-INC-NULL
           ELSE
              MOVE B03-PROV-INC
                TO (SF)-PROV-INC
           END-IF
           IF B03-CAR-ACQ-NON-SCON-NULL = HIGH-VALUES
              MOVE B03-CAR-ACQ-NON-SCON-NULL
                TO (SF)-CAR-ACQ-NON-SCON-NULL
           ELSE
              MOVE B03-CAR-ACQ-NON-SCON
                TO (SF)-CAR-ACQ-NON-SCON
           END-IF
           IF B03-OVER-COMM-NULL = HIGH-VALUES
              MOVE B03-OVER-COMM-NULL
                TO (SF)-OVER-COMM-NULL
           ELSE
              MOVE B03-OVER-COMM
                TO (SF)-OVER-COMM
           END-IF
           IF B03-CAR-ACQ-PRECONTATO-NULL = HIGH-VALUES
              MOVE B03-CAR-ACQ-PRECONTATO-NULL
                TO (SF)-CAR-ACQ-PRECONTATO-NULL
           ELSE
              MOVE B03-CAR-ACQ-PRECONTATO
                TO (SF)-CAR-ACQ-PRECONTATO
           END-IF
           IF B03-RIS-ACQ-T-NULL = HIGH-VALUES
              MOVE B03-RIS-ACQ-T-NULL
                TO (SF)-RIS-ACQ-T-NULL
           ELSE
              MOVE B03-RIS-ACQ-T
                TO (SF)-RIS-ACQ-T
           END-IF
           IF B03-RIS-ZIL-T-NULL = HIGH-VALUES
              MOVE B03-RIS-ZIL-T-NULL
                TO (SF)-RIS-ZIL-T-NULL
           ELSE
              MOVE B03-RIS-ZIL-T
                TO (SF)-RIS-ZIL-T
           END-IF
           IF B03-CAR-GEST-NON-SCON-NULL = HIGH-VALUES
              MOVE B03-CAR-GEST-NON-SCON-NULL
                TO (SF)-CAR-GEST-NON-SCON-NULL
           ELSE
              MOVE B03-CAR-GEST-NON-SCON
                TO (SF)-CAR-GEST-NON-SCON
           END-IF
           IF B03-CAR-GEST-NULL = HIGH-VALUES
              MOVE B03-CAR-GEST-NULL
                TO (SF)-CAR-GEST-NULL
           ELSE
              MOVE B03-CAR-GEST
                TO (SF)-CAR-GEST
           END-IF
           IF B03-RIS-SPE-T-NULL = HIGH-VALUES
              MOVE B03-RIS-SPE-T-NULL
                TO (SF)-RIS-SPE-T-NULL
           ELSE
              MOVE B03-RIS-SPE-T
                TO (SF)-RIS-SPE-T
           END-IF
           IF B03-CAR-INC-NON-SCON-NULL = HIGH-VALUES
              MOVE B03-CAR-INC-NON-SCON-NULL
                TO (SF)-CAR-INC-NON-SCON-NULL
           ELSE
              MOVE B03-CAR-INC-NON-SCON
                TO (SF)-CAR-INC-NON-SCON
           END-IF
           IF B03-CAR-INC-NULL = HIGH-VALUES
              MOVE B03-CAR-INC-NULL
                TO (SF)-CAR-INC-NULL
           ELSE
              MOVE B03-CAR-INC
                TO (SF)-CAR-INC
           END-IF
           IF B03-RIS-RISTORNI-CAP-NULL = HIGH-VALUES
              MOVE B03-RIS-RISTORNI-CAP-NULL
                TO (SF)-RIS-RISTORNI-CAP-NULL
           ELSE
              MOVE B03-RIS-RISTORNI-CAP
                TO (SF)-RIS-RISTORNI-CAP
           END-IF
           IF B03-INTR-TECN-NULL = HIGH-VALUES
              MOVE B03-INTR-TECN-NULL
                TO (SF)-INTR-TECN-NULL
           ELSE
              MOVE B03-INTR-TECN
                TO (SF)-INTR-TECN
           END-IF
           IF B03-CPT-RSH-MOR-NULL = HIGH-VALUES
              MOVE B03-CPT-RSH-MOR-NULL
                TO (SF)-CPT-RSH-MOR-NULL
           ELSE
              MOVE B03-CPT-RSH-MOR
                TO (SF)-CPT-RSH-MOR
           END-IF
           IF B03-C-SUBRSH-T-NULL = HIGH-VALUES
              MOVE B03-C-SUBRSH-T-NULL
                TO (SF)-C-SUBRSH-T-NULL
           ELSE
              MOVE B03-C-SUBRSH-T
                TO (SF)-C-SUBRSH-T
           END-IF
           IF B03-PRE-RSH-T-NULL = HIGH-VALUES
              MOVE B03-PRE-RSH-T-NULL
                TO (SF)-PRE-RSH-T-NULL
           ELSE
              MOVE B03-PRE-RSH-T
                TO (SF)-PRE-RSH-T
           END-IF
           IF B03-ALQ-MARG-RIS-NULL = HIGH-VALUES
              MOVE B03-ALQ-MARG-RIS-NULL
                TO (SF)-ALQ-MARG-RIS-NULL
           ELSE
              MOVE B03-ALQ-MARG-RIS
                TO (SF)-ALQ-MARG-RIS
           END-IF
           IF B03-ALQ-MARG-C-SUBRSH-NULL = HIGH-VALUES
              MOVE B03-ALQ-MARG-C-SUBRSH-NULL
                TO (SF)-ALQ-MARG-C-SUBRSH-NULL
           ELSE
              MOVE B03-ALQ-MARG-C-SUBRSH
                TO (SF)-ALQ-MARG-C-SUBRSH
           END-IF
           IF B03-TS-RENDTO-SPPR-NULL = HIGH-VALUES
              MOVE B03-TS-RENDTO-SPPR-NULL
                TO (SF)-TS-RENDTO-SPPR-NULL
           ELSE
              MOVE B03-TS-RENDTO-SPPR
                TO (SF)-TS-RENDTO-SPPR
           END-IF
           IF B03-TP-IAS-NULL = HIGH-VALUES
              MOVE B03-TP-IAS-NULL
                TO (SF)-TP-IAS-NULL
           ELSE
              MOVE B03-TP-IAS
                TO (SF)-TP-IAS
           END-IF
           IF B03-NS-QUO-NULL = HIGH-VALUES
              MOVE B03-NS-QUO-NULL
                TO (SF)-NS-QUO-NULL
           ELSE
              MOVE B03-NS-QUO
                TO (SF)-NS-QUO
           END-IF
           IF B03-TS-MEDIO-NULL = HIGH-VALUES
              MOVE B03-TS-MEDIO-NULL
                TO (SF)-TS-MEDIO-NULL
           ELSE
              MOVE B03-TS-MEDIO
                TO (SF)-TS-MEDIO
           END-IF
           IF B03-CPT-RIASTO-NULL = HIGH-VALUES
              MOVE B03-CPT-RIASTO-NULL
                TO (SF)-CPT-RIASTO-NULL
           ELSE
              MOVE B03-CPT-RIASTO
                TO (SF)-CPT-RIASTO
           END-IF
           IF B03-PRE-RIASTO-NULL = HIGH-VALUES
              MOVE B03-PRE-RIASTO-NULL
                TO (SF)-PRE-RIASTO-NULL
           ELSE
              MOVE B03-PRE-RIASTO
                TO (SF)-PRE-RIASTO
           END-IF
           IF B03-RIS-RIASTA-NULL = HIGH-VALUES
              MOVE B03-RIS-RIASTA-NULL
                TO (SF)-RIS-RIASTA-NULL
           ELSE
              MOVE B03-RIS-RIASTA
                TO (SF)-RIS-RIASTA
           END-IF
           IF B03-CPT-RIASTO-ECC-NULL = HIGH-VALUES
              MOVE B03-CPT-RIASTO-ECC-NULL
                TO (SF)-CPT-RIASTO-ECC-NULL
           ELSE
              MOVE B03-CPT-RIASTO-ECC
                TO (SF)-CPT-RIASTO-ECC
           END-IF
           IF B03-PRE-RIASTO-ECC-NULL = HIGH-VALUES
              MOVE B03-PRE-RIASTO-ECC-NULL
                TO (SF)-PRE-RIASTO-ECC-NULL
           ELSE
              MOVE B03-PRE-RIASTO-ECC
                TO (SF)-PRE-RIASTO-ECC
           END-IF
           IF B03-RIS-RIASTA-ECC-NULL = HIGH-VALUES
              MOVE B03-RIS-RIASTA-ECC-NULL
                TO (SF)-RIS-RIASTA-ECC-NULL
           ELSE
              MOVE B03-RIS-RIASTA-ECC
                TO (SF)-RIS-RIASTA-ECC
           END-IF
           IF B03-COD-AGE-NULL = HIGH-VALUES
              MOVE B03-COD-AGE-NULL
                TO (SF)-COD-AGE-NULL
           ELSE
              MOVE B03-COD-AGE
                TO (SF)-COD-AGE
           END-IF
           IF B03-COD-SUBAGE-NULL = HIGH-VALUES
              MOVE B03-COD-SUBAGE-NULL
                TO (SF)-COD-SUBAGE-NULL
           ELSE
              MOVE B03-COD-SUBAGE
                TO (SF)-COD-SUBAGE
           END-IF
           IF B03-COD-CAN-NULL = HIGH-VALUES
              MOVE B03-COD-CAN-NULL
                TO (SF)-COD-CAN-NULL
           ELSE
              MOVE B03-COD-CAN
                TO (SF)-COD-CAN
           END-IF
           IF B03-IB-POLI-NULL = HIGH-VALUES
              MOVE B03-IB-POLI-NULL
                TO (SF)-IB-POLI-NULL
           ELSE
              MOVE B03-IB-POLI
                TO (SF)-IB-POLI
           END-IF
           IF B03-IB-ADES-NULL = HIGH-VALUES
              MOVE B03-IB-ADES-NULL
                TO (SF)-IB-ADES-NULL
           ELSE
              MOVE B03-IB-ADES
                TO (SF)-IB-ADES
           END-IF
           IF B03-IB-TRCH-DI-GAR-NULL = HIGH-VALUES
              MOVE B03-IB-TRCH-DI-GAR-NULL
                TO (SF)-IB-TRCH-DI-GAR-NULL
           ELSE
              MOVE B03-IB-TRCH-DI-GAR
                TO (SF)-IB-TRCH-DI-GAR
           END-IF
           IF B03-TP-PRSTZ-NULL = HIGH-VALUES
              MOVE B03-TP-PRSTZ-NULL
                TO (SF)-TP-PRSTZ-NULL
           ELSE
              MOVE B03-TP-PRSTZ
                TO (SF)-TP-PRSTZ
           END-IF
           IF B03-TP-TRASF-NULL = HIGH-VALUES
              MOVE B03-TP-TRASF-NULL
                TO (SF)-TP-TRASF-NULL
           ELSE
              MOVE B03-TP-TRASF
                TO (SF)-TP-TRASF
           END-IF
           IF B03-PP-INVRIO-TARI-NULL = HIGH-VALUES
              MOVE B03-PP-INVRIO-TARI-NULL
                TO (SF)-PP-INVRIO-TARI-NULL
           ELSE
              MOVE B03-PP-INVRIO-TARI
                TO (SF)-PP-INVRIO-TARI
           END-IF
           IF B03-COEFF-OPZ-REN-NULL = HIGH-VALUES
              MOVE B03-COEFF-OPZ-REN-NULL
                TO (SF)-COEFF-OPZ-REN-NULL
           ELSE
              MOVE B03-COEFF-OPZ-REN
                TO (SF)-COEFF-OPZ-REN
           END-IF
           IF B03-COEFF-OPZ-CPT-NULL = HIGH-VALUES
              MOVE B03-COEFF-OPZ-CPT-NULL
                TO (SF)-COEFF-OPZ-CPT-NULL
           ELSE
              MOVE B03-COEFF-OPZ-CPT
                TO (SF)-COEFF-OPZ-CPT
           END-IF
           IF B03-DUR-PAG-REN-NULL = HIGH-VALUES
              MOVE B03-DUR-PAG-REN-NULL
                TO (SF)-DUR-PAG-REN-NULL
           ELSE
              MOVE B03-DUR-PAG-REN
                TO (SF)-DUR-PAG-REN
           END-IF
           IF B03-VLT-NULL = HIGH-VALUES
              MOVE B03-VLT-NULL
                TO (SF)-VLT-NULL
           ELSE
              MOVE B03-VLT
                TO (SF)-VLT
           END-IF
           IF B03-RIS-MAT-CHIU-PREC-NULL = HIGH-VALUES
              MOVE B03-RIS-MAT-CHIU-PREC-NULL
                TO (SF)-RIS-MAT-CHIU-PREC-NULL
           ELSE
              MOVE B03-RIS-MAT-CHIU-PREC
                TO (SF)-RIS-MAT-CHIU-PREC
           END-IF
           IF B03-COD-FND-NULL = HIGH-VALUES
              MOVE B03-COD-FND-NULL
                TO (SF)-COD-FND-NULL
           ELSE
              MOVE B03-COD-FND
                TO (SF)-COD-FND
           END-IF
           IF B03-PRSTZ-T-NULL = HIGH-VALUES
              MOVE B03-PRSTZ-T-NULL
                TO (SF)-PRSTZ-T-NULL
           ELSE
              MOVE B03-PRSTZ-T
                TO (SF)-PRSTZ-T
           END-IF
           IF B03-TS-TARI-DOV-NULL = HIGH-VALUES
              MOVE B03-TS-TARI-DOV-NULL
                TO (SF)-TS-TARI-DOV-NULL
           ELSE
              MOVE B03-TS-TARI-DOV
                TO (SF)-TS-TARI-DOV
           END-IF
           IF B03-TS-TARI-SCON-NULL = HIGH-VALUES
              MOVE B03-TS-TARI-SCON-NULL
                TO (SF)-TS-TARI-SCON-NULL
           ELSE
              MOVE B03-TS-TARI-SCON
                TO (SF)-TS-TARI-SCON
           END-IF
           IF B03-TS-PP-NULL = HIGH-VALUES
              MOVE B03-TS-PP-NULL
                TO (SF)-TS-PP-NULL
           ELSE
              MOVE B03-TS-PP
                TO (SF)-TS-PP
           END-IF
           IF B03-COEFF-RIS-1-T-NULL = HIGH-VALUES
              MOVE B03-COEFF-RIS-1-T-NULL
                TO (SF)-COEFF-RIS-1-T-NULL
           ELSE
              MOVE B03-COEFF-RIS-1-T
                TO (SF)-COEFF-RIS-1-T
           END-IF
           IF B03-COEFF-RIS-2-T-NULL = HIGH-VALUES
              MOVE B03-COEFF-RIS-2-T-NULL
                TO (SF)-COEFF-RIS-2-T-NULL
           ELSE
              MOVE B03-COEFF-RIS-2-T
                TO (SF)-COEFF-RIS-2-T
           END-IF
           IF B03-ABB-NULL = HIGH-VALUES
              MOVE B03-ABB-NULL
                TO (SF)-ABB-NULL
           ELSE
              MOVE B03-ABB
                TO (SF)-ABB
           END-IF
           IF B03-TP-COASS-NULL = HIGH-VALUES
              MOVE B03-TP-COASS-NULL
                TO (SF)-TP-COASS-NULL
           ELSE
              MOVE B03-TP-COASS
                TO (SF)-TP-COASS
           END-IF
           IF B03-TRAT-RIASS-NULL = HIGH-VALUES
              MOVE B03-TRAT-RIASS-NULL
                TO (SF)-TRAT-RIASS-NULL
           ELSE
              MOVE B03-TRAT-RIASS
                TO (SF)-TRAT-RIASS
           END-IF
           IF B03-TRAT-RIASS-ECC-NULL = HIGH-VALUES
              MOVE B03-TRAT-RIASS-ECC-NULL
                TO (SF)-TRAT-RIASS-ECC-NULL
           ELSE
              MOVE B03-TRAT-RIASS-ECC
                TO (SF)-TRAT-RIASS-ECC
           END-IF
           MOVE B03-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE B03-DS-VER
             TO (SF)-DS-VER
           MOVE B03-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE B03-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE B03-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           IF B03-TP-RGM-FISC-NULL = HIGH-VALUES
              MOVE B03-TP-RGM-FISC-NULL
                TO (SF)-TP-RGM-FISC-NULL
           ELSE
              MOVE B03-TP-RGM-FISC
                TO (SF)-TP-RGM-FISC
           END-IF
           IF B03-DUR-GAR-AA-NULL = HIGH-VALUES
              MOVE B03-DUR-GAR-AA-NULL
                TO (SF)-DUR-GAR-AA-NULL
           ELSE
              MOVE B03-DUR-GAR-AA
                TO (SF)-DUR-GAR-AA
           END-IF
           IF B03-DUR-GAR-MM-NULL = HIGH-VALUES
              MOVE B03-DUR-GAR-MM-NULL
                TO (SF)-DUR-GAR-MM-NULL
           ELSE
              MOVE B03-DUR-GAR-MM
                TO (SF)-DUR-GAR-MM
           END-IF
           IF B03-DUR-GAR-GG-NULL = HIGH-VALUES
              MOVE B03-DUR-GAR-GG-NULL
                TO (SF)-DUR-GAR-GG-NULL
           ELSE
              MOVE B03-DUR-GAR-GG
                TO (SF)-DUR-GAR-GG
           END-IF
           IF B03-ANTIDUR-CALC-365-NULL = HIGH-VALUES
              MOVE B03-ANTIDUR-CALC-365-NULL
                TO (SF)-ANTIDUR-CALC-365-NULL
           ELSE
              MOVE B03-ANTIDUR-CALC-365
                TO (SF)-ANTIDUR-CALC-365
           END-IF
           IF B03-COD-FISC-CNTR-NULL = HIGH-VALUES
              MOVE B03-COD-FISC-CNTR-NULL
                TO (SF)-COD-FISC-CNTR-NULL
           ELSE
              MOVE B03-COD-FISC-CNTR
                TO (SF)-COD-FISC-CNTR
           END-IF
           IF B03-COD-FISC-ASSTO1-NULL = HIGH-VALUES
              MOVE B03-COD-FISC-ASSTO1-NULL
                TO (SF)-COD-FISC-ASSTO1-NULL
           ELSE
              MOVE B03-COD-FISC-ASSTO1
                TO (SF)-COD-FISC-ASSTO1
           END-IF
           IF B03-COD-FISC-ASSTO2-NULL = HIGH-VALUES
              MOVE B03-COD-FISC-ASSTO2-NULL
                TO (SF)-COD-FISC-ASSTO2-NULL
           ELSE
              MOVE B03-COD-FISC-ASSTO2
                TO (SF)-COD-FISC-ASSTO2
           END-IF
           IF B03-COD-FISC-ASSTO3-NULL = HIGH-VALUES
              MOVE B03-COD-FISC-ASSTO3-NULL
                TO (SF)-COD-FISC-ASSTO3-NULL
           ELSE
              MOVE B03-COD-FISC-ASSTO3
                TO (SF)-COD-FISC-ASSTO3
           END-IF
           MOVE B03-CAUS-SCON
             TO (SF)-CAUS-SCON
           MOVE B03-EMIT-TIT-OPZ
             TO (SF)-EMIT-TIT-OPZ
           IF B03-QTZ-SP-Z-COUP-EMIS-NULL = HIGH-VALUES
              MOVE B03-QTZ-SP-Z-COUP-EMIS-NULL
                TO (SF)-QTZ-SP-Z-COUP-EMIS-NULL
           ELSE
              MOVE B03-QTZ-SP-Z-COUP-EMIS
                TO (SF)-QTZ-SP-Z-COUP-EMIS
           END-IF
           IF B03-QTZ-SP-Z-OPZ-EMIS-NULL = HIGH-VALUES
              MOVE B03-QTZ-SP-Z-OPZ-EMIS-NULL
                TO (SF)-QTZ-SP-Z-OPZ-EMIS-NULL
           ELSE
              MOVE B03-QTZ-SP-Z-OPZ-EMIS
                TO (SF)-QTZ-SP-Z-OPZ-EMIS
           END-IF
           IF B03-QTZ-SP-Z-COUP-DT-C-NULL = HIGH-VALUES
              MOVE B03-QTZ-SP-Z-COUP-DT-C-NULL
                TO (SF)-QTZ-SP-Z-COUP-DT-C-NULL
           ELSE
              MOVE B03-QTZ-SP-Z-COUP-DT-C
                TO (SF)-QTZ-SP-Z-COUP-DT-C
           END-IF
           IF B03-QTZ-SP-Z-OPZ-DT-CA-NULL = HIGH-VALUES
              MOVE B03-QTZ-SP-Z-OPZ-DT-CA-NULL
                TO (SF)-QTZ-SP-Z-OPZ-DT-CA-NULL
           ELSE
              MOVE B03-QTZ-SP-Z-OPZ-DT-CA
                TO (SF)-QTZ-SP-Z-OPZ-DT-CA
           END-IF
           IF B03-QTZ-TOT-EMIS-NULL = HIGH-VALUES
              MOVE B03-QTZ-TOT-EMIS-NULL
                TO (SF)-QTZ-TOT-EMIS-NULL
           ELSE
              MOVE B03-QTZ-TOT-EMIS
                TO (SF)-QTZ-TOT-EMIS
           END-IF
           IF B03-QTZ-TOT-DT-CALC-NULL = HIGH-VALUES
              MOVE B03-QTZ-TOT-DT-CALC-NULL
                TO (SF)-QTZ-TOT-DT-CALC-NULL
           ELSE
              MOVE B03-QTZ-TOT-DT-CALC
                TO (SF)-QTZ-TOT-DT-CALC
           END-IF
           IF B03-QTZ-TOT-DT-ULT-BIL-NULL = HIGH-VALUES
              MOVE B03-QTZ-TOT-DT-ULT-BIL-NULL
                TO (SF)-QTZ-TOT-DT-ULT-BIL-NULL
           ELSE
              MOVE B03-QTZ-TOT-DT-ULT-BIL
                TO (SF)-QTZ-TOT-DT-ULT-BIL
           END-IF
           IF B03-DT-QTZ-EMIS-NULL = HIGH-VALUES
              MOVE B03-DT-QTZ-EMIS-NULL
                TO (SF)-DT-QTZ-EMIS-NULL
           ELSE
              MOVE B03-DT-QTZ-EMIS
                TO (SF)-DT-QTZ-EMIS
           END-IF
           IF B03-PC-CAR-GEST-NULL = HIGH-VALUES
              MOVE B03-PC-CAR-GEST-NULL
                TO (SF)-PC-CAR-GEST-NULL
           ELSE
              MOVE B03-PC-CAR-GEST
                TO (SF)-PC-CAR-GEST
           END-IF
           IF B03-PC-CAR-ACQ-NULL = HIGH-VALUES
              MOVE B03-PC-CAR-ACQ-NULL
                TO (SF)-PC-CAR-ACQ-NULL
           ELSE
              MOVE B03-PC-CAR-ACQ
                TO (SF)-PC-CAR-ACQ
           END-IF
           IF B03-IMP-CAR-CASO-MOR-NULL = HIGH-VALUES
              MOVE B03-IMP-CAR-CASO-MOR-NULL
                TO (SF)-IMP-CAR-CASO-MOR-NULL
           ELSE
              MOVE B03-IMP-CAR-CASO-MOR
                TO (SF)-IMP-CAR-CASO-MOR
           END-IF
           IF B03-PC-CAR-MOR-NULL = HIGH-VALUES
              MOVE B03-PC-CAR-MOR-NULL
                TO (SF)-PC-CAR-MOR-NULL
           ELSE
              MOVE B03-PC-CAR-MOR
                TO (SF)-PC-CAR-MOR
           END-IF
           IF B03-TP-VERS-NULL = HIGH-VALUES
              MOVE B03-TP-VERS-NULL
                TO (SF)-TP-VERS-NULL
           ELSE
              MOVE B03-TP-VERS
                TO (SF)-TP-VERS
           END-IF
           IF B03-FL-SWITCH-NULL = HIGH-VALUES
              MOVE B03-FL-SWITCH-NULL
                TO (SF)-FL-SWITCH-NULL
           ELSE
              MOVE B03-FL-SWITCH
                TO (SF)-FL-SWITCH
           END-IF
           IF B03-FL-IAS-NULL = HIGH-VALUES
              MOVE B03-FL-IAS-NULL
                TO (SF)-FL-IAS-NULL
           ELSE
              MOVE B03-FL-IAS
                TO (SF)-FL-IAS
           END-IF
           IF B03-DIR-NULL = HIGH-VALUES
              MOVE B03-DIR-NULL
                TO (SF)-DIR-NULL
           ELSE
              MOVE B03-DIR
                TO (SF)-DIR
           END-IF
           IF B03-TP-COP-CASO-MOR-NULL = HIGH-VALUES
              MOVE B03-TP-COP-CASO-MOR-NULL
                TO (SF)-TP-COP-CASO-MOR-NULL
           ELSE
              MOVE B03-TP-COP-CASO-MOR
                TO (SF)-TP-COP-CASO-MOR
           END-IF
           IF B03-MET-RISC-SPCL-NULL = HIGH-VALUES
              MOVE B03-MET-RISC-SPCL-NULL
                TO (SF)-MET-RISC-SPCL-NULL
           ELSE
              MOVE B03-MET-RISC-SPCL
                TO (SF)-MET-RISC-SPCL
           END-IF
           IF B03-TP-STAT-INVST-NULL = HIGH-VALUES
              MOVE B03-TP-STAT-INVST-NULL
                TO (SF)-TP-STAT-INVST-NULL
           ELSE
              MOVE B03-TP-STAT-INVST
                TO (SF)-TP-STAT-INVST
           END-IF
           IF B03-COD-PRDT-NULL = HIGH-VALUES
              MOVE B03-COD-PRDT-NULL
                TO (SF)-COD-PRDT-NULL
           ELSE
              MOVE B03-COD-PRDT
                TO (SF)-COD-PRDT
           END-IF
           IF B03-STAT-ASSTO-1-NULL = HIGH-VALUES
              MOVE B03-STAT-ASSTO-1-NULL
                TO (SF)-STAT-ASSTO-1-NULL
           ELSE
              MOVE B03-STAT-ASSTO-1
                TO (SF)-STAT-ASSTO-1
           END-IF
           IF B03-STAT-ASSTO-2-NULL = HIGH-VALUES
              MOVE B03-STAT-ASSTO-2-NULL
                TO (SF)-STAT-ASSTO-2-NULL
           ELSE
              MOVE B03-STAT-ASSTO-2
                TO (SF)-STAT-ASSTO-2
           END-IF
           IF B03-STAT-ASSTO-3-NULL = HIGH-VALUES
              MOVE B03-STAT-ASSTO-3-NULL
                TO (SF)-STAT-ASSTO-3-NULL
           ELSE
              MOVE B03-STAT-ASSTO-3
                TO (SF)-STAT-ASSTO-3
           END-IF
           IF B03-CPT-ASSTO-INI-MOR-NULL = HIGH-VALUES
              MOVE B03-CPT-ASSTO-INI-MOR-NULL
                TO (SF)-CPT-ASSTO-INI-MOR-NULL
           ELSE
              MOVE B03-CPT-ASSTO-INI-MOR
                TO (SF)-CPT-ASSTO-INI-MOR
           END-IF
           IF B03-TS-STAB-PRE-NULL = HIGH-VALUES
              MOVE B03-TS-STAB-PRE-NULL
                TO (SF)-TS-STAB-PRE-NULL
           ELSE
              MOVE B03-TS-STAB-PRE
                TO (SF)-TS-STAB-PRE
           END-IF
           IF B03-DIR-EMIS-NULL = HIGH-VALUES
              MOVE B03-DIR-EMIS-NULL
                TO (SF)-DIR-EMIS-NULL
           ELSE
              MOVE B03-DIR-EMIS
                TO (SF)-DIR-EMIS
           END-IF
           IF B03-DT-INC-ULT-PRE-NULL = HIGH-VALUES
              MOVE B03-DT-INC-ULT-PRE-NULL
                TO (SF)-DT-INC-ULT-PRE-NULL
           ELSE
              MOVE B03-DT-INC-ULT-PRE
                TO (SF)-DT-INC-ULT-PRE
           END-IF
           IF B03-STAT-TBGC-ASSTO-1-NULL = HIGH-VALUES
              MOVE B03-STAT-TBGC-ASSTO-1-NULL
                TO (SF)-STAT-TBGC-ASSTO-1-NULL
           ELSE
              MOVE B03-STAT-TBGC-ASSTO-1
                TO (SF)-STAT-TBGC-ASSTO-1
           END-IF
           IF B03-STAT-TBGC-ASSTO-2-NULL = HIGH-VALUES
              MOVE B03-STAT-TBGC-ASSTO-2-NULL
                TO (SF)-STAT-TBGC-ASSTO-2-NULL
           ELSE
              MOVE B03-STAT-TBGC-ASSTO-2
                TO (SF)-STAT-TBGC-ASSTO-2
           END-IF
           IF B03-STAT-TBGC-ASSTO-3-NULL = HIGH-VALUES
              MOVE B03-STAT-TBGC-ASSTO-3-NULL
                TO (SF)-STAT-TBGC-ASSTO-3-NULL
           ELSE
              MOVE B03-STAT-TBGC-ASSTO-3
                TO (SF)-STAT-TBGC-ASSTO-3
           END-IF
           IF B03-FRAZ-DECR-CPT-NULL = HIGH-VALUES
              MOVE B03-FRAZ-DECR-CPT-NULL
                TO (SF)-FRAZ-DECR-CPT-NULL
           ELSE
              MOVE B03-FRAZ-DECR-CPT
                TO (SF)-FRAZ-DECR-CPT
           END-IF
           IF B03-PRE-PP-ULT-NULL = HIGH-VALUES
              MOVE B03-PRE-PP-ULT-NULL
                TO (SF)-PRE-PP-ULT-NULL
           ELSE
              MOVE B03-PRE-PP-ULT
                TO (SF)-PRE-PP-ULT
           END-IF
           IF B03-ACQ-EXP-NULL = HIGH-VALUES
              MOVE B03-ACQ-EXP-NULL
                TO (SF)-ACQ-EXP-NULL
           ELSE
              MOVE B03-ACQ-EXP
                TO (SF)-ACQ-EXP
           END-IF
           IF B03-REMUN-ASS-NULL = HIGH-VALUES
              MOVE B03-REMUN-ASS-NULL
                TO (SF)-REMUN-ASS-NULL
           ELSE
              MOVE B03-REMUN-ASS
                TO (SF)-REMUN-ASS
           END-IF
           IF B03-COMMIS-INTER-NULL = HIGH-VALUES
              MOVE B03-COMMIS-INTER-NULL
                TO (SF)-COMMIS-INTER-NULL
           ELSE
              MOVE B03-COMMIS-INTER
                TO (SF)-COMMIS-INTER
           END-IF
           IF B03-NUM-FINANZ-NULL = HIGH-VALUES
              MOVE B03-NUM-FINANZ-NULL
                TO (SF)-NUM-FINANZ-NULL
           ELSE
              MOVE B03-NUM-FINANZ
                TO (SF)-NUM-FINANZ
           END-IF
           IF B03-TP-ACC-COMM-NULL = HIGH-VALUES
              MOVE B03-TP-ACC-COMM-NULL
                TO (SF)-TP-ACC-COMM-NULL
           ELSE
              MOVE B03-TP-ACC-COMM
                TO (SF)-TP-ACC-COMM
           END-IF
           IF B03-IB-ACC-COMM-NULL = HIGH-VALUES
              MOVE B03-IB-ACC-COMM-NULL
                TO (SF)-IB-ACC-COMM-NULL
           ELSE
              MOVE B03-IB-ACC-COMM
                TO (SF)-IB-ACC-COMM
           END-IF
           MOVE B03-RAMO-BILA
             TO (SF)-RAMO-BILA
           IF B03-CARZ-NULL = HIGH-VALUES
              MOVE B03-CARZ-NULL
                TO (SF)-CARZ-NULL
           ELSE
              MOVE B03-CARZ
                TO (SF)-CARZ
           END-IF.
       VALORIZZA-OUTPUT-B03-EX.
           EXIT.

       01 AREA-LDBV1200.
         05 LDBV1200-ID-RAPP-ANA           PIC S9(09)     COMP-3.
         05 LDBV1200-TP-SIN                PIC S9(02)     COMP-3.
         05 LDBV1200-TP-SIN-RED            REDEFINES
            LDBV1200-TP-SIN                PIC  X(02).

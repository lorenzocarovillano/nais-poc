       01 ACC-COMM.
         05 P63-ID-ACC-COMM PIC S9(9)V     COMP-3.
         05 P63-TP-ACC-COMM PIC X(2).
         05 P63-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 P63-COD-PARTNER PIC S9(10)V     COMP-3.
         05 P63-IB-ACC-COMM PIC X(40).
         05 P63-IB-ACC-COMM-MASTER PIC X(40).
         05 P63-DESC-ACC-COMM-VCHAR.
           49 P63-DESC-ACC-COMM-LEN PIC S9(4) COMP-5.
           49 P63-DESC-ACC-COMM PIC X(100).
         05 P63-DT-FIRMA   PIC S9(8)V COMP-3.
         05 P63-DT-INI-VLDT   PIC S9(8)V COMP-3.
         05 P63-DT-END-VLDT   PIC S9(8)V COMP-3.
         05 P63-FL-INVIO-CONFERME PIC X(1).
         05 P63-DS-OPER-SQL PIC X(1).
         05 P63-DS-VER PIC S9(9)V     COMP-3.
         05 P63-DS-TS-CPTZ PIC S9(18)V     COMP-3.
         05 P63-DS-UTENTE PIC X(20).
         05 P63-DS-STATO-ELAB PIC X(1).
         05 P63-DESC-ACC-COMM-MAST-VCHAR.
           49 P63-DESC-ACC-COMM-MAST-LEN PIC S9(4) COMP-5.
           49 P63-DESC-ACC-COMM-MAST PIC X(100).


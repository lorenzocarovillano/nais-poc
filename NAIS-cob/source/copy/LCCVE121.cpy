      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA EST_TRCH_DI_GAR
      *   ALIAS E12
      *   ULTIMO AGG. 14 MAR 2012
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-EST-TRCH-DI-GAR PIC S9(9)     COMP-3.
             07 (SF)-ID-TRCH-DI-GAR PIC S9(9)     COMP-3.
             07 (SF)-ID-GAR PIC S9(9)     COMP-3.
             07 (SF)-ID-ADES PIC S9(9)     COMP-3.
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-DT-EMIS   PIC S9(8) COMP-3.
             07 (SF)-DT-EMIS-NULL REDEFINES
                (SF)-DT-EMIS   PIC X(5).
             07 (SF)-CUM-PRE-ATT PIC S9(12)V9(3) COMP-3.
             07 (SF)-CUM-PRE-ATT-NULL REDEFINES
                (SF)-CUM-PRE-ATT   PIC X(8).
             07 (SF)-ACCPRE-PAG PIC S9(12)V9(3) COMP-3.
             07 (SF)-ACCPRE-PAG-NULL REDEFINES
                (SF)-ACCPRE-PAG   PIC X(8).
             07 (SF)-CUM-PRSTZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-CUM-PRSTZ-NULL REDEFINES
                (SF)-CUM-PRSTZ   PIC X(8).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-TP-TRCH PIC X(2).
             07 (SF)-CAUS-SCON PIC X(12).
             07 (SF)-CAUS-SCON-NULL REDEFINES
                (SF)-CAUS-SCON   PIC X(12).

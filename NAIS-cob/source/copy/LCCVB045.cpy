
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVB045
      *   ULTIMO AGG. 14 MAR 2011
      *------------------------------------------------------------

       VAL-DCLGEN-B04.
           MOVE (SF)-ID-BILA-VAR-CALC-P
              TO B04-ID-BILA-VAR-CALC-P
           MOVE (SF)-COD-COMP-ANIA
              TO B04-COD-COMP-ANIA
           MOVE (SF)-ID-RICH-ESTRAZ-MAS
              TO B04-ID-RICH-ESTRAZ-MAS
           IF (SF)-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
              MOVE (SF)-ID-RICH-ESTRAZ-AGG-NULL
              TO B04-ID-RICH-ESTRAZ-AGG-NULL
           ELSE
              MOVE (SF)-ID-RICH-ESTRAZ-AGG
              TO B04-ID-RICH-ESTRAZ-AGG
           END-IF
           MOVE (SF)-DT-RIS
              TO B04-DT-RIS
           MOVE (SF)-ID-POLI
              TO B04-ID-POLI
           MOVE (SF)-ID-ADES
              TO B04-ID-ADES
           MOVE (SF)-PROG-SCHEDA-VALOR
              TO B04-PROG-SCHEDA-VALOR
           MOVE (SF)-TP-RGM-FISC
              TO B04-TP-RGM-FISC
           MOVE (SF)-DT-INI-VLDT-PROD
              TO B04-DT-INI-VLDT-PROD
           MOVE (SF)-COD-VAR
              TO B04-COD-VAR
           MOVE (SF)-TP-D
              TO B04-TP-D
           IF (SF)-VAL-IMP-NULL = HIGH-VALUES
              MOVE (SF)-VAL-IMP-NULL
              TO B04-VAL-IMP-NULL
           ELSE
              MOVE (SF)-VAL-IMP
              TO B04-VAL-IMP
           END-IF
           IF (SF)-VAL-PC-NULL = HIGH-VALUES
              MOVE (SF)-VAL-PC-NULL
              TO B04-VAL-PC-NULL
           ELSE
              MOVE (SF)-VAL-PC
              TO B04-VAL-PC
           END-IF
           IF (SF)-VAL-STRINGA-NULL = HIGH-VALUES
              MOVE (SF)-VAL-STRINGA-NULL
              TO B04-VAL-STRINGA-NULL
           ELSE
              MOVE (SF)-VAL-STRINGA
              TO B04-VAL-STRINGA
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO B04-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO B04-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO B04-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO B04-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO B04-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO B04-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO B04-DS-STATO-ELAB
           MOVE (SF)-AREA-D-VALOR-VAR-VCHAR
              TO B04-AREA-D-VALOR-VAR-VCHAR.
       VAL-DCLGEN-B04-EX.
           EXIT.

      *----------------------------------------------------------------*
      * COPY PER AREE DI APPOGGIO PER GESTIONE ERRORI DERIVANTI DA
      * PRODOTTO
      *----------------------------------------------------------------*
       01 ISPC0001-AREA-ERRORI.
          10 ISPC0001-ESITO                      PIC  9(002).
          10 ISPC0001-ERR-NUM-ELE                PIC  9(003).
          10 ISPC0001-TAB-ERRORI                 OCCURS 10.
             15 ISPC0001-COD-ERR                 PIC  X(012).
             15 ISPC0001-DESCRIZIONE-ERR         PIC  X(050).
             15 ISPC0001-GRAVITA-ERR             PIC  9(002).
                88 ISPC0001-GR-WARNING-ACT       VALUE 92.
                88 ISPC0001-GR-BLOCCANTE         VALUE 93.
                88 ISPC0001-GR-NON-BLOCC         VALUE 94.
                88 ISPC0001-GR-WARNING-UTE       VALUE 95.
                88 ISPC0001-GR-OPER-NON-CONS     VALUE 00.
             15 ISPC0001-LIVELLO-DEROGA          PIC  9(002).

       01 ISPC0001-STEP-ELAB                     PIC  X(001).

       01 WS-FLAG-RIC-DEROGA-ERR                 PIC  9(001) VALUE ZERO.
          88 CERCA-DEROGA-ERR                                VALUE ZERO.
          88 DEROGA-PRESENTE-ERR                             VALUE 1.

       01 IX-ERR-GENERIC                         PIC  9(004) COMP
                                                             VALUE ZERO.
       01 IX-ERR-DEROG                           PIC  9(004) COMP
                                                             VALUE ZERO.

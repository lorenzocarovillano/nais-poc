      *----------------------------------------------------------------*
      * AREA VALORIZZATORE VARIABILI GARANZIA
      *----------------------------------------------------------------*
      *
           05 (SF)-AREA-VARIABILI-GAR.
              07 (SF)-ELE-MAX-AREA-GAR              PIC S9(04) COMP.
              07 (SF)-TAB-LIVELLI-GAR               OCCURS 20.
                 09 (SF)-ID-GARANZIA                PIC  9(009).
                 09 (SF)-COD-TARI                   PIC  X(12).
                 09 (SF)-ELE-MAX-VAR-GAR            PIC S9(04) COMP.
                 09 (SF)-TAB-VAR-GAR                OCCURS 30.
                    11 (SF)-AREA-VAR-GAR.
                       13 (SF)-COD-VARIABILE    PIC  X(12).
                       13 (SF)-TP-DATO          PIC  X(01).
                       13 (SF)-VAL-IMP          PIC  S9(11)V9(07).
                       13 (SF)-VAL-PERC         PIC   9(05)V9(09).
                       13 (SF)-VAL-STR          PIC  X(60).

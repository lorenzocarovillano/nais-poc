       01 AMMB-FUNZ-BLOCCO.
         05 L16-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 L16-COD-CAN PIC S9(5)V     COMP-3.
         05 L16-COD-BLOCCO PIC X(5).
         05 L16-TP-MOVI PIC S9(5)V     COMP-3.
         05 L16-GRAV-FUNZ-BLOCCO PIC X(1).
         05 L16-DS-OPER-SQL PIC X(1).
         05 L16-DS-VER PIC S9(9)V     COMP-3.
         05 L16-DS-TS-CPTZ PIC S9(18)V     COMP-3.
         05 L16-DS-UTENTE PIC X(20).
         05 L16-DS-STATO-ELAB PIC X(1).
         05 L16-TP-MOVI-RIFTO PIC S9(5)V     COMP-3.
         05 L16-TP-MOVI-RIFTO-NULL REDEFINES
            L16-TP-MOVI-RIFTO   PIC X(3).


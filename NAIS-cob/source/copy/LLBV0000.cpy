      ******************************************************************
      *    COPY LLBV0000 - DICHIARAZIONE VARIABILI DI WORKING COMUNI
      ******************************************************************
      *----------------------------------------------------------------*
      *    GESTIONE TASTI PAGINA
      *----------------------------------------------------------------*
       01 WS-TASTO.
          05 WS-ABILITA                     PIC X(01) VALUE 'A'.
          05 WS-DISABILITA                  PIC X(01) VALUE 'D'.

      *----------------------------------------------------------------*
      *    COSTANTI GLOBALI
      *----------------------------------------------------------------*
       01 WS-COSTANTI.
          05 WS-COD-MACRFUNZ-LEG-BIL  PIC X(02) VALUE 'LB'.
          05 WS-TP-CALC-RIS-LG        PIC X(02) VALUE 'LG'.

      *----------------------------------------------------------------*
      *    DESCRIZIONI DI PAGINA (COSTANTI)
      *----------------------------------------------------------------*
       01 WS-DESCRIZIONI-PAGINA.

      *   DESCRIZIONI PER "FORMA ASSICURATIVA"
          05 WS-DESC-FRM-COLL         PIC X(040) VALUE 'Collettive'.
          05 WS-DESC-FRM-INDI         PIC X(040) VALUE 'Individuali'.
          05 WS-DESC-FRM-ENTR         PIC X(040) VALUE 'Entrambe'.

      *   DESCRIZIONE PER "TIPO CALCOLO"
          05 WS-DESC-CALC-LOCAL       PIC X(010) VALUE 'Local'.
          05 WS-DESC-CALC-IAS         PIC X(010) VALUE 'IAS'.
          05 WS-DESC-CALC-COASS       PIC X(010) VALUE 'Coass'.

      *   DESCRIZIONE PER "STATO ELABORAZIONE"
          05 WS-DESC-STATO-LAVORAZ    PIC X(040) VALUE 'In lavorazione'.
          05 WS-DESC-STATO-CONSOLID   PIC X(040) VALUE 'Consolidata'.

      *----------------------------------------------------------------*
      *    TIPO RICHIESTA
      *----------------------------------------------------------------*
      *    B1 => Estrazione Dati Massiva
      *    B2 => Aggiornamento Dati Estratti
      *    B3 => Calcolo Riserve
      *    B4 => Consolidamento Riserve Calcolate
      *    B5 => Estrazione Dati Massiva e Calcolo Riserve
      *    B6 => Aggiornamento Dati Estratti e Calcolo Riserve
      *    B7 => Verifica Corrispondenza Adesioni Estratte/Calcolate
      *    B8 => Certificazione Dati Estratti
      *    B9 => Certificazione Dati Calcolati
      *    BA => Aggiornamento Calcolo Riserve
      *    BB => Svecchiamento Prenotazioni Annullate
      *    BC => Annullamento Estrazione Dati Massiva
      *    BD => Riserve Somme da Pagare
      *    BE => Aggiornamento Riserve Somme da Pagare
      *    BF => Certificazione Riserve Somme da Pagare
      *    BG => Consolidamento Riserve Somme da Pagare
       01  WS-TP-RICH                    PIC X(002) VALUE SPACES.
           88 WS-RICH-ESTRAZ-MASS        VALUE 'B1'.
           88 WS-RICH-AGG-ESTRAZ         VALUE 'B2'.
           88 WS-RICH-CALC-RISERV        VALUE 'B3'.
           88 WS-RICH-CONSOLIDAM         VALUE 'B4'.
           88 WS-RICH-ESTR-E-CALC        VALUE 'B5'.
           88 WS-RICH-AGG-ESTR-E-CALC    VALUE 'B6'.
           88 WS-RICH-VERIFICA           VALUE 'B7'.
           88 WS-RICH-CERTIF-ESTR        VALUE 'B8'.
           88 WS-RICH-CERTIFIC-CALC      VALUE 'B9'.
           88 WS-RICH-AGG-CALC           VALUE 'BA'.
           88 WS-RICH-SVECCH-PREN-ANN    VALUE 'BB'.
           88 WS-RICH-ANN-ESTRAZ-MASS    VALUE 'BC'.
           88 WS-RICH-ESTR-SOMM-PAG      VALUE 'BD'.
           88 WS-RICH-AGG-SOMM-PAG       VALUE 'BE'.
           88 WS-RICH-CERT-SOMM-PAG      VALUE 'BF'.
           88 WS-RICH-CONSOL-SOMM-PAG    VALUE 'BG'.

      *----------------------------------------------------------------*
      *    FLAG SIMULAZIONE
      *----------------------------------------------------------------*
       01  WS-FLAG-SIMULAZIONE           PIC X(001) VALUE SPACES.
           88 WS-SIMULAZIONE-SI          VALUE 'S'.
           88 WS-SIMULAZIONE-NO          VALUE 'N'.

      *----------------------------------------------------------------*
      *    STATI PRENOTAZIONE
      *----------------------------------------------------------------*
      *    'P' => PRENOTATA
      *    'E' => ESEGUITA
      *    'A' => ANNULLATA
      *    'C' => CERTIFICATA
      *    'S' => CONSOLIDATA
      *    'K' => ESEGUITA KO
       01  WS-STATI-PRENOTAZIONE         PIC X(001) VALUE SPACES.
           88 WS-PREN-ST-PRENOTATA       VALUE 'P'.
           88 WS-PREN-ST-ESEGUITA        VALUE 'E'.
           88 WS-PREN-ST-ESEGUITA-KO     VALUE 'K'.
           88 WS-PREN-ST-ANNULLATA       VALUE 'A'.
           88 WS-PREN-ST-CERTIFICATA     VALUE 'C'.
           88 WS-PREN-ST-CONSOLIDATA     VALUE 'S'.

      *----------------------------------------------------------------*
      *    FLAG ANNULLAMENTO PRENOTAZIONE
      *----------------------------------------------------------------*
       01  WS-FL-ANNULLA-PRENOT                PIC X(001).
           88 WS-FL-ANNULLA-PRENOT-SI          VALUE 'S'.
           88 WS-FL-ANNULLA-PRENOT-NO          VALUE 'N'.

      *----------------------------------------------------------------*
      *    COD BATCH TYPE DA PASSARE IN INPUT AL SERVIZIO DI
      *    PRENOTAZIONE BATCH (ARCHITETTURA BATCH)
      *----------------------------------------------------------------*
      *    620401 => Estrazione Dati x individuali
      *    620402 => Aggiornamento Dati Estratti x individuali
      *    620403 => Calcolo Riserve
      *    620404 => Aggiornamento Calcolo Riserve
      *    620405 => Consolidamento Riserve Calcolate
      *    620406 => Riserve Somme da Pagare
      *    620407 => Aggiornamento Riserve Somme da Pagare
      *    620408 => Consolidamento Riserve Somme da Pagare
      *    620409 => Estrazione Dati x collettive
      *    620410 => Aggiornamento Dati Estratti x collettive
       01  WS-COD-BATCH-TYPE             PIC 9(009) VALUE 0.
           88 WS-CBT-ESTR-DATI           VALUE 620401.
           88 WS-CBT-AGG-ESTR-DATI       VALUE 620402.
           88 WS-CBT-CALC-RIS            VALUE 620403.
           88 WS-CBT-AGG-CALC-RIS        VALUE 620404.
           88 WS-CBT-CONSOLIDAM          VALUE 620405.
           88 WS-CBT-ESTR-SOMM-PAG       VALUE 620406.
           88 WS-CBT-AGG-SOMM-PAG        VALUE 620407.
           88 WS-CBT-CONSOL-SOMM-PAG     VALUE 620408.
           88 WS-CBT-ESTR-DATI-COLL      VALUE 620409.
           88 WS-CBT-AGG-ESTR-DATI-COLL  VALUE 620410.

           EXEC SQL DECLARE DETT_RICH TABLE
           (
             ID_RICH             DECIMAL(9, 0) NOT NULL,
             PROG_DETT_RICH      DECIMAL(5, 0) NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             TP_AREA_D_RICH      CHAR(3),
             AREA_D_RICH         VARCHAR(10000),
             DS_OPER_SQL         CHAR(1) NOT NULL WITH DEFAULT,
             DS_VER              DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_UTENTE           CHAR(20) NOT NULL WITH DEFAULT,
             DS_STATO_ELAB       CHAR(1) NOT NULL WITH DEFAULT
          ) END-EXEC.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVP583
      *   ULTIMO AGG. 09 AGO 2013
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-P58.
           MOVE P58-ID-IMPST-BOLLO
             TO (SF)-ID-PTF(IX-TAB-P58)
           MOVE P58-ID-IMPST-BOLLO
             TO (SF)-ID-IMPST-BOLLO(IX-TAB-P58)
           MOVE P58-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-P58)
           MOVE P58-ID-POLI
             TO (SF)-ID-POLI(IX-TAB-P58)
           MOVE P58-IB-POLI
             TO (SF)-IB-POLI(IX-TAB-P58)
           IF P58-COD-FISC-NULL = HIGH-VALUES
              MOVE P58-COD-FISC-NULL
                TO (SF)-COD-FISC-NULL(IX-TAB-P58)
           ELSE
              MOVE P58-COD-FISC
                TO (SF)-COD-FISC(IX-TAB-P58)
           END-IF
           IF P58-COD-PART-IVA-NULL = HIGH-VALUES
              MOVE P58-COD-PART-IVA-NULL
                TO (SF)-COD-PART-IVA-NULL(IX-TAB-P58)
           ELSE
              MOVE P58-COD-PART-IVA
                TO (SF)-COD-PART-IVA(IX-TAB-P58)
           END-IF
           MOVE P58-ID-RAPP-ANA
             TO (SF)-ID-RAPP-ANA(IX-TAB-P58)
           MOVE P58-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-P58)
           IF P58-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE P58-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P58)
           ELSE
              MOVE P58-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-P58)
           END-IF
           MOVE P58-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-P58)
           MOVE P58-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-P58)
           MOVE P58-DT-INI-CALC
             TO (SF)-DT-INI-CALC(IX-TAB-P58)
           MOVE P58-DT-END-CALC
             TO (SF)-DT-END-CALC(IX-TAB-P58)
           MOVE P58-TP-CAUS-BOLLO
             TO (SF)-TP-CAUS-BOLLO(IX-TAB-P58)
           MOVE P58-IMPST-BOLLO-DETT-C
             TO (SF)-IMPST-BOLLO-DETT-C(IX-TAB-P58)
           IF P58-IMPST-BOLLO-DETT-V-NULL = HIGH-VALUES
              MOVE P58-IMPST-BOLLO-DETT-V-NULL
                TO (SF)-IMPST-BOLLO-DETT-V-NULL(IX-TAB-P58)
           ELSE
              MOVE P58-IMPST-BOLLO-DETT-V
                TO (SF)-IMPST-BOLLO-DETT-V(IX-TAB-P58)
           END-IF
           IF P58-IMPST-BOLLO-TOT-V-NULL = HIGH-VALUES
              MOVE P58-IMPST-BOLLO-TOT-V-NULL
                TO (SF)-IMPST-BOLLO-TOT-V-NULL(IX-TAB-P58)
           ELSE
              MOVE P58-IMPST-BOLLO-TOT-V
                TO (SF)-IMPST-BOLLO-TOT-V(IX-TAB-P58)
           END-IF
           MOVE P58-IMPST-BOLLO-TOT-R
             TO (SF)-IMPST-BOLLO-TOT-R(IX-TAB-P58)
           MOVE P58-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-P58)
           MOVE P58-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-P58)
           MOVE P58-DS-VER
             TO (SF)-DS-VER(IX-TAB-P58)
           MOVE P58-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-P58)
           MOVE P58-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-P58)
           MOVE P58-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-P58)
           MOVE P58-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-P58).
       VALORIZZA-OUTPUT-P58-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVL414
      *   ULTIMO AGG. 22 APR 2010
      *------------------------------------------------------------

       INIZIA-TOT-L41.

           PERFORM INIZIA-ZEROES-L41 THRU INIZIA-ZEROES-L41-EX

           PERFORM INIZIA-SPACES-L41 THRU INIZIA-SPACES-L41-EX

           PERFORM INIZIA-NULL-L41 THRU INIZIA-NULL-L41-EX.

       INIZIA-TOT-L41-EX.
           EXIT.

       INIZIA-NULL-L41.
           MOVE HIGH-VALUES TO (SF)-DESC-AGG(IX-TAB-L41).
       INIZIA-NULL-L41-EX.
           EXIT.

       INIZIA-ZEROES-L41.
           MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-L41)
           MOVE 0 TO (SF)-DT-QTZ(IX-TAB-L41)
           MOVE 0 TO (SF)-VAL-QUO(IX-TAB-L41)
           MOVE 0 TO (SF)-DS-VER(IX-TAB-L41)
           MOVE 0 TO (SF)-DS-TS-CPTZ(IX-TAB-L41).
       INIZIA-ZEROES-L41-EX.
           EXIT.

       INIZIA-SPACES-L41.
           MOVE SPACES TO (SF)-TP-QTZ(IX-TAB-L41)
           MOVE SPACES TO (SF)-COD-FND(IX-TAB-L41)
           MOVE SPACES TO (SF)-TP-FND(IX-TAB-L41)
           MOVE SPACES TO (SF)-DS-OPER-SQL(IX-TAB-L41)
           MOVE SPACES TO (SF)-DS-UTENTE(IX-TAB-L41)
           MOVE SPACES TO (SF)-DS-STATO-ELAB(IX-TAB-L41).
       INIZIA-SPACES-L41-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVMMO4
      *   ULTIMO AGG. 28 FEB 2008
      *------------------------------------------------------------

       INIZIA-TOT-MMO.

           PERFORM INIZIA-ZEROES-MMO THRU INIZIA-ZEROES-MMO-EX

           PERFORM INIZIA-SPACES-MMO THRU INIZIA-SPACES-MMO-EX

           PERFORM INIZIA-NULL-MMO THRU INIZIA-NULL-MMO-EX.

       INIZIA-TOT-MMO-EX.
           EXIT.

       INIZIA-NULL-MMO.
           MOVE HIGH-VALUES TO (SF)-TP-FRM-ASSVA-NULL
           MOVE HIGH-VALUES TO (SF)-AMMISSIBILITA-MOVI-NULL
           MOVE HIGH-VALUES TO (SF)-SERVIZIO-CONTROLLO-NULL
           MOVE HIGH-VALUES TO (SF)-COD-PROCESSO-WF-NULL.
       INIZIA-NULL-MMO-EX.
           EXIT.

       INIZIA-ZEROES-MMO.
           MOVE 0 TO (SF)-ID-MATR-MOVIMENTO
           MOVE 0 TO (SF)-COD-COMP-ANIA
           MOVE 0 TO (SF)-TP-MOVI-PTF.
       INIZIA-ZEROES-MMO-EX.
           EXIT.

       INIZIA-SPACES-MMO.
           MOVE SPACES TO (SF)-TP-OGG
           MOVE SPACES TO (SF)-TP-MOVI-ACT.
       INIZIA-SPACES-MMO-EX.
           EXIT.

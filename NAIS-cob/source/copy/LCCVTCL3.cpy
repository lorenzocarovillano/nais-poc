
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVTCL3
      *   ULTIMO AGG. 02 LUG 2014
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-TCL.
           MOVE TCL-ID-TCONT-LIQ
             TO (SF)-ID-PTF(IX-TAB-TCL)
           MOVE TCL-ID-TCONT-LIQ
             TO (SF)-ID-TCONT-LIQ(IX-TAB-TCL)
           MOVE TCL-ID-PERC-LIQ
             TO (SF)-ID-PERC-LIQ(IX-TAB-TCL)
           MOVE TCL-ID-BNFICR-LIQ
             TO (SF)-ID-BNFICR-LIQ(IX-TAB-TCL)
           MOVE TCL-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-TCL)
           IF TCL-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE TCL-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-TCL)
           END-IF
           MOVE TCL-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-TCL)
           MOVE TCL-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-TCL)
           MOVE TCL-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-TCL)
           IF TCL-DT-VLT-NULL = HIGH-VALUES
              MOVE TCL-DT-VLT-NULL
                TO (SF)-DT-VLT-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-DT-VLT
                TO (SF)-DT-VLT(IX-TAB-TCL)
           END-IF
           IF TCL-IMP-LRD-LIQTO-NULL = HIGH-VALUES
              MOVE TCL-IMP-LRD-LIQTO-NULL
                TO (SF)-IMP-LRD-LIQTO-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMP-LRD-LIQTO
                TO (SF)-IMP-LRD-LIQTO(IX-TAB-TCL)
           END-IF
           IF TCL-IMP-PREST-NULL = HIGH-VALUES
              MOVE TCL-IMP-PREST-NULL
                TO (SF)-IMP-PREST-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMP-PREST
                TO (SF)-IMP-PREST(IX-TAB-TCL)
           END-IF
           IF TCL-IMP-INTR-PREST-NULL = HIGH-VALUES
              MOVE TCL-IMP-INTR-PREST-NULL
                TO (SF)-IMP-INTR-PREST-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMP-INTR-PREST
                TO (SF)-IMP-INTR-PREST(IX-TAB-TCL)
           END-IF
           IF TCL-IMP-RAT-NULL = HIGH-VALUES
              MOVE TCL-IMP-RAT-NULL
                TO (SF)-IMP-RAT-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMP-RAT
                TO (SF)-IMP-RAT(IX-TAB-TCL)
           END-IF
           IF TCL-IMP-UTI-NULL = HIGH-VALUES
              MOVE TCL-IMP-UTI-NULL
                TO (SF)-IMP-UTI-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMP-UTI
                TO (SF)-IMP-UTI(IX-TAB-TCL)
           END-IF
           IF TCL-IMP-RIT-TFR-NULL = HIGH-VALUES
              MOVE TCL-IMP-RIT-TFR-NULL
                TO (SF)-IMP-RIT-TFR-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMP-RIT-TFR
                TO (SF)-IMP-RIT-TFR(IX-TAB-TCL)
           END-IF
           IF TCL-IMP-RIT-ACC-NULL = HIGH-VALUES
              MOVE TCL-IMP-RIT-ACC-NULL
                TO (SF)-IMP-RIT-ACC-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMP-RIT-ACC
                TO (SF)-IMP-RIT-ACC(IX-TAB-TCL)
           END-IF
           IF TCL-IMP-RIT-VIS-NULL = HIGH-VALUES
              MOVE TCL-IMP-RIT-VIS-NULL
                TO (SF)-IMP-RIT-VIS-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMP-RIT-VIS
                TO (SF)-IMP-RIT-VIS(IX-TAB-TCL)
           END-IF
           IF TCL-IMPB-TFR-NULL = HIGH-VALUES
              MOVE TCL-IMPB-TFR-NULL
                TO (SF)-IMPB-TFR-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPB-TFR
                TO (SF)-IMPB-TFR(IX-TAB-TCL)
           END-IF
           IF TCL-IMPB-ACC-NULL = HIGH-VALUES
              MOVE TCL-IMPB-ACC-NULL
                TO (SF)-IMPB-ACC-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPB-ACC
                TO (SF)-IMPB-ACC(IX-TAB-TCL)
           END-IF
           IF TCL-IMPB-VIS-NULL = HIGH-VALUES
              MOVE TCL-IMPB-VIS-NULL
                TO (SF)-IMPB-VIS-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPB-VIS
                TO (SF)-IMPB-VIS(IX-TAB-TCL)
           END-IF
           IF TCL-IMP-RIMB-NULL = HIGH-VALUES
              MOVE TCL-IMP-RIMB-NULL
                TO (SF)-IMP-RIMB-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMP-RIMB
                TO (SF)-IMP-RIMB(IX-TAB-TCL)
           END-IF
           IF TCL-IMP-CORTVO-NULL = HIGH-VALUES
              MOVE TCL-IMP-CORTVO-NULL
                TO (SF)-IMP-CORTVO-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMP-CORTVO
                TO (SF)-IMP-CORTVO(IX-TAB-TCL)
           END-IF
           IF TCL-IMPB-IMPST-PRVR-NULL = HIGH-VALUES
              MOVE TCL-IMPB-IMPST-PRVR-NULL
                TO (SF)-IMPB-IMPST-PRVR-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPB-IMPST-PRVR
                TO (SF)-IMPB-IMPST-PRVR(IX-TAB-TCL)
           END-IF
           IF TCL-IMPST-PRVR-NULL = HIGH-VALUES
              MOVE TCL-IMPST-PRVR-NULL
                TO (SF)-IMPST-PRVR-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPST-PRVR
                TO (SF)-IMPST-PRVR(IX-TAB-TCL)
           END-IF
           IF TCL-IMPB-IMPST-252-NULL = HIGH-VALUES
              MOVE TCL-IMPB-IMPST-252-NULL
                TO (SF)-IMPB-IMPST-252-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPB-IMPST-252
                TO (SF)-IMPB-IMPST-252(IX-TAB-TCL)
           END-IF
           IF TCL-IMPST-252-NULL = HIGH-VALUES
              MOVE TCL-IMPST-252-NULL
                TO (SF)-IMPST-252-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPST-252
                TO (SF)-IMPST-252(IX-TAB-TCL)
           END-IF
           IF TCL-IMP-IS-NULL = HIGH-VALUES
              MOVE TCL-IMP-IS-NULL
                TO (SF)-IMP-IS-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMP-IS
                TO (SF)-IMP-IS(IX-TAB-TCL)
           END-IF
           IF TCL-IMP-DIR-LIQ-NULL = HIGH-VALUES
              MOVE TCL-IMP-DIR-LIQ-NULL
                TO (SF)-IMP-DIR-LIQ-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMP-DIR-LIQ
                TO (SF)-IMP-DIR-LIQ(IX-TAB-TCL)
           END-IF
           IF TCL-IMP-NET-LIQTO-NULL = HIGH-VALUES
              MOVE TCL-IMP-NET-LIQTO-NULL
                TO (SF)-IMP-NET-LIQTO-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMP-NET-LIQTO
                TO (SF)-IMP-NET-LIQTO(IX-TAB-TCL)
           END-IF
           IF TCL-IMP-EFFLQ-NULL = HIGH-VALUES
              MOVE TCL-IMP-EFFLQ-NULL
                TO (SF)-IMP-EFFLQ-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMP-EFFLQ
                TO (SF)-IMP-EFFLQ(IX-TAB-TCL)
           END-IF
           IF TCL-COD-DVS-NULL = HIGH-VALUES
              MOVE TCL-COD-DVS-NULL
                TO (SF)-COD-DVS-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-COD-DVS
                TO (SF)-COD-DVS(IX-TAB-TCL)
           END-IF
           IF TCL-TP-MEZ-PAG-ACCR-NULL = HIGH-VALUES
              MOVE TCL-TP-MEZ-PAG-ACCR-NULL
                TO (SF)-TP-MEZ-PAG-ACCR-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-TP-MEZ-PAG-ACCR
                TO (SF)-TP-MEZ-PAG-ACCR(IX-TAB-TCL)
           END-IF
           IF TCL-ESTR-CNT-CORR-ACCR-NULL = HIGH-VALUES
              MOVE TCL-ESTR-CNT-CORR-ACCR-NULL
                TO (SF)-ESTR-CNT-CORR-ACCR-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-ESTR-CNT-CORR-ACCR
                TO (SF)-ESTR-CNT-CORR-ACCR(IX-TAB-TCL)
           END-IF
           MOVE TCL-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-TCL)
           MOVE TCL-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-TCL)
           MOVE TCL-DS-VER
             TO (SF)-DS-VER(IX-TAB-TCL)
           MOVE TCL-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TCL)
           MOVE TCL-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-TCL)
           MOVE TCL-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-TCL)
           MOVE TCL-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-TCL)
           IF TCL-TP-STAT-TIT-NULL = HIGH-VALUES
              MOVE TCL-TP-STAT-TIT-NULL
                TO (SF)-TP-STAT-TIT-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-TP-STAT-TIT
                TO (SF)-TP-STAT-TIT(IX-TAB-TCL)
           END-IF
           IF TCL-IMPB-VIS-1382011-NULL = HIGH-VALUES
              MOVE TCL-IMPB-VIS-1382011-NULL
                TO (SF)-IMPB-VIS-1382011-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPB-VIS-1382011
                TO (SF)-IMPB-VIS-1382011(IX-TAB-TCL)
           END-IF
           IF TCL-IMPST-VIS-1382011-NULL = HIGH-VALUES
              MOVE TCL-IMPST-VIS-1382011-NULL
                TO (SF)-IMPST-VIS-1382011-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPST-VIS-1382011
                TO (SF)-IMPST-VIS-1382011(IX-TAB-TCL)
           END-IF
           IF TCL-IMPST-SOST-1382011-NULL = HIGH-VALUES
              MOVE TCL-IMPST-SOST-1382011-NULL
                TO (SF)-IMPST-SOST-1382011-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPST-SOST-1382011
                TO (SF)-IMPST-SOST-1382011(IX-TAB-TCL)
           END-IF
           IF TCL-IMP-INTR-RIT-PAG-NULL = HIGH-VALUES
              MOVE TCL-IMP-INTR-RIT-PAG-NULL
                TO (SF)-IMP-INTR-RIT-PAG-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMP-INTR-RIT-PAG
                TO (SF)-IMP-INTR-RIT-PAG(IX-TAB-TCL)
           END-IF
           IF TCL-IMPB-IS-NULL = HIGH-VALUES
              MOVE TCL-IMPB-IS-NULL
                TO (SF)-IMPB-IS-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPB-IS
                TO (SF)-IMPB-IS(IX-TAB-TCL)
           END-IF
           IF TCL-IMPB-IS-1382011-NULL = HIGH-VALUES
              MOVE TCL-IMPB-IS-1382011-NULL
                TO (SF)-IMPB-IS-1382011-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPB-IS-1382011
                TO (SF)-IMPB-IS-1382011(IX-TAB-TCL)
           END-IF
           IF TCL-IMPB-VIS-662014-NULL = HIGH-VALUES
              MOVE TCL-IMPB-VIS-662014-NULL
                TO (SF)-IMPB-VIS-662014-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPB-VIS-662014
                TO (SF)-IMPB-VIS-662014(IX-TAB-TCL)
           END-IF
           IF TCL-IMPST-VIS-662014-NULL = HIGH-VALUES
              MOVE TCL-IMPST-VIS-662014-NULL
                TO (SF)-IMPST-VIS-662014-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPST-VIS-662014
                TO (SF)-IMPST-VIS-662014(IX-TAB-TCL)
           END-IF
           IF TCL-IMPB-IS-662014-NULL = HIGH-VALUES
              MOVE TCL-IMPB-IS-662014-NULL
                TO (SF)-IMPB-IS-662014-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPB-IS-662014
                TO (SF)-IMPB-IS-662014(IX-TAB-TCL)
           END-IF
           IF TCL-IMPST-SOST-662014-NULL = HIGH-VALUES
              MOVE TCL-IMPST-SOST-662014-NULL
                TO (SF)-IMPST-SOST-662014-NULL(IX-TAB-TCL)
           ELSE
              MOVE TCL-IMPST-SOST-662014
                TO (SF)-IMPST-SOST-662014(IX-TAB-TCL)
           END-IF.
       VALORIZZA-OUTPUT-TCL-EX.
           EXIT.

       01  AREA-LDBV2271.
           03 AREA-LDBV2271-I.
              05 LDBV2271-ID-OGG                PIC S9(09) COMP-3.
              05 LDBV2271-TP-OGG                PIC  X(02).
              05 LDBV2271-TP-LIQ                PIC  X(02).
           03 AREA-LDBV2271-O.
              05 LDBV2271-IMP-LRD-LIQTO         PIC S9(12)V9(3) COMP-3.
              05 LDBV2271-IMP-LRD-LIQTO-NULL    REDEFINES
                 LDBV2271-IMP-LRD-LIQTO         PIC X(8).


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVQUE3
      *   ULTIMO AGG. 03 GIU 2019
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-QUE.
           MOVE QUE-ID-QUEST
             TO (SF)-ID-PTF(IX-TAB-QUE)
           MOVE QUE-ID-QUEST
             TO (SF)-ID-QUEST(IX-TAB-QUE)
           MOVE QUE-ID-RAPP-ANA
             TO (SF)-ID-RAPP-ANA(IX-TAB-QUE)
           MOVE QUE-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-QUE)
           IF QUE-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE QUE-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-QUE)
           ELSE
              MOVE QUE-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-QUE)
           END-IF
           MOVE QUE-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-QUE)
           MOVE QUE-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-QUE)
           MOVE QUE-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-QUE)
           MOVE QUE-COD-QUEST
             TO (SF)-COD-QUEST(IX-TAB-QUE)
           MOVE QUE-TP-QUEST
             TO (SF)-TP-QUEST(IX-TAB-QUE)
           IF QUE-FL-VST-MED-NULL = HIGH-VALUES
              MOVE QUE-FL-VST-MED-NULL
                TO (SF)-FL-VST-MED-NULL(IX-TAB-QUE)
           ELSE
              MOVE QUE-FL-VST-MED
                TO (SF)-FL-VST-MED(IX-TAB-QUE)
           END-IF
           IF QUE-FL-STAT-BUON-SAL-NULL = HIGH-VALUES
              MOVE QUE-FL-STAT-BUON-SAL-NULL
                TO (SF)-FL-STAT-BUON-SAL-NULL(IX-TAB-QUE)
           ELSE
              MOVE QUE-FL-STAT-BUON-SAL
                TO (SF)-FL-STAT-BUON-SAL(IX-TAB-QUE)
           END-IF
           MOVE QUE-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-QUE)
           MOVE QUE-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-QUE)
           MOVE QUE-DS-VER
             TO (SF)-DS-VER(IX-TAB-QUE)
           MOVE QUE-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-QUE)
           MOVE QUE-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-QUE)
           MOVE QUE-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-QUE)
           MOVE QUE-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-QUE)
           IF QUE-TP-ADEGZ-NULL = HIGH-VALUES
              MOVE QUE-TP-ADEGZ-NULL
                TO (SF)-TP-ADEGZ-NULL(IX-TAB-QUE)
           ELSE
              MOVE QUE-TP-ADEGZ
                TO (SF)-TP-ADEGZ(IX-TAB-QUE)
           END-IF.
       VALORIZZA-OUTPUT-QUE-EX.
           EXIT.

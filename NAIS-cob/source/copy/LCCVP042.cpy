      *------------------------------------------------------------*
      *   PORTAFOGLIO VITA - PROCESSO VENDITA                      *
      *   CHIAMATA  AL DISPATCHER PER OPERAZIONI DI SELECT O FETCH *
      *         E GESTIONE ERRORE P04  (TABELLA STAT-RICH-EST)     *
      *   ULTIMO AGG.  23 FEB 2011                                 *
      *------------------------------------------------------------*
       LETTURA-P04.

           IF IDSI0011-SELECT
              PERFORM SELECT-P04
                 THRU SELECT-P04-EX
           ELSE
              PERFORM FETCH-P04
                 THRU FETCH-P04-EX
           END-IF.

       LETTURA-P04-EX.
           EXIT.
      *----------------------------------------------------------------*
      *                         SELECT                                 *
      *----------------------------------------------------------------*
       SELECT-P04.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
              EVALUATE TRUE
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *--->       OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI   TO STAT-RICH-EST
                       MOVE 1                      TO IX-TAB-P04
                       PERFORM VALORIZZA-OUTPUT-P04
                          THRU VALORIZZA-OUTPUT-P04-EX

                  WHEN IDSO0011-NOT-FOUND
      *--->       CHIAVE NON TROVATA
                        MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                        MOVE 'SELECT-P04' TO IEAI9901-LABEL-ERR
                        MOVE '005017'     TO IEAI9901-COD-ERRORE
                        MOVE SPACES       TO IEAI9901-PARAMETRI-ERR
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                           THRU EX-S0300
                  WHEN OTHER
      *--->       ERRORE DI ACCESSO AL DB
                        MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                        MOVE 'SELECT-P04' TO IEAI9901-LABEL-ERR
                        MOVE '005016'     TO IEAI9901-COD-ERRORE
                        STRING 'STAT-RICH-EST'                ';'
                               IDSO0011-RETURN-CODE ';'
                               IDSO0011-SQLCODE
                        DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                        END-STRING
                        PERFORM S0300-RICERCA-GRAVITA-ERRORE
                           THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM          TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'SELECT-P04'    TO IEAI9901-LABEL-ERR
              MOVE '005016'        TO IEAI9901-COD-ERRORE
              STRING 'STAT-RICH-EST'                ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
              DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
              END-STRING
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       SELECT-P04-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         FETCH                                  *
      *----------------------------------------------------------------*
       FETCH-P04.

           SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
           SET  WCOM-OVERFLOW-NO                  TO TRUE.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR WCOM-OVERFLOW-YES

               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *-->                 CHIAVE NON TROVATA
                           IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM
                                TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'FETCH-P04'
                                TO IEAI9901-LABEL-ERR
                              MOVE '005017'
                                TO IEAI9901-COD-ERRORE
                              MOVE SPACES
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                           END-IF
                      WHEN IDSO0011-SUCCESSFUL-SQL
      *-->                 OPERAZIONE ESEGUITA CORRETTAMENTE
                           MOVE IDSO0011-BUFFER-DATI  TO STAT-RICH-EST
                           ADD  1                     TO IX-TAB-P04

      *-->  Se il contatore di lettura � maggiore dell' elemento MAX
      *-->  previsto per la TAB.LQUSIONE allora siamo in overflow
                           IF IX-TAB-P04 > (SF)-ELE-LIQ-MAX
                              SET WCOM-OVERFLOW-YES   TO TRUE
                           ELSE
                              PERFORM VALORIZZA-OUTPUT-P04
                                 THRU VALORIZZA-OUTPUT-P04-EX
                              SET IDSI0011-FETCH-NEXT TO TRUE
                           END-IF
                      WHEN OTHER
      *-->                 ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'FETCH-P04'
                             TO IEAI9901-LABEL-ERR
                           MOVE '005016'
                             TO IEAI9901-COD-ERRORE
                           STRING 'STAT-RICH-EST'              ';'
                                IDSO0011-RETURN-CODE ';'
                                IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      *-->        ERRORE DISPATCHER
                  MOVE WK-PGM
                    TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'FETCH-P04'
                    TO IEAI9901-LABEL-ERR
                  MOVE '005016'
                    TO IEAI9901-COD-ERRORE
                  STRING 'STAT-RICH-EST'               ';'
                        IDSO0011-RETURN-CODE ';'
                        IDSO0011-SQLCODE
                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

       FETCH-P04-EX.
           EXIT.

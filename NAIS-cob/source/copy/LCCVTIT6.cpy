      *----------------------------------------------------------------*
      *    COPY      ..... LCCVTIT6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO TITOLO CONTABILE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVTIT5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCVTIT1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-TIT-CONT.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE TIT-CONT

      *--> NOME TABELLA FISICA DB
           MOVE 'TIT-CONT'                  TO   WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF  NOT WTIT-ST-INV(IX-TAB-TIT)
           AND NOT WTIT-ST-CON(IX-TAB-TIT)
           AND     WTIT-ELE-TIT-MAX NOT = 0

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WTIT-ST-ADD(IX-TAB-TIT)

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK

                        MOVE S090-SEQ-TABELLA
                          TO WTIT-ID-PTF(IX-TAB-TIT)
                             TIT-ID-TIT-CONT

      *-->               PREPARAZIONE ED ESTRAZIONE OGGETTO PTF
                         PERFORM PREPARA-AREA-LCCS0234-TIT
                            THRU PREPARA-AREA-LCCS0234-TIT-EX
                         PERFORM CALL-LCCS0234
                            THRU CALL-LCCS0234-EX

                         IF IDSV0001-ESITO-OK
                            MOVE S234-ID-OGG-PTF-EOC
                              TO TIT-ID-OGG
                            MOVE WMOV-ID-PTF
                              TO TIT-ID-MOVI-CRZ
                         END-IF

      *-->               GESTIONE NUMERAZIONE QUIETANZA
                         IF IDSV0001-ESITO-OK
                            PERFORM GEST-NUM-QUIET
                               THRU GEST-NUM-QUIET-EX
                         END-IF

                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        DELETE
                  WHEN WTIT-ST-DEL(IX-TAB-TIT)

                     MOVE WTIT-ID-PTF(IX-TAB-TIT)
                       TO TIT-ID-TIT-CONT
                     MOVE WTIT-ID-OGG(IX-TAB-TIT)
                       TO TIT-ID-OGG
                     MOVE WMOV-ID-PTF
                       TO TIT-ID-MOVI-CRZ

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->        UPDATE
                  WHEN WTIT-ST-MOD(IX-TAB-TIT)

                     MOVE WTIT-ID-PTF(IX-TAB-TIT)
                       TO TIT-ID-TIT-CONT
                     MOVE WTIT-ID-OGG(IX-TAB-TIT)
                       TO TIT-ID-OGG
                     MOVE WMOV-ID-PTF
                       TO TIT-ID-MOVI-CRZ

      *-->           GESTIONE NUMERAZIONE QUIETANZA
                     IF IDSV0001-ESITO-OK
                        PERFORM GEST-NUM-QUIET
                           THRU GEST-NUM-QUIET-EX
                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN
                 PERFORM VAL-DCLGEN-TIT
                    THRU VAL-DCLGEN-TIT-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-TIT
                    THRU VALORIZZA-AREA-DSH-TIT-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-TIT-CONT-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-TIT.

      *--> NOME TABELLA FISICA DB
           MOVE 'TIT-CONT'                  TO   WK-TABELLA.

      *--> DCLGEN TABELLA
           MOVE TIT-CONT                    TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-TIT-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   PREPARA PER LA CALL ALL'LCCS0234 ESTRATTORE OGGETTO PTF
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234-TIT.

           MOVE WTIT-ID-OGG(IX-TAB-TIT)     TO S234-ID-OGG-EOC
           MOVE WTIT-TP-OGG(IX-TAB-TIT)     TO S234-TIPO-OGG-EOC.

       PREPARA-AREA-LCCS0234-TIT-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVTIT5.

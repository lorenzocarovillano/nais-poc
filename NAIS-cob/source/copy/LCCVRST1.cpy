      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA RIS_DI_TRCH
      *   ALIAS RST
      *   ULTIMO AGG. 09 LUG 2010
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-RIS-DI-TRCH PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-TP-CALC-RIS PIC X(2).
             07 (SF)-TP-CALC-RIS-NULL REDEFINES
                (SF)-TP-CALC-RIS   PIC X(2).
             07 (SF)-ULT-RM PIC S9(12)V9(3) COMP-3.
             07 (SF)-ULT-RM-NULL REDEFINES
                (SF)-ULT-RM   PIC X(8).
             07 (SF)-DT-CALC   PIC S9(8) COMP-3.
             07 (SF)-DT-CALC-NULL REDEFINES
                (SF)-DT-CALC   PIC X(5).
             07 (SF)-DT-ELAB   PIC S9(8) COMP-3.
             07 (SF)-DT-ELAB-NULL REDEFINES
                (SF)-DT-ELAB   PIC X(5).
             07 (SF)-RIS-BILA PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-BILA-NULL REDEFINES
                (SF)-RIS-BILA   PIC X(8).
             07 (SF)-RIS-MAT PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-MAT-NULL REDEFINES
                (SF)-RIS-MAT   PIC X(8).
             07 (SF)-INCR-X-RIVAL PIC S9(12)V9(3) COMP-3.
             07 (SF)-INCR-X-RIVAL-NULL REDEFINES
                (SF)-INCR-X-RIVAL   PIC X(8).
             07 (SF)-RPTO-PRE PIC S9(12)V9(3) COMP-3.
             07 (SF)-RPTO-PRE-NULL REDEFINES
                (SF)-RPTO-PRE   PIC X(8).
             07 (SF)-FRAZ-PRE-PP PIC S9(12)V9(3) COMP-3.
             07 (SF)-FRAZ-PRE-PP-NULL REDEFINES
                (SF)-FRAZ-PRE-PP   PIC X(8).
             07 (SF)-RIS-TOT PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-TOT-NULL REDEFINES
                (SF)-RIS-TOT   PIC X(8).
             07 (SF)-RIS-SPE PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-SPE-NULL REDEFINES
                (SF)-RIS-SPE   PIC X(8).
             07 (SF)-RIS-ABB PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-ABB-NULL REDEFINES
                (SF)-RIS-ABB   PIC X(8).
             07 (SF)-RIS-BNSFDT PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-BNSFDT-NULL REDEFINES
                (SF)-RIS-BNSFDT   PIC X(8).
             07 (SF)-RIS-SOPR PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-SOPR-NULL REDEFINES
                (SF)-RIS-SOPR   PIC X(8).
             07 (SF)-RIS-INTEG-BAS-TEC PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-INTEG-BAS-TEC-NULL REDEFINES
                (SF)-RIS-INTEG-BAS-TEC   PIC X(8).
             07 (SF)-RIS-INTEG-DECR-TS PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-INTEG-DECR-TS-NULL REDEFINES
                (SF)-RIS-INTEG-DECR-TS   PIC X(8).
             07 (SF)-RIS-GAR-CASO-MOR PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-GAR-CASO-MOR-NULL REDEFINES
                (SF)-RIS-GAR-CASO-MOR   PIC X(8).
             07 (SF)-RIS-ZIL PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-ZIL-NULL REDEFINES
                (SF)-RIS-ZIL   PIC X(8).
             07 (SF)-RIS-FAIVL PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-FAIVL-NULL REDEFINES
                (SF)-RIS-FAIVL   PIC X(8).
             07 (SF)-RIS-COS-AMMTZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-COS-AMMTZ-NULL REDEFINES
                (SF)-RIS-COS-AMMTZ   PIC X(8).
             07 (SF)-RIS-SPE-FAIVL PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-SPE-FAIVL-NULL REDEFINES
                (SF)-RIS-SPE-FAIVL   PIC X(8).
             07 (SF)-RIS-PREST-FAIVL PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-PREST-FAIVL-NULL REDEFINES
                (SF)-RIS-PREST-FAIVL   PIC X(8).
             07 (SF)-RIS-COMPON-ASSVA PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-COMPON-ASSVA-NULL REDEFINES
                (SF)-RIS-COMPON-ASSVA   PIC X(8).
             07 (SF)-ULT-COEFF-RIS PIC S9(5)V9(9) COMP-3.
             07 (SF)-ULT-COEFF-RIS-NULL REDEFINES
                (SF)-ULT-COEFF-RIS   PIC X(8).
             07 (SF)-ULT-COEFF-AGG-RIS PIC S9(5)V9(9) COMP-3.
             07 (SF)-ULT-COEFF-AGG-RIS-NULL REDEFINES
                (SF)-ULT-COEFF-AGG-RIS   PIC X(8).
             07 (SF)-RIS-ACQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-ACQ-NULL REDEFINES
                (SF)-RIS-ACQ   PIC X(8).
             07 (SF)-RIS-UTI PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-UTI-NULL REDEFINES
                (SF)-RIS-UTI   PIC X(8).
             07 (SF)-RIS-MAT-EFF PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-MAT-EFF-NULL REDEFINES
                (SF)-RIS-MAT-EFF   PIC X(8).
             07 (SF)-RIS-RISTORNI-CAP PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-RISTORNI-CAP-NULL REDEFINES
                (SF)-RIS-RISTORNI-CAP   PIC X(8).
             07 (SF)-RIS-TRM-BNS PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-TRM-BNS-NULL REDEFINES
                (SF)-RIS-TRM-BNS   PIC X(8).
             07 (SF)-RIS-BNSRIC PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-BNSRIC-NULL REDEFINES
                (SF)-RIS-BNSRIC   PIC X(8).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-ID-TRCH-DI-GAR PIC S9(9)     COMP-3.
             07 (SF)-COD-FND PIC X(12).
             07 (SF)-COD-FND-NULL REDEFINES
                (SF)-COD-FND   PIC X(12).
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-ID-ADES PIC S9(9)     COMP-3.
             07 (SF)-ID-GAR PIC S9(9)     COMP-3.
             07 (SF)-RIS-MIN-GARTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-MIN-GARTO-NULL REDEFINES
                (SF)-RIS-MIN-GARTO   PIC X(8).
             07 (SF)-RIS-RSH-DFLT PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-RSH-DFLT-NULL REDEFINES
                (SF)-RIS-RSH-DFLT   PIC X(8).
             07 (SF)-RIS-MOVI-NON-INVES PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-MOVI-NON-INVES-NULL REDEFINES
                (SF)-RIS-MOVI-NON-INVES   PIC X(8).

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA GAR_LIQ
      *   ALIAS GRL
      *   ULTIMO AGG. 02 SET 2008
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-GAR-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-GAR PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-DT-SIN-1O-ASSTO   PIC S9(8) COMP-3.
             07 (SF)-DT-SIN-1O-ASSTO-NULL REDEFINES
                (SF)-DT-SIN-1O-ASSTO   PIC X(5).
             07 (SF)-CAU-SIN-1O-ASSTO PIC X(2).
             07 (SF)-CAU-SIN-1O-ASSTO-NULL REDEFINES
                (SF)-CAU-SIN-1O-ASSTO   PIC X(2).
             07 (SF)-TP-SIN-1O-ASSTO PIC X(2).
             07 (SF)-TP-SIN-1O-ASSTO-NULL REDEFINES
                (SF)-TP-SIN-1O-ASSTO   PIC X(2).
             07 (SF)-DT-SIN-2O-ASSTO   PIC S9(8) COMP-3.
             07 (SF)-DT-SIN-2O-ASSTO-NULL REDEFINES
                (SF)-DT-SIN-2O-ASSTO   PIC X(5).
             07 (SF)-CAU-SIN-2O-ASSTO PIC X(2).
             07 (SF)-CAU-SIN-2O-ASSTO-NULL REDEFINES
                (SF)-CAU-SIN-2O-ASSTO   PIC X(2).
             07 (SF)-TP-SIN-2O-ASSTO PIC X(2).
             07 (SF)-TP-SIN-2O-ASSTO-NULL REDEFINES
                (SF)-TP-SIN-2O-ASSTO   PIC X(2).
             07 (SF)-DT-SIN-3O-ASSTO   PIC S9(8) COMP-3.
             07 (SF)-DT-SIN-3O-ASSTO-NULL REDEFINES
                (SF)-DT-SIN-3O-ASSTO   PIC X(5).
             07 (SF)-CAU-SIN-3O-ASSTO PIC X(2).
             07 (SF)-CAU-SIN-3O-ASSTO-NULL REDEFINES
                (SF)-CAU-SIN-3O-ASSTO   PIC X(2).
             07 (SF)-TP-SIN-3O-ASSTO PIC X(2).
             07 (SF)-TP-SIN-3O-ASSTO-NULL REDEFINES
                (SF)-TP-SIN-3O-ASSTO   PIC X(2).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).

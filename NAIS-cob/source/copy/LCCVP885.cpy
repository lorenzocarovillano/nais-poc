
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVP885
      *   ULTIMO AGG. 17 FEB 2015
      *------------------------------------------------------------

       VAL-DCLGEN-P88.
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-P88)
              TO P88-COD-COMP-ANIA
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P88) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P88)
              TO P88-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-P88)
              TO P88-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-P88) NOT NUMERIC
              MOVE 0 TO P88-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-P88)
              TO P88-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-P88) NOT NUMERIC
              MOVE 0 TO P88-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-P88)
              TO P88-DT-END-EFF
           END-IF
           MOVE (SF)-TP-SERV-VAL(IX-TAB-P88)
              TO P88-TP-SERV-VAL
           MOVE (SF)-TP-OGG(IX-TAB-P88)
              TO P88-TP-OGG
           IF (SF)-DS-RIGA(IX-TAB-P88) NOT NUMERIC
              MOVE 0 TO P88-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-P88)
              TO P88-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-P88)
              TO P88-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-P88) NOT NUMERIC
              MOVE 0 TO P88-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-P88)
              TO P88-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-P88) NOT NUMERIC
              MOVE 0 TO P88-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-P88)
              TO P88-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-P88) NOT NUMERIC
              MOVE 0 TO P88-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-P88)
              TO P88-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-P88)
              TO P88-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-P88)
              TO P88-DS-STATO-ELAB
           IF (SF)-FL-ALL-FND-NULL(IX-TAB-P88) = HIGH-VALUES
              MOVE (SF)-FL-ALL-FND-NULL(IX-TAB-P88)
              TO P88-FL-ALL-FND-NULL
           ELSE
              MOVE (SF)-FL-ALL-FND(IX-TAB-P88)
              TO P88-FL-ALL-FND
           END-IF.
       VAL-DCLGEN-P88-EX.
           EXIT.

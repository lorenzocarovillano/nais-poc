      *----------------------------------------------------------------*
      *    DISPLAY TABELLA TITOLO CONTABILE
      *    N.B Per utilizzare la copy bisogna includere la copy
      *    Infrastrutturale IDSV8888
      *----------------------------------------------------------------*
       DISPLAY-TAB-TIT.

           DISPLAY 'WTIT-STATUS             (' IX-TAB-TIT ') = '
                    WTIT-STATUS               (IX-TAB-TIT)
           DISPLAY 'WTIT-ID-PTF             (' IX-TAB-TIT ') = '
                    WTIT-ID-PTF               (IX-TAB-TIT)
           DISPLAY 'WTIT-ID-TIT-CONT        (' IX-TAB-TIT ') = '
                    WTIT-ID-TIT-CONT          (IX-TAB-TIT)
           DISPLAY 'WTIT-ID-OGG             (' IX-TAB-TIT ') = '
                    WTIT-ID-OGG               (IX-TAB-TIT)
           DISPLAY 'WTIT-TP-OGG             (' IX-TAB-TIT ') = '
                    WTIT-TP-OGG               (IX-TAB-TIT)
           DISPLAY 'WTIT-IB-RICH            (' IX-TAB-TIT ') = '
                    WTIT-IB-RICH              (IX-TAB-TIT)
           DISPLAY 'WTIT-ID-MOVI-CRZ        (' IX-TAB-TIT ') = '
                    WTIT-ID-MOVI-CRZ          (IX-TAB-TIT)
           DISPLAY 'WTIT-ID-MOVI-CHIU       (' IX-TAB-TIT ') = '
                    WTIT-ID-MOVI-CHIU         (IX-TAB-TIT)
           DISPLAY 'WTIT-DT-INI-EFF         (' IX-TAB-TIT ') = '
                    WTIT-DT-INI-EFF           (IX-TAB-TIT)
           DISPLAY 'WTIT-DT-END-EFF         (' IX-TAB-TIT ') = '
                    WTIT-DT-END-EFF           (IX-TAB-TIT)
           DISPLAY 'WTIT-COD-COMP-ANIA      (' IX-TAB-TIT ') = '
                    WTIT-COD-COMP-ANIA        (IX-TAB-TIT)
           DISPLAY 'WTIT-TP-TIT             (' IX-TAB-TIT ') = '
                    WTIT-TP-TIT               (IX-TAB-TIT)
           DISPLAY 'WTIT-PROG-TIT           (' IX-TAB-TIT ') = '
                    WTIT-PROG-TIT             (IX-TAB-TIT)
           DISPLAY 'WTIT-TP-PRE-TIT         (' IX-TAB-TIT ') = '
                    WTIT-TP-PRE-TIT           (IX-TAB-TIT)
           DISPLAY 'WTIT-TP-STAT-TIT        (' IX-TAB-TIT ') = '
                    WTIT-TP-STAT-TIT          (IX-TAB-TIT)
           DISPLAY 'WTIT-DT-INI-COP         (' IX-TAB-TIT ') = '
                    WTIT-DT-INI-COP           (IX-TAB-TIT)
           DISPLAY 'WTIT-DT-END-COP         (' IX-TAB-TIT ') = '
                    WTIT-DT-END-COP           (IX-TAB-TIT)
           DISPLAY 'WTIT-IMP-PAG            (' IX-TAB-TIT ') = '
                    WTIT-IMP-PAG              (IX-TAB-TIT)
           DISPLAY 'WTIT-FL-SOLL            (' IX-TAB-TIT ') = '
                    WTIT-FL-SOLL              (IX-TAB-TIT)
           DISPLAY 'WTIT-FRAZ               (' IX-TAB-TIT ') = '
                    WTIT-FRAZ                 (IX-TAB-TIT)
           DISPLAY 'WTIT-DT-APPLZ-MORA      (' IX-TAB-TIT ') = '
                    WTIT-DT-APPLZ-MORA        (IX-TAB-TIT)
           DISPLAY 'WTIT-FL-MORA            (' IX-TAB-TIT ') = '
                    WTIT-FL-MORA              (IX-TAB-TIT)
           DISPLAY 'WTIT-ID-RAPP-RETE       (' IX-TAB-TIT ') = '
                    WTIT-ID-RAPP-RETE         (IX-TAB-TIT)
           DISPLAY 'WTIT-ID-RAPP-ANA        (' IX-TAB-TIT ') = '
                    WTIT-ID-RAPP-ANA          (IX-TAB-TIT)
           DISPLAY 'WTIT-COD-DVS            (' IX-TAB-TIT ') = '
                    WTIT-COD-DVS              (IX-TAB-TIT)
           DISPLAY 'WTIT-DT-EMIS-TIT        (' IX-TAB-TIT ') = '
                    WTIT-DT-EMIS-TIT          (IX-TAB-TIT)
           DISPLAY 'WTIT-DT-ESI-TIT         (' IX-TAB-TIT ') = '
                    WTIT-DT-ESI-TIT           (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-PRE-NET        (' IX-TAB-TIT ') = '
                    WTIT-TOT-PRE-NET          (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-INTR-FRAZ      (' IX-TAB-TIT ') = '
                    WTIT-TOT-INTR-FRAZ        (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-INTR-MORA      (' IX-TAB-TIT ') = '
                    WTIT-TOT-INTR-MORA        (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-INTR-PREST     (' IX-TAB-TIT ') = '
                    WTIT-TOT-INTR-PREST       (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-INTR-RETDT     (' IX-TAB-TIT ') = '
                    WTIT-TOT-INTR-RETDT       (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-INTR-RIAT      (' IX-TAB-TIT ') = '
                    WTIT-TOT-INTR-RIAT        (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-DIR            (' IX-TAB-TIT ') = '
                    WTIT-TOT-DIR              (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-SPE-MED        (' IX-TAB-TIT ') = '
                    WTIT-TOT-SPE-MED          (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-TAX            (' IX-TAB-TIT ') = '
                    WTIT-TOT-TAX              (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-SOPR-SAN       (' IX-TAB-TIT ') = '
                    WTIT-TOT-SOPR-SAN         (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-SOPR-TEC       (' IX-TAB-TIT ') = '
                    WTIT-TOT-SOPR-TEC         (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-SOPR-SPO       (' IX-TAB-TIT ') = '
                    WTIT-TOT-SOPR-SPO         (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-SOPR-PROF      (' IX-TAB-TIT ') = '
                    WTIT-TOT-SOPR-PROF        (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-SOPR-ALT       (' IX-TAB-TIT ') = '
                    WTIT-TOT-SOPR-ALT         (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-PRE-TOT        (' IX-TAB-TIT ') = '
                    WTIT-TOT-PRE-TOT          (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-PRE-PP-IAS     (' IX-TAB-TIT ') = '
                    WTIT-TOT-PRE-PP-IAS       (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-CAR-ACQ        (' IX-TAB-TIT ') = '
                    WTIT-TOT-CAR-ACQ          (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-CAR-GEST       (' IX-TAB-TIT ') = '
                    WTIT-TOT-CAR-GEST         (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-CAR-INC        (' IX-TAB-TIT ') = '
                    WTIT-TOT-CAR-INC          (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-PRE-SOLO-RSH   (' IX-TAB-TIT ') = '
                    WTIT-TOT-PRE-SOLO-RSH     (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-PROV-ACQ-1AA   (' IX-TAB-TIT ') = '
                    WTIT-TOT-PROV-ACQ-1AA     (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-PROV-ACQ-2AA   (' IX-TAB-TIT ') = '
                    WTIT-TOT-PROV-ACQ-2AA     (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-PROV-RICOR     (' IX-TAB-TIT ') = '
                    WTIT-TOT-PROV-RICOR       (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-PROV-INC       (' IX-TAB-TIT ') = '
                    WTIT-TOT-PROV-INC         (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-PROV-DA-REC    (' IX-TAB-TIT ') = '
                    WTIT-TOT-PROV-DA-REC      (IX-TAB-TIT)
           DISPLAY 'WTIT-IMP-AZ             (' IX-TAB-TIT ') = '
                    WTIT-IMP-AZ               (IX-TAB-TIT)
           DISPLAY 'WTIT-IMP-ADER           (' IX-TAB-TIT ') = '
                    WTIT-IMP-ADER             (IX-TAB-TIT)
           DISPLAY 'WTIT-IMP-TFR            (' IX-TAB-TIT ') = '
                    WTIT-IMP-TFR              (IX-TAB-TIT)
           DISPLAY 'WTIT-IMP-VOLO           (' IX-TAB-TIT ') = '
                    WTIT-IMP-VOLO             (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-MANFEE-ANTIC   (' IX-TAB-TIT ') = '
                    WTIT-TOT-MANFEE-ANTIC     (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-MANFEE-RICOR   (' IX-TAB-TIT ') = '
                    WTIT-TOT-MANFEE-RICOR     (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-MANFEE-REC     (' IX-TAB-TIT ') = '
                    WTIT-TOT-MANFEE-REC       (IX-TAB-TIT)
           DISPLAY 'WTIT-TP-MEZ-PAG-ADD     (' IX-TAB-TIT ') = '
                    WTIT-TP-MEZ-PAG-ADD       (IX-TAB-TIT)
           DISPLAY 'WTIT-ESTR-CNT-CORR-ADD  (' IX-TAB-TIT ') = '
                    WTIT-ESTR-CNT-CORR-ADD    (IX-TAB-TIT)
           DISPLAY 'WTIT-DT-VLT             (' IX-TAB-TIT ') = '
                    WTIT-DT-VLT               (IX-TAB-TIT)
           DISPLAY 'WTIT-FL-FORZ-DT-VLT     (' IX-TAB-TIT ') = '
                    WTIT-FL-FORZ-DT-VLT       (IX-TAB-TIT)
           DISPLAY 'WTIT-DT-CAMBIO-VLT      (' IX-TAB-TIT ') = '
                    WTIT-DT-CAMBIO-VLT        (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-SPE-AGE        (' IX-TAB-TIT ') = '
                    WTIT-TOT-SPE-AGE          (IX-TAB-TIT)
           DISPLAY 'WTIT-TOT-CAR-IAS        (' IX-TAB-TIT ') = '
                    WTIT-TOT-CAR-IAS          (IX-TAB-TIT)
           DISPLAY 'WTIT-NUM-RAT-ACCORPATE  (' IX-TAB-TIT ') = '
                    WTIT-NUM-RAT-ACCORPATE    (IX-TAB-TIT)
           DISPLAY 'WTIT-DS-RIGA            (' IX-TAB-TIT ') = '
                    WTIT-DS-RIGA              (IX-TAB-TIT)
           DISPLAY 'WTIT-DS-OPER-SQL        (' IX-TAB-TIT ') = '
                    WTIT-DS-OPER-SQL          (IX-TAB-TIT)
           DISPLAY 'WTIT-DS-VER             (' IX-TAB-TIT ') = '
                    WTIT-DS-VER               (IX-TAB-TIT)
           DISPLAY 'WTIT-DS-TS-INI-CPTZ     (' IX-TAB-TIT ') = '
                    WTIT-DS-TS-INI-CPTZ       (IX-TAB-TIT)
           DISPLAY 'WTIT-DS-TS-END-CPTZ     (' IX-TAB-TIT ') = '
                    WTIT-DS-TS-END-CPTZ       (IX-TAB-TIT)
           DISPLAY 'WTIT-DS-UTENTE          (' IX-TAB-TIT ') = '
                    WTIT-DS-UTENTE            (IX-TAB-TIT)
           DISPLAY 'WTIT-DS-STATO-ELAB      (' IX-TAB-TIT ') = '
                    WTIT-DS-STATO-ELAB        (IX-TAB-TIT)
           DISPLAY 'WTIT-FL-TIT-DA-REINVST  (' IX-TAB-TIT ') = '
                    WTIT-FL-TIT-DA-REINVST    (IX-TAB-TIT)
           DISPLAY 'WTIT-DT-RICH-ADD-RID    (' IX-TAB-TIT ') = '
                    WTIT-DT-RICH-ADD-RID      (IX-TAB-TIT)
           DISPLAY 'WTIT-TP-ESI-RID         (' IX-TAB-TIT ') = '
                    WTIT-TP-ESI-RID           (IX-TAB-TIT).
           DISPLAY 'WTIT-COD-IBAN           (' IX-TAB-TIT ') = '
                    WTIT-COD-IBAN             (IX-TAB-TIT).
           DISPLAY 'WTIT-IMP-TRASFE         (' IX-TAB-TIT ') = '
                    WTIT-IMP-TRASFE           (IX-TAB-TIT).
           DISPLAY 'WTIT-IMP-TFR-STRC       (' IX-TAB-TIT ') = '
                    WTIT-IMP-TFR-STRC         (IX-TAB-TIT).
           DISPLAY 'WTIT-DT-CERT-FISC       (' IX-TAB-TIT ') = '
                    WTIT-DT-CERT-FISC         (IX-TAB-TIT).
           DISPLAY 'WTIT-TP-CAUS-STOR       (' IX-TAB-TIT ') = '
                    WTIT-TP-CAUS-STOR         (IX-TAB-TIT).
           DISPLAY 'WTIT-TP-CAUS-DISP-STOR  (' IX-TAB-TIT ') = '
                    WTIT-TP-CAUS-DISP-STOR    (IX-TAB-TIT).

       DISPLAY-TAB-TIT-EX.

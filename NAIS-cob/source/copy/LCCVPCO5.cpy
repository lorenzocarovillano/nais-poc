
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVPCO5
      *   ULTIMO AGG. 13 NOV 2018
      *------------------------------------------------------------

       VAL-DCLGEN-PCO.
           MOVE (SF)-COD-COMP-ANIA
              TO PCO-COD-COMP-ANIA
           IF (SF)-COD-TRAT-CIRT-NULL = HIGH-VALUES
              MOVE (SF)-COD-TRAT-CIRT-NULL
              TO PCO-COD-TRAT-CIRT-NULL
           ELSE
              MOVE (SF)-COD-TRAT-CIRT
              TO PCO-COD-TRAT-CIRT
           END-IF
           IF (SF)-LIM-VLTR-NULL = HIGH-VALUES
              MOVE (SF)-LIM-VLTR-NULL
              TO PCO-LIM-VLTR-NULL
           ELSE
              MOVE (SF)-LIM-VLTR
              TO PCO-LIM-VLTR
           END-IF
           IF (SF)-TP-RAT-PERF-NULL = HIGH-VALUES
              MOVE (SF)-TP-RAT-PERF-NULL
              TO PCO-TP-RAT-PERF-NULL
           ELSE
              MOVE (SF)-TP-RAT-PERF
              TO PCO-TP-RAT-PERF
           END-IF
           MOVE (SF)-TP-LIV-GENZ-TIT
              TO PCO-TP-LIV-GENZ-TIT
           IF (SF)-ARROT-PRE-NULL = HIGH-VALUES
              MOVE (SF)-ARROT-PRE-NULL
              TO PCO-ARROT-PRE-NULL
           ELSE
              MOVE (SF)-ARROT-PRE
              TO PCO-ARROT-PRE
           END-IF
           IF (SF)-DT-CONT-NULL = HIGH-VALUES
              MOVE (SF)-DT-CONT-NULL
              TO PCO-DT-CONT-NULL
           ELSE
             IF (SF)-DT-CONT = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-CONT-NULL
             ELSE
              MOVE (SF)-DT-CONT
              TO PCO-DT-CONT
             END-IF
           END-IF
           IF (SF)-DT-ULT-RIVAL-IN-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-RIVAL-IN-NULL
              TO PCO-DT-ULT-RIVAL-IN-NULL
           ELSE
             IF (SF)-DT-ULT-RIVAL-IN = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-RIVAL-IN-NULL
             ELSE
              MOVE (SF)-DT-ULT-RIVAL-IN
              TO PCO-DT-ULT-RIVAL-IN
             END-IF
           END-IF
           IF (SF)-DT-ULT-QTZO-IN-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-QTZO-IN-NULL
              TO PCO-DT-ULT-QTZO-IN-NULL
           ELSE
             IF (SF)-DT-ULT-QTZO-IN = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-QTZO-IN-NULL
             ELSE
              MOVE (SF)-DT-ULT-QTZO-IN
              TO PCO-DT-ULT-QTZO-IN
             END-IF
           END-IF
           IF (SF)-DT-ULT-RICL-RIASS-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-RICL-RIASS-NULL
              TO PCO-DT-ULT-RICL-RIASS-NULL
           ELSE
             IF (SF)-DT-ULT-RICL-RIASS = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-RICL-RIASS-NULL
             ELSE
              MOVE (SF)-DT-ULT-RICL-RIASS
              TO PCO-DT-ULT-RICL-RIASS
             END-IF
           END-IF
           IF (SF)-DT-ULT-TABUL-RIASS-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-TABUL-RIASS-NULL
              TO PCO-DT-ULT-TABUL-RIASS-NULL
           ELSE
             IF (SF)-DT-ULT-TABUL-RIASS = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-TABUL-RIASS-NULL
             ELSE
              MOVE (SF)-DT-ULT-TABUL-RIASS
              TO PCO-DT-ULT-TABUL-RIASS
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-EMES-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-EMES-NULL
              TO PCO-DT-ULT-BOLL-EMES-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-EMES = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-EMES-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-EMES
              TO PCO-DT-ULT-BOLL-EMES
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-STOR-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-STOR-NULL
              TO PCO-DT-ULT-BOLL-STOR-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-STOR = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-STOR-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-STOR
              TO PCO-DT-ULT-BOLL-STOR
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-LIQ-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-LIQ-NULL
              TO PCO-DT-ULT-BOLL-LIQ-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-LIQ = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-LIQ-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-LIQ
              TO PCO-DT-ULT-BOLL-LIQ
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-RIAT-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-RIAT-NULL
              TO PCO-DT-ULT-BOLL-RIAT-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-RIAT = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-RIAT-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-RIAT
              TO PCO-DT-ULT-BOLL-RIAT
             END-IF
           END-IF
           IF (SF)-DT-ULTELRISCPAR-PR-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTELRISCPAR-PR-NULL
              TO PCO-DT-ULTELRISCPAR-PR-NULL
           ELSE
             IF (SF)-DT-ULTELRISCPAR-PR = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTELRISCPAR-PR-NULL
             ELSE
              MOVE (SF)-DT-ULTELRISCPAR-PR
              TO PCO-DT-ULTELRISCPAR-PR
             END-IF
           END-IF
           IF (SF)-DT-ULTC-IS-IN-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTC-IS-IN-NULL
              TO PCO-DT-ULTC-IS-IN-NULL
           ELSE
             IF (SF)-DT-ULTC-IS-IN = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTC-IS-IN-NULL
             ELSE
              MOVE (SF)-DT-ULTC-IS-IN
              TO PCO-DT-ULTC-IS-IN
             END-IF
           END-IF
           IF (SF)-DT-ULT-RICL-PRE-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-RICL-PRE-NULL
              TO PCO-DT-ULT-RICL-PRE-NULL
           ELSE
             IF (SF)-DT-ULT-RICL-PRE = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-RICL-PRE-NULL
             ELSE
              MOVE (SF)-DT-ULT-RICL-PRE
              TO PCO-DT-ULT-RICL-PRE
             END-IF
           END-IF
           IF (SF)-DT-ULTC-MARSOL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTC-MARSOL-NULL
              TO PCO-DT-ULTC-MARSOL-NULL
           ELSE
             IF (SF)-DT-ULTC-MARSOL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTC-MARSOL-NULL
             ELSE
              MOVE (SF)-DT-ULTC-MARSOL
              TO PCO-DT-ULTC-MARSOL
             END-IF
           END-IF
           IF (SF)-DT-ULTC-RB-IN-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTC-RB-IN-NULL
              TO PCO-DT-ULTC-RB-IN-NULL
           ELSE
             IF (SF)-DT-ULTC-RB-IN = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTC-RB-IN-NULL
             ELSE
              MOVE (SF)-DT-ULTC-RB-IN
              TO PCO-DT-ULTC-RB-IN
             END-IF
           END-IF
           IF (SF)-PC-PROV-1AA-ACQ-NULL = HIGH-VALUES
              MOVE (SF)-PC-PROV-1AA-ACQ-NULL
              TO PCO-PC-PROV-1AA-ACQ-NULL
           ELSE
              MOVE (SF)-PC-PROV-1AA-ACQ
              TO PCO-PC-PROV-1AA-ACQ
           END-IF
           IF (SF)-MOD-INTR-PREST-NULL = HIGH-VALUES
              MOVE (SF)-MOD-INTR-PREST-NULL
              TO PCO-MOD-INTR-PREST-NULL
           ELSE
              MOVE (SF)-MOD-INTR-PREST
              TO PCO-MOD-INTR-PREST
           END-IF
           IF (SF)-GG-MAX-REC-PROV-NULL = HIGH-VALUES
              MOVE (SF)-GG-MAX-REC-PROV-NULL
              TO PCO-GG-MAX-REC-PROV-NULL
           ELSE
              MOVE (SF)-GG-MAX-REC-PROV
              TO PCO-GG-MAX-REC-PROV
           END-IF
           IF (SF)-DT-ULTGZ-TRCH-E-IN-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTGZ-TRCH-E-IN-NULL
              TO PCO-DT-ULTGZ-TRCH-E-IN-NULL
           ELSE
             IF (SF)-DT-ULTGZ-TRCH-E-IN = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTGZ-TRCH-E-IN-NULL
             ELSE
              MOVE (SF)-DT-ULTGZ-TRCH-E-IN
              TO PCO-DT-ULTGZ-TRCH-E-IN
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-SNDEN-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-SNDEN-NULL
              TO PCO-DT-ULT-BOLL-SNDEN-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-SNDEN = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-SNDEN-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-SNDEN
              TO PCO-DT-ULT-BOLL-SNDEN
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-SNDNLQ-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-SNDNLQ-NULL
              TO PCO-DT-ULT-BOLL-SNDNLQ-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-SNDNLQ = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-SNDNLQ-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-SNDNLQ
              TO PCO-DT-ULT-BOLL-SNDNLQ
             END-IF
           END-IF
           IF (SF)-DT-ULTSC-ELAB-IN-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTSC-ELAB-IN-NULL
              TO PCO-DT-ULTSC-ELAB-IN-NULL
           ELSE
             IF (SF)-DT-ULTSC-ELAB-IN = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTSC-ELAB-IN-NULL
             ELSE
              MOVE (SF)-DT-ULTSC-ELAB-IN
              TO PCO-DT-ULTSC-ELAB-IN
             END-IF
           END-IF
           IF (SF)-DT-ULTSC-OPZ-IN-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTSC-OPZ-IN-NULL
              TO PCO-DT-ULTSC-OPZ-IN-NULL
           ELSE
             IF (SF)-DT-ULTSC-OPZ-IN = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTSC-OPZ-IN-NULL
             ELSE
              MOVE (SF)-DT-ULTSC-OPZ-IN
              TO PCO-DT-ULTSC-OPZ-IN
             END-IF
           END-IF
           IF (SF)-DT-ULTC-BNSRIC-IN-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTC-BNSRIC-IN-NULL
              TO PCO-DT-ULTC-BNSRIC-IN-NULL
           ELSE
             IF (SF)-DT-ULTC-BNSRIC-IN = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTC-BNSRIC-IN-NULL
             ELSE
              MOVE (SF)-DT-ULTC-BNSRIC-IN
              TO PCO-DT-ULTC-BNSRIC-IN
             END-IF
           END-IF
           IF (SF)-DT-ULTC-BNSFDT-IN-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTC-BNSFDT-IN-NULL
              TO PCO-DT-ULTC-BNSFDT-IN-NULL
           ELSE
             IF (SF)-DT-ULTC-BNSFDT-IN = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTC-BNSFDT-IN-NULL
             ELSE
              MOVE (SF)-DT-ULTC-BNSFDT-IN
              TO PCO-DT-ULTC-BNSFDT-IN
             END-IF
           END-IF
           IF (SF)-DT-ULT-RINN-GARAC-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-RINN-GARAC-NULL
              TO PCO-DT-ULT-RINN-GARAC-NULL
           ELSE
             IF (SF)-DT-ULT-RINN-GARAC = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-RINN-GARAC-NULL
             ELSE
              MOVE (SF)-DT-ULT-RINN-GARAC
              TO PCO-DT-ULT-RINN-GARAC
             END-IF
           END-IF
           IF (SF)-DT-ULTGZ-CED-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTGZ-CED-NULL
              TO PCO-DT-ULTGZ-CED-NULL
           ELSE
             IF (SF)-DT-ULTGZ-CED = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTGZ-CED-NULL
             ELSE
              MOVE (SF)-DT-ULTGZ-CED
              TO PCO-DT-ULTGZ-CED
             END-IF
           END-IF
           IF (SF)-DT-ULT-ELAB-PRLCOS-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ELAB-PRLCOS-NULL
              TO PCO-DT-ULT-ELAB-PRLCOS-NULL
           ELSE
             IF (SF)-DT-ULT-ELAB-PRLCOS = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ELAB-PRLCOS-NULL
             ELSE
              MOVE (SF)-DT-ULT-ELAB-PRLCOS
              TO PCO-DT-ULT-ELAB-PRLCOS
             END-IF
           END-IF
           IF (SF)-DT-ULT-RINN-COLL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-RINN-COLL-NULL
              TO PCO-DT-ULT-RINN-COLL-NULL
           ELSE
             IF (SF)-DT-ULT-RINN-COLL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-RINN-COLL-NULL
             ELSE
              MOVE (SF)-DT-ULT-RINN-COLL
              TO PCO-DT-ULT-RINN-COLL
             END-IF
           END-IF
           IF (SF)-FL-RVC-PERF-NULL = HIGH-VALUES
              MOVE (SF)-FL-RVC-PERF-NULL
              TO PCO-FL-RVC-PERF-NULL
           ELSE
              MOVE (SF)-FL-RVC-PERF
              TO PCO-FL-RVC-PERF
           END-IF
           IF (SF)-FL-RCS-POLI-NOPERF-NULL = HIGH-VALUES
              MOVE (SF)-FL-RCS-POLI-NOPERF-NULL
              TO PCO-FL-RCS-POLI-NOPERF-NULL
           ELSE
              MOVE (SF)-FL-RCS-POLI-NOPERF
              TO PCO-FL-RCS-POLI-NOPERF
           END-IF
           IF (SF)-FL-GEST-PLUSV-NULL = HIGH-VALUES
              MOVE (SF)-FL-GEST-PLUSV-NULL
              TO PCO-FL-GEST-PLUSV-NULL
           ELSE
              MOVE (SF)-FL-GEST-PLUSV
              TO PCO-FL-GEST-PLUSV
           END-IF
           IF (SF)-DT-ULT-RIVAL-CL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-RIVAL-CL-NULL
              TO PCO-DT-ULT-RIVAL-CL-NULL
           ELSE
             IF (SF)-DT-ULT-RIVAL-CL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-RIVAL-CL-NULL
             ELSE
              MOVE (SF)-DT-ULT-RIVAL-CL
              TO PCO-DT-ULT-RIVAL-CL
             END-IF
           END-IF
           IF (SF)-DT-ULT-QTZO-CL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-QTZO-CL-NULL
              TO PCO-DT-ULT-QTZO-CL-NULL
           ELSE
             IF (SF)-DT-ULT-QTZO-CL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-QTZO-CL-NULL
             ELSE
              MOVE (SF)-DT-ULT-QTZO-CL
              TO PCO-DT-ULT-QTZO-CL
             END-IF
           END-IF
           IF (SF)-DT-ULTC-BNSRIC-CL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTC-BNSRIC-CL-NULL
              TO PCO-DT-ULTC-BNSRIC-CL-NULL
           ELSE
             IF (SF)-DT-ULTC-BNSRIC-CL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTC-BNSRIC-CL-NULL
             ELSE
              MOVE (SF)-DT-ULTC-BNSRIC-CL
              TO PCO-DT-ULTC-BNSRIC-CL
             END-IF
           END-IF
           IF (SF)-DT-ULTC-BNSFDT-CL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTC-BNSFDT-CL-NULL
              TO PCO-DT-ULTC-BNSFDT-CL-NULL
           ELSE
             IF (SF)-DT-ULTC-BNSFDT-CL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTC-BNSFDT-CL-NULL
             ELSE
              MOVE (SF)-DT-ULTC-BNSFDT-CL
              TO PCO-DT-ULTC-BNSFDT-CL
             END-IF
           END-IF
           IF (SF)-DT-ULTC-IS-CL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTC-IS-CL-NULL
              TO PCO-DT-ULTC-IS-CL-NULL
           ELSE
             IF (SF)-DT-ULTC-IS-CL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTC-IS-CL-NULL
             ELSE
              MOVE (SF)-DT-ULTC-IS-CL
              TO PCO-DT-ULTC-IS-CL
             END-IF
           END-IF
           IF (SF)-DT-ULTC-RB-CL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTC-RB-CL-NULL
              TO PCO-DT-ULTC-RB-CL-NULL
           ELSE
             IF (SF)-DT-ULTC-RB-CL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTC-RB-CL-NULL
             ELSE
              MOVE (SF)-DT-ULTC-RB-CL
              TO PCO-DT-ULTC-RB-CL
             END-IF
           END-IF
           IF (SF)-DT-ULTGZ-TRCH-E-CL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTGZ-TRCH-E-CL-NULL
              TO PCO-DT-ULTGZ-TRCH-E-CL-NULL
           ELSE
             IF (SF)-DT-ULTGZ-TRCH-E-CL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTGZ-TRCH-E-CL-NULL
             ELSE
              MOVE (SF)-DT-ULTGZ-TRCH-E-CL
              TO PCO-DT-ULTGZ-TRCH-E-CL
             END-IF
           END-IF
           IF (SF)-DT-ULTSC-ELAB-CL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTSC-ELAB-CL-NULL
              TO PCO-DT-ULTSC-ELAB-CL-NULL
           ELSE
             IF (SF)-DT-ULTSC-ELAB-CL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTSC-ELAB-CL-NULL
             ELSE
              MOVE (SF)-DT-ULTSC-ELAB-CL
              TO PCO-DT-ULTSC-ELAB-CL
             END-IF
           END-IF
           IF (SF)-DT-ULTSC-OPZ-CL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTSC-OPZ-CL-NULL
              TO PCO-DT-ULTSC-OPZ-CL-NULL
           ELSE
             IF (SF)-DT-ULTSC-OPZ-CL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTSC-OPZ-CL-NULL
             ELSE
              MOVE (SF)-DT-ULTSC-OPZ-CL
              TO PCO-DT-ULTSC-OPZ-CL
             END-IF
           END-IF
           IF (SF)-STST-X-REGIONE-NULL = HIGH-VALUES
              MOVE (SF)-STST-X-REGIONE-NULL
              TO PCO-STST-X-REGIONE-NULL
           ELSE
             IF (SF)-STST-X-REGIONE = ZERO
                MOVE HIGH-VALUES
                TO PCO-STST-X-REGIONE-NULL
             ELSE
              MOVE (SF)-STST-X-REGIONE
              TO PCO-STST-X-REGIONE
             END-IF
           END-IF
           IF (SF)-DT-ULTGZ-CED-COLL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTGZ-CED-COLL-NULL
              TO PCO-DT-ULTGZ-CED-COLL-NULL
           ELSE
             IF (SF)-DT-ULTGZ-CED-COLL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTGZ-CED-COLL-NULL
             ELSE
              MOVE (SF)-DT-ULTGZ-CED-COLL
              TO PCO-DT-ULTGZ-CED-COLL
             END-IF
           END-IF
           MOVE (SF)-TP-MOD-RIVAL
              TO PCO-TP-MOD-RIVAL
           IF (SF)-NUM-MM-CALC-MORA-NULL = HIGH-VALUES
              MOVE (SF)-NUM-MM-CALC-MORA-NULL
              TO PCO-NUM-MM-CALC-MORA-NULL
           ELSE
              MOVE (SF)-NUM-MM-CALC-MORA
              TO PCO-NUM-MM-CALC-MORA
           END-IF
           IF (SF)-DT-ULT-EC-RIV-COLL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-EC-RIV-COLL-NULL
              TO PCO-DT-ULT-EC-RIV-COLL-NULL
           ELSE
             IF (SF)-DT-ULT-EC-RIV-COLL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-EC-RIV-COLL-NULL
             ELSE
              MOVE (SF)-DT-ULT-EC-RIV-COLL
              TO PCO-DT-ULT-EC-RIV-COLL
             END-IF
           END-IF
           IF (SF)-DT-ULT-EC-RIV-IND-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-EC-RIV-IND-NULL
              TO PCO-DT-ULT-EC-RIV-IND-NULL
           ELSE
             IF (SF)-DT-ULT-EC-RIV-IND = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-EC-RIV-IND-NULL
             ELSE
              MOVE (SF)-DT-ULT-EC-RIV-IND
              TO PCO-DT-ULT-EC-RIV-IND
             END-IF
           END-IF
           IF (SF)-DT-ULT-EC-IL-COLL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-EC-IL-COLL-NULL
              TO PCO-DT-ULT-EC-IL-COLL-NULL
           ELSE
             IF (SF)-DT-ULT-EC-IL-COLL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-EC-IL-COLL-NULL
             ELSE
              MOVE (SF)-DT-ULT-EC-IL-COLL
              TO PCO-DT-ULT-EC-IL-COLL
             END-IF
           END-IF
           IF (SF)-DT-ULT-EC-IL-IND-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-EC-IL-IND-NULL
              TO PCO-DT-ULT-EC-IL-IND-NULL
           ELSE
             IF (SF)-DT-ULT-EC-IL-IND = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-EC-IL-IND-NULL
             ELSE
              MOVE (SF)-DT-ULT-EC-IL-IND
              TO PCO-DT-ULT-EC-IL-IND
             END-IF
           END-IF
           IF (SF)-DT-ULT-EC-UL-COLL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-EC-UL-COLL-NULL
              TO PCO-DT-ULT-EC-UL-COLL-NULL
           ELSE
             IF (SF)-DT-ULT-EC-UL-COLL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-EC-UL-COLL-NULL
             ELSE
              MOVE (SF)-DT-ULT-EC-UL-COLL
              TO PCO-DT-ULT-EC-UL-COLL
             END-IF
           END-IF
           IF (SF)-DT-ULT-EC-UL-IND-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-EC-UL-IND-NULL
              TO PCO-DT-ULT-EC-UL-IND-NULL
           ELSE
             IF (SF)-DT-ULT-EC-UL-IND = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-EC-UL-IND-NULL
             ELSE
              MOVE (SF)-DT-ULT-EC-UL-IND
              TO PCO-DT-ULT-EC-UL-IND
             END-IF
           END-IF
           IF (SF)-AA-UTI-NULL = HIGH-VALUES
              MOVE (SF)-AA-UTI-NULL
              TO PCO-AA-UTI-NULL
           ELSE
              MOVE (SF)-AA-UTI
              TO PCO-AA-UTI
           END-IF
           MOVE (SF)-CALC-RSH-COMUN
              TO PCO-CALC-RSH-COMUN
           MOVE (SF)-FL-LIV-DEBUG
              TO PCO-FL-LIV-DEBUG
           IF (SF)-DT-ULT-BOLL-PERF-C-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-PERF-C-NULL
              TO PCO-DT-ULT-BOLL-PERF-C-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-PERF-C = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-PERF-C-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-PERF-C
              TO PCO-DT-ULT-BOLL-PERF-C
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-RSP-IN-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-RSP-IN-NULL
              TO PCO-DT-ULT-BOLL-RSP-IN-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-RSP-IN = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-RSP-IN-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-RSP-IN
              TO PCO-DT-ULT-BOLL-RSP-IN
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-RSP-CL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-RSP-CL-NULL
              TO PCO-DT-ULT-BOLL-RSP-CL-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-RSP-CL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-RSP-CL-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-RSP-CL
              TO PCO-DT-ULT-BOLL-RSP-CL
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-EMES-I-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-EMES-I-NULL
              TO PCO-DT-ULT-BOLL-EMES-I-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-EMES-I = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-EMES-I-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-EMES-I
              TO PCO-DT-ULT-BOLL-EMES-I
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-STOR-I-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-STOR-I-NULL
              TO PCO-DT-ULT-BOLL-STOR-I-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-STOR-I = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-STOR-I-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-STOR-I
              TO PCO-DT-ULT-BOLL-STOR-I
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-RIAT-I-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-RIAT-I-NULL
              TO PCO-DT-ULT-BOLL-RIAT-I-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-RIAT-I = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-RIAT-I-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-RIAT-I
              TO PCO-DT-ULT-BOLL-RIAT-I
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-SD-I-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-SD-I-NULL
              TO PCO-DT-ULT-BOLL-SD-I-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-SD-I = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-SD-I-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-SD-I
              TO PCO-DT-ULT-BOLL-SD-I
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-SDNL-I-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-SDNL-I-NULL
              TO PCO-DT-ULT-BOLL-SDNL-I-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-SDNL-I = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-SDNL-I-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-SDNL-I
              TO PCO-DT-ULT-BOLL-SDNL-I
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-PERF-I-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-PERF-I-NULL
              TO PCO-DT-ULT-BOLL-PERF-I-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-PERF-I = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-PERF-I-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-PERF-I
              TO PCO-DT-ULT-BOLL-PERF-I
             END-IF
           END-IF
           IF (SF)-DT-RICL-RIRIAS-COM-NULL = HIGH-VALUES
              MOVE (SF)-DT-RICL-RIRIAS-COM-NULL
              TO PCO-DT-RICL-RIRIAS-COM-NULL
           ELSE
             IF (SF)-DT-RICL-RIRIAS-COM = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-RICL-RIRIAS-COM-NULL
             ELSE
              MOVE (SF)-DT-RICL-RIRIAS-COM
              TO PCO-DT-RICL-RIRIAS-COM
             END-IF
           END-IF
           IF (SF)-DT-ULT-ELAB-AT92-C-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ELAB-AT92-C-NULL
              TO PCO-DT-ULT-ELAB-AT92-C-NULL
           ELSE
             IF (SF)-DT-ULT-ELAB-AT92-C = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ELAB-AT92-C-NULL
             ELSE
              MOVE (SF)-DT-ULT-ELAB-AT92-C
              TO PCO-DT-ULT-ELAB-AT92-C
             END-IF
           END-IF
           IF (SF)-DT-ULT-ELAB-AT92-I-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ELAB-AT92-I-NULL
              TO PCO-DT-ULT-ELAB-AT92-I-NULL
           ELSE
             IF (SF)-DT-ULT-ELAB-AT92-I = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ELAB-AT92-I-NULL
             ELSE
              MOVE (SF)-DT-ULT-ELAB-AT92-I
              TO PCO-DT-ULT-ELAB-AT92-I
             END-IF
           END-IF
           IF (SF)-DT-ULT-ELAB-AT93-C-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ELAB-AT93-C-NULL
              TO PCO-DT-ULT-ELAB-AT93-C-NULL
           ELSE
             IF (SF)-DT-ULT-ELAB-AT93-C = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ELAB-AT93-C-NULL
             ELSE
              MOVE (SF)-DT-ULT-ELAB-AT93-C
              TO PCO-DT-ULT-ELAB-AT93-C
             END-IF
           END-IF
           IF (SF)-DT-ULT-ELAB-AT93-I-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ELAB-AT93-I-NULL
              TO PCO-DT-ULT-ELAB-AT93-I-NULL
           ELSE
             IF (SF)-DT-ULT-ELAB-AT93-I = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ELAB-AT93-I-NULL
             ELSE
              MOVE (SF)-DT-ULT-ELAB-AT93-I
              TO PCO-DT-ULT-ELAB-AT93-I
             END-IF
           END-IF
           IF (SF)-DT-ULT-ELAB-SPE-IN-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ELAB-SPE-IN-NULL
              TO PCO-DT-ULT-ELAB-SPE-IN-NULL
           ELSE
             IF (SF)-DT-ULT-ELAB-SPE-IN = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ELAB-SPE-IN-NULL
             ELSE
              MOVE (SF)-DT-ULT-ELAB-SPE-IN
              TO PCO-DT-ULT-ELAB-SPE-IN
             END-IF
           END-IF
           IF (SF)-DT-ULT-ELAB-PR-CON-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ELAB-PR-CON-NULL
              TO PCO-DT-ULT-ELAB-PR-CON-NULL
           ELSE
             IF (SF)-DT-ULT-ELAB-PR-CON = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ELAB-PR-CON-NULL
             ELSE
              MOVE (SF)-DT-ULT-ELAB-PR-CON
              TO PCO-DT-ULT-ELAB-PR-CON
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-RP-CL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-RP-CL-NULL
              TO PCO-DT-ULT-BOLL-RP-CL-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-RP-CL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-RP-CL-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-RP-CL
              TO PCO-DT-ULT-BOLL-RP-CL
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-RP-IN-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-RP-IN-NULL
              TO PCO-DT-ULT-BOLL-RP-IN-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-RP-IN = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-RP-IN-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-RP-IN
              TO PCO-DT-ULT-BOLL-RP-IN
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-PRE-I-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-PRE-I-NULL
              TO PCO-DT-ULT-BOLL-PRE-I-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-PRE-I = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-PRE-I-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-PRE-I
              TO PCO-DT-ULT-BOLL-PRE-I
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-PRE-C-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-PRE-C-NULL
              TO PCO-DT-ULT-BOLL-PRE-C-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-PRE-C = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-PRE-C-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-PRE-C
              TO PCO-DT-ULT-BOLL-PRE-C
             END-IF
           END-IF
           IF (SF)-DT-ULTC-PILDI-MM-C-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTC-PILDI-MM-C-NULL
              TO PCO-DT-ULTC-PILDI-MM-C-NULL
           ELSE
             IF (SF)-DT-ULTC-PILDI-MM-C = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTC-PILDI-MM-C-NULL
             ELSE
              MOVE (SF)-DT-ULTC-PILDI-MM-C
              TO PCO-DT-ULTC-PILDI-MM-C
             END-IF
           END-IF
           IF (SF)-DT-ULTC-PILDI-AA-C-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTC-PILDI-AA-C-NULL
              TO PCO-DT-ULTC-PILDI-AA-C-NULL
           ELSE
             IF (SF)-DT-ULTC-PILDI-AA-C = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTC-PILDI-AA-C-NULL
             ELSE
              MOVE (SF)-DT-ULTC-PILDI-AA-C
              TO PCO-DT-ULTC-PILDI-AA-C
             END-IF
           END-IF
           IF (SF)-DT-ULTC-PILDI-MM-I-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTC-PILDI-MM-I-NULL
              TO PCO-DT-ULTC-PILDI-MM-I-NULL
           ELSE
             IF (SF)-DT-ULTC-PILDI-MM-I = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTC-PILDI-MM-I-NULL
             ELSE
              MOVE (SF)-DT-ULTC-PILDI-MM-I
              TO PCO-DT-ULTC-PILDI-MM-I
             END-IF
           END-IF
           IF (SF)-DT-ULTC-PILDI-TR-I-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTC-PILDI-TR-I-NULL
              TO PCO-DT-ULTC-PILDI-TR-I-NULL
           ELSE
             IF (SF)-DT-ULTC-PILDI-TR-I = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTC-PILDI-TR-I-NULL
             ELSE
              MOVE (SF)-DT-ULTC-PILDI-TR-I
              TO PCO-DT-ULTC-PILDI-TR-I
             END-IF
           END-IF
           IF (SF)-DT-ULTC-PILDI-AA-I-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULTC-PILDI-AA-I-NULL
              TO PCO-DT-ULTC-PILDI-AA-I-NULL
           ELSE
             IF (SF)-DT-ULTC-PILDI-AA-I = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULTC-PILDI-AA-I-NULL
             ELSE
              MOVE (SF)-DT-ULTC-PILDI-AA-I
              TO PCO-DT-ULTC-PILDI-AA-I
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-QUIE-C-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-QUIE-C-NULL
              TO PCO-DT-ULT-BOLL-QUIE-C-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-QUIE-C = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-QUIE-C-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-QUIE-C
              TO PCO-DT-ULT-BOLL-QUIE-C
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-QUIE-I-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-QUIE-I-NULL
              TO PCO-DT-ULT-BOLL-QUIE-I-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-QUIE-I = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-QUIE-I-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-QUIE-I
              TO PCO-DT-ULT-BOLL-QUIE-I
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-COTR-I-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-COTR-I-NULL
              TO PCO-DT-ULT-BOLL-COTR-I-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-COTR-I = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-COTR-I-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-COTR-I
              TO PCO-DT-ULT-BOLL-COTR-I
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-COTR-C-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-COTR-C-NULL
              TO PCO-DT-ULT-BOLL-COTR-C-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-COTR-C = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-COTR-C-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-COTR-C
              TO PCO-DT-ULT-BOLL-COTR-C
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-CORI-C-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-CORI-C-NULL
              TO PCO-DT-ULT-BOLL-CORI-C-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-CORI-C = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-CORI-C-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-CORI-C
              TO PCO-DT-ULT-BOLL-CORI-C
             END-IF
           END-IF
           IF (SF)-DT-ULT-BOLL-CORI-I-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-BOLL-CORI-I-NULL
              TO PCO-DT-ULT-BOLL-CORI-I-NULL
           ELSE
             IF (SF)-DT-ULT-BOLL-CORI-I = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-BOLL-CORI-I-NULL
             ELSE
              MOVE (SF)-DT-ULT-BOLL-CORI-I
              TO PCO-DT-ULT-BOLL-CORI-I
             END-IF
           END-IF
           MOVE (SF)-DS-OPER-SQL
              TO PCO-DS-OPER-SQL
           IF (SF)-DS-VER NOT NUMERIC
              MOVE 0 TO PCO-DS-VER
           ELSE
              MOVE (SF)-DS-VER
              TO PCO-DS-VER
           END-IF
           IF (SF)-DS-TS-CPTZ NOT NUMERIC
              MOVE 0 TO PCO-DS-TS-CPTZ
           ELSE
              MOVE (SF)-DS-TS-CPTZ
              TO PCO-DS-TS-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE
              TO PCO-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB
              TO PCO-DS-STATO-ELAB
           IF (SF)-TP-VALZZ-DT-VLT-NULL = HIGH-VALUES
              MOVE (SF)-TP-VALZZ-DT-VLT-NULL
              TO PCO-TP-VALZZ-DT-VLT-NULL
           ELSE
              MOVE (SF)-TP-VALZZ-DT-VLT
              TO PCO-TP-VALZZ-DT-VLT
           END-IF
           IF (SF)-FL-FRAZ-PROV-ACQ-NULL = HIGH-VALUES
              MOVE (SF)-FL-FRAZ-PROV-ACQ-NULL
              TO PCO-FL-FRAZ-PROV-ACQ-NULL
           ELSE
              MOVE (SF)-FL-FRAZ-PROV-ACQ
              TO PCO-FL-FRAZ-PROV-ACQ
           END-IF
           IF (SF)-DT-ULT-AGG-EROG-RE-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-AGG-EROG-RE-NULL
              TO PCO-DT-ULT-AGG-EROG-RE-NULL
           ELSE
             IF (SF)-DT-ULT-AGG-EROG-RE = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-AGG-EROG-RE-NULL
             ELSE
              MOVE (SF)-DT-ULT-AGG-EROG-RE
              TO PCO-DT-ULT-AGG-EROG-RE
             END-IF
           END-IF
           IF (SF)-PC-RM-MARSOL-NULL = HIGH-VALUES
              MOVE (SF)-PC-RM-MARSOL-NULL
              TO PCO-PC-RM-MARSOL-NULL
           ELSE
              MOVE (SF)-PC-RM-MARSOL
              TO PCO-PC-RM-MARSOL
           END-IF
           IF (SF)-PC-C-SUBRSH-MARSOL-NULL = HIGH-VALUES
              MOVE (SF)-PC-C-SUBRSH-MARSOL-NULL
              TO PCO-PC-C-SUBRSH-MARSOL-NULL
           ELSE
              MOVE (SF)-PC-C-SUBRSH-MARSOL
              TO PCO-PC-C-SUBRSH-MARSOL
           END-IF
           IF (SF)-COD-COMP-ISVAP-NULL = HIGH-VALUES
              MOVE (SF)-COD-COMP-ISVAP-NULL
              TO PCO-COD-COMP-ISVAP-NULL
           ELSE
              MOVE (SF)-COD-COMP-ISVAP
              TO PCO-COD-COMP-ISVAP
           END-IF
           IF (SF)-LM-RIS-CON-INT-NULL = HIGH-VALUES
              MOVE (SF)-LM-RIS-CON-INT-NULL
              TO PCO-LM-RIS-CON-INT-NULL
           ELSE
              MOVE (SF)-LM-RIS-CON-INT
              TO PCO-LM-RIS-CON-INT
           END-IF
           IF (SF)-LM-C-SUBRSH-CON-IN-NULL = HIGH-VALUES
              MOVE (SF)-LM-C-SUBRSH-CON-IN-NULL
              TO PCO-LM-C-SUBRSH-CON-IN-NULL
           ELSE
              MOVE (SF)-LM-C-SUBRSH-CON-IN
              TO PCO-LM-C-SUBRSH-CON-IN
           END-IF
           IF (SF)-PC-GAR-NORISK-MARS-NULL = HIGH-VALUES
              MOVE (SF)-PC-GAR-NORISK-MARS-NULL
              TO PCO-PC-GAR-NORISK-MARS-NULL
           ELSE
              MOVE (SF)-PC-GAR-NORISK-MARS
              TO PCO-PC-GAR-NORISK-MARS
           END-IF
           IF (SF)-CRZ-1A-RAT-INTR-PR-NULL = HIGH-VALUES
              MOVE (SF)-CRZ-1A-RAT-INTR-PR-NULL
              TO PCO-CRZ-1A-RAT-INTR-PR-NULL
           ELSE
              MOVE (SF)-CRZ-1A-RAT-INTR-PR
              TO PCO-CRZ-1A-RAT-INTR-PR
           END-IF
           IF (SF)-NUM-GG-ARR-INTR-PR-NULL = HIGH-VALUES
              MOVE (SF)-NUM-GG-ARR-INTR-PR-NULL
              TO PCO-NUM-GG-ARR-INTR-PR-NULL
           ELSE
              MOVE (SF)-NUM-GG-ARR-INTR-PR
              TO PCO-NUM-GG-ARR-INTR-PR
           END-IF
           IF (SF)-FL-VISUAL-VINPG-NULL = HIGH-VALUES
              MOVE (SF)-FL-VISUAL-VINPG-NULL
              TO PCO-FL-VISUAL-VINPG-NULL
           ELSE
              MOVE (SF)-FL-VISUAL-VINPG
              TO PCO-FL-VISUAL-VINPG
           END-IF
           IF (SF)-DT-ULT-ESTRAZ-FUG-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ESTRAZ-FUG-NULL
              TO PCO-DT-ULT-ESTRAZ-FUG-NULL
           ELSE
             IF (SF)-DT-ULT-ESTRAZ-FUG = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ESTRAZ-FUG-NULL
             ELSE
              MOVE (SF)-DT-ULT-ESTRAZ-FUG
              TO PCO-DT-ULT-ESTRAZ-FUG
             END-IF
           END-IF
           IF (SF)-DT-ULT-ELAB-PR-AUT-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ELAB-PR-AUT-NULL
              TO PCO-DT-ULT-ELAB-PR-AUT-NULL
           ELSE
             IF (SF)-DT-ULT-ELAB-PR-AUT = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ELAB-PR-AUT-NULL
             ELSE
              MOVE (SF)-DT-ULT-ELAB-PR-AUT
              TO PCO-DT-ULT-ELAB-PR-AUT
             END-IF
           END-IF
           IF (SF)-DT-ULT-ELAB-COMMEF-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ELAB-COMMEF-NULL
              TO PCO-DT-ULT-ELAB-COMMEF-NULL
           ELSE
             IF (SF)-DT-ULT-ELAB-COMMEF = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ELAB-COMMEF-NULL
             ELSE
              MOVE (SF)-DT-ULT-ELAB-COMMEF
              TO PCO-DT-ULT-ELAB-COMMEF
             END-IF
           END-IF
           IF (SF)-DT-ULT-ELAB-LIQMEF-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ELAB-LIQMEF-NULL
              TO PCO-DT-ULT-ELAB-LIQMEF-NULL
           ELSE
             IF (SF)-DT-ULT-ELAB-LIQMEF = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ELAB-LIQMEF-NULL
             ELSE
              MOVE (SF)-DT-ULT-ELAB-LIQMEF
              TO PCO-DT-ULT-ELAB-LIQMEF
             END-IF
           END-IF
           IF (SF)-COD-FISC-MEF-NULL = HIGH-VALUES
              MOVE (SF)-COD-FISC-MEF-NULL
              TO PCO-COD-FISC-MEF-NULL
           ELSE
              MOVE (SF)-COD-FISC-MEF
              TO PCO-COD-FISC-MEF
           END-IF
           IF (SF)-IMP-ASS-SOCIALE-NULL = HIGH-VALUES
              MOVE (SF)-IMP-ASS-SOCIALE-NULL
              TO PCO-IMP-ASS-SOCIALE-NULL
           ELSE
              MOVE (SF)-IMP-ASS-SOCIALE
              TO PCO-IMP-ASS-SOCIALE
           END-IF
           IF (SF)-MOD-COMNZ-INVST-SW-NULL = HIGH-VALUES
              MOVE (SF)-MOD-COMNZ-INVST-SW-NULL
              TO PCO-MOD-COMNZ-INVST-SW-NULL
           ELSE
              MOVE (SF)-MOD-COMNZ-INVST-SW
              TO PCO-MOD-COMNZ-INVST-SW
           END-IF
           IF (SF)-DT-RIAT-RIASS-RSH-NULL = HIGH-VALUES
              MOVE (SF)-DT-RIAT-RIASS-RSH-NULL
              TO PCO-DT-RIAT-RIASS-RSH-NULL
           ELSE
             IF (SF)-DT-RIAT-RIASS-RSH = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-RIAT-RIASS-RSH-NULL
             ELSE
              MOVE (SF)-DT-RIAT-RIASS-RSH
              TO PCO-DT-RIAT-RIASS-RSH
             END-IF
           END-IF
           IF (SF)-DT-RIAT-RIASS-COMM-NULL = HIGH-VALUES
              MOVE (SF)-DT-RIAT-RIASS-COMM-NULL
              TO PCO-DT-RIAT-RIASS-COMM-NULL
           ELSE
             IF (SF)-DT-RIAT-RIASS-COMM = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-RIAT-RIASS-COMM-NULL
             ELSE
              MOVE (SF)-DT-RIAT-RIASS-COMM
              TO PCO-DT-RIAT-RIASS-COMM
             END-IF
           END-IF
           IF (SF)-GG-INTR-RIT-PAG-NULL = HIGH-VALUES
              MOVE (SF)-GG-INTR-RIT-PAG-NULL
              TO PCO-GG-INTR-RIT-PAG-NULL
           ELSE
              MOVE (SF)-GG-INTR-RIT-PAG
              TO PCO-GG-INTR-RIT-PAG
           END-IF
           IF (SF)-DT-ULT-RINN-TAC-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-RINN-TAC-NULL
              TO PCO-DT-ULT-RINN-TAC-NULL
           ELSE
             IF (SF)-DT-ULT-RINN-TAC = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-RINN-TAC-NULL
             ELSE
              MOVE (SF)-DT-ULT-RINN-TAC
              TO PCO-DT-ULT-RINN-TAC
             END-IF
           END-IF
ITRAT      MOVE (SF)-DESC-COMP-VCHAR
ITRAT        TO PCO-DESC-COMP-VCHAR
           IF (SF)-DT-ULT-EC-TCM-IND-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-EC-TCM-IND-NULL
              TO PCO-DT-ULT-EC-TCM-IND-NULL
           ELSE
             IF (SF)-DT-ULT-EC-TCM-IND = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-EC-TCM-IND-NULL
             ELSE
              MOVE (SF)-DT-ULT-EC-TCM-IND
              TO PCO-DT-ULT-EC-TCM-IND
             END-IF
           END-IF
           IF (SF)-DT-ULT-EC-TCM-COLL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-EC-TCM-COLL-NULL
              TO PCO-DT-ULT-EC-TCM-COLL-NULL
           ELSE
             IF (SF)-DT-ULT-EC-TCM-COLL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-EC-TCM-COLL-NULL
             ELSE
              MOVE (SF)-DT-ULT-EC-TCM-COLL
              TO PCO-DT-ULT-EC-TCM-COLL
             END-IF
           END-IF
           IF (SF)-DT-ULT-EC-MRM-IND-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-EC-MRM-IND-NULL
              TO PCO-DT-ULT-EC-MRM-IND-NULL
           ELSE
             IF (SF)-DT-ULT-EC-MRM-IND = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-EC-MRM-IND-NULL
             ELSE
              MOVE (SF)-DT-ULT-EC-MRM-IND
              TO PCO-DT-ULT-EC-MRM-IND
             END-IF
           END-IF
           IF (SF)-DT-ULT-EC-MRM-COLL-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-EC-MRM-COLL-NULL
              TO PCO-DT-ULT-EC-MRM-COLL-NULL
           ELSE
             IF (SF)-DT-ULT-EC-MRM-COLL = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-EC-MRM-COLL-NULL
             ELSE
              MOVE (SF)-DT-ULT-EC-MRM-COLL
              TO PCO-DT-ULT-EC-MRM-COLL
             END-IF
           END-IF
           IF (SF)-COD-COMP-LDAP-NULL = HIGH-VALUES
              MOVE (SF)-COD-COMP-LDAP-NULL
              TO PCO-COD-COMP-LDAP-NULL
           ELSE
              MOVE (SF)-COD-COMP-LDAP
              TO PCO-COD-COMP-LDAP
           END-IF
           IF (SF)-PC-RID-IMP-1382011-NULL = HIGH-VALUES
              MOVE (SF)-PC-RID-IMP-1382011-NULL
              TO PCO-PC-RID-IMP-1382011-NULL
           ELSE
              MOVE (SF)-PC-RID-IMP-1382011
              TO PCO-PC-RID-IMP-1382011
           END-IF
           IF (SF)-PC-RID-IMP-662014-NULL = HIGH-VALUES
              MOVE (SF)-PC-RID-IMP-662014-NULL
              TO PCO-PC-RID-IMP-662014-NULL
           ELSE
              MOVE (SF)-PC-RID-IMP-662014
              TO PCO-PC-RID-IMP-662014
           END-IF
           IF (SF)-SOGL-AML-PRE-UNI-NULL = HIGH-VALUES
              MOVE (SF)-SOGL-AML-PRE-UNI-NULL
              TO PCO-SOGL-AML-PRE-UNI-NULL
           ELSE
              MOVE (SF)-SOGL-AML-PRE-UNI
              TO PCO-SOGL-AML-PRE-UNI
           END-IF
           IF (SF)-SOGL-AML-PRE-PER-NULL = HIGH-VALUES
              MOVE (SF)-SOGL-AML-PRE-PER-NULL
              TO PCO-SOGL-AML-PRE-PER-NULL
           ELSE
              MOVE (SF)-SOGL-AML-PRE-PER
              TO PCO-SOGL-AML-PRE-PER
           END-IF
           IF (SF)-COD-SOGG-FTZ-ASSTO-NULL = HIGH-VALUES
              MOVE (SF)-COD-SOGG-FTZ-ASSTO-NULL
              TO PCO-COD-SOGG-FTZ-ASSTO-NULL
           ELSE
              MOVE (SF)-COD-SOGG-FTZ-ASSTO
              TO PCO-COD-SOGG-FTZ-ASSTO
           END-IF
           IF (SF)-DT-ULT-ELAB-REDPRO-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ELAB-REDPRO-NULL
              TO PCO-DT-ULT-ELAB-REDPRO-NULL
           ELSE
             IF (SF)-DT-ULT-ELAB-REDPRO = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ELAB-REDPRO-NULL
             ELSE
              MOVE (SF)-DT-ULT-ELAB-REDPRO
              TO PCO-DT-ULT-ELAB-REDPRO
             END-IF
           END-IF
           IF (SF)-DT-ULT-ELAB-TAKE-P-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ELAB-TAKE-P-NULL
              TO PCO-DT-ULT-ELAB-TAKE-P-NULL
           ELSE
             IF (SF)-DT-ULT-ELAB-TAKE-P = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ELAB-TAKE-P-NULL
             ELSE
              MOVE (SF)-DT-ULT-ELAB-TAKE-P
              TO PCO-DT-ULT-ELAB-TAKE-P
             END-IF
           END-IF
           IF (SF)-DT-ULT-ELAB-PASPAS-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ELAB-PASPAS-NULL
              TO PCO-DT-ULT-ELAB-PASPAS-NULL
           ELSE
             IF (SF)-DT-ULT-ELAB-PASPAS = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ELAB-PASPAS-NULL
             ELSE
              MOVE (SF)-DT-ULT-ELAB-PASPAS
              TO PCO-DT-ULT-ELAB-PASPAS
             END-IF
           END-IF
           IF (SF)-SOGL-AML-PRE-SAV-R-NULL = HIGH-VALUES
              MOVE (SF)-SOGL-AML-PRE-SAV-R-NULL
              TO PCO-SOGL-AML-PRE-SAV-R-NULL
           ELSE
              MOVE (SF)-SOGL-AML-PRE-SAV-R
              TO PCO-SOGL-AML-PRE-SAV-R
           END-IF
           IF (SF)-DT-ULT-ESTR-DEC-CO-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ESTR-DEC-CO-NULL
              TO PCO-DT-ULT-ESTR-DEC-CO-NULL
           ELSE
             IF (SF)-DT-ULT-ESTR-DEC-CO = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ESTR-DEC-CO-NULL
             ELSE
              MOVE (SF)-DT-ULT-ESTR-DEC-CO
              TO PCO-DT-ULT-ESTR-DEC-CO
             END-IF
           END-IF
           IF (SF)-DT-ULT-ELAB-COS-AT-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ELAB-COS-AT-NULL
              TO PCO-DT-ULT-ELAB-COS-AT-NULL
           ELSE
             IF (SF)-DT-ULT-ELAB-COS-AT = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ELAB-COS-AT-NULL
             ELSE
              MOVE (SF)-DT-ULT-ELAB-COS-AT
              TO PCO-DT-ULT-ELAB-COS-AT
             END-IF
           END-IF
           IF (SF)-FRQ-COSTI-ATT-NULL = HIGH-VALUES
              MOVE (SF)-FRQ-COSTI-ATT-NULL
              TO PCO-FRQ-COSTI-ATT-NULL
           ELSE
              MOVE (SF)-FRQ-COSTI-ATT
              TO PCO-FRQ-COSTI-ATT
           END-IF
           IF (SF)-DT-ULT-ELAB-COS-ST-NULL = HIGH-VALUES
              MOVE (SF)-DT-ULT-ELAB-COS-ST-NULL
              TO PCO-DT-ULT-ELAB-COS-ST-NULL
           ELSE
             IF (SF)-DT-ULT-ELAB-COS-ST = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ULT-ELAB-COS-ST-NULL
             ELSE
              MOVE (SF)-DT-ULT-ELAB-COS-ST
              TO PCO-DT-ULT-ELAB-COS-ST
             END-IF
           END-IF
           IF (SF)-FRQ-COSTI-STORNATI-NULL = HIGH-VALUES
              MOVE (SF)-FRQ-COSTI-STORNATI-NULL
              TO PCO-FRQ-COSTI-STORNATI-NULL
           ELSE
              MOVE (SF)-FRQ-COSTI-STORNATI
              TO PCO-FRQ-COSTI-STORNATI
           END-IF
           IF (SF)-DT-ESTR-ASS-MIN70A-NULL = HIGH-VALUES
              MOVE (SF)-DT-ESTR-ASS-MIN70A-NULL
              TO PCO-DT-ESTR-ASS-MIN70A-NULL
           ELSE
             IF (SF)-DT-ESTR-ASS-MIN70A = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ESTR-ASS-MIN70A-NULL
             ELSE
              MOVE (SF)-DT-ESTR-ASS-MIN70A
              TO PCO-DT-ESTR-ASS-MIN70A
             END-IF
           END-IF
           IF (SF)-DT-ESTR-ASS-MAG70A-NULL = HIGH-VALUES
              MOVE (SF)-DT-ESTR-ASS-MAG70A-NULL
              TO PCO-DT-ESTR-ASS-MAG70A-NULL
           ELSE
             IF (SF)-DT-ESTR-ASS-MAG70A = ZERO
                MOVE HIGH-VALUES
                TO PCO-DT-ESTR-ASS-MAG70A-NULL
             ELSE
              MOVE (SF)-DT-ESTR-ASS-MAG70A
              TO PCO-DT-ESTR-ASS-MAG70A
             END-IF
           END-IF.
       VAL-DCLGEN-PCO-EX.
           EXIT.

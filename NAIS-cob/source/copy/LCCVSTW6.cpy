      *----------------------------------------------------------------*
      *    COPY      ..... LCCVSTW6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO STATO OGGETTO WORKFLOW
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-STAT-OGG-WF.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE STAT-OGG-WF.

      *--> NOME TABELLA FISICA DB
           MOVE 'STAT-OGG-WF'                 TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WSTW-ST-INV(IX-TAB-STW)
              AND WSTW-ELE-STAT-OGG-WF-MAX NOT = 0

              MOVE WMOV-ID-PTF     TO STW-ID-MOVI-CRZ

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WSTW-ST-ADD(IX-TAB-STW)
      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK
                        MOVE S090-SEQ-TABELLA
                          TO WSTW-ID-PTF(IX-TAB-STW)
                             WSTW-ID-STAT-OGG-WF(IX-TAB-STW)
                             STW-ID-STAT-OGG-WF
                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WSTW-ST-DEL(IX-TAB-STW)
                     MOVE WSTW-ID-PTF(IX-TAB-STW)  TO STW-ID-STAT-OGG-WF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-DELETE-LOGICA   TO TRUE

      *-->        AGGIORNAMENTO
                  WHEN WSTW-ST-MOD(IX-TAB-STW)
                     MOVE WSTW-ID-PTF(IX-TAB-STW)  TO STW-ID-STAT-OGG-WF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN STATO OGGETTO WF
                 PERFORM VAL-DCLGEN-STW
                    THRU VAL-DCLGEN-STW-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-STW
                    THRU VALORIZZA-AREA-DSH-STW-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-STAT-OGG-WF-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-STW.

      *--> DCLGEN TABELLA
           MOVE STAT-OGG-WF             TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-STW-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVSTW5.

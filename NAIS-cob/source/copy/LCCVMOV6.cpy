      *----------------------------------------------------------------*
      *    COPY      ..... LCCVMOV6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO MOVIMENTO
      *----------------------------------------------------------------*
       AGGIORNA-MOVI.

      *--> CONTROLLO DELLO STATUS
           EVALUATE TRUE
      *-->     INSERIMENTO
               WHEN WMOV-ST-ADD
      *-->        ESTRAZIONE E VALORIZZAZIONE SEQUENCE MOVIMENTO
                  PERFORM ESTR-SEQUENCE-MOVI
                     THRU ESTR-SEQUENCE-MOVI-EX

      *-->        TIPO OPERAZIONE DISPATCHER
                  SET  IDSI0011-INSERT TO TRUE

               WHEN WMOV-ST-MOD

      *-->        TIPO OPERAZIONE DISPATCHER
                  SET  IDSI0011-UPDATE TO TRUE

           END-EVALUATE.

           IF IDSV0001-ESITO-OK
              IF LCCV0005-PREVENTIVAZIONE
                 MOVE WMOV-ID-OGG  TO MOV-ID-OGG
              ELSE
      *-->       PRE VALORIZZA DCLGEN MOVI
                 PERFORM PREVAL-DCLGEN-MOVI
                    THRU PREVAL-DCLGEN-MOVI-EX
              END-IF

      *-->    VALORIZZA DCLGEN MOVI
              PERFORM VAL-DCLGEN-MOV
                 THRU VAL-DCLGEN-MOV-EX

      *-->    VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
              PERFORM VALORIZZA-AREA-DSH-MOV
                 THRU VALORIZZA-AREA-DSH-MOV-EX

      *-->    CALL DISPATCHER PER AGGIORNAMENTO
              PERFORM AGGIORNA-TABELLA
                 THRU AGGIORNA-TABELLA-EX
           END-IF.

       AGGIORNA-MOVI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ESTRAZIONE SEQUENCE MOVIMENTO
      *----------------------------------------------------------------*
       ESTR-SEQUENCE-MOVI.

           MOVE 'MOVI' TO WK-TABELLA.

           PERFORM ESTR-SEQUENCE
              THRU ESTR-SEQUENCE-EX.

           IF IDSV0001-ESITO-OK
              MOVE S090-SEQ-TABELLA        TO WMOV-ID-PTF
                                              MOV-ID-MOVI
           END-IF.

       ESTR-SEQUENCE-MOVI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ESTRAZIONE SEQUENCE POLIZZA
      *----------------------------------------------------------------*
       ESTR-SEQUENCE-POLI.

           MOVE 'POLI' TO WK-TABELLA.

           PERFORM ESTR-SEQUENCE
              THRU ESTR-SEQUENCE-EX.

           MOVE 'MOVI' TO WK-TABELLA.

           IF IDSV0001-ESITO-OK
              MOVE S090-SEQ-TABELLA TO WPOL-ID-PTF
           END-IF.

       ESTR-SEQUENCE-POLI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    ESTRAZIONE SEQUENCE ADESIONE
      *----------------------------------------------------------------*
       ESTR-SEQUENCE-ADES.

           MOVE 'ADES' TO WK-TABELLA.

           PERFORM ESTR-SEQUENCE
              THRU ESTR-SEQUENCE-EX.

           MOVE 'MOVI' TO WK-TABELLA.

           IF IDSV0001-ESITO-OK
              MOVE S090-SEQ-TABELLA
                TO WADE-ID-PTF
           END-IF.

       ESTR-SEQUENCE-ADES-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA IB MOVIMENTO
      *----------------------------------------------------------------*
       S1200-VAL-IB-MOVIMENTO.

           MOVE IDSV0001-COD-COMPAGNIA-ANIA
             TO WS-COD-COMPAGNIA.

           MOVE WMOV-ID-PTF      TO  WS-ID-MOVI-PTF.
           MOVE WS-ID-MOVI-PTF   TO  WS-SEQUENCE-PTF.
           MOVE WS-OGGETTO       TO  WS-MOVIMENTO-APPO.

           INSPECT WS-MOVIMENTO-APPO REPLACING ALL ' ' BY '0'.
           MOVE WS-MOVIMENTO-APPO  TO  WMOV-IB-MOVI.

       S1200-VAL-IB-MOVIMENTO-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PREPARA AREA PER CALL LCCS0234
      *----------------------------------------------------------------*
       S1210-PREPARA-AREA-LCCS0234.

           MOVE WMOV-ID-OGG                TO S234-ID-OGG-EOC.
           MOVE WMOV-TP-OGG                TO S234-TIPO-OGG-EOC.

       S1210-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA ID OGGETTO
      *----------------------------------------------------------------*
       S1220-VALORIZZA-ID-OGG-MOV.

            MOVE S234-ID-OGG-PTF-EOC             TO  MOV-ID-OGG
                                                     WMOV-ID-OGG.

            MOVE S234-IB-OGG-PTF-EOC             TO  MOV-IB-OGG
                                                     WMOV-IB-OGG.

       S1220-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    PRE VALORIZZA DCLGEN MOVI
      *----------------------------------------------------------------*
       PREVAL-DCLGEN-MOVI.

           EVALUATE TRUE
               WHEN WMOV-ST-ADD
                  IF WMOV-TP-OGG-NULL = HIGH-VALUE
                     CONTINUE
                  ELSE
      *--            Estrazione IB-OGGETTO/ID-OGGETTO
                     PERFORM S1210-PREPARA-AREA-LCCS0234
                        THRU S1210-EX
                     PERFORM CALL-LCCS0234
                        THRU CALL-LCCS0234-EX
      *--            Valorizzazione ID-OGGETTO e IB-OGGETTO
                     IF IDSV0001-ESITO-OK
                        PERFORM S1220-VALORIZZA-ID-OGG-MOV
                           THRU S1220-EX
                     END-IF
                  END-IF
      *--         Valorizzazione ID-RICH Moviemnto
                  IF  IDSV0001-ESITO-OK
                  AND LCCV0005-RICHIESTA-SI
                      IF  WRIC-ID-RICH IS NUMERIC
                      AND WRIC-ID-RICH > ZERO
                          MOVE WRIC-ID-RICH   TO WMOV-ID-RICH
                      ELSE
                          MOVE HIGH-VALUE     TO WMOV-ID-RICH-NULL
                      END-IF
                  END-IF
      *--         Valorizzazione IB-Movimento
                  PERFORM S1200-VAL-IB-MOVIMENTO
                     THRU S1200-VAL-IB-MOVIMENTO-EX
           END-EVALUATE.

       PREVAL-DCLGEN-MOVI-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-MOV.

      *--> NOME TABELLA FISICA DB
           MOVE 'MOVI'                  TO   WK-TABELLA.

      *--> DCLGEN TABELLA
           MOVE MOVI                    TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-PRIMARY-KEY    TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-SENZA-STOR           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-MOV-EX.
           EXIT.

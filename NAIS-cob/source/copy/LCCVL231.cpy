      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA VINC_PEG
      *   ALIAS L23
      *   ULTIMO AGG. 31 MAG 2010
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-VINC-PEG PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-ID-RAPP-ANA PIC S9(9)     COMP-3.
             07 (SF)-TP-VINC PIC X(2).
             07 (SF)-TP-VINC-NULL REDEFINES
                (SF)-TP-VINC   PIC X(2).
             07 (SF)-FL-DELEGA-AL-RISC PIC X(1).
             07 (SF)-FL-DELEGA-AL-RISC-NULL REDEFINES
                (SF)-FL-DELEGA-AL-RISC   PIC X(1).
             07 (SF)-DESC PIC X(1000).
             07 (SF)-DT-ATTIV-VINPG   PIC S9(8) COMP-3.
             07 (SF)-DT-ATTIV-VINPG-NULL REDEFINES
                (SF)-DT-ATTIV-VINPG   PIC X(5).
             07 (SF)-CPT-VINCTO-PIGN PIC S9(12)V9(3) COMP-3.
             07 (SF)-CPT-VINCTO-PIGN-NULL REDEFINES
                (SF)-CPT-VINCTO-PIGN   PIC X(8).
             07 (SF)-DT-CHIU-VINPG   PIC S9(8) COMP-3.
             07 (SF)-DT-CHIU-VINPG-NULL REDEFINES
                (SF)-DT-CHIU-VINPG   PIC X(5).
             07 (SF)-VAL-RISC-INI-VINPG PIC S9(12)V9(3) COMP-3.
             07 (SF)-VAL-RISC-INI-VINPG-NULL REDEFINES
                (SF)-VAL-RISC-INI-VINPG   PIC X(8).
             07 (SF)-VAL-RISC-END-VINPG PIC S9(12)V9(3) COMP-3.
             07 (SF)-VAL-RISC-END-VINPG-NULL REDEFINES
                (SF)-VAL-RISC-END-VINPG   PIC X(8).
             07 (SF)-SOM-PRE-VINPG PIC S9(12)V9(3) COMP-3.
             07 (SF)-SOM-PRE-VINPG-NULL REDEFINES
                (SF)-SOM-PRE-VINPG   PIC X(8).
             07 (SF)-FL-VINPG-INT-PRSTZ PIC X(1).
             07 (SF)-FL-VINPG-INT-PRSTZ-NULL REDEFINES
                (SF)-FL-VINPG-INT-PRSTZ   PIC X(1).
             07 (SF)-DESC-AGG-VINC PIC X(100).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-TP-AUT-SEQ PIC X(2).
             07 (SF)-TP-AUT-SEQ-NULL REDEFINES
                (SF)-TP-AUT-SEQ   PIC X(2).
             07 (SF)-COD-UFF-SEQ PIC X(11).
             07 (SF)-COD-UFF-SEQ-NULL REDEFINES
                (SF)-COD-UFF-SEQ   PIC X(11).
             07 (SF)-NUM-PROVV-SEQ PIC S9(8)     COMP-3.
             07 (SF)-NUM-PROVV-SEQ-NULL REDEFINES
                (SF)-NUM-PROVV-SEQ   PIC X(5).
             07 (SF)-TP-PROVV-SEQ PIC X(2).
             07 (SF)-TP-PROVV-SEQ-NULL REDEFINES
                (SF)-TP-PROVV-SEQ   PIC X(2).
             07 (SF)-DT-PROVV-SEQ   PIC S9(8) COMP-3.
             07 (SF)-DT-PROVV-SEQ-NULL REDEFINES
                (SF)-DT-PROVV-SEQ   PIC X(5).
             07 (SF)-DT-NOTIFICA-BLOCCO   PIC S9(8) COMP-3.
             07 (SF)-DT-NOTIFICA-BLOCCO-NULL REDEFINES
                (SF)-DT-NOTIFICA-BLOCCO   PIC X(5).
             07 (SF)-NOTA-PROVV PIC X(300).


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVBEP3
      *   ULTIMO AGG. 09 AGO 2018
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-BEP.
           MOVE BEP-ID-BNFIC
             TO (SF)-ID-PTF(IX-TAB-BEP)
           MOVE BEP-ID-BNFIC
             TO (SF)-ID-BNFIC(IX-TAB-BEP)
           MOVE BEP-ID-RAPP-ANA
             TO (SF)-ID-RAPP-ANA(IX-TAB-BEP)
           IF BEP-ID-BNFICR-NULL = HIGH-VALUES
              MOVE BEP-ID-BNFICR-NULL
                TO (SF)-ID-BNFICR-NULL(IX-TAB-BEP)
           ELSE
              MOVE BEP-ID-BNFICR
                TO (SF)-ID-BNFICR(IX-TAB-BEP)
           END-IF
           MOVE BEP-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-BEP)
           IF BEP-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE BEP-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-BEP)
           ELSE
              MOVE BEP-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-BEP)
           END-IF
           MOVE BEP-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-BEP)
           MOVE BEP-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-BEP)
           MOVE BEP-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-BEP)
           IF BEP-COD-BNFIC-NULL = HIGH-VALUES
              MOVE BEP-COD-BNFIC-NULL
                TO (SF)-COD-BNFIC-NULL(IX-TAB-BEP)
           ELSE
              MOVE BEP-COD-BNFIC
                TO (SF)-COD-BNFIC(IX-TAB-BEP)
           END-IF
           MOVE BEP-TP-IND-BNFICR
             TO (SF)-TP-IND-BNFICR(IX-TAB-BEP)
           IF BEP-COD-BNFICR-NULL = HIGH-VALUES
              MOVE BEP-COD-BNFICR-NULL
                TO (SF)-COD-BNFICR-NULL(IX-TAB-BEP)
           ELSE
              MOVE BEP-COD-BNFICR
                TO (SF)-COD-BNFICR(IX-TAB-BEP)
           END-IF
           MOVE BEP-DESC-BNFICR
             TO (SF)-DESC-BNFICR(IX-TAB-BEP)
           IF BEP-PC-DEL-BNFICR-NULL = HIGH-VALUES
              MOVE BEP-PC-DEL-BNFICR-NULL
                TO (SF)-PC-DEL-BNFICR-NULL(IX-TAB-BEP)
           ELSE
              MOVE BEP-PC-DEL-BNFICR
                TO (SF)-PC-DEL-BNFICR(IX-TAB-BEP)
           END-IF
           IF BEP-FL-ESE-NULL = HIGH-VALUES
              MOVE BEP-FL-ESE-NULL
                TO (SF)-FL-ESE-NULL(IX-TAB-BEP)
           ELSE
              MOVE BEP-FL-ESE
                TO (SF)-FL-ESE(IX-TAB-BEP)
           END-IF
           IF BEP-FL-IRREV-NULL = HIGH-VALUES
              MOVE BEP-FL-IRREV-NULL
                TO (SF)-FL-IRREV-NULL(IX-TAB-BEP)
           ELSE
              MOVE BEP-FL-IRREV
                TO (SF)-FL-IRREV(IX-TAB-BEP)
           END-IF
           IF BEP-FL-DFLT-NULL = HIGH-VALUES
              MOVE BEP-FL-DFLT-NULL
                TO (SF)-FL-DFLT-NULL(IX-TAB-BEP)
           ELSE
              MOVE BEP-FL-DFLT
                TO (SF)-FL-DFLT(IX-TAB-BEP)
           END-IF
           IF BEP-ESRCN-ATTVT-IMPRS-NULL = HIGH-VALUES
              MOVE BEP-ESRCN-ATTVT-IMPRS-NULL
                TO (SF)-ESRCN-ATTVT-IMPRS-NULL(IX-TAB-BEP)
           ELSE
              MOVE BEP-ESRCN-ATTVT-IMPRS
                TO (SF)-ESRCN-ATTVT-IMPRS(IX-TAB-BEP)
           END-IF
           MOVE BEP-FL-BNFICR-COLL
             TO (SF)-FL-BNFICR-COLL(IX-TAB-BEP)
           MOVE BEP-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-BEP)
           MOVE BEP-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-BEP)
           MOVE BEP-DS-VER
             TO (SF)-DS-VER(IX-TAB-BEP)
           MOVE BEP-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-BEP)
           MOVE BEP-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-BEP)
           MOVE BEP-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-BEP)
           MOVE BEP-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-BEP)
           IF BEP-TP-NORMAL-BNFIC-NULL = HIGH-VALUES
              MOVE BEP-TP-NORMAL-BNFIC-NULL
                TO (SF)-TP-NORMAL-BNFIC-NULL(IX-TAB-BEP)
           ELSE
              MOVE BEP-TP-NORMAL-BNFIC
                TO (SF)-TP-NORMAL-BNFIC(IX-TAB-BEP)
           END-IF.
       VALORIZZA-OUTPUT-BEP-EX.
           EXIT.

           EXEC SQL DECLARE LIQ TABLE
           (
             ID_LIQ              DECIMAL(9, 0) NOT NULL,
             ID_OGG              DECIMAL(9, 0) NOT NULL,
             TP_OGG              CHAR(2) NOT NULL,
             ID_MOVI_CRZ         DECIMAL(9, 0) NOT NULL,
             ID_MOVI_CHIU        DECIMAL(9, 0),
             DT_INI_EFF          DATE NOT NULL,
             DT_END_EFF          DATE NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             IB_OGG              CHAR(40),
             TP_LIQ              CHAR(2) NOT NULL,
             DESC_CAU_EVE_SIN    VARCHAR(100),
             COD_CAU_SIN         CHAR(20),
             COD_SIN_CATSTRF     CHAR(20),
             DT_MOR              DATE,
             DT_DEN              DATE,
             DT_PERV_DEN         DATE,
             DT_RICH             DATE,
             TP_SIN              CHAR(2),
             TP_RISC             CHAR(2),
             TP_MET_RISC         DECIMAL(2, 0),
             DT_LIQ              DATE,
             COD_DVS             CHAR(20),
             TOT_IMP_LRD_LIQTO   DECIMAL(15, 3),
             TOT_IMP_PREST       DECIMAL(15, 3),
             TOT_IMP_INTR_PREST  DECIMAL(15, 3),
             TOT_IMP_UTI         DECIMAL(15, 3),
             TOT_IMP_RIT_TFR     DECIMAL(15, 3),
             TOT_IMP_RIT_ACC     DECIMAL(15, 3),
             TOT_IMP_RIT_VIS     DECIMAL(15, 3),
             TOT_IMPB_TFR        DECIMAL(15, 3),
             TOT_IMPB_ACC        DECIMAL(15, 3),
             TOT_IMPB_VIS        DECIMAL(15, 3),
             TOT_IMP_RIMB        DECIMAL(15, 3),
             IMPB_IMPST_PRVR     DECIMAL(15, 3),
             IMPST_PRVR          DECIMAL(15, 3),
             IMPB_IMPST_252      DECIMAL(15, 3),
             IMPST_252           DECIMAL(15, 3),
             TOT_IMP_IS          DECIMAL(15, 3),
             IMP_DIR_LIQ         DECIMAL(15, 3),
             TOT_IMP_NET_LIQTO   DECIMAL(15, 3),
             MONT_END2000        DECIMAL(15, 3),
             MONT_END2006        DECIMAL(15, 3),
             PC_REN              DECIMAL(6, 3),
             IMP_PNL             DECIMAL(15, 3),
             IMPB_IRPEF          DECIMAL(15, 3),
             IMPST_IRPEF         DECIMAL(15, 3),
             DT_VLT              DATE,
             DT_END_ISTR         DATE,
             TP_RIMB             CHAR(2) NOT NULL,
             SPE_RCS             DECIMAL(15, 3),
             IB_LIQ              CHAR(40),
             TOT_IAS_ONER_PRVNT  DECIMAL(15, 3),
             TOT_IAS_MGG_SIN     DECIMAL(15, 3),
             TOT_IAS_RST_DPST    DECIMAL(15, 3),
             IMP_ONER_LIQ        DECIMAL(15, 3),
             COMPON_TAX_RIMB     DECIMAL(15, 3),
             TP_MEZ_PAG          CHAR(2),
             IMP_EXCONTR         DECIMAL(15, 3),
             IMP_INTR_RIT_PAG    DECIMAL(15, 3),
             BNS_NON_GODUTO      DECIMAL(15, 3),
             CNBT_INPSTFM        DECIMAL(15, 3),
             IMPST_DA_RIMB       DECIMAL(15, 3),
             IMPB_IS             DECIMAL(15, 3),
             TAX_SEP             DECIMAL(15, 3),
             IMPB_TAX_SEP        DECIMAL(15, 3),
             IMPB_INTR_SU_PREST  DECIMAL(15, 3),
             ADDIZ_COMUN         DECIMAL(15, 3),
             IMPB_ADDIZ_COMUN    DECIMAL(15, 3),
             ADDIZ_REGION        DECIMAL(15, 3),
             IMPB_ADDIZ_REGION   DECIMAL(15, 3),
             MONT_DAL2007        DECIMAL(15, 3),
             IMPB_CNBT_INPSTFM   DECIMAL(15, 3),
             IMP_LRD_DA_RIMB     DECIMAL(15, 3),
             IMP_DIR_DA_RIMB     DECIMAL(15, 3),
             RIS_MAT             DECIMAL(15, 3),
             RIS_SPE             DECIMAL(15, 3),
             DS_RIGA             DECIMAL(10, 0) NOT NULL WITH DEFAULT,
             DS_OPER_SQL         CHAR(1) NOT NULL WITH DEFAULT,
             DS_VER              DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             DS_TS_INI_CPTZ      DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_TS_END_CPTZ      DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_UTENTE           CHAR(20) NOT NULL WITH DEFAULT,
             DS_STATO_ELAB       CHAR(1) NOT NULL WITH DEFAULT,
             TOT_IAS_PNL         DECIMAL(15, 3),
             FL_EVE_GARTO        CHAR(1),
             IMP_REN_K1          DECIMAL(15, 3),
             IMP_REN_K2          DECIMAL(15, 3),
             IMP_REN_K3          DECIMAL(15, 3),
             PC_REN_K1           DECIMAL(6, 3),
             PC_REN_K2           DECIMAL(6, 3),
             PC_REN_K3           DECIMAL(6, 3),
             TP_CAUS_ANTIC       CHAR(2),
             IMP_LRD_LIQTO_RILT  DECIMAL(15, 3),
             IMPST_APPL_RILT     DECIMAL(15, 3),
             PC_RISC_PARZ        DECIMAL(12, 5),
             IMPST_BOLLO_TOT_V   DECIMAL(15, 3),
             IMPST_BOLLO_DETT_C  DECIMAL(15, 3),
             IMPST_BOLLO_TOT_SW  DECIMAL(15, 3),
             IMPST_BOLLO_TOT_AA  DECIMAL(15, 3),
             IMPB_VIS_1382011    DECIMAL(15, 3),
             IMPST_VIS_1382011   DECIMAL(15, 3),
             IMPB_IS_1382011     DECIMAL(15, 3),
             IMPST_SOST_1382011  DECIMAL(15, 3),
             PC_ABB_TIT_STAT     DECIMAL(6, 3),
             IMPB_BOLLO_DETT_C   DECIMAL(15, 3),
             FL_PRE_COMP         CHAR(1),
             IMPB_VIS_662014     DECIMAL(15, 3),
             IMPST_VIS_662014    DECIMAL(15, 3),
             IMPB_IS_662014      DECIMAL(15, 3),
             IMPST_SOST_662014   DECIMAL(15, 3),
             PC_ABB_TS_662014    DECIMAL(6, 3),
             IMP_LRD_CALC_CP     DECIMAL(15, 3),
             COS_TUNNEL_USCITA   DECIMAL(15, 3)
          ) END-EXEC.

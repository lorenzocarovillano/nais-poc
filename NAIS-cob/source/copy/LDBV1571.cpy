       01 LDBV1571.
          05 LDBV1571-TP-MOVI             PIC S9(005) COMP.
          05 LDBV1571-ID-POLI             PIC S9(009) COMP-3.
          05 LDBV1571-ID-ADES             PIC S9(009) COMP-3.
          05 LDBV1571-ID-OGG              PIC S9(009) COMP-3.
          05 LDBV1571-ID-GRZ              PIC S9(009) COMP-3.
          05 LDBV1571-ID-MOV-CRZ          PIC S9(009) COMP-3.
          05 LDBV1571-TP-OGG              PIC  X(002).
          05 LDBV1571-DT-EFF-OUT          PIC  9(008).

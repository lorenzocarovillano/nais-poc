      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA NUM_OGG
      *   ALIAS NOG
      *   ULTIMO AGG. 02 FEB 2009
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-DATI.
             07 (SF)-COD-COMPAGNIA-ANIA PIC S9(5)     COMP-3.
             07 (SF)-FORMA-ASSICURATIVA PIC X(2).
             07 (SF)-COD-OGGETTO PIC X(30).
             07 (SF)-TIPO-OGGETTO PIC X(2).
             07 (SF)-ULT-PROGR PIC S9(18)     COMP-3.
             07 (SF)-ULT-PROGR-NULL REDEFINES
                (SF)-ULT-PROGR   PIC X(10).
             07 (SF)-DESC-OGGETTO PIC X(250).

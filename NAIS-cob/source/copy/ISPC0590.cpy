      *----------------------------------------------------------------*
      *  Servizio -  Estratto Conto Diagnostico
      *----------------------------------------------------------------*
         02 ISPC0590-DATI-INPUT.
            05 ISPC0590-COD-COMPAGNIA-ANIA           PIC 9(005).
            05 ISPC0590-DATA-INIZ-ELAB               PIC X(008).
            05 ISPC0590-DATA-FINE-ELAB               PIC X(008).
            05 ISPC0590-DATA-RIFERIMENTO             PIC X(08).
            05 ISPC0590-LIVELLO-UTENTE               PIC 9(02).
            05 ISPC0590-FUNZIONALITA                 PIC 9(05).
            05 ISPC0590-SESSION-ID                   PIC X(20).

         02 ISPC0590-DATI-OUTPUT.
            05 ISPC0590-NUM-MAX-PROD                PIC 9(003).
            05 ISPC0590-TAB-PROD                OCCURS 250.
               07 ISPC0590-COD-TARI                  PIC X(012).
               07 ISPC0590-TASSO-TECNICO       PIC 9(05)V9(09).

          02 ISPC0590-AREA-ERRORI.
             03 ISPC0590-ESITO                       PIC 9(02).
             03 ISPC0590-ERR-NUM-ELE                 PIC 9(03).
             03 ISPC0590-TAB-ERRORI            OCCURS 10.
                05 ISPC0590-COD-ERR                  PIC X(12).
                05 ISPC0590-DESCRIZIONE-ERR          PIC X(50).
                05 ISPC0590-GRAVITA-ERR              PIC 9(02).
                05 ISPC0590-LIVELLO-DEROGA           PIC 9(02).


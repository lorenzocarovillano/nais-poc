       01 LDBV0291.
         05 LDBV0291-PUNTAT-ID-TIT       PIC S9(9)   COMP-3.
         05 LDBV0291-ID-MOVI-CRZ         PIC S9(9)   COMP-3.
         05 LDBV0291-ID-OGG              PIC S9(9)   COMP-3.
         05 LDBV0291-TIPO-OGG            PIC X(02).
         05 LDBV0291-STATO-TIT           PIC X(02).
         05 LDBV0291-TP-STAT-BUS         PIC X(02).
         05 LDBV0291-TP-CAUS             PIC X(02).
         05 LDBV0291-TP-TIT              PIC X(02).
         05 LDBV0291-DT-EMIS-TIT         PIC S9(8)  COMP-3.
         05 LDBV0291-TIPO-CURSORE        PIC X(02).

           EXEC SQL DECLARE TEMPORARY_DATA TABLE
           (
             ID_TEMPORARY_DATA   DECIMAL(10, 0) NOT NULL,
             ALIAS_STR_DATO      CHAR(30) NOT NULL,
             NUM_FRAME           DECIMAL(5, 0) NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             ID_SESSION          CHAR(20) NOT NULL,
             FL_CONTIGUOUS_DATA  CHAR(1),
             TOTAL_RECURRENCE    DECIMAL(5, 0),
             PARTIAL_RECURRENCE  DECIMAL(5, 0),
             ACTUAL_RECURRENCE   DECIMAL(5, 0),
             DS_OPER_SQL         CHAR(1) NOT NULL,
             DS_VER              DECIMAL(9, 0) NOT NULL,
             DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL,
             DS_UTENTE           CHAR(20) NOT NULL,
             DS_STATO_ELAB       CHAR(1) NOT NULL,
             TYPE_RECORD         CHAR(3),
             BUFFER_DATA         VARCHAR (32000)
          ) END-EXEC.

       01 IND-RIS-DI-TRCH.
          05 IND-RST-ID-MOVI-CHIU PIC S9(4) COMP-5.
          05 IND-RST-TP-CALC-RIS PIC S9(4) COMP-5.
          05 IND-RST-ULT-RM PIC S9(4) COMP-5.
          05 IND-RST-DT-CALC PIC S9(4) COMP-5.
          05 IND-RST-DT-ELAB PIC S9(4) COMP-5.
          05 IND-RST-RIS-BILA PIC S9(4) COMP-5.
          05 IND-RST-RIS-MAT PIC S9(4) COMP-5.
          05 IND-RST-INCR-X-RIVAL PIC S9(4) COMP-5.
          05 IND-RST-RPTO-PRE PIC S9(4) COMP-5.
          05 IND-RST-FRAZ-PRE-PP PIC S9(4) COMP-5.
          05 IND-RST-RIS-TOT PIC S9(4) COMP-5.
          05 IND-RST-RIS-SPE PIC S9(4) COMP-5.
          05 IND-RST-RIS-ABB PIC S9(4) COMP-5.
          05 IND-RST-RIS-BNSFDT PIC S9(4) COMP-5.
          05 IND-RST-RIS-SOPR PIC S9(4) COMP-5.
          05 IND-RST-RIS-INTEG-BAS-TEC PIC S9(4) COMP-5.
          05 IND-RST-RIS-INTEG-DECR-TS PIC S9(4) COMP-5.
          05 IND-RST-RIS-GAR-CASO-MOR PIC S9(4) COMP-5.
          05 IND-RST-RIS-ZIL PIC S9(4) COMP-5.
          05 IND-RST-RIS-FAIVL PIC S9(4) COMP-5.
          05 IND-RST-RIS-COS-AMMTZ PIC S9(4) COMP-5.
          05 IND-RST-RIS-SPE-FAIVL PIC S9(4) COMP-5.
          05 IND-RST-RIS-PREST-FAIVL PIC S9(4) COMP-5.
          05 IND-RST-RIS-COMPON-ASSVA PIC S9(4) COMP-5.
          05 IND-RST-ULT-COEFF-RIS PIC S9(4) COMP-5.
          05 IND-RST-ULT-COEFF-AGG-RIS PIC S9(4) COMP-5.
          05 IND-RST-RIS-ACQ PIC S9(4) COMP-5.
          05 IND-RST-RIS-UTI PIC S9(4) COMP-5.
          05 IND-RST-RIS-MAT-EFF PIC S9(4) COMP-5.
          05 IND-RST-RIS-RISTORNI-CAP PIC S9(4) COMP-5.
          05 IND-RST-RIS-TRM-BNS PIC S9(4) COMP-5.
          05 IND-RST-RIS-BNSRIC PIC S9(4) COMP-5.
          05 IND-RST-COD-FND PIC S9(4) COMP-5.
          05 IND-RST-RIS-MIN-GARTO PIC S9(4) COMP-5.
          05 IND-RST-RIS-RSH-DFLT PIC S9(4) COMP-5.
          05 IND-RST-RIS-MOVI-NON-INVES PIC S9(4) COMP-5.

       01 LDBV2651.
          05 LDBV2651-ID-OGG                   PIC S9(09) COMP-3.
          05 LDBV2651-TP-OGG                   PIC  X(02).
          05 LDBV2651-TP-MOV1                  PIC S9(5)  COMP-3.
          05 LDBV2651-TP-MOV2                  PIC S9(5)  COMP-3.
10819X    05 LDBV2651-TP-MOV3                  PIC S9(5)  COMP-3.
          05 LDBV2651-DT-INIZ                  PIC S9(8)  COMP-3.
          05 LDBV2651-DT-FINE                  PIC S9(8)  COMP-3.
          05 LDBV2651-DT-INIZ-DB               PIC X(10).
          05 LDBV2651-DT-FINE-DB               PIC X(10).

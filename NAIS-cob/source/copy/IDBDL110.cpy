           EXEC SQL DECLARE OGG_BLOCCO TABLE
           (
             ID_OGG_BLOCCO       DECIMAL(9, 0) NOT NULL,
             ID_OGG_1RIO         DECIMAL(9, 0),
             TP_OGG_1RIO         CHAR(2),
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             TP_MOVI             DECIMAL(5, 0) NOT NULL,
             TP_OGG              CHAR(2) NOT NULL,
             ID_OGG              DECIMAL(9, 0) NOT NULL,
             DT_EFF              DATE NOT NULL,
             TP_STAT_BLOCCO      CHAR(2) NOT NULL,
             COD_BLOCCO          CHAR(5) NOT NULL,
             ID_RICH             DECIMAL(9, 0),
             DS_OPER_SQL         CHAR(1) NOT NULL WITH DEFAULT,
             DS_VER              DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             DS_TS_CPTZ          DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_UTENTE           CHAR(20) NOT NULL WITH DEFAULT,
             DS_STATO_ELAB       CHAR(1) NOT NULL WITH DEFAULT
          ) END-EXEC.

      *----------------------------------------------------------------*
      *    COPY      ..... LCCVPVT6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO PROVVIGIONI DI TRANCHE
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVPVT5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCVPVT1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-PROV-DI-TRCH.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE PROV-DI-GAR

      *--> NOME TABELLA FISICA DB
           MOVE 'PROV-DI-GAR'                 TO WK-TABELLA

      *--> SE LO STATUS NON E' "INVARIATO"
           IF  NOT WPVT-ST-INV(IX-TAB-PVT)
           AND NOT WPVT-ST-CON(IX-TAB-PVT)
           AND     WPVT-ELE-PVT-MAX NOT = 0

              MOVE WPVT-ID-GAR(IX-TAB-PVT)
                TO WS-ID-PVT
              PERFORM RICERCA-ID-GRZ
                 THRU RICERCA-ID-GRZ-EX

      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WPVT-ST-ADD(IX-TAB-PVT)

      *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                     PERFORM ESTR-SEQUENCE
                        THRU ESTR-SEQUENCE-EX

                     IF IDSV0001-ESITO-OK

                         MOVE S090-SEQ-TABELLA
                           TO WPVT-ID-PTF(IX-TAB-PVT)
                              PVT-ID-PROV-DI-GAR
                         MOVE WS-ID-PTF-PVT  TO PVT-ID-GAR
                         MOVE WMOV-ID-PTF    TO PVT-ID-MOVI-CRZ
                     END-IF

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        MODIFICA
                  WHEN WPVT-ST-MOD(IX-TAB-PVT)

                       MOVE WPVT-ID-PTF(IX-TAB-PVT)
                         TO PVT-ID-PROV-DI-GAR
                       MOVE WS-ID-PTF-PVT
                         TO PVT-ID-GAR
                       MOVE WMOV-ID-PTF
                         TO PVT-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WPVT-ST-DEL(IX-TAB-PVT)

                       MOVE WPVT-ID-PTF(IX-TAB-PVT)
                         TO PVT-ID-PROV-DI-GAR
                       MOVE WS-ID-PTF-PVT
                         TO PVT-ID-GAR
                       MOVE WMOV-ID-PTF
                         TO PVT-ID-MOVI-CRZ

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN ADESIONE
                 PERFORM VAL-DCLGEN-PVT
                    THRU VAL-DCLGEN-PVT-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-PVT
                    THRU VALORIZZA-AREA-DSH-PVT-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-PROV-DI-TRCH-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-PVT.

      *--> DCLGEN TABELLA
           MOVE PROV-DI-GAR            TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-PVT-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    RICERCA PTF
      *----------------------------------------------------------------*
       RICERCA-ID-GRZ.

           SET NON-TROVATO TO TRUE.

           PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
                     UNTIL IX-TAB-GRZ > WGRZ-ELE-GAR-MAX
                        OR TROVATO

                  IF WGRZ-ID-GAR(IX-TAB-GRZ) = WS-ID-PVT
                     MOVE WGRZ-ID-PTF(IX-TAB-GRZ)
                       TO WS-ID-PTF-PVT
                     SET TROVATO TO TRUE
                  END-IF

           END-PERFORM.

       RICERCA-ID-GRZ-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVPVT5.

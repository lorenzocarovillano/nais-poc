
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVL714
      *   ULTIMO AGG. 22 APR 2010
      *------------------------------------------------------------

       INIZIA-TOT-L71.

           PERFORM INIZIA-ZEROES-L71 THRU INIZIA-ZEROES-L71-EX

           PERFORM INIZIA-SPACES-L71 THRU INIZIA-SPACES-L71-EX

           PERFORM INIZIA-NULL-L71 THRU INIZIA-NULL-L71-EX.

       INIZIA-TOT-L71-EX.
           EXIT.

       INIZIA-NULL-L71.
           MOVE HIGH-VALUES TO (SF)-COD-RAMO-NULL.
       INIZIA-NULL-L71-EX.
           EXIT.

       INIZIA-ZEROES-L71.
           MOVE 0 TO (SF)-ID-MATR-ELAB-BATCH
           MOVE 0 TO (SF)-COD-COMP-ANIA
           MOVE 0 TO (SF)-TP-MOVI
           MOVE 0 TO (SF)-DT-ULT-ELAB
           MOVE 0 TO (SF)-DS-VER
           MOVE 0 TO (SF)-DS-TS-CPTZ.
       INIZIA-ZEROES-L71-EX.
           EXIT.

       INIZIA-SPACES-L71.
           MOVE SPACES TO (SF)-TP-FRM-ASSVA
           MOVE SPACES TO (SF)-DS-OPER-SQL
           MOVE SPACES TO (SF)-DS-UTENTE
           MOVE SPACES TO (SF)-DS-STATO-ELAB.
       INIZIA-SPACES-L71-EX.
           EXIT.

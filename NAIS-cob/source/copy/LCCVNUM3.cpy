
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVNUM3
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-NUM.
           MOVE NUM-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE NUM-ID-OGG
             TO (SF)-ID-OGG
           MOVE NUM-TP-OGG
             TO (SF)-TP-OGG
           IF NUM-ULT-NUM-LIN-NULL = HIGH-VALUES
              MOVE NUM-ULT-NUM-LIN-NULL
                TO (SF)-ULT-NUM-LIN-NULL
           ELSE
              MOVE NUM-ULT-NUM-LIN
                TO (SF)-ULT-NUM-LIN
           END-IF
           MOVE NUM-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE NUM-DS-VER
             TO (SF)-DS-VER
           MOVE NUM-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE NUM-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE NUM-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB.
       VALORIZZA-OUTPUT-NUM-EX.
           EXIT.

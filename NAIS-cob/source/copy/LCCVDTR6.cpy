      *----------------------------------------------------------------*
      *    COPY      ..... LCCVDTR6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO DETTAGLIO TITOLO DI RATA
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN (LCCV*1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*
       AGGIORNA-DETT-TIT-DI-RAT.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE DETT-TIT-DI-RAT.

      *--> NOME TABELLA FISICA DB
           MOVE 'DETT-TIT-DI-RAT'                  TO WK-TABELLA.

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WDTR-ST-INV(IX-TAB-DTR)
              AND WDTR-ELE-DTR-MAX NOT = 0

      *--->   Impostare ID Tabella DETTAGLIO TITOLO DI RATA
              MOVE WMOV-ID-PTF
                TO DTR-ID-MOVI-CRZ

              MOVE WDTR-ID-TIT-RAT(IX-TAB-DTR)
                TO WS-ID-TIT-RAT

      *-->    RICERCA DELL'ID-TIT-DI-RATA NELLA TABELLA PADRE
              PERFORM RICERCA-TIT-RAT-PTF
                 THRU RICERCA-TIT-RAT-PTF-EX

              EVALUATE TRUE
      *-->        INSERIMENTO
                  WHEN WDTR-ST-ADD(IX-TAB-DTR)

                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA
                            TO WDTR-ID-PTF(IX-TAB-DTR)
                               DTR-ID-DETT-TIT-DI-RAT
                          MOVE WS-ID-PTF-TIT-RAT
                            TO DTR-ID-TIT-RAT

      *-->       ESTRAZIONE OGGETTO PTF
                          PERFORM PREPARA-AREA-LCCS0234-DTR
                             THRU PREPARA-AREA-LCCS0234-DTR-EX
                          PERFORM CALL-LCCS0234
                             THRU CALL-LCCS0234-EX

                          IF IDSV0001-ESITO-OK
                             MOVE S234-ID-OGG-PTF-EOC
                               TO DTR-ID-OGG
                          END-IF
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE

      *-->        CANCELLAZIONE
                  WHEN WDTR-ST-DEL(IX-TAB-DTR)

                     MOVE WDTR-ID-PTF(IX-TAB-DTR)
                       TO DTR-ID-DETT-TIT-DI-RAT
                     MOVE WDTR-ID-TIT-RAT(IX-TAB-DTR)
                       TO DTR-ID-TIT-RAT
                     MOVE WDTR-ID-OGG(IX-TAB-DTR)
                       TO DTR-ID-OGG

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-DELETE-LOGICA    TO TRUE

      *-->        MODIFICA
                  WHEN WDTR-ST-MOD(IX-TAB-DTR)
                     MOVE WDTR-ID-PTF(IX-TAB-DTR)
                       TO DTR-ID-DETT-TIT-DI-RAT
                     MOVE WDTR-ID-TIT-RAT(IX-TAB-DTR)
                       TO DTR-ID-TIT-RAT
                     MOVE WDTR-ID-OGG(IX-TAB-DTR)
                       TO DTR-ID-OGG

      *-->           TIPO OPERAZIONE DISPATCHER
                     SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN DETTAGLIO TITOLO DI RATA
                 PERFORM VAL-DCLGEN-DTR
                    THRU VAL-DCLGEN-DTR-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-DTR
                    THRU VALORIZZA-AREA-DSH-DTR-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF
           END-IF.

       AGGIORNA-DETT-TIT-DI-RAT-EX.
           EXIT.
      *----------------------------------------------------------------*
      *     RICERCA DELL' ID-TIT-RAT-PTF DELLA TABELLA DETT. TIT.RATA
      *     NELLA TABELLA TITOLO DI RATA
      *----------------------------------------------------------------*
       RICERCA-TIT-RAT-PTF.

           SET NON-TROVATO TO TRUE.

            PERFORM VARYING IX-TAB-TDR FROM 1 BY 1
              UNTIL IX-TAB-TDR > WTDR-ELE-TDR-MAX
                 OR TROVATO

              IF WS-ID-TIT-RAT = WTDR-ID-TIT-RAT(IX-TAB-TDR)
                 MOVE WTDR-ID-PTF(IX-TAB-TDR)  TO WS-ID-PTF-TIT-RAT
                 SET  TROVATO                  TO TRUE
              END-IF
            END-PERFORM.

       RICERCA-TIT-RAT-PTF-EX.
           EXIT.

      *----------------------------------------------------------------*
      *   PREPARA PER LA CALL ALL'LCCS0234 ESTRATTORE OGGETTO PTF
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234-DTR.

           MOVE WDTR-ID-OGG(IX-TAB-DTR)     TO S234-ID-OGG-EOC
           MOVE WDTR-TP-OGG(IX-TAB-DTR)     TO S234-TIPO-OGG-EOC.

       PREPARA-AREA-LCCS0234-DTR-EX.
           EXIT.
      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-DTR.

      *--> DCLGEN TABELLA
           MOVE DETT-TIT-DI-RAT         TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-DTR-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVDTR5.

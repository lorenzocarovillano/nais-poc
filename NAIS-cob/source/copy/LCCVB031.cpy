      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA BILA_TRCH_ESTR
      *   ALIAS B03
      *   ULTIMO AGG. 12 SET 2014
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-BILA-TRCH-ESTR PIC S9(9)     COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-ID-RICH-ESTRAZ-MAS PIC S9(9)     COMP-3.
             07 (SF)-ID-RICH-ESTRAZ-AGG PIC S9(9)     COMP-3.
             07 (SF)-ID-RICH-ESTRAZ-AGG-NULL REDEFINES
                (SF)-ID-RICH-ESTRAZ-AGG   PIC X(5).
             07 (SF)-FL-SIMULAZIONE PIC X(1).
             07 (SF)-FL-SIMULAZIONE-NULL REDEFINES
                (SF)-FL-SIMULAZIONE   PIC X(1).
             07 (SF)-DT-RIS   PIC S9(8) COMP-3.
             07 (SF)-DT-PRODUZIONE   PIC S9(8) COMP-3.
             07 (SF)-ID-POLI PIC S9(9)     COMP-3.
             07 (SF)-ID-ADES PIC S9(9)     COMP-3.
             07 (SF)-ID-GAR PIC S9(9)     COMP-3.
             07 (SF)-ID-TRCH-DI-GAR PIC S9(9)     COMP-3.
             07 (SF)-TP-FRM-ASSVA PIC X(2).
             07 (SF)-TP-RAMO-BILA PIC X(2).
             07 (SF)-TP-CALC-RIS PIC X(2).
             07 (SF)-COD-RAMO PIC X(12).
             07 (SF)-COD-TARI PIC X(12).
             07 (SF)-DT-INI-VAL-TAR   PIC S9(8) COMP-3.
             07 (SF)-DT-INI-VAL-TAR-NULL REDEFINES
                (SF)-DT-INI-VAL-TAR   PIC X(5).
             07 (SF)-COD-PROD PIC X(12).
             07 (SF)-COD-PROD-NULL REDEFINES
                (SF)-COD-PROD   PIC X(12).
             07 (SF)-DT-INI-VLDT-PROD   PIC S9(8) COMP-3.
             07 (SF)-COD-TARI-ORGN PIC X(12).
             07 (SF)-COD-TARI-ORGN-NULL REDEFINES
                (SF)-COD-TARI-ORGN   PIC X(12).
             07 (SF)-MIN-GARTO-T PIC S9(12)V9(3) COMP-3.
             07 (SF)-MIN-GARTO-T-NULL REDEFINES
                (SF)-MIN-GARTO-T   PIC X(8).
             07 (SF)-TP-TARI PIC X(2).
             07 (SF)-TP-TARI-NULL REDEFINES
                (SF)-TP-TARI   PIC X(2).
             07 (SF)-TP-PRE PIC X(1).
             07 (SF)-TP-PRE-NULL REDEFINES
                (SF)-TP-PRE   PIC X(1).
             07 (SF)-TP-ADEG-PRE PIC X(1).
             07 (SF)-TP-ADEG-PRE-NULL REDEFINES
                (SF)-TP-ADEG-PRE   PIC X(1).
             07 (SF)-TP-RIVAL PIC X(2).
             07 (SF)-TP-RIVAL-NULL REDEFINES
                (SF)-TP-RIVAL   PIC X(2).
             07 (SF)-FL-DA-TRASF PIC X(1).
             07 (SF)-FL-DA-TRASF-NULL REDEFINES
                (SF)-FL-DA-TRASF   PIC X(1).
             07 (SF)-FL-CAR-CONT PIC X(1).
             07 (SF)-FL-CAR-CONT-NULL REDEFINES
                (SF)-FL-CAR-CONT   PIC X(1).
             07 (SF)-FL-PRE-DA-RIS PIC X(1).
             07 (SF)-FL-PRE-DA-RIS-NULL REDEFINES
                (SF)-FL-PRE-DA-RIS   PIC X(1).
             07 (SF)-FL-PRE-AGG PIC X(1).
             07 (SF)-FL-PRE-AGG-NULL REDEFINES
                (SF)-FL-PRE-AGG   PIC X(1).
             07 (SF)-TP-TRCH PIC X(2).
             07 (SF)-TP-TRCH-NULL REDEFINES
                (SF)-TP-TRCH   PIC X(2).
             07 (SF)-TP-TST PIC X(2).
             07 (SF)-TP-TST-NULL REDEFINES
                (SF)-TP-TST   PIC X(2).
             07 (SF)-COD-CONV PIC X(12).
             07 (SF)-COD-CONV-NULL REDEFINES
                (SF)-COD-CONV   PIC X(12).
             07 (SF)-DT-DECOR-POLI   PIC S9(8) COMP-3.
             07 (SF)-DT-DECOR-ADES   PIC S9(8) COMP-3.
             07 (SF)-DT-DECOR-ADES-NULL REDEFINES
                (SF)-DT-DECOR-ADES   PIC X(5).
             07 (SF)-DT-DECOR-TRCH   PIC S9(8) COMP-3.
             07 (SF)-DT-EMIS-POLI   PIC S9(8) COMP-3.
             07 (SF)-DT-EMIS-TRCH   PIC S9(8) COMP-3.
             07 (SF)-DT-EMIS-TRCH-NULL REDEFINES
                (SF)-DT-EMIS-TRCH   PIC X(5).
             07 (SF)-DT-SCAD-TRCH   PIC S9(8) COMP-3.
             07 (SF)-DT-SCAD-TRCH-NULL REDEFINES
                (SF)-DT-SCAD-TRCH   PIC X(5).
             07 (SF)-DT-SCAD-INTMD   PIC S9(8) COMP-3.
             07 (SF)-DT-SCAD-INTMD-NULL REDEFINES
                (SF)-DT-SCAD-INTMD   PIC X(5).
             07 (SF)-DT-SCAD-PAG-PRE   PIC S9(8) COMP-3.
             07 (SF)-DT-SCAD-PAG-PRE-NULL REDEFINES
                (SF)-DT-SCAD-PAG-PRE   PIC X(5).
             07 (SF)-DT-ULT-PRE-PAG   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-PRE-PAG-NULL REDEFINES
                (SF)-DT-ULT-PRE-PAG   PIC X(5).
             07 (SF)-DT-NASC-1O-ASSTO   PIC S9(8) COMP-3.
             07 (SF)-DT-NASC-1O-ASSTO-NULL REDEFINES
                (SF)-DT-NASC-1O-ASSTO   PIC X(5).
             07 (SF)-SEX-1O-ASSTO PIC X(1).
             07 (SF)-SEX-1O-ASSTO-NULL REDEFINES
                (SF)-SEX-1O-ASSTO   PIC X(1).
             07 (SF)-ETA-AA-1O-ASSTO PIC S9(5)     COMP-3.
             07 (SF)-ETA-AA-1O-ASSTO-NULL REDEFINES
                (SF)-ETA-AA-1O-ASSTO   PIC X(3).
             07 (SF)-ETA-MM-1O-ASSTO PIC S9(5)     COMP-3.
             07 (SF)-ETA-MM-1O-ASSTO-NULL REDEFINES
                (SF)-ETA-MM-1O-ASSTO   PIC X(3).
             07 (SF)-ETA-RAGGN-DT-CALC PIC S9(4)V9(3) COMP-3.
             07 (SF)-ETA-RAGGN-DT-CALC-NULL REDEFINES
                (SF)-ETA-RAGGN-DT-CALC   PIC X(4).
             07 (SF)-DUR-AA PIC S9(5)     COMP-3.
             07 (SF)-DUR-AA-NULL REDEFINES
                (SF)-DUR-AA   PIC X(3).
             07 (SF)-DUR-MM PIC S9(5)     COMP-3.
             07 (SF)-DUR-MM-NULL REDEFINES
                (SF)-DUR-MM   PIC X(3).
             07 (SF)-DUR-GG PIC S9(5)     COMP-3.
             07 (SF)-DUR-GG-NULL REDEFINES
                (SF)-DUR-GG   PIC X(3).
             07 (SF)-DUR-1O-PER-AA PIC S9(5)     COMP-3.
             07 (SF)-DUR-1O-PER-AA-NULL REDEFINES
                (SF)-DUR-1O-PER-AA   PIC X(3).
             07 (SF)-DUR-1O-PER-MM PIC S9(5)     COMP-3.
             07 (SF)-DUR-1O-PER-MM-NULL REDEFINES
                (SF)-DUR-1O-PER-MM   PIC X(3).
             07 (SF)-DUR-1O-PER-GG PIC S9(5)     COMP-3.
             07 (SF)-DUR-1O-PER-GG-NULL REDEFINES
                (SF)-DUR-1O-PER-GG   PIC X(3).
             07 (SF)-ANTIDUR-RICOR-PREC PIC S9(5)     COMP-3.
             07 (SF)-ANTIDUR-RICOR-PREC-NULL REDEFINES
                (SF)-ANTIDUR-RICOR-PREC   PIC X(3).
             07 (SF)-ANTIDUR-DT-CALC PIC S9(4)V9(7) COMP-3.
             07 (SF)-ANTIDUR-DT-CALC-NULL REDEFINES
                (SF)-ANTIDUR-DT-CALC   PIC X(6).
             07 (SF)-DUR-RES-DT-CALC PIC S9(4)V9(7) COMP-3.
             07 (SF)-DUR-RES-DT-CALC-NULL REDEFINES
                (SF)-DUR-RES-DT-CALC   PIC X(6).
             07 (SF)-TP-STAT-BUS-POLI PIC X(2).
             07 (SF)-TP-CAUS-POLI PIC X(2).
             07 (SF)-TP-STAT-BUS-ADES PIC X(2).
             07 (SF)-TP-CAUS-ADES PIC X(2).
             07 (SF)-TP-STAT-BUS-TRCH PIC X(2).
             07 (SF)-TP-CAUS-TRCH PIC X(2).
             07 (SF)-DT-EFF-CAMB-STAT   PIC S9(8) COMP-3.
             07 (SF)-DT-EFF-CAMB-STAT-NULL REDEFINES
                (SF)-DT-EFF-CAMB-STAT   PIC X(5).
             07 (SF)-DT-EMIS-CAMB-STAT   PIC S9(8) COMP-3.
             07 (SF)-DT-EMIS-CAMB-STAT-NULL REDEFINES
                (SF)-DT-EMIS-CAMB-STAT   PIC X(5).
             07 (SF)-DT-EFF-STAB   PIC S9(8) COMP-3.
             07 (SF)-DT-EFF-STAB-NULL REDEFINES
                (SF)-DT-EFF-STAB   PIC X(5).
             07 (SF)-CPT-DT-STAB PIC S9(12)V9(3) COMP-3.
             07 (SF)-CPT-DT-STAB-NULL REDEFINES
                (SF)-CPT-DT-STAB   PIC X(8).
             07 (SF)-DT-EFF-RIDZ   PIC S9(8) COMP-3.
             07 (SF)-DT-EFF-RIDZ-NULL REDEFINES
                (SF)-DT-EFF-RIDZ   PIC X(5).
             07 (SF)-DT-EMIS-RIDZ   PIC S9(8) COMP-3.
             07 (SF)-DT-EMIS-RIDZ-NULL REDEFINES
                (SF)-DT-EMIS-RIDZ   PIC X(5).
             07 (SF)-CPT-DT-RIDZ PIC S9(12)V9(3) COMP-3.
             07 (SF)-CPT-DT-RIDZ-NULL REDEFINES
                (SF)-CPT-DT-RIDZ   PIC X(8).
             07 (SF)-FRAZ PIC S9(5)     COMP-3.
             07 (SF)-FRAZ-NULL REDEFINES
                (SF)-FRAZ   PIC X(3).
             07 (SF)-DUR-PAG-PRE PIC S9(5)     COMP-3.
             07 (SF)-DUR-PAG-PRE-NULL REDEFINES
                (SF)-DUR-PAG-PRE   PIC X(3).
             07 (SF)-NUM-PRE-PATT PIC S9(5)     COMP-3.
             07 (SF)-NUM-PRE-PATT-NULL REDEFINES
                (SF)-NUM-PRE-PATT   PIC X(3).
             07 (SF)-FRAZ-INI-EROG-REN PIC S9(5)     COMP-3.
             07 (SF)-FRAZ-INI-EROG-REN-NULL REDEFINES
                (SF)-FRAZ-INI-EROG-REN   PIC X(3).
             07 (SF)-AA-REN-CER PIC S9(5)     COMP-3.
             07 (SF)-AA-REN-CER-NULL REDEFINES
                (SF)-AA-REN-CER   PIC X(3).
             07 (SF)-RAT-REN PIC S9(12)V9(3) COMP-3.
             07 (SF)-RAT-REN-NULL REDEFINES
                (SF)-RAT-REN   PIC X(8).
             07 (SF)-COD-DIV PIC X(20).
             07 (SF)-COD-DIV-NULL REDEFINES
                (SF)-COD-DIV   PIC X(20).
             07 (SF)-RISCPAR PIC S9(12)V9(3) COMP-3.
             07 (SF)-RISCPAR-NULL REDEFINES
                (SF)-RISCPAR   PIC X(8).
             07 (SF)-CUM-RISCPAR PIC S9(12)V9(3) COMP-3.
             07 (SF)-CUM-RISCPAR-NULL REDEFINES
                (SF)-CUM-RISCPAR   PIC X(8).
             07 (SF)-ULT-RM PIC S9(12)V9(3) COMP-3.
             07 (SF)-ULT-RM-NULL REDEFINES
                (SF)-ULT-RM   PIC X(8).
             07 (SF)-TS-RENDTO-T PIC S9(5)V9(9) COMP-3.
             07 (SF)-TS-RENDTO-T-NULL REDEFINES
                (SF)-TS-RENDTO-T   PIC X(8).
             07 (SF)-ALQ-RETR-T PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-RETR-T-NULL REDEFINES
                (SF)-ALQ-RETR-T   PIC X(4).
             07 (SF)-MIN-TRNUT-T PIC S9(12)V9(3) COMP-3.
             07 (SF)-MIN-TRNUT-T-NULL REDEFINES
                (SF)-MIN-TRNUT-T   PIC X(8).
             07 (SF)-TS-NET-T PIC S9(5)V9(9) COMP-3.
             07 (SF)-TS-NET-T-NULL REDEFINES
                (SF)-TS-NET-T   PIC X(8).
             07 (SF)-DT-ULT-RIVAL   PIC S9(8) COMP-3.
             07 (SF)-DT-ULT-RIVAL-NULL REDEFINES
                (SF)-DT-ULT-RIVAL   PIC X(5).
             07 (SF)-PRSTZ-INI PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-INI-NULL REDEFINES
                (SF)-PRSTZ-INI   PIC X(8).
             07 (SF)-PRSTZ-AGG-INI PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-AGG-INI-NULL REDEFINES
                (SF)-PRSTZ-AGG-INI   PIC X(8).
             07 (SF)-PRSTZ-AGG-ULT PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-AGG-ULT-NULL REDEFINES
                (SF)-PRSTZ-AGG-ULT   PIC X(8).
             07 (SF)-RAPPEL PIC S9(12)V9(3) COMP-3.
             07 (SF)-RAPPEL-NULL REDEFINES
                (SF)-RAPPEL   PIC X(8).
             07 (SF)-PRE-PATTUITO-INI PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-PATTUITO-INI-NULL REDEFINES
                (SF)-PRE-PATTUITO-INI   PIC X(8).
             07 (SF)-PRE-DOV-INI PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-DOV-INI-NULL REDEFINES
                (SF)-PRE-DOV-INI   PIC X(8).
             07 (SF)-PRE-DOV-RIVTO-T PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-DOV-RIVTO-T-NULL REDEFINES
                (SF)-PRE-DOV-RIVTO-T   PIC X(8).
             07 (SF)-PRE-ANNUALIZ-RICOR PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-ANNUALIZ-RICOR-NULL REDEFINES
                (SF)-PRE-ANNUALIZ-RICOR   PIC X(8).
             07 (SF)-PRE-CONT PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-CONT-NULL REDEFINES
                (SF)-PRE-CONT   PIC X(8).
             07 (SF)-PRE-PP-INI PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-PP-INI-NULL REDEFINES
                (SF)-PRE-PP-INI   PIC X(8).
             07 (SF)-RIS-PURA-T PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-PURA-T-NULL REDEFINES
                (SF)-RIS-PURA-T   PIC X(8).
             07 (SF)-PROV-ACQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-ACQ-NULL REDEFINES
                (SF)-PROV-ACQ   PIC X(8).
             07 (SF)-PROV-ACQ-RICOR PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-ACQ-RICOR-NULL REDEFINES
                (SF)-PROV-ACQ-RICOR   PIC X(8).
             07 (SF)-PROV-INC PIC S9(12)V9(3) COMP-3.
             07 (SF)-PROV-INC-NULL REDEFINES
                (SF)-PROV-INC   PIC X(8).
             07 (SF)-CAR-ACQ-NON-SCON PIC S9(12)V9(3) COMP-3.
             07 (SF)-CAR-ACQ-NON-SCON-NULL REDEFINES
                (SF)-CAR-ACQ-NON-SCON   PIC X(8).
             07 (SF)-OVER-COMM PIC S9(12)V9(3) COMP-3.
             07 (SF)-OVER-COMM-NULL REDEFINES
                (SF)-OVER-COMM   PIC X(8).
             07 (SF)-CAR-ACQ-PRECONTATO PIC S9(12)V9(3) COMP-3.
             07 (SF)-CAR-ACQ-PRECONTATO-NULL REDEFINES
                (SF)-CAR-ACQ-PRECONTATO   PIC X(8).
             07 (SF)-RIS-ACQ-T PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-ACQ-T-NULL REDEFINES
                (SF)-RIS-ACQ-T   PIC X(8).
             07 (SF)-RIS-ZIL-T PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-ZIL-T-NULL REDEFINES
                (SF)-RIS-ZIL-T   PIC X(8).
             07 (SF)-CAR-GEST-NON-SCON PIC S9(12)V9(3) COMP-3.
             07 (SF)-CAR-GEST-NON-SCON-NULL REDEFINES
                (SF)-CAR-GEST-NON-SCON   PIC X(8).
             07 (SF)-CAR-GEST PIC S9(12)V9(3) COMP-3.
             07 (SF)-CAR-GEST-NULL REDEFINES
                (SF)-CAR-GEST   PIC X(8).
             07 (SF)-RIS-SPE-T PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-SPE-T-NULL REDEFINES
                (SF)-RIS-SPE-T   PIC X(8).
             07 (SF)-CAR-INC-NON-SCON PIC S9(12)V9(3) COMP-3.
             07 (SF)-CAR-INC-NON-SCON-NULL REDEFINES
                (SF)-CAR-INC-NON-SCON   PIC X(8).
             07 (SF)-CAR-INC PIC S9(12)V9(3) COMP-3.
             07 (SF)-CAR-INC-NULL REDEFINES
                (SF)-CAR-INC   PIC X(8).
             07 (SF)-RIS-RISTORNI-CAP PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-RISTORNI-CAP-NULL REDEFINES
                (SF)-RIS-RISTORNI-CAP   PIC X(8).
             07 (SF)-INTR-TECN PIC S9(3)V9(3) COMP-3.
             07 (SF)-INTR-TECN-NULL REDEFINES
                (SF)-INTR-TECN   PIC X(4).
             07 (SF)-CPT-RSH-MOR PIC S9(12)V9(3) COMP-3.
             07 (SF)-CPT-RSH-MOR-NULL REDEFINES
                (SF)-CPT-RSH-MOR   PIC X(8).
             07 (SF)-C-SUBRSH-T PIC S9(12)V9(3) COMP-3.
             07 (SF)-C-SUBRSH-T-NULL REDEFINES
                (SF)-C-SUBRSH-T   PIC X(8).
             07 (SF)-PRE-RSH-T PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-RSH-T-NULL REDEFINES
                (SF)-PRE-RSH-T   PIC X(8).
             07 (SF)-ALQ-MARG-RIS PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-MARG-RIS-NULL REDEFINES
                (SF)-ALQ-MARG-RIS   PIC X(4).
             07 (SF)-ALQ-MARG-C-SUBRSH PIC S9(3)V9(3) COMP-3.
             07 (SF)-ALQ-MARG-C-SUBRSH-NULL REDEFINES
                (SF)-ALQ-MARG-C-SUBRSH   PIC X(4).
             07 (SF)-TS-RENDTO-SPPR PIC S9(5)V9(9) COMP-3.
             07 (SF)-TS-RENDTO-SPPR-NULL REDEFINES
                (SF)-TS-RENDTO-SPPR   PIC X(8).
             07 (SF)-TP-IAS PIC X(2).
             07 (SF)-TP-IAS-NULL REDEFINES
                (SF)-TP-IAS   PIC X(2).
             07 (SF)-NS-QUO PIC S9(3)V9(3) COMP-3.
             07 (SF)-NS-QUO-NULL REDEFINES
                (SF)-NS-QUO   PIC X(4).
             07 (SF)-TS-MEDIO PIC S9(5)V9(9) COMP-3.
             07 (SF)-TS-MEDIO-NULL REDEFINES
                (SF)-TS-MEDIO   PIC X(8).
             07 (SF)-CPT-RIASTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-CPT-RIASTO-NULL REDEFINES
                (SF)-CPT-RIASTO   PIC X(8).
             07 (SF)-PRE-RIASTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-RIASTO-NULL REDEFINES
                (SF)-PRE-RIASTO   PIC X(8).
             07 (SF)-RIS-RIASTA PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-RIASTA-NULL REDEFINES
                (SF)-RIS-RIASTA   PIC X(8).
             07 (SF)-CPT-RIASTO-ECC PIC S9(12)V9(3) COMP-3.
             07 (SF)-CPT-RIASTO-ECC-NULL REDEFINES
                (SF)-CPT-RIASTO-ECC   PIC X(8).
             07 (SF)-PRE-RIASTO-ECC PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-RIASTO-ECC-NULL REDEFINES
                (SF)-PRE-RIASTO-ECC   PIC X(8).
             07 (SF)-RIS-RIASTA-ECC PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-RIASTA-ECC-NULL REDEFINES
                (SF)-RIS-RIASTA-ECC   PIC X(8).
             07 (SF)-COD-AGE PIC S9(5)     COMP-3.
             07 (SF)-COD-AGE-NULL REDEFINES
                (SF)-COD-AGE   PIC X(3).
             07 (SF)-COD-SUBAGE PIC S9(5)     COMP-3.
             07 (SF)-COD-SUBAGE-NULL REDEFINES
                (SF)-COD-SUBAGE   PIC X(3).
             07 (SF)-COD-CAN PIC S9(5)     COMP-3.
             07 (SF)-COD-CAN-NULL REDEFINES
                (SF)-COD-CAN   PIC X(3).
             07 (SF)-IB-POLI PIC X(40).
             07 (SF)-IB-POLI-NULL REDEFINES
                (SF)-IB-POLI   PIC X(40).
             07 (SF)-IB-ADES PIC X(40).
             07 (SF)-IB-ADES-NULL REDEFINES
                (SF)-IB-ADES   PIC X(40).
             07 (SF)-IB-TRCH-DI-GAR PIC X(40).
             07 (SF)-IB-TRCH-DI-GAR-NULL REDEFINES
                (SF)-IB-TRCH-DI-GAR   PIC X(40).
             07 (SF)-TP-PRSTZ PIC X(2).
             07 (SF)-TP-PRSTZ-NULL REDEFINES
                (SF)-TP-PRSTZ   PIC X(2).
             07 (SF)-TP-TRASF PIC X(2).
             07 (SF)-TP-TRASF-NULL REDEFINES
                (SF)-TP-TRASF   PIC X(2).
             07 (SF)-PP-INVRIO-TARI PIC X(1).
             07 (SF)-PP-INVRIO-TARI-NULL REDEFINES
                (SF)-PP-INVRIO-TARI   PIC X(1).
             07 (SF)-COEFF-OPZ-REN PIC S9(3)V9(3) COMP-3.
             07 (SF)-COEFF-OPZ-REN-NULL REDEFINES
                (SF)-COEFF-OPZ-REN   PIC X(4).
             07 (SF)-COEFF-OPZ-CPT PIC S9(3)V9(3) COMP-3.
             07 (SF)-COEFF-OPZ-CPT-NULL REDEFINES
                (SF)-COEFF-OPZ-CPT   PIC X(4).
             07 (SF)-DUR-PAG-REN PIC S9(5)     COMP-3.
             07 (SF)-DUR-PAG-REN-NULL REDEFINES
                (SF)-DUR-PAG-REN   PIC X(3).
             07 (SF)-VLT PIC X(3).
             07 (SF)-VLT-NULL REDEFINES
                (SF)-VLT   PIC X(3).
             07 (SF)-RIS-MAT-CHIU-PREC PIC S9(12)V9(3) COMP-3.
             07 (SF)-RIS-MAT-CHIU-PREC-NULL REDEFINES
                (SF)-RIS-MAT-CHIU-PREC   PIC X(8).
             07 (SF)-COD-FND PIC X(12).
             07 (SF)-COD-FND-NULL REDEFINES
                (SF)-COD-FND   PIC X(12).
             07 (SF)-PRSTZ-T PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRSTZ-T-NULL REDEFINES
                (SF)-PRSTZ-T   PIC X(8).
             07 (SF)-TS-TARI-DOV PIC S9(5)V9(9) COMP-3.
             07 (SF)-TS-TARI-DOV-NULL REDEFINES
                (SF)-TS-TARI-DOV   PIC X(8).
             07 (SF)-TS-TARI-SCON PIC S9(5)V9(9) COMP-3.
             07 (SF)-TS-TARI-SCON-NULL REDEFINES
                (SF)-TS-TARI-SCON   PIC X(8).
             07 (SF)-TS-PP PIC S9(5)V9(9) COMP-3.
             07 (SF)-TS-PP-NULL REDEFINES
                (SF)-TS-PP   PIC X(8).
             07 (SF)-COEFF-RIS-1-T PIC S9(5)V9(9) COMP-3.
             07 (SF)-COEFF-RIS-1-T-NULL REDEFINES
                (SF)-COEFF-RIS-1-T   PIC X(8).
             07 (SF)-COEFF-RIS-2-T PIC S9(5)V9(9) COMP-3.
             07 (SF)-COEFF-RIS-2-T-NULL REDEFINES
                (SF)-COEFF-RIS-2-T   PIC X(8).
             07 (SF)-ABB PIC S9(12)V9(3) COMP-3.
             07 (SF)-ABB-NULL REDEFINES
                (SF)-ABB   PIC X(8).
             07 (SF)-TP-COASS PIC X(2).
             07 (SF)-TP-COASS-NULL REDEFINES
                (SF)-TP-COASS   PIC X(2).
             07 (SF)-TRAT-RIASS PIC X(12).
             07 (SF)-TRAT-RIASS-NULL REDEFINES
                (SF)-TRAT-RIASS   PIC X(12).
             07 (SF)-TRAT-RIASS-ECC PIC X(12).
             07 (SF)-TRAT-RIASS-ECC-NULL REDEFINES
                (SF)-TRAT-RIASS-ECC   PIC X(12).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-TP-RGM-FISC PIC X(2).
             07 (SF)-TP-RGM-FISC-NULL REDEFINES
                (SF)-TP-RGM-FISC   PIC X(2).
             07 (SF)-DUR-GAR-AA PIC S9(5)     COMP-3.
             07 (SF)-DUR-GAR-AA-NULL REDEFINES
                (SF)-DUR-GAR-AA   PIC X(3).
             07 (SF)-DUR-GAR-MM PIC S9(5)     COMP-3.
             07 (SF)-DUR-GAR-MM-NULL REDEFINES
                (SF)-DUR-GAR-MM   PIC X(3).
             07 (SF)-DUR-GAR-GG PIC S9(5)     COMP-3.
             07 (SF)-DUR-GAR-GG-NULL REDEFINES
                (SF)-DUR-GAR-GG   PIC X(3).
             07 (SF)-ANTIDUR-CALC-365 PIC S9(4)V9(7) COMP-3.
             07 (SF)-ANTIDUR-CALC-365-NULL REDEFINES
                (SF)-ANTIDUR-CALC-365   PIC X(6).
             07 (SF)-COD-FISC-CNTR PIC X(16).
             07 (SF)-COD-FISC-CNTR-NULL REDEFINES
                (SF)-COD-FISC-CNTR   PIC X(16).
             07 (SF)-COD-FISC-ASSTO1 PIC X(16).
             07 (SF)-COD-FISC-ASSTO1-NULL REDEFINES
                (SF)-COD-FISC-ASSTO1   PIC X(16).
             07 (SF)-COD-FISC-ASSTO2 PIC X(16).
             07 (SF)-COD-FISC-ASSTO2-NULL REDEFINES
                (SF)-COD-FISC-ASSTO2   PIC X(16).
             07 (SF)-COD-FISC-ASSTO3 PIC X(16).
             07 (SF)-COD-FISC-ASSTO3-NULL REDEFINES
                (SF)-COD-FISC-ASSTO3   PIC X(16).
             07 (SF)-CAUS-SCON PIC X(100).
             07 (SF)-EMIT-TIT-OPZ PIC X(100).
             07 (SF)-QTZ-SP-Z-COUP-EMIS PIC S9(7)V9(5) COMP-3.
             07 (SF)-QTZ-SP-Z-COUP-EMIS-NULL REDEFINES
                (SF)-QTZ-SP-Z-COUP-EMIS   PIC X(7).
             07 (SF)-QTZ-SP-Z-OPZ-EMIS PIC S9(7)V9(5) COMP-3.
             07 (SF)-QTZ-SP-Z-OPZ-EMIS-NULL REDEFINES
                (SF)-QTZ-SP-Z-OPZ-EMIS   PIC X(7).
             07 (SF)-QTZ-SP-Z-COUP-DT-C PIC S9(7)V9(5) COMP-3.
             07 (SF)-QTZ-SP-Z-COUP-DT-C-NULL REDEFINES
                (SF)-QTZ-SP-Z-COUP-DT-C   PIC X(7).
             07 (SF)-QTZ-SP-Z-OPZ-DT-CA PIC S9(7)V9(5) COMP-3.
             07 (SF)-QTZ-SP-Z-OPZ-DT-CA-NULL REDEFINES
                (SF)-QTZ-SP-Z-OPZ-DT-CA   PIC X(7).
             07 (SF)-QTZ-TOT-EMIS PIC S9(7)V9(5) COMP-3.
             07 (SF)-QTZ-TOT-EMIS-NULL REDEFINES
                (SF)-QTZ-TOT-EMIS   PIC X(7).
             07 (SF)-QTZ-TOT-DT-CALC PIC S9(7)V9(5) COMP-3.
             07 (SF)-QTZ-TOT-DT-CALC-NULL REDEFINES
                (SF)-QTZ-TOT-DT-CALC   PIC X(7).
             07 (SF)-QTZ-TOT-DT-ULT-BIL PIC S9(7)V9(5) COMP-3.
             07 (SF)-QTZ-TOT-DT-ULT-BIL-NULL REDEFINES
                (SF)-QTZ-TOT-DT-ULT-BIL   PIC X(7).
             07 (SF)-DT-QTZ-EMIS   PIC S9(8) COMP-3.
             07 (SF)-DT-QTZ-EMIS-NULL REDEFINES
                (SF)-DT-QTZ-EMIS   PIC X(5).
             07 (SF)-PC-CAR-GEST PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-CAR-GEST-NULL REDEFINES
                (SF)-PC-CAR-GEST   PIC X(4).
             07 (SF)-PC-CAR-ACQ PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-CAR-ACQ-NULL REDEFINES
                (SF)-PC-CAR-ACQ   PIC X(4).
             07 (SF)-IMP-CAR-CASO-MOR PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CAR-CASO-MOR-NULL REDEFINES
                (SF)-IMP-CAR-CASO-MOR   PIC X(8).
             07 (SF)-PC-CAR-MOR PIC S9(3)V9(3) COMP-3.
             07 (SF)-PC-CAR-MOR-NULL REDEFINES
                (SF)-PC-CAR-MOR   PIC X(4).
             07 (SF)-TP-VERS PIC X(1).
             07 (SF)-TP-VERS-NULL REDEFINES
                (SF)-TP-VERS   PIC X(1).
             07 (SF)-FL-SWITCH PIC X(1).
             07 (SF)-FL-SWITCH-NULL REDEFINES
                (SF)-FL-SWITCH   PIC X(1).
             07 (SF)-FL-IAS PIC X(1).
             07 (SF)-FL-IAS-NULL REDEFINES
                (SF)-FL-IAS   PIC X(1).
             07 (SF)-DIR PIC S9(12)V9(3) COMP-3.
             07 (SF)-DIR-NULL REDEFINES
                (SF)-DIR   PIC X(8).
             07 (SF)-TP-COP-CASO-MOR PIC X(2).
             07 (SF)-TP-COP-CASO-MOR-NULL REDEFINES
                (SF)-TP-COP-CASO-MOR   PIC X(2).
             07 (SF)-MET-RISC-SPCL PIC S9(1)     COMP-3.
             07 (SF)-MET-RISC-SPCL-NULL REDEFINES
                (SF)-MET-RISC-SPCL   PIC X(1).
             07 (SF)-TP-STAT-INVST PIC X(2).
             07 (SF)-TP-STAT-INVST-NULL REDEFINES
                (SF)-TP-STAT-INVST   PIC X(2).
             07 (SF)-COD-PRDT PIC S9(5)     COMP-3.
             07 (SF)-COD-PRDT-NULL REDEFINES
                (SF)-COD-PRDT   PIC X(3).
             07 (SF)-STAT-ASSTO-1 PIC X(1).
             07 (SF)-STAT-ASSTO-1-NULL REDEFINES
                (SF)-STAT-ASSTO-1   PIC X(1).
             07 (SF)-STAT-ASSTO-2 PIC X(1).
             07 (SF)-STAT-ASSTO-2-NULL REDEFINES
                (SF)-STAT-ASSTO-2   PIC X(1).
             07 (SF)-STAT-ASSTO-3 PIC X(1).
             07 (SF)-STAT-ASSTO-3-NULL REDEFINES
                (SF)-STAT-ASSTO-3   PIC X(1).
             07 (SF)-CPT-ASSTO-INI-MOR PIC S9(12)V9(3) COMP-3.
             07 (SF)-CPT-ASSTO-INI-MOR-NULL REDEFINES
                (SF)-CPT-ASSTO-INI-MOR   PIC X(8).
             07 (SF)-TS-STAB-PRE PIC S9(5)V9(9) COMP-3.
             07 (SF)-TS-STAB-PRE-NULL REDEFINES
                (SF)-TS-STAB-PRE   PIC X(8).
             07 (SF)-DIR-EMIS PIC S9(12)V9(3) COMP-3.
             07 (SF)-DIR-EMIS-NULL REDEFINES
                (SF)-DIR-EMIS   PIC X(8).
             07 (SF)-DT-INC-ULT-PRE   PIC S9(8) COMP-3.
             07 (SF)-DT-INC-ULT-PRE-NULL REDEFINES
                (SF)-DT-INC-ULT-PRE   PIC X(5).
             07 (SF)-STAT-TBGC-ASSTO-1 PIC X(1).
             07 (SF)-STAT-TBGC-ASSTO-1-NULL REDEFINES
                (SF)-STAT-TBGC-ASSTO-1   PIC X(1).
             07 (SF)-STAT-TBGC-ASSTO-2 PIC X(1).
             07 (SF)-STAT-TBGC-ASSTO-2-NULL REDEFINES
                (SF)-STAT-TBGC-ASSTO-2   PIC X(1).
             07 (SF)-STAT-TBGC-ASSTO-3 PIC X(1).
             07 (SF)-STAT-TBGC-ASSTO-3-NULL REDEFINES
                (SF)-STAT-TBGC-ASSTO-3   PIC X(1).
             07 (SF)-FRAZ-DECR-CPT PIC S9(5)     COMP-3.
             07 (SF)-FRAZ-DECR-CPT-NULL REDEFINES
                (SF)-FRAZ-DECR-CPT   PIC X(3).
             07 (SF)-PRE-PP-ULT PIC S9(12)V9(3) COMP-3.
             07 (SF)-PRE-PP-ULT-NULL REDEFINES
                (SF)-PRE-PP-ULT   PIC X(8).
             07 (SF)-ACQ-EXP PIC S9(12)V9(3) COMP-3.
             07 (SF)-ACQ-EXP-NULL REDEFINES
                (SF)-ACQ-EXP   PIC X(8).
             07 (SF)-REMUN-ASS PIC S9(12)V9(3) COMP-3.
             07 (SF)-REMUN-ASS-NULL REDEFINES
                (SF)-REMUN-ASS   PIC X(8).
             07 (SF)-COMMIS-INTER PIC S9(12)V9(3) COMP-3.
             07 (SF)-COMMIS-INTER-NULL REDEFINES
                (SF)-COMMIS-INTER   PIC X(8).
             07 (SF)-NUM-FINANZ PIC X(40).
             07 (SF)-NUM-FINANZ-NULL REDEFINES
                (SF)-NUM-FINANZ   PIC X(40).
             07 (SF)-TP-ACC-COMM PIC X(2).
             07 (SF)-TP-ACC-COMM-NULL REDEFINES
                (SF)-TP-ACC-COMM   PIC X(2).
             07 (SF)-IB-ACC-COMM PIC X(40).
             07 (SF)-IB-ACC-COMM-NULL REDEFINES
                (SF)-IB-ACC-COMM   PIC X(40).
             07 (SF)-RAMO-BILA PIC X(12).
             07 (SF)-CARZ PIC S9(5)     COMP-3.
             07 (SF)-CARZ-NULL REDEFINES
                (SF)-CARZ   PIC X(3).

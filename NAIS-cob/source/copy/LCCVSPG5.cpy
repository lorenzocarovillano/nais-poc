
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVSPG5
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-SPG.
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-SPG) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-SPG)
              TO SPG-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-SPG)
              TO SPG-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-SPG) NOT NUMERIC
              MOVE 0 TO SPG-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-SPG)
              TO SPG-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-SPG) NOT NUMERIC
              MOVE 0 TO SPG-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-SPG)
              TO SPG-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-SPG)
              TO SPG-COD-COMP-ANIA
           IF (SF)-COD-SOPR-NULL(IX-TAB-SPG) = HIGH-VALUES
              MOVE (SF)-COD-SOPR-NULL(IX-TAB-SPG)
              TO SPG-COD-SOPR-NULL
           ELSE
              MOVE (SF)-COD-SOPR(IX-TAB-SPG)
              TO SPG-COD-SOPR
           END-IF
           IF (SF)-TP-D-NULL(IX-TAB-SPG) = HIGH-VALUES
              MOVE (SF)-TP-D-NULL(IX-TAB-SPG)
              TO SPG-TP-D-NULL
           ELSE
              MOVE (SF)-TP-D(IX-TAB-SPG)
              TO SPG-TP-D
           END-IF
           IF (SF)-VAL-PC-NULL(IX-TAB-SPG) = HIGH-VALUES
              MOVE (SF)-VAL-PC-NULL(IX-TAB-SPG)
              TO SPG-VAL-PC-NULL
           ELSE
              MOVE (SF)-VAL-PC(IX-TAB-SPG)
              TO SPG-VAL-PC
           END-IF
           IF (SF)-VAL-IMP-NULL(IX-TAB-SPG) = HIGH-VALUES
              MOVE (SF)-VAL-IMP-NULL(IX-TAB-SPG)
              TO SPG-VAL-IMP-NULL
           ELSE
              MOVE (SF)-VAL-IMP(IX-TAB-SPG)
              TO SPG-VAL-IMP
           END-IF
           IF (SF)-PC-SOPRAM-NULL(IX-TAB-SPG) = HIGH-VALUES
              MOVE (SF)-PC-SOPRAM-NULL(IX-TAB-SPG)
              TO SPG-PC-SOPRAM-NULL
           ELSE
              MOVE (SF)-PC-SOPRAM(IX-TAB-SPG)
              TO SPG-PC-SOPRAM
           END-IF
           IF (SF)-FL-ESCL-SOPR-NULL(IX-TAB-SPG) = HIGH-VALUES
              MOVE (SF)-FL-ESCL-SOPR-NULL(IX-TAB-SPG)
              TO SPG-FL-ESCL-SOPR-NULL
           ELSE
              MOVE (SF)-FL-ESCL-SOPR(IX-TAB-SPG)
              TO SPG-FL-ESCL-SOPR
           END-IF
           MOVE (SF)-DESC-ESCL(IX-TAB-SPG)
              TO SPG-DESC-ESCL
           IF (SF)-DS-RIGA(IX-TAB-SPG) NOT NUMERIC
              MOVE 0 TO SPG-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-SPG)
              TO SPG-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-SPG)
              TO SPG-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-SPG) NOT NUMERIC
              MOVE 0 TO SPG-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-SPG)
              TO SPG-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-SPG) NOT NUMERIC
              MOVE 0 TO SPG-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-SPG)
              TO SPG-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-SPG) NOT NUMERIC
              MOVE 0 TO SPG-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-SPG)
              TO SPG-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-SPG)
              TO SPG-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-SPG)
              TO SPG-DS-STATO-ELAB.
       VAL-DCLGEN-SPG-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVE123
      *   ULTIMO AGG. 14 MAR 2012
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-E12.
           MOVE E12-ID-EST-TRCH-DI-GAR
             TO (SF)-ID-PTF(IX-TAB-E12)
           MOVE E12-ID-EST-TRCH-DI-GAR
             TO (SF)-ID-EST-TRCH-DI-GAR(IX-TAB-E12)
           MOVE E12-ID-TRCH-DI-GAR
             TO (SF)-ID-TRCH-DI-GAR(IX-TAB-E12)
           MOVE E12-ID-GAR
             TO (SF)-ID-GAR(IX-TAB-E12)
           MOVE E12-ID-ADES
             TO (SF)-ID-ADES(IX-TAB-E12)
           MOVE E12-ID-POLI
             TO (SF)-ID-POLI(IX-TAB-E12)
           MOVE E12-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-E12)
           IF E12-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE E12-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-E12)
           ELSE
              MOVE E12-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-E12)
           END-IF
           MOVE E12-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-E12)
           MOVE E12-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-E12)
           MOVE E12-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-E12)
           IF E12-DT-EMIS-NULL = HIGH-VALUES
              MOVE E12-DT-EMIS-NULL
                TO (SF)-DT-EMIS-NULL(IX-TAB-E12)
           ELSE
              MOVE E12-DT-EMIS
                TO (SF)-DT-EMIS(IX-TAB-E12)
           END-IF
           IF E12-CUM-PRE-ATT-NULL = HIGH-VALUES
              MOVE E12-CUM-PRE-ATT-NULL
                TO (SF)-CUM-PRE-ATT-NULL(IX-TAB-E12)
           ELSE
              MOVE E12-CUM-PRE-ATT
                TO (SF)-CUM-PRE-ATT(IX-TAB-E12)
           END-IF
           IF E12-ACCPRE-PAG-NULL = HIGH-VALUES
              MOVE E12-ACCPRE-PAG-NULL
                TO (SF)-ACCPRE-PAG-NULL(IX-TAB-E12)
           ELSE
              MOVE E12-ACCPRE-PAG
                TO (SF)-ACCPRE-PAG(IX-TAB-E12)
           END-IF
           IF E12-CUM-PRSTZ-NULL = HIGH-VALUES
              MOVE E12-CUM-PRSTZ-NULL
                TO (SF)-CUM-PRSTZ-NULL(IX-TAB-E12)
           ELSE
              MOVE E12-CUM-PRSTZ
                TO (SF)-CUM-PRSTZ(IX-TAB-E12)
           END-IF
           MOVE E12-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-E12)
           MOVE E12-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-E12)
           MOVE E12-DS-VER
             TO (SF)-DS-VER(IX-TAB-E12)
           MOVE E12-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-E12)
           MOVE E12-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-E12)
           MOVE E12-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-E12)
           MOVE E12-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-E12)
           MOVE E12-TP-TRCH
             TO (SF)-TP-TRCH(IX-TAB-E12)
           IF E12-CAUS-SCON-NULL = HIGH-VALUES
              MOVE E12-CAUS-SCON-NULL
                TO (SF)-CAUS-SCON-NULL(IX-TAB-E12)
           ELSE
              MOVE E12-CAUS-SCON
                TO (SF)-CAUS-SCON(IX-TAB-E12)
           END-IF.
       VALORIZZA-OUTPUT-E12-EX.
           EXIT.

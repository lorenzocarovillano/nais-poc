
      ******************************************************************
      * TESTATA JCL
      ******************************************************************
       01 LOGO-JCL-REPORT.
          05 FILLER            PIC X(32)  VALUE
        '*                               '.
          05 FILLER            PIC X(100) VALUE
        'R E P O R T   D I   E L A B O R A Z I O N E   B A T C H'.


       01 DATA-ELABORAZIONE-REPORT.
          05 FILLER                   PIC X(026)  VALUE
        '* DATA ELABORAZIONE    :  '.
          05 REP-GIORNO               PIC X(002) VALUE SPACES.
          05 FILLER                   PIC X(003) VALUE ' / '.
          05 REP-MESE                 PIC X(002) VALUE SPACES.
          05 FILLER                   PIC X(003) VALUE ' / '.
          05 REP-ANNO                 PIC X(004) VALUE SPACES.


       01 ORA-ELABORAZIONE-REPORT.
          05 FILLER                   PIC X(26)  VALUE
        '* ORA  ELABORAZIONE    :  '.
          05 REP-ORA                  PIC X(002) VALUE SPACES.
          05 FILLER                   PIC X(003) VALUE ' : '.
          05 REP-MINUTI               PIC X(002) VALUE SPACES.
          05 FILLER                   PIC X(003) VALUE ' : '.
          05 REP-SECONDI              PIC X(004) VALUE SPACES.


       01 COD-COMPAGNIA-REPORT.
          05 FILLER                   PIC X(26)  VALUE
        '* CODICE COMPAGNIA     :  '.
          05 REP-COD-COMPAGNIA        PIC ZZZZ9 VALUE ZEROES.


       01 DESC-BATCH-REPORT.
          05 FILLER                   PIC X(26)  VALUE
        '* DESCRIZIONE BATCH    :  '.
          05 REP-DESC-BATCH           PIC X(100) VALUE SPACES.


       01 USER-NAME-REPORT.
          05 FILLER                   PIC X(26)  VALUE
        '* USER NAME            :  '.
          05 REP-USER-NAME            PIC X(008) VALUE SPACES.


       01 GRUPPO-AUT-REPORT.
          05 FILLER                   PIC X(26)  VALUE
        '* GRUPPO AUTORIZZANTE  :  '.
          05 REP-GRUPPO-AUT           PIC 9(010) VALUE ZEROES.


       01 MACROFUNZIONALITA-REPORT.
          05 FILLER                   PIC X(26)  VALUE
        '* MACROFUNZIONALITA    :  '.
          05 REP-MACROFUNZIONALITA    PIC X(002) VALUE SPACES.


       01 TIPO-MOVIMENTO-REPORT.
          05 FILLER                   PIC X(26)  VALUE
        '* TIPO MOVIMENTO       :  '.
          05 REP-TIPO-MOVIMENTO       PIC 9(05)  VALUE ZEROES.


       01 DATA-EFFETTO-REPORT.
          05 FILLER                   PIC X(26)  VALUE
        '* DATA EFFETTO         :  '.
          05 REP-DATA-EFFETTO         PIC X(10)  VALUE SPACES.


       01 TIMESTAMP-COMPETENZA-REPORT.
          05 FILLER                   PIC X(26)  VALUE
        '* TIMESTAMP COMPETENZA :  '.
          05 REP-TIMESTAMP-COMPETENZA PIC X(26)  VALUE SPACES.


       01 SIMULAZIONE-REPORT.
          05 FILLER                   PIC X(26)  VALUE
        '* SIMULAZIONE          :  '.
          05 REP-SIMULAZIONE          PIC X(01)  VALUE SPACES.


       01 CANALE-VENDITA-REPORT.
          05 FILLER                   PIC X(26)  VALUE
        '* CANALE VENDITA       :  '.
          05 REP-CANALE-VENDITA       PIC ZZZZ9 VALUE ZEROES.


       01 PUNTO-VENDITA-REPORT.
          05 FILLER                   PIC X(26)  VALUE
        '* PUNTO VENDITA        :  '.
          05 REP-PUNTO-VENDITA        PIC ZZZZ9 VALUE ZEROES.


      ******************************************************************
      * TESTATA BATCH
      ******************************************************************

       01 LOGO-BATCH-REPORT      PIC X(100) VALUE
        '* TESTATA BATCH'.


       01 ID-BATCH-REPORT.
          05 FILLER                   PIC X(30)  VALUE
        'IDENTIFICATIVO BATCH       :  '.
          05 REP-ID-BATCH             PIC ZZZZZZZZ9 VALUE ZEROES.


       01 PROTOCOL-REPORT.
          05 FILLER                   PIC X(30)  VALUE
        'PROTOCOLLO                 :  '.
          05 REP-PROTOCOL             PIC X(50)  VALUE SPACES.


       01 PROGR-PROTOCOL-REPORT.
          05 FILLER                   PIC X(38)  VALUE
        'ELEMENTO PARALLELO         :  '.
          05 REP-PROGR-PROTOCOL       PIC ZZZZ9  VALUE ZEROES.


      ******************************************************************
      * DETTAGLIO LOG ERRORE
      ******************************************************************

       01 DETT-LOG-ERRORE-REPORT      PIC X(100) VALUE
        '* DETTAGLIO LOG ERRORE'.

       01 CAMPI-LOG-ERRORE-REPORT-RIGA-1.
          05 FILLER                   PIC X(007) VALUE
             'LIVELLO'.
          05 FILLER                   PIC X(013) VALUE SPACES.
          05 FILLER                   PIC X(007) VALUE
             'OGGETTO'.
          05 FILLER                   PIC X(025) VALUE SPACES.
          05 FILLER                   PIC X(004) VALUE
             'TIPO'.
          05 FILLER                   PIC X(021) VALUE SPACES.
          05 FILLER                   PIC X(002) VALUE
             'IB'.
          05 FILLER                   PIC X(040) VALUE SPACES.
          05 FILLER                   PIC X(002) VALUE
             'IB'.
          05 FILLER                   PIC X(023) VALUE SPACES.
          05 FILLER                   PIC X(002) VALUE
             'ID'.
          05 FILLER                   PIC X(008) VALUE SPACES.
          05 FILLER                   PIC X(002) VALUE
             'ID'.
          05 FILLER                   PIC X(007) VALUE SPACES.
          05 FILLER                   PIC X(004) VALUE
             'DATA'.
          05 FILLER                   PIC X(049) VALUE SPACES.
          05 FILLER                   PIC X(011) VALUE
             'DESCRIZIONE'.
          05 FILLER                   PIC X(048) VALUE SPACES.
          05 FILLER                   PIC X(003) VALUE
             'COD'.
          05 FILLER                   PIC X(004) VALUE SPACES.
          05 FILLER                   PIC X(008) VALUE
             'SERVIZIO'.
          05 FILLER                   PIC X(013) VALUE SPACES.
          05 FILLER                   PIC X(005) VALUE
             'LABEL'.
          05 FILLER                   PIC X(013) VALUE SPACES.
          05 FILLER                   PIC X(010) VALUE
             'OPERAZIONE'.
          05 FILLER                   PIC X(007) VALUE SPACES.
          05 FILLER                   PIC X(004) VALUE
             'NOME'.
          05 FILLER                   PIC X(010) VALUE SPACES.
          05 FILLER                   PIC X(006) VALUE
             'STATUS'.
          05 FILLER                   PIC X(012) VALUE SPACES.
          05 FILLER                   PIC X(009) VALUE
             'TIMESTAMP'.
          05 FILLER                   PIC X(061) VALUE SPACES.
          05 FILLER                   PIC X(003) VALUE
             'KEY'.

       01 CAMPI-LOG-ERRORE-REPORT-RIGA-2.
          05 FILLER                   PIC X(008) VALUE
             'GRAVITA'''.
          05 FILLER                   PIC X(012) VALUE SPACES.
          05 FILLER                   PIC X(008) VALUE
             'BUSINESS'.
          05 FILLER                   PIC X(023) VALUE SPACES.
          05 FILLER                   PIC X(007) VALUE
             'OGGETTO'.
          05 FILLER                   PIC X(017) VALUE SPACES.
          05 FILLER                   PIC X(007) VALUE
             'POLIZZA'.
          05 FILLER                   PIC X(034) VALUE SPACES.
          05 FILLER                   PIC X(008) VALUE
             'ADESIONE'.
          05 FILLER                   PIC X(018) VALUE SPACES.
          05 FILLER                   PIC X(007) VALUE
             'POLIZZA'.
          05 FILLER                   PIC X(002) VALUE SPACES.
          05 FILLER                   PIC X(008) VALUE
             'ADESIONE'.
          05 FILLER                   PIC X(003) VALUE SPACES.
          05 FILLER                   PIC X(007) VALUE
             'EFFETTO'.
          05 FILLER                   PIC X(049) VALUE SPACES.
          05 FILLER                   PIC X(006) VALUE
             'ERRORE'.
          05 FILLER                   PIC X(051) VALUE SPACES.
          05 FILLER                   PIC X(004) VALUE
             'MAIN'.
          05 FILLER                   PIC X(003) VALUE SPACES.
          05 FILLER                   PIC X(008) VALUE
             'BACK-END'.
          05 FILLER                   PIC X(013) VALUE SPACES.
          05 FILLER                   PIC X(006) VALUE
             'ERRORE'.
          05 FILLER                   PIC X(013) VALUE SPACES.
          05 FILLER                   PIC X(007) VALUE
             'TABELLA'.
          05 FILLER                   PIC X(008) VALUE SPACES.
          05 FILLER                   PIC X(007) VALUE
             'TABELLA'.
          05 FILLER                   PIC X(008) VALUE SPACES.
          05 FILLER                   PIC X(007) VALUE
             'TABELLA'.
          05 FILLER                   PIC X(012) VALUE SPACES.
          05 FILLER                   PIC X(006) VALUE
             'ERRORE'.
          05 FILLER                   PIC X(061) VALUE SPACES.
          05 FILLER                   PIC X(007) VALUE
             'TABELLA'.

       01 CAMPI-LOG-ERRORE-REPORT-RIGA-3.
          05 FILLER                   PIC X(040) VALUE SPACES.
          05 FILLER                   PIC X(008) VALUE
             'BUSINESS'.
          05 FILLER                   PIC X(104) VALUE SPACES.
          05 FILLER                   PIC X(005) VALUE
             'BATCH'.

       01 SL-CAMPI-LOG-ERRORE-REPORT.
          05 FILLER                   PIC X(003)  VALUE SPACES.
          05 REP-LIV-GRAVITA-SL       PIC X(001)  VALUE ALL '-'.
          05 FILLER                   PIC X(005)  VALUE SPACES.
          05 REP-OGG-BUSINESS-SL      PIC X(040)  VALUE ALL '-'.
          05 FILLER                   PIC X(004)  VALUE SPACES.
          05 REP-TP-OGG-BUSINESS-SL   PIC X(002)  VALUE ALL '-'.
          05 FILLER                   PIC X(004)  VALUE SPACES.
          05 REP-IB-OGG-POLI-SL       PIC X(040)  VALUE ALL '-'.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-IB-OGG-ADEI-SL       PIC X(040)  VALUE ALL '-'.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-ID-POLI-SL           PIC X(009)  VALUE ALL '-'.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-ID-ADES-SL           PIC X(009)  VALUE ALL '-'.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-DT-EFF-BUSINESS-SL   PIC X(010)  VALUE ALL '-'.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-DESC-ERRORE-SL       PIC X(100)  VALUE ALL '-'.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-COD-MAIN-BATCH-SL    PIC X(008)  VALUE ALL '-'.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-SERVIZIO-BE-SL       PIC X(008)  VALUE ALL '-'.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-LABEL-ERR-SL         PIC X(030)  VALUE ALL '-'.
          05 FILLER                   PIC X(003)  VALUE SPACES.
          05 REP-OPER-TABELLA-SL      PIC X(002)  VALUE ALL '-'.
          05 FILLER                   PIC X(006)  VALUE SPACES.
          05 REP-NOME-TABELLA-SL      PIC X(018)  VALUE ALL '-'.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-STATUS-TABELLA-SL    PIC X(010)  VALUE ALL '-'.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-TMST-ERRORE-SL       PIC X(026)  VALUE ALL '-'.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-KEY-TABELLA-SL       PIC X(100)  VALUE ALL '-'.

       01 VALORI-CAMPI-LOG-ERRORE-REPORT.
          05 FILLER                   PIC X(003)  VALUE SPACES.
          05 REP-LIV-GRAVITA          PIC 9(001)  VALUE ZEROES.
          05 FILLER                   PIC X(005)  VALUE SPACES.
          05 REP-OGG-BUSINESS         PIC X(040)  VALUE SPACES.
          05 FILLER                   PIC X(004)  VALUE SPACES.
          05 REP-TP-OGG-BUSINESS      PIC X(002)  VALUE SPACES.
          05 FILLER                   PIC X(004)  VALUE SPACES.
          05 REP-IB-OGG-POLI          PIC X(040)  VALUE SPACES.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-IB-OGG-ADES          PIC X(040)  VALUE SPACES.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-ID-POLI              PIC X(009)  VALUE SPACES.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-ID-ADES              PIC X(009)  VALUE SPACES.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-DT-EFF-BUSINESS      PIC X(010)  VALUE SPACES.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-DESC-ERRORE          PIC X(100)  VALUE SPACES.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-COD-MAIN-BATCH       PIC X(008)  VALUE SPACES.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-SERVIZIO-BE          PIC X(008)  VALUE SPACES.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-LABEL-ERR            PIC X(030)  VALUE SPACES.
          05 FILLER                   PIC X(003)  VALUE SPACES.
          05 REP-OPER-TABELLA         PIC X(002)  VALUE SPACES.
          05 FILLER                   PIC X(006)  VALUE SPACES.
          05 REP-NOME-TABELLA         PIC X(018)  VALUE SPACES.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-STATUS-TABELLA       PIC X(010)  VALUE SPACES.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-TMST-ERRORE          PIC X(026)  VALUE SPACES.
          05 FILLER                   PIC X(001)  VALUE SPACES.
          05 REP-KEY-TABELLA          PIC X(100)  VALUE SPACES.

      ******************************************************************
      * RIEPILOGO ESITI BATCH
      ******************************************************************

       01 LOGO-ESITI-BATCH-REPORT     PIC X(100) VALUE
        '* RIEPILOGO ESITI BATCH'.

       01 SL-COUNT                    PIC X(063) VALUE ALL '-'.



       01 LOGO-STANDARD-COUNT.
          05 FILLER                   PIC X(22)  VALUE
        '---                   '.
          05 FILLER                   PIC X(41)  VALUE
        'CONTATORI STANDARD                    ---'.

       01 BUS-SERV-ESEG-REPORT.
          05 FILLER                   PIC X(50)  VALUE
        'BUSINESS SERVICES ESEGUITI'.
          05 FILLER                   PIC X(04)  VALUE ' :  '.
          05 REP-BUS-SERV-ESEGUITI    PIC ZZZZZZZZ9 VALUE ZEROES.

       01 BUS-SERV-OK-REPORT.
          05 FILLER                   PIC X(50)  VALUE
        'BUSINESS SERVICES ESITO OK'.
          05 FILLER                   PIC X(04)  VALUE ' :  '.
          05 REP-BUS-SERV-ESITO-OK    PIC ZZZZZZZZ9 VALUE ZEROES.

       01 BUS-SERV-OK-WARN-REPORT.
          05 FILLER                   PIC X(50)  VALUE
        '            DI CUI WARNING'.
          05 FILLER                   PIC X(04)  VALUE ' :  '.
          05 REP-BUS-SERV-WARNING     PIC ZZZZZZZZ9 VALUE ZEROES.

       01 BUS-SERV-KO-REPORT.
          05 FILLER                   PIC X(50)  VALUE
        'BUSINESS SERVICES ESITO KO'.
          05 FILLER                   PIC X(04)  VALUE ' :  '.
          05 REP-BUS-SERV-ESITO-KO    PIC ZZZZZZZZ9 VALUE ZEROES.



       01 LOGO-CUSTOM-COUNT.
          05 FILLER                   PIC X(19)  VALUE
        '---                '.
          05 FILLER                   PIC X(44)  VALUE
        'CONTATORI PERSONALIZZATI                 ---'.

       01 TAB-GUIDE-LETTE-REPORT.
          05 FILLER                   PIC X(50)  VALUE
        'OCCORRENZE GUIDA LETTE'.
          05 FILLER                   PIC X(04)  VALUE ' :  '.
          05 REP-TAB-GUIDE-LETTE      PIC ZZZZZZZZ9 VALUE ZEROES.

       01 CUSTOM-REPORT.
          05 FILLER                    PIC X(01)  VALUE ' '.
          05 REP-CUSTOM-COUNT-DESC     PIC X(49)  VALUE SPACES.
          05 FILLER                    PIC X(04)  VALUE ' :  '.
          05 REP-CUSTOM-COUNT          PIC ZZZZZZZZ9 VALUE ZEROES.

       01 STATO-FINALE-BATCH-REPORT.
          05 FILLER                   PIC X(50)  VALUE
        'STATO FINALE BATCH'.
          05 FILLER                   PIC X(04)  VALUE ' :  '.
          05 REP-STATO-FINALE         PIC X(50)  VALUE SPACES.

       01 STATO-FINALE-PARALLELO-REPORT.
          05 FILLER                   PIC X(50)  VALUE
        'STATO FINALE PARALLELO'.
          05 FILLER                   PIC X(04)  VALUE ' :  '.
          05 REP-STATO-FINALE-P       PIC X(50)  VALUE SPACES.

       01  RESTO                      PIC 9(5)   VALUE ZEROES.
       01  RESULT                     PIC 9(9)   VALUE ZEROES.

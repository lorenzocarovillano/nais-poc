      ******************************************************************
      *
      *  ROUTINE PER IL CALCOLO DELL'ETA'
      *
      *
      *  INPUT :
      *    LCCC1751-DATA-INFERIORE       DATA INFERIORE FORMATO AAAAMMGG
      *    LCCC1751-DATA-SUPERIORE       DATA SUPERIORE FORMATO AAAAMMGG
      *
      *  OUTPUT :
      *
      *    LCCC1751-ETA-AA-ASSICURATO NUMERO ANNI E NUMERO MESI CHE
      *    LCCC1751-ETA-MM-ASSICURATO INTERCORRE TRA LE DUE DATE
      *
      *    LCCC1751-RETURN-CODE           RETURN CODE
      *    LCCC1751-DESCRIZ-ERR           DESCRIZIONE ERRORE
      ******************************************************************
      *   CAMPI DI INPUT
      *
       01  LCCC1751.
           03  LCCC1751-INPUT.
               05  LCCC1751-DATA-INFERIORE        PIC 9(08).
               05  LCCC1751-DATA-SUPERIORE        PIC 9(08).
      *
      *  CAMPI DI OUTPUT
      *
           03  LCCC1751-OUTPUT.
               05 LCCC1751-ETA-MM-ASSICURATO      PIC 9(002) VALUE ZERO.
               05 LCCC1751-ETA-AA-ASSICURATO      PIC 9(005) VALUE ZERO.
      *
               05  LCCC1751-RETURN-CODE           PIC  X(002).
               05  LCCC1751-DESCRIZ-ERR           PIC  X(200).
      *

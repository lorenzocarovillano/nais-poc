
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVSTW5
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------

       VAL-DCLGEN-STW.
           MOVE (SF)-ID-STAT-OGG-WF(IX-TAB-STW)
              TO STW-ID-STAT-OGG-WF
           MOVE (SF)-ID-OGG(IX-TAB-STW)
              TO STW-ID-OGG
           MOVE (SF)-TP-OGG(IX-TAB-STW)
              TO STW-TP-OGG
           MOVE (SF)-ID-MOVI-CRZ(IX-TAB-STW)
              TO STW-ID-MOVI-CRZ
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-STW) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-STW)
              TO STW-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-STW)
              TO STW-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-STW) NOT NUMERIC
              MOVE 0 TO STW-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-STW)
              TO STW-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-STW) NOT NUMERIC
              MOVE 0 TO STW-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-STW)
              TO STW-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-STW)
              TO STW-COD-COMP-ANIA
           MOVE (SF)-COD-PRCS(IX-TAB-STW)
              TO STW-COD-PRCS
           MOVE (SF)-COD-ATTVT(IX-TAB-STW)
              TO STW-COD-ATTVT
           MOVE (SF)-STAT-OGG-WF(IX-TAB-STW)
              TO STW-STAT-OGG-WF
           IF (SF)-FL-STAT-END-NULL(IX-TAB-STW) = HIGH-VALUES
              MOVE (SF)-FL-STAT-END-NULL(IX-TAB-STW)
              TO STW-FL-STAT-END-NULL
           ELSE
              MOVE (SF)-FL-STAT-END(IX-TAB-STW)
              TO STW-FL-STAT-END
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-STW) NOT NUMERIC
              MOVE 0 TO STW-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-STW)
              TO STW-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-STW)
              TO STW-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-STW) NOT NUMERIC
              MOVE 0 TO STW-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-STW)
              TO STW-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-STW) NOT NUMERIC
              MOVE 0 TO STW-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-STW)
              TO STW-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-STW) NOT NUMERIC
              MOVE 0 TO STW-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-STW)
              TO STW-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-STW)
              TO STW-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-STW)
              TO STW-DS-STATO-ELAB.
       VAL-DCLGEN-STW-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVSPG3
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-SPG.
           MOVE SPG-ID-SOPR-DI-GAR
             TO (SF)-ID-PTF(IX-TAB-SPG)
           MOVE SPG-ID-SOPR-DI-GAR
             TO (SF)-ID-SOPR-DI-GAR(IX-TAB-SPG)
           IF SPG-ID-GAR-NULL = HIGH-VALUES
              MOVE SPG-ID-GAR-NULL
                TO (SF)-ID-GAR-NULL(IX-TAB-SPG)
           ELSE
              MOVE SPG-ID-GAR
                TO (SF)-ID-GAR(IX-TAB-SPG)
           END-IF
           MOVE SPG-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-SPG)
           IF SPG-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE SPG-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-SPG)
           ELSE
              MOVE SPG-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-SPG)
           END-IF
           MOVE SPG-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-SPG)
           MOVE SPG-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-SPG)
           MOVE SPG-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-SPG)
           IF SPG-COD-SOPR-NULL = HIGH-VALUES
              MOVE SPG-COD-SOPR-NULL
                TO (SF)-COD-SOPR-NULL(IX-TAB-SPG)
           ELSE
              MOVE SPG-COD-SOPR
                TO (SF)-COD-SOPR(IX-TAB-SPG)
           END-IF
           IF SPG-TP-D-NULL = HIGH-VALUES
              MOVE SPG-TP-D-NULL
                TO (SF)-TP-D-NULL(IX-TAB-SPG)
           ELSE
              MOVE SPG-TP-D
                TO (SF)-TP-D(IX-TAB-SPG)
           END-IF
           IF SPG-VAL-PC-NULL = HIGH-VALUES
              MOVE SPG-VAL-PC-NULL
                TO (SF)-VAL-PC-NULL(IX-TAB-SPG)
           ELSE
              MOVE SPG-VAL-PC
                TO (SF)-VAL-PC(IX-TAB-SPG)
           END-IF
           IF SPG-VAL-IMP-NULL = HIGH-VALUES
              MOVE SPG-VAL-IMP-NULL
                TO (SF)-VAL-IMP-NULL(IX-TAB-SPG)
           ELSE
              MOVE SPG-VAL-IMP
                TO (SF)-VAL-IMP(IX-TAB-SPG)
           END-IF
           IF SPG-PC-SOPRAM-NULL = HIGH-VALUES
              MOVE SPG-PC-SOPRAM-NULL
                TO (SF)-PC-SOPRAM-NULL(IX-TAB-SPG)
           ELSE
              MOVE SPG-PC-SOPRAM
                TO (SF)-PC-SOPRAM(IX-TAB-SPG)
           END-IF
           IF SPG-FL-ESCL-SOPR-NULL = HIGH-VALUES
              MOVE SPG-FL-ESCL-SOPR-NULL
                TO (SF)-FL-ESCL-SOPR-NULL(IX-TAB-SPG)
           ELSE
              MOVE SPG-FL-ESCL-SOPR
                TO (SF)-FL-ESCL-SOPR(IX-TAB-SPG)
           END-IF
           MOVE SPG-DESC-ESCL
             TO (SF)-DESC-ESCL(IX-TAB-SPG)
           MOVE SPG-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-SPG)
           MOVE SPG-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-SPG)
           MOVE SPG-DS-VER
             TO (SF)-DS-VER(IX-TAB-SPG)
           MOVE SPG-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-SPG)
           MOVE SPG-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-SPG)
           MOVE SPG-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-SPG)
           MOVE SPG-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-SPG).
       VALORIZZA-OUTPUT-SPG-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVD035
      *   ULTIMO AGG. 16 MAR 2009
      *------------------------------------------------------------

       VAL-DCLGEN-D03.
           MOVE (SF)-COD-COMPAGNIA-ANIA
              TO D03-COD-COMPAGNIA-ANIA
           MOVE (SF)-FORMA-ASSICURATIVA
              TO D03-FORMA-ASSICURATIVA
           MOVE (SF)-COD-OGGETTO
              TO D03-COD-OGGETTO
           MOVE (SF)-KEY-BUSINESS
              TO D03-KEY-BUSINESS
           MOVE (SF)-ULT-PROGR
              TO D03-ULT-PROGR
           IF (SF)-PROGR-INIZIALE-NULL = HIGH-VALUES
              MOVE (SF)-PROGR-INIZIALE-NULL
              TO D03-PROGR-INIZIALE-NULL
           ELSE
              MOVE (SF)-PROGR-INIZIALE
              TO D03-PROGR-INIZIALE
           END-IF
           IF (SF)-PROGR-FINALE-NULL = HIGH-VALUES
              MOVE (SF)-PROGR-FINALE-NULL
              TO D03-PROGR-FINALE-NULL
           ELSE
              MOVE (SF)-PROGR-FINALE
              TO D03-PROGR-FINALE
           END-IF.
       VAL-DCLGEN-D03-EX.
           EXIT.

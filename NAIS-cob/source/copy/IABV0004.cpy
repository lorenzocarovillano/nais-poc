      ***************************************************************
      * PGM CHIAMATI
      ***************************************************************
       77 PGM-IABS0020                PIC X(08) VALUE 'IABS0020'.
       77 PGM-IABS0030                PIC X(08) VALUE 'IABS0030'.
       77 PGM-IABS0040                PIC X(08) VALUE 'IABS0040'.
       77 PGM-IABS0050                PIC X(08) VALUE 'IABS0050'.
       77 PGM-IABS0060                PIC X(08) VALUE 'IABS0060'.
       77 PGM-IABS0070                PIC X(08) VALUE 'IABS0070'.
       77 PGM-IABS0080                PIC X(08) VALUE 'IABS0080'.
       77 PGM-IABS0090                PIC X(08) VALUE 'IABS0090'.
       77 PGM-IABS0110                PIC X(08) VALUE 'IABS0110'.
       77 PGM-IABS0120                PIC X(08) VALUE 'IABS0120'.
       77 PGM-IABS0130                PIC X(08) VALUE 'IABS0130'.
       77 PGM-IABS0140                PIC X(08) VALUE 'IABS0140'.
       77 PGM-IABS0900                PIC X(08) VALUE 'IABS0900'.


       77 PGM-IDSS0150                PIC X(08) VALUE 'IDSS0150'.
       77 PGM-IDSS0160                PIC X(08) VALUE 'IDSS0160'.
       77 PGM-IDSS0300                PIC X(08) VALUE 'IDSS0300'.

       77 PGM-IDBSRIC0                PIC X(08) VALUE 'IDBSRIC0'.
       77 PGM-IDBSDER0                PIC X(08) VALUE 'IDBSDER0'.

       77 PGM-LCCS0090                PIC X(08) VALUE 'LCCS0090'.
       77 PGM-IDBSBPA0                PIC X(08) VALUE 'IDBSBPA0'.

       77 PGM-IDES0020                PIC X(08) VALUE 'IDES0020'.

      ***************************************************************
      * TRASCODIFICA STATI BATCH
      ***************************************************************
       77 BATCH-DA-ESEGUIRE                PIC X(01) VALUE '1'.
       77 BATCH-ESEGUITO                   PIC X(01) VALUE '2'.
       77 BATCH-DA-RIESEGUIRE              PIC X(01) VALUE '3'.
       77 BATCH-IN-ESECUZIONE              PIC X(01) VALUE '4'.
       77 BATCH-SIMULATO-OK                PIC X(01) VALUE '5'.
       77 BATCH-SIMULATO-KO                PIC X(01) VALUE '6'.
       77 BATCH-IN-ATTESA-ESECUZIONE       PIC X(01) VALUE '7'.

       77 BATCH-ANNULLATO                  PIC X(01) VALUE 'A'.

      ***************************************************************
      * TRASCODIFICA STATI JOB
      ***************************************************************
       77 JOB-DA-ESEGUIRE                  PIC X(01) VALUE '1'.
       77 JOB-ESEGUITO-OK                  PIC X(01) VALUE '2'.
       77 JOB-DA-RIESEGUIRE                PIC X(01) VALUE '3'.
       77 JOB-IN-ESECUZIONE                PIC X(01) VALUE '4'.
       77 JOB-ESEGUITO-KO                  PIC X(01) VALUE '5'.
       77 JOB-STARTING                     PIC X(01) VALUE '6'.

       77 JOB-DA-ANNULLARE                 PIC X(01) VALUE 'D'.

      *********************************************************
      * TRASCODIFICA STATI BATCH PER PRENOTAZIONE
      *********************************************************
       77 ANNULLATA                        PIC X(01) VALUE 'A'.
       77 CERTIFICATA                      PIC X(01) VALUE 'C'.
       77 PRENOTATA                        PIC X(01) VALUE 'P'.
       77 ESEGUITA                         PIC X(01) VALUE 'E'.
       77 ESEGUITA-KO                      PIC X(01) VALUE 'K'.
       77 CONSOLIDATA                      PIC X(01) VALUE 'S'.

       77 WK-STATE-CURRENT                 PIC X(01) VALUE SPACES.



      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVP013
      *   ULTIMO AGG. 07 DIC 2017
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-P01.
           MOVE P01-ID-RICH-EST
             TO (SF)-ID-PTF
           MOVE P01-ID-RICH-EST
             TO (SF)-ID-RICH-EST
           IF P01-ID-RICH-EST-COLLG-NULL = HIGH-VALUES
              MOVE P01-ID-RICH-EST-COLLG-NULL
                TO (SF)-ID-RICH-EST-COLLG-NULL
           ELSE
              MOVE P01-ID-RICH-EST-COLLG
                TO (SF)-ID-RICH-EST-COLLG
           END-IF
           IF P01-ID-LIQ-NULL = HIGH-VALUES
              MOVE P01-ID-LIQ-NULL
                TO (SF)-ID-LIQ-NULL
           ELSE
              MOVE P01-ID-LIQ
                TO (SF)-ID-LIQ
           END-IF
           MOVE P01-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           IF P01-IB-RICH-EST-NULL = HIGH-VALUES
              MOVE P01-IB-RICH-EST-NULL
                TO (SF)-IB-RICH-EST-NULL
           ELSE
              MOVE P01-IB-RICH-EST
                TO (SF)-IB-RICH-EST
           END-IF
           MOVE P01-TP-MOVI
             TO (SF)-TP-MOVI
           MOVE P01-DT-FORM-RICH
             TO (SF)-DT-FORM-RICH
           MOVE P01-DT-INVIO-RICH
             TO (SF)-DT-INVIO-RICH
           MOVE P01-DT-PERV-RICH
             TO (SF)-DT-PERV-RICH
           MOVE P01-DT-RGSTRZ-RICH
             TO (SF)-DT-RGSTRZ-RICH
           IF P01-DT-EFF-NULL = HIGH-VALUES
              MOVE P01-DT-EFF-NULL
                TO (SF)-DT-EFF-NULL
           ELSE
              MOVE P01-DT-EFF
                TO (SF)-DT-EFF
           END-IF
           IF P01-DT-SIN-NULL = HIGH-VALUES
              MOVE P01-DT-SIN-NULL
                TO (SF)-DT-SIN-NULL
           ELSE
              MOVE P01-DT-SIN
                TO (SF)-DT-SIN
           END-IF
           MOVE P01-TP-OGG
             TO (SF)-TP-OGG
           MOVE P01-ID-OGG
             TO (SF)-ID-OGG
           MOVE P01-IB-OGG
             TO (SF)-IB-OGG
           MOVE P01-FL-MOD-EXEC
             TO (SF)-FL-MOD-EXEC
           IF P01-ID-RICHIEDENTE-NULL = HIGH-VALUES
              MOVE P01-ID-RICHIEDENTE-NULL
                TO (SF)-ID-RICHIEDENTE-NULL
           ELSE
              MOVE P01-ID-RICHIEDENTE
                TO (SF)-ID-RICHIEDENTE
           END-IF
           MOVE P01-COD-PROD
             TO (SF)-COD-PROD
           MOVE P01-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE P01-DS-VER
             TO (SF)-DS-VER
           MOVE P01-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE P01-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE P01-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           MOVE P01-COD-CAN
             TO (SF)-COD-CAN
           IF P01-IB-OGG-ORIG-NULL = HIGH-VALUES
              MOVE P01-IB-OGG-ORIG-NULL
                TO (SF)-IB-OGG-ORIG-NULL
           ELSE
              MOVE P01-IB-OGG-ORIG
                TO (SF)-IB-OGG-ORIG
           END-IF
           IF P01-DT-EST-FINANZ-NULL = HIGH-VALUES
              MOVE P01-DT-EST-FINANZ-NULL
                TO (SF)-DT-EST-FINANZ-NULL
           ELSE
              MOVE P01-DT-EST-FINANZ
                TO (SF)-DT-EST-FINANZ
           END-IF
           IF P01-DT-MAN-COP-NULL = HIGH-VALUES
              MOVE P01-DT-MAN-COP-NULL
                TO (SF)-DT-MAN-COP-NULL
           ELSE
              MOVE P01-DT-MAN-COP
                TO (SF)-DT-MAN-COP
           END-IF
           IF P01-FL-GEST-PROTEZIONE-NULL = HIGH-VALUES
              MOVE P01-FL-GEST-PROTEZIONE-NULL
                TO (SF)-FL-GEST-PROTEZIONE-NULL
           ELSE
              MOVE P01-FL-GEST-PROTEZIONE
                TO (SF)-FL-GEST-PROTEZIONE
           END-IF.
       VALORIZZA-OUTPUT-P01-EX.
           EXIT.

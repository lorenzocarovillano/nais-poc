           EXEC SQL DECLARE COMP_NUM_OGG TABLE
           (
             COD_COMPAGNIA_ANIA  DECIMAL(5, 0) NOT NULL,
             FORMA_ASSICURATIVA  CHAR(2) NOT NULL,
             COD_OGGETTO         CHAR(30) NOT NULL,
             POSIZIONE           DECIMAL(5, 0) NOT NULL,
             COD_STR_DATO        CHAR(30),
             COD_DATO            CHAR(30),
             VALORE_DEFAULT      CHAR(50),
             LUNGHEZZA_DATO      DECIMAL(5, 0),
             FLAG_KEY_ULT_PROGR  CHAR(1)
          ) END-EXEC.

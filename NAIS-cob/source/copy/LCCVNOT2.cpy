      ******************************************************************
      **                                                              **
      **    PORTAFOGLIO VITA ITALIA                                   **
      **                                                              **
      ******************************************************************
       LETTURA-NOT.

           IF IDSI0011-SELECT
            PERFORM SELECT-DISPATCHER
                THRU SELECT-DISPATCHER-EX
         ELSE
            PERFORM FETCH-DISPATCHER
                THRU FETCH-DISPATCHER-EX
           END-IF.

       LETTURA-NOT-EX.
           EXIT.


      *----------------------------------------------------------------*
      *                         SELECT                                 *
      *----------------------------------------------------------------*
       SELECT-DISPATCHER.

           PERFORM CALL-DISPATCHER
              THRU CALL-DISPATCHER-EX.

           IF IDSO0011-SUCCESSFUL-RC
      *-->    GESTIRE ERRORE DB
              EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                     MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1300-SELECT-DISPATCHER'
                                             TO IEAI9901-LABEL-ERR
                     MOVE '005017'           TO IEAI9901-COD-ERRORE
                     MOVE 'NOT-ID-NOTE-OGG'  TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
                  WHEN IDSO0011-SUCCESSFUL-SQL
      *-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                     MOVE IDSO0011-BUFFER-DATI TO NOTE-OGG
                  WHEN OTHER
      *------  ERRORE DI ACCESSO AL DB
                     MOVE WK-PGM
                                        TO IEAI9901-COD-SERVIZIO-BE
                     MOVE 'S1300-SELECT-DISPATCHER'
                                        TO IEAI9901-LABEL-ERR
                     MOVE '005015'      TO IEAI9901-COD-ERRORE
                     MOVE WK-TABELLA    TO IEAI9901-PARAMETRI-ERR
                     PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        THRU EX-S0300
              END-EVALUATE
           ELSE
      *-->    GESTIRE ERRORE DISPATCHER
              MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
              MOVE 'S1300-SELECT-DISPATCHER' TO IEAI9901-LABEL-ERR
              MOVE '005016'                  TO IEAI9901-COD-ERRORE
              MOVE SPACES        TO IEAI9901-PARAMETRI-ERR
              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.
       SELECT-DISPATCHER-EX.
           EXIT.
      *----------------------------------------------------------------*
      *                         FETCH                                  *
      *----------------------------------------------------------------*
       FETCH-DISPATCHER.


             SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
             SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
             SET  WCOM-OVERFLOW-NO                  TO TRUE.

             PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                        OR NOT IDSO0011-SUCCESSFUL-SQL
                        OR WCOM-OVERFLOW-YES

                PERFORM CALL-DISPATCHER
                   THRU CALL-DISPATCHER-EX.

                IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
      *-->            NON TROVATA
                      WHEN IDSO0011-NOT-FOUND
                         IF IDSI0011-FETCH-FIRST
                           MOVE WK-PGM
                             TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'FETCH-DISPATCHER'
                             TO IEAI9901-LABEL-ERR
                           MOVE '005019'
                             TO IEAI9901-COD-ERRORE
                           MOVE 'NOT-ID-NOTE-OGG'
                             TO IEAI9901-PARAMETRI-ERR
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                         END-IF

                      WHEN IDSO0011-SUCCESSFUL-SQL
      *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                         MOVE IDSO0011-BUFFER-DATI TO NOTE-OGG
                          ADD 1 TO CONT-FETCH

      *-->  Se il contatore di lettura � maggiore dell' elemento MAX
      *-->  previsto per la TABELLA allora siamo in overflow
                         IF CONT-FETCH > (SF)-ELE-NOTE-OGG-MAX
                           SET WCOM-OVERFLOW-YES TO TRUE
                         ELSE
                           PERFORM VALORIZZA-OUTPUT-NOT
                              THRU VALORIZZA-OUTPUT-NOT-EX
                           SET  IDSI0011-FETCH-NEXT TO TRUE
                         END-IF
                      WHEN OTHER
      *-------------------ERRORE DI ACCESSO AL DB
                         MOVE WK-PGM
                           TO IEAI9901-COD-SERVIZIO-BE
                         MOVE 'FETCH-DISPATCHER'
                           TO IEAI9901-LABEL-ERR
                         MOVE '005015'
                           TO IEAI9901-COD-ERRORE
                         MOVE WK-TABELLA
                           TO IEAI9901-PARAMETRI-ERR
                         PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            THRU EX-S0300
                     END-EVALUATE
                ELSE
      *-->    GESTIRE ERRORE DISPATCHER
                      MOVE WK-PGM
                        TO IEAI9901-COD-SERVIZIO-BE
                      MOVE 'FETCH-DISPATCHER'
                        TO IEAI9901-LABEL-ERR
                      MOVE '005016'
                        TO IEAI9901-COD-ERRORE
                      MOVE SPACES
                        TO IEAI9901-PARAMETRI-ERR
                      PERFORM S0300-RICERCA-GRAVITA-ERRORE
                         THRU EX-S0300
                END-IF.
       FETCH-DISPATCHER-EX.



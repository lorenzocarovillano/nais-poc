      *-----------------------------------------------------------------*
      *   PORTAFOGLIO VITA - LEGGE BILANCIO
      *   AREA DI PAGINA
      *   PAGINA CALCOLO DELLE RISERVE
      *-----------------------------------------------------------------*
       01 WLB-REC-PREN.
          05 WLB-ID-RICH                     PIC 9(09).
          05 WLB-ID-RICH-COLL                PIC 9(09).
          05 WLB-ID-RICH-CONSOLID            PIC 9(09).
          05 WLB-DT-RISERVA                  PIC 9(08).
          05 WLB-DT-PRODUZIONE               PIC 9(08).
          05 WLB-DT-ESECUZIONE               PIC 9(08).
          05 WLB-DT-ANNULLO                  PIC 9(08).
          05 WLB-DT-CERTIFICAZ               PIC 9(08).
          05 WLB-DT-CONSOLID                 PIC 9(08).
          05 WLB-TP-FRM-ASSVA                PIC X(02).
          05 WLB-RAMO-BILA                   PIC X(12).
          05 WLB-COD-PROD                    PIC X(12).
          05 WLB-IB-POLI-FIRST               PIC X(40).
          05 WLB-IB-POLI-LAST                PIC X(40).
          05 WLB-IB-ADE-FIRST                PIC X(40).
          05 WLB-IB-ADE-LAST                 PIC X(40).
          05 WLB-TP-CALC-RIS                 PIC X(02).
          05 WLB-TP-RICH                     PIC X(02).
          05 WLB-TS-COMPETENZA               PIC 9(18).
          05 WLB-FL-SIMULAZIONE              PIC X(01).
          05 WLB-TP-INVST                    PIC 9(02).
          05 WLB-FL-CALCOLO-A-DATA-FUTURA    PIC X(01).

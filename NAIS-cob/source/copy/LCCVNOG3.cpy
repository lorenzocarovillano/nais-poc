
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVNOG3
      *   ULTIMO AGG. 02 FEB 2009
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-NOG.
           MOVE NOG-COD-COMPAGNIA-ANIA
             TO (SF)-COD-COMPAGNIA-ANIA
           MOVE NOG-FORMA-ASSICURATIVA
             TO (SF)-FORMA-ASSICURATIVA
           MOVE NOG-COD-OGGETTO
             TO (SF)-COD-OGGETTO
           MOVE NOG-TIPO-OGGETTO
             TO (SF)-TIPO-OGGETTO
           IF NOG-ULT-PROGR-NULL = HIGH-VALUES
              MOVE NOG-ULT-PROGR-NULL
                TO (SF)-ULT-PROGR-NULL
           ELSE
              MOVE NOG-ULT-PROGR
                TO (SF)-ULT-PROGR
           END-IF
           MOVE NOG-DESC-OGGETTO
             TO (SF)-DESC-OGGETTO.
       VALORIZZA-OUTPUT-NOG-EX.
           EXIT.

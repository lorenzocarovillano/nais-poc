
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVCLT3
      *   ULTIMO AGG. 28 GEN 2009
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-CLT.
           MOVE CLT-ID-CLAU-TXT
             TO (SF)-ID-PTF(IX-TAB-CLT)
           MOVE CLT-ID-CLAU-TXT
             TO (SF)-ID-CLAU-TXT(IX-TAB-CLT)
           MOVE CLT-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-CLT)
           MOVE CLT-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-CLT)
           MOVE CLT-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-CLT)
           IF CLT-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE CLT-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-CLT)
           ELSE
              MOVE CLT-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-CLT)
           END-IF
           MOVE CLT-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-CLT)
           MOVE CLT-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-CLT)
           MOVE CLT-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-CLT)
           IF CLT-TP-CLAU-NULL = HIGH-VALUES
              MOVE CLT-TP-CLAU-NULL
                TO (SF)-TP-CLAU-NULL(IX-TAB-CLT)
           ELSE
              MOVE CLT-TP-CLAU
                TO (SF)-TP-CLAU(IX-TAB-CLT)
           END-IF
           IF CLT-COD-CLAU-NULL = HIGH-VALUES
              MOVE CLT-COD-CLAU-NULL
                TO (SF)-COD-CLAU-NULL(IX-TAB-CLT)
           ELSE
              MOVE CLT-COD-CLAU
                TO (SF)-COD-CLAU(IX-TAB-CLT)
           END-IF
           IF CLT-DESC-BREVE-NULL = HIGH-VALUES
              MOVE CLT-DESC-BREVE-NULL
                TO (SF)-DESC-BREVE-NULL(IX-TAB-CLT)
           ELSE
              MOVE CLT-DESC-BREVE
                TO (SF)-DESC-BREVE(IX-TAB-CLT)
           END-IF
           MOVE CLT-DESC-LNG
             TO (SF)-DESC-LNG(IX-TAB-CLT)
           MOVE CLT-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-CLT)
           MOVE CLT-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-CLT)
           MOVE CLT-DS-VER
             TO (SF)-DS-VER(IX-TAB-CLT)
           MOVE CLT-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-CLT)
           MOVE CLT-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-CLT)
           MOVE CLT-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-CLT)
           MOVE CLT-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-CLT).
       VALORIZZA-OUTPUT-CLT-EX.
           EXIT.

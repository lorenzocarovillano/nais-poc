      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA TCONT_LIQ
      *   ALIAS TCL
      *   ULTIMO AGG. 02 LUG 2014
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-TCONT-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-PERC-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-BNFICR-LIQ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CRZ PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU PIC S9(9)     COMP-3.
             07 (SF)-ID-MOVI-CHIU-NULL REDEFINES
                (SF)-ID-MOVI-CHIU   PIC X(5).
             07 (SF)-DT-INI-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-END-EFF   PIC S9(8) COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-DT-VLT   PIC S9(8) COMP-3.
             07 (SF)-DT-VLT-NULL REDEFINES
                (SF)-DT-VLT   PIC X(5).
             07 (SF)-IMP-LRD-LIQTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-LRD-LIQTO-NULL REDEFINES
                (SF)-IMP-LRD-LIQTO   PIC X(8).
             07 (SF)-IMP-PREST PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-PREST-NULL REDEFINES
                (SF)-IMP-PREST   PIC X(8).
             07 (SF)-IMP-INTR-PREST PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-INTR-PREST-NULL REDEFINES
                (SF)-IMP-INTR-PREST   PIC X(8).
             07 (SF)-IMP-RAT PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-RAT-NULL REDEFINES
                (SF)-IMP-RAT   PIC X(8).
             07 (SF)-IMP-UTI PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-UTI-NULL REDEFINES
                (SF)-IMP-UTI   PIC X(8).
             07 (SF)-IMP-RIT-TFR PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-RIT-TFR-NULL REDEFINES
                (SF)-IMP-RIT-TFR   PIC X(8).
             07 (SF)-IMP-RIT-ACC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-RIT-ACC-NULL REDEFINES
                (SF)-IMP-RIT-ACC   PIC X(8).
             07 (SF)-IMP-RIT-VIS PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-RIT-VIS-NULL REDEFINES
                (SF)-IMP-RIT-VIS   PIC X(8).
             07 (SF)-IMPB-TFR PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-TFR-NULL REDEFINES
                (SF)-IMPB-TFR   PIC X(8).
             07 (SF)-IMPB-ACC PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-ACC-NULL REDEFINES
                (SF)-IMPB-ACC   PIC X(8).
             07 (SF)-IMPB-VIS PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-NULL REDEFINES
                (SF)-IMPB-VIS   PIC X(8).
             07 (SF)-IMP-RIMB PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-RIMB-NULL REDEFINES
                (SF)-IMP-RIMB   PIC X(8).
             07 (SF)-IMP-CORTVO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-CORTVO-NULL REDEFINES
                (SF)-IMP-CORTVO   PIC X(8).
             07 (SF)-IMPB-IMPST-PRVR PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IMPST-PRVR-NULL REDEFINES
                (SF)-IMPB-IMPST-PRVR   PIC X(8).
             07 (SF)-IMPST-PRVR PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-PRVR-NULL REDEFINES
                (SF)-IMPST-PRVR   PIC X(8).
             07 (SF)-IMPB-IMPST-252 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IMPST-252-NULL REDEFINES
                (SF)-IMPB-IMPST-252   PIC X(8).
             07 (SF)-IMPST-252 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-252-NULL REDEFINES
                (SF)-IMPST-252   PIC X(8).
             07 (SF)-IMP-IS PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-IS-NULL REDEFINES
                (SF)-IMP-IS   PIC X(8).
             07 (SF)-IMP-DIR-LIQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-DIR-LIQ-NULL REDEFINES
                (SF)-IMP-DIR-LIQ   PIC X(8).
             07 (SF)-IMP-NET-LIQTO PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-NET-LIQTO-NULL REDEFINES
                (SF)-IMP-NET-LIQTO   PIC X(8).
             07 (SF)-IMP-EFFLQ PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-EFFLQ-NULL REDEFINES
                (SF)-IMP-EFFLQ   PIC X(8).
             07 (SF)-COD-DVS PIC X(20).
             07 (SF)-COD-DVS-NULL REDEFINES
                (SF)-COD-DVS   PIC X(20).
             07 (SF)-TP-MEZ-PAG-ACCR PIC X(2).
             07 (SF)-TP-MEZ-PAG-ACCR-NULL REDEFINES
                (SF)-TP-MEZ-PAG-ACCR   PIC X(2).
             07 (SF)-ESTR-CNT-CORR-ACCR PIC X(20).
             07 (SF)-ESTR-CNT-CORR-ACCR-NULL REDEFINES
                (SF)-ESTR-CNT-CORR-ACCR   PIC X(20).
             07 (SF)-DS-RIGA PIC S9(10)     COMP-3.
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-INI-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-TS-END-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-TP-STAT-TIT PIC X(2).
             07 (SF)-TP-STAT-TIT-NULL REDEFINES
                (SF)-TP-STAT-TIT   PIC X(2).
             07 (SF)-IMPB-VIS-1382011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-1382011-NULL REDEFINES
                (SF)-IMPB-VIS-1382011   PIC X(8).
             07 (SF)-IMPST-VIS-1382011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-1382011-NULL REDEFINES
                (SF)-IMPST-VIS-1382011   PIC X(8).
             07 (SF)-IMPST-SOST-1382011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-SOST-1382011-NULL REDEFINES
                (SF)-IMPST-SOST-1382011   PIC X(8).
             07 (SF)-IMP-INTR-RIT-PAG PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMP-INTR-RIT-PAG-NULL REDEFINES
                (SF)-IMP-INTR-RIT-PAG   PIC X(8).
             07 (SF)-IMPB-IS PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-NULL REDEFINES
                (SF)-IMPB-IS   PIC X(8).
             07 (SF)-IMPB-IS-1382011 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-1382011-NULL REDEFINES
                (SF)-IMPB-IS-1382011   PIC X(8).
             07 (SF)-IMPB-VIS-662014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-VIS-662014-NULL REDEFINES
                (SF)-IMPB-VIS-662014   PIC X(8).
             07 (SF)-IMPST-VIS-662014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-VIS-662014-NULL REDEFINES
                (SF)-IMPST-VIS-662014   PIC X(8).
             07 (SF)-IMPB-IS-662014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPB-IS-662014-NULL REDEFINES
                (SF)-IMPB-IS-662014   PIC X(8).
             07 (SF)-IMPST-SOST-662014 PIC S9(12)V9(3) COMP-3.
             07 (SF)-IMPST-SOST-662014-NULL REDEFINES
                (SF)-IMPST-SOST-662014   PIC X(8).

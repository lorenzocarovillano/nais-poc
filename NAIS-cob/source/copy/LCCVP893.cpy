
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVP893
      *   ULTIMO AGG. 17 FEB 2015
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-P89.
           MOVE P89-ID-D-ATT-SERV-VAL
             TO (SF)-ID-PTF(IX-TAB-P89)
           MOVE P89-ID-D-ATT-SERV-VAL
             TO (SF)-ID-D-ATT-SERV-VAL(IX-TAB-P89)
           MOVE P89-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-P89)
           MOVE P89-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-P89)
           IF P89-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE P89-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-P89)
           ELSE
              MOVE P89-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-P89)
           END-IF
           MOVE P89-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-P89)
           MOVE P89-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-P89)
           MOVE P89-ID-ATT-SERV-VAL
             TO (SF)-ID-ATT-SERV-VAL(IX-TAB-P89)
           MOVE P89-TP-SERV-VAL
             TO (SF)-TP-SERV-VAL(IX-TAB-P89)
           MOVE P89-COD-FND
             TO (SF)-COD-FND(IX-TAB-P89)
           IF P89-DT-INI-CNTRL-FND-NULL = HIGH-VALUES
              MOVE P89-DT-INI-CNTRL-FND-NULL
                TO (SF)-DT-INI-CNTRL-FND-NULL(IX-TAB-P89)
           ELSE
              MOVE P89-DT-INI-CNTRL-FND
                TO (SF)-DT-INI-CNTRL-FND(IX-TAB-P89)
           END-IF
           MOVE P89-ID-OGG
             TO (SF)-ID-OGG(IX-TAB-P89)
           MOVE P89-TP-OGG
             TO (SF)-TP-OGG(IX-TAB-P89)
           MOVE P89-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-P89)
           MOVE P89-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-P89)
           MOVE P89-DS-VER
             TO (SF)-DS-VER(IX-TAB-P89)
           MOVE P89-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-P89)
           MOVE P89-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-P89)
           MOVE P89-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-P89)
           MOVE P89-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-P89).
       VALORIZZA-OUTPUT-P89-EX.
           EXIT.

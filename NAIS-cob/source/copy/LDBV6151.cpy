       01  LDBV6151.
           05 LDBV6151-TP-TIT-01              PIC X(02).
           05 LDBV6151-TP-TIT-02              PIC X(02).
           05 LDBV6151-TP-STAT-TIT            PIC X(02).
           05 LDBV6151-DT-MAX                 PIC 9(08).
           05 LDBV6151-DT-MAX-DB              PIC X(10).
           05 LDBV6151-DT-MAX-IND             PIC S9(4) COMP-5.
           05 LDBV6151-DT-DECOR-PREST         PIC S9(8) COMP-3.
           05 LDBV6151-DT-DECOR-PREST-DB      PIC X(10).
           05 LDBV6151-TP-OGG                 PIC  X(2).
           05 LDBV6151-ID-OGG                 PIC S9(9) COMP-3.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVDFL3
      *   ULTIMO AGG. 02 LUG 2014
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-DFL.
           MOVE DFL-ID-D-FORZ-LIQ
             TO (SF)-ID-PTF
           MOVE DFL-ID-D-FORZ-LIQ
             TO (SF)-ID-D-FORZ-LIQ
           MOVE DFL-ID-LIQ
             TO (SF)-ID-LIQ
           MOVE DFL-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ
           IF DFL-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE DFL-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL
           ELSE
              MOVE DFL-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU
           END-IF
           MOVE DFL-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE DFL-DT-INI-EFF
             TO (SF)-DT-INI-EFF
           MOVE DFL-DT-END-EFF
             TO (SF)-DT-END-EFF
           IF DFL-IMP-LRD-CALC-NULL = HIGH-VALUES
              MOVE DFL-IMP-LRD-CALC-NULL
                TO (SF)-IMP-LRD-CALC-NULL
           ELSE
              MOVE DFL-IMP-LRD-CALC
                TO (SF)-IMP-LRD-CALC
           END-IF
           IF DFL-IMP-LRD-DFZ-NULL = HIGH-VALUES
              MOVE DFL-IMP-LRD-DFZ-NULL
                TO (SF)-IMP-LRD-DFZ-NULL
           ELSE
              MOVE DFL-IMP-LRD-DFZ
                TO (SF)-IMP-LRD-DFZ
           END-IF
           IF DFL-IMP-LRD-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-IMP-LRD-EFFLQ-NULL
                TO (SF)-IMP-LRD-EFFLQ-NULL
           ELSE
              MOVE DFL-IMP-LRD-EFFLQ
                TO (SF)-IMP-LRD-EFFLQ
           END-IF
           IF DFL-IMP-NET-CALC-NULL = HIGH-VALUES
              MOVE DFL-IMP-NET-CALC-NULL
                TO (SF)-IMP-NET-CALC-NULL
           ELSE
              MOVE DFL-IMP-NET-CALC
                TO (SF)-IMP-NET-CALC
           END-IF
           IF DFL-IMP-NET-DFZ-NULL = HIGH-VALUES
              MOVE DFL-IMP-NET-DFZ-NULL
                TO (SF)-IMP-NET-DFZ-NULL
           ELSE
              MOVE DFL-IMP-NET-DFZ
                TO (SF)-IMP-NET-DFZ
           END-IF
           IF DFL-IMP-NET-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-IMP-NET-EFFLQ-NULL
                TO (SF)-IMP-NET-EFFLQ-NULL
           ELSE
              MOVE DFL-IMP-NET-EFFLQ
                TO (SF)-IMP-NET-EFFLQ
           END-IF
           IF DFL-IMPST-PRVR-CALC-NULL = HIGH-VALUES
              MOVE DFL-IMPST-PRVR-CALC-NULL
                TO (SF)-IMPST-PRVR-CALC-NULL
           ELSE
              MOVE DFL-IMPST-PRVR-CALC
                TO (SF)-IMPST-PRVR-CALC
           END-IF
           IF DFL-IMPST-PRVR-DFZ-NULL = HIGH-VALUES
              MOVE DFL-IMPST-PRVR-DFZ-NULL
                TO (SF)-IMPST-PRVR-DFZ-NULL
           ELSE
              MOVE DFL-IMPST-PRVR-DFZ
                TO (SF)-IMPST-PRVR-DFZ
           END-IF
           IF DFL-IMPST-PRVR-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-IMPST-PRVR-EFFLQ-NULL
                TO (SF)-IMPST-PRVR-EFFLQ-NULL
           ELSE
              MOVE DFL-IMPST-PRVR-EFFLQ
                TO (SF)-IMPST-PRVR-EFFLQ
           END-IF
           IF DFL-IMPST-VIS-CALC-NULL = HIGH-VALUES
              MOVE DFL-IMPST-VIS-CALC-NULL
                TO (SF)-IMPST-VIS-CALC-NULL
           ELSE
              MOVE DFL-IMPST-VIS-CALC
                TO (SF)-IMPST-VIS-CALC
           END-IF
           IF DFL-IMPST-VIS-DFZ-NULL = HIGH-VALUES
              MOVE DFL-IMPST-VIS-DFZ-NULL
                TO (SF)-IMPST-VIS-DFZ-NULL
           ELSE
              MOVE DFL-IMPST-VIS-DFZ
                TO (SF)-IMPST-VIS-DFZ
           END-IF
           IF DFL-IMPST-VIS-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-IMPST-VIS-EFFLQ-NULL
                TO (SF)-IMPST-VIS-EFFLQ-NULL
           ELSE
              MOVE DFL-IMPST-VIS-EFFLQ
                TO (SF)-IMPST-VIS-EFFLQ
           END-IF
           IF DFL-RIT-ACC-CALC-NULL = HIGH-VALUES
              MOVE DFL-RIT-ACC-CALC-NULL
                TO (SF)-RIT-ACC-CALC-NULL
           ELSE
              MOVE DFL-RIT-ACC-CALC
                TO (SF)-RIT-ACC-CALC
           END-IF
           IF DFL-RIT-ACC-DFZ-NULL = HIGH-VALUES
              MOVE DFL-RIT-ACC-DFZ-NULL
                TO (SF)-RIT-ACC-DFZ-NULL
           ELSE
              MOVE DFL-RIT-ACC-DFZ
                TO (SF)-RIT-ACC-DFZ
           END-IF
           IF DFL-RIT-ACC-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-RIT-ACC-EFFLQ-NULL
                TO (SF)-RIT-ACC-EFFLQ-NULL
           ELSE
              MOVE DFL-RIT-ACC-EFFLQ
                TO (SF)-RIT-ACC-EFFLQ
           END-IF
           IF DFL-RIT-IRPEF-CALC-NULL = HIGH-VALUES
              MOVE DFL-RIT-IRPEF-CALC-NULL
                TO (SF)-RIT-IRPEF-CALC-NULL
           ELSE
              MOVE DFL-RIT-IRPEF-CALC
                TO (SF)-RIT-IRPEF-CALC
           END-IF
           IF DFL-RIT-IRPEF-DFZ-NULL = HIGH-VALUES
              MOVE DFL-RIT-IRPEF-DFZ-NULL
                TO (SF)-RIT-IRPEF-DFZ-NULL
           ELSE
              MOVE DFL-RIT-IRPEF-DFZ
                TO (SF)-RIT-IRPEF-DFZ
           END-IF
           IF DFL-RIT-IRPEF-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-RIT-IRPEF-EFFLQ-NULL
                TO (SF)-RIT-IRPEF-EFFLQ-NULL
           ELSE
              MOVE DFL-RIT-IRPEF-EFFLQ
                TO (SF)-RIT-IRPEF-EFFLQ
           END-IF
           IF DFL-IMPST-SOST-CALC-NULL = HIGH-VALUES
              MOVE DFL-IMPST-SOST-CALC-NULL
                TO (SF)-IMPST-SOST-CALC-NULL
           ELSE
              MOVE DFL-IMPST-SOST-CALC
                TO (SF)-IMPST-SOST-CALC
           END-IF
           IF DFL-IMPST-SOST-DFZ-NULL = HIGH-VALUES
              MOVE DFL-IMPST-SOST-DFZ-NULL
                TO (SF)-IMPST-SOST-DFZ-NULL
           ELSE
              MOVE DFL-IMPST-SOST-DFZ
                TO (SF)-IMPST-SOST-DFZ
           END-IF
           IF DFL-IMPST-SOST-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-IMPST-SOST-EFFLQ-NULL
                TO (SF)-IMPST-SOST-EFFLQ-NULL
           ELSE
              MOVE DFL-IMPST-SOST-EFFLQ
                TO (SF)-IMPST-SOST-EFFLQ
           END-IF
           IF DFL-TAX-SEP-CALC-NULL = HIGH-VALUES
              MOVE DFL-TAX-SEP-CALC-NULL
                TO (SF)-TAX-SEP-CALC-NULL
           ELSE
              MOVE DFL-TAX-SEP-CALC
                TO (SF)-TAX-SEP-CALC
           END-IF
           IF DFL-TAX-SEP-DFZ-NULL = HIGH-VALUES
              MOVE DFL-TAX-SEP-DFZ-NULL
                TO (SF)-TAX-SEP-DFZ-NULL
           ELSE
              MOVE DFL-TAX-SEP-DFZ
                TO (SF)-TAX-SEP-DFZ
           END-IF
           IF DFL-TAX-SEP-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-TAX-SEP-EFFLQ-NULL
                TO (SF)-TAX-SEP-EFFLQ-NULL
           ELSE
              MOVE DFL-TAX-SEP-EFFLQ
                TO (SF)-TAX-SEP-EFFLQ
           END-IF
           IF DFL-INTR-PREST-CALC-NULL = HIGH-VALUES
              MOVE DFL-INTR-PREST-CALC-NULL
                TO (SF)-INTR-PREST-CALC-NULL
           ELSE
              MOVE DFL-INTR-PREST-CALC
                TO (SF)-INTR-PREST-CALC
           END-IF
           IF DFL-INTR-PREST-DFZ-NULL = HIGH-VALUES
              MOVE DFL-INTR-PREST-DFZ-NULL
                TO (SF)-INTR-PREST-DFZ-NULL
           ELSE
              MOVE DFL-INTR-PREST-DFZ
                TO (SF)-INTR-PREST-DFZ
           END-IF
           IF DFL-INTR-PREST-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-INTR-PREST-EFFLQ-NULL
                TO (SF)-INTR-PREST-EFFLQ-NULL
           ELSE
              MOVE DFL-INTR-PREST-EFFLQ
                TO (SF)-INTR-PREST-EFFLQ
           END-IF
           IF DFL-ACCPRE-SOST-CALC-NULL = HIGH-VALUES
              MOVE DFL-ACCPRE-SOST-CALC-NULL
                TO (SF)-ACCPRE-SOST-CALC-NULL
           ELSE
              MOVE DFL-ACCPRE-SOST-CALC
                TO (SF)-ACCPRE-SOST-CALC
           END-IF
           IF DFL-ACCPRE-SOST-DFZ-NULL = HIGH-VALUES
              MOVE DFL-ACCPRE-SOST-DFZ-NULL
                TO (SF)-ACCPRE-SOST-DFZ-NULL
           ELSE
              MOVE DFL-ACCPRE-SOST-DFZ
                TO (SF)-ACCPRE-SOST-DFZ
           END-IF
           IF DFL-ACCPRE-SOST-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-ACCPRE-SOST-EFFLQ-NULL
                TO (SF)-ACCPRE-SOST-EFFLQ-NULL
           ELSE
              MOVE DFL-ACCPRE-SOST-EFFLQ
                TO (SF)-ACCPRE-SOST-EFFLQ
           END-IF
           IF DFL-ACCPRE-VIS-CALC-NULL = HIGH-VALUES
              MOVE DFL-ACCPRE-VIS-CALC-NULL
                TO (SF)-ACCPRE-VIS-CALC-NULL
           ELSE
              MOVE DFL-ACCPRE-VIS-CALC
                TO (SF)-ACCPRE-VIS-CALC
           END-IF
           IF DFL-ACCPRE-VIS-DFZ-NULL = HIGH-VALUES
              MOVE DFL-ACCPRE-VIS-DFZ-NULL
                TO (SF)-ACCPRE-VIS-DFZ-NULL
           ELSE
              MOVE DFL-ACCPRE-VIS-DFZ
                TO (SF)-ACCPRE-VIS-DFZ
           END-IF
           IF DFL-ACCPRE-VIS-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-ACCPRE-VIS-EFFLQ-NULL
                TO (SF)-ACCPRE-VIS-EFFLQ-NULL
           ELSE
              MOVE DFL-ACCPRE-VIS-EFFLQ
                TO (SF)-ACCPRE-VIS-EFFLQ
           END-IF
           IF DFL-ACCPRE-ACC-CALC-NULL = HIGH-VALUES
              MOVE DFL-ACCPRE-ACC-CALC-NULL
                TO (SF)-ACCPRE-ACC-CALC-NULL
           ELSE
              MOVE DFL-ACCPRE-ACC-CALC
                TO (SF)-ACCPRE-ACC-CALC
           END-IF
           IF DFL-ACCPRE-ACC-DFZ-NULL = HIGH-VALUES
              MOVE DFL-ACCPRE-ACC-DFZ-NULL
                TO (SF)-ACCPRE-ACC-DFZ-NULL
           ELSE
              MOVE DFL-ACCPRE-ACC-DFZ
                TO (SF)-ACCPRE-ACC-DFZ
           END-IF
           IF DFL-ACCPRE-ACC-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-ACCPRE-ACC-EFFLQ-NULL
                TO (SF)-ACCPRE-ACC-EFFLQ-NULL
           ELSE
              MOVE DFL-ACCPRE-ACC-EFFLQ
                TO (SF)-ACCPRE-ACC-EFFLQ
           END-IF
           IF DFL-RES-PRSTZ-CALC-NULL = HIGH-VALUES
              MOVE DFL-RES-PRSTZ-CALC-NULL
                TO (SF)-RES-PRSTZ-CALC-NULL
           ELSE
              MOVE DFL-RES-PRSTZ-CALC
                TO (SF)-RES-PRSTZ-CALC
           END-IF
           IF DFL-RES-PRSTZ-DFZ-NULL = HIGH-VALUES
              MOVE DFL-RES-PRSTZ-DFZ-NULL
                TO (SF)-RES-PRSTZ-DFZ-NULL
           ELSE
              MOVE DFL-RES-PRSTZ-DFZ
                TO (SF)-RES-PRSTZ-DFZ
           END-IF
           IF DFL-RES-PRSTZ-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-RES-PRSTZ-EFFLQ-NULL
                TO (SF)-RES-PRSTZ-EFFLQ-NULL
           ELSE
              MOVE DFL-RES-PRSTZ-EFFLQ
                TO (SF)-RES-PRSTZ-EFFLQ
           END-IF
           IF DFL-RES-PRE-ATT-CALC-NULL = HIGH-VALUES
              MOVE DFL-RES-PRE-ATT-CALC-NULL
                TO (SF)-RES-PRE-ATT-CALC-NULL
           ELSE
              MOVE DFL-RES-PRE-ATT-CALC
                TO (SF)-RES-PRE-ATT-CALC
           END-IF
           IF DFL-RES-PRE-ATT-DFZ-NULL = HIGH-VALUES
              MOVE DFL-RES-PRE-ATT-DFZ-NULL
                TO (SF)-RES-PRE-ATT-DFZ-NULL
           ELSE
              MOVE DFL-RES-PRE-ATT-DFZ
                TO (SF)-RES-PRE-ATT-DFZ
           END-IF
           IF DFL-RES-PRE-ATT-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-RES-PRE-ATT-EFFLQ-NULL
                TO (SF)-RES-PRE-ATT-EFFLQ-NULL
           ELSE
              MOVE DFL-RES-PRE-ATT-EFFLQ
                TO (SF)-RES-PRE-ATT-EFFLQ
           END-IF
           IF DFL-IMP-EXCONTR-EFF-NULL = HIGH-VALUES
              MOVE DFL-IMP-EXCONTR-EFF-NULL
                TO (SF)-IMP-EXCONTR-EFF-NULL
           ELSE
              MOVE DFL-IMP-EXCONTR-EFF
                TO (SF)-IMP-EXCONTR-EFF
           END-IF
           IF DFL-IMPB-VIS-CALC-NULL = HIGH-VALUES
              MOVE DFL-IMPB-VIS-CALC-NULL
                TO (SF)-IMPB-VIS-CALC-NULL
           ELSE
              MOVE DFL-IMPB-VIS-CALC
                TO (SF)-IMPB-VIS-CALC
           END-IF
           IF DFL-IMPB-VIS-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-IMPB-VIS-EFFLQ-NULL
                TO (SF)-IMPB-VIS-EFFLQ-NULL
           ELSE
              MOVE DFL-IMPB-VIS-EFFLQ
                TO (SF)-IMPB-VIS-EFFLQ
           END-IF
           IF DFL-IMPB-VIS-DFZ-NULL = HIGH-VALUES
              MOVE DFL-IMPB-VIS-DFZ-NULL
                TO (SF)-IMPB-VIS-DFZ-NULL
           ELSE
              MOVE DFL-IMPB-VIS-DFZ
                TO (SF)-IMPB-VIS-DFZ
           END-IF
           IF DFL-IMPB-RIT-ACC-CALC-NULL = HIGH-VALUES
              MOVE DFL-IMPB-RIT-ACC-CALC-NULL
                TO (SF)-IMPB-RIT-ACC-CALC-NULL
           ELSE
              MOVE DFL-IMPB-RIT-ACC-CALC
                TO (SF)-IMPB-RIT-ACC-CALC
           END-IF
           IF DFL-IMPB-RIT-ACC-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-IMPB-RIT-ACC-EFFLQ-NULL
                TO (SF)-IMPB-RIT-ACC-EFFLQ-NULL
           ELSE
              MOVE DFL-IMPB-RIT-ACC-EFFLQ
                TO (SF)-IMPB-RIT-ACC-EFFLQ
           END-IF
           IF DFL-IMPB-RIT-ACC-DFZ-NULL = HIGH-VALUES
              MOVE DFL-IMPB-RIT-ACC-DFZ-NULL
                TO (SF)-IMPB-RIT-ACC-DFZ-NULL
           ELSE
              MOVE DFL-IMPB-RIT-ACC-DFZ
                TO (SF)-IMPB-RIT-ACC-DFZ
           END-IF
           IF DFL-IMPB-TFR-CALC-NULL = HIGH-VALUES
              MOVE DFL-IMPB-TFR-CALC-NULL
                TO (SF)-IMPB-TFR-CALC-NULL
           ELSE
              MOVE DFL-IMPB-TFR-CALC
                TO (SF)-IMPB-TFR-CALC
           END-IF
           IF DFL-IMPB-TFR-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-IMPB-TFR-EFFLQ-NULL
                TO (SF)-IMPB-TFR-EFFLQ-NULL
           ELSE
              MOVE DFL-IMPB-TFR-EFFLQ
                TO (SF)-IMPB-TFR-EFFLQ
           END-IF
           IF DFL-IMPB-TFR-DFZ-NULL = HIGH-VALUES
              MOVE DFL-IMPB-TFR-DFZ-NULL
                TO (SF)-IMPB-TFR-DFZ-NULL
           ELSE
              MOVE DFL-IMPB-TFR-DFZ
                TO (SF)-IMPB-TFR-DFZ
           END-IF
           IF DFL-IMPB-IS-CALC-NULL = HIGH-VALUES
              MOVE DFL-IMPB-IS-CALC-NULL
                TO (SF)-IMPB-IS-CALC-NULL
           ELSE
              MOVE DFL-IMPB-IS-CALC
                TO (SF)-IMPB-IS-CALC
           END-IF
           IF DFL-IMPB-IS-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-IMPB-IS-EFFLQ-NULL
                TO (SF)-IMPB-IS-EFFLQ-NULL
           ELSE
              MOVE DFL-IMPB-IS-EFFLQ
                TO (SF)-IMPB-IS-EFFLQ
           END-IF
           IF DFL-IMPB-IS-DFZ-NULL = HIGH-VALUES
              MOVE DFL-IMPB-IS-DFZ-NULL
                TO (SF)-IMPB-IS-DFZ-NULL
           ELSE
              MOVE DFL-IMPB-IS-DFZ
                TO (SF)-IMPB-IS-DFZ
           END-IF
           IF DFL-IMPB-TAX-SEP-CALC-NULL = HIGH-VALUES
              MOVE DFL-IMPB-TAX-SEP-CALC-NULL
                TO (SF)-IMPB-TAX-SEP-CALC-NULL
           ELSE
              MOVE DFL-IMPB-TAX-SEP-CALC
                TO (SF)-IMPB-TAX-SEP-CALC
           END-IF
           IF DFL-IMPB-TAX-SEP-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-IMPB-TAX-SEP-EFFLQ-NULL
                TO (SF)-IMPB-TAX-SEP-EFFLQ-NULL
           ELSE
              MOVE DFL-IMPB-TAX-SEP-EFFLQ
                TO (SF)-IMPB-TAX-SEP-EFFLQ
           END-IF
           IF DFL-IMPB-TAX-SEP-DFZ-NULL = HIGH-VALUES
              MOVE DFL-IMPB-TAX-SEP-DFZ-NULL
                TO (SF)-IMPB-TAX-SEP-DFZ-NULL
           ELSE
              MOVE DFL-IMPB-TAX-SEP-DFZ
                TO (SF)-IMPB-TAX-SEP-DFZ
           END-IF
           IF DFL-IINT-PREST-CALC-NULL = HIGH-VALUES
              MOVE DFL-IINT-PREST-CALC-NULL
                TO (SF)-IINT-PREST-CALC-NULL
           ELSE
              MOVE DFL-IINT-PREST-CALC
                TO (SF)-IINT-PREST-CALC
           END-IF
           IF DFL-IINT-PREST-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-IINT-PREST-EFFLQ-NULL
                TO (SF)-IINT-PREST-EFFLQ-NULL
           ELSE
              MOVE DFL-IINT-PREST-EFFLQ
                TO (SF)-IINT-PREST-EFFLQ
           END-IF
           IF DFL-IINT-PREST-DFZ-NULL = HIGH-VALUES
              MOVE DFL-IINT-PREST-DFZ-NULL
                TO (SF)-IINT-PREST-DFZ-NULL
           ELSE
              MOVE DFL-IINT-PREST-DFZ
                TO (SF)-IINT-PREST-DFZ
           END-IF
           IF DFL-MONT-END2000-CALC-NULL = HIGH-VALUES
              MOVE DFL-MONT-END2000-CALC-NULL
                TO (SF)-MONT-END2000-CALC-NULL
           ELSE
              MOVE DFL-MONT-END2000-CALC
                TO (SF)-MONT-END2000-CALC
           END-IF
           IF DFL-MONT-END2000-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-MONT-END2000-EFFLQ-NULL
                TO (SF)-MONT-END2000-EFFLQ-NULL
           ELSE
              MOVE DFL-MONT-END2000-EFFLQ
                TO (SF)-MONT-END2000-EFFLQ
           END-IF
           IF DFL-MONT-END2000-DFZ-NULL = HIGH-VALUES
              MOVE DFL-MONT-END2000-DFZ-NULL
                TO (SF)-MONT-END2000-DFZ-NULL
           ELSE
              MOVE DFL-MONT-END2000-DFZ
                TO (SF)-MONT-END2000-DFZ
           END-IF
           IF DFL-MONT-END2006-CALC-NULL = HIGH-VALUES
              MOVE DFL-MONT-END2006-CALC-NULL
                TO (SF)-MONT-END2006-CALC-NULL
           ELSE
              MOVE DFL-MONT-END2006-CALC
                TO (SF)-MONT-END2006-CALC
           END-IF
           IF DFL-MONT-END2006-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-MONT-END2006-EFFLQ-NULL
                TO (SF)-MONT-END2006-EFFLQ-NULL
           ELSE
              MOVE DFL-MONT-END2006-EFFLQ
                TO (SF)-MONT-END2006-EFFLQ
           END-IF
           IF DFL-MONT-END2006-DFZ-NULL = HIGH-VALUES
              MOVE DFL-MONT-END2006-DFZ-NULL
                TO (SF)-MONT-END2006-DFZ-NULL
           ELSE
              MOVE DFL-MONT-END2006-DFZ
                TO (SF)-MONT-END2006-DFZ
           END-IF
           IF DFL-MONT-DAL2007-CALC-NULL = HIGH-VALUES
              MOVE DFL-MONT-DAL2007-CALC-NULL
                TO (SF)-MONT-DAL2007-CALC-NULL
           ELSE
              MOVE DFL-MONT-DAL2007-CALC
                TO (SF)-MONT-DAL2007-CALC
           END-IF
           IF DFL-MONT-DAL2007-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-MONT-DAL2007-EFFLQ-NULL
                TO (SF)-MONT-DAL2007-EFFLQ-NULL
           ELSE
              MOVE DFL-MONT-DAL2007-EFFLQ
                TO (SF)-MONT-DAL2007-EFFLQ
           END-IF
           IF DFL-MONT-DAL2007-DFZ-NULL = HIGH-VALUES
              MOVE DFL-MONT-DAL2007-DFZ-NULL
                TO (SF)-MONT-DAL2007-DFZ-NULL
           ELSE
              MOVE DFL-MONT-DAL2007-DFZ
                TO (SF)-MONT-DAL2007-DFZ
           END-IF
           IF DFL-IIMPST-PRVR-CALC-NULL = HIGH-VALUES
              MOVE DFL-IIMPST-PRVR-CALC-NULL
                TO (SF)-IIMPST-PRVR-CALC-NULL
           ELSE
              MOVE DFL-IIMPST-PRVR-CALC
                TO (SF)-IIMPST-PRVR-CALC
           END-IF
           IF DFL-IIMPST-PRVR-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-IIMPST-PRVR-EFFLQ-NULL
                TO (SF)-IIMPST-PRVR-EFFLQ-NULL
           ELSE
              MOVE DFL-IIMPST-PRVR-EFFLQ
                TO (SF)-IIMPST-PRVR-EFFLQ
           END-IF
           IF DFL-IIMPST-PRVR-DFZ-NULL = HIGH-VALUES
              MOVE DFL-IIMPST-PRVR-DFZ-NULL
                TO (SF)-IIMPST-PRVR-DFZ-NULL
           ELSE
              MOVE DFL-IIMPST-PRVR-DFZ
                TO (SF)-IIMPST-PRVR-DFZ
           END-IF
           IF DFL-IIMPST-252-CALC-NULL = HIGH-VALUES
              MOVE DFL-IIMPST-252-CALC-NULL
                TO (SF)-IIMPST-252-CALC-NULL
           ELSE
              MOVE DFL-IIMPST-252-CALC
                TO (SF)-IIMPST-252-CALC
           END-IF
           IF DFL-IIMPST-252-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-IIMPST-252-EFFLQ-NULL
                TO (SF)-IIMPST-252-EFFLQ-NULL
           ELSE
              MOVE DFL-IIMPST-252-EFFLQ
                TO (SF)-IIMPST-252-EFFLQ
           END-IF
           IF DFL-IIMPST-252-DFZ-NULL = HIGH-VALUES
              MOVE DFL-IIMPST-252-DFZ-NULL
                TO (SF)-IIMPST-252-DFZ-NULL
           ELSE
              MOVE DFL-IIMPST-252-DFZ
                TO (SF)-IIMPST-252-DFZ
           END-IF
           IF DFL-IMPST-252-CALC-NULL = HIGH-VALUES
              MOVE DFL-IMPST-252-CALC-NULL
                TO (SF)-IMPST-252-CALC-NULL
           ELSE
              MOVE DFL-IMPST-252-CALC
                TO (SF)-IMPST-252-CALC
           END-IF
           IF DFL-IMPST-252-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-IMPST-252-EFFLQ-NULL
                TO (SF)-IMPST-252-EFFLQ-NULL
           ELSE
              MOVE DFL-IMPST-252-EFFLQ
                TO (SF)-IMPST-252-EFFLQ
           END-IF
           IF DFL-RIT-TFR-CALC-NULL = HIGH-VALUES
              MOVE DFL-RIT-TFR-CALC-NULL
                TO (SF)-RIT-TFR-CALC-NULL
           ELSE
              MOVE DFL-RIT-TFR-CALC
                TO (SF)-RIT-TFR-CALC
           END-IF
           IF DFL-RIT-TFR-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-RIT-TFR-EFFLQ-NULL
                TO (SF)-RIT-TFR-EFFLQ-NULL
           ELSE
              MOVE DFL-RIT-TFR-EFFLQ
                TO (SF)-RIT-TFR-EFFLQ
           END-IF
           IF DFL-RIT-TFR-DFZ-NULL = HIGH-VALUES
              MOVE DFL-RIT-TFR-DFZ-NULL
                TO (SF)-RIT-TFR-DFZ-NULL
           ELSE
              MOVE DFL-RIT-TFR-DFZ
                TO (SF)-RIT-TFR-DFZ
           END-IF
           IF DFL-CNBT-INPSTFM-CALC-NULL = HIGH-VALUES
              MOVE DFL-CNBT-INPSTFM-CALC-NULL
                TO (SF)-CNBT-INPSTFM-CALC-NULL
           ELSE
              MOVE DFL-CNBT-INPSTFM-CALC
                TO (SF)-CNBT-INPSTFM-CALC
           END-IF
           IF DFL-CNBT-INPSTFM-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-CNBT-INPSTFM-EFFLQ-NULL
                TO (SF)-CNBT-INPSTFM-EFFLQ-NULL
           ELSE
              MOVE DFL-CNBT-INPSTFM-EFFLQ
                TO (SF)-CNBT-INPSTFM-EFFLQ
           END-IF
           IF DFL-CNBT-INPSTFM-DFZ-NULL = HIGH-VALUES
              MOVE DFL-CNBT-INPSTFM-DFZ-NULL
                TO (SF)-CNBT-INPSTFM-DFZ-NULL
           ELSE
              MOVE DFL-CNBT-INPSTFM-DFZ
                TO (SF)-CNBT-INPSTFM-DFZ
           END-IF
           IF DFL-ICNB-INPSTFM-CALC-NULL = HIGH-VALUES
              MOVE DFL-ICNB-INPSTFM-CALC-NULL
                TO (SF)-ICNB-INPSTFM-CALC-NULL
           ELSE
              MOVE DFL-ICNB-INPSTFM-CALC
                TO (SF)-ICNB-INPSTFM-CALC
           END-IF
           IF DFL-ICNB-INPSTFM-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-ICNB-INPSTFM-EFFLQ-NULL
                TO (SF)-ICNB-INPSTFM-EFFLQ-NULL
           ELSE
              MOVE DFL-ICNB-INPSTFM-EFFLQ
                TO (SF)-ICNB-INPSTFM-EFFLQ
           END-IF
           IF DFL-ICNB-INPSTFM-DFZ-NULL = HIGH-VALUES
              MOVE DFL-ICNB-INPSTFM-DFZ-NULL
                TO (SF)-ICNB-INPSTFM-DFZ-NULL
           ELSE
              MOVE DFL-ICNB-INPSTFM-DFZ
                TO (SF)-ICNB-INPSTFM-DFZ
           END-IF
           IF DFL-CNDE-END2000-CALC-NULL = HIGH-VALUES
              MOVE DFL-CNDE-END2000-CALC-NULL
                TO (SF)-CNDE-END2000-CALC-NULL
           ELSE
              MOVE DFL-CNDE-END2000-CALC
                TO (SF)-CNDE-END2000-CALC
           END-IF
           IF DFL-CNDE-END2000-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-CNDE-END2000-EFFLQ-NULL
                TO (SF)-CNDE-END2000-EFFLQ-NULL
           ELSE
              MOVE DFL-CNDE-END2000-EFFLQ
                TO (SF)-CNDE-END2000-EFFLQ
           END-IF
           IF DFL-CNDE-END2000-DFZ-NULL = HIGH-VALUES
              MOVE DFL-CNDE-END2000-DFZ-NULL
                TO (SF)-CNDE-END2000-DFZ-NULL
           ELSE
              MOVE DFL-CNDE-END2000-DFZ
                TO (SF)-CNDE-END2000-DFZ
           END-IF
           IF DFL-CNDE-END2006-CALC-NULL = HIGH-VALUES
              MOVE DFL-CNDE-END2006-CALC-NULL
                TO (SF)-CNDE-END2006-CALC-NULL
           ELSE
              MOVE DFL-CNDE-END2006-CALC
                TO (SF)-CNDE-END2006-CALC
           END-IF
           IF DFL-CNDE-END2006-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-CNDE-END2006-EFFLQ-NULL
                TO (SF)-CNDE-END2006-EFFLQ-NULL
           ELSE
              MOVE DFL-CNDE-END2006-EFFLQ
                TO (SF)-CNDE-END2006-EFFLQ
           END-IF
           IF DFL-CNDE-END2006-DFZ-NULL = HIGH-VALUES
              MOVE DFL-CNDE-END2006-DFZ-NULL
                TO (SF)-CNDE-END2006-DFZ-NULL
           ELSE
              MOVE DFL-CNDE-END2006-DFZ
                TO (SF)-CNDE-END2006-DFZ
           END-IF
           IF DFL-CNDE-DAL2007-CALC-NULL = HIGH-VALUES
              MOVE DFL-CNDE-DAL2007-CALC-NULL
                TO (SF)-CNDE-DAL2007-CALC-NULL
           ELSE
              MOVE DFL-CNDE-DAL2007-CALC
                TO (SF)-CNDE-DAL2007-CALC
           END-IF
           IF DFL-CNDE-DAL2007-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-CNDE-DAL2007-EFFLQ-NULL
                TO (SF)-CNDE-DAL2007-EFFLQ-NULL
           ELSE
              MOVE DFL-CNDE-DAL2007-EFFLQ
                TO (SF)-CNDE-DAL2007-EFFLQ
           END-IF
           IF DFL-CNDE-DAL2007-DFZ-NULL = HIGH-VALUES
              MOVE DFL-CNDE-DAL2007-DFZ-NULL
                TO (SF)-CNDE-DAL2007-DFZ-NULL
           ELSE
              MOVE DFL-CNDE-DAL2007-DFZ
                TO (SF)-CNDE-DAL2007-DFZ
           END-IF
           IF DFL-AA-CNBZ-END2000-EF-NULL = HIGH-VALUES
              MOVE DFL-AA-CNBZ-END2000-EF-NULL
                TO (SF)-AA-CNBZ-END2000-EF-NULL
           ELSE
              MOVE DFL-AA-CNBZ-END2000-EF
                TO (SF)-AA-CNBZ-END2000-EF
           END-IF
           IF DFL-AA-CNBZ-END2006-EF-NULL = HIGH-VALUES
              MOVE DFL-AA-CNBZ-END2006-EF-NULL
                TO (SF)-AA-CNBZ-END2006-EF-NULL
           ELSE
              MOVE DFL-AA-CNBZ-END2006-EF
                TO (SF)-AA-CNBZ-END2006-EF
           END-IF
           IF DFL-AA-CNBZ-DAL2007-EF-NULL = HIGH-VALUES
              MOVE DFL-AA-CNBZ-DAL2007-EF-NULL
                TO (SF)-AA-CNBZ-DAL2007-EF-NULL
           ELSE
              MOVE DFL-AA-CNBZ-DAL2007-EF
                TO (SF)-AA-CNBZ-DAL2007-EF
           END-IF
           IF DFL-MM-CNBZ-END2000-EF-NULL = HIGH-VALUES
              MOVE DFL-MM-CNBZ-END2000-EF-NULL
                TO (SF)-MM-CNBZ-END2000-EF-NULL
           ELSE
              MOVE DFL-MM-CNBZ-END2000-EF
                TO (SF)-MM-CNBZ-END2000-EF
           END-IF
           IF DFL-MM-CNBZ-END2006-EF-NULL = HIGH-VALUES
              MOVE DFL-MM-CNBZ-END2006-EF-NULL
                TO (SF)-MM-CNBZ-END2006-EF-NULL
           ELSE
              MOVE DFL-MM-CNBZ-END2006-EF
                TO (SF)-MM-CNBZ-END2006-EF
           END-IF
           IF DFL-MM-CNBZ-DAL2007-EF-NULL = HIGH-VALUES
              MOVE DFL-MM-CNBZ-DAL2007-EF-NULL
                TO (SF)-MM-CNBZ-DAL2007-EF-NULL
           ELSE
              MOVE DFL-MM-CNBZ-DAL2007-EF
                TO (SF)-MM-CNBZ-DAL2007-EF
           END-IF
           IF DFL-IMPST-DA-RIMB-EFF-NULL = HIGH-VALUES
              MOVE DFL-IMPST-DA-RIMB-EFF-NULL
                TO (SF)-IMPST-DA-RIMB-EFF-NULL
           ELSE
              MOVE DFL-IMPST-DA-RIMB-EFF
                TO (SF)-IMPST-DA-RIMB-EFF
           END-IF
           IF DFL-ALQ-TAX-SEP-CALC-NULL = HIGH-VALUES
              MOVE DFL-ALQ-TAX-SEP-CALC-NULL
                TO (SF)-ALQ-TAX-SEP-CALC-NULL
           ELSE
              MOVE DFL-ALQ-TAX-SEP-CALC
                TO (SF)-ALQ-TAX-SEP-CALC
           END-IF
           IF DFL-ALQ-TAX-SEP-EFFLQ-NULL = HIGH-VALUES
              MOVE DFL-ALQ-TAX-SEP-EFFLQ-NULL
                TO (SF)-ALQ-TAX-SEP-EFFLQ-NULL
           ELSE
              MOVE DFL-ALQ-TAX-SEP-EFFLQ
                TO (SF)-ALQ-TAX-SEP-EFFLQ
           END-IF
           IF DFL-ALQ-TAX-SEP-DFZ-NULL = HIGH-VALUES
              MOVE DFL-ALQ-TAX-SEP-DFZ-NULL
                TO (SF)-ALQ-TAX-SEP-DFZ-NULL
           ELSE
              MOVE DFL-ALQ-TAX-SEP-DFZ
                TO (SF)-ALQ-TAX-SEP-DFZ
           END-IF
           IF DFL-ALQ-CNBT-INPSTFM-C-NULL = HIGH-VALUES
              MOVE DFL-ALQ-CNBT-INPSTFM-C-NULL
                TO (SF)-ALQ-CNBT-INPSTFM-C-NULL
           ELSE
              MOVE DFL-ALQ-CNBT-INPSTFM-C
                TO (SF)-ALQ-CNBT-INPSTFM-C
           END-IF
           IF DFL-ALQ-CNBT-INPSTFM-E-NULL = HIGH-VALUES
              MOVE DFL-ALQ-CNBT-INPSTFM-E-NULL
                TO (SF)-ALQ-CNBT-INPSTFM-E-NULL
           ELSE
              MOVE DFL-ALQ-CNBT-INPSTFM-E
                TO (SF)-ALQ-CNBT-INPSTFM-E
           END-IF
           IF DFL-ALQ-CNBT-INPSTFM-D-NULL = HIGH-VALUES
              MOVE DFL-ALQ-CNBT-INPSTFM-D-NULL
                TO (SF)-ALQ-CNBT-INPSTFM-D-NULL
           ELSE
              MOVE DFL-ALQ-CNBT-INPSTFM-D
                TO (SF)-ALQ-CNBT-INPSTFM-D
           END-IF
           MOVE DFL-DS-RIGA
             TO (SF)-DS-RIGA
           MOVE DFL-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE DFL-DS-VER
             TO (SF)-DS-VER
           MOVE DFL-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ
           MOVE DFL-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ
           MOVE DFL-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE DFL-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           IF DFL-IMPB-VIS-1382011C-NULL = HIGH-VALUES
              MOVE DFL-IMPB-VIS-1382011C-NULL
                TO (SF)-IMPB-VIS-1382011C-NULL
           ELSE
              MOVE DFL-IMPB-VIS-1382011C
                TO (SF)-IMPB-VIS-1382011C
           END-IF
           IF DFL-IMPB-VIS-1382011D-NULL = HIGH-VALUES
              MOVE DFL-IMPB-VIS-1382011D-NULL
                TO (SF)-IMPB-VIS-1382011D-NULL
           ELSE
              MOVE DFL-IMPB-VIS-1382011D
                TO (SF)-IMPB-VIS-1382011D
           END-IF
           IF DFL-IMPB-VIS-1382011L-NULL = HIGH-VALUES
              MOVE DFL-IMPB-VIS-1382011L-NULL
                TO (SF)-IMPB-VIS-1382011L-NULL
           ELSE
              MOVE DFL-IMPB-VIS-1382011L
                TO (SF)-IMPB-VIS-1382011L
           END-IF
           IF DFL-IMPST-VIS-1382011C-NULL = HIGH-VALUES
              MOVE DFL-IMPST-VIS-1382011C-NULL
                TO (SF)-IMPST-VIS-1382011C-NULL
           ELSE
              MOVE DFL-IMPST-VIS-1382011C
                TO (SF)-IMPST-VIS-1382011C
           END-IF
           IF DFL-IMPST-VIS-1382011D-NULL = HIGH-VALUES
              MOVE DFL-IMPST-VIS-1382011D-NULL
                TO (SF)-IMPST-VIS-1382011D-NULL
           ELSE
              MOVE DFL-IMPST-VIS-1382011D
                TO (SF)-IMPST-VIS-1382011D
           END-IF
           IF DFL-IMPST-VIS-1382011L-NULL = HIGH-VALUES
              MOVE DFL-IMPST-VIS-1382011L-NULL
                TO (SF)-IMPST-VIS-1382011L-NULL
           ELSE
              MOVE DFL-IMPST-VIS-1382011L
                TO (SF)-IMPST-VIS-1382011L
           END-IF
           IF DFL-IMPB-IS-1382011C-NULL = HIGH-VALUES
              MOVE DFL-IMPB-IS-1382011C-NULL
                TO (SF)-IMPB-IS-1382011C-NULL
           ELSE
              MOVE DFL-IMPB-IS-1382011C
                TO (SF)-IMPB-IS-1382011C
           END-IF
           IF DFL-IMPB-IS-1382011D-NULL = HIGH-VALUES
              MOVE DFL-IMPB-IS-1382011D-NULL
                TO (SF)-IMPB-IS-1382011D-NULL
           ELSE
              MOVE DFL-IMPB-IS-1382011D
                TO (SF)-IMPB-IS-1382011D
           END-IF
           IF DFL-IMPB-IS-1382011L-NULL = HIGH-VALUES
              MOVE DFL-IMPB-IS-1382011L-NULL
                TO (SF)-IMPB-IS-1382011L-NULL
           ELSE
              MOVE DFL-IMPB-IS-1382011L
                TO (SF)-IMPB-IS-1382011L
           END-IF
           IF DFL-IS-1382011C-NULL = HIGH-VALUES
              MOVE DFL-IS-1382011C-NULL
                TO (SF)-IS-1382011C-NULL
           ELSE
              MOVE DFL-IS-1382011C
                TO (SF)-IS-1382011C
           END-IF
           IF DFL-IS-1382011D-NULL = HIGH-VALUES
              MOVE DFL-IS-1382011D-NULL
                TO (SF)-IS-1382011D-NULL
           ELSE
              MOVE DFL-IS-1382011D
                TO (SF)-IS-1382011D
           END-IF
           IF DFL-IS-1382011L-NULL = HIGH-VALUES
              MOVE DFL-IS-1382011L-NULL
                TO (SF)-IS-1382011L-NULL
           ELSE
              MOVE DFL-IS-1382011L
                TO (SF)-IS-1382011L
           END-IF
           IF DFL-IMP-INTR-RIT-PAG-C-NULL = HIGH-VALUES
              MOVE DFL-IMP-INTR-RIT-PAG-C-NULL
                TO (SF)-IMP-INTR-RIT-PAG-C-NULL
           ELSE
              MOVE DFL-IMP-INTR-RIT-PAG-C
                TO (SF)-IMP-INTR-RIT-PAG-C
           END-IF
           IF DFL-IMP-INTR-RIT-PAG-D-NULL = HIGH-VALUES
              MOVE DFL-IMP-INTR-RIT-PAG-D-NULL
                TO (SF)-IMP-INTR-RIT-PAG-D-NULL
           ELSE
              MOVE DFL-IMP-INTR-RIT-PAG-D
                TO (SF)-IMP-INTR-RIT-PAG-D
           END-IF
           IF DFL-IMP-INTR-RIT-PAG-L-NULL = HIGH-VALUES
              MOVE DFL-IMP-INTR-RIT-PAG-L-NULL
                TO (SF)-IMP-INTR-RIT-PAG-L-NULL
           ELSE
              MOVE DFL-IMP-INTR-RIT-PAG-L
                TO (SF)-IMP-INTR-RIT-PAG-L
           END-IF
           IF DFL-IMPB-BOLLO-DETT-C-NULL = HIGH-VALUES
              MOVE DFL-IMPB-BOLLO-DETT-C-NULL
                TO (SF)-IMPB-BOLLO-DETT-C-NULL
           ELSE
              MOVE DFL-IMPB-BOLLO-DETT-C
                TO (SF)-IMPB-BOLLO-DETT-C
           END-IF
           IF DFL-IMPB-BOLLO-DETT-D-NULL = HIGH-VALUES
              MOVE DFL-IMPB-BOLLO-DETT-D-NULL
                TO (SF)-IMPB-BOLLO-DETT-D-NULL
           ELSE
              MOVE DFL-IMPB-BOLLO-DETT-D
                TO (SF)-IMPB-BOLLO-DETT-D
           END-IF
           IF DFL-IMPB-BOLLO-DETT-L-NULL = HIGH-VALUES
              MOVE DFL-IMPB-BOLLO-DETT-L-NULL
                TO (SF)-IMPB-BOLLO-DETT-L-NULL
           ELSE
              MOVE DFL-IMPB-BOLLO-DETT-L
                TO (SF)-IMPB-BOLLO-DETT-L
           END-IF
           IF DFL-IMPST-BOLLO-DETT-C-NULL = HIGH-VALUES
              MOVE DFL-IMPST-BOLLO-DETT-C-NULL
                TO (SF)-IMPST-BOLLO-DETT-C-NULL
           ELSE
              MOVE DFL-IMPST-BOLLO-DETT-C
                TO (SF)-IMPST-BOLLO-DETT-C
           END-IF
           IF DFL-IMPST-BOLLO-DETT-D-NULL = HIGH-VALUES
              MOVE DFL-IMPST-BOLLO-DETT-D-NULL
                TO (SF)-IMPST-BOLLO-DETT-D-NULL
           ELSE
              MOVE DFL-IMPST-BOLLO-DETT-D
                TO (SF)-IMPST-BOLLO-DETT-D
           END-IF
           IF DFL-IMPST-BOLLO-DETT-L-NULL = HIGH-VALUES
              MOVE DFL-IMPST-BOLLO-DETT-L-NULL
                TO (SF)-IMPST-BOLLO-DETT-L-NULL
           ELSE
              MOVE DFL-IMPST-BOLLO-DETT-L
                TO (SF)-IMPST-BOLLO-DETT-L
           END-IF
           IF DFL-IMPST-BOLLO-TOT-VC-NULL = HIGH-VALUES
              MOVE DFL-IMPST-BOLLO-TOT-VC-NULL
                TO (SF)-IMPST-BOLLO-TOT-VC-NULL
           ELSE
              MOVE DFL-IMPST-BOLLO-TOT-VC
                TO (SF)-IMPST-BOLLO-TOT-VC
           END-IF
           IF DFL-IMPST-BOLLO-TOT-VD-NULL = HIGH-VALUES
              MOVE DFL-IMPST-BOLLO-TOT-VD-NULL
                TO (SF)-IMPST-BOLLO-TOT-VD-NULL
           ELSE
              MOVE DFL-IMPST-BOLLO-TOT-VD
                TO (SF)-IMPST-BOLLO-TOT-VD
           END-IF
           IF DFL-IMPST-BOLLO-TOT-VL-NULL = HIGH-VALUES
              MOVE DFL-IMPST-BOLLO-TOT-VL-NULL
                TO (SF)-IMPST-BOLLO-TOT-VL-NULL
           ELSE
              MOVE DFL-IMPST-BOLLO-TOT-VL
                TO (SF)-IMPST-BOLLO-TOT-VL
           END-IF
           IF DFL-IMPB-VIS-662014C-NULL = HIGH-VALUES
              MOVE DFL-IMPB-VIS-662014C-NULL
                TO (SF)-IMPB-VIS-662014C-NULL
           ELSE
              MOVE DFL-IMPB-VIS-662014C
                TO (SF)-IMPB-VIS-662014C
           END-IF
           IF DFL-IMPB-VIS-662014D-NULL = HIGH-VALUES
              MOVE DFL-IMPB-VIS-662014D-NULL
                TO (SF)-IMPB-VIS-662014D-NULL
           ELSE
              MOVE DFL-IMPB-VIS-662014D
                TO (SF)-IMPB-VIS-662014D
           END-IF
           IF DFL-IMPB-VIS-662014L-NULL = HIGH-VALUES
              MOVE DFL-IMPB-VIS-662014L-NULL
                TO (SF)-IMPB-VIS-662014L-NULL
           ELSE
              MOVE DFL-IMPB-VIS-662014L
                TO (SF)-IMPB-VIS-662014L
           END-IF
           IF DFL-IMPST-VIS-662014C-NULL = HIGH-VALUES
              MOVE DFL-IMPST-VIS-662014C-NULL
                TO (SF)-IMPST-VIS-662014C-NULL
           ELSE
              MOVE DFL-IMPST-VIS-662014C
                TO (SF)-IMPST-VIS-662014C
           END-IF
           IF DFL-IMPST-VIS-662014D-NULL = HIGH-VALUES
              MOVE DFL-IMPST-VIS-662014D-NULL
                TO (SF)-IMPST-VIS-662014D-NULL
           ELSE
              MOVE DFL-IMPST-VIS-662014D
                TO (SF)-IMPST-VIS-662014D
           END-IF
           IF DFL-IMPST-VIS-662014L-NULL = HIGH-VALUES
              MOVE DFL-IMPST-VIS-662014L-NULL
                TO (SF)-IMPST-VIS-662014L-NULL
           ELSE
              MOVE DFL-IMPST-VIS-662014L
                TO (SF)-IMPST-VIS-662014L
           END-IF
           IF DFL-IMPB-IS-662014C-NULL = HIGH-VALUES
              MOVE DFL-IMPB-IS-662014C-NULL
                TO (SF)-IMPB-IS-662014C-NULL
           ELSE
              MOVE DFL-IMPB-IS-662014C
                TO (SF)-IMPB-IS-662014C
           END-IF
           IF DFL-IMPB-IS-662014D-NULL = HIGH-VALUES
              MOVE DFL-IMPB-IS-662014D-NULL
                TO (SF)-IMPB-IS-662014D-NULL
           ELSE
              MOVE DFL-IMPB-IS-662014D
                TO (SF)-IMPB-IS-662014D
           END-IF
           IF DFL-IMPB-IS-662014L-NULL = HIGH-VALUES
              MOVE DFL-IMPB-IS-662014L-NULL
                TO (SF)-IMPB-IS-662014L-NULL
           ELSE
              MOVE DFL-IMPB-IS-662014L
                TO (SF)-IMPB-IS-662014L
           END-IF
           IF DFL-IS-662014C-NULL = HIGH-VALUES
              MOVE DFL-IS-662014C-NULL
                TO (SF)-IS-662014C-NULL
           ELSE
              MOVE DFL-IS-662014C
                TO (SF)-IS-662014C
           END-IF
           IF DFL-IS-662014D-NULL = HIGH-VALUES
              MOVE DFL-IS-662014D-NULL
                TO (SF)-IS-662014D-NULL
           ELSE
              MOVE DFL-IS-662014D
                TO (SF)-IS-662014D
           END-IF
           IF DFL-IS-662014L-NULL = HIGH-VALUES
              MOVE DFL-IS-662014L-NULL
                TO (SF)-IS-662014L-NULL
           ELSE
              MOVE DFL-IS-662014L
                TO (SF)-IS-662014L
           END-IF.
       VALORIZZA-OUTPUT-DFL-EX.
           EXIT.

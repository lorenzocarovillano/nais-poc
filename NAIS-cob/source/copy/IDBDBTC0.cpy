           EXEC SQL DECLARE BTC_BATCH TABLE
           (
             ID_BATCH            INTEGER NOT NULL,
             PROTOCOL            CHAR(50) NOT NULL,
             DT_INS              TIMESTAMP NOT NULL,
             USER_INS            CHAR(30) NOT NULL,
             COD_BATCH_TYPE      INTEGER NOT NULL,
             COD_COMP_ANIA       INTEGER NOT NULL,
             COD_BATCH_STATE     CHAR(1) NOT NULL,
             DT_START            TIMESTAMP,
             DT_END              TIMESTAMP,
             USER_START          CHAR(30),
             ID_RICH             INTEGER,
             DT_EFF              DATE,
             DATA_BATCH          VARCHAR(32000)
          ) END-EXEC.

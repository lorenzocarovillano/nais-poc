      *----------------------------------------------------------------*
      *    COPY      ..... LCCVPRE6
      *    TIPOLOGIA...... SERVIZIO DI EOC
      *    DESCRIZIONE.... AGGIORNAMENTO PRESTITI
      *----------------------------------------------------------------*
      *
      *    N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
      *    ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
      *
      *  - COPY LCCVGRZ5 (VALORIZZAZIONE DCLGEN)
      *  - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
      *  - DICHIARAZIONE DCLGEN GARANZIA (LCCVGRZ1)
      *  - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
      *
      *----------------------------------------------------------------*

       AGGIORNA-PREST.

      *--> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI
           INITIALIZE PREST

      *--> NOME TABELLA FISICA DB
           MOVE 'PREST'                   TO   WK-TABELLA

      *--> SE LO STATUS NON E' "INVARIATO"
           IF NOT WPRE-ST-INV(IX-TAB-PRE)
              AND WPRE-ELE-PRESTITI-MAX NOT = 0

      *--->   Impostare ID Tabella PRESTITI
              MOVE WMOV-ID-PTF              TO PRE-ID-MOVI-CRZ
      *-->    CONTROLLO DELLO STATUS
              EVALUATE TRUE

      *-->        INSERIMENTO
                  WHEN WPRE-ST-ADD(IX-TAB-PRE)

      *-->             ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                       PERFORM ESTR-SEQUENCE
                          THRU ESTR-SEQUENCE-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S090-SEQ-TABELLA
                            TO WPRE-ID-PTF(IX-TAB-PRE)
                               PRE-ID-PREST
                       END-IF

      *-->             PREPARAZIONE ED ESTRAZIONE OGGETTO PTF
                       PERFORM PREPARA-AREA-LCCS0234-PRE
                          THRU PREPARA-AREA-LCCS0234-PRE-EX

                       PERFORM CALL-LCCS0234
                          THRU CALL-LCCS0234-EX

                       IF IDSV0001-ESITO-OK
                          MOVE S234-ID-OGG-PTF-EOC
                            TO PRE-ID-OGG
                       END-IF

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        UPDATE
                  WHEN WPRE-ST-MOD(IX-TAB-PRE)

      *                MOVE WMOV-ID-MOVI  TO PRE-ID-MOVI-CRZ
                       MOVE WMOV-DT-EFF   TO WPRE-DT-INI-EFF(IX-TAB-PRE)
                       MOVE WPRE-ID-PTF(IX-TAB-PRE)   TO PRE-ID-PREST
                       MOVE WPRE-ID-OGG(IX-TAB-PRE)   TO PRE-ID-OGG

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE

      *-->        DELETE
                  WHEN WPRE-ST-DEL(IX-TAB-PRE)

                     MOVE WMOV-ID-PTF   TO WPRE-ID-MOVI-CHIU(IX-TAB-PRE)
                                           PRE-ID-MOVI-CRZ
                     MOVE WMOV-DT-EFF   TO WPRE-DT-END-EFF(IX-TAB-PRE)
                     MOVE WPRE-ID-PTF(IX-TAB-PRE)   TO PRE-ID-PREST
                     MOVE WPRE-ID-OGG(IX-TAB-PRE)   TO PRE-ID-OGG

      *-->             TIPO OPERAZIONE DISPATCHER
                       SET  IDSI0011-DELETE-LOGICA    TO TRUE

              END-EVALUATE

              IF IDSV0001-ESITO-OK

      *-->       VALORIZZA DCLGEN
                 PERFORM VAL-DCLGEN-PRE
                    THRU VAL-DCLGEN-PRE-EX

      *-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                 PERFORM VALORIZZA-AREA-DSH-PRE
                    THRU VALORIZZA-AREA-DSH-PRE-EX

      *-->       CALL DISPATCHER PER AGGIORNAMENTO
                 PERFORM AGGIORNA-TABELLA
                    THRU AGGIORNA-TABELLA-EX

              END-IF

           END-IF.

       AGGIORNA-PREST-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    VALORIZZA AREA COMUNE DISPATCHER
      *----------------------------------------------------------------*
       VALORIZZA-AREA-DSH-PRE.

      *--> DCLGEN TABELLA
           MOVE PREST                   TO IDSI0011-BUFFER-DATI.

      *--> MODALITA DI ACCESSO
           SET  IDSI0011-ID             TO TRUE.

      *--> TIPO TABELLA (STORICA/NON STORICA)
           SET  IDSI0011-TRATT-DEFAULT           TO TRUE.

      *--> VALORIZZAZIONE DATE EFFETTO
           IF WPRE-ST-MOD(IX-TAB-PRE)
              MOVE WMOV-DT-EFF        TO IDSI0011-DATA-INIZIO-EFFETTO
           ELSE
              MOVE ZERO               TO IDSI0011-DATA-INIZIO-EFFETTO
           END-IF.
           MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO
           MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.

       VALORIZZA-AREA-DSH-PRE-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    PREPARA AREA PER CALL LCCS0234
      *----------------------------------------------------------------*
       PREPARA-AREA-LCCS0234-PRE.

           MOVE WPRE-ID-OGG(IX-TAB-PRE)       TO S234-ID-OGG-EOC.
           MOVE WPRE-TP-OGG(IX-TAB-PRE)       TO S234-TIPO-OGG-EOC.


       PREPARA-AREA-LCCS0234-PRE-EX.
           EXIT.

      *----------------------------------------------------------------*
      *    COPY VALORIZZAZIONE DCLGEN
      *----------------------------------------------------------------*
      *     COPY LCCVPRE5.

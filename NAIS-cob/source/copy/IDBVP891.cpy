       01 D-ATT-SERV-VAL.
         05 P89-ID-D-ATT-SERV-VAL PIC S9(9)V     COMP-3.
         05 P89-COD-COMP-ANIA PIC S9(5)V     COMP-3.
         05 P89-ID-MOVI-CRZ PIC S9(9)V     COMP-3.
         05 P89-ID-MOVI-CHIU PIC S9(9)V     COMP-3.
         05 P89-ID-MOVI-CHIU-NULL REDEFINES
            P89-ID-MOVI-CHIU   PIC X(5).
         05 P89-DT-INI-EFF   PIC S9(8)V COMP-3.
         05 P89-DT-END-EFF   PIC S9(8)V COMP-3.
         05 P89-ID-ATT-SERV-VAL PIC S9(9)V     COMP-3.
         05 P89-TP-SERV-VAL PIC X(2).
         05 P89-COD-FND PIC X(12).
         05 P89-DT-INI-CNTRL-FND   PIC S9(8)V COMP-3.
         05 P89-DT-INI-CNTRL-FND-NULL REDEFINES
            P89-DT-INI-CNTRL-FND   PIC X(5).
         05 P89-ID-OGG PIC S9(9)V     COMP-3.
         05 P89-TP-OGG PIC X(2).
         05 P89-DS-RIGA PIC S9(10)V     COMP-3.
         05 P89-DS-OPER-SQL PIC X(1).
         05 P89-DS-VER PIC S9(9)V     COMP-3.
         05 P89-DS-TS-INI-CPTZ PIC S9(18)V     COMP-3.
         05 P89-DS-TS-END-CPTZ PIC S9(18)V     COMP-3.
         05 P89-DS-UTENTE PIC X(20).
         05 P89-DS-STATO-ELAB PIC X(1).


      *-----------------------------------------------------------------
      *   VALORIZZAZIONI TABELLA BILA-FND-ESTR
      *-----------------------------------------------------------------
       S3050-VAL-BILA-FND-ESTR.

           INITIALIZE BILA-FND-ESTR.

      *-->    VALORIZZAZIONE DEI CAMPI B01-ID-BILA-FND-ESTR E
      *-->    B01-ID-BILA-TRCH-ESTR INIBITE X MOTIVI DI PERFORMANCE
      *     MOVE S090-SEQ-TABELLA
      *       TO B01-ID-BILA-FND-ESTR.

      *     MOVE WB03-ID-BILA-TRCH-ESTR
      *       TO B01-ID-BILA-TRCH-ESTR.

           MOVE WPOL-COD-COMP-ANIA
             TO B01-COD-COMP-ANIA.

           IF WLB-TP-RICH = 'B1'
              MOVE WLB-ID-RICH
                TO B01-ID-RICH-ESTRAZ-MAS
              MOVE HIGH-VALUES
                TO B01-ID-RICH-ESTRAZ-AGG-NULL
           END-IF.

           IF WLB-TP-RICH = 'B2'
              MOVE WLB-ID-RICH-COLL
                TO B01-ID-RICH-ESTRAZ-MAS
              MOVE WLB-ID-RICH
                TO B01-ID-RICH-ESTRAZ-AGG
           END-IF.

           MOVE WLB-DT-RISERVA
             TO B01-DT-RIS.

           MOVE WPOL-ID-POLI
             TO B01-ID-POLI.

           MOVE WADE-ID-ADES(IX-TAB-ADE)
             TO B01-ID-ADES.

           MOVE WTGA-ID-GAR(IX-TAB-TGA)
             TO B01-ID-GAR.

           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
             TO B01-ID-TRCH-DI-GAR.

           MOVE LCCC0450-COD-FONDO(IX-TAB-450)
             TO B01-COD-FND
                WS-COD-FND.

           IF LCCC0450-DT-VAL-QUOTE(IX-TAB-450) = 0
              MOVE HIGH-VALUE
                TO B01-DT-QTZ-INI-NULL
           ELSE
              MOVE LCCC0450-DT-VAL-QUOTE(IX-TAB-450)
                TO B01-DT-QTZ-INI
           END-IF.

           MOVE LCCC0450-NUM-QUOTE-INI(IX-TAB-450)
             TO B01-NUM-QUO-INI.

           MOVE LCCC0450-VAL-QUOTA-INI(IX-TAB-450)
             TO B01-VAL-QUO-INI.

           MOVE WS-DT-CALCOLO-NUM
             TO B01-DT-VALZZ-QUO-DT-CA.

      *--> ESTRARRE LA QUOTAZIONE AL DATA DI RISERVA
           SET WS-QTZ-CALC-RIS
            TO TRUE.

           MOVE WS-DT-CALCOLO-NUM
             TO WS-DT-QUOT.

           MOVE LCCC0450-COD-FONDO(IX-TAB-450)
             TO WS-COD-FND.

           PERFORM S0202-PREP-LET-QUOT-AGG-FND
              THRU EX-S0202.

           PERFORM S0203-LETTURA-QUOT-AGG-FND
              THRU EX-S0203.

           IF IDSV0001-ESITO-OK
              IF L41-VAL-QUO > ZERO
                 MOVE L41-VAL-QUO
                   TO B01-VAL-QUO-DT-CALC
33117            IF B01-DT-QTZ-INI-NULL = HIGH-VALUE
33117               MOVE L41-DT-QTZ
33117                 TO B01-DT-QTZ-INI
33117            END-IF

                 IF B01-NUM-QUO-INI = ZERO
                    IF LCCC0450-IMP-INVES(IX-TAB-450) = 0
                       IF LCCC0450-IMP-DISINV(IX-TAB-450) NOT = 0
                          COMPUTE B01-NUM-QUO-INI ROUNDED     =
                                  LCCC0450-IMP-DISINV(IX-TAB-450) /
                                  B01-VAL-QUO-DT-CALC
                       END-IF
                    ELSE
                       COMPUTE B01-NUM-QUO-INI ROUNDED        =
                               LCCC0450-IMP-INVES(IX-TAB-450) /
                               B01-VAL-QUO-DT-CALC
                    END-IF
                 END-IF
              END-IF

              MOVE LCCC0450-NUM-QUOTE(IX-TAB-450)
                TO B01-NUM-QUO-DT-CALC

      *--> ESTRARRE LA QUOTAZIONE AL 31/12
              SET WS-QTZ-CALC-RIS
               TO TRUE

      *--> 31/12 DELL'ANNO PRECEDENTE ALLA DATA DI RISERVA
              MOVE WLB-DT-RISERVA
                TO WK-DATA-APPO-N
              MOVE '31'
                TO WK-DATA-APPO-N-GG
              MOVE '12'
                TO WK-DATA-APPO-N-MM
              MOVE WK-DATA-APPO-N-AA
                TO WK-DATA-APPO-9-AA
              SUBTRACT 1
                  FROM WK-DATA-APPO-9-AA
              MOVE WK-DATA-APPO-9-AA
                TO WK-DATA-APPO-N-AA

      *--> SE QUESTA DATA E' INFERIORE ALLA DECORRENZA DI TRANCHE
              IF WK-DATA-APPO < WB03-DT-DECOR-TRCH
                 MOVE WB03-DT-DECOR-TRCH
                   TO WK-DATA-APPO
              END-IF

              MOVE WK-DATA-APPO
                TO WS-DT-QUOT

              MOVE LCCC0450-COD-FONDO(IX-TAB-450)
                TO WS-COD-FND

              PERFORM S0202-PREP-LET-QUOT-AGG-FND
                 THRU EX-S0202

              PERFORM S0203-LETTURA-QUOT-AGG-FND
                 THRU EX-S0203
           END-IF.

           IF IDSV0001-ESITO-OK
4371          MOVE WB03-DT-DECOR-TRCH TO WK-DT-DECOR-TRCH
4371          IF WLB-DT-RISERVA(1:4) NOT = WK-DT-DECOR-TRCH(1:4)
                 IF L41-VAL-QUO > ZERO
                    MOVE L41-VAL-QUO
                      TO B01-VAL-QUO-T
                 ELSE
      *-->        ESTRAZIONE DELLA QUOTAZIONE DALLA L19

                    PERFORM S0204-PREP-LET-QUOT-FND-UNIT
                       THRU EX-S0204

                    PERFORM S0205-LETTURA-QUOT-FND-UNIT
                       THRU EX-S0205
                    IF IDSV0001-ESITO-OK
                       IF L19-VAL-QUO-NULL NOT = HIGH-VALUES
                          AND L19-VAL-QUO > ZERO
                          MOVE L19-VAL-QUO
                            TO B01-VAL-QUO-T
                       END-IF
                    END-IF
                 END-IF
              ELSE
      *-->        ESTRAZIONE DELLA QUOTAZIONE DALLA L19
      *-->        ALLA DECORRENZA DI TRANCHE

                 PERFORM S0204-PREP-LET-QUOT-FND-UNIT
                    THRU EX-S0204

                 PERFORM S0205-LETTURA-QUOT-FND-UNIT
                    THRU EX-S0205
                 IF IDSV0001-ESITO-OK
                    IF L19-VAL-QUO-NULL NOT = HIGH-VALUES
                       AND L19-VAL-QUO > ZERO
                       MOVE L19-VAL-QUO
                         TO B01-VAL-QUO-T
4371                ELSE
4371                   MOVE B01-VAL-QUO-DT-CALC
4371                     TO B01-VAL-QUO-T
4371                END-IF
                 END-IF
              END-IF

4371  *       MOVE WB03-DT-DECOR-TRCH TO WK-DT-DECOR-TRCH
4371  *       IF WLB-DT-RISERVA(1:4) = WK-DT-DECOR-TRCH(1:4)
4371  *          IF L19-VAL-QUO-NULL NOT = HIGH-VALUES
4371  *             AND L19-VAL-QUO > ZERO
4371  *             MOVE L19-VAL-QUO         TO B01-VAL-QUO-T
4371  *          ELSE
4371  *             MOVE B01-VAL-QUO-DT-CALC TO B01-VAL-QUO-T
4371  *          END-IF
4371  *          MOVE B01-VAL-QUO-INI TO B01-VAL-QUO-T
4371  *       END-IF

              MOVE ZERO
                TO WK-PERCENTUALE

              IF LCCC0450-CONTROVALORE(IX-TAB-450) = 0
              OR LCCC0450-TOT-CONTRATTO            = 0

                 IF LCCC0450-IMP-INVES(IX-TAB-450) = 0
                 OR LCCC0450-TOT-IMP-INVES         = 0

                    IF LCCC0450-IMP-DISINV(IX-TAB-450) NOT = 0
                    OR LCCC0450-TOT-IMP-DISINV         NOT = 0
                       COMPUTE WK-PERCENTUALE                  =
                              (LCCC0450-IMP-DISINV(IX-TAB-450) /
                               LCCC0450-TOT-IMP-DISINV)        * 100
                    END-IF
                 ELSE
                    COMPUTE WK-PERCENTUALE                 =
                           (LCCC0450-IMP-INVES(IX-TAB-450) /
                            LCCC0450-TOT-IMP-INVES)        * 100
                 END-IF
              ELSE
                 COMPUTE WK-PERCENTUALE                    =
                        (LCCC0450-CONTROVALORE(IX-TAB-450) /
                         LCCC0450-TOT-CONTRATTO)           * 100
              END-IF

              MOVE WK-PERCENTUALE
                TO B01-PC-INVST
           END-IF.

       S3050-EX.
           EXIT.




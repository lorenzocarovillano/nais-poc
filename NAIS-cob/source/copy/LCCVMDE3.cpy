
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVMDE3
      *   ULTIMO AGG. 04 SET 2008
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-MDE.
           MOVE MDE-ID-MOT-DEROGA
             TO (SF)-ID-PTF(IX-TAB-MDE)
           MOVE MDE-ID-MOT-DEROGA
             TO (SF)-ID-MOT-DEROGA(IX-TAB-MDE)
           MOVE MDE-ID-OGG-DEROGA
             TO (SF)-ID-OGG-DEROGA(IX-TAB-MDE)
           MOVE MDE-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-MDE)
           IF MDE-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE MDE-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-MDE)
           ELSE
              MOVE MDE-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-MDE)
           END-IF
           MOVE MDE-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-MDE)
           MOVE MDE-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-MDE)
           MOVE MDE-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-MDE)
           MOVE MDE-TP-MOT-DEROGA
             TO (SF)-TP-MOT-DEROGA(IX-TAB-MDE)
           MOVE MDE-COD-LIV-AUT
             TO (SF)-COD-LIV-AUT(IX-TAB-MDE)
           MOVE MDE-COD-ERR
             TO (SF)-COD-ERR(IX-TAB-MDE)
           MOVE MDE-TP-ERR
             TO (SF)-TP-ERR(IX-TAB-MDE)
           MOVE MDE-DESC-ERR-BREVE
             TO (SF)-DESC-ERR-BREVE(IX-TAB-MDE)
           MOVE MDE-DESC-ERR-EST
             TO (SF)-DESC-ERR-EST(IX-TAB-MDE)
           MOVE MDE-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-MDE)
           MOVE MDE-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-MDE)
           MOVE MDE-DS-VER
             TO (SF)-DS-VER(IX-TAB-MDE)
           MOVE MDE-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-MDE)
           MOVE MDE-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-MDE)
           MOVE MDE-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-MDE)
           MOVE MDE-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-MDE).
       VALORIZZA-OUTPUT-MDE-EX.
           EXIT.

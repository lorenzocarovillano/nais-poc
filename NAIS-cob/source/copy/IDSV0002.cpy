       01  CALL-PGM                  PIC  X(00008) VALUE SPACES.
       01  CALL-DESC                 PIC  X(00100) VALUE SPACES.
       01  CALL-AREA                 PIC  X(32000) VALUE SPACES.
       01  CALL-LUNG-AREA            PIC S9(00008) COMP VALUE 0.
       01  FILLER              REDEFINES CALL-LUNG-AREA.
           05  FILLER                PIC  X(00002).
           05  CALL-LUNG-AREA-L      PIC S9(00004) COMP.
       01  CALL-RESP                 PIC S9(00008) COMP VALUE 0.

      *----------------------------------------------------------------*
      *    PUNTATORI
      *----------------------------------------------------------------*
       01 DISPATCHER-ADDRESS.
           05  WS-ADDRESS-DIS USAGE POINTER.

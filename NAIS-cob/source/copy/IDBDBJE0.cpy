           EXEC SQL DECLARE BTC_JOB_EXECUTION TABLE
           (
             ID_BATCH            INTEGER NOT NULL,
             ID_JOB              INTEGER NOT NULL,
             ID_EXECUTION        INTEGER NOT NULL,
             COD_ELAB_STATE      CHAR(1) NOT NULL,
             DATA                VARCHAR(32000),
             FLAG_WARNINGS       CHAR(1),
             DT_START            TIMESTAMP NOT NULL,
             DT_END              TIMESTAMP,
             USER_START          CHAR(30) NOT NULL
          ) END-EXEC.

           EXEC SQL DECLARE SOPR_DI_GAR TABLE
           (
             ID_SOPR_DI_GAR      DECIMAL(9, 0) NOT NULL,
             ID_GAR              DECIMAL(9, 0),
             ID_MOVI_CRZ         DECIMAL(9, 0) NOT NULL,
             ID_MOVI_CHIU        DECIMAL(9, 0),
             DT_INI_EFF          DATE NOT NULL,
             DT_END_EFF          DATE NOT NULL,
             COD_COMP_ANIA       DECIMAL(5, 0) NOT NULL,
             COD_SOPR            CHAR(12),
             TP_D                CHAR(2),
             VAL_PC              DECIMAL(14, 9),
             VAL_IMP             DECIMAL(15, 3),
             PC_SOPRAM           DECIMAL(14, 9),
             FL_ESCL_SOPR        CHAR(1),
             DESC_ESCL           VARCHAR(100),
             DS_RIGA             DECIMAL(10, 0) NOT NULL WITH DEFAULT,
             DS_OPER_SQL         CHAR(1) NOT NULL WITH DEFAULT,
             DS_VER              DECIMAL(9, 0) NOT NULL WITH DEFAULT,
             DS_TS_INI_CPTZ      DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_TS_END_CPTZ      DECIMAL(18, 0) NOT NULL WITH DEFAULT,
             DS_UTENTE           CHAR(20) NOT NULL WITH DEFAULT,
             DS_STATO_ELAB       CHAR(1) NOT NULL WITH DEFAULT
          ) END-EXEC.

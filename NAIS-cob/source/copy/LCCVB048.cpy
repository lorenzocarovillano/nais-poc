         03 (SF)-LEN-TOT                       PIC S9(4) COMP-5.
         03 (SF)-REC-FISSO.
           05 (SF)-ID-BILA-VAR-CALC-P     PIC S9(9)V      COMP-3.
           05 (SF)-COD-COMP-ANIA          PIC S9(5)V      COMP-3.
           05 (SF)-ID-RICH-ESTRAZ-MAS     PIC S9(9)V      COMP-3.
           05 (SF)-ID-RICH-ESTRAZ-AGG-IND PIC X.
           05 (SF)-ID-RICH-ESTRAZ-AGG     PIC S9(9)V      COMP-3.
           05 (SF)-ID-RICH-ESTRAZ-AGG-NULL REDEFINES
              (SF)-ID-RICH-ESTRAZ-AGG     PIC X(5).
           05 (SF)-DT-RIS                 PIC X(10).
           05 (SF)-ID-POLI                PIC S9(9)V      COMP-3.
           05 (SF)-ID-ADES                PIC S9(9)V      COMP-3.
           05 (SF)-PROG-SCHEDA-VALOR      PIC S9(9)V      COMP-3.
           05 (SF)-TP-RGM-FISC            PIC X(2).
           05 (SF)-DT-INI-VLDT-PROD       PIC X(10).
           05 (SF)-COD-VAR                PIC X(30).
           05 (SF)-TP-D                   PIC X(1).
           05 (SF)-VAL-IMP-IND            PIC X.
           05 (SF)-VAL-IMP                PIC S9(11)V9(7) COMP-3.
           05 (SF)-VAL-IMP-NULL REDEFINES
              (SF)-VAL-IMP                PIC X(10).
           05 (SF)-VAL-PC-IND             PIC X.
           05 (SF)-VAL-PC                 PIC S9(5)V9(9)  COMP-3.
           05 (SF)-VAL-PC-NULL REDEFINES
              (SF)-VAL-PC                 PIC X(8).
           05 (SF)-VAL-STRINGA-IND        PIC X.
           05 (SF)-VAL-STRINGA            PIC X(60).
           05 (SF)-VAL-STRINGA-NULL REDEFINES
              (SF)-VAL-STRINGA            PIC X(60).
           05 (SF)-DS-OPER-SQL            PIC X(1).
           05 (SF)-DS-VER                 PIC S9(9)V     COMP-3.
           05 (SF)-DS-TS-CPTZ             PIC S9(18)V    COMP-3.
           05 (SF)-DS-UTENTE              PIC X(20).
           05 (SF)-DS-STATO-ELAB          PIC X(1).
         03 (SF)-AREA-D-VALOR-VAR-IND   PIC X.
         03 (SF)-AREA-D-VALOR-VAR-VCHAR.
           49 (SF)-AREA-D-VALOR-VAR-LEN PIC S9(4) COMP-5.
           49 (SF)-AREA-D-VALOR-VAR     PIC X(4000).



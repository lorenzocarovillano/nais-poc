
      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVTCL4
      *   ULTIMO AGG. 02 LUG 2014
      *------------------------------------------------------------

       INIZIA-TOT-TCL.

           PERFORM INIZIA-ZEROES-TCL THRU INIZIA-ZEROES-TCL-EX

           PERFORM INIZIA-SPACES-TCL THRU INIZIA-SPACES-TCL-EX

           PERFORM INIZIA-NULL-TCL THRU INIZIA-NULL-TCL-EX.

       INIZIA-TOT-TCL-EX.
           EXIT.

       INIZIA-NULL-TCL.
           MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-DT-VLT-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMP-LRD-LIQTO-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMP-PREST-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMP-INTR-PREST-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMP-RAT-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMP-UTI-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMP-RIT-TFR-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMP-RIT-ACC-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMP-RIT-VIS-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPB-TFR-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPB-ACC-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPB-VIS-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMP-RIMB-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMP-CORTVO-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPB-IMPST-PRVR-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPST-PRVR-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPB-IMPST-252-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPST-252-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMP-IS-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMP-DIR-LIQ-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMP-NET-LIQTO-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMP-EFFLQ-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-COD-DVS-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-TP-MEZ-PAG-ACCR-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-ESTR-CNT-CORR-ACCR-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-TP-STAT-TIT-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPB-VIS-1382011-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPST-VIS-1382011-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPST-SOST-1382011-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMP-INTR-RIT-PAG-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPB-IS-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPB-IS-1382011-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPB-VIS-662014-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPST-VIS-662014-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPB-IS-662014-NULL(IX-TAB-TCL)
           MOVE HIGH-VALUES TO (SF)-IMPST-SOST-662014-NULL(IX-TAB-TCL).
       INIZIA-NULL-TCL-EX.
           EXIT.

       INIZIA-ZEROES-TCL.
           MOVE 0 TO (SF)-ID-TCONT-LIQ(IX-TAB-TCL)
           MOVE 0 TO (SF)-ID-PERC-LIQ(IX-TAB-TCL)
           MOVE 0 TO (SF)-ID-BNFICR-LIQ(IX-TAB-TCL)
           MOVE 0 TO (SF)-ID-MOVI-CRZ(IX-TAB-TCL)
           MOVE 0 TO (SF)-DT-INI-EFF(IX-TAB-TCL)
           MOVE 0 TO (SF)-DT-END-EFF(IX-TAB-TCL)
           MOVE 0 TO (SF)-COD-COMP-ANIA(IX-TAB-TCL)
           MOVE 0 TO (SF)-DS-RIGA(IX-TAB-TCL)
           MOVE 0 TO (SF)-DS-VER(IX-TAB-TCL)
           MOVE 0 TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TCL)
           MOVE 0 TO (SF)-DS-TS-END-CPTZ(IX-TAB-TCL).
       INIZIA-ZEROES-TCL-EX.
           EXIT.

       INIZIA-SPACES-TCL.
           MOVE SPACES TO (SF)-DS-OPER-SQL(IX-TAB-TCL)
           MOVE SPACES TO (SF)-DS-UTENTE(IX-TAB-TCL)
           MOVE SPACES TO (SF)-DS-STATO-ELAB(IX-TAB-TCL).
       INIZIA-SPACES-TCL-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVDEQ3
      *   ULTIMO AGG. 03 GIU 2019
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-DEQ.
           MOVE DEQ-ID-DETT-QUEST
             TO (SF)-ID-PTF(IX-TAB-DEQ)
           MOVE DEQ-ID-DETT-QUEST
             TO (SF)-ID-DETT-QUEST(IX-TAB-DEQ)
           MOVE DEQ-ID-QUEST
             TO (SF)-ID-QUEST(IX-TAB-DEQ)
           MOVE DEQ-ID-MOVI-CRZ
             TO (SF)-ID-MOVI-CRZ(IX-TAB-DEQ)
           IF DEQ-ID-MOVI-CHIU-NULL = HIGH-VALUES
              MOVE DEQ-ID-MOVI-CHIU-NULL
                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-DEQ)
           ELSE
              MOVE DEQ-ID-MOVI-CHIU
                TO (SF)-ID-MOVI-CHIU(IX-TAB-DEQ)
           END-IF
           MOVE DEQ-DT-INI-EFF
             TO (SF)-DT-INI-EFF(IX-TAB-DEQ)
           MOVE DEQ-DT-END-EFF
             TO (SF)-DT-END-EFF(IX-TAB-DEQ)
           MOVE DEQ-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA(IX-TAB-DEQ)
           IF DEQ-COD-QUEST-NULL = HIGH-VALUES
              MOVE DEQ-COD-QUEST-NULL
                TO (SF)-COD-QUEST-NULL(IX-TAB-DEQ)
           ELSE
              MOVE DEQ-COD-QUEST
                TO (SF)-COD-QUEST(IX-TAB-DEQ)
           END-IF
           IF DEQ-COD-DOM-NULL = HIGH-VALUES
              MOVE DEQ-COD-DOM-NULL
                TO (SF)-COD-DOM-NULL(IX-TAB-DEQ)
           ELSE
              MOVE DEQ-COD-DOM
                TO (SF)-COD-DOM(IX-TAB-DEQ)
           END-IF
           IF DEQ-COD-DOM-COLLG-NULL = HIGH-VALUES
              MOVE DEQ-COD-DOM-COLLG-NULL
                TO (SF)-COD-DOM-COLLG-NULL(IX-TAB-DEQ)
           ELSE
              MOVE DEQ-COD-DOM-COLLG
                TO (SF)-COD-DOM-COLLG(IX-TAB-DEQ)
           END-IF
           IF DEQ-RISP-NUM-NULL = HIGH-VALUES
              MOVE DEQ-RISP-NUM-NULL
                TO (SF)-RISP-NUM-NULL(IX-TAB-DEQ)
           ELSE
              MOVE DEQ-RISP-NUM
                TO (SF)-RISP-NUM(IX-TAB-DEQ)
           END-IF
           IF DEQ-RISP-FL-NULL = HIGH-VALUES
              MOVE DEQ-RISP-FL-NULL
                TO (SF)-RISP-FL-NULL(IX-TAB-DEQ)
           ELSE
              MOVE DEQ-RISP-FL
                TO (SF)-RISP-FL(IX-TAB-DEQ)
           END-IF
           MOVE DEQ-RISP-TXT
             TO (SF)-RISP-TXT(IX-TAB-DEQ)
           IF DEQ-RISP-TS-NULL = HIGH-VALUES
              MOVE DEQ-RISP-TS-NULL
                TO (SF)-RISP-TS-NULL(IX-TAB-DEQ)
           ELSE
              MOVE DEQ-RISP-TS
                TO (SF)-RISP-TS(IX-TAB-DEQ)
           END-IF
           IF DEQ-RISP-IMP-NULL = HIGH-VALUES
              MOVE DEQ-RISP-IMP-NULL
                TO (SF)-RISP-IMP-NULL(IX-TAB-DEQ)
           ELSE
              MOVE DEQ-RISP-IMP
                TO (SF)-RISP-IMP(IX-TAB-DEQ)
           END-IF
           IF DEQ-RISP-PC-NULL = HIGH-VALUES
              MOVE DEQ-RISP-PC-NULL
                TO (SF)-RISP-PC-NULL(IX-TAB-DEQ)
           ELSE
              MOVE DEQ-RISP-PC
                TO (SF)-RISP-PC(IX-TAB-DEQ)
           END-IF
           IF DEQ-RISP-DT-NULL = HIGH-VALUES
              MOVE DEQ-RISP-DT-NULL
                TO (SF)-RISP-DT-NULL(IX-TAB-DEQ)
           ELSE
              MOVE DEQ-RISP-DT
                TO (SF)-RISP-DT(IX-TAB-DEQ)
           END-IF
           IF DEQ-RISP-KEY-NULL = HIGH-VALUES
              MOVE DEQ-RISP-KEY-NULL
                TO (SF)-RISP-KEY-NULL(IX-TAB-DEQ)
           ELSE
              MOVE DEQ-RISP-KEY
                TO (SF)-RISP-KEY(IX-TAB-DEQ)
           END-IF
           IF DEQ-TP-RISP-NULL = HIGH-VALUES
              MOVE DEQ-TP-RISP-NULL
                TO (SF)-TP-RISP-NULL(IX-TAB-DEQ)
           ELSE
              MOVE DEQ-TP-RISP
                TO (SF)-TP-RISP(IX-TAB-DEQ)
           END-IF
           IF DEQ-ID-COMP-QUEST-NULL = HIGH-VALUES
              MOVE DEQ-ID-COMP-QUEST-NULL
                TO (SF)-ID-COMP-QUEST-NULL(IX-TAB-DEQ)
           ELSE
              MOVE DEQ-ID-COMP-QUEST
                TO (SF)-ID-COMP-QUEST(IX-TAB-DEQ)
           END-IF
           MOVE DEQ-VAL-RISP
             TO (SF)-VAL-RISP(IX-TAB-DEQ)
           MOVE DEQ-DS-RIGA
             TO (SF)-DS-RIGA(IX-TAB-DEQ)
           MOVE DEQ-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL(IX-TAB-DEQ)
           MOVE DEQ-DS-VER
             TO (SF)-DS-VER(IX-TAB-DEQ)
           MOVE DEQ-DS-TS-INI-CPTZ
             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-DEQ)
           MOVE DEQ-DS-TS-END-CPTZ
             TO (SF)-DS-TS-END-CPTZ(IX-TAB-DEQ)
           MOVE DEQ-DS-UTENTE
             TO (SF)-DS-UTENTE(IX-TAB-DEQ)
           MOVE DEQ-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB(IX-TAB-DEQ).

       VALORIZZA-OUTPUT-DEQ-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   COPY LCCVSDI5
      *   ULTIMO AGG. 07 DIC 2017
      *------------------------------------------------------------

       VAL-DCLGEN-SDI.
           MOVE (SF)-TP-OGG(IX-TAB-SDI)
              TO SDI-TP-OGG
           IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-SDI) = HIGH-VALUES
              MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-SDI)
              TO SDI-ID-MOVI-CHIU-NULL
           ELSE
              MOVE (SF)-ID-MOVI-CHIU(IX-TAB-SDI)
              TO SDI-ID-MOVI-CHIU
           END-IF
           IF (SF)-DT-INI-EFF(IX-TAB-SDI) NOT NUMERIC
              MOVE 0 TO SDI-DT-INI-EFF
           ELSE
              MOVE (SF)-DT-INI-EFF(IX-TAB-SDI)
              TO SDI-DT-INI-EFF
           END-IF
           IF (SF)-DT-END-EFF(IX-TAB-SDI) NOT NUMERIC
              MOVE 0 TO SDI-DT-END-EFF
           ELSE
              MOVE (SF)-DT-END-EFF(IX-TAB-SDI)
              TO SDI-DT-END-EFF
           END-IF
           MOVE (SF)-COD-COMP-ANIA(IX-TAB-SDI)
              TO SDI-COD-COMP-ANIA
           IF (SF)-COD-STRA-NULL(IX-TAB-SDI) = HIGH-VALUES
              MOVE (SF)-COD-STRA-NULL(IX-TAB-SDI)
              TO SDI-COD-STRA-NULL
           ELSE
              MOVE (SF)-COD-STRA(IX-TAB-SDI)
              TO SDI-COD-STRA
           END-IF
           IF (SF)-MOD-GEST-NULL(IX-TAB-SDI) = HIGH-VALUES
              MOVE (SF)-MOD-GEST-NULL(IX-TAB-SDI)
              TO SDI-MOD-GEST-NULL
           ELSE
              MOVE (SF)-MOD-GEST(IX-TAB-SDI)
              TO SDI-MOD-GEST
           END-IF
           IF (SF)-VAR-RIFTO-NULL(IX-TAB-SDI) = HIGH-VALUES
              MOVE (SF)-VAR-RIFTO-NULL(IX-TAB-SDI)
              TO SDI-VAR-RIFTO-NULL
           ELSE
              MOVE (SF)-VAR-RIFTO(IX-TAB-SDI)
              TO SDI-VAR-RIFTO
           END-IF
           IF (SF)-VAL-VAR-RIFTO-NULL(IX-TAB-SDI) = HIGH-VALUES
              MOVE (SF)-VAL-VAR-RIFTO-NULL(IX-TAB-SDI)
              TO SDI-VAL-VAR-RIFTO-NULL
           ELSE
              MOVE (SF)-VAL-VAR-RIFTO(IX-TAB-SDI)
              TO SDI-VAL-VAR-RIFTO
           END-IF
           IF (SF)-DT-FIS-NULL(IX-TAB-SDI) = HIGH-VALUES
              MOVE (SF)-DT-FIS-NULL(IX-TAB-SDI)
              TO SDI-DT-FIS-NULL
           ELSE
              MOVE (SF)-DT-FIS(IX-TAB-SDI)
              TO SDI-DT-FIS
           END-IF
           IF (SF)-FRQ-VALUT-NULL(IX-TAB-SDI) = HIGH-VALUES
              MOVE (SF)-FRQ-VALUT-NULL(IX-TAB-SDI)
              TO SDI-FRQ-VALUT-NULL
           ELSE
              MOVE (SF)-FRQ-VALUT(IX-TAB-SDI)
              TO SDI-FRQ-VALUT
           END-IF
           IF (SF)-FL-INI-END-PER-NULL(IX-TAB-SDI) = HIGH-VALUES
              MOVE (SF)-FL-INI-END-PER-NULL(IX-TAB-SDI)
              TO SDI-FL-INI-END-PER-NULL
           ELSE
              MOVE (SF)-FL-INI-END-PER(IX-TAB-SDI)
              TO SDI-FL-INI-END-PER
           END-IF
           IF (SF)-DS-RIGA(IX-TAB-SDI) NOT NUMERIC
              MOVE 0 TO SDI-DS-RIGA
           ELSE
              MOVE (SF)-DS-RIGA(IX-TAB-SDI)
              TO SDI-DS-RIGA
           END-IF
           MOVE (SF)-DS-OPER-SQL(IX-TAB-SDI)
              TO SDI-DS-OPER-SQL
           IF (SF)-DS-VER(IX-TAB-SDI) NOT NUMERIC
              MOVE 0 TO SDI-DS-VER
           ELSE
              MOVE (SF)-DS-VER(IX-TAB-SDI)
              TO SDI-DS-VER
           END-IF
           IF (SF)-DS-TS-INI-CPTZ(IX-TAB-SDI) NOT NUMERIC
              MOVE 0 TO SDI-DS-TS-INI-CPTZ
           ELSE
              MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-SDI)
              TO SDI-DS-TS-INI-CPTZ
           END-IF
           IF (SF)-DS-TS-END-CPTZ(IX-TAB-SDI) NOT NUMERIC
              MOVE 0 TO SDI-DS-TS-END-CPTZ
           ELSE
              MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-SDI)
              TO SDI-DS-TS-END-CPTZ
           END-IF
           MOVE (SF)-DS-UTENTE(IX-TAB-SDI)
              TO SDI-DS-UTENTE
           MOVE (SF)-DS-STATO-ELAB(IX-TAB-SDI)
              TO SDI-DS-STATO-ELAB
           IF (SF)-TP-STRA-NULL(IX-TAB-SDI) = HIGH-VALUES
              MOVE (SF)-TP-STRA-NULL(IX-TAB-SDI)
              TO SDI-TP-STRA-NULL
           ELSE
              MOVE (SF)-TP-STRA(IX-TAB-SDI)
              TO SDI-TP-STRA
           END-IF
           IF (SF)-TP-PROVZA-STRA-NULL(IX-TAB-SDI) = HIGH-VALUES
              MOVE (SF)-TP-PROVZA-STRA-NULL(IX-TAB-SDI)
              TO SDI-TP-PROVZA-STRA-NULL
           ELSE
              MOVE (SF)-TP-PROVZA-STRA(IX-TAB-SDI)
              TO SDI-TP-PROVZA-STRA
           END-IF
           IF (SF)-PC-PROTEZIONE-NULL(IX-TAB-SDI) = HIGH-VALUES
              MOVE (SF)-PC-PROTEZIONE-NULL(IX-TAB-SDI)
              TO SDI-PC-PROTEZIONE-NULL
           ELSE
              MOVE (SF)-PC-PROTEZIONE(IX-TAB-SDI)
              TO SDI-PC-PROTEZIONE
           END-IF.
       VAL-DCLGEN-SDI-EX.
           EXIT.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA PROGR_NUM_OGG
      *   ALIAS D03
      *   ULTIMO AGG. 16 MAR 2009
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-DATI.
             07 (SF)-COD-COMPAGNIA-ANIA PIC S9(5)     COMP-3.
             07 (SF)-FORMA-ASSICURATIVA PIC X(2).
             07 (SF)-COD-OGGETTO PIC X(30).
             07 (SF)-KEY-BUSINESS PIC X(100).
             07 (SF)-ULT-PROGR PIC S9(18)     COMP-3.
             07 (SF)-PROGR-INIZIALE PIC S9(18)     COMP-3.
             07 (SF)-PROGR-INIZIALE-NULL REDEFINES
                (SF)-PROGR-INIZIALE   PIC X(10).
             07 (SF)-PROGR-FINALE PIC S9(18)     COMP-3.
             07 (SF)-PROGR-FINALE-NULL REDEFINES
                (SF)-PROGR-FINALE   PIC X(10).

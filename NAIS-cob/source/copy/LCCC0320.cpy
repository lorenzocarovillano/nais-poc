      *----------------------------------------------------------------*
      *   PROCESSO DI POST-VENDITA - PORTAFOGLIO VITA
      *   AREA INPUT/OUTPUT
      *   SERVIZIO CALCOLO DATA RIC. SUCC.
      *----------------------------------------------------------------*
           05 (SF)-DATI-INPUT.
              07 (SF)-FL-RATE-ACCORPATE     PIC  X(02).
                  88 (SF)-ACCORPATE-SI      VALUE 'SI'.
                  88 (SF)-ACCORPATE-NO      VALUE 'NO'.
              07 (SF)-NUM-RATE-ACCORPATE    PIC S9(09) COMP.
              07 (SF)-DATI-RICORRENZA.
                 10 (SF)-FL-MOD-CALCOLO     PIC 9(01) VALUE 0.
                   88 (SF)-NO-GESTIONE                 VALUE 0.
                   88 (SF)-GESTIONE-ANNIVERSARIA       VALUE 1.
                   88 (SF)-GESTIONE-DATA-FISSA         VALUE 2.
                   88 (SF)-GESTIONE-DATA-SPECIFICA     VALUE 3.
                 10 (SF)-DATA-RICOR-SUCC    PIC 9(08)  VALUE ZEROES.
                 10 (SF)-FREQUENZA          PIC 9(12)  VALUE 0.
                     88 (SF)-NO-FREQUENZA                VALUE 0.
                     88 (SF)-ANNUALE                     VALUE 1.
                     88 (SF)-SEMESTRALE                  VALUE 2.
                     88 (SF)-QUADRIMESTRALE              VALUE 3.
                     88 (SF)-TRIMESTRALE                 VALUE 4.
                     88 (SF)-BIMESTRALE                  VALUE 6.
                     88 (SF)-MENSILE                     VALUE 12.



      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   VALORIZZA OUTPUT COPY LCCVB043
      *   ULTIMO AGG. 14 MAR 2011
      *------------------------------------------------------------
       VALORIZZA-OUTPUT-B04.
           MOVE B04-ID-BILA-VAR-CALC-P
             TO (SF)-ID-PTF
           MOVE B04-ID-BILA-VAR-CALC-P
             TO (SF)-ID-BILA-VAR-CALC-P
           MOVE B04-COD-COMP-ANIA
             TO (SF)-COD-COMP-ANIA
           MOVE B04-ID-RICH-ESTRAZ-MAS
             TO (SF)-ID-RICH-ESTRAZ-MAS
           IF B04-ID-RICH-ESTRAZ-AGG-NULL = HIGH-VALUES
              MOVE B04-ID-RICH-ESTRAZ-AGG-NULL
                TO (SF)-ID-RICH-ESTRAZ-AGG-NULL
           ELSE
              MOVE B04-ID-RICH-ESTRAZ-AGG
                TO (SF)-ID-RICH-ESTRAZ-AGG
           END-IF
           MOVE B04-DT-RIS
             TO (SF)-DT-RIS
           MOVE B04-ID-POLI
             TO (SF)-ID-POLI
           MOVE B04-ID-ADES
             TO (SF)-ID-ADES
           MOVE B04-PROG-SCHEDA-VALOR
             TO (SF)-PROG-SCHEDA-VALOR
           MOVE B04-TP-RGM-FISC
             TO (SF)-TP-RGM-FISC
           MOVE B04-DT-INI-VLDT-PROD
             TO (SF)-DT-INI-VLDT-PROD
           MOVE B04-COD-VAR
             TO (SF)-COD-VAR
           MOVE B04-TP-D
             TO (SF)-TP-D
           IF B04-VAL-IMP-NULL = HIGH-VALUES
              MOVE B04-VAL-IMP-NULL
                TO (SF)-VAL-IMP-NULL
           ELSE
              MOVE B04-VAL-IMP
                TO (SF)-VAL-IMP
           END-IF
           IF B04-VAL-PC-NULL = HIGH-VALUES
              MOVE B04-VAL-PC-NULL
                TO (SF)-VAL-PC-NULL
           ELSE
              MOVE B04-VAL-PC
                TO (SF)-VAL-PC
           END-IF
           IF B04-VAL-STRINGA-NULL = HIGH-VALUES
              MOVE B04-VAL-STRINGA-NULL
                TO (SF)-VAL-STRINGA-NULL
           ELSE
              MOVE B04-VAL-STRINGA
                TO (SF)-VAL-STRINGA
           END-IF
           MOVE B04-DS-OPER-SQL
             TO (SF)-DS-OPER-SQL
           MOVE B04-DS-VER
             TO (SF)-DS-VER
           MOVE B04-DS-TS-CPTZ
             TO (SF)-DS-TS-CPTZ
           MOVE B04-DS-UTENTE
             TO (SF)-DS-UTENTE
           MOVE B04-DS-STATO-ELAB
             TO (SF)-DS-STATO-ELAB
           MOVE B04-AREA-D-VALOR-VAR-VCHAR
            TO (SF)-AREA-D-VALOR-VAR-VCHAR.
       VALORIZZA-OUTPUT-B04-EX.
           EXIT.

      *----------------------------------------------------------------*
      * VALORIZZAZIONE DCLGEN TABELLA "ESTRATTI DI BILANCIO" CON I DATI
      * PREVENTIVAMENTE SELEZIONATI DALLE TABELLE DI PORTAFOGLIO
      *----------------------------------------------------------------*
       S1250-VALORIZZA-OUT-B03.

           MOVE 'S1250-VALORIZZA-OUT-B03'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

MIKE  *    PERFORM INIZIA-TOT-B03
MIKE  *       THRU INIZIA-TOT-B03-EX.

           MOVE 1                           TO WB03-ELE-B03-MAX

      *--------------------- CAMPI PRENOTAZIONE ------------------------

           MOVE WPOL-COD-COMP-ANIA       TO WB03-COD-COMP-ANIA

           IF WLB-TP-RICH = 'B1'
              SET  WB03-ST-ADD           TO TRUE
              MOVE WLB-ID-RICH           TO WB03-ID-RICH-ESTRAZ-MAS
              MOVE HIGH-VALUES           TO WB03-ID-RICH-ESTRAZ-AGG-NULL
           END-IF

           IF WLB-TP-RICH = 'B2'
              PERFORM S9300-READ-TRANCHE-ESTR
                 THRU EX-S9300
              IF TRANCHE-TROVATA
                 SET  WB03-ST-MOD           TO TRUE
                 MOVE B03-ID-BILA-TRCH-ESTR TO WB03-ID-BILA-TRCH-ESTR
              ELSE
                 SET  WB03-ST-ADD           TO TRUE
              END-IF
              MOVE WLB-ID-RICH-COLL      TO WB03-ID-RICH-ESTRAZ-MAS
              MOVE WLB-ID-RICH           TO WB03-ID-RICH-ESTRAZ-AGG
           END-IF

           MOVE WLB-DT-RISERVA           TO WB03-DT-RIS
           MOVE WPOL-ID-POLI             TO WB03-ID-POLI
           MOVE WADE-ID-ADES(IX-TAB-ADE) TO WB03-ID-ADES
           MOVE WGRZ-ID-GAR(IX-TAB-GRZ)  TO WB03-ID-GAR
           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
             TO WB03-ID-TRCH-DI-GAR
           MOVE WPOL-TP-FRM-ASSVA        TO WB03-TP-FRM-ASSVA

ITRFO      MOVE WGRZ-TP-RAMO-BILA(IX-TAB-GRZ)
ITRFO        TO WB03-TP-RAMO-BILA

ITRFO      MOVE WGRZ-RAMO-BILA(IX-TAB-GRZ)
ITRFO        TO WB03-RAMO-BILA

           MOVE WLB-TP-CALC-RIS          TO WB03-TP-CALC-RIS
           MOVE WLB-FL-SIMULAZIONE       TO WB03-FL-SIMULAZIONE

      *----------------------- CAMPI TECNICI ---------------------------

      *-----CODICE RAMO
           MOVE WPOL-COD-RAMO            TO WB03-COD-RAMO.

      *-----CODICE TARIFFA
           MOVE WGRZ-COD-TARI(IX-TAB-GRZ)
             TO WB03-COD-TARI.

      *-----DATA INIZIO VALIDITA TARIFFA
           IF WGRZ-DT-INI-VAL-TAR-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-DT-INI-VAL-TAR-NULL
           ELSE
              MOVE WGRZ-DT-INI-VAL-TAR(IX-TAB-GRZ)
                TO WB03-DT-INI-VAL-TAR
           END-IF.

      *-----CODICE PRODOTTO
           MOVE WPOL-COD-PROD            TO WB03-COD-PROD.

      *-----DATA INIZIO VALIDITA PRODOTTO
           MOVE WPOL-DT-INI-VLDT-PROD    TO WB03-DT-INI-VLDT-PROD.

      *    MOVE WTGA-TP-RGM-FISC(IX-TAB-TGA)  TO WB03-TP-RGM-FISC.
           IF WTGA-TP-RGM-FISC(IX-TAB-TGA) = SPACES OR HIGH-VALUES
                                             OR LOW-VALUES
              MOVE HIGH-VALUES                  TO WB03-TP-RGM-FISC-NULL
           ELSE
              MOVE WTGA-TP-RGM-FISC(IX-TAB-TGA) TO WB03-TP-RGM-FISC
           END-IF

      *---- PRESTAZIONE IN T
           MOVE ZERO  TO WK-DT-ULT-ADEG-PRE-PR
           IF WTGA-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
              MOVE WTGA-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
                TO WK-DT-ULT-ADEG-PRE-PR
           END-IF
           IF WTGA-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA) = HIGH-VALUES
           OR WK-DT-ULT-ADEG-PRE-PR                    < WLB-DT-RISERVA
              IF WTGA-PRSTZ-ULT-NULL(IX-TAB-TGA) = HIGH-VALUES
                 MOVE WTGA-PRSTZ-ULT-NULL(IX-TAB-TGA)
                   TO WB03-PRSTZ-T-NULL
              ELSE
                 MOVE WTGA-PRSTZ-ULT(IX-TAB-TGA)
                   TO WB03-PRSTZ-T
              END-IF
           ELSE
      *--> DEVO ESTRARRE L'IMMAGINE DELLA TRANCHE CON DATA RIVALUTAZIONE
      *--> MINORE ALLA DATA RISERVA
              PERFORM S1670-PREP-LET-TGA
                 THRU EX-S1670
              IF IDSV0001-ESITO-OK
                 PERFORM S1680-LETTURA-TGA
                    THRU EX-S1680
              END-IF
           END-IF.

      *----DATA ULTIMA RIVALUTAZIONE
           IF WTGA-DT-ULT-ADEG-PRE-PR-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-DT-ULT-RIVAL-NULL
           ELSE
              MOVE WTGA-DT-ULT-ADEG-PRE-PR(IX-TAB-TGA)
                TO WB03-DT-ULT-RIVAL
           END-IF.

NS.   * Soluzione temporanea in attesa della fisicizzazione sul DB del
''    * campo contenente la data. Il campo CODICE_TARIFFA_ORIGIN
''    * contiene la data convenzione presente sulla polizza
''         IF WPOL-DT-INI-VLDT-CONV-NULL = HIGH-VALUES
''            MOVE HIGH-VALUES
''              TO WB03-COD-TARI-ORGN-NULL
''         ELSE
''            MOVE WPOL-DT-INI-VLDT-CONV
''              TO WK-DATA-APPO
''            MOVE WK-DATA-APPO
''              TO WB03-COD-TARI-ORGN
NS.        END-IF

VSTEF *----- TIPO TARIFFA
           IF WGRZ-TP-PRSTZ-ASSTA-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-TP-TARI-NULL
           ELSE
              MOVE WGRZ-TP-PRSTZ-ASSTA(IX-TAB-GRZ)
                TO WB03-TP-TARI
           END-IF.

      *----- TIPO PRESTAZIONE
           IF WGRZ-TP-PRSTZ-ASSTA-NULL(IX-TAB-GRZ) NOT = HIGH-VALUES
              EVALUATE WGRZ-TP-PRSTZ-ASSTA(IX-TAB-GRZ)
                WHEN  'RA'
                WHEN  'RB'
                WHEN  'RC'
                WHEN  'RD'
                WHEN  'RE'
                WHEN  'RF'
                WHEN  'RG'
                WHEN  'RH'
                WHEN  'II'
                WHEN  'IT'
                    MOVE 'RE'          TO WB03-TP-PRSTZ
                WHEN OTHER
                    MOVE 'CA'          TO WB03-TP-PRSTZ
              END-EVALUATE
           ELSE
              MOVE HIGH-VALUES         TO WB03-TP-PRSTZ-NULL
           END-IF.

      *----- PURO INVENTARIO TARIFFA
           IF WGRZ-TP-PRE-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE WGRZ-TP-PRE-NULL(IX-TAB-GRZ)
                TO WB03-PP-INVRIO-TARI-NULL
           ELSE
              MOVE WGRZ-TP-PRE(IX-TAB-GRZ)
                TO WB03-PP-INVRIO-TARI
           END-IF.

      *-----TIPO PERIODO PREMIO
           IF WGRZ-TP-PER-PRE-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-TP-PRE-NULL
           ELSE
              MOVE WGRZ-TP-PER-PRE(IX-TAB-GRZ)
                TO WB03-TP-PRE
           END-IF.

      *-----TIPO ADEGUAMENTO PREMIO
           IF WGRZ-TP-ADEG-PRE-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-TP-ADEG-PRE-NULL
           ELSE
              MOVE WGRZ-TP-ADEG-PRE(IX-TAB-GRZ)
                TO WB03-TP-ADEG-PRE
           END-IF.

      *--  �36 => FLAG DA TRASFORMAZIONE
      *    VALORIZZARE CON 'SI' SE TIPO TRANCHE = 'DA TRASFORMAZIONE'
      *    ALTRIMENTI VALORIZZARE A 'NO'
           IF WTGA-TP-TRCH(IX-TAB-TGA) = '5'
              MOVE 'S'                   TO WB03-FL-DA-TRASF
           ELSE
              MOVE 'N'                   TO WB03-FL-DA-TRASF
           END-IF.

      *-----FLAG CARICO CONTABILE
           IF WTGA-FL-CAR-CONT-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-FL-CAR-CONT-NULL
           ELSE
              MOVE WTGA-FL-CAR-CONT(IX-TAB-TGA)
                TO WB03-FL-CAR-CONT
           END-IF

      *-----PREMIO DA RISERVA
      *                        TO WB03-FL-PRE-DA-RIS-NULL

      *-----PREMIO AGGIUNTIVO
           IF WTGA-TP-TRCH(IX-TAB-TGA) = '2'
              MOVE 'S'         TO  WB03-FL-PRE-AGG
           ELSE
              MOVE 'N'         TO  WB03-FL-PRE-AGG
           END-IF.

      *-----TIPO TRANCHE
           MOVE WTGA-TP-TRCH(IX-TAB-TGA)      TO WB03-TP-TRCH.

      *-----TIPO TESTA
           IF DFA-OK
              IF DFA-TP-ISC-FND-NULL = HIGH-VALUES
                 MOVE HIGH-VALUES
                   TO WB03-TP-TST-NULL
              ELSE
                 MOVE DFA-TP-ISC-FND
                   TO WB03-TP-TST
              END-IF
           ELSE
              MOVE HIGH-VALUES TO WB03-TP-TST-NULL
           END-IF

      *-----CODICE CONVENZIONE
           IF WPOL-COD-CONV-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-COD-CONV-NULL
           ELSE
              MOVE WPOL-COD-CONV
                TO WB03-COD-CONV
           END-IF.

      *-----DATA DECORRENZA POLIZZA
           MOVE WPOL-DT-DECOR                 TO WB03-DT-DECOR-POLI.

      *-----DATA DECORRENZA ADESIONE
           IF WADE-DT-DECOR-NULL(IX-TAB-ADE) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-DT-DECOR-ADES-NULL
           ELSE
              MOVE WADE-DT-DECOR(IX-TAB-ADE)
                TO WB03-DT-DECOR-ADES
           END-IF.

      *-----DATA DECORRENZA TRANCHE
           MOVE WTGA-DT-DECOR(IX-TAB-TGA)     TO WB03-DT-DECOR-TRCH.

      *-----DATA EMISSIONE POLIZZA
           MOVE WPOL-DT-EMIS                  TO WB03-DT-EMIS-POLI.

      *-----DATA EMISSIONE TRANCHE
           IF E12-DT-EMIS-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-DT-EMIS-TRCH-NULL
           ELSE
              MOVE E12-DT-EMIS
                TO WB03-DT-EMIS-TRCH
           END-IF.

      *-----DATA SCADENZA TRANCHE
           IF WTGA-DT-SCAD-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-DT-SCAD-TRCH-NULL
           ELSE
              MOVE WTGA-DT-SCAD(IX-TAB-TGA)
                TO WB03-DT-SCAD-TRCH
           END-IF.

      *-----DATA SCADENZA INTERMEDIA
      *---------------                      TO WB03-DT-SCAD-INTMD-NULL

      *--  �54 => DATA SCADENZA PAGAMENTO PREMI
           IF  WGRZ-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ) NOT = HIGH-VALUES
           AND WGRZ-NUM-AA-PAG-PRE(IX-TAB-GRZ)      NOT = ZEROES
               INITIALIZE           IO-A2K-LCCC0003
                                    IN-RCODE
      *    sommare DELTA alla data di input
               MOVE '02'                        TO A2K-FUNZ

      *        FORMATO AAAAMMGG
               MOVE '03'                        TO A2K-INFO

      *        NUMERO GIORNI
               MOVE ZEROES                      TO A2K-DELTA
               COMPUTE A2K-DELTA = WGRZ-NUM-AA-PAG-PRE(IX-TAB-GRZ)
                                   * 12
      *    MESI
               MOVE 'M'                         TO A2K-TDELTA

      *    GIORNI FISSI
               MOVE '0'                         TO A2K-FISLAV

      *    INIZIO CONTEGGIO DA STESSO GIORNO
               MOVE 0                           TO A2K-INICON

      *    DATA INPUT : Data DECORRENZA GARANZIA
               MOVE WGRZ-DT-DECOR(IX-TAB-GRZ)   TO A2K-INDATA

               PERFORM A9200-CALL-AREA-CALCOLA-DATA
                  THRU EX-A9200

                IF IN-RCODE  = '00'
                   MOVE A2K-OUAMG TO WB03-DT-SCAD-PAG-PRE
                END-IF
           ELSE
               MOVE HIGH-VALUE           TO WB03-DT-SCAD-PAG-PRE-NULL
           END-IF.

      *-----DATA ULTIMO PREMIO PAGATO
           PERFORM S1640-LEGGI-ULT-TIT
              THRU EX-S1640.
           IF DTC1-OK
              IF DTC-DT-INI-COP-NULL = HIGH-VALUES
                 MOVE HIGH-VALUES           TO WB03-DT-ULT-PRE-PAG-NULL
              ELSE
                 MOVE DTC-DT-INI-COP        TO WB03-DT-ULT-PRE-PAG
              END-IF
           ELSE
              MOVE HIGH-VALUES              TO WB03-DT-ULT-PRE-PAG-NULL
           END-IF.

VSTEF *    I CAMPI RELATIVI ALL'ASSICURATO VANNO VALORIZZATI SOLO SE
      *    E' EFFETTIVAMENTE PRESENTE L'ASSICURATO
ALEX  *------ DATA NASCITA PRIMO ASSICURATO
      *    IF WK-DT-NASC-1O-ASSTO-NULL = HIGH-VALUES
      *       MOVE HIGH-VALUES
      *         TO WB03-DT-NASC-1O-ASSTO-NULL
      *    ELSE
      *       MOVE WK-DT-NASC-1O-ASSTO
      *         TO WB03-DT-NASC-1O-ASSTO
      *    END-IF
      *------ SESSO PRIMO ASSICURATO
      *    MOVE WK-SEX-1O-ASSTO TO WB03-SEX-1O-ASSTO
      *-------   ETA' RAGGIUNTA DATA CALCOLO
      *    IF WB03-DT-NASC-1O-ASSTO-NULL NOT = HIGH-VALUES
      *       IF WK-ETA-RAGGN-DT-CALC-NULL NOT = HIGH-VALUE
      *          MOVE WK-ETA-RAGGN-DT-CALC TO WB03-ETA-RAGGN-DT-CALC
      *       ELSE
      *          MOVE HIGH-VALUE TO WB03-ETA-RAGGN-DT-CALC-NULL
      *       END-IF
      *    ELSE
      *       MOVE HIGH-VALUES
      *         TO WB03-DT-NASC-1O-ASSTO-NULL
      *       MOVE HIGH-VALUES
      *         TO WB03-SEX-1O-ASSTO-NULL
      *       MOVE HIGH-VALUES
      *         TO WB03-ETA-RAGGN-DT-CALC-NULL
      *    END-IF
ALEX  *    IF WS-FL-ASSIC-PRES
      *
      *------ DATA NASCITA PRIMO ASSICURATO
      *    IF RAN-DT-NASC-NULL = HIGH-VALUES
      *       MOVE HIGH-VALUES
      *         TO WB03-DT-NASC-1O-ASSTO-NULL
      *    ELSE
      *       MOVE RAN-DT-NASC
      *         TO WB03-DT-NASC-1O-ASSTO
      *    END-IF
      *
      *------ SESSO PRIMO ASSICURATO
      *    IF RAN-SEX-NULL = HIGH-VALUES
      *       MOVE HIGH-VALUES
      *         TO WB03-SEX-1O-ASSTO-NULL
      *    ELSE
      *       MOVE RAN-SEX
      *         TO WB03-SEX-1O-ASSTO
      *    END-IF
      *
      *    IF RAN-DT-NASC-NULL NOT = HIGH-VALUES
      *
      *-------   ETA' RAGGIUNTA DATA CALCOLO
      *       MOVE ZERO                        TO WS-DATA-APPO-NUM
      *                                           DATA-SUPERIORE
      *                                           DATA-INFERIORE
      *                                           GG-DIFF
      *       MOVE WS-DT-CALCOLO-NUM           TO DATA-SUPERIORE
      *       MOVE RAN-DT-NASC                 TO WS-DATA-APPO-NUM
      *       MOVE WS-DATA-APPO-NUM            TO DATA-INFERIORE
      *       MOVE 'A'                         TO FORMATO
      *       INITIALIZE                       WS-ETA-RAGGN-DT-CALC
      *                                        WS-ETA-RAGGN-DT-CALC-V
      *
      *       PERFORM S2900-CALCOLO-DIFF-DATE
      *          THRU EX-S2900
      *
      *       IF CODICE-RITORNO = '0'
      *
VSTEF *          DIVIDE GG-DIFF BY 360 GIVING DUR-AAAA
      *                             REMAINDER WK-APPO-RESTO1
      *
      *          IF WK-APPO-RESTO1 > 0
      *             DIVIDE WK-APPO-RESTO1 BY 30 GIVING DUR-MM
      *          ELSE
      *             MOVE 0                TO DUR-MM
      *          END-IF
      *
      *          IF DUR-MM = 12
      *             ADD  1                TO DUR-AAAA
      *             MOVE 0                TO DUR-MM
      *          END-IF
      *
      *          MOVE DUR-AAAA            TO WS-ETA-RAGGN-DT-CALC-NUM
      *          COMPUTE WS-ETA-RAGGN-DT-CALC-DEC = DUR-MM * 10
      *          MOVE WS-ETA-RAGGN-DT-CALC TO WB03-ETA-RAGGN-DT-CALC
      *       ELSE
      *          MOVE HIGH-VALUES  TO WB03-ETA-RAGGN-DT-CALC-NULL
      *       END-IF
      *    ELSE
      *       MOVE HIGH-VALUES     TO WB03-ETA-RAGGN-DT-CALC-NULL
      *       END-IF
      *    ELSE
      *       MOVE HIGH-VALUES
      *         TO WB03-DT-NASC-1O-ASSTO-NULL
      *       MOVE HIGH-VALUES
      *         TO WB03-SEX-1O-ASSTO-NULL
      *       MOVE HIGH-VALUES
      *         TO WB03-ETA-RAGGN-DT-CALC-NULL
      *    END-IF.
      *
      *-----ETA ANNI  PRIMO ASSICURATO
           IF WTGA-ETA-AA-1O-ASSTO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-ETA-AA-1O-ASSTO-NULL
           ELSE
              MOVE WTGA-ETA-AA-1O-ASSTO(IX-TAB-TGA)
                TO WB03-ETA-AA-1O-ASSTO
           END-IF.

      *-----ETA MESI PRIMO ASSICURATO
           IF WTGA-ETA-MM-1O-ASSTO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-ETA-MM-1O-ASSTO-NULL
           ELSE
              MOVE WTGA-ETA-MM-1O-ASSTO(IX-TAB-TGA)
                TO WB03-ETA-MM-1O-ASSTO
           END-IF.

      *-----DURATA ANNI
           IF WPOL-DUR-AA-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-DUR-AA-NULL
           ELSE
              MOVE WPOL-DUR-AA
                TO WB03-DUR-AA
           END-IF

      *-----DURATA MESI
           IF WPOL-DUR-MM-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-DUR-MM-NULL
           ELSE
              MOVE WPOL-DUR-MM
                TO WB03-DUR-MM
           END-IF

      *-----DURATA GIORNI
           IF WPOL-DUR-GG-NULL = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-DUR-GG-NULL
           ELSE
              MOVE WPOL-DUR-GG
                TO WB03-DUR-GG
           END-IF

      *-----ANTIDURATA ALLA DATA DI CALCOLO
           MOVE ZERO                        TO WS-DATA-APPO-NUM
                                               DATA-SUPERIORE
                                               DATA-INFERIORE
                                               GG-DIFF.
           MOVE WS-DT-CALCOLO-NUM           TO DATA-SUPERIORE
           MOVE WTGA-DT-DECOR(IX-TAB-TGA)   TO WS-DATA-APPO-NUM
           MOVE WS-DATA-APPO-NUM            TO DATA-INFERIORE
           MOVE 'A'                         TO FORMATO

           PERFORM S2900-CALCOLO-DIFF-DATE
              THRU EX-S2900

           IF CODICE-RITORNO = '0'
              COMPUTE WB03-ANTIDUR-DT-CALC = GG-DIFF / 360
           ELSE
              MOVE HIGH-VALUES  TO WB03-ANTIDUR-DT-CALC-NULL
           END-IF.

      *-----DURATA RESIDUA ALLA DATA CALCOLO
           IF WTGA-DT-SCAD-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES TO WB03-DUR-RES-DT-CALC-NULL
           ELSE
              MOVE ZERO                        TO WS-DATA-APPO-NUM
                                                  DATA-SUPERIORE
                                                  DATA-INFERIORE
                                                  GG-DIFF
              MOVE WS-DT-CALCOLO-NUM           TO DATA-INFERIORE
              MOVE WTGA-DT-SCAD(IX-TAB-TGA)    TO WS-DATA-APPO-NUM
              MOVE WS-DATA-APPO-NUM            TO DATA-SUPERIORE
              MOVE 'A'                         TO FORMATO

              PERFORM S2900-CALCOLO-DIFF-DATE
                 THRU EX-S2900

              IF CODICE-RITORNO = '0'
                 COMPUTE WB03-DUR-RES-DT-CALC = GG-DIFF / 360
              ELSE
                 MOVE HIGH-VALUES      TO WB03-DUR-RES-DT-CALC-NULL
              END-IF
           END-IF.

      *-----TIPO STATO OGGETTO BUSINESS POLIZZA
           MOVE WS-TP-STAT-BUS-POL     TO WB03-TP-STAT-BUS-POLI.

      *-----TIPO CAUSALE POLIZZA
           MOVE WS-TP-CAUS-POL         TO WB03-TP-CAUS-POLI.

      *-----TIPO STATO OGGETTO BUSINESS ADESIONE
           MOVE WS-TP-STAT-BUS-ADE     TO WB03-TP-STAT-BUS-ADES.

      *-----TIPO CAUSALE ADESIONE
           MOVE WS-TP-CAUS-ADE         TO WB03-TP-CAUS-ADES.

      *-----TIPO STATO OGGETTO BUSINESS TRANCHE
           MOVE WS-TP-STAT-BUS-TGA     TO WB03-TP-STAT-BUS-TRCH.

      *--> TIPO CAUSALE TRANCHE
VSTEF *--> VALORIZZAZIONE DEL CAMPO TIPO CAUSALE TRANCHE
           SET TROVATO-NO              TO TRUE

      *--> RICERCA DEL TIPO STATO DISINVESTIMENTO
           PERFORM VARYING IX-TAB-CAUS FROM 1 BY 1
                     UNTIL IX-TAB-CAUS > WK-TAB-CAUS-ELE-MAX
                        OR TROVATO

              IF WK-TC-ID-TRCH(IX-TAB-CAUS) = WB03-ID-TRCH-DI-GAR

                 IF WK-TC-TP-STAT-DISINV(IX-TAB-CAUS) NOT = 'CL'
                    MOVE WK-COST-CAUSALE-TRCH
                      TO WB03-TP-CAUS-TRCH
                 END-IF

                 SET TROVATO                TO TRUE

              END-IF

           END-PERFORM

           IF TROVATO-NO
              MOVE WS-TP-CAUS-TGA           TO WB03-TP-CAUS-TRCH
           END-IF

      *-----DATA EFFETTO CAMBIAMENTO STATO
      *                   TO WB03-DT-EFF-CAMB-STAT-NULL

      *-----DATA EMISSIONE CAMBIAMENTO DI STATO
      *                   TO WB03-DT-EMIS-CAMB-STAT-NULL

      *-----DATA EFFETTO STABILIZZAZIONE
           IF WTGA-DT-EFF-STAB-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-DT-EFF-STAB-NULL
           ELSE
              MOVE WTGA-DT-EFF-STAB(IX-TAB-TGA)
                TO WB03-DT-EFF-STAB
           END-IF.

      *-----CAPITALE ALLA DATA DI STABILIZZAZIONE
           IF WTGA-PRSTZ-INI-STAB-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-CPT-DT-STAB-NULL
           ELSE
              MOVE WTGA-PRSTZ-INI-STAB(IX-TAB-TGA)
                TO WB03-CPT-DT-STAB
           END-IF

MIKE  *-----DATA EFFETTO RIDUZIONE
      *-----DATA EMISSIONE RIDUZIONE
      *    MOVE ZERO TO WS-APPO-NUM-18
      *    PERFORM S1650-LEGGI-DT-RIDUZIONE
      *       THRU EX-S1650.
      *    IF STB-OK
      *       MOVE STB-DT-INI-EFF          TO WB03-DT-EFF-RIDZ
      *       MOVE STB-DS-TS-INI-CPTZ      TO WS-APPO-NUM-18
      *       MOVE WS-APPO-NUM-18(1:8)     TO WB03-DT-EMIS-RIDZ
      *    ELSE
      *       MOVE HIGH-VALUES             TO WB03-DT-EFF-RIDZ-NULL
      *                                       WB03-DT-EMIS-RIDZ-NULL
      *    END-IF

      *-----CAPITALE ALLA DATA DI RIDUZIONE
      *                   TO WB03-CPT-DT-RIDZ-NULL

      *----DURATA PAG RENDITA
           SET RENDITA-EROGAZIONE  TO  TRUE
           IF WS-TP-CAUS-TGA  = WS-TP-CAUS
              PERFORM S0111-TRATTA-POG
                 THRU EX-S0111
              IF POG-OK
                 MOVE POG-TP-D    TO WS-TP-DATO
                 EVALUATE TRUE
                   WHEN TD-IMPORTO
                        MOVE POG-VAL-IMP    TO   WB03-DUR-PAG-REN
                   WHEN TD-TASSO
                        MOVE POG-VAL-TS     TO   WB03-DUR-PAG-REN
                   WHEN TD-NUMERICO
                        MOVE POG-VAL-NUM    TO   WB03-DUR-PAG-REN
                 END-EVALUATE
              ELSE
                 MOVE HIGH-VALUES           TO   WB03-DUR-PAG-REN-NULL
              END-IF
           ELSE
              MOVE HIGH-VALUES              TO   WB03-DUR-PAG-REN-NULL
           END-IF.

      *-----DURATA PAG PREMI
           IF WGRZ-NUM-AA-PAG-PRE-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-DUR-PAG-PRE-NULL
           ELSE
              MOVE WGRZ-NUM-AA-PAG-PRE(IX-TAB-GRZ)
                TO WB03-DUR-PAG-PRE
           END-IF

      *-----FRAZIONAMENTO PREMI
           SET NO-TROVATO-6101         TO TRUE
           SET MOVIM-QUIETA            TO TRUE
           PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
           UNTIL IX-TAB-PMO >  WPMO-ELE-PMO-MAX

                IF  WPMO-ID-OGG(IX-TAB-PMO)  = WGRZ-ID-GAR(IX-TAB-GRZ)
                AND WPMO-TP-MOVI(IX-TAB-PMO) = WS-MOVIMENTO
                    IF WPMO-FRQ-MOVI-NULL(IX-TAB-PMO) NOT = HIGH-VALUES
                       MOVE WPMO-FRQ-MOVI(IX-TAB-PMO)
                         TO WB03-FRAZ
                    ELSE
                       MOVE HIGH-VALUES
                         TO WB03-FRAZ-NULL
                    END-IF
                    SET TROVATO-6101 TO TRUE
                END-IF

           END-PERFORM.

           IF NO-TROVATO-6101
              SET NO-TROVATO-6002 TO TRUE
              SET GENER-TRANCH    TO TRUE
              PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
              UNTIL IX-TAB-PMO >  WPMO-ELE-PMO-MAX

                 IF  WPMO-ID-OGG(IX-TAB-PMO)  = WGRZ-ID-GAR(IX-TAB-GRZ)
                 AND WPMO-TP-MOVI(IX-TAB-PMO) = WS-MOVIMENTO
                     IF WPMO-FRQ-MOVI-NULL(IX-TAB-PMO) NOT = HIGH-VALUES
                        MOVE WPMO-FRQ-MOVI(IX-TAB-PMO)
                          TO WB03-FRAZ
                     ELSE
                       MOVE HIGH-VALUES
                         TO WB03-FRAZ-NULL
                     END-IF
                     SET TROVATO-6002 TO TRUE
                 END-IF

              END-PERFORM
           END-IF.

29398      IF NO-TROVATO-6101
           AND NO-TROVATO-6002
              SET NO-TROVATO-6003 TO TRUE
              SET GENER-TRAINC    TO TRUE
              PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
              UNTIL IX-TAB-PMO >  WPMO-ELE-PMO-MAX

                 IF  WPMO-ID-OGG(IX-TAB-PMO)  = WGRZ-ID-GAR(IX-TAB-GRZ)
                 AND WPMO-TP-MOVI(IX-TAB-PMO) = WS-MOVIMENTO
                     IF WPMO-FRQ-MOVI-NULL(IX-TAB-PMO) NOT = HIGH-VALUES
                        MOVE WPMO-FRQ-MOVI(IX-TAB-PMO)
                          TO WB03-FRAZ
                     ELSE
                       MOVE HIGH-VALUES
                         TO WB03-FRAZ-NULL
                     END-IF
                     SET TROVATO-6003 TO TRUE
                 END-IF

              END-PERFORM
29398      END-IF.

           IF  NO-TROVATO-6101
VSTEF      AND NO-TROVATO-6002
29398      AND NO-TROVATO-6003
               MOVE HIGH-VALUES TO WB03-FRAZ-NULL
           END-IF.

VSTEF *-----NUMERO PREMI PATTUITI
            MOVE HIGH-VALUES TO  WB03-NUM-PRE-PATT-NULL.

      *----FRAZIONAMENTO INIZIALE EROGAZIONE RENDITA
           IF WGRZ-FRAZ-INI-EROG-REN-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-FRAZ-INI-EROG-REN-NULL
           ELSE
              MOVE WGRZ-FRAZ-INI-EROG-REN(IX-TAB-GRZ)
                TO WB03-FRAZ-INI-EROG-REN
           END-IF

      *----ANNI DI RENDITA CERTA
           SET NO-TROVATO-6009         TO TRUE.
           SET VARIA-OPZION            TO TRUE.
           PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
           UNTIL IX-TAB-PMO >  WPMO-ELE-PMO-MAX

29398 *         IF  WPMO-ID-OGG(IX-TAB-PMO)  = WGRZ-ID-GAR(IX-TAB-GRZ)
29398           IF  WPMO-ID-OGG(IX-TAB-PMO)  = WADE-ID-ADES(1)
                AND WPMO-TP-MOVI(IX-TAB-PMO) = WS-MOVIMENTO
                    IF WPMO-AA-REN-CER-NULL(IX-TAB-PMO) NOT = HIGH-VALUE
                       MOVE WPMO-AA-REN-CER(IX-TAB-PMO)
                         TO WB03-AA-REN-CER
                    ELSE
                       MOVE HIGH-VALUES
                         TO WB03-AA-REN-CER-NULL
                    END-IF
                    SET TROVATO-6009 TO TRUE
                END-IF

           END-PERFORM.

           IF NO-TROVATO-6009
              MOVE HIGH-VALUES           TO WB03-AA-REN-CER-NULL
           END-IF.

      *----CODICE DIVISA
           MOVE WTGA-COD-DVS(IX-TAB-TGA)  TO WB03-COD-DIV.

      *----VALUTA
           MOVE WTGA-COD-DVS(IX-TAB-TGA)  TO WB03-VLT.

      *----GESTIONE SPECIALE
           IF WGRZ-COD-FND-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE WGRZ-COD-FND-NULL(IX-TAB-GRZ)
                TO WB03-COD-FND-NULL
           ELSE
              MOVE WGRZ-COD-FND(IX-TAB-GRZ)
                TO WB03-COD-FND
           END-IF.

      *----RISCATTI PARZIALI
      *----CUMULO RISCATTI PARZIALI
      *    MOVE ZERO                  TO WS-SOMMA-IMPO-LIQ.

      *    SET RISCATT-KO             TO TRUE.
      *    PERFORM S1625-PREP-P-RISCATTI
      *       THRU EX-S1625.

      *    IF RISCATT-OK
      *       MOVE HIGH-VALUES               TO WB03-RISCPAR-NULL
      *       MOVE WS-SOMMA-IMPO-LIQ         TO WB03-CUM-RISCPAR
      *    ELSE
      *       MOVE HIGH-VALUES               TO WB03-RISCPAR-NULL
      *                                         WB03-CUM-RISCPAR-NULL
      *    END-IF.

      *----RISERVA MATEMATICA CHIUSURA PRECEDENTE
           IF TAB-RIS-OK
              IF RST-RIS-TOT-NULL = HIGH-VALUES
                 MOVE RST-RIS-TOT-NULL
                   TO WB03-RIS-MAT-CHIU-PREC-NULL
              ELSE
                 MOVE RST-RIS-TOT    TO WB03-RIS-MAT-CHIU-PREC
              END-IF
           ELSE
              MOVE HIGH-VALUES TO WB03-RIS-MAT-CHIU-PREC-NULL
           END-IF.

      *----COEFFICIENTE DI RISERVA 1 IN T
           IF TAB-RIS-OK
              IF RST-ULT-COEFF-RIS-NULL = HIGH-VALUES
                 MOVE RST-ULT-COEFF-RIS-NULL
                   TO WB03-COEFF-RIS-1-T-NULL
              ELSE
                 MOVE RST-ULT-COEFF-RIS
                   TO WB03-COEFF-RIS-1-T
              END-IF
           ELSE
              MOVE HIGH-VALUES TO   WB03-COEFF-RIS-1-T-NULL
           END-IF.

      *----COEFFICIENTE DI RISERVA 2 IN T
           IF TAB-RIS-OK
              IF RST-ULT-COEFF-AGG-RIS-NULL = HIGH-VALUES
                 MOVE RST-ULT-COEFF-AGG-RIS-NULL
                   TO WB03-COEFF-RIS-2-T-NULL
              ELSE
                 MOVE RST-ULT-COEFF-AGG-RIS
                   TO WB03-COEFF-RIS-2-T
              END-IF
           ELSE
              MOVE HIGH-VALUES TO    WB03-COEFF-RIS-2-T-NULL
           END-IF.

      *----RISERVA PURA IN T
           IF TAB-RIS-OK
              IF RST-RIS-MAT-NULL = HIGH-VALUES
                 MOVE RST-RIS-MAT-NULL
                   TO WB03-RIS-PURA-T-NULL
              ELSE
                 MOVE RST-RIS-MAT
                   TO WB03-RIS-PURA-T
              END-IF
           ELSE
              MOVE HIGH-VALUES TO   WB03-RIS-PURA-T-NULL
           END-IF.

      *----RISERVA ACQUISTO IN T
           IF TAB-RIS-OK
              IF RST-RIS-ACQ-NULL = HIGH-VALUES
                 MOVE RST-RIS-ACQ-NULL
                   TO WB03-RIS-ACQ-T-NULL
              ELSE
                 MOVE RST-RIS-ACQ
                   TO WB03-RIS-ACQ-T
              END-IF
           ELSE
              MOVE HIGH-VALUES TO  WB03-RIS-ACQ-T-NULL
           END-IF.

      *----RISERVA ZILLMERATA IN T
           IF TAB-RIS-OK
              IF RST-RIS-ZIL-NULL = HIGH-VALUES
                 MOVE RST-RIS-ZIL-NULL
                   TO WB03-RIS-ZIL-T-NULL
              ELSE
                 MOVE RST-RIS-ZIL
                   TO WB03-RIS-ZIL-T
              END-IF
           ELSE
              MOVE HIGH-VALUES TO    WB03-RIS-ZIL-T-NULL
           END-IF.

      *----RISERVA SPESE IN T
           IF TAB-RIS-OK
              IF RST-RIS-SPE-NULL = HIGH-VALUES
                 MOVE RST-RIS-SPE-NULL
                   TO WB03-RIS-SPE-T-NULL
              ELSE
                 MOVE RST-RIS-SPE
                   TO WB03-RIS-SPE-T
              END-IF
           ELSE
              MOVE HIGH-VALUES TO    WB03-RIS-SPE-T-NULL
           END-IF.

      *----PRESTAZIONE INIZIALE
           IF WTGA-PRSTZ-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-PRSTZ-INI-NULL
           ELSE
              MOVE WTGA-PRSTZ-INI(IX-TAB-TGA)
                TO WB03-PRSTZ-INI
           END-IF.

      *----PRESTAZIONE AGGIUNTIVA AL TEMPO 0
      *--- ACCESSO ALLA TABELLA OGGETTO COLLEGATO
           IF WTGA-PRSTZ-AGG-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-PRSTZ-AGG-INI-NULL
           ELSE
              MOVE WTGA-PRSTZ-AGG-INI(IX-TAB-TGA)
                TO WB03-PRSTZ-AGG-INI
           END-IF.

      *----PRESTAZIONE RISTORNI CAPITALIZZATI
      * WB03-RAPPEL-NULL

      *----PREMIO PATTUITO INIZIALE
           IF WTGA-PRE-PATTUITO-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-PRE-PATTUITO-INI-NULL
           ELSE
              MOVE WTGA-PRE-PATTUITO(IX-TAB-TGA)
                TO WB03-PRE-PATTUITO-INI
           END-IF.
      *
      *----PREMIO DOVUTO INIZIALE
           IF WTGA-PRE-INI-NET-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-PRE-DOV-INI-NULL
           ELSE
              MOVE WTGA-PRE-INI-NET(IX-TAB-TGA)
                TO WB03-PRE-DOV-INI
           END-IF.

VSTEF *----PREMIO ANNUALIZZATO RICORRENTE
           IF WGRZ-TP-PER-PRE(IX-TAB-GRZ) = 'R'
              IF  WTGA-PRE-LRD-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
              AND WB03-FRAZ-NULL                NOT = HIGH-VALUES
              AND TROVATO-6101

                 COMPUTE WB03-PRE-ANNUALIZ-RICOR =
                         WB03-FRAZ               *
                         WTGA-PRE-LRD(IX-TAB-TGA)
              ELSE
                 MOVE HIGH-VALUES
                   TO WB03-PRE-ANNUALIZ-RICOR-NULL
              END-IF
           ELSE
               MOVE HIGH-VALUES
                 TO WB03-PRE-ANNUALIZ-RICOR-NULL
           END-IF.

           MOVE WGRZ-TP-PER-PRE(IX-TAB-GRZ)    TO WS-TP-PER-PREMIO.

      *-------------------- RISOLUZIONE SIR05 ------- INIZIO ----------*
      *----PREMIO CONTABILE
           MOVE HIGH-VALUES                    TO WB03-PRE-CONT-NULL.
           MOVE ZERO                           TO WS-DIFF-PRE.
           MOVE ZERO                           TO WS-TAX-TOT.

           IF WS-PRE-TOT-NULL NOT = HIGH-VALUES
              PERFORM S1300-TOT-TAX-TITOLO-CONT
                 THRU EX-S1300-TOT

              IF IDSV0001-ESITO-OK
                 IF WS-TAX-TOT-NULL NOT = HIGH-VALUES
                    SUBTRACT WS-TAX-TOT
                        FROM WS-PRE-TOT
                      GIVING WS-DIFF-PRE
                 END-IF
              END-IF
           END-IF.

           IF WS-DIFF-PRE > 0
              MOVE WS-DIFF-PRE
                TO WB03-PRE-CONT
           END-IF.

      *----PREMIO PURO INIZIALE
           IF WTGA-PRE-PP-INI-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-PRE-PP-INI-NULL
           ELSE
              MOVE WTGA-PRE-PP-INI(IX-TAB-TGA)
                TO WB03-PRE-PP-INI
           END-IF.

      *----COEFF OPZIONE IN RENDITA
           MOVE HIGH-VALUES            TO WB03-COEFF-OPZ-REN-NULL.

      *----COEFF OPZIONE IN CAPITALE
           MOVE HIGH-VALUES            TO WB03-COEFF-OPZ-CPT-NULL.

      *----PROVVIGIONI ACQUISTO
           MOVE ZERO                   TO WS-TOT-PROV-ACQ-WTGA.

           IF WTGA-PROV-1AA-ACQ-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
              ADD WTGA-PROV-1AA-ACQ(IX-TAB-TGA)
               TO WS-TOT-PROV-ACQ-WTGA
           END-IF.

           IF WTGA-PROV-2AA-ACQ-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
              ADD WTGA-PROV-2AA-ACQ(IX-TAB-TGA)
               TO WS-TOT-PROV-ACQ-WTGA
           END-IF.

           IF WS-TOT-PROV-ACQ-WTGA = 0
              MOVE HIGH-VALUE
                TO WB03-PROV-ACQ-NULL
           ELSE
              MOVE WS-TOT-PROV-ACQ-WTGA
                TO WB03-PROV-ACQ
           END-IF.

      *----PROVVIGIONI DI ACQUISTO RICORRENTI
           IF WTGA-PROV-RICOR-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-PROV-ACQ-RICOR-NULL
           ELSE
              MOVE WTGA-PROV-RICOR(IX-TAB-TGA)
                TO WB03-PROV-ACQ-RICOR
           END-IF.

      *----PROVVIGIONI INCASSO
           IF WTGA-PROV-INC-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-PROV-INC-NULL
           ELSE
              MOVE WTGA-PROV-INC(IX-TAB-TGA)
                TO WB03-PROV-INC
           END-IF.

      *----CARICAMENTO GESTIONE
           IF WTGA-IMP-CAR-GEST-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-CAR-GEST-NULL
           ELSE
              MOVE WTGA-IMP-CAR-GEST(IX-TAB-TGA)
                TO WB03-CAR-GEST
           END-IF.

      *--  �160 => CARICAMENTO INCASSO NON SCONTATO
      *    NON VALORIZZARE
           MOVE HIGH-VALUE        TO WB03-CAR-INC-NON-SCON-NULL

      *--  �159 => CARICAMENTO INCASSO
           IF WTGA-IMP-CAR-INC-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-CAR-INC-NULL
           ELSE
              MOVE WTGA-IMP-CAR-INC(IX-TAB-TGA)
                TO WB03-CAR-INC
           END-IF.

      *----ALIQUOTA MARGINE RISERVA
ALEX       IF WK-ALQ-MARG-RIS-NULL = HIGH-VALUE
              MOVE  WK-ALQ-MARG-RIS-NULL  TO WB03-ALQ-MARG-RIS-NULL
           ELSE
              MOVE  WK-ALQ-MARG-RIS       TO WB03-ALQ-MARG-RIS
           END-IF.
ALEX  *    MOVE 'PERCMARGSOLV'
      *      TO WK-COD-PARAM.
      *    PERFORM S0216-IMPOSTA-POG
      *       THRU EX-S0216.
      *
      *    IF IDSO0011-SUCCESSFUL-RC
      *       EVALUATE TRUE
      *           WHEN IDSO0011-NOT-FOUND
      *                MOVE HIGH-VALUES TO WB03-ALQ-MARG-RIS-NULL
      *           WHEN IDSO0011-SUCCESSFUL-SQL
      *                MOVE IDSO0011-BUFFER-DATI    TO PARAM-OGG
      *                MOVE HIGH-VALUES TO WB03-ALQ-MARG-RIS-NULL
      *                MOVE POG-TP-D    TO WS-TP-DATO
      *                EVALUATE TRUE
      *                  WHEN TD-IMPORTO
      *                       MOVE POG-VAL-IMP    TO WB03-ALQ-MARG-RIS
      *                  WHEN TD-TASSO
      *                       MOVE POG-VAL-TS     TO WB03-ALQ-MARG-RIS
      *                  WHEN TD-NUMERICO
      *                       MOVE POG-VAL-NUM    TO WB03-ALQ-MARG-RIS
      *                END-EVALUATE
      *           WHEN OTHER
      *                MOVE WK-PGM
      *                  TO IEAI9901-COD-SERVIZIO-BE
      *                MOVE 'S0216-IMPOSTA-POG'
      *                  TO IEAI9901-LABEL-ERR
      *                MOVE '005016'
      *                  TO IEAI9901-COD-ERRORE
      *                STRING 'LDBS1130;'
      *                       IDSO0011-RETURN-CODE ';'
      *                       IDSO0011-SQLCODE
      *                  DELIMITED BY SIZE
      *                  INTO IEAI9901-PARAMETRI-ERR
      *                END-STRING
      *                PERFORM S0300-RICERCA-GRAVITA-ERRORE
      *                   THRU EX-S0300
      *
      *       END-EVALUATE
      *    ELSE
      *       MOVE WK-PGM
      *         TO IEAI9901-COD-SERVIZIO-BE
      *       MOVE 'S0216-IMPOSTA-POG'
      *         TO IEAI9901-LABEL-ERR
      *       MOVE '005016'
      *         TO IEAI9901-COD-ERRORE
      *       STRING 'LDBS1130;'
      *              IDSO0011-RETURN-CODE ';'
      *              IDSO0011-SQLCODE
      *         DELIMITED BY SIZE
      *         INTO IEAI9901-PARAMETRI-ERR
      *       END-STRING
      *       PERFORM S0300-RICERCA-GRAVITA-ERRORE
      *          THRU EX-S0300
      *
      *    END-IF.
      *
ALEX       IF WK-ALQ-MARG-C-SUBRSH-NULL = HIGH-VALUE
              MOVE WK-ALQ-MARG-C-SUBRSH-NULL
                TO WB03-ALQ-MARG-C-SUBRSH-NULL
           ELSE
              MOVE WK-ALQ-MARG-C-SUBRSH
                TO WB03-ALQ-MARG-C-SUBRSH
           END-IF.
ALEX  *----ALIQUOTA MARGINE CAP SOTTO RISCHIO
      *    MOVE 'MARGCAPSRIS'
      *      TO WK-COD-PARAM.
      *    PERFORM S0216-IMPOSTA-POG
      *       THRU EX-S0216.
      *
      *    IF IDSO0011-SUCCESSFUL-RC
      *       EVALUATE TRUE
      *           WHEN IDSO0011-NOT-FOUND
      *                MOVE HIGH-VALUES
      *                  TO WB03-ALQ-MARG-C-SUBRSH-NULL
      *
      *           WHEN IDSO0011-SUCCESSFUL-SQL
      *                MOVE IDSO0011-BUFFER-DATI
      *                  TO PARAM-OGG
      *                MOVE HIGH-VALUES
      *                  TO WB03-ALQ-MARG-C-SUBRSH-NULL
      *                MOVE POG-TP-D
      *                  TO WS-TP-DATO
      *                EVALUATE TRUE
      *                  WHEN TD-IMPORTO
      *                       MOVE POG-VAL-IMP
      *                         TO WB03-ALQ-MARG-C-SUBRSH
      *                  WHEN TD-TASSO
      *                       MOVE POG-VAL-TS
      *                         TO WB03-ALQ-MARG-C-SUBRSH
      *                  WHEN TD-NUMERICO
      *                       MOVE POG-VAL-NUM
      *                         TO WB03-ALQ-MARG-C-SUBRSH
      *                END-EVALUATE
      *
      *           WHEN OTHER
      *                MOVE WK-PGM
      *                  TO IEAI9901-COD-SERVIZIO-BE
      *                MOVE 'S0216-IMPOSTA-POG'
      *                  TO IEAI9901-LABEL-ERR
      *                MOVE '005016'
      *                  TO IEAI9901-COD-ERRORE
      *                STRING 'LDBS1130;'
      *                       IDSO0011-RETURN-CODE ';'
      *                       IDSO0011-SQLCODE
      *                  DELIMITED BY SIZE
      *                  INTO IEAI9901-PARAMETRI-ERR
      *                END-STRING
      *                PERFORM S0300-RICERCA-GRAVITA-ERRORE
      *                   THRU EX-S0300
      *
      *       END-EVALUATE
      *    ELSE
      *       MOVE WK-PGM
      *         TO IEAI9901-COD-SERVIZIO-BE
      *       MOVE 'S0216-IMPOSTA-POG'
      *         TO IEAI9901-LABEL-ERR
      *       MOVE '005016'
      *         TO IEAI9901-COD-ERRORE
      *       STRING 'LDBS1130;'
      *              IDSO0011-RETURN-CODE ';'
      *              IDSO0011-SQLCODE
      *         DELIMITED BY SIZE
      *         INTO IEAI9901-PARAMETRI-ERR
      *       END-STRING
      *       PERFORM S0300-RICERCA-GRAVITA-ERRORE
      *          THRU EX-S0300
      *
      *    END-IF.

      *----CLASSIFICAZIONE IAS
           IF WGRZ-TP-IAS-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE WGRZ-TP-IAS-NULL(IX-TAB-GRZ)
                TO WB03-TP-IAS-NULL
           ELSE
              MOVE WGRZ-TP-IAS(IX-TAB-GRZ)
                TO WB03-TP-IAS
           END-IF

      *----TIPO COASS
           MOVE HIGH-VALUES              TO WB03-TP-COASS-NULL

      *----NS QUOTA
           MOVE HIGH-VALUES              TO WB03-NS-QUO-NULL

      *----TASSO MEDIO
           MOVE HIGH-VALUES              TO WB03-TS-MEDIO-NULL

      *----CAPITALE RIASSICURATO
           MOVE HIGH-VALUES              TO WB03-CPT-RIASTO-NULL

      *----PREMIO RIASSICURATO
           MOVE HIGH-VALUES              TO WB03-PRE-RIASTO-NULL

      *----TRATTATO RIASSICURATO
           MOVE HIGH-VALUES              TO WB03-TRAT-RIASS-NULL

      *----RISERVA RIASSICURATA
           MOVE HIGH-VALUES              TO WB03-RIS-RIASTA-NULL

      *----TRATT RIASS ECCEDENTE
           MOVE HIGH-VALUES              TO WB03-TRAT-RIASS-ECC-NULL

      *----CAP RIASSIC ECCEDENTE
           MOVE HIGH-VALUES              TO WB03-CPT-RIASTO-ECC-NULL

      *----PREM RIASS ECCEDENTE
           MOVE HIGH-VALUES              TO WB03-PRE-RIASTO-ECC-NULL

      *----RISERVA RIASS ECCEDENTE
           MOVE HIGH-VALUES              TO WB03-RIS-RIASTA-ECC-NULL

      *----IB POLIZZA
           IF WPOL-IB-OGG-NULL = HIGH-VALUES
              MOVE WPOL-IB-OGG-NULL
                TO WB03-IB-POLI-NULL
           ELSE
              MOVE WPOL-IB-OGG
                TO WB03-IB-POLI
           END-IF.

      *----IB ADES
           IF WADE-IB-OGG-NULL(IX-TAB-ADE) = HIGH-VALUES
              MOVE WADE-IB-OGG-NULL(IX-TAB-ADE)
                TO WB03-IB-ADES-NULL
           ELSE
              MOVE WADE-IB-OGG(IX-TAB-ADE)
                TO WB03-IB-ADES
           END-IF.

      *----IB TRANCHE
           IF WTGA-IB-OGG-NULL(IX-TAB-TGA) = HIGH-VALUES
              MOVE WTGA-IB-OGG-NULL(IX-TAB-TGA)
                TO WB03-IB-TRCH-DI-GAR-NULL
           ELSE
              MOVE WTGA-IB-OGG(IX-TAB-TGA)
                TO WB03-IB-TRCH-DI-GAR
           END-IF.

      *----AGENZIA
      *    IF RRE-COD-PNT-RETE-INI-NULL = HIGH-VALUES
      *       MOVE HIGH-VALUES
      *         TO WB03-COD-AGE-NULL
      *    ELSE
      *       MOVE RRE-COD-PNT-RETE-INI
      *         TO WB03-COD-AGE
      *    END-IF.

      *----SUBAGENZIA
      *    IF RRE-COD-PNT-RETE-END-NULL = HIGH-VALUES
      *       MOVE HIGH-VALUES
      *         TO WB03-COD-SUBAGE-NULL
      *    ELSE
      *       MOVE RRE-COD-PNT-RETE-END
      *         TO WB03-COD-SUBAGE
      *    END-IF.

      *----CANALE
      *    IF RRE-COD-CAN-NULL = HIGH-VALUES
      *       MOVE HIGH-VALUES
      *         TO WB03-COD-CAN-NULL
      *    ELSE
      *       MOVE RRE-COD-CAN
      *         TO WB03-COD-CAN
      *    END-IF.

TESTKK     MOVE HIGH-VALUES    TO WB03-DT-SCAD-INTMD-NULL
TESTKK     MOVE HIGH-VALUES    TO WB03-DT-EFF-CAMB-STAT-NULL
TESTKK     MOVE HIGH-VALUES    TO WB03-DT-EMIS-CAMB-STAT-NULL
TESTKK     MOVE HIGH-VALUES    TO WB03-DT-EFF-RIDZ-NULL
TESTKK     MOVE HIGH-VALUES    TO WB03-DT-EMIS-RIDZ-NULL
TESTKK     MOVE WLB-DT-RISERVA TO WB03-DT-PRODUZIONE.

      *    �213 --> Durata Garanzia Anni
           IF WGRZ-DUR-AA-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-DUR-GAR-AA-NULL
           ELSE
              MOVE WGRZ-DUR-AA(IX-TAB-GRZ)
                TO WB03-DUR-GAR-AA
           END-IF

      *    �214 --> Durata Garanzia Mesi
           IF WGRZ-DUR-MM-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-DUR-GAR-MM-NULL
           ELSE
              MOVE WGRZ-DUR-MM(IX-TAB-GRZ)
                TO WB03-DUR-GAR-MM
           END-IF

      *    �215 --> Durata Garanzia Giorni
           IF WGRZ-DUR-GG-NULL(IX-TAB-GRZ) = HIGH-VALUES
              MOVE HIGH-VALUES
                TO WB03-DUR-GAR-GG-NULL
           ELSE
              MOVE WGRZ-DUR-GG(IX-TAB-GRZ)
                TO WB03-DUR-GAR-GG
           END-IF

      *    �216 --> Antidurata alla data calcolo 365
      *             (Data Calcolo - Data Decorrenza Tranche)
           MOVE ZERO                        TO WS-DATA-APPO-NUM
                                               DATA-SUPERIORE
                                               DATA-INFERIORE
                                               GG-DIFF.
           MOVE WS-DT-CALCOLO-NUM           TO DATA-SUPERIORE
           MOVE WTGA-DT-DECOR(IX-TAB-TGA)   TO WS-DATA-APPO-NUM
           MOVE WS-DATA-APPO-NUM            TO DATA-INFERIORE
           MOVE 'A'                         TO FORMATO

           PERFORM S2950-CALCOLO-DIFF-DATE-365
              THRU EX-S2950

           IF CODICE-RITORNO = '0'
              COMPUTE WB03-ANTIDUR-CALC-365 = GG-DIFF / 365
           ELSE
              MOVE HIGH-VALUES  TO WB03-ANTIDUR-CALC-365-NULL
           END-IF

      *    �220 --> Codice Fiscale Contraente
      *    MOVE HIGH-VALUES                TO WB03-COD-FISC-CNTR-NULL

      *    QUESTO ACCESSO NELLA VERSIONE DI UNIPOL VIENE UTILIZZATA PER
      *    REPERIRE IL CODICE FISCALE DEL CONTRAENTE. SI DEVE FARE LA
      *    COSA
ALEX  *    PERFORM S0162-PREP-ACC-RAN-CONT
      *       THRU EX-S0162
      *    PERFORM S0165-LEGGI-RAPP-ANA
      *       THRU EX-S0165
      *
      *    IF IDSV0001-ESITO-OK
      *       PERFORM S0200-PREP-LETT-PERS
      *          THRU EX-S0200
      *       IF IDSV0001-ESITO-OK
      *          PERFORM S0201-LEGGI-PERS
      *             THRU EX-S0201
      *       END-IF
      *    END-IF

ALEX  *    IF IDSV0001-ESITO-OK
      *       IF WGRZ-ID-1O-ASSTO-NULL(IX-TAB-GRZ) NOT = HIGH-VALUES
      *          MOVE WGRZ-ID-1O-ASSTO(IX-TAB-GRZ)
      *            TO WK-ID-ASSTO
      *          PERFORM S0160-PREP-ACC-RAN-ASS
      *             THRU EX-S0160
      *          PERFORM S0165-LEGGI-RAPP-ANA
      *             THRU EX-S0165
      *
      *    �252 --> Data decesso Assicurato 10 Ass
      *          IF IDSV0001-ESITO-OK
      *          AND WS-FL-ASSIC-PRES
      *            IF WGRZ-TP-PRSTZ-ASSTA(IX-TAB-GRZ) = 'TF'
      *             IF RAN-DT-DECES-NULL NOT = HIGH-VALUES
      *                AND RAN-DT-DECES > 0
      *                MOVE 'M' TO WB03-STAT-TBGC-ASSTO-1
      *             ELSE
      *                MOVE 'V' TO WB03-STAT-TBGC-ASSTO-1
      *             END-IF
      *            END-IF
      *    �263 --> Stato tabagico assicurato  10 Ass
      *             IF RAN-FL-FUMATORE-NULL NOT = HIGH-VALUES
      *                MOVE RAN-FL-FUMATORE TO
      *                             WB03-STAT-TBGC-ASSTO-1
      *             END-IF
      *    �263 --> Stato tabagico assicurato  10 Ass FINE
      *          END-IF
      *
      *    �252 --> Data decesso Assicurato 10 Ass
      *          IF IDSV0001-ESITO-OK
      *          AND WS-FL-ASSIC-PRES
      *             PERFORM S0200-PREP-LETT-PERS
      *                THRU EX-S0200
      *          END-IF
      *          IF  IDSV0001-ESITO-OK
      *          AND WS-FL-ASSIC-PRES
      *             PERFORM S0201-LEGGI-PERS
      *                THRU EX-S0201
      *          END-IF
      *
      *       END-IF
      *    END-IF
      *
ALEX  *    IF IDSV0001-ESITO-OK
      *       IF WGRZ-ID-2O-ASSTO-NULL(IX-TAB-GRZ) NOT = HIGH-VALUES
      *          MOVE WGRZ-ID-2O-ASSTO(IX-TAB-GRZ)
      *            TO WK-ID-ASSTO
      *          PERFORM S0160-PREP-ACC-RAN-ASS
      *             THRU EX-S0160
      *          PERFORM S0165-LEGGI-RAPP-ANA
      *             THRU EX-S0165
      *    �253 --> Data decesso Assicurato 20 Ass
      *          IF IDSV0001-ESITO-OK
      *          AND WS-FL-ASSIC-PRES
      *            IF WGRZ-TP-PRSTZ-ASSTA(IX-TAB-GRZ) = 'TF'
      *             IF RAN-DT-DECES-NULL NOT = HIGH-VALUES
      *                AND RAN-DT-DECES > 0
      *                MOVE 'M' TO WB03-STAT-TBGC-ASSTO-2
      *             ELSE
      *                MOVE 'V' TO WB03-STAT-TBGC-ASSTO-2
      *             END-IF
      *            END-IF
      *    �264 --> Stato tabagico assicurato  20 Ass
      *             IF RAN-FL-FUMATORE-NULL NOT = HIGH-VALUES
      *                MOVE RAN-FL-FUMATORE TO
      *                             WB03-STAT-TBGC-ASSTO-2
      *             END-IF
      *    �264 --> Stato tabagico assicurato  20 Ass FINE
      *          END-IF
      *    �253 --> Data decesso Assicurato 20 Ass
      *
      *          IF IDSV0001-ESITO-OK
      *          AND WS-FL-ASSIC-PRES
      *             PERFORM S0200-PREP-LETT-PERS
      *                THRU EX-S0200
      *          END-IF
      *          IF  IDSV0001-ESITO-OK
      *          AND WS-FL-ASSIC-PRES
      *             PERFORM S0201-LEGGI-PERS
      *                THRU EX-S0201
      *          END-IF
      *
      *       END-IF
      *    END-IF
      *
ALEX  *    IF IDSV0001-ESITO-OK
      *       IF WGRZ-ID-3O-ASSTO-NULL(IX-TAB-GRZ) NOT = HIGH-VALUES
      *          MOVE WGRZ-ID-3O-ASSTO(IX-TAB-GRZ)
      *            TO WK-ID-ASSTO
      *          PERFORM S0160-PREP-ACC-RAN-ASS
      *             THRU EX-S0160
      *          PERFORM S0165-LEGGI-RAPP-ANA
      *             THRU EX-S0165
      *    �254 --> Data decesso Assicurato 30 Ass
      *          IF IDSV0001-ESITO-OK
      *          AND WS-FL-ASSIC-PRES
      *            IF WGRZ-TP-PRSTZ-ASSTA(IX-TAB-GRZ) = 'TF'
      *             IF RAN-DT-DECES-NULL NOT = HIGH-VALUES
      *                AND RAN-DT-DECES > 0
      *                MOVE 'M' TO WB03-STAT-TBGC-ASSTO-3
      *             ELSE
      *                MOVE 'V' TO WB03-STAT-TBGC-ASSTO-3
      *             END-IF
      *            END-IF
      *    �265 --> Stato tabagico assicurato  30 Ass
      *             IF RAN-FL-FUMATORE-NULL NOT = HIGH-VALUES
      *                MOVE RAN-FL-FUMATORE TO
      *                             WB03-STAT-TBGC-ASSTO-3
      *             END-IF
      *    �265 --> Stato tabagico assicurato  30 Ass FINE
      *          END-IF
      *    �254 --> Data decesso Assicurato 30 Ass
      *
      *          IF IDSV0001-ESITO-OK
      *          AND WS-FL-ASSIC-PRES
      *             PERFORM S0200-PREP-LETT-PERS
      *                THRU EX-S0200
      *          END-IF
      *          IF  IDSV0001-ESITO-OK
      *          AND WS-FL-ASSIC-PRES
      *             PERFORM S0201-LEGGI-PERS
      *                THRU EX-S0201
      *          END-IF
      *
      *       END-IF
      *    END-IF

ALEX  *    MOVE WK-STAT-TBGC-ASSTO-1 TO WB03-STAT-TBGC-ASSTO-1
ALEX  *    MOVE WK-STAT-TBGC-ASSTO-2 TO WB03-STAT-TBGC-ASSTO-2
ALEX  *    MOVE WK-STAT-TBGC-ASSTO-3 TO WB03-STAT-TBGC-ASSTO-3
      *    QUESTE VALORIZZAZIONI NON C'ERANO NELLA VERSIONE PRECEDENTE
ALEX  *    MOVE WK-COD-FISC-CNTR     TO WB03-COD-FISC-CNTR
ALEX  *    MOVE WK-COD-FISC-ASSTO1   TO WB03-COD-FISC-ASSTO1
ALEX  *    MOVE WK-COD-FISC-ASSTO2   TO WB03-COD-FISC-ASSTO2
ALEX  *    MOVE WK-COD-FISC-ASSTO3   TO WB03-COD-FISC-ASSTO3

      *    SOLO PER INDEX
           IF IDSV0001-ESITO-OK
3357  *       �247 --> Tipo Versamento
3357  *       IF IDSV0001-ESITO-OK
3357  *          MOVE WGRZ-TP-PER-PRE(IX-TAB-GRZ) TO WS-TP-PER-PREMIO
3357  *          IF WS-PREMIO-RICORRENTE
3357  *             PERFORM S0206-PREP-LET-DETT-TIT
3357  *                THRU EX-S0206
3357  *             PERFORM S0207-LETTURA-DETT-TIT
3357  *                THRU EX-S0207
3357  *             IF IDSV0001-ESITO-OK
3357  *                IF NO-TITOLO-COLLEGATO
3357  *                   MOVE 'N' TO WB03-TP-VERS
3357  *                ELSE
3357  *                   PERFORM S0208-PREP-LET-TITOLO
3357  *                      THRU EX-S0208
3357  *                   PERFORM S0209-LETTURA-TITOLO
3357  *                      THRU EX-S0209
3357  *                   IF IDSV0001-ESITO-OK
3357  *                      IF TIT-TP-PRE-TIT = 'PE'
3357  *                         MOVE 'P' TO WB03-TP-VERS
3357  *                      END-IF
3357  *                      IF WTGA-TP-TRCH(IX-TAB-TGA) = '2'
3357  *                         MOVE 'S' TO WB03-TP-VERS
3357  *                      END-IF
3357  *                      IF WTGA-TP-TRCH(IX-TAB-TGA) = '1' AND
3357  *                         TIT-TP-PRE-TIT NOT = 'PE'
3357  *                         INITIALIZE IO-A2K-LCCC0003
3357  *                         MOVE '02'          TO A2K-FUNZ
3357  *                         MOVE '03'          TO A2K-INFO
3357  *                         MOVE 1             TO A2K-DELTA
3357  *                         MOVE 'A'           TO A2K-TDELTA
3357  *                         MOVE WPOL-DT-DECOR TO A2K-INDATA
3357  *                         MOVE '0'           TO A2K-FISLAV
3357  *                                               A2K-INICON
3357  *                         PERFORM A9200-CALL-AREA-CALCOLA-DATA
3357  *                            THRU EX-A9200
3357  *                         IF IDSV0001-ESITO-OK
3357  *                            MOVE A2K-OUAMG  TO WK-DT-DECOR-POL
3357  *                            IF WTGA-DT-DECOR(IX-TAB-TGA) >=
3357  *                               WPOL-DT-DECOR AND
3357  *                               WTGA-DT-DECOR(IX-TAB-TGA) <=
3357  *                               WK-DT-DECOR-POL
3357  *                               MOVE 'Z'     TO WB03-TP-VERS
3357  *                            END-IF
3357  *                            IF WTGA-DT-DECOR(IX-TAB-TGA) >
3357  *                               WK-DT-DECOR-POL
3357  *                               MOVE 'X'     TO WB03-TP-VERS
3357  *                            END-IF
3357  *                         END-IF
3357  *                      END-IF
3357  *                   END-IF
3357  *                END-IF
3357  *             END-IF
3357  *          END-IF
3357  *       END-IF

      *       �251 --> Produttore
      *       IF IDSV0001-ESITO-OK
      *          PERFORM S2801-LEGGI-RAPP-RETE-AC
      *          IF RRE-COD-ACQS-CNTRT-NULL NOT = HIGH-VALUES
      *             MOVE RRE-COD-ACQS-CNTRT TO B03-COD-PRDT
      *          END-IF
      *       END-IF
      *       �251 --> Produttore FINE

      *       �255 --> Capitale rischio morte
              IF WTGA-CPT-RSH-MOR-NULL(IX-TAB-TGA)NOT = HIGH-VALUES
                 MOVE WTGA-CPT-RSH-MOR(IX-TAB-TGA) TO
                        WB03-CPT-ASSTO-INI-MOR
              END-IF
      *       �255 --> Capitale rischio morte  FINE

      *       �256 --> Tasso stabilizzazione limitata
              IF WGRZ-TP-ADEG-PRE(IX-TAB-GRZ) = '4'
               IF WGRZ-TS-STAB-LIMITATA-NULL(IX-TAB-GRZ)
                                          NOT = HIGH-VALUES
                 MOVE WGRZ-TS-STAB-LIMITATA(IX-TAB-GRZ) TO
                        WB03-TS-STAB-PRE
               END-IF
              END-IF
      *       �256 --> Tasso stabilizzazione limitata  FINE

      *       �261 -->  Costi di emissione
              IF IDSV0001-ESITO-OK
                 PERFORM S2812-DIRITTI-EMIS-PERFEZ
                 IF DIR-OK
                    IF LDBV0371-DTC-DIR-NULL NOT = HIGH-VALUES
                     MOVE LDBV0371-DTC-DIR TO WB03-DIR-EMIS
                    END-IF
                 ELSE
                    MOVE ZEROES TO WB03-DIR-EMIS
                 END-IF
              END-IF

      *       �261 -->  Costi di emissione FINE

      *       �262 -->  data esito titolo
              IF IDSV0001-ESITO-OK
                 PERFORM S2813-MAX-DATA-ESI-TIT
                 IF DTC2-OK
                   IF DTC-DT-ESI-TIT-NULL = HIGH-VALUES
                      MOVE HIGH-VALUES
                              TO WB03-DT-INC-ULT-PRE-NULL
                   ELSE
                     MOVE DTC-DT-ESI-TIT       TO WB03-DT-INC-ULT-PRE
                   END-IF
                ELSE
                   MOVE HIGH-VALUES
                                      TO WB03-DT-INC-ULT-PRE-NULL
                END-IF
              END-IF

      *       �262 -->  data esito titolo    FINE

      *       �266 --> Decrescenza Capitale
              IF IDSV0001-ESITO-OK
                IF WGRZ-TP-PRSTZ-ASSTA(IX-TAB-GRZ) = 'TC'
                 IF WGRZ-FRAZ-DECR-CPT-NULL (IX-TAB-GRZ)
                    NOT = HIGH-VALUES
                    MOVE  WGRZ-FRAZ-DECR-CPT(IX-TAB-GRZ) TO
                            WB03-FRAZ-DECR-CPT
                 END-IF
                END-IF
              END-IF
      *       �266 --> Decrescenza Capitale Fine
      *       �267 --> Premio puro ultimo
              IF WTGA-PRE-PP-ULT-NULL(IX-TAB-TGA) NOT = HIGH-VALUES
                 MOVE WTGA-PRE-PP-ULT(IX-TAB-TGA) TO WB03-PRE-PP-ULT
              ELSE
                 MOVE WTGA-PRE-PP-ULT-NULL(IX-TAB-TGA)
                   TO WB03-PRE-PP-ULT-NULL
              END-IF
      *       �267 --> Premio puro ultimo FINE

      *       �268 --> Premio attivo di tranche temporaneamente
      *                appoggiato nel campo WB03-RAPPEL
              MOVE ZERO                  TO WB03-RAPPEL
              IF E12-CUM-PRE-ATT-NULL NOT = HIGH-VALUES
                 MOVE E12-CUM-PRE-ATT       TO WB03-RAPPEL
              ELSE
                 MOVE E12-CUM-PRE-ATT-NULL  TO
                      WB03-RAPPEL-NULL
              END-IF
      *       �268 --> Premio attivo di tranche

34369 *            --> Causale sconto
34369          MOVE HIGH-VALUES TO WB03-CAUS-SCON
34369          IF E12-CAUS-SCON-NULL NOT = HIGH-VALUES
34369             MOVE E12-CAUS-SCON   TO WB03-CAUS-SCON
34369          END-IF
           END-IF.

ITRFO      PERFORM I000-INIZIALIZZA-CAMPI-CPI
ITRFO         THRU I000-EX

ITRFO      IF WPOL-FL-POLI-CPI-PR-NULL NOT = HIGH-VALUE AND
ITRFO         WPOL-FL-POLI-CPI-PR = 'S'
ITRFO         PERFORM V000-VALORIZZA-CPI
ITRFO            THRU V000-EX
ITRFO      END-IF.

           IF  WK-MESI-CARENZA-NULL = HIGH-VALUES
               MOVE WK-MESI-CARENZA-NULL  TO WB03-CARZ-NULL
           ELSE
               MOVE WK-MESI-CARENZA       TO WB03-CARZ
           END-IF.

       EX-S1250.
           EXIT.
ITRFO *-----------------------------------------------------------------
      *    INIZIALIZZAZIONE DEI CAMPI INSERITI SULLA BILA_TRCH_ESTR PER
      *    IT RATIONALIZATION
      *-----------------------------------------------------------------
       I000-INIZIALIZZA-CAMPI-CPI.

           MOVE 'I000-INIZIALIZZA-CAMPI-CPI' TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO          TO TRUE
           MOVE WK-LABEL-ERR                 TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE HIGH-VALUES
             TO WB03-ACQ-EXP-NULL

           MOVE HIGH-VALUES
             TO WB03-REMUN-ASS-NULL

           MOVE HIGH-VALUES
             TO WB03-COMMIS-INTER-NULL

           MOVE HIGH-VALUES
             TO WB03-NUM-FINANZ-NULL

           MOVE HIGH-VALUES
             TO WB03-TP-ACC-COMM-NULL

           MOVE HIGH-VALUES
             TO WB03-IB-ACC-COMM-NULL.

       I000-EX.
ITRFO      EXIT.
ITRFO *-----------------------------------------------------------------
      *    VALORIZAZZIONE DEI CAMPI INSERITI SULLA BILA_TRCH_ESTR PER
      *    IT RATIONALIZATION
      *-----------------------------------------------------------------
       V000-VALORIZZA-CPI.

           MOVE 'V000-VALORIZZA-CPI'        TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           IF WTGA-ACQ-EXP-NULL(IX-TAB-TGA) = HIGH-VALUE
              CONTINUE
           ELSE
              MOVE WTGA-ACQ-EXP(IX-TAB-TGA)
                TO WB03-ACQ-EXP
           END-IF

           IF WTGA-REMUN-ASS-NULL(IX-TAB-TGA) = HIGH-VALUE
              CONTINUE
           ELSE
              MOVE WTGA-REMUN-ASS(IX-TAB-TGA)
                TO WB03-REMUN-ASS
           END-IF

           IF WTGA-COMMIS-INTER-NULL(IX-TAB-TGA) = HIGH-VALUE
              CONTINUE
           ELSE
              MOVE WTGA-COMMIS-INTER(IX-TAB-TGA)
                TO WB03-COMMIS-INTER
           END-IF

           IF IDSV0001-ESITO-OK
              PERFORM L320-ACCEDI-EST-POLI-CPI-PR
                 THRU L320-EX
           END-IF

           IF IDSV0001-ESITO-OK
              IF POL-ID-ACC-COMM-NULL NOT = HIGH-VALUES AND ZEROES
                                                        AND LOW-VALUES
                 PERFORM L310-ACCEDI-ACC-COMM
                    THRU L310-EX
              END-IF
           END-IF.

       V000-EX.
ITRFO      EXIT.
      *-----------------------------------------------------------------
      *    LETTURA DELLA TABELLA DETTAGLIO TITOLO CONTABILE
      *    SOMMANDO L'IMPORTO TASSA
      *-----------------------------------------------------------------
       S1300-TOT-TAX-TITOLO-CONT.

           MOVE 'S1300-TOT-TAX-TITOLO-CONT'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

      *--> VALORIZZAZIONE DCLGEN

           MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) TO DTC-ID-OGG.
           MOVE 'TG'                            TO DTC-TP-OGG.

           MOVE WLB-DT-RISERVA          TO IDSI0011-DATA-INIZIO-EFFETTO
                                           IDSI0011-DATA-FINE-EFFETTO.
           MOVE WS-DT-TS-PTF            TO IDSI0011-DATA-COMPETENZA.

      *--> TRATTAMENTO-STORICITA
           SET IDSI0011-TRATT-DEFAULT   TO TRUE.
           SET IDSI0011-ID-OGGETTO      TO TRUE.
      *
      *  --> Nome tabella fisica db
           MOVE 'DETT-TIT-CONT'         TO IDSI0011-CODICE-STR-DATO.
           MOVE DETT-TIT-CONT           TO IDSI0011-BUFFER-DATI.

      *--> TIPO OPERAZIONE
           SET  IDSI0011-FETCH-FIRST     TO TRUE.

      *  --> Modalita di accesso
           SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
           SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC  OR
                         NOT IDSO0011-SUCCESSFUL-SQL
                         OR  IDSV0001-ESITO-KO

             PERFORM CALL-DISPATCHER
                THRU CALL-DISPATCHER-EX

             IF IDSO0011-SUCCESSFUL-RC
                EVALUATE TRUE
                  WHEN IDSO0011-NOT-FOUND
                       IF IDSI0011-FETCH-FIRST
                          MOVE ZERO             TO WS-TAX-TOT
                       END-IF

                  WHEN IDSO0011-SUCCESSFUL-SQL
                       MOVE IDSO0011-BUFFER-DATI
                         TO DETT-TIT-CONT
                       IF DTC-TAX-NULL NOT = HIGH-VALUES
                          ADD DTC-TAX           TO WS-TAX-TOT
                       END-IF
                       SET  IDSI0011-FETCH-NEXT TO TRUE

                  WHEN OTHER
                       MOVE WK-PGM
                         TO IEAI9901-COD-SERVIZIO-BE
                       MOVE 'S1300-TOT-TAX-TITOLO-CONT'
                         TO IEAI9901-LABEL-ERR
                       MOVE '005016'
                         TO IEAI9901-COD-ERRORE
                       STRING 'DETT TIT CONTABILE;'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                         DELIMITED BY SIZE
                         INTO IEAI9901-PARAMETRI-ERR
                       END-STRING
                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
      *
              END-EVALUATE
             ELSE
                MOVE WK-PGM
                  TO IEAI9901-COD-SERVIZIO-BE
                MOVE 'S1300-TOT-TAX-TITOLO-CONT'
                  TO IEAI9901-LABEL-ERR
                MOVE '005016'
                  TO IEAI9901-COD-ERRORE
                STRING 'DETT TIT CONTABILE;'
                       IDSO0011-RETURN-CODE ';'
                       IDSO0011-SQLCODE
                       DELIMITED BY SIZE
                       INTO IEAI9901-PARAMETRI-ERR
                END-STRING
                PERFORM S0300-RICERCA-GRAVITA-ERRORE
                   THRU EX-S0300
             END-IF
           END-PERFORM.

       EX-S1300-TOT.
           EXIT.
ITRFO *----------------------------------------------------------------*
      *    RECUPERO DATI ESTENSIONE POLIZZA CPI E PROTECTION           *
      *----------------------------------------------------------------*
       L320-ACCEDI-EST-POLI-CPI-PR.

           MOVE 'L320-ACCEDI-EST-POLI-CPI-PR'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
                                            IDSI0011-DATA-FINE-EFFETTO
           MOVE IDSV0001-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA

           INITIALIZE IDSI0011-BUFFER-DATI
           INITIALIZE EST-POLI-CPI-PR

           MOVE IDSV0001-TRATTAMENTO-STORICITA
             TO IDSI0011-TRATTAMENTO-STORICITA.

      *--> LIVELLO OPERAZIONE
           SET IDSI0011-ID                TO TRUE

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT            TO TRUE
      *--> ID CHIAVE
           MOVE  WPOL-ID-POLI             TO P67-ID-EST-POLI-CPI-PR

           MOVE 'EST-POLI-CPI-PR'         TO IDSI0011-CODICE-STR-DATO

           MOVE EST-POLI-CPI-PR           TO IDSI0011-BUFFER-DATI.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSO0011-SUCCESSFUL-RC    TO TRUE
           SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE
      *

           PERFORM CALL-DISPATCHER        THRU CALL-DISPATCHER-EX

           IF IDSV0001-ESITO-OK
              IF IDSO0011-SUCCESSFUL-RC

                 EVALUATE TRUE
                     WHEN IDSO0011-SUCCESSFUL-SQL
      *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI  TO EST-POLI-CPI-PR

                       PERFORM VALORIZZA-OUTPUT-P67
                          THRU VALORIZZA-OUTPUT-P67-EX

                       IF WP67-NUM-FINANZ-NULL = HIGH-VALUE
                          CONTINUE
                       ELSE
                          MOVE WP67-NUM-FINANZ
                            TO WB03-NUM-FINANZ
                       END-IF

                     WHEN IDSO0011-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA

                       MOVE WK-PGM  TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR TO IEAI9901-LABEL-ERR
                       MOVE '005016' TO IEAI9901-COD-ERRORE
                       MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                       STRING PGM-IDBSP670         ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                       END-STRING

                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
                     WHEN OTHER
      *--->   ERRORE DI ACCESSO AL DB

                       MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR      TO IEAI9901-LABEL-ERR
                       MOVE '005016'      TO IEAI9901-COD-ERRORE
                       MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                       STRING PGM-IDBSP670         ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                       END-STRING

                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300

                 END-EVALUATE
              ELSE
      *-->    GESTIRE ERRORE
                 MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR       TO IEAI9901-LABEL-ERR
                 MOVE '005016'           TO IEAI9901-COD-ERRORE
                 MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                 STRING PGM-IDBSP670         ';'
                        IDSO0011-RETURN-CODE ';'
                        IDSO0011-SQLCODE
                        DELIMITED BY SIZE
                        INTO IEAI9901-PARAMETRI-ERR
                 END-STRING

                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           ELSE
      *-->    GESTIRE ERRORE
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR       TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              MOVE SPACES TO IEAI9901-PARAMETRI-ERR
              STRING PGM-IDBSP670         ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING

              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

       L320-EX.
ITRFO      EXIT.
ITRFO *----------------------------------------------------------------*
      *    RECUPERO DATI ACCORDO COMMERCIALE
      *----------------------------------------------------------------*
       L310-ACCEDI-ACC-COMM.

           MOVE 'L310-ACCEDI-ACC-COMM'      TO WK-LABEL-ERR.
           SET IDSV8888-DEBUG-MEDIO         TO TRUE
           MOVE WK-LABEL-ERR                TO IDSV8888-AREA-DISPLAY.
           PERFORM ESEGUI-DISPLAY  THRU ESEGUI-DISPLAY-EX.

           INITIALIZE ACC-COMM

           INITIALIZE IDSI0011-BUFFER-DATI

           MOVE WPOL-ID-ACC-COMM          TO P63-ID-ACC-COMM

           SET IDSI0011-TRATT-SENZA-STOR  TO TRUE.

      *--> LIVELLO OPERAZIONE
           SET IDSI0011-PRIMARY-KEY       TO TRUE

      *--> TIPO OPERAZIONE
           SET IDSI0011-SELECT            TO TRUE

           MOVE 'ACC-COMM'                TO IDSI0011-CODICE-STR-DATO

           MOVE ACC-COMM                  TO IDSI0011-BUFFER-DATI.

      *--> INIZIALIZZA CODICE DI RITORNO
           SET  IDSO0011-SUCCESSFUL-RC    TO TRUE
           SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE
      *


           PERFORM CALL-DISPATCHER        THRU CALL-DISPATCHER-EX

           IF IDSV0001-ESITO-OK
              IF IDSO0011-SUCCESSFUL-RC

                 EVALUATE TRUE
                     WHEN IDSO0011-SUCCESSFUL-SQL
      *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                       MOVE IDSO0011-BUFFER-DATI  TO ACC-COMM

                       PERFORM VALORIZZA-OUTPUT-P63
                          THRU VALORIZZA-OUTPUT-P63-EX

                       MOVE WP63-TP-ACC-COMM
                         TO WB03-TP-ACC-COMM

10829                  IF WB03-TP-ACC-COMM = 'C'
                          MOVE WP63-IB-ACC-COMM
                            TO WB03-IB-ACC-COMM
10829                  END-IF

                     WHEN IDSO0011-NOT-FOUND
      *-->    NESSUN DATO IN TABELLA
                       MOVE WK-PGM  TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR TO IEAI9901-LABEL-ERR
                       MOVE '005016' TO IEAI9901-COD-ERRORE
                       MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                       STRING PGM-IDBSP630         ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                       END-STRING

                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                          THRU EX-S0300
                     WHEN OTHER
      *--->      ERRORE DI ACCESSO AL DB

                       MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                       MOVE WK-LABEL-ERR  TO IEAI9901-LABEL-ERR
                       MOVE '005016'      TO IEAI9901-COD-ERRORE
                       MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                       STRING PGM-IDBSP630         ';'
                              IDSO0011-RETURN-CODE ';'
                              IDSO0011-SQLCODE
                              DELIMITED BY SIZE
                              INTO IEAI9901-PARAMETRI-ERR
                       END-STRING

                       PERFORM S0300-RICERCA-GRAVITA-ERRORE
                               THRU EX-S0300

                 END-EVALUATE
              ELSE
      *-->    GESTIRE ERRORE
                 MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
                 MOVE WK-LABEL-ERR       TO IEAI9901-LABEL-ERR
                 MOVE '005016'           TO IEAI9901-COD-ERRORE
                 MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                 STRING PGM-IDBSP630         ';'
                        IDSO0011-RETURN-CODE ';'
                        IDSO0011-SQLCODE
                        DELIMITED BY SIZE
                        INTO IEAI9901-PARAMETRI-ERR
                 END-STRING

                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF
           ELSE
      *-->    GESTIRE ERRORE
              MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
              MOVE WK-LABEL-ERR       TO IEAI9901-LABEL-ERR
              MOVE '005016'           TO IEAI9901-COD-ERRORE
              MOVE SPACES TO IEAI9901-PARAMETRI-ERR
              STRING PGM-IDBSP630         ';'
                     IDSO0011-RETURN-CODE ';'
                     IDSO0011-SQLCODE
                     DELIMITED BY SIZE
                     INTO IEAI9901-PARAMETRI-ERR
              END-STRING

              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                 THRU EX-S0300
           END-IF.

ITRFO  L310-EX.
           EXIT.



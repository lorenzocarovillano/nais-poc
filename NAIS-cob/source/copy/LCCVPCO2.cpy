      *----------------------------------------------------------------*
      *   PORTAFOGLIO VITA - PROCESSO VENDITA                          *
      *   CHIAMATA AL DISPATCHER PARAM-COMP                            *
      *   ULTIMO AGG.  01 MARZO 2007                                   *
      *----------------------------------------------------------------*

      *----------------------------------------------------------------*
      *               GESTIONE LETTURA (SELECT/FETCH)                  *
      *----------------------------------------------------------------*
       LETTURA-PCO.

           IF IDSI0011-SELECT
              PERFORM SELECT-PCO
                 THRU SELECT-PCO-EX
           ELSE
              PERFORM FETCH-PCO
                 THRU FETCH-PCO-EX
           END-IF

           MOVE IX-TAB-PCO
             TO (SF)-ELE-PAR-COMP-MAX.

       LETTURA-PCO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         SELECT                                 *
      *----------------------------------------------------------------*
       SELECT-PCO.

           PERFORM CALL-DISPATCHER
               THRU CALL-DISPATCHER-EX.

              IF IDSO0011-SUCCESSFUL-RC
                 EVALUATE TRUE
                     WHEN IDSO0011-SUCCESSFUL-SQL
      *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                        MOVE IDSO0011-BUFFER-DATI
                          TO PARAM-COMP
                        MOVE 1               TO IX-TAB-PCO
                        PERFORM VALORIZZA-OUTPUT-PCO
                           THRU VALORIZZA-OUTPUT-PCO-EX

                     WHEN IDSO0011-NOT-FOUND
      *--->          CHIAVE NON TROVATA
                          MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                          MOVE 'SELECT-PCO'  TO IEAI9901-LABEL-ERR
                          MOVE '005017'      TO IEAI9901-COD-ERRORE
                          MOVE SPACES        TO IEAI9901-PARAMETRI-ERR
                          PERFORM S0300-RICERCA-GRAVITA-ERRORE
                             THRU EX-S0300
                     WHEN OTHER
      *--->          ERRORE DI ACCESSO AL DB
                          MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                          MOVE 'SELECT-PCO'  TO IEAI9901-LABEL-ERR
                          MOVE '005016'      TO IEAI9901-COD-ERRORE
                          STRING 'PARAM-COMP'         ';'
                                 IDSO0011-RETURN-CODE ';'
                                 IDSO0011-SQLCODE
                          DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                          END-STRING
                          PERFORM S0300-RICERCA-GRAVITA-ERRORE
                             THRU EX-S0300
                 END-EVALUATE
              ELSE
      *-->       GESTIRE ERRORE DISPATCHER
                 MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                 MOVE 'SELECT-PCO'  TO IEAI9901-LABEL-ERR
                 MOVE '005016'      TO IEAI9901-COD-ERRORE
                 STRING 'PARAM-COMP'          ';'
                         IDSO0011-RETURN-CODE ';'
                         IDSO0011-SQLCODE
                 DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                 END-STRING
                 PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    THRU EX-S0300
              END-IF.

       SELECT-PCO-EX.
           EXIT.

      *----------------------------------------------------------------*
      *                         FETCH                                  *
      *----------------------------------------------------------------*
       FETCH-PCO.

           SET  IDSO0011-SUCCESSFUL-RC            TO TRUE.
           SET  IDSO0011-SUCCESSFUL-SQL           TO TRUE.
           SET  WCOM-OVERFLOW-NO                  TO TRUE.

           PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
                      OR NOT IDSO0011-SUCCESSFUL-SQL
                      OR WCOM-OVERFLOW-YES

               PERFORM CALL-DISPATCHER
                  THRU CALL-DISPATCHER-EX

               IF IDSO0011-SUCCESSFUL-RC
                  EVALUATE TRUE
                      WHEN IDSO0011-NOT-FOUND
      *-->            CHIAVE NON TROVATA
                           IF IDSI0011-FETCH-FIRST
                              MOVE WK-PGM
                                TO IEAI9901-COD-SERVIZIO-BE
                              MOVE 'FETCH-PCO'
                                TO IEAI9901-LABEL-ERR
                              MOVE '005017'
                                TO IEAI9901-COD-ERRORE
                              MOVE SPACES
                                TO IEAI9901-PARAMETRI-ERR
                              PERFORM S0300-RICERCA-GRAVITA-ERRORE
                                 THRU EX-S0300
                           END-IF
                      WHEN IDSO0011-SUCCESSFUL-SQL
      *-->            OPERAZIONE ESEGUITA CORRETTAMENTE
                            MOVE IDSO0011-BUFFER-DATI  TO PARAM-COMP
                            ADD  1                     TO IX-TAB-PCO
                            IF IX-TAB-PCO > (SF)-ELE-PAR-COMP-MAX
                               SET WCOM-OVERFLOW-YES   TO TRUE
                            ELSE
                               PERFORM VALORIZZA-OUTPUT-PCO
                                  THRU VALORIZZA-OUTPUT-PCO-EX
                               SET IDSI0011-FETCH-NEXT TO TRUE
                            END-IF
                      WHEN OTHER
      *-->            ERRORE DI ACCESSO AL DB
                           MOVE WK-PGM       TO IEAI9901-COD-SERVIZIO-BE
                           MOVE 'FETCH-PCO'  TO IEAI9901-LABEL-ERR
                           MOVE '005016'     TO IEAI9901-COD-ERRORE
                           STRING 'PARAM-COMP'         ';'
                                  IDSO0011-RETURN-CODE ';'
                                  IDSO0011-SQLCODE
                           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                           END-STRING
                           PERFORM S0300-RICERCA-GRAVITA-ERRORE
                              THRU EX-S0300
                  END-EVALUATE
               ELSE
      *-->        ERRORE DISPATCHER
                  MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                  MOVE 'FETCH-PCO'   TO IEAI9901-LABEL-ERR
                  MOVE '005016'      TO IEAI9901-COD-ERRORE
                  STRING 'PARAM-COMP'         ';'
                          IDSO0011-RETURN-CODE ';'
                          IDSO0011-SQLCODE
                  DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                  END-STRING
                  PERFORM S0300-RICERCA-GRAVITA-ERRORE
                     THRU EX-S0300
               END-IF
           END-PERFORM.

       FETCH-PCO-EX.
           EXIT.


      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   INIZIALIZZAZIONE CAMPI NULL COPY LCCVNOT4
      *   ULTIMO AGG. 02 SET 2008
      *------------------------------------------------------------

       INIZIA-TOT-NOT.

           PERFORM INIZIA-ZEROES-NOT THRU INIZIA-ZEROES-NOT-EX

           PERFORM INIZIA-SPACES-NOT THRU INIZIA-SPACES-NOT-EX

           PERFORM INIZIA-NULL-NOT THRU INIZIA-NULL-NOT-EX.

       INIZIA-TOT-NOT-EX.
           EXIT.

       INIZIA-NULL-NOT.
           MOVE HIGH-VALUES TO (SF)-NOTA-OGG.
       INIZIA-NULL-NOT-EX.
           EXIT.

       INIZIA-ZEROES-NOT.
           MOVE 0 TO (SF)-ID-NOTE-OGG
           MOVE 0 TO (SF)-COD-COMP-ANIA
           MOVE 0 TO (SF)-ID-OGG
           MOVE 0 TO (SF)-DS-VER
           MOVE 0 TO (SF)-DS-TS-CPTZ.
       INIZIA-ZEROES-NOT-EX.
           EXIT.

       INIZIA-SPACES-NOT.
           MOVE SPACES TO (SF)-TP-OGG
           MOVE SPACES TO (SF)-DS-OPER-SQL
           MOVE SPACES TO (SF)-DS-UTENTE
           MOVE SPACES TO (SF)-DS-STATO-ELAB.
       INIZIA-SPACES-NOT-EX.
           EXIT.

      *------------------------------------------------------------
      *   PORTAFOGLIO VITA
      *   AREA RICH
      *   ALIAS RIC
      *   ULTIMO AGG. 21 NOV 2013
      *------------------------------------------------------------
           05 (SF)-STATUS                    PIC X(001).
             88 (SF)-ST-ADD                     VALUE 'A'.
             88 (SF)-ST-MOD                     VALUE 'M'.
             88 (SF)-ST-INV                     VALUE 'I'.
             88 (SF)-ST-DEL                     VALUE 'D'.
             88 (SF)-ST-CON                     VALUE 'C'.
           05 (SF)-ID-PTF                   PIC S9(9)    COMP-3.
           05 (SF)-DATI.
             07 (SF)-ID-RICH PIC S9(9)     COMP-3.
             07 (SF)-COD-COMP-ANIA PIC S9(5)     COMP-3.
             07 (SF)-TP-RICH PIC X(2).
             07 (SF)-COD-MACROFUNCT PIC X(2).
             07 (SF)-TP-MOVI PIC S9(5)     COMP-3.
             07 (SF)-IB-RICH PIC X(40).
             07 (SF)-IB-RICH-NULL REDEFINES
                (SF)-IB-RICH   PIC X(40).
             07 (SF)-DT-EFF   PIC S9(8) COMP-3.
             07 (SF)-DT-RGSTRZ-RICH   PIC S9(8) COMP-3.
             07 (SF)-DT-PERV-RICH   PIC S9(8) COMP-3.
             07 (SF)-DT-ESEC-RICH   PIC S9(8) COMP-3.
             07 (SF)-TS-EFF-ESEC-RICH PIC S9(18)     COMP-3.
             07 (SF)-TS-EFF-ESEC-RICH-NULL REDEFINES
                (SF)-TS-EFF-ESEC-RICH   PIC X(10).
             07 (SF)-ID-OGG PIC S9(9)     COMP-3.
             07 (SF)-ID-OGG-NULL REDEFINES
                (SF)-ID-OGG   PIC X(5).
             07 (SF)-TP-OGG PIC X(2).
             07 (SF)-TP-OGG-NULL REDEFINES
                (SF)-TP-OGG   PIC X(2).
             07 (SF)-IB-POLI PIC X(40).
             07 (SF)-IB-POLI-NULL REDEFINES
                (SF)-IB-POLI   PIC X(40).
             07 (SF)-IB-ADES PIC X(40).
             07 (SF)-IB-ADES-NULL REDEFINES
                (SF)-IB-ADES   PIC X(40).
             07 (SF)-IB-GAR PIC X(40).
             07 (SF)-IB-GAR-NULL REDEFINES
                (SF)-IB-GAR   PIC X(40).
             07 (SF)-IB-TRCH-DI-GAR PIC X(40).
             07 (SF)-IB-TRCH-DI-GAR-NULL REDEFINES
                (SF)-IB-TRCH-DI-GAR   PIC X(40).
             07 (SF)-ID-BATCH PIC S9(9)     COMP-3.
             07 (SF)-ID-BATCH-NULL REDEFINES
                (SF)-ID-BATCH   PIC X(5).
             07 (SF)-ID-JOB PIC S9(9)     COMP-3.
             07 (SF)-ID-JOB-NULL REDEFINES
                (SF)-ID-JOB   PIC X(5).
             07 (SF)-FL-SIMULAZIONE PIC X(1).
             07 (SF)-FL-SIMULAZIONE-NULL REDEFINES
                (SF)-FL-SIMULAZIONE   PIC X(1).
             07 (SF)-KEY-ORDINAMENTO PIC X(300).
             07 (SF)-ID-RICH-COLLG PIC S9(9)     COMP-3.
             07 (SF)-ID-RICH-COLLG-NULL REDEFINES
                (SF)-ID-RICH-COLLG   PIC X(5).
             07 (SF)-TP-RAMO-BILA PIC X(2).
             07 (SF)-TP-RAMO-BILA-NULL REDEFINES
                (SF)-TP-RAMO-BILA   PIC X(2).
             07 (SF)-TP-FRM-ASSVA PIC X(2).
             07 (SF)-TP-FRM-ASSVA-NULL REDEFINES
                (SF)-TP-FRM-ASSVA   PIC X(2).
             07 (SF)-TP-CALC-RIS PIC X(2).
             07 (SF)-TP-CALC-RIS-NULL REDEFINES
                (SF)-TP-CALC-RIS   PIC X(2).
             07 (SF)-DS-OPER-SQL PIC X(1).
             07 (SF)-DS-VER PIC S9(9)     COMP-3.
             07 (SF)-DS-TS-CPTZ PIC S9(18)     COMP-3.
             07 (SF)-DS-UTENTE PIC X(20).
             07 (SF)-DS-STATO-ELAB PIC X(1).
             07 (SF)-RAMO-BILA PIC X(12).
             07 (SF)-RAMO-BILA-NULL REDEFINES
                (SF)-RAMO-BILA   PIC X(12).

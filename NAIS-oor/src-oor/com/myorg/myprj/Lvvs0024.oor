package com.myorg.myprj;

import java.lang.Override;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import com.myorg.myprj.ws.Idsv0003;
import com.myorg.myprj.ws.Ivvc0213;
import com.myorg.myprj.ws.Lvvs0024Data;

/**Original name: LVVS0024<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0024
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
 * **------------------------------------------------------------***</pre>*/
class Lvvs0024 extends Program {

	//==== PROPERTIES ====
	//Original name: WORKING-STORAGE
	private Lvvs0024Data ws := new Lvvs0024Data();
	//Original name: IDSV0003
	private Idsv0003 idsv0003;
	//Original name: INPUT-LVVS0024
	private Ivvc0213 ivvc0213;


	//==== METHODS ====
	/**Original name: PROGRAM_LVVS0024_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
	public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
		this.idsv0003 := idsv0003;
		this.ivvc0213 := ivvc0213;
		// COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
		//              THRU EX-S0000.
		s0000OperazioniIniziali();
		// COB_CODE: PERFORM S1000-ELABORAZIONE
		//              THRU EX-S1000
		s1000Elaborazione();
		// COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
		//              THRU EX-S9000.
		s9000OperazioniFinali();
		return 0;
	}

	public static Lvvs0024 getInstance() {
		return (Lvvs0024)Programs.getInstance(Lvvs0024.clazz);
	}

	/**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
	private void s0000OperazioniIniziali() {
		// COB_CODE: INITIALIZE                        IX-INDICI
		//                                             IVVC0213-TAB-OUTPUT
		//                                             WK-VAR-APPO
		//                                             AREA-IO-TRANCHE.
		initIxIndici();
		initTabOutput();
		initWkVarAppo();
		initAreaIoTranche();
		// COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
		idsv0003.getSqlcode().setSuccessfulSql();
		// COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
		idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
		// COB_CODE: MOVE IVVC0213-AREA-VARIABILE
		//             TO IVVC0213-TAB-OUTPUT.
		ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
	}

	/**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
	private void s1000Elaborazione() {
// COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
//              THRU S1100-VALORIZZA-DCLGEN-EX
//           VARYING IX-DCLGEN FROM 1 BY 1
//             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
//                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
//                   SPACES OR LOW-VALUE OR HIGH-VALUE
		ws.setIxDclgen((short)1);
		do
		while (! (ws.getIxDclgen() > ivvc0213.getEleInfoMax() | Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) | Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) | Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted())))
			s1100ValorizzaDclgen();
			ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
		enddo
			//--> PERFORM DI CONTROLLI SUI I CAMPI CARICATI NELLE
			//--> DCLGEN DI WORKING
		// COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
		//           AND IDSV0003-SUCCESSFUL-SQL
		//                  THRU S1200-CONTROLLO-DATI-EX
		//           END-IF
		if (idsv0003.getReturnCode().isSuccessfulRc() & idsv0003.getSqlcode().isSuccessfulSql()) then
			// COB_CODE: PERFORM S1200-CONTROLLO-DATI
			//              THRU S1200-CONTROLLO-DATI-EX
			s1200ControlloDati();
		endif
		// COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
		//                AND IDSV0003-SUCCESSFUL-SQL
		//           *--> CALL MODULO PER RECUPERO DATA
		//                       THRU S1300-TIPO-TRANCHE-EX
		//                END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc() & idsv0003.getSqlcode().isSuccessfulSql()) then
				//--> CALL MODULO PER RECUPERO DATA
			// COB_CODE: PERFORM S1300-TIPO-TRANCHE
			//              THRU S1300-TIPO-TRANCHE-EX
			s1300TipoTranche();
		endif
	}

	/**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
	private void s1100ValorizzaDclgen() {
		// COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
		//              IVVC0218-ALIAS-TRCH-GAR
		//                TO DTGA-AREA-TRANCHE
		//           END-IF.
		if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasTrchGar())) then
			// COB_CODE: MOVE IVVC0213-BUFFER-DATI
			//               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
			//                IVVC0213-LUNGHEZZA(IX-DCLGEN))
			//             TO DTGA-AREA-TRANCHE
			ws.setDtgaAreaTrancheFormatted(ivvc0213.getBufferDatiFormatted()[ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni():ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1]);
		endif
	}

	/**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
	private void s1200ControlloDati() {
		// COB_CODE: IF DTGA-DUR-AA(IVVC0213-IX-TABB)  NOT NUMERIC
		//           AND DTGA-DUR-MM(IVVC0213-IX-TABB) NOT NUMERIC
		//                TO IDSV0003-DESCRIZ-ERR-DB2
		//           END-IF.
		if (! Functions.isNumber(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDurAa().getWtgaDurAa()) & ! Functions.isNumber(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDurMm().getWtgaDurMm())) then
			// COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
			idsv0003.getReturnCode().setFieldNotValued();
			// COB_CODE: MOVE WK-PGM
			//             TO IDSV0003-COD-SERVIZIO-BE
			idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
			// COB_CODE: MOVE 'DATO-NON NUMERICO'
			//             TO IDSV0003-DESCRIZ-ERR-DB2
			idsv0003.getCampiEsito().setDescrizErrDb2("DATO-NON NUMERICO");
		endif
		// COB_CODE: IF DTGA-DUR-AA(IVVC0213-IX-TABB)  NUMERIC
		//           AND DTGA-DUR-MM(IVVC0213-IX-TABB) NUMERIC
		//             END-IF
		//           END-IF.
		if (Functions.isNumber(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDurAa().getWtgaDurAa()) & Functions.isNumber(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDurMm().getWtgaDurMm())) then
			// COB_CODE: IF DTGA-DUR-AA(IVVC0213-IX-TABB) EQUAL ZEROES
			//           AND DTGA-DUR-MM(IVVC0213-IX-TABB) EQUAL ZEROES
			//                TO IDSV0003-DESCRIZ-ERR-DB2
			//           END-IF
			if (ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDurAa().getWtgaDurAa() = 0 & ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDurMm().getWtgaDurMm() = 0) then
				// COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
				idsv0003.getReturnCode().setFieldNotValued();
				// COB_CODE: MOVE WK-PGM
				//             TO IDSV0003-COD-SERVIZIO-BE
				idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
				// COB_CODE: MOVE 'DATO-NON NUMERICO'
				//             TO IDSV0003-DESCRIZ-ERR-DB2
				idsv0003.getCampiEsito().setDescrizErrDb2("DATO-NON NUMERICO");
			endif
		endif
	}

	/**Original name: S1300-TIPO-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *    GESTIONE TIPO TRANCHE DI GARANZIA
	 * ----------------------------------------------------------------*
	 * --> TEST TIPO LIVELLO</pre>*/
	private void s1300TipoTranche() {
		// COB_CODE: IF IVVC0213-TP-LIVELLO = 'G'
		//                 THRU S2027-EX
		//           END-IF.
		if (ivvc0213.getDatiLivello().getTpLivello() = 'G') then
			// COB_CODE: PERFORM S2027-TRANCHE
			//              THRU S2027-EX
			s2027Tranche();
		endif
	}

	/**Original name: S2027-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *   ELABORA  TRANCHE DI GAR
	 * ----------------------------------------------------------------*</pre>*/
	private void s2027Tranche() {
		// COB_CODE: IF DTGA-DUR-AA(IVVC0213-IX-TABB) IS NUMERIC
		//              MOVE DTGA-DUR-AA(IVVC0213-IX-TABB)  TO WK-DURA-AA
		//           ELSE
		//              MOVE ZEROES                         TO WK-DURA-AA
		//           END-IF.
		if (Functions.isNumber(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDurAa().getWtgaDurAa())) then
			// COB_CODE: MOVE DTGA-DUR-AA(IVVC0213-IX-TABB)  TO WK-DURA-AA
			ws.setWkDuraAa(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDurAa().getWtgaDurAa());
		else
			// COB_CODE: MOVE ZEROES                         TO WK-DURA-AA
			ws.setWkDuraAa(0);
		endif
		// COB_CODE: IF DTGA-DUR-MM(IVVC0213-IX-TABB) IS NUMERIC
		//              MOVE DTGA-DUR-MM(IVVC0213-IX-TABB)  TO WK-DURA-MM
		//           ELSE
		//              MOVE ZEROES                         TO WK-DURA-MM
		//           END-IF.
		if (Functions.isNumber(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDurMm().getWtgaDurMm())) then
			// COB_CODE: MOVE DTGA-DUR-MM(IVVC0213-IX-TABB)  TO WK-DURA-MM
			ws.setWkDuraMm(ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDurMm().getWtgaDurMm());
		else
			// COB_CODE: MOVE ZEROES                         TO WK-DURA-MM
			ws.setWkDuraMm(0);
		endif
			//
		// COB_CODE: COMPUTE WK-APPO-MESI = (WK-DURA-MM * 30).
		ws.setWkAppoMesi(abs(ws.getWkDuraMm() * 30));
		// COB_CODE: COMPUTE WK-APPO-ANNI = (WK-DURA-AA * 360).
		ws.setWkAppoAnni(abs(ws.getWkDuraAa() * 360));
			//
		// COB_CODE: COMPUTE IVVC0213-VAL-IMP-O =
		//                   ((WK-APPO-ANNI + WK-APPO-MESI) / 360).
		ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal((decimal(17,7))(((double)(ws.getWkAppoAnni() + ws.getWkAppoMesi())) / 360), 18, 7));
	}

	/**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
	private void s9000OperazioniFinali() {
		// COB_CODE: MOVE SPACES                     TO IVVC0213-VAL-STR-O.
		ivvc0213.getTabOutput().setValStrO("");
		// COB_CODE: MOVE 0                          TO IVVC0213-VAL-PERC-O.
		ivvc0213.getTabOutput().setValPercO(Trunc.toDecimal(0, 14, 9));
			//
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

	public void initIxIndici() {
		ws.setIxDclgen(0);
		ws.setIxTabTga(0);
	}

	public void initTabOutput() {
		ivvc0213.getTabOutput().setCodVariabileO("");
		ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
		ivvc0213.getTabOutput().setValImpO(0);
		ivvc0213.getTabOutput().setValPercO(0);
		ivvc0213.getTabOutput().setValStrO("");
	}

	public void initWkVarAppo() {
		ws.setWkDuraAa(0);
		ws.setWkDuraMm(0);
	}

	public void initAreaIoTranche() {
		ws.setDtgaEleTgaMax(0);
		for integer idx0 in 1.. Lvvs0024Data.DTGA_TAB_TRAN_MAXOCCURS 
		do
			ws.getDtgaTabTran(idx0).getLccvtga1().getStatus().setStatus(Types.SPACE_CHAR);
			ws.getDtgaTabTran(idx0).getLccvtga1().setIdPtf(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdTrchDiGar(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdGar(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdAdes(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdPoli(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdMoviCrz(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiu(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtIniEff(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtEndEff(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaCodCompAnia(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtDecor(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScad(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIbOgg("");
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpRgmFisc("");
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmis(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpTrch("");
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAa(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMm(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGg(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMor(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiat(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAntic(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNet(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIni(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUlt(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIni(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUlt(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIni(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUlt(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivto(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProf(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSan(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpo(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTec(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSopr(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStab(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStab(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFis(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndiciz(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTec(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrd(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrd(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIni(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUlt(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivto(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStab(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMor(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIni(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlCarCont(Types.SPACE_CHAR);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqto(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBns(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaCodDvs("");
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfis(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpScon(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqScon(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcq(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarInc(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGest(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAssto(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAssto(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAssto(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAssto(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAssto(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAssto(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetr(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetr(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGarto(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnut(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrch(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIni(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUlt(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUlt(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbb(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpAdegAbb(Types.SPACE_CHAR);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaModCalc("");
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAz(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAder(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfr(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVolo(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProd(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTar(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPre(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlImportiForz(Types.SPACE_CHAR);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforz(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000Nforz(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMora(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAntic(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivto(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcq(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcq(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicor(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvInc(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcq(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvInc(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicor(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcq(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvInc(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicor(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlProvForz(Types.SPACE_CHAR);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIni(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPre(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstz(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePr(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUlt(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNet(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuito(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpRival("");
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMat(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScad(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGest(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpManfeeAppl("");
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsRiga(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsOperSql(Types.SPACE_CHAR);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsVer(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsIniCptz(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsEndCptz(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsUtente("");
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsStatoElab(Types.SPACE_CHAR);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGest(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRival(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfe(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrc(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExp(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAss(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInter(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAss(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInter(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAss(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInter(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(0);
			ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdc(0);
		enddo
	}

}//Lvvs0024
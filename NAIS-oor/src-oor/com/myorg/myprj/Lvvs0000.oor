package com.myorg.myprj;

import java.lang.Override;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import com.myorg.myprj.ws.Idsv0003;
import com.myorg.myprj.ws.InputLvvs0000;
import com.myorg.myprj.ws.Lvvs0000Data;
import com.myorg.myprj.ws.enums.Lvvc0000FormatDate;

/**Original name: LVVS0000<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0000
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CALCOLO DATA
 * **------------------------------------------------------------***</pre>*/
class Lvvs0000 extends Program {

	//==== PROPERTIES ====
	//Original name: WORKING-STORAGE
	private Lvvs0000Data ws := new Lvvs0000Data();
	//Original name: IDSV0003
	private Idsv0003 idsv0003;
	//Original name: INPUT-LVVS0000
	private InputLvvs0000 inputLvvs0000;


	//==== METHODS ====
	/**Original name: PROGRAM_LVVS0000_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
	public long execute(Idsv0003 idsv0003, InputLvvs0000 inputLvvs0000) {
		this.idsv0003 := idsv0003;
		this.inputLvvs0000 := inputLvvs0000;
		// COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
		//              THRU EX-S0000.
		s0000OperazioniIniziali();
		// COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
		//           AND IDSV0003-SUCCESSFUL-SQL
		//                  THRU EX-S1000
		//           END-IF.
		if (this.idsv0003.getReturnCode().isSuccessfulRc() & this.idsv0003.getSqlcode().isSuccessfulSql()) then
			// COB_CODE: PERFORM S1000-ELABORAZIONE
			//              THRU EX-S1000
			s1000Elaborazione();
		endif
		// COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
		//              THRU EX-S9000.
		s9000OperazioniFinali();
		return 0;
	}

	public static Lvvs0000 getInstance() {
		return (Lvvs0000)Programs.getInstance(Lvvs0000.clazz);
	}

	/**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
	private void s0000OperazioniIniziali() {
		// COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
		idsv0003.getSqlcode().setSuccessfulSql();
		// COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
		idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
			//
		// COB_CODE: IF LVVC0000-FORMAT-DATE EQUAL SPACES
		//           OR LVVC0000-FORMAT-DATE EQUAL LOW-VALUE
		//           OR LVVC0000-FORMAT-DATE EQUAL HIGH-VALUE
		//              SET LVVC0000-AAA-V-GGG         TO TRUE
		//           END-IF.
		if (Conditions.eq(inputLvvs0000.getFormatDate().getFormatDate(), Types.SPACE_CHAR) | Conditions.eq(inputLvvs0000.getFormatDate().getFormatDate(), Types.LOW_CHAR_VAL) | Conditions.eq(inputLvvs0000.getFormatDate().getFormatDate(), Types.HIGH_CHAR_VAL)) then
			// COB_CODE: SET LVVC0000-AAA-V-GGG         TO TRUE
			inputLvvs0000.getFormatDate().setAaaVGgg();
		endif
	}

	/**Original name: S0005-CTRL-DATI-INPUT-1<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLO DATI INPUT FORRMATO 1
	 * ----------------------------------------------------------------*</pre>*/
	private void s0005CtrlDatiInput1() {
		// COB_CODE: IF LVVC0000-DATA-INPUT-1 NOT NUMERIC
		//                TO IDSV0003-DESCRIZ-ERR-DB2
		//           END-IF.
		if (! Functions.isNumber(inputLvvs0000.getDataInput1Formatted())) then
			// COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
			idsv0003.getReturnCode().setFieldNotValued();
			// COB_CODE: MOVE WK-PGM
			//             TO IDSV0003-COD-SERVIZIO-BE
			idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
			// COB_CODE: MOVE 'DATA-INPUT NON NUMERICA'
			//             TO IDSV0003-DESCRIZ-ERR-DB2
			idsv0003.getCampiEsito().setDescrizErrDb2("DATA-INPUT NON NUMERICA");
		endif
		// COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
		//           AND IDSV0003-SUCCESSFUL-SQL
		//               END-IF
		//           END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc() & idsv0003.getSqlcode().isSuccessfulSql()) then
			// COB_CODE: IF LVVC0000-DATA-INPUT-1 = ZERO
			//                TO IDSV0003-DESCRIZ-ERR-DB2
			//           END-IF
			if (Characters.EQ_ZERO.test(inputLvvs0000.getDataInput1Formatted())) then
				// COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
				idsv0003.getReturnCode().setFieldNotValued();
				// COB_CODE: MOVE WK-PGM
				//             TO IDSV0003-COD-SERVIZIO-BE
				idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
				// COB_CODE: MOVE 'DATA-INPUT NON VALORIZZATA'
				//             TO IDSV0003-DESCRIZ-ERR-DB2
				idsv0003.getCampiEsito().setDescrizErrDb2("DATA-INPUT NON VALORIZZATA");
			endif
		endif
	}

	/**Original name: S0010-CTRL-DATI-INPUT-2<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLO DATI INPUT FORMATO 2
	 * ----------------------------------------------------------------*</pre>*/
	private void s0010CtrlDatiInput2() {
		// COB_CODE: IF LVVC0000-DATA-INPUT-1 NOT NUMERIC
		//                TO IDSV0003-DESCRIZ-ERR-DB2
		//           END-IF.
		if (! Functions.isNumber(inputLvvs0000.getDataInput1Formatted())) then
			// COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
			idsv0003.getReturnCode().setFieldNotValued();
			// COB_CODE: MOVE WK-PGM
			//             TO IDSV0003-COD-SERVIZIO-BE
			idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
			// COB_CODE: MOVE 'DATA-INPUT-2 NON NUMERICA'
			//             TO IDSV0003-DESCRIZ-ERR-DB2
			idsv0003.getCampiEsito().setDescrizErrDb2("DATA-INPUT-2 NON NUMERICA");
		endif
		// COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
		//           AND IDSV0003-SUCCESSFUL-SQL
		//               END-IF
		//           END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc() & idsv0003.getSqlcode().isSuccessfulSql()) then
			// COB_CODE: IF LVVC0000-DATA-INPUT-1 = ZERO
			//                TO IDSV0003-DESCRIZ-ERR-DB2
			//           END-IF
			if (Characters.EQ_ZERO.test(inputLvvs0000.getDataInput1Formatted())) then
				// COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED TO TRUE
				idsv0003.getReturnCode().setFieldNotValued();
				// COB_CODE: MOVE WK-PGM
				//             TO IDSV0003-COD-SERVIZIO-BE
				idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
				// COB_CODE: MOVE 'DATA-INPUT-2 NON VALORIZZATA'
				//             TO IDSV0003-DESCRIZ-ERR-DB2
				idsv0003.getCampiEsito().setDescrizErrDb2("DATA-INPUT-2 NON VALORIZZATA");
			endif
		endif
	}

	/**Original name: S0015-CTRL-DATI-INPUT-3<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLO DATI INPUT FORMATO 3
	 * ----------------------------------------------------------------*</pre>*/
	private void s0015CtrlDatiInput3() {
		// COB_CODE: IF LVVC0000-DATA-INPUT-1 NOT NUMERIC
		//                TO IDSV0003-DESCRIZ-ERR-DB2
		//           END-IF.
		if (! Functions.isNumber(inputLvvs0000.getDataInput1Formatted())) then
			// COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
			idsv0003.getReturnCode().setFieldNotValued();
			// COB_CODE: MOVE WK-PGM
			//             TO IDSV0003-COD-SERVIZIO-BE
			idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
			// COB_CODE: MOVE 'ANNI-INPUT-2 NON NUMERICA'
			//             TO IDSV0003-DESCRIZ-ERR-DB2
			idsv0003.getCampiEsito().setDescrizErrDb2("ANNI-INPUT-2 NON NUMERICA");
		endif
		// COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
		//           AND IDSV0003-SUCCESSFUL-SQL
		//               END-IF
		//           END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc() & idsv0003.getSqlcode().isSuccessfulSql()) then
			// COB_CODE: IF LVVC0000-DATA-INPUT-1 = ZERO
			//                TO IDSV0003-DESCRIZ-ERR-DB2
			//           END-IF
			if (Characters.EQ_ZERO.test(inputLvvs0000.getDataInput1Formatted())) then
				// COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED TO TRUE
				idsv0003.getReturnCode().setFieldNotValued();
				// COB_CODE: MOVE WK-PGM
				//             TO IDSV0003-COD-SERVIZIO-BE
				idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
				// COB_CODE: MOVE 'ANNI-INPUT-2 NON VALORIZZATA'
				//             TO IDSV0003-DESCRIZ-ERR-DB2
				idsv0003.getCampiEsito().setDescrizErrDb2("ANNI-INPUT-2 NON VALORIZZATA");
			endif
		endif
		// COB_CODE: IF LVVC0000-DATA-INPUT-1 NOT NUMERIC
		//                TO IDSV0003-DESCRIZ-ERR-DB2
		//           END-IF.
		if (! Functions.isNumber(inputLvvs0000.getDataInput1Formatted())) then
			// COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
			idsv0003.getReturnCode().setFieldNotValued();
			// COB_CODE: MOVE WK-PGM
			//             TO IDSV0003-COD-SERVIZIO-BE
			idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
			// COB_CODE: MOVE 'MESI-INPUT-2 NON NUMERICA'
			//             TO IDSV0003-DESCRIZ-ERR-DB2
			idsv0003.getCampiEsito().setDescrizErrDb2("MESI-INPUT-2 NON NUMERICA");
		endif
	}

	/**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
	private void s1000Elaborazione() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN LVVC0000-AAAA-V-9999999
		//                   END-IF
		//              WHEN LVVC0000-AAA-V-GGG
		//                   END-IF
		//              WHEN LVVC0000-AAA-V-MM
		//                   END-IF
		//              WHEN OTHER
		//                               TO IDSV0003-DESCRIZ-ERR-DB2
		//           END-EVALUATE.
		switch inputLvvs0000.getFormatDate().getFormatDate()
		case Lvvc0000FormatDate.AAAA_V9999999:
			// COB_CODE: PERFORM S0005-CTRL-DATI-INPUT-1 THRU EX-S0005
			s0005CtrlDatiInput1();
			// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
			//              PERFORM C100-CONVERTI-FORMAT-1  THRU C100-EX
			//           END-IF
			if (idsv0003.getReturnCode().isSuccessfulRc()) then
				// COB_CODE: PERFORM C100-CONVERTI-FORMAT-1  THRU C100-EX
				c100ConvertiFormat1();
			endif
		case Lvvc0000FormatDate.AAA_V_GGG:
			// COB_CODE: PERFORM S0010-CTRL-DATI-INPUT-2 THRU EX-S0010
			s0010CtrlDatiInput2();
			// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
			//              PERFORM C200-CONVERTI-FORMAT-2  THRU C200-EX
			//           END-IF
			if (idsv0003.getReturnCode().isSuccessfulRc()) then
				// COB_CODE: PERFORM C200-CONVERTI-FORMAT-2  THRU C200-EX
				c200ConvertiFormat2();
			endif
		case Lvvc0000FormatDate.AAA_V_MM:
			// COB_CODE: PERFORM S0015-CTRL-DATI-INPUT-3 THRU EX-S0015
			s0015CtrlDatiInput3();
			// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
			//              PERFORM C300-CONVERTI-FORMAT-3  THRU C300-EX
			//           END-IF
			if (idsv0003.getReturnCode().isSuccessfulRc()) then
				// COB_CODE: PERFORM C300-CONVERTI-FORMAT-3  THRU C300-EX
				c300ConvertiFormat3();
			endif
		default:
			// COB_CODE: SET  IDSV0003-OPER-NOT-V       TO TRUE
			idsv0003.getReturnCode().setIdsv0003OperNotV();
			// COB_CODE: MOVE WK-PGM TO IDSV0003-COD-SERVIZIO-BE
			idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
			// COB_CODE: MOVE 'FORMATO RICHIESTO NON VALIDO'
			//                       TO IDSV0003-DESCRIZ-ERR-DB2
			idsv0003.getCampiEsito().setDescrizErrDb2("FORMATO RICHIESTO NON VALIDO");
		endswitch
	}

	/**Original name: C100-CONVERTI-FORMAT-1<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONVERSIONE IN AAAA,9999999
	 * ----------------------------------------------------------------*
	 * -- CALCOLO ANNO BISESTILE</pre>*/
	private void c100ConvertiFormat1() {
		// COB_CODE: MOVE ZERO                TO WK-ANNO-DIVISIONE
		//                                       RESTO.
		ws.setWkAnnoDivisione(0);
		ws.setResto(0);
		// COB_CODE: MOVE LVVC0000-DATA-INPUT-1 TO WK-DATA-INPUT.
		ws.getWkDataInput().setWkDataInputFormatted(inputLvvs0000.getDataInput1Formatted());
		// COB_CODE: DIVIDE WK-AAAA-INPUT BY 4 GIVING RISULT REMAINDER RESTO.
		ws.setRisult((short)(ws.getWkDataInput().getAaaaInput() / 4));
		ws.setResto((short)(ws.getWkDataInput().getAaaaInput() % 4));
		// COB_CODE: IF RESTO = 0
		//              MOVE 366 TO WK-ANNO-DIVISIONE
		//           ELSE
		//              MOVE 365 TO WK-ANNO-DIVISIONE
		//           END-IF.
		if (ws.getResto() = 0) then
			// COB_CODE: MOVE 29  TO TAB-GG (2)
			ws.getTabGiorni().setGg(2, (short)29);
			// COB_CODE: MOVE 366 TO WK-ANNO-DIVISIONE
			ws.setWkAnnoDivisione((short)366);
		else
			// COB_CODE: MOVE 28  TO TAB-GG (2)
			ws.getTabGiorni().setGg(2, (short)28);
			// COB_CODE: MOVE 365 TO WK-ANNO-DIVISIONE
			ws.setWkAnnoDivisione((short)365);
		endif
			//-- CALCOLO GIORNO TOTALE DELL'ANNO
		// COB_CODE: MOVE ZERO                TO WK-GG-APPOGGIO.
		ws.setWkGgAppoggio(0);
// COB_CODE: PERFORM VARYING IND-MESE FROM 1 BY 1
//           UNTIL IND-MESE > WK-MM-INPUT OR
//                 IND-MESE > 12
//             END-IF
//           END-PERFORM.
		ws.setIndMese((short)1);
		do
		while (! (ws.getIndMese() > ws.getWkDataInput().getMmInput() | ws.getIndMese() > 12))
			// COB_CODE: IF IND-MESE < WK-MM-INPUT
			//              ADD TAB-GG(IND-MESE)  TO  WK-GG-APPOGGIO
			//           END-IF
			if (ws.getIndMese() < ws.getWkDataInput().getMmInput()) then
				// COB_CODE: ADD TAB-GG(IND-MESE)  TO  WK-GG-APPOGGIO
				ws.setWkGgAppoggio(Trunc.toShort(ws.getTabGiorni().getGg(ws.getIndMese()) + ws.getWkGgAppoggio(), 4));
			endif
			// COB_CODE: IF IND-MESE = WK-MM-INPUT
			//              ADD WK-GG-INPUT       TO  WK-GG-APPOGGIO
			//           END-IF
			if (ws.getIndMese() = ws.getWkDataInput().getMmInput()) then
				// COB_CODE: ADD WK-GG-INPUT       TO  WK-GG-APPOGGIO
				ws.setWkGgAppoggio(Trunc.toShort(ws.getWkDataInput().getGgInput() + ws.getWkGgAppoggio(), 4));
			endif
			ws.setIndMese(Trunc.toShort(ws.getIndMese() + 1, 2));
		enddo
			//-- VALORIZZAZIONE AREEA DI OUTPUT
		// COB_CODE: MOVE WK-AAAA-INPUT           TO WK-NUMERICI.
		ws.getWkDataOutputV().setNumericiFormatted(ws.getWkDataInput().getAaaaInputFormatted());
		// COB_CODE: COMPUTE WK-APPO-DEC = WK-GG-APPOGGIO / WK-ANNO-DIVISIONE.
		ws.getWkAppoDecV().setWkAppoDec(Trunc.toDecimal((decimal(11,7))(((double)(ws.getWkGgAppoggio())) / ws.getWkAnnoDivisione()), 8, 7));
			//
		// COB_CODE: IF WK-DECIM-AP GREATER ZERO
		//              MOVE WK-DECIM-AP          TO WK-DECIMALI
		//           ELSE
		//              MOVE 9999999              TO WK-DECIMALI
		//           END-IF.
		if (Characters.GT_ZERO.test(ws.getWkAppoDecV().getDecimApFormatted())) then
			// COB_CODE: MOVE WK-DECIM-AP          TO WK-DECIMALI
			ws.getWkDataOutputV().setDecimaliFormatted(ws.getWkAppoDecV().getDecimApFormatted());
		else
			// COB_CODE: MOVE 9999999              TO WK-DECIMALI
			ws.getWkDataOutputV().setDecimali(9999999);
		endif
			//
		// COB_CODE: MOVE WK-DATA-OUTPUT          TO LVVC0000-DATA-OUTPUT.
		inputLvvs0000.setDataOutput(Trunc.toDecimal(ws.getWkDataOutputV().getWkDataOutput(), 11, 7));
	}

	/**Original name: C200-CONVERTI-FORMAT-2<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONVERSIONE IN AAAA,GGG
	 * ----------------------------------------------------------------*</pre>*/
	private void c200ConvertiFormat2() {
		// COB_CODE: MOVE LVVC0000-DATA-INPUT-1            TO WK-DATA-INPUT.
		ws.getWkDataInput().setWkDataInputFormatted(inputLvvs0000.getDataInput1Formatted());
			//
			//    SE IL MESE E' FEBBRAIO E IL GIORNO E' 28 O 29
			//    OPPURE SIAMO AL TRENTUNESIMO GIORNO DEL MESE
		// COB_CODE: IF  (WK-MM-INPUT = 2
		//           AND (WK-GG-INPUT  = 28 OR 29))
		//           OR  WK-GG-INPUT  = 31
		//                 TO WK-GG-DA-SOMMARE
		//           ELSE
		//                 TO WK-GG-DA-SOMMARE
		//           END-IF
		if (ws.getWkDataInput().getMmInput() = 2 & (ws.getWkDataInput().getGgInput() = 28 | ws.getWkDataInput().getGgInput() = 29) | ws.getWkDataInput().getGgInput() = 31) then
			// COB_CODE: MOVE 30
			//             TO WK-GG-DA-SOMMARE
			ws.setWkGgDaSommare((short)30);
		else
			// COB_CODE: MOVE WK-GG-INPUT
			//             TO WK-GG-DA-SOMMARE
			ws.setWkGgDaSommareFormatted(ws.getWkDataInput().getGgInputFormatted());
		endif
			//    DECREMENTO IL NUMERO DEI MESI DI UNO
		// COB_CODE: IF WK-GG-DA-SOMMARE GREATER ZERO
		//              COMPUTE WK-MESI = WK-MM-INPUT - 1
		//           END-IF.
		if (Characters.GT_ZERO.test(ws.getWkGgDaSommareFormatted())) then
			// COB_CODE: COMPUTE WK-MESI = WK-MM-INPUT - 1
			ws.setWkMesi(Trunc.toShort(abs(ws.getWkDataInput().getMmInput() - 1), 2));
		endif
		// COB_CODE: COMPUTE WK-DATA-OUTPUT = ((( WK-MESI * 30 ) +
		//                                     WK-GG-DA-SOMMARE) / 360).
		ws.getWkDataOutputV().setWkDataOutput(Trunc.toDecimal((decimal(12,7))(((double)(ws.getWkMesi() * 30 + ws.getWkGgDaSommare())) / 360), 11, 7));
			//
		// COB_CODE: IF WK-DECIMALI EQUAL ZERO
		//              MOVE 9999999                      TO WK-DECIMALI
		//           END-IF.
		if (Characters.EQ_ZERO.test(ws.getWkDataOutputV().getDecimaliFormatted())) then
			// COB_CODE: MOVE 9999999                      TO WK-DECIMALI
			ws.getWkDataOutputV().setDecimali(9999999);
		endif
			//
		// COB_CODE: MOVE WK-AAAA-INPUT                   TO WK-NUMERICI.
		ws.getWkDataOutputV().setNumericiFormatted(ws.getWkDataInput().getAaaaInputFormatted());
		// COB_CODE: MOVE WK-DATA-OUTPUT                  TO LVVC0000-DATA-OUTPUT.
		inputLvvs0000.setDataOutput(Trunc.toDecimal(ws.getWkDataOutputV().getWkDataOutput(), 11, 7));
	}

	/**Original name: C300-CONVERTI-FORMAT-3<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONVERSIONE IN AAAA,MM
	 * ----------------------------------------------------------------*</pre>*/
	private void c300ConvertiFormat3() {
		// COB_CODE: MOVE LVVC0000-DATA-INPUT-1            TO WK-DATA-INPUT.
		ws.getWkDataInput().setWkDataInputFormatted(inputLvvs0000.getDataInput1Formatted());
		// COB_CODE: COMPUTE WK-APPO-MESI = (WK-MM-INPUT * 30).
		ws.setWkAppoMesi(ws.getWkDataInput().getMmInput() * 30);
		// COB_CODE: COMPUTE WK-APPO-ANNI = (WK-AAAA-INPUT * 360).
		ws.setWkAppoAnni(ws.getWkDataInput().getAaaaInput() * 360);
			//
		// COB_CODE: COMPUTE LVVC0000-DATA-OUTPUT =
		//                   ((WK-APPO-ANNI + WK-APPO-MESI) / 360).
		inputLvvs0000.setDataOutput(Trunc.toDecimal((decimal(17,7))(((double)(ws.getWkAppoAnni() + ws.getWkAppoMesi())) / 360), 11, 7));
	}

	/**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
	private void s9000OperazioniFinali() {
		// COB_CODE: GOBACK.
		throw new ReturnException();
	}

}//Lvvs0000
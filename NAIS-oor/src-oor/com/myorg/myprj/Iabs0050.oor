package com.myorg.myprj;

import java.lang.Override;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;

import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import com.myorg.myprj.commons.data.dao.BtcBatchTypeDao;
import com.myorg.myprj.commons.data.to.IBtcBatchType;
import com.myorg.myprj.copy.Idsv0010;
import com.myorg.myprj.copy.IndLogErrore;
import com.myorg.myprj.copy.Sqlca;
import com.myorg.myprj.ws.BtcBatchType;
import com.myorg.myprj.ws.Idsv0003;
import com.myorg.myprj.ws.enums.Idsv0003LivelloOperazione;
import com.myorg.myprj.ws.redefines.BbtCommitFrequency;
import com.myorg.myprj.ws.redefines.BbtTpMovi;

/**Original name: IABS0050<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE
 *  F A S E         : GESTIONE TABELLA BTC_BATCH_TYPE
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
class Iabs0050 extends Program implements IBtcBatchType {

	//==== PROPERTIES ====
	//Original name: SQLCA
	private Sqlca sqlca := new Sqlca();
	private DbAccessStatus dbAccessStatus := new DbAccessStatus(sqlca);
	private BtcBatchTypeDao btcBatchTypeDao := new BtcBatchTypeDao(dbAccessStatus);
	//Original name: DESCRIZ-ERR-DB2
	private string descrizErrDb2 := "";
	//Original name: IDSV0010
	private Idsv0010 idsv0010 := new Idsv0010();
	//Original name: IND-BTC-BATCH-TYPE
	private IndLogErrore indBtcBatchType := new IndLogErrore();
	//Original name: IDSV0003
	private Idsv0003 idsv0003;
	//Original name: BTC-BATCH-TYPE
	private BtcBatchType btcBatchType;


	//==== METHODS ====
	/**Original name: PROGRAM_IABS0050_FIRST_SENTENCES<br>*/
	public long execute(Idsv0003 idsv0003, BtcBatchType btcBatchType) {
		this.idsv0003 := idsv0003;
		this.btcBatchType := btcBatchType;
		// COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
		a000Inizio();
		// COB_CODE: PERFORM A300-ELABORA             THRU A300-EX
		a300Elabora();
		// COB_CODE: GOBACK.
		//last return statement was skipped
		return 0;
	}

	public static Iabs0050 getInstance() {
		return (Iabs0050)Programs.getInstance(Iabs0050.clazz);
	}

	/**Original name: A000-INIZIO<br>
	 * <pre>*****************************************************************</pre>*/
	private void a000Inizio() {
		// COB_CODE: MOVE 'IABS0050'              TO   IDSV0003-COD-SERVIZIO-BE.
		idsv0003.getCampiEsito().setCodServizioBe("IABS0050");
		// COB_CODE: MOVE 'BTC_BATCH_TYPE'        TO   IDSV0003-NOME-TABELLA.
		idsv0003.getCampiEsito().setNomeTabella("BTC_BATCH_TYPE");
		// COB_CODE: MOVE '00'                    TO   IDSV0003-RETURN-CODE.
		idsv0003.getReturnCode().setReturnCode("00");
		// COB_CODE: MOVE ZEROES                  TO   IDSV0003-SQLCODE
		//                                             IDSV0003-NUM-RIGHE-LETTE.
		idsv0003.getSqlcode().setSqlcode(0);
		idsv0003.getCampiEsito().setNumRigheLette(0);
		// COB_CODE: MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2
		//                                             IDSV0003-KEY-TABELLA.
		idsv0003.getCampiEsito().setDescrizErrDb2("");
		idsv0003.getCampiEsito().setKeyTabella("");
		// COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
		a001TrattaDateTimestamp();
	}

	/**Original name: A100-CHECK-RETURN-CODE<br>
	 * <pre>*****************************************************************</pre>*/
	private void a100CheckReturnCode() {
		// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
		//              END-EVALUATE
		//           END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc()) then
			// COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
			idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
			// COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
			idsv0003.getCampiEsito().setDescrizErrDb2(descrizErrDb2);
			// COB_CODE: EVALUATE IDSV0003-SQLCODE
			//               WHEN ZERO
			//                             CONTINUE
			//               WHEN +100
			//                  END-IF
			//               WHEN OTHER
			//                             SET IDSV0003-SQL-ERROR TO TRUE
			//           END-EVALUATE
			if (idsv0003.getSqlcode().getSqlcode() = 0) then
				// COB_CODE: CONTINUE
				//continue
			elseif (idsv0003.getSqlcode().getSqlcode() = 100) then
				// COB_CODE: IF IDSV0003-SELECT                OR
				//              IDSV0003-FETCH-FIRST           OR
				//              IDSV0003-FETCH-NEXT
				//                      CONTINUE
				//           ELSE
				//                      SET IDSV0003-SQL-ERROR TO TRUE
				//           END-IF
				if (idsv0003.getOperazione().isSelect() | idsv0003.getOperazione().isFetchFirst() | idsv0003.getOperazione().isFetchNext()) then
					// COB_CODE: CONTINUE
					//continue
				else
					// COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
					idsv0003.getReturnCode().setSqlError();
				endif
			else
				// COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
				idsv0003.getReturnCode().setSqlError();
			endif
		endif
	}

	/**Original name: A300-ELABORA<br>
	 * <pre>*****************************************************************</pre>*/
	private void a300Elabora() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-PRIMARY-KEY
		//                 PERFORM A301-ELABORA-PK             THRU A301-EX
		//              WHEN IDSV0003-WHERE-CONDITION
		//                 PERFORM A302-ELABORA-WC             THRU A302-EX
		//              WHEN OTHER
		//                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
		//           END-EVALUATE.
		switch idsv0003.getLivelloOperazione().getLivelloOperazione()
		case Idsv0003LivelloOperazione.PRIMARY_KEY:
			// COB_CODE: PERFORM A301-ELABORA-PK             THRU A301-EX
			a301ElaboraPk();
		case Idsv0003LivelloOperazione.WHERE_CONDITION:
			// COB_CODE: PERFORM A302-ELABORA-WC             THRU A302-EX
			a302ElaboraWc();
		default:
			// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
			idsv0003.getReturnCode().setInvalidLevelOper();
		endswitch
	}

	/**Original name: A301-ELABORA-PK<br>
	 * <pre>*****************************************************************</pre>*/
	private void a301ElaboraPk() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-SELECT
		//                 PERFORM A310-SELECT-PK             THRU A310-EX
		//              WHEN OTHER
		//                 SET IDSV0003-INVALID-OPER TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getOperazione().isSelect()) then
			// COB_CODE: PERFORM A310-SELECT-PK             THRU A310-EX
			a310SelectPk();
		else
			// COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
			idsv0003.getReturnCode().setInvalidOper();
		endif
	}

	/**Original name: A302-ELABORA-WC<br>
	 * <pre>*****************************************************************</pre>*/
	private void a302ElaboraWc() {
		// COB_CODE: EVALUATE TRUE
		//              WHEN IDSV0003-SELECT
		//                 PERFORM W000-SELECT-WC             THRU W000-EX
		//              WHEN OTHER
		//                 SET IDSV0003-INVALID-OPER TO TRUE
		//           END-EVALUATE.
		if (idsv0003.getOperazione().isSelect()) then
			// COB_CODE: PERFORM W000-SELECT-WC             THRU W000-EX
			w000SelectWc();
		else
			// COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
			idsv0003.getReturnCode().setInvalidOper();
		endif
	}

	/**Original name: A310-SELECT-PK<br>
	 * <pre>*****************************************************************</pre>*/
	private void a310SelectPk() {
		// COB_CODE: EXEC SQL
		//             SELECT
		//                COD_BATCH_TYPE
		//                ,DES
		//                ,DEF_CONTENT_TYPE
		//                ,COMMIT_FREQUENCY
		//                ,SERVICE_NAME
		//                ,SERVICE_GUIDE_NAME
		//                ,SERVICE_ALPO_NAME
		//                ,COD_MACROFUNCT
		//                ,TP_MOVI
		//             INTO
		//                :BBT-COD-BATCH-TYPE
		//               ,:BBT-DES-VCHAR
		//               ,:BBT-DEF-CONTENT-TYPE
		//                :IND-BBT-DEF-CONTENT-TYPE
		//               ,:BBT-COMMIT-FREQUENCY
		//                :IND-BBT-COMMIT-FREQUENCY
		//               ,:BBT-SERVICE-NAME
		//                :IND-BBT-SERVICE-NAME
		//               ,:BBT-SERVICE-GUIDE-NAME
		//                :IND-BBT-SERVICE-GUIDE-NAME
		//               ,:BBT-SERVICE-ALPO-NAME
		//                :IND-BBT-SERVICE-ALPO-NAME
		//               ,:BBT-COD-MACROFUNCT
		//                :IND-BBT-COD-MACROFUNCT
		//               ,:BBT-TP-MOVI
		//                :IND-BBT-TP-MOVI
		//             FROM  BTC_BATCH_TYPE
		//             WHERE COD_BATCH_TYPE = :BBT-COD-BATCH-TYPE
		//           END-EXEC.
		btcBatchTypeDao.selectByBbtCodBatchType(btcBatchType.getBbtCodBatchType(), this);
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) then
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
		endif
	}

	/**Original name: W000-SELECT-WC<br>
	 * <pre>*****************************************************************</pre>*/
	private void w000SelectWc() {
		// COB_CODE: PERFORM Z200-SET-INDICATORI-NULL THRU Z200-EX.
		z200SetIndicatoriNull();
		// COB_CODE: EXEC SQL
		//             SELECT
		//                COD_BATCH_TYPE
		//                ,DES
		//                ,DEF_CONTENT_TYPE
		//                ,COMMIT_FREQUENCY
		//                ,SERVICE_NAME
		//                ,SERVICE_GUIDE_NAME
		//                ,SERVICE_ALPO_NAME
		//                ,COD_MACROFUNCT
		//                ,TP_MOVI
		//             INTO
		//                :BBT-COD-BATCH-TYPE
		//               ,:BBT-DES-VCHAR
		//               ,:BBT-DEF-CONTENT-TYPE
		//                :IND-BBT-DEF-CONTENT-TYPE
		//               ,:BBT-COMMIT-FREQUENCY
		//                :IND-BBT-COMMIT-FREQUENCY
		//               ,:BBT-SERVICE-NAME
		//                :IND-BBT-SERVICE-NAME
		//               ,:BBT-SERVICE-GUIDE-NAME
		//                :IND-BBT-SERVICE-GUIDE-NAME
		//               ,:BBT-SERVICE-ALPO-NAME
		//                :IND-BBT-SERVICE-ALPO-NAME
		//               ,:BBT-COD-MACROFUNCT
		//                :IND-BBT-COD-MACROFUNCT
		//               ,:BBT-TP-MOVI
		//                :IND-BBT-TP-MOVI
		//             FROM  BTC_BATCH_TYPE
		//             WHERE COD_MACROFUNCT   = :BBT-COD-MACROFUNCT
		//                                      :IND-BBT-COD-MACROFUNCT AND
		//                   TP_MOVI          = :BBT-TP-MOVI
		//                                      :IND-BBT-TP-MOVI        AND
		//                   SUBSTR(DEF_CONTENT_TYPE, 1, 2)
		//                                    = :BBT-DEF-CONTENT-TYPE
		//           END-EXEC.
		btcBatchTypeDao.selectRec(btcBatchType.getBbtCodMacrofunct(), btcBatchType.getBbtTpMovi().getBbtTpMovi(), btcBatchType.getBbtDefContentType(), this);
		// COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
		a100CheckReturnCode();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
		//              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
		//           END-IF.
		if (idsv0003.getSqlcode().isSuccessfulSql()) then
			// COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
			z100SetColonneNull();
		endif
	}

	/**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>*****************************************************************</pre>*/
	private void z100SetColonneNull() {
		// COB_CODE: IF IND-BBT-DEF-CONTENT-TYPE = -1
		//              MOVE HIGH-VALUES TO BBT-DEF-CONTENT-TYPE-NULL
		//           END-IF
		if (indBtcBatchType.getLabelErr() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO BBT-DEF-CONTENT-TYPE-NULL
			btcBatchType.setBbtDefContentType(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcBatchType.Len.BBT_DEF_CONTENT_TYPE));
		endif
		// COB_CODE: IF IND-BBT-COMMIT-FREQUENCY = -1
		//              MOVE HIGH-VALUES TO BBT-COMMIT-FREQUENCY-NULL
		//           END-IF
		if (indBtcBatchType.getOperTabella() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO BBT-COMMIT-FREQUENCY-NULL
			btcBatchType.getBbtCommitFrequency().setBbtCommitFrequencyNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BbtCommitFrequency.Len.BBT_COMMIT_FREQUENCY_NULL));
		endif
		// COB_CODE: IF IND-BBT-SERVICE-NAME = -1
		//              MOVE HIGH-VALUES TO BBT-SERVICE-NAME-NULL
		//           END-IF
		if (indBtcBatchType.getNomeTabella() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO BBT-SERVICE-NAME-NULL
			btcBatchType.setBbtServiceName(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcBatchType.Len.BBT_SERVICE_NAME));
		endif
		// COB_CODE: IF IND-BBT-SERVICE-GUIDE-NAME = -1
		//              MOVE HIGH-VALUES TO BBT-SERVICE-GUIDE-NAME-NULL
		//           END-IF
		if (indBtcBatchType.getStatusTabella() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO BBT-SERVICE-GUIDE-NAME-NULL
			btcBatchType.setBbtServiceGuideName(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcBatchType.Len.BBT_SERVICE_GUIDE_NAME));
		endif
		// COB_CODE: IF IND-BBT-SERVICE-ALPO-NAME = -1
		//              MOVE HIGH-VALUES TO BBT-SERVICE-ALPO-NAME-NULL
		//           END-IF
		if (indBtcBatchType.getKeyTabella() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO BBT-SERVICE-ALPO-NAME-NULL
			btcBatchType.setBbtServiceAlpoName(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcBatchType.Len.BBT_SERVICE_ALPO_NAME));
		endif
		// COB_CODE: IF IND-BBT-COD-MACROFUNCT = -1
		//              MOVE HIGH-VALUES TO BBT-COD-MACROFUNCT-NULL
		//           END-IF
		if (indBtcBatchType.getTipoOggetto() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO BBT-COD-MACROFUNCT-NULL
			btcBatchType.setBbtCodMacrofunct(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcBatchType.Len.BBT_COD_MACROFUNCT));
		endif
		// COB_CODE: IF IND-BBT-TP-MOVI = -1
		//              MOVE HIGH-VALUES TO BBT-TP-MOVI-NULL
		//           END-IF.
		if (indBtcBatchType.getIbOggetto() = -1) then
			// COB_CODE: MOVE HIGH-VALUES TO BBT-TP-MOVI-NULL
			btcBatchType.getBbtTpMovi().setBbtTpMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BbtTpMovi.Len.BBT_TP_MOVI_NULL));
		endif
	}

	/**Original name: Z200-SET-INDICATORI-NULL<br>*/
	private void z200SetIndicatoriNull() {
		// COB_CODE: IF BBT-DEF-CONTENT-TYPE-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-BBT-DEF-CONTENT-TYPE
		//           ELSE
		//              MOVE 0 TO IND-BBT-DEF-CONTENT-TYPE
		//           END-IF
		if (Characters.EQ_HIGH.test(btcBatchType.getBbtDefContentType(), BtcBatchType.Len.BBT_DEF_CONTENT_TYPE)) then
			// COB_CODE: MOVE -1 TO IND-BBT-DEF-CONTENT-TYPE
			indBtcBatchType.setLabelErr((short)-1);
		else
			// COB_CODE: MOVE 0 TO IND-BBT-DEF-CONTENT-TYPE
			indBtcBatchType.setLabelErr((short)0);
		endif
		// COB_CODE: IF BBT-COMMIT-FREQUENCY-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-BBT-COMMIT-FREQUENCY
		//           ELSE
		//              MOVE 0 TO IND-BBT-COMMIT-FREQUENCY
		//           END-IF
		if (Characters.EQ_HIGH.test(btcBatchType.getBbtCommitFrequency().getBbtCommitFrequencyNullFormatted())) then
			// COB_CODE: MOVE -1 TO IND-BBT-COMMIT-FREQUENCY
			indBtcBatchType.setOperTabella((short)-1);
		else
			// COB_CODE: MOVE 0 TO IND-BBT-COMMIT-FREQUENCY
			indBtcBatchType.setOperTabella((short)0);
		endif
		// COB_CODE: IF BBT-SERVICE-NAME-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-BBT-SERVICE-NAME
		//           ELSE
		//              MOVE 0 TO IND-BBT-SERVICE-NAME
		//           END-IF
		if (Characters.EQ_HIGH.test(btcBatchType.getBbtServiceName(), BtcBatchType.Len.BBT_SERVICE_NAME)) then
			// COB_CODE: MOVE -1 TO IND-BBT-SERVICE-NAME
			indBtcBatchType.setNomeTabella((short)-1);
		else
			// COB_CODE: MOVE 0 TO IND-BBT-SERVICE-NAME
			indBtcBatchType.setNomeTabella((short)0);
		endif
		// COB_CODE: IF BBT-SERVICE-GUIDE-NAME-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-BBT-SERVICE-GUIDE-NAME
		//           ELSE
		//              MOVE 0 TO IND-BBT-SERVICE-GUIDE-NAME
		//           END-IF
		if (Characters.EQ_HIGH.test(btcBatchType.getBbtServiceGuideName(), BtcBatchType.Len.BBT_SERVICE_GUIDE_NAME)) then
			// COB_CODE: MOVE -1 TO IND-BBT-SERVICE-GUIDE-NAME
			indBtcBatchType.setStatusTabella((short)-1);
		else
			// COB_CODE: MOVE 0 TO IND-BBT-SERVICE-GUIDE-NAME
			indBtcBatchType.setStatusTabella((short)0);
		endif
		// COB_CODE: IF BBT-SERVICE-ALPO-NAME-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-BBT-SERVICE-ALPO-NAME
		//           ELSE
		//              MOVE 0 TO IND-BBT-SERVICE-ALPO-NAME
		//           END-IF
		if (Characters.EQ_HIGH.test(btcBatchType.getBbtServiceAlpoName(), BtcBatchType.Len.BBT_SERVICE_ALPO_NAME)) then
			// COB_CODE: MOVE -1 TO IND-BBT-SERVICE-ALPO-NAME
			indBtcBatchType.setKeyTabella((short)-1);
		else
			// COB_CODE: MOVE 0 TO IND-BBT-SERVICE-ALPO-NAME
			indBtcBatchType.setKeyTabella((short)0);
		endif
		// COB_CODE: IF BBT-COD-MACROFUNCT-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-BBT-COD-MACROFUNCT
		//           ELSE
		//              MOVE 0 TO IND-BBT-COD-MACROFUNCT
		//           END-IF
		if (Characters.EQ_HIGH.test(btcBatchType.getBbtCodMacrofunctFormatted())) then
			// COB_CODE: MOVE -1 TO IND-BBT-COD-MACROFUNCT
			indBtcBatchType.setTipoOggetto((short)-1);
		else
			// COB_CODE: MOVE 0 TO IND-BBT-COD-MACROFUNCT
			indBtcBatchType.setTipoOggetto((short)0);
		endif
		// COB_CODE: IF BBT-TP-MOVI-NULL = HIGH-VALUES
		//              MOVE -1 TO IND-BBT-TP-MOVI
		//           ELSE
		//              MOVE 0 TO IND-BBT-TP-MOVI
		//           END-IF.
		if (Characters.EQ_HIGH.test(btcBatchType.getBbtTpMovi().getBbtTpMoviNullFormatted())) then
			// COB_CODE: MOVE -1 TO IND-BBT-TP-MOVI
			indBtcBatchType.setIbOggetto((short)-1);
		else
			// COB_CODE: MOVE 0 TO IND-BBT-TP-MOVI
			indBtcBatchType.setIbOggetto((short)0);
		endif
	}

	/**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
	private void a001TrattaDateTimestamp() {
		// COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
		a020ConvertiDtEffetto();
		// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
		//              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
		//           END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc()) then
			// COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
			a050ValorizzaCptz();
		endif
	}

	/**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
	private void a020ConvertiDtEffetto() {
		// COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
		//                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
		//           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
		//           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
		//           *                                TO IDSV0003-DESCRIZ-ERR-DB2
		//                   CONTINUE
		//                ELSE
		//                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
		//                END-IF
		if (! Functions.isNumber(idsv0003.getDataInizioEffetto()) | idsv0003.getDataInizioEffetto() = 0) then
				//       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
				//       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
				//                                TO IDSV0003-DESCRIZ-ERR-DB2
			// COB_CODE: CONTINUE
			//continue
		else
			// COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
			idsv0010.setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
			// COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
			//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
			
			// COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
			idsv0010.setWsDataInizioEffettoDb(idsv0010.getWsDateX());
		endif
		// COB_CODE: IF IDSV0003-SUCCESSFUL-RC
		//              END-IF
		//           END-IF.
		if (idsv0003.getReturnCode().isSuccessfulRc()) then
			// COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
			//              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
			//              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
			//           END-IF
			if (Functions.isNumber(idsv0003.getDataFineEffetto()) & idsv0003.getDataFineEffetto() != 0) then
				// COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
				idsv0010.setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
				// COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
				//Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
				
				// COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
				idsv0010.setWsDataFineEffettoDb(idsv0010.getWsDateX());
			endif
		endif
	}

	/**Original name: A050-VALORIZZA-CPTZ<br>*/
	private void a050ValorizzaCptz() {
		// COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
		//                   IDSV0003-DATA-COMPETENZA  = 0
		//           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
		//           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
		//           *                                TO IDSV0003-DESCRIZ-ERR-DB2
		//                   CONTINUE
		//                ELSE
		//                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
		//                END-IF.
		if (! Functions.isNumber(idsv0003.getDataCompetenza()) | idsv0003.getDataCompetenza() = 0) then
				//       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
				//       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
				//                                TO IDSV0003-DESCRIZ-ERR-DB2
			// COB_CODE: CONTINUE
			//continue
		else
			// COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
			idsv0010.setWsTsCompetenza(idsv0003.getDataCompetenza());
		endif
		// COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
		//                   IDSV0003-DATA-COMP-AGG-STOR  = 0
		//           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
		//           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
		//           *                                TO IDSV0003-DESCRIZ-ERR-DB2
		//                   CONTINUE
		//                ELSE
		//                                       TO WS-TS-COMPETENZA-AGG-STOR
		//                END-IF.
		if (! Functions.isNumber(idsv0003.getDataCompAggStor()) | idsv0003.getDataCompAggStor() = 0) then
				//       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
				//       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
				//                                TO IDSV0003-DESCRIZ-ERR-DB2
			// COB_CODE: CONTINUE
			//continue
		else
			// COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
			//                               TO WS-TS-COMPETENZA-AGG-STOR
			idsv0010.setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
		endif
	}

	public string getDescrizErrDb2() {
		return this.descrizErrDb2;
	}

	@Override
	public integer getBbtCodBatchType() {
		return btcBatchType.getBbtCodBatchType();
	}

	@Override
	public void setBbtCodBatchType(integer bbtCodBatchType) {
		this.btcBatchType.setBbtCodBatchType(bbtCodBatchType);
	}

	@Override
	public string getCodMacrofunct() {
		return btcBatchType.getBbtCodMacrofunct();
	}

	@Override
	public void setCodMacrofunct(string codMacrofunct) {
		this.btcBatchType.setBbtCodMacrofunct(codMacrofunct);
	}

	@Override
	public String getCodMacrofunctObj() {
		if (indBtcBatchType.getTipoOggetto()>= 0) then
		    return getCodMacrofunct();
		else
		    return null;
		endif;
	}

	@Override
	public void setCodMacrofunctObj(String codMacrofunctObj) {
		if (codMacrofunctObj!= null) then
		    setCodMacrofunct(codMacrofunctObj);
		    indBtcBatchType.setTipoOggetto(0);
		else
		    indBtcBatchType.setTipoOggetto(-1);
		endif;
	}

	@Override
	public integer getCommitFrequency() {
		return btcBatchType.getBbtCommitFrequency().getBbtCommitFrequency();
	}

	@Override
	public void setCommitFrequency(integer commitFrequency) {
		this.btcBatchType.getBbtCommitFrequency().setBbtCommitFrequency(commitFrequency);
	}

	@Override
	public Integer getCommitFrequencyObj() {
		if (indBtcBatchType.getOperTabella()>= 0) then
		    return getCommitFrequency();
		else
		    return null;
		endif;
	}

	@Override
	public void setCommitFrequencyObj(Integer commitFrequencyObj) {
		if (commitFrequencyObj!= null) then
		    setCommitFrequency(commitFrequencyObj);
		    indBtcBatchType.setOperTabella(0);
		else
		    indBtcBatchType.setOperTabella(-1);
		endif;
	}

	@Override
	public string getDefContentType() {
		return btcBatchType.getBbtDefContentType();
	}

	@Override
	public void setDefContentType(string defContentType) {
		this.btcBatchType.setBbtDefContentType(defContentType);
	}

	@Override
	public String getDefContentTypeObj() {
		if (indBtcBatchType.getLabelErr()>= 0) then
		    return getDefContentType();
		else
		    return null;
		endif;
	}

	@Override
	public void setDefContentTypeObj(String defContentTypeObj) {
		if (defContentTypeObj!= null) then
		    setDefContentType(defContentTypeObj);
		    indBtcBatchType.setLabelErr(0);
		else
		    indBtcBatchType.setLabelErr(-1);
		endif;
	}

	@Override
	public string getDesVchar() {
		return btcBatchType.getBbtDesVcharFormatted();
	}

	@Override
	public void setDesVchar(string desVchar) {
		this.btcBatchType.setBbtDesVcharFormatted(desVchar);
	}

	@Override
	public string getServiceAlpoName() {
		return btcBatchType.getBbtServiceAlpoName();
	}

	@Override
	public void setServiceAlpoName(string serviceAlpoName) {
		this.btcBatchType.setBbtServiceAlpoName(serviceAlpoName);
	}

	@Override
	public String getServiceAlpoNameObj() {
		if (indBtcBatchType.getKeyTabella()>= 0) then
		    return getServiceAlpoName();
		else
		    return null;
		endif;
	}

	@Override
	public void setServiceAlpoNameObj(String serviceAlpoNameObj) {
		if (serviceAlpoNameObj!= null) then
		    setServiceAlpoName(serviceAlpoNameObj);
		    indBtcBatchType.setKeyTabella(0);
		else
		    indBtcBatchType.setKeyTabella(-1);
		endif;
	}

	@Override
	public string getServiceGuideName() {
		return btcBatchType.getBbtServiceGuideName();
	}

	@Override
	public void setServiceGuideName(string serviceGuideName) {
		this.btcBatchType.setBbtServiceGuideName(serviceGuideName);
	}

	@Override
	public String getServiceGuideNameObj() {
		if (indBtcBatchType.getStatusTabella()>= 0) then
		    return getServiceGuideName();
		else
		    return null;
		endif;
	}

	@Override
	public void setServiceGuideNameObj(String serviceGuideNameObj) {
		if (serviceGuideNameObj!= null) then
		    setServiceGuideName(serviceGuideNameObj);
		    indBtcBatchType.setStatusTabella(0);
		else
		    indBtcBatchType.setStatusTabella(-1);
		endif;
	}

	@Override
	public string getServiceName() {
		return btcBatchType.getBbtServiceName();
	}

	@Override
	public void setServiceName(string serviceName) {
		this.btcBatchType.setBbtServiceName(serviceName);
	}

	@Override
	public String getServiceNameObj() {
		if (indBtcBatchType.getNomeTabella()>= 0) then
		    return getServiceName();
		else
		    return null;
		endif;
	}

	@Override
	public void setServiceNameObj(String serviceNameObj) {
		if (serviceNameObj!= null) then
		    setServiceName(serviceNameObj);
		    indBtcBatchType.setNomeTabella(0);
		else
		    indBtcBatchType.setNomeTabella(-1);
		endif;
	}

	@Override
	public integer getTpMovi() {
		return btcBatchType.getBbtTpMovi().getBbtTpMovi();
	}

	@Override
	public void setTpMovi(integer tpMovi) {
		this.btcBatchType.getBbtTpMovi().setBbtTpMovi(tpMovi);
	}

	@Override
	public Integer getTpMoviObj() {
		if (indBtcBatchType.getIbOggetto()>= 0) then
		    return getTpMovi();
		else
		    return null;
		endif;
	}

	@Override
	public void setTpMoviObj(Integer tpMoviObj) {
		if (tpMoviObj!= null) then
		    setTpMovi(tpMoviObj);
		    indBtcBatchType.setIbOggetto(0);
		else
		    indBtcBatchType.setIbOggetto(-1);
		endif;
	}

}//Iabs0050
package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.ws.enums.Ispc0140RicalcQuiet;
import com.myorg.myprj.ws.occurs.Ispc0140SchedaP;
import com.myorg.myprj.ws.occurs.Ispc0140SchedaT;

/**Original name: ISPC0140-DATI-INPUT<br>
 * Variable: ISPC0140-DATI-INPUT from copybook ISPC0140<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ispc0140DatiInput {

	//==== PROPERTIES ====
	public final static integer SCHEDA_P_MAXOCCURS := 1;
	public final static integer SCHEDA_T_MAXOCCURS := 20;
	//Original name: ISPC0140-COD-COMPAGNIA
	private string codCompagnia := DefaultValues.stringVal(Len.COD_COMPAGNIA);
	//Original name: ISPC0140-COD-PRODOTTO
	private string codProdotto := DefaultValues.stringVal(Len.COD_PRODOTTO);
	//Original name: ISPC0140-COD-CONVENZIONE
	private string codConvenzione := DefaultValues.stringVal(Len.COD_CONVENZIONE);
	//Original name: ISPC0140-DATA-INIZ-VALID-CONV
	private string dataInizValidConv := DefaultValues.stringVal(Len.DATA_INIZ_VALID_CONV);
	//Original name: ISPC0140-FUNZIONALITA
	private string funzionalita := DefaultValues.stringVal(Len.FUNZIONALITA);
	//Original name: ISPC0140-DATA-RIFERIMENTO
	private string dataRiferimento := DefaultValues.stringVal(Len.DATA_RIFERIMENTO);
	//Original name: ISPC0140-LIVELLO-UTENTE
	private string livelloUtente := DefaultValues.stringVal(Len.LIVELLO_UTENTE);
	//Original name: ISPC0140-SESSION-ID
	private string sessionId := DefaultValues.stringVal(Len.SESSION_ID);
	//Original name: ISPC0140-DATA-DECORR-POLIZZA
	private string dataDecorrPolizza := DefaultValues.stringVal(Len.DATA_DECORR_POLIZZA);
	//Original name: ISPC0140-MODALITA-CALCOLO
	private char modalitaCalcolo := DefaultValues.CHAR_VAL;
	//Original name: ISPC0140-RICALC-QUIET
	private Ispc0140RicalcQuiet ricalcQuiet := new Ispc0140RicalcQuiet();
	//Original name: ISPC0140-DEE
	private string dee := DefaultValues.stringVal(Len.DEE);
	/**Original name: ISPC0140-ELE-MAX-SCHEDA-P<br>
	 * <pre>----------------------------------------------------------------*
	 *    SCHEDE PRODOTTO
	 * ----------------------------------------------------------------*</pre>*/
	private string eleMaxSchedaP := DefaultValues.stringVal(Len.ELE_MAX_SCHEDA_P);
	//Original name: ISPC0140-SCHEDA-P
	private []Ispc0140SchedaP schedaP := new [SCHEDA_P_MAXOCCURS]Ispc0140SchedaP;
	/**Original name: ISPC0140-ELE-MAX-SCHEDA-T<br>
	 * <pre>----------------------------------------------------------------*
	 *    SCHEDE TRANCHE
	 * ----------------------------------------------------------------*</pre>*/
	private string eleMaxSchedaT := DefaultValues.stringVal(Len.ELE_MAX_SCHEDA_T);
	//Original name: ISPC0140-SCHEDA-T
	private []Ispc0140SchedaT schedaT := new [SCHEDA_T_MAXOCCURS]Ispc0140SchedaT;

	//==== CONSTRUCTORS ====
	public Ispc0140DatiInput() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for int schedaPIdx in 1.. SCHEDA_P_MAXOCCURS 
		do
			schedaP[schedaPIdx] := new Ispc0140SchedaP();
		enddo
		for int schedaTIdx in 1.. SCHEDA_T_MAXOCCURS 
		do
			schedaT[schedaTIdx] := new Ispc0140SchedaT();
		enddo
	}

	public void setDatiInputBytes([]byte buffer, integer offset) {
		integer position := offset;
		codCompagnia := MarshalByte.readFixedString(buffer, position, Len.COD_COMPAGNIA);
		position +:= Len.COD_COMPAGNIA;
		codProdotto := MarshalByte.readString(buffer, position, Len.COD_PRODOTTO);
		position +:= Len.COD_PRODOTTO;
		codConvenzione := MarshalByte.readString(buffer, position, Len.COD_CONVENZIONE);
		position +:= Len.COD_CONVENZIONE;
		dataInizValidConv := MarshalByte.readString(buffer, position, Len.DATA_INIZ_VALID_CONV);
		position +:= Len.DATA_INIZ_VALID_CONV;
		funzionalita := MarshalByte.readFixedString(buffer, position, Len.FUNZIONALITA);
		position +:= Len.FUNZIONALITA;
		dataRiferimento := MarshalByte.readString(buffer, position, Len.DATA_RIFERIMENTO);
		position +:= Len.DATA_RIFERIMENTO;
		livelloUtente := MarshalByte.readFixedString(buffer, position, Len.LIVELLO_UTENTE);
		position +:= Len.LIVELLO_UTENTE;
		sessionId := MarshalByte.readString(buffer, position, Len.SESSION_ID);
		position +:= Len.SESSION_ID;
		dataDecorrPolizza := MarshalByte.readString(buffer, position, Len.DATA_DECORR_POLIZZA);
		position +:= Len.DATA_DECORR_POLIZZA;
		modalitaCalcolo := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		ricalcQuiet.setRicalcQuiet(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		dee := MarshalByte.readString(buffer, position, Len.DEE);
		position +:= Len.DEE;
		eleMaxSchedaP := MarshalByte.readFixedString(buffer, position, Len.ELE_MAX_SCHEDA_P);
		position +:= Len.ELE_MAX_SCHEDA_P;
		for integer idx in 1.. SCHEDA_P_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				schedaP[idx].setSchedaPBytes(buffer, position);
				position +:= Ispc0140SchedaP.Len.SCHEDA_P;
			else
				schedaP[idx].initSchedaPSpaces();
				position +:= Ispc0140SchedaP.Len.SCHEDA_P;
			endif
		enddo
		eleMaxSchedaT := MarshalByte.readFixedString(buffer, position, Len.ELE_MAX_SCHEDA_T);
		position +:= Len.ELE_MAX_SCHEDA_T;
		for integer idx in 1.. SCHEDA_T_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				schedaT[idx].setSchedaTBytes(buffer, position);
				position +:= Ispc0140SchedaT.Len.SCHEDA_T;
			else
				schedaT[idx].initSchedaTSpaces();
				position +:= Ispc0140SchedaT.Len.SCHEDA_T;
			endif
		enddo
	}

	public []byte getDatiInputBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, codCompagnia, Len.COD_COMPAGNIA);
		position +:= Len.COD_COMPAGNIA;
		MarshalByte.writeString(buffer, position, codProdotto, Len.COD_PRODOTTO);
		position +:= Len.COD_PRODOTTO;
		MarshalByte.writeString(buffer, position, codConvenzione, Len.COD_CONVENZIONE);
		position +:= Len.COD_CONVENZIONE;
		MarshalByte.writeString(buffer, position, dataInizValidConv, Len.DATA_INIZ_VALID_CONV);
		position +:= Len.DATA_INIZ_VALID_CONV;
		MarshalByte.writeString(buffer, position, funzionalita, Len.FUNZIONALITA);
		position +:= Len.FUNZIONALITA;
		MarshalByte.writeString(buffer, position, dataRiferimento, Len.DATA_RIFERIMENTO);
		position +:= Len.DATA_RIFERIMENTO;
		MarshalByte.writeString(buffer, position, livelloUtente, Len.LIVELLO_UTENTE);
		position +:= Len.LIVELLO_UTENTE;
		MarshalByte.writeString(buffer, position, sessionId, Len.SESSION_ID);
		position +:= Len.SESSION_ID;
		MarshalByte.writeString(buffer, position, dataDecorrPolizza, Len.DATA_DECORR_POLIZZA);
		position +:= Len.DATA_DECORR_POLIZZA;
		MarshalByte.writeChar(buffer, position, modalitaCalcolo);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, ricalcQuiet.getRicalcQuiet());
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, dee, Len.DEE);
		position +:= Len.DEE;
		MarshalByte.writeString(buffer, position, eleMaxSchedaP, Len.ELE_MAX_SCHEDA_P);
		position +:= Len.ELE_MAX_SCHEDA_P;
		for integer idx in 1.. SCHEDA_P_MAXOCCURS 
		do
			schedaP[idx].getSchedaPBytes(buffer, position);
			position +:= Ispc0140SchedaP.Len.SCHEDA_P;
		enddo
		MarshalByte.writeString(buffer, position, eleMaxSchedaT, Len.ELE_MAX_SCHEDA_T);
		position +:= Len.ELE_MAX_SCHEDA_T;
		for integer idx in 1.. SCHEDA_T_MAXOCCURS 
		do
			schedaT[idx].getSchedaTBytes(buffer, position);
			position +:= Ispc0140SchedaT.Len.SCHEDA_T;
		enddo
		return buffer;
	}

	public void setIspc0140CodCompagniaFormatted(string ispc0140CodCompagnia) {
		this.codCompagnia:=Trunc.toUnsignedNumeric(ispc0140CodCompagnia, Len.COD_COMPAGNIA);
	}

	public integer getIspc0140CodCompagnia() {
		return NumericDisplay.asInt(this.codCompagnia);
	}

	public void setCodProdotto(string codProdotto) {
		this.codProdotto:=Functions.subString(codProdotto, Len.COD_PRODOTTO);
	}

	public string getCodProdotto() {
		return this.codProdotto;
	}

	public void setCodConvenzione(string codConvenzione) {
		this.codConvenzione:=Functions.subString(codConvenzione, Len.COD_CONVENZIONE);
	}

	public string getCodConvenzione() {
		return this.codConvenzione;
	}

	public void setDataInizValidConv(string dataInizValidConv) {
		this.dataInizValidConv:=Functions.subString(dataInizValidConv, Len.DATA_INIZ_VALID_CONV);
	}

	public string getDataInizValidConv() {
		return this.dataInizValidConv;
	}

	public void setIspc0140FunzionalitaFormatted(string ispc0140Funzionalita) {
		this.funzionalita:=Trunc.toUnsignedNumeric(ispc0140Funzionalita, Len.FUNZIONALITA);
	}

	public integer getIspc0140Funzionalita() {
		return NumericDisplay.asInt(this.funzionalita);
	}

	public void setDataRiferimento(string dataRiferimento) {
		this.dataRiferimento:=Functions.subString(dataRiferimento, Len.DATA_RIFERIMENTO);
	}

	public string getDataRiferimento() {
		return this.dataRiferimento;
	}

	public void setIspc0140LivelloUtente(short ispc0140LivelloUtente) {
		this.livelloUtente := NumericDisplay.asString(ispc0140LivelloUtente, Len.LIVELLO_UTENTE);
	}

	public short getIspc0140LivelloUtente() {
		return NumericDisplay.asShort(this.livelloUtente);
	}

	public void setSessionId(string sessionId) {
		this.sessionId:=Functions.subString(sessionId, Len.SESSION_ID);
	}

	public string getSessionId() {
		return this.sessionId;
	}

	public void setDataDecorrPolizza(string dataDecorrPolizza) {
		this.dataDecorrPolizza:=Functions.subString(dataDecorrPolizza, Len.DATA_DECORR_POLIZZA);
	}

	public string getDataDecorrPolizza() {
		return this.dataDecorrPolizza;
	}

	public void setModalitaCalcolo(char modalitaCalcolo) {
		this.modalitaCalcolo:=modalitaCalcolo;
	}

	public void setIspc0140ModalitaCalcoloFormatted(string ispc0140ModalitaCalcolo) {
		setModalitaCalcolo(Functions.charAt(ispc0140ModalitaCalcolo, Types.CHAR_SIZE));
	}

	public char getModalitaCalcolo() {
		return this.modalitaCalcolo;
	}

	public void setDee(string dee) {
		this.dee:=Functions.subString(dee, Len.DEE);
	}

	public string getDee() {
		return this.dee;
	}

	public void setIspc0140EleMaxSchedaP(short ispc0140EleMaxSchedaP) {
		this.eleMaxSchedaP := NumericDisplay.asString(ispc0140EleMaxSchedaP, Len.ELE_MAX_SCHEDA_P);
	}

	public short getIspc0140EleMaxSchedaP() {
		return NumericDisplay.asShort(this.eleMaxSchedaP);
	}

	public void setIspc0140EleMaxSchedaT(short ispc0140EleMaxSchedaT) {
		this.eleMaxSchedaT := NumericDisplay.asString(ispc0140EleMaxSchedaT, Len.ELE_MAX_SCHEDA_T);
	}

	public short getIspc0140EleMaxSchedaT() {
		return NumericDisplay.asShort(this.eleMaxSchedaT);
	}

	public Ispc0140SchedaP getSchedaP(integer idx) {
		return schedaP[idx];
	}

	public Ispc0140SchedaT getSchedaT(integer idx) {
		return schedaT[idx];
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer COD_COMPAGNIA := 5;
		public final static integer COD_PRODOTTO := 12;
		public final static integer COD_CONVENZIONE := 12;
		public final static integer DATA_INIZ_VALID_CONV := 8;
		public final static integer FUNZIONALITA := 5;
		public final static integer DATA_RIFERIMENTO := 8;
		public final static integer LIVELLO_UTENTE := 2;
		public final static integer SESSION_ID := 20;
		public final static integer DATA_DECORR_POLIZZA := 8;
		public final static integer MODALITA_CALCOLO := 1;
		public final static integer DEE := 8;
		public final static integer ELE_MAX_SCHEDA_P := 4;
		public final static integer ELE_MAX_SCHEDA_T := 4;
		public final static integer DATI_INPUT := COD_COMPAGNIA + COD_PRODOTTO + COD_CONVENZIONE + DATA_INIZ_VALID_CONV + FUNZIONALITA + DATA_RIFERIMENTO + LIVELLO_UTENTE + SESSION_ID + DATA_DECORR_POLIZZA + MODALITA_CALCOLO + Ispc0140RicalcQuiet.Len.RICALC_QUIET + DEE + ELE_MAX_SCHEDA_P + Ispc0140DatiInput.SCHEDA_P_MAXOCCURS * Ispc0140SchedaP.Len.SCHEDA_P + ELE_MAX_SCHEDA_T + Ispc0140DatiInput.SCHEDA_T_MAXOCCURS * Ispc0140SchedaT.Len.SCHEDA_T;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Ispc0140DatiInput
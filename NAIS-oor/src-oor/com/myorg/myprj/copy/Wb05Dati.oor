package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.format.PicParams;
import com.bphx.ctu.af.util.format.PicParser;
import com.bphx.ctu.af.util.format.PicUsage;

import com.myorg.myprj.ws.redefines.Wb05DtRis;
import com.myorg.myprj.ws.redefines.Wb05IdRichEstrazAgg;
import com.myorg.myprj.ws.redefines.Wb05ProgSchedaValor;
import com.myorg.myprj.ws.redefines.Wb05ValImp;
import com.myorg.myprj.ws.redefines.Wb05ValPc;

/**Original name: WB05-DATI<br>
 * Variable: WB05-DATI from copybook LCCVB051<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Wb05Dati {

	//==== PROPERTIES ====
	//Original name: WB05-ID-BILA-VAR-CALC-T
	private integer wb05IdBilaVarCalcT := DefaultValues.INT_VAL;
	//Original name: WB05-COD-COMP-ANIA
	private integer wb05CodCompAnia := DefaultValues.INT_VAL;
	//Original name: WB05-ID-BILA-TRCH-ESTR
	private integer wb05IdBilaTrchEstr := DefaultValues.INT_VAL;
	//Original name: WB05-ID-RICH-ESTRAZ-MAS
	private integer wb05IdRichEstrazMas := DefaultValues.INT_VAL;
	//Original name: WB05-ID-RICH-ESTRAZ-AGG
	private Wb05IdRichEstrazAgg wb05IdRichEstrazAgg := new Wb05IdRichEstrazAgg();
	//Original name: WB05-DT-RIS
	private Wb05DtRis wb05DtRis := new Wb05DtRis();
	//Original name: WB05-ID-POLI
	private integer wb05IdPoli := DefaultValues.INT_VAL;
	//Original name: WB05-ID-ADES
	private integer wb05IdAdes := DefaultValues.INT_VAL;
	//Original name: WB05-ID-TRCH-DI-GAR
	private integer wb05IdTrchDiGar := DefaultValues.INT_VAL;
	//Original name: WB05-PROG-SCHEDA-VALOR
	private Wb05ProgSchedaValor wb05ProgSchedaValor := new Wb05ProgSchedaValor();
	//Original name: WB05-DT-INI-VLDT-TARI
	private integer wb05DtIniVldtTari := DefaultValues.INT_VAL;
	//Original name: WB05-TP-RGM-FISC
	private string wb05TpRgmFisc := DefaultValues.stringVal(Len.WB05_TP_RGM_FISC);
	//Original name: WB05-DT-INI-VLDT-PROD
	private integer wb05DtIniVldtProd := DefaultValues.INT_VAL;
	//Original name: WB05-DT-DECOR-TRCH
	private integer wb05DtDecorTrch := DefaultValues.INT_VAL;
	//Original name: WB05-COD-VAR
	private string wb05CodVar := DefaultValues.stringVal(Len.WB05_COD_VAR);
	//Original name: WB05-TP-D
	private char wb05TpD := DefaultValues.CHAR_VAL;
	//Original name: WB05-VAL-IMP
	private Wb05ValImp wb05ValImp := new Wb05ValImp();
	//Original name: WB05-VAL-PC
	private Wb05ValPc wb05ValPc := new Wb05ValPc();
	//Original name: WB05-VAL-STRINGA
	private string wb05ValStringa := DefaultValues.stringVal(Len.WB05_VAL_STRINGA);
	//Original name: WB05-DS-OPER-SQL
	private char wb05DsOperSql := DefaultValues.CHAR_VAL;
	//Original name: WB05-DS-VER
	private integer wb05DsVer := DefaultValues.INT_VAL;
	//Original name: WB05-DS-TS-CPTZ
	private long wb05DsTsCptz := DefaultValues.LONG_VAL;
	//Original name: WB05-DS-UTENTE
	private string wb05DsUtente := DefaultValues.stringVal(Len.WB05_DS_UTENTE);
	//Original name: WB05-DS-STATO-ELAB
	private char wb05DsStatoElab := DefaultValues.CHAR_VAL;
	//Original name: WB05-AREA-D-VALOR-VAR-LEN
	private short wb05AreaDValorVarLen := DefaultValues.BIN_SHORT_VAL;
	//Original name: WB05-AREA-D-VALOR-VAR
	private string wb05AreaDValorVar := DefaultValues.stringVal(Len.WB05_AREA_D_VALOR_VAR);


	//==== METHODS ====
	public void setDatiBytes([]byte buffer, integer offset) {
		integer position := offset;
		wb05IdBilaVarCalcT := MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_ID_BILA_VAR_CALC_T, 0);
		position +:= Len.WB05_ID_BILA_VAR_CALC_T;
		wb05CodCompAnia := MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_COD_COMP_ANIA, 0);
		position +:= Len.WB05_COD_COMP_ANIA;
		wb05IdBilaTrchEstr := MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_ID_BILA_TRCH_ESTR, 0);
		position +:= Len.WB05_ID_BILA_TRCH_ESTR;
		wb05IdRichEstrazMas := MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_ID_RICH_ESTRAZ_MAS, 0);
		position +:= Len.WB05_ID_RICH_ESTRAZ_MAS;
		wb05IdRichEstrazAgg.setWb05IdRichEstrazAggFromBuffer(buffer, position);
		position +:= Wb05IdRichEstrazAgg.Len.WB05_ID_RICH_ESTRAZ_AGG;
		wb05DtRis.setWb05DtRisFromBuffer(buffer, position);
		position +:= Wb05DtRis.Len.WB05_DT_RIS;
		wb05IdPoli := MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_ID_POLI, 0);
		position +:= Len.WB05_ID_POLI;
		wb05IdAdes := MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_ID_ADES, 0);
		position +:= Len.WB05_ID_ADES;
		wb05IdTrchDiGar := MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_ID_TRCH_DI_GAR, 0);
		position +:= Len.WB05_ID_TRCH_DI_GAR;
		wb05ProgSchedaValor.setWb05ProgSchedaValorFromBuffer(buffer, position);
		position +:= Wb05ProgSchedaValor.Len.WB05_PROG_SCHEDA_VALOR;
		wb05DtIniVldtTari := MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_DT_INI_VLDT_TARI, 0);
		position +:= Len.WB05_DT_INI_VLDT_TARI;
		wb05TpRgmFisc := MarshalByte.readString(buffer, position, Len.WB05_TP_RGM_FISC);
		position +:= Len.WB05_TP_RGM_FISC;
		wb05DtIniVldtProd := MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_DT_INI_VLDT_PROD, 0);
		position +:= Len.WB05_DT_INI_VLDT_PROD;
		wb05DtDecorTrch := MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_DT_DECOR_TRCH, 0);
		position +:= Len.WB05_DT_DECOR_TRCH;
		wb05CodVar := MarshalByte.readString(buffer, position, Len.WB05_COD_VAR);
		position +:= Len.WB05_COD_VAR;
		wb05TpD := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		wb05ValImp.setWb05ValImpFromBuffer(buffer, position);
		position +:= Wb05ValImp.Len.WB05_VAL_IMP;
		wb05ValPc.setWb05ValPcFromBuffer(buffer, position);
		position +:= Wb05ValPc.Len.WB05_VAL_PC;
		wb05ValStringa := MarshalByte.readString(buffer, position, Len.WB05_VAL_STRINGA);
		position +:= Len.WB05_VAL_STRINGA;
		wb05DsOperSql := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		wb05DsVer := MarshalByte.readPackedAsInt(buffer, position, Len.Int.WB05_DS_VER, 0);
		position +:= Len.WB05_DS_VER;
		wb05DsTsCptz := MarshalByte.readPackedAsLong(buffer, position, Len.Int.WB05_DS_TS_CPTZ, 0);
		position +:= Len.WB05_DS_TS_CPTZ;
		wb05DsUtente := MarshalByte.readString(buffer, position, Len.WB05_DS_UTENTE);
		position +:= Len.WB05_DS_UTENTE;
		wb05DsStatoElab := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		setWb05AreaDValorVarVcharBytes(buffer, position);
	}

	public []byte getDatiBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeIntAsPacked(buffer, position, wb05IdBilaVarCalcT, Len.Int.WB05_ID_BILA_VAR_CALC_T, 0);
		position +:= Len.WB05_ID_BILA_VAR_CALC_T;
		MarshalByte.writeIntAsPacked(buffer, position, wb05CodCompAnia, Len.Int.WB05_COD_COMP_ANIA, 0);
		position +:= Len.WB05_COD_COMP_ANIA;
		MarshalByte.writeIntAsPacked(buffer, position, wb05IdBilaTrchEstr, Len.Int.WB05_ID_BILA_TRCH_ESTR, 0);
		position +:= Len.WB05_ID_BILA_TRCH_ESTR;
		MarshalByte.writeIntAsPacked(buffer, position, wb05IdRichEstrazMas, Len.Int.WB05_ID_RICH_ESTRAZ_MAS, 0);
		position +:= Len.WB05_ID_RICH_ESTRAZ_MAS;
		wb05IdRichEstrazAgg.getWb05IdRichEstrazAggAsBuffer(buffer, position);
		position +:= Wb05IdRichEstrazAgg.Len.WB05_ID_RICH_ESTRAZ_AGG;
		wb05DtRis.getWb05DtRisAsBuffer(buffer, position);
		position +:= Wb05DtRis.Len.WB05_DT_RIS;
		MarshalByte.writeIntAsPacked(buffer, position, wb05IdPoli, Len.Int.WB05_ID_POLI, 0);
		position +:= Len.WB05_ID_POLI;
		MarshalByte.writeIntAsPacked(buffer, position, wb05IdAdes, Len.Int.WB05_ID_ADES, 0);
		position +:= Len.WB05_ID_ADES;
		MarshalByte.writeIntAsPacked(buffer, position, wb05IdTrchDiGar, Len.Int.WB05_ID_TRCH_DI_GAR, 0);
		position +:= Len.WB05_ID_TRCH_DI_GAR;
		wb05ProgSchedaValor.getWb05ProgSchedaValorAsBuffer(buffer, position);
		position +:= Wb05ProgSchedaValor.Len.WB05_PROG_SCHEDA_VALOR;
		MarshalByte.writeIntAsPacked(buffer, position, wb05DtIniVldtTari, Len.Int.WB05_DT_INI_VLDT_TARI, 0);
		position +:= Len.WB05_DT_INI_VLDT_TARI;
		MarshalByte.writeString(buffer, position, wb05TpRgmFisc, Len.WB05_TP_RGM_FISC);
		position +:= Len.WB05_TP_RGM_FISC;
		MarshalByte.writeIntAsPacked(buffer, position, wb05DtIniVldtProd, Len.Int.WB05_DT_INI_VLDT_PROD, 0);
		position +:= Len.WB05_DT_INI_VLDT_PROD;
		MarshalByte.writeIntAsPacked(buffer, position, wb05DtDecorTrch, Len.Int.WB05_DT_DECOR_TRCH, 0);
		position +:= Len.WB05_DT_DECOR_TRCH;
		MarshalByte.writeString(buffer, position, wb05CodVar, Len.WB05_COD_VAR);
		position +:= Len.WB05_COD_VAR;
		MarshalByte.writeChar(buffer, position, wb05TpD);
		position +:= Types.CHAR_SIZE;
		wb05ValImp.getWb05ValImpAsBuffer(buffer, position);
		position +:= Wb05ValImp.Len.WB05_VAL_IMP;
		wb05ValPc.getWb05ValPcAsBuffer(buffer, position);
		position +:= Wb05ValPc.Len.WB05_VAL_PC;
		MarshalByte.writeString(buffer, position, wb05ValStringa, Len.WB05_VAL_STRINGA);
		position +:= Len.WB05_VAL_STRINGA;
		MarshalByte.writeChar(buffer, position, wb05DsOperSql);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, wb05DsVer, Len.Int.WB05_DS_VER, 0);
		position +:= Len.WB05_DS_VER;
		MarshalByte.writeLongAsPacked(buffer, position, wb05DsTsCptz, Len.Int.WB05_DS_TS_CPTZ, 0);
		position +:= Len.WB05_DS_TS_CPTZ;
		MarshalByte.writeString(buffer, position, wb05DsUtente, Len.WB05_DS_UTENTE);
		position +:= Len.WB05_DS_UTENTE;
		MarshalByte.writeChar(buffer, position, wb05DsStatoElab);
		position +:= Types.CHAR_SIZE;
		getWb05AreaDValorVarVcharBytes(buffer, position);
		return buffer;
	}

	public void setWb05IdBilaVarCalcT(integer wb05IdBilaVarCalcT) {
		this.wb05IdBilaVarCalcT:=wb05IdBilaVarCalcT;
	}

	public integer getWb05IdBilaVarCalcT() {
		return this.wb05IdBilaVarCalcT;
	}

	public void setWb05CodCompAnia(integer wb05CodCompAnia) {
		this.wb05CodCompAnia:=wb05CodCompAnia;
	}

	public integer getWb05CodCompAnia() {
		return this.wb05CodCompAnia;
	}

	public void setWb05IdBilaTrchEstr(integer wb05IdBilaTrchEstr) {
		this.wb05IdBilaTrchEstr:=wb05IdBilaTrchEstr;
	}

	public integer getWb05IdBilaTrchEstr() {
		return this.wb05IdBilaTrchEstr;
	}

	public void setWb05IdRichEstrazMas(integer wb05IdRichEstrazMas) {
		this.wb05IdRichEstrazMas:=wb05IdRichEstrazMas;
	}

	public integer getWb05IdRichEstrazMas() {
		return this.wb05IdRichEstrazMas;
	}

	public void setWb05IdPoli(integer wb05IdPoli) {
		this.wb05IdPoli:=wb05IdPoli;
	}

	public integer getWb05IdPoli() {
		return this.wb05IdPoli;
	}

	public void setWb05IdAdes(integer wb05IdAdes) {
		this.wb05IdAdes:=wb05IdAdes;
	}

	public integer getWb05IdAdes() {
		return this.wb05IdAdes;
	}

	public void setWb05IdTrchDiGar(integer wb05IdTrchDiGar) {
		this.wb05IdTrchDiGar:=wb05IdTrchDiGar;
	}

	public integer getWb05IdTrchDiGar() {
		return this.wb05IdTrchDiGar;
	}

	public void setWb05DtIniVldtTari(integer wb05DtIniVldtTari) {
		this.wb05DtIniVldtTari:=wb05DtIniVldtTari;
	}

	public void setWb05DtIniVldtTariFormatted(string wb05DtIniVldtTari) {
		setWb05DtIniVldtTari(PicParser.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).parseInt(wb05DtIniVldtTari));
	}

	public integer getWb05DtIniVldtTari() {
		return this.wb05DtIniVldtTari;
	}

	public void setWb05TpRgmFisc(string wb05TpRgmFisc) {
		this.wb05TpRgmFisc:=Functions.subString(wb05TpRgmFisc, Len.WB05_TP_RGM_FISC);
	}

	public string getWb05TpRgmFisc() {
		return this.wb05TpRgmFisc;
	}

	public void setWb05DtIniVldtProd(integer wb05DtIniVldtProd) {
		this.wb05DtIniVldtProd:=wb05DtIniVldtProd;
	}

	public void setWb05DtIniVldtProdFormatted(string wb05DtIniVldtProd) {
		setWb05DtIniVldtProd(PicParser.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).parseInt(wb05DtIniVldtProd));
	}

	public integer getWb05DtIniVldtProd() {
		return this.wb05DtIniVldtProd;
	}

	public void setWb05DtDecorTrch(integer wb05DtDecorTrch) {
		this.wb05DtDecorTrch:=wb05DtDecorTrch;
	}

	public void setWb05DtDecorTrchFormatted(string wb05DtDecorTrch) {
		setWb05DtDecorTrch(PicParser.display(new PicParams("S9(8)").setUsage(PicUsage.PACKED)).parseInt(wb05DtDecorTrch));
	}

	public integer getWb05DtDecorTrch() {
		return this.wb05DtDecorTrch;
	}

	public void setWb05CodVar(string wb05CodVar) {
		this.wb05CodVar:=Functions.subString(wb05CodVar, Len.WB05_COD_VAR);
	}

	public string getWb05CodVar() {
		return this.wb05CodVar;
	}

	public void setWb05TpD(char wb05TpD) {
		this.wb05TpD:=wb05TpD;
	}

	public char getWb05TpD() {
		return this.wb05TpD;
	}

	public void setWb05ValStringa(string wb05ValStringa) {
		this.wb05ValStringa:=Functions.subString(wb05ValStringa, Len.WB05_VAL_STRINGA);
	}

	public string getWb05ValStringa() {
		return this.wb05ValStringa;
	}

	public void setWb05DsOperSql(char wb05DsOperSql) {
		this.wb05DsOperSql:=wb05DsOperSql;
	}

	public char getWb05DsOperSql() {
		return this.wb05DsOperSql;
	}

	public void setWb05DsVer(integer wb05DsVer) {
		this.wb05DsVer:=wb05DsVer;
	}

	public integer getWb05DsVer() {
		return this.wb05DsVer;
	}

	public void setWb05DsTsCptz(long wb05DsTsCptz) {
		this.wb05DsTsCptz:=wb05DsTsCptz;
	}

	public long getWb05DsTsCptz() {
		return this.wb05DsTsCptz;
	}

	public void setWb05DsUtente(string wb05DsUtente) {
		this.wb05DsUtente:=Functions.subString(wb05DsUtente, Len.WB05_DS_UTENTE);
	}

	public string getWb05DsUtente() {
		return this.wb05DsUtente;
	}

	public void setWb05DsStatoElab(char wb05DsStatoElab) {
		this.wb05DsStatoElab:=wb05DsStatoElab;
	}

	public char getWb05DsStatoElab() {
		return this.wb05DsStatoElab;
	}

	/**Original name: WB05-AREA-D-VALOR-VAR-VCHAR<br>*/
	public []byte getWb05AreaDValorVarVcharBytes() {
		[]byte buffer := new [Len.WB05_AREA_D_VALOR_VAR_VCHAR]byte;
		return getWb05AreaDValorVarVcharBytes(buffer, 1);
	}

	public void setWb05AreaDValorVarVcharBytes([]byte buffer, integer offset) {
		integer position := offset;
		wb05AreaDValorVarLen := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		wb05AreaDValorVar := MarshalByte.readString(buffer, position, Len.WB05_AREA_D_VALOR_VAR);
	}

	public []byte getWb05AreaDValorVarVcharBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeBinaryShort(buffer, position, wb05AreaDValorVarLen);
		position +:= Types.SHORT_SIZE;
		MarshalByte.writeString(buffer, position, wb05AreaDValorVar, Len.WB05_AREA_D_VALOR_VAR);
		return buffer;
	}

	public void setWb05AreaDValorVarLen(short wb05AreaDValorVarLen) {
		this.wb05AreaDValorVarLen:=wb05AreaDValorVarLen;
	}

	public short getWb05AreaDValorVarLen() {
		return this.wb05AreaDValorVarLen;
	}

	public void setWb05AreaDValorVar(string wb05AreaDValorVar) {
		this.wb05AreaDValorVar:=Functions.subString(wb05AreaDValorVar, Len.WB05_AREA_D_VALOR_VAR);
	}

	public string getWb05AreaDValorVar() {
		return this.wb05AreaDValorVar;
	}

	public Wb05DtRis getWb05DtRis() {
		return wb05DtRis;
	}

	public Wb05IdRichEstrazAgg getWb05IdRichEstrazAgg() {
		return wb05IdRichEstrazAgg;
	}

	public Wb05ProgSchedaValor getWb05ProgSchedaValor() {
		return wb05ProgSchedaValor;
	}

	public Wb05ValImp getWb05ValImp() {
		return wb05ValImp;
	}

	public Wb05ValPc getWb05ValPc() {
		return wb05ValPc;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WB05_ID_BILA_VAR_CALC_T := 5;
		public final static integer WB05_COD_COMP_ANIA := 3;
		public final static integer WB05_ID_BILA_TRCH_ESTR := 5;
		public final static integer WB05_ID_RICH_ESTRAZ_MAS := 5;
		public final static integer WB05_ID_POLI := 5;
		public final static integer WB05_ID_ADES := 5;
		public final static integer WB05_ID_TRCH_DI_GAR := 5;
		public final static integer WB05_DT_INI_VLDT_TARI := 5;
		public final static integer WB05_TP_RGM_FISC := 2;
		public final static integer WB05_DT_INI_VLDT_PROD := 5;
		public final static integer WB05_DT_DECOR_TRCH := 5;
		public final static integer WB05_COD_VAR := 30;
		public final static integer WB05_TP_D := 1;
		public final static integer WB05_VAL_STRINGA := 60;
		public final static integer WB05_DS_OPER_SQL := 1;
		public final static integer WB05_DS_VER := 5;
		public final static integer WB05_DS_TS_CPTZ := 10;
		public final static integer WB05_DS_UTENTE := 20;
		public final static integer WB05_DS_STATO_ELAB := 1;
		public final static integer WB05_AREA_D_VALOR_VAR_LEN := 2;
		public final static integer WB05_AREA_D_VALOR_VAR := 4000;
		public final static integer WB05_AREA_D_VALOR_VAR_VCHAR := WB05_AREA_D_VALOR_VAR_LEN + WB05_AREA_D_VALOR_VAR;
		public final static integer DATI := WB05_ID_BILA_VAR_CALC_T + WB05_COD_COMP_ANIA + WB05_ID_BILA_TRCH_ESTR + WB05_ID_RICH_ESTRAZ_MAS + Wb05IdRichEstrazAgg.Len.WB05_ID_RICH_ESTRAZ_AGG + Wb05DtRis.Len.WB05_DT_RIS + WB05_ID_POLI + WB05_ID_ADES + WB05_ID_TRCH_DI_GAR + Wb05ProgSchedaValor.Len.WB05_PROG_SCHEDA_VALOR + WB05_DT_INI_VLDT_TARI + WB05_TP_RGM_FISC + WB05_DT_INI_VLDT_PROD + WB05_DT_DECOR_TRCH + WB05_COD_VAR + WB05_TP_D + Wb05ValImp.Len.WB05_VAL_IMP + Wb05ValPc.Len.WB05_VAL_PC + WB05_VAL_STRINGA + WB05_DS_OPER_SQL + WB05_DS_VER + WB05_DS_TS_CPTZ + WB05_DS_UTENTE + WB05_DS_STATO_ELAB + WB05_AREA_D_VALOR_VAR_VCHAR;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer WB05_ID_BILA_VAR_CALC_T := 9;
			public final static integer WB05_COD_COMP_ANIA := 5;
			public final static integer WB05_ID_BILA_TRCH_ESTR := 9;
			public final static integer WB05_ID_RICH_ESTRAZ_MAS := 9;
			public final static integer WB05_ID_POLI := 9;
			public final static integer WB05_ID_ADES := 9;
			public final static integer WB05_ID_TRCH_DI_GAR := 9;
			public final static integer WB05_DT_INI_VLDT_TARI := 8;
			public final static integer WB05_DT_INI_VLDT_PROD := 8;
			public final static integer WB05_DT_DECOR_TRCH := 8;
			public final static integer WB05_DS_VER := 9;
			public final static integer WB05_DS_TS_CPTZ := 18;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
	}
}//Wb05Dati
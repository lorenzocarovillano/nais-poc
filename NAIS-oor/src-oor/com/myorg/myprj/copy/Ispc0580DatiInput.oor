package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: ISPC0580-DATI-INPUT<br>
 * Variable: ISPC0580-DATI-INPUT from copybook ISPC0580<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Ispc0580DatiInput {

	//==== PROPERTIES ====
	//Original name: ISPC0580-COD-COMPAGNIA-ANIA
	private string codCompagniaAnia := DefaultValues.stringVal(Len.COD_COMPAGNIA_ANIA);
	//Original name: ISPC0580-DATA-INIZ-ELAB
	private string dataInizElab := DefaultValues.stringVal(Len.DATA_INIZ_ELAB);
	//Original name: ISPC0580-DATA-FINE-ELAB
	private string dataFineElab := DefaultValues.stringVal(Len.DATA_FINE_ELAB);
	//Original name: ISPC0580-DATA-RIFERIMENTO
	private string dataRiferimento := DefaultValues.stringVal(Len.DATA_RIFERIMENTO);
	//Original name: ISPC0580-LIVELLO-UTENTE
	private string livelloUtente := DefaultValues.stringVal(Len.LIVELLO_UTENTE);
	//Original name: ISPC0580-FUNZIONALITA
	private string funzionalita := DefaultValues.stringVal(Len.FUNZIONALITA);
	//Original name: ISPC0580-SESSION-ID
	private string sessionId := DefaultValues.stringVal(Len.SESSION_ID);


	//==== METHODS ====
	public void setDatiInputBytes([]byte buffer, integer offset) {
		integer position := offset;
		codCompagniaAnia := MarshalByte.readFixedString(buffer, position, Len.COD_COMPAGNIA_ANIA);
		position +:= Len.COD_COMPAGNIA_ANIA;
		dataInizElab := MarshalByte.readString(buffer, position, Len.DATA_INIZ_ELAB);
		position +:= Len.DATA_INIZ_ELAB;
		dataFineElab := MarshalByte.readString(buffer, position, Len.DATA_FINE_ELAB);
		position +:= Len.DATA_FINE_ELAB;
		dataRiferimento := MarshalByte.readString(buffer, position, Len.DATA_RIFERIMENTO);
		position +:= Len.DATA_RIFERIMENTO;
		livelloUtente := MarshalByte.readFixedString(buffer, position, Len.LIVELLO_UTENTE);
		position +:= Len.LIVELLO_UTENTE;
		funzionalita := MarshalByte.readFixedString(buffer, position, Len.FUNZIONALITA);
		position +:= Len.FUNZIONALITA;
		sessionId := MarshalByte.readString(buffer, position, Len.SESSION_ID);
	}

	public []byte getDatiInputBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, codCompagniaAnia, Len.COD_COMPAGNIA_ANIA);
		position +:= Len.COD_COMPAGNIA_ANIA;
		MarshalByte.writeString(buffer, position, dataInizElab, Len.DATA_INIZ_ELAB);
		position +:= Len.DATA_INIZ_ELAB;
		MarshalByte.writeString(buffer, position, dataFineElab, Len.DATA_FINE_ELAB);
		position +:= Len.DATA_FINE_ELAB;
		MarshalByte.writeString(buffer, position, dataRiferimento, Len.DATA_RIFERIMENTO);
		position +:= Len.DATA_RIFERIMENTO;
		MarshalByte.writeString(buffer, position, livelloUtente, Len.LIVELLO_UTENTE);
		position +:= Len.LIVELLO_UTENTE;
		MarshalByte.writeString(buffer, position, funzionalita, Len.FUNZIONALITA);
		position +:= Len.FUNZIONALITA;
		MarshalByte.writeString(buffer, position, sessionId, Len.SESSION_ID);
		return buffer;
	}

	public void setCodCompagniaAniaFormatted(string codCompagniaAnia) {
		this.codCompagniaAnia:=Trunc.toUnsignedNumeric(codCompagniaAnia, Len.COD_COMPAGNIA_ANIA);
	}

	public integer getCodCompagniaAnia() {
		return NumericDisplay.asInt(this.codCompagniaAnia);
	}

	public void setDataInizElab(string dataInizElab) {
		this.dataInizElab:=Functions.subString(dataInizElab, Len.DATA_INIZ_ELAB);
	}

	public string getDataInizElab() {
		return this.dataInizElab;
	}

	public void setDataFineElab(string dataFineElab) {
		this.dataFineElab:=Functions.subString(dataFineElab, Len.DATA_FINE_ELAB);
	}

	public string getDataFineElab() {
		return this.dataFineElab;
	}

	public void setDataRiferimento(string dataRiferimento) {
		this.dataRiferimento:=Functions.subString(dataRiferimento, Len.DATA_RIFERIMENTO);
	}

	public string getDataRiferimento() {
		return this.dataRiferimento;
	}

	public void setIspc0580LivelloUtente(short ispc0580LivelloUtente) {
		this.livelloUtente := NumericDisplay.asString(ispc0580LivelloUtente, Len.LIVELLO_UTENTE);
	}

	public void setIspc0580LivelloUtenteFormatted(string ispc0580LivelloUtente) {
		this.livelloUtente:=Trunc.toUnsignedNumeric(ispc0580LivelloUtente, Len.LIVELLO_UTENTE);
	}

	public short getIspc0580LivelloUtente() {
		return NumericDisplay.asShort(this.livelloUtente);
	}

	public void setIspc0580FunzionalitaFormatted(string ispc0580Funzionalita) {
		this.funzionalita:=Trunc.toUnsignedNumeric(ispc0580Funzionalita, Len.FUNZIONALITA);
	}

	public integer getIspc0580Funzionalita() {
		return NumericDisplay.asInt(this.funzionalita);
	}

	public void setSessionId(string sessionId) {
		this.sessionId:=Functions.subString(sessionId, Len.SESSION_ID);
	}

	public string getSessionId() {
		return this.sessionId;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer COD_COMPAGNIA_ANIA := 5;
		public final static integer DATA_INIZ_ELAB := 8;
		public final static integer DATA_FINE_ELAB := 8;
		public final static integer DATA_RIFERIMENTO := 8;
		public final static integer LIVELLO_UTENTE := 2;
		public final static integer FUNZIONALITA := 5;
		public final static integer SESSION_ID := 20;
		public final static integer DATI_INPUT := COD_COMPAGNIA_ANIA + DATA_INIZ_ELAB + DATA_FINE_ELAB + DATA_RIFERIMENTO + LIVELLO_UTENTE + FUNZIONALITA + SESSION_ID;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Ispc0580DatiInput
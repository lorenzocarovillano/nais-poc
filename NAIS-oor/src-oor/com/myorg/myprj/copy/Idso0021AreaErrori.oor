package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.ws.enums.Idso0011ReturnCode;
import com.myorg.myprj.ws.enums.Idso0011SqlcodeSigned;

/**Original name: IDSO0021-AREA-ERRORI<br>
 * Variable: IDSO0021-AREA-ERRORI from copybook IDSO0021<br>
 * Generated as a class for rule COPYBOOK_RECORDS_THRESHOLD.<br>*/
public class Idso0021AreaErrori {

	//==== PROPERTIES ====
	/**Original name: IDSO0021-RETURN-CODE<br>
	 * <pre> --  CAMPI ERRORI
	 *           COPY IDSV0006 REPLACING ==IDSO0021== BY ==IDSO0021==.
	 *    inizio COPY IDSV0006
	 *    inizio COPY IDSV0004</pre>*/
	private Idso0011ReturnCode returnCode := new Idso0011ReturnCode();
	/**Original name: IDSO0021-SQLCODE<br>
	 * <pre>   fine COPY IDSV0004</pre>*/
	private Idso0011SqlcodeSigned sqlcode := new Idso0011SqlcodeSigned();
	//Original name: IDSO0021-DESCRIZ-ERR-DB2
	private string descrizErrDb2 := DefaultValues.stringVal(Len.DESCRIZ_ERR_DB2);
	//Original name: IDSO0021-COD-SERVIZIO-BE
	private string codServizioBe := DefaultValues.stringVal(Len.COD_SERVIZIO_BE);
	//Original name: IDSO0021-NOME-TABELLA
	private string nomeTabella := DefaultValues.stringVal(Len.NOME_TABELLA);
	//Original name: IDSO0021-KEY-TABELLA
	private string keyTabella := DefaultValues.stringVal(Len.KEY_TABELLA);
	//Original name: IDSO0021-NUM-RIGHE-LETTE
	private string numRigheLette := DefaultValues.stringVal(Len.NUM_RIGHE_LETTE);


	//==== METHODS ====
	public void setIdso0021AreaErroriBytes([]byte buffer, integer offset) {
		integer position := offset;
		returnCode.setReturnCode(MarshalByte.readString(buffer, position, Idso0011ReturnCode.Len.RETURN_CODE));
		position +:= Idso0011ReturnCode.Len.RETURN_CODE;
		sqlcode.setSqlcodeSigned(MarshalByte.readInt(buffer, position, Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED));
		position +:= Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
		setCampiEsitoBytes(buffer, position);
	}

	public []byte getIdso0021AreaErroriBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, returnCode.getReturnCode(), Idso0011ReturnCode.Len.RETURN_CODE);
		position +:= Idso0011ReturnCode.Len.RETURN_CODE;
		MarshalByte.writeInt(buffer, position, sqlcode.getSqlcodeSigned(), Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED);
		position +:= Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED;
		getCampiEsitoBytes(buffer, position);
		return buffer;
	}

	public void initIdso0021AreaErroriHighValues() {
		returnCode.setReturnCode(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Idso0011ReturnCode.Len.RETURN_CODE));
		sqlcode.setSqlcodeSigned(Types.HIGH_INT_VAL);
		initCampiEsitoHighValues();
	}

	public void initIdso0021AreaErroriSpaces() {
		returnCode.setReturnCode("");
		sqlcode.setSqlcodeSigned(Types.INVALID_INT_VAL);
		initIdso0021CampiEsitoSpaces();
	}

	public void setCampiEsitoBytes([]byte buffer, integer offset) {
		integer position := offset;
		descrizErrDb2 := MarshalByte.readString(buffer, position, Len.DESCRIZ_ERR_DB2);
		position +:= Len.DESCRIZ_ERR_DB2;
		codServizioBe := MarshalByte.readString(buffer, position, Len.COD_SERVIZIO_BE);
		position +:= Len.COD_SERVIZIO_BE;
		nomeTabella := MarshalByte.readString(buffer, position, Len.NOME_TABELLA);
		position +:= Len.NOME_TABELLA;
		keyTabella := MarshalByte.readString(buffer, position, Len.KEY_TABELLA);
		position +:= Len.KEY_TABELLA;
		numRigheLette := MarshalByte.readFixedString(buffer, position, Len.NUM_RIGHE_LETTE);
	}

	public []byte getCampiEsitoBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, descrizErrDb2, Len.DESCRIZ_ERR_DB2);
		position +:= Len.DESCRIZ_ERR_DB2;
		MarshalByte.writeString(buffer, position, codServizioBe, Len.COD_SERVIZIO_BE);
		position +:= Len.COD_SERVIZIO_BE;
		MarshalByte.writeString(buffer, position, nomeTabella, Len.NOME_TABELLA);
		position +:= Len.NOME_TABELLA;
		MarshalByte.writeString(buffer, position, keyTabella, Len.KEY_TABELLA);
		position +:= Len.KEY_TABELLA;
		MarshalByte.writeString(buffer, position, numRigheLette, Len.NUM_RIGHE_LETTE);
		return buffer;
	}

	public void initCampiEsitoHighValues() {
		descrizErrDb2 := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.DESCRIZ_ERR_DB2);
		codServizioBe := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.COD_SERVIZIO_BE);
		nomeTabella := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.NOME_TABELLA);
		keyTabella := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.KEY_TABELLA);
		numRigheLette := LiteralGenerator.create(Types.HIGH_CHAR_VAL, Len.NUM_RIGHE_LETTE);
	}

	public void initIdso0021CampiEsitoSpaces() {
		descrizErrDb2 := "";
		codServizioBe := "";
		nomeTabella := "";
		keyTabella := "";
		numRigheLette := "";
	}

	public void setDescrizErrDb2(string descrizErrDb2) {
		this.descrizErrDb2:=Functions.subString(descrizErrDb2, Len.DESCRIZ_ERR_DB2);
	}

	public string getDescrizErrDb2() {
		return this.descrizErrDb2;
	}

	public string getDescrizErrDb2Formatted() {
		return Functions.padBlanks(getDescrizErrDb2(), Len.DESCRIZ_ERR_DB2);
	}

	public void setCodServizioBe(string codServizioBe) {
		this.codServizioBe:=Functions.subString(codServizioBe, Len.COD_SERVIZIO_BE);
	}

	public string getCodServizioBe() {
		return this.codServizioBe;
	}

	public void setNomeTabella(string nomeTabella) {
		this.nomeTabella:=Functions.subString(nomeTabella, Len.NOME_TABELLA);
	}

	public string getNomeTabella() {
		return this.nomeTabella;
	}

	public void setKeyTabella(string keyTabella) {
		this.keyTabella:=Functions.subString(keyTabella, Len.KEY_TABELLA);
	}

	public string getKeyTabella() {
		return this.keyTabella;
	}

	public void setIdso0021NumRigheLetteFormatted(string idso0021NumRigheLette) {
		this.numRigheLette:=Trunc.toUnsignedNumeric(idso0021NumRigheLette, Len.NUM_RIGHE_LETTE);
	}

	public short getIdso0021NumRigheLette() {
		return NumericDisplay.asShort(this.numRigheLette);
	}

	public Idso0011ReturnCode getReturnCode() {
		return returnCode;
	}

	public Idso0011SqlcodeSigned getSqlcode() {
		return sqlcode;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer DESCRIZ_ERR_DB2 := 300;
		public final static integer COD_SERVIZIO_BE := 8;
		public final static integer NOME_TABELLA := 18;
		public final static integer KEY_TABELLA := 20;
		public final static integer NUM_RIGHE_LETTE := 2;
		public final static integer CAMPI_ESITO := DESCRIZ_ERR_DB2 + COD_SERVIZIO_BE + NOME_TABELLA + KEY_TABELLA + NUM_RIGHE_LETTE;
		public final static integer IDSO0021_AREA_ERRORI := Idso0011ReturnCode.Len.RETURN_CODE + Idso0011SqlcodeSigned.Len.SQLCODE_SIGNED + CAMPI_ESITO;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Idso0021AreaErrori

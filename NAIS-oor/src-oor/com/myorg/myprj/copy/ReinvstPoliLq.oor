package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.marshal.SignType;
import com.bphx.ctu.af.util.Functions;

import com.myorg.myprj.ws.redefines.L30CodSubAge;
import com.myorg.myprj.ws.redefines.L30DtDecorPoliLq;
import com.myorg.myprj.ws.redefines.L30IdMoviChiu;
import com.myorg.myprj.ws.redefines.L30TpInvstLiq;

/**Original name: REINVST-POLI-LQ<br>
 * Variable: REINVST-POLI-LQ from copybook IDBVL301<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class ReinvstPoliLq {

	//==== PROPERTIES ====
	//Original name: L30-ID-REINVST-POLI-LQ
	private integer l30IdReinvstPoliLq := DefaultValues.INT_VAL;
	//Original name: L30-ID-MOVI-CRZ
	private integer l30IdMoviCrz := DefaultValues.INT_VAL;
	//Original name: L30-ID-MOVI-CHIU
	private L30IdMoviChiu l30IdMoviChiu := new L30IdMoviChiu();
	//Original name: L30-DT-INI-EFF
	private integer l30DtIniEff := DefaultValues.INT_VAL;
	//Original name: L30-DT-END-EFF
	private integer l30DtEndEff := DefaultValues.INT_VAL;
	//Original name: L30-COD-COMP-ANIA
	private integer l30CodCompAnia := DefaultValues.INT_VAL;
	//Original name: L30-COD-RAMO
	private string l30CodRamo := DefaultValues.stringVal(Len.L30_COD_RAMO);
	//Original name: L30-IB-POLI
	private string l30IbPoli := DefaultValues.stringVal(Len.L30_IB_POLI);
	//Original name: L30-PR-KEY-SIST-ESTNO
	private string l30PrKeySistEstno := DefaultValues.stringVal(Len.L30_PR_KEY_SIST_ESTNO);
	//Original name: L30-SEC-KEY-SIST-ESTNO
	private string l30SecKeySistEstno := DefaultValues.stringVal(Len.L30_SEC_KEY_SIST_ESTNO);
	//Original name: L30-COD-CAN
	private integer l30CodCan := DefaultValues.INT_VAL;
	//Original name: L30-COD-AGE
	private integer l30CodAge := DefaultValues.INT_VAL;
	//Original name: L30-COD-SUB-AGE
	private L30CodSubAge l30CodSubAge := new L30CodSubAge();
	//Original name: L30-IMP-RES
	private decimal(15,3) l30ImpRes := DefaultValues.DEC_VAL;
	//Original name: L30-TP-LIQ
	private string l30TpLiq := DefaultValues.stringVal(Len.L30_TP_LIQ);
	//Original name: L30-TP-SIST-ESTNO
	private string l30TpSistEstno := DefaultValues.stringVal(Len.L30_TP_SIST_ESTNO);
	//Original name: L30-COD-FISC-PART-IVA
	private string l30CodFiscPartIva := DefaultValues.stringVal(Len.L30_COD_FISC_PART_IVA);
	//Original name: L30-COD-MOVI-LIQ
	private integer l30CodMoviLiq := DefaultValues.INT_VAL;
	//Original name: L30-TP-INVST-LIQ
	private L30TpInvstLiq l30TpInvstLiq := new L30TpInvstLiq();
	//Original name: L30-IMP-TOT-LIQ
	private decimal(15,3) l30ImpTotLiq := DefaultValues.DEC_VAL;
	//Original name: L30-DS-RIGA
	private long l30DsRiga := DefaultValues.LONG_VAL;
	//Original name: L30-DS-OPER-SQL
	private char l30DsOperSql := DefaultValues.CHAR_VAL;
	//Original name: L30-DS-VER
	private integer l30DsVer := DefaultValues.INT_VAL;
	//Original name: L30-DS-TS-INI-CPTZ
	private long l30DsTsIniCptz := DefaultValues.LONG_VAL;
	//Original name: L30-DS-TS-END-CPTZ
	private long l30DsTsEndCptz := DefaultValues.LONG_VAL;
	//Original name: L30-DS-UTENTE
	private string l30DsUtente := DefaultValues.stringVal(Len.L30_DS_UTENTE);
	//Original name: L30-DS-STATO-ELAB
	private char l30DsStatoElab := DefaultValues.CHAR_VAL;
	//Original name: L30-DT-DECOR-POLI-LQ
	private L30DtDecorPoliLq l30DtDecorPoliLq := new L30DtDecorPoliLq();


	//==== METHODS ====
	public void setReinvstPoliLqFormatted(string data) {
		[]byte buffer := new [Len.REINVST_POLI_LQ]byte;
		MarshalByte.writeString(buffer, 1, data, Len.REINVST_POLI_LQ);
		setReinvstPoliLqBytes(buffer, 1);
	}

	public string getReinvstPoliLqFormatted() {
		return MarshalByteExt.bufferToStr(getReinvstPoliLqBytes());
	}

	public []byte getReinvstPoliLqBytes() {
		[]byte buffer := new [Len.REINVST_POLI_LQ]byte;
		return getReinvstPoliLqBytes(buffer, 1);
	}

	public void setReinvstPoliLqBytes([]byte buffer, integer offset) {
		integer position := offset;
		l30IdReinvstPoliLq := MarshalByte.readPackedAsInt(buffer, position, Len.Int.L30_ID_REINVST_POLI_LQ, 0);
		position +:= Len.L30_ID_REINVST_POLI_LQ;
		l30IdMoviCrz := MarshalByte.readPackedAsInt(buffer, position, Len.Int.L30_ID_MOVI_CRZ, 0);
		position +:= Len.L30_ID_MOVI_CRZ;
		l30IdMoviChiu.setL30IdMoviChiuFromBuffer(buffer, position);
		position +:= L30IdMoviChiu.Len.L30_ID_MOVI_CHIU;
		l30DtIniEff := MarshalByte.readPackedAsInt(buffer, position, Len.Int.L30_DT_INI_EFF, 0);
		position +:= Len.L30_DT_INI_EFF;
		l30DtEndEff := MarshalByte.readPackedAsInt(buffer, position, Len.Int.L30_DT_END_EFF, 0);
		position +:= Len.L30_DT_END_EFF;
		l30CodCompAnia := MarshalByte.readPackedAsInt(buffer, position, Len.Int.L30_COD_COMP_ANIA, 0);
		position +:= Len.L30_COD_COMP_ANIA;
		l30CodRamo := MarshalByte.readString(buffer, position, Len.L30_COD_RAMO);
		position +:= Len.L30_COD_RAMO;
		l30IbPoli := MarshalByte.readString(buffer, position, Len.L30_IB_POLI);
		position +:= Len.L30_IB_POLI;
		l30PrKeySistEstno := MarshalByte.readString(buffer, position, Len.L30_PR_KEY_SIST_ESTNO);
		position +:= Len.L30_PR_KEY_SIST_ESTNO;
		l30SecKeySistEstno := MarshalByte.readString(buffer, position, Len.L30_SEC_KEY_SIST_ESTNO);
		position +:= Len.L30_SEC_KEY_SIST_ESTNO;
		l30CodCan := MarshalByte.readPackedAsInt(buffer, position, Len.Int.L30_COD_CAN, 0);
		position +:= Len.L30_COD_CAN;
		l30CodAge := MarshalByte.readPackedAsInt(buffer, position, Len.Int.L30_COD_AGE, 0);
		position +:= Len.L30_COD_AGE;
		l30CodSubAge.setL30CodSubAgeFromBuffer(buffer, position);
		position +:= L30CodSubAge.Len.L30_COD_SUB_AGE;
		l30ImpRes := MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.L30_IMP_RES, Len.Fract.L30_IMP_RES);
		position +:= Len.L30_IMP_RES;
		l30TpLiq := MarshalByte.readString(buffer, position, Len.L30_TP_LIQ);
		position +:= Len.L30_TP_LIQ;
		l30TpSistEstno := MarshalByte.readString(buffer, position, Len.L30_TP_SIST_ESTNO);
		position +:= Len.L30_TP_SIST_ESTNO;
		l30CodFiscPartIva := MarshalByte.readString(buffer, position, Len.L30_COD_FISC_PART_IVA);
		position +:= Len.L30_COD_FISC_PART_IVA;
		l30CodMoviLiq := MarshalByte.readPackedAsInt(buffer, position, Len.Int.L30_COD_MOVI_LIQ, 0);
		position +:= Len.L30_COD_MOVI_LIQ;
		l30TpInvstLiq.setL30TpInvstLiqFromBuffer(buffer, position);
		position +:= L30TpInvstLiq.Len.L30_TP_INVST_LIQ;
		l30ImpTotLiq := MarshalByte.readPackedAsDecimal(buffer, position, Len.Int.L30_IMP_TOT_LIQ, Len.Fract.L30_IMP_TOT_LIQ);
		position +:= Len.L30_IMP_TOT_LIQ;
		l30DsRiga := MarshalByte.readPackedAsLong(buffer, position, Len.Int.L30_DS_RIGA, 0);
		position +:= Len.L30_DS_RIGA;
		l30DsOperSql := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		l30DsVer := MarshalByte.readPackedAsInt(buffer, position, Len.Int.L30_DS_VER, 0);
		position +:= Len.L30_DS_VER;
		l30DsTsIniCptz := MarshalByte.readPackedAsLong(buffer, position, Len.Int.L30_DS_TS_INI_CPTZ, 0);
		position +:= Len.L30_DS_TS_INI_CPTZ;
		l30DsTsEndCptz := MarshalByte.readPackedAsLong(buffer, position, Len.Int.L30_DS_TS_END_CPTZ, 0);
		position +:= Len.L30_DS_TS_END_CPTZ;
		l30DsUtente := MarshalByte.readString(buffer, position, Len.L30_DS_UTENTE);
		position +:= Len.L30_DS_UTENTE;
		l30DsStatoElab := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		l30DtDecorPoliLq.setL30DtDecorPoliLqFromBuffer(buffer, position);
	}

	public []byte getReinvstPoliLqBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeIntAsPacked(buffer, position, l30IdReinvstPoliLq, Len.Int.L30_ID_REINVST_POLI_LQ, 0);
		position +:= Len.L30_ID_REINVST_POLI_LQ;
		MarshalByte.writeIntAsPacked(buffer, position, l30IdMoviCrz, Len.Int.L30_ID_MOVI_CRZ, 0);
		position +:= Len.L30_ID_MOVI_CRZ;
		l30IdMoviChiu.getL30IdMoviChiuAsBuffer(buffer, position);
		position +:= L30IdMoviChiu.Len.L30_ID_MOVI_CHIU;
		MarshalByte.writeIntAsPacked(buffer, position, l30DtIniEff, Len.Int.L30_DT_INI_EFF, 0);
		position +:= Len.L30_DT_INI_EFF;
		MarshalByte.writeIntAsPacked(buffer, position, l30DtEndEff, Len.Int.L30_DT_END_EFF, 0);
		position +:= Len.L30_DT_END_EFF;
		MarshalByte.writeIntAsPacked(buffer, position, l30CodCompAnia, Len.Int.L30_COD_COMP_ANIA, 0);
		position +:= Len.L30_COD_COMP_ANIA;
		MarshalByte.writeString(buffer, position, l30CodRamo, Len.L30_COD_RAMO);
		position +:= Len.L30_COD_RAMO;
		MarshalByte.writeString(buffer, position, l30IbPoli, Len.L30_IB_POLI);
		position +:= Len.L30_IB_POLI;
		MarshalByte.writeString(buffer, position, l30PrKeySistEstno, Len.L30_PR_KEY_SIST_ESTNO);
		position +:= Len.L30_PR_KEY_SIST_ESTNO;
		MarshalByte.writeString(buffer, position, l30SecKeySistEstno, Len.L30_SEC_KEY_SIST_ESTNO);
		position +:= Len.L30_SEC_KEY_SIST_ESTNO;
		MarshalByte.writeIntAsPacked(buffer, position, l30CodCan, Len.Int.L30_COD_CAN, 0);
		position +:= Len.L30_COD_CAN;
		MarshalByte.writeIntAsPacked(buffer, position, l30CodAge, Len.Int.L30_COD_AGE, 0);
		position +:= Len.L30_COD_AGE;
		l30CodSubAge.getL30CodSubAgeAsBuffer(buffer, position);
		position +:= L30CodSubAge.Len.L30_COD_SUB_AGE;
		MarshalByte.writeDecimalAsPacked(buffer, position, l30ImpRes);
		position +:= Len.L30_IMP_RES;
		MarshalByte.writeString(buffer, position, l30TpLiq, Len.L30_TP_LIQ);
		position +:= Len.L30_TP_LIQ;
		MarshalByte.writeString(buffer, position, l30TpSistEstno, Len.L30_TP_SIST_ESTNO);
		position +:= Len.L30_TP_SIST_ESTNO;
		MarshalByte.writeString(buffer, position, l30CodFiscPartIva, Len.L30_COD_FISC_PART_IVA);
		position +:= Len.L30_COD_FISC_PART_IVA;
		MarshalByte.writeIntAsPacked(buffer, position, l30CodMoviLiq, Len.Int.L30_COD_MOVI_LIQ, 0);
		position +:= Len.L30_COD_MOVI_LIQ;
		l30TpInvstLiq.getL30TpInvstLiqAsBuffer(buffer, position);
		position +:= L30TpInvstLiq.Len.L30_TP_INVST_LIQ;
		MarshalByte.writeDecimalAsPacked(buffer, position, l30ImpTotLiq);
		position +:= Len.L30_IMP_TOT_LIQ;
		MarshalByte.writeLongAsPacked(buffer, position, l30DsRiga, Len.Int.L30_DS_RIGA, 0);
		position +:= Len.L30_DS_RIGA;
		MarshalByte.writeChar(buffer, position, l30DsOperSql);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeIntAsPacked(buffer, position, l30DsVer, Len.Int.L30_DS_VER, 0);
		position +:= Len.L30_DS_VER;
		MarshalByte.writeLongAsPacked(buffer, position, l30DsTsIniCptz, Len.Int.L30_DS_TS_INI_CPTZ, 0);
		position +:= Len.L30_DS_TS_INI_CPTZ;
		MarshalByte.writeLongAsPacked(buffer, position, l30DsTsEndCptz, Len.Int.L30_DS_TS_END_CPTZ, 0);
		position +:= Len.L30_DS_TS_END_CPTZ;
		MarshalByte.writeString(buffer, position, l30DsUtente, Len.L30_DS_UTENTE);
		position +:= Len.L30_DS_UTENTE;
		MarshalByte.writeChar(buffer, position, l30DsStatoElab);
		position +:= Types.CHAR_SIZE;
		l30DtDecorPoliLq.getL30DtDecorPoliLqAsBuffer(buffer, position);
		return buffer;
	}

	public void setL30IdReinvstPoliLq(integer l30IdReinvstPoliLq) {
		this.l30IdReinvstPoliLq:=l30IdReinvstPoliLq;
	}

	public integer getL30IdReinvstPoliLq() {
		return this.l30IdReinvstPoliLq;
	}

	public void setL30IdMoviCrz(integer l30IdMoviCrz) {
		this.l30IdMoviCrz:=l30IdMoviCrz;
	}

	public integer getL30IdMoviCrz() {
		return this.l30IdMoviCrz;
	}

	public void setL30DtIniEff(integer l30DtIniEff) {
		this.l30DtIniEff:=l30DtIniEff;
	}

	public integer getL30DtIniEff() {
		return this.l30DtIniEff;
	}

	public void setL30DtEndEff(integer l30DtEndEff) {
		this.l30DtEndEff:=l30DtEndEff;
	}

	public integer getL30DtEndEff() {
		return this.l30DtEndEff;
	}

	public void setL30CodCompAnia(integer l30CodCompAnia) {
		this.l30CodCompAnia:=l30CodCompAnia;
	}

	public integer getL30CodCompAnia() {
		return this.l30CodCompAnia;
	}

	public void setL30CodRamo(string l30CodRamo) {
		this.l30CodRamo:=Functions.subString(l30CodRamo, Len.L30_COD_RAMO);
	}

	public string getL30CodRamo() {
		return this.l30CodRamo;
	}

	public void setL30IbPoli(string l30IbPoli) {
		this.l30IbPoli:=Functions.subString(l30IbPoli, Len.L30_IB_POLI);
	}

	public string getL30IbPoli() {
		return this.l30IbPoli;
	}

	public void setL30PrKeySistEstno(string l30PrKeySistEstno) {
		this.l30PrKeySistEstno:=Functions.subString(l30PrKeySistEstno, Len.L30_PR_KEY_SIST_ESTNO);
	}

	public string getL30PrKeySistEstno() {
		return this.l30PrKeySistEstno;
	}

	public void setL30SecKeySistEstno(string l30SecKeySistEstno) {
		this.l30SecKeySistEstno:=Functions.subString(l30SecKeySistEstno, Len.L30_SEC_KEY_SIST_ESTNO);
	}

	public string getL30SecKeySistEstno() {
		return this.l30SecKeySistEstno;
	}

	public void setL30CodCan(integer l30CodCan) {
		this.l30CodCan:=l30CodCan;
	}

	public integer getL30CodCan() {
		return this.l30CodCan;
	}

	public void setL30CodAge(integer l30CodAge) {
		this.l30CodAge:=l30CodAge;
	}

	public integer getL30CodAge() {
		return this.l30CodAge;
	}

	public void setL30ImpRes(decimal(15,3) l30ImpRes) {
		this.l30ImpRes:=l30ImpRes;
	}

	public decimal(15,3) getL30ImpRes() {
		return this.l30ImpRes;
	}

	public void setL30TpLiq(string l30TpLiq) {
		this.l30TpLiq:=Functions.subString(l30TpLiq, Len.L30_TP_LIQ);
	}

	public string getL30TpLiq() {
		return this.l30TpLiq;
	}

	public void setL30TpSistEstno(string l30TpSistEstno) {
		this.l30TpSistEstno:=Functions.subString(l30TpSistEstno, Len.L30_TP_SIST_ESTNO);
	}

	public string getL30TpSistEstno() {
		return this.l30TpSistEstno;
	}

	public void setL30CodFiscPartIva(string l30CodFiscPartIva) {
		this.l30CodFiscPartIva:=Functions.subString(l30CodFiscPartIva, Len.L30_COD_FISC_PART_IVA);
	}

	public string getL30CodFiscPartIva() {
		return this.l30CodFiscPartIva;
	}

	public void setL30CodMoviLiq(integer l30CodMoviLiq) {
		this.l30CodMoviLiq:=l30CodMoviLiq;
	}

	public integer getL30CodMoviLiq() {
		return this.l30CodMoviLiq;
	}

	public void setL30ImpTotLiq(decimal(15,3) l30ImpTotLiq) {
		this.l30ImpTotLiq:=l30ImpTotLiq;
	}

	public decimal(15,3) getL30ImpTotLiq() {
		return this.l30ImpTotLiq;
	}

	public void setL30DsRiga(long l30DsRiga) {
		this.l30DsRiga:=l30DsRiga;
	}

	public long getL30DsRiga() {
		return this.l30DsRiga;
	}

	public void setL30DsOperSql(char l30DsOperSql) {
		this.l30DsOperSql:=l30DsOperSql;
	}

	public char getL30DsOperSql() {
		return this.l30DsOperSql;
	}

	public void setL30DsVer(integer l30DsVer) {
		this.l30DsVer:=l30DsVer;
	}

	public integer getL30DsVer() {
		return this.l30DsVer;
	}

	public void setL30DsTsIniCptz(long l30DsTsIniCptz) {
		this.l30DsTsIniCptz:=l30DsTsIniCptz;
	}

	public long getL30DsTsIniCptz() {
		return this.l30DsTsIniCptz;
	}

	public void setL30DsTsEndCptz(long l30DsTsEndCptz) {
		this.l30DsTsEndCptz:=l30DsTsEndCptz;
	}

	public long getL30DsTsEndCptz() {
		return this.l30DsTsEndCptz;
	}

	public void setL30DsUtente(string l30DsUtente) {
		this.l30DsUtente:=Functions.subString(l30DsUtente, Len.L30_DS_UTENTE);
	}

	public string getL30DsUtente() {
		return this.l30DsUtente;
	}

	public void setL30DsStatoElab(char l30DsStatoElab) {
		this.l30DsStatoElab:=l30DsStatoElab;
	}

	public char getL30DsStatoElab() {
		return this.l30DsStatoElab;
	}

	public L30CodSubAge getL30CodSubAge() {
		return l30CodSubAge;
	}

	public L30DtDecorPoliLq getL30DtDecorPoliLq() {
		return l30DtDecorPoliLq;
	}

	public L30IdMoviChiu getL30IdMoviChiu() {
		return l30IdMoviChiu;
	}

	public L30TpInvstLiq getL30TpInvstLiq() {
		return l30TpInvstLiq;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer L30_COD_RAMO := 12;
		public final static integer L30_IB_POLI := 40;
		public final static integer L30_PR_KEY_SIST_ESTNO := 40;
		public final static integer L30_SEC_KEY_SIST_ESTNO := 40;
		public final static integer L30_TP_LIQ := 2;
		public final static integer L30_TP_SIST_ESTNO := 2;
		public final static integer L30_COD_FISC_PART_IVA := 16;
		public final static integer L30_DS_UTENTE := 20;
		public final static integer L30_ID_REINVST_POLI_LQ := 5;
		public final static integer L30_ID_MOVI_CRZ := 5;
		public final static integer L30_DT_INI_EFF := 5;
		public final static integer L30_DT_END_EFF := 5;
		public final static integer L30_COD_COMP_ANIA := 3;
		public final static integer L30_COD_CAN := 3;
		public final static integer L30_COD_AGE := 3;
		public final static integer L30_IMP_RES := 8;
		public final static integer L30_COD_MOVI_LIQ := 3;
		public final static integer L30_IMP_TOT_LIQ := 8;
		public final static integer L30_DS_RIGA := 6;
		public final static integer L30_DS_OPER_SQL := 1;
		public final static integer L30_DS_VER := 5;
		public final static integer L30_DS_TS_INI_CPTZ := 10;
		public final static integer L30_DS_TS_END_CPTZ := 10;
		public final static integer L30_DS_STATO_ELAB := 1;
		public final static integer REINVST_POLI_LQ := L30_ID_REINVST_POLI_LQ + L30_ID_MOVI_CRZ + L30IdMoviChiu.Len.L30_ID_MOVI_CHIU + L30_DT_INI_EFF + L30_DT_END_EFF + L30_COD_COMP_ANIA + L30_COD_RAMO + L30_IB_POLI + L30_PR_KEY_SIST_ESTNO + L30_SEC_KEY_SIST_ESTNO + L30_COD_CAN + L30_COD_AGE + L30CodSubAge.Len.L30_COD_SUB_AGE + L30_IMP_RES + L30_TP_LIQ + L30_TP_SIST_ESTNO + L30_COD_FISC_PART_IVA + L30_COD_MOVI_LIQ + L30TpInvstLiq.Len.L30_TP_INVST_LIQ + L30_IMP_TOT_LIQ + L30_DS_RIGA + L30_DS_OPER_SQL + L30_DS_VER + L30_DS_TS_INI_CPTZ + L30_DS_TS_END_CPTZ + L30_DS_UTENTE + L30_DS_STATO_ELAB + L30DtDecorPoliLq.Len.L30_DT_DECOR_POLI_LQ;

		//==== CONSTRUCTORS ====
		private Len() {		}


		//==== INNER CLASSES ====
		public static class Int {

			//==== PROPERTIES ====
			public final static integer L30_ID_REINVST_POLI_LQ := 9;
			public final static integer L30_ID_MOVI_CRZ := 9;
			public final static integer L30_DT_INI_EFF := 8;
			public final static integer L30_DT_END_EFF := 8;
			public final static integer L30_COD_COMP_ANIA := 5;
			public final static integer L30_COD_CAN := 5;
			public final static integer L30_COD_AGE := 5;
			public final static integer L30_IMP_RES := 12;
			public final static integer L30_COD_MOVI_LIQ := 5;
			public final static integer L30_IMP_TOT_LIQ := 12;
			public final static integer L30_DS_RIGA := 10;
			public final static integer L30_DS_VER := 9;
			public final static integer L30_DS_TS_INI_CPTZ := 18;
			public final static integer L30_DS_TS_END_CPTZ := 18;

			//==== CONSTRUCTORS ====
			private Int() {			}

		}
		public static class Fract {

			//==== PROPERTIES ====
			public final static integer L30_IMP_RES := 3;
			public final static integer L30_IMP_TOT_LIQ := 3;

			//==== CONSTRUCTORS ====
			private Fract() {			}

		}
	}
}//ReinvstPoliLq
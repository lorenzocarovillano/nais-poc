package com.myorg.myprj.copy;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.SignType;

import com.myorg.myprj.ws.enums.Iabv0006CounterStd;
import com.myorg.myprj.ws.enums.Iabv0006GuideRowsRead;
import com.myorg.myprj.ws.occurs.Iabv0006EleErrori;

/**Original name: IABV0006-CUSTOM-COUNTERS<br>
 * Variable: IABV0006-CUSTOM-COUNTERS from copybook IABV0006<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class Iabv0006CustomCounters {

	//==== PROPERTIES ====
	public final static integer ELE_ERRORI_MAXOCCURS := 10;
	//Original name: IABV0006-COUNTER-STD
	private Iabv0006CounterStd counterStd := new Iabv0006CounterStd();
	//Original name: IABV0006-GUIDE-ROWS-READ
	private Iabv0006GuideRowsRead guideRowsRead := new Iabv0006GuideRowsRead();
	//Original name: IABV0006-LIM-CUSTOM-COUNT-MAX
	private short limCustomCountMax := (short)10;
	//Original name: IABV0006-MAX-ELE-CUSTOM-COUNT
	private short maxEleCustomCount := DefaultValues.BIN_SHORT_VAL;
	//Original name: IABV0006-ELE-ERRORI
	private []Iabv0006EleErrori eleErrori := new [ELE_ERRORI_MAXOCCURS]Iabv0006EleErrori;

	//==== CONSTRUCTORS ====
	public Iabv0006CustomCounters() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for int eleErroriIdx in 1.. ELE_ERRORI_MAXOCCURS 
		do
			eleErrori[eleErroriIdx] := new Iabv0006EleErrori();
		enddo
	}

	public void setCustomCountersBytes([]byte buffer, integer offset) {
		integer position := offset;
		counterStd.setCounterStd(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		guideRowsRead.setGuideRowsRead(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		limCustomCountMax := MarshalByte.readShort(buffer, position, Len.LIM_CUSTOM_COUNT_MAX);
		position +:= Len.LIM_CUSTOM_COUNT_MAX;
		maxEleCustomCount := MarshalByte.readBinaryShort(buffer, position);
		position +:= Types.SHORT_SIZE;
		for integer idx in 1.. ELE_ERRORI_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				eleErrori[idx].setEleErroriBytes(buffer, position);
				position +:= Iabv0006EleErrori.Len.ELE_ERRORI;
			else
				eleErrori[idx].initEleErroriSpaces();
				position +:= Iabv0006EleErrori.Len.ELE_ERRORI;
			endif
		enddo
	}

	public []byte getCustomCountersBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, counterStd.getCounterStd());
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeChar(buffer, position, guideRowsRead.getGuideRowsRead());
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeShort(buffer, position, limCustomCountMax, Len.LIM_CUSTOM_COUNT_MAX);
		position +:= Len.LIM_CUSTOM_COUNT_MAX;
		MarshalByte.writeBinaryShort(buffer, position, maxEleCustomCount);
		position +:= Types.SHORT_SIZE;
		for integer idx in 1.. ELE_ERRORI_MAXOCCURS 
		do
			eleErrori[idx].getEleErroriBytes(buffer, position);
			position +:= Iabv0006EleErrori.Len.ELE_ERRORI;
		enddo
		return buffer;
	}

	public void setLimCustomCountMax(short limCustomCountMax) {
		this.limCustomCountMax:=limCustomCountMax;
	}

	public short getLimCustomCountMax() {
		return this.limCustomCountMax;
	}

	public void setMaxEleCustomCount(short maxEleCustomCount) {
		this.maxEleCustomCount:=maxEleCustomCount;
	}

	public short getMaxEleCustomCount() {
		return this.maxEleCustomCount;
	}

	public Iabv0006CounterStd getCounterStd() {
		return counterStd;
	}

	public Iabv0006EleErrori getEleErrori(integer idx) {
		return eleErrori[idx];
	}

	public Iabv0006GuideRowsRead getGuideRowsRead() {
		return guideRowsRead;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer LIM_CUSTOM_COUNT_MAX := 4;
		public final static integer MAX_ELE_CUSTOM_COUNT := 2;
		public final static integer CUSTOM_COUNTERS := Iabv0006CounterStd.Len.COUNTER_STD + Iabv0006GuideRowsRead.Len.GUIDE_ROWS_READ + LIM_CUSTOM_COUNT_MAX + MAX_ELE_CUSTOM_COUNT + Iabv0006CustomCounters.ELE_ERRORI_MAXOCCURS * Iabv0006EleErrori.Len.ELE_ERRORI;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Iabv0006CustomCounters
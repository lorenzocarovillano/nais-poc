package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;

/**Original name: WK-APPO-DT-X<br>
 * Variable: WK-APPO-DT-X from program LOAS0820<br>
 * Generated as a class for rule FORCED_TO_CLASS.<br>*/
public class WkAppoDtX {

	//==== PROPERTIES ====
	//Original name: WK-DT-X-GG
	private string gg := DefaultValues.stringVal(Len.GG);
	//Original name: FILLER-WK-APPO-DT-X
	private char flr1 := '/';
	//Original name: WK-DT-X-MM
	private string mm := DefaultValues.stringVal(Len.MM);
	//Original name: FILLER-WK-APPO-DT-X-1
	private char flr2 := '/';
	//Original name: WK-DT-X-AAAA
	private string aaaa := DefaultValues.stringVal(Len.AAAA);


	//==== METHODS ====
	public string getWkAppoDtXFormatted() {
		return MarshalByteExt.bufferToStr(getWkAppoDtXBytes());
	}

	public []byte getWkAppoDtXBytes() {
		[]byte buffer := new [Len.WK_APPO_DT_X]byte;
		return getWkAppoDtXBytes(buffer, 1);
	}

	public []byte getWkAppoDtXBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeString(buffer, position, gg, Len.GG);
		position +:= Len.GG;
		MarshalByte.writeChar(buffer, position, flr1);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, mm, Len.MM);
		position +:= Len.MM;
		MarshalByte.writeChar(buffer, position, flr2);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, aaaa, Len.AAAA);
		return buffer;
	}

	public void setGgFormatted(string gg) {
		this.gg:=Trunc.toUnsignedNumeric(gg, Len.GG);
	}

	public short getGg() {
		return NumericDisplay.asShort(this.gg);
	}

	public char getFlr1() {
		return this.flr1;
	}

	public void setMmFormatted(string mm) {
		this.mm:=Trunc.toUnsignedNumeric(mm, Len.MM);
	}

	public short getMm() {
		return NumericDisplay.asShort(this.mm);
	}

	public char getFlr2() {
		return this.flr2;
	}

	public void setAaaaFormatted(string aaaa) {
		this.aaaa:=Trunc.toUnsignedNumeric(aaaa, Len.AAAA);
	}

	public short getAaaa() {
		return NumericDisplay.asShort(this.aaaa);
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer GG := 2;
		public final static integer MM := 2;
		public final static integer AAAA := 4;
		public final static integer FLR1 := 1;
		public final static integer WK_APPO_DT_X := GG + MM + AAAA + 2 * FLR1;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//WkAppoDtX
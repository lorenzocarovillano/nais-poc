package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;

import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.copy.DettTitCont;
import com.myorg.myprj.copy.Idsv0002;
import com.myorg.myprj.copy.Idsv00122;
import com.myorg.myprj.copy.Idsv0015;
import com.myorg.myprj.copy.Lccc0006;
import com.myorg.myprj.copy.Lccvgrzz;
import com.myorg.myprj.copy.Lccvtitz;
import com.myorg.myprj.copy.ParamComp;
import com.myorg.myprj.copy.TitCont;
import com.myorg.myprj.copy.WkDtcMax;
import com.myorg.myprj.ws.enums.GaranziaAcc;
import com.myorg.myprj.ws.enums.GaranziaBase;
import com.myorg.myprj.ws.enums.Ricerca;
import com.myorg.myprj.ws.enums.WsTpOggLccs0024;
import com.myorg.myprj.ws.enums.WsTpStatTit;
import com.myorg.myprj.ws.enums.WsTpTit;
import com.myorg.myprj.ws.occurs.WtitTabTitCont;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LCCS0490<br>
 * Generated as a class for rule WS.<br>*/
public class Lccs0490Data {

	//==== PROPERTIES ====
	public final static integer WTIT_TAB_TIT_CONT_MAXOCCURS := 200;
	//Original name: WK-PGM
	private string wkPgm := "LCCS0490";
	//Original name: PARAM-COMP
	private ParamComp paramComp := new ParamComp();
	//Original name: TIT-CONT
	private TitCont titCont := new TitCont();
	//Original name: DETT-TIT-CONT
	private DettTitCont dettTitCont := new DettTitCont();
	//Original name: LCCVTITZ
	private Lccvtitz lccvtitz := new Lccvtitz();
	//Original name: WK-DTC-MAX
	private WkDtcMax wkDtcMax := new WkDtcMax();
	/**Original name: WK-LABEL<br>
	 * <pre>----------------------------------------------------------------*
	 *  COMODO
	 * ----------------------------------------------------------------*</pre>*/
	private string wkLabel := DefaultValues.stringVal(Len.WK_LABEL);
	//Original name: WK-DT-DECOR-GRZ
	private string wkDtDecorGrz := DefaultValues.stringVal(Len.WK_DT_DECOR_GRZ);
	//Original name: WK-FRAZIONAMENTO
	private string wkFrazionamento := DefaultValues.stringVal(Len.WK_FRAZIONAMENTO);
	//Original name: WK-DT-INFERIORE
	private string wkDtInferiore := DefaultValues.stringVal(Len.WK_DT_INFERIORE);
	//Original name: WK-DT-SUPERIORE
	private string wkDtSuperiore := DefaultValues.stringVal(Len.WK_DT_SUPERIORE);
	//Original name: WK-DT-COMPETENZA
	private string wkDtCompetenza := DefaultValues.stringVal(Len.WK_DT_COMPETENZA);
	//Original name: WK-DT-DECORRENZA
	private string wkDtDecorrenza := DefaultValues.stringVal(Len.WK_DT_DECORRENZA);
	//Original name: WK-DT-ULT-TIT-INC
	private string wkDtUltTitInc := DefaultValues.stringVal(Len.WK_DT_ULT_TIT_INC);
	//Original name: WK-TIPO-RICORRENZA
	private char wkTipoRicorrenza := DefaultValues.CHAR_VAL;
	/**Original name: FORMATO<br>
	 * <pre>-- AREA ROUTINE PER IL CALCOLO</pre>*/
	private char formato := DefaultValues.CHAR_VAL;
	//Original name: DATA-INFERIORE
	private DataInferioreLccs0490 dataInferiore := new DataInferioreLccs0490();
	//Original name: DATA-SUPERIORE
	private DataSuperioreLccs0490 dataSuperiore := new DataSuperioreLccs0490();
	//Original name: GG-DIFF
	private string ggDiff := DefaultValues.stringVal(Len.GG_DIFF);
	//Original name: CODICE-RITORNO
	private char codiceRitorno := DefaultValues.CHAR_VAL;
	//Original name: IN-RCODE
	private string inRcode := DefaultValues.stringVal(Len.IN_RCODE);
	/**Original name: WK-RECUPERO-PROVV<br>
	 * <pre>----------------------------------------------------------------*
	 *  COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
	private string wkRecuperoProvv := "RP";
	//Original name: WK-PREMIO
	private string wkPremio := "PR";
	//Original name: LCCVGRZZ
	private Lccvgrzz lccvgrzz := new Lccvgrzz();
	/**Original name: GARANZIA-BASE<br>
	 * <pre>----------------------------------------------------------------*
	 *  FLAGS
	 * ----------------------------------------------------------------*</pre>*/
	private GaranziaBase garanziaBase := new GaranziaBase();
	//Original name: GARANZIA-ACC
	private GaranziaAcc garanziaAcc := new GaranziaAcc();
	//Original name: RICERCA
	private Ricerca ricerca := new Ricerca();
	//Original name: IX-INDICI
	private IxIndiciLccs0490 ixIndici := new IxIndiciLccs0490();
	//Original name: IEAI9901-AREA
	private Ieai9901Area ieai9901Area := new Ieai9901Area();
	//Original name: IEAO9901-AREA
	private Ieao9901Area ieao9901Area := new Ieao9901Area();
	//Original name: IDSV0002
	private Idsv0002 idsv0002 := new Idsv0002();
	//Original name: AREA-LCCC0062
	private WcomAreaPagina areaLccc0062 := new WcomAreaPagina();
	//Original name: LCCC0006
	private Lccc0006 lccc0006 := new Lccc0006();
	/**Original name: WS-TP-STAT-TIT<br>
	 * <pre>*****************************************************************
	 *     TP_STAT_TIT (Stato del titolo contabile)
	 * *****************************************************************</pre>*/
	private WsTpStatTit wsTpStatTit := new WsTpStatTit();
	/**Original name: WS-TP-OGG<br>
	 * <pre>*****************************************************************
	 *     TP_OGG (TIPO OGGETTO)
	 * *****************************************************************</pre>*/
	private WsTpOggLccs0024 wsTpOgg := new WsTpOggLccs0024();
	/**Original name: WS-TP-TIT<br>
	 * <pre>*****************************************************************
	 *     TP_TIT     (Tipologia del titolo contabile)
	 * *****************************************************************</pre>*/
	private WsTpTit wsTpTit := new WsTpTit();
	//Original name: IDSV0015
	private Idsv0015 idsv0015 := new Idsv0015();
	//Original name: IO-A2K-LCCC0003
	private IoA2kLccc0003 ioA2kLccc0003 := new IoA2kLccc0003();
	//Original name: WTIT-ELE-TIT-MAX
	private short wtitEleTitMax := DefaultValues.BIN_SHORT_VAL;
	//Original name: WTIT-TAB-TIT-CONT
	private []WtitTabTitCont wtitTabTitCont := new [WTIT_TAB_TIT_CONT_MAXOCCURS]WtitTabTitCont;
	//Original name: WDTC-ELE-DETT-TIT-MAX
	private short wdtcEleDettTitMax := DefaultValues.BIN_SHORT_VAL;
	//Original name: IDSV0012
	private Idsv00122 idsv00122 := new Idsv00122();
	//Original name: DISPATCHER-VARIABLES
	private DispatcherVariables dispatcherVariables := new DispatcherVariables();

	//==== CONSTRUCTORS ====
	public Lccs0490Data() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for int wtitTabTitContIdx in 1.. WTIT_TAB_TIT_CONT_MAXOCCURS 
		do
			wtitTabTitCont[wtitTabTitContIdx] := new WtitTabTitCont();
		enddo
	}

	public string getWkPgm() {
		return this.wkPgm;
	}

	public string getWkLabel() {
		return this.wkLabel;
	}

	public void setWkDtDecorGrzFormatted(string wkDtDecorGrz) {
		this.wkDtDecorGrz:=Trunc.toUnsignedNumeric(wkDtDecorGrz, Len.WK_DT_DECOR_GRZ);
	}

	public integer getWkDtDecorGrz() {
		return NumericDisplay.asInt(this.wkDtDecorGrz);
	}

	public string getWkDtDecorGrzFormatted() {
		return this.wkDtDecorGrz;
	}

	public void setWkFrazionamentoFormatted(string wkFrazionamento) {
		this.wkFrazionamento:=Trunc.toUnsignedNumeric(wkFrazionamento, Len.WK_FRAZIONAMENTO);
	}

	public integer getWkFrazionamento() {
		return NumericDisplay.asInt(this.wkFrazionamento);
	}

	public string getWkFrazionamentoFormatted() {
		return this.wkFrazionamento;
	}

	public void setWkDtInferioreFormatted(string wkDtInferiore) {
		this.wkDtInferiore:=Trunc.toUnsignedNumeric(wkDtInferiore, Len.WK_DT_INFERIORE);
	}

	public integer getWkDtInferiore() {
		return NumericDisplay.asInt(this.wkDtInferiore);
	}

	public string getWkDtInferioreFormatted() {
		return this.wkDtInferiore;
	}

	public void setWkDtSuperiore(integer wkDtSuperiore) {
		this.wkDtSuperiore := NumericDisplay.asString(wkDtSuperiore, Len.WK_DT_SUPERIORE);
	}

	public void setWkDtSuperioreFormatted(string wkDtSuperiore) {
		this.wkDtSuperiore:=Trunc.toUnsignedNumeric(wkDtSuperiore, Len.WK_DT_SUPERIORE);
	}

	public integer getWkDtSuperiore() {
		return NumericDisplay.asInt(this.wkDtSuperiore);
	}

	public string getWkDtSuperioreFormatted() {
		return this.wkDtSuperiore;
	}

	public void setWkDtCompetenzaFormatted(string wkDtCompetenza) {
		this.wkDtCompetenza:=Trunc.toUnsignedNumeric(wkDtCompetenza, Len.WK_DT_COMPETENZA);
	}

	public integer getWkDtCompetenza() {
		return NumericDisplay.asInt(this.wkDtCompetenza);
	}

	public void setWkDtDecorrenzaFormatted(string wkDtDecorrenza) {
		this.wkDtDecorrenza:=Trunc.toUnsignedNumeric(wkDtDecorrenza, Len.WK_DT_DECORRENZA);
	}

	public integer getWkDtDecorrenza() {
		return NumericDisplay.asInt(this.wkDtDecorrenza);
	}

	public void setWkDtUltTitInc(integer wkDtUltTitInc) {
		this.wkDtUltTitInc := NumericDisplay.asString(wkDtUltTitInc, Len.WK_DT_ULT_TIT_INC);
	}

	public integer getWkDtUltTitInc() {
		return NumericDisplay.asInt(this.wkDtUltTitInc);
	}

	public string getWkDtUltTitIncFormatted() {
		return this.wkDtUltTitInc;
	}

	public void setWkTipoRicorrenza(char wkTipoRicorrenza) {
		this.wkTipoRicorrenza:=wkTipoRicorrenza;
	}

	public char getWkTipoRicorrenza() {
		return this.wkTipoRicorrenza;
	}

	public void setFormato(char formato) {
		this.formato:=formato;
	}

	public void setFormatoFormatted(string formato) {
		setFormato(Functions.charAt(formato, Types.CHAR_SIZE));
	}

	public void setFormatoFromBuffer([]byte buffer) {
		formato := MarshalByte.readChar(buffer, 1);
	}

	public char getFormato() {
		return this.formato;
	}

	public void setGgDiffFromBuffer([]byte buffer) {
		ggDiff := MarshalByte.readFixedString(buffer, 1, Len.GG_DIFF);
	}

	public string getGgDiffFormatted() {
		return this.ggDiff;
	}

	public void setCodiceRitorno(char codiceRitorno) {
		this.codiceRitorno:=codiceRitorno;
	}

	public void setCodiceRitornoFromBuffer([]byte buffer) {
		codiceRitorno := MarshalByte.readChar(buffer, 1);
	}

	public char getCodiceRitorno() {
		return this.codiceRitorno;
	}

	public void setInRcodeFromBuffer([]byte buffer) {
		inRcode := MarshalByte.readFixedString(buffer, 1, Len.IN_RCODE);
	}

	public string getInRcodeFormatted() {
		return this.inRcode;
	}

	public string getWkRecuperoProvv() {
		return this.wkRecuperoProvv;
	}

	public string getWkPremio() {
		return this.wkPremio;
	}

	public void setWtitEleTitMax(short wtitEleTitMax) {
		this.wtitEleTitMax:=wtitEleTitMax;
	}

	public short getWtitEleTitMax() {
		return this.wtitEleTitMax;
	}

	public void setWdtcEleDettTitMax(short wdtcEleDettTitMax) {
		this.wdtcEleDettTitMax:=wdtcEleDettTitMax;
	}

	public short getWdtcEleDettTitMax() {
		return this.wdtcEleDettTitMax;
	}

	public WcomAreaPagina getAreaLccc0062() {
		return areaLccc0062;
	}

	public DataInferioreLccs0490 getDataInferiore() {
		return dataInferiore;
	}

	public DataSuperioreLccs0490 getDataSuperiore() {
		return dataSuperiore;
	}

	public DettTitCont getDettTitCont() {
		return dettTitCont;
	}

	public DispatcherVariables getDispatcherVariables() {
		return dispatcherVariables;
	}

	public GaranziaAcc getGaranziaAcc() {
		return garanziaAcc;
	}

	public GaranziaBase getGaranziaBase() {
		return garanziaBase;
	}

	public Idsv0002 getIdsv0002() {
		return idsv0002;
	}

	public Idsv00122 getIdsv00122() {
		return idsv00122;
	}

	public Idsv0015 getIdsv0015() {
		return idsv0015;
	}

	public Ieai9901Area getIeai9901Area() {
		return ieai9901Area;
	}

	public Ieao9901Area getIeao9901Area() {
		return ieao9901Area;
	}

	public IoA2kLccc0003 getIoA2kLccc0003() {
		return ioA2kLccc0003;
	}

	public IxIndiciLccs0490 getIxIndici() {
		return ixIndici;
	}

	public Lccc0006 getLccc0006() {
		return lccc0006;
	}

	public Lccvgrzz getLccvgrzz() {
		return lccvgrzz;
	}

	public Lccvtitz getLccvtitz() {
		return lccvtitz;
	}

	public ParamComp getParamComp() {
		return paramComp;
	}

	public Ricerca getRicerca() {
		return ricerca;
	}

	public TitCont getTitCont() {
		return titCont;
	}

	public WkDtcMax getWkDtcMax() {
		return wkDtcMax;
	}

	public WsTpOggLccs0024 getWsTpOgg() {
		return wsTpOgg;
	}

	public WsTpStatTit getWsTpStatTit() {
		return wsTpStatTit;
	}

	public WsTpTit getWsTpTit() {
		return wsTpTit;
	}

	public WtitTabTitCont getWtitTabTitCont(integer idx) {
		return wtitTabTitCont[idx];
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WK_LABEL := 30;
		public final static integer WK_DT_DECOR_GRZ := 8;
		public final static integer WK_FRAZIONAMENTO := 5;
		public final static integer WK_DT_INFERIORE := 8;
		public final static integer WK_DT_SUPERIORE := 8;
		public final static integer WK_DT_COMPETENZA := 8;
		public final static integer WK_DT_DECORRENZA := 8;
		public final static integer WK_DT_ULT_TIT_INC := 8;
		public final static integer GG_DIFF := 5;
		public final static integer IN_RCODE := 2;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Lccs0490Data
package com.myorg.myprj.ws;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.ws.redefines.WsDataCompRstX;

/**Original name: WORKING-STORAGE<br>
 * Working Storage Data: WORKING-STORAGE from program LVVS0114<br>
 * Generated as a class for rule WS.<br>*/
public class Lvvs0114Data {

	//==== PROPERTIES ====
	//Original name: WK-CALL-PGM
	private string wkCallPgm := "";
	//Original name: WS-DT-RST
	private WsDtRst wsDtRst := new WsDtRst();
	//Original name: WS-DT-RST-9
	private string wsDtRst9 := "00000000";
	//Original name: WS-DT-RST-AP
	private string wsDtRstAp := "00000000";
	//Original name: WS-DATA-COMPETENZA
	private long wsDataCompetenza := DefaultValues.LONG_VAL;
	//Original name: WS-DATA-COMP-RST-X
	private WsDataCompRstX wsDataCompRstX := new WsDataCompRstX();
	//Original name: WS-DT-9999MMGG
	private WsDt9999mmggLvvs0114 wsDt9999mmgg := new WsDt9999mmggLvvs0114();
	//Original name: RIS-DI-TRCH
	private RisDiTrchIdbsrst0 risDiTrch := new RisDiTrchIdbsrst0();
	//Original name: IX-DCLGEN
	private short ixDclgen := DefaultValues.BIN_SHORT_VAL;
	//Original name: IX-TAB-ADE
	private short ixTabAde := DefaultValues.BIN_SHORT_VAL;


	//==== METHODS ====
	public void setWkCallPgm(string wkCallPgm) {
		this.wkCallPgm:=Functions.subString(wkCallPgm, Len.WK_CALL_PGM);
	}

	public string getWkCallPgm() {
		return this.wkCallPgm;
	}

	public void setWsDtRst9(integer wsDtRst9) {
		this.wsDtRst9 := NumericDisplay.asString(wsDtRst9, Len.WS_DT_RST9);
	}

	public void setWsDtRst9FromBuffer([]byte buffer) {
		wsDtRst9 := MarshalByte.readFixedString(buffer, 1, Len.WS_DT_RST9);
	}

	public integer getWsDtRst9() {
		return NumericDisplay.asInt(this.wsDtRst9);
	}

	public string getWsDtRst9Formatted() {
		return this.wsDtRst9;
	}

	public void setWsDtRstAp(integer wsDtRstAp) {
		this.wsDtRstAp := NumericDisplay.asString(wsDtRstAp, Len.WS_DT_RST_AP);
	}

	public integer getWsDtRstAp() {
		return NumericDisplay.asInt(this.wsDtRstAp);
	}

	public string getWsDtRstApFormatted() {
		return this.wsDtRstAp;
	}

	public void setWsDataCompetenza(long wsDataCompetenza) {
		this.wsDataCompetenza:=wsDataCompetenza;
	}

	public long getWsDataCompetenza() {
		return this.wsDataCompetenza;
	}

	public void setIxDclgen(short ixDclgen) {
		this.ixDclgen:=ixDclgen;
	}

	public short getIxDclgen() {
		return this.ixDclgen;
	}

	public void setIxTabAde(short ixTabAde) {
		this.ixTabAde:=ixTabAde;
	}

	public short getIxTabAde() {
		return this.ixTabAde;
	}

	public RisDiTrchIdbsrst0 getRisDiTrch() {
		return risDiTrch;
	}

	public WsDataCompRstX getWsDataCompRstX() {
		return wsDataCompRstX;
	}

	public WsDt9999mmggLvvs0114 getWsDt9999mmgg() {
		return wsDt9999mmgg;
	}

	public WsDtRst getWsDtRst() {
		return wsDtRst;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer WS_DT_RST9 := 8;
		public final static integer WS_DT_RST_AP := 8;
		public final static integer WK_CALL_PGM := 8;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Lvvs0114Data
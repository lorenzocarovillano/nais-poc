package com.myorg.myprj.ws.occurs;

import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.core.marshal.MarshalByte;
import com.bphx.ctu.af.util.Functions;

import com.modernsystems.ctu.data.NumericDisplay;
import com.myorg.myprj.ws.enums.Lccc006TrattDati;

/**Original name: ISPC0140-SCHEDA-P<br>
 * Variables: ISPC0140-SCHEDA-P from copybook ISPC0140<br>
 * Generated as a class for rule OCCURS_GROUP.<br>*/
public class Ispc0140SchedaP {

	//==== PROPERTIES ====
	public final static integer AREA_VARIABILI_P_MAXOCCURS := 100;
	//Original name: ISPC0140-TIPO-LIVELLO-P
	private char tipoLivelloP := DefaultValues.CHAR_VAL;
	//Original name: ISPC0140-CODICE-LIVELLO-P
	private string codiceLivelloP := DefaultValues.stringVal(Len.CODICE_LIVELLO_P);
	//Original name: ISPC0140-FLAG-MOD-CALC-PRE-P
	private Lccc006TrattDati flagModCalcPreP := new Lccc006TrattDati();
	//Original name: ISPC0140-NUM-COMPON-MAX-ELE-P
	private string numComponMaxEleP := DefaultValues.stringVal(Len.NUM_COMPON_MAX_ELE_P);
	//Original name: ISPC0140-AREA-VARIABILI-P
	private []Ispc0140AreaVariabiliP areaVariabiliP := new [AREA_VARIABILI_P_MAXOCCURS]Ispc0140AreaVariabiliP;

	//==== CONSTRUCTORS ====
	public Ispc0140SchedaP() {
		init();
	}

	//==== METHODS ====
	public void init() {
		for int areaVariabiliPIdx in 1.. AREA_VARIABILI_P_MAXOCCURS 
		do
			areaVariabiliP[areaVariabiliPIdx] := new Ispc0140AreaVariabiliP();
		enddo
	}

	public void setSchedaPBytes([]byte buffer, integer offset) {
		integer position := offset;
		tipoLivelloP := MarshalByte.readChar(buffer, position);
		position +:= Types.CHAR_SIZE;
		codiceLivelloP := MarshalByte.readString(buffer, position, Len.CODICE_LIVELLO_P);
		position +:= Len.CODICE_LIVELLO_P;
		flagModCalcPreP.setFlagModCalcPreP(MarshalByte.readChar(buffer, position));
		position +:= Types.CHAR_SIZE;
		numComponMaxEleP := MarshalByte.readFixedString(buffer, position, Len.NUM_COMPON_MAX_ELE_P);
		position +:= Len.NUM_COMPON_MAX_ELE_P;
		for integer idx in 1.. AREA_VARIABILI_P_MAXOCCURS 
		do
			if (position <= buffer.size()) then
				areaVariabiliP[idx].setAreaVariabiliPBytes(buffer, position);
				position +:= Ispc0140AreaVariabiliP.Len.AREA_VARIABILI_P;
			else
				areaVariabiliP[idx].initAreaVariabiliPSpaces();
				position +:= Ispc0140AreaVariabiliP.Len.AREA_VARIABILI_P;
			endif
		enddo
	}

	public []byte getSchedaPBytes([]byte buffer, integer offset) {
		integer position := offset;
		MarshalByte.writeChar(buffer, position, tipoLivelloP);
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, codiceLivelloP, Len.CODICE_LIVELLO_P);
		position +:= Len.CODICE_LIVELLO_P;
		MarshalByte.writeChar(buffer, position, flagModCalcPreP.getFlagModCalcPreP());
		position +:= Types.CHAR_SIZE;
		MarshalByte.writeString(buffer, position, numComponMaxEleP, Len.NUM_COMPON_MAX_ELE_P);
		position +:= Len.NUM_COMPON_MAX_ELE_P;
		for integer idx in 1.. AREA_VARIABILI_P_MAXOCCURS 
		do
			areaVariabiliP[idx].getAreaVariabiliPBytes(buffer, position);
			position +:= Ispc0140AreaVariabiliP.Len.AREA_VARIABILI_P;
		enddo
		return buffer;
	}

	public void initSchedaPSpaces() {
		tipoLivelloP := Types.SPACE_CHAR;
		codiceLivelloP := "";
		flagModCalcPreP.setFlagModCalcPreP(Types.SPACE_CHAR);
		numComponMaxEleP := "";
		for integer idx in 1.. AREA_VARIABILI_P_MAXOCCURS 
		do
			areaVariabiliP[idx].initAreaVariabiliPSpaces();
		enddo
	}

	public void setTipoLivelloP(char tipoLivelloP) {
		this.tipoLivelloP:=tipoLivelloP;
	}

	public char getTipoLivelloP() {
		return this.tipoLivelloP;
	}

	public void setCodiceLivelloP(string codiceLivelloP) {
		this.codiceLivelloP:=Functions.subString(codiceLivelloP, Len.CODICE_LIVELLO_P);
	}

	public string getCodiceLivelloP() {
		return this.codiceLivelloP;
	}

	public void setIspc0140NumComponMaxEleP(short ispc0140NumComponMaxEleP) {
		this.numComponMaxEleP := NumericDisplay.asString(ispc0140NumComponMaxEleP, Len.NUM_COMPON_MAX_ELE_P);
	}

	public short getIspc0140NumComponMaxEleP() {
		return NumericDisplay.asShort(this.numComponMaxEleP);
	}

	public Ispc0140AreaVariabiliP getAreaVariabiliP(integer idx) {
		return areaVariabiliP[idx];
	}

	public Lccc006TrattDati getFlagModCalcPreP() {
		return flagModCalcPreP;
	}


	//==== INNER CLASSES ====
	public static class Len {

		//==== PROPERTIES ====
		public final static integer TIPO_LIVELLO_P := 1;
		public final static integer CODICE_LIVELLO_P := 12;
		public final static integer NUM_COMPON_MAX_ELE_P := 3;
		public final static integer SCHEDA_P := TIPO_LIVELLO_P + CODICE_LIVELLO_P + Lccc006TrattDati.Len.FLAG_MOD_CALC_PRE_P + NUM_COMPON_MAX_ELE_P + Ispc0140SchedaP.AREA_VARIABILI_P_MAXOCCURS * Ispc0140AreaVariabiliP.Len.AREA_VARIABILI_P;

		//==== CONSTRUCTORS ====
		private Len() {		}

	}
}//Ispc0140SchedaP